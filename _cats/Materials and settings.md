---
layout: category
category: "Materials and settings"
---

This content is obsolete and is now maintained
[at makerforums](https://forum.makerforums.info/c/k40/Material-Settings)
and is continuing on there. Please go log in there with the same google
account you used for Google+ and you will still own all the content you
wrote.  This site is out of date and not maintained.
