---
layout: post
title: "Hi guys ! ive worked out that i can swap an old coreldraw 12 to corel x7 (2014) but i have meet a problem :( my laser cutter wont to use the whole workspace..."
date: July 02, 2016 14:34
category: "Discussion"
author: "Krzysztof Soloducha"
---
Hi guys  ! ive worked out that i can swap an old coreldraw 12 to corel x7 (2014) but i have meet a problem :( my laser cutter wont to use the whole workspace... all staff what i draw is being moved to the top of the workspace  like using just top half of workspace. anyone got some ideas ? im very new to lasers by the way :D





**"Krzysztof Soloducha"**

---
---
**Jim Hatch** *July 03, 2016 22:09*

Definitely points to a Corel issue. I have X5 I think (although I just ordered the X8 upgrade - supposed to be some very nice stuff in there). It's got to be something with the plugin they install.


---
**Ben Marshall** *July 04, 2016 01:30*

I have corel x8 with the M2 nano board. At first I had a ratio issue similar to yours. Make sure anytime you adjust properties(print size,dpi,etc), you do it through the engrave tab, as it acts like a universal change. If you change properties thru the cut tab that are different from the engrave tab -that's when your ratios get out of whack(thru my experience). If I'm only cutting, I make my changes in engrave and verify them in the cut properties. I do this with every job and haven't had issues. If you are still having issues let me know and I'll screen shot if need be﻿


---
**Krzysztof Soloducha** *July 06, 2016 20:31*

im preapering myself for holiday and i havent got time for a machine but when i find a spare min i wil have a look into those settings again

very big THANKS guys !!

hope that you will enjoy your x8 Jim :) ive tried it and i have found that work i soo much easier :D good luck !


---
*Imported from [Google+](https://plus.google.com/100580354597411783871/posts/Try6skDoU4g) &mdash; content and formatting may not be reliable*
