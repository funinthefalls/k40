---
layout: post
title: "I'm reposting this as it was deleted for some reason and it might come in handy for other first time LaserWeb3 users"
date: August 04, 2016 22:34
category: "Software"
author: "Alex Hodge"
---
I'm reposting this as it was deleted for some reason and it might come in handy for other first time LaserWeb3 users. Here is my original post:



"Having issues with LaserWeb3. I can connect to (via USB) and send gcode to my smoothie k40 via Pronterface with no issues. For instance, G1 x100 burns a line at s.5 (my default) to x100. It's all behaving properly. I can move with and without laser and PWM is working properly. In LaserWeb3, I'm able to connect via USB (apparently, though the interface doesn't make it very clear), I'm able to use the movement buttons, I'm unable to fire the laser though. Also, if I send G1 codes it actually seems to lock up the interface and eventually stops responding to any command. Any ideas? I'm on the latest (non-edge) version of the smoothie firmware.﻿"



It turns out that additional configuration needs to be done after installation. In the GUI there is a settings area that needs some input to be complete before it is fully functional. +Peter van der Walt pointed out that the necessary settings will even be highlight RED. I'll report back with some screenshots this evening in case anyone is interested.





**"Alex Hodge"**

---
---
**Jon Bruno** *August 04, 2016 22:48*

Yeah, if you look at the settings tab the required fields are pointed out.

The pre-populated info is just placeholding.

You still need to manually enter the appropriate info. Those fields only display sample code. They are not pre defined defaults. They are merely syntax hints.﻿


---
**Alex Hodge** *August 04, 2016 22:53*

**+Jon Bruno** Yeah I imagine that if I actually fill out the settings it will start working properly LOL. First time running the software  for me and for whatever reason I guess it wasn't cool for me to offer up my feelings that it wasn't completely clear and easy to use. Thanks for your help!


---
**Jon Bruno** *August 04, 2016 23:00*

That's ok. Peter is an amazing developer bringing us a remarkable piece of software.

Because of his efforts laymen like myself can now squeeze every ounce of awesome out of my otherwise heap of scrap . Every day he's up early and goes to bed late further enhancing this package.



All of his hard work makes the extremely technical seem attainable for goons like me.

But that's where worlds collide.

I find myself learning about JavaScript and Gcode as well as a whole drawer full of acronyms just so I can keep up.



There is a learning curve, But once you start learning , the possibilities begin to become a reality.



We all owe Peter a great debt for his hard work and dedication to the project as well as his incredible support ethic.



So yeah, I guess popping in and complaining right off the bat could ruffle some feathers.. 

Lesson learned.



I hope you get LW3 working well and you stick around to see the enhancements continue to flow.



Remember "git pull" often and make sure to drop my man some coin so he can have an excuse to relax. #beerfund﻿


---
**Alex Hodge** *August 05, 2016 00:09*

**+Jon Bruno** I totally appreciate the work he does and if you were able to read all the comments of the post he deleted, you'd see that I wasn't disrespectful or complaining. Just offering some input on what was confusing for a first time user. His response before he kicked me out of the community and deleted the thread? "Noted. 1 out of 2000 users feel it's uncommunicative. Got it." He blocked me so I can't follow him on G+ either so I might chat with him outside a public forum. I know **+Ariel Yahni** saw everything that was written back and forth before it was deleted. Not sure if anyone else did, but whatever. I'll just keep poking away at this K40 until it's right and I'll post some good documentation if I can when it's done. This place is for helping each other not worrying about hurt egos.


---
**Alex Krause** *August 05, 2016 01:33*

This should be posted in the laserweb community... if you posted here in the K40 is probibly the reason it was deleted


---
**Ariel Yahni (UniKpty)** *August 05, 2016 01:54*

**+Alex Hodge**​ being using / testing LaserWeb since last year.  Ive seen every single update, so that's hundreds if not thousands.  For a dev it's imposible to accommodate every single user, feature, request. There's always room to improve but also think that's there is a reason for the things that are allready there


---
**Alex Hodge** *August 05, 2016 02:24*

It was posted in our software section. There's no mystery why he deleted it.


---
**Alex Hodge** *August 05, 2016 02:25*

**+Ariel Yahni** agreed. I actually think it's incredibly ambitious and well designed software. A power user tool. 


---
**Alex Hodge** *August 05, 2016 02:29*

Anyway guys enough said about the little issue earlier today. I'm still having issues using laserweb3 but I'm likely not going to pursue it further. 


---
**Scott Marshall** *August 05, 2016 02:59*

**+Alex Hodge** Drop me an email @ ALL-TEK594@aol.com

I'll  share my config and what I know about getting things working.

I don't have all the answers, but will help if I can.



Scott


---
**Ariel Yahni (UniKpty)** *August 05, 2016 03:14*

**+Alex Hodge**​ I'm here to help. Let me know what troubles you


---
**Scott Marshall** *August 05, 2016 04:10*

Thanks Ariel, You're an asset to this group.



I'm back st the endstop thing, and wondered if you had a chance to copy out the endstop part of your config?



When I invert the logic, the system think the endstop is made. I'm looking it over to see if I missed something.



If I can work out what the hitch is, I'm in good stead, and we can expect a bunch more Smoothie/Laserweb guys here.



I've already sold a fair number if install kits even after  explaining to people there's still the endstop issue. It seems they have faith in our group to work through such things.



I appreciate the help.



Thanks, Scott


---
**Alex Hodge** *August 05, 2016 05:34*

Thanks all. I'll follow up tomorrow. For now I can upload gcode directly to smoothie and it runs fine. Still can't get laserweb to behave.


---
*Imported from [Google+](https://plus.google.com/106033318557602563181/posts/1z4LLwnJ8PG) &mdash; content and formatting may not be reliable*
