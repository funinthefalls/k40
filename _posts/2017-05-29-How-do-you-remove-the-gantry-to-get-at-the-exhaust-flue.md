---
layout: post
title: "How do you remove the gantry to get at the exhaust flue?"
date: May 29, 2017 17:34
category: "Discussion"
author: "bbowley2"
---
How do you remove the gantry to get at the exhaust flue?







**"bbowley2"**

---
---
**Ned Hill** *May 29, 2017 18:21*

Depends on the machine cabinet you have as there can be some variations.  For mine there are four bolts on the inside edge of the frame.  Two in the front and two in the back that fasten to ledges in the front and back.  If you remove the gantry I recommend using a marker to add registration marks so you can get it back as close to same position as possible.  This will simplify realignment.  



Some people have  been able to remove the vent by reaching through and unscrewing the fasteners and titling and pulling the vent out without removing the gantry.  I had to unmount my gantry and unmount my tube as well because the bolt from one the of the tube brackets came down far enough to block the clearance needed to move the vent.



If you are still having problems removing the vent post some pics so we can see exactly what your setup is. 


---
**Ned Hill** *May 29, 2017 18:24*

Here's what I had to do to remove my vent.  Hopefully your's will be easier.

[plus.google.com - Ok just wanted to give a breakdown of what it took to removed the carrige fra...](https://plus.google.com/108257646900674223133/posts/TWpTcM9gQeY)


---
**bbowley2** *May 29, 2017 18:36*

Thanks.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/XwkraUMxXss) &mdash; content and formatting may not be reliable*
