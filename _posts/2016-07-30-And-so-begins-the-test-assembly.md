---
layout: post
title: "And so begins the test assembly ..."
date: July 30, 2016 23:22
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
And so begins the test assembly ...

![images/15725fdd1120f9e3451d040edb7f7f85.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15725fdd1120f9e3451d040edb7f7f85.jpeg)



**"Ashley M. Kirchner [Norym]"**

---
---
**3D Laser** *July 30, 2016 23:51*

What material are you cutting 


---
**Ashley M. Kirchner [Norym]** *July 30, 2016 23:59*

3mm birch


---
**3D Laser** *July 31, 2016 02:45*

**+Ashley M. Kirchner** really where do you get yours because that looks so much cleaner and more uniform than the ones I use 


---
**Ashley M. Kirchner [Norym]** *July 31, 2016 07:19*

From these guys:

[http://www.ocoochhardwoods.com/](http://www.ocoochhardwoods.com/)


---
**Scott Marshall** *July 31, 2016 16:11*

Whatcha buildin?

(apologies if I missed the prelude to the build)



Kinda looks like a motorcycle transmission, it had the dogs, clutch plates, but no shaft or forks.



Looks like fun in any case.



Scott


---
**Ashley M. Kirchner [Norym]** *July 31, 2016 16:20*

Some days I wish G+ would allow one to add more pictures to the original post ... I'll post another picture ...


---
**Scott Marshall** *July 31, 2016 16:23*

+1 on the picture thing. And stickies too.

Don't want to make work for you, just interested...


---
**Ben Marshall** *August 03, 2016 01:37*

"I've got too many clamps..." Said no woodworker ever


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/bEUKr6F8MWM) &mdash; content and formatting may not be reliable*
