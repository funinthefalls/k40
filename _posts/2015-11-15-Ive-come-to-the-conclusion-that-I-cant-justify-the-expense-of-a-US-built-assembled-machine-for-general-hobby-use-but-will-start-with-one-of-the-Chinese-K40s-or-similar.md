---
layout: post
title: "I've come to the conclusion that I can't justify the expense of a US-built/assembled machine for general hobby use, but will start with one of the Chinese K40s (or similar)"
date: November 15, 2015 03:33
category: "Discussion"
author: "Mark Clausen"
---
I've come to the conclusion that I can't justify the expense of a US-built/assembled machine for general hobby use, but will start with one of the Chinese K40s (or similar).  At some point in the future, I may perform incremental upgrades  (air assist, new control board, etc.).



Some questions remain:



There are multiple eBay sellers of what appear to be identical (or at least similar) machines.  Some, like the group's cover picture (with the digital meter), and others with the analog meter.  (I've seen some comments that the model with the analog meter has a roomier interior for eventual upgrades/improvements).



Question: Are these machines essentially identical, or at least similar?  



Question:  Are there any differences between the various vendors selling these?



Many thanks for your inputs!





**"Mark Clausen"**

---
---
**Anthony Coafield** *November 15, 2015 03:37*

When I was looking to get mine I found that several of the different sellers were actually the same place operating under different ebay names. That was in Australia. Exactly the same descriptions, including typos, descriptions updated on the same day, all sorts of things. I'd guess if they are different stores that they're getting them from the same place anyway and will be identical.



I can't comment on which is better between the two models I'm sorry.


---
**Stephen Warren** *November 15, 2015 04:47*

I'm a member of 2 makerspaces each with a K40, both purchased through eBay.



In the first case, I didn't do the ordering, but I believe we picked an item with the cheapest price (I think that was ~365 USD when I looked this summer). We ended up with an MS10105 controller board and MoshiDraw software. This HW/SW combination was almost unusable since something kept crashing in the middle of the job, and would then drive the laser head to the far bottom/right of the bed with the LASER on, requiring a power cycle to stop it since there are no end-stop switches. Needless to say this wasn't very useful, so someone soon spent hundreds of dollars on a LightObjects DSP controller upgrade.



In the second case, I wanted to avoid the immediate upgrade requirement. So, I picked a slightly more expensive eBay item that sounded like it had different software. The item did indeed; CorelDraw 12 with a LASER plugin, and LaserDRW(?), and some other controller board. This board was not /massively/ better on the SW front, but at least it is functional enough to continue using for now.



I would certainly recommend you try to avoid MS10105 and/or MoshiDraw.


---
**Stephen Warren** *November 15, 2015 04:50*

BTW, the seller I purchased from was motosupermart, although I have no idea if they're still selling the exact HW/SW combination I ended up with. My price was 424 USD including shipping to the US. I though I remembered the item listing showing a digital meter but the actual device came with an analog meter.


---
**Anthony Coafield** *November 15, 2015 06:03*

I made sure mine came with Corel as opposed to Moshi also.


---
**Stephane Buisson** *November 15, 2015 10:07*

to help to understand, a cheap K40 is nothing more of a combination of 5 main components:

a laser tube, a power supply , XY table, optics, electronic.

the 3 most expensive being the first 3.

the last 2 will need to be changed being the weakness. go for analogue, the digital make no difference. upgrade is cheap but time consuming. (about 200 usd on top)



Avoid AliExpress sellers, as the buyer protection scheme is a scam.


---
**Coherent** *November 15, 2015 12:58*

As you may have  already noticed, many of the Ebay sellers are in the same location. This is because some are the same seller or affiliated. They  seem to have multiple seller accounts. My guess is that this is primarily to catch buyers who shy away from account names which have had negative feedback and lowered their rating. All in all they are pretty much all the same machines. Some sellers list different software. or warranties. Corel Laser and LaserDrw are actually usable. Not the best, but will work. As far as warranty good luck. If you plan on upgrading the board, the software is irrelevant. Just stay clear of those sellers who web forum members have had issues and if you do have problems or damage take photo's and try to get problems taken care of quickly. Don't leave feedback until you have. Once you leave feedback, (especially if good) you have lost what little leverage you have for the seller to want to help you.


---
**Scott Thorne** *November 15, 2015 14:01*

Mine came with laser draw and coral laser as well as coral draw 12....I've not had any problems with the hardware or software....I'm doing done small upgrades...have not completed any yet...the first will be the laser head...then the table...the clamp that came in it is garbage.


---
*Imported from [Google+](https://plus.google.com/104269704312353087056/posts/dHCSco6ZAM4) &mdash; content and formatting may not be reliable*
