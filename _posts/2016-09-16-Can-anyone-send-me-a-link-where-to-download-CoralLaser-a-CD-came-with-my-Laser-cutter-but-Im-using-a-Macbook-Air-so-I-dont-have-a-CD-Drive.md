---
layout: post
title: "Can anyone send me a link where to download CoralLaser, a CD came with my Laser cutter but I'm using a Macbook Air so I don't have a CD Drive?"
date: September 16, 2016 11:28
category: "Discussion"
author: "Jack O'Reilly"
---
Can anyone send me a link where to download CoralLaser, a CD came with my Laser cutter but I'm using a Macbook Air so I don't have a  CD Drive? Any Help would be greatly appreciated :) Thanks!





**"Jack O'Reilly"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 12:24*

I have the K40 CD that came with mine hosted here:

[drive.google.com - K40 Software CD.rar - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwamRBZkduekxlMHM/view?usp=sharing)



It contains the non-genuine CorelDraw that was provided, the CorelLaser & some random stuff in Chinese.


---
**Ariel Yahni (UniKpty)** *September 17, 2016 22:50*

**+David Pasick**​ look above


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 23:37*

David Pasick: you may want to verify that your controller board is the m2nano, as this CD that I have linked above is for that controller board.


---
*Imported from [Google+](https://plus.google.com/116150448511980225555/posts/27ny6QGE3Bg) &mdash; content and formatting may not be reliable*
