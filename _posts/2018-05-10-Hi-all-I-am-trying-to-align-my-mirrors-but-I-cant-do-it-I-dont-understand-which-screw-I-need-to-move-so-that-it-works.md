---
layout: post
title: "Hi all I am trying to align my mirrors, but I can't do it, I don't understand which screw I need to move so that it works"
date: May 10, 2018 19:16
category: "Modification"
author: "Denisse Bazbaz"
---
Hi all 

I am trying to align my mirrors, but I can't do it, I don't understand which screw I need to move so that it works. 

[http://www.instructables.com/id/K40-Laser-Cutter-Mirror-Alignment/](http://www.instructables.com/id/K40-Laser-Cutter-Mirror-Alignment/)

Can someone help me or guide me throw so that I can align the mirrors? 

I already enter to a tutorial but still don't understand. Also when I try to align, after I make a complete tryout it doesnt burn nothing. 







**"Denisse Bazbaz"**

---
---
**David Piercey** *May 10, 2018 20:07*

Here's the guide I used: [https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/](https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/)


---
**HalfNormal** *May 10, 2018 20:11*

[https://www.everythinglasers.com/forums/topic/great-alignment-sources/](https://www.everythinglasers.com/forums/topic/great-alignment-sources/)

[everythinglasers.com - Great alignment sources - Everything Lasers](https://www.everythinglasers.com/forums/topic/great-alignment-sources/)


---
**Denisse Bazbaz** *May 11, 2018 01:51*

**+David Piercey****+HalfNormal** Hi So I already follow all the steps try to move the second mirror so that it aligns perfect when I move it to front. Here is the image when I move it back and front its the movement of the arrow. I don't know why the laser beam don't gets to the third mirror. The arrow shows the movement from first to second tryout.

![images/436fcd088d6ccc09feee6347042a2054.png](https://gitlab.com/funinthefalls/k40/raw/master/images/436fcd088d6ccc09feee6347042a2054.png)


---
**HalfNormal** *May 11, 2018 01:57*

Do not give up, it can take hours to get it right. Read all the alignment guides to get an idea of what you are up against. Sometimes you have to move the whole mount not just the screws. It is critical that you adjust everything in the order that is recommended or you will just chase your tail. You might need to adjust the laser tube as well. Remember these were made to work in a narrow area and not the whole bed.


---
**Denisse Bazbaz** *May 11, 2018 02:18*

**+HalfNormal** ok but I need to align it to the whole bed right? 

I really have tried a lot, but when I move something, the other thing moves and I can't know when Im doing it right... 




---
**HalfNormal** *May 11, 2018 02:21*

Yes whole bed. Read all guides.


---
**Denisse Bazbaz** *May 11, 2018 02:24*

But I think thats why the laser don't point to the third mirror. 

Ill try to do it again

Hope it works




---
**HP Persson** *May 11, 2018 11:36*

Visual picture of your problem, if i understood it correct.

The beam is not paralell, you need to move 2nd mirror plate to straighten it. Always re-center the beam in furthest position before you test-fire, if not you get old error + new movement as total error.

![images/db1d6e243edd9513dbd3d96c31203092.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db1d6e243edd9513dbd3d96c31203092.jpeg)


---
**Anthony Bolgar** *May 11, 2018 15:09*

One thing I have noticed on my lasers is that I need to have the beam enter the laser head slightly high in the circle so that when it bounces off the internal mirror it is centered. The rabbit laser guide explains why you need to do this.


---
**James Rivera** *May 11, 2018 17:05*

Don’t forget to cover the hole to your mirrors, or you may end up having to clean the smoke residue off them after you’re done with the alignment. I used blue masking tape and a penny to cover the hole.


---
**Denisse Bazbaz** *May 11, 2018 23:45*

**+James Rivera** Ok thanks!



**+HP Persson** Hi Thanks, yes indeed this is the problem, I already moved and try to align the mirror 2 so that the beam is parallel but haven't make it. Here I attached a video so that you can see better. 

I don't know why this is happening or what I need to do so that it works. Hope you can help me maybe by FaceTime or something it will be really helpful! 





![images/ee19decc104fbee803ac133cd056fffc](https://gitlab.com/funinthefalls/k40/raw/master/images/ee19decc104fbee803ac133cd056fffc)


---
**BEN 3D** *May 12, 2018 11:43*

**+Denisse Bazbaz** that is the way you should callibrate. Now my way would be Mark the 3 Screw s, move two on the top or two on a side in the same direction and with the same ammount of degree. In this way you would get a movement in horizontal or vertical direction. Then fire again and again, these two spots Need to be one. And also check that the carriage not goes Up and down. In my Laser machine the botton plate was wabbleling.


---
**Denisse Bazbaz** *May 13, 2018 15:01*

**+BEN 3D** Hi 

Thanks, I already tried to do what you told me and still the laser beam isn't parallel. How can I check that the carriage not goes up and down, I can't see it move. 

On the video I move it so that I can see movement and apparently it doesnt move. 

The screws that Ive been moving are the ones of the bracket and also on the mirror. 

Thanks

![images/a7813d7cb2055f7b75982676f93b853e](https://gitlab.com/funinthefalls/k40/raw/master/images/a7813d7cb2055f7b75982676f93b853e)


---
**BEN 3D** *May 14, 2018 08:05*

There was a hole in the botton of my machine. I put my Finger into it and hold the metal with my finger so I was able to push and pull the hole botton from a convex to a concarf surface. I used a transparent tube with water inside to check the carriage travel way. This tool is called Schlauchwaage in germany. Both end of the Tube are Open and you can use the water to Level two points to the same high. Just google Schlauchwaage unfortiantly I did not know the englisch word for it.


---
**HalfNormal** *May 14, 2018 12:20*

Water level


---
**Rob Simonton, Jr.** *May 17, 2018 04:23*

I had to adjust the position of the head a little for the laser to finally work right. Also, start with the first mirror and get it clean and aligned perfectly before moving on to the next one. It's worth the effort. Engraving is much more rewarding when the machine works right.


---
**BEN 3D** *May 17, 2018 04:45*

Hurra 😀 Gratulation 🎉, we all had have such a Moment in the past.. I think it is one of the difficults thing with the Laser!


---
*Imported from [Google+](https://plus.google.com/112467917885418951758/posts/i9YA8Y8JiKn) &mdash; content and formatting may not be reliable*
