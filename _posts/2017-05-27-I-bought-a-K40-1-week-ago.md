---
layout: post
title: "I bought a K40 1 week ago"
date: May 27, 2017 17:41
category: "Software"
author: "rich lee"
---
I bought a K40 1 week ago.

Mirrors REALLY out of alignment .. but I THINK ... I solved that ...sorta



2 QUESTIONS :

1) ... I can burn WOOD just fine ... but cannot get Anodized Tags to show even the smallest hint of a mark... no matter how slow I go .. or how HIGH I set the power.



2) ... CorelLaser & LaserDRW ....do THIS...

When I type something in as a creation.. it look fine.

When I go to the Pre-Engrave screen .. I need to mirror and / or rotate to read it right... HOWEVER ... no matter what I do, the x and Y are reversed.

When I move the object West ... it goes South . ... when I move it South ... it goes East or West.



At first it only did that on Corel X4 & X5 ... now it does it on LaserDRW also.



I did input the correct Model # and Serial # ( M2 ) so THAT does not seem to be it.



Any help is appreciated.



Rich





**"rich lee"**

---
---
**Ned Hill** *May 27, 2017 18:02*

1.) If you're not sure about aligning the mirrors I recommend the floating wombat guide for alignment.   If you use this guide I also recommend you use a marker to label the mirrors screws as indicated in the guide. 

[https://plus.google.com/108257646900674223133/posts/LGfT6SS3Tcc](https://plus.google.com/108257646900674223133/posts/LGfT6SS3Tcc)



2.)  Not sure about anodized aluminium as I've never tried it.  But it could be tied to a bad mirror alignment.



3.)  Your laser should engrave as seen in the preview screen for corellaser.  Make sure in the engrave manager screen that "Rotate" says "Do nothing" and "Mirror" is unchecked. 

![images/4e91cb542505ab2544c4de8d0c9be178.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4e91cb542505ab2544c4de8d0c9be178.png)


---
**rich lee** *May 27, 2017 18:25*

Thank you for your interest and response... BUT ... I SOLVED IT !



well ... I Partially take credit ....I decided to change from my W7 laptop ... to a W7 desktop .. both had the same Corel X$ on them ... both had the same Corel X4 on them.. and I had installed the LaserDRW as well on both.



SOMEHOW ... Both of my problems above... were COMPLETELY solved !



Perhaps MAGIC has entered into the mix.. and favored me.... OR   something about the USB PORT settings are different... Either way ... the is SOMETHING to be learned about this... but the current mystery is  ' What .. is thje lesson to be learned ? '

 

I did notice the laptop kept giving me  a  DEFENDER Security error ... and at one point updated SOMETHING ... but I did not have WiFi hooked up on the desktop... so it will never run into that issue.



Thanks again ... Rich


---
*Imported from [Google+](https://plus.google.com/105001403262883346709/posts/eHPRxjfng6R) &mdash; content and formatting may not be reliable*
