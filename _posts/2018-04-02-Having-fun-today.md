---
layout: post
title: "Having fun today!"
date: April 02, 2018 01:35
category: "Object produced with laser"
author: "HalfNormal"
---
Having fun today!



![images/9107ab80354de381f6ce6c6ae83850d3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9107ab80354de381f6ce6c6ae83850d3.jpeg)



**"HalfNormal"**

---
---
**James Rivera** *April 02, 2018 04:23*

LOL!


---
**Don Kleinschnitz Jr.** *April 02, 2018 11:09*

I see what you did there .... :)


---
**Ned Hill** *April 03, 2018 02:02*

It's not too late..




---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/RFDPMtof5Bg) &mdash; content and formatting may not be reliable*
