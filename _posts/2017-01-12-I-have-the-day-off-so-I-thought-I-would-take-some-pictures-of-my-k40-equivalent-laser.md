---
layout: post
title: "I have the day off so I thought I would take some pictures of my k40 equivalent laser"
date: January 12, 2017 19:28
category: "Discussion"
author: "Abe Fouhy"
---
I have the day off so I thought I would take some pictures of my k40 equivalent laser. This is a kl-3020 but looks very similar. I purchased it off a local classified for $200.



Some of the cool features I found out after reading is it came standard with air assist, a good air pump, laser dot, adjustable bed (knob in the corner), digital power output and one power supply vs two (not sure why this is a benefit, but have seen many people upgrade it). 



What is the same. Same case, same pump (which seems to good), same tubing, LED light strips, conflicting markings (instructions said the opposite of the video) for which is the water input/output. Setup was easy, but the instructions was laughable. No instructions on how to setup laserdrw config, but luckily I guessed and it worked. No interlocks, no safety features and no fuses. No fuses really?



Overall I am happy with it. I haven't had a ton of time to play with it yet though. Do you have any suggestions for it/upgrades to it? I am looking though dons wiki now. I have had it for about a week and have had 12hrs of time with it. It was working but not perfectly aligned.



![images/56afeb70f7e2d8a36b87e981f46876eb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/56afeb70f7e2d8a36b87e981f46876eb.jpeg)
![images/703341c10c173c7e0e071a9763b0259f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/703341c10c173c7e0e071a9763b0259f.jpeg)
![images/f1585c3f2ef49bbd661430b1f53c3686.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1585c3f2ef49bbd661430b1f53c3686.jpeg)
![images/f077fe6d8ecd52d0f794438b131151d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f077fe6d8ecd52d0f794438b131151d4.jpeg)
![images/57cbcc012be25d655fa2e252f70676d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57cbcc012be25d655fa2e252f70676d9.jpeg)
![images/ef551ca4b681c380a89f40c876117aac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef551ca4b681c380a89f40c876117aac.jpeg)

**"Abe Fouhy"**

---
---
**Cesar Tolentino** *January 12, 2017 19:58*

Good find. Congrats


---
**Sebastian Szafran** *January 12, 2017 20:22*

Go LaserWeb.


---
**Abe Fouhy** *January 12, 2017 20:54*

Thanks guys! I wanted to post some pics and notes, so if anyone is looking for a new machine they can search for kl-320, kl-3030 or k-320, kl-3020 on ebay too. Seems to bit a few hundred bucks cheaper :)


---
**Stephane Buisson** *January 13, 2017 09:12*

**+Abe Fouhy** the only real advantage is the setable bed with knob, compare basic K40. the air assist (really needed) seem to be poor upgrade quality. red dot nice   but 2 would be better (focus). digital Power setting isn't an advantage compare  a basic and more precise reading of all fashion Vu meter (not a % of power input, but real output). So really ask yourself if the adjustable bed is worth the price difference with the basic K40 ?

today the best K40 buy is a basic K40 (vu meter) with a good air assist and Cohesion 3D upgrade. adjustable bed isn't a priority at this power range, but could be handy


---
**Abe Fouhy** *January 13, 2017 10:11*

That makes sense, I am so new to this game. I am not really sure what is an upgrade yet, just happy that these are cheap enough for me that I can actually play with one that is my own. :)


---
**Stephane Buisson** *January 13, 2017 13:47*

cheap is what we have in common, the mod/upgrade is to make it good too.

and because the mod are cheap too, we are also demonstrating that bigger machine pricing isn't always justified, or throwing some light when it's the case.


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/D9qQS5VYNKc) &mdash; content and formatting may not be reliable*
