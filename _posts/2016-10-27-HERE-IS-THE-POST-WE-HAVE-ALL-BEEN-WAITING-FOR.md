---
layout: post
title: "HERE IS THE POST WE HAVE ALL BEEN WAITING FOR!"
date: October 27, 2016 17:09
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---


HERE IS THE POST WE HAVE ALL BEEN WAITING FOR!

...or at least I have.





DRUM ROLL ....................................



The open drain, NO level shifter, No pot connections, no strange grounds, no weird 5V connections,  ....... literally only 2 wires....



PWM control >>>>>>>



WORKS!



Just like I expected it to ......... I wasn't going crazy after all !!!!



**+Ray Kholodovsky** .... the "Test" switch works fine along with it!



...and it works for both **+Robert Rossi** and I  ..... heh!

... and **+Robert Rossi** and I have different LPS versions  .... heh!



<s>------------------------</s>

The first scope picture:

Channel 1 shows the internal PWM P2.4 

Channel 2 shows the "L" pin on the LPS



The second & 3rd scope pictures: 

Channel 1 shows the internal PWM from P2.4.

Channel 2 shows the lasers response to that PWM ......





Its your CHALLENGE to guess how I measured the laser output ...... :)





Channel 2 on the 2cnd and 3rd pictures is not what I expected and requires a a lot of scrutiny ..... so my list of things to explore just became huge ...... 





Gosh I am pleased with these results :) .....







![images/3f20dfe5eea7a26187ea505c348ab3f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f20dfe5eea7a26187ea505c348ab3f5.jpeg)
![images/a06ae4310829eae45c552965d117cef5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a06ae4310829eae45c552965d117cef5.jpeg)
![images/688eba45dcf15fdb2495ec2069f6458b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/688eba45dcf15fdb2495ec2069f6458b.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *October 27, 2016 17:21*

![images/fca5cb1eb8fcc144fa2b10f7d3c66c29.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/fca5cb1eb8fcc144fa2b10f7d3c66c29.gif)


---
**Nick Williams** *October 27, 2016 17:27*

Very Nice


---
**K** *October 27, 2016 18:12*

As someone who knows nothing about reading scopes, why is channel 2 not what you expected?


---
**Don Kleinschnitz Jr.** *October 27, 2016 18:30*

I generally expected the laser to somewhat follow  the supply turning on/off at the pwm rate.

 I also notice very large pulse at a much lower freq than the pwm.

 I figured I would see a large excursion on leading edge of every pwm pulse as the tube ionizes.

Got to think about this more and experiment with various pwm DF and frequencies. My setup could also be lieing to me...


---
**K** *October 27, 2016 18:32*

**+Don Kleinschnitz** Are you getting full 100% of your laser tube power? I'm assuming that's what the open drain pin does?


---
**Don Kleinschnitz Jr.** *October 27, 2016 18:54*

**+Kim Stroman** I don't know, does anyone? Unless you have a power meter at the output how do you know? I am fairly certain that the PWM is allowing 100% though?


---
**K** *October 27, 2016 18:56*

**+Don Kleinschnitz** I guess what I meant is, the open drain eliminates the need for the shifter, and the shifter was needed to actually pull 5V?


---
**Don Kleinschnitz Jr.** *October 27, 2016 19:25*

If you pull P2.4 raw from the processor on the main board that's a 3.3v signal and not open drain. So you need a level shifter to get a 5v transition. However level shifter are not set up as drivers, even if they provide 0-5v doesn't mean the are compatible with the LPS input. I don't plan to test this config and  all of this is avoided by using one of the FET drivers and two wires. 

Also a post on my blog that summarizes the how and why coming soon. 




---
**greg greene** *October 27, 2016 19:31*

What controller board?


---
**Ariel Yahni (UniKpty)** *October 27, 2016 19:31*

**+Don Kleinschnitz**​ so the 2 cables still go as you sketched a couple of days before? 


---
**Don Kleinschnitz Jr.** *October 27, 2016 19:34*

**+greg greene** I'm using a smoothie 5x. Shouldn't matter though.


---
**Don Kleinschnitz Jr.** *October 27, 2016 19:35*

**+Ariel Yahni** yes exactly like the pictorial. **+Robert Rossi**​ did also.


---
**greg greene** *October 27, 2016 19:35*

Hmmm, are you saying the stock board can do this????


---
**Don Kleinschnitz Jr.** *October 27, 2016 19:48*

**+greg greene** the stock board M2nano works this way? This is for conversions?


---
**Ariel Yahni (UniKpty)** *October 27, 2016 19:51*

**+greg greene**​ this all started as the stock board is only connected to the psu via 4 wires. 24v,GND,5v,L


---
**greg greene** *October 27, 2016 19:51*

Ah - so for Smoothies then "


---
**Don Kleinschnitz Jr.** *October 27, 2016 19:58*

**+Ariel Yahni**​ yes, that is one reason I was so convinced this would work, L is the only control from a nano to the LPS.

BTW:

I do not recommend using the LPS 5v for two reasons: 

1. It is implemented with a 7805 and provides 1amp max. 

2. I don't like connecting the controller without isolation to a HV supply. 

Note: 

In some supplies IN is also not isolated.


---
**Ariel Yahni (UniKpty)** *October 27, 2016 20:06*

**+Don Kleinschnitz**​ how about powering the board from the 24v and gnd from the psu


---
**Timo Birnschein** *October 27, 2016 20:42*

I have an Arduino Nano doing this no problem at all. Why did you expect this to not work? <b>confused</b>

Or are you working on your own PSU?



Actually I believe many people are just using direct connections with 5V I/O directly wired to the K40 PSU.


---
**Timo Birnschein** *October 27, 2016 20:58*

**+Don Kleinschnitz**  Number 1 really shouldn't be a problem since you're just powering the micro controller and the logic part of a stepper motor driver. The high current side of the motor driver stage and the motors are connected to 24V!

Number 2 is a good point but in my cause there was no problem so far and it should be usable because it was setup exactly like this with the moshi board as well.


---
**greg greene** *October 27, 2016 21:17*

Ok, now I am confused.  Timo, is your board the stock M2 Nano board or some other board?

What I'm trying to get into my older style senior brain is - can this be down with the stock board that came with the machine

The Smoothie has advantages over the stock board to be sure, but I'd like to get these results with the stock one if that is possible.

But then - would LaserWEB run with the stock board in this setup.



U'm confused again - time for a nap. :)


---
**Timo Birnschein** *October 27, 2016 21:54*

**+greg greene** I have an Arduino Nano running "grbl" as firmware ([https://github.com/McNugget6750/K40-grbl-board](https://github.com/McNugget6750/K40-grbl-board)). So I replaced the original Moshi board that came with the laser. In order to make the Moshi board work with gCode you'd have to hack the microprocessor and I don't think anyone has ever tried that before because it is just so much simpler to throw it away and get a new board running in two days.



So if you want to run LaserWeb, you have to change the controller board as far as I know.




---
**greg greene** *October 27, 2016 21:56*

Awesome - another reason for a smoothie type board then !



Thanks for taking the time to let me know - I appreciate it !


---
**Robert Rossi** *October 27, 2016 22:45*

Hi, everyone...I have my system configured like this as well.  Using the 2.4 small mosfet on a smoothieboard.  2 wires, no level shifters, no crazy ground requirements, test button still works, pot still in the system.....I also verified the PWM output and duplicated Don's rig. Nice clean signals. - Robert  


---
**Paul de Groot** *October 28, 2016 00:25*

That is exactly what I do with my grbl arduino controller. I connect directly to the laser psu. Works well but need to do some engraving examples to show how well or how bad it is☺


---
**HalfNormal** *October 28, 2016 00:30*

**+greg greene** The actual control of the laser on/off is done through L as a ground/low of the 24VDC of the laser power supply. The nano only enables the laser through the L input and does not do any laser power control. The actual power control is done with the pot that goes to the IN varying a 0 to 5 volt signal. 0 VDC being no power and 5 VDC being maximum power. If you vary the IN with a 5 VDC PWM signal, power control can be achieved. By enabling the laser with a PWM signal on the L input, this would be in essence controlling the laser by PPI explained here [http://smoothieware.org/forum/t-1604860/laser-pulsed-control-ppi](http://smoothieware.org/forum/t-1604860/laser-pulsed-control-ppi)

[smoothieware.org - Laser pulsed control (PPI) - Smoothie Project](http://smoothieware.org/forum/t-1604860/laser-pulsed-control-ppi)


---
**greg greene** *October 28, 2016 00:38*

Understood, the other half of the problem being to enable the pwm generator to respond to the drawing via interpreting the grayscale values as the value that you want the laser to operate at, as in a value of white - assuming that to be 0 or no power up to a value of 255 - assuming that to be black or full power (maybe not 100% so that the tube lasts - but something under perhaps 80% ?).  So you need a piece of hardware that reads the generated file (G Code?) and tells the pwm generator what value to send for that level of grayscale. I believe the Smoothie board does this - but the stock board cannot - as it does not have the required coding.



Did I get it right?


---
**HalfNormal** *October 28, 2016 00:40*

**+greg greene** Bingo! :-) The stock board does not have the wiring to control laser power but your interpretation is correct.


---
**Paul de Groot** *October 28, 2016 00:43*

**+Gregory Green**​ in grbl you can set the min and max value. The drawing program like inkscape does have a engraving plugin that translates via mapping the min and max shades to the set values. Laserweb 3 does do similar mapping where you set the range. Next to this comes the speed that needs to be matched to these mapping.  I believe laserweb3 does that automatically.  In inkscape i need to manually experiment what  speed works best.


---
**greg greene** *October 28, 2016 00:56*

AH, I see, ok then - that helps me make up my mind to get a smoothie board, but there does seem to be a lot of discussion about other bits required to make the board work as it should - like the level sifter that i believe keeps the  pwm signal in the 0 to 5V range - but then some say no it isn't needed.  Seems to me that since the smoothie is a purpose designed board it probably should have been  designed  with that in mind, but on the other hand - it is for machines other than the K40 laser - so maybe not.  Then there is the question of the PSU - some installations show another PSU in addition to the stock one - perhaps because they have changed th stepper motors and those require more power than the stock PSU has?



What I'm trying to get set in my mind is what I have to get to get it all to work without ordering stuff afterwards.  I've been in the electronics hobby since I built my first computer in High School - in 1967 - so yes I'm older than most dirt :) I'm also a Ham - so I understand how to operate a soldering iron - just trying to get a plan in place.



Thanks so much for your time and patience everybody has a little piece of the puzzle.


---
**Vince Lee** *October 28, 2016 05:45*

This is the same setup I have (with a AZSMZ Mini), using my custom adapter board, the 24V supply from the LPS, and directly controlling L with the mosfet output.  No level shifters.  No changes to power knob.  Since all these signals are available from the original cables, it allowed a reversible plug-in replacement that can also switch between the old and new boards with dip switches, which I find useful but others may or may not.  I had to play around quite a bit to find the sweet spot period where my LPS response gave a smooth range of power levels, which for me was at 200 microseconds.


---
**greg greene** *October 28, 2016 14:01*

Vince Lee - can you send me some info on your board ?  I have the stock K40 with the green output blocks - if that makes any difference



Thanks


---
**Don Kleinschnitz Jr.** *October 28, 2016 14:17*

**+Vince Lee** sitting in que behind my other work on this was the notion that most of us were running too high of a pwm freq. one that exceeded the supplys response. My calcs suggested 200us. The scope traces above may be showing proof of such...


---
**Robert Rossi** *October 28, 2016 14:45*

Don/group  - I am connecting my K40 to my my industrial exhaust fan this weekend...trying to get my system setup so I can actually start some engraving....All the tests I did worked great and I have very clean PWM signals from the smoothie....using just the FET output and a single ground wire......I'm using Inkscape and the visicut plugin to talk to my K40...and the small amount of marking and cutting I did worked great.     I was curios to what programs do the others use?...  Next I would like to determine which FET output is best 2.4 or 2.5 ...or if there is a difference between the two FETs.... I will run these tests and let the group know as soon as I get my system into its final home in my shop.


---
**Vince Lee** *October 29, 2016 08:49*

**+greg greene** here is a link to a doc I wrote up for it.  It was designed to plug in directly to the stock cabling for a moshiboard so the power supply pinout doesn't really matter.  I believe the power plug is slightly different for a nano board, so slight adjustments may be needed for nanoboard cabling.  [https://docs.google.com/document/d/1cdnJmbJ4UsGVkWX3LgAMKOc3ImU9cXQA5RWOFvzn8i0/edit?usp=drive_web](https://docs.google.com/document/d/1cdnJmbJ4UsGVkWX3LgAMKOc3ImU9cXQA5RWOFvzn8i0/edit?usp=drive_web)


---
**greg greene** *October 29, 2016 13:56*

!hanks !


---
**Don Kleinschnitz Jr.** *October 29, 2016 15:36*

**+Timo Birnschein** I never doubted it would work but many seemed unable to get it to work. So I verified it by tracing the supplies actual internals. Then tested on my own setup. There was little to no knowledge of the supplies internals and tons of confusion around many configurations. 


---
**Timo Birnschein** *October 29, 2016 16:49*

**+Don Kleinschnitz** Yeah, that sounds totally reasonable! This is definitely valuable information and will make people more comfortable converting their machines to something that actually works! :)




---
**jeremiah rempel** *February 20, 2017 22:37*

I did this very thing on mine.  Now I accidently blew up the 5v Cap on the LPS due to wiring mishap.  (I'm so dumb when I get excited).  After I replace the Capacitor I was up and running again and the output was great.  I did a greyscale of my daughter in the woods with a ton of light and you know what.  I could definitely see all her features and the trees and grass, but my MKS (I know but I didn't when I purchased it) resets after about 30 seconds of running like this.  Well the USB drops anyways.  I did have to change the PWM period to something like 2500 in the config for it to have proper power curves.  I'm surprised people thought it wouldn't work.  Of course, it does.  I just wish it would stay running. :(  I'm getting a smoothie ASAP.  Plus you all seem like a bunch of great people, so why not contribute.


---
**Don Kleinschnitz Jr.** *February 20, 2017 22:59*

**+jeremiah rempel** interesting, I just recently found some information on laser response while studying the electrical discharge characteristics of CO2 tubes. 

This reference claimed the cutoff frequency is more like 3Khz which = 333 us. This is due to the time it takes molecules to collide.

...............

This suggests that a PWM of much longer than 200us may be necessary to get good fidelity. Like 10x, which would be in the 2000-3000us range ...... and your at 2500 ...mmm.

................

I made the assumption that the current in the tube was a good facsimile of the light output. This new information would negate that assumption. The tube exhibits negative resistance when discharging so the current is not only non-linear it is probably faster than the time it takes the tube to reach the lasing energy levels...

................. 

See Appendix C in : 

[http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html](http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/a44Kq2Ccvto) &mdash; content and formatting may not be reliable*
