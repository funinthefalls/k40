---
layout: post
title: "Some inspiration to wake up your Maker spirit !!!"
date: November 13, 2014 16:24
category: "Object produced with laser"
author: "Stephane Buisson"
---
Some inspiration to wake up your Maker spirit !!!



Oven to shape acrylic, have a look to the embedded video.





**"Stephane Buisson"**

---
---
**Stephane Buisson** *November 13, 2014 17:38*

Imagine what form you can do with your 3D printer to unleash your creativity ;-)) 


---
**Stephane Buisson** *November 14, 2014 00:25*

important info about perspex:

[https://www.google.co.uk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=7&cad=rja&uact=8&ved=0CEsQFjAG&url=http%3A%2F%2Fwww.theplasticshop.co.uk%2Fplastic_safety_data_sheets%2Fopaque_colours_perspex_safety_data_sheet.pdf&ei=J9lkVPiEDYLb7AbJxYGICA&usg=AFQjCNH73YLNAvx5O04wboU2V6ltZJ0AsA&sig2=xjac8H3lmuGESykah7nzcQ&bvm=bv.79189006,d.ZGU](https://www.google.co.uk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=7&cad=rja&uact=8&ved=0CEsQFjAG&url=http%3A%2F%2Fwww.theplasticshop.co.uk%2Fplastic_safety_data_sheets%2Fopaque_colours_perspex_safety_data_sheet.pdf&ei=J9lkVPiEDYLb7AbJxYGICA&usg=AFQjCNH73YLNAvx5O04wboU2V6ltZJ0AsA&sig2=xjac8H3lmuGESykah7nzcQ&bvm=bv.79189006,d.ZGU)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/TJmofUXK5or) &mdash; content and formatting may not be reliable*
