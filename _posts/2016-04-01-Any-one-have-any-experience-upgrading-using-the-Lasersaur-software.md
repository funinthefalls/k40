---
layout: post
title: "Any one have any experience upgrading using the Lasersaur software?"
date: April 01, 2016 17:59
category: "Modification"
author: "Anthony Bolgar"
---
Any one have any experience upgrading using the Lasersaur software? I have found the hardware for $99, it is a drop in replacement for the K40 at [http://www.miles-milling.com/LasaurShield/LaserK40USB.htm](http://www.miles-milling.com/LasaurShield/LaserK40USB.htm)





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *April 01, 2016 20:17*

Good to know.


---
**Bob Milton** *June 21, 2016 00:18*

I was hesitant considering the above comments, but I hated the Moshi so much I decided to give it a try anyway.  It has worked really good so far.  Not saying there might be problems in the future such as crc errors.  I've been using it for about a month with no problems.  Apparently the code was ported to a Atmega2560, so I'm not sure that made it run better than the atmega328.  But for me it seems to run smooth. 


---
**Robi Akerley-McKee** *June 28, 2016 01:16*

I yanked the Moshidraw board on one K40 and replaced it with a Arduino Mega2560 and a RAMPS 1.4 and on the other I'm converting it right now to a MKS board.  Come the 1st I'm ordering a new laser tube, since I just fried my tube in the RAMPS machines. running build laser marlin firmware and inkscape and a modded turnkey laser in inkscape 0.91


---
**Cesar Tolentino** *October 09, 2016 21:58*

Hello guys. Just joined here. Had my laser since September 2013.  It worked great even with the moshidraw.  I were able to fine tune my work flow. But a month ago, the moshiboard died on me.  Long story short. I bought the lasersaur card mentioned above.  And surprisingly it worked wonderfully. It is truly a drop in no fuss solution. It just worked from day one. No configuration to play with.  Just wanted to share


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/Yf7VPGRzvdf) &mdash; content and formatting may not be reliable*
