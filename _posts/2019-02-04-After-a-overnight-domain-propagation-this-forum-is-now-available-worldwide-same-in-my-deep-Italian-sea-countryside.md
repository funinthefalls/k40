---
layout: post
title: "After a overnight domain propagation, this forum is now available worldwide, same in my deep Italian sea/countryside"
date: February 04, 2019 07:18
category: "Discussion"
author: "Stephane Buisson"
---
[https://forum.makerforums.info/c/k40](https://forum.makerforums.info/c/k40)



After a overnight domain propagation, this forum is now available worldwide, same in my deep Italian sea/countryside.

It is a delight to see all our categories up and running. Like we are used to.

Early March, when G+ will make the tools available to us, we will backup the full things from there.

We will see at that time, what we will be able to do with it. From a single Wordpress copy ([K40.makerforums.info](http://K40.makerforums.info)) or if we can extract and import here.



I will continue here to moderate, and keep the mood up, for all members to enjoy, as I did for nearly past 5 years, and I will be delighted if **+Don Kleinschnitz Jr.** agree to do the same.



Special Big Thank You to **+Anthony Bolgar** for having setting up our new home.



Stephane BUISSON



[https://forum.makerforums.info/t/this-is-now-the-new-home-our-k40-community/115](https://forum.makerforums.info/t/this-is-now-the-new-home-our-k40-community/115)





**"Stephane Buisson"**

---
---
**Stephane Buisson** *February 04, 2019 07:34*

I am so happy to access all the other communities from there without to have to login again with another user/pwd. Cnc OX, 3D Print,  Leather,  all the family to help me create, and MAKE.


---
**Gee Willikers** *February 04, 2019 08:14*

**+Stephane Buisson** what kind of leather work do you do? A year or two ago I was making wallets, small bag, belts and keychains. I've been meaning to get back into it seeing as how I've spent so much money on that hobby.


---
**Stephane Buisson** *February 04, 2019 08:36*

**+Gee Willikers** total newbee with leather, i buy a tools'kit from Ebay to train, my goal is to mix this knowledge to design multi-material objects. think like 3D print and lasercut to build a structure and cloth & leather to cover for a nice "rendering" final touch. 

[forum.snips.ai - Enclosure challenge (satellite) - 3D printing casings - Snips Community](https://forum.snips.ai/t/enclosure-challenge-satellite/1358)


---
**Duncan Caine** *February 07, 2019 12:42*

Hi Stephane, help needed.  I have signed up to the K40 group in the Maker forum but have not received the activation email, I have asked for it to be resent but that was 24 hours ago.  I have checked my spam folder and thats clear.  As a moderator can you help?  Thank you.  Duncan 


---
**Stephane Buisson** *February 07, 2019 12:48*

Hi **+Anthony Bolgar** could you have look at **+Duncan Caine** request. Thx


---
**Anthony Bolgar** *February 07, 2019 14:36*

I activated him.




---
**Ned Hill** *February 07, 2019 14:37*

**+Stephane Buisson** **+Duncan Caine** **+Anthony Bolgar**  I got it.  Duncan you are now active for the forum and welcome aboard.


---
**Duncan Caine** *February 07, 2019 14:42*

Thank you all, now able to login to new forum.


---
**O Comentarista** *February 10, 2019 20:41*

**+Stephane Buisson**, sorry for arrive late in discussion, but the choice of the platform target to migrate the member base still open? I think we have better platform alternatives (with a nicer look and more similar to google +) like Mastodon. I am a programmer in my spare time and can help you if you are curious about knowing more.



[joinmastodon.org - Mastodon](https://joinmastodon.org/)






---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/7HWDGQ5Sn5Z) &mdash; content and formatting may not be reliable*
