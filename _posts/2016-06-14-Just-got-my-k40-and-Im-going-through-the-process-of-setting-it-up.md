---
layout: post
title: "Just got my k40 and I'm going through the process of setting it up"
date: June 14, 2016 06:37
category: "Original software and hardware issues"
author: "Jack Sivak"
---
Just got my k40 and I'm going through the process of setting it up. So far everything works fine, but while aligning the fixed mirror I've run into an issue where the screws aren't long enough to get the laser to the proper height. With the default settings it's too high, but raising it the required amount would no longer make the screws fit (as need in the second picture). Is there a way to get longer screws that can replace them, or do I need some other hack?



![images/a0b77cce8c9856ac150619d55ed1ddbc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0b77cce8c9856ac150619d55ed1ddbc.jpeg)
![images/0d5f1dbc0b797b32d84ee62b43569ff8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d5f1dbc0b797b32d84ee62b43569ff8.jpeg)

**"Jack Sivak"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 07:09*

You should be able to get a similar screw but longer from any local hardware store. I had to shim my first mirror up as you've done also, but fortunately mine didn't require as much raising. Another option is you could maybe raise the white bracket that is below it. It's screws could be longer.


---
**Christoph E.** *June 14, 2016 09:49*

Like Yuusuf said, It's regular metric screws. Looks like M3 oder M4, you can get those everywhere.



Maybe you should also get some nuts, just in case the original threads break.


---
**Stephane Buisson** *June 14, 2016 10:15*

what is the tube diameter compare to the holder, any wrapping around the tube pushing it up ?


---
**Ariel Yahni (UniKpty)** *June 14, 2016 11:28*

Those are M3


---
**Jack Sivak** *June 14, 2016 16:25*

**+Yuusuf Sallahuddin** Thanks! I'll look into raising the lower bracket. If I can fit one or two spacers underneath it that should be enough.



**+Stephane Buisson** You mean raising the laser tube? I would have to lower it, and I'm hesitant to do major work on that component. Easier if I can just mess around with the mirror.



**+Chris E**  **+Ariel Yahni** I'll look into getting more M3 screws this week. Thanks!


---
**Scott Marshall** *June 14, 2016 17:12*

Somethings not right here.

This has never been an issue in the past, with anyone I'm aware of anyhow. 



Is this one of the K40s that is shipped with the tube separately? If so I suspect you may be assembling it wrong, which given the documentation with a K40, would be easy to do.



I suppose there's always a 1st time , but I'm thinking you may be attempting to fix a non-problem.



Also be aware that the beam needs only hit the mirror, it's not required to hit the exact center, while I wouldn't want it way off on an edge, there's nothing to ba gained by trying to center it.



The Tube should be set on the same plane as the head mirror. The trick to getting a flying mirror setup working properly is getting the beam PARALLEL to the direction of movement. This is necessary because the distance between the mirrors changes.



Ina fixed system, you can simply adjust out errors by moving the  X & Y screws to "hit the target", but with a moving system, you have to "hit the target" as the distance changes. If the beam is out of exact parallel with the direction of movement, the impact point will shift as the target moves.



Given that, your  tube should be set on exact same height as the center of the entry hole in your head. It also should be perfectly horizontal, or your beam will be heading upward or downward.



The beam MUST leave the last non moving mirror (the one in question - call it mirror 1) perfectly parallel to the plane of motion, in both X and Y.

in other words, it must hit the exact same point on the 2nd mirror at both ends of the Y travel. This same  applies to the  mirror 2 to mirror 3(head) gap as well.



So you see you cannot use the moving mirror adjustments to correct for height or lateral errors, they MUST be adjusted for a parallel beam. The errors are corrected by moving the tube and head.



This is how I do it, I'm sure there are many others:

Step #1 is to get the tube as close to on plane with the head.



Use the best torpedo level you have and level the machine rails (X & Y). Then install the tube level along it's length (which isn't exactly true to the beam, but it's close) and set it's height to match the the center of the heads beam entry opening using a length of 1/4" tubing as a water level. This gets you close.



Wire up the tube so you can fire it. Don't forget the cooling system - DON"T ever run the tube even for 'just a second' without water flow.



Then install Mirror #1 and adjust it's position using masking tape (you can put a small piece of aluminum foil between it and the mirror to keep adhesive and smoke off the mirror), and pulse the laser (test button) starting @ 0 and just turn it up until it leaves a tiny mark.



See the attached mirror alignment tutorial.



Once your mirrors are aligned, you may have to go back and adjust the tube. Final fine adjustments can be made by adjusting the head height and rotation to get the cutting beam perfectly vertical.



It's a pain the 1st time you do it, but after that,  it goes surprisingly fast.

Once your tube is placed, (and you're experienced) a mirror 'touch up" can be done in 5 minutes.



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)

I hope my ramblings help more than confuse.

Have fun



Scott


---
**Ariel Yahni (UniKpty)** *June 14, 2016 17:28*

Wise words **+Scott Marshall**​


---
**Jack Sivak** *June 14, 2016 18:00*

**+Scott Marshall** This came fully assembled; I didn't touch the laser tube. I was already following the exact same instructions you just linked me, and it says to get the beam in the center of the mirror, which I was attempting to do. Initially the beam was hitting pretty far towards the top and the side, so I've been trying to center the beam like the instructions say. Should I follow your advice or that of the linked document? Because it seems like they're conflicting.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 18:09*

**+Jack Sivak** From my experience, Scott's advice is spot on. There is no need to be perfectly centred on the 1st or 2nd mirror. The only mirror that it would be beneficial to be centred on is the 3rd (in the head) so that the beam bounces downwards at as close to perpendicular to the material (else you will have a mitred cut).


---
**Jack Sivak** *June 14, 2016 18:23*

**+Yuusuf Sallahuddin** Ok, cool. I'll ignore the part in the instructions about centering the beam then, and just focus on getting the fixed stage to consistently hit the y stage at every range. Thanks!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 18:44*

**+Jack Sivak** As Scott mentions, just make sure the beam is hitting somewhere that is not on the edges of mirror 1 & 2. As long as it's in the main body area will be fine. It took me a few days to get my alignment right the first time. Turned out it was because mirror 1 has the 2 screws at the back of the mount (like in your picture) that were sitting on top of the mount below & causing it to angle the beam downwards. I shimmed that up with 2 washers just so that it was no longer angling the beam downwards along the Y-axis. After that first issue, alignment now takes max of 10 minutes now that I know what I'm doing.


---
**Scott Marshall** *June 14, 2016 20:31*

The center of the mirror is really irrelevant, it reflects the same no matter where you hit it. I'd say to follow whatever makes good sense to you. It sure won't hurt anything to get the beam in the center, but if it requires hardware mods, somethings wrong.



Some people just can'r accept anything but perfect, and can't sleep at night just knowing their beam is off center, if you're one of them, by all means adjust till it's perfect.



If you want to get it close with the stock hardware and run it (I did), I won't  tell.



They did test that laser, so the beam made to the head once already exactly as it's built.



It does get easier as you go. The K40 is a patience and logical thinking puzzle masquerading as a handy machine.



Have fun.


---
*Imported from [Google+](https://plus.google.com/106960809851059243398/posts/QYKJDaNSqu4) &mdash; content and formatting may not be reliable*
