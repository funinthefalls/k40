---
layout: post
title: "Does anyone know that type, manuf. and/or part no"
date: July 08, 2016 20:51
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Does anyone know that type, manuf. and/or part no. for the 4 pin power connector on the M2 Nano is. I want to get the female side for my smoothie conversion.





**"Don Kleinschnitz Jr."**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2016 20:58*

VH3.96 by JST. There was also a comparable part from TE that I got some samples of. Fits like a glove. My breakout board for it arrives tomorrow. 


---
**William Klinger** *July 08, 2016 21:12*

Look at the MOLEX .157" series too. Jameco Electronics sells smaller quantities.


---
**Don Kleinschnitz Jr.** *July 08, 2016 22:32*

**+Ray Kholodovsky** **+William Klinger** thanks guys. I learned that these are called "wire board connectors"  or CONN RECEPT 4 POS W/RAMP SL-156   I am buying from: 

[http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=201599419&uq=636035940876325333](http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=201599419&uq=636035940876325333)

[http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=201599393&uq=636035940876325333](http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=201599393&uq=636035940876325333)


---
**Ray Kholodovsky (Cohesion3D)** *July 09, 2016 00:17*

**+Don Kleinschnitz** 

[http://www.digikey.com/product-detail/en/jst-sales-america-inc/B4P-VH(LF)(SN)/455-1641-ND/926549](http://www.digikey.com/product-detail/en/jst-sales-america-inc/B4P-VH(LF)(SN)/455-1641-ND/926549)

[http://www.digikey.com/product-detail/en/te-connectivity-amp-connectors/1-1123723-4/A106863-ND/686927](http://www.digikey.com/product-detail/en/te-connectivity-amp-connectors/1-1123723-4/A106863-ND/686927)



Like I said I've got the TE ones, perfect fit.


---
**Ray Kholodovsky (Cohesion3D)** *July 09, 2016 00:19*

**+William Klinger** technically it's .156" pitch not .157" this is the 3.96mm vs 4mm pitch difference, just like JST's XH is 2.5 not 2.54mm.  Believe it or not this small difference has been the source of many hours of sourcing inquiries for me over the last few days...


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/5tQQQBTqskF) &mdash; content and formatting may not be reliable*
