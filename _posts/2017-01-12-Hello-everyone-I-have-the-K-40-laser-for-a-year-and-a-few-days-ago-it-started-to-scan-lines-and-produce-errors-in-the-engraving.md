---
layout: post
title: "Hello everyone, I have the K-40 laser for a year and a few days ago it started to scan lines and produce errors in the engraving"
date: January 12, 2017 17:17
category: "Materials and settings"
author: "Manuel Maria Organv\u00eddez Rodr\u00edguez"
---
Hello everyone,

I have the K-40 laser for a year and a few days ago it started to scan lines and produce errors in the engraving. What can happen? What maintenance can be done to the laser machine?





![images/6e4108e61811606cb2a43f1134b8d938.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6e4108e61811606cb2a43f1134b8d938.jpeg)



**"Manuel Maria Organv\u00eddez Rodr\u00edguez"**

---
---
**Ned Hill** *January 13, 2017 12:54*

Probably need to check for loose belts, mirrors or lens.  Also check the pulley set screws on the motor shafts.








---
**Manuel Maria Organvídez Rodríguez** *January 15, 2017 11:05*

Thanks Ned Hill


---
**Russ “Rsty3914” None** *February 03, 2017 02:41*

My belts were very loose, and optical sensor did not line up well...


---
*Imported from [Google+](https://plus.google.com/104340601585724034705/posts/BtmMmbMHafV) &mdash; content and formatting may not be reliable*
