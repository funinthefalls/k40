---
layout: post
title: "The eagle file for k40"
date: October 16, 2016 22:13
category: "Hardware and Laser settings"
author: "Paul de Groot"
---
The eagle file for k40





**"Paul de Groot"**

---
---
**Don Kleinschnitz Jr.** *October 16, 2016 22:36*

U "ROCK"!


---
**Paul de Groot** *October 16, 2016 22:38*

**+Don Kleinschnitz** I blush 😄


---
**Paul de Groot** *October 19, 2016 08:30*

**+Don Kleinschnitz** the yellow laser input from the nano m2 is connected to ic 2 opto coupler !


---
**Don Kleinschnitz Jr.** *October 19, 2016 13:12*

**+Paul de Groot** Is it connected to the cathode of the opto directly or through a diode?

Looks like whatever gnds the cathode on IC-2 stops the PWM oscillator. 

On the (green-only) unit that I am tracing the collector of IC-2 connects to pin 4 (DTC) of TL494CN which I think changes the dead time to infinity, i.e. turning the pwm off. On your drawing IC-2's collector is connected to pin 5(CT).



Things are getting more complex as I have two supplies in front of me:

1. the one in my machine with green-white i/o connectors

2. the dead one I am tracing green-only i/o connectors.

It is already clear that these two are not wired exactly the same.



In the green-only supply the L pin and WP pin are connected together and then to IC-2.  Strangely, in all of the manufacturer literature explaining connecting a green-only supply the L pin is never part of the wiring schema, weird. Since L is the only thing connected to the LPS in a standard K40 it must be the control and I am assuming it is PWM.



On my supply (green-white) the L pin is not connected to any other i/o pin but I suspect it is connected through a diode to IC-2. 



What kind of supply are you tracing; green-white or green-only?



Does your board look like this and is there a # on the board? Mine has a JRL0150 # in the corner.





![images/90ab4abab72de67c78a13f6cfb4f339c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90ab4abab72de67c78a13f6cfb4f339c.jpeg)


---
**Don Kleinschnitz Jr.** *October 19, 2016 13:14*

The failure on the supply I am tracing. The diode that comes from the on board test switch ..... that seems unlikely :(.

Welcome to the K40 adventure!

![images/a96ea9074c65e2dcdec4a09835554c94.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a96ea9074c65e2dcdec4a09835554c94.jpeg)


---
**Paul de Groot** *October 19, 2016 20:33*

**+Don Kleinschnitz** i do have a different lps! They look very similar but slightly different layout. Will look up the serial model number tonight and maybe i can answer your other questions. Indeed it is such an adventure since they day the box arrived. I read somewhere that this was a chinese uni project and i can see how why people think that. Some parts of the design are really questionable. Wonder why negative logic? Couldn't they wire the anode of the opto couplers to the connector? Questions, questions ....


---
**Paul de Groot** *October 19, 2016 20:54*

Saw your board has a web address! Look at [en.jnmydy.com](http://en.jnmydy.com) and read the description under products. Still smiling.

[en.jnmydy.com - Jinan Zhenyu Electronics Co., Ltd](http://en.jnmydy.com)


---
**Don Kleinschnitz Jr.** *October 19, 2016 20:59*

Ya saw that and went there, not very useful?


---
**Paul de Groot** *October 19, 2016 22:00*

**+Don Kleinschnitz** just couldn't resist and had a laugh. Omg if they would build things for planes would you travel on them?


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/HJCXH9s7sex) &mdash; content and formatting may not be reliable*
