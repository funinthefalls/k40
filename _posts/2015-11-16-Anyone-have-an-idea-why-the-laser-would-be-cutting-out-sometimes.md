---
layout: post
title: "Anyone have an idea why the laser would be cutting out sometimes?"
date: November 16, 2015 05:44
category: "Discussion"
author: "Anthony Coafield"
---
Anyone have an idea why the laser would be cutting out sometimes? I've had it lose focus and then stop, other times it's gotten a lot duller and stopped. It's also not cutting very well. 



I'm guessing an air assist should help the cutting a lot, I cleaned off a mirror and it helped, but a bit worried about the losing power. If I turn the laser switch off and let it continue with the job I can then turn the switch back on and it'll work for a few more seconds...



I leave it for 1/2 an hour or so after it happening as I fiddle around on other stuff but it's happening a lot more regularly now. I couldn't even cut a 15cm line in 3mm plywood this afternoon. I have fresh water running through the laser. Doesn't look to be any air bubbles. 



EDIT: Just happened again, water cool going through it (lots of ice packs in bucket) the current is still up when it stops (12mA), that doesn't change at all. 



The manufacture date of the tube is 17 Sep 2013, so over 2 years old. Is that odd for a brand new machine?﻿





**"Anthony Coafield"**

---
---
**Imko Beckhoven van** *November 16, 2015 06:24*

How "old" is the tube meaning any idea on the total hours used?


---
**Anthony Coafield** *November 16, 2015 06:47*

Maybe 2.5-3hrs total.



Although that said when I got the engraver the tube wasn't packed in foam like it said it should have been. Took me 3 days to get the stripped screw out of the back to check the tube in the first place.



EDIT: Just happened again, water cool going through it (lots of ice packs in bucket) the current is still up when it stops (12mA), that doesn't change at all. 



The manufacture date of the tube is 17 Sep 2013, so over 2 years old. Is that odd for a brand new machine?


---
**Imko Beckhoven van** *November 16, 2015 07:37*

assuming you got the cooling running at a normal flow, my guess is the tube.... I had the same issue's started with using losing power (same settings not cutting all the way) after that it made some weird noice and no beam. Solution ... went online orderd a replacement tube.


---
**Anthony Coafield** *November 16, 2015 07:41*

That's what I'm thinking too **+Imko Beckhoven van** but that shouldn't happen with less than 3 hours of run time should it?


---
**Imko Beckhoven van** *November 16, 2015 07:49*

No absolutly not!! You can destoy a tube in les than 3 hours but only is you crank everyting way up.......


---
**Anthony Coafield** *November 16, 2015 09:16*

Thanks. I saw it hit 17 once as it started to cut and turned it straight down, so definitely no cranking! I've most been engraving anyway. One thing going for me is I haven't left feedback yet...


---
**Scott Thorne** *November 16, 2015 11:00*

It could be a number of things, thermal expansion of the tube internal mirrors causing misalignment, power supply with a weak soldier joint or bad fly back transformer, or just weak gas in to tube.


---
**Scott Thorne** *November 16, 2015 11:02*

You need to look at the tube when it's losing power like that because if the meter is still going up like that then it sounds like a mirror misalignment.


---
**Scott Thorne** *November 16, 2015 11:09*

One other thing, if you get the water too cold, you will get condensation in the tube and that's not a good thing., I just use regular distilled water and I've never had a problem, when they say keep the water around 35....that's Celsius.


---
**Mark Clausen** *November 16, 2015 13:27*

I'm a complete novice (haven't even obtained my machine, yet), but am fairly well-versed in physics and engineering.  Reading all the specs of various laser tube manufacturers shows that the general range of cooling water temperature is 10-30 C (50-86 F), or sometimes 10-40 C (50-104 F), with an optimum temp of 20 C (68 F).   Note that there is a bottom end to the specified range of cooling water temps.



Short version:  Avoid an ice-water bath unless you can maintain the water temp flowing into the tube to be within the specified range.



Long version:  Using an icewater bath will certainly move the cooling temp down below the specified range. Condensation is one problem (as has already been mentioned).  Several university laser physics guides I read mention condensation specifically, both with respect to optics as well as the high-voltage current bleed-off (of course, those were for much larger systems than we use). Another threat is the potential to crack a warm tube with extremely cold water.  Finally (just speculation on my part), if thermal expansion can cause a mis-alignment of optics, then thermal contraction can do the same -- hence the optimum operating temperature of a tube.  

I look forward to hearing what those with actual, hands-on experience think.


---
**Scott Thorne** *November 16, 2015 14:27*

You are right Mark, all of the above, avoid ice water...I feel comfortable with my water at room temp.


---
**Coherent** *November 16, 2015 15:33*

Hopefully it's not the tube. They usually slowly loose power and then just quit, but there are always lemons & exceptions. (like running it without any cooling). If it's arcing somewhere it should be noticeable. It could be the control board, as electronics tend to fail when they heat up. But before pointing to the electronics, I would check the wiring and mirrors/lens. Check all the wiring really well. Something may be loose and the machine movement makes it come and go. Test fire it at low power on a piece of scrap and wiggle wires and see if it cuts out. Alignment and lens focus issues will obviously effect cut/engrave quality. Triple check all your mirror alignments and make sure they (and the lens) are clean. Check your bed height from the head at all four extremes of the X/Y. If it does turn out to be a component of the control board or power supply, they do make a "freeze" spray that can be helpful in locating bad/overheating components. Even poor solder joints can cause hard to locate issues as things heat up or cool down. Sorry I can't be more helpful and hopefully it's something you can isolate.   


---
**Anthony Coafield** *November 17, 2015 00:27*

**+Mark Clausen** Thanks for the info. I learned a lot. It's almost summer here in Australia so the resting temperature of the water was is the mid to high twenties. I have a 20L bucket and with the ice in it the temp was still around the 20C mark, so there doesn't seem to be any problem there. 


---
**Anthony Coafield** *November 17, 2015 00:30*

**+Scott Thorne** Thank you for your suggestions. I tried to watch it as it happened, but I couldn't actually see anything. I've been thinking about the misalignment, but if it loses power and gradually fades out around the correct line I would assume they're all right. I double check them though.


---
**Anthony Coafield** *November 17, 2015 00:33*

**+Marc G** Thank you for your help. It's a nightmare helping to troubleshoot something you can't see, hear or feel. Engrave works beautifully everywhere, and I'm only having an issue when cutting, so I'll check the electronics again, see if anything is going on there. 


---
**Gary McKinnon** *November 18, 2015 14:49*

I have heard some people have power issues if the unit isn't grounded properly.


---
**Anthony Coafield** *November 18, 2015 20:33*

Here in Australia we have a ground wire on every socket and plug, so I've used a grounded wire and checked inside that the ground connection is also connected to the plug (which it is). I can check a few other power cords though and see if that does anything. Thanks **+Gary McKinnon** 


---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/aFvCkxmZj4a) &mdash; content and formatting may not be reliable*
