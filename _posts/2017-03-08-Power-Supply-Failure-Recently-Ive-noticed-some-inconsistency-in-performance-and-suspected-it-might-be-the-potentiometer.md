---
layout: post
title: "Power Supply Failure? Recently I've noticed some inconsistency in performance and suspected it might be the potentiometer"
date: March 08, 2017 14:45
category: "Original software and hardware issues"
author: "Jeff Johnson"
---
Power Supply Failure?

Recently I've noticed some inconsistency in performance and suspected it might be the potentiometer. I ordered a new one but it arrives tomorrow. Last night I was engraving some wood and everything was working well until I heard a pop. At first I suspected it was a capacitor but there was no smell of a blown cap. I checked the tube and there are no cracks or leaking water. Finally, I disassembled the power supply and found that the plastic shell around the transformer powering the laser tube was extremely hot and cracked. Could this be my problem all along - a failing power supply? The symptoms now are that it doesn't go above .5mA. This is enough to get the laser tube to glow a little but not nearly enough to create a laser beam. The seller on eBay still exists and provided a 2 year warranty so I'm waiting for a response from them. It came from the states. Maybe the pictures will help someone identify the problem. By the way, in the time I've owned this is't paid for itself many times over so if I have to buy a new one I'm Ok with that. 



![images/8119100f2a8370a5a5eae4b0be505e89.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8119100f2a8370a5a5eae4b0be505e89.jpeg)
![images/7a8fa80b818478c2fe6ee8c3b8176a4e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a8fa80b818478c2fe6ee8c3b8176a4e.jpeg)
![images/395dbceb5e06960f9dc0375ac20f0bff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/395dbceb5e06960f9dc0375ac20f0bff.jpeg)
![images/da679d52b2fec205283e39b052b3faa1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/da679d52b2fec205283e39b052b3faa1.jpeg)

**"Jeff Johnson"**

---
---
**greg greene** *March 08, 2017 15:06*

Yup  looks like the HV circuits are toast - check the hv connection to the tube, I'm thinking you will find  corrosion there causing high resistance which means a heavier draw on the components which results in what you see. 


---
**Jeff Johnson** *March 08, 2017 16:04*

The connections on the tube are sealed pretty well. I'll have to cut back a silicone cover to look at it but I'll see what I can find.


---
**Don Kleinschnitz Jr.** *March 08, 2017 17:35*

What kind of coolant are you using .....?


---
**Jeff Johnson** *March 08, 2017 19:48*

**+Don Kleinschnitz** I use a 5 gallon bucket with 3.5 gallons of water and a 1 gallon frozen water jug.  Plus a little of the chlorine I use in my pool to keep it from getting slimy.


---
**Jeff Johnson** *March 10, 2017 14:42*

As expected, the ebay seller won't respond to warranty questions.. I've filed a PayPal dispute so maybe they'll respond that way. Even though the seller is in China, it shipped from a warehouse in California. Hopefully this gets their attention.


---
**Don Kleinschnitz Jr.** *March 10, 2017 15:48*

**+Jeff Johnson** FYI we recently found that very little Clorox is needed 6ml/ 5 gallons. High conductivity coolant causes tube and LPS problems. So be careful that you aren't using to much chlorine. It doesn't take much to raise the conductivity. I don't know how this amount would relate to pool chlorine.


---
**Jeff Johnson** *March 10, 2017 16:22*

I use a tiny chip off of a tablet. I'm not sure how much but it seems to be about the same ratio used in my pool and can barely be detected by smell. I'll be sure to actually measure  before I replace the water next time. Thanks for the tip!


---
**Fernando Bueno** *March 11, 2017 13:33*

Sorry to get me in the conversation with an offtopic, but I have a problem with my newly purchased K40 and that **+Jeff Johnson** could fix it. When the machine was connected yesterday, there was a short circuit and the Fu2 fuse of the power supply was blown. As my machine is the same as yours and has the same power supply, could you look at what values the fuse has and tell me? In your first picture, it is the one in the upper left, next to a bridge to the right of the green AC input connector. Although I enlarge the photograph, I can not see the values. Thank you very much.


---
**Jeff Johnson** *March 11, 2017 14:09*

**+Fernando Bueno** this is the best picture I  can get. Does this help?

![images/c82c058000c9621c5a8248362e108675.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c82c058000c9621c5a8248362e108675.jpeg)


---
**Fernando Bueno** *March 11, 2017 14:28*

Thank you for answering so quickly, **+Jeff Johnson**. I can not see the value. It is seen that it is 250v, but the Amperes are below and can not be read. I have put a 5A and it has re-melted as soon as I turned on the machine and now I do not know if it has been melted because it is of a lower value than the correct one or because the problem comes from the power supply.

This is how my fuse has been and right where it puts the Amperes, is where it is burned. Bad luck, right?

![images/b6692de4b2cd1d6c169f95dfe7ce0ee8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6692de4b2cd1d6c169f95dfe7ce0ee8.jpeg)


---
**Don Kleinschnitz Jr.** *March 11, 2017 14:31*

**+Fernando Bueno** if this is blowing without firing your laser then you likely have a broken supply. Any charred parts you can see?


---
**Fernando Bueno** *March 11, 2017 15:11*

**+Don Kleinschnitz**​ The only thing that is burned is the fuse, the rest is clean. I think the problem may come from the switch, that when I turned it, I did it very slowly and caused a spark. Now you may have soldered a fuse of a lower amperage of the proper and that makes it melt just after turning on the machine. But not if this is true, the problem is in the switch or elsewhere before the power supply.


---
**Don Kleinschnitz Jr.** *March 11, 2017 17:09*

**+Fernando Bueno** I guess we will have to see if it blows again after you replace the fuse. It's unlikely that a switch caused this unless the supply is wired in wrong. You can trace the AC wiring and you can test the inputs to the supply for shorts when in and out of the AC circuit.


---
**Fernando Bueno** *March 11, 2017 17:29*

**+Don Kleinschnitz** I am almost convinced that the problem is in the switches, both in the main and in the emergency. On Monday I'm going to go buy one of each to make sure there's not the problem.

But now there is the fuse of the power supply. I saw that a few weeks ago you published photographs of two power sources and the one shown in the photograph on the left is the same model as mine. Could you see if you see the value of the Fu2 fuse? I understand that it must be 10A or higher, since the fuse in the plug of the socket is 10A. The fuse that I have soldered and that has been burned as soon as I connected the machine, it was 5A and it makes sense that this happened as well. Thank you very much for your help.


---
**Don Kleinschnitz Jr.** *March 12, 2017 12:38*

**+Fernando Bueno** 



1) you can pull off the wires and check that the switches are working correctly with a multi-meter.

2) this is new machine and it failed at first use? If so you should get in contact with the vendor and do not tell them you have been inside the supply :)

3) I had to pull the fuse to identify it. Its a T5A 250V slow blow fuse. Here is a replacement source: 

[https://www.amazon.com/Axial-T5AL250V-T5L250V-Cartridge-5X20mm/dp/B00ENL0YOE/ref=sr_1_3?s=industrial&ie=UTF8&qid=1489321200&sr=1-3&keywords=T5A+glass](https://www.amazon.com/Axial-T5AL250V-T5L250V-Cartridge-5X20mm/dp/B00ENL0YOE/ref=sr_1_3?s=industrial&ie=UTF8&qid=1489321200&sr=1-3&keywords=T5A+glass)

4) the mains fuse and the LPS fuse should not be the same value. The main protects the whole machine whereas this fuse protects the laser power supply. These supplies are rated at 3A and the 5A slow blow is a liberal choice of protection. My bet is still that the problem is internal to the supply but these K40's have surprised me before :).



![images/0f6d2cb87c57d1a126473c9b7ac646c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f6d2cb87c57d1a126473c9b7ac646c9.jpeg)


---
**Fernando Bueno** *March 12, 2017 13:24*

It makes sense what you tell me, **+Don Kleinschnitz**. The machine is new, but the seller is in China, me in Spain and when I bought the machine I was already aware of the problems I could have. Any solution I have to take it directly myself and without thinking about the warranty of the seller.



If the fuse is blown when the power supply is switched on, it is clear that the problem must be on the switches. Tomorrow I will buy two good quality switches and I will also buy a new fuse like the one you tell me about. The one I put is not of that type and it may be that there is the cause of it being burned as soon as the machine is turned on.



Thank you very much for your help.


---
**Jeff Johnson** *March 13, 2017 16:36*

Update: I've ordered a new Flyback Transformer from AliExpress. There are apparently several different styles and Ali was the only place selling the specific model in my power supply. It was only $25 shipped and now the waiting begins . . .

[aliexpress.com - 40-50W High Voltage Flyback Transformer for CO2 Laser Power Supply PSU MYJG-40 50](https://www.aliexpress.com/item/40-50W-High-Voltage-Flyback-Transformer-for-CO2-Laser-Power-Supply-PSU-MYJG-40-50/32798747070.html?spm=2114.13010608.0.0.PgrsnB)


---
**Don Kleinschnitz Jr.** *March 13, 2017 16:41*

**+Jeff Johnson** when you are done unless you want it don't throw it out. I would love to open up that flyback and see what is inside :).


---
**Jeff Johnson** *March 13, 2017 16:43*

I'd be happy to send it to you as long as shipping isn't too much. From what I've seen in searching for a replacement it's a lot of windings of very thin wire.




---
**Don Kleinschnitz Jr.** *March 13, 2017 16:47*

**+Jeff Johnson** i can likely pay the shipping. Were are you located?

I think it may have one or more of: coil, diode multiplier, capacitor, ballast resistor. 


---
**Jeff Johnson** *March 13, 2017 16:52*

**+Don Kleinschnitz** I'm in northwest Florida 


---
**Don Kleinschnitz Jr.** *March 13, 2017 16:55*

**+Jeff Johnson** that works, I'm in Utah. Let me know the size box and weight and I can send a label.


---
**Jeff Johnson** *March 31, 2017 13:26*

**+Don Kleinschnitz** I haven't forgotten about you, just insanely busy and not thinking about lasers for a bit while I wait for the replacement. Well, the replacement arrived and the laser is working again! 

The box is 7"x4.5"x3.5" and weighs between 1 and 1.5 pounds. I only have a large shipping scale . . . This is the same box the new one came in and it says .45kg on the customs form.


---
**Don Kleinschnitz Jr.** *March 31, 2017 13:50*

**+Jeff Johnson** 1-1.5 lbs is box and flyback? 

Need email I can send label to. 

Did the new one work?


---
**Jeff Johnson** *March 31, 2017 14:00*

Yes, that weight is including the flyback. My email is jwjexec@gmail.com. And the new flyback works perfectly.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/LqVp55MbtpE) &mdash; content and formatting may not be reliable*
