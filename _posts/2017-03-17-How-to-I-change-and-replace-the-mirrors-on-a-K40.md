---
layout: post
title: "How to I change and replace the mirrors on a K40?"
date: March 17, 2017 17:36
category: "Discussion"
author: "Bill Keeter"
---
How to I change and replace the mirrors on a K40? I've had some new ones ready to add but been putting it off. Last night I tried spinning the round metal holder counter clockwise but it didn't budge and I don't want to force anything.



Is there something really basic i'm missing?





**"Bill Keeter"**

---
---
**Ariel Yahni (UniKpty)** *March 17, 2017 17:47*

May the FoRcE be with you. Just a little


---
**Bill Keeter** *March 17, 2017 17:49*

**+Ariel Yahni** Okay, so I was doing it the correct way?


---
**Anthony Bolgar** *March 17, 2017 17:50*

Yes, it unscrews counter clockwise.


---
**Ariel Yahni (UniKpty)** *March 17, 2017 17:51*

Yes just a little force in my case


---
**Bill Keeter** *March 18, 2017 03:35*

Worked like a charm. thanks guys.



**+Ariel Yahni** one quick question. When measuring the focus distance of 50.8mm. Is it from the top of the focus lens or the bottom? My lens is from Saite Cutter. It's 2.85mm thick and it's curved upwards on both the top and bottom surfaces. 



I've been measuring from the bottom of the lens but know i'm wondering if i'm been wrong this whole time while cutting 3mm acrylic.


---
**Ariel Yahni (UniKpty)** *March 18, 2017 03:55*

From what I know is the bottom. Best you do a slanted test and will give you a better grasp of the margin you have. Anyway I believe **+Anthony Bolgar**​ is an expert on this


---
**Anthony Bolgar** *March 18, 2017 04:28*

I would definitely run a ramp test. That way you will get the most exact distance for the focasl length, and as a bonus you can measure the distance from the bottom of the head at the sweet spot so it is easier to set up work pieces in the future.


---
**Joe Alexander** *March 18, 2017 06:01*

I was under the impression that FL is measured from the mid of the focus lense, is this not correct? granted this is only a difference of a mil or 2...


---
**Anthony Bolgar** *March 18, 2017 06:21*

That is why the ramp test is the best way. You will find the best focal length this way, and do not need to worry where you are measuring from, just p;lace the head over the thinest part of the kerf that was made, and measure from the bottom of the head to the thin point of the kerf. Then when setting up material all you do is make sure that the top of the material to the bottom of the head is the distance you have just calculated using the ramp test.


---
**Don Kleinschnitz Jr.** *March 18, 2017 20:24*

I believe it is the middle but it really doesn't matter because as a few have said a ramp test is needed to account for all the sources of error. 

Another reason to do a ramp, is like in my case I got sent the wrong lens mine is 38.1 and it took me a bit to figure that out after measuring 50.8 over and over but not able to cut. 


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/YyBjLUupoTG) &mdash; content and formatting may not be reliable*
