---
layout: post
title: "Multi-layer acrylic animation No further info on the build"
date: August 01, 2017 12:51
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Multi-layer acrylic animation



No further info on the build




{% include youtubePlayer.html id="1vVH_Ewue9E" %}
[https://www.youtube.com/watch?v=1vVH_Ewue9E](https://www.youtube.com/watch?v=1vVH_Ewue9E)





**"HalfNormal"**

---
---
**Mitch Newstadt** *August 20, 2017 02:32*

neat... pretty easy to do.. but that one is nicely done...  looks like multi layers of acrylic edge lit  by leds and etched probably with a laser... the blue cross hairs is one layer and the different position x-wings are different layers ... have the blue on constant and the reds alternating on... fit it all in a nice frame and volia.. 




---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/CP6PY51owYN) &mdash; content and formatting may not be reliable*
