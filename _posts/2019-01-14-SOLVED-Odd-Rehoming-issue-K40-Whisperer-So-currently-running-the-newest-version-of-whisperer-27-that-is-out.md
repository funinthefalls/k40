---
layout: post
title: "SOLVED Odd Rehoming issue (K40 Whisperer): So, currently running the newest version of whisperer (.27?) that is out"
date: January 14, 2019 04:45
category: "Software"
author: "'Akai' Coit"
---
<b>SOLVED</b>



Odd Rehoming issue (K40 Whisperer):



So, currently running the newest version of whisperer (.27?) that is out. The issue I'm seeing is when it rehomes after a raster engraving. It hits the stop kinda hard, which throws it out of alignment. This is directly affecting raster engraving with multiple passes. What I've resorted to doing is manually reinitializing/rehoming after a single pass to start the following pass to avoid the misalignment. This problem affects vectoring and rastering directly after a raster engraving unless manual reinitialization/rehoming is done. How can I help troubleshoot this issue?





**"'Akai' Coit"**

---
---
**Scorch Works** *January 14, 2019 18:23*

I will try to reproduce the problem tonight.  How fast were you engraving?


---
**Scorch Works** *January 15, 2019 03:37*

I see what was happening.  I uploaded version 0.29 which should resolve the issue.  <b>Thanks 'Akai' Coit for the detailed description of what was happening and when it was happening.</b>  Someone else tried to report this issue yesterday but I couldn't tell when it was happening from the description they provided. (so I was checking the wrong thing.)


---
**'Akai' Coit** *January 20, 2019 07:45*

**+Scorch Works** Sorry for the late reply. I am not getting notifications of responses to my posts here, so I have to remember to actively check things. I will download and try the newest version and let you know if it fixes the problem for me. Thanks for working on this. Glad I can help find bugs (and describe them effectively enough for you to figure out what's going on). I'll keep going until everything is perfect! :D


---
**Scorch Works** *January 20, 2019 14:16*

**+'Akai' Coit** No problem. I had the same notification problem a while back.


---
**'Akai' Coit** *January 22, 2019 08:08*

Happy to say that this issue seems to have disappeared. Thank you for your work, good sir!


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/WtxPG6pqgm3) &mdash; content and formatting may not be reliable*
