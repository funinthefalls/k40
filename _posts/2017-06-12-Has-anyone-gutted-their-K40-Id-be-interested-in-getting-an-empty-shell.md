---
layout: post
title: "Has anyone gutted their K40? I'd be interested in getting an empty shell"
date: June 12, 2017 04:09
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Has anyone gutted their K40? I'd be interested in getting an empty shell. Well a motion system wouldn't hurt.  But it doesn't need to run, so no need of a tube, optics, or working psu. 









**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Joe Alexander** *June 12, 2017 05:48*

I've considered replacing my gantry to something nearly identical to what **+Ariel Yahni** did with openrail extrusion, but haven't quite committed yet as it still works atm. Once I work out the pulley system and mount my new laser head I'll be more committed. The main issue I have is that it still really limits the size of material I can work with vs. just building a new extrusion enclosure(potentially with a pass-through). What are you planning?


---
**Ray Kholodovsky (Cohesion3D)** *June 12, 2017 14:25*

I just want a blue k40 shell I can take to trade shows :) 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/AiTXwfQ3fwV) &mdash; content and formatting may not be reliable*
