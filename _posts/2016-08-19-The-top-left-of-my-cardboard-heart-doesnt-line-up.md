---
layout: post
title: "The top left of my cardboard heart doesn't line up"
date: August 19, 2016 13:43
category: "Discussion"
author: "Scott Pollmann"
---
The top left of my cardboard heart doesn't line up.  Does this mean j need to continue aligning?  Or is something else not correct?



Also, was told to post a pic of my mirrors.  Are these any good?







![images/1d414ab35d697813711befe71ee6315c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1d414ab35d697813711befe71ee6315c.jpeg)
![images/b5cc9544c692b753039d135a149ef58f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b5cc9544c692b753039d135a149ef58f.jpeg)
![images/b1b931b21db715833c98e348592eb300.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b1b931b21db715833c98e348592eb300.jpeg)

**"Scott Pollmann"**

---
---
**Scott Pollmann** *August 19, 2016 14:16*

Oh, I did get a new lens from LightObject, and new air assist head (need air pump yet).  Believe the lens is convex side up, and flat side down.  The difference in sides wasn't as dramatic with the new lens.


---
**Ariel Yahni (UniKpty)** *August 19, 2016 14:37*

Stock mirrors do work for most,  just keep them clean.  


---
**greg greene** *August 19, 2016 14:39*

What your seeing is a mechanical misalignment of the x y rails.


---
**Scott Pollmann** *August 19, 2016 14:41*

**+greg greene** that would effect my trying to get a good laser alignment too.  



How does one align the rails?  Do you have a tutorial/instructions you could link?


---
**greg greene** *August 19, 2016 14:47*

I don't, but I would do the following, 

1. make sure the end point sensors are fastened down securely.  These devices tell the head mechanism when it has reached the 'home' or start position.  When you turn the power on, the head should travel up to the left to this position every time;.  The stepper motors then respond to the software the number of 'steps' needed to get to where you want to engrave.  Make sure the belts are tight, if the belt slips one or more gear tooth because it is loose then the head will start out at the wrong spot - like you see in your graphic.  If all those are OK, then you will have to use a ruler and make sure the rails are parallel and the head can travel on them without encountering any rough spots that may cause the head to bind.  I found one on mine and I could not get good alignment till that was fixed.


---
**Bart Libert** *August 19, 2016 15:09*

your stepper motors loose steps. this is very bad. try to lower your speed and if possible increase stepper power if you can do that on the drivers. Misalignment could be the reason why they loose steps


---
**Scott Pollmann** *August 19, 2016 15:14*

**+Bart Libert** was cutting at 7mm/sec.  Still have yet to check connections 


---
**Bart Libert** *August 19, 2016 15:43*

steppers don't have feedback, so somewhere in the path the stepper looses steps because of friction in the system


---
**Ian C** *August 19, 2016 15:58*

Hey buddy. Having just spent hours levelling and aligning my own K40's bed, I can honestly say it takes time to do well. I found fitting a red laser diode to the top of the first mirror and a raised target to the second helped me get the Y axis (bed levelled in Y plane) spot on with no deviation up or down and left and right. I had the shim the rear left side of the bed up 4mm and the rear right 2mm. For the X axis, I did mount the same laser as a guide but measured the distance from the left guide rail to the side of the machine body, which seemed to work well. I have wathe numerous YouTube videos and between them all and my skills as an engineer have managed to figure out roughly what adjustments do what to the beam path, but there is more I can learn I'm sure. One of my belts felt slack on my Y axis so I tightened that to.


---
**Robi Akerley-McKee** *August 22, 2016 20:39*

**+greg greene** Looks more like the steppers loosing steps somewhere..  Cutting slower would probably help that.  I think there is only 1amp from the 24v side of things...  So the steppers don't get alot of power.


---
**greg greene** *August 22, 2016 21:26*

and if you have a marginal power supply to start with - you could be subject to less than the 1 amp


---
*Imported from [Google+](https://plus.google.com/103382513064902122956/posts/CdnNXAbvg3L) &mdash; content and formatting may not be reliable*
