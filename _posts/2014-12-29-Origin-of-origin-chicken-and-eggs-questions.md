---
layout: post
title: "Origin of origin & chicken and eggs questions"
date: December 29, 2014 13:59
category: "Software"
author: "Stephane Buisson"
---
Origin of origin & chicken and eggs questions.





We need to start somewhere, sometime. 

After looking around to make a big picture about how laser cuter users works and the softwares they use(not only K40), and collecting others info on their mods, It will be time to summarised and ask ourself what will be the best solution to upgrade the K40.



Chicken and eggs question, what to put first ? the choice of hardware board or the users experience meaning the software chain.



To help to answer that question, I asked myself what I don't like with my actual Moshi solution,  they try to do everything from drawing, managing the cutting/engraving option, communicating with the machine  not forgetting hardware board.

the result become an all or nothing solution, and due to the amount of bugs, more a nothing solution.



after a first run about the hardware, I went for the software. I found something very interesting, made by a student Thomas OSTER for is bachelor thesis, it's name Visicut, licences as free software, and Windows/Mac/Linux compatible. your work Thomas is in the spirit of this community, so I am delighted to underline it here.



You saved us a lot of time with all the questions you answered in your thesis <s>> </s>[http://hci.rwth-aachen.de/materials/publications/oster2011a.pdf](http://hci.rwth-aachen.de/materials/publications/oster2011a.pdf)



<s>I am reading it for now, and I will develop in other post. (could we match visicut with the best 2015 board around ?)</s>



<s>I quote Thomas:</s>



<s>"Abstract</s>

<s>Laser-cutters are central devices in personal fabrication and widely used in fab labs. Since those are open for everyone, an easy to use software solution is important. There are many vendor supplied solutions and some open source approaches, but none of them can provide full platform independence and good usability.</s>

<s>We created a tool named VisiCut, which allows using laser-cutters from nearly any operating system and even prepare the laser-jobs at home to minimize the time needed in the lab. This tool has some advantages over the existing solutions, which include positioning directly on a live camera picture of the material, detailed pre</s> view rendering and saving of complete jobs for easy distribution and portability. As base for VisiCut, we created a new library dedicated for laser-cutting, which is called LibLaserCut. This library provides easy interfaces for implementing laser- cutter drivers. The first driver, we implemented for the library, controls an Epilog ZING laser-cutter.

Both are written in pure Java, which make them platform independent. They are licensed as free software in order to be available for everyone and to allow contin- uous development and community support. For VisiCut, we analyzed existing interactive systems used in fabrication environ- ments and conducted a survey to to determine the habits of people using the laser- cutter in our fab lab. We also created a few UI prototypes and did user tests to improve them.

For the LibLaserCut, we analyzed the possibilities of the Epilog ZING laser-cutter among with available software solutions. We designed an interface for laser-cutter drivers and implemented the driver for the Epilog ZING, which is based on the open source driver CUPS-epilog.

Further, we evaluated VisiCut through a user test and tested the whole system on different platforms. We also created a list of improvements and enhancements, which should be addressed in future developement."





**"Stephane Buisson"**

---
---
**Thomas Oster** *January 01, 2015 22:12*

Hi.If you want to use VisiCut,right now the LAOS board [http://laoslaser.org](http://laoslaser.org) is your best choice. A smoothie board driver is in development,but it will take some time. 


---
**Stephane Buisson** *January 01, 2015 22:37*

Thank you so much Thomas for Joining our community.

your Visicut software is great, and we place great hope for a driver to work with the Smoothie board.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/ihZvgq8UYVW) &mdash; content and formatting may not be reliable*
