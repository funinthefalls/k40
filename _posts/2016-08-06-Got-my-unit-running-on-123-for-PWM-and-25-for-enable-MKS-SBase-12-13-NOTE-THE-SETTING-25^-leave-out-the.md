---
layout: post
title: "Got my unit running on 1.23 for PWM and 2.5 for enable (MKS SBase 1.2/1.3) NOTE THE SETTING: 2.5^ leave out the !"
date: August 06, 2016 01:30
category: "Smoothieboard Modification"
author: "Jon Bruno"
---
Got my unit running on 1.23 for PWM and 2.5 for enable

(MKS SBase 1.2/1.3)  NOTE THE SETTING: 2.5^   leave  out the ! for pin inversion. you do not want to invert the pin.

Brian W.H. Phillips was right about the LED taking it to 24V when idle.

I pulled D14 and everything is hunky doree now..

I would be concerned if running an SBase board that this could potentially damage the circuit in the LPSU. Looking at a spare power supply I have here it looks like the L pin on the PSU is the cathode pin of an opto isolator. (photo transistor, opto coupler...) the tiny little led inside could definitely be damaged if it is connected to the 24v rail and should it fail internally the everything on the 5v rail on the SBase board could be destroyed. Do Brians zeiner diode trick or pull the diode. either way you shouldn't run it without some mod or protection.



![images/00a1459cee795e5c71a5658766d089ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00a1459cee795e5c71a5658766d089ac.jpeg)
![images/137244bf3c02b132d9f0d5d77d932265.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/137244bf3c02b132d9f0d5d77d932265.jpeg)
![images/0962a7ad630105d730b3c75c6e50994d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0962a7ad630105d730b3c75c6e50994d.jpeg)

**"Jon Bruno"**

---
---
**Anthony Bolgar** *August 06, 2016 01:54*

Can you post your config file? I have an sbase bpoard I want to use in my Redsail LE400 laser. It is an older 40w with a larger bed than the K40, so I really want to get it going, the original controller is not made anymore and no one at redsail even new what board they used..it is that old. But solidly built, and I made the Z axis motorized (It had a hand wheel originally, so it was easy to just add a stepper.


---
**Jon Bruno** *August 06, 2016 01:59*

Absolutely. this is setup for the K40.. I'm still tinkering with the endstops but the settings should be correct. Also this is set at 1/32 microstepping for a quieter ride..



[https://drive.google.com/open?id=0B36nra1ArP-VR1dCVWIzMEZSZzQ](https://drive.google.com/open?id=0B36nra1ArP-VR1dCVWIzMEZSZzQ)


---
**Anthony Bolgar** *August 06, 2016 02:00*

Says I need permision to access.


---
**Jon Bruno** *August 06, 2016 02:02*

try now?


---
**Anthony Bolgar** *August 06, 2016 02:04*

I can fit a true 50w tube in it so once I have it up and running I will order a 50W and upgrade it, will be about twice as powerful as a K40, so I should be able to cut 3/4 plywood in 1 or 2 passes with air assist.


---
**Jon Bruno** *August 06, 2016 02:06*

Thats gonna be a treat!



Say, can you get the file now?


---
**Anthony Bolgar** *August 06, 2016 02:06*

Access denied, you should have an email from me that google sent requesting access


---
**Jon Bruno** *August 06, 2016 02:06*

I thought I made it public... ok let me do some research


---
**Jon Bruno** *August 06, 2016 02:07*

ok youve got it


---
**Anthony Bolgar** *August 06, 2016 02:07*

OK worked finally

Thanks.


---
**Jon Bruno** *August 06, 2016 02:08*

can you see the images linked to this post?


---
**Anthony Bolgar** *August 06, 2016 02:08*

Yup


---
**Anthony Bolgar** *August 06, 2016 02:09*

are you using a level shifter on the PWM pin?


---
**Jon Bruno** *August 06, 2016 02:10*

yes


---
**Jon Bruno** *August 06, 2016 02:11*

I would send you one but I think shipping is more than its worth..

I can put a stamp on it but there isn't any room for an address.


---
**Anthony Bolgar** *August 06, 2016 02:11*

Thanks for the info. Another question, did you leave the pot in as well to allow manually adjusting the  current?


---
**Jon Bruno** *August 06, 2016 02:13*

It's there but the wiper is disconnected and moved to the level shifter. once you go 2 wire the pot will bugger up everything. can't mix PWM and analog input..


---
**Anthony Bolgar** *August 06, 2016 02:17*

I could have sworn I saw someone whho had both the pot and the PWM input. Will have to do some searching in the communities.


---
**Jon Bruno** *August 06, 2016 02:17*

its all software controlled now.. which may or may not prove to be a pain in the ass for on the fly tuning.. but I think raster work will be a lot nicer.

I really won't know until I start burning stuff, but I figure  DSP Redsails and Epilogs don't have a pots so there has got to be something to this software definable power stuff


---
**Anthony Bolgar** *August 06, 2016 02:18*

Especially since smothieware is doing power /acceleration calcs.


---
**Alex Hodge** *August 07, 2016 08:10*

are you using + or - on p2.5?


---
**Jon Bruno** *August 07, 2016 13:55*

- pin. The + pin is tied to 24v+


---
**Alex Hodge** *August 07, 2016 18:09*

**+Jon Bruno** Right. The + pin is 24v. I get 0 on the - pin which is what we're using. So how is that dangerous and why do I need to remove the LED? I got my multimeter out and at no time, firing or idle do I see 24v on that - pin or the L pin I'm connecting to on the LPS. Also, the whole pin inversion thing is confusing to me. I currently have the pin inverted in my config. Laser firing and PWM both work great...


---
**Jon Bruno** *August 07, 2016 18:13*

I'm driving right now so I can't really give you much detail but check that pin and reference against chassis ground or simply touch the ethernet connector Shield and do a voltage readng between the shield and that PIN


---
**Jon Bruno** *August 07, 2016 18:16*

As for the inversion of the pin take a look at your controller right now if the unit is Idle that green light should be off if it is on the laser is enabled and that is not what you want


---
**Alex Hodge** *August 07, 2016 18:33*

**+Jon Bruno** Yeah. That's how I took the measurement. I have +24v on the + pin and 0v on the - pin. I'm using a Fluke multimeter, if I check all the other screw headers (heater1, heater2 and fan...I dont have the pin numbers in front of me), they're +24v on the + pin and +22v on the -.


---
**Alex Hodge** *August 07, 2016 18:35*

I'm active low though which means I want it off to fire, so I do want inversion. jesus I need to go to school for this crap.


---
**Anthony Bolgar** *August 07, 2016 18:45*

**+Jon Bruno**, **+Stephane Buisson**  was the one who had the PWM and POT together in their K40 Here is a link to his schematic: [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)


---
**Alex Hodge** *August 07, 2016 20:15*

Anthony, I'm not sure why you would want that pot in the mix though. It can be done, but then your PMW would be kinda worthless.


---
**Anthony Bolgar** *August 07, 2016 20:22*

For on the fly power adjustments if the setting you set in the gcode is not optimum


---
**Alex Hodge** *August 07, 2016 21:18*

**+Anthony Bolgar** Personally, I would start over. Adjust the gcode and run again.


---
**Jon Bruno** *August 08, 2016 01:11*

He's not doing pwm on his power setting in that schematic. 


---
**Jon Bruno** *August 08, 2016 01:21*

There is a grey area of info out there between the 1 and 2 wire setups.



As I understand it,



The 1 wire config uses pwm to modulate the actual Laser firing while maintaining the stock pot for max mA setting. In other words the duty cycle of the laser tube  is controlled by the pwm signal while the max output power is constant.



With the 2 wire setup there is an enable pin involved as well. 

The enable pin arms and disarms the laser firing but the actual power to the tube is modulated via PWM. 

In other words the laser is turned on and then the power is adjusted with pwm to desired levels during the burn.

At least that's how I understand it works between the two.



The diagram you linked is a 1 wire setup﻿


---
**Stephane Buisson** *August 08, 2016 20:26*

**+Alex Hodge** the pot act as ceiling (to set max mA , to  preserve the tube life) I barely use it at all, the cutting setup is software made. (well my K40 is on the road for one month, so no assistance at the moment)


---
**Anthony Bolgar** *August 08, 2016 20:52*

That is a very good reason to keep it. Thanks for clarifying that for us Stephane.


---
**David Cook** *August 09, 2016 20:22*

I set my max laser power to 0.8 in the smoothie config file  to make sure I don't overdrive my tube.



I am using a 2-wire setup  but my laser fire wire is tied to GND permanently.     My PWM wire actually turns the beam on or off as well as sets it's power level.



I do not have a POT in the circuit.



I DO  still have the interlock button on the panel  and I make sure to turn the button off before I open the cover, just in case.    but my SBASE / LaserWeb setup works perfectly and I have had a lot of success in multiple jobs ran with it so far.



However I did find that my SBASE  does not have a 5V output,  it's actually outputting 4.7V     so I may be having max power issues,  but since I limited max power to 0.8 I am not quite sure  what effect the 4.7V actually has.






---
**Jon Bruno** *August 30, 2016 18:28*

**+Alex Hodge** Just reviewing my posts.. I thought I had better answer you ..

The 24v potential is on the LED, that potential is therefore available at the lpsu Enable/Active/Fire! pin. This input pin typically connects to an opto-isolator or a simple transistor before going into a PWM IC or other triggering circutry. What we are doing by not removing D14 is leaving a 24V+ potential on the cathode of the Opto and while there is 5V+ on the anode if the diode or if a transistor the junction should break down there could be some substantial down stream damage to anything on the 5v rail


---
**Chuck Comito** *February 12, 2017 23:59*

I realize this is quite an old post however would any of you be willing to share your pinouts as well as your config file. I'm struggling with getting any PWM signal at the moment. I either have thing wire wrong (I don't think I do) or my board simply isn't working as it should. I've tried the PWM pin out from 1.23, 2.5 and scoped other pins without luck. At this point I don't care if I have the pot ceiling installed or not. Whatever way I can get to simply work is fine for me. I can always refine it later.



Thanks in advance if you can help.


---
**Jon Bruno** *February 13, 2017 14:51*

Hey Chuck..

Sadly you, much like myself took what we thought was the easy way out.

I bought an MKS board assuming it was a cheaper alternative to an actual smoothie board ( truth be told, they were out of stock).. I was wrong, at least when the expense of time is factored in.

I did have some success with PWM by using the two wire setup (one for Laser Enable(pin 2.5^) and one for PWM power level(pin 1.23)) and a level shifter board. the setup was more of a headache than I was willing to struggle with any longer.

Endstops weren't properly working, pwm functionality was flaky at best and firmware updates that people were pushing out did not properly apply to this board. I shitcanned the idea of using this for laser work.. It'll probably be great for a polargraph or some other cartesian device but in this configuration with this particular hardware it was a battle every step of the way.

Perhaps others will chime in with advice or solutions to your issue, I however have nothing but bad things to say about this board and I'm sorry that you are finding it difficult to configure.

Bottom line, It's a big fat steaming piece of garbage.

If you search on my posts I'm sure I shared my findings along the way, perhaps they will prove useful to you.

The config file I was running can be found at: [https://drive.google.com/file/d/0B36nra1ArP-VR1dCVWIzMEZSZzQ](https://drive.google.com/file/d/0B36nra1ArP-VR1dCVWIzMEZSZzQ)



best of luck.


---
**Chuck Comito** *February 13, 2017 14:59*

Unfortunately I have to say I agree with you. I bought it not knowing what I was getting. Honestly I didnt know what a smoothie was. So much lost time and frustration. Looks like I'm ordering a smoothie lol. Thank you for your input. It doesn't help that the k40 LPS has so many configurations out there either. No "one way" solution. Uggg. 


---
**Jon Bruno** *February 13, 2017 15:06*

My recommendation ... [http://cohesion3d.com/](http://cohesion3d.com/)

grab the mini laser controller.. he's a member of these groups and this controller is what we've all been waiting for. had it been available when I started out I could have saved so much time and money


---
**Chuck Comito** *February 14, 2017 02:33*

Well funny enough Jon, I got it working today... Took a lot of digging but so far so good. I haven't ran any tests yet as I have to align all the mechanics before starting a project. I found some other threads by you and that led me to where I'm at. I was able to track down the elusive PWM signal, and have it fire the laser. Unfortunately there is not a test fire button (that work) within Laserdraw3. I did find a macro I could use for alignment purposes though. I almost bought the cohesion3d months ago but didn't because I didn't have the patience to wait for his new batch to come in... Should of and still might. At least as of today I feel somewhat accomplished. I'll keep the group posted on the rest of my progress. Hopefully when I'm done I'll be able to supply a detailed project for others to follow.

Thanks for you help!


---
*Imported from [Google+](https://plus.google.com/105850439698187626520/posts/X3AgusNF1YR) &mdash; content and formatting may not be reliable*
