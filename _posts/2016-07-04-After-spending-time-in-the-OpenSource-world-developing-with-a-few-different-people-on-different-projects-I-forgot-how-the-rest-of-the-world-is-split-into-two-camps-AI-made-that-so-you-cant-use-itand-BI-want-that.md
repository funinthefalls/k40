---
layout: post
title: "After spending time in the OpenSource world developing with a few different people on different projects, I forgot how the rest of the world is split into two camps A...I made that so you can't use it....and B....I want that,"
date: July 04, 2016 15:29
category: "Discussion"
author: "Anthony Bolgar"
---
 After spending time in the OpenSource world developing with a few different people on different projects, I forgot how the rest of the world is split into two camps A...I made that so you can't use it....and B....I want that, and if you don't give it to me I'm going to steal it. Great thing about OpenSource, anyone can use anything that is licensed accordingly as long as they give attribution to the original Author. Not really learning anything from those Facebook groups, anyways.I learned so much more in a far shorter time on Google+, learning from the hardware and software engineers directly. So with that being said, I bid all on Facebook adieu. I am back where I belong!





**"Anthony Bolgar"**

---
---
**Scott Marshall** *July 04, 2016 18:15*

I agree completely, but have never been a Facebook fan. My only problem is the convoluted way Github and similar opensource  hubs are arranged.(at least it seems that way when you don't understand it yet)



It's a full time job learning the software to navigate them. I'm just getting the "lay of the land" on how they work, but am admittedly in over my head with Git and the "pin" site.



Maybe I'm spoiled by "conventional forums" and whatever this community is. They seem much more intuitive.



There's a wealth of info out there, and I might just contribute to it if I ever figure out HOW.



I managed to get Laser web into my machine, but it confirmed my fear of web-based software and the site went down in the middle of my 1st "test" Laserweb project.. I'm just not a fan of cloud based anything for important applications.



I've been meaning to thank you , I saw that it was you that contributed the "how to get Laserweb" step by step.



I would still be thrashing around if it wasn't for your well laid out step by step.



The only oops I had was the 1st time around I let Git install it's miniterminal (default)  instead of using the "operate from windows" option.



I let it set for a while, and after a hour of letting my head clear, it was obvious why it was telling me "Git isn't a recognized command". I reloaded Git using the windows option, and it went right through. You might want to put a note in your instructions, I would have but I don't dare try to edit a page in there with my limited experience.



Scott


---
**Anthony Bolgar** *July 04, 2016 18:21*

Thanks for the kind remarks **+Scott Marshall** Makes doing things seem easier when you get nice feedback like that.


---
**Scott Marshall** *July 05, 2016 02:00*

I always thought that a lot of people use these resources silently.



I'd guess maybe one in 100 give thanks to the author.



You are doing more good than you'd probably expect. I try to help people whenever I have something unique to contribute.



I do wish I had more time to learn some of the other platforms you have successfully  mastered. 



Since I started my "Kit" project, I've not had nearly as much time to spend here, and feel strangely guilty, about "neglecting the community".



Fortunately, there are a great many knowledgeable people here,  so people with issues always seems to have help at the ready.



I"ve been a fairly active member of several forums thru the Years, and have to agree with you, this group is a shining star among them. Good people here.



Scott


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/fcyqmStdF4q) &mdash; content and formatting may not be reliable*
