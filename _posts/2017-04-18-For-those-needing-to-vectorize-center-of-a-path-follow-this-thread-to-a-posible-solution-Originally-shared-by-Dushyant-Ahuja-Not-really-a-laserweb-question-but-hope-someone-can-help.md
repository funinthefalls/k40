---
layout: post
title: "For those needing to vectorize center of a path follow this thread to a posible solution Originally shared by Dushyant Ahuja Not really a laserweb question, but hope someone can help"
date: April 18, 2017 17:37
category: "Software"
author: "Ariel Yahni (UniKpty)"
---
For those needing to vectorize center of a path follow this thread to a posible solution 



<b>Originally shared by Dushyant Ahuja</b>



Not really a laserweb question, but hope someone can help. Mods - please delete if you think this is not appropriate.



I have been making pop-cards manually for a long time, however efforts at cutting them using my Silhouette have been dismal at best. All the tutorials I've found to convert line drawings create two lines for each of the line in the image. Was wondering if someone can advise on the best way to convert these images to vector with just one line. 



Would laserweb handle this correctly? If yes, it might make sense for me to get a few cutter blades and use my 3D printer instead.

![images/995475e06759337c2d0df2589a16c1df.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/995475e06759337c2d0df2589a16c1df.gif)



**"Ariel Yahni (UniKpty)"**

---
---
**Ashley M. Kirchner [Norym]** *April 19, 2017 05:51*

LW will simply follow a path and cut a single line. So if that image is already a vector file, you're set. Even if the paths have a stroke to them, if they are still a single line path, LW will cut a single line, regardless of the stroke.



Going further, you can draw that image in such a way that you control which lines it cuts and which lines it lightly scores so that folding is easier. All those dotted lines can be scored on a laser.


---
**Ashley M. Kirchner [Norym]** *April 19, 2017 05:52*

As for the idea of using your 3D printer to cut ... you'll need a head that swivels so it can run the blade along curves. You can't just keep the blade straight while attempting to cut a curve.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/WV5kcNHB6Fp) &mdash; content and formatting may not be reliable*
