---
layout: post
title: "Just went into my shop and tried to turn on my K 40"
date: March 26, 2017 21:56
category: "Hardware and Laser settings"
author: "bbowley2"
---
Just went into my shop and tried to turn on my K 40.  Nothing.  There is power to the switches but the machine is dark.  Is there a fuse in these machines?





**"bbowley2"**

---
---
**Mark Brown** *March 26, 2017 22:52*

There is a fuse in the very back, where the power cord goes in, accessible from outside the back of the machine I believe.



And another within the power supply.


---
**Bob Buechler** *March 27, 2017 00:59*

Sounds basic, but if your machine has an emergency stop button, also make sure it's not in the stop position. I've nudged it without noticing while working on upgrades etc and it certainly confused me for a sec. 


---
**bbowley2** *March 27, 2017 03:56*

I'm getting power to the power supply but the power stops there.  I see the fuse in the power supply on the board.  Any idea on the rating of the fuse.


---
**HP Persson** *March 27, 2017 06:35*

If the fuse on the power supply is gone, it went for a reason. Mostly it´s the bridge rectifier blowing up located a inch further in on the power supply board, check if it looks blown.



Also check the cables to the buttons, broken buttons and switches are pretty common on these machines.


---
**bbowley2** *March 27, 2017 17:03*

I can't see any burned or discoloration on the board.  The fuse looks blown and there is no continuity when checked with VOM.  Is there any wiring diagrams available?  A blown fuse on the board would indicate other problems.  I'm new to this and I might want to upgrade the PS and controller boars.  Where should I look for these?

![images/81bfd88d07a9d3afad999f1233da1a0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/81bfd88d07a9d3afad999f1233da1a0f.jpeg)


---
**Bob Buechler** *March 27, 2017 19:30*

**+Ray Kholodovsky** offers a drop-in controller replacement called **+Cohesion3D** (specifically, the mini bundle). I had great luck replacing mine, and Ray is very helpful. I don't know about the PS.


---
**Ray Kholodovsky (Cohesion3D)** *March 27, 2017 22:09*

Yep here's the link for the k40 bundle:  [cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



**+Don Kleinschnitz** knows a thing or two about these power supplies :) and you may want to check out his blog on the subject. 


---
**Don Kleinschnitz Jr.** *March 27, 2017 22:39*

**+bbowley2** schematics and such are avail on this blog. Look under LPS for info and lets continue from there. 



Clearly your fuse is blown (common failure) and likely due to a blown MOSFET, Rectifier bridge, power diode or flyback. 



WARNING: these supplies are dangerous and can kill you. I do not recommend repair unless you are a trained on troubleshooting high voltages.



[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Bob Buechler** *March 28, 2017 00:04*

**+Don Kleinschnitz** Given that these LPSs fail so commonly, do you have a recommendation for a drop-in replacement? Or is there no such thing? I tried digging through your blog for a link but none of the areas where you provide product links specify that you use them to drive the tube, only the other systems. Though I'm sure I just couldn't find the right spot in the docs.


---
**Don Kleinschnitz Jr.** *March 28, 2017 01:31*

**+Bob Buechler** there are links to supplies in the LPS sections:



[http://donsthings.blogspot.com/2017/01/k40-lps-configuration-and-wiring.html](http://donsthings.blogspot.com/2017/01/k40-lps-configuration-and-wiring.html)



..... but I can't really recommend any specific vendor. I suspect that light object is a good choice but frankly you can buy two of the standard types for the price of the LO one. 



So I would just buy an ebay unit that is the same as in your machine. They often go on sale and I have seen them for as low as $65.



I even priced out the cost of repair and with shipping your better off to get a new one. 



Once I find out why they fail we can figure out how to make them more reliable. Thats why I am asking for dead ones. 


---
**bbowley2** *April 04, 2017 23:57*

I found one for $80 on ebay.  I'll send you the bad one for your tests.


---
**Don Kleinschnitz Jr.** *April 05, 2017 00:52*

**+bbowley2** that would be awesome and helpful for tests, where are you located.


---
**bbowley2** *April 06, 2017 01:05*

Arizona


---
**Don Kleinschnitz Jr.** *April 06, 2017 13:45*

Ok I am in UTAH so not bad on shipping. Trying to find out how to get you electronic shipping label :). Never done that :(.


---
**Don Kleinschnitz Jr.** *April 06, 2017 14:28*

**+bbowley2** can you send your address and LPS weight and size to this email? don.kleinschintz@hotmail.com


---
**bbowley2** *April 06, 2017 20:11*

Shipping is not a problem.  Do you know of any LaserWeb for Dummies.  This software is incomprehensible to me.  I need a beginners guide.


---
**Don Kleinschnitz Jr.** *April 06, 2017 22:44*

Do you need my address? 

On laserweb what do you want to do first. We could do a video hangout?


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 22:47*

Do hangouts on air, it will be recorded.  It would help all the people!


---
**Don Kleinschnitz Jr.** *April 06, 2017 23:11*

**+Ray Kholodovsky** is hangouts on air something special?


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 23:14*

It's a public stream and it automatically gets recorded and posted to your youtube for future viewing.  Fundamentally it's still a hangouts video call. 



[support.google.com - Hangouts On Air with YouTube Live - YouTube Help](https://support.google.com/youtube/answer/7083786?hl=en)


---
**bbowley2** *April 08, 2017 11:35*

As soon as the new lps is here I would like the basics of LaserWeb.  The drawing and text tools.  I am using Ventric Cut2D and it works great for G code with my CNC.  I would like the K40 to follow paths rather than the back and forth like a dot matrix printer.  Don,  I'll let you know when to send me your address.  I have an account with UPS.


---
**Don Kleinschnitz Jr.** *April 08, 2017 12:37*

**+bbowley2** 



Long answer, while we are waiting for the supply:



Ok, I'm still learning my tool chain myself but willing to help get you up to speed as best I can. 

I looked at Ventric Cut2D as I do not use it and its a bit expensive for my taste but that should not be much of a problem.



Its best to decide what you want to do with your laser cutter and then pick the tool chain you want to use, in your case I will assume Ventric Cut2D to LW to smoothie. However there are multiple other tool chains that you can use and many contain inexpensive or free software.... your choice



Do you want to cut, engrave or both? Can Cut2D create everything you want to do?

.............

As an example, I have more or less settled on this tool chain :

..GIMP for: Sophisticated Image editing including grey scale conversion. These typically create raster(s).

..Inkscape for: LW pre-design and simpler cutting operations, cutting 2D parts and combining images and cutting into one operation. LW can handle inside outside cuts that compensate for laser kerf.

..Sketchup for: more complex CAD design I then export 2D faces to LW or the other tools above. 

..Sketchup and SketchUCam for more complex CAM design and export Gcode to LW.

...................

Here are some considerations: 

LW is not a design tool I see it as providing four categories of functions: 

1. Basic machine driver and interface to your C3D controller, including work-space positioning and machine controls.

2. Translator from various file types to Gcode, including images and CAD drawings.

3. Laser job programming, setting up order of operations etc...

4. Basic CAM operations such as inside and outside cutting.



In your case I am going to assume that yours will be:

... Ventric Cut2D for design and edit, exporting Gcode

... LW for machine control, work space positioning etc



The exceptions may be:

... CUT2D may not create (or allow you to create) perfect Gcode for a laser. Although Gcode is touted as a universal CNC language its not that simple. There are interpretive issues and machine dependent codes at play that may interrupt the work-flow and require Gcode editing or change of your design source tool. This will depend on how flexible CUT2D is to configure.



Every design tool is different and every CNC device has its own special love it needs to be shown, welcome to hobby CNC.



Attached is a video that I did using Inkscape to set up a LW hybrid job with images and cutting operations. It may help your initial understanding LW. 




{% include youtubePlayer.html id="PylIwah4jPs" %}
[youtube.com - Inkscape design for CAM operations in LaserWeb4](https://youtu.be/PylIwah4jPs)




---
**bbowley2** *April 22, 2017 12:54*

Don, I'll need your address so I can send you my old LPS.

Thanks


---
**Don Kleinschnitz Jr.** *April 22, 2017 13:46*

**+bbowley2** I sent it to you in a PM. 


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/5NU44XtLmpn) &mdash; content and formatting may not be reliable*
