---
layout: post
title: "I am newbie on this topic. I have just bought a laser cutter from a manufacturer in China"
date: March 31, 2015 14:49
category: "Discussion"
author: "Jose A. S"
---
I am newbie on this topic. I have just bought a laser cutter from a manufacturer in China. I have a 40w CO2 laser and apparently is type 4. My concern is about safety. Do we need glasses to protect our eyes at the time the machine is running?Thank you!..All the best!

![images/cd2558915ed5f3771d9b2f852b80c183.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cd2558915ed5f3771d9b2f852b80c183.jpeg)



**"Jose A. S"**

---
---
**Stephane Buisson** *April 01, 2015 06:29*

Welcome here ;-))



Moving light catch your attention, and you can't resist to look at it.

A machine with work in progress should not be left unattended (fire risk hazard), so you look at it.



Yes you definitely need goggle.



look ebay with "Safety Glasses Goggles for 10600nm CO2 laser"


---
**Bright World** *April 01, 2015 10:33*

Dear Stephane Buisson,

Gud evening!

I am satheesh from Tamil Nadu-India. We are one of the leading company in developing Electrical and Electronics Controlling Products, Lighting advertisement board,  Interior sticker and wall designing, Solar power plant installation etc.. Now we are interest in acrylic innovative products. Your machine will help us to develop a acrylic  products. so kindly send me a technical details, prize details with terms and condition to ksbrightworld@gmail.com.


---
**Jose A. S** *April 01, 2015 14:51*

Stephane, 



Thank you for your comment. I am really amazed how people is forgets the security while they are using these machines. 



Regards! and kisses! :P...you have save my asXX


---
**Apple Wang** *April 03, 2015 06:13*

Dear Jose,



As you know this machine you bought is a CNC machine, when you set down it, it can cut or engrave the materials by itself, so you needn't looking at it, then you won't be hurt. The only thing you need to take care is you can't put your hands or something between the path of laser, i mean between the  reflecting mirror, or you will get a burn.



Anyway, be careful is a good thing, just take attention when you working.



Best&RgdS

apple

EETO Machinery  Co., Ltd

Office 1401, Building B3, Wanda Plaza, Economic & Technology Development Zone, Wuhan, China

Tel：400-027-1897                                   Fax：027-84618501

Mobile: +86-15527861820(Whatsapp Available)

Web：[www.eetomachinery.com](http://www.eetomachinery.com)                

E-Mail：info@eetomachinery.com

Skype: lovelx0226


---
**Bee 3D Gifts** *April 28, 2015 02:32*

I get scared when i am aligning the mirrors and wear glasses..lol and I already sizzled my finger cause I am sooo used to 3D printers and forget there is a LASER going through mirrors and in my path! =)

Also..welcome!


---
**Tim Fawcett** *May 05, 2015 08:18*

Caution is always the best policy - it is a class 4 laser product (Invisible beam, danger from direct AND scattered radiation) so always be careful. Saying that, a thick acrylic screen especially one with good absorption in the infra-red region should be Ok (after all it cuts acrylic because it absorbs the beam. But better not to watch it cutting or if you do at an oblique angle to minimise risk.



Once you have confidence in the controller setup you do not need to watch every cut - just keep an eye out for fire!


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/EfMkzj9dMX1) &mdash; content and formatting may not be reliable*
