---
layout: post
title: "What temps are people running there water tems at,i live i Australia and the summer temps get to 45 deg cel ,i uograded to a refigerated under sink water cooler that ibturn off when it lowers the water temp to 15 deg and back"
date: March 30, 2016 08:12
category: "Discussion"
author: "Phillip Conroy"
---
What temps are people running there water tems at,i live i Australia and the summer temps get to 45 deg cel ,i uograded to a refigerated under sink water cooler that ibturn off when it lowers the water temp to 15 deg and back on when the water hits 20 deg .now that it is coming up to winter i will be using a 300mm computer cooling radator with twin 12 volt fans-all mounted outside .

What is everyboder else using?





**"Phillip Conroy"**

---
---
**Stephane Buisson** *March 30, 2016 08:23*

I know it's not always easy but try to find out the specs of your laser tube.

45C is far too much


---
**Anthony Bolgar** *March 30, 2016 08:32*

I think **+Phillip Conroy** meant the ambient air temperature reaches 45 deg cel not that the cooling water is at 45 deg cel. Phillip, am I correct in assumig that the laser is not in a climate controlled building ? 


---
**Phillip Conroy** *March 30, 2016 09:22*

It does have air con however i like to run between 15 to 20 and limit my laser cutting times to 3hours max than 1 hour rest however it means running a 300 watt water cooler and a second pump,


---
**Scott Thorne** *March 30, 2016 09:49*

I just use frozen bottles of water and put them in my distilled water tank as needed....I have a thermocouple and digital display installed and I never let it get up past 65 degrees f.


---
**Stephane Buisson** *March 30, 2016 09:52*

On this community we never really talk about energy and energy efficiency.

honestly a 40W tube should not output much BTU in 3 H, with a big water tank the raising temp is not that important. with 1 H rest the tank return to ambient T. the main things is the mass, location (on the floor) and his shape (thermal exchange with air). Honestly "wasting 300W on a chiller" is more than the K40 itself (including air exhaust+pump+air assist).

I do understand you could live in a hot country, but with aircond, you are back in the general case.


---
**Stephane Buisson** *March 30, 2016 10:03*

as per **+Scott Thorne** suggestion, your freezer is more energy efficient than a chiller


---
**Phillip Conroy** *March 30, 2016 10:13*

Yes but got sick of freezing and changing frozeb=n water bottles and found a chiller second hand for 20 dollars.i know the chiller uses more power than every thing else however only using durning half yeay --rest going to use water radator outside 


---
**Stephane Buisson** *March 30, 2016 10:39*

**+Phillip Conroy** your reason is as good as any other, go the way you feel best for you.


---
**Scott Thorne** *March 30, 2016 10:39*

**+Stephane Buisson**...lol...so true...that was my point...I'm not going to waste more power with a chiller when my freezer is already running...why not utilize it?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 11:46*

Personally I also live in Australia, however I use my laser cutter in the garage of my house, so it is out of the sun at all times, my water is about 30L of water, with no cooling. Although I have to admit, I tend to not want to do much work when it is 40 degrees +, so my laser hasn't really had to deal with that yet & I tend to not use the laser cutter for more than 1-2 hours at a time, usually with breaks in between as I am tweaking/adjusting files as I go.


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/iewjHb5QUpZ) &mdash; content and formatting may not be reliable*
