---
layout: post
title: "Great little web site with a bunch of free laser cut wood files"
date: November 29, 2016 09:21
category: "Repository and designs"
author: "Anthony Bolgar"
---
Great little web site with a bunch of free laser cut wood files. [http://cartonus.com/](http://cartonus.com/)





**"Anthony Bolgar"**

---
---
**Stephane Buisson** *November 29, 2016 11:30*

Nice, thank you **+Anthony Bolgar** ;

Post moved to repository & design section


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/fVCfztHzWJK) &mdash; content and formatting may not be reliable*
