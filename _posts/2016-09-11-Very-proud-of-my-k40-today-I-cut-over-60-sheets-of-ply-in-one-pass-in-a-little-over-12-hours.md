---
layout: post
title: "Very proud of my k40 today I cut over 60 sheets of ply in one pass in a little over 12 hours"
date: September 11, 2016 01:16
category: "Discussion"
author: "3D Laser"
---
Very proud of my k40 today I cut over 60 sheets of ply in one pass in a little over 12 hours 





**"3D Laser"**

---
---
**Ariel Yahni (UniKpty)** *September 11, 2016 01:48*

Growing business, very good


---
**Phillip Conroy** *September 11, 2016 06:21*

What did your water temps get to?.i read somewhere that laser tune needs time to recover (that is what i tell the wife ) so 3hours and 25deg is the most i will do without a 1 hour break


---
**greg greene** *September 11, 2016 13:27*

Wow - can you stack them that high under the head?


---
**3D Laser** *September 11, 2016 13:43*

**+Phillip Conroy** I use those gel ice packs that you  can put in your cooler and change them out every two hours are so.  It's a poor mans chiller but works great. I did have some natural breaks in there as I would throw a piece in and they take about 5-10 minutes to cut but wouldn't come back down stairs to but another one in for 20 minutes are so


---
**3D Laser** *September 11, 2016 13:43*

**+greg greene** nope.  just 1, 1/8 piece at a time 


---
**greg greene** *September 11, 2016 14:11*

I don't know what you were making, but that's a lot of work for a day !


---
**3D Laser** *September 11, 2016 22:56*

I make card trays for table top war games 


---
**greg greene** *September 11, 2016 23:03*

Love those games - :like Panzer Leader etc.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/gzFXqWnU5Ub) &mdash; content and formatting may not be reliable*
