---
layout: post
title: "Suddenly the Corellaser/LaserDRW toolbar is staying available"
date: November 13, 2017 17:06
category: "Software"
author: "HalfNormal"
---
Suddenly the Corellaser/LaserDRW toolbar is staying available.



I am not sure what has changed, but I am using Win10 and the toolbar for going from Corel to the laser settings is staying available. There is still an icon on the bottom toolbar for accessing settings but the elusive toolbar is staying put!





**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/cdNP9Aqc5k6) &mdash; content and formatting may not be reliable*
