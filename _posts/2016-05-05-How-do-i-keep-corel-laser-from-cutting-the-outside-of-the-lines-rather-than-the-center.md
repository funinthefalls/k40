---
layout: post
title: "How do i keep corel laser from cutting the outside of the lines rather than the center?"
date: May 05, 2016 23:24
category: "Discussion"
author: "Tony Sobczak"
---
How do i keep corel laser from cutting the outside of the lines rather than the center?





**"Tony Sobczak"**

---
---
**Anthony Bolgar** *May 05, 2016 23:41*

Set your line width to hairline. That should solve your issue. It also prevents double cuts on lines.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 01:32*

**+Tony Sobczak** **+Anthony Bolgar** I've had no luck with hairline. When I set to hairline it cuts the line twice. I have had luck using 0.01 for the line width, so it only cuts once.


---
**Tony Sobczak** *May 06, 2016 06:18*

Hairline looks like the lowest I can get in coreldraw.


---
**Jim Bennell (PizzaDeluxe)** *May 06, 2016 06:26*

Try setting the object to black fill and no line weight (none) and make sure the object is closed (no open nodes) .


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 08:29*

**+Tony Sobczak** You can manually click inside the box that says Hairline & type 0.01. That's what I do.



**+Jim Bennell** Black fill & no line width works only for closed objects. You can't black fill a path that isn't close (else you get it joining the path anyway where you don't want it to). Sometimes you want an unclosed path (e.g. if you just want to cut lines for living hinge) so you have to sometimes work with the 0.01 line width to get around that (unless maybe you make a rectangle that is 0.01mm wide). But mostly, black fill was the way I went about it to begin with, but since finding out about the 0.01 line width, I've been using that. Although, black fill makes it easier to see if the paths are closed as you mentioned, and also easier to see what you want to click on. I've yet to test, but I'm curious whether we can use colour-filled paths (e.g. red, blue, etc) & send them to CorelLASER plugin & if it just converts them to black. Would be useful for viewing all layers stacked on top of each other.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/cymgf7XSyqU) &mdash; content and formatting may not be reliable*
