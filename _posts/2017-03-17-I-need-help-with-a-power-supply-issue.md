---
layout: post
title: "I need help with a power supply issue"
date: March 17, 2017 02:03
category: "Original software and hardware issues"
author: "timb12957"
---
I need help with a power supply issue. I had a lapse of intelligence and plugged a blower fan in to one of the rear 110v ports of my machine. When I flipped the switch to power up my laser, the blower fan came on and apparently pulled more amps than the outlet was able to deliver. My laser went dead.  Now the  power led on the PSU does not lite. I removed the PSU cover, pulled the only fuse I found and checked it. It checked fine. I discovered one of the wires that was supposed to be soldered to the back of the three post socket of the main power wire was only twisted on to the lug. I soldered this and thought maybe  I had found the problem. However I still get nothing when I switch on my laser. I have done a visual inspection of the PSU and do not see anything that looks burned or blown. Any ideas?  Since I assume PSU's may vary, I have included a pic of mine.

![images/af0b2286b6c39682b52e76cc89c4822a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/af0b2286b6c39682b52e76cc89c4822a.jpeg)



**"timb12957"**

---
---
**Don Kleinschnitz Jr.** *March 17, 2017 02:53*

Is this an old picture. The power led in this picture is on? 

Can you measure the AC voltage input to the supply?



Also have you checked the input plug, usually to there is a fuse in it.


---
**Joe Alexander** *March 17, 2017 03:01*

I second what Don said as the main input fuse on the back may have blown. It is either a round stand-alone or a modular plug with a pull-out tab. This fuse blowing would prevent anything from lighting up, even the on switch.


---
**timb12957** *March 17, 2017 03:33*

Don it is a current picture of the PSU. The led is just reflecting the camera flash. As for the input plug, I had never seen one like this before. It did have a fuse and it was blown!! Thanks to the awesome people in this community for all the help!!!!!

![images/c1494fddbfa16fa028fefcd130629550.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1494fddbfa16fa028fefcd130629550.jpeg)


---
**Don Kleinschnitz Jr.** *March 17, 2017 03:54*

Gosh its great to get one right on the first try.... for once :)



If I was you, considering the state of wiring you described, I would check every connection and every ground for a good connection and continuity.

 


---
**Alex Krause** *March 17, 2017 03:57*

Replace that outlet as well... Looks like a short to ground and it damaged the receptical 


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/huZzg6UwRwr) &mdash; content and formatting may not be reliable*
