---
layout: post
title: "Hi all, I've been reading this page for a while now and I finally took the plunge and purchased one of these K40's"
date: April 15, 2015 23:40
category: "Discussion"
author: "David Wakely"
---
Hi all,



I've been reading this page for a while now and I finally took the plunge and purchased one of these K40's. I've got to say I'm quite impressed, everything worked perfectly out of the box - even mirror alignment suprisingly.



My main question is what are your recommendations on cut speeds and power for different materials, does anyone have a list? I mainly plan on cutting 3mm ply, 3mm acrylic and 5-6mm acrylic. I have successfully cut these materials but I want to know if my power and speeds are ok as I don't want to blow my laser tube!



Thanks in anticipation!





**"David Wakely"**

---


---
*Imported from [Google+](https://plus.google.com/106624957596821487265/posts/Qivh3r8JpkF) &mdash; content and formatting may not be reliable*
