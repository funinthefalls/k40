---
layout: post
title: "First modification completed :) This was a nice easy one"
date: October 19, 2015 03:50
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
First modification completed :) This was a nice easy one. Not so cheap, however reasonable price in my opinion. Galvanised steel perforated board (330x350mm, with ~5mm holes) for the cutting table replacement (ripped out the stupid clamp thing). I opted for steel as it is magnetic & if required I can use it to hold pieces in place with magnets.



Cost AU$38.50, cut to size from a local metal supplier.



Basically install just required me to undo screws for the exhaust vent, then drop the board in, replace screws in the exhaust vent.



Measured the distance from the board to the laser head & it is sitting at ~55mm. Gives me about 5mm thickness to work with (which is suitable to begin with as most of my plans for this machine are leather of <5mm thickness). The perforated board is sitting on top of the 4 bolts that hold the original cutting table in place.

![images/185c4d49802b93dfb7ff47590ae003e3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/185c4d49802b93dfb7ff47590ae003e3.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ashley M. Kirchner [Norym]** *October 19, 2015 06:55*

Nice. I opted for simple aluminum honeycomb. For $37 I got a piece that's 24"x48" which I just cut down into the size that I need. That gave me several extra pieces for later. 


---
**Mike Hull** *October 19, 2015 09:11*

I have heard that the clamp is designed for holding rubber stamps. Cutting rubber stamps is one of the main uses these machines were designed for in China.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 19, 2015 10:55*

**+Mike Hull** That's an interesting thought & I agree with **+Allready Gone**: wonder why they would need to clamp it in place... Seems strange. I would just lay it flat on the bed. Unless the heat from cutting is prone to warping the rubber maybe & thus clamping it holds its shape better?


---
**Mike Hull** *October 19, 2015 11:11*

If you are doing repeated operations it makes sense to be able to quickly centralise your working material in the same place each time. In China rubber stamps are still in every day use - business record keeping is done differently from in the West and by a regimented process.


---
**Kirk Yarina** *October 20, 2015 00:46*

**+Allready Gone** Stamping is a hobby thing in the states.  One of my neighbors is a serious stamper.  As far as I can tell it's tied in with scrapbooking and homemade greeting cards.  Not my cup of tea, but she's serious about it and belongs to a club of like minded individuals.  The group is larger than I expected.


---
**Ashley M. Kirchner [Norym]** *October 20, 2015 02:12*

So you can now create custom stamps for them.  :)


---
**Phil Willis** *October 20, 2015 17:41*

After removing the clamp from my machine I think it might be ideal for holding PCB's while soldering components.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 20, 2015 20:39*

**+Phil Willis** That's a good idea Phil. May as well put it to some good use :)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/cgSqGcMGAvM) &mdash; content and formatting may not be reliable*
