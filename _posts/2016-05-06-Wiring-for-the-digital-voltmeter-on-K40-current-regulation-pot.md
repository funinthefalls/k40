---
layout: post
title: "Wiring for the digital voltmeter on K40 current regulation pot"
date: May 06, 2016 12:30
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Wiring for the digital voltmeter on K40 current regulation pot.

![images/4bba7bf38638965638fef5bab3c2128f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4bba7bf38638965638fef5bab3c2128f.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Jean-Baptiste Passant** *May 06, 2016 14:38*

Be careful which Voltmeter you get ;)

**+Don Kleinschnitz** Voltmeter use 3 wire : 

Red is Vin, Bleck is GND and White is Sensor.

Some voltmeter use 2 wire, VIN and Sensor being together.

The two wire versions are not good for one reason : there is a min voltage for it to work.



Let's say the min voltage is 2.5V, then the Voltmeter will stay off until you set the pot to 2.5V which is 50% of the Laser power.



Using a 3 pin Voltmeter allow you to read the value even if it is 0.01V


---
**Don Kleinschnitz Jr.** *May 06, 2016 19:41*

Yup sorry I should have mentioned to use the right Voltmeter.


---
**Purple Orange** *May 27, 2016 07:49*

Is it possible to use this mini Amp meter in line with the current reg pot to get a digital reading of what mA the "knob is delivering at the current setting? If so, what would the wiring look like?



[http://www.aliexpress.com/item/green-LED-0-100A-DC-Digital-Panel-Ammeter-AMP-100A-Ampere-Meter/1066162194.html?ws_ab_test=searchweb201556_10,searchweb201602_3_10037_507_10032_10020_9912_10017_10021_10022_10009_10008_10018_101_10019,searchweb201603_9&btsid=ea4fe388-3367-45cd-8512-79529704dda7](http://www.aliexpress.com/item/green-LED-0-100A-DC-Digital-Panel-Ammeter-AMP-100A-Ampere-Meter/1066162194.html?ws_ab_test=searchweb201556_10,searchweb201602_3_10037_507_10032_10020_9912_10017_10021_10022_10009_10008_10018_101_10019,searchweb201603_9&btsid=ea4fe388-3367-45cd-8512-79529704dda7)


---
**Jorge Robles** *February 17, 2017 10:30*

**+Don Kleinschnitz**I 've got this one. [es.aliexpress.com - 1 Unids Superior Calidad de la CC 100 V 10A Del Amperímetro Del Voltímetro Azul + Rojo LED Dual Amp Digital Volt Gauge Meter](https://es.aliexpress.com/item/M65-Free-Shipping-DC-100V-10A-Voltmeter-Ammeter-Blue-Red-LED-Amp-Dual-Digital-Volt-Meter/32668484474.html)



Could I use your setup to read the pot and the Current?


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/7tKY3Yg6yD6) &mdash; content and formatting may not be reliable*
