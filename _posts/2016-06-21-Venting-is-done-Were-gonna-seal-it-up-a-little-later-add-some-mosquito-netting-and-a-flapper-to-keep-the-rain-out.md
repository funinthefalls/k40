---
layout: post
title: "Venting is done. We're gonna seal it up a little later, add some mosquito netting, and a flapper to keep the rain out"
date: June 21, 2016 19:47
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Venting is done. We're gonna seal it up a little later, add some mosquito netting, and a flapper to keep the rain out. But, the fan is blowing now and I'm ready to try out my new smoothie powered laser. 



![images/511beecb194a27cb72a8a09232130eeb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/511beecb194a27cb72a8a09232130eeb.jpeg)
![images/74517ae841494f77734dadf174f77111.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74517ae841494f77734dadf174f77111.jpeg)
![images/6aa2cd2938622ce68cd7bb890e8a7156.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6aa2cd2938622ce68cd7bb890e8a7156.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 19:48*

**+Ben Marshall** 


---
**Ben Marshall** *June 21, 2016 20:20*

Looks great! 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 21, 2016 20:38*

You could put a right angle that focuses it downwards to keep rain out. Looks great though.


---
**Ben Marshall** *June 21, 2016 20:40*

I can send you a pick of what my exhaust looks like (with screen) if you need some ideas 


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 20:52*

Sure. Start a hangout chat. 


---
**Evan Fosmark** *June 21, 2016 20:52*

Looks really nice! May want to put a grate on the exit in order to keep starlings from trying to build a nest in it.


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 20:54*

Yep, that's what I meant by flapper. 


---
**Evan Fosmark** *June 21, 2016 20:55*

What software are you using with the Smoothieboard?


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 20:59*

As of now, LaserWeb2 to make gcode (working on this right now) and Repetier Host (attempting shortly) to send it. At least it was useful for those initial movement and burn tests in my other post.  I also have Vectric Aspire I use for my CNC. Maybe will get LW3 going later. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/XimpkihpYnP) &mdash; content and formatting may not be reliable*
