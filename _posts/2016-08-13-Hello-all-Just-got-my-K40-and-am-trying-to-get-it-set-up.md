---
layout: post
title: "Hello all. Just got my K40 and am trying to get it set up"
date: August 13, 2016 23:17
category: "Software"
author: "Derrick Armfield"
---
Hello all.   Just got my K40 and am trying to get it set up.   Couldn't get the software to install from the disk.   Did a search here and found the Dropbox files from another user but that didn't work either.   



Frustrated I downloaded the Free Trial of CorelDraw and now I have a working machine.   This is only a 7 day fix.   I sent a message to the seller on eBay for help.  Anybody have any other helpful tips?





**"Derrick Armfield"**

---
---
**greg greene** *August 13, 2016 23:29*

Is laser draw the problem - or Corel? I also has problems downloading from the dropbox - took 3 times before it was successful.   Make sure your model number and serial number are correctly entered - it won't work without it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 13, 2016 23:53*

if that's my dropbox that you are referring to then i'm not sure what is wrong with it. although i decided to stop sharing the software here due to piracy concerns for the community as a whole.


---
**greg greene** *August 14, 2016 00:17*

No, I got it from the sellers drop box


---
**Derrick Armfield** *August 14, 2016 00:23*

CorelDraw is my problem.   The other files download just fine.   It gives me an error "Failed to install ISKernel Files.  Make sure you have appropriate priviliges on this machine".


---
**greg greene** *August 14, 2016 00:24*

run install as administrator


---
**Derrick Armfield** *August 14, 2016 00:24*

I did find your dropbox (Yuusuf) and when I downloaded it my virus scan went off and quarantined the file.


---
**greg greene** *August 14, 2016 00:25*

happens sometimes - but the file is ok


---
**greg greene** *August 14, 2016 01:27*

Virus scan picks up on the keygen file

I had the same error on Corel - go to the the setup.exe pgm and right click then select run as administrator - it should install correctly then - mine did.


---
**Derrick Armfield** *August 14, 2016 01:40*

Will give that a shot and report back.  Thanks!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 14, 2016 02:12*

Yeah, it's the keygen that is the problem. Also, the version I had in my dropbox is no good anymore. It constantly comes up asking for a valid serial number. It's probably worth buying the Corel Draw if you are planning on keeping the original controller/software. I am going the upgraded controller way in the near future.


---
**Derrick Armfield** *August 14, 2016 02:29*

Please forgive my computer ignorance.   I'm assuming that I have to UnZip the file(s) etc. and am trying to do that right now.   



I did the "Right Click, Run as Admin" and it comes up with an error of "Cannot Find Languages......"


---
**Derrick Armfield** *August 14, 2016 02:34*

The only laptop of mine that has a CD drive is an old Vista.   What I just did was to UnZip the files from the disk and am now copying the UnZipped files onto a Flash drive so that I can put it on my newer computer.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 14, 2016 02:39*

**+Derrick Armfield** Hopefully that does the job for you :)


---
**Derrick Armfield** *August 14, 2016 12:54*

Finally got it.   I'm not a high tech redneck.   



Followed the Dropbox from the other user and this time it finally worked.  I think most of the issue was that I wasn't right clicking and unzipping the folder.   Doh!


---
*Imported from [Google+](https://plus.google.com/116858736662206054147/posts/QDX5nLEsufR) &mdash; content and formatting may not be reliable*
