---
layout: post
title: "I just installed a 38mm lens and the difference in performance is quite drastic"
date: September 27, 2016 18:44
category: "Modification"
author: "Jeff Johnson"
---
I just installed a 38mm lens and the difference in performance is quite drastic. With the stock lens I've been cutting 3mm MDF and ply at 18ma and 9mm/s. Now I'm cutting at 12ma and12mm/s. The edges are no longer burned and the plywood cuts much more consistently, cutting through knots and glue better than before.  The focus was adjusted with both lenses so I'm attributing this increased performance to the smaller beam cross section. The hole it makes in 3mm wood is barely larger than a human hair.





**"Jeff Johnson"**

---
---
**Ariel Yahni (UniKpty)** *September 27, 2016 19:03*

Are you getting same speed on mdf and ply?  Because with stock lense you should get 3mm ply cut at 10mms @ 10mA


---
**Jeff Johnson** *September 27, 2016 19:05*

I haven't tried changing the speed between them.  I'll do that this evening. It's possible that the stock lens wasn't good quality and I'm just now seeing the results of a decent lens.


---
**Jeff Johnson** *September 27, 2016 19:27*

I also discovered that with the stock lens I saw no performance increase past 18ma. I'll test with the new lens.


---
**Ariel Yahni (UniKpty)** *September 27, 2016 19:27*

Still you have a very good performance increase 


---
**Jeff Johnson** *September 27, 2016 19:36*

Yes I do and I am very pleased.


---
**greg greene** *September 27, 2016 20:42*

And as long as your happy - all is good


---
**Phillip Conroy** *September 27, 2016 21:45*

what size lens is it?


---
**Phillip Conroy** *September 28, 2016 08:39*

Acording to this link a 38mm focal length lens had a depth of feild [where beam is narrowest and will cut the best] of .8mm and a stock k40 50mm focal length lens is 1.4m m so a 50mm lens should cut thicker stuff

[parallax-tech.com - Frequently Asked Questions about CO2 laser lenses for
      cutting](http://www.parallax-tech.com/faq.htm)


---
**Phillip Conroy** *September 28, 2016 08:41*

To be a fair test both lens should be new as they wear without showing anything by looking at them


---
**Jeff Johnson** *September 28, 2016 11:46*

The 38mm lens produced a smaller diameter beam, giving a greater power density. A couple videos testing various lenses showed the 38mm lens cutting significantly deeper. I'm not an expert in this and it seems counter intuitive but I can see a smaller hole in and out of 3mm weed and MDF than I was getting with the 50mm. I'll find those videos when I'm at the office and link them here. The results were the opposite of what I was expecting and reading but I'm seeing them myself.﻿


---
**Jeff Johnson** *September 28, 2016 14:59*

Phillip, here is one of the videos showing some tests.


{% include youtubePlayer.html id="61bGzWicRRk" %}
[youtube.com - RDWorks Learning Lab 62 Energy density](https://www.youtube.com/watch?v=61bGzWicRRk)


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/eAKssNuinHZ) &mdash; content and formatting may not be reliable*
