---
layout: post
title: "A couple of quick jobs to test out the machine after upgrading to my smoothie based board from Cohesion 3D and Ray Kholodovsky The first was a small quick rastor to test the grayscale engraving"
date: September 08, 2016 21:49
category: "Object produced with laser"
author: "Carl Fisher"
---
A couple of quick jobs to test out the machine after upgrading to my smoothie based board from Cohesion 3D and **+Ray Kholodovsky**



The first was a small quick rastor to test the grayscale engraving. Still some tweaks to make but it was my first stab at it.  Dark 20mm/sec, Light 60mm/sec,  20%min/80%max



The second was a vector trace done in LaserWeb to test the vector capabilities. Ran that one at 20% power and 66mm/sec 



![images/1c52aab585a83a56db150e88fc404ad4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c52aab585a83a56db150e88fc404ad4.jpeg)
![images/b788ada6b2847ee90c540fafd8013c90.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b788ada6b2847ee90c540fafd8013c90.jpeg)

**"Carl Fisher"**

---
---
**Stephen Sedgwick** *September 08, 2016 22:01*

love them.... picture after my own heart... :-)


---
**greg greene** *September 08, 2016 22:22*

Guess I'm going to go Smoothie !!!


---
**Ariel Yahni (UniKpty)** *September 08, 2016 22:23*

Did you like the vector capabilities from LW


---
**Jim Hatch** *September 08, 2016 22:33*

Wow! that is so cool. Will save me a ton of time mucking about with image processing trying to get just the right dithering patterns.


---
**Carl Fisher** *September 08, 2016 22:36*

**+Ariel Yahni** so far yes. My only real complaint is that it was a long trace job and there was no indicator that it was working. It just sort of checked out for a while and at one point I even got a wait/kill popup. I just let it go and it did finally finish up. It did a good job on the trace but the original was line art already. I still have to see how well it does on a more detailed picture.




---
**Ariel Yahni (UniKpty)** *September 08, 2016 23:09*

**+Carl Fisher**​ please share the original image I want to try and replicate. Vectorizing will not work for every occasion but it's a real time saver


---
**Todd Miller** *September 08, 2016 23:40*

Looks Awesome !




---
**Alex Krause** *September 09, 2016 01:41*

**+Jim Hatch**​ here is one of my grayacales with no dither

![images/b2478926f5a5d891aa3a0e8e6718283e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2478926f5a5d891aa3a0e8e6718283e.jpeg)


---
**Jim Hatch** *September 09, 2016 01:44*

**+Alex Krause**​ really nice.


---
**Carl Fisher** *September 09, 2016 01:45*

500 mm/sec? I think my machine would jump out of the case at that speed. I'll have to see just how fast I can go without losing steps.


---
**Alex Krause** *September 10, 2016 03:22*

stock the k40 is rated up to 500mm/s running the corel laser plugin... that is for raster i wouldn't suggest cutting at that speed... it very well may jump out of its case lol


---
**Carl Fisher** *September 10, 2016 17:48*

**+Alex Krause** So I did some playing around today. I was actually doing some raster stuff at 600/light 400/dark and it never missed a step. 



For reference I also tried to vector at 400 and it was missing steps pretty regularly (may be belt tension, not sure yet) but also tweaking the gantry pretty good while jumping around so I can see that being very hard on the machine trying to vector very fast.


---
**Adam J** *September 17, 2016 23:21*

Does the smoothieboard engrave faster than the stock board? 


---
**Carl Fisher** *September 19, 2016 01:14*

The machine is what will limit the speeds. either board can push the machine past its capabilities so I'm not sure if there is an acceptable answer to that question. I can comfortably engrave at speeds up to 500mm/sec as mentioned above and have been able to push almost 600mm/sec for short amounts but you risk missing steps at that point regardless of your board.




---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/73R2JYPGe5w) &mdash; content and formatting may not be reliable*
