---
layout: post
title: "Hello all I am new to laser engraving and still figuring settings out"
date: May 31, 2017 15:01
category: "Modification"
author: "Richard Gallatin"
---
Hello all I am new to laser engraving and still figuring settings out. I was wondering what is a good temp ranger for my water to be that is safe and will not harm laser tube. I am looking for a chiller but not really wanting to spend the 300+ dollars. Right now I am getting up to 23 celius and adding some ice to my bucket. I am upgrading to the Cohesion 3d Mini. For now I am doing it outside so the exhaust has not been an issue so far. 





**"Richard Gallatin"**

---
---
**Ned Hill** *May 31, 2017 15:16*

Richard, you will find varying opinions on the subject but my  take is that you will want to keep it between 18-22C.  Watch the low end as condensation on the tube maybe come an issue for you especially outside.  I just keep some frozen 1/2 liter water bottles in the freezer to swap out as needed in my water reservoir. 


---
**Anthony Bolgar** *May 31, 2017 15:33*

I agree with Ned, any where between 18 and 22 is fine, but I try to keep it under 20.




---
**Steve Clark** *May 31, 2017 16:13*

Here is a chart that is helpful (page 2) with regards to avoiding condensation. 

[synrad.com - www.synrad.com/Manuals/Technical%20Bulletins/TB12_Prevent%20Condensation.pdf](http://www.synrad.com/Manuals/Technical%20Bulletins/TB12_Prevent%20Condensation.pdf)

You might also consider buying one of those 20 dollar meters for measuring humidty.


---
*Imported from [Google+](https://plus.google.com/102728020130316654078/posts/1HuUzEkbMAT) &mdash; content and formatting may not be reliable*
