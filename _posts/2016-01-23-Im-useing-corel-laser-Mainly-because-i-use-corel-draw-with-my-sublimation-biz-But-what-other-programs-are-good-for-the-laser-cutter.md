---
layout: post
title: "Im useing corel laser Mainly because i use corel draw with my sublimation biz But what other programs are good for the laser cutter"
date: January 23, 2016 10:51
category: "Discussion"
author: "beny fits"
---
Im useing corel laser 

Mainly because i use corel draw with my sublimation biz 

But what other programs are good for the laser cutter 





**"beny fits"**

---
---
**Jim Hatch** *January 23, 2016 15:16*

I used to use Inkscape (it's an open source program so it's free). It's a good app for someone who doesn't already have a good alternative they're comfortable with. I also tried Rhino and Fusion360 which I use for 3D printing work (I have a Makerbot). Both are really good but overkill for 2D laser cutting/engraving. I've used Corel Draw in the past but never was an "expert". Picked it up again when I got the K40. Right now though I'm back to Adobe Illustrator. AI CS2 is free from Adobe and like Corel can do most anything I need for the laser. Either is fine - I settled in CS2 because my Makerspace's laser software can read AI files without needing to go through a DXF or BMP translation step and because my son is a designer who uses the latest version of AI and can help when I get stuck :-D



Bottom line though, I don't think you can go wrong with any of them for what our machines can do and what we have to go through with getting files to the machine (I don't use the Corel plugin because mine came with a virus so I have to go through LaserDRW). If you're already comfortable with Corel I don't think you'd gain anything switching. A newbie might be better off with AI or Inkscape (the latter is pretty easy to learn).



I've added Photograv to my suite though for doing photograph prep. It simplifies the often fiddly trial & error process of getting the right settings of contrast, dithering, etc.



All of this assumes stock hardware. I haven't done my upgrade to a smoothie or ramps board and tried using Peter's software yet.


---
*Imported from [Google+](https://plus.google.com/116814442468846937788/posts/PqrVZT3FcMf) &mdash; content and formatting may not be reliable*
