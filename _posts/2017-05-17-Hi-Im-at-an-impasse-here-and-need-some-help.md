---
layout: post
title: "Hi, Im at an impasse here and need some help"
date: May 17, 2017 04:49
category: "Hardware and Laser settings"
author: "Steve Clark"
---
Hi, I’m at an impasse here and need some help. 



Details, K40 – C3D – win10 –LW4



Talked to Ray and I’m wired correctly.





Just installed the C3D kit everything is good things all seem to work like they suppose to… well sort of…I got this one little problem.



I’m not understanding or having trouble with this relationship between the old ‘Current Knob’ and the LW Control Laser Test button. If I turn the knob to about 8 ma the LW disconnects…and I get this message:



"Server error: Writing to COM port (GetOverlappedResult): File not found Machine disconnected Machine disconnected No serial ports found on server!"



In LW ‘settings’ these are the values:





PWM MAX S VALUE 1 

CHECK-SIZE POWER 0 % (Not sure what this means)

TOOL TEST POWER 20 % 

TOOL TEST DURATION 5 MS (I’m not sure what this means… options seem to be 1-9… only that can’t be milliseconds )



If I fire at about 3 ma it works and anything above that will or may crap out… 8 and above for sure it will. If it is at zero then there is no firing.







**"Steve Clark"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 05:27*

I did tell him that "if you literally turn the pot up and LW disconnects" then you've got some other issue (possibly psu) that you need to investigate - may want to seek help in the k40 group. But yes, I was expecting that issue to be described instead of a repeat of this. 


---
**Steve Clark** *May 17, 2017 05:43*

That’s correct I did re-post in this community at the recommendation of Ray . Did you read that part? Maybe there was a language barrier?



I went back and saw what you posted after my closing post. What you said makes absolutely no sense to a neo-type like me. Can you clean it up into something that resembles English or are you just here to pick fights?



Ray, sorry if I repeated some of it. It was the only way I knew to explain it.






---
**Steve Clark** *May 17, 2017 07:43*

**+Baked Bean** What you are saying is an EMI field is occuring by something I'm doing. What I am doing is test firing the laser in th LW control with the old current knob at say 5 ma and it works. Move the knob to 8 it shuts the LW down.



There were no problems before the changes.


---
**Don Kleinschnitz Jr.** *May 17, 2017 10:11*

**+Steve Clark** it looks like you are having a noise problem. We have seen these kinds of problems and they usually relate to:



1. Grounding design and integrity

2. Noise susceptibility in the USB hardware on the PC (this was the culprit in an array of these issues)

3. HV archs

....

Search the C3D community as there was a lot of discussion about this kind of problem there.

...

I do not know your exact configuration. But here are some things to try:

1. Insure your ground integrity. Make sure that loads  have grounds that return directly to their power source. 

2. Insure grounds have good connections.

3. Insure that power source grounds are connected from the source to a single point, typically gas tight to the frame. I use a bolt screwed to the frame with star washers on the top and bottom of each ring tongue.

4. Do not run the board from USB power and disconnect the USB power from the controller board. If you are using a C3D **+Ray Kholodovsky** can help with the jumper config.

5. Check in the dark for any HV arc and correct it.

6. If you are running a stock K40 LPS with a smoothie or other controller other than the stock one, you could be exceeding the specs on the supply. 

Note: Although we have suspected the LPS DC to be the culprit in the past, we have never proven it to be true. You can measure the current draw from the 5 & 24V to verify. You can also add a separate DC supply.




---
**Steve Clark** *May 17, 2017 18:18*

Lots of homework! Thks all...


---
**Steve Clark** *May 17, 2017 22:19*

Now that  I’m starting to understand…I think this might be the problem (see pic) 



Accept the original K40 PS, I’m going to disconnect the other three power supplies and see what I get with the new usb cable.



 If that doesn’t work then I’ll pull the C3D panel and move it outside the box. There looks to be just enough wire lengths to do so.



![images/07f3d6faf6db8eb2f2f41849ee0e0ec1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07f3d6faf6db8eb2f2f41849ee0e0ec1.jpeg)


---
**Steve Clark** *May 18, 2017 06:35*

No offense taken. Now that I understand where your coming from you can call me anything you want.  I was never trained to be a ‘wire bender’- grin ( I knew I should have taken an electronics course in college.



  The external stepper driver is/was for the z table. It will go away as soon as I have the x and y working. I thought it would simplify things starting out that way(not hooking it the z up). I also do not have the 4 pin female motor connector yet for that and it should be here Friday. In the mean time I can shut it down and even remove it if necessary.

 

Here is where I don’t understand what you are saying. What Spi and Icd are you referring to? Sorry, ‘Electronics for Dummy’s time’… are you referring to the Z axis  hardware or the LCD ribbon? Or something else?  I’ll be glad to yank it for the test once I know what it is. 




---
**Don Kleinschnitz Jr.** *May 18, 2017 09:49*

**+Steve Clark** look carefully at the list that I sent you. 

The wiring is messy but what usually matters is grounding, and noise injected through power supplies. Sometimes tying wires in bundles close together makes the problem worse.



I would focus on:

1. Grounding Hygiene: Grounds should not be daisy chained. i.e wiring from the power supply to ground on a load then connected from that load to the next load etc. is bad. Rather all grounds should return to the source. I do not see a single point ground anywhere? All source grounds should connect to the frame at one point.

2. Try to find a configuration where the noise problem is not manifested. Run from the SD card without the USB connected.



I still think your best bet is disconnecting the power that is fed from the USB connection.


---
**Joe Alexander** *May 18, 2017 12:11*

first off I have to admit thinking "holy wires Batman!" upon seeing the entangled bundle in the above pic (OCD twitch) but if you happen to have a usb hub with power switches for the ports that works to cut the power out.(at least the one i have does) I can boot up my machine remotely to swap firmware, or turn it off and control it from the main power switch. However I now use a raspi3 running lw,comm-server and access it instead of usb. 

  I would also add a terminal strip to anchor all ground together and bolt that puppy to the chassis


---
**Don Kleinschnitz Jr.** *May 18, 2017 14:23*

**+Baked Bean** **+Ray Kholodovsky** I thought it was a land to cut?


---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2017 14:31*

The diodes are all there, removing USB power is a solder trace to cut on the bottom, but I don't recommend it due to the potential for the user to screw up the board. 


---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2017 17:09*

Yah man, too many variables in play here. Don't want people cutting stuff 


---
**Steve Clark** *May 18, 2017 17:51*

I think Baked has at least one finger pointed at me... My wiring is as bad as China? ...that really hurts man...grin


---
**Steve Clark** *May 18, 2017 19:25*

That's good, my Pop use to shake his head at us for our talk. We use to think he was so out of touch.  I'm old now so try to humor me...


---
**Steve Clark** *May 19, 2017 00:29*

I pulled all the power supplies and wiring out bought a new USB cable and it's fixed!



Jog, Home, and laser testing all good. 



Very little is going back in... but anything that does will be right and be tested. The 4 pin connector for the Z axis isn't here yet but the manual control is NOT going back in and I can learn LW without it.



Thank you very much Baked, Ray, Joe, and Peter for shoving me in the right direction! 



One more question. I know stepper motors can produce back current when they are turned is something be careful of with these lasers? I moved the laser head over by hand today and that reminded me of that as it made the GLCD beep. 




---
**Ray Kholodovsky (Cohesion3D)** *May 19, 2017 00:31*

Yep, don't do it. If you burn a stepper or the board because of it, well, you know the rest... :)


---
**Steve Clark** *May 20, 2017 07:04*

Well it is sort of fixed about 30% of the time it will quit in a job..Doing the last bit of grounding in the morning.


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/XxveJC74Ut8) &mdash; content and formatting may not be reliable*
