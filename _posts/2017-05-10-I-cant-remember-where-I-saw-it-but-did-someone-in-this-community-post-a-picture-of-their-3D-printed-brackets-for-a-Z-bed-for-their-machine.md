---
layout: post
title: "I can't remember where I saw it, but did someone in this community post a picture of their 3D printed brackets for a Z-bed for their machine?"
date: May 10, 2017 00:22
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
I can't remember where I saw it, but did someone in this community post a picture of their 3D printed brackets for a Z-bed for their machine? I seem to recall the brackets being red. Would love to see them again ... anyone?





**"Ashley M. Kirchner [Norym]"**

---
---
**Ned Hill** *May 10, 2017 00:33*

This is what I recall most recently.  [plus.google.com - Adjustable Bed A BIG thank you to +Kelly S for the design of the bed and +An...](https://plus.google.com/+HalfNormal/posts/PNyrev4xqS4)


---
**Ned Hill** *May 10, 2017 00:38*

I also seem to recall there was also a blog post from one of hacker space groups also.


---
**Ashley M. Kirchner [Norym]** *May 10, 2017 00:41*

It wasn't that one, nor the mods from the hacker group. Sucks when I can't remember who/where I saw it.


---
**Ned Hill** *May 10, 2017 00:45*

This one perhaps? [plus.google.com - Here are some pictures of my DIY adjustable Z table for my K40. It is made of...](https://plus.google.com/+AnthonyBolgar/posts/iFrSt2bLmS9)




---
**Ned Hill** *May 10, 2017 00:45*

red 3d printed corners


---
**Ashley M. Kirchner [Norym]** *May 10, 2017 00:50*

I don't think so. I think what I saw was posted in someone else's post as a comment. That's what's making it more difficult to find. But I do recall the comment, and I recall the next comment where someone said they like where it (the design) was going.


---
**Ashley M. Kirchner [Norym]** *May 10, 2017 00:54*

If I don't find it it's not the end of the world. Just hoping someone might've remember seeing it, or the original person posting it again. All I remember of the picture that it was showing a corner of the frame, and it was in red, with square aluminum pieces ... unfortunately, that's just about all of them out there ...


---
**Arion McCartney** *May 10, 2017 02:10*

Here's my remixed design. It works great for me! [thingiverse.com - K40 Adjustable Bed by arion02](http://www.thingiverse.com/thing:2299337)


---
**Ray Kholodovsky (Cohesion3D)** *May 10, 2017 14:39*

**+Ryan Branch** had the one that was featured on hackaday. But I think those were white parts. 


---
**Anthony Bolgar** *May 10, 2017 18:21*

I am printing out yours **+Arion McCartney** right now. Funny coincidence :)


---
**Anthony Bolgar** *May 10, 2017 18:23*

I am also making one using some t8 leasdscrews, bearings and some aluminum channel and v-slot extrusions. Will post some pics once it is complete.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/4BQtDLG6mc1) &mdash; content and formatting may not be reliable*
