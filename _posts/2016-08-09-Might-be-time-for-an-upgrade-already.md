---
layout: post
title: "Might be time for an upgrade already!"
date: August 09, 2016 12:59
category: "Discussion"
author: "Bob Damato"
---
Might be time for an upgrade already! Ive had my little 40W unit for a very short time but already see the utility in it.  I have been making gaskets for GT40 Transaxles that are no longer available.  (link below).   

One thing I quickly determined is that my cutting area is not big enough. That cant be fixed on this model, Im stuck with that size.  Other things I want are air assist, red dot laser assist, ethernet abilities (the usb link is getting old) and a honeycomb table as well as a rotary attachment..  Also something in the 60-80W range would be nice too.  



That being said, does anyone know of a laser that has all these features? Doesnt seem feasable to try and fix or upgrade what I have.  Direct print from photoshop would be nice but Im not holding my breath on that one. Hahaha.



I also expect it would be a $2500 unit or more, but thats fine.

Thanks everyone!





 
{% include youtubePlayer.html id="Z7qWUjn_5dQ" %}
[https://youtu.be/Z7qWUjn_5dQ](https://youtu.be/Z7qWUjn_5dQ)





**"Bob Damato"**

---
---
**Jim Hatch** *August 09, 2016 14:25*

Lots of large bed (up to 3'x4') 80W ones on ebay. They're basically an upscaled K40. I'd still upgrade one with a Smoothie but they tend to come with RDWorks instead of LaserDrw/CorelLaser which is reasonably useful software. This guy has a lot of good stuff including the purchase, updating and use of his large format chinese laser as well as the software:

[https://www.youtube.com/user/SarbarMultimedia/videos](https://www.youtube.com/user/SarbarMultimedia/videos)


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/1WHoNjjJ1gw) &mdash; content and formatting may not be reliable*
