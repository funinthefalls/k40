---
layout: post
title: "Just finished with my latest project. I used my Laser to make a custom panel out of neon green acrylic & jig sawed the original panel to make it fit"
date: January 06, 2016 02:59
category: "Modification"
author: "Todd Miller"
---
Just finished with my latest project.



I used my Laser to make a custom panel

out of neon green acrylic & jig sawed the

original panel to make it fit.  I made some

errors, but I'm happy for now....

![images/e5186ff58f45ab4d9b719f6812bdbc06.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e5186ff58f45ab4d9b719f6812bdbc06.jpeg)



**"Todd Miller"**

---
---
**Sean Cherven** *January 06, 2016 03:20*

Nice! What are you using for the Z-Table? Are you still using the stock controller board?


---
**Todd Miller** *January 06, 2016 03:39*

Yes, I'm using the M2 board it came with.



For the Z-Table, look @ [lightobject.com](http://lightobject.com);



1     LSR-PWRTB320     Power table/ bed kit for K40 small laser machine



1     ECNC-2M415     Mini 2 Phase 1.5A 1-axis Stepping Motor Driver



1       ECNC-SC04     Stepping motor controller board & tester



1     EPS-D24V5APS     DC 24V 5A Switching Power Supply.



And a SPDT switch for up/down; plus you need

to add limit switches.


---
**Josh Rhodes** *January 06, 2016 04:05*

Excellent Mod, Love it!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 06, 2016 04:50*

That looks really cool :) Looks really well done & professional.


---
**varun s** *January 06, 2016 04:59*

Looks cool, any info on that power factor board? 


---
**ChiRag Chaudhari** *January 06, 2016 05:40*

Great job man. Pretty awesome looking control Panel. Love the temp guages. Where did you get those from?


---
**Scott Marshall** *January 06, 2016 07:01*

Nice Job.



 Is that the stock laser control board? 



Much fancier than what I received. I've got a Power Level dial, Laser Enable and Laser Test buttons, and Analog Meter.  Seems to be functionally the same but cheap(er).

 Your setup seems to have buttons that step up and down to set the output power. Coarse & Fine, Up and down, and digital % output?



Nicer than the pot and meter. Guess you've got the Cadilliac package, and I got the Work Truck with mats and AM radio.



You're using the machine to build itself. Machined all the stepper motor mounting blocks for my mill with my mill... 



Had a shop teacher back in HS who said that a mill and lathe were the only tools which could build themselves - now the laser can upgrade itself.




---
**Todd Miller** *January 06, 2016 23:47*

The laser originally came with just a On/Off switch and the power board.  What the power board displays, is just an arbitrary value from 1 - 100  nothing to to with milliamps.



Since I wanted to know exactly what was going on, I added the ma Meter so I wouldn't exceed the limit of the tube.



All other switches; air, cooling, aux, z-table and vent were added by me along with the temp gauges.



The temp gauges came from Ebay and are nice but unless your right on top of them, they are hard to read.  I may replace...



Also, I like the fact that my laser can make parts for itself ;-)


---
**Scott Marshall** *January 07, 2016 00:14*

Is the step up and step down feature a homebrew, or factory arrangement?


---
**I Laser** *January 07, 2016 00:29*

Nice job Todd!



Scott, that's the same type of machine I've got. It has buttons for coarse and fine power setting, but nothing to show you what is actually going through the tube! Not sure it's a cadillac, to be honest I'd be happier with the work truck. ;)


---
**Todd Miller** *January 07, 2016 00:42*

Yeah,  I figured ooooh it's 'digital', must be a better model !



But in reality, I could've saved some $$$ and went with the cheaper unit with a POT.  The quality of the rest of the laser is just as poor.


---
**I Laser** *January 07, 2016 00:58*

It's funny the quality of my analog machine is better than my digital machine. :\



Analog has optical limit switches, larger separated electronics compartment, better engineered gantry and head. Just seems a nicer machine all round. The electronics compartment on the digital has a removable plate on the side which isn't that helpful for accessing the internals. Oh well, live and learn. ;)



Just wondering, where's the temperature sensor placed for the 'laser head temp'?


---
**Scott Marshall** *January 07, 2016 01:24*

Thanks. Looked different.  It seems as though there are more than 1 place in China making these units. 

Maybe it's a different mfr, maybe just a running change.



I was thinking if it was a better current control board I'd track one down. Looks like I'll have to make one if I want to change.

Work Trucks are nice...






---
**Todd Miller** *January 07, 2016 01:35*

The sensor is a temperature probe "shrink tubed" to a steel brake line while filled with silicone heat sink compound.



I place one at each end of the laser tube.



[https://sites.google.com/site/litterbox99/home/laser/sensor.jpg](https://sites.google.com/site/litterbox99/home/laser/sensor.jpg)



 


---
**ChiRag Chaudhari** *January 07, 2016 01:39*

**+Todd Miller** that is great idea to get almost correct temperature reading.


---
**I Laser** *January 07, 2016 02:18*

Very cool Todd! Can you buy them off the shelf, or have to build them?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2016 03:09*

**+Todd Miller** I'm curious, in that photo of the temperature probes shrink-tubed, I notice the main "clear" water tube is not so clear near the steel brake line that you used. It's kind of orangey. What caused this? Was it from heating the shrink tube or is something else causing this discolouration?


---
**Todd Miller** *January 07, 2016 03:26*

Good eye !



I'd concur it was due to chemical reaction/rust from the steel tubing and distilled water I was using for cooling.



Since then I've used distilled water & "red line water wetter" a performance cooling system chemical additive for my cooling system and

the rust has not progressed.



 





 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2016 03:43*

**+Todd Miller** Aha, good old rust. Well, at least you've solved that problem. It's a great setup you've got going & the panel is awesome. I like the transparency of the panel allowing you to see into the electronics section. Another thing I was curious on, your temperatures read 16.1 (Laser Head Temp) & 16.4 (Cooling Input Temp). I imagined that the cooling input would be lower than the head temp?


---
**I Laser** *January 07, 2016 05:30*

Ha, thought the same thing, then wondered if there was just a slight cooling effect in action (low ambient temps, going through the tube with the laser not in use).



Looking at your pic/comment again I can see it's a home made sensor. I assume you've tested the for variances considering it's not in direct contact with the water?


---
**Todd Miller** *January 07, 2016 23:25*

Yuusuf; 



The temp gauges are real inexpensive and not super accurate.  I powered all 5 of them up and at the same time and let them sit for 30 minutes to stabilize.   They all read different, but were close +/- .5c - 1.5c.  I just picked the two that were closest. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2016 23:36*

**+Todd Miller** Haha. I see; that explains how it gets colder as it passes through the laser tube. Thanks for explaining that one. At least you know the temperature roughly (within +/- 2c). For the purpose, I think we don't need much more precision than that.


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/bqGHwoqtLLc) &mdash; content and formatting may not be reliable*
