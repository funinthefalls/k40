---
layout: post
title: "Last week, a friend's cat met its unfortunate end in a tangle with a car"
date: April 16, 2017 19:29
category: "Object produced with laser"
author: "Ulf Stahmer"
---
Last week, a friend's cat met its unfortunate end in a tangle with a car. Thus I decided to make him some replacements. I'm pretty happy with how they turned out. No substitute for the one he lost, though.



The difference between my laser-cut cats is their head design.



![images/118e64364fdd60bebdd80f19a6bdb82e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/118e64364fdd60bebdd80f19a6bdb82e.jpeg)
![images/998b066f928e79146a5851463b7da963.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/998b066f928e79146a5851463b7da963.jpeg)

**"Ulf Stahmer"**

---
---
**Ned Hill** *April 16, 2017 19:36*

Nice job.


---
**Cesar Tolentino** *April 16, 2017 19:50*

I'm really impressed


---
**Ashley M. Kirchner [Norym]** *April 16, 2017 22:26*

Very nice way to remember Kitty.


---
**Richard Vowles** *April 16, 2017 23:01*

Much better than taxidermy.


---
**Ulf Stahmer** *April 17, 2017 01:35*

**+Ned Hill** Thanks, Ned. Means a lot from you because you always make such beautiful things!



**+Richard Vowles** There's something wonderfully dark about your comment. :)


---
**Ned Hill** *April 17, 2017 01:55*

**+Richard Vowles** As someone who has done taxidermy, laser cutting is definitely less mess ;)


---
**HalfNormal** *April 17, 2017 13:01*

**+Ulf Stahmer** Did you design the cat yourself or use a stock one?


---
**Ulf Stahmer** *April 17, 2017 13:58*

**+HalfNormal**  I designed them myself, based on [thingiverse.com - Reindeer by ehler](http://www.thingiverse.com/thing:591235) which I made to decorate a gingerbread house at Christmas. I think I'll make some more cats in different poses. Then I could sell them in a box of 10 and label the box a "Crazy Cat Lady Starter Kit" :)


---
**Ned Hill** *April 17, 2017 14:02*

Lol, Crazy Cat Lady Starter Kit.  I love it, great idea.


---
**ALFAHOBBIES** *April 17, 2017 15:48*

Anywhere to download the cat's?


---
**Ulf Stahmer** *April 20, 2017 19:55*

**+ALFAHOBBIES** I posted the files here [thingiverse.com - Cats - laser cut by wolfpaw98](http://www.thingiverse.com/thing:2259603)

Please post photos of your litter when they're born!


---
**Ned Hill** *April 20, 2017 19:57*

Excellent, thanks so much Ulf. :)


---
**ALFAHOBBIES** *April 20, 2017 20:00*

Wow! That's great. Thank you.


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/VhpKiRQgSbv) &mdash; content and formatting may not be reliable*
