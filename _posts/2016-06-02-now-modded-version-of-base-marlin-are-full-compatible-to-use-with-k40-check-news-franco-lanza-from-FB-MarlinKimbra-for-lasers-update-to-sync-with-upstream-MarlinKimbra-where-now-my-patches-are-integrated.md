---
layout: post
title: "now modded version of base marlin are full compatible to use with k40 check: news franco lanza from FB: MarlinKimbra for lasers update to sync with upstream MarlinKimbra where now my patches are integrated"
date: June 02, 2016 17:33
category: "Modification"
author: "Mauro Manco (Exilaus)"
---
now modded version of base marlin are full compatible to use with k40 check:



news franco lanza from FB:



MarlinKimbra for lasers update to sync with upstream MarlinKimbra where now my patches are integrated.



Pre-configured branch for K40 with cooler and flowsensor: [https://git.nexlab.net/machi](https://git.nexlab.net/machi)…/MarlinKimbra/…/k40_flow_cooler



Pre-configured branch for K40 with NO cooler and NO flowsensor:

[https://git.nexlab.net/](https://git.nexlab.net/)…/MarlinKim…/tree/k40_noflow_nocooler



Updated pronterface for the new M142 mcode: 

[https://git.nexlab.net/machinery/Printrun](https://git.nexlab.net/machinery/Printrun)



Update inkscape plugin for new M142 mcode and better rastering:

[https://git.nexlab.net/](https://git.nexlab.net/)…/laser-gcode-exporter-inkscape-plug…



And remember, if you like my work on laser edition of MarlinKimbra, inkscape plugin and pronterface, support me by donating or even just by spread the project home:



[https://www.nexlab.net/k40-laser/](https://www.nexlab.net/k40-laser/)



Reddit: [https://www.reddit.com/](https://www.reddit.com/)…/marlinkimbra_inkscape_plugin_and_…/



hacker news:

[https://news.ycombinator.com/item?id=11815379](https://news.ycombinator.com/item?id=11815379)





**"Mauro Manco (Exilaus)"**

---
---
**Julia Longtin** *June 03, 2016 14:24*

have to sign in to look at this?


---
**cory brown** *June 03, 2016 18:11*

No i didn't... I think he has an other link on his [https://www.nexlab.net/donations/k40-laser/](https://www.nexlab.net/donations/k40-laser/) page...


---
*Imported from [Google+](https://plus.google.com/114285476102201750422/posts/649s8uS2Bp1) &mdash; content and formatting may not be reliable*
