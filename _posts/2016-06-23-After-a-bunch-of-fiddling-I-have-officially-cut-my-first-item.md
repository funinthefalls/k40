---
layout: post
title: "After a bunch of fiddling, I have officially cut my first item"
date: June 23, 2016 21:07
category: "Discussion"
author: "Ned Hill"
---
After a bunch of fiddling, I have officially cut my first item. Yeah! Cut out my daughter's name on the cardboard from the back of an old legal pad.  Did the old trick of cutting the name and then reattaching slightly off set. :)



![images/904d9057624caed1a759f56183420672.png](https://gitlab.com/funinthefalls/k40/raw/master/images/904d9057624caed1a759f56183420672.png)
![images/b2248829fb2968db16a36ba95f454093.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b2248829fb2968db16a36ba95f454093.png)

**"Ned Hill"**

---
---
**Stephen Sedgwick** *June 23, 2016 21:53*

looks good :-)


---
**Travel Rocket** *July 01, 2016 00:18*

Nice! What setting did you use? (ampere and speed)


---
**Ned Hill** *July 01, 2016 00:28*

Lol that was many lasered items ago now and I can't recall.  It's basically thin card board so probably like 15% power (tube starts lasing at 10%) at 10 mm/s.


---
**Travel Rocket** *July 04, 2016 12:57*

Awesome, oh what does the honeycomb do?


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/6mutRzN1gnQ) &mdash; content and formatting may not be reliable*
