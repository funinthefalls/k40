---
layout: post
title: "Hey guys, i ordered a k40 from amazon"
date: January 18, 2017 06:17
category: "Original software and hardware issues"
author: "Nick Hale"
---
Hey guys, i ordered a k40 from amazon.  Came in two days later. Packaging looked good.  Started my inspection and found the roller holds the right side of the Gantry up broken off. I was able to find the roller in the bottom of the tray. The head broke off the screw, but I was able to extract the threads from the roller. My first question is what size screw is that? It looks like an M2 machine screw. I contacted the Amazon Seller and they told me they could not get replacement parts.



Second question. There was three softwares on the disc. I installed all three of them after installing Corel Draw X7. But when I launch Corel laser it throws an error. It says Corel Draw is not installed. The other two softwares that came on the disc would launch fine, but when I went to send the job it said mismatch model ID. I check the model number and it is the same one that was listed. Is there a cheap alternative to this board? Or does anybody have a idea how to get Corel to work

![images/b9c863fc726c7136bcf7c485cdfc49b5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b9c863fc726c7136bcf7c485cdfc49b5.jpeg)



**"Nick Hale"**

---
---
**Andy Shilling** *January 18, 2017 12:08*

You may have luck looking in your local R/C model shop for screws that size, otherwise you might need to drill and tap it to a slightly bigger size.




---
**Coherent** *January 18, 2017 12:33*

take the the thread you have to a hardware store. They will match it. Also tell the seller you want a refund of the amount for the part and gas etc to go buy and repair it. They most likely will.




---
**HalfNormal** *January 18, 2017 12:58*

I had a similar issue. It is a metric size but I do not remember exactly what. The replacement worked just fine. You can find the entire assembly all over the net if you want to go that direction. 


---
**Nick Hale** *January 18, 2017 19:28*

I live in a small town with only a small lumberyard and a fastenal store. Both places laughed me out of the store. 


---
**Nick Hale** *January 19, 2017 01:14*

I ordered a small screw kit from amazon.  Hopefully it will have the one i need.  Any advice on the software? 


---
**Helcio Xavier** *January 24, 2017 14:06*

I ordered on AliExpress today, I'm already scared.

Under what conditions I received the machine is a long trip to Brazil.


---
**Russ “Rsty3914” None** *March 13, 2017 03:15*

Light object I believe...has them..


---
*Imported from [Google+](https://plus.google.com/101739286835462605895/posts/DdrKn6WzJ7T) &mdash; content and formatting may not be reliable*
