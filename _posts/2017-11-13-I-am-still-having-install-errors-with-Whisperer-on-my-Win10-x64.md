---
layout: post
title: "I am still having install errors with Whisperer on my Win10 x64"
date: November 13, 2017 15:21
category: "Software"
author: "David Retske"
---
I am still having install errors with Whisperer on my Win10 x64.  I have followed Scorch Works' instructions.  Device Manager says that libusb-Win32 Devices -> USB-EPP/.... is installed, but Whisperer keeps giving me the 

"USB Error: No backend available (libUSB driver not installed)"  

error. 

I have been using Zadig to install.  Am I missing something?





**"David Retske"**

---
---
**David Retske** *November 13, 2017 15:22*

M2 Nano Board on the laser.




---
**David Retske** *November 13, 2017 15:42*

Here it is in device manager

![images/1109e844948c7348d2ddaaeeb77040f2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1109e844948c7348d2ddaaeeb77040f2.png)


---
**Scorch Works** *November 13, 2017 16:43*

The device manager looks correct. I don't know why it is not working for you.





I would remove the driver and try Installing it again.   Do not use the filter driver option (7b) make sure you use option 7a.



If that does not work, remove it again and try the Zadig version for XP.



When installing make sure you use the Zadig versions linked in the instructions, they are known to work.


---
**David Retske** *November 13, 2017 16:56*

I have tried with both options.  This picture was from the 7a install.


---
**Scorch Works** *November 13, 2017 17:11*

Make sure you try the XP version.  



Also do the normal computer trouble shooting stuff like reboot the computer and turn the laser off then on again. 



It would be worth trying on another computer if you have one available.


---
**David Retske** *November 13, 2017 17:14*

Ok I 'll try those.  I was trying to get it going on a macbook, but I got distracted.  I'll both again soon.




---
**David Retske** *November 14, 2017 01:07*

Quick update.  I haven't been able to get it working on the Windows 10 pc, but I finished it up on the Macbook at the end of the day.

I'm sure it has to do with Zadig and not installing the LibUSB, because I had the same issue on the Macbook until I used brew to install the libUSB library.

I'm running with this solution for now, I will update if I figure anything else out.


---
**Scorch Works** *November 14, 2017 01:19*

Thanks for the update.



Problems like this drive me crazy because it looks like the driver is there in your device manager.


---
**David Retske** *November 14, 2017 02:58*

I agree and I'm sure if I did a manual install I could get it done, but the Win10 is my main baby and if she goes down I go down.  

The Mac is old and I was willing to risk it.




---
**krystle phillips** *December 12, 2017 21:48*

**+Scorch Works** I am having a similar issue it is being detected as a virus






---
**Scorch Works** *December 13, 2017 00:51*

**+krystle phillips**​ What is being detected as a virus? The driver?


---
*Imported from [Google+](https://plus.google.com/+DavidRetske/posts/jpTydNuJWN4) &mdash; content and formatting may not be reliable*
