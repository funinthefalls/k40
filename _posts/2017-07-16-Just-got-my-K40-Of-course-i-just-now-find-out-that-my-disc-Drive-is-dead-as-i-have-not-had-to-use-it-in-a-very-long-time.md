---
layout: post
title: "Just got my K40, Of course i just now find out that my disc Drive is dead as i have not had to use it in a very long time"
date: July 16, 2017 05:12
category: "Original software and hardware issues"
author: "Kenneth White"
---
Just got my K40,  Of course i just now find out that my disc Drive is dead as i have not had to use it in a very long time.  Managed to find the Correlaser software,  No clue where to find LaserDrw.  I tried K40 Whisper and it works but It seemed like it did more burning then engraving and i had the power set so low the laser was barely on.



So my questions are, Where do i get correl that works, tried their site but it would not give me a download, only for the new Correl Suite 2017.  



what software do i use For Engraving Photos?



And then the typical, This is my first laser cutter, how the heck do i align, Focus, set the power since its on a Pot, to where i can Dither and not have to f**@# with the dang knob to do cuts, etc.  this is nothing like my little 1w laser i got for craps and giggles and attached to my 3d printer.





**"Kenneth White"**

---
---
**Kenneth White** *July 16, 2017 05:32*

If I do need to change The board, I DO have an Arduino Uno and a CNC Shield, (The one with 4 Stepper Drivers) and Could Do GERBL but i would definitley need help doing that, Alternatively I also have an Extra GT2560 Board from my 3d printer.


---
**Don Kleinschnitz Jr.** *July 16, 2017 13:42*

You could try this?:[http://www.scorchworks.com/K40whisperer/k40whisperer.html](http://www.scorchworks.com/K40whisperer/k40whisperer.html) 

[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
**Kenneth White** *July 16, 2017 15:32*

**+Don Kleinschnitz** erm, i kind of mentioned that i have tried that, it works, but its barely a piece of software.


---
**HalfNormal** *July 16, 2017 15:40*

Use your search function and you will find all the answers to your questions. There is even a link to the original CD that came with the K40. Lots of luck and have fun.


---
**Kenneth White** *July 16, 2017 15:52*

**+HalfNormal** I spent like 4 1/2 hours searching i found the link to the Original Site, but the Only Software available was CorelDraw and WinSeal.  I have a B-Lock device, and i cant Corel to install on my computer, its got a bug where its saying its already installed even though its not.  Ended up Finding the Software here: [lcshenhuilaser.com - Download - Liaocheng shenhui laser equipment Co,.ltd.](http://www.lcshenhuilaser.com/download.html)


---
**Don Kleinschnitz Jr.** *July 16, 2017 15:56*

**+The Mad-Mapper** sure enough I didn't see that you had tried K40whisperer.

Your questions are all good ones and answers are all over this forum.

I suggest start by reading everything that is available by searching the community. 

Note that pinned at the top of the community page is a summary post of <i>_how to use this community and resources</i>. You will find most of your answers there, if not, post specific questions relating to your needs..... :)_


---
**HalfNormal** *July 16, 2017 15:56*

Hopefully soon we will have a FAQ in place and make it less painful to find what everyone is looking for. Please don't get too frustrated in finding the answers. We will do our best to answer you.


---
**Don Kleinschnitz Jr.** *July 16, 2017 15:59*

**+The Mad-Mapper** 

#K40Corel


---
**Kenneth White** *July 16, 2017 16:16*

**+Don Kleinschnitz** 

![images/77c6814a377ac25354ed74271b460251.png](https://gitlab.com/funinthefalls/k40/raw/master/images/77c6814a377ac25354ed74271b460251.png)


---
**Don Kleinschnitz Jr.** *July 16, 2017 16:24*

**+The Mad-Mapper** 

... that was a # to collect this post for the community. It resolves if you enter it into the community search bar. It doesn't in the main bar, likely cause the server has not added it yet.


---
**Scorch Works** *July 18, 2017 23:36*

**+The Mad-Mapper** "...i have tried that(K40 Whisperer), it works, but its barely a piece of software." LOL you need to download LaserDRW!


---
**Kenneth White** *July 19, 2017 05:42*

**+Scorch Works** shhhhhh you know ive changed my opinion on it lol.  K40 Whisper is great, way better then the shite software that comes with the machine.  and i have no doubt it will continue to get better.  Although still considering switching to GRBL so i can have PWM lol, I hate adjusting dials lol.


---
**Scorch Works** *July 19, 2017 11:55*

**+The Mad-Mapper** I know I was just reading old comments and it struck me funny.  Power control is definitly a good reason to switch.


---
*Imported from [Google+](https://plus.google.com/100692370397257622708/posts/NpCQspYdNGq) &mdash; content and formatting may not be reliable*
