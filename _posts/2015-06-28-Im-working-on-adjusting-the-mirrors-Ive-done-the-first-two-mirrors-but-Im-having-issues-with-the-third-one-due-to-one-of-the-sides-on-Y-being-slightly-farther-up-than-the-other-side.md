---
layout: post
title: "I'm working on adjusting the mirrors. I've done the first two mirrors, but I'm having issues with the third one due to one of the sides on Y being slightly farther up than the other side"
date: June 28, 2015 03:53
category: "Discussion"
author: "quillford"
---
I'm working on adjusting the mirrors. I've done the first two mirrors, but I'm having issues with the third one due to one of the sides on Y being slightly farther up than the other side. How should I adjust that?





**"quillford"**

---
---
**David Richards (djrm)** *June 28, 2015 04:20*

Hi again, you need to describe your problem better so i can understand it. I found some detailed instructions online some to help me to align my mirrors. Mine second mirror was way out when i received my cutter but they seem ok now. The main broblem was due to the mirror mount having got bent in transit and i had to dismantle it to straighten it before re-aligning it. I'll look for the instructions later if you still need them. 


---
**quillford** *June 28, 2015 04:25*

**+david richards** the bar the cutting head moves along is at a slight angle


---
**David Richards (djrm)** *June 28, 2015 04:58*

Have the mounting holes been drilled incorrectley? I think you will need to sort this problem before aligning the mirrors. There will be a focus problem othrrwise. You may be able to partially compensate by mirror alignment. Part of the procedure is to get the spot correct at each end of the carriage travel.


---
**Steven Robinson** *July 11, 2015 11:47*

 Mine shared this problem. The cuts on end of gantry were not square, Remove and grind off a bit of metal to make 90 degree ends. Don't remove much metal !


---
*Imported from [Google+](https://plus.google.com/115654175984463400645/posts/25EeDMiPjoE) &mdash; content and formatting may not be reliable*
