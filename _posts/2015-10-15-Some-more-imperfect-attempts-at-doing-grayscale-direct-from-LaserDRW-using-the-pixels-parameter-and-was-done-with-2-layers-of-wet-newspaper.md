---
layout: post
title: "Some more imperfect attempts at doing grayscale direct from LaserDRW using the \"pixels\" parameter, and was done with 2 layers of wet newspaper"
date: October 15, 2015 19:31
category: "Materials and settings"
author: "Kirk Yarina"
---
Some more imperfect attempts at doing grayscale direct from LaserDRW using the "pixels" parameter, and was done with 2 layers of wet newspaper.



Upper Left is 100mm/s,  10ma, pixels 3 steps



Upper RIght is 100mm/s, 15 ma, pixels 3 steps



Lower Left is 66mm/s, 12ma, pixels 4 steps (probably shouldn't have changed from 3...)



Lower Right is 66mm/s, 12ma, pixels 5 steps.  The paper was getting try and starting to flame up, so I paused and resprayed the paper.  The gap is when I bumped it testing how dry the paper was.



The second picture is the original, a grayscale conversion of pic of Bled Castle in Slovenia.



![images/aa2b524770c1e330c1c5ce465a1aab68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa2b524770c1e330c1c5ce465a1aab68.jpeg)
![images/49c7c4b84b4c32f0578b4d89f00105c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/49c7c4b84b4c32f0578b4d89f00105c9.jpeg)

**"Kirk Yarina"**

---
---
**Kirk Yarina** *October 16, 2015 00:42*

It cools the glass and prevents cracking (tested that :)  ), without anything the glass will crack).  Another technique is to buy laser masking paper, but it's spendy.   Some use liquid dish soap; haven't tried that yet.



I forgot to invert the photo and  show the back side of the glass, but these are just to try and beat the software in to submission and get the right speed and power settings.


---
**Ashley M. Kirchner [Norym]** *October 16, 2015 04:49*

Just wet paper, no other treatment on the glass?


---
**Ashley M. Kirchner [Norym]** *October 16, 2015 04:59*

Also, what exactly is the "pixels" parameter ... what does it do? I'm assuming the 'repeat' is just how many times it will repeat the whole engraving process.


---
**Kirk Yarina** *October 16, 2015 18:11*

**+Ashley M. Kirchner** Just newspaper and water.  I've also used copier paper, works just as well.  I think the paper sucks up too much power and isn't consistent, but maybe it's operator error.   I read that cheap soda glass works best, can't get much cheaper than broken windows, as long as you replace the busted panes first :)



As far as I can tell the "pixels" parameter will take a pixel by pixel sized block, average the black intensity, then turn on (fire or not, depending on "invert") the laser for enough pixels to make the average intensity of the engraved block match the picture.  For example, if it's 50% gray then it'll hit half the dots.  It's called halftone or dithering.  Since you can only make a dot that's on or off you make enough small dots so your eye sees the average thinks it's a shade of gray.



Or I'm completely wrong.  The copier paper test I did a few days back looks like that's what's happening, but...    It's the last minute mad rush before winter here in the north woods (we're on a small farm close to Lake Superior), plus I'll be out of town for a week,  so there's not a lot of spare time these days.  Try experimenting with the settings on scrap, see what happens, and report back.



Repeat will repeat things for me, like vector cut twice; I mostly use it for multi pass cutting but it's probably more useful than that.  Haven't tried it engraving, yet.


---
**Ashley M. Kirchner [Norym]** *October 16, 2015 18:26*

Awesome, that's good info. I haven't tried glass yet, but I have a couple of blocks of clear glass that are about 3/4" thick that I wanted to try to engrave. I'm still rather hesitant to be completely honest.



I did however buy a can of that CRC Dry Moly stuff to try and engrave some scrap aluminum sheet that I have ... no such luck. Tried different settings while 'Marking' but the stuff just comes off when you clean it with alcohol (as suggested.) So for the time being, metals will remain on the back burned till I figure out another option.


---
**Kirk Yarina** *October 18, 2015 12:37*

Besides the busted window pieces I'm trading some cottage signs for glass and acrylic scraps at a local hardware store.  Should give me lots of test pieces.  I wouldn't want to try your expensive sounding pieces without a lot of testing first.  Are they regular glass or something else?


---
*Imported from [Google+](https://plus.google.com/+KirkYarina/posts/ce8khS7SrEh) &mdash; content and formatting may not be reliable*
