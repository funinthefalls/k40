---
layout: post
title: "Cut some blower fan brackets for the dual extruder mod for my Wanhao I3 printer"
date: April 19, 2016 00:53
category: "Object produced with laser"
author: "David Cook"
---
Cut some blower fan brackets for the dual extruder mod for my Wanhao I3  printer.  I designed the dual nozzle mount also that is 3D printed.  This is to mount the E-3D Chimera & Cyclops extruder system.  I originally 3D printed the fan mount and then I wanted to modify it but not wait 2 hours for the new one lol.  So I modified it for the laser.  3mm flourescent green cast acrylic.  Very UV sensitive !   I designed these parts for a friend that goes by "Prometheus 3D"  and that's his Trident logo as well





**"David Cook"**

---
---
**Anthony Bolgar** *April 19, 2016 05:00*

Nice idea, are you planning on publishing these mods? I have a Di3 that I would love to do this to.


---
**Stephane Buisson** *April 19, 2016 06:36*

Nice job done with SmoothK40, raster and cut in same job. (see video, 2nd in album)


---
**David Cook** *April 19, 2016 15:33*

**+Anthony Bolgar**  Thanks.  I do plan to publish these mods.  


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/3kRXUqdMoQA) &mdash; content and formatting may not be reliable*
