---
layout: post
title: "For an adjustable objective lens ??? Been thinking about this"
date: November 04, 2016 12:18
category: "Modification"
author: "Don Kleinschnitz Jr."
---
For an adjustable objective lens ???



[https://www.amazon.com/stepper-Stepper-degrees-Sliding-slipway/dp/B00SWDQUGK/ref=pd_sbs_328_3?_encoding=UTF8&pd_rd_i=B00SWDQUGK&pd_rd_r=NM015R0TRMPQJAZDE63D&pd_rd_w=vHvz8&pd_rd_wg=RSECs&psc=1&refRID=NM015R0TRMPQJAZDE63D](https://www.amazon.com/stepper-Stepper-degrees-Sliding-slipway/dp/B00SWDQUGK/ref=pd_sbs_328_3?_encoding=UTF8&pd_rd_i=B00SWDQUGK&pd_rd_r=NM015R0TRMPQJAZDE63D&pd_rd_w=vHvz8&pd_rd_wg=RSECs&psc=1&refRID=NM015R0TRMPQJAZDE63D)



Been thinking about this. I may want to make this hack so that I can easily snap in different focal length lenses for cutting vs engraving. 



I use a lift table to adjust for large pieces and eventually a rotary table will be built.





**"Don Kleinschnitz Jr."**

---
---
**Michael Audette** *November 04, 2016 12:44*

Just rip one out of a CD-ROM/DVD drive and try it out.  I bet you can find one for free somewhere :D  $20 bucks!




---
**Don Kleinschnitz Jr.** *November 04, 2016 12:53*

Yes I have done as you suggest for other projects but found that getting the remounting of the slider right was a pain. 

What I like about this one is that the leadscrew, base and slide are all there. 

Your idea is certainly cheaper.


---
**HP Persson** *November 05, 2016 03:04*

I tried one of thoose, doesnt have any (enough) holding torque at all, even powered.

So i went with a smaller Nema8 instead, work in progress.

I don´t have a movable table, so i´m making a autofocus.

![images/b8dd99dfb0210a6b4b125508f97d5f5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8dd99dfb0210a6b4b125508f97d5f5b.jpeg)


---
**Don Kleinschnitz Jr.** *November 05, 2016 11:47*

**+HP Persson** I saw that slide you posted and had to have it :). Bought it and now its admiring me on my desk waiting for a project....


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/7QQ4NYE3dMm) &mdash; content and formatting may not be reliable*
