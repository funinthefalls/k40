---
layout: post
title: "Idea for Z Axis Mod I have been thinking of making a simple scissor lift for an adjustable bed"
date: January 31, 2016 17:08
category: "Modification"
author: "HalfNormal"
---
Idea for Z Axis Mod



I have been thinking of making a simple scissor lift for an adjustable bed. A motor or hand crank can be used for adjustment. I will test with wood and then make a metal/aluminum one that would be more permanent.

What do you think?

[http://www.instructables.com/id/DIY-Scissor-lift/](http://www.instructables.com/id/DIY-Scissor-lift/)





**"HalfNormal"**

---
---
**Arestotle Thapa** *February 01, 2016 04:31*

There is a post of someone using a lab jack.


---
**Coherent** *February 01, 2016 20:56*

You can buy auto power or wiper motors really cheap on amazon ($10 or so). They are geared with slow rpm so pretty powerful, run on 9-12v dc and are easy to create mounts for, especially of you have a 3d printer. Also, a simple on-off-on dpdt switch will allow you to reverse power polarity and motor rotation direction. I mounted one to a lab jack and it worked well.


---
**ThantiK** *February 02, 2016 02:15*

**+Nicholas Seward** has a 3D printer design that uses this; don't think he's brought it to life though. -- [http://3dprint.com/wp-content/uploads/2014/07/seward-portable.gif](http://3dprint.com/wp-content/uploads/2014/07/seward-portable.gif)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/TR2TvuisMXa) &mdash; content and formatting may not be reliable*
