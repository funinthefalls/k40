---
layout: post
title: "Any suggestions on cutting a 3mm wood without soot?"
date: July 11, 2016 10:05
category: "Materials and settings"
author: "Travel Rocket"
---
Any suggestions on cutting a 3mm wood without soot? I cant seem to perfect it when cutting. any idea or setting suggestion?



Thanks!





**"Travel Rocket"**

---
---
**Anthony Bolgar** *July 11, 2016 10:43*

Try using masking tape o top of the plywood.


---
**Jim Hatch** *July 11, 2016 11:43*

Might need it on the bottom too depending on what your bed is made of as you can get flashback residue on the undersides.



Or run a few passes of 220 grit sandpaper over it (i use a random orbit sander).



Isopropyl alcohol can also clean it off.


---
**Evan Fosmark** *July 11, 2016 17:51*

If you're talking about soot on the sides of the cuts, the trick seems to be to limit the exposure time for the laser. Remember, you want the laser to vaporize the wood, not burn. I run at 30% power, 9.5mm/s. Keeps it fast enough that I get little-to-no charring on the edges. Air-assist is really useful, too.


---
**Travel Rocket** *July 12, 2016 05:41*

**+Anthony Bolgar** will try on this tonight! Thanks!



 **+Jim Hatch** what is your preffered bed? Honeycomb? what does it do?



**+Evan Fosmark** Do you have a sample of how it looks like?


---
**Jim Hatch** *July 12, 2016 11:38*

I have a pin bed I made using pneumatically driven finish nails (18 ga).


---
**Travel Rocket** *July 12, 2016 12:41*

**+Evan Fosmark**  how many pass can you cut the wood?


---
**Travel Rocket** *July 12, 2016 12:45*



Im having problems with cutting wood (5mm) I tried almost everything from increasing the power (disadvantage of soot) and decreasing the speedtime (flame will appear). any good combination?



Thanks﻿


---
**Evan Fosmark** *July 12, 2016 18:05*

Are you certain you have it focused?


---
**Travel Rocket** *July 13, 2016 02:25*

yes, i cut acrylic plastic and its good. i am having a hard time when i cut wood


---
*Imported from [Google+](https://plus.google.com/100865688973733022249/posts/DSXZAQEUDdw) &mdash; content and formatting may not be reliable*
