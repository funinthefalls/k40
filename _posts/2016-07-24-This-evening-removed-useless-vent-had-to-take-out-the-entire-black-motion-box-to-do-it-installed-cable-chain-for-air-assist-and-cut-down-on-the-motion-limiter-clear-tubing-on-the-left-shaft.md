---
layout: post
title: "This evening: removed useless vent, had to take out the entire black motion box to do it, installed cable chain for air assist, and cut down on the motion limiter clear tubing on the left shaft"
date: July 24, 2016 02:38
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
This evening: removed useless vent, had to take out the entire black motion box to do it, installed cable chain for air assist, and cut down on the motion limiter clear tubing on the left shaft. All in all the head can now span 24cm in the Y, although since my air assist sticks out in that direction it's a bit less. 



![images/2f9187b9b4850a862329b226dcef6d5f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f9187b9b4850a862329b226dcef6d5f.jpeg)
![images/63f1317757ae864b0f9a27c826aeb982.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/63f1317757ae864b0f9a27c826aeb982.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Pippins McGee** *July 24, 2016 05:46*

those limiter switches your machine came with or you installed aftermarket?

my k40 machine does not have, it can travel a bit more to the left physically but 0,0 leaves a gap on the left of unused (accessible) space


---
**Robert Selvey** *July 24, 2016 06:09*

What size is your cable chain ?


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2016 14:41*

Stock limit switches. I am aware of 2 variants of the k40 - the other kind has optical limit switches and a ribbon cable for the motor and endstops. 

10x20 drag chain. Just a few bucks on aliexpress for 1 meter. I used half. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/dM9Wq7N322A) &mdash; content and formatting may not be reliable*
