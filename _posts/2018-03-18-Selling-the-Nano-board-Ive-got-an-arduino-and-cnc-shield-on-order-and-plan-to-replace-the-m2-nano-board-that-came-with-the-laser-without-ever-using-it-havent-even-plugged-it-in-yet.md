---
layout: post
title: "Selling the Nano board. I've got an arduino and cnc shield on order and plan to replace the m2 nano board that came with the laser without ever using it (haven't even plugged it in yet)"
date: March 18, 2018 03:42
category: "Original software and hardware issues"
author: "Tom Traband"
---
Selling the Nano board.



I've got an arduino and cnc shield on order and plan to replace the m2 nano board that came with the laser without ever using it (haven't even plugged it in yet). I'm thinking of selling the board to cover costs for some of the upgrades.



Looking on eBay I see a couple of listings, one with the USB dongle. Does the dongle "match" the board in some way so that the 2 should be sold together?





**"Tom Traband"**

---
---
**HalfNormal** *March 18, 2018 04:01*

The dongle is for the laserdrw software. Before K40 Whisperer it was the only software that could be used with the M2Nano. 


---
**Adrian Godwin** *March 18, 2018 12:11*

I'm puzzled as to why software like laserdrw is shipped with something that shows every sign of minimising cost. Surely they'd have sourced something like K40 Whisperer themselves at lower cost ? 



I'm sure the vast majority of the effort that's gone into Whisperer is the reverse-engineering, yet it's still more usable than the alternatives - mostly because it doesn't put effort into crap, badly documented editing features  that nobody needs because they can do it in the design package of their choice.



What am I missing ? Do the manufacturers believe they can't sell a cutter without the traditional edit software ? The low-cost 3d printer manufacturers seem to have latched on to open tools easily enough.

 


---
**Don Kleinschnitz Jr.** *March 18, 2018 14:02*

**+Adrian Godwin** 



Start (editorial);

Most hardware vendors especially in china ship software for free as they really consider it a cost of building the hardware. Apparently there isn't is not a lot of $ spent on good software engineering as hacks are considered as good enough.



I also suspect that laserdraw was developed quite a long time ago in the corel era and the proprietary interface was considered a way to protect sales ...



It is beyond me why they just don't ship a grbl/Gcode compatible controller in the K40 like many of the cheap x-y machines you find on Bangood. Perhaps they consider the proprietary controller their IP. Which is ironic considering how much software china rips off from elsewhere. 



Then again it is beyond me why many of the K40's easy to fix things are not addressed. Like why go through the effort to add a digital panel that removes function. Why not include air assist, interlocks etc ......



Perhaps it is the K40 manufacturers way of keeping us Makers entertained :).



end(editorial);


---
**Paul de Groot** *March 18, 2018 20:28*

I guess their business model is a franchise where many factories build the k40 which explains the many versions and quality differences. The original owners probably make enough money not to be bothered with improving the tool chain. I'm surprised how many controller boards we sell every week.


---
**HalfNormal** *March 18, 2018 20:39*

The software has not been updated since 2013.


---
**Anthony Bolgar** *March 18, 2018 20:50*

**+Paul de Groot**  If you did some more marketing it would be even more sales for you. But I bet once you start selling that Laser Safety system an exceptionally smart friend of yours is developing, sales will also increase ;)


---
**Paul de Groot** *March 18, 2018 21:43*

**+Anthony Bolgar** yes I love you comment. Just got stuck with two frozen shoulders which is extremely limiting my ability to do anything at this moment. Hopefully I get over this fast so I can spend some effort on this.


---
**Anthony Bolgar** *March 18, 2018 23:14*

Get well soon my friend




---
**Ned Hill** *March 19, 2018 03:56*

Yes **+Tom Traband** the dongle is matched to the board so they would need to be sold together.


---
**HalfNormal** *March 19, 2018 03:58*

**+Ned Hill** the dongle is not mated to the board. Put the board number into laser draw and if you have the dongle, laser draw will work.


---
**Ned Hill** *March 19, 2018 04:04*

**+HalfNormal** Really? I always thought they were matched.  Then why do you even have to put the board number into the software?  It has to match to something and I would have thought it was the dongle.  Unless the dongle has many numbers?


---
**HalfNormal** *March 19, 2018 04:06*

**+Ned Hill** I do not know the answer. I just know that I replaced a board and did not need a new dongle. 


---
**HalfNormal** *March 19, 2018 04:08*

That is why K40 Whisperer works.


---
**Ned Hill** *March 19, 2018 04:13*

Well, K40 whisperer is a reverse engineering of the code and strips out that security feature.  Anyway it's interesting to know that you don't have to have a matching board and dongle.  


---
**Anthony Bolgar** *March 19, 2018 04:24*

There is no security native on the controller board. The dongle ONLY authorizes the LaserDrw software. What Scorch did was figure out what the controller wanted to see sent to allow movement etc. The protocol he cracked has no security on it, they just do not document anything about it to the masses, so it took a while until Scorch decided to do traffic captures on the USB port to figure out the protocol. Same as Lightburn has done with the Ruida protocol.


---
**Ned Hill** *March 19, 2018 04:46*

**+Anthony Bolgar** Ahh ok, thanks :)


---
**Ned Hill** *March 19, 2018 04:57*

**+Anthony Bolgar** So does this mean that you don't really have to put your unique board number into the software?  Since the start everyone said you had to do it so I did and always told everyone after me to do the same.  Unless  a range of numbers are tied to the software?


---
**Anthony Bolgar** *March 19, 2018 05:26*

The board number tells the software what type of board it is as there are the M2Nano, and HXT variants (about 8 of them) 


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/4euet7C3nqw) &mdash; content and formatting may not be reliable*
