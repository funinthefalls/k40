---
layout: post
title: "Last weekend my K40 is gone in flame...."
date: February 13, 2015 09:48
category: "Discussion"
author: "Alessandro Milano"
---
Last weekend my K40 is gone in flame....

Not a machine problem , but the acrilic I was cutting is gone in flame while I was in another room and when I feel smell it was too late....

Now I need to restore the mechanism : I need a new support for the last stage of the lens , air tube and cables from stepping motor to the laser pointer.... sigh... i don't think i'll find them easily...

Pay attention when you use your machine ! In a minute of distraction you can create destruction !! Sigh !!





**"Alessandro Milano"**

---
---
**Manuel Conti** *February 13, 2015 09:51*

Hi, where did you buy your machine? I'm italian. 


---
**Imko Beckhoven van** *February 13, 2015 09:51*

[http://www.lightobject.com/Search.aspx](http://www.lightobject.com/Search.aspx) maby the have some of the parts you need.﻿ look for the d 40 upgrade parts..


---
**Alessandro Milano** *February 13, 2015 09:53*

**+Manuel Conti** Ciao, l'ho presa tramite eBay da un venditore cinese. Ho speso 900 euro spedizione e dogana inclusi

**+Imko Beckhoven van** Thank you , now I take a look (hoping !!!!)


---
**Stephane Buisson** *February 13, 2015 09:55*

Maybe **+Joy Yang**  could help, he is working with the K40 factory.


---
**Manuel Conti** *February 13, 2015 09:56*

**+Alessandro Milano**​ grazie mille. Avevo intenzione di acquistarla anche io ma mi preoccupavo sia della dogana che mi arrivasse guasta. Pensavo a quei venditori che hanno i magazzini qui in europa, almeno per risparmiare i dazi doganali... 


---
**Stephane Buisson** *February 13, 2015 10:01*

for the lens -> [http://www.ebay.co.uk/itm/Dia-12mm-CVD-IIVI-ZnSe-Focus-Lens-10600nm-CO2-Laser-Engraver-Cutter-FL-1-5-/221627408067?pt=UK_H_G_Major_Appliances_Oven_Cooker_Hoods_ET&hash=item339a05dec3](http://www.ebay.co.uk/itm/Dia-12mm-CVD-IIVI-ZnSe-Focus-Lens-10600nm-CO2-Laser-Engraver-Cutter-FL-1-5-/221627408067?pt=UK_H_G_Major_Appliances_Oven_Cooker_Hoods_ET&hash=item339a05dec3)


---
**Alessandro Milano** *February 13, 2015 10:27*

**+Manuel Conti** In effetti è meglio, quando l'ho presa spedivano solo da Hong Kong, ora invece molti anche da UK. Ho avuto a che fare con questo [http://stores.ebay.it/businesspleasant/](http://stores.ebay.it/businesspleasant/) che spedisce appunto da UK

E' molto bello come sistema ma all'inizio ci vuole un po di pazienza. Se devi ancora prenderla cerca le macchine da 40W che hanno il pannello di controllo NON con la rotella ma solo con i pulsanti, sono le più recenti !

**+Stephane Buisson** Thank's !!!


---
**Bee 3D Gifts** *March 01, 2015 18:43*

Hello

We had an issue like that too. Where we get out acrylic from they use brown paper for the film on it and it always caught a flame! So we turned down our laser to 30 or sometimes 20 and repeat it about 3 times and no fire. just to be safe. 


---
**Alessandro Milano** *March 02, 2015 09:50*

Hi Bee,

happy for you to prevent the fire.

Now I use a webcam so if I left the machine alone I can continously check the work.

Now I use a more powerful air pump to prevent better.

Enjoy !

A.M.


---
**Bee 3D Gifts** *March 10, 2015 17:56*

Well get this...Ours caught a flame yesterday..melted the belt..destroyed. =(..now we will put a webcam in too..lol


---
**Alessandro Milano** *March 11, 2015 07:59*

I really feel very sorry !


---
**Bee 3D Gifts** *March 13, 2015 01:47*

Thank you...we just got the new one and the tube had water in it..can you believe this? Now we have to return that one...what a hassle.


---
**Alessandro Milano** *March 16, 2015 12:59*

OMG !!

Be patient !!


---
*Imported from [Google+](https://plus.google.com/100341006317202452626/posts/AirKqfDz828) &mdash; content and formatting may not be reliable*
