---
layout: post
title: "Made a wall plaque or desk prop for a friends daughter that's joining a college sorority this semester"
date: October 17, 2017 01:18
category: "Object produced with laser"
author: "Ned Hill"
---
Made a wall plaque or desk prop for a friends daughter that's joining a college sorority this semester. Made from 1/4" Baltic ply and measures ~8x9". Engraved and the engraved areas painted.

![images/c99d787fd74af2debe682279ef9d15ee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c99d787fd74af2debe682279ef9d15ee.jpeg)



**"Ned Hill"**

---
---
**Anthony Bolgar** *October 17, 2017 01:55*

Nice job Ned. Keep sharing your ideas with us!


---
**Ned Hill** *October 17, 2017 02:00*

Lol, the hardest part of this project was finding the right shade of turquoise blue. I had no Idea there were that many shade variants :P


---
**HalfNormal** *October 19, 2017 12:38*

You never end to amaze me on your skills!


---
**Alessandro Sagliano** *November 29, 2017 13:13*

how you have paint?




---
**Ned Hill** *November 29, 2017 21:21*

**+Alessandro Sagliano** I mask the wood before engraving and then paint the engraved areas.


---
**Alessandro Sagliano** *November 30, 2017 23:47*

Thanks for reply, i have 2 question.1)how you have mask? 2)what paintnyou have used?


---
**Ned Hill** *December 01, 2017 00:01*

1.)I use  this transfer tape for masking larger pieces. 

 You can also use wide painters tape if you carefully butt the edges together. 



2.) The paint I typically use is just acrylic crafting paint, usually in a satin finish.



I also typically prefer to seal the surface before masking to help prevent edge bleed when painting.  I also do it because I don't like to put a gloss finish over the paint as it makes it too glossy.  In this case the wood was sealed with a clear lacquer first before masking and engraving.

[signwarehouse.com - RTape Conform 4075RLA Transfer Tape – High Tack – 8.5 in x 100 yds](https://www.signwarehouse.com/p/rtape-conform-4075rla-transfer-tape-high-tack-85-in-x-100-yds)


---
**Alessandro Sagliano** *December 01, 2017 00:26*

Thanks.very thanks


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/au1nS8sTs2t) &mdash; content and formatting may not be reliable*
