---
layout: post
title: "Anyone know how to setup air asist that turns on automaticy , need to find a switched output i can drive a relay from"
date: February 04, 2016 06:03
category: "Modification"
author: "Phillip Conroy"
---
Anyone know how to setup air asist that turns on automaticy , need to find a switched output i can drive a relay from





**"Phillip Conroy"**

---
---
**I Laser** *February 04, 2016 07:30*

My low tech solution is to put it on the same powerboard as the laser lol. :) But my pump is a  240v industrial grade compressor from a Linotype-Hell scanner.



edit: obviously missed the word relay sorry :|


---
**Jim Bennell (PizzaDeluxe)** *February 05, 2016 05:47*

I believe on the M2Nano board that comes with many k40's and maybe on some of the other models of boards there is a few pins that send a 5v signal to turn on a relay for air water etc . 



Heres an Image with a lil explaination.

[http://cdn.instructables.com/F7U/HLR5/IEID6DWF/F7UHLR5IEID6DWF.MEDIUM.jpg](http://cdn.instructables.com/F7U/HLR5/IEID6DWF/F7UHLR5IEID6DWF.MEDIUM.jpg)



I found the pdf on the w3cad site which has had a google virus warning for a lil while now, I proceeded to the site and found this rar'd up pdf with the m2nano board and started translating it on google translate.



(it seems like the main w3cad site id down or missing now but the files are still there to download if you have the link)



I think this is fine I've had no issues or hits with virus scans on my end -here's the link proceed at your own risk.



[http://www.3wcad.com/download/M2Nano%E4%B8%BB%E6%9D%BF%E7%A1%AC%E4%BB%B6%E6%89%8B%E5%86%8C.rar](http://www.3wcad.com/download/M2Nano%E4%B8%BB%E6%9D%BF%E7%A1%AC%E4%BB%B6%E6%89%8B%E5%86%8C.rar)



But you could probably get by with the image. 



My board looks a lil different then the image, mine looks more like the one in the pdf. But either way take a volt meter and check which pins supply a signal . It may not send a signal until the job starts. I'll have to check it out I have a couple spare relays laying around some where. 


---
**Phillip Conroy** *February 05, 2016 08:38*

Thank you


---
**David Richards (djrm)** *February 05, 2016 23:38*

I fed my air pump through a cheap chinese solid state relay and drive its opto coupler input from a transistor which gets turned on by the power percentage signal through a resistor, this goes off and on with the master control on the front panel. it works well enough. I had to put a small capacitor across the load because the ss relay would not turn on properly driving the inductive load of the air pump. hth David.


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/M7yuehBpfMK) &mdash; content and formatting may not be reliable*
