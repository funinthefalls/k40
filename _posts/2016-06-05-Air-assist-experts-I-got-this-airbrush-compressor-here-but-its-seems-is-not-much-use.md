---
layout: post
title: "Air assist experts. I got this airbrush compressor here but it's seems is not much use"
date: June 05, 2016 16:40
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Air assist experts. I got this airbrush compressor here but it's seems is not much use. Once I open the valve it blows all presume inmediatly and 0 psi constant after that. That's on a 1/4 OD tubing. If I use the supplied airbrush pistol it can maintain about 20 psi. Tested at 0 psi it does help with the flames and smoke but nothing more. Should I get something else or is there a hack for this? 

![images/a63b590fa4db408d300e284257f190e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a63b590fa4db408d300e284257f190e0.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *June 05, 2016 16:56*

Stick a metering valve in the hose line. That's all the airbrush is doing (more or less).


---
**Roberto Fernandez** *June 05, 2016 18:02*

places a pneumatic flow regulator , and thereby control the air flow by the outlet nozzle . At the maximum nozzle flow , the pressure is 0, and at 0 flow rate the pressure is 7-8 bar.

something like this: [http://fileserver.ingersollrand.com/images_tfm_complete//F01_l.jpg](http://fileserver.ingersollrand.com/images_tfm_complete//F01_l.jpg)

or the most cheaper like this:

[https://adajusa.es/reguladores-de-caudal/regulador-de-caudal-14-tubo-o6-unidireccional-orientable.html](https://adajusa.es/reguladores-de-caudal/regulador-de-caudal-14-tubo-o6-unidireccional-orientable.html)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/hNNukQuYwG1) &mdash; content and formatting may not be reliable*
