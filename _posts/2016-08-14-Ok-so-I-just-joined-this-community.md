---
layout: post
title: "Ok so I just joined this community"
date: August 14, 2016 14:48
category: "Discussion"
author: "Joey Wheeler"
---
Ok so I just joined this community. I unpacked my CNCShop K40 this weekend and I am having a difficult time getting it to communicate with my computer. I'm running windows 7 (64 bit), and I have installed corel 12, corellaser, and laserdraw. Laser test fires fine. When I plug it in I can tell that the USB is detecting it. When I go to engrave in corellaser I get a prompt saying "engraving machine disconnected." I don't think laserdraw recognizes it at all. I tried to install a driver provided on the disk, but it fails to install (and all the prompts are in chinese so I don't know what I'm doing). I've entered the device ID in the setup (got that from reading here), and all wires appear well connected. HELP!





**"Joey Wheeler"**

---
---
**greg greene** *August 14, 2016 15:09*

you need to add in the serial number of the controller board and the model number to the device initialize portion of the software on Corel and LaserDRW before it will communicate correctly with the computer.


---
**Joey Wheeler** *August 14, 2016 16:10*

I did that


---
**Joey Wheeler** *August 14, 2016 16:26*

What model number would I use, just to confirm?


---
**greg greene** *August 14, 2016 16:58*

It will be on the board - usually a model 52 with some other numbers


---
**Jim Hatch** *August 14, 2016 18:15*

**+greg greene**​​​ The model will be the only entry with an "M2" in it if it's like all the other K40s.



**+Joe Wheeler**​​ I always start troubleshooting with LaserDRW since that doesn't require any special DLLs or add-in configuration for CorelLaser. BTW, you need to set the model & serial number in LaserDRW as well - it won't pick up your settings from CorelLaser.



Check that the PC recognizes that the USB key dongle is connected to it. Then connect the cable form another usb port on the PC and make sure it's firmly inserted into the connector on the board - some of the boards don't line up well with the hole cut in the side of the machine for the cable connector.



Fire up LaserDRW and make sure it authenticates - that's virtually the only English on the splash screen. It should also establish communication with the K40. Then you can try a test engrave using LaserDRW before moving to the Corel install.﻿


---
**Alex Krause** *August 14, 2016 18:28*

Real quick question... did you plug in the USB dongle key that came with the machine I have heard of people finding it floating around the machine under the bed


---
**Alex Krause** *August 14, 2016 18:28*

It looks like a thumbdrive


---
**Scott Marshall** *August 14, 2016 18:31*

The installer file you need to use is buried in a .RAR file. If you install the Corel liason to laserdrw from the .exe it will not work.

That little fact cost me 2 weeks.



Scott


---
**Joey Wheeler** *August 15, 2016 16:11*

Thanks for the replies everyone. I simply could not get it to work on the laptop I purchased specifically for this. I tried everything, donating at least 10 hours. All of the software installed just fine, including coreldraw, the laser plug-in, and laserdraw. The programs would never "see" the laser when I plugged it in. The dongle was plugged in, taken out, and tried in every combination of USB port.



I went and dusted off my old laptop running Vista on one of the first dual core processors in existence and it ran the laser just fine. I had to take virtually every program off the computer so it wouldn't take an hour to boot up and run slower than dial up internet... but I've got a solution now. It works out really... I can just keep that laptop with the laser and use my other laptop for design.


---
*Imported from [Google+](https://plus.google.com/100248825696138022944/posts/DcT6YkUM1m1) &mdash; content and formatting may not be reliable*
