---
layout: post
title: "Can someone with an authentic smoothie board please share their config file that works with the K40 please?"
date: May 19, 2016 20:24
category: "Smoothieboard Modification"
author: "Custom Creations"
---
Can someone with an authentic smoothie board please share their config file that works with the K40 please? I have a 5x and just need a proper config file that works.



TIA!





**"Custom Creations"**

---
---
**Jean-Baptiste Passant** *May 20, 2016 06:08*

I'll send you that once I get home in 11 hours ;)


---
**Alex Hodge** *May 22, 2016 02:15*

Should be the same config file for any smoothie user. "Authentic" board or not.


---
**Custom Creations** *May 22, 2016 02:23*

**+Alex Hodge** I understand that. I am just asking for a file that is setup already for the Smooth-k40. :) A friend of mine has the MKS SBASE board and used the Smoothie file and it did not work properly. He changed the config file to the one from the SBASE website and it worked so there must be a slight difference somewhere..


---
**Jean-Baptiste Passant** *May 22, 2016 14:20*

[https://onedrive.live.com/redir?resid=C9D594B6EA28774A!72048&authkey=!ACOekRwUxups1aQ&ithint=folder%2ctxt](https://onedrive.live.com/redir?resid=C9D594B6EA28774A!72048&authkey=!ACOekRwUxups1aQ&ithint=folder%2ctxt)



Sorry for keeping you waiting.


---
**Custom Creations** *May 22, 2016 14:27*

**+Jean-Baptiste Passant** That just looks like a stock unmodified config file?


---
**Jean-Baptiste Passant** *May 22, 2016 14:50*

Nope, my file, with the switch added.



Unless I uploaded the wrong file but it should be good.



What are you hoping to find that way ?



Almost everything you have to modify depends on your machine (which motor it use etc...)


---
**Custom Creations** *May 22, 2016 14:58*

**+Jean-Baptiste Passant** I am not "looking" for anything specifically. I just noticed that the stepper motors per/mm were same as stock config. I read that it should be set to ~157.48 or something like that. It just looks mostly stock at a quick glance. ;)


---
**Jean-Baptiste Passant** *May 22, 2016 15:03*

I changed the motor to see if there was any enhancements over the stock ones :)


---
**cory brown** *May 23, 2016 20:46*

**+Jean-Baptiste Passant** I'm setting up my Smoothie board as well. Can you tell me what your pin 2.5 and 1.31 are connecting to? are you using a logic level converter on both of these pins?  Are you using the latest edge build? I'm hopping to create an easier to read diagram showing some of the different configurations. Thanks!


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/WZKux1Ai5aa) &mdash; content and formatting may not be reliable*
