---
layout: post
title: "Broken mirror adjustment screw replacement My first mirror was loose on the mount so I removed it to tighten and clean it"
date: November 25, 2017 18:24
category: "Hardware and Laser settings"
author: "HalfNormal"
---
Broken mirror adjustment screw replacement



My first mirror was loose on the mount so I removed it to tighten and clean it. While doing so I bent the brass adjustment screw. Needing one quickly, I went to the local hardware store and purchased some M3 .5 x 20 mm cap head screws. I 3D printed some caps and I am back in business. The new screws are hardened steel so they will not bend!



![images/b027d88e809b571be33575ed54d1339c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b027d88e809b571be33575ed54d1339c.jpeg)



**"HalfNormal"**

---
---
**Paul de Groot** *November 26, 2017 03:48*

Very inventive !


---
**Ron Ginger** *November 26, 2017 13:52*

Has anyone tried to change the layout of these adjustment screws? It seems to me it would be easier  to adjust if the side with the single screw had the screw centered on the mirror. That way it should only change the angle of the mirror, while the two screws on the other side would only change the vertical pitch.



I think I will try this, all it takes  is one tapped hole, and I can always simply put the screw back in the original hole.


---
**Don Kleinschnitz Jr.** *November 26, 2017 15:40*

The adjustment layout on these mounts are pretty typical for low cost mirror adjustments that want these degrees of freedom.

That said the stock K40 implementation is poor and would like to design new ones :).



Some info here:

[http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html?m=1](http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html?m=1)


---
**Ron Ginger** *November 26, 2017 19:46*

I tried moving the single screw until the three screws are about equally spaced around the center of the lens. It seems to work better.



The mounts are poorly made- the detent holes are just drill dimples and the adjusting screw ends are not cut to any cone or ball- they are just whacked off like the end of any screw. The parts are roughly cut, not de-burred, just poor finish.



The bracket holding my back mount had about 6 holes in it- all crooked, not perpendicular to the surface. Clearly drilled by hand at assembly. It was big enough to turn around so I took it out- a real trick to get my hand in there, drilled and tapped new holes and  and  put it back in.



So, after all this it is a bit easier to align, but I still get only about 20-30 watts, as measured by RDworks DoHICkey. Right at the tube I only get 30w.


---
**Don Kleinschnitz Jr.** *November 27, 2017 03:58*

Pretty sure standard 40 watt K40's don't put out 40 watts :(. 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Nba6vL1RpZe) &mdash; content and formatting may not be reliable*
