---
layout: post
title: "Hi K40 owners! I just ordered the Saite head with air assist"
date: May 17, 2016 01:24
category: "Air Assist"
author: "Richard Wallace"
---
Hi K40 owners!

 

I just ordered the Saite head with air assist.  Can anyone tell me the diameter of the air connection?  What's the best tubing to get to connect it?  Will 4mm ID aquarium air tube fit and work well or is it larger/smaller?



Thanks!

![images/1e82878fe4bcdcf79c4960d7d679b78e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e82878fe4bcdcf79c4960d7d679b78e.jpeg)



**"Richard Wallace"**

---
---
**Alex Krause** *May 17, 2016 02:09*

Where did you pick that up from I need to get an air assist head


---
**Anthony Bolgar** *May 17, 2016 02:55*

Lightobjects.com has one for $18.50, but you also need to buy an 18mm lens to work with it, or make an adapter ring to use the stock 12mm lens.


---
**Richard Wallace** *May 17, 2016 02:58*

from here:

[http://www.ebay.com/itm/252303388050](http://www.ebay.com/itm/252303388050)

or here:

[http://www.ebay.com/itm/252273200569](http://www.ebay.com/itm/252273200569) (includes mounting plate with larger hole).



I haven't received mine yet, but it looks like a few people here are using them successfully.  It won't fit into the hole of the standard mounting plate, so you need to increase the hole size of your existing one or buy the kit that includes the new mounting plate.


---
**Richard Wallace** *May 17, 2016 03:00*

you'll also need a 18mm lens.



this one looks a little better than the lightobjects one because the head itself can adjust up or down.


---
**Anthony Bolgar** *May 17, 2016 03:06*

I chose the Lightobjects head because I already had made my z table adjustable, so no need to move the lens up or down. I also weighs a little less, which helps during acceleration/deceleration, improving accuracy (I know, probably very little difference, but engineering best practices says we should always attempt to lighten the moving load in motion control systems.) The LO head also fits the standard mounting plate in a K40. One other option is to use a 3D printed air assist nozzle that fits over the stock laser head. here are a few good designs on Thingiverse.


---
**Alex Krause** *May 17, 2016 03:27*

**+Anthony Bolgar**​ the air assist on LO is sold out till May 20th unless there is more than one on there for the k40


---
**Anthony Bolgar** *May 17, 2016 03:39*

Was not aware of that, but May 20th is only 4 days away :)


---
**Alex Krause** *May 17, 2016 03:41*

It's been on back order for a week been checking every day


---
**Alex Krause** *May 17, 2016 03:47*

**+Anthony Bolgar**​ which grade of lens is the best choice? I see it takes 18 and 20 mm lenses


---
**Anthony Bolgar** *May 17, 2016 03:57*

It uses an 18mm lens ad a 20mm mirror. The lens I chose was [http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx](http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx) for $35.00 I replaced the mirror with a 20mm MO mirror I purchased on ebay.

You could use the stock mirror from the original head, but I recomend replacing all the mirrors with MO ones, you will see a dramatic performance increase.


---
**Jean-Baptiste Passant** *May 17, 2016 06:00*

Got the same one as you **+Richard Wallace** , but I haven't added the air assist tube atm, I'm not at home so I can't measure it :S


---
**Damian Trejtowicz** *May 17, 2016 06:27*

Im using this head and im happy with.you can adjust focus point on it,just move up and down bottom part of the head.

Also im selling mounting plate for it



[https://www.ebay.co.uk/itm/222099691631](https://www.ebay.co.uk/itm/222099691631) 




---
**Phillip Conroy** *May 17, 2016 06:53*

I am using a coiled air hoseon ebay for under5 dollarshttp://[www.ebay.com.au/itm/Black-Recoil-Coiled-Air-brush-Hose-Airbrush-SPRAY-GUN-Compressor-Hose-Part-/131762041570?hash=item1eada132e2:g:CVEAAOSwoudW9Pa-](http://www.ebay.com.au/itm/Black-Recoil-Coiled-Air-brush-Hose-Airbrush-SPRAY-GUN-Compressor-Hose-Part-/131762041570?hash=item1eada132e2:g:CVEAAOSwoudW9Pa-)


---
**Stephane Buisson** *May 17, 2016 09:16*

I removed my coiled hose to replace it with linear hose into drag chain.


{% include youtubePlayer.html id="0r5sdS8zqY0" %}
[https://youtu.be/0r5sdS8zqY0?t=34](https://youtu.be/0r5sdS8zqY0?t=34)


---
**Pigeon FX** *May 17, 2016 23:18*

I just got my one and it takes 4mm ID  tubing on the air assist.....I now just got to work out how to fit it right as I just slapped it in the way I assumed it went in and the hole for the mirror is a good 4mm higher then the stock unit, so I have done something wrong! 


---
**Phillip Conroy** *May 18, 2016 08:22*

Remove the knuled ring ,i had to do same on mine


---
*Imported from [Google+](https://plus.google.com/109253210676013174731/posts/LuP7XJjgyGE) &mdash; content and formatting may not be reliable*
