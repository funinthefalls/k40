---
layout: post
title: "Why does the head move in one direction fast but when your doing a cut takes ages to get to the star position"
date: January 11, 2016 21:54
category: "Discussion"
author: "Tony Schelts"
---
Why does the head move in one direction fast but when your doing a cut

takes ages to get to the star position. Is there a setting somewhere ? 



Thanks





**"Tony Schelts"**

---
---
**Stephane Buisson** *January 11, 2016 21:58*

**+Tony Schelts** are you still stock ? which board ? which soft ? you should give a minimum info if you expect a reply.


---
**Jim Hatch** *January 11, 2016 22:28*

My machine takes an unremarkable (e.g. nothing  I'd even think about being slow) time to get to the home position when turning it on or when clicking the return button (arrow) in LaserDRW. Maybe a second. What board do you have in yours?


---
**Tony Schelts** *January 11, 2016 23:03*

Just the bog standard CorelLaser and laserdrw  M2 board I think.  Its not a problem just irritating it zooms to the start of the back and front position. then moves about 3 mm a second to the right position..  Sorry cant remember which is x and which is Y. 


---
**I Laser** *January 12, 2016 03:55*

When you select engrave/cut in corel there's an option in the settings that sets whether the head returns to zero (can't remember the exact name, next to the release button I think).



I noticed on my machine when this was unchecked it would take ages for the head to get to the correct position. Check it and see if that helps. ;)


---
**Tony Schelts** *January 12, 2016 10:05*

Thanks wll give that a go


---
**I Laser** *January 12, 2016 10:09*

No worries, the checkbox is called "donot back". ;)


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/YMkEoeRmLqr) &mdash; content and formatting may not be reliable*
