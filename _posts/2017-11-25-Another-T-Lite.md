---
layout: post
title: "Another T-Lite"
date: November 25, 2017 17:50
category: "Object produced with laser"
author: "Tony Sobczak"
---
Another T-Lite.



![images/ad2b650458d83a1f6f843c9e91779ea1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad2b650458d83a1f6f843c9e91779ea1.jpeg)
![images/c72c52d586259f651caa40dab36726a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c72c52d586259f651caa40dab36726a8.jpeg)

**"Tony Sobczak"**

---
---
**Anthony Bolgar** *November 25, 2017 18:05*

Nice!




---
**Ned Hill** *November 25, 2017 19:32*

Very nice!


---
**Dushyant Ahuja** *November 25, 2017 19:53*

wow




---
**Abe Fouhy** *November 26, 2017 07:30*

Beautiful work!


---
**Rob Mac** *November 26, 2017 08:28*

Hi Tony, What are you using for the glass?


---
**Tony Sobczak** *November 26, 2017 17:24*

It's not glass but a sheet of vellum.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/A5Ps26nkgNU) &mdash; content and formatting may not be reliable*
