---
layout: post
title: "I have never taken my laser up to the max, I'm sure i heard somewhere that its not good to go over 10ma"
date: November 09, 2015 10:20
category: "External links&#x3a; Blog, forum, etc"
author: "Tony Schelts"
---
I have never taken my laser up to the max, I'm sure i heard somewhere that its not good to go over 10ma. Is this true or false.  I have a chinese 40w as per the picture.





**"Tony Schelts"**

---
---
**Stephane Buisson** *November 09, 2015 10:49*

to try to answer your question, you should understand the work inside the tube

[https://en.wikipedia.org/wiki/Carbon_dioxide_laser](https://en.wikipedia.org/wiki/Carbon_dioxide_laser)

and

[http://www.laserfx.com/FAQ/FAQ3.html](http://www.laserfx.com/FAQ/FAQ3.html)



now the limit in mA max for a specific tube should be in it's spec. Unfortunately many different tube are fitted in K40, (mine have no sticker at all). from different reading over the net, I would say (statisicly) 15 to 18mA should the max for a 40W (but it's some 35w in the wild)


---
**Scott Thorne** *November 11, 2015 01:13*

I've upgraded my tube to a puri 40 watt tube from laser depot...16 to 18 [ma.is](http://ma.is) the max on this tube.


---
**Tony Schelts** *November 11, 2015 06:52*

What is the best vector cutting format?


---
**Scott Thorne** *November 11, 2015 11:54*

I'm not really sure on that one...I'm new to this and I've only had a chance to use mine for about an hour...good question though.


---
**David Cook** *November 12, 2015 16:57*

So is the ma limit strictly related to the cooling capability ?  I'd love to use max power as needed as long as I can keep it cool


---
**Scott Thorne** *November 12, 2015 16:58*

No, it will make the gases inside inert quicker if I'm not mistaken and also shorten the life span of the 90% mirror.


---
**David Cook** *November 12, 2015 16:59*

**+Scott Thorne** I see. That makes sense. Thank you


---
**Scott Thorne** *November 12, 2015 17:13*

 Your welcome man....I'm just learning the basics myself


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/5fMU8nKTmbC) &mdash; content and formatting may not be reliable*
