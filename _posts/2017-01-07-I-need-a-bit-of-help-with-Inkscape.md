---
layout: post
title: "I need a bit of help with Inkscape"
date: January 07, 2017 19:58
category: "Discussion"
author: "3D Laser"
---
I need a bit of help with Inkscape.  I designed a layout for one of my trays but I can't save it as a file that is compatible with laser draw.  When I export the file as a png it's all black and I can't figure out how to save it as a bitmap file  





**"3D Laser"**

---
---
**Cesar Tolentino** *January 07, 2017 20:12*

I believe laser draw can read dxf. And inkscape can export DXF ? And also SVG...


---
**3D Laser** *January 07, 2017 20:19*

**+Cesar Tolentino** I can't get laser draw to open the dxf or svg 


---
**Cesar Tolentino** *January 07, 2017 20:23*

Apology, a friend of mine recently bought a K40 and I know he told me he can open dxf or svg on his with laser draw....



Lets try, wmf.... based on this link... it can open this file type..

[daycounter.com - LaserDrw Tutorial](https://www.daycounter.com/Articles/LaserDrw/LaserDrw-Tutorial.phtml)



PS. I dont have laserdraw.... i used to have moshidraw....



Also checked and open the wmf file... and it is not black like png


---
**Cesar Tolentino** *January 07, 2017 22:45*

Did it work?


---
**3D Laser** *January 07, 2017 23:35*

**+Cesar Tolentino** I haven't tried it yet we had a work function to go to tonight I got it to work kinda by using paint to convert it to bitmap but the low image quality gave me a jagged cut instead of a straight line


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 08, 2017 04:26*

Is the PNG all black when you import it into LaserDraw? I imagine it is saving the background as transparent, so you may want to test putting a white rectangle behind everything in the image to force it to have a white background.


---
**3D Laser** *January 08, 2017 04:30*

**+Yuusuf Sallahuddin** I think you are right l will try that tomorrow 


---
**Vince Lee** *January 10, 2017 01:18*

MoshiDraw added DXF importing about a year ago, but AFAIK, with a nano board and LaserDrw you're stuck with bitmaps only.


---
**3D Laser** *January 10, 2017 01:19*

**+Vince Lee** yet another reason I need to upgrade 


---
**Justin Mitchell** *January 12, 2017 16:00*

You need to save as EMF format from inkscape, and laserdrw will happily load that. this is the workflow we use at our hackspace




---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/NTceinu8MnG) &mdash; content and formatting may not be reliable*
