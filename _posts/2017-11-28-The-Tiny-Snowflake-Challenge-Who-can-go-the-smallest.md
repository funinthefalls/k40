---
layout: post
title: "The Tiny Snowflake Challenge! Who can go the smallest?"
date: November 28, 2017 00:55
category: "Object produced with laser"
author: "Steve Clark"
---
The Tiny Snowflake Challenge!



 Who can go the smallest?



Here is one that I did today it’s 12 mm x 12 mm x .8 mm thick. I used a plastic treated fiber paper (not the best choice) with this one.  I’m going to try other materials later. 



Can you beat this? It’s gotta have closed hole patterns too! Heh…heh… 





![images/661549c1cec9cda5037ecc6bf94b45c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/661549c1cec9cda5037ecc6bf94b45c9.jpeg)
![images/90746d4936704c0902c8bdc33fd64796.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90746d4936704c0902c8bdc33fd64796.jpeg)

**"Steve Clark"**

---
---
**Jeff Lin Lord** *December 09, 2017 18:46*

Actually using the K40 to cut these devices for a medical application.  Smallest hole drawn at 0.1mm and created 0.6mm hole.  ![images/f9dea74876023830f298683390c2a49a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9dea74876023830f298683390c2a49a.jpeg)


---
**Rafael Augusto** *February 15, 2018 21:30*



Hi boy! All right? What configuration did you use to achieve such an effective result? Congrats on the job!






---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/PDVzr7FcPKp) &mdash; content and formatting may not be reliable*
