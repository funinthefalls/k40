---
layout: post
title: "Got my K40 a few days ago from a guy near me"
date: August 13, 2018 18:07
category: "Original software and hardware issues"
author: "Steve Pelland"
---
Got my K40 a few days ago from a guy near me. I set it up and burned a nice logo into some MDF. Next day, I turn it on and it homes Y axis fine but X axis just wants to go right and won't stop.  I have been searching and came up with a guy with the same issue and he just cleaned all the connectors and it worked fine. No dice here.  Any ideas? (before upgrading)







**"Steve Pelland"**

---
---
**Don Kleinschnitz Jr.** *August 13, 2018 19:54*

Sounds like x end stop is always on telling it to move right to get off it. Check end stop connections.


---
**Steve Pelland** *August 14, 2018 13:03*

**+Don Kleinschnitz**  It appears that when powered on, there is ~5v present on that optical sensor when it is interrupted, and 0 when not on the Y axis.... X axis 0v either way. Looks like I am going to try and source an optical sensor here locally

  Thanks for the pointer.


---
**Don Kleinschnitz Jr.** *August 14, 2018 14:34*

**+Steve Pelland** 

Here is an end stop repair post that may help you. Most often the interposer impacts the sensor and damages it. Insure that you readjust the flag that interrupts the sensor.



Note in the post that you may have to change the sensor and resistor for it to work with a new part.



[donsthings.blogspot.com - Repairing K40 Optical Endstops](http://donsthings.blogspot.com/2017/08/repairing-k40-optical-endstops.html)


---
**Steve Pelland** *September 12, 2018 15:13*

So in the midst of the repair, I saw the trace was lifted and in really bad shape.  I decided that mechanical end stops would be better and easier. I wired up a limit switch in the normally open position and gave it a test. Perfect. Now I need longer wires to wire it up.  I'll replace the bad one first and then just replace the other when it goes. Thanks for the help guys. I'll do up a wiring diagram if anyone wants later on.




---
*Imported from [Google+](https://plus.google.com/+StevePelland/posts/Bzzy87QS1XV) &mdash; content and formatting may not be reliable*
