---
layout: post
title: "Spent the past few hours machining an air nozzle for the machine while I work on a completely new head design"
date: August 04, 2016 04:32
category: "Air Assist"
author: "Eric Flynn"
---
Spent the past few hours machining an air nozzle for the machine while I work on a completely new head design.

![images/7e55829cae7fc646ece4abd0790f0ddd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e55829cae7fc646ece4abd0790f0ddd.jpeg)



**"Eric Flynn"**

---
---
**Alex Krause** *August 04, 2016 04:35*

**+Eric Flynn**​ you must adopt me I'm an orphan machinist working at a job that makes production parts in volume so I don't get to play on my turret lathes 


---
**Anthony Bolgar** *August 04, 2016 04:50*

Looks real nice!


---
**Eric Flynn** *August 04, 2016 04:51*

Thanks!!


---
**Alex Krause** *August 04, 2016 04:54*

Did you use your tailstock with a drill bit chucked up to do your air cavity?


---
**Alex Krause** *August 04, 2016 04:57*

Sorry if I haven't said it yet Welcome to the community, glad you joined us and I can't wait to see more upgrades from you... truly an inspiration love the lathe work


---
**Eric Flynn** *August 04, 2016 05:17*

Thanks for the welcome! Yes.  I drilled it initially, then I bored it with a boring bar.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 04, 2016 05:29*

Wow, that's a nice replacement head. :)


---
**Phillip Conroy** *August 04, 2016 08:44*

How is the weight compared to stock or a brought ons as you do not want to add to much mass and overload stepper moters


---
**Eric Flynn** *August 04, 2016 13:15*

I tried to keep the weight down.  Its not too bad.  I wouldnt be worried about overloading the steppers as much as affecting acceleration rates, and possibly inducing some jerkyness on fast direction changes as higher speeds.


---
**Phillip Conroy** *August 04, 2016 15:23*

it looks good what id it made of


---
**Eric Flynn** *August 04, 2016 16:37*

Aluminum , and a stainless steel and brass hose connectiont


---
*Imported from [Google+](https://plus.google.com/107613521854079979112/posts/BfkJ4GUthng) &mdash; content and formatting may not be reliable*
