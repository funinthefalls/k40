---
layout: post
title: "Just finished building a rotary attachment for my K40"
date: March 26, 2016 18:48
category: "Modification"
author: "Anthony Bolgar"
---
Just finished building a rotary attachment for my K40. How do I connect it to my Arduino/Ramps controller, and how do I design files for use with this?





**"Anthony Bolgar"**

---
---
**Ariel Yahni (UniKpty)** *March 26, 2016 19:00*

Also want to know


---
**Anthony Bolgar** *March 26, 2016 19:28*

How do I compensate for diameter of the object? Is there a formula to use, or is it trial and error? I wonder if LaserWeb2 will have a feature to use the rotary attachment on the Z or Extruder axis, with a setting in the laserweb2 software to set everything up :)


---
**Mishko Mishko** *March 26, 2016 19:49*

I don't think there's a need for trial and error. For your setup, there's only one diameter aalong which the point will travel the same length in one step as on the other axis. When you have this figured out, you only have to divide the new diameter with that "parent" diameter to get the ratio for height/width image deformation. ﻿


---
**Mishko Mishko** *March 26, 2016 20:53*

I've just made a simple Excel calculator for this, but I don't know how to attach files here, never used Google+ before. So, if anyone needs it, help;)


---
**Anthony Bolgar** *March 26, 2016 23:27*

Thanks Mishko, that makes sense to me. Easy to figure that ratio out.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2016 02:42*

**+Mishko Mishko** I don't need the excel calculator, but you can use your Google Drive to upload the file, then share the link for it. See [https://support.google.com/drive/answer/2494822?hl=en](https://support.google.com/drive/answer/2494822?hl=en) for info on how to do that.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/JhA4UrfWNaa) &mdash; content and formatting may not be reliable*
