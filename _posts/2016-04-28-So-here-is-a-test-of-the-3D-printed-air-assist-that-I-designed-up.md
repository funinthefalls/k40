---
layout: post
title: "So here is a test of the 3D printed air-assist that I designed up"
date: April 28, 2016 17:58
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So here is a test of the 3D printed air-assist that I designed up. More text on the original post explaining results/tests performed.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



I decided to give the 3D printed Air Assist v1.01 that I designed a test. Even though I had broken parts off it (as the design was flawed, containing a few weak points). I managed to dodgy up a solution to give it a test.



So I tested Vector engraving the word "TEST". In the process, I decided to also test spacing for the rectangles used in the clipping mask. So I created rectangles 0.5mm tall and spaced them apart 0.5mm. Turns out that is too far apart.



Anyway, as you may notice in the video, the smoke seems to disperse left & right (possibly front & back too, due to the design) instead of just all going to the back. So it has left some of the standard smear marks of smoke on the materials I cut, but this time it seems to be more evenly distributed (i.e. instead of all being at the back where the extraction fan drags it out, it seems to be front/back/left/right). This was one of the aims of the air-assist design, so it seems it functions as intended.



I did the vector engrave on 3 different materials in this test. 1st (on the left) was on 3mm MDF. 2nd (in middle) was on 4-5mm natural vegetable tanned leather. 3rd (on the right) was on 3mm birch plywood.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2016 17:59*

So, there are also some photos of the results on each material. I will link them here:



3mm MDF:

[https://drive.google.com/file/d/0Bzi2h1k_udXwRURyZ0VFVHRZX0E/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwRURyZ0VFVHRZX0E/view?usp=sharing)



4-5mm Natural Vegetable Tanned Leather:

[https://drive.google.com/file/d/0Bzi2h1k_udXwSmVaX1NOeWJRcVk/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwSmVaX1NOeWJRcVk/view?usp=sharing)



3mm Birch Plywood:

[https://drive.google.com/file/d/0Bzi2h1k_udXwQWRuZUt0S1d3SDQ/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwQWRuZUt0S1d3SDQ/view?usp=sharing)


---
**ThantiK** *April 28, 2016 20:18*

Oh my god that horrible noise.  You're not arcing to the frame or anything are you?


---
**Dennis Fuente** *April 28, 2016 21:35*

wow listening to the video it sure sounds like your tube is arching badly thats what i added to my response in the lens change post


---
**Scott Thorne** *April 28, 2016 23:27*

**+Yuusuf Sallahuddin**...you are the man...I bow my head to you sir....great work coming from you...it's always a pleasure to see your post! 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 02:32*

**+ThantiK** **+Dennis Fuente** I am unsure what noise you are referring to. Is it the scratching kind of sound that you can hear when the laser is firing? or the noise that you can hear right from the start (humming/droning noise)?



The humming/droning is my air assist (as I am using an aquarium pump & it's quite noisy). The scratching sound when the laser is firing only happens when my laser beam makes contact with an object. I actually am not certain what causes it, but I am fairly certain there are no arcs inside the case (although I could verify by putting another webcam in where the tube is to watch what is going on in there & maybe another in where the power supply/controller board are). I assume if it is arcing it would short the house or cause some sort of electrical issue wouldn't it? At least the k40 housing would be charged wouldn't it?



edit: PS. you've got me all concerned now lol.



**+Scott Thorne** Thanks Scott. I can't wait until I design up a mount to hold the webcam at an angle so it can run be attached to the laser head & get it 3D printed. Then I'll be able to see POV of the laser beam (or close enough). I've pulled the webcam apart & it's internal components are very small & light, so perfect to attach straight to the laser head. I've also found another old webcam of mine (in storage) & will put that where the current cam is, so I continue to get this perspective + a POV.


---
**I Laser** *April 29, 2016 09:06*

I think they're pointing to the fuzzy sound when the laser fires, which does sound like electrical arcing. Though it could just be your microphone is sensitive to a particular frequency accentuating the burning sound.



Very nice work, though I would have to wonder whether introducing turbulence is beneficial or if it would be better to have the nozzles angled so they push the smoke back toward the extraction fan. Would make life harder for lighter material, but pushing the smoke out might reduce staining.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 10:33*

**+I Laser** Not just the microphone being sensitive, because I can hear it with my ears also. I placed another webcam in with the tube & watched it while doing some work for 10 minutes or so & not a single arc. I have a feeling it is a noise coming from the material getting zapped because if I fire with no material directly through one of the holes in my cutting bed, no noise. But if I fire at material, it is kind of random when it makes that noise. Sometimes does, sometimes doesn't.



All of the nozzles are currently angled to hit the focal point, so some do point backwards towards the extraction fan. But then again they all point in to the centre. It would be interesting to test a bar with nozzles angled backwards like you suggest. I had a thought of using a vacuum instead of blowing air would be an option. Have a vacuum attached on a tube running on the laser head & suck the smoke directly into that, vent it outside the house. I pulled apart one of my airpumps & tried to reverse the mechanism, but it was too weak to do much. I don't have a vacuum to test with & I imagine it would be noisy too, unless you could get a very small vacuum (like those usb powered ones for pc keyboards).



I tend to always use magnets on my materials anyway, as most materials either bow slightly or lighter stuff tends to not want to sit flat (e.g. fabric). So I just magnet the corners to hold it taut while cutting/engraving.


---
**Scott Thorne** *April 29, 2016 11:06*

**+Yuusuf Sallahuddin**...it's the material getting vaporized...mine does the same thing...the microphone just makes yours more noticeable.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 11:09*

**+Scott Thorne** Cool, thought it was something like that. I've had it happen quite a lot. I wasn't concerned until someone mentioned that it sounds like arcing. Then I started stressing & put a cam in with the tube to check.


---
**Ben Walker** *April 29, 2016 14:55*

phew!  I hear that sound sometimes myself.  Sometimes it starts in the middle of the job.  sometimes stops in the middle of the job.  Most of the time that buzzing does not present at all.  I will only be using the K40 a short time regardless as I have an order for a slightly more robust machine on preorder.  Maybe I should double my grounding measures.


---
**Don Kleinschnitz Jr.** *April 29, 2016 22:14*

FYI this is the camera that I am planning on using to watch my machine: it works with my droid tablet using an app called CameraFI, which btw claims to work with any usb web cam.

[http://www.amazon.com/DBPOWER-Waterproof-Borescope-Endoscope-Inspection/dp/B0158DJ20W?ie=UTF8&psc=1&redirect=true&ref_=oh_aui_detailpage_o08_s02](http://www.amazon.com/DBPOWER-Waterproof-Borescope-Endoscope-Inspection/dp/B0158DJ20W?ie=UTF8&psc=1&redirect=true&ref_=oh_aui_detailpage_o08_s02)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 00:07*

**+Don Kleinschnitz** Thanks for that. It does seem decent in it's small size and a lot of those borescopes tend to come with built-in led lighting. Unfortunately lower resolutions than I'd prefer, but I guess for the price range it's pretty good.


---
**I Laser** *April 30, 2016 03:16*

**+Yuusuf Sallahuddin** That vacuum on the back of the head is either crazy or genius. Maybe a bit of both lol :)



If the suction was strong enough it would do away with the air assist and extraction fan in one hit!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 03:55*

**+I Laser** Or even Crazy Genius, like a mad scientist muwahaha. I think it's a possible option for extracting the smoke directly away from the cut. Just need some kind of vacuum that can be funnelled down to a small tube that can attach to the back of the head.


---
**Don Kleinschnitz Jr.** *April 30, 2016 04:12*

 I ended up with LO air assist and a 450 CFM fan with 6" pipe and the cutting is very good. The fan is so powerful it actually lifts some material :) as I sealed up the cabinet it just evacuates the whole cabinet.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 04:35*

**+Don Kleinschnitz** Could you funnel the fan to extract from below the work piece, then it would act like a vacuum holding your work piece in place maybe?


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/6uEjrfHs3RK) &mdash; content and formatting may not be reliable*
