---
layout: post
title: "This is going to be a stupid question, but made a square in CorelDRAW and rounded the corners, when I try and cut it out will only cut the rounded edges and not the straight part of the lines"
date: April 02, 2017 22:23
category: "Discussion"
author: "Robert Selvey"
---
This is going to be a stupid question, but made a square in CorelDRAW and rounded the corners, when I try and cut it out will only cut the rounded edges and not the straight part of the lines. So I made just a square and I hit start to cut and the process window shows that it's done but it never cuts. If I make a circle it will cut it, just for some reason, it will not cut a square anyone know why this is happening?





**"Robert Selvey"**

---
---
**Ned Hill** *April 02, 2017 22:32*

That's not a stupid question.  It's actually just plain weird.  Do the squares look fine in the preview?


---
**Robert Selvey** *April 02, 2017 22:35*

**+Ned Hill** The one with the rounded corners the straight parts are like faded. But the preview for the regular square looks normal the windows at the bottom pops up and says finished but it never cuts it out.. If you change it to a circle it will cut it out fine.




---
**Ned Hill** *April 02, 2017 22:45*

Not really sure what's going on there.  If you have the cut output file set to EMF try switching back to WMF and see if that makes a difference.


---
**Robert Selvey** *April 02, 2017 22:47*

Did that and tried plotter also, same results. only does it making a square all other shapes are fine.


---
**Ned Hill** *April 02, 2017 22:49*

How did you create the square in CD?  Did you use the Rectangle tool?


---
**Robert Selvey** *April 02, 2017 23:02*

Yes




---
**Jim Hatch** *April 03, 2017 02:30*

**+Robert Selvey** The fact that the straight lines are faded makes me think that there's an issue with the color/stroke setting. Try making it a black stroke.


---
**Robert Selvey** *April 09, 2017 19:46*

Still can not figure out why it won't cut out squares




---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/29TQhWT2NBA) &mdash; content and formatting may not be reliable*
