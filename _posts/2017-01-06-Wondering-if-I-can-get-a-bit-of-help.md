---
layout: post
title: "Wondering if I can get a bit of help"
date: January 06, 2017 03:52
category: "Discussion"
author: "3D Laser"
---
Wondering if I can get a bit of help.  I am one of those people that still uses laserdrw (hoping up upgrade later this year). But I have someone who wants me to make an x wing template holder.  They have a free board game geek svg file online but I can't get it to open in laserdrw so I can place the shapes to fit the size of my other card trays.  Any help would great





**"3D Laser"**

---
---
**Joe Keneally** *January 06, 2017 04:35*

Pass the template and I would be happy to tinker with it. 


---
**Jim Hatch** *January 06, 2017 13:51*

Also, download Inkscape. It's open source design. Same capabilities as AI and Corel so you can read in files that you need to convert for LaserDRW. I use Corel X8 for my lasers but teach using Inkscape and they do pretty much the same stuff, just different names for things. Since it's free I know all my students can get it and I don't have to worry about translating between AI, Corel, Affinity, etc during class.


---
**3D Laser** *January 06, 2017 13:53*

**+Jim Hatch** what file format would I have to convert it to for laserdrw


---
**Jim Hatch** *January 06, 2017 13:59*

BMP will do. There are a few filetyoes it can open. I don't use it much because the CorelLaser plugin works for me so I do my K40 work in Corel and send it straight to the laser.


---
**3D Laser** *January 06, 2017 14:00*

**+Jim Hatch** I wanted to try the coral laser but wasn't for sure how to set if up


---
**Jim Hatch** *January 06, 2017 14:05*

I installed Corel then ran the install for CorelLaser. The plugin tool bar showed up on the Corel toolbar. Just start CorelLaser and not Corel itself when using it.


---
**3D Laser** *January 06, 2017 14:35*

**+Joe Keneally** [thingiverse.com - Star Wars X-Wing Movement Templates by helagak](http://www.thingiverse.com/thing:1268628) here is a thing a verse file for it but I think it's for a printer.  Board game geek has svg file but I can't get it to copy and paste from my phone or 


---
**3D Laser** *January 06, 2017 14:36*

**+Joe Keneally** if possible I need them to fit in a 311x186mm space thanks Joe


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/aZkkSRGjw3L) &mdash; content and formatting may not be reliable*
