---
layout: post
title: "Did some engraving tests to see how detailed I could engrave"
date: February 14, 2017 21:13
category: "Object produced with laser"
author: "Paul de Groot"
---
Did some engraving tests to see how detailed I could engrave. So far not too bad.



![images/d888e78bbbe5abc9643f6dff175e2297.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d888e78bbbe5abc9643f6dff175e2297.jpeg)
![images/ac9d15c6bfaffe9d7aef4645a201190c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ac9d15c6bfaffe9d7aef4645a201190c.jpeg)
![images/c0e386543c0eb8650e5113e7781a7660.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c0e386543c0eb8650e5113e7781a7660.jpeg)
![images/23d26977c378d30a014d6997ce18528a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/23d26977c378d30a014d6997ce18528a.jpeg)
![images/4c2ef1483bd262bf130abb613115811b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c2ef1483bd262bf130abb613115811b.jpeg)
![images/76e34757a84ebca253f8d481dd392c45.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/76e34757a84ebca253f8d481dd392c45.jpeg)
![images/5159828d6d999301b3fdccd56d8894bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5159828d6d999301b3fdccd56d8894bd.jpeg)

**"Paul de Groot"**

---
---
**Kostas Filosofou** *February 14, 2017 22:12*

Nice! Can you share your settings please? I have try to engrave some logos also but with no so good results..


---
**Paul de Groot** *February 14, 2017 22:21*

**+Konstantinos Filosofou** i use a bespoke controller, an arduino uno r4 with 10bits engraving. See [awesome.tech - Science experiements and kits for makers and hobbyists](http://awesome.tech) for some details. Settings 380dpi speed 600mm/min yes it's slow. 


---
**takitus ᵗᵃᵏᶦᵗᵘˢ** *March 08, 2017 19:34*

thats pretty good man! looks like you might be able to get even more out of it if you adjust your focus a little


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/7nw2h4y58NB) &mdash; content and formatting may not be reliable*
