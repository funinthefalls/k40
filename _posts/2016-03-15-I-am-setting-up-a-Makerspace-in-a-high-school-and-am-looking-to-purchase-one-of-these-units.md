---
layout: post
title: "I am setting up a Makerspace in a high school and am looking to purchase one of these units"
date: March 15, 2016 12:22
category: "Discussion"
author: "Robert Curtis"
---
I am setting up a Makerspace in a high school and am looking to purchase one of these units.  Any recommendations on where/who I should purchase one of these units?





**"Robert Curtis"**

---
---
**3D Laser** *March 15, 2016 12:32*

I got mine from Vipshopforyou the eBay seller while I had a few issues with mine they where quick to offer help and a partial refund 


---
**Tony Sobczak** *March 15, 2016 17:01*

Globalfreeshipping.   They provide a good quality device and their customer support is excellent. 


---
**Scott Marshall** *March 15, 2016 18:00*

+1 on GFS, I bought mine there, and had issues, which they handled well.

 From the word "on the street" they are out of stock right now.



I don't know as I'd recommend a K40 for a makerspace, especially a HS one. 

They are an experimenter class machine, basically a pre-assembled kit. They have no safetys, interlocks or even door latches. There are hazards from exposed  laser energy, very high voltage and moving parts. They have no UL (or any other) approval. In addition, the software supplied with it is glitchy and of questionable legality.



There are no interlocks to prevent you from destroying the laser tube by forgetting to turn on the water pump or overdriving the tube. I personally know of 3 people here who destroyed the laser tube because they forgot to turn on the pump. I've come close a few times myself.

The water pump is just barely adequate, and the exhaust is totally  inadequate for a public (or High School Shop) setting.



The "instructions" are 2 pages of useless babble. You are going to depend on other users for any help getting it running. 



This community is great, loaded with knowlegable users, and will probably get you through most pitfalls.



All of these issues (except the UL approval) CAN be remedied by someone with industrial machinery expertise, but at a cost. Done up properly, with interlocks and safety equipment installed, the machine probably would work OK for a makerspace where people of legal age signed off on a safety waiver.



Don't get me wrong, the K40 is an incredible machine FOR THE PRICE, and I love it, but it's strictly an experimenters device.



Scott


---
**Robert Curtis** *March 15, 2016 19:22*

Thanks Scott and everyone for your help.  it is much appreciated as I am new to this laser cutting stuff!.  That is great info.  My difficulty is that the grant I am applying for is limited to $1000.  The Epilog machines I was looking at are really expensive and big - I was trying to find something smaller and cheaper.  Any thoughts on a different unit that you could recommend?


---
**Scott Marshall** *March 15, 2016 22:48*

Boss Lasers has one for under a $1000, but it's short accessories. I can't vouch for them at all, I just happened on them the other day. There's another one they've been advertising lately, can't remember the name right now, but I think they are in the $800 range, but they are pretty low power.



Your budget is pretty slim for a ready to use laser, that's why most of us here are buying the K40 and modifying them. By the time most of us are done, I'd bet we are over a grand in them, but we get to do it a bit at a time, and enjoy the work. Plus if we laser our hand, or blow up a $300 tube, it's only on us.



You might consider a CNC router, they're probably a little more capable than most smaller lasers, and realitivily user friendly. At least you won't get burned by an invisible beam. the smaller ones carry a Dremel and you're within "Power tool protocol" for high schoolers.



Look to "Openbuilds, CNCzone, and Sawmill Creek" forums for info on the routers. You may be able to get a kit and have a class build or something like that. That's as much an education as the use of the tool.



It's mostly bolt together, and can be run with a garage sale computer on XP. Wiring is almost all low voltage or plug in. (No 15,000 volt wiring).



If there's anything I can do to help get you running, let me know. I love the tech in schools programs (my 2 sons just started college)



Good luck!



Scott﻿



[https://www.bosslaser.com/boss-ls-1416.html](https://www.bosslaser.com/boss-ls-1416.html)



[http://openbuilds.com/](http://openbuilds.com/) (have parts kits as well)



[http://www.sawmillcreek.org/forum.php](http://www.sawmillcreek.org/forum.php)



[http://www.cnczone.com/](http://www.cnczone.com/)


---
**Robert Curtis** *March 16, 2016 00:37*

Thanks.  I looked at the bosslaser but it seemed the cheapest was around $4k.  Seems like no decent laser cutter is going to be in the under $1000 price range.


---
**Alex Krause** *March 16, 2016 04:54*

Like **+Scott Marshall**​ said a small router/mill might be better for the inexperienced and price range you are looking at **+Mark Carew**​ over at open builds has a cbeam kit that needs some electronics/spindle purchased but is a rock solid machine that can have some Lexan guards installed easily to offer the safety needed for your application 


---
**Robert Curtis** *March 16, 2016 12:08*

Another question - it looks like the software that comes with these sub $1000 laser cutters isn't good - does it even work with Windows 7?


---
**Scott Thorne** *March 16, 2016 13:42*

**+Robert Curtis**...I bought the shennui sh-350 50 watt engraver on amazon....dsp controller 20×12 workspace...

.1800.00 I love it...it works wIth windows 10,8 and Vista too 


---
**Scott Marshall** *March 16, 2016 17:24*

Scott has about the best deal I've seen if the Shennui is complete for $1800. A fund raiser or raffle would bring that in range.



Yes, the software that comes with the K40 is Windows 7 compatible. There are 2 programs included, one is an obsolete version of CorelDraw called "Corelaser 2013.02". And there's a VERY rudimentary program called LaserDRW  that only allows only basic shape and text drawing, but transfers in bitmap files. it saves in it's own format which isn't compatible with anything. Both pieces of software are activated by a USB Key (included) and the serial number is the same for all users. There is no support through Corel, and no one seems to know if it's being distributed under license or not.



It gets the job done, but isn't real user friendly. It also has no instructions, for use or installation, and is challenging to even read, as most of the documentation is in Chinese only, and it's installer is buried in a .Rar file which requires a translator to open, and as I found out, not all .rar readers detect the install "suite".

Once installed, it's all english, but you have to experiment to work out how to use it. It's probably easier for previous Corel Draw users.



All in all, it's about like the machine, you can make it work, but it's not ready for prime time.



Scott


---
**Scott Thorne** *March 16, 2016 20:55*

**+Scott Marshall** hit the nail on the head...it's all about the machine and what you intend to do with it...I only bought the sh-350 because I needed the bigger workspace for my real job...the company pays me to draw up pieces and cut them...it will pay itself off...plus I like the laserworks program...you can do Lots of neat things with it. 


---
**Scott Thorne** *March 17, 2016 10:26*

[http://www.amazon.com/gp/aw/d/B01CU5RMLW/ref=mp_s_a_1_fkmr2_2?qid=1458210267&sr=8-2-fkmr2&pi=AC_SX236_SY340_FMwebp_QL65&keywords=50+watt+laser+cutter#](http://www.amazon.com/gp/aw/d/B01CU5RMLW/ref=mp_s_a_1_fkmr2_2?qid=1458210267&sr=8-2-fkmr2&pi=AC_SX236_SY340_FMwebp_QL65&keywords=50+watt+laser+cutter#)


---
**Robert Curtis** *March 18, 2016 12:10*

Thanks so much for the help and advice!


---
*Imported from [Google+](https://plus.google.com/102533765013675564454/posts/YeQzrfpbcs7) &mdash; content and formatting may not be reliable*
