---
layout: post
title: "Does anyone know how to get the LaserDRW plugin installed into Corel Draw 12?"
date: March 15, 2015 15:46
category: "Software"
author: "Seth Mott"
---
Does anyone know how to get the LaserDRW plugin installed into Corel Draw 12?  I am able to cut/engrave directly from LaserDRW, but this doesn't do me any good unless I want to create in LaserDRW ... which I don't!





**"Seth Mott"**

---
---
**Bill Parker** *March 15, 2015 15:59*

Sorry no as mine is moshidraw.


---
**Jim Coogan** *March 15, 2015 17:07*

If you go to the Light Objects forum ([http://www.lightobject.info/](http://www.lightobject.info/)) they might be able to help you.  My understanding is that the plug-in only works with certain versions of Corel Draw.  I use Laser Draw all the time right now until I upgrade and then I will have Laser CAD.  Laser Draw lets you pull in just about any graphic created in most graphics program.  I do work in my CAD program or PhotoShop and export the files as BMP or one of the other files it supports and have zero problems.  I will admit that LaserDraw is one royal pain and I wish someone would write plug-ins for other packages. 


---
**Dan Shepherd** *March 15, 2015 22:03*

Have you ran the CorelLaser driver setup ?  That is the only way that I could get mine to load. Run CorelLaser\drivers\setup.exe    If you can't find that file on disk.  I will upload it to the Laser Engraving and Cutting Facebook group    [https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
**Seth Mott** *March 16, 2015 12:49*

Yeah, that was it.  I had already installed Corel Laser, but when I opened it before, nothing happened. Turns out, if you already have LaserDRW open, Corel Laser will not open.  Once I closed LaserDRW, Corel Laser opened and it had the docked Laser toolbar in the upper right corner.  Works pretty well!


---
**Andrew Cilia** *December 23, 2015 13:50*

Could you please upload CorelLASER.exe somewhere I can get it? The CD in my machine was scratched and I can't read it.


---
*Imported from [Google+](https://plus.google.com/108197211464469447407/posts/BPjxvNaXcAF) &mdash; content and formatting may not be reliable*
