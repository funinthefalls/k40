---
layout: post
title: "Is it a cube or shall i not glue one side and make it a potpourri box whats your thoughts?"
date: June 05, 2016 14:30
category: "Repository and designs"
author: "Tony Schelts"
---
Is it a cube or shall i not glue one side and make it a potpourri box whats your thoughts?



![images/3ad38eacdfd7b14d58fe10ad9bb29fdf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ad38eacdfd7b14d58fe10ad9bb29fdf.jpeg)
![images/517f6887213fd45ed9e0dc0d0e0389c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/517f6887213fd45ed9e0dc0d0e0389c5.jpeg)
![images/94f8cfe3bdc48e8e1bbac414aaf9cef4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/94f8cfe3bdc48e8e1bbac414aaf9cef4.jpeg)
![images/d1c2ecc721a86e062a275606f8925271.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1c2ecc721a86e062a275606f8925271.jpeg)

**"Tony Schelts"**

---
---
**Scott Thorne** *June 05, 2016 16:08*

Nice...potpourri box...looks good man. 


---
**Jim Hatch** *June 05, 2016 16:19*

I'd not glue one side and let it be used as a potpourri box (or with the top off, a votive candles light). If you glue the top it's only a knicknack. If you leave the top unglued it can still be a knicknack cube but also other things.


---
**Jim Hatch** *June 05, 2016 16:19*

Very nice work by the way.


---
**Tony Schelts** *June 05, 2016 17:20*

My thought also, Thats decided keep one side lose.  Gonna design some other sides so people can choose different options.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 20:44*

I agree with the others. Nice work & keep the top unglued for adding potpourri. Might be worthwhile adding a finger hole or something to aid opening it if you are planning to sell these.


---
**Tony Schelts** *June 05, 2016 21:10*

Good Idea, need to decide which face to use, I don't want to kill the elephant. Probably butterfly.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 22:29*

**+Tony Schelts** The butterfly face looks suitable if you put it in the top centre. Unfortunately, 1 face will always be on the bottom.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/26zG9E15kQy) &mdash; content and formatting may not be reliable*
