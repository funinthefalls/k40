---
layout: post
title: "Having some slight problems when engraving something then cutting it out"
date: December 24, 2016 23:46
category: "Discussion"
author: "tyler hallberg"
---
Having some slight problems when engraving something then cutting it out. Things wont be lined up and will cut through the engraving. I am using corelDRAW x8 as of right now, my smoothie board is on order so hopefully wont be a problem for long. Any ideas how to get this to work short of making a box around everything and using it for an outline for engraving and cutting so its all sitting in the right spot? Thanks!!





**"tyler hallberg"**

---
---
**Jim Hatch** *December 25, 2016 00:17*

Do it as 2 layers. Then create a registration mark (or box) that you don't actually engrave/cut (make it a 0 stroke).


---
**Ned Hill** *December 25, 2016 03:45*

**+Tyler Hallberg** I create a 1x1mm box, with no line color and no fill, and stick it in the top left corner of the work space.  You then select what you want to engrave plus the box and engrave.  Then do the same with cutting.  


---
**Ian C** *December 25, 2016 19:22*

Hey. I used to create an alignment layer in Corel then copy and paste the designs cut outline on this layer also. Once there I would reduce the line type to none, so it was invisible. Now for every layer I sent to the machine, be it cut or engrave, I would also select this invisible line so the software had a constant alignment reference from layer to layer. Worked well for me and avoided having to mess around with a single pixel or box, which I found tedious to use


---
**Jim Hatch** *December 26, 2016 15:01*

**+Ian C**​ Good approach. I don't do the single pixel method either. If I can't see it then I don't think about it. I like something large enough to be visible. 🙂 My box is the size of my material. That way if I forget to turn it off it engraves on the bed 😀 But a mark or small box is simple which is a good start for someone new to this.


---
**Ian C** *December 28, 2016 15:58*

**+Jim Hatch** Yeah it's what works best for the individual at the end of the day eh. I might not have done the single pixel trick quite right, but then got the outline idea right so stuck with it


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/8AnUsqe9yuk) &mdash; content and formatting may not be reliable*
