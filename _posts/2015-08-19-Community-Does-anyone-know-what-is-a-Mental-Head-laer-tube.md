---
layout: post
title: "Community, Does anyone know what is a \"Mental-Head\" laer tube?"
date: August 19, 2015 16:07
category: "Discussion"
author: "Fadi Kahhaleh"
---
Community,

Does anyone know what is a "Mental-Head" laer tube?

and how it is different than a normal head glass tube?



I fear the "Mental" is a typo for "Metal" but much of the far-east manufacturers use "Mental" not sure why!



here is a picture:

[http://i00.i.aliimg.com/img/pb/965/956/460/460956965_309.jpg](http://i00.i.aliimg.com/img/pb/965/956/460/460956965_309.jpg)



any feedback on how is this good or bad...etc would be appreciated.



F.K.





**"Fadi Kahhaleh"**

---
---
**Jon Bruno** *August 19, 2015 16:20*

That image is of a laser tube similar to a RECI tube. They are produced differently than an entirely glass envelope tube. As for the quality differences I do not know. I do know however that RECI tubes have different power requirements than an all glass tube of similar dimensions and RECI tubes reportedly can last quite a bit longer. But my help into the "this versus that comparison" unfortunately ends here.


---
**Fadi Kahhaleh** *August 19, 2015 16:34*

Thanks **+Jon Bruno** 

At least I know from your comment about RECI it is not a cost-cutting design. So that is a good indication.


---
**William Steele** *August 19, 2015 19:17*

The metal end also eliminates one of the major failure points of the all glass designs... the water cooled jacket that often fails (by falling off) of them.  I've never actually worn out one of these RECI tubes and I have one with over 5000 hours on it!  The all glass (cheap) tubes normally fail at around 1000-1500 hours.


---
**Fadi Kahhaleh** *August 19, 2015 23:28*

Thanks for the info **+William Steele** 

Looks like it is the way to go.


---
**Flash Laser** *August 20, 2015 00:54*

**+Fadi Kahhaleh** Hi, this is coco . this "Mental-Head" laser tube's lifetime is longer than normal head glass tube, ofcause it cost more. hope it is helpful.


---
**Jon Bruno** *August 20, 2015 15:11*

BTW, I believe it's "Metal" not "Mental".. Seeing it written over and over again is making me feel a little "mental". 


---
*Imported from [Google+](https://plus.google.com/115068648817066933731/posts/bYDzhA1EviH) &mdash; content and formatting may not be reliable*
