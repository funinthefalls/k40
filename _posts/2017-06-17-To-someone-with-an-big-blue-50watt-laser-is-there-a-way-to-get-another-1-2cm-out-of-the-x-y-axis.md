---
layout: post
title: "To someone with an big blue 50watt laser is there a way to get another 1-2cm out of the x y axis"
date: June 17, 2017 00:41
category: "Discussion"
author: "3D Laser"
---
To someone with an big blue 50watt laser is there a way to get another 1-2cm out of the x y axis 





**"3D Laser"**

---
---
**1981therealfury** *June 17, 2017 23:55*

Not without removing the limit switches, my full travel from end to end on the limit switches is 498mm x 301mm so only way for me to get any more would be to remove them, but they are very close to the end of the run anyway so at least on my K50 i don't think there is much to be gained. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/GoPhPsHYy9f) &mdash; content and formatting may not be reliable*
