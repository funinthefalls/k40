---
layout: post
title: "Been struggling for a few weeks with my K40 that I purchased locally off of Craigslist"
date: March 01, 2016 17:26
category: "Discussion"
author: "Kelly Burns"
---
Been struggling for a few weeks with my K40 that I purchased locally off of Craigslist.  It came with the Light Objects Air Assist head, but stock Mirrors and Lens.  I bit the bullet and purchased upgraded Lens and Mirrors, from Light Ojbects.  Installed last night and did a quick alignment.  WOW! I can't get over the difference.  Going to keep playing with it and working on an adjustable Z-Table.



Now onto Ramps/LaserWeb upgrade.  





**"Kelly Burns"**

---
---
**Joe Keneally** *March 01, 2016 17:44*

So just upgrading the mirrors and lens made a BIG difference?


---
**Joe Spanier** *March 01, 2016 17:53*

Huge


---
**Kelly Burns** *March 01, 2016 17:56*

Yes.  In short, it made a gigantic difference.  I'm not saying that everyone needs an upgrade.  Clearly some stock K40s do pretty well.  Mirrors seemed to be a better quality, but the Lens seemed to be FAR SUPERIOR.  



I think the Light object lens and air assist head alone would be a huge upgrade and well worth it.  



For comparison with what others are getting, I'm going to do cutting and engraving tests on various materials tonight. I'll post results.  


---
**Joe Keneally** *March 01, 2016 17:58*

I look forward to seeing your results!


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/GZucJK5hosG) &mdash; content and formatting may not be reliable*
