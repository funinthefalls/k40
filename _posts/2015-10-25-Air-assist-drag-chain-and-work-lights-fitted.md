---
layout: post
title: "Air assist, drag chain and work lights fitted"
date: October 25, 2015 18:40
category: "Modification"
author: "Phil Willis"
---
Air assist, drag chain and work lights fitted.


**Video content missing for image https://lh3.googleusercontent.com/-OJ77wraMsfc/Vi0iF8qXsnI/AAAAAAAAWUE/0HrKvueKg4o/s0/gplus1717042548.mp4.gif**
![images/c497b2d086d28c368c29ee415b742493.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/c497b2d086d28c368c29ee415b742493.gif)



**"Phil Willis"**

---
---
**Phil Willis** *October 26, 2015 09:00*

**+Allready Gone** A separate 12V power supply that will also power the red laser pointer(s) on the head via a DC to DC converter.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2015 09:03*

Where do you get the drag chain thing? Looks nice & tidy.


---
**Phil Willis** *October 26, 2015 09:11*

**+Yuusuf Salahuddin** Its on thingiverse. A combination of these:



[http://www.thingiverse.com/thing:890705](http://www.thingiverse.com/thing:890705)

[http://www.thingiverse.com/thing:34661](http://www.thingiverse.com/thing:34661)

[http://www.thingiverse.com/thing:993505](http://www.thingiverse.com/thing:993505)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2015 09:15*

**+Phil Willis** Oh so they're custom 3d printed for the purpose. That's cool. Unfortunately I don't have a 3d printer (yet!).


---
**Paul Haban** *October 26, 2015 17:21*

I added a drag chain to mine, but when the head goes to the top right, the chain curls towards the Y axis / Bottom instead of staying straight. When it does this, it blocks the laser. Any ideas?


---
**Stephane Buisson** *October 26, 2015 18:12*

**+Yuusuf Salahuddin** drag chain

[http://www.ebay.co.uk/itm/10-x-20mm-1M-Open-On-Both-Side-Plastic-Towline-Cable-Drag-Chain-st-/201311188600?hash=item2edf14f678:g:j-QAAOSw34FVCdUe](http://www.ebay.co.uk/itm/10-x-20mm-1M-Open-On-Both-Side-Plastic-Towline-Cable-Drag-Chain-st-/201311188600?hash=item2edf14f678:g:j-QAAOSw34FVCdUe)
 cheaper than 3D print yourself

**+Paul Haban** i did fixed it the other way around.

the fixed point is near the laser hole output, so the chain curve is behind the head, never in the path.


---
**Phil Willis** *October 26, 2015 22:38*

**+Paul Haban** The chain should have a direction it will not bend in and that should "face" the front of the laser. Mine is mounted so its already a fair way from the laser but it physically can't bend in that direction.


---
**Phil Willis** *October 26, 2015 22:45*

**+Stephane Buisson** I was already printing the ends so printing the whole chain was easier and about the same price as buying one.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 27, 2015 01:20*

**+Stephane Buisson** Thanks Stephane. I was actually considering asking in this group if anyone with a 3d printer wanted to print the setup for me for $ & what the cost would be.


---
**Paul Haban** *October 27, 2015 17:28*

Thanks guys! I had the chain oriented in the right direction but the end mounts had a bar on one attachment end and a rounded end on the other attachment. I swapped those and now the chain cannot drift into the laser. Thanks again!


---
**David Cook** *October 29, 2015 19:37*

looks good


---
*Imported from [Google+](https://plus.google.com/+PhilWillisAtHome/posts/SBf3RjhWCTv) &mdash; content and formatting may not be reliable*
