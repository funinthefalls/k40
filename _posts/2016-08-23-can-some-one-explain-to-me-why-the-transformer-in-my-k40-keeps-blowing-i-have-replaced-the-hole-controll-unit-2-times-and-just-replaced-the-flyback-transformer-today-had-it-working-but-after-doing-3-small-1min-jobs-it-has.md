---
layout: post
title: "can some one explain to me why the transformer in my k40 keeps blowing i have replaced the hole controll unit 2 times and just replaced the flyback transformer today had it working but after doing 3 small 1min jobs it has"
date: August 23, 2016 06:18
category: "Discussion"
author: "beny fits"
---
can some one explain to me why the transformer in my k40 keeps blowing i have replaced the hole controll unit 2 times and just replaced the flyback transformer today had it working but after doing 3 small 1min jobs it has stoped working im prity shore thay are ment to run for more than 5 mins befor blowing up





**"beny fits"**

---
---
**Alex Krause** *August 23, 2016 06:29*

How many mA were you running?


---
**Eric Flynn** *August 23, 2016 06:32*

Control unit?  Replaced 2 times?  Do you mean the tube supply (the blue box) or the motion control board?


---
**beny fits** *August 23, 2016 06:39*

**+Eric Flynn** the blue box 


---
**beny fits** *August 23, 2016 06:41*

**+Alex Krause** i had it running about 90% of the max that the trim pot will go to 


---
**Eric Flynn** *August 23, 2016 06:50*

That's a bit high. What are you cutting/engraving?  


---
**beny fits** *August 23, 2016 07:05*

**+Eric Flynn** i was trying to engrave the back of a mirror 


---
**Phillip Conroy** *August 23, 2016 07:35*

K40tube should never go over 18ma if you want the tube to last as for power supply check for voltage switch[110v -220v] on side of power supply is set for your country,here in Australia i set it to 220v


---
**beny fits** *August 23, 2016 07:36*

**+Phillip Conroy** yeah i have it set for australia have been using it for about a year i just can't understand why it keeps blowing 


---
**Phillip Conroy** *August 23, 2016 08:00*

When l had a failure of the cooling water pump last year not only did the tube crack and stop working ,the power supply blow.Maybe the tube is stuffed? ..if the tube is stuffed and there is no where for the 22,000 volts to go ,it will blow the power supplys hv transformer .Also do you have a multimeter? And can you SAFELY test the mains voltage


---
**Phillip Conroy** *August 23, 2016 08:01*

Any other items in house blown lately,lights,etc or noise on tv,cordless phone ,that was not there before trouble started,ask next door as well


---
**beny fits** *August 23, 2016 08:49*

**+Phillip Conroy** i dont know i moved house 3 months ago the lazer was working fine at my old house but it did not work at the new house i replaced the transformer today and got it working again but as i said stoped working after only 5 mins


---
**William Steele** *August 23, 2016 09:15*

Make sure your ground/earth is connected properly.


---
**Phillip Conroy** *August 23, 2016 10:37*

since you replaced the power supply has the laser fired at all[made a mark on anything],if no tube is blown,

Where in au are you in au


---
**Stephen Sedgwick** *August 23, 2016 13:15*

You might check the line voltage in the house - it should be 110 steady on both poles.  With 220 between the 2 poles, I know my brothers house had something with this going on for some time where most items would work fine but occasionally the items that were really sensitive to the fluctuation it would fry it over and over again.  It was a combination of a bad ground on one pole, and then the incoming line from the road had a bad connection -it would cause fluctuations from 60v to over 300v because of spikes.


---
**Phillip Conroy** *August 23, 2016 14:35*

Do not muck around with 240 volts it kills.get an electrian to check it.i am an ex air force electronics tech  and  hecking power your self if not trained is not worth it.......check next door [and a fewvhouses both sides]if light globe or other electrial items do not last long.


---
**Stephen Sedgwick** *August 23, 2016 15:11*

You can take a volt meter to test this component... I would say don't try fixing it yourself (unless it is a grounding issue), but testing shouldn't be too bad.  If it is a 110v fluctuation due to ground you will see dips while on the 110 to ground with a constant 220v across poles.  You can set the meter up and watch for a period of time and watch, also possible load changes when HVAC is kicking in and out can show some of this also as that will commonly be the heaviest load on the system.  If it is a problem with the lines - your 220v would normally fluctuate also.


---
**Robi Akerley-McKee** *August 23, 2016 19:14*

I've also have had a bad connection coming into the house just before the the weather head.  Called the power company and they came out that day and replaced all three clamp connectors on the wires from the pole to the wires going into the house. And I had a bad leg on one breaker going to the ovens. USA 120v/240v  Ovens use 240v across the 2 phases from the pole.  1 phase to neutral(ground) is 120v, 1 phase to another phase is 240V.  Here in the US we call our house power single phase, but actually it's 2 phase. usually 180 degrees apart.  3 phase power is 120 degrees apart and usually 208v between phases (you never get to the 240v because of the 60 degree difference, but it's still 120v from one of the phase to the neutral (ground).


---
**beny fits** *August 23, 2016 20:53*

**+Phillip Conroy** yes i used it for about 5 mins


---
**beny fits** *August 23, 2016 20:55*

**+Phillip Conroy** muswellbrook nsw 


---
**Stephen Sedgwick** *August 23, 2016 21:14*

Thankfully in most residential services you don't have 3 phase......I wish you could get it easier...


---
**beny fits** *August 23, 2016 21:42*

I moved to a new estate area and the power seams to be fine 




---
*Imported from [Google+](https://plus.google.com/116814442468846937788/posts/84N1siUDmye) &mdash; content and formatting may not be reliable*
