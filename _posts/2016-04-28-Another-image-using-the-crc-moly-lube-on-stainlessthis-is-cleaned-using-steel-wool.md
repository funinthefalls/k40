---
layout: post
title: "Another image using the crc moly lube on stainless...this is cleaned using steel wool!"
date: April 28, 2016 23:26
category: "Object produced with laser"
author: "Scott Thorne"
---
Another image using the crc moly lube on stainless...this is cleaned using steel wool!

![images/1c20275b5f4eb6ba143347448f44de2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c20275b5f4eb6ba143347448f44de2b.jpeg)



**"Scott Thorne"**

---
---
**Ariel Yahni (UniKpty)** *April 28, 2016 23:53*

Super cool!!! 




---
**Scott Thorne** *April 29, 2016 00:01*

**+Ariel Yahni**...thanks...I was just playing around today..first time I turned it on in a week.


---
**Alex Krause** *April 29, 2016 00:10*

**+Scott Thorne**​ have you tried aluminum plate with moly? I think this would make an awesome way to finish off custom gantry plates/brackets for CNC and 3d printers 


---
**HalfNormal** *April 29, 2016 00:10*

I have to get myself some of that stuff to play with.


---
**Anthony Bolgar** *April 29, 2016 00:35*

Does moly lube work as good as cermark? Are there any benefits of one over the other?


---
**Alex Krause** *April 29, 2016 01:16*

Moly lube is about 10$USD a can that's a plus


---
**Jim Hatch** *April 29, 2016 01:17*

Just got my CRC from Amazon today. Gotta try it after I wrap up a gift box for my son - he's graduating college and I'm making him a business card box for his new company. First project using living hinges - wicked cool stuff, curves and stretches a bit too :-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 02:37*

Wow that is a really cool effect Scott. How permanent is the resulting image? Also, wouldn't steel wool scratch the steel? I'd test something like laying painter's masking tape, doing a cut of the outline of the object (or a full engrave), then weed the sections you want to apply the moly, then apply moly, engrave, then remove all the remaining painter's tape. Should minimise cleanup, but might be more work overall.


---
**dstevens lv** *April 29, 2016 04:36*

**+Alex Krause** I haven't been able to get it to work well with AL.  It looks good when it rasters but when I clean it most of it wipes off.  I've tried several settings, focus, etc.  With steel and stainless it's good and easy to set up.  I got a good raster on the second try with steel and used the same settings for SS.   It's not got the depth of color that the Cermark does but I'm not sure anyone would notice unless they were trying to compare.


---
**Alex Krause** *April 29, 2016 04:49*

**+dstevens lv**​ what type of aluminum are you using to test? And was there any prep work before (sand blasting, alcohol, ect...) I know that some aluminum is sent from the factory with a lamanent or clear coat to help keep it corrosion resistant I guess we will find out in a week or so when my K40 arrives :)


---
**dstevens lv** *April 29, 2016 05:08*

**+Alex Krause** Some 6061 T6 and 3003 H14.  Both virgin pieces, nice and shiney, used acetone to clean prior to applying the CRC.  Anodized and powder coated AL engraves well as does AL with Cermark applied.  Using the dry moly on AL isn't working for me.


---
**Scott Thorne** *April 29, 2016 10:09*

**+Alex Krause**...I've used aluminum also and I get the same result as stainless steel...it's permanent and won't come off with steel wool...although speeds have to be slow and powers high ...50mm/s at 60%power


---
**Scott Thorne** *April 29, 2016 10:11*

**+Yuusuf Sallahuddin**...the result is very permanent....I cleaned this with steel wool and it didn't scratch at all...

Maybe something soft like aluminum would scratch.


---
**Scott Thorne** *April 29, 2016 10:12*

**+Anthony Bolgar**...Cermak is 100.00 a can....so yes it's better than Cermak....lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 10:34*

**+Scott Thorne** Awesome, I will have to give it a test sometime. Would be a good way to mark tools for a mechanic/tradie to put their name/business details on it (so they don't get stolen or if lost can be returned).


---
**ED Carty** *April 29, 2016 13:50*

**+Scott Thorne** NICE


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 14:23*

I just had a thought Scott. Does it give different effect (or darkness) when you use different power levels?


---
**Scott Thorne** *April 29, 2016 14:56*

**+Yuusuf Sallahuddin**...if I go with lower power levels it won't adhere to the material. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 14:59*

**+Scott Thorne** Ah, that is unfortunate. Could have produced some awesome effects if differing power levels would still adhere. I guess dithering is the only way to achieve something similar to a gradient then.


---
**dstevens lv** *April 29, 2016 21:01*

**+Yuusuf Sallahuddin** The lube only transfers whatever the laser is outputting to the surface.  In this case the molybdenum reacts to the laser etching a gun metal gray color into the material, a shade of the color of the lube.  The reaction occurs as a constant that is dictated by the power and speed settings of the machine.  The Cermark is different in that it's a ceramic binder instead of molybdenum.  That is one of the reasons they can bind different colors into the solution.



The least expensive I've found for a cona of Cermark is about $80, still substantially more than the lube.  For those that are doing more than just occasional engraving the Cermark concentrate is a better value than the spray.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 23:59*

**+dstevens lv** Thanks for sharing that dstevens. I suppose it's a matter of what is your intention weighed up against price. The moly lube method seems a good balance for just creating a monotone marking on the steel. I'll have to look into the Cermark stuff a bit more to see results from it.


---
**Scott Thorne** *April 30, 2016 00:06*

**+Yuusuf Sallahuddin**...I'm in the process of rastering a much bigger piece using the moly...**+dstevens lv** is right but cermark is also 80 to 100 a can for every color you want...you can still only use one color at a time no matter what you are rastering....well unless you're doing abstract art...lol. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 09:18*

**+Scott Thorne** When you say you can only use 1 colour at a time, is that because you will get them mixed up or the application of them will be difficult? I imagine cutting a stencil with some cardstock/ply/acrylic first would assist in the application of the different colours in different areas, meaning you could possibly do multiple colours in one go. But, at 80 to 100 per can, seems a bit steep. Might be okay if you were doing it for some art exhibition or a customer willing to pay for all the cans, but other than that the moly seems to be the way to go.


---
**Scott Thorne** *May 06, 2016 10:01*

**+Yuusuf Sallahuddin**...if you were using different color cermark sprays...it send it would be difficult to get the placement right.


---
**andy creighton** *May 26, 2016 20:21*

My local store was out of crv but had another brand of "moly oil" for $5.  It sprays clear and does not give the same effect.   It did leave a faint image but it wiped of with a towel.

I'll have to pick up some crc next trip




---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/eVL93SvnmxY) &mdash; content and formatting may not be reliable*
