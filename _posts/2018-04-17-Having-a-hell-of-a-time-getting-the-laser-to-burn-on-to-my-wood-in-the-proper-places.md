---
layout: post
title: "Having a hell of a time getting the laser to burn on to my wood in the proper places"
date: April 17, 2018 01:19
category: "Discussion"
author: "freebird1963"
---
Having a hell of a time getting the laser to burn on to my wood in the proper places. I 

seem to run off the edges or start to low or to high or to far right.

I use a K40 and Whisperer. I use the red cross hair but still not getting it.

I am not talking about on a square piece of wood. I have some hearts from Michaels and just can't figure out how to start the head in the proper position.

Guess I really don't understand now the laser starts the vector cut. Images are of the design down in CDx4. The heart outline is the actual size of the heart as I traced and scanned it. Then the second image is as it first appears in Whisperer. 3rd image is after I have moved the laser head by dragging the image around on the right side of Whisperer 4th image is the laser head on the heart ready to engrave. 5th is the outcome and can see middle right side to bottom is off the edge.  

In image 3 were I have dragged the head to were I think it should be on the heart I assume that the gray dot represents the laser head. And then when I hit vector engrave it then moves from that gray dot to start the engraving and uses taht as the reference point for the image ? Is that right ? If so how else can I measure to get the head right in the first place. So aggravating. Hope I made sense. Frustrated with all the waste.

Thanks.



![images/3ec1d6fd0c11958ab67680f43c61e7af.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ec1d6fd0c11958ab67680f43c61e7af.jpeg)
![images/1ca8962556f6fbecb902ecdbd777d163.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ca8962556f6fbecb902ecdbd777d163.jpeg)
![images/40b58a33632f3b44a7c38c155071ae45.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40b58a33632f3b44a7c38c155071ae45.jpeg)
![images/c8e53ac932d34a01f90a4160d81a02c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c8e53ac932d34a01f90a4160d81a02c6.jpeg)
![images/6f37c47c85091f5977e1cee4f0f6df4b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6f37c47c85091f5977e1cee4f0f6df4b.png)

**"freebird1963"**

---
---
**Joe Alexander** *April 17, 2018 03:10*

yea the gray dot is the starting reference, so it should be on the very edge of your hearts or maybe just off the edge. if you had an outline around the names in a heart shape you could cut a "jig" out of some scrap material, leaving a hole to drop your hearts in.


---
**Anthony Bolgar** *April 17, 2018 04:11*

Place a layer of masking tape on the bed, and cut the outline of the heart into it. Then just place the heart over the outline, perfect postioning every time.




---
**Duncan Caine** *April 17, 2018 21:43*

Imagine the heart is actually a square, then 0,0 is top left, and that's where you need to start. So put your laser pointer at 0,0 (which is off the heart shape) and all should be ok


---
*Imported from [Google+](https://plus.google.com/101728425728882321133/posts/8cE3rwJYVsu) &mdash; content and formatting may not be reliable*
