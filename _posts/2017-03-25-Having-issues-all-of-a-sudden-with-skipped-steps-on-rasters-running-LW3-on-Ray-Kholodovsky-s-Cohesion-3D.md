---
layout: post
title: "Having issues all of a sudden with skipped steps on rasters running LW3 on Ray Kholodovsky 's Cohesion 3D"
date: March 25, 2017 00:23
category: "Discussion"
author: "Chris Sader"
---
Having issues all of a sudden with skipped steps on rasters running LW3 on **+Ray Kholodovsky**'s Cohesion 3D. I've tightened the Gantry belt as mentioned in **+Alex Krause**'s post, but I'm still having the issue. It's sort of unpredictable.



<b>Originally shared by Alex Krause</b>



Started noticing a bunch of shifting in my images... I thought I was missing steps at first... so I checked my X axis belt tension on my k40 and felt a ton of slack in the belt. So I jogged the head to where the hole that leads to the electronics pannel is and tightened both screws that I circled in the second image a 1/4 turn at a time until the belt was where it should be... fingers crossed that this fixes my problem and remember don't over tighten the belt



![images/9294f39d9cf3cce1d4bc94180ce8384d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9294f39d9cf3cce1d4bc94180ce8384d.jpeg)
![images/09fd3171f286aff8c6b7a2f7b19a956b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09fd3171f286aff8c6b7a2f7b19a956b.jpeg)

**"Chris Sader"**

---
---
**Alex Krause** *March 25, 2017 00:29*

Are you powering off the laser PSU for 24?


---
**Chris Sader** *March 25, 2017 00:31*

**+Alex Krause** yes. 


---
**Alex Krause** *March 25, 2017 00:33*

Happen to know how much current your steppers are running at?


---
**Chris Sader** *March 25, 2017 00:37*

**+Alex Krause** Last I checked the pots were set at .41


---
**Chris Sader** *March 25, 2017 00:39*

I also have a feeling I'm just doing the engraving wrong...most of what i do is cutting. It's not as smooth or fast a I've seen some people do when they engrave.


---
**Alex Krause** *March 25, 2017 00:40*

The reason I ask is that the 24v supplied by the laser PSU is only rated at 1amp max **+Don Kleinschnitz**​ confirmed this... I also noticed I had more skipping issues with the coiled hose from my air assist attached while going at high speed and acceleration settings 


---
**greg greene** *March 25, 2017 01:00*

good info !


---
**Don Kleinschnitz Jr.** *March 25, 2017 01:09*

**+Chris Sader** try this test image in both image and vector (svg) form.



To test: 

...Load the SVG directly.

...Save the .svg as an image and then load.



Laser them both.



You can also repeat them across the work space for more information relative to x-y location.



Its intention is to create a known set of vector moves and raster image that you can then diagnose by looking at straightness spaceing etc.



[drive.google.com - Resoluton test pattern.svg - Google Drive](https://drive.google.com/file/d/0B9A1NYiboK99WUh2NldlMVFGcjQ/view?usp=sharing)


---
**tyler hallberg** *March 25, 2017 02:38*

I've had this problem since I switched to a cohesion board on anything I try to engrave larger than 2 or 3 inches. I've tried everything short of changing my stepper motors 


---
**tyler hallberg** *March 25, 2017 02:40*

![images/ab88f71a3aff34ae6003fd54f1e0cc8b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab88f71a3aff34ae6003fd54f1e0cc8b.jpeg)


---
**Alex Krause** *March 25, 2017 02:42*

Try slowing down your engrave speed a bit **+tyler hallberg**​ I saw more problems at 400mm/sec than I did after changing my max speed to around 300mm/sec by doing so you need to back off the power at which you engrave to compensate for moving slower


---
**Alex Krause** *March 25, 2017 02:43*

Is the cohesion board able to change the micro stepping? If you can back off to 1/16 micro steps you should get more torque from your motors 


---
**tyler hallberg** *March 25, 2017 02:52*

We have tried everything. I see no speed change when going from 100mm/s to 400mm/s. I have changed out the controller parts, re did the configure file, update firmware, and nothing helps. Doing smaller things is fine, but it almost gets lost when doing larger engravings.




---
**Alex Krause** *March 25, 2017 02:56*

Have you tried LW4 to see if you get the same problems?


---
**tyler hallberg** *March 25, 2017 03:04*

I am using LW4. If I turn off join pixels machine runs like garbage. If I leave it on it runs but not the speed i set it at. I just upgraded the firmware again to see if anything has changed. will update shortly 


---
**tyler hallberg** *March 25, 2017 03:09*

Any pointers on where to change the micro steps? is that in the config file?


---
**Alex Krause** *March 25, 2017 03:17*

It would be a jumper if **+Ray Kholodovsky**​ added it to the board 


---
**Stephane Buisson** *March 25, 2017 09:38*

Speed & inertia: do you increase the head weight ? (cable chain, air assist) could be related with max current available for the motor

saying that, i just notice you have only one break in your image so just one (large) skipping. hard to think it could be a software setting.

maybe  just an acceleration issue (not computed) before changing direction. sorry just a shout in the dark !!!


---
**Claudio Prezzi** *March 25, 2017 10:49*

**+tyler hallberg** Try to reduce accelleration. That's where stepps get lost most often.


---
**Claudio Prezzi** *March 25, 2017 10:55*

Also, the transfer speed between PC and Smoothie is limited and it is proportional to feed and pixel resolution (=laser diameter). If you set laser diameter to 0.1mm for example, then you would be limited to less than 100mm/s. With 0.2mm laser diameter, you could reach twice this feed.


---
**Claudio Prezzi** *March 25, 2017 10:57*

Join Pixels help to reduce the transfer volume by combining mutliple pixels with same power in one command.


---
**Stephane Buisson** *March 25, 2017 11:02*

if you 'Print' twice the same image, does it "break" at the same point ?


---
**Don Kleinschnitz Jr.** *March 25, 2017 12:04*

**+Alex Krause** interesting that the specs on these machines is 300mm/sec, I think.


---
**Don Kleinschnitz Jr.** *March 25, 2017 12:20*

**+Claudio Prezzi** this could take us down a rat hole but isn't the incoming stream isolated (buffered) from the machines movement? 

I can see how average speed can be effected by a slow down in input but isn't a movement buffered and then marked? 

I imagined that if there was inadequate input data the head pauses waits for data and then continues. I can also imagine that if the input cannot keep up with the movement then pauses cause the mechs to do a lot of start-stops and that could excite a resonance or exaggerate something like loose belts. 

My point being that the input stream throughput shouldn't be the source of missteps or mis-positioning should it?

BTW I have a similar but not as severe problem with my machine. I created the test pattern to hunt down what I assumed to be a mechanical problem with the belts or some other overshoot etc. 


---
**Don Kleinschnitz Jr.** *March 25, 2017 12:23*

**+tyler hallberg** can you take a picture that shows the left and right side of the image? It would be useful to know if both edges are shifting or just one. This would point to a scan positioning problem or a reversal problem.


---
**Don Kleinschnitz Jr.** *March 25, 2017 12:32*

**+tyler hallberg** **+Chris Sader** if possible I would measure the 24V voltage and current. [I am assuming your are using the LPS 24V]. 



I wonder if the LPS 24V cannot handle high speed and peak stepper currents. Subsequently stepper power is reduced slightly during fast and high load moves.  



As **+Alex Krause** suggested the LPS 24V is rated at 1 amp and I am pretty sure that voltage is only used for motors in the NANO whereas the logic is powered from the 1A LPS 5V.  



Like others I am guessing..... 


---
**Claudio Prezzi** *March 25, 2017 12:46*

**+Don Kleinschnitz** Right, there is a planner queue in the firmware that "caches" the next few commands. By default, this is 32 moves on smoothieware (or 16 on grbl). With 0.1mm pixel size, this is 3.2mm only! But the communication still has to deliver enough data in average for the selected feed. At 100mm/s and 0.1mm/Pixel, this is 1000 G1 moves per second!


---
**Claudio Prezzi** *March 25, 2017 12:54*

I don't think that the transfer limitation could result in lost stepps! 

I just explained that because **+tyler hallberg** wrote he used a feed of 100-400mm/s.


---
**Don Kleinschnitz Jr.** *March 25, 2017 13:09*

**+Claudio Prezzi** Ah I get it ...thanks.


---
**Chris Sader** *March 25, 2017 13:32*

**+Stephane Buisson** not on mine. Luckily I got through a whole batch of 6 sided dice (so six jobs) without it happening last night


---
**William Niedermeyer** *January 15, 2018 18:52*

I had this problem right of the bat. I took apart the X gantry to get to the tension bracket. What I found was the belt slipping and grinding away on the tension wheel. This was caused by a metal burr in the bracket that the tensioner wheel screw shaft attaches the wheel too. Tensioning the bracket according to the posts was actually digging the plastic tensions wheel into the burr causing it to stop or grind the wheel and belts. To over come this I went another route. I took a metal toothed wheel that matched the belt teeth, and moved the bearing from the plastic tension wheel to it. I cleaned the burr on the bracket and reassembled the tension wheel onto the bracket and put the x gantray back together. This looks like a manufacturing defect that may be plauging a lot of people seeing drift in their cutting. I also used a medium thread lock on the tension wheel shaft nuts. And, put a drop of light machine oil on the bearing inside. Hope this helps. 


---
*Imported from [Google+](https://plus.google.com/107747838010116449754/posts/a6JBBgDVHSh) &mdash; content and formatting may not be reliable*
