---
layout: post
title: "Made a pin cutting bed out of 1,5mm aluminium and 10mm killer spikes"
date: January 12, 2018 22:12
category: "Modification"
author: "Bernd Peck"
---
Made a pin cutting bed out of 1,5mm aluminium and 10mm killer spikes. Sanded the aluminium to difuse the laser. 



Two edges have aluminium angles to which I have attached a plastic measuring tape with double sided glue tape.

These angles shall allow me to dial in paper for cutting better than before. 

(The masking tape is improvised - which means it should last forever 😂😂)



The pin cutting bed lies on top of the normal cutting plate and is not adjustable in height.



For my purpose to cut my reporter notepads easier and the double-sided self-printed paper more precise it should work.



[https://m.imgur.com/a/3yC5z](https://m.imgur.com/a/3yC5z)

![images/09552b99e47e3fba197779cd0a0158c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09552b99e47e3fba197779cd0a0158c0.jpeg)



**"Bernd Peck"**

---
---
**Paul de Groot** *January 12, 2018 22:56*

You have given me an idea with the paper measuring tape. Up to IKEA to get a free paper measuring tape. 😁


---
**Kyle Kerr** *January 13, 2018 09:56*

I have subscribed to a guy, sabar multimedia, on YouTube that has an excellent series of videos about his laser machines. The most recent covers anodized aluminium. Anodization should work in place of sanding, if one were so inclined.


---
**Claudio Prezzi** *January 13, 2018 12:44*

Very nice and professional looking!


---
**Bernd Peck** *January 13, 2018 20:32*

Thanks for your nice feedbacks. Will have a look at Sabar channel for the anodized aluminium


---
**Fook INGSOC** *January 13, 2018 22:29*

....slight acid etching would probably also work to give the AL a matte finish!!!


---
**Steve Clark** *January 14, 2018 21:33*

For anyone looking for the spikes>>>

[ebay.com - Details about 200 PCS Metal Studs Rivet Bullet Spikes for Leather Bracelets/Dog Collars/Bags](https://www.ebay.com/itm/200-PCS-Metal-Studs-Rivet-Bullet-Spikes-for-Leather-Bracelets-Dog-Collars-Bags/172856726467?hash=item283f1053c3:g:r5YAAOSwD6xZslAd)




---
**BEN 3D** *January 15, 2018 22:58*

And a shop that deliver to germany [ebay.de - Details zu 100 Stück Metall Leder Nieten Killer Nieten Spitznieten Spike Ziernieten Punk](https://www.ebay.de/itm/100-Stuck-Metall-Leder-Nieten-Killer-Nieten-Spitznieten-Spike-Ziernieten-Punk/372094349013?_trkparms=aid%3D555018%26algo%3DPL.SIM%26ao%3D2%26asc%3D49139%26meid%3D2dc64778577949dcb6b07bd03c71f150%26pid%3D100005%26rk%3D1%26rkt%3D6%26mehot%3Dpp%26sd%3D251398931846&_trksid=p2047675.c100005.m1851)




---
*Imported from [Google+](https://plus.google.com/100238441539373475210/posts/goYqqJGQDWY) &mdash; content and formatting may not be reliable*
