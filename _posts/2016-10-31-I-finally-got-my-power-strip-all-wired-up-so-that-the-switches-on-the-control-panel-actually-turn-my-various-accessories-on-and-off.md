---
layout: post
title: "I finally got my power strip all wired up so that the switches on the control panel actually turn my various accessories on and off"
date: October 31, 2016 03:25
category: "Discussion"
author: "K"
---
I finally got my power strip all wired up so that the switches on the control panel actually turn my various accessories on and off. Thank you to **+Don Kleinschnitz** for helping me with the wiring!



![images/e0eb639b27e2ed59b7fd5a8354aec363.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e0eb639b27e2ed59b7fd5a8354aec363.jpeg)
![images/13ebc40dd3e589fefb4e608a4643160a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/13ebc40dd3e589fefb4e608a4643160a.jpeg)

**"K"**

---
---
**Ariel Yahni (UniKpty)** *October 31, 2016 03:34*

Oh I need that urgently, nicely done


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 31, 2016 06:10*

Looks elegant :)


---
**Don Kleinschnitz Jr.** *October 31, 2016 12:25*

Good design and job!


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/eQ1A5CK6o8y) &mdash; content and formatting may not be reliable*
