---
layout: post
title: "Hi guys, I need your help for choosing a laser cutter ...."
date: March 30, 2017 08:36
category: "Discussion"
author: "black rat miniatures"
---
Hi guys,



I need your help for choosing a laser cutter ....



Firstly, a short presentation: Bastien 24years, french graphic designer / product designer,

I have been interested in new technologies such as cnc, 3D, etc ...

At this moment, I set up my own company for creating miniature sets.

I am in the perspective of buying a Chinese type laser cutter :

40k 40w - arround 400 €

50w - around 1300 €

My objectiv is cutting the 3mm mdf and 1.5 to 3mm plexy (or similar)

I went around a lot of american/english forum and I understood that there are some changes to machine machines for more efficient / accurate machines (laser alignment, background support, software, etc. ..) it's not something that scares me because I'm a handyman.

I found a lot to choose from the 40w but much less on the 50w ...

I would like to have the advice / feedback / owning this type of machine :) the idea is whether they can match its my expectations,

I joining the type of product that I would like to realize, you think it's possible with 40w? the product size is small (for the building approximatly 25x25x15cm, and circle token approximatly 3cm)



Thx friends,

Have nice week ✌🏻️



Sorry my english is really so bad ...



![images/f954ca757dae5e7905c7c7136e818c3d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f954ca757dae5e7905c7c7136e818c3d.jpeg)
![images/f5c685870c87722c63768f15e7d643b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f5c685870c87722c63768f15e7d643b3.jpeg)
![images/2a5e10c68dde47bc26cf347a45d97df7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2a5e10c68dde47bc26cf347a45d97df7.jpeg)
![images/4a50b3188b07c449965b68fa04e9a073.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4a50b3188b07c449965b68fa04e9a073.jpeg)
![images/9dbbd72c090fdc2c0efefd11c3bacda0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9dbbd72c090fdc2c0efefd11c3bacda0.jpeg)

**"black rat miniatures"**

---
---
**Phillip Conroy** *March 30, 2017 08:48*

K40 ,is limited to 300mm wide X 200mm high,Wil easy   cut  3mm mdf and acrylic, have been using mine for this for over 18 months Nice small desktop machine, great  hobby machine to learn on,needs air assist and water flow switches ,and door switch for learners


---
**Phillip Conroy** *March 30, 2017 08:57*

I just upgraded to the Chinese 50wat 500mm wide X 300mm high,much better machine then k40 ,faster ,smoother can set layers to etch and cut in one job.has air asist and water flow protection  built in.only manual hight adjustable laser bed,dsp controller miles ahead of stock k40.I think the model is DC 350 $2000 ver $500 k40.if buying the 50wat laser cutter make sure it has dsp controller 


---
**Stephane Buisson** *March 30, 2017 09:26*

Bienvenue ici **+black rat miniatures**.



il y a une grosse différence de prix entre le 40 et 50W. un 40w modifié te reviendra a moins de 550€ et devrai sastisfaire ta demande. Mais si ta demande évolue tu sera bloqué par la taille ou la puissance. ceci dit rien ne t'empêche de te faire les dents et ton travail courrant sur une k40 et de faire des jobs de temps a autres à l'extérieur. (Fablab)



les mod transforme la k40 en machine utilisable, je veux dire du point de vue software donc change ta carte par une C3D. Air assist et lit amélioreront les perfs (puissance efficace et réduction des marques de brulure).






---
**black rat miniatures** *March 30, 2017 09:33*

**+Phillip Conroy** 

 thank you for your answer! I saw all the upgrade that it is necessary to improve the machine, you made them on, your ? 

I understood that it is a machine to start, understand the operation, making test, improvise, and it is not a very big risk for this price.

You think I could have a sufficient definition with the laser to achieve what I want to do? To start this size cut me agree ^^

thx man


---
**black rat miniatures** *March 30, 2017 09:40*

**+Phillip Conroy** Thank you, I did not see your second comment, yes that's what I understood about the 50w, it's not the same price, but I think it's a good investment.

 

I saw what you could do! it's great! 



I did not know the dsp controller, I will inquire, 

I thank you


---
**black rat miniatures** *March 30, 2017 10:05*

**+Stephane Buisson** merci stéphane! 

en fait pour le moment c'est surtout, tester et me faire la main, et si j'arrive à amortir la machine en produisant des décors de qualité c'est top! 

j'aimerai me faire un catalogue de produit réalisé en découpe laser et ainsi pouvoir les présenter aux gens. Donc il faut que ce soit un minimum sympas :) 

Pour pouvoir plus tard les vendre ou prendre des commandes (et si besoin passer sur une 50w).



ma demande n’évoluera pas en puissance, je veux dire par la, je n'aurai jamais à découper autre chose que du mdf 3mm et du pmma.

peux être que la t'aille de découpe sera un peux juste, et encore c'est même pas dis.



Le problème ici sur Nice c'est qu'il n'y en a pas, il y en a un dans un lycée mais avec le plan vigipirate c'est interdit au public extérieur...



Air assist c'est la modification de l'exhaust?

Lit? je ne connais pas





Je te remercie pour toutes les infos c'est top :)


---
**Stephane Buisson** *March 30, 2017 10:22*

en 2 mn j'ai vu plusieurs fablab sur Nice


{% include youtubePlayer.html id="Dr4R7-nRHME" %}
[youtube.com - Nice : un FabLab au CEEI](https://www.youtube.com/watch?v=Dr4R7-nRHME)

[http://fablabo.net/wiki/Cartographie_des_fablabs_fran%C3%A7ais](http://fablabo.net/wiki/Cartographie_des_fablabs_fran%C3%A7ais)



Lit : un lit en nid d'abeille réduit la surface de contact ce qui minimise les brulures et laisse passer le flux d'air de l'air assist.



le gros de ta recherche avant HA doit se faire sur les softs. en effet le soft qui vient avec la becane est une cata (windows only). mais comme le hard est propriétaire tu dois le changer pour aller vers les softs open sources comme laserweb ou Visicut (fromage ET dessert). c'est pourquoi je te recommande la carte  C3D mini ou Smoothie. voir les 4 communautés correspondante.



regarde aussi mes 2 veilles video youtube pour te faire une idée des mods.



il y a en a une autre que j'aime beaucoup c'est le lit monté sur ressort inversés gardant le matériel a distance focale.






---
**black rat miniatures** *March 31, 2017 12:03*

merci stephane :)

malheureusement sur les fablab que tu as trouvé apparemment les deux sont fermé, les site internet ne sont plus en ligne, et malgré un tas de recherche sur le net je n'y trouve aucune infos ni aucune actualité... concernant l'autre fablab il est dans le departement mais pas sur nice (à environ 1h00).. il faudrait que je pose un jour de congés pour y aller ^^ ca me permettrai de voir et tester des trucs 



pour le lit, j'ai vue quelque tuto sur cette modification oui, mais je pensai pas que ca s'appelait comme ca ^^

je vais aller jeter un oeil sur ta chaine youtube.



la je pense que j'ai toutes les infos pour ce que je veux en faire :)  je vais prendre ce que tu me conseille comme hard et je vais aller voir les rubriques correspondantes, merci beaucoup pour ton aide en tout cas! 


---
**Phillip Conroy** *March 31, 2017 19:39*

The stock k40 uses a m2 board to control the laser,be careful some of the cheaper 50 watt 500mm X 300mm machines also use this board,not as good or versitie as dsp


---
**black rat miniatures** *April 01, 2017 07:58*

thx for your advice philip! 

on the advice of stephane i think I will buy a k40 and change the card for a C3D :)




---
*Imported from [Google+](https://plus.google.com/107090422237759503233/posts/9BZXhxiHGW5) &mdash; content and formatting may not be reliable*
