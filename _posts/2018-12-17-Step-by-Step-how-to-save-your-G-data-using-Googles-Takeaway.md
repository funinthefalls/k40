---
layout: post
title: "Step by Step how to save your G+ data using Google's Takeaway"
date: December 17, 2018 17:36
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Step by Step how to save your G+ data using Google's Takeaway.

You can view without having to join.



[https://www.everythinglasers.com/forums/topic/how-to-save-your-g-data-step-by-step/](https://www.everythinglasers.com/forums/topic/how-to-save-your-g-data-step-by-step/)





**"HalfNormal"**

---
---
**James Rivera** *December 17, 2018 19:04*

Cool. Trying it now...


---
**James Rivera** *December 17, 2018 22:26*

Failed first attempt. 2nd attempt failed, too. I downloaded 2gb zip anyway. Talk about useless. It 1 post for each of my categories, and nothing of my posts/comments elsewhere. WTH?


---
**HalfNormal** *December 17, 2018 22:38*

Under the "ActivityLog"  directory is a list of G+ activity including Comments.html. This contains all your comments. 


---
**James Rivera** *December 18, 2018 00:12*

**+HalfNormal** I don't have one. I guess it just plain failed.


---
**HalfNormal** *December 18, 2018 00:54*

When you start the process does it show that it's downloading files?


---
**HalfNormal** *December 18, 2018 00:54*

Are you able to come up with a web page that looks like the one I posted?


---
**Don Kleinschnitz Jr.** *December 18, 2018 14:48*

I think I did it right (per your post) and it downloaded multiple archives.



Looks like this;



Opening these folders revealed a combination of HTML, jpeg and .csv files.



The +1 on posts; have content but the context links you back to G+.



The collections; has an HTML file with a G+ link ???



The Photos; has jpg in folders by date and some .csv files.



The Posts file has mostly csv and a few jpg with # added to the jpg extension.



Did I do this wrong?

How are these archives useful when they point back to G+ links when G+ will be gone?

No idea how this is useful for reconstruction although it appears to save pictures.



So bottom line is what would you do with these archives?



BTW I thought this was supposed to be HTML and JSON..... seems like metadata is all .csv?? 

![images/1c482f023f1f622c829f8c6cc9736355.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c482f023f1f622c829f8c6cc9736355.jpeg)


---
**HalfNormal** *December 18, 2018 15:35*

**+Don Kleinschnitz Jr.** The confusion is how Google has presented the information. Under ActivityLog are a few html files that will open +1 comments made by you and one that will post ones made by other people. Under Posts, after the CSV and JPG files are html files sorted by date. All the previous files are what are used by the html post information. These are the actual G+ posts but not formatted the way they are on the site. They do include the comments by other G+ users. 

It is unfortunate that Google did not include an easy viewer for seeing all the information. Maybe someone will do that.




---
**Don Kleinschnitz Jr.** *December 18, 2018 15:43*

**+HalfNormal** ok I did not look far enough down the file list for useful post information. This is kind a mess.....:(.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/bi3zWtXZDR9) &mdash; content and formatting may not be reliable*
