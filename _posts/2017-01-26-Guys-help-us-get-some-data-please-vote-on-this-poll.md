---
layout: post
title: "Guys help us get some data, please vote on this poll"
date: January 26, 2017 17:31
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Guys help us get some data, please vote on this poll.



<b>Originally shared by Ariel Yahni (UniKpty)</b>



Welcome to all our new members. 



As we pass 1500+  it will be interesting to see what kind of laser are you running so vote, vote , vote bellow :)





**"Ariel Yahni (UniKpty)"**

---
---
**Austin Baker** *January 27, 2017 20:33*

Curious about Diode Lasers and what those entail. Have people replaced the CO2 tube with a diode laser in their K40? What are the approximate costs/power levels offered with a diode laser?


---
**Joe Walters** *January 28, 2017 12:23*

I don't have a k40 yet, but I'm using a 2.5w diode on my home brew engraver. From everything I've seen CO2 is the way to go. They seem to last longer and have a much higher output than any of the diodes that I have seen.  


---
**Ariel Yahni (UniKpty)** *January 28, 2017 13:22*

Yes CO2 have much more power so cutting is its strength but it much more complicated to calibrate and maintain. Diode  are very simple, very good for engraving ( CO2 tube are wasted very fast compared to diode ) but not much for cuttng


---
**Nick Hale** *January 29, 2017 19:16*

Im running a 15w diode and a k40.  I love using the diode with arduino over the k40.  Its so easy an idiot could use it.




---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/A2Y2LMB8XfG) &mdash; content and formatting may not be reliable*
