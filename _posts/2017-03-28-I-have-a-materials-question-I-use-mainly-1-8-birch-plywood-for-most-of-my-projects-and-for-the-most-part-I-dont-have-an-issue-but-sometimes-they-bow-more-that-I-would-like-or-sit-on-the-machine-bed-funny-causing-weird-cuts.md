---
layout: post
title: "I have a materials question? I use mainly 1/8 birch plywood for most of my projects and for the most part I don't have an issue but sometimes they bow more that I would like or sit on the machine bed funny causing weird cuts"
date: March 28, 2017 01:59
category: "Discussion"
author: "3D Laser"
---
I have a materials question?  I use mainly 1/8 birch plywood for most of my projects and for the most part I don't have an issue but sometimes they bow more that I would like or sit on the machine bed funny causing weird cuts.  Is there a wood other than mdf that is more stable than plywood but will not break the bank in the process?





**"3D Laser"**

---
---
**Ashley M. Kirchner [Norym]** *March 28, 2017 02:12*

Most ply will do that over time. Try clamping the sheet down. I hold mine down with magnets in each corner. 


---
**Ashley M. Kirchner [Norym]** *March 28, 2017 02:44*

I scavenge hard drive magnets and use them in the corners of the piece:

![images/d637a26696bd27655835165ff81045d1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d637a26696bd27655835165ff81045d1.jpeg)


---
**Alex Krause** *March 28, 2017 02:48*

Do this as a test take a piece of bowed wood... Place concave side use a spray bottle with water and dampen the bowl side, set out in the sun to dry with the convex side up (moist side down) should take the bow out of the wood 


---
**Phillip Conroy** *March 28, 2017 07:35*

I cut my 2440 mm X 100mm sheets done to 200mm wide with bandsaw then stack flat with weight on them 


---
**Ned Hill** *March 28, 2017 12:05*

I keep a spray bottle of water handy like **+Alex Krause** said to take the bow out of wood.  I typically will spray and put some weight on the board cup side down and let it dry.  Works great.  I also store my thinner wood like **+Phillip Conroy**.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/8m3gnKmfuYm) &mdash; content and formatting may not be reliable*
