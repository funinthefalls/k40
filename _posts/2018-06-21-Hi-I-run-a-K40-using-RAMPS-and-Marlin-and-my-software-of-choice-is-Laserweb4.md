---
layout: post
title: "Hi I run a K40 using RAMPS and Marlin and my software of choice is Laserweb4"
date: June 21, 2018 11:26
category: "Software"
author: "Andy Carter"
---
Hi



I run a K40 using RAMPS and Marlin and my software of choice is Laserweb4.



I've been using the same setup for 18 months with no issue.



I upgraded to the latest version of Laserweb4 a week ago and the profile I had set up for my machine had disappeared from the available list in settings. No big problem I just created a new one. The real issue is that the 'S' value set in the gcode is duplicating. Instead of S70 (I set this in the cut profile) the gcode is showing S7070. Again no great issue as I can fix it easily in my text editor.



any ideas?





**"Andy Carter"**

---
---
**Anthony Bolgar** *June 21, 2018 15:01*

I would go to Github and open an issue there. You will get a faster response I think. Jorges is pretty quick at dealing with issues.




---
**James Rivera** *June 21, 2018 16:26*

**+Peter van der Walt** 


---
**Peter van der Walt** *June 21, 2018 16:46*

**+James Rivera** I no longer belong to the LW dev team, post in the LaserWeb community please (: 

[LaserWeb/CNCWeb](https://plus.google.com/communities/115879488566665599508)


---
**Anthony Bolgar** *June 22, 2018 14:57*

**+James Rivera** Peter is no longer a part of the LaserWeb project (He left about a year ago) **+Todd Fleming** is the lead on the project now.




---
*Imported from [Google+](https://plus.google.com/100953109353612781968/posts/j5RY5x14JH1) &mdash; content and formatting may not be reliable*
