---
layout: post
title: "HEY GUYS! I'm having a really weird problem that I cant seem to find a solution too anywhere else"
date: June 19, 2017 19:09
category: "Discussion"
author: "Marc Beckwith"
---
HEY GUYS! 



I'm having a really weird problem that I cant seem to find a solution too anywhere else. 



This started out of no-where as I was just doing another project.  



Whenever I end up engraving, the lines are coming out wavy and irregular 



![images/5c9d4fb2f5f6a527a4f2a47f5bb0e719.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c9d4fb2f5f6a527a4f2a47f5bb0e719.jpeg)
![images/1c62384940019a961fc2b3eea195efa9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c62384940019a961fc2b3eea195efa9.jpeg)

**"Marc Beckwith"**

---
---
**Ned Hill** *June 19, 2017 20:48*

Common culprits; loose belt, loose mirror/mount, loose lens holder.


---
**Alex Krause** *June 19, 2017 21:24*

Or you are going to fast and your work is shifting side to side :P


---
**Ned Hill** *June 19, 2017 21:44*

I also once had a problem, before I put in with a drag chain, with the air line dragging on a light piece causing it to shift back and forth.. #K40wavylines


---
**Ned Hill** *June 20, 2017 17:13*

**+Marc Beckwith** let us know if you identified the problem so we can add it to the knowledge base or if you need some more help.


---
*Imported from [Google+](https://plus.google.com/105917640032798246851/posts/f7mFLLXMGEh) &mdash; content and formatting may not be reliable*
