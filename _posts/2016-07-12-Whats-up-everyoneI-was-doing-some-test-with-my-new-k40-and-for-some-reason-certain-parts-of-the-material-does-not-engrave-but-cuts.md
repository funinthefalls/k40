---
layout: post
title: "What's up everyone...I was doing some test with my new k40 and for some reason certain parts of the material does not engrave but cuts"
date: July 12, 2016 18:09
category: "Hardware and Laser settings"
author: "Meir Santos"
---
What's up everyone...I was doing some test with my new k40 and for some reason certain parts of the material does not engrave but cuts.  Probably not explaining myself well.  Basically if i try to engrave at let say x,y position it engraves but when I set it to x2,y2 it doesn't.  There seems to be a location that engraving (on plywood) doesn't work.  I tried cutting in the same area and it does outline...just doesn't engrave....is this a software issue?





**"Meir Santos"**

---
---
**Ned Hill** *July 12, 2016 18:32*

Sounds to me that it might be an alignment issue.  Have you gone through a full mirror alignment?


---
**Michael Knox** *July 12, 2016 19:02*

Had the same issue it was a alignment issue for me


---
**Meir Santos** *July 12, 2016 19:13*

I'm having the hardest time aligning this thing...is there any step by step ways to align the mirrors?  I've looked online and used the pdf for aligning the k40...but I can't get it perfect...sigh.


---
**Meir Santos** *July 12, 2016 19:19*

**+Michael Knox** what was the best way you aligned the mirrors?


---
**Ned Hill** *July 12, 2016 19:28*

+Meir Santos See this guide.  [https://1drv.ms/b/s!AtpKmELEkcL8imoUD99sdJZZT-am](https://1drv.ms/b/s!AtpKmELEkcL8imoUD99sdJZZT-am)  I would recommend using a sharpie and labling the screws A, B, and C, as designated in the guide, for easy reference.


---
**adrian miles** *July 14, 2016 07:08*

you know the best thing i did in aligning mirrors was to put a red laser pointer at one of the mirrors - adjust the screws so you can SEE what they do - once you see it you will understand what is happening and should find it much easier to align


---
*Imported from [Google+](https://plus.google.com/107056752453136978834/posts/bVQPjaviQRT) &mdash; content and formatting may not be reliable*
