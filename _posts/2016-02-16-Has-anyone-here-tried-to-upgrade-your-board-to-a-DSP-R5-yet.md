---
layout: post
title: "Has anyone here tried to upgrade your board to a DSP R5 yet?"
date: February 16, 2016 00:55
category: "Modification"
author: "Brien Watson"
---
Has anyone here tried to upgrade your board to a DSP R5 yet?  I just bought one and the schematic that Light Object supplies (online not in the box) is very technical, not in lay mans terms..  If you have can you explain what you did?





**"Brien Watson"**

---
---
**Brooke Hedrick** *February 16, 2016 03:06*

Here's the base of what I used to convert mine with the lightobject DSP 7.  There's a link to a pdf from here.



[http://www.lightobject.info/viewtopic.php?f=16&t=1372](http://www.lightobject.info/viewtopic.php?f=16&t=1372)


---
**Brien Watson** *February 17, 2016 03:24*

Thank you very very much Brooke.  This is going to be a huge help.  The R 5 does not come with stepper motor drivers and that was stumping me somewhat...  No know I'll need to order those.


---
**Brooke Hedrick** *February 17, 2016 03:52*

**+Brien Watson**, My kit came with two stepper drivers and I ordered another for the z-table.


---
**Brien Watson** *February 22, 2016 01:49*

Everything is a lot clearer now.  😃


---
*Imported from [Google+](https://plus.google.com/111917591936954651643/posts/RDRQ8pGxiVB) &mdash; content and formatting may not be reliable*
