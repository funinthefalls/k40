---
layout: post
title: "Is there a fuse breaker that will cause the laser to stop firing it was working earlier today when I went back and turned everything back on to get started I'm On my learning curve everything works with the lasers and firing"
date: December 08, 2015 20:03
category: "Hardware and Laser settings"
author: "Timothy \u201cMike\u201d McGuire"
---
Is there a fuse breaker that will cause the laser to stop firing it was working earlier today when I went back and turned everything back on to get started I'm On my learning curve everything works with the lasers and firing any ideas 





**"Timothy \u201cMike\u201d McGuire"**

---
---
**Gary McKinnon** *December 08, 2015 20:25*

With some of the older models a few people had problems with the machine not being properly grounded. I don't know enough about electronics to check that but there may be some answers in some of these links :



[https://www.google.co.uk/search?site=&source=hp&q=k40+laser+cutter+properly+grounded&oq=k40+laser+cutter+properly+grounded&gs_l=hp.3...822158.832971.0.833082.59.44.7.6.6.0.190.3597.35j8.43.0....0...1c.1.64.hp..26.33.2174.0.AXamHoPB9HA](https://www.google.co.uk/search?site=&source=hp&q=k40+laser+cutter+properly+grounded&oq=k40+laser+cutter+properly+grounded&gs_l=hp.3...822158.832971.0.833082.59.44.7.6.6.0.190.3597.35j8.43.0....0...1c.1.64.hp..26.33.2174.0.AXamHoPB9HA)


---
**Gary McKinnon** *December 08, 2015 20:29*

What controller board do you have, and is your software MoshiDraw ?




---
**David Richards (djrm)** *December 08, 2015 20:43*

There may be some kind of interlock switch to prevent the laser firing if, say, the lid is open etc. The switch is connected to some of the power supply terminals. There will be no laser output unless the power control is turned up to above a few milliamps. There may be an LED on the power supply which should come on when the lase is active, this should come on when the test fire button is pressed. hth David.


---
**Timothy “Mike” McGuire** *December 08, 2015 21:57*

**+Gary McKinnon**  

 here is a short video of what I'm getting	


{% include youtubePlayer.html id="6HtKIqtjAuo" %}
[https://www.youtube.com/watch?v=6HtKIqtjAuo](https://www.youtube.com/watch?v=6HtKIqtjAuo)


---
**Scott Thorne** *December 08, 2015 22:32*

It's a loose connection, the tube is bad, or the power supply is shot....is the tube leaking water outside of the cooling shell?﻿


---
**David Richards (djrm)** *December 08, 2015 22:48*

There is a also tiny red test fire button on the power supply alongside the connectors, when pressed the nearby LED should come on together with the laser. I could not tell too much from your video but I think most people will have had to fix some loose connections when commissioning a new machine. n.b. Always use proper eye protection, especially when operating with the lid open. hth David.


---
**Scott Thorne** *December 08, 2015 22:50*

Be careful using the test fire button, it fires the laser at 30mA....full power....can burn the laser tube super fast.


---
**Scott Thorne** *December 08, 2015 22:52*

There is a fuse inside the power supply, you have to take it apart to get to it.


---
**Timothy “Mike” McGuire** *December 08, 2015 23:07*

**+david richards** 

There is a test fire button on the power supply right next to a green LED which stays on all the time when I press the test fire button nothing happens as far as the laser goes but there is a low-frequency that comes from the power supply when the button is pressed for test fire


---
**David Richards (djrm)** *December 08, 2015 23:17*

**+Scott Thorne** Thanks for sharing that Scott, I never knew - neither have I had cause to press the test button. Do you know if the button defeats any/all the interlock settings. Sounds as if its trying if there is a noise and light coming on. But the light should come on when operated manually too. Obviously something isn't right on Timothy's machine. The head would not do its initial homing if the board was totally broken or not powered. Timothy, does the head move when you send it a job or move the head with the PC software ? Does the power supply LED flicker when you send it a job or press the front panel test button? D


---
**Timothy “Mike” McGuire** *December 08, 2015 23:24*

Yes it homes fine I can send a job to the machine and it carries out the job but no laser it's beginning to sound likeThe high-voltage rail on the power supply maybe 


---
**Scott Thorne** *December 08, 2015 23:28*

Then I'm guessing it the tube...if you hear a hum from the power supply when you press the test fire button then it's probably working...the test fire button bypass the pot switch on the control board....use it wisely!


---
**Timothy “Mike” McGuire** *December 09, 2015 01:46*

How would I bypass the pot switch is there anyway to test tube


---
**Scott Thorne** *December 09, 2015 10:46*

Sorry about the last text, the power supply bypasses the pot switch automatically...I recommend not using the test fire switch, because it sends full power to the tube.


---
**Timothy “Mike” McGuire** *December 11, 2015 17:48*

Update to my laser not firing. According to the eBay seller it's the power supply the high-powered rail is not activating so they are sending a new power supply which will take about three or four weeks for more we are talking about China so much forgetting Christmas presents done this year..

There's any question what controller board should I look at for replacing the existing controller I hate this software thanks to everybody for your help


---
**Fred Padovan** *December 18, 2015 15:16*

  Can i ask who the seller was I'm have problems with mine too, I was told he would replace the laser  tube in 7 to 10 days ...still waiting....     : ( 

Was planing on making Christmas stuff with mine,  Maybe next year


---
*Imported from [Google+](https://plus.google.com/109629943464502534866/posts/2PPkQH1x6Jk) &mdash; content and formatting may not be reliable*
