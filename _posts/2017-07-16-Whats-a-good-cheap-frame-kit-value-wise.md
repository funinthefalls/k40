---
layout: post
title: "What's a good cheap frame/kit (value wise;"
date: July 16, 2017 23:20
category: "Modification"
author: "ThantiK"
---
What's a good cheap frame/kit (value wise; price per area) that I could transfer all the K40 components into?



Are there plans with materials that cost around $200-$300 (T-slot?) that would give me a workable 2ftx1.5ft, or similar?  I'd like to be able to at least cut something the size of an ultimaker frame out of 6mm wood...





**"ThantiK"**

---
---
**Ariel Yahni (UniKpty)** *July 16, 2017 23:25*

Haven't seen a kit but you can just shrink **+Peter van der Walt**​ Freeburn. Your budget should get you there


---
**Anthony Bolgar** *July 16, 2017 23:39*

That is one choice, there are also a few other designs on Openbuilds.


---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2017 00:08*

If you're doing that, I need the empty k40 shell. 


---
**Justin Mitchell** *July 17, 2017 11:46*

The shell / box was the most valuable bit of the K40, the tube, optics, psu and controller are easy enough to source :)




---
**joe mckee** *July 17, 2017 15:49*

I have lots of K40 shells but they are rather far away to ship , it would be cheaper to make one ...




---
**ThantiK** *July 17, 2017 15:52*

**+Justin Mitchell** honestly I should have built from the ground up 


---
**Anthony Bolgar** *July 17, 2017 18:03*

Lightobject sells some frame kits.


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/25t2R6ujCXz) &mdash; content and formatting may not be reliable*
