---
layout: post
title: "Anyone with experience cutting cards on the K40, when I try the card just gets dirty because of the dust"
date: March 30, 2016 10:54
category: "Discussion"
author: "Tony Schelts"
---
Anyone with experience cutting cards on the K40, when I try the card just gets dirty because of the dust.  

Thanks. 





**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 11:48*

By card, are you referring to thick cardstock paper (e.g. 200gsm+)? Or something in particular?



I have cut quite a lot of 200gsm cardstock when doing tests for my projects and it will usually end up with scorched edges, even with air assist on. I imagine the only real solution would be taping it to protect the edges from excess burn/soot marks, however for my purposes I haven't needed perfectly clean edges on cardstock.


---
**Scott Marshall** *March 30, 2016 14:21*

I think you mean playing cards? If so, most have a coating (usually polyethylene (LDPE)), which tends to melt and burn with a smoky residue. The only solution is to find uncoated cards or ones coated with a laser friendly  material.



I have a Silhouette Cameo CNC cutter (around$250) I use for paper cutting, it beats the laser hands down for speed and precision in paper, cardstock and Tee shirt vinyl. It handles 12" wide material up to 12 feet long. Worth the money if you do much of that kind of work. The only downside is it's software which is proprietary and not as user friendly as decent CAD or drawing suites. It works OK, but it's one more to learn.


---
**Tony Schelts** *March 30, 2016 18:51*

yes 200gsm card. a friend of mine is getting married and she wanted me to see if i can cut them for her. too much soot i am using air assist


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 23:20*

**+Tony Schelts** I would start by firstly dropping the power as low as possible. My machine runs @ approximately 4mA at the lowest I can get it to do anything noticeable. Then I would increase the cut speed to something like 30 or 50mm/s. Then I would run the cut. Might take 2 passes to get through it, and will probably leave some soot on the edge, but not a great deal. If you put masking tape on the edge may prevent/minimise it, but I'm not certain on that.


---
**Tony Schelts** *March 31, 2016 11:47*

Thanks Yuusuf and all


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/DSnDCEvCjxt) &mdash; content and formatting may not be reliable*
