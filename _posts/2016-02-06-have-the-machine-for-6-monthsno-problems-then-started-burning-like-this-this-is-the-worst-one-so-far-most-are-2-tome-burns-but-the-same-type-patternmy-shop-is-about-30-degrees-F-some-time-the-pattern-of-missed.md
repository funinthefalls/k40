---
layout: post
title: "have the machine for 6 months...no problems, then started burning like this ( this is the worst one so far, most are 2 tome burns, but the same type pattern...my shop is about 30 degrees F, some time the pattern of \"missed\""
date: February 06, 2016 23:55
category: "Discussion"
author: "Top Notch Woodworks"
---
have the machine for 6 months...no problems, then started burning like this ( this is the worst one so far, most are 2 tome burns, but the same type pattern...my shop is about 30 degrees F, some time the pattern of "missed" engraving is real light color, other time real dark. and it also seems to move from time to time....ANY help would be greatly appreciated, I cleaned the mirrors..., Ken

![images/8568504719793501665ffbc4792abd93.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8568504719793501665ffbc4792abd93.jpeg)



**"Top Notch Woodworks"**

---
---
**Phillip Conroy** *February 07, 2016 11:07*

my guss is mirror alignment as it engraves soildly at top and bottom


---
**Patryk Hebel** *February 07, 2016 13:01*

Yes I had exactly same problem. The mirror alligment might be a little off. Take a piece of cardboard test it in 4 corners and center to make sure laser reaches everywhere.


---
**Top Notch Woodworks** *February 07, 2016 19:17*

Patryk, was your burn as bad as this? Also one you aligned the mirrors did is fix it for you?


---
**Patryk Hebel** *February 08, 2016 00:10*

**+Top Notch Woodworks** Yes it was exactly the same. The mirror alligment just fixed it and everything is going perfectly fine now.


---
*Imported from [Google+](https://plus.google.com/101589446251356417180/posts/fWdjMBariVa) &mdash; content and formatting may not be reliable*
