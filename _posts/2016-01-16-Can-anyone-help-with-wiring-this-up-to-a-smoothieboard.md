---
layout: post
title: "Can anyone help with wiring this up to a smoothieboard?"
date: January 16, 2016 13:19
category: "Modification"
author: "Ben Alderson"
---
Can anyone help with wiring this up to a smoothieboard? 





**"Ben Alderson"**

---
---
**Ben Alderson** *January 16, 2016 17:18*

**+Peter van der Walt** I've read the guide, but trying to understand what my PSU needs to fire the laser. Do I need to set the power via ttl or pwm AND trigger the kh line to fire? Or just send a pwm or ttl signal to IN? 


---
**Ben Alderson** *January 16, 2016 17:18*

K+ line, not kh! 


---
**Ben Alderson** *January 16, 2016 17:22*

Ahhhh the joys of undocumented power supplies! Any idea how to get smoothie to send an activation signal and a pwm signal? Need to put a level converter together on Monday to check this for reals.


---
**Ben Alderson** *January 16, 2016 18:04*

Thanks for that - trouble is I don't have a TH pin, only K-,  K+, IN, G and 5V. Connecting the two K pine together causes laser to fire (used this with a momentary switch to do alignment), just need a level converter to try out pwm now. 


---
**Ben Alderson** *January 17, 2016 09:34*

On the original panel only one k line was connected - I assumed (wrongly) that the panel board just fed it a timed pulse, but your explanation makes so much more sense. Once my level converter turns up I can test it properly. Thanks. 


---
**Ben Alderson** *January 19, 2016 15:20*

Sorry to dig this up again - just realised the photo I thought I attached to the original post didn't show up, so apologies if the text alone was a bit cryptic!



Anyway - got my level shifters today, wired one up to pin 2.5 on the Smoothie (big mosfet) and I'm getting something approaching a PWM signal at 5v now (more like 4.2-4.4 according to my scope, but the 5V input was only showing as 4.7 so that could be the cause there).



Anyway: I've got PWM going to IN, and nothing going to K+ or K-. So far no joy, no laser firing.



Looking at those links you shared I see someone mentioned that the PWM to IN may need a capacitor - this suggests to me that it's not looking for a PWM signal, but an analog TTL voltage? Any idea what size of capacitor would be needed?



Going to try using the Switch command to K+ next...


---
**Ben Alderson** *January 19, 2016 16:26*

Ah. Sorry. Got PWM to K+ and nothing happens



Tried to IN and nothing happens.



Connected K+ to Pin 0.26 as per the switch example and it fired the laser immediately on switch on and wouldn't switch off (yay E-stop buttons!)


---
**Ben Alderson** *January 19, 2016 16:28*

(Pin 0.26 to K+ I tried pull up and pull down, same result).



So looks like PWM to K+ and a P0.26 to somewhere else to fire?


---
**Ben Alderson** *January 19, 2016 20:06*

Looking at this wiring diagram from CNC Zone that seems to match the input pins on my PSU:



[https://www.dropbox.com/s/t6841sv8iom1b41/Wiring.jpg?dl=0](https://www.dropbox.com/s/t6841sv8iom1b41/Wiring.jpg?dl=0)



If I'm reading it right, then K+ needs to be pulled down to fire the laser. The power is set by a voltage from 0 - 5V on the IN pin. Normally this is achieved by setting a pot, but if I have a PWM signal from the Smoothie I can stick a RC low pass filter on it and get an analogue  0-5V to set power.



I think this is what the poster on one of your links was hinting at with the capacitor.



I dream of a future when even cheap, grey market import electronics come with comprehensive and accurate datasheets :D


---
*Imported from [Google+](https://plus.google.com/104155514534927678351/posts/jmu97ewe6gv) &mdash; content and formatting may not be reliable*
