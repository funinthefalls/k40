---
layout: post
title: "Is there a way to make a 18\" lens fit?"
date: June 07, 2018 17:51
category: "Hardware and Laser settings"
author: "Aaron Cusack"
---
Is there a way to make a 18" lens fit? Can't get the light object air assist adapter here in the UK.

![images/d6ec930c3a7aa91d8f1b5fb8568a3122.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d6ec930c3a7aa91d8f1b5fb8568a3122.jpeg)



**"Aaron Cusack"**

---
---
**James Rivera** *June 07, 2018 18:57*

Actually, it looks like you can—from eBay.[ebay.co.uk - Details about K40 Laser Engraver Lightobject 18mm Laser Head w/air assist mount](https://m.ebay.co.uk/itm/K40-Laser-Engraver-Lightobject-18mm-Laser-Head-w-air-assist-mount/253570733848?hash=item3b09fe5718:g:7WgAAOSwOZpa2B0a)


---
*Imported from [Google+](https://plus.google.com/104714035697130129832/posts/fLzpz4XRSLF) &mdash; content and formatting may not be reliable*
