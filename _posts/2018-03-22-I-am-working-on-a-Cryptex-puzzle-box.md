---
layout: post
title: "I am working on a Cryptex puzzle box"
date: March 22, 2018 16:25
category: "Original software and hardware issues"
author: "timb12957"
---
I am working on a Cryptex puzzle box. The dxf file came from user Da3da1u5 on the Instructables website. [http://www.instructables.com/id/Laser-Cut-Cryptex/](http://www.instructables.com/id/Laser-Cut-Cryptex/) Using Draftsight, I have sorted all the pieces into 8 x 12 blocks to match my laser travel. All the pieces worked out fine, except the letters. If I open the dxf file using DraftSight, the letters appear as they should (see photo A). CorelDraw 12 will not open the dxf file, so I thought I would open the dxf file in Inkscape, then save it as a WMF file which will open with CorelDraw 12. However, when the dxf file opens in Inkscape, it looks like photo B. Anyone know how to correct this?



![images/bbfd89bee3189ab2a0d0a4be7304296d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bbfd89bee3189ab2a0d0a4be7304296d.jpeg)
![images/d339d4b7db4e5fa6cb3c27a7fa634572.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d339d4b7db4e5fa6cb3c27a7fa634572.jpeg)

**"timb12957"**

---
---
**Jonathan Davis (Leo Lion)** *March 22, 2018 17:09*

I would suggest that I might be a formating issue 


---
**James Rivera** *March 22, 2018 21:47*

All of the letters seem to have a "\pxq" prefix. I suspect this is converting some whitespace character. Check that the spacing you used in Draftsight is not inserting some other characters that Inkscape cannot convert (perhaps because it is Unicode?).


---
**Abe Fouhy** *March 22, 2018 21:49*

Looks like inkscape is having an issue with the path of the letters oddly. If you look close it is displaying an error about the path on each letter. It is possible that it may be using a font that you don't have and it is trying to substitute the font and failing. Can you save it as a different type of vector file that encapulates the font like ps, eps, or pdf and then open it in inkscape?


---
**timb12957** *March 23, 2018 01:04*

I solved it the long way. Instead of using the dxf file I downloaded, I just started from scratch in DraftSight. I made a top row of squares, then created individual letters A-Z. With one row done, I used the array feature to get 10 rows all the same.


---
**Abe Fouhy** *March 24, 2018 22:30*

Nice!


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/5HXk8tYEBUN) &mdash; content and formatting may not be reliable*
