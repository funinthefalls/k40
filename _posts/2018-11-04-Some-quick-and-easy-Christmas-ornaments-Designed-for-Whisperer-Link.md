---
layout: post
title: "Some quick and easy Christmas ornaments. Designed for Whisperer. Link:"
date: November 04, 2018 22:01
category: "Repository and designs"
author: "Clayton Braun"
---
Some quick and easy Christmas ornaments. Designed for Whisperer.  



Link: [https://drive.google.com/file/d/137XeCIhdUltXB8nkGrWqkZ1ChK8xFyDC/view?usp=drivesdk](https://drive.google.com/file/d/137XeCIhdUltXB8nkGrWqkZ1ChK8xFyDC/view?usp=drivesdk)





**"Clayton Braun"**

---
---
**Martin Dillon** *November 04, 2018 22:16*

No access.  Can you post a shareable link?


---
**Clayton Braun** *November 04, 2018 22:54*

**+Martin Dillon**  added it to post.  Forgot about the sharing part. 


---
*Imported from [Google+](https://plus.google.com/+ClaytonBraun/posts/74zzgQCQwgc) &mdash; content and formatting may not be reliable*
