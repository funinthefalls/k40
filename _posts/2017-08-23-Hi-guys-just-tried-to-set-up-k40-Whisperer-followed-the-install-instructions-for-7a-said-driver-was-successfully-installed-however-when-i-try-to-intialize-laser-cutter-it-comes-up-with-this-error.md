---
layout: post
title: "Hi guys, just tried to set up k40 Whisperer, followed the install instructions, for 7a, said driver was successfully installed, however when i try to intialize laser cutter, it comes up with this error?"
date: August 23, 2017 10:20
category: "Discussion"
author: "Brendon Todd"
---
Hi guys, just tried to set up k40 Whisperer, followed the install instructions, for 7a, said driver was successfully installed, however when i try to intialize laser cutter, it comes up with this error? any ideas what the problem is?

![images/f50fb5a74f3d3002c4b9a028e2c85a15.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f50fb5a74f3d3002c4b9a028e2c85a15.png)



**"Brendon Todd"**

---
---
**Scorch Works** *August 23, 2017 11:40*

It looks like you managed to download the windows 64bit executable that was compiled bad.   Download K40 Whisperer again.     


---
**Scorch Works** *August 23, 2017 11:49*

The bad version was posted for a few hours but has been replaced.  The program code did not change so it is the same revision.  The usb part of the executable was broken because it was built with the wrong version of PyUSB.


---
**Scorch Works** *August 24, 2017 14:10*

**+Brendon Todd**​ were you able to get K40 Whisperer to initialize?


---
**Brendon Todd** *August 25, 2017 04:09*

yes, that worked great!!


---
*Imported from [Google+](https://plus.google.com/104627062651775475102/posts/hicJyq9tERb) &mdash; content and formatting may not be reliable*
