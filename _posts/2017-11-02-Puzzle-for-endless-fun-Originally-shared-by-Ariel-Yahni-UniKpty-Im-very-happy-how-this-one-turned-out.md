---
layout: post
title: "Puzzle for endless fun :) Originally shared by Ariel Yahni (UniKpty) Im very happy how this one turned out"
date: November 02, 2017 14:47
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
Puzzle for endless fun :)



<b>Originally shared by Ariel Yahni (UniKpty)</b>



Im very happy how this one turned out. Made this for my kid. It's a puzzle. Good luck solving it :)





![images/b2e21cfe8898b8bdcea0df6ecac34513.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2e21cfe8898b8bdcea0df6ecac34513.jpeg)
![images/e42a35b975e60a01c50e7b5f5e14a25d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e42a35b975e60a01c50e7b5f5e14a25d.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**BEN 3D** *November 03, 2017 06:54*

Cool!


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/7gtTZDmUrQU) &mdash; content and formatting may not be reliable*
