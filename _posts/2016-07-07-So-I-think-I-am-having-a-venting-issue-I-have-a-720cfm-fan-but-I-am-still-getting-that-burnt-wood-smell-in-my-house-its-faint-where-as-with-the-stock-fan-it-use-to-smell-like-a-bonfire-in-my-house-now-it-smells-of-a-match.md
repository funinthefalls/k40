---
layout: post
title: "So I think I am having a venting issue I have a 720cfm fan but I am still getting that burnt wood smell in my house it's faint where as with the stock fan it use to smell like a bonfire in my house now it smells of a match"
date: July 07, 2016 03:13
category: "Discussion"
author: "3D Laser"
---
So I think I am having a venting issue I have a 720cfm fan but I am still getting that burnt wood smell in my house it's faint where as with the stock fan it use to smell like a bonfire in my house now it smells of a match could I have a leak causing it or is that typical?  I heard carbon filters could help but wanted to see if something else could be wrong here is a pic of my setup 

![images/eafa382890c8ee6ad77e62824afd88d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eafa382890c8ee6ad77e62824afd88d8.jpeg)



**"3D Laser"**

---
---
**Joe Alexander** *July 07, 2016 03:26*

are you keeping the cut pieces and scrap in closed containers? I'd imagine that all that cut surface area that is singed will be off gassing odor to some degree. Maybe a light air freshener or an "odor eater" per say.


---
**Scott Marshall** *July 07, 2016 04:50*

Seal your ductwork past the pump well, joints and seams (looks like the exposed part there is taped up nice) because when you put the fan on the laser end of the ductwork the ductwork is under pressure and any small leaks will push the smoke into your house.

It's best to put the fan on the wall penetration, that way any leaks just draw in room air, but you're already all installed so that's not practical.



Another issue with venting is where the discharge is located on the outside. The smoke can easily be drawn back into the house, especially if you have a really big blower. You are drawing the entire house down to a negative pressure, and that makes all the little air leaks draw in outside air as make up air. Solutions are to get the discharge up and away from the house as far as practical. Sometimes all you need is 6' of duct on the outside to get the smoke well away from the house. It depends a lot on prevailing winds rooflines, other nearby buildings.



The whole negative pressure thing is magnifier by other air movers, range hoods, furnaces, AC, whole house fans, pellet & wood stoves, and gas heaters, even waters heaters pull air out of the house and all provide a potential path back in when they are off.



It gets complicated.



In the summer you can open a window in your laser room, and it will supply make up air so your laser blower does't pull down the whole house.



Industrial machinery has a make up air duct from outside run directly into the machinery cabinet, so the system is a complete loop isolated from human space. (required by code everywhere I've installed machinery. (in commercial buildings a 'cheat' is to use the roof drains to pull in the make up air for smaller systems)



If you're ambitious and have a outside wall in your laser room, or nearby, you could install a dryer vent kit to draw in your fresh air directly to your laser. That's really the right way to do it, but few of us go through the effort. (I did do it for my pellet stove though - required for code)



On last thought: Make sure you've got a good CO2 alarm.



Hope it helps.



Scott



.


---
**Alex Krause** *July 07, 2016 05:37*

**+Scott Marshall**​ you never cease to amaze me with general knowledge! **+Corey Budwine**​ you may try a bit of the gasketing between your exhaust joints and use aluminized tape under the duct tape in conjunction might keep some of the leaks under control


---
**Alex Krause** *July 07, 2016 05:43*

Also the residue left from cutting in the bed will leave a persistent odor the lignins and other byproducts from the combustion of cellulose will linger and hard to remove... that's why my smoker always smells like I've just cooked something even thow it may have been weeks/months since I've done anything with it


---
**Pippins McGee** *July 07, 2016 06:21*

**+Alex Krause** on that note alex,

what do you use to clean cutting residue? particularly from wood like MDF.

leaves permanent sticky residue, alcohol doesn't clean it off.. really cakes it on.


---
**Alex Krause** *July 07, 2016 06:28*

**+Pippins McGee**​ try a stove/range cleaning solution they usually have a foaming action and come with a citrus scent... you can always send it thru the dish washer or steam/power wash after to remove the remaining residue 


---
**Phillip Conroy** *July 07, 2016 08:51*

I use my laser to only cut 3mm mfd and weekly i remove a tin tray i made jp and wash it under hot running water and usd a brush comes off easily


---
**Pippins McGee** *July 07, 2016 23:26*

**+Phillip Conroy** 

mm I'm going to need a different Bed.

I made one out of super fragile super thin aluminium honeycomb material. Like paper.

Can't wash or scrub it, can't even wipe it without it bending and tearing so rely on it not filthing up in the first place, or a cleaning solution that would dissolve and rinse it off.



all good, going to make a new bed out of expanded steel mesh instead...


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/GywnjqEVCmM) &mdash; content and formatting may not be reliable*
