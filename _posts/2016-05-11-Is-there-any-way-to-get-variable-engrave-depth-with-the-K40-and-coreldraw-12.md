---
layout: post
title: "Is there any way to get variable engrave depth with the K40 and coreldraw 12?"
date: May 11, 2016 18:31
category: "Smoothieboard Modification"
author: "ben crawford"
---
Is there any way to get variable engrave depth with the K40 and coreldraw 12? I know white is not engrave and black is engrave but is there like a %50 gray where it engraves at half power so you can do shading?





**"ben crawford"**

---
---
**Alex Krause** *May 11, 2016 19:15*

You need pwm control of the laser for that I believe... But I could be wrong


---
**Tony Sobczak** *May 11, 2016 20:20*

Possible with smoothie? 


---
**Alex Krause** *May 11, 2016 20:24*

 **+Peter van der Walt**​ can laserweb process grayscale rasters with varying power outputs while running?


---
**Alex Krause** *May 12, 2016 01:33*


{% include youtubePlayer.html id="IebrIhNqfQM" %}
[https://youtu.be/IebrIhNqfQM](https://youtu.be/IebrIhNqfQM)﻿   this is a video running a laser diode on smoothie not sure if the same will totally apply with the k40 but I am going to give it a shot here in a few weeks after I get comfortable with the machine I will convert to smoothie


---
**Alex Krause** *May 12, 2016 03:39*

:) **+Peter van der Walt**​ thank you! That is a huge upgrade from the crappy stock software


---
**ben crawford** *May 12, 2016 14:30*

Does anyone have a link to a control board upgrade that would let me use this laserweb?


---
**Alex Krause** *May 12, 2016 15:35*

**+ben crawford**​ USA : [http://shop.uberclock.com/collections/smoothie](http://shop.uberclock.com/collections/smoothie)  Europe : [http://robotseed.com/index.php?id_category=7&controller=category&id_lang=2](http://robotseed.com/index.php?id_category=7&controller=category&id_lang=2)


---
**Alex Krause** *May 12, 2016 16:51*

There are other boards that run the smoothie firmware like the azsmz (aliexpress) and the azteeg boards from [www.panucatt.com](http://www.panucatt.com)


---
**Stephane Buisson** *May 12, 2016 21:38*

the 4XC come with ethernet port (more easy with Visicut)


---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/fzg2EaYtdB3) &mdash; content and formatting may not be reliable*
