---
layout: post
title: "Issue K40 Whisperer \"Sending Data Stopped: (K40 Whisperer V0.15) --------------------------- maximum recursion depth exceeded in cmp --------------------------- OK ---------------------------\" Hi Scorch Works , I create a PNG"
date: November 29, 2017 00:58
category: "Original software and hardware issues"
author: "BEN 3D"
---
Issue K40 Whisperer



"Sending Data Stopped:  (K40 Whisperer V0.15) --------------------------- maximum recursion depth exceeded in cmp --------------------------- OK    ---------------------------"



Hi **+Scorch Works**,

I create a PNG in Inkskape with the Letter HEMMI in Old Cologne Font.

I opened it with Paint and add some lines.

I open a new Inkslape Doc and imported the edited PNG.

I use a Inksape function to convert it.

I edit the result as red line like you explained in your Video (normaly it works)

I saved it as a 4 cm high and 20 cm long version.

I imported it to K40 Whisperer V0.15

Result: Error Message appairs.

Sending Data Stopped:  (K40 Whisperer V0.15) --------------------------- maximum recursion depth exceeded in cmp --------------------------- OK    <s>-------------------------</s>

Could you explaint what goes wrong? I not shure what the correct interpretation of the message is. 



![images/58aedb565079cb965fedf1e53afb321e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/58aedb565079cb965fedf1e53afb321e.png)
![images/1281f12ea1dcdf21f581c5c3396cce83.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1281f12ea1dcdf21f581c5c3396cce83.png)

**"BEN 3D"**

---
---
**BEN 3D** *November 29, 2017 01:01*

Red Line Setting

![images/f636e055d72b843ab960c3b017a1e776.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f636e055d72b843ab960c3b017a1e776.png)


---
**Scorch Works** *November 29, 2017 04:15*

**+BEN 3D** can you send the svg file to my e-mail address so I can investigate. My address is in the help menu.


---
**BEN 3D** *November 29, 2017 11:09*

Hi +Scorch Works, 

Yes indeed, where could I find your mail address?

Cheers Ben


---
**Scorch Works** *November 29, 2017 12:27*

**+BEN 3D** My e-mail address is in the K40 Whisperer 'Help' menu


---
**BEN 3D** *November 29, 2017 13:30*

**+Scorch Works**

Fine, I will send it to you this afternoon.


---
**Scorch Works** *November 29, 2017 16:59*

I am pretty sure the error is a result of the many lines that are nearly on top of each other in your SVG file.  K40 Whisperer should be able to handle it so your file will be a good test file while I am fixing it.  Thanks for sending it.

![images/a55668e14a0bfe106007b68ac58bb478.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a55668e14a0bfe106007b68ac58bb478.png)


---
**BEN 3D** *November 29, 2017 22:17*

**+Scorch Works**

Ah, nice. that is a result of the convertation function in german "Bitmap nachzeichen" see my screenshot. May be there is a way to reduce it to a single line in the svg File with Inkscape automatically


---
**BEN 3D** *November 29, 2017 22:22*

Ah, if you know what you do, it was easy to fix, in the svg file with inkscape.I delete the useless lines in a few seconds, that should be work now.


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/JZz6gWQWWaS) &mdash; content and formatting may not be reliable*
