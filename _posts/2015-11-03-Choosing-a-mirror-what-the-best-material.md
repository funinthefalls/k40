---
layout: post
title: "Choosing a mirror, what the best material ?"
date: November 03, 2015 14:58
category: "Hardware and Laser settings"
author: "Stephane Buisson"
---
Choosing a mirror, what the best material ?



Si, Mo, HQ Znse, CoCrPt ?



[http://www.ebay.co.uk/itm/HQ-ZnSe-GaAs-Focus-Focal-Lens-Si-Mo-K9-Reflective-Mirrors-CO2-Laser-Engraving-/181596677092?var=&hash=item2a480163e4:m:meMO7Y-AHxPJ3mdppCpT1mw](http://www.ebay.co.uk/itm/HQ-ZnSe-GaAs-Focus-Focal-Lens-Si-Mo-K9-Reflective-Mirrors-CO2-Laser-Engraving-/181596677092?var=&hash=item2a480163e4:m:meMO7Y-AHxPJ3mdppCpT1mw)



or



[http://www.ebay.co.uk/itm/Dia-20mm-Platinum-CoCrPt-Reflection-Reflective-Mirror-for-CO2-Laser-Engraver-/141588635610?var=&hash=item20f7571fda:m:m_cAGpfqEn4-xe_hAJQ6FyA](http://www.ebay.co.uk/itm/Dia-20mm-Platinum-CoCrPt-Reflection-Reflective-Mirror-for-CO2-Laser-Engraver-/141588635610?var=&hash=item20f7571fda:m:m_cAGpfqEn4-xe_hAJQ6FyA)





**"Stephane Buisson"**

---
---
**Scott Howard** *November 04, 2015 06:07*

How do I become a member? 


---
**Stephane Buisson** *November 04, 2015 08:29*

**+Peter van der Walt** thank you


---
**Stephane Buisson** *November 04, 2015 08:30*

**+Scott Howard** Welcome here, by just joining the community, and sharing your experiences


---
**Kirk Yarina** *November 05, 2015 16:30*

**+Peter van der Walt** Why?



What makes Mo better than the other mirror types for our long wavelength CO2 lasers?


---
**Stephane Buisson** *November 05, 2015 16:41*

**+Peter van der Walt** thank you for your precisions.

I went for the CoCrPt because of delivery time.

(3 weeks was too long), it's quick and cheap so I could afford to have a go.



precision diameter 20mm thickness 1.75 (original is 3, but it's provided with spacer)

to unfit just unscrew from front.


---
**Kirk Yarina** *November 06, 2015 14:30*

**+Peter van der Walt** Not doubting your experience or credentials, nor sure why you'd infer that.  Just hoping you'd expand your one word answer to learn more about why it's a better material; thanks for the explanation.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/aMH18JfohNr) &mdash; content and formatting may not be reliable*
