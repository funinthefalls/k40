---
layout: post
title: "Ok im all new to the lasers and cnc ."
date: November 02, 2016 21:12
category: "Modification"
author: "paul baker"
---
Ok im all new to the lasers and cnc . I just bought a k40 and would like to upgrade the board to a smoothieboard. What all do i need to get started. Do i just need the smoothie board or do i need other parts to make this all work. Thanks.





**"paul baker"**

---
---
**Ariel Yahni (UniKpty)** *November 02, 2016 21:46*

**+paul baker**​ how tech save are you? It will on depend on that but, into order of preference: board, air assist, mirrors, lenses, focus ( bed or lens holder). I have a collection here with many links on these upgrades.  [https://plus.google.com/collection/EndMTB](https://plus.google.com/collection/EndMTB)


---
**paul baker** *November 02, 2016 21:58*

Im somewhat tech savey. I can take apart and put anything back togather but im just learning the electronics. Im more of a mechanical guy than a tech guy.


---
**Anthony Bolgar** *November 02, 2016 22:18*

Then you should have no problem upgrading your K40 to a smoothieboard (Just make sure you do not buy the MKS Sbase clone, they violate the open source licensing, and are poor quality) You would need a 3X or 4X smoothieboard, level shifter,middleman interface board, and  GLCD display and adapter (optional). Many of us have made the conversion, so help is available. Great benefit of the upgrade is that you can stop using the horrible Corel plugin, and start using LaserWeb. The level shifter and middleman board are both very inexpensive (about $20 for the 2 , and you would have 2 spare middleman boards (they come as a set of 3 from OSHpark). The level shifter is only a couple of dollars. The display and adapter are not necessary, and are almost as expensive as buying the smoothieboard X3. I would actually get the 4X smoothieboard because you can access it via network, and it has a spare driver for a rotary axis . Hope this helps.


---
**Mark Leino** *November 03, 2016 04:36*

I have three middleman boards on the way, happy help out some folks who need one when they arrive since I will have two extras


---
**Jez M-L** *November 03, 2016 14:40*

Get some micro switches for end stops as well. I got the 5xc. Bit overkill but I can use it for other projects later and just swap over the SD card to run separate machines.  Once you get a smoothieboard you will be researching a lot of further upgrades you can do relatively cheaply to automate stuff from the board. I have a SSR that controls my fan and a 12v PSU to activate a pneumatic solenoid to turn my air assist. All of this can be controlled via gcode so that the air assist and extractor fan turn on just before cutting and turn off when the job is complete. No need to do it manually. Lots of ideas to implement!


---
**paul baker** *November 04, 2016 12:18*

Thanks for all the info. i will get this stuff ordered today. I have some micro switches for end stops and im looking into a new bed at this time. Were can i find the info on wiring the boards once they arrive.


---
**Mark Leino** *November 04, 2016 12:43*

Some of the guys just posted some help links for me, i'll get a link quick


---
**Jez M-L** *November 04, 2016 12:51*

Paul. It depends on the psu you have for your laser but the guides on the smoothieboard site kicked me off in the right direction. Then it was a case of more googling for the setup I wanted (no pot control. All my power is controlled from gcode) and a bit of testing and messing about with the smoothie config. Short of that there are these forums if you get proper stuck. With the boards you get a boat load of connectors and have to make these up but the smoothieboards are pretty bulletproof when it comes to wiring things in the wrong way round. Just remember to power everything down before plugging and unplugging stuff. Especially the steppers. You can blow the steppers drivers on the board of you do this, however you can add your own stepper drivers and still have them run by the board. I had great fun pulling everything apart and rebuilding, but I also had moments of shouting and screaming at it. How is your alignment on the K40. If you have it dialled in then I would suggest marking everything so that if you need to take it apart you can put it back in roughly the same place. I got a bit too excited when tearing down and had a bloody made aligning everything once putting it all back together. 


---
**Mark Leino** *November 04, 2016 12:55*

[https://plus.google.com/101181750414718280484/posts/43cLxcaNYn5](https://plus.google.com/101181750414718280484/posts/43cLxcaNYn5)


---
*Imported from [Google+](https://plus.google.com/106678406934842798729/posts/WvW3YjhFx63) &mdash; content and formatting may not be reliable*
