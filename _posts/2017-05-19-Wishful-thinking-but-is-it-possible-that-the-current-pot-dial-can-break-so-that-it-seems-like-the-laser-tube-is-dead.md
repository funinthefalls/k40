---
layout: post
title: "Wishful thinking but is it possible that the current pot / dial can break so that it seems like the laser tube is dead?"
date: May 19, 2017 18:00
category: "Discussion"
author: "Adam J"
---
Wishful thinking but is it possible that the current pot / dial can break so that it seems like the laser tube is dead? How would I go about testing my laser tube?



Laser just died while cutting, water flow was good and temp okay.



Desperately need a new tube, can anybody recommend a UK stockist?



Thanks!





**"Adam J"**

---
---
**Claudio Prezzi** *May 19, 2017 18:11*

If you have a multimeter, measure the resistance between the middle pin of the pot and one of the other.



If you don't have a multimeter, disconnect the pot (at the PSU side) an connect the IN pin to the 5V pin (labels on the PSU). Warning: this makes it full power! You should then be able to testfire with the test switch.


---
**Adrian Godwin** *May 19, 2017 18:46*

I'd recommend Just Add Sharks



[shop.justaddsharks.co.uk - Laser Tubes – Just Add Sharks](https://shop.justaddsharks.co.uk/collections/laser-tubes)






---
**Adam J** *May 19, 2017 18:56*

Thanks for the quick reply Claudio, really appreciated. I'm not too good with a multimeter so taken pics of what I'm doing. Does this mean the pot is okay?



Thanks!

![images/c5de5061b7811114e306d3a4f6b15880.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c5de5061b7811114e306d3a4f6b15880.jpeg)


---
**Adam J** *May 19, 2017 18:56*

![images/b99dcdd3bda5f38d7efb0564a76e0125.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b99dcdd3bda5f38d7efb0564a76e0125.jpeg)


---
**Adam J** *May 19, 2017 19:13*

Adrian, do you have one of their tubes? The 30W one I'm guessing? Does it fit OK?


---
**Claudio Prezzi** *May 19, 2017 19:33*

**+Pikachupoo** Is there a cable connected to the middle pin? I don't see one on the picture. Just turn the pot while measuring. It's ok if the value changes between something like 0 and 500 Ohm (or 1kOhm).


---
**Adam J** *May 19, 2017 19:55*

My bad, I connected the wires and it seems to be working, changing between 0 and 500 Ohm.



Everything is working but no laser firing. I've read similar posts on here that suggest it could be the PSU. Laser power just disappeared middle of a job. Do laser tubes just die this way suddenly? Without specialist equipment, I understand it's impossible to test the high voltage side of the PSU?






---
**Adam J** *May 19, 2017 22:50*

Well, I've ordered a new PSU and LightObjects replacement tube. I guess it's going to be one or the other and it's essential to keep spares. I'll post which of the two it was for the records!


---
**Frank Fisher** *May 24, 2017 06:56*

My pot was loose on delivery and is also inclined to turn slightly by itself during use, presume vibration?  It seems incredibly sensitive and the slightest turn can more than double the power.  Is there a better pot to fit?


---
**Claudio Prezzi** *May 24, 2017 07:11*

You can replace the pot with a multi turn pot like this [http://www.ebay.com/itm/1-3590S-2-502L-5K-Ohm-Rotary-Knob-Wirewound-Precision-Potentiometer-10-Turn-New/332212855800?_trksid=p2047675.c100010.m2109&_trkparms=aid%3D555018%26algo%3DPL.SIM%26ao%3D1%26asc%3D40130%26meid%3Da29b086576e940d2abeb1d7cb5091beb%26pid%3D100010%26rk%3D4%26rkt%3D6%26sd%3D262504968787](http://www.ebay.com/itm/1-3590S-2-502L-5K-Ohm-Rotary-Knob-Wirewound-Precision-Potentiometer-10-Turn-New/332212855800?_trksid=p2047675.c100010.m2109&_trkparms=aid%3D555018%26algo%3DPL.SIM%26ao%3D1%26asc%3D40130%26meid%3Da29b086576e940d2abeb1d7cb5091beb%26pid%3D100010%26rk%3D4%26rkt%3D6%26sd%3D262504968787)


---
**Frank Fisher** *May 24, 2017 07:14*

That looks perfect, so it can give really fine adjustment at the low end?


---
**Claudio Prezzi** *May 24, 2017 07:17*

At least much finer than the original pot, and you can block it from moving.


---
**Frank Fisher** *May 24, 2017 11:41*

Just ordered one, will be 3 weeks but I can wait. Don't want to mess about before my upcoming event deadline anyway, too risky.




---
**Frank Fisher** *July 03, 2017 16:27*

Ha it finally turned up today, that's 6 weeks, and the connectors are marked differently to the standard one so now scouring the internet (in vain so far) for a wiring scheme.


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/QFyeS4rrrUu) &mdash; content and formatting may not be reliable*
