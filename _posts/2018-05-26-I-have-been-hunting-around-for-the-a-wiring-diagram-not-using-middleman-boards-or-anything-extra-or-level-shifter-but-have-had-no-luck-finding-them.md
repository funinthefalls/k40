---
layout: post
title: "I have been hunting around for the a wiring diagram (not using middleman boards or anything extra, or level shifter) but have had no luck finding them"
date: May 26, 2018 18:19
category: "Smoothieboard Modification"
author: "Mark Leino"
---
I have been hunting around for the a wiring diagram (not using middleman boards or anything extra, or level shifter) but have had no luck finding them. I moved my k40 a couple times and in the process a wire came loose and I decided to rewire it, but I can't find any of them documentation. I had it running fine.



I'm using a smoothieboard (original, bought about a year and a half ago) k40 blue box laser, stock power supply. I had it running back with laserweb3 and it was working fine. Does anyone have location of the easiest way to wire these? Most of the links seem to be broken I have been checking.



Thanks all.





**"Mark Leino"**

---
---
**Don Kleinschnitz Jr.** *May 26, 2018 19:55*

It would be helpful to list broken links (especially mine :)). 



Can you identify what wire came loose. Also what is not working. 



Also check the <i>Getting Started</i> post thats pinned on the community for more info. 


---
**Mark Leino** *May 27, 2018 00:17*

I have movement now,  it connects,  but laser  test does not work.  Does anyone have a file I could test,  to ensure things are working right?  Is the a location of a file dump with firmware settings that are correct?  I'm using a cnc version of firmware. 



Several of the getting started posts links are broken.  I will try to list them.  **+Don Kleinschnitz**​ all your info seems to be working,  but there is a lot of sorting out of info needed.  Need a k40 conversion for dummies post


---
**Don Kleinschnitz Jr.** *May 27, 2018 12:45*

**+Mark Leino** 

Are you referring to this "Getting Started" : [plus.google.com - GETTING STARTED?..........NEED HELP? IF YOU ARE NEW TO THIS COMMUNITY, G+, A...](https://plus.google.com/+DonKleinschnitz/posts/BLPdnUPo4ds)



I tried all these links with success?



Hopefully you have visited my blog and realize there is a conversion index and labeled sections on the right side bar? :)



[https://donsthings.blogspot.com/](https://donsthings.blogspot.com/)




---
*Imported from [Google+](https://plus.google.com/+MarkLeino/posts/EMHYgMeTWfw) &mdash; content and formatting may not be reliable*
