---
layout: post
title: "Is there any better software than the CorelLaser program that comes with the equipment?"
date: July 30, 2016 19:13
category: "Software"
author: "Jeremy A"
---
Is there any better software than the CorelLaser program that comes with the equipment? I was ok using it till my antivirus picked up a trojan in the CorelLaser folder. After it deleted the assemble.dll file CorelLaser no longer opens.

Are there any other options for engraving software?

![images/ba7e425fa4fc472f752babe0a8b531e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ba7e425fa4fc472f752babe0a8b531e6.jpeg)



**"Jeremy A"**

---
---
**Ned Hill** *July 30, 2016 19:47*

If your K40s came stock with coreldraw 12 try upgrading to coreldraw X5. Yuusuf has a hosted copy in his dropbox folder [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XcCXxJgh3hT](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XcCXxJgh3hT) 

Just follow the readme file.


---
**Jeremy A** *July 30, 2016 19:54*

I have CorelX8 & it works perfectly. 

My problem is with CorelLaser installing a backdoor virus on my system every time I install it.


---
**Ned Hill** *July 30, 2016 20:00*

You can try the corellaser in the dropbox. 


---
**Jeremy A** *July 30, 2016 20:08*

i'll give it a shot!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 31, 2016 02:11*

I am fairly certain that antivirus is picking it up as a false positive. You could add exceptions into your antivirus to ignore the CorelLaser & related files. However, to your original question regarding better software the only real option is via a controller upgrade (Smoothie, as Peter mentioned in another post).


---
*Imported from [Google+](https://plus.google.com/+jeremyaversa/posts/Ei8Lkhx7NBT) &mdash; content and formatting may not be reliable*
