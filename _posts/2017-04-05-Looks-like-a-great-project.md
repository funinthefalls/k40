---
layout: post
title: "Looks like a great project!"
date: April 05, 2017 23:42
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Looks like a great project!

[http://www.thingiverse.com/thing:2222031](http://www.thingiverse.com/thing:2222031)





**"HalfNormal"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2017 00:51*

Interesting. I like the idea of modular pieces. Can allow kids to create all sorts of wild creations if there are multiple join points.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2017 00:53*

Wooo, you could even create modular join points on like his hand so they can interchange weapons :D


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/bqt9BGfhZoa) &mdash; content and formatting may not be reliable*
