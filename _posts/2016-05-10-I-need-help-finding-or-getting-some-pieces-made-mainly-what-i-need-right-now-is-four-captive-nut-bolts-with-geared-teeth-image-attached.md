---
layout: post
title: "I need help finding or getting some pieces made, mainly what i need right now is four captive nut bolts with geared teeth (image attached)"
date: May 10, 2016 19:56
category: "Modification"
author: "ben crawford"
---
I need help finding or getting some pieces made, mainly what i need right now is four captive nut bolts with geared teeth (image attached). They need to be able to grab hold of a threaded rod and using a belt with the rod turn it so it can raise and lower a platform. I am basically making my own Z table. I can get most of the parts but these nuts are hard to find. If anyone has a 3D printer and would like to make me four of these to fit a 10mm threaded rod i will pay for them.

![images/6057d2a1b0a9d48663f9a91d31d3f8d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6057d2a1b0a9d48663f9a91d31d3f8d7.jpeg)



**"ben crawford"**

---
---
**Anthony Bolgar** *May 10, 2016 20:07*

I'd be happy to print them for you Ben. Give me a couple of days, in the mean time send me your shipping info.


---
**ben crawford** *May 10, 2016 20:19*

I just sent an email to the email address listed on your profile, it includes my address and a photo of what i was thinking (not the same photos as above)


---
**Jim Hatch** *May 10, 2016 20:25*

Why not get them from LightObject. You're looking for standard stepper motor drive gear wheel pulley right?



[http://www.lightobject.com/Search.aspx?k=Belt](http://www.lightobject.com/Search.aspx?k=Belt)


---
**ben crawford** *May 10, 2016 20:33*

I did not know they sold just the parts, i might just go with that then.


---
**Jim Hatch** *May 10, 2016 20:38*

Yep. Some good stuff too. Things like drive belts, motors, etc etc. When my tube goes, I'll bump it to a 45W laser tube from them and their little extension box for the side of the blue case (the 45W is 1M long but our 30W tubes are only 70cm so there's not room in the box for the bigger tube). That'll give me 50% more power so I can cut faster or deeper.



The advantage over a 3D print is it will be right first time, no issues matching the gear teeth to the belt teeth, etc. (not you from Anthony but I've got a 3D printer too and I never get it right the first time for a new part design).


---
**ben crawford** *May 10, 2016 20:41*

I am wondering if i should just save and get the whole kit, i am looking at around $100 for my DIY table and it might not work perfectly.


---
**Jim Hatch** *May 10, 2016 20:43*

:-) yeah, I'm looking at their Z table myself. Save a ton of time just getting theirs than making my own.﻿ I'm just waiting until I install my Smoothie board so I can drive it.


---
**ben crawford** *May 11, 2016 17:19*

**+Anthony Bolgar** Can you make me some captive nut gears anyways? i want to make a basic slimmed down version that should get me through until i get the  pro version.


---
**Anthony Bolgar** *May 12, 2016 23:26*

I will make you some, just need a few days to get caught up with my real job.

 They need to be a 10mm bore, how many teeth per pulley do you want? And I am assuming you will be using GT2 belt?


---
**ben crawford** *May 16, 2016 18:01*

yes i will be using a GT2 belt, not sure how many teeth it needs to have.


---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/HA2woPDkiCi) &mdash; content and formatting may not be reliable*
