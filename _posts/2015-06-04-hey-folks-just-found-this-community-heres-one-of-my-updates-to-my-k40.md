---
layout: post
title: "hey folks, just found this community. here's one of my updates to my k40"
date: June 04, 2015 15:20
category: "Modification"
author: "charlie wallace"
---
hey folks, just found this community. here's one of my updates to my k40



[https://charliex2.wordpress.com/2014/06/21/lightobject-z-table-for-the-k40ebay-chinese-lasers/](https://charliex2.wordpress.com/2014/06/21/lightobject-z-table-for-the-k40ebay-chinese-lasers/)





**"charlie wallace"**

---
---
**Eddie Pratt** *June 06, 2015 23:12*

Wow, that's a great project you've got going on there! Well done! I haven't done anything that advanced. I did remove the built-in laser bed of mine and replaced it with an adjustable clamp stand. I can then adjust up and down for varying thickness material. I put a ceramic tile with some aluminium honeycomb on top of the stand and that works great as a laser bed that doesn't reflect, but absorbs heat well.



[http://i01.i.aliimg.com/wsphoto/v0/1811192631_1/4-4-Aluminum-Lab-font-b-Lift-b-font-Lifting-Platforms-Stand-Rack-Scissor-Lab-Jack.jpg](http://i01.i.aliimg.com/wsphoto/v0/1811192631_1/4-4-Aluminum-Lab-font-b-Lift-b-font-Lifting-Platforms-Stand-Rack-Scissor-Lab-Jack.jpg) 


---
*Imported from [Google+](https://plus.google.com/103597034352682332839/posts/YRECQM6jhAE) &mdash; content and formatting may not be reliable*
