---
layout: post
title: "is it normal to have miss drilled screw threads on the x-axis bar?"
date: September 03, 2016 08:24
category: "Discussion"
author: "waco kid"
---
is it normal to have miss drilled screw threads on the x-axis bar?  i just got my machine in and had to reset the x axis belt and when i removed the top bar i noticed when they tapped the threads for the screws they missed the mark and my screw holes are more U shaped then O shaped.  is this normal with the thrown together craftsmanship of these or should i start looking for a new axis bar.  the current set up does seem to be pretty solid, no movement on the bar currently.  







**"waco kid"**

---
---
**Scott Marshall** *September 03, 2016 14:43*

Bad threads, missing screws, wires hanging, loose everything out of adjustment.



 My carriage looked like it was cut from the extrusion by a metal eating beaver.



It's all part of the $300 laser experience...



Once you have it all worked out, (and it will happen) the factory exhaust duct removed, the factory bed replaced with a decent homemade one, and a good air assist adjustable focus head installed, you'll have a quite useful tool for far less than the $3000 to $6000 of a ready made commercial one. It's the "Sweat Equity" principle.



By then, you'll be thinking "geez this factory software is pretty crappy...."



So goes the K40 laser.



Enjoy!


---
**waco kid** *September 04, 2016 07:52*

Lol.  Already have the bed and exhaust replaced.  Looking into the air assist head now :)


---
**Scott Marshall** *September 04, 2016 12:07*

I have and recommend this one. Besides being a nice air assist setup, it allows easy focus adjustment so you don't have to fuss with material height, just get it with in 1/4" or so and the head lets you do the fine adjustment.  It does take some metal work (opening up the plate and shimming the head downward about .030) to get it installed, but it's well worth the afternoon's work.



The best deal is to get the head and the mirror/lens kit. (the head takes a larger dia lens) 



[ebay.com - Details about  CO2 K40 Laser Stamp Engraver Head Mirror 20mm Focus Lens 18mm Integrative Mount](http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050?hash=item3abe742d92:g:kagAAOSwIrNWFIY7)


---
**waco kid** *September 04, 2016 14:13*

thanks!  i'm going to grab one of those later this week.  what about a guide laser so you can see where the laser is going to cut.


---
**Scott Marshall** *September 04, 2016 15:02*

To be honest I think the laser pointer attachments are a personal thing. I have never had a need for one, but there are people who swear by them. I usually cut in 1 go, and if I do make a multi pass (engrave then cut) I just leave the workpiece in place. 



When I do need to "spot" something in, I just turn the power down to a couple of ma and tap the test button. This shows the mark, but it's not a deep or marring mark (like a carpenter uses a light pencil mark) If it really bothers you, you can put a small piece of tape on the work and spot on that.



There's a clamp on pointer that fits that head, and I think they (Saite Cutter) sell it as a package deal. They're not too expensive (10 or 12 bucks I believe), and if you find out you don't use it you can always take it off.



If you plan on doing a lot of plywood cutting , consider ordering a longer (2 1/2") focal length lens, as they improve the cutting speed and quality, especially on birch ply. One advantage of that head is the air nozzle/lens holder slides right out, making a lens swap a 2 minute job.

Above the 2 1/2" (3"+) the focus gets less specific and you start to get a wider softer cut and less engraving detail.  (Stock focal length is 2" or 50.8mm)



Hope that helps some,



Scott


---
*Imported from [Google+](https://plus.google.com/105465271425419072597/posts/X1y5qbEnERu) &mdash; content and formatting may not be reliable*
