---
layout: post
title: "Can anybody put me in the right direction please?"
date: February 25, 2017 21:40
category: "Discussion"
author: "Andy Shilling"
---
Can anybody put me in the right direction please? I have redesigned my front panel for the switches etc in Sketchup, but loading it into LW3 and looking at the path it seems completely wrong. 

I have exported as .DXF file and loaded it in but this is what I get. I have also tried to load it into Inkscape but when I save the file as a .svg even after setting the page size I end up with what seems to be extra canvas around the image.



image 1 is inkscape.svg (red lines are excess)

image 2  is what it looks like it Sketchup

image 3 is .DXF imported to LW3





![images/22216efea6982d19a572685b9df681cc.png](https://gitlab.com/funinthefalls/k40/raw/master/images/22216efea6982d19a572685b9df681cc.png)
![images/6fcc5277edb6846422ea29717a5dc52b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6fcc5277edb6846422ea29717a5dc52b.png)
![images/094ba5c4492a84931d71183152e17a12.png](https://gitlab.com/funinthefalls/k40/raw/master/images/094ba5c4492a84931d71183152e17a12.png)

**"Andy Shilling"**

---
---
**Ariel Yahni (UniKpty)** *February 25, 2017 22:47*

**+Andy Shilling** Post the file please


---
**Andy Shilling** *February 25, 2017 22:51*

 **+Ariel Yahni** I have included the DXF and the original SKP.

Thanks for having a look.



[drive.google.com - Laser - Google Drive](https://drive.google.com/open?id=0B4PVxTUHzQtfUGNGUHZZanliNVU)


---
**greg greene** *February 25, 2017 22:52*

I know inkscape has an option to show page outline that can be turned off, also you can export by page or by drawing and I think also by object


---
**Ariel Yahni (UniKpty)** *February 25, 2017 22:55*

Here you go [send-anywhere.com - Send Anywhere](http://sendanywhe.re/I8JI4IOC) this should work


---
**Andy Shilling** *February 25, 2017 22:55*

**+greg greene** I did come across the export by drawing somewhere but that turned out exactly the same. I have never really worked with Inkscape but this seemed my best option to change from the DXF to a SVG.




---
**Ariel Yahni (UniKpty)** *February 25, 2017 22:56*

Mind that i used this procedure [github.com - LaserWeb3](https://github.com/LaserWeb/LaserWeb3/wiki/Workflow:-Sketchup-2D)


---
**Andy Shilling** *February 25, 2017 22:59*

**+Ariel Yahni** That is spot on my friend Thank you. Now could you tell me where I went wrong and how you fixed it in 2 seconds lol.


---
**Ariel Yahni (UniKpty)** *February 25, 2017 23:04*

**+Andy Shilling** look at my response above with the wiki link


---
**Andy Shilling** *February 25, 2017 23:08*

**+Ariel Yahni** I was one step out, I was saving it as Polyface Mesh  rather than polyLines. Thank you for putting me onto this wiki.


---
**Ariel Yahni (UniKpty)** *February 25, 2017 23:12*

Yeah mesh will not work until we have 3D printer support :)


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/19N979aa3nu) &mdash; content and formatting may not be reliable*
