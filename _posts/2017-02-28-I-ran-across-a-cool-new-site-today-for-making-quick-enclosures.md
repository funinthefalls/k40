---
layout: post
title: "I ran across a cool new site today for making quick enclosures"
date: February 28, 2017 19:27
category: "Software"
author: "Ric Miller"
---
I ran across a cool new site today for making quick enclosures. [http://www.makercase.com](http://www.makercase.com)



I'm going to experiment with something today and I will report back. 





**"Ric Miller"**

---
---
**Ric Miller** *February 28, 2017 23:23*

It works great. Pretty simple to use. Is there a list of cool laser related tools like this somewhere?


---
**Timo Reinhardt** *March 01, 2017 11:00*

This site is awesome, I'm using it always when I need a box. Simply export in SVG format and adjust in your graphic program as required. Mainly I'm using the finger-option and add a hinge afterwards in CD


---
**Madyn3D CNC, LLC** *March 04, 2017 11:48*

This is perfect, thank you for sharing


---
*Imported from [Google+](https://plus.google.com/102361688882180483990/posts/1CVTjnHi1Y3) &mdash; content and formatting may not be reliable*
