---
layout: post
title: "ello to everyone!! it is my first comment here, I got my k40 recently, in a few hours I aligned (what a nigthmare!) using guides publised here, thanks for that, just Im wondering, whats happen if I use the ampere meter upper"
date: August 15, 2018 20:07
category: "Discussion"
author: "Djinn per se"
---
ello to everyone!!  it is my first comment here, I got my k40 recently, in a few hours I aligned (what a nigthmare!) using guides publised here, thanks for that, just I´m wondering, whats happen if I use the ampere meter upper of 15mA? can i burn the power supply? I tried to cut Acrilyc with 15mA but it takes 3pases at 10mm/s



Another thing jus to confirm, this machines does not capable to control automatically the laser power right? just can do that making an upgrade.



Thanks in advance for you help! If any one needs help with SolidWorks, CamWorks,Artcam or Gcode, here have a good friend that can try to help.

Regards!



![images/7b2ecaa2fd03e3083e26f161d28b083f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b2ecaa2fd03e3083e26f161d28b083f.jpeg)
![images/b7d840130edd46f3dbae226d40fba7d0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7d840130edd46f3dbae226d40fba7d0.jpeg)

**"Djinn per se"**

---
---
**Kelly Burns** *August 15, 2018 20:58*

 I use 16ma as my max, but I don’t often even go that high.  You won’t burn your power supply,  it you will reduce life of the Tube and power supply. Often there is no real power increase to the head if you go above 16ma. Not enough to justify the additional wear and tear. In reality K40 are often overdriven 30-35 watt tubes. So you results are about normal.  Go lower power and go slower.   


---
**Joe Alexander** *August 15, 2018 21:07*

using the laser tube above 15ma will shorten its life span substantially. try reducing the speed for cutting down to 5-6mm/s and make sure your material is at the right focus depth (50.8mm from the lens to the middle of your material for cutting). 

Also with an upgraded control board you can control the power output better, using the pot as a "max power" cap.


---
**Justin Mitchell** *August 16, 2018 09:39*

You should be able to cut through 3mm acrylic in a single pass at those kind of speeds and power, if it takes 3 passes then something is wrong.  could be as simple as not having the work at the focal point, other common issues are the focusing lens being in upside down.   Previous guidance was istr that running it above 18mA would drastically shorten the life of the tube, as will running it with inadequate cooling, the lower you run it the closer you will get to the rated lifespan.


---
**Djinn per se** *August 16, 2018 12:20*

Thanks to all!! It was ver y useful and yes my problem was the focal length, now I have a gage for the distancie, thanks for yours advice!


---
**Kelly Burns** *August 16, 2018 13:48*

**+Djinn per se** Glad you got it.  Mine has not cut 3mm acrylic in one pass since the original tube and Power Supply.  I don't cut much and my engravings look much better with my new Tube and PS.    Glad someone realized that it might be a basic Focal issue.  



btw...  Just keep playing with it and having fun.  Eventually aligning, focus and tweaking will be second nature. 


---
*Imported from [Google+](https://plus.google.com/111916164289873438273/posts/XXqQuskwST3) &mdash; content and formatting may not be reliable*
