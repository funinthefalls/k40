---
layout: post
title: "Shared on November 05, 2017 12:37...\n"
date: November 05, 2017 12:37
category: "Object produced with laser"
author: "Art Fenerty (ArtF)"
---


![images/82198bc82e0884198a35a939465a5b70.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/82198bc82e0884198a35a939465a5b70.jpeg)



**"Art Fenerty (ArtF)"**

---
---
**Art Fenerty (ArtF)** *November 05, 2017 16:53*

Top engraving, 100 watt kern laser, bottom image is modified K40 laser.




---
**BEN 3D** *November 07, 2017 20:08*

Really cool, both of them! What Kind of wood is that? And how can I create one?


---
**Printin Addiction** *November 07, 2017 20:21*

What were the modifications to the K40?


---
**Don Kleinschnitz Jr.** *November 07, 2017 23:35*

What gcode generator?


---
**Art Fenerty (ArtF)** *November 08, 2017 01:03*

 I think thats in poplar, but Ive run them in maple etc.. 



Its a K40 modified to be run by a PoKeys57CNC board under 

Auggie , a free cnc controller that will only run a Pokeys575CNC board.

  Auggie generates the Gcode for laser imaging on its own, but its GCode is specific to the program as it incorporates the image file as a datafile attached to the Gcode.  You will find threads on using Auggie at [gearotic.com - "Gearotic Motion Gear design Software"](http://www.gearotic.com) in the forum. Both Auggie and the forum are free, but you have to email to get an account to stop the spam bots.

  There are threads from a few of K40 users showing how it was done.


---
*Imported from [Google+](https://plus.google.com/+ArthurFenerty/posts/aeimckB7CfL) &mdash; content and formatting may not be reliable*
