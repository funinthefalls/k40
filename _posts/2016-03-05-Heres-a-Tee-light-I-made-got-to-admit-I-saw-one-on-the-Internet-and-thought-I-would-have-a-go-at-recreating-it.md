---
layout: post
title: "Here's a Tee light I made, got to admit I saw one on the Internet and thought I would have a go at recreating it"
date: March 05, 2016 22:32
category: "Object produced with laser"
author: "Tony Schelts"
---
Here's a Tee light I made,  got to admit I saw one on the Internet and thought I would have a go at recreating it.  so cant claim It was my design.

I had an accident earlier, I noticed the occasional blank spot when cutting I mean didn't go all the way through. So I decided to clean the mirrors and lens.  When I was transporting the parts back to the machine, I dropped the air assist mounting with the lens in it.  The drop caused 2 cracks all the way through in 2 places, but not at the center.  I decided to see if it would still work, and it cuts better than it did before.  I guess I have it better aligned.  Anyway I made this with my broken lens. 



![images/2618e9373255b5c29f655ed8007d7bcc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2618e9373255b5c29f655ed8007d7bcc.jpeg)
![images/64c4ee9ccb0ab0ea65804bad2e9d8b37.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/64c4ee9ccb0ab0ea65804bad2e9d8b37.jpeg)
![images/21cba734d142ae05fb89c63b1b0afc10.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/21cba734d142ae05fb89c63b1b0afc10.jpeg)
![images/3bdc6c0971edb428f15604d77b55d477.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3bdc6c0971edb428f15604d77b55d477.jpeg)

**"Tony Schelts"**

---
---
**I Laser** *March 05, 2016 23:26*

Looks really good, does the top get hot at all?  No smell of formeldahyde?? lol



Nice MacGuyver too!


---
**ThantiK** *March 05, 2016 23:26*

Could probably do with far less finger joints.  Little sandpaper to clear up the edges, and some lacquer. 


---
**Tony Schelts** *March 05, 2016 23:31*

Absolutely right, No it doesn't get hot and no smells, I do need to pay attention to detail.  ThantiK do you have pictures of things you have made on the Laser?? 


---
**ThantiK** *March 06, 2016 01:29*

**+Tony Schelts**, I don't - unfortunately.  I use an Epilog at my hackerspace, mostly helping others with their own projects.  My projects are less artsy and more mechanical engineering, so they don't have to look pretty. :P


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 06, 2016 05:33*

Looks pretty cool. I find the darkened edges can look good on certain projects, and they do look quite nice on this one. 


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/VQ8EWyvW96e) &mdash; content and formatting may not be reliable*
