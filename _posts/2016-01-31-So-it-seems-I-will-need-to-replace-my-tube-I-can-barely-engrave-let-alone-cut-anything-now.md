---
layout: post
title: "So it seems I will need to replace my tube, I can barely engrave let alone cut anything now"
date: January 31, 2016 05:22
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
So it seems I will need to replace my tube, I can barely engrave let alone cut anything now. Mirrors are clean and aligned, lens is brand new, so I am pretty sure it is the tube. Any body have any ideas where best to buy either a 40w or maybe a 50w (with a case extension). I would like it to ship from North America if possible unless the overseas price + shipping is too good to pass up.





**"Anthony Bolgar"**

---
---
**Joe Spanier** *January 31, 2016 07:01*

I got one from [automationtechnologiesinc.com](http://automationtechnologiesinc.com) and it was much nicer than the tube in my k40. Seemed like it had more power too. 


---
**Anthony Bolgar** *January 31, 2016 07:23*

I tried the lens upside down and downside up...no real change.


---
**I Laser** *January 31, 2016 09:31*

Was it working fine before you changed the lens/head? Have you tried it with the original hardware?



Also did you actually find the focal length? Try the 'ramp test'. a piece of material on a slanted angle, lowest point and the floor, highest point just below the head (so you don't hit anything), burn a single line horizontally across it, starting about midway to the highest point and measure the thinnest point to your head.



Might be a dud tube, apparently if it works for more than a few weeks it usually lasts a while. ;)


---
**Anthony Bolgar** *January 31, 2016 09:45*

I am pretty sure I have the focal length correct, from bottom of nozzle to underside of lens was 28 mm leaving 22.8mm from tip of nozzle to work piece. I'll run a ramp test just to be sure.


---
**I Laser** *January 31, 2016 10:04*

At a guess you're 8mm out. Anyway a  ramp test will tell you what it should be. Hope it saves you the cost of a new tube!


---
**Scott Thorne** *January 31, 2016 11:39*

Laser depot is where I bought mine...275.00.  But it comes with a year warranty...it's I puri which is I good brand.


---
**I Laser** *February 02, 2016 07:18*

Actually just installed one of mine and did a ramp test and you're pretty close, mine came in at 24.5mm, so probably is your tube if everything else is clean/aligned.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/Fu1wA8yhGUH) &mdash; content and formatting may not be reliable*
