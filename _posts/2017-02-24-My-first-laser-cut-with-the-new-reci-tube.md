---
layout: post
title: "My first laser cut with the new reci tube"
date: February 24, 2017 22:10
category: "Object produced with laser"
author: "Paul de Groot"
---
My first laser cut with the new reci tube. Turned out well. I have never ever made anything out of leather so this is my first attempt at a Walter Mitty wallet☺



![images/f680fdfbf54bb1bdde7cea93f129bf34.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f680fdfbf54bb1bdde7cea93f129bf34.jpeg)
![images/46b350c31baa183a9a75d46868f8f26d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/46b350c31baa183a9a75d46868f8f26d.jpeg)
![images/7380f3299e298288dbd9ff020b0fd54b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7380f3299e298288dbd9ff020b0fd54b.jpeg)
![images/c1a8decf03bb23e452185a81a9fefcd2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1a8decf03bb23e452185a81a9fefcd2.jpeg)

**"Paul de Groot"**

---
---
**Ariel Yahni (UniKpty)** *February 24, 2017 22:30*

Very cool , nicely done. What leather did you use? Weight ?


---
**Don Kleinschnitz Jr.** *February 24, 2017 22:42*

Nice glad you are up and running again.


---
**Paul de Groot** *February 25, 2017 00:45*

**+Ariel Yahni** it's 1.5 mm think and scrap leather being sold on ebay. 15 x 10 cm pieces for $15. Probaby cow hide.


---
**Paul de Groot** *February 25, 2017 00:46*

**+Don Kleinschnitz** thanks don, it was really a nerve breaking week without the machine and having to complete so many exhibition things for the maker faire. Any further thoughts on the k40?


---
**Don Kleinschnitz Jr.** *February 25, 2017 01:30*

**+Paul de Groot** you mean the trip to CA?






---
**Paul de Groot** *February 25, 2017 01:51*

**+Don Kleinschnitz** yes the trip to San Mateo ☺


---
**Don Kleinschnitz Jr.** *February 25, 2017 02:05*

As of now we are planning on it :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 25, 2017 03:10*

Good job **+Paul de Groot**. Little tip for cutting stitching holes for standard thread... about 1mm holes are perfect.


---
**Reinier Rossen** *February 26, 2017 12:32*

**+Paul de Groot** just a heads up in case you aren't already aware; cheap leather tends to be refinished splits which means it's a layer of leather with a synthetic finish. This is likely to contain PVC and I'm sure you know what that means :-) considering the shineyness and pebbled texture I wouldnt be surprised if you are indeed dealing with such leather. In case you want to continue working with leather look into vegetable tanned/vegtan leather. In the Netherlands usually called "tuigleer". 


---
**Paul de Groot** *May 21, 2017 03:09*

Just showed it at the San Francisco makerfaire 


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/2kaKUFnWUiJ) &mdash; content and formatting may not be reliable*
