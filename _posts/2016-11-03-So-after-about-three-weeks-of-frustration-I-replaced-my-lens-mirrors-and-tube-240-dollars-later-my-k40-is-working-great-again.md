---
layout: post
title: "So after about three weeks of frustration I replaced my lens mirrors and tube (240 dollars later) my k40 is working great again"
date: November 03, 2016 13:38
category: "Discussion"
author: "3D Laser"
---
So after about three weeks of frustration I replaced my lens mirrors and tube (240 dollars later) my k40 is working great again.  Well almost great I forgot to tighten the nuts on the mirror and now it's misaligned but that is an easy fix.  But I found a 50 on eBay for 800 and I am thinking about pulling the trigger and upgrading if I do how is the secondary market for a upgraded k40?





**"3D Laser"**

---
---
**HalfNormal** *November 03, 2016 15:04*

I purchased a stock K40 that was a few years old with some issues with the moshi board for $150 shipped a few month ago if that helps. It actually works great now after fixing the issues which cost about $20.00


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/BLEifCLtvfK) &mdash; content and formatting may not be reliable*
