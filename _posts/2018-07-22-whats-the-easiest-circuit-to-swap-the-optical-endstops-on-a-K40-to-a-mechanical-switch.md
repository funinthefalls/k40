---
layout: post
title: "what's the easiest circuit to swap the optical endstops on a K40 to a mechanical switch?"
date: July 22, 2018 13:49
category: "Original software and hardware issues"
author: "Frederik Deryckere"
---
what's the easiest circuit to swap the optical endstops on a K40 to a mechanical switch? I've been trying stuff but I'm not an electronics guru. I looked t Don's schematics but can't fully grasp how the optical ones work. It seems even when disconnected there's a 3V signal on the endstop wire. Then enabling the switch should pull it to ground. Is that correct?





**"Frederik Deryckere"**

---
---
**Don Kleinschnitz Jr.** *July 22, 2018 14:44*

Need to know what controller and how you are trying to connect from switch to that controller. 

i.e. are you wiring from the switch directly to controller or using daughter boards.  



<i>It seems even when disconnected there's a 3V signal on the endstop wire.</i>

Where are you measuring this? At the controller?


---
**Frederik Deryckere** *July 22, 2018 16:40*

It's a cohesion 3D. Yeah I measured at the end of the ribbon cable but not connected further to the small pcb, so basically  I measured the Endstop Y pin on the controller.


---
**Don Kleinschnitz Jr.** *July 22, 2018 19:08*

I think the default on that port is pulled up to 3v so that is why an open reads 3v. 


---
**Don Kleinschnitz Jr.** *July 22, 2018 19:10*

Do you want to connect the switches to the daughter card or wire without the ribbon cable? 


---
**Don Kleinschnitz Jr.** *July 22, 2018 19:10*

Btw why switch from the stock optical?


---
**Frederik Deryckere** *July 22, 2018 23:38*

optical died on me. I have one on the way but in the meantime I really wanted to test the Cohesion 3D setup ;-) I figured it out btw. Turns out I confused the NO and NC pin of the switch. All works now. Your website helped a bunch. Again! thanks ;-)




---
**Don Kleinschnitz Jr.** *July 23, 2018 01:18*

Most times the optical have a mechanical failure from impacting the interposer. If you end up replacing it check my blog as the stock led side resistor won't work. 


---
**Frederik Deryckere** *July 23, 2018 12:33*

yes I read that part ;-)


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/C7yKrpEsL2o) &mdash; content and formatting may not be reliable*
