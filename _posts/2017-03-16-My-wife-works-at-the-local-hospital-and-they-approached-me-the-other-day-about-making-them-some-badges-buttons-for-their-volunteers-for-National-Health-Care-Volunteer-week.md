---
layout: post
title: "My wife works at the local hospital and they approached me the other day about making them some badges/buttons for their volunteers for National Health Care Volunteer week"
date: March 16, 2017 15:18
category: "Object produced with laser"
author: "Ned Hill"
---
My wife works at the local hospital and they approached me the other day about making them some badges/buttons for their volunteers for National Health Care Volunteer week.  This is the logo they wanted with a hand cupped under a heart.  They also wanted the heart to be red and the hand to be blue.  I've done painting on pieces posting etching, but for 100 pieces I wasn't too excited about the time it would take to do the precise painting, especially considering what their budget was.  These are 2" (50mm) diameter pieces in 3mm alder wood.



I've also done painting pre etching but for one or two small items at a time.  With reverse etching pre painting is easier because  you don't have to be overly precise on where the paint is placed as the excess gets lasered off.  But you still need to be somewhat precise so you don't have paint overlapping where you don't want it.  So the question became how to accurately pre paint a 4x12" board for doing 10 button's at a time.  



What I decided to do is to created a rough outline of the individual details to be painted and do a quick and very light vector etch of the outlines (60mm/s at 13%).  Then I painted the outlined areas with a small flat brush and didn't worry with staying in the lines too much, so it was quick.  After the paint dried I masked, etched and cut.  Appears to work really well.  The most critical factor is that the outline etch needs to VERY light as the depth of the etch will get transferred during the main engraving etch.  The last picture shows my first attempt where I did the outline etch at 40mm/s with 15% power and the outline etch is still clearly visible.  On subsequent tests I also smoothed out and tightened up the outline etch a bit to make it even less likely to be noticeable.   Hopefully I will be able to maintain good positioning on replacing the board post painting, especially for the pieces toward the other end of the board. Even If the alignment is off just a bit it would just mean having to touch up a few of them which is still better than painting the whole thing post etching.



![images/60278e100db80a893d59d63bcb18aeb1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/60278e100db80a893d59d63bcb18aeb1.png)
![images/7f9361500b52fd5b94636b46715cdf08.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7f9361500b52fd5b94636b46715cdf08.png)
![images/44169226a8b0e3838ea71473e57de51b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/44169226a8b0e3838ea71473e57de51b.png)
![images/1e93193363ebd70b569594cc3b289013.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1e93193363ebd70b569594cc3b289013.png)
![images/46fb5f792a407eef13528668f7c44a12.png](https://gitlab.com/funinthefalls/k40/raw/master/images/46fb5f792a407eef13528668f7c44a12.png)
![images/c0427f18d2e8d3763d3ce34e4f406ca5.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c0427f18d2e8d3763d3ce34e4f406ca5.png)

**"Ned Hill"**

---
---
**Don Kleinschnitz Jr.** *March 16, 2017 15:29*

Brilliant!


---
**Ulf Stahmer** *March 16, 2017 15:36*

Nice job!!


---
**Jim Hatch** *March 16, 2017 15:54*

Really nice solution. They look great.



If you're using the same size material stock (say 12x12) you could always put in a corner jig so you can just drop the sheet in the same place before & after painting to solve any potential alignment issues.


---
**Ned Hill** *March 16, 2017 16:00*

Thanks Jim, Yeah I do have a corner jig so it should be ok like you said.


---
**Nate Caine** *March 16, 2017 17:55*

You might consider using permanent markers; you'd be surprised how fast the "painting" goes with the proper sized nib.    In the sample shown below (a prototype scrap), there was no sanding done to remove the edge soot shadow (see the sun).  You might consider if the edge shadow gives some artistic depth to your image (around the heart for example), or otherwise choose a light sanding to make the image "pop".  



If you engrave the dark background of your artwork it is easy to use markers on the red heart and the blue hand as there is broad space between them.  You can use a very light color to give a faint tint around the boarder.  The letters will still be crisp. 



The permanent markers act somewhat like wood stain and depending on the wood really bring out the grain.  A few days ago I posted a "pi day" medallion.  We did a similar one with a tan marker over the wood and it gave it a leather effect. 





![images/0aaa6f1f77aa6f4c77cef5b962ff6f8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0aaa6f1f77aa6f4c77cef5b962ff6f8c.jpeg)


---
**Ned Hill** *March 16, 2017 18:00*

**+Nate Caine**​ good ideas :-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 17, 2017 00:27*

**+Ned Hill** Great idea & I think I've got a slight modification to make it so you don't have to worry about the etch power being too high. You could have another piece of ply as a stencil, cutting out a "rough" shape like you did for each colour section. Then you could place it above your work material & paint in the "gaps".


---
**Ned Hill** *March 17, 2017 01:15*

**+Yuusuf Sallahuddin**​ That was actually another thought I had if I couldn't get the outline etch to work.  I think having the laser mark the piece directly probably gives the best accuracy but what you suggest is certainly a viable option.  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 17, 2017 01:21*

**+Ned Hill** Yeah, definitely a good method. I had another thought regarding your etch, could you do it right on the actual borders of the objects (heart/hands) so that even if power was too high you would never notice it once the final engrave has been done.


---
**Ned Hill** *March 17, 2017 02:41*

**+Yuusuf Sallahuddin** Yeah that's a good idea.  You would then probably want to be sure to over paint slightly to give yourself a bit of leeway in any slight error in repositioning the piece after painting.


---
**Mishko Mishko** *March 18, 2017 17:54*

If spray paint could be used, I'd simply cut paper stencils for individual colors, leaving enough bleeding margin, tape and spray, then proceed with etching and cutting. If hand-painting, this might work, too, I guess, if not with paper, probably with some kind of thin film, like Mylar, Lexan or similar...


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/DRFmpewPqtp) &mdash; content and formatting may not be reliable*
