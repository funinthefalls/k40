---
layout: post
title: "Just installed this tube and have not fired it yet"
date: July 10, 2016 16:37
category: "Discussion"
author: "george ellison"
---
Just installed this tube and have not fired it yet. I have filled the bucket with fresh water and let it run for a while to clear all the bubbles. Just seeing if everybody thinks it is now ok to run. In the photo you can see very small bubbles and a sort of small reservoir of water to the right of the metal bit in the tube, is this normal? Thanks :)

![images/0835d7b7cca714b76f8af11c1fe4f1aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0835d7b7cca714b76f8af11c1fe4f1aa.jpeg)



**"george ellison"**

---
---
**george ellison** *July 10, 2016 19:11*

Oh I see so the air goes up along that short section of plastic tube then out the exit? Should I put it back into this position after if leave it with the exit facing up?


---
**george ellison** *July 10, 2016 19:12*

That's a good idea, thank you


---
**Timo Birnschein** *July 10, 2016 20:12*

Did this actually happen to anyone? This is a little tiny bubble reservoir and there is no heat source directly connected to that enclosed air. It should be almost impossible to heat up that area to a point where the tension gets to big for the tube to handle... But maybe I'm wrong...?


---
**Scott Marshall** *July 10, 2016 21:07*

**+Timo Birnschein** You are not. 



Given a choice though, I'd prefer to give my laser every possible advantage for a long fruitful life. Bubbles be gone.



Better safe than sorry...



Scott


---
**HP Persson** *July 10, 2016 22:11*

Grab som water wetter next time near a gas station or store where they have auto parts. It lowers the surface tension of the water and reduces the possibility to air pockets or bubbles to form to almost zero.


---
**Scott Marshall** *July 10, 2016 22:50*

**+HP Persson** Great idea!

I used to use that in my race car, (many moons ago) it helps keep bubbles from forming around the exhaust ports.



Using it in the laser is a super idea!



Jegs and Summit carry it (and similar products) if you can't find it locally.


---
**ThantiK** *July 11, 2016 00:57*

If I understand correctly, that metal tube itself is a heat source.  See the bubble touching it?  Not a good idea.



If you look closely, you'll see that the high voltage input is connected to a very thin wire inside there.  This excites the CO2 gas, and that metal tube in there does/can get hot.


---
**Pippins McGee** *July 11, 2016 02:31*

never thought to rotate the tube so the exit faces up haha.

I have very dangerously been lifting and tilting the entire machine to allow air bubble to get out of that spot and leave via the exit - very annoying to have to do.


---
**ThantiK** *July 11, 2016 02:59*

**+Pippins McGee**, I've certainly thought of it - but my machine came perfectly aligned (thank the gods!) - and I really didn't want to turn it and mess up the alignment. :(


---
**Pippins McGee** *July 11, 2016 05:41*

**+ThantiK** my friend... the day I get my machine aligned, i will not even want to breath on the machine..



so i understand where you're coming from hahaha. I fear I will never have an aligned machine.


---
**Scott Marshall** *July 11, 2016 11:04*

Mr McGee,, After two or 3 times through it,  you'll be "tuning it up" in 5 minutes.



If you find some free time on a Saturday afternoon, or can't sleep some evening, run through it a couple of times. It becomes automatic so fast you'll be amazed.

Make a little box or "organizer" for the supplies, and print off your own instructions.By the time you get done writing them, you won't need them anymore.



 Mark the screws in order you adjust, an maybe which way to turn them like (Left/right -CCW=Rt) with a sharpie. It all triggers your memory. I have a terrible memory that only gets worse as time goes along and find little notes and kits of tools jog the frayed nerve cells.



This also applies to  adjusting the focus and Calibrating Home


---
**george ellison** *July 11, 2016 13:12*

Thanks for your advice, the big bubble has now gone through the exit tube, leaving a 3mm bubble which doesn't seem to want to move what ever I do?!?!


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/DczVBjJEL7p) &mdash; content and formatting may not be reliable*
