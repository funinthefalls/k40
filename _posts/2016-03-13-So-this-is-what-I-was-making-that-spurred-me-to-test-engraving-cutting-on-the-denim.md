---
layout: post
title: "So, this is what I was making that spurred me to test engraving/cutting on the denim"
date: March 13, 2016 09:50
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So, this is what I was making that spurred me to test engraving/cutting on the denim.



It's basically a test for a fabric plant pot. I thought I would see if I can engrave the name of the plant (in this case I chose Rosemary, since I love it on lamb... mmm yummy).



So I put the word ROSEMARY on the right of one side of the cube-planter. On the left of that, I put a basic drawing of a sprig of rosemary.



So, overall, after engraving & cutting with the laser, it works. The engraving however is a little bit too subtle with such small/thin lines in my imagery.



If you intend to engrave on denim, I suggest making whatever it is really thick lines/blocky shapes, to increase the contrast against the background denim.



![images/4424681a5497de06111b9c65bdb1146a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4424681a5497de06111b9c65bdb1146a.jpeg)
![images/44ab4d6bff55a019b8946b20299c8caf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44ab4d6bff55a019b8946b20299c8caf.jpeg)
![images/bc7183b5be0cbcf575f4247a7a901311.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bc7183b5be0cbcf575f4247a7a901311.jpeg)
![images/624c97718ca8b216204696616b438bac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/624c97718ca8b216204696616b438bac.jpeg)
![images/9585d79e97879c05c2d7e93128713f51.png](https://gitlab.com/funinthefalls/k40/raw/master/images/9585d79e97879c05c2d7e93128713f51.png)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Scott Marshall** *March 13, 2016 22:52*

Maybe follow up with a quick dip in a weak dye solution - with hopes it grabs onto the freshly cut fiber ends, and not so much on the virgin denim.



I'll bet you can find a way to bring out the engraving with some experimentation.



Clever idea too, the Fabric pots, Bet they sell well if you get the bugs out. Garage sale/thrift shop jeans provide tons of raw material.



I used to make shooting rest beanbags  by stitching up the legs of old jeans. got 4 per pair. Nearly everybody that saw them made some, next thing you know our clubs firing line was covered with them.



Let me know when yo go into production, I know several people that would love em.



Scott﻿



Edit - PS

The wheels were turning on your fabric pots,then a thought materialized amongst the rusty gearbox, and it occurred to me that you could glue the denim onto cardboard, other fabric, or even glue it to a flower pot after lasering your design right thru. they have fabric glue in the craft shops, and there's an iron on tape used for sewing that joins layers too.



With the 2 layer fabric, you create a "fabric laminate" and could engrave it just like plywood or any other layer ed material.












---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 14, 2016 01:22*

**+Scott Marshall** I am actually in the process of ordering eco material for the fabric planter pots, so I will let you know once that has arrived & I have finalised the production designs.



I didn't notice your edit until now, however that is a great idea. Especially since I will be lining the inside with eco-fabric (black in colour) & then I could line the exterior with the engraved denim, which would show the black through. Thanks for the idea.


---
**Scott Marshall** *March 14, 2016 01:56*

If the pots are those compressed manure ones, better order an oversized exhaust fan too....


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 14, 2016 06:54*

**+Scott Marshall** Haha, that would be wise if it was compressed manure.



The eco-fabric I'm getting is one that is non-woven polypropylene. Supposedly bio-degradable, UV stabilised (for harsh Australia UV conditions), weed blocking, hydrophilic treated (allows water to drain through), aerobic, registered Allowed Input with BFA (Biological Farmers of Australia), won't leech toxins, etc. Supposedly has a lifespan of 3-4 years (exposed to sun) or 6-8 years (if fully covered). According to the product specs at least.



So, if the outer was lined with Engraved Denim (or other fabric) then theoretically the interior lining could have an extended lifespan. My concern with lining the exterior, however, is that it may affect the ability to drain/breathe. A few tests are in order I believe.


---
**Scott Marshall** *March 14, 2016 20:44*

A couple of years ago the compressed manure thing was all the rage here in the US. They were selling garden animal ornaments and that sort of casting for upwards of 30 bucks. And couldn't make them fast enough. They're considered "green" as they melt into fertilizer, and contain no "chemicals" They used the same process as making the starter pots, but changed the mold. The fad is over, I couldn't even find one to send you a picture. 

[http://www.ebay.com/itm/Square-4-inch-Cow-Pots-Biodegradable-Natural-Manure-Peat-Pot-Pack-of-25-/381422330434?hash=item58ce8ae242:g:bPsAAOxyVaBS6Aze](http://www.ebay.com/itm/Square-4-inch-Cow-Pots-Biodegradable-Natural-Manure-Peat-Pot-Pack-of-25-/381422330434?hash=item58ce8ae242:g:bPsAAOxyVaBS6Aze)



It's amazing what you can sell. I keep trying to find the "next big thing", but it seems I am a poor judge of consumers. 



I hope that's not bad news as I like your planters and think it should sell well.



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 15, 2016 00:48*

**+Scott Marshall** I'm similar, always trying to find something decent that will take off. You're right that it is amazing what people can sell. Some of the stuff I see I wonder who on earth buys it.



The planters is only part of my idea for the "next big thing". There's more that I'm working on, but it's a secret until I finalise the design/supplies of materials.



I've worked out with the fabric bags, square base is the only ones that work. I tried rectangular base but it has too much pressure on the long walls & wants to just fall over/spill out. Take a look on my profile if you're interested in seeing the failure. But, square bags with walls at least half the height of the square base width/length all work. You can increase the height to whatever you want & the pressure on each side wall is even, so it holds itself up nicely. So for the people that you know that would be interested, I will be able to make any size (within reason).



I think those manure peat pots would be useful for starting seedlings, as you can then just pot into normal soil & it will give it a slight extra boost of growth from the fertilising. Never really seen them around here in Oz, but they probably were available at some point when I wasn't interested in gardening.


---
**Scott Marshall** *March 15, 2016 03:55*

Those pots are much more common in plain pressed paper pulp, used here for egg cartons and fast food drink trays. The manure ones are pretty pricey compared to the paper fiber ones.



Maybe if you used a 3/4" pine base and drilled it for 3/16 vertical dowels to support the fabric.Use a needle and fishing line to attach the denim.

Don't know if any of my ideas are any help, I can't help myself, I love scheming up this stuff.



I've got my 'secret projects" list too, but as time goes on, I'm less secretive, I'll never live long enough to try it all, maybe somebody else can take a glimmer of inspiration from my half done ideas...



I was thinking the shipping from there to here is probably pretty high, but I've never sent anything that way. I do get some flea killer (the one made by Bayer) for my cats that comes from "Oz",  Always wondered how they sell it cheaper than US suppliers.





Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 15, 2016 04:19*

**+Scott Marshall** I think you may be onto something with the dowel idea. Could actually just stitch it into the fabric (use 2 layers) like when you put elastic in or drawstring, but close off both ends so it is permanently stuck in there for support. Alternatively, the iron-on interfacing you mentioned earlier comes in a variety of thickness/strength, so could possibly reinforce with something like that to add extra strength. Thanks for sharing your ideas, it always gets the cogs turning in my head at least.



Shipping is not too bad from Oz in general. Although you have to try to make the most of it since they charge based on ridiculous weights (e.g. 0-1kg, 1-1.5kg, etc). So you need to make sure you that you use as much as possible of the weight limit, to maximise value for money (or minimise shipping per piece). I imagine that fabric planter bags will be somewhere in the 200-250g range, but would have to verify after I receive my eco-fabric & make some. Also, you may be able to post as "document" due to the lightness & thus save quite a lot on the shipping cost.



EDIT: just checked postage prices for "Parcel" from AU to USA. It is ridiculous. For 0.5kg Parcel, will cost somewhere around AU$14 (no tracking) or AU$21 (tracking). For 1kg it goes up to around AU$30 to AU$36. That's using Australia Post as carrier. It doesn't allow for much possibility of international sales. Only reasonably priced option is Sea Mail (2-3 months ETA), for AU$10.50 for 1kg. Would have to check into other more economical carriers.



I have started sharing some of my secret ideas also, for pretty much the same reason. I'm not planning on doing it any more or have other things that I want to concentrate on. Someone else may benefit from the idea/knowledge.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/8AHdPNZdWsr) &mdash; content and formatting may not be reliable*
