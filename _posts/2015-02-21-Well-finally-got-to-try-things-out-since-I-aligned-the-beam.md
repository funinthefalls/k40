---
layout: post
title: "Well finally got to try things out since I aligned the beam"
date: February 21, 2015 19:55
category: "Materials and settings"
author: "Tim Fawcett"
---
Well finally got to try things out since I aligned the beam. Cutting performance is significantly improved. I am able to cut 3mm MDF in one pass at 10mA and 10mm/S and it goes through 3mm Acrylic like a hot knife through butter at 10mA & 7mm/s. I accidentally did a pass on 3mm MDF at 10mA & 20mm/s - it was mostly cut through, just a few little hangers-on.



So here is the front panel and case for the controller I am building for the interlocks on the K40 - the unit will manage dual door interlock switches, an Emergency Stop button - shutting down the laser if either open - but also monitoring the changes to detect failures. It is also going to measure inflow and outflow temp on the laser and the water tank temp, shutting down the laser if the coolant gets too hot or stops flowing.



The control panel is 3mm white acrylic which has been etched to produce the lettering (16mm/s at 10mA) which has given a nice deep engrave. They were then back-filled with airbrush paint and the excess polished off the glossy front when dried.



Now the question is to persist with the MDF for the case or to move to clear 3mm Acrylic



![images/8e1e2191554f3f564b9d3a040d0c66e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e1e2191554f3f564b9d3a040d0c66e6.jpeg)
![images/6d37ba1251f36bc6f92fba6fa95797a1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d37ba1251f36bc6f92fba6fa95797a1.jpeg)
![images/062a9c179f1347a445665df0e91527b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/062a9c179f1347a445665df0e91527b1.jpeg)
![images/f6b6572600f3b091a69fd6ea84fb9f3e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6b6572600f3b091a69fd6ea84fb9f3e.jpeg)
![images/6d8ff9c4239a633796a62a452995845d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d8ff9c4239a633796a62a452995845d.jpeg)

**"Tim Fawcett"**

---
---
**Sean Cherven** *February 21, 2015 20:15*

Very Nice! Do you plan on making this project open source? If so, I would love to build one!


---
**Tim Fawcett** *February 21, 2015 21:32*

Hi Sean I will be doing - might even run a kickstarter to get proper PCBs for the controller made if there is enough interest. I am also putting together a set of boards that give plug and socket connectors so I can swap between the Moshi controller and the RAMPS controller.



In case anyone is wondering the knob is there to allow the temperature sensors to be calibrated and to allow the interlocks to be temporarily overridden to allow service and maintenance.


---
**Eric De Larwelle** *February 21, 2015 21:35*

I'm looking for a speeds and feeds chart for the K40, have you seen one?


---
**Tim Fawcett** *February 21, 2015 21:48*

No I haven't - but this is an ideal forum for putting one together - it will also be a good diagnostic for those having problems -if they cannot meet the speeds or currents for the cut then it shows their setup has problems.


---
*Imported from [Google+](https://plus.google.com/113171525751920354895/posts/8KJrMHNyFEh) &mdash; content and formatting may not be reliable*
