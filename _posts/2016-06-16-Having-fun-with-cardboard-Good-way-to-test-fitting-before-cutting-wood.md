---
layout: post
title: "Having fun with cardboard. Good way to test fitting before cutting wood"
date: June 16, 2016 22:14
category: "Software"
author: "Ben Marshall"
---
Having fun with cardboard. Good way to test fitting before cutting wood.

![images/50182dd6ee77acd9c5e94c6115202751.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/50182dd6ee77acd9c5e94c6115202751.jpeg)



**"Ben Marshall"**

---
---
**Jim Hatch** *June 16, 2016 23:17*

yep. Many Amazon boxes will now die for your laser :-)


---
**Bruce Garoutte** *June 17, 2016 20:56*

Saweet!


---
**Evan Fosmark** *June 21, 2016 18:18*

How do you make the interconnecting pattern? By hand?


---
**Ben Marshall** *June 21, 2016 18:19*

**+Evan Fosmark** No its all done on the laser. Called box joints


---
**Evan Fosmark** *June 21, 2016 18:36*

Sorry, I meant did you make the box joint pattern yourself before sending it to the laser, or did you use something to auto-generate the box joint pattern?


---
**Ben Marshall** *July 04, 2016 01:12*

Sorry for the late reply. I made the box joint myself on Corel draw. I haven't seen much for auto-generating patterns like this yet with corel. There are ways to get the program to do a bit of the legwork, but no one click solution. Lots and lots of testing to get it right on the program, which is why I always use cardboard first.


---
**Jim Hatch** *July 04, 2016 05:20*

There are a bunch of box making programs on the web - you put in the height, width and length (and for some the kerf size) and it generates a box pattern usually in PDF. You can drop that in Corel and then use the Trace function to turn it into paths. Then you can modify it (for example if you don't want a top you'd smooth out all the fingers on the top edges of the sides and delete the top). Just Google "laser cut box maker".


---
**Bradley Blodgett** *July 26, 2016 11:40*

Just curious **+Ben Marshall**. What kind of settings are you usings power-wise?  I have been fiddling trying to get mine to print from Corel and I always seem to burn rather than cut especially on thinner stuff. There seems to be a fine line right about the 10ma point.


---
**Ben Marshall** *August 05, 2016 18:55*

5mA for cutting, 9.7 mm/s. I always start with the lowest power setting and go up from there. If you start burning material speed up your head movement


---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/UcrBFGYxhgZ) &mdash; content and formatting may not be reliable*
