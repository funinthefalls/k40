---
layout: post
title: "Tried to cut 3mm acrylic for my first cut and I am not having any luck sorted mirrors and cleaned them still no luck tried speeds between 8 and the 25 even went up to 80 percent power and still no luck"
date: October 30, 2016 14:27
category: "Discussion"
author: "Rob Turner"
---
Tried to cut 3mm acrylic for my first cut and I am not having any luck sorted mirrors and cleaned them still no luck tried speeds between 8 and the 25 even went up to 80 percent power and still no luck 





**"Rob Turner"**

---
---
**Scott Marshall** *October 30, 2016 15:01*

The focus (distance from the head to the work) is critical. If you're off only a few mm, you have very little cutting power. 



If your mirrors are indeed aligned well, and optics are clean, the focus is where I would go,



Move a piece of paper up and down, small amounts, firing a low power pulse at each height. The spot that is smallest is where you want the center of the acrylic (thickness) positioned. It's about 50.2mm from the center of the lens. You can get close by cutting a toothpick to 50mm and touching it to the center of the lens surface. That's not very precise, but it saves a lot of time homing in on the 'zone'



If you send me your email, I'll send you an article I just wrote (still in draft) on lenses and focusing.



ALLTEK594@aol.com



Scott


---
**Alex Krause** *October 30, 2016 16:49*

Can you post a picture of the cut... if there is any scorching or discoloration of it then you got polycarbonate (Lexan) instead of acrylic and that stuff doesn't cut for crap


---
**Cesar Tolentino** *November 01, 2016 21:15*

And also make sure that the last lens at the bottom of the head us correctly positioned. Curve dude face up. Flat side face down


---
**Rob Turner** *November 02, 2016 00:33*

Finally sorted it out I forgot to clean one my mirrors when I was checking the alignments cutting nicely software annoying at times epecelly when it repeats the objects I did before hand and want it to do a new object 


---
*Imported from [Google+](https://plus.google.com/114544098138525257938/posts/7s74ow2f78s) &mdash; content and formatting may not be reliable*
