---
layout: post
title: "Hi folks, I'm using my laser nearly since a year now and have been successful cutting different materials"
date: February 28, 2017 23:11
category: "Discussion"
author: "Timo Reinhardt"
---
Hi folks,

I'm using my laser nearly since a year now and have been successful cutting different materials.

However, I'm not able to do engraving based on something different than a vector graph. What happens is simply, that the laser moves in x-axis only, no recognizable y-movements can be seen.

Could someone share some snapshots of the used settings for successful bitmap engraving? Or does someone faces similar issues?



Thanks,

Timo





**"Timo Reinhardt"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 28, 2017 23:40*

What software are you running? CorelDraw/CorelLaser or LaserDRW or something else?


---
**greg greene** *March 01, 2017 01:04*

try saving the file as PNG?


---
**Timo Reinhardt** *March 01, 2017 10:57*

I'm running the "original" Corel Draw packet - as mentioned it works fine for vector  graphics.

Would be great if someone could share some screenshots (step-by-step) off the laser settings


---
*Imported from [Google+](https://plus.google.com/117627514782022095999/posts/LY54hmvFgW6) &mdash; content and formatting may not be reliable*
