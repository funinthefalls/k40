---
layout: post
title: "Originally shared by Frank Herrmann New Device in my Workshop!"
date: April 25, 2017 09:16
category: "Smoothieboard Modification"
author: "Frank Herrmann"
---
<b>Originally shared by Frank Herrmann</b>



New Device in my Workshop! A K40UIII Laser machine. Please check Photos and Video. Now i need a table for this. please ignore the pseudo table :) One Question, please send me a link to change the Controller Board to GRBL or Smoothiboard. My special interest is the wiring from old to new and i don't found good schematics, if you know please add a comment with link. many Thanks!



![images/21b5e734a107119be74fe482d8b453a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/21b5e734a107119be74fe482d8b453a9.jpeg)
![images/f4e54201f8badd6a4fdb58025f6a726a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4e54201f8badd6a4fdb58025f6a726a.jpeg)
![images/b68bf0d635818eeaf32fe5cbd878f60f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b68bf0d635818eeaf32fe5cbd878f60f.jpeg)

**"Frank Herrmann"**

---
---
**Jérémie Tarot** *April 25, 2017 10:11*

Haha, already here 😉


---
**Anthony Bolgar** *April 25, 2017 11:23*

**+Ray Kholodovsky** sells a drop in replacement Smoothieware capable board. Takes about 20 minutes to install and get up and running. This is the easiest way to upgrade the K40. Welcome to the K40 community!


---
**Claudio Prezzi** *April 25, 2017 11:41*

For wiring information, check **+Don Kleinschnitz**'s blog @ [donsthings.blogspot.ch - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.ch/2016/11/the-k40-total-conversion.html)


---
**Anthony Bolgar** *April 25, 2017 11:43*

**+Don Kleinschnitz** has really documented the K40, and is an excellent source of information. Thanks for mentioning him **+Claudio Prezzi**


---
**Steve Anken** *April 25, 2017 13:33*

Agreed, get Ray's Cohesion, it was trivial to convert compared to the first Smoothie conversion I did, and Ray is a techie who is excellent with people too. 


---
**Steve Anken** *April 25, 2017 14:00*

BTW, where is your air exhaust? You do not want to breath that stuff.




---
**Stephane Buisson** *April 25, 2017 15:52*

welcome into our k40 community **+Frank Herrmann** 



C3D is the easy way (see links in about community): [https://plus.google.com/u/0/communities/116261877707124667493](https://plus.google.com/u/0/communities/116261877707124667493)



grbl on smoothie: [http://smoothieware.org/grbl-mode](http://smoothieware.org/grbl-mode)










---
**Nick Williams** *April 25, 2017 16:13*

Hi Frank 

Is this the same Frank who supported LaserInk??



Below is K40 running the LaserInk software and firmware.

![images/4aaebf3a4f6a2794538afcb79b682927.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4aaebf3a4f6a2794538afcb79b682927.jpeg)


---
**Frank Herrmann** *April 25, 2017 16:50*

**+Anthony Bolgar** Yes i ordered a C3D mini with GLCD Adapter, thank's for ur advice.




---
**Frank Herrmann** *April 25, 2017 16:51*

**+Nick Williams** Nope, this is the Frank from germany Founder of XATC :) But Laserink looks intresting.

[plus.google.com - Hello Friends, long time it was silent on this channel. Why? The answer, it w...](https://plus.google.com/+FrankHerrmann1967/posts/YCGMLexp1TK)


---
**Frank Herrmann** *April 25, 2017 16:55*

**+Steve Anken** For the first tests i used my open window. But next i'll get a thin mdf and some magnets will hold this on my Basement window :)


---
**Nick Williams** *April 25, 2017 17:55*

Here is a cal test done with an unreleased modified version of grbl. 

This head is moving at  300 mm/sec or 18000 mm /min.



I would have already released the firmware but currently it uses a special format for the high speed rastering functionality additionally archs have been removed to allow for code space.



Because of the special firmware the only way to get these kinds of speed is to use the laser ink software. It is my plan to share the software with community but work still needs to be done. 

![images/991918fbcf147b2945b3b1ab50b898d8](https://gitlab.com/funinthefalls/k40/raw/master/images/991918fbcf147b2945b3b1ab50b898d8)


---
**Nick Williams** *April 25, 2017 17:59*

Additionally Paul de Groot has created a really cool drop in replacement board that will run grbl and my  my firmware.

![images/474f52aae097a963f9ac3fb7133767f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/474f52aae097a963f9ac3fb7133767f5.jpeg)


---
**Phillip Meyer** *April 25, 2017 21:47*

I have to recommend the Cohesione3D board


---
**Frank Wiebenga** *April 26, 2017 00:25*

**+Nick Williams**​ nope it is a different one. I've still got the original laser ink, and it is still kicking. I use v carve for the engraving with a different post. This diode needs to die before I'll upgrade.

![images/a5c1cca3348d280c9a281c3e556d6cf5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a5c1cca3348d280c9a281c3e556d6cf5.jpeg)


---
**Nick Williams** *April 26, 2017 01:00*

**+Frank Wiebenga** 

Great to here from you Frank. Glad that this is still working so well. 



Hopefully I will be making a few announcements to kickstart supporters soon. The big thing I have been working on is the ability to open multiple documents to create a single job. So with the new software you will be able to open and position  save the setting to a project file, for multiple images and vector files. A new document type of text will also be added.



Additionally working to bring the software to the  CO2 community. 


---
**Frank Wiebenga** *April 26, 2017 01:27*

**+Nick Williams** If you do please work to get a post processor setup with Autodesk Fusion 360. They now support laser CAM.


---
**Frank Herrmann** *April 26, 2017 07:48*

**+Nick Williams** & **+Frank Wiebenga** Some words to my plan, yes i got the smoothiclone C3D mini, but i want to use the new grbl-lpc. Thats a GRBL Clone for the Smoothieboard LPC. I used this for my CNC3040 and i'm very satisfied with this firmware. 


{% include youtubePlayer.html id="jv0lxEan_Xw" %}
[https://www.youtube.com/watch?v=jv0lxEan_Xw](https://www.youtube.com/watch?v=jv0lxEan_Xw)

[github.com - gnea/grbl-LPC](https://github.com/gnea/grbl-LPC) 


---
**Frank Herrmann** *April 26, 2017 07:48*

At the end i'll use the laser with fusion360.




---
**Claudio Prezzi** *April 26, 2017 08:11*

**+Nick Williams** I could be mistaken, but your video doesn't look like 300mm/s, not even close to. This one was 280mm/s with grbl-LPC:

![images/8a5026a828e3d8e3ec6aba2bfce17b2c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a5026a828e3d8e3ec6aba2bfce17b2c.jpeg)


---
*Imported from [Google+](https://plus.google.com/+FrankHerrmann1967/posts/jjZM4FGdQq2) &mdash; content and formatting may not be reliable*
