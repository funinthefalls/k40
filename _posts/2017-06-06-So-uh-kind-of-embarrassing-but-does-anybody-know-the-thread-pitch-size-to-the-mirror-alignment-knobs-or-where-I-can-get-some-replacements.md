---
layout: post
title: "So uh... kind of embarrassing but does anybody know the thread pitch/size to the mirror alignment knobs or where I can get some replacements ?"
date: June 06, 2017 05:53
category: "Discussion"
author: "Tony Marrocco"
---
So uh... kind of embarrassing  but does anybody know the thread pitch/size to the mirror alignment knobs or where I can get some replacements ? Same with the screws that screw into the top of the stepper motor for the Y axis. M2.5? 



Thanks ahead of time !





**"Tony Marrocco"**

---
---
**Joe Alexander** *June 06, 2017 06:17*

I would guess either m2.5 or m3, seems to be the common size of most screws on the machine.


---
**Tony Marrocco** *June 06, 2017 06:26*

Alright, I'll buy an assortment to make sure. Just wanted to check. Thanks ! 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 06, 2017 12:51*

I'm assuming you snapped off the head of the mirror alignment screw? I know I did on one of mine. Not sure the thread size as I just found a random bolt that matched in my shed.


---
**Tony Marrocco** *June 06, 2017 19:31*

Yeah I did! Well at least I'm not the only one. 


---
*Imported from [Google+](https://plus.google.com/112686049554441919913/posts/4J7cUy9QUY1) &mdash; content and formatting may not be reliable*
