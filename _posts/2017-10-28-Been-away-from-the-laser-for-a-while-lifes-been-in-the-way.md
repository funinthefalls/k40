---
layout: post
title: "Been away from the laser for a while (life's been in the way...)"
date: October 28, 2017 15:44
category: "Discussion"
author: "Mike Meyer"
---
Been away from the laser for a while (life's been in the way...). Anyhow, I've always been a little concerned with the aquarium pump that came with the setup and I've been thinking about adding a simple inline flow meter to verify that things are working as advertised. Found this PC flow meter on Amazon, but my question to all you engineering types out there is this: do you think this will constrict flow into the water jacket? Any thoughts are appreciated

![images/b8bad454228868f549b00f620e69d638.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8bad454228868f549b00f620e69d638.jpeg)



**"Mike Meyer"**

---
---
**Don Kleinschnitz Jr.** *October 28, 2017 16:00*

I doubt it will cause enough restriction to be a problem. I use a flow switch to protect my system. With a switch you can insure that the LPS shuts down if the water-flow stops and you are not looking at the flow indicator.


---
**Andy Shilling** *October 28, 2017 16:01*

Do you have the link as I manually check my flow every time I cut and this would be a great visual aid.


---
**Mike Meyer** *October 28, 2017 18:27*

Yeah; here's one. They sell them cheaper on Amazon.

[zoro.com - Buy Flowmeters - Free Shipping over $50 &#x7c; Zoro.com](https://www.zoro.com/sp-scienceware-flow-indicator-pin-wheel-19937-0002/i/G8454695/feature-product?gclid=EAIaIQobChMIgZDUxtCT1wIVwh6GCh1SkA4eEAQYAiABEgKdGvD_BwE)




---
**Andy Shilling** *October 28, 2017 22:21*

**+Mike Meyer**​ thank you very much.


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/ZiexLhzheGs) &mdash; content and formatting may not be reliable*
