---
layout: post
title: "My K40 has been smoothified! All working ok"
date: July 23, 2016 11:47
category: "Smoothieboard Modification"
author: "Mircea Russu"
---
My K40 has been smoothified! All working ok. Stepper current set at 0.5A. Motors tend to whine while moving and after stopping, while with original board I heard no motor noise. Any ideas?



![images/70b23e9593d4baa10f1d06804c690991.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/70b23e9593d4baa10f1d06804c690991.jpeg)



**"Mircea Russu"**

---
---
**Scott Marshall** *July 23, 2016 17:41*

A little motor noise is common with micro stepping drives. It's so prevalent some people have written Gcode "music" to play on the CNC tool of your choice. Search Utube for a variety of musical renditions.



1 amp is a little light for stock K40 motors, but may work OK as long as you aren't moving fast and changing direction hard. If so you may find you will miss steps or drive the Allegro chips into false shutdown.



1 Amp is the "popular" settimg for the stock K40 motors for Smoothie Alpha and Beta currents, and it's doubtful setting it a little high will hurt anything. In the event something jams hard and you aren't present to hit the emergency stop, you want the current set low enough that it will shut down instead of damaging something.



I personally would rather have it set a bit higher than required as missed steps and false shutdowns could be worse than a short hesitation before shutdown in a real jam. In the 1st case you lose your workpiece for no real reason, but in a real jam, you've already lost your workpiece and as long as it shuts down before you ruin hardware, it's a good outcome. (as good as a jammed laser cutter can be that is).



I kinds like the 'musical' sound of my machines as they go about their business.



Scott


---
**Mircea Russu** *July 23, 2016 19:18*

**+Scott Marshall** thank you for your answer! Let them sing! Somebody mentioned the LPS 24V output is 1A so I thought 0.5A each stepper, Stephane In his bluebox guide has them set at 0.6A X and 0.5A Y axis. I shall eventually replece the steppers and go separate power supply. 


---
**Scott Marshall** *July 23, 2016 19:25*

The stock Steppers are actually some pretty decent little motors. I wouldn't change them unless they give you problems.



 I'm 100% behind going with a second power supply and take all the load you can off the main supply. It will run cooler, put out more power  for the laser tube and probably last longer. Your Smoothie will like the nice clean noise free power too.



Viva la musica!


---
**Jon Bruno** *July 24, 2016 03:05*

Yeah, I mentioned it because I used the same copy of Ariel's config file and he had Alpha and Beta set to 1.5A..(he uses external drivers so this setting had no effect) but for me... Well, I need to change my LPSU now... Thats' why I offered  the warning. I've been in a few of these power supplies, some of them are not as stout as you'd hope. definately enjoy the music, and if possible, move on to a larger separate psu for the steppers


---
**Ben Myers** *April 24, 2017 00:32*

Hi **+Mircea Russu** you wouldnt happen to have a wiring diagram would you?




---
**Mircea Russu** *April 24, 2017 07:28*

**+Ben Myers**

 To be honest I don't use it anymore, I've rebuilt from the ground, now using different 12V steppers and mechanical endstops. The wiring was normal MKS/Smoothie diagram with the pinout for the ribbon cable from **+Don Kleinschnitz** [https://www.google.com/search?q=k40+laser+ribbon+cable+pinout&client=ubuntu&espv=2&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjmrMjDyLzTAhXmDpoKHaxvBi4QsAQILw&biw=1920&bih=965#imgrc=6TR-ulJcAFLEgM](https://www.google.com/search?q=k40+laser+ribbon+cable+pinout&client=ubuntu&espv=2&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjmrMjDyLzTAhXmDpoKHaxvBi4QsAQILw&biw=1920&bih=965#imgrc=6TR-ulJcAFLEgM):


---
**Ben Myers** *May 09, 2017 22:29*

thanks Mircea, I have all the movement and endstops working just a little stuck on the pwm for the laser..


---
**Mircea Russu** *May 10, 2017 07:34*

PWM for laser is simple, just have a common GND with the LPS and use the "-" from the pin 2.4 (fan) to the LPS "L"pin (rightmost connector, rightmost pin) [raw.githubusercontent.com](https://raw.githubusercontent.com/chris334/MKS-SBASE/Mks-Sbase-V1.2-English/Mks-Sbase%20v1.2%20Pin%20Layout.jpg)




---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/XM3FZaVSXJU) &mdash; content and formatting may not be reliable*
