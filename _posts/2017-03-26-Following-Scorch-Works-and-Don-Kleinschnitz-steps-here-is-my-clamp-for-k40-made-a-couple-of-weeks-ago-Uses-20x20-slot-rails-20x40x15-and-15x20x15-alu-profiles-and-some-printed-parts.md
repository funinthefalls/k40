---
layout: post
title: "Following Scorch Works and Don Kleinschnitz steps, here is my clamp for k40 (made a couple of weeks ago) Uses 20x20 slot rails, 20x40x1.5 and 15x20x1.5 alu profiles and some printed parts"
date: March 26, 2017 12:36
category: "Modification"
author: "Jorge Robles"
---
Following **+Scorch Works** and **+Don Kleinschnitz** steps, here is my clamp for k40 (made a couple of weeks ago)



Uses 20x20 slot rails, 20x40x1.5 and 15x20x1.5 alu profiles and some printed parts. I've loosely designed at [http://a360.co/2n5biCB](http://a360.co/2n5biCB) 



Have to say I'm very happy with my build,  Green printed parts are jigs of 1.5, 2 and 3mm to be put between the columns and the rails, so can adjust focus in a predictable way.



Cons are that I've lost a bit of cutting area in as Y motor is poorly placed on K40.

If anyone have access to 15x15 rails could gain some space. Anyways, I've got a nice 300x200 cutting area.



Overall cost is under 40EUR





![images/95b9877fd403018d1d9dcf6b38a429f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/95b9877fd403018d1d9dcf6b38a429f1.jpeg)
![images/f1d6c55e708b87bcae96306dcd42f2de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1d6c55e708b87bcae96306dcd42f2de.jpeg)
![images/55073d01cb3ab60509ae2bb777de1ac6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55073d01cb3ab60509ae2bb777de1ac6.jpeg)

**"Jorge Robles"**

---
---
**Don Kleinschnitz Jr.** *March 26, 2017 13:19*

You beat me to the build :). Very very nice work and design ideas.

Couple of questions, 6 actually :):

1). How are the spring shafts in the clamps connected to the upper angle frame,  threaded rivet?

2). Does the unit sit freely in the compartment or bolted?

3).Can you point us to the parts sources for rails.

4). How does the rear clamp work and slide forward and back then lock. Do they slide in the rail and then lock with the wing nut? Any concerns about it binding if the left and right sides are moved relative to each other? 

5). I like the toggle clamp in your CAD design do you have a source for them. I suspect they will interfere with the gantry? i.e to tall?

6). Are the columns fixed height or adjustable if so how do they adjust? 



Some notes: I used a dremmel with cutting wheel to cut away the exhaust port until it was even with the back gantry frame that will buy you enough space for the rear clamp to keep some area.


---
**Jorge Robles** *March 26, 2017 14:26*

**+Don Kleinschnitz**  Much obliged :)



1). Have no threads! The plastic nut has an slot to insert a M5 hex head bolt from the bottom, whose thread end I've cut away. The same nut is made to slot into the rail, providing enough stiffness and squareness, and have a inner M3 nut trap to fix the part to the column.



2. The bottom rails are screwed to the k40 case. Had hard times measuring to get them square and well placed. Used M4 T-Nuts to screw from the bottom outside the case with M4 bolts. The holes made to the case are 5mm, so have some air to perfectly align.



3. [motedis.co.uk - Profile 20 I-Type slot 5](http://www.motedis.co.uk/shop/Slot-profiles/Profile-20-I-Type-slot-5:::999991_99999133.html)



4. It uses the top wing nuts to screw tight to the lower rail. As the threaded shaft have that printed guides to the nuts (blue glue locked to the shaft, 3rd pic) and are M4 (rail holes are 4.8mm) it allows some twist (10~15º) But the whole clamp part is strong enough, no bendings so far.



5. Well they were fine, but as they are printed and have no barrel nuts in my store, made barrel nuts myself with some 8mm Alu tube, drill and thread. The first try to tight the thread went off :|  Anyway, flynuts or knobs are ok :)



6. As you can see, I did some printed supplements (black top of columns) to perfectly reach the focus height (82~83mm) for engraving. Trying to solve the T/2 problem I made that fixed height jigs (green, 2nd pic). If you have to cut 3mm MDF, then loosen flynuts, take away the clamp, insert the jigs on the bottom of the columns, screw again.



I've updated the CAD file, the 40x20 profile is bolted to the rear of the column with rail nuts.



Notes:

I've cutted my exhaust thing also (the black rubber rim is a must if you have to save all your fingers ). I will do again as maybe not enough if insert the bottom jigs. If no jigs used, the rear clamp goes perfectly below that.



An improvement to the upper profile is to cut out a couple of thumb holes, so you can push the lower profile without harming your nails.






---
**Russ “Rsty3914” None** *March 26, 2017 16:12*

you know there is wheel bearings designed for the extruded aluminum that you could implement in your design....They would ride inside the grooves...






---
**Jorge Robles** *March 26, 2017 16:15*

I'm not sure what would do for this clamps. Can you explain? :)


---
**Don Kleinschnitz Jr.** *March 26, 2017 17:57*

**+Jorge Robles** I think that **+Russ None** is saying that we could put wheels that ride on the extrusions to make the columns move easier. I considered that but I think it takes up to much space.


---
**Don Kleinschnitz Jr.** *March 26, 2017 18:13*

**+Jorge Robles** I liked your idea of using the extrusion because the movement side to side is constrained and it is simpler to lock from the top. The extrusion also affords more stiffening options if needed.



I modified my design to do something similar: 

1). The design still allows independent vertical adjustment and locking  of each corner.

2). A separate locking knob(s) lock each columns sliding position.

3). Mounts to floor with 2 brackets and thumbscrews for easy removal. 

.......

All the parts/materials except the 1/4" jack screws and the knobs are off the shelf from home stores, Mcmaster and [servocity.com](http://servocity.com)

...... 

I also plan to use spacers to adjust for 1/2T but haven't added that yet as I am trying to do it without removing the clamp.



Newest version is here: [3dwarehouse.sketchup.com - K40 Manual Clamping Table](https://3dwarehouse.sketchup.com/model/27ed5dd7-ef04-4c53-a5c9-5e5535a1c256/K40-Manual-Clamping-Table)






---
**Don Kleinschnitz Jr.** *March 26, 2017 19:11*

**+Jorge Robles** what do you think of this idea for the 1/2 T problems these spacers are added from the top without removing clamp?



![images/4cfe7613dc319929468eab19bda1bb2d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4cfe7613dc319929468eab19bda1bb2d.jpeg)


---
**Jorge Robles** *March 26, 2017 19:19*

Umm.. cannot figure how are these working. I mean, one of the objectives is to get the material clamped. Using these... the material goes lying on the top? Or contrary, that ones pushes the material down compensating the columns out of focus? 



I was thinking on it, but ended using the spacers. One minute task.

Another solution could be having (on my build) knobs on the rear fixing bolts, loosing the upper clamp and insert spacers there, but seems more difficult to do.


---
**Jorge Robles** *March 26, 2017 19:26*

**+Don Kleinschnitz** Have to say, I don't change material each work, so don't find so hard to do :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 26, 2017 20:03*

For adjusting the focus, could you have a separate rail between the top & the bottom (spring loaded rail) that you can push down with a threaded bolt? This would allow you to adjust the focus height in whatever increments you need.

![images/551a5763a5034e5fd1e61eb4e373c949.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/551a5763a5034e5fd1e61eb4e373c949.jpeg)


---
**Jorge Robles** *March 26, 2017 20:16*

**+Yuusuf Sallahuddin**​ Nice! Have to find a place to put them without losing clamping  space. Also you will have a limit off focus. But instead screws I should use calibrated spacers ;)


---
**Don Kleinschnitz Jr.** *March 26, 2017 22:46*

**+Jorge Robles**​​​ you insert spacer in the hole and slide to the left. The lower part of the spacer pushes the surface of the material down. 

The keyhole slot can also be put in the top frame. 

Different size spacers push material down different distances. 

Works very similar to **+Yuusuf Sallahuddin**​​ adjustable upper plate. 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 01:26*

I'm curious for any of you guys who have actually built this setup, does it work well with non-rigid materials too (e.g. leather)?


---
**Don Kleinschnitz Jr.** *March 27, 2017 01:30*

**+Yuusuf Sallahuddin** I am designing, **+Jorge Robles**​ is building. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 01:48*

**+Don Kleinschnitz** I figured you wouldn't have built yet, with your tinkering style I'd imagined you want to get the build "just right" before you starting putting any tools to materials :)


---
**Ulf Stahmer** *March 27, 2017 03:15*

Nice work **+Jorge Robles**! I really like this clamping mechanism as well and am in the process of building my own. I found some 2" springs and 2" Chicago screws (see photo below) at Home Depot and as the springs sit on the big head of the screw, it eliminates the need for the bottom plate. It looks like it will work really well. With minimal effort, the design could even be modified to be laser cut and glued. All you'd need in parts is a sheet of 3 mm MDF, 4 springs and 4 Chicago screws. Really inexpensive!



I'll post a photo of my setup when I'm done.

![images/f0fbcf9be7b22c87b6d3ee3c3f52a912.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f0fbcf9be7b22c87b6d3ee3c3f52a912.jpeg)


---
**Ulf Stahmer** *March 27, 2017 03:27*

**+Yuusuf Sallahuddin**  Don's an engineer through and through. I understand his affliction very well as I, too, am one.  This need for design engineers to "understand" often trumps the "doing" and tends to become more pronounced as we age. :) Gotta love **+Don Kleinschnitz** for his passion!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 05:01*

**+Ulf Stahmer** I have to say everything **+Don Kleinschnitz** does is extremely useful to the rest of us, so his affliction is a welcomed one. I find myself designing things more often than making them too, usually due to a lack of resources (hate waiting for 

parts in the mail so I usually procrastinate on ordering them) or getting side-tracked with new ideas that I can't wait to design.


---
**Jorge Robles** *March 27, 2017 06:18*

**+Yuusuf Sallahuddin** leather and so requires hard mesh below. Clamps cannot tension enough to keep that straight.

Also, the mesh should be rigid enough. I've tried with some light metallic mesh and bends upon tightening.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 09:29*

**+Jorge Robles** Thanks for the clarification. Looks like I'll be sticking to my current setup for a while then :)


---
**Don Kleinschnitz Jr.** *March 27, 2017 12:11*

**+Ulf Stahmer** thanks for recognizing my affliction and its great to know there are others out there with the same condition. 



You all would probably be surprised how much I build, its just lately I choose very carefully what it is and for whom. 



My passion while I was in corp USA was wood turning: [www.turnedoutright.com](http://www.turnedoutright.com)



...................................................

My learned "maker" philosophy:

... CAD twice cut once

... Every solution is hidden under its complete understanding of the problem

... The hands of many collaborative posts make light work

... Genius is 80% learning, listening and open sourcing; followed by 20% perspiration

... If you listen, the person that uses your product will tell you when your are done designing 

... As simple as possible but not simpler, means that its complicated to get to simple

... Any sufficiently advanced technology is indistinguishable from your ability to exceeding the limits of your imagination

... The most brilliant designs are those that look like they aren't

... Once a problem has been solved the excitement depletes exponentially with the number of clones you build

... The first build of a solution will always work better than any other clone

...

 It's my grandsons birthday soon. The cutting templates were designed to be laser cut the wood was not (to thick).

![images/1bb08bd504fb79ef1c25a10277716423.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bb08bd504fb79ef1c25a10277716423.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 12:27*

**+Don Kleinschnitz** That's a great gift for the grandson. Reminds me of gifts from my grandfather when I was a small child (& even again when my niece was a small child). I'm sure the little fellow will love it. There was another one like this that my grandfather also made which was a puppy with moving legs like the cricket/grasshopper (in your pic), but he also had a pivoting waist & tail. Thought might be an idea for your next gift for the little guy.


---
**Don Kleinschnitz Jr.** *March 27, 2017 12:30*

**+Yuusuf Sallahuddin** if you ever find a link to plans or a picture I will add it to his collection.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 12:34*

**+Don Kleinschnitz** If I can't find a link I'll sketch up something based on the design I remember (or maybe even check with my sister if my niece still has it to take photos of).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 12:41*

**+Don Kleinschnitz** I'll make a separate post for you on my wall so I stop spamming Jorge's post.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 12:51*

**+Don Kleinschnitz** No actual progress on that as yet. Been a bit side-tracked with other ideas lately, but it's still on the cards.


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/KAymmyGZnQ1) &mdash; content and formatting may not be reliable*
