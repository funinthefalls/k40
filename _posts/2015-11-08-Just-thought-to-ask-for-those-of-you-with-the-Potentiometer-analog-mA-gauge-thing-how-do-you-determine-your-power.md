---
layout: post
title: "Just thought to ask... for those of you with the Potentiometer & analog mA gauge thing, how do you determine your power?"
date: November 08, 2015 11:52
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Just thought to ask... for those of you with the Potentiometer & analog mA gauge thing, how do you determine your power?



e.g. I say that I am using 6mA power, because when I turned the potentiometer & held test fire down for 1-2 seconds the value showing on gauge was 6mA.



Is this correct method?





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Stephane Buisson** *November 08, 2015 14:01*

**+Yuusuf Salahuddin**  can I ask you to limit your number of post for the clarity of the whole page. (to not dilute and allow other to quickly find out their favorite subject)



it's not a censorship, you can of course use as much comments as you want, but in a fewer posts way.



Thank you


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 08, 2015 15:24*

**+Stephane Buisson** Can do Stephane. I'll try limit my posts to more specific issues.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 08, 2015 15:25*

**+Carl Duncan** Thanks for that idea Carl. Just might have to try that instead.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 12, 2015 18:13*

**+Stephane Buisson** Is there a possibility of adding extra pages or topic boards into this group? Just had a thought of if we had a topic/page specifically for jobs/tests/settings used would be good place for people to post images of jobs completed with what settings they used.


---
**Stephane Buisson** *November 12, 2015 18:32*

**+Yuusuf Salahuddin** of course, excelent idea, that the way to improve readeablity.



done, ask for more if you want


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 13, 2015 03:25*

**+Stephane Buisson** Yeah I just thought I'd like to post my tests/projects so others can see results, without clogging up the main page/area.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/VFQnk2VAoB3) &mdash; content and formatting may not be reliable*
