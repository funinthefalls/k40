---
layout: post
title: "Hello all, First off, I want to thank all of you for the knowledge I've acquired by reading your posts"
date: December 28, 2016 06:52
category: "Original software and hardware issues"
author: "Eric Raymond"
---
Hello all, 

First off, I want to thank all of you for the knowledge I've acquired by reading your posts.  It's helped me navigate Corel laser and taught me about my laser cutter.  Unfortunately, I've got a problem.  I bought a KH 3020B 40w CO2 laser cutter off eBay back in July.  I didn't use it much until last month when started this project.  I've been using it  cut parts out of ¼" mdf. It's been working great.  I've got the power set to 50 percent...and running it for 6 passes with air assist.  They're cutting nice...everything was great until the laser stopped lasing.  The controller board is still firing it, but nothing is coming out of the tube.  When I fire the laser manually, the tube glows very faintly at the anode end. Looked in the manual and it says the tube is only warrantied for 3 months... Has anyone else had a similar situation?   How would I go about testing the power supply or tube?  Thanks

Eric





**"Eric Raymond"**

---
---
**Ned Hill** *December 29, 2016 02:50*

See this post for some good troubleshooting info.  [plus.google.com - (EDIT: Problem solved, thank you to all who helped. This post hasgarnered some…](https://plus.google.com/+vapetech/posts/U1pax4sFSXS)


---
**Eric Raymond** *December 29, 2016 03:42*

Thanks....I'll check it out and let you know.


---
*Imported from [Google+](https://plus.google.com/113987339959685856688/posts/HUg2LdSagtR) &mdash; content and formatting may not be reliable*
