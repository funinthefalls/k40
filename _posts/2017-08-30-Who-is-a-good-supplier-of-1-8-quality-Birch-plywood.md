---
layout: post
title: "Who is a good supplier of 1/8 quality Birch plywood?"
date: August 30, 2017 14:25
category: "Materials and settings"
author: "William Kearns"
---
Who is a good supplier of 1/8 quality Birch plywood? Any tips?





**"William Kearns"**

---
---
**Jim Hatch** *August 30, 2017 15:46*

I use Amazon with mixed results - the 1/8" stuff seems to be pretty consistent. The 1/4" stuff less so.



When it's critical that I get consistent cutting results over multiple iterations of a project I get it from the local Woodcraft store (they're a chain so you may have one near you). They have real Baltic Birch (some Baltic Birch plywood is actually regular Birch ply that doesn't meet the manufacturing definition of Baltic Birch and has more voids, etc). You can get full sheets (60"x60" - BB isn't usually made in 4x8 foot sheets) and cut it down or get cuts from them in some fairly common sizes. Prices are reasonable and quality is consistent.


---
**Ashley M. Kirchner [Norym]** *August 30, 2017 16:31*

[ocoochhardwoods.com - Ocooch Hardwoods - Supplier of Thin Wood for Scroll Sawing, Carving Blocks, Intarsia wood, Plywood for scroll sawing, and more.](http://www.ocoochhardwoods.com) - not only for Birch but other woods as well.


---
**Ned Hill** *September 01, 2017 04:28*

I second ocooch hardwoods.  Good product and great people to deal with.


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/TfLQwv2qAd4) &mdash; content and formatting may not be reliable*
