---
layout: post
title: "I am continually annoyed by the covers on my K40 slamming closed"
date: May 13, 2018 20:23
category: "Modification"
author: "Don Kleinschnitz Jr."
---
I am continually annoyed by the covers on my K40 slamming closed. Have considered gas's springs etc...

A bit of a cheesy looking fix but one more annoyance elliminated! 


**Video content missing for image https://lh3.googleusercontent.com/-wmvTiOVoOMs/Wviev4nehUI/AAAAAAAA4AM/FI1m7F8WBkQ2iJA3ntfT6lt1pnfnEVNtACJoC/s0/20180513_131531.mp4**
![images/c001132cf6b18428505527436d6b0261.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c001132cf6b18428505527436d6b0261.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Paul de Groot** *May 13, 2018 21:51*

I agree more, I got my hands twice between the lid with a bang ^_^


---
**Printin Addiction** *May 14, 2018 02:14*

Very simple and clever.....


---
**Nitro Zeus** *May 16, 2018 15:49*

Maybe just square it up? just 3d print a little box to cover it. That would look fine.

Like a little cube.


---
**Nitro Zeus** *May 16, 2018 15:50*

**+Nitro Zeus** or a trapezoid 


---
**Shamblen Davidson** *May 21, 2018 00:33*

I have a shelf above mine with a magnet hanging down that touches the lid when I lift it up, and that keeps it in place.


---
**Grey Drifter** *May 30, 2018 02:16*

nice execution. would rubber feet on the lower sill or the lid let in too much air?




---
**Don Kleinschnitz Jr.** *May 30, 2018 12:22*

**+Grey Drifter** some of us do exactly that because some make up air is a good thing to get good flow across the cut. Some drill holes in the front as well. 


---
**Eric Lovejoy** *June 15, 2018 10:38*

just lay down some foam tape on the edge. 




---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/i38RfoCWHJv) &mdash; content and formatting may not be reliable*
