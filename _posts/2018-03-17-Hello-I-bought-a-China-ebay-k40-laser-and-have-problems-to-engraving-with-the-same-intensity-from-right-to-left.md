---
layout: post
title: "Hello I bought a China (ebay) k40 laser and have problems to engraving with the same intensity from right to left"
date: March 17, 2018 11:16
category: "Original software and hardware issues"
author: "M\u00e1rio Santos"
---
Hello I bought a China (ebay) k40 laser and have problems to engraving with the same intensity from right to left.

When it goes to the right it does not engraving with the same intensity, it seems that the laser loses power in the right.

It will be because of the alignment of the laser / mirror?

Thanks for some tips :)﻿





**"M\u00e1rio Santos"**

---
---
**Brass Honcho** *March 17, 2018 15:44*

clean the focus lens;  ive had the problem before.  Smoke stains will most accumulate on the areas of the lens that are least used.  Unscrew the housing for the focus leans then clean the lens. 


---
**HalfNormal** *March 17, 2018 16:02*

Make sure what you are working on is level to the head.


---
**Mário Santos** *March 17, 2018 17:03*

**+Brass Honcho**  I'll try this, thanks for the tip and for your time :)

**+HalfNormal** Thank for your tip :)




---
**Ned Hill** *March 17, 2018 17:24*

You also need to verify that your alignment is good.  


---
*Imported from [Google+](https://plus.google.com/111123929078847414796/posts/So9XCZ6RKRq) &mdash; content and formatting may not be reliable*
