---
layout: post
title: "How does one setup multiple pass cutting on the laser"
date: November 13, 2015 02:03
category: "Software"
author: "Embeddedtronics"
---
How does one setup multiple pass cutting on the laser.  K40 with coreldraw/laserdraw 



Thanks







**"Embeddedtronics"**

---
---
**Sean Cherven** *November 13, 2015 03:52*

Oddly Enough, I've yet been able to figure this out myself (atleast not accurately).


---
**Embeddedtronics** *November 13, 2015 04:04*

My google-fu hasn't been good. Not one good answer found yet. 


---
**Sean Cherven** *November 13, 2015 04:12*

Hopefully somebody else here has the answer. 


---
**Anthony Bolgar** *November 13, 2015 12:57*

Could it be as simple as just running the design 2 or three times in a row without moving the target material on the cutting bed?


---
**Embeddedtronics** *November 13, 2015 13:44*

I use the "repeat" command but after each pass, a dialog box pops up on the screen and you have to press OK for the next pass to start.  I would like to find a way that it will do all passes consecutively. 


---
**Embeddedtronics** *November 13, 2015 15:32*

This is the work around I found this morning.  For each pass you want, add it as a task.  So for three passes, add task three times and press start. 


---
*Imported from [Google+](https://plus.google.com/110430811460241251216/posts/Akspz6ACoD6) &mdash; content and formatting may not be reliable*
