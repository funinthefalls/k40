---
layout: post
title: "Here is a not too expensive air dryer for those having issues with moisture in the air assist air source"
date: May 27, 2017 01:37
category: "Air Assist"
author: "Anthony Bolgar"
---
Here is a not too expensive air dryer for those having issues with moisture in the air assist air source.



[https://www.harborfreight.com/compressed-air-dryer-40211.html](https://www.harborfreight.com/compressed-air-dryer-40211.html)







**"Anthony Bolgar"**

---
---
**Brent Crosby** *May 27, 2017 13:52*

I had one of those . . . only lasted a couple of years. 



Replaced it with this, which seems to be a much better built product:

[https://www.ebay.com/p/?iid=131488655430](https://www.ebay.com/p/?iid=131488655430)

[ebay.com - Ingersoll Rand DryStar 15 SCFM Refrigerated Air Dryer - D25IN &#x7c; eBay](https://www.ebay.com/p/?iid=131488655430&&&dispItem=1&ul_ref=http%253A%252F%252Frover.ebay.com%252Frover%252F1%252F711-117182-37290-0%252F2%253Fmpre%253Dhttp%25253A%25252F%25252Fwww.ebay.com%25252Fitm%25252Flike%25252F131488655430%25253Fchn%25253Dps%252526dispItem%25253D1%2526itemid%253D131488655430%2526targetid%253D340521012127%2526device%253Dm%2526adtype%253Dpla%2526googleloc%253D9033717%2526poi%253D%2526campaignid%253D764892186%2526adgroupid%253D39783961149%2526rlsatarget%253Daud-296850005184%253Apla-340521012127%2526abcId%253D978836%2526merchantid%253D101501455%2526gclid%253DCIr7lO-akNQCFUZrfgodaj8LuA%2526srcrot%253D711-117182-37290-0%2526rvr_id%253D1219498417964&chn=ps)


---
**Anthony Bolgar** *May 27, 2017 15:55*

Thanks, I'll check out that link


---
**Phillip Conroy** *June 09, 2017 10:10*

I had problems with moisture on focal less added 1 inch wide by 1 meter length of pvc pipe filled with desiccant water absorbing crystals, worked great for 9 months ,then winter hit and was getting water again,added another meter of crystals problem solved,total cost $60


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/iwKsNvHvEpR) &mdash; content and formatting may not be reliable*
