---
layout: post
title: "I made these gift card holders to add some originality rather than the standard envelope"
date: December 26, 2015 14:53
category: "Object produced with laser"
author: "Cam Mayor"
---
I made these gift card holders to add some originality rather than the standard envelope. They went over very well. Easy to design and fast to make.

![images/66b94cdfca296986ba4f137efd513398.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/66b94cdfca296986ba4f137efd513398.jpeg)



**"Cam Mayor"**

---
---
**Anthony Bolgar** *December 26, 2015 16:28*

Could you share the files please. I think these are great.


---
*Imported from [Google+](https://plus.google.com/110883213653290462996/posts/FbMua2hApwr) &mdash; content and formatting may not be reliable*
