---
layout: post
title: "What is the difference between a ZnSe and GaAs focus lens?"
date: May 07, 2016 01:50
category: "Discussion"
author: "Custom Creations"
---
What is the difference between a ZnSe and GaAs focus lens?





**"Custom Creations"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 02:12*

[http://directedlight.com/laser-components/catalog/laser-co2-lenses/](http://directedlight.com/laser-components/catalog/laser-co2-lenses/)


---
**Custom Creations** *May 07, 2016 02:33*

**+Yuusuf Sallahuddin** Thank you! So compared to the stock K40 setup, would these things be the best combination?



[http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F381mm-P799.aspx](http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F381mm-P799.aspx)



[http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)


---
**Jim Hatch** *May 07, 2016 03:01*

I went with the 50.8mm focal lens version of the one you're looking at. (Same air assist head.) The 38mm one will only leave about 10mm to the material. That's good & bad. The good is you get somewhat better power because the light beam has less distance to spread. Not sure if that's a material difference or just academic for our purposes.



The bad is that it restricts the space between the material and head for the smoke to be dispersed. That might cause swirling and cavitations around the nozzle. Might also allow the occasional flame that is not pushed aside by the air jet to hit the nozzle. Again not sure if this is material or academic.



The 50.8 works for me. Would be good to know from someone who has the 38.1 for their real world experience to know for sure.


---
**Custom Creations** *May 07, 2016 03:07*

**+Jim Hatch** I thought it was measured from nozzle tip.. The 38.1mm is 1.5" and the 50.8mm is 2". Not much of a big diff with a powerful exhaust fan and a decent air pump for the assist. Best of all, you get the power at a shorter focal length. That was my assumption anyways. I am new to this and am probably looking at this the wrong way but would like for some real world input ;)


---
**Jim Hatch** *May 07, 2016 03:13*

Not measured from the nozzle tip - measured from the lens. The lens is up near the base of that knurled ring you see on the nozzle.



You're right about looking for real world input. These are not high precision machines outside of the laser assembly. 



Not sure who has the shorter focal length lens on here. Most of the folks who have switched that I know about went with the 50. It'd be nice to have some real data.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 07:07*

**+Custom Creations** **+Jim Hatch** I notice in that article that I linked that it suggests that:



"At any given focal length, a larger diameter lens will yield a smaller focused spot if the incoming beam is expanded to fill the larger lens."



Does this mean if we were to get a wider diameter lens & use a beam splitter (which I've seen somewhere) that it would cause the beam to split larger & hit the lens in a larger areas, focusing in more to give a better focus?


---
**Scott Thorne** *May 07, 2016 09:43*

I've got the 38mm....it's great for engraving ....not so good for cutting because the waist of the beam is shorter than the 50.8mm lens...the 38mm has a smaller dot size though...but being closer to the material makes my air assist work better. 


---
**Jim Hatch** *May 07, 2016 12:45*

**+Scott Thorne**​ good real world input. Thanks!


---
**Thor Johnson** *May 07, 2016 21:33*

I have the 25mm (came with the machine)...  what +Scott Thorne said.  It makes a nice fine dot, but you can't cut thru >3/16 in one pass with it because the depth of focus is so small.  I will be adding a 50 asap.



Will someone with a 50 show how fine of a line they can engrave?   I don't think I'll keep using the 25, but...


---
**Scott Thorne** *May 08, 2016 08:35*

**+Thor Johnson**...I have 1 of every focal length...the 50.8 dot is not much bigger than the 38.0 but it Cuts so much better...I think it's. 007 of an inch.


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/bu3NppdAjBr) &mdash; content and formatting may not be reliable*
