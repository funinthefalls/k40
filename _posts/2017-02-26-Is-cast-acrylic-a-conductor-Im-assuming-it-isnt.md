---
layout: post
title: "Is cast acrylic a conductor? I'm assuming it isn't"
date: February 26, 2017 03:53
category: "Discussion"
author: "Bill Keeter"
---
Is cast acrylic a conductor? I'm assuming it isn't. It being plastic and all.  ...BUT since I've got a project where where acrylic will touch a PCB i'm thinking it's safer to ask.





**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 04:04*

I have no reason to believe it is. Plastic tends not to be, and cast acrylic doesn't have any metallic additives to my knowledge. 

Should be fine, I'm sure that's been done more than several times. Use plastic standoffs if possible? :)


---
**Ned Hill** *February 26, 2017 04:05*

At reasonable voltages it will be a insulator.  I think it will be fine.


---
**Bill Keeter** *February 26, 2017 04:10*

yeah, the PCB is only running 5V


---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 04:11*

Ned makes a valid point, dielectric stuff gets fun at around 300v. Insulators start conducting, etc. fun stuff all around. 


---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 04:11*

Ask me why I like flux so much. 


---
**Alex Krause** *February 26, 2017 07:02*

As far as plastics go I have noticed less charge in acrylic in the shavings that are generated while machining than other plastics PVC a common insulation used for wires holds a charge when machined. When I mill it the static charge makes it stick to everything... And trasnsferance via air suction of those shavings through a tube results in the worst static discharge I've ever seen.. I know this doesn't build confidence just want you to know to remove any fine particles that adhear to the plastic after laser or CNC fabrication 


---
**Don Kleinschnitz Jr.** *February 26, 2017 16:17*

The dielectric strength (minimum breakdown voltage) of:

Acrylic ................

.. 430 V/mill.

.. 430 V /.001 

...430V/.025 mm

... 17,200 V/mm

PVC ......................

... 544 V/mil

... 21,760 V/mm

AIR

... 3,000V/mm

... 76 V/mil

.......................................

Therefor for a .22" (5.58 mm) thick peice of acrylic the dielectric strength = (.22 / .001) * 430= 94,600 volts.

........................................... 

Standard appliance wire is PVC or PVC/Nylon 300 & 600 V dependent on cable construction.

FEP/PFA/ PTFE cable can be as low as 125V

...........................................................

The dielectric constant (@ 1Khz)

... Acrylic = 2.7 -4.5

... Plexiglass 3.2

... PVC =  3.19

As you can see all these plastics have very similar charging capacities

...........



I use Home Depot acrylic for most of my projects because it is such a good insulator, is strong, easy to cut and mill and its clear. For fastening I use acrylic glue, threaded inserts and plastic screws.

 

.......I needed these #s to design a safe HV LPS lab :)


---
**Ned Hill** *February 26, 2017 16:20*

Lol I knew **+Don Kleinschnitz** was going to chime in with some numbers.  Excellent info as always. :)


---
**Don Kleinschnitz Jr.** *February 26, 2017 16:24*

**+Ned Hill** didn't want to disappoint lol. It was coincidental timing cause I just got done (yesterday) doing these calculations to determine how to protect myself from 20,000 volts :).


---
**Ned Hill** *February 26, 2017 16:32*

**+Alex Krause** as you pointed PVC is very prone to static (triboelectric) charging.  This is because of the chlorine content.  Halogen elements (F, Cl, Br, I, At; listed in order of decreasing electronegativity) are very electronegative, meaning they love to pick up extra electrons from other things. 


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/7mJCZU1yvXn) &mdash; content and formatting may not be reliable*
