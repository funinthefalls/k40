---
layout: post
title: "Can't remember if this was posted before"
date: April 13, 2017 10:00
category: "Discussion"
author: "James G."
---
Can't remember if this was posted before





[http://www.instructables.com/id/Patterns-Collection-for-Bending-Rigid-Materials](http://www.instructables.com/id/Patterns-Collection-for-Bending-Rigid-Materials)

![images/a3d184385a0a810e947a1a466f4c1796.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3d184385a0a810e947a1a466f4c1796.jpeg)



**"James G."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2017 10:15*

Saw that on Instructables earlier today. Almost shared it myself. Will be a handy reference for many materials.


---
*Imported from [Google+](https://plus.google.com/114312362882399477330/posts/d8hhNNqMqkr) &mdash; content and formatting may not be reliable*
