---
layout: post
title: "Here we go guys. Z-Table design v2"
date: December 21, 2015 00:49
category: "Modification"
author: "ChiRag Chaudhari"
---
Here we go guys. Z-Table design v2. Only about $25 !!!



If ordered every thing from china and if you are ready to wait:

> 5 x GT2 20T pulley = $5

> 1 x GT2 Timing Belt = $5 (5 feet cut to length- more that what we need)

> 1 x Nema 17 = $9



This one I got from HomeDepot.com not sure if available everywhere or not, but will spend some more time looking for alternative.

> 4 x 7/8" Shower door rollers = $5 



Total = $24 and lets say if we order everything form within the country may be $40-50 depending on where you are. 

![images/2e60aa622fb5d00f9452e2cf6b99efab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2e60aa622fb5d00f9452e2cf6b99efab.jpeg)



**"ChiRag Chaudhari"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 00:59*

Looks great. Now I understand the way it will raise/lower all 4 corners at same time. I am curious (and have been since the last design), what is the purpose of the shower roller?


---
**ChiRag Chaudhari** *December 21, 2015 01:22*

**+Yuusuf Sallahuddin** Glad you asked. But I hope I can put that in words after 4 IPA.. :P . If you look at its design, its 100% perfect for our application and its cheap! The center part can be fixed to the bottom plate with provided bolt. Surrounding plastic wheel/roller spins free that can be attached to our pulley and we can achieve the movement we need for our Z Table to work. Same size bearing is 1. more expensive and 2. center need to be raised from bottom plate to let outer part spin free. 



I hope I made some sense. Again cheap and easy available at local hardware store are two key things I look for in any Mod. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 01:43*

**+Chirag Chaudhari** Yeah that definitely makes sense. I thought it was something similar to that. It's a really great & simple design. I love how it works & would love to integrate into my conveyor-table idea. Will give me focal-length precision control of the height (for when cutting different thicknesses of leather) & also allow me to do larger pieces in one sheet (minimising waste leather; as I usually end up with near-useless scraps of leather).



So is the 7/8" spacer (acrylic/wood) glued to the shower door roller so that the 20T GT2 Pulley can be stuck to it too?



I have a thought that we could possibly reduce the overall height by removing the 20T GT2 pulley & integrating it into the 7/8" spacer. Could cut a hexagon in the centre of the 7/8" spacer-pulley-combo to put a nut (like what you have at the top) & spot weld it to the 5mm threaded shaft?


---
**Paul de Groot** *December 21, 2015 02:11*

How do you prevent it from locking up when running into the ends of the threaded rod?


---
**Anthony Coafield** *December 21, 2015 04:08*

This looks great. I could easily adapt it to just us a hand crank if I wanted to also...


---
**ChiRag Chaudhari** *December 21, 2015 04:39*

**+Paul de Groot**​ well for that just like X and Y axes we have to put optical End switch for Y axes. ﻿Edit: Sorry not Y but Z axes.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 05:24*

**+Chirag Chaudhari** I think you mean for Z-axis, not Y.



Or we could just be sure to not make it go too far. I don't have any end switches for my x & y axes however I just make sure not to go past 300mm on the x-axis & 228mm on the y-axis.



Although, end switches would be ideal to prevent it going too far, as when mine did (while I was testing what is too far) it vibrated all the mirrors out of alignment.


---
**Gee Willikers** *December 21, 2015 05:27*

FWIW -- I have one of these. Small but does the job.

[http://www.lightobject.com/Power-table-bed-kit-for-K40-small-laser-machine-P722.aspx](http://www.lightobject.com/Power-table-bed-kit-for-K40-small-laser-machine-P722.aspx)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 05:33*

**+Jeff G** That's great, but no fun! Always more fun to design & build your own stuff (& usually more expensive in my own experience).


---
**Gee Willikers** *December 21, 2015 05:37*

**+Yuusuf Sallahuddin** I meant to build my own, but I did some work for my day job and my boss said "...company credit card... buy what you need to facilitate the production of..." So I bought some accessories.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 05:42*

**+Jeff G** That's awesome. I need a boss like that haha.


---
**ChiRag Chaudhari** *December 21, 2015 06:17*

**+Jeff G** I have spent so much money making something that already exist but at higher prices, so many times that it's just  ridiculous. But the end result is just so awesome and helps many people spend few bucks and get decent results. 

Of course that Z Table may just work out of the box, but since I am involved in a little bit of manufacturing, when I see something that can be built for less than half the cost, I go for it. I get hell of a kik out of it. LOL. 


---
**ChiRag Chaudhari** *December 21, 2015 06:44*

**+Yuusuf Sallahuddin** "So is the 7/8" spacer (acrylic/wood) glued to the shower door roller so that the 20T GT2 Pulley can be stuck to it too?"

That is exactly the plan Yussuf. The center part of the pulley that stay stationary is 15mm in Diameter and then there is about 3mm plastic roller part that spins free around it which we can be used to make this idea work. However if we user bigger pulley we can eliminate spacer and save 2-3mm. But since I have 6 of the 20T GT2 pulleys lying around I decided to go with them. 



If somehow we can cut the groves/teeth on or around the shower door roller OMG, that will reduce the height by another 15mm. But we must save the belt from coming off while turning the pulley from top or bottom. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 06:54*

**+Chirag Chaudhari** I did do a test cutting the 40T on 3mm MDF & I don't like it. The results are not clean because of scorching. Maybe it is possible to create better pulleys with more air-pressure on air-assist, however I am unsure. Or possibly different material (acrylic maybe) might give better results.


---
**ChiRag Chaudhari** *December 21, 2015 06:58*

**+Yuusuf Sallahuddin** You are absolutely right. You know what next couple of weeks Im  going to be so busy, but I have to go visit University Fab Lab and use their Epilog Laser to try cut some 30T Pulleys on Acrylic. That machine is a beast. It does so amazing job. After all its 20K machine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 08:06*

**+Chirag Chaudhari** I am not sure if we have something like that here, but what is a FabLab? Do you get to use their equipment for free or you have to be a student or you pay some fees?


---
**Imko Beckhoven van** *December 21, 2015 08:43*

How do you join the timing belt to make a loop? I have a simular setup as my Z bed for 3d printer and I had to buy a looped timing belt. If it skips you will lose the level of the bed and worse get it to get stuck....


---
**ChiRag Chaudhari** *December 21, 2015 16:32*

**+Yuusuf Sallahuddin** FabLab is like Maker Lab except there is no membership and its almost free to use. They charge $1 for 5 minutes of use of laser cutter, $$/gram for 3D printing. Plus they have embroidery machines, vinyl cutters and many more small equipments. Also they teach you to program arduino and many different things. Pretty awesome place to hangout.


---
**ChiRag Chaudhari** *December 21, 2015 16:33*

**+Imko Beckhoven van** Good catch. But I found out that they do sell 6ft closed loop 2GT belts. I think they are called 1524mm 2GT closed loop.


---
**Imko Beckhoven van** *December 21, 2015 16:36*

**+Chirag Chaudhari**​ I know ;-) got mine if I rember correctly from adafruit [https://www.adafruit.com/products/1184](https://www.adafruit.com/products/1184)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 19:53*

**+Chirag Chaudhari** I did find there is 3 FabLab in Australia haha. Luckily enough, 1 of them is within 1 hour drive from my home. It is at the State Library of QLD & I check their details & I only see costs for consumable materials (e.g. acrylic or 3d printing plastic). Also a $25 cost for a safety induction. I will have to go up one day & take a look.


---
**ChiRag Chaudhari** *December 21, 2015 20:17*

**+Yuusuf Sallahuddin** That is awesome man. Also I am looking into some other option instead of gluing pulley to the door roller. 2 Bolt bearing block looks interesting. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 20:22*

**+Chirag Chaudhari** 2 bolt bearing block, I had to google. But it looks very promising. Particularly the one that mounts like this

[http://www.bearingson.com/PrdImg/2-bolt_flange_bearings/cast_iron_2-bolt_flanges/1333_CAT.jpg](http://www.bearingson.com/PrdImg/2-bolt_flange_bearings/cast_iron_2-bolt_flanges/1333_CAT.jpg)



So you could in fact have your threaded shaft run from this section all the way up to the table, with the pulley anywhere you wanted on the shaft. It would be good if you can get the threaded shaft with maybe 1 inch of one end unthreaded (to slip the bearing block & pulley over & grub screw on).


---
**Anthony Bolgar** *January 14, 2016 11:57*

Lll you need to do is file a flat on the threaded rod where you want the GT2 pully to grip the threaded rod.


---
**Anthony Bolgar** *January 14, 2016 12:01*

Paul, to stop it from locking up you can do 1 of 2 things:



a) just pay attention to what you are doing and take your finger off the raise button before it reaches the end.



b) install limit switch that breaks the stepper circuit if the endstop is triggered.



To me, the easiest would be to just pay attention and take your finger off the button.




---
**Jose A. S** *January 14, 2016 14:42*

Where did you get the NEMA 17 for only 9 dlls?

Thank in advance!


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/f1ZYrpYpmAr) &mdash; content and formatting may not be reliable*
