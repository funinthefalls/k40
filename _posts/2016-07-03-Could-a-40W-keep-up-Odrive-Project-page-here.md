---
layout: post
title: "Could a 40W keep up? Odrive! Project page here;"
date: July 03, 2016 05:06
category: "Modification"
author: "HalfNormal"
---
Could a 40W keep up?



Odrive! Project page here; [https://hackaday.io/project/11583-odrive-high-performance-motor-control](https://hackaday.io/project/11583-odrive-high-performance-motor-control)



Demonstration here


{% include youtubePlayer.html id="WT4E5nb3KtY" %}
[https://www.youtube.com/watch?v=WT4E5nb3KtY](https://www.youtube.com/watch?v=WT4E5nb3KtY)





**"HalfNormal"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 03, 2016 05:33*

It would be nice if they could, then we could get faster engraves!


---
**Ariel Yahni (UniKpty)** *July 03, 2016 05:34*

That's insane


---
**Ned Hill** *July 03, 2016 14:38*

Lol that is insane.  Wonder what kind of laser power you would need to engrave at that speed.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/a7LZ49J6D8y) &mdash; content and formatting may not be reliable*
