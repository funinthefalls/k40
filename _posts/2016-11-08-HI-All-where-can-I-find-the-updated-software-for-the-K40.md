---
layout: post
title: "HI All, where can I find the updated software for the K40"
date: November 08, 2016 16:59
category: "Software"
author: "Mickey Carlson"
---
HI All, where can I find the updated software for the K40. My old laptop died and bought a new one with Windows 10 and it dosent recognize the engraver or the software that came with it!!



Help!!

Thanks





**"Mickey Carlson"**

---
---
**HalfNormal** *November 09, 2016 02:09*

What controller board?


---
**Mickey Carlson** *November 09, 2016 14:16*

Not sure, its the one that came with the machine!


---
**HalfNormal** *November 09, 2016 14:17*

There are two, a nano or Moshi. 


---
**Mickey Carlson** *November 11, 2016 05:38*

I've seen a pic of a nano, but mine doesn't have either name on it. Only Lihuiyu Studio Labs

![images/f50dbb5115340c1226b8b81e1922dab1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f50dbb5115340c1226b8b81e1922dab1.jpeg)


---
**Mickey Carlson** *November 11, 2016 05:51*

Just can't figure out why my new laptop running windows 10 doesn't find it!


---
**Mark Smith** *November 11, 2016 05:56*

I'm just getting started and did not get a CD, and the orange metallic dongle is not mounting a disk on any computer I own (windows and mac)... My board is similar to Mickey's except says M9




---
**Mickey Carlson** *November 11, 2016 06:39*

I think my USB stick went bad


---
**HalfNormal** *November 11, 2016 14:59*

The USB dongle does not mount like a disk. You have to look under USB in device manager to see it. If the computer does not see the board, it could be the board or the 5 VDC supplying the board. 


---
**Michael Isaac** *November 11, 2016 16:47*

I had the same problem. As far as I know there is no new software. I reverted to an older version of Windows to get it to work right. 


---
**HalfNormal** *November 11, 2016 18:34*

Many of us are using Win10 without issues.


---
**Michael Isaac** *November 11, 2016 21:04*

I'll have to try again. When I tried before Win 10 was new, maybe updates have fixed it now.


---
**Mickey Carlson** *November 15, 2016 16:04*

The USB dongle is listed in Device Manager as "USBkey"" but I can't access it.

Does anybody know where I might acquire another one until I do an upgrade? 


---
*Imported from [Google+](https://plus.google.com/116234528579533617312/posts/XumkFqY3hke) &mdash; content and formatting may not be reliable*
