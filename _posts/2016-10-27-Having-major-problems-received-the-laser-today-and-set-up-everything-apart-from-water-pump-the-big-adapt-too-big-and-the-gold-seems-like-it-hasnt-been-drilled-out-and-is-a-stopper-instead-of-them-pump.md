---
layout: post
title: "Having major problems received the laser today and set up everything apart from water pump the big adapt too big and the gold seems like it hasn't been drilled out and is a stopper instead of them pump"
date: October 27, 2016 19:55
category: "Discussion"
author: "Rob Turner"
---
Having major problems received the laser today and set up everything apart from water pump the big adapt too big and the gold seems like it hasn't been drilled out and is a stopper instead of them pump 

![images/89b63002668f12411b6ed235ac1dbd0c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/89b63002668f12411b6ed235ac1dbd0c.jpeg)



**"Rob Turner"**

---
---
**Ariel Yahni (UniKpty)** *October 27, 2016 20:11*

Top should go to the IN on the tube. The other is just where the pump takes the water and sends it to the other piece mentioned before


---
**Mike Grady** *October 27, 2016 20:13*

I had the exact same pump and brass fitting with mine, the fitting only screws down part of the way into the pump thread, what i done was got it in as far as i could and then seal the top with silicon then tested to see what the flow was like


---
**Mike Grady** *October 27, 2016 20:16*

sorry didnt notice brass fitting hadnt been drilled


---
**Rob Turner** *October 27, 2016 20:22*

I didn't notice brass part not drilled so set it all up brass attached to pipe and perfect only problem is that it isn't drilled out so no water can't pass threw the pipe and I don't even have drill to sort it out, I have contacted the selling but not holding my breath there 


---
**Jim Hatch** *October 27, 2016 20:46*

The brass fitting is a common one (at least here in the US) and runs only a couple of dollars at Home Depot. 



Or you could simply drill it out.



You'll get old waiting for the seller's resolution.


---
**Ashley M. Kirchner [Norym]** *October 27, 2016 20:47*

Quick run to Home Depot will get that sorted out ...


---
**Jim Hatch** *October 27, 2016 20:50*

**+Ashley M. Kirchner**​ quick run to the tool chest for a drill would too 😀


---
**Rob Turner** *October 27, 2016 21:35*

I'm thinking next best thing with demand pointers with get that out in no time at all 



[https://plus.google.com/photos/...](https://profiles.google.com/photos/114544098138525257938/albums/6346261400011549713/6346261397956518498)


---
**Jim Hatch** *October 27, 2016 22:41*

Yep. A Dremel is your friend 😀


---
**Scott Marshall** *October 28, 2016 08:14*

If you think that's a major problem, boy are you going to hate your alignment lesson....



Happy Lasering



Scott


---
**Rob Turner** *October 28, 2016 08:42*

Oh I am already prepared for that one but then I have watched so many youtube videos regarding that part I think I have that nailed down lol I won't jinx myself though 


---
**Scott Marshall** *October 28, 2016 09:13*

Ultimately, it's all good entertainment. I have as much fun tinkering  as I do using it. 



Little catches like the undrilled fitting just add an interesting twist. It wouldn't be so humorous if you connected it and damaged your tube though. So it was a good catch, spotting it right off. I've got a brand new one waiting to set up, and I wonder what awaits. It looks good from the outside.

    

I've seen a lot of machined products like that that were missing a feature. 



They're made on an automatic screw machine, and if a cutter breaks or something, it's often up to an operator watching 5 or 6 machines to catch it and find all the rejects in a 55 gallon drum of fittings.       



I once had an employee complain about a batch of self drilling screws he was using to assemble a sub-panel only to discover they had no threads. They would drill in and seem to run in, but would 'strip' as soon as they bottomed. He was just moving right along and never noticed the missing threads. By the time he came to me he had run about 2 dozen of them into the heavy steel subpanel. "These keep stripping out" he said, handing me one just as he saw the problem. We had to remove the subpanel and drive them out from the back.   



Scott


---
**Rob Turner** *October 28, 2016 12:25*

Oh not the next catch is loading this blasted machine in windows 10 the drivers only have xp ect and windows 8 nothing for upgraded drivers or Windows anyone got a clue 


---
**Jim Hatch** *October 28, 2016 14:27*

Install for Win8. It works fine on Win 10. Bunch of us here doing that. Petty standard practice to load the latest ones available and see how it works. 



You're going to have issues with Corel Draw and CorelLaser. Draw is a pirated version of the software - some folks have a copy with a virus. There's a link in the group to a clean upload. That's Corel 12 but the CorelLaser plugin works all the way up to Corel X8. You'll also complain that the CorelLaser plugin icons to send stuff to the laser disappear and you have to restart it after every job. That's not the case 🙂 the icons disappear from the Corel toolbar but they're in the system status bar in the lower right corner of Windows. Everyone's been there.



It's worth searching the group for newbie questions because you're going to go through a bunch - it's not a plug & play & use in the garage machine. It takes some fiddling. But it doesn't cost $6,000 either. All the answers to your questions are out there already 😀 We all went thru the same issues.


---
**Rob Turner** *October 29, 2016 14:57*

Well I tried it on my older windows 8 laptop and still was saying couldn't install then all of a sudden this appeared 

[https://plus.google.com/photos/...](https://profiles.google.com/photos/114544098138525257938/albums/6346901011312314337/6346901010038718226)


---
*Imported from [Google+](https://plus.google.com/114544098138525257938/posts/SYhkqfzvWbm) &mdash; content and formatting may not be reliable*
