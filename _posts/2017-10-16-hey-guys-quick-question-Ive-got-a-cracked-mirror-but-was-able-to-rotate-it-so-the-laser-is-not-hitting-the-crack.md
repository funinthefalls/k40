---
layout: post
title: "hey guys, quick question, I've got a cracked mirror, but was able to rotate it so the laser is not hitting the crack"
date: October 16, 2017 06:00
category: "Hardware and Laser settings"
author: "Brendon Todd"
---
hey guys, quick question, I've got a cracked mirror, but was able to rotate it so the laser is not hitting the crack.



which type of mirror is better to get? Si or Mo mirrors? i can get a set of 3 off ebay, but not sure which are better.





**"Brendon Todd"**

---
---
**Ned Hill** *October 16, 2017 14:25*

I would say Mo as they tend to be more robust.


---
**Arjen Jonkhart** *October 21, 2017 02:28*

Anything is better than K19 :). Si is slightly more reflective but as mentioned before, doesn't last as long.


---
*Imported from [Google+](https://plus.google.com/104627062651775475102/posts/17TAtnvtXVo) &mdash; content and formatting may not be reliable*
