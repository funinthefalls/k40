---
layout: post
title: "Anyone else have an extra long end cap on their Y rod?"
date: October 20, 2016 00:11
category: "Discussion"
author: "Bill Keeter"
---
Anyone else have an extra long end cap on their Y rod? This one part is stopping me from going an extra inch. Grrr..  Is it easy to cut or replace?

![images/9fb9ee9675435d33c4b8390688aab07a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9fb9ee9675435d33c4b8390688aab07a.jpeg)



**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 20, 2016 00:22*

The clear rubbery thing. Yep I cut mine. Wasn't easy. I left some of it on but I think I gained some 4-5cm of Y travel. 


---
**Bill Keeter** *October 20, 2016 00:44*

It's the metal piece at the end of the rod. Bottom left of the pic. I'm assuming it's holding the rod in place. It seems to longer on mine versus other pics I've seen b


---
**Alex Krause** *October 20, 2016 01:16*

That vinyl tubing is a hard stop to keep the mirror assembly from crashing into the lid in the event you over travel or runaway situation... if you have a copper tubing cutter you can trim back a bit of it pretty easily 


---
**Bill Keeter** *October 20, 2016 01:37*

**+Alex Krause** yeah that's what I figured it was for. So it's just vinyl? This whole time I thought it was metal. Okay. So much easier to mod then. 



Currently this is as far as it will travel. So there's still room available. ![images/072a511cf80747831d8187f46d6bd29a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/072a511cf80747831d8187f46d6bd29a.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 20, 2016 01:46*

Yeah I scored that with a utility knife to about halfway down and then snipped around it. 


---
**Alex Krause** *October 20, 2016 02:17*

**+Bill Keeter**​ is this a Full spectrum laser?


---
**Bill Keeter** *October 20, 2016 02:39*

**+Alex Krause** It's one of the generic K40s. Just with white walls and red top doors.


---
**Alex Krause** *October 20, 2016 02:40*

Anything the red and white has that the blue and white doesn't come with?


---
**Bill Keeter** *October 20, 2016 03:50*

Added another 40mm !!![images/2e895da5bb3ee6ea5beb858a37accde9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2e895da5bb3ee6ea5beb858a37accde9.jpeg)


---
**Bill Keeter** *October 20, 2016 03:52*

**+Alex Krause** I think it's the same as the blue. Haven't seen any major differences but I'm new to the K40. So I could have missed something obvious.


---
**Bill Keeter** *October 20, 2016 03:54*

**+Alex Krause** ![images/db4dc7723467f3235f2e83bc4f286f19.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db4dc7723467f3235f2e83bc4f286f19.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 20, 2016 03:54*

Looks cleaner.  Did you gut the bed plate out? Can we get another pic under the hood (electronics cavity on the right)?


---
**Bill Keeter** *October 20, 2016 04:01*

![images/ab53fefaa327c5df882aa2233f7fd7e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab53fefaa327c5df882aa2233f7fd7e0.jpeg)


---
**Bill Keeter** *October 20, 2016 04:01*

![images/1bc9bcb0866413141b38dbbdd02d53ca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bc9bcb0866413141b38dbbdd02d53ca.jpeg)


---
**Bill Keeter** *October 20, 2016 04:02*

![images/d9ac81401a565d77889a1e5b2c9396ee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d9ac81401a565d77889a1e5b2c9396ee.jpeg)


---
**Bill Keeter** *October 20, 2016 04:06*

It also has a led light strip added in the back for the bed. 



**+Alex Krause**  yeah I removed the bed plate. It was just attached to the frame with 5 long spacers. 2 of which should still be in the photos. Next few days I'm adding a honeycomb bed. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 20, 2016 04:50*

I have what looks to be similar, but made of white plastic. More so on the end of the rod closer to the laser tube, so it prevents me going back an extra inch or so that I would like.


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/PGn5a7xqXss) &mdash; content and formatting may not be reliable*
