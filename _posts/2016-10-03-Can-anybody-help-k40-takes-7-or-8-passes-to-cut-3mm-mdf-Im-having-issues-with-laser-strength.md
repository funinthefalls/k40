---
layout: post
title: "Can anybody help k40 takes 7 or 8 passes to cut 3mm mdf I'm having issues with laser strength"
date: October 03, 2016 17:24
category: "Hardware and Laser settings"
author: "John Finnegan"
---
Can anybody help k40 takes 7 or 8 passes to cut 3mm mdf I'm having issues with laser strength 





**"John Finnegan"**

---
---
**Jim Hatch** *October 03, 2016 17:37*

Depends if it's a new problem or has always been that way. This is usually a problem with alignment or focus. Check those first before worrying about hardware (like a failing tube). Also check mirrors and lens for cleanliness. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 03, 2016 17:47*

Also, what kind of speed/power are you using? I used about 6mA, 20mm/s & can cut through 3mm ply in 2 passes. Can't recall for MDF, but it was similar.


---
**Ariel Yahni (UniKpty)** *October 03, 2016 17:51*

If it by bad alignment try this guide [https://plus.google.com/+ArielYahni/posts/ho5JAUMgTmE](https://plus.google.com/+ArielYahni/posts/ho5JAUMgTmE)


---
**Ashley M. Kirchner [Norym]** *October 03, 2016 18:18*

**+Yuusuf Sallahuddin**, with air assist, I do 8mA @ 10mm/s for a single pass. Less power if I'm cutting something like Aspen as there's no glue to cut through.


---
**John Finnegan** *October 03, 2016 19:04*

Thanks guys I was right up to 20mA and aroun 8mm/s I tried maybe 6 passes and still didn't go fully through I realigned today and cleaned lenses but still no luck I'm going crazy 


---
**Jim Hatch** *October 03, 2016 19:12*

Is the lens right side up and focus correct?


---
**John Finnegan** *October 03, 2016 19:30*

I try that Jim not sure how to focus do you mean align the mirrors 


---
**Ashley M. Kirchner [Norym]** *October 03, 2016 19:43*

20mA is too high for a K40, you're over driving it and shortening its lifespan. When you look at the lens while it's in the screw ring, you should be able to see your face reflected back. Or, if you can tell the difference, the curved side goes up, flat down. As for focus, by default, all these machines come with a 50.8mm focal length, meaning the bottom of the lens to the material should be that distance.


---
**Jim Hatch** *October 03, 2016 19:49*

What **+Ashley M. Kirchner** said - make sure the bump is up for the lens and then the distance between the bottom of the lens to the top of the material is 50.8mm. You can get a bit more performance by making it 49.3mm so you're focused midway through the 3mm material but it's not really necessary to get good cuts with lower power than you're using now.


---
**John Finnegan** *October 03, 2016 19:49*

**+Ashley M. Kirchner** thanks for your help and  for the advice I really didn't want to use the k40 at 20mA but it was the only way to get the cut done il adjust everything tomorrow hopefully that works 


---
**Ashley M. Kirchner [Norym]** *October 03, 2016 19:51*

Another test you can do is called the ramp test. Put a scrap piece of board at an angle, whether front to back, or left to right, doesn't matter. Then run a cut mark in the same direction. You'll notice it starting with a wide burn, gradually get thinner, then go wide again. Where the line is thinnest, that's your focal plane. This YouTube video shows one method. Notice how the line gets thinner towards the end. 
{% include youtubePlayer.html id="o4ccNRMTpGQ" %}
[https://www.youtube.com/watch?v=o4ccNRMTpGQ](https://www.youtube.com/watch?v=o4ccNRMTpGQ) Where the line is thinnest, that's the focal plane.


---
**Ashley M. Kirchner [Norym]** *October 03, 2016 19:54*

It's also worth checking whether both the bed itself is level, as well as the rails being level in relation to the bed. I used to have a hard time cutting things in the lower right corner till I paid closer attention and noticed that the bed was lower in that corner, causing the laser beam to be out of focus and not cutting all the way through. Small details make a huge difference.


---
**John Finnegan** *October 04, 2016 08:35*

Thanks a million guys that worked brilliant I levelled everything out and

raised the bed thanks for the tips on the focus point it's working better

than ever that's the end of two of me going crazy


---
**Ashley M. Kirchner [Norym]** *October 04, 2016 17:36*

Awesome! Glad you sorted it out.


---
*Imported from [Google+](https://plus.google.com/108115662975653767680/posts/EkovwEQmRXg) &mdash; content and formatting may not be reliable*
