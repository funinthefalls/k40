---
layout: post
title: "Got the RAMPS 1.2 up and running with new pins.h definitions"
date: March 01, 2016 02:38
category: "Software"
author: "ThantiK"
---
Got the RAMPS 1.2 up and running with new pins.h definitions.  Also had to disable LCD support -- Is the LCD support in TurnKey Tyranny Marlin useful at all if you're using LaserWeb?  Chilipepper is kinda friggin' useful.  I've got a Raspberry Pi 3 on order that will be used for this machine. :D

![images/99e39b156e2d3153a356213ce62f9b24.png](https://gitlab.com/funinthefalls/k40/raw/master/images/99e39b156e2d3153a356213ce62f9b24.png)



**"ThantiK"**

---
---
**Anthony Bolgar** *March 01, 2016 04:27*

I still like having the 12864 LCD with rotary encoder so I can print from SD cards, as well as being able to modify speed on the fly using the LCD control panel.


---
**ThantiK** *March 01, 2016 04:44*

**+Anthony Bolgar** are those basically the only things the panel gives you control over?  Cause there's no need for bed temp, no need for hot end temp, etc.


---
**ThantiK** *March 01, 2016 04:52*

Honestly, 250000 should be the default communication speed on Serial.  I'm pretty sure most Linux distros now have the serial fix in place.  That, plus the 256 byte buffer increase should help on the raster side, right?



Smoothie use native USB for communication or is it a simulated serial device?



Also; RAMPS 1.2 -- I don't exactly have the ability to worry about SD printing. :P


---
**ThantiK** *March 01, 2016 05:46*

**+Peter van der Walt**, at least let me pay for the materials cost.  Even if it's at-cost, I can't guarantee I'll have time to work on it quickly.



If I recall correctly, Brook Drumm had to purchase a $20k USB id from the USB consortium, and I believe he's offered some of the sub-IDs up for the reprap community free of charge.  I'm going to talk with Arthur Wolf about this, see if it's worth getting high speed usb 2.0 communication running with a legit ID and maybe some kernel patches or a kernel module.  This whole virtual serial thing is something that smoothie is in a position to eliminate if we can get the right minds on it.  If we can manage it, everyone benefits.  And 12mbps communication speeds would certainly give a lot of headroom, no?


---
**Anthony Bolgar** *March 01, 2016 13:07*

I am interested in a smoothiebrainz/k40 daughterboard combo. I can send you some $$ via paypal to pay for the board costs.


---
**Brandon Satterfield** *March 01, 2016 14:09*

Got a smoothie board and K40 sitting waiting to be hacked and laserweb'd. Finding time is killing me. Be happy to assist in testing soon too. Still cleaning up files and cutting pieces for opencncza, but the K40 is next in line. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/75rk3DVjht6) &mdash; content and formatting may not be reliable*
