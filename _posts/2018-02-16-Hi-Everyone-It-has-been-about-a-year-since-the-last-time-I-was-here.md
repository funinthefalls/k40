---
layout: post
title: "Hi Everyone. It has been about a year since the last time I was here"
date: February 16, 2018 03:08
category: "Discussion"
author: "Christopher Elmhorst"
---
Hi Everyone. It has been about a year since the last time I was here. I had to box up my laser and move. I'm un-boxing it this weekend, building a table, and making a Z-Axis for the laser.



I no longer have my computer that I used when I first got the laser so I lost all the software that I had downloaded. Anyone know where to download the software that a gentleman in here had developed himself? I really liked it and I can not think of the name of it...



Thanks in advance. 





**"Christopher Elmhorst"**

---
---
**HalfNormal** *February 16, 2018 03:31*

k40 whisperer


---
**James Rivera** *February 16, 2018 06:13*

**+Christopher Elmhorst** This is excellent software!  I haven't had to upgrade my K40 yet thanks to this.  I already have a #Cohesion3D board I intend to use, but for now, this works for me.



[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
**Duncan Caine** *February 16, 2018 14:46*

Inkscape and K40 Whisperer, both free and both excellent.


---
**Anthony Bolgar** *February 18, 2018 03:02*

LaserWeb is probably the software you were using before. It is good, as is K40 Whisperer. Choose the one you like the best and start burning things!!




---
**James Rivera** *February 18, 2018 19:16*

**+Anthony Bolgar** does LaserWeb work with the default board that comes with the K40? I thought the answer was no.


---
**Anthony Bolgar** *February 18, 2018 20:31*

Sorry, I forgot that it did not work with the stock board. Everyone I know upgraded to a C3D Mini or Smoothie board. Thanks for the catch. 


---
*Imported from [Google+](https://plus.google.com/+ChristopherElmhorst/posts/EnwX3n1SvCe) &mdash; content and formatting may not be reliable*
