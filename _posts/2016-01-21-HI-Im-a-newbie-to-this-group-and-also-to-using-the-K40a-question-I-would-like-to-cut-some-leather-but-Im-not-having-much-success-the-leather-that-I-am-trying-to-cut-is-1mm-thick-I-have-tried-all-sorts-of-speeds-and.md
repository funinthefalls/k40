---
layout: post
title: "HI, I'm a newbie to this group and also to using the K40,a question , I would like to cut some leather, but I'm not having much success, the leather that I am trying to cut is 1mm thick, I have tried all sorts of speeds and"
date: January 21, 2016 20:43
category: "Discussion"
author: "Norman Kirby"
---
HI, I'm a newbie to this group and also to using the K40,a question , I would like to cut some leather, but I'm not having much success, the leather that I am trying to cut is 1mm thick, I have tried all sorts of speeds and power settings,  my K40 is just as it comes, no mods, can anyone help or am I being to ambitious in wanting to cut leather 





**"Norman Kirby"**

---
---
**Anthony Bolgar** *January 21, 2016 22:19*

First thing to check is that the mirrors are aligned correctly. Then check the lens to make sure it is installed the right way up. Also, make sure that the item you are trying to cut is the correct distance away from the lens (50.8mm or 2")

 If everything is set up properly you shgould have no issues cutting 1mm thick leather.


---
**Jim Hatch** *January 22, 2016 00:05*

Can you cut other materials? 1mm leather shouldn't be a problem at all. How does it do with 1/8" plywood, cardboard or acrylic? If you're not able to cut it points to a lack of laser power and Anthony's spot on with the likely culprits - defocused either because of distance from the material, lens being upside down or alignment being poor. I'd look at them in that order assuming you've been able to do any kind of engraving already (which indicates that at least you're getting some power to the material).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2016 03:52*

Hi Norman. I regularly cut leather (pretty much all I do) with my K40. Mine is stock & I had major issues getting it to cut to begin with. As Anthony says, my issue was a combination of poor mirror alignment, lens being upside down when it was sent to me & also the distance from lens to workpiece was out by a few mm (because I removed the standard cutting area & replaced). For reference, 1mm leather should cut like a hot knife through butter with 7mA power @ 15mm/s. Also, you may find charring along the edges unless you install some form of air-assist to blow out any fire that starts (happens regularly with leather). I just use an air-pump focused through a ball-inflation needle to target the area right where the laser cuts. Once you get these things sorted it should work a charm.


---
**Norman Kirby** *January 22, 2016 08:10*

thanks for the replies, I'll look at the mirrors and the table height as suggested, heres hoping




---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2016 08:43*

**+Norman Kirby** Keep us posted on your progress & we can maybe offer other solutions if something is still amiss.


---
**Norman Kirby** *January 22, 2016 18:57*

well after a good few hours on the K40, starting with mirror at the back that was half black(which is now clean) reseated it and aligned it to the laser, then the next mirror that took some lining up but eventually got it right, then the lense fortunately that was in the right way and i got that lined up, table height set as suggested 50.8mm, then it was with trepadition that i then put some leather in place, pressed cut and bingo cut it no bother, but i will need to invest in a air assist though, once again, 

thanks guys for the advice


---
**Jim Hatch** *January 22, 2016 19:00*

**+Norman Kirby** Great news. Until you get the air assist try to clean the lens & mirrors frequently. I would with every time I used it. When the smoke from wood or leather isn't blown away (or sucked out of the back) it leaves deposits on the lens & mirrors. Someone here (I think) posted test results showing only 10 minutes of smoky cutting would cut the power delivered to his material by more than 20%. Ever since I read that I have cleaned my machine for every use - and sometimes I would do it in the middle of a long cut.


---
**Joseph Midjette Sr** *January 23, 2016 11:57*

Mirror alignment seems pretty straight forward and common sense. Does anyone have a good guide for cutting depths, head distance, recommended power levels for specific materials, etc. etc.? 


---
*Imported from [Google+](https://plus.google.com/103798744920995011573/posts/X4adDTXuDzF) &mdash; content and formatting may not be reliable*
