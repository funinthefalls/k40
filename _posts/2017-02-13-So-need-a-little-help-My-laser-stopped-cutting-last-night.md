---
layout: post
title: "So need a little help. My laser stopped cutting last night"
date: February 13, 2017 12:57
category: "Discussion"
author: "3D Laser"
---
So need a little help.  My laser stopped cutting last night.  It was a sudden stop with no fade in power.  I am thinking my power supply is blown but wanted to get people's opinions 





**"3D Laser"**

---
---
**Don Kleinschnitz Jr.** *February 13, 2017 13:32*

Is "My laser stopped cutting...." referring to the entire machine or the laser itself?


---
**3D Laser** *February 13, 2017 13:52*

**+Don Kleinschnitz**  just the laser the rails still move 


---
**Don Kleinschnitz Jr.** *February 13, 2017 14:25*

**+Corey Budwine** OK 

Please precede your answers using the numbers of the questions below.



Post a picture of your LPS showing the input connectors



..... @ the panel

1.0 is the power switch on? Had to ask :)

1.1 Does the laser fire when you push the panels Test Switch with the Laser Switch engaged?

1.2 Do you have interlocks installed?

1.2.1 Are the interlocks closed? If yes how do you know.



..... @ the LPS

2.0 Does the laser fire with the test button that is on the power supply?

2.1 Is the LED on the power supply illuminated?



..... measured @ the LPS

3.0 does the laser fire if you ground "L", with interlocks closed and Laser Switch engaged?

3.1 What does the 24VDC on the LPS measure with voltmeter

3.2 What does the 5VDC on the LPS measure with a voltmeter



I will have other measurements when I see the picture of the LPS.


---
**Kostas Filosofou** *February 13, 2017 14:34*

..An then look also this post [plus.google.com - Any idea why this has happened ??](https://plus.google.com/+KonstantinosFilosofou/posts/9AF1i2hZjU2)


---
**3D Laser** *February 13, 2017 14:37*

Power and laser button on

Does not fire when I press test

No innerlocks

Does not fire when I press test on the laser supply

Unsure about the led at work at the moment 



Do not have a way to measure the power supply 


---
**Don Kleinschnitz Jr.** *February 13, 2017 15:03*

**+Corey Budwine** ok pls check the led when you get home.



Please answer these questions by referencing the question #'s in my post. Makes it much less confusing. 



It looks like the supply is bad but without measuring voltages cannot be sure.



Next step is to get cover off the supply and look for charred parts.


---
**Paul de Groot** *February 14, 2017 02:08*

**+Don Kleinschnitz**​ it seems there is market for replacement psu's. Might be a good opportunity for you to redesign the existing psu with some safety features like current/short/temp protection 


---
**Don Kleinschnitz Jr.** *February 15, 2017 00:12*

**+Paul de Groot** that might be viable once we find out exactly why these fail. I have recently been studying gas discharge in lasers and that characteristic makes LPS a bit different than a normal switcher.


---
**Paul de Groot** *February 15, 2017 01:07*

**+Don Kleinschnitz** cool let me know what you find out and happy to help you 


---
**3D Laser** *February 15, 2017 01:39*

**+Paul de Groot** it was the power supply. I forgot I had a spear I hooked it up real quick and the laser fired.  Now I just have to mount it and re align the laser


---
**Don Kleinschnitz Jr.** *February 15, 2017 01:57*

**+Corey Budwine** I am researching the cause of LPS failures, so if you want to donate it for failure analysis that will help the cause. 


---
**3D Laser** *February 15, 2017 03:10*

**+Don Kleinschnitz** sure why just cc me on the results 


---
**Madyn3D CNC, LLC** *February 20, 2017 20:52*

**+Don Kleinschnitz** gas discharge? I'll have to keep an eye on the blog for future publications on that. 



Here's Don's blog for anyone that is not aware. IMO some of the best engineering/research info on K40's currently on the web.  [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/)


---
**Don Kleinschnitz Jr.** *February 20, 2017 23:15*

**+Madyn3D CNC, LLC** thanks for the plug :)

See appendix C: 



[http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html](http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/LfYdU3WTREF) &mdash; content and formatting may not be reliable*
