---
layout: post
title: "GETTING STARTED?..........NEED HELP? IF YOU ARE NEW TO THIS COMMUNITY, G+, AND/OR THE K40 ..."
date: January 28, 2018 14:00
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
GETTING STARTED?..........NEED HELP?



IF YOU ARE NEW TO THIS COMMUNITY, G+, AND/OR THE K40 ...



BEFORE YOU START ASKING QUESTIONS, PLEASE READ ALL THE LINKS BELOW.



Using these HINTS enables the community to help YOU to a solution faster!



Click on the links below for more information



#K40ReadTheseFirst

#K40GettingStarted

#K40FollowingSomeone

#K40UsingHangouts

#K40Searching

#K40FormatingText

#K40PersonalMessage

#K40GettingHelp

#K40SharingInformation

#K40MediaSharing







**"Don Kleinschnitz Jr."**

---
---
**Dominic Notaro** *March 26, 2018 05:31*

I have a K40 and I opened up the engraving cutting area to 10" x 8", added a cohesion3D mini board, made my own X adjustable bed, and made a new control panel face. By increasing the cutting area found out the intensity of the laser varies quite a bit. Came to the forum and was directed to a paper on how to align the mirrors. It was a very helpful guide on mirror alignment While doing the mirror alignment cracked 2 of the mirrors "1 & "2. Bought the moly mirrors to replace the mirrors that came with the K40. Installed the new mirrors and continued with the alignment. I did a lot of shimming and elongation of mounting holes for the gantry. I spent a better part of a day aligning; tightening bolts, making shims, untightening bolts and making new shims. It's not perfect but it's better than it was. My next project is building a water chiller.

![images/fc69710ac5e6763e3cabad9771244984.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fc69710ac5e6763e3cabad9771244984.jpeg)


---
**rich lee** *May 22, 2018 12:55*

My 1 yr old K40 was working fine... until the desktop died.

NOW ... I re-installed software on laptop , and XY are reversed. I THOUGHT I had good knowledge of the setup , but apparently NOT !

I re-entered the ID and the BOARD # ..... but COREL-LASER just will not change the XY.



Any ideas ?



Rich


---
**Jay Solis** *November 06, 2018 04:34*

do you have any charts that have power settings for different materials that i can engrave?


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/BLPdnUPo4ds) &mdash; content and formatting may not be reliable*
