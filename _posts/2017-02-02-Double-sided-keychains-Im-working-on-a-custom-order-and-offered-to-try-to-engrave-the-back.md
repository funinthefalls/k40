---
layout: post
title: "Double sided keychains. I'm working on a custom order and offered to try to engrave the back"
date: February 02, 2017 17:14
category: "Object produced with laser"
author: "Jeff Johnson"
---
Double sided keychains.

I'm working on a custom order and offered to try to engrave the back. Well, as you can see, it worked out perfectly. I cut 15 keychains on a 12" x 6" x 1/8" piece of cherry. I'll be finishing the order this weekend (making 30 more) and I'll make a video showing how I was able to line up the engraving on the back. The keychains are about 2.25" tall so the text has to be pretty well lined up to look good. It was easier to do than I thought.

![images/45488d4dd61aea98a2709acb16382533.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/45488d4dd61aea98a2709acb16382533.jpeg)



**"Jeff Johnson"**

---
---
**Ned Hill** *February 02, 2017 17:31*

Nice work.  




---
**Jim Hatch** *February 02, 2017 18:15*

Nice. But is the 1/8" wood thick enough to prevent the scrolled "tails" from breaking? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 03, 2017 01:24*

Looks good. A thought on Jim's mention of the tails possibly breaking, you could fillet the corners a little (like with 3D printing) to reinforce the corners attaching the tail. So instead of sharp corners, they would be rounded out with a small radius (adding extra resistance to breaking at that point).


---
**Austin Baker** *February 03, 2017 08:13*

Definitely interested in how you lined up the reverse engraving!


---
**Jeff Johnson** *February 04, 2017 19:09*

**+Yuusuf Sallahuddin** I bent and twisted them a little and while they didn't break I wasn't happy with how they felt.  I redid them in 5mm plywood and they are very sturdy now. They're not as pretty as the cherry wood but they're durable. Thanks for bringing this up.

![images/5ffdf3a4282e99cb67379b294db29247.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5ffdf3a4282e99cb67379b294db29247.jpeg)


---
**Ned Hill** *February 04, 2017 21:31*

You sometimes have to consider grain direction for stability on some designs especially with thin stock.  So in this case you could try the grain going the other direction which would strengthen the tail.  However that would weaken the top loop so it's a trade off sometimes.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 04, 2017 21:33*

**+Jeff Johnson** You're right that the 5mm plywood versions don't look as pretty as the cherry wood. You could maybe consider doing something similar with stain (for non-engrave areas)/black (for engrave areas) like **+Ned Hill** has recently done on some wonderful pieces.


---
**Ned Hill** *February 04, 2017 21:36*

Thanks **+Yuusuf Sallahuddin** but I actually got the idea for the stain from something that Jeff had done earlier. ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 04, 2017 21:42*

**+Ned Hill** Haha, well I guess the circle comes around again.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/Usn1YgRMP9H) &mdash; content and formatting may not be reliable*
