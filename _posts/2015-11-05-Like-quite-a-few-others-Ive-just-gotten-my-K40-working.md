---
layout: post
title: "Like quite a few others I've just gotten my K40 working"
date: November 05, 2015 09:51
category: "Discussion"
author: "Anthony Coafield"
---
Like quite a few others I've just gotten my K40 working. I'd love to do some photo engraving onto wood (Christmas is coming up and all, easy gifts for grandparents!) but not too sure how to start. Can anyone point me toward the way to go about it please? 



Thanks to this great community I've been able to get the thing working. It came with no software or manuals, just the usb key stick.





**"Anthony Coafield"**

---
---
**Kirk Yarina** *November 05, 2015 16:26*

Google is your friend :), there's a number of articles on how to prepare pictures for engraving.  Look for halftone and dithering, and you'll need to mentally filter out some "buy me" from otherwise useful articles (I'm frugal...).  Gimp and other free products will do what you need with possibly a bit of extra effort.  Plan on experimenting (I made a stack of 3 x 5 x 1/4" hardwood test blocks out of scrap), it's part of the fun.



On my phone or I'd give you some links.


---
**Stephane Buisson** *November 05, 2015 17:01*

as **+Kirk Yarina** said, YouTube is your friend too.

Inkscape is great, I can recommend you this channel:


{% include youtubePlayer.html id="playlist" %}
[https://www.youtube.com/playlist?list=PLGLfVvz_LVvTSi9bKrvGR2_DBg0Tv8Dxo](https://www.youtube.com/playlist?list=PLGLfVvz_LVvTSi9bKrvGR2_DBg0Tv8Dxo) **+Anthony Coafield** 


---
**Anthony Coafield** *November 05, 2015 22:11*

Thanks so much both of you. Google is my friend. I think my brain was fried after working all the rest out I couldn't even think where to start. A good sleep and all is much better! 


---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/9M3FWCo9NRW) &mdash; content and formatting may not be reliable*
