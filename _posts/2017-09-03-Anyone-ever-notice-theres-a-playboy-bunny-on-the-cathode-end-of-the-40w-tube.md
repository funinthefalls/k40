---
layout: post
title: "Anyone ever notice there's a playboy bunny on the cathode end of the 40w tube"
date: September 03, 2017 23:07
category: "Air Assist"
author: "Tony Sobczak"
---
Anyone ever notice there's a playboy bunny on the cathode end of the 40w tube. 

![images/f1c6e1832f254c3ef0d8fa1b0803b427.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1c6e1832f254c3ef0d8fa1b0803b427.jpeg)



**"Tony Sobczak"**

---
---
**HalfNormal** *September 03, 2017 23:58*

Looks more like someone holding up a peace sign with their fingers.


---
**Don Kleinschnitz Jr.** *September 04, 2017 00:44*

It's an upside down R. It's so they know to point this side to the Right. 


---
**Jeremie Francois** *September 04, 2017 06:33*

**+Don Kleinschnitz** bye bye dreamers, here comes an engineer! :D


---
**Adrian Godwin** *September 04, 2017 07:13*

But it's on the left. 

Is that why it's upside down ?




---
**Michael Audette** *September 05, 2017 13:02*

R= Rejected :D


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/2F5mBzVEhUL) &mdash; content and formatting may not be reliable*
