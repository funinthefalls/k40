---
layout: post
title: "Making some tokens and dice for a print-n-play of Tiny Epic Quest"
date: November 05, 2016 14:16
category: "Object produced with laser"
author: "Tev Kaber"
---
Making some tokens and dice for a print-n-play of Tiny Epic Quest. 



Etching at 6mA/400mm/s, cutting at 10mA/9mm/s. 



The color on the dice was added using DecoColor paint pens. 

![images/ecfcd7b0ab1c75ce255bc898e244c865.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ecfcd7b0ab1c75ce255bc898e244c865.jpeg)



**"Tev Kaber"**

---
---
**Anthony Bolgar** *November 05, 2016 15:57*

The dice look great.


---
**Timo Birnschein** *November 06, 2016 17:22*

I really like it as well!


---
**Tev Kaber** *November 07, 2016 02:23*

I hand-painted the tokens as well. ![images/7bc01486f7508609b0f3e62b2eebbdd6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7bc01486f7508609b0f3e62b2eebbdd6.jpeg)


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/QH8eWbh3ryE) &mdash; content and formatting may not be reliable*
