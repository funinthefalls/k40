---
layout: post
title: "since u laser cut for 3hours a day and can never re member to drain air compressors tank of water this will come in very handy"
date: April 15, 2016 06:23
category: "Modification"
author: "Phillip Conroy"
---
[http://www.ebay.com.au/itm/Automatic-Electronic-Timed-Air-Compressor-Tank-DRAIN-VALVE-220V-1-2-IND003-/181403801082?hash=item2a3c8255fa:m:mj5YazFM3htX6UsYSC31ddg](http://www.ebay.com.au/itm/Automatic-Electronic-Timed-Air-Compressor-Tank-DRAIN-VALVE-220V-1-2-IND003-/181403801082?hash=item2a3c8255fa:m:mj5YazFM3htX6UsYSC31ddg)



since u laser cut for 3hours a day and can never re member to drain air compressors tank of water this will come in very handy

![images/27587472e0ad9cc015169826c01e0452.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/27587472e0ad9cc015169826c01e0452.jpeg)



**"Phillip Conroy"**

---
---
**Phillip Conroy** *April 15, 2016 08:25*

This fits on bottom of tank and i will set it to only 1 second at very low flow rate,i will loose very little air


---
**Phillip Conroy** *April 15, 2016 08:27*

I posted this item as i had never seen anything like it before a d that it might be of intrest to other people,i may also get onefor my water traps as well


---
**Brent Crosby** *April 16, 2016 13:49*

Keeps the tank from rusting. The automatic ones like this work great. If you are not using the compressor for a week, just turn the compressor off and unplug valve--no waste. 


---
**Phillip Conroy** *April 16, 2016 22:38*

My air compressor is 30metters[90 feet] away and becayse it is not recomonded to run them on extension leads as it burns out the motor[startup current 5x running current =voltage drop that kills air compressors] i have got away with it on my 2hp compressor for 5 months howeer upgrading to a 3hp air compressor i am going to have to place a 240 volt relay at air compressor and have the compressor run of the relay an a 3meter heavey duty extension lead.

I will have the auto drain valve set for 30minutes delay and 0.5 seconds drain ,as i laser cut for 3 hours this will hopefully eep tank dry..........ps drain valve hs 5mm opening


---
**Anthony Bolgar** *April 17, 2016 23:15*

This is a great idea. I am going to add one to my compressor, as it is about 50 feet from the lasers. I am in the process of hard piping air from my main shop to the area of my house that I am using for all my toys. This way I have no noise issues as the compressor cycles on and off. I will have a manifold that will split it off to the 2 lasers as well as a general use air fitting, and a line going to my downdraft mini spray booth I use for airbrushing.


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/cW1L8eGjyAk) &mdash; content and formatting may not be reliable*
