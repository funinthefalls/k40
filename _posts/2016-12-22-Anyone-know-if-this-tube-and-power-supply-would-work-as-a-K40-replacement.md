---
layout: post
title: "Anyone know if this tube and power supply would work as a K40 replacement?"
date: December 22, 2016 00:50
category: "Modification"
author: "greg greene"
---
Anyone know if this tube and power supply would work as a K40 replacement?  It is rated at 68 W output, but I'm not sure if the power supply can be made to work with a PWM output as from a smoothieboard/clone



[http://www.ebay.com/itm/50W-CO2-Laser-Tube-Standard-125cm-Length-Power-Supply-Engraver-Cutting-/331898713436?hash=item4d46b4755c:g:2uoAAOSwOVpXdxkM](http://www.ebay.com/itm/50W-CO2-Laser-Tube-Standard-125cm-Length-Power-Supply-Engraver-Cutting-/331898713436?hash=item4d46b4755c:g:2uoAAOSwOVpXdxkM)





**"greg greene"**

---
---
**Ned Hill** *December 22, 2016 01:11*

I think you would need a tube case extender to accommodate the extra length.   Light Object sells one I believe.


---
**Don Kleinschnitz Jr.** *December 22, 2016 01:17*

Is a standard K40 tube about 28.5" = 72cm. This would require a big cabinet extension? 50 watt and above would require larger mounts?


---
**James Rivera** *December 22, 2016 04:20*

Looks like **+Ned Hill** had the right idea.  Check this out:



[lightobject.com - K40 Mini Laser Machine Tube Extension Case](http://www.lightobject.com/K40-Mini-Laser-Machine-Tube-Extension-Case-P960.aspx)


---
**Ned Hill** *December 22, 2016 05:15*

Yeah that light object extension will extend a K40 case up to 100cm which would fit a 50W tube but not a 60W.


---
**Stephane Buisson** *December 22, 2016 09:10*

a bit too big (125cm>100 extention), if you haven't already you will need to upgrade your optics too, the bills stack up but the gantry size. (compare 50w machine) is  it worth it ??? (personal choice)


---
**greg greene** *December 22, 2016 13:08*

That's the thing I guess, probably better to invest in a larger machine and get a larger workspace 



Thanks all


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/UhxuZX74vB8) &mdash; content and formatting may not be reliable*
