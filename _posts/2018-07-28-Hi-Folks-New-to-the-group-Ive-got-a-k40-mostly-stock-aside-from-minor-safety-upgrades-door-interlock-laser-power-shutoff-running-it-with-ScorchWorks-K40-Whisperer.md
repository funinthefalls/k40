---
layout: post
title: "Hi Folks. New to the group. I've got a k40, mostly stock aside from minor safety upgrades (door interlock, laser power shutoff), running it with ScorchWorks K40 Whisperer"
date: July 28, 2018 20:22
category: "Air Assist"
author: "Samuel Trost"
---
Hi Folks. New to the group. I've got a k40, mostly stock aside from minor safety upgrades (door interlock, laser power shutoff), running it with ScorchWorks K40 Whisperer. I recently installed the LightObject laser head with air assist, the recommended mirror and the 18 mm x 50 mm FL lens. After installation, the laser is performing very poorly compared to the stock head. It was even struggling to cut through printer paper after I had turned the power quite high. I did notice that the raster function seemed to perform more normally than the vector functions, but still weak.



My troubleshooting:

1. Tightened fasteners to make sure nothing was loose.

2. Adjusted the position of the head in the bracket to recenter. No real effect.

3. Ramp test. Focal point is approximately at the bed height.

4. Checked with air assist vs air assist nozzle removed. Air did reduce flaring and charring as expected, did not affect cut power.



I haven't yet put on the old head and tried that again. Any suggestions?







**"Samuel Trost"**

---
---
**Ned Hill** *July 28, 2018 21:27*

1. Did you do a full realignment of all your mirrors?

2. Make sure the lens is oriented correctly with the bump side up.

3. Make sure the laser is exiting the center of the air assist nozzle and not clipping the edge.  I check it with a piece of tape on the bottom of the nozzle and then use a small mirror to inspect the tape after the test fire.


---
**Samuel Trost** *July 28, 2018 21:30*

1. Didn't repeat alignment, I thought I had been pretty careful not to bump anything, and the output seems to be straight and aligned, just wimpy. But, I guess I better.

2. I did make sure the convex side was up, and the ramp test did show a tightly focused spot at about the right focal point.


---
**Don Kleinschnitz Jr.** *July 29, 2018 02:10*

Make sure the beam is centered in the head and travelling perpendicular to the bed. 

Do so by burning a spot on a material fixed to the bed. Move the bed up or down. Then mark another spot. If the spot moves position the beam is not perpendicular to the bed.


---
**Kelly Burns** *July 29, 2018 17:07*

After to you check items Don and Ned  mentioned, look at the Mirror and Lens in Laserhead.  Examine them close for Cracks.   I managed to crack a mirror when installing it in the head.  I cracked a lens in a different way, but still careless.  Both effected performance greatly.  Even when perfectly aligned.



As for alignment, you definitely have to make sure the beam is entering the holder and is hitting mirror as close to center as possi ble.  Rotating the head even a half of a degree can make a huge difference.  




---
**Wendel Swartz** *July 30, 2018 01:03*

Ned is most likely correct. Same thing happened to me with the same upgrade. Turns out the mirror above the lense was skewed and my beam wasn't in the center of the lense. Scotch tape on the tip of the nozzle will leave an impression that you can see and show how close to center you are.


---
**Samuel Trost** *August 08, 2018 02:20*

Sorry for the delay responding, I didn't have a chance to go work on it till tonight.

I went to do the alignment and put tape over mirror 1 without looking to closely. Test fired it, seemed ok. Took off the tape and noticed a line through the mirror. Guess I found the problem. Any ideas why this happened? Cuz it seems like it was working fine and then it wasn't. I still have the mirror I took out of the stock head, going to install that as mirror 1 now.

![images/a890c5bce335666cea7fce6b6bf8bd84.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a890c5bce335666cea7fce6b6bf8bd84.jpeg)


---
**Kelly Burns** *August 08, 2018 02:24*

I cracked mine by tightening it too much.  I have seen mirrors crack from heat build up as well.  They get dirty and then heat up.  


---
**Wendel Swartz** *August 08, 2018 02:27*

Debris heated up on the suface of the mirror. If it cant reflect the beam it has to absorb it. If this happens to your lense use rubber gloves and dont inhale the dust. It is highly toxic. I would go ahead and clean the mirrors and lense before firing again.


---
*Imported from [Google+](https://plus.google.com/108207677112289382841/posts/Urhp698ocjk) &mdash; content and formatting may not be reliable*
