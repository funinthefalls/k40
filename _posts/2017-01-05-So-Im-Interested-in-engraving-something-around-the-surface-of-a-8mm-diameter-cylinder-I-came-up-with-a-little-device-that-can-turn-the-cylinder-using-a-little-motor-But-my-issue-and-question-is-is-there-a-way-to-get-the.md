---
layout: post
title: "So I'm Interested in engraving something around the surface of a 8mm diameter cylinder, I came up with a little device that can turn the cylinder using a little motor, But my issue and question is, is there a way to get the"
date: January 05, 2017 18:31
category: "Hardware and Laser settings"
author: "Jesse Veltrop"
---
So I'm Interested in engraving something around the surface of a 8mm diameter cylinder, I came up with a little device that can turn the cylinder using a little motor, But my issue and question is, is there a way to get the laser to engrave all of a pattern while staying only on the X axis and not moving downward the Y axis as it engraves? since my turner spins the stock the laser needs to stay on the same Y level while enrgaving otherwise ill have to devise a contraption that both turns AND moves the stock. any bright ideas or settings I don't know of? any ideas appreciated, Thanks!





**"Jesse Veltrop"**

---
---
**Joe Alexander** *January 05, 2017 18:36*

normally you would swap the cable for the Y-axis with the rotary motor, or at least disable it by disconnecting


---
**Jesse Veltrop** *January 05, 2017 18:45*

how would I disconnect it? is there a certain wire I need to pull out and would you be able to tell me which one, or is disconnecting the Y axis a software setting?


---
**Jesse Veltrop** *January 05, 2017 19:20*

**+Jesse Veltrop** Upon further fiddling I found the belts for the Y axis but they arent exactly in a easy spot for quick removal, also on the stock mother board there seems to be two plugs one labeled X and one Y, the X plug is not connected to anything and if I remove the Y wire or switch it to the X plug(edit: I see now the X is controlled by the ribbon cable), the laser wont function, any ideas of how to disconnect the Y axis while preserving the X? I'm perplexed why removing the Y stepper wire causes the machine to stop working altogether, atleast with laserDRW


---
**Don Kleinschnitz Jr.** *January 05, 2017 21:44*

**+Don Kleinschnitz**  should be able to just remove the Y cable. The X drive goes through the white ribbon cable. How will you synchronize the rotation with the horizontal moves?


---
**Jesse Veltrop** *January 05, 2017 21:51*

experimentation I suppose, I hadn't given it too much thought as I wanted to make sure it worked first, but considering the motors change speed based on the power they receive, different sized gears will change the speed as well, also I could potentially change the engrave speed too if I need a very fine adjustment,  my vision is that with specific gear sets ill be able to select which gears I need for varying diameters.   *Ill try disconnecting it again, if it still doesn't work  maybe Ill post a video of me showing it working, unplugging it, then showing it not working


---
**Jesse Veltrop** *January 05, 2017 22:03*

**+Jesse Veltrop** I tried it again and whenever I remove the Y stepper wire from the board, when I try to engrave sometimes laserDRW says it sent the data and sometimes it says my machine has been disconnected, in both cases neither the laser head fires nor does the cutter head move at all, the only movement is sometimes when I try to engrave it will move all the way to the left on the x axis far past where I have my origin point set and thats all I can get it to do


---
**Don Kleinschnitz Jr.** *January 05, 2017 22:10*

No idea I am not running stock controller or software, perhaps the M2 is smarter than I thought and it knows the stepper is not plugged in. 

In any case you will have to control the rotary's rotation with the controller if you want any fidelity.

I would google and check out the FB community to see if anyone has added a rotary to a stock configuration. I think is it set up to do so as there are controls in the software.



Ah, look at the software and see if fiddling with the rotary setting, with the y disconnected, works :).


---
**Jesse Veltrop** *January 05, 2017 22:40*

There is a rotary setting in laserDRW? perhaps that is my issue. is it that little check box in the very bottom of the engrave screen? if not could you tell me where to find it? if that doesnt work than I suppose I should use this as an excuse to upgrade my board :)


---
**Don Kleinschnitz Jr.** *January 05, 2017 23:28*

**+Jesse Veltrop** sorry all my Laserdraw, dongle and M2 crap went into the trash. 

Before that I was planning to add a rotary and noticed related settings in the UI but never used it. 

I also thought that I could unplug the  Yaxis stepper and plug a rotary attachment stepper in place of it.


---
**Jesse Veltrop** *January 06, 2017 00:47*

I did get it too SORT OF work by checking the "rotary" box but all the settings are really vague and do strange things, I think my best bet is to get a new board and a REAL rotary attachment, thanks though


---
**Don Kleinschnitz Jr.** *January 06, 2017 01:28*

**+Jesse Veltrop** by new board do you mean a different controller?


---
**Jesse Veltrop** *January 06, 2017 15:07*

Im new to all of this so im not entirely sure, the whole smoothie scene looks very enticing plus theres others out there too I assume? I always assumed that they were new motherboards for it, are they just something to go in between the motherboard and the laptop as a middle man for better control?


---
**Don Kleinschnitz Jr.** *January 06, 2017 15:10*

look at Cohesion 3D **+Ray Kholodovsky**. A drop in controller.


---
**Jesse Veltrop** *January 06, 2017 15:46*

**+Don Kleinschnitz** Thanks, will do




---
*Imported from [Google+](https://plus.google.com/114760861883962577037/posts/BbmGtPaQK2e) &mdash; content and formatting may not be reliable*
