---
layout: post
title: "Constant Velocity Image Rastering I have been working very hard on creating a new firmware based on grbl that will provide Constant Velocity Image Rastering"
date: October 30, 2016 18:52
category: "Discussion"
author: "Nick Williams"
---
Constant Velocity Image Rastering



I have been working very hard on creating a new firmware based on grbl that will provide Constant Velocity Image Rastering. This is what the big manufacture such as Epilog and FullSpectrum use. 



The video shows in real time the generation of the calibration ruler used by the LaserWeb guys. The head is moving at 250 mm/sec with acceleration set to 500 mm/s^2. Notice the over shoot of the head on each pass, This allows the head to accelerate to a fixed velocity before pixels begin to burn.  This also allows me to have lower acceleration of 500 mm/sec^2. High rates of acceleration put mechanical stresses on both stepper motors and machine. The noticeable results of high extreme acceleration are machine chatter, position loss of the stepper and blown motors.  The entire image is rendered in just over 1 min. Also notice how smoothly the head moves in both directions. 



I have also included an image of a mountains seen highlighting the ability to do grey scale image rastering. notice the fade of the clouds in the upper right hand corner.



This was done using an Uno as the controller. Compare this 16 Mhz controller with the Smoothie coming in at 120 Mhz. Smoothie owners would need to be the judge as I do not have experience with that controller and I am not clear where its performance limitations are.



300 mm/sec is my limit after that things begin to fall apart. This is the max I can squeeze out of my 16 Mhz UNO controller, my plan is to move this to an arduino Due which is a 84 mhz processor. If every thing scale this would allow me to do image rastering at 1500 mm/sec, This is close to the upper limit of what an Epilog can do. The big question would be if I were able to achieve this kind of performance then how would our $400 K40 handle it???? :-)





![images/ab79b3e5168a9d5c4ff2847dc98b3791.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab79b3e5168a9d5c4ff2847dc98b3791.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-JLx_T50UL9E/WBZBc0tBfvI/AAAAAAAACww/wBpSLwg83wY5-Ux7tJta-3Ua-LsR014LQCJoC/s0/VID_20161027_112941.mp4.gif**
![images/ab45c64854db4647e5b0c404299967f5.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/ab45c64854db4647e5b0c404299967f5.gif)
![images/dc1233c110857ce0f5ec4a3278b60796.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc1233c110857ce0f5ec4a3278b60796.jpeg)

**"Nick Williams"**

---
---
**Ariel Yahni (UniKpty)** *October 30, 2016 19:10*

Very good result. 


---
**Paul de Groot** *October 31, 2016 05:47*

Looking very good 


---
**Nick Williams** *October 31, 2016 15:17*

Thanks Guys :-)


---
**Ariel Yahni (UniKpty)** *October 31, 2016 15:22*

**+Nick Williams** i have a spare uno with shield that id like to try, can you share your physical hookup and firmware/settings?


---
**Nick Williams** *October 31, 2016 16:41*

**+Ariel Yahni** just PM you let me know if you didn't get the message:-)


---
*Imported from [Google+](https://plus.google.com/107520953345127905250/posts/D5VQNWfCGmi) &mdash; content and formatting may not be reliable*
