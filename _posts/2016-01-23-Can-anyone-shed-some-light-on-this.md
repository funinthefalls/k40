---
layout: post
title: "Can anyone shed some light on this?"
date: January 23, 2016 01:08
category: "Smoothieboard Modification"
author: "Ben Alderson"
---
Can anyone shed some light on this? I posted a few days ago and got some very useful help, but still not quite there yet.



I've been banging my head off the wall with this one for about a week now.



Was given a K40 laser with dead motherboard back in October and ordered an X4 smoothieboard to go with it. After a few months of reading and researching I finally started the conversion at the start of January. All went well at first - I've completely rewired the steppers and and endstops using proper cables and cable tracks, and got it working and square with a Smoothieboard. Then came the laser control…



My PSU has the following pins for laser control:

K- K+ G IN 5V



After a lot of experimentation I've come to the conclusion that it's a dual pin setup - K+ needs to be pulled to fire the laser, and a potentiometer needs to be connected across G IN 5V to set power.



Apart from the crappy moshi board, the machine also had a control panel: [https://www.dropbox.com/s/uaapfn66y6o96ri/2016-01-22%2021.13.47.jpg?dl=0](https://www.dropbox.com/s/uaapfn66y6o96ri/2016-01-22%2021.13.47.jpg?dl=0)



This looks to be a digital potentiometer - the right hand side connector is to G IN 5V (and the K switches, of which only one was connected and it was only connected to the test-fire button and a pull resistor). The right hand connector is a serial connection, but was not in use. The IC is completely unmarked, but I believe it to be a digipot IC of some description.



Powering that control board up and pressing the buttons gives me a variable voltage coming out of its IN pin, 0 to 5V on a not-quite-linear scale (50% is 3v).



So far so good - I thought I could set the PWM pin on smoothie to use 5V via the MOSFET, sink it through a RC low pass filter to get an analogue voltage and Bob would be my auntie's live in lover. But sadly it was not to be.



Further investigation reveals that on the PSU side, the 3 pins labeled as G IN 5V actually have the following voltages on them: 0V 1.68V 5V. Now understandably when I connect pin 2.4 or 2.5 to another pin that's floating at 1.68 volts the Smoothie isn't very happy (the orange LED for the pin comes on even when no G1 move is being selected). I've tried connecting it in various ways, including through an RC low pass filter and with a diode in series to prevent the reverse voltage reaching the smoothie. So far nothing has worked.



Apologies for the long post, I've been bugging folk on IRC for the last week and reading and trying everything I can think of and I'm about ready to give up. There <b>must</b> be a way to make this work!



In my investigations I've tried the following:



PWM to K+ or K- = no laser fire

PWM to IN = no laser fire

PWM to IN and K+ pulled up = laser fire but no control over power level

K+ pulled up via a switch statement on pin 0.26 = laser fire but no control over power level

K+ pulled up via switch statement on pin 0.26 with the original digipot board = laser fire + manual control of power, but essentially useless as have to enable/disable laser with M3 or M5 (hoping to use Visicut which, as far as I can tell, doesn't support doing this, only supports setting power by S command per line, not turning laser on/off per line)





**"Ben Alderson"**

---
---
**Stephane Buisson** *January 23, 2016 09:21*

Look like some of my original problems with 4XC, I did fight ground loops for some time.

do you use a level shifter ? the pot / digipot is left as ceiling to protect the tube.


---
**Ben Alderson** *January 23, 2016 09:46*

Peter - I'm using a switch and got laser fire working fine (from the example you shared previously). I'll have a look at Laser Web as it's looking like I may have to consider switching tool chain. 


---
**Ben Alderson** *January 23, 2016 09:50*

**+Stephane Buisson**​ I thought grounding issue but all my ground points have continuity to the case and ground pin on the power cable.  I'm using a level converter on the laser fire switch, and I'm currently feeding 5v into the mosfet to get a 5v pwm out, but also tried that with a level converter.


---
**Ben Alderson** *January 23, 2016 14:22*

Maybe I need to put a pulldown resistor on the IN pin to ground it before sending my variable voltage to it?


---
**Ben Alderson** *January 23, 2016 18:14*

I've tried all three of those scenarios and each time I've got nothing to happen. Tried with and without level converters, with and without low pass filtering the pwm, and so on. There's a LAOS post somewhere that I just found that mentions pulling down the IN line, so it's all I've got left to try really! 



All components are sharing the same ground (psu and smoothie (psu is providing the 24v and 5v supply to smoothie as well as doing the laser)) and I've confirmed that they're all at the same potential with a meter. 



Pwm is giving me the correct output range, but as soon as I connect it to the IN line (which, with nothing connected, is floating at 1.7v) it stops working, hence why I'm now considering pulling it down with a resistor... 


---
**Ben Alderson** *January 23, 2016 19:09*

I just reread what I wrote, and I did indeed say that, which worried me a little so although I was sure that wasn't what I meant I've rechecked: all grounds, GNDs and Gs are at the same level, and /separately/ to that I have continuity between the earth pin, case and ac input to psu. Given how much time I've spent on this and how much sleep I've missed nothing would surprise me at this stage, but I'm happy to say Gnd and earth are both working well, and are separate things. 


---
**Ben Alderson** *January 23, 2016 19:10*

I haven't tried putting a straight 5v onto IN yet, but the processed 0- 5V from the digipot board does indeed vary the power as expected. 


---
**Ben Alderson** *January 23, 2016 23:57*

RESOLVED!



Solution for anyone looking (for this PSU only!)



PWM signal inverted from Smoothie  board, connected (negative side of it) to the IN pin on the PSU. The IN pin also has to be pulled up to 5V via a resistor (using 1.46k on mine, seems reasonable) to the 5V line next to IN.



Laser is trigged via K+ (inverted and pulled down on smoothie) and power set via the PWM with the S parameter.



Confirmed working with Visicut, also just installed Laserweb on my raspeberry pi media player so will experiment with that, too.


---
**Stephane Buisson** *January 24, 2016 00:01*

Glad you get it done, all you have to do is to enjoy !

;-))  Could you find and share a ref for your specific PSU, thx.


---
**Stephane Buisson** *January 24, 2016 08:53*

**+Peter van der Walt**  every readers here isn't electronic geek, by identifying /describing components it more easy to make a "howto", for readers to repeat and reproduce.


---
**Ben Alderson** *January 24, 2016 14:46*

**+Stephane Buisson** It's a JNMYDY PSU, no model number that I can see on the board. Here's the low votage side:

[https://www.dropbox.com/s/to6wntbxxy2uzrg/2016-01-22%2022.54.19.jpg?dl=0](https://www.dropbox.com/s/to6wntbxxy2uzrg/2016-01-22%2022.54.19.jpg?dl=0)


---
**Ben Alderson** *January 24, 2016 14:48*

**+Peter van der Walt** The leve shifter is on the laser trigger as the smoothie can only pull up to 3.3v. The pull up I added is on the PWM IN channel, no level shifter as I'm driving the mosfet  that powers that pin from a 5V supply line.


---
**Ben Alderson** *January 24, 2016 17:07*

Peter - I never called the mosfet a level shifter. Level shifter is on a separate line (the laser trigger). 


---
**Jonathan Peace** *January 25, 2016 20:02*

You do have a link between D+ and D- ?



Pulling K+ low on mine always fires the laser, and K- is just grounded.






---
*Imported from [Google+](https://plus.google.com/104155514534927678351/posts/8dHeqTQJghL) &mdash; content and formatting may not be reliable*
