---
layout: post
title: "upgraded from a k40 to a 500x700 Chinese laser purchased on ebay from a us seller power supply went out 3 weeks in but they were very cool in helping me with a replacement, very good support from them"
date: April 29, 2017 01:20
category: "Modification"
author: "A \u201calexxxhp\u201d P"
---
upgraded from a k40 to a 500x700 Chinese laser purchased on ebay from a us seller power supply went out 3 weeks in but they were very cool in helping me with a replacement, very good support from them. I purchased the replacement and they reimburse me the money right away as they did not have it in stock, So far love it made  some improvements like motorized z bed also auto focus. must say, can't leave my k40 behind since it started my laser journey, Made some improvements on the k40 as well both work perfectly. even mini camera on the nozzle to view the work in progress on screen.





**"A \u201calexxxhp\u201d P"**

---
---
**Joe Alexander** *April 29, 2017 01:44*

sounds awesome, post some images to share with us! I'm definitely intrigued by the nozzle cam!


---
**Mark Brown** *April 29, 2017 01:49*

I'm pretty impressed that they warrantied the power supply.


---
**A “alexxxhp” P** *April 29, 2017 01:49*

sure



![images/408be80f36f6f5a718f37ccfacffcabd](https://gitlab.com/funinthefalls/k40/raw/master/images/408be80f36f6f5a718f37ccfacffcabd)


---
**A “alexxxhp” P** *April 29, 2017 01:52*

thy told me a year warranty and 3 month for the tube 1st issue was solved without any excuses


---
**A “alexxxhp” P** *April 29, 2017 03:01*

camera is connected to the network so I can monitor it on my phone when I walk away from the machine.


---
**Ben Delarre** *April 29, 2017 07:18*

Alejandro which US eBay seller did you use?


---
**A “alexxxhp” P** *April 29, 2017 08:12*

[ebay.com - Details about  60W 110V CO2 Engraver Cutter Laser Engraving Machine w/ USB Interface New](http://www.ebay.com/itm/60W-110V-CO2-Engraver-Cutter-Laser-Engraving-Machine-w-USB-Interface-New/252796092487?_trksid=p2045573.c100033.m2042&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20131017132637%26meid%3D7f801c3e5bdf4a44bb049a178dd8da3a%26pid%3D100033%26rk%3D1%26rkt%3D8%26sd%3D252796092487)



very fast shipping took about 5 days to get it it was ready, I checked everything when I removed it from the crate everything was secured  mirrors were aligned doors are nice and secured no rattles it worked out of the box. 3 weeks later PS goes out. no hassles for replacement.


---
**Don Kleinschnitz Jr.** *April 29, 2017 10:55*

What camera did you use ?


---
**A “alexxxhp” P** *April 29, 2017 18:26*

mini spy cam purchased on amazon




---
**Adrian Godwin** *May 01, 2017 00:32*

Have you got a remote fire extinguisher control too ?!




---
**Don Kleinschnitz Jr.** *May 01, 2017 11:58*

**+Adrian Godwin** actually I was wondering if a PIR sensor in the cabinet could pull the interlock and shut to the laser down in fire situations. Probably would see any IR and stay on?


---
**Adrian Godwin** *May 01, 2017 15:22*

+Don Kleinschnitz  Yes, I think it would see too much - not only the laser itself but the heat from an ongoing cut. Same goes for a smoke sensor. 



It might be that there's something different about an uncontrolled fire vs. the laser cutting, but I'm not sure I want to start too many  fires to find out ! 




---
**A “alexxxhp” P** *May 02, 2017 01:27*

not a bad idea going to work on a fire detection mechanism for my laser 


---
*Imported from [Google+](https://plus.google.com/114764932222807234342/posts/ZpqQ8gr1atF) &mdash; content and formatting may not be reliable*
