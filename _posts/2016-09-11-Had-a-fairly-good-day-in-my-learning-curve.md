---
layout: post
title: "Had a fairly good day in my learning curve"
date: September 11, 2016 05:35
category: "Discussion"
author: "J.R. Sharp"
---
Had a fairly good day in my learning curve. Did not like my adjustable bed design (only cutting 3mm ply for now) so I went back to the old standoffs that came with it, I realized they were about 6mm off, so I made shims/pedestals to hold my mesh bed. I love it when a plan comes together. Played around with some simple designs, but I have to say I hate Corel with a passion.





**"J.R. Sharp"**

---
---
**Alex Krause** *September 11, 2016 06:39*

Upgrade to a smoothieboard and come join the LaserWeb fun :)


---
**J.R. Sharp** *September 11, 2016 17:05*

That is what I am considering. I am working on creating a business plan around building things. Engraving is only secondary. Would a smoothieboard work for this as well? Was tornw between that and one of the lightobject upgrade kits. 



Corel is decent, but kind of a pain in the ass to do things (especially joints) because some of the items I am looking to build are larger than the cut area...




---
*Imported from [Google+](https://plus.google.com/116586822526943304995/posts/TUGWiE7ujev) &mdash; content and formatting may not be reliable*
