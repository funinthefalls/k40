---
layout: post
title: "Made from 3mm alder and painted with fine tip paint pens"
date: April 16, 2017 14:13
category: "Object produced with laser"
author: "Ned Hill"
---
Made from 3mm alder and painted with fine tip paint pens.  The inner flower stamen are actually painted with brass calligraphy ink so they pop a bit more than apparent in the photo.  I love how the negative space of the engraving enhances the illusion of the flowers coming out through the cross

![images/0cf5fe00e9d70ee1994557adbf33a24d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0cf5fe00e9d70ee1994557adbf33a24d.jpeg)



**"Ned Hill"**

---
---
**Steve Anken** *April 16, 2017 14:31*

Yes, the illusions of depth are one of the "tricks" of artists. Lovely work.




---
**Don Kleinschnitz Jr.** *April 16, 2017 15:50*

You did it again, superb! Happy Easter!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/BHtY9bktzbc) &mdash; content and formatting may not be reliable*
