---
layout: post
title: "So I have taken laser apart, cleaned it, Cut the exhaust down and in the process building a new bed for it"
date: February 16, 2016 02:38
category: "Discussion"
author: "3D Laser"
---
So I have taken laser apart, cleaned it, Cut the exhaust down and in the process building a new bed for it. Now how do I make sure it is all square when I put it back together?





**"3D Laser"**

---
---
**Scott Marshall** *February 16, 2016 04:10*

I just used a good quality square to get the aluminum frame squared up. the rest is measure and adjust, the sheet metal is out a mile, is constantly oil canning, and   (fortunately) only effects cutting depth (focus).



Hint:

The trick on the belts is to lock the length, back off the adjustments, then jump the whole affair on the pulleys and tension. It's not too bad of a job like that (don't open the belt loop unless you have to, and mark the length if you do), but a real pia trying to connect it while on the pulleys. near impossible I'd say.



Once you get it back together shim the standooffs as required to get the 4 corners of whatever you use for the cutting deck the same distance from the head. I originally replaced my standoffs with 10-32 threaded rod. They you can adjust with a double nut setup.

The factory cutting deck is a waste of time and sheetmetal - a piece of hardware cloth or such ought to do it.

I've been using a dozen "Great Bear" plastic disposible cups. They were just the right height, and I knew I'd be upgrading soon. They got me by just fine for quite a bit of plywood cutting and engraving (although the bears are now showing signs of severe laser attack from above).



Height of the high teck Great Bears is 2.723" - around 2 3/4". I have no idea where I got them, been using them to mix epoxy for a while now...



I just got done mounting a tight piece of 1/8 plywood in the aluminum frame (used thick CA to hold it securely but still easy to remove) and drew the largest square I could get it to run without crashing (mine is 211mm x 314mm). Next session I'll cut out the floor to prepare for a combination drop out floor for large objects, and manually operated  adjustable floor (for thick objects and focusing. I ordered a 2.5" focal length lens so it will focus at the bottom of the aluminum rails, so my new floor can be oversized. It will not need to extend into the rails, only meet the bottom. 

Redesign to accomidate fish tank stand now in mix.



Subject to change at any time without notice or reason, I'm making this up as I go. 



Don't worry, I seriously doubt you CAN put it together worse than it was....



have fun, Scott


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/EeD3ZpTMjfU) &mdash; content and formatting may not be reliable*
