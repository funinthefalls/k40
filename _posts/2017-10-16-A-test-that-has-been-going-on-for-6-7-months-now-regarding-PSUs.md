---
layout: post
title: "A test that has been going on for 6-7 months now regarding PSUs"
date: October 16, 2017 10:51
category: "Discussion"
author: "HP Persson"
---
A test that has been going on for 6-7 months now regarding PSU´s.



What i have done

- Asked people with PSU problem what their issues was and how they appeared

- Suggested a change of Rectifier or flyback depending on the faults they experienced



So far the list is like this



15 PSU´s with confirmed flyback (HVT) problems, with a new flyback the machine worked as usual again.

5 PSU´s with confirmed bridge rectifier problems, with new rectifier the machine worked as usual again.

14 PSU´s died again within two to 90 days even with a new flyback.

12 of these PSU´s has now gone trough a change of the bridge rectifier to a 700V, 5A from the original 1A(?) and a new flyback again.



Of the 12 with changed flyback and rectifier, 11 is still working after more than 60 days. One has burned again but on the mosfet-side.

A total of 31 PSU issues was documented, 20 of them changed parts, 10 scrapped the PSU´s.

Of the 5 with new rectifier only, all is still reported to work.



Notes about power levels, coolant water type and ambient temps were noted but there is no common denominator that could explain the problems.



I am nowhere close to a engineer to explain this...

I do see how it changes though and the amount of PSU´s tested tells me there is something to this that need further investagation, and a explanation of why and how.



Can anyone explain these results ? :)

Or a wild guess? :)







**"HP Persson"**

---
---
**Adrian Godwin** *October 16, 2017 13:16*

For what it's worth, I obtained a K40 that had failed on first plug-in. I found the fuse blown and the bridge rectifier shorted. I replaced the bridge rectifier and so far it's worked OK (a few months but relatively little use). 



Not sure of the rectifier rating I used, as it was one I had to hand, but probably higher rating than the original. Bigger, anyway :). 



So I can add 1 to the tally of 'new rectifier, still working'.


---
**Frank Fisher** *October 16, 2017 15:32*

I believe I just lost my flyback transformer after a few months use.  Anyone know where I can get one <b>yesterday</b> for 40W as estimates are early December from China :(




---
**Nate Caine** *October 16, 2017 18:04*

**+HP Persson**​​​​​​​​​​​​, it might be helpful to know some additonal information about the failures you've documented as they provide clues to the failure mechanisms.



(1)  How many were operating in 240V (full-wave rectifier) environments versus 120V (half-wave rectifier) locales?  <i>(This might help to identify that the bridge rectifier is undersized.)</i>



(2)  When the rectifier failed, how often was this associated with an initial power-on condition versus a power supply failure in the middle of a job?  <i>(This might point to an inadequately chosen NTC inrush surge current device.)</i>



(3)  When the rectifier fails, does it fail in the shorted condition or open circuit?  <i>(This might point to forward over-current versus excessive Peak Inverse Voltage, PIV, condition.)</i>  I note that most SMPS's have NTC devices to limit inrush current, and MOV devices to quench voltage spikes.  Unfortunately, these K40 laser power supplies do not have voltage surge protection MOVs.



(4)  For the HV-transformer, was the failure associated with a long and high powered job, in a heavy use environment?  Or was it more of a random failure?  Some of the photos posted here point to a HV-transformer failure that look like it overheated, and physically warped/melted.  <i>(This might identify a design problem and that the transformer is under-rated, versus a manufacturing problem of the transformer itself--insulation breakdown or internal component failure.)</i>   



(5)  Was the HV-transformer problems associated with a wiring problem (i.e. HV lead arcing to chassis)?  <i>(The HVPS design anticipated either an arc or open circuit (broken tube) condition, and protects against it.  This might indicate the design needs more robust protection.)</i>



(6)  How many of the failures can be attributed to these being cheap bastards?  <i>(Are the components, or the HVPS's themselves, factory rejects or seconds, that were destined to fail anyway and they just didn't care?)</i>


---
**HP Persson** *October 16, 2017 18:09*

**+Nate Caine** Thanks. Have do dig a bit in the notes to find some more details, but most of them have been 110-120v users.

Great points, i will check it up.


---
**HalfNormal** *October 16, 2017 18:10*

**+Nate Caine** I tend to agree with #6. You get what you pay for and for the price, we are not buying high end quality machines.


---
**Don Kleinschnitz Jr.** *October 16, 2017 22:20*

I have repaired or helped folks repair more LPS than I can count and have 5 of them in test and or repair.

I have not kept as good as records as **+HP Persson** but have evidence of the following that you can add to the pool of knowledge:

1. The #1 failure is the HVT and most of the time replacement fixes it. Tear-down of two HVT failures showed that no coils were open The doubler diodes could not be tested or sectioned. 100% of the HVT replacements were either confirmed as a fixes or fix was assumed because the discussion ended after the replacement. 



2. The #2 failure is the bridge rectifier. It typically blows the fuse. Bridge and fuse replacement put these supplies back into service.



3. My 5 supplies turned out to have the following problems: 

...3x HVT's. 

...1x enable diode and circuit blown. I suspect the AC cable was accidentally plugged into the DC socket.

... 1x AC bridge rectifier, shorted diodes



..........

My static analysis of the parts used in the LVPS has not shown a design issue but that assumes the parts are of sufficient quality and meet the data sheet specs. Some parts like the HVT diodes are unmarked and un-testable. 



I cringe at some of the PCB spacing.



I have plans this winter to do a dynamic analysis and get scope traces to look for transient problems.

.........

I think that the possible failure causes I have seen sort out as follows:  



<b>HVT</b> 

a.) the doubl-er diodes PIV is inadequate or/and 

b.) the potting on the secondary breaks down and shorts the secondary.  When the supply is unloaded (bad tube) and fired I have proven that the HVT will arc through the potting. Interestingly I have yet to blow a supply with arcs to gnd.



<b>Bridge Rectifier</b>

I have measured in excess of 600VDC on the DC main buss. I know from personal :( experience that an arc on this buss will take out the rectifier. I suspect the spacing in the area of the Low Voltage PWM controller to be inadequate especially if there is contamination. 



One other comment. Any form of internal arc or LVPS PWM failure in the 24V regulator section can create a catastrophic failure all the way back to the Bridge rectifier.


---
**HalfNormal** *October 16, 2017 22:24*

**+Don Kleinschnitz**​​ I am sure you ment to reference **+HP Persson**​​ in your post. I will take the compliment of confusing us!


---
**Don Kleinschnitz Jr.** *October 16, 2017 22:49*

**+HalfNormal** whoops!






---
**Paul de Groot** *October 17, 2017 05:39*

As **+Don Kleinschnitz**​ states it's basically a poorly designed psu. There is not short output prevention. I think a redesign of the psu is not that complicated. Mainly pcb layout, current protection and better matched components 


---
**HP Persson** *October 17, 2017 06:26*

Thanks for the input everyone :)



I actually started this excel sheet to see if there was something common between tap water or contaminated water and power supply troubles. Instead the bridge rectifier turned up as something slowing down the HVT committing suicide.



Out of the 31 power supplies, 26 was running at 110-120V, only 5 at 240-250V.

From the 13 with changed parts, 10 was ran at 110v.



Always interesting to find out more about our beloved K40 monsters :)


---
**Phillip Conroy** *October 17, 2017 07:12*

Cheap,low quality laser power supplys, my k50 pwr supply lasted 100 hours ,changed flyback transformer and ordered this [https://www.aliexpress.com/item/Z-Series-Intelligent-Z80-Co2-Laser-Power-Supply-80W-For-Laser-Engraving-Machine-With-LCD/32806003257.html?spm=a2g0s.9042311.0.0.Gfuy](https://www.aliexpress.com/item/Z-Series-Intelligent-Z80-Co2-Laser-Power-Supply-80W-For-Laser-Engraving-Machine-With-LCD/32806003257.html?spm=a2g0s.9042311.0.0.Gfuy)


---
**Phillip Conroy** *October 17, 2017 07:13*

[aliexpress.com - Intelligent Z80 Co2 Laser Power Supply 80W For Laser Engraving And C Machine](https://www.aliexpress.com/item/Z-Series-Intelligent-Z80-Co2-Laser-Power-Supply-80W-For-Laser-Engraving-Machine-With-LCD/32806003257.html?spm=a2g0s.9042311.0.0.Gfuy)


---
**Phillip Conroy** *October 17, 2017 07:14*

Warning on ebay this laser power supply  is $700 ,this power supply can also be used on tubes as low as 40 watts


---
**HP Persson** *October 17, 2017 08:44*

**+Phillip Conroy** Thanks, i ordered one of these when you linked one on facebook a week ago or something like that. Interesting to see how it works with my machines :)


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/LWmgyuA3ZB8) &mdash; content and formatting may not be reliable*
