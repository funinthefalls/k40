---
layout: post
title: "Finally got the K40 guts working well in the ULS machine"
date: February 07, 2018 01:21
category: "Object produced with laser"
author: "David Cook"
---
Finally got the K40 guts working well in the ULS machine. Using GRBL 1.1 on a Arduino CNC shield.  Many thanks to the LaserWeb team!!





**"David Cook"**

---
---
**Claudio Prezzi** *February 07, 2018 10:02*

**+LightBurn Software** It's not very kind to advertise your software when somebody reported he is happy with LaserWeb!

Or should we do the same when you write about LB?


---
**LightBurn Software** *February 07, 2018 16:22*

I’ve removed the post. My apologies.


---
**Claudio Prezzi** *February 08, 2018 18:19*

Thanks. 


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/gQwf9B74SAX) &mdash; content and formatting may not be reliable*
