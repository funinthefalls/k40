---
layout: post
title: "Has anyone used this middleman board? I'm trying to use this with the smoothie"
date: January 31, 2016 23:25
category: "Smoothieboard Modification"
author: "Arestotle Thapa"
---
Has anyone used this middleman board? I'm trying to use this with the smoothie. I'm just gathering parts and haven't started the upgarde yet! Thanks

[https://www.spaelectronics.com/shop/k40-middleman-board.html](https://www.spaelectronics.com/shop/k40-middleman-board.html)





**"Arestotle Thapa"**

---
---
**Anton Fosselius** *February 01, 2016 00:00*

What happend with the y axis?


---
**Arestotle Thapa** *February 01, 2016 00:17*

Y axis is not connected through the ribbon cable in my k40.


---
**Anton Fosselius** *February 01, 2016 06:41*

Ok. I thought so. Have not taken a closer look at my k40 yet. But I bet it's the same ;)


---
**Anthony Bolgar** *February 01, 2016 06:42*

The Y axix plugs directly into the Ramps board in my setup. I am sure it is the same with the smoothie board, the middleman only breaks out the x axis and end stops that are on the ribbon cable.


---
**Arestotle Thapa** *February 01, 2016 12:19*

**+Anthony Bolgar**  are you using two end stops per axis? I'm trying to figure out the best spot to install the switches and how to route the wires.


---
**Anthony Bolgar** *February 01, 2016 12:37*

I only use the home stops, the home for x and home for y run in the ribbon cable. Will be adding end of travel stops for both axis' in the near future.


---
**David Richards (djrm)** *February 05, 2016 23:45*

I bought some of these boards from OSH Park, the connectors from RS in the UK. I have connected it to my azsmz board but not yet fitted it to my Laser cutter. The main reason to have it is as an interface to the K40 ribbon cable.


---
**Arestotle Thapa** *February 07, 2016 21:58*

**+david richards**  are you using smoothie? If so what software are you using? I want to test the board and steppers before connecting the to laser.


---
**David Richards (djrm)** *February 07, 2016 22:14*

**+Arestotle Thapa** I have an azsmz mini board running what was the latest smoothieware a couple of months ago. I have not yet fitted it to my K40.


---
**ThantiK** *February 10, 2016 17:32*

**+Peter van der Walt** this is the breakout I was referring to.


---
**Mark Finn** *February 10, 2016 18:16*

There's also this one: [http://portfolioabout.me/k40-smoothielaser-breakout-board/](http://portfolioabout.me/k40-smoothielaser-breakout-board/) that you can make yourself or the guy sells it in his store for $6 with connectors. He says he based it on the middleman.



If you make it yourself, his part number for the terminals is wrong, you want 277-1860-ND


---
*Imported from [Google+](https://plus.google.com/104400841682788049551/posts/eHd1aieTkZ1) &mdash; content and formatting may not be reliable*
