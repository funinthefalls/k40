---
layout: post
title: "Whats your current / most used focal length"
date: August 18, 2016 12:49
category: "Hardware and Laser settings"
author: "Ariel Yahni (UniKpty)"
---
Whats your current / most used focal length





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *August 18, 2016 12:52*

FB group pol for reference [https://goo.gl/Sx67wQ](https://goo.gl/Sx67wQ)


---
**Damian Trejtowicz** *August 18, 2016 12:57*

7.5 inch


---
**Don Kleinschnitz Jr.** *August 18, 2016 13:08*

BTW my lens is not the typical K40 lens and it caused me problems at the start. I had to measure it to get focal set right.


---
**Bart Libert** *August 18, 2016 14:11*

Damian. 7.5 inch can you tell something more why such a long FL. Special application?


---
**Scott Marshall** *August 18, 2016 21:17*

2.5 mm lens is a cutting animal. It rolls thru Baltic Birch 1/4"+ in 1 pass.



I'm not shy on the throttle, mine shows 20ma (don't know if the $2 meter is accurate) wide open, and if that's what it takes, that's what I run. No ill effects as yet.



I figure I'll have to replace the tube for reasons other than simply "worn out" anyhow, and the next one will be a 10k hr class tube. Probably larger, as it's quite hard to find 10K hr tubes in 700 or 800mm.

80W should fit, run at 60 should live a long time if I don't do something dumb.



I'll baby a $400-$500 tube where I will hammer the $135 model.



I'm not advocating this approach for everyone. If your like me, you don't need encouragement. (is the carpeting worn off under your gas pedal?)



Scott



Good job on the polls Ariel! Keeps things interesting and we learn a bit.


---
**Don Kleinschnitz Jr.** *August 19, 2016 03:31*

**+Scott Marshall** 2.5 mm or 2.5"


---
**Don Kleinschnitz Jr.** *August 19, 2016 03:32*

[http://www.troteclaser.com/en-us/support/faqs/pages/selection-of-the-right-lens.aspx](http://www.troteclaser.com/en-us/support/faqs/pages/selection-of-the-right-lens.aspx)


---
**Scott Marshall** *August 19, 2016 04:01*

Yeah Don. been working too long. Mixing my units, maybe that's the reason nothing's working today.



2.5"



I'm from that generation that defaults to the english system when given a choice.



Scott


---
**Don Kleinschnitz Jr.** *August 19, 2016 11:28*

**+Scott Marshall** I am from that same generation :).


---
**Robi Akerley-McKee** *August 20, 2016 21:06*

I have a 2" 50.8mm and a 4" 101.6mm.  The 4" is so I can cut a hole in the case to get below the stepper to put in long objects.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/LFM2h47nKo7) &mdash; content and formatting may not be reliable*
