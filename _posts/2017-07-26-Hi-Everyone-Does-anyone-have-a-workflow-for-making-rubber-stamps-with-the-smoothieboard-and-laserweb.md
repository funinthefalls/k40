---
layout: post
title: "Hi Everyone, Does anyone have a workflow for making rubber stamps with the smoothieboard and laserweb?"
date: July 26, 2017 06:10
category: "Discussion"
author: "Ben Myers"
---
Hi Everyone,



Does anyone have a workflow for making rubber stamps with the smoothieboard and laserweb? I have tried but my result are dissapointing. I will post some pics when I get home.



Thanks,



Ben





**"Ben Myers"**

---
---
**Claudio Prezzi** *July 26, 2017 06:28*

I just paint the stamp with a picture editor (like fireworks/photoshop), then mirror it horizontally and invert it. Save as JPG and run as Laser Raster with 0.05 - 0.1mm tool diameter in LaserWeb. Works fine for me.


---
**Claudio Prezzi** *July 26, 2017 06:30*

You just once need to find the right power and feedrate for your laser rubber to get about 50% deep. Eventually it makes sense to do multiple path.


---
**Joe Alexander** *July 26, 2017 12:35*

i did a stamp the other day using 1/8" stamp rubber from amazon, I'll see if I documented the settings when I get home. I know I ended up doing 3 passes to control the depth without overshooting it, and only did the one so far.(something like 10ma on pot 70% power@20-25mm/s, 3 pass) Make sure to add your results to this post for everyone else!


---
**Don Kleinschnitz Jr.** *July 26, 2017 13:28*

#K40RubberStamp


---
**Ben Myers** *July 26, 2017 22:33*

The problem is instead of doing long laser burns they are like a series of dots (hard to explain will need to get a video). I put in the old M2 board and it engraved perfectly...




---
*Imported from [Google+](https://plus.google.com/112260902064831380364/posts/9zyivFs1Zwp) &mdash; content and formatting may not be reliable*
