---
layout: post
title: "Someone near me is selling a smoothieboard 5X + GLCD Adapter, if anyone is interested"
date: June 08, 2016 14:43
category: "Smoothieboard Modification"
author: "Jean-Baptiste Passant"
---
Someone near me is selling a smoothieboard 5X + GLCD Adapter, if anyone is interested. I already have one so if anyone need it just tell me and I'll hook you with him.



He is asking for 100€ + Shipping, France or Europe is probably a better idea as shipping would be a dealbreaker outside europe I believe.



I'm always checking the nearby fablab for that kind of deal if anyone is interested.





**"Jean-Baptiste Passant"**

---
---
**Jean-Baptiste Passant** *June 08, 2016 14:44*

Also it has a 5V1A reg and a kill button installed.


---
**Pete Sobye** *June 11, 2016 14:37*

What improvement over the stock board would this be? Also if it's still available and I bought it, what else would I need to add and does it come programmed with installation instructions, I'm a noooooob !!


---
**Jean-Baptiste Passant** *June 11, 2016 15:24*

Well, improvements can be seen on many post here :P (Speed, simplicity and Laserweb are probably the biggest improvements)



I can copy my own configuration and schema on the SD if needed.



With this you will probably need a K40 middleman board depending on your power supply (if it use a ribbon cable or not), and a lcd as there is none with it.


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/YcJLmpHQPtv) &mdash; content and formatting may not be reliable*
