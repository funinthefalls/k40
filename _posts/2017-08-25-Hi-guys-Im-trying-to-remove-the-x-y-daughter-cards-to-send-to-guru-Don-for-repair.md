---
layout: post
title: "Hi guys - I'm trying to remove the x/y daughter cards to send to guru Don for repair!"
date: August 25, 2017 20:20
category: "Discussion"
author: "Brian Li"
---
Hi guys - I'm trying to remove the x/y daughter cards to send to guru Don for repair!  The screws for the y-axis card were easy to get to, but the x card screws are blocked by a rail so I can't get a good angle on the screw - I can't quite get a grip on it, and I'm afraid to keep trying cause I might strip the screw. Any tips any how to get a little more clearance on this?

Thanks!

![images/3df3b56a3e5704fad44f3371c17d34fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3df3b56a3e5704fad44f3371c17d34fd.jpeg)



**"Brian Li"**

---
---
**Ashley M. Kirchner [Norym]** *August 26, 2017 00:13*

Only way I ever got to that was disassembling the whole carriage.


---
**Kelly Burns** *August 28, 2017 13:31*

If you remove the 2 screws that hold the T bracket on the X rail you will be able to rotate the bracket.  


---
*Imported from [Google+](https://plus.google.com/113792581999914867877/posts/NM5AGmCkvCx) &mdash; content and formatting may not be reliable*
