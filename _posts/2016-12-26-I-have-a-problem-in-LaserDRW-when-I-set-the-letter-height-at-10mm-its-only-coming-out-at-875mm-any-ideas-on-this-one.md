---
layout: post
title: "I have a problem in LaserDRW when I set the letter height at 10mm its only coming out at 8.75mm, any ideas on this one?"
date: December 26, 2016 16:24
category: "Software"
author: "john dakin"
---
I have a problem in LaserDRW when I set the letter height at 10mm its only coming out at 8.75mm, any ideas on this one?





**"john dakin"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 18:18*

Does the same thing happen with different fonts? Could be that the 10mm height of the lettering takes into account ascenders & descenders (i.e. parts of letters that are tall, e.g. in the letter t, & parts of letters that go down, e.g. the letter q). If you write a word like "telegraph", is the measurement from the top of the t to the bottom of the p 10mm?


---
**john dakin** *December 26, 2016 18:39*

Really no idea whats going on here, I adjusted my mirrors as they were not hitting the centre of them, now I seem to have a lot less power and the font height is not as it should be, I did the word telegraph and I set the character height to be 15mm but its actually coming out at 7.52mm on the letter "L" and the rest of the letters are smaller


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 22:07*

Sounds like maybe this is related to your Board Model and/or Board ID. I've heard when they're not set correctly that the scale of objects can come out quite different to what it should be. I suggest verifying that your Board Model & ID are set correctly in the LaserDRW options. I'm not familiar with LaserDRW since I used CorelDraw/CorelLaser, but I believe the plugin is the same, so hopefully this screenshot is similar to what your options are:



[https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)


---
**john dakin** *December 26, 2016 23:45*

Thanks for the info Yuusuf, I will try it later


---
**Armando Araiza** *December 27, 2016 17:53*

What is your DPI settings on engraving? When I initially purchased my mine I set the dpi to 600 and noticed a size reference change in all things cut or engraved. Set it back to 1000dpi and resolved my issue if your board and model are correct




---
**john dakin** *December 27, 2016 17:55*

its set to 1000 dpi


---
*Imported from [Google+](https://plus.google.com/110405157088691728751/posts/BfNnAuS51q8) &mdash; content and formatting may not be reliable*
