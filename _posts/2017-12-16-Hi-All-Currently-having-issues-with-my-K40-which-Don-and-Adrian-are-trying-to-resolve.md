---
layout: post
title: "Hi All Currently having issues with my K40 which Don and Adrian are trying to resolve"
date: December 16, 2017 16:43
category: "Discussion"
author: "Frank Farrant"
---
Hi All



Currently having issues with my K40 which Don and Adrian are trying to resolve.

.

Is there anyone in or around Abingdon, Oxfordshire over the next few days who could take a look at the machine and find/solve the problem ?

.

Also, does anyone have a spare K40 I could borrow ?  



If you can help with anything - please text me on 07718 048862. 



Thanks

Frank





**"Frank Farrant"**

---
---
**Frank Farrant** *December 17, 2017 16:32*

Hi

I'm looking for some Silicone to use when I attach the cables to the tube.  

Is Dow Corning 703 ok to use ?

If not, can you recommend something I can buy in the UK which is freely available ?

Thanks


---
*Imported from [Google+](https://plus.google.com/110419041310362555971/posts/jAYEAG2Ntde) &mdash; content and formatting may not be reliable*
