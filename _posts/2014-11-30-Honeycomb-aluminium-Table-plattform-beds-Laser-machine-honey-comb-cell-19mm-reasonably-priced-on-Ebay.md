---
layout: post
title: "\"Honeycomb aluminium Table, plattform, beds, Laser machine honey comb cell 19mm\" reasonably priced on Ebay"
date: November 30, 2014 15:05
category: "Hardware and Laser settings"
author: "Stephane Buisson"
---
"Honeycomb aluminium Table, plattform, beds, Laser machine honey comb cell 19mm" reasonably priced on Ebay.



[http://www.ebay.co.uk/itm/271422470503?_trksid=p2060778.m2749.l2649&var=570286186272&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.co.uk/itm/271422470503?_trksid=p2060778.m2749.l2649&var=570286186272&ssPageName=STRK%3AMEBIDX%3AIT)



![images/3edb710844a39f42759d5b0338f0fe7c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3edb710844a39f42759d5b0338f0fe7c.jpeg)
![images/d888c9d3daea8b94205bea2d75db3d6d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d888c9d3daea8b94205bea2d75db3d6d.jpeg)
![images/8664a052910c7db6592f0747d26a00f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8664a052910c7db6592f0747d26a00f0.jpeg)

**"Stephane Buisson"**

---


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/HinyXxVhJ94) &mdash; content and formatting may not be reliable*
