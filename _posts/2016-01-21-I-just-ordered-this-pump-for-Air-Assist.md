---
layout: post
title: "I just ordered this pump for Air Assist"
date: January 21, 2016 20:48
category: "Modification"
author: "David Cook"
---
I just ordered this pump for Air Assist. Hydrofarm AAPA70L 60-Watt 70-LPM

[http://www.amazon.com/Hydrofarm-AAPA70L-60-Watt-Commercial-Outlets/dp/B002JPM91W/ref=sr_1_8?s=lawn-garden&ie=UTF8&qid=1453409034&sr=1-8&keywords=air+pump](http://www.amazon.com/Hydrofarm-AAPA70L-60-Watt-Commercial-Outlets/dp/B002JPM91W/ref=sr_1_8?s=lawn-garden&ie=UTF8&qid=1453409034&sr=1-8&keywords=air+pump)





**"David Cook"**

---
---
**Eric Parker** *January 22, 2016 00:22*

looks like mine


---
**Stephane Buisson** *January 22, 2016 12:38*

Noisy ???


---
**David Cook** *January 22, 2016 14:36*

**+Stephane Buisson** probably. I'll let you know once it arrives 


---
**Eric Parker** *January 23, 2016 09:13*

not too bad.


---
**Joseph Midjette Sr** *January 23, 2016 11:46*

I saw the model under this one, significantly lower wattage and price, 20 watt and 40 LPM I believe. Will that model not be sufficient? Is the 60 watt 70 LPM not overkill? 


---
**Eric Parker** *January 23, 2016 19:09*

I have a 55 watt 60LPM pump and it seems almost perfect, but I have nothing to compare it to.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/4fEPaeBUzHX) &mdash; content and formatting may not be reliable*
