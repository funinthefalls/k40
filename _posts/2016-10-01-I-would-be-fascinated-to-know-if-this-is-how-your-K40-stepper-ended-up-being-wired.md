---
layout: post
title: "I would be fascinated to know if this is how your K40 stepper ended up being wired"
date: October 01, 2016 22:46
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
I would be fascinated to know if this is how your K40 stepper ended up being wired. Colors in this order? These are paired with an ohm meter.

I assume pin 1 is on the right in this picture

![images/8d77ab157d35f316841a887c99c6653a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d77ab157d35f316841a887c99c6653a.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Anthony Bolgar** *October 02, 2016 00:21*

I have seen so many different variations on the color coding. No real standard that I can decipher.


---
**Don Kleinschnitz Jr.** *October 02, 2016 00:31*

Ya, I know just looking to see if there are any matches.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 02, 2016 00:53*

Can't really say for mine, as I am using Scott's ACR to interface & make the connections easy for me. From what Scott has shared the wiring colours seem to be based on the mood of the day in the factory lol.


---
**Ariel Yahni (UniKpty)** *October 02, 2016 01:18*

I have seen rainbow of colors on these


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/CeY6M9Jvs2t) &mdash; content and formatting may not be reliable*
