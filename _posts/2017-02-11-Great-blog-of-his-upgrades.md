---
layout: post
title: "Great blog of his upgrades"
date: February 11, 2017 13:34
category: "Modification"
author: "Anthony Bolgar"
---
Great blog of his upgrades.



[http://jimmybdesign.com/k40-laser-cutter/](http://jimmybdesign.com/k40-laser-cutter/)





**"Anthony Bolgar"**

---
---
**Andy Shilling** *February 11, 2017 14:00*

Great write up and upgrades. Does the air pump create any issue with vibrations? I have mine hanging but I like the fact you mounted it to the cabinet.


---
**Ned Hill** *February 11, 2017 14:42*

Interesting write up.  Not sure about mounting all those auxiliary units (fans, pump) to the cabinet,   since all the vibration could cause issues.  Also since that guy is only using a simple air/water heat exchanger it's never going to get colder than the ambient temp of the room.  I do like that 90 deg duct horn on the back.


---
**John Sturgess** *February 11, 2017 18:22*

**+Andy Shilling** he does mention he doesn't recommend it due to the vibration at the end of that section. I've used it as a guide for my closed loop cooling system except i've gone for a 240mm rad and peltier block.


---
**Andy Shilling** *February 11, 2017 18:25*

**+John Sturgess** Clearly missed that part lol.


---
**Anthony Bolgar** *February 11, 2017 21:53*

I run a 240 rad on my 1.5kW water cooled spindle on my cnc router. Works great.


---
**Jim Bilodeau** *February 12, 2017 23:50*

Hey guys, this is my blog; I never got around to posting it here! Ask away with any questions. 



As John mentioned, I did originally mount the air assist pump to the chassis, but it vibrated too much so I took it off and just sit it nearby.



Vibration from the radiator fan and water pump is not an issue. 



Cooling has not been an issue so far (right now it gets too cold because it is freezing out and it sits next to the door outside). Come summertime it will be an issue; as Ned noted it will never get below ambient as it is. I think I'll add in a TEC eventually; and maybe switch to a 240mm radiator.


---
**Jim Bilodeau** *February 13, 2017 00:49*

**+John Sturgess** What peltier cooler did you buy and how did you integrate it with your setup?


---
**John Sturgess** *February 13, 2017 08:53*

**+Jim Bilodeau** I've got one of the diy water cooling kits that come with the peltier, water block, heatsink and fan that'll probably need upgrading. I've not tested anything yet as the whole laser is getting overhauled. To go on the machine I've also got the same small reservoir and pump, I'm thinking about adding the peltier after the rad and a second reservoir pre tube. Therefore I can measure temperature before and after the tube. My laser is in the house, the rad and fans will adequately cool it in the winter but in the summer the ambient temp will be too high so I'll use the peltier then. 


---
**Jim Bilodeau** *February 14, 2017 00:46*

**+John Sturgess** A block like this? [amazon.com - Amazon.com: Yobett Aluminum Water Cooling Blocks for Cpu Heatsink Cooler Plater 41x 41 X 12mm: Computers & Accessories](https://www.amazon.com/Yobett-Aluminum-Cooling-Blocks-Heatsink/dp/B00DHC2GJ6/ref=s9u_simh_gw_i3?_encoding=UTF8&fpl=fresh&pf_rd_m=ATVPDKIKX0DER&pf_rd_s&pf_rd_r=DTM0TGK58RT10Q3F8YMT&pf_rd_t=36701&pf_rd_p=a6aaf593-1ba4-4f4e-bdcc-0febe090b8ed&pf_rd_i=desktop)



Seems like a pretty easy thing to integrate. Connect to a 12V psu with a potentiometer inline to regulate the temp. I may do the same!


---
**John Sturgess** *February 14, 2017 10:53*

**+Jim Bilodeau** Like this: [ebay.co.uk - Details about  DIY kits Thermoelectric Peltier Semiconductor Refrigeration Water Cooling System](http://www.ebay.co.uk/itm/DIY-kits-Thermoelectric-Peltier-Semiconductor-Refrigeration-Water-Cooling-System-/291643679395?hash=item43e75162a3:g:h5EAAOSwENxXmcKS) 

I wasn't going to use a pot, i'm not sure if/how that'll work. I'm going to use an Arduino for the safety system, measuring temps and flow, if the temp goes above a certain setpoint have it turn the peltier on via a relay.I'll add a few degrees of hysterisis so it isn't constantly turning on/off.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/UPkcTvV9wKF) &mdash; content and formatting may not be reliable*
