---
layout: post
title: "Hey LaserWeb users, can you run multiple lasers from the same host computer, simultaneously?"
date: December 01, 2016 17:10
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Hey LaserWeb users, can you run multiple lasers from the same host computer, simultaneously? Something like, load up design file, select laser1, send. Select laser2, send. Or even, be able to load up a different design file and send it to a different machine... Or is the data being send a constant stream as the machine works and therefore cannot be interrupted? 





**"Ashley M. Kirchner [Norym]"**

---
---
**Ashley M. Kirchner [Norym]** *December 01, 2016 17:20*

Ok, so one interface, one stream then? Does that mean I can open multiple instances of LW and pick a different machine to send to (and hope it doesn't bog down the connection)? 


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 17:43*

That's easy enough, it's the same approach I use on my unix server when I need to run multiple instances of a node application. So then the only question would be the two (or more) data streams and how they might possibly affect each other, throughput and such. Guess that's to be tested. I really don't want to have to run multiple host computers, one per machine.



I suppose alternatively if the machines are on a network, I can simply copy the design files to them, then trigger/monitor them from one central computer ... Hmmmm.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/Bwwb55xNQnU) &mdash; content and formatting may not be reliable*
