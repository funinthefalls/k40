---
layout: post
title: "Has anyone had the issue that when cutting an object that is just lines that it goes over them twice?"
date: June 07, 2016 16:19
category: "Discussion"
author: "Ben Crawford"
---
Has anyone had the issue that when cutting an object that is just lines that it goes over them twice? is this because it sees the like as an object and it tries to cut both sides?





**"Ben Crawford"**

---
---
**Ariel Yahni (UniKpty)** *June 07, 2016 17:11*

More or less. You need to make the lines 0.025 thick﻿.  At least on Corellaser


---
**Jim Hatch** *June 07, 2016 18:31*

Yes. Either the line is thick (I use .001mm for the stroke weight on any cut line) or the software actually draws it as a box (a really really skinny box). I've had the box as a line issue with LaserCut on a big Chinese laser - only saw it under seriously large magnification.


---
*Imported from [Google+](https://plus.google.com/110714520603831442756/posts/jRsE9zE9YcK) &mdash; content and formatting may not be reliable*
