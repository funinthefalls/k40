---
layout: post
title: "After I started rebuilding my K40 some time ago I cannot find the stock green ballast resistor anymore, I am 50% sure it was there :( Does anyone use similar part like this one here?"
date: August 22, 2016 11:19
category: "Modification"
author: "Sebastian Szafran"
---
After I started rebuilding my K40 some time ago I cannot find the stock green ballast resistor anymore, I am 50% sure it was there :(



Does anyone use similar part like this one here? -> [https://www.aliexpress.com/item/AE-50K-Ohm-200W-Watt-Power-Metal-Shell-Case-Wirewound-Resistor/1121557665.html](https://www.aliexpress.com/item/AE-50K-Ohm-200W-Watt-Power-Metal-Shell-Case-Wirewound-Resistor/1121557665.html)



And where was the ballast resistor connected, between L- on power supply and via Ampermeter to GND?





**"Sebastian Szafran"**

---
---
**Phillip Conroy** *August 22, 2016 12:54*

my k40 came without any balast resistor -brought mar 2015


---
**Don Kleinschnitz Jr.** *August 22, 2016 13:57*

Mine either


---
**HalfNormal** *August 22, 2016 14:09*

These are found on the Moshi  powered systems with older power supplies. 


---
**Sebastian Szafran** *August 22, 2016 14:34*

My K40 was originally Moshi based, but I scrapped all stock boards. It is possible that the resistor has been connected to Moshi and after getting rid of it is not needed anymore. I need to search for old pictures. 


---
**Robi Akerley-McKee** *August 22, 2016 19:16*

I'm using a 60Kohm 100w resistor in green that looks similar to your picture. I soldered the wires on, then put heat shrink over the top right up to the housing. here's a 50Kohm listing [http://www.ebay.com/itm/100W-50K-Ohm-Screw-Tap-Mounted-Aluminum-Housed-Wirewound-Resistors-2-Pcs-/310578131352?hash=item484fe63198](http://www.ebay.com/itm/100W-50K-Ohm-Screw-Tap-Mounted-Aluminum-Housed-Wirewound-Resistors-2-Pcs-/310578131352?hash=item484fe63198)


---
**Robi Akerley-McKee** *August 22, 2016 19:19*

**+Sebastian Szafran** It would depend on which power supply you have, not the control board.


---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/GqkmKdKjTMV) &mdash; content and formatting may not be reliable*
