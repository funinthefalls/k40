---
layout: post
title: "I'm expanding my keychains into illinois now"
date: February 19, 2016 17:18
category: "Object produced with laser"
author: "Ben Walker"
---
I'm expanding my keychains into illinois now.   Same template that I shared yesterday. 

![images/4f6396c6ae843579a1c9c8bae3976317.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f6396c6ae843579a1c9c8bae3976317.jpeg)



**"Ben Walker"**

---
---
**Ariel Yahni (UniKpty)** *February 19, 2016 18:22*

What material is it? 


---
**Ben Walker** *February 19, 2016 18:35*

**+Ariel Yahni** it's 1/8 inch Baltic birch plywood.  I cut both sides and glue them together.   


---
**Jim Hatch** *February 19, 2016 23:42*

:-) you're going to be the LEO keychain king!


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/YWMe9GfCTUV) &mdash; content and formatting may not be reliable*
