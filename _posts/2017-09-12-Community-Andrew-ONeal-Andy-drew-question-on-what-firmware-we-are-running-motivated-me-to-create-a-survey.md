---
layout: post
title: "Community; +Andrew ONeal (Andy-drew) question on what firmware we are running motivated me to create a survey"
date: September 12, 2017 15:05
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
<b>Community;</b>



+Andrew ONeal (Andy-drew)  question on what firmware we are running motivated me to create a survey.



Below is a link to a survey but please <b>do not fill it out</b> just yet.



Instead, I am interested in getting your input to see if there are important choices or questions that I am missing. 



So please review the content and let me know in a post below what I missed. 



[if you are wondering why I did not use the G+ survey tool, its simply because it is to restrictive :(!]



[https://goo.gl/forms/ICoVO45KqegkVDyI2](https://goo.gl/forms/ICoVO45KqegkVDyI2)





**"Don Kleinschnitz Jr."**

---
---
**Jim Hatch** *September 12, 2017 15:40*

Couldn't see the questions without taking the survey. But, I'd add "Stock" or "Nano" to the Controller question - you've just got the 3rd party ones listed. For interface software I think you'll get confusion from people who are using Corel Laser (CorelDraw with the LaserDraw plug-in). So either an additional option for Corel or a note that it's using LaserDraw to control the machine vs the design step. Source design software - might want to add Adobe Illustrator (CS). 


---
**Don Kleinschnitz Jr.** *September 12, 2017 16:36*

**+Jim Hatch** 

I added your suggestions and in the process realized that I forgot "firmware" then added it.



I thought you could just "next" on each form screen and it would not log any content. Anyway I will delete all contents and start fresh after the review.



Thanks for reviewing! 


---
**Scorch Works** *September 12, 2017 16:52*

Controller:

I would like to know which stock boards people are using:

Moshi

6C6879-LASER-M2

6C6879-LASER-M1

6C6879-LASER-M 

6C6879-LASER-B2

6C6879-LASER-B1

6C6879-LASER-B

6C6879-LASER-A

But that may be too many options...



Interface:

Laser Draw without Corel should also be an option.



Moshidraw should be an option.



I think Ponterface is supposed to be Pronterface (typo?).


---
**Ashley M. Kirchner [Norym]** *September 12, 2017 17:27*

<b>*Firmware:*</b> Do you want to make a distinction with Smoothieware firmware? There's the stock (XYZ) download, then there's the custom builds for 4-AXIS mode (XYZA) or more.



<b>*Host/Hardware:*</b> For the 'Other' option, how about making that a text area instead of just a single line input field? The reason I say this is because I have successfully run the LaserWeb backend on a RaspberryPi, C.H.I.P., as well as the standard front end on a UDOO x86 (which runs Win10), as well as the more mundane Win7 and Win10 on a regular laptop.


---
**Don Kleinschnitz Jr.** *September 12, 2017 17:30*

**+Scorch Works** 

Ok all that is added!

The list of Moshi board levels is gonna cost ya :)!



Try it again please ....


---
**Ashley M. Kirchner [Norym]** *September 12, 2017 17:32*

Also, I use Adobe Illustrator for vector work, and I use Adobe Photoshop for raster. I also design in Autodesk AutoCAD as well as Autodesk Inventor. I suspect the application section might be rather lengthy for some. :)


---
**Don Kleinschnitz Jr.** *September 12, 2017 17:44*

**+Ashley M. Kirchner** your stuff is added.....


---
**Ashley M. Kirchner [Norym]** *September 12, 2017 18:28*

LOL. If you get an empty response ... that's probably me clicking through 'Next' without filling anything. :) It said it recorded it so ...


---
**Scorch Works** *September 12, 2017 18:30*

Moshi should be a seperate option. The list of boards I gave are the options in Laser Draw.  Moshidraw uses Moshi boards which do not work with Laser Draw.  



It would also be nice to have a text entry field for other stock boards.


---
**Don Kleinschnitz Jr.** *September 12, 2017 19:08*

**+Scorch Works** sorry but now I am confused as I do not use these controllers.



 Can you give me a list of K40 stock controllers and what software they use :).




---
**Scorch Works** *September 12, 2017 19:51*

**+Don Kleinschnitz** The best list I have is the one I posted (in your survey already).  Those boards work with Laser Draw. 



There are Moshi boards that only work with Moshidraw but I know nothing about them (I have no list.)








---
**Don Kleinschnitz Jr.** *September 12, 2017 21:27*

**+Scorch Works** Ok I think got it. I already have added moshi and Moshidraw so I think we are covered. 


---
**Scorch Works** *September 12, 2017 21:36*

**+Don Kleinschnitz** OK, Thanks for adding the list of stock boards.  I ofen wonder which ones are more common.


---
**HalfNormal** *September 12, 2017 23:53*

**+Don Kleinschnitz** Here is my 2 cents;



There are actually two stock controllers for the standard K40 which is the m2Nano by Lihuiyu Studio Labs and Moshi by Moshisoft.



The boards you list as Moshi Boards are m2Nano boards

Moshi Boards are the A board, A board 2, A board 3, MS10105, and C107 Controlboard


---
**Ashley M. Kirchner [Norym]** *September 13, 2017 00:21*

**+HalfNormal**, what's listed on the Firmware page is exactly what you described above, it's what is in the controller. The interface software page is also correct.


---
**HalfNormal** *September 13, 2017 00:26*

**+Ashley M. Kirchner** You are correct, the pages do reflect the correct information. Going to edit my post to remove the errors. Thanks for pointing them out. :-)


---
**BEN 3D** *September 13, 2017 06:20*

Hi I have the K40 with the orignal installed Board. M2Nano.

[plus.google.com - Start to set up a 40k China Laser engraving Machine.](https://plus.google.com/109140210290730241785/posts/7Hnf889VDeh)



Is that the Moshi Board Version 6C6879-LASER-M2 ? I did not have find this information on the Board, and had not powered my device on yet :-D, hopely I can finish my work in it this week.


---
**Scorch Works** *September 13, 2017 11:30*

**+BEN 3D** Yes, that is the M2 board.


---
**Scorch Works** *September 13, 2017 11:34*

**+Don Kleinschnitz** I think the list of boards you added for me is going to cause confusion and reduce the response rate.  I suggest you delete the page with the list of boards I provided.


---
**Don Kleinschnitz Jr.** *September 13, 2017 11:54*

**+Scorch Works** what were you going to use the information for?

The question on board # only will come up if stock board is checked... Perhaps I will just add a "I dont know" selection :).


---
**Scorch Works** *September 13, 2017 12:17*

**+Don Kleinschnitz** With additional work K40 Whisperer should work with all of the boards I listed (right now only M2 is supported).  I would potentially use the results to determine which boards to work on adding next.


---
**Don Kleinschnitz Jr.** *September 13, 2017 12:47*

**+Scorch Works** Ok I am going to leave it in as it is now set up, it probably won't disrupt the response rate and getting you the info you need would help the community ....


---
**BEN 3D** *September 13, 2017 12:54*

Thanks **+Scorch Works** really nice to know..., may you could give advice to the community how they can identify witch board they have.


---
**HalfNormal** *September 13, 2017 12:56*

Ben, if search, you will find a great wealth of information on how to identify your board.


---
**BEN 3D** *September 13, 2017 12:57*

ok


---
**Scorch Works** *September 13, 2017 13:43*

I am OK with the way it is but here is a possible change that could make it more clear.



What I really want is the "Mainboard" setting in LaserDraw.  So it would make sense if the title was "Mainboard Version" and the question was 

"What is your mainboard setting in LaserDraw?"

The question should only be asked if 'LaserDraw' or 'LaserDraw Plugin for Corel' was selected for interface software.



This way the options match something that can be looked up in the settings.

![images/3276403a1368627f4877452482a3ab6c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3276403a1368627f4877452482a3ab6c.png)


---
**Don Kleinschnitz Jr.** *September 14, 2017 03:58*

**+Scorch Works** added what you wanted.... I think. You definitely owe me :)!


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/afqTD1k5UXE) &mdash; content and formatting may not be reliable*
