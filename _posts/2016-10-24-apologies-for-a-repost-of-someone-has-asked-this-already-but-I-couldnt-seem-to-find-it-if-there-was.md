---
layout: post
title: "apologies for a repost of someone has asked this already, but I couldn't seem to find it if there was"
date: October 24, 2016 19:19
category: "Original software and hardware issues"
author: "Jesse Veltrop"
---
apologies for a repost of someone has asked this already, but I couldn't seem to find it if there was. My question for you all is I need help with the setup of my k40.  I don't know a whole lot about electrical things, so when I got the laser and saw that it has some sort of plug on the back labeled ground I got concerned.  I know that since there's water and electricity grounding should be vitally important. The instructions neglected to mention it, so I did a little googling but I can't get a clear answer, some people say run a wire from that plug to a copper rod in your backyard, others say that if you live in america the three prong power adapter plug it comes with in your house's wall outlet should be just fine.  So if any of you could shed a little light on what should be done to get my laser up and running it'd be much appreciated :)





**"Jesse Veltrop"**

---
---
**greg greene** *October 24, 2016 20:00*

I grounded mine - some of those who haven't have complained of shocks, some have not - It's best to do it to a separate ground rod - but for many they have to rely on the integrity of the house ground system.


---
**Jesse Veltrop** *October 24, 2016 20:48*

So do I simply run a copper wire from the ground joint in the back to a pipe/rod in my yard? do you have any recommendations, such as how long it should be or whether it has to be a rod or a pipe? also one other concern being I have heard of ground loops, I don't understand exactly what these are, but do you know if it is something I should worry about?


---
**greg greene** *October 24, 2016 21:06*

Most hardware places can sell you a ground rod - 6 to 8 foot long copper covered metal rod.  Some jurisdictions where the ground is hard use a plate rather than a rod but the purpose is the same - to provide the least resistant path to ground for anything grounded to it.  The idea is electricity follows the path of least resistance - so it will flow to the ground rod rather than through your body if something malfunctions.  The ground lug is connected to the rod via copper wire - 14 guage stranded is usual - you get it at the same place as the rod.  Your house uses ground rods - or an acceptable alternative as part of it's ground system.  However, each and every connection between you and that system must be in ideal shape for it to work.  Every connection has the potential for corrosion and getting lose over time - this introduce a little bit of resistance between the machine and the eventual ground.  Get enough of them and you become the path of least resistance.  For me I ground the machine via a 14 Gu copper ground wire to a 1 inch copper pipe that holds all the ground of my Amateur radio shack, and that pipe is then grounded to a rod outside.  I have never had a problem with any of my stuff using this setup.  A ground loop becomes possible when there is more than one ground point for the equipment to 'see' electrically, for example, that plug with a ground prong plugs into the house ground, if there is also another less resistant ground point in the vicinity and you happen to touch both that point and the equipment ground only through the house system, you then become a conduit to the better ground, any malfunctioning equipment that normally ground through the house system will discover you and you may get a shock.  Not really a likely scenario with most made in North America stuff - but these boxes are not the pinnacle of electrical engineering.  Sorry for the long winded reply, and I don't mean to scare you - but if you have the ability to use a ground rod  - do it - it can't hurt and it may keep you around to show us a whole bunch of wonderful lasered stuff !


---
**Jesse Veltrop** *October 24, 2016 21:17*

Thanks for the input! I haven't been able to get a good answer from anyone until now, so its just been sitting in my garage being useless. so whats a good method for attachment? just wrap the stranded wire around the top of the rod that's sticking out of the dirt a few times and tape it down with electrical tape?


---
*Imported from [Google+](https://plus.google.com/114760861883962577037/posts/gAy3RZkDM31) &mdash; content and formatting may not be reliable*
