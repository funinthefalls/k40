---
layout: post
title: "Using existing gas water heater or gas furnace external external venting for the laser fumes?"
date: March 20, 2018 12:48
category: "Discussion"
author: "Rob Mitchell"
---
Using existing gas water heater or gas furnace external external venting for the laser fumes?



I was wondering if anyone sees an issue with tapping into either the existing gas water heater or gas furnace external vents to extract my laser fumes. Instead of creating yet another vent to external.



I was thinking I could tie into one of those existing pipes using a Y-connector (not a T connector). I am concerned that I may get fumes coming back but think the angle of the y connector gives the fumes a better straight shot as apposed to using a t-connector which would give the fumes a 50/50  chance of going either way at the junction.



Thoughts?





**"Rob Mitchell"**

---
---
**Jim Hatch** *March 20, 2018 13:10*

Well first it's against the building code. Especially since it's under positive pressure you're going to disrupt the airflow characteristics of the appliance flues which is likely to cause combustion & operating issues as well.


---
**Don Kleinschnitz Jr.** *March 20, 2018 13:30*

That solution sure would be tempting :(....but:



As **+Jim Hatch** suggests you are going to be blowing air and fumes into the output of whatever the vent is connected to. I would not trust that the fumes will all go one direction or the other just due to a Y vs T. I would guess that it will flow both directions but toward the lowest resistance which might be the furnace VS a longer stack pipe (through the roof). It seems possible that you will blow stink and potentially combustible materials into your appliance. Consider that if this happens while they are off when they come on there could be an explosion. 



If you have an ugly event your insurance will not cover you with a modification like this ...




---
**Rob Mitchell** *March 20, 2018 18:49*

Well that is valuable feedback and based on it I will not be routing my venting way. It's either going to be direct vent to external or a stand alone charcoal filter based DIY internal venting approach.


---
*Imported from [Google+](https://plus.google.com/108013641525868982855/posts/BTH5CMcFDa6) &mdash; content and formatting may not be reliable*
