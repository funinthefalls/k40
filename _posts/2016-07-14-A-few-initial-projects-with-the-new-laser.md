---
layout: post
title: "A few initial projects with the new laser"
date: July 14, 2016 23:25
category: "Object produced with laser"
author: "Jeremy Hill"
---
A few initial projects with the new laser.  What an adventure! - mirrors, slate, and wood to practice with have made some nice simple gifts



![images/901a505f8263402aed58dba1ccb91e21.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/901a505f8263402aed58dba1ccb91e21.jpeg)
![images/3e787744d157c595c2c6588653a4cea5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3e787744d157c595c2c6588653a4cea5.jpeg)
![images/5329adf0d7d08de5037820e478e8f1a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5329adf0d7d08de5037820e478e8f1a7.jpeg)
![images/a3ff0ff6e1ba888b85f2f243fe027774.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3ff0ff6e1ba888b85f2f243fe027774.jpeg)
![images/f9d99324353c08782093ad22c4ce333b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9d99324353c08782093ad22c4ce333b.jpeg)
![images/f4ab3830784bf2a577c17fece81fd385.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4ab3830784bf2a577c17fece81fd385.jpeg)
![images/4e8fa46035331c4295254fbec4f1f07e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e8fa46035331c4295254fbec4f1f07e.jpeg)

**"Jeremy Hill"**

---
---
**Anthony Bolgar** *July 15, 2016 00:07*

Welcome to the addiction!


---
**Jeremy Hill** *July 15, 2016 00:09*

Thanks.  Between this and the X-carve, I have been able to make a lot of gifts for others.  Someday I may have to consider selling items, but not yet.


---
**HalfNormal** *July 15, 2016 03:08*

**+Jeremy Hill** Now I know where I  have seen you! You also hang out on the Inventables site! :-)


---
**Jeremy Hill** *July 15, 2016 12:11*

HalfNormal- you were the one who convinced me to check out this google+ community.  It was a great recommendation!


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/7ah1FdUY5Q3) &mdash; content and formatting may not be reliable*
