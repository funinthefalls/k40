---
layout: post
title: "A test with some different speeds and powers....amazing how this turned out at 500mm/s at 20% power which was 5 mA's"
date: April 02, 2016 17:11
category: "Object produced with laser"
author: "Scott Thorne"
---
A test with some different speeds and powers....amazing how this turned out at 500mm/s at 20% power which was 5 mA's.

![images/1876014ebb4d394de0f2de2e0664ee3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1876014ebb4d394de0f2de2e0664ee3a.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 17:24*

Looks good, but is it supposed to move like that?


---
**Gee Willikers** *April 02, 2016 17:30*

Stock machine or DSP upgrade? I have a DSP but when I tried over 300 mm/s it was a position losing motor skipping disaster. I'll have to try again.


---
**Scott Thorne** *April 02, 2016 17:44*

Dsp...but stock in my machine....**+Yuusuf Sallahuddin**....what are you referring to? 


---
**ED Carty** *April 02, 2016 19:07*

**+Scott Thorne** I think he means the design.. kinda messes with the eye balls. :) looks good my friend


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 20:53*

**+Scott Thorne** Yeah was joking cos it looks like the design is swirling around when I stare at it. But looks good though. Nice result on the engrave.


---
**Scott Thorne** *April 02, 2016 20:56*

**+Yuusuf Sallahuddin**...

Thanks my friend....I still have so much to learn...lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 21:03*

**+Scott Thorne** It's a fun learning process all this lasing. I'm thoroughly enjoying it myself, even when I make mistakes. You've been pumping out some pretty cool pieces, so I'd say you're doing pretty good.


---
**Scott Thorne** *April 02, 2016 22:12*

**+Yuusuf Sallahuddin**...thanks man....I try to spend at least an hour a day in my garage playing with the engraver.


---
**ED Carty** *April 02, 2016 23:40*

**+Scott Thorne** Im jealous

You produce some wonderful objects


---
**Scott Thorne** *April 02, 2016 23:42*

**+ED Carty**....thanks bro....I'm just in the learning stage Ed....but I'm slowly making progress.


---
**Scott Thorne** *April 02, 2016 23:45*

One thing I was trying to point out was how small this actually was...it's only 3 inches squared.


---
**HP Persson** *April 02, 2016 23:46*

Awsome, you should add some spray laquer to it and frame it as a painting ;)


---
**Scott Thorne** *April 02, 2016 23:48*

It still amazes me how detailed these things can be....**+HP Persson**...thanks man...that's a good idea.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 23:55*

**+Scott Thorne** 3 inches square for that entire image? Well that is even more impressive. Such detail in such small size.


---
**ED Carty** *April 02, 2016 23:59*

**+Scott Thorne** [https://www.epiloglaser.com/resources/sample-club.htm](https://www.epiloglaser.com/resources/sample-club.htm) 

take a look at these projects.


---
**Scott Thorne** *April 03, 2016 00:09*

**+ED Carty**...sweet


---
**Scott Thorne** *April 03, 2016 00:11*

**+ED Carty**...I wish I could take credit for some of the designs that I post but alas...they are just things that I've tweaked off of the internet....I download stuff and tweak the crap out of stuff...lol


---
**Scott Thorne** *April 03, 2016 00:16*

**+Yuusuf Sallahuddin**...I posted a different pic...check it out.


---
**ED Carty** *April 03, 2016 00:31*

tweaking is what it is all about. that is how you get started in anything. You are fast on your way Scott. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/XBZ8x4kzHvt) &mdash; content and formatting may not be reliable*
