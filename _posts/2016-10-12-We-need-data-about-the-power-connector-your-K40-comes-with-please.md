---
layout: post
title: "We need data about the power connector your K40 comes with please!"
date: October 12, 2016 18:42
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
We need data about the power connector your K40 comes with please! 

And please tell us whether that is on the JST or Screw Terminal PSU type and whether you have a ribbon cable. 



<b>Originally shared by Carl Fisher</b>



I was talking with **+Ray Kholodovsky** about the design of the daughter board connector that takes the main PSU power feed on the C3D boards. Specifically around the plug used by the various power supplies.



It appears that mine is somewhat backwards from his but I'm wondering if there are even more options out there. Attached is a picture of mine which as you can see doesn't fit the board connector properly, but does work while his fits properly and appears to be reversed from mine.



Thoughts?



![images/b173b67d326f26cba1149d252e385497.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b173b67d326f26cba1149d252e385497.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Don Kleinschnitz Jr.** *October 12, 2016 19:37*

My LPS has this connector for the DC. 

-Conn Recept 4 pos W/RAMP SL-156

-TE ConnectivityAMP

-Amp PN: 640250-4

-Digikey PN: A19952-ND

-Pins for above: A19991CT-ND



There are no ribbon cables on my LPS

You can see my LPS in this picture.

The connector above is the one on the right in this picture.

Ignore my wiring as this is my smoothie conversion not a stock K40.

![images/fb4ebdea9a9d312c51d20816c3cfcec0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb4ebdea9a9d312c51d20816c3cfcec0.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 13, 2016 16:00*

Hey Ray. My PSU & m2Nano images are here: [https://spaces.google.com/space/379777357/post/UgwsKCMfNSpSHQgH5Bt4AaABBw](https://spaces.google.com/space/379777357/post/UgwsKCMfNSpSHQgH5Bt4AaABBw)



Looks like mine are the screw-type plugs on the PSU & I did have a ribbon cable to the m2nano.


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2016 16:18*

**+Yuusuf Sallahuddin** The spaces link didn't work :(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 13, 2016 16:20*

**+Ray Kholodovsky** Ah, I'll manually share the pics. I'll find them in my G Drive/Photos album stuff. Bear with me.



edit: or alternatively you can go directly to the spaces place ([https://spaces.google.com/space/379777357](https://spaces.google.com/space/379777357)) & it's the 2nd post (at the bottom of the lot).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 13, 2016 16:23*

![images/093d8482be0164df78ab022ccc6cd9a0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/093d8482be0164df78ab022ccc6cd9a0.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 13, 2016 16:27*

This was the power cable end attachment that my K40 PSU had. It seems the same as the OP plug (with the little triangle notch).

![images/808d5cd251d7eda0815b70b0d8af6c62.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/808d5cd251d7eda0815b70b0d8af6c62.jpeg)


---
**Ulf Stahmer** *October 14, 2016 12:17*

Mine looks just like **+Don Kleinschnitz**'s (minus all his added in wiring :) )


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/4BGpv7S8SFU) &mdash; content and formatting may not be reliable*
