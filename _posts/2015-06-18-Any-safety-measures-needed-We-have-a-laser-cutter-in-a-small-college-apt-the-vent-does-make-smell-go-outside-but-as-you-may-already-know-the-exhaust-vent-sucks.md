---
layout: post
title: "Any safety measures needed? We have a laser cutter in a small college apt- the vent does make smell go outside but as you may already know the exhaust vent sucks!"
date: June 18, 2015 16:14
category: "Discussion"
author: "jamie gomez"
---
Any safety measures needed? We have a laser cutter in a small college apt- the vent does make smell go outside but as you may already know the exhaust vent sucks! we are using it daily for prototyping 





What are long term risk? Smell, Vision, health? Can you look at it while its cutting? I mean not watch the whole processed but I do take a 30 second look - I feel it might be straining my eyes because I swear glasses? I literally sit next to it while its cutting - basically 20 inches away 





**"jamie gomez"**

---
---
**Jim Root** *June 20, 2015 03:32*

If you are going to watch it cut for any length of time I'd recommend getting safety glasses/goggles designed for the type of laser beam that it uses. I got mine from eBay for about $30. They are glasses but they fit over my regular glasses well enough for the occasional peak. As for venting get as much of it out as you can. If you can smell it it's in the air.


---
*Imported from [Google+](https://plus.google.com/114723275374564353861/posts/bzLzno3XiQg) &mdash; content and formatting may not be reliable*
