---
layout: post
title: "I got my K40 last week, and already decided that the included software sucks, so I am trying to use Gimp => Inkscape => k40 Whisperer"
date: November 01, 2017 03:10
category: "Discussion"
author: "Arjen Jonkhart"
---
I got my K40 last week, and already decided that the included software sucks, so I am trying to use Gimp => Inkscape => k40 Whisperer. It seemed to go alright, but then the timeouts started, so I probably need some ideas here. See picture of timeout error. 



Picture details: 127x127mm in 141dpi , SVG, 113Kb

Whisper: Raster Engrave, 150 mm/s

K40 laser: 40%, USB connected with libusb-win32 driver v1.2.6.0

Media: mirror glass (soapy paper)

Computer: high end i7, 32 Gb 

Resources: ~0.1% CPU, 277 Mb MEM, 0 Disk



I quit watching youtube after the first time this happened :(, close everything down the second time, and increase priority to "real time" the third time.



Then I sat down and quit wasting mirror - and wrote a post. Any ideas?



(Fiona is on the left, Leo on the right - both Weimaraners) 

![images/2efce578deeedc34ebbcfaa5270e6270.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2efce578deeedc34ebbcfaa5270e6270.jpeg)



**"Arjen Jonkhart"**

---
---
**Anthony Bolgar** *November 01, 2017 04:12*

Try using a different USB cable, the blue ones that come with the K40 are known to have issues. Hope this helps.




---
**Printin Addiction** *November 01, 2017 10:19*

Make sure the head is not still moving from the last command before you hit another button, that will definitely cause this to happen. I think the K40 is not looking for input while it is moving the head, the software doesnt get a resonse back from the controller so it retries, then they get out of sync.



Plus you should set the halftone setting for raster graphics to get a better greyscale image.


---
**Printin Addiction** *November 01, 2017 12:20*

I think the above comment (clicking while head is moving) is only applicable when performing an raster/vector engrave or vector cut, I don't notice it when the head is moving via the move buttons, and I know I click those very quickly.


---
**Scorch Works** *November 01, 2017 13:38*

Did K40 Whiperer stop running do to too many timeouts or did you stop it by clicking the Stop button? 

 

Timeouts generally will not affect the output because K40 Whisperer just resends the data that failed to send an moves on.  However, if there are too many timeouts on a single packet of data the run will be terminated by K40 Whisperer.


---
**William Heston** *November 01, 2017 14:18*

I've had the same problem on a raster job, only it started with the very first packet: the head never even started moving, it just started counting timeouts. I've only been able to do one small test raster job, the larger one I actually want to do times out every time. I've also had it just stop doing anything when it's supposed to start sending packets--no message on the status bar, no head movement, no activity. I thought it was too little power to the laser, as that seems to kill it mid-job sometimes, but I don't think that's the issue in these cases.


---
**Scorch Works** *November 01, 2017 15:24*

I would try Anthony's suggestion of trying a different USB cable and make sure both ends of the USB cable are plugged all the way in.  Testing with a different computer would also be useful.


---
**Scorch Works** *November 01, 2017 19:26*

**+Arjen Jonkhart**​

**+William Heston**​ 



1. Do you have a USB hub between your computer and laser?



2. Does everything work for long jobs in LaserDRW?




---
**William Heston** *November 01, 2017 23:21*

No hub. I've never even tried LaserDRW, used K40 Whisperer from day one.


---
**Scorch Works** *November 02, 2017 00:26*

**+William Heston**​ was it a one time occurrence or is it a regular problem?  


---
**William Heston** *November 02, 2017 00:41*

Regular, on raster jobs over a certain size. I'll have to get back to you on exactly what size they start to fail.


---
**Scorch Works** *November 02, 2017 01:04*

The next version of K40 Whisperer will handle communication with the K40 differently. It should greatly reduce the number of timeouts.  I am not sure if it will solve this problem because I am not sure why you are having the problem while others are not.

 


---
**Scorch Works** *November 02, 2017 01:11*

If you guys don't mind I would appreciate each of you to saving a settings file from K40 Whisperer ('File'-'Save Settings File'). Then e-mail it to the e-mail adress in the help menu.


---
**Arjen Jonkhart** *November 02, 2017 04:28*

Settings file sent. 



Strange, I don't see my comment from this afternoon? Anyway, I tried another USB cable, but that didn't help. I did use that file for the mirror on some wood, and was able to burn that successfully 6 times in a row. However, the seventh time, when I increased to 50% it failed again. It is usually around the same place as well, although not exactly. The picture is about 4x4, and it fails before it hits 1" into it. I definitely have a much higher occurrence of failure with higher power settings. It makes me wonder about the power supply? Or perhaps the drivers on the motherboard. Could it be related to loss of power?



I got an analog volt meter, and an analog ammeter coming soon, so perhaps that will start telling a tale as well.



Oh yeah, I did notice on a vector file I ran tonight that the timeouts happen quite often. So, when it uploads the packets, it times out 2-5 times before it finishes - but it stayed ahead of the cut, so the job finished.



Good night - thanks for everybody's input.


---
**Scorch Works** *November 02, 2017 14:20*

The change it the result depending on the power setting does indicate that there may be some problem in the machine.  



You can adjust the allowable number of timeouts in the K40 Whisperer settings. Increasing the timouts may just hide the problem without fixing the underlying issue.


---
*Imported from [Google+](https://plus.google.com/109364066707746072621/posts/Vdqiu6M6C7x) &mdash; content and formatting may not be reliable*
