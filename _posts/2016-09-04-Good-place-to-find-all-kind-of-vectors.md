---
layout: post
title: "Good place to find all kind of vectors"
date: September 04, 2016 22:06
category: "Repository and designs"
author: "Ariel Yahni (UniKpty)"
---
Good place to find all kind of vectors

[http://www.freepik.com/](http://www.freepik.com/)





**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 04, 2016 23:12*

Thanks, will check it out later :)


---
**ThantiK** *September 04, 2016 23:44*

[http://kenney.nl/assets](http://kenney.nl/assets) -- also a lot of awesome CC0 assets for games.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/foQHSP3y8bg) &mdash; content and formatting may not be reliable*
