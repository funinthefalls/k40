---
layout: post
title: "Still looking for the best solution available to update a K40"
date: December 16, 2014 14:27
category: "Hardware and Laser settings"
author: "Stephane Buisson"
---
Still looking for the best solution available to update a K40.



[http://smoothieware.org/smoothieboard](http://smoothieware.org/smoothieboard)

If hardware is one thing, what about matching software ?



+Thomas Sanladerer , +Arthur Wolf





**"Stephane Buisson"**

---
---
**Arthur Wolf** *December 16, 2014 16:22*

About software for a laser cutter, you pretty much treat it like a CNC mill with a very thin tool, so any CNC mill/router G-code generator will work fine. Open-Source options include Freecad, Heekscad, Gcodetools. I personally use Cambam ( it's cheap but not free ).

Then once you have your G-code, to control it it's the same as with a 3D printer : Pronterface, Octoprint, etc ...

If you need to generate G-code for engraving you can use this : [http://fablabo.net/wiki/Raster2Gcode](http://fablabo.net/wiki/Raster2Gcode)


---
**Arthur Wolf** *December 16, 2014 16:24*

Oh and this too : [http://fablabo.net/wiki/Laserengraver](http://fablabo.net/wiki/Laserengraver)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/geanwqnEQEg) &mdash; content and formatting may not be reliable*
