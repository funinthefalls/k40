---
layout: post
title: "PWM and pinout. I did read a thousand times, but I cannot grasp it.sorry, just to be sure: I've got a GGR PSU"
date: March 01, 2017 10:11
category: "Modification"
author: "Jorge Robles"
---
PWM and pinout.



I did read a thousand times, but I cannot grasp it.sorry, just to be sure:



I've got a GGR PSU. I will use rightmost L to fire pwm laser, and keeping the POT.



I use a Level shifter.



As I understand, as L is active Low, does my pin need to be PWM inverted?









**"Jorge Robles"**

---
---
**Don Kleinschnitz Jr.** *March 01, 2017 14:14*

What controller are you using (need to know the source of the PWM)?

Generally:

Yes connect an open drain to the L on the power supply.

Keep the pot.

DO NOT use a level shifter

If you use the open drain you do NOT invert PWM.

What config pin are you using.



Picture of your LPS and controller would help.



[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)




---
**Jorge Robles** *March 01, 2017 14:33*

**+Don Kleinschnitz** I've sent you a pm


---
**Don Kleinschnitz Jr.** *March 01, 2017 14:52*

**+Jorge Robles** got it, need a schematic for that board.


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/5kJhCnB3YnR) &mdash; content and formatting may not be reliable*
