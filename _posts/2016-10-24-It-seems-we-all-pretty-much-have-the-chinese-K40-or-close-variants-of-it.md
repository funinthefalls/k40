---
layout: post
title: "It seems we all pretty much have the chinese K40 or close variants of it"
date: October 24, 2016 12:24
category: "Hardware and Laser settings"
author: "Bob Damato"
---
It seems we all pretty much have the chinese K40 or close variants of it. I am putting together a spreadsheet of power settings to keep track of what works and what doesnt work. To properly cut plexiglass I went through a good amount of it and made a nice little stack of scrap material before getting it right. 



I will post it when I get some good content on it, right now I only have 3 or 4 entries.. Meanwhile, if anyone has anything they would like to contribute to it, that would be great!  



Essentially it has: (for power, I read it in volts since I put a volt meter on mine across the POT)



1/8" Plexi          Speed 10mm/sec     Power .61v      Cut    

1/4" Maple        Speed 200                 Power .83v       Engrave





etc etc etc.

Bob







**"Bob Damato"**

---
---
**Stephane Buisson** *October 24, 2016 14:54*

bob, settings are so dependant of

1) your alignement settings

2) your consumable (lens, mirrors)

3) how old is your tube (wearing)



please note it's barely 2 k40 the same.

you will find out yourself if you change your tube.

(2 supposed 40W tube are not the same) 


---
**Bob Damato** *October 24, 2016 17:24*

That figures!




---
**Eric Rihm** *October 24, 2016 18:05*

Having a reference point to start from is convenient, even if it won't be completely accurate for other people's machines


---
**Chris B** *October 28, 2016 02:07*

Yeah, being able to reference different materials comparatively is likely to be helpful, just as long as people know these are just estimates; it's useful to know that material X is going to need Y% more power than material Z to cut through.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/dUSCBYZghgW) &mdash; content and formatting may not be reliable*
