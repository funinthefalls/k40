---
layout: post
title: "Hey all, Finally got the laser probe, looks like my tube has snuffed it"
date: May 07, 2016 03:13
category: "Hardware and Laser settings"
author: "I Laser"
---
Hey all,



Finally got the laser probe, looks like my tube has snuffed it. 10mA reads 2-3 watts on problem machine and 19-20 watts on the other. Now before I pull the trigger on a new tube I want to make sure there's nothing I'm overlooking!



The mA meter seems pretty stable on the questionable machine, though I've noticed it occasionally jitters slightly (nothing more than 1mA).



The tube lights up fine, although I notice it arcs on top of the metal shield occasionally (inside the tube) whereas the other machine doesn't exhibit this behaviour. Haven't been able to tell, but I'm wondering if the slight drop in mA is caused by this.



Anyway anything else that could cause this other than the magic gas has dispersed?



Here's the replacement I'm looking at:



[http://bit.ly/1SZEerd](http://bit.ly/1SZEerd)



Cheers :D





**"I Laser"**

---
---
**Jim Hatch** *May 07, 2016 03:32*

Sounds like it's toasted.



Did you get the power probe with the printed directions listing one time for the test and the face of the probe with a different time? Mine is 40.8 seconds in the directions and 30 seconds on the face. 



Time matters for precision but not relevant for you - as long as you measured each one for the same amount of time and you allowed it to cool between measurements. It's the big delta you're showing that's the tip off (& the arcing).



Measure your existing tube to make sure the new one will fit. Most of what we get as 40W tubes are actually only 30W without overdriving them - they're not long enough. That's why the bigger wattages have wider machines or an extension off the side. 


---
**Jim Hatch** *May 07, 2016 03:33*

BTW, your good tube is measuring pretty close to what mine does at that power level.


---
**I Laser** *May 07, 2016 03:52*

Yeah, the meter face itself states 30 secs, on the back it has 38 secs engraved into it and the reference material has a sticker over the printed 30 secs stating 38 secs. Obviously they test them and change the time accordingly, real high tech lol.



I let it cool off, well dunked into a glass of room temp water and made sure it was stable. Tried it twice on both machines with the same results.



Thanks I'll measure the tube. I'm asking if they are insuring it as well, due to the horror stories I've read. Not that I want another machine but the price point of $285 for a tube or $400 for another machine does make me wonder.



Hoping the tube is fresh as stated in the advert and will last a reasonable amount of time...



edit: glad it measures similar to yours :)

edit2: existing tube is 70cm so unless someone else pipes in with an alternative theory I'll buy a tube.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 06:58*

Could the wattage power drop on that specific tube be related to the power supply? Just thought I'd pipe in since it popped in my head. If that's possible, might be worth a test with the other unit's power supply (unless you've already done it).


---
**I Laser** *May 07, 2016 07:08*

I wondered that too initially. But the mA meter reads fine, it responds to increases/decreases and is pretty stable except the occasional slight hiccup.



I'm not an electronics expert so if someone else thinks the PSU could be an issue happy to hear your input!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 07:11*

**+I Laser** Yeah, I've got no real idea, as I'm not very knowledgeable regarding electronics. Was just a possible thought. Would be interesting to know other's opinion on if the PSU could be the issue.


---
**I Laser** *May 07, 2016 07:24*

Me either lol, hence happy for someone more knowledgable to pipe in. 



I'd prefer not to swap PSU's over, happy to whack a multimeter in there, but that seems a bit redundant to me as the mA meter is responding as expected.



Appreciate the input. :)


---
**Phillip Conroy** *May 07, 2016 08:42*

I hope you are wearing laser safety glasses when you are looking at the laser tube firing...............Is the 10ma in the same spot it allways has been?.

It is not ezey to measure power supply out put to the tube as is is 22,000 volts and without the right gear and training do not even try.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 09:22*

**+Phillip Conroy** I've been looking via webcam, so it doesn't go in my eyes.


---
**I Laser** *May 07, 2016 09:44*

I wear glasses and have read they will stop a laser at this wave length. To be honest I rarely even watch the tube when firing. ;)﻿



Hard to tell regarding the 10mA being in the same position as I recently replaced the pot. The pot goes up to 999, at 350 the laser is pushing 10mA. The pot being variable resister makes me think the PSU is working as intended.﻿


---
**Scott Thorne** *May 07, 2016 10:49*

For the record...watching the tube fire without safety glasses is harmless to your eyes....the beam being reflected of off something and melting your eyeball is the reason for the glasses!


---
**Jim Hatch** *May 07, 2016 12:42*

**+Scott Thorne**​ true enough except if it's cutting the beam is making it through the material and will hit whatever is underneath. If it's wood or aluminum you're good (beam is absorbed) but if it's steel you have the possibility of sharded reflections. Not really likely it'll get you or that the beam will be cohesive enough to cause damage but glasses are cheap insurance :-)


---
**Scott Thorne** *May 07, 2016 13:48*

 That was the point...people see the laser radiation sticker and assume that it puts out radiation from everywhere at the tube....not true...just the output optics.


---
**Dennis Fuente** *May 07, 2016 21:52*

reading the above post so my 40 Watt is actually 30 watt maybe this is why i can't cut through 3 MM ply i also read in the post from the manufacture of the above tube on ebay the life of the tube decrease with the shelf time  so if i understand this the tube is losing power all the time it's sets there HUH


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 22:25*

**+Dennis Fuente** Yeah I recall Mishko mentioning that the tube loses gas over time, regardless of whether it is in use. So you may as well use it. I don't think the 30W as opposed to 40W is the issue for your cutting through 3mm ply, as I am still using the stock tube (which would also be 30W instead of 40W) & I can get through 3mm ply/mdf with power 10mA & speeds around 25mm/s @ 2 passes. Something else has to be causing your issue (alignment, lens focus depth, or possibly your tube is faulty).


---
**Dennis Fuente** *May 07, 2016 22:34*

Yuusuf Well for sure something is amiss i have worked for hours trying to make this thing cut 3mm ply going over and over the mirrors and lens alignment replaced the mirrors and then replaced the lens and head assembly installed airassit so whats left the power supply is firing the tube so i don't think this is the problem when the machine arrived it had lost's of dust on it inside so if the tube loses gas and power maybe mine was bad from the start don't know as this is my first and only machine i don't have a power meter to measure it with, not going to go any further until i get thsi cutting thing figured out


---
**I Laser** *May 08, 2016 00:09*

Apparently ebay seller will replace tube if it arrives broken so going to pull the trigger on a new one.



I've read you don't solder the wire on, you wrap it around the connectors and silicone it in place.



Stupid question, do I have to use a specific silicone? Wasn't sure if different types of silicone can be more conductive. I have a tube of this [http://bit.ly/1rEgsrC](http://bit.ly/1rEgsrC) that I'm thinking of using.


---
**Dennis Fuente** *May 08, 2016 01:12*

What i found with my tube when it was assembled they injected the silicone around the wire and into the tubing around the connection but what occured is the silicone wicks through the wiring and insulates it from the tube wire what i did was to tin the wire and wrap it like a coil spring around the wire from the tube then coat it with silicone the stuff the machine comes with is fast drying next slip the tubbing over the wire connection and then inject more silicone into the tubing worked no arching and the wire is against the housing as i had to turn the tube to get the bubbles to go out


---
**I Laser** *May 08, 2016 07:32*

Ah cool, unfortunately I lost the tube of silicone that came with the first machine and the second machine didn't come with any. :\


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/Bq4nqD6ZWNK) &mdash; content and formatting may not be reliable*
