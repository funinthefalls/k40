---
layout: post
title: "My K40 is setting in my door step can't wait to get home and get it hooked up"
date: February 02, 2016 19:44
category: "Discussion"
author: "3D Laser"
---
My K40 is setting in my door step can't wait to get home and get it hooked up





**"3D Laser"**

---
---
**Anthony Bolgar** *February 02, 2016 20:26*

Have fun!!


---
**Jim Hatch** *February 02, 2016 21:55*

...and so begins your journey to the dark side of obsession...that way lies the modding and upgrade path }:-)


---
**3D Laser** *February 02, 2016 22:08*

**+Jim Hatch** someone gave me a free ramps board to start the journey but I'm scared to death to hook it up


---
**Jim Hatch** *February 02, 2016 22:25*

:-) get it working vanilla first so you know it's starting out working right. Then just do one mod at a time so if you run into a problem you won't be trying to trace it amongst multiple possible failure points.



I'm in the middle of my upgrade to the Lightobjects focusing head before swapping out the control board. The new head requires a more precise laser/mirror alignment then the stock. Turns out my laser was focusing somewhat to the rear of the head towards the back of the machine. I never really noticed it much because there's nothing between the lens and the material so if the beam pathway isn't 90 degrees to the head it's still good. 



With the Laser objects head there's about an inch of a conically shaped fitting below the lens itself to handle the air assist. That means a beam coming out of the lens at an angle hits the inside of the "nose cone" and never reaches the material. 



I've got some mirror alignment in front of me :-) Would have driven me nuts if I had swapped the controller at the same time & suddenly didn't get a laser hit on the bed.


---
**Anthony Bolgar** *February 02, 2016 22:27*

I am having the same problem with the LightObject air assist head. I was thinking that my tube had fried, but now hearing what you have as a problem, that sounds more likely to me as my K40 is only a few months old.


---
**Jim Hatch** *February 02, 2016 22:33*

**+Anthony Bolgar**​ Easy to check. Just unscrew the bottom cone section and hit the test button. If it's working but misaligned you'll see it hit your bed (material on the bed) somewhere. In my case I put a piece of a post-it in front of the mirror & got the burn mark pretty much centered so I knew the laser was working. With the nose cone on I'd hear a buzz but didn't get a hit on the material. With it off it hits behind the head a couple of inches back.


---
**3D Laser** *February 02, 2016 22:47*

I think the air assist will be my first upgrade.  Btw did you get a new lens or did you modify the 12mm on that came with it to fit?


---
**Anthony Bolgar** *February 02, 2016 23:41*

I got a new 18mm lens


---
**Jim Hatch** *February 03, 2016 01:41*

**+Anthony Bolgar**​ me too. Also got a new gold mirror. So technically it's 3 upgrades (bigger lens, better mirror and air assist) in one :-)


---
**3D Laser** *February 03, 2016 03:56*

So after getting it out of the box I can't find the right size Allen wrench to open it up its driving me crazy


---
**Jim Hatch** *February 03, 2016 04:25*

**+Corey Budwine**​ :-) Try a small slotted/straight (not Phillips) screwdriver. You might have one that fits. The screws aren't wicked tight. 


---
**Ben Walker** *February 03, 2016 10:59*

Awesome news Corey.  Glad you are ramping up your system.   The air assist is amazing for sure.   I printed my own while I wait for the light object.   I am going through them like mad.   The hit the exhaust port (it's coming out today)  and when it hits it goes out of alignment and catches fire.   Not good considering the stock exhaust is so puny.  I came up with a mod to my mod.   I had a cheap hobby knife like an xacto.   well the barrel was the perfect size for the tubing to go in so that is now mounted to my head.   It's aluminum so no more God knows what it smoldering.   

You need to call off sick a few days.   You are going to have a blast.   Enjoy 


---
**3D Laser** *February 04, 2016 02:44*

So after fiddling with my laser for an hour trying to get power supply compartment I finally had enough and drilled out the set screw when they put it in they stripped it.  I still haven't fired it and already beat the machine up 😂


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/Lwyg4mipb2J) &mdash; content and formatting may not be reliable*
