---
layout: post
title: "Finally jumped in and bought a k40"
date: April 13, 2016 02:16
category: "Hardware and Laser settings"
author: "andy creighton"
---
Finally jumped in and bought a k40.  Has anyone actually had trouble from running at full power?   I have 2 fslaser at school with the same tube that we run full power all the time without any issues. 





**"andy creighton"**

---
---
**3D Laser** *April 13, 2016 02:54*

Full power killed my tube fast just be careful 


---
**I Laser** *April 13, 2016 06:27*

You shouldn't run the tube over 16mA (I keep mine below 10). Also make sure water is running, no air bubbles in tube and water temps stay below 25c.


---
**andy creighton** *April 13, 2016 10:10*

Thanks.  I'll keep it low.   I've only run the laser for about 10min so far (not full power)  but it seemed pretty week at 15ma.


---
**I Laser** *April 13, 2016 11:15*

Check your alignment, there's a link floating around here that explains this (it's linked below in Papi Chulo's thread). Clean mirrors and lens too.



Also make sure your material is at the right focal length (distance) from the laser head.


---
**andy creighton** *April 14, 2016 02:00*

I cut the gantry focus tool from thingiverse tonight and discovered that my clamp bed is way below focal range.  I shimmed some 1/8" acrylic up above the clamp and it cut clean with two passes at 10mm feed and 15ma power.


---
**Anthony Bolgar** *April 14, 2016 02:49*

Do you have the Thingiverse link for the focus tool?


---
**andy creighton** *April 21, 2016 19:25*

[http://www.thingiverse.com/thing:1482364](http://www.thingiverse.com/thing:1482364)


---
*Imported from [Google+](https://plus.google.com/111039030319838939279/posts/cqXnyhcaLMH) &mdash; content and formatting may not be reliable*
