---
layout: post
title: "What do you do with an extra Middleman board?"
date: October 10, 2016 13:47
category: "Modification"
author: "Don Kleinschnitz Jr."
---
What do you do with an extra Middleman board? 



Build an optical end stop tester .... of course !



Lately I have been tinkering with end stops more than anyone should and while looking in my Smoothie conversion parts bin it occurred to me that I could use my extra Middleman board and connector to make an endstop tester.



Done .....



Schematic is here with use instructions: 

[http://www.digikey.com/schemeit/project/k40-endstop-tester-L91LPBG2001G/](http://www.digikey.com/schemeit/project/k40-endstop-tester-L91LPBG2001G/)



Package is here, nothing impressive. It uses 2x 2016 button batteries and holder. : 

[https://goo.gl/photos/TatG7gw7LjWsMtgSA](https://goo.gl/photos/TatG7gw7LjWsMtgSA)



Video is here: 

[https://goo.gl/photos/MDxDGBuseskPnTGS9](https://goo.gl/photos/MDxDGBuseskPnTGS9)



Note: if you don't have a Middleman board you can make one of these using a FPC connector whose pins are soldered to the battery, switches and LED's see the schematic. 





**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 10, 2016 15:21*

That's pretty handy. I'm almost thinking after seeing this that it would be useful to permanently be able to see the end-stop "state" (via LED indicators such as in your video).


---
**Don Kleinschnitz Jr.** *October 10, 2016 15:26*

**+Yuusuf Sallahuddin** you could easily add LED's to the operating Middleman board.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 10, 2016 15:36*

**+Don Kleinschnitz** I'm not using a middleman myself, but Scott's ACR kit. I will see what Scott thinks about adding the ability in his next revisions/upgrades to the design of his ACR.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/MkSQRatqqG9) &mdash; content and formatting may not be reliable*
