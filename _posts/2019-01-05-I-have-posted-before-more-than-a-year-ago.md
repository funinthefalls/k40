---
layout: post
title: "I have posted before more than a year ago"
date: January 05, 2019 04:13
category: "Original software and hardware issues"
author: "krystle phillips"
---
I have posted before more than a year ago.

My K40 has been gathering dust. I became frustrated when I couldn't get it to work with Windows 10 and then when I finally got a computer with Windows 7 calibrating it proved to be a magnanimous task.



My question is...

Has a method been figured out to operate the machine with Windows 10

Also is there someone offering guidance for first time use?





**"krystle phillips"**

---
---
**Ned Hill** *January 05, 2019 04:36*

What software have you been trying to use to control the laser?  If it's the stock coreldraw with corellaser, it doesn't play nice with windows 10.  To use corellaser it really requires an up to date version of CorelDraw like X8.  



You would be better off using the free K40 whisperer program in conjunction with inkscape.   [scorchworks.com - K40 Whisperer](https://www.scorchworks.com/K40whisperer/k40whisperer.html)



Lightburn is a better program, but it requires upgrading to a different control board like the Cohesion3D.



First time user questions are always welcome and we try and help as best we can with advice.


---
**Adrian Godwin** *January 05, 2019 12:02*

It would be ideal if someone on this list with their own K40 could help you, but if there's nobody local you should try a hackspace - they're all over the world and nearly always have a laser cutter and people familiar with it.



[wiki.hackerspaces.org - List of Hacker Spaces - HackerspaceWiki](https://wiki.hackerspaces.org/List_of_Hacker_Spaces)



Note that they're volunteer-run and don't generally provide a commercial service - so you will usually need to get involved, get to know people and use the facilities rather than expecting ready-made classes. However, you may be able to offer to pay for one-on-one tuition from an experienced user. 


---
**Ned Hill** *January 05, 2019 13:51*

**+krystle phillips** I went back and looked at your original posts from a year ago and I see I basically gave you the same information then.  Are you saying you didn't get K40 Whisperer to work for you?


---
**Duncan Caine** *January 05, 2019 18:01*

I'm using Win 10 without any problems with my stock K40 laser.  As Ned said, download and install K40 Whisperer, you will need to follow the detailed and comprehensive install instructions carefully, but having done so, you'll be OK  Just load the file you want to cut and/or engrave and off you go.

[scorchworks.com - K40 Whisperer](https://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
**Scorch Works** *January 05, 2019 18:36*

There is now a simple way to install the driver and install K40 Whisperer.  Just download the file named "K40 Whisperer Installer 0.27.exe" from the web page  and follow the prompts to install K40 Whisperer and the required driver.  (You will also need to install Inkscape for K40 Whisperer to work with SVG files)


---
**James Rivera** *January 05, 2019 22:10*

**+krystle phillips** I'm just guessing, but I bet the problem with getting it to work on Windows 10 is the driver signature enforcement. Basically, if the drivers aren't signed, then that is a red flag and you should think twice before installing them because they might have malware/viruses in them. But, if you trust the source (or just don't care/are willing to risk it) then you can disable this option and reboot to install the unsigned drivers.

[howtogeek.com - How to Disable Driver Signature Verification on 64-Bit Windows 8 or 10 (So That You Can Install Unsigned Drivers)](https://www.howtogeek.com/167723/how-to-disable-driver-signature-verification-on-64-bit-windows-8.1-so-that-you-can-install-unsigned-drivers/)


---
**James Rivera** *January 05, 2019 22:11*

But I'd recommend K40 Whisperer. I use it and it works great!


---
**krystle phillips** *January 06, 2019 13:36*

Do not want you guys to think I disappeared but new computer and I am going to follow all of the advice so graciously given here and attempt it again. When I finish installing everything i will comment again. Give me about an hr.....this is the only thing I will be doing today.


---
**krystle phillips** *January 06, 2019 14:00*

**+Adrian Godwin** I went to hackerspace and the site wasnt secure then again with a name like hackerspace it left me concerned. Have you used it?




---
**krystle phillips** *January 06, 2019 14:00*

Is there a preferred build for inkscape?


---
**Scorch Works** *January 06, 2019 14:08*

**+krystle phillips** the most recent stable release (0.92.3)


---
**Adrian Godwin** *January 06, 2019 14:45*

**+krystle phillips** The security warning appears to indicate that the certificate is incomplete (recently - I didn't get it until I retried).  I shall point it out to the maintainers.



Don't  be misled by the media's take on the word 'hacker.' Hacking is in general part of the practice of technical craftmanship, building or modifying things to do their job differently or better - while that does include investigation of security issues the emphasis on criminality is largely a media invention. The word (in a technical sense - it also has uses in journalism and horseriding) comes from MIT in the 1960s.



[https://en.wikipedia.org/wiki/Hackerspace](https://en.wikipedia.org/wiki/Hackerspace)

[https://en.wikipedia.org/wiki/Hacker_culture](https://en.wikipedia.org/wiki/Hacker_culture)

[https://www.urbandictionary.com/define.php?term=hacker](https://www.urbandictionary.com/define.php?term=hacker)


---
**krystle phillips** *January 06, 2019 15:05*

I downloaded everything (inkscape latest version and whisperer) and but as luck would have it, the dongle/usb is not reading. I cannot see the files on it.

Help **+Scorch Works** **+Adrian Godwin** **+James Rivera** **+Duncan Caine** **+Ned Hill**


---
**Ned Hill** *January 06, 2019 15:07*

You don’t need the dongle for k40 whisperer. 


---
**Ned Hill** *January 06, 2019 15:10*

The dongle is only required if you are using laserdraw/corellaser. 


---
**Ned Hill** *January 06, 2019 15:12*

After you install whisperer, watch the videos on the whisper web site to get an idea of how to get started. 


---
**krystle phillips** *January 06, 2019 15:20*

**+Ned Hill** okay I understand. Now to the site I go


---
**krystle phillips** *January 06, 2019 16:36*

**+Ned Hill** **+Scorch Works** **+James Rivera** **+Adrian Godwin**

I installed the driver and restarted but....

USB Error: Laser USB device not found

![images/57341f3f540e2aed0dea0e346a0f1f8e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/57341f3f540e2aed0dea0e346a0f1f8e.png)


---
**Ned Hill** *January 06, 2019 16:43*

I’m not well versed with whisperer, but go back to the down load page and see the driver install instructions. ![images/ac6415eb12bc2223595cdcac021d027b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ac6415eb12bc2223595cdcac021d027b.jpeg)


---
**krystle phillips** *January 06, 2019 17:02*

**+Ned Hill** I followed the instructions exactly.



The first error was a USB Error as well and it stated 

USB Error: No backend available (lib USB driver not installed)

Now its

USB Error: Laser USB device not found

****edit

I went and used the other option and it worked perfectly. I also saw an old post from **+Scorch Works** that had a screen shot of how to verify. So after i did it i checked device manager and it was successful. One hurdle complete lets go onto the next.  Last time I also could not get it to cut but I was using x8 so let me see if It can cut this time.






---
**Scorch Works** *January 06, 2019 17:34*

**+krystle phillips** I am glad you have it working now.  In the end what was the last thing you did to get it working? Was it the K40 Whisperer installer or the Zadig installer that worked for you?


---
**krystle phillips** *January 06, 2019 18:56*

**+Scorch Works** Thanks. I tried Zadig twice and it didnt work. I used the K40 installer after and perfect.



New issue I  just finished watching your first video "Overview/Introduction" I imported an image resized and everything but every image I import into whisperer is rendered as grayscale.....which means its raster engrave right?



When i select that option it moves a lot but nothing happens no image goes on the paper.



How do I get all of the colours (raster engrave, vector engrave and  vector cut)


---
**James Rivera** *January 06, 2019 19:11*

**+krystle phillips** If you haven’t watched the YouTube videos on the page yet, I highly recommend you do so. They are not too long and tell you exactly how to do pretty much everything you need in Inkscape to use the file in K40 Whisperer.[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
**Scorch Works** *January 06, 2019 19:37*

**+krystle phillips** this video is the best to get started making a design.  Only the stroke colors get converted to vector operations. (Not fill or imported images.) [https://m.youtube.com/watch?v=9wYvbenigtk](https://m.youtube.com/watch?v=9wYvbenigtk)

[youtube.com - K40 Whisperer: Making a Design](https://m.youtube.com/watch?v=9wYvbenigtk)


---
**krystle phillips** *January 07, 2019 00:01*

**+Scorch Works** Okay.

I got all three but  when you said adjust the power to 1million....how do I do that?

![images/3960eaf0149600bf7449de8300456274.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3960eaf0149600bf7449de8300456274.jpeg)


---
**Scorch Works** *January 07, 2019 02:57*

**+krystle phillips** In the video I think I set it to 10 Milliamps.  To set the power you need to hold down the test button and adjust the knob until the current on the meter reads what you want.  I think the general guideline is don't go above 16 mA otherwise you run the risk of significantly reducing the life of your laser tube.  (I don't go above 12 mA.)   Make sure you don't have anything you care bout under the laser when you hold down the test button because whatever is under the laser is going to get hit by a laser.

![images/63a39e34d3c965bff74ff0a5653449d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/63a39e34d3c965bff74ff0a5653449d8.jpeg)


---
**krystle phillips** *January 07, 2019 09:08*

**+Scorch Works** well seems my laser may going through something because at 14 it's now able to cut through cardstock.

Just barely...now trying 15.



![images/fff6e88ad5ac99d414f413000c2bb9e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fff6e88ad5ac99d414f413000c2bb9e5.jpeg)


---
**krystle phillips** *January 07, 2019 09:15*

**+Scorch Works** This is the design 

![images/fdba2eacefe3b44ecb585159e4bab087.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fdba2eacefe3b44ecb585159e4bab087.jpeg)


---
**krystle phillips** *January 07, 2019 09:20*

**+Scorch Works** This is the outcome after 15 mA

Barely made it through the cardstock and no raster

![images/e388d18ae8b4160a0ccd72b9cdb4b39b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e388d18ae8b4160a0ccd72b9cdb4b39b.jpeg)


---
**Scorch Works** *January 07, 2019 13:16*

**+krystle phillips** It should easily cut through cardstock.  Did you go through a mirror alignment procedure?  And make sure your mirrors and lens are clean?  


---
**krystle phillips** *January 07, 2019 13:30*

**+Scorch Works** I will look at them. My husband threatened me not to touch them lol

Took him a couple days to get them right.

It could very well be that since its been packed away about a year.


---
**Joe Alexander** *January 07, 2019 14:30*

what speed are you running that job at? for cutting 1/8" birchwood I usually go 6-8 mm/s@15ma max.And are you using an air assist? 

   If your laser has sat for a long period of time I would recommend changing your coolant water if you haven't yet. If it has indeed sat for a year with the same water there is the possibility you are losing some power to bleed-out into the water(can gain conductivity as it becomes contaminated). Are you hearing any squealing?

 and to check the mirrors without touching them i pulse the laser at each corner with a lil tape on the laser head opening before the third mirror(not after the focus lens) and see if they all stack appropriately. if they do then it should be aligned. if they are dirty clean them VERY GENTLY with a Q-tip and some isopropanol or similar soft solvent. Do not use acetone, it will ruin your mirrors. And dont press hard, it can mar the coating.


---
**krystle phillips** *January 07, 2019 20:57*

**+Joe Alexander** I ran it at 8mm/s

I do not have air assist unfortunately

Talk to me about coolant water..I have no clue what that is. 

There is a little squeak nothing significant.

I may have something I can use..no acetone gotcha.



Wish me luck and I look out for your answers on the other questions.


---
**Joe Alexander** *January 07, 2019 21:01*

the water you run through the laser tube to cool it needs to be distilled water and changed periodically. Tap water has minerals and other contaminants that increase its conductivity, allowing power to leech into the water and lowering the tubes' output. easy fix, just buy a few fresh gallons at the local market. the squeaking can indicate this or a failing HVT in the laser psu. if its only a small squeak I wouldn't fret about that. Also be aware that the laser tube is a consumable item and loses its "oomph" over time. It may very well be time for a fresh tube if nothing else helps.


---
**krystle phillips** *January 07, 2019 21:05*

**+Joe Alexander** ok....i have tubing for days. When it was put down the tubes and pump were removed so no dormant water.  When I started it back yesterday I used regular tap water...I am not sure what was used before.



Can I use regular water till I get a chance to get the distilled?


---
**Joe Alexander** *January 07, 2019 21:14*

its not recommended and may be your issue. I'd run to safeway/walmart or an equivalent and get some distilled water. Its usually only a dollar a gallon or so.


---
**krystle phillips** *January 07, 2019 23:54*

**+Joe Alexander** I am in the Caribbean. Distilled water isn't as easy to come by here. I will ask around.


---
**Scorch Works** *January 08, 2019 01:02*

I wonder if the laser beam is bouncing off of something in the laser head.  I would check to see if the laser beam has more power before entering the laser head then it does after exiting.  You could do this by taping a piece of paper over the entrance to the laser head, then doing a test fire.  If the paper marks faster before the laser head than it does after then there is a problem in the laser head


---
**krystle phillips** *January 22, 2019 10:28*

**+Scorch Works** Work got in the way...let me test this theory.


---
*Imported from [Google+](https://plus.google.com/+krystlephillips/posts/NVuZx2SSVdb) &mdash; content and formatting may not be reliable*
