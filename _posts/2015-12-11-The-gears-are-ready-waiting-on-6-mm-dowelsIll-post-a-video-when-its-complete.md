---
layout: post
title: "The gears are ready waiting on 6 mm dowels.....I'll post a video when it's complete"
date: December 11, 2015 16:23
category: "Discussion"
author: "Scott Thorne"
---
The gears are ready waiting on 6 mm dowels.....I'll post a video when it's complete.

![images/159f3e85d43f4b751173e061564259a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/159f3e85d43f4b751173e061564259a9.jpeg)



**"Scott Thorne"**

---
---
**ChiRag Chaudhari** *December 11, 2015 17:07*

Scott, you have done some pretty amazing work with the K40. So far I have been using 50W Epilogue MiniHelix at the University and its a hell of a machine. But from your work and video cutting gears I am IMPRESSED. So can you please tell me more about your setup ? Just throw the words, I will catch them. No need to write in full detail. 


---
**Gary McKinnon** *December 11, 2015 17:49*

Very nice gears. That wasn't all one cut i take it, did you cut the struts seperately from the gear ?




---
**Gary McKinnon** *December 11, 2015 17:54*

Because i've been wondering if there's a way to make a 'hole' that doesn't go all the way through. I guess i may need to upgrade the controller for that ?




---
**Scott Thorne** *December 11, 2015 18:12*

Those are one cut....with an outer ring glued to each one....I have the plans saved I'll share them.


---
**Scott Thorne** *December 11, 2015 18:14*

**+Chirag Chaudhari**...the trick is the head alignment....when I get checked in to the hotel I'll explain more...it's my birthday so the wife has me on a mini vacation.


---
**Scott Thorne** *December 11, 2015 18:36*

My machine came with a broken tube so I didn't want to wait to get a new machine so I bought a puri 40 watt tube from laser depot, I removed the air duct completely and installed a honeycomb table, I installed an air assist head from light objects and bought 3 20mm mo mirrors 3mm thick, lens is 18mm znse from light objects, to set my hight at 2 inches which is the fl of the lens I cut a q tip 2 inches and stuck it up the head until it touched the lens and made marks with a sharpie all the way around on the rails....the alignment of the head is the most important part of alignment...it needs to be aligned without the lens installed, place a piece of wood below the head with the lens out, if the head is not turned exactly right the spot on the wood will look like a half moon instead of a full circle around 2mm in diameter...other than that I use Corel laser and laser draw....my speeds and power are 5 to 7 mA.....at 7mm/s when cutting and 400mm/s at 4 mA when engraving.


---
**Scott Thorne** *December 11, 2015 19:05*

**+Gary McKinnon**....everything was done at one pass....7mA at7mm/s.....I downloaded the file off of the internet and had to separate the layers and rearrange everything to fit the cutting space of the k40....this was written for a much bigger laser cutter....I saved the files after making the changes because I knew I would end up making more than 2 of them.


---
**ChiRag Chaudhari** *December 11, 2015 22:11*

**+Scott Thorne** Great! Thanks for all the info. I am still waiting for the Light Object Laser head. I have already updated the machine with Ramps. But WOW, you are only drawing 7mA for cutting ? I have read some people saying dont go over 22mA and some say 18mA. So I guess I really have to run bunch of experiments to find the sweet spot.


---
**Scott Thorne** *December 11, 2015 22:17*

Stay away from running current that high....greatly reduced your tube life....I never run mine over 8.....if you have to cut with really high mA....then the speed is way too high...you will find a nice balance, just do some testing....most important is the laser head alignment....it will cut even with the beam not going straight through the lens but not efficiently, try the lens removal method...you will be surprised what you will find that way.


---
**ChiRag Chaudhari** *December 11, 2015 22:27*

**+Scott Thorne** Exactly, I have been thinking the same way. I guess the Lens Removal method is actually really really important. If the beam passes through lens right in the middle, its going to concentrate or get focused ~100% producing maximum power/heat. That way one can get max efficiency at lower current. Also I would rather have longer life of laser than cutting my files at faster speeds. 



I am getting deep into this. Love it. Love the load of awesome information!!!


---
**Scott Thorne** *December 11, 2015 22:40*

Anytime man....I see you are into drones too....I have the phantom 3 advanced...I'm in love with that thing man.


---
**Gary McKinnon** *December 11, 2015 23:20*

Love the [diydrones.com](http://diydrones.com). I'm working on a drive that would give drones a very long flight time.


---
**ChiRag Chaudhari** *December 12, 2015 00:20*

Yeah I love anything that I can fly with a remote control. I mainly like the small ~250 size FPV racers. Phantom 3 is really nice copter for aerial photography. I dont have any experience on dji platform though, for my bigger rigs I fly APM, Pixhawk and now OP Revo.



Hopefully once I figure out perfect settings to engrave on Carbon Fiber (with proper ducting of course) I can etch my logo on the ones I assemble for customers. 


---
**ChiRag Chaudhari** *December 12, 2015 00:25*

**+Gary McKinnon** Wow, at this point I can not ask anything more than longer flight times.


---
**Scott Thorne** *December 12, 2015 00:53*

I know...mine is 20 minutes tops.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/emHoFbD4HZw) &mdash; content and formatting may not be reliable*
