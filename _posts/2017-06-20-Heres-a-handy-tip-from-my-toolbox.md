---
layout: post
title: "Here's a handy tip from my toolbox"
date: June 20, 2017 20:44
category: "Discussion"
author: "Ned Hill"
---
Here's a handy tip from my toolbox. You know how sometimes your piece of plywood doesn't cut all the way through at one spot and you have to finish it off with a sharp knife?  You then end up with an area of the cut that is clean wood compared to the brown lasered edge. You could  sand off all the edges to make it match or, if you want to keep the dark edge, just use a brown sharpie marker to quickly color and blend the area.   A black marker can work as well but the brown generally gives a better match. Also good for touching up engraved areas.  #K40ShopTips



![images/3864fd9f782277e63c0799a69c146974.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3864fd9f782277e63c0799a69c146974.jpeg)
![images/8984825cbccc69a8ddde6e4030766f21.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8984825cbccc69a8ddde6e4030766f21.jpeg)
![images/dfeba3fb7804912de3a53de850b98f80.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dfeba3fb7804912de3a53de850b98f80.jpeg)

**"Ned Hill"**

---
---
**Anthony Bolgar** *June 20, 2017 20:53*

Excellent tip **+Ned Hill** Thanks for sharing.


---
**Abe Fouhy** *June 22, 2017 18:13*

Works great on scratched hardwood floor too! 


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/UxEjFGBmp9E) &mdash; content and formatting may not be reliable*
