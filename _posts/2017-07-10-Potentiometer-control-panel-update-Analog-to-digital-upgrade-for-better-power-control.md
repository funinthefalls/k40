---
layout: post
title: "Potentiometer control panel update Analog-to-digital upgrade for better power control"
date: July 10, 2017 02:37
category: "Discussion"
author: "Grafisellos Barranquilla"
---


Potentiometer control panel update

Analog-to-digital upgrade for better power control





![images/1f13dbd5739eaa309ffad5ecd5fdc80d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f13dbd5739eaa309ffad5ecd5fdc80d.jpeg)
![images/0afb508e11b221a45f23f52038d0b544.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0afb508e11b221a45f23f52038d0b544.jpeg)
![images/26561beed7b426bd7ad7c52f5a7c14fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/26561beed7b426bd7ad7c52f5a7c14fd.jpeg)

**"Grafisellos Barranquilla"**

---
---
**HP Persson** *July 10, 2017 18:47*

Are you looking for instructions how to convert it ?

Let me know and i´ll show you (or someone else quicker :) )




---
**HalfNormal** *July 11, 2017 00:56*

Actually going digital will make fine tuning the K40 harder not easier. The power is not linear and using a 10 turn pot gives finer adjustability than a digital to analog circuit. 


---
*Imported from [Google+](https://plus.google.com/104328369179353942228/posts/CzKMdAoTQPc) &mdash; content and formatting may not be reliable*
