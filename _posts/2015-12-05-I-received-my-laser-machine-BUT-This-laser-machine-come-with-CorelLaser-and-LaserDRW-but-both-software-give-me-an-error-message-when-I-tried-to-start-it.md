---
layout: post
title: "I received my laser machine BUT. This laser machine come with CorelLaser and LaserDRW but both software give me an error message when I tried to start it"
date: December 05, 2015 03:38
category: "Discussion"
author: "Gilles Letourneau"
---
I received my laser machine BUT.



This laser machine come with CorelLaser and LaserDRW but both software give me an error message when I tried to start it.  I tried on two computers first one is Win7 Ultimate version the second computer is Win 8 and both computer have same bug.

![images/7bdc2c0655fa699f1f8df7a295fc4bfd.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7bdc2c0655fa699f1f8df7a295fc4bfd.png)



**"Gilles Letourneau"**

---
---
**Thomas B.** *December 05, 2015 05:53*

check your Windows Eventlog....



if  

c:\program files (x86)\corel\corel graphics 12\programs\kodakcms.dll



caused the Error, you can fix it ...



[http://community.coreldraw.com/talk/coreldraw_x3_and_older/f/22/t/8923](http://community.coreldraw.com/talk/coreldraw_x3_and_older/f/22/t/8923)


---
**Gilles Letourneau** *December 05, 2015 15:43*

It's not that file..  The event viewer show an error with USBHook.dll    



question:



Could it be because I'm running on a 64 Bit version of Win 7 ?



anybody ever ran LaserDRW on a 64 bit OS ?


---
**Gilles Letourneau** *December 05, 2015 17:20*

before you fix it .. was it the same error message that is on the picture ?



what is the name of the file for the usb ? and do you mean the USB "safe dog" they sent with the laser machine ?


---
**Gary McKinnon** *December 05, 2015 18:32*

Some people have had problems that were fixed after an uninstall then reinstall. Can you run CorelDraw ?


---
**Gilles Letourneau** *December 05, 2015 19:20*

Hi Gary,



Yes Corel Draw 12 is working fine but there is no plugin (Laser).    Standalone software LaserDRW and Corel Laser from 3DCAD are not working (see error message on the picture) and if  the event viewer of windows is accurate, it seem that it's because of a file named "USBHook.dll" that is part of both 3DCAD software.  



I tried to uninstall and reinstall but forget it always the same error message when I try to start the programs. Anyway software are not working on two of my computers.



Also I'm in my control panel right now and I can see that the driver for the USB Dongle (Safe Dog) is recognize as a simple "USB Input Device" no yellow spot on it but , is it the right driver ? maybe not.   In the other hand, the driver for the laser machine is recognize as a "Interface USB-EPP/12C...CH341A" and there is no yellow spot on it so I think that this one is ok.



I'm dreaming about a DSP controler and I'm going to order it but it's goint to take awhile to receive it and in the mean time I want to play with my Laser Machine so I need to find why the software are not working.


---
**Gary McKinnon** *December 05, 2015 19:31*

I had corrupted file problems while reading from the CD that came with the cutter, so i downloaded the free version of CorelDraw 12 from here, installed and all was good :



[http://getintopc.com/softwares/development/download-corel-draw/](http://getintopc.com/softwares/development/download-corel-draw/)


---
**Gilles Letourneau** *December 05, 2015 20:09*

Hi Gary

Hi All



Ok, I install the software on a Laptop running Win 7 64 bit and it's working.   This laptop doesn't have a big settings because there is almost no software installed.



My bug on the other machine is probably because of another software already installed and has nothing to do with the fact that the OS is a 64 bit version.



In fact my other two machine where I have the bug with the laser software have almost the same settings and I will not crash my windows to find why it doesn't work.



My bug is fixed for now or more "workaround" and I will begin my first Laser Test.  after 3 weeks of reading, asking questions and searching, I have it and it's time to have fun.



I will post my tests / result / questions along the way.



thanks


---
**Gary McKinnon** *December 05, 2015 20:24*

Good you got it working, the first machine i installed it on was an old WinXP laptop and worked very well. I use Sketchup, export to PDF then import the PDF into CorelDraw for cutting.



If you want to export to PDF from Sketchup make sure the projection is parallel and you have all your fancy edges turned off.


---
*Imported from [Google+](https://plus.google.com/101789423601866268108/posts/4NhBGUyBntB) &mdash; content and formatting may not be reliable*
