---
layout: post
title: "Hi, I have one of the older K40 machines without a amp meter originally installed, so planing to install one"
date: September 28, 2016 09:31
category: "Modification"
author: "Dag Elias S\u00f8rdal"
---
Hi,



I have one of the older K40 machines without a amp meter originally installed, so planing to install one. 



anyone else installed a amp meter and have some good tips when installing. 



I am worried about the High voltage...





**"Dag Elias S\u00f8rdal"**

---
---
**Scott Marshall** *September 28, 2016 09:46*

AS you should be, (concerned about the High Voltage) - it's very dangerous if mis-handled and you should understand what you're doing completely before attempting such a modification.



That said, it's doable, Here's brief (OK, not so brief) instructions, but do your homework, and understand how the current flows, the meter works and exactly how and why you are hooking it up as you are.



Then test it under safe conditions, using all possible precautions.



I recommend getting a qualified technician to help you with it if your're not 100% sure of what you are doing.



This is no joke, the high voltage here is NOT like static electricity, or a spark plug wire. One shock could easily be your last. RESPECT IT.



OK warnings over, here's how:



This is actually quite simple, but mistakes are potentially deadly, so it bears nit-picking the details.



What you have for Laser tube power is a simple circuit with a DC power source . The source puts out about 20ma @ 15,000volts. 



There's a negative wire going From your power supply unit (PSU) to the Emitting end of the tube. This wire is usually wrapped around the Laser Tube a few times before being connected to it's terminal. (this aids in the startup) This wire can be any color (K40s are wired in random color wire) 

The negative wire goes back to the Common or Ground (different names for the same thing) which in turn ties it to earth ground (the ground terminal of your line cord and the terminal on the back of the K40) This path MUST me maintained or any part of the K40 can become charged.



 The positive wire carries the high voltage to the Positive end of the tube. It is 'Usually' a thicker (due to extra insulation) red wire, but again, you can't count on color codes in a K40.

Neither of the tube connections should not be disturbed unless they are defective or leaking.



Ok, quick tech break, If you haven't heard the water/electricity analogy, here it is again - I hate it, but it serves our needs here)



With water you have several measures of it's strength, Pressure, which indicated how hard it is being pushed through a pipe by a force, and volume, which tells you how much water is flowing for a given time period. A water circuit has a pump feeding a loop of pipe back to it's inlet, thus a complete loop or "circuit" . an adjustable valve will serve as our Laser tube in this example.

 Pressure & Volume are inter-related, changing one effects the other. If you close the valve some, the pressure goes up and the volume goes down, assuming the pump power remains the same.



Voltage is roughly analogous to Pressure. It's how hard the electricity is trying to move along a circuit. Our Power Supply is the pump. What we want to measure is our volume or current. To do so requiresd cutting into the circuit, in both the water or electric example.



A panel meter (analog or digital) measures how many electrons pass through the wire. simple enough, you would think, just cut the wire and put it in. Not so fast!!

We have a complication. Leaking electricity can kill you, and we need to avoid it. WHERE we put the meter matters A LOT.



Since 15000 volts can jump a few inches (yes, it can electrocute you without even touching the wire, all you have to do is get close)

directly connecting it to the meter would be crazy right? YES, but there IS a way to do it safely. 



Some more Electricity facts we need to know:

1 - In a circuit, electricity flow (current or flow) is the same at every place in the circuit (simple loop type, which is what we have here), so we can put the meter anywhere in the circuit and it will read the same.



2 - A current meter requires a 'voltage drop' of a few volts (let's call it 10volts to work - let's just say it needs a sample) This mean one terminal of our current meter will always be 10 volts more or less than the other one.



3 - 'Potential' is the term used to describe the level at which an object is charged compared to the Earth (which includes you and me) The Earth is always Zero Volts (which is why lightning likes to strike it Think of our K40 as 'micro lightning'



If you look at the circuit of a Laser tube, there's 2 places to put a meter in, between the PSU and the Positive connection of the tube, or between the PSU and the Negative connection of the tube.



The Problem (WRONG WAY): 

If we hook our current meter to the positive side of the PSU, the meter is connected to the +15,000 volts. One terminal will be at 15,000 volts above ground and the other terminal will have a potential of 14990 volts  (- the 10v sample) Pretty scary stuff on your front panel...



I don't think we want out meter right on the front panel charged at 15000 volts, after all that's what we're going through this discussion to prevent. (thank you for your patience, we're almost there)



The Solution:  If we hook the meter between the laser tube (which has 15,000 v coming out of it - more on this later) and ground, the meter will be attached solidly to a 0 volt Grounded point. 



One side of the meter will have a Zero Volt Potential, and the other side will have a +10 volt potential (MUCH better huh?)



The same amount of current flows thru the meter as on the high voltage side, but the voltage is only a few volts above ground (We'll skip ohms law and the reasons why, trust me on this one.)



/////////////////////////////////////////////////////////////////////////

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

/////////////////////////////////////////////////////////////////////////



The Bottom line, How to connect the meter in a safe manner:



Buy your meter.  You will want a 0-20 milliamp meter with an internal shunt. The shunt is a resistor which send the sample to the meter, on large meters,  it's mounted separately, we don't need that arrangement here. 

Digital or analog both work fine here, but you'll need DC power to operate the Digital meter internal circuitry.

Ideally, ideally a meter with a 20ma full scale reading will give you the best resolution. Going smaller won't show the top portion of the range (it will 'pin' and possibly even be damaged). Going larger will reduce the resolution to where the usefulness of the meter will be reduced. (Example: on a 200ma FullScale meter, it will only be reading @ 1/10th scale when the laser is on full power)

On a digital meter, since you see the reading clearly, this is less important. Just make sure you have the desired number of decimal places. You'll find anything beyond 1 decimal point just results in a lot of flashing, useless numbers. Even reading down to 0.1 milliamps is of highly questionable value.

(Useful fact: A milliamp is 1/1000 of an amp)



Install your meter: 

Power Off, Line cord taped or otherwise secured to prevent accidental connection.



Mount the meter in the panel face where you like it.



If you selected a digital meter, connect the required power and get it running before connecting the current loop to it. You should see the display come on and see a low to no reading. This way, you know it's working before starting up the high voltage loop and if there's problems, they can be dealt with without worrying about high voltage.



Follow the negative wire from the laser tube emitting end (left from the front) into the electrical enclosure. It should end up at the leftmost terminal of the power supply Mains connector (leftmost 4 pin connector). Remove it and reroute it (along with a NEW 2nd wire of equal size but different color (18-22awg is good) to your newly mounted Milliammeter. Leave a loop as a strain relief where it crosses the door hinge, anchoring it on both sides so it cannot be pinched or cut.



Wiring: Attach the wire coming from the laser tube to the POSITIVE connection of your Milliammeter. 



Attach the new wire to the negative side of your Milliammeter.



Attach (If not already done, doublecheck) the new wire to PSU ground connection where the factory wire was removed.



Make these secure connections, these are critical safety points and if any of these wires comes off, it will be powered with the full 15kv of the PSU and can do great harm to man and machine. As long as it remains connected, it's a <10V pussycat)



Be VERY careful on initial startup. If anything seems wrong, or you hear arcing/hissing, remove the line cord from the wall, don't touch the K40.

Find the problem and correct it.



If at any time during your upgrade you have a question or feel uncomfortable, get in touch thru my website ALL-TEKSYSTEMS.com

and I'll do my best to help you figure it out.



Good luck,

Scott



I have kits to make this easier if you're interested, including both digital & analog versions with both Voltage and Current that read directly from the high voltage tube input. - they're not up on the site yet, but I have the components in stock and will put together whatever you need.



The line of PSU/Panel upgrade meters is called the "Electric Company" kits and will be advertised in the next few months (sooner if I get caught up)










---
**Dag Elias Sørdal** *September 28, 2016 13:02*

**+Scott Marshall** WOW! That was thorough. I am an ROV pilot tranee and we "work" with 3400V @ 16A. So I know this stuff is dangerous... Anyway great point in putting it between ground and laser. That was actually my thought, but I had no "reason" just gut feeling. thanks for the good reply :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 28, 2016 14:25*

**+Scott Marshall** I agree with **+Dag Elias Sørdal**, great thorough response. Even though a lot of it eludes me, it's great that you care enough to share all these safety tips with us. With my limited electronics/electrical knowledge I would have just thought it safe to splice the ammeter anywhere into the line feed, potentially killing myself in the process, so I'm glad to read such a safety conscious post explaining the process & the why not to do it that way. Thanks heaps for watching out for all our safety & the tidbits of knowledge that you're sharing with us all :) Much appreciated.


---
**HP Persson** *September 28, 2016 16:03*

Even though i know how to connect it, i had to read it - twice. Good information and detailed explanation, love it :)


---
*Imported from [Google+](https://plus.google.com/+DagEliasSørdalurm8/posts/bN9hvMVAJDz) &mdash; content and formatting may not be reliable*
