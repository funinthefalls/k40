---
layout: post
title: "I'm a complete noob to laser cutters and have a random question about Air Assist setups"
date: February 07, 2017 15:05
category: "Modification"
author: "matt s"
---
I'm a complete noob to laser cutters and have a random question about Air Assist setups. I understand that everyone uses some form of a compressor for their air assist, but is it possible to use a very strong fan similar to a 3d printer fan to accomplish a similar result? I know the little fan won't produce the psi the air compressor's will, but If I could build decent fan mount that could hold 1 or 2 fans similar to the Air Assist mod I feel like it could do enough to fan the flames/dust without having needing a noisy air compressor. Any thoughts? 





**"matt s"**

---
---
**Ned Hill** *February 07, 2017 15:26*

An air assist serves several functions. It helps to keep the bottom of the lens clean of junk build up, keeps the work area clear, prevents flare ups and provides air (oxygen) to enhance cutting efficiency.   These functions are most efficiently done by having the air exist just below the lens and straight down onto the work surface.   Fans might work to a certain extent but they are not going to be anywhere near as efficient as a proper air assist setup.



A full blown compressor would defiently be noisy but there are other options like an air airbrush compressor that are not that noisy.


---
**Joe Keneally** *February 07, 2017 15:33*

I would be concerned with the vibration the fans would introduce to the system along with the added wear they might possible bring as well.


---
**Ned Hill** *February 07, 2017 15:34*

Good point **+Joe Keneally** :)


---
**matt s** *February 07, 2017 15:40*

**+Joe Keneally** **+Ned Hill**  Nice info, I didn't think it would work but I wanted to ask about different options.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 07, 2017 17:18*

**+matt s**



**+HP Persson** has a fan on his setup for air-assist if I recall correct.


---
**Paul de Groot** *February 07, 2017 20:55*

I do use a little pc fan and it does work really well. 3d printed a drag chain for the power wires to make it safe and prevents entanglement of the carriage. ☺

![images/beecce3f1c5b10e95d462ceb3149009f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/beecce3f1c5b10e95d462ceb3149009f.jpeg)


---
**Vince Lee** *February 08, 2017 22:33*

I use a small fan too and it works perfectly.  I was concerned about having yet another loud piece of separate machinery to run, maintain, and keep free of moisture, and thought I'd give a fan a try.  I went through a couple of iterations, and ended up using a small 24V 2-inch fan, having tried two models before finding one that blew really strongly and smoothly.  It is mounted to the head and connected with cable chain (very similar to Paul's above) and has some aluminum fins mounted on either side of the fan to help direct the flow.  It seems to work really well both keeping the lens clean and blow out the flames that used to sometime appear while cutting wood before.

![images/05e93070aa27a65acd683df3d2b1b32d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05e93070aa27a65acd683df3d2b1b32d.jpeg)


---
**Joe Keneally** *February 09, 2017 02:10*

Good info!!


---
**matt s** *February 09, 2017 20:53*

Thank you everyone for all the helpful tips and ideas. I'm currently trying to get my Glass etching settings dialed in.


---
*Imported from [Google+](https://plus.google.com/106809986839735143620/posts/GtSxtvuFeih) &mdash; content and formatting may not be reliable*
