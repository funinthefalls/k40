---
layout: post
title: "Water chiller hook up? I just got a water chiller"
date: April 07, 2017 16:42
category: "Discussion"
author: "Derek Schuetz (FidgetThings)"
---
Water chiller hook up? 



I just got a water chiller.  Was wondering do these have a pump built into them? So I just fill their reservoir and I'm good to go or do I need a separate reservoir also with a water pump? 





**"Derek Schuetz (FidgetThings)"**

---
---
**Mark Brown** *April 07, 2017 17:09*

What sort of chiller is it?  A proper industrial-ish one? Undersink mount?


---
**Derek Schuetz (FidgetThings)** *April 07, 2017 18:00*

[ebay.com - Details about  USA Stock! HOT! 110V 60Hz CW-5200DG Industrial Water Chiller for CO2 Laser Tube](https://www.ebay.com/itm/272579091085) 


---
**Jim Hatch** *April 07, 2017 18:22*

It's got a pump inside it. I use one (CW8000) on a 60W laser.


---
**Derek Schuetz (FidgetThings)** *April 07, 2017 18:28*

**+Jim Hatch** ok that's what I was thinking thank you


---
*Imported from [Google+](https://plus.google.com/118280415605525600445/posts/Xy5vrJVRnC7) &mdash; content and formatting may not be reliable*
