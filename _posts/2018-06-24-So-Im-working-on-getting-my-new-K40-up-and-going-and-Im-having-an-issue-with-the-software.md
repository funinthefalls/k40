---
layout: post
title: "So I'm working on getting my new K40 up and going, and I'm having an issue with the software"
date: June 24, 2018 04:12
category: "Original software and hardware issues"
author: "Mason C"
---
So I'm working on getting my new K40 up and going, and I'm having an issue with the software. The CD I got has CorelLaser and some other software on it, but it says I need to install CorelDraw first. There's also a USB dongle that my computer recognizes but won't show in the "My Computer" folder.

From what I can tell, CorelDraw is a $500 software package. I can't imagine everybody is buying these cheap lasers and spending double the cost on the software... what am I missing? Is there a free download or a "lite" version or something somewhere?





**"Mason C"**

---
---
**Anthony Bolgar** *June 24, 2018 04:43*

The CD has a hacked version of CorelDraw on it in one of the subdirectories. The USB dongle is a key to allow you to use the software, there is no actual software on the USB dongle. Do yourself a favor and check out K40 Whisperer. Scorch wrote it to use with the K40 without having to use the crappy software that comes with it, and there is no need to use the USB dongle. Basically all you do is create an SVG file in Inkscape of what your design is to be, and open it with K40 WHisperer which sends it to the laser. Very easy to use. The program can be found at [scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html) He details step by step how to install it and how to use it.


---
**Don Kleinschnitz Jr.** *June 24, 2018 13:17*

....or just get yourself a new controller [C3D] and reasonably priced software [Lightburn].


---
*Imported from [Google+](https://plus.google.com/100963713799508705729/posts/bJY23QAva7Y) &mdash; content and formatting may not be reliable*
