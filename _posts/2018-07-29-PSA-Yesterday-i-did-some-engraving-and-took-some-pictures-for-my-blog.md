---
layout: post
title: "PSA: Yesterday i did some engraving and took some pictures for my blog"
date: July 29, 2018 08:14
category: "Discussion"
author: "Frederik Deryckere"
---
PSA: Yesterday i did some engraving and took some pictures for my blog. The auto flash was on and it messed up my limit switch optical sensor. Result: a shift in the X. I reproduced the error so I was sure it wasn't a fluke. Not something you want to happen on a long engrave or with expensive wood ;-)

![images/530463f879ad9d144763ec4ab3412f11.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/530463f879ad9d144763ec4ab3412f11.jpeg)



**"Frederik Deryckere"**

---
---
**Kelly Burns** *July 29, 2018 16:55*

This is interesting.  I would think that Infrared Sensors would not be affected by Camera Flash. Of course, the circuitry on these is not very fault tolerant in general.   



Are you running stock setup or have you switched the controller out? 



  


---
**Frederik Deryckere** *July 29, 2018 20:07*

C3D board


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/aBSf3Ff1d7c) &mdash; content and formatting may not be reliable*
