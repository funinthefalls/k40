---
layout: post
title: "I should have sought the advice of this group first, but i have decided to purchase an upgrade"
date: September 09, 2016 08:09
category: "Discussion"
author: "Tony Schelts"
---
I should have sought the advice of this group first, but i have decided to purchase an upgrade. what are your thoughts

[http://www.ebay.co.uk/itm/201662703429?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.co.uk/itm/201662703429?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)





**"Tony Schelts"**

---
---
**Mircea Russu** *September 09, 2016 10:08*

Usually these machines run around 2000GBP. Make sure it's not a Moshi or misses accessories like pump, fan, compressor. A good blue-white "50W" G350 is better than a lousy red-black "60W" with flimsy carriages and Moshi. 


---
**Ian C** *September 09, 2016 11:34*

From what I've read these are reasonable machines, I'm considering this, an 80 watt version or cnc router. There is a Facebook page called X700 Clone off the top of my head, which you may find makes good reading, lots of buyers on there with this type of machine and they're very happy with them


---
**Ian C** *September 09, 2016 11:35*

To add the 80 watt versions hover around the £2000 mark


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/9jFaBnzcL9i) &mdash; content and formatting may not be reliable*
