---
layout: post
title: "I want to share a picture because it's so impressive..."
date: December 10, 2016 17:13
category: "Discussion"
author: "java lang"
---
I want to share a picture because it's so impressive...

After some testcuts and some minutes later I heard some odd soundings coming from my cutter ant then: bang and splintering noises. I hurried up and disconnected all electricity, dismounted the dust fan, opened the rear because I was convinced that the lasertube was broken. But nothing to see, everythin seems to be OK, then I opended the front....

Short story: Don't use a glass plate as base...

:)

![images/26b32e7130444c45cfbf1c9dd956c39a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/26b32e7130444c45cfbf1c9dd956c39a.jpeg)



**"java lang"**

---
---
**Gee Willikers** *December 10, 2016 17:22*

Yup. Heat stress fractured it you did.


---
**Andy Shilling** *December 10, 2016 17:25*

Did you have it on a very setting? I've use mine to etch glass and it never cracks. I'm a little concerned now.


---
**java lang** *December 10, 2016 17:28*

No, it wasn't etching. I cutted 12 mm arylic and had forgotten to remove the glass I used befor for some light papercuts...


---
**Gee Willikers** *December 10, 2016 17:28*

<s>I think they were cutting on it or trying to cut the glass itself</s>. Too much heat at one pin point. Etching at speed should is normally fine.


---
**Andy Shilling** *December 10, 2016 17:29*

Right that's a relief. Thanks for sharing. It's easy to forget the safety aspects of using these things.


---
**Gee Willikers** *December 10, 2016 17:30*

**+java lang** It happens. I myself today broke the glass sheet I use in my 3d printer.


---
**java lang** *December 10, 2016 17:32*

**+Gee Willikers** yes exactly, it was the 3d printer glass


---
**Gee Willikers** *December 10, 2016 17:35*

**+java lang** fwiw I'm trying mirror now. They say the metallic backing and paint help even and retain the heat.


---
**java lang** *December 10, 2016 17:41*

this is my new plate from the harware store buyed afterwards ...

![images/7a4f99c9c466cbee9d0990cfe6e8d11c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a4f99c9c466cbee9d0990cfe6e8d11c.jpeg)


---
**Gee Willikers** *December 10, 2016 17:46*

I used the light object Z bed and tapered pins. Honestly due to the burrs on the holes in the bed things tend to be uneven but for my purposes it works.

![images/562b21b9e56fce49d26a110be4d1a59b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/562b21b9e56fce49d26a110be4d1a59b.jpeg)


---
*Imported from [Google+](https://plus.google.com/111347667569437072694/posts/XpSWHURpVra) &mdash; content and formatting may not be reliable*
