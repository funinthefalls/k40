---
layout: post
title: "Does anybody know the engraving settings for leather?"
date: February 15, 2016 14:57
category: "Software"
author: "Patryk Hebel"
---
Does anybody know the engraving settings for leather? I'm using CorelLaser 12.





**"Patryk Hebel"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2016 19:17*

Depends on the thickness of the leather. I generally take a scrap of the leather & do some test runs first at varying powers/speeds to determine what is the best & cleanest look. Takes a while to do full range of settings for test runs but is definitely worth it for future reference (if you plan on doing lots of leather). Easiest way is to do something simple like a 10x10mm square & engrave it. Then you can also do 10x10mm square to test cutting as well.



But as a rough starting point I use around 4-8mA power & 500mm/s speed (always). Thinner leather can be difficult to engrave without blasting all the way through it (e.g. my 1-2mm kangaroo hide even at lowest engrave settings goes almost all the way through it). With thinner leather I tend to lift the piece approximately 3mm closer to the laser head (to defocus the beam a bit). This tends to give a less crisp engrave, but prevents it blasting all the way through.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2016 19:34*

Here is a link to an album of mine where I show engraving settings test on 3-5mm thick leather. [https://lh3.googleusercontent.com/wK-YVG2SMHL95W1BCyqkfGsM5RyxAfX1ka_Glvbb-RiYeaxbUOHFpqwCGOpqdRWaHzjODZjo9GBYLQ=s0](https://plus.google.com/photos/115098100716537966732/albums/6219899124736833569)


---
*Imported from [Google+](https://plus.google.com/102509166997588398801/posts/BjQpUmfPCra) &mdash; content and formatting may not be reliable*
