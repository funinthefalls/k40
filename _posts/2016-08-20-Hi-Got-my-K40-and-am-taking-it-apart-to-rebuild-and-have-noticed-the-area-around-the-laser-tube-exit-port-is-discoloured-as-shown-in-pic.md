---
layout: post
title: "Hi, Got my K40 and am taking it apart to rebuild and have noticed the area around the laser tube exit port is discoloured as shown in pic"
date: August 20, 2016 22:00
category: "Original software and hardware issues"
author: "Jez M-L"
---
Hi,



Got my K40 and am taking it apart to rebuild and have noticed the area around the laser tube exit port is discoloured as shown in pic.  Is this normal and should I clean it off? If yes what would be the best stuff to clean it with?



Thanks in advance,



Jez

![images/10a2d39dc37b327bd9858ee23ac915bb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/10a2d39dc37b327bd9858ee23ac915bb.jpeg)



**"Jez M-L"**

---
---
**Gunnar Stefansson** *August 21, 2016 01:37*

I would say, it doesn't matter that much, mainly that the glass where the beam comes from is clean, personally I clean the glass and what I see on your picture with eartips and some alcohol while laser is powered off and don't use it until 30min just to be sure any leftover alcohol has evaporated, I just think a glass laser tube should be clean, cause in my machine I can see it from the front.


---
**Robi Akerley-McKee** *August 21, 2016 02:42*

I second what Gunnar said.  I put a piece of tape over that big hole to cut down on smoke coming into the tube area. First laser fire put a little hole in the tape.  I also removed the exhaust duct inside the machine to allow more air flow from the cut chamber.  I also ran tape over the joint of the tube lid in front of the exhaust fan to suck more from the inside on the cut chamber. I also put some wedges to hold the fan tight against the back of the machine and taped the top.  Reduced my cleaning from every day to once a week.  So now the tube chamber sucks in outside air instead of getting smoke pulled through it.


---
**Savannah Champagne** *August 21, 2016 04:49*

On a side note, does anyone know if you can get eye damage from just looking at the tube while in operation? Like would it be bad to leave the back bay door off the machine and use it?


---
**Robi Akerley-McKee** *August 21, 2016 05:18*

I don't know, but I won't be looking at it. I'm not taking a chance with it.  I'm still working on adding a lid switch to my setup, seeing this question, I think I'll be adding one to the back lid as well.


---
**Ian C** *August 21, 2016 14:29*

Hey. As a newbie to the whole laser thing and with a K40, I looked into eye protection and it has been said on many sites that just ordinary safety glasses will work with this type of laser. I've just spent countless hours levelling my bed up with the lids open and single shot firing the laser for alignment purposes without problem. Of coruse I will not stand in the way of where I think the laser beam will travel in, that would be silly, but as long as you're careful you'll be fine. I've had the back tube cover off when doing this to. WIth the colour you're seeing, I'm inclined to say it's just the epoxy colour transfering through the glass, as the front water jacket is epoxied to the front of the laser tube. I knwo this as I had to glue mine back on after delviery.


---
**Savannah Champagne** *August 21, 2016 16:27*

I might be wrong but I thought regular safety glasses would just protect you from a direct beam into the eyes, i didn't think they would protect you from the radiation.


---
**Ian C** *August 24, 2016 14:07*

Hey buddy. From what I've read it's the heat of the laser that could cause more damage than the actual laser beam. The beam isn't as harming. However I'd still not take chances and wear ordinary safety specs. I'm yet to find a dedicated pair of safety specs I can honestly say is for our laser to protect our eyes


---
*Imported from [Google+](https://plus.google.com/103448790120483541642/posts/2GnnZL6cW47) &mdash; content and formatting may not be reliable*
