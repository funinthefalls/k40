---
layout: post
title: "Veteran K40 owners: Do you find that the stock vent manifold that extends into the bed is valuable, or have you removed/trimmed it back without noticing a decrease in ventilation?"
date: March 15, 2017 16:09
category: "Modification"
author: "Bob Buechler"
---
Veteran K40 owners: Do you find that the stock vent manifold that extends into the bed is valuable, or have you removed/trimmed it back without noticing a decrease in ventilation?



I ask because, during testing I've found I have an additional ~30mm of play on both axes and I'd like to be able to use, and the vent manifold is in the way.



Thoughts?﻿





**"Bob Buechler"**

---
---
**Cesar Tolentino** *March 15, 2017 16:19*

I cut mine maybe an inch. That's the material my grinder can cut without hitting the wall.  The vacuum is still the same with the exhaust


---
**Ariel Yahni (UniKpty)** *March 15, 2017 16:20*

I removed mine


---
**Jim Hatch** *March 15, 2017 16:24*

Took mine out. The exhaust works well without it in the way. It's probably the first mod most people do to these.


---
**Bob Buechler** *March 15, 2017 17:09*

Given how much room it takes up compared to the size of the chassis hole, I wouldn't be surprised if exhaust actually improved somewhat by removing it entirely. Okay then, thanks for the feedback. I'll yank mine entirely and give it a try. 


---
**Cesar Tolentino** *March 15, 2017 20:20*

I cut mine only hoping to get a bigger cutting area. Removing it did not give me more. That's why I only took what I need. Beyond that, it dies not give more area


---
**Bob Buechler** *March 15, 2017 20:53*

my laser head is capable of traveling to about 230mmx330mm, and the manifold does occupy some of that extra space. <b>shrug</b> I guess we'll see what happens when I actually try to use that space for a job. :)


---
**E Caswell** *March 15, 2017 21:47*

**+Bob Buechler** not a "veteran" but I removed mine once I knew everything was working ok.  Removed it and installed a bigger fan by cutting a round hole in the extraction point. In my view it's much better and keeps the chamber fume free. that's the main reason I did it rather than more bed room.


---
**Stephane Buisson** *March 15, 2017 23:05*

Anyway you want to do it when you go for the honeycomb bed mod ;-))


---
**Bob Buechler** *March 15, 2017 23:15*

I can't find a decent adjustable bed mod that doesn't cost an arm and a leg...


---
**Stephane Buisson** *March 15, 2017 23:24*

**+Bob Buechler** 

best idea is that one

[plus.google.com - Adjustable platform I made for my K40. (Freshly cleaned for the video.) http...](https://plus.google.com/+ScorchWorks/posts/UkrvPtra7UK)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 15, 2017 23:43*

I totally removed mine. I get 330 x 230mm on my K40 bed. Removed what I believe was a covering angle bracket over the front stepper motor to gain maybe +10mm to bring it to 230 on the Y.



I also attached some plywood onto the default exhaust fan at the top half (which didn't suck through the air vent hole anyway) because it was preventing a good seal on the K40 vent area.


---
**Lance Ward** *March 16, 2017 00:15*

**+Bob Buechler** I picked up a $15. Lab jack from ebay, attached it with double sided tape to enclosure bottom and sit honeycomb bed on the jack.  Works good enough that I'm considering extending the jack knob out the front for easier adjustment.


---
**Chuck Comito** *March 16, 2017 03:49*

I have no idea how the stock one worked as it came out within 5 minutes of owning it. I can say that with the squirrel cage blower I have on it I don't need the duct.. just printed this guy today as an additional mod to the vent system. 



![images/d8f373f3fb8508464f6a0b344768552a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d8f373f3fb8508464f6a0b344768552a.jpeg)


---
**Jim Eder** *March 16, 2017 04:32*

I cut my manifold back flush with the edge of the XY table frame. I found that the airflow taking fume away from the part being cut was much better than fully removing it. I used a decent in-line duct fan in my outlet tube and I am very satisfied with the air flow. Here is my duct fan which I put closest to the exit of the house so as to minimize the portion of the duct which was exposed to positive pressure. I bought the 6" dia version.



[amazon.com - Amazon.com: iPower GLFANXBOOSTER6 Inline Duct Booster Fan with Cord, 6 inch Diameter, 240 CFM; 110/120V; 37 Watt, Extra-long 5.5' Grounded Power Cord, All Vent Use: Patio, Lawn & Garden](https://www.amazon.com/gp/product/B008N4QIZG/ref=oh_aui_detailpage_o06_s01?ie=UTF8&psc=1)



I also used this dust hood adaptor to connect to some duct work. It fits perfectly on the back of the K40. I immediately used standard duct work to convert this to 6 in dia duct work. You can see the fumes flying away from the work. Nice!



[https://www.amazon.com/gp/product/B003NE59HE/ref=oh_aui_detailpage_o06_s02?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B003NE59HE/ref=oh_aui_detailpage_o06_s02?ie=UTF8&psc=1)




---
**Kelly S** *March 16, 2017 15:21*

I trimmed mine back so it did not cross the rail.  I took it out all together once and smoke was all over, but that was also with the stock fan.  I have upgraded that to a real blower and never been happier.    


---
**Mark Brown** *March 17, 2017 02:17*

I left it in so the air flow is going across the top of the piece, rather than underneath where it isn't doing any good.  But if you've got a big enough fan it probably doesn't matter.


---
**Bob Buechler** *March 17, 2017 02:55*

My manifold is screwed in and not spot welded like the older ones, but it doesn't seem easy to remove. They cut part of the back gantry rail to make room for it, and now that the tube compartment is in place, the manifold looks kinda wedged between what's left of the rail and the bottom of the tube compartment. And since the manifold is larger than the rear exhaust hole, I can't just pull it out from the back. 



Any tips on how to remove it without taking apart the gantry?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 17, 2017 02:59*

**+Bob Buechler** Mine seemed the same as what you describe. I pulled it out by removing the gantry. The gantry removal was very simple (just 4 bolts if I recall correct).


---
**Bob Buechler** *March 17, 2017 03:41*

**+Yuusuf Sallahuddin** hmm. Did all four rails have to come out, or just the back rail?


---
**Chuck Comito** *March 17, 2017 04:18*

**+Bob Buechler**​, the entire gantry is held to the chassis with 4 bolts. Once those are out you can remove the whole thing. Then you'll be able to access the vent. To see the bolts, remove the panel in the front held on by 2 screws. This will expose your front stepper and front screws. Make sure the stock bed is removed as well. The rear bolts are sort of tucked underneath the back (further most) rail of the gantry. Be careful with the cables. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 17, 2017 05:55*

**+Bob Buechler** As Chuck mentions, the entire gantry is bolted together as 1 piece, so it all had to come out in 1 piece. Quite simple to remove & replace.


---
**Bob Buechler** *March 17, 2017 15:00*

**+Yuusuf Sallahuddin** Okay, thanks. Is it difficult to restore laser alignment, or do you have to go through the realignment procedure again afterward?


---
**Bob Buechler** *March 17, 2017 15:01*

**+Chuck Comito** Thanks for the detailed instructions!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 17, 2017 15:07*

**+Bob Buechler** As I had mine out of the machine for about a week (while I fiddled around) I had to do the entire alignment process again. But it was only fractionally out from the previous alignment, so very quick compared to my first attempts at aligning.


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/cFMCEA7vcaT) &mdash; content and formatting may not be reliable*
