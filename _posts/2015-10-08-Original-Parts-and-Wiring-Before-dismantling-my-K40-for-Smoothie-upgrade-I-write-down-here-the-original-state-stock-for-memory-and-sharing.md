---
layout: post
title: "Original Parts and Wiring Before dismantling my K40 for Smoothie upgrade, I write down here the original state (stock) for memory and sharing"
date: October 08, 2015 13:15
category: "Hardware and Laser settings"
author: "Stephane Buisson"
---
Original Parts and Wiring



Before dismantling my K40 for Smoothie upgrade, I write down here the original state (stock) for memory and sharing.

Wiring specifications on photos comments.



![images/48c6f8f9e59895229b35927ef91fe54d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/48c6f8f9e59895229b35927ef91fe54d.jpeg)
![images/7885915f26ee68d47c571c3ca79148e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7885915f26ee68d47c571c3ca79148e0.jpeg)
![images/6ee520e5a6805f7af4e0f3bbd950ed98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6ee520e5a6805f7af4e0f3bbd950ed98.jpeg)
![images/8924e9ad9fdd2510e313d4513e3e7152.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8924e9ad9fdd2510e313d4513e3e7152.jpeg)
![images/15427a358bb787fe6163e18933a5468a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15427a358bb787fe6163e18933a5468a.jpeg)
![images/852c8bc537309bee9524d6b1b9168a76.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/852c8bc537309bee9524d6b1b9168a76.jpeg)

**"Stephane Buisson"**

---
---
**David Cook** *October 14, 2015 19:35*

how will you control the laser power now ?  is it done with the X5 ? or do you still plan to use the panel mount POT ?



I know that the X5 will have a PWM output for the laser but I wonder if the PWM works on the same input as the POT V-control 0-5V


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/5GARrrVPZUP) &mdash; content and formatting may not be reliable*
