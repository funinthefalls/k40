---
layout: post
title: "I want to replace the mirrors but don't what size I am supposed to get any help I this?"
date: July 10, 2017 05:47
category: "Modification"
author: "William Kearns"
---
I want to replace the mirrors but don't what size I am supposed to get any help I this?



![images/9c7eca398e31ed41b902ad144d3bafdd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9c7eca398e31ed41b902ad144d3bafdd.jpeg)
![images/8c3ce6a67e02e63005c6205c10823726.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8c3ce6a67e02e63005c6205c10823726.jpeg)

**"William Kearns"**

---
---
**HP Persson** *July 10, 2017 07:46*

20mm is standard.

There is SI and MO (and some others) to choose from, SI is a bit better, MO is tougher against smoke and cleaning while SI is very delicate.

Keep away from K9 mirrors - far away!




---
**Don Kleinschnitz Jr.** *July 10, 2017 13:07*

#K40optics


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/i8kHP3bRgCb) &mdash; content and formatting may not be reliable*
