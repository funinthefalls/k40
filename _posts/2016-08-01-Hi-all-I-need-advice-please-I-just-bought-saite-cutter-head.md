---
layout: post
title: "Hi all, I need advice please. I just bought saite cutter head"
date: August 01, 2016 04:15
category: "Modification"
author: "Pippins McGee"
---
Hi all, I need advice please.

I just bought saite cutter head.

Cannot get laser to exit in centre, no matter where I get it to enter.

Enters in centre, leaves at an angle, part of beam hitting air assist cone.

How can this be possible.



Tried with 18mm lens and then 12mm lens with the spacer for holding 12mm.

Tried two different mirrors.



If it's entering in the centre how can it possibly leave at an angle is what I'm trying to figure out.any ideas appreciated cheers.



Anyone else with saite can maybe advise if I have installed it wrong? 



see last pic for where beam is exiting.

right on edge of exit cone. way off centre.



![images/45e7095def62d4a6b239bf6135bcfbda.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/45e7095def62d4a6b239bf6135bcfbda.jpeg)
![images/16b9633cf3d37320c14c2293aa568eb1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/16b9633cf3d37320c14c2293aa568eb1.jpeg)
![images/33e1b90c107862b8d55f4c834f23ed09.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/33e1b90c107862b8d55f4c834f23ed09.jpeg)

**"Pippins McGee"**

---
---
**Alex Krause** *August 01, 2016 04:35*

I think your new head isn't perpendicular to the gantry


---
**Anthony Bolgar** *August 01, 2016 04:38*

You need to shim the two screws that hold it to the carriage. It is not sitting exactly perpindicular to the beam. Try raising one side or the other until it is exiting correctly.


---
**Anthony Bolgar** *August 01, 2016 04:40*

You can test this without shimming by just twisting the head by hand a little to the left or right and watching where it exits. Then shim accordingly. Hope this helps you.


---
**Pippins McGee** *August 01, 2016 05:05*

**+Alex Krause** **+Anthony Bolgar**  thank you both for your time.

I can adjust the where the laser leaves the head in terms of Y direction by twisting head left and right.

But to adjust where it leaves in terms of X direction, do you mean I should shim one of these two screws, to raise the carriage on one side?

[http://oi66.tinypic.com/20fuqt5.jpg](http://oi66.tinypic.com/20fuqt5.jpg)



I loosened those screws and tilt the top-plate towards the right a bit and find it exits more in the centre..

is that what you meant by shimming the screws? are those the screws you meant?

as that seems to help fix it.



thanks..


---
**Anthony Bolgar** *August 01, 2016 05:27*

Exactly.


---
**Phillip Conroy** *August 01, 2016 05:33*

any air assist nozzel adds an extrax item to align,on mine i just drillrd/filed side that the beam was hitting.use tape on bottom of air assist nozzel to check


---
**Anthony Bolgar** *August 01, 2016 07:02*

Philip, your method may work, but is a kludge fix. The best way is to actually shim the mount until it is perpendicular to the beam.


---
**Pippins McGee** *August 01, 2016 08:12*

**+Anthony Bolgar** thanks guys.

I added one washer underneath left-hand screw on the mounting plate, and now laser exits the nozzle in the centre! thank you.

i never would've thought to do that....

cheers


---
**Mircea Russu** *August 01, 2016 12:57*

**+Pippins McGee** just check in every corner, my xy gantry was twisted, you might need to fix that first if you have the same condition.




---
**Pippins McGee** *August 02, 2016 13:20*

All good now, yatta! cheers.

Got to say, the saite_cutter head makes for cutting thicker materials much easier.

its height adjustment allowing to drop in height in between each pass on 9mm MDF, now allows me to cut it in 3 low power passes.



Used to have to do 4-5 passes with the Lightobject head.

Also results in a much straighter cut.



Can adjust focus distance between paper and 9mm MDF on the fly now - much easier than making my bed adjustable height..


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/YnDZA89vtCR) &mdash; content and formatting may not be reliable*
