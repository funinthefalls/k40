---
layout: post
title: "Testing a mock-up of a clock face as a gift for a friend"
date: October 06, 2018 22:40
category: "Object produced with laser"
author: "James Rivera"
---
Testing a mock-up of a clock face as a gift for a friend. I used #FreeCAD and #Inkscape to create the SVG file for #K40Whisperer.

![images/15b93aa7883fb68d23e7fafd29525397.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15b93aa7883fb68d23e7fafd29525397.jpeg)



**"James Rivera"**

---
---
**HalfNormal** *October 06, 2018 23:07*

Very nice!


---
**James Rivera** *October 07, 2018 02:49*

**+HalfNormal** Thanks! Tango dancing is her favorite pastime, and she just moved and needs a living room clock, so this seemed like a good idea.


---
**ottolaser share** *October 10, 2018 13:01*

good


---
**Nigel Conroy** *November 07, 2018 20:57*

Dot mode will give great results when cutting cardboard and greatly reduce the risk of fire. Russ Sadler did a great video on it. It does slow the cut rate down


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/U3qfhjr2dXP) &mdash; content and formatting may not be reliable*
