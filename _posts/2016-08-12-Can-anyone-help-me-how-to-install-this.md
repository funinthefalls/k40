---
layout: post
title: "Can anyone help me how to install this? :("
date: August 12, 2016 03:38
category: "Modification"
author: "Travel Rocket"
---
Can anyone help me how to install this? :(

![images/ea445193a9c0bdbbdae225a73d84f89f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ea445193a9c0bdbbdae225a73d84f89f.jpeg)



**"Travel Rocket"**

---
---
**David Cook** *August 12, 2016 19:21*

I installed that on my machine,  what's the issue ?  I can help if you tell me what exactly you are having trouble with




---
**David Cook** *August 12, 2016 19:25*

[https://drive.google.com/open?id=0ByNecjb0RD-TbVc2SVI5R2tWQ0k](https://drive.google.com/open?id=0ByNecjb0RD-TbVc2SVI5R2tWQ0k)


---
**Travel Rocket** *August 13, 2016 07:41*

I posted my Laser head , i really wanted to know how to attach this stuff on this.



[https://lh3.googleusercontent.com/_GCBrjRVQY9V3oe_1iQ8uoSQMMZvVvotZcEUIehUbcCKiaCM1jsl9CznkiNbEdShOeqL204Brd5vu_G6hRsjbY-Pt4Vhopj0jLU=s0](https://plus.google.com/photos/100865688973733022249/albums/6318214256697853617?sqi=118113483589382049502&sqsi=5c5218e7-4c52-4f3f-ab90-a7db94060f25)



Can you recommend any air assist for this?


---
**Ulf Stahmer** *August 13, 2016 11:57*

There are lots of tapped holes on the mirror and lens holder on your open head design.  To mount the traditional head, you need an L bracket with holes on the vertical surface to attach it to the machine and one on the horizontal surface to install the new head.  I'm happy with my open head design, so I haven't converted.  I'm sure someone has made a conversion bracket somewhere.


---
*Imported from [Google+](https://plus.google.com/100865688973733022249/posts/Hmsr4fx4fWu) &mdash; content and formatting may not be reliable*
