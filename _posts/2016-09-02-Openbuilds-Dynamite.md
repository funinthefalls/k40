---
layout: post
title: "Openbuilds Dynamite :)"
date: September 02, 2016 00:35
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
Openbuilds Dynamite :)

![images/84516b0df86d3b1df2aa6c62837e5c50.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/84516b0df86d3b1df2aa6c62837e5c50.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Alex Krause** *September 02, 2016 01:04*

Are you out of stuff to laser on again?


---
**Ray Kholodovsky (Cohesion3D)** *September 02, 2016 01:04*

Key word being again. 


---
**Ariel Yahni (UniKpty)** *September 02, 2016 01:06*

I could not resist


---
**Alex Krause** *September 02, 2016 01:07*

I was about to warn everyone to hide all their cookies... 


---
**Ariel Yahni (UniKpty)** *September 02, 2016 01:09*

Well I do have plenty of other stuff, but what's the fun in that

![images/96ef7781ad5619414eec1f32fba0a80a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96ef7781ad5619414eec1f32fba0a80a.jpeg)


---
**Alex Krause** *September 02, 2016 01:10*

Nice ply stash :) 


---
**Mircea Russu** *September 02, 2016 07:52*

rotary?




---
**Ariel Yahni (UniKpty)** *September 02, 2016 11:32*

**+Mircea Russu**​ in this particular case, no. But if you look into my profile or YouTube you will find some tests on a rotary running with smoothie 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/VNXxvWtwFFG) &mdash; content and formatting may not be reliable*
