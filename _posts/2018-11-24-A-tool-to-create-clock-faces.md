---
layout: post
title: "A tool to create clock faces"
date: November 24, 2018 16:56
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
A tool to create clock faces.



[https://clocktickgenerator.makertoolmaker.com/](https://clocktickgenerator.makertoolmaker.com/)







**"HalfNormal"**

---
---
**James Rivera** *November 24, 2018 17:40*

That’s pretty cool!


---
**Stephane Buisson** *November 25, 2018 07:22*

I love all this tools, I am dreaming at what I can do like when I am in a DIY shop ;-))



DIY shop syndrome hit again.



Thank you **+HalfNormal**


---
**HalfNormal** *November 25, 2018 15:09*

**+Stephane Buisson** you are more than welcome!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/iXvTyEdfvA3) &mdash; content and formatting may not be reliable*
