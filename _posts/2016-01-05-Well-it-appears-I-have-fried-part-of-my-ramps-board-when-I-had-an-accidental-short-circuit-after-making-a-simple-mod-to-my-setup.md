---
layout: post
title: "Well it appears I have fried part of my ramps board when I had an accidental short circuit after making a simple mod to my setup"
date: January 05, 2016 15:05
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
Well it appears I have fried part of my ramps board when I had an accidental short circuit after making a simple mod to my setup. My X axis will no longer work. I have replaced the a4988 driver, but no luck. The stepper is OK , I switched the cable to the Y axis and it works fine. Is there anyway of remapping the X axis to use the Z axis until I can get a replacement board?





**"Anthony Bolgar"**

---
---
**Sean Cherven** *January 05, 2016 15:53*

I can send you a replacement RAMPS Board (Brand New) for $14 Shipped if you would like. I can get it out to you ASAP.



As for remapping the X Axis to Z, yes, it can be done, but must be configured in the firmware. I don't use the firmware for RAMPS, so I can't be of anymore help on that subject.


---
**Anthony Bolgar** *January 05, 2016 16:01*

Sounds great Sean, I live in Niagara Falls, Ontario Canada, how long do you think it would take to get here?


---
**Sean Cherven** *January 05, 2016 16:49*

Ahh, you live in Canada? Silly me for thinking everybody lives in the USA.

Let me just double check on the shipping costs.


---
**Sean Cherven** *January 05, 2016 17:01*

Okay, it looks like it's going to cost around $8 to ship it. So I can do it for $19 Shipped.



And it should take about a week for you to receive it.


---
**Anthony Bolgar** *January 05, 2016 17:07*

Thanks for the info Peter. And Sean, I will get back to you later today. Thank you to both of you.


---
**Sean Cherven** *January 05, 2016 17:13*

No problem! I'm just trying to help. The ramps board is a spare that I have laying around. I just figured I'd offer it to ya.


---
**Sean Cherven** *January 05, 2016 17:16*

**+Peter van der Walt** are you located in Canada as well?


---
**Sean Cherven** *January 05, 2016 17:24*

Okay, sounds good! Shipping to the USA is cheaper. I can do $14 shipped to the USA, or $19 shipped to Canada (price to Canada may vary, I only did a quote to Niagara Falls, so I don't know if the shipping cost to Canada will vary by location.)


---
**ChiRag Chaudhari** *January 06, 2016 23:50*

**+Peter van der Walt** Wow its that easy to map the pins. I wanted to know if I can use two stepper motors to drive Y axes since I am building 1000mm X 600mm machine with 10mm rods. And they are heavy. So now I can use any of the Z2, E0 or E1 to mimic Y axes.


---
**Anthony Bolgar** *January 09, 2016 15:01*

1000mm is pretty big to only use 10mm rods. You will find that they will flex quite a bit in the middle with very little load on them. 10mm would be ok if it is a fully supported rail, however with just end supports you might not be happy with the lack of stiffness. 




---
**Anthony Bolgar** *January 09, 2016 15:04*

And I finally managed to find the time to remap the pins on my ramps board.....the x and z axis' were both fried so I managed to get it to work by remapping x to extruder 0. I also updated the lcd splash screen with my own personal info and graphic. Will post a pic later when I am near the laser. When my new ramps board comes in, I will just keep it as a spare, since the remap took care of my issues.


---
**Anthony Bolgar** *January 09, 2016 16:03*

Thanks Peter.


---
**ChiRag Chaudhari** *January 09, 2016 17:39*

**+Anthony Bolgar** I haven't tried to put them together yet, but yeah 1000x10mm rods are pretty heavy. I'm thinking they will not just bent in the center by their own weight but will also bend the 600mm rods. 

I am going to need to find some half circle linear bearings so that I can support them from underneath. Also instead of using X=1000mm and Y=600mm I must swap them. I think I did ordered 1000mm extruded aluminum as well, but linear bearings makes life so easy using rods.



PS: playing with codes here and there is fun for sure. Would love to know what you did and how as well. I was actually thinking about leaning the entire firmware as it has so many unnecessary things that is only good for 3d printers.


---
**ChiRag Chaudhari** *January 09, 2016 17:45*

**+Peter van der Walt**​ well honestly I haven't even used linear bearings yet, bought all those parts in Nov 2014 to make 20x30" bed diode laser cutter. But then got super busy building FPV racers and my eBay business skyrocked.

Epilog laser at the university has been so busy lately so purchased K40 and now I'm thinking need to get back to the good old project. I can use K40 to cut precise parts.

﻿


---
**ChiRag Chaudhari** *January 09, 2016 17:51*

PS: Peter that's something actually I wanted to talk to you about. I was gonna write to you but sill haven't got good couple hours to draft it. I should just call you up and talk and it.


---
**ChiRag Chaudhari** *January 09, 2016 18:21*

Hit me up after coffee! 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/3WSv5TLyGrm) &mdash; content and formatting may not be reliable*
