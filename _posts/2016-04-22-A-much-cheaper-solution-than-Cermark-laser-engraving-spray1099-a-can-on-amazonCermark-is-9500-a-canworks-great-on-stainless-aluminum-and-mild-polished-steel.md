---
layout: post
title: "A much cheaper solution than Cermark laser engraving spray...10.99 a can on amazon....Cermark is 95.00 a can....works great on stainless, aluminum and mild polished steel"
date: April 22, 2016 18:07
category: "Discussion"
author: "Scott Thorne"
---
A much cheaper solution than Cermark laser engraving spray...10.99 a can on amazon....Cermark is 95.00 a can....works great on stainless, aluminum and mild polished steel.

![images/40d35656854d1f55f8394a6714086517.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40d35656854d1f55f8394a6714086517.jpeg)



**"Scott Thorne"**

---
---
**Jim Hatch** *April 22, 2016 18:22*

Same process used? Spray it on and lase it? Does it work on other materials like ceramic, glass or stone? How'd you figure that out? :-)


---
**Scott Thorne** *April 22, 2016 18:29*

A friend of mine owned a laser engraving shop and he told me about it...said he uses it all the time...not sure about the stuff you mentioned **+Jim Hatch**...but I've tried it on all the materials that I've mentioned. 


---
**Jim Hatch** *April 22, 2016 18:30*

I'll order some and play :-) I'll report back.


---
**Jim Hatch** *April 22, 2016 18:36*

Have you tried the Quick Dry version too or just the standard?


---
**Alex Krause** *April 22, 2016 18:48*

You should test the spray on dry graphite lube as well. I don't know if it works but it's carbon based 


---
**Scott Thorne** *April 22, 2016 19:30*

**+Jim Hatch**...sorry man..I didn't answer your question...yes..spray on and let dry then lase....beware though....50mm/s at 12mAs....when when rastering....when using cut line 50mm/s at 8mA's


---
**Scott Thorne** *April 22, 2016 19:57*

**+Alex Krause**...how much is it? 


---
**Jim Hatch** *April 22, 2016 21:09*

The PB Blaster folks have Dry Film Spray Graphite for $4.25 at my Home Depot. Gonna try that while I wait for my CRC can to come in from Amazon.


---
**Alex Krause** *April 22, 2016 21:14*

**+Scott Thorne**​ I've seen it as low as 3$ at the autoparts store


---
**Scott Thorne** *April 22, 2016 22:12*

**+Jim Hatch**...it will only work if the base is molybdenum. 


---
**Scott Thorne** *April 22, 2016 22:13*

**+Alex Krause**...thanks for the heads up brother...I'm gonna get some and try it. 


---
**Casey Cowart** *April 22, 2016 23:27*

I tried dry moly lube on aluminum and it would wipe off.  I had my 40w laser pwr cranked up and tried slowing down speeds but never got it to bond properly... Any idea what I'm doing wrong?


---
**Scott Thorne** *April 22, 2016 23:51*

 **+Casey Cowart**..I have no idea...I rastered it on aluminum and stainless at 50mm/s...at 12mA's...and it bonded so good I couldn't scratch it off with brillo. 


---
**Casey Cowart** *April 22, 2016 23:57*

Thanks **+Scott Thorne**​, I'll do another run and see if I can get better results.


---
**Scott Thorne** *April 23, 2016 00:00*

**+Casey Cowart**...make sure you let it dry very well...I used my wife's hair dryer .....really burn it in when you engrave.


---
**Casey Cowart** *April 23, 2016 00:02*

Good to know!!  I'm not sure I dried it nearly that thoroughly.  Hopefully that will do the trick.


---
**Phillip Conroy** *April 23, 2016 09:26*

Can not find crc dry moly in Australia,and usa ebay sellers will not ship here.


---
**Scott Thorne** *April 23, 2016 11:46*

**+Phillip Conroy**...I would try any molybdenum...

Spray lube...that's the base needed for adhesion. 


---
**Alex von Horn** *April 24, 2016 05:18*

Try Blackwood's for a source in aus. Picked some up the other day. Expect to pay $40 or so. 


---
**Phillip Conroy** *April 24, 2016 09:41*

Thank you alex ,i will keep that in mind,in the mean time brought some moly grease for $13 for a tin will try that first-i am also going to try and melt powdered glass with laser unfocased at first[without fo al lens] any one know the melting pint of glass and what temps the laser beam is


---
**Ariel Yahni (UniKpty)** *April 24, 2016 14:55*

So miracles seem to happen and found this at my local store [https://imgur.com/1QYOm70](https://imgur.com/1QYOm70)  should I get the quick dry? 


---
**Scott Thorne** *April 24, 2016 15:06*

**+Ariel Yahni**...I'm not sure about that...the regular stuff dries in 20 minutes....or use a hair dryer and it's dry in 3 minutes


---
**dstevens lv** *April 25, 2016 01:49*

The CRC dry moly spray lube is available in the US at your local Grainger for about US$14 a can.


---
**Bill Parker** *April 25, 2016 19:10*

When I have read about this it stated it needed more power than 40w I only have a K40 and it it is said they only put out 35w so I would read up on it before spending time and money on it.


---
**Scott Thorne** *April 26, 2016 09:35*

**+Bill Parker**...more power equals slow speeds...I only have a 50 watt...it works great at 12 mA/s


---
**dstevens lv** *April 26, 2016 19:51*

I bought a couple of cans, one from a local supplier called Spray On LU200 and a can of the CRC from Grainger.  There is a difference in viscosity with the CRC working a bit better.  Steel and stainless it works pretty well.  Not the color depth of Cermark but a nice raster.  On AL I don't quite have the settings right and getting there is more time consuming than it was with the Cermark AL product.  All in all it's a good low cost hack.


---
**Stuart Rubin** *April 27, 2016 18:07*

I like the price of that! Can you please share some pics of your results?


---
**Scott Thorne** *April 27, 2016 20:34*

**+Stuart Rubin**...go to my Google page...there is a pic of my results there. 


---
**Stuart Rubin** *May 01, 2016 23:14*

Does anyone think this would work? 

[http://www.mcmaster.com/#dry-moly-lubricants/=12866fb](http://www.mcmaster.com/#dry-moly-lubricants/=12866fb)
It's fairly cheap and it happens that McMaster is a short drive from my house (close than a Grainger, etc.)

It has molybdenum disulfide but also Teflon.

Thoughts?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 18, 2016 06:53*

**+Phillip Conroy** **+Alex von Horn** Just found this in Australia, at SuperCheapAuto.

[http://www.supercheapauto.com.au/online-store/products/Liqui-Moly-Mos-2-Anti-Friction-Engine-Treatment-300mL.aspx?pid=156706#Recommendations](http://www.supercheapauto.com.au/online-store/products/Liqui-Moly-Mos-2-Anti-Friction-Engine-Treatment-300mL.aspx?pid=156706#Recommendations)



Only $17.69. Going to go buy some now before they shut & see if it works later tonight. It is MoS2 (Molybdenum Disulfide) based, like the CRC stuff **+Scott Thorne** uses. Hopefully it works. I'll post results later today.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 18, 2016 08:32*

**+Scott Thorne** Quick question: how long does the moly take to dry? I hit it with heat gun for a few minutes & the stuff I got was still liquidy. The stuff I got is not aerosol though, it's a liquid grease stuff, about the consistency of gravy.


---
**Scott Thorne** *May 18, 2016 09:44*

**+Yuusuf Sallahuddin**...about 5 minutes.


---
**dstevens lv** *May 18, 2016 18:00*

**+Yuusuf Sallahuddin** That's not dry moly lube.  We're using dry molly lube.  I don't know if what you have will work.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 18, 2016 19:52*

**+dstevens lv** Yeah I was rushing because the store was about to close, so I grabbed this stuff. It did partially work, even when 100% wet. I've left some on the stainless steel overnight to see if it will dry out a bit & then I will try it again.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/h1VFL9WdTCY) &mdash; content and formatting may not be reliable*
