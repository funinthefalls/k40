---
layout: post
title: "Anyone care to share their basic \"starter\" settings for cutting/engraving/rastering?"
date: May 09, 2016 21:33
category: "Hardware and Laser settings"
author: "Custom Creations"
---
Anyone care to share their basic "starter" settings for cutting/engraving/rastering? I just got a pile of scrap acrylic from the local Lowes and I want to advance from cardboard..Much appreciated :) 



TIA

![images/a335f8979d9b7ff3b260c49ab3024a1c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a335f8979d9b7ff3b260c49ab3024a1c.jpeg)



**"Custom Creations"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 21:44*

Never done acrylic myself, so I am also interested to know.


---
**Joe Keneally** *May 09, 2016 21:49*

How did you get the scrap?  Did you have to pay or was it free?  Just curious!




---
**Custom Creations** *May 09, 2016 21:55*

**+Joe Keneally** The 6 larger pieces on the bottom (the ones not wrapped) I paid $1-3 each for. The wrapped pieces were free.. Basically you go to your local Lowes or Home Depot and check out their bin where the stuff is cut. They are usually happy to get rid of it and maybe make a few bucks in the process. I paid less than $10 for that whole pile.. ;)


---
**Pigeon FX** *May 09, 2016 22:12*

I have been doing 3mm Acrylic @ ~8/10mA (30%) 8mm/s for cuts, and same power but 100-200mm/s for engraving. happy with the results. 


---
**Imko Beckhoven van** *May 09, 2016 22:13*

Do you know the type of the acrylic, is it cast or pulled?


---
**3D Laser** *May 10, 2016 03:04*

Be very careful from what I know that stuff isn't laser safe and you might harm yourself cutting it specially if you have not upgraded your exhaust 


---
**3D Laser** *May 10, 2016 03:09*

From my experience cast acrylic has the paper backing that is safe to cut extruded acrylic while "safe" and I use that loosely it smells aweful


---
**Custom Creations** *May 10, 2016 03:14*

**+Corey Budwine** Yes, this stuff has the clear tape coating on it and it does smell like superglue when cutting..


---
**3D Laser** *May 10, 2016 03:21*

**+Custom Creations** I would stay clear of it if I was you.  I cut a few pieces of extruded acrylic and I had to clear out my house for a few hours because it smelled so bad.  I know it's more expensive than the scrap Ben but you can get 12 X 12 sheets of cast acrylic for 3.70 - 6.00 on eBay let me know if you want a good sellers information plus if it prevents you from getting the "black lung" it's well worth it 


---
**Custom Creations** *May 10, 2016 03:28*

**+Corey Budwine** Could always use a good source ;)


---
**3D Laser** *May 10, 2016 11:01*

I use eBay seller sbautomation678 and plastics_2000_usa like I said there sheets are anywhere from 3.70 to 5.95. sbautomation678 will give you a deal and ship 21 pieces flat rate which saves a ton on shipping 


---
**Joe Keneally** *May 11, 2016 15:44*

Thanks for the info!


---
**Greg Curtis (pSyONiDe)** *May 11, 2016 20:08*

I use lots and lots of cast acrylic in mine. I run 9-10mA 400mm/sec on raster engraves, same power, 35mm/sec for vector engraves (thin lines) and 15mA 12-15mm/sec for cuts. My cuts vary because some of my pieces get engraved to the cut edge, making the material alighting thinner.



Hopefully this is more in tune with what you were asking for. I have also worked with extruded acrylic, but prefer cast for a few reasons, including less smell, sharper/more consistent engraving results (seems like cast may be slightly harder?) and the paper peels easier/cleaner than the plastic coatings.﻿


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/iQXJZSWdNwJ) &mdash; content and formatting may not be reliable*
