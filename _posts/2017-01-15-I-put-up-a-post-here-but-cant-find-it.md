---
layout: post
title: "I put up a post here but can't find it !!!!"
date: January 15, 2017 21:06
category: "Original software and hardware issues"
author: "Jason Farrell"
---
I put up a post here but can't find it !!!! 





**"Jason Farrell"**

---
---
**Ned Hill** *January 15, 2017 21:33*

Probably in the group spam folder.  Happens sometimes.  Need a group Mod to release it.  


---
**Jason Farrell** *January 15, 2017 23:11*

Can I ask here or do I need to message someone ? 


---
**Ned Hill** *January 15, 2017 23:41*

**+Alex Krause**​ **+Yuusuf Sallahuddin**​ **+Peter van der Walt**​  one of these peeps should be able to handle it.


---
**Alex Krause** *January 16, 2017 02:16*

Unfortunately I am not a moderator of this group :) thanks **+Ned Hill**​ I am a moderator of the LaserWeb group though ;) 


---
**Ned Hill** *January 16, 2017 02:18*

Ahhh, ok **+Alex Krause** thanks any way ;)


---
**Alex Krause** *January 16, 2017 02:19*

No worries, I appreciate the tag :) shows that people remember me 


---
*Imported from [Google+](https://plus.google.com/110053066297964291796/posts/ZSxB1uwivDn) &mdash; content and formatting may not be reliable*
