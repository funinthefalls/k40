---
layout: post
title: "It is actually quite incredible to me the amount of information available about the K40 and other similar lasers that has been compiled in the last 2 years or so"
date: September 12, 2017 03:15
category: "Discussion"
author: "Anthony Bolgar"
---
It is actually quite incredible to me the amount of information available about the K40 and other similar lasers that has been compiled in the last 2 years or so. There are multiple open source software options now, a variety of plug and play replacement controllers, as well as a very large community pushing the limits of these Chinese lasers which were until recently considered to be an ill advised purchase. I have to thank all that have contributed to the vast information pool we now have to work from. Special thanks to **+Stephane Buisson** for starting this G+ community, and to people like **+Don Kleinschnitz** who have done some serious research on these machines.





**"Anthony Bolgar"**

---
---
**Kelly Burns** *September 12, 2017 03:24*

Amen!  Many K40s would be boat anchors and parts donors if it wasn't for these people!  


---
**Jim Fong** *September 12, 2017 04:48*

I blame it on a lightning strike which killed the original controller in my k40.  I probably wouldn't be here if it wasn't for Ray and his Cohesion3d replacement board.  All of you guys have been great!!!


---
**Don Kleinschnitz Jr.** *September 12, 2017 13:09*

Thank guys, I enjoy solving tough problems and helping. This is the way I learn and get access to fab technology that I can maintain. Lately I have been distracted by summer projects and an OX.....


---
**Don Kleinschnitz Jr.** *September 12, 2017 13:11*

**+Anthony Bolgar** so you have a new machine :) How does it compare? I assume that like the K40 there are no schematics for anything? Pictures of the subsystems would be fun and create more "drool" factor from us all :)!

How many of the upgrades that we do are in that stock machine.


---
**Stephane Buisson** *September 12, 2017 14:56*

Thanks**+Anthony Bolgar** for your kind words & contributions.

The initial K40 upgrade was a tech pleasure to do. 

Myself I would like to thanks **+Arthur Wolf** for his help to Smoothiefy my K40, and create a well justified trend for upgrading our laser. 

Thank you Peter van der Walt to have launched Laserweb after that and **+Claudio Prezzi** and other devs to keep on.

 to **+Ray Kholodovsky** for his development C3D Mini plug in board replacement.

And big thanks to **+Don Kleinschnitz** and all of you to make this community a success.

I am extremely busy at the moment to build my house and I shouldn't plug back my k40 (OX, 3D printer, ...) under my new roof before next year. I just keep an eye on the spam and I am sorry if some post aren't released fast enough. keep on the good work ...



![images/435cfb4e4926d51ede3d6918edc4ec5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/435cfb4e4926d51ede3d6918edc4ec5b.jpeg)


---
**David Cook** *September 14, 2017 19:28*

It is incredible how much information has been compiled in that amount of time. !  


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/E8N4GxXyPox) &mdash; content and formatting may not be reliable*
