---
layout: post
title: "I find lots of neat things I think about making on my laser"
date: March 18, 2017 18:31
category: "Repository and designs"
author: "timb12957"
---
I find lots of neat things I think about  making on my laser. Lots of the time, the graphic is not in a format that will open or import into Corel laser. One such example is the bird in this picture. It came from Thingverse, but the STEP format does not work with Corel. Can someone offer some tips as to how to go from various drawing formats to workable laser cutting files?

![images/532983a8b909bc87811132ce002b689a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/532983a8b909bc87811132ce002b689a.jpeg)



**"timb12957"**

---
---
**Don Kleinschnitz Jr.** *March 18, 2017 19:55*

Did you see the attached video? Perhaps helps with the LW4 part of the tool chain.




{% include youtubePlayer.html id="PylIwah4jPs" %}
[youtube.com - Inkscape design for CAM operations in LaserWeb4](https://youtu.be/PylIwah4jPs)



..also check out the LW4 web site for whatever tool you are using.

.........................

Frankly, I abandoned Corel (I had lot of compatibility problems), could have been the operator ??.

..........................



My tool chain is:



.............Source & design .............

...Images = Gimp

...Vector based & hybid projects = Inkscape (I can pull images from Gimp imported to  Inkscape for hybrid files that include: vector, cutting & engraving)

... CAD= Sketchup + (DXF + Phlatboys CAM plugins)



* I use this only when I have complex parts that need kerf adjustments.



....................Conversion......................

123D design = to convert various file types like STEP to STL. Use SU STL plugin to import.



Various online services ... [http://www.3dtransform.com](http://www.3dtransform.com)



....Gcode generation & delivery......



LW4= & I use some of its simple CAM operations



................................



The STEP file of the bird above would have to be broken into cut-able parts. You can convert directly to separate SVG parts using 123D design. Then load the parts into LW for cutting.

I would have expected the design files to be separate SVG's vs one STEP file.




---
**timb12957** *March 18, 2017 23:56*

Very good, thanks guys!


---
**Stephane Buisson** *March 19, 2017 13:02*

nice

file is there :[http://www.thingiverse.com/thing:41823](http://www.thingiverse.com/thing:41823)



[thingiverse.com - Bird ready for laser cutting or 3d printing.  by hexleyosx](http://www.thingiverse.com/thing:80577)


---
**Don Kleinschnitz Jr.** *March 19, 2017 14:55*

**+Stephane Buisson**  bingo! all the work is done ;)


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/2FkYsmsJTyR) &mdash; content and formatting may not be reliable*
