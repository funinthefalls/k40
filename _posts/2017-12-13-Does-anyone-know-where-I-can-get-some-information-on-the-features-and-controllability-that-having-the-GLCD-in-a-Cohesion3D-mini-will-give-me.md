---
layout: post
title: "Does anyone know where I can get some information on the features and controllability that having the GLCD in a Cohesion3D mini will give me?"
date: December 13, 2017 03:46
category: "Modification"
author: "David Allen Frantz"
---
Does anyone know where I can get some information on the features and controllability that having the GLCD in a Cohesion3D mini will give me? 





**"David Allen Frantz"**

---
---
**Ned Hill** *December 13, 2017 05:29*

There is a G+ community for the C3D [Cohesion3D](https://plus.google.com/u/0/communities/116261877707124667493)  and a facebook group [https://www.facebook.com/groups/1045892858887191/](https://www.facebook.com/groups/1045892858887191/)    where you could also ask.


---
**Don Kleinschnitz Jr.** *December 13, 2017 12:35*

This kind of information comes from the Smoothie firmware side which the C3D is compatible with. It depends on what level firmware you are running and as I recall the docs are pretty sketchy. Try the smoothie forum. 



That said, I found the panel capable of controlling all the fundamental functions of the machine which made it worth it to me. This is especially true when troubleshooting or running the machine stand alone as you do not need a host to control the machine. 


---
**David Allen Frantz** *December 14, 2017 15:06*

**+Don Kleinschnitz** when running the machine untethered do you put the SVG files on the SD card in the controller or on the back of the GLCD?


---
**Don Kleinschnitz Jr.** *December 14, 2017 15:37*

On the SD card, running from the GLDC is purported to be problematic.


---
**David Allen Frantz** *December 14, 2017 15:46*

Right on. Thanks Don!


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/KUp35erou8Y) &mdash; content and formatting may not be reliable*
