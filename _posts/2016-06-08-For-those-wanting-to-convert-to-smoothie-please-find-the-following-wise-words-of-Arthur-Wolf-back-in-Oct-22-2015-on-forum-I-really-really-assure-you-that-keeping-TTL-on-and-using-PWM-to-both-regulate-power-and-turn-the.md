---
layout: post
title: "For those wanting to convert to smoothie please find the following wise words of Arthur Wolf back in Oct 22 2015 on forum: \"I really really assure you that keeping TTL on and using PWM to both regulate power and turn the"
date: June 08, 2016 12:32
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
For those wanting to convert to smoothie please find the following wise words of **+Arthur Wolf** back in Oct 22 2015 on [smoothieware.org](http://smoothieware.org) forum:



"I really really assure you that keeping TTL on and using PWM to both regulate power and turn the laser off works fine, and is what <b>at least</b> a hundred users are doing."



"Smoothie lowers laser power ( via PWM ) while accelerating and decelerating so that power-per-distance is constant, so you <b>need</b> to be controlling the laser with pwm. TTL should be used only as a secondary feature."





**"Ariel Yahni (UniKpty)"**

---
---
**Arthur Wolf** *June 08, 2016 12:33*

And like ... I was correct right ? I didn't mess up or anything ? Haven't touched a laser in a long time.


---
**Ariel Yahni (UniKpty)** *June 08, 2016 12:34*

Until my laser explodes you will be correct.


---
**Arthur Wolf** *June 08, 2016 12:35*

Well if I don't hear from you i'll know what happened


---
**Vince Lee** *June 08, 2016 21:02*

FYI, I found that you can also use PWM to control the TL line instead of the IN line; which was more convenient for me since I wanted to keep the potentiometer connected to the IN line for backwards compatibility with the old control board and also allow interactive tweaking of the power level while cutting.  I just had to adjust the PWM period in the config file to get a smooth grayscale ramp.


---
**Alex Krause** *June 09, 2016 06:07*

**+Ariel Yahni**​ can you help me figure out where I need to connect the 2.4 pwm pin to the power supply we have 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/FuzsbjuMCQM) &mdash; content and formatting may not be reliable*
