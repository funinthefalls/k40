---
layout: post
title: "FINALLY! After about three weeks of tinkering mishaps and a lot of plywood I finally got my laser to cut through plywood like butter!"
date: February 28, 2016 04:33
category: "Discussion"
author: "3D Laser"
---
FINALLY!  After about three weeks of tinkering mishaps and a lot of plywood I finally got my laser to cut through plywood like butter!  I am staring to be impressed by this machine rather than frustrated my only issue now is how loud it is.  One thing at a time though 





**"3D Laser"**

---
---
**ThantiK** *February 28, 2016 04:53*

What electronics setup are you using?


---
**Jason Reiter** *February 28, 2016 05:35*

What did you do because it takes me about 12 passes on 3mm ply ? 


---
**Anton Fosselius** *February 28, 2016 06:37*

**+Jason Reiter** what? I cut 3mm birch plywood in one pass. But it's slow and the cut becomes black.


---
**Tony Sobczak** *February 28, 2016 07:41*

1/4" plywood in two passes. 


---
**Scott Thorne** *February 28, 2016 12:02*

3/8 in one pass


---
**3D Laser** *February 28, 2016 12:04*

**+Jason Reiter** I was like that to check your focal length I was about a 1/8 of an in off when I adjusted it cut through no problem


---
**3D Laser** *February 28, 2016 12:05*

**+ThantiK** stock nano board and laserdrw


---
**Jason Reiter** *February 28, 2016 13:22*

I assume to check the focal point do like the ramp test? What speed and power are you useing. for the 3mm ply


---
**3D Laser** *February 28, 2016 13:55*

I did the ramp test .  I did 10ma at 5ms and it was to much.  It burnt it up pretty good. I think I could get through it now. 8ma at 8ms I am going to try this later on today 


---
**Jason Reiter** *February 28, 2016 15:06*

Corey thanks I readjusted my focal length and cut one pass at 5mms @ 10ma. Wow what a small change makes


---
**Tony Sobczak** *February 28, 2016 15:51*

How does one adjust the focal length?


---
**3D Laser** *February 28, 2016 15:58*

You adjust the distance from the material to the laser head I just threw a scrap piece of 1/8 ply under the material I was cutting worked like a charm


---
**Jason Reiter** *February 29, 2016 13:49*

The standard focal point for min was 50.8mm So I cut a peice of scrap at 50.8mm for a gauge and then I worked from there to fine tune by doing the ramp test . Min cuts  3mm ply perfect with the grain and close against the grain on 5mms @10ma. Still working on it 


---
**Jason Reiter** *February 29, 2016 13:57*

Corey Budwine  My rails are still out a little bit when the head is on the far right side the laser wont lighn up.  about 70% of the cutting area is good . I have tried to tweak but it is when I get it closer the others gets out of wack. Seems impossiable to have the whole area alighn right. I have not compleately removed the rails yet as I was trying to keep from this. Any suggestions?


---
**3D Laser** *February 29, 2016 18:40*

**+Jason Reiter** sounds like your rails are still not square.  I was like that for a while too.  Start again with you first mirror use it to aim for as close to the center as possible to the second mirror.  At this point I had those four screws lossened so I could adjust the frame to help get the beam center both at the top of the rail and at the bottom of the rail. The thing to remember that helped me was to adjust two screws at the same time.  adjusting the top at the same time will raise and lower the the beam.  Adjusting the side will move it left are right adjust just one for fine tuning.  From that point it's a matter of trial and error.  Keep at it and it will click.  I used a lot of masking tape to get it right.  Even now it's not perfect.  Check for one of my earlier post there is a guy named Scott that helped me a ton with his post going step by step.  From what you are describing the beam is not hitting the hole at the farthest point put a large piece of tape on the lens holder to see where the beam is hitting and adjust to get it in the center 


---
**Jason Reiter** *February 29, 2016 19:14*

LOL I found your post and downloaded the instruction that is very helpfull I felt like you did in that post the other night trying to adjust more. Just when I thin I am right there I find out I am far away but I am getting closer thanks to the people on here like you .


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/C53ejxHtKoY) &mdash; content and formatting may not be reliable*
