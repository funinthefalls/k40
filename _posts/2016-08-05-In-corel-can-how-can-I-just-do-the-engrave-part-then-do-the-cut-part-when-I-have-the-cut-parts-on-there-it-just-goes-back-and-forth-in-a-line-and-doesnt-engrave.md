---
layout: post
title: "In corel can how can I just do the engrave part then do the cut part when I have the cut parts on there it just goes back and forth in a line and doesn't engrave"
date: August 05, 2016 01:31
category: "Discussion"
author: "David Spencer"
---
In corel can how can I just do the engrave part then do the cut part when I have the cut parts on there it just goes back and forth in a line and doesn't engrave





**"David Spencer"**

---
---
**Bob Damato** *August 05, 2016 01:55*

The way I do it is put your cut parts on one layer and your engrave parts on another layer. Then 'print' selection only (not whole page). 

Bob


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 02:04*

Set them as separate tasks. 1st, add the engrave to the task queue (with starting unticked). Then add the cut to the task queue (you can tick starting now if it is your final task to add).


---
**Eric Flynn** *August 05, 2016 02:47*

Honestly, other than this non intuitive "feature" , and the wording of GUI items, CorelLaser isnt all that bad.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 08:32*

Totally agreed, **+Eric Flynn**. It does exactly what it is supposed to do, just could do with better English translation I think. I'd imagine all of the wording issues are probably due to direct translations. Once you learn how it works & what some of the more obscure things do, it's actually pretty nifty.


---
**David Spencer** *August 05, 2016 13:59*

**+Bob Damato** I used your method and it worked thanks.


---
**Eric Flynn** *August 05, 2016 14:06*

**+Yuusuf Sallahuddin**​ Yea, I just wish it had a task list view, and task manager. Other than that, it is quite capable. 


---
**Bob Damato** *August 05, 2016 14:19*

Good to hear!  **+Yuusuf Sallahuddin** , I have a question for you. I see you recommended using queues, so I tried looking that up but no go. I cant seem to find anything online about making queues, can you point me in the right direction for some online tutorial? Thank you!

bob


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 14:45*

**+Bob Damato** First, when you go to run the task you will notice a "starting" checkbox on either the cut/engrave screen. Tick that off. Beside that, there is an "All Layers" option (in a dropdown box). Make sure it is set to "All Layers". Then, when you want to add task to queue, you will just click Add Task (near the tickbox for "starting"), not Start (up the top). It will do nothing, but it will have added it to the queue. Then you do your next layer, add as task again, continue until you have all the tasks added. It will only say 1 task in the list, unless you used the "Repeat" option greater than 1. I think... Anyway, once all tasks have been added, you click Start icon (on toolbar or system tray right-click menu). Hope that makes sense.


---
**Bob Damato** *August 05, 2016 16:23*

Thank you! Ill play with this when I get home and see how it goes :)


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/Tca2UrxnVpZ) &mdash; content and formatting may not be reliable*
