---
layout: post
title: "Time for my K40D baby to grow a little :) MGN sliders on all axis (even Z) and optimized drive system to allow for minimum loss on the X and Y movements, so i can keep the K40 case and still have about 400x600 size :)"
date: January 16, 2018 09:57
category: "Modification"
author: "HP Persson"
---
Time for my K40D baby to grow a little :)

MGN sliders on all axis (even Z) and optimized drive system to allow for minimum loss on the X and Y movements, so i can keep the K40 case and still have about 400x600 size :)

Electronics goes in a external case.

![images/4fda5ef749dd5fb8e4495058c69b13a2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4fda5ef749dd5fb8e4495058c69b13a2.png)



**"HP Persson"**

---
---
**Joe Alexander** *January 16, 2018 10:43*

Awesome, remember to take plenty of photos and document it well for everyone else on here that is inevitably interested :P And if you haven't yet check out a similar build by **+Ariel Yahni** tagged #UK40OB on [openbuilds.com](http://openbuilds.com).


---
**HP Persson** *January 16, 2018 10:49*

Yeah, i was looking at openbuilds first, but with their parts i calculated a 180mm loss on X and Y, each, only for the drive motors.

So i made my own system, taking up only 60mm which gives me 120mm more X and Y movement the laser actually can reach on the same footprint :)



The upgrade project is filmed so it´s at least 90% documented :)



Very very early prototype of the belt and pulley setup.

![images/842b0be8388f85a013d26c21d584a893.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/842b0be8388f85a013d26c21d584a893.jpeg)


---
**Joe Alexander** *January 16, 2018 10:50*

[openbuilds.com - UK40OB](https://openbuilds.com/builds/uk40ob.3988/) check this one out done by another group member, similar to yours minus the linear rails :P


---
**HP Persson** *January 16, 2018 10:55*

**+Joe Alexander** Yeah, Ariel´s build is probably the best version so far based on Vslots, really low loss on the movement in his design aswell :)


---
**Ariel Yahni (UniKpty)** *January 16, 2018 12:12*

This wiil be a great achievement


---
**Carl Fisher** *January 16, 2018 14:43*

Absolutely on my to-do list so following closely. 




---
**HP Persson** *January 16, 2018 23:21*

First part of the project, nothing fun yet, just the tear down of the original gantry. But keep a eye out for next chapter before the weekend, i´ll try to remember to update here too :)




{% include youtubePlayer.html id="Z4cq4b5NfO0" %}
[https://www.youtube.com/watch?v=Z4cq4b5NfO0](https://www.youtube.com/watch?v=Z4cq4b5NfO0)


{% include youtubePlayer.html id="Z4cq4b5NfO0" %}
[youtube.com - K40D upgrade project - part 1](https://www.youtube.com/watch?v=Z4cq4b5NfO0&feature=youtu.be)


---
**Bryan Hepworth** *January 20, 2018 08:30*

very nice indeed - have you got the MGN slider specs handy?


---
**HP Persson** *January 20, 2018 09:16*

Y is MGN12 with MGN12H block, 600mm long

X is MGN15 with MGN15H blocks, 400mm long

My K40D can probably fit 700mm sliders for Y, but not sure yet what mine will be in the end, as i´m hiding a small electronic compartment to the right for the controller board.


---
**Tony Sobczak** *January 21, 2018 07:41*

Following 


---
**HP Persson** *January 24, 2018 17:56*

Getting closer, first prototype-parts done and ready to be installed.

Time for X-gantry now :)



Final bed size will be 600x390 usable space for the beam to reach, actual bed will be slightly bigger :)

![images/e20d3743464d73faa07bbb4bb2649de7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e20d3743464d73faa07bbb4bb2649de7.png)


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/1cRytJPTYz6) &mdash; content and formatting may not be reliable*
