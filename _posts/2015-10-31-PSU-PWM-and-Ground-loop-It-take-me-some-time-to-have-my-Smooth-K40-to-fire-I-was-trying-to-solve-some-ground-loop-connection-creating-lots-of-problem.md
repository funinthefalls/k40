---
layout: post
title: "PSU, PWM and Ground loop. It take me some time to have my Smooth K40 to fire, I was trying to solve some ground loop connection creating lots of problem"
date: October 31, 2015 18:24
category: "Smoothieboard Modification"
author: "Stephane Buisson"
---
PSU, PWM and Ground loop.



It take me some time to have my Smooth K40 to fire, I was trying to solve some ground loop connection creating lots of problem.



Well going back to basic, I found to connect the PWM to the PSU wasn’t where it I supposed to be. I was trying to solve a wrong problem.

I try  for far too long to connect the PWM on PSU IN, until I find out with the MYJG40W, the PWM should be on  L (WP).

IN is still of use after the pot, to inject the laser setting for the laser power. We have the pulse (V) and the pot to limit max current (A) to protect the laser tube life expectancy.



It’s plenty of different PSU in the evolution of K40’s. Best to find out your case is to look your connections with the original board. (the very old one didn’t have PWM).



Unsure, I did order a level shifter (2GBP) and it arrived yesterday, so I fit it in. so I can guaranty it work with it.

But can't say if it's compulsory, as I don't fancy the extra work to test.



Unchanged and still in function: Potentiometer, Laser Switch, Test switch



So in my case (MYJG40W - Smoothieboard 4XC) here is the schema:



   PSU                                          Smoothie

+24V                       <-<s>>                VBB+</s>

<s>Ground                   <>          VBB Ground</s>

<s>    L         <</s> via Level shifter <-      2.5!       (you notice the ! )

  5V            <s>>  level shifter   <</s>      3.3V



Then 2 motors and 2 limit switches, that’s it really.



To avoid ground loop connect pin 2.5 from JP33 and not behind the Mosfet.



**+Arthur Wolf** 





**"Stephane Buisson"**

---
---
**Arthur Wolf** *October 31, 2015 19:59*

So it works ?


---
**Stephane Buisson** *October 31, 2015 21:44*

Yes **+Arthur Wolf** , work nicely and SMOOTHLY

;-))


---
**Arthur Wolf** *October 31, 2015 21:44*

Yay :)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/MuZg49Gb3BL) &mdash; content and formatting may not be reliable*
