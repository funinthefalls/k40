---
layout: post
title: "Im sorry, but i didnt manage to insert this picture into my already generated post"
date: March 08, 2016 22:17
category: "Hardware and Laser settings"
author: "Martin Larsen"
---
Im sorry, but i didnt manage to insert this picture into my already generated post.

This is the content of the softwarecd that followed my K40 lasercutter.

Can anybody tell if this looks odd or normal?

![images/561dcec1516efc82c1ff170f1c2addb9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/561dcec1516efc82c1ff170f1c2addb9.jpeg)



**"Martin Larsen"**

---
---
**Martin Larsen** *March 08, 2016 22:18*

Perhaps Yuusuf Sallahuddin?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2016 22:45*

Yeah, that's definitely different to the contents of my CD. I just shared a link to a printscreen I took of the contents of my CD.



[https://drive.google.com/file/d/0Bzi2h1k_udXwWnYtdEF2SkxHVE0/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWnYtdEF2SkxHVE0/view?usp=sharing)


---
**Scott Thorne** *March 09, 2016 00:07*

That's the cd for the 50 watt with the dsp controller made by ruida!


---
**Mauro Manco (Exilaus)** *March 09, 2016 06:53*

Confirm  that cd for 50w.....  do you find in a usb dongle?? if no have can't use laserdraw  usb are same this [https://richardgrisafi.files.wordpress.com/2014/10/2014-10-10-02-07-32-e1412922089360.jpg?w=800&h=519](https://richardgrisafi.files.wordpress.com/2014/10/2014-10-10-02-07-32-e1412922089360.jpg?w=800&h=519)


---
**Scott Thorne** *March 09, 2016 18:43*

No I don't have a dongle...sorry...but I don't need one with the 50 watt


---
*Imported from [Google+](https://plus.google.com/105195340721594982075/posts/b5cimmRxeCv) &mdash; content and formatting may not be reliable*
