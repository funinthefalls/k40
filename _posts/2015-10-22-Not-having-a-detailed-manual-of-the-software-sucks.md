---
layout: post
title: "Not having a (detailed) manual of the software sucks"
date: October 22, 2015 06:12
category: "Hardware and Laser settings"
author: "Ashley M. Kirchner [Norym]"
---
Not having a (detailed) manual of the software sucks. There, I said it. Now that I have that off of my chest ... can anyone tell me what exactly the 'Laser head step by' is used for in LaserDRW? It's in the lower right of the 'Engraving Manager' window that comes up when you click Engrave ...

![images/ac19f367710b4fb9fb0d909a78ab9783.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ac19f367710b4fb9fb0d909a78ab9783.png)



**"Ashley M. Kirchner [Norym]"**

---
---
**Ashley M. Kirchner [Norym]** *October 22, 2015 07:25*

Oh, so nothing particularly important then. Ooookay, moving on. :) Thanks!


---
**Peter** *October 22, 2015 11:08*

If you adjust the step by the Up/Down/Left/Right buttons you should see the image on the work space move. You can adjust the start position of the laser by dragging and dropping the image too.


---
**Ashley M. Kirchner [Norym]** *October 22, 2015 15:31*

Yeah, I don't generally do that. I use exact XY values since I know what size the art is and I measure the material going in. I also 3D printed an alignment piece so that I always have the same placement of the material at 0,0 (and the laser starts its work area 1mm offset from that.)


---
**Peter** *October 22, 2015 16:03*

I had plans to make something to space between the outer frame and the 0/0 spot so I could measure exactly where material had to be placed. Got to find and order a new machine now though.


---
**Ashley M. Kirchner [Norym]** *October 22, 2015 16:28*

The advantage of also having a 3D printer is that I can easily and quickly prototype something. I made a couple of pieces yesterday which work great. Today I'm going to refine them a bit more and reprint them. It's basically a piece that rests against the rail all the way in the back and against the left one. It's a very fat 'L' shape with the bottom of the 'L' against the back. The 90 degrees angle in it sits at 0,0 so I can always place the material in it and know where it starts. Works pretty well. I managed to engrave a clear piece of acrylic on both sides and the alignment was perfect.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2015 08:59*

**+Peter Jones** I placed a piece of cardboard covering my entire bed & then cut out 300mm x 200mm at 0,0 position. Then the area cut out is the working area. Obviously since it is only thin card it is not suitable for giving something solid to push up against, but the idea is to use something thicker in future (e.g. 6mm ply) & do the same with it, even if I have to cut through it multiple times in order to get all the way through it.


---
**Ashley M. Kirchner [Norym]** *October 26, 2015 19:22*

I just used a piece of scrap acrylic. I know anything larger than 30mm would be enough to figure out the left edge. Once I got that, I 3D printed the spacer for it and now I can just align pieces up against the spacer.


---
**Peter** *October 26, 2015 22:18*

Once I have the new machine and I have a fixed point far left and back of the machine I will be cutting with scrap a marker for the outer limits of the cutting area, using that as the 0/0 point. Need the machine delivering first thought :(


---
**Ashley M. Kirchner [Norym]** *October 26, 2015 22:23*

Ironically, one of the things I discovered after making those spacers, is that the rails aren't square. Guess who's going to be ripping apart his machine after he's done engraving 100 or so pieces of acrylic this week.


---
**Peter** *October 26, 2015 22:53*

How far out is it? How did you discover it?


---
**Ashley M. Kirchner [Norym]** *October 26, 2015 23:30*

It's mainly the back rail, the one that's partially cut to fit the exhaust vent. The two pieces don't line up in a straight line. But the front one also isn't square with the side rails. This explains why when the laser head carriage moves, I can tell it isn't parallel to the front rail. Putting a square in each of the corners, it's off by 1-2mm. And I just checked one of the many pieces I've been cutting ... not square. Checked several in fact, nope. So yeah, time to tear it apart and square it up ... well, over a weekend. Not right now as I have several things to finish by this weekend. Stuff that isn't critical to be perfectly square.


---
**Peter** *October 26, 2015 23:32*

Maybe just give it a light smack first. I cant imagine tearing it apart to then put it back in the same holes will help much. Maybe twisted in shipping and needs lightly twisting back.


---
**Ashley M. Kirchner [Norym]** *October 26, 2015 23:36*

Can't give it a smack while it's inside the case, specially not the back rail which doesn't seem to be aligned anyway. :)


---
**Peter** *October 27, 2015 00:03*

The 40W one I had and returned, that stood on three legs, four of you pressed the other corner down slightly. The chassis is so flimsy that there isnt really a good place to start when straightening it. How do you make sure the chassis is straight to then line up the insides? Can you straighten the insides without playing with the frame? How much work do you think you'll do to it to make it right?


---
**Ashley M. Kirchner [Norym]** *October 27, 2015 01:07*

The rails don't need to be square with the chassis, but they do need to be square with each other, and that's what I'm concerned about. One can always drill new holes in the chassis. And I already have a nice chuck of aluminum block that I'm going to drop into the bottom (or bolt to the bottom from the outside, haven't decided yet) for support.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/UGNkHrpJ8Tt) &mdash; content and formatting may not be reliable*
