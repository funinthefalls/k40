---
layout: post
title: "My K40 engraving on slate. Works like a dream!"
date: August 13, 2015 17:27
category: "Materials and settings"
author: "David Wakely"
---
My K40 engraving on slate. Works like a dream! I've been making slate door numbers to sell on eBay. I've sold 30+ so far! The machine has paid for itself!

![images/b01f3afe57433f973afd8959e3ecb96e.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/b01f3afe57433f973afd8959e3ecb96e.gif)



**"David Wakely"**

---
---
**James McMurray** *August 14, 2015 22:57*

How much can you charge for those?


---
**David Wakely** *August 15, 2015 19:24*

**+James McMurray** I usually sell them for around £5 - £7 depending on size. People seem to love them! 


---
**Gregory J Smith** *August 16, 2015 00:43*

Brilliant! Gotta get me some slate. David, can you advise what settings (speed and power) you use? Is one pass enough? BTW - I've labelled by power knob 0-10. Cheers.


---
**David Wakely** *August 17, 2015 21:17*

Hi there **+Gregory J Smith**, the settings I'm using are approx 10ma at 400mms. I was using 200mms but i found there was barely no difference in quality when sped up to 400mms. The slate I'm using is Spanish slate and engraves nice. Any more advice let me know! Good luck


---
**I Laser** *August 18, 2015 10:08*

Looks good David.



Is the machine doing much more than just burning the surface? As it doesn't really look like its engraving as such in the video.


---
**David Wakely** *August 18, 2015 12:14*

**+I Laser** it is marking the surface. It doesn't actually make a deep engrave. I think it's similar to glass engraving. I think it fractures the surface somehow. I'll try and post a close up of the engraving result for you.


---
**I Laser** *August 19, 2015 09:42*

Ah that makes sense, would love a close up shot thanks! :)


---
**I Laser** *September 07, 2015 03:33*

I ended up getting some slate, Chinese black, though it's more of a grey colour. Whilst the machine manages to mark the surface you'd really need to clear coat the lot. The surface scratches away easily and flakes a bit too much. Think I'll be skipping this medium, thanks for the idea anyway.


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/S2LmhJW2ip8) &mdash; content and formatting may not be reliable*
