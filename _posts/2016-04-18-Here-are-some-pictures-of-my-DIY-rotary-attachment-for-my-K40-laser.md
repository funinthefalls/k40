---
layout: post
title: "Here are some pictures of my DIY rotary attachment for my K40 laser"
date: April 18, 2016 10:55
category: "Modification"
author: "Anthony Bolgar"
---
Here are some pictures of my DIY rotary attachment for my K40 laser. Total cost was about $25.00



![images/8a187406a494ec6a0ea4a6a941a69605.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a187406a494ec6a0ea4a6a941a69605.jpeg)
![images/40d6dc36f77b0bd2953bbb04cdaafe55.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40d6dc36f77b0bd2953bbb04cdaafe55.jpeg)
![images/00300cef1cb4f1f6eb535dcd03df2b93.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00300cef1cb4f1f6eb535dcd03df2b93.jpeg)
![images/a523b09dfc1bef99af91b92ed5835fda.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a523b09dfc1bef99af91b92ed5835fda.jpeg)

**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *April 18, 2016 11:21*

Thanks. It's a combination of a couple of designs that werew on thingiverse along with my own design changes. Was going to use 2 steppers, but found it works juyst fine with one.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2016 13:11*

That is really cool. I was just recently thinking about/wanting to build one myself, but hadn't got around to designing it. This looks great. I'd imagine that the 2nd axle should be movable though, to allow different sized objects in it (e.g. small objects won't house on the wheels properly & larger objects will sit too high). Overall absolutely awesome though.


---
**Anthony Bolgar** *April 18, 2016 13:59*

I am currently building another one that will be adjustable between axles as well as having an adjustable height roller for things like the neck of a bottle.


---
**Mishko Mishko** *April 18, 2016 14:36*

Looks great. Pretty much what I had in mind, the other axis moveble as Yuusuf said. my question is, why 6 wheels? An object you put on will only lean on 4, and in case of any imperfection, this will only make things worse... But I may be wrong, of course.. What's the OD of the o-rings? Did you calculate these from your Y axis?


---
**Sebastian Szafran** *April 18, 2016 14:42*

This rotary attachment is so simply great, I give +1 :)


---
**Anthony Bolgar** *April 18, 2016 14:47*

The extra rollers are for doing shorter items. I did not calculate anything, just made the wheels to what looked good, I will calibrate it once I have it installed.


---
**HalfNormal** *April 18, 2016 19:04*

Any plans to post the build?


---
**Mishko Mishko** *April 18, 2016 19:08*

The aproximate diameter of my timing sprockets with a belt on is 13mm. I guess I'd have to do some reduction if I want to just plug the motor with the same steps/rev without any further calibration, as 13mm seems too small to me. Say 2:1 and use 26mm DIA wheels (i.e. 20X3 o-rings). Unless there's an option to do the calibration within CorelLaser, but I couldn't find any option to do that...


---
**Anthony Bolgar** *April 18, 2016 22:27*

Thank you to all that have liked this project, feels good to know others like your work. I need to gather up all the files I used and make a complete set for uploading to Thingiverse, as this is a combination of two other designs and my own mods to them. It should be up by Friday, but I will let you all know when I put it up.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2016 23:07*

**+Anthony Bolgar** It is a great design/project & for around $25 is pretty good. Did you 3D print all the white plastic sections?


---
**Brien Watson** *April 18, 2016 23:09*

That's awesome!


---
**Dennis Fuente** *April 18, 2016 23:27*

nice work and great idea 


---
**David Cook** *April 19, 2016 00:54*

That's cool !




---
**Anthony Bolgar** *April 19, 2016 01:16*

**+Yuusuf Sallahuddin** No, all the white sections are laser cut, the red is 3D printed.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 19, 2016 01:50*

**+Anthony Bolgar** Sweet as. Very nice build.


---
**Rodney Huckstadt** *September 12, 2016 00:29*

Any update on this one :) 




---
**Anthony Bolgar** *May 04, 2018 22:29*

Sorry, forgot all about posting the files for this, will try and do so this weekend.




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/R6R7cjn4RtJ) &mdash; content and formatting may not be reliable*
