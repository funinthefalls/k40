---
layout: post
title: "Hi All, I am shafiq from Malaysia"
date: March 16, 2016 08:27
category: "Hardware and Laser settings"
author: "shafiqlc"
---
Hi All, I am shafiq from Malaysia.



Anyone know this item? RX20-30-30k resistor. The green component near power suply.



Anyone facing the failure of this component?. I change the component 3 times since i buy the machine.



Any advice  on how to keep this component safe?



It is rare to get the component in my area. need to import from china.



Thank you

![images/709f32a325d3cae070a74db9cc2efe9b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/709f32a325d3cae070a74db9cc2efe9b.jpeg)



**"shafiqlc"**

---
---
**Stephane Buisson** *March 16, 2016 09:37*

Just to say, this component is only on the old model of K40.

new PSU don't need this external component.


---
**Heath Young** *March 16, 2016 22:24*

That's the ballast resistor, and only on the older model of the laser power supply as another reader mentioned. The problem is that it isn't rated for an appropriate voltage - its rated for 1000V (see datasheet - [http://www.micro-ohm.com/power/pwrrx20.html](http://www.micro-ohm.com/power/pwrrx20.html)) when its getting a much higher voltage across it. Is it arcing to the case or across itself? You may be able to tightly wrap it with some Kapton tape to improve the situation.


---
*Imported from [Google+](https://plus.google.com/107114863917053711386/posts/Wx8EfkJaiS4) &mdash; content and formatting may not be reliable*
