---
layout: post
title: "Changing Mirror diameter 20mm, thickness 3mm or less with adapter"
date: November 06, 2015 14:55
category: "Hardware and Laser settings"
author: "Stephane Buisson"
---
Changing Mirror

diameter 20mm, thickness 3mm or less with adapter.



when I unscrew my mirror and look through I find out why it crack.

the coating was gone, poor product surely, but maybe also due to cleanning friction. make your own conclusion.



![images/a95855f2e60da8d76d03d52f14bf9a78.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a95855f2e60da8d76d03d52f14bf9a78.jpeg)
![images/dc70110ae3d25d2a85c967dfb4af5ecf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc70110ae3d25d2a85c967dfb4af5ecf.jpeg)
![images/ce47fb54eb86ae7ff8cb5529a1259258.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ce47fb54eb86ae7ff8cb5529a1259258.jpeg)

**"Stephane Buisson"**

---
---
**Coherent** *November 06, 2015 18:27*

I've been wondering if not cleaning and having excess soot or debre on it could cause excessive heat to build up and damage? I did see a video where a guy had a low cost beam combiner/reflector and installed  it backwards (so the um-coated side was facing the laser) which caused  immediate burn/break when the laser was fired.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/DUBPvK8gAkg) &mdash; content and formatting may not be reliable*
