---
layout: post
title: "Hi everybody. Brand new to the hobby"
date: January 15, 2017 20:25
category: "Air Assist"
author: "Austin Baker"
---
Hi everybody. Brand new to the hobby. Received a K40 from Amazon last week (it was actually a Prime item, which was surprising), and quickly noticed online that people were making several simple upgrades to improve performance, quality of life, etc. Chief among them seems to be the air-assist upgrade. I've looked at pictures, posts (such as those on this community), and videos, and I haven't seen a single one use a flexible hose & nozzle to position/point the air flow (see image). Any reason these aren't used more often? A lot of the heads/attachments seem to place the air in a fixed and un-modifiable position. Just curious... thanks!



PS -- Are there any good introductory resources to help me with other useful upgrades?









**"Austin Baker"**

---
---
**Ned Hill** *January 15, 2017 21:02*

Part of the function of the air assist is to keep material from accumulating on the bottom of the lens so the air is typically routed to exit below the lens.  Also you want something compact so it's not hitting anything at the extremes of the gantry movement. 


---
**Don Kleinschnitz Jr.** *January 16, 2017 00:32*

Been there tried the hanging hose..... what **+Ned Hill** said.


---
**Austin Baker** *January 16, 2017 00:42*

**+Ned Hill** Ah, thanks. I had not considered the air being used to keep the lens clear as well -- though it was only used to keep the target area clear.


---
**Ned Hill** *January 16, 2017 00:49*

The air assist also functions to keep flare ups down and to enhance the burn of the laser.  So ideally you want something blowing down into the cut.


---
**Don Kleinschnitz Jr.** *January 16, 2017 00:51*

**+Austin Baker** I wasted a lot of time then just got the LO assy and moved on :)


---
**Austin Baker** *January 16, 2017 03:12*

Thanks!


---
*Imported from [Google+](https://plus.google.com/100996155784545993018/posts/Hz6ARzsZs7v) &mdash; content and formatting may not be reliable*
