---
layout: post
title: "Open question to Chinese K40 Manufactures: Clearly the classic blue K40 has great, inexpensive hardware that gets the job done for many people"
date: March 30, 2016 18:07
category: "Discussion"
author: "Stuart Rubin"
---
Open question to Chinese K40 Manufactures: Clearly the classic blue K40 has great, inexpensive hardware that gets the job done for many people. But so many people change out the controller and software to RAMPS/Smoothie/DSPs/etc. for better performance. Why don't you ship the K40 with the open source hardware/software that people are upgrading to? I would think that it would be less expensive, have better community support, etc. You would get the best of both worlds - your manufacturing, but with community-supported open source firmware.





**"Stuart Rubin"**

---
---
**MNO** *March 30, 2016 18:29*

I would add also to do other things in more mechanical design. 

like fix for exhaust, removable botom, or making it little higher. Less space for electronics, more space for cutting :),  etc... So things that everybody makes or wants anyway... 


---
**Thor Johnson** *March 30, 2016 18:45*

Aw man... they're just now starting to add air assist tubing and a digital power meter ;)



Though I think a smoothie+LCD for under $100 vs a LIXUE board for the same price would make this kind of a no-brainer... as far as costs go. 


---
**Jim Hatch** *March 30, 2016 18:53*

Because they don't have to. And because it will cost more for a product with pretty thin margins. We in this group self-select for the tinkerer so our desire to upgrade may not be representative of their customer base. If they're like mine, they arrive and work out of the box with a little YouTube video backup. For Joe or JoAnn Average, that's good enough.



For tinkerers I'd suggest it is as well. We can decide post purchase which things bother us most and are willing to pay to fix. Would you have paid twice as much for one with a Smoothie board? Or for a lid interlock or water coolant flow switch or...



What they really ought to do is fix the instructions and software so they read & operate with English (or French or Australian ;-) or other non-Chinese) as a primary language vs the very rough translation. That would improve usability enormously.



BTW, the issues aren't just with K40s - our LegacyLaser ($5,000) has similar issues with software and manuals.



With all their faults, these manufacturers are very good at defining and selling a minimally viable product at what is likely the lowest cost possible.


---
**Stephane Buisson** *March 30, 2016 23:21*

looking into that matter, China factories didn't really anticipated the coming of new boards and the open sources movement. the market was divided from entry level to high specs. DSP was existing for the top end, and start to appear into K40 as a reaction. but let's be honest a 500$ DSP could be advantageously be replaced by a Smoothie for around 150$ and the software will benefit from open source development and support. Factories wasn't ready to lose control on their products. Still providing the basic K40 open for all to mod is a good thing.

From there, our community could have a role to play with the help of **+Arthur Wolf** and **+Peter van der Walt** .

Smoothiebrainz+K40daughter could be a solution under a kit form (at least at the begining, before to convince factories).

<i>"Now is it the good time?"</i> we know the concept work well (with Smothieboard), SmoothieBrainz is still prototype and the daughterboard isn't out. I could understand the community ask for more proof. As far As I know, no production is organised, and asking community members to solder components on a board is a step to far.

SmoothieBrainz isn't only for K40 (CNC,PNP,3D printer, plasma cutter,...) and could require to look at it with broader vision.

I don't know if **+OpenBuilds**  will take the risk to produce it and offer in their shop, and love to ear about it.

Maybe robotseed (EU) associated with Uberclock (US) when then move to SmoothieBoard 2.

For those who didn't follow a SmoothieBrainz is a simplified Board (for cost), just the brain without steppers, to be completed with a daughterboard for your function. (why pay for 5 steppers if you need 2 or 3), without saying it simplify the maintenance/repair.



So in a bit of time I intended to post a Poll. to know who could be interested and see from there.

SmoothieBrainz should work with Laserweb and Visicut.


---
**MNO** *March 31, 2016 01:21*

Stephanie can You please explain difference between SmoothieBoard and Ramps...

I'm new and was thinking RAMPS was really open and mega cheap... is there some advantage of using SmoothieBoard? I was thinking to use RAMPS because i already have them (ordered too much when building my 3d printer). But i'm  curious of differences...


---
**Thor Johnson** *March 31, 2016 01:26*

**+MNO** The big thing is that the Smoothie is <b>*fast*</b>.  For 3D Printers, this isn't so much of a big deal because you're waiting on the plastic and it doesn't matter if the printhead sits still in 1 spot for just a second.  OTOH, for a laser, <b>consistency in speed</b> is key -- if you are approaching a corner and you need to slow down to make the corner, then you need to adjust the power or you're going to end up with blackened corners.  The RAMPS have gotten better, but the Smoothie has always been good at that.



I just wish you could stuff LaserWeb <b>into</b> the Smoothie...


---
**MNO** *March 31, 2016 01:54*

So what i just saw in second on LaserWeb is that it works with marlin and that means with RAMPS... :) Thanks Thor. I need to learn so much :):) so less time...


---
**Arthur Wolf** *March 31, 2016 07:21*

**+MNO** Smoothie is faster, but also has insanely complete documentation, more features, a larger community, easier configuration, active development ...


---
**Stephane Buisson** *March 31, 2016 08:49*

**+MNO** Ramps/Arduino is 8 Bit, Marlin is a mature development, so mature in fact (so much things in it) it have some difficulties to run into the limited Arduino memories, forcing to make some choices. No such limitations with Smoothie (32Bit), lot's faster (>10X), opening new horizons like acceleration. (example; when you engrave, you can manage the ray power to decrease when the head reduce speed before changing to reverse direction, so you can engrave both way with good quality on images' borders). the couple Ramps/Marlin look like end of life compare Smoothie solutions, time to move on.


---
**Stuart Rubin** *March 31, 2016 14:59*

All of these points are well taken. I don't yet have my machine (on a boat in the ocean...) to study the electronics, but my suspicion is that that actual BOM cost of their Moshi controller board is comparable to that of decent Arduino-based board. Lots of the expensive stuff power supply, power FETs for the switcher, connectors, would be the same. But there would be no added cost for a Corell license, whatever the Moshi firmware costs (if any), USB dongle, etc. I assume a lot of the added expense to the controller upgrades have to do with LCDs and other peripherals, which are nice, but not critical.



I'd love to hear from the manufacturers on this!


---
**Stephane Buisson** *March 31, 2016 15:15*

**+Stuart Rubin** if we do like cheap K40, it's not our goal to make it cheaper for the chinese factory, but to make it fully functional and open source driven softwares. (in plain english).

 **+Peter van der Walt** not sure it would be interresting to loose time on reverse engineering, it's proprietary anyway.


---
**Phillip Conroy** *April 05, 2016 00:50*

i am more than happy with  the stock board that came with my laser cutter. i cut 3mm mdf for at least 3 hours a day for wife scrapbooking ,99% is cutting all the through the mdf and 1 % of the time only only using 2ma power to mark the surface. The only thing i would want my software/hardware to do is control the lasers power level,however i get around this by doing 2 passes and manualiy changeing the power level.


---
**Kornel Varga** *May 12, 2016 20:54*

Here is an example :

I built  Chinese made gas engine on bikes.  On a well known forum a guy made a good upgrade to make it less loud because it was damn loud.  So the guy sent the upgrade plains to one of the big manufacturer for free.  Do you think they ever used it?  No! 

  The answer was: ppl going to buy it without the upgrade,  we don't have to invest anything. If they buy it why should we do anything... 


---
*Imported from [Google+](https://plus.google.com/104069580748008438325/posts/Qt39vaZ691L) &mdash; content and formatting may not be reliable*
