---
layout: post
title: "Ok so I just installed my new cohesion3d card in my k40"
date: November 27, 2017 01:55
category: "Modification"
author: "David Allen Frantz"
---
Ok so I just installed my new cohesion3d card in my k40. Tried to follow along with the instructions but I don't know what version of laserweb4 to download. And I'm not sure how to setup and go from there. Any words of wisdom for ME? Thanks!





**"David Allen Frantz"**

---
---
**Ariel Yahni (UniKpty)** *November 27, 2017 02:02*

Download versión according to your OS.

You can watch my videos on YouTube for an easy start.


---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2017 03:56*

The installers are linked here: [github.com - LaserWeb4-Binaries](https://github.com/LaserWeb/LaserWeb4-Binaries/blob/master/README.md)



Can you point me towards exactly what was unclear in the instructions so that I can improve it?  


---
**David Allen Frantz** *November 30, 2017 11:12*

Okay so when I try to connect my cohesion3d controller upgraded K40 laser buy USB into Laser Web for it connects the machine for just a second and then says " no supported firmware detected. Closing Port com 1 "


---
**Ray Kholodovsky (Cohesion3D)** *November 30, 2017 13:54*

What is the computer and operating system you are using? 


---
**David Allen Frantz** *November 30, 2017 23:42*

**+Ray Kholodovsky**  it’s a Fujitsu laptop running windows 10… 


---
**Ray Kholodovsky (Cohesion3D)** *November 30, 2017 23:44*

Windows 10 does not need any drivers.  You didn't try to install the smoothie drivers did you?


---
**David Allen Frantz** *December 01, 2017 01:53*

**+Ray Kholodovsky** no


---
**David Allen Frantz** *December 01, 2017 01:58*

**+Ray Kholodovsky** I tried to download the firmware off the Cohesion3D site link and put it on the SD card in the controller thinking it was out dated but the file wouldn’t download to the SD card. 


---
**David Allen Frantz** *December 01, 2017 11:00*

I got it to talk.  Had to switch my USB serial port setting in laserweb4 to a COM4 style 


---
**David Allen Frantz** *December 01, 2017 13:03*

Now if I bring a png file into laserweb it won’t show the image in the grid. Nor will it show the grid. And if I try to tweet the grid laserweb freezes. 


---
**David Allen Frantz** *December 02, 2017 14:47*

OK so I have tried numerous times to uninstall and reinstall laser web in order to solve the blank screen upon opening of laser web but nothing is working any suggestions?


---
**Ray Kholodovsky (Cohesion3D)** *December 02, 2017 15:06*

Can **+Ariel Yahni** check if this is the known issue and provide the link for the fix ? Is this the white screen issues or something else? 


---
**Ariel Yahni (UniKpty)** *December 02, 2017 15:15*

**+David Allen Frantz** read this post all the way to the end and you'll find location of a folder you need to delete after unistall depending on your OS. After that reinstall again

[plus.google.com - Hello, I cannot anymore make the software start.. I get a blank screen when ...](https://plus.google.com/100075568232518942576/posts/WQHxyQCF2Sf)


---
**David Allen Frantz** *December 03, 2017 15:38*

Right on work perfectly thank you so much!

![images/2b5d42f1e0ca531dbb13e8cce13f8889.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b5d42f1e0ca531dbb13e8cce13f8889.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2017 00:35*

Great. Thanks Ariel. 


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/Pmvqiq8poGe) &mdash; content and formatting may not be reliable*
