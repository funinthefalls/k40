---
layout: post
title: "I hadn't used my mostly stock K40 (I only modified the bed) in about a month or so, and when I turned it on yesterday, my (always on) Windows 7 PC didn't even recognize it"
date: July 23, 2018 17:31
category: "Original software and hardware issues"
author: "James Rivera"
---
I hadn't used my mostly stock K40 (I only modified the bed) in about a month or so, and when I turned it on yesterday, my (always on) Windows 7 PC didn't even recognize it. I has been sitting with the same cable connected to the always on PC the whole time. I tried plugging the USB cable into a known good port (my OpenBuilds Ox CNC was on it) and it still didn't recognize it. When I tried to reinstall the Zadag drivers and when I selected List All Devices the CH341 didn't pop up.  I have yet to try a different cable, but I don't see how the cable would suddenly fail (I'm still going to try it).  I leave all switches off including the e-stop when it is not in use, so if the cable is not the cause then I'm going to be really stumped. :-(





**"James Rivera"**

---
---
**HalfNormal** *July 23, 2018 19:01*

Check power to the board. You might have lost your power supply or just at that connection.


---
**James Rivera** *July 23, 2018 19:52*

The whole system <b>seems</b> to power up (I see the control panel stuff all come on). I'd need to open the electronics cover to check--does the stock board have indicator lights?


---
**HalfNormal** *July 23, 2018 21:30*

The controller board does not have any indicator lights


---
**Dag Elias Sørdal** *July 24, 2018 05:08*

**+James Rivera** I got the same problem. Connected it to a new computer and it worked then...


---
**Dave Lawrence** *July 24, 2018 13:50*

I find that my system will change my controller to ramps unexpectedly. Even with ch340 drivers still installed. I need to go into divide settings and disable ramps and change it to the ch340 under com ports. 


---
**James Rivera** *July 25, 2018 06:41*

I tried to troubleshoot it today and it just...worked. WTF? Ok, nevermind. 


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/GCW59nJ3PV2) &mdash; content and formatting may not be reliable*
