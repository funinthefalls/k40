---
layout: post
title: "Plans for building a DIY strip heater for forming acrylic sheets"
date: June 26, 2017 22:48
category: "External links&#x3a; Blog, forum, etc"
author: "Madyn3D CNC, LLC"
---
Plans for building a DIY strip heater for forming acrylic sheets. Thought this was pretty cool so figured I'll share with the community! 



[http://www.lasersketch.com/strip%20heater.htm](http://www.lasersketch.com/strip%20heater.htm)





**"Madyn3D CNC, LLC"**

---
---
**Stephane Buisson** *June 27, 2017 07:57*

make me think about 2 related very old posts :



Nichrome Wire :

[https://plus.google.com/117750252531506832328/posts/XrRFJm8XJuR](https://plus.google.com/117750252531506832328/posts/XrRFJm8XJuR)



oven forming :

[plus.google.com - Some inspiration to wake up your Maker spirit !!! Oven to shape acrylic, hav...](https://plus.google.com/117750252531506832328/posts/TJmofUXK5or)



to a great maker **+Ned Hill**


---
**Ned Hill** *June 27, 2017 12:13*

**+Stephane Buisson** Cool, I've been considering making a small bender for some things I want to make.  I like the nichrome wire setup.


---
**Don Kleinschnitz Jr.** *June 28, 2017 14:04*

My acrylic bender has been one of my most popular tools. I have found that I can bend almost any part I want. It works easily with up to .22" material. It uses a nicrome wire and a modified (added power control) HF battery charger that I also use for DIY woodburner tool I made. 



Note: The vacuum port in the back of this picture has nothing to do with the bender. I got the basic design from YouTube and added the protractor to the left for precise angles.



Examples of bending boxes and brackets is here:  [http://donscncthings.blogspot.com/2017/06/dox-power-controller-module.html](http://donscncthings.blogspot.com/2017/06/dox-power-controller-module.html)

[http://donscncthings.blogspot.com/2017/05/wiring-tinyg.html](http://donscncthings.blogspot.com/2017/05/wiring-tinyg.html)



![images/55688463f92ec895006381c624cbc5dd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55688463f92ec895006381c624cbc5dd.jpeg)


---
**Don Kleinschnitz Jr.** *June 28, 2017 14:05*

The power supply for the acrylic bender. I get adjustable 0-10 amps at 12V.



![images/4f57a524cd24d642b55b45d6493e4984.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f57a524cd24d642b55b45d6493e4984.jpeg)


---
**Madyn3D CNC, LLC** *June 28, 2017 17:04*

That's really cool Don, so it's just one strand of nichrome wire providing the heat for the bend? Does the wire actually touch the acrylic?


---
**Don Kleinschnitz Jr.** *June 28, 2017 20:40*

**+Madyn3D CNC, LLC** Yes 

... one strand of 20ga nichrome wire. Easy to get as its used for vapes.

.... one 3/4 aluminum extrusion the width of your table

.... reflective duct tape inserted into the extrusion for reflective surface

... 2 x 1/4-20" adjustment bolts on either end of the table with a slot cut in the head to guide the wire and set the elevation of the wire above the extrusion

... 2 x threaded inserts to hold the adjustment bolts in the wood base

... 1 x 1/4-20 bolt to hold a spring tension on the wire while it heats. Note: do not run the current through the spring as it will get angry, heat up and loose its temper :).

... various scrap woods to make the table.



The wire does not touch the acrylic but adjusted as close as possible.



You need to match the wire diameter with your power source for max heat:

[http://www.jacobs-online.biz/nichrome/NichromeCalc.html](http://www.jacobs-online.biz/nichrome/NichromeCalc.html)



[https://goo.gl/photos/ZbGBUYJCzSMiym6eA](https://goo.gl/photos/ZbGBUYJCzSMiym6eA)



[https://goo.gl/photos/Dx2t23bMsQnDiUV96](https://goo.gl/photos/Dx2t23bMsQnDiUV96)







![images/4935c15d351d7f1efb8fba0836b4572d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4935c15d351d7f1efb8fba0836b4572d.jpeg)


---
**Madyn3D CNC, LLC** *July 09, 2017 14:31*

Thanks Don! I have all of that stuff on hand, multiple spools of nichrome from 30AWG-20AWG. I'm going to give it a shot, bending acrylic would be a huge benefit to the workshop. 


---
**Don Kleinschnitz Jr.** *July 09, 2017 14:42*

**+Madyn3D CNC, LLC** its one of my favorite tool and I made most of my OX wiring brackets using it. Some very complex. Enjoy!


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/BqPL6Y6BB8u) &mdash; content and formatting may not be reliable*
