---
layout: post
title: "Hi Everyone, Have had my K40 for around a year (without the digital display) and it has been working perfectly fine, the wife uses it to cut 3mm plywood cake toppers, all of a sudden we can't increase the current past 5, it"
date: December 09, 2017 03:39
category: "Original software and hardware issues"
author: "Hodgkins Family"
---
Hi Everyone,

Have had my K40 for around a year (without the digital display) and it has been working perfectly fine, the wife uses it to cut 3mm plywood cake toppers, all of a sudden we can't increase the current past 5, it will go down but will not increase, I have googled the problem, but can't find anyone else with the same issue?

Everything is alligned, just not sure what is going wrong?



Also we just use the stock software, but when we go to cut, we can't increase the power as it is greyed out and at 75%.



Any help greatly appreciated.





**"Hodgkins Family"**

---
---
**Don Kleinschnitz Jr.** *December 09, 2017 13:49*

Typical problems:

...Bad tube (this one is hard to prove other than the process of ...eliminating the below items).



...Bad power supply but usually presented with arching, blow fuse, crackling noise



<s>...A defect in the optical path like:</s>

<s>.......Mirror out of alignment</s>

<s>.......Burnt mirror surface</s>

<s>.......Interference within the objective lens housing</s>

<s>.......Build up of debris on lens</s>



<s>...Improper cooling, the laser heats up and power drops or the laser is damaged.</s>



The above are some typical ones, eliminate those and if the problem persists we can look at unusual possibilities. 


---
**Lars Andersson** *December 09, 2017 22:38*

Can a defect in the optical path make it impossible to increase current over 5 mA?


---
**Don Kleinschnitz Jr.** *December 09, 2017 23:12*

Doh!,  forget the optical path & cooling above, didn't have coffee.....


---
**Hodgkins Family** *December 11, 2017 06:56*

I am thinking that it must either be the power supply or the laser tube.

I tried a new Potentiometer, but no luck.

The wife said, she was cutting at 15mA and all of a sudden it dropped down to 5mA.

The colour of the laser is blue in the tube. I am thinking that it might point to the power supply?


---
**Don Kleinschnitz Jr.** *December 11, 2017 13:21*

**+Brenden Hodgkins** unfortunately its really hard to tell which of these two parts is bad. 

How many hrs on the tube?

I would bet on the supply as well..... likely the HVT.

You can  verify that the "IN" pin on the LPS has 5VDC on it when the pot is all the way up as a check.


---
**Hodgkins Family** *December 11, 2017 22:44*

The tube would have around 50 hours, only been used in 5 - 10 minutes at a time.


---
*Imported from [Google+](https://plus.google.com/117086809695161378057/posts/gGFXfmAjDnn) &mdash; content and formatting may not be reliable*
