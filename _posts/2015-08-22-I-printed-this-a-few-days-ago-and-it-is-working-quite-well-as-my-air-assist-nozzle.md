---
layout: post
title: "I printed this a few days ago and it is working quite well as my air assist nozzle"
date: August 22, 2015 02:27
category: "Repository and designs"
author: "James McMurray"
---
I printed this a few days ago and it is working quite well as my air assist nozzle.  You can download a printable file here.  [http://www.thingiverse.com/thing:976774](http://www.thingiverse.com/thing:976774)





**"James McMurray"**

---
---
**Stephane Buisson** *August 22, 2015 09:04*

3D printed air assist could take fire, you should be aware of that, install it only when needed, I wrap mine in foil to reduce the risk.


---
**James McMurray** *August 23, 2015 16:09*

Is the fire danger from the laser beam hitting the side of the nozzle or from heat reflecting off whatever you are cutting/engraving?


---
**Stephane Buisson** *August 24, 2015 08:52*

What I mean is this extension is in plastic, at short distance of the materiel you want to cut/engrave, if you don't put your compressor on for a job, then you have a risk to have a flame on the material reaching the nozzle, and transmitting the fire to it.


---
**James McMurray** *August 29, 2015 01:11*

With my K40 I removed the internal ventilation chute, with this nozzle attached it will run into the overhanging chute at the back of the build area when it homes.

I think you can change the "Home"location, since the way the build platform is setup you can't easily use that spot anyway.  I don't know how to do that yet.

I am going to design a version 2 to account for this issue.  I will post it when I do.


---
**James McMurray** *September 13, 2015 23:33*

I am using an aquarium pump, it is noisy, but I located it in another room.  I am not measuring pressure.  It actually works quite well.  

Here is the URL  [http://www.amazon.com/EcoPlus-728450-Single-Outlet-Commercial/dp/B002JLJC0W/ref=sr_1_6?s=pet-supplies&ie=UTF8&qid=1442187188&sr=1-6&keywords=aquarium+air+pump](http://www.amazon.com/EcoPlus-728450-Single-Outlet-Commercial/dp/B002JLJC0W/ref=sr_1_6?s=pet-supplies&ie=UTF8&qid=1442187188&sr=1-6&keywords=aquarium+air+pump)





Cheers,

James


---
**Lee Maddison** *September 30, 2015 11:52*

i have just had a friend make me this and recieved today. looking forward to finally putting air assist on my laser. many thanks for the link etc.


---
**James McMurray** *September 30, 2015 18:06*

Lee,   I posted an updated air assist for the K40 if you still have the original exhaust chute in the machine.  This version of the air assist will run into the chute when homing when the laser is first turned on.  The updated one just needs the tip filed or sanded  slightly to miss the exhaust chute.  

Good luck with your laser.


---
**Lee Maddison** *September 30, 2015 19:16*

Hi mate. Just fitted and seen it hits the chute. Have u got pics of your air assist set up. So i can see if doing it right. Many thanks for reply. Have u got.the link to the new one. 


---
**James McMurray** *October 01, 2015 01:16*

Lee,  Sorry about that.  I knew I was removing my exhaust chute so when I did the original Air Assist nozzle I didn't worry about the interference, but realized after the fact that I should have taken it into consideration.  I blame having a printer next to me so I don't need to think through all the issues as carefully as I probably should when posting parts.  Here is the link to the new one.

[http://www.thingiverse.com/thing:1013719](http://www.thingiverse.com/thing:1013719)





this one just needs the tip filed or sanded to the final correct length for your machine.  They all seem to have slightly different geometry in manufacture.



Cheers,

James


---
**Lee Maddison** *October 01, 2015 07:51*

no problem, i'll try the new one. many thanks again.


---
*Imported from [Google+](https://plus.google.com/115183135050042620815/posts/Cb8EvbBTDq8) &mdash; content and formatting may not be reliable*
