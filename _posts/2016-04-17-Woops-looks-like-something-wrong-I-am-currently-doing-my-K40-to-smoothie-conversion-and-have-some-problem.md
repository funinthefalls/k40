---
layout: post
title: "Woops, looks like something wrong :( I am currently doing my K40 to smoothie conversion and have some problem..."
date: April 17, 2016 18:12
category: "Smoothieboard Modification"
author: "Jean-Baptiste Passant"
---
Woops, looks like something wrong :(



I am currently doing my K40 to smoothie conversion and have some problem...



I am using the K40 Middleman V2 for easier wiring.



Motors are running fine (have no datasheet though :/).



But endstop are not triggering, and LCD is not working.



M119 return X0 Y1 Z1 when none are physicaly trigered.



LCD have no backlight or anything, I am using the glcd adapter and a 1A regulator so it should be ok but it's actually not. I double checked the cables and everything looks fine but it's not lighting up.



I have tried cabling everything one by one, and endstop are my actual problem.



I have connected the EStY and EST X to the smoothieboard, 5Vin with the 5V from endstop and Gnd to Gnd, but so far it's not working like it should.



Anything I did wrong here ?



Thank you for your help !





**"Jean-Baptiste Passant"**

---
---
**Stephane Buisson** *April 17, 2016 18:25*

keep up the the good mood.

I am far away of my K40, but ask on Smoothie IRC channel, always somebody their to help, be patient some time (US timezone is more active).

endstop check with multimeter then your config file. (from memory can't remember)


---
**Jean-Baptiste Passant** *April 17, 2016 18:57*

I'm always in a good mood :P



I will definitely hit Smoothie IRC and check the endstop. I was planing on replacing the stock endstop with mehanical endstop, but until I get a way to mount them I need to use the optical endstop...



I'll check tomorrow, I've passed the day doing upgrades.



Thank uou **+Stephane Buisson**


---
**Jean-Baptiste Passant** *April 26, 2016 07:29*

Update : Everything is ok, managed to make everything work this weekend.

I am now looking for an easy way to make diagram to explain how I did it.


---
**Stephane Buisson** *April 26, 2016 09:57*

**+Jean-Baptiste Passant** :-))

tu peux editer mon lien (smoothK40 top right) sur le site smoothie, et ajouter ton experience

je vais inviter plusieurs personne a le faire, on devrait comme cela couvrir toutes les differentes versions de K40 ( mine is 2014).

as tu essaye Visicut ?


---
**Jean-Baptiste Passant** *April 26, 2016 11:44*

Non, toujours pas essayé Visicut, de ce que j'ai cru comprendre c'est uniquement par Ethernet ?



Pour l'instant j'ai essayé en USB et je suis en train d'installer un Raspberry Pi avec LaserWeb pour faciliter la gestion.



J'essayerais le 7-8 mai, c'est promis :P


---
**Stephane Buisson** *April 26, 2016 13:04*

non, plus maintenant, Thomas Oster l'a mentioner quelques part usb is fine, je sais plus trop ou.

c'est vrai que je le fait en ethernet c'est plus pratique d'avoir le Mac plus loin de K40.



sinon il y aussi fusion 360 en usb.


---
**Jean-Baptiste Passant** *April 26, 2016 13:39*

Cool, je testerais cela.



Fusion 360 me parait bien complet, peut-être trop pour mon utilité ?



Je vais me faire une idée de l'ensemble des logiciels, un peu comme avec les imprimantes 3D (cura, Slic3r, Simplify3d etc...).


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/XxVTCHSY7Ck) &mdash; content and formatting may not be reliable*
