---
layout: post
title: "K40 + Lasersaur. Engraving attempt. 3 colors, 3 intensities, 3 times lasing"
date: November 11, 2016 06:00
category: "Discussion"
author: "Cesar Tolentino"
---
K40 + Lasersaur.  Engraving attempt. 3 colors, 3 intensities, 3 times lasing.  And then cut. Using inkscape to gcode plugin, loaded to lasaurapp. 



Just sharing...﻿





**"Cesar Tolentino"**

---
---
**Michael Audette** *November 11, 2016 13:01*

I like your air-assist.  Do you have a DXF/SVG and some quick directions for that?  I'm thinking I may go this way instead of an air compressor.


---
**Cesar Tolentino** *November 11, 2016 15:45*

**+Michael Audette** no problem. I'm at work right now. But later I will upload the file.  


---
**Cesar Tolentino** *November 12, 2016 05:02*

**+Michael Audette** Here are the files as promised.



[https://drive.google.com/open?id=0B1xyPwLXXQk3SDdPamd5V002NEk](https://drive.google.com/open?id=0B1xyPwLXXQk3SDdPamd5V002NEk)





[drive.google.com - K40 Laser Head - Google Drive](https://drive.google.com/open?id=0B1xyPwLXXQk3SDdPamd5V002NEk)


---
**Michael Audette** *November 12, 2016 12:32*

Thanks!  I'm going to give it a shot.  Does it do a pretty good job keeping flare ups at bay and the smoke out of the laser path?


---
**Cesar Tolentino** *November 12, 2016 14:05*

in most materials that does not go on fire yes, it does a pretty good job. It can really make the smoke away from the lens. I haven't tried poly carbonate again with this setup. That material is very flammable.



check this video. Will you consider that a flare up? without fan? it will make little fire and make burn marks fast on the wood.




{% include youtubePlayer.html id="pXPAsT0reaM" %}
[https://www.youtube.com/watch?v=pXPAsT0reaM](https://www.youtube.com/watch?v=pXPAsT0reaM)


{% include youtubePlayer.html id="pXPAsT0reaM" %}
[youtube.com - K40 CO2 Laser Update.](https://www.youtube.com/watch?v=pXPAsT0reaM)


---
*Imported from [Google+](https://plus.google.com/+CesarTolentino/posts/gwSrXQc7hAp) &mdash; content and formatting may not be reliable*
