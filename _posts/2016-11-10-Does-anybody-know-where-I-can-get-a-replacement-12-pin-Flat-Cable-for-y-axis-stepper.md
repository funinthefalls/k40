---
layout: post
title: "Does anybody know where I can get a replacement 12 pin Flat Cable for y axis stepper?"
date: November 10, 2016 03:41
category: "Discussion"
author: "Niels Sorensen"
---
Does anybody know where I can get a replacement 12 pin Flat Cable for y axis stepper?   The cable is labeled AWM 20706 105C 60V VW-1 and I believe it's 36 inches







**"Niels Sorensen"**

---
---
**Alex Krause** *November 10, 2016 03:47*

**+Ray Kholodovsky**​


---
**Anthony Bolgar** *November 10, 2016 04:43*

Digikey should sell them.


---
**Paul de Groot** *November 10, 2016 05:49*

Or RS online or Element14 


---
**Don Kleinschnitz Jr.** *November 10, 2016 12:02*

Let us know if you find one. I tried and didn't. 


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 02:14*

**+N Sorensen** I know I talked to you about getting an upgrade to replace the stock M2nano board...

The Cohesion3D Mini Laser Upgrade Bundle you'd need is now available for pre-order at [cohesion3d.com](http://cohesion3d.com)  

Since it doesn't seem feasible to replace the ribbon cable, it will be best to remove it entirely and run the 7 or 8 wires needed for the motor and 2 endstops directly.


---
**Anthony Bolgar** *November 24, 2016 02:30*

I have one that I took out of a Redsail LE400. Let me know if you want it, just pay shipping **+N Sorensen** 


---
*Imported from [Google+](https://plus.google.com/113412478181800917088/posts/g3wwGx1EvS9) &mdash; content and formatting may not be reliable*
