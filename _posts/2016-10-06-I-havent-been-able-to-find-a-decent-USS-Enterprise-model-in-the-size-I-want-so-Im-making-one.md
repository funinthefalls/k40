---
layout: post
title: "I haven't been able to find a decent USS Enterprise model in the size I want so I'm making one"
date: October 06, 2016 15:57
category: "Object produced with laser"
author: "Jeff Johnson"
---
I haven't been able to find a decent USS Enterprise model in the size I want so I'm making one. I've just about got the saucer complete. I'm redesigning the internal support structure now to make it easier to assemble and more sturdy. This is really fun! 

![images/b055598afaa0b23c524e19cfb34fcf01.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b055598afaa0b23c524e19cfb34fcf01.jpeg)



**"Jeff Johnson"**

---
---
**Jim Hatch** *October 06, 2016 16:15*

Way to go! Lots of people are consumers of other people's design work. Love to see what people come up with working from the very start of the process vs the end (cutting/engraving).




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 06, 2016 17:14*

Nice work. What thickness ply are you working with? You might find smoother results with a thinner material & more layers. Albeit, it may be a bit more complicated for the assembly.


---
**Jeff Johnson** *October 06, 2016 17:18*

This is 3mm and it's what I use for most things. I'm going for the best mix of detail and ease of assembly as possible. I would like to get some 1mm for my N scale train sets. buildings and bridges are pretty expensive.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 06, 2016 17:22*

**+Jeff Johnson** It would be nice to be able to make your own model buildings/bridges/scenery easily for your sets. I've seen some pretty cools designs posted in this group a fair while ago where someone was laser cutting model buildings/etc for miniature wargames (e.g. Warhammer).


---
**Jeff Johnson** *October 06, 2016 17:26*

For now I've been printing buildings on card stock and assembling them. I have a nice scale model cad app that helps build and texture your models. I suppose I could cut things out of the same card stock. Now I have another project to try . . .


---
**Alex Krause** *October 06, 2016 18:04*

123dmake


---
**Jeff Johnson** *October 06, 2016 18:31*

I've never used that. I've got Corel X7 for making brochures and flyers for my business and was happy to see I can use it with the laser cutter. The software for the model train buildings is [http://www.modeltrainsoftware.com/](http://www.modeltrainsoftware.com/)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 06, 2016 18:45*

**+Jeff Johnson** I can also attest to the coolness of 123D Make. Free software that can slice 3D models for you into parts for laser cutting. Pretty nifty software.


---
**greg greene** *October 06, 2016 21:03*

Make it so number one!


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/RUFejvoxNQb) &mdash; content and formatting may not be reliable*
