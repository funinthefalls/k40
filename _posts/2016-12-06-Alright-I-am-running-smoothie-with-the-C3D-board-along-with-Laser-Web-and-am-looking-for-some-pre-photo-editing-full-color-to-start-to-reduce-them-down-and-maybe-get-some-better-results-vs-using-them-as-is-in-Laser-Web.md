---
layout: post
title: "Alright I am running smoothie with the C3D board, along with Laser Web and am looking for some pre photo editing (full color to start) to reduce them down and maybe get some better results vs using them as is in Laser Web"
date: December 06, 2016 06:16
category: "Discussion"
author: "Kelly S"
---
Alright I am running smoothie with the C3D board, along with Laser Web and am looking for some pre photo editing (full color to start) to reduce them down and maybe get some better results vs using them as is in Laser Web.  I can play with the settings all day but I feel I am missing some pre-lasering steps for prepping the photo.  Thank you in advance, will check in the morning.  





**"Kelly S"**

---
---
**Scott Thorne** *December 06, 2016 14:13*

Email me the photo and i will edit it...scottthorne23@yahoo.com 


---
**Kelly S** *December 06, 2016 14:51*

**+Scott Thorne** Oh Thank you for the offer, but I'd like to be able to edit them myself too.  :D


---
**Scott Thorne** *December 06, 2016 15:40*

Lol...ok


---
**Scott Thorne** *December 06, 2016 16:04*

**+Peter van der Walt**...I'll do a video and post it when i get off...everything will be done in photoshop.


---
**Kelly S** *December 09, 2016 19:27*

Bump, still curious.  :)


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/ZC5T2NzVYeo) &mdash; content and formatting may not be reliable*
