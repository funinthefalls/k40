---
layout: post
title: "Anyone need a middleman like board? I have two extras from my OSHPark order that I would throw in an envelope and send to someone in the US for free"
date: February 23, 2016 15:54
category: "Smoothieboard Modification"
author: "Mark Finn"
---
Anyone need a middleman like board?  I have two extras from my OSHPark order that I would throw in an envelope and send to someone in the US for free.  The design is from here: [http://portfolioabout.me/k40-smoothielaser-breakout-board/](http://portfolioabout.me/k40-smoothielaser-breakout-board/) and it needs about $4 (shipped) worth of connectors from digikey.



The digikey parts are A100331-ND, 277-1860-ND, and 277-1751-ND	 (cut that one in half).





**"Mark Finn"**

---
---
**Stephane Buisson** *February 23, 2016 16:26*

**+Carl Duncan** Fusion 360, Visicut and Laserweb are smoothie candidate softwares ;-))


---
**Ben Walker** *February 23, 2016 17:30*

if these are still available I think it may be what I need for an upcoming upgrade (my machine with ribbon cables).


---
**Mark Finn** *February 23, 2016 18:10*

**+Carl Duncan** I don't have my upgrade done yet, and I'm actually going with RAMPS for now just because I have one lying around.  


---
**Mark Finn** *February 23, 2016 18:11*

**+Stephane Buisson** Are those the options for RAMPS as well?


---
**Mark Finn** *February 23, 2016 18:14*

**+Ben Walker** Privately send me an address and I'll toss one in the mail.


---
**Arthur Wolf** *February 23, 2016 21:17*

That's a pretty cool project. Would have been cool if it used the original Smoothieboard though.


---
**Arestotle Thapa** *February 24, 2016 03:11*

**+Mark Finn** if you still have one left, please do send me one. I'm in process of upgrade to smoothie compatible board but right now stuck with the RRD GLCD!


---
**Stephane Buisson** *February 24, 2016 10:44*

**+Arestotle Thapa** take care cheap RRD GLCD the connector is inverted, I did loose time with that too.


---
**Arestotle Thapa** *February 24, 2016 12:08*

**+Stephane Buisson** mine was completely blank with not even backlit. I recently found some one in this forum - [http://community.robo3d.com/index.php?threads/sainsmart-tft-lcd-screen-kit-for-arduino-due-uno-r3-mega2560-r3-raspberry-pi.6213/](http://community.robo3d.com/index.php?threads/sainsmart-tft-lcd-screen-kit-for-arduino-due-uno-r3-mega2560-r3-raspberry-pi.6213/)  and try rotating the connectors 180 degree. Once I rotated the connectors, I can see these menu options "watch, play, jog, prepare, custom, configuration, probe" but I'm not sure if that is what smoothie is supposed to shows or not. The jog is also not working properly. Were you able to get yours to work properly?


---
**Stephane Buisson** *February 24, 2016 12:20*

**+Arestotle Thapa** 1st sorry, where I am at the moment is very poor internet connexion. Ok it's a good chance your GLCD work, but you need a better 5v, if power is too low the display work without contrast (so you can't see it) usb do not deliver enought power. need external PSU /voltage regulator.


---
**Arestotle Thapa** *February 24, 2016 12:25*

Thank you **+Stephane Buisson**  will try with external 5v.


---
**Mark Finn** *February 27, 2016 20:54*

**+Arestotle Thapa** I tried to send you a message on hangouts a few days ago but never got a reply.  I need your address If I'm going to send you the board.


---
**Arestotle Thapa** *February 27, 2016 21:57*

**+Mark Finn** I don't use G+ a whole lot so I've not figured out how to send private message yet! :)


---
**Arestotle Thapa** *March 09, 2016 11:55*

**+Mark Finn** received the board. Thanks a lot for sharing! BTW I had never heard of OSHPark before. That is also going to be handy!


---
*Imported from [Google+](https://plus.google.com/102167782284542666252/posts/7KQZfakcp9T) &mdash; content and formatting may not be reliable*
