---
layout: post
title: "Wanna Make a Jigsaw Puzzle?"
date: November 06, 2018 17:52
category: "Repository and designs"
author: "HalfNormal"
---
Wanna Make a Jigsaw Puzzle?



[https://cdn.rawgit.com/Draradech/35d36347312ca6d0887aa7d55f366e30/raw/b04cf9cd63a59571910cb226226ce2b3ed46af46/jigsaw.html](https://cdn.rawgit.com/Draradech/35d36347312ca6d0887aa7d55f366e30/raw/b04cf9cd63a59571910cb226226ce2b3ed46af46/jigsaw.html)





**"HalfNormal"**

---
---
**Stephane Buisson** *November 07, 2018 07:43*

my grand children will love it, thank you so much !


---
**Don Kleinschnitz Jr.** *November 07, 2018 14:01*

#K40Projects


---
**Duncan Caine** *December 06, 2018 17:28*

Here's a link to an Inkscape extension which creates jigsaws from within Inkscape [inkscape.org - Draw Freely &#x7c; Inkscape](https://inkscape.org/~Neon22/%E2%98%85lasercut-jigsaw)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/QBin7L8KvJk) &mdash; content and formatting may not be reliable*
