---
layout: post
title: "Alignment changes I've been trying to correct the 1st mirror alignment (the beam slopes downward towards the front, quickly going outside the hole in the head)"
date: April 19, 2017 23:13
category: "Modification"
author: "Adrian Godwin"
---
Alignment changes



I've been trying to correct the 1st mirror alignment (the beam slopes downward towards the front, quickly going outside the hole in the head). It's easy enough to correct it but as soon as I put the machine together again it's wrong.



I suspect what's happening is that I lean on the tube cover to refit the fan, and it moves something. It doesn't spring back but stays wrong.



Any ideas what might be bending ? I realise that there are many horrible brackets that need replacing (I hate putting it back together without replacing multiple awful features) but I'd like to get it basically functional before spending too much time on improvements. Or I'll get distracted and never get it running - and I could do with it working so I can cut some acrylic for those improvements!





Picture : temporary bed is a cake cooling rack. Fits very well ! It's not the cause of my problem though, as the beam is misaligned at the second mirror.







**"Adrian Godwin"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 20, 2017 00:06*

The beam sloping downwards towards the front is something I originally struggled with. I found that the 1st mirror has 2 screws underneath its mount at the back (holding the mirror section) that were protruding enough to cause them to touch the U shaped bracket below it. I ended up shimming the front (where you screw to the U shape bracket) up with some washers beneath it to prevent the 2 back screws causing the tilt in the mount.


---
**Kelly Burns** *April 20, 2017 01:00*

Not sure what you mean "when you put the machine back together"   You shouldn't have to take anything apart to adjust the mirrors.  How are you adjusting the mirrors?  


---
**Adrian Godwin** *April 20, 2017 09:36*

Thanks Yuusuf, that could be it - I'll look into it.



Kelly, it's not apart much - just closing the tube cover and refitting the extractor fan. I've tried doing it in stages and pretty sure it's when I lean over to pack the fan into the loose slots, I put some weight on the tube cover. I can avoid this of course, but any small loading seems to mess it up so it soon occurs. 


---
**Kelly Burns** *April 20, 2017 11:02*

There should definitely not be anything that loose or close enough to the cover for the alignment to be affected by those actions.  Sounds to me like the tube may not be fastened down correctly or that first mirror is loose.  I would look at that closely. 



Btw... clever use of cake cooling rack.  


---
**Joe Alexander** *April 20, 2017 12:32*

if you look on amazon you can get a plastic vent flange for like $5. Mount it in the back over the gap and you can put the fan wherever you want and not have it blocking the door every time.(or do as most do and replace it with something in-line) Makes accessing the tube a breeze without having to remove the ventilation.


---
**Adrian Godwin** *April 21, 2017 14:13*

Yes, that's definitely a modification to come soon. I've already cut away part of the fan housing to allow it to sit lower in the frame. This avoids the fan sticking out above the cutter (and leaking 'suck') but isn't enough to allow the tube cover to open freely.

![images/e21290e36c713c559f09e7bb5d7ff316.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e21290e36c713c559f09e7bb5d7ff316.jpeg)


---
**Adrian Godwin** *April 21, 2017 14:13*

Fan lower in bracket

![images/2dfbb826db01e67f0799c01a98e45dcf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2dfbb826db01e67f0799c01a98e45dcf.jpeg)


---
**Adrian Godwin** *August 06, 2017 14:01*

It's been a while as I've been too busy to look at it again but this is a followup to my original query about alignment.



Yuusuf was right - the tube cover was hitting the first mirror alignment screws as it closed. It can be clearly seen by watching the mirror from inside the case, and opening/closing the lid. It appears that as the lip passes the ailgnment screw it was pushing it.



It's possible the mirror can be moved - perhaps by moving the tube, too - but I made a small cutout in the lip and it's no longer a problem.





![images/1ace47f1dd299520b0d22c498bc1f61e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ace47f1dd299520b0d22c498bc1f61e.jpeg)


---
**Adrian Godwin** *August 06, 2017 14:03*

although the adjustment screw is just inside the case, the cover is slightly too shallow and the cover lip comes inside the blue painted metal.

![images/4181fe1b33969964268feda9ddff443d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4181fe1b33969964268feda9ddff443d.jpeg)


---
*Imported from [Google+](https://plus.google.com/100361775132886183172/posts/DdEWWM9zutS) &mdash; content and formatting may not be reliable*
