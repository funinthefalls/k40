---
layout: post
title: "The phenomena of the laser cut. Most of us have noticed when we cut thick wood or plastic the taper we get"
date: December 13, 2017 21:07
category: "Discussion"
author: "Steve Clark"
---
The phenomena of the laser cut.



Most of us have noticed when we cut thick wood or plastic the taper we get. But did you notice that it is the reverse of the focal cone of the beam!?



I believe this is because as the beam passes through the focus path in a more concentrated form and that the heat diameter for vaporization also increases.



So, if you were to plot this increase in intensity you would see a reversed  cone shape and that is just what we get on the edge of our parts.



I welcome comments on this to give us a better standing of the laser process. 



![images/acff3c13a0a9e0b4f031dd293f49e7f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/acff3c13a0a9e0b4f031dd293f49e7f3.jpeg)



**"Steve Clark"**

---
---
**Ned Hill** *December 13, 2017 23:06*

To me it really depends on where the focal point is relative to the material thickness, since the beam is basically an hourglass shape, as it passes through the focal point.


---
**Don Kleinschnitz Jr.** *December 14, 2017 00:40*

The beam diverges as it leaves the laser narowing to it's smallest waste and then expanding again. Like **+Ned Hill**​ says, hourglass like. The focal point is based upon a  complex  set of physics in the tube but it's smallest point is a ways out.

The K40 objective lens refocusses the beam changing the characteristics of the hourglass including the point of focus, but it's still an hourglass profile. Various focal length lenses have a longer and shorter hourglasses that's why depending on the material thickness and lens some or all of the taper is apparent. The taper can also be steep or gradual. 



Lastly, the power in the beam is gaussian shaped meaning the power across the spot is not linear.  Stronger in the middle and weaker at the edges.



Actually it's alot more complex than this ..:).


---
**Ned Hill** *December 14, 2017 01:07*

Also, You can minimize the angle of the cut by setting the height of working piece so that the laser focus point is halfway into the work piece. 


---
**Steve Clark** *December 14, 2017 06:02*

Yes, the hourglass shape is correct for the beam focus and in the example picture I posted the beam FP was into the material about 1/3rd of the thickness. Also, I understand the Gaussian effect.



However I’m not sure your understanding what I’m suggesting. Maybe I should not have said “heat” diameter for vaporization. My understanding and opinion simply stated is… the laser does not actually itself produce the heat but the C02 laser beam is a electro-magnetic light wave and at the frequency of 10.6 microns contains energy that is absorbed by the material starting at the surface.



Moving down into material the photons coming in then transfer their energy to the atoms and molecular structure pushing them into oscillation trying to hold and maintain their atomic bonds. This energy into the atomic or molecular structure of the material, in turn, causes the material to heat up and vaporize.



 Acceleration of vaporization occurs as the beam reaches its peak intensity profile.  As Don said, this “dot” looks like a spot sprayed on a piece of wood with paint can. I think its called a transverse mode of electromagnetic radiation. As the beam travels downward in the focus profile, it intensifies and the vaporization zone or node which expands and creates the reverse tapered kerf.



After or just before the beam reaches the FP and depending on the beam energy thus the pressure of the vaporization, it will start to disperse and defocus the beam and quickly losing power. The produced kerf edge and beam dispersal profile will look like the drawing below.





![images/cea7c0431e1c58e5f9b761f6e9c0262e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cea7c0431e1c58e5f9b761f6e9c0262e.jpeg)


---
**Don Kleinschnitz Jr.** *December 14, 2017 14:19*

"the laser does not actually itself produce the heat but the C02 laser beam is a electro-magnetic light wave and at the frequency of 10.6 microns contains energy that is absorbed by the material starting at the surface."



Yup.... Certainly the notion of transferring energy from one medium to another is true for generating heat in any material no? In this case its light in other cases is may be friction etc.. 



<s>----------------</s>

I see what you mean in your drawing. 

The cuts I have done, mostly on acrylic, have an annoying tapir like the beams optical profile not like the reverse dispersion scenario you suggest. I don't know about wood. 

I assume that when the material combusts the optical path is disrupted. I figured that is why we blow air into the gap, to remove the effect of the gasses from vaporization and also cool the material. Is your observation with or without air assist?



Why do you think this is an important realization :)?




---
**Steve Clark** *December 14, 2017 17:14*

LOL, no not real important, just an observation and trying to understand what is happening. I'm going to have to dig out some acrylic I've cut and look at the edges though.



Maybe part of what I'm seeing is particularity of my machines vertical alignment. 



I need more testing and data! LOL



 


---
**Ned Hill** *December 14, 2017 19:07*

I think what's happening is that the theoretical hourglass shape of a "cut" assumes there is a constant beam power density through the cut.  As the beam burns through the wood it lose considerable power density not only from the expanding beam past the focus point but also from interaction with the material.  This would then lead to a narrowing of the cut rather than an expansion.


---
**Steve Clark** *December 14, 2017 20:15*

Right, That kind of is what I'm thinking. I think as intensity of the cut beam increases by excited gases and vapor pressure in the burn sphere as it travels toward the focal point the beam is overwhelmed and defocused by the vapor and debris not being able to clear fast enough. I'm guessing this happens very close either side of the FP...


---
**Ned Hill** *December 14, 2017 20:40*

I think you might also have to consider the "cutting" profile in the direction of the beam travel.  It's not straight up and down but rather sloped down from the front edge of the beam to the trailing edge.  This would aso lead to less power density at the bottom of the cut.


---
**Steve Clark** *December 14, 2017 21:20*

**+Ned Hill**  Ya, that makes sense... as the vapor pollution increases and  power is consumed  the decreases thus burn trailing. So feed rate is a controlling factor in development of an acceptable curve in the cut along with penetration...


---
**Claudio Prezzi** *December 15, 2017 09:17*

I think it's much simpler. On the surface, the material can dissipate energy into the ambient air which cools down the material and therefore reduces the cutting energy. 


---
**Chris Kehn** *December 20, 2017 17:24*

So, in order to avoid the "taper affect", the beams optimum focal point should be at the materials surface?


---
**Ned Hill** *December 20, 2017 17:26*

No you want the focus point to be in the middle for cutting. 


---
**Steve Clark** *December 20, 2017 17:58*

**+Ned Hill** as Ned said and I also believe the lens used can change/improve the kerf profile. i.e. different FL.


---
**Don Kleinschnitz Jr.** *December 20, 2017 18:58*

**+Steve Clark** longer depth of focus = less taper. 


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/7CreZN3VHAW) &mdash; content and formatting may not be reliable*
