---
layout: post
title: "Testing laser-etching dice. Works quite well, although scale and alignment is a bit trial-and-error"
date: June 25, 2016 20:16
category: "Object produced with laser"
author: "Tev Kaber"
---
Testing laser-etching dice. Works quite well, although scale and alignment is a bit trial-and-error. Would probably be easier if I had my cutting area ruled and if I had a laser pointer to show placement better.

![images/7ac0c56bffc1a41ef560015500fe52d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7ac0c56bffc1a41ef560015500fe52d9.jpeg)



**"Tev Kaber"**

---
---
**Ariel Yahni (UniKpty)** *June 25, 2016 20:29*

Where did you buy those empty? 


---
**Tev Kaber** *June 25, 2016 20:31*

I got some white and colored ones from Amazon.

White: [https://www.amazon.com/gp/product/B00LW5CBL8/](https://www.amazon.com/gp/product/B00LW5CBL8/)

Color: [https://www.amazon.com/gp/product/B00BNWGVDO/](https://www.amazon.com/gp/product/B00BNWGVDO/)



Originally I was trying to do heat-transfer with a laser printer and clothes iron, but it never worked that well.


---
**Tev Kaber** *June 25, 2016 20:33*

Heh, I actually used the clamp the K40 comes with to hold the die in place (against a piece of wood to move it down the Y a bit).  I'm probably the only one who's actually used that terrible thing.  ;)


---
**Derek Schuetz** *June 25, 2016 20:44*

What are your setting I have these are dice and have a project 


---
**Tev Kaber** *June 25, 2016 20:48*

Pretty much the default settings, I just got it set up last night and haven't tweaked anything. 320mm/s I believe, and the power knob is roughly in the middle. I really need some kind of mod for an LED readout that shows me a power % instead of guesstimating.


---
**Derek Schuetz** *June 25, 2016 20:49*

What does your meter read (mA) when it's engraving


---
**Tev Kaber** *June 25, 2016 20:56*

Hmm, not sure, I'll take a look next time I etch. 



I feel like that meter is useless, it goes from 0-30 and if I remember right the power I was using was probably around 3 or 4, hard to read anything exact on an analog meter.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 21:36*

**+Tev Kaber** To read from the ammeter, before I start my job I usually hold down the test fire button for a few seconds whilst adjusting the dial until I get it at a value that I am happy with. The value will stay constant while you are holding the fire button down. Just make sure whatever you are going to be doing the job on isn't under the beam when you do this. I generally mark lines on the machine dial surrounds in pencil for different values (e.g. 4mA, 6mA, 10mA) so I've got a vague idea of where to set the power in future. Over time these marks may need adjustment though as the tube starts to give up the ghost.



Also, there are a few people that have done some very simple modification inline with the pot where they've placed a Volt meter that will display a value between 0-max (I think 5 or 3.3?). Not %, but better than being in the dark & better than my "test fire" method which wastes tube time.



In regards to aligning your jobs, simplest method (in my opinion) is to place some plywood (or cheap wood) & create cutting jigs where you place your objects in pre-cut "slots". As an example, assuming these dice are 20mm cubed, then I would cut a 20mm square in some ply at set co-ordinates, & then every time I go to etch one of these I would use that jig. As the dice have rounded corners you may have to use a few thicknesses of ply to get higher than the corners or aligning will not be so straight (you want to have it on the nice straight edges).


---
**Tev Kaber** *June 25, 2016 21:37*

Great tips, thanks!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 21:39*

**+Tev Kaber** Final tip, keep track of settings that work for you for future reference :) Things will be much easier that way.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/JxKEaqgqzXX) &mdash; content and formatting may not be reliable*
