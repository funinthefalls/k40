---
layout: post
title: "Had some power fluctuations or something during this etch"
date: July 02, 2016 16:07
category: "Object produced with laser"
author: "Tev Kaber"
---
Had some power fluctuations or something during this etch. My house has low voltage, I may put a line regulator on the laser. 



Shown here before and after sanding. 

![images/ab5e1f09f1e9bf540a39de276838a5db.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab5e1f09f1e9bf540a39de276838a5db.jpeg)



**"Tev Kaber"**

---


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/BxcEnkZd1WW) &mdash; content and formatting may not be reliable*
