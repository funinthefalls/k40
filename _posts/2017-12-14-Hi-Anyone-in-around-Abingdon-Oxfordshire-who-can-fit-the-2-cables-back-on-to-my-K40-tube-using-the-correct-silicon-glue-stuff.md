---
layout: post
title: "Hi Anyone in / around Abingdon, Oxfordshire who can fit the 2 cables back on to my K40 tube using the correct silicon glue stuff"
date: December 14, 2017 14:14
category: "Original software and hardware issues"
author: "Frank Farrant"
---
Hi

Anyone in / around Abingdon, Oxfordshire who can fit the 2 cables back on to my K40 tube using the correct silicon glue stuff.



Next couple of days ?



Happy to pay of course. 



Cheers

Frank





**"Frank Farrant"**

---
---
**Adrian Godwin** *December 14, 2017 14:44*

I'm not so far away, in Bedford, and I need to go that direction on Friday. But Abingdon is stuffed with tech specialists, you should be able to find someone closer.



Try oxford hackspace - [oxhack.org - Open Night / Social Night](http://Oxhack.org)




---
**Frank Farrant** *December 14, 2017 15:13*

**+Adrian Godwin** 

Be really great if you could make it over tomorrow - anytime as I'm at home all day.  Tea, biscuits, beer and cash all on offer as I'm desperate to get it working again :)

My Mob: 07718 048862


---
**Joe Alexander** *December 14, 2017 15:35*

as long as its 100% silicone it will work, and youtube has several great videos about how to reattach the wires to your tube. do not attempt to solder, simply wrap around the stub tightly, use some teflon tape to reinforce it a bit and tie it down, then silicone it. An inch of the water tubing slid over the connector afterwards help further insulate it from arcing to the chassis. just remember to slide it into the wires before you attach to the tube lol


---
**Don Kleinschnitz Jr.** *December 14, 2017 15:43*

There are replacement instructions on my blog... 

+Adrian Godwin can you check the lps IN pin for correct voltage?


---
**Adrian Godwin** *December 14, 2017 16:17*

OK. I should point out that I haven't done this procedure before, but I do have 40 years or so of practical experience in the electronics industry and am confident of following Don's procedure.

Tea, biscuits, and a tenner for petrol ?




---
**Frank Farrant** *December 14, 2017 16:21*

Sounds great, thanks very much.

Call or text me for address/postcode.




---
**Don Kleinschnitz Jr.** *December 14, 2017 19:09*

**+Adrian Godwin** **+Frank Farrant** Kinda like a K40 "reality show" episode! :)


---
**Adrian Godwin** *December 15, 2017 19:57*

Mixed success today.

 

The original tube was soldered at the HV end, twisted wire at the LV end.  I soldered the LV end OK but the HV end wouldn't take the solder (and I did use 'proper' lead solder with a nice strong rosin flux). It seemed to take eventually, with the help of some TC wire wrapped around the post, but when tested it arced badly.



I'm hoping that allowing some time for the silicone to set properly will help, but it may need another go.



Don, I couldn't test the control voltage as I was only blipping the laser on - arcing was too bad to leave it while I fiddled with a DVM.




---
**Adrian Godwin** *December 15, 2017 19:59*

The silicone (a 703 clone from china that came with my cutter) was really thin and slippery. It made it difficult to keep the silicone tube in place.



I guess it would be better to silicone the joint, let it set, then add the tubing with a touch more silicone. But I couldn't stay 24 hours for it to set :)  


---
**Don Kleinschnitz Jr.** *December 15, 2017 21:40*

**+Adrian Godwin** it was soldered on the tube end? I did not think that the anode would take solder that is why its usually wrapped and then encased in silicon.

Did you wrap it then slide silicon tubing over it followed by filling it with silicon.

When you "blipped" it on did the tube light up?



**+Frank Farrant** can you post a picture of the supply, or did you in another post I forgot.


---
**Adrian Godwin** *December 15, 2017 21:57*

Frank had already swapped the tubes, so I didn't look at the old one. But the anode wire had residual solder whilst the cathode wire had clearly only been wrapped.



I'm pretty sure the tube lit - it was hard to be sure as the arc around the anode was loud and bright so I didn't want to examine carefully. But I definitely got an impression of a pink line.



In both cases I cleaned and attempted to tin the pin, then wrapped a little stranded wire around it, soldered that to the tinning, then soldered the lpsu wire to the solder blob. This worked well on the cathode - I had successfully got some solder to take on the end of the pin - but I was less sure about the anode. I couldn't get the previously tinned HV wire to link firmly to the pin, but the wrap of stranded wire seemed firm.



Re Joe's comment - the stranded ground wire was full of silicone in the strands that had been wound around the pin. It's not like a proper gas-tight wire-wrap connection - I think there's a fair chance that silicone could creep under the entire connection and insulate it, so I'm much happier soldering it, even if it's just to a closely-fitting wrap of wire. Maybe a better approach would be to wrap it, heatshrink it, then silicone & tube it to add an insulating layer.



I'm wondering why they don't use a spring contact (a female connector pin) strengthened with heatshrink and then surrounded in silicone.




---
**Frank Farrant** *December 15, 2017 22:47*

**+Don Kleinschnitz** Do you need a picture of the PS ?  If so, what part(s) ?




---
**Frank Farrant** *December 15, 2017 22:49*

**+Adrian Godwin** Thanks for coming over. Appreciate it.  The silicone feels set, but I won't test again until the morning.


---
**Don Kleinschnitz Jr.** *December 16, 2017 12:34*

**+Frank Farrant** Just a photo of the supply from the front showing the connectors.

Can you summarize where you are with this repair now?



To date summary?

...Replaced a LPS

...Replaced a laser

...You have a digital control panel


---
**Adrian Godwin** *December 16, 2017 14:10*

Problem at the moment is severe arcing around the anode. I'm aware this can damage the lpsu so don't want to leave it on whilst investigating - prefer to do a possible fix and then a brief test.



The digital control panel is more advanced than ones I've seen before : it has a load of buttons marked +1%, +10% etc. 



Like this : 



[https://www.alibaba.com/product-detail/k40-laser_60658285211.html](https://www.alibaba.com/product-detail/k40-laser_60658285211.html) 


---
**Frank Farrant** *December 16, 2017 14:11*

**+Don Kleinschnitz** Laser stopped working 2 weeks ago and China Techy said it was the tube, so I bought a new tube ( next day from Amazon ) and put it in the unit.  

China Techy then asked me to do a test on the PS by shorting out 2 wires at the front.  I did this and he said its the PS at fault.  Shipped me a new PS and that arrived the other day.  

Then I messaged here to ask for help connecting the 2 cables to the Tube.  

.

Both wires attached yesterday by soldering and silicone, which I allowed to set overnight,  but when I pressed Test button on K40 today, I still get a loud noise and Arcing.  

.




---
**Frank Farrant** *December 16, 2017 14:13*

**+Don Kleinschnitz** Pictures to follow shortly.  

I am really dependant on the machine, which I realise is stupid, but I need to get it working.

.

Should I post another request for urgent help ?   I'd also like to borrow a K40 from someone locally if possible ?


---
**Frank Farrant** *December 16, 2017 14:22*

**+Don Kleinschnitz** **+Frank Farrant** 

![images/dd282710918ac84fc18e2afb09926022.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dd282710918ac84fc18e2afb09926022.jpeg)


---
**Frank Farrant** *December 16, 2017 14:24*

**+Don Kleinschnitz**

![images/1f96262b4dcba5e8e865dac1f9e80636.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f96262b4dcba5e8e865dac1f9e80636.jpeg)


---
**Frank Farrant** *December 16, 2017 14:25*

**+Don Kleinschnitz**

![images/1bad7ad8f6e7a0a892f41ce16effceb5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bad7ad8f6e7a0a892f41ce16effceb5.jpeg)


---
**Frank Farrant** *December 16, 2017 14:26*

**+Don Kleinschnitz** 

![images/afbd5f23e86584d4746361e0e277e7f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afbd5f23e86584d4746361e0e277e7f5.jpeg)


---
**Don Kleinschnitz Jr.** *December 16, 2017 14:42*

**+Frank Farrant** Ok now I know what supply it is.



While you find local help I will keep trying to help from remote ....



Please post close up pictures of both the lasers anode and cathode connections.



Typically even a loose connection at the anode will not create a large arc unless the tube is unloaded. 



To eliminate confusion please refer to the # of these questions when you respond. Sorry but some of these questions may be repeats of previously asked ones:



1.) what happens at the laser tube when you press the test switch that is down on the LPS?

1.a) does the anode arc?

1.b) does the tube light up

1.c) will the beam burn through a piece of tape placed on one of the mirrors



2.) can you video the laser showing the anode when you push the test button on the LPS.

3.) did the anode connection arc before you replaced the laser and replaced the connection

4.) do you have and can you use a volt meter

5.) What is that white box on top of the LPS?

6.) Is the removed insulation on the red/white wires on the left where you shorted out the wires for the china techy?


---
**Frank Farrant** *December 16, 2017 15:04*

**+Don Kleinschnitz** 

![images/dbc9965f00373062a829e834dfa9ab7c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dbc9965f00373062a829e834dfa9ab7c.jpeg)


---
**Frank Farrant** *December 16, 2017 15:05*

**+Don Kleinschnitz** 

![images/7e922c23cbb4cb4024b451d1765857d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e922c23cbb4cb4024b451d1765857d7.jpeg)


---
**Frank Farrant** *December 16, 2017 15:07*

**+Don Kleinschnitz** 

Following a test this morning where it s Arced I cleaned the surrounding cables and metal work and when I plugged it in again I get no power at all. The machine doesn't power up.  Should I check fuse in plug ?


---
**Frank Farrant** *December 16, 2017 15:27*

**+Don Kleinschnitz** 

![images/ac5f48d284b5d0c739d998b3c2a15402.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ac5f48d284b5d0c739d998b3c2a15402.jpeg)


---
**Frank Farrant** *December 16, 2017 15:28*

**+Don Kleinschnitz** 

#4 Yes and yes with instructions

#5 The white box goes to the light in the cutting area. 

#6 Yes. I realise I need to tape them up. 


---
**Don Kleinschnitz Jr.** *December 16, 2017 15:46*

**+Frank Farrant** gosh! yes there should be a fuse in the rear plug assy.


---
**Don Kleinschnitz Jr.** *December 16, 2017 15:49*

**+Don Kleinschnitz** let me know when you get it powered again, make sure the green light on the LPS is on. 

Then lets answer questions 1-3.


---
**Frank Farrant** *December 16, 2017 16:14*

**+Don Kleinschnitz** 

![images/354ceb580b10010cbcafbeb06793bac5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/354ceb580b10010cbcafbeb06793bac5.jpeg)


---
**Frank Farrant** *December 16, 2017 16:15*

**+Don Kleinschnitz** I can't see one?

Should I check the fuse in the plug ?


---
**Frank Farrant** *December 16, 2017 16:23*

**+Don Kleinschnitz** 

Plug is ok. 

Is there an internal fuse I can check ?

Thanks 


---
**Adrian Godwin** *December 16, 2017 16:56*

**+Frank Farrant** you can remove that white sleeving tied on the cathode now. It was only there to keep the silicone in place until it set. Shouldn't make any difference to the arcing though.




---
**Adrian Godwin** *December 16, 2017 16:57*

**+Don Kleinschnitz** you mention 'tube unloaded' - I guess this could happen if the cathode connection was poor. **+Frank Farrant** check the wiring in the power supply section. Maybe the blue ground wire wasn't reconnected ?




---
**Frank Farrant** *December 16, 2017 17:10*

**+Adrian Godwin** I'm not getting any power in to the machine at all, nothing.  The machine doesn't start, no lights, no light on PS, zilch.

Not sure what to do now.  Don't hardly ever panic, but I have a dozen Christmas Eve boxes to do and no machine at the mo.




---
**Adrian Godwin** *December 16, 2017 17:15*

**+Frank Farrant** the mains inlet on the rear panel has a fuse in it. Under where the mains lead ('kettle lead') goes is a little compartment which can be pulled out. It should have at least one fuse in (and sometimes there's a spare too).



I notice that there are two blue wires on the green connector to the power supply. One goes to mains, the other goes to the tube cathode. I'm assuming they're connected correctly since I think incorrect wiring there would stop the power supply coming on at all, and you probably just unplugged the old supply and plugged in the new. But you could check that they're good connections (screwed down onto the actual wire, not the insulation) and the screws are tight.

 


---
**Frank Farrant** *December 16, 2017 18:15*

**+Adrian Godwin** **+Don Kleinschnitz**

The fuse in the inlet looked ok but I swapped it with the spare but still no power. 

No light on the board, nothing. 

Panicking now. What can I do next ?


---
**Frank Farrant** *December 16, 2017 18:36*

**+Adrian Godwin** **+Don Kleinschnitz**

Ok. Solved the No Power issue. The EStop button had been pressed down by the Laser Machihe Gremlin 😂

So, back to Arcing issue. 


---
**Frank Farrant** *December 16, 2017 18:40*

**+Don Kleinschnitz** **+Adrian Godwin**

#3 No. It never arced before. 

For #1 and #2, if I video the tube from the Anode end whilst pressing test, this will show answers to both won't it ?

Do I press the Test button on the PS ?


---
**Don Kleinschnitz Jr.** *December 16, 2017 19:51*

**+Frank Farrant** Yes all but 1c which is to see if the beam is exiting the laser.

Yes the button down on the PS.




---
**Frank Farrant** *December 16, 2017 21:03*

**+Don Kleinschnitz** 

![images/5e39bc6b786a75246514b399ae2a6329](https://gitlab.com/funinthefalls/k40/raw/master/images/5e39bc6b786a75246514b399ae2a6329)


---
**Frank Farrant** *December 16, 2017 21:05*

**+Don Kleinschnitz** 

![images/aca364902d76caf93235ade2bc49932f](https://gitlab.com/funinthefalls/k40/raw/master/images/aca364902d76caf93235ade2bc49932f)


---
**Frank Farrant** *December 16, 2017 21:06*

**+Don Kleinschnitz** 

I don't think the tube lights up at all. 

I also put some tape on the first mirror but nothing on it. 


---
**Frank Farrant** *December 16, 2017 21:21*

**+Don Kleinschnitz** Faulty tube maybe ?

Should I try the original one back in ?


---
**Don Kleinschnitz Jr.** *December 16, 2017 21:44*

**+Frank Farrant** well its clear from the arc that the LPS is good :)!



Is the anode to close to side wall? Try rotating the tube 45 degrees so  the anode is further away from the case.




---
**Frank Farrant** *December 16, 2017 21:51*

**+Don Kleinschnitz** we moved the tube to the left ( looking from front of machine ) as much as possible but I'll try the 45 dwg twist now. BRB. 


---
**Frank Farrant** *December 16, 2017 22:09*

**+Don Kleinschnitz** I know the previous tube was only a finger width from the end of the case at the Anode end as I wanted to make sure I put the new one in at a similar place. 

I'm also certain that the 2 connections were upright. 

Anyway. Tried turning it. No change as your'll see in vids to follow. 


---
**Frank Farrant** *December 16, 2017 22:12*

**+Don Kleinschnitz**

![images/f3ee58f1e0d916563dc5043a803517fa](https://gitlab.com/funinthefalls/k40/raw/master/images/f3ee58f1e0d916563dc5043a803517fa)


---
**Frank Farrant** *December 16, 2017 22:18*

**+Don Kleinschnitz** 

![images/2eb6631815ac10f37455f21bf5d1c5ae](https://gitlab.com/funinthefalls/k40/raw/master/images/2eb6631815ac10f37455f21bf5d1c5ae)


---
**Frank Farrant** *December 16, 2017 22:20*

**+Don Kleinschnitz** I checked with China Techy that the tube was correct before buying it but is there a chance it's wrong and won't work as a result ?


---
**Frank Farrant** *December 16, 2017 22:30*

**+Don Kleinschnitz** Is it possible that the new Tube was DOA ?


---
**Don Kleinschnitz Jr.** *December 16, 2017 22:43*

**+Frank Farrant** sure with china all things are possible. Surprising though.

Are you sure that the cathode is connected to gnd. Can you measure the resistance to the frame from the cathode, pls.


---
**Frank Farrant** *December 16, 2017 22:45*

**+Don Kleinschnitz** How do I do that ?


---
**Adrian Godwin** *December 17, 2017 10:01*

The tricky thing is getting a connection to the cathode - you really want to measure from the pin, not the wire.



But try :



Set the meter to ohms, touch the probes together to check it changes from infinity to near zero. Connect one probe (doesn't matter which) to chassis. Maybe use the earthing screw. Touch the other probe to the cathode pin. You might be able to poke it down through the silicone until it connects, but you'll probably be testing to the solder, not the pin. I guess to be sure you'd have to pull the silicone off and touch to the actual pin, not the place where I soldered the wire. 


---
**Frank Farrant** *December 17, 2017 12:50*

**+Don Kleinschnitz** **+Adrian Godwin**

If I try Cathode to Chassis the reading doesn't change on the Meter. 

Measured Cathode Pin to Earth pin on Green block on PS connector and I get 00.7


---
**Don Kleinschnitz Jr.** *December 17, 2017 14:45*

**+Frank Farrant** 

by: "If I try Cathode to Chassis the reading doesn't change on the Meter. " do you mean that it reads open i.e. no ohms?



Perhaps post a picture of the meters face while doing that test.



"Measured Cathode Pin to Earth pin on Green block on PS connector and I get 00.7" was the "Earth pin" the "L" pin on the LPS or the FG?



On the LPS left green connector:

A.) "L" read to the cathode pin on the laser should read very low resistance

B.) "FG" read to the case should read very low resistance.



Post a picture of your meters face for either A or B that does not read close to 0 ohms.


---
**Frank Farrant** *December 17, 2017 15:35*

**+Don Kleinschnitz** 

"L" to Cathode

![images/4d222ed93cf8df88255e33dcb92dd90c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4d222ed93cf8df88255e33dcb92dd90c.jpeg)


---
**Frank Farrant** *December 17, 2017 15:41*

**+Don Kleinschnitz** 

"FG" to case



![images/86b10410bc8d4d93ea44bbe4089d7dec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/86b10410bc8d4d93ea44bbe4089d7dec.jpeg)


---
**Don Kleinschnitz Jr.** *December 17, 2017 20:08*

**+Frank Farrant** 

OK how about 

C.) "L" to case

and 

D.) "L" to "FG"?


---
**Frank Farrant** *December 17, 2017 20:21*

**+Don Kleinschnitz** 

C: 00.6

D: 00.5 - 00.6


---
**Frank Farrant** *December 17, 2017 21:16*

**+Don Kleinschnitz** 

Hi Don

Are you aware of a Silicone I can buy and safely use here in the uk ?

Was thinking of changing the tube back to the original one ?


---
**Frank Farrant** *December 17, 2017 22:21*

**+Joe Alexander** **+Don Kleinschnitz**

Hi Joe / Don

I didn't get any silicone with the machine so need to buy some.  I'm in the uk - s this one ok ?

[amazon.co.uk - Permatex 81724 Sensor-Safe Ultra Blue RTV Silicone Gasket Maker, 3.35 oz. Tube Size: 3.35 Ounce: : Car & Motorbike](https://www.amazon.co.uk/Permatex-81724-Sensor-Safe-Silicone-Gasket/dp/B00TQ3GAA6)






---
**Adrian Godwin** *December 18, 2017 00:10*

This one should do (note, it's 'neutral cure')



[wickes.co.uk - Wickes Frame Silicone Sealant White 310ml &#x7c;](http://www.wickes.co.uk/Wickes-Frame-Silicone-Sealant-White-310ml/p/243013)



I suggested Dow 703 as it sounds like a Dow number and most of the chinese suppliers call it 703. But it seems Dow don't make that - though '703 silicone' will find plenty of suitable products.



What you need is 'alkoxy based' or 'neutral cure'. NOT 'acetoxy based'. Avoid anything with 'acet' in the name - it generates acetic acid while curing which will corrode the wire and pin


---
**Frank Farrant** *December 18, 2017 00:18*

**+Adrian Godwin** **+Don Kleinschnitz**

Thanks. 

It says 48 hours to cure.  Can I test it before that ?


---
**Don Kleinschnitz Jr.** *December 18, 2017 03:04*

**+Frank Farrant** it looks like your cathode is connected to the supply and ground correctly....



This seems like a bad tube although that is hard to believe.


---
**Don Kleinschnitz Jr.** *December 18, 2017 12:34*

**+Frank Farrant** Here is one example of a user that had arc'ing as a result of a bad tube:



[plus.google.com - Another arcing video. This one is more similar to what I experienced on one o...](https://plus.google.com/106608075082031214915/posts/9LYvrYuCkMi)


---
**Frank Farrant** *December 18, 2017 12:46*

**+Don Kleinschnitz** thanks

Will this be ok as I can buy locally

 Servisol Silicone Adhesive Sealant 75ml



Code: RE89W




---
**Don Kleinschnitz Jr.** *December 18, 2017 13:05*

**+Frank Farrant** I think it will be ok, you can always replace it when we get you working. 

At this point I would guess that nothing will stop this arc if the tube is open. 

I use the blue stuff you linked to earlier. I have to check on the corrosive impact but I think that problem is well outside of your timeline.

BTW I found the Permatex Blue RTV at auto stores and in Wallmart.


---
**Frank Farrant** *December 18, 2017 13:34*

**+Don Kleinschnitz** thanks Don. 

I'll try the original tube back in with the Servisol Silicone. 

Since posting, the Chinese tech guy has said it's ok to use. 

Any idea how long I should wait before I try it ?  24 hours ?


---
**Don Kleinschnitz Jr.** *December 18, 2017 13:52*

I would recommend 24hrs. 

What has the Chinese tech said about the current state of the problem...tube bad?

Are you getting warranty help from them.


---
**Frank Farrant** *December 18, 2017 14:09*

**+Don Kleinschnitz** I bought this tube from Amazon seller almost £200 so it will be going back. 

When the machine stopped working Chinese said tube but after I had ordered it they said to test the PS and then sent me a new PS. 

I will put the original tube back in and follow the video about twisting the cables on and using Silicone. 

Really hope it works. 

Will look for a 2nd hand K40 as a spare as I've lost 100's of orders. 

Will let you know how it goes. 

Thanks 


---
**Frank Farrant** *December 20, 2017 14:16*

**+Don Kleinschnitz** **+Adrian Godwin**

Hi. Getting somewhere. 

Original tube back in and cables on yesterday. Tested it this morning and fires ok. Checked the mirrors and all looks good. 

However, I've just gone to cut some 3mm mdf and Ply but it won't cut anywhere near through it. More like a deep engrave. 

Using all usual settings / height etc. 

.

Does the new PS need some type of adjustment so it matched with % reading on the Amperemeter indicator ?

Thanks 


---
**Adrian Godwin** *December 20, 2017 14:32*

Don covers this in depth at [donsthings.blogspot.co.uk - Engraving and PWM Control](http://donsthings.blogspot.co.uk/2016/12/engraving-and-pwm-control.html) but mostly with respect to a smoothie controller. It may also be relevant to the stock controller but I'm not sure of the setup. Anyone know ?



I've used both digital and analog controls on laser cutters and I feel more comfortable when there's an actual milliameter present. The digital dials seem to be just % of max PWM output and don't really tell you what the laser is doing. 


---
**Frank Farrant** *December 20, 2017 15:19*

**+Adrian Godwin** **+Don Kleinschnitz**

I've only ever used the digital reading on my K40d and know as much about electronics / lasers, as a goldfish.  

Anyone with a K40d had to change PS - did this affect the % power needed ?

Any tests I can do ?


---
**Don Kleinschnitz Jr.** *December 20, 2017 16:10*

**+Frank Farrant** the LPS should not need adjustment. I know you have checked the mirrors but I would suspect an optical alignment problem. With all the tube jocky-ing its likely your laser tube is out of alignment. 

I would walk through an alignment again insuring that the tube is parallel to the gantry and the beam is going straight through the cutting head and not hitting anything.



The reason I do not have a digital panel is that the actual current from the tube is the only we  have as to what the tube is actually doing.


---
**Adrian Godwin** *December 20, 2017 20:52*

**+Don Kleinschnitz** I think some PSUs have a gain adjustment that let you set the actual tube current that corresponds to 100% on the digital meter. Or there may be a setting somewhere that does the same thing.



I recall a PSU on our first hackspace laser that was set too high, so 100% on the panel was more than the max for the 50W tube. We think this contributed to a short life of the tube, though most tubes we tried got far less life than advertised, even after adjusting this.



Unfortunately, even if you can find the adjustment, you need a real meter in the tube circuit to set it up correctly.




---
**Don Kleinschnitz Jr.** *December 20, 2017 21:49*

**+Adrian Godwin** yes there is an adjustment pot but its not an adjustment that I think would change **+Frank Farrant** power that much and there is no good way to know optical power vs current.


---
*Imported from [Google+](https://plus.google.com/110419041310362555971/posts/424bgcFe5yX) &mdash; content and formatting may not be reliable*
