---
layout: post
title: "HP Persson Are you the one that had an open frame head on your machine?"
date: December 02, 2018 13:38
category: "Modification"
author: "Don Kleinschnitz Jr."
---
**+HP Persson**



Are you the one that had an open frame head on your machine? I am looking at options for designing a new head for my K40 and considering an open frame vs tube head. Any thoughts on the design or a reference picture would be useful?













**"Don Kleinschnitz Jr."**

---
---
**HalfNormal** *December 02, 2018 16:18*

I did/do not have an open head design on my machine. I am more of a fan of the enclosed design which also facilitates using air assist.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/iq1CLmNP2vv) &mdash; content and formatting may not be reliable*
