---
layout: post
title: "Actually received reply from the vendor! I mentioned in another post that my laser has developed issues"
date: February 17, 2016 17:08
category: "Discussion"
author: "HalfNormal"
---
Actually received reply from the vendor!



I mentioned in another post that my laser has developed issues. I received a reply from the vendor verifying that it does have a two year warranty and please send as much info including pictures and/or video of the issue so that it can be resolved. WOW! Not bad for a $366 gamble!





**"HalfNormal"**

---
---
**3D Laser** *February 17, 2016 17:24*

Awesome. Mine came with a dent in the side and the gave me a 75 dollar refund.  For as cheap as these machines are they sure do back them pretty well


---
**Scott Marshall** *February 17, 2016 18:31*

Glad to hear it Halfnormal - It's good to hear of a positive surprise!



I too had damage issues, and it was handled well. A $200 refund on a $389 machine. It wasn't the sellers fault either, the poor packing was done by the seller in China, and the US seller never opened the box.



Remember 2 things, #1, these cost a lot to ship back. The "Crate Shifters" as I've heard them called, get a VERY good deal from shipping companies as they ship a lot of stuff. If the consumer ships it back, the shipping costs rapidly go deep into the sellers cost of the machine. The 2nd is that ebay sellers get hit hard if they show anything less than perfect feedback. a couple of bad reports hits a seller right in the wallet for some time to come.



Another reason they settle up for cash is they sell boxes of "stuff" and know nothing of what's in the box. My seller has available everything from tire machines, to restaurant equipment to bed linens by the gross.

They never open the box. A container comes off a boat to a rented holding area at the boatyard, gets unpacked, crates addressed and shipped, without  ever transferring to a warehouse if they can manage it.

It's part of how they sell this stuff so cheap. Very few workers, NO parts, No service, Just shipping and receiving. It's all good unless something bad happens.



Which is good for everyone as long as you know what the deal is.

I did a lot of homework before I ordered the laser.



I see a lot of posts from people who expect Wal-mart type customer service, and never thought about WHY they are getting it so cheap.

If you want High end service, you pay for it, one way or another.

When you consider a fully thought out and backed machine (still Asian made) with the capabilities of the K40, a well written manual, and service organization will cost you AT LEAST $4000US, the K40 makes sense even with the negatives. 

You have to be technically capable (or at least willing to learn), understand the dangers, and expect it to take time and money to get it working. For most of us, that's fine, we would mod our $4000 laser if we could afford one.



For many of us, it's the only way we could ever afford to own a laser cutter besides scratch building, and even that path is more expensive than the K40 at it's current price point.

Even if I had to buy an entire 2nd machine for parts, I don't think I could complain about it much. 



That being said, I would never consider using one of these machines to make a living. They're basically a kit, shipped assembled as a packing method. The parts and construction are is cheap and low quality as it gets, but hey, it's a loveable ugly duckling. And some darned impressive work can be done with it, with proper attention.



This Site will do a lot toward helping prospective buyers know what they are getting into. 



HalfNormal and Corey, who did you buy from?



I think would be good to get a list going of GOOD (rather than the bad ones) sellers,

because I'd bet most of us here will be in the market again....



 I dealt with Banyan Imports known on ebay as "GlobalFreeShipping" and would buy from them again.



Scott 


---
**HalfNormal** *February 17, 2016 19:04*

I second everything you stated Scott. In the few months I have been using this unit, I know that I will be purchasing a larger named brand unit and start doing some "side business" with it. I would have been upset if I spent a lot of money only to find out that it would collect dust or be too much work to use.

The "broker" was Sasha but the company I am working with is technology-etrade from Ebay. Nice part is that everything is traceable back to the purchase.


---
**Scott Marshall** *February 17, 2016 19:35*

**+HalfNormal** 

The big boys ought to be glad these are in the mix, they are selling a lot of high end lasers. Like you said, they are the "Test unit" for a lot of us.



For me, I wanted to build from scratch, but couldn't pass up the low buck way to get a feel for the whole system. I figured if it was total junk, I'd use some of the parts. Or, like you said, I was concerned it would end up in the corner. That's not going to happen. This is a powerful, practical tool. Making gaskets, boxes, even cutting felt for lining drawers, I have a new "go-to" tool in my box! I'm pleasantly surprised.



If I could afford a name brand, I'd go that way too, but I think I'm going to build a 80-100watt unit with a 1 meter bed. Mach 3 and a Smoothstepper or something like it. I may even set it up to carry a small router head.



At least now when I throw down the big bucks, I'm A LOT more educated than I was. Worth every cent, even if it blows up today.



And, I have actually sold some trinkets I made on it. Good therapy.



Scott


---
**3D Laser** *February 17, 2016 19:43*

**+Scott Marshall** my seller was on eBay name Vipshopforyou


---
**3D Laser** *February 17, 2016 19:44*

**+HalfNormal** btw did you get my email


---
**HalfNormal** *February 17, 2016 22:26*

**+Corey Budwine**

Yep replied. It will be in the mail  this evening.


---
**HalfNormal** *February 17, 2016 22:29*

**+Scott Marshall** I would really like to build my own but when it is time to "do it" I will be hitting the ground running. Looks like it will be keeping me plenty busy. And yes I will be keeping the K40 for the small jobs.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/bhMTc6yzeoS) &mdash; content and formatting may not be reliable*
