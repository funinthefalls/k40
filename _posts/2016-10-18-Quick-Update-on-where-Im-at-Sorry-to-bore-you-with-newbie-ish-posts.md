---
layout: post
title: "Quick Update on where I'm at. Sorry to bore you with newbie ish posts"
date: October 18, 2016 18:47
category: "Discussion"
author: "Kris Sturgess"
---
Quick Update on where I'm at. Sorry to bore you with newbie ish posts.



Laser is setup.

Water reservoir is running good. No bubbles in tube. Good Flow. 22-24c Temp

Air Assist. Running Good.

Exhaust Fan is running Ok.



I had to fix the acrylic window. All 4 corners were snapped because someone in China was overzealous. The 2 screw posts in the center came detached. I'll leave it for now. May epoxy them back on at a later date.



Confirmed plugs at back are 110v un-grounded. Currently have fan, air pump and water pump plugged in with no issues.



Software is somewhat installed. Everything was in Chinese. Got LaserDrw, Winseal and CorelLaser installed. The CorelDraw file craps out at install every time. Not sure what to do?



Laser is completely out of alignment. Only works for bottom left side for my brief testing. I'm currently 3D printing some alignment holders and have a copy of TimtheWombats alignment instructions printed.



Going to give it a whirl tonight when I have some un-interupted time to do it right. I understand it will take a while (many attempts) and is a "learned' art. 



As always, open to wisdom, guidance and suggestions.



Kris the newbie with laser





**"Kris Sturgess"**

---
---
**Ariel Yahni (UniKpty)** *October 18, 2016 18:52*

It's looks you've been preparing for a while or this is your 10 laser. Good job


---
**Kris Sturgess** *October 18, 2016 18:54*

10 laser? This is my first "big" cutting laser. I have a Neje 8K 1W that I tinkered with before. So a lot is all new to me. Just trying to be cautious and not screw anything up. lol


---
**Jim Hatch** *October 18, 2016 19:58*

Don't operate it without the acrylic window unless you've got laser safe safety glasses. Otherwise it's pretty easy for laser reflection to occur which won't cut anything when it bounces but it will still boil an eyeball.




---
**Kris Sturgess** *October 18, 2016 20:21*

Thanks **+Jim Hatch** I'm not that dumb. lol As I stated, I fixed the acrylic window. Only it's attached by the 4 corners. The center posts are not attached. No gap is visible with the window mounted in this way. 


---
**Jim Hatch** *October 18, 2016 20:27*

😀 Gorilla duct tape is good for taping it in place. Or 3M makes an exterior grade double sided tape that's just wicked tenacious. The corner screws are probably all you really need though.


---
**Kris Sturgess** *October 18, 2016 20:41*

It's all good for now. Barely noticeable.  

On to bigger issues. Managed to get CorelDraw X4 installed. It's completely in Chinese. Should I locate another version? How does it work with this stupid security dongle? Should I just go with Laserdrw? 


---
**Jim Hatch** *October 18, 2016 23:05*

LaserDrw is awful. I use it only to troubleshoot the machine - if it can't connect then Corel won't either.



I got Corel Draw 12 with my machine and a CorelLaser plugin to install. But I don't like pirated software so i installed my own copy of CotelDraw X8. Then the plugin from the CD and it all works well. (On Win 7 and Win10)


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/aFjWPcdGJ9U) &mdash; content and formatting may not be reliable*
