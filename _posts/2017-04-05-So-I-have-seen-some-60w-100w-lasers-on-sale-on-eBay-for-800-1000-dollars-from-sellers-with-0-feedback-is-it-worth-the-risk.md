---
layout: post
title: "So I have seen some 60w-100w lasers on sale on eBay for 800-1000 dollars from sellers with 0 feedback is it worth the risk"
date: April 05, 2017 17:42
category: "Discussion"
author: "3D Laser"
---
So I have seen some 60w-100w lasers on sale on eBay for 800-1000 dollars from sellers with 0 feedback is it worth the risk 





**"3D Laser"**

---
---
**Stephane Buisson** *April 05, 2017 18:00*

better have pay on collection or Paypal guaranty.


---
**Ned Hill** *April 05, 2017 18:08*

I assume that's without shipping?


---
**3D Laser** *April 05, 2017 18:08*

**+Stephane Buisson** how do you do pay on collection in eBay do you know 


---
**3D Laser** *April 05, 2017 18:08*

**+Ned Hill** shipping says 20 I will try to post the links in a bit 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 18:45*

Every seller has to start at 0 feedback rating, but I'm surprised ebay allows them to sell 800-1000 dollar items with 0 feedback. I recall having to get my selling price limit increased above $100 when I first started selling things on ebay (back in 2014 odd).



As others have said, the risk is not something small so just verify that you have assurances in place in case something goes awry.


---
**Stephane Buisson** *April 05, 2017 18:48*

**+Corey Budwine**, it's an agreement with the seller, you must ask the seller before bidding.


---
**Jeff Johnson** *April 05, 2017 21:49*

I've seen a couple of these and most steal stock images from another seller and sometimes don't even bother to remove the sellers name or watermark. I don't trust them.




---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/RSpUkYrMDcP) &mdash; content and formatting may not be reliable*
