---
layout: post
title: "I have heard that some material produces dangerous gasses when cut with a laser"
date: November 07, 2017 02:24
category: "Materials and settings"
author: "timb12957"
---
I have heard that some material produces dangerous gasses when cut with a laser. Is it ok to cut Polystyrene foam insulation board with a laser?

![images/b61842569d9d8abf73966a5211e7e36c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b61842569d9d8abf73966a5211e7e36c.jpeg)



**"timb12957"**

---
---
**Ned Hill** *November 07, 2017 03:11*

Not sure how well it will cut vs melting, but there is no  extra danger with fumes for this material.  Just make sure you have good ventilation as with all things.


---
**Ned Hill** *November 07, 2017 03:17*

Actually there is a danger with fire as this material melts (as I was thinking) and tends to catch fire.  It's only recommended to try cutting thin pieces while paying close attention.


---
**Michael Audette** *November 07, 2017 11:47*

Cut with hot wire all the time.  Doesn't'' catch fire with the wire but might with a laser.  Not sure if it's fumes are toxic...I've always done it in a well ventilated area.




---
**HalfNormal** *November 07, 2017 12:46*

Here is a list of what is safe and what is not safe to cut with a laser.



[atxhackerspace.org - Laser Cutter Materials - ATXHackerspace](http://atxhackerspace.org/wiki/Laser_Cutter_Materials)


---
**Michael Audette** *November 07, 2017 12:49*

**+HalfNormal** - excellent list! - Again - I've only hit this with a hot-wire in the past....I don't think I would ever stick this in a laser.




---
**Customer Service Sinjoe** *November 08, 2017 14:46*

A problem is horrible smell.If with a filter tank that would be perfect.




---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/NiRGxFxiXrW) &mdash; content and formatting may not be reliable*
