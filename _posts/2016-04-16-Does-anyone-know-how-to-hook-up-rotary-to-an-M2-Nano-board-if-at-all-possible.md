---
layout: post
title: "Does anyone know how to hook up rotary to an M2 Nano board, if at all possible"
date: April 16, 2016 12:20
category: "Hardware and Laser settings"
author: "Mishko Mishko"
---
Does anyone know how to hook up rotary to an M2 Nano board, if at all possible. I've been searching here and on youtube, but no luck so far...





**"Mishko Mishko"**

---
---
**Scott Marshall** *April 16, 2016 16:00*

Most rotary attachments just connect to the Y axis and replace it. It's that simple in most cases, that's probably there's no video.



You may or may not have to change the steps/mm setting in the driver software, depending on drive ratio (roller size, steps/rev) and such.



The only thing you may need to watch out for is the current setting in the motor driver. Your attachment may have a smaller stepper motor and require you turn down the current setting in the Y driver. Just compare the motor specs of your current Y motor to the rotary's motor, usually right on the motors. If the stepper's current rating is larger, it's probably ok to run it as is, if it's lower you may want to turn the driver down to prevent overheating the rotary's motor.



The only possible gotcha would be if one or the other motor was a different type. Most are bipolars and can be connected 4 wire (google stepper connections), but if one is a unipolar, you'll be out of luck unless you change driver modules. Chances of this are very remote, Bipolar motors are the standard, and the unipolars are pretty much obsolete and not used much in Home lasers/printers etc.



If you run into problems, post the details here, and I or another of our helpful members will help you through it.



Scott


---
**Mishko Mishko** *April 16, 2016 16:18*

Thank you Scott. I thought that maybe there's some dedicated connector. I only have bipolar motors, so I guess I'll simply choose one with similar current, so I don't have to change that every time i switch to and from rotary, should be more than enough for what I need, I don't intend to move heavy objects. If I choose the method of turning by friction, it shouldn't be hard to adjust the steps/mm just by choosing the diameter of the contact wheel. Of course, if i use the chuck, this is another story, but I'm leaning towards the friction, at least for the start. I guess I'll just cut it from acrylic and glue it together, throw in some small bearings and two rubber wheels, this should do the trick. Will post if it works well. Thanks again.


---
**Scott Marshall** *April 16, 2016 18:11*

You're absolutely correct about choosing the right size motor to match your existing Y motor, and as long as the Rotary stepper motor has a larger current rating than your Y motors, you should be able to swap out without changing anything. (Once you have compatible connectors installed) 



Driving a bipolar motor under it's rated current just limits it's torque/speed which shouldn't be a problem for most objects you would normally rotary engrave. Your workpiece would have to be solid stone or something like that to require much power at all.



Most Lasers use NEMA 23 motors with 200 steps/rev. You can probably find an exact match without much searching. If you figure out your existing drive ratio (pulley tooth count and the Belt's tooth count) it's easy to figure out your current step/mm (or inch) and match it with your rotarys drive wheel - Pi x D/200...



Good Luck,

Scott


---
**Mishko Mishko** *April 16, 2016 20:39*

My motors are NEMA 17, 17HM4410N and 17HM3448N. I've got plenty of those to try, not a problem really, will figure the current empirically if nothing else;) Mine are all 1.8 deg/step, so no problem there either, just have to do some math with belts/tooth count etc., just as you proposed...

Thanks Scott


---
**Stephane Buisson** *April 17, 2016 06:02*

**+Mishko Mishko** take care of very limited power delivred by PSU, you can check the spec for my motor (plenty of different motor are used in K40)

 [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide).


---
**Mishko Mishko** *April 17, 2016 07:28*

Thanks Stephane, that's very informative. The motor I picked from my stock shouldn't draw more current than this board can supply, at least I think so. It's just an old stepper from the batch I have for years, rated 12V/75ohms, so the current is around 160mA. Your Y motor, just for comparison, is rated at 430 mA, so I believe there's plenty of slack there.



I've tried to google my motors, but got nothing except when I changed the lettering a bit (HM to HW), and that only for the x motor, not sure if it's the same thing, [http://www.aliexpress.com/item/17HW3448-stepper-motor/32612896261.html](http://www.aliexpress.com/item/17HW3448-stepper-motor/32612896261.html)



I've decided to wait with the board replacement until I figure everything else and do some mods, and for the time being, even the existing board and Corel Laser software do the job, if a bit clumsy;) But if I manage to fry the board, that will only streamline the process, so I'm not overly concerned. Will be careful though, anyway;)


---
**Alex Hodge** *April 27, 2016 16:17*

**+Mishko Mishko** I've got the same stepper motors as you. I know you posted about this a while back but I'm trying to get up and running with my new smoothie based board and I can't find any details on these stepper motors! Have you had any luck? I need to know the max current rating at least.


---
**BeenThere DoneThat** *March 18, 2017 21:20*

So, did anyone find a replacement for the Y axis stepper motor? Mine is labeled Type 42BHM39-100-22D. 



Im attaching a picture. On the sticker,  it says I=500.  Is this possibly the current rating? 500ma?



I'm building a rotary, and i just dont want to fry the controller board. If the A4988 on the controller is regulated to =<500ma, I should be able to connect my 800ma max rated motor without hurting any thing.



I feel like there aren't that many of us doing this in the K40 with the M2Nano. Light object says not to hook theirs directly to the controller, but i dont seem to find anything in the way of a motor drive to go in between. So I'm assuming everyone using those has switched out their controllers to a controller that supports the motor. 



So, I've shopped a little for another nema17 motor compatible with the existing, but I cant find the specs.

![images/61c078b10ddd9da7d1ad9f4fce0289e3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/61c078b10ddd9da7d1ad9f4fce0289e3.jpeg)


---
**Gordon Drage** *August 07, 2017 00:26*

**+BeenThere DoneThat** Did you have any luck finding a compatible replacement motor for the Y-Axis?  I'm looking for the same one.  My part number is the same as yours, so if you found a suitable replacement, I'd appreciate the new part number.  I just cant seem to find a site that has the spes for the 42BHM39-100-22.  A couple have made me think that a 42BYGH prefix may be the new numbering, but I'd appreciate any confirmation before I part with my 'hard earned'.  :-)


---
**BeenThere DoneThat** *August 08, 2017 04:32*

I was not looking to replace my motor, I was attempting to establish the electrical specs of the existing one, so as not to damage the controller by attaching a rotary X axis. Turns out I did the math, and I used a motor that just exceeded 500ma current rating, maybe 600ma, and voltage was at least 12 or so, I think mine was rated at 48v. I didn't care about torque because I'm driving a ~50:1 gear box. 


---
**Gordon Drage** *August 08, 2017 05:47*

Thanks **+BeenThere DoneThat**, that is what I am trying to do too.  I dont need to replace my Y-Axis motor, but I wanted to find one I can just plug into the board, by swapping the original plug.  I'm trying to build a rotary axis and plan to just plug in a matching stepper to the Y-axis and then - hopefully - get the gearing right to have the rotation at something acceptable to engrave cylindrical things with... If yours just 'plugs and plays' without any issue, could you post what the part number is of the one you used?  If not, thank you anyway, I can probably work with what you have already provided.


---
*Imported from [Google+](https://plus.google.com/114169453703152974799/posts/Ks5GA117KAT) &mdash; content and formatting may not be reliable*
