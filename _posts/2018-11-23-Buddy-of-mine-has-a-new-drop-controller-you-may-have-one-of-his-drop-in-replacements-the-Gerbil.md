---
layout: post
title: "Buddy of mine has a new drop controller, you may have one of his drop in replacements, the Gerbil"
date: November 23, 2018 06:12
category: "External links&#x3a; Blog, forum, etc"
author: "Anthony Bolgar"
---
Buddy of mine has a new drop controller, you may have one of his drop in replacements, the Gerbil. Now he has the super gerbil, kick starter is going on now. Great board, here is a video of a mill conversion he did, but it works with lasers as well (X,Y,Z and Rotary capable)



Kickstarter is at [https://www.kickstarter.com/projects/2118335444/automate-anything-with-super-gerbil-cnc-gcode-cont?ref=discovery&term=super%20gerbil](https://www.kickstarter.com/projects/2118335444/automate-anything-with-super-gerbil-cnc-gcode-cont?ref=discovery&term=super%20gerbil)




{% include youtubePlayer.html id="G_9dc0BAf0M" %}
[https://youtu.be/G_9dc0BAf0M](https://youtu.be/G_9dc0BAf0M)





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *November 23, 2018 08:33*

If you have any questions, feel free to ask. I have a direct line to Paul, so answers will come from the horses mouth!




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/EXTBnRkqaxm) &mdash; content and formatting may not be reliable*
