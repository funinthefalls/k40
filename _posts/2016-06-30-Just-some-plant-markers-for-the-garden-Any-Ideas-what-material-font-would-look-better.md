---
layout: post
title: "Just some plant-markers for the garden. Any Ideas what material/font would look better?"
date: June 30, 2016 00:01
category: "Object produced with laser"
author: "Christoph E."
---
Just some plant-markers for the garden.



Any Ideas what material/font would look better?

As the main-font isn't really readable I'm not yet satisfied. :-(



P.S. the last one is just wet, same material as the 3rd one.

![images/32b790230215318f19daaa86335ce735.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/32b790230215318f19daaa86335ce735.jpeg)



**"Christoph E."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 30, 2016 00:19*

Nice idea. I was considering doing this sort of thing a while back, but gave up because I figured wooden plant markers will end up degrading. I did test on bamboo though & the engraves were really nice, but they actually didn't discolour (or go black).



In regards to fonts, for a starter I would consider enlarging the font. Bigger is always more legible (to a point).



Are you wishing to stick with a cursive style font for the "common name"? I would probably reverse it around so that it's a sans-serif font for the "common name" & a cursive font for the latin name. Although, I may even use an italicised serif font for the latin name (as it would be more legible than cursive).



edit: another thought would be to coat the entire piece with masking tape prior to engrave. Engrave what you need to, paint over the engraved areas, then remove the remaining tape when it's dry. You would end up being able to increase the contrast of your words/borders/etc a lot easier just through the choice of colour. If you plan on selling, this could also be a selling point (different colour labels/sets).


---
**Christoph E.** *June 30, 2016 00:46*

**+Yuusuf Sallahuddin** I considered selling these so degrading might not be that bad, if people really like it they'll get new ones. :P



Picked that "Schneidler Maxim" font because I sometimes use it at work and really like it, but it's obviously not made for engraving. I'll take your advice for the next one and try using a sans-serif font.



I'm a bit scared of masking tape as i just have an improvised "air assist" atm. Basically I'm right now using an air gun to get rid of the flames lol

But it's still a nice idea and I'm going to give it a shot. ^^



Any suggestions for another material? This is uhm.. plywood, can't really tell what sort just ripped it out of a broken cupboard.





P.S. all the old "garden women" laughed down on me when i put it in my garden. -.-


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 30, 2016 01:11*

I was looking into bamboo stirring woks & the likes to use. Plywood will definitely not last due to swelling with moisture & it will split the layers. My original air-assist was just a ball-inflation needle pumping air from a pathetically weak aquarium air-pump onto the cut point. You could probably dodgy up some solution with your air-gun & a bit of tubing to a ball inflation needle (if you have one handy) & zip-tie it onto the laser head (just bend the needle to point at the cut point).



Material wise, I can't really say, as by the looks of it you're cutting it with the laser as well as engraving. So it needs to be something cut-able, unless you are happy to cut with different tools & engrave after. Or if you could buy something precut (e.g. the wok stirrers I was considering for this exact same purpose).


---
*Imported from [Google+](https://plus.google.com/100193166302371572888/posts/NdXefUr2Fvq) &mdash; content and formatting may not be reliable*
