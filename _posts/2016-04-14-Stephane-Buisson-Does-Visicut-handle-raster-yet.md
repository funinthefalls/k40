---
layout: post
title: "Stephane Buisson Does Visicut handle raster yet?"
date: April 14, 2016 02:59
category: "Software"
author: "Anthony Bolgar"
---
**+Stephane Buisson** Does Visicut handle raster yet? And if it does can you vector cut and raster engrave in the same job?



Thanks in advance





**"Anthony Bolgar"**

---
---
**Ariel Yahni (UniKpty)** *April 14, 2016 05:11*

I think I told **+Peter van der Walt**​​ about this for LW﻿. It just makes the workflow much simpler


---
**Stephane Buisson** *April 14, 2016 09:08*

Visicut do raster yes, and I never try to do it at once (I do only cut for my automatas).

I can't see any reason it will not do it at once.

for that you will choose a custom mapping.

like cut red line and engrave the rest.

you can also place different files in the same job and execute at once.

**+Thomas Oster** is the best person to ask.



 It's a long time I want to try, but I am far away from my K40.


---
**Stephane Buisson** *April 14, 2016 09:13*

[https://hci.rwth-aachen.de/lasercutter](https://hci.rwth-aachen.de/lasercutter)


---
**Anthony Bolgar** *April 14, 2016 16:13*

Thanks for the info


---
**Stephane Buisson** *April 19, 2016 06:33*

**+Anthony Bolgar**  look at **+David Cook**  last post. he does run a smoothK40. not sure if it's visicut but in photo/video you can see raster and cut in same job.

[https://photos.google.com/share/AF1QipOMDejccZ5_vZyD0SaH9Jqi0ggbzEYvvmAxd7LNRQrMNaAsz5oSzyymcIwTeQpemA?key=WXU3dkdlWTZZMHZ2b2ZDdXlnOTR6N0Q1OG5vSV9n](https://photos.google.com/share/AF1QipOMDejccZ5_vZyD0SaH9Jqi0ggbzEYvvmAxd7LNRQrMNaAsz5oSzyymcIwTeQpemA?key=WXU3dkdlWTZZMHZ2b2ZDdXlnOTR6N0Q1OG5vSV9n)


---
**David Cook** *April 19, 2016 15:32*

**+Stephane Buisson** & **+Anthony Bolgar**  Actually...   I am now running a Ramps setup (smoothy clone was damaged) and using turnkey Tyranny Inkscape plugin and Repetier Host.  in that video linked I am doing a vector engrave and then a vector cut.



I checked out visicut but it did not seem all intuitive at first so I left it.  now Im going to try out LaserWeb for the control software.



However  using the setup above I was able to raster engrave and cut in the same program though.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/GSLNhz1yUta) &mdash; content and formatting may not be reliable*
