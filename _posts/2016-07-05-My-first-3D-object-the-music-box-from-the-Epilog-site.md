---
layout: post
title: "My first 3D object, the \"music box\" from the Epilog site"
date: July 05, 2016 04:52
category: "Object produced with laser"
author: "Tev Kaber"
---
My first 3D object, the "music box" from the Epilog site. 

Etch: 6mA 320mm/s, Cut 8mA 5mm/s, two passes (probably could have gone a little faster). 1/4" poplar. 



Didn't account for kerf, so the fit was slightly loose, but planned to glue it anyway. 



![images/70f8a8c05aa97c8b2f855c76383a6d54.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/70f8a8c05aa97c8b2f855c76383a6d54.jpeg)
![images/d83681b8277e3be4ab1e83c632b12301.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d83681b8277e3be4ab1e83c632b12301.jpeg)

**"Tev Kaber"**

---
---
**Scott Marshall** *July 05, 2016 05:36*

Nice work, that's sharp!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 05, 2016 08:23*

That looks really nice. A great box for storing little nick-nacks.


---
**Travel Rocket** *July 05, 2016 11:37*

so neat! did you use tape so that it will not soot?


---
**Victor Hurtado** *July 05, 2016 12:04*

Felicitaciones buen trabajo, una consulta donde puedo conseguir un paquete de vectores para laser para hacer este tipo de trabajos


---
**Jim Hatch** *July 05, 2016 12:21*

A handy trick for holding things together is to use pins. They're like brads but are smaller/thinner at 23ga. If you're in the U.S. you can get a pneumatic pin nailer for under $30 from Amazon or Harbor Freight. Pins come in a variety of lengths that are perfect for pinning the finger joints together. With some glue the joints will be as strong as the wood.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 05, 2016 13:10*

**+Jim Hatch** Looks like a handy tool to add to the collection. Thanks for sharing that tidbit.


---
**Tev Kaber** *July 05, 2016 20:24*

I sanded the surfaces to remove soot (220 grit).  Here's the project files btw: [https://www.epiloglaser.com/resources/sample-club/music-box-laser-cut.htm](https://www.epiloglaser.com/resources/sample-club/music-box-laser-cut.htm)


---
**Scott Marshall** *July 05, 2016 21:11*

I tried a bunch of different things, masking tape, several kinds, lotack (film carrier for decals), Kapton tape, etc. All are a PIA to remove especially if you do lettering or scrollwork. 

In the end I discovered a quick hit on the beltsander is the best way. 



A good air assist head and aggressive exhaust minimizes it, but there always some shading around the edges and inside letter islands. 



I notice on your box you left the dark edges, I think it adds a nice contrast.



I may just have to make a few of those, they would be nice gifts filled with a variety of goodies.(or a music box).



Here's a source for music box guts. (and other cool junk). shipping is kind of abusive, or I'd buy more stuff from them. 

[http://www.goldmine-elec-products.com/searchprods.asp](http://www.goldmine-elec-products.com/searchprods.asp)



Scott


---
**Tony Schelts** *October 18, 2016 22:38*

I couldn't download the files probably because I am in the UK. can you share them please




---
**Tev Kaber** *October 19, 2016 13:56*

Weird, dunno why epilog would block non-us visitors.

This is the Epilog page:  [https://www.epiloglaser.com/resources/sample-club/music-box-laser-cut.htm](https://www.epiloglaser.com/resources/sample-club/music-box-laser-cut.htm)



Here is the file, I put it up on Amazon S3:  [https://s3.amazonaws.com/tevk/laser/musicbox8.cdr](https://s3.amazonaws.com/tevk/laser/musicbox8.cdr)


---
**Tony Schelts** *October 19, 2016 22:57*

Thanks Tev got it now

Not from Epilog site, when I tried to download just came up with a page full of weird characters


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/CsW3ghR18s9) &mdash; content and formatting may not be reliable*
