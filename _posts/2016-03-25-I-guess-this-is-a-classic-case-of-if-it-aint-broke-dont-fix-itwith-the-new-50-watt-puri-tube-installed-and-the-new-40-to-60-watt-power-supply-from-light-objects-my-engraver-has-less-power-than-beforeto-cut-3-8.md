---
layout: post
title: "I guess this is a classic case of \"if it ain't broke don't fix it\"...with the new 50 watt puri tube installed and the new 40 to 60 watt power supply from light objects my engraver has less power than before....to cut 3/8"
date: March 25, 2016 16:10
category: "Discussion"
author: "Scott Thorne"
---
I guess this is a classic case of "if it ain't broke don't fix it"...with the new 50 watt puri tube installed and the new 40 to 60 watt power supply from light objects my engraver has less power than before....to cut 3/8 plywood I'm having to run it at 60% power...with the old stuff I could cut it at 50%...I should be shot...guess I'm putting the old power supply and tubby back in....what a waste of time and money! 





**"Scott Thorne"**

---
---
**Anthony Bolgar** *March 25, 2016 16:26*

Are you sure it is not an alignment issue?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 25, 2016 16:28*

That seems counter-productive, to have a more powerful tube that performs worse. I wonder, could it be something to do with the plywood? As it is layers/glue/whatever, it may vary.



I would consider trying on something else that is a more consistently the same material (e.g. acrylic).



If it turns out that it is less powerful (no matter what you're cutting), I'd be a bit annoyed at myself too. I wonder, other than using more power & performing worse, are there any other differences with the result of the 50w tube (e.g. thickness of cut, maybe longer lifespan, or other advantages)??


---
**Jim Hatch** *March 25, 2016 16:32*

Have you gotten it aligned (maybe a dumb question). Small mis-alignments can have big impacts on the delivered "power" of the beam. Did you change the mirrors or lens at the same time you were doing the tube upgrade? Those are the obvious suggestions but sometimes we forget the obvious when looking at tricky problems.


---
**Ashley M. Kirchner [Norym]** *March 25, 2016 16:39*

The real question is, where is the fault (if there is one.) Is it the tube? The PSU?


---
**Scott Thorne** *March 25, 2016 17:06*

**+Ashley M. Kirchner**....both are new...the alignment is perfect...just screwed up by thinking that bigger is better...not so in this case.


---
**Scott Thorne** *March 25, 2016 17:10*

**+Yuusuf Sallahuddin**..lol...lesson learned...bigger is not always better...I've tried cutting acrylic as well as plywood and the same problem arises...more power needed...so I'm going to put the original power supply back in and see what happens....either it will get better or worse...that will let me know the power of the tube at least...I know my settings with the original tube and power supply...takes about 20 minutes to change it...I'll keep you posted.


---
**Alex Krause** *March 25, 2016 17:12*

Did you put a volt/amp meter on the output of the power supply to see if there was any difference and verify that the PSU output was what it is rated at


---
**Alex Krause** *March 25, 2016 17:13*

I've heard that these tubes are often overdriven by the stock power 


---
**Scott Thorne** *March 25, 2016 17:33*

**+Alex Krause**....this power supply is supposed to be for 40 to 60 watt tube....the tube I have is a puri 50 watt....I'll know more when I hook up the old power supply.


---
**Scott Thorne** *March 25, 2016 18:45*

**+Peter van der Walt**...the power supply has no pot on it.. go figure...max mA's on the tube is 18...go figure...lol


---
**Alex Krause** *March 25, 2016 18:49*

**+Scott Thorne**​ what brand of PSU did you buy?


---
**Scott Thorne** *March 25, 2016 19:30*

**+Peter van der Walt**...I guess I have the exception from light objects...it's made by Jupiter electronics.


---
**Scott Thorne** *March 25, 2016 19:57*

**+Peter van der Walt**...you are right...it does have one...right between the connections...but it is turned all the way up already.


---
**Phillip Conroy** *March 25, 2016 20:55*

Talk to seller first,their may be a run in period,or some other issue you have not thought off. The pot maybe in its lowest postion [may work backward]


---
**Scott Thorne** *March 25, 2016 21:57*

**+Phillip Conroy**...I've tried adjusting the pot both ways and it makes no difference...I'll go recheck my work tomorrow...I feel defeated at this point in the day so I'm going to get a fresh start tomorrow....I think putting the old power supply in Tomorrow is my best bet, I've ordered a milliamp gauge but it won't be in for another 3 weeks.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/9BUf9FzPedZ) &mdash; content and formatting may not be reliable*
