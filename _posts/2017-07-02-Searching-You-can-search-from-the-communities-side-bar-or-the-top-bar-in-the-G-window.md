---
layout: post
title: "Searching You can search from the communities side bar or the top bar in the G+ window"
date: July 02, 2017 02:46
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40Searching



<b>Searching</b>

You can search from the communities side bar or the top bar in the G+ window.

Searching using hashtags can get you to a filtered subject matter quicker 

.. Try this in the  search at the top: #K40search

This leads you to a post on G+ searching 

Hint: Both hashtags and quotes surrounding search terms work as a means of filtering





**"Don Kleinschnitz Jr."**

---
---
**BEN 3D** *September 06, 2017 05:38*

Hi Don, I use google+ App from Apple IOS. Do you know a way to filter/shrank/minimise the search results just to the K40 Laser Cummunity? 


---
**Don Kleinschnitz Jr.** *September 06, 2017 14:18*

**+BEN 3D** try searching in this window.....



![images/d52353f1b476d8096783d9ba6a27e3de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d52353f1b476d8096783d9ba6a27e3de.jpeg)


---
**BEN 3D** *September 06, 2017 15:33*

Hi **+Don Kleinschnitz** 

it seems that the IOS does not support this.![images/bad9fafe27b2516b3e92bde30fd0fb70.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bad9fafe27b2516b3e92bde30fd0fb70.png)


---
**Don Kleinschnitz Jr.** *September 06, 2017 16:14*

**+BEN 3D** I would think it is your browser (safari) that is different?

Did you try chrome. Sorry but I am not a IOS user.



Anyone else able to help **+BEN 3D**?


---
**BEN 3D** *September 06, 2017 16:18*

No no, I use the google+ App from the IOS App Store. It seems that it have just one Searchfield for any staff.


---
**Don Kleinschnitz Jr.** *September 06, 2017 16:45*

**+BEN 3D** let me check on my droid,....




---
**Don Kleinschnitz Jr.** *September 07, 2017 01:45*

**+BEN 3D** You are right I could not find a way to search a specific community on my Droid's G+ either :(.


---
**BEN 3D** *September 07, 2017 03:38*

**+Don Kleinschnitz**  Good to know that the browser Version is a bit different as the App Version. I spend a couple of hours to find a way to search in the community at self. Next time if I have a new Issue, I will start a comparing to the browser Version first ;-).


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/93nxB7MKdmE) &mdash; content and formatting may not be reliable*
