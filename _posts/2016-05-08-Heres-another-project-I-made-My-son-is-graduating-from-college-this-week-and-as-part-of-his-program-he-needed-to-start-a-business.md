---
layout: post
title: "Here's another project I made. My son is graduating from college this week and as part of his program he needed to start a business"
date: May 08, 2016 21:10
category: "Object produced with laser"
author: "Jim Hatch"
---
Here's another project I made. My son is graduating from college this week and as part of his program he needed to start a business. So here's a business card box made of 1/4" Baltic Birch, living hinges for the banding around the sides, an inner box made with slotted parts and engraving for his school info. 



I also put Chicago's Professor Pollack's "financial tips on an index card" on the banding and base of the box. Figured it was just what a kid starting out in his career should have :-) He was one of only a dozen kids who was named a Syracuse Scholar so that got front & center billing on the box. 



It's all press fit - no glue would fit. Think I have to work on the tolerances :-) I did pin the banding in place in the back next to the dovetail joint using 23ga headless pins.



![images/65f5452c1c51e4edfd28447a00b151ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65f5452c1c51e4edfd28447a00b151ff.jpeg)
![images/a990a129b10a86fc00fd8828b1eed657.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a990a129b10a86fc00fd8828b1eed657.jpeg)
![images/ca9c3004e9a9ffdc5e7048a80d05f6d5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca9c3004e9a9ffdc5e7048a80d05f6d5.jpeg)
![images/642780701208909bc61c7b392bbabb7a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/642780701208909bc61c7b392bbabb7a.jpeg)
![images/cbe8c17b44a86ff1eafbe45851721f37.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cbe8c17b44a86ff1eafbe45851721f37.jpeg)

**"Jim Hatch"**

---
---
**Anthony Bolgar** *May 08, 2016 21:29*

You must be a proud father. Congrats on your sons achievement!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 21:31*

That's a really nice piece as well Jim. A lot of the detail in your post goes over my head (maybe region specific knowledge) but I love all the extra details you added to the casing (I'm sure they have some serious significance, just I don't understand it). I'm curious as to whether you design your living hinges yourself or use some kind of software/tool to assist with it? My attempts resulted in snapping hinges haha. On a side note, as your pieces are really nice, I would like to suggest for longevity of the internal linings to consider something like moccasin suede. Definitely has a longer lifespan than felt. Probably slightly higher price, but over here (in AU) I can get a sqft of suede for between AU$6-9 (although you have to buy the whole hide usually ~6-18 sqft).



edit: Congratulations to the son for graduation day!


---
**Jim Hatch** *May 08, 2016 21:34*

:-) doing it in 3yrs saved me a bundle. He did it because he didn't like the weather.



Found a girlfriend though who is on a 5yr architecture program so he's going to take the school up on a job offer. He's going to be a grad assistant and they're also paying for him to get his MBA.



Told him fumes will be his downfall - motorcycle gas fumes or a girl's perfume. Either makes a guy irrational :-D


---
**Jim Hatch** *May 08, 2016 21:38*

**+Yuusuf Sallahuddin**​ good idea on the moccasin suede. There's a leather store a couple of towns over. They always have great scrap & small piece piles - no need to buy the hide :-) I'll give that a look for the next one. Working on a large Settlers of Cataan board and custom box. That one looks like it will take 2 sheets of 18x24 plywood.


---
**Jim Hatch** *May 08, 2016 21:43*

Check out Snijlabs ([http://www.deferredprocrastination.co.uk/blog/2011/laser-cut-lattice-living-hinges/](http://www.deferredprocrastination.co.uk/blog/2011/laser-cut-lattice-living-hinges/)) for everything living hinge oriented you ever wanted to know :-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 21:44*

**+Jim Hatch** Something like that for gaming would definitely benefit from soft suede as it will get plenty of use (from cards/tokens/etc) & the suede will handle years of that treatment. You can use a "contact adhesive" to attach the suede. Check at the leather shop too regarding that. If you're handy with a needle & thread, consider adding stitch holes also & hand-stitch the edges on to give that professional look ;) Pretty easy process using leather needles (blunt points) & waxed thread. Should be able to get all that from the leather supplier too (reasonably cheap for the thread/needles too).



edit: Oh you can laser cut the suede too very easily. The 2mm stuff I have cuts beautifully. Can also be engraved (with low power like 4mA & high speeds like 500mm/s). Gives a nice subtle engrave.


---
**Jim Hatch** *May 08, 2016 21:47*

**+Yuusuf Sallahuddin**​ I use 3M 300 Spray Adhesive. It's a contact adhesive in a spray form. There are 2 other strengths available from 3M too. Not sure it's easier than a brush out version though.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 21:58*

**+Jim Hatch** Haven't tested that 3m stuff before, but if it works well let me know. I purchase the contact adhesive directly from my leather supplier. Can't find any specifics on the makeup of the adhesives I use for leather/suede, but there is either "cement" or "adhesive". The cement says "84% hydrocarbons" & the adhesive is "50% hydrocarbons".

[http://www.maclace.com.au/product.php?id=1514&list_page=1](http://www.maclace.com.au/product.php?id=1514&list_page=1)

[http://www.maclace.com.au/product.php?id=1409&list_page=1](http://www.maclace.com.au/product.php?id=1409&list_page=1)

Once bonded, you need a blade to get the pieces apart.



Thanks for the link for the living hinges. I think I saw this one once before & it all looked too technical to me haha. I saw some equations & ran away lol.


---
**Jim Hatch** *May 09, 2016 13:15*

**+Susan Moroney** Thanks. Credit goes to the boy for all of his hard work.


---
**Tony Sobczak** *May 10, 2016 21:55*

**+Jim Hatch**​ are you using a K40?  If so how did you do the edges out of a 12in piece. Nice job though.


---
**Jim Hatch** *May 10, 2016 23:33*

**+Tony Sobczak**​ I use both a K40 (modded to a 14x10 bed) and a 60W with an 18x24 bed. Small stuff I do on the K40, large stuff on the 60. This is 5x5.5x3in. 



Not sure what you mean by "edges out of a 12in piece".


---
**Tony Sobczak** *May 11, 2016 01:24*

The side is longer than 12in. I had to cut the sides in half to finish it on my K40.


---
**Jim Hatch** *May 11, 2016 01:46*

**+Tony Sobczak** Ah, you mean the banding. Yeah it is something like 20" long. The first one I tried as a "pass thru" from the back (split the band design into 3 sections where the living hinges were with little side cuts to clip the scrap off. Stuck a 4X24 from the rear (no exhaust in place) and the big U shaped slot behind the rail. It actually came out pretty good - stopped when it finished a section, pulled the excess off and then moved the whole thing forward to the front rail. 



But I had issues on some of the graphics so I had to recut it and I didn't have the patience to do it on the K40 so I ran it into the big 60W. Way easier. I thought about making it in 2 pieces but couldn't see a good place for the extra cut. Pinning it would have kept it in place but I couldn't fit the vertical line from a cut in the design anywhere that made sense.



The project made me really want a bigger machine. I'm seriously thinking about the LightObject 400x600 kit. I can strip out the guts of my K40 & Smoothie board and use it in the LO kit. What seemed to be plenty big enough when I was doing little boxes, purses, coasters and tchotchkes now seems small for the things I want to make. Graduating to bigger stuff :-)   



The other thing I learned from the two attempts was that I don't do regular polyurethane anymore. The rub/wipe on finishes like Danish Oil and Wipe-on Poly are way easier and come out much better. The first one I used spray poly and kept having to rub down with 000 3M pad and re-spray. It didn't seem to flow. The only trouble with the wipe-on stuff is that I can only find the wipe-on poly online (Amazon). At least the oil finishes like danish & tung I can get locally when I run out or don't have enough for  a project.


---
*Imported from [Google+](https://plus.google.com/114480049764906531874/posts/NmaAC4voN2L) &mdash; content and formatting may not be reliable*
