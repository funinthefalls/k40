---
layout: post
title: "Bradley Blodgett Unfortunately you have replies turned off on your post"
date: October 22, 2016 15:57
category: "Discussion"
author: "HalfNormal"
---
**+Bradley Blodgett** Unfortunately you have replies turned off on your post.

I just wanted to say that the warranty is only as good as the word of the vendor. I too have a "2 year warranty" but have never been able to collect on it.

Lots of luck getting your unit running again.





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *October 22, 2016 16:50*

**+Bradley Blodgett**​ if you don't get help from vendor come back here and we will see what we can do to help.


---
**Jeffrey Rodriguez** *October 22, 2016 16:51*

Is there any replacement parts site or anything of that nature for k40 ?. I am also have issues with my vendor my TUBE is Arcing to enclosure which from my understanding potential signs of bad Tube 


---
**Don Kleinschnitz Jr.** *October 22, 2016 19:40*

Depending on the part folks get them from various vendors. Light Object has many compatible parts.


---
**Stephane Buisson** *October 22, 2016 22:06*

if the tube is fuck, your meter will not move as the current pass through. I notice your pot is at the maximum witch in most case far over the recommended maximum current and could be a cause of death. well the tube is a consumable, and your machine could be working. to have an extra clue check the led on the Psu (inside) when you hit test fire to see if it turn on. not enought to be 100% if it's the tube, but the other way around could incriminate the PSU.

A tube could also die on the shelve if not used, gaz is not eternal.


---
**Bradley Blodgett** *October 29, 2016 00:22*

Thanks guys I just saw this today for some reason. I have never really moved my dial past 50%. It unfortunately looks like the tube as I replaced the power supply today with the same results. My seller recommended I find a US-based technician to service it, and I replied back today asking where to locate that type of service. At this point, if I have to replace the tube I'll be at what I paid for the whole unit with the PSU that I didn't need to buy. I guess it's the price you pay when you get something low end. I suppose $600 is still less expensive than most ready made offerings here in the states 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Rb7TQAPFYwL) &mdash; content and formatting may not be reliable*
