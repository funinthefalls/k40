---
layout: post
title: "Got stuck in thinking about the K40 again.."
date: April 15, 2017 19:32
category: "Modification"
author: "HP Persson"
---
Got stuck in thinking about the K40 again.. bad habit :P



Talked to a rep. from China about the PSU´s, most of them with the blue film on top are rated 1A on the 24V line and 0.5A on the 5V line.



That got me thinking, 1A to power two stepper motors + controller board.

I know the steppers are 0.9A mostly, but this is a bit vague for me, so i borrowed my PSU from my 3D printer, 24v 5A and the result was pretty cool, much much smoother movements and no rattling or hesitation on higher speeds. Less heat on the steppers too (how that happened i don´t know).



Maybe i found a new thing? powering the controller/steppers with external PSU?

Anyone else seen this? maybe **+Don Kleinschnitz** ? :)





**"HP Persson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 19:47*

It's come up a few times. Keep in mind that we're running the steppers at much less current - maybe around 400mA each? 

If you crank the driver current all the way up you may start seeing psu overload symptoms. 

Quite a few people have made the switch to add an external PSU. 


---
**HP Persson** *April 15, 2017 19:49*

**+Ray Kholodovsky** Thanks, i didn´t touch any settings on the stepper drivers, i just hooked up another PSU for the 24V, and still saw a big change :)


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 19:51*

Oh interesting. If you start moving fast enough people say that there is not enough current available and the motor can stutter. Well now you can have the ability to increase the current as much as you want (turn the pot 90 deg clockwise so the flat faces to the right) if you reach a limit. 


---
**Don Kleinschnitz Jr.** *April 15, 2017 20:46*

**+HP Persson** I did a power budget early and concluded that the stock PS would run a K40 but marginally. To make matters more doubtful the 24V is a simplistic offline switcher so I am not sure about the regulation or peak response.



The 5v and 12V inside the supply are linear regulators rated whose chips are rated at 1 amp respectively. The 5V is heat synced and the 12v is not.



The K40 was obviously designed for slow/moderate speed engraving and these supplies and controllers are probably sufficient for that. Most of us are likely running them outside that design profile.  



I didn't analyze it any further because I:

1. Did not want my HVPS in any way connected to my controller.

2. Power was marginal. Motors create peak loads and lots of inductive noise and its easy to imagine that a machine running on average at the supply's rating would droop when under peak loads.

3. Did not want potentially dirty power (24VDC) when regulated down to run my controller.



I use the HVPS +5 and 24Vdc for lighting and powering my lift table. I have always used a separate 24VDC and 12V power supply so I have nothing to compare with.



I am not surprised an external supply is necessary for the machine to run at peak performance. I have not seen any on my machine but as you all know I am not stressing it much.


---
**Jim Fong** *April 15, 2017 21:25*

I have to try this, have a extra 24volt supply.   The laser was stuttering a lot while burning a image at 150mm/sec.   it didn't loose steps so it might not be power supply related.   



Anyone having issues with the speed of the USB serial for feeding the gcode??  










---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 21:28*

Plenty of anecdotal stuff circling this question. Easiest way to find out is to put the gCode onto the sd card and run from there. (Either start it via the glcd or send a play command over terminal - look up Smoothie player) 


---
**Jim Fong** *April 16, 2017 03:34*

**+Ray Kholodovsky** I ran a test image at 500mm/sec. Gcode file size about 5megabyte.  When streaming with the USB serial, movement is stuttering and you can tell that it never reaches that speed.  Copy that same file over to the SD card and play it via the glcd panel, movement is much smoother and runs fast.  






---
**HP Persson** *April 16, 2017 08:33*

**+Jim Fong** This may be the USB buffer too on your computer. If you have a older computer or simpler model, the usb host controller controls all ports, instead of one controller per port or set of ports.

You can try this by removing every usb-product connected, remove usb-hubs and try again with the same job.



USB-hubs are awful for high speed USB2 stuff, even worse if they are not powered.



If you have a newer computer they often have one controller per port or pair of ports.


---
**HP Persson** *April 16, 2017 08:36*

I did one more test, replacing my old HT Master5 board again (the new Corel board version after M2 Nano).

But didn´t see as much difference as i did when moving the head with the Cohesion3D board :)



Do you think the underpowered 24v may be a contributing factor for the line of dead PSU´s we seen lately **+Don Kleinschnitz** ?


---
**Don Kleinschnitz Jr.** *April 16, 2017 12:46*

**+HP Persson** I had wondered about that but every dead supply I had users check and the ones I have here have good 24VDC. My current suspicions for most failed parts are input bridge rectifiers and flyback transformers. 


---
**Jim Fong** *April 16, 2017 13:47*

**+HP Persson** it's a older Dell desktop core2duo 3ghz Win7. I tried different USB ports, all the same stuttering.  I reinstalled windows recently so pretty much a fresh install.  The only USB product connected is the mouse and keyboard. 



Also tried a newish Dell i5 laptop, same sluggish movement. 



Same thing with a HP i7 laptop, maybe a little faster



All Win7 with USB serial setting at 250kbaud. Latest version of laserweb4 binary.  1.1 smoothieboard driver on the laptops. 1.0 on the desktop. 



Running the same gcode from the SD card, the axis speed seems to be almost twice as fast with much smoother motion. 


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/EfxJQcqwo64) &mdash; content and formatting may not be reliable*
