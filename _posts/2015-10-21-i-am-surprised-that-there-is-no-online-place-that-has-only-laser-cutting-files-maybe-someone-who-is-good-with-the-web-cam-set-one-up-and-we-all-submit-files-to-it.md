---
layout: post
title: "i am surprised that there is no online place that has only laser cutting files ,maybe someone who is good with the web cam set one up and we all submit files to it"
date: October 21, 2015 22:26
category: "Discussion"
author: "Phillip Conroy"
---
i am surprised that there is no online place that has only laser cutting files ,maybe someone who is good with the web cam set one up and we all submit files to it. i mainly do scrapbooking file cutouts





**"Phillip Conroy"**

---
---
**ThantiK** *October 21, 2015 22:48*

thingiverse has a lot of laser projects, and it's easy to find ideas on etsy as well.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 22, 2015 01:25*

Maybe we could as a group set up a site dedicated to this. (I am currently studying Web Dev).


---
**Stephane Buisson** *October 22, 2015 10:52*

Excelent idea. I have to say the success of a design repository website is linked with the file status in term of right.

for 3D printing I do like youmagine for that reason. when you add a file, you can choose a licence to go with.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 23, 2015 07:38*

**+Stephane Buisson** I was thinking the exact same thing if we created a site for it would need to allow people to choose to sell their design (I assume for cheap price like $0.50-1.00 each or something) & attribute different licenses with it or give it away for free (if they so choose).


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/Jkv3mCnhyft) &mdash; content and formatting may not be reliable*
