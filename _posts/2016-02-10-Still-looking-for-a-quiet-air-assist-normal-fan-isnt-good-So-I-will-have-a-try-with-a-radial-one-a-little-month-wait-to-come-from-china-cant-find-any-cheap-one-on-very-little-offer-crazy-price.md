---
layout: post
title: "Still looking for a quiet air assist, normal fan isn't good, So I will have a try with a radial one, (a little month wait to come from china) can't find any cheap one on , very little offer & crazy price"
date: February 10, 2016 18:02
category: "Modification"
author: "Stephane Buisson"
---
Still looking for a quiet air assist, normal fan isn't good, So I will have a try with a radial one, (a little month wait to come from china)

can't find any cheap one on [Ebay.co.uk](http://Ebay.co.uk) , very little offer & crazy price.

Hopefully I'll got enough pressure out of it to blow into 5mm ID tube. Airflow:28.9CFM

It would be lovely to have it built in the K40.





**"Stephane Buisson"**

---
---
**Thor Johnson** *February 10, 2016 18:17*

With the 5mm tube, you need a lot of pressure to move a lot of air... I don't think you can get a blower to work if your nozzle is < 12mm.  Most people Adee are using large (60lpm+)  aquarium pumps that are supposedly pretty quiet...


---
**Scott Thorne** *February 10, 2016 19:11*

The aquarium pump that came with mine is plenty of air and very quiet....better than I thought it was going to be.


---
**Phillip Conroy** *February 10, 2016 21:46*

i use a full size air compressor with pressure reg and water trap ,just as important is the exhust fan,my second upgtade uses a 8 inch monster fanhttp://[www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT) .with stock fan had to wait 5 seconds for the smoke to clear when cutting mdf,this monster is 10 times better also threw away crap stock tubing and replaced it with aluminion tube from house exhust fan .because of all the mdf i have cut my tube is covered in mdf dust and can not even see into the tube.i do a lot of mdf cutting 2 or 3 hours a day


---
**Stephane Buisson** *February 10, 2016 23:45*

thank **+Peter van der Walt** I'll try the 15l/mn (32KPA) from your second link. should be about the same pressure than the 120mm radial fan (28.9CFM=38KPA) minus a bigger loss with reduction. test in a month time.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/TmcYvWaFU9m) &mdash; content and formatting may not be reliable*
