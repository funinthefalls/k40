---
layout: post
title: "Hello all, This is my first post"
date: September 19, 2016 18:49
category: "Discussion"
author: "Cloudbase Engineering"
---
Hello all,



This is my first post.  I am considering buying either a K40 or a blue laser diode system to engrave a logo in black anodized aluminum.  My concerns with the K40 are all the bad press from the earlier versions having dangerous wiring as well as other issues.  I am curious if the newer version I see with the digital touch pad and E stop (not DSP) are any better.  My parts are roughly 1.5" square and curious if there is enough z height to get my part under the laser and in focus.  Would I need to add the LO Motor Z option?  I would prefer to not add a DSP to this and run it stock as my needs are simple.  However, I would also see myself engraving pics and line drawings on leather as well.  Can I get by with a stock K40 to do this.



Anyone also have any experience with a Blue diode laser machine that has used it to etch logos in black anodize.  I know it can be done with as little as 1W but curious if the CO2 laser looks better.



My other option is buy the 50w laser on ebay, but I would need to make more room for one that large.  However the plus is its already a DSP machine and can take my CAD dxf files.



I am open to any and all advice.



Marc





**"Cloudbase Engineering"**

---
---
**Scott Marshall** *September 20, 2016 01:37*

The K40 are dangerous and wired terrible to this day. 



BUT... with a little fixing up and a LOT of reading and learning....



You can make one into a darned nice laser that will eat alive any diode laser for any reasonable price. They excel at burning anodize from aluminum, and acrylic and wood up to about 10mm in 1 pass (if you've got a good tube and aren't afraid to push on it hard from time to time). The K40 really is only a 30w laser, but does amazing things, most of them on quite low power settings. See some of the engravings members have produced..

As far as  the safety goes, consider it a table saw that can blind you. Take sensible precautions and treat it with the respect due a serious power tool. It is indeed. I think many discount the dangers because theres no screaming blade waiting to lop off a row of fingers. It's every bit as dangerous as that, but so is ANY tool that will do the things it will do.



Inspect all the wiring, correct any loose or disconnected grounds, tighten all fasteners, maybe adding some blue loctite.

You can expect to remove the stock bed and exhaust ductwork, and install your own bed. Nothing fancy is generally required, just some expaneded metal or a nailboard, especially after you add the adjustable focus air assist (Saite Cutter Ebay). Fresh mirrors and lens should go with head, and a pump will be needed for the air assist if you don't have shop air.

The big thing is the learning, and most of it is just getting in there and having at it. The software is Kludgy, pirated, obsolete, unsupported and half Chinese, but it works OK. To Upgrade to something better generally requires spending some money and/or doing a controller swap. There's a guy who sells plug ins for the non-electrically inclined.



Read thru the archives here, it's not really easy to navigate, but it's all there.



When you're 'done' you'll probably have $600-$1000 in it, but will have a new hobby, skill set, and won't have any doubts on where to go next. It's some work, but if you don't mind some work, it's great fun.



I'll be buying a "box stock" virgin K40 in the next few weeks and will be doing (or attempting) to demonstrate how to turn it from what I call a "kit - assembled for shipping" into a gem and slick safe laser. When I'm done, I'll be selling it. (this may take a year or more, so you 'll have to follow along with your own)



I hope you decide to accept the mission, and I can promise you, you HAVE already done the hard part. You've found this group.

These guys WILL NOT ever laugh at or belittle you for any question, and they will go out of their way to help you with any problem, small or large.



Hope you join the fun.



Scott




---
**Cloudbase Engineering** *September 20, 2016 05:47*

Hey Scott,



Thanks for the great reply.  I actually do have some experience with laser cutters.  I use to have one at my last job (Morntech 80w 960) and it worked great.  I made a bunch of stuff for work as well as for myself and wife who is a school teacher.  I'm torn with buy the K40 which is likely too small, but just fine for my logo etching or, make room for the larger 50w machine which already has the DSP and the same software I am use to using on the Morn laser.  I know the larger machine would come in handy, but its a bit more of an investment in both money and space...


---
**Scott Marshall** *September 20, 2016 08:06*

The familiarity with the software and a better initial build quality DO make the larger unit attractive.

 You do indeed have a dilemma, but not a terrible one to have. Jim Hatch here has both I believe and may be able to add some specifics. Not owning the 50W model, I can only go by what I've seen, build is a bit better, they actually have basics like low flow shutdown and that kind of thing. From what I know of the laser and optics, they arejust a bigger version of the K40, equally de/over rated (depending on your point of view) The K40 says 40watt, produces 30, tube effeciency about 9%, I'd guess the 50 watt version performs similarly, probably producing 40w actual. Mirrors and head likely will need replacement, and the supplied air and water pumps are likely marginal (as well as exhaust) I think the bigger and slightly more robust motion control and better controller are where the extra $700 goes. Many of those are basic gcode "run from the card" controllers and don't actually do much. You still end up creating your Gcode with CAMBAM or MACH2 like us machine guys are more used to. I AM getting spoiled by the draw it and cut it function of even the iffy software the lasers come with. It makes 1 off or 'quickie' projects practical.



I'm not aware of any forum quite like this one for the bigger stuff. CNCzone has spotty laser coverage, and much of the info there is quite old and outdated.



I'd be willing to be if you were to look at us here in 2 or 3 years we will have grown into THE hobby  laser place on the web. I believe more and more people will discover these machines and eventually move to the bigger ones.

We occasionally have visitors with the 50+ watt "K50" machines, but they don't stay around, I'm not sure why. They most certainly are welcome. It maybe the specifics being different that they can't get what they need here, but it seems we have quite a few members with larger machines.



I think it just takes time, and the format here isn't helpful toward arranging previous topics for easy access as many other forum engines are. I think Google by now has received the message, (I personally have written them regarding the problem, but I'm pretty sure need more than the handful of us that are here to get their attention.)

I'd imagine the Pokemon group with 20k members gets heard, but us laserheads don't have the numbers ... yet.



If yo go for the 50w machine, maybe we'll have to form up a sub group here. I'd be in on that. New lasers to conquer....



(and an excuse for a new toy)





Scott


---
**Cloudbase Engineering** *September 20, 2016 17:10*

Thanks for the great post Scott.  I will be making a decision in the next week or so.  The issue with the 50w is logistics.  It needs to ship to an address where someone can sign for it so would need ti have it go to my parents who are retired or possibly a freight forwarder...  The ultimate location is an EAA airplane hanger at KWHP airport and having it dropped off there would be ideal but unsure if it could be coordinated to work.  If it goes to my parents I need a way to get it on a uhual trailer and off since the crate is so big.  If I convince myself I need 60w then i get enough bed size to cut a wing rib but I dont have much room in the hangar



Marc


---
*Imported from [Google+](https://plus.google.com/115143003847620794726/posts/a9NLUkLPc8B) &mdash; content and formatting may not be reliable*
