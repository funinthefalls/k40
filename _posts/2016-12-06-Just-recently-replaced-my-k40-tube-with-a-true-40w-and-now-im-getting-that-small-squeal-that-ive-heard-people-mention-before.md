---
layout: post
title: "Just recently replaced my k40 tube with a true 40w and now im getting that small squeal that i've heard people mention before"
date: December 06, 2016 00:59
category: "Discussion"
author: "Tim barker"
---
Just recently replaced my k40 tube with a true 40w and now im getting that small squeal that i've heard people mention before. It sounds as if its coming from the power supply, but other than the sound, it seems to still be running fine. Anybody have some thoughts on if i should be worried or not? Thanks in advance for any insight.





**"Tim barker"**

---
---
**greg greene** *December 06, 2016 01:11*

you may need a new PSU - sounds like this one is being over taxed by the higher draw of the new tube


---
**Tim barker** *December 06, 2016 01:19*

i was afraid of that. was hoping that maybe the mA's could be adjusted on the PSU (if the sound was an issue at all, that is)


---
**Tim barker** *December 06, 2016 01:20*

my power supply does say 40w on it though so technically it should be just right for the new tube. or am i wrong for assuming that?


---
**Ashley M. Kirchner [Norym]** *December 06, 2016 01:26*

Just because your car engine can spin at 9000 rpm does not mean you will constantly be driving it like that.



Get it? :)


---
**Tim barker** *December 06, 2016 01:31*

Are you saying that most power supply's are rated at a higher wattage than the tube you want to run?


---
**Tim barker** *December 06, 2016 01:31*

Or should be rated higher...


---
**Ashley M. Kirchner [Norym]** *December 06, 2016 01:38*

It should be. I wouldn't want to run my PSU at full power to sustain the tube. I'm not saying you can't do that, but just like with running the tube at full power, you will shorten its life dramatically, the same will happen with the PSU.


---
**Tim barker** *December 06, 2016 01:48*

Gotcha. Yeah that makes sense. I should also add that it only makes the noise when on occasion when cutting. Engraving seems to be fine. I'll look into new power supplies. I guess I assumed that 40w tube and supply would be made to go hand in hand. Now that I'm thinking about it, could a lower watt tube be overdriven by a higher watt power supply?


---
**Ashley M. Kirchner [Norym]** *December 06, 2016 02:00*

Yes, but not recommended.


---
**Tim barker** *December 06, 2016 02:02*

By overdriven I meant to say driven too hard. Like in a bad way


---
**greg greene** *December 06, 2016 02:20*

The PSU supplies the power the tube demands - upto the limit of the PSU, so no it wouldn't overdrive the tube.  I remember Light Objects selling upgraded PSU units to use when you use the full 40 Watt tube - you might check them out - the stock ones are somewhat marginal - even for the 32 Watt tubes


---
**Ashley M. Kirchner [Norym]** *December 06, 2016 02:24*

**+greg greene**, saying a PSU will not overdrive a tube is an invalid statement. A PSU can easily overdrive a tube, if the tube is less than the PSU. Take a 40W PSU, and a 35W tube, you can easily push the tube to 40W (and ultimately destroy it in the process.)


---
**Ashley M. Kirchner [Norym]** *December 06, 2016 02:25*

And plenty so called "40W" Chinese machines actually have a 35W tube in them with a PSU that can drive 40W ...


---
**Tim barker** *December 06, 2016 02:41*

So im having a hard time grasping this then. A 40w PSU isn't quite enough for  40w tube, but if you have a larger power supply than tube it can destroy it as well?


---
**Ashley M. Kirchner [Norym]** *December 06, 2016 02:47*

I didn't say an equivalent PSU isn't enough. I  said, you wouldn't want to drive it at 100% to sustain the tube.



A larger PSU is better, and since you control its output, you can lower it to match the tube. and in the process extend the PSU's longevity.



You can put a V12 in a Mini Cooper, doesn't mean it will survive it. Similarly, you can put a V4 in a large dump truck, doesn't mean the engine will survive trying to drive that thing loaded.


---
**Ashley M. Kirchner [Norym]** *December 06, 2016 02:57*

Basically, you can put a 100W PSU in the machine and drive it at 33% for a small 35W tube. No issues. Just don't run it any higher. 


---
**Tim barker** *December 06, 2016 02:58*

Ah! Gotcha. Thank you


---
**Don Kleinschnitz Jr.** *December 06, 2016 04:05*

What does the current meter read?

What coolant are you using?


---
**Tim barker** *December 06, 2016 04:08*

Current meter reads at normal powers. It has a really small bump in power when the sound happens, but nothing of any significance. I did notice now that the color of the beam inside the tube intensifies when the sound happens as well


---
**Claudio Prezzi** *December 06, 2016 08:01*

The sound could also be a high voltage spark between a tube end and the case. You should check if you have enough distance from the tube to the case on all sides. Check if you mounted the new tube too close to the case on one side, or the new tube is longer than the old.


---
**Don Kleinschnitz Jr.** *December 06, 2016 14:07*

If the current meter is reading the correct value of current then the tube is likely being driven ok and the problems is not the supply. 



It could be an arc outside (as **+Claudio Prezzi**) mentioned or also inside the tube. 



Does this happen only during a job or also intermittently if you hold the test button? Does the current bump up or drop lower?



Might be related:

I recently worked with someone on an unusual LPS problem where there was an internal arc and current leak from the outputs end epoxy seal through the water bucket to ground. He was blowing LPS every few months. Turned out he was using standard anti-feeze which is more conductive and a corona track had formed in the output seal. He replaced the anti-freeze with distilled water and from what I had heard fixed it? Very weird. 

I have no way of confirming cause and effect in this weird case but I can verify that normal antifreeze is more conductive than RV antifreeze and is not recommended. 

I don't know if the corona leak was caused by a defect in the output seal or if the leak was caused by using standard antifreeze. I have also heard reports of folks feeling a tickle from their water bucket and wondered if the two were related. Is there current flow through our reservoirs in normal operation?


---
**greg greene** *December 06, 2016 14:15*

Don, that problem has been reported, I've noticed a product on EBay [http://www.ebay.com/itm/322204363935?_trksid=p2060353.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/322204363935?_trksid=p2060353.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT) that might help - although I don't know if it connects any differently to the HV on the tube. If your machine is in a spot where a direct to ground rod connection can be made - it's a good idea.


---
**Tim barker** *December 06, 2016 14:22*

Cloudio, now that you mention it my new tube is mighty close to my first mirror. I had my head down there looking at the end of the tube trying to identify an issue, but I was mainly focused on the bubble in the end at that time. I'll take another look when I get back to the laser.


---
**Tim barker** *December 06, 2016 14:30*

Don, that was my initial thought, but the sound is coming directly from the power supply. Much louder when I have my ears there vs the tube. 



It does happen when I press the test button as well. Its funny...when the control knob is turned up passed half way, the sound will reflect on the meter with a small (almost unnoticeable) DROP in power. If its below half way it reads as a small JUMP in power.

Really strange.



I have checked with my hand in the water and haven't noticed any current t in there yet


---
**Don Kleinschnitz Jr.** *December 06, 2016 14:37*

**+Tim barker** pretty sure that if you are having an arc you will see it. 



Turn out the lights and see if you can see and arc. If not;



Turn out the lights, setup and take a video of the tubes output end in operation. While operating with the rear cover open position  yourself in front of the machine with safety glasses, start the video and push and hold the test button hopefully until the squeal starts.  



In the video look carefully at the end of the tube and see if you can detect an arc inside the tube.



Hint I have used a selfie stick to take videos while out of the way of the laser compartment,.



Please be careful and wear glasses. 




---
**Tim barker** *December 06, 2016 14:38*

Will do, Don. I'll get a video here as soon as I am back home to my laser.


---
**Don Kleinschnitz Jr.** *December 06, 2016 14:50*

**+Tim barker** some thoughts;



If there is a HV arc it acts somewhat like a short and the supply might make the noise as the load changes, not the tube.



Potential explanation of the strange pot reaction;



If the supply is running nearer capacity (pot turned past 1/2) when the short happens the supply is saturated by the additional load and the current drops.



If the supply is running below capacity when the short happens the supply can provide more current and the meter jumps up.



I am guessing ...... HV power is a strange animal.... and BTW dangerous!






---
**greg greene** *December 06, 2016 15:03*

That makes sense Don, watts is calculated as voltage times amperage so depending on which is closest tot he max available - one or the other has to change.


---
*Imported from [Google+](https://plus.google.com/113646430521733366133/posts/CHbULgs9pVu) &mdash; content and formatting may not be reliable*
