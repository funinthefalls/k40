---
layout: post
title: "I think I already know the answer to this question is \"It does not matter\", but ..."
date: March 20, 2017 17:52
category: "Original software and hardware issues"
author: "timb12957"
---
I think I already know the answer to this question is "It does not matter", but ... Does the direction of water flow matter as to which is input and which is output?





**"timb12957"**

---
---
**Don Kleinschnitz Jr.** *March 20, 2017 18:03*

I don't see how but then again I could not see how the conductivity of the coolant mattered !

I have heard some say it does .....


---
**Ashley M. Kirchner [Norym]** *March 20, 2017 20:32*

I'd make the HV end the "cold" end. But who knows if it really matters, dunno.


---
**Chris Hurley** *March 20, 2017 20:55*

<b>wild and unfounded thoughts</b> maybe it matters due to the temp difference and stress from the differential?  


---
**Abe Fouhy** *March 21, 2017 00:42*

In water to water heat exchangers it very important, they need to be cross flow, ie flow from the left exchanger plates flows into flow going right on the opposite excganger.  For this application not sure which side is hotter the cathode or anode?


---
**Don Kleinschnitz Jr.** *March 21, 2017 12:11*

**+Abe Fouhy** mmmm, I think the cathode is hotter but that is a good question. An item to measure I guess :).


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/D1CNyNm1m8D) &mdash; content and formatting may not be reliable*
