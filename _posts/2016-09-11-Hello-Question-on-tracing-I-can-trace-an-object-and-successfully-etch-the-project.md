---
layout: post
title: "Hello. Question on tracing. I can trace an object and successfully etch the project"
date: September 11, 2016 03:13
category: "Discussion"
author: "Derrick Armfield"
---
Hello.  

Question on tracing.    I can trace an object and successfully etch the project.   The problem is when I try to CUT along the outside edge.    Have tried to Fill with Color (black) but the entire rectangle turns black and the image is covered also.  



If I try to CUT the image it tries to cut around everything that was etched when all I want is the outside. 



Any advice?   





**"Derrick Armfield"**

---
---
**Ariel Yahni (UniKpty)** *September 11, 2016 03:21*

On that square., fill must the 0 or nome and stroke must be 0.025mm 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 10:41*

Sounds to me like you have the engrave object still selected when you are trying to cut... When I was still using CorelDraw/Laser, I would place my engrave on one spot on the canvas & place the cut sections on other spots. Then I could just left click on whichever task I wanted to do (e.g. the engrave object) & then click the Engrave or Cut button in the toolbar.



Also, another thing you will want to read about (here in previous posts) is about aligning engrave & cut jobs. There are issues with that which require you to create a reference point. Personally I would create a no-fill, no-border rectangle around the engrave object, position it where it belongs & also create a matching sized no-fill, no-border rectangle around my cut components (aligned where they belong also). The no-fill, no-border rectangle will register for the purposes of alignment, but because it has no-fill & no-border it will not cut or engrave.


---
**Derrick Armfield** *September 11, 2016 16:30*

Thanks for the reply.   Will do a search on Aligning Engrave and Cut jobs.   Was having a big issue with this last night as the outside  border was shifting between Etch and Cut.


---
**Randy Draves** *September 11, 2016 21:35*

I've been following this instructable and it's been working really good.

[instructables.com - K40 Laser Cutter: How to Cut and Engrave in One Job](http://www.instructables.com/id/How-to-Cut-and-Engrave-Using-a-K40-Laser-Cutter/?ALLSTEPS)


---
**Derrick Armfield** *September 12, 2016 04:13*

Followed the link you provided and it finally works!


---
*Imported from [Google+](https://plus.google.com/116858736662206054147/posts/WDpYCoCkbox) &mdash; content and formatting may not be reliable*
