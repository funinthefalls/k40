---
layout: post
title: "Not actually a big mod, but a small one that helps ;) Noticed how the USB on my laptop was in a position to easy break or snap, so i bought a 5 usb hub and a cable"
date: March 16, 2016 09:28
category: "Modification"
author: "HP Persson"
---
Not actually a big mod, but a small one that helps ;)

Noticed how the USB on my laptop was in a position to easy break or snap, so i bought a €5 usb hub and a cable. And now it never get lost.



I´ll make a bracket to put the cable into and move the board to keep all cables inside completely as a upgraded mod :)

![images/dc47f47ef1e3e8497f49dd4523fc883e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc47f47ef1e3e8497f49dd4523fc883e.jpeg)



**"HP Persson"**

---
---
**ED Carty** *March 16, 2016 13:15*

Looks good


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/KX62XjgidRZ) &mdash; content and formatting may not be reliable*
