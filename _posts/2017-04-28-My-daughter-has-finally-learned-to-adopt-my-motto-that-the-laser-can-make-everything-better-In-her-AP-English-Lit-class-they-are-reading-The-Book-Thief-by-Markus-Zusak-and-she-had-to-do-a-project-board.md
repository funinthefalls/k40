---
layout: post
title: "My daughter has finally learned to adopt my motto that \"the laser can make everything better\" :) In her AP English Lit class they are reading \"The Book Thief\" by Markus Zusak and she had to do a project board"
date: April 28, 2017 11:45
category: "Object produced with laser"
author: "Ned Hill"
---
My daughter has finally learned to adopt my motto that "the laser can make everything better" :)  In her AP English Lit class they are reading "The Book Thief" by  Markus Zusak and she had to do a project board.  She had me cut the mirror ornaments and the title.  :D



![images/bc6a6f829c8dd8feff75a20955153800.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bc6a6f829c8dd8feff75a20955153800.png)
![images/34d58146edc5e3de05d4b9fd3482adc9.png](https://gitlab.com/funinthefalls/k40/raw/master/images/34d58146edc5e3de05d4b9fd3482adc9.png)
![images/c4ec067ce20bb4e1e6d94857c699753a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c4ec067ce20bb4e1e6d94857c699753a.png)

**"Ned Hill"**

---
---
**HalfNormal** *April 28, 2017 13:02*

Pretty soon you will need to purchase one for her so you can still get your projects done!


---
**Cesar Tolentino** *April 28, 2017 14:03*

Or diy a bigger laser


---
**Ned Hill** *April 28, 2017 14:05*

Lol yeah I'm currently debating about getting a bigger laser or a cnc router next.


---
**Cesar Tolentino** *April 28, 2017 14:11*

Bigger laser.  Why? Because you can cut templates out of 1/4 ply and then use hand router to cut thicker materials. Just finished my diy CNC yesterday and now I'm thinking what to do with it. Lol. But if it's a diy laser I build, then there is so many projects I can already think of.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/JhnDW9NDMoa) &mdash; content and formatting may not be reliable*
