---
layout: post
title: "This is my water cooling. A cheap \"tupper\" style 7 liter box with distilled water"
date: May 24, 2017 17:08
category: "Modification"
author: "Claudio Prezzi"
---
This is my water cooling. A cheap "tupper" style 7 liter box with distilled water.

![images/f06569b33ba2cbd61bc8c88d3a7a386e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f06569b33ba2cbd61bc8c88d3a7a386e.jpeg)



**"Claudio Prezzi"**

---
---
**Ariel Yahni (UniKpty)** *May 24, 2017 17:55*

I like this idea very much


---
**Anthony Bolgar** *May 24, 2017 19:27*

Claudio, I have a plug and play drop in replacement Grbl based board on its way to me. After I have evaluated it , would you like me to send it on to you to check out? Todd mentioned that you might be interested, with all the work you put into adding GRBL.


---
**Ariel Yahni (UniKpty)** *May 24, 2017 19:48*

**+Anthony Bolgar**​ Is this the one from Paul?


---
**Anthony Bolgar** *May 24, 2017 19:49*

Yup, he sent it off to me on Monday, at no charge. Nice guy!


---
**Phillip Conroy** *May 24, 2017 23:02*

Only  problem is light can start the growth of algae, I would paint most of tank leaving a viewing slot so you can still see water level


---
**Claudio Prezzi** *May 24, 2017 23:34*

**+Anthony Bolgar**​ Could you send me a link with some more info about the board?


---
**Claudio Prezzi** *May 24, 2017 23:38*

**+Phillip Conroy**​​ You are right. That's why I use distilled water and a box with a closed lid, so the water doesn't get contaminated. Blocking light could help further.


---
**Anthony Bolgar** *May 24, 2017 23:59*

Sure thing **+Claudio Prezzi**


---
**Anthony Bolgar** *May 25, 2017 06:55*

Here is the link you asked for **+Claudio Prezzi**  [http://awesome.tech/cheap-chinese-k40-ebay-laser/](http://awesome.tech/cheap-chinese-k40-ebay-laser/)


---
**Claudio Prezzi** *May 25, 2017 15:36*

**+Anthony Bolgar**​ That looks interesting. Would be nice if I could test it to see if I can optimize LW4 for it.


---
**Anthony Bolgar** *May 25, 2017 16:13*

OK, once I have evaluated it for a while, I will get it sent off to you.


---
**Claudio Prezzi** *May 25, 2017 18:22*

**+Anthony Bolgar**​ Cool, thanks!


---
*Imported from [Google+](https://plus.google.com/+ClaudioPrezzi/posts/DizLJyeuebM) &mdash; content and formatting may not be reliable*
