---
layout: post
title: "Battling with your softwares ? I start to look at the alternatives last year"
date: January 19, 2016 12:41
category: "Software"
author: "Stephane Buisson"
---
Battling with your softwares ? I start to look at the alternatives last year.

my today workflow is explained in this video. (SketchUp-InkScape-Visicut)

Thank you again **+Thomas Oster** you are a Star.

 I use a modified K40 with Smoothieboard 4XC as explain in the other video. 
{% include youtubePlayer.html id="0r5sdS8zqY0" %}
[https://www.youtube.com/watch?v=0r5sdS8zqY0](https://www.youtube.com/watch?v=0r5sdS8zqY0)



this will not work with unmodified K40 (stock), you need to mod for a new board.





**"Stephane Buisson"**

---
---
**Stephane Buisson** *January 19, 2016 12:43*

I could not see any reason, the Smoothiebrainz (**+Peter van der Walt** ) to not work with Visicut too (and with Laserweb).

time will tell


---
**Stephane Buisson** *January 19, 2016 14:14*

**+Peter van der Walt** , **+Thomas Oster**  said it should work with usb now and say how to, honestly I haven't try, Ethernet working fine with me.


---
**Sylvain Maubleu** *February 03, 2016 10:24*

Hi I don't want to polute the community, so I think this post is appropriate.

I use a CAD software to design part to be cut. I have a K40 laser machine and CorelLaser and Laserdraw to do the cutting. I do not have a Smoothie board just the serial USB interface.

The DXF files exported from CAD are correctely imprted in CorelLaser, but, every non closed curves are cut on both side of the curve. 

On the other hand, all closed curves (ellipse, circles) or surfaces are cute only once.



All my curves thikness is "Airline", I thought it should have done the job but no.



Do you guies know a mean to have the open curves cut only once? 




---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/daKS1eVBjg3) &mdash; content and formatting may not be reliable*
