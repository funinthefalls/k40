---
layout: post
title: "Hi guys, i started asking around here this spring and I finally joined the K40 owners club this Friday"
date: August 21, 2016 19:04
category: "Hardware and Laser settings"
author: "Purple Orange"
---
Hi guys, i started asking around here this spring and I finally joined the K40 owners club this Friday. However when I unboxed it today, it was not in working order as I have seen many of yours aswel when delivered. Here are some photos of the problems in the bed-area. What do to do correct this, how much of an effort? Where to start (I have already stared a case via e-bay ). 



I would gladly recieve some feed back from yoe experienced users in these areas.



* Bed bent

* Y-axis rod does not line up square

* Y-axis rod has a deep scratch close to the home-end stop

* The 3rd mirror& lens holder is loose and one for the bolts has snapped off.



Please advice!



Cheers

/Micael



![images/82174ef2cedccb211703e41d2bc5a407.png](https://gitlab.com/funinthefalls/k40/raw/master/images/82174ef2cedccb211703e41d2bc5a407.png)
![images/bfcd4f7d53d43a9d79f1e734f2cfef41.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bfcd4f7d53d43a9d79f1e734f2cfef41.png)
![images/53992bcdd65578dafe0d7d6e472352c7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/53992bcdd65578dafe0d7d6e472352c7.png)
![images/74dc6266689b3491f5442938b2a93443.png](https://gitlab.com/funinthefalls/k40/raw/master/images/74dc6266689b3491f5442938b2a93443.png)
![images/0157c82d2c62bb55ed1e3c7af5601f7e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0157c82d2c62bb55ed1e3c7af5601f7e.png)

**"Purple Orange"**

---
---
**Derek Schuetz** *August 21, 2016 19:29*

Demand a refund 


---
**Jim Hatch** *August 21, 2016 20:00*

The bed you're likely to toss pretty quickly so no big deal. The Y-axis misalignment is typically although yours is more apparent than most - easy to fix by slipping the belt a couple of notches (you could do that while removing the bed & exhaust duct). The carriage mount is a bit harder to deal with. The standoff is readily available (Amazon carries them or eBay - they're called PCB standoffs & I think they're like $3 for a bag of 10). The trouble is you've got to get the existing stud end out which will require a drill & extractor. 



I'd also check the wiring to see if something was disconnected and keeping it from working. You can do all that while waiting for a refund or a payoff from the vendor.


---
**Scott Marshall** *August 21, 2016 21:44*

If your damage was caused by the accessories crashing around loose in there (fan and pump) the carriage is likely bent beyond salvage, I made one, but they're readily available. Here's a link to a reputable parts source that also has competitive prices.

You may as well order the complete carriage and air assist head, it's probably where you'll eventually end up anyway.



Like Jim said, take lots of photos and hold firm on a high settlement, My machine was in just about the same shape as yours and I ended up with a $250 settlement 'to have it repaired locally' - The re-shipping companys DO NOT want to re-ship and have no parts or expertise to repair. On top of that they absolutely DO NOT want any negative feedback.



I asked mine simply for replacement parts and they couldn't provide them, so they countered with cash.



(I dealt with Globalfreeshipping in CA)



Good luck, take your time and you'll have it made into a 1st class machine in a few weeks.



[http://www.ebay.com/itm/K40-DIY-CO2-Laser-Rubber-Engraving-Part-Kit-Head-Mount-Mirror-Holder-Support-/262410719156?hash=item3d18e5cfb4:g:0EcAAOSw0HVWFMb7](http://www.ebay.com/itm/K40-DIY-CO2-Laser-Rubber-Engraving-Part-Kit-Head-Mount-Mirror-Holder-Support-/262410719156?hash=item3d18e5cfb4:g:0EcAAOSw0HVWFMb7)



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**Alex Krause** *August 21, 2016 22:35*

Take a video and point out all the stuff that is wrong with it... they will request a video for any type settlement 


---
**Alex Krause** *August 21, 2016 22:37*

Also inspect the laser tube and look for any signs of cracks or disconnected wires from it


---
**3D Laser** *August 21, 2016 23:05*

I would ask for a refund or at least funds back to fix it I know with mine I got 275 dollars back with my machine due to the issues if you need a new axis I have one I will sell you I bought two just to have one for parts 


---
**Purple Orange** *August 23, 2016 17:25*

Thanks for the feedback,  having a dialgue with the seller.  We will see where it lands... 


---
**Purple Orange** *August 27, 2016 14:25*

I had a long dialgue with the seller,  whom of course tried to slip away.  I  got tired of it and stated 3 options for them to settle or i would involve eBay to settle the matter.  1. Complete refund &  i ship back the faulty machine,  seller pay shipping.  2. A new machine in full working order,  i return the faulty.  Seller pays for shipping both ways.  3. I keep the machine  seller pays the actual cost of spare parts and the cost for  hiring a local company for the repsirs.  Seller agreed to 3. I sourced links on ebay for the faulty parts and sent the list plus the requested funds back GBP205.  The seller replied that's too much,  we will give yo GBP80.  I handed the case over to eBay...  Ten minutes later the seller had made a total refund to my PayPal account,  with out any request of returning the faulty k40 and closed the case.  So...  It looks like I currently have received a (faulty) k40 for free.  And I should have a  budget to repair and upgrade the k40 @ pretty much zero cost for me.  A happy day in k40gland


---
**Jim Hatch** *August 27, 2016 15:08*

Your experience is not abnormal. Like you said you get it fixed up nicely now for less than the machine cost originally. 🙂


---
**Purple Orange** *August 27, 2016 21:29*

Now I need some good advice on good replacement parts 


---
**Jim Hatch** *August 28, 2016 02:08*

I like LightObject for most everything. Or Google lasersaur and look at the suppliers in their bill of materials.


---
*Imported from [Google+](https://plus.google.com/112469922804049225807/posts/HmNAEHs21Fe) &mdash; content and formatting may not be reliable*
