---
layout: post
title: "Very new to this forum and apologise for jumping straight in with a plea for help"
date: May 30, 2016 19:54
category: "Original software and hardware issues"
author: "Stephen Smith"
---
Very new to this forum and apologise for jumping straight in with a plea for help.

Took delivery of a 40w machine last week and after many headaches and teeth grinding I have got the thing to both cut and engrave. Great, but it will not cut or engrave where I want it to. When switched on the home poition of the head appears to be in the centre of the table. When I send the drawing (via Corellaser) to the machine the head travels to the right and down until it hits the stop and then makes a terrible noise until I switch the machine off. I am not very technically minded, I recently sold my ULS machine thinking I would not use it any more, big mistake. Any help with getting the thing to home to the top left of my Corel Draw page will be very greatly appreciated. Many thanks, Stephen





**"Stephen Smith"**

---
---
**Derek Schuetz** *May 30, 2016 20:05*

Honestly it's the problem I had with coreldraw the plugin did not work 90% of the time. Laserdrw worked Everytime but it was so limited. So I installed a ramps board and it was working great until my tube just tanked. But I still don't think my ramps wiring is right which may have lead to the tube failing early 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 30, 2016 20:11*

Hi Stephen. It's a sad fact that the support/instructions for these machines is severely lacking. Lucky this group is around, as we've generally all been there done that.



So, a few suggestions I have to start off with are:

1. Make sure your board ID & model are set correctly in the CorelLaser settings. (see here: [https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)). You will find the board model & board ID on the circuit board that the USB cable plugs into (under the switch panel lid).



2. Check the settings that the files are sent to the laser with (see here: [https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)).



I've noticed that a lot of times early issues with things like speed/home position are affected by the board ID & model being incorrectly set by the software.



Hopefully these can assist you to get up & going.


---
**Phillip Conroy** *May 30, 2016 20:13*

Check that all the wires are seated right on the circuit card at front of right hand bay[board at 90deg -where usb plugs in] also check board version on circuit card and in the laser cutting pop up plugin check that the corect board is selected


---
**Ariel Yahni (UniKpty)** *May 30, 2016 20:13*

**+Stephen Smith**​ are you placing the drawing in the correct spot on the corellaser plugging ( windows that appears after clicking the small icon at the top left?).  If it's trying to home left and down or right and down?  Seems you are looking the the machine from the back? It should home left up


---
**Stephen Smith** *May 30, 2016 20:22*

Thanks, I did think that there was a mirror situation going on, I will try again tomorrow morning, if I cannot get the thing going properly  tomorrow the supplier can have it back. Stephen


---
**Stephen Smith** *May 30, 2016 20:24*

**+Phillip Conroy**

Many thanks, all the numbers are correct, I will give the wiring a once over. Stephen


---
**Stephen Smith** *May 30, 2016 20:26*

**+Yuusuf Sallahuddin**

Many thanks, I will re-check the board settings but I am pretty sure they are correct, thanks for the links. Stephen


---
**Alex Krause** *May 30, 2016 20:42*

Does the machine home to the upper left of the machine when. You turn it on or run the initialize device button on the corellaser plugin﻿


---
**Stephen Smith** *May 30, 2016 21:44*

**+Alex Krause**

Hello Alex, no it does not. If I have the head in the top right position before swiching on the head moves downwards and tries to move to the right and makes a great deal of noise when it can not progress. If I have the head in the top left position and then switch on, the head moves to the centre of the cutout section of the bed.


---
**Alex Krause** *May 30, 2016 21:46*

**+Ariel Yahni**​ weren't you having a homing issue like this? What did you do to solve this?


---
**Ariel Yahni (UniKpty)** *May 30, 2016 21:51*

Yes I had the issue but it seems intermittent,  and only goes to left bottom, I will say is either the board or the software. Why? Because if I select to make 2 jobs repeated when it ask me to says yes to the second one it goes left bottom to home and the back to left top and homes correctly


---
*Imported from [Google+](https://plus.google.com/111326322372516669266/posts/8sXtWpRgo9m) &mdash; content and formatting may not be reliable*
