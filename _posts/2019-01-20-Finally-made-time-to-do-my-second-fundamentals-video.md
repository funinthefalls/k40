---
layout: post
title: "Finally made time to do my second fundamentals video"
date: January 20, 2019 04:50
category: "Discussion"
author: "Robert Buchholz"
---
Finally made time to do my second fundamentals video.  Hope to have some upgrade videos next.  Always looking for feedback or ideas on which components to cover next. 


{% include youtubePlayer.html id="bPoLv_-PFBQ" %}
[https://youtu.be/bPoLv_-PFBQ](https://youtu.be/bPoLv_-PFBQ)





**"Robert Buchholz"**

---
---
**Doug LaRue** *January 20, 2019 21:38*

Here's a combiner mount I made for my K40 - [https://www.thingiverse.com/thing:2915288](https://www.thingiverse.com/thing:2915288)

 

There's also an alignment jig to use first and there's a drag chain mount in the repo too.


---
**Ned Hill** *January 21, 2019 00:35*

Nice job.  Lots of good basic info there.


---
**Stephane Buisson** *January 23, 2019 09:45*

allways good to go back to basic. thank's **+Robert Buchholz**


---
*Imported from [Google+](https://plus.google.com/117736558877962713281/posts/5EjcKuZRUHL) &mdash; content and formatting may not be reliable*
