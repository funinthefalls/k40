---
layout: post
title: "Looking to replace exhaust motor that came with the 80watt laser"
date: August 19, 2017 15:02
category: "Hardware and Laser settings"
author: "Mike Gallo"
---
Looking to replace exhaust motor that came with the 80watt laser. Description it's in chinese, can't read it.  This motor burn out as soon I plug in , fire spit out from back.  

![images/2a1d772530775dd5350404c7b0547b15.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2a1d772530775dd5350404c7b0547b15.jpeg)



**"Mike Gallo"**

---
---
**Martin Dillon** *August 19, 2017 15:45*

If you can't find a replacement. This is what I use.  I got mine a little cheaper than this but it works well and is surprisingly quiet.  [grizzly.com - So far, so good](http://www.grizzly.com/products/1-HP-Light-Duty-Dust-Collector-Polar-Bear-Series/G1163P)  Inlet on mine is 4" and outlet is 5" so you need a 5-4 reducer.


---
**Steve Clark** *August 19, 2017 18:31*

Here is what I’m using:



[http://www.ebay.com/itm/Air-Blower-Pump-Fan-480-Watt-0-64HP-For-Inflatable-Bounce-House-Bouncy-Castle-/191941885115?hash=item2cb0a0bcbb:g:~s4AAOSwHoFXur6Y](http://www.ebay.com/itm/Air-Blower-Pump-Fan-480-Watt-0-64HP-For-Inflatable-Bounce-House-Bouncy-Castle-/191941885115?hash=item2cb0a0bcbb:g:~s4AAOSwHoFXur6Y)



then one of these to control the draw.



[https://www.walmart.com/ip/Yescom-Electronic-Fan-Speed-Controller-Variable-Adjustor-for-Hydroponics-Inline-Duct-Exhaust-Ceiling-Fans/161658436?wmlspartner=wlpa&selectedSellerId=1913&adid=22222222227049535055&wmlspartner=wmtlabs&wl0=&wl1=g&wl2=c&wl3=146686767032&wl4=pla-51318218047&wl5=9032224&wl6=&wl7=&wl8=&wl9=pla&wl10=113510113&wl11=online&wl12=161658436&wl13=&veh=sem](https://www.walmart.com/ip/Yescom-Electronic-Fan-Speed-Controller-Variable-Adjustor-for-Hydroponics-Inline-Duct-Exhaust-Ceiling-Fans/161658436?wmlspartner=wlpa&selectedSellerId=1913&adid=22222222227049535055&wmlspartner=wmtlabs&wl0&wl1=g&wl2=c&wl3=146686767032&wl4=pla-51318218047&wl5=9032224&wl6&wl7&wl8&wl9=pla&wl10=113510113&wl11=online&wl12=161658436&wl13&veh=sem)



Finally one of these to mount to the intake for the Aluminum exhaust tube. Works well you need to mount it on a piece of wood to keep it from walking around a bit. I almost always run it at half speed or even less.



[https://www.zoro.com/oatey-toilet-flange-floor-4-in-spigot-fit-43587/i/G0954712/](https://www.zoro.com/oatey-toilet-flange-floor-4-in-spigot-fit-43587/i/G0954712/)






---
**Mike Gallo** *August 19, 2017 18:34*

thank you for the help and links.




---
**Martin Dillon** *August 19, 2017 19:08*

**+Steve Clark** I like it.  Much cheaper solution.  I used what I already had.  One question though, why control the air flow? 


---
**Steve Clark** *August 19, 2017 20:43*

Well... think of how a jet engine works or a blow forge, the more air going by the fuel (the part we are cutting) the more chance of a accelerated ignition and fire blowup because we are providing a lot of oxygen and pulling away the waste gases. I believe we want just enough to pull the smoke away from our laser beam and local environment and inside of the laser..


---
**Martin Dillon** *August 19, 2017 22:14*

Do you have air assist? I was thinking of blowing out a candle. 

My thought was to come up with a way to focus the vacuum under my cutting table so I could get a semi vacuum table going to hold things in place.


---
**Steve Clark** *August 20, 2017 03:30*

Yes, I have air assist the LightObject 19mm laser head with air-assist. A vacuum assist would a interesting challenge. I will be interested in what you come up with.  **+Martin Dillon** 


---
**Paul de Groot** *August 20, 2017 05:45*

I use an air assist and further replaced the faulty fan on the back of the k40 with a pc fan. Works like a charm. 


---
*Imported from [Google+](https://plus.google.com/108921378497443668729/posts/2Cb11y8pAyk) &mdash; content and formatting may not be reliable*
