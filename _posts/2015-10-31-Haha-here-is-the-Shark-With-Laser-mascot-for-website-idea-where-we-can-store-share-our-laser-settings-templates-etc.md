---
layout: post
title: "Haha, here is the Shark With Laser (mascot for website idea where we can store/share our laser settings/templates/etc)"
date: October 31, 2015 15:15
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Haha, here is the Shark With Laser (mascot for website idea where we can store/share our laser settings/templates/etc).

![images/41f96620100c8cfc88a80174d6e71daa.png](https://gitlab.com/funinthefalls/k40/raw/master/images/41f96620100c8cfc88a80174d6e71daa.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**David Cook** *November 03, 2015 00:50*

"sharks with frickin' laser beams attached to their heads! "


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 03, 2015 05:13*

**+David Cook** Haha, that was the inspiration.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Q5TaryYNMpo) &mdash; content and formatting may not be reliable*
