---
layout: post
title: "Adjustable laser tube mounts .Any one found any of these that fit into a k40"
date: August 24, 2016 09:36
category: "Discussion"
author: "Phillip Conroy"
---
Adjustable laser tube mounts .Any one found any of these that fit into a k40 







**"Phillip Conroy"**

---
---
**Phillip Conroy** *August 24, 2016 09:37*

forgot link to what i have already brought and did not fit...[http://www.aliexpress.com/item/Co2-laser-tube-holder-tube-supports-50-80cm-for-50-180W-Co2-glass-laser-tube/32597666905.html?spm=2114.13010308.0.49.gbMwYT](http://www.aliexpress.com/item/Co2-laser-tube-holder-tube-supports-50-80cm-for-50-180W-Co2-glass-laser-tube/32597666905.html?spm=2114.13010308.0.49.gbMwYT)


---
**Ian C** *August 24, 2016 13:55*

I'm keen to know as well as adjustable and more stable ones would be nice. I saw those ones on Ali, glad I didn't press buy now. Thank you for the heads up.


---
**Robert Selvey** *August 25, 2016 01:16*

You might want to check these out. [http://www.thingiverse.com/thing:1581069](http://www.thingiverse.com/thing:1581069) and he also has a great video on putting them in. 
{% include youtubePlayer.html id="SkjaRbDl4HI" %}
[https://www.youtube.com/watch?v=SkjaRbDl4HI](https://www.youtube.com/watch?v=SkjaRbDl4HI)


---
**Don Kleinschnitz Jr.** *September 27, 2016 12:49*

**+Phillip Conroy** is this the same one: [banggood.com - CO2 Laser Tube Support Stand Holder Adjustable 60-90MM](http://www.banggood.com/CO2-Laser-Tube-Support-Stand-Holder-Adjustable-50-80MM-p-965906.html)



What did not fit about it .... thks.


---
**Phillip Conroy** *September 28, 2016 09:17*

Too wide  size is stated as 11cm wide


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/f4Uy4Yz1N87) &mdash; content and formatting may not be reliable*
