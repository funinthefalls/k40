---
layout: post
title: "So Im considering upgrading my new c3d controller with the GLCD display"
date: December 08, 2017 13:57
category: "Modification"
author: "David Allen Frantz"
---
So I’m considering upgrading my new c3d controller with the GLCD display. I already have the GLCD all I need is the little adapter to plug it in. My question is if I do that, do I still need to have the knob for the wattage adjustment or will that be all adjusted through the GLCD control? 





**"David Allen Frantz"**

---
---
**Joe Alexander** *December 08, 2017 16:08*

you will still have the pot to control the max power level and for on the fly adjustments. most people prefer it that way over straight ttl from the board.


---
**David Allen Frantz** *December 08, 2017 17:11*

**+Joe Alexander** OK great I was just curious to know if I needed to leave room for it on the new panel I’d be making… Thanks!


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/Z3kcTfc5d7x) &mdash; content and formatting may not be reliable*
