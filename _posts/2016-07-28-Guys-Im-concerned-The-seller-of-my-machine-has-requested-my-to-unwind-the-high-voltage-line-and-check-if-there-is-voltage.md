---
layout: post
title: "Guys, I'm concerned... The seller of my machine has requested my to unwind the high voltage line and check if there is voltage"
date: July 28, 2016 08:02
category: "Hardware and Laser settings"
author: "Anthony Santoro"
---
Guys, 

I'm concerned... The seller of my machine has requested my to unwind the high voltage line and check if there is voltage. 

I have attached the instruction video that they've provided... Is this just insane?? 

How high is the voltage on out of these power supplies? I could get a serious shock from doing this. 

Do you think a standard multimeter would be a better way to check voltage from the power supply, or will this not be strong enough? 

Your advice will be appreciated. 







**"Anthony Santoro"**

---
---
**Stephane Buisson** *July 28, 2016 08:15*

on this side of the PSU, you are >15000V not good with multimeter.

but you don't need a value, just to find out if PSU is dead or if it's the tube. if no arcing your PSU is dead.

but not really a test for everybody to do.


---
**Anthony Santoro** *July 28, 2016 08:58*

15000V!! That's huge! 

I think I'll just get a nail in a long piece of timber to test the arcing. My multimeter will explode at 15000V.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 28, 2016 09:35*

Alternatively place a webcam in the tube compartment & watch what happens when you test fire. You should be able to visibly see any arcs (as it is basically dark in there).


---
**I Laser** *July 28, 2016 09:56*

There is NO OH&S in China!


---
**Jon Bruno** *July 28, 2016 10:51*



Testing for arcs can damage the power supply.

Oh and then there's this...


{% include youtubePlayer.html id="O5Jn3Ys8YNU" %}
[https://youtu.be/O5Jn3Ys8YNU](https://youtu.be/O5Jn3Ys8YNU)﻿


---
**Don Kleinschnitz Jr.** *July 28, 2016 11:55*

Arching to grnd is not good for the supply nor the human holding the plug. Sorry, this is just stupid, these supplies are LETHAL!



It seems that we as community we should come up with a safe way to test LVPS's. I see lots of posts where during troubleshooting it is necessary to isolate a bad LPS from the Laser.



Essentially we would need a DIY HV probe that will work with a DVM and some published values that indicate good-bad. You can buy probes but they are expensive.

----------------------------

Here are some videos on the subject of testing LPS: 



Don't like the safety on this one:


{% include youtubePlayer.html id="dz4ln4_vT3g" %}
[https://www.youtube.com/watch?v=dz4ln4_vT3g](https://www.youtube.com/watch?v=dz4ln4_vT3g) .... no idea what the resistor value is?



Better: 


{% include youtubePlayer.html id="Qz6J4Rs7VCw" %}
[https://www.youtube.com/watch?v=Qz6J4Rs7VCw](https://www.youtube.com/watch?v=Qz6J4Rs7VCw), this uses 10x 47meg resistors enclosed in a shop made probe.



You can get some HV resistors here:

[https://www.amazon.com/Axial-Glaze-Voltage-Power-Resistor/dp/B00UBWL58S/ref=sr_1_1?ie=UTF8&qid=1469706518&sr=8-1&keywords=High+Voltage+Power+Resistor](https://www.amazon.com/Axial-Glaze-Voltage-Power-Resistor/dp/B00UBWL58S/ref=sr_1_1?ie=UTF8&qid=1469706518&sr=8-1&keywords=High+Voltage+Power+Resistor)


---
**Jon Bruno** *July 28, 2016 11:58*

Most of the people in this forum should not be playing with high voltage. Remember when they used to install stickers that said There are no user-serviceable parts inside. As a matter of fact there very few people in this forum properly educated on operating a laser in a safe manner.﻿


---
**David Lancaster** *July 28, 2016 12:04*

You could try the packing peanut test.  I haven't verified it myself, but supposedly give you a quick indication if the supply is completely failed (though it doesn't discriminate between a good supply and a marginal one).

Visit here and search for "peanut":

[http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/252972-software-posts.html](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/252972-software-posts.html)


---
**Don Kleinschnitz Jr.** *July 28, 2016 12:07*

**+Jon Bruno** That is true, but it does not stop them from trying :(


---
**Don Kleinschnitz Jr.** *July 28, 2016 12:11*

**+David Lancaster** I wonder if a neon bulb (like in an AC tester) would light next to the HV cable??? Or a coil of wire around the HV cable connected to a current sense.


---
**Anthony Santoro** *July 28, 2016 12:33*

Thanks guys, I also think this is crazy. 

I tested it with a rig, I was far away from the arc (there actually wasn't any arc in the end. The power supply must have failed). 

I agree, these are potentially lethal machines and the idiots selling these machines don't realise they're risking peoples lives by instructing dangerous tasks. 

An alignment video that the supplier provided showed a person holding their hand in front of the beam with a piece of cardboard... Just asking for 3rd degree burns! 

There are so many negatives about these machines and their suppliers, but for the price point there is literally no other option. 


---
**Don Kleinschnitz Jr.** *July 28, 2016 12:45*

**+Anthony Santoro** Just FYI, others that have had a LPS fail have repaired them when the input rectifier have failed. Is there any fuse etc blown?


---
**greg greene** *July 28, 2016 12:56*

There are plenty of insulated high voltage testers around on ebay - I use them in HF Amps for Ham Radio - use one of them.


---
**David Cook** *July 28, 2016 19:32*

That's nuts lol


---
**greg greene** *July 28, 2016 19:33*

NO! a multimeter will NOT do it, but it might KILL YOU trying


---
**Anthony Santoro** *July 28, 2016 22:18*

Thanks for the advice guys! 

I tested this in the safest way possible, with a rig on a rubber mat. I stood far away from the live wire. 

The support from the seller is suicidal. 


---
**I Laser** *July 28, 2016 22:59*

I wonder if the warranty covers funeral expenses these days...



I guess the reality is, they are cheap machines and most sellers (at least on eBay) appear to be drop shippers with very little experience.



I won't be playing around with the PSU anytime soon as I have very little electrical experience and as Peter van der Walt has pointed out it will only take a couple of people to accidentally die following dubious advice before the government will crack down on the use of such machines.


---
**Matt Omond** *July 28, 2016 23:03*

Just tell them it didn't work. At least you should get a replacement. 


---
**Anthony Santoro** *July 28, 2016 23:06*

**+I Laser** did you end up getting the bubbles out of your tube? What was your solution?


---
**I Laser** *July 28, 2016 23:24*

**+Anthony Santoro**  Yes, it was an absolute nightmare. It seems my home made flow detection using float switches had been aerating the water. The tube design exacerbated this. I ended up submerging the return lines and replacing the water completely.


---
*Imported from [Google+](https://plus.google.com/106649411786258336023/posts/jHx7UTRiK3z) &mdash; content and formatting may not be reliable*
