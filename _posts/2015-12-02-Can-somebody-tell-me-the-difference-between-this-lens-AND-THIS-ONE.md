---
layout: post
title: "Can somebody tell me the difference between this lens: AND THIS ONE:"
date: December 02, 2015 23:22
category: "Discussion"
author: "Sean Cherven"
---
Can somebody tell me the difference between this lens:

[http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx](http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx)



AND THIS ONE:

[http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F508mm-P670.aspx](http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F508mm-P670.aspx)





**"Sean Cherven"**

---
---
**Gee Willikers** *December 02, 2015 23:29*

Just the quality I imagine. fwiw have the LSR-ZNSE1850HQ and I'm quite happy with it.


---
**Stephane Buisson** *December 02, 2015 23:47*

Max power and price .


---
**Sean Cherven** *December 02, 2015 23:52*

But how much do you actually gain? My budget is tight this year, and i'm trying to justify the extra cost.


---
**Stephane Buisson** *December 02, 2015 23:56*

I am not sure you gain anything, it`s more about the lens solidity certification before shattering.

(the possible amount of energy going through)


---
**Sean Cherven** *December 03, 2015 00:03*

So would u suggest I get the cheaper one for now?


---
**Stephane Buisson** *December 03, 2015 00:10*

yes I do, light object is just a reseller on that one (imported mention).

->  better HQ Znse 18 mm 50.8 focal

[http://www.ebay.co.uk/itm/HQ-ZnSe-GaAs-Focus-Focal-Lens-Si-Mo-K9-Reflective-Mirrors-CO2-Laser-Engraving-/181596677092?var=&hash=item2a480163e4:m:meMO7Y-AHxPJ3mdppCpT1mw](http://www.ebay.co.uk/itm/HQ-ZnSe-GaAs-Focus-Focal-Lens-Si-Mo-K9-Reflective-Mirrors-CO2-Laser-Engraving-/181596677092?var=&hash=item2a480163e4:m:meMO7Y-AHxPJ3mdppCpT1mw)


---
**Gee Willikers** *December 03, 2015 00:50*

I have this bookmarked - the k40 group members on fb recommended it.



[http://www.ebay.com/itm/261623000116?_trksid=p2055119.m1438.l2649&var=560531468505&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/261623000116?_trksid=p2055119.m1438.l2649&var=560531468505&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Scott Thorne** *December 03, 2015 10:48*

I have the 29 dollar one and it works great.


---
**Sean Cherven** *December 03, 2015 12:56*

I'm probably gonna go with the $29 one. Thanks everyone for your help!


---
**Scott Thorne** *December 03, 2015 12:58*

**+Sean Cherven**​...I would....the other is a waste of money...just my opinion.


---
**Coherent** *December 03, 2015 15:10*

I bought the HQ one. Must admit I see an improvement over the original 12mm lens that came with the machine. Can't comment on the $29 one but like others stated you may see very little difference & if money is an issue go with the lower priced one.


---
**Scott Thorne** *December 03, 2015 15:37*

That's good to know, it might just make a big difference...I might just order one today so I can see what the difference really is....with the 29 dollar one I can cut 3/16 plywood at 6mA and a speed of 8mm/s.


---
**Sean Cherven** *December 03, 2015 17:46*

**+Scott Thorne**, is the $29 one a improvement over the stock lens? 


---
**Stephane Buisson** *December 03, 2015 18:05*

pay attention to the lens diameter 12 or 18 mm ?


---
**Sean Cherven** *December 03, 2015 18:15*

Yah ik, I'm gonna be getting the air assist head.


---
**Scott Thorne** *December 03, 2015 19:13*

It is a big improvement over the stock lens....I couldn't cut 1/8 plywood without burning it with the stock lens...get it you won't regret it.


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/2vFd2PQape5) &mdash; content and formatting may not be reliable*
