---
layout: post
title: "When I test fire I get noise but no burn anywhere , not even at the first mirror"
date: August 05, 2016 17:34
category: "Original software and hardware issues"
author: "Frank Fisher"
---
When I test fire I get noise but no burn <b>anywhere</b>, not even at the first mirror.  This may not be connected to the dodgy power dial because as far as I read the test fire bypasses this and sends full power to the tube.





**"Frank Fisher"**

---
---
**Ariel Yahni (UniKpty)** *August 05, 2016 17:39*

Do you see the plasma on the tube? Like a pink/red color when firing? 


---
**Frank Fisher** *August 05, 2016 17:40*

OK I will have a look ...




---
**Frank Fisher** *August 05, 2016 17:42*

I see lightning between the red high voltage lead direct to the case, looks like it is bypassing the tube!




---
**Frank Fisher** *August 05, 2016 17:43*

I believe they are sealed on with clear silicon which was already blackened at this spot when it arrived.




---
**Stephane Buisson** *August 05, 2016 17:47*

you should try to redo connection. get rid of old silicone. reconnect and new silicone.


---
**Ariel Yahni (UniKpty)** *August 05, 2016 17:47*

So no color inside the tube when firing? 


---
**Frank Fisher** *August 05, 2016 17:47*

someon youtubes this exact thing [https://www.google.es/url?sa=t&rct=j&q=&esrc=s&source=web&cd=7&cad=rja&uact=8&ved=0ahUKEwic9fu76qrOAhWGcRQKHTPNCmMQtwIISDAG&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DIKlPz9qI_ho&usg=AFQjCNGSccGcYtjNuDXvmZwv2jN6mrczAw&sig2=2erG9Z_1Yo-lqz_EHnVIlw](https://www.google.es/url?sa=t&rct=j&q=&esrc=s&source=web&cd=7&cad=rja&uact=8&ved=0ahUKEwic9fu76qrOAhWGcRQKHTPNCmMQtwIISDAG&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DIKlPz9qI_ho&usg=AFQjCNGSccGcYtjNuDXvmZwv2jN6mrczAw&sig2=2erG9Z_1Yo-lqz_EHnVIlw)




---
**Stephane Buisson** *August 05, 2016 17:51*

You will find here some old post with arcing, you aren't the first nor the last ;-))


---
**Frank Fisher** *August 05, 2016 17:51*

the power isn't going through the tube at all, presume at minimum I now have to disconnect and reconnect both wires and silicon, or is the tube already dead?




---
**greg greene** *August 05, 2016 17:53*

I can vary the strength of the test firing by using the controller pot 


---
**Eric Flynn** *August 05, 2016 18:02*

**+Frank Fisher**  When you test fire, check to see where it is arcing.  If it is arching where the connection is made at the tube, redo the connection.  If it is arcing somewhere along the wire (which happens) due to a bad HV wire, you can try to seal the puncture with silicone, and try to move it so its not so close to metal.  DO NOT fire it too much when its arcing, only a bit to test where its arcing. This can kill your PSU.


---
**Frank Fisher** *August 05, 2016 18:04*

yes the pot needs to be quite high on mine to make it arc :(




---
**Bart Libert** *August 05, 2016 18:06*

this is certainly not good. I don't think it can kill the tube, maybe the PSU if not correctly grounded but it is verry possible that the arc connects the +15000v (dunno exactly how much on a 40W tube) to the metal of your machine. 




---
**Frank Fisher** *August 05, 2016 18:07*

I think the problem is at the connection as it has blackened, melted and bubbled the silicon there.  I would have thought if the tube was working it would be so much easier to go through it instead?


---
**Ariel Yahni (UniKpty)** *August 05, 2016 18:11*

Disconnect, discharge, remove silicon, clean, reconnect, apply silicon 


---
**Frank Fisher** *August 05, 2016 18:13*

OK that will be a job for tomorrow as I have no clear silicon.  Which type of silicon should I use,  the acid or neutral?




---
**Jon Bruno** *August 05, 2016 18:13*

the unit should have come with a small tube of HV silicone that they use on the tube to insulate the connections.. 

Clean it up, reconnect the anode wire,  and apply some and then let it fully cure..

If that doesn't fix it...



You already have a damaged POT.. now you are looking at a possible damaged tube.. I wouldn't be surprised if this unit was hurt during its trip west.

Contact the seller asap and ask for your money or a replacement.


---
**Eric Flynn** *August 05, 2016 18:18*

The pot may not be damaged.  It may just be that the air gap is large enough to require full power before it will arc.


---
**Frank Fisher** *August 05, 2016 18:18*

Already contacted them but they say they only answer in a one hour window around midnight they say.  Will triple check there is no silicon but never saw it on my unpacking.


---
**Jon Bruno** *August 05, 2016 18:19*

**+Eric Flynn** he had an issue earlier today where his pot was spinning freely


---
**Eric Flynn** *August 05, 2016 18:20*

Ahh, yea, did he check the knob?  Its a collet type and may just be loose.  if its tight, and it still spins, then yea, bad pot.


---
**Jon Bruno** *August 05, 2016 18:22*

**+Frank Fisher** They used to ship with a small blue zipper bag.. in the bag they would typically provide double backed tape, the bootleg software cd, the software hasp key, and a tube of some very untasty toothpaste.


---
**Ariel Yahni (UniKpty)** *August 05, 2016 18:24*

As said above,  you should have a white silicon on you happy meal bag included with the laser


---
**Jon Bruno** *August 05, 2016 18:26*

Hah! "Happy meal"


---
**Frank Fisher** *August 05, 2016 18:32*

We just found the tube of silicon, would never have known, was just wedged behind the x/y mechanism and took a lot of getting out.


---
**Frank Fisher** *August 05, 2016 18:32*

Haven't seen any tape though.




---
**Eric Flynn** *August 05, 2016 18:33*

I havent figured out what the tape was supposed to be fore anyway.  I used it for other stuff!!


---
**Jon Bruno** *August 05, 2016 18:36*

Awesome. You know that eventually would have caught on fire.

Clean everything up with some alcohol or acetone to get the nasties off.then liberally apply some silicone. It is self levelling so it will tend to dribble and run off. If you can snip a small length of your silicone cooling hose off (1/2") and stick it over the pin on the tube and then add silicone to that structure. That will give you yet more insulation.


---
**Jon Bruno** *August 05, 2016 18:38*

haha the tape may be for holding down work pieces.. I alwasys see people using it to align the machines but thats not its intended purpose. Since these are "stamp engravers" I assume it's used to keep rubber stock in place on the bed.


---
**Frank Fisher** *August 05, 2016 18:38*

I suppose I need to do both sides in case the earth isn't attached properly at the other end.  How long does it take to cure?


---
**Frank Fisher** *August 05, 2016 18:40*

We have spare water tubing quite similar so can use a bit of that.


---
**Jon Bruno** *August 05, 2016 18:40*

the other side is rarely an issue but it couldn't hurt to touch it up.

it takes a little while to cure. I can't say exactly how long because I usually apply and go off onto something else.


---
**Frank Fisher** *August 06, 2016 07:51*

Their support guys are behind the Chinese firewall so don't send them a youtube link like I just did :/


---
**Frank Fisher** *August 06, 2016 11:33*

I have sent a photo to them of the charred, molten silicone from the arcing, which was there as shipped before I plugged it in. Even at that stage it looked wrong to me.  As it is brand new and I dont want a paypal dispute ending badly I have contacted their support before diving in to repair it,  If they dont authorise resealing the connector they can refund or try sending a working one.




---
**Jon Bruno** *August 06, 2016 12:09*

Just do one thing. Do not play with the arc. Don't show anyone, leave it alone from now until it's fixed.

If you keep zapping away the power supply will be the next bad part.


---
**Frank Fisher** *August 06, 2016 12:16*

Oh for sure.  Didn't even make my own video of it, plenty out there already!  Won't be switched on again until fixed.


---
**Jon Bruno** *August 06, 2016 12:17*

I posted one a while back if you need stock footage to use for demonstration. Check my YouTube channel it's in there... Sigmazgfx﻿     




{% include youtubePlayer.html id="O5Jn3Ys8YNU" %}
[https://youtu.be/O5Jn3Ys8YNU](https://youtu.be/O5Jn3Ys8YNU)


---
**Frank Fisher** *August 09, 2016 06:39*

Heard back from the seller, they say tube is likely broken (given it was blackened at shipping why not) and I should buy a new one from Alibaba and they'll pay me back.  Never heard anything like it.  Anyone good at ebay/paypal disputes as I never had to before in all these years.


---
**Frank Fisher** *August 10, 2016 06:34*

Another day, Paypal dispute open, and another message that I should buy a tube and they will pay me back somehow ...


---
*Imported from [Google+](https://plus.google.com/114228670405979897634/posts/fcs5id9Pk5f) &mdash; content and formatting may not be reliable*
