---
layout: post
title: "What should be used on the guide rails to lube them ?"
date: September 05, 2016 01:12
category: "Discussion"
author: "Robert Selvey"
---
What should be used on the guide rails to lube them ?





**"Robert Selvey"**

---
---
**Jim Hatch** *September 05, 2016 01:56*

They really don't need lubing. They do need to be cleaned every once in awhile. Dust and smoke particles can build up and cause skipping. I wipe mine with isopropyl alcohol.


---
**Scott Marshall** *September 06, 2016 12:36*

Jim is right. If you lube them, they will attract dirt and wear rapidly.


---
**Nigel Conroy** *September 07, 2016 19:18*

You can use a dry lubricant. 3-in-one make one and so do a bunch of others.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/8gHTLhiafSo) &mdash; content and formatting may not be reliable*
