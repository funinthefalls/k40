---
layout: post
title: "Okay, I am happy that I stayed on schedule to have my K40 Modded by Jan.1 I even broke out the old Canon C100 to film with instead of my phone!"
date: December 29, 2017 01:34
category: "Modification"
author: "Tech Bravo (Tech BravoTN)"
---
Okay, I am happy that I stayed on schedule to have my K40 Modded by Jan.1 I even broke out the old Canon C100 to film with instead of my phone! And my air pump is 950 GPH, not GPM LOL. I caught that after the fact. I am excited to push out some work and see what this thing will do! Thanks to Don, Frank, Ray, Cohesion3D, Lightburn Software, and everyone else who helped and followed me through this project! Until next time. Peace Out! 


{% include youtubePlayer.html id="6i-DkGoAL7g" %}
[https://youtu.be/6i-DkGoAL7g](https://youtu.be/6i-DkGoAL7g)





**"Tech Bravo (Tech BravoTN)"**

---
---
**Chris Hurley** *December 30, 2017 19:52*

Any chance we could get some pictures of your mount for the cable chain with the air hose? 


---
**Tech Bravo (Tech BravoTN)** *December 30, 2017 20:11*

**+Chris Hurley** [drive.google.com - Air Assist - Google Drive](https://drive.google.com/drive/folders/1hoO04XzJkJTdCALJ-dHn9uVwN3S3B2a1?usp=sharing)


---
**Chris Hurley** *December 30, 2017 20:19*

**+Tech Bravo** awesome thanks! Was it the 80w compressor used? 


---
**Tech Bravo (Tech BravoTN)** *December 30, 2017 20:23*

32 watt 950gph. Probably gonna go bigger on the air pretty soon. Go to [techbravo.net - K40 Laser Info - Bravo Technologies](http://techbravo.net/k40) and click the items in the list. They link to the actual products i used.


---
**Mark Kirkwold** *January 06, 2018 21:34*

Looks great, one question though. Does the air pump vibrate at all? I'd be concerned about introducing vibrations to the optical system and work area; probably the tube would not like it either. 


---
**Tech Bravo (Tech BravoTN)** *January 06, 2018 21:51*

**+Mark Kirkwold** thanks. actually, it did not vibrate (or at least it was not noticeable) but as it runs and warms up the vibration becomes more profound. i have since relocated it behind the engraver and mounted it on the table to isolate it from the machine. it is much better now. that also gives better access to the electronics and reduces the ambient temperature.


---
**Tech Bravo (Tech BravoTN)** *January 06, 2018 22:17*

here is the video explaining the drag chain: 
{% include youtubePlayer.html id="yiQeWHagxAY" %}
[youtube.com - Cyclops K40 Drag Chain Install Details](https://youtu.be/yiQeWHagxAY)




---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/g2pKrPAmrqx) &mdash; content and formatting may not be reliable*
