---
layout: post
title: "The beautifully carved patterns can be used to decorate the room"
date: June 09, 2018 09:56
category: "Discussion"
author: "leize yan"
---
The beautifully carved patterns can be used to decorate the room.

![images/928f546cbaff6841e166b28842648caa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/928f546cbaff6841e166b28842648caa.jpeg)



**"leize yan"**

---
---
**Don Kleinschnitz Jr.** *June 09, 2018 13:41*

Can you share the source files?


---
**leize yan** *June 11, 2018 07:47*

**+Don Kleinschnitz** what do you mean the  source files?


---
**Anthony Bolgar** *June 12, 2018 00:37*

He means the files you load into the laser software to cut out the shapes.




---
**leize yan** *June 12, 2018 06:24*

sorry，the files are my customer


---
*Imported from [Google+](https://plus.google.com/114945572418578072206/posts/7ihMUxr5sao) &mdash; content and formatting may not be reliable*
