---
layout: post
title: "Hi everyone, I am having difficulty cutting anything at all with this machine"
date: February 07, 2015 00:21
category: "Discussion"
author: "Jessica Pobjoy"
---
Hi everyone,

I am having difficulty cutting anything at all with this machine.

3mm ply, acrylic, or MDF just not working.  I can cut simple shapes out of 3mm ply, but it takes three passes at 2mm/s with the laser up full, which really doesn't seem right to me.

Engraves fine, and can cut through cardboard neatly, no worries.

I have triple checked the mirror / laser alignment, including checking the alignment at different points around the XY axis by moving the carriage and checking the laser "dot" marks with masking tape.

It seems as though we just can't get enough energy in the laser to penetrate the material.

We are also getting a lot of heat and flame when cutting, and a seemingly uneven cut depth when looking at the reverse of the material after cutting.

Any tips for us? Unfortunately this machine isn't really working for us at all right now.

I would appreciate any help or suggestions.  Many thanks,

JESS POBJOY





**"Jessica Pobjoy"**

---
---
**Rick Ruggiero** *February 07, 2015 00:46*

Hi Jessica,  I had a host of similar problems with the one I purchased, however mine would not even mark acrylic.  In fact if I held the laser test button down for 10 - 15 seconds at full power the laser would bore a hole about .2mm into the acrylic sheet.



At full power my machine would not even cut paper.  I suspect that there is a problem (with yours and mine)with the Moshi board and/or the  power supply. 



Is your machine grounded to a separate earth?



I have sent my machine back and am waiting for my refund.  



Rick


---
**Tim Fawcett** *February 09, 2015 21:00*

What cutting current are you using?

Also have you properly aligned the beam - mine took a lot of fettling to get it spot on.


---
*Imported from [Google+](https://plus.google.com/100410994816290313574/posts/ALrjN8kpYdE) &mdash; content and formatting may not be reliable*
