---
layout: post
title: "I have a question about Air Assist, currently I am using the stock Air Pump which I got with my unit, a ACO-002 which is 35 Watt and 40 Lt per minute system"
date: December 14, 2017 07:12
category: "Air Assist"
author: "Sunny Koh"
---
I have a question about Air Assist, currently I am using the stock Air Pump which I got with my unit, a ACO-002 which is 35 Watt and 40 Lt per minute system. Recently I ran the pump and feel the power under the nozzle and it while still blowing air doesn't seem to be that powerful. Does upgrading the Air Pump to something more powerful help in the cutting and engraving of Acyrlic and similar materials?





**"Sunny Koh"**

---
---
**Andy Shilling** *December 14, 2017 07:21*

I don't run the air assist when cutting acrylic, I don't know if it's just me but I find it clouds the surface and then need cleaning where as without it leaves it's nice and clean.


---
**Joe Alexander** *December 14, 2017 07:29*

that's cause acrylic sublimates instead of vaporizing, so it can re-condense on surrounding materials. I have heard a thin bit of soap helps treat the surface beforehand, or tape.


---
**java lang** *December 14, 2017 09:01*

If you feel the power under the nozzle I think you have a way too much airflow. There is an excellent article about air/smoke assist in here: [k40laser.se - Air assist - K40 Laser](https://k40laser.se/air-assist/) .

I just removed my air-assist completely as I had also this surface effects and replaced it with a smoke assist like [https://k40laser.se/smoke-assist/](https://k40laser.se/smoke-assist/) and I'm very happy now. I'm using a 12V fan running at 5V to provide a very very small flow blowing away the smoke and to prevent from flaming-up.


---
**Don Kleinschnitz Jr.** *December 14, 2017 14:22*

#K40AirSmokeAssist


---
**Ned Hill** *December 14, 2017 15:19*

As that excellent  linked post above said, you really don't need a large flow rate for the standard "air assist" heads most of us have.  I have a flow gauge on mine and I'm really only running 7-10 L/min for most things.  It's enough to keep the lens/cut area clear and to mitigate against flame ups.


---
**Don Kleinschnitz Jr.** *December 14, 2017 15:36*

**+Ned Hill** what flow gauge do you use?.


---
**Ned Hill** *December 14, 2017 15:50*

**+Don Kleinschnitz**  It's one I got off Banggood but it looks like they aren't selling that one anymore.  

![images/d4b3d91ed53451673477a7267741d860.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d4b3d91ed53451673477a7267741d860.png)


---
**Ned Hill** *December 14, 2017 16:01*

**+Don Kleinschnitz** I found it on amazon [https://www.amazon.com/LZQ-5-Flowmeter-2-5-25-Meter-Oxygen/dp/B01CKR39XA/](https://www.amazon.com/LZQ-5-Flowmeter-2-5-25-Meter-Oxygen/dp/B01CKR39XA/)


---
**Don Kleinschnitz Jr.** *December 14, 2017 19:07*

I need if for no other reason that it looks cool :)!


---
**Ned Hill** *December 14, 2017 19:11*

It's also really cool if you light them with an LED. :)

![images/8d5c269623f4ee44f5e024c316389b5f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d5c269623f4ee44f5e024c316389b5f.jpeg)


---
**Don Kleinschnitz Jr.** *December 14, 2017 19:21*

#K40AirAssist

OK.... that did it one is now going on order! Maybe one for my CNC mill also :)...


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/6574QapJ4bW) &mdash; content and formatting may not be reliable*
