---
layout: post
title: "How thick mdf boards can I cut?"
date: October 08, 2015 01:08
category: "Materials and settings"
author: "Niclas Jansson"
---
How thick mdf boards can I cut?

Does anyone know/have experience with that?





**"Niclas Jansson"**

---
---
**ThantiK** *October 08, 2015 01:11*

You don't (typically).  MDF contains formaldehyde, a very toxic chemical.  You don't want to be cutting MDF with a laser.  Heck, even cutting MDF with traditional methods can be somewhat harmful to your health.  And generally you're looking at a quarter of an inch.


---
**Niclas Jansson** *October 08, 2015 01:14*

Even if I have really good extraction of the fumes?

What is the option if I want a smooth surface that I can both engrave and cut then?



I want to do embossing "plates" for leather, regular ply leaves small grain marks in the leather, and acrylic can't hold the detail of the engraving for the emboss..


---
**Gregory J Smith** *October 09, 2015 01:05*

**+ThantiK** Interesting info re MDF.  I've done some research on materials that are suitable for laser cutting and many say that MDF is ok (one site saying it's their preferred material - e.g. Laser cutting shop "MDF cuts so well it seems like it was made for laser cutting").  I've been cutting 3mm MDF easily. I haven't tries anything thicker. I do prefer to use plywood because it gives a more natural looking wood result.  I cut 3mm plywood easily. 7mm plywood is doable with a couple of slow passes.  I imagine that 6mm MDF would be cutable.  Engraving of any thickness material is doable of course.


---
*Imported from [Google+](https://plus.google.com/105892779133697678832/posts/7mfTBAhiUJz) &mdash; content and formatting may not be reliable*
