---
layout: post
title: "Ok, I have a question.. I have a 1000x1000mm CNC router setup and I want to do a laser setup also on the machine"
date: January 25, 2016 23:04
category: "Discussion"
author: "Custom Creations"
---
Ok, I have a question.. I have a 1000x1000mm CNC router setup and I want to do a laser setup also on the machine. My question is: Should I purchase the K40 laser setup or should I go with the JTech diode setup? Based on your opinion, please give me pros and cons of both options.



TIA for your help!





**"Custom Creations"**

---
---
**Anthony Bolgar** *January 25, 2016 23:46*

Personally I am against laser diode setups because of the safety issues. To make using a diode safe, you would need to build an enclosure around your existing machine, set it up with door safety interlocks. By the time you make it safe to operate, you have spent more than a 40W CO2 sells for. And a Co2 laser is much more versatile, not only can it engrave, but it can cut some fairly thick materials in one pass.




---
**jacob aikey** *January 27, 2016 03:02*

I was in your shoes a few years ago; I started with a 3watt green laser diode set up and it pretty much sucked at marking everything, then I got a 5watt blue diode and it marked wood and dark plastics and other such things, after some advice I went to a 3watt 808 it diode and it was 10 times better at marking dark objects. Now after all that and if you don't know, setting up a liquid cooled diode system can break 500 bucks easy. I bought the k40, it was under 400 bucks delivered to Pennsylvania. After just 5 minutes with this thing I though why didn't I just buy this first. It etches and cuts most non metallic materials up to 4mm thick. It also does a nice job on anodized aluminum.  Bang for buck the co2 k40 is the way to go. And it is easy to upgrade to a larger platform if needed. 


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/9wMsVWKyRdm) &mdash; content and formatting may not be reliable*
