---
layout: post
title: "I have a question, hoping some one can answer me"
date: October 29, 2018 11:16
category: "Discussion"
author: "data357"
---
I have a question, hoping some one can answer me.  I will be buying a k40 in the next few weeks in Australia.  Tired of my 5 watt led.  I see 2 versions on ebay here so far.  1 that has an old style mili amp needle meter (Looks like the unit shown on this site)  and another that has a digital one.  Both about the same price.  Any suggestions on which unit is better and I don't mean the mili amp meter itself.  The one with the needle has a rotary dial for amperage adjustment and only a couple of other switches.  The digital one has press bubble buttons by the look of it for adjustment.  I would probably prefer the rotary dial one with the needle more old school and less issues.  SO any comments on either of these.



Thank You

Terry





**"data357"**

---
---
**Don Kleinschnitz Jr.** *October 29, 2018 13:30*

This may help in your decision. These machines are hard to operate properly without a current meter: [https://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html](https://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html)


---
**Joe Alexander** *October 29, 2018 14:15*

As don said its ideal to have the analog milliamp meter in line regardless and several of those digital panels have gone "wonky" on their owners, being unreliable in its power levels and not showing the actual tube output. So go for the old school one with the pot and needle gauge, its tried and true :)


---
**Stephane Buisson** *October 29, 2018 17:22*

+1 for old school, especially if you change your controller board.


---
**Printin Addiction** *October 30, 2018 10:03*

The digital panel is very flaky, you hit +.1 and it will sometimes  jump 1 or 10.  


---
*Imported from [Google+](https://plus.google.com/107491746550616523444/posts/Zg93fd8EjhE) &mdash; content and formatting may not be reliable*
