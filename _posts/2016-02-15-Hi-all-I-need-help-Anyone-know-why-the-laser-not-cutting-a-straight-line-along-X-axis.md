---
layout: post
title: "Hi all. I need help. Anyone know why the laser not cutting a straight line along X-axis"
date: February 15, 2016 18:20
category: "Discussion"
author: "Najat Hamdan"
---
Hi all. I need help. Anyone know why the laser not cutting a straight line along X-axis. It becomes like 'ocean' or 'wave'. 



Thank you for your kind response! :)

![images/7910c2e77d8bba46f43fb84c5798414f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7910c2e77d8bba46f43fb84c5798414f.jpeg)



**"Najat Hamdan"**

---
---
**3D Laser** *February 15, 2016 18:37*

Have you to make sure your bed is square 


---
**Najat Hamdan** *February 15, 2016 18:42*

Can you explain further? 



I just figure out that the wheel at the laser cutter head is melting due to fire yesterday. 


---
**3D Laser** *February 15, 2016 18:50*

I'm new to this myself but if your bed is bent it could be causing the laser not to cut in a straight line in in the process of squaring my bed now I had to take it all apart and make sure all the rails where straight.  I would check to make sure your stepper motors have no bends in them


---
**Najat Hamdan** *February 15, 2016 19:08*

My problem is different from yours. Bed is okay but the wheel (the white colour) that rolling the laser head not moving smooth. And it make sound like train. 


---
**3D Laser** *February 15, 2016 19:18*

Sounds like the wheels need to be replaced light objects sells them of a few bucks a piece [http://www.lightobject.com/DK40-upgrade-parts-C68.aspx](http://www.lightobject.com/DK40-upgrade-parts-C68.aspx) not for sure which one it would be but this is there k40 upgrade page you will see them there 


---
**Scott Marshall** *February 16, 2016 04:35*

[http://www.ebay.com/itm/4pc-40W-CO2-Laser-Rubber-Engraver-Shenhui-K40-Head-Mount-Carriage-Wheel-Roller-/262227013983](http://www.ebay.com/itm/4pc-40W-CO2-Laser-Rubber-Engraver-Shenhui-K40-Head-Mount-Carriage-Wheel-Roller-/262227013983)


---
**Najat Hamdan** *February 16, 2016 05:23*

Wow! Thank you for your kind response. I try to contact the person who i bought the machine before.  Hopefully the problem will solved. I'll update from time to time so that might be informative for others when the problem encounter to them.


---
*Imported from [Google+](https://plus.google.com/115934885983734426699/posts/JdoHVbFUhDz) &mdash; content and formatting may not be reliable*
