---
layout: post
title: "Today's project: building the table and relocating the machine"
date: June 19, 2016 02:15
category: "Smoothieboard Modification"
author: "Ray Kholodovsky (Cohesion3D)"
---
Today's project: building the table and relocating the machine. 

Tomorrow, hopefully, I set up the exhaust to that window. 

FYI the machine already runs on smoothie. I did that the first full day I had with it. I count delivery as day 0 and all I did was unpack and test fire it. 

![images/57e53b7cf3802f443f7d3192bba88a19.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57e53b7cf3802f443f7d3192bba88a19.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Evan Fosmark** *June 19, 2016 17:48*

Where'd you find the nice sized table top? Been looking to build a larger bench but I want something about 30" deep and >6 feet wide. Haven't found anything at Home Depot.


---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2016 17:50*

That's a 24x48 mdf sheet. The frame is 2' deep but a bit wider by 4" so the right most tabletop is just an extra slab of mdf I had lying around. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/9HHigSGURsb) &mdash; content and formatting may not be reliable*
