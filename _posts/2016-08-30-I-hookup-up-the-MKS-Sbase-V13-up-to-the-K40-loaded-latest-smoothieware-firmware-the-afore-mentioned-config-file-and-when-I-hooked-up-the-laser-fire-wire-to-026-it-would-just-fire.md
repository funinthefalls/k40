---
layout: post
title: "I hookup up the MKS Sbase V1.3 up to the K40, loaded latest smoothieware firmware, the afore mentioned config file, and when I hooked up the laser fire wire to 0.26 it would just fire"
date: August 30, 2016 02:26
category: "Smoothieboard Modification"
author: "Robi Akerley-McKee"
---
I hookup up the MKS Sbase V1.3 up to the K40, loaded latest smoothieware firmware, the afore mentioned config file, and when I hooked up the laser fire wire to 0.26 it would just fire.  wouldn't turn off.. so I have a bit of work to do there.  I might need to make it an open drain instead.  Wednesday I'll work on it again.  Laserweb3 worked fine.





**"Robi Akerley-McKee"**

---
---
**Piotr Kwidzinski** *August 30, 2016 02:58*

**+Robi Akerley-McKee** - if you used config file posted by **+Ariel Yahni** he is using pin 2.5 and not 2.6. Look at line 123 in original file with laser_module_pin and update accordingly.




---
**Alex Krause** *August 30, 2016 03:01*

Did you try to invert the pin with ! In the config file


---
**Jon Bruno** *August 30, 2016 17:07*

I know the config he's talking about but I don't remember if it's Ariels or not, but I tried to use it once.



This config  does use 0.26!^



Unfortunately the file in question is slightly incompatible and actually quite a bit reckless to use with a stock K40.



He's defined the drive currents much too high for the K40 LPSU to handle.

(~1.5A)



IIRC his configuration was setup with external drivers and a separate PSU for the steppers.



Actually thinking back now, I do believe it was Ariel that provided the file initially.



His AZSMZ config doesn't happily jive with the Sbase board.

That file probably shouldn't be used as a base example for this device.



I believe for the MKS Sbase 1.3 the best method is definitely the 2 wire setup.

I run my PWM for laser power via level shifter on 1.23 and my laser enable on 2.5 with D14 removed.



laser_module_pin  1.23

switch.laserfire.output_pin 2.5^  #<-<s>-</s> Notice there is no "!"



Some may chose to disagree, But I've been through plenty of testing and 2 power supplies to determine that those settings provided in his config aren't a healthy MKS Sbase 1.3 choice.



For me the one wire setup just doesn't (hasn't) work(ed) properly on this board.


---
*Imported from [Google+](https://plus.google.com/103866996421752978311/posts/J5UjQ6NrHYo) &mdash; content and formatting may not be reliable*
