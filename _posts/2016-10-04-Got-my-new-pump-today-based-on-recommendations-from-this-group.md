---
layout: post
title: "Got my new pump today based on recommendations from this group"
date: October 04, 2016 01:20
category: "Air Assist"
author: "Don Kleinschnitz Jr."
---
Got my new pump today based on recommendations from this group.



I am glad that I went one size up from my original choice.



At power up it seems like a good replacement for my HF airbrush pump.



First off it has a rational fitting so I can get rid of my jury rigged connectors.



It has rubber feat and its pretty quiet it will be silent compared  to my 440CFM evacuation fan.



I also got the drag chain recommended and I wish I would have not wasted time with the coily cord approach.



[https://www.amazon.com/gp/product/B002JLGJVM/ref=oh_aui_detailpage_o00_s00?ie=UTF8&th=1](https://www.amazon.com/gp/product/B002JLGJVM/ref=oh_aui_detailpage_o00_s00?ie=UTF8&th=1)



[https://www.amazon.com/gp/product/B00880AVL2/ref=oh_aui_detailpage_o00_s01?ie=UTF8&th=1](https://www.amazon.com/gp/product/B00880AVL2/ref=oh_aui_detailpage_o00_s01?ie=UTF8&th=1)




**Video content missing for image https://lh3.googleusercontent.com/-ArIju6bO1ow/V_MD5rBHxvI/AAAAAAAAeg4/EuuoVQxN6bUjmJNGsgobySHklqkLcb62gCJoC/s0/20161003_180356.mp4.gif**
![images/233fa182e362e6f454169fd680a6f6f1.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/233fa182e362e6f454169fd680a6f6f1.gif)



**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *October 04, 2016 03:00*

That's the one I got. 


---
**Bill Keeter** *October 04, 2016 03:46*

is it really that quiet? If so that's going in my shopping cart.


---
**Ariel Yahni (UniKpty)** *October 04, 2016 03:50*

It's super quiet


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 04, 2016 06:48*

Wow, that's ridiculously quiet. Would almost not notice if it is switched on or off lol. Another thing for the list of "to buy sometime".


---
**Rodney Huckstadt** *October 04, 2016 22:07*

I 3d printed my own drag chain, love the 3d printer, total cost about $3 in materials


---
**Bill Keeter** *October 16, 2016 18:56*

**+Don Kleinschnitz** was it the 1300 gph you purchased? Any update how well it's running in your machine? i'm about to add this to my Amazon wishlist.


---
**Don Kleinschnitz Jr.** *October 16, 2016 20:44*

**+Bill Keeter** yes the one posted above. I haven't cut with it yet machine is down for conversion.


---
**Bentley Davis** *May 03, 2018 13:06*

What's the minimum GPH recommended for the air assist?


---
**Don Kleinschnitz Jr.** *May 03, 2018 17:24*

**+Bentley Davis** it kinda depends on speed, materials being cut and your plumbing. I previously had an airbrush pump but replaced it do to noise.

I am using 1300 which provides  plenty of air. I use a valve to control the volume.

I have seen folks even using computer fans mounted on the head. 

 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/RX7bTiwptdS) &mdash; content and formatting may not be reliable*
