---
layout: post
title: "I'm thinking about buying one of this machines maybe from eBay"
date: September 19, 2016 11:19
category: "Discussion"
author: "James Farrow"
---
I'm thinking about buying one of this machines maybe from eBay. What are people's past experience with the machines, purchase process, and shipping. Thanks! 😀





**"James Farrow"**

---
---
**Robert Selvey** *September 19, 2016 11:28*

The one I got it from on ebay came all in great shape and it was the newer version not the analog.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 19, 2016 12:43*

Mine came in a lot better condition than some people's have. I basically had nothing wrong with mine except a minor alignment issue, whilst some people have had dented/bent out of shape, major issues. But, the sellers are usually able to be persuaded to give a decent sized refund if anything is in bad condition, so I would take the plunge again. Shipping on mine was from a national stockist (i.e. in same country) & took a few days by courier to my house. Although, seeing it in the back of the courier truck made me wonder about what condition it would be in (it had "this way up" stickers which seemed to be pointing to the wall of the truck).


---
**greg greene** *September 19, 2016 13:01*

I got mine from a US Seller, I figured they bought them in container lots, so they were well packed and protected on the voyage over.  The journey from them to me was a good one as it arrived almost in alignment and no issues.


---
**James Farrow** *September 19, 2016 19:38*

I'm wondering about the condition that it will arrive in and the shipping cost for it to be sent to New Zealand.


---
**James Farrow** *September 19, 2016 19:44*

How about this one. Is this the one that everyone is using?

[ebay.com - Details about  High Precise 40W CO2 USB Laser Engraving Cutting Machine Engraver Wood Cutter](http://m.ebay.com/itm/200902245694?_mwBanner=1)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 19, 2016 22:05*

**+James Farrow** That looks to be the same as mine. I'd have a look around a bit & see if you can find an ebay seller that ships from NZ, as there may be one there. Alternatively, try one of the Australian sellers, since we're just over the water & less chance of damage in freight if less distance.


---
*Imported from [Google+](https://plus.google.com/+JamesFarrowtheAMAZING/posts/hjCZ71WBAZV) &mdash; content and formatting may not be reliable*
