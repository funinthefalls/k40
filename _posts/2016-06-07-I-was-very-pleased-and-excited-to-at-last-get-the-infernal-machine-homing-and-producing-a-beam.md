---
layout: post
title: "I was very pleased and excited to at last get the infernal machine homing and producing a beam"
date: June 07, 2016 09:48
category: "Original software and hardware issues"
author: "Stephen Smith"
---
I was very pleased and excited to at last get the infernal machine homing and producing a beam. Now my problem is probably being caused by myself. I have a small shape which needs to be cut out and in the space left I need to inset another piece of material and engrave it. The machine will cut out the shape and will also engrave but never in the same place. I need to engrave a large number of different texts within the cutout shape but just cannot get it to register. Any suggestions greatly appreciated.





**"Stephen Smith"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 07, 2016 10:00*

Hi Stephen. If you are using CorelDraw/CorelLaser, then first step is to make sure on your engrave/cut screen the Do Not Back option is ticked.



That will mean it always starts a new task at the same spot it started the previous task (refer-X & refer-Y).



Next, when creating your files to engrave, you need to set a reference point for the top-left. A lot of people have used 1px dot in the top left corner. That way each layer/task can be referenced off that dot.



I've found that an easier option (for me at least) is to create a rectangle the size of the material (e.g. 300x200mm) then position whatever it is I want to cut/engrave in the rectangle. Then make sure the rectangle has no fill & no border, then select the rectangle + whatever I want to cut/engrave. As the rectangle has no fill/border it will not engrave/cut, however it's existence is registered by CorelLaser & as such it aligns the objects to be engraved/cut correctly.



Then, if you cut your shape, you can flip it (or place another in there) & engrave away, provided you have the 2nd task aligned in the same position on a no fill/border rectangle of the same size as the first task.



Hope that makes sense & helps.


---
**HalfNormal** *June 07, 2016 14:40*

I use white and black for referece. Black cuts and white does not. If I make a white box one pixel smaller than the cut box I can engrave anywhere inside the box with the same reference as the cut box.


---
**Stephen Smith** *June 07, 2016 16:02*

Many thanks, in essence that is exactly what I have been trying to do but I thought that having set the work area to 300x200 the softare/machine would understand the positioning. I will give it another go tomorrow with a 300x200 box without borders or fill.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 07, 2016 23:15*

**+HalfNormal** I tried a white bordered rectangle previously & it did cut when I sent it to CorelLaser. Do you mean white fill? I haven't tried that.


---
**HalfNormal** *June 07, 2016 23:43*

**+Yuusuf Sallahuddin**​ yes white fill.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 08, 2016 01:39*

**+HalfNormal** I'll have to give that a try then. Thanks.


---
**Pippins McGee** *June 08, 2016 12:50*

Funny this has come up today.

I am trying to work out a similar/same  issue last night when trying to engrave something then cut it out precisely .



My CorelDraw doc consisted of 2 layers. One with bitmap version of my image and one with vector outline of it.

Both resized within CorelDraw to have the exact same dimensions as each other.

Visually one could be placed on top of the other and the outline and the engrave line up perfectly to the pixel.



Engrave the bitmap. CorelLaser tells the X and Y refer points and a Preview. Ok.

Cut the vector. CorelLaser tells the exact same X and Y refer points also, but it cuts several MM off from where it should be cutting around the engrave.



I managed to achieve a cut around the engrave in the end , but I don't think it is the correct solution at all.



In CorelDraw you select an object and it tells you the dimensions of it along the toolbar (+ you can change it).

From within the doc. both the vector outline and the bitmap of my image were set with the exact same dimensions.



However in the CorelLaser 'preview' screen (when you over your mouse over the object in the preview) it tells you the dimensions, and it gave different dimensions to what was set in the coreldraw doc (several MM different)

Not only that, but the dimensions in the preview were different between the engrave and the cut...



I figured the preview is the most important part for the cut and engrave to line up and have the same dimensions, so back in the coreldraw doc, I had to make the dimensions of each object different to each other (changing the dimensions of each object back and forth a million times, checking the CorelLaser preview screen in between) until I found a combination of dimensions that resulted in both objects having the same dimensions when hovering mouse over it in the preview.



Only then (when the preview screen of the cut AND engrave, had not only the same X and Y refer points, but also the same dimensions in the 'preview' window) did it cut around the engrave properly.

(even though the x and Y refer points were the same as each other the whole time even from the beginning)



...i'll figure it out someday.. but maybe this is of help to you and someone can advise on why it happens.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 08, 2016 13:37*

**+Pippins McGee** Do you have your settings correct in the "CorelDraw Settings" dialog of CorelLaser? Should be the first icon on the toolbar for CorelLaser. These settings control how CorelDraw parses info to CorelLaser.



Check this: [https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)



You want them both set to WMF. If they're not, that could be the cause of your issues.


---
**Pippins McGee** *June 08, 2016 15:07*

**+Yuusuf Sallahuddin** 

hmmmmmm thanks for your response.

engraving was set to WMF but cutting was set to PLT

I figured I was probably just going about things the wrong way to achieve a cut around an engrave precisely.. either that or the software/machine was just being dodgy.

but perhaps it is just a setting like this causing the difference between cut and engrave...



ill change them both to wmf and have another crack at it in the morning.

cheers!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 08, 2016 15:22*

**+Pippins McGee** Hopefully that solves it. If not, post back any new info regarding what happens & we'll get it sorted out.


---
**Pippins McGee** *June 09, 2016 12:33*

**+Yuusuf Sallahuddin** it seems to have solved it... thanks alot Yuusuf. haha.

How did you know about that setting?

If only I knew what they meant it would make more sense haha.

Those are the little things I wonder how anyone could possibly know..


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 09, 2016 15:19*

**+Pippins McGee** I saw a youtube video early on when I got my k40 if I recall correct. A lot of what I know works is purely from trial & error (lots of them) & from random things I remember seeing/reading from other people's experiences.


---
*Imported from [Google+](https://plus.google.com/111326322372516669266/posts/7XtBQqTA8ZT) &mdash; content and formatting may not be reliable*
