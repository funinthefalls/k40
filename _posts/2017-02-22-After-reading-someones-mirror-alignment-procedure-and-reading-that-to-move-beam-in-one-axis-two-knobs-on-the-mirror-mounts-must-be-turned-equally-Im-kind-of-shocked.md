---
layout: post
title: "After reading someone's mirror alignment procedure and reading that to move beam in one axis, two knobs on the mirror mounts must be turned equally, I'm kind of shocked"
date: February 22, 2017 20:23
category: "Discussion"
author: "Lance Ward"
---
After reading someone's mirror alignment procedure and reading that to move beam in one axis, two knobs on the mirror mounts must be turned equally, I'm kind of shocked.  Real kinematic mounts have two adjusting knobs and a fixed pivot point.  These three points define a plane and the two adjustments provide for precise X and Y adjustments with no crossover.



Has anyone replaced the stock, Chinese mirror mounts with real kinematic mounts like the Newport MM-2A?  They're not that expensive.  The Chinese clone mounts would be even less.



I haven't received my K40 yet but this seems like a potential major improvement in the alignment process.





**"Lance Ward"**

---
---
**Paul de Groot** *February 22, 2017 20:28*

Well it is a very cheap machine and be prepared for many more surprises. Check the electronic ground. Wait till you see the chinese software. 😂 someone mentioned that this looks like a project put together by chinese students. 


---
**Ariel Yahni (UniKpty)** *February 22, 2017 20:31*

**+Paul de Groot**​ completely true


---
**Don Kleinschnitz Jr.** *February 22, 2017 20:43*

**+Lance Ward** I want to upgrade but have not found suitably priced and sized mounts. Do you have a source?

Aren't these mounts like 2" with a large hole? 


---
**Lance Ward** *February 22, 2017 21:06*

**+Don Kleinschnitz**  Yes, MM-2A is 2"x2" mount with a 30mm hole for mounting optic.  I have a couple here and will see what it takes to make it work when the machine gets here.  There's quite a few US made mounts on ebay for $30.-50. each.  I've checked out a few on Alibaba that look dirt cheap but not sure about buying from them.  [login.alibaba.com - Alibaba Manufacturer Directory - Suppliers, Manufacturers, Exporters &amp; Importers](https://www.alibaba.com/product-detail/High-Stability-Kinematic-Mount-lens-mount_929381198.html)


---
**Don Kleinschnitz Jr.** *February 22, 2017 21:11*

**+Lance Ward** I looked at this one:



[http://www.ebay.com/itm/co2-laser-reflection-reflective-lens-mirror-housing-holder-fixture-mount-25mm/322130664221?hash=item4b007bdb1d](http://www.ebay.com/itm/co2-laser-reflection-reflective-lens-mirror-housing-holder-fixture-mount-25mm/322130664221?hash=item4b007bdb1d)



And concluded that the entire optical path would have to be moved up to accommodate. I will be interested in your conclusions.


---
**Lance Ward** *February 22, 2017 21:22*

The translation mount could be eliminated I think.  I'm thinking that the mirror is big enough that you could fix the mirror in optimal position and just adjust beam to translate across the mirror face as needed.  But that price is awesome!  I'll have a good look when the machine gets here.


---
**greg greene** *February 22, 2017 21:36*

Let us know, they do appear to be a better holder - if they fit


---
**Paul de Groot** *February 22, 2017 21:37*

**+Lance Ward** this is the fun part of the machine. You get to feed your inner nerd☺


---
**greg greene** *February 22, 2017 21:39*

Always Hungry !!!!


---
**Abe Fouhy** *June 09, 2017 20:30*

**+Lance Ward**​ did you ever get the kinematic mounts?


---
**Lance Ward** *June 09, 2017 21:38*

I have one MM-2A mount that at some point, I intend to experiment with.  Currently, I'm using the stock mounts and they work nominally as-is and by adjusting only 2 of 3 knobs.  I guess I'm not sure of reasoning behind the three adjustments per mirror as the adjustment at the apex only seems to facilitate translating the plane of mirror slightly in Z-axis when adjusted equally with knobs at the other vertices.   This would put beam in a slightly different position on mirror face but not sure why that would be desirable if the beam geometry is intended to be fixed at 90 degree angles?


---
**Abe Fouhy** *June 10, 2017 07:45*

Yea, I don't get it either, when I first started I jacked ask three around making it a huge PITA to get right again. How could the MM-2As help with alignment? I never used kinematics before. 


---
*Imported from [Google+](https://plus.google.com/114422684423642232545/posts/TwFbTNmwLBW) &mdash; content and formatting may not be reliable*
