---
layout: post
title: "I have decided to do some testing to once and for all answer the question of what is the max mA to run a K40 tube at to extend its life"
date: January 27, 2017 23:46
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
I have decided to do some testing to once and for all answer the question of what is the max mA to run a K40 tube at to extend its life. I have 2 identical K40's with new (under 10 hour) tubes in them. I will run all my gcode files on one keeping the mA under 15 and the other running at 25mA. I will keep track of the distance and time cut/engraved, and make a comparison once on of the tubes degrades. This may take months, or possibly years, but I reall want to see what is the best way to run things. My thought is that the reduction in life (if there is one) will be made up for by the amount or distance of material cut/engraved.





**"Anthony Bolgar"**

---
---
**Phillip Conroy** *January 28, 2017 00:24*

Do you have a laser beam power meter to measure tube wattage output? As without one it will be hard to measure tube power loose. My tube when from 40 watts to 14 watts reallyquick at aaroundd 600 hours of cutting 3mm   mdf at 7.5mm a sec and mainly 10ma power.one day it was cutti g fine a d the next day would not cut ccompletely tthrough , l belive it was more to do with the level of gas in the tube than tube worn out  ,the power suddenly ddroppedto 14 watts aAcordg to my laser power tester 


---
**Phillip Conroy** *January 28, 2017 00:34*

Forgot to add tube water tank range was 10 deg c to 20 deg c and cut for 3hours at a time than rested tube for a hour before cutting again. Tube was in the k40 for 18 months and 2 weeks earlier measured beam at 37 watts ,l had not notice tube was on its way out......... I did not have zaspare tube as if tube stored for 6 months it will have lost gasses in that time .

iIooldbe iinterested in hearing from other k40 oowners with power meters on how long their tube last............ 


---
**Anthony Bolgar** *January 28, 2017 00:43*

I will be getting a power meter from bell lasers to aid in the testing. Water weill run from a common resevoir so temps should be identical.


---
**Anthony Bolgar** *January 28, 2017 00:46*

If you want to help with the cost my link is [paypal.me - Pay Niagara Clock using PayPal.Me](https://www.paypal.me/NiagaraClock)


---
**HP Persson** *January 28, 2017 11:37*

That´s a test i want to see :) I´ll chip in some more after the weekends when i get my card back :)


---
**Anthony Bolgar** *January 28, 2017 12:59*

Thanks HP


---
**Don Kleinschnitz Jr.** *January 28, 2017 21:01*

**+Anthony Bolgar** I'm sure you know this but some things to consider that I dug out thinking about this:



Sources of laser failure:



1. Leak down over time (will even go dead on shelf) 

2. Heat (proportional to the current). 

3. Tube temp range (proportional to the cooling and current)



I wonder if  it would be better to run the two tubes continuously at different currents and stable temps to try and get two points on the curve. Running pulsed may introduce a lot of error in the model due to the effect of various duty cycles. From a constant power you may be able can adjust for duty cycles in actual operation.



Still only two points on a curve but at least its some data. 



Most tubes are warranted for 3,000-10,000 hrs at 18ma. 



This video may be useful to your testing: 
{% include youtubePlayer.html id="asM0s7wm4AM" %}
[youtube.com - RDWorks Learning Lab 40 Laser Output Power v Temperature](https://www.youtube.com/watch?v=asM0s7wm4AM)


---
**Anthony Bolgar** *January 28, 2017 22:07*

Thanks for the link **+Don Kleinschnitz**


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/5jTW6ddnRGi) &mdash; content and formatting may not be reliable*
