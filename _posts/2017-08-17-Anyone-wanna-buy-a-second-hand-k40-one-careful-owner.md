---
layout: post
title: "Anyone wanna buy a second hand k40, one careful owner?"
date: August 17, 2017 14:41
category: "Discussion"
author: "photomishdan"
---
Anyone wanna buy a second hand k40, one careful owner? Slight signs of use. 



![images/625d179dc23b51f606d8665abe505a0c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/625d179dc23b51f606d8665abe505a0c.jpeg)
![images/2f6065376ede76bbcc41f44303fc1e97.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f6065376ede76bbcc41f44303fc1e97.jpeg)

**"photomishdan"**

---
---
**Chris Hurley** *August 17, 2017 14:50*

What happened? 


---
**photomishdan** *August 17, 2017 15:02*

**+Chris Hurley** I wish I knew, turned my back and flames & toxic fumes. I have it in a log cabin and it was moments away from taking the whole thing. PSA keep a CO2 fire extinguisher on hand!


---
**Nate Caine** *August 17, 2017 15:08*

Was the machine running at the time?  What were you cutting?  I'm surprised at the external heat damage:  melted exhaust hose and melted front panel and wiring harness.  Very hot.


---
**photomishdan** *August 17, 2017 15:13*

**+Nate Caine** was cutting 3mm sparkly gold acrylic at the time. I walked out of the workshop and by the time I turned around it was ablaze! Very scary. I was only just starting to get used to it as well! I had two extractors on the unit, one stock and another inline and they've both been destroyed. The only thing that survived was the kettle lead! lol 


---
**Don Kleinschnitz Jr.** *August 17, 2017 15:25*

#K40Fire

Man that got hot. Note to self: start work on a fire detector .... kinda hard to do with an InfaRed laser in there!


---
**Nate Caine** *August 17, 2017 15:31*

On the commercial machine (that I taught classes on, not a K40) we upgraded the exhaust hose to a metal dryer hose.  That way a stray ember is contained in the exhaust system (rather than melting thru a plastic hose).  In your fire did the entire orange hood window melt thru into the fire?  Quite dramatic and scary.  Glad no one was hurt.


---
**photomishdan** *August 17, 2017 15:34*

**+Nate Caine** yeah, the whole orange window melted, not a sign of it left! I might be able to salvage the nema17s, control board and power unit. But I'm a little annoyed right now. I think it's time for a beer, that should clear the toxic fumes from my throat!


---
**Jim Fong** *August 17, 2017 16:48*

Wow I'm sorry this happened to you. I've heard that cutting plastics with laser can be quite combustible. Was the air assist on?



I have a smoke detector right above the laser and two fire extinguishers within arms reach.  





This could be something to put inside the case if there is enough room. 



[http://www.swiftfireprotection.com/Products/Fire_Extinguisher_Sales/Fire_Extinguishers/stovetop/stovetop.html](http://www.swiftfireprotection.com/Products/Fire_Extinguisher_Sales/Fire_Extinguishers/stovetop/stovetop.html)


---
**Steve Clark** *August 17, 2017 17:52*

A couple of questions, did you have air assist? The 3mm sparkly gold acrylic, was it cast acrylic? Also do you suspect the glitter may have contributed to the fire? 



My gut tells me that it may be part of the blame in reflecting the beam or even deflecting the vaporized plastic flow from going down and out of the beam flow path.



I have an after market exhaust blower and I noticed that too high of air extraction with plastic encourages flaming so I added a speed control to slow the draw. 



As some have said here about the air assist, it tends to blow the flame out along with keeping the lens clean.



I've seen some air assist with long thin nozzles right at the cut point to better help in clearing the vapors when plastic cutting



I have a ABC Fire extinguisher close by my machine but now you got me looking for a small Halon or CO2 as the first option with the ABC as backup.   I see some "old stock" small Halon out of Arizona for 30 or 40 bucks on ebay.



I could see mounting one direct and just hit the lever. I wouldn't want to open door and add more oxygen to the flame. Also the blower is going to have to be shut down at the same time so maybe a switch setup to do both...or a front main  



Sorry that happened to you! Lost your laser tube to freezing  a time back and now this...


---
**photomishdan** *August 17, 2017 18:02*

**+Steve Clark** I haven't had much luck have I? I have cut the 'glittery' acrylic before and it seemed ok. I had an air assist (3D printed nozzle) and it had always worked well to extinguish small flames in the past when cutting plywood. I've never had any flames when cutting acrylic before, so I would say that the added glitter was most certainly an attributing factor. Maybe having two extractor fans inline was too much, but i felt like the cabinet didn't have a huge inlet, I hadn't modified any extra holes.



I'm debating wether to replace it now or cut my losses, the newer models now with digital power display look interesting. But looking into any other alternatives. I was in the middle of starting to make acrylic jewellery, but this has stopped that for now.


---
**Steve Clark** *August 17, 2017 19:05*

**+Don Kleinschnitz**  I'm wondering if you could set up a thermocouple/s and use a Arduino or other device to monitor the temperature inside to give an alarm? 


---
**Don Kleinschnitz Jr.** *August 17, 2017 20:27*

**+Steve Clark** its on my list to try that or a PIR 

but I wonder if after its adjusted to ignore the normal IR would it be to slow to respond to react soon enough to prevent this level of damage.


---
**Steve Clark** *August 17, 2017 21:14*

I have cheap one I can test but I think the response time is quite fast. 

I've put my fingers on the probe before and it will climb pretty fast. I'll do it again with a close flame and see what I get.


---
**Don Kleinschnitz Jr.** *August 17, 2017 22:47*

**+Steve Clark** if it's a PIR the question is if the it laser gives false positives.

If it's a thermocouple where do you place it in the box so that by the time it detects the internals have melted or started a secondary fire.

Interested in your tests....


---
**Steve Clark** *August 18, 2017 01:09*

**+Don Kleinschnitz**  Sorry, I wasn't clear on the what I was going to test. It is a thermocouple  I had.  A cheap one like this:



[http://www.ebay.com/itm/Fahrenheit-Celsius-Digital-Thermometer-Temperature-Meter-Gauge-C-F-PC-MOD-Indoor-/271757785983?hash=item3f4606bb7f:g:N20AAOSwNSxUyjcP](http://www.ebay.com/itm/Fahrenheit-Celsius-Digital-Thermometer-Temperature-Meter-Gauge-C-F-PC-MOD-Indoor-/271757785983?hash=item3f4606bb7f:g:N20AAOSwNSxUyjcP)



Here are the results, which I think are positive. With a digital hot air gun set at 125C and 2 inches away the temperature rose from 29C to 50C in just under 15 sec. 



The test done with a butane hand torch and flame off to one side about an inch and two inches from the flame tip. 5 to 7 seconds from 30C to 50C .



The  butane torch with the flame tip swiping slowly over the thermocouple body 3 seconds from 28C to 70C plus. (actually went up to 100C before dropping back)



I'd use something like this for a real unit maybe up on the top edge of the window. More of them if there is a way to wire them.



[lightobject.com - 3ft (1M) K Type Thermocouple](http://www.lightobject.com/3ft-1M-K-Type-Thermocouple-P113.aspx)



 


---
**Don Kleinschnitz Jr.** *August 18, 2017 02:38*

**+Steve Clark** that sounds encouraging, we should put it in series with to the interlocks?


---
**Paul de Groot** *August 18, 2017 06:50*

**+Steve Clark** **+Anthony Bolgar**​ id working to build something like that. So watch him!


---
**William Steele** *August 18, 2017 19:11*

By chance, did your air assist fall off and get in the laser beam path?  If it was only a press fit it's quite possible that was the root cause.




---
**photomishdan** *August 18, 2017 19:18*

**+William Steele** quite possibly. The irony of it all is that I'm currently waiting for a dedicated air assist head to come through the post! That'll have to go into eBay when I receive that. Looks like I'm going to replace with either a 50w or 60w unit instead, the limitations of the bed size of the k40 and the need to modify basically every aspect of the machine made me come to the conclusion that it's best for me to buy a unit that works properly off the shelf. 


---
**Steve Clark** *August 20, 2017 03:57*

**+Don Kleinschnitz**  I haven't forgotten you Don just still thinking about it. I have some ideas.


---
**Don Kleinschnitz Jr.** *August 20, 2017 15:20*

**+Steve Clark** OK lets do a separate thread.


---
**crispin soFat!** *August 22, 2017 03:54*

YIKES!!


---
**photomishdan** *August 22, 2017 10:08*

Oh good, this just arrived from china! ![images/ec2ddf5d849d3c3c4e7db29e7c7f7baa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec2ddf5d849d3c3c4e7db29e7c7f7baa.jpeg)


---
**William Steele** *August 22, 2017 10:10*

I love irony!


---
**William Steele** *August 22, 2017 10:11*

Wanna sell it? :-)


---
**photomishdan** *August 22, 2017 10:18*

**+William Steele** yes. I'll do it cheap and fast for UK delivery. 


---
**William Steele** *August 22, 2017 10:25*

Ah... I'm in the US.


---
**Padilla's custom leather** *September 10, 2017 23:18*

ya wanna sell the original mounting bracket for the head?


---
*Imported from [Google+](https://plus.google.com/105674876465839652573/posts/JfFcekiFAdT) &mdash; content and formatting may not be reliable*
