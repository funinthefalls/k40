---
layout: post
title: "Got my K40 today and can't for the life of me figure this one out"
date: June 13, 2018 02:59
category: "Hardware and Laser settings"
author: "Clayton Braun"
---
Got my K40 today and can't for the life of me figure this one out.  My X axis belt is really loose.  As you can see by my tests something is way off.  So I go and tighten the belt and the screws screw into the belt and the carriage can't even move! There has got to be a fix for this already posted, but I can't find it. Any help?



![images/b006a6aa1c7739b197f6aeb4393063f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b006a6aa1c7739b197f6aeb4393063f8.jpeg)
![images/b4329e7d4328e019e0802506228ada50.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b4329e7d4328e019e0802506228ada50.jpeg)
![images/cd3f5e3c39ae2f2132231139c119dca1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cd3f5e3c39ae2f2132231139c119dca1.jpeg)

**"Clayton Braun"**

---
---
**Fook INGSOC** *June 13, 2018 03:22*

...is your stepper motor energized while you are trying to move the carriage???....correct belt tension & de-energize stepper to move carriage and see if there is any play or slop in the belt!!!


---
**Printin Addiction** *June 13, 2018 09:14*

You can always cut the screws down a bit, or try to find shorter ones.


---
**Claudio Prezzi** *June 13, 2018 11:37*

Something like this could also happen when the mirrors are misalignend. Did you check the alignement?


---
**Clayton Braun** *June 13, 2018 21:53*

It was for sure the belt.  Thanks for the comments! lol I guess I knew the answer.  I couldn't find any short screws so I made due.

![images/b7565beb4dc7527a6fba7a81f2840d2e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7565beb4dc7527a6fba7a81f2840d2e.jpeg)


---
**Clayton Braun** *June 13, 2018 21:54*

![images/7ad6c956f96ce1189e72fda93824b684.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7ad6c956f96ce1189e72fda93824b684.jpeg)


---
**Fook INGSOC** *June 14, 2018 00:40*

Much Bettah!😁👍🏻


---
**Eric Lovejoy** *June 15, 2018 10:14*

Mine has holes on the front and back where you can insert an allen wrench to adjust the belt tension. If yours doesnt have those holes, you might be able to access the belt tension screws from around the gantry.


---
*Imported from [Google+](https://plus.google.com/+ClaytonBraun/posts/T5xpMUeshpU) &mdash; content and formatting may not be reliable*
