---
layout: post
title: "Paul de Groot made Hackaday!"
date: June 11, 2017 04:10
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
**+Paul de Groot** made Hackaday!

[http://hackaday.com/2017/06/10/drop-in-controller-for-ebay-k40-laser-engraver-gets-results/](http://hackaday.com/2017/06/10/drop-in-controller-for-ebay-k40-laser-engraver-gets-results/)





**"HalfNormal"**

---
---
**Paul de Groot** *June 11, 2017 11:29*

Thanks **+HalfNormal**​ that's amazing !😁


---
**HalfNormal** *June 11, 2017 16:31*

No, that's Awesome! 😆


---
**Paul de Groot** *June 11, 2017 18:53*

**+HalfNormal** 😂


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/2rDHV1hBvQw) &mdash; content and formatting may not be reliable*
