---
layout: post
title: "Well here's some random tests I did with Cat face, using the method suggested by Marc G from (although slightly modified as I am not needing to paint on leather)"
date: November 07, 2015 08:14
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Well here's some random tests I did with Cat face, using the method suggested by **+Marc G** from [http://www.instructables.com/id/Enhance-Your-Image-when-Reverse-Laser-Engraving-on/](http://www.instructables.com/id/Enhance-Your-Image-when-Reverse-Laser-Engraving-on/) (although slightly modified as I am not needing to paint on leather).



Very surprised & happy with the effect. Didn't bother doing full engrave of the design as I want to get it just right before I bother with that. Can't be bothered waiting for ages for it to finish (it took about 1hr for me to perform all these tests & modify files as I was going).



So I tested first on a piece of orange card (210gsm), then on a random scrap of 3mm thick natural vegetable tanned leather, and also part way through that job I paused & added a scrap of orange canvas that I had (which I've been meaning to test on for a bit now).



Settings were Engrave, 500mm/s, 3 pixel steps, 6mA power.



![images/725ce4aa58a3f1af672229adb1cc530d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/725ce4aa58a3f1af672229adb1cc530d.jpeg)
![images/f2654b691ede804fa3438655c987b537.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f2654b691ede804fa3438655c987b537.jpeg)
![images/180145f24e5b88588c300675c0ac158d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/180145f24e5b88588c300675c0ac158d.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**David Cook** *November 10, 2015 20:49*

Thank you for sharing your tests and results ﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 11, 2015 06:46*

**+David Cook** I'd prefer not to be shanked :P


---
**David Cook** *November 11, 2015 14:33*

**+Yuusuf Salahuddin** ha. A good preference indeed ;-)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/6AW7AHKepqi) &mdash; content and formatting may not be reliable*
