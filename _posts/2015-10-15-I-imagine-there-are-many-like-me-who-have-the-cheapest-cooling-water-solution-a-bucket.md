---
layout: post
title: "I imagine there are many like me who have the cheapest cooling water solution - a bucket"
date: October 15, 2015 05:41
category: "Hardware and Laser settings"
author: "Gregory J Smith"
---
I imagine there are many like me who have the cheapest cooling water solution - a bucket.  However as the weather warms up in Australia it's harder to keep the water below 20DegC (see below).  So now I keep a few drink bottles in the freezer where they turn to blocks of ice and then dunk them in the bucket on a rotation basis.  Cheap, easy, effective.



(This is below).  



Many will have noticed or heard that the power of the laser reduces with increased water temperature.  In my case I've found the difference between 20DegC and 25DegC to be about 15%.  I understand that the cooling water should be below 20DegC, and that you shouldn't run the laser if the water exceeds 30DegC.



I've done some reading reading on CO2 sealed tube lasers.  Apparently the CO2 gas is broken down by the laser action to CO and 02.  A catalyst is sealed inside the tube that acts to recombine the gases to make new CO2.  I understand this recombination needs a cool temperature and time.  It takes place in the spiral tube you see inside the main outer tube.  When the temperature is hotter, the recombination of gases is slower and so your CO2 level is reduced.



(Somebody please tell me if I'm on the right track or not - I'm not a laser expert but always interested to know how things work.)

![images/e0fb704da9c3faa6673847ec8433215a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e0fb704da9c3faa6673847ec8433215a.jpeg)



**"Gregory J Smith"**

---
---
**Phillip Conroy** *October 15, 2015 07:06*

I 2 tryed the frozen drink bottle trick ,however after researched water chillers for lazers -cw-3000 for around $400 found out it was just water and a computer water radator and fans,not refrigerated,so i tryed a computer water cooler and fans -was ok at around 25 deg,still not happy searched for a small refrigerated cooler and found a second hand under sink water chiller that is refrigerated and works great,and only cost $40 temps now  15 deg


---
**Stephane Buisson** *October 15, 2015 07:20*

do you have any algaes ?

what about a close loop of distilled water into a container (laser tube pipes in, air tight against evaporation ), this container in bucket fill with ordinary water for thermal exchange, other icy bottles from freezer to keep it cool ?


---
**Phillip Conroy** *October 15, 2015 11:36*

No algaes yet,thinking of running anti freeze car radator coolent


---
**Phillip Conroy** *October 15, 2015 11:37*

Since i have open contaner the coolent may smell


---
**Phillip Conroy** *October 15, 2015 11:38*

If i use car radator coolent


---
**Coherent** *October 15, 2015 12:47*

Get some RV antifreeze. Made to put in the RV water lines during storage. It's way cheaper than the auto antifreeze and it's not toxic (to you or pets). About $3-4 a gallon at Walmart.


---
**Phillip Conroy** *October 15, 2015 13:00*

Has anyone used car radator coolent in there laser,how did it go


---
**David Cook** *October 15, 2015 17:25*

Nice! I like the frozen drink bottle solution as it does not keep adding water volume like it would by adding ice directly.    I just posted my simple coolant tank (bucket)  I put together


---
**I Laser** *October 17, 2015 09:35*

Couldn't find RV antifreeze here in Australia. You can buy food grade glycol, but it's extremely expensive. From a bit of research I ended up using Nulon Red as it contains no silicates and has an anti organic additive. It's quite toxic, mines in a old home brew tank with lid (not air tight). It's been in there for 4 months and still seems okay.



With the rise in temperatures I've been doing the same thing with 2 litre bottles and whilst it works changing them out is a pain as I have to rinse them off before refreezing. I tried to separate the cooling but nothing is as effective as having the frozen bottle in the tank...



Glad I didn't buy a cw-3000, to label them chillers is so misleading if it's just a radiator/fan setup (which I already have from an old water cooled PC). They are of no use when ambient temps are high..



So wish I could find a cheap under sink chiller as that seems to be the answer.


---
**Phillip Conroy** *October 17, 2015 10:09*

Just do a search on ebay for undersink chiller and select second hand and follow the search ,then select to be notifyed by email when ones come on ebay


---
**Troy Baverstock** *October 17, 2015 21:16*

Wouldn't the cw-3000 have a Peltier on its radiator, so it will go below ambient. For less cost  I decided to buy the parts to make my own Peltier cooler with 3 radiators, flow and temp sensors etc, but it's still a work in process, so I don't know how successful it will be.


---
**I Laser** *October 18, 2015 01:50*

I guess it's more dependant on where you live as opposed to production capacity. Lately it's been pretty routine for my tank to be sitting between 20-25c when the machine hasn't been used in days!



I looked into peltiers too, but it seems a lot of work to get it right. I'm also not sure whether they are capable of doing whats required but will be very interested to see how you go Troy!


---
**Troy Baverstock** *October 18, 2015 02:06*

Me too! The setup is using cheap eBay cpu cooling parts and goes - passive radiator with fan - Peltier water block with heatsinkfan - 2nd larger Peltier water block heatsinkfan fan. The 'idea' was to scrub off temp with the passive radiator and switch on the 2  peltiers when needed, it would all be arduino controlled and I have a temp/flow sensor at either end for measurements and to shut of the laser if I loose water flow. The most expensive parts where the various hose fittings. The pump is 12vdc and so is my air assist so I should have complete control over those from the same arduino.


---
**I Laser** *October 18, 2015 03:48*

Not sure I'm understanding the idea, but unless you've got cold ambient room temperature to start with, a passive rad and fan is not going to help. 



In fact from the limited reading I've done, adding a rad into a peltier setup is not recommended as it counter acts the effect by trying to bring the water to ambient temps. The best way to setup apparently is to have a small insulated tank 1-2 litres, line goes straight to cold side of peltier and then back into tank. It allows the 'cold' cycle to be compounded. Most use a decent passive heatsink on the hot sideo f the peltier with decent fan. But this is PC cooling, so not sure how it transfers to the laser, though CPU/GPU's output way more heat than a 35w laser, so I assume it should be quite effective!



Another thing I considered was a 'water cooling bong', basically evaporative  cooling. But that would mean changing the cooling medium. I've followed the ebay search for a chiller, assume I'll be fighting Gregory for it though lol.


---
**Troy Baverstock** *October 18, 2015 04:39*

The passive radiator is in the loop before the 2 peltiers, it's true that if say 20deg water were exiting the laser tube and the ambient temp was above that the rad would actually warm the water instead, but then it goes into the 2 peltier/water blocks so it won't take much to bring it back down again. More likely though the water will be coming out above ambient. (I haven't assembled this yet so surprises may await).



As for the gpu/cpu thing, yes they generate less heat but you have to cool it further so requires more cooling power as it doesn't like running at 60-90deg c :)



(Keeping in mind where I live temps are 26-36deg most of the year)


---
**I Laser** *October 18, 2015 05:04*

Ah ok, sounds like you got it under control. ;) Let us know how you track, as I'm sure there's many more people looking for a cheap effective cooling system.


---
**Phillip Conroy** *October 18, 2015 12:18*

I have seen photos of inside the cw-3000 and there is no extra cooling apart from the fan and radator,it my under sink chiller uses 450 watts to power the compressor for the refridrater side of things ,i also use a seperat pump to run water through the chiller.

I use 2 6 plug power boards to enable me to control things,like running just the cooler to precool the water when needed,or turn off air compressor,

Indervigily switched parts of my laser system are

-air compressor

2 water pumps

2 extraction fans

Air assist

Vacumm cleaner-to clear out small cut peaces of 3mm mdf

Water chiller

10 watt led -using an old computer cpu au cooler

2 digital temp gauges -will add alarm when temp going to tube hits 20 degs

 Will be adding a cross hair laser pointer soon as well as a third power board


---
**I Laser** *October 19, 2015 04:09*

Would you mind dropping by my place and pimping my setup Phillip??



Mine is such a 'backyarder' in comparison! 


---
**Phillip Conroy** *October 19, 2015 08:24*

If you live in central vic ,australia ,yes i will drop by


---
**I Laser** *October 20, 2015 09:38*

Unfortunately not, but thanks for the offer. :)


---
**Stephane Buisson** *October 20, 2015 15:54*

damn it seem of a lot power waste, if you look at the problem on temp side, Why not using the cold from air compressor to cool water too. idea to dig up !

concentric air line into water return...as thermal exchange


---
**Victor Hurtado** *December 13, 2015 11:56*

Una consulta cuanto tiempo le dejas enfriar el agua, antes de usar el laser o prender la maquina


---
*Imported from [Google+](https://plus.google.com/105767412220617661397/posts/Kue1jqXncMA) &mdash; content and formatting may not be reliable*
