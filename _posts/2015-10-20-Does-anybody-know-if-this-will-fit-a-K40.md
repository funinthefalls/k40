---
layout: post
title: "Does anybody know if this will fit a K40?"
date: October 20, 2015 15:21
category: "Discussion"
author: "george ellison"
---
Does anybody know if this will fit a K40? 



[http://www.ebay.co.uk/itm/Water-Cooling-40W-Sealed-Laser-Tube-for-CO2-Laser-Engraver-machine-3000-4500-H-/111801328950?hash=item1a07e0e536:g:qJwAAOSwAYtWJJkx&autorefresh=true](http://www.ebay.co.uk/itm/Water-Cooling-40W-Sealed-Laser-Tube-for-CO2-Laser-Engraver-machine-3000-4500-H-/111801328950?hash=item1a07e0e536:g:qJwAAOSwAYtWJJkx&autorefresh=true)





**"george ellison"**

---
---
**Stephane Buisson** *October 20, 2015 15:31*

73 cm too long, doesn't fit in


---
**george ellison** *October 20, 2015 15:41*

Ahh, thats a shame


---
**george ellison** *October 20, 2015 15:42*

[http://www.ebay.co.uk/itm/40W-CO2-Laser-Tube-Engraver-cutter-Water-Cool-700mm-Shenhui-K40-/321882425353?hash=item4af1b00809:g:zLwAAMXQlgtRuGGK](http://www.ebay.co.uk/itm/40W-CO2-Laser-Tube-Engraver-cutter-Water-Cool-700mm-Shenhui-K40-/321882425353?hash=item4af1b00809:g:zLwAAMXQlgtRuGGK) This one is probably the correct one but £31 dearer :(


---
**george ellison** *October 21, 2015 06:35*

Hard a word with  he ebay seller and they say that it will fit into a k40 like shown in the photo, don't know whether to opt for it or not?


---
**Chev Chelios** *October 21, 2015 14:40*

Light [object.com](http://object.com) has an extension casing for the 40-45w tube.


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/gqB8UJJABFK) &mdash; content and formatting may not be reliable*
