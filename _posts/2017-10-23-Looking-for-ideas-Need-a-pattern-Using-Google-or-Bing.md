---
layout: post
title: "Looking for ideas? Need a pattern? Using Google or Bing?"
date: October 23, 2017 01:17
category: "Discussion"
author: "HalfNormal"
---
Looking for ideas? Need a pattern? Using Google or Bing?

Here is a better idea. Continue reading at your own risk!



Long story short, went looking for a laser project for a friend. Kept getting links to Pinterest. Decided that I would give it a try after avoiding it for years. Guess what? They have more of what you are looking for than any search engine! I do warn you, be very careful! You will spend hours and not know it. Do not say I have not warned you! Now go to Pinterest and type in laser cut projects and get burning! (you do have to step away from the computer and go to the laser sometime) Oh yea, don't forget to eat and shower sometime.



Here is an example of a site I found from Pinterest. It has some great Christmas SVG files



[http://thecraftchop.bravesites.com/entries/2015/12](http://thecraftchop.bravesites.com/entries/2015/12)







**"HalfNormal"**

---
---
**Ariel Yahni (UniKpty)** *October 23, 2017 01:27*

Agreed, been using Pinterest forever and it's endless


---
**Jeff Lamb** *October 25, 2017 15:10*

And me


---
**Andrew ONeal (Andy-drew)** *October 31, 2017 16:05*

While lost in pintraz 😉, you happen to see any files for heater circuits or super cap circuits? Been looking for something quick and easy to try on 3d and k40. Been so long since I've fired up the laser though been so busy with projects that came with Summer and new kabota tractor.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Z2GA8VGZCPD) &mdash; content and formatting may not be reliable*
