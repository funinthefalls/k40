---
layout: post
title: "Shop made Rotary works pretty well. This completes the upgrades I wanted to make to my K40"
date: September 10, 2017 22:31
category: "Modification"
author: "Kelly Burns"
---
Shop made Rotary works pretty well.  This completes the upgrades I wanted to make to my K40.  Not a big need for Rotary with my Woodworking, but my kids will be using the Laser for gifts and to make money engraving non Woodworking Items.  I will also be engraving a bunch of Tumblers to give to the friends and family that support my Woodworking Business.



![images/9960720b581b1b117e2c729a6b7b3336.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9960720b581b1b117e2c729a6b7b3336.jpeg)
![images/b2b5d23e57313dae64af960b6521e5f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2b5d23e57313dae64af960b6521e5f1.jpeg)
![images/f609efc0e86d71a985a2fd7e2a68d4c2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f609efc0e86d71a985a2fd7e2a68d4c2.jpeg)

**"Kelly Burns"**

---
---
**Kelly Burns** *September 10, 2017 22:54*

Working Rotary in action...

![images/b1dd72f67cffebcf4ab894d1fa18dec1](https://gitlab.com/funinthefalls/k40/raw/master/images/b1dd72f67cffebcf4ab894d1fa18dec1)


---
**Anthony Bolgar** *September 10, 2017 23:00*

Nice job! Looks really well put together.


---
**Kelly Burns** *September 10, 2017 23:15*

Thank You.  I was very surprised how solid it turned out.  The rollers are from an old inkjet printer covered with silicone tubing.  Scraps of 2040 Extrusion and 3D printed parts inspired from another design. 


---
**Mark Brown** *September 11, 2017 00:31*

Very cool. 



Are you doing anything to the tumblers before hand, or just blasting the paint off them?


---
**Kelly Burns** *September 11, 2017 01:52*

**+Mark Brown** just removing the paint.   I have a Powder Coating setup, but these preprinted ones are actually cheaper than the plain stainless ones.  


---
**Don Kleinschnitz Jr.** *September 11, 2017 02:17*

What controller and software driving?


---
**Kelly Burns** *September 11, 2017 02:27*

I'm running an MKS sBase with Grbl LPC and driving with LaserWeb.


---
**Jorge Robles** *September 11, 2017 06:25*

Have to cut my d**n k40 bottom 😤


---
**BEN 3D** *September 11, 2017 07:27*

Really cool, how does this work? Will the y axis replaced by the rotary axis?



Or will the rotary axis attached? How complicated is it to diy one?


---
**Don Kleinschnitz Jr.** *September 11, 2017 14:57*

#K40Rotary


---
**BEN 3D** *September 11, 2017 15:01*

uhhhh thanks **+Don Kleinschnitz** 


---
**Kelly Burns** *September 11, 2017 15:55*

**+Jorge Robles**  I did a hack job on it.  My grinder Locked up on me so I used a jig saw and tin snips to finish.  It does what I need though.  It's not a hard job, just messy and noisy.  I did it with all electronics and carriage components removed.  



You can make a smaller, more shallow version and do smaller items without cutting bottom, but with my setup was limited to items that were < 60mm diameter.  Plus I actually cut the bottom out for other reasons.  I want to be able to engrave my woodworking items post assembly.  




---
**Kelly Burns** *September 11, 2017 16:11*

**+BEN 3D**   I write everything up eventually, but Don directed you to good info. I have been gleaning info from the web for a while.  My design is based of one I stumbled across.  I turned it into a 3D model and then substituted the parts I had on-hand.  



I am currently swapping out Y axis because I didn't have the version/config for GRBL.  I use GRBL config settings to change the Y axis steps/mm on the fly as needed.  I'm hoping to correct that tonight with a new version of Grbl LPC from another user.



I did make a plywood version of a Rotary last year that I used with the stock controller. Since I couldn't change the Y axis step settings, I just distorted the original image as needed to make it work.  As I recall, I had to stretch the image by ~30% along the Y axis.  



I would encourage people to try this.  If you have access to a 3D printer, you can make this for about $30-$40.  I only spent $14 on a stepper?  Everything else came from scraps and spare parts I had.  It adds some great functionality. My daughter saw her Monogrammed tumbler and is already planning teacher gifts for holidays.  


---
**Arion McCartney** *September 19, 2017 19:23*

**+Kelly Burns**  Thanks for sharing this.  I am working on making one based on your design, unless you have shared the 3D printing files somewhere.  Just wondering if you experience the item being engraved walking up or down your rotary?  Is this even an issue, or is there something that keeps it from walking up/down the rods?   Only thing I am trying to figure out now is the best distance between the two rods.  What is the spacing on yours if you don't mind sharing?  Thank you.


---
**Kelly Burns** *September 19, 2017 19:28*

**+Arion McCartney** I was trying to cleanup my models before making them available, but I can release some draft versions if it would help.  I'll do that tonight and post a link to my good drive.  



The only issue I have had was engraving pens.  They aren't heavy enough and the wheels do slip on the rods.  



It's not perfect, but can't beat it for the price.  


---
**Kelly Burns** *September 19, 2017 19:29*

I am working on some accessories that clamp on the rails to keep things straight. 


---
**Arion McCartney** *September 19, 2017 19:32*

**+Kelly Burns** That would be awesome and much appreciated. I have made a different version rotary from Thingiverse, but it ended up not working as well as I'd hoped for. Your design seems simple and worth a shot. Thanks again.


---
**John Warren** *December 02, 2017 22:41*

If you dont mind sharing, what settings are you using when you engrave your cup?  I have done a couple for my kids but they always seem to be a bit dull when I am finished.  

![images/119a67c2c54560593231b1c527e2609d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/119a67c2c54560593231b1c527e2609d.jpeg)


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/3izV5DKimmw) &mdash; content and formatting may not be reliable*
