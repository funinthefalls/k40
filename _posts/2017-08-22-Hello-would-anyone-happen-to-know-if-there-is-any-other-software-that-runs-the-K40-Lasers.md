---
layout: post
title: "Hello, would anyone happen to know if there is any other software that runs the K40 Lasers ??"
date: August 22, 2017 04:37
category: "Discussion"
author: "Lawrence Kelley"
---
Hello, would anyone happen to know if there is any other software that runs the K40 Lasers ?? other than the Laser Drw or K40 Whisperer ?? I appreciate your time and information, new to all of this and really enjoy when I get this thing to work, but lately nothing but problems, hoping for a simpler solution  :-) here's the error message I am getting, very confusing



![images/48df9e16c16a5a7883c79e54fe07e897.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/48df9e16c16a5a7883c79e54fe07e897.jpeg)
![images/cb65454752b78e1c616242d849f1043a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cb65454752b78e1c616242d849f1043a.jpeg)
![images/58c9461c0e6104b936fd2975c8065cff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58c9461c0e6104b936fd2975c8065cff.jpeg)
![images/328bbf9c8705eba0726dd7b784a5460e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/328bbf9c8705eba0726dd7b784a5460e.jpeg)
![images/ff8f576e9ceb6eb1bb6c7c885ee8f15b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff8f576e9ceb6eb1bb6c7c885ee8f15b.jpeg)
![images/8d49f730a93fa6915deb99fbc71276ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d49f730a93fa6915deb99fbc71276ed.jpeg)
![images/3be182893afbe5f3397a3267d78c041d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3be182893afbe5f3397a3267d78c041d.jpeg)

**"Lawrence Kelley"**

---
---
**HalfNormal** *August 22, 2017 13:01*

Looks more like a USB communication issue than a software issue. Try a new cable or even another computer if you have one available. 


---
**Steve Clark** *August 22, 2017 15:44*

I was having comm problems for a time and ordered aTripp Lite USB 2.0 A/B Gold Device Cable with Ferrite Chokes, 3' someone recommended here. That solved the problem.


---
**Scorch Works** *August 22, 2017 20:38*

It is an out of memory error.  I fixed it so there is a better error in v0.06. The available memory is artificially low because I have only been releasing 32bit executables of K40 Whisperer.  I have a 64 bit version ready but I think users may need to switch to a 64 bit driver.  I need to do a little testing.


---
**Scorch Works** *August 22, 2017 20:41*

Hopefully we will get 'Lawrence and the Machine' up and running again soon.


---
**Lawrence Kelley** *August 23, 2017 03:51*

Thank you all so much, you are all a Blessing, I plan to go tomorrow (Wednesday) and pick up a New (Refurbished Desktop) with 1 TB and use it only for my Laser Engraver, and also while there get some New Cables  :-)  any download suggestions  :-)  so I can be ready and do this right  :-)  you are all a True Blessing in my K40 World as a Newbee  :-)


---
**Scorch Works** *August 23, 2017 11:52*

The fixed 64 bit executable is now available on the K40 Whisperer web page.


---
*Imported from [Google+](https://plus.google.com/108241240546580115383/posts/GgSfBphvvjM) &mdash; content and formatting may not be reliable*
