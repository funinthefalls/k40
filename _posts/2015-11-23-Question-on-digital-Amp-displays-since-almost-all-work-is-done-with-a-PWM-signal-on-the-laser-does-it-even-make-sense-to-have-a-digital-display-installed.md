---
layout: post
title: "Question on digital Amp displays: since almost all work is done with a PWM signal on the laser, does it even make sense to have a digital display installed?"
date: November 23, 2015 21:04
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Question on digital Amp displays: since almost all work is done with a PWM signal on the laser, does it even make sense to have a digital display installed? It's never going to be accurate enough, or fast enough to display accurate numbers, so really, why bother?





**"Ashley M. Kirchner [Norym]"**

---
---
**Coherent** *November 24, 2015 12:27*

I added one to mine and also left the analog one on the panel. It's accurate for cuts. Doesn't waver much within 1/10 from start to finish of the cut.  When engraving it's pretty useless just like the analog. The analog at least bounces around a little when engraving.  But most importantly the blue led meter looks cool


---
**Gary McKinnon** *November 24, 2015 12:56*

Agreed, no point in digi readout with PWM. But it does look cool :)


---
**Ashley M. Kirchner [Norym]** *November 24, 2015 17:38*

**+Carl Duncan**, not referring to a volt meter, but an Amp meter. :)


---
**Ashley M. Kirchner [Norym]** *November 24, 2015 18:31*

Ah, gotcha!


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/6wZwENimThp) &mdash; content and formatting may not be reliable*
