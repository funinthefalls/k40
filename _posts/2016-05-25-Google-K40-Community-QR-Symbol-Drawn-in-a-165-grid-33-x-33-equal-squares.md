---
layout: post
title: "Google K40 Community QR Symbol, Drawn in a 16.5\" grid 33 x 33 equal squares"
date: May 25, 2016 12:55
category: "Discussion"
author: "Scott Marshall"
---
Google K40 Community QR Symbol, Drawn in a 16.5" grid 33 x 33 equal squares.



Please scan and verify function.

Thank, Scott



![images/d5724f9f590b3b82f574ae4602611261.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d5724f9f590b3b82f574ae4602611261.jpeg)
![images/9525b218499ca575ed5c9c4b669d93d6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9525b218499ca575ed5c9c4b669d93d6.jpeg)

**"Scott Marshall"**

---
---
**Roberto Fernandez** *May 25, 2016 13:14*

It works.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 14:33*

It's strange, I kept receiving a 404 error saying that the page doesn't exist. Until I manually opened the K40 group via google search. Then I scan the code & now it works properly. Might have to push that issue to the Win10 Mobile team for investigation.


---
**Carl Fisher** *May 25, 2016 16:56*

Worked first shot for me




---
**Scott Marshall** *May 26, 2016 09:57*

Thanks Guys! 



I don;'t know if I can put these on all my shirts, weeding the QR is a bear (weeding is removing the unused bits left after the cnc plotter is done) - it hard to tell what stays and what goes.....



I may make this a logo of it's own, so to  speak, and put it on one side of the shirt . 



Need to sell some 1st, Come on guys, I've got 14 K40 shirts on ebay, and I'll give any K40 guys a 10% discount if you e-mail me direct.



I've had several inquiries on Beta testing the K40 electronics kits,  If anyone is interested send me an e-mail, and I'll put you on my mailing list for weekly updates.



Thanks all, and I'll be getting things together as fast as parts arrive....


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 11:48*

**+Scott Marshall** Yeah weeding that would be a bit of a nightmare. You don't happen to have inkjet printer & inkjet vinyl transfer paper that you could use for these? Then you could just print the QR code onto the inkjet vinyl transfer & heat press on. What's the eBay link btw? I never saw it in your original post.


---
**Scott Marshall** *May 26, 2016 14:23*

I've gone through about all the transfer paper there is, and there's none out there I would put my name on. It just doesn't hold up in use. Too bad too, I could sell a LOT of photo tees, but I won't sell something that starts to degrade in 8 or 10 washes (or less). 



The word on printable Vinyl is it's iffy too, but I'd have to do more testing to see how it actually lasts. I guess I ought o order some and play with it. The homework involved in putting out a quality product is time consuming and expensive. I refuse to sell junk though.



Silkscreening is an option, but will raise the price point on these higher than I think will sell. 

I've got them priced pretty tight @ $20 for a 2 color 2 sided name brand shirt, Shipped no less, and I haven't sold any yet. 

I figured the K40 guys were pretty enthusiastic and would at least buy a few when I put them up. Now I'm worried about the electronics, I've got a LOT of time and money in those, I hope I didn't misjudge the market....



I've got 14 up on ebay, I put them in the K40 Laser dept, so they wouldn't be buried in 5000 t shirt listings in the clothing section.

.

If you search for K40 Laser Accessories you should run right into them.

let me know if you don't , traffic is in the single digits in 24 hrs, maybe I screwed up?


---
**Scott Marshall** *May 26, 2016 14:27*

Link:

[http://www.ebay.com/itm/K40-Laser-T-Shirt-Ash-Grey-Medium-/272253242232?hash=item3f638ecb78:g:H0IAAOSwH6lXRKHT](http://www.ebay.com/itm/K40-Laser-T-Shirt-Ash-Grey-Medium-/272253242232?hash=item3f638ecb78:g:H0IAAOSwH6lXRKHT)



From that one, you can use the "see my other lisrtings" and that ought to jump you to the rest. I have a couple non-laser that I havn't listed, Ebay has had technical problems with the listing engine since yesterday.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 18:11*

**+Scott Marshall** Thanks Scott. Didn't get around to it yet because I was making a drag-cable, installing my new air-assist nozzle, my webcam mount (so it moves along with the Y-rail). Will take a look now. I'm definitely interested in one of the shirts & the electronic kits when they're ready (& I have the $). I'm on a bit of a tight budget generally, so might be a couple of weeks before I can get shirt/electronics when they're good to go. I'll take a look now.



On the vinyl printable transfer sheets, I used to use one called JetOpaque, that I would purchase from a company in Sydney. I made some shirts using it in 2009 for a fishing competition that my step-father & I were attending & for the rest of our team. I've worn one of those shirts probably about 10 times a year (minimum) & the print is still in good condition. Although, edges are slightly starting to lift on some of the sharper edged pieces. I'll take some pics of it sometime later & find the supplier details & pass them on if you're interested.


---
**Scott Marshall** *May 26, 2016 18:56*

That's a great lead, I'll look for some. Does it do color? laser or IJ? 



The lifting edges are a common issue if you Ironed them on, it's hard to get enough pressure. They lasted longer than a lot of "commercial" shirts I've owned though!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 19:16*

**+Scott Marshall** It does colour, inkjet. I'm not 100% certain off the top of my head if it does laser printer or not, will have to dig it out & check for you. Give me 1/2 hour & I'll find it, it's details, take a photo of the 7 year old shirt, & find the supplier link. I used a proper heat press to put them on, but mind you, the shirts are 7 years old now, so lifting is probably expected after that timeframe. That being said, they have lasted pretty well for 7 years & multiple years of wear & washing.


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/XgpkvC4Hc8q) &mdash; content and formatting may not be reliable*
