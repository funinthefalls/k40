---
layout: post
title: "I've had the K40 for a few days"
date: December 05, 2017 16:42
category: "Original software and hardware issues"
author: "J M"
---
I've had the K40 for a few days. I noticed that the power % fluctuates while its cutting. My machine came with a digital readout.

 

Example: 

If I set the power to 25% , it will jump to 1% after a few seconds of cutting.

I readjust to 25% then it jumps to 99% .. or it switches to off. I constantly adjust the power throughout the cutting process.



Has any one come across this ?

 Thanks.





**"J M"**

---
---
**Joe Alexander** *December 05, 2017 19:32*

I would inspect all of your crimps, verify that the laser tube connections are solid and siliconed well, and that your water pump is running smoothly. Are you monitoring the temperature of the water? should be between 15-25°C and never exceed 30°C. Gives you something to check while waiting for others to reply at least :)


---
**J M** *December 06, 2017 15:22*

**+Joe Alexander** All the connections seem to be solid and the water temperature is cool. I've been reading other posts where people have been experiencing similar issues with a flakey digital control board. They recommend replacing with an analog meter & potentiometer. This looks like a good idea at the moment.  Thanks for the feedback. 


---
*Imported from [Google+](https://plus.google.com/109593509915410590400/posts/54LyYv1YBun) &mdash; content and formatting may not be reliable*
