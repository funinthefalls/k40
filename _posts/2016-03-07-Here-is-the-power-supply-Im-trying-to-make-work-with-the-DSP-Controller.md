---
layout: post
title: "Here is the power supply, I'm trying to make work with the DSP Controller"
date: March 07, 2016 04:37
category: "Modification"
author: "Jerry Martin"
---
Here is the power supply, I'm trying to make work with the DSP Controller

![images/f71c6d40d889a047d5298c253ba36af9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f71c6d40d889a047d5298c253ba36af9.jpeg)



**"Jerry Martin"**

---
---
**Gee Willikers** *March 07, 2016 05:00*

Not quite like mine But those small connections I think:



<b>*Confirm this first*</b>



G Ground

P+ Water Protection switch

L Laser fire button

G Ground to fire button and pot?

IN center of mA pot

+5 to pot



So similar to my other post here: [https://plus.google.com/photos/+JeffGolenia/albums/6203813861516646785](https://plus.google.com/photos/+JeffGolenia/albums/6203813861516646785)



Connectors as pictured left to right

<b>4 pin no change</b>

Yellow wire to ammeter and tube

Yellow to machine ground post

AC in

AC in



<b>6 pin</b>

G and P+ generic safety switch / water protection

L (TTL) (old test fire button) -- to dsp TTL screw

GROUND (old side a of pot/button) -- to dsp GROUND screw

IN (PWM) (old center of pot) -- to dsp PWM screw

+5v (old side b of pot) -- no connection



<b>4? pin</b>

No connections. Untrusted. Supplied power to old Moshi board.



<b>So all you should need is 3 wires and a jumper.</b>



FYI water protection (WP) -- connect your pump pressure switch between WP and GROUND. <b>If connecting to LO X7 DSP jumper WP and GROUND on LASER 2</b> Cover switch is In1.


---
**Stephane Buisson** *March 07, 2016 07:40*

hi,

Same PSU ref as mine (but different connectors).

if any help:  [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)


---
*Imported from [Google+](https://plus.google.com/111904744658622469563/posts/Ee1JZMrncSR) &mdash; content and formatting may not be reliable*
