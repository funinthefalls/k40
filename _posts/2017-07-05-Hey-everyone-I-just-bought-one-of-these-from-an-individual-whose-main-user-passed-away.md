---
layout: post
title: "Hey everyone! I just bought one of these from an individual whose main user passed away"
date: July 05, 2017 12:32
category: "Discussion"
author: "Brandon D"
---
Hey everyone! I just bought one of these from an individual whose main user passed away. He knows nothing about the unit, as it was a joint effort operation, and he only dealt with the CNC machines. This device is the one with the microdog USBKey, but the original control board was replaced with one from [electronics-shop.dk](http://electronics-shop.dk) it's the bigbear 2XX as far as I can see. Looks like this [https://bigbear.dk/help/stepcontrol/lib/usb_stepper_2xx.jpg](https://bigbear.dk/help/stepcontrol/lib/usb_stepper_2xx.jpg)



Now my question is, does anyone know about this controller? And if so, help me out! I can't seem to get it to show up on my laptop I'm using, though the green and red lights work on the controller (red seems to mean that drivers are installed and correctly communicating if I read correctly).



Few other points:

 I can manually fire the laser by turning the knob up, but the te st laser button does not work.  



When powered up, stepper have power going to them as they're difficult to move, as all nema17s are while voltage is applied. 





Any and all suggestions! Or should I bite it and buy a new controller such as RAMPS?





**"Brandon D"**

---
---
**Anthony Bolgar** *July 05, 2017 12:47*

I would suggest looking at the C3d drop in replacement board at [cohesion3d.com - Cohesion3D Mini](http://cohesion3d.com/cohesion3d-mini/) Good time to buy as it is $20.01 off right now.


---
**Brandon D** *July 05, 2017 13:27*

Yeah bit more than I want to spend atm lol. Ramps for $20 is nicer haha nut I'll look into it 


---
**Stephane Buisson** *July 05, 2017 14:09*

**+Brandon D** ramps isn't worth more than 20usd because it's an old 8bits slow processor. if you want 6 x speed for 5x price  allowing acceleration for good raster then you will save a lot on material, then you will realise you had made the good choice. also C3D is a drop in replacement, not a weekend's killer.


---
**Brandon D** *July 05, 2017 14:25*

I'm extremely familiar with 3D printers. I suppose I will have to pick one up.... looking at that stepper controller that they used is far less conplex than any control board. I'll look into it 


---
**Brandon D** *July 05, 2017 14:34*

**+Anthony Bolgar** looking into this, it seems it uses Molex connectors for the nema17s, while mine seems to be a ribbon cable?


---
**Anthony Bolgar** *July 05, 2017 14:35*

The board has a ribbon cable connector as well as molex, it covers all know variants of the K40


---
**Brandon D** *July 05, 2017 14:48*

Alright. Well do you have any suggestions on what I could possibly to do get this thing running in the mean time?


---
**Anthony Bolgar** *July 05, 2017 15:39*

From the write up on their website, you can send g code over USB to the controller. Supposedly the controller shows up as a com port in windows, not sure what software will connect to it however, I will do a little bit of searching to see what I can find. FYI, the C3D boards are in stock and it would only be a few days to get one. You might want to ask the original owners partner if there any programs on his computer that look like they were used with the laser. Obviously he was running something with it.


---
**Anthony Bolgar** *July 05, 2017 15:42*

Google is your friend.....took a whole 45 seconds to find the info you needed [bigbear.dk - Stepper Controller Help](https://www.bigbear.dk/help/stepcontrol/index.php?Whatismicrostepping.html)


---
**Brandon D** *July 05, 2017 23:01*

Yes. I have been through that site multiple times. I finally got the drivers installed, but the download for the control.exe is 0kb, and the site that sells them has a bad download link. 


---
**Brandon D** *July 07, 2017 01:46*

Just as an FYI, I got it figured out after contacting the company. Their website switched servers so their links were basically useless for downloads!


---
**Anthony Bolgar** *July 08, 2017 18:27*

Glad things worked out!




---
*Imported from [Google+](https://plus.google.com/106429087520568972232/posts/SM4o6cc5mZo) &mdash; content and formatting may not be reliable*
