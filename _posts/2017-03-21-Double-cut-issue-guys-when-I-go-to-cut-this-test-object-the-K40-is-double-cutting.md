---
layout: post
title: "Double cut issue..... guys, when I go to cut this test object, the K40 is double cutting"
date: March 21, 2017 17:22
category: "Discussion"
author: "J Perry"
---
Double cut issue..... guys, when I go to cut this test object, the K40 is double cutting.  The object on the left is hairline and the one on the right is a fill w/o the lines (which is what you see on the left).  For both, I am getting double cutting.  This object was drawn in Autocad and brought into Corellaser.  Any help would be appreciated.  Thanx.

![images/a0e59e510b1c558162051a4ef501ab75.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0e59e510b1c558162051a4ef501ab75.jpeg)



**"J Perry"**

---
---
**Mark Brown** *March 21, 2017 17:30*

Have you tried inverting the colors? It shouldn't matter on the filled one, but ya never know.


---
**Ned Hill** *March 21, 2017 17:32*

Line widths typically need to be less than or equal to 0.01mm with corellaser or you get the double cut.  Generally hairline is greater than that.


---
**Nate Caine** *March 21, 2017 17:46*

Your description is a little vague, but...      I've seen similar problems when a design created in one program is imported into another.  Objects get duplicated.  



To check:  With the design imported into Corel, try selecting the filled object on the right, then delete it.  Then refresh the display.  If the object still appears to be there, then you must have deleted the duplicate.  If the object is gone, you deleted the only copy.  



You can repeat this a few times to be certain you are left with only a single copy.  If, by accident, you delete the final copy, just do "undo" to restore it back.



Repeat for the outline object on the left.  



----------------



You can get some <b>really</b> strange results when you have two identical filled objects on top of each other.  The result can appear as an "engraved outline" with no fill.  Very confusing.


---
**J Perry** *March 21, 2017 18:50*

0.01 Line Width setting did the trick!!  Thanx.


---
*Imported from [Google+](https://plus.google.com/107694592875565680026/posts/iMmTSQBFQ32) &mdash; content and formatting may not be reliable*
