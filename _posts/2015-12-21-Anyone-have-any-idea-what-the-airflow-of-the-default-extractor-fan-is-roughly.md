---
layout: post
title: "Anyone have any idea what the airflow of the default extractor fan is roughly?"
date: December 21, 2015 05:28
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Anyone have any idea what the airflow of the default extractor fan is roughly? I want to upgrade it to something better, however I don't actually know what is better.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**I Laser** *December 21, 2015 08:35*

Not sure what the stock fan is, but even with modifications to the shroud to close the air leaks it's not much cop.



From what I read you want something that can push 400cfm. I bought one rated just above that and it's great. Place it on the other end of your exhaust if you can, stops it getting clogged by soot. ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 08:40*

**+I Laser** Thanks for the info I Laser. Yeah I basically want something that isn't so weak. I have the exhaust venting outside via 4-5m of flexible pipe & the current fan is just not powerful enough. Could you please point me to where you bought your 400+ cfm fan?


---
**I Laser** *December 21, 2015 09:06*

[http://www.ebay.com.au/itm/INLINE-CENTRIFUGAL-EXHAUST-DUCT-FAN-BLOWER-with-METAL-BLADE-LEATHER-JACKET/261259041870?_trksid=p2047675.c100010.m2109&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D20140122130056%26meid%3Dac95b7f255094b76bbbbf895a1f10f03%26pid%3D100010%26rk%3D2%26rkt%3D16%26mehot%3Dpp%26sd%3D261806972882](http://www.ebay.com.au/itm/INLINE-CENTRIFUGAL-EXHAUST-DUCT-FAN-BLOWER-with-METAL-BLADE-LEATHER-JACKET/261259041870?_trksid=p2047675.c100010.m2109&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D20140122130056%26meid%3Dac95b7f255094b76bbbbf895a1f10f03%26pid%3D100010%26rk%3D2%26rkt%3D16%26mehot%3Dpp%26sd%3D261806972882)



:)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 09:44*

**+I Laser** Thanks :)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/aaZuevxWsym) &mdash; content and formatting may not be reliable*
