---
layout: post
title: "Does anyone have a high resolution copy of this schematic?"
date: October 11, 2016 16:41
category: "Smoothieboard Modification"
author: "K"
---
Does anyone have a high resolution copy of this schematic? It only zooms in a little, and I can't read most of the tiny writing as well as some of the legend. 

![images/4dc56b932199510cc58cfbd1c402cfbf.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4dc56b932199510cc58cfbd1c402cfbf.png)



**"K"**

---
---
**Ariel Yahni (UniKpty)** *October 11, 2016 16:48*

At the bottom of this page you will find the details for the board.  [http://smoothieware.org/smoothieboard](http://smoothieware.org/smoothieboard)


---
**Ariel Yahni (UniKpty)** *October 11, 2016 16:48*

[smoothieware.org - Smoothieboard - Smoothie Project](http://smoothieware.org/smoothieboard)


---
**K** *October 11, 2016 17:01*

Maybe you can answer this, I have a level shifter, and I know one set of bidirectional pins gets attached to L on the PSU and then a pin on the board (I'm using the X5), one goes from the 3.3V on the board to the 3.3v on the shifter out to 5V on the PSU, but this schematic only shows a ground going from the 5V side of the level shifter to the PSU. Does the X5 not also get a ground connection on the 3.3v side?




---
**Ariel Yahni (UniKpty)** *October 11, 2016 17:07*

**+Kim Stroman**​ going to refer yo to the best we have from recently [https://plus.google.com/109807573626040317972/posts/CXFk1n4oezp](https://plus.google.com/109807573626040317972/posts/CXFk1n4oezp) you where allready tagged there.  I haven't done that yet


---
**K** *October 11, 2016 17:11*

Thank you! I totally forgot about that post.


---
**Ariel Yahni (UniKpty)** *October 11, 2016 17:12*

**+Alex Krause**​​, **+Kim Stroman**​​ is on the last stretch, can we helper here?  I'm not sure


---
**K** *October 11, 2016 17:14*

The part in the schematic that was confusing me is the color coded area below the level shifter, but the more I look at it, it looks like the green lines are just indicating pins on the smoothie board.




---
**Ariel Yahni (UniKpty)** *October 11, 2016 17:15*

Yes that why I liked to the original schematic on smoothie


---
**K** *October 11, 2016 18:06*

Ok. This is what I've figured based on Alex's post:



PSU 5V ==> Level Shifter 5V



3.3V Level Shifter ==> 3.3v pin on X5 board



PSU L ==> one of the bi-directional spots on level shifter (5V side)



Bi-directonal spot on level shifter (3.3v side) ==> Hot End NEG terminal


---
**Alex Krause** *October 12, 2016 01:45*

**+Kim Stroman**​ Sorry for the late response I was in a CPR/First Aid/AED training course today for a vast majority of my day... let me know if you still need help


---
**K** *October 12, 2016 22:28*

**+Alex Krause** Hi Alex. I have a quick question for you. I have everything wired up, and the Laser On/Off switch works, as does the test switch. At the moment I'm foregoing the pot since I had to order a different one for my non-stock PSU. I'm controlling via LaserWeb, and I can't get the laser to fire. Can you take a look at my post right above yours to see if that looks correct? I'm wondering if I've got my level shifter wrong. Also, what codes are you using for Laser On and Laser Off? In the settings tab of LaserWeb3 it says those are optional and that it can pull the codes from firmware, but when I generate G-Code via the CAM tab I get no on/off commands, just straight moves.


---
**Alex Krause** *October 12, 2016 22:34*

Can you also upload your config file to Google drive so I can peak at it


---
**K** *October 12, 2016 22:37*

Here's the file. [https://drive.google.com/open?id=0B1kvMudpU9EMTWRkbEhaTHJyQ28](https://drive.google.com/open?id=0B1kvMudpU9EMTWRkbEhaTHJyQ28)



You'll see the Laser PWM pin set at 2.5. I originally had it at 2.5!, and when that didn't work I swapped it. 

[drive.google.com - config - Google Drive](https://drive.google.com/open?id=0B1kvMudpU9EMTWRkbEhaTHJyQ28)


---
**K** *October 13, 2016 02:14*

Ok, so I figured I'd see if mine needed to be grounded, and I grounded both from the PSU to the LS and from the LS to the X5. No dice. 


---
**Alex Krause** *October 13, 2016 02:22*

![images/2f160bc61d3cde4e61a1c9baa79822aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f160bc61d3cde4e61a1c9baa79822aa.jpeg)


---
**Alex Krause** *October 13, 2016 02:24*

**+Kim Stroman**​ remove the # at the beginning of those lines and change that 20 to 200... it should work after that if your level shifter is wired in the right direction


---
**Alex Krause** *October 13, 2016 02:26*

You have all the settings that controller the laser Commented out 


---
**K** *October 13, 2016 02:28*

**+Alex Krause** I really suck at this, LOL. Thank you again!


---
**Alex Krause** *October 13, 2016 02:31*

**+Kim Stroman**​​ don't forget to post your first "Hello World" engrave :)


---
**K** *October 13, 2016 03:13*

**+Alex Krause** I'm still not getting a laser on/off via software, either by manually sending M3 S.XX or when I run g-code. I've uncommented out the lines you highlighted, and I soldered up another level shifter just in case the one I had was bad, and it's still not firing.


---
**Alex Krause** *October 13, 2016 03:13*

G0 is laser off G1 is laser on


---
**K** *October 13, 2016 03:15*

G1 isn't giving working (when input into the code box on the main screen.)


---
**Alex Krause** *October 13, 2016 03:20*

**+Kim Stroman**​ Using the L pin might require you to have a Potentiometer installed... right now without one I believe you are set at zero power... do you have any resistors laying around?


---
**K** *October 13, 2016 03:21*

!**+Alex Krause** I do!




---
**K** *October 13, 2016 03:21*

Would it make sense to wire to the High pin?


---
**K** *October 15, 2016 03:17*

I noticed some interesting behavior tonight. My meter kept jumping, and the pot would inconsistently adjust the current. I zeroed out my Smoothie by mistake, and so I hit the reset button on my X5, and I noticed the meter jumped by 5mA when I did! I tried it twice and that happened. Is this electrical interference or something else?


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/RM6FkLmD4qe) &mdash; content and formatting may not be reliable*
