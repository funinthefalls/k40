---
layout: post
title: "for the light object z table i'm assuming i can just hook that directly to my smoothie but does anyone know the pitch of the lead screws so i can adjust the firmware accordingly?"
date: July 25, 2016 16:55
category: "Discussion"
author: "Derek Schuetz"
---
for the light object z table i'm assuming i can just hook that directly to my smoothie but does anyone know the pitch of the lead screws so i can adjust the firmware accordingly?





**"Derek Schuetz"**

---
---
**Derek Schuetz** *July 25, 2016 17:03*

I could do that but just wanted to see if someone had the exact number 


---
**Anthony Bolgar** *July 25, 2016 17:04*

The info is probably in the LightObjects forum on their website


---
**Derek Schuetz** *July 25, 2016 17:05*

They have no info on their table they even say no instructions provided you should just be able to figure it out in 90 minutes. I know I can but I thought maybe someone here has used it with a smoothie board already


---
**Alex Krause** *July 25, 2016 18:00*

**+Don Kleinschnitz**​ has one I believe


---
**Don Kleinschnitz Jr.** *July 25, 2016 20:17*

The lift screws are 1mm threads.

Let me know how your smoothie integration goes as I will have to do the same.


---
**Corey Renner** *July 25, 2016 23:55*

Got mine today.  Don is correct, the screws are M6 x 1.0 pitch


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/im1Qh7gVEMr) &mdash; content and formatting may not be reliable*
