---
layout: post
title: "Sorry for my english.why when I engrave is the text transparent?"
date: April 10, 2018 14:04
category: "Hardware and Laser settings"
author: "Maurizio Olivieri"
---
Sorry for my english.why when I engrave is the text transparent? I would like the result to be like in the second photo, that is white.

I hope I was clear



![images/f97d975e6fbef9901873f4ed3fd2345d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f97d975e6fbef9901873f4ed3fd2345d.jpeg)
![images/d63944099d09e33a3f4c5a0c81345e99.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d63944099d09e33a3f4c5a0c81345e99.jpeg)

**"Maurizio Olivieri"**

---
---
**pierre pino** *April 10, 2018 16:56*

I think this is not visible because during the engraving you had to stop blowing. To have a visible engraving must blow during the engraving.


---
**Grafisellos Barranquilla** *April 11, 2018 00:08*

Estás seguro que es acrílico 100% ese acabado lo da es el plastico


---
**Andy Shilling** *April 11, 2018 06:47*

It looks like you are laser cutting the text rather than rastering it.


---
**Ned Hill** *April 11, 2018 16:42*

Also cast acrylic engraves better than extruded.


---
**Maurizio Olivieri** *April 12, 2018 06:19*

Tnx for support....problem was  not acrylic 100%


---
**Alberto Filippetti** *April 12, 2018 18:03*

ciao ho lo stesso problema come hai risolto ?


---
*Imported from [Google+](https://plus.google.com/109843175636105778254/posts/5oBPL6G7FFo) &mdash; content and formatting may not be reliable*
