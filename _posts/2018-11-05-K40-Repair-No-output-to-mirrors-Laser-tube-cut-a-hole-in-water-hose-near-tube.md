---
layout: post
title: "K40 Repair - No output to mirrors - Laser tube cut a hole in water hose near tube?"
date: November 05, 2018 02:19
category: "Original software and hardware issues"
author: "AL"
---
K40 Repair - No output to mirrors - Laser tube cut a hole in water hose near tube?

I got a strange one here I would appreciate if anyone could chime in if they seen something like this or have a idea what to look for to find the exact problem to repair.







My k40 has worked for a year fine since I got it. I use it sparingly but never had a issue. I have a water pump going to a chiller that has always worked fine. Started working on something a few days ago, and I cut for 20 minutes without trouble. Then I noticed the last cutting out mid cut and I was looking for a issue. I was checking water flow and it did seem like one hose may of been slightly kinked. When I unkinked it and got full flow back water outlet stream was very hot, so I thought maybe the tube overheated. I left it off for awhile giving it time to cool. Made sure water flow was good and cool before trying again and I could not get any output from my laser on the material and immediately had water everywhere. I quickly powered all down and found that some how the tube, in the back housing, cut a hole through the water hose. Very strange to me, but I could see the laser tube was not outputting through the tube now and just lighting up at the beginning of the laser.







After cleaning, drying, and replacing house I tried the laser a few more times on very low settings and I have no output to material, but almost instantly I had a new pinhole size hole in the new hose next to the tube in the back. So somehow at least a partial laser must be coming out of the side of my tube hitting the waterline next to it. Very strange so I wanted to see if anyone has seen something like this, and if my tube is fried or if their is a possible cause I could look for.







Thanks!





**"AL"**

---
---
**Don Kleinschnitz Jr.** *November 05, 2018 02:37*

Can you post pictures of we're the hold is forming?


---
**AL** *November 06, 2018 15:26*

**+Don Kleinschnitz Jr.**   I made this picture to show were I am at with this. You will notice a few things. The black scorching is were the water line originally melted through and where the pinhole appeared within 10 seconds of attempted use after repairing water line.  I am assuming this was caused by a HV arc?



HV wire appears in tact and I wouldn't think this arc originated at the terminal connection due to the location of the black scorching being over 6 inches away and a bit around the housing as you can see.



The tube does not fully light up anymore then I energize it I can just see the high voltage sparks inside the tube in the first 2 inches or so where it is a small diameter right next to the HV wire as shown in the picture.



Any suggestions on what I should try or test to determine the state of this? Any way to test if my tube is fried now? I will add that this tube has about 2hrs of run time since I got it about 2 years ago.







[https://imgur.com/a/pUAOTl4](https://imgur.com/a/pUAOTl4)

![images/1789ef6ea38a59c00ea66025cc99c6ab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1789ef6ea38a59c00ea66025cc99c6ab.jpeg)


---
**Joe Alexander** *November 07, 2018 11:38*

Sounds like a fried tube to me, and that the power is finding less resistance to that part of the chassis over through the gas in the tube. Plus most laser tubes have a maximum life span thats more in the 1-1.5 year range so 2 years on a tube isn't all that bad. Only way I know to test a tube is to either hook up a new one or to hook it up to another laser PSU and see if it works. Personally I'd try replacing the tube first since you know it had a critical temp issue.


---
**Don Kleinschnitz Jr.** *November 07, 2018 14:20*

**+Al K** I agree with the notion this is a HV leak. 



LPS will arc violently when there is no load (bad tube). They can easily arc across a 2" gap. Usually this means the tube is bad and is providing no load.



I would think that you could see this arc if you "pulse test" with the rear cover off in a dark room. Use safety procedures and stay well away. 



Isolating bad tube vs LPS is tough. If the LPS has a strong arc it is usually ok and the tube is bad. If not its usually the LPS.

.......

You can try cutting those tie wraps and routing the HV wire away from the coolant tube to see if it arc's to a different place. 



You can also try putting a thick piece of Plexiglas/acrylic between the HV lead and the coolant tube. I would use a min of 1/4" thick.



All this will prove is that the LPS is probably good and the source of the water line damage.

....

<b>what coolant are you using??</b>



......

LPS are not that expensive these days I have seen them for $40. My guess is that you will replace it in a year anyway. Depending on your budget getting one with the new tube may save you some grief.

.....

Notes:

.... its best to install a tube flow meter and temp monitor into the interlock circuit to prevent tube damage from overheat.

... tubes have a shelf life as well as an operating life as **+Joe Alexander** has suggested.




---
*Imported from [Google+](https://plus.google.com/106595231022808628679/posts/EkTYMJenFVs) &mdash; content and formatting may not be reliable*
