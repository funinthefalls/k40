---
layout: post
title: "HI, I was previously running with GRBL and got full laser power at 5V and I recently shifted to smoothie but the power was kinda lower the before"
date: May 24, 2016 08:52
category: "Smoothieboard Modification"
author: "Athul S Nair"
---
HI,



    I was previously running with GRBL and got full laser power at 5V and I recently shifted to smoothie but the power was kinda lower the before. So yesterday I checked the voltage between IN and GND and noticed a 3.6 V instead of 5V for S1 command. after a lot of checking I found that if I disconnect WP from ground I will get 4.98V at IN and voltage reduce to 3.6V when WP connected to GND. I"m sending PWM and active low signal through a bi-directional TTL , and tried to power the HV side(5V side) of LLC from seperate power supply, but result was same. Anyone have any idea what causing it??



Anybody checked there PWM volatge at IN terminal??



I'm attaching my connection diagram.



Thanks



![images/3339f8ac3beb11de13e15047472228ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3339f8ac3beb11de13e15047472228ff.jpeg)
![images/fdac3eb931245ea54316c0176b2bbfaa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fdac3eb931245ea54316c0176b2bbfaa.jpeg)

**"Athul S Nair"**

---
---
**Stephane Buisson** *May 24, 2016 09:09*

Please, specify your PSU model, it seem to be 3 differents one (at least, not including the very old one), creating different combinations. Connection would depend on PSU. thank you for sharing your experience.

I personally would like to keep the pot as ceiling to protect the tube life. 15ma -> is about 3.6V (from memory) more than 3.3V this is why I use a level shifter.


---
**Athul S Nair** *May 24, 2016 09:19*

**+Stephane Buisson** [http://www.laserpsu.com/laser-power-supply-p-767.html](http://www.laserpsu.com/laser-power-supply-p-767.html)



Mine was eorking fine with GRBL and I didn't use LLC there,  ANd with smoothie If I directly connect smoothie PWM pin to IN(without LLC) i get 3.3 V for S1 command


---
**Athul S Nair** *May 24, 2016 09:32*

**+Jean-Baptiste Passant**  I think we have the same PSU from our previous discussion [https://plus.google.com/114978037514571097180/posts/MxAWWe4jmzS](https://plus.google.com/114978037514571097180/posts/MxAWWe4jmzS)



Can you check your pwm voltage


---
**Stephane Buisson** *May 24, 2016 09:51*

yes my understanding is PWM is a square signal 0-5V in our case with level shifter, it's the pot to reduce it to 3.6V ceiling.

it's the frequency (% of time on which vary the laser power)


---
**Stephane Buisson** *May 24, 2016 09:53*

I just request [http://www.jnmydy.com/](http://www.jnmydy.com/) for a download link in english for their PSU. (most of the K40)


---
**Athul S Nair** *May 24, 2016 10:01*

**+Peter van der Walt** If remove WP from GND 

I'll get 5V at IN terminal for S1 command(3.3 V smoothie)


---
**Athul S Nair** *May 24, 2016 10:25*

Test 1

WP --> GND

PWM from LLC measured with multimeter(PWM is not connected to IN)



S0      ------    0V

S0.5   -----     2.37V

S1      -----     4.90V



Test 2



WP <s>-</s>> GND

PWM is connected to IN and mutimeter



S0        ------   0.04V

S0.25   ------   0.91V

S0.5     ------   1.80V

S1        -------   3.7V  





Test 3



WP---> disconnected from GND

PWM is connected to IN and multimeter



S0        ------   0.04V

S0.25   -------  1V

S0.5      ------   2.12V

S1         -------   4.85V


---
**Athul S Nair** *May 24, 2016 12:55*

**+Stephane Buisson** **+Peter van der Walt**

1. If i directly conncet PWM pin (without LLC, ) to IN with WP connected to ground , thers's  no reduction in voltage .I get 3.3 V for S1



2. If I use LLC and measure voltage across Output of LLC and GND(PWm is not connected to IN, i.e., IN is open, WP is connected),  get 4.9V on multimeter for S1



3. If use LLc and connect LLC output to IN and WP to GND, it'd be 3.7 V for S1, that's the issue. Could it be a LLC issue?


---
**Stephane Buisson** *May 24, 2016 14:02*

**+Athul S Nair** ground loop ? so no power for LLC, maybe try to power LLC from another 5v source.


---
**Athul S Nair** *May 24, 2016 16:25*

**+Stephane Buisson** "ground loop" what did you mean?



I tried powering HV side of LLC from a DC supply, but same result . Here also if I disconnect WP voltage will raise to 5V.



Can anyone give me links of laser PSUs having same control like me?

Also what's the max current can be given by the 5V pin?mine is 20ma, isn't enough?







What I don't understand is that if I directly connect smoothie laser pin to IN without any LLC , I get 3.3 V  at IN, no reduction in voltage. Problem occurs when connect via LLC.

Could it be an LLC issue?

Can you tell me the MOSFET name in your LLC?


---
**Stephane Buisson** *May 24, 2016 16:33*

when you have voltage back from the ground, you are not at 0 anymore so the 5V-G isn't 5V,

you think G=0 but not anymore leading your logic to be mistaken. ground is a problem in this machine. said in a different way your ground leak some voltage.


---
**Athul S Nair** *May 24, 2016 16:40*

**+Peter van der Walt**  can you give the link, I could only find one directional LLC in spark fun, 


---
**Athul S Nair** *May 24, 2016 16:43*

**+Stephane Buisson**any way to solve that?




---
**Stephane Buisson** *May 24, 2016 16:48*

it's ennoying to solve, try another power source (old phone charger on main, not connected to K40 PSU) at least for confirmation


---
**Athul S Nair** *May 24, 2016 16:50*

**+Stephane Buisson** when I power LLC 5V side from separate power supply, I will have to connect all grounds together ,  i.e., GNDs from smoothie , laser PSU, and external DC supply, right?




---
**Stephane Buisson** *May 24, 2016 17:02*

just power the LLC from other PSU

(the 5V and 0 wire), the 0 and K40 ground ( are from different PSU with no connexion). so you have 5V differencial  for LLC (with no  ground loop). I would also try to power Smoothieboard with clean 5V, same side as LLC and connect only the PWM


---
**Athul S Nair** *May 24, 2016 17:45*

**+Stephane Buisson**I'm confused , in LLC grounds are made common,  so even if I connect 5v and gnd from external supply , it'll get connected to gnd from smoothie .

Can you give me a connection diagram ? 

a simple paper sketch would be enough 


---
**Stephane Buisson** *May 24, 2016 20:05*

I am not next to my machine, but from [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide) schema

power the smoothie independently of K40 psu. blue/ orange (orange being 0V not ground).

on the level shifter you have 2 wires (3.3v and PWM 2.5)

on LLC 5V side, 3 wires 0 and 5V from mobile PSU then PWM to in via pot.(for your PSU not my case) don't forgot 5v (mobile charger to pot). so the pot is PWM 0V (mobile charger) at one end, 5V(mobile charger) at the other end, connected to PSU IN in the middle.

because you have a differential of 5V, (from mobile charger) your level shifter will raise it by 5V (and not 3.6 because 0V and ground are not connected to level shifter in)before being reduce by the pot to the desired value.

level shifter and smoothie could have the same PSU (but not the k40 one, mobile charger is not grounded)


---
**Athul S Nair** *May 25, 2016 17:59*

**+Stephane Buisson**my laser psu doesn't have a 24V port so I'm powering my smoothie from a separate SMPS(12V). 

On 3.3V side of LLC ,  3.3v and GND is taken from smoothie board, and on 5V side , 5V and GND taken from laser PSU. In LLC. Both grounds are made common I.e., GNDs of smoothie and laser PSU are connected together .



In your connection also I'm pretty sure that GNDs of both 5V side and 24 V of your laser PSU are common and you power smoothie from that 24 and LLC from that 5 V, that means all GNDs connected together.

Now say if I power 5 V side of LLC from separate adapter, and 3.3 V side same as before (from smoothie), so GNDs of adapter and smoothie are made common. Now I need to connect GND of smoothie with laser PSU. Simply all ground are connected together.

 Also I don't have any pot in my IN terminal only PWM from smoothie


---
**Athul S Nair** *May 26, 2016 09:06*

**+Stephane Buisson** 

[https://drive.google.com/open?id=0B5_IWI68ZqZTZzVyRHB1RlRLeTg](https://drive.google.com/open?id=0B5_IWI68ZqZTZzVyRHB1RlRLeTg)

Is this hat you mean?



If not, please correct it


---
**Athul S Nair** *May 26, 2016 09:52*

**+Peter van der Walt** [https://drive.google.com/open?id=0B5_IWI68ZqZTcF9BVUd4OHBUbUE](https://drive.google.com/open?id=0B5_IWI68ZqZTcF9BVUd4OHBUbUE)



like this?



Can't I directly connect WP to GND, that's what I did when running with smoothie?



How can I calculate the laser PWM frequency?


---
**Athul S Nair** *May 26, 2016 10:10*

**+Peter van der Walt** My configuration is same as yours and I already setup a laser fire pin. 



But 3.3V pwm at LV side only giving me 3.6V PWm at HV siide. I measured all the voltage using DMM



HV -GND=  4.98V

LV-GND  =  3.3V

LV1-GND  = 3.3V (S1 COMMAND)

HV1-GND  = 3.6V



[https://drive.google.com/open?id=0B5_IWI68ZqZTbEp1TGVQOVFzWm8](https://drive.google.com/open?id=0B5_IWI68ZqZTbEp1TGVQOVFzWm8)



This is my current connection please take a look at this


---
**Athul S Nair** *May 26, 2016 10:15*

**+Peter van der Walt** 

 Did you check my connection, I want someone to  make sure connection is correct except for wp through some protection, I will set up it as soon as I get a water flow sensor

[https://drive.google.com/open?
id=0B5_IWI68ZqZTbEp1TGVQOVFzWm8](https://drive.google.com/open?%0Aid=0B5_IWI68ZqZTbEp1TGVQOVFzWm8)



How can I calculate the PWM freq?


---
**Athul S Nair** *June 01, 2016 08:21*

**+Peter van der Walt** **+Stephane Buisson** 

I figured out what's the problem.



10K pull up resistor on the HV side of logic level converter was too large in my case. I figured this out by doing,



1. connected 5V to IN terminal by a wire , measured voltage across it. Got 4.95 on DMM.

2. Connected 10K between IN and 5V got 3.7V if TL is not grounded and 4.04 when grounded. What I need is 5V when TL is LOW.

3. Connected 1K between in 5V and IN , got 4.8V at both TL disconnected from GND and at TL is LOW



I think this is due to the high input impedance of IN terminal 



[https://learn.sparkfun.com/tutorials/pull-up-resistors](https://learn.sparkfun.com/tutorials/pull-up-resistors)



So replaced 10K with 1k in logic level converter and I'm getting around 4.8V for S1(100% duty cycle)



Also ordered [https://www.sparkfun.com/products/11771](https://www.sparkfun.com/products/11771)  and CD4050 .

I'll check those too


---
**Stephane Buisson** *June 01, 2016 08:30*

good to know, thx


---
**Alex Krause** *June 02, 2016 14:41*

This is the level shifter I purchased 

[https://www.sparkfun.com/products/11771](https://www.sparkfun.com/products/11771)


---
**Athul S Nair** *June 03, 2016 07:22*

**+Alex Krause** did you check the voltage b/w IN and GND?


---
**Athul S Nair** *June 03, 2016 07:47*

**+Peter van der Walt** [https://drive.google.com/folderview?id=0B5_IWI68ZqZTTlZyeEx0aDdmRGc&usp=drive_web](https://drive.google.com/folderview?id=0B5_IWI68ZqZTTlZyeEx0aDdmRGc&usp=drive_web)



Sorry I didn't see that request


---
**Athul S Nair** *June 03, 2016 08:15*

[https://drive.google.com/open?id=0B5_IWI68ZqZTMEllamlZX3lQWVE](https://drive.google.com/open?id=0B5_IWI68ZqZTMEllamlZX3lQWVE)



Do I have to remove # from all the lines?


---
**Alex Krause** *June 03, 2016 16:50*

**+Athul S Nair**​ on my laser PSU? Not sure where you want me to check


---
**Athul S Nair** *June 04, 2016 06:16*

**+Alex Krause** yeah between IN and GND for S1 command


---
**Alex Krause** *June 04, 2016 06:19*

It's labeled as 5v on the PSU but I haven't thrown a multimeter on it yet


---
**Athul S Nair** *June 04, 2016 06:19*

**+Alex Krause** 

can you check it?


---
**Alex Krause** *June 04, 2016 06:23*

In the idle state or under movement?


---
**Athul S Nair** *June 04, 2016 06:26*

**+Alex Krause** Idle state it would be 0, I want know the voltage when doing a job with full laser power(S1)


---
**Alex Krause** *June 04, 2016 06:28*

I can check but I have an analog machine don't want to go above 15ma for the sake of keeping my tube operational


---
**Alex Krause** *June 04, 2016 06:39*

@15 ma I get 3.48v @10ma I get 3.63v @ 5ma I get 4.18v @ 1ma I get 4.78v.... I have to test the output of the potentiometer to get my reading my terminals are covered in hot glue on the PSU


---
**Athul S Nair** *June 04, 2016 06:42*

**+Alex Krause** Those current readings are form your laser machine?

I mean current taken by the tube ?


---
**Alex Krause** *June 04, 2016 06:42*

**+Athul S Nair**​ it looks like my In pin is pulled Low for more power


---
**Alex Krause** *June 04, 2016 06:45*

I have 5v across the in pin when the machine is idle 


---
**Athul S Nair** *June 04, 2016 06:46*

**+Alex Krause** Do you have any idea what should be the current to get the max. laser power?



If voltage is reducing ,that could be due to low current, right?


---
**Alex Krause** *June 04, 2016 06:50*

Voltage reading for max power? I can't test for current without disconnecting wire and putting my meter in series with the circuit. I have been told not to exceed 18ma of current on my overdriven 40w k40 tube... which is more like a 30-35w tube. So being safe I typically don't drive it more than 10ma to keep longevity of my hardware


---
**Alex Krause** *June 04, 2016 06:55*

I know there are a few different power supplies for the K40 some pull down and some pull up to initiate fire


---
**Athul S Nair** *June 04, 2016 07:00*

**+Alex Krause** okay thanks :)


---
**Stephane Buisson** *June 04, 2016 07:05*

**+Alex Krause** please identify your PSU, maybe we could find any technical documentation. (and find out how many different we have).


---
*Imported from [Google+](https://plus.google.com/114978037514571097180/posts/cFoouZqQAng) &mdash; content and formatting may not be reliable*
