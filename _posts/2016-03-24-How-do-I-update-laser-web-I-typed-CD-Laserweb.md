---
layout: post
title: "How do I update laser web, I typed CD Laserweb;"
date: March 24, 2016 05:00
category: "Software"
author: "Andrew ONeal (Andy-drew)"
---
How do I update laser web, I typed CD Laserweb; chmod +x update.sh; ./update.sh



Said chmod not recognized?





**"Andrew ONeal (Andy-drew)"**

---
---
**Andrew ONeal (Andy-drew)** *March 24, 2016 05:51*

Awesome thank you, I had asked a question a while back under another blog about #define laser_PWM xxxxxx // HERTZ because laser performs differently when this value is not set correctly for whichever program being ran ie laser web, inkscape plugin, j-tech, imag2gcode etc.  After digging and digging I couldn't find much of an answer so I decided to play with inputs based on code being used in Marlin and gcode files. I ended up using (50000/2) // HERTZ and seemed to work but I have no way to know for sure since I'm clearly not a code guy. Some programs would only work at 25000 HERTZ while others required 50000 HERTZ. Did I do right, did this fix the issue or mask it? Just don't want to hurt the laser or process by just changing in and out numbers i know nothing about. Also getting low memory error from all the added bloat in the firmware left over from modded 3dprinting firmware. Thanks again


---
**Andrew ONeal (Andy-drew)** *March 24, 2016 18:46*

Which board specifically and is the mod much different than what I have with Marlin or will it be plug in play ?


---
**Andrew ONeal (Andy-drew)** *March 25, 2016 06:03*

Checked out boards on ebay there are several types which do you suggest and will it connect in the same way the ramps did? 


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/Y6rgcjE3r6a) &mdash; content and formatting may not be reliable*
