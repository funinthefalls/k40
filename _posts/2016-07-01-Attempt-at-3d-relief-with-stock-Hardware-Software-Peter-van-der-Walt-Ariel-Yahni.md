---
layout: post
title: "Attempt at 3d relief with stock Hardware/Software Peter van der Walt Ariel Yahni"
date: July 01, 2016 20:23
category: "Object produced with laser"
author: "Alex Krause"
---
Attempt at 3d relief with stock Hardware/Software

**+Peter van der Walt**​

**+Ariel Yahni**​



![images/66e9949bbfcf7510c4faee0814176ea2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/66e9949bbfcf7510c4faee0814176ea2.jpeg)
![images/c58eccb530a068c6896bb8152c4ebd7d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c58eccb530a068c6896bb8152c4ebd7d.jpeg)
![images/7489e6c8d71de8b3bdd2c0d5d30f4542.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7489e6c8d71de8b3bdd2c0d5d30f4542.jpeg)

**"Alex Krause"**

---
---
**Ariel Yahni (UniKpty)** *July 01, 2016 20:25*

Settings 


---
**Alex Krause** *July 01, 2016 20:28*

**+Ariel Yahni**​ dither image first (Floyd sternberg) 2pass,First pass 11ma-150mm/s second pass 5ma-450mm/s. Done in black walnut


---
**David Cook** *August 02, 2016 22:14*

not bad at all.  Hoping Smoothie/LaserWeb combo gets me closer


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/8RDfPmTLDyo) &mdash; content and formatting may not be reliable*
