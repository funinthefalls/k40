---
layout: post
title: "This guys been modding K40's since they first started coming out of China (2008-2009) He swapped out the old moshi board with an old school Rolland X-Y plotter control board (wow!) Anyone know who this guy is?"
date: February 28, 2017 16:57
category: "Discussion"
author: "Madyn3D CNC, LLC"
---
This guys been modding K40's since they first started coming out of China (2008-2009)



He swapped out the old moshi board with an old school Rolland X-Y plotter control board (wow!) 



Anyone know who this guy is? Or better yet, perhaps here in the k40 community? It's people like this who I enjoy keeping in my circles to learn from :) 

[http://www.synthfool.com/laser/](http://www.synthfool.com/laser/)





**"Madyn3D CNC, LLC"**

---
---
**Mark Brown** *March 01, 2017 14:48*

Think he just really got a lemon or has their quality control come up a lot since then?



Mine arrived perfectly intact.


---
**Madyn3D CNC, LLC** *March 01, 2017 15:46*

This was in 09, back when they had the garbage moshi boards in and real shoddy hardware, they were much worse than what we have now. Back then, these machines were not even marketed as "laser engravers/cutters", they were just "stamp machines".



 Here in the US and other European countries, we do signatures. We sign papers, documents, etc.. In China and other Asian-Pacific countries, they have a thing for annular stamps. [begin sarcasm] You're not a "real" professional or businessman unless you have your own stamp [end sarcasm]. 



They are just red, circular stamps that represent a person just as our signature does over here. Google should have more on that if you're interested. 



So needless to say, these machines in China are just stamp machines. Once TPP happened and we had direct access to Chinese goods as consumers, these lasers started coming in to the USA as "laser cutters/engravers" and it took a few years of upgrades and mods to get them up to par with an actual laser cutter/engraver that can handle more than just burning a 2"x2" piece of rubber.



So that's that, hopefully I was able to spread some history and knowledge on these K40's to anyone reading, but if I'm just parroting something you already know, then apologies! ;)


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/YbMT1acR2Ja) &mdash; content and formatting may not be reliable*
