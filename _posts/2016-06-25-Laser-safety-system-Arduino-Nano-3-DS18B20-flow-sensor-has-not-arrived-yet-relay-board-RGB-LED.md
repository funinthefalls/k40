---
layout: post
title: "Laser safety system: Arduino Nano + 3 DS18B20 + flow sensor (has not arrived yet) + relay board + RGB LED"
date: June 25, 2016 17:16
category: "Modification"
author: "Mircea Russu"
---
Laser safety system: Arduino Nano + 3 DS18B20 + flow sensor (has not arrived yet) + relay board + RGB LED. 3D printed LCD case and electronics support plate.



![images/477d3871351f6a536c1c6643e61ac436.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/477d3871351f6a536c1c6643e61ac436.jpeg)
![images/c1d3f01600e1aa8153d3edf453be5701.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1d3f01600e1aa8153d3edf453be5701.jpeg)

**"Mircea Russu"**

---
---
**Anthony Bolgar** *June 25, 2016 17:27*

Have you thought about adding door interlock switches to the system to disable laser if door is open?


---
**Derek Schuetz** *June 25, 2016 17:38*

Nice clean wiring


---
**Mircea Russu** *June 25, 2016 17:48*

The door switch is wired directly, for me that is the most critical safety feature. The Laser Enable switch is in series with door interlock and relay driven by Arduino.


---
**Anthony Bolgar** *June 25, 2016 17:53*

OK, got ya.


---
**Manuel Conti** *June 25, 2016 18:12*

Great! I'm waiting the tutorial :-) 


---
**Ray Kholodovsky (Cohesion3D)** *June 25, 2016 19:04*

**+Brian Bland** thoughts? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 19:29*

That's cool. Would be handy to be able to see those values.


---
**Mircea Russu** *June 26, 2016 07:36*

The original LaserSafety is on github. He uses some analog temp sensors and I chose ds18b20 the steel waterproof model as I have plenty of them. The plastic parts I drew up in a hurry in SketchUp and 3d printed. You can find similar on Thingiverse but I was too lazy to search and measure :)


---
**Kris Sturgess** *October 30, 2016 23:00*

Mircea, Did you modify code at all??


---
**Mircea Russu** *October 31, 2016 08:05*

No, I'm running the code on github.





[https://github.com/executivul/LaserSafety](https://github.com/executivul/LaserSafety)


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/JSFnHCPN8hJ) &mdash; content and formatting may not be reliable*
