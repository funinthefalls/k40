---
layout: post
title: "Does anyone have any recommendations on what air hose to use for air assist?"
date: December 26, 2015 15:20
category: "Modification"
author: "Nathaniel Swartz"
---
Does anyone have any recommendations on what air hose to use for air assist? Right now I'm using a polyethylene air hose (from  [http://www.harborfreight.com/20-pc-air-compressor-starter-kit-61310.html](http://www.harborfreight.com/20-pc-air-compressor-starter-kit-61310.html)) but it seems too rigid, should I go get a rubber hose instead?﻿





**"Nathaniel Swartz"**

---
---
**Brooke Hedrick** *December 26, 2015 15:50*

I use 5x8 flexible tubing.  It is soft and flexible, but holds well enough to be routed for air assist.



[http://www.lightobject.com/5X8-Silicon-Flex-Tube-for-Air-Assistance-or-Water-Cooling-P776.aspx](http://www.lightobject.com/5X8-Silicon-Flex-Tube-for-Air-Assistance-or-Water-Cooling-P776.aspx)


---
**Coherent** *December 26, 2015 17:57*

What works well is to put a fitting (preferably with an adjustable valve attached) on the back or side of the unit and on the inside have it connected with a barb to silicone or small vinyl tubing which is much more flexible when attached to your laser head.


---
**Coherent** *December 29, 2015 18:43*

I've posted this elsewhere, but here's a link to a photo of the air connector and valve I used. My total cost was about $10 and it used silicone tubing to the head on the inside.

[http://k40d40lasers.freeforums.net/thread/28/coolant-flow-sensor-air-valve](http://k40d40lasers.freeforums.net/thread/28/coolant-flow-sensor-air-valve)


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/MTbaLzfzVqm) &mdash; content and formatting may not be reliable*
