---
layout: post
title: "Found a couple of great online picture conversion programs"
date: February 03, 2018 22:08
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Found a couple of great online picture conversion programs.



[http://www.snapstouch.com/default.aspx](http://www.snapstouch.com/default.aspx)



[https://online.rapidresizer.com/photograph-to-pattern.php](https://online.rapidresizer.com/photograph-to-pattern.php)





**"HalfNormal"**

---
---
**Anthony Bolgar** *February 03, 2018 22:42*

Thanks for sharing!




---
**BEN 3D** *February 04, 2018 10:50*

rapidresizer sounds good, is it free?


---
**HalfNormal** *February 04, 2018 15:34*

They are both free!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/PjMHZDGJmii) &mdash; content and formatting may not be reliable*
