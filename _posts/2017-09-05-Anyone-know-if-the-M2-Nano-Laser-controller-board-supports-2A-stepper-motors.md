---
layout: post
title: "Anyone know if the M2 Nano Laser controller board supports 2A stepper motors?"
date: September 05, 2017 11:31
category: "Hardware and Laser settings"
author: "J edd"
---
Anyone know if the M2 Nano Laser controller board supports 2A stepper motors? Looking to get a rotary attachment, so if anyone knows of one that works with the M2 Nano controller, I'd appreciate your insites.





**"J edd"**

---
---
**Jim Fong** *September 05, 2017 13:27*

The m2nano stepper drivers are Allegro A4985 which are 1amp max output for stepper motors. They are setup to deliver less than that. I think it is about 0.85amps. 



The M2nano only has two stepper drivers for X and Y. You can't hook up a third rotary stepper motor to the board.  The OEM software doesn't support rotary axis anyway. 



Most users upgrade to a  Cohesion3d Mini which supports 4 stepper motors.  You can use laserweb which supports rotary.  



I have a C3d Mini, works great.  


---
**Joe Alexander** *September 05, 2017 13:40*

what jim said, website below. By far the most supported open-source controller board for the K40 lasers. 

[cohesion3d.com - Cohesion3D: Powerful Motion Control](http://cohesion3d.com/)


---
**J edd** *September 05, 2017 13:42*

Thanks Jim and Joe! I really appreciate it. I'll order a Cohesion controller today.  



Any particular rotary attachment you like better than others?


---
**Joe Alexander** *September 05, 2017 15:03*

i actually made my own, there was a thingiverse file posted a while back for it on the K40 group.


---
**Ray Kholodovsky (Cohesion3D)** *September 05, 2017 16:56*

I would add that the the A4988 drivers we include are probably around 1amp max as well. It's enough for the XY to drive the head, and has been enough for some homebrew z tables and rotary attachments, but not enough power for something like the Lightobject ones.  



We have a guide for wiring these using our external stepper adapters and external drivers like TB6600 here: [cohesion3d.freshdesk.com - Wiring a Z Table and Rotary: Step-by-Step Instructions : Cohesion3D](https://cohesion3d.freshdesk.com/solution/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)



The PUS in the k40 is insufficient and a separate external 24v psu is need so you don't blow the k40 one. 


---
**J edd** *September 05, 2017 17:58*

Thanks Ray and Joe. One follow-up question. Which 1A stepper motor is recommended?




---
**Ray Kholodovsky (Cohesion3D)** *September 05, 2017 18:00*

No specifics. You can run a motor at any current. You'd use a Nema 17 stepper that is 40 or 48mm long. The 48mm ones are around 1.8 amps max. I run them off A4988's on my 3d printers. Either the driver will be able to move it with enough power for your setup, or it won't. 

The current is adjusted on the stepper driver, if that helps make more sense. 


---
**Joe Alexander** *September 05, 2017 19:12*

i use 17hs-0404s steppers, they are really quiet and dont get as hot as a 12v stepper with higher impedance(the ohm value, 3-4 vs 24)


---
**J edd** *September 10, 2017 21:23*

Would this motor work or do I need something that is more matched with the motors that are already in the K40?



[amazon.com - Amazon.com: Zyltech Nema 17 Stepper Motors 1.5 A 0.42 Nm 59  40mm Body w/ 1m Cable & Connector for 3D Printer/CNC: Industrial & Scientific](https://smile.amazon.com/Zyltech-Stepper-Motors-Connector-Printer/dp/B01GNAN2U0/ref=sr_1_15?ie=UTF8&qid=1505078444&sr=8-15&keywords=nema+17+stepper+motor)


---
**Ray Kholodovsky (Cohesion3D)** *September 10, 2017 21:29*

Seems fine to me, and a pretty good price!


---
*Imported from [Google+](https://plus.google.com/106957590634526127966/posts/hforiwFa7mJ) &mdash; content and formatting may not be reliable*
