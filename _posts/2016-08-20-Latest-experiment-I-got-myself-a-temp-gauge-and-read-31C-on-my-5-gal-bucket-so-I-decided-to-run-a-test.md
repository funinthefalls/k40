---
layout: post
title: "Latest experiment. I got myself a temp gauge and read 31C on my 5 gal bucket, so I decided to run a test"
date: August 20, 2016 02:43
category: "Hardware and Laser settings"
author: "Ariel Yahni (UniKpty)"
---
Latest experiment. I got myself a temp gauge and read 31C on my 5 gal bucket, so I decided to run a test. Measure at ambient and then drop in some cold on the bucket, it reached 25C. It seems I can gain some important cut depth. Same settings, speed, cut location was used﻿

![images/db417356ea52feeddab556e2e44e4acb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db417356ea52feeddab556e2e44e4acb.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Eric Flynn** *August 20, 2016 03:11*

For sure.  30C is just too hot. 25C is as well, but 5 degrees makes a huge difference.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 20, 2016 03:14*

That's cool to know. I'm going to have to monitor my water temp then.


---
**Robert Selvey** *August 20, 2016 05:11*

What is the best way to keep the temps down in a 5 gallon bucket ?


---
**Eric Flynn** *August 20, 2016 05:14*

Add some ice packs for a temporary fix.  If you want to just stay at ambient, a radiator with a fan will suffice.  If you want a permanent solution that will get you below ambient, a chiller of some sort is required.


---
**Robert Selvey** *August 20, 2016 05:36*

Would making the tubing longer going from the pump in a coil in a cooler filled with ice then into the tube work ?


---
**Eric Flynn** *August 20, 2016 05:36*

Yes, absolutely, as long as your pump can handle it.  the stock pump likely will not.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 20, 2016 06:16*

**+Eric Flynn** Wouldn't it depend on the head flow of the pump vs where the ice-cooler is located? e.g. I run my pump & 35L tank above head height. Although, then again it has to push back up above head height too.


---
**Eric Flynn** *August 20, 2016 06:17*

Yes indeed.


---
**HP Persson** *August 20, 2016 07:02*

Just be careful when adding cold water, if you get a non mixed fluid it´s possible to crack the tube. I was careless one time and dumped 2L ice cold water into the 5gallon bucket, took a couple of minutes until i heard a "click" and water started raining :)



Now i have a computer radiator outside my window with double pumps, to cool the water. But if you are in a hot country that wouldnt work.


---
**greg greene** *August 20, 2016 21:55*

wow,  I never considered the temp of the cooling water as being that significant to the efficiency of the tube - Thanks for the helpful info


---
**Gunnar Stefansson** *August 21, 2016 01:43*

Do also consider what kind of pump you're using in my case, with around 1.5liters in the system the pump alone heats the water about 3 degrees celcius, minor thing but also adds abit, maybe not as much in a 5gallon setup, but depends on the pump.


---
**Robert Selvey** *August 21, 2016 02:48*

I'm going to have my line run through a copper tube coil in a cooler with ice and water.


---
**Robert Selvey** *August 23, 2016 16:08*

Was just thinking is it safe to run the water through copper tubing. I don't want any break down of metals in the water. So should I run plastic tube instead in the cooler ?


---
**greg greene** *August 23, 2016 16:13*

They run pure alcohol in copper tubing - should be safe enough for water.


---
**Ariel Yahni (UniKpty)** *August 23, 2016 16:14*

Will the copper tubing break that easily?  Copper is a very good disipator just yesterday was at the hardware store looking for those, the only reason I did not was I cannot solder them and they did not have any other solution 


---
**Robert Selvey** *August 24, 2016 01:10*

I wonder if running a the copper tubing through the ice water if it would cause condensation and if that would be bad. I have my laser in the garage and in the day time it can hit 95f out right now so just trying to find the best way to keep the tube cool. Welcome all suggestions please.


---
**greg greene** *August 24, 2016 01:17*

try an aquarium chiller?


---
**Eric Flynn** *August 24, 2016 01:59*

Greg, that would obviously work, however most of us a cheapskate DIY'ers and would prefer a cheapskate DIY solution.


---
**Robert Selvey** *August 24, 2016 02:56*

Yes the cheaper the better..:)


---
**greg greene** *August 24, 2016 12:50*

85 bucks on ebay


---
**adrian miles** *December 18, 2016 09:13*

what thickness material is that




---
**Gunnar Stefansson** *December 18, 2016 10:25*

Looks like 5~6mm thick to me. But hard to say from the picture!


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/LKcHD91g4Lo) &mdash; content and formatting may not be reliable*
