---
layout: post
title: "+ALPHAHOBBIES (may have the wrong alias as is did not resolve) is adding a analog meter to his K40 which has the digital panel"
date: April 07, 2017 14:32
category: "Modification"
author: "Don Kleinschnitz Jr."
---
+ALPHAHOBBIES (may have the wrong alias as is did not resolve)



is adding a analog meter to his K40 which has the digital panel.



I think **+Bob Buechler** did this add but I cannot find the post and thread.



If either of you, or someone, can point me to the thread I will create a formal blog post on this conversion.











**"Don Kleinschnitz Jr."**

---
---
**ALFAHOBBIES** *April 07, 2017 15:53*

I must have messed up my post somehow. I'll post more later when to show what I am up to. Thanks Don.


---
**HP Persson** *April 07, 2017 18:30*

The connection is the same as on the analog versions. In series with the negative cable from the tube, and from meter to power supply ground.

Or was it something else discussed earlier you searched for? :)


---
**Bob Buechler** *April 07, 2017 18:48*

There's this thread: [plus.google.com - I bought the digital panel version of the K40, which does not come with the a...](https://plus.google.com/+BobBuechler/posts/cggqu9Rjh85)



I've stalled a bit on this due to competing priorities and I'm still skittish on modifying the stock wiring... especially related to the tube itself. 



Here's an updated diagram that (hopefully) indicates that the L- and FG on the LPS are connected internally:



[https://goo.gl/photos/tDrAkFzj4KLQ5EGi6](https://goo.gl/photos/tDrAkFzj4KLQ5EGi6)


---
**ALFAHOBBIES** *April 07, 2017 18:52*

Thanks for the info on the tube part. 


---
**ALFAHOBBIES** *April 07, 2017 19:11*

Here is what I was wondering about. I would like to change from the digital readout to just the pot and meter like the old k40 lasers. Wonder what wires go to the pot like Don did in his things.

![images/2ff4c4a1e1a9b6ab177b53c3a38df501.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2ff4c4a1e1a9b6ab177b53c3a38df501.jpeg)


---
**Bob Buechler** *April 07, 2017 19:13*

Are you using the stock controller (m2 nano)?


---
**ALFAHOBBIES** *April 07, 2017 19:16*

I'm getting rid of the m2 nano and going with the cohesion 3d mini.


---
**ALFAHOBBIES** *April 07, 2017 19:24*

![images/ba9c7de0d3ef28a0d64a3fd3a5bd589e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ba9c7de0d3ef28a0d64a3fd3a5bd589e.jpeg)


---
**HP Persson** *April 07, 2017 19:25*

If you have this PSU, it´s the connector where the green arrow points to. (ignore the red arrow)



G, IN, 5V from left to right, it´s also stamped on the board.

![images/ebc7db83e537733527dd29db9298bd7d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ebc7db83e537733527dd29db9298bd7d.jpeg)


---
**ALFAHOBBIES** *April 07, 2017 19:27*

I have the power supply in the above picture with the 3 green connectors.


---
**HP Persson** *April 07, 2017 19:29*

I was too quick, here´s your pic with text added ;)

![images/0bfd47b0374166bf4f56aae92d460060.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0bfd47b0374166bf4f56aae92d460060.jpeg)


---
**ALFAHOBBIES** *April 07, 2017 19:29*

The wires on the digital readout board look like this. Can the pot replace the board?

![images/027f829b983a5eed6d02df04614a04e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/027f829b983a5eed6d02df04614a04e5.jpeg)


---
**ALFAHOBBIES** *April 07, 2017 19:32*

We're both to fast. I understand what wires from the digital board go to the pot now.


---
**ALFAHOBBIES** *April 07, 2017 19:34*

So I bet the red wire just above the G wire on the power supply is the laser test on the digital board? 


---
**HP Persson** *April 07, 2017 19:39*

Yes, red is test, add a button to that one and ground on the other side, so when button is pressed the red is grounded. But use a button that doesn't lock/latch in pressed position (forgot the English word for it :) )

Follow Don´s awesome guide for the better pot solution and you are done :)



The two purple ones are laser enable, do you have that button now?


---
**ALFAHOBBIES** *April 07, 2017 19:49*

That's exactly what I'm doing. Don's pot solution. Part's should be here tomorrow. I have a switch connected to the purple laser enable wires. I'll hook up a momentary push button switch to the laser test red wire too. What ground on the power supply is best to use for the test fire switch? The G one with the orange wire on it?


---
**HP Persson** *April 07, 2017 19:54*

Yeah, orange works fine to test-fire button.

I have converted one of my digital machines to Don´s solution too with C3D controller, not tested live yet though but it seems very good :)



When done, don´t forget to send **+Don Kleinschnitz** a beer on Paypal donation, he provides much knowledge to the K40 machines, well worth it! ;)


---
**ALFAHOBBIES** *April 07, 2017 20:08*

I'll be sure to PayPal a beer. Amazing guy helping so many people. And thank you for your help too. I learned a lot and know I'm doing things right. 


---
**Don Kleinschnitz Jr.** *April 07, 2017 23:21*

If someone wants to send me one of those digital boards I can create wiring and schematics to take away the mystery. 


---
**HP Persson** *April 08, 2017 10:23*

**+Don Kleinschnitz** May it help with high resolution pics of it? I´m in Sweden but i can send you mine if it helps better.


---
**Don Kleinschnitz Jr.** *April 08, 2017 11:37*

**+HP Persson** Sure lets try the pictures, I have done that before. 



I am sure you know this but if you can take them from above as parallel to the board as possible that is the best. 



Take one of the top and one of the bottom from the same distance but including the entire board. 



I take the pictures, print them out and then glue them to either side of a substrate so it is close to feeling like a real board :)



Take any other pictures that you think will help especially where traces disappear or the chip numbers are hard to read in the larger picture. 



Thanks in advance!


---
**HP Persson** *April 08, 2017 12:00*

**+Don Kleinschnitz** Sure, i have a overhead camera rig for my DSLR so no problems with pics. I´ll email you a zip-file later when done!


---
**ALFAHOBBIES** *April 08, 2017 12:36*

Another question I have is about the PWM cable that comes with the cohesion 3d mini. It seems that it is meant to take the place of the pot control. I thought I heard that some people are using the cable and the pot together to act as a cut off and limit the power. Just asking because with out the PWM cable I wonder if the laser power would not change by itself and would only be manually controlled by the pot.



If this is the case I how would you wire the pot and PWM cable to work together?


---
**Don Kleinschnitz Jr.** *April 08, 2017 12:44*

**+HP Persson** again you ROCK!


---
**Don Kleinschnitz Jr.** *April 08, 2017 12:45*

**+ALFAHOBBIES** I will let Ray tell you the latest on wiring.


---
**ALFAHOBBIES** *April 08, 2017 13:13*

Just did a lot of reading on the cohesion group and it seems I don't need the PWM cable. Seems the cable can be used to bypass the pot. But it is recommended to stay with the pot or the digital board depending on what you have or like better. PWM is controlled by gcode and the smoothie.


---
**Don Kleinschnitz Jr.** *April 08, 2017 13:33*

**+ALFAHOBBIES** you always need a PWM connection to the controller and I think the latest recommendation from RAY is the L connection on the LPS. You can elect to use or not use the POT but I think Ray recommends leaving it in.



My recommendation is both PWM on L and pot installed.



**+Ray Kholodovsky**?




---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 15:20*

You don't wire the "pwm cable" that comes with the Cohesion3D kit. You keep the digital panel or pot connected. That sets the ceiling for your power level, then software controls the power within that range. 


---
**ALFAHOBBIES** *April 08, 2017 15:55*

Ray, thanks. You say in the install instructions to put the SD card in the slot. It has software for the board. Does it run that software right off the SD card? So I just leave the SD card in all the time so it can run from it?


---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 15:57*

Put card in, leave card in. 


---
**ALFAHOBBIES** *April 08, 2017 15:58*

I only ask because I'm used to 3d printers that have memory on them and do not need an SD card to hold setting data.


---
**HP Persson** *April 13, 2017 15:55*

**+Don Kleinschnitz** , emailed you a couple of days ago, check spam-folder. May ended up there ;)


---
**Don Kleinschnitz Jr.** *April 13, 2017 16:24*

**+HP Persson** can't find any mail?? Try again or over to hangouts?


---
**HP Persson** *April 13, 2017 16:31*

**+Don Kleinschnitz** I sent from my gmail instead now, maybe works better :)


---
**Don Kleinschnitz Jr.** *April 16, 2017 13:25*

**+HP Persson** and I shared pictures and concluded that creating a schematic is not a useful exercise since some parts at blanked out and it is likely its a microprocessor anyway. This will not stop us from creating a "How To" guide.



**+HP Persson** here is what I took from our conversation and added the wire colors from **+ALFAHOBBIES** pictures above. Can you verify.



Gnd orange

In     yellow

5V    white

P+    n/c  (sometimes actually  wired to K+ but open in the wiring)

P-     Red  (actually wired to K-)


---
**Don Kleinschnitz Jr.** *April 16, 2017 13:47*

BTW does this digital panel fit into the standard K40 cutout in the right cover? If so it would be nice to have a cut outline for a new panel to go with this guide that allows the panel to stay in place with the pot and its digital voltmeter added.  Don't know if there is room?




---
**HP Persson** *April 16, 2017 16:34*

Seems like it´s routed different on the two types of PSU´s existing.



I made this graphic a couple of days ago, and it seems like this is how they use it.

Only thing that seems to differ is one less GND on the lower one, and P+/GND is swapped around.



PSU with small white connectors.

From 5V on PSU to 5V on panel

From IN on PSU to IN on panel

From GND on PSU to G on panel

From K+ on PSU to P- on panel

From K- on PSU to -- not connected.



PSU with all green connectors

From 5V on PSU to 5V on panel

From IN on PSU to IN on panel

From GND on PSU to G on panel

From L on PSU to P- on panel





![images/5d8ad4d98da7302fd6ab2cb6fca27569.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5d8ad4d98da7302fd6ab2cb6fca27569.jpeg)


---
**Don Kleinschnitz Jr.** *April 16, 2017 17:02*

**+HP Persson** You did the heavy lifting I will blog post this in aggregate soon :).


---
**ALFAHOBBIES** *April 17, 2017 10:08*

Here is where I wired the pot in the wire colors are shown Red Orange Yellow White.



![images/e86be154417d87103dc19c25784c74f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e86be154417d87103dc19c25784c74f1.jpeg)


---
**Don Kleinschnitz Jr.** *April 17, 2017 12:00*

**+ALFAHOBBIES**thanks for the drawing. Did you completely remove the board from the panel and replace it with a push button pot and digital meter along with other components like the power switch?

Can you give me a drawing of the hole it came out of so we can suggest a panel design?




---
**HP Persson** *April 17, 2017 12:21*

I had this panel inside the lid of the K40, made for the digital one.

But, it had a "membrane" on top to make out the buttons, like a thick vinyl with printed buttons on them.

This fits the digital panel + three switches

![images/f484321789442f3b1482424d56d69e98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f484321789442f3b1482424d56d69e98.jpeg)


---
**ALFAHOBBIES** *April 17, 2017 13:41*

Yep, I completely removed the digital panel and replaced it with the pot and meter. I no longer have the panel so I can't measure it. I also added a push button switch to test fire the laser as shown in the drawing. I made a new panel out of plastic.


---
**ALFAHOBBIES** *April 17, 2017 13:44*

I really like the digital volt meter and pot conversion. The volt meter makes it super easy and quick to adjust power now.


---
**HP Persson** *April 17, 2017 13:54*

**+ALFAHOBBIES** I´m curious, can you show a pic of your conversion? :)

Mine is still hanging in the wires, haha


---
**ALFAHOBBIES** *April 17, 2017 15:09*

I'll post some pics tonight when I get home.


---
**ALFAHOBBIES** *April 17, 2017 20:12*

Top view

![images/b0af16c4372302e90ad83ceffff1d3b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b0af16c4372302e90ad83ceffff1d3b8.jpeg)


---
**ALFAHOBBIES** *April 17, 2017 20:12*

Underside

![images/df098f9cf0ffe44faf73776c62195446.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/df098f9cf0ffe44faf73776c62195446.jpeg)


---
**ALFAHOBBIES** *April 17, 2017 20:14*

Rotated the mini board so I can get at the SD card from the top.

![images/9e98da9320af9de5be9b997d4bbab1c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e98da9320af9de5be9b997d4bbab1c0.jpeg)


---
**ALFAHOBBIES** *April 17, 2017 20:17*

I ordered a big meter by mistake so I figured with my eyes getting older I'll just use it. I really like the volt meter.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/GEC1hyLLEbn) &mdash; content and formatting may not be reliable*
