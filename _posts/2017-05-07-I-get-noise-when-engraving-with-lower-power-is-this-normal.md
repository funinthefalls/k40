---
layout: post
title: "I get noise when engraving with lower power, is this normal?"
date: May 07, 2017 00:21
category: "Discussion"
author: "Jonathan Tzeng"
---
I get noise when engraving with lower power, is this normal? Higher power at 10mw is fine, just gets noisy when it's lower. I made sure my grounding is good and used distilled water





**"Jonathan Tzeng"**

---
---
**Mark Brown** *May 07, 2017 01:37*

How long has the water been in there?  Is it a sort of squealing noise?


---
**Steve Clark** *May 07, 2017 02:35*

Just as a comparison mine makes no noises different at lower powers vs high... I'd change the water also as it's a cheap check. Have you had the laser long? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2017 03:02*

Could also be the material you are engraving. I have had certain plywoods make a funny high pitch noise when engraving or cutting it at 4mA.


---
**Jonathan Tzeng** *May 07, 2017 04:35*

It's like a squeaky noise, high pitched. I just did a flushing of my water lines with mild bleach, tap water, distilled, and then refill with fresh distilled water.



If I'm doing continuous cutting it also makes that noise at low power. I've always had this sound a low power but now am trying to make sure everything is right since I've already gone through 2 power supplies over several months.


---
**Don Kleinschnitz Jr.** *May 07, 2017 14:10*

**+Jonathan Tzeng** if you are in the US. I (the LPS ambulance chaser) wouldn't mind if you donated one to the LPS research activity :).


---
*Imported from [Google+](https://plus.google.com/111839747151212088406/posts/hnqYR98zxNy) &mdash; content and formatting may not be reliable*
