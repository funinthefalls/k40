---
layout: post
title: "Can anyone point out the simple mistake?"
date: April 11, 2017 15:36
category: "Original software and hardware issues"
author: "Frank Fisher"
---
Can anyone point out the simple mistake?  I was just setting up for my first attempt at cutting out pre-printed items and set up a flat board for alignment, drawing the size of the cut area on it.  IT ISN'T SQUARE! The A4 the laser cut into the board goes off anywhere from -3 to +3 mm.  Given how small the items I wanted to cut are this is too big a margin of error.  Where do I start sorting this?  Head alignment?  Level of table?  Adjustments somewhere else?





**"Frank Fisher"**

---
---
**Evan Ford** *April 11, 2017 15:53*

Is it off in both X and Y directions or just X or Y?  I would check belt tension first.


---
**Frank Fisher** *April 11, 2017 16:05*

it seems to be most out at top left and bottom right, coming in OK around the middles.




---
**Nigel Conroy** *April 11, 2017 17:08*

I linked to a video that helps with making sure the software is set correctly for your machine.

Corrected an error on mine over a larger area.



Can only help.



[plus.google.com - Calibrating machine using RDworks... Very interesting. I'll be trying this ou...](https://plus.google.com/+NigelConroy/posts/jVt73LMqisZ)


---
**Frank Fisher** *April 11, 2017 17:20*

Ooh a new option, software. This gets deeper!




---
**Frank Fisher** *April 11, 2017 17:50*

Oh one comment "RDworks won't work on the type of controllers that come stock with k40's." so that's out then.




---
**Stephane Buisson** *April 11, 2017 18:07*

few ideas for diagonal issue:

Lens in holder is well in the center and not up side down.

Check if laser head ok, I mean Z is vertical.

See as well if bed is horizontal at equal distance of focus point.-

 finally alignement (everything should be as closed as 90°, no mirror chasing the next one to the hit center), check tube parrallel with bed & X, ...




---
**Nigel Conroy** *April 11, 2017 19:14*

Sorry I've the K50 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2017 22:41*

Check that the Y-axis rail is square. There have been cases where people's K40s have arrived with the rail significantly closer to the back on one side of the Y-axis rail than the other.


---
**Frank Fisher** *April 12, 2017 08:26*

Yes Yusuuf while the cutting area appears square the whole rig is off 3-4 mil from the case at one end.  No idea how to adjust but would also solve the air duct being similarly angled across the top.  Does look like the simple mistake I am looking for :) but how to fix?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2017 10:42*

**+Frank Fisher** You'll probably have to unmount the whole rig & possibly redrill the holes in the base (4 bolts if I recall correct, 2 each side) on the side that is out of square. I'd consider just elongating the existing holes, then you can put the bolts in on that side & slide rig to square position & then tighten in place.


---
**Frank Fisher** *April 12, 2017 11:34*

OK got the tools, but does sound like a lost day including realigning the lens as well?  Still can maybe move the awful vent back a bit at the same time.

Thanks.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2017 13:36*

**+Frank Fisher** Yeah I think it will probably be a decent job to square it all up. Hopefully all goes well.


---
**Jeff Johnson** *April 13, 2017 17:05*

My k40 was out of square when cutting and it ended up being the belts on the left and right. I loosened the screws that held the extension on the motor and carefully adjusted the left and right sides until they were equal distance from the front of the rail. After doing this I tested a 280 x 180mm rectangle and it's less than 1mm off diagonally where before this it was off by more than 3mm and horizontal lines were slanted a little. So far, with the items I create, this hasn't been an issue, even with boxes.


---
*Imported from [Google+](https://plus.google.com/114228670405979897634/posts/5M8WKCsJqAZ) &mdash; content and formatting may not be reliable*
