---
layout: post
title: "Originally shared by Alex Krause Really liking what I can do with my k40-smootiefied setup"
date: August 12, 2016 20:05
category: "Object produced with laser"
author: "Alex Krause"
---
<b>Originally shared by Alex Krause</b>



Really liking what I can do with my k40-smootiefied #Laserweb setup

![images/1e147c31761cd42988af0988b918cd33.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e147c31761cd42988af0988b918cd33.jpeg)



**"Alex Krause"**

---
---
**Phillip Conroy** *August 12, 2016 20:28*

Nice


---
**Michael Knox** *August 12, 2016 21:43*

Really nice!!!!


---
**greg greene** *August 12, 2016 21:44*

Really Really nice !!!!


---
**Jim Hatch** *August 13, 2016 03:50*

Wow!


---
**Alex Krause** *August 13, 2016 03:51*

Thnx **+Jim Hatch**​ and everyone else :)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/4WohYftC8Uh) &mdash; content and formatting may not be reliable*
