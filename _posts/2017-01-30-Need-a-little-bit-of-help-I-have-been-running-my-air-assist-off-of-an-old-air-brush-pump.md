---
layout: post
title: "Need a little bit of help I have been running my air assist off of an old air brush pump"
date: January 30, 2017 03:40
category: "Air Assist"
author: "3D Laser"
---
Need a little bit of help I have been running my air assist off of an old air brush pump.  I have noticed lately that it is not putting out as much air as it once did and I think it will fail on me any day now as I have worked the thing to death over the past year preforming an action it was not designed to Donny running it long hours day after to day.  For once I want to be proactive in replacing it.  I was looking at an aquarium air pump but was unsure as to how big I need to get for air flow



The ones I am looking at range from about 500gph to about 1700gph.  Any idea what the best size is to get 





**"3D Laser"**

---
---
**Ned Hill** *January 30, 2017 03:56*

I've been using an airbrush pump like you.  I have a flow gauge in my system and I've been running 10-20 lpm (160-320 gph) depending on the job.  I am also using the LO air assist head.  So I think the 500 gph would be sufficient.  

As a side note, I have been using a fan (actually the stock blower) to blow on my airbrush pump to help keep it from getting too hot and hopefully prolong it's life.


---
**3D Laser** *January 30, 2017 14:31*

**+Ned Hill** yea I haven't been doing that and I noticed I had flakes of metal in my water trap the other day so I think my piston is about shot as it runs very hot all the time but a fan on the pump is a good idea 


---
**1981therealfury** *February 01, 2017 13:24*

i use an aquarium pump that is rated for 60lpm (no idea if its accurate as i don't have a flow meter to check) but it seems to be more than adequate for the job.  After a good few hours of running it might be warm to the touch, but that's about it.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/KFHtmhfxYYR) &mdash; content and formatting may not be reliable*
