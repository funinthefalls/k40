---
layout: post
title: "where is the best place to list a new K 40 for sale"
date: November 15, 2017 08:23
category: "Discussion"
author: "Padilla's custom leather"
---
where is the best place to list a new K 40 for sale.  It has been on here and my local craigslist with no hits, so I thought someone here could make a good recommendation.  I am in west TN Clarksville to be exact and would like to get at least 380.00 for the machine.

Thanks for any input I can get!!! 





**"Padilla's custom leather"**

---
---
**Gavin Dow** *November 29, 2017 14:39*

That's going to be a tough sell no matter where you post it. On a good day, a K40 sells for $380 or less on eBay, and that's from a seller that offers a suggestion of culpability and customer support.

You could give eBay a try, but you'll be paddling upstream unless you lower your expectations on price.

Best of luck to you.


---
**Padilla's custom leather** *November 30, 2017 20:42*

sold at my price


---
**Gavin Dow** *December 01, 2017 12:27*

Nice, congrats!


---
*Imported from [Google+](https://plus.google.com/106971026516976154588/posts/2D2otcGZkoS) &mdash; content and formatting may not be reliable*
