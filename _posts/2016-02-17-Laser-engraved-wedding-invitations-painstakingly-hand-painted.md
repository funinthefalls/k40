---
layout: post
title: "Laser engraved wedding invitations - painstakingly hand painted"
date: February 17, 2016 14:39
category: "Object produced with laser"
author: "Linda Barbakos"
---
Laser engraved wedding invitations - painstakingly hand painted. Was a huge labour of love but our guests were impressed.

![images/96b570f1fd09efc7091451743235763c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96b570f1fd09efc7091451743235763c.jpeg)



**"Linda Barbakos"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 17, 2016 15:55*

Those are very nice & I can imagine the painting would have been painstaking indeed.


---
**Jim Hatch** *February 17, 2016 18:28*

Beautiful. Not that it will help you, but as I'm doing something similar for a play my daughter is in I'll throw it out there.



I'm doing plaques with the logo/splash picture of Joseph & the Amazing Technicolor Dreamcoat. I did the Engrave in a 12x12 piece of black acrylic. Then I did a cut of parts of the logo in heavy paper stock (like file folder thickness). I sprayed a low tack 3M adhesive on the back of the resulting stencil. I placed that on the engraved acrylic and the sprayed high tack adhesive into the etching. Then blew colored glitter onto the stencil and it stuck in the etch. Still had to touch everyone of them but I didn't have to be talented (I'm an engineer not an artist) to get all the colors in the etching. I did have to cut several stencils but that was easy by turning layers on & off in my AI file.


---
**Linda Barbakos** *February 17, 2016 19:08*

It's such a shame that you can't post pictures in your comments, they sound pretty!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 17, 2016 20:10*

**+Jim Hatch** That's similar to a method I used for airbrushing some leather products, although I just used masking tape to stick the stencil to the leather. You mention you used "low tack adhesive" to stick the stencil, so I am curious as to when you are finished does the stencil just pull off easily & leave minimal residue on the piece?


---
**Jim Hatch** *February 18, 2016 00:35*

**+Yuusuf Sallahuddin**​ - yes the stencils come right off. 3M sells 3 different spray can adhesives from low-tack to super strength. The low tack stuff keeps the stencil flat so you don't end up with other glue & glitter sneaking under the edges.



I did it on a lark as my original project was to do it in a reverse image cut into clear and then use edge lit LEDs and I wanted to test the settings. But the black turned out so good I went the colored glitter route.


---
*Imported from [Google+](https://plus.google.com/107262725887494713165/posts/eYW7KYViHE4) &mdash; content and formatting may not be reliable*
