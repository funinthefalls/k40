---
layout: post
title: "K40 Manual OK not actually a lot better then any thing coming out of china"
date: October 22, 2016 23:20
category: "Hardware and Laser settings"
author: "Nick Williams"
---
K40 Manual



OK not actually a lot better then any thing coming out of china. Not sure how many of you have heard of FullSpectrum Laser a company that produces lasers out of Las Vegas. Well they got there start by converting K40 lasers with a custom controller based on Beagle Bone. I would not necessarily recommend these guys but they did produce a pretty good manual for there original K40 Laser read through it about 80% applies to the K40.



[http://wiki.milwaukeemakerspace.org/_media/equipment/fslaser-40watt.pdf](http://wiki.milwaukeemakerspace.org/_media/equipment/fslaser-40watt.pdf)







**"Nick Williams"**

---
---
**Ariel Yahni (UniKpty)** *October 22, 2016 23:34*

Wow I did not know that


---
**Nick Williams** *October 23, 2016 00:07*

Yah, it is pretty amazing they have made an entire business out of retrofitting these Chinese lasers this is still true with the exception of there Hobby Laser which is actually there low end. All of there larger lasers are basically Chinese lasers with a custom controller. They have expanded into 3D printing but  I personally have had a bad experience with these guys. I have a Hobby Laser but have gutted it replacing the controller and the power supply. I have also replaced the tube but this can reasonably be attributed to normal use. There software was a really a pain to use. 


---
**Jim Hatch** *October 23, 2016 00:19*

They have a new one coming next month (the Muse) that looks like a knockoff of what the Glowforge is supposed to be.


---
**Don Kleinschnitz Jr.** *October 24, 2016 16:22*

**+Ariel Yahni** this is what you and I were musing about on a few posts, I.E building a better K40!


---
**Ariel Yahni (UniKpty)** *October 24, 2016 16:26*

**+Don Kleinschnitz**​ exactly. My understanding is the the K40 are not legally certified to comply with US law or something like that. If that's not the case then I do t know why others haven't gotten to do this as they did


---
**Nick Williams** *October 24, 2016 17:56*

**+Ariel Yahni** I have wounder this exact question. I believe the legal end run involves becoming a re seller just like all of these other companies that ship K40s from the states they are just reselling the K40 laser. Of course the fact that they are modifying it introduces a grey area?? 


---
**Ariel Yahni (UniKpty)** *October 24, 2016 17:59*

**+Nick Williams**​ not sure at all. I know there is a laser CLASS that requires some certification of some sort. 


---
*Imported from [Google+](https://plus.google.com/107520953345127905250/posts/UFEdBAB4NLP) &mdash; content and formatting may not be reliable*
