---
layout: post
title: "Rockler hardwood company sells a 30 pound box of assorted hardwood scraps for 25 dollars....I bought one and it arrived Saturday....great buy my friends"
date: October 30, 2016 14:50
category: "Discussion"
author: "Scott Thorne"
---
Rockler hardwood company sells a 30 pound box of assorted hardwood scraps for 25 dollars....I bought one and it arrived Saturday....great buy my friends

![images/946304535b36a513664d220740892b72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/946304535b36a513664d220740892b72.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 30, 2016 16:03*

Love the dragons & phoenixes. Looks great. What's the rough depth on engrave on something like this?


---
**Scott Thorne** *October 30, 2016 16:07*

3/16...maybe a little more.


---
**Timo Birnschein** *October 30, 2016 19:25*

This looks fantastic!

Do you use air assist? And if yes, is it an aquarium pump? And if yes, do you have an air tank in between the pump and the nozzle? If no, I believe one can see the air vibrating in your engraving. A small tank will solve that problem and you get rid of the ripple in the background :)


---
**Scott Thorne** *October 30, 2016 19:53*

The ripple is the grain in the alderwood.


---
**Coherent** *January 03, 2017 14:22*

Scott, are you still using the 50w GTSUN laser you bought last year to do these engravings? I'm looking to buy another and not sure if I want a 60w, 80w or even need one that big (red  700x500mm models). Are you still happy with yours? Would you get a larger one or more watts if you had to do it again?


---
**Scott Thorne** *January 04, 2017 10:32*

**+Coherent**....I'm still using the 50 watt chinese machine...i am doing the 3d carves with it...i did upgrade the tube to a 60 watt though.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/KHP5i9GgXWg) &mdash; content and formatting may not be reliable*
