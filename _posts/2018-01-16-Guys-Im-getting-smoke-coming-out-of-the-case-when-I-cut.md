---
layout: post
title: "Guys I'm getting smoke coming out of the case when I cut"
date: January 16, 2018 13:34
category: "Modification"
author: "Chris Hurley"
---
Guys I'm getting smoke coming out of the case when I cut. I would like to get it down to the point where I don't need to worry about fumes giving me cancer later in life. I've replaced the original fan with a inline 6inch duct fan but still get a smell. Would installing a bathroom fan above the machine be of any use? 





**"Chris Hurley"**

---
---
**Adrian Godwin** *January 16, 2018 14:06*

Smoke, or a smell ? If you can see actual smoke coming out, then consider why that is. Perhaps a large gap ? poor air flow ? 

The extraction fan should give a slightly lowered air pressure inside the case, causing air to be pulled in though all the other holes. If smoke comes out, something isn't right. Perhaps your air assist is pumping air in faster than the extractor sucks it out.



If it's just a smell, then maybe it comes out after the extractor has stopped.  If you're concerned about your health, consider the materials you're cutting. Real wood is probably OK, but MDF or masonite is full of glue. Acrylic also produces unpleasant fumes.



A cooker hood is probably a more effective defence than a bathroom fan.

 


---
**Chris Hurley** *January 16, 2018 14:12*

It's more of a smell in the my shop room. I'm thinking your correct on the air asst perhaps over powering my exhaust fan. Do you think it will be a good idea to remove the original vent that is in the cutting area? Or is a directed vent more efficient than a undirected larger vent?


---
**Don Kleinschnitz Jr.** *January 16, 2018 15:03*

My guess is you are not moving enough air in/out of box and or exit is leaking. 

Where does it vent to?


---
**Joe Alexander** *January 16, 2018 20:33*

if you haven't already some foam sealer tape around the edges of the openable compartments might help contain some of the odor, also toss your waste material into a sealed container as it tends to off gas also. Make sure though that you dont completely seal the air flow so that the extractor fan doesnt get choked down(I have a pass-through slot i cut in the front that is my vent so it pulls across the material to the back and out). tape seal all duct joints, and having the fan on the end rather than the beginning helps considerably. that will definitely handle any smoke issues, and should help with the smell a bit. and i yanked that original vent part day one and put a full 6 in duct flange on the back.


---
**Don Kleinschnitz Jr.** *January 16, 2018 22:42*

What CFM exhaust fan are you using and what size opening?


---
**Chris Hurley** *January 16, 2018 22:50*

I'm using a 6 inch fan which is pulling 220 cfm from a 4 inch line. 


---
**Don Kleinschnitz Jr.** *January 16, 2018 23:01*

**+Chris Hurley** I am running 440 CFM sucking on a 6 inch duct. I don't get any smell from the box but I do not run it hard.

The table area is like a hurricane....



I cannot imagine that if you have enough air flow across the table you should smell much of anything, your neighbors..... that another question :).



[amazon.com - VenTech IF6 6" Inline Duct Fan 440 CFM: Built In Household Ventilation Fans: Amazon.com: Industrial & Scientific](https://www.amazon.com/gp/product/B004YXDQZU/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)



[http://donsthings.blogspot.com/search/label/K40%20Air%20Systems](http://donsthings.blogspot.com/search/label/K40%20Air%20Systems)




---
**Jeff Lin Lord** *January 17, 2018 01:18*

Recently added a decent computer type fan at the front of my K40 cutting bed.  Mounted it on the cross rail above the stepper motor   It’s blowing from front to rear and actually it sits a bit to the right of center.  And I cut the smelliest cancer causing stuff possible (acrylic types).   And it has made a huge difference without adding anymore in line fans and I’m even using the stock exhaust.  I do have air assist also, but I learned from my experience with a full spectrum unit that the air assist wants to be no higher than 20psi, approx.  



All in all it seems that my little fan has helped pull in more fresh air from the open key hole in the lid.  I had already sealed up the cutting area prior to this fan addition.  


---
**Chris Hurley** *January 21, 2018 03:47*

So I went and took your guys advice and got a range hood for the K40. It works really well. The smell from the mdf are way lower. I just have to clean out the workshop to cut down on the old dust/materials. 

![images/d7475b10a00978abefde8392e6a56e3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d7475b10a00978abefde8392e6a56e3a.jpeg)


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/VfmxQwEB3D7) &mdash; content and formatting may not be reliable*
