---
layout: post
title: "So I'm converting my K40 over to the Smoothie board [ yes, still, it's slow progress ] and am wondering if anyone has any info on the stepper motor 17HW3448N-15AD"
date: July 05, 2016 09:41
category: "Smoothieboard Modification"
author: "Darren Steele"
---
So I'm converting my K40 over to the Smoothie board [ yes, still, it's slow progress ] and am wondering if anyone has any info on the stepper motor 17HW3448N-15AD.  Could do with a pin out so I know which cable goes where.  Has anybody done any work in this area?





**"Darren Steele"**

---
---
**Jean-Baptiste Passant** *July 05, 2016 09:51*

Wlel, I changed my motors because the stock ones are unknown on the web.



Wiring is easy, just follow this tutorial (Trial & error is risk free so go for it, probably the easiest way) : [http://reprap.org/wiki/Stepper_wiring#Methods_and_procedures](http://reprap.org/wiki/Stepper_wiring#Methods_and_procedures)


---
**Gary McKinnon** *July 05, 2016 10:46*

Can't find anything on that motor apart from other people looking for a datasheet :(


---
**Ariel Yahni (UniKpty)** *July 05, 2016 11:19*

Don't think you need it. Just take the motor and conect it. Be sure not to over current it. Start low and adjust 


---
**HalfNormal** *July 05, 2016 15:38*

**+Darren Steele** **+Gary McKinnon** someone at the cnczone forum found some info; [http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/300948-finally-found-info-stepper-motors-cheap-k40.html](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/300948-finally-found-info-stepper-motors-cheap-k40.html)


---
**Stephane Buisson** *July 05, 2016 15:40*

if it work with your PSU ( & stock Board), it will work with Smoothie too. just find out the pair(S) by trying to turn the shaft manually when motor disconnected, 2 cable shorted, if motor blocked, you have a pair (so the other 2 should do the same). So now you have a+a-b+b-, a or b doesn't matter as you can change rotation in the config file.


---
**Mircea Russu** *July 05, 2016 20:30*

**+Jean-Baptiste Passant**​ what steppers did you chose? Have a link?


---
**Ariel Yahni (UniKpty)** *July 05, 2016 20:31*

I would not know why would you need to change the motors? 


---
**Mircea Russu** *July 05, 2016 20:32*

For 12v :)


---
**Jean-Baptiste Passant** *July 05, 2016 20:40*

**+Mircea Russu** Not really, it's more because I want to know what I work with than for specifications. It's like a car, you can drive it without even knowing what kind of motor you have, but when doing maintenance it's better to know it.

The motor I used are this ones : [http://goo.gl/bviB0s](http://goo.gl/bviB0s)

Datasheet is available here : [http://goo.gl/LZt9d9](http://goo.gl/LZt9d9)

**+Ariel Yahni** See my answer to Mircea


---
**Mircea Russu** *July 06, 2016 04:17*

Thank you **+Jean-Baptiste Passant** how about the y stepper? These seem to have only one axle. Did you go double stepper for y?


---
**Jean-Baptiste Passant** *July 06, 2016 11:00*

**+Mircea Russu** Sorry, forgot about that, I used 17HS8401 for this.

Exactly the same as 17HS4401 but with two shaft.


---
**Tony Sobczak** *July 06, 2016 21:20*

Follow


---
**Darren Steele** *July 07, 2016 11:56*

Thanks for everybody's help so far.  Quick update.  I got the y-controller hooked up to the smoothie board using the same pinout.  I can now connect to the web interface and manually move the head.  The x-stepper was connected with a ribbon so I'm in the process of taking the ribbon connector out and splicing some controller wires from the smoothie board.  So tempted to just solder/shrink wrap the wires for testing and then dropping a connector block in...but that would be hacking.  So I'm going to get a connector block sorted out and do the job properly!


---
**Darren Steele** *July 07, 2016 15:32*

Didn't get a connector block...I hacked it :) I'm now trying to convince LaserWeb3 to communicate vie ethernet.


---
**Ariel Yahni (UniKpty)** *July 07, 2016 16:01*

**+Darren Steele**​ you should be able to jog with no issues via ethernet on LW


---
**Mircea Russu** *July 29, 2016 19:02*

**+Jean-Baptiste Passant** I've been searching the web for 17HS8401 but it's single shafted just like the 17HS4401 with larger torquw. Could you please take a picture of your dual shafted Y stepper model? Thank you!




---
*Imported from [Google+](https://plus.google.com/+DarrenSteele/posts/FraX9T9LKKW) &mdash; content and formatting may not be reliable*
