---
layout: post
title: "Well, I've lost power again. This time everything works except the laser won't fire"
date: September 25, 2017 14:03
category: "Discussion"
author: "bbowley2"
---
Well, I've lost power again.  This time everything works except the laser won't fire. 

I'm at a loss as far as trouble shooting.  The machine turns on and the laser head will follow svg files but just won't fire.  Last time my power supply was bad.  I was able to trace incoming power to the power supply.  The fuse on the board was blown.  Replaced the PSB but this time everything works except the laser won't fire.







**"bbowley2"**

---
---
**Don Kleinschnitz Jr.** *September 25, 2017 14:23*

Sorry we need a lot more info than this as many things could cause the laser not to fire.



With the power on ....



1. Is the LED down on the LPS on or off

2. Does it fire if you push the "test" button down on the LPS?


---
**bbowley2** *September 25, 2017 14:47*

The red led is on.



It doesn't fire when I push the test button on the LPS.


---
**bbowley2** *September 25, 2017 14:56*

oops, the green led is on.


---
**Nate Caine** *September 25, 2017 14:57*

Impossible to remember, but does your machine have a dial to set the laser power?  If so, is it turned up?  Any wiring problems, or bad potentiometer?  



The "TEST" (or "TEXT") button on the power supply will turn the laser on, but only if the "IN" (Intensity) input is active.  Sometimes bad wiring, or a bad pot, can fool you.


---
**Don Kleinschnitz Jr.** *September 25, 2017 15:11*

Post picture showing the LPS input connectors.



Insure as **+Nate Caine** suggests that the pot is all the way up and try "test" again.



If that does not fire you measure "IN" relative to grd with a DVM and it should be close to 5VDC.


---
**bbowley2** *September 25, 2017 18:36*

No pot.  Digital buttons for increase or decrease.


---
**bbowley2** *September 25, 2017 18:59*

Schematic for tracing voltages?




---
**bbowley2** *September 25, 2017 19:00*

![images/f2a441c9914d845ec8b8a943db75475c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f2a441c9914d845ec8b8a943db75475c.jpeg)


---
**Don Kleinschnitz Jr.** *September 26, 2017 12:52*

Measure "IN " with respect to gnd, with the digital controls set as high as you can. The post values.



[photos.google.com - Google Photos](https://photos.google.com/share/AF1QipPz-n7M8PMjPMwQM7Zwg105THVIbipHXA-8PLT01n1NdXHoyJR5tNjtp_4cWBCv5Q/photo/AF1QipNGC6ul9cK6a_QnOHDxHVgsSkyviPwt1_qZW-VA?key=ZHBXTFNTSjN6ZWNwSU13cXRTUFlkM0pxOTl2Smln)


---
**bbowley2** *September 26, 2017 13:46*

IN to gnd is 4.8vdc


---
**Don Kleinschnitz Jr.** *September 26, 2017 14:18*

**+bbowley2** just checking to be sure... when "IN" is 4.8V did you try to fire the laser with the test button?

If I recall not that long ago you popped a supply and sent me your old one, right.



<b>Symptoms</b>

...Your green led is on so you are getting power to the supply and low voltage is being output.

...The "IN" is 4.8VDC and when you push the red "test" button on the LPS the laser does not fire...

...You have checked all wiring to the tube especially the cathode wire? 



<b>Other checks</b>

Do you have a current meter installed? If not I think the cathode should be tied to the -L of the LPS and ground. You can check. 



.......................

It sounds like a dead HVT or it could still be your tube. To do final check we would have to do an arc test on the LPS. 

Once you have tried and checked everything else I can walk you through this test but its a last resort test because its dangerous ....

....................

Have you been running the machine a lot, the supply looks pretty clean inside? How many hours of use?

Seems like an excessive failure rate. 

What kind of coolant are you using in your system?




---
**bbowley2** *September 26, 2017 17:14*

I haven't run the machine very much.  I mostly use the K40 to cut 1/8" acrylic for engraving on my Piranha FX rotary.   

I'm using distilled water running through an aquarium chiller with a 1 gallon tank that houses the water pump.  I use a small amount of aquarium anti algae power. 


---
**Don Kleinschnitz Jr.** *September 26, 2017 17:56*

**+bbowley2** have you checked the cathode wiring as suggested above?


---
**bbowley2** *September 26, 2017 20:50*

All wiring checks out.  Absolutely no discharge using a chicken stick.  It can only be the HV fly back.  This seems to be a field repairable problem.  There is a 3 pin plug on the back of the HVT.  I will look for the part online.  Thanks again.




---
**Gary Balu** *September 27, 2017 22:50*

Same problem. Wold this be the red machine?  Still trying to get something worked out with the seller.  They insist it is the water protection circuit. 


---
**waco kid** *September 28, 2017 00:14*

same problem with a brand new blue box machine.  pot checked out on a meter, psu comes on, same diagram as above but i have a red led instead of green.  tube is wired correctly, will not fire in test.  


---
**Gary Balu** *September 28, 2017 01:25*

Video I sent the seller this morning. Now they are saying once I return this one they will send another one....






{% include youtubePlayer.html id="eCGxFZrA6K4" %}
[youtube.com - VID 20170926 223624](https://youtu.be/eCGxFZrA6K4)


---
**Don Kleinschnitz Jr.** *September 28, 2017 11:31*

 **+Gary Balu** **+waco kid** Let us know how it goes with the seller.

Does the laser light up at all when you pressed the test switch on the LPS?

In the mean time if you can use a DVM do the same measurements as **+bbowley2** above on IN with the digital power controls or the pot at its highest setting. Should be close to 5V when measured to gnd.



**+Gary Balu** you can verify the WP sensor is working just by shorting it out and trying the test button, but the test button overrides that circuit.


---
**waco kid** *September 28, 2017 13:29*

my unit was bought back in april and has been in storage since so kinda screwed with contacting the seller at this point.  my own fault, should have known better and tested it when i got it.  i have another blue box k40 that i've had no problems with so testing wasn't a high priority.   zero power to the tube, does not light up at all.  red light on the psu shows power going in, nothing coming out.  most likely going to buy a new psu and trouble shoot this one in the background and if i can bring it back, then i'll just have a spare on hand 




---
**Gary Balu** *September 29, 2017 16:17*

**+Don Kleinschnitz** The laser sits there. Doesn't flash or go bright. eBay found in my favor, but I'm still losing. All the time and money I spent on this one performing the tests and buying parts won't get me anything more than A refund. Nobody will assure me it is even a total refund. So beware who you deal with. 




---
**Gary Balu** *September 29, 2017 16:19*

**+waco kid** The blues seem to be quite a bit more stable than the red units judging from searching the internet. Next one I get will be blue. 


---
**bbowley2** *October 03, 2017 01:46*

Well, I replaced the LPS and no change.  No laser.  I'm going to go over everything I tried with the old LPS to see if I can puzzle this out.  Major disappointment.


---
**Don Kleinschnitz Jr.** *October 03, 2017 14:48*

**+bbowley2** AH SHIT! I will look back over what we did and see if there are some hints. 


---
**bbowley2** *October 03, 2017 15:08*

Thanks Don.  Seems like I never do anything that has a simple solution.  Does the tube glowing when I fire it mean the tube is OK?




---
**Don Kleinschnitz Jr.** *October 03, 2017 18:02*

**+bbowley2** what kind of glowing, picture?


---
**bbowley2** *October 03, 2017 18:21*

It is a white or blueish white.

I got it working.  I pulled the LPS out.  Reinstalled it making sure all terminals were tight and it works.

I have a flyback on order so I will have a spare for the next time.

I saw a mention of replacing the 40W tube with a 50W tube.  My new LPS is a 40-50W unit but the stock tube is a little over 700 mm and I can't find any 50W tubes that will fil.  Does this mean I will have to cut a hole in the case to accommodate the longer tube and is that even possible?

Thanks again for your help.  Maybe someday I will be answering these nubby questions.


---
**Don Kleinschnitz Jr.** *October 03, 2017 19:02*

**+bbowley2** all is ok by just tightening? We missed something because turning pot all the way up and pusing test bypasses all other enables? sigh!

I have never found a 50 watt tube that will fit a K40. Cutting hole and adding extension is doable. LO sells an extension and have seen some designs on thingyverse. I plan to cut one from acrylic. 


---
**bbowley2** *October 06, 2017 16:03*

Something I've noticed with the new LPS is test firing doesn't have the same power as it use to.  The cutting power is the same but not the test firing.


---
**Don Kleinschnitz Jr.** *October 06, 2017 19:49*

**+bbowley2** is the power setting (pot) set at the same value for both?


---
**bbowley2** *October 07, 2017 03:35*

It is


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/BXugbGQArb3) &mdash; content and formatting may not be reliable*
