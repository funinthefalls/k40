---
layout: post
title: "So what are the alternative's for a smoothie setup?"
date: December 27, 2016 04:51
category: "Discussion"
author: "Bill Keeter"
---
So what are the alternative's for a smoothie setup? I was planning to use **+Scott Marshall** ACR solution, but I haven't heard from him in quite some time. What other options are there for an easy Smoothie setup?





**"Bill Keeter"**

---
---
**Alex Krause** *December 27, 2016 05:13*

The genuine smoothie is really easy to setup **+Don Kleinschnitz**​ did alot of research and up with a very simple way to connect it


---
**Don Kleinschnitz Jr.** *December 27, 2016 10:23*

Here is the build log:

[donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



The smoothie conversion is pretty straight forward  using this research and implementation depending on what you want in the converted machine. 



You can pick from this build what works the best for your implementation.



Clearly the easiest integration is using the Cohesion 3D.


---
**Bill Keeter** *December 27, 2016 16:25*

**+Don Kleinschnitz** so to use a genuine smoothie I just need that middle board PCB listed in your blog? I don't have that ribbon cable version k40. So I just need to manually wire everything to it? Plus my end stops are mechanical. Not optical. Also confused why the X-axis is wired through the middle board instead of directly into the smoothie.



**+Peter van der Walt** so the Cohesion3D is just a custom Smoothie PCB with everything setup already? Wonder when **+Ray Kholodovsky** will have more in stock. Very cool. Do wish it was setup to support the GLCD shields made for the Smoothie.


---
**Ray Kholodovsky (Cohesion3D)** *December 27, 2016 16:33*

**+Bill Keeter** the Cohesion3D Mini contains the entire "brains" of smoothie with 4 driver sockets, various other peripherals to drive 3D Printers and such, and then all the connectors we know about for a k40. It's the same size and mounting hole pattern as the stock M2Nano Green PCB so you should be able to swap it out with just 4 screws. 

The production run is almost complete, and new orders should be shipping in just a few weeks. 

The Mini does have a little adapter board option to connect the RepRapDiscount GLCD. It's a lot more compact than the Smoothieboard one. ![images/620a1e573df81e71d6c4aa2180512799.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/620a1e573df81e71d6c4aa2180512799.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 27, 2016 16:35*

I do want to add one point. Cohesion3D Boards contain everything you need. There's no separate PCBs, no level shifters needed, no drilling new holes in your machine (usually not anyways) and there should be minimal rewiring required. It's an all in one solution meant to drop right in. 


---
**Don Kleinschnitz Jr.** *December 27, 2016 19:02*

**+Bill Keeter** if your endstops are wired not ribbon cable you do not need the middleman. You also don't need anything but the cohesion 3d if you go that way.


---
**Bill Keeter** *December 27, 2016 19:40*

**+Don Kleinschnitz** Wait? I don't need the middleman? Could I just wire up a smoothie directly to my K40? I thought there was some power/wiring that needed an adapter to make the smoothie work.



I'm seriously thinking about going with the Cohesion3D board regardless. 



**+Ray Kholodovsky** does the Cohesion3D have enough power on the 5V to support a GLCD without modification. From your comments above i'm assuming it does.


---
**Ray Kholodovsky (Cohesion3D)** *December 27, 2016 19:41*

**+Bill Keeter**  Yep.  There's something like 5v 850mA available from the internal regulator which is plenty enough for the onboard "brains" and a GLCD.  Check the Cohesion3D group here in G+, plenty of people running my Mini with a GLCD.


---
**Don Kleinschnitz Jr.** *December 27, 2016 23:25*

**+Bill Keeter** the middleman just converts the ribbon cable to separate x stepper and endstops connectors that plug into smoothie. The cohesion does that conversion on board.


---
**Wolfmanjm** *December 28, 2016 01:24*

Yea you can wire up a genuine smoothie board direct to the k40 if you don't have the ribbon cable version. stuff just plugs in, no middleman board required.


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/cAM88CVA7NR) &mdash; content and formatting may not be reliable*
