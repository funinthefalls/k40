---
layout: post
title: "Hey Guys, do you know where I can buy a Smoothieboard or similar in Germany?"
date: November 09, 2016 12:14
category: "Smoothieboard Modification"
author: "Nico Neumann"
---
Hey Guys, do you know where I can buy a Smoothieboard or similar in Germany?





**"Nico Neumann"**

---
---
**Ariel Yahni (UniKpty)** *November 09, 2016 12:24*

Official EU seller here [https://plus.google.com/+ArielYahni/posts/4HDF9ZA8rfX](https://plus.google.com/+ArielYahni/posts/4HDF9ZA8rfX)


---
**Nico Neumann** *November 09, 2016 12:34*

I found a german Reseller: [http://www.filafarm.de/products/smoothieboard](http://www.filafarm.de/products/smoothieboard) Anyone ordered here?

[filafarm.de - Smoothieboard](http://www.filafarm.de/products/smoothieboard)


---
**Nico Neumann** *November 09, 2016 13:25*

Thank you **+Peter van der Walt** and **+Ariel Yahni** 


---
**Marcus B.** *November 09, 2016 18:01*

Ich habe mein x4 auch von [Filafarm.de](http://Filafarm.de)


---
**Nico Neumann** *November 09, 2016 18:25*

**+Marcus B.**​ das ist gut zu hören. Hast du es in einem k40 eingebaut?


---
**Marcus B.** *November 09, 2016 18:46*

So ist zumindest der Plan, aktuell liegt das Board noch daneben. Komme momentan leider nicht dazu. Zeitgleich soll ein neuer Kopf (Air Assist) inclusive neuer Linse rein.


---
*Imported from [Google+](https://plus.google.com/106888540859762729476/posts/gXBWbPjjxqp) &mdash; content and formatting may not be reliable*
