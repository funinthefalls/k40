---
layout: post
title: "So I'm in the process of gathering info to make a proposal to get one of these for my Technology classroom"
date: June 13, 2016 16:22
category: "Discussion"
author: "Nigel Conroy"
---
So I'm in the process of gathering info to make a proposal to get one of these for my Technology classroom.



One thing around the water cooling I've been thinking about is keeping the water cold. 



Has anyone looked at or thought about using an aquarium chiller to keep the reservoir of water a bit cooler?



Like one of these.





**"Nigel Conroy"**

---
---
**Alex Krause** *June 13, 2016 17:15*

Is this a peltier?


---
**Lance Robaldo** *June 13, 2016 17:16*

The peltier cooler won't be able to keep up with the laser.  You'd be better off with something like this: [http://www.drsfostersmith.com/product/prod_display.cfm?c=3578+3743+28150&pcatid=28150](http://www.drsfostersmith.com/product/prod_display.cfm?c=3578+3743+28150&pcatid=28150)


---
**Brandon Smith** *June 13, 2016 17:17*

Overall I think the use of a five gallon bucket is the best low cost solution. However, even though I am doing that right now, I will be adding a dual 120mm fan radiator based setup just before the inlet of the laser. when my new larger machine arrives this week. The setup you posted seems awfully pricey for what it is, especially because it is so tiny. There are better new setups on ebay, and some really nice used setups. Search for heat exchangers and you will find better solutions. I wouldn't worry about trying to keep the reservoir cool, but focus on cooling the water just before it goes into the laser.


---
**Lance Robaldo** *June 13, 2016 17:18*

Also, the larger the reservoir you use, the more stable the water temperature will be.  I use a 20 gallon rubbermaid tub for my reservoir and it holds temperature very nicely.


---
**Alex Krause** *June 13, 2016 17:18*

You could try using a mini fridge and keep a reservior inside with some strategically placed inlet/outlet tubing


---
**Nigel Conroy** *June 13, 2016 18:11*

Being in school I was concerned with having to get ice to add to the bucket. Is adding ice necessary? 



That is perfectly logical not to cool the reservoir but the water before entry. Thanks for input. 



How much as an estimate for a system with upgrades, such as air assist and smoothie?


---
**Thor Johnson** *June 13, 2016 18:56*

I haven't needed to use ice (like **+Peter van der Walt**  said, condensation is a no-no), but I've only run 3 hours at a time.  I plan to use a radiator and fan for a closed-loop eventually (for some reason, nasty things like to grow in buckets around here); if it's indoors, I think a 5-20 gallon bucket will be able to dissipate the heat into the room AC well enough @ 40W... a bigger laser needs a chiller (figure power of laser * 10 for watts of cooling needed * 4 if you need BTU's -- a rough estimate for 40W is 400W / 1366 BTU or 1/10th ton) or a cooling tower (but that's messy).



I'd actually cool it before it went into the bucket -- just to make sure the temperature doesn't change rapidly -- be very worried about condensation (if it gets on the end of your tube, it dies due to unclean optics, and if it gets on the outside of your tube, it can start shorting out the high-voltage -- perhaps to the laser's enclosure -- run the "redundant" ground to the case!).


---
**Nigel Conroy** *June 13, 2016 19:17*

I'll be running it in a classroom with AC so a bucket by the sounds of it should dissipate enough heat.Thanks.



I've just been given the go ahead for the project. I'll start a separate thread for advise on my proposed shopping list...


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/Ph8nfxWLB4v) &mdash; content and formatting may not be reliable*
