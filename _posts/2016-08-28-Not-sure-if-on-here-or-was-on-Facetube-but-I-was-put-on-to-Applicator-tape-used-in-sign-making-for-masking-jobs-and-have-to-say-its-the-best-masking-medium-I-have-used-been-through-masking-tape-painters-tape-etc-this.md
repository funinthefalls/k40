---
layout: post
title: "Not sure if on here or was on Facetube, but I was put on to Applicator tape (used in sign making) for masking jobs and have to say it's the best masking medium I have used (been through masking tape, painters tape etc) this"
date: August 28, 2016 23:41
category: "Hardware and Laser settings"
author: "Rodney Huckstadt"
---
Not sure if on here or was on Facetube, but I was put on to Applicator tape (used in sign making) for masking jobs and have to say it's the best masking medium I have used (been through masking tape, painters tape etc)  this stuff cuts clean and is removed very easily :) works great on wood and acrylic, yet to try it on glass.





**"Rodney Huckstadt"**

---
---
**Rob Morgan** *August 29, 2016 02:25*

What type/brand aplication tape?


---
**Rodney Huckstadt** *August 29, 2016 03:55*

no brand, the stuff I got was from a sign maker, just called Application tape, medium bond.  i'll have a look on the inside of the tube tonight


---
**Terry Taylor** *August 29, 2016 16:38*

Also called transfer tape. It is very thin, low tack masking tape. Do NOT use the plastic kind. Get the paper type transfer tape. Lots of sellers on eBay. I use in my vinyl sign business.


---
*Imported from [Google+](https://plus.google.com/109243783228349869845/posts/5KKsugnYTJ9) &mdash; content and formatting may not be reliable*
