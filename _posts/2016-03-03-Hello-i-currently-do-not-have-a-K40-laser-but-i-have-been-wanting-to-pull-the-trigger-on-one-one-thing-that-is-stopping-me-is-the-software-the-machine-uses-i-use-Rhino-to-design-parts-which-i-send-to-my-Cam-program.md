---
layout: post
title: "Hello i currently do not have a K40 laser but i have been wanting to pull the trigger on one, one thing that is stopping me is the software the machine uses, i use Rhino to design parts which i send to my Cam program"
date: March 03, 2016 22:07
category: "Discussion"
author: "Dennis Fuente"
---
Hello i currently do not have a K40 laser but i have been wanting to pull the trigger on one, one thing that is stopping me is the software the machine uses, i use Rhino to design parts which i send to my Cam program converting it to Gcode to run my CNC mill and lathe, i have read somewhere that the K40 machines uses Hpgl printer language, well my question is i have an older CNC mill that uses a program called modela this program accepts DXF and STL files covert's them to HPGL and send's it to the milling machines board to cut the part however it is 2d it has a z depth the files pass through either a parallel port or a series port i am wondering if this program could be modified to run the K 40 laser or if there already exisit a program to do this. thanks great forum lots of info.

Oh one other thing what is the life of the CO2 tube

Dennis 





**"Dennis Fuente"**

---
---
**Thor Johnson** *March 03, 2016 22:49*

So, the software that comes with it is usb-dongle-locked to the cutter, so you can't use other software unless you replace the controller (DSP, RAMPS, or Smoothie).



RAMPS and Smoothie accept G-Code, so you can use Inkscape or other things to make G-Code out of cad files (DXF is good).



The stock programs are LaserDraw, which accepts Bitmap Only (it will convert SVGs to Bitmaps), and CorelLaser [which requires CorelDraw], which can accept both vectors (DXF/SVG) and bitmaps.



The stock programs seem easier to do custom setups (use the computer to move the head to the top left and press Burn), but they don't do "only certain layers" very well, nor do they have control over the laser power (which the upgrades above do, so you can burn Red=100%, Blue=20%).



I'm using stock right now, and preparing to go to RAMPS in a bit.  Not sure about laser tube life, but it seems that in all of these low-cost things it's completely random if the tube, power supply, or controller fails first.


---
**Dennis Fuente** *March 03, 2016 22:51*

Ok thanks for that one I help a freind once where we unlocked a doggle based software hacked it i wonder if i can do the same here


---
**Thor Johnson** *March 03, 2016 22:58*

I think it would be complicated; the software uses the dongle and the serial number from the PCB (so you have to find it and enter it) or it won't communicate with the laser at all.  It will run, but it won't produce g-code (it can make g-code, but I haven't seen a way to /load/ the g-code).



Now if you make a way fro me to "just send g-code to the laser" (and document what g-codes are valid for what), now that would be a celebrated tool!  The laser itself shows up as serial port.  Monitoring the serial port on mine resulted in gibberish.


---
**Dennis Fuente** *March 03, 2016 23:50*

Hello So which software will work with the machine i mean can i design in a software and then import it into the cutting software or do they have to match in some way since i don't have a machine yet i trying to understand how this works before dropping the hammer one

Thansk 


---
**Thor Johnson** *March 04, 2016 00:26*

Stock:

You save your file as a BMP, then you can import it into LaserDraw which will translate it and send it to the laser -- but only does bitmaps, regardless of what it says.

<s>or</s>

You load the file into CorelDraw (a pirated version is usually on the CD), then use the CorelLaser Plugin to translate and send it to the laser -- this works with both vectors (DXF at least) and bitmaps.



<s>-or Not Stock-</s>

You rip out the controller and replace it so you can use actual g-code (Smoothie/RAMPs)  or a printer driver (DSPBoard).


---
**Dennis Fuente** *March 04, 2016 00:29*

would you know if the board can be flashed with a program like repetier .92 i use this on my 3d printer and it works great.

Thanks a gain for the info


---
**Thor Johnson** *March 04, 2016 00:31*

No, it's a custom chip (I think it's a Cypress SOC, but they sanded most of the identifying marks off).


---
**Dennis Fuente** *March 04, 2016 00:35*

OH well so much for that one, so will this machine work as it comes i mean work cut and engrave or ?


---
**Thor Johnson** *March 04, 2016 00:39*

It will.  You'll have to align it, and you will pull your hair out getting LaserDraw to understand you (eg, if you set for multiple passes, then change the drawing, then burn, the first pass will be done with <b>the old pattern</b> and subsequent passes will have the new pattern), but it will work.  I highly recommend an air assist nozzle (printed, a bent copper tube or something purchased) to keep your lens and mirrors clean.



And make sure the lens is facing the correct way... it seems that 80% are shipped upside down (still works, but it becomes a 0.7mm line instead of tack-sharp).


---
**Thor Johnson** *March 04, 2016 00:40*

And that's if it comes to you in one piece (which seems to be a problem as well, but I've seen it arrive in good condition here for many people -- just not me).


---
**Dennis Fuente** *March 04, 2016 03:26*

my thanks again for the information i will take all this into consideration would you know the life expectancy of the co2 laser tube how long do they last before replacement

thanks again for your help in answering my questions


---
**Vince Lee** *March 04, 2016 05:57*

While I have a smoothieboard I plan to hook up as a switchable controller at some point, I'm still running MoshiDraw with the Moshiboard that came with my cutter.  It supports importing .DXF, .PLT (HGPL), .AI and a few other structured formats, as well as .BMP and .JPEG for bitmap images.  I've used it to import some drawings (like a laser-cut x-wing from thingiverse) from inkscape and it's done fine converting the drawings I gave it once I determined what options to select when saving out the .DXF.  



Most of the time I still do simple jobs from within MoshiDraw, but for others the import facility seems to work well enough that I have a viable pipeline from external apps.  I don't know about LaserDraw, as folks here seem to like it better, but it sounds like a Moshi-based cutter <b>might</b> offer more import options.﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 04, 2016 05:59*

Hi Dennis. I recall seeing the tube expectancy is 500 hours, when looking for replacement CO2 tubes for the K40. I haven't needed to purchase one yet myself, just thought I'd get an idea of how much I need to budget when I do need it. Although, I am not 100% certain if that 500 hours estimate is precise or not. Could be more, could be less. Maybe someone else here who has had their machine longer than me (& probably uses it more too) would have a better idea on the life expectancy. Just thought I'd let you know what I recall seeing.



On the topic of software, the way I use my K40 (stock controller/software) is to create my files in Adobe Illustrator first (as I am much more proficient in that than Corel). I then have to save as Adobe Illustrator 9 version, as anything higher will not import (crashes the CorelDraw). So then I import into CorelDraw, with the CorelLaser plugin. Select what I want to do, add to task queue, then once I've added everything I want to do I hit start. E.g. Add Engrave to Queue, Add Cut layer 1 to file, Add Cut layer 2 to file, then Start (I usually do this way as I can then manually adjust power between different engraves/cuts since the stock controller/software doesn't allow software controlled power, as Thor mentioned).



Take a look around at the LaserWeb stuff that is being worked on by some of the members here (many posts in this group regarding it). It requires an upgraded controller board, but definitely seems to add some more functionality that is lacking in the stock setup.


---
**Vince Lee** *March 04, 2016 06:05*

You can download MoshiDraw 2015x, from the moshisoft website. The import options were added recently.  It needs the dongle to cut or engrave, but the drawing options now work without it.  The program is buggy and confusing to start, but you can test out if the import options do what you need.  Once you're inside the app, you can also upgrade to the latest version (MoshiDraw 2016x).



[http://www.moshidraw.com/English/Support/Update/](http://www.moshidraw.com/English/Support/Update/)




---
**Dennis Fuente** *March 04, 2016 18:22*

Thanks vince for the info i first need to buy a machine i went through all this with my 3d printer i just can't understand the benifit from locking the machine up this way on the manufacture's side .


---
**Vince Lee** *March 04, 2016 18:56*

This, I think I understand.  The companies selling the cutters are basically just bolting together parts and don't want to get into the software or board-level hardware business.  Moshidraw and Laserdraw provide them with cheap, plug-and-play solutions.  While there are good open source solutions out there, they still cost more money.  If one of the companies making smoothieboards were to create a single board that was plug compatible with, say, a moshiboard, then I think maybe it would gain some traction but my guess is it would be difficult to get the cost down without approaching the volume that the moshiboard probably enjoys.


---
**Dennis Fuente** *March 05, 2016 00:58*

So I ordered my unit with the laserdraw board hope it wasn't a mistake


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/SW7Zw2r9gyg) &mdash; content and formatting may not be reliable*
