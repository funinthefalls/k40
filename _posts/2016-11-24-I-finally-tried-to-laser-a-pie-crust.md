---
layout: post
title: "I finally tried to laser a pie crust"
date: November 24, 2016 15:21
category: "Object produced with laser"
author: "Ric Miller"
---
I finally tried to laser a pie crust. It works well. 

![images/cf7c2733b5b2f66c165ad2c672702661.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cf7c2733b5b2f66c165ad2c672702661.jpeg)



**"Ric Miller"**

---
---
**greg greene** *November 24, 2016 15:28*

I'll have a piece with cheese and ice cream please !


---
**Ric Miller** *November 24, 2016 15:29*

**+greg greene** what kind of cheese do you put on apple pie? :) 


---
**Jim Hatch** *November 24, 2016 15:32*

Cheddar of course 🙂


---
**Ric Miller** *November 24, 2016 15:40*

Weird. 


---
**Dave Finnerty (TheMadAdmin)** *November 24, 2016 15:42*

Very cool or maybe hot


---
**Ric Miller** *November 24, 2016 15:47*

I was inspired by an Instructable but I used pre made rolled up pie crust. [instructables.com - Laser Cut Peach Pie](https://www.google.com/amp/www.instructables.com/id/Laser-Cut-Peach-Pie/%3Famp_page%3Dtrue?client=safari)


---
**greg greene** *November 24, 2016 16:43*

Cheddar on Pie - delicious - try it !


---
**Don Kleinschnitz Jr.** *November 24, 2016 18:38*

Cheddar!


---
**Kelly S** *November 24, 2016 19:48*

Haha this is really neat, wish I had seen it sooner.  :D


---
**Dave Durant** *November 25, 2016 19:04*

The turkey in the middle looks lasered in-place instead of cut out then added like the leaves. Did you put the full sheet in your machine to do the turkey, add the leaves, put on top of the pie then bake? I'm guessing it was that or put the finished pie into the machine to do the turkey, which I'd sorta like to see video on.


---
**Ric Miller** *November 26, 2016 07:12*

**+Dave Durant** yes. That's what I did. I also etched the inner leaves on one layer and the cuts on another. 


---
**David Cook** *November 28, 2016 16:18*

Ha  clever idea.  came out great.  


---
*Imported from [Google+](https://plus.google.com/102361688882180483990/posts/KTEmmLQeqXR) &mdash; content and formatting may not be reliable*
