---
layout: post
title: "I want to create a repository/ gallery of laser cutting and engraving jobs that you folks have run so that new users have a starting point to work off of and can compare results"
date: February 13, 2017 19:48
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I want to create a repository/ gallery of laser cutting and engraving jobs that you folks have run so that new users have a starting point to work off of and can compare results. Details in the linked original post.



<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



I'd like to start a repository of benchmark laser jobs that new users can run. 



So, if you're up and running, here's the idea.



You have a job that you ran.  I'll use rastering a picture as an example.   

You have a picture of a boat that you engraved using LaserWeb with some DPI, beam diameter, and power settings.  You share those values/ settings and show a picture of the outcome.  You share the raw image, settings, and the gCode file. 



We host a repository of such examples with the files accessible. 



A newcomer is having difficulty dialing in their machine.  They navigate to this repository.  First, they run the image in LaserWeb with their existing settings, whatever those might be.  Then they adjust settings to match yours and run the job again.  Finally, they can run the gCode file you provided.  Now they have a before, in the middle, and after set of engravings they can use to compare quality and performance.  



Please contribute by sending a zip file with all that type of information:

-raw image file 

-gCode output file

-a text description of DPI, beam diameter, power values, what material was used

-a screenshot of the entire LaserWeb window so people can see the actual job setup values.



Email files to info (at) cohesion3d (dot) com





Feedback welcome.

Thanks,

Ray





**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/2ZpnQUrYFe6) &mdash; content and formatting may not be reliable*
