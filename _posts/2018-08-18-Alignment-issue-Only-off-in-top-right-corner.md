---
layout: post
title: "Alignment issue: Only off in top right corner"
date: August 18, 2018 02:11
category: "Original software and hardware issues"
author: "S Th"
---
Alignment issue: Only off in top right corner. 



Update: Not really resolved, but better



TL/DR: gantry and frame were not square, but I’m still not happy with the result. Any other suggestions why the head would move up (or the beam move down) only when moving to one corner?



I took the gantry out and placed it on the flattest surface in my house, and that top right corner was a couple mm higher than the others. I was able to square it up by screwing the other corners into a flat board with the high corner overhanging the edge. A screw in the fourth corner to a board beneath it allowed me to put a semi-controlled amount of force on that corner and get it back into square. 



Unfortunately I had the EXACT same problem when I put the gantry back in the laser!  I tried shimming the other 3 corners up by as much as a half an inch, but still no dice. The alignment protocol still only got me parallel in three corners, but not the top right. 



I removed the gantry again and it was again out of square in the same direction, which surprised me because I was pulling that corner down relative to the others. 



I squared it up again and put it back in the laser. This time I noticed a small rocking motion and a slight gap between the frame and the gantry. I shimmed this, realigned the first and second mirrors, and still got the same behavior on the laser head. It was significantly better, and by splitting the difference I was able to get an acceptable alignment. I’m still not happy with it though.



Could it be that I just need a more accurate system to align the laser? Or simply more practice?



Original post:



I followed **+HP Persson** ‘s alignment guide and did great all the way through. However, when I finished I did a test fire in the top right corner and I’m about 2mm low. I’m right on the money in all three other corners as you can see in the pictures. (Should be bottom right, then bottom left, then top left, then top right)



Is this a gantry that’s out of square? If so how do I fix that?



![images/f32b31ceeabd96e05f0f3d9be2d961a0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f32b31ceeabd96e05f0f3d9be2d961a0.jpeg)
![images/830566d83c1c2472b64ab920be6d994c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/830566d83c1c2472b64ab920be6d994c.jpeg)
![images/e8f0cfb12370154c36142549b640b104.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8f0cfb12370154c36142549b640b104.jpeg)
![images/3d56747e0c9316e6e84a6337c816dcee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d56747e0c9316e6e84a6337c816dcee.jpeg)

**"S Th"**

---
---
**Joe Alexander** *August 18, 2018 03:52*

you could try adding a small shim under that corner to raise just that corner as its the only sections that is off. Also, why run your drag chain as such? seems weird to anchor it at the front left corner. I have mine tied to the right side of the gantry and most tie it to the top right area as the wires come from that direction anyways. keeps it from stretching across the work area with at top right position as yours seems to do.


---
**S Th** *August 18, 2018 11:48*

**+Joe Alexander** I’ve had a couple people on Facebook suggest shimming, so that’ll probably be the first thing I try. I used to have my drag chain anchored in the top right, but I think it’s too large. It felt like it was putting pressure on the laser head as it would move across the top of the workspace. Also, when the head would move from the bottom left to the top right the chain would sag and it could get into the laser path. I had more room at the bottom of the work area, and with the chain wrapped behind the head there is no way for it to get into the laser path. The chain drags across the work area when it is attached in the top right, too, it just does it at the bottom left instead of the top right like mine. 


---
**Djinn per se** *August 20, 2018 18:31*



I had to use a washer on the withe bracket of the second mirror, may be you too




---
*Imported from [Google+](https://plus.google.com/116589695629402182526/posts/Q3WMjyCBeTh) &mdash; content and formatting may not be reliable*
