---
layout: post
title: "Has anyone tries to control his lasercutter with just an arduino uno running grbl and then send the gcode using another host?"
date: August 05, 2016 08:38
category: "Discussion"
author: "Bart Libert"
---
Has anyone tries to control his lasercutter with just an arduino uno running grbl and then send the gcode using another host? If yes can you tell me how you connected the PWM output to the laser PSU power in ? Or how you connected it up at all ?





**"Bart Libert"**

---
---
**HalfNormal** *August 05, 2016 12:44*

**+Bart Libert** I have done it and it works fine. I used a GRBL sheid to run the servo motors. The PWM pin connects to where the wiper for the pot goes and ground on the ground side of the pot. Be careful. The other side of the pot is +5 volts. 


---
**Bart Libert** *August 05, 2016 13:00*

And is 100% duty cycle (so +5Vdc on the wiper output) 100% power on a 40W laser ? How many mA does your laser shoot then ?




---
**HalfNormal** *August 05, 2016 13:04*

You are correct. 100% duty cycle = 100% laser power or 5 volts at the wiper and ground. All the lasers vary on maximum current and that also varies on maximum laser power. Some of the tubes are really 35 watt tubes being over driven so your max output can be more or less the rated 40 watts.


---
**Bart Libert** *August 05, 2016 14:26*

I allready got that info somewhere that some k40 cutters are in reality only 35Watt, but I actually don't ask this to be used on a K40 but on a machine I'll build all by myself. I allready built a 80W machine with 1220x610mm cutting area and that one is driven by linuxcnc on a pc motherboard. But I'm looking to make a somewhat smaller machine with some added features using a 40Watt tube. So that info would be very interesting. The GUI to drive it would also be something I'll make myself to run on a raspberry PI with a LCD touch interface. There are also plans to add extra functionality like camera positioning (I allready have that one on the 80W machine) and an extra 405nm 0.5W bluray diode laser mounted too on the same head. The reason for the extra 0.5W 405nm bluray laser is because I want to experiment again with super high resolution grayscale engraving. Thats something I don't have to do on my 80W machine since the beam is way to powerfull even at low energy levels.




---
*Imported from [Google+](https://plus.google.com/104850277500909359562/posts/f6VMdkJ23Fq) &mdash; content and formatting may not be reliable*
