---
layout: post
title: "Quick question. Won't screwing this part of the LO head in until it's tight, make contact with the lens and absolutely destroy it with scratches and pressure?"
date: June 10, 2016 10:52
category: "Modification"
author: "Pippins McGee"
---
Quick question.

Won't screwing this part of the LO head in until it's tight, make contact with the lens and absolutely destroy it with scratches and pressure?



Just making sure I'm not doing something wrong.

it won't screw all the way in to make a flush surface as it hits the lens before that point..

![images/be1d2f9c45284677c5e94a2f010fd333.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be1d2f9c45284677c5e94a2f010fd333.jpeg)



**"Pippins McGee"**

---
---
**Christoph E.** *June 10, 2016 11:19*

To me it looks like (depending on the material used) it may scratch the lens but the beam will not hit the scratched part anyway?



Hard to tell, you also don't want the lens to be lose. :-/


---
**Christoph E.** *June 10, 2016 11:24*

What if you put an O-Ring between lens and adapter so it won't hit the surface of the lens?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 10, 2016 11:28*

I'd think you are right that it would scratch the surface of the lens. 

**+Chris E** I think that is what a few people have done if I recall correct.


---
**Pippins McGee** *June 10, 2016 11:31*

good point Chris on the scratched part not being the part the laser passes through. But it just seems unprofessional that the design asks for you to screw this metal piece in until it makes contact with the lens in order for the assembly to be tight.

Thanks Yuusuf - if that is what others have had to do then it sounds like the way to go!

I will look for a soft spacer to use.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 10, 2016 12:03*

**+Pippins McGee** [https://www.bunnings.com.au/kinetic-12mm-fibre-body-washers-5-pack_p4920306](https://www.bunnings.com.au/kinetic-12mm-fibre-body-washers-5-pack_p4920306)

This is the sort of thing I was thinking would be suitable. Some kind of fabric/fibre washer. I believe they generally use in bathroom stuff.


---
**Don Kleinschnitz Jr.** *June 10, 2016 12:17*

I just snugged mine up .... probably does scratch the edge. "Professional" and K40 parts are not usually words that fit in the same sentence ;)


---
**Pippins McGee** *June 10, 2016 12:29*

**+Yuusuf Sallahuddin** cheers m8y, thats perfect.



haha **+Don Kleinschnitz** I guess you have a point .


---
**Pippins McGee** *June 10, 2016 14:14*

jeez, focal distance with LO head does not suit stock bed height at all.

bed needs to be quite a bit higher

going to have to make custom adjustable bed before I can use the LO head...

you find same thing **+Don Kleinschnitz** ?


---
**Christoph E.** *June 10, 2016 14:58*

Higher should not be the problem. It's just 4 screws, put some washers in :-)





Edit: ar. looked it up. It's 5 holding the original bed


---
**Don Kleinschnitz Jr.** *June 10, 2016 16:20*

**+Pippins McGee** I went to a move-able bed and controller from LO for many reasons and that is one:

- accommodate various head sizes

- faster simpler and more accurate setup for multiple material thicknesses

- eventual auto focus setting


---
**Pippins McGee** *June 11, 2016 14:13*

**+Don Kleinschnitz** yep ok fair enough. I hear ya...

I have honeycomb aluminium sheet coming in mail and an adjustable height lab jack stand.


---
**Timo Birnschein** *June 11, 2016 20:11*

I destroyed my lense on my K40 by screwing on the original lens holder too tightly...


---
**Don Kleinschnitz Jr.** *June 11, 2016 21:58*

I think I may just have realized why I did not damage mine: [http://donsthings.blogspot.com/2016/06/k40-air-assist.html](http://donsthings.blogspot.com/2016/06/k40-air-assist.html)


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/ikXMfUQhCWh) &mdash; content and formatting may not be reliable*
