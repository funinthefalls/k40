---
layout: post
title: "Saw this on a Makezine weekly tip"
date: October 17, 2017 02:11
category: "Discussion"
author: "Ned Hill"
---
Saw this on a Makezine weekly tip.  Might be something interesting to try.  I think the keys are that the surface has to be sealed first, before engraving, and shallow engraving depths. #K40ShopTips

 
{% include youtubePlayer.html id="fFwXT2XgQKs" %}
[https://www.youtube.com/watch?v=fFwXT2XgQKs&feature=youtu.be](https://www.youtube.com/watch?v=fFwXT2XgQKs&feature=youtu.be) 





**"Ned Hill"**

---
---
**Don Kleinschnitz Jr.** *October 18, 2017 11:53*

Excellent technique ...

I don't know how the laser will react to a slight finish coat of lacquer or urethane. One can apply a light coat of finish, then engrave and then apply the dye. This will help keep the dye from bleeding and result in crisp lines on the outline. 

In pyrography the burned interface between the smooth area and the engraved area seals the grain with the heat, I am guessing the laser does the same.


---
**Ned Hill** *October 18, 2017 12:11*

**+Don Kleinschnitz** I engrave through poly and lacquer all the time without any issues.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/PLUvfYNNFhV) &mdash; content and formatting may not be reliable*
