---
layout: post
title: "At the showroom of a very known manufacturer of porcelain dinnerware, they have a new collection for a celebrity where all this items are lasered"
date: March 16, 2017 23:49
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
At the showroom of a very known manufacturer of porcelain dinnerware, they have a new collection for a celebrity where all this items are lasered.



![images/bbb1aaf819a651460f2035fdf6c3a42c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bbb1aaf819a651460f2035fdf6c3a42c.jpeg)
![images/ea1a1d91730eda21976de7b78e8167ca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ea1a1d91730eda21976de7b78e8167ca.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**Jonathan Davis (Leo Lion)** *March 17, 2017 00:43*

Okay 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/1YH71Xe4bxp) &mdash; content and formatting may not be reliable*
