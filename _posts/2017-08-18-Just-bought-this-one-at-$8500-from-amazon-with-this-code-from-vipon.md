---
layout: post
title: "Just bought this one at $85.00 from amazon with this code from vipon"
date: August 18, 2017 02:15
category: "Material suppliers"
author: "Ariel Yahni (UniKpty)"
---
Just bought this one at $85.00 from amazon with this code from vipon.



SG4D-6Y4MTN-VLNDYF





**"Ariel Yahni (UniKpty)"**

---
---
**Anthony Bolgar** *August 18, 2017 02:34*

Code will not work for me :(


---
**Ariel Yahni (UniKpty)** *August 18, 2017 02:41*

Go to [vipon.com](http://vipon.com) use the search for laser tube and request a new code


---
**Ariel Yahni (UniKpty)** *August 18, 2017 02:52*

[vipon.com - Orion Motor Tech Premium CO2 Laser Tube for Laser Engraver Cutting Machine (40W)](https://www.vipon.com/product/4199004)


---
**Jorge Robles** *August 18, 2017 07:47*

Oh my! I wanna video tutorial of replacement process :)


---
**Ray Rivera** *August 18, 2017 16:34*

Nice! I ordered mine. Will put in safekeeping as a spare.

![images/a3413648a87eea7f3c573190c893f44b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a3413648a87eea7f3c573190c893f44b.png)


---
**Printin Addiction** *August 18, 2017 22:55*

Never received email confirmation from vipon :( when I created my account.


---
**Anthony Bolgar** *August 19, 2017 01:47*

**+Ray Rivera** Keep in mind that a laser tube has a shelf life. Even if the tube is not being used, the lifespan is ticking away. Cheap tubes are usually good for about 18 months of storage, a high quality CO2 tube may get you 3-4 years of shelf life.


---
**Ray Rivera** *August 19, 2017 06:20*

Thanks Anthony. With the current burn rate I am doing right now it won't be on the shelf more than a few months. Been going through a lot of power on time trying to get the flex hinges just right.


---
**Ray Rivera** *August 22, 2017 16:47*

Received and onboxed just o inspect, man this thing was well packed. Box in a box. Tube was wrapped in bubble wrap and then packed in styrofoam before being placed in the other boxes.




---
**Ariel Yahni (UniKpty)** *August 22, 2017 16:57*

Nice to hear **+Ray Rivera**​ so I got a chance to receive this intact :)


---
**Ariel Yahni (UniKpty)** *August 26, 2017 18:39*

Go mine today. Seems it survived

![images/6df9a685e9e244ba25c890db6e1d698a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6df9a685e9e244ba25c890db6e1d698a.jpeg)


---
**Ariel Yahni (UniKpty)** *August 26, 2017 18:40*

![images/cc4e4dae515749111aa625a899091aa8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc4e4dae515749111aa625a899091aa8.jpeg)


---
**Jorge Robles** *August 26, 2017 18:49*

😍😍


---
**Ray Rivera** *August 26, 2017 18:51*

Nice!!!


---
**Anthony Bolgar** *August 26, 2017 20:43*

I just bought the 60W (55mm diameter X 1000mm length , so really a true 50W tube) for $144. Great deal, now I need to decide what machine gets it, will probably build one from scratch to use this tube.


---
**Anthony Bolgar** *August 26, 2017 21:06*

Wow, got mine just in time, they are all gone at the 50% off price. Literally minutes after I ordered they were all gone. :)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/B5pY2UxiWr4) &mdash; content and formatting may not be reliable*
