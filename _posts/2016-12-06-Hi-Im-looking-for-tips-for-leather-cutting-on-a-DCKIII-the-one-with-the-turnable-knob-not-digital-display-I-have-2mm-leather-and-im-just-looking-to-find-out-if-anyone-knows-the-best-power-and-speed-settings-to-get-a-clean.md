---
layout: post
title: "Hi, Im looking for tips for leather cutting on a DCKIII (the one with the turnable knob, not digital display) I have 2mm leather and im just looking to find out if anyone knows the best power and speed settings to get a clean"
date: December 06, 2016 20:23
category: "Materials and settings"
author: "Shane Lynch"
---
Hi,



Im looking for tips for leather cutting on a DCKIII (the one with the turnable knob, not digital display)



I have 2mm leather and im just looking to find out if anyone knows the best power and speed settings to get a clean cut through without alot of burning. 



I have been trying different settings and cant find the sweet spot. some cuts take alot of passes and some cuts char the edges, which isnt great. 



Im working with a stock laser, no mods made to it just yet, but if you can recommend some good early modifications please do.



Any tips appreciated, 



Shane





**"Shane Lynch"**

---
---
**Alex Krause** *December 06, 2016 20:38*

**+Yuusuf Sallahuddin**​


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 07, 2016 05:49*

**+Shane Lynch** From my experience (mostly with natural vegetable tanned leather) it's best to run with low power (4-6mA) & run multiple passes. If I recall correct, 20mm/s @ 2 passes will usually get through about 1.5-2mm leather. Just make sure you have it in focus (centre of leather @ ~50.8mm/2 inches from lens).



Pretty much nothing you do will prevent it burning, but air assist will help with preventing as many scorch marks/soot stains at the cut site edges.



Best bet is to clean up the edges afterwards (water/sandpaper/cloth) to remove as much of the char as possible.


---
**Shane Lynch** *December 11, 2016 17:33*

Thanks for the advice. i was trying to do a one run pass and i was either shriveling the leather or not going through the whole thing.



Ill just keep messing with the settings, ive alot of leather on hand to play with.


---
*Imported from [Google+](https://plus.google.com/102016608119814536184/posts/W1nE3uUwAYM) &mdash; content and formatting may not be reliable*
