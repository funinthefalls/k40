---
layout: post
title: "My simple water cooling system. Home Depot bucket"
date: October 15, 2015 17:21
category: "Hardware and Laser settings"
author: "David Cook"
---
My simple water cooling system.  Home Depot bucket.  I added a little blue food coloring so i could see the volume in the laser tube and see the airbubbles easier. I added a cover to keep dust from entering the "tank".

I need to add a coolant flow switch next in the interlock circuit.

![images/5282ec548bd6c4e1ee7bdd8aee61c0ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5282ec548bd6c4e1ee7bdd8aee61c0ed.jpeg)



**"David Cook"**

---
---
**Gregory J Smith** *October 16, 2015 02:05*

A cover is simple smart. Colour is great. The choice will be up to others (daughter who must be obeyed). Blood red would be my choice :-)


---
**Joseph Coco** *October 16, 2015 03:13*

I don't have experience with this, but I've seen a flowmeter test which said a lidded container without air holes slows the rate (
{% include youtubePlayer.html id="PI6TKkP4mqE" %}
[https://www.youtube.com/watch?v=PI6TKkP4mqE&index=3&list=PLLofGrZX5F84XyteWO8BTHltbnrJE3g4i](https://www.youtube.com/watch?v=PI6TKkP4mqE&index=3&list=PLLofGrZX5F84XyteWO8BTHltbnrJE3g4i))


---
**David Cook** *October 16, 2015 03:22*

Its a decent size cut i put through the side of the cover.  Also its just loosely sitting on top. Not air tight at all. 


---
**Kirk Yarina** *October 16, 2015 18:17*

I've read that algae will grow in plain water and gum up the tube, and is also a problem with water cooled computer parts.  A laser equipped friend suggested windshield washer fluid (what I use), others use RV antifreeze, both of which supposedly keep the green stuff from growing.  If you're in the states near a Wally World they're both about $2.50US a gallon.  Cheap WWF is blue, RVA is pink and a little food coloring could make it red (sorry dear, that's how it comes :) )


---
**David Cook** *October 16, 2015 18:25*

**+Kirk Yarina**  Thanks for the advice.   I'll give that a shot,  makes sense.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/ZzvzZkqqor6) &mdash; content and formatting may not be reliable*
