---
layout: post
title: "New Web Based 3D slicer This software is multi machine use"
date: February 13, 2016 17:17
category: "Software"
author: "HalfNormal"
---
New Web Based 3D slicer



This software is multi machine use. It can be used with 3D printers, CNC and yes lasers!

Nice part is that the output for the laser is DXF or SVG

Link here; [https://grid.space/kiri](https://grid.space/kiri)

Video here;


{% include youtubePlayer.html id="qEgwf28nKag" %}
[https://www.youtube.com/watch?v=qEgwf28nKag](https://www.youtube.com/watch?v=qEgwf28nKag)





**"HalfNormal"**

---
---
**ED Carty** *February 14, 2016 00:27*

Very Nice


---
**Anthony Bolgar** *February 15, 2016 15:25*

**+Peter van der Walt** Anything here that you can use?


---
**Anthony Bolgar** *February 15, 2016 15:35*

I do like being able to slice objects into layers to be cut and reassembled later.


---
**Anthony Bolgar** *February 15, 2016 15:37*

Not sure if you saw this ion another post, but I'll ask again....do you have any need for an M2 board and dongle from a K40? If you want it, it is yours.


---
**Anthony Bolgar** *February 15, 2016 16:36*

Send me your address info and I will stick it in the mail tomorrow (Today is a holiday, Family Day her in Canada)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/RWvrfsZxc68) &mdash; content and formatting may not be reliable*
