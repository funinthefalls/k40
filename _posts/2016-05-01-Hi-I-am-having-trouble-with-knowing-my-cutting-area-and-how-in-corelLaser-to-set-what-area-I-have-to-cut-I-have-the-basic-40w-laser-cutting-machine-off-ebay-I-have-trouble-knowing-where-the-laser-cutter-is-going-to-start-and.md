---
layout: post
title: "Hi I am having trouble with knowing my cutting area and how in corelLaser to set what area I have to cut I have the basic 40w laser cutting machine off ebay I have trouble knowing where the laser cutter is going to start and"
date: May 01, 2016 21:34
category: "Discussion"
author: "Donna Gray"
---
Hi I am having trouble with knowing my cutting area and how in corelLaser to set what area I have to cut I have the basic 40w laser cutting machine off ebay I have trouble knowing where the laser cutter is going to start and the area it is going to cut I have been using card stock and doing 1 cut to see where it is going to cut before I do the real thing which is a major waste of time can anyone help me to set cutting area and to know where it will cut first in corelLaser





**"Donna Gray"**

---
---
**Jim Hatch** *May 01, 2016 22:34*

Make a small square (or dot) in LaserDRW in the upper left corner. Then send it to the laser but make sure you have no offsets (X-Refer Y-Refer should be set to 0.0000) in the popup control window. That will give you the 0,0 point LaserDRW will refer everything else off of.



Then measure from the left rail to the edge of the square (or dot). Measure from there to the rail on the front of the machine. Draw a box that size in LaserDRW and cut it out of 1/8 or 1/4" material. You might also want to etch the measurement inside the box so you remember what it is. 



The resulting piece of plywood can be used as a spacer between your rail and the material you want to cut so the upper left corner of the material is always on the 0,0 mark for your machine. 



Once you know where 0,0 is you can move your cut within the LaserDRW canvas to where you want it on the material or keep your drawing on the 0,0 in LaserDRW and use the Refer settings in the cut dialog popup to specify where you want the cut to start.



BTW, this should work for CorelLaser just as well as LaserDRW. You're just trying to get a known fixed point where you can then refer all other measurements from.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 23:10*

I just placed a piece of 3mm MDF down on the cutting area, covering pretty much the whole area. K40 can cut 300x200mm area stock. So you want to cut a 300x200mm area out of the MDF (or plywood) leaving the outside piece as Jim says, as a guide.


---
**Donna Gray** *May 02, 2016 01:32*

Thanks I will try that Yuusuf Sallahuddin (Y.S. Creations)


---
**Donna Gray** *May 03, 2016 04:54*

Yuusuf Sallahuddin (Y.S. Creations)Jim Hatch I have worked out cutting space I have also worked out to change the setting in the properties to the M2 Laser setting as I read I had a Nanno M2 marked on the electrical section so I tried it and now my laser cutter is cutting much faster It was moving super slow before this I stumbled across a you yube video showing how to change the settings Thanks everyone for your help The only thing is now that it is moving faster it doesn't cut my 3mm MDF all the way through Sorry I have probably asked this question before but what speed setting and mA setting do I need my Laser settings ??


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 03, 2016 07:50*

To cut 3mm MDF, I use power of around 4-6mA & 25mm/s & 2/3 passes Donna. I tend not to do things much faster than 6mA as it creates more smoke. I prefer to do a couple of passes (even though it takes more time) than to have more charred edges. If I was you, I'd set the speed to 25mm/s & power to around 6mA (as a start). Then in the cutting dialog window there is the Repeat option. I would put 10 in there & let it run 3 before I even bother checking, but after 3 I would check every time a pass has completed if the cut is all the way through. I use magnets to hold the piece to my cutting area (to prevent me moving things when I check it) & I just use a needle or something suitably thin to to try lift one area of the cut. If it lifts easily, it's cut through. If not, then let it do another pass & test again.



Also, a side note, I use 4mA power & do a quick Test fire before I do anything to determine where it is going to hit. Other's have attached laser pointers to their air-assist that shows where the laser will fire with a red dot (more expense).


---
*Imported from [Google+](https://plus.google.com/103145403582371195560/posts/i8sjY6JvsZ1) &mdash; content and formatting may not be reliable*
