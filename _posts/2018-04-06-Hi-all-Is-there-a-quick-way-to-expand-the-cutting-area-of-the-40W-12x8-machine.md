---
layout: post
title: "Hi all, Is there a quick way to expand the cutting area of the 40W 12x8 machine?"
date: April 06, 2018 01:28
category: "Modification"
author: "Gabriel Rodriguez"
---
Hi all,

Is there a quick way to expand the cutting area of the 40W 12x8 machine?





**"Gabriel Rodriguez"**

---
---
**Dan Stuettgen** *April 06, 2018 01:47*

Yes. Take out the metal frame that is spring loaded and them use something to support the material you want to cut at the correct focal length.  You will gain a good amount if cutting area.


---
**greg greene** *April 06, 2018 01:47*

reciprocating saw


---
**Ned Hill** *April 06, 2018 02:18*

Nothing quick.  You would need to relocate the power supply, cut out the partition and add longer rails.


---
**Dan Stuettgen** *April 06, 2018 02:22*

Guess we should ask how much space you want ti expand to?.  What size do you want to be able to cut?


---
**Chuck Comito** *April 07, 2018 23:33*

Remove everything from the k40 and remount it to openbuilds extrusion and viola it's as big as you want!! :-) 


---
*Imported from [Google+](https://plus.google.com/117599648666727975189/posts/SfYsCJ344tZ) &mdash; content and formatting may not be reliable*
