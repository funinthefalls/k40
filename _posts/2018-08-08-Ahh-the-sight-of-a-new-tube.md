---
layout: post
title: "Ahh the sight of a new tube"
date: August 08, 2018 22:53
category: "Discussion"
author: "Ned Hill"
---
Ahh the sight of a new tube. Makes me all happy inside 🤗



Opted for the one sold by LightObject. Could have saved some money from another vendor but LOs tubes come prewired and are tested at time of shipping.  Also the tube construction looks very professional and high quality.  





![images/55c6f8ed7de4739554267ba0aa8b2cfb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55c6f8ed7de4739554267ba0aa8b2cfb.jpeg)
![images/30f838118d8ebf74ddf7a2ca2ff7166e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/30f838118d8ebf74ddf7a2ca2ff7166e.jpeg)

**"Ned Hill"**

---
---
**James Rivera** *August 08, 2018 23:23*

Please do a follow-up post on how it performs once you have it installed!


---
**Ned Hill** *August 08, 2018 23:33*

Will do!




---
**Mike Meyer** *August 09, 2018 00:44*

If you were to guess, how many hours do you think you got out of the original k40 tube? Did it die a slow death or did it just tank?


---
**Ned Hill** *August 09, 2018 01:07*

Not sure on hours.  I've had my laser for just over 2 years now.  I would classify my use overall as moderate with some heavy production runs in that time.  A few months ago I noted that the tube was drifting into a higher TM mode and I was getting much less power.  It still works fine for 1/8" material,  just have to cut a bit slower and at higher power.  It now really struggles to cut 1/4" ply even in 2 passes.   Even then I'm going so slow it results in lots of charing.   Decided it was time and I'm going to do a bunch of other upgrades at the same time.  


---
**Brandon Satterfield** *August 09, 2018 01:33*

SPTs are good stuff. I tried to join the laser product resell market, but light objects owns it. They are a good company with good products. I still use SPT products in my laser CNCs and they make products that can compete with the name brands out of the same region great choice **+Ned Hill** 


---
**Don Kleinschnitz Jr.** *August 09, 2018 03:07*

**+Ned Hill** what do you plan to do with the old tube?  😁.


---
**Ned Hill** *August 09, 2018 04:04*

**+Don Kleinschnitz** Lol. Well since it does still have a bit of life left in it, I was planning to hang on to it as an emergency back up.  Why do you ask? 


---
**Don Kleinschnitz Jr.** *August 10, 2018 12:19*

**+Ned Hill** I have some tests on my list to characterize the water conductivity and need a test setup with a laser and its water jacket. The laser does not have to work.

Then again it may take me some time to get to it. I figured if you were trashing it I would ask :).

I have a long list of stuff waiting in the shop. :(


---
**Anthony Bolgar** *August 10, 2018 22:11*

**+Don Kleinschnitz** I have an old 30W that is fading, would that work for you?


---
**Don Kleinschnitz Jr.** *August 11, 2018 00:12*

**+Anthony Bolgar**  I think it would.




---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/Mm6anmMg48c) &mdash; content and formatting may not be reliable*
