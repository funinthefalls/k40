---
layout: post
title: "Had not seen this anywhere in the description it already has interior lights installed"
date: June 30, 2017 03:22
category: "Discussion"
author: "William Kearns"
---
Had not seen this anywhere in the description it already has interior lights installed 

![images/42ca7e9c70dca93246e4493256c5b0b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42ca7e9c70dca93246e4493256c5b0b8.jpeg)



**"William Kearns"**

---
---
**Don Kleinschnitz Jr.** *June 30, 2017 03:29*

This is a new machine with led lights pre-installed? 

**+Arthur Wolf**, Soon they will come with a Smoothie installed :)


---
**William Kearns** *June 30, 2017 03:30*

**+Don Kleinschnitz** yup straight out the box


---
**William Kearns** *June 30, 2017 03:31*

Looks like some better tube mounts as well?![images/478fb638751bcb2835a6ebb972f3ee08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/478fb638751bcb2835a6ebb972f3ee08.jpeg)


---
**Dushyant Ahuja** *June 30, 2017 03:53*

Converting to Smoothie is really easy now. **+Ray Kholodovsky** mini board is a drop-in bundle. I modified my laser in 10 minutes - now I can use laserweb now. 


---
**Don Kleinschnitz Jr.** *June 30, 2017 04:11*

WOW! I wonder if they sell these mounts as spare parts :).


---
**Don Kleinschnitz Jr.** *June 30, 2017 04:12*

Do you think they are watching us ........ creeepy!


---
**William Kearns** *June 30, 2017 05:20*

they are bound to be 




---
**Jonas Rullo** *June 30, 2017 06:22*

That looks exactly like the one we got over a year ago. LED lights were on a high voltage converter. Same tube clamps. 


---
**William Kearns** *June 30, 2017 06:24*

is that one still going now?




---
**Arthur Wolf** *June 30, 2017 09:24*

**+Don Kleinschnitz** If you look at one of my recent posts, we are actually working on having Open-Source, pre-assembled pro laser machines with Smoothieboard pre-installed for sale soon-ish. None will be as small as the  K40 though.


---
**Don Kleinschnitz Jr.** *June 30, 2017 12:40*

**+Arthur Wolf** I did not notice the price? 


---
**Arthur Wolf** *June 30, 2017 12:42*

**+Don Kleinschnitz** Some of the machines are at [robotseed.com](http://robotseed.com), the prices include shipping and instructions, to France, for other destinations we'd have to actually look up costs.


---
**Steve Clark** *June 30, 2017 17:40*

Looks the same as mine only it is red/white in color. The light strip works good but I carefully peeled the mine off and moved it up and under the ledge shown in your picture to get rid of the glare.



There are two plugs on the back housing  for the fan and water pump. Mine were really cheap and loose plus just two prong…no ground so I have replaced them. The laser tube mounts  look the same as mine.



I  bought it the first of the year and it was about $365 with tax and free shipping. 




---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/hFB85vrUeFM) &mdash; content and formatting may not be reliable*
