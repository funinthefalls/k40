---
layout: post
title: "Trying to convert a drawing from 3mm material to 5mm material"
date: March 09, 2017 23:35
category: "Discussion"
author: "Robert Selvey"
---
Trying to convert a drawing from 3mm material to 5mm material. I only want to change the size of the connecting joints, does anyone know of a tutorial for CorelDRAW that explains how to do it?









**"Robert Selvey"**

---
---
**Ned Hill** *March 10, 2017 12:17*

It's not easy to do with CD, as far as I'm aware of.  To me you would need to convert the drawing to curves, if not already, and then resize the joints manually with the shape tool. 


---
**Robert Selvey** *March 10, 2017 12:25*

Is there another program out there that would be easier to use ?


---
**Gavin Dow** *March 14, 2017 14:20*

I use DraftSight (free) for modifying .dxf files. I have also had some luck just resizing existing curves in CorelDraw, or drawing new rectangles and centering them over the old curves. If your assembly is supposed to have specific tolerances, make sure you align your new rectangles correctly (centered, inside, or outside justified). You can delete the old curves, or move them onto a different layer for 3mm stuff only, so you can have it if you need it later on.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/4FAYTPst9H9) &mdash; content and formatting may not be reliable*
