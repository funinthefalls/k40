---
layout: post
title: "I want to get a new cutting head with an air assist nozzle"
date: October 27, 2016 08:02
category: "Original software and hardware issues"
author: "Gerard Mullan"
---
I want to get a new cutting head with an air assist nozzle. The eBay page tells me there are 2 models of head for the k40. How do i know which one I have? Thanks





**"Gerard Mullan"**

---
---
**Ariel Yahni (UniKpty)** *October 27, 2016 10:41*

What are the 2 options you are seeing? Can you post the links? 


---
**Bob Damato** *October 27, 2016 16:07*

Probably this one? [http://www.ebay.com/itm/252303388050?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/252303388050?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT)

[ebay.com - Details about  CO2 K40 Laser Stamp Engraver Head Mirror 20mm Focus Lens 18mm Integrative Mount](http://www.ebay.com/itm/252303388050?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Gerard Mullan** *October 27, 2016 17:08*

Yeah, I'm curious about that post. It's saying not to buy if the version of laser head you have doesn't have air assist. I thought that was to change the laser head to an air assist model.


---
*Imported from [Google+](https://plus.google.com/117648854578727329148/posts/gXjeLabitAJ) &mdash; content and formatting may not be reliable*
