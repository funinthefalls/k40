---
layout: post
title: "Does anyone know if Corel academic version is different than the home/student version other than the price?"
date: September 13, 2016 17:35
category: "Discussion"
author: "Rod Presnell"
---
Does anyone know if Corel academic version is different than the home/student version other than the price?  Also, can you use the academic version with the laser engraver?  





**"Rod Presnell"**

---
---
**Gee Willikers** *September 13, 2016 17:54*

I think you need the full version to use the laser cutting plugin. Corel needs to be able to run the script and I don't think the basic/academic versions do.


---
**Jim Hatch** *September 13, 2016 18:45*

What is the version actually called? I have X8 with academic pricing (you need a valid educational institution email to validate or other proof of employment by a school). The license is not for commercial work though. Okay if the stuff you do is for friends, family, students, etc. 



The difference between a full retail and the academic pricing is the licensing. The Home & Student actually installs differently with different named executables so that's why H&S doesn't work with the plug in.


---
**Rod Presnell** *September 13, 2016 18:57*

CorelDRAW Graphics Suite X8 Academic is what it says on amazon.


---
**Jim Hatch** *September 13, 2016 22:48*

**+Gee Willikers**​ the plugin appears to be looking for a specific directory and executable to install itself. Both are different with Home & School. Technically it looks like you could fool it with some registry changes, directory and executable renaming but it's a tedious process to work out all the connections.



**+Rod Presnell**​ I have Corel Draw Graphics Suite X8 and it works fine.


---
*Imported from [Google+](https://plus.google.com/103834600584593086520/posts/Rdha3kUyZdE) &mdash; content and formatting may not be reliable*
