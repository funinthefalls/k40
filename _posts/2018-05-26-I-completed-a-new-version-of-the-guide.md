---
layout: post
title: "I completed a new version of the guide"
date: May 26, 2018 16:54
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
I completed a new version of the guide. It has the digital panel added **+HalfNormal** along with corrections and closures on connectors that went nowhere.



<b>"Troubleshooting A K40 Laser Power Subsytem 1.0" *</b>



[https://drive.google.com/file/d/1yAGfqw3VVKW91qEpCVFOuAwYoyI7PPuh/view?usp=sharing](https://drive.google.com/file/d/1yAGfqw3VVKW91qEpCVFOuAwYoyI7PPuh/view?usp=sharing)



<b>The guide is located under "About Community"</b>



<b>*Review Appreciated</b>

It would be great if folks could tread through it looking for any kind of errors or confusion. I have been looking at it to long to see anything.



<b>Associated post here:</b>

[http://donsthings.blogspot.com/2018/05/troubleshooting-k40-laser-power-system.html](http://donsthings.blogspot.com/2018/05/troubleshooting-k40-laser-power-system.html)







**"Don Kleinschnitz Jr."**

---
---
**David Piercey** *June 04, 2018 23:03*

I cannot cheer this enough. Thank you!


---
**Frank W** *July 17, 2018 00:50*

Just what I needed. Thank you for taking the time to make this.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/3Z6ggsZ1TWH) &mdash; content and formatting may not be reliable*
