---
layout: post
title: "I'm replacing my K40 control panel with a custom one"
date: August 31, 2018 18:23
category: "Modification"
author: "Robert Luken"
---
I'm replacing my K40 control panel with a custom one. The current one has the digital power control and I'm replacing it with an analog pot and DVM display.  I know how to connect this and the Test Fire button to the LPS but I can't tell how the Laser On/Off switch and the associated LED should be connected. It appears that the Laser On/Off just kills 5V power to the panel. 

Anyone have a schematic or know how to do this?





**"Robert Luken"**

---
---
**HalfNormal** *August 31, 2018 18:44*

You will find everything you need to know here;

[http://donsthings.blogspot.com/?m=1](http://donsthings.blogspot.com/?m=1)

[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/?m=1)


---
**Don Kleinschnitz Jr.** *August 31, 2018 20:47*

Schematics and help in doing such a conversion is as **+HalfNormal** suggests, on my blog. If you have trouble post here...


---
*Imported from [Google+](https://plus.google.com/107953279340834579617/posts/NvnSWfF8yaU) &mdash; content and formatting may not be reliable*
