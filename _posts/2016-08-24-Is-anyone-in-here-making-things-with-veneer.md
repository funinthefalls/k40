---
layout: post
title: "Is anyone in here making things with veneer ?"
date: August 24, 2016 14:27
category: "Object produced with laser"
author: "Bart Libert"
---
Is anyone in here making things with veneer ? If yes, what kind of veneers doe you use and what kind of things do you make with it ? 

![images/7bf80621312bc3e19d56691a67873f0b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7bf80621312bc3e19d56691a67873f0b.jpeg)



**"Bart Libert"**

---
---
**Bob Damato** *August 24, 2016 16:46*

Funny you mention that, I just started. I wanted to make a marquetry design of the '55 Thunderbird and put it on a table. I have some veneer that I just ordered and have been experimenting with different types and power levels that give it a nice cut and not make it burst into flames.  :) 



my next step is to somehow get the design into corel and cut it with different shades of wood. That should be fun.



bob


---
**Paul Hayler** *August 25, 2016 00:24*

Bent wood rings,bangles and guitar pics.  Then I personalise them by engraving.


---
**Jeremy Hill** *August 26, 2016 17:08*

I bought a pack of different veneers from amazon to give me a start with this- [https://www.amazon.com/gp/product/B003F0C9EW/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B003F0C9EW/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
*Imported from [Google+](https://plus.google.com/104850277500909359562/posts/PZsJXv7DmJr) &mdash; content and formatting may not be reliable*
