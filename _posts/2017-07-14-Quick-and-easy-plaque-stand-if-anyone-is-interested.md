---
layout: post
title: "Quick and easy plaque stand if anyone is interested"
date: July 14, 2017 15:22
category: "Object produced with laser"
author: "Madyn3D CNC, LLC"
---
Quick and easy plaque stand if anyone is interested. Can be easily edited in Corel, or whatever your preferred software may be. 

![images/27a74a37fe7066836484cbf7f5ba184c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/27a74a37fe7066836484cbf7f5ba184c.png)



**"Madyn3D CNC, LLC"**

---
---
**Anthony Bolgar** *July 14, 2017 16:04*

Thanks for sharing.


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/Am4biMjULbH) &mdash; content and formatting may not be reliable*
