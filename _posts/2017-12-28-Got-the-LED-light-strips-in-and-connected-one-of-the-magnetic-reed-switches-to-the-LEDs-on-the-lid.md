---
layout: post
title: "Got the LED light strips in and connected one of the magnetic reed switches to the LEDs on the lid"
date: December 28, 2017 07:06
category: "Modification"
author: "Tech Bravo (Tech BravoTN)"
---
Got the LED light strips in and connected one of the magnetic reed switches to the LEDs on the lid. Just waiting on the LightObject air assist head. Getting ready to rock and roll this year!




{% include youtubePlayer.html id="CyqntvxLZZg" %}
[https://youtu.be/CyqntvxLZZg?list=PL6E6gv4lDIgkeV5TpHpwSDoY5et6rJUFb](https://youtu.be/CyqntvxLZZg?list=PL6E6gv4lDIgkeV5TpHpwSDoY5et6rJUFb)





**"Tech Bravo (Tech BravoTN)"**

---
---
**Don Kleinschnitz Jr.** *December 28, 2017 10:17*

I suggest that you also put a interlock on the back cover and your water pump if you have not already.



Its better to put a defeat block on the actual door switch that you want to override than a master override switch.



I defeat mine with a magnet on the switch so that you cannot close the cover with the defeat installed. Override switches get left on and then you are operating unsafe. Your eyes are not worth taking a risk.  



FYI: [donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Tech Bravo (Tech BravoTN)** *December 28, 2017 13:50*

**+Don Kleinschnitz** awesome ideas and i will take your advice. i have extra magnets so that will become my "service jig". thanks for your input. btw: i've been a fan of your blog ever since i began my journey into laser engravers :)


---
**Alex Krause** *December 28, 2017 18:14*

I have a whole box of just metric stick on tape measures...


---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/4UkJVy3WfQq) &mdash; content and formatting may not be reliable*
