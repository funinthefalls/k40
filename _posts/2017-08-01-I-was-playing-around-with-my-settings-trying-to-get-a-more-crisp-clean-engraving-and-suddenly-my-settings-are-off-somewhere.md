---
layout: post
title: "I was playing around with my settings trying to get a more crisp, clean engraving and suddenly my settings are off somewhere"
date: August 01, 2017 19:17
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
I was playing around with my settings trying to get a more crisp, clean engraving and suddenly my settings are off somewhere.



I use Corel Laser. The issue is that when I set my piece size in Corel Draw/Laser...it actually engraves it smaller than the size I set. For example...If I'm trying to engrave something 3" wide, it actually engraves 2" wide. 



Has anyone had this problem before using any software? Any guidance on what I can check or change would be greatly appreciated. 



#K40software #K40CorelLaser #K40Settings





**"Nathan Thomas"**

---
---
**Martin Dillon** *August 01, 2017 19:22*

Yes.  I can't do a screenshot right now, but there is a setting in Corel laser where you can set the DPI I think.  If you change that it will scale your drawing.  For example if the default is 500 and you set it to 1000 you drawing will be half size.  You just need to reset everything to default settings.


---
**Nathan Thomas** *August 01, 2017 20:01*

You are the man!


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/R3hAAK2xCTs) &mdash; content and formatting may not be reliable*
