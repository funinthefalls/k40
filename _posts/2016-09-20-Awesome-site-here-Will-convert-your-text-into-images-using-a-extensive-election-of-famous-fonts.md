---
layout: post
title: "Awesome site here. Will convert your text into images using a extensive election of famous fonts"
date: September 20, 2016 12:21
category: "Repository and designs"
author: "Ariel Yahni (UniKpty)"
---
Awesome site here. Will convert your text into images using a extensive election of famous fonts [http://fontmeme.com/famous-fonts/](http://fontmeme.com/famous-fonts/)





**"Ariel Yahni (UniKpty)"**

---
---
**Scott Marshall** *September 20, 2016 12:49*

Very nice. You find a lot of nuggets like this one. Thanks for sharing.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 20, 2016 13:22*

That's definitely cool. Always good to have extra resources like this.


---
**Rodney Huckstadt** *September 22, 2016 01:19*

Just did a pen box using this site for the text,  looks great (used lord of the rings font)



[https://drive.google.com/open?id=0B4Fvr6rQHzh9amJ4OGFwcC1oSUk](https://drive.google.com/open?id=0B4Fvr6rQHzh9amJ4OGFwcC1oSUk)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 22, 2016 05:06*

**+Rodney Huckstadt** Nice... "One box to contain them all!"


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/GpAE3sRfdq4) &mdash; content and formatting may not be reliable*
