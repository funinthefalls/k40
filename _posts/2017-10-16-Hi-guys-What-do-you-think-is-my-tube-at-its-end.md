---
layout: post
title: "Hi guys, What do you think, is my tube at it's end?"
date: October 16, 2017 18:26
category: "Original software and hardware issues"
author: "E Elzinga"
---
Hi guys,



What do you think, is my tube at it's end?

It lost power, hardly used it. Maybe for about 1 hour. Didn't use it for a while because Corel kept crashing, was unusable. After switching to C3D it's doing this (also with the old board) I'm using fresh distilled water. It hardly cuts paper on full power (10mA) let alone wood.



Anybody got an idea I could try?



Tia Erik.





**"E Elzinga"**

---
---
**Chris Hurley** *October 17, 2017 01:32*

The video is...not very useful. Try a test fire on 5-6 MA for 10 seconds . 


---
*Imported from [Google+](https://plus.google.com/113837421619430195320/posts/1eeVAvGV1ME) &mdash; content and formatting may not be reliable*
