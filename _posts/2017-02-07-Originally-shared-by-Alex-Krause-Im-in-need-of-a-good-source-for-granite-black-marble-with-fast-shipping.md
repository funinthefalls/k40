---
layout: post
title: "Originally shared by Alex Krause :( I'm in need of a good source for granite/black marble with fast shipping..."
date: February 07, 2017 03:29
category: "Material suppliers"
author: "Alex Krause"
---
<b>Originally shared by Alex Krause</b>



:( I'm in need of a good source for granite/black marble with fast shipping... My girlfriend's father passed away unexpectedly lastnight 





**"Alex Krause"**

---
---
**Ned Hill** *February 07, 2017 04:14*

Sorry to hear that. How big of a piece are you looking for?




---
**Anthony Bolgar** *February 07, 2017 09:13*

Sorry to hear that.  What size were you looking for?


---
**HalfNormal** *February 07, 2017 12:31*

Sorry for your loss. You might find a nice piece at a hardware store or at a local cabinet maker.


---
**Alex Krause** *February 07, 2017 14:49*

Needing an 8x12 piece at a minimum of 1/2" thick I could always cut down a larger piece with a tile saw


---
**Ned Hill** *February 07, 2017 15:06*

Hmm..I have a 4x6x1" piece I was going to be willing to donate, but nothing that big.  I've picked up scrap pieces from countertop installers before.  An installer would probably be a good person to consult for a quick source.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/FJVC1cXnH8X) &mdash; content and formatting may not be reliable*
