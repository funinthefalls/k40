---
layout: post
title: "We will see how this works out!"
date: November 18, 2016 01:44
category: "Modification"
author: "HalfNormal"
---
We will see how this works out!



HFS (R) Plate 8x8"; Overall Height 10"; Lab Jack Scissor Stand Platform LAB $26.99 USD with free shipping.



[http://www.ebay.com/itm/HFS-R-Plate-8x8-Overall-Height-10-Lab-Jack-Scissor-Stand-Platform-LAB/272299082598?_trksid=p2054502.c100227.m3827&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20160908103841%26meid%3D09dcfacf893242688ede5f9a75aee064%26pid%3D100227%26rk%3D1%26rkt%3D4%26sd%3D272299082598](http://www.ebay.com/itm/HFS-R-Plate-8x8-Overall-Height-10-Lab-Jack-Scissor-Stand-Platform-LAB/272299082598?_trksid=p2054502.c100227.m3827&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20160908103841%26meid%3D09dcfacf893242688ede5f9a75aee064%26pid%3D100227%26rk%3D1%26rkt%3D4%26sd%3D272299082598)







**"HalfNormal"**

---
---
**Kelly S** *November 18, 2016 02:56*

I was just looking at those the other day, when you get it and have it set up definitely wanna see.  


---
**Mark Leino** *November 18, 2016 02:56*

Ummmm I might have missed something... What's it for?


---
**Anthony Bolgar** *November 18, 2016 03:04*

adjustable Z table for laser bed


---
**Kelly S** *November 18, 2016 03:36*

*On the cheap.




---
**Andy Shilling** *November 18, 2016 15:17*

I've got mine. I've taken the two plates off the top and bottom so I can fix a new bed it the top via the mounting holes and eventually fix it to the Base of the laser cabinet.

The only issue I have at the moment is because to knob raises with the stand a simple stepper and belt won't work without a tensioner. I need to find a way to mount the stepper on to the middle bar on the stand then it will be ok ( I think) 


---
**HalfNormal** *November 18, 2016 15:37*

I am going to see if I can mount the raising mech on the bottom to make it stationary and easier to automate. 


---
**Kelly S** *November 18, 2016 15:43*

**+HalfNormal** that is a good idea, that way wouldn't even need a stepper if you extended the knob outwards through the case.  Super looking forward to seeing how these work out and will be ordering one soon for myself as well.  And I would keep the bottom plate and drill 4 holes to bolt it to the bottom of the machine and probably just have the bed sit ontop.


---
**Andy Shilling** *November 18, 2016 15:44*

That's not a bad idea I'll have a look in a bit and see if it's possible to move them around. The threaded bars in the middle have circlips on so they will be easy to remove, it's just whether or not the running slot it the right diameter.



I'll let you know in a little while but if it works that would be a cracking idea. I'd probably just leave it as a manual lift if I could mount it like that and just put a hole in the front for the knob.


---
**HalfNormal** *November 18, 2016 15:45*

**+Kelly S** Those are all my thoughts too! We will see how it ends up.


---
**Niels Sorensen** *November 18, 2016 16:02*

[http://www.thingiverse.com/thing:1648481](http://www.thingiverse.com/thing:1648481)



Instead of an adjustable bed how about and adjustable lens mount.    I was working on an adjustable bed and had considered these lab tables but I'm going to build something similar to this from acrylic (no 3D printer).  I plan to attach the air assist portion of my head to the mount instead the exposed lens.  I'll let you know how it works out.



[thingiverse.com - Adjustable lensholder with dual line pointer holder, air assist nozzle and focus leveling block for K40 Laser by DerZeitgeist](http://www.thingiverse.com/thing:1648481)


---
**HalfNormal** *November 18, 2016 16:43*

**+N Sorensen** I have been thinking about an adjustable lens too but could not resist this lab jack at this price. It is all an experiment to see what is an inexpensive way to get height adjustment. 


---
**Andy Shilling** *November 18, 2016 17:02*

OK I've taken the thread out completely but the bars do not fit in the slots properly to drop them down but with a piece of m8 stud it should be easy to adjust the height on this.



I've placed the thread in position for the photo so you can see where I want to put it. Just need to get a welder out to mount a nut to hold it. The red line is a small piece of plate and the black dot the captive nut. The thread will push against the bottom bar and force the lift up while remaining at the same level.





Hope that makes sense.

![images/7d58c60eaa42f0ba16a730fb2fe60378.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7d58c60eaa42f0ba16a730fb2fe60378.png)


---
**HalfNormal** *November 18, 2016 18:32*

I am real curious to see how it works. I did not think it would be an easy swap of parts but doable with a little elbow grease.


---
**Kelly S** *November 19, 2016 03:00*

**+Andy Shilling** That looks really good!  Curious to see where it ends for sure now.  



Also happy to see other people had the same thoughts to use these.  I thought about an adjustable lens, but after ordering the LightObjects setup I would rather leave it as is, as I just made a drag chain for it all as well lol.  So adjustable bed is the way I want to go.


---
**Andy Shilling** *November 19, 2016 12:46*

I've mounted it in the cabinet now photo to follow, I just need to get a new piece of stud and I should have it all working.



Cinema at the moment then on to hardware store.


---
**Kelly S** *November 20, 2016 01:13*

**+Andy Shilling** look forward to seeing it, and maybe a parts list of what it took to get it there.  Definitely going to have to order one for myself soon.


---
**Jim Bilodeau** *November 20, 2016 22:27*

Looking forward to see how this comes together in the K40! This is the last upgrade I plan to make - it would be very helpful to see how others get it to work well


---
**Kelly S** *November 20, 2016 23:29*

**+Jim Bilodeau** I am in the same boat as you :)  got all my other upgrades done or in the process so easier adjustable bed will be nice.  Currently have to adjust a bunch of bolts acting as stand offs and it gets old fast.  :p


---
**Andy Shilling** *November 20, 2016 23:32*

Yes sorry guys had a little problem today, if/when you do this make sure to take out the stepper ribbon first or you'll end up making repairs like this.

![images/95b0764a005a92fec3d1359a766dc352.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/95b0764a005a92fec3d1359a766dc352.jpeg)


---
**Kelly S** *November 20, 2016 23:37*

**+Andy Shilling** OH no, did you drill through it accidentally?


---
**Andy Shilling** *November 20, 2016 23:54*

Yes completely cut through 8 of the 12 tracks so had to repair it, I'll order up a new one when I can find out where to get them. 



I have drilled and tapped the hole now in the front of the cabinet but because of the time the repair took I didn't get chance to fit the stud.



Now that I've got this far I'll see it through but to be honest it's not the best idea, you don't get a lot of movement with it fitted like this. If you cut or engrave different thicknesses of wood etc it might be worth while but if you want anything over approximately 50mm ie a cup or box then forget it.



Once I've got the stud in place I'll post a short video to show what I mean.


---
**Kelly S** *November 20, 2016 23:58*

Highly look forward to it, I mostly do thin woods or acrylic so it would seem worthwhile for my needs.  Curious to see how you setup a bar in the front threaded for a longer rod.  


---
**giavonni palombo** *November 21, 2016 03:50*

Tried it. Dont work it wobbles so one side goes up a few mm higher then the other side does it. Dont wast the time.


---
**HalfNormal** *November 21, 2016 14:13*

**+giavonni palombo** was yours the 6 or 8 inch square?


---
**giavonni palombo** *November 21, 2016 15:59*

6X6. I did  make it so the screw did not move up or down. The problem was the tolerances are not that great. If I remember the difference from one side to the other was 4 or 7mm over the 10 inch width of my table.


---
**giavonni palombo** *November 21, 2016 16:10*

[thingiverse.com - Z Adjustable Bed for K40 Chinese Laser by brianvanh](http://www.thingiverse.com/thing:1906231)



I like this concept, simple.


---
**Kelly S** *November 21, 2016 17:04*

**+giavonni palombo**  seen this posted on Facebook yesterday and printed one.  Will be going to the hardware store later for the other bits.   Looking for a better belt system though, maybe chain to prevent laser melting rubber from reflections or anything. 


---
**HalfNormal** *November 21, 2016 17:30*

**+Kelly S** **+giavonni palombo** The concept looks interesting but the top of the threaded rods are not supported and if you tension them with a belt, they will bow in. Using the expanded metal can cause binding if that is what he is thinking will keep the rods from compressing in. Just my thoughts. Otherwise I think it is a great start.


---
**Kelly S** *November 21, 2016 18:35*

**+HalfNormal** the threaded rod it's self does not turn, and I think being through the grate will keep them upright.  In any case some thin aluminum stock as a frame around the top to keep them erect would be easy to make as well.  


---
**Andy Shilling** *November 21, 2016 19:03*

How do I attach a video to this post?


---
**HalfNormal** *November 21, 2016 19:55*

**+Andy Shilling** The best way to post video is to link from your gdrive or youtube.


---
**Andy Shilling** *November 21, 2016 19:56*

On it now thanks 


---
**Andy Shilling** *November 21, 2016 19:59*


{% include youtubePlayer.html id="L8D_ETrpWHA" %}
[https://www.youtube.com/watch?v=L8D_ETrpWHA](https://www.youtube.com/watch?v=L8D_ETrpWHA)



Here we go, finally managed to get it in and working, I still need to fix the bed to the lift but a rare earth magnet is working so far lol.



I will be making a perspex bracket to help keep the bar that is being push level but I am happy with the results at the moment. I've tapped one part of the chasis to take the thread of the stud but also fixed the nut on the outside with a spot of superglue.



If I change anything, I think I will make the stand more central which should also help with the level of the lift.


{% include youtubePlayer.html id="L8D_ETrpWHA" %}
[youtube.com - Attempt at adjustable laser need](https://www.youtube.com/watch?v=L8D_ETrpWHA)


---
**Kelly S** *November 21, 2016 20:33*

**+Andy Shilling** that looks great!  Come do mine.  :D


---
**Andy Shilling** *November 21, 2016 20:36*

Thank you but I'm really not sure it will stay very long, let's see if I can sort out the perspex bracket first.


---
**HalfNormal** *November 21, 2016 20:42*

Nice Job! It will be interesting to see how I get it to work.


---
**Kelly S** *November 21, 2016 23:43*

**+Andy Shilling** Would you mind showing a picture of how the bolt is attached to the push bar at the base?  I am debating over two systems but may default to this as it doe snot require me getting a belt, lol.


---
**Andy Shilling** *November 22, 2016 10:53*

I haven't made the bracket yet so I'm just using a nylok nut on the end to make it  a little wider and it is just pushing against the bar


---
**Kelly S** *November 24, 2016 01:23*


{% include youtubePlayer.html id="eaFhdqUQaiI" %}
[youtube.com - DIY laser cutter bed that is adjustable](https://youtu.be/eaFhdqUQaiI) off topic but got the this bed lift working with a ball chain.  :)


---
**HalfNormal** *November 24, 2016 17:03*

**+Kelly S** That is so sweet! Did you modify the adjustable nut to accommodate the ball chain?


---
**Andy Shilling** *November 24, 2016 17:14*

Well **+Kelly S**​ I suppose you think your Flash out doing my attempt at the adjustable bed lol. Well done that looks great I love the simplicity of the ball chain.






---
**Kelly S** *November 24, 2016 17:15*

**+HalfNormal** yes, I made a new adjustable nut with the same top as the original but setup for a ball chain.  What's nice is it works really really good regardless of the tightness of the chain, have the pully setup that there is always enough balls in the track to never slip, even when the connector goes around.  :)  if I have a chance later today will add my remix to thingiverse.  Expect my new aluminum  bed tomorrow  (48% open with holes, was a pretty good deal)  will get it installed.  And see how the movement is.  If it binds will make sleeves to go through the bed, just tall enough to not protrude too far and that will fix that issues as well.

![images/0726f92ff2deaceea9ab35b778423fe5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0726f92ff2deaceea9ab35b778423fe5.jpeg)


---
**Kelly S** *November 24, 2016 17:19*

**+Andy Shilling** no, not trying to outdo at all buddy.  :)  I started a new thread but posted it here as well, as there was previous interest in it.  And thanks for the compliment, I tried to keep all the parts easily sourced as I live in a tiny town, if I have access to them, everyone does lol.


---
**Andy Shilling** *November 24, 2016 17:22*

Did you say your uploading the files to thingiverse? If so could you post the link on here so I can send them off for printing please.


---
**Kelly S** *November 24, 2016 17:25*

I sure will **+Andy Shilling**, if I am lucky will be able to sneak off to the office at some point today.  ;)


---
**HalfNormal** *November 24, 2016 18:12*

**+Kelly S** I am with the family in CO for the holidays. I cannot wait to get back home to start printing the parts! I think I will add thrust bearings and washers on the adjustment nuts to smooth out the lift. Thanks for doing the hard work for us!


---
**Kelly S** *November 24, 2016 20:17*

**+HalfNormal** that's not a bad idea, I actually think just a washer alone would be fine.  Will definitely try that when I get my new plate tomorrow and cut to size.  The only spot I think it would bind is whatever sheet or plate is riding up and down the threads, potentially getting stuck on a thread.  I think the best solution would be a spacer between the threads and the bed and your idea of a washer.  


---
**Kelly S** *November 24, 2016 20:19*

I also ordered these to put in my plate to get less surface area under my projects as I mostly cut, should make air flow really good.

![images/e44491dc71981f6665b7e63bbdd54b48.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e44491dc71981f6665b7e63bbdd54b48.png)


---
**Kelly S** *November 25, 2016 03:35*

**+Andy Shilling** **+HalfNormal** here is the link to my thingiverse file [thingiverse.com - K40 manual Adjustable Bed with Ball Chain by AntTopia](http://www.thingiverse.com/thing:1915794)


---
**Kelly S** *November 25, 2016 21:19*

With aluminum plate installed, works great.

![images/b6fa6cffa46971f737145bd327747b04.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6fa6cffa46971f737145bd327747b04.jpeg)


---
**Andy Shilling** *November 25, 2016 21:49*

**+Kelly S** thank you very much I'll have a look in a bit.


---
**Andy Shilling** *November 29, 2016 22:50*

Thanks **+Kelly S** Just ordered my printed bits for the bed.  I think I need to sell enough laser cut bits to be able to buy my own 3d printer lol. I'm now just waiting on the C3D **+Ray Kholodovsky** ;)


---
**Kelly S** *November 29, 2016 23:14*

**+Andy Shilling** if you do not mind me asking, where did you order from and what was the cost?


---
**Andy Shilling** *November 29, 2016 23:18*

[3dhub.com](http://3dhub.com)  I just found a local printer and it works out about £10 plus £4 postage. The particular seller I chose offered ABS and PLA for the same price, others were a lot more even up to £50 mark.[3dhub.com - Products &#x7c; 3D&#x7c;Hub &#x7c; 3D Printing](http://3dhub.com)


---
**Kelly S** *November 29, 2016 23:34*

Thanks for the info, pondering renting my 3d printers services on the side.  :)


---
**Andy Shilling** *November 30, 2016 09:30*

Sorry I forgot to say this price is for a 400 micron layer height, it would be £50 for a 200 height.


---
**John Sturgess** *December 02, 2016 16:00*

**+Andy Shilling** Is that the correct URL? I'm in the UK and am looking for a printer for these at a reasonable price.


---
**Andy Shilling** *December 02, 2016 16:47*

**+John Sturgess** Yes I'm in Kent and the seller is in London I believe. I'll see if I can get his direct contact details if you like.


---
**Andy Shilling** *December 03, 2016 08:09*

[3dhubs.com - Steelman's Hub](https://www.3dhubs.com/leicester/hubs/steelman)


---
**Andy Shilling** *December 03, 2016 08:10*

**+John Sturgess** here is the link to the 3D hub I used, my prints have now been posted so I will upload a photo once they are here.


---
**John Sturgess** *December 03, 2016 09:23*

**+Andy Shilling** Cheers, I'll check it out


---
**John Sturgess** *December 05, 2016 14:04*

I found this and want to incorporate the bearings into my z table. I "need" a definitive x,y 0 as i engrave a wide range of depths and having a set 0,0 will make life a lot easier!

[plus.google.com - This is a proof of concept mock up for an adjustable height bed for my laser…](https://plus.google.com/111438403155069984294/posts/eNCk9QGn3qD)


---
**Andy Shilling** *December 05, 2016 14:10*

If you could print a 5th chain pulley I don't see why it couldn't be mounted to a stepper and used to drive the chain/table.


---
**Kelly S** *December 05, 2016 19:01*

**+Andy Shilling** That is a good idea, however after some use I have decided I will end up going a different route.  It does work really really well, however with the threaded rod poking up all the time, it cuts into usable room, easy to work around with smaller objects adjusting the offsets of the work, however I will need to eventually use the entire area inside, and will soon be looking into using the lab jack I think.  I will kep it around though lol.


---
**Andy Shilling** *December 05, 2016 19:13*

I've taken my lab jack out now because it only gives you about an inch of depth and I want to etch glass vases. Once I get my prints I will be able to set up your bed and work it out from there. 


---
**Kelly S** *December 05, 2016 19:32*

Definitely look forward to seeing if you improve it.  :) I was thinking about having the bolt come in through the bottom of the machine and have everything under it.  But it would require me to cut a hole in the desk it is on hahaha...  But it would allow for completely accessible bed area as the bolts would then be able to push up the plate as they go in and out.


---
**Andy Shilling** *December 07, 2016 23:05*

Look what turned up today. Had to go for a pretty colour.![images/2441a8ada6a7d777cd11c9850dbaed73.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2441a8ada6a7d777cd11c9850dbaed73.jpeg)


---
**Kelly S** *December 07, 2016 23:11*

I like that blue, I have that filament :) used it for my cable chain :)  look forward to seeing your outcome and if you can improve.

![images/2373db55818db3e97587cbf107dbb110.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2373db55818db3e97587cbf107dbb110.jpeg)


---
**Peter Roig** *December 18, 2016 08:14*

**+Andy Shilling** for your version with the lift, how did you modify the lift to work in that position? I am trying to figure it out in the clip but without one infront of my I cant xD




---
**Andy Shilling** *December 18, 2016 10:28*

Their is a photo of it above the video that may help but I replaced the thread with a new piece because the original has a left and right thread on it. I've taken mine out again now so I will take another photo for you in a while.


---
**Andy Shilling** *December 18, 2016 22:29*

As you can see I removed the thread section and that is it. Once it was bolted to the base all that was needed was a new threaded section to push against the bottom bar. Hope that helps.

![images/7df0474cd8e845a4c42fdf1284cd3a16.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7df0474cd8e845a4c42fdf1284cd3a16.jpeg)


---
**Kelly S** *December 18, 2016 22:34*

Hey **+Andy Shilling** how tall would you say that is completely flattened?


---
**Kelly S** *December 18, 2016 22:35*

Also, does it sway at all while extended?


---
**Andy Shilling** *December 18, 2016 22:39*

I'll pop out to the shed and check but yes it does sway a little. I've just stopped using it because of the movement.


---
**Andy Shilling** *December 18, 2016 22:50*

**+Kelly S**​ your looking at 2.25 inch when completely collapsed.

![images/d25d003cb864ce043750ac598fa88ce5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d25d003cb864ce043750ac598fa88ce5.jpeg)


---
**Kelly S** *December 18, 2016 23:54*

Ah, so a little movement then, that's no good.  Thanks for the quick reply!  Are you thinking of other alternatives?  Will be modding mine with a larger gantry sometime soon and still have to think about the Z lol.


---
**HalfNormal** *December 18, 2016 23:57*

**+Kelly S**​  The 8X8 one I have is pretty solid. I have yet to modify it. Will update when I do.


---
**Andy Shilling** *December 19, 2016 00:03*

**+Kelly S** I'll be going over to the chain lift very soon, I just wasn't happy with the lab jack idea. My biggest problem was I have started etching drinking glasses and using that jack means I can't drop it low enough to get the required focal length.


---
**HalfNormal** *December 19, 2016 00:08*

**+Andy Shilling**​ how much of that 2 inches do you think you're going to gain?


---
**Kelly S** *December 19, 2016 00:23*

**+HalfNormal** so very little sway?  I am thinking more of when the machine will be engraving.   Almost thought to cut a square in the bottom and mount the lift on the table.


---
**HalfNormal** *December 19, 2016 00:29*

So far the little testing I have done it works fine for what it is. Have not done a lot of precision work yet.


---
**Andy Shilling** *December 19, 2016 06:17*

+HalfNormal​ in bit sure what I'll gain tbh but I was doing some point glasses and then needed to be flat on the base because of the diameter of the glass. Having this mounted to the base just made this impossible. 


---
**HalfNormal** *March 16, 2017 01:40*

**+Andy Shilling** Kelly S files for printing the ball chain adjuster is missing from Thingiverse. If you still have them handy, could you email them to larrygon at gmail? Thanks!


---
**Andy Shilling** *March 16, 2017 05:33*

**+HalfNormal**​​ i will have a check for you when i get home from work but I too may have deleted it, I never got it to work properly using the ball chain even printing a 5th wheel for a tensioner. I would recommend using the 6mm wide 5mm pitch belt version as that works perfectly.


---
**HalfNormal** *March 16, 2017 12:25*

**+Andy Shilling**​ thanks for the info


---
**Kelly S** *March 16, 2017 14:47*

I can upload them again later today.   Mine is still groing strong.  :]


---
**Andy Shilling** *March 16, 2017 14:51*

**+Kelly S**​ I tried 3 or 4 different size balls but none of them would work, I think it was more to do with the pitch than anything else but the belt is working fine for me now. 


---
**Kelly S** *March 16, 2017 15:02*

Hmm, not sure I custom tailored it to the ball I had in hand.  So hard to tell if random ones would work, I cannot even get mine to slip without having to be forceful lol.  **+Andy Shilling**


---
**HalfNormal** *March 16, 2017 15:06*

**+Kelly S**​ Thanks for uploading the files again. Please let me know when they are available. 


---
**HalfNormal** *March 18, 2017 22:00*

**+Kelly S** Have you uploaded the files yet?


---
**Andy Shilling** *March 18, 2017 22:25*

**+HalfNormal** Here you go I found mine buddy.

[drive.google.com - Ballchain - Google Drive](https://drive.google.com/open?id=0B4PVxTUHzQtfYkh3QW1ZNGhIZm8)


---
**HalfNormal** *March 18, 2017 22:26*

**+Andy Shilling**​ a really BIG thank you!


---
**Andy Shilling** *March 18, 2017 22:33*

**+HalfNormal** your lucky, it was in my trash can. If I wasn't as lazy as I am I would have deleted it ;)


---
**Kelly S** *March 19, 2017 22:47*

Is that all of it?  I just now located the files,  Did a reloading of my PS after I made that and it was in a backup.  Can upload, or e-mail it to you if that isn't the full file.  :)   **+HalfNormal**


---
**HalfNormal** *March 19, 2017 22:50*

**+Kelly S** Andy's upload is the whole enchilada. In fact I am printing it right now. I appreciate you finding the files. I will let you know how it turns out for me. A big thanks for your hard work on designing this.


---
**Kelly S** *March 19, 2017 22:55*

No biggie, sorry it took me so long to locate lol.   I have ... a few hard drives in this computer and they are not very organized.  Should be good to go if you can find the proper size ball chain.   I still use that bed and it has been so handy.   Not mention in there is, bolt it to the bottom of the K40.  It will firm it up a ton.  

 


---
**HalfNormal** *March 19, 2017 23:00*

Funny you mention the ball chain size. I was at the local ACE and they sell it by the foot but it is expensive so will source it someplace cheaper. They have some that is larger than the "standard" size that I was thinking of using but then would have to redesign the adjusters to accommodate the larger balls. Will see how it turns out. You seem to have nailed it so I will not worry about it now.


---
**Andy Shilling** *March 19, 2017 23:03*

**+Kelly S**​ agreed with the bolting it down, just wish i could have found a ball chain that fitted it well enough, it would have saved me quite a few £ on different sizes of chain and then the printing of the new parts for the belt. 


---
**Kelly S** *March 19, 2017 23:22*

**+HalfNormal** I got that ball chain at an Ace.  Was not in the section where they sell it by the foot, but in the lighting section.  It is used to extend basement lights.  


---
**HalfNormal** *March 19, 2017 23:24*

**+Kelly S** All they had in the lighting section was packages of a couple feet of the chain. They did not carry the brand you obtained.


---
**Kelly S** *March 20, 2017 03:35*

Ah I had to get two packs (yes still works with the couplers in place).   And that stinks. 




---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/9GU3zXg2Noq) &mdash; content and formatting may not be reliable*
