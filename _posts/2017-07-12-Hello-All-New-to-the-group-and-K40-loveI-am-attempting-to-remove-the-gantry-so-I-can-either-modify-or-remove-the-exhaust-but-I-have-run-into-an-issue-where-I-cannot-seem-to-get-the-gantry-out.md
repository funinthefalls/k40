---
layout: post
title: "Hello All, New to the group and K40 love...I am attempting to remove the gantry so I can either modify or remove the exhaust but I have run into an issue where I cannot seem to get the gantry out"
date: July 12, 2017 16:54
category: "Discussion"
author: "Brian Ching"
---
Hello All, 

New to the group and K40 love...I am attempting to remove the gantry so I can either modify or remove the exhaust but I have run into an issue where I cannot seem to get the gantry out. It catches on the front lip of the case and there does not seem to be enough clearance to lift the rear of the gantry. See attached pic. I did a summary search but other than 'remove gantry' I did  not find anything along the lines I was looking for. Any help would be appreciated.

Thanks,

Brian Ching

![images/1f1a32c949b4ef45e44a87ccb9e3e949.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f1a32c949b4ef45e44a87ccb9e3e949.jpeg)



**"Brian Ching"**

---
---
**Madyn3D CNC, LLC** *July 12, 2017 18:00*

I didn't remove the gantry when I took out my exhaust. I just cut the whole exhaust out with a dremel cut-off wheel right from the back

![images/6846cdac13dd87abdf482898240df8b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6846cdac13dd87abdf482898240df8b2.jpeg)


---
**Madyn3D CNC, LLC** *July 12, 2017 18:04*

and there she is with all that extra cutting area. Good luck, deff one of the most practical and efficient hard mods IMO, aside from electronics. 

![images/d5beb988465b38df4aa453b7a7b24b6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d5beb988465b38df4aa453b7a7b24b6a.jpeg)


---
**Brian Ching** *July 12, 2017 18:04*

**+Madyn3D CNC, LLC** Thanks! That was my other thought...Looks to be about 1/4" larger opening should do the trick.


---
**Dennis Luinstra** *July 12, 2017 18:55*

From what I can remember you have to remove all the 4 bolts and slide the gantry to the back, then lift the front and it will slide over the front lip instead of sticking under it. 


---
**Ashley M. Kirchner [Norym]** *July 12, 2017 18:56*

You have to remove the exhaust port first, then the gantry will come out easier. On mine the exhaust port was bolted in on the back. Some folks have reported theirs being welded in unfortunately. If it's welded, you can use a Dremel to either cut the funnel inside the cabinet, above the gantry, or cut the welds on the back and remove it all together. I removed the whole stupid thing as it only sucks from the top of the material and with the air assist, I have smoke (and debris) being blown under the material, so the exhaust funnel wasn't working at all. Out it came.


---
**Nate Caine** *July 12, 2017 19:24*

I was able to remove mine, but it was far enough back that I don't remember the tricks.  (I was also able to work on the engine of my car, without cutting a hole in the hood, too.)



My machine is torn apart right now, but one thing I notice that you might give a try:



Remove the 4 screws that secure the exhaust port in place.



Put the gantry about half-way across front to rear of the machine.  Move the carriage to the far right to get it out of the way.



Standing at the left of the machine with you left hand in the exhaust port (which is floating), lift it up (perhaps a 1/2 inch).



This allows the "lip" of the <b>rear</b> I-beam of the X-Y table to clear the white exhaust port which you are suspending with your left fingers..



That means you can push the x-y table about 1/4-inch further towards the rear of the machine.  Hopefully that will get you the space you need to clear the lip at the <b>front</b> of the machine.



With your right hand at the front of the machine, lift the i-beam up (that is the problem in your photo).



In my case, there was now enough extra space to clear the front lip of the machine case.



Raising the x-y table about halfway up, required me to reposition the gantry slightly towards the rear of the machine.



The rest is carefully tipping the x-y table assembly to avoid the mirror hitting the left of the case.  



I recall this meant pulling the right portion of the table out first.



As you know, no two K40's are the same, and these troublesome dimensions might vary between our two machines.


---
**Ned Hill** *July 13, 2017 05:38*

To remove my exhaust vent I had to unmount the tube because the bolts for one of the mounts came down too far and there wasn't enough clearance to get vent over the back ledge.  See below for my post about it.



[https://plus.google.com/u/0/108257646900674223133/posts/TWpTcM9gQeY](https://plus.google.com/u/0/108257646900674223133/posts/TWpTcM9gQeY)


---
*Imported from [Google+](https://plus.google.com/108079990870910423382/posts/JaW6V9W6adk) &mdash; content and formatting may not be reliable*
