---
layout: post
title: "Anyone ever use Photo VCarve to laser etch a photo then import it into CorelLaser for etching?"
date: January 24, 2016 01:52
category: "Software"
author: "Joseph Midjette Sr"
---
Anyone ever use Photo VCarve to laser etch a photo then import it into CorelLaser for etching? Also, anyone ever hear tell of using Repetier, (which is what I use for my 3D printers), on a 40W laser printer with the stock mainboard?





**"Joseph Midjette Sr"**

---
---
**Anthony Bolgar** *January 24, 2016 05:18*

Repetier firmware will not run on the stock mainboard. Not sure about repetier host to send gcode to the board, but I highly doubt it would work, as the stock boards from the K40 lasers are proprietary. Sorry, I have never heard of Photo VCarve, but will see what I can learn about it and get back to you.


---
**Brooke Hedrick** *January 24, 2016 05:51*

I use the VCarve products for cnc work.  Specifically they are used to create the toolpaths/gcode that milling software translates to movement.



Photo VCarve specifically can take an image and translate it into the toolpaths a cnc mill can follow to engrave the photo into some solid material.


---
*Imported from [Google+](https://plus.google.com/104424492303580972567/posts/KR6H7WnJqK2) &mdash; content and formatting may not be reliable*
