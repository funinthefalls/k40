---
layout: post
title: "Im considering the smoothie update for my k40"
date: September 07, 2016 16:17
category: "Smoothieboard Modification"
author: "Bob Damato"
---
Im considering the smoothie update for my k40. I hear good and I hear bad, but overall it sounds like its pretty much worth it. I notice there is a 3x 4x and 5x. They all work with our lasers? And its just basically features that are different between the models? I like the features of the 5x but I have been checking the web site for quite a while now and they are always out of stock...





**"Bob Damato"**

---
---
**Ariel Yahni (UniKpty)** *September 07, 2016 16:24*

**+Bob Damato**​ ethernet on the 4x and 5x are indeed a great addition, specially as some software releases will use it more. Besides that I would take the 3x, it will give you 3 axis ( possible Z table)  but if you plan on having a rotary then minimum you'll need 4 axis


---
**Dan Stuettgen** *September 07, 2016 17:12*

The 3x is a 3 axis controller, the 4x is a 4 axis controller and the 5x is a 5 axis controller.  I purchased s 4x myself just in case I wanted to add a rotary table, but the 4x board seem to always be available wher the 5x are very hard to get. plus the 4x are cheaper .  It hs the Ethernet connector and runs the same software as the 5x.


---
**Bob Damato** *September 07, 2016 19:40*

Thanks guys, I appreciate it. I cant even think Id need a 5x so that makes the decision easy!




---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/XTHcxbdRKG9) &mdash; content and formatting may not be reliable*
