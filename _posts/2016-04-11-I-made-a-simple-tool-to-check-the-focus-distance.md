---
layout: post
title: "I made a simple tool to check the focus distance"
date: April 11, 2016 14:30
category: "Modification"
author: "Michael Ang"
---
I made a simple tool to check the focus distance. You might need to adjust the length of the leg that says "BED" but it's pretty easy to do. [http://www.thingiverse.com/thing:1482364](http://www.thingiverse.com/thing:1482364)





**"Michael Ang"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 16:00*

That's a great idea, but I would probably make it come off the carriage instead of the rail. But either way I suppose it would do the same job.


---
**Michael Ang** *April 11, 2016 16:35*

Yeah the reason I don't do it off the carriage is to avoid moving the carriage by hand. Using the rail I can just pull the rail by hand - pretty fast.


---
**HalfNormal** *April 12, 2016 01:25*

My forehead hurts from hitting it with the palm of my hand!


---
**Corey Renner** *June 13, 2016 02:45*

Is the focus distance the same with the oem head as it is with the LO air-assist head?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 13, 2016 03:11*

**+Corey Renner** The focal distance is determined by the lens. So, the distance will be based on whatever lens you have. If you have a 50.8mm focal lens, you will have to focus from the lens. I'm not certain if that is supposed to be from the base of the lens, or the centre, or the top, but I just focus mine based off the base of the lens. From what I understand, the LO head is a different shape/height than the oem head, so you would need to focus at a different point based on where the lens is possibly.


---
**Corey Renner** *June 13, 2016 05:36*

Got it.  Thanks, c


---
*Imported from [Google+](https://plus.google.com/101545054223690374040/posts/c54Zc7LcCQn) &mdash; content and formatting may not be reliable*
