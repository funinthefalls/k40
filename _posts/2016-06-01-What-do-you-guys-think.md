---
layout: post
title: "What do you guys think?"
date: June 01, 2016 02:02
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
What do you guys think?





**"Ariel Yahni (UniKpty)"**

---
---
**Don Kleinschnitz Jr.** *June 01, 2016 02:24*

Certainly a simple answer to the variable focus problem. 

I wonder:

-Should it have a guide that insures that it does not mis-align the lens as you move it up and down, making the beam non-parallel to the bed. Beam parallelism is hard to recognize and creates lower power and angled edge cuts.

-How do you create positive pressure to keep the lens clean using air assist.

-Can you add a laser indicator? If you do will the focus point of the pointer change as you raise and lower the lens assy.

-Using this would allow you to simply exchange lenses with differing focal lengths. Maybe put lenses in cartridges.

-Also easier to clean lenses


---
**Anthony Bolgar** *June 01, 2016 02:30*

I would personally not use this, for all the same reasons as **+Don Kleinschnitz** mentioned. I prefer the tube within a tube method to make the focus adjustable.


---
**Don Kleinschnitz Jr.** *June 01, 2016 02:53*

Someone on this or the FB forum was working on one that adjusts using a small DVD stepper and rack and pinon, that did solve some of these problems. I finally decided to use a movable table once I found out that "its not nice to mess with the optics" :).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 01, 2016 04:10*

I think **+HP Persson** has something similar to this in his laser?


---
**Stephane Buisson** *June 01, 2016 08:44*

my conviction is its better to adjust the object (30x20max) to the K40 focal lens than the opposite. if one day I do my own version of the K40, I would introduce the object from the front, raise it up to a ceiling (focal lens height), remove the ceiling board of the ray path (witch could have also another function, like becoming a top cover).

fixed focal design have also plenty of advantages, cheap, easy to match webcam image of the object(material) with virtual design...


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/dpDxtNyEgF7) &mdash; content and formatting may not be reliable*
