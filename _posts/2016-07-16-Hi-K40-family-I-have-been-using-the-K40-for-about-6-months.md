---
layout: post
title: "Hi K40 family. I have been using the K40 for about 6 months"
date: July 16, 2016 04:36
category: "Hardware and Laser settings"
author: "Anthony Santoro"
---
Hi K40 family. 

I have been using the K40 for about 6 months. After this time the laser lost its power so instead of replacing the tube I just bought a new machine. 

The new machine has an issue! I'm not surprised! 

When cutting a continuous line, the machine will start quite strong, but the laser strength will continually decline as the line is being cut. At the beginning of the line, the wood was cut through, by the end of a line the laser hadn't even marked the plywood. 

I'm thinking the power supply is dodgy, what are your thoughts? 

![images/516cd8726955c5c4a583fefda8701bae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/516cd8726955c5c4a583fefda8701bae.jpeg)



**"Anthony Santoro"**

---
---
**Anthony Santoro** *July 16, 2016 04:38*

Just a note, the laser is aligned perfectly. The issue isn't alignment. 


---
**Alex Krause** *July 16, 2016 04:42*

You did a four corners test? Also what is your water temp at the beginning of the cut and toward the end? As the water gets warmer the tube will lose power


---
**Ariel Yahni (UniKpty)** *July 16, 2016 04:44*

You say perfect alignment but is your focus the same all around the bed? 


---
**Alex Krause** *July 16, 2016 04:45*

**+Ariel Yahni**​ I noticed my bed to be several MM off from front to back and side to side


---
**Anthony Santoro** *July 16, 2016 04:46*

Hi mate, 

Yep, laser works well at all 4 corners. It is the long continuous cuts that are the issue, alignment and instantaneous power is okay. 

Water temp is constant at about 10 degrees. I was using the same cooling set up on my previous machine without drama. 


---
**Ariel Yahni (UniKpty)** *July 16, 2016 04:48*

1mm or less can affect the cut along the bed


---
**Anthony Santoro** *July 16, 2016 04:49*

I'm confident that it isn't the alignment or focus. I tested this by cutting a circle. If it was a focus/alignment issue then the beginning and end of the circle would be the same cut strength. Instead the start of the circle is strong, the end of the circle is not even marked on the wood. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 16, 2016 04:51*

Is your mA gauge dropping as it does the long continuous cuts? That would indicate power being an issue if so.


---
**Ariel Yahni (UniKpty)** *July 16, 2016 04:52*

Does the size matter when you  do those tests? 


---
**Anthony Santoro** *July 16, 2016 04:56*

mA meter is constant. It is 28mA through the entire cut. 

This is fairly high compared to my old machine. My old machine was 21mA max. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 16, 2016 04:58*

**+Anthony Santoro** I am pretty certain you shouldn't be running the power higher than 20mA. Could be that the problem is that you are running the power too high & it is unstable at that power level. Try 10mA & run the same cuts & see if it drops power towards the end of the cut.


---
**Ariel Yahni (UniKpty)** *July 16, 2016 05:02*

agree with **+Yuusuf Sallahuddin** What thickness its that ply? Why you need so much power for that cut?


---
**Anthony Santoro** *July 16, 2016 05:07*

On my old K40 I always had the power at max. I assumed that this machine would be exactly the same. I even purchased it from the same seller. 

I'll try to dial down the power to about 20mA to see if it can sustain the cut. I'm out of the house at the moment but will try soon. 

Just trying to cut 3mm ply. On the old machine it was cutting at max power at 11mm/s in a single pass when I first purchased it. After 6 months of heavy usage the tube lost so much power that it wouldn't even cut through after 3 passes of 6mm/s. Same plywood. 


---
**Ariel Yahni (UniKpty)** *July 16, 2016 05:09*

**+Anthony Santoro** trust me when i say you have an issue with aligment if you cannot cut 3mm ply at 10mA in one pass


---
**Anthony Santoro** *July 16, 2016 05:10*

At what speed? 


---
**Anthony Santoro** *July 16, 2016 05:13*

Maybe I'm missing something important in the alignment. 

I got all 4 corners hitting the exact same mark. I did a ramp test to find the perfect cutting height then cut 4 chocks to hold up the ply. 

(the original cutting platform was removed.)

What have I missed. 

By the way, thanks for your advice everybody. I really appreciate it! 


---
**Ariel Yahni (UniKpty)** *July 16, 2016 05:18*

You could be hitting the inside of the cone on exit, that will reduce power buy a lo.t Rotate the laser head a tiny bit left or right and do a test fire. You should see a difference


---
**Anthony Santoro** *July 16, 2016 05:20*

Thanks mate, will try. 


---
**Anthony Santoro** *July 16, 2016 05:21*

Are you in Melbourne? 


---
**Ariel Yahni (UniKpty)** *July 16, 2016 05:23*

Nope, im a cross the world from you. Panama


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 16, 2016 05:24*

Alternatively the lens could be upside down too. That would cause difficulty in cutting through anything. I'm in Gold Coast, Ariel is overseas somewhere.


---
**Anthony Santoro** *July 16, 2016 05:30*

Just for confirmation, which way should the lens be? I think I've had it right for the last 6 months, but just want confirmation. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 16, 2016 05:40*

The lens should be with the convex (bump) facing upwards & either flat/convex (bowl) downwards.


---
**Alex Krause** *July 16, 2016 05:42*

If I remember correctly bump goes up


---
**Alex Krause** *July 16, 2016 05:44*

Are all your mirrors clean and your lens?


---
**Stephane Buisson** *July 16, 2016 08:36*

for the instability, my bet would be the gaz in the tube is braking down. after some rest it restabilize (a bit less each time) -> your power is too high, and I don't think it's the PSU.

The cut result is an other matter and could be impacted by ray path. (mirrors/lens quality,  alignement/clean)


---
**Anthony Santoro** *July 16, 2016 08:39*

Perfect. That's for your advice everybody. 


---
**Jim Hatch** *July 16, 2016 13:27*

+1 **+Stephane Buisson** 



I agree with Stephanie. The power is too high. For a 30W tube (that's what these are - you're overpowering to reach 40W) running at more than 90% power is going to shorten the tube life. You shouldn't run these more than about 18-20 ma for very long or you're running it at over 100% of the power that it's designed for. That works, but at the expense of the tube. Might be a reason your first tube died on you. 28ma is more in the 60W (real) tube power level for a CO2 laser. I never run any of my lasers at more than 95% power to help maintain good tube life.


---
**Anthony Santoro** *July 16, 2016 13:49*

Thanks for the advice Jim! 


---
**Anthony Santoro** *July 20, 2016 08:22*

Hi team, 

Just an update to this machine state. I have tried to realign the machine, didn't fix the issue. Turning the power down to 20mA doesn't fix the issue, it still runs out of power before the end of a line. 

The other thing I'm finding is that when I do a short cut, if I repeat the same cut in the same spot at the same speed and power, the cut depth is different. 

Very strange. 

Here is a video of the laser losing its puff, you can see that by the end of the line there is no cutting, just marking the material. 



Let me know your thoughts. 



[https://www.dropbox.com/s/s4aule1t97oh5is/MOV_2512.mp4?dl=0](https://www.dropbox.com/s/s4aule1t97oh5is/MOV_2512.mp4?dl=0)




---
**Stephane Buisson** *July 20, 2016 11:25*

20mA is still too much, for some tube the max is more 15/18mA, and your could be already weaken so your new max could be 12 or less.

Some tips to find out: 

- find out for any inscription on the tube to find specs.

- short tube length(=<700mm) is more likely to be 30 or 35w than 40. (so max mA is less)


---
**Anthony Santoro** *July 20, 2016 11:37*

The plate badge on the tube says peak power 40w, 'agreement power' 37w.

I'm not too sure what they mean by 'agreement power'. 

Would 18mA be better? 


---
**Stephane Buisson** *July 20, 2016 11:49*

any brand ? tube length?

you will find some post here where some buyer found a 35W label under a 40W one.



May I suggest you do the opposite, make your same line at low power, again and again increasing power (10, 12, 14, 16), and look if your loss of efficency still happen.

You will learn about your tube.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 20, 2016 18:29*

I agree with Stephane. Start with low power (10 or less mA) to test marking & see if it dies off by the end of the line. If it marks fine at 10mA, increase to 12/14/16/18 & see at what point does the power issue reoccur.


---
**David Cook** *July 20, 2016 19:35*

My tube is maxing out at 13ma when I press the test fire button on the PSU  :-(   time for a new one.

**+Stephane Buisson**  would you be able to email me your smoothie config file ?  I'd like to compare it to mine.  I don;t think my power control PWM is working properly.  


---
**Anthony Santoro** *July 22, 2016 13:34*

Now the new machine isn't even firing! Everything is powered on, laser switch active, I press the test fire button or run a cut and there is no laser fired at all. 0mA, no matter what I try! 

I am going to try to get some kind of refund from the seller, poor quality machine! My first K40 machine had none of these issues. 


---
*Imported from [Google+](https://plus.google.com/106649411786258336023/posts/QfgpVJrg7Kh) &mdash; content and formatting may not be reliable*
