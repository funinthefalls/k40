---
layout: post
title: "Airbrush Tanning Kit things. I got them for $6 each from local auction place (they work too)"
date: November 30, 2015 07:26
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Airbrush Tanning Kit things. I got them for $6 each from local auction place (they work too). I wanted them for the air pump things, thinking maybe I can use them for the air-assist on the K40. Not sure if the pressure is enough, however it provides a nice constant stream of air though & is reasonably quiet.



The pump section reckons it is 12V 8Amp. I wonder is it possible to run this off the power supply that comes with the K40 or whether I need a secondary power supply?



![images/e0633bfc52a2bf3ed80024a978155c1c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e0633bfc52a2bf3ed80024a978155c1c.jpeg)
![images/ad03c0b21cc01444be443d0b967b8f5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad03c0b21cc01444be443d0b967b8f5b.jpeg)
![images/a3d06aa48c0a9ba4197536ec6582d688.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3d06aa48c0a9ba4197536ec6582d688.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Phillip Conroy** *November 30, 2015 07:58*

Do not hook up to k40 pwr supply unless you want smoke to escape,8 amps extra is a lot more than it can handle,try bang good web sit for a 12 volt 10amp power supply


---
**Phillip Conroy** *November 30, 2015 08:04*

[http://www.banggood.com/Wholesale-DC-12V-15A-180W-Switch-Power-Supply-Driver-For-LED-Strip-220V110V-p-50523.html](http://www.banggood.com/Wholesale-DC-12V-15A-180W-Switch-Power-Supply-Driver-For-LED-Strip-220V110V-p-50523.html)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 30, 2015 08:04*

**+Phillip Conroy** Thanks Phillip. Will take that into account. Will just run off the 240V supply for the time being & if it does the job for air-assist, will get an extra power supply for it later :)


---
**Gary McKinnon** *November 30, 2015 10:03*

I've read of a couple of guys on forums who used airbrush pumps with good results.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/jhCVX3SWuQh) &mdash; content and formatting may not be reliable*
