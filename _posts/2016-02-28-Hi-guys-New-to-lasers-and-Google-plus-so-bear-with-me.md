---
layout: post
title: "Hi guys, New to lasers and Google plus so bear with me"
date: February 28, 2016 22:25
category: "Software"
author: "Amanda Fisher"
---
Hi guys, New to lasers and Google plus so bear with me.

I have a stock k40 running coral laser and Corel draw, what other software can I use? Can I use illustrator with the laser works plugin or do I need a new board for that?





**"Amanda Fisher"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 29, 2016 09:12*

I use Illustrator to draw my files, save as AI9 format (as anything higher will not import), then I import it into CorelDraw with the Corel Laser plugin & engrave/cut from there. As far as I am aware, it isn't possible to go straight from Illustrator with the stock board. Maybe a new board would allow that, but for the couple of minutes it takes to save as AI9 & import to Corel, I imagine the expense isn't really worth it.


---
**Stephane Buisson** *February 29, 2016 12:09*

I didn't do it myself (not a AI user), but you have a plugin Illustrator to Visicut. (need board upgrade)


---
**Amanda Fisher** *February 29, 2016 12:36*

Thanks. I am currently working in ai then transferring but it would be nice to do everything from ai. Ordered a new lens and air assist so new board is a long way down the line.


---
*Imported from [Google+](https://plus.google.com/109538003670125385590/posts/QmzoPc9w8sY) &mdash; content and formatting may not be reliable*
