---
layout: post
title: "What mistakes am I about to make?"
date: January 22, 2017 03:46
category: "Smoothieboard Modification"
author: "Kris Backenstose"
---
What mistakes am I about to make?





**"Kris Backenstose"**

---
---
**Don Kleinschnitz Jr.** *January 22, 2017 04:38*

check out the diagrams and schematics here: 

[donsthings.blogspot.com - A Total K40-S Conversion Reference](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Don Kleinschnitz Jr.** *January 22, 2017 04:42*

As a star ......You have 24V on the LPS pin L ....?? If I am reading this right that will blow up the supply?


---
**Kris Backenstose** *January 22, 2017 04:52*

Woah thanks. Anything else?




---
**Don Kleinschnitz Jr.** *January 22, 2017 14:21*

Is this a Cohesion 3D board? Not familiar with the edge connector nomenclature. 

I don't see a PWM signal from the smoothie to the "L" pin or anywhere?




---
**Kris Backenstose** *January 22, 2017 19:55*

Its just a nanoM2 board I am replacing. I used your diagram to make mine. I am really frustrated because I spent hours trying to make sure everything was wired correctly. I can't get the opto sensors to work. I have continuity to the breakout on the other end of the ribbon cable in the k40 for all the pins but the smoothie just doesn't see it.


---
**Don Kleinschnitz Jr.** *January 22, 2017 20:15*

**+Kris Backenstose**  have you looked at this:

[donsthings.blogspot.com - K40 Optical Endstops](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)



You have to make sure that 5V is going to the endstops correctly and they are on the right smoothie pins.



What smoothie board are you using? 


---
**Kris Backenstose** *January 22, 2017 21:18*

smoothie 1.0b I just measured the pins and YL and XL have 5v all the time.


---
**Don Kleinschnitz Jr.** *January 23, 2017 00:05*

**+Kris Backenstose** 

I do not know what a smoothie 1.0B is? Can you post picture.



Your FTC connector is not the same pin out as my schematic above.



There is no Yendstop on the last pin (12). The Y endstop is over next to the X Home on pin 9.



The 5V that I am referring to is on pin 10.



If the X and Y enstops are reading 5 V even when the sensor is interrupted (slid to each extreeme) then ... well that is weird.



I also have no sense for physically where the end stops are wired to the smoothie board.


---
**Kris Backenstose** *January 23, 2017 10:27*

oh wow this wiring is totally wrong for YL,XL,B-,B+,A-,A+,5V,


---
**Kris Backenstose** *January 23, 2017 10:28*

I had it transposed. Didn't realize the ribbon is conductive only on one side.


---
**Don Kleinschnitz Jr.** *January 23, 2017 11:02*

**+Kris Backenstose** 



This is the obligatory "I told you so" message 



from the blog: [donsthings.blogspot.com - K40-S Middleman Board Interconnect](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html)



"Note: the pins on this connector are reversed from the other end due to the nature of the FFC connector and single sided cable, its complex to describe, hope this picture helps."



I did the same thing :) :(


---
**Kris Backenstose** *January 23, 2017 12:21*

LOL


---
**Kris Backenstose** *January 24, 2017 09:41*

Finally got motion working but the X stepper was having a seizure. Finally figured out it was a bad molex connector I made.



Soldered in a new Y photo sensor only to find out it still won't work. AARRrrrrhghghhg


---
**Don Kleinschnitz Jr.** *January 24, 2017 13:48*

**+Kris Backenstose** why a new Ysensor?


---
**Joe Alexander** *January 25, 2017 09:51*

I replaced the opto sensor with a mechanical switch right away, always works for me without fail and an easy wire as it is just an interruptor.


---
*Imported from [Google+](https://plus.google.com/113059362277072411228/posts/1P8TYk27Jon) &mdash; content and formatting may not be reliable*
