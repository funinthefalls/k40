---
layout: post
title: "So it looks like i need to install a new tube?"
date: October 17, 2015 07:39
category: "Discussion"
author: "george ellison"
---
So it looks like i need to install a new tube? Is this easy to do and where are the best/cheapest ones? Thanks





**"george ellison"**

---
---
**Phillip Conroy** *October 17, 2015 08:37*

Easy to change,just 2 wires,just make sure lots of sirlastic on terminals,i forgot to turn water on after i rearanged computer and other stuff with laser cutter,and the glass shatered on small pipe laser output end,i also blow the power suppy at the same time.

How do you know the tube has failed?


---
**george ellison** *October 17, 2015 12:03*

sparking from the left end of the tube, checked the connection but its fine. A brown colour has also appeared. Read on another forum that this is the sign of a damaged tube, if it is arcing with the case like mine is. 


---
**Joey Fitzpatrick** *October 17, 2015 15:12*

Tube is fairly easy to change.  When connecting your HV wires to the new tube, Do Not Solder the connections!  The heat from the soldering process can damage the seal at the Tube itself.  Wrap the wiring tightly around the metal post and seal it with a piece of tubing filled with silicone.  

 


---
**Gee Willikers** *October 17, 2015 16:05*

Know what you're buying.  A standard 40w tube will not fit a K40 based on length. K40s tubes are actually 35watts+/-. This one I have bookmarked as recommended on another forum: [http://pages.ebay.com/link/?nav=item.view&id=141438136578&alt=web](http://pages.ebay.com/link/?nav=item.view&id=141438136578&alt=web) ﻿


---
**george ellison** *October 18, 2015 07:43*

Right, didn't know that. Is this one correct? [http://www.ebay.co.uk/itm/40W-CO2-Laser-Tube-Engraver-cutter-Water-Cool-700mm-Shenhui-K40-/321882425353?hash=item4af1b00809:g:zLwAAMXQlgtRuGGK](http://www.ebay.co.uk/itm/40W-CO2-Laser-Tube-Engraver-cutter-Water-Cool-700mm-Shenhui-K40-/321882425353?hash=item4af1b00809:g:zLwAAMXQlgtRuGGK)


---
**Gee Willikers** *October 18, 2015 07:56*

Honestly I couldn't tell you for sure as I've never encountered the issue myself but it look like it should.



If your budget allows you can also get a larger tube and power supply, and cut a hole on the side of the machine to accommodate it.


---
**george ellison** *October 18, 2015 08:02*

Do you know the actual length of the k40 tube, that would help me find a correct replacement. I'm guessing that if it says k40 in the title and its from china its likely to fit.


---
**Gee Willikers** *October 18, 2015 08:03*

Hang on I'll measure mine.


---
**Gee Willikers** *October 18, 2015 08:18*

About 710mm


---
**george ellison** *October 18, 2015 19:13*

Sounds about right, "40W CO2 Laser Tube Engraver cutter Water Cool 700mm Shenhui K40" Thats the title on th ebay listing


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/iLpStBuYJvj) &mdash; content and formatting may not be reliable*
