---
layout: post
title: "Has anyone hooked up an indicator light to the laser on/off button?"
date: May 01, 2016 16:42
category: "Modification"
author: "mcpojack"
---
Has anyone hooked up an indicator light to the laser on/off button? I would like to know when it is energized. Thanks.





**"mcpojack"**

---
---
**Don Kleinschnitz Jr.** *May 01, 2016 19:57*

No but I don't think it is hard to do depending on the K40 config you have.


---
**mcpojack** *May 01, 2016 20:39*

I have the el cheepo eBay $369 model. Two pushbuttons, one large power rocker switch, power adjustment pot and a meter. Thanks for the reply Don.


---
**Don Kleinschnitz Jr.** *May 01, 2016 21:18*

I posted a schematic in this community for my K40 (search for K40 schematic)  you can reference it and come up with a light scheme. 

Let me know if you need help. 

If I recall the "laser switch" provides a ground to enable the supply. I don't know what the actual LPS input circuit looks like but I assume its a high impedance. I would guess that a LED could be used connected through a resistor to one side of the switch (not the ground side) and it would light when the switch is grounded. How are the pins on your LPS labeled?




---
**mcpojack** *May 01, 2016 22:01*

Thanks. I will take a look and see what I can come up with. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 23:03*

That's a good idea. Would be useful to know when it is energised as you said.


---
**Don Kleinschnitz Jr.** *May 01, 2016 23:53*

I was also thinking it could be a blinker.

This would get your attention lol

[https://www.adafruit.com/products/1190](https://www.adafruit.com/products/1190)



or more practically

[https://www.adafruit.com/products/680](https://www.adafruit.com/products/680)



but this would let the whole neighborhood know :)

[http://www.amazon.com/Intensity-Revolving-Emergency-Magnetic-Rotating/dp/B00H4M3Q8U/ref=sr_1_1?ie=UTF8&qid=1462146779&sr=8-1&keywords=rotating+led+emergency+light](http://www.amazon.com/Intensity-Revolving-Emergency-Magnetic-Rotating/dp/B00H4M3Q8U/ref=sr_1_1?ie=UTF8&qid=1462146779&sr=8-1&keywords=rotating+led+emergency+light)




---
*Imported from [Google+](https://plus.google.com/116692008627768105608/posts/DD9YHY4jgGT) &mdash; content and formatting may not be reliable*
