---
layout: post
title: "This doesn't belong here, however I know that the community owner and moderators are also in this community"
date: December 04, 2016 09:35
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
This doesn't belong here, however I know that the community owner and moderators are also in this community. So here goes: evidently my posts to the LW community are being deleted, and I'd like to understand why. While I believe I posted something two nights ago, I didn't actually verify. However, I posted again a few hours ago, I watched it get posted, and I even commented on my own post. Now I go back and it has disappeared. This leads me to believe that my previous post was also removed then. Both of them, without as much as an explanation why.



All I posted was my experience installing LW and the issues and solutions. I can't imagine having pissed someone off with that, someone who has the ability to remove my post I might say too.



I'd like to believe maybe G+ is acting up, however that is the only community that my posts are disappearing from. At the very least, I deserve an explanation why they're getting deleted, or removed, or hidden, or whatever is happening with them.





**"Ashley M. Kirchner [Norym]"**

---
---
**Alex Krause** *December 04, 2016 09:50*

I can double check in the moderation folder of laser web... more than likely some flagged the post on accident I will look into it


---
**Alex Krause** *December 04, 2016 09:58*

Released the posts... it looks as if the the anti spam counter measures done an a few other groups I'm in have finally trickled to the laser web community... it looks as if in order to prevent the spread of pornographic spam that is plaguing many popular groups that some posts are being flagged by members who newly join a community 


---
**Stephane Buisson** *December 04, 2016 10:04*

**+Ashley M. Kirchner** I can read your 3h post from the G+ main stream, but agreed it doesn't show up in LW.

maybe a Google snag, I am surely not on top of everything.

in K40 community sometime i see some post in the spam box with comments by other on it, strange very strange...


---
**Kelly S** *December 04, 2016 17:26*

I also noticed some posts in other areas being flagged if new users post, it will await approval.   


---
**Ashley M. Kirchner [Norym]** *December 04, 2016 17:31*

Guess it's time to take G+ out back and give it a good flogging. Thanks **+Alex Krause**​. 


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/4R7Fq98tvts) &mdash; content and formatting may not be reliable*
