---
layout: post
title: "Nice project, learned a lot from it"
date: June 14, 2016 03:51
category: "Modification"
author: "Ariel Yahni (UniKpty)"
---
Nice project, learned a lot from it.  Now I need some black magic to piece together that belt. 

![images/400c17a375a8cad27cd6d22996fec061.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/400c17a375a8cad27cd6d22996fec061.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 04:07*

I like the way the bolts go through & the nuts lock into the vertical plastic part. That's ingenious.



How far does the belt travel? Is there a place where you can join the belt where it will not move onto any of your cog teeth?


---
**Alex Krause** *June 14, 2016 04:12*

Holy Smokes Batman that's beautiful was it all cut out of acrylic? How many pieces did you actually have to buy


---
**Ariel Yahni (UniKpty)** *June 14, 2016 04:12*

Not my design, but lots of cleververss by the author. I could go about 10cm before the belt go's to the stepper.


---
**Ariel Yahni (UniKpty)** *June 14, 2016 04:15*

All acrylic. I hade everything else. Bearings, screws, still missing some rubber bands but I'm sure my daughters will help on that. 


---
**Alex Krause** *June 14, 2016 04:15*

Did you 3d print the gears? Or laser them?


---
**Ariel Yahni (UniKpty)** *June 14, 2016 04:16*

Here's the link to the files Laser Rotary Attachment found on #Thingiverse [http://www.thingiverse.com/thing:1359528](http://www.thingiverse.com/thing:1359528)


---
**Ariel Yahni (UniKpty)** *June 14, 2016 04:17*

There are some errors in the drawings and also you need to make sure you cut the holes of the screws based on the size you have/buy


---
**Ariel Yahni (UniKpty)** *June 14, 2016 04:17*

**+Alex Krause**​ all laser


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 04:35*

Thanks for sharing the file **+Ariel Yahni**. I think I will have to have a go at this in the near future too, as I have a few things I bought to laser that require a rotary.


---
**Alex Krause** *June 14, 2016 04:41*

**+Ariel Yahni**​ 1/4" acrylic?


---
**Ariel Yahni (UniKpty)** *June 14, 2016 11:23*

Mostly 3mm


---
**Scott Marshall** *June 14, 2016 19:45*

That size belt is available from stock, if you don't want to splice that is.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/cXEhwgXsRcH) &mdash; content and formatting may not be reliable*
