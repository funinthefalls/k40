---
layout: post
title: "Calibrating machine using RDworks... Very interesting. I'll be trying this out and seeing if it aids in accuracy"
date: January 30, 2017 20:29
category: "Hardware and Laser settings"
author: "Nigel Conroy"
---
Calibrating machine using RDworks... 

Very interesting. I'll be trying this out and seeing if it aids in accuracy. 

Hopefully it wasn't me that was making the mistakes!!!



But that's more likely.....





**"Nigel Conroy"**

---
---
**Nigel Conroy** *January 30, 2017 21:12*

Well I couldn't wait to do this and over 300mm on the x axis it measured 302 and 250 on the Y came out at 251.75



Followed directions in video and recut and the measurements were spot on.... 



Not a massive thing but every little helps.


---
**Austin Baker** *February 01, 2017 07:14*

I think from a tolerance standpoint, 2mm X and 1.75mm Y are significant! This RDworks software will write values to the stock K40 controller board, or are you calibrating a different controller? Nice job!


---
**Nigel Conroy** *February 01, 2017 13:45*

Agree that over 300mm that's a significant error. 



This is to the stock controller on a K50 which I assume is the same or similar to a stock K40


---
**laurence champagne** *February 06, 2017 19:01*

RDworks won't work on the type of controllers that come stock with k40's.



I love my K40. Like I love this thing but the only problem is not being able to adjust the step length on the board. Mine is off and I have to adjust things in my files to get it to engrave correctly which is annoying and will be a disaster when I get a new board.





The M2 nano does everything I ask of it and I don't want to upgrade but am going to have to just so I can adjust one little setting.



There has to be a way to access the setting on these boards but it's going to be almost impossible with the chinese support.


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/jVt73LMqisZ) &mdash; content and formatting may not be reliable*
