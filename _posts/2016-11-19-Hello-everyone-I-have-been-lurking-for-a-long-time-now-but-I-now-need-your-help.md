---
layout: post
title: "Hello everyone, I have been lurking for a long time now but I now need your help"
date: November 19, 2016 11:56
category: "Materials and settings"
author: "Claude Jounot"
---
Hello everyone,



I have been lurking for a long time now but I now need your help.



So I have a K40 machine with a Smoothieboard mod (K40 middleman board + logic level converter), it has been working well for almost six months, cutting through 5mm plywood at 2.7mm/s 80%, but now I can't cut through it even at 100% and 1mm/s...



I've checked the mirror and they are perfectly aligned, I've cleaned them too, with no luck.



Laser is working but I don't have a power probe so I can't check the real power.



I have tried a calibration pattern (cutting squares at speed from 1mm/s to 10mm/s and power from 10% to 100%) but none cut through the wood.



What can I do to find the problem ?



Thank you





**"Claude Jounot"**

---
---
**Phillip Conroy** *November 19, 2016 12:10*

mirrors and focal lenes wear out without any noticeable damage showing,measure cut width 


---
**Phillip Conroy** *November 19, 2016 12:10*

Where in the world are you located?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 19, 2016 13:05*

Check focal point has not shifted too. Do a ramp test. Lens orientation correct after cleaning?


---
**Stephane Buisson** *November 19, 2016 13:58*

lasertube wear too. it's a good chance if your optic is ok, your electronic should be ok, but the gaz in your tube.

full power reduce tube life exponentially.


---
*Imported from [Google+](https://plus.google.com/108967318011878521385/posts/73CcBj6VNWx) &mdash; content and formatting may not be reliable*
