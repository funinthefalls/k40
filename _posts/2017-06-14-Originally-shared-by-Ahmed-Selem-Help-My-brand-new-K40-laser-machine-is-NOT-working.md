---
layout: post
title: "Originally shared by Ahmed Selem Help! My brand new K40 laser machine is NOT working!!"
date: June 14, 2017 16:03
category: "Original software and hardware issues"
author: "Ahmed Selem"
---
<b>Originally shared by Ahmed Selem</b>



Help! My brand new K40 laser machine is NOT working!! First time I switched it on, the on/off switch flashed and wouldn't turn on so I changed the fuse which was blown. The on/off switch is now working fine but the power supply is not so the laser head never moved. I have checked the voltage switch on the side of the power supply to make sure it's on 230V (since I am in the UK). Apart from this, I don't know why it is not working. I would really appreciate your help. Thanks



![images/f59c0252e3306caada4f2513d34f6153.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f59c0252e3306caada4f2513d34f6153.jpeg)
![images/136ee59f7be143af64e74e9dcf3a9671.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/136ee59f7be143af64e74e9dcf3a9671.jpeg)
![images/821a2e9a958550e2ced4c4d00e2d8f2d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/821a2e9a958550e2ced4c4d00e2d8f2d.jpeg)
![images/556cf9ec4f5341e4378fc5c1dadd0529.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/556cf9ec4f5341e4378fc5c1dadd0529.jpeg)
![images/37c6b072ecfc17e4a90341856b8e2e92.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/37c6b072ecfc17e4a90341856b8e2e92.jpeg)
![images/dab409a8741a13ed265eef396dd2e8cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dab409a8741a13ed265eef396dd2e8cd.jpeg)

**"Ahmed Selem"**

---


---
*Imported from [Google+](https://plus.google.com/101837846316328889812/posts/fsimfVPaBSi) &mdash; content and formatting may not be reliable*
