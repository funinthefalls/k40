---
layout: post
title: "Can anyone recommend a good supplier for the ~45mm sized 20ma or 30ma ammeters?"
date: March 15, 2018 22:45
category: "Discussion"
author: "Jake B"
---
Can anyone recommend a good supplier for the ~45mm sized 20ma or 30ma ammeters?  



I have a ~65mm one that's dead on accurate. I ordered a 45mm one from eBay China and its junk-- it doesn't zero and the scale seems completely wrong.



I like the smaller sized one, because it fits better in the panel... though I could probably use my existing larger one....   I just don't want to waste my time ordering another dud.





**"Jake B"**

---
---
**Don Kleinschnitz Jr.** *March 17, 2018 00:05*

The meter in this post is bigger than the stock but most conversions are using it.

Can't vouch for the quality but the one on my test station is holding up.



[donsthings.blogspot.com - Adding an Analog Milliamp Meter to a K40](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html)


---
*Imported from [Google+](https://plus.google.com/101176403058148207601/posts/RTH8evyAPLS) &mdash; content and formatting may not be reliable*
