---
layout: post
title: "Good Day! Learned today to clean lenses every now and then"
date: October 26, 2016 23:27
category: "Original software and hardware issues"
author: "Kris Sturgess"
---
Good Day!



Learned today to clean lenses every now and then. Amazing how much power you recover. Lol Also had the head apart for the first time.



Can some identify what this white thing is inline with my water feed?



I traced the wiring back and it is in series with my door interlock. Flow meter possibly?



I thought maybe water temp for my gauge on the panel but I found the probe just dangling in the case.



Kris

![images/a2fa011d719f30482a1863c0b9733e56.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a2fa011d719f30482a1863c0b9733e56.jpeg)



**"Kris Sturgess"**

---
---
**Anthony Bolgar** *October 26, 2016 23:30*

Looks like a flow sensor to me. That is why it would be in series with the interlock switch....no flow = no laser in this situation. Can save you the price of a tube if you ever forget to turn on the cooling pump, or it fails part way through a job.


---
**Kris Sturgess** *October 26, 2016 23:31*

Any safe way to test it?


---
**Anthony Bolgar** *October 26, 2016 23:33*

Turn on everything but the cooling pump. Try to run a job, if the laser will not fire the flow sensor is working. If the laser fires, then the sensor is broken or wired incorrectly.


---
**Anthony Bolgar** *October 26, 2016 23:34*

If it does fire without the cooling pump, shut it down right away so you do not damage the tube.


---
**Kris Sturgess** *October 26, 2016 23:49*

Yup flow sensor and it works. No mention of it in the ad when I bought this thing. Good to know. I will re-do the wiring for it as it is not quite up to quality.



Now to re-do the water temp gauge that's doing nothing.... lol...



Might look for a dual readout style. Monitor In/Out. Or figure out a Aurduino type setup/display.



Oh the fun of tinkering.. 



Thanks for your help!




---
**Ashley M. Kirchner [Norym]** *October 27, 2016 01:13*

I've always admired the Chinese total lack of safety ... this picture shows it perfectly too. Water near electrical outlets ... nothing could ever possibly go wrong there. :)


---
**Anthony Bolgar** *October 27, 2016 01:13*

Have a look at [https://github.com/funinthefalls/LaserSafetySystem](https://github.com/funinthefalls/LaserSafetySystem) It was developed with the majority of the work done by **+Jon Bruno** 


---
**Anthony Bolgar** *October 27, 2016 01:15*

Ya, I moved all the accessory outlets to a separate ground box attached to the case. Did not like the whole water/electricity thing they have going on.


---
**Kris Sturgess** *October 27, 2016 01:25*

Ya. Seems a little sketchy. High voltage and water so close.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/1g8qqGbY8SC) &mdash; content and formatting may not be reliable*
