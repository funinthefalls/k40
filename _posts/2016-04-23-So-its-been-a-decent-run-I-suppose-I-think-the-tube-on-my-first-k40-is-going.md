---
layout: post
title: "So it's been a decent run I suppose, I think the tube on my first k40 is going"
date: April 23, 2016 11:43
category: "Hardware and Laser settings"
author: "I Laser"
---
So it's been a decent run I suppose, I think the tube on my first k40 is going. I've cleaned and realigned, checked focal length, but it's just not cutting like it use to. 



Any suggestion on how to tell, I'm guess a meter like **+Scott Thorne** has is the only real way to tell? The MA meter is still responding as expected, I cut some new mirrors today and whacked them in, they're clean as a whistle and it's still like the power has dropped off :(



If anyone in Australia has purchased a replacement tube, happy for suggestions on that front too. Even upgrading if possible!





**"I Laser"**

---
---
**Phillip Conroy** *April 23, 2016 15:45*

About how many hours do you think you have done on that tube?Where in Aussie land do you live[i am in central vic-Bendigo],have you tryed a new focal lens?as i have found that the focal lens can have undetectable damage/wear .my tube has about 400 hours on it and still going strong,i have a laser power meter if you want a lend and you are willing to pay a refundable security and post[i am the person who found them on special ]04 34 55 06 78


---
**I Laser** *April 23, 2016 21:45*

At a rough guess, about 300 hours. I replaced the focal lens fairly recently though have noticed the lightobject air assist head has slightly scratched it in a circular pattern where it tightens up. Not happy Jan!!! :|



Ah so you're responsible for the meter lol! Thanks for the offer appreciate it, I've just pulled the trigger on one, I have two machines so no doubt will need one sooner or later. :)



It's strange as the tube seems fine, seems to fire okay, it's just lacking penetration. 3mm MDF takes two passes at 6.5mm when it previously would take a single pass at the same speed.



 I'm sure it's something obvious I'm missing! Where did you buy your lens from?


---
**Phillip Conroy** *April 23, 2016 22:15*

Tested mine with power meter just after 1st  mirror =15 watts at 10 ma my focal lens 18mm came from ebay   [http://www.ebay.com.au/itm/252217170604?_trksid=p2057872.m2749.l2649&ssPageName=STRK:MEBIDX:IT](http://www.ebay.com.au/itm/252217170604?_trksid=p2057872.m2749.l2649&ssPageName=STRK:MEBIDX:IT)

What do you mean by cut new mirrprs?


---
**I Laser** *April 23, 2016 22:24*

Mirrors cut from old hard disks, PM me your address and I'll send you a set. :)

They're a bit rough on the edges and a little smaller than I'd like (18mm) but they work fine.


---
**Phillip Conroy** *April 24, 2016 10:14*

Yes i have heard of using hard drjve platters as mirrors,would be interseted in seeing your power readings with them compared to proper mirrors,do they heat up at all


---
**I Laser** *April 24, 2016 10:50*

Yes it should be very interesting, hopefully Bell Laser is quicker on the delivery than they were for Scott!



Can't say I've checked them heat wise, been using them for a few months now and haven't had any issues. Were as good as the mirrors they replaced anyway. :)


---
**Phillip Conroy** *April 24, 2016 20:33*

I gotbmy meter about a week before scott, they ran out of stock,how do you make the mirrors


---
**I Laser** *April 24, 2016 23:57*

Ha, good to know, hopefully shouldn't have long to wait then.



Regarding the hard disk mirrors, basically a dodgy template and a hole saw, contrary to what Bunnings told me/link below states you're going to want at least a 25mm hole saw, the saw itself negates 4mm (lennox brand) from the working area. For the next batch I'm thinking maybe a 32mm hole saw, which will provide 28mm mirrors, they'd fit the holders but still need to check the LO head.



Anyway link here:

[http://diylaser.blogspot.com.au/2012/01/get-your-co2-laser-mirrors-for-free.html](http://diylaser.blogspot.com.au/2012/01/get-your-co2-laser-mirrors-for-free.html)


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/EC4cVUSsdvP) &mdash; content and formatting may not be reliable*
