---
layout: post
title: "Just did a test cut of this image with CorelLASER"
date: May 07, 2016 01:30
category: "Software"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Just did a test cut of this image with CorelLASER.



I decided to test if I can make the separate cut layers different colours & whether Corel will recognise them & convert to black, or if it will do something odd. So far, it's still cutting, but it seems that it has no problem recognising the two colours & they are both converted to black in the cut dialog window.



This is useful to me, as sometimes I am using multiple layers & it gets difficult to tell which is which & select the one I want when they are all the same colour. Thought it may come in handy to someone else.

![images/d3963d13b665ac098456888a21a4d4f7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d3963d13b665ac098456888a21a4d4f7.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Tony Sobczak** *May 07, 2016 02:11*

I've used red,blue green and black on different layers without any issues so far.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 02:12*

**+Tony Sobczak** Yeah seems to be good. I had just been using lots of black layers. Quite confusing. Much better to be able to colour them. I don't know why I didn't even think about this or test this til now.


---
**Pigeon FX** *May 07, 2016 23:05*

Yep, I have used different colors for different cuts, like internal cutout, and it seems to handle them fine as long as the lines are selected. Really handy as I tend to use a few different colours when designing in Illustrator, CorelLASER is growing on me, even though it has its evil quirks like to many notifications and a whited out 'File Edit....' menu......a small price to pay, and less traumatic experiance then I was expecting. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 00:19*

**+Pigeon FX** Oh you have the whited out File Edit menu issue too. Not a big drama, but confused me to begin with. I thought the toolbar was switched off haha.



I was doing some cutting a few hours ago & it seems that white borders on objects cut also. Didn't expect that & was not what I was intending. But yeah, CorelLaser is decent once you get the hang of it. However, for these particular images that I was working with, as I changed them to vector engraves (by creating many 0.5mm wide rectangles that I intersected with the actual image in AI) there were loooooots of individual objects. This caused it to lag about 5-10 minutes everytime I clicked "Cut" to open the dialog window. Running MacBook Pro with 16gb ram, so it's not a ram issue (was only 20% used) & was chewing about 30-40% cpu when waiting for the dialog to finally open. So it seems to have some limitations on the amount of objects in a cut-layer before you are going to crash it.


---
**Pigeon FX** *May 08, 2016 00:39*

Thats good to know about white boarders, could see that catching me out and confusing the hell out of me if I didn't know why!


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/4MnPYBzJS8Z) &mdash; content and formatting may not be reliable*
