---
layout: post
title: "Originally shared by Bill Brayman Found this conversation on MeWe between a user and MeWe staff"
date: December 30, 2018 15:51
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
<b>Originally shared by Bill Brayman</b>



Found this conversation on MeWe between a user and MeWe staff. You may find the answers interesting.



MeWe user posts list of missing features:



1. Can't share post privately to our followers also includes able to share posts privately to selected users.

2. When resharing posts from followers/group there's no mention of the users/group name who the post been shared from.

3. Don't get it why MeWe doesn't allows to view our profile and group to public non MeWe users without signing up when we share the link.

4. There's no link options for posts. How to share individual post with the help of links. Posts can only be shared within MeWes system.

5. When sharing posts in group why the shared content is not seen on ones timeline.

6. No group classification. Like which is your owned group and joined.

7. No sharing counter. How many and who shared your post.

8. No option to disable comments and sharing.











Mark Stronge of MeWe answers:



All of your posts are private, shared to your contacts, there are no followers on MeWe so choose who you want to give access to be able to chat to. A close friends option is already in the works.



Already in the works



MeWe is built on consent and privacy. Public posts are coming but they will be public within MeWe, not to the web.



This is related to privacy and the design of MeWe, that may change.



Groups and your timeline are separate.



A very minor thing, you should know which group you created though.



That might be added when the sharing with original source shown is added



Disabling comments might be useful, you can already disable sharing of all of your posts in the settings.



Followers is coming in 2 weeks





**"HalfNormal"**

---
---
**ThantiK** *December 31, 2018 01:22*

Not public to the web means useless.  My group posts a lot of research and help for people who are troubleshooting issues. No public posts means no finding, no discovery, no reason to join. 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/4gBQg1GJwjf) &mdash; content and formatting may not be reliable*
