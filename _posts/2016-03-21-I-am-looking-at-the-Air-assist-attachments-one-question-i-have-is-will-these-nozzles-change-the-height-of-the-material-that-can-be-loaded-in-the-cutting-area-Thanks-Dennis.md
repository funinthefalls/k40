---
layout: post
title: "I am looking at the Air assist attachments one question i have is will these nozzles change the height of the material that can be loaded in the cutting area Thanks Dennis"
date: March 21, 2016 20:19
category: "Discussion"
author: "Dennis Fuente"
---
I am looking at the Air assist attachments one question i have is will these nozzles change the height of the material that can be loaded in the cutting area 

Thanks

Dennis 





**"Dennis Fuente"**

---
---
**Stephane Buisson** *March 21, 2016 22:31*

nope, this machine have a fixed focal length.

meaning the distance between lens and object you want to engrave should be 50.8mm. for cutting you can adjust by half the material thickness.

Meaning spare room for big object is under.


---
**Brooke Hedrick** *March 22, 2016 00:15*

An adjustable z-table is what you will want to handle different height/thickness objects.


---
**TinkerMake** *March 22, 2016 02:07*

Does this mean that if you are cutting through 3mm ply, you will need to adjust the height of the object for each pass? Newbie to lasers, pardon any basic questions...**+Stephane Buisson** 


---
**Stephane Buisson** *March 22, 2016 12:32*

**+TinkerMake** on the math side yes, but in the fact you don't (don't move between 2 passes, you risk your origin position). as the first pass will have done some of the work (more than the half), and the following pass will do the job.(consider the second pass less effective as you have some char in the way, Air assist help a lot to have a better combustion and less ashes) 

Ps: I do 3mm plywood in one pass


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/SQaEJ8cBs5u) &mdash; content and formatting may not be reliable*
