---
layout: post
title: "Question: Should I try blackening aluminum? I've seen several 20W scanning lasers (laser markers with galvos) do the trick (though they guard their settings unless you buy from them), but one thing I'm worried about with the"
date: February 16, 2016 21:59
category: "Materials and settings"
author: "Thor Johnson"
---
Question:  Should I try blackening aluminum?

I've seen several 20W scanning lasers (laser markers with galvos) do the trick (though they guard their settings unless you buy from them), but one thing I'm worried about with the K40 is that in the K40, the laser is perpendicular to the object, so if the light gets reflected back into the tube, you can overload it... (not a problem for galvo scanners because what gets reflected scatters into the room [and you!  wear protection!])



Anyone done this with a K40 and willing to share settings or should I stay away from this?





**"Thor Johnson"**

---
---
**Stephane Buisson** *February 16, 2016 23:32*

this is not a direct answer to your question, but the ray will have hard "return path" through the lens. the goal of the lens is to concentrated the ray, the other way around the ray will be diffracted by the lens protecting the tube. but the lens could shatter by overheating if you get the perfect calibration (hard on K40).


---
**Scott Thorne** *February 17, 2016 10:26*

I've engraved lots of anodized aluminum with the k40....high power slow speeds is the way to go with it....I was actually doing serial numbers on weapons....so I was etching down in a little bit....but I've never had any problems doing it.


---
*Imported from [Google+](https://plus.google.com/100850237464188460837/posts/Ts9DUccBzKa) &mdash; content and formatting may not be reliable*
