---
layout: post
title: "So my eBay laser tube arrived broken...see pic Question is, do you think if I adhere it back together with silicone sealant would it still work?"
date: June 14, 2017 19:14
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
So my eBay laser tube arrived broken...see pic



Question is, do you think if I adhere it back together with silicone sealant would it still work?

![images/e1cc2ef7b8670b3caf99026e104b72bf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1cc2ef7b8670b3caf99026e104b72bf.jpeg)



**"Nathan Thomas"**

---
---
**Joe Alexander** *June 14, 2017 19:33*

if its only the water part and not the gas seal then theoretically yes. Silicone and pray my friend :)


---
**Don Kleinschnitz Jr.** *June 14, 2017 20:28*

Sorry to see that your having so many "experiences"....

Here is a post on the subject. Seems as if epoxy is an available and good choice.

[plus.google.com - What to do?? My water pump stopped working and I didn't notice for quite a wh...](https://plus.google.com/112581340387138156202/posts/Jz9gAKuSfTc)




---
**Nathan Thomas** *June 14, 2017 20:36*

**+Don Kleinschnitz** damn, wish I would've found this post earlier when I searched. 



I've already started using Super glue...the gorilla glue brand as that's what I had laying around.



So far results are mixed. The plan was to go over the super glue with silicone adhesive to ensure no leakage but it popped off on me. 



In the process of round two now but if it doesn't work will go get the glass glue and other things as suggested in the post you linked


---
**Don Kleinschnitz Jr.** *June 14, 2017 22:18*

**+Nathan Thomas** I assume that you know about this. This kind of thing is captured there since its so hard to find stuff on this community. Also you can follow me and see my collections which capture these posts by category.



[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Nathan Thomas** *June 14, 2017 22:29*

**+Don Kleinschnitz** got it 😎


---
**Steve Clark** *June 15, 2017 16:17*

I would use 3M 1838 two part Scotch-Weld Epoxy. The only drawback on it is it is expensive. We used it for aerospace applications, satellites, missiles, and bonding magnets into motors. Very good stuff and what I would use if my tube came apart. Be sure to read the tech sheet on prepping the surfaces…



[hillas.com - 3M Scotch-Weld Epoxy Adhesive Green Part B/A, 2 fl oz Kit, Scotch-Weld 1838, 62-1838-0530-8 &#x7c; Hillas.com](http://www.hillas.com/Categories/Epoxy-Adhesives/3M-Scotch-Weld-Epoxy-Adhesive-1838-Green-Part-B-A-2-fl-oz-Kit-6-per-case.html?gclid=CjwKEAjw4IjKBRDr6p752cCUm3kSJAC-eqRt1OJnfoVma7RpZgHyf5LLME_RKwlGgeTGuUw_BEfoZRoC_Tzw_wcB)



I see it on ebay at times but would think twice about buying there as the epoxy only has a 24 month shelf life.  Though I have used some that was expired by 2 years and it still worked fine but just on noncritical stuff. 




---
**Nathan Thomas** *June 15, 2017 17:46*

Update:



So far so good. What I did was apply 3 layers of adhesive to compensate for not having or using the recommended epoxy.



The first layer is Gorilla Glue brand super glue that I had lying around. I gave that about 10 minutes to cure. Then applied a thick layer of waterproof silicone sealant adhesive. Gave that about an hour to harden a little bit. Then came back with another layer of super glue on the top and the bottom just for overkill and in case there were any tiny spots where water could leak that I missed. I let the whole thing cure overnight.



No leaks so far as I've filled the tube with water this morning. Still waiting for the silicone to fully cure over the nodes before test firing.


---
**Ned Hill** *June 16, 2017 15:21*

Just for future reference cyanoacrylate glues (superglue) are not recommended around optics as they tend to fume and can get on the optics.


---
**Nathan Thomas** *June 16, 2017 15:33*

**+Ned Hill** yes I was told that earlier, but it was already too late. I've already ordered another tube anyways 


---
**Don Kleinschnitz Jr.** *June 16, 2017 16:11*

**+Nathan Thomas** what do you plan for the tube when dead or if this repair fails?


---
**Nathan Thomas** *June 16, 2017 16:13*

Get rid of it and buy another. The eBay seller is sending me a replacement but I'm going to use it as a spare whenever it arrives.


---
**Don Kleinschnitz Jr.** *June 16, 2017 16:17*

**+Nathan Thomas** if you going to trash it anyway I'm looking for one partly operational to test with,  coolant conductivity, flow  etc tests. Doesn't need an good output. 


---
**Nathan Thomas** *June 16, 2017 18:00*

**+Don Kleinschnitz** ok I'll know by tomorrow at the latest. Was waiting for the silicone to cure and will test fire. But it's yours if it doesn't work.


---
**Don Kleinschnitz Jr.** *June 16, 2017 19:19*

**+Nathan Thomas** were are you located?


---
**Nathan Thomas** *June 22, 2017 15:50*

Ok, I tested it and all seems good so far. No leaks and it is engraving well so far. I haven't tried cutting yet which I'll get to hopefully in the next day or so. I'm back to work this week, so time is scarce again. 


---
**Nathan Thomas** *June 22, 2017 15:52*

**+Don Kleinschnitz** I'm in the U.S.



This tube is working so far, but I do have the original tube if you want it. I know it doesn't work because it was the tube that blew with the condensation on it. But as far as why exactly, not sure yet. I don't see any obvious cracks, so if there's a crack somewhere it's small.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/Zt2q1x3bTpG) &mdash; content and formatting may not be reliable*
