---
layout: post
title: "Video attached - Can anybody explain why this happening?"
date: June 29, 2017 22:17
category: "Discussion"
author: "Adam J"
---
Video attached - Can anybody explain why this happening? C3D mini (Smoothie) installed and was previously working fine. Jittering / Stuttering while cutting, producing a horrible cut. Cut is a vector and has previously cut perfectly fine. Cut is on card at 64 m/s.







**"Adam J"**

---
---
**greg greene** *June 29, 2017 22:29*

Have the same setup, sometimes I get that too, I think it has to do with the motor driver boards not providing enough current for the motors.


---
**Jeff Lamb** *June 30, 2017 06:15*

I'd have to agree. I had similar problems at anything over 60mm/s  I added a 24v 5a psi, made sure the y belts were tight enough and now I can go much faster without problems.  Even managed a 450mm/s engrave last night without problems although I still have to watch back the video and calculate actual speed.


---
**Adam J** *June 30, 2017 13:41*

Thanks for the information guys. After a hard reset of everything, the problem went away! I'd like to tighten my Y axis belt on the right-hand side but I'm not sure where to do this? I've removed the partition between cutting area and PSU/Board. I'm not sure exactly what or where I need to tighten?


---
**Jeff Lamb** *June 30, 2017 15:27*

**+Pikachupoo** The y belt tensioners are access through two small holes at the back either side of the fan.  You will need a fairly long cross head driver to reach them.


---
**Adam J** *July 04, 2017 17:07*

Cheers Jeff. This did turn out to be a dodgy driver board. After a hard reset, everything seemed okay. Later on, I couldn't cut through hardly anything. Checked my alignment which was spot on, cleaned mirrors and lenses. Still couldn't cut through barely anything. Decided to replace both drivers and the problem went away.


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/Cra5eAkJVBj) &mdash; content and formatting may not be reliable*
