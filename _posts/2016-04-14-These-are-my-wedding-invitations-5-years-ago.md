---
layout: post
title: "These are my wedding invitations , 5 years ago"
date: April 14, 2016 19:31
category: "Object produced with laser"
author: "Roberto Fernandez"
---


These are my wedding invitations , 5 years ago. if it serves as inspiration.



![images/372a122d2d2e2f57e04f2784e5550a9e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/372a122d2d2e2f57e04f2784e5550a9e.jpeg)
![images/c078a544eda34bd2d22e5ca6613cdd4e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c078a544eda34bd2d22e5ca6613cdd4e.jpeg)
![images/5b5558842dd340ad6a702435572c80a3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5b5558842dd340ad6a702435572c80a3.jpeg)

**"Roberto Fernandez"**

---
---
**Alejandro Gil** *April 15, 2016 09:19*

Precioso!!!!


---
**Roberto Fernandez** *April 15, 2016 13:11*

Gracias Alejandro.


---
*Imported from [Google+](https://plus.google.com/102797371787170159448/posts/ZV9bM2RNjXp) &mdash; content and formatting may not be reliable*
