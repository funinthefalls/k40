---
layout: post
title: "Completed my K40S interface board connects to all control signals, less the Y axis steppers, includes interlock ports for simpler wiring"
date: September 16, 2016 20:14
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Completed my K40S interface board connects to all control signals, less the Y axis steppers, includes interlock ports for simpler wiring.



![images/fe2ff06265f5b0211187ec6451887471.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe2ff06265f5b0211187ec6451887471.jpeg)
![images/167834fb05cf60f521238146d5292db9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/167834fb05cf60f521238146d5292db9.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**greg greene** *September 16, 2016 20:32*

kewl


---
**Anthony Bolgar** *September 16, 2016 21:10*

Have you figured out the optimal way to get the PWM signal to the PSU? I know you were doing quite a bit of research  and testing in the past.


---
**Jim Hatch** *September 16, 2016 21:46*

You know a laser engraver could engrave those labels into the wiring blocks. They could even be a different color with some painters tape masking for the engrave operation and then a quick shot of spray paint to fill in the engrave.



Know anyone with a laser? ;-)


---
**Don Kleinschnitz Jr.** *September 17, 2016 00:21*

This is killing me ...... I wish I had one of those laser things .....:).


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/TyzYMyrjZj8) &mdash; content and formatting may not be reliable*
