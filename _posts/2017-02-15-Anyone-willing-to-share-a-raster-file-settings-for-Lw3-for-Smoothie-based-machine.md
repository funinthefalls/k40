---
layout: post
title: "Anyone willing to share a raster file + settings (for Lw3) for Smoothie based machine?"
date: February 15, 2017 13:59
category: "Hardware and Laser settings"
author: "Kostas Filosofou"
---
Anyone willing to share a raster file + settings (for Lw3) for Smoothie based machine? I am new to this I just try some raster engraving but I am not sure if I use the correct settings... I think my machine don't proper regulate the laser power in raster..









**"Kostas Filosofou"**

---
---
**Claudio Prezzi** *February 15, 2017 21:02*

Using a grayscale like that could probably help you

![images/e3281fad7d01402f8b77926ab88f0538.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e3281fad7d01402f8b77926ab88f0538.jpeg)


---
**Kostas Filosofou** *February 15, 2017 22:59*

**+Claudio Prezzi** thanks I'll give it a try...


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/AGGbMf56Jke) &mdash; content and formatting may not be reliable*
