---
layout: post
title: "Peltier Cooling System Part 6. Its alive Finally I finished this cooler!"
date: August 15, 2017 04:31
category: "Modification"
author: "Steve Clark"
---
Peltier Cooling System Part 6. “It’s alive”



Finally I finished this cooler! A quick summary:



Six 60 watt Peltiers = 360 watts with an estimated actual calculated output of 252 watts of cooling.



A small reserve tank of slightly less than 2 liters with a total system capacity measured of 2 1/4 liters.



The pump is an inline “Little Giant” 2-MDQ –SC Water Pump.



The cooling line and pump is insulated with closed cell foam 9 to 12 mm thick with a few areas of only about 3mm. The reserve tank box varies from 12mm to 38mm thick.



The system is temperature controlled with a JLD-612 Temperature Controller and there is an inline thermocouple also an inline flow switch linked to the laser switch and a visual type flow paddle wheel.



Panel lights show that there is power to the Peltier’s six fans on all the time when the main power switch is.  There are also lights for each the two Peltier banks that go on when power is applied (turning on and off) to them through the temperature controller. The Temp Controller lights up when it cycles the relays but that is not telling the power got there.



Each Peltier bank has its own 30 amp power supply and each has a line fuse of 20 amp on the 12 volt side.  The power going to the Peltiers is of course  turned on and off by the Temp Controller and through solid state relays for each bank. 



At this point it appears that the Peltiers are doing fine without me coming up with some sophisticated current controlling device as the efficiency is great. I’ve ran the system twice for at least 6 hrs (without the laser) each time and they are performing fine. Note that the Temp Controller pulses the power to the Pelteirs for the first 5 mins or so then the pulsing slowly fades over the next 7 mins to almost completely gone then turns on and off to maintain the programmed temp needs. The cool down plot is not linear but cools faster toward the end. Makes sense with the way the controller pulses. If it was powered on full time at first I’m sure it would cool the water down in less than half the time it is now. I’m not sure I want to mess with it until I understand the Peltiers current draws and needs better.



I learned a lot about planning wire runs as the bundles can add up really fast and also about calculating wire sizes for current needs. Also planning the coolant line flows and addressing the filling and draining of the water problems. I found out the hard way to pretest ALL water line connections BEFORE closing it all up. I can write up something about draining and refilling the water later if there is an interest as it’s not as simple as it seems.



System performance so far is excellent. It takes about 20 minutes to cool the system down to the programmed temperature 20C from 29-30C. Today was a mild day and it was about 10 or 12 minutes to go from 25C to 20C.  At this point the system has not had any problems keeping the water cool and the temperature has only fluctuated a degree or so staying within the programmed limits easily. However, this is based on the 6 runs one raster or 20min and 5 vector cuts of 4 min each back to back. Limits are set to 20C plus or minus 2C right now as we have had a few days of 40 to 45% humidity. Later I’m going to reprogram it to about 16C nominal.



The Temp Controller works this way, it pulses the power to the Pelteirs for the first 5 mins or so then the pulsing slowly fades over the next 7 mins to almost completely gone… then when it reaches the programmed temp it turns on and off to maintain it.. The cool down plot is not linear but cools faster toward the end. Makes sense with the way the controller pulses. If it was powered on full time at first I’m sure it would cool the water down in less than half the time it is now. I’m not sure I want to mess with it until I understand the Peltiers current draws and needs better. Given the cutting and engraving I’ve done so far, once its reach temp, it’s not even noticeable that it is doing any work to control the temp while running.

 

One thing I noted is that my notes is that say I was cutting 6.5mm thick Popular at 12ma (300mm/min)…now I’m getting the same cut at 7ma. I’ll have to do a few more test to be sure but this observation suggest better performance at a lower power with the cooling and with the CD3 board. (no re-alignments of the mirrors or lens cleaning has been done).



I’ll try to post some schematics of the wiring later and a parts list but for now here are some final pictures.   





![images/310af3a7f3bf458e874da43f64f2cd77.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/310af3a7f3bf458e874da43f64f2cd77.jpeg)
![images/ec19e0a7e1cbcf37230c5bc81be3f882.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec19e0a7e1cbcf37230c5bc81be3f882.jpeg)
![images/82545911acfa11ba0a0ae8b019c1b5ab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/82545911acfa11ba0a0ae8b019c1b5ab.jpeg)
![images/2542c1f8922438fd64b0f84adc2a0289.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2542c1f8922438fd64b0f84adc2a0289.jpeg)
![images/ff2841d994d898ef727015f0a5042a5f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff2841d994d898ef727015f0a5042a5f.jpeg)
![images/93d22f7ab5de6c1d34b929b434f2b6c8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/93d22f7ab5de6c1d34b929b434f2b6c8.jpeg)
![images/a871537cc6fca0b8443a886095f717e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a871537cc6fca0b8443a886095f717e9.jpeg)

**"Steve Clark"**

---
---
**Anthony Bolgar** *August 15, 2017 05:28*

Nicely done


---
**Jorge Robles** *August 15, 2017 06:07*

Congrats! Average cost, 400usd?


---
**Don Kleinschnitz Jr.** *August 15, 2017 12:02*

Sweet design and packaging, best I have seen up till now on this community. Can't wait for the design information :). A peek inside the cooling box would be a treat.



1. Is there anything special about the JLD-612 Temperature Controller, it seems expensive for an on-off controller?



2. Your maximum deltaT was 10 degrees? I am wondering about this approach since my shop this summer hit 80-90 degrees.



It seems that cooling has a significant impact on actual power. Makes sense, would be great to have a function defined for relative output power vs coolant temperature. Maybe we should be spending more $ on cooling than getting bigger lasers? 

Lower cooling temp = more power at lower current = less LPS current = longer laser life = more reliable power!



We need to come up with a cheap power meter. Idea hint: did you know that peltier's work in reverse. Hit them with a hot laser and they will generate an electrical output :). Not gotten a chance to work on this yet.



#K40Cooling


---
**Steve Clark** *August 15, 2017 23:54*

**+Jorge Robles** **+Don Kleinschnitz** 

I’m not a guru on these controllers but this is a PID type.

This is the information I read on PID controllers. “A proportional–integral–derivative controller is a control loop feedback mechanism (controller) widely used in industrial control systems.” And here is the manual link:



[http://mythopoeic.org/misc-files/JLD612_Manual.pdf](http://mythopoeic.org/misc-files/JLD612_Manual.pdf) 



I bought the DC type which runs off of 12 to 32 v.



I’m still learning what it does but it’s not a simple on off controller. I don’t completely understand the PID parameters well enough to adjust them so I’m using the Auto Tune mode “ By simply press a single button the built-in artificial intelligents is activated to automatically calculate and set parameters (P, I, D, SouF, ot) that fit the condition to be controlled.”



The first day I tested the system I bypassed the laser tube just in case I got things too cold. It was 41C outside, really hot for here, and maybe 31 or 32C inside ( the wife opened the garage door earlier and left it open). My first test was 20C nominal plus or minus 2C.  



After that I played with the temp controller and set the nominal temp to 10C plus or minus 2C… it started at the 20C and took about a degree per minute to reach the overshoot point of 8C then retreated to stabilize at 10C so there seems to be plenty of cooling power (a Delta T of over 30C?). I just have not played with it enough yet to know the limitations. When I have time I’d like to run a hour long laser job at to see how well it controls the temp.



I drove over to LightObject  and after a long discussion with them about the cooling system I was working on bought this controller on their recommendation. The unit cost $36 for the 12 v version . The relays were $9.50 ea and heat sinks $6.50.

 

One note I didn’t comment on earlier is when the ambient temp is high (larger Delta T), I believe the insulation makes a more significant difference in speed of cooling and ability to maintain the programmed nominal. The  silicone tubing seems to be very thermally conductive with no real insulative properties to speak of.

 

Don, I’m not sure which coolant “box” you wanted to see the inside of, the electrical or the peltier… but here are pics of both. Lots of wires but really wasn’t too hard to keep track of by marking.



I do have a pair of digital ammeters and shunts but I’m running out of space so I’m not sure where I would mount them. Those shunts are big!. Plus I have no real way to check the shunts calibration. They are suppose to be the right ones but they came from China and I’ve read some can be off by quite a bit. Also 20 amps is right at the limit of my meter.  I suppose I could use a resistor in the circuit. I believe I saw someone doing that on youtube.



I haven’t added it all up but I think I have about $350 in it however, I think I could build one for around 200 bucks for the K40 now.  I over sized this one for the future day when my tube starts going south so I could put a higher wattage tube in.







![images/9af967549cf10969f2b5dc69fa54c0c3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9af967549cf10969f2b5dc69fa54c0c3.jpeg)


---
**Steve Clark** *August 15, 2017 23:55*

It only let me post one photo here is the other...

![images/ea689d934e03770fcb5c3ba17f8d8649.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ea689d934e03770fcb5c3ba17f8d8649.jpeg)


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/A2m6Csg59b8) &mdash; content and formatting may not be reliable*
