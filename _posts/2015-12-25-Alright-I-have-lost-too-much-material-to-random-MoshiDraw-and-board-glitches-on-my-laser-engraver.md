---
layout: post
title: "Alright. I have lost too much material to random MoshiDraw and board glitches on my laser engraver"
date: December 25, 2015 08:45
category: "Discussion"
author: "Paul Haban"
---
Alright. I have lost too much material to random MoshiDraw and board glitches on my laser engraver. Should I upgrade my K40 with smoothieboard or LightObject DSP board? 





**"Paul Haban"**

---
---
**Brooke Hedrick** *December 25, 2015 13:13*

I can vouch for lightobjects.  It has been an excellent upgrade to my k40/MoshiDraw machine.  LaserCAD is much more reliable and easier to use.



I have only been using a laser cutter for about 3 months now and have no other basis for comparison.


---
**Anthony Bolgar** *December 25, 2015 16:12*

DSP controllers are good, but it much less expensive to upgrade the board to a smoothie board or Ramps 1.4/Arduino board combo. Some great open source software available for these boards, Visicut works with smoothieboard, but my new favorite is LaserWeb byPeter van der Walt. Great thing about LaserWeb is that Peter is an active member of this group and can help troubleshoot any issues you might have. If you go with Ramps 1.4/Arduino, you can design in Inkscape and export the gcode created by using the Turnkey Tyranny plugin for Inkscape. Does vector and raster very well. 



My current workflow is as follows:



a)Design using CorelDraw X17 and save as SVG file.



b)Import the SVG file into Inkscape template that has all the page settings setup for the Laser cutter/engraver.



c)Run the Turnkey Tyranny Inkscape plugin to great the gcode (Does both vector and raster in the file).



c)Send the gcode file to the printer using Pronterface.





Once the LaseWeb software is out of Beta, I will most likely start using it as my default. 



The workflow will be simplified to the following:



a)Design using CorelDraw X7 and save as SVG file.



b) Import into LaserWeb and send to Laser Cutter/Engraver.



Not sure on the exact process for smoothieboard using Visicut, but Stephane has upgrade his machine to smoothieboard and I am sure he could help you with the upgrade. 



If you want help with a Ramps 1.4/Arduino upgrade, let me know and I can help you with the upgrade process.






---
**Joe Spanier** *December 25, 2015 16:13*

You'll be happier either direction you take. 


---
**Tom Hlina** *December 25, 2015 16:36*

LaOs ([http://redmine.laoslaser.org/projects/laos/wiki](http://redmine.laoslaser.org/projects/laos/wiki)) will be an other option. I'm very happy with it.


---
*Imported from [Google+](https://plus.google.com/115695050282198990263/posts/PodpML6ZzWG) &mdash; content and formatting may not be reliable*
