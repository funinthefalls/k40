---
layout: post
title: "Making some name tags for some people who work at the hospital where my wife works"
date: December 17, 2018 15:18
category: "Object produced with laser"
author: "Ned Hill"
---
Making some name tags for some people who work at the hospital where my wife works.  3mm alder. I do pin and magnetic backs.  I also came up with away to attach a badge reel clip. It’s a dropped slot cut from acrylic and glued to the back at the bottom. Since it’s a magnetic back I also have to beef up the magnetic holding power by adding extra magnets glued to the metal plate with JB weld. 



![images/0322b7a5e5980bd1810a4dbfe8bbf4cf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0322b7a5e5980bd1810a4dbfe8bbf4cf.jpeg)
![images/2ac2e7f160f21692157c28996e8597e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2ac2e7f160f21692157c28996e8597e5.jpeg)
![images/b76692b724c2ae3eabd3c5e12bf97a72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b76692b724c2ae3eabd3c5e12bf97a72.jpeg)

**"Ned Hill"**

---
---
**HalfNormal** *December 17, 2018 15:32*

They look great! I have thought of making pins for my fellow nurses at work but in the OR, everything has to be fire retardant so wood is definitely out.


---
**Ned Hill** *December 17, 2018 15:39*

**+HalfNormal** you can get two tone acrylic to laser engrave. The top layer is one color but when you engrave it the engraving shows a different color underneath. 


---
**HalfNormal** *December 17, 2018 15:55*

**+Ned Hill** Playing with two tone acrylic is on my todo list. Just need to get around to purchasing some.


---
**Ned Hill** *December 17, 2018 16:02*

**+HalfNormal**  lol mine to :)


---
**Don Kleinschnitz Jr.** *December 17, 2018 16:03*

We're do you get two tone?


---
**HalfNormal** *December 17, 2018 16:25*

**+Don Kleinschnitz Jr.** Here is one of several sources.



[inventables.com - Inventables: 2 color Acrylic](https://www.inventables.com/categories/materials/acrylic?utf8=%E2%9C%93&selected_filters%5BEffect%5D%5B%5D=2+color)


---
**Anders Sandström** *December 18, 2018 07:59*

But does not acrylic burn way nastier than wood?

Or is that flame retardant treated acrylic?


---
**Travis Sawyer** *December 18, 2018 11:15*

Look into Johnson plastics and b.f. plastics. They have what I use for the pocket badges for lodge.


---
**Travis Sawyer** *December 18, 2018 11:21*

![images/4971e16a9189352dc3cfac90b2029cf9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4971e16a9189352dc3cfac90b2029cf9.jpeg)


---
**Ned Hill** *December 18, 2018 14:06*

**+Travis Sawyer** Very nice!  


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/4qWB3NWroT1) &mdash; content and formatting may not be reliable*
