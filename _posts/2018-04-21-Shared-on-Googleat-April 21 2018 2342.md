---
layout: post
title: "Shared on April 21, 2018 23:42...\n"
date: April 21, 2018 23:42
category: "Discussion"
author: "Mario Garcia"
---

{% include youtubePlayer.html id="mfL-b1D8ZH4" %}
[https://youtu.be/mfL-b1D8ZH4](https://youtu.be/mfL-b1D8ZH4) 





**"Mario Garcia"**

---
---
**Don Kleinschnitz Jr.** *April 23, 2018 10:57*

Looking at your posts it looks like you:



1. Have no laser output when you push the test button on the laser power supply (LPS) ?

2. The led down on the LPS is illuminated?



Are these correct?



Note the output of this supply is <b>LETHA</b> so extreme caution is suggested when operating it such as you video shows...


---
**Mario Garcia** *April 26, 2018 18:14*

**+Don Kleinschnitz**    thank for your safety recomendation I thought as well,  

For #1 yes there are not lasser output when I did push the test button.  #2 yes the LED is iluminated,  I think it was enough power to move the CNC head but not for lasser due to if I run a routine it runs correctly but wihtout lasser. 


---
**BEN 3D** *April 27, 2018 06:04*

Did you put a pice of paper directly to the laser? May it is working but your Mirrors are not calibrated correctly. Because your tube is illuminated and the light of the laser is not detectable for human eyes or cameras. Here is a Video where I tested my with a Paper. 
{% include youtubePlayer.html id="PJAI9bCLrd0" %}
[youtube.com - BEN 3D China Laser und ein Zettel](https://youtu.be/PJAI9bCLrd0)


---
**Mario Garcia** *April 29, 2018 16:32*

**+BEN 3D**  it was working properly, I was cutting a segment of MDF  then  it did the incomplete job,  it continue movig but with out lasser.  anyhow I will try as you did with your equipment, regards


---
**BEN 3D** *April 30, 2018 05:38*

**+Mario Garcia** hopefully may just a minor mirror loosen screw dissalightment Issue.



If that does not work and the paper stay holeless then may the wire connection between the powersupply and the ampere meter and the tube is broken anywhere...


---
**Doug LaRue** *May 06, 2018 03:28*

There is a test button on the power supply. It's small but it's there and it bipasses the control panel wiring.  Use a non-conductive piece of wood or something to push the button and if it fires then the problem is wiring.


---
**Doug LaRue** *May 06, 2018 03:31*

I see, it was working and then stopped so it is not likely the wiring. But the button on the PSU will tell you this. If it works then maybe the power level POT failed. But if it does not work, I've heard people can get it working by replacing the Flyback transformer and the power rectifier on the PSU.  Or get a new PSU.


---
*Imported from [Google+](https://plus.google.com/105821114351730689720/posts/itMn1AUMthA) &mdash; content and formatting may not be reliable*
