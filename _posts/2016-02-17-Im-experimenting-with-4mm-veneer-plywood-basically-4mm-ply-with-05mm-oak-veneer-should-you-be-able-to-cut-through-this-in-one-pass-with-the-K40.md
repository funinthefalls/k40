---
layout: post
title: "I'm experimenting with 4mm veneer plywood (basically 4mm ply with 0.5mm oak veneer), should you be able to cut through this in one pass with the K40?"
date: February 17, 2016 14:52
category: "Discussion"
author: "Linda Barbakos"
---
I'm experimenting with 4mm veneer plywood (basically 4mm ply with 0.5mm oak veneer), should you be able to cut through this in one pass with the K40?



I've done some reference cuts but none seem to be the clear winner, also the cuts are inconsistent stopping partially through, what does this suggest about the alignment of my machine? Any feedback would be really appreciated.



![images/783d9d830470f3e6a62dd7dbe04ab7dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/783d9d830470f3e6a62dd7dbe04ab7dc.jpeg)
![images/29cac27cbedaa41c1d0d522c5ae9f867.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/29cac27cbedaa41c1d0d522c5ae9f867.jpeg)

**"Linda Barbakos"**

---
---
**Joseph Midjette Sr** *February 17, 2016 15:15*

I would venture to say that the inconsistencies have a lot to do with the materials that make up the ""Ply's"/Layers"" of the plywood. Try a setting of 70% @ 5mm's and see what that does for you....


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 17, 2016 15:54*

I have had similar results when cutting through leather at times. Some parts cut all the way through, whilst others are not quite all the way through. I would concur with Joseph, that the inconsistencies are likely related to the layers in the ply. I would suggest checking your lens orientation & focal distance (as these will also affect the ability to cut). Lens should be concave side facing down & focal distance (for stock machine) should be 50.8mm. From what I have read it's best to focus half way through the thickness of your material (if possible). Also, I have at times had to slow down to 3mm/s when cutting thick leather (3-5mm thickness), as it just refuses to cut if I go faster.


---
**Jim Hatch** *February 17, 2016 17:52*

As your tests show it's possible - slow with higher power is the key. The inconsistency is likely not the laser's issue. Your test cuts look like what you should expect. There is some dropout at the end but I don't know if it's the start or end of a cut. If it's the end of the cut furthest from the Y axis rail then you may want to look at tightening up the alignment.



The dropouts you're seeing in the middle is likely due to the material. Plywood has all sorts of stuff under the skin - different wood densities, glue, voids, etc so it's not uncommon to see variability of the depth of cut. You can really see this if you were to do a similar test sheet of cast acrylic which is homogenous through its entire thickness.



I usually do multiple higher speed cuts rather than try for a single pass. In LaserDRW you can set a #  of repeats and it'll redo the cut for you - just don't move the piece between run. Lower power cuts make cleaner ones oftentimes because there's less burning so I have less postprocessing to do.


---
**Linda Barbakos** *February 17, 2016 18:11*

Thanks, all your comments are very helpful. I will do the same tests on acrylic just to be sure as I'm fairly certain the alignment is good. 



I'm using CorelDRAW 12 which also has the repeat option so I can try that as well. My laser cutter came with the origin in the top right hand corner so all my cuts are mirrored (starting right to left) which is dumb but it works... ﻿


---
**Linda Barbakos** *February 19, 2016 21:38*

Just a follow up on this, I had the lens installed incorrectly but also my rail was not level along the x-axis which I think is the reason why the cuts were trailing off. I then realigned the laser head so that laser beam would hit it dead centre rather than off to one side, the beam was being cut off at higher power it seemed. All in all, I am getting much better performance now. Thanks for the helpful feedback!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 20, 2016 00:54*

**+Linda Barbakos** Good to hear that you've got it working better. Happy lasering!


---
**Mona Xu** *December 07, 2016 12:06*

We are the manufacturer of plywood machine, All our products with CE approved.more details please contact us.

Tel(whatsApp):+8615165558525

e-mail: moanjinlun@gmail.com


{% include youtubePlayer.html id="JgNIQPGAXro" %}
[youtube.com - jinlun spindless veneer peeling machine Max log diameter 800mm](https://www.youtube.com/watch?v=JgNIQPGAXro&feature=autoshare)


---
*Imported from [Google+](https://plus.google.com/107262725887494713165/posts/D8pq5HXepfu) &mdash; content and formatting may not be reliable*
