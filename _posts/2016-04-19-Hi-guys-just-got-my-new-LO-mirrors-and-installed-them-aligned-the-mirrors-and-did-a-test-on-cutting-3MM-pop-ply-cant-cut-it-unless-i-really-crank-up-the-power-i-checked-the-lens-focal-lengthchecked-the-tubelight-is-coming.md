---
layout: post
title: "Hi guys just got my new LO mirrors and installed them aligned the mirrors and did a test on cutting 3MM pop ply can't cut it unless i really crank up the power i checked the lens focal length,checked the tube,light is coming"
date: April 19, 2016 20:14
category: "Discussion"
author: "Dennis Fuente"
---
Hi guys just got my new LO mirrors and installed them aligned the mirrors and did a test on cutting 3MM pop ply can't cut it unless i really crank up the power i checked the lens focal length,checked the tube,light is coming right down the center don't see any problem there but i did notice something when i did an alignment the beam made 2 dot's on the target and i can't figure out why 





**"Dennis Fuente"**

---
---
**Jim Hatch** *April 19, 2016 20:36*

Lens in the correct way? Any water or dirt on the final mirror or lens?


---
**Dennis Fuente** *April 19, 2016 20:47*

Jim Thanks for the response brand new mirrors from lightobjects gold plated version, i pulled out the lens and double checked it for proper convex concave orintation bevel side is up per forum 


---
**Jim Hatch** *April 19, 2016 21:08*

The 2 spot thing suggests an aberration that's splitting the beam. Either on the lens or the nozzle. I just did the LO upgrade myself but am okay. I have had the 2 dots issue caused by a water (actually alcohol) when I was cleaning it. Once it evaporated & I wiped it for good measure it cleared up the problem. If it's not that or something like a metal burr in the nozzle aperture then it would seem to be a lens aberration. You'll want to talk to LO about that.


---
**Dennis Fuente** *April 19, 2016 21:23*

the lens is orignal you think it could be the lens itself 


---
**Dennis Fuente** *April 19, 2016 21:27*

so just looked at LO and they don't offer a12 MM replacement lens


---
**Jim Hatch** *April 19, 2016 22:05*

I upgraded the head at the same time and while I was at it the lens (18mm) too :-)



Starting at the source do you have the 2 dot issue anywhere else? Either using a piece of cardboard or a piece of tape or sticky note on each mirror starting at the one in the back with the laser (the fixed mirror) then the Y-Axis mirror and then the X-Axis (laser head) one. If all of those imprint a clean hole then the aberration is being introduced at the final mirror or lens. To figure out which, take off the lens and run the laser shot to see what it looks like unfocused - still should be round & a single spot. 



I prefer cardboard or a 1/4" plywood piece for this test because I don't have to worry about burning through the paper or tape and not getting a clear reading. The plywood is going to be able to take a sustained test shot at say 10ma which should tell you if you've got a clean circular spot. 



Wherever in the chain you get the double spot is where you'll know is the problem - the last optic that's directing the beam before your test ply.


---
**Dennis Fuente** *April 19, 2016 22:32*

Jim 

OK thanks for that i will go and give a test right now 



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 19, 2016 23:48*

I just had the 2 dot issue a few days back. Mine also were oval shaped, instead of circle. Turned out my alignment was quite off (since I was moving things & testing things). It seemed to be the final mirror for me. Also, I noticed in amongst all this that cleaning mirrors & lens with water works better than using methylated spirits. Methylated spirits kept leaving weird marks on the mirrors/lens that looked like it was burned. Once I washed them with water they were nearly perfectly clean again.


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/UALof3AMUdh) &mdash; content and formatting may not be reliable*
