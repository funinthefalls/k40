---
layout: post
title: "Shop Tip - When you need a metallic color engraving infill try metallic calligraphy inks"
date: February 09, 2018 03:58
category: "Discussion"
author: "Ned Hill"
---
Shop Tip - When you need a metallic color engraving infill try metallic calligraphy inks.  Especially for gold this is my go to infill color.  I’ve tried a wide range of gold paints/paint pens/gold leaf material but this brass ink pictured gives a very vibrant and iridescent yellow gold color that I love. Not perfect in all cases but for small details it usually makes the pieces pop.  Anyone else have recommendations for go to color infill materials? #K40ShopTips 

![images/04cc2b67ca89a9e593caa99576a9ecdc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/04cc2b67ca89a9e593caa99576a9ecdc.jpeg)



**"Ned Hill"**

---
---
**Rich Sobocinski** *February 09, 2018 12:00*

Any examples to show?


---
**Ned Hill** *February 09, 2018 21:04*

**+Rich Sobocinski** pics don’t pick up iridescent properties of ink but here’s a recent thing I made. The sun and moon are colored  with the ink and it’s so much better in person. ![images/7d557c6c6959c9eba240f9129a05f120.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d557c6c6959c9eba240f9129a05f120.jpeg)


---
**Rich Sobocinski** *February 10, 2018 02:21*

**+Ned Hill** nice! Thanks. I can pick up a little of the iridescence


---
**Joel Brondos** *February 19, 2018 23:18*

**+Ned Hill** Man, you must have a steady hand to get the ink in there that precisely.


---
**Ned Hill** *February 20, 2018 00:56*

**+Joel Brondos** The background shield is masked before engraving and then I paint with the mask still on.  So I don't need to be super precise.


---
**Joel Brondos** *February 22, 2018 02:13*

The best ideas are often the simplest. What do you use for masking?


---
**Ned Hill** *February 22, 2018 02:21*

For small things I use 3M delicate surface wide blue painters tape. For larger things I use 8” wide lettering transfer tape (Conform RTape 4075RLA)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/JaNDacGdsXD) &mdash; content and formatting may not be reliable*
