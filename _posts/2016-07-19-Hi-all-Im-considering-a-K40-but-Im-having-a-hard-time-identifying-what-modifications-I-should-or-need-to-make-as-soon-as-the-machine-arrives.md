---
layout: post
title: "Hi all - I'm considering a K40, but I'm having a hard time identifying what modifications I should (or need) to make as soon as the machine arrives"
date: July 19, 2016 15:19
category: "Modification"
author: "Darryl Kegg"
---
Hi all - I'm considering a K40, but I'm having a hard time identifying what modifications I should (or need) to make as soon as the machine arrives.  I've seen references to air assist, bed changes, board changes etc... but nothing comprehensive that really ranks what I need to do versus what can really be optional.  Thanks in advance.





**"Darryl Kegg"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 15:20*

Air assist, board. I did those immediately after getting my machine. 

Disclosure: I make a smoothie powered board that is a drop in upgrade for the K40 and we are getting ready for to mass manufacture it. 


---
**greg greene** *July 19, 2016 15:31*

Depends on what you want it for, most work great for engraving right out of the box.  The air assist head/mirror/18mm focus lens setup from Light Objects gives you a finer beam for intricate work and the option to use larger pieces as your work piece can be higher utilising more of the working area. The smoothjie board will allow use of different software and hopefully at some point - control over beam strength (Z Axis) for really outstanding engraving.  The problem with that is the tube they use is not really usable for PWM control. New advances happen every day it seems so watch this list and when someone has  a working setup you like you can duplicate it.


---
**Darryl Kegg** *July 19, 2016 15:37*

**+Ray Kholodovsky**  - so air assist is designed to cool the point where the laser hits the material?   Does the board upgrade allow use of GRBL style controls, or is the software still 100% proprietary? 


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 15:40*

The smoothie upgrade lets you use any gcode from places such as LaserWeb, Pronterface, or any of the other open softwares.


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 15:40*

The air assist is so that the work piece does not start burning where the laser hits it.


---
**Ariel Yahni (UniKpty)** *July 19, 2016 15:41*

**+Darryl Kegg**​air assist will: put out flames, provide more oxygen for better cuts


---
**Jim Hatch** *July 19, 2016 15:45*

The short answer is none. The K40 once setup and aligned will suffice for most users.



The long answer is it depends on what you plan to use it for. It also depends on who is going to use it. For instance, if it's just you using it then a lid interlock that will cut power to the laser when the lid is opened is a nice add on. If your kids are going to use it then the interlock goes to the top of the list.



You can divide up the normal mods into ones that are safety oriented, performance oriented or workflow.



Safety options include the lid interlock and a water sensor cutoff in case your water pump fails. 



Performance mods include things like air assist, new lens, new mirrors, removal of the standard exhaust manifold, truing up the rails to be square, level and parallel to the laser tube and even the addition of intake fans and interior lighting. Removing the stock material bed and replacing it with a larger one and/or a height adjustable bed fall into this area as well.



Workflow mods are those that let you work outside the CorelLaser limits that their OEM controller impose. A new controller package (like Ray or Scott's) give you plug & play replacement of the stock Nano board and allow you to run other software like Peter's LaserWeb. That gives you more control over the files you're cutting as well as streamlining the process by cutting out some intermediary steps required to do complex work through CorelLaser. (No reason at at to use LaserDRW that came with the machine in any case.)



But, the K40 is very capable out of the box and if you're new to lasers you may want to focus on using what you have and then picking upgrades that provide you with the most value. 






---
*Imported from [Google+](https://plus.google.com/104543549241847780442/posts/RhHt7UhASUW) &mdash; content and formatting may not be reliable*
