---
layout: post
title: "Why wheels??? I just received my unit last Friday, but why would they upgrade the feet with wheels?"
date: August 16, 2017 03:26
category: "Discussion"
author: "Printin Addiction"
---
Why wheels???



I just received my unit last Friday, but why would they upgrade the feet with wheels? Doesn't make sense to me. I didnt want to modify the unit too much before I made sure it worked well, so I designed these, and printed them in flexible TPU. The unit is pretty stable now.





![images/aac9a8e16dd2c048ea8d7f1f1c4ab236.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aac9a8e16dd2c048ea8d7f1f1c4ab236.jpeg)



**"Printin Addiction"**

---
---
**Printin Addiction** *August 16, 2017 09:54*

I was amazed that I had such a close color match to the unit in flexible TPU, thanks maker geeks.


---
**Don Kleinschnitz Jr.** *August 16, 2017 11:21*

I  am trying to think of a practical use for a rolling K40?


---
**Ned Hill** *August 17, 2017 15:26*

Ehh...Sometimes it's helpful if something rolls underneath. But other than that it's kind of pointless.


---
*Imported from [Google+](https://plus.google.com/+PrintinAddiction/posts/adDpqa4L6qa) &mdash; content and formatting may not be reliable*
