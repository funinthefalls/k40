---
layout: post
title: "Please Help! I have a new K40 with a laser firing problem"
date: January 29, 2018 16:40
category: "Original software and hardware issues"
author: "Tim Robertson"
---
Please Help! 

I have a new K40 with a laser firing problem.



- The laser will test fire only from the power supply and only when the digital power controller is disconnected from the power supply (see red arrow)



- I have replaced the Moshi control board with a new one and the problem is the same.



- It has a flow switch that is closing and functioning properly.

_ All continuity is good on the ribbon cable.



Please advise.



You can also email to robertson_tw@hotmail.com with subject: K40



Thanks,



Tim



![images/ad438e83c5cbd51249178e12878ed8ea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad438e83c5cbd51249178e12878ed8ea.jpeg)



**"Tim Robertson"**

---
---
**Don Kleinschnitz Jr.** *January 29, 2018 19:08*

I suspect you have a bad control panel which. You can try for a new one or many are replacing the panel with the pot. 

I can walk you through measurements to further prove the panel is bad.


---
**Tim Robertson** *February 05, 2018 14:17*

I've replaced the control board and problem remains the same. Ive found a schematic and will review the connections soon. 


---
**Don Kleinschnitz Jr.** *February 05, 2018 15:57*

**+Tim Robertson** by "control board" do you mean the control panel. If you have a schematic of that control panel board can you share? 



Does the laser fire with the "Test" button on the panel??



Something from the control panel is keeping the LPS from being enabled to fire.



Schematic for your LPS:

[https://www.digikey.com/schemeit/project/k40-lps-2-EFKO7C8303M0/](https://www.digikey.com/schemeit/project/k40-lps-2-EFKO7C8303M0/)



Here is a testing sheet I created that may help you.

I suggest using it to test if you have the right voltages down on the LPS during operation.



Note: grounding some signals on the LPS will result in the laser firing so use eye protection.

![images/b04d4d3de57814e260b9b02a1b1476dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b04d4d3de57814e260b9b02a1b1476dc.jpeg)


---
**Tim Robertson** *February 05, 2018 17:19*

**+Don Kleinschnitz**  

Hi Don, 

Yes it is the digital control panel with the push buttons. It's actually just a wiring connections diagram but atleast I can confirm proper connectons. I also like the idea of going to a potentiometer. Thanks for your support!

![images/2ddb16383816807f5744151d86d187d0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2ddb16383816807f5744151d86d187d0.jpeg)


---
**Tim Robertson** *February 05, 2018 17:29*

**+Tim Robertson** 

The laser will nerver test fire at the control panel, the LED's come on, laser enable and power adjust buttons function, digits change with power adjustments.   All appears normal accpted for test fire at the panel and test fire being dissabled at the power supply when panel is connected. Thanks again!


---
**Tim Robertson** *February 05, 2018 19:26*

![images/359b29f6fc545e40cbe11e0c2b1ef825.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/359b29f6fc545e40cbe11e0c2b1ef825.jpeg)


---
**Don Kleinschnitz Jr.** *February 05, 2018 20:31*

**+Tim Robertson** thanks for making it pretty :)!



In your schematic it shows that P is not connected?!!!



Did you check the voltages according to the table?



Does your machine have a separate 24VDC supply?


---
**Aart A** *August 13, 2018 13:03*

Hi Tim,



I have a same issue, grab a brand new crappy chineeeese piece of s*t k40. (This was a total waist of money - to dealing with k40 seems only for masochist). Set up the optical pathway as the manufacturers set up was actually dangerous, and fireing only in test mode. Now Im go through many stages, and finally in direct contact with the manufacturer what a hell is there. I bet the Power unit cause the issue: there are a 4 connectors link between the Power unit and the Control Board. I measured a solid 8 Volts at the GND and L /the triggering signal port/ when NO program runs, and a jumping 9ish Volts when a program running. It seems the Control Board working well, generate the triggering signal to drive the laser tube. And the XY movements of the head are perfect. I believe the issue is the Power unit high initial 8 Volts potential on the L port. I just waiting for the manufacturers response. 


---
*Imported from [Google+](https://plus.google.com/112357784892177881137/posts/HjZfFKyDMEH) &mdash; content and formatting may not be reliable*
