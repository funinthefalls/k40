---
layout: post
title: "Originally shared by Todd Fleming Laser Cut and Mill Cut now support open paths"
date: December 03, 2016 19:53
category: "Software"
author: "Anthony Bolgar"
---
<b>Originally shared by Todd Fleming</b>



Laser Cut and Mill Cut now support open paths. It uses a greedy algorithm to order the cuts. Other operations ignore open paths.

![images/455694d959ededbfd918bc77042a563b.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/455694d959ededbfd918bc77042a563b.gif)



**"Anthony Bolgar"**

---
---
**Jonathan Davis (Leo Lion)** *December 03, 2016 20:05*

Looks like i have a update to do.


---
**Jonathan Davis (Leo Lion)** *December 03, 2016 21:11*

**+Peter van der Walt** always useful.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/ZqBPQmCsiWK) &mdash; content and formatting may not be reliable*
