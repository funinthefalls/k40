---
layout: post
title: "Using a dollar store square and nails to make a registration fixture"
date: July 31, 2018 13:01
category: "Modification"
author: "HalfNormal"
---
Using a dollar store square and nails to make a registration fixture.



![images/83f6c8bb7335a43b529ebc0e3792e163.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/83f6c8bb7335a43b529ebc0e3792e163.jpeg)



**"HalfNormal"**

---
---
**Thor Johnson** *July 31, 2018 16:26*

Nice.  What I did was put 2 dots on the top of anything I wanted to be able to reposition (eg, a jig for holding plastic pieces to be engraved), then used a red pointer ([https://www.thingiverse.com/thing:1002341](https://www.thingiverse.com/thing:1002341)), and manually move the  jig so it lined up with the "burn dots."

[thingiverse.com - Laser pointer for K40 Chinese CO2 Laser by ThorMJ](https://www.thingiverse.com/thing:1002341)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/ZFm3ujBynRm) &mdash; content and formatting may not be reliable*
