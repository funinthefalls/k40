---
layout: post
title: "What's the possibility of increasing the bed size on a K40 by taking out all of the components and making a new set of rails with something like Makerslide?"
date: August 22, 2016 17:54
category: "Modification"
author: "Chris Sader"
---
What's the possibility of increasing the bed size on a K40 by taking out all of the components and making a new set of rails with something like Makerslide?





**"Chris Sader"**

---
---
**Thomas Dudley** *August 22, 2016 18:09*

I've been wondering exactly this - the price is good for a 40w tube, steppers & power supply. It's say the possibility is possible, but the technical end is still intimidating. I know there are Instructables about making engravers, and controllers made on arduino/rasperry pi boards.


---
**Ariel Yahni (UniKpty)** *August 22, 2016 18:11*

Yes it is, working on it myself


---
**Eric Flynn** *August 22, 2016 18:44*

Yes, very doable. Not going to buy a whole lot of space though if using the stock enclosure.


---
**Thomas Dudley** *August 22, 2016 19:21*

But once could potentially build a new enclosure with a Z-table but use the existing hardware (minus enclosure) & new board?


---
**Ariel Yahni (UniKpty)** *August 22, 2016 19:31*

**+Thomas Dudley**​ yes you can


---
**Chris Sader** *August 22, 2016 21:01*

**+Ariel Yahni** any details you can share?


---
**Ariel Yahni (UniKpty)** *August 22, 2016 21:04*

**+Chris Sader**​ I'm first doing all the parts and testing them outside, then will work on getting them in. I'm doing as I'm moving forward. Lots of desitions to make


---
**Chris Sader** *August 22, 2016 21:19*

Anybody know what the largest x and y size you could go with stock mirrors and lens?


---
**Anthony Bolgar** *August 22, 2016 21:20*

I have a work area of 330mm X 230mm with the stock setup, just removed the exhaust vent.


---
**Alex Krause** *August 22, 2016 21:59*

I've seen an upgrade done using Openbuilds v-slot which looked pretty awesome it requires removing the electronics partition inside the machine and relocating the power supply... but the over all cutting area was about 500mmx200mm give or take a few mm. I would highly recommend using Openbuilds hardware instead of makerslide ;) ;) ;)


---
**Chris Sader** *August 22, 2016 23:39*

**+Alex Krause**​ link?


---
**Chris Sader** *August 22, 2016 23:40*

**+Anthony Bolgar**​ I removed my vent last night. Haven't tried cutting with it like that yet, but I imagine it'll be fine. Nice to have that bit of usable cutting area back.


---
**Eric Flynn** *August 23, 2016 00:01*

There are several  documented laser builds out there.  Any of them would be a fine candidate for a transplant of the K40 guts.  Look at the Buildlog.net machine.


---
**Alex Krause** *August 23, 2016 00:47*

[https://plus.google.com/+ArielYahni/posts/ZJHS6hb2PeM](https://plus.google.com/+ArielYahni/posts/ZJHS6hb2PeM)


---
**Robi Akerley-McKee** *August 23, 2016 05:02*

I just trimmed the hose on my y guide rod and just gained 10mm ish of travel. So now I'm to 327mm x 225mm ish.  Had an issue compiling marlin, i'll be fixing that Wednesday.  Also added the front lid interlock switch as well today.  Working on the 3040 cnc tomorrow.


---
*Imported from [Google+](https://plus.google.com/107747838010116449754/posts/XN9ciJ4ybBy) &mdash; content and formatting may not be reliable*
