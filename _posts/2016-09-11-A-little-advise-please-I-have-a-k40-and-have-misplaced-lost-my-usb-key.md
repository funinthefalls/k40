---
layout: post
title: "A little advise please.. I have a k40 and have misplaced (lost) my usb key.."
date: September 11, 2016 03:40
category: "Hardware and Laser settings"
author: "Chris Boggs"
---
A little advise please..  I have a k40 and have misplaced (lost) my usb key..  Is there a source to purchase a replacements..  Any advise would be greatly appreciated..  





**"Chris Boggs"**

---
---
**Robi Akerley-McKee** *September 11, 2016 05:36*

search ebay, I have seen them there...  you could also upgrade the controller board to something else like a smoothieboard or arduino+RAMPS 1.4




---
**J.R. Sharp** *September 11, 2016 05:36*

Upgrade board. God has told you to do so by hiding your key.


---
**Chris Boggs** *September 11, 2016 12:16*

I just purchased this unit..  And am working on selling my K40.. But I need that key..  I'll check eBay also.. 

![images/1cd1a7832393cf1d6a41f4300c8b7625.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1cd1a7832393cf1d6a41f4300c8b7625.png)


---
**Jim Hatch** *September 11, 2016 13:40*

Check one of the folks who have done a Smoothie upgrade. Either here or on one of the Smoothie boards.


---
**Chris Boggs** *September 11, 2016 14:30*

Great idea.. Thanks Jim


---
**Scott Marshall** *September 11, 2016 18:47*

+1 on ebay.



 In the last day or 2 I was surprised to see some place selling JUST the keys - about 30bucks If I remember correctly. There was also an M2nano board & key set for about 80.




---
**Sebastian Szafran** *September 11, 2016 21:57*

**+Chris Boggs**​ I have one key available. 


---
**Chris Boggs** *September 11, 2016 23:19*

**+Sebastian Szafran** that would be awesome..  I'll be glad to buy it..  Are you in the States?  


---
**Sebastian Szafran** *September 12, 2016 17:09*

**+Chris Boggs**​ I've sent you a message on Hangouts.


---
**Chris Boggs** *September 12, 2016 22:19*

**+Sebastian Szafran** just found mine..  Thanks again for being willing to assist 


---
*Imported from [Google+](https://plus.google.com/101316203748949165087/posts/VUEFx6xtY9Z) &mdash; content and formatting may not be reliable*
