---
layout: post
title: "I changed the main board of my K40 because it burned ."
date: July 24, 2017 21:12
category: "Hardware and Laser settings"
author: "Jose Salatino"
---
I changed the main board of my K40 because it burned .

The engines work fine, but the software (corel), asks for the new Device ID. 

Anyone know where to find the ID of the new board? Thanks.





**"Jose Salatino"**

---
---
**Jose Salatino** *July 25, 2017 08:49*

No hay algún software que me permita ver el id de la placa nueva?


---
**Jose Salatino** *July 25, 2017 08:52*

La otra duda es si la llave usb que tiene solo acepte la placa vieja, y si es asi no habría nada que hacer. A no ser que haya un software que me permita cambiar el id de la placa en la llave Usb


---
**Ariel Yahni (UniKpty)** *July 25, 2017 09:23*

ahora no tengo la otra placa a mano pero cada placa trae el device ID pegado afuera. No se si una llave puede funcionar con otra placa.



Podes probar este software que funciona con algunas palcas originales y no necesita llave [scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)﻿


---
*Imported from [Google+](https://plus.google.com/110470219898054181794/posts/R195Vn4enDK) &mdash; content and formatting may not be reliable*
