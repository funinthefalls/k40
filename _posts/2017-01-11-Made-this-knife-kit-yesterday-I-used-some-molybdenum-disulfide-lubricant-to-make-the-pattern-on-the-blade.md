---
layout: post
title: "Made this knife kit yesterday. I used some molybdenum disulfide lubricant to make the pattern on the blade"
date: January 11, 2017 13:19
category: "Object produced with laser"
author: "Niels Sorensen"
---
Made this knife kit yesterday.  I used some molybdenum disulfide lubricant to make the pattern on the blade.  The K40 doesn't quite get the material hot enough to form a solid layer but did manage to lay down a durable pattern.  I scrubbed it with a green abrasive pad and acetone.  Settings were 2ms at 18mW



[https://www.amazon.com/gp/product/B0013J62P4/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B0013J62P4/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1)



![images/25ca74b189e389a21c43ce40df999610.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/25ca74b189e389a21c43ce40df999610.jpeg)
![images/15e71c79f0be8c76201fe44afdc2ed1d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15e71c79f0be8c76201fe44afdc2ed1d.jpeg)

**"Niels Sorensen"**

---
---
**Bob Damato** *January 11, 2017 19:37*

That looks great. I have found that more than one pass darkens it up quite a bit when using that spray moly


---
*Imported from [Google+](https://plus.google.com/113412478181800917088/posts/39WCwzvvBrL) &mdash; content and formatting may not be reliable*
