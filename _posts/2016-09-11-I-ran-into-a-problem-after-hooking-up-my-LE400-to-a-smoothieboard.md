---
layout: post
title: "I ran into a problem after hooking up my LE400 to a smoothieboard"
date: September 11, 2016 10:26
category: "Discussion"
author: "Anthony Bolgar"
---
I ran into a problem after hooking up my LE400 to a smoothieboard. One of the stepper motors is dead. It is a dual shaft Nema 17 motor, but the shafts are very long, one side is 20mm, the other is 30mm. The longest I can seem to find is 13 on one side and 19on the other. Anyone know where I can find something close to this? I looked for the model number online, it is a 42BYG112LST by KDE Mtor, could not find any info on it at all. Is there a sloution to my replacement issue, any way of using the available dual shaft steppers with the shorter shafts? Any suggestion is welcome.





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *September 11, 2016 10:29*

Would it be possible to use a solid coupler and 5mm steel rod to extend the shafts?


---
**Phillip Conroy** *September 11, 2016 10:52*

Try here    [http://kde-motor.com/e42cp2.htm](http://kde-motor.com/e42cp2.htm)


---
**Anthony Bolgar** *September 11, 2016 11:25*

Thanks Philip. I sent them a message, but as they are the manufacturer, I do not see them selling me just 1 PC. But there may be a chance that they can direct me to a distributor that will. I have figured out another option, RobotDigg.com has a dual shaft motor that will work with a little bit of effort, the one shaft is correct length, the other is 12mm short of what I need, but the body is 10mm larger than the original, so I am only out by 2mm, I think I can make it work. If not, I can use two stepper motors if I make a mount bracket for the second one (Not my first choice, but it is possible.) BTW, are you still interested in the K40 tube hangars?


---
**Anthony Bolgar** *September 11, 2016 20:21*

I figured out an easy way to use two separate motors, so I ordered a nice pair fro RobotDigg. Did not want to pay 30 for express shipping so this project is now on hold for the next few weeks until the motors and MXL pulleys arrive. Not a big deal, I have enough other projects on the go to keep me busy until judgement day.


---
**Anthony Bolgar** *September 13, 2016 23:57*

So I was cleaning up my work area and out of the corner of my eye, I spotted my rotary attachment I made for the laser cutter. And on this little rotary sits a Nema 17 motor, so now I do not have to wait for the ones from Robotdigg to arrive. Both of the motors on this axis will be identical models so I do not envision having any problems using them. I am just happy I don't have to delay finishing this project.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/QaHGVESZGz1) &mdash; content and formatting may not be reliable*
