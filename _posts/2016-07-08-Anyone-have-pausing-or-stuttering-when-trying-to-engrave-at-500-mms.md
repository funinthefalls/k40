---
layout: post
title: "Anyone have pausing or stuttering when trying to engrave at 500 mms?"
date: July 08, 2016 22:18
category: "Discussion"
author: "Tanner Wessel"
---
Anyone have pausing or stuttering when trying to engrave at 500 mms?





**"Tanner Wessel"**

---
---
**Alex Krause** *July 08, 2016 23:06*

[https://plus.google.com/109807573626040317972/posts/4n3q8Mry9df](https://plus.google.com/109807573626040317972/posts/4n3q8Mry9df)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 09, 2016 02:27*

I did a couple of times & I found it also jumping sometimes. But that was due to belt tension. The pausing happened only of engraves with a lot going on.


---
*Imported from [Google+](https://plus.google.com/107121030507076192902/posts/Drqx3cG5kyf) &mdash; content and formatting may not be reliable*
