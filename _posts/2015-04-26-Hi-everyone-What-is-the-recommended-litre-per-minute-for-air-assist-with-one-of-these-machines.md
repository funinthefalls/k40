---
layout: post
title: "Hi everyone, What is the recommended litre per minute for air assist with one of these machines"
date: April 26, 2015 21:41
category: "Hardware and Laser settings"
author: "David Wakely"
---
Hi everyone,



What is the recommended litre per minute for air assist with one of these machines. I have a 40lpm air pump but i don't think its man enough and i sometimes struggle to cut through ply.



Thanks





**"David Wakely"**

---
---
**Sean Cherven** *April 26, 2015 22:10*

Good question. I wanna know this also


---
**Jon Bruno** *April 27, 2015 13:51*

Me too.. Up to now I've just been feeding it air from a small compressor and adjusting it to "that looks good". the compressor scares the bejesus out of me every time it kicks on so a smaller air pump would be a welcome change.

Also what air assist nozzle are people using? I printed one from thingiverse but I'm not sure it's close enough to the work...and it hits the exhaust vent shroud when homing. 


---
**David Wakely** *April 27, 2015 18:40*

I'm using a 3d printed one from eBay but I have a nice one on order from light object. Just gotta wait until it arrives from the USA.



I'm hoping my 40lpm pump is enough but I may try a compressor to see if I get a better result.



I'll keep you posted on how it goes.



I've also got an arduino based water flow and temperature monitor with LCD. I'll post completed pics soon


---
**Jon Bruno** *April 28, 2015 17:07*

cool!


---
**Jon Bruno** *April 30, 2015 11:17*

Last night I removed my 'big' compressor and installed a small GAST rotary vacuum pump I used to use for screen printing. I hooked the output side to my air assist and all I can say is wow! This unit is much quieter and produces a silly volume of air stream.

I don't know what the recommended volume is for cutting but this feels like a nice solid not too forceful column.

Just an update on my side. :-)


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/PtCP9uDNN2A) &mdash; content and formatting may not be reliable*
