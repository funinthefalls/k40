---
layout: post
title: "I still have a way to go...."
date: May 14, 2016 00:52
category: "Discussion"
author: "Todd Miller"
---
I still have a way to go....

![images/785d6ad361d4a369d64c89745171ca6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/785d6ad361d4a369d64c89745171ca6a.jpeg)



**"Todd Miller"**

---
---
**Ariel Yahni (UniKpty)** *May 14, 2016 00:58*

When I grow up I wana be just like you


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 14, 2016 01:46*

I want it! Looking good. Is that a vacuum bed I see in the middle?


---
**Ariel Yahni (UniKpty)** *May 14, 2016 02:07*

**+Yuusuf Sallahuddin**​ look at the amazing Z axis below. 


---
**Todd Miller** *May 14, 2016 02:08*

I hope it acts like a vacuum bed :-)



I'm "venting" the bed using two Rule model 240 marine 4 inch in-line bilge blowers.   I don't know if it's enough, that's last on my list.



I want to lase first !


---
**Sebastian C** *May 14, 2016 08:43*

Great! Is this your own construction or something like lasersaur?


---
**Todd Miller** *May 14, 2016 23:19*

Assembled with parts purchased from LightObject.com  Not really a kit, all DIY.


---
**John-Paul Hopman** *May 15, 2016 19:53*

I have been wondering, it costs $400ish for a 12" x 8", 40W laser. Then you want to upgrade the controller to something that doesn't suck, you have to customize / jury rig the air vent, add a air pressure line to head, etc... If you want a more powerful laser, you have to modify the case for it to fit, larger cutting platform, scrap the case, and the case itself already seems to have a lot of wasted space.



So my question is, would it be better to buy a cheap printer and slowly upgrade it, reusing as much as you can, or just build a larger printer from scratch that would allow you to upgrade to a larger laser without first wasting cash on the 30-40W laser that comes with the cutter?


---
**Ariel Yahni (UniKpty)** *May 15, 2016 20:59*

Well the tube and psu are just almost at the cost of the machine itself. A 60w tu can cost you almost the same as the entry Chinese machine


---
**Andrew ONeal (Andy-drew)** *May 19, 2016 05:57*

Man I saw one but it was a bit larger I believe, but the guy built the gantry against the wall and had tube vertical instead of horizontal to save space. It's online somewhere, anyways awesome build man.


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/jSb4MiFyGy4) &mdash; content and formatting may not be reliable*
