---
layout: post
title: "Got one last K40 middleman board to give away for free"
date: July 11, 2016 17:29
category: "Modification"
author: "Jean-Baptiste Passant"
---
Got one last K40 middleman board to give away for free.



First one went to **+Jim Hatch** , this one was soldered and desoldered, it's not brand new but it works and it's free.



If anyone need it, just give me your adress privately and it will be sent in the next few days.



First come first served.





**"Jean-Baptiste Passant"**

---
---
**Joe Alexander** *July 11, 2016 17:55*

I am definitely interested, yet cannot seem to message you direct... kinda sad considering I tend to be IT for most my jobs.


---
**Jean-Baptiste Passant** *July 11, 2016 17:58*

**+Joe** , sent you a hangout for you. to give me your adress ;)


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/PBGry59FokX) &mdash; content and formatting may not be reliable*
