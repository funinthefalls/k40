---
layout: post
title: "Stephane Buisson here are the photos you requested"
date: July 23, 2015 22:34
category: "Hardware and Laser settings"
author: "Gee Willikers"
---
**+Stephane Buisson** here are the photos you requested. If these aren't adequate let me know.



![images/c4b158b673cc31671a0c6c117d4992ca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4b158b673cc31671a0c6c117d4992ca.jpeg)
![images/85ad933383dcc4fd5b4c0e952ba993a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/85ad933383dcc4fd5b4c0e952ba993a8.jpeg)
![images/b9a498f4086fa93dce53fd18ad17b20d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b9a498f4086fa93dce53fd18ad17b20d.jpeg)
![images/8a5120b8cc26a1413101789a865c7abf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a5120b8cc26a1413101789a865c7abf.jpeg)
![images/55d0c6505c52c0f90d9e864493fff85c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55d0c6505c52c0f90d9e864493fff85c.jpeg)
![images/0a5e5a95f2ae2b5edfad04f33bfe80bf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a5e5a95f2ae2b5edfad04f33bfe80bf.jpeg)
![images/d707f0075e132c11a0933986eeb90891.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d707f0075e132c11a0933986eeb90891.jpeg)
![images/56a32b18e06e6f757eb75f1ec3cf9e5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/56a32b18e06e6f757eb75f1ec3cf9e5b.jpeg)

**"Gee Willikers"**

---
---
**Stephane Buisson** *July 24, 2015 14:08*

Thank you Jeff.

it's clear about the Moshi board, 

less about the laser PSU (different to mine), for potential upgrade, you (or other member) will need to know if those PSU could trigger the laser via PWM.

Inscription in chinese don't help to identify and find out for your exact model.


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/4ukGJSbqHby) &mdash; content and formatting may not be reliable*
