---
layout: post
title: "LAYZOR plans now available! The LAYZOR project has been dominating all my free time since the beginning of the year"
date: September 01, 2018 13:15
category: "External links&#x3a; Blog, forum, etc"
author: "Frederik Deryckere"
---
LAYZOR plans now available!



The LAYZOR project has been dominating all my free time since the beginning of the year. This post marks another milestone in this crazy journey.



So what happenend? Well I finally finished the complete plans for converting any K40 to the LAYZOR. I'm very satisfied with the result and I actually finished it a few weeks earlier than anticipated, so I'm giving myself a golden medal for that.



The plans are for sale over here: [https://manmademayhem.com/layzor/myshop/#forsale](https://manmademayhem.com/layzor/myshop/#forsale)



Cheers!





**"Frederik Deryckere"**

---
---
**Don Kleinschnitz Jr.** *September 01, 2018 14:34*

You certainly deserve a Gold Metal....!


---
**Don Kleinschnitz Jr.** *September 01, 2018 14:36*

Note: not to diminish your work which is excellent, you may find that the cost of those plans ($32) are a bit high ;).


---
**Frederik Deryckere** *September 01, 2018 16:17*

Thanks Don! Yeah it's a difficult choice. If it weren't for paypal taking their generous part... I talked it over and you're right. Back to 20€ it is!


---
**Dennis Fuente** *September 02, 2018 22:06*

any chance you could make the plans Rhino 4.0 compatible 


---
**Frederik Deryckere** *September 03, 2018 11:41*

Sure.

[mediafire.com - LAYZOR v1.1 Rhino 4.zip](http://www.mediafire.com/file/2vzlqhq2njf0k6y/LAYZOR_v1.1_Rhino_4.zip/file)

Let me know if it works for you. If so, I'll add it to my site. Thanks!


---
**Dennis Fuente** *September 03, 2018 16:37*

**+Frederik Deryckere** Hi Frederik thanks for the Rhino file but i could not get it to open


---
**Frederik Deryckere** *September 03, 2018 17:01*

Hmm strange, maybe you can download a Rhino5 demo version and try to open it that way?


---
**Dennis Fuente** *September 03, 2018 20:45*

ya it's real strange because i can see a preview but nothing shows in the 4 view panels,  is it a DXF or just a Rhino 3DM thanks


---
**Frederik Deryckere** *September 03, 2018 20:53*

It's a Rhino .3DM file. I exported it as Rhino 4 legacy file and it gave me some warnings but nothing breaking the model. I then opened that file in Rhino 5 and that worked for me. Do you see layers in the layer panel?


---
**Dennis Fuente** *September 05, 2018 16:04*

it states no filtered layers to display


---
**Frederik Deryckere** *September 07, 2018 10:42*

My best guess is that some machine parts that are off-the-shelf components were downloaded from grabcad and imported in Rhino. I didn't model everything myself obviously. So maybe some of those imported models don't play well with older versions of Rhino. idk it's just a guess at this point




---
**Dennis Fuente** *September 07, 2018 16:40*

could be i will paly around with it some more and let you know if i have any success  


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/VtVC9ernFM7) &mdash; content and formatting may not be reliable*
