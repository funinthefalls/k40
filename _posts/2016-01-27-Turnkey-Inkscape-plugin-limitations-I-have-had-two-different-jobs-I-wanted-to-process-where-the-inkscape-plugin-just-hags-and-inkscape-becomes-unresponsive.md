---
layout: post
title: "Turnkey Inkscape plugin limitations ? I have had two different jobs I wanted to process where the inkscape plugin just hags and inkscape becomes unresponsive"
date: January 27, 2016 20:47
category: "Software"
author: "David Cook"
---
Turnkey Inkscape plugin limitations ?



I have had two different jobs I wanted to process where the inkscape plugin just hags and inkscape becomes unresponsive.



are there know limitations for this plugin?  such as limited amount of objects,  etc ?



I tried laser etching a paragraph of text  all converted to paths and it would not export.  If I only selected two lines of the text it would work fine.



I also tried cutting a small quadcopter frame  and it would not export,   about 165 objects selected.   now when I used the "combine" command in Inkscape to join ALL of the perimiter outlines together  then it was able to export.



( Yes Peter I do plan on moving to Laser Web :-)  I just want to understand the inkscape plugin a bit more.





**"David Cook"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 28, 2016 03:00*

I don't use Inkscape, however your description makes me think it is possibly a RAM limitation. How much RAM is the system running?


---
**David Cook** *January 28, 2016 04:23*

**+Yuusuf Sallahuddin** I thought so to. It's an Asus g1S gaming laptop with 4gb ram, 64 bit win 7 etc.. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 28, 2016 05:42*

**+David Cook** I am running 16gb ram with my system & I notice that photoshop/illustrator can chew a lot of memory the bigger the file gets (some files I work with are in the 300mb range). Leads to slow, chuggy performance which would probably crash with less ram). So it is possible that when utilising large amount of objects it is using too much ram, thus hanging/crashing. It may be possible to combine all of yours lines of text together like how you said you did for the quadcopter frame & may solve your issue there.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/CUyqjQXTiWB) &mdash; content and formatting may not be reliable*
