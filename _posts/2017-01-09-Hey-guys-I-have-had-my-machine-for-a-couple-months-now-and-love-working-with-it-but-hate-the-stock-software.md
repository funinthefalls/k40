---
layout: post
title: "Hey guys, I have had my machine for a couple months now and love working with it, but hate the stock software"
date: January 09, 2017 12:36
category: "Software"
author: "Patrick G"
---
Hey guys, I have had my machine for a couple months now and love working with it, but hate the stock software. Does anyone in the community have any Corel Lazer tutorials that they can share with me to make the software more user friendly? 





**"Patrick G"**

---
---
**Ariel Yahni (UniKpty)** *January 09, 2017 12:52*

**+Patrick G** The easiest use path is to update the controller of the machine and use LaserWeb. In case you are not up to it at the edn of this page there are a couple of link that could be of help 

[thebetatester.xyz - Troubleshooting](http://www.thebetatester.xyz/troubleshooting/)


---
**Jim Hatch** *January 09, 2017 13:43*

Youtube has tons of Corel tutorials. The only difference is that CorelLaser adds the toolbar icons to send the file to the laser.


---
*Imported from [Google+](https://plus.google.com/108727782554326629229/posts/cmEDT1PMMDk) &mdash; content and formatting may not be reliable*
