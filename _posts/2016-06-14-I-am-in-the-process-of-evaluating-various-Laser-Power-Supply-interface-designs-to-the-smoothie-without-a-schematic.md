---
layout: post
title: "I am in the process of evaluating various Laser Power Supply interface designs to the smoothie without a schematic"
date: June 14, 2016 14:01
category: "Modification"
author: "Don Kleinschnitz Jr."
---
I am in the process of evaluating various Laser Power Supply interface designs to the smoothie without a schematic.

1. If you have found a schematic would you please send me a link.

2. If you have a dead LPS ( MYJG40W) that you are willing to donate so that I can take it apart and map out the interface I would be happy to pay reasonable shipping. I live in the US at 84092.



I think having a better idea of what is under the covers would help us all.







**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 16:19*

I'll keep you in mind if mine dies anytime in the near future.


---
**Vince Lee** *June 14, 2016 23:02*

Are you interested in how the PSU works, or just the pinouts?  FWIW, I read somewhere early on that on my PSU, the two enable lines each ground an optoisolator, which makes them pretty safe to control.


---
**Don Kleinschnitz Jr.** *June 15, 2016 12:35*

I would like to model the AC and DC characteristics of the inputs. There are lots of questions in my mind like the response of the enable signals TH/TL to a PWM input and the reaction of the IN signal to PWM & a pot that creates a DC offset. I won't be happy with the level shifter connected to the IN signal that we are all using until I understand how the power supply's interface works :). 

What type of setup do you have to your laser power?


---
**Vince Lee** *June 16, 2016 19:08*

**+Don Kleinschnitz** I am PWM controlling the TL (or TH?) signal and leaving the IN connected to the pot.  I did this in part because I made a switchable adapter board that lets me go back to the Moshiboard, and it was easiest to control the PSU with the same inputs.  I don't need a level shifter but am buffered by an on-board mosfet on my AZSMZ mini.  It works well now that I've tuned the PWM frequency to give me a smooth grayscale ramp. I'm guessing it works only because of internal capacitance/inductance of the power supply, since TL and TH are digital inputs which I normally wouldn't expect to be PWM-controllable.  I'd also be interested in definitively knowing what is inside the PSU, however.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/P1uPSTc9sAq) &mdash; content and formatting may not be reliable*
