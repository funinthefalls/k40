---
layout: post
title: "Great and Cheap gift idea! Down the rabbit hole I am going to take you!"
date: May 08, 2016 17:13
category: "Object produced with laser"
author: "HalfNormal"
---
Great and Cheap gift idea!



Down the rabbit hole I am going to take you!

Take a wooden spoon and add some graphics!

Do a search for wood burning spoon patterns and be prepared to spend days seeing all the creativity. Here is a site that just specializes in the item.

[https://www.etsy.com/market/wood_burned_spoons](https://www.etsy.com/market/wood_burned_spoons)

![images/4c50707e00783ca940d0ad604f06f7f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c50707e00783ca940d0ad604f06f7f2.jpeg)



**"HalfNormal"**

---
---
**I Laser** *May 08, 2016 20:06*

Possibly a nice niche for someone :)



Though most of them look like they're done by hand. I looked at paper cut a while back. Could do a much better product with a laser but people seemed to shy from something that wasn't hand produced for some reason :\


---
**HalfNormal** *May 08, 2016 20:54*

**+I Laser** I was thinking for a last minute gift idea. Quick and easy to make personal. I found a dozen 12" spoons for $6.99


---
**I Laser** *May 08, 2016 23:12*

Go for it, a small outlay to see whether or not it works.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/7eMPt6NN2WU) &mdash; content and formatting may not be reliable*
