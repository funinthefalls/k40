---
layout: post
title: "If anyone looking for inspiration - here's how I solved my adjustable bed"
date: March 19, 2018 14:35
category: "Modification"
author: "Peter Alfoldi"
---
If anyone looking for inspiration - here's how I solved my adjustable bed. Haven't seen it anywhere else.

 Height is adjusted by 4 screws fixed/secured to the bottom of the frame. Nuts, are embedded into 3D printed part which protrude thru a flanged bearing so I can adjust the nut which is on the bottom, thru the top :) then the bed is a simple piece of MDF with nails.

![images/fbf56200e153fd80adc4ac1c2cbb5650.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fbf56200e153fd80adc4ac1c2cbb5650.jpeg)



**"Peter Alfoldi"**

---
---
**HalfNormal** *March 19, 2018 15:10*

There is a similar one out there that all the adjustments are connected with a belt. I made one myself.


---
**Peter Alfoldi** *March 19, 2018 15:37*

**+HalfNormal** Do you have a picture? mine is "belt ready" too, just don't have the belt yet, so need to adjust all 4 corners the same amount :)


---
**HalfNormal** *March 19, 2018 15:43*

The picture shows when it was tried with a ball chain but it was then reprinted and a belt used.

[plus.google.com - Adjustable Bed A BIG thank you to +Kelly S for the design of the bed and +A...](https://plus.google.com/+HalfNormal/posts/PNyrev4xqS4)


---
**Peter Alfoldi** *March 19, 2018 15:53*

I see, a bit different, but similar approach :) all others I have seen have rotated the screw to raise the nut.. in your/mine design we are rotating the nut and screw is fixed.


---
**HalfNormal** *March 19, 2018 15:54*

I cannot take credit for the design.


---
**Anthony Bolgar** *March 19, 2018 17:25*

I have one where it is the nut that rotates as well. I find it easier to build this way, and less expensive.


---
**James Rivera** *March 19, 2018 17:31*

**+Peter Alfoldi** it’s hard to tell from the picture—how did you attach the nails to the MDF? Also, is that pegboard?


---
**Peter Alfoldi** *March 19, 2018 19:14*

**+James Rivera** I don't know what is pegboard :) it is a 1cm thick and strong laminated scrap wood board similar to MDF but not with such fine grit. I have pre-drilled small holes into the board and then hammered in the nails


---
*Imported from [Google+](https://plus.google.com/105128850753393988085/posts/jVhP4yaJPEd) &mdash; content and formatting may not be reliable*
