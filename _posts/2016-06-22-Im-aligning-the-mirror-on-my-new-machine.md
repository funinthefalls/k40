---
layout: post
title: "Im aligning the mirror on my new machine"
date: June 22, 2016 21:08
category: "Discussion"
author: "Christina Marie"
---
Im aligning the mirror on my new machine. I've got the first one that sits next to the tube shooting an accurate beam all the way down the Y axis rail... however the second mirror is shooting a beam more like a scattered buck shot beam.... any help?





**"Christina Marie"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 22, 2016 21:24*

Is the 2nd mirror dirty? I'd suggest having a look at that & giving it a clean off either way. Might be the cause of the scatter.


---
**Christina Marie** *June 23, 2016 01:21*

yep it had tape residue on it!


---
*Imported from [Google+](https://plus.google.com/106654713649021155154/posts/fR9pkFEiLXk) &mdash; content and formatting may not be reliable*
