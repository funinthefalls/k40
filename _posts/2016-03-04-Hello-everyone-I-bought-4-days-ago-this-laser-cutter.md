---
layout: post
title: "Hello everyone I bought 4 days ago this laser cutter ."
date: March 04, 2016 19:13
category: "Discussion"
author: "Bogdan Tudor"
---
Hello everyone I bought 4 days ago this laser cutter . Corellaser , laserdrw working very  good really impressed but today my laser tub no lighting , current indication dead , I checked the laser tube  , no crack on it , I reinstall corellaser and laserdrw  , I stick the wire direct to the ground nothing happening  .What can it be , power supply , laser tube  or both any suggestion 

Thank you 





**"Bogdan Tudor"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 04, 2016 22:04*

I'd also suggest checking your power setting (either on the potentiometer or the digital thing) is up. 


---
**Casey Cowart** *March 05, 2016 04:02*

If no power to unit at all, check hidden fuse where plug attaches to the chassis.  Small pull out tray I believe with fuse in it. 


---
**Bogdan Tudor** *March 05, 2016 11:54*

I check the laser and tester switch it's fine , potentiometer it's fine as well , power is on the software working perfect but no laser  . I can;t see any crack on the laser tube  .What else I could try ? Thank you for advice . By the way your K40 look amazing nice work **+Carl Duncan** 


---
**Bogdan Tudor** *March 05, 2016 13:42*

No fire with test switch and nothing happened when I hold on the test switch and turn around the pot paper still same 


---
**Bogdan Tudor** *March 05, 2016 14:47*

You have right , one wire I get from pot and for next  one I need to cut 

power wire  because are encapsulated between laser tube and power supply and what meter I need to have to be right  . This transformer looking well don't smell maybe don't do nothing    . I had the water hose (in and out )little bit  burn on to the end of the laser tube . Thank you 


---
**Bogdan Tudor** *March 05, 2016 15:21*

yes current meter no pot  and I measure with pot on 0 and on 30 max to pass the laser tube  right  are ages from the last measurement I think 10 years  


---
**Bogdan Tudor** *March 05, 2016 15:48*

finally power supply good laser tube bad . Thank you everyone especial **+Carl Duncan** 


---
*Imported from [Google+](https://plus.google.com/113429103226355613989/posts/2zwBPwxQmg7) &mdash; content and formatting may not be reliable*
