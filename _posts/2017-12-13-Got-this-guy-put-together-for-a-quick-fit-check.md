---
layout: post
title: "Got this guy put together for a quick fit check"
date: December 13, 2017 03:53
category: "Object produced with laser"
author: "David Allen Frantz"
---
Got this guy put together for a quick fit check. Still got a few tweaks but when it’s done I’ll have it up on thingiverse. 



![images/384a169dbca48c266c547401ef5a2ef5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/384a169dbca48c266c547401ef5a2ef5.jpeg)
![images/9e16f84aee72065bff05847356616b86.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e16f84aee72065bff05847356616b86.jpeg)

**"David Allen Frantz"**

---
---
**Don Kleinschnitz Jr.** *December 13, 2017 12:37*

Excellent thanks for sharing.


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/52YdwCwPuKt) &mdash; content and formatting may not be reliable*
