---
layout: post
title: "What makes a plywood good for laser cutting?"
date: October 22, 2017 15:18
category: "Materials and settings"
author: "HalfNormal"
---
What makes a plywood good for laser cutting?



[http://n-e-r-v-o-u-s.com/blog/?p=6042](http://n-e-r-v-o-u-s.com/blog/?p=6042)





**"HalfNormal"**

---
---
**Ned Hill** *October 22, 2017 16:29*

Very interesting.  Wasn't aware of small custom ply mfgs.


---
**Jeremie Francois** *October 23, 2017 06:35*

Ouch... "metal “glitter” is intentionally embedded in the glue, to help manufacturers identify their wood". Great find & very informative as usual from n-e-r-v-o-u-s!


---
**Mike Gallo** *December 07, 2017 13:03*

Ok, thx. Lol now I know why I see like spark when it's been cut, I was thinking that the laser hit the metal grill and cause to spark but I don't see on 1/4 thick wood. .. 


---
**Madyn3D CNC, LLC** *December 14, 2017 15:01*

A friend of mine who makes  RaspberryPi/Arduino based arcade cabinets had this very same problem with plywood. He had to import from Canada, a lower grade plywood and his business almost went under. He ultimately went with particle board, customers were pissed. 


---
**Mike Gallo** *December 14, 2017 15:07*

That's not good, in my case was the nozzle off by few mm offset , some how the screw that hold the 3 lens get loose to easy, can't over tight. I learned more info about the wood that I cut, why was sparkling while cutting.  Thx for the input. 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/6HfPzAdxZ8n) &mdash; content and formatting may not be reliable*
