---
layout: post
title: "Struggling to get my new K40 aligned"
date: January 20, 2017 22:07
category: "Original software and hardware issues"
author: "Rob Kingston"
---
Struggling to get my new K40 aligned. First mirror is pretty much bang on, maybe a little high. When aligning the second mirror, its fine when close to the tube, when moved to the other end there is between 5-10mm drift, re-adjust to be correct at that end, then when moved back to the other end it's out again!



Things I've checked:



- first mirror is defintly at 45 deg to tube

- attempted to set mirror at 45 DSG to x axis (although I don't think this is perfect)

- the frame appears to be out of allignment to the chassis so I will see if I can adjust this.



I've seen a few people mention adjusting the tube but there doesn't appear to be adjustments you can make with the standard clamps?



Cheers in advance!





**"Rob Kingston"**

---
---
**Ian C** *January 20, 2017 22:47*

Hey buddy. Welcome to the K40 :-) On a serious note it's a great machine, but takes time to align.



You do need to level and straighten the bed first, so get this as square and level as you can. On my K40 I had to lift one corner by 3mm to get it level.



With your mirror aligning, you have the fixed mirror aligned to the 2nd Y axis mirror in the nearest position but then it moves out of line. In this instance you'd manually adjust it for any nearest inaccuracies and then use the screw adjusters to bring the farthest away alignment back. So where it's out once you move the Y axis the the furthest away position, then adjust your thumb screws to bring the laser dot to the middle of the mirror.

Keep in mind manually align the mirrors for the closest points and use the thumb screws for the furthest.

The floatingwombat's alignment guide is very handy and is on here somewhere.



The tube is another thing that needs to be checked and aligned parallel to the bed/carriage in the X and Y axis. However it's not easy with no adjustment available as standard. Packing pieces are your friend here to make the best of a bad job.

There is some 3D printed adjustable tube hangars available, I printed one but then sold the K40 to make way for my X700 before I'd even tried to fit them. Mind you I'd got my K40 all aligned nicely, took ages to do, but it was working well within its capabilities. 



Ian


---
**Phillip Conroy** *January 21, 2017 01:04*

My k40 took six months to align,, Iput up with out of aalignment that long  as only needed  the top left 100mm  100mm ,so it iis notthe end of the world if not perfect first go,.iIonly got good aalignment after the 4 th try,had to loosen and pack one end of tube,ttrial and error ,try one end packing of 5 layers of Electral tape and see what happens .p'sonly move screws in pairs ie top 2 ,and side 2 


---
**Ned Hill** *January 21, 2017 01:29*

The Floating Wombat guide is in the External Links section if you need it.


---
**Rob Kingston** *January 21, 2017 20:03*

Thanks for the pointers guys, going to have another crack at this tomorrow, when people are aligning the bed and the laser tube to each other what do you do it in  relation to? I'm thinking of using the dividing wall between the laser tube and the bed as its a common feature to both sections?




---
**Ian C** *January 21, 2017 20:27*

No worries you're welcome buddy. We've all been there, took me over 12 hours in total to get mine nice as it was my first machine. Yeah I would use that panel, it's common to both main components so will give a good starting/reference point. I also used the left hand side of the casing to align the Y axis rails. I used a digital level to check the tube level against that of the bed X rails. If you remove the bed at any time and haven't already done so, remove or shorten the extractor funnel at the back at the same time :-)


---
**Rob Kingston** *January 21, 2017 20:36*

Great thanks Ian i will crack on with that tomorrow, was already planning on shortening the duct at the same time, il update my progress/findings tomorrow :)


---
**Ian C** *January 22, 2017 18:50*

You're welcome buddy. Keep us posted on your progress


---
**Rob Kingston** *January 22, 2017 22:04*

Managed to get this all lined up and running today, its pretty close to perfect, to get it any closer I'm going to need to make some adjustable laser tube brackets. Cheers for the help all


---
**Ian C** *January 22, 2017 22:22*

Great news, well done. None of the commercially available tube hangars work with the K40, but there is a 3D pritned design that does. I printed one out, with a second planned for if I did ned to realign the tube etc, but solde the machine before I ever tried it


---
*Imported from [Google+](https://plus.google.com/104868032632666700277/posts/6WkkRReejfR) &mdash; content and formatting may not be reliable*
