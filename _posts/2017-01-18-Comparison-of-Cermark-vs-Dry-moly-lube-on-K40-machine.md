---
layout: post
title: "Comparison of Cermark vs. Dry moly lube on K40 machine"
date: January 18, 2017 16:20
category: "Materials and settings"
author: "Jason Johnson"
---
Comparison of Cermark vs. Dry moly lube on K40 machine. 
{% include youtubePlayer.html id="5ziyB9EEnL8" %}
[https://youtu.be/5ziyB9EEnL8](https://youtu.be/5ziyB9EEnL8)





**"Jason Johnson"**

---
---
**Stephane Buisson** *January 18, 2017 16:40*

I would add a 3rd way to etch metal

in step 1 in this instructable use a k40

[instructables.com - How To Tattoo a Knife Blade w/ (Proper) Metal Etching](http://www.instructables.com/id/How-to-Tattoo-a-Knife-Blade-W-Proper-Metal-Etching/)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 18, 2017 17:29*

The results on the dry moly are definitely a big difference compared to the Cermark. I would hazard that it could be a difference in the % of MoS2 in the CRC Dry Moly (I think around 8% if I recall from MSDS that I read) compared with the stuff I used (around 82%, again if I recall correct from the MSDS). I have yet to do some more tests on stainless with it, but the results I got were much better than the results this guy got in his video.



**+Stephane Buisson** I like the method you shared. Since we all have lasers, I guess we could laser the tape instead of cutting it to give us much more precision on the design. I wonder would using a 12v charger of some kind be better than trying to use a puny little 9v battery?


---
**Ned Hill** *January 18, 2017 17:42*

What most people don't realize is that Cermark is a mix of Moly disulfide, clay slip, and very fine glass powder.  The clay acts as an energy absorber so the glass will melt and bind to the metal.  The moly is just the pigment.  This is why the Cermark is darker, because you get thicker layer of Moly coloring.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 18, 2017 17:52*

**+Ned Hill** That's interesting Ned. Does having the glass powder give it a longer lasting effect?


---
**Ned Hill** *January 18, 2017 17:59*

**+Yuusuf Sallahuddin** with the many bonds the glass forms with the metal, and itself, it will be a more robust surface compared to Moly disulfide bonded to metal with low to no cross linking.  Here's the link to the Theramark site which explains what is happening.  [http://www.thermark.com/content/view/16/86/](http://www.thermark.com/content/view/16/86/)


---
**Stephane Buisson** *January 18, 2017 18:02*

12v better than 9v but we also have kids in our readers so a battery is more safe ;-))


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 19, 2017 01:01*

**+Ned Hill** That was an interesting read. Good to note that the Thermark product will also bond to glass. So you could use it on drinking glasses to add colourful effects too. Pretty cool.



**+Stephane Buisson** Sounds good, just thought something with a more reliable or constant power source may minimise the issue mentioned in the Instructable where he had to do small patches at a time as the battery was not powerful enough to do the entire blade in one go.


---
**Ned Hill** *January 19, 2017 02:30*

**+Yuusuf Sallahuddin** I've actually been looking at trying to do some reverse engineering on the product for personal use.  I'll share if I ever get to it. 



On the etching, with the 9V battery he was limited by the battery capacity.  Higher voltage will drive the reaction faster.  You just don't want to drive it too fast.  I've seen people take wallwart charges and use them for etching.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 19, 2017 04:35*

**+Ned Hill** That would be great if you can reverse engineer it. Reading through the Thermark article you shared gives a vague idea of what way to go about it, as they suggest their product utilises pretty much the same standard ingredients that are in pottery/ceramic glazes for colouring. Not 100% sure about the special "absorber" components though, that'd be the hard part I guess.



I thought the battery capacity may be the issue with the etching, hence I was considering a charger of some sort. I imagine 12v charger would be a reasonable speed & provide a continuous source. Looks like a fun project to test sometime.


---
**Bob Damato** *January 19, 2017 14:02*

I wonder what his results would be if he made more than one pass on the moly spray.  Ive used cermark and the cheap moly spray and find I do need more than one pass with the cheap stuff, however the end result is just as good as the cermark.  Also, a second coat of moly spray helps too.


---
*Imported from [Google+](https://plus.google.com/114673513688962471823/posts/bTDe4zXedoU) &mdash; content and formatting may not be reliable*
