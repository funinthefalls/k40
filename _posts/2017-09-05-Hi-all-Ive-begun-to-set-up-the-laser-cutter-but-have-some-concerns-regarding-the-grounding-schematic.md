---
layout: post
title: "Hi all, I've begun to set up the laser cutter but have some concerns regarding the grounding schematic"
date: September 05, 2017 20:54
category: "Discussion"
author: "Benton Chuter"
---
Hi all,



I've begun to set up the laser cutter but have some concerns regarding the grounding schematic. 



The installation instructions describe establishing a separate ground rod and connecting that to a pin located on the back of the machine. However, this action seems either redundant and mildly performance reducing or dangerous, depending on the wiring scheme. 



In the former case, if the grounds of all electronic components are connected to the ground output prong, which is standard here in the US, then adding an additional ground is not only unnecessary but potentially harmful, as it could cause a ground loop since the outlet ground and the added ground may have different voltages. 



If the grounds of the electronics are not all connected to the ground prong and instead rely solely on the ground pin on the back of the machine then the device seems an electrical hazard, since there wouldn't be the same voltage and control measures. 



How should I proceed? I'm not an electrician but the prescribed setup seems likely to cause problems.



Edit: After probing the rear ground pin and ground prong, there seems to be only 1.2 ohms of resistance, suggesting that they are connected as they should be. I intend to find the resistance of the extension cord after  a print finishes but at what resistance should I consider adding a grounding rod for safety purposes?







**"Benton Chuter"**

---
---
**Don Kleinschnitz Jr.** *September 05, 2017 21:23*

Normal US grounding does not require a ground rod at the machine as it should be grounded at your main box. 



The important thing is that the safety ground has the lowest return impedance back to the source as possible. You don't want your body to be a lower impedance return than your safety ground.



The only time I would even consider another rod is if I was in a "Out" building and even then driving a ground rod does not guarantee that you have a shorter path to the source.


---
**Benton Chuter** *September 05, 2017 22:36*

Thank you for your response.



When referring to standard grounding in the US I merely mean that it's standard in the US to have all the grounds of an electrical device linked to a single ground, which may be connected through the third wire prong to main ground, which has a grounding rod. I did not mean to suggest that it's US standard to have a ground rod at the machine. 



Since I'm not familiar with the wiring of this machine I was hoping to discern whether the ground prong from the 3-prong outlet, which connects to the house main, is in electrical contact with the external ground pin described in the instructions or if they ground different components of the machine. While connecting all grounds of an electrical device seems standard to me as someone living in the US, I wanted to make sure that this held true for foreign imports like this as I've heard of rather janke wiring schematics in cheap Chinese electronics that would never be produced in the US. 



Similarly, I wanted to make sure that the ground pin of the three prong plug is in fact being used and that if I don't ground the pin on the back the machine is still grounded.



The laser cutter is in a shed with a 50ft extension cord from the house main line. Should l still disregard that step in the installation instructions?


---
**Don Kleinschnitz Jr.** *September 06, 2017 14:11*

**+Benton Chuter**   K40's direct from the factory would not pass US standards for safety. They are not subject to the same standards as US manufactured and imported/exported machines but that is another discussion ....



When wired as they were intended with the proper quality of construction K40's can be electrically safe however note that they are not "laser safe" (see below). Faulty wiring, soldering and assembly often leave these machines marginally electrically.



It is best to view a K40 as a kit that has been partially assembled that you need to finish :0.

.......

I wish I could assure you that K40's are grounded correctly as they come from the factory but "your mileage may vary".  BTW if you need  schematics they are available from this community. 



Its a good idea to trace and check the wiring/grounding before you use the machine. 



Particular things to check are:



<b>Main plug safety ground</b>: by inspection and DVM insure that the ground lug is grounded to the frame near the plug with a bolt, star washer and nut. Insure that the paint is removed from the surface before tightening.



<b>Operator surfaces</b>: With a DVM insure that the ground lug is connected to each cabinet surface including doors.



<b>On the LPS</b>:  with a DVM insure that -L and FG   are connected to the ground lug. This is on the rightmost connector and is labeled on the PCB.



General inspection tightening and soldering of all connections is a good idea.



If you have any specific question as you do the above do not hesitate to post it....

......

<b>50ft extension cord</b>: if the above checks ok your machine may be electrically safe at 50 ft provided you are using a 3 prong plug and the cord is rated for that distance and current. I do not know off the top of my head what current a K40 draws. You want to make sure that the cord has the right size wire so that you do not have appreciable voltage drop across the cord and/or heating.



<b>Out building ground rod</b> A grounding rod might be effective just to insure that you have a good safety ground. It's hard to know if the path of current through the earth to the source is lower impedance than the cords paths.  The path to the ground will be in parallel with the cords ground and if it ends up having  a lower impedance to the source (i.e your mains) any stray current will follow that path. Having a ground wire in the cord and a wire to a ground rod will likely not cause a problem. The safety current will follow the lowest impedance path. You are just trying to insure that its not your body.



<b>Laser and HV safety</b> : These machines do not protect the operator from damaging laser light and lethal High Voltages created as part of the laser subsystem. To make a K40 safe <b>you must install interlocks</b>. When you decide to do this you can search the community or come back with a separate post to get help doing so. Do not operate your machine without interlocks. 



Remember this is a: "pre-assembled kit" not a consumer safe machine! Its your task to make it safe.... :).


---
*Imported from [Google+](https://plus.google.com/111199965236730083474/posts/XPj3Q27M4Ew) &mdash; content and formatting may not be reliable*
