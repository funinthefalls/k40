---
layout: post
title: "Has anyone replaced the extractor slot in their cutter?"
date: May 04, 2015 23:00
category: "Modification"
author: "Stuart Middleton"
---
Has anyone replaced the extractor slot in their cutter? It seems to me that a wider one that reaches the full width of the cutting surface would be much better. I was thinking of designing one to cut if no one else has done this upgrade.





**"Stuart Middleton"**

---
---
**Jim Coogan** *May 05, 2015 00:02*

When I did an alignment and had to pull the xy stage out (it was bent) I also pulled out the exhaust assembly if that is the extractor slot you are talking about.  That, the fan, and the hose were among the first things tossed.  And the stage in the center with that adjustable slot.  The slot size in the back is not a real issue.  I bought a 4" adapter plate off Amazon that fits that slot perfect.  Added 20 foot of dryer hose and then added an inline fan that is attached at the end of the hose which is mount to an external vent.  That all dramatically improved the exhaust for the laser.  Some people have also pulled out that exhaust piece, left the slot open, and added an exhaust fan in the bottom of the unit as a sort of downdraft. 


---
**Stuart Middleton** *May 07, 2015 07:00*

What inline fan did you use? Was it a domestic bathroom extractor or did you use an industrial one? I've just got my long hose and will make an adapter plate tonight but haven't decided in the fan yet. Not sure how powerful it needs to be or how it will compare to the current one.


---
*Imported from [Google+](https://plus.google.com/+StuartMiddleton1/posts/6dY2weCa1gV) &mdash; content and formatting may not be reliable*
