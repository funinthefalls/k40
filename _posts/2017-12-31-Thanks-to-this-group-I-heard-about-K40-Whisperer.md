---
layout: post
title: "Thanks to this group I heard about K40 Whisperer"
date: December 31, 2017 06:18
category: "Software"
author: "Michael Isaac"
---
Thanks to this group I heard about K40 Whisperer. I just got it set up and was playing around with it, moving the head around rapid. And I keep getting a pop up saying "Too many transmission errors". Not every move, but about every 3rd move. It seems like everything keeps working though, just have to hit ok on the pop up. 



Does anyone know if this is a problem that will mess up cuts/prints? 



I'm suspecting it could be due to the USB cable length, since its 8 feet long, around a bunch of other wires, and probably a cheap one (came with the cutter). I really only need a 4 or 5ft cable. Or it could just be because I'm doing too many big rapid movements, cutting jobs wouldn't do that.





**"Michael Isaac"**

---
---
**Joe Alexander** *December 31, 2017 06:52*

does your usb cable have ferrite on both ends? its that plastic block that usually annoys most, but can help prevent noise.


---
**Michael Isaac** *December 31, 2017 07:03*

It has one on one end. I tried it both ways, next to to the PC and next to the cutter, didn't seem to make a difference.


---
**Printin Addiction** *December 31, 2017 14:21*

Make sure it completes a move before clicking the next move.


---
**Michael Isaac** *January 04, 2018 20:30*

Used k40 Whisperer some more, and I think I see what is happening. It seems like, if the head is far from home,  and you tell it to move, it is not waiting long enough for the head to return to home before it sends the next move request. If I home it before moving it, there are no more transmission errors.



Thanks for the great software Scorch Works! 


---
*Imported from [Google+](https://plus.google.com/115218763778220927346/posts/eg6qhyKmjWu) &mdash; content and formatting may not be reliable*
