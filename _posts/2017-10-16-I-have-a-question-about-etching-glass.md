---
layout: post
title: "I have a question about etching glass"
date: October 16, 2017 00:32
category: "Materials and settings"
author: "Martin Dillon"
---
I have a question about etching glass.  I was thinking about etching my wife's name into the bottom of her glass pie pans and was wondering if it might cause them to crack?  I would be etching onto the bottom side not the eating surface.  It is so when we go to pot lucks we can find our dishes easier.

Any thought on this?





**"Martin Dillon"**

---
---
**ki ki** *October 16, 2017 09:56*

I have seen people etching glass by painting the glass with black paint / nail polish, this 'sticks' the paint to the glass I believe. I am not sure how food safe this is, maybe reverse the image and etch from the outside? This can be done with a 2.5W laser diode on simple glass plate without cracking. IT seems to produce 'micro fractures' in the glass. Try find the type of glass used in the pie dishes (pyrex?) and search from there.


---
**Ned Hill** *October 16, 2017 14:32*

**+Martin Dillon** assuming the pans are pyrex I would think it's ok as long as you don't go too deep.  Just a light etch. 


---
**Nate Caine** *October 16, 2017 18:30*

My intuition says "don't do it".  



When you cut glass, you scribe it to provide a structured defect for a crack to follow.  And that's kind of what you'd be doing.  I'm worried about that once it sees a 400F oven.  In addition to a <i>rapid</i> change in temperature.



Although they <b>do</b> chemically etch glass, car windows, etc.  I see a lot of car windows here that once they get chipped, the crack propagates.  Then again, one sees scratched pyrex--from normal use--that survives the oven.  Perhaps it's a matter of the magnitude of the scratch..



A few of the glass laser projects I've seen over the years suffered from excessive power.  The "etch" was irregular around the edges and depth of penetration.  Not pretty.  Lower power seemed to work best.  In fact a second pass left an interesting and unexpected ripple texture.  



Be sure to "blow" off the glass dust that will pile up around the work area.  If you "sweep" it off, the glass dust will tend to scratch the remaining glass (each is as hard as the other).



Perhaps a neighbor has a pyrex baking dish you could borrow?  >:)


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/TyiuPriUJYb) &mdash; content and formatting may not be reliable*
