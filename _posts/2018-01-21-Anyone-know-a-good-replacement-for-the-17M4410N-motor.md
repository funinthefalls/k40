---
layout: post
title: "Anyone know a good replacement for the 17M4410N motor?"
date: January 21, 2018 18:58
category: "Original software and hardware issues"
author: "Frederik Deryckere"
---
Anyone know a good replacement for the 17M4410N motor? Mine is dead and there is no solid info to be found all over the internet. And I would know cause I searched every last corner of it.





**"Frederik Deryckere"**

---
---
**Alex Krause** *January 21, 2018 19:03*

Check **+Don Kleinschnitz**​ blog post

[http://donsthings.blogspot.com/2016/06/k40-s-motors.html?m=1](http://donsthings.blogspot.com/2016/06/k40-s-motors.html?m=1)


---
**Stephane Buisson** *January 22, 2018 08:26*

don't need to be a particular model, but you need to pay attention to the current as the power supply don't have much spare, so don't oversized it.


---
**Frederik Deryckere** *January 22, 2018 15:58*

**+Alex Krause** Yeah I found that post. But it conflicts with other sources. Specifically I need the step angle and rated current of that motor. So odd there is very little info about it, and I haven't found a solid lead from someone who succesfully changed theirs.


---
**Alex Krause** *January 22, 2018 16:06*

**+Frederik Deryckere** the output from the power supply is only 1 amp this has been found by tearing down several power supplies so each motor would have to be .5 amp to not overwork the power supply 


---
**Frederik Deryckere** *January 22, 2018 20:05*

**+Alex Krause** I have done some digging and found the original manufacturer of the motor. Long story short that exact model isn't available anymore but from their naming scheme it seems this was either a 0.9deg 1A or 1.7A(!) motor. Doesn't make sense but hey, nothing about that unit does. I bought a 0.9A to be on the safer side. We'll see how it performs. 


---
**HalfNormal** *January 23, 2018 13:06*

Here is the M2nano manual that has some info on the stepper motors. There is an english translation.

[drive.google.com - M2NanoDRV主板硬件手册.pdf](https://drive.google.com/file/d/0B9qXcw3VRDg0Z2FLV1ozTG13TG8/view)


---
**Frederik Deryckere** *January 23, 2018 13:48*

great thx! I'm currently talking to the company. They seem to be helpful, but communication is not optimal ;-). This is my motor. The logo helped me find the manufacturer. [http://en.baolongmotor.com](http://en.baolongmotor.com)

![images/4f3035259f5a8e52864493988c44b0a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f3035259f5a8e52864493988c44b0a2.jpeg)


---
**Frederik Deryckere** *January 26, 2018 08:00*

Well this is settled then. My newly ordered 0.9A 0.9deg motor will perform just fine it seems.

![images/c2d16ad01c1d61a5898869940ca51af2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c2d16ad01c1d61a5898869940ca51af2.jpeg)


---
**Dennis Luinstra** *January 28, 2018 04:45*

Frederik, where did you ordered the new motor and what type / number? And did it include the pulley? 


---
**Frederik Deryckere** *January 28, 2018 08:08*

I bought this one: [ebay.com - Details about DE Ship 0.9deg Nema 17 Stepper Motor Bipolar 0.9A 36Ncm 42x42x39mm 4-wires DIY](https://www.ebay.com/itm/DE-Ship-0-9deg-Nema-17-Stepper-Motor-Bipolar-0-9A-36Ncm-42x42x39mm-4-wires-DIY/231657793068)

Note that it is not dual shaft! For a direct K40 replacement you'd need a dual shaft.




---
**Frederik Deryckere** *January 28, 2018 08:14*

Here's its sheet

![images/37be3f6188539fb3129577da08e48f8a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/37be3f6188539fb3129577da08e48f8a.jpeg)


---
**Frederik Deryckere** *January 28, 2018 08:17*

I haven't tried it though. So I'm not even sure it works. But specs seem close enough.


---
**Stephane Buisson** *January 28, 2018 09:45*

0.9A is a lot, hopefully your PSU will survive, how far do you test it ? 

(normal K40 motor is 0.5A)


---
**Frederik Deryckere** *January 28, 2018 10:00*

My original motor was 1A. I posted the spec sheet earlier in this conversation. I searched and contacted the original manufacturer of the motor bc there was a LOT of conflicting info on the step angle and current rating of the motors. Maybe other K40 versions got other motors. Idk ;-)


---
**Stephane Buisson** *January 28, 2018 10:24*

for sure all the K40 aren't equaly build.

but PSU as always been weak/limit.

just think about others readers they should know about it before jumping on bigger motors.


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/cHRUo1BFLVf) &mdash; content and formatting may not be reliable*
