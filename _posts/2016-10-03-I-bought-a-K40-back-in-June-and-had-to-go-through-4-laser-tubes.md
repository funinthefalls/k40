---
layout: post
title: "I bought a K40 back in June, and had to go through 4 laser tubes"
date: October 03, 2016 00:49
category: "Original software and hardware issues"
author: "Jimmy Vo"
---
I bought a K40 back in June, and had to go through 4 laser tubes. the 1st being faulty and the 2nd and 3rd being shipped improperly. the 4th one finally arrive and i installed it. i manage to cut one project out of it so i thought that was the end of my problems. i had it sitting for a while and recently i got to use it again. i transfer the machine over from a tight corner onto a large utility cart to have better access to the back of the machine, after moving it i had to calibrate the mirrors again. i noticed while calibrating the knob did not match up with the power gauge. i would turn it up to 11 to see only 6 on the screen. it thought nothing of it until i started another project and it suddenly started arcing from the end of the tube. 



my assumption is that this tube is also faulty and only showed its issues over time as the seal was slowly releasing gas.  





**"Jimmy Vo"**

---
---
**Alex Krause** *October 03, 2016 03:22*

Arcing is usually due to improper insulation of the wires that connect the tube. Electricity will always seek the shortest path to ground with the least resistance. Can you take a picture of your connections to the tube... please be careful there is 10-20k volts running on the high voltage side of the PSU


---
**Jimmy Vo** *October 03, 2016 03:44*

![images/517d5c5f4cfaa7469189527c175c9a75.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/517d5c5f4cfaa7469189527c175c9a75.jpeg)


---
**Jimmy Vo** *October 03, 2016 03:48*

The arcing is occurring from the farthest end. 

![images/334fec52162dcc247eb6a0bf8eab3530.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/334fec52162dcc247eb6a0bf8eab3530.jpeg)


---
**Claudio Prezzi** *October 03, 2016 15:58*

There is not enough distance between the end of the tube an the case! It should be at least a few cm. Do you have some space left on the other side? Otherwise, the tube doesn't fit your machine.


---
**Jimmy Vo** *October 03, 2016 16:26*

Yea i do. I guess ill have to adjust my mirrors again. Hopefully my seller still offers me another tube as an backup


---
*Imported from [Google+](https://plus.google.com/109566762708840291719/posts/Y3mJTJCy5pJ) &mdash; content and formatting may not be reliable*
