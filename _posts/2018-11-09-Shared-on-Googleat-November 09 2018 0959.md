---
layout: post
title: "Shared on November 09, 2018 09:59...\n"
date: November 09, 2018 09:59
category: "Object produced with laser"
author: "Paul de Groot"
---

{% include youtubePlayer.html id="QXNrcDI0U0s" %}
[https://www.youtube.com/watch?v=QXNrcDI0U0s](https://www.youtube.com/watch?v=QXNrcDI0U0s)





**"Paul de Groot"**

---
---
**Kelly Burns** *November 10, 2018 17:30*

What software are running and processing the image with?  Looks fantastic 


---
**Paul de Groot** *November 10, 2018 21:48*

It runs on a highly modified version of grbl that I ported to the STM32F103 processor. Bascially it run a 32bits version capable of driving 5 axis, 16bits pwm, with additional M6 and Txxx ports. See for details my Kickstarter campaign Super Gerbil 


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/XrCN4KsRb9Z) &mdash; content and formatting may not be reliable*
