---
layout: post
title: "Guys where and what mirror is thought to be the best type for the k40?"
date: November 21, 2017 00:57
category: "Hardware and Laser settings"
author: "Chris Hurley"
---
Guys where and what mirror is thought to be the best type for the k40? 





**"Chris Hurley"**

---
---
**HP Persson** *November 21, 2017 02:37*

Almost anything than original :P

Keep away from K9, go for MO or SI mirrors.

Mo is what i use myself, a little less reflectance but can stand more beating. SI give a little more power to the lens but are delicate and need cleaning often to not heat up and destroy the coating.

There is other types too, as CU but the gain is very small on our machines.



Where you buy them are not so important, the biggest problem with ebay/aliexpress is to get three good ones, you almost have to grab 5, to get 3 good and hope they all survive the shipping :)


---
**James Lilly** *November 21, 2017 05:19*

I cracked one of the original K9 that shipped with my unit.  I purchased 3x HQ Si Plated gold Reflective Mirrors for $35 fro CNCOLETECH on Amazon.  Free 1 day shipping with Prime. I have only replaced the broken one so far, and it works great.


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/PU1dDx1KXZh) &mdash; content and formatting may not be reliable*
