---
layout: post
title: "Where should I order a k40 from?"
date: December 03, 2015 01:39
category: "Discussion"
author: "Scott Howard"
---
Where should I order a k40 from?  I'm in South Carolina. ....thanks! !!





**"Scott Howard"**

---
---
**Scott Howard** *December 03, 2015 02:58*

Ok thanks


---
**Gee Willikers** *December 04, 2015 23:48*

Exactly what Carl said above. However if you immediately intend to upgrade the electronics to a DSP (as i did) get one with the Moshi board if its cheaper.


---
**Scott Howard** *December 05, 2015 05:18*

Thanks! 


---
**Scott Thorne** *December 06, 2015 04:24*

Stay away from a company called technology e-trade!


---
**Scott Howard** *December 06, 2015 04:25*

Not good? 


---
**Scott Thorne** *December 06, 2015 04:27*

No....my machine arrived with a broken tube and I had to replace it myself....the company keeps telling me that they sent me a replacement but I have yet to see it.﻿


---
**Scott Thorne** *December 06, 2015 04:27*

The company just scams people....Stay far away....Lol


---
**Scott Howard** *December 06, 2015 04:28*

Ok thanks, do you know of a good company?


---
**Scott Thorne** *December 06, 2015 04:28*

With that being said....now that it works...it's awesome.


---
*Imported from [Google+](https://plus.google.com/113905238524886327481/posts/VNh5aPbaYDm) &mdash; content and formatting may not be reliable*
