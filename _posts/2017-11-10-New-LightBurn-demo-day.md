---
layout: post
title: "New LightBurn demo day!"
date: November 10, 2017 08:42
category: "Software"
author: "LightBurn Software"
---
New LightBurn demo day!


{% include youtubePlayer.html id="hLDGV1XlozE" %}
[https://www.youtube.com/watch?v=hLDGV1XlozE](https://www.youtube.com/watch?v=hLDGV1XlozE)





**"LightBurn Software"**

---
---
**Jonathan Davis (Leo Lion)** *November 17, 2017 21:28*

How do people download the tool. 


---
**LightBurn Software** *November 17, 2017 21:30*

**+Jonathan Davis** - it’s not released yet, but will be very shortly - hoping for the end of this month.


---
**Jonathan Davis (Leo Lion)** *November 17, 2017 21:40*

**+LightBurn Software** alrighty and thanks for the information. I own a Elekes maker system and the BenBox system had a line of design tools that were quite handy for creating designs on the fly. 


---
**LightBurn Software** *November 17, 2017 21:42*

The release will be posted on the FB page @ [http://facebook.com/LightBurnLaser](http://facebook.com/LightBurnLaser) and we’ll have a website up fairly soon too.  Lots of pieces starting to come together.


---
**Jonathan Davis (Leo Lion)** *November 17, 2017 21:44*

**+LightBurn Software** very nice and hopefully it will be a free open source program. Also as a consideration, if possible is allowing the software to work on older systems like Mac OS X 10.6.8 or for example Windows xp. 


---
**LightBurn Software** *November 17, 2017 21:47*

Not free, but not expensive either.  Trying hard to make it accessible to everyone, but at the same time rewarding the effort that went into creating it.


---
**Jonathan Davis (Leo Lion)** *November 17, 2017 21:48*

**+LightBurn Software** of course 


---
**LightBurn Software** *November 17, 2017 21:50*

I’ll have to look at what the min-spec is, but a number of beta users are on Win7 or older Macs.  The software is written to be fast, so performance on older machines should be decent.


---
**Jonathan Davis (Leo Lion)** *November 17, 2017 21:51*

**+LightBurn Software** Good to know because I own a few different OS systems, two Macs, four PCs 


---
*Imported from [Google+](https://plus.google.com/110213862985568304559/posts/CUTiXUstaUu) &mdash; content and formatting may not be reliable*
