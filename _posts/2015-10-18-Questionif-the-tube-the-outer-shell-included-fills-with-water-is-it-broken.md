---
layout: post
title: "Question....if the tube., the outer shell included fills with water is it broken?"
date: October 18, 2015 21:03
category: "Hardware and Laser settings"
author: "Scott Thorne"
---
Question....if the tube., the outer shell included fills with water is it broken? Looks like I can see cracks in the tube...but I'm not sure about that. Also if the tube is bad and I press the test button will the machine do nothing at all?





**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 18, 2015 21:25*

Hi Scott. I have recently just bought this machine also. I will run down stairs & do a quick check for you then return to let you know.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 18, 2015 21:30*

Just checked & looking at mine the water only flows through the inner tube. None in the outer tube.



Regarding test button, have you a) increased the dial or power of the machine? & b) pressed the button that turns the laser on?



I imagine if you've done a & b then there is something wrong with your tube. Hopefully some of the other guys here can give you more information about that.


---
**Scott Thorne** *October 18, 2015 21:44*

Thanks so much. The outer shell is half full of water, I've only had it 1 day and the laser has never worked.


---
**ThantiK** *October 18, 2015 22:27*

Yep, almost certainly the laser tube was broken from delivery.  It's not exactly an uncommon occurrence.


---
**Scott Thorne** *October 18, 2015 22:29*

Any advice on where to get another one....everything else seems to work fine...I made a drawing and the head moves like it is supposed to.


---
**ThantiK** *October 18, 2015 22:37*

**+Scott Thorne**, contact the seller.  Take pictures, let them know that the tube was broken in delivery.  They carry their own insurance for things like this and depending on the seller, it's possible they'll simply ship you a new tube.


---
**Scott Thorne** *October 18, 2015 22:39*

The seller was technology-etrade......I've already emailed them through eBay....maybe I will hear from them tomorrow.


---
**Jon Bruno** *October 19, 2015 11:07*

Yeah the outer envelope is the co2 reservoir. There shouldn't be any water in there. If there is the tube is no longer any good because it's open to the atmosphere now.




---
**Jon Bruno** *October 19, 2015 11:08*

Well... That and it's full of water.. Lol


---
**Stuart Middleton** *October 19, 2015 18:14*

Mine was broken like this. If you look carefully you'll see where the glad is broken. Usually around the glass coil. Remember, make sure the seller gets you a replacement qyickly, before the eBay refund time runs out. If they drag their feet just force a refund through eBay. It arrived broke so they need to fix it or refund you. 


---
**Scott Thorne** *October 20, 2015 21:24*

Lol...Thanks Jon.....this is my first co2 laser so I'm new to it....after spending 280 on a new tube...I'm learning fast.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/aDDiQ2xYAD1) &mdash; content and formatting may not be reliable*
