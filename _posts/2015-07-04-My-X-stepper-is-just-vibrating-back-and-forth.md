---
layout: post
title: "My X stepper is just vibrating back and forth"
date: July 04, 2015 22:59
category: "Hardware and Laser settings"
author: "quillford"
---
My X stepper is just vibrating back and forth. I'm using ramps and marlin. I've confirmed it is not the stepper driver as the Y motor works if i plug it into the X motor header. I've also tried adjusting the vref. Does anyone have any suggestions?





**"quillford"**

---
---
**David Richards (djrm)** *July 04, 2015 23:06*

Hi, do you have end stop settings configured correctley?


---
**David Richards (djrm)** *July 04, 2015 23:07*

Its not that is it. Perhaps the motors are wired differentley.


---
**quillford** *July 04, 2015 23:12*

I disabled endstops.


---
**David Richards (djrm)** *July 04, 2015 23:18*

perhaps the ribbon is damaged, measure the motor winding resistance at the ramps board with everything connected.


---
**quillford** *July 04, 2015 23:24*

I suspected the ribbon cable, so I am connecting directly to the controller.


---
**David Richards (djrm)** *July 04, 2015 23:39*

Have you got another stepper motor to try, what else havnt you eliminated?


---
**David Richards (djrm)** *July 04, 2015 23:41*

The stepper drivers dont like being disconnected whilst running. They could be damaged now, even if they were ok before.


---
**quillford** *July 05, 2015 01:30*

I think I figured out the source of the problem. It seems whenever I connect the motor with some sort of extension (i.e. the ribbon cable), it just vibrates. (When I said I was directly connecting above, I meant that I wasn't going through the ribbon cable and circuit but instead I was using an extension I made using the connector and just wires.) If I actually directly connect it, it works fine. I think I'll just be switching out the stepper, but is there anything that can happen when extending the wires that would cause this? I already checked for continuity. 


---
**David Richards (djrm)** *July 05, 2015 08:57*

I imagine the extra resistance in your extended wiring is causing the maximum achievable stepping rate to decrease, you could try changing the maximum feed rate value in the Marlin configuration. Another thing to consider is what value of micro stepping you have the driver jumpers set to. Is the stepper power supply ok? what voltage are you using?

I've just checked the DEFAULT_MAX_FEDDRATE values I'm using on my 3Dprinter with NEMA-17 stepper motors and they are set to 200 mm/sec, the default in Marlin appears to be 500. Im using 8x microstepping iirc and STEPS_PER_UNIT is set to 80.  hth David.


---
**Lars Mohler** *July 11, 2015 14:57*

It can only be the wiring or the motor if the X driver is working. Check for an open or shorted winding in your X motor. If it looks good, test the X motor on the Y driver.


---
**quillford** *July 11, 2015 22:27*

**+triac777** I'm not with the laser cutter atm so can't check the winding, but I already made sure it is not the driver.


---
**Mathew Smith** *August 29, 2016 23:17*

Mine was doing the  same thing. Make sure that you have the jumpers installed underneath the motor drivers on the ramps board.  Once I got those installed everything worked.


---
*Imported from [Google+](https://plus.google.com/115654175984463400645/posts/4sWxbZPRnJ4) &mdash; content and formatting may not be reliable*
