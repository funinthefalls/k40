---
layout: post
title: "In case anyone is interested in my experience with MoshiDraw 2015x issues I am having as well as what support is having me try to resolve them I am sharing a playlist of the videos I am sending"
date: October 10, 2015 14:05
category: "Discussion"
author: "Brooke Hedrick"
---
In case anyone is interested in my experience with MoshiDraw 2015x issues I am having as well as what support is having me try to resolve them I am sharing a playlist of the videos I am sending.  



The issue I started with is that when cutting, in the middle of a cut, it will stop following the path and start cutting a straight line along the Y axis until it crashes into the limit where the only solution is turning the machine off and on.




{% include youtubePlayer.html id="playlist" %}
[https://www.youtube.com/playlist?list=PL8lf4YK9R2zpHVpuypZjtjiXgis9XpDqS](https://www.youtube.com/playlist?list=PL8lf4YK9R2zpHVpuypZjtjiXgis9XpDqS)





**"Brooke Hedrick"**

---
---
**Phillip Conroy** *October 10, 2015 23:41*

Iwould say check all nnections,if good contact seller


---
**Brooke Hedrick** *October 11, 2015 01:03*

Check out the videos when you get a chance and see if it still looks like connection issues.


---
**Joey Fitzpatrick** *October 11, 2015 01:31*

Are you running moshidraw on windows 8 or 10?  I am not sure if it functions correctly on 8 or 10.  I have seen the same issue on a K40 with moshi.  The fix was to run it on a clean xp install.  I know that XP is outdated but it seems to work best.  I believe the problem is with the data transfer between the PC and the laser engraver.  when communication breaks down or lags, the engraver has a mind of its own.  It sucks that people with moshi main boards are stuck using moshidraw


---
**Brooke Hedrick** *October 11, 2015 04:25*

Hey Joey, I am using Windows 7.


---
**Brooke Hedrick** *October 11, 2015 04:33*

I don't see running XP as a good choice.  It would be a pain to use it without giving it internet access and giving XP internet access would just be asking for trouble.



My hopes are that by working with support I can help improve the software enough to be able to use the cutter before I work on the DSP upgrade after Halloween.



So far, I have been posting videos between 10p-12a my time and support gets back to me with suggestions by the time I am up the next day.



They haven't yet suggested XP as a solution and I put in my video descriptions the version of Windows, MoshiDraw, and the MS... Controller board.


---
**Stephane Buisson** *October 11, 2015 08:50*

Hi,

this exactly why, I am changing my electronic board. because my choice is now to go for Visicut and give up the Moshi way.

My Smoothie conversion is going well so far (moving XY) but not finished yet.

My choice for Smoothie is motivated by :

- Software chain (InKscape/Illustrator/CAD -> Visicut)

- extra Gpio (water flow control, door switch, Z or rotary ext. )

- cost half of Light object DSP (with LCD)

- wiring very simple to do ( original motor connectors plug directly into smoothieboard, just extend end stops, and 2 wires for laser pwm)


---
**David Cook** *October 12, 2015 02:06*

Exactly what happened to me as well  with the Y axis.

Already changed out the board to an Azteeg X5  like Stephane.


---
**Miguel Alvarado** *May 13, 2016 16:35*

hola, yo he comprado este laser 40w y sufro con moshidraw... alguien ha intentado instalar moshi 2016?


---
**Miguel Alvarado** *May 13, 2016 16:37*

**+Brooke Hedrick**  try with a virtual machine with win xp






---
**Brooke Hedrick** *May 14, 2016 05:41*

I upgraded to a LightObject DSP.   Very happy with software and hardware.


---
*Imported from [Google+](https://plus.google.com/+BrookeHedrick/posts/HmnLmEKDnKn) &mdash; content and formatting may not be reliable*
