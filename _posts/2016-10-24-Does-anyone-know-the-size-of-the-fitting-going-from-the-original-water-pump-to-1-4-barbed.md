---
layout: post
title: "Does anyone know the size of the fitting going from the original water pump to 1/4 barbed?"
date: October 24, 2016 18:59
category: "Hardware and Laser settings"
author: "kyle clement"
---
Does anyone know the size of the fitting going from the original water pump to 1/4 barbed?  Mine had the brass fitting not the push in one





**"kyle clement"**

---
---
**Scott Marshall** *October 25, 2016 04:19*

It's 1/2" NPT (pipe thread)


---
**kyle clement** *October 25, 2016 04:27*

Thank you


---
*Imported from [Google+](https://plus.google.com/106211725666880587072/posts/321VpAFmkK9) &mdash; content and formatting may not be reliable*
