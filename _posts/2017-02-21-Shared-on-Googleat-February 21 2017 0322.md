---
layout: post
title: "Shared on February 21, 2017 03:22...\n"
date: February 21, 2017 03:22
category: "Discussion"
author: "Robert Selvey"
---
[http://geargenerator.com/#200,200,100,6,1,3,1818.8999999999069,4,1,8,2,4,27,-90,0,0,16,4,4,27,-60,1,1,12,1,12,20,-60,2,0,60,5,12,20,0,0,0,2,-329](http://geargenerator.com/#200,200,100,6,1,3,1818.8999999999069,4,1,8,2,4,27,-90,0,0,16,4,4,27,-60,1,1,12,1,12,20,-60,2,0,60,5,12,20,0,0,0,2,-329)





**"Robert Selvey"**

---
---
**Steve Anken** *February 21, 2017 04:08*

Downloaded the default gear and did a quick simulation in ArtCAM (holes added). So far your gear program is the best I've seen, for free, and it seems to be something a lot of people like to make. Thank you, I will definitely try doing some gears. 



![images/8260a23cb674200588762579cacb4c30.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8260a23cb674200588762579cacb4c30.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 21, 2017 05:07*

Looks good. Thanks for sharing.


---
**Don Kleinschnitz Jr.** *February 21, 2017 14:50*

Thanks for sharing, on my list of tools.




---
**Gunnar Stefansson** *February 22, 2017 08:58*

Wow, amazing. Thanks for Sharing... Bookmarked! :D


---
**Circle J** *February 27, 2017 05:13*

zexy. thank you!


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/NbcVzH1NZa5) &mdash; content and formatting may not be reliable*
