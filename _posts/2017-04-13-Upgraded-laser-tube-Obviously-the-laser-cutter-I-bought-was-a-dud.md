---
layout: post
title: "Upgraded laser tube!!! Obviously the laser cutter I bought was a dud"
date: April 13, 2017 13:43
category: "Modification"
author: "Jeff Johnson"
---
Upgraded laser tube!!! Obviously the laser cutter I bought was a dud. The laser tube lasted about half the time I expected and the power supply, fan and water pump failed and have been replaced. Even with this poor performance, I managed to earn enough to buy 5 of these machines. that's pretty good I think!



I've been wanting a more powerful laser but the items I make fit just fine in the 300x200mm bed of this laser. I emailed Light Objects with what I was looking for and they suggested this

[http://www.lightobject.com/SPT-45W-CO2-Sealed-Laser-Tube-1M-P751.aspx](http://www.lightobject.com/SPT-45W-CO2-Sealed-Laser-Tube-1M-P751.aspx)



It fit my needs which were a 50m diameter and power requirements that matched my power supply. 



I bought the thinnest cutoff wheel I could find for my angle grinder and made a hole for it. I also needed to extend the water tube and high voltage wire. The water tube was easy but nobody around here sells high voltage wire - except auto parts stores! I purchased a spark plug wire and spliced it in and it works great. 



My initial testing shows that I can cut 3mm MDF at 15ma and 20mm per second. I'll keep dialing this down to get the lowest power / highest speed I can get.



I've got a lot of cleanup to do and a box to make to protect he tube. Hopefully I'll get that done tonight.



![images/318daee6a8d780c9f654fab37b8ae227.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/318daee6a8d780c9f654fab37b8ae227.jpeg)
![images/8538a5b0d0f1d603e42a38871b4593df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8538a5b0d0f1d603e42a38871b4593df.jpeg)

**"Jeff Johnson"**

---
---
**Don Kleinschnitz Jr.** *April 13, 2017 14:11*

Are you running the standard 40W LPS with this?


---
**Jeff Johnson** *April 13, 2017 16:56*

**+Don Kleinschnitz** Yes and it seems to be working fine. 


---
**K** *April 13, 2017 19:48*

Can you tell me more about the box you have at the back of your machine where the duct runs in?


---
**Jeff Johnson** *April 13, 2017 20:27*

**+Kim Stroman**​​ Sure. It's a box I made from foam board that I purchased at Walmart. There's a hole that allows the exhaust to be sucked in. Blowing air out through the tube is a 90 millimeter computer fan. It's a very heavy duty fan and blows about 120 CFM. The fan, along with internal LED lighting , is powered buy a 12 volt power supply I added inside. I also have an inline exhaust fan at the end of the duct that blows the exhaust out the window and keeps my room smoke-free.


---
**Ned Hill** *April 13, 2017 20:50*

LO object actually sells a tube extension box for doing this in case you didn't know.  [lightobject.com - K40 Mini Laser Machine Tube Extension Case](http://www.lightobject.com/K40-Mini-Laser-Machine-Tube-Extension-Case-P960.aspx)


---
**Jeff Johnson** *April 13, 2017 21:49*

**+Ned Hill** I saw that after I had purchased the parts to build a box. I wish I had done more searching on their site. The parts cost me $20 and for twice that I could have had it already made.


---
**Ned Hill** *April 13, 2017 22:04*

**+Jeff Johnson** Will be interested to see how you are making your box though.


---
**Jeff Johnson** *April 13, 2017 22:05*

I'll be sure to take pictures.


---
**Cesar Tolentino** *April 17, 2017 22:51*

Is this the original k40 power supply?


---
**Jeff Johnson** *April 17, 2017 22:52*

**+Cesar Tolentino** yes. This laser tube has power requirements that are met by the original power supply.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/fN1276v3qAM) &mdash; content and formatting may not be reliable*
