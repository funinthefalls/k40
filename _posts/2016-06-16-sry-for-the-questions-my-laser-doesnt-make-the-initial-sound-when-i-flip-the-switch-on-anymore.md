---
layout: post
title: "sry for the questions.... my laser doesnt make the initial sound when i flip the switch on anymore...."
date: June 16, 2016 21:43
category: "Discussion"
author: "Christina Marie"
---
sry for the questions.... my laser doesnt make the initial sound when i flip the switch on anymore.... however the test switch shoots the beam... any suggestions?





**"Christina Marie"**

---
---
**Alex Krause** *June 16, 2016 22:13*

1. Check the connections on your board... 2. Endstops my be bad... 3. If you have optical endstops clean them


---
**Ariel Yahni (UniKpty)** *June 16, 2016 22:26*

What kind of sound? 


---
**Christina Marie** *June 17, 2016 00:13*

its that little musical sound when you flip the on switch that goes (do dii) that sound is not happening. When i go to output it says machine open but won't output it just has a pop up saying outputting but nothing happen. I've replaced usb cord still nothing


---
**Christina Marie** *June 17, 2016 00:16*

the d3 red light on my board is on?


---
**Christina Marie** *June 17, 2016 00:21*

it doesnt hone itself


---
**Alex Krause** *June 17, 2016 00:45*

The machine will home without being plugged into the computer... try moving your gantry manually away from the endstops then turn on... does the machine attempt to home then?


---
**Christina Marie** *June 17, 2016 00:48*

no it does not home them :/ dongle key is in as well


---
**Christina Marie** *June 17, 2016 00:51*

I've never had the machine grounded... has worked for years. you thing that could be it?


---
**Alex Krause** *June 17, 2016 02:01*

Old age... your board or part of your power supply may be going out


---
**Christina Marie** *June 17, 2016 02:13*

ok thanks all! do you guys have you lasers grounded?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 04:49*

I don't have my laser grounded, as in Australia our wall-socket has a 3rd pin for ground. I know some have said not to rely on that, but I'm fairly confident that the ground in my house is suitable.


---
**Donna Gray** *June 17, 2016 06:44*

I didn't have mine grounded as well as the power in our house has the grounding and I read somewhere that if anything goes wrong it will just flick the switch in your meter box so I didn't worry


---
*Imported from [Google+](https://plus.google.com/106654713649021155154/posts/4Q4oVGLyzT7) &mdash; content and formatting may not be reliable*
