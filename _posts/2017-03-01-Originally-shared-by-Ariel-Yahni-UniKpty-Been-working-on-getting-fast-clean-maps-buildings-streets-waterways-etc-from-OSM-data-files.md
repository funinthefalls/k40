---
layout: post
title: "Originally shared by Ariel Yahni (UniKpty) Been working on getting fast clean maps ( buildings, streets, waterways, etc ) from OSM data files"
date: March 01, 2017 04:14
category: "Repository and designs"
author: "Ariel Yahni (UniKpty)"
---
<b>Originally shared by Ariel Yahni (UniKpty)</b>



Been working on getting fast clean maps ( buildings, streets, waterways, etc ) from OSM data files. I have gotten what i think its pretty usable vectors.



Bellow a laser engrave done with LaserWeb 4.0.40 



Sadly this process requires Rhino and Grasshopper.



If you have some vector maps please go to [www.openstreetmap.org](http://www.openstreetmap.org) and follow this procedure

[http://recordit.co/ed2chWGFsv](http://recordit.co/ed2chWGFsv)



Contact me via PM to send me that downloaded file. Try to limit the download to a small section like in the video, plenty of data there





**"Ariel Yahni (UniKpty)"**

---
---
**Claudio Prezzi** *March 01, 2017 08:29*

Cool idea!


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/S659LEFcwrY) &mdash; content and formatting may not be reliable*
