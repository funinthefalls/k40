---
layout: post
title: "You know where I can buy in Europe a power supply?"
date: August 17, 2016 16:55
category: "Hardware and Laser settings"
author: "Pedro David"
---
You know where I can buy in Europe a power supply?





**"Pedro David"**

---
---
**Bart Libert** *August 19, 2016 10:43*

I see you don't seem to get a lot of answers. I found that [www.ebay.de](http://www.ebay.de) does have some available locally in Germany. That would be a nice place to get them from


---
*Imported from [Google+](https://plus.google.com/113379323980322370039/posts/ZHhnF3aGhdA) &mdash; content and formatting may not be reliable*
