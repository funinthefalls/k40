---
layout: post
title: "Hello folks. I recently purchased one of the many cheap laser cutters from eBay (specifically this guy ) and I've been playing with it over the weekend"
date: September 28, 2015 18:29
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Hello folks. I recently purchased one of the many cheap laser cutters from eBay (specifically this guy [http://r.ebay.com/YIX0ji](http://r.ebay.com/YIX0ji)) and I've been playing with it over the weekend. Unboxing, setup, and testing went without much trouble. Calibrating the laser took a while but eventually I figured it out. I can't say it's perfect yet but rather, 'it's a work in progress.'



A couple of things that I immediately noticed and I was wondering if anyone here has any suggestions/recommendations:



a) Straight out of the box, its 0,0 position is about 25mm away from the left rail. Looking in LaserDRW and just about any setting I could find, it says that's 0 for X. Manually I can move the head all the way to the left and still have the laser clear the rail, so I could conceivably print in that area, but I can't figure out how to reset the zero limit on it. Any suggestions?



b) Alternatively, I suppose I can simply print wider by setting the proper size in the print dialog and shift the work piece over by that same amount (25mm or so.) But there has to be a better way, no?



c) Mine did not come with an air assist, do I need an actual compressor to install one, or can I use an aquarium AIR pump? Do I actually need a lot of air flow around the nozzle, or is it just to help push the fumes/smoke out of the way of the laser?



d) I can't seem to figure out a vertical focus, or if I even need to deal with that. The reason I bring this up is because the bed that it comes with, has that spring loaded little "window" to supposedly hold a work piece in it, but that's such a small piece! I can simply put my larger work piece right ON the bed and cut away. So I've been thinking of removing that bed all together and replacing it with a piece of sheet metal and a honey comb on top of that. However, without cutting the standoffs, this would raise the work piece by at least 1/4", so I was wondering if focus becomes an issue ... and if so, how do I check/adjust/fix that?



And lastly, at least for the time being,

e) I'm currently only using the Chinese supplies LaserDRW by importing files I create in other applications, like Illustrator. But what I'd like to figure out is whether I can use Illustrator to create both engraving and cutting layers and load it in LaserDRW to have it do it all in one shot. Right now I engrave first, then do a second 'print' to cut the piece out. I won't (or rather, can't) use the pirated copy of CorelDRAW and CorelLASER that came with the thing, not only because it's pirated, but also because the version supplied won't work under Windows10.





**"Ashley M. Kirchner [Norym]"**

---
---
**David Wakely** *September 28, 2015 18:42*

Wow that's a long post! Welcome to the community.



I can say from experience that you should defiantly rip the old cutting bed out and replace it with some thing that you can adjust the height on. I personally use a piece of egg crate and a lab jack to adjust the height. It gives me much better ability to work on different materials. 



Focus is important and the best way to measure it is to take out the cutting bed and place a piece of wood on a sloping angle with one end above where the old table was and the other below it. Cut a straight line on it and the part where the line is the thinnest and cleanest is your focal point. Measure that point to a reference point on the machine i.e the head carriage and cut your self a tool so you can always measure it easily.



As for air assist it's a definite must for cutting. I use a 40lpm piston pump for an aquarium cost me around £25 and a 3d printed air assist nozzle around £10 from eBay. These defiantly make a massive difference to cutting



These machines cannot support cutting/engraving in the same job without the Lightobject DSP upgrade which is expensive and a hell of a lot of work.﻿



Anything else let me know!



Happy lasering!!


---
**Ashley M. Kirchner [Norym]** *September 28, 2015 18:56*

Thanks for the suggestion on how to find the focal point. The standoffs on this one were all bend sideways to match the holes on the bed. Shoddy workmanship ... but then, what'd you expect for sub-$400. :)



Meh, I was afraid of the answer as far as engraving and cutting in one 'print' command. Evidently, if I used the CorelLASER software, I can do that by drawing things on different layers and then telling it to queue up tasks, first the engraving, and then the cutting. But seeing as how the Corel software provided is pirated and doesn't work on Win10, I'm stuck trying to figure out a different solution. As I mentioned, right now I have to hit engrave then come back after it's done and do a second print job for the cut pass. Tedious but it'll do for now.



I'll look into an air pump and get that installed, as well as a freaking safety cut-of switch on the lid to kill the laser if the lid is opened. I have some other ideas for improvements as well but for now, one thing at a time.



I did read about replacing the electronics for a DSP board and I may still do that. I'll just have to figure out all the wiring and pins and what not. For someone who's been working with electronics for some years, it shouldn't be all that hard to figure out.


---
**Ashley M. Kirchner [Norym]** *September 28, 2015 18:59*

Oh, as for adjusting the height on whatever replacement I decide on ... that may be yet another project some day. I can see me rolling a 3D printer controller variant into this. After all, they have XYZ and E (for extrusion). So I could conceivably use that (use E for turning the laser on or off.) But then I saw some DSP controllers that can not only do that, but also control the power of the laser which is also beneficial ...


---
**David Wakely** *September 28, 2015 22:13*

DSP controller is defiantly the way forward but very expensive.



As for queueing tasks I tend not to do this as I forget to adjust the power once a task is complete. I find it much easier to run the jobs separate. I also find the use of layers much more helpful. 


---
**Ashley M. Kirchner [Norym]** *September 28, 2015 22:43*

And therein lies the next problem, power adjusting. At least on mine it's a manual pot on the control panel to adjust the laser output, which, as you pointed out, gets iffy when queuing tasks. It would be so much better if I can set the output in the software based on what I'm queuing. But alas, I don't think LaserDRW has that capability.



Is there something else that would work, or is that part of replacing the control board as well?



Also, I haven't found a way yet to reset the x-value for its 0,0 home position - mine is about 25mm away from the left rail and I'd like it closer.


---
**Ashley M. Kirchner [Norym]** *September 28, 2015 23:54*

Interesting, the documentation that came with it specifically say not to go above 6mA. And at 6-7mA, I'm cutting through 1/8" acrylic just fine (at 6mm/s). I suspect thicker material would require a higher setting, but then I'd be going against their recommendation of max 6mA ...



As for the air assist, is there some documentation that anyone has put together in terms of how it should be done? Where/What should the air be blowing to/at? Since I have my own 3D printer, I can easily print a nozzle for it, but I'd still need to know where the thing goes or what it's pointed at ...


---
**Ashley M. Kirchner [Norym]** *September 29, 2015 00:49*

Would you happen to have a link to the Harbor Freight one?


---
**Ashley M. Kirchner [Norym]** *September 29, 2015 01:22*

Oh wow, that's bigger than I had anticipated ... I thought you were talking something the size of an aquarium air pump. :)


---
**Joey Fitzpatrick** *September 29, 2015 02:56*

If you can get corellser up and running, you will like it better.  with corellaser there is a way to cut and engrave in the same job. You simply "add task" and then you are prompted to start the next job after the first one is finished.  There is a tutorial somewhere on this forum.  You will need to be sure to add a 1mm object in the top left corner to keep your engravings and cuts aligned.


---
**Ashley M. Kirchner [Norym]** *September 29, 2015 02:59*

**+Joey Fitzpatrick**, CorelLASER runs ... as long as I don't enable layers, which is what is needed. As soon as I enable layers, it crashes. No amount of removing, reinstalling fixed it. And I tend to change the laser's power setting between cutting and engraving, which I realized wouldn't help much when doing everything in one output stream.


---
**Joey Fitzpatrick** *September 29, 2015 13:57*

**+Ashley M. Kirchner** I had a similar problem with corel crashing.  I had software installed ,on my PC ,that was a resource hog(solidworks in my case)  When ever I tried to run jobs that were large, the PC would lock up.  I wound up creating a multi boot system on my laptop and dedicated an operating system solely for my K40.(I use Boot-it-Bare-Metal)  It is kind of a Pain in the @$$ to set up, but it works great.  You could always use a spare PC and just dedicate it for Laser Cutting and Engraving.  

Also -- here is a link to the youtube video that shows the workflow process to engrave and cut in the same job.    
{% include youtubePlayer.html id="MR7967VHKnI" %}
[https://www.youtube.com/watch?v=MR7967VHKnI](https://www.youtube.com/watch?v=MR7967VHKnI)       Keep in mind that the video does not show the trick about creating a 1mm object in a master layer to keep the jobs aligned.


---
**Ashley M. Kirchner [Norym]** *September 29, 2015 15:14*

Yeah, I would rather convert to a completely different setup. Ideally I'd switch out the controller for something much better and perhaps open source and get rid of LaserDRW and its required dongle. Having to manually control the laser output is really not the way to go if one wants to run a single job for engraving and cutting, it would have to be done in the software but the hardware needs to support that of course. 



For now I'll stick with it, do the small improvements I have in mind like a cutoff switch on the lid, a better fit for the exhaust fan,  adding air assist, and install some internal LED strips to light up the work area, replace the bed for a piece of sheet metal and a honey comb on top. I also need to do some minor fixing of the "control" panel as some of the buttons aren't fitting properly.



For the amount of money that these machines cost, they're OK. With some improvements, they can be so much better. 


---
**Raja Rajan** *October 05, 2015 20:24*

Hi Ashley,Exactly I bought the same one ,but when i tried to setup for one week (I am unable to engrave anything) .



Do we need to ground the wire?(the back side ) is that mandatory?



I ahve connected all water inlet and outlet and Fan and Laserdraw software and and initialize the device everything.



When i hit engrave i don't see any laser beam coming to acrylic.



I have tested the optical path as well,Except grounding i have done everythin.Am i missing something?



PLZZ HELP!!!


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/YFdwqRBYMPA) &mdash; content and formatting may not be reliable*
