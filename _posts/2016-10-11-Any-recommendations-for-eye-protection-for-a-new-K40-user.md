---
layout: post
title: "Any recommendations for eye protection for a new K40 user?"
date: October 11, 2016 20:50
category: "Discussion"
author: "James Rivera"
---
Any recommendations for eye protection for a new K40 user?  I still haven't so much as plugged it in, and while I do plan on always having the lid shut while operating it, I would still like to have proper eye protection (read: laser safety glasses that block the wavelength this laser emits).





**"James Rivera"**

---
---
**Jim Hatch** *October 11, 2016 21:01*

Amazon has some. Need to be certified for 10600nm which is the CO2 laser spectrum. Regular safety glasses made of acrylic will defract (the window in the lid is acrylic) but won't stop the beam from boiling your eyeball 🙂


---
**James Rivera** *October 11, 2016 21:07*

**+Jim Hatch** Thanks! Any links to possible candidate eyewear?


---
**Don Kleinschnitz Jr.** *October 11, 2016 21:45*

Second to getting the proper glasses. Add interlocks to the doors including the back door.


---
**Dan Stuettgen** *October 11, 2016 23:00*

Go to [lightobject.com](http://lightobject.com).  they have the glasses you need.  They run about $35.00.  


---
**Bill Keeter** *October 11, 2016 23:06*

yeah, they sell them through Amazon too. Also someone said the same glasses are cheaper through ebay from another seller. 


---
**Jim Hatch** *October 11, 2016 23:16*

I got these: [amazon.com - Laser Safety Eyewear - Co2/Excimer Filter In Black Plastic Fit-Over Frame Style. - Safety Glasses - Amazon.com](https://www.amazon.com/gp/product/B000HJMS4A/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)



And these: [https://www.amazon.com/gp/product/B00L2SU1TY/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00L2SU1TY/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)



I prefer the fit of the first ones. Make sure whatever you get provide some side protection as well as just the fronts.


---
**James Rivera** *October 12, 2016 00:46*

**+Dan Stuettgen** **+Jim Hatch** Thanks!  I'll check those out.


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/Ux2wvdUCi2F) &mdash; content and formatting may not be reliable*
