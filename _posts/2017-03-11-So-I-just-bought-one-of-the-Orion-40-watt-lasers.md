---
layout: post
title: "So I just bought one of the Orion 40 watt lasers"
date: March 11, 2017 18:12
category: "Modification"
author: "Shawn Thompson"
---
So I just bought one of the Orion 40 watt lasers. What board is suggested to change out to use Corel draw or anything that is better than this Nano M2 board that is in it. The drawing program that came with it leaves alot to be desired to say the least.

TIA

Shawn





**"Shawn Thompson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 18:25*

The smoothie upgrade is the way to go and many people in this group have used the Cohesion3D Mini as a drop in upgrade: 

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Shawn Thompson** *March 11, 2017 18:29*

Thank you. Just got on this group and found the mods list and all. Thank you for the fast reponse




---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 18:32*

Sure thing. The Cohesion3D Mini drops right in to replace the stock board, gets you full grayscale control for engraving, and is an all around affordable board that runs smoothie firmware. 

If you have any questions let me know, and good luck with your machine!


---
*Imported from [Google+](https://plus.google.com/117105678915986397850/posts/4PyreM6xZVK) &mdash; content and formatting may not be reliable*
