---
layout: post
title: "Been trying to get my new toy up and running"
date: August 01, 2015 18:28
category: "Software"
author: "Niclas Jansson"
---
Been trying to get my new toy up and running. 

Seems a bit to slow even though I tried to max out the speed 500mm/s.. Doesn't really feel like the speed settings is effecting it at !?



What am I doing wrong?



![images/8fa48dcdbe87eef10e1f6e2b3392f180.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8fa48dcdbe87eef10e1f6e2b3392f180.jpeg)
![images/c10a810168a9408edbd7c11c27fee0f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c10a810168a9408edbd7c11c27fee0f8.jpeg)

**"Niclas Jansson"**

---
---
**Jon Bruno** *August 01, 2015 18:30*

Hmm... 400,000 mm/sec?

Try 400.000 and see if it behaves differently﻿


---
**Jon Bruno** *August 01, 2015 18:32*

I'm not sure if ","  = "."﻿ so I'm not sure if any of your settings are valid


---
**Niclas Jansson** *August 01, 2015 18:34*

it is a ,


---
**Jon Bruno** *August 01, 2015 18:36*

Well if it's a , (comma) your machine will never run that. The limits on these steppers falls just below 450mm/sec however 400.000 is 400mm/sec not 400 thousand mm/sec


---
**Jon Bruno** *August 01, 2015 18:40*

But also, I notice your company field and model type field are filled with gibberish as well. I believe that has something to do with the font file but the selection itself seems wrong as well.




---
**Niclas Jansson** *August 01, 2015 18:40*

can't even change it, it puts a comma ( , ) if i just write 400

And if i write a period ( . ) i get a windows "Boing" sound telling me i'm being naughty..


---
**Jon Bruno** *August 01, 2015 18:41*

Interesting.

Where did you get the software from?


---
**Niclas Jansson** *August 01, 2015 18:42*

I got a bag in the package, it was suposed to hold a manual but it was empty accept for the usb dongle, so i don't even know what i own :(


---
**Jon Bruno** *August 01, 2015 18:44*

If you have a 6C6879 Lihuiyu board you can grab the updated software from the website.


---
**Niclas Jansson** *August 01, 2015 18:44*

The software is from the included CD.

Coreldraw 12 and two other things where installed, a driver and some Chinese app.


---
**Niclas Jansson** *August 01, 2015 18:44*

Can i see what i have on the machine somewhere ?


---
**Jon Bruno** *August 01, 2015 18:45*

I remember my install was funky too but I reinstalled with some updates and the craziness went away. 


---
**Niclas Jansson** *August 01, 2015 18:46*

Ok, I'll try to get my hands on some new stuff then =)


---
**tony alexander rico cera** *August 01, 2015 18:49*

150 speed


---
**Niclas Jansson** *August 01, 2015 18:58*

Problem is solved, thanks for the help.

I was trying to engrave from Coreldraw, that totally didn't work. When using the Chinese software, it works :D



And the model looks like it should now. 

I'll try to update some stuff and try CorelDraw again..

or is that totally broken ?


---
**tony alexander rico cera** *August 01, 2015 18:59*

Corel 7 not.... Use x6 


---
**Jon Bruno** *August 01, 2015 19:34*

Make sure your model and serial number jive in the Corel driver.

They aren't carried over from one to the other. You may set it in laserdrw but you have to re-enter it into the Corel side too. Unfortunately it's not like a regular printer driver.


---
**Sebastian Szafran** *August 01, 2015 22:38*

It looks that you have a Moshiboard with USB key and Chinese software. I didn't manage to use it for my projects. 

Currently I am replacing electronic with RepRap board which allows to read files from most of the software. 


---
**Joey Fitzpatrick** *August 02, 2015 16:22*

It looks like you have the mainboard configured as a xxxxx-xxxxx-B1 in corel draw.  Look at your mainboard number (In corel draw settings)and make sure you have the right main board selected.  Most of these newer machines have a xxxxx-xxxxxx-B2 board(Lihuiyu Studio Board)  Like Jon Bruno said, the settings are not carried over to coreldraw when laserdraw is installed.  engraving and cutting should work normally from corel/laserdraw.  I made the same mistake when I first installed the software(It engraved VERY slow)


---
*Imported from [Google+](https://plus.google.com/105892779133697678832/posts/Xyr33AUP94L) &mdash; content and formatting may not be reliable*
