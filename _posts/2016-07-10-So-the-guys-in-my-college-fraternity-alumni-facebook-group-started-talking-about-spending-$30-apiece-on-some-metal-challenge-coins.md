---
layout: post
title: "So the guys in my college fraternity alumni facebook group started talking about spending $30 apiece on some metal challenge coins"
date: July 10, 2016 07:09
category: "Object produced with laser"
author: "Ned Hill"
---
So the guys in my college fraternity alumni facebook group started talking about spending $30 apiece on some metal challenge coins.  I was like, guys I've got a laser machine now I can make something.  So I came up with this wooden challenge coin and even designed a small display stand to go with them.  Made from 1/8" alder.



![images/089e33ec1ef7f1859f9a1834a3e9dc4b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/089e33ec1ef7f1859f9a1834a3e9dc4b.png)
![images/3cf00296ccfb6ffaf80c09b5b0108afa.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3cf00296ccfb6ffaf80c09b5b0108afa.png)
![images/d84245bc7976ac3b1826018e6eb770ea.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d84245bc7976ac3b1826018e6eb770ea.png)
![images/0c57e18e9fb8540e51559b151709e7da.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0c57e18e9fb8540e51559b151709e7da.png)

**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 10, 2016 07:29*

As I'm not from America, I'm unfamiliar with this stuff. What is a challenge coin? What you've made looks great.


---
**Scott Marshall** *July 10, 2016 08:16*

Nice work!



(I AM from America, but didn't do the Fraternity thing when in school ( I was too busy fixing TV's to pay my tuition, rather sorry I missed out on it though)


---
**Ned Hill** *July 10, 2016 12:54*

**+Yuusuf Sallahuddin** Thanks Yuusuf.  You can read about challenge coins here [https://en.wikipedia.org/wiki/Challenge_coin](https://en.wikipedia.org/wiki/Challenge_coin)




---
**Jim Hatch** *July 10, 2016 13:42*

Nice. I designed something along those lines for the MakerSpace UAV challenge. I did a wood & acrylic sandwich so the surface was dust proof (no exposed crevices/engraves) and I got both dark wood engraves and acrylic etch effects. 



I like your stand design. Nice and simple but just what's need to keep it as a display item vs sitting in a drawer.


---
**Ned Hill** *July 10, 2016 13:46*

Thanks **+Jim Hatch** :)  Do you have a pic of the UAV coin?  Would love to see what you did with the combined wood and acrylic etching.


---
**Jim Hatch** *July 10, 2016 19:22*

**+Ned Hill**​ I'm on a business trip this week. I'll have to run into the MakerSpace to get a photo. I think we kept some for the sample display we drag around to different Maker events.


---
**Ned Hill** *July 11, 2016 01:54*

**+Jim Hatch** No problem, just if you ever get a chance.  You've got me thinking now.  Question, what did you use to join the acrylic and wood?


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/LaRLYkEvX2X) &mdash; content and formatting may not be reliable*
