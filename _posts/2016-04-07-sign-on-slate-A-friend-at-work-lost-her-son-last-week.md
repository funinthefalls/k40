---
layout: post
title: "sign on slate A friend at work lost her son last week"
date: April 07, 2016 03:58
category: "Discussion"
author: "HalfNormal"
---
sign on slate



A friend at work lost her son last week. The community is chipping in on a garden in his memory. He was 2 years old and had some favorite sayings. Hi Five was one and his nickname was Panda Bear. This is done on 1/4 inch slate tile. When it got to marking the panda, I notice it was not all there! When I had been adjusting the mirrors, I did not tighten one down. Had to redo the panda. As you can see, it all came out great.

![images/239adc4385b0b7b80be8498516f7335e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/239adc4385b0b7b80be8498516f7335e.jpeg)



**"HalfNormal"**

---
---
**Scott Thorne** *April 07, 2016 04:36*

Awesome work man...that came out good. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 06:38*

Looks great & good community project. Condolences to your friend.


---
**Anthony Bolgar** *April 08, 2016 09:19*

Nice. A great memorial for the little guy.


---
**I Laser** *April 08, 2016 10:43*

It's so sad, I really didn't want to comment. Hope the little guy is in a better place!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/RnYx97STgHL) &mdash; content and formatting may not be reliable*
