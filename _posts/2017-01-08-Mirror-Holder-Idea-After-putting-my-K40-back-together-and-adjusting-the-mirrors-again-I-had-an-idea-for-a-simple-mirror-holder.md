---
layout: post
title: "Mirror Holder Idea After putting my K40 back together and adjusting the mirrors again, I had an idea for a simple mirror holder"
date: January 08, 2017 17:10
category: "Modification"
author: "HalfNormal"
---
Mirror Holder Idea



After putting my K40 back together and adjusting the mirrors again, I had an idea for a simple mirror holder. It is not new but can be expensive to purchase. It is to make a gimbal mount to hold the mirror. This way the center of the mirror never moves and you get a more accurate convergence. Below is a link to one on Thingiverse that I think can be modified easily.

Watcha think?



[http://www.thingiverse.com/thing:320853](http://www.thingiverse.com/thing:320853)





**"HalfNormal"**

---
---
**Ned Hill** *January 08, 2017 18:21*

Interesting idea but I'm not sure how well you would be able to lock in the position. 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/MhAJfvf2VHh) &mdash; content and formatting may not be reliable*
