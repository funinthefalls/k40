---
layout: post
title: "Originally shared by HACKADAY Lining up the beam from your homebrew (or retrofitted) laser cutter doesnt come without its challenges"
date: March 10, 2016 11:12
category: "Hardware and Laser settings"
author: "Sebastian Szafran"
---
<b>Originally shared by HACKADAY</b>



Lining up the beam from your homebrew (or retrofitted) laser cutter doesn’t come without its challenges. For instance, how do I use my remaining eye to align an invisible beam that has enough power to burn through some objects in its path? Some of us will…





**"Sebastian Szafran"**

---
---
**Anthony Bolgar** *March 10, 2016 14:24*

A little difficult to do with a K40. No room to drop in the temp mirror.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2016 14:37*

Lol... How do I use my remaining eye...



One thing I have noticed when cutting is sometimes the laser beam is visible briefly in the smoke that is given off. So I wonder, can we use some kind of fog/smoke/dust to create a visible path for while we are aligning the beam?


---
**Anthony Bolgar** *March 10, 2016 18:03*

I am assuming a fog machine would be the best option, just not sure what kind of damage it may do to the moving parts (ie build up of a film of some sort)


---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/QT8Nch5B4rW) &mdash; content and formatting may not be reliable*
