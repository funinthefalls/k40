---
layout: post
title: "Question: I'm looking at my water in and out lines on the k40 and they are white"
date: June 09, 2017 21:33
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Question:



I'm  looking at my water in and out lines on the k40 and they are white. 



It is the stock lines that came with the machine and they were opaque originally. Now they are white like something is coating the inside. Machine is about 4 months old. I use distilled water and bleach. 



Has anyone else noticed this in their lines or is it a sign or something else?







**"Nathan Thomas"**

---
---
**HalfNormal** *June 09, 2017 23:42*

I might be incorrect but I believe it is a reaction of the lines to the bleach. Mine did the same thing. As long as you do not see any growth in the tube, you are ok.


---
**Ned Hill** *June 10, 2017 05:09*

I agree that it's probably the bleach.


---
**Don Kleinschnitz Jr.** *June 15, 2017 10:19*

Strangely, my machine has been sitting dormant and drained as I went off on a CNC router binge (another story) and yesterday I noticed that my pipes are white powdery inside? I do not use bleach!


---
**HalfNormal** *June 15, 2017 12:30*

**+Don Kleinschnitz**​ but you do use an additive which could react to the hose.


---
**Don Kleinschnitz Jr.** *June 15, 2017 14:14*

**+HalfNormal** algaecide but the white was not there when there was water in it??


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/ZAjBmqcCVZp) &mdash; content and formatting may not be reliable*
