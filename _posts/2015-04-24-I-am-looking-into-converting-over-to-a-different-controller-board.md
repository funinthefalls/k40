---
layout: post
title: "I am looking into converting over to a different controller board"
date: April 24, 2015 01:00
category: "Modification"
author: "Sean Cherven"
---
I am looking into converting over to a different controller board. I do not want to re-wire anything, so a drop-in replacement would be best.



What controller board do you recommend I switch to?

My budget is around $150





**"Sean Cherven"**

---
---
**Stephane Buisson** *April 24, 2015 07:04*

Great question!



I have still some hope on the couple visicut/smoothieboard, but do not have any news for some time. it will be nice if **+Arthur Wolf** could give us an update on the contest to know if **+Thomas Oster** win a Smoothieboard to be able to develop the Smoothieboard driver for Visicut.


---
**Tim Fawcett** *April 24, 2015 12:11*

Hi Sean, I have done the update to the RAMPS 1.4 - all information will be on the web in a few hours.


---
**Sean Cherven** *April 24, 2015 14:06*

Does the RAMPS 1.4 conversion/firmware capable of Raster Engraving?


---
**Tim Fawcett** *April 24, 2015 17:07*

I haven't done any rastering with it but it is supposed to be capable of it.


---
**Tim Fawcett** *April 25, 2015 16:59*

OK now I have done some rastering - only limited, but looks like it may be promising


---
**Sean Cherven** *April 25, 2015 17:02*

Hmm... Rastering is kinda important to me... Any other ideas?


---
**Tim Fawcett** *April 25, 2015 17:24*

I guess it depends on what you want to do - the TunkkeyTyrrany setup does not seem to like pure raster data imported inot Inkscape but can cope with native within Inkscape


---
**Sean Cherven** *April 25, 2015 17:33*

Do you know if the LasaurApp works with Raster Engraving?


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/HX37KyjDESf) &mdash; content and formatting may not be reliable*
