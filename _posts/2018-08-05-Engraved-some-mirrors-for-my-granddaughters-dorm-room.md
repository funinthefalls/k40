---
layout: post
title: "Engraved some mirrors for my granddaughter's dorm room"
date: August 05, 2018 22:19
category: "Object produced with laser"
author: "HalfNormal"
---
Engraved some mirrors for my granddaughter's dorm room. Put some colored construction paper behind the etchings to give it some color and make it stand out.



![images/7dd535dbf0089fd20af831c7630e5dec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7dd535dbf0089fd20af831c7630e5dec.jpeg)
![images/e91294f0f0709e8c5840fa2d3432ac2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e91294f0f0709e8c5840fa2d3432ac2b.jpeg)
![images/10b061e8d4937562ab2639d9ad3e8536.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/10b061e8d4937562ab2639d9ad3e8536.jpeg)
![images/e1934a44bf3f217f4ea8c77630697653.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1934a44bf3f217f4ea8c77630697653.jpeg)

**"HalfNormal"**

---
---
**Fook INGSOC** *August 05, 2018 22:53*

Impressively Cool!!!


---
**Don Kleinschnitz Jr.** *August 05, 2018 23:44*

I wanna do this... Looks great. 

Settings ? 

Engrave from front or back? 


---
**HalfNormal** *August 06, 2018 01:06*

**+Don Kleinschnitz** It is engraved from the back of the mirror. You have to reverse the image. It was done at about 200 mm/sec at 70% power. You have to experiment a bit because not all mirror backing is created equal. The first one I did, I had to go over twice because it did not burn all the material away to the glass.


---
**Ned Hill** *August 08, 2018 23:09*

Cool idea!


---
**Nigel Conroy** *August 21, 2018 20:32*

Very nice idea


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/jmD4RQACfEe) &mdash; content and formatting may not be reliable*
