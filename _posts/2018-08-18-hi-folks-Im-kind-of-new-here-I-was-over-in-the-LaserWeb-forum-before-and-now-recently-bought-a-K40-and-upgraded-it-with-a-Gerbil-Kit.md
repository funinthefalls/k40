---
layout: post
title: "hi folks, I'm kind of new here, I was over in the LaserWeb forum before and now recently bought a K40 and upgraded it with a Gerbil Kit"
date: August 18, 2018 08:39
category: "Hardware and Laser settings"
author: "Maxim Matthew"
---
hi folks,



I'm kind of new here, I was over in the LaserWeb forum before and now recently bought a K40 and upgraded it with a Gerbil Kit. And hope you guys can help me with my currend emergency issue.



I was working in LaserWeb all perfectly good. But... laser quality was not that good. So I was trying LightBurn. LightBurn does offer an option for the Gerbil board to choose. It has the right workingarea dimensions and also knows my home position is in the upper left corner. all good.





Until I hit the go to home button. than my laserhead was trying exit the box on the bottom left corner. ugly sound.



I turned off my machine and after that nothing…

not in laserweb not in lightburn.

jogging or moving to another position is not working.



the motors looking ok... I can move the head when machine ist turned off, and when on.. it has a tight grip.



I have the feeling something went wrong in my setting:



Connecting Machine @ USB,COM4,115200baud 

Machine connected



 Firmware grbl 1.1e detected

 $$

 $0=10

 $1=255

 $2=0

 $3=0

 $4=0

 $5=1

 $6=0

 $10=31

 $11=0.010

 $12=0.002

 $13=0

 $20=0

 $21=0

 $22=1

 $23=3

 $24=600.000

 $25=1000.000

 $26=250

 $27=2.500

 $28=0.000

 $30=2048

 $31=5

 $32=1

 $100=160.000

 $101=160.000

 $102=250.000

 $110=0.000

 $111=10000.000

 $112=500.000

 $120=3000.000

 $121=3000.000

 $122=10.000

 $130=320.000

 $131=230.000

 $132=200.000





can anyone help or has an idea ?

kind regards

max







**"Maxim Matthew"**

---
---
**LightBurn Software** *August 21, 2018 07:10*

Your $110 setting is zero - that's your X maximum movement rate, so you're going to want to set that to the same as $111.



Once that's fixed, try issuing simple gcode commands in the console to make sure everything is moving properly:



G0 X0 Y0  should send it to the origin



G0 X10  should send it away from the origin in X

G0 Y10  should send it away from the origin in Y



If either of those two commands grinds, your machine is in negative coordinate space, and would need an offset applied to it for LightBurn to work.


---
**Maxim Matthew** *August 21, 2018 09:48*

**+LightBurn Software** thank you


---
*Imported from [Google+](https://plus.google.com/105883028987453428385/posts/3mwEVNYGZjo) &mdash; content and formatting may not be reliable*
