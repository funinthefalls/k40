---
layout: post
title: "I have never been on a forum with so many knowledgable users I asked 2 questions and got 2 great answers which worked fine so while I am a roll I will ask a third question if I may My brothers K40 has a knob and a meter to"
date: June 06, 2017 11:07
category: "Modification"
author: "Don Recardo"
---


I have never been on a forum with so many knowledgable users 

I asked 2 questions and got 2 great answers which worked fine 

so while I am a roll I will ask a third question if I may



My brothers K40 has a knob and a meter to adjust power levels , Mine has push buttons and a digital display. I want to make mine the same as his



I am confident about the swap except for the meter connections 

I have worked out where to conect switches for test etc and to put the pot to control the power but....

On his machine the meter is conected between the far end of the tube and ground

 but the return from the far end of the tube on mine goes through a big green resistor bolted to the side of the case of the laser and then to ground so..



What is the resistor for on mine ( my brothers doesnt have one )

do I remove the resistor and connect the meter the same as in his 

do I keep the resistor and put the meter between the tube and resistor

do I keep the resistor and put the meter between the resistor and ground ?



Cheers

Don





**"Don Recardo"**

---
---
**Adrian Godwin** *June 06, 2017 11:21*

Any of those would probably work. It should also be possible to use a more sensitive meter connected to measure the voltage across the resistor instead of the current through it.



What value is the resistor ?




---
**Don Recardo** *June 06, 2017 11:36*

Hi Adrian . The resistor measures about 30K ohms 

Its a big ceramic jobbie about 3" long and an inch in diameter.



Just for a test I set my power level to 50% and measured between the hot end of the resistor and ground as I pressed the test button using the mA scale on my multimeter

From what I read It seems most tubes run about 23mA full power so I was looking for about half that and sure enough

I read 11.6 mA so it looks like if I leave the resistor in place 

and just put the meter between the hot end and ground I am good to go .

I just wonder why they put the resistor on mine with a digital read out and not on the ones with analogue readout .

Don 






---
**Joe Alexander** *June 06, 2017 13:28*

i think the big green load resistor is added dependant on the type of laser PSU. My old K40 psu has it while the new one i replaced it with doesn't need it. ma meter was between resistor and laser psu on old, now tube end and laser psu with new. only remove the load resistor if you change your power supply imo.

Tube end------resistor------laser PSU

tube end-------laser psu 


---
**Don Recardo** *June 06, 2017 13:29*

Thanks Joe. I will keep the resistor inline


---
**Don Kleinschnitz Jr.** *June 06, 2017 13:50*

**+Don Recardo** the resistor is a ballast resistor and is necessary if your machine came with it. If you change your supply the new ones doesnt need it. 

The ballast resistor limits the current when the tube ionizes because it exhibits negative resistance characteristics.



The meter ( if you are using a milliamp meter) will need to be in series with that resistor, the cathode of the tube and ground.

If you put the meter across the resistor you will need to measure VOLTS and calibrate the meter accordingly. I suggest using an analog milliamp meter in series.

Typically we run a 30ma meter an amazon version is linked in my blog...



Here is how to add a meter: [http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html)



Here is how to add a pot if you want to loose the digital control, otherwise just add the meter:

[donsthings.blogspot.com - When the Current Regulation Pot Fails!](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)



Here is the main blog:

[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Nate Caine** *June 06, 2017 15:51*

Don Recardo, the first two questions are free.  Normally we start charging at the third one, but we'll let it slide this time.



Based on what's you've described, you have an older power supply (as do I), but mine uses a 51K-ohm ballast resistor.



One thing that I'll caution you about is the voltage across that resistor.  In my case 51K x 20mA = 1,000V  ...so be careful!  In fact, I added a vinyl sleeve around the wire from the tube cathode to the resistor (see photo).  The existing wire is only rated at 600V, and is in direct contact with the grounded metal chassis, so that sleeve adds a bit more margin.



I don't like Adrian's answer, and I don't follow your measurments that followed. 



Joe Alexander's answer is correct (and Don Kleinschnitz filled in  details).



(tube cathode) --> (resistor) --> (+ mA meter - ) --> (PS laser return input)



=============================

Now to add to the confusion....technical stuff...



On <b>NEWER</b> power supplies, the "laser return" is a terminal on a connector on the power supply, and that is directly connected to the chassis ground inside the HVPS.  You could verify this with your multimeter.  It should read zero ohms to chassis ground.  (These power supplies use an <b>indirect</b> technique to regulate the tube current based on the setting of the laser power adjustment input--either a pot or input voltage.)



On <b>OLDER</b> power supplies (mine and others), the "laser return" is usually a dedicated wire soldered right to the HVPS board.  Inside the HVPS there is an additional circuit that conditions and shapes the Laser Return current and then shunts it to ground.  So technically, this wire is "laser return" and not "ground".   (With the machine off, if you measure that wire input directly to chassis ground.  It should read about 300-ohm--could be different on various supplies.  I'd be interested in this result.)



That internal circuit also gives a signal back to the HVPS control circuit.  So it <b>directly</b> monitors the laser current, and makes slight adjustments to the HV(+) output to the tube anode, in order to tightly regulate the tube current.  Typically, these older HVPS supply <b>only</b>  HV to the tube, and rely on a separate LVPS for the +24V and +5V for the machine control.  



In some HVPS supplies, there actually is an internal jumper that allows you to connect the Laser Return wire directly to chassis ground, or to select the tube return current monitoring option.  One manufacturer suggested <b>direct ground</b> for better <i>cutting</i>, and <b>current monitoring</b> for better <i>engraving</i>. 



The best I can figure is that no two K40 machines are wired the same.  At this point there appears to be at least two flavors of the new power supplies, and two of the older supplies.







![images/c58baf01418887a69b7b3ea6409d871c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c58baf01418887a69b7b3ea6409d871c.jpeg)


---
**Nate Caine** *June 06, 2017 16:11*

Here's a picture of Nate Caine's HVPS label.  

This is a HVPS only.  It incorporates laser tube current monitoring.  



In my machine there is a separate LVPS for +24V and +5V.



![images/a99464245524126084e1038442aea0c3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a99464245524126084e1038442aea0c3.jpeg)


---
**Don Kleinschnitz Jr.** *June 06, 2017 16:53*

**+Nate Caine** the most recent schematic is here with most of the corrections ... I think.

I also have rationalized, to my satisfaction anyway, how the drive works. Thanks for you help on it ....

......

One open question in my mind is the tubes current regulation. It has been suggested that these supplies regulate the tubes current.



It clear that the feedback circuit measures the current in the flyback primary and that adjusts the PWM. However I don't think that it really measures the current in the tube, unless there is some reflected current from secondary to primary I do not understand.



It seems to me that the current in the primary is a proxy for the current in the tube but not the real current, especially since the tube starts out as a high impedance and then goes negative resistance. 

.......

We probably should not hijack this post further so I will copy this to a fresh one for a discussion.



[donsthings.blogspot.com - K40 Laser Power Supply Driver Circuits](http://donsthings.blogspot.com/2017/04/k40-laser-power-driver-circuits.html)






---
**Nate Caine** *June 06, 2017 17:07*

**+Don Kleinschnitz** I think that's what I mentioned before.  The HVPS control circuitry processes a variety of inputs (some for control, some for fault detection) and modulates the drive to the HV transformer accordingly.  



As mentioned in my description of some <b>OLD</b> HVPS, they <i>do</i> <b>directly</b> sample the laser tube return current.



On several <b>NEW</b> supplies they <b>indirectly</b> monitor the drive current to the HV transformer as a proxy for the actual tube current.  Remember, it is a <i>transformer</i>.  So just as it steps <b>up</b> the voltage (by the turns ratio), it also steps <b>down</b> the current (by the same turns ratio).  This seems imprecise, but they calibrate out much of the sloppiness at the factory.  That's what one of those adjustment pots do.  



As mentioned, some HVPS use both schemes (indirect transformer drive current, and direct tube current monitoring), and allow the user to select between the two options.



Regardless, most supplies monitor the drive current to the HV transformer anyway as a quick response to a faults--mainly a high current response to an HV arc to the chassis.  They also need to respond to an open circuit failure.  


---
**Don Recardo** *June 06, 2017 21:10*

**+Don Kleinschnitz** **+Nate Caine**  and all others that helped 

thank you for the replies

They were really usefull ( well untill you got too technical )



I have a 30mA meter on its way to me and I already have a nice 10 turn pot which even has a display on the end like a clock face so you can read its set value 0 - 100.

I will be taking out the old digital display board out and putting in the pot and meter following your description of Tube-Ballast resistor-Meter-PSU



I just measured the resistance on the wire returning from the ballast to the PSU and it reads less than 0.5 ohm on the 200 ohm range I took a picture of the HV end of the PSU and the return wire goes to the left most terminal of a 4 way plug and its labeled FG which I assume is Frame Ground , Does this mean its straight to the frame and that I could actually just connect it to any handy earth spot on the frame ?



I will let you know how it goes , but thanks for the help 



**+Nate Caine**. I didnt realise only the first two questions were free. I will try to remember to bring my wallet next time but at my age the memory is rather flakey :-)





![images/60c2f11f788f5858bacee11200641ea7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60c2f11f788f5858bacee11200641ea7.jpeg)


---
**Don Recardo** *June 06, 2017 21:14*

![images/d0411c8b0ced7dafe13ffd59856980be.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d0411c8b0ced7dafe13ffd59856980be.jpeg)


---
**Nate Caine** *June 07, 2017 17:28*

**+Don Recardo** Yes, according to your measurements in <i>your</i> machine the laser return at the HVPS goes straight to ground (chassis ground, frame ground, etc).



The connector in the photo is labeled:

[FG] [AC] [AC] [L-]



And you said the wire from the laser tube, thru the ballast resistor, goes to the [FG] "Frame Ground" terminal.  It <i>should</i> go to [L-] "Laser Negative", but since you said they are connected together on the board, I suppose there is no harm done.



I'd encourage you to carefully document your machine before you start making modifications.  Plenty of photos, wire colors, and signal names.  It's a lot easier to document a working machine, than a broken one.  If you have to disconnect similar colored wires, tag them with a label before you start.



============

Side note:  Your HVPS uses the terminology [L-] for the "Laser Negative" return terminal.  Elsewhere on the HVPS you <i>might</i> see a terminal lablel [L] which is <b>NOT</b> the same as [L-].  The [L] is just a shorter version of what other HVPSs call  [TL] "Trigger Low". [TL]  comes from the controller board and tells the HVPS to fire the laser when the [TL] signal is logic low, i.e. 0v.  You should take a few photos of your power supply and connectors and share them here.


---
**Don Recardo** *June 07, 2017 20:27*

**+Nate Caine** Yes that all makes good sense to document and photograph all the wires and connectors before I start. I shall also only change one thing at a time , for example , remove the pwm wires from the digital board and put them on the pot . Remove the laser test wires from the digital board and connect them to the new test switch. I will also add a laser on/off switch which the digital board didnt have . It was all to easy to lean on the test fire button when your hand was in the machine , dont ask me how I know .

I will take pics of the PSU and connectors and post them here incase it helps any one else . I just have to wait for my 30mA meter to arrive now from Gibralter



Thanks for your help  and sorry but I forgot my wallet again



Don


---
**Don Recardo** *June 07, 2017 20:34*

I forgot to add . 

You may have noticed that the [ L- ] terminal on my psu , the right hand one on the 4 pin connector at the HV end of the PSU is connected to a green/yellow earth wire that is bolted  at the other end to the lasers external case 


---
**Don Kleinschnitz Jr.** *June 08, 2017 12:42*

**+Don Recardo** what do you mean by the "Lasers External Case"... picture?


---
**Don Recardo** *June 08, 2017 18:51*

**+Don Kleinschnitz** by external case I mean the big blue box its built in . here is a picture showing where its bolted on


---
**Don Recardo** *June 08, 2017 18:51*

![images/4a92b8fd43c6e8917453b442b72bbbd4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4a92b8fd43c6e8917453b442b72bbbd4.jpeg)


---
**Don Kleinschnitz Jr.** *June 08, 2017 20:55*

**+Don Recardo** thanks lol that makes sense now that I reread your post.


---
**Don Recardo** *June 08, 2017 21:00*

If i followed Nayes post correctly I believe he was saying that the laser tube return should go to L- and the  FG terminal should be grounded to the case. My machine is connected the other way around


---
**Don Kleinschnitz Jr.** *June 08, 2017 23:35*

**+Don Recardo** did not catch that. Yes the cathode return of the laser should go to -L on the AC power connector. The safety ground is FG. These are actually connected inside the supply thats why what you have works.

I actually ground my LPS FG to a bolt in the frame next to my LPS as I wanted to minimize high speed ground currents from going across the machine. Additionally I ran the green and yellow safety from the plug to that same lug.


---
**Adrian Godwin** *June 09, 2017 22:30*

In the light of your description of the resistor - yes, it's a ballast resistor, you shouldn't remove it and the voltage across it will be quite high (though could still be measured to determine tube current). I had thought it was a current sense resistor but that doesn't seem to be the case.

 


---
*Imported from [Google+](https://plus.google.com/106290033771296978518/posts/3p4Q5wXGCjW) &mdash; content and formatting may not be reliable*
