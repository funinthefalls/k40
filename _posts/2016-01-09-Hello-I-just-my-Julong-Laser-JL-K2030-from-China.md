---
layout: post
title: "Hello, I just my Julong Laser JL-K2030 from China"
date: January 09, 2016 08:49
category: "Hardware and Laser settings"
author: "Sunny Koh"
---
Hello, I just my Julong Laser JL-K2030 from China. (Flew to Hong Kong to collect as it was cheaper than shipping to Singapore) Paid 2400 RMB with free shipping to Shenzhen, includes Air Assist & Red Dot Laser, an upgraded linear track, a LiHuiYu Labs Board. Paid another 50 RMB for the Honey Comb board. Any suggestions on what to do other than calling them up for a new plotter part?

![images/6249751b8ccc98a937d7e2ba693c22bf.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6249751b8ccc98a937d7e2ba693c22bf.png)



**"Sunny Koh"**

---
---
**William Steele** *January 09, 2016 12:38*

That's a simple adjustment... Just loosen the screws holding the belt pulley rod to the motor and realign the gantry... Then tighten them back.  You will need to take the cover off the very front of the platform and you will see the motor underneath it.


---
**William Steele** *January 09, 2016 12:39*

The plate is just below the red box you drew in this picture... There is a single screw on either side holding it in place.


---
**Anton Fosselius** *January 09, 2016 13:34*

Oh. Mine looks the same :)


---
**Sunny Koh** *January 09, 2016 17:42*

**+William Steele** I will try when I get back to the office on Monday, any pictures on where I should be adjusting would be helpful. My main company staff would have to call the factory on Monday as he is formally from the mainland. Seems that it does't touch the end stop switch as well.


---
**Sunny Koh** *January 11, 2016 07:38*

Was an easy fix, they have a video on fixing, [http://v.youku.com/v_show/id_XNzMyODA2NTIw.html?from=s1.8-1-1.2#paction](http://v.youku.com/v_show/id_XNzMyODA2NTIw.html?from=s1.8-1-1.2#paction)



Seems that rotate it until you hear a click sound


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/7C6Pb28MKXS) &mdash; content and formatting may not be reliable*
