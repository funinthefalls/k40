---
layout: post
title: "I'm also looking into my options on adding a new bed to my K40"
date: April 29, 2017 19:23
category: "Modification"
author: "Andy Knuts"
---
I'm also looking into my options on adding a new bed to my K40. While at it I'm thinking about making it motorized... I found the one from lightobject ( [http://www.lightobject.com/Power-Z-Table-Bed-Kit-for-K40-Small-Laser-Machine-P722.aspx](http://www.lightobject.com/Power-Z-Table-Bed-Kit-for-K40-Small-Laser-Machine-P722.aspx) ) and I was wondering if anyone here is using it? Is it any good? And I'm also wondering if it's really worth the money or can we make one ourselves for less money?







**"Andy Knuts"**

---
---
**Adrian Godwin** *April 29, 2017 19:39*

I see that they also offer a scissor-lift table. It costs more, possibly because it can take heavier loads.  Are there other advantages or disadvantages ? 



It seems to me that the parts for a lightweight scissor lift mechanism could be mostly laser cut, though the threaded parts and belt for the screwjack design are in any case fairly cheap.


---
**Dave Durant** *April 29, 2017 21:49*

Do you have a part number for the scissor one at LO? I'm not seeing it.


---
**Mark Brown** *April 29, 2017 22:25*

There's this one, but it's too big for the k40



[lightobject.com - Motorized power Z table 600x400 50lb load](http://www.lightobject.com/Motorized-power-Z-table-600x400-50lb-load-P902.aspx)


---
**Steven Kalmar** *April 30, 2017 00:30*

There's this one. It looks like a good design.  



[hackaday.io - K40 Motorized Z axis](https://hackaday.io/project/19251-k40-motorized-z-axis)


---
**Russ “Rsty3914” None** *April 30, 2017 01:01*

Not too happy with this design...

The pieces were razor sharp. Do not rub your hand around the table edge.

The taps were stripped out.  Bad bearings, very cheap.  Turns very hard and now I know why it needs so much power.   Im actually designing a scissor lift table for my k40






---
**Martin Dillon** *April 30, 2017 01:19*

I made my own scissor type lift and I adjust it with a knob.  I should have made a double scissor like the one above.  I would say mine is  accurate to 1/16 inch but the lower it goes the more squishy it gets


---
**Russ “Rsty3914” None** *April 30, 2017 01:20*

That one is pretty nice, but not the z table puctured above...you got pics on yours ?


---
**Martin Dillon** *April 30, 2017 01:52*

Yes that one is nice.  I made mine because it was the simplest way I could think to make one with the stuff I had.  I will try and post a picture.




---
**Dave Durant** *April 30, 2017 03:53*

The K40 one from LO looks nice but it seems to lock you in to the original x/y size of a K40. Scissor designs look like they don't do that nearly as much.


---
**Russ “Rsty3914” None** *April 30, 2017 04:37*

Ya I making mine to utilize the whole area, why not


---
**Martin Dillon** *April 30, 2017 21:44*

here is a picture of my adjustable table. It is 2.5 inches thick at its lowest setting and gives me 1.5 inches of adjustment.  That includes the honeycomb top.

![images/f61a2c14fa81d99e503279fe438e6294.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f61a2c14fa81d99e503279fe438e6294.jpeg)


---
**Martin Dillon** *April 30, 2017 21:45*

Here is a picture of my top and also shows my air assist hose.

![images/aafb86c7914f858e8a4b260c557bf759.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aafb86c7914f858e8a4b260c557bf759.jpeg)


---
**Russ “Rsty3914” None** *April 30, 2017 22:21*

Well done !! Did you add stepper too control height adjust ?


---
**Martin Dillon** *April 30, 2017 22:44*

no.  there is just a knob connected to the threaded rod on the left side.  I thought about having it go through the side of the enclosure but there is plenty of room to adjust it inside the case.


---
**Adrian Godwin** *May 01, 2017 00:29*

Yes, the one Mark linked is the one I'd seen.


---
**Tony Sobczak** *May 01, 2017 01:09*

I've had one for 4 months now and no problems with it. 


---
**Steve Clark** *May 01, 2017 01:46*

I also have one and it works great. I am considering rebuilding it to expand to the full k40 travel.


---
**David Komando** *May 01, 2017 04:21*

I own one of these and it's so not thought out on so many levels. Frustrated I ended up taking it out completely and replacing with a $30 Amazon purchased scissor table. Not as pretty but it actually works 10x better. 


---
**Brooke Hedrick** *May 01, 2017 11:42*

I have one.  It works well. Getting just the right tension on the belt was the trick to avoid jams.


---
**Andrew Nichols** *May 01, 2017 13:06*

**+David Komando** Do you have a link to the scissor table you use?


---
**Martin Dillon** *May 01, 2017 13:31*

I did a quick search and this one looks like the best one.  I don't like the fact that the knob moves up with the table.  You would have to take off the table every time you adjust it.  [amazon.com - A-214B2 Karter Scientific Stainless Steel Lab Jack 4"x4" Max. Height 5.9 in (15cm): Jack Stands: Amazon.com: Industrial & Scientific](https://www.amazon.com/214B2-Karter-Scientific-Stainless-Height/dp/B006UKI63C/ref=pd_sbs_328_49?_encoding=UTF8&pd_rd_i=B006UKI63C&pd_rd_r=5T55JXBRCB597JRA6RME&pd_rd_w=RojfP&pd_rd_wg=53kmL&psc=1&refRID=5T55JXBRCB597JRA6RME#feature-bullets-btf)


---
**David Komando** *May 01, 2017 13:47*

**+Andrew Nichols** I ended up with the 8x8. Had to remove the bottom plate since it was so high the laser wasn't focusing. After removing the bottom plate though my laser focus was amazing! Not to say its the best solution out there but it did work for me.

Here is the link of the one I have:

[https://www.amazon.com/American-Educational-Laboratory-Scissor-Overall/dp/B00657ZTIU/](https://www.amazon.com/American-Educational-Laboratory-Scissor-Overall/dp/B00657ZTIU/)



Plan on modding the little scissor table to add a add a stepper motor. Will probably end up using the one from the Light objects Z-Axis table. 



BTW, some of the biggest issues I had with the Light objects table: 

- it wastes a lot of space in the K40 making it even smaller. 

- it didn't work properly after a day of messing with it, i saw how poorly engineered it was.

- My laser assembly module wasn't the standard and actually got hung up on the table to where my laser was no longer able to be homed. 




---
**Steve Clark** *May 01, 2017 16:28*

Interesting that some have had problems with the LO table. I've had no problems whatsoever. I've heard others comments and there are always a few in the mix that are having problems.



 In watching the table run I suspect the majority of problems come from the timing belt and pulley system driving the screws as I can see wobble. Even in mine which has no problems. I would think too much of this wobbling may cause cogging and motor stall under load.



The wobble could come from the bearing shafts not being concentric to the treads on one or both ends (Most likely).  Or the bearing pockets not aligned (less likely). The threaded shafts one or more are bent.



Another consideration is that the the drive nuts are to tight and binds against any screw wobble  because the design doesn't allow them to float horizontally to compensate.  A little horizontal float is of course acceptable to have because your desired function is vertical. 


---
**Dave Durant** *May 01, 2017 17:19*

Anyone have a link to a decent powered scissor table? Hopefully one that will take orders from a C3D board.


---
**Pigeon FX** *May 05, 2017 10:34*

I would happily buy a light object one if the shipping was not so high to Europe (not their fault), I really hope they get distribution in Europe. I have tried using the 4" Lab jacks, but both the ones I tried had far to much play in them to be a reliable solution.....surprised an Asian clone has not come out yet.  


---
**Steve Clark** *May 05, 2017 16:17*

They are made for LO in China. Ironic point to this is the owners of LO are of Chinese descent and last time I was there they were complaining about struggling to get good lead times from their suppliers.

 

This came up in a conversation I had with them as I was curious about getting parts made over there compared to having them made here.



I was considering getting a source because on some products I produce I'm buying local but the parts come from China anyway. (Shipping cost is prohibitive BTW  for small lots). 


---
*Imported from [Google+](https://plus.google.com/101382109936100724325/posts/CCKWxRqPFob) &mdash; content and formatting may not be reliable*
