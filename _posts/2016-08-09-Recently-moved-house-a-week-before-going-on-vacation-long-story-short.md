---
layout: post
title: "Recently moved house, a week before going on vacation, long story short.."
date: August 09, 2016 11:00
category: "Discussion"
author: "Linda Barbakos"
---
Recently moved house, a week before going on vacation, long story short.. I lost the damn USB key to the laser cutter. What to do now?! Husband says buy a new one haha but I want to know if there are any tricks to override the USB?





**"Linda Barbakos"**

---
---
**Jim Hatch** *August 09, 2016 11:42*

You can try the folks you bought it from. Or do the Smoothie board upgrade and use LaserWeb to drive it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 09, 2016 11:47*

Or find someone that did the Smoothie upgrade that has the USB key laying around still.


---
**Paul de Groot** *August 09, 2016 11:53*

I  have one for free after my upgrade. Just contact me privately and i get it to you


---
**Darren Steele** *August 09, 2016 14:32*

I have one which you can have too - I'm in the UK


---
**Linda Barbakos** *August 09, 2016 18:53*

**+Paul de Groot** **+Darren Steele** Thank you both so much for the offers!


---
**Ulf Stahmer** *August 09, 2016 21:57*

Hey **+Linda Barbakos**, looks like you've been helped out by forum friends!  That's great.  Here's a suggestion for you:  attach your K40 to a cheap USB hub, install it inside and plug your new USB key into the hub.  That way you'll always know where it is!


---
**Linda Barbakos** *August 10, 2016 04:13*

**+Ulf Stahmer** thanks for the suggestion, I've already tried to implement this after seeing it on the forum a few months back. For some reason, it didn't recognize the USB key.. On that note, I FOUND IT! **+Paul de Groot** **+Darren Steele** I had "safely" put it in an inside zipped compartment of a handbag I was favouring at the time of our move. Crisis averted 😅


---
**Paul de Groot** *August 10, 2016 08:44*

Hahahaha a woman's hand bag has so many secret compartments. My wife hides the kitchen sink and couch in it!


---
*Imported from [Google+](https://plus.google.com/107262725887494713165/posts/RXauo89wY9m) &mdash; content and formatting may not be reliable*
