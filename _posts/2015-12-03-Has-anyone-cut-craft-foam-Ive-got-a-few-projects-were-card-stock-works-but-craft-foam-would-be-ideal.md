---
layout: post
title: "Has anyone cut craft foam? I've got a few projects were card stock works but craft foam would be ideal"
date: December 03, 2015 16:42
category: "Materials and settings"
author: "Nathaniel Swartz"
---
Has anyone cut craft foam?  I've got a few projects were card stock works but craft foam would be ideal.  I've seen people cut A4 sheets of the stuff on diode based home built engravers without issue, but I was wondering about any gotchas





**"Nathaniel Swartz"**

---
---
**Stephane Buisson** *December 03, 2015 17:13*

I have no clue, but I would pay attention of fume intoxication, and fire hazard.


---
**Coherent** *December 03, 2015 19:00*

Craft foam (EVA) cuts easily is not very flammable and I've had good sharp cuts/results. Most types of foam contain thermoplastics which melt, so you'll have to test to see what speed and power works best. I've had good luck with 1/4in foam and then cleaned any char residue off edges with alcohol.

 As Stephane stated beware of toxic fumes. FYI some foams are a vinyl chloride product and will produce chloride gas when cut.

 Foams and foam core board that contains PVC is going to be flammable. PVC products are not recommended for laser cutting.


---
**Nathaniel Swartz** *December 03, 2015 20:24*

Thanks! Do good ways exist to tell pvc vs Eva?


---
**Coherent** *December 03, 2015 22:07*

Here is a link to a Hackaday article/video regarding identification of plastics prior to cutting. it may help.

[http://hackaday.com/2015/03/14/how-to-identify-plastics-before-laser-cutting-them/](http://hackaday.com/2015/03/14/how-to-identify-plastics-before-laser-cutting-them/)


---
**Nathaniel Swartz** *December 04, 2015 16:14*

The flame test works very well, I was able to test a few  types of plexiglass I had been wondering about as well as foam.


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/FN2BKijySz2) &mdash; content and formatting may not be reliable*
