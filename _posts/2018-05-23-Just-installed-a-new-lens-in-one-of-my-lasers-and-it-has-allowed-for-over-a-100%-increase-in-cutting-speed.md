---
layout: post
title: "Just installed a new lens in one of my lasers, and it has allowed for over a 100% increase in cutting speed"
date: May 23, 2018 05:10
category: "Modification"
author: "Anthony Bolgar"
---
Just installed a new lens in one of my lasers, and it has allowed for over a 100% increase in cutting speed. (I went from 8mm/s on 3mm MDF with a 60W tube to 18mm/s )The lens was made by II-IV Infrared, and purchased from Cloudray Laser on ebay. This is a high quality lens with a very small focus dot, and it is a game changer. Not too shabby for $50 CDN delivered within 2 weeks. Here is the link if you are interested. [https://www.ebay.ca/itm/CO2-Laser-II-VI-ZnSe-Focus-Lens-Dia-20mm-for-Engraver-Cutter-FL-50-8mm-2-inch/122175400409?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649](https://www.ebay.ca/itm/CO2-Laser-II-VI-ZnSe-Focus-Lens-Dia-20mm-for-Engraver-Cutter-FL-50-8mm-2-inch/122175400409?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649)





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *May 23, 2018 22:35*

**+Ray Kholodovsky** thoughts on how these compare to what you are selling?


---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2018 22:55*

We don’t have that exact size, just 12 and 18mm diameter for presumably smaller lasers. 

What we carry are also ZnSe lenses. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/29wSFKJc8AD) &mdash; content and formatting may not be reliable*
