---
layout: post
title: "While deeping into Moshidraw 2014, (not easy task because poor manual), I am looking at future possible mods"
date: November 11, 2014 00:44
category: "Hardware and Laser settings"
author: "Stephane Buisson"
---
While deeping into Moshidraw 2014, (not easy task because poor manual), I am looking at future possible mods.



good news the latest K40 power supply (MYJG40W) accept PWM and 5V analog out control.

[http://en.jnmydy.com/products_detail/&productId=3818917a-64be-4cb5-985e-6dc249ad7414.html](http://en.jnmydy.com/products_detail/&productId=3818917a-64be-4cb5-985e-6dc249ad7414.html)





Potential candidate could be with an Arduino shield from Mr BEAM. Got some 5v available from PSU. need to look into the steppers specs and compatibility.

if Mr Beam agreed to make them available to a large K40 users base. Maybe a kit with RaspeberryPi + UNO + Shield + open source Software (cut &raster). 

or just Shield & soft. (the rest could be source locally)



hopefully the price could stay entry level to go with the $430/€365/£275 of the K40 on ebay.



but I have read from Brook or Nils the software is still to come …

Will it become a standard like Octoprint did ?

 



other mod I am thinking about is a moving Z for focal setting inspired by 
{% include youtubePlayer.html id="CC9kGOlxlHs" %}
[China Laser Cutter From Ebay Adjustable Lens Mod. DCK40III](https://www.youtube.com/watch?v=CC9kGOlxlHs) , that could be combine with 3D printed air assist a bit like this one .[http://www.thingiverse.com/thing:72691](http://www.thingiverse.com/thing:72691)



I could design that one to be 3D printed.







**+Nils Hitze** **+Brook Drumm** **+Mr Beam** **+Gina Häußge** 

![images/0b1d3696c22d6c67b9d6ba6ba714ca37.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0b1d3696c22d6c67b9d6ba6ba714ca37.jpeg)



**"Stephane Buisson"**

---
---
**Other World Explorers** *December 26, 2014 07:30*

Can you please explain how to hook up  PWM and 5V analog out control; on this unit? 



That would go a long way for me to getting my DSP up and running without having to drop $235 USD on a replacement PSU. 



Thanks ! 


---
**Eric De Larwelle** *January 03, 2015 16:20*

I have a similar power supply. The PWM signal goes to the input marked "IN".


---
**Joshua Taft** *January 31, 2015 06:26*

I couldn't get the laser to fire when hooked into in. I have the ground wire from the supply 6 pin to the big mosfet power in. Then supply in is connected to the ground on p2_5. That did nothing for me. so i changed "in" to "L" and got the laser to fire. 10% was about 5mA, 20% was about 7mA. Any idea what i am missing? stop points are good. and steppers move well. Just haveing trouble with the laser fire. I have same supply as you do. what is your 5v on the 6pin connected to?


---
**Stephane Buisson** *January 31, 2015 16:08*

the red In(5) and the blue 5v (6) go to the potentiometer with the green L. all three wires are the thinnest.


---
**Troy Baverstock** *May 11, 2015 23:40*

This might be what I'm looking for, If the pot is used just for control voltages, can I used a much lower wattage pot? 



I'd either like to control a dual motorised pot from an arduino (reading the second output to determine pot position for current)



Or PWM directly, is there an actual example fo someone doing this?


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/FqNbGMzrwav) &mdash; content and formatting may not be reliable*
