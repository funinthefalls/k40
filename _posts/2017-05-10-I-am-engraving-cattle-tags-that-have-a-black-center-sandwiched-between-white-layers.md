---
layout: post
title: "I am engraving cattle tags that have a black center sandwiched between white layers"
date: May 10, 2017 17:13
category: "Hardware and Laser settings"
author: "Kristi Hawkins"
---
I am engraving cattle tags that have a black center sandwiched between white layers. I need help to try and speed up my process. Here are my parameters that I have tried:



Left tag (D1):

Speed 150, pixel 2, steps 1, power 30%

        This takes 1 hour 20 minutes for 6 tags



Right tag (D8):

Speed 300mm/S, pixel 1, steps .75, power 75%



        This takes 1 hour but in some places the laser doesn't get through the top white layer, so I have to go mover it again. The other concern is the lines that I have in the D8 tag.



Objective: decrease time because time=$

                   Smooth the engraving



Any thoughts would be helpful! Thank you!

![images/9f6bfb04220ebc6555420fb55f2d9a29.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9f6bfb04220ebc6555420fb55f2d9a29.jpeg)



**"Kristi Hawkins"**

---
---
**Jeff Johnson** *May 10, 2017 17:40*

I make a lot of drink coasters and I've found that I can really speed things up by setting the surface to just a little out of optimum focus. I'm using Corel Draw with the Corel Laser interface and I can set the "Pixel" to 5. I engrave at 400mm/s and set the power depending on how deep I want to engrave. Putting the surface slightly out of focus gives wider beam coverage and helps eliminate the lines. Of course you'd have to play with the distance from the lens and, the speed and power to get the best results. 


---
**Kristi Hawkins** *May 10, 2017 18:04*

**+Jeff Johnson** 

Thank you! I wasn't sure what the pixel was actually doing. I thought it made the depth of the engraving.



I will try that!


---
**Kristi Hawkins** *May 10, 2017 18:06*

**+Ashley M. Kirchner** 



Thank you for your thoughts! I have considered that as well. 






---
**Jeff Johnson** *May 10, 2017 18:47*

The pixel setting makes it skip lines for me.  The higher the number the fewer passes needed. For larger and deeper engraving it still looks great, specially when focus is off a little. If the focus is right on you will see more defined lines.


---
**Kristi Hawkins** *May 10, 2017 19:33*

**+Jeff Johnson** 

What makes the depth of the engraving? Steps or power?


---
**Jeff Johnson** *May 10, 2017 19:50*

**+Kristi Hawkins** It's a combination of steps, power and speed and the settings are different for every material. I engrave a lot of hickory, walnut and poplar and each one uses different settings. This picture is walnut - I engrave the shallow lines in the background at 5ma and 6 pixels. The logo in the foreground is 12ma and 5 pixels. Both are done at 400mm/s. Wood is probably more forgiving when using higher pixel counts but play with it and see happens.

![images/ff0caa0af243eb62ccd368f814134d51.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff0caa0af243eb62ccd368f814134d51.jpeg)


---
**Phillip Conroy** *May 10, 2017 19:58*

Is the laser firing in both directions, bidirectional must not be ticked


---
**Kristi Hawkins** *May 11, 2017 12:46*

**+Phillip Conroy** 

Where is the bidirectional setting so I can check?


---
**Phillip Conroy** *May 11, 2017 21:32*

Bottom of screen that pops up when you go to cut stufff


---
**Mark Brown** *May 12, 2017 11:43*

"Bidirectional must NOT be checked to cut in both directions"??


---
*Imported from [Google+](https://plus.google.com/101479367502614716645/posts/Sr5gnG5Pggk) &mdash; content and formatting may not be reliable*
