---
layout: post
title: "Originally shared by Dushyant Ahuja Just converted my K40 to Smoothieware"
date: June 30, 2017 03:55
category: "Smoothieboard Modification"
author: "Dushyant Ahuja"
---
<b>Originally shared by Dushyant Ahuja</b>



Just converted my K40 to Smoothieware. Thanks **+Ray Kholodovsky** for making the swap so easy. Literally took me 10 minutes and most of it was unscrewing the stock board. 



Thanks to the developers of laserweb - the software is really nice. Though I'm still learning. 



Is there a database of setting people generally use? Would it make sense to have a git with standard settings, or would that not be useful at all because of how different the machines can be?

![images/7ce924ff2fcfb1ee8d23ea2f9a69ec8f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7ce924ff2fcfb1ee8d23ea2f9a69ec8f.jpeg)



**"Dushyant Ahuja"**

---


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/6CH8uL8XKHc) &mdash; content and formatting may not be reliable*
