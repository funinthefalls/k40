---
layout: post
title: "Made this slate door number and treated with mineral oil"
date: February 05, 2016 22:19
category: "Object produced with laser"
author: "David Wakely"
---
Made this slate door number and treated with mineral oil. Looks good I think!

![images/c554d788a414a8aca77f17ce19ac41b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c554d788a414a8aca77f17ce19ac41b7.jpeg)



**"David Wakely"**

---
---
**Jim Hatch** *February 05, 2016 22:37*

Really nice job!


---
**Anthony Bolgar** *February 05, 2016 23:31*

I have one on the bed engraving right now. I hope it turns out as nice as this one.


---
**Scott Thorne** *February 06, 2016 00:07*

Great job man!


---
**Anthony Bolgar** *February 06, 2016 05:34*

What settings did you use. On my K40 it seems like I need to run a minimum of 12mA at 500mm/s to get a decent engrave. Are these numbers close to yours?


---
**David Cook** *February 06, 2016 06:55*

Looks great 


---
**David Wakely** *February 06, 2016 10:32*

I use about 7ma 400mms to produce this. I'm also using 2 pixel per step setting as slate doesn't need the higher quality of 1


---
**Jim Hatch** *February 06, 2016 14:27*

**+Anthony Bolgar**​ that's fast and near max power. How is yours when you slow it down (slower speeds = higher delivered power).



**+David Wakely**​ have you done photos at the 2px step too? I was thinking of doing one for the house with a pic of the house on it.


---
**Anthony Bolgar** *February 06, 2016 15:09*

I haven't done any photos yet. Will try slowing down and seeing if I can lower the power setting. AT least the slate I get is cheap, a dollar for a 200mm X 15omm oiece at my local dollar store, so I can do a lot of experimenting.


---
**David Wakely** *February 07, 2016 10:01*

With pictures I normally do 1px step for the better quality. Never tried it on slate though


---
**Kelly Burns** *February 10, 2016 22:20*

Where do you pick up the slate?


---
**David Wakely** *February 11, 2016 16:41*

I get my slate form a roofing suppliers 


---
**Coherent** *February 18, 2016 22:12*

I've tried ceramic tile but not slate. Does the slate show the laser etching a lighter color or is there a specific process you use to get these results?


---
**Kelly Burns** *February 18, 2016 22:53*

I picked up a bunch or reclaimed slate that was removed from 100 Year old house in Cincinnati.  Still need to play with the settings, but I'm getting much better.  Thanks for the motivation.


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/6BfAvYR1aNe) &mdash; content and formatting may not be reliable*
