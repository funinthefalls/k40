---
layout: post
title: "This is going to be my K40 conversion"
date: January 20, 2018 21:42
category: "External links&#x3a; Blog, forum, etc"
author: "Frederik Deryckere"
---
[http://www.manmademayhem.com/?p=3160](http://www.manmademayhem.com/?p=3160)



This is going to be my K40 conversion.





**"Frederik Deryckere"**

---
---
**Adrian Godwin** *January 20, 2018 22:25*

Sweet. How much extrusion does it use ?




---
**Frederik Deryckere** *January 21, 2018 00:09*

**+Adrian Godwin** 10 2-meter bars


---
**Dennis Luinstra** *January 21, 2018 08:14*

Great! Good luck and enjoy building it! This gives me inspiration to build a new one myself in the future, replacing our k40. 


---
**Frederik Deryckere** *January 21, 2018 09:07*

thanks! Hope all goes well.


---
**Frederik Deryckere** *January 21, 2018 17:34*

**+Michael Johnson** could you elaborate?


---
**Frederik Deryckere** *January 22, 2018 15:39*

Hmm I don't know if that would be worthwhile. It will add up to the tally, it will conflict with mounted pieces, and i'm not convinced it will add to the rigidity. I mean it probably will, but I did a test with one of my corner brackets and that seems plenty strong. Especially in a laser environment where you don't have the same forces as a cnc mill.


---
**Frederik Deryckere** *January 22, 2018 16:21*

**+Michael Johnson** i already bookmarked that website :-)


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/LPi4gMHkdtK) &mdash; content and formatting may not be reliable*
