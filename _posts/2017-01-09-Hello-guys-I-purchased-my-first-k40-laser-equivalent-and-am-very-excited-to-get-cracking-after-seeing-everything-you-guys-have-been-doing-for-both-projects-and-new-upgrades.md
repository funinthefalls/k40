---
layout: post
title: "Hello guys, I purchased my first k40 laser equivalent and am very excited to get cracking after seeing everything you guys have been doing for both projects and new upgrades!"
date: January 09, 2017 08:53
category: "Original software and hardware issues"
author: "Abe Fouhy"
---
Hello guys,



I purchased my first k40 laser equivalent and am very excited to get cracking after seeing everything you guys have been doing for both projects and new upgrades!



My laser is a kl-320 40w laser, looks exactly the same but with a digital power readout and 6 push button power adjustments and a test button.



I plugged it in and was cutting and engraving up a storm, and noticed I couldn't cut through 3mm, seeing that I should be able to decided to line up the mirrors and while I was hitting the test button I guess I held it longer than I should of more of a 1-2 sec push vs a quick tap and blew something up. The red LED below the test button doesn't light up and I am figuring that I blew the 5v trigger part of the low voltage to high voltage circuit. Gussing this is on k+.



I am about to tear into the machine and do some testing, and wanted to know if there were any schematics out there on this machine, or if I should take the time to map it for the community?



Also if you can think of any good tips, advice or upgrades I should be thinking of, I am all ears. I am 3 days new to this!



Thanks all!



Abe





**"Abe Fouhy"**

---
---
**Abe Fouhy** *January 09, 2017 10:20*

UPDATE: Checked the connections of the manual power control board and found that one of the solder joints on the test button was lacking solder. Resoldered it and it is now activating the high power side, but no laser firing. I hear a sparking sound in the power supply, and come to think of it, when lasing it may of been making that sound originally but I thought that it may be the sound it makes. I am thinking not. 



Tomorrow (or today it is 2am) plan is to take apart the case of the PS and see if there is any apparent damage. Any thoughts on what component may be having issues?


---
**Abe Fouhy** *January 09, 2017 10:25*

To troubleshoot the tube for power what is a safe way to test this if it is firing. I do not want to look at the tube for potential eye damage. Should I wait for my co2 laser goggles to come?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 09, 2017 11:52*

Check **+Don Kleinschnitz** blog as I believe he has mapped some of the schematics on the PSU at least.


---
**Don Kleinschnitz Jr.** *January 09, 2017 15:22*

**+Abe Fouhy**  I assume that you do not have a K40. 



Disclaimer: Warning these HV supplies have extremely high voltages and can hold a lethal charge even after the power is removed. Do not attempt repair of these supplies unless you have specific training and experience in HV electronic equipment. 

Any attempt to test or repair a HV supply is at your risk.



Take a look at this blog index nearly everything in a K40 is documented with drawings and schematics. Then let me know how to help.



[donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**HalfNormal** *January 09, 2017 17:38*

**+Abe Fouhy** You can hold down the test button indefinitely and with any power. It is what is under the laser that you have to be careful about! 


---
**Abe Fouhy** *January 09, 2017 17:49*

Ok. Good to know. I thought that the 5v side had a lot of current going through it when the test button was pushed, but it looks like I had a PS issue all along and happened to die when testing. Thanks for the that, on a new device it's hard to know whats what.


---
**Abe Fouhy** *January 10, 2017 08:56*

So my fixed a solder joint on my controller board for the test button and now the PS isn't making any noise and the laser is firing! I just need to align those mirrors and I am good!


---
**Don Kleinschnitz Jr.** *January 10, 2017 13:14*

**+Abe Fouhy** could you elaborate on what the fix was? Perhaps even post a photo for others to reference. 

When you say controller and test button do you mean in the supply?


---
**Abe Fouhy** *January 10, 2017 19:49*

Well I am not sure it entirely fixed, but what I did was take my multimeter and test all the 5 pin connections (where the cable plugs in from the PS) to the in and out of the test switch and measured the ohms and found that on one connection of the output of the test switch had double the ohms, so I resoldered the connection and it's "working" now. 


---
**Abe Fouhy** *January 12, 2017 19:33*

**+Don Kleinschnitz** here is a photo of my board and solder joint issue. It wasn't the prettiest ive ever done but effective. There was no solder on the bottom right leg of the switch.



![images/e63e12fe1d02046746fc321319705569.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e63e12fe1d02046746fc321319705569.jpeg)


---
**Don Kleinschnitz Jr.** *January 12, 2017 20:38*

Thanks. That supply looks different than any I have seen.


---
**Abe Fouhy** *January 12, 2017 20:43*

Thank you, I have been looking at your wiki, looks great!


---
**Abe Fouhy** *January 13, 2017 10:51*

So as I was aligning my mirrors, looks like my laser firing issue is intermittent. So i busted open the power supply and found the ground wire that goes to the ground lug on the back and the ground of the ac plug is not connected and i think this is where my crackling shorting sound is coming from on the PS. Can I hook this into the FG terminal of the AC side of the PS where the large resistor wire from the laser is plugged into?


---
**Abe Fouhy** *January 13, 2017 10:53*

![images/aa126eb516eec032fbaac0184ebd3cbb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa126eb516eec032fbaac0184ebd3cbb.jpeg)


---
**Abe Fouhy** *January 13, 2017 11:00*

Hmm looks like that is s big no. Floating ground for hv return only 


---
**Don Kleinschnitz Jr.** *January 13, 2017 13:50*

HV (tube cathode) return (usually green yellow) should be wired back directory to the power supply. What resistor are you talking about?

The AC grnd should be grounded to the frame at the AC plug?


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/QnYt88G1tqg) &mdash; content and formatting may not be reliable*
