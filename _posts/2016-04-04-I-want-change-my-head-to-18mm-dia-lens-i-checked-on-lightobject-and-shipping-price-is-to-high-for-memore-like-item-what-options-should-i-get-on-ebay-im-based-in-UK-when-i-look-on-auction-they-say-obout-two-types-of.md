---
layout: post
title: "I want change my head to 18mm dia lens, i checked on lightobject and shipping price is to high for me(more like item) what options should i get on ebay (im based in UK) when i look on auction they say obout two types of"
date: April 04, 2016 12:14
category: "Modification"
author: "Damian Trejtowicz"
---
I want change my head to 18mm dia lens, 

i checked on lightobject and shipping price is to high for me(more like item)

what options should i get on ebay (im based in UK)

when i look on auction they say obout two types of heads(higher and lower)



mainly i want this one, but not sure it will fit



[http://www.ebay.co.uk/itm/CO2-Laser-Head-Lens-Holder-K40-Red-Dot-Positioning-DIY-CNC-40W-Stamp-Engraver-/252323994812?hash=item3abfae9cbc:g:feUAAOSwh-1W34NN](http://www.ebay.co.uk/itm/CO2-Laser-Head-Lens-Holder-K40-Red-Dot-Positioning-DIY-CNC-40W-Stamp-Engraver-/252323994812?hash=item3abfae9cbc:g:feUAAOSwh-1W34NN)





**"Damian Trejtowicz"**

---
---
**Stephane Buisson** *April 04, 2016 12:43*

your link look good, saite_cutter is a regular Seller on laser product. 


---
**Phillip Conroy** *April 04, 2016 12:59*

[http://www.ebay.com.au/itm/252273200569?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/252273200569?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)

That is the one i fitted tk my k40,however had to playaround with the mounting bracket to make it fit..ps you may need to align the laser head if beam hits side of air assist,i had to pack one side up 3 thin washers 


---
**Phillip Conroy** *April 04, 2016 13:03*

Forgot to add takes 28mm focal lens,also used a washer so that i could use old 12mm focal lens if needed


---
**Scott Marshall** *April 04, 2016 18:17*

I've got this one, (same as your link, but loaded) and 2, 3, 4" lenses, very happy with it. I did have to put a couple of felt washers under it, but that may be my custom carriage. Saite cutter is 1st class, good stuff, usually cheaper than LO by a fair amount. Pretty sure they ship worldwide.



[http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005?hash=item3abf7326d5:g:kagAAOSwIrNWFIY7](http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005?hash=item3abf7326d5:g:kagAAOSwIrNWFIY7)


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/FBiy1bv5b7x) &mdash; content and formatting may not be reliable*
