---
layout: post
title: "Doing a little project with CHIP ($9 computer) and Adafruit Trellis"
date: August 15, 2016 19:18
category: "Object produced with laser"
author: "Tev Kaber"
---
Doing a little project with CHIP ($9 computer) and Adafruit Trellis. The idea is a simple MP3 player for my 2-year-old, each button triggers a different song. Just in the initial design phase now.



I'll probably manually add holes as needed to this practice enclosure, then make a second cleaned up version. 



![images/78d47ab8d1401cd29c4e3780a51d4c0d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/78d47ab8d1401cd29c4e3780a51d4c0d.jpeg)
![images/e01ed648f93853ca3f3c067b0f47d5aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e01ed648f93853ca3f3c067b0f47d5aa.jpeg)
![images/7d29f63c547bd45656f4b61beaffefac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d29f63c547bd45656f4b61beaffefac.jpeg)

**"Tev Kaber"**

---
---
**Robert Selvey** *August 16, 2016 03:58*

What kind of wood is this made out of and where did you get it ?


---
**Tev Kaber** *August 16, 2016 04:11*

3mm plywood, I got it from eBay: [http://www.ebay.com/itm/1-8-3mm-x-8-x-10-Baltic-Birch-Plywood-for-CNC-Laser-Scoll-Saw-40-pieces-/351780641491](http://www.ebay.com/itm/1-8-3mm-x-8-x-10-Baltic-Birch-Plywood-for-CNC-Laser-Scoll-Saw-40-pieces-/351780641491)



8x10 pieces are nice because they are just about the right size for the K40 cutting area, and 3mm is nice because it can be easily cut in one pass.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/ctn1XRvnAcU) &mdash; content and formatting may not be reliable*
