---
layout: post
title: "I have created a github repository for a Laser Safety System that I am using on my K40 laser"
date: January 21, 2016 19:44
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
I have created a github repository for a Laser Safety System that I am using on my K40 laser. 



This was based upon some code from SigmaGgfx.



The schematic will be uploaded as soon as I find a few minutes to create it.



It monitors coolant flow, coolant temperature, and door interlock.



You can see the project at:

[https://github.com/funinthefalls/LaserSafetySystem](https://github.com/funinthefalls/LaserSafetySystem)





**"Anthony Bolgar"**

---
---
**Jim Hatch** *January 21, 2016 20:14*

Nicely done. Good to have these all in one place instead of piecemealing the warning/safety systems together. 



Not sure if you can, but have you thought about adding sensors for exhaust fan on & air assist as well? Those won't necessarily kill you or your laser but if they shutdown it can get dirty/messy in there.


---
**Anthony Bolgar** *January 21, 2016 20:19*

I will be adding some code to this that will activate a peripherals board that controls exhaust, air assist, laser pointer, and lights, and returns the status of each one, and will give a visual and audible warning if there is a problem with any of them. My goal is to have the safety system and peripherals running on a custom PCB.


---
**Anthony Bolgar** *January 21, 2016 20:43*

Good idea Peter.


---
**Anthony Bolgar** *January 22, 2016 07:37*

**+Peter van der Walt** would you be able to create a quick schematic for me? I haven't used eagle cad in forever and I am sure you could probably bang this out in a few minutes. If you don't have the time, I will make a quick and dirty one in CorelDraw.


---
**Anthony Bolgar** *January 22, 2016 07:52*

Just the basic layout based on the pinouts in the readme file, and if you have time, the nand/and section. Take all the time you need , it's almost 3am here in Niagara Falls, and I am off to bed as soon as I clear my emails for the day.


---
**Anthony Bolgar** *January 22, 2016 07:58*

Thanks very much.


---
**Jon Bruno** *February 01, 2016 14:59*

I'm really glad that my cobbled hack has grown some legs.. I hope it becomes a most useful project. Thanks for taking the reigns Anthony. 


---
**Anthony Bolgar** *February 01, 2016 16:51*

Glad you like it Jon!


---
**Jon Bruno** *February 01, 2016 16:54*

I'm following the repo if I find some time I'll try to contribute.


---
**Anthony Bolgar** *February 01, 2016 16:56*

Thanks, any help is appreciated. Do you know how to use Eagle CAD? I would like to get a schematic posted, but it has been over 10 yrs since I used any CAD system.


---
**Jon Bruno** *February 01, 2016 17:02*

I haven't used eagle in a while.. I designed a forklift controller but never had it etched..  The last project I actually etched and built 
{% include youtubePlayer.html id="wz3TUkCHEJs" %}
[https://youtu.be/wz3TUkCHEJs](https://youtu.be/wz3TUkCHEJs) was a simple board to replace the hand etched one I made for this clock project


---
**Jon Bruno** *February 01, 2016 17:39*

**+Peter van der Walt** It's not like you are sitting around.. you've got countless projects boiling on the burners.. You are an amazing fellow..


---
**chuck ismail** *May 18, 2016 07:55*

Hi, Anthony Bolgar, do you mind to share the circuit wiring diagram for laser safety system ?


---
**Anthony Bolgar** *May 18, 2016 07:59*

The wiring instructions are in the start of the Arduino code as comments.


---
**chuck ismail** *May 18, 2016 10:19*

noted. thank you


---
**chuck ismail** *August 28, 2016 10:59*

Dear mr Anthony, i would like to know, why do i have error like this "no matching function for call to 'LiquidCrystal_I2C::LiquidCrystal_I2c", i have included the i2c library for your information. have you encounter this problem before.?


---
**Jon Bruno** *August 28, 2016 21:20*

Oh, and Anthony.. That's Mr. SigmazGFX


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/NPnQJ8VcMa9) &mdash; content and formatting may not be reliable*
