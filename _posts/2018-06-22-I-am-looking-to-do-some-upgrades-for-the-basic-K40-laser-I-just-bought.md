---
layout: post
title: "I am looking to do some upgrades for the basic K40 laser I just bought"
date: June 22, 2018 21:43
category: "Modification"
author: "Vlad Isari"
---
I am looking to do some upgrades for the basic K40 laser I just bought. I intend to do bottles and tumblers engraving.



Shortly what I have in mind, but please advise me!!!:

1. I have a M2 Nano mainboard. Is this enough or should I go for a better kit?

2. Software, I intend to use K40 Whisperer from Scorch? is this alright for all the setting required to work with a rotary?

3.I found an mini 2 axis rotary, 20cm x 10cm x 20cm that should fit in the laser case , but I am not 100% sure it will work with my board.

4. Should I go for a adjustable table or a Honeycomb bed?

5. What else I need for what i intend to do? any other upgrades?



Thank you! I attached some img with my laser, no spam. 



![images/af54358ef9254a89867349e23596ed5d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/af54358ef9254a89867349e23596ed5d.jpeg)
![images/90abc363dfa813ab85d5793e4d8806c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90abc363dfa813ab85d5793e4d8806c1.jpeg)
![images/979b768c1e341722551615771956a335.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/979b768c1e341722551615771956a335.jpeg)
![images/aa98f72f7866340081fd68eeb340f971.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa98f72f7866340081fd68eeb340f971.jpeg)

**"Vlad Isari"**

---
---
**Anthony Bolgar** *June 22, 2018 22:15*

I would suggest upgrading the controller to a C3D mini, which is a drop in replacement. Then I would use LightBurn software, it works so much better than anything else, and has a specific module to do rotary engravings. I would also upgrade to an air assist head with a better quality 18mm lens. The Cohesion 3D web site sells all the things you would need to upgrade, they even sell the LightBurn software. Check it out at [cohesion3d.com - Cohesion3D: Powerful Motion Control](http://Cohesion3D.com)




---
**Vlad Isari** *June 23, 2018 06:05*

**+Anthony Bolgar** Thank you Anthony , it looks like it's worth!


---
**Stephane Buisson** *June 23, 2018 07:15*

Your main difficulties would be the focus distance. make yourself a piece of wood to place between your lens and object. and lift the rotary accordingly. otherwise honeycomb bed is the must to reduce fume marks on flat material.



also you could have a look at open source Visicut software (compatible with Smoothieboard like (C3D)), rotary attachment functions.




---
**Don Kleinschnitz Jr.** *June 23, 2018 11:49*

**+Vlad Isari** Before you add the cool stuff ....... make your machine safer and protect the expensive parts :)!



If your machine does no have these I would suggest their addition:

1. INTERLOCKS on front and rear cover to make the unit safer

2. A water flow sensor that interlocks the laser power

3. An overtemp sensor interlock to protect the tube from overheating.



All these can be found in this community and my blog.


---
**Vlad Isari** *June 23, 2018 20:20*

Thank you for all your answers and advices.  

Also,

Is it necessary to upgrade with an air assist head and better quality 18mm lenses  if i intend to engrave on stainless steel bottles?  


---
**Don Kleinschnitz Jr.** *June 24, 2018 13:14*

**+Vlad Isari** 



Air assist and better lenses improve the quality of your output but you CAN get it done with stock lenses and no air assist. 



Adding these changes is really a matter of enhancing the output quality to meet your needs.



I suggest getting your machine working and set up with a new controller and stock optical parts and then systematically upgrading as you see the need to improve quality. This includes the safety add-ons. This machine can blind you!






---
*Imported from [Google+](https://plus.google.com/114019829123284097632/posts/DCNoFt3xrQJ) &mdash; content and formatting may not be reliable*
