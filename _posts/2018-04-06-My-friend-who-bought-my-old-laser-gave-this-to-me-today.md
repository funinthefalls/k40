---
layout: post
title: "My friend who bought my old laser gave this to me today"
date: April 06, 2018 04:07
category: "Object produced with laser"
author: "HalfNormal"
---
My friend who bought my old laser gave this to me today. It is a bookmark made out of scrap wood he had and cut real thin.

![images/58c27fdf31191629b69d297e55e7034e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58c27fdf31191629b69d297e55e7034e.jpeg)



**"HalfNormal"**

---
---
**James Rivera** *April 06, 2018 15:47*

Lmao


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/3SmcYhofzrF) &mdash; content and formatting may not be reliable*
