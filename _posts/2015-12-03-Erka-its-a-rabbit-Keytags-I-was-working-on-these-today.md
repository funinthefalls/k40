---
layout: post
title: "Erka (it's a rabbit) Keytags - I was working on these today"
date: December 03, 2015 09:38
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Erka (it's a rabbit) Keytags - I was working on these today. Crazy amount of cutting to get it to cut through the leather. I did 15 passes @ 4mA @ 10mm/s & still only got 99% of the way through the leather.



![images/75f8e6972ecf046a15868940d143a8e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/75f8e6972ecf046a15868940d143a8e0.jpeg)
![images/b779b071a99e2fd64b8bef972fbc2d92.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b779b071a99e2fd64b8bef972fbc2d92.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Stephane Buisson** *December 03, 2015 09:50*

how thick is your leather ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 03, 2015 10:31*

**+Stephane Buisson** This leather was only about 1.8-2mm thick. Thinner than I have been using lately, but still being problematic to cut. Although the air-assist (with the air pump from airbrush) seems to help reduce the sooty marks & minimise burning.


---
**Scott Thorne** *December 06, 2015 06:09*

It should be cutting that easy...I just cut through a old belt...it's almost 4 mm thick , 1 pass 7mA at 7 mm/s


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 06, 2015 07:26*

**+Scott Thorne** Thanks Scott. Maybe I need to check my alignment on the mirrors & get it more precise. My leather supplier used to work with laser cutters & he said it should cut through super easy too.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 06, 2015 07:53*

**+Scott Thorne** I've just gone & taken a look at the laser cutter & started pulling pieces apart. I notice this with my lens on the laser head. Looks like there is no concave/convex area on the lens & it is not clear. Is that normal? [https://goo.gl/photos/nyP8PYLQR4Xo8jiT7](https://goo.gl/photos/nyP8PYLQR4Xo8jiT7)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 06, 2015 13:10*

**+Scott Thorne** Thanks for your post earlier. Made me check things & it turns out that my lens was actually upside down. Using your settings (7mA @ 7mm/s) I manage to cut through 2mm leather like it is butter.


---
**Scott Thorne** *December 06, 2015 13:11*

**+Yuusuf Sallahuddin**​....anytime my friend...I'm glad you were able to get it fixed.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/EjxN9ZqbKN9) &mdash; content and formatting may not be reliable*
