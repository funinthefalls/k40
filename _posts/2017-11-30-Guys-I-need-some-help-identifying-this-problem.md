---
layout: post
title: "Guys, I need some help identifying this problem"
date: November 30, 2017 02:39
category: "Modification"
author: "Chuck Comito"
---
Guys, I need some help identifying this problem. I get an overlap in the Y axis. So far the things I've tried are:

Belt tension checked ok.

Swapped to a larger stepper motor.

Swapped stepper driver. 

Verified pulley tooth count.

This is making me crazy. It seems to repeat the exact same way every time. 

My k40 is heavily modified running laserweb4 with smoothie and xternal stepper drivers. Any thoughts would be great. 



![images/409ba1a11dfa83db3a79fbb3b952996d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/409ba1a11dfa83db3a79fbb3b952996d.jpeg)



**"Chuck Comito"**

---
---
**Eric Le Méné** *November 30, 2017 06:07*

hi, i would say this might have to do with the laser switching control <s>delayed</s> . because this overlap doesnt occur on the outside square (bottom left angle).

try reversing line cutting direction on every other line to see if it overlaps on both sides. then you have a good hint or what it might be


---
**HP Persson** *December 03, 2017 22:56*

If you changed to GT2 belt and pulleys, there is the reason to the error you see.

The k40´s do use 20 tooth pulleys, but 2.032 pitch, so the wheel is actually bigger. Per revolution this adds up.


---
**Chuck Comito** *December 03, 2017 23:14*

I'm in the process of swapping out the pulleys now. I was using some unknowns I had lying around. They appear to be gt2 but who knows. I'll report back hopefully tonight but thanks for giving me some ideas to look at. 


---
**Chuck Comito** *December 04, 2017 00:43*

 So I changed the pulleys out and the Y axis appears to be working. I think it was the belt getting hung up on the edge of the pulley. Thanks for your input guys!


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/8vTRx3X4uTY) &mdash; content and formatting may not be reliable*
