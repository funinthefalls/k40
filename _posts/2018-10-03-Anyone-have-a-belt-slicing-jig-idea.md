---
layout: post
title: "Anyone have a belt slicing jig idea?"
date: October 03, 2018 23:08
category: "Discussion"
author: "Jamie Richards"
---
Anyone have a belt slicing jig idea?  My X axis belt, may possibly be stretched and the one I bought is 6mm, which is too big for the pulley.  Thinking about just finding a taller MXL pulley.  Why would they do this?  The Ys are 6mm, but not the x.  Going to try and rig something using a razor blade I guess.  



My belt is tight, but I guess it can throw measurements off a little by being stretched.  It still performs well, as shown in my speed test, but if I cut something at 160mm, it will actually cut it to 164mm.  The closer I cut to home, the more accurate it is; no problem with Y.





**"Jamie Richards"**

---
---
**James Rivera** *October 04, 2018 04:40*

Are you sure it is MXL and not GT2?


---
**Don Kleinschnitz Jr.** *October 04, 2018 11:13*

Doesn't LO have the right size belts?


---
**Don Kleinschnitz Jr.** *October 04, 2018 13:09*

Thinking about your belt slicing problem here is an idea for a jig.

I made something like this to cut plastic strips and maybe it would work for belts.....although they are pretty tough to cut.





![images/68480308d8175e3859bb0fbfb440b700.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/68480308d8175e3859bb0fbfb440b700.jpeg)


---
**Jamie Richards** *October 05, 2018 15:51*

Thanks Don, that's something on the order of what I was thinking!  LightObject is overpriced imo.  I've read where people have cut them by hand, but want something I can get it started by hand and then just pull it through.  The K40 X uses MXL I'm told.  Also assuming the two Ys are MXL, but they won't need narrowed should the time come to change them.


---
**Jamie Richards** *October 05, 2018 16:14*

Alternatively, I can switch to GT2 and just figure out how to program the steps in the Cohesion3D board, but if it should fail and I have to put the nano back in, it wouldn't work correctly.


---
**James Rivera** *October 05, 2018 16:19*

IIRC, the tooth profile of MXL is inferior compared to GT2 wrt backlash. If I’m right, then now might be a good time to upgrade.

[blog.misumiusa.com - You've Got the Power! MR vs. GT &#x7c; MISUMI USA Blog](http://blog.misumiusa.com/youve-got-the-power-mr-vs-gt/)


---
**Don Kleinschnitz Jr.** *October 05, 2018 16:28*

**+James Rivera** #K40Belts


---
**Jamie Richards** *October 05, 2018 18:33*

I suppose if I did switch, I could always keep the old belt and pulley as a backup should the C3D fail; I'm kind of considering it.  I would need to find info on programing the steps or whatever for the X. I'm sure I'd figure it out, but FIGURING IT OUT!!! lol


---
**Jamie Richards** *October 05, 2018 18:45*

Here's a low cost solution.  Will this work?  



Robot check?  What is that when I post a link?



[amazon.com - Robot Check](https://www.amazon.com/SUNKEE-Timing-Pulleys-Meters-Printing/dp/B00WW256TY/ref=lp_16411441_1_11?s=industrial&ie=UTF8&qid=1538764654&sr=1-11)


---
**Jamie Richards** *October 05, 2018 18:49*

Just realized, I'll need a wider idler pulley for the tensioner if I go with this.




---
**Jamie Richards** *October 07, 2018 07:17*

Update.  Solved the mystery thanks to Tony in the C3D group.  The k40 Grbl steps are 157.575.  I did manage to make a jig that worked quite well by using the blade from a bic razor.  Managed to cut myself even though I used pliers to take it apart.  The belt they sent was almost 10mm and the K40 takes just under 5mm.

![images/e77e35271fc89f3796fc604016ff4e95.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e77e35271fc89f3796fc604016ff4e95.jpeg)


---
**James Rivera** *October 07, 2018 16:58*

**+Jamie Richards** #Tenacity 😃👍


---
**Jamie Richards** *October 11, 2018 02:47*

Here's the belt I ordered and they sent me 10mm.



[https://www.ebay.com/itm/MXL-Open-Belt-6mm-Width-2-032mm-Pitch-for-3D-Printer-RepRap-Prusa-one-meter-long/183114727167?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2060353.m2749.l2649](https://www.ebay.com/itm/MXL-Open-Belt-6mm-Width-2-032mm-Pitch-for-3D-Printer-RepRap-Prusa-one-meter-long/183114727167?ssPageName=STRK:MEBIDX:IT&_trksid=p2060353.m2749.l2649)


---
**James Rivera** *October 11, 2018 05:59*

**+Jamie Richards** So you got 40% free belt!  ;-)


---
**Jamie Richards** *October 11, 2018 11:54*

lol, yeah, but I'm still thinking about changing the pulleys and going to a GT2 setup because the belts are so much cheaper and supposedly more stable.  MXL is only going to become more expensive as it becomes harder to get.  Or I'm going to say the heck with it and build a larger gantry, which I'll use the newer stuff on.  


---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/jeCmtKppXGZ) &mdash; content and formatting may not be reliable*
