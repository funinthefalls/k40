---
layout: post
title: "Interesting idea, wonder if it really works?"
date: April 10, 2018 19:20
category: "Materials and settings"
author: "Anthony Bolgar"
---
Interesting idea, wonder if it really works?



[https://www.thingiverse.com/thing:2853756](https://www.thingiverse.com/thing:2853756)





**"Anthony Bolgar"**

---
---
**syknarf** *April 11, 2018 11:46*

Will try as soon I get my machine working again.


---
**Ned Hill** *April 11, 2018 15:13*

Interesting.  Dry erase markers contain a resin, pigment and alcohol solvents.  The resin and pigment are apparently getting baked onto the surface.  This is using a 1000 mw UV diode laser and I kind of doubt it will work with our CO2 lasers, probably just get blasted off, but will be interesting to try.   Probably using lowest power with a  defocused beam.  [https://sciencing.com/list-6040398-chemicals-dry-erase-markers.html](https://sciencing.com/list-6040398-chemicals-dry-erase-markers.html)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/VXKaEpDau6w) &mdash; content and formatting may not be reliable*
