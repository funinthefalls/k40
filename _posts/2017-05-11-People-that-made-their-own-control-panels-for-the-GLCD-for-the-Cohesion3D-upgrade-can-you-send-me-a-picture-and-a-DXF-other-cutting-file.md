---
layout: post
title: "People that made their own control panels for the GLCD for the Cohesion3D upgrade, can you send me a picture and a DXF/ other cutting file?"
date: May 11, 2017 16:53
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
People that made their own control panels for the GLCD for the Cohesion3D upgrade, can you send me a picture and a DXF/ other cutting file?  I get a lot of new users asking for a panel design that will accommodate the screen.





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Anthony Bolgar** *May 11, 2017 17:02*

Here is mine, has a lot of extras in it, but easy enough to edit the file. [thingiverse.com - K40 Front Panel by funinthefalls](http://www.thingiverse.com/thing:1564287)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/N3gEjaSJUTQ) &mdash; content and formatting may not be reliable*
