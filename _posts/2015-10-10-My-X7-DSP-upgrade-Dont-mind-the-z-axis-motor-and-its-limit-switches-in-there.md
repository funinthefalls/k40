---
layout: post
title: "My X7 DSP upgrade. Don't mind the z axis motor and its limit switches in there"
date: October 10, 2015 00:46
category: "Modification"
author: "Gee Willikers"
---
My X7 DSP upgrade. Don't mind the z axis motor and its limit switches in there. I have yet to make a z bed.



![images/53556ed30ed45e7ab817db90ea688fde.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/53556ed30ed45e7ab817db90ea688fde.jpeg)
![images/72c64e0f3d829e1479d89f792005e898.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/72c64e0f3d829e1479d89f792005e898.jpeg)
![images/a782f558d74a634fd30fb65dcd35ed2a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a782f558d74a634fd30fb65dcd35ed2a.jpeg)
![images/febafa5df94b05e7633f648c0c62c995.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/febafa5df94b05e7633f648c0c62c995.jpeg)
![images/309ab921a8278c67e512700643dd9e05.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/309ab921a8278c67e512700643dd9e05.jpeg)
![images/f206eea7dd0d7bc236af7fef340f85a3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f206eea7dd0d7bc236af7fef340f85a3.jpeg)
![images/a45de769c27ce33e5e9f922f7b4c92e7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a45de769c27ce33e5e9f922f7b4c92e7.jpeg)

**"Gee Willikers"**

---
---
**Cam Mayor** *October 10, 2015 00:55*

Nicely done.


---
**Joey Fitzpatrick** *October 10, 2015 01:48*

Nice and Tidy.  Looks like a clean install!!


---
**Gee Willikers** *October 10, 2015 02:05*

Thank you.


---
**Andrew Cilia** *October 10, 2015 03:59*

Very cool. Could you post the BOM and where to buy? I'd like to do a similar upgrade. 


---
**Gee Willikers** *October 10, 2015 04:23*

**+Andrew Cilia** I added it to the album.


---
**Brooke Hedrick** *October 10, 2015 06:08*

Nice job!


---
**Stephane Buisson** *October 10, 2015 08:28*

Good work, thank you for sharing your DSP option


---
**Gee Willikers** *October 10, 2015 16:31*

**+Carl Duncan** I used a metal nipping tool similar to : [http://ecx.images-amazon.com/images/I/61442i6Mv9L._SY355_.jpg](http://ecx.images-amazon.com/images/I/61442i6Mv9L._SY355_.jpg) or [http://store-planetools.com/ProductImages/kleintools/76011B%20Nibbler.JPG](http://store-planetools.com/ProductImages/kleintools/76011B%20Nibbler.JPG)



They nip out little rectangles from the metal. Time consuming but works well and doesn't deform or heat the metal.


---
**Ashley M. Kirchner [Norym]** *October 10, 2015 18:55*

So I have a question: do you not use the network port at all? I know it says the USB port is for OS upgrades which leads me to believe you don't print through it (like the stock controller does.) But what about the network port?


---
**Gee Willikers** *October 10, 2015 19:07*

Firmware upgrades use a usb stick and the usb port on the front of the unit. That port is also for uploading files.



The rear usb port connects to the pc for uploading files like any other device.



The network port does the same as the rear usb port - its for uploading files via pc. You can select either the usb or network port in the software. 



I <i>have</i> used the network port. It gave me issues at first but they seem to have gone away with the latest firmware upgrade. You assign the dsp unit a static ip. I do need to test it further, I'm just out of available ethernet ports on my router at the moment.



Ultimately I'd like to setup a Raspberry Pi as a wireless bridge and server. The Pi would connect wirelessly to my router, its ethernet port to the laser engraver. I'd also connect my old laser printer to share and add a disk for shared design files so I can work from any computer in the house. But that's another project...


---
**Ashley M. Kirchner [Norym]** *October 10, 2015 21:41*

Thanks for the USB/Ethernet port explanation. That's good info to have. As I already have a central storage system for my place, being able to connect the thing via network would be a big plus for me. Then I can work anywhere and send files to it when needed. Though knowing me, I will likely send something to it without realizing I haven't loaded the material yet. Which is why I have 'a camera' on the list of things to add. :)


---
**Al Tamo** *October 11, 2015 01:10*

Dsp works properly with that PSU?


---
**Gee Willikers** *October 11, 2015 01:12*

Yes. 24vdc, 5 amp.


---
**Al Tamo** *October 11, 2015 01:14*

I think that was necesary a bigger one for the controlers, etc


---
**Al Tamo** *October 11, 2015 06:56*

Oook, I see that you have 2 PSU ooook﻿


---
**Gee Willikers** *October 11, 2015 06:58*

The large one in back came with the machine, it powers the laser tube. The smaller unit to the right powers everything else.


---
**Al Tamo** *October 11, 2015 07:00*

My option is smoothieboard, dsp its too expensive for me, I love the LCD but its more expensive.


---
**David Cook** *October 14, 2015 20:12*

ah !! seeing this just gave me an idea.   when my replacement power supply arrives  I think I should just add a more powerful 24V power supply as well just to drive the X5 and the motors.   I probably need to connect the grounds together though so that the X5 signals reference properly in the laser supply.


---
**Jonathan penner** *March 06, 2016 03:37*

i am working on doing this upgrade to my laser right now and all the documents i have found on light object and other sites used a different power supply than this one. the one you have is the same as mine did you find a wiring diagram to make it work. any tips?


---
**Gee Willikers** *March 06, 2016 04:00*

It was nothing special, it wires directly to AC power, the ammeter and the dsp. Did you need the pinout of the power supply / connections to the dsp?


---
**Jonathan penner** *March 06, 2016 04:07*

that would help thanks. i thought i had found everthing i needed to do this upgrade but now that its all in front of me i may be in over my head


---
**Gee Willikers** *March 06, 2016 04:21*

Okay, let me put something together.


---
**Jonathan penner** *March 06, 2016 04:22*

thanks man that would be awesome


---
**Gee Willikers** *March 07, 2016 02:04*

Connectors as pictured left to right

<b>4 pin</b>

Grey wire to ammeter and tube

Wire to machine ground post

AC in

AC in



<b>6 pin</b>

ENABLE A -- }

ENABLE B -- } jumper these two or add generic safety switch.

TTL (old test fire button) -- to dsp TTL screw

GROUND (old side a of pot) -- to dsp GROUND screw

PWM (old center of pot) -- to dsp PWM screw

+5v (old side b of pot) -- no connection



<b>4 pin</b>

No connections. Untrusted. Supplied power to old Moshi board.



*So all you should need is 3 wires and a jumper. *



FYI water protection (WP) -- connect your pump pressure switch between WP and GROUND. 


---
**Jonathan penner** *March 07, 2016 02:07*

awesome thank you man this is exactly what i needed


---
**Gee Willikers** *March 07, 2016 02:15*

Oh btw on Laser2 jumper WP and ground or it will throw a water protection error at you. If you want to use the lid switch, it connects to IN1 and GROUND. I have it on a keyswitch myself so the kids can't fire the laser.


---
**Jonathan penner** *March 07, 2016 02:17*

Good idea with the key switch I hope to get started actually putting this all back together as soon as I'm off work tomorrow thank you very much 


---
**Gee Willikers** *March 07, 2016 02:20*

Anytime!


---
**Gee Willikers** *March 07, 2016 03:24*

**+Jerry Martin** Like this?


---
**Jonathan penner** *March 08, 2016 00:26*

ok sorry for all the questions but on the first pin on the 4 pin you mean 2 separate wires correct the grey wire running to the tube and then another running to the ammeter


---
**Gee Willikers** *March 08, 2016 00:38*

Both are grounds. Leftmost pin to the meter (if equipped). The other meter pin goes to the tube.



The second pin goes to the grounding post in the back of the machine or similar.


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/L6PWKhHu95i) &mdash; content and formatting may not be reliable*
