---
layout: post
title: "Yes!!! My first real engraving. Did both sides of my wife's Kindle leather case for her birthday"
date: January 21, 2016 04:51
category: "Object produced with laser"
author: "David Cook"
---
Yes!!! My first real engraving. Did both sides of my wife's Kindle leather case for her birthday. Raster on front and vector on the rear.   So cool. I want to mark and etch alll my stuff now lol.



12% power of 90% limited max

3,500mm/m

30PPM

TurnkeyTyranny Inkscape plugin.



I put a piece of paper down and drew an outline of the kindle case at 10% power to help align the case. Worked good



Thank you everyone in this group for all the help in getting me to this point.   I think I am still under the $700 mark all in on this machine. It's working great .



Engraving leather smells like burning people! Lol





![images/aeb80207ba4573bb4f23b4ac85609678.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aeb80207ba4573bb4f23b4ac85609678.jpeg)
![images/d863e49e44670fd4181491935e297009.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d863e49e44670fd4181491935e297009.jpeg)
![images/6cb0af96c79cb5c488d59e8534ab463f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6cb0af96c79cb5c488d59e8534ab463f.jpeg)
![images/1d2944501e02024ddcad33338b62e881.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1d2944501e02024ddcad33338b62e881.jpeg)
![images/758f13a69d6452367b122e4c497223a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/758f13a69d6452367b122e4c497223a7.jpeg)

**"David Cook"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 21, 2016 05:12*

That turned out awesome. I'm glad it worked for you. It also is concerning to me that you know what burning people smells like haha. I have to admit that the smell of burning leather is quite nasty. To me it smells like when you singe your hair.


---
**David Cook** *January 21, 2016 05:19*

Ha-ha I knew that comment would stand out. I learned to solder as a kid and one time I had a bad experience catching a falling soldering iron in mid air. That taught me to let it drop later in life. But that smell of burn skin yuk 


---
**I Laser** *January 21, 2016 06:25*

Ha, that's a sure-fire way to learn (pardon the pun) lol

The covers look great, nice job!


---
**Richard Taylor** *January 21, 2016 14:53*

I'm guessing it was real leather? I rastered what turned out to be a vinyl covered "leather" case... Not a good idea! (Chlorine gas isn't good for humans)﻿


---
**David Cook** *January 21, 2016 16:23*

**+Richard Taylor** I ended up doing a little research on this case before I beamed it,  turned out to be genuine tanned hide.  It's good to know that some faux leather could be vinyl, It seems that a lot of faux leather is also PU  Polyurethane.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/jL12rk4LrDj) &mdash; content and formatting may not be reliable*
