---
layout: post
title: "I am about ready to give up on my K40"
date: November 14, 2017 19:48
category: "Hardware and Laser settings"
author: "Ron Ginger"
---
I am about ready to give up on my K40. I cannot get the marking with moly to work. I have very carefully checked the alignment. I can do a good job of engraving on wood. If I try to mark an object it simply wipes off. I tried full power and as slow as 5mm/sec.  I have trouble cutting wood- I tried just 1/16" thick bass and it takes two passes at least, then leaves a very wide at at the top. Even 3 passes wont cut through 1/8" acrylic.



I suspect I am not getting the required power out. Has anyone tried one of the power meters that look like a lump of aluminum on a meat thermometer? Is it worth buying one?



Any thing to look for on low power? Alignment and focus seem to be a good as I can get them.





**"Ron Ginger"**

---
---
**greg greene** *November 14, 2017 20:05*

Do you have an air assist head?

If so, make sure the beam from the last mirror hits dead center on the entry hole of the head - AND - make sure the head is not cocked out of 90 degree alignment down towards the work piece.  Other wise the beam will enter the head - but part of it will hit the inner wall of the head instead of directly exiting the bottom.  Also make sure 'bump' of your lens is facing UP


---
**Joe Alexander** *November 14, 2017 20:07*

I would guess that either your alignment is still off or your tube/psu are not outputing properly. What does the meter read on the front panel when your running your job(how many ma). and how far are you focusing the material at? The beam could also be getting "clipped" at the laser head on some air assist heads if you dont hit it dead center, this can also cause a major drop in efficiency.


---
**Buhda “Pete” Punk** *November 14, 2017 20:13*

How is your ventilation, do you have good airflow to clear the mung/soot between the output and the work. .  


---
**Printin Addiction** *November 14, 2017 20:32*

I would loosen the head so that it can move in the mount, then push it all the way up, test fire, down , test fire, etc.... The sweet spot for me was about 4 O'clock near the middle.


---
**Ron Ginger** *November 14, 2017 22:16*

Thanks for the response.

Yes, I have air assist, and I have seen the problem of the beam hitting the cone. I think I am clear on that now. I do have good air flow.  I have been running about 18-20 ma tube current. I have a digital control, but I added a meter. The digital control is showing about 70% when the current is about 18-20. I understand that is the limit one should run these tubes.



I have just ordered a DoHICkey, power measurement probe from Russ Sadler (RDWorks). It will take a while for me to get it, but it will be most interesting to make an actual power measurement.


---
**Joe Alexander** *November 14, 2017 22:49*

yea the max for these tubes is 23ma but that will wear out the tube fairly quickly. Most peeps try to stay down in the 10-15ma area, I myself cut 1/8" birch ply at 14ma, 6-8mm/s with air assist on(large aquarium pump) and i cut in one pass as long as im in focus. I would double check your focus lens that its bump up, a wide beam and high levels make me think its upside down and not focusing the beam properly.


---
**Mark Brown** *November 15, 2017 00:51*

Focal distance?


---
**Alex Raguini** *November 15, 2017 01:57*

I'm experiencing the same problem. 


---
**HP Persson** *November 15, 2017 01:59*

Agree with Mike and others above, it sounds to me as a focal problem. The original k40 bed actually has the focal point inside the clamp, not on top of the bed.



make sure you have 50.8mm between lens under side and surface material for engraving. And centered of the thickness for cutting.

Example: 4mm thickness, you should have 48.8mm between material surface and lens underside.



This rule always applies to everything you put into the machine. If you add a 25mm high metal object, you should still have 50.8mm from material surface to lens under side.



Your problem is probably with this!


---
**Ron Ginger** *November 15, 2017 12:37*

I have added an adjustable table and set the focal distance with a gauge block, although I have some concern about the exact position of the lens. I am sure I am very close, although I have not run a sloped board test for a while. I also changed the mirrors and lens from the original to some I found on amazon.




---
*Imported from [Google+](https://plus.google.com/104419714348493626449/posts/gV283kEnTBg) &mdash; content and formatting may not be reliable*
