---
layout: post
title: "Hi everyone. I am new to laser machine.Purchase it recently.Have not used it ."
date: December 29, 2015 16:21
category: "Hardware and Laser settings"
author: "Dheeraj Arora"
---
Hi everyone.



I am new to laser machine.Purchase it recently.Have not used it .

I am facing the issue that laser goes to intial position and then start moving left and doesnot stop.Please see the video.Any help is appreciated.Thanks


**Video content missing for image https://lh3.googleusercontent.com/-hyqqWyu4a9w/VoKyUhU4VNI/AAAAAAAAA1s/r7WtZia0JEw/s0/VID_20151228_174715.mp4.gif**
![images/02da1ca3b5f0f0a0dff919fbcb0703f8.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/02da1ca3b5f0f0a0dff919fbcb0703f8.gif)



**"Dheeraj Arora"**

---
---
**Stephane Buisson** *December 29, 2015 16:26*

your machine is homing (going to 0.0), unfortunately, it seem the endstop contact is not working. you should check the wiring (disconnected ?) or the switch itself. 


---
**Dheeraj Arora** *December 29, 2015 17:10*

Thanks for quick reply.Wiring seems fine.I am new to laser.Which switch are you talking about where can i see it ? 

Links to my board

[http://i.imgur.com/BS8DsDG.jpg](http://i.imgur.com/BS8DsDG.jpg)

[http://i.imgur.com/lAI69yo.jpg](http://i.imgur.com/lAI69yo.jpg)

[http://i.imgur.com/dhC8yX7.jpg](http://i.imgur.com/dhC8yX7.jpg)


---
**Timothy “Mike” McGuire** *December 29, 2015 17:12*

What type of in stop does your machine use mechanical or optical 


---
**Timothy “Mike” McGuire** *December 29, 2015 17:24*

If the stops are optical then a small thin piece of metal should go between the to half of the optical sensor before Machine 0 check to see if the metal is off to one side it needs to be in the middle of the sensor


---
**Stephane Buisson** *December 29, 2015 17:47*

from your photos I saw you have a Nano board, the connexion is made in the flat white cable, (little chance the problem would be located there). you should look on the other end (XY table), from the sound of it only the switch to the left is affected. check there (as floating cable in the air, this flat cable moving, it's a good chance your problem is located here). a a ohm meter is your best friend.


---
**Brooke Hedrick** *December 29, 2015 20:01*

I also noticed your lcd displays 10.1 when it hits the home position.  Is that the laser mah reading?  If so, that's a bit scary to happen just when homing.


---
**Dheeraj Arora** *December 31, 2015 15:53*

Thanks for the help guys But I still need to put some hard work before it starts woking.Thanks All


---
*Imported from [Google+](https://plus.google.com/118105035571850216408/posts/D3AwnjHNaEV) &mdash; content and formatting may not be reliable*
