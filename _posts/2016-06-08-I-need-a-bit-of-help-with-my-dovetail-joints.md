---
layout: post
title: "I need a bit of help with my dovetail joints"
date: June 08, 2016 11:20
category: "Discussion"
author: "3D Laser"
---
I need a bit of help with my dovetail joints.  I am working on a modular system for the games X-wing and armada.  Right now this is working pretty good but the joints are more loose than I would like.  Do I need to make the tail part a bit bigger if so by how much or is there a way to cut it make it more sung again I am a bit behind the times and I am still using laserdrw



![images/06f13ece7053b9ad2302e4581062553f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/06f13ece7053b9ad2302e4581062553f.jpeg)
![images/07d619f80bf4832f0c659df466ca7a60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07d619f80bf4832f0c659df466ca7a60.jpeg)
![images/f12b5c416f4e3e6b6b757694dbce9ef5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f12b5c416f4e3e6b6b757694dbce9ef5.jpeg)
![images/d71fb59beb6df9126a591d1d115b9949.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d71fb59beb6df9126a591d1d115b9949.jpeg)
![images/c969c2af9adcf912892ea7e13b2d6e0a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c969c2af9adcf912892ea7e13b2d6e0a.jpeg)
![images/2a4ec818edc4fb0a07528e7d1e5d09a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2a4ec818edc4fb0a07528e7d1e5d09a8.jpeg)
![images/9076b91ffda3669cac30fb9ee1bf7a85.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9076b91ffda3669cac30fb9ee1bf7a85.jpeg)

**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 08, 2016 11:47*

You could either make the tail fractionally larger (to account for the laser kerf) or make the groove the tail slots in smaller (again accounting for the kerf). Or you could make tail larger & groove smaller (1/2 kerf each). Then it should be tighter. You will have to check what your kerf is on cutting this material, as it differs per material. If you have digital calipers will make it easier to measure. I would cut a 100mm square & measure what the result is. E.g. 99mm after cut means 0.5mm kerf (1mm total/2 sides).


---
**Stephane Buisson** *June 08, 2016 11:48*

Sorry to not be able to help with your precise request.

You indirectly underline the software issue with the K40. a software which try to do all (design, laser cut settings, and pushing it o K40). a bit too much for doing it well. I would rather to have a good design software, and a second to organize and push to K40.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 08, 2016 11:52*

**+Stephane Buisson** I would agree with that. Would be nice to be able to just design the files so that pieces "fit" & then have the "push" software take into account the kerf & manage adjustments necessary for that.


---
**Jim Hatch** *June 08, 2016 12:07*

The kerf on mine in Baltic Birch runs just shy of 0.1mm in case that helps. 


---
**Stephane Buisson** *June 08, 2016 13:09*

at around 0.1, but wood will expand with humidity. if your wood is very dry when cutting, then you should be very near of good in normal condition.


---
**Jim Hatch** *June 08, 2016 13:41*

**+Stephane Buisson**​ I agree. Definitely a good point to be aware of. With wood you're better off with a little play if it's dry (or conversely, a little snug if cut when very humid). With acrylic that wouldn't be an issue which is probably one reason plastic is used for these types of applications.


---
**3D Laser** *June 08, 2016 13:44*

I do both acrylic and wood for these


---
**Jim Hatch** *June 08, 2016 13:52*

**+Corey Budwine**​ You'll probably want to have two versions of the cutting files - one for wood and one for Acrylic. A parametric design would be better so you could have it update on the fly but I don't think we can do that yet. 



It would be a good feature addition to LaserWeb 2 😀


---
**3D Laser** *June 08, 2016 13:58*

What is a paramedic design 


---
**Ashley M. Kirchner [Norym]** *June 08, 2016 16:13*

**+Yuusuf Sallahuddin**, unfortunately, because of many different factors, you can't simply make a software that 'knows what to do'. Hardware is different. Beam widths are different. Cutting blades are different. Routing bits are all different. Each material will cut differently. It's up to the operator to know their tools, and adjust accordingly. I know, on my K40, for birch, I can offset my lines by 0.1mm and everything will fit perfect. However, for acrylic that's a different value that I don't remember right now (I have them written down near the laser.) When I do dovetails, I offset both sides by 0.1mm. it's a nice snug fit. The front loader I made a while back ([http://tiny.cc/frontloader](http://tiny.cc/frontloader)) has glue holding the axle plates, nothing else. The entire thing is friction fit. Good 'ol 0.1mm offset.


---
**3D Laser** *June 08, 2016 16:21*

Thanks for the advise everyone 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/b6QBVh4ppd5) &mdash; content and formatting may not be reliable*
