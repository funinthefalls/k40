---
layout: post
title: "I recently bought a K40 and I am exporting from Rhino to Illustrator to cut using Corel Draw"
date: September 26, 2016 09:21
category: "Software"
author: "Phillip Meyer"
---
I recently bought a K40 and I am exporting from Rhino to Illustrator to cut using Corel Draw. However, I have noticed that although I am scaling correctly in Corel Draw (100%) it appears to cut smaller. I think that this is because of the thickness of the laser (albeit small) burning a % of the image away.

Does anyone have any good stats of how much you need to scale the outside up and inside down to take account for what is lost when burning?

Thanks!





**"Phillip Meyer"**

---
---
**Anthony Bolgar** *September 26, 2016 09:31*

The kerf cut is usually around 1mm thick. You could try resizing the item by adding 1mm in all directions.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 26, 2016 12:10*

Another method to check your kerf is to cut a fixed size shape (e.g. 20mm x 20mm) & then measure the result to determine what your kerf is.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 26, 2016 12:11*

Oh, the kerf will also vary depending on a few factors (e.g. thickness of material, power level used, focal point for the cut).


---
**Phillip Meyer** *September 26, 2016 12:49*

Hi, thanks for the excellent feedback! I'm cutting 4 to 6mm buffalo horn so it's taking 2 or 3 passes (or more) to get through. I've actually looked and I can see that the edge surface is slightly curved, presumably because each pass takes a little more off the top surface.


---
*Imported from [Google+](https://plus.google.com/117194752728291709572/posts/Bn7fjHaoM4t) &mdash; content and formatting may not be reliable*
