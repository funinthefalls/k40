---
layout: post
title: "Hey all... I just installed one of rays boards in my k40 Can't get laser to fire..."
date: December 17, 2016 17:33
category: "Discussion"
author: "rick jarvis"
---
Hey all... I just installed one of rays boards in my k40

Can't get laser to fire... Any help on what I've done wrong? Thanks

![images/1b2b5ce2843fd2224ae2fbaba29c0f1c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b2b5ce2843fd2224ae2fbaba29c0f1c.jpeg)



**"rick jarvis"**

---
---
**Don Kleinschnitz Jr.** *December 17, 2016 18:00*

I don't see the PWM connection from the controller anywhere?

**+Ray Kholodovsky** ?? Also the Pot (white dotted wire looks strange).  Are two leads cut?


---
**rick jarvis** *December 17, 2016 18:14*

**+Don Kleinschnitz**​ followed Carl's guide, it was what Ray emailed me.. I know I done something wrong, and it may be that. But I did connect that as instructions says.


---
**Don Kleinschnitz Jr.** *December 17, 2016 23:11*

Can you link me to it.


---
**Ray Kholodovsky (Cohesion3D)** *December 18, 2016 00:08*

Spent some time debugging with Rick.  We couldn't get the laser fire working (despite the c3d mini pwm output definitely outputting the expected voltages). I had Rick hook the pot back up and fall back to pulsing the L like you guys are fond of. His physical test button works and I'm waiting for him to report back if he can get the laser to fire from LW. 


---
**rick jarvis** *December 18, 2016 00:55*

Ray spent a long time helping me get things going today and yes we did get it firing through LW. Excellent support and customer service!  Thank you **+Ray Kholodovsky**​

And think you **+Don Kleinschnitz**​ for taking the time to answer today


---
**Don Kleinschnitz Jr.** *December 18, 2016 03:27*

What was the end problem and cure?


---
**rick jarvis** *December 18, 2016 03:43*

Well we never quite figured that out. The control board was putting the expected output for the pwm but wouldn't fire in anyway. We had to wire it a different way as **+Ray Kholodovsky**​ mentioned above. This machine has been very odd...


---
**Ray Kholodovsky (Cohesion3D)** *December 18, 2016 04:13*

**+Don Kleinschnitz** his laser does not seem to respond to any sort of PWM input - and that includes from the single wire L configuration that you consider 'proper'.  It's the ribbon cable and green screw terminals psu k40 build. 


---
**Don Kleinschnitz Jr.** *December 18, 2016 13:10*

**+Ray Kholodovsky** so it still not working, says otherwise above? So how is it wired now?


---
**Stephane Buisson** *December 18, 2016 13:15*

any reference for that PSU ? (year, built number, ...)


---
**Ray Kholodovsky (Cohesion3D)** *December 18, 2016 15:26*

**+Don Kleinschnitz** he can fire, but it seems that the power is the same no matter the S value for PWM. (This was only a limited test so hard to say for sure that this is the case). 

The wiring is all stock with L from the psu going to a different MOSFET on my board (2.5 - the bed MOSFET - which is also pwm capable). 


---
**Don Kleinschnitz Jr.** *December 18, 2016 15:53*

**+Ray Kholodovsky** some things to check. 

1. Does applying a gnd to L fire the laser?

2. If you adjust the pot (assuming it is stock) and then press test does the current in the meter change with the POT position (i.e. up and down)?


---
**Madyn3D CNC, LLC** *December 19, 2016 07:44*

With a PWM output acting up I'd be looking at the variable resistor for sure, along with checking the square wave output on an o-scope will tell a good story. How about noise? These Chinese PSU's could have that modular signal speaking gibberish.. noise + signal = ???? 



Interested in learning more about these boards, as this is the first time hearing about them. Are these oshpark boards or populated PCB's? Very interested, I've been waiting to see some after market control boards for these K40's, I knew it was only a matter of time!


---
**Don Kleinschnitz Jr.** *December 19, 2016 11:54*

**+Madyn3D CNC, LLC** not sure how to help (if you need some) as I have lost track at to where you are with this problem.

Did you try the two tests that I suggested to Ray above?

Scoping the L pin will help to verify the type of signal the LPS is getting from LW. I am not sure what you mean by the "variable resistor". You will need the Current Regulation pot connected even though you are connected to pin "L".

Can you send an updated wiring picture or diagram.




---
**Madyn3D CNC, LLC** *December 19, 2016 21:31*

Thanks Don, I've been building square wave and logic level PWM boards for quite some time now, so I'm just trying to see if my 2cents is worth anything as far as a PWM output goes. Whenever some mystery would occur with any kind of PWM output, for me, 90% of the time my problem was noise disturbing the signal.



Variable Resistor= pot, trimmer, potentiometer, trim pot, etc...



My problem was fixed days ago and you were a huge help with that. There must have been a slow gas leak in the tube that was non-visible, new tube should be here tomorrow. Wasted a lot of time messing with the PSU and the PSU is fine. 



I've only got 1 year with this laser, so I still have lots to learn about these.. However, I have more than a few years with all different square wave outputs. Here's the 555 PWM boards I use to assemble a few years ago...

[http://imgur.com/erUPGyH](http://imgur.com/erUPGyH)


---
**rick jarvis** *January 03, 2017 13:21*

very sorry guys, i have not seen these post. my apologies. i don't own a scope, and i don't know a whole lot about this stuff




---
**Don Kleinschnitz Jr.** *January 03, 2017 13:33*

**+rick jarvis** willing to stay on this problem with you but I cannot tell exactly how it it currently wired.


---
**rick jarvis** *January 03, 2017 14:26*

**+Don Kleinschnitz**​ when I get home I can add photos but it seems to be working the new way **+Ray Kholodovsky**​ had me wire it. I don't have a whole lot of time lately to play with it. But will for sure add photos. Thank you for your help


---
**Don Kleinschnitz Jr.** *January 03, 2017 14:49*

I might have misunderstood. So its working OK then?


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2017 17:00*

**+Don Kleinschnitz** the way we left off, and best I understand nothing has changed, is that Rick can run at a single laser value with the C3D controlling L. This is his "works" but we still need to determine if changing the pot results in different laser fire levels. 




---
**Don Kleinschnitz Jr.** *January 03, 2017 18:27*

**+Ray Kholodovsky** , **+Rick Jarvis** that is simple to test? Change the pot hit "Test  Switch" and notice if the current changes with the pot? Are you thinking that the PWM inside the PS is not working?


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2017 18:32*

I had him meter the PWM output of the C3D and all was behaving according to plan.  Is it possible that the psu would be "frozen" at a particular power level? I think so.  Yeah, we need Rick to test these things before proceeding with any debugging strategy.


---
**Don Kleinschnitz Jr.** *January 03, 2017 18:54*

**+Ray Kholodovsky** The LPS PWM is generated from an internal PWM controller using the POT to set the DF. The "L" signal simply enables the internal PWM to be .... on or off. 

Therefore if the L signal is changing DF and the laser power is constant then ..... well that is weird :(.

Its more likely that the L signal is not changing DF. 

Your suggestion to test with the test button and "L" disconnected at various pot positions is a good starting point.



BTW how did you measure the C3D PWM's DF with a meter?


---
*Imported from [Google+](https://plus.google.com/105230811165966980201/posts/RCWZ6Kh4Ncj) &mdash; content and formatting may not be reliable*
