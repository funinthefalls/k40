---
layout: post
title: "I sliced the printed air-assist open to see what was inside & check if the air tunnels were clogged from the print or if they had been replicated correctly from the design"
date: May 11, 2016 23:51
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
I sliced the printed air-assist open to see what was inside & check if the air tunnels were clogged from the print or if they had been replicated correctly from the design. They were mostly replicated correctly, but the print did no justice to the design, leaving very uneven patches everywhere & a lot of gaps between the layers causing leakage of the air. When plugged into 2 aquarium air pumps I could feel air exiting, but very weakly. The design seemed to funnel the air exiting, but hard to tell with such weak pressure.



If anyone wants the STL to give it a test print you can grab it here: [https://drive.google.com/file/d/0Bzi2h1k_udXwUF8xa0lQTUtMV0k/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwUF8xa0lQTUtMV0k/view?usp=sharing)



Keep in mind the design is still a work in progress. Any feedback from anyone who tests it is much appreciated.



![images/6098bad5853352d3e066c5a03a5a0229.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6098bad5853352d3e066c5a03a5a0229.jpeg)
![images/c82adb2f09f97e8c6b7febf8b7828e30.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c82adb2f09f97e8c6b7febf8b7828e30.jpeg)
![images/0b4904505a44bdcb6439798f425738e3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0b4904505a44bdcb6439798f425738e3.jpeg)
![images/3d5c0e358864d55928cfd91d1d5162ad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d5c0e358864d55928cfd91d1d5162ad.jpeg)
![images/84f760beb259a52b2ca1935f4fa40a69.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/84f760beb259a52b2ca1935f4fa40a69.jpeg)
![images/13203b253e8f9c2f285180638b7754f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/13203b253e8f9c2f285180638b7754f6.jpeg)
![images/d4e396e01d324bd30aab180c824c17e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d4e396e01d324bd30aab180c824c17e1.jpeg)
![images/6d478c0188680f64b4a44a8174925cbe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d478c0188680f64b4a44a8174925cbe.jpeg)
![images/533b95e7d85a00d0b65a014b98888c25.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/533b95e7d85a00d0b65a014b98888c25.jpeg)
![images/3d536a3a6119c0148c5c659c1b2639ee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d536a3a6119c0148c5c659c1b2639ee.jpeg)
![images/319bf7ca02c987a5a4386c1917bf7c4e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/319bf7ca02c987a5a4386c1917bf7c4e.jpeg)
![images/0016941bd61759dbfe21245de094d5e7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0016941bd61759dbfe21245de094d5e7.jpeg)
![images/884d536eb2440cfe4f87e0e9dfbadae0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/884d536eb2440cfe4f87e0e9dfbadae0.jpeg)
![images/f735714b78fc5b0a7c33ac2caf768bc1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f735714b78fc5b0a7c33ac2caf768bc1.jpeg)
![images/25fb7d379170b3a85f36c9d972f15b19.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/25fb7d379170b3a85f36c9d972f15b19.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ashley M. Kirchner [Norym]** *May 12, 2016 04:38*

An FDM printer can not print anything without a bottom support. So your upper "walls", or ceiling, whatever you want to call them, they will always look like crap. What you need is an SLS printer. Send your file to Shapeways, let them print it for you. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 04:45*

**+Ashley M. Kirchner** I will have to look up what FDM & SLS printers are, as I am not really aware of a great deal of the limitations of 3d printing. The resin one I had printed previously came out spectacular. Just my design flaws caused snapped parts. I am getting the same guy to reprint this new one, so we'll see how that goes. I will also look into Shapeways as I don't know what they are either. Thanks Ashley.


---
**Alex Krause** *May 12, 2016 04:46*

Design it square on one side and have it printed vertically with supports that will require clean up might be an option with a FDM printer to get a better quality fdm. But SLS would be the way to go and you could get it made out of 100% metal


---
**Alex Krause** *May 12, 2016 04:50*

**+Yuusuf Sallahuddin**​ SLS selective laser sintering (pro) and FDM fused disposition modeling (at home user)


---
**Alex Krause** *May 12, 2016 04:52*

**+Yuusuf Sallahuddin**​ and SLA printing is photo sensitive resin 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 05:08*

**+Alex Krause** Thanks for that. I just saw that I can have it printed out of stainless steel :) only $40-45 too. I'm assuming USD but that's still pretty good. Although it'll probably be the shipping that kills it.



**+Ashley M. Kirchner** I just took a look at what FDM & SLS & Shapeways were. Thanks for sharing the knowledge. I think I will give it a go that way after I get the design fully prototyped out. According to shapeways model checker thing, my walls are too thin in certain places.


---
**Ashley M. Kirchner [Norym]** *May 12, 2016 14:10*

**+Alex Krause**, SLS can use several kinds of material, including metal, sand, plastics ...


---
**Alex Krause** *May 12, 2016 14:15*

**+Ashley M. Kirchner**​ I know :) I believe the material used for the FDM pictured above has bronze fill in it. I was letting **+Yuusuf Sallahuddin**​ know he could have it printed in all metal if he chose to have it SLS printed


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 22:28*

**+Alex Krause** It was called Brass Fill, with 80% Brass 20% PLA. It's a nice material that would be nice for maybe some other 3d printed pieces that don't require actual functionality (e.g. a small statue).



Thanks to both of you **+Alex Krause** **+Ashley M. Kirchner** for sharing the Shapeways. I like the fact that I can get it printed in metal. The price is not actually too bad either, although will have to see after I revise the model to fix the walls that are too thin.


---
**Ashley M. Kirchner [Norym]** *May 12, 2016 23:12*

Where's your model again? I want to see what I can do with it as far as printing it here at home, at a higher quality.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 23:24*

**+Ashley M. Kirchner** [https://drive.google.com/file/d/0Bzi2h1k_udXwUF8xa0lQTUtMV0k/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwUF8xa0lQTUtMV0k/view?usp=sharing)



You'll have to look at thickening the walls in certain areas as they are only 1mm thick. I will be redoing the model soon to cater for thicker walls in areas where I didn't make it thick enough. I had a thought that this one may have been airtight on the print if the walls were thicker. Might be that the the 1mm thin walls cause the leakage.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 23:26*

**+Ashley M. Kirchner** Actually, I might get onto redoing the model today. I'll share the revised model with thicker walls once I redo it.


---
**Ashley M. Kirchner [Norym]** *May 13, 2016 00:35*

I'd still want to try the current version, before you change it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 02:38*

**+Ashley M. Kirchner** I've created a new version (v1.04) with the thicker walls. Will tag you in the post.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 02:41*

Updated version: [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Ew93jx1Y4hv](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Ew93jx1Y4hv)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/5FCbmVPfNKX) &mdash; content and formatting may not be reliable*
