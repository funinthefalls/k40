---
layout: post
title: "Hi, I just bought a new K40"
date: July 07, 2018 16:22
category: "Original software and hardware issues"
author: "Aart A"
---
Hi,



I just bought a new K40. Done the beam set-up, it is perfectly in-the-middle focus. The issue: The engraving started, moving the head and seems follow the design, but no laser beam. BUT: the TEST button perfectly firing! The pot perfectly adjust the level. The cooling water flowing and 20 degree Celsius. Looks everything is in the right order - except no laser beam during the procedure. Please HELP!





**"Aart A"**

---
---
**HalfNormal** *July 07, 2018 16:33*

Make sure you have the enable switch enabled.


---
**Aart A** *July 07, 2018 16:43*

**+HalfNormal** Yes, it is perfectly ON. 




---
**HalfNormal** *July 07, 2018 16:47*

what software are you using?


---
**Aart A** *July 07, 2018 17:22*

Corel 12 + Corel Laser - Manufacturer"s originals




---
**HalfNormal** *July 07, 2018 17:25*

Make sure the connection from the controller board to the power supply is good.


---
**HalfNormal** *July 07, 2018 17:25*

It seems like you're not getting a signal from the controller board to the power supply to fire


---
**Aart A** *July 07, 2018 17:43*

Just test the cable from the control board to PSU with a multimeter, it is OK. You are right I believe. Would you think I need to change the control panel? Or chasig after where the signal lost? 


---
**HalfNormal** *July 07, 2018 17:46*

Someone else had a similar problem recently. It was actually a bad solder joint on the power supply side. Check out donsblogspot, he has some great troubleshooting ideas there and power supply information.


---
**Aart A** *July 07, 2018 19:00*

Thanks a lot for your very useful posts! 




---
**James Lilly** *July 07, 2018 22:26*

I don't quite remember what if mine followed the same problem, but the magnetic switch in the water interrupt unit was bad.


---
**Aart A** *July 08, 2018 11:51*

Thanks, the magnetic valve was unchecked. But obviously the water flow perfectly so ignored this and have no idea this sensor even exist. Looking after now, thanx again. 






---
**Aart A** *July 08, 2018 12:55*

My machine have no water sensor. It seems the control board or the main unit have some issue. 


---
**Aart A** *July 24, 2018 11:14*

**+HalfNormal** Thanks, but this case the issue are more deper and technical, and an old joke not fit.




---
*Imported from [Google+](https://plus.google.com/113668638090739510724/posts/G1kfD678uPj) &mdash; content and formatting may not be reliable*
