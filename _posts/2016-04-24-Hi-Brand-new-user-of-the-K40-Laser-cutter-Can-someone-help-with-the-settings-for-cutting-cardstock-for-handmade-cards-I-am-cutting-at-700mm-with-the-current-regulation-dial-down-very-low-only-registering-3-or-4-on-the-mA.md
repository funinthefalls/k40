---
layout: post
title: "Hi Brand new user of the K40 Laser cutter Can someone help with the settings for cutting cardstock for handmade cards I am cutting at 700mm with the current regulation dial down very low only registering 3 or 4 on the mA"
date: April 24, 2016 22:05
category: "Discussion"
author: "Donna Gray"
---
Hi Brand new user of the K40 Laser cutter Can someone help with the settings for cutting cardstock for handmade cards I am cutting at 700mm with the current regulation dial down very low only registering 3 or 4 on the mA meter but still charing quite a bit when I go to cut settings in coralLaser it says my power is 75% but it is light grey so I am unable to adjust the power down can anyone please help





**"Donna Gray"**

---
---
**Scott Marshall** *April 24, 2016 22:18*

CO2 lasers don't cut card stock as well as bladed CNC cutters, I don't know of anyone who has been able to get a cut without at least a little charring. An air assist attachment will help, as will cutting faster and raising the power as required. 



Corel does not have control over the laser power in the k40, just ignore the setting and use the knob (also true in LaserDRW)

Speed control is thru the pop up panel when you go to cut, and 700mm/sec is very fast, if you're actually going that fast, you may have trouble with rounded corners or dropped steps, causing distorted cuts.





I use a Silhouette for my cardstock work.

The Laser "shines" on plywood, MDF and Acrylic, but paper materials do char.

It can only be minimized, not prevented.



Scott


---
**Donna Gray** *April 24, 2016 22:42*

What speed would you suggest Scott I wasn't going to buy this laser cutter until someone comment that they use it all the time for cardstock and it doesn't char Now I am just totally confused I must admit though I was excited even just to get it to cut a rectangle out at this stage I am just trying to find the best settings


---
**Scott Marshall** *April 24, 2016 22:59*

Sorry to hear that. As I have the Silhouette, I have not tried to persue the card stock performance extensively, but the word on here is it's very difficult to eliminate charring. I'd imagine it will be even more difficult without air assist. The airflow serves to keep the heat from spreading into the material, and moves pre-combustion gases away from the kerf, thus eliminates actual flare ups (fire) which are common on thicker wood based materials.



I'd start with about 250mm/sec and play with the power until you get the best results. Don't be afraid to go up, but watch out for fire.



The laser cuts by vaporizing the material, not burning as is often thought, with wood fiber, the heat that persists is what causes the charring as I understand it. Airflow from an assist nozzle will cool the area and lessen the charring, as will cutting fast with higher power, vaporizing the cut without allowing time for the heat to build.



The type of paper matters a lot. Some darken with less heat than others, so experimenting with different papers may help.



I hope someone with more paper cutting experience chimes in, I'm afraid that's about all I can offer on the subject, as I haven't spent much time trying to make it work. It may well be possible, I just have not been able to do it, nor have heard of anyone who has. 

I'm always looking to learn new techniques, and would like to hear about a technique that allows me to cut cardstock in the laser, It would have a lot of advantages over the Cameo.



I wish I could help more.



Good luck, Scott


---
**Donna Gray** *April 24, 2016 23:01*

Thanks Scott I am now in contact with the person who says the use it all the time with card stock hopefully he can help me


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 01:20*

**+Donna Gray** As far as I'm aware, speeds over 500mm/s do nothing. First thing I want to suggest (unless you have already done it) is to set your machine id & board model in the CorelLaser preferences. (see [https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing))



Once that is sorted, you will have the machine performing as expected. But speeds of >30mm/s for cutting are just problematic, like **+Scott Marshall** mentions, you may miss steps, have issues on corners, etc. Also, it can cause severe vibration with intricate details (e.g. I cut a series of 0.5mm circles spaced apart 0.5mm & it vibrated the mirrors out of alignment). As you are intending to use for intricate card designs, I wouldn't go above 30mm/s.



Another thing that some people do to minimise charring on the edges is to mask the cutting area with painter's tape first. As you are working with paper/cardstock, you would need a very low tack masking tape (to prevent the paper/cardstock ripping when you remove the tape & to prevent residual adhesive being left on your paper/cardstock). I can't comment on the efficacy of this with cardstock as I haven't worked much with it (except for testing/prototyping/templating purposes & I am not concerned about charred edges on those).


---
**Donna Gray** *April 25, 2016 06:58*

**+Yuusuf Sallahuddin** How do I know what my Machine Id and board model is???Sorry for not knowing this info you have helped so much thanks 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 07:05*

**+Donna Gray** That's fine. We all were at that point to begin with. The machine ID & board model will be on the board inside the machine. On the right hand side of the machine, where the USB enters, you need to open that section up (unplug from power first just to be on the safe side as there are electricals in there). So where the USB cable plugs in is the mainboard. On it will be some details for the board (one will say the device ID & board number). I will post a pic in a minute after I transfer from phone.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 07:15*

**+Donna Gray** Here are some photos of where to find the Device ID & Board Model.



[https://drive.google.com/file/d/0Bzi2h1k_udXwQUxUbmh6bDltYzA/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwQUxUbmh6bDltYzA/view?usp=sharing)



[https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing)


---
**Ben Walker** *April 28, 2016 14:59*

I have been doing done experiments and I have seen charring on more fibrous card stock.  Try it with a smooth cardstock. I turn my dial 1/3 power and cut at 25 mms on a very smooth, heavy cardstock.  When I switch to a thicker textured card stock yes it chars or won't cut through.   And forget about chipboard.   Stinky mess. Fire. #dontgothere


---
**Jeff Johnson** *September 08, 2016 19:16*

I'm able to cut paper without charring at about 2ma and 50 mm/s. 2ma is as low as the laser will operate. Any lower and it buzzes but no laser is produced.


---
**Cherisse Nicastro** *October 13, 2016 03:10*

I have had my machine for 5 days. After way to many hours aligning mirrors, I have been cutting cardstock for the last couple of days, luckily, I needed to cut black for the project I am working on.  I still make sure that I am using the "mirror" option so any chafing is on the back side of the paper. I also have a silhouette, but was hoping to speed up the process with the laser for producing class kits on the new machine. I was also hoping to be able to cut through 2 to three layers at a time... It is working pretty well at 14.5% and 15mm/s for pretty detailed svg designs. (I don't see anywhere on my machine where it displays mA). Any faster and it catches fire.... Lower % and it doesn't get through the paper. All that said I am still having issues with the paper catching fire occasionally. I have been using a piece of cardboard to keep the paper flat... I am sure that it is part of the problem. So, I was looking at buying a "honeycomb" ([lightobject.com - 300X200 Honeycomb. Fit K40 machine](http://www.lightobject.com/300X200-Honeycomb-Fit-K40-machine-P705.aspx)). Since I am such a novice, I was hoping for advise as to whether or not this would help... I have also read abou "air assist", do I need to go there? The primary reason I bought the machine was to cut cardstock. Any advice?


---
**Jeff Johnson** *October 13, 2016 03:16*

**+Cherisse Nicastro** Air assist is vital. It prevents fire and clears away smoke which allows the laser a more clear path and greater power. Without it I get a lot of fires on paper, wood and MDF.


---
**Cherisse Nicastro** *October 13, 2016 04:25*

Thanks Jeff... Now I just need to figure out what to buy... Off to research. Here is what I am think I need so far based on other posts... although I am not super handy, so changing out the laser head scares me....

- [http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)

[https://www.amazon.com/gp/product/B002JLGJVM/ref=crt_ewc_title_huc_2?ie=UTF8&smid=A3N1ITIX788DZC&th=1](https://www.amazon.com/gp/product/B002JLGJVM/ref=crt_ewc_title_huc_2?ie=UTF8&smid=A3N1ITIX788DZC&th=1)

 [https://www.amazon.com/Standard-Airline-Tubing-Accessories-25-Feet/dp/B0002563MW/ref=pd_bxgy_199_2?ie=UTF8&psc=1&refRID=EWNX7APAMZ15BMMWY2RA](https://www.amazon.com/Standard-Airline-Tubing-Accessories-25-Feet/dp/B0002563MW/ref=pd_bxgy_199_2?ie=UTF8&psc=1&refRID=EWNX7APAMZ15BMMWY2RA)



[lightobject.com - 18mm Laser Head w/Air Assist. Ideal for K40 Machine](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)


---
**Jeff Johnson** *October 13, 2016 17:26*

You may need a new focus lens with that head. My lens is 12mm and that head says 18mm. I am using a Harbor Freight airbrush pump because I got tired of my large compressor kicking on and scaring the crap out of me. I'm also using a tiny brass tube angled at the point where the laser cuts rather than a new head. This lets me easily adjust and move it out of the way if needed.


---
**Jeff Johnson** *October 13, 2016 17:32*

This is the pump I use and it provides plenty of pressure. I get a really strong jet of air fro the small brass tube I use. Larger diameter openings will be weaker, though. I cut the curly hose and attached some aquarium tubing from the compressor to the inside of the laser cutter. I'm using the curly hose inside. [http://www.harborfreight.com/1-6-hp-40-psi-oilless-airbrush-compressor-93657.html](http://www.harborfreight.com/1-6-hp-40-psi-oilless-airbrush-compressor-93657.html)


---
**Cherisse Nicastro** *October 14, 2016 02:01*

Jeff, thank you so much! Any chance you could send me a picture of your setup so I can see see the "tiny brass tube" and how you connected it?


---
**Jeff Johnson** *October 14, 2016 13:14*

**+Cherisse Nicastro** Here's a picture. If you need more details I can take some more pictures. The black curly hose is attached to the head carriage so that no pressure is ever put on the brass tube when it moves. I had to wrap teflon tape around the brass tube to form a tight fit. I use 3M One Wrap to secure it. The angle is off in this pic since I just removed it to clean the lens. Make sure it isn't in line with the laser because it will reflect and burn other things. [https://drive.google.com/open?id=0B751t-yy-x-nNzhhTW1fNmRSX2M](https://drive.google.com/open?id=0B751t-yy-x-nNzhhTW1fNmRSX2M)


---
**Cherisse Nicastro** *October 14, 2016 14:05*

Thanks **+Jeff Johnson**! I really really appreciate your help. Now I just need to get the parts and make it happen :)


---
*Imported from [Google+](https://plus.google.com/103145403582371195560/posts/LpWJzSMaafF) &mdash; content and formatting may not be reliable*
