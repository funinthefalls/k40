---
layout: post
title: "My K40 is now 5 weeks old and has finally found a home in my basement!"
date: April 16, 2016 01:21
category: "Modification"
author: "Ulf Stahmer"
---
My K40 is now 5 weeks old and has finally found a home in my basement!  (Although I can't quite understand why my wife didn't like it on the coffee table next to the fireplace, to keep the peace I had to find a different place to vent it  :)  ).  I know that the stock exhaust fan and connection leave a bit to be desired, but I wasn't quite ready to throw in the towel and buy a new blower just yet.



Using some 4" duct piping, and some wood, I made some manifolds and a custom exhaust.  I hacked the fan body as shown in the photos to try to maximize flow.  I also took time to remove the flash off the plastic fan components to ensure smooth flow, especially around the inlet where the flash was really bad.  I chose to use steel pipe rather than the flexible blue pipe to decrease resistance losses.  I also tried to minimize the exhaust length and the number of elbows, again to minimize resistance losses and maximize air flow.  (those fluid dynamics courses years ago finally pay off!)



I'm still going to tape up all the joints, but I'm pretty happy with the performance so far.  Unfortunately I haven't done any real world testing yet by actually cutting something.  That will be next.  Unfortunately, I have to go away on business for the next two weeks.



![images/98175eb23c7e1c89316bf46c513ba9bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98175eb23c7e1c89316bf46c513ba9bd.jpeg)
![images/8f998d092667c82569bd7d3baeee8e2a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f998d092667c82569bd7d3baeee8e2a.jpeg)
![images/361711c3e2694494a665b844ca27f741.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/361711c3e2694494a665b844ca27f741.jpeg)
![images/169efaf683eefb2d96c2ca8ec2f5605a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/169efaf683eefb2d96c2ca8ec2f5605a.jpeg)
![images/c5f26b29c070f6e141b17c55738c4ee1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c5f26b29c070f6e141b17c55738c4ee1.jpeg)
![images/98f326218a3d4d643d1606abbbf0b34c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98f326218a3d4d643d1606abbbf0b34c.jpeg)
![images/8ed3133fdbb96d06512708d66c370119.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ed3133fdbb96d06512708d66c370119.jpeg)
![images/c3a931686476171cc3c0474a42e85cb1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c3a931686476171cc3c0474a42e85cb1.jpeg)
![images/6d5ae56d7cca9fbe0dfc25d16e10f911.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d5ae56d7cca9fbe0dfc25d16e10f911.jpeg)

**"Ulf Stahmer"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 16, 2016 02:18*

That looks like a really good setup. Hope it works out as good as it looks.


---
**Ulf Stahmer** *April 16, 2016 02:57*

Thanks, Yuusuf!  I hope so, too.  I ran an unscientific test using a facial tissue with the original setup and my modded one and this one is definitely better.  Time will tell.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 16, 2016 03:33*

**+Ulf Stahmer** Unscientific test. I like it.


---
**Brien Watson** *April 17, 2016 03:57*

I printed out a great piece for that on the 3D printer. You can get it on [thingyverse.com](http://thingyverse.com), type in K40 Lasers in the search box.  Great pieces in there for the K40,s


---
**Ulf Stahmer** *April 17, 2016 18:40*

Thanks, **+Brien Watson**.  You're right.  Lots of good stuff there.  I modeled one as well, but my 3D printer bed was just a bit too small, and my scroll saw was lonely.  Got to keep my tool family happy, too!


---
**Brien Watson** *April 18, 2016 01:20*

Lol. I hear ya!


---
**3D Laser** *April 19, 2016 01:23*

I would strongly suggest getting a new fan.  I was using the stock one and it's not powerful enough to remove the fumes.  My laser is in my basement and with the stock fan my house began to smell like a bonfire when I cut wood and melted plastic when I cut acrylic.  My new fan is an 8 inch 732 fan and it works great it cost me 90 dollars 


---
**Ulf Stahmer** *April 19, 2016 01:58*

Thanks, **+Corey Budwine**.  I haven't ruled it out.  I just thought that I would try this first.  If it doesn't work, I'll investigate some blowers.


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/NuSRcHnA9eY) &mdash; content and formatting may not be reliable*
