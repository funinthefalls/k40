---
layout: post
title: "Hello everyone. recently I bought a k40"
date: March 06, 2018 13:17
category: "Hardware and Laser settings"
author: "Maurizio Olivieri"
---
Hello everyone. recently I bought a k40.

I installed Corel DRAW 12

I installed CorelLaser 2013.02

I entered the Usb dongle. I started USBKey.exe and entered the Mainboard number.

I started CoralLaser

I entered the MAINBOARD number: 6C6879-LASER-M2

I entered the DEVICE ID number on the mainboard.

I restarted corelDraw.

All this was done on windows 7 and windows XP.

On both operating systems it DOES NOT WORK

Error message always the same: ENGRAVING MACHINE IS DISCONNECTED. How should I do?



Greetings

Maurizio





**"Maurizio Olivieri"**

---
---
**Stephane Buisson** *March 06, 2018 14:47*

Benvenuto **+Maurizio Olivieri**,

A) maybe a poor usb cable (packet loss), try a better one with ferrite.

b) it's a dropbox link on this community for a good version corel download. search into community.

C) try (open source) K40 Whisperer & Inkscape

D) go for a Smoothie conversion (C3D) you will find everything here. and access more open source softwares (Laserweb, Visicut) or more with a small licence fee.


---
**Maurizio Olivieri** *March 06, 2018 19:54*

When I connect the laser to the computer with the usb cable, you should tell me

found new hardware?

Because nothing happens to me.

I tried 3 usb cables.


---
**Anthony Bolgar** *March 06, 2018 20:43*

Download and try K40 Whisperer. Much better than the stock piece of crap that ships with the laser. Or as **+Stephane Buisson** mentioned, for the best solution, upgrade to the COhesion 3D mini board and buy Lightburn or use Visicut or LaserWeb,




---
**robdude1969** *March 08, 2018 02:43*

Test that all your usb ports are functioning. I had to use a hub since two ports were not functioning. Smooth sailing after that - here is the next step... making your first print. 
{% include youtubePlayer.html id="kI8PljQ4f1o" %}
[youtube.com - LaserDRW 3 with K40 laser etching aluminum](https://youtu.be/kI8PljQ4f1o)






---
*Imported from [Google+](https://plus.google.com/109843175636105778254/posts/QhZZjJQmYzP) &mdash; content and formatting may not be reliable*
