---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) Here is a video of the process that I used when creating my files for Vector Engraving (using Adobe Illustrator CC 2015)"
date: April 27, 2016 11:15
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Here is a video of the process that I used when creating my files for Vector Engraving (using Adobe Illustrator CC 2015). I imagine a similar process could be used for other softwares, but I am unfamiliar with them so cannot say for certain.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Anthony Bolgar** *April 27, 2016 11:33*

So you create a set of parallel lines closely spaced, then clip the lines into the desired container shape. Can be done 100% in CorelDraw. So simple now that you have shown me how. Thanks for the information!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 11:58*

**+Anthony Bolgar** Yeah basically that. Just make sure the lines are not too high (0.5mm seems to be good) & also the spacing may need adjusting (I spaced at 0.25mm apart). Might be a bit close.


---
**Ariel Yahni (UniKpty)** *April 27, 2016 14:03*

I have done that before, used it for one of the LW videos to show svg color identification for speed and power. I used Inkscape wich has an option to create the grid just with some values. Also i think there is a plugin for that created for a eggbot. Do you consider in any way the dot size of the laser? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 15:16*

**+Ariel Yahni** I have never actually used this for a project, just remember doing it to test what would happen. Then another user asked the question, so I thought I'd show what happened when I did it/how I did it. That's interesting to know that Inkscape can create the grid with just a few values. Probably much easier than Adobe Illustrator if that is the case. I definitely didn't consider the size of anything, as it was all trial & error in order to get values that gave the result I was looking for (i.e. close enough that it looked like an engrave). It would be worth looking into that as it could give better results by spacing the lines perfectly. Unfortunately, the hole size would vary with power level & things like how well aligned the mirrors are & how close to the focal point. Ideally, it could be calculated, but I've noticed for myself that my dot size is so small that I wouldn't be able to accurately measure it. Would need something like a set of vernier calipers that are capable of measuring extremely small distances (i.e. <1mm).


---
**Ariel Yahni (UniKpty)** *April 27, 2016 15:20*

Sure the spot size can vary but I can't believe there is no spec values on the size. At least a range. My diode has an spec value and it does make a lot of difference for example in LW Raster since you need to scale it before computing 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 15:34*

**+Ariel Yahni** Due to the lack of official support for the K40s, I am less surprised that there is no spec values. It's possible that it is out there somewhere, but probably in Chinese (which I don't speak/read). I think we as a community have a lot of trial & error knowledge, so we could compile together a range. Closest option I've had for measuring the kerf of a certain material is by cutting a 100mm x 100mm square out. On some pieces, I notice that the result is more like 99mm x 99mm, so a 1% loss. I believe that your idea to actually consider the dot size is worth investigation. I'd imagine the original software has to consider the dot size to some degree in their determination of how many steps (or fractions of a step) it will step per mm when doing a standard engrave (e.g. there is the pixel step option that you can change which increases the spacing between the engrave lines). So they must be working off some value. From memory, the document is set to 1000dpi, so I think we could assume it is working off 1000 dots or lines per inch when engraving. Thus a dot would be 1/1000th of an inch (0.0254mm). But I am just assuming so it's probably nowhere near correct.


---
**Stuart Rubin** *April 27, 2016 17:30*

This is a great trick. I never would have thought to use the text as a mask for for the lines like that. I'm sure it will come in handy. I could also see making Ben-Day dots or other patterns for a similar effect.

I used Inkscape on my old homebrewed engraver and made good use of the "Eggbot Contributed" extension called "Hatchfill". As the name implies, you can fill in areas with cross hatches, parallel lines, etc. and it's quite configurable. I highly recommend it. Maybe there is an equivalent for AI.


---
**Ariel Yahni (UniKpty)** *April 27, 2016 17:46*

**+Stuart Rubin**​ that's the one I was referring to ^


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 18:28*

**+Stuart Rubin** I would be careful with the Ben-Day dots. I did try in the past with 0.5mm circles spaced apart & the machine vibrated a hell of a lot. To the point it loosened the alignment bolts on the mirror mounts & through my mirrors out of alignment. Albeit, I may have had the speed too high. I can't recall exactly as I never documented that test.



I'm not certain as to plugins for AI, as I just use it stock. But, I imagine you could do similar crosshatch effects or angled lines just by creating a different pattern to apply the clipping mask to.


---
**Scott Thorne** *April 27, 2016 20:42*

**+Yuusuf Sallahuddin**...how much is A.I?


---
**Ariel Yahni (UniKpty)** *April 27, 2016 20:47*

**+Scott Thorne**​ you are talking price for illustrator?  You can get it by subscription 


---
**Scott Thorne** *April 27, 2016 20:54*

**+Ariel Yahni**...not to sound retarted...but explain please. 


---
**Ariel Yahni (UniKpty)** *April 27, 2016 21:07*

**+Scott Thorne**​ never I will think that.  [https://creative.adobe.com/plans](https://creative.adobe.com/plans) you choose single app and it's 19.99 monthly. Also you could buy the box at Amazon. Most of this companies now sell software as a service since it's cheaper at first


---
**Scott Thorne** *April 27, 2016 21:10*

**+Ariel Yahni**...thanks for the tip...I didn't know that...I'm going to check it out. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2016 01:24*

**+Scott Thorne** I got given 2 years subscription for free as part of a course I was studying, so I'm not certain the overall cost. I have the whole Creative Cloud suite (which looking at the link Ariel provided is $49/m). Personally, I don't like the idea of having to pay for software again & again (i.e. subscription) but I guess it makes it smaller payments than one payment of $1000, so in reality it is probably a better payment structure for end-users than the previous buy-it-outright model.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/3sky7g1fUZD) &mdash; content and formatting may not be reliable*
