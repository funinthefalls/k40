---
layout: post
title: "Hi all. Ok, so I am trying to get familiar with engraving acrylic"
date: April 09, 2017 18:19
category: "Hardware and Laser settings"
author: "Arion McCartney"
---
Hi all.  Ok, so I am trying to get familiar with engraving acrylic.  I have a design that I am trying to engrave with the k40/Cohesion3d.  I can't get it to come out looking decent.  I don't know if it is my settings, the shades of black in the design, or what... Settings I've tried are pot set at 8mA, 18000mm/m, 0-25% power setting is LW4 for raster image.  I've tried 0-15% power and it did not come out any better.  I previously did a vector engraving and it came out much better, but of course that was not a raster engraving.  I've seen many posted pictures of awesome looking raster engravings on edge lit acrylic.  I also tried a non-negative image and it came out even worse.  Am I missing something???   I attached a screenshot of the LW4 workspace of what I am trying to engrave, the other picture is the vector fill path engraving I did that came out prety good, but it was not raster. THANKS!



![images/8bd5c115a5c6877ea1ecfc72558d7463.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8bd5c115a5c6877ea1ecfc72558d7463.png)
![images/17d913b9ed88bf5c367e99f546021fb5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/17d913b9ed88bf5c367e99f546021fb5.jpeg)

**"Arion McCartney"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 20:26*

Did we get your pwm performance tuned? As in the laser will actually respond to the % in LW appropriately?


---
**Arion McCartney** *April 09, 2017 20:43*

Seems to be  a bit better.  I increased "Acceleration" .  I noticed Donsthings ( [http://donsthings.blogspot.com/search/label/K40%20Smoothie%20Configuration](http://donsthings.blogspot.com/search/label/K40%20Smoothie%20Configuration)) has his Acceleration set at 5000, and mine was at 3000.  I haven't gone up to 5000 yet but it seems to be improving results a bit with wood engraavings



[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Smoothie%20Configuration)


---
**Alex Krause** *April 09, 2017 21:59*

Selective dither... The gray needs to to be half tone to get the lighter effect


---
**Arion McCartney** *April 10, 2017 00:23*

**+Alex Krause**​ Ok, so I'd get better results with halftone dither. I'll give it a shot! I'm a little confused as I asked wether dithering was necessary with c3d smoothie and I think **+Ray Kholodovsky**​ recommended to not dither because smoothie varies speed/power for grayscale images... I can't find the post so I can't go back and look because I'm on my phone so I could have referenced that wrong (Ray correct me if I'm wrong in that reference)... 

I've read that lower power is all that's needed to engrave acrylic so that's what I've been doing also. I appreciate the help!


---
**Ray Kholodovsky (Cohesion3D)** *April 10, 2017 00:26*

All I said was you don't <b>have to dither anymore</b> with smoothie because of the variable power control, unlike the stock board.  But you can feel free to do what you need to. It's all art from here on out :)


---
**Arion McCartney** *April 10, 2017 00:29*

**+Ray Kholodovsky**​ Thanks, this is fun learning the ins and outs. Acrylic is kind of expensive to learn on though haha. I'll try dithering this next round of testing and post the differences here.


---
**Alex Krause** *April 10, 2017 00:44*

Acrylic doesn't behave the same as wood with pwm... It doesn't really change the way acrylic Frost's..


---
**Arion McCartney** *April 10, 2017 00:47*

**+Alex Krause**​ good to know, thanks. I'll keep experimenting. 


---
*Imported from [Google+](https://plus.google.com/110543831642314750018/posts/YBuPeaLQuGA) &mdash; content and formatting may not be reliable*
