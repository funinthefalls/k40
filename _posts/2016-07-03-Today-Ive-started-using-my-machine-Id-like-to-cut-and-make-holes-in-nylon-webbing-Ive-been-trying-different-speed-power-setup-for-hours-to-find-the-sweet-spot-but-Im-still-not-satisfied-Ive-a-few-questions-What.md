---
layout: post
title: "Today I've started using my machine, I'd like to cut, and make holes in nylon webbing, I've been trying different speed/power setup for hours, to find the sweet spot, but I'm still not satisfied :( I've a few questions: -What"
date: July 03, 2016 20:35
category: "Hardware and Laser settings"
author: "Akos Balog"
---
Today I've started using my machine, I'd like to cut, and make holes in nylon webbing, I've been trying different speed/power setup for hours, to find the sweet spot, but I'm still not satisfied :(



I've a few questions:

-What is the safe max current to drive the tube?

-What is the max speed, with the original HW? (M2 Nano controller)

-In coreldraw/laserdraw the cutting/engraving toolbar disappears after cutting, and I have to restart coreldraw, to have it again? Is there any fix for that?



-When I'm cutting the nylon webbing with the laser, the fume makes it matte/sticky around the cut. Is there any way, to avoid it, or to remove it after cutting?





**"Akos Balog"**

---
---
**HalfNormal** *July 03, 2016 20:39*

1) You will find the toolbar in the bottom right corner of the task bar. Sometimes it might be in the hidden icon area. I pinned mine to the task bar.

2) You can drive the tube as hard as you want but the higher the current, the shorter the life on the tube. Also you need to make sure you are cooling the tube sufficiently when using higher current. 

3) No experience with nylon. You need to search the web for proper handling when burning.


---
**Ariel Yahni (UniKpty)** *July 03, 2016 20:45*

Welcome. To answer in order. - max should be 15mA or what's rated on the tube. - max speed is about 500mms.  -  in Windows next to the clock the is an small arrow ( windows 10, other windows are similar)  there you will find an icon for the plugging. Not sure about the webbing cut


---
**andy creighton** *July 05, 2016 01:56*

Try cutting through masking tape to prevent the burn marks.   You may also want to check your focus height.  If it isn't cutting that nylon you probably need to raise it closer to the cutting head with some shims. 


---
**Akos Balog** *July 05, 2016 06:01*

Thanks, Yesterday I've checked the focus, and it needed some adjustment. Now it cuts much cleaner. Today I'll try the masking tape.


---
**Akos Balog** *July 06, 2016 09:37*

**+andy creighton** I've tried it, and masking tape helped a lot, not only prevented burn marks, but also helped achieving cleaner/nicer cut ;) Now I've been able, to make truly high quality holes. Just need to make some kind of form/mold, to make positioning the webbing easier.


---
*Imported from [Google+](https://plus.google.com/106984452297752036237/posts/ZbcXx5JJ5xG) &mdash; content and formatting may not be reliable*
