---
layout: post
title: "Hello all - I'm getting strange \"spikes\" at the entry point of my cuts ruining the smooth edge, any idea why?"
date: May 18, 2018 19:55
category: "Original software and hardware issues"
author: "Adam J"
---
Hello all - I'm getting strange "spikes" at the entry point of my cuts ruining the smooth edge, any idea why? I've cleaned the rails - currently back to running stock board - My Smoothie killed itself (it was happening with Smoothie installed too).



Any help much appreciated!

![images/ed92e9e9ffa9697d6f868345a0cfbf8e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ed92e9e9ffa9697d6f868345a0cfbf8e.jpeg)



**"Adam J"**

---
---
**Don Kleinschnitz Jr.** *May 18, 2018 22:38*

What material are you cutting? 

Does it do this with all materials?

Can you give us a zoomed back picture?




---
**Adam J** *May 19, 2018 09:22*

I'm using 3mm acrylic - cutting at 6 m/s @ 12/15 ma. It happens with plywood too, although less obvious because of charring. The bed is level - Tried without air assist and it's the same - Tried reducing power/speed and vice versa. At a loose end here... Used to happen occasionally previously but happening with every cut now - only at laser entry/exit point...

![images/7482851044de97d62cdbf499cab28c83.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7482851044de97d62cdbf499cab28c83.jpeg)


---
**Don Kleinschnitz Jr.** *May 19, 2018 11:40*

**+Pikachupoo** .....thinking .....not an obvious problem, never seen before! Seems it would have something to do with a slightly delayed start.

Does it only do this on the first "start"? 

What does it do if it starts and stops in the middle of a cut?

Does it only do it on circles?



Maybe set up a test to see the problem better:

..Set up a test that cuts long dashes with space between them.

.. Manually cut and polish near the laser cut so you can look in the edge of the material.

..Then look at the edge of the material under magnification ...



Look for clues ....


---
**Randy Ferrill** *May 19, 2018 19:56*

In the CNC as a cutting world they do a piercing off to the side of the part then move to the actual part to avoid just this issue. 


---
**Randy Ferrill** *May 19, 2018 19:57*



CNC plasma cutting


---
**Adam J** *May 22, 2018 18:40*

![images/7ef2a1a56086a69df7c0bb5f4f4e5b87.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7ef2a1a56086a69df7c0bb5f4f4e5b87.jpeg)


---
**Adam J** *May 22, 2018 18:47*

I'll try the test as you suggest Don, before I do, I just wanted to update - 



I decided to swap over my K40 with my back up machine (fresh out the box) so I could finish an urgent project. After finally getting alignment spot on, I noticed the "spikes/splinters" occur on this machine too! So this rules out a lazy power supply/tube. The only thing I've swapped over to this machine was the adjustable air assist head, the lens and #3 mirror.



I've since tried, to no avail:

- Swapping the #3 mirror with a replacement 

- Cutting in two passes with less power / more speed

- Cleaning all the mirrors

- Disconnected air assist hose and cut without it

- Tried cutting ply instead of acrylic (same result)



The only thing I'm doing that's not quite right is using normal tap water presently. Normally use deionsed, need to get some! - 

Surely no link?



Wanted to share this with you first as surely this rules out many things, being a new machine... Any new ideas what this can be?



 

![images/09e84ff0ffe6f980329bbdefa59df804.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09e84ff0ffe6f980329bbdefa59df804.jpeg)


---
**Don Kleinschnitz Jr.** *May 23, 2018 12:44*

**+Pikachupoo** I assume you are running a stock machine with stock control software?

If so we cannot look at what is being sent the machine.



Either the source being sent to the machine has an error in it or you have found something endemic to all K40's, which would be weird and unlikely.



<i>I noticed in the last picture, if I have them lined up right, there seems to be an offset at the start. This observation could be erroneous due to the pictures perspective.</i>



1. Is <b>A&B</b> the starting points of the circle ?



<b>If so:</b>



<b>D:</b> seems to be slightly offset, overlapped etc. 

<b>C</b>: seems to have a chamfer on the edge for some but not all the circumference. 



Could the software be at fault by not starting or ending a circle properly??



<b>Suggestion:</b> Create arcs, circles and lines of different types to see if all objects do this.



<b>Community Call Out:</b> You could also ask others in this community to cut similar circles to see if all our machine do this. Mine is broke! Post the specifications of the circle you are cutting.



<b>Suggestion</b> You could also try K40 Whisperer (free software) and see if it does the same thing with a similar circle.

[http://www.scorchworks.com/K40whisperer/k40whisperer.html](http://www.scorchworks.com/K40whisperer/k40whisperer.html)



![images/5ee05047ca817cc066afbf1df380460c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5ee05047ca817cc066afbf1df380460c.jpeg)


---
**Mark Brown** *May 26, 2018 02:14*

It's definitely doing that at the point where the laser starts?  Otherwise I'd say it's something about the graphics software (Corel?) not closing the shape properly. 


---
**Leonard Shelby** *October 09, 2018 13:36*

I've had the same problem when my parts were not fixed to the workspace (or maybe your workspace itself is a bit wobbly?)


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/Ygm2JiaeDKx) &mdash; content and formatting may not be reliable*
