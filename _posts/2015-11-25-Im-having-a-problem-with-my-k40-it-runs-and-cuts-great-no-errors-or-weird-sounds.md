---
layout: post
title: "I'm having a problem with my k40, it runs and cuts great, no errors or weird sounds"
date: November 25, 2015 05:13
category: "Software"
author: "Michael Isaac"
---
I'm having a problem with my k40, it runs and cuts great, no errors or weird sounds. But the cuts don't match my design. I made the dxf in draftsight, then import and cut with corellaser. Anyone have any idea whats happening here? 

All my settings are default in corellaser. It's weird because the four corner circle cuts are correct relative to each other, same with the four rectangles, but the position of the circles relative to the rectangles and to the outer edge is off. I tried messing with some settings in corellaser, but it just made the problem much worse, so I went back to default.

![images/3cfc97e9e31f8e7d32f5a85310a94f11.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3cfc97e9e31f8e7d32f5a85310a94f11.png)



**"Michael Isaac"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 25, 2015 06:19*

That's interesting, because as you say, everything is in correct x-position, and y-positions are correct on certain elements, however just the entire circles section is offset somewhat on the y-axis. I am curious as to whether the entire process was cut at the same time? I also wonder if there is something wrong with the import process from your dxf/draftsight file (never used it before). Maybe CorelLaser stuffs it up somehow.


---
**Michael Isaac** *November 25, 2015 06:46*

Yes, it was all cut at the same time.

I'm also suspecting something with the dxf file. Draftsight has 16 different versions of dxf to choose from when saving, I've only tried two, with the same results.

I just installed libreCAD, and will try that tomorrow.

Do you have any free CAD programs to recommend that are known to work well with corellaser and the k40? Or specific dxf versions I should use?


---
**Mauro Manco (Exilaus)** *November 25, 2015 07:37*

i have issue with corel laser and dxf format....work better with svg... try to change format i feel corel draw no covert fine dxf file....


---
**Gary McKinnon** *November 25, 2015 12:08*

I use Sketchup, export the model to PDF then import the PDF to the free CorelDraw that came with the cutter, then cut using the CorelLaser plugin. If you want to try this then you have to set a few menu options in Sketchup :



1. View>Edge Style should be only Edges ticked.



2. Camera>Parallel Projection should be ticked.



These settings give good results for me so far, but i haven't cut anything larger than 50mm across yet.


---
**Coherent** *November 25, 2015 13:44*

Make sure you have the correct machine selected or you'll get strange results. If that's not the issue, and it's not another software setting being off and the issue repeats exactly the same, then it's likely a import/export file problem


---
**Michael Isaac** *November 25, 2015 18:31*

Thanks for the replies, I've done some more messing around, but with no luck.

So I tried saving the file in draftsight as PDF and SVG, but both had scaling issues when I imported into corellaser, and the SVG had a big grey rectangle around it. I didn't like these problems, so I didn't try cutting them, atleast not yet.

What programs are you using to create PDF's and SVG's? 

I also tried saving the file from LibreCAD, which uses the 2007 version of dxf, but when I cut it it had the same problem as before.

I remember trying out SketchUp in the past, and it wasn't free, and it was a lot different then the CAD programs I'm use to. So I'm not sure if I wont to dive into that program.

 I think if I don't find a solution to this soon I will convert the cutter to use a smoothieboard, then I could use the CAD/CAM programs I'm use to (I also have a CNC mill). Only problem is smoothieboards are out of stock right now.


---
**Coherent** *November 25, 2015 19:44*

It looks like a pretty simple design. I would duplicate in Corel (or trace to duplicate) then check the dimensions and try the cut directly from Corel/plugin. SVGs are scalable vector files and not a precise cad file, PDF's  are even worse and raster files worse still. You're likely having export/import issues with these files as different software may handle them differently.  I've had no scaling or placement issues directly importing DWG or DXF files from Autocad into Corel & cutting. You may have to "save as" an older version from Autocad for Corel to recognize the file and handle it properly depending on what version of Corel or Autocad you use.


---
**Michael Isaac** *November 26, 2015 00:19*

Marc, I took your advice and tried tracing the pattern in Corel, then I cut that shape, it's even more messed up than the dxf. I have a picture of it, but I'm not sure how to add a picture in a comment here.


---
**Brooke Hedrick** *November 26, 2015 03:09*

Have you tried slowing down the movement speeds?  Any chance steps are getting missed?


---
**Coherent** *November 26, 2015 09:28*

**+Michael Isaac**

 In that case (cutting directly from Corel and/or LaserDrw is off) it's likely your machine or settings in the software. You don't say what model machine you have but double (triple) check your machine model selection. If you are absolutely positive it's correct,  draw some simple squares/shapes and test mark on paper and see if you are loosing steps on an axis.

If so, slow it down like Brooke suggested. If you are already at a reasonable speed, try to locate the cause (loose belt, pulley allen screw etc , or possibly even a bad wiring connection).  These have little quality control when built and travel a long way so are notorious for loose parts and connections. Also, if the machine is powered off, the x/y should move freely and not bind or catch anywhere. A bad stepper or electronics is a possibility, but first eliminate mechanical possibilities.


---
**Michael Isaac** *November 26, 2015 17:38*

I had the wrong machine selected, haha. I don't have time today to see if it works right, probably will tomorrow.


---
**Michael Isaac** *November 28, 2015 01:26*

Now that I have the right main board selected, my laser cutter works great, dxf's are no problem, and it's very accurate.



What had confused me was that on the cutting screen in corellaser you select your tool, but there is only one option, the 320 laser machine, so I thought I had the right one selected. But thenI went to properties, and realized there were 7 different mainboards to choose from, and the default was the wrong one.



 I'm very happy with it now. Thanks for the help everyone.


---
**Gary McKinnon** *November 28, 2015 10:11*

Excellent :)


---
*Imported from [Google+](https://plus.google.com/115218763778220927346/posts/dY5K8fpmdDT) &mdash; content and formatting may not be reliable*
