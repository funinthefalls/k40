---
layout: post
title: "Thanks to this form just posted a video of what I discovered when calculating the focal distance of my lens and must say it was a bit of surprise!"
date: November 25, 2015 19:18
category: "External links&#x3a; Blog, forum, etc"
author: "DIY3DTECH.com"
---
Thanks to this form just posted a video of what I discovered when calculating the focal distance of my lens and must say it was a bit of surprise! Later this afternoon will test it out and see if what I discover was correct and will post an another video!  Thanks again to all for your help!





**"DIY3DTECH.com"**

---
---
**David Richards (djrm)** *November 25, 2015 22:45*

Hi diy, don't forget to take into account the distance between the light source and lens when calculating the focal length of the lens using the method you show. A true reading of focal length will only ever be obtained with an object at infinite distance from the lens. if the lamp is less than 1 meter from the lens then the result will be a few percent off. Here is a handy online calculator I found which shows the theory and can be used to calculate results too. [http://hyperphysics.phy-astr.gsu.edu/hbase/geoopt/image.html](http://hyperphysics.phy-astr.gsu.edu/hbase/geoopt/image.html)

Kind regards, David.


---
**DIY3DTECH.com** *November 25, 2015 23:22*

Many thanks David, the light source in this case was an easy 4 to 5 feet away.  Also understand that I am focusing visible light too so I am simply guessitmating  anyway.  It was a fun experiment all Marc G's fault as he gave me this link :-)


{% include youtubePlayer.html id="v0znprj3xsw" %}
[https://www.youtube.com/watch?v=v0znprj3xsw](https://www.youtube.com/watch?v=v0znprj3xsw)






---
**Stephane Buisson** *November 26, 2015 11:03*

**+DIY3DTECH.com** Your last posts have been block by the spam filter.

Please, to keep this page readable for all, do not overcharge it with link to your videos (including adds), I don't mind a link to your channel once for all. Hopefully you will understand.



I try to limit the number of posts for clarity sake, don't see here a censure, you are welcome to post and don't hesitated to comments.



Thank you.


---
**DIY3DTECH.com** *November 26, 2015 12:50*

Stephanie thank you for the feedback not sure what you mean when say page (I am a bit of Google+ novice) as I only see the link to YouTube and not body.  Is there a better way to share as the only way I know is from the "playlist" share on YouTube.  Thanks again for the feedback!


---
*Imported from [Google+](https://plus.google.com/+DIY3DTECHcom/posts/YapVp2hCoGj) &mdash; content and formatting may not be reliable*
