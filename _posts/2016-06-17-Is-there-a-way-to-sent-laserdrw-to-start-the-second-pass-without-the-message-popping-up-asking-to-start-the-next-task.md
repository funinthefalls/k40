---
layout: post
title: "Is there a way to sent laserdrw to start the second pass without the message popping up asking to start the next task?"
date: June 17, 2016 02:27
category: "Discussion"
author: "3D Laser"
---
Is there a way to sent laserdrw to start the second pass without the message popping up asking to start the next task?





**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 04:53*

Yep there is. What you need to do is as follows:



1. add first task to the queue, make sure that the tickbox "Starting" is not checked, then click "Add Task" button. The dialog will disappear.



2. add second task to queue, again making sure that tickbox is not checked.



3. continue adding tasks until you've added all that you want. Then when you are adding the last task, click the checkbox "Starting" before you press "Add Task".



It should now do all the tasks in one go. Only other thing, there is an option for "Dialog" or "All Layers" in a dropdown box next to "Add Task". I have mine set to All Layers. I have a feeling this is necessary also, but can't remember specifically.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 04:54*

Oh, sorry, I just registered you said LaserDRW. That was for CorelLaser.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/U3xnfpAd5WV) &mdash; content and formatting may not be reliable*
