---
layout: post
title: "Originally shared by Anthony Bolgar I ordered one of these: It can connect via serial RX/TX ."
date: January 19, 2018 15:40
category: "Modification"
author: "Anthony Bolgar"
---
<b>Originally shared by Anthony Bolgar</b>



I ordered one of these: [https://www.banggood.com/3_5-Inch-Nextion-Enhanced-HMI-Intelligent-Smart-USART-UART-Serial-Touch-TFT-LCD-Module-Display-Panel-p-1188732.html?rmmds=search&cur_warehouse=CN](https://www.banggood.com/3_5-Inch-Nextion-Enhanced-HMI-Intelligent-Smart-USART-UART-Serial-Touch-TFT-LCD-Module-Display-Panel-p-1188732.html?rmmds=search&cur_warehouse=CN)



It can connect via serial RX/TX . There is a GUI editor available for free to design screens and program functions, it can control 8 GPIO pins as well as having the serial interface. I am going to make an open source version of the MKS TFT screens that is going to be focused on laser installations. Will be able to control exhaust fans, cooling pumps etc. via the GPIO's and relays, as well as sending move and homing cycles, alignment test firing and other functions using Gcodes sent over serial.





**"Anthony Bolgar"**

---
---
**HalfNormal** *January 19, 2018 15:57*

Found it cheaper here

[robotshop.com - Nextion Enhanced 3.2'' HMI Touch Display](https://www.robotshop.com/en/nextion-enhanced-32-hmi-touch-display.html?gclid=CjwKCAiAy4bTBRAvEiwAFtatHL-az8r1755nrX8qLd9e9hsiNKTlGE6vlUDdXx0Jw9DB2F3qnyvU7RoCnoIQAvD_BwE)


---
**HalfNormal** *January 19, 2018 16:06*

Darn you! I just bought one from the link above. Shipping is not included but it does come from the US and should be here in a few days. DARN YOU! ;-)


---
**Anthony Bolgar** *January 19, 2018 16:32*

Would you like to collaborate on this project? I could use the help:)




---
**HalfNormal** *January 19, 2018 16:34*

Sounds like fun! Keep in touch.


---
**Roger Kolasinski** *January 19, 2018 17:23*

Is that any different from this one for $24.80 with free shipping?

![images/56771b9c54b044a50425ff1d9675dd27.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/56771b9c54b044a50425ff1d9675dd27.jpeg)


---
**HalfNormal** *January 19, 2018 17:25*

Looks like the same model number.


---
**Don Kleinschnitz Jr.** *January 20, 2018 15:30*

Have any of you tried programming this yet? This would be cool for a stand alone laser operations monitor. Its in my "save for later" as I have way to many projects waiting. Will be happy to collaborate ...... 



I assume this is the same one:

[amazon.com - Robot Check](https://www.amazon.com/MakerFocus-Nextion-Enhanced-Version-NX4024K032/dp/B01LAG13DA/ref=sr_1_1?ie=UTF8&qid=1516461451&sr=8-1&keywords=nextion+enhanced+display)


---
**HalfNormal** *January 20, 2018 15:32*

**+Don Kleinschnitz**​ wrong model. Should be NX4832K035


---
**Anthony Bolgar** *January 20, 2018 17:18*

I pulled the trigger on the enhanced 7" display from banggood, it was 10 dollars cheaper than the 5" (I know, no rhyme or reason to pricing sometimes)


---
**Anthony Bolgar** *January 20, 2018 17:22*

**+Don Kleinschnitz** I bought the display for just what ou mentioned, a stand alone operations display for the laser. I am also hoping to build the laser safety system into the display as well.




---
**Don Kleinschnitz Jr.** *January 20, 2018 17:27*

**+HalfNormal** thanks for that catch. I ordered directly from Nextion. They have these all  the way up to 7" displays.


---
**HalfNormal** *January 30, 2018 02:02*

 I just received my display today. It's going to be a bit before I have a chance to play with it though.


---
**Don Kleinschnitz Jr.** *January 30, 2018 12:17*

...takes forever to get them here :(.


---
**Don Kleinschnitz Jr.** *January 30, 2018 18:56*

I need to quit hanging out with you guys. Now I have another project that is more interesting than the many I haven't already finished....



Documentation is sketchy.....

![images/b89f511f2dd1fa8424baf06ca246f663.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b89f511f2dd1fa8424baf06ca246f663.jpeg)


---
**HalfNormal** *January 30, 2018 18:58*

Wow! That was the shortest forever! ;-)


---
**Don Kleinschnitz Jr.** *January 30, 2018 19:15*

OMG I ordered the wrong one and **+HalfNormal** warned me! There is a subtle difference in the part # I did not notice:

NX4832K035 vs 

NX4832T035

Notice the K and T. The T does not have GPIO... UGGHHHGGHG!



Looks like you also need and expansion board to get the local GPIO?



[https://www.itead.cc/display/nextion/nextion-expansion-board.html](https://www.itead.cc/display/nextion/nextion-expansion-board.html)






---
**HalfNormal** *January 30, 2018 19:53*

Thanks! Just ordered one.


---
**Don Kleinschnitz Jr.** *January 30, 2018 21:05*

Pretty limited documentation. No startup docs.

Look up Nextion on Youtube its helpful.



Download editor: [nextion.itead.cc - Download - Nextion](https://nextion.itead.cc/resources/download/)



<b>Power:</b>

Plug the red 5v wire to on micro usb daughter card

Plug the black gnd to the - on the micro SD card

The yellow and blue are serial cables not needed unless you are communicating with another controller.



<b>Programming:</b>

The display program that you create with the display editor is uploaded and then you copy it to SD card and subsequently load it into the display.

You load it into the display be removing and replacing power.



<b>Talking to the display</b>

You can listen to and write to the display using serial and the Nextion library (not tried it).



<b>Support resources</b>

The support forum is rudimentary and somewhat rude.



<b>GPIO</b>

No hints yet as to how you use the GPIO that is if I had the right card :(.



Feel free to correct my errors. So far not sure that an Ada-fruit arduino derivative with a display and library isn't simpler.



I will create a HMI program and then decide if I go by the GPIO version.


---
**Anthony Bolgar** *January 31, 2018 23:31*

Just received my 7" enhanced (the one that uses GPIO). Editor is a little clunky, but not too hard to figure out.




---
**HalfNormal** *February 15, 2018 20:57*

My received my expansion board. Look out GPIO!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/ESg3rEE6AhT) &mdash; content and formatting may not be reliable*
