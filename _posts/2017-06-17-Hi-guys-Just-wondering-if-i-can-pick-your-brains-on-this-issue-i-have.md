---
layout: post
title: "Hi guys, Just wondering if i can pick your brains on this issue i have"
date: June 17, 2017 23:52
category: "Hardware and Laser settings"
author: "1981therealfury"
---
Hi guys,



Just wondering if i can pick your brains on this issue i have.  Basically when i cut a circle or anything that uses both X and Y axis at the same time the cutter never completes the cut, there is always a little bit that just doesn't cut, the issue does not affect squares or any shape with just straight edges.   I have had this issue for several weeks and thought it was due to belt tension issue but i have tested various tensions on all 3 belts and apart from distorting the shape of the circle it has had no effect on the issue i have.  



Anyone had this issue before or any ideas what might be causing it?



Any help is greatly appreciated.

![images/762ec3f76241774f6765336e1bbc7be6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/762ec3f76241774f6765336e1bbc7be6.jpeg)



**"1981therealfury"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 18, 2017 13:47*

I've seen similar things happen when my irregular shapes have curves at the start & finish point too. I'm running LW on mine though, so not sure if there needs to be a slight overshoot built into software for these kind of issues.


---
**HalfNormal** *June 18, 2017 17:04*

I have found that if you are not completely aligned ie everything parallel, then any irregularities in the material can cause issues. As you have noticed, I have to use a little extra power or slow the cut on more complex shapes.


---
**1981therealfury** *June 19, 2017 10:26*

I'm using RDworks not sure if there is a setting for over-cutting but its really annoying, i have tried different speeds and it doesn't make a difference and it only happens on cuts with a curve, straight lines work fine and a square cuts perfectly.  Not sure where to go with this to try and resolve it at the minute.


---
*Imported from [Google+](https://plus.google.com/+1981therealfury/posts/3PpNPfKEjXe) &mdash; content and formatting may not be reliable*
