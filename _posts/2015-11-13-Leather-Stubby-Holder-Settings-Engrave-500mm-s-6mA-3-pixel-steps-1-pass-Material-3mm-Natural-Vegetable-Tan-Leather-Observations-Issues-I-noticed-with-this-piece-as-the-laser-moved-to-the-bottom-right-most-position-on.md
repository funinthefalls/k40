---
layout: post
title: "Leather Stubby Holder Settings: Engrave, 500mm/s 6mA, 3 pixel steps, 1 pass Material: 3mm Natural Vegetable Tan Leather Observations/Issues: I noticed with this piece, as the laser moved to the bottom-right-most position on"
date: November 13, 2015 03:38
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Leather Stubby Holder

Settings: Engrave, 500mm/s 6mA, 3 pixel steps, 1 pass

Material: 3mm Natural Vegetable Tan Leather



Observations/Issues:

I noticed with this piece, as the laser moved to the bottom-right-most position on the leather piece, it started to engrave slightly weaker. It still worked, however the depth of the engrave is fractionally less. I am assuming this is a minor issue with alignment of my laser beam.



Also, once completed I am not so happy with the depth of the engrave. I would have preferred the entire engrave to be deeper. So, next time I will either double the power or slow the engrave speed to about 50% of what it was set at.



![images/616740136bc48b6a789e6a32d9f80dcf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/616740136bc48b6a789e6a32d9f80dcf.jpeg)
![images/f505163f9818931337d001da0c176586.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f505163f9818931337d001da0c176586.jpeg)
![images/8a21fc142b3b200bd332d7547212aad9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a21fc142b3b200bd332d7547212aad9.jpeg)
![images/1a3ac8537ab005d4627d3c3e8003c2f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1a3ac8537ab005d4627d3c3e8003c2f1.jpeg)
![images/ac7069bb50ef8786b8e0a91b3a324d37.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ac7069bb50ef8786b8e0a91b3a324d37.jpeg)
![images/cb2648b3ad8482d1b4d98c3cf5375f52.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cb2648b3ad8482d1b4d98c3cf5375f52.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/aWsB16eVGDE) &mdash; content and formatting may not be reliable*
