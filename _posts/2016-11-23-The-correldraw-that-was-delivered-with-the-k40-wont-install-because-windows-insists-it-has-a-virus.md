---
layout: post
title: "The correldraw that was delivered with the k40 won't install because windows insists it has a virus"
date: November 23, 2016 11:21
category: "Original software and hardware issues"
author: "Eward Somers"
---
The correldraw that was delivered with the k40 won't install because windows insists it has a virus. This coming from china.. I'm not certain if I can trust it. Anyone else had this problem?





**"Eward Somers"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 23, 2016 11:28*

Everyone did. It's because it is a non-genuine version.


---
**Eward Somers** *November 23, 2016 11:57*

Typical china... 


---
**greg greene** *November 23, 2016 13:22*

The virus note is it recognizing the keygen code - many people report this problem- contact the seller


---
**Jim Hatch** *November 23, 2016 13:25*

I did and it wasn't the keygen, it was in a DLL. I just bought a valid copy of Corel (X8) and only installed CorelLaser which is the plugin and is clean. I was unwilling to take the risk of putting a bogus potential virus laden application on my machine.


---
**greg greene** *November 23, 2016 13:29*

Understood, that is the same problem reported by others.  The DLL apparently contains code that mimics the authorization protocol, but in so doing triggers a virus warning. Using a valid copy is the right way to go !


---
**Eward Somers** *November 23, 2016 13:55*

I think I'll swap out the board for one that works with laserweb or something. Will cost me way less.


---
**greg greene** *November 23, 2016 14:14*

true. that's what I am doing


---
**Paul de Groot** *November 23, 2016 21:39*

**+Eward Somers** that's what everyone does. What hardware and software do you consider to use and how much would you spend on that if i may ask?


---
**Cesar Tolentino** *November 23, 2016 21:50*

Does it laser comes with moshidraw? Maybe u can use that for now?


---
**Eward Somers** *November 25, 2016 02:21*

**+Paul de Groot** Think of using laserweb3 with a grbl board. I'm on a student budget right now so I'd like to keep this in the 50$ range if possible. Considering I'll probably want to add stuff like an air assist, new exhaust,etc pretty soon.


---
**Eward Somers** *November 25, 2016 02:22*

**+Cesar Tolentino** It only came with an illegal version of coreldraw.


---
**Paul de Groot** *November 25, 2016 02:26*

**+Eward Somers** if you are not in a hurry i can give u one for free. Or i can send you my grbl shield with the components i have in house but that means you need to assemble it. Just pm me privately if you are interested 


---
*Imported from [Google+](https://plus.google.com/101993772584820352944/posts/AcBSjfETPw7) &mdash; content and formatting may not be reliable*
