---
layout: post
title: "Hi everyone, just bought the K40 from ebay and trying to get it set up"
date: August 30, 2016 22:21
category: "Software"
author: "Toby Burkill"
---
Hi everyone, just bought the K40 from ebay and trying to get it set up. Unfortunately I only have Macs and none of them have CD drives so struggling to sort the software. I've managed to install parallels so I can run windows but now struggling to find moshidraw. Every time I go to the website I've seen mentioned on here before, whenever I try to download the file I just get 'bad request' so not sure what's going wrong. Also I have an orange dongle if that helps? Any help would be massively appreciated. Cheers





**"Toby Burkill"**

---
---
**Paul de Groot** *August 30, 2016 23:22*

Hi Toby i had similar issues. The dongle contains only a key for the software to work. I believe the software is only for windows so you might need to install vm ware on your mac to emulate a windows machine. At work i use inkscape on my mac via vmware. O my winows i only managed to get moshidraw to work and it is very immature.  I can use it but it is not a very productive toolchain so i have developed a prorotype drop-in controller based on open software: Grbl , inkscape and cheton cnc. So far i have used it for a few months and it works great.  If anyone is interested i might produce a small batch. It is really easy to convert just unplug the moshi controller and plug the existing plugs and flat ribbon into the drop controller. Anyone who wants to try just pm me.


---
**Ariel Yahni (UniKpty)** *August 30, 2016 23:30*

Indeed as **+Paul de Groot**​ said no native Mac support. Either VM or boot camp any will do. I have tried both. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 31, 2016 05:50*

Problem is he needs the software as he has no CD drive. Is it MoshiDraw or CorelDraw/CorelLaser/LaserDRW? Check your model on the controller board.


---
**Toby Burkill** *August 31, 2016 07:47*

Cheers **+Paul de Groot**, but yeh like **+Yuusuf Sallahuddin**said it's even just getting the software that I'm having trouble with. Whenever I go through this form I just get a popup saying 'Bad Request'. I'm not sure which model, I'll double check tonight. Cheers everyone

![images/5e33e194738cf044ee55745b12768f41.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5e33e194738cf044ee55745b12768f41.png)


---
**Toby Burkill** *September 01, 2016 13:01*

**+Yuusuf Sallahuddin** So managed to view the CD on an old imac, managed to copy across everything except corel Draw - annoyingly. Also partitioned my mac with windows 8.1 and downloaded corel draw 12 from elsewhere - tried to install but says this application doesn't work with my version of windows. Unfortunately I've not used windows for a good 10 years so I'm struggling with using it. Do you know if there is a certain Windows I need to be able to use corel draw 12? 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 01, 2016 13:10*

**+Toby Burkill** That seems odd. I've used Corel Draw on Win7 & 10 personally & both worked fine. I think I have also seen that others here have used on Win8, but can't say for certain. Might be a dodgy copy of Corel Draw 12. You can run anything up to Corel Draw x8, provided it is not a Home & Student version, so maybe try a different version of CD (I used x5 & x7 successfully with CorelLaser plugin).


---
**crispin soFat!** *September 07, 2016 15:06*

Win8 is currently the most compatible with all the various older and newer applications, while Win7 is simply elderly and Win10 isn't yet fully supported by lots of popular software packages so best avoided for now. I just bought my first PC, a Asus desktop from Fry's for $129!


---
*Imported from [Google+](https://plus.google.com/+TobyBurkill/posts/Z2wntmb1VRQ) &mdash; content and formatting may not be reliable*
