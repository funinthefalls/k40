---
layout: post
title: "Hey our Kickstarter has just gone live! See"
date: July 06, 2017 21:08
category: "Discussion"
author: "Paul de Groot"
---
Hey our Kickstarter has just gone live! See [https://www.kickstarter.com/projects/2118335444/gerbil-the-open-upgrade-for-your-k40-laser](https://www.kickstarter.com/projects/2118335444/gerbil-the-open-upgrade-for-your-k40-laser)





**"Paul de Groot"**

---
---
**Stephen Sedgwick** *July 06, 2017 21:51*

Is this different hardware than **+Ray Kholodovsky** is selling/designed? (assuming so) and is the software different than software that has been discussed in this forum "LaserWeb"?  I am all for shared market place I am just trying to understand if this is an actual push for new functionality or if it is just getting into the "kickstarter" game. 


---
**HalfNormal** *July 06, 2017 23:37*

**+Stephen Sedgwick** This is another alternative replacement option for the K40 original controller. It uses an Arduino UNO running GRBL. Any program that can output G-code can be used. It will not allow you to use the original software that comes with the K40 . 


---
**Paul de Groot** *July 07, 2017 00:09*

**+HalfNormal** It's a super UNO since I designed a new board with an enhanced AVR to meet the 16bits PWM engraving requirements. Thanks for helping out.


---
**Paul de Groot** *July 07, 2017 00:12*

**+Stephen Sedgwick** I designed this for myself but got a lot of friends asking for a controller, so that's why I do this Kickstarter. The actual special feature is the enhanced engraving that makes engraving very easy for non-experienced users. Just giving back to the community and more choice is always nice for people with a limited budget.


---
**HalfNormal** *July 07, 2017 00:15*

**+Paul de Groot**​ thanks for correcting my error. 


---
**Paul de Groot** *July 07, 2017 00:32*

**+HalfNormal**  no worries, just hoping people don't compare it to a basic Arduino UNO and discard its features


---
**Madyn3D CNC, LLC** *July 08, 2017 19:08*

I'm sure there's a lot of people that don't have much experience with smoothie, but plenty of experience with arduino and writing code in general. 



Someone who would fit that description would be me. 



Said person would be easily sold on an arduino based upgrade, while running away scared from a smoothie upgrade. 



I hope this helps a bit with Stephen's concern :) 


---
**Nick Williams** *July 09, 2017 20:26*

Hi Paul Just saw this great :-) Which you all the luck:-)




---
**Paul de Groot** *July 09, 2017 21:59*

**+Nick Williams** thanks!


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/hc1Hkh7Tk7Q) &mdash; content and formatting may not be reliable*
