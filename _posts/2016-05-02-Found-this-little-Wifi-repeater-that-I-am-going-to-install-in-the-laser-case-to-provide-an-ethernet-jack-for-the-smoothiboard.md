---
layout: post
title: "Found this little Wifi repeater that I am going to install in the laser case to provide an ethernet jack for the smoothiboard"
date: May 02, 2016 16:22
category: "Modification"
author: "Anthony Bolgar"
---
Found this little Wifi repeater that I am going to install in the laser case to provide an ethernet jack for the smoothiboard.

[http://www.banggood.com/Wireless-N-WiFi-Repeater-802_11n-Router-Signal-Range-Extender-Amplifier-300Mbps-p-1036563.html](http://www.banggood.com/Wireless-N-WiFi-Repeater-802_11n-Router-Signal-Range-Extender-Amplifier-300Mbps-p-1036563.html)





**"Anthony Bolgar"**

---
---
**Alex Krause** *May 02, 2016 16:29*

Let us know how it works! Hopefully it adhears to standards and allows you to connect to WEP/WPS networks


---
**Ariel Yahni (UniKpty)** *May 02, 2016 16:30*

Does it support Bridge or Point to Point mode? Repeater mode just takes an existing signal to repeat. 


---
**Anthony Bolgar** *May 02, 2016 16:31*

I am not to worried about that, if it doesn't (highly unlikely) I only wasted $14.00


---
**Anthony Bolgar** *May 02, 2016 16:32*

I just want to use it as a repeater.


---
**Ariel Yahni (UniKpty)** *May 02, 2016 16:35*

I like the idea. Will try to find something that will provide an ethernet over wifi so you can connect it directly to smoothie


---
**Jim Hatch** *May 02, 2016 16:39*

It's $18 USD delivered from Amazon (Prime) [http://smile.amazon.com/Wireless-Repeater-MINI300-Professional-Amplifier/dp/B01190X8IA?ie=UTF8&keywords=wireless%20repeater%20ethernet&qid=1462207055&ref_=sr_1_8&sr=8-8](http://smile.amazon.com/Wireless-Repeater-MINI300-Professional-Amplifier/dp/B01190X8IA?ie=UTF8&keywords=wireless%20repeater%20ethernet&qid=1462207055&ref_=sr_1_8&sr=8-8)


---
**Anthony Bolgar** *May 02, 2016 16:45*

**+Ariel Yahni** That is what this is for, it picks up the network wifi signal and repeats it for extra range, but it also has an ethernet jack to use to plug into the smoothieboard.


---
**Ariel Yahni (UniKpty)** *May 02, 2016 16:55*

Guide here [http://www.tp-link.us/article-395.html](http://www.tp-link.us/article-395.html)


---
**Scott Marshall** *May 02, 2016 18:08*

I apologize if I'm missing the obvious here, wouldn't be the 1st time,

but the smoothie board already has Ethernet interconnectivity on it, I don't think the 3X ones have the connector installed, but all you have to do is add one. All the boards are the same, 3,4,5 are just a matter of population.



Maybe I'm missing something here, but worst case you have to add the components to the board if you had an older 3X, 4 and 5's already have them.



That's WIRED ethernet, but I like wired networks for a lot of reasons. Especially on machinery.



It's a neat gadget, and the price is right.



It's similar to (or uses) the Xbee line of Rf boards, if you're tech guy, you can make your own long range WiFi from them.



Scott


---
**Jim Hatch** *May 02, 2016 19:19*

**+Scott Marshall** I don't have an ethernet jack in my garage (or my basement either). My home network exists entirely of an ethernet cable from the incoming cable router (in the corner of the basement) to my WAP/Router in my mudroom upstairs. With this I should be able to put the Smoothie on the network without having to run a cable from the garage to the mudroom.


---
**Scott Marshall** *May 02, 2016 19:50*

The problem with wireless networks is dropouts. They happen, no matter how good the equipment. (I come from industry where RF machinery control is taboo, for good reason) 

I'm not sure if a smoothie will tolerate a short dropout without  PC buffering.  (PC at the Laser) It very well may. 

As long as the you load the Sdcard over the network and then run it from Sd memory, it ought to be ok.



If the software uses the sdcard memory as a buffer and doesn't start the job until the data is complete, it also would be safe.



This is a good Question for Peter . It depends on how the software handles the data.



A 100ft Cat6 ethernet cable is about 10 bucks and NEVER drops out.



I like the piece of mind enough, I just take my PC to my laser. I also like to work from one place, and want to be there when the laser is running. You need to set up the workpiece anyway.

I often hit the start, and as soon as it moves realize something's not right. 



I have a network printer and hate running downstairs to go get my print, but it won't burn if it jams, a laser may.

 I'm fixing up a couple junk PCs just to run my 'gadgets' and the laser is going to get it's own PC in the next couple weeks.



I guess it's a matter of personal preference.

I was just thought maybe you guys did't know the smoothie is Ethernet capable as it is.



Scott


---
**Francis Lee** *May 02, 2016 20:05*

I have this vonet from Amazon for my Xbox 360. It's touchy. When you first set it up, sit right next to your WAP and after that you can walk away to weaker signal levels.


---
**Jim Hatch** *May 02, 2016 20:19*

**+Francis Lee**​​ Thanks for the tip. Fortunately the K40 is only about 20 feet from my current wireless router. Just a room and a wall in between that I don't want to mess around stringing an Ethernet cable to.﻿



 **+Scott Marshall**​ Not to mention my wife would be less than thrilled unless I buried that $10 Ethernet net cable in the wall and there's a sliding glass door on the wall between the router and the K40 (& a slab underneath). I'll suffer an occasional drop out writing to the Smoothie's SD card to save the trouble of dealing with the wiring.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/SNr9RLzEkpU) &mdash; content and formatting may not be reliable*
