---
layout: post
title: "**RESOLVED** ...missing dependency (pyusb) Laptop (that this was 100% working on) decided to die on me (hard drive starting to go bad), so I had to set up on another laptop while I wait for a replacement hard drive in the"
date: September 05, 2018 04:58
category: "Software"
author: "'Akai' Coit"
---
<b>**RESOLVED**</b>

...missing dependency (pyusb)



Laptop (that this was 100% working on) decided to die on me (hard drive starting to go bad), so I had to set up on another laptop while I wait for a replacement hard drive in the mail. Got everything setup per the usual instructions, but running into an issue that gives the error "global name 'usb' is not defined". running from command line, I get "Unable to load USB library (Sending data to Laser will not work.)" right before the gui shows up for k40 whisperer. Google searches have turned up nothing so far. Any suggestions on what I need to fix here?



Running in a Linux environment.





**"'Akai' Coit"**

---
---
**Scorch Works** *September 05, 2018 11:54*

Did you install pyusb? 

"sudo pip install pyusb"


---
**'Akai' Coit** *September 06, 2018 00:03*

That was the missing component. Adding a dependency list to the Linux ReadMe would definitely be in order. 



I can help you with that sometime if you'd like. Basically set up a clean install on a spare laptop I have lying around and figure out all the package dependencies you need to get everything up and running (with the lowest level install, so all necessary deps are covered, not just ones that are already in popular distros) so anyone setting this up can skip having to ask any dependency questions in the future.



This would also be extremely beneficial if I wish to make an installer script for my flavor of Linux in the future.


---
**Scorch Works** *September 06, 2018 01:04*

**+'Akai' Coit**  I am glad you got it working.  Step #10 in the Readme_Linux.txt file should have installed pyusb for you.  (There is a line for pyusb in the requirements.txt file)  


---
**'Akai' Coit** *September 06, 2018 01:24*

Ah, didn't initially notice the requirements.txt file


---
**Timothy Rothman** *September 07, 2018 03:21*

Scorch is on fire!!!  I have gerbil controller but I've not finished hooking it up because I'd miss K40 Whisperer which currently only communicates to the stock nano controller.


---
**'Akai' Coit** *September 07, 2018 06:42*

I see what you did there... :P


---
**'Akai' Coit** *October 17, 2018 04:54*

I should note here the reason I didn't think to follow that step in the instructions is because I'm using Archlinux (or Manjaro, which is a spin off of Arch), and installing anything I usually go through the package manager. Which all the files in requirements.txt are in the repository, so we're all good. :)


---
**'Akai' Coit** *October 17, 2018 05:15*

I think tk is a required package as well. I got a tkinter error today when I set it up on yet another laptop.


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/1tDch4Md88z) &mdash; content and formatting may not be reliable*
