---
layout: post
title: "I want to replace the lens on my K40"
date: September 25, 2017 14:45
category: "Discussion"
author: "bbowley2"
---
I want to replace the lens on my K40.  I've seen prices from $21.00 (Amazon) to $220.00 (Johnson Plastics)  Does anyone know the best recommendation for a replacement lens?





**"bbowley2"**

---
---
**Don Kleinschnitz Jr.** *September 25, 2017 15:07*

Try searching K40Optics, K40 mirrors etc. Lots show up...


---
**ki ki** *September 25, 2017 16:45*

from research I have done online I believe the lens you need is a ZnSe lens, 12mm, gold coated one... I could be wrong so best check for yourself..


---
**Ned Hill** *September 25, 2017 23:20*

I bought mine from Light Objects and have no complaints.  


---
**Paul de Groot** *September 26, 2017 06:44*

Make sure that the lens is made of USA sourced ZnSe which is much clearer than the Chinese sourced lenses. $22 to 40 is a fair price to pay for enhanced engraving and better cutting 


---
**Don Kleinschnitz Jr.** *September 26, 2017 12:47*

#K40Optics


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/PrFdatdf2X8) &mdash; content and formatting may not be reliable*
