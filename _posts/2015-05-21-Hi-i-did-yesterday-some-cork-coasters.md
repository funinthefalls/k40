---
layout: post
title: "Hi, i did yesterday some cork coasters"
date: May 21, 2015 05:43
category: "Object produced with laser"
author: "Robert Bilko"
---
Hi,

i did yesterday some cork coasters. i think they are good, but seems a little bit burned... I did with 2mA and 350 mm/s. When I checked some post by other on forums, they are using 3rd parameter - PPI. My question is, where can i set this or how to adjust the machine for less burning? I am using Corellaser ...

![images/96c7bd6e0cfd13b8da79071cff2a75c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96c7bd6e0cfd13b8da79071cff2a75c5.jpeg)



**"Robert Bilko"**

---
---
**Sean Cherven** *May 21, 2015 13:19*

The K40 does not support PPI. At least as far as I know.


---
**Robert Bilko** *May 21, 2015 13:21*

Oh I see... i am newbie with lasers ... So is there any other way to do it with less burning?


---
**Sean Cherven** *May 21, 2015 13:23*

A faster speed or lower power. How do you set the mA on the laser? It shouldn't even run at 2mA. My laser doesn't kick in until about 3-4 mA


---
**Robert Bilko** *May 21, 2015 13:27*

So i have a analog Ampermeter built in ... I draw a scale to potentiometer (set with test button for laser). So I have a scale from 1 to 18mA...  Faster speed .... so far as i figured out, 400 mm/s is the fastes what i can uses without belt jumping ...


---
**Robert Bilko** *May 21, 2015 13:31*

Make any sence to vary with pixels per step ?


---
**Sean Cherven** *May 21, 2015 13:32*

Pixels per inch would make it easier to control the burning effect, but I don't think these lasers have that feature.


---
**Eric Parker** *May 21, 2015 15:18*

The analog ammeter won't go fulll swing when engraving, so you can't rely on that. Cutting, sure.


---
**Jim Root** *May 22, 2015 02:32*

There is some kind of pixel density control in Laser DWR.  Not sure what I'd does. 


---
**Sean Cherven** *May 22, 2015 02:36*

What's it called


---
**Jim Root** *May 22, 2015 02:38*

I don't have it near by at the moment but it is near the bottom of the engrave/cutting dialog. I won't be back to where my laser is until next Wednesday. I'll check into it when I get back. 


---
**Sean Cherven** *May 25, 2015 18:21*

I wish I knew what the other options were for lol


---
**Robert Bilko** *May 25, 2015 19:07*

So finaly i was able to set the machine for optimal result. There is an option to get the engraving faster and better for cork. I vary with pixel/step value (set for 3, speed was 380mm/s, power 3mA). The cork coaster was much better - not so burned and just as "printed". I used a cover tape - its a little bit much more work with peel of, but it worth. For 1st time was the pixel/steps value 1 - thats why was os burned (this value is normally for cutting), 2 is for engraving... 3 was the optimal value for cork :)


---
*Imported from [Google+](https://plus.google.com/102666509595136399929/posts/fBANEhGPtCo) &mdash; content and formatting may not be reliable*
