---
layout: post
title: "Hey - has anyone etched onto faux suede?"
date: March 03, 2016 13:14
category: "Materials and settings"
author: "Pete OConnell"
---
Hey - has anyone etched onto faux suede? (100% polyester)



If so, what power and speed did you use?





**"Pete OConnell"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 03, 2016 13:40*

Not faux suede, but I did test on real suede. I used somewhere around 4mA power @ 500mm/s engrave (using CorelDraw/CorelLaser plugin).



Turned out reasonably nice, but very subtle (since I was doing it onto black suede).



I'd recommend starting with some scrap (if you have any) & starting at as low a power as you can get it to work & as high as speed as possible. Then tweak from there to get the desired effect.



I'd imagine if the faux suede is 100% polyester it will melt quickly, although I did try on a 100% polyester canvas with same settings (4mA @ 500mm/s) & it gave an interesting effect. (see this pic, orange material is the polyester canvas: [https://drive.google.com/file/d/1E04GzH4rpTSQ1AoAydJSvHs9EBet6enjyQ/view?usp=sharing](https://drive.google.com/file/d/1E04GzH4rpTSQ1AoAydJSvHs9EBet6enjyQ/view?usp=sharing))


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/GuS1bysyYqG) &mdash; content and formatting may not be reliable*
