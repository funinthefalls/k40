---
layout: post
title: "Just playing around with an idea I had for being able to see what the laser is shooting whilst it is engraving/cutting"
date: April 02, 2016 16:49
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Just playing around with an idea I had for being able to see what the laser is shooting whilst it is engraving/cutting.



Basically it is just my old phone (Lumia 520), connected via USB to pc, using Project My Screen app (so I can see it via the laptop screen). I attached it to the laser head via an MDF plate that I created to hold it in place.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 16:49*

**+HalfNormal** Here is my playing around.


---
**HalfNormal** *April 02, 2016 16:52*

Sweet! Great visual. Nice work.


---
**Scott Marshall** *April 02, 2016 18:49*

Cool. Using is as a diagnostic?



Gives me an idea. I've got a couple "lipstick" cams, maybe I'll put one on the carriage so I can safely watch the cut on a monitor. I've got some 7" backup monitors, I could put that right on the top of the panel. The laser can't hurt your eyes that way, worst it can do is wreck a $15 camera.



I'm jealous, I can't even figure out how to post pictures/drawings, and you guys are putting up videos. This Google 'forum' isn't like any other I've used.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 20:27*

**+Scott Marshall** My thought was so that I can stop getting up & straining to see inside while it is cutting. Then I can view it on the screen (while I am editing/preparing other files/parts for cut/engrave).



Can't hurt to be able to video it as I go too, then if something does happen I'll have it caught on film & be able to show people here to get suggestions/fix :)



It'd be nice to have a backup monitor somewhere on the laser to be able to see the result from your lipstick cams.



For pictures & drawings, I usually add them to my Google Drive, then you can share the link from there (or when you click the upload button for photos, you can choose Google Drive). For videos, I posted it to youtube & then shared it from there.



I'll take a couple of screenshots & tag you so you can figure it out.


---
**Scott Marshall** *April 02, 2016 20:48*

Thanks. 

Do you mean  the dropbox when you say "Google Drive", or something else?

When it comes to sharing files online, All I am familiar with is your basic couple of forum engines, this form is totally new to me. I have a similar trouble on Openbuilder. I don't use Facebook or Twitter or anything of that sort, and have posted to Youtube only a couple of times, so am kind of limited in my experience with this sort of thing.

I appreciate the help.

Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 20:59*

**+Scott Marshall** "Google Drive" is similar to dropbox, but Google's version. You can download an app for it for Windows (or Mac OSX if I recall correct) to allow you to have a folder on your computer that you just drop files into & the app will sync them to "the cloud". Basically then you have access to it from any of your Google related apps. I haven't used facebook/twitter or the likes for many years, so I am totally unfamiliar with their new layouts & I really can't be bothered learning them (since they are major procrastination tools).



Google+ is a bit to get your head around to begin with, but once you figure it out, it's not too bad. Still some issues with it that are annoying (e.g. cannot post image as a reply to a post... have to create a whole new post or share the link for an image from elsewhere).



Anyway, I posted some screenshots for you to guide you through posting an image to K40 group. Hope they help (read the captions for the images as they give more info than the pic alone).


---
**HalfNormal** *April 02, 2016 21:34*

I am an old forum fuddy duddy! Hate the restrictive nature of Facebook and G+. Give me search and an easy interface! :-) 


---
**Scott Marshall** *April 02, 2016 23:23*

HalfNormal: Amen brother!



YS,

 Well at you know how to work this newfangled setup here. The only "apps" I have are YouTube and This place (not sure what they call it - "Google Community"? 

Not Impressed either. 99% of the time I want to post is to show someone a drawing, or part. Promised Half a schematic for safeties and auto air timer, and can't post it. Most forums you can just drop it in the post or at worst have to load the image to a storage file and reference it.

Thanks for th info,  I'll hack at it tonight if the mood strikes me.


---
**I Laser** *April 03, 2016 00:02*

**+HalfNormal** I switched back to classic g+ to get the search box. Can do this by going to settings, there's an option to switch back to classic mode. Search box will appear top left under the forum heading and pic.


---
**Scott Thorne** *April 03, 2016 00:21*

Awesome video **+Yuusuf Sallahuddin**....great idea.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2016 00:37*

**+Scott Thorne** Thanks Scott. Thought it's a good way to use an old phone that I won't be using anymore.



**+I Laser** That's one of the reasons I dislike the new G+ setup. No search within the community group (or at least none I could find). At least old G+ had a search & I was used to it's layout.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2016 19:28*

**+Allready Gone** Hahaha. I actually don't really mind a majority of genres of music, as long as it has a decent beat/rhythm. Lyrics tend not to be paid any attention, unless I am specifically focusing on the lyrics.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 05:46*

**+Allready Gone** Haha, I actually use Kangaroo Hide leather mostly these days. It cuts soooo much easier than cowhide, because it is only 1-1.5mm thick, but it has 10x strength of cowhide (per thickness). I can purchase it wholesale from my supplier for about au$100/m2. Can't remember exact price, but very close to that. If you take a look at my wallet pics on my page they were done with kangaroo hide. Engraving is an issue on Kangaroo Hide, as even at lowest power setting & fastest engrave speed it almost burns all the way through. Have to  defocus the lens (by raising the leather closer) to minimise that issue, but it makes the resolution of the image worse. Either that or increase pixel steps, but again it makes the image look not so nice.



40w is difficult to cut through anything thicker than 2mm leather without serious scorching. I tested on a red- dyed piece recently for a phone case & it took me about 40 passes @5mA @ 20-30mm/s (can't remember which) & it was scorched horribly. But then I did my phone case (the brown one I have posted) in a different leather that is same thickness, more stiff, and dyed/embossed with a faux crocodile print & it was 5 passes @ 5mA @ 20-30mm/s & barely scorched at all. It is actually very difficult working with leather because they are all so different in quality. You need to do tests on small offcuts to get the settings specific for that piece before you can cut a final product.



I haven't tried crocodile leather yet, because it is extremely expensive.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 09:24*

**+Allready Gone** Hahaha, sounds like my wallet. Empty most of the time. I haven't sewn a liner on the inside, although I did intend to (I purchased the material for it and all). I just forgot to add it haha. But I purchased a few different materials as a test.



I was wanting to get black canvas, but they had none in stock so they suggested something very similar (can't remember it's name off the top of my head... "drill" I think). Also got some Ripstop Nylon to do a test also. Just haven't had need to make wallet as yet. I even considered adding some colourful printed materials (e.g. with superman or cartoon logos).



A lot of times in past I would line my leather projects with moccasin suede. It adds extra rigidity to the piece as it is approximately 1.5-2.5mm thick.



I have to some degree given up on leatherwork as saleable projects, due to the high expense of leather & people's unwillingness to pay what it's worth for my materials + time. I still do it for hobby/personal/gifts or the occasional commission piece where someone is willing to pay what it's worth.



edit: I forgot to mention, I've seen deer-hide used for a lot of leatherwork projects online and it looks to give quite a nice result also. I'd imagine it would be slightly soft (due to the thinness) similar to the roo-hide. I'd consider lining it with something to increase rigidity for pieces that require it. You can use contact adhesive (for leather/fabric) that will stick the pieces together well & then hand-stitch (or machine stitch if you have a capable machine) together. Actually, some people don't even stitch, they rivet stuff instead.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 22:39*

**+Allready Gone** I haven't bought a wallet for years, as I make my own when I need/want a new one. Actually, my previous wallet I had for about 4 years and it's still in perfect condition. Shows it doesn't get much use haha. I'm going to be posting my lasercut files for the leather wallets I made recently, so keep your eyes peeled for them if you are interested.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Vks8xJ7bir2) &mdash; content and formatting may not be reliable*
