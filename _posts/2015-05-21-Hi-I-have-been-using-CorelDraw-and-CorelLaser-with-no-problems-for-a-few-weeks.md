---
layout: post
title: "Hi, I have been using CorelDraw and CorelLaser with no problems for a few weeks"
date: May 21, 2015 00:30
category: "Discussion"
author: "Trenton"
---
Hi,



I have been using CorelDraw and CorelLaser with no problems for a few weeks.  Today, when I went to click on Engrave or Cut in CorelLaser, it says there is an "Unspecified Error" and it wont let me send anything to the laser. 



Does anyone have any suggestions?



Thanks!





**"Trenton"**

---
---
**tony alexander rico cera** *May 21, 2015 00:34*

umm first time I hear that. Which option apology to record and cut into corel laser which is not


---
**Trenton** *May 21, 2015 00:37*

I'm sorry but I don't understand your comment or question


---
**tony alexander rico cera** *May 21, 2015 00:38*



which is the option to engrave and cut once


---
**Trenton** *May 21, 2015 00:40*

Both engrave and cut do not work.  It just says "Unspecified Error" and wont let me send anything to the laser itself.


---
**Gregory J Smith** *August 19, 2015 10:02*

I guess by now you've got this sorted out.  In my case I have Corel set to engrave or cut only selected objects.  if I forget to select anything I get the "Unspecified error" message.


---
*Imported from [Google+](https://plus.google.com/108213621154298105694/posts/DQi6uWA5VTc) &mdash; content and formatting may not be reliable*
