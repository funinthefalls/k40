---
layout: post
title: "I'm still setting up this K40 and wanted to know which laser settings everyone uses"
date: March 06, 2017 19:44
category: "Hardware and Laser settings"
author: "Nathan Thomas"
---
I'm still setting up this K40 and wanted to know which laser settings everyone uses. I saw a set of instructions online that showed JPG/PLT but alot of youtube vids go between WMF/WMF and bitmaps. Does it make a huge difference? Does it depend on your machine or design you're working on? 





**"Nathan Thomas"**

---
---
**Ned Hill** *March 06, 2017 20:11*

It depends on the version of CD you are using and the input file types.  The really old versions of CD (i.e version 12), that have come stock with a lot of machines, seem to work best with WMF/WMF.  I found that the more current versions of CD had issues with some layered curve file types (i.e. postscript) when using WMF for engraving. For the newer versions of CD (i.e. X5-X8) I think BMP or JPG is fine for engrave, I've used both without issues.  For cutting WMF works fine in most cases, but does sometimes have problems with handling open curve vector cutting (wants to close the curves).  In those instances EMF seemed to be the only output file type to work.  It's certainly possible other output file types may work in any given instance, but the settings I'm recommending seem to work best overall in my experience.


---
**Nathan Thomas** *March 06, 2017 23:12*

**+Ned Hill** gotcha, appreciate ya!


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/ZQS3fHeuAUg) &mdash; content and formatting may not be reliable*
