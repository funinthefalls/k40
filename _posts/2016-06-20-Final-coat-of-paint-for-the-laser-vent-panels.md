---
layout: post
title: "Final coat of paint for the laser vent panels"
date: June 20, 2016 19:23
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Final coat of paint for the laser vent panels. Now drying in the sun. 

![images/b6202471279ea9b1da77eb32ac5ecb7e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6202471279ea9b1da77eb32ac5ecb7e.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ben Marshall** *June 21, 2016 16:19*

Interested to see the final setup.  Keep us posted and good luck


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/Ras8pPeYCZS) &mdash; content and formatting may not be reliable*
