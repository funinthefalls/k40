---
layout: post
title: "Any suggestions or thoughts for an \"in-line\" HV probe or voltage divider running to a DVM LCD on the machine?"
date: December 16, 2016 17:15
category: "Modification"
author: "Madyn3D CNC, LLC"
---
Any suggestions or thoughts for an "in-line" HV probe or voltage divider running to a DVM LCD on the machine?



If you notice the red DVM LCD I already have on the machine, that just reads the voltage for the lipo pack powering my LED's inside the laser, as well as a 'CREE' HID LED as a "head light" on the laser head. Very bright. Total draw for all of the lighting inside the machine is about 35mah. Not very relevant to this post but just a memo. 



Today I'm cutting 2 more holes for 2 more LCD's right next to the one you see in the picture. One will be for water temp LCD and the other one I would like to read voltage output. I'm surprised I can't find more on this, aside from some folks just measuring the readings with a multimeter and HV probe.



Generally, it's not a good idea to mess with the output of these Chinese PSU's, which is why I run my LED's and fan off a LiPoly pack mounted inside the machine in an enclosure. The LiPoly pack is outfitted with a 100mah USB charge port for when I need to juice up the batteries. 



Please excuse my poor editing job, but I figured the easier it is to understand exactly what I want to do, the easier it will be for you guys to share thoughts on this....  

![images/e8f7e7faebbb5143a3189925b763229c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8f7e7faebbb5143a3189925b763229c.jpeg)



**"Madyn3D CNC, LLC"**

---


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/Ae8kYLJAwYX) &mdash; content and formatting may not be reliable*
