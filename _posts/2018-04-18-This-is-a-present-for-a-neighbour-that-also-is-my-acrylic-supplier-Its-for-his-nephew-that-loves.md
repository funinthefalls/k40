---
layout: post
title: "This is a present for a neighbour (that also is my acrylic supplier...) It's for his nephew that loves \""
date: April 18, 2018 17:50
category: "Object produced with laser"
author: "syknarf"
---
This is a present for a neighbour (that also is my acrylic supplier...) It's for his nephew that loves "

paw patrol", and she's going to use it as a night light. 

Made in 3mm clear cast acrylic and mounted on a wooden box that can be used as a mini jewellery.

She is so happy with this.



![images/1fe0634d025799457bb854a2991fd67c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1fe0634d025799457bb854a2991fd67c.jpeg)
![images/48f81beffbf684425d94a6f16d879361.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/48f81beffbf684425d94a6f16d879361.jpeg)

**"syknarf"**

---
---
**Ned Hill** *April 18, 2018 19:48*

That's cool.  Nice work.


---
**syknarf** *April 20, 2018 17:45*

**+Ned Hill** thanks.


---
**BEN 3D** *April 27, 2018 06:08*

Hey great Idea, my Son also love this, should made one :-)


---
*Imported from [Google+](https://plus.google.com/113055105026792621247/posts/apNFwPNTfvr) &mdash; content and formatting may not be reliable*
