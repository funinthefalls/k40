---
layout: post
title: "Hi, I know very little about Laser engraver / cutter I'm looking to buy a Cheap laser Engraver Cutter (40W CO2) it's only for my hobby and I saw a lot of models"
date: November 20, 2015 16:11
category: "Discussion"
author: "Gilles Letourneau"
---
Hi,



I know very little about Laser engraver / cutter



I'm looking to buy a Cheap laser Engraver Cutter (40W CO2) it's only for my hobby and I saw a lot of models.  They almost look all the same so which one to choose?



The answer could be "What do you want to do with a laser machine"



My best material to work on will be plastic, acrylic, plexiglass for engraving and certainly cutting.  



In fact Cutting is the first reason for me to buy a laser machine and engraving will be a bonus :-) 



I have a 3D printer and so, the laser cutter will be a nice device to add on my bench.



The model I'd like to buy is from EcPur.



[www.ecpur.com/itm/2220.html](http://www.ecpur.com/itm/2220.html)



They sell it at $466 USD and they sell an option to 60W for an extra $350.



Question



Should I spend more money and take a DSP model ?



What will I gain if I have a DSP model ?

BTW..  I'm not sure yet of what DSP mean :-)



If cutting is my first goal, should a take the 60W version ?



How thick can I cut plexiglass with a 40W and how more with 60W ?



Which software is commonly use with laser machine like that ?  



Any comment or quick tips I should know before I buy ? 



Many thanks for any suggestions



Gilles from Montreal,





**"Gilles Letourneau"**

---
---
**Imko Beckhoven van** *November 20, 2015 16:27*

That are a lot of questions, seems to me it whoud be smart to do some reading of the stuff posted here. Dont get me wrong but its coming across like you have no idea what youre getting youreself into... ﻿


---
**Stephane Buisson** *November 20, 2015 16:37*

Bienvenue **+Gilles Letourneau**,

first read K40 Modifications & improvements link into to right corner. that will give you an overview  of the situation.

looking at software first could be a good approach, as k40 is a generic term, but the laser cutter could come with different electronic (and softwares to go with).

your link is Moshi the worst, except (but the cheapest), so not so bad if your goal is to replace it.

DSP, is just an other type of electronic, now it's more expensive than Smoothie which came after and use open source softwares.

as hobby 40W is good, as you could go twice for deeper cut into material.

good reading here, as it's plenty of info for you. When you go for it, come back and post your experience.


---
**Coherent** *November 20, 2015 16:37*

Like Imko says, do some research. More power allows cutting thicker material at faster speeds. DSP is a much better controller, better cuts and more file types and options. also look at the cutting area size (bigger is always better) and important options like air assist, adjustable bed, cooling system safety shutoff, and rotary device. Now... go search and read, read and read some more... then buy!


---
**Gilles Letourneau** *November 20, 2015 18:44*

I know I need to read more but I also know that I progress faster when I have the device in front of me to practice and make tests.



I still have question before I buy but I will need to buy to understand better.



From what I see.. a DSP model may be better.  I'm not afraid of any hardware modification because I like hardware but I prefer when the software are good from the start because I'm not a super fan of open source. 



So adding "air assist", "water flow detector" or bed  will be easy for me but I'm lazy when it's time to play with open source and code :-)



The same company sell a DSP model for $1500



[http://www.ecpur.com/itm/2.html](http://www.ecpur.com/itm/2.html)



It's 1000$ more that the standard 40W.



I will continu to read specially about Smoothie.



Question



Should I pay 500$ more for the 60W version of the machine I showed in my first post (basic controler) 



OR



Should I pay 1000$ more for the 40W DSP model ?


---
**Coherent** *November 20, 2015 20:13*

If it were me, I'd choose a 60w with DSP! You'll just have to research and see what best suits your needs. Some of the lower priced board "upgrade" options (like the smoothie) that you see folks talking about here and other forums cut great and use gcode, but  the software cannot raster engrave. That may very well change but as of now unless it's a DSP controller that also engraves, it should be a consideration. If you haven't already visited the LightObject web site, they have a number of upgrades specific to these machines as well general laser hardware/parts and information. So, you could buy your 60w version for $500 more and then spend $550 more and upgrade to color screen X7 DSP Kit which includes good driver software and an air assist head etc.  So in  this scenario it would be to your advantage cost wise to make your initial purchase with some features and add others yourself based on the figures you quoted.


---
**Stephane Buisson** *November 21, 2015 10:11*

Don't be afraid by open source, a development by a main developper but with the community support for improvement and fork, would become quickly a must by comunity decision or be ignored.

(octoprint, Visicut, open office, inkscape, ...) 

free to donwload and try, You wouldn't come back.



for comparison, Moshidrawand the chinese official software is a fail.


---
**Gilles Letourneau** *November 23, 2015 16:04*

Hi Marc,  I will take a part of your advise.  I will buy the 40W and upgrade with the DSP kit form light object.   For the 60W I'm not sure yet.  I'm still looking to see how much ticker I can cut with 60W.


---
**Stephane Buisson** *November 23, 2015 17:04*

Gilles if you really need a powerfull one, you should look that one: [http://www.ebay.co.uk/itm/80W-Co2-USB-Laser-Cutting-Machine-Laser-Cutter-Engraver-Chiller-100x40cm-/221947006609?hash=item33ad128e91:g:TpoAAOSwo6lWIRO-](http://www.ebay.co.uk/itm/80W-Co2-USB-Laser-Cutting-Machine-Laser-Cutter-Engraver-Chiller-100x40cm-/221947006609?hash=item33ad128e91:g:TpoAAOSwo6lWIRO-)


---
**Gilles Letourneau** *November 23, 2015 17:35*

Something else about the 60W updated version...  If I buy the 40W machine...   Can I just change the laser tube for a 60W in the future ? or do I need to change focus lens,  reflection mirror and the power supply also ? 


---
**Stephane Buisson** *November 23, 2015 17:43*

nope, more powerful the laser tube, longer and bigger is the tube, and don't fit into your enclosure. PSU is different, mirror and lens need better spec, update for more power is a no go option.



check max size cut & power before purchase (go big very quickly, do you have space ?)


---
**Gilles Letourneau** *November 23, 2015 17:43*

Hi Stephane,



I just follow your link and it look nice but they don't ship to Canada and I'm in Canada :-)


---
**Gilles Letourneau** *November 23, 2015 17:55*

Ok about the update.  I was suspecting something like that. 



The place where I want to buy "[ecpur.com](http://ecpur.com)" they ask 350$ for the 60W updated version but they don't talk about what else will be change.  I will send them questions and see what they say...   it must be bigger because with this updated version, the shipping cost more because there is an extra 22kg it's probably a bigger machine but they don't give any picture.   That's the problem with chinese..  the price is cheaper but the information is also cheap :-)


---
**Gilles Letourneau** *November 26, 2015 00:29*

I think I found the Laser machine that I want.  There is a lot of model and capacity but it's only for hobby.  At the same time I didn't want something too small and reach the limit too fast.  Also buying in China take very long time for delivery in Canada or it cost 1000$ for delivery by air.  So I found one that can be ship by ground and get here in 10 days but now before I make the final move I still have a question.



The model is a 50W with a 400 X 400 mm electric level honeycomb bed.  Air assiste and water pump included.  It is not a DSP controler but it is direct CorelDraw, WinSealXP and LaserDRW. (don't support MoshiDraw)  



The price is 2000 US ship to my door.



Question:



With a 50W what is the max size of plexiglass that I can cut ?  I know I could pass more than one time but I think the cut it less nice when we do that (not sure)  



How is the price with the description I gave ?﻿


---
*Imported from [Google+](https://plus.google.com/101789423601866268108/posts/NcvSq3KD2Zj) &mdash; content and formatting may not be reliable*
