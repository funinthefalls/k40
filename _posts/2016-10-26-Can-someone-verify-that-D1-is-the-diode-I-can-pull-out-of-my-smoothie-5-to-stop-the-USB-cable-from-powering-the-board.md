---
layout: post
title: "Can someone verify that D1 is the diode I can pull out of my smoothie 5 to stop the USB cable from powering the board"
date: October 26, 2016 05:39
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
Can someone verify that D1 is the diode I can pull out of my smoothie 5 to stop the USB cable from powering the board.



Are there any side effects from doing that? Other than you can't power from the USB that is :).





**"Don Kleinschnitz Jr."**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 26, 2016 05:50*

It's installed on my boards now, but I have run without it and all is good. 


---
**Ray Kholodovsky (Cohesion3D)** *October 26, 2016 05:52*

I want to add, I hope you have a 5v supply ready to power the smoothie, as I don't trust the 5v coming out of the laser psu. 



Also I added a normally connected solder jumper on the bottom of my boards in case anyone wants to do what you did they would just cut the trace there. 


---
**Don Kleinschnitz Jr.** *October 26, 2016 13:57*

**+Ray Kholodovsky** the cut jumper would be nice. I am running a fat 24V supply with the 5V regulator installed in both the smoothie and the GLCD adapter so I should be ok.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/cBNeh9tYtDj) &mdash; content and formatting may not be reliable*
