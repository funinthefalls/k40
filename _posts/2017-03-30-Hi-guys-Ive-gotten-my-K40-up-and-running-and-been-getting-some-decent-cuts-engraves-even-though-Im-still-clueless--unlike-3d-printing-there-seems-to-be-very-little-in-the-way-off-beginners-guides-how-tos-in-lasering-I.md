---
layout: post
title: "Hi guys, I've gotten my K40 up and running and been getting some decent cuts/engraves even though I'm still clueless  (unlike 3d printing, there seems to be very little in the way off beginners guides/how tos in lasering) I"
date: March 30, 2017 03:34
category: "Discussion"
author: "Craig \u201cInsane Cheese\u201d Antos"
---
Hi guys,



I've gotten my K40 up and running and been getting some decent cuts/engraves even though I'm still clueless 🤔 (unlike 3d printing, there seems to be very little in the way off beginners guides/how tos in lasering)



I tried to cut this last night, and for some reason the circles/curves come out... Well not circles or curves.



The other stuff I've been cutting seems to be ok, I'm wondering where the screw up is. The file looks fine when imported into LaserWeb4. I've occasionally been getting the edges of raster engraves not lining up, but the image looking fine.



Any ideas?



K40/Cohesion3d Mini, mirrors and head are aligned best I can.



![images/e6ed9cf814874891326fa7e7c3da2d4b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e6ed9cf814874891326fa7e7c3da2d4b.jpeg)
![images/97ded234ee5c06e60986c1d3b1dc3b83.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97ded234ee5c06e60986c1d3b1dc3b83.jpeg)

**"Craig \u201cInsane Cheese\u201d Antos"**

---
---
**Alex Krause** *March 30, 2017 04:18*

How fast we're you traveling on your cut path?


---
**Alex Krause** *March 30, 2017 04:20*

Also have you checked to make sure your mirrors aren't wobbly and the lens is secure a floating lense can cause this


---
**Craig “Insane Cheese” Antos** *March 30, 2017 04:20*

6mm/s at 40%. It's 3mm cast acrylic.


---
**Craig “Insane Cheese” Antos** *March 30, 2017 04:22*

I'll double check all of the mirrors/lens when I get home. 


---
**Alex Krause** *March 30, 2017 04:22*

That speed rules out that


---
**Alex Krause** *March 30, 2017 04:22*

6mm/s is a decent speed 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2017 04:44*

Could belt tension also be responsible for the wobbly cuts on the curves? Or would it show on the straight lines too?


---
**Craig “Insane Cheese” Antos** *March 30, 2017 07:29*

Great success! Turns out the screws holding the gantry mirror had come loose. The crappy 5mm screws they installed barely bit the mirror holder correctly. Replaced them with some 10mm hex heads with split washers. That should hold it properly. Looks like most of the screws are barely long enough. Replaced the motor screws as well.


---
*Imported from [Google+](https://plus.google.com/114851766323577673740/posts/6JNdY18XeEM) &mdash; content and formatting may not be reliable*
