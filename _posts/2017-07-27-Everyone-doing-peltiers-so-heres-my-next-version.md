---
layout: post
title: "Everyone doing peltiers, so heres my next version"
date: July 27, 2017 21:39
category: "Modification"
author: "HP Persson"
---
Everyone doing peltiers, so here´s my next version.

Had a 4x40w peltier earlier that actually worked in my Swedish cold climate.

This one is 4x80w



But, what if we water cool the hot side too?

Well thats what i´m trying to find out, if i can grab some more cooling efficiency out of it :)



Setup will have three loops, one to cool the hot side with a 240mm radiator, and the other loop will go from the peltier down to the lasers coolant tank. And the third from the coolant tank trough the laser. Totally 3 pumps.

Do not want to hook it up directly to the tube just yet :)



Will do a couple of tests

- One block with air cooled heatsinks on the hot side

- Three blocks with water cooled hot side

And then with and without running the machine.



Just have to make a clamp to hold everything together, tried with cable ties but they wasn´t enough. Shoot a idea if you have one on the clamping situation :)

![images/43482552b211f7b1fad296230ded6004.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43482552b211f7b1fad296230ded6004.jpeg)



**"HP Persson"**

---
---
**Don Kleinschnitz Jr.** *July 27, 2017 22:17*

Interesting ....



What clamp came to mind quick. 



With radiator cooling the hot side won't get lower than ambient though will it? My problem is ambient is now in the 90F.



Not clear in the picture but the middle plates are intended to be threaded side rails the top plates bolts screw into. Pulls from top to middle and bottom to middle.

![images/463d9c2bcd433dbd2885165f9b7dc35f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/463d9c2bcd433dbd2885165f9b7dc35f.jpeg)


---
**HP Persson** *July 27, 2017 22:30*

**+Don Kleinschnitz** Did some quick calculations, a water block can carry the heat away from the peltier quicker, than a heatsink with a fan can do. Both cooled with ambient air though and it will never get below ambient correct, but it´s how quick i can get there and how close to ambient i can go :)



It depends on the heat sink too of course, and it´s ability to pick up the heat. Same reason why water cooling is popular at computers over air cooling, it´s more efficient. Both still using ambient air.



That got me thinking if i watercool it, will i gain anything worth doing it this way :)

If i can get the temp on the hot side down closer to ambient, i would gain on the cold side too.

Just a crazy idea yet, and i love ideas, there´s where facts are born :)





Plate with bolts tightening it would work, great pic, got my ideas flowing again :)


---
**Steve Clark** *July 28, 2017 03:04*

Does this help your thoughts? 



[tellurex.com - www.tellurex.com/media/uploads/PDFs/peltier-faq.pdf](https://www.tellurex.com/media/uploads/PDFs/peltier-faq.pdf)



Look at para #15


---
**William Kearns** *July 28, 2017 04:01*

Second attempt ![images/cf1f452c79a1ef1bdc6d555f0293ff17.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cf1f452c79a1ef1bdc6d555f0293ff17.jpeg)


---
**Claudio Prezzi** *July 28, 2017 07:05*

You can stack multiple peltiers on top of each other to get more temp difference ;)


---
**Don Kleinschnitz Jr.** *July 28, 2017 14:40*

**+Steve Clark** **+HP Persson** looks like it would be better to stack modules horizontally across a heat exchanger vs vertically in a stack.

Use longer exhangers vs smaller stacked?



[https://www.amazon.com/RDEXP-122x41x12mm-Aluminium-Graphics-Radiator/dp/B06XX11R4L/ref=sr_1_2?ie=UTF8&qid=1501252772&sr=8-2&keywords=peltier+heat+pipes](https://www.amazon.com/RDEXP-122x41x12mm-Aluminium-Graphics-Radiator/dp/B06XX11R4L/ref=sr_1_2?ie=UTF8&qid=1501252772&sr=8-2&keywords=peltier+heat+pipes)





<b>These seems pretty cheap?</b>



[amazon.com - Amazon.com: Aibecy DIY Kit Thermoelectric Peltier Cooler Refrigeration Cooling System Heat Sink Conduction Module: Home Improvement](https://www.amazon.com/dp/B072QBGLXB?psc=1)



[https://www.amazon.com/gp/product/B01J1S15UK/ref=crt_ewc_title_dp_1?ie=UTF8&psc=1&smid=A2RASZZ5MQ7SCV](https://www.amazon.com/gp/product/B01J1S15UK/ref=crt_ewc_title_dp_1?ie=UTF8&psc=1&smid=A2RASZZ5MQ7SCV)


---
**Steve Clark** *July 28, 2017 23:24*

From what I’ve read stacking is only if you want to increase you cold to heat ratio a lot and way below what we need.  Efficiency wise, apparently if one wants to only reduce the ambient temperature say  between 10 or 30 degrees it’s better to run them parallel and in series. 



Oh...before I forget...are you using thermoconductive grease between your layers?


---
**HP Persson** *July 29, 2017 12:31*

**+Steve Clark** Tried with thermopads first, but changed now to real thermal grease.


---
**HP Persson** *July 29, 2017 21:36*

The tests begins now.

Went with this clamp-style from **+Don Kleinschnitz** idea earlier.



Any suggestion on data to keep track of?

Right now i´m measuring coolant temp, hot side temp, ambient temp.

And i will time cool down and heat up times when machine is running aswell.



Got some more TEC´s, so after this i´ll try some stacking/cascading too :)



Had to rebuild a old computer PSU to keep up with the power :P

![images/f6bcaac6379f4f04c5c07eb1ba2eba6b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6bcaac6379f4f04c5c07eb1ba2eba6b.jpeg)


---
**Don Kleinschnitz Jr.** *July 29, 2017 21:41*

Water flow? Radiator in out/temp.


---
**HP Persson** *July 29, 2017 21:42*

**+Don Kleinschnitz** Smart, i got a arduino somwhere around with a flow meter on. I´ll hook it up!

Nice to see if increased flow really give any better cooling too, or at what flow it´s not helping.




---
**HP Persson** *August 04, 2017 22:32*

First test was back-asswards, had the peltiers flipped the wrong way :P



But, here is the first test results.



4x80w peltiers, connected to a 750w PSU at 12V.

Pulled about 34A continuously :P

40x80mm water blocks with four TEC 12708.



How i tested it

27.5c ambient, identical pumps in each tank.

All water was 27C when the test started.

Temperature of the hot and cold side measured in the tank.



Test 1

Hot side on 2 water blocks - 10L tank, 240mm radiator

Cold side on 1 water block - 10L tank

After 30min - cold side temp: 16.2c



Test 2

Hot side on 1 water block - 10L tank, 240mm radiator

Cold side on 2 water blocks - 10L tank

After 30min - cold side temp: 12.7c





Bonus test

2x60w peltiers, one 40x40mm water block. Hot side air cooled

After 30min - cold side temp: 22.4c

![images/76a424bcbcac499fe4ed90eec06e4f3c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/76a424bcbcac499fe4ed90eec06e4f3c.jpeg)


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/S8hdAGRKnmw) &mdash; content and formatting may not be reliable*
