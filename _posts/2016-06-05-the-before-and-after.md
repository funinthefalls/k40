---
layout: post
title: "the before and after"
date: June 05, 2016 11:29
category: "Object produced with laser"
author: "Scott Thorne"
---
the before and after.



![images/023fe7bda41503fe8e0bb0fa2fca763b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/023fe7bda41503fe8e0bb0fa2fca763b.jpeg)
![images/62c17ec18883ba819afd87af6cc5dce0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/62c17ec18883ba819afd87af6cc5dce0.jpeg)

**"Scott Thorne"**

---
---
**Anthony Bolgar** *June 05, 2016 12:16*

Real nice...does your machine do greyscale, or is that dithered?


---
**Scott Thorne** *June 05, 2016 13:09*

+it's dithered lightly...but I have to adjust contrast and brightness before rastering using rdcam laserworks.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 20:40*

Thanks Scott. It's good to see the process pics. It's amazing how much of the detail is retained in the final piece.


---
**Todd Miller** *June 06, 2016 00:43*

Dude !  You are the 'GrayScale' Master !


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/RqCDNHXsXic) &mdash; content and formatting may not be reliable*
