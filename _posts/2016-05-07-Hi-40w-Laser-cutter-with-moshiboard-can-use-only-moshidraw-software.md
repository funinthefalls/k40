---
layout: post
title: "Hi, 40w Laser cutter with moshiboard can use only moshidraw software?"
date: May 07, 2016 10:15
category: "Software"
author: "Anderson Firmino"
---
Hi, 40w Laser cutter with moshiboard can use only moshidraw software? 😢😢😢





**"Anderson Firmino"**

---
---
**3D Laser** *May 07, 2016 10:38*

I have a extra nano board make me an offer and its yours


---
**Vince Lee** *May 07, 2016 22:47*

With a moshiboard you can 1) draw in MoshiDraw, or 2) draw in another app like Inkscape and import DXF files into MoshiDraw only when you want to send the files to the K40, or 3) draw in CorelLaser and use the MoshiLaser utility for CorelDraw to send the files to the K40.


---
**Anderson Firmino** *May 11, 2016 02:47*

**+Corey Budwine**​ moshiboard?


---
**Anderson Firmino** *May 11, 2016 02:48*

**+Vince Lee**​ ok, but moshi's software stop ever! 😥😥😥


---
**3D Laser** *May 11, 2016 02:51*

The nano board is the newer version board that comes with the k40


---
**3D Laser** *May 11, 2016 02:52*

Nano board will let you use laserdrw and coral draw


---
**Anderson Firmino** *May 11, 2016 06:17*

oK! Its easy to install? what the price? ;)


---
**3D Laser** *May 11, 2016 11:38*

It should be plug and play to switch out a Moshi board for a nano board but I'm not for sure can someone advise 


---
*Imported from [Google+](https://plus.google.com/+AndersonFirmino/posts/8F43J1xNkHq) &mdash; content and formatting may not be reliable*
