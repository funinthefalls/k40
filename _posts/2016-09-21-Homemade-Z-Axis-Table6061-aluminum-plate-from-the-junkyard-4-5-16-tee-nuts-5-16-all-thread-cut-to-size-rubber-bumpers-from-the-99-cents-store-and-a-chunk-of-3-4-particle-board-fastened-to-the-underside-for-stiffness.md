---
layout: post
title: "Homemade Z-Axis Table...6061 aluminum plate from the junkyard, 4 5/16 tee nuts, 5/16 all-thread cut to size, rubber bumpers from the 99 cents store and a chunk of 3/4\" particle board fastened to the underside for stiffness"
date: September 21, 2016 23:20
category: "Modification"
author: "Mike Meyer"
---
Homemade Z-Axis Table...6061 aluminum plate from the junkyard, 4 5/16 tee nuts, 5/16 all-thread cut to size, rubber bumpers from the 99 cents store and a chunk of 3/4" particle board fastened to the underside for stiffness and weight...bandabing-bandaboom.



![images/64dcda23621d64e928d6d9d3184e8318.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/64dcda23621d64e928d6d9d3184e8318.jpeg)
![images/9cf758a3a664f6b72364092787341b17.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9cf758a3a664f6b72364092787341b17.jpeg)
![images/3921d4467422fa10b36742912d53f3bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3921d4467422fa10b36742912d53f3bd.jpeg)

**"Mike Meyer"**

---
---
**Ariel Yahni (UniKpty)** *September 21, 2016 23:22*

Very cool, i like it


---
**Gee Willikers** *September 21, 2016 23:37*

Rivets, counter sunks heads... serious business



And flatter / more range than my $1something dollar lightobject one lol shoot me


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 22, 2016 05:09*

Looks good. Now you need some blades or nails sticking out of it to suspend the material for less backburn on cutting due to reflection.


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/Wbq5VyWVdM1) &mdash; content and formatting may not be reliable*
