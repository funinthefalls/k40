---
layout: post
title: "PC Stick Computer for laser control Asus VivoStick PC TS10 The reason for this particular model was the price ($86.00 delivered from Amazon) and because it comes from a reliable PC manufacturer, it comes with a legitimate"
date: February 04, 2017 15:48
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
PC Stick Computer for laser control



Asus VivoStick PC TS10



The reason for this particular model was the price ($86.00 delivered from Amazon) and because it comes from a reliable PC manufacturer, it comes with a legitimate version of Windows. Windows licensing seems to be a problem on the Chinese clones.



Bottom line is that it works like a champ. It can be used with or without a monitor to be a headless unit which would be nice for the MAC users. Most of the complaints are due to issues with speed and lag. The unit I received had an older version on Win10 installed and needed quite a few updates to be up to date. It would have been quite useless while updating. Now it is snappy and very useful. In fact I am writing this on the unit now!



Downsides? Glad you asked! Stock internal SSD has only 32 Gig and after update and cleanup, leaves about 14 Gig available. You can always attach an external drive for more storage or use cloud storage. Need a monitor that has an HDMI input or video adapter to use standard VGA.



[https://www.asus.com/us/Stick-PCs/VivoStick-PC-TS10/](https://www.asus.com/us/Stick-PCs/VivoStick-PC-TS10/)



A great unbiased review here

[http://www.computershopper.com/g00/desktops/reviews/asus-vivostick-pc-ts10?i10c.referrer=http%3A%2F%2Fwww.computershopper.com%2Fdesktops%2Freviews%2Fasus-vivostick-pc-ts10](http://www.computershopper.com/g00/desktops/reviews/asus-vivostick-pc-ts10?i10c.referrer=http%3A%2F%2Fwww.computershopper.com%2Fdesktops%2Freviews%2Fasus-vivostick-pc-ts10)





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *February 05, 2017 00:28*

Have you installed LW and used it with your K40?




---
**HalfNormal** *February 05, 2017 00:31*

My GRBL k40 is down for rehab so I have not run LW but it ran all the software for the stock M2Nano.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/7suy9aT6hyF) &mdash; content and formatting may not be reliable*
