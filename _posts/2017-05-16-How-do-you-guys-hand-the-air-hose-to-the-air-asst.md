---
layout: post
title: "How do you guys hand the air hose to the air asst?"
date: May 16, 2017 01:16
category: "Air Assist"
author: "Chris Hurley"
---
How do you guys hand the air hose to the air asst? Anyone have pictures to show? 





**"Chris Hurley"**

---
---
**Joe Alexander** *May 16, 2017 04:05*

some use coiled hose and let it hang, others(like me) use cable carriers for air assist and the laser pointer power leads.

![images/a867e775363600280091d25e5902f0cf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a867e775363600280091d25e5902f0cf.jpeg)


---
**Martin Dillon** *May 16, 2017 04:07*

I just made a bracket with a hole in it.  I stubbed the air fitting out the left side of the picture and just use the one piece of tape to keep the hose against the wall.  The hose is a cheap vinyl hose and kind of stiff but move around freely and doesn't get caught on anything.

![images/566222fc1c5c563e19ad9f9d893fb016.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/566222fc1c5c563e19ad9f9d893fb016.jpeg)


---
**Joe Alexander** *May 16, 2017 04:48*

yea that should work. Nice bed btw im kinda jealous, I have a honeycomb but its tha undersized one for the K40's with what i consider an excessive amount of border. like this one:

[ebay.com - Details about  300x200 Honeycomb for K40 CO2 laser machine](http://www.ebay.com/itm/300x200-Honeycomb-for-K40-CO2-laser-machine-/400953559978?hash=item5d5ab1cbaa:g:e5cAAOSwEjFXdU4T)

   I am in the process of building a gantry and side rails to make a larger work area like this:

[http://openbuilds.com/builds/uk40ob.3988/](http://openbuilds.com/builds/uk40ob.3988/)


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/Cfwsu72NvWx) &mdash; content and formatting may not be reliable*
