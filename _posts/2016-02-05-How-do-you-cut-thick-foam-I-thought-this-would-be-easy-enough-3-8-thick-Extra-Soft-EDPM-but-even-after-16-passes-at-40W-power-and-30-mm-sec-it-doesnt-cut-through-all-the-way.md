---
layout: post
title: "How do you cut thick foam? I thought this would be easy enough (3/8 thick \"Extra Soft\" EDPM), but even after 16 passes at 40W power and 30 mm/sec, it doesn't cut through all the way"
date: February 05, 2016 16:00
category: "Materials and settings"
author: "Thor Johnson"
---
How do you cut thick foam?

I thought this would be easy enough (3/8 thick "Extra Soft" EDPM), but even after 16 passes at 40W power and 30 mm/sec, it doesn't cut through all the way.  And there appears to be a "ghost cut" approx. 1/2" below the real cut.



If I decrease the speed, I get more fire (which my air assist promptly blows out), but it doesn't seem to cut any better.  I can pierce it with 3-4 shots of 80%+ power for ~1/2 second (by thumb on test button) -- if you try to just hold the button down for 1 second, it makes a crater, but doesn't cut through.



Is this just something that "can't be done" or what should I try?

Why do I have a ghost beam 1/2" below it for horizontal lines only (and it's not because the cut went all the way through -- the "cut through" portion is roughly 75% of the way from the right -- the left was not cut all the way through, yet you can see the line in the inside going all the way across)?



![images/9b7415b6fc04ce61a8314cb829ed58fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b7415b6fc04ce61a8314cb829ed58fb.jpeg)
![images/5441ba5803109c5e0a79a6edff6ff45b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5441ba5803109c5e0a79a6edff6ff45b.jpeg)

**"Thor Johnson"**

---
---
**Imko Beckhoven van** *February 05, 2016 16:33*

Google laser beam focus.. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 05, 2016 16:57*

Check out regarding your lens orientation. Concave side facing down. Seems a lot of us have had this same issue.


---
**Thor Johnson** *February 05, 2016 19:41*

I'll double check the lens.  Right now it's convex side facing foam, flat side facing laser.  The foam is positioned so that the focus point is on the top of the foam --checked with a 100W LED bulb (I tried it in the middle of the foam thickness, but that worked slightly worse).  Should I raise the foam by 1/8" every pass (that's hard, but maybe I should build a jig)?


---
**Thor Johnson** *February 06, 2016 03:49*

So it was upside down and putting the curve upward helped a lot...  I put the foam back to focal on bottom, but it still takes 3 passes 20 mm/s 40 watts to almost cut through it... And the phantom burn is still there.  Any more tricks?


---
**Imko Beckhoven van** *February 06, 2016 09:17*

The focus of a laser beam ant a straight line. It has a point wich has the sharpest focus. That is why A afjustable z bed is a great upgrade. [http://www.photonics.com/images/Web/Articles/2008/5/1/Forsch_Fig1.jpg](http://www.photonics.com/images/Web/Articles/2008/5/1/Forsch_Fig1.jpg)



Cutting thick material you have to much powerloss because its being out of focus.




---
**Thor Johnson** *February 07, 2016 10:54*

That's why I put the focal length on the bottom: each pass should theoretically be easier than the last because the been is more focused the deeper you go.  I think I'm going to take the material for now and try something thinner.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 07, 2016 12:55*

**+Thor Johnson** I've read that putting the focus should be half way through the material as it will then create like an hourglass kind of shape as it cuts through. e.g. like this

\/

/\



Supposedly makes it easier to cut through things.


---
*Imported from [Google+](https://plus.google.com/100850237464188460837/posts/gu1551BMq5W) &mdash; content and formatting may not be reliable*
