---
layout: post
title: "Just got k40 laser. DOA. Laser test fires"
date: March 22, 2016 17:16
category: "Hardware and Laser settings"
author: "Wayne Herndon"
---
Just got k40 laser. DOA. Laser test fires. laser head moves freely by hand but does not return to home position when machine is turned on. Also, no communication through USB port. Which is more likely to be bad the 6C6879 M2 mainboard or the power supply to the mainboard?





**"Wayne Herndon"**

---
---
**Thor Johnson** *March 22, 2016 17:25*

Probably the mainboard.  My first one did that; I looked, and all of the chips had popped off of it during shipping :(

Forced seller to send me new one. 


---
**Wayne Herndon** *March 22, 2016 17:42*

Thanks for the reply. I just ordered one from BJ East Signs. Seller says he will reimburse me. We will see if he does. If not, I keep issue open with eBay and PP.


---
**Heath Young** *March 24, 2016 03:27*

Mainboard. Mine died the same way.


---
*Imported from [Google+](https://plus.google.com/113849066359765553098/posts/UVm1cJf7Pu1) &mdash; content and formatting may not be reliable*
