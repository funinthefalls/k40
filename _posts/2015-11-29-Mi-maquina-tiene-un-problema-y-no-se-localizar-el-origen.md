---
layout: post
title: "Mi maquina tiene un problema, y no se localizar el origen"
date: November 29, 2015 11:50
category: "Hardware and Laser settings"
author: "Al Tamo"
---
Mi maquina tiene un problema, y no se localizar el origen.



Si pongo la máquina a cortar, pero con el botón del láser apagado, se mueve sin ningún problema, pero si lo conecto me dice miles de veces el pc que la maquina se ha desconectado, alguien sabria decirme que puede ser?





**"Al Tamo"**

---
---
**Stephane Buisson** *November 29, 2015 12:14*

maybe current leakage trough ground. (ground your K40)

or ground loop .

does your USB cable have a ferrite protection ?


---
**Al Tamo** *November 29, 2015 12:17*

Tengo dos cables uno con ferrita y otro sin ferrita, los fallos los tiene con los dos.


---
*Imported from [Google+](https://plus.google.com/112166512335650872249/posts/hQj1ZjxWyCN) &mdash; content and formatting may not be reliable*
