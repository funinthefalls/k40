---
layout: post
title: "Anyone experienced this error on K40 Whisperer?"
date: September 03, 2018 18:03
category: "Software"
author: "Vilius Kraujutis (viliusk)"
---
Anyone experienced this error on K40 Whisperer? 

"The laser cutter stopped responding after sending. data was complete"



It is shown randomly. Seems to be shown more often when I cut. Engraving seems to be fine.



I've done today 15minutes engraving and now cutting that picture from bigger plywood piece is impossible. It stops randomly sometimes after 3s, sometimes after 10s :/



I'm this on Macbook with latest HighSierra OS update.

I tried all 3 versions from [http://www.scorchworks.com/K40whisperer/k40whisperer.html#download](http://www.scorchworks.com/K40whisperer/k40whisperer.html#download) 0.19, 0.20, 0.21 - all versions did not work



I'm having Python 2.7.10 installed via brew.



When I start this app, I'm getting:

objc[6876]: Class FIFinderSyncExtensionHost is implemented in both /System/Library/PrivateFrameworks/FinderKit.framework/Versions/A/FinderKit (0x7fff915d4c90) and /System/Library/PrivateFrameworks/FileProvider.framework/OverrideBundles/FinderSyncCollaborationFileProviderOverride.bundle/Contents/MacOS/FinderSyncCollaborationFileProviderOverride (0x10ab8dcd8). One of the two will be used. Which one is undefined.







![images/2588a0a5c257b3b1b87d604672f79860.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2588a0a5c257b3b1b87d604672f79860.png)



**"Vilius Kraujutis (viliusk)"**

---
---
**Scorch Works** *September 04, 2018 03:00*

That is an indication that the transfer of data between K40 Whisperer is not consistent.  (i.e. there are interuptions in the data transfer.)  As the error states the data was already sent to the laser so the job should finish correctly.  The error ocurred while K40 Whisperer was waiting for the laser to send a message indicating the job was complete.   The error could be due to a bad USB cable.


---
**Vilius Kraujutis (viliusk)** *September 04, 2018 07:14*

I have replaced the cable already :( I'll try to replace the cable again. Any brand from Amazon you could recommend? 


---
**Vilius Kraujutis (viliusk)** *September 04, 2018 07:16*

**+Scorch Works** btw is there a way to see logs and stack traces? I would like to debug and see what was actual problem there? 


---
**Vilius Kraujutis (viliusk)** *September 04, 2018 07:17*

The job did not complete. It's stops in the middle. And that's very annoying, since I can't restart the work and I can't continue cutting. 


---
**Vilius Kraujutis (viliusk)** *September 04, 2018 07:18*

**+Scorch Works** thanks for your response. Very much appreciated. :) 


---
**Scorch Works** *September 04, 2018 10:47*

**+Vilius Kraujutis** You can set DEBUG = true at the beginning of k40_whisperer.py to get additional debug info during a seeion of K40 Whisperer. 


---
**Scorch Works** *September 04, 2018 11:00*

**+Vilius Kraujutis** The message you reported "The laser cutter stopped responding after sending data was complete" can only occur after all of the data has been sent to the laser.  You indicated that you tried a few different versions of K40 Whisperer some of them are not capable of generating the error you reported. Therefore  there must have been different errors.  I have a pretty good idea what they were but the latest version tries to handle the communication errors better.  It will keep trying until you stop the run rather than faulting after a given number of tries.  Please stick with the latest version if you want to work through issues.   It is difficult to figure out what is going on if you are reporting behavior from different versions.


---
**Scorch Works** *September 04, 2018 20:28*

**+Vilius Kraujutis** I don't have a recommendation for a USB cable.  I only know that switching cables has fixed the timeout/disconnect isdue for other people. (I still use the long blue cable that came with my laser.  I have not had any problems with it.)  It is a bit of a mystery as to why some people have problems and others do not.


---
**Scorch Works** *September 04, 2018 20:31*

**+Vilius Kraujutis** I don't have a recommendation for a USB cable.  I only know that switching cables has fixed the timeout/disconnect issue for other people. (I still use the long blue cable that came with my laser.  I have not had any problems with it.)  It is a bit of a mystery as to why some people have problems and others do not.


---
**Tom Traband** *September 06, 2018 23:57*

I've seen odd communication errors due to electrical noise and/or electromagnetic interference in different environments. Try to ensure the computer and the laser cutter share a ground (or earth) connection, such as being plugged into the same outlet or power strip. Check that the cutter chassis is grounded and that the control board is too. Look for other sources of emi (ungrounded motors, buzzing florescent lights).


---
**Vilius Kraujutis (viliusk)** *September 07, 2018 04:56*

We don't have grounded plug in that room :/ I used my MacBook Pro on battery. I had electric noise issues with OX CNC, but I then made a low pass filter and now noise is not a problem. 


---
**Vilius Kraujutis (viliusk)** *September 07, 2018 04:57*

Anyone knows if is there a low pass filters for k40? 


---
**Jamie Richards** *September 08, 2018 19:57*

Definitely want to find a way to ground the unit.  Electrically, it's somewhat more dangerous than a CNC.  Also, make sure your mobile phone isn't near.


---
**Vilius Kraujutis (viliusk)** *September 15, 2018 07:35*

That's what error I received with DEBUG=True. NB, those new lines I've added manually before starting engraving. cc **+Scorch Works**

![images/30389b6bd4c23c215b7cc1afa3fdc1e9.png](https://gitlab.com/funinthefalls/k40/raw/master/images/30389b6bd4c23c215b7cc1afa3fdc1e9.png)


---
**Vilius Kraujutis (viliusk)** *September 15, 2018 07:42*

**+Scorch Works**, here's that line 2741.

![images/248003db7f9bb1aca8a8b6970c422da8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/248003db7f9bb1aca8a8b6970c422da8.png)


---
**Vilius Kraujutis (viliusk)** *September 15, 2018 07:42*

Line 2765:

![images/597f44295ec98b468fd970a63f8e74cb.png](https://gitlab.com/funinthefalls/k40/raw/master/images/597f44295ec98b468fd970a63f8e74cb.png)


---
**Scorch Works** *September 15, 2018 16:25*

Your computer is loosing it's connection to the laser and it stops responding to the computer.  When the laser stops responding to the computer the program can't  fix that.  It sounds like you have an electrical noise problem or electromagnetic interference problem.  I don't think your problem can be fixed by changing the program.  However, I have been wrong before, feel free to try.


---
**Vilius Kraujutis (viliusk)** *September 16, 2018 18:51*

Could it be that there's not enough power from the power source? 



This thought came to my mind when I noticed, that once the error occurs, the laser head goes to home position. :/ Seems like it's doing a reset action. 



Also, I tried to run it slowly on 10% of the power - it worked fine for quite a long time, but it stops once I've increased power to 30%.



But it could be due to increased electrical noise from laser as well. Could it be? What do you think?






---
**Scorch Works** *September 18, 2018 22:13*

Yes, I think it could be a lose of power.  Could be as simple as a loose power wire connection that gets jiggled at higher speeds.


---
*Imported from [Google+](https://plus.google.com/+ViliusKraujutis/posts/4qtpzx6RLRm) &mdash; content and formatting may not be reliable*
