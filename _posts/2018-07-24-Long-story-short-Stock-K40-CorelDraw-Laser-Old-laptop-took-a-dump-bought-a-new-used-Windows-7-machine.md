---
layout: post
title: "Long story short. Stock K40, CorelDraw/Laser. Old laptop took a dump, bought a new (used) Windows 7 machine;"
date: July 24, 2018 21:37
category: "Original software and hardware issues"
author: "Mike Meyer"
---
Long story short. Stock K40, CorelDraw/Laser. Old laptop took a dump, bought a new (used) Windows 7 machine; reinstalled Corel, etc., and now I've run into a problem. I've gone through the match-up process of making sure it's the correct board and model, but the x and y axis basically lock-up at the completion of a job. The laser will cut and etch, but it acts very stiff as if the stepper motors are fighting each other. Once power is removed from the laser, no problem, both the x and y axis move smoothly. Anyone seen this?





**"Mike Meyer"**

---
---
**HalfNormal** *July 25, 2018 02:48*

You have to "unlock" the carriage to be able to move it if you are powered up. This keeps it from loosing position after a run. It has been awhile since I used the software but if I remember correctly, it is in the menu options in the icon in the lower right hand corner.


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/ZNyTRN4ysqq) &mdash; content and formatting may not be reliable*
