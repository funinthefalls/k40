---
layout: post
title: "Has anyone tried to etch a full liquor bottle with their laser?"
date: December 01, 2017 19:56
category: "Materials and settings"
author: "Vanessa Darrey"
---
Has anyone tried to etch a full liquor bottle with their laser? bonus if it was a patron bottle, hoping the flatter area of these bottles will excuse the need for a rotary ....... I'm trying to see if its worth the risk 





**"Vanessa Darrey"**

---
---
**Thor Johnson** *December 01, 2017 22:41*

I've seen pros do it with similar machines (at the Jack Daniels distillery -- looked to be 40-60W units)... You'll need to cut the bottom out and raise the machine to fit the bottle in there at the correct focal distance.



Current best practice is to put a wetted piece of tissue paper over it to make the engraving better (it did on a piece of glass for me - but I haven't tried bottles yet).


---
**Vanessa Darrey** *December 06, 2017 20:31*

interesting, I've never heard of the tissue paper trick, then again i don't know too much lol... thanks for the help!


---
*Imported from [Google+](https://plus.google.com/103818040832480056470/posts/HUf5gqGhfRn) &mdash; content and formatting may not be reliable*
