---
layout: post
title: "Just bought a K40. Unfortunately arrived with a broken Y axis belt, but strangely the Y axis still seems movable"
date: September 26, 2016 16:43
category: "Original software and hardware issues"
author: "Steve Lim"
---
Just bought a K40. Unfortunately arrived with a broken Y axis belt, but strangely the Y axis still seems movable.



Is the belt necessary for the Y axis to function? I can manually move the laser head out into the middle and then when I switch on the machine, it makes its way on both the X & Y axes back into the top left corner without much ado.





This K40 is a little "special" - besides the air assist, red laser tracking, it comes with and a manually adjustable Z axis bed out of the box. a little stiff though, it's a screw drive that is "belt driven" on all four corners.



![images/160366444280e40f5a1bb7c90f5db6d6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/160366444280e40f5a1bb7c90f5db6d6.jpeg)
![images/3d105c5c04e68fcc7744ada1f1888688.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d105c5c04e68fcc7744ada1f1888688.jpeg)
![images/db4f1d4b05f7959bd4ebe6dde7c258ba.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db4f1d4b05f7959bd4ebe6dde7c258ba.jpeg)

**"Steve Lim"**

---
---
**Don Kleinschnitz Jr.** *September 26, 2016 16:57*

Sounds like your K40 is "very special" in that it doesn't need a belt to drive the Y axis ...lol.

My guess is the belt is somehow still around the pulleys enough to move?? Certainly that will not work long term.



Interesting "special items". I notice the second mirror mount also looks different.



I would contact the seller on the belt situation as you likely will have to remove the gantry.


---
**Imnama** *September 26, 2016 17:03*

I have the same machine. Works excellent. You do need to fix that belt. The only disadvantage is that the adjust point for the Z axis is higher than the work surface. So bigger peaces will be lifted by it. I placed a honeycomb on top of this, problem solved.


---
**Ariel Yahni (UniKpty)** *September 26, 2016 17:09*

It will move without the belt but you could have issue when you start looking at fine details since the the head will not move the exact same amount on both sides


---
**Don Kleinschnitz Jr.** *September 26, 2016 17:16*

**+Ariel Yahni** how the heck does Y move without the belt which is connected to the stepper ......I'm lost.


---
**Anthony Bolgar** *September 26, 2016 17:32*

Dual drive belts?


---
**Ariel Yahni (UniKpty)** *September 26, 2016 17:32*

**+Don Kleinschnitz**​ there are 2 belts, one on each side. For it to move the one broken can't be the one attached directly to the stepper. So it move but surely is not moving exactly in sync or at least at high speed


---
**Don Kleinschnitz Jr.** *September 26, 2016 17:51*

**+Ariel Yahni** the belt is not a loop with each end screwed down ...? Never looked underneath yet.


---
**Ariel Yahni (UniKpty)** *September 26, 2016 17:54*

**+Don Kleinschnitz**​ yes one loop on each side. One of the attached to the stepper directly and the other via the extension rod. You can say they are both connected 


---
**Don Kleinschnitz Jr.** *September 26, 2016 17:55*

Looks like this?  .....OMG I found your post in my collections  ...lol

[lh3.googleusercontent.com - lh3.googleusercontent.com/Zq-tw3DzwFlK7AKl_1LgEExQnq94rnXJ4YJ941-XAer_1I6rLk4IPKg0EKAhjna_emfE43nVvcs=w1920-h1080-rw-no](https://lh3.googleusercontent.com/Zq-tw3DzwFlK7AKl_1LgEExQnq94rnXJ4YJ941-XAer_1I6rLk4IPKg0EKAhjna_emfE43nVvcs=w1920-h1080-rw-no)


---
**Alex Krause** *September 26, 2016 18:07*

The screw probibly fell out of the bottom of the where it connects to the gantry the belt has a hole on each end that a screw goes thru


---
**Steve Lim** *September 28, 2016 14:19*

Thanks for all the replies guys. That certainly makes sense, the left side is just a loop and the stepper is driven on the right. Does anybody have a walkthrough on removing the gantry? I gather mine might be a little more complicated because of the adjustable Z bed.


---
**Steve Lim** *September 28, 2016 14:34*

**+Imnama** Great to hear that, i'm looking forward to getting it working. Anything specific to watch out with this particular build? Are you still running it on Corel Laser & the default controller?



The seller gave me a honeycomb bed with the order, I think i'm going to cut out one the honeycomb section above the Z adjustment a little more so that I don't have to remove it to adjust the height.



I noticed that the hex head on mine is pretty stiff and with the included "driver", there is quite a lot of play in the shaft. I guess the hex head is pretty worn out. Does your has the same issue?


---
**Imnama** *September 28, 2016 15:49*

Hi Steve,  I'm still running on Corel and default controller, works fine for me. Yes, the Z axes is very stiff but a drop (well, 4) of oil solved most of that. As normal with all theese k40 like machines, the ventilation system is the first thing to replace. I got one that is used on boats to get rid of fuels vapors. (dutch link: [conrad.nl - windmotor](https://www.conrad.nl/nl/radiaalventilator-12-v-90-mh-75-cm-tmc-037101-12v-570095.html) )


---
**mina saad** *July 21, 2017 09:37*

Hi,  I need to buy an air assist head and air compressor for my k40 to use it for cutting MDF. Can you please recommend the right type.

Thanks


---
**Don Kleinschnitz Jr.** *July 21, 2017 15:32*

**+mina saad** search the community you will find many choices and opinions.


---
*Imported from [Google+](https://plus.google.com/100031757103430769888/posts/R5LNrHjF4Vt) &mdash; content and formatting may not be reliable*
