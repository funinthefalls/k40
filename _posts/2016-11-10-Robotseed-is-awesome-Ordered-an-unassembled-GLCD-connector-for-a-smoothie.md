---
layout: post
title: "Robotseed is awesome. Ordered an unassembled GLCD connector for a smoothie"
date: November 10, 2016 17:39
category: "Smoothieboard Modification"
author: "Bill Keeter"
---
Robotseed is awesome.



Ordered an unassembled GLCD connector for a smoothie. My GLCD has backward plugs. So getting a unassembled smoothie connector seemed to make sense.



Anyway, they sent 2 PCBs. So I could pick white or black. Plus they included an external SD card reader. Which is awesome. As i'll be able to mount it however I want inside the K40 now. Maybe a vertical slot on the controller panel on the K40 for inserting the SD card?

![images/79ea9481f15fc40f99fdbe80acd1dcaa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/79ea9481f15fc40f99fdbe80acd1dcaa.jpeg)



**"Bill Keeter"**

---


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/iKqi7aeR7y1) &mdash; content and formatting may not be reliable*
