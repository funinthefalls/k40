---
layout: post
title: "Is the connector labeled switch a standard molex connector?"
date: May 16, 2016 15:30
category: "Smoothieboard Modification"
author: "Alex Krause"
---
Is the connector labeled switch a standard molex connector? I'm going to buy the female socket to create a harness for the limit switches to connect to smoothie in case I ever want to revert back to the stock board

![images/645fdf5d46be4fbf37d1aa07bd5ca11b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/645fdf5d46be4fbf37d1aa07bd5ca11b.png)



**"Alex Krause"**

---
---
**Jon Bruno** *May 16, 2016 15:54*

lol.. not sure why you'd ever want to do that... Go back that is...


---
**Alex Krause** *May 16, 2016 15:56*

If I decide to buy a 50-60w machine with larger work surface so I can sell the k40 with stock board and drop smoothie into the new machine


---
**Jon Bruno** *May 16, 2016 15:59*

Ahh. Ok .. Yeah, that's a legit reason. If I went bigger I'd probably sell the k40 already modded. But my next question was going to be "when are more smoothies going to be available" I guess the shortage makes your idea more feasible.


---
**Jon Bruno** *May 16, 2016 15:59*

Are the clone boards any good? In a pinch...


---
**Alex Krause** *May 16, 2016 16:00*

4xc is still available last I knew but 3xc sells out just about as soon as they hit the market


---
**Jon Bruno** *May 16, 2016 16:15*

Really? Where are they available? The US vendor shows sold out.


---
**Alex Krause** *May 16, 2016 16:55*

**+Peter van der Walt**​ just checked the stock connector only the middle three pins are used I will need to run a continuity test to see if they are NO or NC limits


---
**Alex Krause** *May 16, 2016 16:57*

No need for a meter contacts for limit switch are visible and are NC contacts


---
**William Klinger** *May 16, 2016 17:50*

The connector labeled Switch is a JST connector. I see many of them advertised with and without wires attached on e-Bay. JST makes a number of different size connectors that look a lot alike. Measure the pin to pin centers to determine the correct pitch.


---
**Jeremie Francois** *May 16, 2016 18:02*

really looks like a JST PH serie to me


---
**Andrew ONeal (Andy-drew)** *May 19, 2016 05:51*

I got the wrong board, I went to buy smoothie board but didn't know which one to get. They had some listed at 150$ and some for allot less. I ended up getting a mks gen1.3 😞, while waiting on board I did some research and now see the differences. Live and learn


---
**Alex Krause** *May 19, 2016 06:01*

Uberclock, Robotseed for genuine smoothie boards depending on region... Panucatt offers Smothie based boards (azteeg)that fall below the price point with a few less features while still retaining opensouce community backing. My next board will be from one of these suppliers 


---
**Andrew ONeal (Andy-drew)** *May 19, 2016 06:01*

Here's the link, I took a break from laser had to do remodel of my cave lol. Then took time learning about CNC and bought three 200oz min23 steppers.





Look at this on eBay [http://www.ebay.com/itm/321689906657](http://www.ebay.com/itm/321689906657)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/jd3jqRwdu1r) &mdash; content and formatting may not be reliable*
