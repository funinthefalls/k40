---
layout: post
title: "Well I had a weekend of pen making, I realise this is for laser questions"
date: November 30, 2015 23:49
category: "Object produced with laser"
author: "Tony Schelts"
---
Well I had a weekend of pen making,  I realise this is for laser questions. So it kind of is.  I posted a pen rack that I designed and wondered if anyone had given it a try.  the question is.  Is there any scope to cut these out and sell them on line.  And any suggestions of design changes. 

![images/ffa21b6080650d7045114752ddcbda30.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ffa21b6080650d7045114752ddcbda30.jpeg)



**"Tony Schelts"**

---
---
**Anthony Coafield** *December 01, 2015 00:02*

Soon as I get my new tube I'll be giving one a shot. Everyone who wants to sell pens should have at least one, so once the design is worked out I'd look at selling some.


---
**Scott Thorne** *December 01, 2015 01:52*

I got the email you sent me but could not get Corel draw to open it.


---
**Tony Schelts** *December 01, 2015 07:27*

do you want different format


---
**Scott Thorne** *December 01, 2015 10:22*

Sure I will try a different format thanks **+Tony Schelts**​.


---
**Larry Baird** *October 18, 2016 17:30*

I would like to try one of these also if you can pass on the file.


---
**Tony Schelts** *October 18, 2016 18:06*

Try this like I told someone before. This was an experiment and for the most part worked. The holes on the bottom should be bigger. I am still using mine.[onedrive.live.com - OneDrive](https://1drv.ms/u/s!Ao37nZ0aQo9MkxvFH0pDGrr6JRX4)


---
**Tony Schelts** *October 18, 2016 18:06*

Try this like I told someone before. This was an experiment and for the most part worked. The holes on the bottom should be bigger. I am still using mine.[onedrive.live.com - OneDrive](https://1drv.ms/u/s!Ao37nZ0aQo9MkxvFH0pDGrr6JRX4)


---
**Tony Schelts** *October 18, 2016 18:28*

Just slightly adjusted it (4mm) [https://1drv.ms/u/s!Ao37nZ0aQo9MnxJjo2Jp3dBayW84](https://1drv.ms/u/s!Ao37nZ0aQo9MnxJjo2Jp3dBayW84)

[onedrive.live.com - OneDrive](https://1drv.ms/u/s!Ao37nZ0aQo9MnxJjo2Jp3dBayW84)


---
**Larry Baird** *October 18, 2016 22:35*

Thank you Tony, I'll give it a try and let you know.




---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/Zh4kwpPGJxj) &mdash; content and formatting may not be reliable*
