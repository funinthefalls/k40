---
layout: post
title: "Hi all, does anyone have experience with the Hailea aquarium chiller or similar, to use it with a laser cutter in a warm environment?"
date: July 11, 2017 18:43
category: "Modification"
author: "Dennis Luinstra"
---
Hi all, does anyone have experience with the Hailea aquarium chiller or similar, to use it with a laser cutter in a warm environment? (we want to use our K40 in Australia, qld) The price is a lot better than the CW 5000 chillers! 



I probably want to buy one of these chillers, and use it with an external 20l Jerrycan and submersible pump, but don't know which type to buy, the 250 watt 1/6 hp or the 150 1/10hp?



[http://www.hailea.com/e-hailea/product1/HC-250A.htm](http://www.hailea.com/e-hailea/product1/HC-250A.htm)





**"Dennis Luinstra"**

---
---
**Anthony Bolgar** *July 11, 2017 19:04*

From the description I think you would be OK with the 150. But if the price difference isn't too great, I would go with the 250 just to be safe. There seems to be quite a few Australian vendors of this product.


---
**Dennis Luinstra** *July 12, 2017 18:29*

Thanks Anthony, the 250 is only 25 more euro's expensive than the 150. Think I'll buy the 250 in a couple of days on Ebay and see how it goes. 


---
**E Elzinga** *September 06, 2017 21:52*

Any experience yet? :)




---
**Dennis Luinstra** *September 07, 2017 18:37*

Yes, the is working great, cools the water in our Jerrycan to the desired temperature and when the temperature rises 1 degree Celsius it chills to the desired temperature again. 


---
**E Elzinga** *September 07, 2017 18:55*

Is the chiller before, after or parallel to the  jerrycan? Piking up a chiller tomorrow so I can finaly use the K40 :)


---
**Dennis Luinstra** *September 07, 2017 18:56*

K40 uses a pump in the Jerrycan and chiller has its own pump in the same Jerrycan 


---
*Imported from [Google+](https://plus.google.com/101821475060654077048/posts/6dm9nMxMzAG) &mdash; content and formatting may not be reliable*
