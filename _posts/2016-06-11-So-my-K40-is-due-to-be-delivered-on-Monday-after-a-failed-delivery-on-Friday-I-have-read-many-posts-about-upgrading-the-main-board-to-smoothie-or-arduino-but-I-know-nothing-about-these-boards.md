---
layout: post
title: "So my K40 is due to be delivered on Monday (after a failed delivery on Friday) I have read many posts about upgrading the main board to smoothie or arduino but I know nothing about these boards"
date: June 11, 2016 14:22
category: "Modification"
author: "Pete Sobye"
---
So my K40 is due to be delivered on Monday (after a failed delivery on Friday) 

I have read many posts about upgrading the main board to smoothie or arduino but I know nothing about these boards. Does anyone have a real idiots step by step guide to which board and how to install / program such a board?

Thanks

Pete





**"Pete Sobye"**

---
---
**Ariel Yahni (UniKpty)** *June 11, 2016 14:43*

**+Pete Sobye**​ welcome and get ready to rumble.  Just kidding. Once you get you machine, take pictures of the current board / Power supply. There are a couple of different version and for an upgrade you need to consider those. 


---
**Anthony Bolgar** *June 11, 2016 15:10*

**+Pete Sobye**  Here is a link to a smoothie upgrade blog.

[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)


---
**Anthony Bolgar** *June 11, 2016 15:11*

**+Pete Sobye** here is one for arduino/Ramps 1.4

[http://3dprintzothar.blogspot.ca/2014/08/40-watt-chinese-co2-laser-upgrade-with.html](http://3dprintzothar.blogspot.ca/2014/08/40-watt-chinese-co2-laser-upgrade-with.html)


---
**Pete Sobye** *June 11, 2016 15:59*

**+Ariel Yahni** **+Anthony Bolgar**

Thank you. When the K40 arrives on Monday I will photograph the board and PSU and post here. 

Just a point to note is that I will not be using the machine for any fancy graphics engraving. I plan to use it to cut 3mm clear acrylic to make display covers for me and my fellow Radio Ham's Morse keys. So merely rectangular shapes which will be bonded together to form the cover. Given this, is an upgrade stil worthy as I expect I could do this in its stock format. However I have read that the software is a bit of a pig to use so having the option to use alternative programs will be a bonus. 


---
**Ariel Yahni (UniKpty)** *June 11, 2016 16:10*

**+Pete Sobye** i think the software is bearable for simple stuff like that. If you know how to draw in ai, inkscape, this will be similar, again for simple stuff


---
**Anthony Bolgar** *June 11, 2016 16:35*

If all you are doing is simple vector cutting I would not bother with the upgrade.


---
**Pete Sobye** *June 11, 2016 17:13*

**+Ariel Yahni** **+Anthony Bolgar** 

Again, thanks. if the main board isn't worth an upgrade (although its only £25) would the air assist head from LO be worth it? I may just do the upgrades anyway as I have about £250 to throw at this to get the best out of it.


---
**Anthony Bolgar** *June 11, 2016 17:14*

Air assist head will help cut deeper and faster. Best upgrade you can do


---
**Ariel Yahni (UniKpty)** *June 11, 2016 17:19*

**+Pete Sobye**​ as **+Anthony Bolgar**​ said air assist would be a very good choice, mind that not only the head is needed but also air supply. Then I would buy the Z table as you would need to focus onto the material if you go beyond 3mm


---
**Pete Sobye** *June 11, 2016 17:31*

**+Anthony Bolgar** **+Ariel Yahni** 

Thanks Ariel. I think, for now I will do the Arduino Upgrade as well as Air assist head. The rest can follow later. Is it possible to obtain an SD card with a tried and tested set up on it?

Also Do I still need to photo the board and PSU if I am doing this upgrade:- [http://3dprintzothar.blogspot.ca/2014/08/40-watt-chinese-co2-laser-upgrade-with.html](http://3dprintzothar.blogspot.ca/2014/08/40-watt-chinese-co2-laser-upgrade-with.html)


---
**Ariel Yahni (UniKpty)** *June 11, 2016 18:04*

Unless you see something different from that post,  you don't. 


---
**Jim Hatch** *June 11, 2016 18:46*

No need to go the Smoothie route for what you're doing. Or even for more complex graphics. The included software (or Corel + the driver to go direct to laser) is fine. I'm only upgrading to streamline my workflow so I can engrave & cut using line colors to designate speed and power. That will let me have a single workflow for my laser and the bigger MakerSpace one I use a lot.


---
**Scott Marshall** *June 11, 2016 21:00*

I'm releasing a solderless Aftermarket Controller installation kit in about 2 weeks.(waiting on boards) 100% plug and play (you still have to work through the software, but that's a prerequsite to any software package).



The "ALLTek Systems K40 controller retrofit Kit" includes 2 logic level shifters and a 3.3v regulator which will be handy if you go with the Arduino route. In addition it includes an upgraded power supply and an interconnect card that plugs into most, if not all K40 variants.



Coming shortly after that will be my "switchable" Retrofit kit which allows you to change between the factory M2Nano controller and any aftermarket controller in seconds, that way, you don't loose your old work when you go to a upgraded controller. Additionally, it allows you t continue using your factory setup while working out the aftermarket hardware and software. It also is a solder-free kit.



Both Kits come with Complete Instructions, Free support and a 100%  satisfied or 100% of your money back guarantee.



While functional, the provided stock K40 M2nano board is far from user friendly, and just getting it running will likely tax your patience. Once you have it figured out, there is a lot you can do with it. That being said, a LOT of people do upgrade. I'm hoping my kits make it feasible for less electronically experienced people do do so easily, as well as make it a whole lot easier for those who are.



Keep me in mind if and when you get there. I'm looking for Beta testers, and will work with you personally to make sure you get it up and running.



Welcome to the K40 Group, and good luck.



PS - You need to hear this BEFORE your K40 arrives:



When you set up your k40, plug the K40 and pump into  switchable outlet strip. That way you won't accidentally start the laser without coolant flow, a common cause of early an instant tube death.


---
**Ariel Yahni (UniKpty)** *June 11, 2016 21:05*

**+Scott Marshall**​ if you need I can beta test


---
**Scott Marshall** *June 11, 2016 21:12*

**+Ariel Yahni** Drop me an email and I'll tell what the scoop is. I was going to wait until I proved the board in my machine to mention it here, but am quite confident it won't need any major revisions. Scott594@aol.com


---
**Jim Hatch** *June 11, 2016 21:13*

**+Scott Marshall**​ put me down for a switchable retrofit kit. I am really stoked someone has gone to the trouble to finish out the bits & pieces needed for the Smoothie conversion !


---
**Scott Marshall** *June 11, 2016 21:24*

**+Jim Hatch** Jim, send me an email, and I'll send you some particulars on the kit. (board shots and lead times). 



I need a few good tester for both kits, patient, as these boards are slow have made, unless you want to pay 10X the going rate.



For Beta testers,



I'm looking for all skill levels, and any format, ie, Smoothie, Arduino Ramps - even homebrew.



Patient individuals that are willing to take their time to report back on problems, positives, and general experience.



My end: I promise if you Beta test my stuff, you will get a price break, and I will work with you personally to make sure you get running.

Right up front, I'm a hardware guy, for software, I'll help if I can, but I'm not an expert. I'm a retired (due to bad health) Engineer and Industrial controls Business owner.


---
**Anthony Bolgar** *June 11, 2016 21:27*

**+Scott Marshall** this would be a good board to test LaserWeb with. It would basically be testing the board to make sure it handles everything correctly, and testing LaserWeb to make sure it can deal with the conversion board OK. Let me know how much you want for one (private message me), and I will see if we still have any funds to get one for testing. I am pretty sure I can find some coin for this board.


---
**Jim Hatch** *June 11, 2016 21:29*

**+Scott Marshall**​ Sent you an email. I've got both the Smoothie 4X from Arthur's latest batch he had and an AZSMZ Mini (version 2.1) I picked up a few months ago when Arthur was out of stock of true Smoothie boards.


---
**Scott Marshall** *June 11, 2016 21:53*

**+Anthony Bolgar** I'm almost done costing all the components out, and will have pricing for you in a day or 2 on the Single Board, and hopefully tenative pricing on the Switchable variant a few days after that.

I can't figure out how to PM in this forum. I'll let you know as soon as I have hard pricing and lead times (my protoype run is getting thin and it's not even here yet)


---
**Anthony Bolgar** *June 12, 2016 00:04*

Sounds good.


---
**Pete Sobye** *June 12, 2016 16:49*

**+Scott Marshall**

I am definitely interested in a plug and play set up so please bear me in mind. Also with beta teating. I am a complete NOOOOOB and have never even touched an arduino/smoothie board let alone played with gone. Heck, I don't even have my K40 yet, it arrives tomorrow!! pete.sobye@icloud.com is my email if you want to get in touch. 


---
**Scott Marshall** *June 12, 2016 22:10*

**+Pete Sobye** Pete, your're exactly the kind of K40 person I built this stuff for.



There's plenty of stuff available for experienced electronics hobbiests and engineers, but they are probably less than 50% of the K40 users out there. Even they often don't want to fool around tracing out all the circuits, ordering all the parts one at a time, as still possibly in making a mistake.



Most people can learn the software by trial and error, but with the electronics, you usually only get one mistake and you end up burning up a couple hundred bucks worth of circuitry.



My assortment of products include complete upgrade "modules" with detailed instructions that can be installed with a screwdriver. No soldering, no schematic reading. (I DO provide all the schematics and technical info - no secrets here).

I will be releasing a product list and pricing in the next couple of weeks.

In mentioning my products to you, I kind of opened the floodgates, I had intended to wait until I had product ready to ship before letting the cat out of the bag, so to speak. The reaction verifies that there is an interest in this concept, which is good, as I have a lot of time and money in designing these products. (and my laser has been apart a couple hundred times - at least it seems like it!)



It will realistically take you a month or more to get the laser dialed in, learn the software, and make the "normal" upgrades.



In front of an aftermarket controller, you'll want an air assist head, new lens and mirrors, and most likely a better exhaust blower.

(My power supplies allow use of cheap and powerful 12V marine bilge blowers,and water pumps). My stuff includes a cheap and easy to install 12v or 120v  fan speed control. There's also combo lighting kit/fan speed controller. They are available now, and will be listed in my official release price list.



Free mods that will help are:

#1,  remove the factory "duct" from the exhaust. This is done by removing the 4 screws around the exhaust outlet, and removing the sheetmetal duct. It serves no useful purpose and restricts the flow to the point of useless. The factory fan actually works ok with this free fix, as long as you don't put a long restrictive discharge tube on it. I used the expandable dryer duct, but galvanized is better, flow wise. If you don't have air assist, you'll have fires with stock blower, even with the duct out.



#2   While you're at it take out the factory "worktable/clamp" and drop in in the nearest scrap metal pile (with the duct).

Some people build screw adjustable  tables, but some screen, expanded metal (Walmart sells "disposable grill toppers" which are just light expanded aluminum mesh and the right size once you trim the edges). and an assortment of wooden shims works well. This goes hand in hand with the air assist head (see below)



#3    Align your mirrors! This is huge, and totally free. You may need to remove the rail assembly to remove the duct, so that's a good time to learn this, as it will knock the adjustment out a bit. It's usually out a bit from the factory.



Not Free, but the best bang for your buck is right here. Saite Cutter on ebay specialize in XY plotter/cutter parts and laser parts. They are reliable, ship fast, and their stuff is 1st rate. See link. Buy their cutter head (you'll need a new lens, the old one is too small), lens and maybe the mirror kit. I installed this set and it transformed the laser. You will need an air compressor or pump. It's worth it. The air assist blows air onto the kerf. It keeps fire from forming and the way it's designed protects the lens from spatter and smoke. That's all terrific, but the real kicker is it has a little thumbscrew on the side that lest you adjust the focus within about 3/8". That means you don't have to fool around adjusting the work height to the Exact height, just get it close, and zero in with the adjustable head. (it's what make the Grill topper/wood block thing practical.



Links you'll want:



Mirror Alignment.

[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



How to install the lens correctly, and in depth optics info.

[http://www.ophiropt.com/](http://www.ophiropt.com/)



Other links:

[http://www.repairfaq.org/sam/laserfaq.htm#faqtoc](http://www.repairfaq.org/sam/laserfaq.htm#faqtoc)



Setup:

[http://richardgrisafi.com/2014/10/10/setup-and-maintenance-of-a-40w-laser-cutter-from-ebay/](http://richardgrisafi.com/2014/10/10/setup-and-maintenance-of-a-40w-laser-cutter-from-ebay/)



Saite Cutter Air Assist head:

[http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050](http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050)



Loaded Head (w/Mirror & Lens)

[http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005](http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005)



Pump: (can't personally vouch, I use shop air, but many people use a pump)

[http://www.ebay.com/itm/HQ-Hailea-Air-Compressor-Pump-Aquarium-Fish-Laser-Engraving-Cutting-80W-80L-Min-/252371628440](http://www.ebay.com/itm/HQ-Hailea-Air-Compressor-Pump-Aquarium-Fish-Laser-Engraving-Cutting-80W-80L-Min-/252371628440)



Mirrors (you want 20mm):

[http://www.ebay.com/itm/3pcs-Si-Reflection-Mirrors-Lens-10600nm-CO2-laser-Cutting-Engraving-Dia-20-25-mm-/262410725203](http://www.ebay.com/itm/3pcs-Si-Reflection-Mirrors-Lens-10600nm-CO2-laser-Cutting-Engraving-Dia-20-25-mm-/262410725203)



Duct (use only fireproof)::

[http://www.ebay.com/itm/6-inch-x-25-ft-Flexible-DUCTING-duct-for-exhaust-fan-blower-/121232851992?hash=item1c3a0a8418](http://www.ebay.com/itm/6-inch-x-25-ft-Flexible-DUCTING-duct-for-exhaust-fan-blower-/121232851992?hash=item1c3a0a8418)

Food:

[http://www.ebay.com/itm/1-8-X-12-X-12-Baltic-Birch-Plywood-Great-for-Laser-Cnc-45-pack-/182133175460?hash=item2a67fbb4a4](http://www.ebay.com/itm/1-8-X-12-X-12-Baltic-Birch-Plywood-Great-for-Laser-Cnc-45-pack-/182133175460?hash=item2a67fbb4a4)



I'll be in touch.

Scott



Have fun!



Scott


---
**Ben Walker** *June 13, 2016 17:57*

**+Scott Marshall** You don't know how much this excites me.  I am a HUGE K40 fan but I am not so keen to soldering and tinkering.  I guess I could get there but I have way too many other things that take up so much time (hello, these things don't pay for themselves (yet!)).  Keep me in mind as well.  I am very interested in what you have to share and don't mind supporting the community.  You have already been so much help as it is.


---
**Pete Sobye** *June 14, 2016 20:18*

**+Scott Marshall** Thanks for that wonderful information Scott. Prior to your post I ordered the air assist head from Light Objects but will definitely look into the alternative mirrors. 

As for the duct, I will take your advice and ditch that too.

I still have the original fan in the back and will look into a different tube.

The Clamp was ditched as soon as I opened the lid.



While writing this I am looking into the Saite head and mirrors. If you say it will transform the cutter, I may get this setup and sell the Light Object one when it arrives.



Thanks again for the advice. Would you be able to send me a direct contact email or are you happy to discuss here?

Pete


---
**Pete Sobye** *June 14, 2016 20:39*

**+Scott Marshall** another quick question Scott. The Saite head has an Mo mirror but the set they sell are SI. Does that matter?


---
**Scott Marshall** *June 14, 2016 20:47*

Get the Moly, they are tougher and last forever.



I bought a set of mirrors (Mo) and lens from Saite not to long ago, maybe they're just out of stock?


---
**Scott Marshall** *June 14, 2016 20:51*

Pete, you'll find some perfectionists that will tell you the Molybdenum Mirrors are inefficient, but the difference is undetectable, and you can clean them with little fear of damage. (it's like 99.6% vs 99.7% difference with the silicon)



That's critical for a K40 as they are hanging out unprotected and DO get dirty. Dirty or scratched mirrors are less efficient than clean ones, so it's a win - win. They do cost a little more up front, but last nearly forever.



Here's the set I bought if memory serves.

On sale!

[http://www.ebay.com/itm/ZnSe-Focal-Focus-Lens-3-MO-Reflective-Mirror-40W-CO2-Laser-Stamp-Engraver-K40/172050707677?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D36860%26meid%3D8c0556590aa34ee9a309c54121d5f029%26pid%3D100005%26rk%3D4%26rkt%3D6%26sd%3D252283989821](http://www.ebay.com/itm/ZnSe-Focal-Focus-Lens-3-MO-Reflective-Mirror-40W-CO2-Laser-Stamp-Engraver-K40/172050707677?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D36860%26meid%3D8c0556590aa34ee9a309c54121d5f029%26pid%3D100005%26rk%3D4%26rkt%3D6%26sd%3D252283989821)





[http://www.ebay.com/itm/Mo-20mm-Reflective-Mirror-18mm-Znse-Lens-Focal-2-CO2-Laser-Stamp-Engraver-K40-/252283989821](http://www.ebay.com/itm/Mo-20mm-Reflective-Mirror-18mm-Znse-Lens-Focal-2-CO2-Laser-Stamp-Engraver-K40-/252283989821)


---
**Jim Hatch** *June 14, 2016 21:38*

**+Scott Marshall**​ spot on! Your links should be done sort of sticky topic all by itself. 



The list if free/cheap mods is good too.  We get hung up with modding these that we forget they're pretty good without necessarily buying any new/replacement parts..






---
**Scott Marshall** *June 14, 2016 21:54*

Several of us have been working to try to set up what amounts to Stickys on here,  and have made some progress, but it's slow going.



There is a couple of places designated, but the 3 of us (Yuusuf, HalfNormal and Myself) just haven had the time to move much into it. I need to look into the mechanics of it so we can send people to it. We're thinking it will be a "reference section".



It seemed like a great idea last winter when I was stuck in and bored.



I guess I fixed that..

I've been so busy with the board development, I haven't had time to get all the stuff together. I've been working so hard I've killed a keyboard, and this one's getting iffy...


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/1bMA5ix9QXV) &mdash; content and formatting may not be reliable*
