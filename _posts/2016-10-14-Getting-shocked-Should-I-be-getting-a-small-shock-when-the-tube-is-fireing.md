---
layout: post
title: "Getting shocked. Should I be getting a small shock when the tube is fireing?"
date: October 14, 2016 14:12
category: "Discussion"
author: "giavonni palombo"
---
Getting shocked. Should I be getting a small shock when the tube is fireing? What I mean is, I had a small cut on a finger and felt a tingling when touching the k40 case. So I tested and It is small, .01volt. IT only happens when the tube fires.  Could this caus my tube problem of the screeching noise, and irregular mA bouncing slightly? When I got the k40 the first thing I did was clean off the paint for the grounding wires. I have checked all my wires are tight, the only thing I think it could be is the brown wire from the end of the tube, but then again I dont know. Any thoughts? 





**"giavonni palombo"**

---
---
**Thor Johnson** *October 14, 2016 14:31*

The K40 grounds the tube to the frame, so if the ground you're plugged in to has resistance ([long wire, corroded terminal, etc]), the chassis will be hot when the tube is firing (why there's a warning in most manuals to connect the extra ground -- as a safety measure).



You probably need to run a separate ground to your machine that goes to a copper water pipe, to a panel, or to a grounding rod.  Or change the wiring to the tube so that the negative terminal goes to the power supply and not the case (that might be dangerous as the wire will have more inductance than the case, and the power supplies are finicky on the K40's.  DO NOT run the wires touching each other (there's 15KV between them, and I'm not convinced the insulation is up to snuff).




---
**Jim Hatch** *October 14, 2016 14:55*

I had exactly the same problem and ran a 10ga wire from the case grounding lug to a copper grounding rod driven into the earth outside my garage.


---
**HalfNormal** *October 14, 2016 15:00*

Others here have said that they have had a bad water pump and it was conducting through the water to the laser.


---
**greg greene** *October 14, 2016 15:08*

These machine absolutely MUST be grounded via the grounding lug.  The preferred method is to a separate ground rod system.  I know that is not possible for all folks - but you have to realise the engineering that went into the electrical paths is - to put it mildly - shoddy at best.  It works only if all connections and components work at 100% - which will not be the case for all time.


---
**Don Kleinschnitz Jr.** *October 14, 2016 17:44*

Between what 2 points are you measuring .01 volts? 

There should be a single grnd wire from laser to the gnd on the laser power supply.

The AC input plug is gnded with a screw to the frame in the back as well as a wire to the power supply. 

Check all these with ohm meter.


---
**Scott Marshall** *October 14, 2016 19:04*

Check your ground wire where it connects to the IEC line cord connector at the rear of the cabinet.



 They used a quick slide connector there and it falls off frequently.



It's easy to pull loose while working in the electrical box and not realize it.

Solder it in place if possible, if not tighten it by crimping slightly with pliers. 



As long as your building is wired properly and the receptacle is in good condition, that's all the grounding needed. If you're getting a shock from the cabinet then something is wrong with the ground wiring either in the K40 or in your building. An outlet tester can be had at most hardware/home centers for about 3 bucks and that will tell the tale on the building grounding. A quick tracing of the ground wire in the k40 will verify it's condition.



The left hand PSU connector(4 conductor) is the mains in.

 Pin1 is the DC return for the Laser tube, and is tied to Pin 2 which is the mains ground. Follow this wire - it will go to the quickslide connector on the mains inlet, and from there will go to the ground post on the rear of the cabinet. It should be obvious where the problem is.



Another way to check is with a meter or test light from the ground pin on the AC plug to bare metal on the cabinet will tell you if there's a problem.



If you get the problem fixed and want to upgrade your grounding system, install a GFCI outlet for the Laser, that way, if there's ever a future problem with your grounding, it will shutdown everything before you are shocked. (They really are that sensitive and have saved a lot of lives.)



Scott


---
**greg greene** *October 14, 2016 19:10*

Yes a GFI is an excellent idea


---
**Thor Johnson** *October 14, 2016 19:15*

But if you're doing a GFI, be sure to do only 1.  If you put 2 in series, both stop working (silly electromagnetics).


---
**giavonni palombo** *October 14, 2016 20:33*

I believe halfnormal is correct. I put one end of the multimeter in the coolant and the other to a ground and I getting 19.5 volts when the pump is running. I also with the pump running and the machine off put one lead of multimeter in the coolant and the other lead to one side of the mA meter and was getting 19.5 volts as well. Now when its running and the tube fires the voltage drops to zero. Thats odd, where is it voltage going? Could this be causing some type of feed back and causing my tube to "screech "?



Now I will answer some of your questions.



   I was put one lead of the meter on the cabinet , blue part, the other lead to an electrical ground.



I did from the start ground the power supply to the case, and the case to the plug. I did check the connections are good.








---
**Scott Marshall** *October 14, 2016 21:49*

At this point, if your chassis is grounded, I'm thinking your screech is a high voltage leak, most probably from your high voltage in at the right side (looking from front) of the tube to the water inlet tube. 



Power off and machine safe, clean well around the Highvoltage connection and the glass between the water line and the HV connection. It's common for the electricity to draw dust which then turns to damp dirt and provides a leakage path along the surface of the glass. Clean it well, and wipe with WD40 or any moisture displacing lube. See if the noise stops.(water voltage should settle down too)



If so, you may want to wipe the area down with solvent to remove the oil, and put down a fresh layer of RTV silicone around the high voltage connection.



Beyond that, there's the possibility of a bad tube arcing internally, but I've never seen one do it this subtle. Usually there's little doubt where the leak is as evidenced by a large and scary arc.



A screech is never good, can also be a bad power supply. Keep at it until you find it. (or it will eventually become obvious and severe)



Be Safe



Scott


---
**giavonni palombo** *October 16, 2016 14:36*

Im going to check out all that you guy suggested. Thank you for your time. Ill let you know whats going on.


---
**giavonni palombo** *October 18, 2016 14:23*

I did get a new water pump on that does not leak voltage into the coolant with the tube not fireing. The laser seems to be more stable in power But now, with one multi tester lead in the coolant and one grounded to my outlet im get 120 - 130 volts only when the tube fires. Has anyone else tested this? Is this normal since there is high voltage running inside the tube. 


---
**greg greene** *October 18, 2016 15:47*

Not Normal - your coolant should neber have an electrical charge - you have a problem with a voltage leak from the tube - or the HV Wire to the coolant path.  If the coolant leaks and is charged and your the path of less resistance to ground ie: one hand touching the metal case of the unit and another part of your body touching a coolant spill - your going to get a shock .  This is why these machines must be grounded


---
**giavonni palombo** *October 19, 2016 14:41*

Machine is grounded, grounding points are cleaned to metal no paint. I have added a ground from the PS case to the laser case from there to the plug at the machine. All my connections from the plug to the braker panel are grounded. All of this is within 8 feet of the grounding rod.  I can not find externally any type of short or arcing. No fogging inside of my tube nothing suggesting a leak of coolant.



What are you using as coolant? 

Have you actually tested any voltage in your coolant? 








---
**greg greene** *October 19, 2016 16:47*

My coolant is distilled water with a touch of alcohol - no voltage detected, I'm grounded directly to a ground rod through copper 12 ga wire,


---
**giavonni palombo** *October 19, 2016 18:39*

I think I got it! Added a wire from the case to my outlet.  So I think the plug on the machine is so so, going to replace soon. 


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/DdXke7EzQCP) &mdash; content and formatting may not be reliable*
