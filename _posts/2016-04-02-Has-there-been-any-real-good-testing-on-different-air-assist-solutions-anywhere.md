---
layout: post
title: "Has there been any real good testing on different air assist solutions anywhere?"
date: April 02, 2016 17:35
category: "Discussion"
author: "HP Persson"
---
Has there been any real good testing on different air assist solutions anywhere?

It´s almost like high PSI is the unwritten law of air assist, but doing some test myself i get other results on different materials.



Anyone know of any writeup or extensive testing on what air assist does to cutting/engraving and is flow or pressure is better on different materials?



I dont beleive high pressure is the key, so i want to learn more :)

Personally i do not do any engraving at all, and only cutting acrylic and i notice for me it´s more about moving the smoke away, not shooting a superman beam of air trough the lens :)



What are your ideas about air assist? hit me up, i want to read and learn from all perspectives!

Sorry for the bad english ;)





**"HP Persson"**

---
---
**Jim Hatch** *April 02, 2016 17:50*

10-15 psi works fine for me on my K40 as well as the bigger 60w laser so I also don't think high pressures are needed.



The objective is to keep the smoke out of the beam path and away from the lens as well as prevent fire when cutting materials that flame under laser power/speed cuts. Without it I get about a 25% effective power reduction in as little as 15 minutes of a plywood cutting project due to the lens getting dirty. With it I don't have to clean the lens after every project anymore.


---
**Scott Marshall** *April 02, 2016 18:28*

Jim, do you have a metering valve after the regulator? I turned mine up to 5 psi once and it was rocking the workpiece. When I was setting it up I had a Dwyer rotameter in it to check flow, and it pegged past 50lpm @ 5psi. That's with an aluminum air assist with a 18mm lens from Saite cutter. The orfice is about .080".



For all materials I've been cutting, mostly wood and acrylic, less is more. <1psi (28mm/Hg) @ 5-10 lpm seems to work best. Just enough to move the smoke away and keep flames from establishing. 



15 psi would blow my workpieces off the table, and there's a good chance of it popping the lens from the mount or blowing the nozzle off. (if the lens has 1/2sq in area force on the lens would be 7.5lbs) Double that on the nozzle.



Air assist actually reduces cutting depth on wood and acrylic, as reported by several manufacturer guides I've read, and my experience backs that up. The reason I use it at all is fire prevvention and lens protection. It helps keep the wood from staining too, but not completely.


---
**Jim Hatch** *April 02, 2016 18:57*

I'll have to check the volume. I have it attached to a large compressor (30 gals) I use in the garage. It can do 12cfm but I figured the 1/4" hose & small brass connector to the LightObjects nozzle was volume limited anyway and pressure is the only thing I pay attention to. 10-15psi takes care of the flames I used to get from plywood.


---
**Scott Marshall** *April 02, 2016 19:07*

The way this sort of air purge is normally done (industrially) is to regulate down to 3-5psi then adjust with a metering valve. Airbrush setups do it too.



Like you say, the system may be restricted and doing the metering, Lightobject may have designed in an internal metering orfice I don't have on mine. (which is a plus for the Lightobject head.)



I tried to find the pressure vs.cut depth article I read, but can't remember which laser company has it. If I find it, I'll put it up.


---
**HP Persson** *April 02, 2016 23:44*

It´s pretty funny why i ask, my doughter age 7 was drying her hair with a hair dryer and was yelling that my air assist pump was too noisy.

"" Daddy, can´t you use this one instead "" and showed me her purple hair dryer, as a joke :P



So i wanted to show her why i needed the air compressor running, i turned it off and ran the same cut again, but this time took her advice and just wedged the hair dryer in the lid blowing towards the are where it´s cutting.



I removed the dryer, opened the lid and was about to show her the quality of the cut...

And it was perfect, actually better edges and less smoke-dirt around the cut.



That got me thinking, if it´s more to the story about air assist rather than having a turbulence in the lens nossle...

(i do not have the standard K40 air nossle, just a piece of metal holding the lens)



Happy to see your replies, fill in with more ideas or knowledge you may have. Always fun to see what others is thinking and have tried ;)


---
**Scott Marshall** *April 03, 2016 03:51*

I think you were curing poor ventilation rather than adding air assist.



 Like I mentioned before, the only advantages I've seen with air assist on the materials I cut is lens protection, fire prevention and a little airwash to keep the smoke from staining. (which is what the purple hairdryer is doing)

 Beyond that, it pulls heat from the cut which reduces effiency.



Looks like your daughter just confirmed my observations.



She may have a futture in lasers!


---
**Stephane Buisson** *April 03, 2016 09:34*

all depend where you mesure the PSI (on compressor), the point is all depend on your output. I can only speak for myself, but low PSI with small outputS (one vertical from the lens and from aside) do work very well.

[https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser](https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser).

the  jobs of air assist is to clear the smoke from lens (prevent dirt on lens and reduce efficiency through smoke) but also boost slightly the combustion on some material.


---
**Scott Marshall** *April 03, 2016 21:42*

Large commercial machines (500-5000w) Typically run high pressure gasses (Oxygen, Nitrogen, Argon, Helium, and often proprietary mixes) at high pressure depending on what they are cutting. I've worked on a few through the years, and watching one cut 1/8" Stainless @ 100 feet per minute is something. The parts are barely warm as they come off, and look like they were cut by an end mill.



I think one place the "high pressure" philosophy is information trickle down from these big industrial machines. They're NOTHING like ours, completely enclosed beamways, water cooled optics, and half million dollar price tags. If you're cutting out airplane parts for Uncle Sam, it's in the budget....



One misconception is that when using Compressed Air or O2, it's not the material combustion that's aided, it's the vapors created by the flash vaporization of the cut material that are "burned", clearing the beam path of optical distortion/deflection. the inert gasses push the cloud of waste out of the beam path, and are selected for thermal properties and deniity as well as cost.


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/Lxeh3gU1To7) &mdash; content and formatting may not be reliable*
