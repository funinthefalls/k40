---
layout: post
title: "Question for the scientists and engineers out there..."
date: June 15, 2017 20:09
category: "Discussion"
author: "Nathan Thomas"
---
Question for the scientists and engineers out there...



I have 2 machines connected to the same bucket of water used for cooling the laser tubes. But one tube chills 2 degrees colder than the other...any idea why this would be?



Maybe the temp gauge is faulty on one of them is my hope...but any thoughts on other causes I could check?





**"Nathan Thomas"**

---
---
**Andy Shilling** *June 15, 2017 20:19*

Two different pumps? Maybe one runs hotter than the other. Most likely the gauge though.


---
**Abe Fouhy** *June 15, 2017 20:24*

There are a lot of variables not seeing your setup, but my money is in sensor tolerance. Each device has a tolerance of what it sends as a signal to be interrupted as temp, ie resistance, voltage or in digital that internally and converted on the chip like 1-wire. Usually the tolerance is around +/-5% so if we do the math 2C/.05 = 40C it is reading. That is in the ballpark of the temp the laser. How do you rectify this?

Create a recirculation loop with a known temp, analog or digital and let's say your thermometer reads 20C, test both your sensors till they read the same to each other or to all three. Or if using an ardunio to calculate yelp you could add in a temp baseline conversion to baseline both to the same number on your guage.



Hope this helps. :)


---
**Don Kleinschnitz Jr.** *June 15, 2017 20:26*

Complex question.....

Differences to consider:

...Delivery paths and tube size and material to each machine.

...One machine using more laser power. 

....laser currents arent the same.

.... differences in contact or measuring interface

.... One machine is in Utah one is in Florida. 



I assumed you are using one pump. But pump differences if not.



What are you measuring them with and where.






---
**Cesar Tolentino** *June 15, 2017 20:49*

Can you switch the temp monitor? And see if this is still true?


---
**Nathan Thomas** *June 15, 2017 22:13*

**+Andy Shilling** yes two different pumps


---
**Nathan Thomas** *June 15, 2017 22:18*

**+Don Kleinschnitz** I'd understand if it happened while they were running, but I'm seeing the difference when they are not running. 

Two different pumps, one for each machine...both in the same 5 gal bucket. Same brand/model pumps for both.

Machines are right next to each other lol

They both have digital temp gauges..so that's why I'm curious. Need to see which one is the accurate meter if any.


---
**Nathan Thomas** *June 15, 2017 22:19*

Will try that as well, thanks!


---
**Don Kleinschnitz Jr.** *June 15, 2017 23:52*

**+Nathan Thomas** were are the sensors? Is 2 degrees a problem?


---
**Jim Fong** *June 16, 2017 02:20*

Probably just calibration.  I have several differently temp probes. The good ones will read within 1/2 degree or less.   Others can be a couple degrees off.  The platinum RTD probe Walh is my most accurate one and I use it to verify others. 



Some meters will have a adjustment pot to calibrate while others are done in software.   Do a ice water bath to calibrate yours if possible.  You can look up how that is done. 


---
**Phillip Conroy** *June 16, 2017 05:22*

Test pump flow,the pump that came with my 50w white and blue lazcutter was only pumping 1 liter per min, all the research I did o  my tube says minimum 2 liters per minute, changed to a 240 volt 150 watt pond pump and added a flow meter ,now at 2.19liters per [min.as - Joshua Turner](http://min.as) a bonus tube now has more power can now  cut 3mm ply at 16 mm a second


---
**Nathan Thomas** *June 16, 2017 09:58*

**+Don Kleinschnitz** not a problem yet just has me irked....and curious. I live in a high humidity area so I have to keep an eye on temps very closely.


---
**Don Kleinschnitz Jr.** *June 16, 2017 14:40*

**+Phillip Conroy** Whoa PAUSE, are you saying you have proven that if you increase flow from 1-2 liters per minute that your power increased?



I wonder what the saturation point for water flow vs tube power is. 



I have been thinking about isolating my coolant system using high capacity 12VDC pump to get the motor out of the bucket, this gives me additional motivation.



Can you point us to the flow meter you are using?






---
**Nathan Thomas** *June 16, 2017 14:44*

**+Don Kleinschnitz** I had the same question as it relates to water flow/power...that would be amazing 😎


---
**Jim Fong** *June 16, 2017 16:28*

**+Don Kleinschnitz** I use a 5l/min flojet pump with 5 gallon bucket of water.  I don't think I saw much if any increase of power on my k40.   It cut the same with the stock pump The actual water temperature makes more of a difference.  Mine starts off around 17c.   I notice the tube starts to lose power if the water is greater than 23-25c.  Ice packs help cool before it reaches that level.  The specific heat of water is pretty high so it absorbs/transfers heat very well.  Someone did a calculation heat transfer for a laser tube, need to find that post to see what the conclusion was. 


---
**Phillip Conroy** *June 16, 2017 18:03*

[http://www.ebay.com.au/itm/NEW-1-5-Flow-Water-Sensor-Meter-Digital-LCD-Display-control-1-120L-min-/142003760983?hash=item2110156f57:g:S7gAAOSwBw5XRNeI](http://www.ebay.com.au/itm/NEW-1-5-Flow-Water-Sensor-Meter-Digital-LCD-Display-control-1-120L-min-/142003760983?hash=item2110156f57:g:S7gAAOSwBw5XRNeI)


---
**Don Kleinschnitz Jr.** *June 16, 2017 19:20*

**+Phillip Conroy** gulp ,,,, not that cheap !


---
**Phillip Conroy** *June 16, 2017 20:43*

Well cheaper than a new tube


---
**Jim Fong** *June 16, 2017 21:11*

**+Phillip Conroy** I ended up using a water flow switch from lightobjects.  I think about $10.  It has saved me a couple of times. Doesn't tell you the flow rate but it's not really needed for most cases. 


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/JrBQXC7tm6d) &mdash; content and formatting may not be reliable*
