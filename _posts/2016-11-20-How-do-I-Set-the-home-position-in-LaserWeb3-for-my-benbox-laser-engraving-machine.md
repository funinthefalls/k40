---
layout: post
title: "How do I Set the home position in LaserWeb3 for my benbox laser engraving machine?"
date: November 20, 2016 21:53
category: "Software"
author: "Jonathan Davis (Leo Lion)"
---
How do I Set the home position in LaserWeb3 for my benbox laser engraving machine?





**"Jonathan Davis (Leo Lion)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2016 22:29*

You need endstop switches. Your machine doesn't have them. 


---
**Jonathan Davis (Leo Lion)** *November 20, 2016 22:35*

**+Ray Kholodovsky** Yep but is it possible to add them via your controller board?


---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2016 22:36*

Yes of course.... Just check the bottom row of white connectors, and there will be a wiring explanation when the instructions are ready. 


---
**Jonathan Davis (Leo Lion)** *November 20, 2016 22:42*

**+Ray Kholodovsky** alrighty and i do see the connectors.


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/NAULTj3pjrj) &mdash; content and formatting may not be reliable*
