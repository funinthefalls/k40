---
layout: post
title: "Having trouble engraving then cutting in same job"
date: April 09, 2016 23:40
category: "Discussion"
author: "Rodney Fox"
---
Having trouble engraving then cutting in same job. I've tried the pixel in the upper left corner. I've tried two layers one for engraving one for cutting and sending them as different task to my machine. It won't automatically start cutting after engrave and when I do start the cut it is way off. Any tips?





**"Rodney Fox"**

---
---
**3D Laser** *April 09, 2016 23:48*

What software are you using 


---
**Jim Hatch** *April 09, 2016 23:55*

It won't do both in the same job (at least my LaserDRW won't). Even my big laser will only do both in one job when the image is all vector based. 



You have to send the layers as separate jobs. If they're not lining up, check the X and Y parameters in the panel that opens up when you send the job to the cutter. They're called "Refer-X" and Y in LaserDRW. Make sure they're the same in both jobs. Also make sure the reference point is the same (top left or center or whatever you did for the first job). The workspace or canvas should be the same fr both as well or you'll get an offset due to where the content actually starts in the drawing.


---
**Rodney Fox** *April 10, 2016 00:07*

It's corel draw. The engrave and cut are on different layers. I do send them as different task. All parameters are the same. I'll double check again and go through everything to make sure. 


---
**Rodney Fox** *April 10, 2016 00:24*

The cut layer and engraving layer have identical setting and parameters. The cut layer won't start immediately after engraving. But i'm doing everything I'm supposed to. I highlight everything for engraving and hit the engrave button and hit add task after checking all settings. Then I make that layer invisible. Make the cut layer visibke. Highlight it. Hit the cut button making sure all parameters are the same. Then hit the starting button then add task. It starts engraving like its supposed to but then stops when engraving is done. 


---
**I Laser** *April 10, 2016 00:41*

Not sure, but usually you add tasks, then click the play button. If you select 'all layers' option when adding tasks it won't prompt between tasks. (thanks to yuusuf for pointing that out).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 00:59*

As **+I Laser** mentions, it will work without prompting between tasks by adding each task individually (with "All Layers" selected instead of "Dialog" & without starting) to the queue then once all are added hitting play.I just did some work earlier & added about 10 tasks, then hit play. Each task just kept running until all were completed. No prompts in between.



When you mention that it stops after the engraving is done, does it prompt you to "Start next task?" or something similar? Or does it just refuse to go to the cut at all?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 01:02*

Oh, adding for the alignment issue when you do start the cut, it being way off... there is an option called "Do Not Back". I will add a couple of screenshots of my settings so you can see what I have mine set to (which works perfectly). I use a 1mm x 1mm square in the top left corner of all layers, to assist with aligning them for separate tasks. Make sure everything is grouped too.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 01:12*

Here is some screenshots of my working settings.

[https://drive.google.com/file/d/0Bzi2h1k_udXwLW8zT3RLcC1Zd1U/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwLW8zT3RLcC1Zd1U/view?usp=sharing)

[https://drive.google.com/file/d/0Bzi2h1k_udXwYUdVc1I5NFY1R2c/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwYUdVc1I5NFY1R2c/view?usp=sharing)

[https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)



Obviously, change the speeds to whatever you need the speed to be at.



One other thing, possibly check that your model number is set correctly for the mainboard. (Unless you have already done that). You can end up with strange happenings when it is not changed from whatever it defaults to.



[https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)


---
**Rodney Fox** *April 10, 2016 02:33*

Model number is set correctly. The only thing I have different is I have dialog selected in both the cut and engrave layer instead of "all layers". I'll switch that and see what happens. Thanks for the great response. 


---
**Tom Spaulding** *April 10, 2016 03:03*

Have you watched this video?


{% include youtubePlayer.html id="MR7967VHKnI" %}
[https://www.youtube.com/watch?v=MR7967VHKnI](https://www.youtube.com/watch?v=MR7967VHKnI)


---
**Rodney Fox** *April 10, 2016 04:55*

Got it figured out. I switched it to "all layers" ran perfect. There was a video I watched on YouTube and the guy selected dialog and it ran good. That's what I was going off of. Anyways thanks for all the help. Great response time. I really appreciate it. 


---
**Tony Sobczak** *April 10, 2016 19:30*

Good info.


---
*Imported from [Google+](https://plus.google.com/102909111821340176897/posts/fp1UsAKTNwm) &mdash; content and formatting may not be reliable*
