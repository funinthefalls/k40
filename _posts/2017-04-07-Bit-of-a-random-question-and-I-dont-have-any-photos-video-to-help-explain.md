---
layout: post
title: "Bit of a random question, and I don't have any photos/video to help explain"
date: April 07, 2017 01:28
category: "Original software and hardware issues"
author: "Craig \u201cInsane Cheese\u201d Antos"
---
Bit of a random question, and I don't have any photos/video to help explain.



Would the y axis skip steps if the belt is misaligned along the gantry? I've been getting slippage/skips when moving at 100mm/s and I've noticed that the belt sits right on the top of the stepper gear and flares out as it spins. 



I can't seem to find a way to lower the belt, or raise the stepper. Swapping it with one I've got here and a toothed pulley isn't a big deal, but I'd rather not realign my mirrors for the 3rd time this week.



Photo is of where the belt sits. If I push it down it just walks back to that position.

![images/454dd1ad897c4ceddc3fbbd44428e1df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/454dd1ad897c4ceddc3fbbd44428e1df.jpeg)



**"Craig \u201cInsane Cheese\u201d Antos"**

---


---
*Imported from [Google+](https://plus.google.com/114851766323577673740/posts/Sm2ouN12FYe) &mdash; content and formatting may not be reliable*
