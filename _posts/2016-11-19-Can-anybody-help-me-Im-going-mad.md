---
layout: post
title: "Can anybody help me I'm going mad"
date: November 19, 2016 12:24
category: "Original software and hardware issues"
author: "Andy Shilling"
---
Can anybody help me I'm going mad. Once again I'm sure it's because I'm using the moshi board ( will be replaced in the new year)  the wife and I are trying to make some wooden tags and wooden trees for Christmas gift ideas but I can't get the system to engrave then cut; any ideas where I'm going wrong?



The tags are made in coraldraw with 1 pixel thick black borders and images are jpegs as new layers.

Am I right in thinking that I need to define,cut and engrave on each layer, if so how do I do this?  



Also can you guys point me in the direction of relevant files I can buy for this because I can't use cad for toffee.





**"Andy Shilling"**

---
---
**Kelly S** *November 19, 2016 18:59*

With CorelLaser you have to add a task.  Set it to engrave first, then make a new layer of what needs to be cut.  Then when you go to do the project start on layer one, set to engrave then click add task.  Go to layer 2 and set it to cut.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/VfT5h7Sr1Pn) &mdash; content and formatting may not be reliable*
