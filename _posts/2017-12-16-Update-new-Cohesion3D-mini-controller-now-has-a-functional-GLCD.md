---
layout: post
title: "Update!: new Cohesion3D mini controller now has a functional GLCD!"
date: December 16, 2017 21:32
category: "Smoothieboard Modification"
author: "David Allen Frantz"
---
Update!: new Cohesion3D mini controller now has a functional GLCD!



![images/ab9c24ba8eb7ccfcd13d79de48b0517e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab9c24ba8eb7ccfcd13d79de48b0517e.jpeg)
![images/05e4e5cb5e5d0407bb919e5d5e70e6d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05e4e5cb5e5d0407bb919e5d5e70e6d7.jpeg)
![images/bb7cf92ca12920f9d7a551693a7db6a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb7cf92ca12920f9d7a551693a7db6a7.jpeg)

**"David Allen Frantz"**

---
---
**Don Kleinschnitz Jr.** *December 16, 2017 21:45*

Looks awesome.


---
**Coherent** *February 20, 2018 20:51*

Looks good! I've still got my C3d board and a GLCD sitting in a box... I need to get it all installed one of these days. 


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/4SfeEXpvgLN) &mdash; content and formatting may not be reliable*
