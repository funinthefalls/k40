---
layout: post
title: "More photons in action...engraving, painting, a little sanding to produce the weathered wood look.."
date: November 25, 2016 22:23
category: "Object produced with laser"
author: "Mike Meyer"
---
More photons in action...engraving, painting, a little sanding to produce the weathered wood look..

![images/cc6616c239a41134e5ef5d11b473f1e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc6616c239a41134e5ef5d11b473f1e9.jpeg)



**"Mike Meyer"**

---
---
**greg greene** *November 25, 2016 22:35*

what wood are you using?


---
**Mike Meyer** *November 25, 2016 22:51*

3mm Russian Birch


---
**David Cook** *November 28, 2016 16:20*

very cool !    nice design 


---
**3D Laser** *December 07, 2016 17:48*

How did you keep the paint from bleeding 


---
**Mike Meyer** *December 07, 2016 18:39*

Seal the wood...I used a thin layer of bondo, sanded it with 320, then laid down my base coat. Other people have used Polyseal and other products, but the objective is to eliminate the capillary action that is typical with raw wood.


---
**greg greene** *December 07, 2016 19:11*

Have you ever tried liquid masking?


---
**Mike Meyer** *December 08, 2016 02:23*

No, but more on this story as it develops. I have some ordered.


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/WyDKaPfJZ6H) &mdash; content and formatting may not be reliable*
