---
layout: post
title: "New to laser engraving, and have been considering getting a K40"
date: June 07, 2016 20:06
category: "Discussion"
author: "Evan Fosmark"
---
New to laser engraving, and have been considering getting a K40.



I'm concerned with the safety aspect. Are there any documents on how to safely operate this device? What's the risk of watching the laser cutter work through the top cover? 



Happy to see such an active community centered around this device.





**"Evan Fosmark"**

---
---
**Jim Hatch** *June 07, 2016 20:22*

There is an instruction manual of sorts that comes with most of them. It's not terribly detailed though. 



That being said, they're pretty safe (after you've got it set up and verified that they didn't forget to attach the ground wire, etc in the box). Operating the laser requires that the machine is turned on and that a pushbutton is pressed to activate the laser. If that's not pushed, no laser beam comes out :-)



Leaving the lid down when running the laser is safe and watching it through the window is okay. The laser wavelength the CO2 laser produces won't penetrate through the plastic window in the lid. Watching it with the lid open may expose you to flashback which can be very very bad so don't look at it if the lid is open (or put an interlock in so you can't operate it with the lid up). Get a pair of laser safe goggles (10600mu wavelength or CO2 laser) - you can get them on Amazon. Wear those whenever you operate the laser with the lid open.


---
**ThantiK** *June 07, 2016 23:38*

Gotta agree with **+Susan Moroney** on this one.  The laser itself isn't the scary part.  It's the 15kV power supply, that if your ground line is faulty, or the laser becomes disconnected, can arc and potentially instantly kill you.



Acrylic is actually opaque to the IR-range laser, so looking through the acrylic window on the top isn't a problem.  However, just for safety sake, glasses when the laser is firing are recommended.  The emitted light that you can see isn't dangerous, but it is a very pinpoint light that can probably cause a dot in your vision for a little while.  The K40 isn't really a good machine to start off with unless you're a hacker/maker.  The software is terrible, the machines should really be considered more of a "laser starter kit" for most people.  If you can put together ikea furniture without directions, you could probably use a K40.



Hopefully this will change when I or **+Peter van der Walt** put together a drop-in electronics replacement.


---
**Evan Fosmark** *June 08, 2016 05:34*

Thanks for all of the comments. I'm feeling better about it now. I'm a tinkerer at heart am not afraid of the hardware hacking that seems to be required to get it in good working order. I just wanted to ensure its something I'd feel safe approaching.



Now to find a good way to set up suitable ventilation from a basement garage. :)﻿


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/4DZax4Qt8WM) &mdash; content and formatting may not be reliable*
