---
layout: post
title: "For those who have used water wetter in their water coolant to decrease bubbles how much did you use?"
date: January 06, 2018 19:19
category: "Modification"
author: "Kevin Lease"
---
For those who have used water wetter in their water coolant to decrease bubbles how much did you use?





**"Kevin Lease"**

---
---
**Don Kleinschnitz Jr.** *January 06, 2018 21:51*

I would NOT use <i>Water Wetter</i> until **+HalfNormal** and I complete some tests.



In my case it raised the water conductivity to an unacceptable level and was difficult to rinse out. It also did not stop the bubbles.



**+HalfNormal**'s experience is different and we do not yet know why.

......

See this post. Updated section on water bubble treatment;

[donsthings.blogspot.com - K40 Coolant](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
**Ned Hill** *January 07, 2018 01:21*

Besides the bubbles that are formed during a pump priming, bubbles can also form from dissolved gases in the water.  These small bubbles tend to nucleate out over a short time frame and once the system reach an equilibrium they aren't usually an issue.  I've personally never need to use a water wetter with distilled water.  



Typically the dissolved gas is CO2 and is more prevalent in tap water which has been pressurized.  Using distilled water should have less dissolved gas.  The only other ways to initially removed dissolved gases is by sparging with He gas or boiling and then protecting with a CO2 scrubber during cool down, or by pulling a vacuum on the water for a length of time, preferably during a membrane filtration.  Sorry, my lab geek came out :)


---
**Don Kleinschnitz Jr.** *January 07, 2018 14:18*

**+Ned Hill** Are you saying that after a flush and refill [with distilled water] your tube does not get a large bubble at the anode and/or cathode?

My bubbles do not come back once I clear them but to clear them I have to lift the entire machine and rotate it until the bubbles flow out the exit tube. My exit tube is pointing horizontally toward the back of the tube so there is no natural exit.



![images/c5155d7e84f622e49686f468fe20111a](https://gitlab.com/funinthefalls/k40/raw/master/images/c5155d7e84f622e49686f468fe20111a)


---
**Ned Hill** *January 07, 2018 14:25*

**+Don Kleinschnitz** After changing the water I do get the initial bubbles where I have to tilt the machine.  I'm not sure even water wetter can eliminate all of it.


---
**Don Kleinschnitz Jr.** *January 07, 2018 14:57*

**+Ned Hill** 

#K40Bubbles



I can verify that in my case the initial bubbles did not clear. If we could stop this then it would be less annoying to change water.



I am now thinking of ways to use valves to close the tubes water loop while changing the water and then bleeding the lines before opening them up after the exchange? This would be used after you once get the bubbles out by tilting.



Then again as I look at this drawing, maybe the simple approach is to have a drain valve in the bucket just above the pumps  water level so that the tank can be drained yet the  cooling loop is never broken. The disadvantage is that not all the water is replaced with a exchange but I think that small amount would not be a problem? I like this idea because it allows the tank to be drained without pulling it out from below the machine. It would also facilitate having a larger reservoir for more cooling capacity.





![images/905e801b5583349af14d4f0903eed1e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/905e801b5583349af14d4f0903eed1e9.jpeg)


---
**HalfNormal** *January 07, 2018 16:34*

Once the bubbles are eliminated, the best way to keep air or bubbles out of the system is to clamp the lines. I was using hemostats to clamp first the outlet and then the inlet tube while the pump was still running. Brass valves would be a better option in the long run. 


---
**Ned Hill** *January 07, 2018 16:43*

When I was in college I worked in a pet store and was also an aquarium hobbyist.  They make valved quick disconnects for things like under the cabinet canister filters so you can clean out the filter without siphoning out the tank.  Like this [amazon.com - Amazon.com : Marineland Magnum Quick Disconnect Double Valve 1/2" 1 pk (PA0586) : Aquarium Filter Accessories : Pet Supplies](https://www.amazon.com/Marineland-Magnum-Disconnect-Double-PA0586/dp/B0002APYNQ)


---
**Ned Hill** *January 07, 2018 16:55*

Hmmm now that I think about it you might still get some air from the disconnect space when reattaching.  Bubble trap right before the tube inlet perhaps?


---
**Don Kleinschnitz Jr.** *January 07, 2018 17:42*

**+Ned Hill** ... that is why I was thinking of  bleeding or never let the loop go dry.


---
**ThantiK** *January 07, 2018 21:04*

If you let the water pump run overnight, and don't have your return tube letting coolant <i>fall</i> back into your reservoir (creating bubbles), then eventually the bubbles will dissolve into the coolant and remove themselves from the tube.  At least, this was my experience.


---
**Don Kleinschnitz Jr.** *January 08, 2018 13:24*

**+ThantiK** I wish mine did that, I let it run for a day long with the return tube under water and the initial bubbles stayed :(.


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/3MdEBvChyyr) &mdash; content and formatting may not be reliable*
