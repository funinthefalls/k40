---
layout: post
title: "I just had a thought pop in my head & I'm wondering if anyone knows..."
date: October 27, 2016 22:03
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
I just had a thought pop in my head & I'm wondering if anyone knows...



When using say 50% max power (or PWM at 50%), does that count as 50% of the tube time usage? (e.g. 1 minute at 50% is equal to 30 seconds at 100%)??



Curious... 





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Jim Hatch** *October 27, 2016 22:39*

No. It's not linear. It's like your car. If your car redlines at 6500 rpm running it at 2000 rpm is pretty easy on it. Running it at 6400 isn't redlining it but it's much harder on the engine.



The gases in the tube get consumed following a logarithmic rate as the power goes up. Running up to about 80% is pretty flat and then gas degradation starts. It goes near vertical as you cross the 95% power level. You can "over rev" it and get more power at 110% for instance but you're burning the tube very fast.



Tubes (standard CO2 ones anyway) can run for hundreds to thousands of hours based on initial construction & usage. You can't do much about the first but you can affect the latter.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2016 02:26*

**+Jim Hatch** Thanks heaps for that information Jim. That makes a lot of sense that it is a logarithmic function rather than a linear function. I tend to run my machine at max of 6mA, so I guess being so low will affect the latter (you mentioned at the end) significantly.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/A1vcJsnrKG9) &mdash; content and formatting may not be reliable*
