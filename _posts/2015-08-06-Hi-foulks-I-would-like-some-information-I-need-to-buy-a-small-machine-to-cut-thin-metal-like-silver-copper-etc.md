---
layout: post
title: "Hi foulks , I would like some information , I need to buy a small machine to cut thin metal , like silver, copper, etc"
date: August 06, 2015 20:17
category: "Discussion"
author: "Ton Borlenghi"
---
Hi foulks , I would like some information , I need to buy a small machine to cut thin metal , like silver, copper, etc. Do you know whicth small machine would do it? I don´t know if this k40 will do it.

Thanks





**"Ton Borlenghi"**

---
---
**ThantiK** *August 07, 2015 00:33*

How thin are we talking?  Like silver leaf (the stuff that's thinner than foil)?  Possibly.  1mm thick stuff?  Don't even entertain the idea.


---
**James McMurray** *August 07, 2015 00:51*

Cutting silver with a laser is tough, when the silver melts it becomes almost  perfectly specular which is a big issue with lasers.  It can be done, but for copper and Sterling I would use a chemical etch to cut out small parts.


---
**Haotian Laser** *August 07, 2015 09:02*

hi, we have small machine to cut thin metal, pls send your request details to my email: amber@htlasercnc.com


---
**Gregory J Smith** *August 16, 2015 00:51*

I haven't had any luck with the K40 cutting any sort of metal - even aluminium foil. Any tips would be appreciated. I was thinking of spraying it black and trying again.


---
**Haotian Laser** *August 17, 2015 01:09*

**+Gregory J Smith** hi , K40 mainly work on non-metal materials and some andoised aluminum. Any question pls send me email: amber@htlasercnc.com


---
*Imported from [Google+](https://plus.google.com/102234207190353621908/posts/NBrZMUCWCZG) &mdash; content and formatting may not be reliable*
