---
layout: post
title: "Finally managed to get Windows partitioned on my mac and all the software installed even though I don't have a CD drive (thanks apple)"
date: September 09, 2016 14:11
category: "Hardware and Laser settings"
author: "Toby Burkill"
---
Finally managed to get Windows partitioned on my mac and all the software installed even though I don't have a CD drive (thanks apple). 



I read an article of tips posted on here about what power/speed and passes were needed for different materials. Now it said 3mm Acrylic was 1 pass with air assist at 10mm/s at about 13ma. Now when I try this it takes me about 5 passes, does anyone recognise this? Is it an issue that I'm doing something wrong or have something misaligned still?



I previously had some issues with things not cutting through at the farthest point away from the laser exit but I realigned the mirrors and it's hitting dead centre now and cutting through now (could still do with some improvement though). 



 





**"Toby Burkill"**

---
---
**Ariel Yahni (UniKpty)** *September 09, 2016 14:25*

First rule of fight club...  I mean laser cutting you must aling the laser beam, you are not getting full power to the material﻿.  Good read [https://plus.google.com/+ArielYahni/posts/ho5JAUMgTmE](https://plus.google.com/+ArielYahni/posts/ho5JAUMgTmE)


---
**Toby Burkill** *September 09, 2016 14:26*

**+Ariel Yahni** I've read when people mention aligning the actual tube but have no idea how you do that. 


---
**Ariel Yahni (UniKpty)** *September 09, 2016 14:28*

^^^^^^ look at that link. Very easy to follow, and if you master it, heaven will be at your fingertips


---
**Toby Burkill** *September 09, 2016 14:29*

**+Ariel Yahni** ah sorry didn't see the link. Amazing thank you! I know how I'm spending my Friday night… haha


---
**Ariel Yahni (UniKpty)** *September 09, 2016 14:39*

You should at least do 1 pass at 10mA -  10mms or better. But don't tread yourself bad if you don't, practice makes masters.


---
**Jim Hatch** *September 09, 2016 16:37*

2nd rule of fight club is the lens must be convex (bump) side up. 3rd rule is you must be 50.8mm from the lens to the material.


---
*Imported from [Google+](https://plus.google.com/+TobyBurkill/posts/EHovTZHwmDE) &mdash; content and formatting may not be reliable*
