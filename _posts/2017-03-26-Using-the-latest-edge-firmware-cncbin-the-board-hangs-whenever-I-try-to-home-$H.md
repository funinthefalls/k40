---
layout: post
title: "Using the latest edge firmware-cnc.bin the board hangs whenever I try to home ($H)"
date: March 26, 2017 18:44
category: "Smoothieboard Modification"
author: "Mircea Russu"
---
Using the latest edge firmware-cnc.bin the board hangs whenever I try to home ($H). Using the normal firmware.bin works ok for homing (G28). Any ideas?





**"Mircea Russu"**

---
---
**Mircea Russu** *March 27, 2017 05:29*

**+Peter van der Walt**​ every time I issue G28.2 or $H which should do the same the board freezes instantly. No movement at all. The config works for normal firmware so endstops should be configured ok.


---
**Mircea Russu** *March 27, 2017 05:32*

Only xy. Z disabled. Leds stop blinking, that's why I said it hangs


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/4GLinMMaf9J) &mdash; content and formatting may not be reliable*
