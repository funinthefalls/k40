---
layout: post
title: "4 Space Invaders on ~23mm2 stainless capped tiles"
date: July 13, 2016 13:42
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
4 Space Invaders on ~23mm2 stainless capped tiles. Done using Dry Moly, 10mA @ 150mm/s @ 5 pixel steps. For a mosaic artwork a guy I know is working on.



What settings are others using for their dry moly attempts? I'm not 100% happy with the results of this as the moly seems to have not stained as dark as I would like. I imagine more power or slower speed or 1 pixel step may fix that.



Also, I noticed a strange little zigzag on all the edges. I'm not sure if it is visible in the photos, but I have a feeling it's because my aquarium pumps are vibrating the entire table slightly. Might be why I've noticed a difference in cutting/engraving ability since I started using 2 pumps instead of 1 (not hitting exact same spot every time).



![images/7de698b21689ffec478a4fa5e6b93bca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7de698b21689ffec478a4fa5e6b93bca.jpeg)
![images/afd08f1bf17415f1b22cf49db69d4b04.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afd08f1bf17415f1b22cf49db69d4b04.jpeg)
![images/7b46da566514925bfb481968b05d559e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b46da566514925bfb481968b05d559e.jpeg)
![images/0fa417217f0040e50e402a5a7854b825.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0fa417217f0040e50e402a5a7854b825.jpeg)
![images/1e5165e71150c9c31992ad4a18075174.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e5165e71150c9c31992ad4a18075174.jpeg)
![images/426c9ae455e0529ec91be6ea4dda1b67.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/426c9ae455e0529ec91be6ea4dda1b67.jpeg)
![images/c005a1e2356ecaa1677f039169062f8e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c005a1e2356ecaa1677f039169062f8e.jpeg)
![images/a3a990765accbec2c54a61ba4c8c9ea1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3a990765accbec2c54a61ba4c8c9ea1.jpeg)
![images/f3609c0abe41ef31a6dd4f3e301dddd7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3609c0abe41ef31a6dd4f3e301dddd7.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Corey Renner** *July 13, 2016 16:46*

I love that.  I wonder if my wife would let me do the stove backsplash in Space Invaders...


---
**Ned Hill** *July 13, 2016 18:46*

Could the zigzagged edges be aliasing due to the larger steps?


---
**Ned Hill** *July 13, 2016 18:50*

It might acutally be due to the vibration as I see what looks like a moire pattern in the background.   Like I had talked about previously as your engrave rate is at a 3x multiple of your 50Hz power line frequency.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 13, 2016 21:47*

**+Ned Hill** It is possible that it is related to the 5 pixel steps, although I never noticed it on wood, but that could be because wood burns whilst the stainless just marked. The moire pattern in the background is possibly the stainless capping on the tile, as it looked slightly like it had been buffed with an orbital sander or something to begin with (some circular pattern in the metal). I'm unsure whether we use 50Hz or 60Hz powerline frequency here in Australia (can't remember), but you could be right. It could also be because I don't have my bed bolted in place, it's just resting on the legs lol. I will have to sort out bolting it back in & give it a try again.


---
**Ned Hill** *July 13, 2016 21:56*

**+Yuusuf Sallahuddin** At least according wikipedia Australia uses 50hz. :-)


---
**I Laser** *July 14, 2016 02:13*

Ha, they're cute! Brings back memories, numerous hours wasted on the Atari 2600 and table top machines in the 80's.



What 'dry moly' product did you end up settling on? Last time I checked in I noticed you were playing with a few!


---
**Corey Renner** *July 14, 2016 03:46*

Not a waste, watch PIXELS, could come in handy some day.



c


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 14, 2016 07:19*

**+I Laser** I decided on a product by a company called Minehan. They had a local warehouse nearby that stocks supplies for some of their more wholesale customers. They service a lot of the mining industry in general. The product is called Circle Spray. It's (if I recall correct) 84% MoS2 compared with a weak <10% for CRC etc that I could find online.



**+Corey Renner** I would say that it will definitely come in handy if we are ever invaded by giant pixelated enemies haha.


---
**I Laser** *July 14, 2016 07:54*

Thanks **+Yuusuf Sallahuddin**, so the MoS2 is the important part. Don't think Minehan have any distributors in Victoria.



How much is a can? I'll see whether they'd be prepared to send me some in the post.



hmmm aerosol can via post. :| Trying to find another spray that would be suitable but there aren't many that specifically state what the mos2 content is.



Giving up for the night, a few hours reading tech data sheets is more than I can handle lol!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 14, 2016 13:36*

**+I Laser** The Minehan one didn't state the MoS2 content either, although I managed to find the content of the few different ones I looked into (CRC, Minehan, etc) via their MSDS. I think provided you have a content of MoS2 around the same as the CRC Dry Moly, then it would be good to go.



The can of the Minehan stuff was supposed to be $25 odd, but the bloke just did it for an even $20 since they don't usually sell direct to customers.



Check around with local mechanics possibly, as from my understanding dry moly is used in engines/mechanical components where wet lubricant is unusable. They may be able to point you to a supply locally.


---
**I Laser** *July 15, 2016 02:52*

Thanks again **+Yuusuf Sallahuddin**, I assumed the MoS2 content in CRC Dry Moly was too low hence you using circle spray. I'll give Minehan a call and see if they have any stockists down south. 



Failing that I'll start searching again, found a couple that have between 10-30% MoS2 which I believe is the same as CRC Dry Moly, though well below what you're saying is in circle spray and they are more expensive too! :\


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 15, 2016 07:32*

**+I Laser** The Circle Spray was the best I could find, although it seems others have great results with the CRC Dry Moly, so whatever the % is in that is obviously enough. I just couldn't find the CRC anywhere nearby & I hate waiting for things to arrive from online ordering (too impatient).


---
**I Laser** *July 15, 2016 07:41*

Ah lmao, a man after my own heart... When I want something, I want it yesterday!



Though truth be told I didn't get a chance to call minehan today as I was too busy. I tried their online contact us last night but it's broke (probably getting too much spam!).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 15, 2016 07:46*

**+I Laser** When I contacted them I tried their online first & it was broken, so I rang (had to leave voicemail) & sms'd one of their numbers that was for the local outlet. I believe it was Nathan from the Stapylton store. He got back to me about 2 hours later. Stapylton store is probably the best to work with since it would be closest to you anyway. If we can post it in the mail & you have issues with them I'd be happy to assist from my end (since they're only about 20 mins up the road).


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/AEbh6SofiRW) &mdash; content and formatting may not be reliable*
