---
layout: post
title: "Anyone got a smoothie config to share?"
date: June 16, 2016 18:05
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Anyone got a smoothie config to share?  Mostly I'd just like to verify the steps per mm for the X and Y axes, and check if the stock laser module from the K40 guide will work or I need to change any values.





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Stephane Buisson** *June 16, 2016 21:07*

easy to calculate, prepare yourself a file to cut with some lines like 5, 10 15 cm long in X and in Y. cut it, then with a caliper mesure it, and compare the result with what you expect. with alpha=expected/real, recalculate the new step/mm with old one x alpha. for me the result was  157.575 as state in K40+Smoothie=SmoothK40 link.


---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2016 21:16*

**+Ariel Yahni**  already told me 157.5 and that seems to be accurate-ish, I can fine tune this later. Thanks **+Stephane Buisson**






---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/DvXjaKfj2Wn) &mdash; content and formatting may not be reliable*
