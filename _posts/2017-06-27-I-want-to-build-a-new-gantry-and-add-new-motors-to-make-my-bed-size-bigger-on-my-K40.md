---
layout: post
title: "I want to build a new gantry and add new motors to make my bed size bigger on my K40"
date: June 27, 2017 01:33
category: "Discussion"
author: "Robert Selvey"
---
I want to build a new gantry and add new motors to make my bed size bigger on my K40. Can I use a smoothie board for any size gantry?





**"Robert Selvey"**

---
---
**Alex Krause** *June 27, 2017 01:38*

Yes


---
**Alex Krause** *June 27, 2017 01:38*

Just change it in your config file


---
**Robert Selvey** *June 28, 2017 15:09*

Where  is the config file located?


---
**Alex Krause** *June 30, 2017 02:04*

On your SD card when the board is plugged into your computer you can access it in the file manager


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/9KcrjTWxm6p) &mdash; content and formatting may not be reliable*
