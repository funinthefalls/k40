---
layout: post
title: "could someone tell me if I can hookup a water pressure / sensor to a control board in side of a k40 laser cutting engraving machine the control card says that you can used to control exhaust fan, water pumps and other"
date: March 27, 2018 07:00
category: "External links&#x3a; Blog, forum, etc"
author: "Dale Capen"
---
could someone tell me if I can hookup a water pressure / sensor to a control board  in side of a  k40 laser cutting engraving machine the control card says that you can used to control exhaust fan, water pumps and other external devices here is the picture of the card I am talking about it uses a 2 pin small like portable phone battery plug I thank you for your help

![images/5105aa35fa3411a53e7f4f2115c20455.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5105aa35fa3411a53e7f4f2115c20455.jpeg)



**"Dale Capen"**

---
---
**Don Kleinschnitz Jr.** *March 27, 2018 12:55*

I have seen this diagram suggesting that to to there is a control port as you suggest.

I do not recall anyone using it yet but expect someone will post if they have. 



<b>There are two issues to solve:</b>

1. What does the output electronics look like. This could be found by tracing the circuit backward from the connector.



2. When does the output get asserted by the firmware. This could be found by connecting an indicator on the output and running the machine through it's states. When we know #1 we can determine what kind of indicator to use.



I can solve #1 when I get home tomorrow but #2 needs a machine with a stock card. Mine is converted. 




---
**Dale Capen** *March 28, 2018 05:22*

**+Don Kleinschnitz** **+Don Kleinschnitz** 

[https://plus.google.com/photos/...](https://profiles.google.com/photos/110408102287186991812/albums/6537861765091632833/6537861764524731522)


---
**Dale Capen** *March 28, 2018 05:23*

**+Dale Capen** 

[https://plus.google.com/photos/...](https://profiles.google.com/photos/110408102287186991812/albums/6537861886863908177/6537861888031772002)


---
**Dale Capen** *March 28, 2018 05:23*

**+Dale Capen** 

[https://plus.google.com/photos/...](https://profiles.google.com/photos/110408102287186991812/albums/6537862070433666961/6537862067931750850)


---
**Dale Capen** *March 28, 2018 05:25*

**+Dale Capen** **+Dale Capen** **+Dale Capen** **+Dale Capen****+Dale Capen****+Dale Capen** 

[https://plus.google.com/photos/...](https://profiles.google.com/photos/110408102287186991812/albums/6537862440285755617/6537862441348868274)


---
**Dale Capen** *March 28, 2018 05:25*

**+Dale Capen** 

[https://plus.google.com/photos/...](https://profiles.google.com/photos/110408102287186991812/albums/6537862537623128513/6537862535580151186)


---
**Dale Capen** *March 28, 2018 05:27*

what is the switch machine switch


---
**Don Kleinschnitz Jr.** *March 28, 2018 17:18*

I don't have that switch I think it's the main power switch.


---
**Don Kleinschnitz Jr.** *March 28, 2018 21:37*

Lots of pictures. What are you looking for help on?




---
**Dale Capen** *March 30, 2018 19:04*

**+Don Kleinschnitz** I thought if you saw the pictures you would be able to see if you thought I could plug the water pressure sensor on to the laser card  the first picture


---
**Don Kleinschnitz Jr.** *March 30, 2018 22:27*

**+Dale Capen** Not sure I am clear on what you are wanting to connect to what. 



A water flow sensor typically connects to the LPS not the controller. 



Search the forum for this hookup and there is lots of info here:

[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)




---
*Imported from [Google+](https://plus.google.com/110408102287186991812/posts/gvraVfD2i8S) &mdash; content and formatting may not be reliable*
