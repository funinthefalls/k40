---
layout: post
title: "Hi Folks I hope someone can help me"
date: May 11, 2015 12:34
category: "Hardware and Laser settings"
author: "Lisa Samtani"
---
Hi Folks I hope someone can help me. I have one of these machines and have been running it fairly successfully over the last 4 months or so. I say fairly successfully because I notice it stops communicating with the PC every now and then.



However for the last few days I cant get it to connect at all. I get an error saying the "USB device is not recognized" This is a constant error. It happens on any computer I try to connect the laser to.



Does anyone have a fix for this ??



I've tried all the usual fixes to try to uninstall the USB devices etc But the problem is not the Computers it's definitely on the laser side





**"Lisa Samtani"**

---
---
**Sean Cherven** *May 11, 2015 12:38*

Oh boy, that's not good. That specific error is often not fixable, and when it is, it's not an easy fix.


---
**Lisa Samtani** *May 11, 2015 12:47*

do you know where I might start Sean.

It's been driving me crazy for 2 or 3 days now


---
**Sean Cherven** *May 11, 2015 12:51*

I wish I knew. Maybe somebody else might know


---
**Jim Root** *May 11, 2015 22:44*

My guess would be either bad USB cable or bad board. But it is just that "My Guess".


---
**Stuart Middleton** *May 12, 2015 10:20*

First check another USB port on your PC, or even an other PC. Then change the cable. Then check the connector on the controller board. It is not secured other than with solder so can come loose if the cable is stressed. If all look good and it still won't work, you may need to take the controller board out and look for any signs of solder problems (dry joints, non-soldered joints etc.)

Failing that, a new controller board I guess! :(


---
*Imported from [Google+](https://plus.google.com/107608914773085359690/posts/23kQqrwg7in) &mdash; content and formatting may not be reliable*
