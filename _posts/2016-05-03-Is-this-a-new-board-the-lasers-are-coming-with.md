---
layout: post
title: "Is this a new board the lasers are coming with?"
date: May 03, 2016 21:36
category: "Discussion"
author: "Custom Creations"
---
Is this a new board the lasers are coming with? Mine was delivered today and also has nema 17 motors instead of the funky ones some have shown.



![images/d7c7a96714febef1d65dd0ea0c1c83e3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d7c7a96714febef1d65dd0ea0c1c83e3.jpeg)
![images/f43e1bdab1e5e8a440b15b071c4684f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f43e1bdab1e5e8a440b15b071c4684f2.jpeg)

**"Custom Creations"**

---
---
**Carl Fisher** *May 10, 2016 21:26*

That's what mine has as well and I just received mine last week.


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/cE55RYggLbe) &mdash; content and formatting may not be reliable*
