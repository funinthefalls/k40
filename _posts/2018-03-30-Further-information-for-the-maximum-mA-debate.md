---
layout: post
title: "Further information for the maximum mA debate"
date: March 30, 2018 03:22
category: "Discussion"
author: "Anthony Bolgar"
---
Further information for the maximum mA debate.



Quality tubes can run at high mA. Here are the specs on a Sinjoe 80W tube:

TECHNICAL SPECIFICATIONS



    Power: 80-90 watts

    Operating Current: 26-29 mA

    Max Current: 30 mA

    LifeSpan: 10,000 hours at <28mA

    Diameter: 80mm (3.2 inches)

    Length: 12400mm (49 inches)

    Humidity 10-60 percent

    Warranty: 12 months





Notice that the lifespan is 10,000 hours at 27 and under mA (Which is running at 90% power or less)



And for a Reci 80W tube:



TECHNICAL SPECIFICATIONS



    Power: rated 80 watts; Maximum 90 watts

    Operating Current: 25-27 mA

    Max Current: 30 mA

    LifeSpan: 10,000 hours at <28mA

    Diameter: 80mm (3.2 inches)

    Length: 1250mm (49 inches)

    Humidity 10-60 percent

    Warranty: 12 months



Notice you can run the Reci tube at 100% power and still have an estimated 10,000 hours lifespan. This tube is on sale at [http://laser-depot-usa.com/reci-w1-80w-1200mm-laser-tube-free-shipping](http://laser-depot-usa.com/reci-w1-80w-1200mm-laser-tube-free-shipping)  for $450.00 (Use coupon code 50off). Shipping is free. 



Cost per hour to replace the tube is:

4.5 cents per hour.



Think how much more money you can be making running at higher mA, therefor faster cutting speeds and less passes.



So why would you want to max your power out at 8mA???????????????

 







**"Anthony Bolgar"**

---
---
**HalfNormal** *March 30, 2018 13:05*

Not all tubes are created equal! (obvious statement)

I buy cheap when I know I will not be using it a long time. That is why I started with a K40 and then upgraded to a better laser. I will be upgrading the tube to a RECI when the one in my new one fails. I just had to justify the cost.


---
**Jim Hatch** *March 30, 2018 13:46*

RECI tubes are nice and they provide good reliable data on the tube. I wish other tubes did the same.



Cheap Chinese tubes won't give you that data and the info they do give is suspect. Even the wattage of the tube is almost always overstated - might be what the tube can deliver when overpowered but not what you'd see when you're using it in real life with an eye to keeping it running for more than a few hours.


---
**Anthony Bolgar** *March 30, 2018 13:48*

Cloudray and Sinjoi are pretty reliable as well. FYI, everything is on for 50% off at laser depot usa right now (Prices appear artificially inflated though) After the discount the price is actually pretty good, with free shipping. They have some nice RECI tubes available right now.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/4tU6TCWkcdp) &mdash; content and formatting may not be reliable*
