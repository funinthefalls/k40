---
layout: post
title: "I am using the software which came shipped with my laser (CorelDraw12 and CorelLaser) I have a pattern I want to cut that is saved in .cdr format"
date: November 30, 2017 14:14
category: "Original software and hardware issues"
author: "timb12957"
---
I am using the software which came shipped with my laser (CorelDraw12 and  CorelLaser)   I have a pattern I want to cut that is saved in .cdr format. When I open the file, there are objects in three different colors. Black which is meant for the cutouts, and Red and Blue which are meant for engraving. Can someone walk me through the process to engrave only the Red or Blue objects? I hope it does not require going thru and deleting all the Black objects manually, because they are rather intricately mixed in with the Blue objects. I have attached a link to the file in question. Thanks





**"timb12957"**

---
---
**Chris Hurley** *November 30, 2017 14:30*

I've found breaking it up into at least 3 different actions works best. So yeah what you are thinking about deleting the different colours is what I would do. 


---
**William Heston** *November 30, 2017 15:24*

K40 Whisperer will allow you to burn one color at a time; in fact, I don't think it'll let you do it any other way.


---
**timb12957** *November 30, 2017 16:32*

I will look into Whisperer, thanks


---
**Ned Hill** *December 01, 2017 14:35*

The blue, red and black are in separate layers in that file. At the top line for each layer,  in the object manager,  there are some icons.  You will need to deselect the print icon for some layers for them to be able to go to the laser.    After enabling print, just select all the objects in the layer.

![images/c0e3d7a7c5f3f0e1299e835004bbcc7e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c0e3d7a7c5f3f0e1299e835004bbcc7e.png)


---
**timb12957** *December 01, 2017 16:35*

The knowledge base here in this forum is awesome! Thanks guys!


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/8cM3a4MbZWb) &mdash; content and formatting may not be reliable*
