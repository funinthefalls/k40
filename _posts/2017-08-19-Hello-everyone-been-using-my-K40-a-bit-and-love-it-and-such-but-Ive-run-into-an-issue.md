---
layout: post
title: "Hello everyone, been using my K40 a bit and love it and such but I've run into an issue"
date: August 19, 2017 20:28
category: "Discussion"
author: "Eward Somers"
---
Hello everyone, been using my K40 a bit and love it and such but I've run into an issue.



I've moved apartments to the ground floor with no accessible windows to hang the exhaust out of.



I recall reading that some people used the K40 indoors with co2 filters.



Anyone that has experience or knowledge on this that can help me out?





**"Eward Somers"**

---
---
**Tadas Aš** *August 19, 2017 20:50*

I am interested too so leaving this comment for seeing further replies


---
**Paul de Groot** *August 20, 2017 05:39*

Hi **+Eward Somers**​ i managed to buy a inhouse airfilters. It's basically a box with a vacuum cleaner and a charcoal filter. So far it works really well except it's noisy.😀


---
**Eward Somers** *August 20, 2017 06:20*

**+Paul de Groot** can you link me one? :)


---
**Paul de Groot** *August 20, 2017 12:26*

[ebay.com.au - Details about  High Energy Efficient Fish Tank Aquarium Oxygen Air Pump With Magnetism 110-240](http://r.ebay.com/T097TX) Is the airpump that I use for airassist. I could not find a link for the trotec filter unit. So will try to google a bit more




---
**Paul de Groot** *August 20, 2017 12:29*

[https://www.google.com.au/search?client=safari&hl=en-au&biw=1366&bih=928&tbm=isch&sa=1&ei=FYCZWYWGBYH28QXm9amQCA&q=trotec+charcoal+laser+filter&oq=trotec+charcoal+laser+filter&gs_l=mobile-gws-img.3...54280.65802.0.66574.31.30.1.0.0.0.499.9455.2-12j13j4.29.0....0...1.1.64.mobile-gws-img..6.7.2530...0j35i39k1j0i30k1j0i8i30k1j0i24k1j30i10k1.V7M8IiYJYMg#imgrc=Z1fgsj2v9gRNTM](https://www.google.com.au/search?client=safari&hl=en-au&biw=1366&bih=928&tbm=isch&sa=1&ei=FYCZWYWGBYH28QXm9amQCA&q=trotec+charcoal+laser+filter&oq=trotec+charcoal+laser+filter&gs_l=mobile-gws-img.3...54280.65802.0.66574.31.30.1.0.0.0.499.9455.2-12j13j4.29.0....0...1.1.64.mobile-gws-img..6.7.2530...0j35i39k1j0i30k1j0i8i30k1j0i24k1j30i10k1.V7M8IiYJYMg#imgrc=Z1fgsj2v9gRNTM):





[google.com.au - trotec charcoal laser filter - Google Search](https://www.google.com.au/search?client=safari&hl=en-au&biw=1366&bih=928&tbm=isch&sa=1&ei=FYCZWYWGBYH28QXm9amQCA&q=trotec+charcoal+laser+filter&oq=trotec+charcoal+laser+filter&gs_l=mobile-gws-img.3...54280.65802.0.66574.31.30.1.0.0.0.499.9455.2-12j13j4.29.0....0...1.1.64.mobile-gws-img..6.7.2530...0j35i39k1j0i30k1j0i8i30k1j0i24k1j30i10k1.V7M8IiYJYMg#imgrc=Z1fgsj2v9gRNTM:)


---
*Imported from [Google+](https://plus.google.com/101993772584820352944/posts/jb7RGRP9tZL) &mdash; content and formatting may not be reliable*
