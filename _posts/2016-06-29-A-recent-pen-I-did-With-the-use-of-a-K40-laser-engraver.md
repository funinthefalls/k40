---
layout: post
title: "A recent pen I did. With the use of a K40 laser engraver"
date: June 29, 2016 08:18
category: "Object produced with laser"
author: "Eoin Kirwan"
---
A recent pen I did. With the use of a K40 laser engraver

![images/0ffb4e578fb2874ffb254cfd7c38e1cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0ffb4e578fb2874ffb254cfd7c38e1cd.jpeg)



**"Eoin Kirwan"**

---
---
**Glyn Jones** *June 29, 2016 08:55*

I think this is great


---
**Christoph E.** *June 30, 2016 00:04*

Great job. What kind of wood is this?


---
*Imported from [Google+](https://plus.google.com/103438187016886371314/posts/hQMHr3KcYha) &mdash; content and formatting may not be reliable*
