---
layout: post
title: "I was 'commissioned' to make the trophies for an upcoming race at the local track"
date: August 16, 2016 12:26
category: "Object produced with laser"
author: "Bob Damato"
---
I was 'commissioned' to make the trophies for an upcoming race at the local track. This is how it came out. The granite I used was interesting in that it had flakes in it that just werent going to cut so I get the black blotchy look. It actually looks kind of nice in person and the text is way easier to read than it appears in the picture.  I hope they like them. They are substantial, and I wouldnt mind getting one of these if I won!



![images/4b007962b63556ec32b65aee19a639c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b007962b63556ec32b65aee19a639c4.jpeg)



**"Bob Damato"**

---
---
**Bob Damato** *August 16, 2016 12:28*

I just noticed that in this picture the 2nd and 3rd place trophies dont have their sides polished or edges cleaned up like the 1st place one. I finished each one with adhesive cork backing so they wont scratch a desk or mantle.




---
**Ariel Yahni (UniKpty)** *August 16, 2016 13:04*

Impressive and very good looking


---
**Phillip Conroy** *August 16, 2016 13:11*

Nice ,well done


---
**greg greene** *August 16, 2016 13:19*

Where were you able to get the stone from?


---
**Bob Damato** *August 16, 2016 13:23*

Thanks guys! **+greg greene** I called a local granite supply place and asked if they had scraps. Each and every one of them that I called said to come down and take as much as I wanted. They have a lot of sink cut outs etc that they want to get rid of. Didnt cost me a thing. When this particular owner found out I was making trophies out of them he asked about how I engrave. When I told him about the laser set up he asked if I could make a small stone out of granite for a family pet that recently passed away.  I said of course! When I got there he had a huge stack of these pieces already cut for me to the right size I needed. It worked out great :) 


---
**greg greene** *August 16, 2016 13:26*

Great Idea !


---
**Robi Akerley-McKee** *August 16, 2016 17:38*

you can take a bit of white artist paint and squeegie is into the cut areas to bring up the contrast.  I used a really light granite and used indian ink to darked the cut areas.


---
**Bob Damato** *August 17, 2016 00:04*

THats a terrific idea!


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/ehpJJoBcFLP) &mdash; content and formatting may not be reliable*
