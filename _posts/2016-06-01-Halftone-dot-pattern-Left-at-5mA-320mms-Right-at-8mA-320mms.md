---
layout: post
title: "Halftone dot pattern. Left at 5mA /320mms - Right at 8mA / 320mms"
date: June 01, 2016 18:16
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Halftone dot pattern. Left at 5mA /320mms -  Right at 8mA / 320mms.  Stock K40 with upgraded focus lense. Is this acceptable? ﻿ What's you current workflow on stock machines? 

![images/282ba47d74d7fd9d5eedc3b531cb41a1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/282ba47d74d7fd9d5eedc3b531cb41a1.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Brandon Smith** *June 01, 2016 18:54*

I like the halftone on the right. Have not tried halftone myself, but I like the subtle look.


---
**Alex Krause** *June 01, 2016 20:05*

I use GIMP with brightness/contrast adjust then Dither. Or use threshold depending on the effect I am wanting to achieve 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 02, 2016 00:11*

Somewhere in between the two would be nice. The left seems a little light & the right seems a little dark. I'd try the 8mA @ 500mm/s. It'll get the job done slightly quicker too.


---
**Alex Krause** *June 02, 2016 00:30*

Is that cardboard/pasteboard?


---
**Ariel Yahni (UniKpty)** *June 02, 2016 01:06*

**+Alex Krause**​ is mdf


---
**Alex Krause** *June 02, 2016 01:41*

The frayed edges tricked me :P


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Doid5RkAXAN) &mdash; content and formatting may not be reliable*
