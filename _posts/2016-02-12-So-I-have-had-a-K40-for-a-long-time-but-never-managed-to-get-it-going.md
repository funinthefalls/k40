---
layout: post
title: "So I have had a K40 for a long time, but never managed to get it going"
date: February 12, 2016 08:36
category: "Modification"
author: "Richard Vowles"
---
So I have had a K40 for a long time, but never managed to get it going. I purchased it working, bought it home and it didn't work. Seller said it was working when I got it - so. 



I purchased a replacement board from Full Spectrum a couple of years ago ($US500 - ouch!) but never installed it because I largely had no idea what I was doing. Following along here, I am now thinking that the board is designed to completely replace the K40 board rather than just the USB adapter board. 



I've taken some photos - so any advice would be greatly appreciated! 





![images/feba944e56237f0bfd1f4a1e3c6e82ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/feba944e56237f0bfd1f4a1e3c6e82ce.jpeg)
![images/4ef3572980169d9b2f36336c9580052b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4ef3572980169d9b2f36336c9580052b.jpeg)

**"Richard Vowles"**

---
---
**ThantiK** *February 12, 2016 13:11*

Yes, it's designed to replace the whole USB adapter board and controller.  


---
**Richard Vowles** *February 12, 2016 17:38*

So the board with the big transformers as well?


---
**ThantiK** *February 12, 2016 18:15*

Nope, that looks like a power supply with the casing taken off of it.  That's what fires the tube.  That stays.


---
**Richard Vowles** *February 12, 2016 19:13*

Right, thanks! I thought that was the stuff behind the grill on the left side of the casing, but you never know :-)


---
**ThantiK** *February 12, 2016 19:34*

That's a power supply too!  Looks like it powers the laser PSU, and the controller boards; you'll need to keep both of those.  The metal case with the holes, and the board that has the huge heatsink on it and the transformers.


---
*Imported from [Google+](https://plus.google.com/+RichardVowles/posts/5fxp4YaQruD) &mdash; content and formatting may not be reliable*
