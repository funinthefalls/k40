---
layout: post
title: "What's best for cleaning lens a high percentage alcohol or acetone?"
date: November 06, 2015 01:45
category: "Discussion"
author: "Nathaniel Swartz"
---
What's best for cleaning lens a high percentage alcohol or acetone?  I've seen people mention both online.





**"Nathaniel Swartz"**

---
---
**Joey Fitzpatrick** *November 06, 2015 02:25*

I use 91% alcohol. just be sure to use a soft cloth or q-tip/cotton swab and clean gently(to avoid scratches) 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 06, 2015 07:20*

So far I've been using Mineral Turpentine with a cotton q-tip/cotton earbud. Not sure if the turpentine causes any damage (as I am new to this too), but it seems to do the job well.


---
**Leanne Purnell** *November 06, 2015 09:09*

I use rose spirits its 91% alcohol & a cotton bud, works great and evaporates clear doesn't leave streaks, and don't need wipe dry. Can get from any cake decorating supply place, even online. sounds strange i know but we also do cake decorating.


---
**Nathaniel Swartz** *November 06, 2015 13:47*

Awesome, thanks all, I'll have to run to the drug store as soon as it opens.


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/iuBTyoWNbzf) &mdash; content and formatting may not be reliable*
