---
layout: post
title: "I recently repaired an optical end stop for a community member and felt it was time to share the repair procedure"
date: August 30, 2017 04:32
category: "Original software and hardware issues"
author: "Don Kleinschnitz Jr."
---
#K40OpticalEndStop

I recently repaired an optical end stop for a community member and felt it was time to share the repair procedure.

Since you cannot buy replacements (at least I have not found them) I figured you could try and repair your own end stops. 



Its pretty straightforward if you have basic soldering tools and skills.



Take a read and let me know if there is any thing I missed or is unclear.





[http://donsthings.blogspot.com/2017/08/repairing-k40-optical-endstops.html](http://donsthings.blogspot.com/2017/08/repairing-k40-optical-endstops.html)





**"Don Kleinschnitz Jr."**

---
---
**Paul de Groot** *August 30, 2017 06:44*

My k40 end stops are hall sensors. Not optical so be aware of the many options 


---
**Don Kleinschnitz Jr.** *August 30, 2017 13:23*

**+Paul de Groot** mmm it does say 'Optical" :)?


---
**HP Persson** *August 31, 2017 17:57*

Nice guide! Dont have the optical ones myself, but i see the problems with them almost every day. Good to have a guide to link! :)


---
**Don Kleinschnitz Jr.** *August 31, 2017 18:39*

**+HP Persson** I have used optical sensors in this kind of applications for years so I was surprised there were so many problems, especially touted as electrical noise. Now I think its because there is a design problem with these (incorrect pullup) and the sensor gets clobbered by the interposer. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/KZpTBq7k8v8) &mdash; content and formatting may not be reliable*
