---
layout: post
title: "Finally got my laser cutter to cut"
date: February 26, 2016 03:11
category: "Discussion"
author: "3D Laser"
---
Finally got my laser cutter to cut.  It's not perfect and I can't wait for my 18mm lens to get here but not bad.  However I do have a few issues.  My laser cutter is cutting at an angel.  The ends are not a straight 90 degrees more like 85 to 88 degrees any reason why it would do that also it takes me two passes to get through the material at least and in some spots it still doesn't want to cut out.  Any help would be great 



![images/dbcba1db9244fa5264700695d9afcbfe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dbcba1db9244fa5264700695d9afcbfe.jpeg)
![images/fb2bbf8d0c479fbc1c0fe605df791288.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb2bbf8d0c479fbc1c0fe605df791288.jpeg)
![images/c4b92823568c371e324aacfef3744eab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4b92823568c371e324aacfef3744eab.jpeg)
![images/b6ac91975ef84a10b53eca8a632aebd1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6ac91975ef84a10b53eca8a632aebd1.jpeg)

**"3D Laser"**

---
---
**I Laser** *February 26, 2016 03:15*

The angle is most likely caused by your laser head not being perpendicular to the work piece.


---
**3D Laser** *February 26, 2016 03:32*

You mean the beam is not hitting the center of the lens?


---
**I Laser** *February 26, 2016 03:46*

No, I mean your laser head is not square to your work piece. 



They say a picture paints a thousand words, even a crappy photoshop picture lol...



[http://s12.postimg.org/74j6db1gd/387006455_397.jpg](http://s12.postimg.org/74j6db1gd/387006455_397.jpg)


---
**Richard Taylor** *February 26, 2016 07:23*

I had angled cuts on my K40 before I made sure all of the mirrors were aligned.

Depending where on the cutting area I was, I would get different angles due to the beam hitting the final mirror in different places.

Thermal paper (till receipts) are very useful during alignment...


---
**Stephane Buisson** *February 26, 2016 08:10*

**+Corey Budwine** As I try to explain you before, setting the laser isn't about to have the laser hitting the mirror center, but to be parrallel to the X or Y, the laser head should be vertical (Z have to be set first), if the K40 isn't perfectly built the laser could be off the mirror center and have the perfect path.

mirror center isn't a goal, being parallel is.

from the photos, You are working on a nice little project here ;-))


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/89FWsVHQh8B) &mdash; content and formatting may not be reliable*
