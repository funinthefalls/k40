---
layout: post
title: "When you want to make something big, but are limited to 8 1/2 x 10"
date: December 28, 2017 18:15
category: "Object produced with laser"
author: "Printin Addiction"
---
When you want to make something big, but are limited to 8 1/2 x 10.



This was a Christmas present for my stepfather, each tile took approximately 3 hours. Then I applied black stonefil by Johnson Plastics, waited about 10, then wiped off the excess, I think the effect was pretty cool, almost looks like pencil or charcoal. I couldnt find the GT emblem anywhere so I traced it out in inkscape. The strips of veneer for the logo are different woods, not colored with stain or paint. I was afraid of the mustang breaking so I attached it with magnets, so that it could be removed.





![images/ef8bd41d2583543f7f2af4ca48fb304d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef8bd41d2583543f7f2af4ca48fb304d.jpeg)
![images/df4f2531558d002b4a7eabdd58bdd44a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/df4f2531558d002b4a7eabdd58bdd44a.jpeg)
![images/3a2fb60b5e883e008213af1056dd8ea0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3a2fb60b5e883e008213af1056dd8ea0.jpeg)
![images/3b3362705817813a911beb546929059c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b3362705817813a911beb546929059c.jpeg)

**"Printin Addiction"**

---
---
**Joe Alexander** *December 28, 2017 18:22*

gorgeous!


---
**Ned Hill** *December 28, 2017 18:31*

That's beautiful.  Great job!  What kind of tile did you use?




---
**Printin Addiction** *December 28, 2017 18:42*

**+Ned Hill** I had the hardest time finding white tile, the best I could do was 4x6 glossy white. I searched all of the the Home Depots and Lowes in my area. About 2 weeks later, I was looking for rock salt in my garage, and found a milk crate full of 6x6 matte white tiles, which ended up being perfect. 


---
**HalfNormal** *December 28, 2017 19:05*

Unbelievable!


---
**Andy Shilling** *December 28, 2017 20:54*

What a fantastic piece looks amazing.


---
**Mike Mauter** *January 01, 2018 15:04*

Very nice job-beautifully done. Almost everything I do with my laser is on painted ceramic tile. I use this  [homedepot.com - Daltile Glacier White 12 in. x 12 in. Ceramic Floor and Wall Tile (11 sq. ft. / case)-55001212HD1P2 - The Home Depot](https://www.homedepot.com/p/Daltile-Glacier-White-12-in-x-12-in-Ceramic-Floor-and-Wall-Tile-11-sq-ft-case-55001212HD1P2/202519403)   I usually cut them down to either 8 by 10 or 8 by 12. 




---
**Printin Addiction** *January 01, 2018 16:50*

**+Mike Mauter** you paint the tile first? So your burning away the paint, then sealing it?



I should try that along with etching the tile, then compare the results. 


---
*Imported from [Google+](https://plus.google.com/+PrintinAddiction/posts/6LJRWrAvFFx) &mdash; content and formatting may not be reliable*
