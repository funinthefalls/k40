---
layout: post
title: "Just to insure everyone here gets the most recent updates on my K40-S design I offer these links"
date: June 11, 2016 15:42
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Just to insure everyone here gets the most recent updates on my K40-S design I offer these links. Caution: this is all work in process but thought I would share for those engaged in the same activity.



I wish blogger had an integration with G+ that let me include specific communities when you post. Perhaps that is to much to ask from two products from the same company :). 





[http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)



[http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.html](http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.html)



[http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html)





**"Don Kleinschnitz Jr."**

---
---
**Anthony Bolgar** *June 11, 2016 15:45*

Great job documenting your conversion. I like that you do not make assumptions, you use the scientific method properly (Hypothesis, Testing, Evaluation of Data). Your work will ease the suffering of the not so technically minded who want to upgrade their K40.


---
**Jim Hatch** *June 11, 2016 15:49*

👏👍 great work. I owe you a pint or two 😃


---
**Don Kleinschnitz Jr.** *June 11, 2016 16:02*

Thanks for the encouragement, going slow but wanted to get a repeatable result.


---
**Dennis Fuente** *June 11, 2016 22:30*

so did you make the board or purchase it


---
**Don Kleinschnitz Jr.** *June 11, 2016 22:56*

purchased it ... from here...[https://oshpark.com/shared_projects/3W1BpcNl](https://oshpark.com/shared_projects/3W1BpcNl)


---
**Don Kleinschnitz Jr.** *December 11, 2016 06:30*

**+Dennis Fuente** purchased see post for details


---
**Jeremy G (WeisTek Engineering)** *February 20, 2018 07:07*

Hi, Don glad to see the K40 Middle man board worked for you. Are you still using it or have you moved on to another solution?



Thanks

Weistek


---
**Don Kleinschnitz Jr.** *February 20, 2018 12:33*

**+Jeremy G** its installed and working :).



You can see it to the left middle of the box.



[photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/Gvu4M28TBt2bUhfv2)






---
**Jeremy G (WeisTek Engineering)** *February 21, 2018 02:37*

:) that's awsome, glad it working.



great write up btw.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Bf5KUhsHmVP) &mdash; content and formatting may not be reliable*
