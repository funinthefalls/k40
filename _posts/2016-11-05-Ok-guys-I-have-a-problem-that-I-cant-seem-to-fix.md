---
layout: post
title: "Ok guys, I have a problem that I can't seem to fix"
date: November 05, 2016 22:05
category: "Modification"
author: "Mark Leino"
---
Ok guys, I have a problem that I can't seem to fix. For the last 24 hours I have been bugging **+Alex Krause**​ getting my smooth k40 put together. I wired it up just like he did and everything works great -except the laser does not turn on. 



I wired it up with a level shifter and a smoothie board, the steppers work and I can run g code from laser web. The test (text lol) light on the power supply has also never came on. The laser will not do anything. I looked at the tube, could not see cracks. I have been running water through it all day, no leaks or drips. I tested all the switches on the control panel, all seemed to be functioning properly.



I have been hunting for loose wires with no luck. I was hoping you guys might be able to help me out.



Thanks, I appreciate your time :-)





**"Mark Leino"**

---
---
**Alex Krause** *November 05, 2016 22:10*

**+Don Kleinschnitz**​ this sounds like the HV side of the transformer isn't responding correctly... can you chime in


---
**Mark Leino** *November 05, 2016 22:11*

Is there a fuse in these power supplies? 


---
**Mark Leino** *November 05, 2016 22:27*

**+Peter van der Walt** could you give a little more detail? I did hook up a wire to ground from the back of the k40


---
**HalfNormal** *November 05, 2016 22:57*

**+Mark Leino**ark Do you have the laser enable wired up? If the button has been removed, make sure you have it jumped. 


---
**Mark Leino** *November 06, 2016 01:16*

I have all the switches still wired in.    Is there a safe way to test the high voltage side of psu? Bottom line is no current at all is passing through laser tube. 



From what I understand, pressing the"test" button should bypass the enable switch. The ammeter on the laser control panel has never moved since I got it. 



I pulled off the connections on both sides of the laser, to ensure they were connected. Blue wire goes from laser tube, to ammeter, then to ground. On the positive side, a heavy gauge wire comes directly from power supply.



Pic is the back of power supply.

I'm highly suspecting no current is being sent to positive side of laser tube

![images/0003ac16c00d0a5abdae6a5225f3f61f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0003ac16c00d0a5abdae6a5225f3f61f.jpeg)


---
**Ariel Yahni (UniKpty)** *November 06, 2016 02:00*

**+Mark Leino**​ start a claim with the [supplier.mine](http://supplier.mine) got with a cracked tube, mind I just noticed after I removed the plastic cover it had


---
**Mark Leino** *November 06, 2016 02:39*

I did already. I was determined to get this functioning this weekend, but it's got me whipped. I went through every connection, tested everything I know how to, opened up the power supply, it does have a small fuse inside BTW. Only think I couldn't test is whether voltage is coming to laser tube. Thanks everyone for your help.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 06, 2016 07:39*

Could it be your Smoothie config file? #laser_enable false... should be laser_enable true (without the # at the start commenting it out).


---
**Don Kleinschnitz Jr.** *November 06, 2016 12:01*

Use High Voltage and laser safety procedures (including wearing protective glasses) when making any of these measures.



1. Do you have a test switch on the power supply?



2. If you push it does it fire if the enable button is pushed on.

You said above the red or green led on the LPS mother board is not lit?

3. Do you have AC to the supply (measure at the terminals)



4. Is the glass fuse blown (read resistance across it) or any charred parts. Unplug the machine from the wall and wait a few minutes when you do the above.



Post a picture of your LPS from the connector end showing wiring. 



If the laser does not fire when the enable is asserted and the test button (on LPS motherboard) pushed you have something wrong with the LPS. If  I recall the led on the LPS motherboard should come on when AC power applied. (I am remote from my machine).






---
**Mark Leino** *November 06, 2016 14:24*

1. Yes, well it says text haha

2. No

There are 2 leds on power supply, next to AC power terminal strip. The green led (power) comes on when I turn on main power switch. The red led (L) does not come on no matter what buttons I push. The ammeter by main power switch has never moved either. I checked resistance between all connections between start of laser tube and ground, all were fine. I did not check between power supply and laser tube, since connection seemed sealed at power supply.



3. Yes, read 119volts from black to red, black to yellow, and black to other yellow on 4 wire terminal strip into power supply.



4. 0.2 ohms across tiny glass fuse inside of power supply, nothing appears out of place, or charred.

 Thanks **+Don Kleinschnitz**​



![images/21f0f6331386a511059efe7fe8c9e5a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/21f0f6331386a511059efe7fe8c9e5a6.jpeg)


---
**Don Kleinschnitz Jr.** *November 06, 2016 14:55*

**+Mark Leino** it seems like the supply is dead or we still have the interlock loop open some how. 



DO NOT try and measure the output of the LPS's high voltage. You needs special tools and it won' tell you anything you don't already know and it is dangerous. This supply puts out extremely high voltage.



I am guessing its not related to the pot as the "text" switch does not work.





Here are some Hail Mary measures:



Wear your laser glasses:



With machine on and interlocks and "Laser Switch" closed.

Can you measure and tell me what voltage from gnd** to these pins are:

**there is gnd on the DC connector to the right



P+ (black)

G (black)

K+ (white)

K- (white



You might as well measure the pot leads while you are at it.




---
**Mark Leino** *November 06, 2016 15:10*

**+Don Kleinschnitz**​

Just to confirm so I get the right measurements- test with "laser switch" depressed to the on position, "test laser" switch depressed, and text switch pressed


---
**Mark Leino** *November 06, 2016 15:50*

**+Don Kleinschnitz**​

I'm feeling a little dumb right now. I pulled the connectors off the smoothie to use the ground.  Sure as sh** I pressed the test button and the led (L) lit up, and the laser fired.



So the problem lies in my wiring to the smoothie....


---
**Don Kleinschnitz Jr.** *November 06, 2016 16:55*

+Mark Leino



My recommendation in the picture.



Something must be holding enable high.



I would be interested in what is miss wired when you solve this?



The picture keeps disappearing ??


---
**Don Kleinschnitz Jr.** *November 06, 2016 16:59*

Trying again

![images/cee2268645aa64b41003806b278bc803.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cee2268645aa64b41003806b278bc803.jpeg)


---
**Mark Leino** *November 06, 2016 17:54*

Well I found what was wrong. It was the 4 wires directly off the smoothie board. I misunderstood how Alex had it wired. He had the main 4 wires wired to A,b,d of main smoothie terminal. I had them abcd, so it was not allowing laser to fire. Dumb mistake, it was the first thing I wired up.



Now I have the opposite problem. When I turn on the "laser switch" it fires right up. 



While doing all my checking, I noticed that the "test switch" was allowing some current to pass even when off. So this is likely the cause. I will report back. Thanks **+Don Kleinschnitz**​. I had pretty much given up until I got some more suggestions from you:-)


---
**Mark Leino** *November 06, 2016 17:58*

Well it was leaking through the test switch, but something is still holding the laser on. 



**+Ariel Yahni**​ what was that you told me about getting a laser? Something like it's not a fairy tale or something?? Haha so true


---
**Mark Leino** *November 06, 2016 18:07*

[https://plus.google.com/109807573626040317972/posts/CXFk1n4oezp](https://plus.google.com/109807573626040317972/posts/CXFk1n4oezp)



This is how I have it wired now﻿


---
**Ariel Yahni (UniKpty)** *November 06, 2016 18:08*

Exactly, far from it. Some even till today do 3 more more passes to cut 3mm just because aligning is a he'll also, other just have luck 


---
**Alex Krause** *November 06, 2016 18:14*

As far as the laser staying on did you connect to the mosfet or the 2.4 pin directly? This will make a difference in your config file


---
**Mark Leino** *November 06, 2016 18:18*

I have changed nothing in the firmware. 

![images/c63812d5b427ae2e1b949c3032cbf660.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c63812d5b427ae2e1b949c3032cbf660.jpeg)


---
**Alex Krause** *November 06, 2016 18:20*

Open your config file check to see if laser fire pin is set to 2.4!


---
**Don Kleinschnitz Jr.** *November 06, 2016 22:24*

Are you wired and configured as my drawing shows?

You have to make sure the pwm on 2.4 in config file is as the drawing shows.


---
**Don Kleinschnitz Jr.** *November 06, 2016 22:24*

Are you wired and configured as my drawing shows?

You have to make sure the pwm on 2.4 in config file is as the drawing shows.


---
**Alex Krause** *November 06, 2016 22:26*

Sorry that is what I was getting at... if it was set to 2.4! The laser would be on all the time


---
**Mark Leino** *November 06, 2016 22:42*

Ok after all of that, an intermittently loose wire on the k40 power supply plug, me being dumb and misunderstanding the terminal strip terminology, the test switch on the panel working intermittently, I think it's working as it should! Got the firmware deal figured out. Now just need to configure stepper motors and good to go.... I hope. I already changed the limit switches enabling x min y max. 



Any one have a link for config changes for k40? 


---
**Mark Leino** *November 06, 2016 22:45*

I went back to how Alex had it. That allows for use of the potentiometer correct? To change to allow the smoothie to control pwm, I would need an optocoupler diode, is that what I am understanding?


---
**Mark Leino** *November 07, 2016 01:56*

I can run g code in laserweb, but having issues getting the laser to fire. Oddly when I unplug the USB (with the k40 on) the laser fires. I have gone in and switched the laser fire pin, changed laser enable from false to true, and nothing changes it seems? If I unhook level  shifter from pwm and 3.3v it does the exact same thing. Starting to think that on top of everything else I screwed up the level shifter as well. Any ideas?


---
**Alex Krause** *November 07, 2016 02:01*

**+Mark Leino**​ I sent you a message on hangouts


---
**Don Kleinschnitz Jr.** *November 07, 2016 03:19*

**+Mark Leino** neither. You do not need anything but 2 wires and you don't need to do anything to the pot ... please check the diagram. 




---
**Don Kleinschnitz Jr.** *November 07, 2016 03:21*

 .......I don't really get why after all this research and testing we are still using/recommending  level shifters and making this more complex than it needs to be.... TWO WIRES!  

Its not helping our Smoothie adoption....

Perhaps I am still missing something?


---
**Alex Krause** *November 07, 2016 03:21*

**+Don Kleinschnitz**​ I have instructed him to follow your diagram and explained that the opto diode was internal to the PSU which could be confusing to some folks


---
**Alex Krause** *November 07, 2016 03:28*

**+Don Kleinschnitz**​ it might help if you do some engraving and post the results of it or I can rewire mine and do so... might help get people on board... the only reason why I did a level shifter was I was told to do so at the time because of the lack of knowledge of the internals of the PSU. The only way to get people onboard is to show the end results of a gray scale engrave


---
**Don Kleinschnitz Jr.** *November 07, 2016 03:30*

**+Mark Lein**      AH .... the diode in the white box was intended to show what the PWM is connected to inside the supply.

That's probably to much information for most folks, I will remove it.


---
**Don Kleinschnitz Jr.** *November 07, 2016 03:43*

**+Alex Krause** what would the logic be that engraving is somehow sensitive to the PWM control interface. (assuming it is a proper digital signal with variable DF).



PWM is digital and no matter the end result power in a PWM controlled system uses exactly the same mechanism whether you are raster-ing, vectoring etc.



Or that a level shifter would engrave better than an open drain?



They are all digital PWM pulses with varying duty factors?



I will ask **+Robert Rossi** to show some of the engraving he is doing while I am still testing PWM response.


---
**Alex Krause** *November 07, 2016 04:09*

All I'm trying to say is people will adopt what "works" I will rewire mine in a day or two and do some engraving. I have no doubts that your way is the best way to wire it, but without actually seeing any results people are hesitant to try something different than what has been posted out there. I think that is the biggest hurdle to overcome with adapting to what is most likely the best practice in dealing with these machines 


---
**Mark Leino** *November 07, 2016 04:12*

In all honesty I was looking for the easiest way, and being unfamiliar with this controller I used what I had. If you had left out the opticoupler and open drain I would have done it that way in a heart beat. I thought they were things I had to buy. I also had the controller in my hands and was trying to wire it 2 minutes later 


---
**Mark Leino** *November 07, 2016 04:19*

Wired it **+Don Kleinschnitz**​​ way, success! Like he had on his drawing, and pin (2.4), not (2.4!) Easy!

**+Alex Krause**​​ thanks for your awesome in depth trouble shooting. I owe you guys big time!


---
**Alex Krause** *November 07, 2016 04:21*

**+Mark Leino**​ Awesome! I am glad you got it working using **+Don Kleinschnitz**​ method. Now on to LASER EVERYTHING. Muhahahahah


---
**Mark Leino** *November 07, 2016 04:31*

Oh I know I'm looking around thinking of engraving everything! Just so happens I was prepared for this and order several sheets of acrylic, the ups delivery guy must really wonder what I do with all these crazy shaped packages


---
**Mike Wiles** *January 15, 2017 08:01*

Hi guys,



I just read this post, and it looks like **+Mark Leino** has the exact same PSU as I have in my k40. I got my Smoothieboard months ago, and have had fits and starts getting it to work. I am very close though.



My problem was that I couldn't get the laser to fire at all. I had it wired up following **+Don Kleinschnitz**'s blog post here: [http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html). I would run gcode from LaserWeb, and I could only get the laser to fire when I held down the test button. 



That got me to thinking that I needed to use the K+ wire, which I guessed from Don's configuration file here: [https://drive.google.com/file/d/0B9A1NYiboK99VEthU1NYMGxLbFk/view?usp=sharing](https://drive.google.com/file/d/0B9A1NYiboK99VEthU1NYMGxLbFk/view?usp=sharing). This was confusing because I keep seeing "only two wires!" but Don's config references 2.4 and 2.5.



Anyway, I tied the K+ to the "red side" of 2.5. This forced the laser always on. I went in to the config and tried every combination: 2.4^, 2.4v, 2.4^!, and 2.4v!. No luck with any of them. The laser will not go off.



Does anyone have any idea what I have done wrong? I will attempt to include a pic, and my current config so you can delight in my incompetence!



Thanks for any help you can give me!



Mike 






---
**Mike Wiles** *January 15, 2017 08:03*

If it isn't clear...

The yellow wire on the connector that isn't pushed in all way is spliced to K+. I pulled it out a bit to remind me to take it completely out once I took the pic. I didn't want to burn the house down!

The green connector on the lower left is the +5v line. I have been afraid to leave it plugged in because I wanted complete control of when the Smoothie was on while I was testing.

 

![images/4fc902ad74878ad8cc1f9a998f76793a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4fc902ad74878ad8cc1f9a998f76793a.jpeg)


---
**Mike Wiles** *January 15, 2017 08:20*

If it wasn't obvious, I'm very new to Google+, I hope I haven't breached etiquette. I can't figure out a good way to attach my config, so I am just pasting in what seems appropriate.



## Laser module configuration

laser_module_enable                          true            # Whether to activate the laser module at all. All configuration is

                                                              # ignored if false.

#laser_module_pin                             2.4             # this pin will be PWMed to control the laser. Only P2.0 - P2.5, P1.18, P1.20, P1.21, P1.23, P1.24, P1.26, P3.25, P3.26

                                                              # can be used since laser requires hardware PWM

#laser_module_maximum_power                   1.0             # this is the maximum duty cycle that will be applied to the laser

#laser_module_minimum_power                   0.0             # This is a value just below the minimum duty cycle that keeps the laser

                                                              # active without actually burning.

#laser_module_default_power                   0.8             # This is the default laser power that will be used for cuts if a power has not been specified.  The value is a scale between

                                                              # the maximum and minimum power levels specified above

#laser_module_pwm_period                      20              # this sets the pwm frequency as the period in microseconds



switch.laserfire.enable 		true 		#



#switch.laserfire.output_pin 		2.5^ 			# connect to laser PSU fire (!^ if to active low, !v if to active high)

#switch.laserfire.output_pin 		2.5v 			# connect to laser PSU fire (!^ if to active low, !v if to active high)

#switch.laserfire.output_pin 		2.5!^ 			# connect to laser PSU fire (!^ if to active low, !v if to active high)

switch.laserfire.output_pin 		2.5!v 			# connect to laser PSU fire (!^ if to active low, !v if to active high)



switch.laserfire.output_type 		digital 		#



switch.laserfire.input_on_command 		M3 		# fire laser



switch.laserfire.input_off_command 		M5 # laser off



 


---
**Don Kleinschnitz Jr.** *January 15, 2017 14:50*

**+Mike Wiles** sorry I am pretty confused, as I often get, as to what you have and what is working or not. 



The K+ signal has nothing to do with the laser PWM, in fact its 4.5V. The LPS wiring should stay like stock except for 2 wires to ADD to the laser power supply on its DC connector as the diagram below shows.



You can save your config file to your Gdrive and then share it with a link here.



In the config file:

I would suggest a review of this section: [http://smoothieware.org/configuring-smoothie](http://smoothieware.org/configuring-smoothie)



The # at the beginning of a statement makes that line a comment and essentially disables it.



The switch statements in my laser config file can be ignored for now as they are not relevant to getting PWM to work. 



Your file dump above has multiple #'s that are disabling the controls for PWM. 

 After insuring that you are wired like these diagrams, I would start fresh with my config file and concentrate on getting PWM to work before changing any of the config file. 



I am certain that my config file with the wiring as this diagram suggests will work :).



Have you wired the system exactly like this.

[photos.google.com - New photo by Don Kleinschnitz](https://goo.gl/photos/xkEx5PiPwk9QEdyHA)


---
**Don Kleinschnitz Jr.** *January 15, 2017 15:10*

**+Peter van der Walt** Are you saying this is an obsolete post or just an older unresolved problem? it showed it was 7 hrs old.


---
**Mike Wiles** *January 15, 2017 15:47*

Thanks for your response, **+Don Kleinschnitz**!



Yes, I believe I have wired the laser power section like your diagram. I don't have the middle man circuit you describe because everything seemed to work as they were supposed to. I was able to connect the stepper motors and end stops directly to the board.



As for the config file, I intentionally left the commented lines in to give myself a log of what I had done, and also prove that I tried each combination. (I also made sure to restart the smoothie each time so the config changes took effect.)



I piggybacked on this particular post because it looks like **+Mark Leino** has the exact same machine as I do, and he solved his very similar sounding problem. Unfortunately, one of his linked pictures doesn't resolve any more, and it looks like whatever troubleshooting and "ah ha!" Moments happened in private conversations. Th solution feels tantalizing close to me! 



To summarize my wiring and config... the four blue wires coming from my laser psu are going to the following places:



24v - to the 12-24v input on the smoothie (which must be working, because my machine will execute gcode.)

GND - to the GND of the 12-24v, and also the "right-most" side of the middle "big mosfet" (sorry, I don't know the PIN number, but I'm sure it matches your diagram.)

5v - to the 5v input on the smoothie (but I don't keep that plugged in because I want it to be lowered by my pc's USB while I am testing.

L - to pin 2.4 on the smoothie. (Left-most, just like your diagram.)



I hope this makes sense. Thanks again for the help!


---
**Mark Leino** *January 15, 2017 15:54*

I can't even remember what I did to fix it.. It was something that I did wrong though... Sorry!


---
**Mark Leino** *January 15, 2017 15:59*

Not sure it will help, but this is all I have connected to my smoothie and it works.

![images/79c1d804dec824741fbb59af267e9eb5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/79c1d804dec824741fbb59af267e9eb5.jpeg)


---
**Don Kleinschnitz Jr.** *January 15, 2017 16:00*

**+Mike Wiles**  I think that you are wired incorrectly. Check the photo below of my actual wiring and yours. Make sure you are using the correct edge connectors. You do not have a socket in the leftmost position so you may be off by one?

![images/6cedc70dd7a691fbc672d64d996ec589.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6cedc70dd7a691fbc672d64d996ec589.png)


---
**Mike Wiles** *January 15, 2017 17:13*

Here is my marked up image. I think everything matches what I am seeing in both **+Don Kleinschnitz** and **+Mark Leino** pics.

![images/b6abd9637bfbcdcc849bc4defc9a2063.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6abd9637bfbcdcc849bc4defc9a2063.jpeg)


---
**Don Kleinschnitz Jr.** *January 15, 2017 17:42*

**+Mike Wiles** Running my exact config file?

This now looks different than the first picture you attached as you had something in the 5th connector from the left edge. Now its the same as mine?


---
**Mike Wiles** *January 15, 2017 18:25*

I removed the K+. That held the laser on for always and forever. Basically, I reverted back to my initial problem: the laser never fires.



Before I posted this, I did a diff of your config against mine. It resulted in disabling the stock extruded heater settings, and adding the laser module settings that refer to 2.5. These settings are what led me to believe that I needed to connect K+. 



From there, I tried the four combinations of the 2.4 pin setting, three of which are commented out.


---
**Don Kleinschnitz Jr.** *January 15, 2017 19:51*

**+Mike Wiles** K+ is 5V and should not have had any effect if it was connected to "L"?



The configuration I am using works if its wired like mine, which you seem to be.



Something else is wrong. I will look at this thread again tonight. Do you have an oscilloscope?


---
**Mike Wiles** *January 15, 2017 22:56*

I connected K+ to 2.5 because I thought "Obviously closing the Test switch (short-circuiting K+ and K-) causes the laser to fire. Maybe pin 2.5 is effectively serving that purpose. Maybe that is why there is that entire section referring to 2.5"



I do not have an oscilloscope. That is one of the things I really regret leaving behind during my moves!



I will upload my config in a minute.


---
**Mike Wiles** *January 15, 2017 23:00*

Here is my config. I'm going to go through it and make it as identical to yours as possible. 



[https://drive.google.com/open?id=0B6iMocsPMfDOMjR3eG1MYjNXZTA](https://drive.google.com/open?id=0B6iMocsPMfDOMjR3eG1MYjNXZTA)


---
**Don Kleinschnitz Jr.** *January 16, 2017 00:12*

**+Mike Wiles** just download and use mine?


---
**Don Kleinschnitz Jr.** *January 16, 2017 00:38*

**+Mike Wiles** K+ is connected internally to 5v in the LPS through a pullup. It will not fire the laser. You connected the output of a mosfet through a pullup to 5v ....generates  a tiny bit of heat that's all :).

Connecting K- to 5v would turn on the laser. However the PWM from 2.5 in my config is negative true so the laser would have stayed on with no PWM signal.

Anyway I know we will get this to work with my schema :). 


---
**Mike Wiles** *January 16, 2017 03:03*

Success!



I was initially concerned with taking the numbers from **+Don Kleinschnitz** 's config because I am not using a middle man board anywhere. But I tried it anyway. As defined, my machine went bonkers as soon as I tried to home it.



I expected this, so step 2 was to change the following settings in the config:



  alpha_dir_pin                                0.5              # Pin for alpha stepper direction

  beta_dir_pin                                 0.11             # Pin for beta stepper direction

  alpha_max_endstop                            nc            # NOTE set to nc if this is not installed

  alpha_max                                    324              # this gets loaded after homing when home_to_max is set

  beta_max                                     218              #

  panel.enable                                 false             # set to true to enable the panel code

  network.enable                               true            # enable the ethernet network services



The last two are obvious. I don't have a panel, and I am hooking my Smoothie directly to my network.



The first five relate to how my steppers are setup and where my endstops are. I don't know if it is right, but it definitely works this way.



Anyway, I ran my gcode and it WORKED PERFECTLY!



Thank you **+Don Kleinschnitz** for your diagrams, blog posts, and config file!

Thank you **+Mark Leino** for taking the time to post your own pic, and giving me this post to leech from! ;)



Now to find calibration gcode and deal with my mirrors! (The adjustment screws seem to be superglued in place, so I am not looking forward to this part!)



Mike


---
**Don Kleinschnitz Jr.** *January 16, 2017 05:05*

**+Mike Wiles** and we have another winner!

Great job sticking with it ...... now the fun begins! 

My donation account is always open :)


---
*Imported from [Google+](https://plus.google.com/+MarkLeino/posts/FnfHYCxLExs) &mdash; content and formatting may not be reliable*
