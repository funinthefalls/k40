---
layout: post
title: "Ok so I've been thinking about my moshi board and the fact I have no X movement"
date: December 19, 2016 10:33
category: "Original software and hardware issues"
author: "Andy Shilling"
---
Ok so I've been thinking about my moshi board and the fact I have no X movement. Does anybody know if the home endstop under the X cartilage is only an endstop and could this play havoc when engraving?



I noticed a couple of times over the last few days, when engraving I get a little glitch and it steps over  to the left a few mm.



I'm now wondering if it's this endstop stopping the whole thing working 







**"Andy Shilling"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 19, 2016 10:45*

I don't really know, but could unplug it & see what happens. Could be that it is dirty (if optical type) & is triggering as having reached the end?


---
**Andy Shilling** *December 19, 2016 11:00*

That's exactly what I'm thinking, I've got new mechanical ones here ready to upgrade in the new year but the optical is fed by a ribbon and in not sure of the pinout to solder the new one in.



I'll try and unplug it first and see what that does.


---
**Don Kleinschnitz Jr.** *December 19, 2016 11:45*

**+Andy Shilling** Andy all of the pin out and design information for optical end stops is in this build log under "Interfaceing to the gantry electronics"

[donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Don Kleinschnitz Jr.** *December 19, 2016 11:46*

**+Andy Shilling** Is your X not working at all or has an intermittent movement (glitch)?

Does the machine home properly?


---
**Andy Shilling** *December 19, 2016 11:57*

**+Don Kleinschnitz**​ Y homes but doesn't move at all now.


---
**Don Kleinschnitz Jr.** *December 19, 2016 12:10*

**+Andy Shilling** Sorry, I mean it homes and then doesn't move X for a job? This is a stock K40?




---
**Andy Shilling** *December 19, 2016 12:19*

Sorry yes it's stock and X doesn't home at all,  normally when I turn it on it homes, now it doesn't and even sending something to it from the pc still won't make it move. 



If I give it a new start/home point from the pc for a job the Y will locate but nothing from the  X


---
**HalfNormal** *December 19, 2016 12:58*

**+Andy Shilling** I purchased a used K40 with a Moshi board that would not home. I found some software that would reset the Moshi board. I do not have access to that computer now but I am sure if you Google for it you will come across it. The board works just fine now.


---
**Andy Shilling** *December 19, 2016 13:01*

**+HalfNormal**​ thanks for the tip I'll try looking for it.


---
**Andy Shilling** *December 19, 2016 13:29*

+HalfNormal​ thanks again I've my test found the file online and I will try it when I get home. Fingers crossed.


---
**HalfNormal** *December 19, 2016 20:32*

**+Andy Shilling**​ please let me know if it helps.


---
**Andy Shilling** *December 19, 2016 20:39*

**+HalfNormal**​ I've just got it working to a degree, it's moving again but after trying to engrave a couple of bits it is struggling again. I managed to do the image below but as you can see it failed. Everything just stopped this time the laser the X and Y. I reset it tried again and it stopped in the same position again. I think I best just wait for my new board.

![images/af18bd7988de11b5e3bce6fbf6b44c38.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/af18bd7988de11b5e3bce6fbf6b44c38.jpeg)


---
**HalfNormal** *December 19, 2016 20:41*

It was worth the try!


---
**Andy Shilling** *December 19, 2016 20:45*

Yes it was. I took the optical stops out and cleaned them also and checked all connections so I am at a loss now. (Hurry up new year and Cohesion3D)


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/Six4dAFpy4A) &mdash; content and formatting may not be reliable*
