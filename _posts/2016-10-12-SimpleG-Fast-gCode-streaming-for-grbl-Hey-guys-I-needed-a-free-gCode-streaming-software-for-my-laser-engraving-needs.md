---
layout: post
title: "SimpleG - Fast gCode streaming for grbl Hey guys, I needed a free gCode streaming software for my laser engraving needs"
date: October 12, 2016 16:02
category: "Software"
author: "Timo Birnschein"
---
SimpleG - Fast gCode streaming for grbl



Hey guys, I needed a free gCode streaming software for my laser engraving needs. Since the only streaming software I could find was commercial and costs about $30-$40 I decided to develop my own and make it open source.

It needs testing! I'd like to ask you guys if you would like to test this software with your grbl controlled lasers.



It is a console based windows app that can be used as follows:

SimpleG.exe -f test.gcode -F SimpleG-end.gcode -p com6



A more precise documentation can be found here:

[https://github.com/McNugget6750/SimpleG](https://github.com/McNugget6750/SimpleG)



The sender was able to push 466 commands per second to my Arduino Nano based grbl laser! That gives me 2800mm/min max feed rate which makes engraving using grbl quite usable!



Looking forward to your comments.





**"Timo Birnschein"**

---


---
*Imported from [Google+](https://plus.google.com/+TimoBirnschein/posts/A4HHvU65fZS) &mdash; content and formatting may not be reliable*
