---
layout: post
title: "Could not figure out a way to add photos to my original post, so here is the 5V source for spot lasers and temp gauges on the Phoenix connector"
date: July 11, 2016 03:10
category: "Modification"
author: "Terry Taylor"
---
Could not figure out a way to add photos to my original post, so here is the 5V source for spot lasers and temp gauges on the Phoenix connector. Also my modded control panel.



![images/a3d4e48cce8b1c459a644a5cda15ea15.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3d4e48cce8b1c459a644a5cda15ea15.jpeg)
![images/dfc811c6a9be66b96dc93ef2ba9af85a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dfc811c6a9be66b96dc93ef2ba9af85a.jpeg)

**"Terry Taylor"**

---
---
**Anthony Bolgar** *July 11, 2016 03:32*

Looks good **+Terry Taylor** 


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/78Z98ycVmMa) &mdash; content and formatting may not be reliable*
