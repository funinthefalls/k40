---
layout: post
title: "What does everyone use to seal there water cooling tube to the co2 laser"
date: September 29, 2017 03:50
category: "Discussion"
author: "Space Ronin"
---
What does everyone use to seal there water cooling tube to the co2 laser. I still have the stock tubing it came with and  haven't fired it up yet. 





**"Space Ronin"**

---
---
**Phillip Conroy** *September 29, 2017 05:16*

I just warm tube enf up in a cup of freshly boiled water ,then gently push on ,then using small cable tie clamp it..Glass tubes can not take much pressure so metal  hose clamps are out.i also change the flow switch,so that it is on the outlet of the tube .I do this because if it was on the inlet and a hose split or came off the laser sill fires until water runs out.

When it is on the outlet of tube if a tube splits or comes off the laser will stop firing g straight away


---
**Don Kleinschnitz Jr.** *September 29, 2017 11:10*

#K40Cooling


---
**Space Ronin** *September 29, 2017 13:15*

Sorry I forgot to mention stock tubing could differ machine to machine. Mine is this super rubbery silicon it almost feels like latex tubing used in a hospital. Was thinking of just normal silicon aquarium tubing. 


---
**Andy Shilling** *September 29, 2017 14:25*

I use 6mm silicon hose designed for car engines, excellent at handling heat but is also very very strong.




---
*Imported from [Google+](https://plus.google.com/103130365677659082439/posts/NN4EJDdQw9s) &mdash; content and formatting may not be reliable*
