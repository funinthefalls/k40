---
layout: post
title: "I need a adhesive foil to glue a veneer laser cut to an iphone"
date: July 26, 2016 11:48
category: "Material suppliers"
author: "Jennifer Hejna"
---
I need a adhesive foil to glue a veneer laser cut to an iphone. Does anybody have an idea where and how to find that ?





**"Jennifer Hejna"**

---
---
**Scott Marshall** *July 26, 2016 16:06*

I buy copper foil from a firm on ebay, I use it for RF shielding, but I understand it's used for stained glass work as well. I'd  expect those outlets would selll aluminum tape as well. 

Aluminum duct tape is used for sealing HVAC ducts and is normally found in a roll of 2"wide at your local homecenter.



Scott


---
**Jennifer Hejna** *July 27, 2016 07:45*

thank you scott!


---
*Imported from [Google+](https://plus.google.com/110073971503012766297/posts/FLAi1swubUv) &mdash; content and formatting may not be reliable*
