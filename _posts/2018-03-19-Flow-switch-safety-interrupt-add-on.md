---
layout: post
title: "Flow switch safety interrupt add on"
date: March 19, 2018 19:24
category: "Modification"
author: "HalfNormal"
---
Flow switch safety interrupt add on




{% include youtubePlayer.html id="I5lgv0rzeaY" %}
[https://youtu.be/I5lgv0rzeaY](https://youtu.be/I5lgv0rzeaY)





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *March 19, 2018 21:15*

How about a display that shows flow rate :) You sharing the code??



BTW: I saw this show up in my phone alerts but it is not in Laser Cutting and Engraving community page. 

I also notice that below this post is another post; <i>K40x axis problem</i>, that is also not in the main forum ....



...WTH!


---
**HalfNormal** *March 19, 2018 21:30*

Not sure if that's because you're following me or not. G+ can be a pain sometimes! Oh yeah if you follow the link on the video it has the code and you'll see why I'm not doing flow rate.


---
**Stephane Buisson** *March 20, 2018 18:39*

post was in spam box,  I release it (**+Don Kleinschnitz** you should be able to do it as well, as I am not here everyday, I try my best)


---
**Stephane Buisson** *March 20, 2018 18:44*

like that mod  (-> todo list)


---
**Don Kleinschnitz Jr.** *March 20, 2018 19:11*

**+Stephane Buisson** Heh, I did not know there was a spam box.... I will look each day when I pick up my posts. No notification of spam I guess??


---
**Stephane Buisson** *March 21, 2018 06:54*

**+Don Kleinschnitz** a green dot appear in the green 'Manage" button, to catch your attention. I will leave the next spam for you.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/4Dkx7msFXu2) &mdash; content and formatting may not be reliable*
