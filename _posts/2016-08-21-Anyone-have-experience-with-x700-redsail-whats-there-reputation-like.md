---
layout: post
title: "Anyone have experience with x700 redsail. whats there reputation like"
date: August 21, 2016 14:14
category: "Discussion"
author: "Tony Schelts"
---
Anyone have experience with x700 redsail. whats there reputation like. I think thats what it is a 60w model





**"Tony Schelts"**

---
---
**William Steele** *August 21, 2016 14:44*

I've been using the x700 for years now... no issues at all with it.


---
**Jim Hatch** *August 21, 2016 14:53*

Yep, they're basically a step up from the K40 but can have all the same issues - poor wiring, misalignment and of course they're only 50 or 55W machines. If it doesn't have the laser tube extension box sticking out the back side it probably isn't a real 60W tube - those are 1100-1150mm long depending on the quality of the tube (higher quality tubes can be on the shorter side). 


---
**William Steele** *August 21, 2016 14:58*

They have vastly superior electronics... good stepper controllers and software that is somewhat better (though not perfect, but very usable.)  The rated power on it is actually only listed at 50 watts... which matches the tube that was installed.  The 60 watt version does have the extended chassis box.  We have one of each.


---
**Jim Hatch** *August 21, 2016 15:11*

Ours had everything wired through the emergency shutdown switch with low gauge wire - that was getting hot. We had been warned of switch melting issues so when we saw what was happening we rewired properly. Also the water flow sensor wasn't wired in. The tube lasted about 8 months but that was okay as we could get a new full power one. We had the extension box as well. We did upgrade the PSU so we can bump it again if we want to put in an 80 or 100W tube if we see enough use out of it.


---
**William Steele** *August 21, 2016 16:00*

Curious, was your actually a RedSail or a clone of it?  The RedSail devices have a big logo on the front indicating it and are normally high quality.  There are a lot of eBay sellers selling clone machines that look nearly identical.  The only way to be sure is to buy directly from RedSail in China or their only US distributor, CNCCheap on eBay.


---
**Tony Schelts** *August 21, 2016 18:06*

Im in the uk and ebay are selling something that looks identical. [http://www.ebay.co.uk/itm/60W-CO2-Laser-Cutter-Engraver-Laser-Cutting-Engraving-Machine-45-7-X34-x37-7-/301815748646?hash=item46459ed826:g:xycAAOSwvg9XW9mV](http://www.ebay.co.uk/itm/60W-CO2-Laser-Cutter-Engraver-Laser-Cutting-Engraving-Machine-45-7-X34-x37-7-/301815748646?hash=item46459ed826:g:xycAAOSwvg9XW9mV) just wanting to find out how good they were?  Sold out of germany


---
**Jim Hatch** *August 21, 2016 20:25*

**+William Steele**​ Good question. We got it a year or so ago from a distributor here in the US but I thought he was from PA. Don't think it was CNCCheap but we didn't go through eBay. It was a lead from someone who is in the laser cutting/engraving service business.


---
**Tony Schelts** *August 21, 2016 20:29*

Thanks Jim anyway im sure it'll be better than k40.


---
**Jim Hatch** *August 21, 2016 20:46*

**+Tony Schelts**​ For sure. You can actually get the same person to respond to you more than once. You're not getting the manufacturer du jour. 🙂


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/6z5qPtixApB) &mdash; content and formatting may not be reliable*
