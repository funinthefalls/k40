---
layout: post
title: "Starting the chiller"
date: July 07, 2017 01:29
category: "Object produced with laser"
author: "William Kearns"
---
Starting the chiller 

![images/4c56923d648bc56f86c6979fa6d066e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c56923d648bc56f86c6979fa6d066e4.jpeg)



**"William Kearns"**

---
---
**Alex Krause** *July 07, 2017 03:09*

Make sure you don't get condensation forming on the laser tube


---
**Sl33pydog** *July 07, 2017 04:03*

Watching this closely. I'm going to try something really similar to you.  I hope you get good results.


---
**William Kearns** *July 07, 2017 06:48*

**+Sl33pydog**  just ordered a dew point sensor and will set the controller couple degrees above the dew point in the room


---
**Nitro Zeus** *May 16, 2018 17:12*

Hey brother shoot me a PM I have built a peltier cooler with 4 70 watt modules. I used the same exact water block from alibaba. Kinda funny. Anyways reach out. We are doing the same project!


---
**Nitro Zeus** *May 16, 2018 17:13*

I had a meanwell 35 amp 12 volt supply and it died.. I cant figure it out. So I ended up using two ATX power supplies. Its a bit of a rats nest with the wiring, but it's almost sorted out.


---
**William Kearns** *May 23, 2018 01:08*

i ended up having to do 4 as well. 2 just wouldn't keep up and I am using a ATX power supply as well. I get them from the computer graveyard at work.


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/aVyzVpyrB8e) &mdash; content and formatting may not be reliable*
