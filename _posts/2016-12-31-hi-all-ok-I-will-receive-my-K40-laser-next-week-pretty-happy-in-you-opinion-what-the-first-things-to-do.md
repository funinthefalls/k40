---
layout: post
title: "hi all; ok I will receive my K40 laser next week ( pretty happy ) in you opinion what the first things to do ?"
date: December 31, 2016 16:32
category: "Discussion"
author: "J. DTRTR"
---
hi all; 

ok I will receive my K40 laser next week ( pretty happy ) 

in you opinion what the first things to do ? 

upgrade ? air assist ? to do ? 

thanks for your advice 

Julien 













**"J. DTRTR"**

---
---
**Don Kleinschnitz Jr.** *December 31, 2016 16:52*

I would verify everything works with the standard setup.


---
**K** *December 31, 2016 17:47*

After you verify that everything works properly, you may consider adding an interlock switch so that the laser doesn't fire when the lid is open. Also, be sure it's grounded properly. 


---
**Ashley M. Kirchner [Norym]** *December 31, 2016 19:00*

Go over the machine with a fine comb. Check, and re-check everything, connections, alignment, metal walls that have been bent out of shape, leveling the bed, check for proper grounding, check for water leaks. After that, air assist will soon be on the top of the list, possibly followed by replacing the controller for a better one, and other hardware upgrades. I've had my machine for a year and some months and I've only added air assist so far. I'm just now getting ready to upgrade the controller (awaiting delivery.) So you can most definitely run as-is, or with minimal additions/upgrades.


---
**J. DTRTR** *December 31, 2016 19:44*

ok that's some good news ( the fact that it can be used out off the box ) I plan air assist and switching to grbl 1.1 and laserWeb in next future but for now i'll just use it as is since it seem to be totally possible


---
**Ned Hill** *December 31, 2016 19:57*

At the top the list upgrade wise is an air assist (eliminates flare ups and keeps the bottom of the lens clean).  Check the mirror on the lens head and make sure it's in good condition.  I and a number of other people have received mirrors that had some severe galvanic corrosion on them due to the machine being packaged with residue water in the laser tube.  I replaced all my mirrors with good quality ones and got a good 20mm lens to go with my air assist head from the start and haven't looked back.  You'll probably want to also upgrade the water pump to a better one and get a flow cut off switch to prevent the laser from firing if there is no flow as that will kill your laser tube.  You are also probably going to want to remove or cut down the interior exhaust vent to increase your work area and alse create a better bed to work with (an expanded metal sheet or aluminium honeycomb).  Optimize those things and then you can move on to a better controller board after you get some familiarity with your machine.


---
**Ashley M. Kirchner [Norym]** *December 31, 2016 20:46*

You can most certainly use it out of the box, but you'll have to account for some adjustments here and there. Out of the box, you will soon discover the exhaust port is in the way of using the full bed. I ended up removing the whole darned thing. Some folks will just cut it back a bit. Out of the box, my gantry was way out of square and I had to pull the whole thing out to fix that. Shortly after that, I added the air assist. I replaced my mirrors (haven't done the lens yet.) And now I'm waiting for the replacement open source controller. Little steps. 


---
**Robi Akerley-McKee** *January 01, 2017 03:10*

**+J. DTRTR** Smoothieware is a much better choice than GRBL even a RAMPS is better than GRBL 


---
*Imported from [Google+](https://plus.google.com/101405162141132297916/posts/7c8p2YVPhz7) &mdash; content and formatting may not be reliable*
