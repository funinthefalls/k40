---
layout: post
title: "What's the best tube mount that's 3d printable?"
date: February 01, 2018 18:13
category: "Modification"
author: "Andy Shilling"
---
What's the best tube mount that's 3d printable? I'm not convinced the ones I printed a year ago are better than stick but probably not the best out there. 



What are you using and could you supply links please.





**"Andy Shilling"**

---
---
**HalfNormal** *February 02, 2018 04:09*

This was the last one that I printed up but never installed due to selling the unit.

[thingiverse.com - K40 40W CO2 laser tube hanger by Crashoverride](https://www.thingiverse.com/thing:2214741)


---
**HalfNormal** *February 02, 2018 04:10*

![images/2dea890d3b78ba9de34b344cc9492647.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2dea890d3b78ba9de34b344cc9492647.jpeg)


---
**Andy Shilling** *February 02, 2018 07:07*

That looks nice and simple but knowing how well made these machines are made I wonder how many shims I might need to get the correct hight.


---
**Don Kleinschnitz Jr.** *February 02, 2018 12:02*

[https://www.thingiverse.com/thing:2259794](https://www.thingiverse.com/thing:2259794)


---
**Andy Shilling** *February 02, 2018 12:07*

**+Don Kleinschnitz** I'm just printing the other one **+HalfNormal** posted but that was the one I was looking at. I guess I'll just have to try both and see which one fits in my extension tube.

![images/37864b959a2533864f733189f82c310f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/37864b959a2533864f733189f82c310f.jpeg)


---
**HalfNormal** *February 02, 2018 12:18*

The picture I took of it does not show it correctly. That top piece actually rotates 180 degrees and that is your height adjustment.


---
**Andy Shilling** *February 02, 2018 12:20*

**+HalfNormal** right I get it, left and right but the fixing bolts to the chassis and height but turning that top but upside down. Thanks that'll do nicely


---
**Don Kleinschnitz Jr.** *February 02, 2018 12:56*

Just a note: 

I was in the back of my machine replacing a leaking coolant hose on the output side of the tube [that was not a fun exercise] and I noticed that the stock mounting of the tube leaves it quite "springy". 



Its because the tube mounts are lined with rubber! I wonder if this is part of the constant need for alignment? I have never seen an optical system were the light source is shock mounted be successful?



<b>Have any of you that changed out the mounts noticed better optical stability and less need to realign optics?</b>



I am still thinking that the laser should be mounted on a sub-frame that is installed into the k40 cabinet as a modular unit.... perhaps when I go to 50-60 watts I can make one that slides in from the right to left of the cabinets cavity :). Oh  shit.... doing it again.... can't leave good enough alone!


---
**HalfNormal** *February 02, 2018 12:58*

**+Andy Shilling** here is the correct orientation.

**+Don Kleinschnitz** notice the box?!



![images/a9446c11d73dad49acc4814424fc40be.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a9446c11d73dad49acc4814424fc40be.jpeg)


---
**Andy Shilling** *February 02, 2018 13:02*

**+HalfNormal** that looks perfect, I'm running it at 40% infill so will take a little while to do but hopefully be ready when the rest of the machine is rewired and new extraction position is sorted.


---
**Nigel Conroy** *February 02, 2018 13:52*

**+Don Kleinschnitz**

Are you thinking of something like the mount in this video?


{% include youtubePlayer.html id="HuNZu1-BXuY" %}
[youtube.com - RDWorks Learning Lab 119 One Piece Tube and Mirror Mount INSTALL](https://www.youtube.com/watch?v=HuNZu1-BXuY)




---
**Don Kleinschnitz Jr.** *February 02, 2018 14:03*

**+Nigel Conroy** exactly that!


---
**Dennis Luinstra** *February 10, 2018 03:59*

Nice! Did you print the mount using pla or abs? Here in Australia it can be very hot and when the laser is not used, just stored in the workshop (40+  degrees Celsius) would pla give any problems / deformation? 


---
**HalfNormal** *February 10, 2018 04:02*

PLA. With a melting point of 190C, I am sure that 40C is safe!


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/BtfMUXazVdr) &mdash; content and formatting may not be reliable*
