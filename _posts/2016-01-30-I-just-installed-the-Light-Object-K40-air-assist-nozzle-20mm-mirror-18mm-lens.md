---
layout: post
title: "I just installed the Light Object K40 air assist nozzle (20mm mirror, 18mm lens)"
date: January 30, 2016 19:01
category: "Modification"
author: "Anthony Bolgar"
---
I just installed the Light Object K40 air assist nozzle (20mm mirror, 18mm lens). What should the distance be from the tip of the nozzle to the work piece?





**"Anthony Bolgar"**

---
---
**Stephane Buisson** *January 30, 2016 19:49*

Focal lens is focal lens, depending on your lens (most probably 50.8mm)

distance is between bottom of the lens and mid thickness of material (cut), or top if engrave. Make your calculation with your new head.


---
**Anthony Bolgar** *January 30, 2016 19:50*

I will have to take the nozzle apart and use a micrometer to measure the lens' exact location in the head, then work it out from there. I was hoping someone else had already done this.


---
**Pete OConnell** *January 30, 2016 20:31*

Wooden skewer is what I used, stuck it in the end till it touched the lens and then measured that


---
**Anthony Bolgar** *January 30, 2016 21:46*

Thanks, I will try the skewer trick.


---
**Tony Schelts** *January 30, 2016 23:38*

My laser head a lens has got lost in the post.  :(


---
**Anthony Bolgar** *January 31, 2016 05:17*

Which orientation should the lens be?


---
**I Laser** *January 31, 2016 09:40*

Concave facing up.


---
**HalfNormal** *January 31, 2016 16:57*

This is one of the best videos explaining how to focus the K40 with the air-assist.


{% include youtubePlayer.html id="vuW8fi6yMto" %}
[https://www.youtube.com/watch?v=vuW8fi6yMto&list=PLInTrkIbj69kPO_UP81yxX-xov4SVyPlD&index=16](https://www.youtube.com/watch?v=vuW8fi6yMto&list=PLInTrkIbj69kPO_UP81yxX-xov4SVyPlD&index=16)


---
**Steve Prior** *October 05, 2016 05:08*

Can anyone confirm the lens orientation?  Above I Laser says concave up, but the video by DIY3DTech says convex up.


---
**HalfNormal** *October 05, 2016 12:25*

**+Steve Prior**​ definitely convex up!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/56gb1ciqAZj) &mdash; content and formatting may not be reliable*
