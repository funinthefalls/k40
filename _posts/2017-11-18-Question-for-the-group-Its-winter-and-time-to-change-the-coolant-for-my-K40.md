---
layout: post
title: "Question for the group. It's winter and time to change the coolant for my K40"
date: November 18, 2017 04:06
category: "Discussion"
author: "Alex Raguini"
---
Question for the group.  It's winter and time to change the coolant for my K40.  I'm already using deionized water.  I've read of people adding some chlorox to keep stuff from "growing" in the water, anti-freeze to prevent freezing, etc.



My question is, what is everyone doing to their coolant water to keep it clean and prevent it from sapping energy from the laser?



I've had my k40 a little over 5 weeks.  The laser is in my unheated workshop where outside temperatures can drop below freezing.  I'd appreciate any suggestions.







**"Alex Raguini"**

---
---
**Joe Alexander** *November 18, 2017 05:31*

beware with antifreeze as it can increase the amount of current bleed into your reservoir. See more info at [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com) for detailed numbers.


---
**Don Kleinschnitz Jr.** *November 18, 2017 10:37*

Do not use antifreeze during operation. You have to either heat and circulate the water or drain it if you are not using it.

Some use aquarium heaters to heat and control the coolants temp in winter, you can search the community for that info and others will chime in.


---
**Alex Raguini** *November 18, 2017 15:13*

what about the Clorox suggestions I've read.  I want to keep the system clean but don't want current loss either.

 


---
**Lars Andersson** *November 18, 2017 18:41*

What is the current path from the gas discharge inside the tube to the cooling water? Is it through the glass wall?


---
**Mark Brown** *November 18, 2017 23:52*

Yes, the laser is running at high enough voltage to overcome the dielectric strength of the glass.  Fun stuff starts happening at thousands of volts.


---
**Don Kleinschnitz Jr.** *November 19, 2017 02:42*

**+Mark Brown** We dont exactly know the mechanism I think the tube and water act like a big capacitor at this voltage.


---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/XD6g1wARotR) &mdash; content and formatting may not be reliable*
