---
layout: post
title: "Got my smoothie hardware and a new Kangaroo mini windows 10 computer to run it"
date: November 17, 2015 03:56
category: "Discussion"
author: "David Cook"
---
Got my smoothie hardware and a new Kangaroo mini windows 10 computer to run it.  I plan to mount the kangaroo inside the laser cabinet. Should be up and running within a week now 

![images/d0a9edbc395a69d39899521820480f9f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d0a9edbc395a69d39899521820480f9f.jpeg)



**"David Cook"**

---
---
**I Laser** *November 19, 2015 22:06*

Now that is sweet! Ultimate all in one laser cutter, good job!


---
**David Cook** *November 19, 2015 22:15*

Thanks,   I just bought a small touch screen HDMI touchscreen to use with it as well   [https://www.adafruit.com/products/2260](https://www.adafruit.com/products/2260)


---
**Arestotle Thapa** *January 31, 2016 03:13*

**+David Cook** Were you able to get that board to work?


---
**David Cook** *November 29, 2016 17:56*

**+Arestotle Thapa** Yes, absolutely.  works great with good power control to the Laser


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/LN2PPCR2S9K) &mdash; content and formatting may not be reliable*
