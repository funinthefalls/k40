---
layout: post
title: "hi all, I feel like a burden but I have yet another question/problem I am having with this 'to-be' fantastic machine =] have been helping my gf cut card tonight via CorelDrawx7 (imported .svg files generated from her"
date: June 11, 2016 14:09
category: "Software"
author: "Pippins McGee"
---
hi all,

I feel like a burden but I have yet another question/problem I am having with this 'to-be' fantastic machine =]



have been helping my gf cut card tonight via CorelDrawx7 (imported .svg files generated from her silhouette cameo program. Well, from a website that converted the .studio3 files into .svg files)



Most of the cuts work fine, but some of them such as the one in image have big problem.

The laser head takes off and goes crashing into the bottom of the table at a certain point throughout the cutting process.

If I change the cut setting to 'inside first' as opposed to 'as drawing' for eg. it will get to a different point in the cutting sequence before doing the same thing, so it isn't a particular object in the file causing it to get confused. It keeps trying to cut once it has hit the bottom of the table, it's basically losing complete track of where it's supposed to go to do the cutting. Thinking the table is 30cm lower than where it actually is in a way.



I've seen a video of it happening to someone else but no solution was posted. They were on Moshi also so I figured it wouldn't happen to me being on the m2 nano / Corel...



Any ideas please?



I get around it by stripping the file into two halfs and pasting half the objects in one page and half on another page and making two separate cuts but I shouldn't have to do this.



any ideas welcome

Cheers all,

![images/5b49bb263b79f84eb9bc81b4e6a8ff47.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5b49bb263b79f84eb9bc81b4e6a8ff47.jpeg)



**"Pippins McGee"**

---
---
**Anthony Bolgar** *June 11, 2016 15:33*

Is it only this file or does it happen with other files? And are you running the stock controller?


---
**Pippins McGee** *June 11, 2016 15:49*

**+Anthony Bolgar** stock controller stock everything.

tonight I import 4 different svg files of similar nature (only thing different about this file being imported I guess is it is the most 'crowded' in terms of number of seperate objects.)



This was only file that the machine went haywire whilst cutting.

Whilst I can get around it essentially, it ruins my confidence that the machine can do something like this and that I don't understand why when theremust be a technical reason to explain the machines decision to go off course.


---
**Anthony Bolgar** *June 11, 2016 16:32*

Could be a buffer overflow issue. Since it is the most crowded file you used, it will generate the most lines of Gcode. The controller serial buffer may not be able to handle that large of a file or maybe the speed it is being sent from the PC to the controller.


---
**Jim Hatch** *June 11, 2016 18:52*

**+Anthony Bolgar**​ maybe buffer, but that's not a particularly complex file. My Catan boards have more design elements and path changes in one playing piece (out of the dozen it does on one board). There's something else wrong. 



**+Pippins McGee**​ What happens if you use a different file format like DXF instead of the SVG? 


---
**Miguel Sánchez** *June 11, 2016 18:52*

While I do think nothing excuses the absolutely miserable performance of Moshi software I have managed to cut some "reluctanct" files by simplifying them. Fewer points in lines may make the difference between controller getting mad or doing its job. I gave up on multipart files, so when I needed to cut five parts that fit nicely into a small space I ended up doing each one of them from a different file (or repetitions of the same one if they were similar). That is why I will be replacing the controller very soon.


---
**Jim Hatch** *June 11, 2016 18:58*

**+Miguel Sánchez**​ I have a Nano based laser (haven't gotten the time to do my Smoothie/LaserWeb upgrade yet) and do complex Catan boards and map engravings. But I don't use SVG as a file format - it's either AI or DXF. 



I still do the bulk of my work in Illustrator but am okay with Corel. I may upgrade to the latest Corel and just use that going forward. It's supposed to be better from a CAD/CAM/CNC orientation than Illustrator. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 12, 2016 01:52*

I dont think it is related to buffer as I have done extremely complicated cut files with thousands of objects (for my vector "mock" engrave cutting method).



I'd check your Device Initialise Settings for the Corel Laser plugin where it allows you to set PageSizeX & PageSizeY. Make sure they are no bigger than 320 x 200. See: [https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)



If the Y is set too high that might be what is causing your issue. Although from what you say, I imagine the file dimensions are within the bounds & something is going awry during the process. Another suggestion I have is to check that all the paths are closed in the SVG. It's quite frequent that unclosed paths cause strange behaviour (sometimes cutting extra lines).



Also, what speeds are you using? I find that with high speeds while cutting (>30mm/s) can cause the machine to vibrate a lot, skip steps, etc. So I guess checking your belt tensions could be another thing.



Any chance you could film what is happening?


---
**Pippins McGee** *June 12, 2016 11:56*

**+Yuusuf Sallahuddin** **+Jim Hatch** 

Ok well.. I think I found the cause..



The imported svg file, whilst visually looking like it fits within the 300x200 Corel Page, has an object way way way way off the bounds of the page. Have to zoom right out until looking at the page from space and I see an object like a meters below the page coordinates.



I didn't think to look out of the page boundaries as 1) I just assumed the file wouldn't have something silly like that, 2) having the page setup to 300x200 I assumed even if you put an object out of the page bounds, it would be ignored, and not cause the laser to try and travel over to it (hitting the end of the table) past the page dimensions set.



it hasn't happened since I removed that object so.. I think that was it.. Or this was just a coincidence, in any case if it happens again and this wasn't the reason I'll post back a video.



but for now! thank you, I didn't think to zoom right out until I read your responses.

i may have been too quick to assume the worst in the machine haha, most likely human error.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 12, 2016 13:13*

**+Pippins McGee** That's good that you got it sorted. One thing to note is that it actually doesn't matter what size the document is in Corel Draw. I just use whatever the default document size is & always place my objects to engrave/cut/etc outside the bounds of that document with no issues cutting/engraving them. Sometimes I have like 20 layers in separate areas outside the main document bounds & they all work fine from where they are located. I guess in future, before engraving/cutting, a quick way to prevent issues like what you had with the stray object 1m below the rest would be to select all (CTRL+A) & then group them (CTRL+G). Once grouped you should be able to see the dimensions of the grouped objects, which would let you know if there are any strays (as it would be a weird size). Might be an odd occurrence with the Silhouette Cameo software when it exported the SVG, or when Corel imported it. I have no issues with SVGs that I created in AI, so I'd hazard a guess it's something to do with that Silhouette Cameo software since you mentioned it happened on a few different files.


---
**Miguel Sánchez** *June 12, 2016 16:17*

Every time I assumed I had found a workaround for the faults of my K40 I have been later proven wrong (after two years of ocasional use). I have experienced exactly the type of problem you mentioned with known-good DXF files. So while it may well be that your case was due to user error I am positive there is a failure mode for ok files that looks exactly the same as yours. The reason for it or how to avoid is one of the reasons I am on this list. However, I am not holding my breath.


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/K9HfKCGbdAt) &mdash; content and formatting may not be reliable*
