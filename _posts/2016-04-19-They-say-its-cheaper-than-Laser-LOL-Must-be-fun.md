---
layout: post
title: "They say it's cheaper than Laser LOL. Must be fun"
date: April 19, 2016 17:45
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
They say it's cheaper than Laser LOL. Must be fun



[http://paintwithgunpowder.com/](http://paintwithgunpowder.com/)

![images/30d3f93f4496a72123bf125a513b8c68.png](https://gitlab.com/funinthefalls/k40/raw/master/images/30d3f93f4496a72123bf125a513b8c68.png)



**"Ariel Yahni (UniKpty)"**

---
---
**Scott Marshall** *April 19, 2016 21:53*

Ah, Yeah, look for him in the Darwin awards,



 "He was a good artist, but very poor with common sense....."


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 19, 2016 23:49*

Gives a nice effect. Although I imagine it is more time consuming to do (didn't look at the site yet).


---
**I Laser** *April 23, 2016 10:19*

The other problem with this method is you actually need to be artistic! :P


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/iHVN34tXRiq) &mdash; content and formatting may not be reliable*
