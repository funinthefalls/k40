---
layout: post
title: "Yuusuf Sallahuddin Hello, tomorrow I want to etch some text on the back of my Wifes, Kindle cover"
date: January 20, 2016 18:35
category: "Discussion"
author: "David Cook"
---
**+Yuusuf Sallahuddin** Hello,   tomorrow I want to etch some text on the back of my Wifes, Kindle  cover.  It's a genuine

 leather material.   I do not have enough experience with the power levels yet.  It's her birthday and she will be out for a few hours and I wanted to surprise her.



Would be able to suggest a starting power level and feedrate as well as CW or PPM mode of operation?   Thank you for your advice.



and no,  I will not hold you responsible if I destroy it lol





**"David Cook"**

---
---
**I Laser** *January 20, 2016 20:42*

You might want to make sure it's okay to laser as faux leather usually contains vinyl. Vinyl creates chlorine gas when burnt by the laser. Chlorine gas is not only extremely dangerous (ww1 chemical warfare) but will also ruin your machine.



[http://hackaday.com/2015/03/14/how-to-identify-plastics-before-laser-cutting-them/](http://hackaday.com/2015/03/14/how-to-identify-plastics-before-laser-cutting-them/)


---
**David Cook** *January 20, 2016 20:48*

**+I Laser**   ahh !!  thank you  I had not even thought about that.


---
**David Cook** *January 21, 2016 00:19*

Turns out it IS a genuine leather Kindle case,  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 21, 2016 00:29*

Hi David. Sorry, just checked google now. I am still using stock controller, so I am not sure about PPM or CW.



Basically, for engraves I usually do between 4-8mA power & I always do @ 500mm/s speed. As I assume the leather is not very thick, I would choose 4mA power (or lower if you are able to get the power lower & still mark).



Some things I have engraved I have had to lift the item up closer to the lens (to defocus it a bit) as the engrave even at 4mA can cut all the way through the leather (e.g. my 0.8-1mm kangaroo hide leather). In these cases I usually lift it about 3-4mm (by placing something under the item).


---
**Jim Hatch** *January 21, 2016 03:25*

I always start out very low power & high speed. I can tweak the power while it's cutting if I want it to mark a bit more. If it turns out it's too fast to get a good marking I leave the piece in the machine, change the parameters in LaserDRW and rerun it. Registration is usually spot on so it's not a problem if I have to run it a couple or three times to get it just right. 



Then I write it all down in a little notebook so I have a new starting place the next time I use the same material.


---
**David Cook** *January 21, 2016 04:53*

Thanks for the advice guys !   I just posted my first engraved piece in the other section. Worked great!


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/88dPEkjfpuU) &mdash; content and formatting may not be reliable*
