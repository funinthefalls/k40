---
layout: post
title: "Here are some pictures of my DIY adjustable Z table for my K40"
date: December 13, 2015 02:49
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
Here are some pictures of my DIY adjustable Z table for my K40. It is made of aluminum channel for the base, aluminum angle for the top layer with a steel grate as the material support structure. The corner brackets are 3d printed and the base has strong rare earth magnets to keep it in place in the laser. The threaded rod is 5/16 dia. and the adjustable nuts have a 3d printed casing on them which makes it easy to adjust them up and down to set the height. It cost approximately $25.00 in materials to build it.



![images/131a82c048aca0d9b17bd8b904f5d69c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/131a82c048aca0d9b17bd8b904f5d69c.jpeg)
![images/2727cd13c67e65f74bc6d673f38d0e98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2727cd13c67e65f74bc6d673f38d0e98.jpeg)
![images/190d3e2a9ec86b8136726a085ae3df86.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/190d3e2a9ec86b8136726a085ae3df86.jpeg)
![images/52dcbb5c6e32619da51bfe286d6b44fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52dcbb5c6e32619da51bfe286d6b44fc.jpeg)

**"Anthony Bolgar"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2015 06:27*

That's pretty cool. I was considering something similar, but thought I may have issues with raising it & keeping it level. Maybe some small string line levels on the bed would make it easy to keep it level.


---
**Anthony Bolgar** *December 13, 2015 08:08*

I will be laser cutting some acrylic stepped gauge blocks in 1mm increments per step to set the height at each corner


---
**Scott Thorne** *December 13, 2015 10:49*

That's what I did with my honeycomb table....I have 2 6 inch long 2 1/2 square tubing in the bottom with 1/4 and 1/8 spacers to sit on top of the tubing to get the table to the right hight....I have sharpie marks on the xy frame so I know when I'm at the right height...works well for me.


---
**ChiRag Chaudhari** *December 13, 2015 18:38*

looks great man. like the idea to use magnets, its so simple and clever.


---
**Scott Marshall** *December 18, 2015 17:56*

While in Walmart the other day, I ran across a material ideal for this, a lightweight expanded metal about 16 x 16", 3 pieces for around $4.

It's a package of disposable grill grates. They're somewhat dished, but could easily e cut down to a 14 x 14 flat section, and a lot cheaper/more available than standard expanded metal, which is insanely expensive in small sections, and much heavier than we need for this application.



Slid into the frame, it would be disposible once it got chewed up and dirty.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 20:52*

I wonder if the 3d printed brackets on the corners are actually required? From looking at the pics it seems they are used to hold the side L channels in place? Is that the only purpose they serve? I like the overall simplicity of the design, just trying to work out if I can use something else instead of the 3d brackets (as I have no 3d printer).


---
**Anthony Bolgar** *May 10, 2016 13:39*

You can just use metal L brackets


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 10, 2016 15:02*

**+Anthony Bolgar** Cool. Thought that might be the case, but wasn't sure if there was an extra purpose to the red ones. Thanks for that.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/iFrSt2bLmS9) &mdash; content and formatting may not be reliable*
