---
layout: post
title: "Hi all, I'm new to the community, I have just purchased my k40 laser cutter and am looking forward to solving all of the issues that have been posted on this community"
date: December 16, 2015 09:03
category: "Hardware and Laser settings"
author: "Anthony Santoro"
---
Hi all,

I'm new to the community, I have just purchased my k40 laser cutter and am looking forward to solving all of the issues that have been posted on this community.

I'm sure that this is noted somewhere in the discussion boards, but I couldn't find it - what is the maximum current draw from these machines?

I am living in Australia, we have 240VAC mains supply. I am building a small control panel so that my laser cutting will be 'idiot proof'. The control panel will be built such that the laser cutter can't be powered on until both the pump and the fan are running, time to get my inner electrician out. 

In the control panel I am going to put in a circuit breaker and emergency stop button, but I just want to check what current rating you would recommend. Do you think these machines will draw more than 3A at full load on 240V? Is a 10A circuit breaker suffice? 



Thanks guys! I look forward to participating in this community in the future!



Anthony

Australia





**"Anthony Santoro"**

---
---
**Stephane Buisson** *December 16, 2015 11:53*

all depend on your mod, but if you use the original PSU, 3 Amp is plenty !


---
*Imported from [Google+](https://plus.google.com/106649411786258336023/posts/gHtWqFQMH6b) &mdash; content and formatting may not be reliable*
