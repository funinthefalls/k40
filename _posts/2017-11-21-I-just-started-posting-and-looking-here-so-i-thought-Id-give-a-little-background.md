---
layout: post
title: "I just started posting and looking here so i thought I'd give a little background"
date: November 21, 2017 05:15
category: "Object produced with laser"
author: "James Lilly"
---
I just started posting and looking here so i thought I'd give a little background.  I am a fine artist who creates faux finished wall sculptures now using a laser cutter to do most of my cutting.  It became apparent that I would need a cutter of my own so thus the K40 experiment.  Here is an example of my work.

![images/3bd8c440456a86f542992614ce14433d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3bd8c440456a86f542992614ce14433d.jpeg)



**"James Lilly"**

---
---
**Ned Hill** *November 21, 2017 05:22*

Welcome to the community.  Very cool piece.  Will love to see more of your work.


---
**James Lilly** *November 21, 2017 05:28*

[www.jameslillyart.com](http://www.jameslillyart.com)




---
**Paul de Groot** *November 21, 2017 05:38*

Love your work, amazing !


---
**Phillip Conroy** *November 21, 2017 05:47*

Outstanding work


---
**James Lilly** *November 21, 2017 05:49*

Thanks.  I truly appreciate the advice over the last few days so that I can get back up and rolling.


---
**HalfNormal** *November 21, 2017 12:56*

Great work and inspiration! 


---
*Imported from [Google+](https://plus.google.com/114642009939368433927/posts/GqhJH3PWMGN) &mdash; content and formatting may not be reliable*
