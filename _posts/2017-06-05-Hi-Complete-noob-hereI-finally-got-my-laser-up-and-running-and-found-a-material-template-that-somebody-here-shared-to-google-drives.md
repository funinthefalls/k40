---
layout: post
title: "Hi, Complete noob here...I finally got my laser up and running and found a material template that somebody here shared to google drives"
date: June 05, 2017 02:12
category: "Materials and settings"
author: "Tony Marrocco"
---
Hi,



Complete noob here...I finally got my laser up and running and found a material template that somebody here shared to google drives.  My issue is when I import it to Laserweb it pops up with "file didn't load right" or something like that and all the text is gone.



While it's in Inkscape it looks perfectly fine. Do I have to take each and every little piece and make it it's own layer manually in inkscape and then import it to laserweb? Here's the template link [https://drive.google.com/open?id=0BwnmNmBaSoaGMHQwXzRQdW11YUk](https://drive.google.com/open?id=0BwnmNmBaSoaGMHQwXzRQdW11YUk)





**"Tony Marrocco"**

---
---
**Ashley M. Kirchner [Norym]** *June 05, 2017 03:16*

Looking at the file in Illustrator, there are several things wrong with it. So I posted a new one on my GDrive: [https://drive.google.com/open?id=0B8NExy6DoX2RY2dqUEF0ZF9xd0k](https://drive.google.com/open?id=0B8NExy6DoX2RY2dqUEF0ZF9xd0k)


---
**Tony Marrocco** *June 05, 2017 04:07*

**+Ashley M. Kirchner** Thank you so much!! One day hopefully I'll learn how to fix things like that.  I think I can figure out how to work this thing now.


---
**Ashley M. Kirchner [Norym]** *June 05, 2017 06:09*

I uploaded version 2 because I forgot to convert the gradient wheel to a raster image.


---
*Imported from [Google+](https://plus.google.com/112686049554441919913/posts/2wBqFZFNWjk) &mdash; content and formatting may not be reliable*
