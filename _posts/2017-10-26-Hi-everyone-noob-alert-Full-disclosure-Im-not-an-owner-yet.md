---
layout: post
title: "Hi everyone... noob alert. Full disclosure Im not an owner yet"
date: October 26, 2017 17:16
category: "Discussion"
author: "Jimmy G"
---
Hi everyone... noob alert. Full disclosure Im not an owner yet.

Have read a bit and looked at many vids on youtube but cant seem to get any real idea whether the K40 is capable of cutting paper cleanly without scorching it. I seems to depend on the cut out point of the laser and the max forward cutting speed. Im not interested in etching or cutting wood just paper or light card.

Also air assist would be needed and perhaps some other mods? It seems its at the limit of what the K40 can do?

Can I ask users experiences?



Jimmy





**"Jimmy G"**

---
---
**BEN 3D** *October 26, 2017 21:30*

Hi Jimmy,



i just starting to use my k40 and also interested in what can it cut. Post an example here and I will try to cut it. 



Chears Ben


---
**Jimmy G** *October 26, 2017 21:48*

Wow thats amazing help Ben thanks sooo much for the offer.

I will work ip a sample file tomorrow or over the weekend and upload it somewhere.



Jimmy


---
**Ned Hill** *October 26, 2017 22:59*

You will definitely need an air assist or you will have a fire cutting paper.  When cutting paper the trick is to find the right power/speed settings that just barely cut the paper.  You will still have some slight charring along the edges, the laser is burning the paper after all, but it's usually easy to brush any char off.


---
**Jimmy G** *October 27, 2017 10:25*

Does it usually disappear completely Ned?

Its pretty important for my application as the paper will be white.



Jimmy


---
**Jimmy G** *October 27, 2017 14:40*

**+BEN 3D** Hi Ben, heres a sample .eps file I found online which you could use perhaps. It should give you some idea of how it will go with my application. I will use 100 to 120gsm paper ideally..

[my.pcloud.com - sample.zip - Shared with pCloud](https://my.pcloud.com/publink/show?code=XZFtyV7ZoNLZaK6cvijUGYkeDvblIH2UnydgBC1X)



Really want this to work.

Thanks again.



Jimmy


---
**BEN 3D** *November 14, 2017 17:10*

Hi **+Jimmy G**, I did not forget you, I already tried to open your file with the onstock software laser dwr3, but it seems to be incompatible. I also tried inkskape and there should be a workarround to import. I plan to convert it to svg and do my first cuts with k40 whisperer, hopefully I could cut it this weekend.


---
**Jimmy G** *November 14, 2017 19:13*

Thanks ben.


---
**BEN 3D** *November 21, 2017 23:34*

Hi Jimmy G, 

I create my first paper cut today, next I convert your file to inkscape svg. 

![images/d2d7952eb9c8892a94b4a909ca8668b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d2d7952eb9c8892a94b4a909ca8668b2.jpeg)


---
**BEN 3D** *November 21, 2017 23:35*

Here a german video of my first try. 
{% include youtubePlayer.html id="9zPgPYWUWZw" %}
[https://youtu.be/9zPgPYWUWZw](https://youtu.be/9zPgPYWUWZw)


---
**BEN 3D** *November 22, 2017 20:39*

I did not found a way to convert your file on my windows device, but I found online page [convertio.co](http://convertio.co) I opened the resulting svg with inkspape and set it as cuting red wirhout infill.



I used a 0.1 mm thin paper that I have had laying arround. 



My laser was set to 4 mA and here a Video of the cut

![images/946a2768057fd1359d2818e7cda5bf25.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/946a2768057fd1359d2818e7cda5bf25.jpeg)


---
**BEN 3D** *November 22, 2017 20:40*

And a picture

![images/58ee175f555f1bdebfbd580c35c26913.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58ee175f555f1bdebfbd580c35c26913.jpeg)


---
**BEN 3D** *November 22, 2017 20:42*

And once more  FYI +Jimmy G

![images/b430e1811ecbef1ce55e12422f65278a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b430e1811ecbef1ce55e12422f65278a.jpeg)


---
*Imported from [Google+](https://plus.google.com/115435642430478571255/posts/F2oPBmmNMdY) &mdash; content and formatting may not be reliable*
