---
layout: post
title: "Hello there, I'm slowly thinking about jumping in the laser bandwagon"
date: April 11, 2016 12:18
category: "Discussion"
author: "Maxime Favre"
---
Hello there,

I'm slowly thinking about jumping in the laser bandwagon. I have to say that I've learned a fair amount of things by just reading the tons of usefull informations you're sharing here. Great community !



However I don't have enough space right now for a co2 machine, the k40 could be fine but 300x200mm is a little short.Plus the shipping costs to europe are quite stupid.



I was wondering what materials this kind of machine can do ? It seems that 5.5W is the most powerfull they offer.



My main goals are plywood and acrylic cutting. Those lasers could do it ? If yes thickness ?



I bet electronic is limited but I have a spare azteeg v5 mini laying around.

I'm not hoping for the "dream machine", let's say a compromise to make some first steps with laser.



Sincerely



Max





**"Maxime Favre"**

---
---
**Imko Beckhoven van** *April 11, 2016 12:30*

Looks like the Same laser they use at a "misterbeam" its a kickstarter but they explain the possibilitys on there site. 


---
**Jean-Baptiste Passant** *April 11, 2016 12:46*

Hello !



I own one of those laser (500mW version), and I have to tell you : do yourself a favor and wait to be able to get a K40.



This thing cannot cut anything except paper and foam(not sure of the exact name) also some colors are not cutted well, but you can engrave wood.



You can engrave some materials but it take so long...



I have engraved my phone case with a 3cm by 3cm design, it took 20 minutes and you can barely see the engraving (the case is white plastic).



It is driven by Grbl or similar, which is not bad, but not really good either...



The 5.5W version is way overpriced, and even though you might be able to cut something (probably not plywood or acrylic), it will definitely not be similar to a k40.



The photos on ebay show engraving on 98% of them, and cutted pieces are only foam or paper (there is a plane but you can't see the material, pretty sure it's thin cardboard). The photos are (99% sure) made with the 5.5W laser.



Do not bother with it ;)



EDIT : Also written on the listing : Can be carved materials: bamboo, wood, paper, plastic, leather, etc.



5.5W can cut 3MM linden wood 



So do not bother with it ;)


---
**Jean-Baptiste Passant** *April 11, 2016 12:47*

**+Imko Beckhoven van** it looks like it, but it isn't and I am 100%sure of that.


---
**Maxime Favre** *April 11, 2016 13:51*

**+Jean-Baptiste Passant** Thank you for your feedback !

500mW looks clearly underpowered for what I want to do.

I'm not doing business, it's mainly for hobby so if the cuts take a little more time it's okay as long as the results are fine.



I hope there's a significant gap between 500 and 2500/5500mW. I'd like to hear some feedbacks on them.

Another option could be to buy a 2 axis chassis and an L-cheapo 3.5W laser, about 400usd.



I've seen the Mrbeam kickstarter. It looks nice with cool features but a bit bulky and pricy


---
**Stephane Buisson** *April 11, 2016 14:00*

Welcome here

**+Maxime Favre** best way to get a K40 in EU is from [ebay.de](http://ebay.de) or [ebay.co.uk](http://ebay.co.uk) (for tax reason, vat included), delivery is not that bad.

[http://www.ebay.co.uk/itm/CO2-LASER-ENGRAVER-ENGRAVING-MACHINE-40W-CUTTING-CUTTER-ARTWORK-PROFESSIONAL-/111954607117?hash=item1a1103bc0d:g:~iIAAOSwEK9W~hSR](http://www.ebay.co.uk/itm/CO2-LASER-ENGRAVER-ENGRAVING-MACHINE-40W-CUTTING-CUTTER-ARTWORK-PROFESSIONAL-/111954607117?hash=item1a1103bc0d:g:~iIAAOSwEK9W~hSR)



from germany (delivery <50€)

[http://www.ebay.co.uk/itm/40W-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Cutter-3-Years-Warranty-/191813391586?hash=item2ca8f814e2:g:YmQAAOSwx-9WzqtP](http://www.ebay.co.uk/itm/40W-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Cutter-3-Years-Warranty-/191813391586?hash=item2ca8f814e2:g:YmQAAOSwx-9WzqtP)



you can make a slot in the side to work on larger material, see my 1st video.

good luck


---
**Stephane Buisson** *April 11, 2016 14:02*

**+Maxime Favre** en France [http://www.ebay.fr/itm/40W-CO2-machine-de-gravure-laser-de-coupe-USB-logiciel-Graveur-Cutter-inclus-/111871590867?hash=item1a0c1101d3:g:x0sAAOSw5dNWk2E0](http://www.ebay.fr/itm/40W-CO2-machine-de-gravure-laser-de-coupe-USB-logiciel-Graveur-Cutter-inclus-/111871590867?hash=item1a0c1101d3:g:x0sAAOSw5dNWk2E0)


---
**Maxime Favre** *April 11, 2016 14:06*

Hi **+Stephane Buisson**, Thanks, I've read your guides, great work !

I've already contacted the german one, he offered me 400$ shipping to switzerland, more than the machine price...  Will try with the UK :)


---
**Stephane Buisson** *April 11, 2016 14:12*

**+Maxime Favre** [http://www.ebay.ch/itm/40W-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Cutter-Wood-working-Crafts-/262374032028?hash=item3d16b6029c:g:MqkAAOSw9r1WAlX0](http://www.ebay.ch/itm/40W-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Cutter-Wood-working-Crafts-/262374032028?hash=item3d16b6029c:g:MqkAAOSw9r1WAlX0)


---
**Thor Johnson** *April 11, 2016 14:15*

I used to think that you just needed time if you didn't have enough power, but it's not true; if you don't have enough power, things are just not possible, no matter how much time you give it because the material is dissipating the heat faster than the laser can deliver it; for example (K40 40W), I can cut 1/8 birch ply, etch paint off metal, but engraving brown hard-anodize is very iffy (the anodize shot was taken at 3mm/s, full power and it took an hour):

[http://www.atl-3d.com/18-birch-ply-awesome-paint-good-anodize-not-so-much/](http://www.atl-3d.com/18-birch-ply-awesome-paint-good-anodize-not-so-much/)


---
**Maxime Favre** *April 11, 2016 14:17*

**+Stephane Buisson** Looks like you're an ebay master ;) thanks I'll contact them.


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/HEhNnGbTVzq) &mdash; content and formatting may not be reliable*
