---
layout: post
title: "Well, the K40 was aligned out of the box!"
date: August 04, 2015 15:35
category: "Object produced with laser"
author: "James McMurray"
---
 

Well,  the K40 was aligned out of the box!  Really it was, I checked each mirror and lens and it made perfect circles on graph paper wherever I set it up to draw them.  

Also, I thought the one I bought wouldn't cut very well, but this one follows a vectored path to cut.  Very clean, very narrow cut line on cardboard.   I am very pleasantly surprised.  Tomorrow I am going to print an air nozzle.  I wonder if using nitrogen instead of compressed air will make for less mess and cleaner optics?  

Fun So Far.    I bought this so I could cut out fabric for my partners boy clothing line.  Her designs are a blast!!  Interactive cloths for the Boy!﻿

![images/b0c44bc4f40f632998165a6207701d05.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b0c44bc4f40f632998165a6207701d05.jpeg)



**"James McMurray"**

---
---
**James McMurray** *August 05, 2015 02:16*

I think I am going to purchase a air assist cone made from AL, my 3D printer is using ABS which will burn.  Is Lightobject the best place to get one?


---
**Joey Fitzpatrick** *August 05, 2015 15:01*

Light Object is one of the better places to get one.  Keep in mind that you will also need to buy an 18mm focal lens for it.  The lens that comes with the K40 is too small to fit properly in the light object air assist.(I believe it is only 12mm). You will also need to modify or replace the mounting plate because the mounting hole needs to be enlarged to 22mm 


---
**Kirk Yarina** *August 08, 2015 12:12*

I've seen people modify nylon washers to adapt the lens to the holder.


---
**Jason Barnett** *August 14, 2015 19:25*

**+Joey Fitzpatrick** **+James McMurray** The lens is a different size, but since your laser cutter is working, just cut a spacer from a spare bit of plastic. Just two concentric circles, outer=18mm inner=12, worked for me.

Also, when I ordered my air assist nozzle from LO, I enlarged the mounting hole, only to later realize that It wasn't necessary. When the two halves are unscrewed, the threaded section is actually threaded on both ends and is a separate assembly. There is the top mirror mount, next is the threaded assembly, then the lense mount and finally the air assist section.


---
**Joey Fitzpatrick** *August 14, 2015 21:00*

Un-friggin Believable!!!!  I spent multiple hours making a bracket with a larger hole. I never did get the slotted holes just right, but I have it mounted anyways.  I just took my Light Object lens apart, because I had to see if you were right.  It looks like the top piece is all one piece.  I used a pair of soft jaw pliers to try and unscrew it.  Low and behold you are correct.  The threads unscrew from the top.  Looks like I will be putting the original plate back on.  Thank you **+Jason Barnett**  for this info!!


---
**James McMurray** *August 14, 2015 22:57*

I printed one for now and it is working, the pump I purchased is really loud. 


---
*Imported from [Google+](https://plus.google.com/115183135050042620815/posts/CFkaQAB8qXP) &mdash; content and formatting may not be reliable*
