---
layout: post
title: "I played around with some different line drawings to do some speed testing, but found that text worked better than my square, circle, zigzag pattern"
date: October 03, 2015 20:27
category: "Discussion"
author: "Kirk Yarina"
---
I played around with some different line drawings to do some speed testing, but found that text worked better than my square, circle, zigzag pattern.   Using LaserDRW my K40 tops out around 40mm/sec in cutting (vector) mode.  At 45 it starts to lose position, at 50 it's pretty obvious.

 

![images/a4ceba73e6375aec614755435ad7991a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4ceba73e6375aec614755435ad7991a.jpeg)



**"Kirk Yarina"**

---
---
**Veronica Lamberson** *September 22, 2017 18:54*

Hello kirk


---
*Imported from [Google+](https://plus.google.com/+KirkYarina/posts/Gh4M5DwcfLR) &mdash; content and formatting may not be reliable*
