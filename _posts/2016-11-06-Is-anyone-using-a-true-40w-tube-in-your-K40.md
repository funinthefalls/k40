---
layout: post
title: "Is anyone using a true 40w tube in your K40?"
date: November 06, 2016 05:41
category: "Modification"
author: "K"
---
Is anyone using a true 40w tube in your K40? One that requires cutting the side and adding an enclosure to the end? If so, how is it working for you? What did you need to invest in other than the larger tube and the enclosure to cover the larger tube?





**"K"**

---
---
**Tim barker** *December 05, 2016 15:39*

Any chance you have tried this yourself yet? I just ordered a new tube for myself but it turns out it's the true 40w and not the standard k40 tube. I'm behind on orders now so I'm debating on putting it in anyways. Looking around for experience in this area


---
**K** *December 05, 2016 19:16*

**+Tim barker** I have not had the time do buy a new tube yet. 


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/Nuqn27QjiFP) &mdash; content and formatting may not be reliable*
