---
layout: post
title: "My name is Larry and I am a Laser Horder!"
date: July 08, 2016 03:54
category: "Discussion"
author: "HalfNormal"
---
My name is Larry and I am a Laser Horder!



Well today I finally received another K40 laser. It is a used unit purchased from a member of this forum. It has some issues so I was able to get it at a very good price. I have some ideas on what I want to do with it. I will share them as I get them done. So my tally is 2 K40 lasers and two 3 watt solid state lasers. 





**"HalfNormal"**

---
---
**Anthony Bolgar** *July 08, 2016 04:14*

I have you beat. I have 1 - K40, 1- Redsail LE400, 1- 6.5W laser diode. 1- 2.5W laser diode ;)

 And an OX CNC (800mmX1500mm) :)


---
**HalfNormal** *July 08, 2016 04:44*

**+Anthony Bolgar** Oh yea?! I forgot to add my X-Carve CNC and Prusa I3 3D printer. :-)


---
**Anthony Bolgar** *July 08, 2016 05:37*

I forgot my Wanhao Di3 3D printer, and 50" Vinyl Plotter ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 08, 2016 07:59*

Either of you care to share with me? Hahaha.


---
**Anthony Bolgar** *July 08, 2016 08:34*

I am like an only child when it comes to my toys, I don't like sharing! :)


---
**Scott Marshall** *July 08, 2016 09:10*

ASR 33 Teletype with paper tape punch and reader, Ohio Scientific C1p Computer (6502) with tape drive 8" floppy drive and of course the cool green CRT monochrome display.(actually a converted Zenith BW TV) , 1976 Zenith FC Chassis 19" (best set they ever made), 1st year of production (1980) Sony Betamax,(the one with the clock on top and mechanical tuner), the list goes on. Sanford and Son of tech here. I won't even get into the guns, or sound equipment, all old "junk" if you ask my family.



I can't help it, I like my toys too. It's why I can't find anything though.



It's OK, Half, just get a 3d printer to go with the lasers and call it your "fab shop" Works for me.


---
**HalfNormal** *July 08, 2016 12:36*

**+Scott Marshall** when I moved a few years ago, my wife made me get rid of a bunch of stuff. My trusty Sony Beta max 1 was part of the cleansing! 


---
**greg greene** *July 08, 2016 13:03*

Heath H8, Z100 8" drives, PDP=11 K40 and a laser pointer


---
**Scott Marshall** *July 08, 2016 14:08*

**+HalfNormal** Sacrilege!


---
**greg greene** *July 08, 2016 14:12*

The Z-100 or the PDP-11?


---
**Scott Thorne** *July 08, 2016 20:56*

You guys are crazy....lol


---
**greg greene** *July 08, 2016 21:22*

Funny - that's what my wife and kids say too !!!


---
**I Laser** *July 08, 2016 22:16*

beta max? do you guys actually have tapes? Anyway I hear they've got this new fangdango thing called ultra high definition, maybe you could plug your monochrome crt up to it! :P


---
**HalfNormal** *July 08, 2016 23:38*

**+I Laser**​​ I actually have 2 other Beta Max units and yes lots of tapes! **+Scott Marshall**​​ Oh yea I forgot about my Kenwood reel to reel deck with a a few Led Zeppelin albums.﻿ my wife made me also get rid of my 8-tracks. I saw my unit go for $300 on Pawn Stars a few months back.


---
**greg greene** *July 09, 2016 00:37*

Is that one of them there newfangled magic lantern shows ?


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/eDfFZKF3qG1) &mdash; content and formatting may not be reliable*
