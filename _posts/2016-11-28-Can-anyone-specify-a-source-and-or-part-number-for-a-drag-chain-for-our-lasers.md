---
layout: post
title: "Can anyone specify a source and or part number for a drag chain for our lasers?"
date: November 28, 2016 20:58
category: "Modification"
author: "Bob Damato"
---
Can anyone specify a source and or part number for a drag chain for our lasers? I just bought one and it doesnt turn a tight enough radius to work in the top left area of the bed. So before spending even more money and flushing it down the toilet, Id like to order the correct piece this time. Thank you.  (you might be able to sense my frustration)

Bob





**"Bob Damato"**

---
---
**Don Kleinschnitz Jr.** *November 28, 2016 22:25*

I am in the process of converting mine with this 

[amazon.com - Black Plastic Drag Chain Cable Carrier 10 x 15mm for CNC Router Mill - Twist Chains - Amazon.com](https://www.amazon.com/gp/product/B00880AVL2/ref=oh_aui_detailpage_o00_s01?ie=UTF8&psc=1)



I am anchoring mine to the right rear side of the cabinet. 

I am using this approach.


{% include youtubePlayer.html id="8zBpTaGNAkc" %}
[https://www.youtube.com/watch?v=8zBpTaGNAkc](https://www.youtube.com/watch?v=8zBpTaGNAkc)


---
**Bob Damato** *November 28, 2016 22:50*

Thank you **+Don Kleinschnitz** Have you verified that drag chain will bend the minimum radius? It says .7" which should be plenty!




---
**Don Kleinschnitz Jr.** *November 28, 2016 23:32*

I have had to make brackets that position the head end correctly (one viable position and height so that the x axis can move to its extreme against the back left wall. I also found there is one place and elevation that works for the right side mounting.

This post is not complete but thought it could be useful.

[donsthings.blogspot.com - K40 Gantry Connection Management](http://donsthings.blogspot.com/2016/11/k40-gantry-connection-management.html)


---
**Ned Hill** *November 29, 2016 12:50*

Thanks for sharing Don.  I like your brackets.  I have a drag chain waiting to be installed and just haven't gotten around to it.  But the other day I accidentally ran over my airline on a big cut so I'm going to have to suck it up and get it done. Lol :)


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 06:33*

I use a 7x7mm drag chain, and mine runs on top of the head to the side, unlike most here who run it behind the head (with the curve pointing towards the back). Bend radius is fine, even with the cover closed. 3D printed my own brackets for the head, and side. The 1 meter length that most are is perfect to get 2 pieces, one for the head, and one to run on the side to the back of the machine.


---
**Don Kleinschnitz Jr.** *December 01, 2016 17:31*

**+Ashley M. Kirchner** I wondered why some run to the side X direction and another down the right side in the y direction. I guess if the single direction does not work I will find out why?


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 17:38*

No, that works too. Some people have theirs attached in the middle of the back panel then reaching out to the head. I don't like flying stuff across the work piece, so I took the approach most manufacturers of large CNC type machines do, which is to run one along the x-axis, and one along the y-axis. It keeps things clean and there's nothing flying over the work piece.


---
**Don Kleinschnitz Jr.** *December 01, 2016 17:44*

**+Ashley M. Kirchner** makes sense ... wondering what it the disadvantage of flying over work piece as the X axis does and I assume the cover is closed during the job?

BTW I like the conventional approach but was lazy ..... :).


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 17:53*

Well yeah, the cover is closed (though depending on the machine, some don't have a cover at all) ... For me it's primarily the stresses on the drag chain. At my office we have several CNC-style machines, large 8'x4' beds (2 UV printers and 1 CNC), but also smaller machines. Specially on the UV printers, there are a lot of tubes and cables running through the drag chain (which is massive). Mounted sideways, that thing will sag and over time will cause it to start wearing on the pins and get stuck. Whereas mounted upright, those stresses are removed completely. I've seen a drag chain break and it wasn't pretty. Specially since it was on one of the UV printers and the broken part pinched and cut through one of the ink lines. The manufacturer redesigned it after that and started mounting them upright, one along each axis.



Ultimately though, this is a small laser cutter, and it's all personal preference. If you want to run one, mounted sideways, from the side to the head, or from the back to the head, as long as it works, go for it.


---
**Don Kleinschnitz Jr.** *December 01, 2016 18:36*

Thanks that explanation makes sense and worth heeding.


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 18:54*

This is the 3D drawing of my drag chain setup. I only printed the plates and bought the drag chain (the plates are made to match the chain). While I could've printed the chain links as well, I opted to just spend the $6 on the chain. Not worth splitting hair over.

![images/37ca92feeec3a62a06d366261d57db89.png](https://gitlab.com/funinthefalls/k40/raw/master/images/37ca92feeec3a62a06d366261d57db89.png)


---
**Bob Damato** *December 02, 2016 15:25*

Thanks everyone! I ordered the part **+Don Kleinschnitz** recommended and it works perfectly. I ran my hose and wire through it, and its all good. Ill post pictures this weekend when I get home, but Im already loving it! Attaching the ends of the drag chain was a bit of a pain but I managed to get through it.


---
**Don Kleinschnitz Jr.** *December 02, 2016 16:41*

**+Ashley M. Kirchner** did you get this design from somewhere or design it yourself, if not are you willing to share the plate designs?


---
**Ashley M. Kirchner [Norym]** *December 02, 2016 18:42*

I designed it myself, did several test prints, etc., etc. Let me get home later and I'll dig up the STL files. 


---
**Don Kleinschnitz Jr.** *December 02, 2016 22:05*

**+Ashley M. Kirchner** thanks very much.


---
**Ashley M. Kirchner [Norym]** *December 03, 2016 07:45*

**+Don Kleinschnitz**, here you go: [dropbox.com - Drag Chain Plates.7z](https://goo.gl/JAHLWM)

Please note, these were designed specifically to use with this drag chain: [http://r.ebay.com/6ASXD4](http://r.ebay.com/6ASXD4)


---
**Don Kleinschnitz Jr.** *December 03, 2016 15:18*

**+Ashley M. Kirchner** really nice and simple designs, wish I had know about these before I did my install :). 

Probably convert later. Can I share these designs?


---
**Ashley M. Kirchner [Norym]** *December 03, 2016 17:33*

On here? Sure. 


---
**Don Kleinschnitz Jr.** *December 03, 2016 17:51*

**+Ashley M. Kirchner** you alrady shared it here :)? I was going to add it to my build blog, if you are ok, as an alternate approach. [http://donsthings.blogspot.com/2016/11/k40-gantry-connection-management.html](http://donsthings.blogspot.com/2016/11/k40-gantry-connection-management.html)


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/VdXm3jHx61Z) &mdash; content and formatting may not be reliable*
