---
layout: post
title: "Made this rings for my daughter, it's really simple and fast to make yet you can make thousand of design with this"
date: October 31, 2017 14:21
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
Made this rings for my daughter, it's really simple and fast to make yet you can make thousand of design with this.

I particularly made this one with LigthBurn Beta by **+Jason Dorie**​​

![images/d382f2b98050c1e45004e2025369ac08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d382f2b98050c1e45004e2025369ac08.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *October 31, 2017 14:21*

Source files and info hosted here

[openbuilds.com - Peace & Love Rings &#x7c; OpenBuilds](https://openbuilds.com/projects/peace-love-rings.82/)


---
**Ned Hill** *October 31, 2017 17:55*

Cool idea, I'm going to have to give this a try.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/LJUTYkGRKYE) &mdash; content and formatting may not be reliable*
