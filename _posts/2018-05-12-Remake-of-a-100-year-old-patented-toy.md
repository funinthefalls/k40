---
layout: post
title: "Remake of a 100 year old patented toy"
date: May 12, 2018 23:46
category: "Repository and designs"
author: "HalfNormal"
---
Remake of a 100 year old patented toy. It is intended for kids learning to learn multiplication of small numbers.



Thanks to **+Buhda Punk**. I found this while answering his question! 



[https://www.thingiverse.com/thing:2891426](https://www.thingiverse.com/thing:2891426)







**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *May 13, 2018 12:57*

#LaserToys

Quite Ingenious




---
**Ned Hill** *May 14, 2018 17:15*

What Don said :)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/RigtycEwjeJ) &mdash; content and formatting may not be reliable*
