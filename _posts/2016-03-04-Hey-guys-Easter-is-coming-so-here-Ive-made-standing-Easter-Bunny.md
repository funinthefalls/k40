---
layout: post
title: "Hey guys. Easter is coming so here I've made standing Easter Bunny"
date: March 04, 2016 12:36
category: "Object produced with laser"
author: "Patryk Hebel"
---
Hey guys. Easter is coming so here I've made standing Easter Bunny. 



Tools used: 

- K40 laser engraver

- Scroll Saw



Engraving Settings:

280mm/s /w 16mw



Materials:

- 6mm MDF



Took me about 1 hour and 30 minutes. What you guys think? =D



![images/b37e2587e33df303e356e0efc3e72b13.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b37e2587e33df303e356e0efc3e72b13.jpeg)



**"Patryk Hebel"**

---
---
**3D Laser** *March 04, 2016 13:19*

Cute I like it


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 04, 2016 13:53*

Looks pretty cool :) Not certain if you tried, but you can cut 3mm MDF with the laser, not so sure about 6mm. Although it does end up with a slightly dark edge. You could always give it an extra 1mm or so & sand it off afterwards so you get rid of the blackened edge.


---
**Patryk Hebel** *March 04, 2016 14:02*

**+Yuusuf Sallahuddin** I know, but I'm usully working with 6 - 8mm mdf and for engraving parts this machine is enough. I could mod this machine to cut through 6mm mdf. However since I have scroll saw I don't really mind and I don't have to worry about black edges :)


---
**I Laser** *March 04, 2016 21:57*

You should be able to get through 6-8mm with multiple passes, though you'd need to sand the black edges off. Being a hack with hand tools that's the way I'd go lol.


---
*Imported from [Google+](https://plus.google.com/102509166997588398801/posts/eYncCVNGCcP) &mdash; content and formatting may not be reliable*
