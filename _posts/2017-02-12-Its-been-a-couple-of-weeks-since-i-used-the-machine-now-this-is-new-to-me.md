---
layout: post
title: "It's been a couple of weeks since i used the machine, now this is new to me"
date: February 12, 2017 00:15
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
It's been a couple of weeks since i used the machine, now this is new to me. At 15 mA an it does not even mark the paper, did my tube died?


**Video content missing for image https://lh3.googleusercontent.com/-XtAatfNVe9M/WJ-pNFJMwKI/AAAAAAAB03U/1F2Swe5OrhYDWQSDSw-ehD0A6KRoKWRWACJoC/s0/20170211_191350.mp4**
![images/76bdddf59c102b1a6cf49075934c9281.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/76bdddf59c102b1a6cf49075934c9281.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 12, 2017 01:01*

Hopefully not. I have no idea though. It's interesting to note that it is still creating the purple inside the tube though, which would suggest to me that the tube is still alive somewhat.



I wonder is the optics end of the tube damaged/dirty/messed up in some way?


---
**Ariel Yahni (UniKpty)** *February 12, 2017 01:04*

Thats the only thing that would make sense


---
**Ned Hill** *February 12, 2017 01:38*

Not sure either, but Yuusuf covered all the idea's I had.  Love the Chicago song playing in the background though. ;)


---
**Joe Alexander** *February 12, 2017 04:22*

I had my old tube do this and figured it was a dead tube. Swapped it out and problem was fixed.


---
**Roberto Fernandez** *February 12, 2017 06:02*

Is There  a water flow sensor ? May be a dirty water problem.


---
**Ariel Yahni (UniKpty)** *February 12, 2017 06:06*

No sensor, I would be amazed it it's dirty water. Will change it tomorrow 


---
**Don Kleinschnitz Jr.** *February 12, 2017 17:07*

I doubt it's related to water. The ionization stream seems to indicate the tube is  producing light (like others have said) and the current seems to back that up. Can you turn current up and down? 

Seems like internal output mirror has a problem.

Certainly weird symptoms. 


---
**Ariel Yahni (UniKpty)** *February 12, 2017 17:13*

**+Don Kleinschnitz**​ I can defenetly see change in currebt either in the mA meter and by the intesity of the  light while doing this


---
**Don Kleinschnitz Jr.** *February 12, 2017 17:18*

**+Ariel Yahni** mysterious, stabbing in darkness, is the mirror end of laser hotter than normal?


---
**Ariel Yahni (UniKpty)** *February 12, 2017 17:20*

Haven't got there yet, will do hopefully this afternoon


---
**Stephane Buisson** *February 12, 2017 18:29*

the purple isn't the laser in itself, can't remember where I read the explaination about it. will post here if I found it again.

[http://www.repairfaq.org/sam/laserco2.htm#co2prin](http://www.repairfaq.org/sam/laserco2.htm#co2prin)


---
**Don Kleinschnitz Jr.** *February 12, 2017 19:52*

**+Stephane Buisson** correct but I would be surprised if it is ionizing that well and not putting out any light. Certainly possible.


---
**Gunnar Stefansson** *February 12, 2017 21:41*

Wow! that's pretty odd... Check the Mirror on the backside of the tube, it might have burned through it releasing the energy in the back! I had that problem! But that limited me to engraving only, at full power. 



But that there's nothing coming out is pretty weird! 


---
**Maker Cut** *February 13, 2017 15:13*

I would check the optics first, as suggested by several others. I will also say that each tube manufacturer has different requirements when cleaning the optics, some use alcohol others require acetone.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/SzYi69n17pd) &mdash; content and formatting may not be reliable*
