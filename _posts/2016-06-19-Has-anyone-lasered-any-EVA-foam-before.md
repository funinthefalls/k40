---
layout: post
title: "Has anyone lasered any EVA foam before?"
date: June 19, 2016 10:21
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Has anyone lasered any EVA foam before? I saw some at the local Bunnings Hardware (as floor mats) & had a thought of getting it to laser but was a bit concerned about toxic fumes. Not certain what I want to laser with it yet, but just thought it would possibly be a nice material to work with.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ned Hill** *June 19, 2016 12:36*

Just from a chemistry perspective you will not generate any overly toxic fumes from burning EVA (Ethylene-vinyl acetate).  Probably will have a vinegar smell. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 19, 2016 12:43*

**+Ned Hill** Thanks for that Ned. That's reassuring.


---
**Jim Hatch** *June 19, 2016 13:52*

It's OK to cut. Low power & high speed so it doesn't melt (especially in the corners). The MSDS doesn't show anything worse than hydrogen bromide as a byproduct. More importantly for our purposes, Epilog shows it as an approved laserable material.



Here's a video on cutting it: [https://vimeo.com/84511174](https://vimeo.com/84511174)




---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 20, 2016 00:36*

**+Jim Hatch** Thanks for that Jim. I did see Epilog said it is an okay material, but wasn't sure if maybe their machines have better exhaust/fume extraction. Wiki reckons HBr is highly corrosive, so maybe not good for the machine with large amounts of cutting it?


---
**Jim Hatch** *June 20, 2016 00:56*

Haven't seen an issue with EVA like PVC - might have to do with the quantity of outgassing. Might be an issue if that's the only thing you laser and a lot of it but no one I know who does it has a problem. (A few guys I know use it for architectural models & stuff. Not only cutting but also scoring so the resulting piece can be bent into shapes for a lot of tab & slot construction of models.)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/KuTogoZtnig) &mdash; content and formatting may not be reliable*
