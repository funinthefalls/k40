---
layout: post
title: "Happy PI Day! Refrigerator Magnet, 3mm Birch ply, 50mm diameter (157mm circumference, hehe)"
date: March 13, 2017 19:54
category: "Object produced with laser"
author: "Nate Caine"
---
Happy PI Day! 

Refrigerator Magnet, 3mm Birch ply, 50mm diameter (157mm circumference, hehe).



The engraved background really brings out the texture in the wood.﻿

![images/a07dc59c4d744111f12d6b24f26a103f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a07dc59c4d744111f12d6b24f26a103f.jpeg)



**"Nate Caine"**

---


---
*Imported from [Google+](https://plus.google.com/107379565262839168986/posts/6m8qzmJABv6) &mdash; content and formatting may not be reliable*
