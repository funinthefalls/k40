---
layout: post
title: "Some amazingly cool looking art being created by the 3D printer"
date: July 02, 2016 00:04
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Some amazingly cool looking art being created by the 3D printer. #12HoursPrintFail



![images/b316c84ec5da2aec6f44dd7109d7b1da.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b316c84ec5da2aec6f44dd7109d7b1da.jpeg)
![images/448143ca9d19880684e8406f08f171ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/448143ca9d19880684e8406f08f171ae.jpeg)
![images/a8dfbe02c5228f8a381356406f0d091b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a8dfbe02c5228f8a381356406f0d091b.jpeg)

**"Ashley M. Kirchner [Norym]"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 02, 2016 00:20*

I guess art is just another word for mess. Does look pretty cool though.


---
**Ashley M. Kirchner [Norym]** *July 02, 2016 00:24*

Myah, you could say that. The thing is, most of us here, at some point or another, have had headaches with our lasers. Something isn't aligned, it's not cutting through, this failed, that stopped working, etc., etc. And while last night I was able to get some nice, clean pieces cut on my laser, the 3D printer decided it was going to throw a fit. So yeah, it happens. You roll with it. Clean it, and restart the job ...


---
**Jim Hatch** *July 02, 2016 00:29*

At least with the laser I don't wake up with it having eaten $30 of filament. None of my laser projects have taken more than an hour or so in the machine (the bigger 60W 18x24 one). I routinely have overnight jobs run on the 3D printer and half probably end up with issues. But if I sit & watch it I'll go 10 for 10 without a problem as it lures me into a sense of false comfort.


---
**Ashley M. Kirchner [Norym]** *July 02, 2016 00:37*

This was $2.20 worth of filament, it's calculated at print time based on the purchase price (including the shipping, if I paid any.) I've had 5 minute laser job cost me more than that in materials. But, it's still a valid point.



This was my mistake. This thing is rock solid and rarely fails. I just forgot to add a fresh laying of glue when I started this batch. It's a freshly cleaned glass and there isn't enough glue on it yet to last through several batches. I've done 70+ hour print jobs and gone away for the weekend, no issues at all.


---
**festerND NaN** *July 02, 2016 04:02*

watched a $400 endmill drag through a 8 hour cutting operation... my 3d printer fails seem cute in comparison!


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/7XiiZHbpcou) &mdash; content and formatting may not be reliable*
