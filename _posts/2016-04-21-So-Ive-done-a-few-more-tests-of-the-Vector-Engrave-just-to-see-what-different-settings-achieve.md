---
layout: post
title: "So I've done a few more tests of the Vector Engrave, just to see what different settings achieve"
date: April 21, 2016 02:01
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I've done a few more tests of the Vector Engrave, just to see what different settings achieve. I've noted the settings on each image. I've also attached the original setup file again.



![images/c5b64cc1e53d61e864a91010c00a33dd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c5b64cc1e53d61e864a91010c00a33dd.jpeg)
![images/c7ac483775270b2314dc53727eeec99d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c7ac483775270b2314dc53727eeec99d.jpeg)
![images/09e20ac6c026869f2196efc82bc8ecad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09e20ac6c026869f2196efc82bc8ecad.jpeg)
![images/8c7d986b89960bc9a12f66689dad5b76.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8c7d986b89960bc9a12f66689dad5b76.png)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XtkLKPK8s8j) &mdash; content and formatting may not be reliable*
