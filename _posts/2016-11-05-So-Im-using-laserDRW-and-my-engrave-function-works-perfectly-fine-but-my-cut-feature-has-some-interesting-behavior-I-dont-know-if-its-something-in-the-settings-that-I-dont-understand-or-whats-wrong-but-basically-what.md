---
layout: post
title: "So I'm using laserDRW and my engrave function works perfectly fine, but my cut feature has some interesting behavior, I don't know if its something in the settings that I don't understand or whats wrong, but basically what"
date: November 05, 2016 22:19
category: "Original software and hardware issues"
author: "Jesse Veltrop"
---
So I'm using laserDRW and my engrave function works perfectly fine, but my cut feature has some interesting behavior, I don't know if its something in the settings that I don't understand or whats wrong, but basically what happens is when I go to cut something the first weird thing it does is no matter how thin I make the lines on my model to cut it'll run over the line once and then on the second pass it'll do it again but just slightly larger than the first pass, the weird part begins after its "done" with the design I give, it'll just kinda move around in randomish strange movements, sometimes it'll shoot a random little dot here and there too, and If i try to use the "stop" function in laserDRW whihc normally works fine for engraving it will just keep going until its done doing its little dance. any ideas?





**"Jesse Veltrop"**

---
---
**Scott Marshall** *November 06, 2016 02:25*

You 'double cut' is likely a line width issue, and the odd movements and firing are almost always tiny hidden artifacts left from the original drawing. You need to zoom way in to see (and remove ) them.



If you're using Autocad or Draftsight, you have to remove all hidden layers, just turning them off isn't adequate. That will 'double line" and do the hidden object thing. Drove me nuts.


---
**Jesse Veltrop** *November 06, 2016 03:13*

What confuses me is the Engrave feature won't notice them but the cut feature does, but thanks for the input, I'll give it a try


---
**Scott Marshall** *November 06, 2016 03:15*

That setup does a lot of weird stuff. You just have to live with it, which isn't too bad once you get it figured out. Takes a while though.


---
*Imported from [Google+](https://plus.google.com/114760861883962577037/posts/8eu76Y9TU48) &mdash; content and formatting may not be reliable*
