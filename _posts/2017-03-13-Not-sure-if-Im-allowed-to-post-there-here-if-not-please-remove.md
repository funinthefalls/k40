---
layout: post
title: "Not sure if I'm allowed to post there here, if not, please remove!"
date: March 13, 2017 19:32
category: "Discussion"
author: "Adam J"
---
Not sure if I'm allowed to post there here, if not, please remove!



I'm selling my brand new 3XC Smoothieboard v1.1 which I purchased from [robotseed.com](http://robotseed.com) a few months ago (still with receipt). I have opted to go for a Cohesion board because mine is a ribbon cabled newer K40 and I don't fancy the possibility of me butchering the wiring. So if anybody in the UK is looking for a smoothieboard at a lower price, here you go:



[http://www.ebay.co.uk/itm/132125270127?ssPageName=STRK:MESELX:IT&_trksid=p3984.m1555.l2649](http://www.ebay.co.uk/itm/132125270127?ssPageName=STRK:MESELX:IT&_trksid=p3984.m1555.l2649)





**"Adam J"**

---


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/NW53rz3qZ71) &mdash; content and formatting may not be reliable*
