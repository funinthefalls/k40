---
layout: post
title: "Hello Everybody. I am totally new to Laser cutting"
date: November 06, 2015 10:25
category: "Software"
author: "Tony Schelts"
---
Hello Everybody.  I am totally new to Laser cutting.  I have recently purchased  a K40 as per th pricture.  It came with Laserdraw 3.  I have coreldraw x7 what would be the best format for creating vectors for cutting that this machine would recognise.  I only seem to have some success cutting using the software that came with it.????  When I have tried in the past the machine just hops all over the place from one spot to another.  I realise its a matter of preparing the file properly.  Any help would be gratefully appreciated.





**"Tony Schelts"**

---
---
**Coherent** *November 06, 2015 13:46*

Install Corel software before you install CorelLaser. Then make sure the correct machine is selected when you use the CorelLaser plugin. If not it will do all kinds of strange things like you mentioned.  Make your lines in Corel "hairline" and select the "cut" icon from the plugin on the top right, and ensure your speed setting is correct. Other than that you can find some links on the web on how to use Corel to both engrave and cut in the same operation. 


---
**Coherent** *November 06, 2015 18:02*

Sorry, but noticed you said LaserDraw... There is LaserDraw which is I believe is a Canadian based software and LaserDrw which is the Chinese software that comes with many of the Chinese lasers. Since you said ver 3 just assumed you meant LaserDrw. If not, then I'm not very familiar with LaserDraw (which I think is up to version 8 or 9).


---
**Tony Schelts** *November 06, 2015 18:15*

Your right LaserDRW


---
**Tony Schelts** *November 06, 2015 18:18*

My Corel software cant see the laser when its connected.  But when I start Corel laser it opens up Coreldraw.


---
**Tony Schelts** *November 06, 2015 18:20*

Could it be windows 10 affecting it?


---
**Coherent** *November 06, 2015 18:34*

That's how mine works and I believe the way it's supposed to. For the engraver plugin to load, you have to load CorelLaser. If I simply start Corel on it's own, the plugin does not load. Corel Laser recognizes and loads the actual Corel Draw software, but Corel Draw will not load and recognize CorelLaser.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/9yxuaoeLYap) &mdash; content and formatting may not be reliable*
