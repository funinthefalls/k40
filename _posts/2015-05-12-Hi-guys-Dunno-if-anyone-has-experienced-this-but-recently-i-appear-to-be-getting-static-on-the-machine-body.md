---
layout: post
title: "Hi guys, Dunno if anyone has experienced this but recently i appear to be getting static on the machine body"
date: May 12, 2015 21:51
category: "Hardware and Laser settings"
author: "David Wakely"
---
Hi guys,



Dunno if anyone has experienced this but recently i appear to be getting static on the machine body. When i touch it i can feel a small amount of electricity. Ive checked for obvious split wires etc but its too small to be from the HV line. Anyone ever had this before? I seem to be able to touch an LED and see it illuminate a very small amount when the positive is touching the machine case and negative is touching me. Its slightly crazy!





**"David Wakely"**

---
---
**David Richards (djrm)** *May 12, 2015 22:04*

Hello David, What you call static could be due to some leakage from the power supply lifting the chassis potential above zero. the laser cutting machine case should be grounded, in the uk my machine is grounded with a three core mains supply cable. I have seen pictures of us machines with a seperate ground terminal. It is possible that the usb lead acts as a ground through your pc but I wouldn't count on it. Hth David.﻿


---
**David Wakely** *May 12, 2015 22:11*

I am also in the UK and am using 3 core cable. Could it be that the Chinese wiring is questionable? I may take a closer look at the grounding. Worst case i'll put an earth rod in the ground! Thanks!


---
**David Wakely** *May 12, 2015 22:31*

Just to clarify does that mean my PSU is shot? or does it mean thee is an issue with the wiring to my cutter?


---
**Sean Cherven** *May 12, 2015 22:45*

It means your chassis is not grounded properly. Check the ground connections. 


---
**David Wakely** *May 12, 2015 23:01*

Ok ill have a check tomorrow! hopefully i can find it!


---
**Sean Cherven** *May 12, 2015 23:20*

It's easy. There's two wires attached to the Banana Jack at the back of the machine. Make sure they are tight, and follow the wires from there.


---
**David Richards (djrm)** *May 13, 2015 06:57*

On my own there is a green wire going from the mains iec chassis plug to an eyelet bolted to the chassis. Sometimes you see these crimp eyelets fixed without the wire insulation being removed. Check the earth continuity using a meter and have a close look at the mechanical fixing too.


---
**David Wakely** *May 13, 2015 17:41*

Hi guys you were right! Turns out it was a earth problem! The earth pin on the plug end had too much shielding on it and it wasn't making any contact with the earth in the socket! I'm gonna post a picture! Thanks for all the guidance!


---
**Joey Fitzpatrick** *May 15, 2015 23:36*

I had the same problem with my machine.  I would get a small static shock when I touched the case after it had been used for a while.  I added an extra copper ground wire on the lug on the rear of the unit and clamped it to a metal water line that was nearby.  The booklet that came with my unit recommended to use a 6 ft copper ground rod pounded into the ground.  This took care of the static build up.


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/icfQhBJhcus) &mdash; content and formatting may not be reliable*
