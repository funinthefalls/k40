---
layout: post
title: "Hi to all, I have a laser k40 and I have a strange problem"
date: December 27, 2016 09:43
category: "Discussion"
author: "Mirco Slepko"
---
Hi to all,

I have a laser k40 and I have a strange problem.

When cutting the wood the edge is not straight but towards half thickness is inclined outwardly.

The strange thing is that if you cut a round shape, the phenomenon is not the same at all points but only in certain parts occurs.

anyone have any idea what it is?

Thanks to all those who will answer to me.



![images/74c68d59678fab367bf0844a37db7776.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74c68d59678fab367bf0844a37db7776.jpeg)
![images/b72359828cd57e572677426e25f70940.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b72359828cd57e572677426e25f70940.jpeg)

**"Mirco Slepko"**

---
---
**Phillip Conroy** *December 27, 2016 10:13*

It has to dowith laser beam being hour glass shaped  with the nnarrowest  point being the center of hourglass,which is the focal length of your lens, .Stock k40 focal lens is 50.08mm which is where you want to hit at center of whatyou are cutting..so iif youare cutting 3mm mmdf likeme you want the mdf to be 50.08 mm -1.5mm =48.58 from bottom of local lens to top of mdf 


---
**Mirco Slepko** *December 27, 2016 10:26*

very well, you gave me a good indication.

do you think if I add an air assit that wooden maximum thickness I can cut?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 27, 2016 10:37*

Technically you can cut through much thicker than 3mm, but not in 1 pass & probably not cleanly. Even with air-assist, I'd suggest that the maximum you could cut through with reasonable cleanness is about 5-6mm. Even then, I'd say the K40 would be pushing it to do that cleanly & at a reasonable speed (> 1mm/s). But, never know really. Maybe some others have had better results with their air-assist setups than I have with mine.


---
**Mirco Slepko** *December 27, 2016 10:40*

I just ordered on ebay for a small air compressor, I have already printed air nozzle. as soon as I have time to do some testing I will post some technical data that is useful to all.

thank you


---
**Ariel Yahni (UniKpty)** *December 27, 2016 12:29*

Does it happens inwards at some point in the same piece?


---
**Jeff Johnson** *December 27, 2016 16:40*

Also check that the beam hits the center of the focal lens. If it's off center, the beam will be slightly diagonal. You can do a rough  check by removing the lens and replacing the end cap that holds the lens. Place masking tape over the hole and press it firmly to emboss the shape of the hole into it. Then fire a short burst and check to see if it's centered. I have to readjust this every couple months.




---
**Mirco Slepko** *December 27, 2016 16:42*

Thanks Jeff! i will check it


---
**Anthony Bolgar** *December 28, 2016 09:12*

With air assist, I can cut 6mm mdf at 8-10mm/s on my K40


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 28, 2016 09:26*

**+Anthony Bolgar** That's good to know. Does it come out reasonably clean on the edges? What power level are you using also at that speeds?


---
**Anthony Bolgar** *December 28, 2016 09:30*

I am one of the people that run at 20-25ma, I treat the tube as a consumable, when it dies I will just replace it. The throughput speed at higher mA makes up for shortened tube life.

 (BTW, my tube is over a year old now and still going strong, cooling is a big part of tube life.)


---
**Mirco Slepko** *December 28, 2016 09:31*

wow cut 6mm is really much!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 28, 2016 11:03*

**+Anthony Bolgar** Thanks. I've been trying to keep mine around 10mA & have had it going for over 12 months with no issues so far. Will have to have a play around with higher mA settings & see how it goes. I think your idea of the tube being a consumable makes sense, if increasing the power can decrease job time significantly, it's worth it in the long run (since they aren't hugely expensive to replace).


---
**Jeff Johnson** *December 28, 2016 14:57*

I haven't noticed much performance increase above 18ma. At 18ma I can cut 1/4 inch poplar at 6mm/s with nice edges. I use a 5 gallon bucket with 3.5 gallons of water and a frozen 1 gallon water jug for cooling. I replace the jug every couple hours as the ice melts. 


---
**Mirco Slepko** *December 28, 2016 15:08*

so you can cut the poplar plywood 6mm with only 18 mA?

probably I am a beginner and I have not understood something ... uff

Now the first thing I install the air assist but do not believe in a strong increase in performance


---
**Jeff Johnson** *December 28, 2016 16:47*

Air assist has given the greatest performance increase for me. I am cutting poplar boards, not plywood. The glue in plywood, combined with the poor quality wood often used in the middle layers, make it inconsistent. The thickest plywood I normally use is 3mm. This is the edge of a test piece of 1/4 inch poplar.

![images/0d10665c940d9e4b8c5dae5cfcc72cd2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d10665c940d9e4b8c5dae5cfcc72cd2.jpeg)


---
**Mirco Slepko** *December 28, 2016 16:48*

very very well, thank you for this confirm!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 28, 2016 17:06*

Interesting **+Jeff Johnson**. Going to have to see if I can find similar thickness poplar locally.


---
**Jeff Johnson** *December 28, 2016 17:30*

**+Yuusuf Sallahuddin** I have some red oak in this thickness as well but haven't tested it yet since I've been so busy fullfilling Christmas orders. I also have some 3mm hickory, walnut and cherry to try. I have so many projects that this is starting to feel like work.


---
**Mirco Slepko** *December 29, 2016 07:56*

waiting for the mailman to deliver me the air pump, I'm trying to understand if the mirrors are aligned well. I realized that the third mirror (the last) do not get a single beam but two because I see that on paper tape you make 2 holes. do you think this is normal or is there something wrong alignment?


---
**Jeff Johnson** *December 29, 2016 08:08*

**+Mirco Slepko** something is wrong. I get a single beam at each mirror. Also, be sure to perform the check at the limits of travel for the mirrors. For example, when ensuring that the beam is centered in the focal lens I test test the head at each of the 4 corners. There are many videos on mirror alignment and it may be helpful to watch a few. You'll start with the first mirror in the series and work your way down to the last right before the lens. Sounds like you're being maybe hitting the edge of a mirror or the lens housing.


---
**Mirco Slepko** *December 29, 2016 08:36*

confirm that the optimal situation is when the beam hits a single point?

If so I will have to lose a few hours to re-check all.

thanks guys are giving me a really big help!!!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 29, 2016 10:12*

**+Mirco Slepko** Yeah, optimal is when you get the beam to hit the exact same point on the 3rd (travelling) mirror at all 4 corners of the machine (top left, top right, bottom left, bottom right). It will take a while to sort this stuff out when you first are learning, but eventually you will be able to do the alignment process in about 10 minutes once you know what you're doing.


---
**Cesar Tolentino** *December 30, 2016 17:06*

I believe the issue is table height.  Like Yuusuf have mentioned.  It is the distance between the last lens and the material being cut.  I had my K40 since 2013.  My power never go above 10ma. 


---
*Imported from [Google+](https://plus.google.com/+MircoSlepko/posts/KEnH1WfJ5Jh) &mdash; content and formatting may not be reliable*
