---
layout: post
title: "Is it ok to watc the laser with the lid closed or is dangerous, or only if the lid is open"
date: April 08, 2016 10:21
category: "Discussion"
author: "Tony Schelts"
---
Is it ok to watc the laser with the lid closed or is dangerous, or only if the lid is open.





**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 11:17*

I'm not certain, but I tend to watch it with the lid closed, but end up with minor temporary flash blindness (like if you stare at sun or torch or whatever). I can't imagine it is good for your eyes at least.


---
**Jean-Baptiste Passant** *April 08, 2016 11:29*

Unless someone checked the lid for UV protection rating, I would limit the time passed watching the laser through it. Either way, glasses are always recommended.


---
**Anthony Bolgar** *April 08, 2016 12:12*

CO2 lasers have a wavelenght of 10600uM. It is absorbed by regular glass or acrylic. You only need a simple pair of safety glasses, preferably the ones that wrap around your face, to protect your eyeballs.


---
**Vince Lee** *April 08, 2016 15:55*

I wouldn't mess around with eyewear.  I got glasses specifically rated for use around co2 lasers.  The risk I see is that cheap safety glasses (like ones used in labs for splashing) might be too thin for adequate protection as they could quickly burn through if the beam reflected out for some reason. Thicker lenses would buy more time.  Under normal circumstances with the lid closed I think the risk is  low, but remember that a focussed beam cuts through acrylic nearly instantaneously.


---
**Thor Johnson** *April 08, 2016 15:57*

Yuusuf Sallahudin nailed it.  Though I think if you stare into the sun, you can get more than temporary blindness, so I make it a point not to watch the beam for very long, especially at high powers.



You don't need to worry about UV or even IR protection; Anthony is right that any optics will absorb it (plastic or glass glasses).  Even if you shoot your eye with it, it won't get to your retina (but at 40W, it will do an impromptu/uncontrolled lasik procedure on you lens, so I wouldn't do that -- not blindness, but you'd have a cataract until a professional fixed it [but they could -- unlike the Blue/Green/Red lasers that destroy the retina because they get focused on it by your eyball's lens]).


---
**Jean-Baptiste Passant** *April 08, 2016 15:58*

**+Vince Lee** You are right, do not go cheap when talking about safety. 

You wouldn't be able to do the laser surgery yourself anyway :P


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 16:09*

**+Vince Lee** I'd imagine that the focused beam cutting through acrylic near instantly is not really an issue we need to concern ourselves with when the lid is shut, as the beam would be seriously out of focus from that position. The beam would have to reflect off whatever object you are cutting and then up ~100mm to even get to the plastic window on the laser cutter. Given that the stock lens has a focal point of 50.8mm, that would be 3 times the distance of the optimal focal point. Still, possibly cause some eye damage or as Thor says, an impromptu Lasik surgery. I'd avoid looking at it if possible. Use a webcam or something similar to stream the image to a screen where you can safely monitor the progress.


---
**Vince Lee** *April 08, 2016 16:20*

**+Yuusuf Sallahuddin** yeah.  The scenario I was thinking about was something coming misaligned or falling into the beam before the lens.  While not optimally focused, the beam would still be parallel at that point and thus dangerous if it reflected off a flat surface.


---
**Stuart Rubin** *April 08, 2016 18:24*

With your remaining eye, do not stare at the laser beam.


---
**Tony Schelts** *April 08, 2016 22:42*

Thanks for your thought's guys.  Another Question.  Do any of you make money from your laser and what sort of things do you tend to sell???


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 09, 2016 00:41*

**+Tony Schelts** Currently I don't make any money from my laser. It just chews my money (on materials) & gives nothing back :(


---
**Vince Lee** *April 09, 2016 20:04*

**+Tony Schelts** the only money I am likely to make will be pirate doubloons for the little monkeys.  Besides, my impression is that engraving jobs don't end up paying an exorbitant amount per hour and sound a bit too much like work to me.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 09, 2016 20:43*

**+Tony Schelts** I think the lasering can have saleable products, it's just a matter of what & to who & is it worth the time/effort/materials. Sometimes the amount of materials/time that goes into something is just not feasible to sell for what you want to get paid for your time. e.g. a wallet takes me ~5 hours to make, & something like $30-40 materials. Including my time, that makes it $140, which seems like people aren't really willing to pay. Although, I may just not have found the right market to sell them for that price.



Some in this group have sold a bunch of things, eg: organisers for some tabletop games; someone else sold some bookmarks; I think I've seen other's selling engraved slate for house numbers, etc.



**+Vince Lee** From your last statement, you sound like an Aussie: "too much like work" lol.



Pirate doubloons sounds like a good idea. I actually saw a 3d model for them earlier today (when I was searching for random models to play around with). [http://www.turbosquid.com/3d-models/free-gold-pirate-coin-3d-model/880336](http://www.turbosquid.com/3d-models/free-gold-pirate-coin-3d-model/880336)


---
**I Laser** *April 09, 2016 23:47*

There's definitely things you can make and sell. I've made a  small amount, and also covered the costs of purchasing equipment and material. 



Unfortunately with globalisation, you're competing with third world wages. The list of items I've considered making but have found cheaper elsewhere is quite long! Therefore I always do my homework prior to starting work on a new item. I need to know I can cover my costs and sell at a competitive price first.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 01:16*

**+I Laser** That's exactly correct. Also, if attempting to sell internationally (via ebay, etsy, etc) you have all sorts of fees, horrible postage costs, and the issue of postage taking time to get anywhere. These things can cause lack of sales.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/MxYszR5FBf9) &mdash; content and formatting may not be reliable*
