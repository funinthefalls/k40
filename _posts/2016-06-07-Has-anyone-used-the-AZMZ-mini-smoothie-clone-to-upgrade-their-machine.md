---
layout: post
title: "Has anyone used the AZMZ mini smoothie clone to upgrade their machine?"
date: June 07, 2016 02:17
category: "Smoothieboard Modification"
author: "Eric Rihm"
---
Has anyone used the AZMZ mini smoothie clone to upgrade their machine? I don't think I need a level shifter with it as it has an option to change it from 3.3v to 5v. I'm just not really sure on how to wire it up and the guides for the k40 don't match. I also have a breakout board that is ready to go [http://reprap.org/wiki/AZSMZ_Mini](http://reprap.org/wiki/AZSMZ_Mini)





**"Eric Rihm"**

---
---
**Derek Schuetz** *June 07, 2016 02:20*

If you find a good source that shows it without the pot and laser fire button let me know


---
**Ariel Yahni (UniKpty)** *June 07, 2016 02:22*

**+Eric Rihm**​ you don't need it. Look here [https://gist.github.com/openhardwarecoza/81f8ce627a3f2e3cdf8f](https://gist.github.com/openhardwarecoza/81f8ce627a3f2e3cdf8f)


---
**Eric Rihm** *June 08, 2016 02:36*

I'm not really sure on wiring the power up with the breakout board and into the smoothie, any advice?


---
**Ariel Yahni (UniKpty)** *June 08, 2016 02:47*

sure, is that azsmz correct?

Can i have a picture of your k40 psu?


---
**Eric Rihm** *June 08, 2016 02:53*

Here is the version I bought 

[http://www.ebay.com/itm/New-Ver-2-1-AZSMZ-32bit-3D-Printer-controller-Ver2-1-AZSMZ-12864-LCD-/261942397637?hash=item3cfcfbcac5:g:yLQAAOSwYGFUyxXQ](http://www.ebay.com/itm/New-Ver-2-1-AZSMZ-32bit-3D-Printer-controller-Ver2-1-AZSMZ-12864-LCD-/261942397637?hash=item3cfcfbcac5:g:yLQAAOSwYGFUyxXQ)



And a link to the pic of PSU [http://i.imgur.com/odk3z2S.jpg](http://i.imgur.com/odk3z2S.jpg)


---
**Eric Rihm** *June 08, 2016 03:04*

And the breakout board I used, it's populated [https://oshpark.com/shared_projects/3W1BpcNl](https://oshpark.com/shared_projects/3W1BpcNl)


---
**Ariel Yahni (UniKpty)** *June 08, 2016 03:04*

ok, its a little different from mine but the result its the same. I will ask you first to first confirm that: you know how to connect the steppers, endstops?


---
**Ariel Yahni (UniKpty)** *June 08, 2016 03:05*

ok so you can the machine running without the laser? Smoothie is configured?


---
**Ariel Yahni (UniKpty)** *June 08, 2016 03:17*

**+Eric Rihm**  are you there?


---
**Eric Rihm** *June 08, 2016 04:08*

No that is the part I don't know how to do, I don't have smoothie running need to figure out wiring


---
**Ariel Yahni (UniKpty)** *June 08, 2016 04:17*

Yes but wiring the laser it's one thing the other is the motors and endstops. Have you ever done that before on a 3d printer maybe?  Just want to make sure from where to start


---
**Derek Schuetz** *June 08, 2016 04:19*

I can help you maybe with motion I've wired slot of 3d printers to azteeg smoothie boards


---
**Eric Rihm** *June 08, 2016 04:22*

No but I all I need to do is wire the breakout board and smoothie to psu and should be able to figure out the axis


---
**Ariel Yahni (UniKpty)** *June 08, 2016 04:29*

ok. post a pic of the populated middleman. Need to see what you have used there


---
**Eric Rihm** *June 08, 2016 04:33*

[http://i.imgur.com/WQjXKFi.jpg](http://i.imgur.com/WQjXKFi.jpg)


---
**Eric Rihm** *June 08, 2016 04:34*

[http://i.imgur.com/iZYKFMk.jpg](http://i.imgur.com/iZYKFMk.jpg)


---
**Ariel Yahni (UniKpty)** *June 08, 2016 05:02*

**+Eric Rihm** sorry but thought tose where connected. Please another picture of the current connection from the psu to the stock board.


---
**Ariel Yahni (UniKpty)** *June 08, 2016 05:14*

**+Eric Rihm** going to sleep but here is a picture of the overoll scope. If i remember correctly the ribbon passes endstops and X stepper, if so the follow from it [http://imgur.com/8F8Xz1F](http://imgur.com/8F8Xz1F)


---
**Chris Sader** *August 10, 2016 18:01*

**+Eric Rihm** any updates? I just ordered a AZMZ, so curious what your setup ended up looking like


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/Nvmy7QMa9JC) &mdash; content and formatting may not be reliable*
