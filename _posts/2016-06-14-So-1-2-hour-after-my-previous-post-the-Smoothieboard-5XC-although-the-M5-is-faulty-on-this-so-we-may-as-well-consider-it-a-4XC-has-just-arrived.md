---
layout: post
title: "So 1/2 hour after my previous post, the Smoothieboard 5XC (although the M5 is faulty on this, so we may as well consider it a 4XC) has just arrived"
date: June 14, 2016 03:56
category: "Smoothieboard Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So 1/2 hour after my previous post, the Smoothieboard 5XC (although the M5 is faulty on this, so we may as well consider it a 4XC) has just arrived.



A big thanks to **+Arthur Wolf** for designing & developing this board.



Now, onto the fun part. Figuring out what I have to do with it.



So along with my previous post ([https://plus.google.com/+YuusufSallahuddinYSCreations/posts/3sMrHbN6WF5](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/3sMrHbN6WF5)) I should be on the road to conversion shortly. As soon as I figure out what is necessary to buy/attach/do.



![images/3286cbd03e1d8410899217827a1d5e7b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3286cbd03e1d8410899217827a1d5e7b.jpeg)
![images/5c4a80952da3ee4e63288bce8e6b1439.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c4a80952da3ee4e63288bce8e6b1439.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ariel Yahni (UniKpty)** *June 14, 2016 04:09*

Based on your previous post you need a middleman board as **+Alex Krause**​ said. That's to convert the cables types. Then you will need a logic converter. Take a look at my collection on K40 upgrades. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 04:32*

Thanks **+Ariel Yahni** & **+Alex Krause**. I'm not certain what a logic converter is, but I'll take a look through your K40 upgrades.


---
**Alex Krause** *June 14, 2016 04:33*

[https://www.sparkfun.com/products/11771](https://www.sparkfun.com/products/11771)


---
**Ariel Yahni (UniKpty)** *June 14, 2016 04:33*

Essentially your board takes. 3.3v and the psu 5v. ﻿


---
**Alex Krause** *June 14, 2016 04:34*

It translates the pwm signal from the smoothie to a power level the laser recognizes to get the full use of the pwm 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 04:38*

Thanks again guys. So the "logic converter" is also what people have referred to as a "level shifter"? So simply put, it converts the pwm to a higher voltage level to suit the PSU?



I'd be lost without people like you.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 06:54*

**+Peter van der Walt** Thanks for that. I'm not really certain what is going on in those pictures from the scope, but I can at least see that the middle photo (with logic shifter) is more zig-zaggy haha. The other 2 look much more stable. Is the converter that **+Alex Krause** suggested suitable? If so, I'll grab that one.


---
**Alex Krause** *June 14, 2016 06:58*

**+Yuusuf Sallahuddin**​ on another post of yours I supplied a link to an au retailer that carries the sparkfun Texas Instrument txb0104. 


---
**Alex Krause** *June 14, 2016 07:00*

Those zigzag lines are emf noise in the square wave i believe... and means you have very poor output control of the pwm signal **+Peter van der Walt**​ please correct me if I'm wrong it's been ages since I used an O-scope


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 07:06*

**+Alex Krause** Thanks for that Alex. I did see you posted that for Susan. I'll get one from that supplier so I can hopefully be up & running in the near future.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 07:50*

**+Peter van der Walt** I've ordered one of the SparkFun ones from the link Alex provided in different post of mine. Thanks for your help.


---
**Eric Rihm** *June 14, 2016 11:36*

I have an extra break out board you can have for free if you want it. Just email me ericrihm@gmail.com 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 11:42*

**+Eric Rihm** Hi Eric. Is a break out board the same as a middleman board? If so, thanks kindly for your offer, however someone locally in Australia has graciously offered one that they have as an extra also.


---
**Don Kleinschnitz Jr.** *June 14, 2016 12:44*

MMMmmm, not sure that I buy that the signals on the scope  are telling us anything that bad. My read in order of pictures: 

1. Different scale and horizontal settings than the other pictures, DC scale: 3 divisions @ 100mv = .3v, that does not look right,  must be 10x probe = 3.xV as the other pictures are scaled the same?

2. 0-5V signal, looks like shifting sync due to changing PWM high time (low time is same) which can have many sources including the PWM repeat-ability or simply the sync setup on the scope. 

The only way I can imagine the LS would cause this is if it intermittently changed response characteristics. The LS is pretty passive.

Unless this LS is weird they are just MOSFETs in series where the gate is controlled by the PS. I suppose supply noise could turn the FETS off... but that would have to be a large PS change.

The noise levels I see are normal especially if its at the other end of a cable. Doesn't look like any DC shift on signal assuming the arrow is ground.

3. Very clean full 3V swing source with no apparent DC offset.



I have done a lot of research on the LPS-PWM interface design. I am inclined to believe that the problem people are seeing in power level are caused by the DC offsets when using the IN port on the LPS and especially when leaving the "Current Regulation" pot installed. 

I am also warming up to an open drain approach to this design that does not need a ls. I can't verify any of this until I get my smoothie setup and test some configurations ..... 

If I use a LS, I plan to use the Adafruit breakout. 



[http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.html](http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.html)


---
**Joe Spanier** *June 14, 2016 14:45*

So that is the spark fun LS not the cheapo eBay one. 



It is running through a bread board at the moment though. 



**+Don Kleinschnitz**​ this was my first time using an O-scope and I have no real idea what I'm doing. If I'm being honest. The probe was on 10x, yes. 



Do you have advice on settings you would like to see? I have it for a couple more days and would happily hook it up to the various controllers I have. 



**+Peter van der Walt**​ I did hook the Brainz to the laser and just tested power output with similar results. 23-24ma


---
**Vince Lee** *June 15, 2016 03:04*

At 100% shouldn't the pwm output be a constant voltage (3.3v or 0v if inverted) anyway unless it has been capped at a lower percentage in the config?  I can see flakey hardware potentially introducing some nonlinearity in the power output curve, but at full power I don't see how it could matter, especially if controlling an enable line instead of the IN line.


---
**Don Kleinschnitz Jr.** *June 15, 2016 15:13*

Yes the PWM would be fully high at 100% DF and power at max. However at other DF's the PWM pulse would be a lot smaller. At 10% DF the PWM pulse would be about 2ms. 

I do not know how these various ports would react to higher frequency PWM control. It may be fine but it also could create transient problems in the power output and supply. 

When using the IN pin which is an analog input, along with the Current Regulation pot the PWM signal will have a DC offset. If not a problem it will at least create a variable output power dependent on how the PS reacts to the composite PWM-DC signal. In this case I would like to understand how to map the PWM+pot setting to a predictable output power.

Lastly knowing the input characteristics of the PS may allow an open drain interface that does not need a level shifter :).


---
**Arthur Wolf** *June 15, 2016 16:08*

**+Peter van der Walt** There is a native DAC on the LPC1769, you don't need to add a digipot it's all there already.

Adding support for it to the laser module is trivial, somebody did it a long time ago but it was never merged ( likely because nobody had use for it )


---
**Alex Krause** *June 15, 2016 18:07*

Does this mean I can tie into the center tap for my pot input to regulate power?


---
**Don Kleinschnitz Jr.** *June 15, 2016 22:21*

**+Peter van der Walt** an analog output to IN (0-5v) and set the max power from a control panel widget in laserweb? Get rid of the pot.



Actually, just add the max power control to Laserweb and use an open drain PWM to IN, get rid of the pot.


---
**Joe Spanier** *June 16, 2016 03:39*

So the way mine is wired theres a 5v signal to IN and PWM goes to H (or L if I wanted to use a low level signal)


---
**Joe Spanier** *June 16, 2016 03:39*

[http://www.lightobject.com/100W180W-PWM-CO2-Laser-Power-Supply-P74.aspx](http://www.lightobject.com/100W180W-PWM-CO2-Laser-Power-Supply-P74.aspx)


---
**Ariel Yahni (UniKpty)** *June 16, 2016 04:12*

**+Ray Kholodovsky**​


---
**Ariel Yahni (UniKpty)** *June 16, 2016 04:16*

So the nano is only connected to the psu via 24v, 5v, L, G.  Where do we connect the 5v to?  


---
**Joe Spanier** *June 16, 2016 04:19*

Most of the laser psus have a 5v output on them. You would tie that to the IN.


---
**Ariel Yahni (UniKpty)** *June 16, 2016 04:26*

So in the K40 those. 5v would be connected to the IN in the pot.  The would make max power on it and we control power via pwm? Is that it? 


---
**Vince Lee** *June 17, 2016 02:09*

Or if you are going to tie IN to 5v anyway you can leave the pot connected to it and turn it up to full power or a nominal Max safe power (e.g. 18ma).  Then you have some freedom to do real time adjustments.


---
**Don Kleinschnitz Jr.** *June 17, 2016 18:13*

I do not have a TH/TL pin on my supply. 


---
**Alex Krause** *June 17, 2016 19:22*

Lol I'm so lost... there are so many ways these can be wired I wish I had Line diagram so I don't mess it up 


---
**Alex Krause** *June 17, 2016 20:03*

**+Peter van der Walt**​ LMAO!!


---
**Ariel Yahni (UniKpty)** *June 17, 2016 20:39*

#rotarytoolsupportpleaseneedtoengrve50whiskeyglasses


---
**Ariel Yahni (UniKpty)** *June 17, 2016 20:44*

#nohashtagnovalid


---
**Don Kleinschnitz Jr.** *June 18, 2016 00:47*

**+Peter van der Walt** the L pin in the power connector? Has anyone tried this, some testing shows it will not respond to a pwm signal and it's low true so no invert?


---
**Vince Lee** *June 18, 2016 18:14*

**+Don Kleinschnitz** I was able to pwm the L pin. My psu appears to power on much faster than it powers down between pulses, so i had to greatly increase the pwm period (decrease the frequency) to find a rate where the output responded linearly.   It works fine now but it would be nice if the software supported a correction curve to map power to duty cycle for more flexibility in setting this up.  It would also allow one to even get rid of the offset when using pwm on the IN line.


---
**Don Kleinschnitz Jr.** *June 24, 2016 13:16*

**+Vince Lee** when you say you decreased the frequency do you mean that you changed the PWM period in the configuration file to something longer than 20MS? 

Does this infer that the LPS takes longer than 20MS to shut off?

I wondered how the 20MS was chosen. 

I wonder does this create a problem with engraving quality?


---
**Vince Lee** *June 24, 2016 17:17*

**+Don Kleinschnitz** Yes, I don't have the final number in front of me, but I'm using a period much higher than 20uS.  In order for PWM to work on a digital input, it seems to me that the frequency has to be faster than the power supply can respond, otherwise you'd just be pulsing dots instead of reducing power. But at the same time, you don't want the frequency to be so fast that the power supply treats intermediate values as constant-on or constant-off.  With the default period, I found that the laser jumps up to near full power anytime the pwm power level as moved much more than zero, which implies to me my power supply can turn on more quickly than it can turn off.  As for engraving quality, it's possible pwm can affect things, but even at a 200 microsecond period (10x) if engraving at a fast 500mm/s, that spaces pulses 1/20mm apart on the engraved piece.


---
**Don Kleinschnitz Jr.** *June 27, 2016 12:19*

**+Vince Lee** lately I have been wondering about the whole response picture including the PWM input, PS response and the tubes response.

BTW do you mean milliseconds in the above? 

It makes sense that the period must be long enough for the laser to react (go high and then low) before the next cycle.

I watched 
{% include youtubePlayer.html id="BXJTSBeywHQ" %}
[https://youtu.be/BXJTSBeywHQ](https://youtu.be/BXJTSBeywHQ) with interest. The "pre-ignition" discussion started me wondering if at low powers the response is different than at high powers?

I am back from vacation and plan to set up a test that looks at the entire system from PWM to laser light. 

My plan is to build an adjustable PWM generator and an IR sensor at the bed. Then I can look at both on my scope with the intention of understanding better how the entire system responds to various PWM inputs.

First I have to figure out how to measure the output power at the bed from 0-40 watts without frying anything :).


---
**Vince Lee** *June 28, 2016 04:10*

**+Don Kleinschnitz**  hi very nice write up you have by the way.  The comment in my config file says that the value entered there is in microseconds, but I have no other info confirming this.  I checked my wiring again and I think I might have a slightly different psu than most.  The one I have matches the one in the following post (not mine, but a ref I used for my conversion).  I have kept all the existing wiring and simply replaced (inserted inline actually) my switchable adapter board at the controller board connectors, so the line I am controlling with pwm is the "L" line in the diagram.  I left the pot, test fire, and safety enable switch in place.  [http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html](http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html)


---
**Don Kleinschnitz Jr.** *June 28, 2016 04:26*

**+Vince Lee** I think the mystery is solved, you are using "L" or sometimes called "TL" on the control section of the supply. The one that we were talking about that does not work is the L on the power connector. :)


---
**Ariel Yahni (UniKpty)** *June 28, 2016 04:31*

**+Don Kleinschnitz** im using the L from the power connector


---
**Don Kleinschnitz Jr.** *June 28, 2016 04:39*

**+Ariel Yahni** oh SNAP! Its the far right pin on the supply looking at the connector end of the supply?


---
**Ariel Yahni (UniKpty)** *June 28, 2016 04:49*

Yes. I just used 24v and G to power the board and L to the mosfet negative. Only one cable. 5v not used. I think its exactly the same as **+Vince Lee** 


---
**Don Kleinschnitz Jr.** *June 28, 2016 05:05*

+Vince Lee used the L in this drawing which I think is different than what you are using .... I am confused again.

[http://laserpointerforums.com/attachments/f43/40803d1360135734-power-supply-15w-co2-laser-iner.png](http://laserpointerforums.com/attachments/f43/40803d1360135734-power-supply-15w-co2-laser-iner.png)


---
**Ariel Yahni (UniKpty)** *June 28, 2016 11:25*

**+Don Kleinschnitz**​ this is his blog [http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html?m=1](http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html?m=1)


---
**Don Kleinschnitz Jr.** *June 28, 2016 13:36*

**+Ariel Yahni** thanks for the link, I see that he used the power supply "Fire" signal which looks like what we call are calling "L" ... go figure !


---
**Ariel Yahni (UniKpty)** *June 28, 2016 13:50*

**+Don Kleinschnitz**​ the only issue here is determining what's really happening and if it's optimal.  Again I say if the stock board is only connected by this why do we need to tap onto another 


---
**Don Kleinschnitz Jr.** *June 28, 2016 19:02*

**+Ariel Yahni** your point is that the stock board must use the "L" signal to turn the laser on and off during operation so it should also work as a PWM control.... makes sense! 

I actually got so absorbed in the "IN" signal "thing" that I never thought about how the stock board controls the ON/OFF of the laser, it must be the "L" signal since the controller does not connect to any of the other pins ....DUH!

Did I capture your point?


---
**Ariel Yahni (UniKpty)** *June 28, 2016 19:37*

**+Don Kleinschnitz**​ 10000% correct. So the other connection should be if you want to control max power (pot). 


---
**Vince Lee** *June 28, 2016 20:35*

**+Don Kleinschnitz** the drawing is from somebody else's conversion with the same power supply but my connections are not the same.  That post has two drawings with different pin labels.  On the big schematic, the control connector has two pins named P1 and P2 that are labeled H and L on the smaller drawing (my PSU has no visible labels at all).  I am not PWM controlling either of these.  Instead, I am using the L on the power supply connector, which is the same line that was formerly connected to the FIRE pin on the moshiboard.


---
**Don Kleinschnitz Jr.** *June 29, 2016 00:39*

**+Vince Lee** I think that I am back under control. **+Ariel Yahni** connected me to the output of his laser power supply and realigned my neural pathways. :)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/UgKWBdU6711) &mdash; content and formatting may not be reliable*
