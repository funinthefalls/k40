---
layout: post
title: "I was wondering if anyone had a spare Cohesion board laying around they could help me out with"
date: May 05, 2017 13:49
category: "Discussion"
author: "Robert Selvey"
---
I was wondering if anyone had a spare Cohesion board laying around they could help me out with. Can't deal with this stock board any longer it's been a nightmare doing strange stuff lately. Back in Oct, last year I got nerve damage in my left hand and not able to work anymore, so I would like to try and make some money with k40 if I could. Thanks.





**"Robert Selvey"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2017 16:52*

We're in stock 


---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2017 16:53*

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Darryl Kegg** *May 05, 2017 19:23*

Woohoo!   just got my shipment notification....  


---
**Steve Clark** *May 06, 2017 00:36*

Same here looking forward to it... yet...I'm a little afraid of this project too...as it's not in my area of knowledge. Heh..Heh...such is life...gotta crawl before you can walk!


---
**Ray Kholodovsky (Cohesion3D)** *May 06, 2017 00:41*

Gents, I advise you to read our upgrade guide (I think it's about time I added pics for the other machine type to it btw) and get started with LaserWeb. That should take a little while to keep you busy. 


---
**Mike Lowe** *May 06, 2017 13:43*

My C3D board arrives Saturday. Unfortunately I don't get home from a business trip till Wednesday....


---
**Steve Clark** *May 06, 2017 19:06*

Ray, Your right and everything is good. I've played with LaserWeb and until I'm using it on the machine I'm far as I can go with it. Knowing G-Code already was a big time saver. 



I've also read over the upgrade guide more than once and it's straight forward and with great detail.  I'm always a little cautious with electronics. I mostly know what not to do... I just worry about doing something wrong that destroys the board. grin  


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/KCFXABi44Ya) &mdash; content and formatting may not be reliable*
