---
layout: post
title: "SIGH In an attempt to align the mirrors in my new K40 machine, I snapped one of the adjustment screws in the #1 mirror"
date: July 29, 2017 21:19
category: "Discussion"
author: "Scott Pollmann"
---
<b>SIGH</b>   In an attempt to align the mirrors in my new K40 machine, I snapped one of the adjustment screws in the #1 mirror.  This after I removed the table and started to loosen the bolts so that I could align the laser from mirror 1 to mirror 2.  Didn't even get to the mirror on the cutting head yet.  :(



The reviews on the light object $10 mirror holder aren't very good.  Where should I get a replacement?  Or.... even better.... is there such a thing as a machine that is adjusted well?  Maybe someone that is upgrading from a K40?





**"Scott Pollmann"**

---
---
**Nate Caine** *July 29, 2017 22:11*

The Light Object mount that you mentioned looks a lot like the original K40.  How'd you manage to break the screw?  I'd think you could replace the screw for 50-cent and a trip to the hardware store. Cap screw. Remember METRIC!


---
**Scott Pollmann** *July 29, 2017 22:55*

I would love to say that I was a weight lifter and a beast, however, this is a support group for geeks and nerds.... they would know I wasn't being honest.  



I was over zealous when tightening the nut.  Mechanical things aren't my area of expertise, but I believe we could have lots of fun with the K40.


---
*Imported from [Google+](https://plus.google.com/103382513064902122956/posts/8qUiCRJ3bcb) &mdash; content and formatting may not be reliable*
