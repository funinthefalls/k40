---
layout: post
title: "Happy 4th of July to all my USA compatriots"
date: July 04, 2017 19:21
category: "Object produced with laser"
author: "Ned Hill"
---
Happy 4th of July to all my USA compatriots.  Made from 1/4" birch ply with 1/8" alder medallion. Red and blue wood stained with white infilled engraved stars. All sealed with 2 coats of gloss poly. 



![images/a572c20dc4bd470fb7f5c73639c8a672.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a572c20dc4bd470fb7f5c73639c8a672.jpeg)
![images/62ed0a339624364b49f99397026d7030.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/62ed0a339624364b49f99397026d7030.jpeg)

**"Ned Hill"**

---
---
**Anthony Bolgar** *July 04, 2017 19:44*

Very nice Ned


---
**Alex Krause** *July 04, 2017 20:03*

**+Ned Hill**​ was that some Unicorn spit stain?


---
**Ned Hill** *July 04, 2017 20:11*

Lol **+Alex Krause**.  I read that and was like "what the hell is is talking about, unicorn spit stain?"  Thought it must be a Kansas thing ;)  So I googled it and now understand :P  The stain I used is just a clear water based Minwax stain that I found at Lowes that you can get tinted a variety of colors.


---
**Ned Hill** *July 04, 2017 20:15*

![images/f3aa19e4d3b99f58ed4c4706908d28c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3aa19e4d3b99f58ed4c4706908d28c9.jpeg)


---
**Madyn3D CNC, LLC** *July 12, 2017 18:09*

That looks amazing Ned, great work. 


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/K7PZq8LGy64) &mdash; content and formatting may not be reliable*
