---
layout: post
title: "Well I decided to attend a craft fare for the first time in my life and had a table for 2 days"
date: July 31, 2016 22:16
category: "Object produced with laser"
author: "Tony Schelts"
---
Well I decided to attend a craft fare for the first time in my life and had a table for 2 days.  It was a great experience I made £64.00 over 2 days but it hadn't been advertised publicly.  More importantly I have 4 people wanting me to make things for them to sell. So all in all I'm Happy.

My son took the picture and it looks like I'm sleeping but I'm not,  its not very good quality either.

![images/014f3cbd4795cfea505658537313a208.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/014f3cbd4795cfea505658537313a208.jpeg)



**"Tony Schelts"**

---
---
**I Laser** *August 01, 2016 00:29*

Always nice to hear people making a return on their investment, especially considering the machine is considered hobby level.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 01:34*

Nice work. As I Laser said, it's great to see someone making some money off this hobby of ours.


---
**Victor Hurtado** *August 01, 2016 01:38*

Cheers Tony, first of all congratulating you for your work a favor suddenly your hotel stay or know where I can get designs of laser cut and ready bone vectorized, I'm looking couple arts qu is lamps, boxes of chocolate, liqueurs wines and other materials pair MDF. THANK YOU


---
**Robert Selvey** *August 01, 2016 21:19*

You table looks amazing! what kind of wood do you use for your boxes ?


---
**Tony Schelts** *August 02, 2016 11:54*

3mm Birch Ply, But I am going to make some out of Hardwood.  I have purchased a drum sander that can sand down to 0.8mm if needed, so 3mm shoud be fine. May be too flimsy but Im going to give it a try at stome point.


---
**Pippins McGee** *August 02, 2016 13:22*

looks great, any photo gallery of your products in close-up?


---
**Tony Schelts** *August 02, 2016 15:21*

No, first time trying to sell stuff, several people asked for my bussiness card. So many things I should of Done.  I will do some photos though. I guess my adroid phone will have to do. 


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/2zqF6pXiKZq) &mdash; content and formatting may not be reliable*
