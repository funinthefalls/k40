---
layout: post
title: "Does anyone have a laser cut test file?"
date: May 07, 2015 13:46
category: "External links&#x3a; Blog, forum, etc"
author: "Stuart Middleton"
---
Does anyone have a laser cut test file? Something that does width tests of cut, precise sizes of boxes and circles and large to tiny font tests, that sort of thing? I thought I'd ask before spending the time to design one.





**"Stuart Middleton"**

---
---
**Chris M** *May 08, 2015 20:07*

There's one over at thingiverse

[http://www.thingiverse.com/thing:728579](http://www.thingiverse.com/thing:728579)


---
*Imported from [Google+](https://plus.google.com/+StuartMiddleton1/posts/GcqfvSacgTy) &mdash; content and formatting may not be reliable*
