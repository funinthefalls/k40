---
layout: post
title: "An UPDATE related to my obsession with PWM control on the K40-S :) Realizing that engraving is the most demanding application of PWM control in a K40, I have been working toward finding the correct K40 PWM control operating"
date: January 18, 2017 18:49
category: "Hardware and Laser settings"
author: "Don Kleinschnitz Jr."
---
An UPDATE related to my obsession with PWM control on the K40-S :)

 

Realizing that engraving is the most demanding application of PWM control in a K40,  I have been working toward finding the correct K40 PWM control operating point. 



All mental avenues led me to answering this question. 



"Given the proper PWM control signal on "L". What should the PWM period be set to?"



I know that though empirical testing some have already arrived at the conclusion given below, its not news but it is an encouraging match of findings.



This analysis does not intend to suggest that the PWM settings is the only variable to consider when engraving but it does seek to eliminate the PWM period setting as an unknown.

 

.............. Start of short answer.............................

[Recommendations for Engraving Settings]



Keep the kind of material constant and start your engraving evaluation with these setting using a granular grey shade test pattern.  



PWM period = 200us



Speed= 300 mm/sec



If the quality is not acceptable, independently try the following, while retaining the above PWM period settings;



1. Adjust the max power (Current Regulation) until you find an acceptable level at the lowest grey shade



2. Lower the speed below 300 mm/sec until you find an acceptable level



Try combinations of  1 & 2 above



Speeds above 300mm/sec and PWM periods below 200us are likely to degrade grey scaling.



Your exact results may vary !



.............. End of short answer .................................................



### For inquiring minds read on:



I arrived here by modelling multiple variable factors whose output is shown in the below table. 



The fact that the same PWM period settings were independently identified by both model and empirical results, encourages me as to the models validity. 



Important: The model does show that 20us, which is the default in the smoothie configuration, is not likely to produce good engraving.



The lasers response time has not been thoroughly verified but the model suggests that the K40 should engrave, although marginal at low grey shade Duty Factors, even if the lasers response was as slow as 20us. Well below the 2us response times I measured empirically. 



## If you want to make your head hurt, like it did mine, read on ....



The table below provides the output from one scenario where the laser response is assumed to be 20us. 



From these tables one can gauge how the K40 might engrave at different speeds,  PWM periods, PWM control resolution and laser response settings.



The detail is here: 

[http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html](http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html)





![images/cc4ec35fbfcd405acfa08b0a0d45f65a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc4ec35fbfcd405acfa08b0a0d45f65a.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Ned Hill** *January 19, 2017 01:35*

Very comprehensive and nicely done. Thanks so much for the work and information.


---
**Paul de Groot** *January 19, 2017 05:31*

Will be doing some pwm measurements tonight with a friends CRO digital scope. My results with 10bits and F=300

![images/b42317bc8babbfe8ced5ef07b3e2bac6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b42317bc8babbfe8ced5ef07b3e2bac6.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/YiWfQDnhnVp) &mdash; content and formatting may not be reliable*
