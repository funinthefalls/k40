---
layout: post
title: "This Incredible Wooden Book Is a Series of Puzzles That Have to Be Solved to Continue Reading"
date: October 20, 2016 23:27
category: "Object produced with laser"
author: "HalfNormal"
---
This Incredible Wooden Book Is a Series of Puzzles That Have to Be Solved to Continue Reading

[http://www.thisiscolossal.com/2016/08/wooden-puzzle-book/](http://www.thisiscolossal.com/2016/08/wooden-puzzle-book/)





**"HalfNormal"**

---
---
**greg greene** *October 20, 2016 23:38*

amazing - love to get the plans for that and make one


---
**Jim Hatch** *October 21, 2016 00:57*

I'm a kickstarter backer of the Silenda (he lives sort of nearby in fact - just moved here in the middle of the campaign). I've got his full book and the plans for making my own coming as my reward for backing. Gonna give his to my son as a present and make a couple others with custom stories for my other kids.


---
**Thor Johnson** *October 21, 2016 17:00*

I didn't see it on KS in time, so I've been watching/waiting.  

Their store still isn't up :(


---
**Jim Hatch** *October 21, 2016 19:26*

He's waiting to get his KS orders out before opening the doors to more orders. He showed way more restraint than any Kickstarter I've seen - he figured out how many he could make and shut it down when he hit that number. Every other KS I've seen they just keep it going for the duration and often end up with way more than they can possibly produce in the promised time. Then they're late.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/bz8i3ZMVrS8) &mdash; content and formatting may not be reliable*
