---
layout: post
title: "Hello fellow engravers, I have a question about my k40"
date: July 30, 2017 09:37
category: "Original software and hardware issues"
author: "Tadas A\u0161"
---
Hello fellow engravers, I have a question about my k40. The thing is that I'm concerned about my cutting power. The current regulator has a travel distance as marked by the blue line, but the laser tube starts working only at the point 2nd from the left and if i give it full current it has the current indicator only goes up to 20mA. Is this behaviour normal, or should I be concerned?

![images/0331749952309d2b2b54acd979ffcf65.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0331749952309d2b2b54acd979ffcf65.jpeg)



**"Tadas A\u0161"**

---
---
**Paul de Groot** *July 30, 2017 09:58*

Yes that's normal.


---
**Tadas Aš** *July 30, 2017 12:37*

Okay, two more questions. Is it possible to get through 4mm plywood not getting it as black as coal? And the 2nd is how do i choose the number of passes in corellaser, because it started doing 2 for some reason

![images/bc3d8b7071311ab7b8bee747a4bbdbc8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bc3d8b7071311ab7b8bee747a4bbdbc8.jpeg)


---
**Martin Dillon** *July 30, 2017 21:17*

2 passes is because your line width is too big.  I thing you need to be below .01mm




---
**Bob Buechler** *July 31, 2017 22:39*

**+Martin Dillon** How small can the K40 go in terms of line width?


---
**Paul de Groot** *July 31, 2017 23:27*

0.15 mm so it's more that fine...


---
**Nate H** *August 01, 2017 14:52*

Different plywoods will show different color characteristics (type of wood, type and amount of glue, thickness, etc.)  Some plywood I cut is hardly brown, while other brands are much more black.  Also, a powerful air assist will help cut down on the conventional burn/flair-up which can add soot.  Try different speeds/power ratios.


---
**Bob Buechler** *August 02, 2017 21:16*

**+Paul de Groot** .. but isn't .15mm larger than .01mm?


---
**Paul de Groot** *August 02, 2017 22:00*

**+Bob Buechler** indeed, just mentioning what can be achieved with the K40. It is not a professional tool. 0.15 is good enough for most purposes like cutting and engraving




---
*Imported from [Google+](https://plus.google.com/105123664639790704570/posts/Zp5C5bsqCL1) &mdash; content and formatting may not be reliable*
