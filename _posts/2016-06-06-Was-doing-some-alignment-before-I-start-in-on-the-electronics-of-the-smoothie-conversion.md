---
layout: post
title: "Was doing some alignment before I start in on the electronics of the smoothie conversion"
date: June 06, 2016 23:12
category: "Original software and hardware issues"
author: "Alex Krause"
---
Was doing some alignment before I start in on the electronics of the smoothie conversion. My beam isn't hitting in the same place along the X axis. My Y axis travel yields the beam in the same spot  any suggestions on how to fix this?

![images/d45f475fe205cdbadda42410b1fcda51.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d45f475fe205cdbadda42410b1fcda51.jpeg)



**"Alex Krause"**

---
---
**Stephen Sedgwick** *June 06, 2016 23:28*

You need to actually move the laser mirror just before the lens.  Not the one that actually connected to the lens but the other one, you'll need to slide the full mirror so it'll shoot the laser parallel to that beam.


---
**Alex Krause** *June 06, 2016 23:33*

**+Stephen Sedgwick**​ thank-you :) beam is hitting in same spot now 


---
**Don Kleinschnitz Jr.** *June 06, 2016 23:39*

....its good at the left side? Perhaps the laser tube is not perpendicular to the first mirror and you don't see that  angles error coming off the 2cnd much at the head when it is full left? 



I would try aligning it on the right them move back to the left and verify its off and how far.



Laser needs to be perpendicular to first mirror and it to the second and it to the heads mirror. 



Ideally the laser tube and its beam is parallel to the x axis carriage and the mirrors are theoretically at right angles to them...



This all assumes that the x axis carriage is square to the y axis and the head is tracking true to the slide.


---
**Don Kleinschnitz Jr.** *June 06, 2016 23:44*

**+Alex Krause** By the way do you have micro-switch or optical endpoints on your K40 that our converting?


---
**Alex Krause** *June 06, 2016 23:47*

**+Don Kleinschnitz**​ Micro switch ran thru N/C contacts... hoping it will be less EMF noise


---
**Ariel Yahni (UniKpty)** *June 07, 2016 02:42*

So I did not understand **+Stephen Sedgwick**​ suggestion


---
**Alex Krause** *June 07, 2016 02:45*

Move the  fixed mirror located on the gantry﻿


---
**Pippins McGee** *June 07, 2016 02:51*

**+Alex Krause** when you say move the mirror are you meaning make adjustments to the 3 adjustment screws? or do you mean actually MOVE the mirror physically move the entire mirror and its assembly somehow?



Sorry, I also didn't understand Stephens suggestion and have the same issue as you have (well, had now)



cheers,


---
**Don Kleinschnitz Jr.** *June 07, 2016 02:58*

**+Alex Krause** sorry another question did you replace the optical with the switch.


---
**Alex Krause** *June 07, 2016 02:58*

I used the 3 adjustment screws but if your mirror mount is too far out of whack you may have to square it up 


---
**Stephen Sedgwick** *June 07, 2016 03:07*

Yes the 3 mount screw not mirror screws,  I had the same issue with mine


---
**Christoph E.** *June 10, 2016 13:08*

Why is everyone using receipt tape for this? I also tried and after I was done adjusting everything the mirrors got dirty due to the smoke. So I cleaned them and used chunks of PCBs to adjust it again.


---
**Pippins McGee** *June 12, 2016 12:09*

i found same thing Chris, single layer tape incinerates instantly and left a smoke spot on the mirror.

i start using several layers of tape instead so no smoke being generated on mirror.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/VG7kFQvbQT5) &mdash; content and formatting may not be reliable*
