---
layout: post
title: "What is everybody using for a Biocide?"
date: January 03, 2019 23:07
category: "Discussion"
author: "Sean Cherven"
---
What is everybody using for a Biocide? I don't like the idea of using bleech, so are there any other options I could use?





**"Sean Cherven"**

---
---
**James Rivera** *January 03, 2019 23:54*

This is what I use: Tetra AlgaeControl Water Treatments, 3.38-Fluid Ounce

[amazon.com - Robot Check](https://smile.amazon.com/gp/product/B001D4TNH8/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Sean Cherven** *January 03, 2019 23:58*

**+James Rivera**   That's what I'm using for algae control, but is that also a biocide?


---
**Ned Hill** *January 04, 2019 04:02*

Short answer: Yes, it functions as a biocide.



Longer answer: The tetra algae control uses a polyquaternary ammonium compound as the active ingredient.  Polyquats are effective disinfectants for many simple organisms. 


---
**Sean Cherven** *January 04, 2019 04:04*

**+Ned Hill** Got it! Thanks!


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/R87ij8FLUEm) &mdash; content and formatting may not be reliable*
