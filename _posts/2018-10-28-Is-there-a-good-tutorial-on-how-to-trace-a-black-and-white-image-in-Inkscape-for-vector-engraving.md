---
layout: post
title: "Is there a good tutorial on how to trace a black and white image in Inkscape for vector engraving?"
date: October 28, 2018 16:15
category: "Software"
author: "Martin Dillon"
---
Is there a good tutorial on how to trace a black and white image in Inkscape for vector engraving?  After many tries I got it to work but it went over every line twice.  If I remember right the double cut has to do with the line width.  What does the line width have to be set at to get only one cut?

I am using a stock K40 and K40 Whisperer 





**"Martin Dillon"**

---
---
**HalfNormal** *October 28, 2018 16:45*

Take your pick on YouTube. The secret is finding one that speaks to you!


---
**Scorch Works** *October 30, 2018 02:17*

Inkscape will trace bitmap images but like most tracers it will trace the outlines.  So if you want a simple circle you need to start with a filled in circle.  If the circle you start with is just the outline of a circle the tracer will trace the inside and outside of the pixels that form the outline of a circle.  I think this is the most common difficulty people have with tracers.  Like any tool if you know the limitations it is easier to work with them.


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/JqUjiEGmxye) &mdash; content and formatting may not be reliable*
