---
layout: post
title: "So my new 58W tube has a nice new home in my Redsail LE400"
date: November 12, 2016 00:27
category: "Modification"
author: "Anthony Bolgar"
---
So my new 58W tube has a nice new home in my Redsail LE400. Just need to buy some new tubing to get the water hooked up and give it a test tomorrow. It was longer than it looked, I have 7mm of clearance in the case....but at least I did not need to make a hole in the case for the tube. I think this tube is going to spoil me and make me hate the 30ishW tube that is in my K40. Almost twice the power, and a motorized bed as well. I am HAPPY! (doing snoopy;'s happy dance)





**"Anthony Bolgar"**

---
---
**Stephane Buisson** *November 12, 2016 15:59*

take care of your Snoopy's knee  ;-))


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/QdnPWS2wc1n) &mdash; content and formatting may not be reliable*
