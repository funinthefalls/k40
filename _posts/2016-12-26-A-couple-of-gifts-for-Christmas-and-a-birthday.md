---
layout: post
title: "A couple of gifts for Christmas and a birthday"
date: December 26, 2016 10:08
category: "Object produced with laser"
author: "K"
---
A couple of gifts for Christmas and a birthday. A Dymaxion/Fuller Projection Map cut from 3mm birch w/ 3D printed joints on the inside for my dad, and a sticky note holder for my step-mom who loves flamingos. 



Edit: links to the files. Post-it note holder: [http://www.thingiverse.com/thing:1997297](http://www.thingiverse.com/thing:1997297)

Map: [http://www.thingiverse.com/make:279879](http://www.thingiverse.com/make:279879)



![images/f1a19792329e707c600d071eba4267c8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1a19792329e707c600d071eba4267c8.jpeg)
![images/d9b8cf3921c1a0d3a54589c8c8307286.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d9b8cf3921c1a0d3a54589c8c8307286.jpeg)

**"K"**

---
---
**Jim Hatch** *December 26, 2016 14:40*

Nice. Unique gifts are the best. The laser's ability to customize things for people is great. Nice work. I like the screws you used on the globe. They really set the whole piece off nicely.


---
**3D Laser** *December 26, 2016 19:22*

How are you holding the globe together 


---
**K** *December 26, 2016 19:26*

**+Corey Budwine** The globe uses 3D printed joints on the inside. 

![images/e9272fe3d91327fc5380cae284444731.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e9272fe3d91327fc5380cae284444731.png)


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/LDUmDA6kPdx) &mdash; content and formatting may not be reliable*
