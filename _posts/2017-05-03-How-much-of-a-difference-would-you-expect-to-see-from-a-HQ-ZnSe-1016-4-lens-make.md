---
layout: post
title: "How much of a difference would you expect to see from a HQ ZnSe 101.6/4\" lens make?"
date: May 03, 2017 16:11
category: "Discussion"
author: "Nigel Conroy"
---
How much of a difference would you expect to see from a HQ ZnSe 101.6/4" lens make?

Over the standard 1.5" lens







**"Nigel Conroy"**

---
---
**Steve Clark** *May 03, 2017 20:14*

Difference in what way?



A longer focus point because of a narrower angle of approach so maybe straighter side walls. Perhaps easier to get a narrower cut line.



Some loss of heat intensity in the beam per Ma output perhaps due to having to pass through more air??






---
**Nigel Conroy** *May 03, 2017 20:25*

Sorry I meant to say I'm cut capacity


---
**Ned Hill** *May 04, 2017 00:23*

With a longer focal length you get a longer depth of field which gives you a longer work length before the beam broadens out,  like Steve said.  You won't be able to cut more in one pass, but you can do multiple passes to cut thicker pieces with straighter sides.  Also the standard lens is 2" (50.8mm) not 1.5" :)


---
**Nigel Conroy** *May 04, 2017 00:33*

On the stuff I've read it says the longer focal length can cutter thicker pieces, was curious if anyone had one for comparison



New machine has a 1.5"(40mm) focal length. According to the seller but I'll have to see. 


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/Z2S9fiYSe42) &mdash; content and formatting may not be reliable*
