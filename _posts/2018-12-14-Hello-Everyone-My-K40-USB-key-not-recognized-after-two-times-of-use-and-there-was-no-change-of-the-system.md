---
layout: post
title: "Hello Everyone. My K40 USB key not recognized after two times of use - and there was no change of the system"
date: December 14, 2018 17:14
category: "Original software and hardware issues"
author: "Aart A"
---
Hello Everyone. My K40 USB key not recognized after two times of use - and there was no change of the system. Just stop to working with "need key" message. What is the solution? Thanks!





**"Aart A"**

---
---
**Ned Hill** *December 14, 2018 17:54*

I had this happen to me once and I believe I just changed USB ports.  It just worked out that way for some reason.  The key may not be sitting in the slot all the way or the key may have gotten corrupted.


---
**Adrian Godwin** *December 14, 2018 20:53*

If that doesn't fix it, use K40whisperer instead of the supplied software. It works well (better, in my opinion) and doesn't need a dongle / key.


---
*Imported from [Google+](https://plus.google.com/113668638090739510724/posts/d6aPQu4V1aS) &mdash; content and formatting may not be reliable*
