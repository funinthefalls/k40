---
layout: post
title: "Hey Guys I've noticed that my laser does not turn off when moving between separate paths both when laser cutting and engraving(which probably explains why the engraving just becomes a square of burnt wood) I am using a K40"
date: November 26, 2016 13:08
category: "Discussion"
author: "josh hatfield"
---
Hey Guys



I've noticed that my laser does not turn off when moving between separate paths both when laser cutting and engraving(which probably explains why the engraving just becomes a square of burnt wood)



I am using a K40 machine with no air assist and a grbl(0.9) arudino uno with a gsheild.



Running the most up to date version of laserweb



Has anyone else had this problem or found a solution for it?





**"josh hatfield"**

---
---
**Ariel Yahni (UniKpty)** *November 26, 2016 13:51*

**+josh hatfield**​ do you have the Laser Off under Laserweb settings? 


---
**Don Kleinschnitz Jr.** *November 26, 2016 13:52*

What indications suggest it's not turning off?

How is the arduino pwm connected to the laser power supply?


---
**josh hatfield** *November 26, 2016 14:01*

**+Don Kleinschnitz** 



Hi Don



I have e attached a photo. Notice how there are several diagonal cuts where the beam travelled between each of the holes and slots I was trying to cut out.



I generate a pwm signal from digital pin 11 and then connect it via the red wire to the variable resistor knob output on the laster power supply



![images/a3b179d93b0ec34bf2f22e809ec9c622.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3b179d93b0ec34bf2f22e809ec9c622.jpeg)


---
**HalfNormal** *November 26, 2016 16:04*

Are you using L for on/off or 0 PWM? Do you still have the pot connected? If the pot is still still in the circuit, then you will never be at 0 volts. 


---
**josh hatfield** *November 26, 2016 16:45*

**+HalfNormal** I am sending a pwn signal into the laser power "IN" pin as marked on this image.



Note. I have have gotten the laser to cut at differences intensities with gcode generated on this file.



The laser power varies with the signal but the lines are still cut along the movement of the machine. I think there may be a software issue. I have attached the gcode output file below. It seems the M4 and M3 commands (turn head counter clockwise and clockwise) seem to occur often. M5 is only found at the end of the gcode. The fact that S400 seems to be the default speed throughout the file makes me wonder if something is not working right. Is speed does not change the pwn signal and therefore power won't work either.



I checked my connection before posting this and I noticed that +5v and gnd for the pot where still connected. I removed them but the issue still occurs. 

![images/52e60aa29e17be3aa23f71683106c75d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52e60aa29e17be3aa23f71683106c75d.jpeg)


---
**josh hatfield** *November 26, 2016 16:46*

[drive.google.com - file (7).gcode - Google Drive](https://drive.google.com/file/d/0B8pvPiWhMwS9VVZUUWp4VzdKa0k/view?usp=sharing)


---
**Don Kleinschnitz Jr.** *November 26, 2016 16:55*

I reccommend using the L pin for pwm which must be low true. The L pin requires 5ma so the Arduino should drive it. Set it for pull up and make sure it is LOW true. It should work but I reccommend an open drain MOSFET to further isolate the processor from the HV laser power supply.



Leave the pot in its stock config. nothing connected. If you want full control from the Arduino then set it to full on. Otherwise the power will be the product of pot and pwm duty factors.



More info here. 



[http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-)


---
**josh hatfield** *November 26, 2016 17:21*

Cool thanks I will take a look


---
**HalfNormal** *November 26, 2016 17:52*

**+josh hatfield** **+Don Kleinschnitz** When using an arduino to control the K40, using the IN connection is the easiest and does not need any additional hardware. You get into PPI territory using L. 0 VDC on the IN = 0 output, 5 VDC = Max output without enabling/disabling the laser. GRBL S command should be 0 during moves. If you do not see that then that is the problem. Make sure your GRBL post processor is aware of this. If you are using M4, 5, then you need to connect that to the L input to disable the laser during those commands. 


---
**josh hatfield** *November 26, 2016 18:49*

Awesome thanks guys. I will investigate these suggestions


---
**josh hatfield** *November 26, 2016 19:23*

Awesome. I think should fix it


---
**Don Kleinschnitz Jr.** *November 26, 2016 19:31*

**+HalfNormal** L PWM is not the same as PPI :). Why do you figure IN is easier? Using IN gets you into composite power control territory;).


---
**HalfNormal** *November 26, 2016 20:05*

**+Don Kleinschnitz** L is used by both the M2nano and Moshi for enable only. The power control of the laser is done by the IN. If the PWM signal into L is low enough, you could theoretically out run the efficiency with the x-y travel which cannot be done with the IN input. Also if you look at the installation instructions for aftermarket DSP controllers, the PWM signal goes to the IN input. Using IN, does not need any more control other than a 0-5 vdc signal or additional hardware/software control.


---
**Don Kleinschnitz Jr.** *November 26, 2016 20:50*

**+HalfNormal**​

In a stock k40 the input to L is a pwm signal from the M2 whereas a gnd turns on the laser at a specific DF for a dot or segment time. The max power that it turns it on is manually adjustable at the panel by an analog voltage on IN. 

I have traced the supplies internals and the IN controls an internal pwm that duplicates that of a pwm on L.

When using a pwm control from a controller the internal pwm control of power is only useful for adjusting max thresholds which can also be accomplished by setting limits in the configuration.

Your right that DSP vendors show the use of IN and that confused me for some time. IMO and after looking at the supply internals ..... They are wired wrong ;). That's not to say using IN as a pwm won't work it's just that I do not think it is how the supply was intended to be controlled.


---
**HalfNormal** *November 26, 2016 22:38*

**+Don Kleinschnitz** That is very interesting observation about the M2nano. I will have to check when I am back home about a PWM signal on L. If that is true, I wonder why they do not control power through the software. I have always thought it was just a hard enable (on/off). Second, why have two ways to control power? If you are using L, why bother having IN adjustable at all? Yes you are correct that the powersupplies are very confusing! :-)


---
**Don Kleinschnitz Jr.** *November 27, 2016 15:27*

**+HalfNormal** I have suggested that once a new install is tested that you can tie IN to five volts and control and configure all power settings from software.

I think some may like the comfort of a manual master control knob to use as the laser power decreases or to insure they are not exceeding limits. 


---
**josh hatfield** *November 29, 2016 19:54*

For other people in my situation. I found that updating GRBL to 1.1 and then using M3 as laser on and M5 as laser off made the laser turn off when moving between cutting paths. For some reason I don't think G0 and G1/2/3 produced the desired effect. 


---
*Imported from [Google+](https://plus.google.com/100555832645554213708/posts/WW95XcotjB7) &mdash; content and formatting may not be reliable*
