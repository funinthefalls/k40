---
layout: post
title: "Ok So after 2 weeks of frustration and disappointment with my K40 40w laser cutting machine that stopped working I have had to send my machine back to the ebay seller and get my money back as they would not honour the 2 year"
date: June 10, 2016 20:43
category: "Discussion"
author: "Donna Gray"
---
Ok So after 2 weeks of frustration and disappointment with my K40 40w laser cutting machine that stopped working I have had to send my machine back to the ebay seller and get my money back as they would not honour the 2 year warranty that they advertised with the machine I live in Australia and still would like to buy a 40w C02 laser cutting machine when my money is refunded but as you can imagine I am a little worried about where I should buy from Can anyone give me any info as to where to buy a reliable machine with warranty guaranteed Where have all of you Australian people purchased from??? 





**"Donna Gray"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 10, 2016 20:58*

I purchased on eBay from "amazingitem" [http://www.ebay.com.au/usr/amazingitem?_trksid=p2057872.m2749.l2754](http://www.ebay.com.au/usr/amazingitem?_trksid=p2057872.m2749.l2754)



They refunded me $50 of the total purchase price due to alignment issues that wouldn't sort out simply.



I haven't had any issues requiring warranty or anything yet, so can't comment on that. Although I'm generally not the sort of person to take things back for warranty. Rather I pull them apart & see how they work & see if I can dodgy up a way to make it work.


---
**Pippins McGee** *June 10, 2016 22:33*

I (Sydney Australia) also purchased mine from 'amazingitem' ebay seller in Aust.

[http://www.ebay.com.au/itm/321081981831?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/321081981831?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



However I haven't attempted to contact them again for anything warranty related so I can't comment on their willingness to uphold the advertised warranty.



It was in good and working condition when I got it from them though.

Well, so far anyway..


---
**Donna Gray** *June 10, 2016 22:46*

How long have  you had yours for Pippins McGee & Yuusuf Sallahuddin


---
**Marc Liu** *June 11, 2016 02:32*

I am from China K40 factory. I can offer K40 with 400$ and Good Quality


---
**Donna Gray** *June 11, 2016 02:38*

Yes but is that $400 AUD and will you offer 2 year warranty on all parts Also cost to deliver to Australia


---
**Marc Liu** *June 11, 2016 02:42*

Laser tube 3 months warranty.Do not include shipping


---
**Donna Gray** *June 11, 2016 02:44*

Marc Liu How would you compare a C02 40w laser machine to a 3020 CNC Router?


---
**Marc Liu** *June 11, 2016 02:55*

I am only responsible for sales of factory. laser machine and CNC Router are diff things.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 11, 2016 07:23*

**+Donna Gray** Sorry, I didn't notice you asking how long I've had mine in amongst there. I've had mine since around early-mid October 2015.


---
**Pippins McGee** *June 13, 2016 12:00*

**+Donna Gray** sorry didn't see your question. I've only had it for a just under a month which is not good enough to vouch for the longevity of the life of the parts, but as a whole - the machine had no flaws or faults.


---
*Imported from [Google+](https://plus.google.com/103145403582371195560/posts/go1XsLqXvN7) &mdash; content and formatting may not be reliable*
