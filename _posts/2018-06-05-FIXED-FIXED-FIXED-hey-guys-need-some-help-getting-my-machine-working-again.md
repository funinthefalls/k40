---
layout: post
title: "FIXED FIXED FIXED... hey guys need some help getting my machine working again"
date: June 05, 2018 23:47
category: "Original software and hardware issues"
author: "James poulton"
---
FIXED FIXED FIXED... hey guys need some help getting my machine working again. ive been searching around for ages to no avail. basically ive extended my bed used only original parts apart from adding the extra length on the x azis.. everything was going fine and dandy till ive hooked the mechanics up and now it wont home properly and when i open a file in whisperer and move the image around, when i move left it goes right and visa versa.. ups up and downs down when i move it around in whisperer..  ive looked at my limit switches and they look ok but im no expert on limit switches,does the metal stripes that go in the switches break a beam or something? ive extended the bed in the whisperer softwarethe strip cable is in the correct way also. I'm at a loss now,please help lol......  





**"James poulton"**

---
---
**James Rivera** *June 06, 2018 00:27*

It sounds like you have the stepper motor plugs/wires reversed. As in, flip the cable plug over.


---
**James poulton** *June 06, 2018 08:47*

![images/94cf93f48d5934bcbb7f0a69a5f31e82.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/94cf93f48d5934bcbb7f0a69a5f31e82.jpeg)


---
**James poulton** *June 06, 2018 08:48*

![images/e14e04897e6c7042ae4a4950f011eefd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e14e04897e6c7042ae4a4950f011eefd.jpeg)


---
**James poulton** *June 06, 2018 08:50*

My board an strip wire orientation, the strip wires can only go one way? Wiring facing the pins in the female end. 

![images/5e20bc81619c568f04f29e0fb79548c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5e20bc81619c568f04f29e0fb79548c6.jpeg)


---
**James poulton** *June 06, 2018 09:03*

![images/e47afb574ed47c789f219fc945bb71cd](https://gitlab.com/funinthefalls/k40/raw/master/images/e47afb574ed47c789f219fc945bb71cd)


---
**James Rivera** *June 06, 2018 16:01*

Are you sure you put the X & Y motors where they used to be? In the video you seem to be jogging both axes at once. Can you jog a single axis (use the arrows) and see if it moves the axis you expect?


---
**Scorch Works** *June 06, 2018 16:36*

It looks like you have 2 different problems. 

1. Not homing properly. (This is most likely an endstop issue.)  



2. The x axis is reversed. It looks like you may have switched the setting for home in upper right "on" in the general settings.  That setting reverses the x axis so changing it from your current setting will reverse the x axis drection.








---
**James poulton** *June 06, 2018 19:08*

**+James Rivera** i didnt dissemble the gantry at all other than disconnect cables and take of the x axis rail and replace with a longer one, oh and the dual stepper drive shaft (if thats what its called) if i single jog in using whisperer up it goes up, down/down. but if i single jog right it goes left, visa versa. The corner position arrows in whisperer work the same also, so from normal k40 home (top left) press top right corner it goes to the left on my laser head. if i then go bottom right its now in bottom left as it went left and not right in the second command and so fourth..  i hope you get what i mean :) 


---
**James poulton** *June 06, 2018 19:19*

**+Scorch Works** hey :) im going to install some mechanical endstops i was planing on doing them anyway at some point.  do you happen to know the connector for the endstops and the x axis terminal block on the m2nano? past crimping connectors ive no real idea but trying to learn. 



Are get the end stops done and go from there, i havent changed any setting in whisperer other than extending the bed size and i did that after all the new problems started. 



cheers Scorch 


---
**James poulton** *June 06, 2018 19:19*

just some extra tests

![images/ddd280084d3cd7b6cb7425b87b7970c1](https://gitlab.com/funinthefalls/k40/raw/master/images/ddd280084d3cd7b6cb7425b87b7970c1)


---
**Scorch Works** *June 06, 2018 19:32*

**+James poulton** no, I don't know what the connector names/part numbers are.


---
**James poulton** *June 06, 2018 19:45*

this one i took out the flat cable of the y axis and put the x axis in the y, get me.. ?  :)

![images/f94de132a184a1301854b37151ec7ba1](https://gitlab.com/funinthefalls/k40/raw/master/images/f94de132a184a1301854b37151ec7ba1)


---
**Scorch Works** *June 06, 2018 20:18*

The end stops are wired through the ribbon cable.


---
**James poulton** *June 06, 2018 20:33*

the connectors are JST-XH 4 & 5 pin.




---
**James poulton** *June 07, 2018 08:18*

Im going to take the ribbon out put a stepper motor cable on, an connect the mech end stops to the board via the spare connector on the right of the board above where the 24v goes in..


---
**James poulton** *June 07, 2018 08:19*

Thanks for everyone's input. 


---
**James poulton** *June 12, 2018 09:40*

Update.. i installed the new limit switches and it fixed one problem, its now moving in the correct way when i move the image around in whisperer or use the jog buttons, but still not going home. are upload a new video




---
**James poulton** *June 12, 2018 09:50*

![images/3e234eec536862b84ce7387aa2638364](https://gitlab.com/funinthefalls/k40/raw/master/images/3e234eec536862b84ce7387aa2638364)


---
**James poulton** *June 12, 2018 09:54*

this morning i jumped the limit switches and the laser head is finally going home, how do i go about seeing what wires ive either put in wrong or broke? 


---
**James poulton** *June 12, 2018 09:57*

video of after i jumped the limit switches

![images/29aebbe83c36d03b8d76c7416f0a202b](https://gitlab.com/funinthefalls/k40/raw/master/images/29aebbe83c36d03b8d76c7416f0a202b)


---
**James poulton** *June 12, 2018 12:14*

FIXED.... on my board the limit switches need to be wired grnd and NC.. did that an shes homing like a mother f$%ker.. 


---
*Imported from [Google+](https://plus.google.com/113949094171942001104/posts/jmWJNyfWo9W) &mdash; content and formatting may not be reliable*
