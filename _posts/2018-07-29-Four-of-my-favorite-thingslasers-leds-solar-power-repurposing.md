---
layout: post
title: "Four of my favorite things...lasers, leds, solar power, repurposing"
date: July 29, 2018 18:03
category: "Object produced with laser"
author: "Mike Meyer"
---
Four of my favorite things...lasers, leds, solar power, repurposing. Harbor Freight sells these 50 LED solar ropes for about 10 bucks... I harvested the guts out of it, wired some white LEDs in a piece of PVC and layered them between some old CDs. Laser is a great tool for making repeated, precision cuts. This thing puts out some light, too. Fun little project...



![images/c6d56611eb973b36640f51c536a6204e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c6d56611eb973b36640f51c536a6204e.jpeg)
![images/58e868fbe0ee0af718cb7e1433fa2480.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58e868fbe0ee0af718cb7e1433fa2480.jpeg)
![images/f35ceb8f236a4c0c2ebe6076f81de6c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f35ceb8f236a4c0c2ebe6076f81de6c1.jpeg)
![images/47c118ae91cea52e3f731d27c3fc5c6f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/47c118ae91cea52e3f731d27c3fc5c6f.jpeg)

**"Mike Meyer"**

---
---
**Fook INGSOC** *July 29, 2018 18:28*

KEWL!!!


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/FNiCWGHobZu) &mdash; content and formatting may not be reliable*
