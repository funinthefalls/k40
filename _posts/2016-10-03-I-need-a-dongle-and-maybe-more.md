---
layout: post
title: "I need a dongle!!!! and maybe more"
date: October 03, 2016 17:08
category: "Smoothieboard Modification"
author: "Ric Miller"
---
I need a dongle!!!! and maybe more. 



I blew a few capacitors on my power supply and took the machine apart for some upgrades. I sent the Power supply to an electrical engineering friend of mine and was going to upgrade my bed and such. 



While I had it apart I had a medical emergency and experienced a major seizure. I now have some pretty bad amnesia and cannot remember a ton of stuff. It is very frustrating. 



I am trying to put this machine back together and cannot find my dongle anywhere. Has anyone recently upgraded to a smoothie and no longer need there old board / dongle?



I may just take the plunge and order a smoothie upgrade. 





**"Ric Miller"**

---
---
**Anthony Bolgar** *October 03, 2016 17:21*

You would not regret a smoothie upgrade. I am so happy that I did. But unfortunately I already sold my nano board and dongle. You can purchase dongles on ebay, but they are almost as much as a smoothiebaord.


---
**Ric Miller** *October 03, 2016 18:01*

That is what I was thinking. I was hoping to find one that was not in use so I can at least get up and running again. Which board did you buy?


---
**Ariel Yahni (UniKpty)** *October 03, 2016 18:09*

**+Ric Miller** board will depend on how far you want to go. Smoothieboard 3x can run 3 motors, so 2 for XY and the third maybe a rotary or Z bed. Then the 4x is the same but with 1 more motor and ethernet.




---
**Ariel Yahni (UniKpty)** *October 03, 2016 18:11*

Here are the official store [plus.google.com - Smoothieboard > Original Smoothieware board Original US >…](https://plus.google.com/+ArielYahni/posts/4HDF9ZA8rfX)


---
**Ric Miller** *October 03, 2016 18:24*

Is the LCD add on worth it?




---
**Ariel Yahni (UniKpty)** *October 03, 2016 18:52*

Not until recently that major laser specific updates where done to it. 


---
**Ariel Yahni (UniKpty)** *October 03, 2016 18:55*

[https://plus.google.com/101797329177621784159/posts/cNf8snxyU7z](https://plus.google.com/101797329177621784159/posts/cNf8snxyU7z)


---
**Ric Miller** *October 03, 2016 19:01*

That is very new. Maybe I shouldn't. I am trying to stay on a budget since I cannot work right now.


---
**Ric Miller** *October 03, 2016 19:57*

Thanks Peter


---
**Ric Miller** *October 04, 2016 20:59*

Shortly after I ordered the smoothie board I found it. :)![images/585441c8cb9c9f0bf62a36b234ce4695.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/585441c8cb9c9f0bf62a36b234ce4695.jpeg)


---
**Anthony Bolgar** *October 04, 2016 22:43*

Isn't that a kick in the pants.


---
**Ric Miller** *October 04, 2016 23:34*

**+Anthony Bolgar** I kept dreaming about it last night. :)


---
*Imported from [Google+](https://plus.google.com/102361688882180483990/posts/jK5q8y7Lrt3) &mdash; content and formatting may not be reliable*
