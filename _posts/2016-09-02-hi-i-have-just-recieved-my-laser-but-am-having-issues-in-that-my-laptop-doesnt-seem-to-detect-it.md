---
layout: post
title: "hi i have just recieved my laser but am having issues, in that my laptop doesnt seem to detect it"
date: September 02, 2016 19:17
category: "Discussion"
author: "Martin McCarthy (NGB Discos)"
---
hi 

i have just recieved my laser but am having issues, in that my laptop doesnt seem to detect it. I have installed the drivers (as far as im aware) and still nothing I believe its a faulty motherboard, i was just wondering if anybody else had encounterd the same issue , and also i read somewhere else that there is a diode on the motherboard that lights up when a device is connected, is this true as i cant see one . Thanks in advance.





**"Martin McCarthy (NGB Discos)"**

---
---
**Alex Krause** *September 02, 2016 20:55*

Did you plug in the dongle that came with the machine it looks like a thumb drive


---
**Martin McCarthy (NGB Discos)** *September 02, 2016 21:51*

yes have that plugged in but my laptop doesnt seem to detect the machine 




---
**Alex Krause** *September 02, 2016 21:53*

Have you input the serial number of the board into the settings and choose the correct motherboard as well?


---
**Alex Krause** *September 02, 2016 21:53*

Are you running on a Mac Book or a Windows laptop


---
**HP Persson** *September 03, 2016 08:38*

If you are running Windows10 with latest updates, it doesnt accept unsigned drivers. Noticed that with my laptop, only when the drivers were there before the update it works.



But i got the sound that something was connected, but no drivers were accepted.




---
**Thor Johnson** *September 04, 2016 03:59*

On mine, the main chip <b>wasn't there</b> and neither was the USB-RS232 chip (not sure what happened, so I went back to the ebay seller and he (grudgingly) sent me a new board.  It should show up as USB device even if the drivers won't load (and most times they will because it just shows up as a serial port -- the dongle OTOH, shows up as a HID device, so the drivers should be golden for that too).




---
**Martin McCarthy (NGB Discos)** *September 04, 2016 11:09*

**+Alex Krause**  is the serial number the white sticker on the board?




---
**Martin McCarthy (NGB Discos)** *September 04, 2016 11:13*

im not getting any sound at all when i plug it in 




---
**Martin McCarthy (NGB Discos)** *September 04, 2016 11:47*

yeah i think something broke on the board as its not detecting anything being plugged in and have a tried a couple of different cables and 3 different laptops and nothing happens when you plug it in 




---
*Imported from [Google+](https://plus.google.com/108373254461031274579/posts/3tgn7vFKsgh) &mdash; content and formatting may not be reliable*
