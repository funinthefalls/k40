---
layout: post
title: "I truly apologize for what will appear to be my lazy request here, but after some searching on this forum, the modifications section, as well as lightobject's website, I have not found a lot of detail on the LO air assist"
date: July 05, 2016 17:13
category: "Modification"
author: "Jeremy Hill"
---
I truly apologize for what will appear to be my lazy request here, but after some searching on this forum, the modifications section, as well as lightobject's website, I have not found a lot of detail on the LO air assist install.  It seems pretty straight forward but the size of the air assist pieces are larger than the hole that is in the original mounting piece.  I suspect the straight forward element of this is to cut that hole larger to fit, but wanted to see if that truly was all that I needed to complete?  Thank you.





**"Jeremy Hill"**

---
---
**Carlos Barraza** *July 05, 2016 17:23*

Mine is a light object and is the correct size. It comes apart into 4 pieces total 3 that mount to the plate and then the air cap.


---
**Carlos Barraza** *July 05, 2016 17:24*

The threaded part will unscrew from the mirror holder and you will see a smaller set of threads.but have photos if needed


---
**Ariel Yahni (UniKpty)** *July 05, 2016 17:27*

**+Jeremy Hill**​ there should be no reason to enlarge the hole unless your version of the factory hole is smaller wich I doubt. The LO air assist splits in 3 pieces. The bottom part is just a screw for the hose from the air supply. The other in the middle is where you place the focus lense and it's goes top and bottom of the metal plate in the gantry 


---
**Jeremy Hill** *July 05, 2016 17:27*

I took apart each of the pieces and none will fit through the hole in place.  Perhaps I received the wrong piece.  I ordered the part from LO under the Dk40 upgrade section.  My concern is cutting the plate and having a small/thin section on the front of it remaining.  It might be a bit too small for stability.


---
**Jeremy Hill** *July 05, 2016 17:29*

When I get back home, I will try to get the details/sizes.  I understand I won't be able to post pics but can work through that to show the issue.


---
**Ariel Yahni (UniKpty)** *July 05, 2016 17:35*

You can post pics in this thread via [imgur.com](http://imgur.com) service


---
**Jeremy Hill** *July 05, 2016 17:36*

ok. thank you.  I will give that a try when I get my photos.  I appreciate the feedback.


---
**greg greene** *July 05, 2016 17:41*

I will be getting my air assist this week, so please post pictures of how it is suppossed to go together


---
**Jim Hatch** *July 05, 2016 18:08*

**+Jeremy Hill**​ The bottom piece (after you take the cone shaped piece off) will also unscrew. It's very tight the first time. You'll think it won't come apart but it will. 


---
**HalfNormal** *July 05, 2016 19:31*

**+Jeremy Hill** If you look at the picture in this post

[https://plus.google.com/100825332636790645093/posts/ikXMfUQhCWh](https://plus.google.com/100825332636790645093/posts/ikXMfUQhCWh)

or the picture here

[https://www.amazon.com/18mm-Laser-Assisted-Ideal-Machine/dp/B0094WLPYK](https://www.amazon.com/18mm-Laser-Assisted-Ideal-Machine/dp/B0094WLPYK)

You can see there are threads between the the mirror holder and the next set of threads. I took a set of pliers padded so it would not damage the threads on the larger part and then screwed off the lens assembly.


---
**Jeremy Hill** *July 05, 2016 19:58*

[http://imgur.com/a/40uYL](http://imgur.com/a/40uYL)

[http://imgur.com/clp9BKg](http://imgur.com/clp9BKg)

[http://imgur.com/sMwzwZQ](http://imgur.com/sMwzwZQ)

[http://imgur.com/eTNlbn8](http://imgur.com/eTNlbn8)

Hopefully this works.  Here are the photos of the issue


---
**Ariel Yahni (UniKpty)** *July 05, 2016 20:10*

Defenetly you've got something out of the ordinary


---
**Ariel Yahni (UniKpty)** *July 05, 2016 20:29*

I would suggest you find or design that piece and cut it from 3mm acrylic


---
**Jeremy Hill** *July 05, 2016 21:42*

Ok got it! The smaller thread was definitely hidden and tight but it was there. Thank you again for the feedback. I would definitely been cutting the hole of you didn't give me the feedback to look at it again. 


---
**Jeremy Hill** *July 05, 2016 22:22*

[http://imgur.com/3BKP6oS](http://imgur.com/3BKP6oS)


---
**greg greene** *July 05, 2016 23:53*

Thanks Jeremy !


---
**Jim Hatch** *July 06, 2016 00:48*

Since it has to come apart to install, I don't know why it ships so tight. You'd think the parts that would need to be removed would be somewhat loose.


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/AyGJnHpSw3L) &mdash; content and formatting may not be reliable*
