---
layout: post
title: "Does anybody know why my black fan at the back on the control box area isn't spinning?"
date: July 21, 2015 07:47
category: "Discussion"
author: "george ellison"
---
Does anybody know why my black fan at the back on the control box area isn't spinning? Or do none of them work anyway? Thanks





**"george ellison"**

---
---
**Fadi Kahhaleh** *July 21, 2015 16:47*

Follow the power leads coming out from it, where do they go to? can you post a pic of the connector/pins that they hook up to?

(it should work, mine does anyways)


---
**Jon Bruno** *July 21, 2015 18:43*

Funny you should post this.. I had the same question last night. Mine just recently seems to have started to turn on/off on it's own .. I wonder if there is a temperature sensor in it or something is faulty....

it was off (or quiet) when I began a job and seemingly ramped up to full a few minutes later. I think I'm going to  check and see if it's contaminated and/or maybe stuck (sticky)?


---
**Fadi Kahhaleh** *July 21, 2015 19:10*

You had me run down to the basement to double check! It turned on as soon as I switched on the machine.


---
**David Richards (djrm)** *July 21, 2015 19:24*

Hi George, is the fan you mention on the power supply box. I think my setup could be different from yours. The only regular fan apart from the main extractor is inside the case in the metal power supply box. This fan is always on as far as I know, it blows out from the side of the case. I'm thinking or turning this one so it sucks in to keep more of the smell in.


---
**george ellison** *July 22, 2015 07:44*

Will have a look and post a pic, thanks for the comments. :)


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/PGMGMzW73xh) &mdash; content and formatting may not be reliable*
