---
layout: post
title: "Question: I am wanting to figure out a cheap upgrade/change for the laser Amp display/pot - I would like to go with the digital one you are seeing in some of the newer/little more expensive models - however I haven't been"
date: June 07, 2016 19:39
category: "Discussion"
author: "Stephen Sedgwick"
---
Question:  I am wanting to figure out a cheap upgrade/change for the laser Amp display/pot - I would like to go with the digital one you are seeing in some of the newer/little more expensive models - however I haven't been able to find a good source for the control/display... any ideas?  Something similar to the one below - it doesn't have to be integrated controls however they would be nice to have...

![images/432fcf9ed687dfcba193c375de9fbb3f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/432fcf9ed687dfcba193c375de9fbb3f.jpeg)



**"Stephen Sedgwick"**

---
---
**Jim Hatch** *June 07, 2016 20:23*

LightObject has a digital display available.


---
**Stephen Sedgwick** *June 07, 2016 21:40*

**+Jim Hatch** I wasn't even thinking it was a 20A that was being used.... lol, thank you that helped a ton :-)


---
**Jim Hatch** *June 07, 2016 21:59*

I use LO for a lot of stuff. Quick shipping, decent prices and never any issues.


---
*Imported from [Google+](https://plus.google.com/118053372455470937565/posts/284Dwhvo7ij) &mdash; content and formatting may not be reliable*
