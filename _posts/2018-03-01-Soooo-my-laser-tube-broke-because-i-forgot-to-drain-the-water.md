---
layout: post
title: "Soooo my laser tube broke because i forgot to drain the water"
date: March 01, 2018 09:00
category: "Hardware and Laser settings"
author: "Dag Elias S\u00f8rdal"
---
Soooo my laser tube broke because i forgot to drain the water. 



Anyway when looking at my tube I found that it's actually 800mm and not 1000. I thought i bought a 50w laser...

(My laser is a bit lager than the k40 ones.



So the question now is should I go for a 800 or 1000 replacement tube?



How will it effect my PSU?



And also, anyone have experience with eBay sellers. I live inn Norway so a rugged packaging is needed.



Just to let you guys know, I have had this tube for probably 4-5 years.



![images/3fa7041636389182dbc92f269ed75724.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3fa7041636389182dbc92f269ed75724.jpeg)
![images/a1d76345743a2578d72b165ec19dee79.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a1d76345743a2578d72b165ec19dee79.jpeg)

**"Dag Elias S\u00f8rdal"**

---
---
**Don Kleinschnitz Jr.** *March 01, 2018 13:02*

There are a number of K40's in this community running tubes that are wattage rated above the LPS specs. 

I would expect it will work but you will not get full power.



[donsthings.blogspot.com - Will a 40 Watt LPS Drive a 60W Laser?](http://donsthings.blogspot.com/2018/02/will-40-watt-lps-drive-60w-laser.html)


---
**Anthony Bolgar** *March 01, 2018 13:26*

My 50W PSU drives a 60W tube without a problem. I get the full power (measured it with a power meter)


---
**Phillip Conroy** *March 01, 2018 20:30*

Most laser power supplys are rated for a range of tubes ie 40 to 80watts


---
*Imported from [Google+](https://plus.google.com/+DagEliasSørdalurm8/posts/SvwTA42tDvf) &mdash; content and formatting may not be reliable*
