---
layout: post
title: "A very clever rotary design that uses no motor, software setup etc"
date: October 21, 2017 13:30
category: "External links&#x3a; Blog, forum, etc"
author: "Don Kleinschnitz Jr."
---
#K40Rotary



A very clever rotary design that uses no motor, software setup etc.

It pushes the frame in the Y which in turn rotates the object being engraved!




{% include youtubePlayer.html id="mCQqn0kfvCo" %}
[https://www.youtube.com/watch?v=mCQqn0kfvCo&t=27s](https://www.youtube.com/watch?v=mCQqn0kfvCo&t=27s)





**"Don Kleinschnitz Jr."**

---
---
**Jorge Robles** *October 21, 2017 16:35*

My k40 is thriving for it :))


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/HscBw6RAghc) &mdash; content and formatting may not be reliable*
