---
layout: post
title: "Does anyone have any solutions for algae control"
date: March 20, 2017 12:37
category: "Discussion"
author: "craisondigital"
---
Does anyone have any solutions for algae control. I try to Change the water often, but algae seems to build up quick.





**"craisondigital"**

---
---
**Andy Shilling** *March 20, 2017 13:02*

I've seen some posts talking about chlorine in the past but i don't know if that turned out ok, i run a 25 litre drum of distilled water and the container if blacked out from any type of light. I base this on the same principle of my fish aquarium to much light= shed load of algae.


---
**greg greene** *March 20, 2017 13:23*

a few ounces of rubbing alcohol in the distilled water works for me


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 20, 2017 13:32*

There have been many discussions on this topic since I joined this group (about 1.5yrs ago). Most recent there was a discussion of specifically what NOT to put in the water.



Unfortunately I can't seem to find the exact post in the archive here, but I'm sure it's still there. The discussion was on how various additives people are using affect the laser tube.



Actually, I did find it (just remembered that RV Antifreeze was a part of the discussion):



[plus.google.com - COOLING WATER "IT MATTERS" After flailing for a few days with what I thought...](https://plus.google.com/113684285877323403487/posts/jDRGVhd6zqy)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 20, 2017 13:33*

There was also mention somewhere (that post or maybe another I saw just now) of someone utilising an aquarium UV filter (which is supposed to kill algae I think). Others have said Chlorox, Bleach, & various other things I cannot remember.


---
**Ned Hill** *March 20, 2017 13:44*

If you are having a lot of algae growth that means your water reservoir, and or tubing, is getting too much light.  Shield the reservoir from both direct and indirect light and that should cut down on the algae.  You can also add about 6mL bleach to 5gal of water.  We don't recommend concentration much higher than that due to increase conductivity.


---
**Don Kleinschnitz Jr.** *March 20, 2017 14:21*

Here is the core post with data and ongoing testing: 

[donsthings.blogspot.com - Laser Tube: Protecting, Operating, Cooling & Repairing](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
*Imported from [Google+](https://plus.google.com/113025010074387144690/posts/RgUB7NHDW2y) &mdash; content and formatting may not be reliable*
