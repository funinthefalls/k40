---
layout: post
title: "Ok guys. So I got the machine yesterday and started setup"
date: May 11, 2016 14:26
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Ok guys. So I got the machine yesterday and started setup.  By all means dealing with the software is he'll,  but anyway I had other more important issues. Laser would not fire. Did some checking on all connection without success. Then this morning with fresh eyes I see this. So I can assume it's dead? 

![images/486dc57cce9923ecb00cfb565a8ffb03.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/486dc57cce9923ecb00cfb565a8ffb03.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Alex Krause** *May 11, 2016 14:28*

Time to send global free shipping a message DOA


---
**3D Laser** *May 11, 2016 14:37*

Cracked tube is a dead tube 


---
**Joe Keneally** *May 11, 2016 14:42*

Looks like a catastrophic failure of the outer water jacket!  Bummer Dude!




---
**Ariel Yahni (UniKpty)** *May 11, 2016 14:46*

It hurts so badly. I did send them a message yesterday regarding laser not firing and this morning they replaced asking for videos, etc so I just sent them this picture 


---
**Gunnar Stefansson** *May 11, 2016 14:52*

Oh man... that's sad... Good luck getting the replacement, hope it goes fast.


---
**Alex Krause** *May 11, 2016 14:53*

You should send a video about your homing issue as well **+Ariel Yahni**​


---
**Brandon Satterfield** *May 11, 2016 15:48*

crap!


---
**Martin Carney** *May 11, 2016 16:00*

For those who bought on eBay and had a problem like this on arrival, how smoothly did the warranty /replacement process go?




---
**Ariel Yahni (UniKpty)** *May 11, 2016 16:02*

Just to confirm that there are no other issues regarding the machine, if the tube is cracked I should not expect any reading from the ammeter  correct? 


---
**Joe Keneally** *May 11, 2016 16:06*

**+Martin Carney** i am working with my vendor to get my PSU replaced. It should be coming....


---
**Ariel Yahni (UniKpty)** *May 11, 2016 17:02*

Is the general solution to get a refund? What's the average refund I should get? I'm trying to source a tube that can be DHL directly to me


---
**Alex Krause** *May 11, 2016 17:21*

**+Ariel Yahni**​ try looking for a 30-35w tube that's what is actually in these machines 


---
**Ariel Yahni (UniKpty)** *May 11, 2016 17:32*

Thanks **+Alex Krause**​ would it make sense to get something a little more powerful. What's the top that the psu can handle


---
**Alex Krause** *May 11, 2016 17:35*

Not sure of the PSU but as wattage goes up on the tube they get longer they make a case extension for larger tube but it's kinda pricey for what it is in my opinion


---
**Alex Krause** *May 11, 2016 17:39*

**+Scott Thorne**​ might be able to answer your PSU question I'm pretty sure he has a 50w tube


---
**Scott Thorne** *May 11, 2016 19:09*

**+Ariel Yahni**...the psu at light objects is 235.00...and it's huge...but it works good...I got the 50 watt tube from laser depot for 275.00


---
**Ariel Yahni (UniKpty)** *May 11, 2016 19:14*

Thanks **+Scott Thorne** wow. that's way off my budget. I will need to make it pay for itself first. Need to find a reputable source to get the new tube fast and cheap


---
**Scott Thorne** *May 11, 2016 20:51*

**+Ariel Yahni**...you can contact the seller...good luck though...the seller is bought my k40 from gave me a complete run around...but I've heard good things about other sellers.


---
**Ariel Yahni (UniKpty)** *May 11, 2016 20:54*

**+Scott Thorne**​ was it globalfreeshiping? 


---
**Scott Thorne** *May 11, 2016 21:21*

That's it. 


---
**Greg Curtis (pSyONiDe)** *May 11, 2016 22:26*

I got the same tube from laser depot, and used a 4" steel duct for the protrusion. I uploaded pics in the mods section.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 23:12*

**+Ariel Yahni** That's unfortunate that your adventure begins in this manner. I hope it gets sorted for you soon (at minimal out of pocket expense).


---
**I Laser** *May 11, 2016 23:30*

Interesting question posed by OP that seems unanswered would the mA meter still read a voltage even if the tube is dead?


---
**Greg Curtis (pSyONiDe)** *May 11, 2016 23:32*

I'm sure someone knows for sure, but I'd think not. No gas to conduct the electricity, no current to measure.﻿



When I replaced my tube thinking it had lost its CO2 charge, it was arcing from the input to the frame. I replaced it and it was still doing this.



I found out I didn't do a good enough job sealing the electrode and it was arcing through the silicone I used. Peeled and re-sealed and left to dry overnight and it was all better.



I do recall the meter registering mA, when trying to fire, but it was a very startling sound and was an immediate indication of a serious problem.﻿


---
**I Laser** *May 11, 2016 23:34*

Thought that would be the case, given it's a circuit with the tube in between, just not sure and no one has provided an definitive answer yet. :)


---
**Alex Krause** *May 12, 2016 03:58*

The amp meter is on the return side of the wiring so that makes sense


---
**Ariel Yahni (UniKpty)** *May 12, 2016 11:56*

So 5h ago ( this seller replays from China which make communication slow)  responded to my various emails with pictures and information I sent regarding tube broken, homing issue to wrong direction,  and crashed exhaust saying "It seemed the laser tube broken.

We can send you a new replacement tube.

Please let us know.

Thank you for your understanding"  how can a seller have such a positive feedback with an answer like that


---
**Alex Krause** *May 12, 2016 13:12*

That's awesome that they are willing to send you a new tube. But what about the homing issue?


---
**Ariel Yahni (UniKpty)** *May 12, 2016 13:23*

It's good and bad. They did not answer anything else, but if it take them a day for every answer it's going to take forever


---
**Scott Thorne** *May 12, 2016 20:39*

**+Ariel Yahni**..they offered to send me a new tube also...but only after I sent them the broken one...then they wanted to wait until they received it then send a new one to me...takes around 5 to 6 weeks for the whole process.


---
**Greg Curtis (pSyONiDe)** *May 12, 2016 20:57*

I agree with the language barrier thing as well as the time zone factor. Sounds like a good response, albeit any shipping time involved.


---
**Anthony Bolgar** *May 12, 2016 21:20*

Open a paypal dispute, it will light a fire under their butts!


---
**Scott Thorne** *May 12, 2016 21:24*

**+Anthony Bolgar**...I did  and was told that I had to return the machine at my cost...lol....I'll never order from ebay again...at least amazon has 100% refund regardless of circumstances. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 22:42*

**+Ariel Yahni** When dealing with these Chinese sellers, I find that they rarely answer all questions you ask. They seem to generally only ever answer either the first or the last question you ask. I understand it's a language barrier issue, but as you mentioned it can make getting a satisfactory result very time consuming.



Something I've learned when dealing with online sellers (even Australian sellers) is that they are all pretty pathetic when it comes to reading what you have asked. So, to combat this issue I have taken to listing my questions in the message as such:



1. Question 1 that I want them to answer?

2. Question 2 that I want them to answer?

3. etc.



In the hopes that due to the formatting of the communique, it maybe makes it easier for them to spot the actual questions they need to answer.


---
**Scott Thorne** *May 12, 2016 22:49*

**+Ariel Yahni**...let me know if the power supply is bad...if they won't replace it...I'll donate the 40 watt supply that came out of my machine.


---
**Ariel Yahni (UniKpty)** *May 13, 2016 13:10*

Day 3 update response from seller "We can get you replacement tube and exhaust but we don't see your video about homing issues" I sent that 2 days ago. I need to corner this guy into issuing a refund


---
**Alex Krause** *May 13, 2016 14:18*

Post the video of homing to YouTube then send them the link


---
**Ariel Yahni (UniKpty)** *May 13, 2016 14:21*

Yeah did that, shared a folder from G but I would be very unproductive for them not to have resolved that issue while make business with the rest of the world." Assumption is the mother of all fuckups "


---
**Ariel Yahni (UniKpty)** *May 14, 2016 11:52*

Day 4. To the surprise of all no answer today. I think it's time to issue a complain


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 14, 2016 21:26*

**+Ariel Yahni** Not sure you can really count yesterday as a day since it's weekend. They may not work weekends. However, it might be worth starting a paypal complaint so that they sort it out for you quicker.


---
**Ben Walker** *May 16, 2016 11:55*

How they keep their ratings up: intimidation.  If you give a low rating they will get right on to getting things settled but will badger to no end to get you to give them 5 stars.  They will threaten and coerce to no end and believe me you will get a lot of communication from that end.  Seems they are more concerned with those stars than profit.


---
**Ariel Yahni (UniKpty)** *May 16, 2016 11:55*

Day 6 response "According to our supplier, the problem should be the limit switch of the Y axis.

Please take down the limit switch and check whether it was damaged during transit or not.

If it was damaged, we may need to resend you a new limit switch.

Thanks for your cooperation." so a damaged limit switch is making the Y axis go the other way? Mmm interesting




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 16, 2016 12:06*

**+Ariel Yahni** This may sound a bit weird, but could the Y-axis motor be plugged in backwards to the controller? Not sure if the motor would run with reversed polarity, but maybe it is causing it to run backwards.


---
**Ariel Yahni (UniKpty)** *May 16, 2016 12:25*

**+Yuusuf Sallahuddin** Unless im wrong, that should be the reason, dont think the limit switch could have anything to do. I dont want to touch it yet until im further into the dispute


---
**Ariel Yahni (UniKpty)** *May 19, 2016 11:39*

Update Day "lost the count" they offered me 30% refund I said no that at least need 300.now they say half. The thing here is that half won't cover the new tube and they say is final offer. Recommendations? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 20, 2016 01:37*

**+Ariel Yahni** Could maybe see if they are willing to send a new tube? That would probably work out cheaper for them & you'd get a new tube.


---
**Ariel Yahni (UniKpty)** *May 20, 2016 02:32*

**+Yuusuf Sallahuddin**​ allready bought the tube,  so refund is the only way for me. The tube cost me more than what they are offering


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 20, 2016 03:41*

**+Ariel Yahni** Ah fair enough. That makes sense then. Well if it is not a huge difference between what they offer back & what the tube replacement cost, then I would take the loss. If it is a large difference, I would try negotiate more. Maybe sending them a copy of the invoice for the new tube's cost.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/28UUoEz9Ed5) &mdash; content and formatting may not be reliable*
