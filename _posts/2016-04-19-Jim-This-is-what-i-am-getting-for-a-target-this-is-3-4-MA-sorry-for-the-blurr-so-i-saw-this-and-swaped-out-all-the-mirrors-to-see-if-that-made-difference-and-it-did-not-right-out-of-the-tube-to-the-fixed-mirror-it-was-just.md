---
layout: post
title: "Jim This is what i am getting for a target this is @3-4 MA sorry for the blurr so i saw this and swaped out all the mirrors to see if that made difference and it did not, right out of the tube to the fixed mirror it was just"
date: April 19, 2016 23:18
category: "Discussion"
author: "Dennis Fuente"
---
Jim 

This is what i am getting for a target this is @3-4 MA sorry for the blurr so i saw this and swaped out all the mirrors to see if that made difference and it did not, right out of the tube to the fixed mirror it was just one dot but from that mirror to the next this is what i get.

 between the fact the machine won't cut 3 MM ply and the fact the software won't send certain files to be cut it realy sucks

![images/798b3890089cbeb4bbf62d435fd9af59.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/798b3890089cbeb4bbf62d435fd9af59.jpeg)



**"Dennis Fuente"**

---
---
**Scott Thorne** *April 19, 2016 23:44*

Looks like it's not hitting the center of the 1st mirror....I've had that happen to me when I installed the new tube in my machine. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 19, 2016 23:53*

Or it could be similar to the issue I am having currently. First mirror hits dead on centre, but from there the 2nd mirror hits at 2 different spots when at the front & back of the rail. Mine is less severe, but it is related to the beam coming off the 1st mirror not parallel to the rail (or 2nd mirror in both front & back position). I originally had this issue when I first got my K40 & solved it by levelling the 1st mirror (by putting some washers under it before screwing it's housing down). It seems that some screws at the back of the 1st mirror mount are touching the U-shaped mount below it, that forces it to angle downwards (on mine).


---
**Dennis Fuente** *April 20, 2016 00:07*

well it is little off the center not by much how could that cause it to have a split beam from the first fixed mirror to the y mirror.

 Yuusuf 

leveling the first mirror to what the frame rail or, i have had enough head banging for today i will have at it again tomarrow 


---
**Jim Hatch** *April 20, 2016 00:47*

**+Yuusuf Sallahuddin** What Yuusuf said :-) I actually had a bear of a time getting the Y-Axis mirror aligned correctly. I finally had to shim under the fixed mirror's mount. Then I adjusted the mirror adjusting knobs. Without the shims the mirror adjustment was really skewed to make it center on the Y-Axis mirror. 



You could also pop the original mirror back in (you said you had replaced the mirrors with LO ones so you can verify that it's not a mirror problem by checking the original). Or swap out the fixed mirror and see if that's the culprit. Those are quick to check. 



Three good pieces of news though. It's not your lens, in fact you now know two mirrors and the lens are not the problem and once you get it fixed that's going to fix the cutting issue. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 20, 2016 04:10*

**+Dennis Fuente** I didn't register that was from the one firing. I thought it was from firing at 2 different positions. So my previous response isn't related to the beam splitting. I am unsure as to why the beam splits, but I just had the same thing happening with mine over the last couple of days (with the beam splitting). I don't know what caused it, but after realigning my mirrors it has stopped splitting the beam in 2. Mine was splitting after the 3rd mirror, just before the lens. I have a feeling it was related to the mirror/lens head being slightly rotated & the beam was hitting the 3rd mirror high, instead of the centre. After cleaning mirrors & lens & fixing the alignment so it hit centre, it seems to have no splitting now.


---
**Phillip Conroy** *April 20, 2016 11:09*

Turn up the power a bit at a time and see what u get-mine outputs a doughnut shape and cuts 3mm mdf at 8ma like butter-even getting focased beam down to 0.1mm 


---
**Phillip Conroy** *April 20, 2016 11:12*

Just remmebered that beam shapes have been talked about before -a single spot beam is em0 -doughnut shape beam is em1 -both are normal do a google shearch for laser beam shapes and em1


---
**Phillip Conroy** *April 20, 2016 11:22*

[https://www.rp-photonics.com/modes.html](https://www.rp-photonics.com/modes.html)

Forgot to add t befor the mode eg tem0 is spot-tem1 is doughtnut shape


---
**Phillip Conroy** *April 20, 2016 11:35*

Another link to describe laser beam shapes

[https://en.wikipedia.org/wiki/Transverse_mode](https://en.wikipedia.org/wiki/Transverse_mode)


---
**Dennis Fuente** *April 20, 2016 16:36*

Hi Guys so back to the head banging i searched videos on utube for mirror alignment didn't see were anyone described that the first mirror should have the beam hit it dead center i only saw where they moved the first mirror to hit the Y mirror on center and the X mirror so am i missing something, also how high a MA setting do you use to cut 3MM pop ply.



thanks 

Dennis 


---
**Scott Thorne** *April 20, 2016 16:55*

**+Dennis Fuente**....I can cut it on 6 mA.


---
**Jim Hatch** *April 20, 2016 17:05*

mirrors can have spherical (or aspherical) aberrations too so it's best to hit dead on center if you can. That may mean you need to adjust the mirror mount where it attaches to the frame of the machine. There are 2 screws holding the plate that the mirror is attached to. I had to raise mine about 4mm to get it to hit correctly.



There's a distinction between "alignment" and "aiming". The first is rough (get it pretty near the middle) and the 2nd is the precision step - get it dead center.


---
**Jim Hatch** *April 20, 2016 17:06*

You use the mounts to get it aligned and the brass screws to aim it.


---
**Dennis Fuente** *April 20, 2016 17:23*

Thanks guys for your response and help question should the first mirror next to the tube should the beam be centered on it or is it used to align the beam to hit the Y mirror 


---
**Jim Hatch** *April 20, 2016 17:33*

both :-)



Remember small errors close to the laser translates to large errors as you get further away & carom off on a 90 degree angle into the focusing head.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 20, 2016 22:22*

Yeah totally agree with Jim's last statement. Small errors closer to the tube results in larger errors further.



Also, I recently had it hitting too high on mirror 3 & it was causing it to cut angled on the work piece, resulting in requiring much higher power to cut through 3mm ply (well more passes since I don't like to turn power above 10mA). 


---
**Dennis Fuente** *April 20, 2016 22:26*

Did you see the picture i just posted of my tube end 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 20, 2016 22:31*

**+Dennis Fuente** I was just looking at it. Going to comment on it.


---
**Dennis Fuente** *September 28, 2018 19:39*

i shelved my K40 for a time the software just didn't work well now i have light burn and it just works so back to my laser, so here i go again trying to get the dam mirrors aligned i have the first mirror dead on center as the next order mirror i get it center then the beam won't even hit the head way off doesn't even hit the head out in space i have leveled the gantry and aligned to square up with the side wall check the tube is square just can't get the dam second mirror to the head aligned and i can't figure out what the problem when i move the gantry to a different location in the machine it doesn't hit the head the beam is out in space 


---
**Jim Hatch** *September 28, 2018 19:46*

Wow, long dead thread 🙂 I'm gonna be of little help on this now - last weekend I finally tossed my K40 in the trash. Between a 60W Redsail and a 45W Glowforge I hadn't used the K40 in a couple of years so I finally just tossed it to make room for a new CNC plasma cutter. I did find a new (well probably a year or so old) 40W tube I had bought as a potential replacement for the K40 if anyone is in the market for one.


---
**Dennis Fuente** *September 28, 2018 20:09*

yeah like i said i shelved it ,how much for the tube what brand is it if i could just get the machine to perform reasonably well that would be a start


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/Ts3Na8MH257) &mdash; content and formatting may not be reliable*
