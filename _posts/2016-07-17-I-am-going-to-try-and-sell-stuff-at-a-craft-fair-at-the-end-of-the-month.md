---
layout: post
title: "I am going to try and sell stuff at a craft fair at the end of the month"
date: July 17, 2016 16:07
category: "Object produced with laser"
author: "Tony Schelts"
---
I am going to try and sell stuff at a craft fair at the end of the month.  Among other things I have been making coasters to put cups on.  Firstly what are your thoughts.  2 what sort of price would you expect to sell them for and what sort of finish do I need to put on them.  I have to sand them first.  All in 3mm Birch PLy



![images/043f1188084c9de623426dcdf9468c3e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/043f1188084c9de623426dcdf9468c3e.jpeg)
![images/0f650ba88604be39117a7354c39a2518.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f650ba88604be39117a7354c39a2518.jpeg)
![images/9912b203de28665b903b132778e49872.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9912b203de28665b903b132778e49872.jpeg)

**"Tony Schelts"**

---
---
**Jim Hatch** *July 17, 2016 16:26*

Pricing is going to depend on area you're from (NYC environs will be more expensive than Des Moines for instance). Also what type of craft fair you're at - home side business type of casual crafters or more high-end juried crafts and artists.



BTW, some of those designs look fragile. Have you given them the kid's whack them on a table test? For coasters I typically go 1/4" (6mm). Most commercial ones seem to be that thickness as well. 


---
**Tony Schelts** *July 17, 2016 16:32*

6mm wow. new to this sort of stuff. Im actually in the uk.  No its not high end.  What finish would you use.??


---
**Jim Hatch** *July 17, 2016 16:37*

I use a wipe on poly stain & finish. I find it easier than using brush on finishes. I do brush some into the edges first so they're sealed.



For high end ones you can use the cast resin finishes that give you the thick clear coat you see on bar tops. That won't work on your filigreed ones because it will flow off. And it's expensive stuff so you want to use it on things you're charging a premium for.


---
**Tony Schelts** *July 17, 2016 16:41*

**+Jim Hatch** thanks Jim


---
**Tony Schelts** *July 17, 2016 16:45*

Jim I have a small air brush, would that be any good for applying the wipe on poly varnish? Its a water base finish isn't it


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/12sHgCMmx27) &mdash; content and formatting may not be reliable*
