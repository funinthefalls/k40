---
layout: post
title: "Hello all. I am working with K40 for about 2 years"
date: September 03, 2017 18:29
category: "Original software and hardware issues"
author: "Maja Velimirovic"
---
Hello all. I am working with K40 for about 2 years. I recently bought another K40 laser engraver. I have problem when engraving. It looks like the beam is hitting sometimes beside part that I am engraving. You can see that in photo. Little dots near the name engraved. I have checked alignment of all mirrors and lens. Everything seems fine. I am not sure what is the problem. Can anyone help?

![images/cdbab434e58dc1d72d1bacd415e0b91b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdbab434e58dc1d72d1bacd415e0b91b.jpeg)



**"Maja Velimirovic"**

---
---
**Ned Hill** *September 03, 2017 20:01*

Not sure what the material is you are engraving, but could it be splatter?  Seems to be too random for laser scattering due to an alignment issue.


---
**BEN 3D** *September 06, 2017 15:37*

**+Maja Velimirovic** hey are you solved this? 


---
*Imported from [Google+](https://plus.google.com/110563519336458822965/posts/3xYNMdj6xNN) &mdash; content and formatting may not be reliable*
