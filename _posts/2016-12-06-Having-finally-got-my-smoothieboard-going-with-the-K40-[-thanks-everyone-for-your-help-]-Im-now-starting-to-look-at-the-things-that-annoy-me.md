---
layout: post
title: "Having finally got my smoothieboard going with the K40 [ thanks everyone for your help ] I'm now starting to look at the things that annoy me"
date: December 06, 2016 06:16
category: "Modification"
author: "Darren Steele"
---
Having finally got my smoothieboard going with the K40 [ thanks everyone for your help ] I'm now starting to look at the things that annoy me.  The first issue is trailing my ethernet cable across my office to put the K40 onto my LAN.  I pondered taking the ESP8266 route but every time I looked at the instructions it seemed a little daunting.  I've now decided to take one of the old Raspberry Pi's that I have laying around and turn it into a WiFi-Ethernet bridge.  So in theory I can do all the same stuff I normally do without trailing ethernet cables around.  This also ties in neatly with my plans to take a RPi display that I have laying around [ I have a LOT of crap just sitting around ] and turn it into a controller to run jobs off the SD card and any other stuff I can think of.



Anybody done something similar?  I'll post the results of the bridge setup on Friday when I get home again





**"Darren Steele"**

---
---
**John-Paul Hopman** *December 06, 2016 16:13*

Don't recall what it is called, but I originally purchased a small nano router for connecting my 3D printer via wifi. I believe its main purpose is for providing private wireless networks when traveling, but it will also act as a repeater, allowing you to plug the k40s ethernet into the device instead.


---
*Imported from [Google+](https://plus.google.com/+DarrenSteele/posts/afByyqsX1X7) &mdash; content and formatting may not be reliable*
