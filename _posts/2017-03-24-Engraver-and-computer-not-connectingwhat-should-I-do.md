---
layout: post
title: "Engraver and computer not connecting......what should I do?"
date: March 24, 2017 22:29
category: "Discussion"
author: "clayton Phillips"
---
Engraver and computer not connecting......what should I do?





**"clayton Phillips"**

---
---
**Ned Hill** *March 24, 2017 22:42*

If you can we will need some more info on your setup.  e.g. Is this a new machine? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 24, 2017 23:21*

Could be the USB cable. If you have a printer floating around, see if you can borrow its cable for a test (they're usually the same).


---
**clayton Phillips** *March 24, 2017 23:43*

**+Ned Hill** it is a new machine 


---
**Ned Hill** *March 25, 2017 01:07*

What software do you have?


---
**Alex Krause** *March 25, 2017 04:46*

Do you have the Usb dongle plugged into the computer... Looks like a thumb drive... You will also have to set up the software for the type of board you have and input the serial number off the board into the software 


---
**clayton Phillips** *March 25, 2017 17:59*

Is there anyone in the Denver area that knows about this machine


---
**chris82o** *March 26, 2017 05:27*

If you are using the software it comes with you need to put the key from your board into the program also make sure you have the usb key plugged in when using it as well 


---
*Imported from [Google+](https://plus.google.com/114650676764947414234/posts/ZFE6WGmkn3d) &mdash; content and formatting may not be reliable*
