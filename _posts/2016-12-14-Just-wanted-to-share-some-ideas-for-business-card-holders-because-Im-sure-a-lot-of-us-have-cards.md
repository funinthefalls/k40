---
layout: post
title: "Just wanted to share some ideas for business card holders, because I'm sure a lot of us have cards"
date: December 14, 2016 19:10
category: "Object produced with laser"
author: "Madyn3D CNC, LLC"
---
Just wanted to share some ideas for business card holders, because I'm sure a lot of us have cards. 



One thing I like to do with friendly local establishments is conversate a little about what small business owners typically conversate about. I'll bring up the importance of business cards, and if I notice they just have a few random stacks of cards by the register I'll show them my card holders and offer to make them a few custom card holders free of charge,  to help clean things up a bit.



 Of course they are always more than happy to keep one of my card holders out in public view as well, and I'm more than happy to stop by every so often to drop off another stack of cards. This has always worked out great  and I hope it works out for anyone who may be trying to launch a business off the ground. There's a few benefits of doing this aside from getting your name out there locally.  I am happy to share this technique as long as you live nowhere near a 25 mile radius from me.. (I'm kidding, but not really) 



There's tons of awesome card holders out there, but here's the 3D model I use. Then to customize them it's just a 5 minute job on the laser with a very small amount of acrylic. I use M4's to attach the acrylic plate but use whatever works for you. 



Here's the .STL file which I just uploaded to Thingaverse to easily share it..

[https://www.thingiverse.com/thing:1968360](https://www.thingiverse.com/thing:1968360)



Here's my entire collection of 3D printed K40 laser mods.....

[https://www.thingiverse.com/Madyn3D-CNC_LLC/collections/laser-mods](https://www.thingiverse.com/Madyn3D-CNC_LLC/collections/laser-mods)



Note: I'm currently working on getting a lot of my files uploaded to YouMagine and off of Thingaverse. IMO, Thingaverse has already shown their true colors back when they had somewhat of a monopoly on this. Now we have many choices, many of which are NOT entitled to file a patent on anything you upload to the site by terms of agreement. 



![images/0f818d4ca72c5187632f14549d821def.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f818d4ca72c5187632f14549d821def.jpeg)
![images/ef2279cd5c6f1b08a3321d3bfa1687cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef2279cd5c6f1b08a3321d3bfa1687cd.jpeg)
![images/e5dcd7723554a1d6484b5b7b1ed6cc50.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e5dcd7723554a1d6484b5b7b1ed6cc50.jpeg)

**"Madyn3D CNC, LLC"**

---


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/2dxoD1EtKZd) &mdash; content and formatting may not be reliable*
