---
layout: post
title: "Manual Z Axis. Available on Amazon for $17.99"
date: December 15, 2017 19:41
category: "Modification"
author: "David Allen Frantz"
---
Manual Z Axis. Available on Amazon for $17.99. Search for Lab Jack. Super useful. 



![images/bb596bf125b696f46208e84d124f7eed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb596bf125b696f46208e84d124f7eed.jpeg)
![images/3aeceb5b40562ee70714fefed6ce470c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3aeceb5b40562ee70714fefed6ce470c.jpeg)
![images/c19f5ff6ceccbf1fe559a2ce8a285597.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c19f5ff6ceccbf1fe559a2ce8a285597.jpeg)
![images/279e710da2592f993d3ed2382f16857d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/279e710da2592f993d3ed2382f16857d.jpeg)
![images/0f7cfe3f251e893b968ebb4c48ba4d13.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f7cfe3f251e893b968ebb4c48ba4d13.jpeg)
![images/091e7b8a334569c551b2fdc0e79875ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/091e7b8a334569c551b2fdc0e79875ae.jpeg)

**"David Allen Frantz"**

---
---
**David Allen Frantz** *December 15, 2017 19:43*

Etched on 3/4” hardwood. ![images/d763efb4e3d199bf393dca7704714441.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d763efb4e3d199bf393dca7704714441.jpeg)


---
**James Lilly** *December 16, 2017 03:31*

Bought the same one myself, haven't used it yet though.  Contemplating how to arrange the cutting bed, among other things.




---
**David Allen Frantz** *December 16, 2017 19:02*

It's not without its flaws. I made this acrylic reference grid that indexes with the gantry to line things up if necessary. 

![images/62f0daf31acd4dd9a30c0f4fb881229e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/62f0daf31acd4dd9a30c0f4fb881229e.jpeg)


---
**James Lilly** *December 16, 2017 19:25*

Most of my cutting and engraving are with 1/8" mdf or birch plywood.  Once in a while I"ll have something thicker and need to adjust of course...rather than stacking blocks or something.


---
**David Allen Frantz** *December 16, 2017 19:37*

Right on


---
**James Lilly** *December 19, 2017 22:35*

Still just laying things out and figuring out how I ant things to look.

[https://plus.google.com/photos/...](https://profiles.google.com/photos/114642009939368433927/albums/6501390556900876977/6501390560514532786)


---
**Mike Albers** *December 20, 2017 14:47*

**+James Lilly** I like the idea of putting the square in there.  I wonder if using the lab riser ( saw it on you tube) fixed on the bottom, weld grilled sheet metal to it would make a good platform.  My thinking is later on I could replace knob with a gear and nema 17 motor and make it digital.


---
**Abe Tusk** *December 20, 2017 15:39*

Wow, thank you for this find!




---
**David Allen Frantz** *December 20, 2017 18:42*

I had that same thought Mike


---
**Bob Damato** *December 24, 2017 21:27*

This is what Ive been using for maybe a year or so now. its perfect. I started with a larger one but found it didnt go down as far as I wanted, plus it was tough to reach the adjustor knob while in the laser. So I got a smaller one and its perfect.




---
**James Lilly** *December 24, 2017 21:44*

Yes, some of the larger ones are 3" closed which is practically where the focus height is.


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/hWQ4NwBYCiL) &mdash; content and formatting may not be reliable*
