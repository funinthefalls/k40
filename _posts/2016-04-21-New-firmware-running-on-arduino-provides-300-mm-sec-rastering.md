---
layout: post
title: "New firmware running on arduino provides 300 mm/sec rastering"
date: April 21, 2016 19:07
category: "Software"
author: "Nick Williams"
---
New firmware running on arduino provides 300 mm/sec rastering.

I have been working on modifications to the grbl firmware to enable higher details with the diode lasers that I sell.

Below is an image of the detail I was able to get with the astec calendar.

This was done only using a 3 watt laser:-)

This is a new break through, I have only had this working for about 2 weeks. I have included a video of this running on a Shapeoko.



![images/65f8a88af07e9d59e4c938fece6e1d19.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65f8a88af07e9d59e4c938fece6e1d19.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-T72yCeYxJ_0/Vxkkx3y9JpI/AAAAAAAACcU/jwbz2lcZzOoaKph70bGhBATEvB_XAo9Fg/s0/2016-03-30%252B07.40.52.mp4.gif**
![images/b80b84f31dc85a2ea31e2a89da86dd73.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/b80b84f31dc85a2ea31e2a89da86dd73.gif)

**"Nick Williams"**

---
---
**Nick Williams** *April 21, 2016 19:09*

Forgot to mention, Although I did not show an images or video of the K40 I have tested on the K40 this is where I got the 300/mm sec. Unfortunately I forgot my phone so I  was not able to show it.


---
**Frank Wiebenga** *April 22, 2016 05:51*

Dude your mysql server was hacked months ago. You don't let your backers know what's up. I can't even use your software since the last update. Fix the null references! I'm glad to see improvements though. 


---
**Manuel Conti** *April 23, 2016 06:55*

Hi, what software are you using (into the video)? Tnks


---
**Frank Wiebenga** *April 23, 2016 13:03*

**+Manuel Conti**​ He is using object works. [https://www.kickstarter.com/projects/1923304356/laser-ink](https://www.kickstarter.com/projects/1923304356/laser-ink)


---
**Nick Williams** *April 27, 2016 19:30*

Sorry about the website there is some deprecated PHP code I was able to reboot my website and it would work for weeks. Now the problem seems to happen again within hours. working on this. Frank, I have posted to my forum with regards to the update under general. 


---
*Imported from [Google+](https://plus.google.com/107520953345127905250/posts/6CimCXby6tF) &mdash; content and formatting may not be reliable*
