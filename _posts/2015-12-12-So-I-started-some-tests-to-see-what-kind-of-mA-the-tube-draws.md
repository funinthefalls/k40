---
layout: post
title: "So I started some tests to see what kind of mA the tube draws"
date: December 12, 2015 22:32
category: "Discussion"
author: "ChiRag Chaudhari"
---
So I started some tests to see what kind of mA the tube draws. LCD menu only has 20% and 50% settings to test fire. But its pretty easy to modify ultralcd.cpp to define different power settings. 



The surprising part is at 40% power it only draws 3.5mA and its jumps to 10mA at 45%. At 50% its 13mA. So I need to find sweet spot between 40 and 45 now. 



As **+Scott Thorne**​​ suggested I will try to not go over 7mA. May be then I can modify the code to never exceed that by changing the range of PWM.

![images/dcc2a0bc1c7e331fc0a4ba86d1b36363.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dcc2a0bc1c7e331fc0a4ba86d1b36363.jpeg)



**"ChiRag Chaudhari"**

---
---
**ChiRag Chaudhari** *December 12, 2015 22:59*

Ok so just got done testing 40 to 44% power and results are super strange.

1. @ 42% power currents jumps to ~8mA for a second and then comes back down to ~4mA.

2.  @ 43% power current jumps to ~9mA for about a second and then drops down to ~4mA.

3. @ 44% power current stays at ~10mA.



When the current drops I can see the laser beams inside the tube also kind of powers down. I mean you can literally see the brightness goes down. 



So what is that ? Is that Lower quality tube for something to do with power supply? I see on LightObject website they states that "Unlike other low quality tubes you may find on eBay, this tube is made by a reputable company. The output is very stable with a low divergence."



Any idea folks ?


---
**Scott Thorne** *December 13, 2015 01:44*

If at 44% you get a stable 10ma output..then it's neither the tube or the power supply, sounds like the controller to me...I guess it could be the supply but unlikely.


---
**Stephane Buisson** *December 13, 2015 11:13*

do you check with a oscilloscope ?

maybe I am wrong, but my understanding is the signal is square in V (cheap PSU). that mean  X% is the percentage of TIME the signal is at v Volt (define by pot).

PWM is nothing to do with Amp (except maybe the time needed for chemical reaction to happen in the tube and before saturation).



This why I kept the pot in my mod (to control max mA). but I don't have a oscillo, and the VU meter is useless at those frequency.



edit: Sorry I was a bit slow this morning, your % is showing a digital pot log curve (with 5v max =100%) and corresponding power.



it seem this digital version isn't better calibrated than the analog one ;-((


---
**ChiRag Chaudhari** *December 15, 2015 22:50*

**+Stephane Buisson** Thanks for your help. The thing is If I am running some gcode at the same power level but with PPM=40, the mA on the Analogue meter stays stable. But it drops like the video I posted only when I keep the Laser Fire switch pressed. So that made me think that Instead of continuous laser if I keep it pulsing the power stays stable.


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/gSujQ2M7MtJ) &mdash; content and formatting may not be reliable*
