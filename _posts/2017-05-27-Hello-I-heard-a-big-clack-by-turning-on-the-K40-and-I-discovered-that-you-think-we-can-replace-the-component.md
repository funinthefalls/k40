---
layout: post
title: "Hello I heard a big clack by turning on the K40, and I discovered that, you think we can replace the component?"
date: May 27, 2017 16:41
category: "Original software and hardware issues"
author: "Jean-Phi Clerc"
---


Hello

I heard a big clack by turning on the K40, and I discovered that, you think we can replace the component? Or you have to change all the food?

Thank you all

Jean Philippe



![images/25e21413b849c34232efca3e7b3a1019.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/25e21413b849c34232efca3e7b3a1019.jpeg)
![images/fc64c933efd7ea0ab3d76081e8f2d9f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fc64c933efd7ea0ab3d76081e8f2d9f1.jpeg)

**"Jean-Phi Clerc"**

---
---
**Gee Willikers** *May 27, 2017 16:51*

That was a fuse. If it blew something inside but be terribly damaged.


---
**Don Kleinschnitz Jr.** *May 27, 2017 18:42*

Main fuse blown. It's a common problem when supply dies. How old is it?


---
**Jean-Phi Clerc** *May 27, 2017 19:11*

a few hours only ... K40 not used since many weeks and boum


---
**Don Kleinschnitz Jr.** *May 28, 2017 00:05*

**+Jean-Phi Clerc** 

Other than try and get replacement from your vendor...

..you have a couple of routes depending on how electronics savvy you are.



If you are not good at testing electronic parts.

A. replace the supply $60-70



If you are good at Repair:

B. Replace the Flyback as a stab in the dark $30



C. Look for damage or shorts then replace the bad parts. These components:

..Rectifier bridge (common)

..Power transistors

..Power Diodes

...$20-30 depends on how many bad parts.



D. The problem could be B-C so the supply could end up being more expensive than a new one.



WARNING: This unit outputs lethal voltages so I do not recommend repairs unless you are skilled at HV electronics.

.......

Are any pars other than the fuse charred, if so post pictures pls.



Where are you located?




---
**Phillip Conroy** *May 28, 2017 03:37*

Was the laser firing when fuse [blew.if](http://blew.if) no than could be limited to  dc side of power supply ,that what powers the stepper moters and control board


---
**Phillip Conroy** *May 28, 2017 03:40*

Change fuse to same size new one and remove middle and right green connector  to eliminate external to power supply problems, if fuse don't blow problem elsewhere


---
**Fernando Bueno** *May 28, 2017 09:34*

**+Jean-Phi Clerc** That is a very common problem in this type of machines. I had the same problem some time ago and they helped me solve it. I leave you the link to the conversation where the answers are: [plus.google.com - Power Supply Failure? Recently I've noticed some inconsistency in performance...](https://plus.google.com/105896334581840652176/posts/LqVp55MbtpE)



In my case, what caused the fuse to burn was the general switch, which turned on the machine, jumped a spark and caused a short circuit in the power supply. I replaced the main switch and the emergency switch with good quality ones. Then I isolated the power supply, changed all the wiring connectors and finally, I soldered the burned fuse and the bridge rectifier, since it had also been burned.



With all this the problem was solved and since then the machine worked perfectly until one day it stopped working again. This time it was the high voltage transformer. I changed it for a new one and for the moment the photo works perfectly again.


---
**Don Kleinschnitz Jr.** *May 28, 2017 12:03*

**+Fernando Bueno** I watch the LPS failures pretty carefully. My guess is that your switch was a separate failure, but of course still needed replacement :).

Most failures seem to be related to the HV flyback transformer. Sometimes it is accompanied by a bridge rectifier failure and sometimes not. Sometimes the bridge rectifier fails and the flyback does not. Sometimes just the flyback.



**+Jean-Phi Clerc** if it was me I would replace the fuse and the bridge rectifier if nothing else is charred. 

See: [donsthings.blogspot.com - K40 LPS repair and test](http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html)



Beyond that repair, a new supply may be the long term cheaper route.


---
**Fernando Bueno** *May 28, 2017 18:07*

**+Don Kleinschnitz** The change of the main switch and the emergency button may not be necessary (especially the latter), but for the price of new, I think it was worth it, especially if I avoid any future problem. In any case, I think the important thing is to use quality components when replacing them.


---
**Jean-Phi Clerc** *May 28, 2017 20:17*

**+Don Kleinschnitz**  , i leave in France,  

Normally, I will have someone who can verify the components one by one, we will see what is dead. A big thank you to all for your opinions


---
**Don Kleinschnitz Jr.** *May 29, 2017 11:45*

**+Jean-Phi Clerc** Just a note on testing components. The Flyback primary can be tested with a DVM (low ohms) however the secondary will likely read OPEN since it has a HV diode in series. If all else is good then it is likely the flyback!


---
**bbowley2** *May 31, 2017 17:03*

After my LMS blew the fuse I discovered that the second ground wire had disconnected.  Bought an $80 PS from Ebay because the vendor didn't respond.  The K40 has worked fine.  Don Kleinschnitz, I haven't rec your address so I can send the bad LPS to you.


---
**Don Kleinschnitz Jr.** *May 31, 2017 18:24*

**+bbowley2** whoops I sent it again via PM on G+.


---
**Jean-Phi Clerc** *June 19, 2017 12:12*

Hi all, i come back, was away since few days, and my problem with the K40 wait after me ;-) i found this fuser : [conrad.fr - Mini-fusible ESKA 887124 temporisé -T- sortie radiale rond 5 A 250 V 1 pc(s) - Vente Mini-fusible ESKA 887124 temporisé -T- sortie radiale rond 5 A 250 V 1 pc(s) sur conrad.fr &#x7c; 537935](http://www.conrad.fr/ce/fr/product/537935/Mini-fusible-ESKA-887124-temporis-T-sortie-radiale-rond-5-A-250-V-1-pcs?ref=list) (french) do you think it is the good one






---
**Don Kleinschnitz Jr.** *June 19, 2017 13:47*

**+Jean-Phi Clerc** 

#K40lpsrepair



<b>*First the warning:There are LETHAL VOLTAGES  in the  K40 HV power supply I do not recommend opening the cover and attempting repair.*</b>



....FUSE

That fuse should work but I have never used that one.

Below is a post with information on repair including an axial fuse link on amazon, which might be an easier install. 

It also has updated schematics.

[donsthings.blogspot.com - K40 LPS repair and test](http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html)



You can also install a 5A car blade fuse and holder which has the advantage of being easier to get and is replaceable without an iron. You can usually get the holder and blade at car parts stored.



I install a reset-able one during a supply repair because I have found that I blow them a lot during troubleshooting and soldering is annoying. 

[https://www.amazon.com/gp/product/B000GGOO14/ref=crt_ewc_title_srh_1?ie=UTF8&psc=1&smid=ATVPDKIKX0DER](https://www.amazon.com/gp/product/B000GGOO14/ref=crt_ewc_title_srh_1?ie=UTF8&psc=1&smid=ATVPDKIKX0DER)



[https://goo.gl/photos/MfQJ1mCjZs6686qa9](https://goo.gl/photos/MfQJ1mCjZs6686qa9)



... Replacing bad components

I have been working on +bbowley's bad supply as a part of learning how these supplies operate an fail. 



The AC bridge is a common failure which blows the fuse. Multiple things can cause the bridge to draw to much current so its likely that you will have to replace more than one part and you may blow another fuse and bridge in the process. There are links to parts in the post below. 

I have not had time to create a proper repair procedure but if you do decide to try a repair here are some hints:



*DO THIS WITH THE POWER OFF AND THE HVT UNPLUGGED OR UNBOLTED. DO NOT ATTEMPT VOLTAGE MEASUREMENTS WITH POWER ON. THE INTERNAL VOLTAGES CAN EXCEED 600 VDC AND METER PROBES CAN ARC, DAMAGING THE SUPPLY AND YOU! *



A. Test the bridge for any shorts on its 4 terminals.

B. Test the 4 diodes on the HV side for shorts

C. Un-solder pin 1 and pin 2 on IC2, test pin 2 to pin 1 for a short. 

Replace all parts that have shorts before re-aplying power.



 when testing for shorts test with the meter on ohms. Measure one direction and then swap the leads and measure again.



A: Rectifier Bridge: [https://goo.gl/photos/gnpre7EkwnP8Frfh9](https://goo.gl/photos/gnpre7EkwnP8Frfh9)

B: HV side diodes: [https://goo.gl/photos/cFogaNvE87s9CdEr9](https://goo.gl/photos/cFogaNvE87s9CdEr9)

C: IC2: [https://goo.gl/photos/e1GP8QBdKvamMQJA9](https://goo.gl/photos/e1GP8QBdKvamMQJA9)








---
**Jean-Phi Clerc** *June 30, 2017 05:44*

**+Don Kleinschnitz**  I'm still alive ;-) tried to repair but no way, not strong enough in changing components, can you give me a direct link where to buy the correct supply? thanks again


---
**Don Kleinschnitz Jr.** *June 30, 2017 11:57*

**+Jean-Phi Clerc** this link is further up in the post and has part # and supplier links? 

[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com) - K40 LPS repair and test



What have you tried to repair, how and with what ?


---
**Jean-Phi Clerc** *July 02, 2017 06:18*

**+Don Kleinschnitz** Hi, i'm trying to find exact model for supply for spare, my problem is i have no knowledge in electronics components... have a fuse with me, will test it as soon as possible, my supply is like the picture here if you kno

w where to buy it [photos.google.com - K40](https://goo.gl/photos/2LWVpzWtQRMAJHKG6) 


---
**Don Kleinschnitz Jr.** *July 02, 2017 12:19*

**+Jean-Phi Clerc** any of the 40W supply's you find on line with the same set and orientation of green connectors should work



Examples and where you can get them from:

Aliexpress:

[aliexpress.com - 35-50W CO2 Laser Power Supply for CO2 Laser Engraving Cutting Machine MYJG-40T](https://www.aliexpress.com/item/30-40W-CO2-Laser-Power-Supply-for-CO2-Laser-Engraving-Cutting-Machine-MYJG-40T/32718812352.html?spm=2114.01010208.3.9.1dQ7KO&ws_ab_test=searchweb0_0,searchweb201602_2_10152_10065_10151_10068_10130_10084_10083_10080_10082_10081_10110_10178_10136_10137_519_10111_10060_10112_10113_10155_10114_437_10154_10056_10055_10054_10182_10059_303_100031_10099_10078_10079_10103_10073_10102_10052_10053_10142_10107_142_10050_10051-10102,searchweb201603_51,ppcSwitch_4&btsid=db6d0256-5a03-4775-8ea5-f035d580bc43&algo_expid=dd67bec9-5014-4040-bf3f-44d876611576-1&algo_pvid=dd67bec9-5014-4040-bf3f-44d876611576)

Amazon:

[https://www.amazon.com/Power-Supply-Engraver-Cutter-Connector/dp/B01MT2UO8D/ref=sr_1_3?ie=UTF8&qid=1498997424&sr=8-]3&keywords=40w+laser+power+supplies](https://www.amazon.com/Power-Supply-Engraver-Cutter-Connector/dp/B01MT2UO8D/ref=sr_1_3?ie=UTF8&qid=1498997424&sr=8-%5D3&keywords=40w+laser+power+supplies)






---
**Jean-Phi Clerc** *July 05, 2017 09:14*

thank you very much **+Don Kleinschnitz** , price os ok for me, i have found the correct model with your help, and hope this will arrive soon (for information, i tried before with fuser change but no way)... 


---
**Don Kleinschnitz Jr.** *July 05, 2017 16:31*

**+Jean-Phi Clerc** changing the fuse almost never works. If yours has failed as most do, you will need to change the bridge rectifier and potentially one other part.

If you want to attempt a repair for a spare let me know. I am assuming you are not in the US?


---
**Jean-Phi Clerc** *July 05, 2017 20:14*

**+Don Kleinschnitz** i live in France (Commenailles, little Town in Jura department where i created a rural Fablab) but I am interested for a spare yes 


---
**Don Kleinschnitz Jr.** *July 06, 2017 01:36*

**+Jean-Phi Clerc** ok got a lot on my plate. I will try to get to it soon but remind me if I do not :)!


---
**Jean-Phi Clerc** *July 26, 2017 05:46*

Hi all, Hi **+Don Kleinschnitz**, have just received my new power supply, Do you have any advice or point of attention for setting up? I would not blow up this one !! :-) 

![images/0f914146c057614d5175e012ca04abe5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f914146c057614d5175e012ca04abe5.jpeg)


---
**Don Kleinschnitz Jr.** *July 26, 2017 13:16*

**+Jean-Phi Clerc** there really is nothing to setup in these supplies.





UNPLUG ALL POWER FROM THE WALL.



 1. I would remove the thin blue film on its case. It serves no purpose that I can find and I think it reduces its thermal properties.

2. Make sure that the FG is connected electrically to the frame and that it is bolted down solid.

3. I dont recommend soldering the anode connection (because you can damage the tube) but if you do use "ball soldering" technique. 

4. Use the proper silicone when re-poting the anode connection. A post on replacement is attached. The white silicon that came with your machine is the best but alternatives cited in the post are good also.

4. Route the anode wire as far away from metal and ground as you can. Don't tie wrap it to anything.

5. Connect the cathode wire back to the -L of the supply and nowhere else. -L is on the leftmost connection on the supply. Do NOT confuse with L which is on the rightmost connection. Should just be a matter of reconnecting the wire you took off when removed the supply.



I assume you still want to repair your old supply? Its still on my to-do list to post on that procedure :)!





[donsthings.blogspot.com - K40 Laser Tube Specifications, Maintenance, Failure & Replacement](http://donsthings.blogspot.com/2017/05/k40-laser-tube-specifications.html)


---
**Jean-Phi Clerc** *July 27, 2017 10:12*

Thanks **+Don Kleinschnitz**​ :-). Answer is Yes for repair ;-)


---
**Don Kleinschnitz Jr.** *July 27, 2017 19:44*

**+Jean-Phi Clerc** pls review this post and let me know what you think. I assume you will want to replace BR1 and the fuse.



[donsthings.blogspot.com - Repairing the K40 LPS #1](http://donsthings.blogspot.com/2017/07/repairing-k40-lps-1.html)




---
**Jean-Phi Clerc** *July 28, 2017 06:14*

**+Don Kleinschnitz**, excellent, you give information step by step for best understanding and diag, i have "evernoted" it for reading later, my first goal is to change the supply this WE. thanks for the work!


---
**Jean-Phi Clerc** *August 16, 2017 19:47*

Hello **+Don Kleinschnitz**, my K40 came back to life with the change of the electric power supply, no particular problem, I reflechie to order a feed ahead now, or repair the existing / diagnose with all your documentation, thank you for all your advice!


---
**Don Kleinschnitz Jr.** *August 16, 2017 21:34*

**+Jean-Phi Clerc** let me know how the repair goes...


---
**Jean-Phi Clerc** *August 18, 2017 04:39*

**+Don Kleinschnitz**​ I read again your fantastic resource information site about K40, i found a bad URL, in picture attached 

![images/e6d2889c845105684ffbda4586587fbd.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e6d2889c845105684ffbda4586587fbd.png)


---
**Don Kleinschnitz Jr.** *August 18, 2017 12:50*

**+Jean-Phi Clerc** can you provide me a url for the above as I cannot find this snippet :).

BTW you can leave comments at the bottom of blog posts :).


---
**Jean-Phi Clerc** *August 18, 2017 15:17*

ok, so i give information in the blog directly :-) 


---
*Imported from [Google+](https://plus.google.com/+JeanPhiClerc/posts/BRbD6hVk5cx) &mdash; content and formatting may not be reliable*
