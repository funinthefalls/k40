---
layout: post
title: "Okay, so I was cutting my first finger joints tonight"
date: October 02, 2016 02:21
category: "Object produced with laser"
author: "J.R. Sharp"
---
Okay, so I was cutting my first finger joints tonight. For some reason the laser went back, cut them off and then also cut them corner to corner. What the hell?





**"J.R. Sharp"**

---
---
**J.R. Sharp** *October 02, 2016 03:26*

![images/133bfe1350dbed50bd639f0a287a44d7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/133bfe1350dbed50bd639f0a287a44d7.png)


---
**J.R. Sharp** *October 02, 2016 03:28*

![images/14db051289552727f9ae6066b15e8ea5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14db051289552727f9ae6066b15e8ea5.jpeg)


---
**J.R. Sharp** *October 02, 2016 03:29*

![images/bf33c090a95e6348bc19cf7ec7a0dbec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf33c090a95e6348bc19cf7ec7a0dbec.jpeg)


---
**Ariel Yahni (UniKpty)** *October 02, 2016 03:44*

So the software for some reason is trying to close that path.  So you need to make it 1 piece


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 02, 2016 04:01*

Yeah, paths aren't closed will be likely the issue. Alternatively look for any invisible lines (i.e. no stroke, no fill) in your file (I had that happen before & it cut a solid line where it should've been dashed lines).


---
**J.R. Sharp** *October 02, 2016 04:32*

Well hell. How do I go about closing paths? They are node to node.




---
**J.R. Sharp** *October 02, 2016 05:02*

Nevermind, I found it and you all were right! Thanks!!




---
*Imported from [Google+](https://plus.google.com/116586822526943304995/posts/C8PHszbX2nK) &mdash; content and formatting may not be reliable*
