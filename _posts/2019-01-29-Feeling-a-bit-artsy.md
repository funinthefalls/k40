---
layout: post
title: "Feeling a bit artsy?"
date: January 29, 2019 23:09
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Feeling a bit artsy?

[https://msurguy.github.io/SquiggleCam/](https://msurguy.github.io/SquiggleCam/)

![images/6063edb3425ca8a71a256fffda7a1e09.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6063edb3425ca8a71a256fffda7a1e09.jpeg)



**"HalfNormal"**

---
---
**Travis Sawyer** *January 30, 2019 02:00*

Neat!


---
**James Rivera** *January 30, 2019 06:43*

Trippy.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/HbW4GZwsp7r) &mdash; content and formatting may not be reliable*
