---
layout: post
title: "For all my USA compatriots, Happy 4th of July!"
date: July 04, 2016 04:00
category: "Object produced with laser"
author: "Ned Hill"
---
For all my USA compatriots, Happy 4th of July!

![images/f02dd6de7f2015ccbcc4b15f77cc5044.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f02dd6de7f2015ccbcc4b15f77cc5044.jpeg)



**"Ned Hill"**

---
---
**Julia Longtin** *July 04, 2016 04:23*

bah humbug! ;)


---
**Anthony Bolgar** *July 04, 2016 05:20*

Nicely done.


---
**Travel Rocket** *July 04, 2016 09:38*

awesome,  what is the speed and power used on this piece?


---
**Ned Hill** *July 04, 2016 12:57*

Birch ply 20% power at 250 mm/s


---
**Travel Rocket** *July 04, 2016 13:01*

How long did you made this one? 


---
**Travel Rocket** *July 04, 2016 13:02*

How to get the perfect vector setting for text, after typing just convert to outline and thats it? Sorry im new to this.


---
**Ned Hill** *July 04, 2016 13:06*

Doing text by raster engraving so it just engraves as it's shown on the screen.


---
**Travel Rocket** *July 04, 2016 13:49*

Raster meaning you convert them to curves on corel? No fill or just outline?


---
**Ned Hill** *July 04, 2016 13:56*

There are 2 ways to engrave, raster and vector.  With raster engraving it's just like a ink jet printer.  The laser head moves back and forth and lays down the engraving one line at a time like an image.  With the K40 corel plugin you can do cutting and engraving.  The cutting function is vector while the engraving function is raster.  So if you are engraving text with the engrave function it just engraves everything that is dark.


---
**Ned Hill** *July 04, 2016 14:01*

You can engrave text with the cutting function, as a vector, but I haven't tried that yet.  I've engraved some box outlines as vectors though.  Have to keep in mind that with vector engraving that if your line is thicker than say 0.001mm it will engrave both sides of the line.


---
**Ned Hill** *July 04, 2016 18:41*

**+R Ninte** Yes


---
**Travel Rocket** *July 05, 2016 01:56*

Thanks!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/joEjLa7ZdNh) &mdash; content and formatting may not be reliable*
