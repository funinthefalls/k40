---
layout: post
title: "Hey guys, I received my k40 yesterday and am going to try it out today"
date: October 20, 2017 17:13
category: "Hardware and Laser settings"
author: "Marko Marksome"
---
Hey guys,

I received my k40 yesterday and am going to try it out today.

I have one big question tho. On the backside there are 2 white outlets of some sort. I really dont know what this is used for. It seems that those are some UK outlets that you can normaly find in the walls. Is this for the vent and the water pump?

They are connected with some wires.

Could you help?





**"Marko Marksome"**

---
---
**joe mckee** *October 20, 2017 17:24*

check the wiring to them and if they are ok connect water pump and air assist and they are live when you switch the machine on .they are normally cut to accept UK and Europe 


---
**Marko Marksome** *October 20, 2017 18:22*

Ok that sound cool. The wiring leads to different locations. One leads to the power supply and the other one i cant locate. How to know where the pump and where the vent goes?


---
**Ned Hill** *October 20, 2017 19:02*

**+Marko Marksome** those plugs are typically for auxiliary power for things like the water pump and exhaust fan.  You can check their voltage with a meter and they should match your wall outlet voltage.  Doesn't matter what get's plugged into which outlet.  


---
**Marko Marksome** *October 20, 2017 20:09*

Oh ok thanks for the info. Its handy to have those directly included in the laser. I have a water pump, the standard one that everybody gets. Should i submerge it under water? The pump has a cable on it where the connection doesnt seem to be waterproof. I am a little sceptical about submerging a pump with a cable that is made in china and switching it on. Is this thing suposed to work like that?


---
**Adrian Godwin** *October 20, 2017 22:27*

They usually are submerged, yes. The cable eventually disappears inside a resin lump, though that might not be visible from the outside.



However, I still wouldn't put my hand in the water carelessly, especially if you use tap waterv (not recommended). It seems some current from the laser power supply leaks there.



You're 'lucky' to get UK outlets. Here in the UK they usually seem to ship with US or German outlets !






---
**Adrian Godwin** *October 20, 2017 22:33*

Mine are both connected the same, but if they do turn out to be different (eg one powered all the time and one only when the laser is on) then you want the water pump on all the time and the extractor only when cutting.




---
**Marko Marksome** *October 20, 2017 22:43*

Actually im not that lucky. I live in Austria and we use outlets like they use in Germany. But since the pump and the fan go into the laser itself, i just have to replace one cable for the main power. Thankfully i have enough of those. I will power it up tomorow. Thanks to everybody for the clearance!


---
**Nik Green** *October 22, 2017 10:01*

Marko, where are you in Austria? I'm in Salzburg, non German speaker. But I would be interested in how you are doing with your machine.


---
**Marko Marksome** *October 22, 2017 10:18*

Hey i live in Vienna. The machine is fine. I have tested it allready. I am about to remove the exhaust duct. Turns out to be more complicated as i previously tought.

This machine needs a lot of love, but it has potential...


---
*Imported from [Google+](https://plus.google.com/110288443111992997281/posts/FvE7RLZTBDB) &mdash; content and formatting may not be reliable*
