---
layout: post
title: "Best way to engrave this? I practice on sheet plexiglass and make melted designs in it but not trophy worthy"
date: August 08, 2016 16:45
category: "Object produced with laser"
author: "Bob Damato"
---
Best way to engrave this? I practice on sheet plexiglass and make melted designs in it but not trophy  worthy.  I tried several power levels too.   Id hate to get one of these in and ruin it.



[http://www.ebay.com/itm/222200158685?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/222200158685?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT)





**"Bob Damato"**

---
---
**Jim Hatch** *August 08, 2016 17:08*

It's doable but I'd suggest won't be "trophy worthy" on glass. Lasers can do glass engraving (use either dish soap or wet newsprint/paper towels on the glass when engraving) but really good trophy style stuff might be better done with an chemical paste etch or sand blasting.


---
**Bob Damato** *August 08, 2016 18:08*

I figured as much. Possibly lucite trophy would be better?


---
**Jim Hatch** *August 08, 2016 19:06*

If you don't want to go the other routes for etching the glass, then yep :-) BTW, remember you can flame treat the edges of acrylics with a propane torch to get that smooth glass finish. Sometimes the laser doesn't make the cleanest edge (usually does but nice to have a touch-up sometimes).


---
**Bob Damato** *August 08, 2016 19:08*

I do the acid etch now, but it doesnt give the nice depth. Thank you for the tips :)


---
**Jim Hatch** *August 08, 2016 19:10*

Yeah. You need a really big laser or a fiber one to do glass at depth. You can try on plate glass (you can get it at Home Depot) using the soap or wet paper technique to see if that gives you what you want. But I think you'll end up with plastic :-)


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/QgMEKmhoc8a) &mdash; content and formatting may not be reliable*
