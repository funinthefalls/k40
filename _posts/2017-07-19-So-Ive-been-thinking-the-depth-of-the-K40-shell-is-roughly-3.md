---
layout: post
title: "So I've been thinking, the depth of the K40 shell is roughly 3\""
date: July 19, 2017 14:39
category: "Discussion"
author: "Christopher Elmhorst"
---
So I've been thinking, the depth of the K40 shell is roughly 3". What if you reinforce the bottom edges with steel (to keep the integrity of the shell so the rails don't shift), and then cut out the bottom and create a deeper bottom (create a special stand if you will) and take a scissor jack to create a Z-Table? What are your thoughts? 





**"Christopher Elmhorst"**

---
---
**Jim Hatch** *July 19, 2017 14:53*

It should work. That's the concept behind FSL's Muse with the extension.


---
**Jorge Robles** *July 19, 2017 15:46*

It's on my to-do ;)


---
**Kyle Kerr** *July 19, 2017 15:58*

I'm pretty sure there are K40 style lasers with adjustable z. If you get really fancy you could add a 4th axis and etch rounded objects. :)


---
**Christopher Elmhorst** *July 19, 2017 18:00*

the rounded objects are really where the money is, but I'd wait and buy one that can do it, I don't trust myself to do that right now hahaha.

**+Kyle Kerr** 


---
**William Kearns** *July 20, 2017 04:19*

Doing that now ![images/b63e83219847cc4143639c98007c747b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b63e83219847cc4143639c98007c747b.jpeg)


---
**William Kearns** *July 20, 2017 04:20*

Also replacing couple of pieces of the LO z table to increase travel height to 11-12 inches 


---
**William Kearns** *July 20, 2017 04:36*

![images/fd3718c72bbb7182e76939711f363df1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fd3718c72bbb7182e76939711f363df1.jpeg)


---
**E Caswell** *July 20, 2017 09:36*

**+Christopher Elmhorst** **+Kyle Kerr** must admit that the rotory axis to me is more valuable than the bed adjusting. 

I can live without bed adjustment but couldn't live without rotory. 

First mod I did and would never go back. 




---
**William Kearns** *July 21, 2017 04:32*

Building extension for bottom of laser![images/169b2310c5c6cb0ec8e4f559bb586f03.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/169b2310c5c6cb0ec8e4f559bb586f03.jpeg)


---
*Imported from [Google+](https://plus.google.com/+ChristopherElmhorst/posts/PXdXaaup2cr) &mdash; content and formatting may not be reliable*
