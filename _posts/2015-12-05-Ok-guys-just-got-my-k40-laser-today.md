---
layout: post
title: "Ok guys just got my k40 laser today"
date: December 05, 2015 18:09
category: "Discussion"
author: "Lawrence Simm"
---
Ok guys just got my k40 laser today.    Everything is hooked up and when I try to start engraving the laser is arcing out at the back of the tube away from the mirror.     What am I doing wrong here.       Thanks 





**"Lawrence Simm"**

---
---
**Gary McKinnon** *December 05, 2015 18:24*

You have to align the mirrors, they usually arrive unaligned.


---
**Lawrence Simm** *December 05, 2015 18:27*

Gary I think the laser tube is junk.     I have water in the both tubes.    


---
**Gary McKinnon** *December 05, 2015 21:03*

Oh man :( What, you have a definite leak ?


---
**Todd Miller** *December 06, 2015 02:11*

Arcing is the High Voltage jumping to

a ground or your metal chassis.  



On my laser, the HV is fed at the rear of

the tube with a red wire and is covered with silicon tubing or caulk @ the glass.  



If your tube is damaged as you stated, I wouldn't fire it anymore.  



HV is 15,000 - 20,000 volts and can easily jump to the nearest ground/person/water source.


---
**Lawrence Simm** *December 06, 2015 03:02*

Should there be water in both chambers of the glass tube ? I don't see a crack or anything 


---
**Lawrence Simm** *December 06, 2015 03:02*

The tube is not even lighting up like other videos I've seen 


---
**Scott Thorne** *December 06, 2015 04:19*

Yes....don't fire it....that's how I fried my power supply as soon as it arrived...a new one cost 98.00 bucks!


---
**Scott Thorne** *December 06, 2015 04:20*

No there should only be water in one chamber....the middle cooling shell.


---
**Lawrence Simm** *December 06, 2015 04:29*

Thanks guys I bought this from ebay I'm waiting to hear back from the seller. I don't think that seller sells replacement parts there gonna have to replace the whole unit 


---
**Scott Thorne** *December 06, 2015 04:32*

Yeah...I would send it back...I cost me another 400.00....just to get mine working....I should have sent mine back right away but I didn't wanna wait over a month to get a new one so I replaced the tube and power supply myself.....it works fantastic with the upgraded tube.


---
**Lawrence Simm** *December 06, 2015 04:35*

Well it only took a week to get it so hopefully I'm not out to long 


---
**Scott Thorne** *December 06, 2015 04:42*

Maybe not.....you have to wait to get the refund then reorder...then you might get one in the same shape or worse....the first 2 replacement tubes from Amazon arrived broken so I ordered one from laser depot.


---
**Lawrence Simm** *December 06, 2015 05:20*

They just offered to send me a new tube and $50 refund.. Hmm 


---
**Scott Thorne** *December 06, 2015 05:57*

Lol....good luck with that


---
*Imported from [Google+](https://plus.google.com/105668845795384171497/posts/SrC5oELfYXu) &mdash; content and formatting may not be reliable*
