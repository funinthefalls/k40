---
layout: post
title: "Does anyone else have this air assist head from Saite Cutter on EBay?"
date: January 04, 2017 15:12
category: "Discussion"
author: "Bill Keeter"
---
Does anyone else have this air assist head from Saite Cutter on EBay? Does it assemble in the order I have it shown? My issue is the 2 washers. One looks to hold the lens and the other to keep the lens from moving when air assist is used. The problem is that with everything assembled the washers restrict the vertical movement by a good 5mm. 

﻿

![images/99e589e19f7e7ba5b28b9a814855da81.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/99e589e19f7e7ba5b28b9a814855da81.jpeg)



**"Bill Keeter"**

---
---
**K** *January 04, 2017 18:58*

I have that head and mine didn't come with washers at all. You just pop the lens in the cone and screw down the insert and the lens doesn't move. 


---
**Phillip Conroy** *January 04, 2017 19:13*

The washers are for the stock 12 mm focal lens ,not needed if using 18 mm focal lens


---
**Bill Keeter** *January 04, 2017 19:22*

thanks **+Kim Stroman** and **+Phillip Conroy**. I guess I should have taken out the 18mm lens out of the packaging and did a test fit. I thought I was being smart by keeping it in wrapped up until I was ready to mount. Doh.


---
**Bill Keeter** *January 05, 2017 19:41*

**+Kim Stroman** **+Phillip Conroy** Do you guys happen to have a design of the mount that holds this Saite laser head? Figure there has to be one floating around here that people are using to mount this new air assist head.


---
**K** *January 05, 2017 20:32*

**+Bill Keeter** I don't. I've got a custom bed so my files wouldn't work for stock machines. 


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/Y94RvBBAmQ6) &mdash; content and formatting may not be reliable*
