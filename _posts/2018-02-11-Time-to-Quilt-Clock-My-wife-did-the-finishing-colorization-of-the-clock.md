---
layout: post
title: "Time to Quilt Clock My wife did the finishing colorization of the clock"
date: February 11, 2018 00:33
category: "Object produced with laser"
author: "HalfNormal"
---
Time to Quilt Clock



My wife did the finishing colorization of the clock.



![images/60837cf10f0676b2199840c0f1b8516e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60837cf10f0676b2199840c0f1b8516e.jpeg)
![images/80b13793839cdffa9882ab3867466d5a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/80b13793839cdffa9882ab3867466d5a.jpeg)

**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/W5NyD6SWqvR) &mdash; content and formatting may not be reliable*
