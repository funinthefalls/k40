---
layout: post
title: "I'm trying to make some give away journal sized bookmarks for an upcoming show"
date: May 26, 2016 15:43
category: "Materials and settings"
author: "Carl Fisher"
---
I'm trying to make some give away journal sized bookmarks for an upcoming show. I picked up a box of 0.10 cast acrylic sheet (mirrored) in various colors.



I just tested on a piece and found that after the cut the material is a bit too brittle. When I flex the cut to fit over a sheet of paper, one or the other of the legs keeps snapping right at the top.



Is this material just not suited for this and if not what would be a better choice? I know different acrylics have different properties and I need something with a bit of flex without snapping.





![images/26bc12129114d2e97de76403a047663b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/26bc12129114d2e97de76403a047663b.jpeg)
![images/3165f18e01a1b5ee600bba9767866822.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3165f18e01a1b5ee600bba9767866822.jpeg)

**"Carl Fisher"**

---
---
**3D Laser** *May 26, 2016 16:14*

Wood might be better 


---
**Carl Fisher** *May 26, 2016 16:58*

Well rats, I'm now convinced this material won't work for the intended purpose. Just too brittle when you flex it. I redesigned a bit and it still broke at the same spot, just on both sides now since I bridged the bottom.



[http://i1306.photobucket.com/albums/s573/gndprx/IMG_20160526_124135%20Small_zpsasuhmlfj.jpg](http://i1306.photobucket.com/albums/s573/gndprx/IMG_20160526_124135%20Small_zpsasuhmlfj.jpg)




---
**Greg Curtis (pSyONiDe)** *May 27, 2016 12:02*

That's very thin material, and quite long. Perhaps using a blow dryer/heat gun to sort of pre-form the legs apart a little too reduce the stress.


---
**Simon Jackson** *June 01, 2016 07:32*

One list I looked at listed the following plastics materials as laser-cuttable without too toxic fumes...Acrylic, Polypropylene, Polycarbonate, Nylon, LDPE / HDPE, HIPS and Mylar. 


---
**Greg Curtis (pSyONiDe)** *June 01, 2016 09:55*

Nylon may be a good option, much more flexible, but in my experience, tends to not vaporize like acrylic does. During an engrave test, it actually sort of curled as the engrave progressed. The cut produced similar melting-not-cutting effects.



I admit the piece I was testing was thicker 3mm (1/8 inch) but this is my normal wood and thickness.﻿


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/G71ntirExfn) &mdash; content and formatting may not be reliable*
