---
layout: post
title: "Laser eye saftey seems like a no brainier but I've read a ton of conflicting info"
date: November 05, 2015 14:49
category: "Discussion"
author: "Nathaniel Swartz"
---
Laser eye saftey seems like a no brainier but I've read a ton of conflicting info.    I've seen anything from they are pretty much all the same, to spend every sent you have. I've been wearing welding goggles or leaving the room and watching via camera.   What should I be wearing when operating my cutter?





**"Nathaniel Swartz"**

---
---
**Coherent** *November 05, 2015 15:54*

From everything I have read simply closing the lid (if you have a plexi/acrylic window) is sufficient. Looking at the point of cut/engrave is not like looking at a welding or plasma arc. The laser beam is invisible and what you see is actually the fire or burning from the heat the beam produces. Wearing welding goggles isn't necessary. The laser beam must be directed at or reflected toward your eyes to cause harm.  Even a 4% reflection (off shinny materials etc) can cause eye damage depending on your power settings. The acrylic window (and glass) is opaque to the harmful rays and essentially absorb/block them. The difference between glass and acrylic is that a powerful laser beam reflected can actually burn through the acrylic and get to you whereas with glass it will reflect back or absorb. With low power C02 lasers like these a beam reflection off the material surface and burning up through the acrylic window is highly unlikely (except possibly with mirror and another lens setup) and almost impossible even if you tried.  If your window is yellow tinted, it just cosmetic, clear will work just as well, just close the door before firing the laser. Better yet install a micro switch on the door so it can't fire with the door open.  If you want to keep the door open and watch, get some laser safety goggles, which are essentially just acrylic like the window.


---
**Stephane Buisson** *November 05, 2015 17:15*

unlike **+Marc G** I would recommend to wear 10600 goggles, your sight will be unconscientiously be drag to laser fire, like magnet (it's the cheapest security after door switch, you can get it around 30 usd, it's no brainer)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2015 08:25*

**+Marc G** I wonder, you mention that if you are using a glass window then it will essentially absorb/reflect the laser beam. Couldn't it also possibly focus the beam? Or because it is not concave/convex it will disperse the beam randomly everywhere, thus effectively "absorbing" it?


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/CtKBfYyXrBs) &mdash; content and formatting may not be reliable*
