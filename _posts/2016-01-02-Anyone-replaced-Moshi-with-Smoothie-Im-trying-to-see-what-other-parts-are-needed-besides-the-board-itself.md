---
layout: post
title: "Anyone replaced Moshi with Smoothie? I'm trying to see what other parts are needed besides the board itself"
date: January 02, 2016 18:19
category: "Smoothieboard Modification"
author: "Arestotle Thapa"
---
Anyone replaced Moshi with Smoothie? I'm trying to see what other parts are needed besides the board itself.





**"Arestotle Thapa"**

---
---
**Arestotle Thapa** *January 03, 2016 04:58*

Thank you **+Peter van der Walt** . Power supply unit and the Moshiboard version in the link you send is same as mine. However I see that the pot is still used. So do you set the pot to max and then control the laser power with PWM only?


---
**Arestotle Thapa** *January 03, 2016 14:20*

**+Peter van der Walt** . Could you please clarify this - "Mid job you cant change the Sxxx pwm value" - does this mean you can't have cut and engrave in the same job using different layer or color or object? May be I'm not using the right term.  Thanks for your help!


---
**Arestotle Thapa** *January 03, 2016 22:26*

Understood. Thanks **+Peter van der Walt** .  So basically I need to get a smoothie board, 4 limit switches, 1 level shifter, and a LCD screen!


---
**3D Laser** *January 12, 2016 21:13*

This looks very difficult to do


---
*Imported from [Google+](https://plus.google.com/104400841682788049551/posts/HxvfQ8RsqPA) &mdash; content and formatting may not be reliable*
