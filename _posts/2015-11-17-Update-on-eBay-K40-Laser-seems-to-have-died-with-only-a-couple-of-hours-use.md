---
layout: post
title: "Update on eBay K40- Laser seems to have died with only a couple of hours use"
date: November 17, 2015 04:10
category: "Discussion"
author: "Anthony Coafield"
---
Update on eBay K40- Laser seems to have died with only a couple of hours use. I contacted the seller who haveasked I find someone to check it (which they'll pay for) and send they'll send out any spare parts needed. They've also offered to give a partial refund for the inconvenience. 



Not sure how I'll find someone to check it, or that it would be worth the cost. Should I see if they'll just send out a new laser and power supply and save the hassle?





**"Anthony Coafield"**

---
---
**Nathaniel Swartz** *November 18, 2015 12:41*

Is it completely dead (no power lights no movement when you turn on the machine) or is the laser not firing? 


---
**Anthony Coafield** *November 18, 2015 20:31*

Started working for a short time before fading out, then gradually fading out faster and faster. Sometimes it's a fade out, other times it appears to flare a bit and then stop. 


---
**Anthony Coafield** *November 18, 2015 20:31*

Cutting at 12mA I can do about 5cm currently before the laser fades out.


---
**Nathaniel Swartz** *November 20, 2015 12:32*

Try running at 5 ma -9 ma and see if the beam lasts longer


---
**Anthony Coafield** *November 20, 2015 21:11*

It runs longer, but can't cut! I haven't had any problem engraving because I keep it down around 5-6ma. 


---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/PQC47V1NRRa) &mdash; content and formatting may not be reliable*
