---
layout: post
title: "Hi guys, where do you keep your machine ?"
date: May 21, 2017 21:57
category: "Discussion"
author: "Lucian Depold"
---
Hi guys, where do you keep your machine ? I am thinking of putting my laser into my garage, but the humidity is sometimes over 70%. 



Could this cause damage ?



Lucian





**"Lucian Depold"**

---
---
**Ned Hill** *May 21, 2017 22:03*

The high humidity could cause some corrosion issues but would definitely cause issues with water condensing on the tube and possibly the tube mirrors.  Depending where you live you might also have to be concerned with winter temps if the garage is unheated.  If the tube freezes it can crack, but don't add antifreeze as that causes other issues. 


---
**Lucian Depold** *May 21, 2017 22:09*

**+Ned Hill** Thank you. What would be an acceptable humidity ? I plan to move the laser  to the top floor during winter, but i have to renovate the floor first. So the garage solution would be only for 3 - 4 months.


---
**Ned Hill** *May 21, 2017 22:16*

Short term it's probably ok.  You just don't want to get your cooling water too cold to where you start getting too much below the dew point in your environment (Dew point depends on temp, pressure and humidity).  I'm sure some other people might have some suggestions for you.


---
*Imported from [Google+](https://plus.google.com/105895976959355796398/posts/GYgfM9NbV1A) &mdash; content and formatting may not be reliable*
