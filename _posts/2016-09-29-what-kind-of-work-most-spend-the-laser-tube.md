---
layout: post
title: "what kind of work most spend the laser tube?"
date: September 29, 2016 01:47
category: "Materials and settings"
author: "Anderson Firmino"
---
what kind of work most spend the laser tube? cut or engrave? Whats the best material to stencils?





**"Anderson Firmino"**

---
---
**Alex Krause** *September 29, 2016 02:32*

Depends on what you are going to do with the stencil


---
**Alex Krause** *September 29, 2016 02:33*

Manilla folders make pretty good stencils for spray paint art


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 29, 2016 07:02*

Engraving takes a long time, although I mostly use my machine for cutting. However, if I was to calculate out the hours of usage for my tube/machine then I would say cutting & engraving are about equal (e.g. 10x cut job @ 20 mins & 1x engrave job @ 3 hours). I've used 300gsm card for making stencils for airbrushing dyes onto leather, however it was quickly soaked with dye so not really that suitable. Something more appropriate for that usage would be some form of thin but sturdy clear plastic (in my opinion).


---
*Imported from [Google+](https://plus.google.com/+AndersonFirmino/posts/ZJuGr2mmHD9) &mdash; content and formatting may not be reliable*
