---
layout: post
title: "My lens was upside down !!! Lol I finally got my new Molybdenum mirrors installed and aligned, and I noticed my lens was freaking inverted lol"
date: December 15, 2015 03:20
category: "Discussion"
author: "David Cook"
---
My lens was upside down !!!

Lol I finally got my new Molybdenum mirrors installed and aligned, and I noticed my lens was freaking inverted lol.

The convex side was facing down. Lol.  So I reversed it so the concave side was facing the work piece and wow nice sharp pinpoint. Lol I wonder how many units shipped with inverted lenses and the amount of people that don't realize it





**"David Cook"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2015 03:49*

Mine definitely did. It's good that you finally noticed though, since it makes a huge difference in cutting. I'm curious how much difference the higher quality mirrors make.


---
**Tony Schelts** *December 15, 2015 14:56*

where did you get therm from, and how much. 


---
**David Cook** *December 15, 2015 14:58*

**+Tony Schelts** from eBay for around $40. Look in the mods section of this group for my post about them.   I'm getting a dramatic increase in cutting lower when i test fire the laser now. Well the mirrors plus proper lens orientation. 


---
**David Cook** *December 15, 2015 14:58*

**+Tony Schelts** [https://plus.google.com/+DavidCook42/posts/Tkt48fdTcmr](https://plus.google.com/+DavidCook42/posts/Tkt48fdTcmr)


---
**Tony Schelts** *December 15, 2015 16:24*

Im sure mine is the same, because I took my lens out to clean it and put it back as I found it and Im sure the convex was down> but I throught that was how thery were supposed to be.


---
**David Cook** *December 15, 2015 23:17*

I just saw on the web that there seems to be some debates on proper lens orientation. lol.  so with that being said, everyone should test their system both ways and see which they prefer  

mine is setup like this now

[http://image.thefabricator.com/a/the-importance-of-focal-positions-in-laser-cutting-lens-effective-focal-length.jpg](http://image.thefabricator.com/a/the-importance-of-focal-positions-in-laser-cutting-lens-effective-focal-length.jpg)



[http://www.ophiropt.com/user_files/co2/Spherical-aberration.gif](http://www.ophiropt.com/user_files/co2/Spherical-aberration.gif)








---
**Scott Marshall** *December 18, 2015 17:30*

It would seem sharper is better. You can always defocus on the workpiece, but if the lens is backward, you are limited as to exactly how sharp you can go. You just may need that fine line someday soon.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/1hNjcaPV2oD) &mdash; content and formatting may not be reliable*
