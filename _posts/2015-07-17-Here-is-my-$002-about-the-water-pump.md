---
layout: post
title: "Here is my $0.02 about the water pump"
date: July 17, 2015 14:57
category: "Modification"
author: "Lars Mohler"
---
Here is my $0.02 about the water pump. After only a few hours of use the impeller jammed and starved the tube of cooling water. I was most fortunate that the tube did not sustain any damage (at least that I can tell).  It seems imperative that a good pump (even a Harbor Freight pump is better) and a water flow indicator of some sort is required. I think I will set up a cut-out circuit to shut down the machine as well as an audible alarm just to make it more dramatic. Do you think flashing lights and a klaxon would be too... over the top?





**"Lars Mohler"**

---
---
**David Richards (djrm)** *July 17, 2015 15:41*

A warning lamp and or an audible alarm would be useful. More important is the interlock to inhibit the laser when cooling is not present. A useful addition would be if the alarm latched on, this would draw attention to an intermittent flow which could otherwise be missed. Hth David.


---
*Imported from [Google+](https://plus.google.com/108272305441097782007/posts/WMcHCJeXNep) &mdash; content and formatting may not be reliable*
