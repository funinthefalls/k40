---
layout: post
title: "The new engraver is amazing...cuts 3/8 plywood at 50% power and a speed of 10mm/s....beautiful construction, the rotary attachment is dead on.....I've already engraved his and hers glasses....Lol"
date: December 25, 2015 18:21
category: "Discussion"
author: "Scott Thorne"
---
The new engraver is amazing...cuts 3/8 plywood at 50% power and a speed of 10mm/s....beautiful construction, the rotary attachment is dead on.....I've already engraved his and hers glasses....Lol





**"Scott Thorne"**

---
---
**Scott Thorne** *December 25, 2015 18:25*

Correction....that was 1/4 plywood....sorry.


---
**Coherent** *December 25, 2015 19:03*

That"s great. Sounds like a big upgrade from the smaller unit.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/hH33qaJbYR9) &mdash; content and formatting may not be reliable*
