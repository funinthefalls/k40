---
layout: post
title: "She's so pretty"
date: July 16, 2017 20:48
category: "Object produced with laser"
author: "Christopher Elmhorst"
---
She's so pretty 

![images/93ceb197b8fb4f56625b9746367cef1b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/93ceb197b8fb4f56625b9746367cef1b.jpeg)



**"Christopher Elmhorst"**

---
---
**Ned Hill** *July 16, 2017 22:19*

Ahhh...like a newborn baby picture.  Now go get it dirty :)




---
**Christopher Elmhorst** *July 17, 2017 04:39*

I tested it out a bit today :) 




---
*Imported from [Google+](https://plus.google.com/+ChristopherElmhorst/posts/Vn9cm4Ko1kc) &mdash; content and formatting may not be reliable*
