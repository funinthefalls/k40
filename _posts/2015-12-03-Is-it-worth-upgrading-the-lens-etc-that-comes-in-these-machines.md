---
layout: post
title: "Is it worth upgrading the lens etc that comes in these machines"
date: December 03, 2015 13:27
category: "Discussion"
author: "Tony Schelts"
---
Is it worth upgrading the lens etc that comes in these machines.  I think its about 10-12mm.  does size matter????  :0





**"Tony Schelts"**

---
---
**Anthony Bolgar** *December 03, 2015 14:27*

Higher quality lens and mirrors make a considerable difference in performance. Larger lens' and mirriors are better.


---
**Coherent** *December 03, 2015 15:16*

I've read reports of some pretty poor quality  control on these machines and the lower quality lens that come with them. A better quality of optics can only help. FYI the original is 12mm. Unless you plan on getting an air assist that accepts the 18mm lens, replace yours with the same.


---
**Tony Schelts** *December 03, 2015 16:31*

Thanks for your advice.  What are the recommended makes to go for. I live in the UK but I guess they are probably available.


---
**Coherent** *December 03, 2015 19:16*

A number of members in the US have bought from Light Object since they are in the US. You most likely can find the exact same lenses and mirrors on Ebay possibly cheaper (most are China imports). Unfortunately quality may differ greatly vendor to vendor and knowing what quality you're getting is the iffy part. Name brand lenses and mirrors from known manufacturers cost 2-3 times as much as the China imports, but quality is better. You'll likely pay more from a reputable vendor in the UK, but at least know what you're getting. You'll just have to weigh the cost & use (hobby, business etc) and go from there.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/Z4apZTaZmbp) &mdash; content and formatting may not be reliable*
