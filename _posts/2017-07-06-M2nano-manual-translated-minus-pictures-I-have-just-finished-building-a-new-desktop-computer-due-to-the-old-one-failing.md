---
layout: post
title: "M2nano manual translated (minus pictures) I have just finished building a new desktop computer due to the old one failing"
date: July 06, 2017 23:17
category: "Hardware and Laser settings"
author: "HalfNormal"
---
M2nano manual translated (minus pictures)



I have just finished building a new desktop computer due to the old one failing. One of the first things I tried was to convert the original M2nano manual into English. The information is quite eye opening. It has all the configuration settings for the different nano boards. This lets you know what your motor and belt are. Also gives a little insight into how the board works. 

Here is the translation and original. Enjoy!



Translated;

[https://drive.google.com/open?id=0B9qXcw3VRDg0Q3Iyb0Y4NU1KTVE](https://drive.google.com/open?id=0B9qXcw3VRDg0Q3Iyb0Y4NU1KTVE)



Original;

[https://drive.google.com/open?id=0B9qXcw3VRDg0Z2FLV1ozTG13TG8](https://drive.google.com/open?id=0B9qXcw3VRDg0Z2FLV1ozTG13TG8)







**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *July 08, 2017 13:06*

#K40M2Nano



**+HalfNormal** this is great work thanks for doing this. Quite insightful.



<b>Quick read</b>

Observations/interpretations/questions/inconsistencies.



<b>Motor and power supply selection</b>

For x axis using nema 42 run at 1.5 amps but better performance at much higher currents and voltages. 

Does this mean the on board drivers can handle more current, no? 

On mine the stepper driver is a Allegro A4985 whose drive is rated at  35V +/-1A. @24V that might be 1.45A, consistent with the above recommendation and what I think the LPS 24VDC supply can handle.



Shows how to connect external M415DRV, drives. 



<b>Noise immunity</b>

Recommends 3 separate supplies for best noise immunity. Standard K40 don't do this? USB noise problems can occur from stepper drive current.



9 out of 10 times hardware failures are caused by High Voltage :)




---
**laurence champagne** *July 08, 2017 15:50*

Thanks so much for this. Can anyone figure out if you are able to adjust the steps per mm with any of this information. There are 50 instances of the word step in there and there is a lot of information about stepper motors but I'm not for sure if I seen anything about adjusting this setting.


---
**Aydın Şatıroğlu** *October 12, 2018 08:02*

where can i buy this controller


---
**Don Kleinschnitz Jr.** *October 12, 2018 10:04*

**+Aydın Şatıroğlu** [amazon.com - Amazon.com: k40 laser controller](https://www.amazon.com/s/ref=nb_sb_ss_c_1_10?url=search-alias%3Daps&field-keywords=k40+laser+controller&sprefix=k40+contro,aps,197&crid=3N1UGUM0I3DYR)


---
**Aydın Şatıroğlu** *October 12, 2018 17:50*

Hi



Thanks for response quickly first

but i am afraid this is m2 nano version

m2 external driver version thanks



Aydın


---
**Aydın Şatıroğlu** *October 12, 2018 17:51*

i need external driver version m2


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/5z8oqNoC2qS) &mdash; content and formatting may not be reliable*
