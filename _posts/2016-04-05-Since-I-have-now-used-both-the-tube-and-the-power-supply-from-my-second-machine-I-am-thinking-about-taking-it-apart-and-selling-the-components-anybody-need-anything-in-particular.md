---
layout: post
title: "Since I have now used both the tube and the power supply from my second machine I am thinking about taking it apart and selling the components anybody need anything in particular"
date: April 05, 2016 03:02
category: "Discussion"
author: "3D Laser"
---
Since I have now used both the tube and the power supply from my second machine I am thinking about taking it apart and selling the components anybody need anything in particular  





**"3D Laser"**

---
---
**Alex Krause** *April 05, 2016 03:13*

Everything but the tube and power supply?


---
**Anthony Bolgar** *April 05, 2016 03:19*

I would be interested in the x and y axis setup. How much would you want for it?


---
**3D Laser** *April 05, 2016 11:02*

**+Alex Krause** yes 


---
**3D Laser** *April 05, 2016 11:03*

**+Anthony Bolgar** did you want the mirrors and the motors or just the axis 


---
**Anthony Bolgar** *April 05, 2016 13:19*

No mirrors, just motors and axis


---
**3D Laser** *April 05, 2016 16:52*

**+Anthony Bolgar** cool I am sure we can work out a deal


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/ZKqHAuGQUnd) &mdash; content and formatting may not be reliable*
