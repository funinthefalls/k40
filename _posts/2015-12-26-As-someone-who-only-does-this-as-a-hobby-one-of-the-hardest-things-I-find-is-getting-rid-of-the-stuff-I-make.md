---
layout: post
title: "As someone who only does this as a hobby, one of the hardest things I find is getting rid of the stuff I make"
date: December 26, 2015 15:00
category: "Object produced with laser"
author: "Cam Mayor"
---
As someone who only does this as a hobby, one of the hardest things I find is getting rid of the stuff I make. This was an afternoon of cutting and gluing that was quite happily accepted by a friend. The six beer inside helped to make it more attractive.



![images/e473b5e9e1bcd00a12113631c3e496a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e473b5e9e1bcd00a12113631c3e496a6.jpeg)
![images/877f87866d1f1d6d386a45a0090c82ef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/877f87866d1f1d6d386a45a0090c82ef.jpeg)

**"Cam Mayor"**

---


---
*Imported from [Google+](https://plus.google.com/110883213653290462996/posts/JsxjQVMhGtm) &mdash; content and formatting may not be reliable*
