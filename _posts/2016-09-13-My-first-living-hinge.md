---
layout: post
title: "My first living hinge"
date: September 13, 2016 14:31
category: "Object produced with laser"
author: "David Spencer"
---
My first living hinge. 



![images/3d835143919f4b0e523502c9d12f1bb0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d835143919f4b0e523502c9d12f1bb0.jpeg)
![images/7e1aee1fb2d9eb760fdca03546438e6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e1aee1fb2d9eb760fdca03546438e6a.jpeg)

**"David Spencer"**

---
---
**A “alexxxhp” P** *September 13, 2016 16:04*

Looks nice


---
**photomishdan** *September 13, 2016 19:40*

How long did it take on the K40? 


---
**David Spencer** *September 13, 2016 20:01*

 20-30 minutes 


---
**photomishdan** *September 13, 2016 20:03*

**+David Spencer** is there a template for this, or did you design it yourself? 


---
**andy creighton** *September 14, 2016 01:38*

Very nice.   I couldn't wait to design one when I got my laser running.   No matter how many times you see them they are always fascinating. 


---
**David Spencer** *September 18, 2016 19:18*

**+photomishdan** [https://www.thingiverse.com/thing:15845](https://www.thingiverse.com/thing:15845)


---
**photomishdan** *September 18, 2016 19:35*

**+David Spencer** Thanks David. Its really good to have a look at the files.


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/fyPJEzC7yov) &mdash; content and formatting may not be reliable*
