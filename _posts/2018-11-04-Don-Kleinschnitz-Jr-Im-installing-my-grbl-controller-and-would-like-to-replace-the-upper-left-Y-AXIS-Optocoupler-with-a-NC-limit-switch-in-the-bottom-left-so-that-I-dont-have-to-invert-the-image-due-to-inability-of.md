---
layout: post
title: "Don Kleinschnitz Jr. . I'm installing my grbl controller and would like to replace the upper left (Y-AXIS) Optocoupler with a NC limit switch in the bottom left so that I don't have to invert the image due to inability of"
date: November 04, 2018 16:03
category: "Modification"
author: "Timothy Rothman"
---
**+Don Kleinschnitz Jr.** .  I'm installing my grbl controller and would like to replace the upper left (Y-AXIS) Optocoupler with a NC limit switch in the bottom left so that I don't have to invert the image due to inability of inkscape to give an origin at upper left.  



I think I can remove the resistor that effectively would protect and disable the Y-opto, and then wire the NC limit switch to pins 7 and 9 of C10.  Can you please confirm.  



Of course, I'm referring to "the middleman" schematics on your site.

[http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)







**"Timothy Rothman"**

---
---
**Don Kleinschnitz Jr.** *November 04, 2018 19:05*

What controller are you using?

You are referring to the end-stop daughter board schematic but mention the middleman board?

How is your controller connected to the daughter card?



.......

I assume you are referring to R3 on the daughter board. Indeed if you pull the LED's resistor the output transistor should shut off and produce a high impedance.



Where are you going to connect the switch in at? 

If you are going to have to pull out the daughter board and remove a resistor anyway, why not  pull out the Optocoupler and connect a plug to pin 7 & 9. That way there is no question of interference?

You can solder leads and a 2 pin plug into the respective ground and Y signal holes that you vacated when you took out the optocoupler!


---
**Timothy Rothman** *November 06, 2018 06:08*

I'm using Paul's grbl from Awesome.tech.  It's easier to pull a single leg of a resistor than desolder 4 legs at once of the Opto.  Using your terminology on your site, I'm actually referring to the daughter card.


---
**Don Kleinschnitz Jr.** *November 06, 2018 14:15*

**+Timothy Rothman** True that, but you still have to get a ground and Y signal soldered to that board :). 

I periodically repair these which requires removal and replacement of the coupler. The part comes out easy with some solder wick. If you do not care about keeping the part you can pull the cover off, clip the leads and un-solder the remaining wire.


---
*Imported from [Google+](https://plus.google.com/107673954565837994597/posts/gSBASQU96BL) &mdash; content and formatting may not be reliable*
