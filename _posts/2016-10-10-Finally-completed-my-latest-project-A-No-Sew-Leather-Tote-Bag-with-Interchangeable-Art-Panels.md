---
layout: post
title: "Finally completed my latest project: A No-Sew Leather Tote Bag with Interchangeable Art Panels"
date: October 10, 2016 09:44
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Finally completed my latest project: A No-Sew Leather Tote Bag with Interchangeable Art Panels.



Pretty much the entire project was completed with the assistance of my K40, with the exception of dying/manually hammering rivets/press-studs.



If you have an Instructables.com account, you can view the entire tutorial for the project there. Also, give me a vote in the Tandy Leather Contest 2016 if you like my entry.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Finally completed my entry & the tutorial on Instructables.com.



If you have an account there & like my entry, please give me a vote for the Tandy Leather Contest 2016.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Don Kleinschnitz Jr.** *October 10, 2016 10:12*

Excellent project and craftsmanship. Went to instructables and found the Tandy contest but could not find your project in the entries to vote on?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 10, 2016 10:26*

**+Don Kleinschnitz** Thanks Don. There is some clause where they have to approve it for eligibility. So it won't be available until they do that. Not sure what time it is in US at the moment, so might have missed the end-of-business hours for today.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 10, 2016 18:05*

The entry has been accepted as eligible for the contest & voting is now live! If you're interested in giving me a vote, you can sign-in easily with your G+ account rather than creating a new account :)


---
**Don Kleinschnitz Jr.** *October 10, 2016 18:13*

**+Yuusuf Sallahuddin** Done.... good luck on the contest.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 10, 2016 18:46*

**+Don Kleinschnitz** Thanks very much. Your support (and everyone else's) is much appreciated.


---
**HalfNormal** *October 11, 2016 00:42*

Great looking and practical to boot! Just voted for it on Instructables.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 11, 2016 11:34*

**+HalfNormal** Thanks for that. :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 14, 2016 23:19*

I would like to update anyone that gave me support by voting for my entry. I've just received an email from Instructables stating that I have progressed to the next round of judging as a finalist :)



Thanks to everyone for their support of my project!


---
**Don Kleinschnitz Jr.** *October 15, 2016 00:56*

NICE! Congrats


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 01:05*

**+Don Kleinschnitz** Thanks Don :) Much appreciated.


---
**HalfNormal** *October 15, 2016 01:08*

Do we need to vote again? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 01:36*

**+HalfNormal** The votes now are just down to the judges having an extra round of votes I believe. Closed for user voting now.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 21, 2016 19:44*

New update. I've received an email from Instructables this morning explaining that they are having issues with their website, hence the late posting of winners. They are hoping to have it fixed & post winners next week.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 25, 2016 21:34*

Results are finally in & I have received a 3rd place prize :) Thanks again for everyone's support.


---
**Don Kleinschnitz Jr.** *October 25, 2016 23:22*

Congrats In my view its 1st place work!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2016 06:40*

**+Don Kleinschnitz** Thanks Don. Much appreciated. Unfortunately you weren't one of the judges haha.


---
**HalfNormal** *October 26, 2016 12:50*

**+Yuusuf Sallahuddin** you are number one in my book!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2016 13:16*

**+HalfNormal** Thanks Half. Either way I'm stoked to have been recognised as a winner. Better than being a runner up (& I still get the cool robot shirt I've been wanting haha).


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/QaReLouuM8g) &mdash; content and formatting may not be reliable*
