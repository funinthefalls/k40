---
layout: post
title: "I need a 1.7 vdc 60 amp constant current diode driver...anyone have one laying around or know where to get one?"
date: February 19, 2017 16:09
category: "Discussion"
author: "Scott Thorne"
---
I need a 1.7 vdc 60 amp constant current diode driver...anyone have one laying around or know where to get one?





**"Scott Thorne"**

---
---
**Don Kleinschnitz Jr.** *February 19, 2017 16:25*

Are you the user that asked **+Ray Kholodovsky** for such?



It was an intriguing problem so did some research on this when he asked me and I found nothing close to this level of current capacity.



You may know, laser diode drivers are more complex than folks realize as they can easily fry the diode in u-secs with transients, the diodes are expensive fuses. They need close current regulation and soft start functions.



I concluded that the best approach might be to get a standard LD drive (that has all the power control stuff) and add a high current MOSFET output stage that can handle 60 amps. My guess is that you probably also want PWM control?



Why 60 amps? 

 


---
**Scott Thorne** *February 19, 2017 22:29*

The 50 watt yag diode puts out 50 watts @ 60 amps.


---
**Scott Thorne** *February 19, 2017 22:30*

I can run it now but my driver only has 35 amps of output current @ 1.7 vdc.


---
**Don Kleinschnitz Jr.** *February 19, 2017 22:31*

**+Scott Thorne** Gulp! that't a gagle whew!


---
**Don Kleinschnitz Jr.** *February 19, 2017 22:33*

Can you point me to the 35 amp one?

Schematic would be ideal.


---
**Scott Thorne** *February 20, 2017 10:14*

I'll try and find you a schematic...it didnt come with one but the model is SDL8110B


---
**Don Kleinschnitz Jr.** *February 20, 2017 13:00*

Update: whoops this is not the SDL8110B

ignore below. Although this site is interesting





[http://redlum.xohp.pagesperso-orange.fr/electronics/SDL800.html](http://redlum.xohp.pagesperso-orange.fr/electronics/SDL800.html)



[http://redlum.xohp.pagesperso-orange.fr/electronics/data/ldi800.man.pdf](http://redlum.xohp.pagesperso-orange.fr/electronics/data/ldi800.man.pdf)


---
**Scott Thorne** *February 20, 2017 17:28*

I have 3 of these too


---
**Scott Thorne** *February 20, 2017 17:28*

I think these are good for around 2 amps....lol


---
**Scott Thorne** *February 20, 2017 17:33*

Hey Don if you can make one of these work...I'll send you 2 of them.


---
**Don Kleinschnitz Jr.** *February 20, 2017 18:22*

**+Scott Thorne** you mean the sdl800?


---
**Scott Thorne** *February 20, 2017 18:31*

Yes....i have 3 of them


---
**Don Kleinschnitz Jr.** *February 21, 2017 17:05*

**+Scott Thorne** I took a quick look at the design of the SDL800, if I read it all correct, the diode is run with the anode at ground and the cathode at <s>V. That makes the beefier power supply more complex to build/buy and I don't like (</s>) supplies if they are not needed.



Do you know if this is normal for high power diodes to be run that way. My 2 watt does not.



Do you have specs on your diode?



What are you going to do with this setup?



I stumbled on this one that can be modulated:

[http://optlasers.com/en/high-power-drivers/64-5khz-3-24v-60a-hpldd-5902693110477.html](http://optlasers.com/en/high-power-drivers/64-5khz-3-24v-60a-hpldd-5902693110477.html)



........

Although the SDL800 has a lot of useful features I judge it would be easier to add a stage to a cheap drive like.

[amazon.com - Amazon.com: 5A Constant Current/Voltage LED Driver Battery Charging Module Voltmeter Ammeter Compatible With Arduino by Atomic Market: Computers & Accessories](https://www.amazon.com/gp/product/B00TNKE0M2/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)




---
**Scott Thorne** *February 21, 2017 18:45*

I looked at the one you posted and it only goes down to 3 volts....I'll send you the info on my diode when i get off


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/ANTRUvZqtuo) &mdash; content and formatting may not be reliable*
