---
layout: post
title: "HI All. New to the group and am considering one of the k40 units to engrave on my woodworking projects"
date: August 17, 2016 19:16
category: "Software"
author: "Allen Oertel"
---
HI All.  New to the group and am considering one of the k40 units to engrave on my woodworking projects.  However, I'll also need to get a laptop as I currently only have chromebooks.  I haved a computer resale shop nearby that sells good used laptops that run either windows 7, 8.1 or 10.  If I'm getting new laptop, I'd like the operating system to be up to date, but I've not seen any information concerning windows 10. How do you recommend i proceed on this?





**"Allen Oertel"**

---
---
**Jim Hatch** *August 17, 2016 19:29*

I am running Windows 10. I believe folks here are running all of the versions you mentioned. Pick the ones you're .let comfortable with.


---
**greg greene** *August 17, 2016 19:32*

no problem on my laptop with win 10


---
**Jonathan Davis (Leo Lion)** *August 17, 2016 19:45*

Win 10 of course and I do know that if you want a inexpensive computer I know the Lenovo Ideapad100s is a good cheep option at $150


---
**HalfNormal** *August 17, 2016 21:05*

Win10 with no issues. 


---
**VetGifts By G** *August 17, 2016 21:12*

I'm running 10 as well, its all well!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 17, 2016 21:26*

I've ran the software on win7 & win10. Works fine on both. Only issue with win10 is that the toolbar disappears, but there is the system tray icon near the clock that has all the functions you need accessible.


---
**Ben Marshall** *August 18, 2016 00:41*

Running 10 without issues. Do not use Norton anti-virus! It will block your program from running. I have McAfee w/o issues. Other members can recommend other av programs that work as well


---
**HalfNormal** *August 18, 2016 00:55*

Avast plays nice with the software.


---
**Robi Akerley-McKee** *August 18, 2016 07:18*

Running Windows xp,7,8.1,10 and Linux Debian 16.04LTS no problems.  I'm running arduino based board right now, not Moshidraw.


---
*Imported from [Google+](https://plus.google.com/106799321907858459532/posts/e9nU2n9AjqE) &mdash; content and formatting may not be reliable*
