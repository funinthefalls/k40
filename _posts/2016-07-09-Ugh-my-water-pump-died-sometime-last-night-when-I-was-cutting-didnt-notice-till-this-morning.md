---
layout: post
title: "Ugh, my water pump died sometime last night when I was cutting, didn't notice till this morning"
date: July 09, 2016 15:14
category: "Modification"
author: "Tev Kaber"
---
Ugh, my water pump died sometime last night when I was cutting, didn't notice till this morning. Hopefully I didn't destroy the tube... it was still cutting fine when I shut it down for the night. 



Can't seem to find any water pumps of sufficient volume at local pet stores, any suggestions of where else to look?  I could order online but I wanted to laser some stuff this weekend...





**"Tev Kaber"**

---
---
**greg greene** *July 09, 2016 15:31*

hook it  up to a faucet with a garden hose - put the drain outside - you don't need a large volume.  This will get you through the weekend till you get another aquarium pump


---
**greg greene** *July 09, 2016 15:31*

And it will water your garden/lawn at the same time: Bonus !!!


---
**Jim Hatch** *July 09, 2016 15:42*

Or go to a home/building supply/hardware store and get a backyard pond or fountain pump. 


---
**Coherent** *July 09, 2016 17:16*

In the U.S. go to Home Depot, Ace, Lowes etc. for a new pump. And, put a cheap flow sensor on your water line wired to your "fire" button so that when the water flow stops, your laser won't fire. Cheap insurance!


---
**HP Persson** *July 10, 2016 14:34*

I use pumps made for caravans and trailer homes, small 12v pumps and powerful enough to cool the laser. You can also find similar pumps made for use in boats. No need for the fish tank pumps, theese pumps are cheaper and more powerful. Atleast here in Sweden :)

Think they are named kitchenette pumps in english :)




---
**Tev Kaber** *July 10, 2016 16:03*

I notice most non-aquarium pumps have a large (3/4" ID) diameter port, what's the right way to adapt that down to the tiny cooling tube diameter? Will a pump care that it's squeezing down like that, will the pressure hurt it?


---
**Jim Hatch** *July 10, 2016 17:02*

If you get a garden or fountain pump you'll find adapters made for the outlet fitting of the pump. Lots of small garden fountains use 1/4" tubing for the flow. If you can't find the adapters with the pump then you can make your own using a series of adapters that step it down to the laser's cooling tubing size. In the U.S. Home Depot or Lowes (or any decent sized hardware store) has a section of different plumbing adapters you can make work. If you don't have one, a shout-out to the list here should get you a piece mailed from a friendly soul. :-) 



I'll volunteer if you can wait a few days - I'm flying to a client's for most of the week so won't be able to make you one quick but can next weekend.


---
**Jim Hatch** *July 10, 2016 17:07*

Amazon has them too - search on 3/4" GHT to 3/8" barb. The garden hose thread is what's used in the pump and I seem to remember the laser's hose was 3/8" ID.


---
**Tev Kaber** *July 10, 2016 17:54*

Got it figured out, thanks for the help!  After much perusing of a confusing array of fitting adapters, managed to find the two I needed to go from the pump output to a barb for the water inlet. 



Laser seems to be working fine, though all my jostling to get the air bubbles back out seems to have bumped a lens slightly out of alignment.  And when I pressed my laser test button, the button broke, stuck down!  Haha time for a new test button, prolly have something on hand for that. 



Been meaning to tweak the alignment, never calibrated it, since it seemed pretty good as it arrived, now's a good a time as any.  ;)


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/MwosbbcPHqX) &mdash; content and formatting may not be reliable*
