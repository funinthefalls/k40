---
layout: post
title: "Adding a GLCD to K40 Smoothie My GLCD arrived today and sadly Uberclock is still out of the GLCD shield for the smoothie"
date: November 02, 2016 22:27
category: "Smoothieboard Modification"
author: "Bill Keeter"
---
Adding a GLCD to K40 Smoothie



My GLCD arrived today and sadly Uberclock is still out of the GLCD shield for the smoothie. Can I just run wires down to the specific leads on the smoothie? Any thing else needed?



Also I'm getting my Smoothie from **+Scott Marshall** as one of his ACR kits. Since it runs a separate power supply with 24v and 5v to the Smoothie is there any need to buy a 5V switch regulator to drive the extra power for the GLCD?





**"Bill Keeter"**

---
---
**Anthony Bolgar** *November 02, 2016 22:40*

You need to put in a diode instead of the Voltage regulator, it is in the [smoothie.org](http://smoothie.org) site instructions for displays. It also shows the wiring diagram so you can connect using individual wires instead of the adapter, but the adapter is much easier to use. And you can order from RobotSeed.com, it took 8 days for mine to get from Europe to Canada, shipping was not expensive.


---
**Scott Marshall** *November 03, 2016 20:29*

Working on your kit today Bill. Been sick, so it's on and off, but I'm making progress.

 I've included a 3amp extra 5V  regulator, so it should be able to handle any gear you attach no problem. I'm going to a switching regulator design for the newer units, It's been a nightmare trying to get genuine Linear regulator chips lately. The switchers run so cool they don't even need a heatsink until you pass 2amps. 



I added all the headers to the Smoothie, so your board should be a plug-in when you get it.



I expect you kit to ship by Monday at the latest. I've got one who's in front of yours (he was also waiting on the 4X), but am doing a 'parallel' build. 


---
**Bill Keeter** *November 03, 2016 20:32*

**+Scott Marshall**  thanks for the update Scott. Hope you're feeling better. No worries on timing. Health before hobby :)


---
**Scott Marshall** *November 03, 2016 20:43*

It makes me feel better to work on my toys...



Thanks Bill


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/ZtNzop5UdeY) &mdash; content and formatting may not be reliable*
