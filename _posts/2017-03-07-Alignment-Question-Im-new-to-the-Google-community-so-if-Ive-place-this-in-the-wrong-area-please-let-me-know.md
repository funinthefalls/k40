---
layout: post
title: "Alignment Question... I'm new to the Google+ community, so if I've place this in the wrong area, please let me know"
date: March 07, 2017 22:43
category: "Discussion"
author: "Bryan Markovich"
---
Alignment Question...

I'm new to the Google+ community, so if I've place this in the wrong area, please let me know. 



I just bought a K40 laser, and I was trying to go through the alignment procedure. Most procedures will tell you the first mirror is no big deal as long as it's close to the center, but nobody tells you what to do if it's not. Hopefully you can tell from the ash on the mirror how the laser is hitting the left edge of the first mirror. Is this acceptable, or will I need to adjust the first mirror?



Thanks!

![images/6c51da1b5a47c7624846be2f6685e19c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c51da1b5a47c7624846be2f6685e19c.jpeg)



**"Bryan Markovich"**

---
---
**Mark Brown** *March 08, 2017 00:25*

I'd see if there's any room for the mirror to slide on those two screws you can see. If it's already slid all the way the next step would be to adjust (flex or shim) the tube brackets, but I'd just put up with it instead of doing that.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2017 01:01*

The first mirror is adjustable in the same way that the 2nd mirror is (using the 3 screws).  In this case, it looks such that your screws are as far out as possible on the right 2 screws (because the mirror square seems flat against the mount square). As such you can't adjust that way I guess.



I would consider removing the whole mirror from the base white coloured mount & drilling the front hole (for those 2 screws) wider to the right (when looking from back of machine to front). Or alternatively drilling the silver plate under the mirror a bit further to the opposite side for the front hole (probably easier than trying to get a drill into the tube compartment). Then you could have an extra 1mm or so to adjust the angle to centre the beam.



However, I can't see any reason why hitting dead centre on the first mirror is so necessary. I imagine that the only real reason would be so that the spot doesn't only partially deflect (e.g. maybe hitting the rim). If the entire laser spot is hitting on the mirror, the position on the mirror shouldn't really matter. But, it could be related to heat transfer & longevity of the mirror (no idea, just thought popped in my head).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2017 01:03*

Actually, upon closer inspection of your pic, it looks like the back most of those 2 screws may actually be in a horizontal slit in the silver plate under the mirror? Maybe you can loosen that screw & pivot the mirror a bit?


---
**Phillip Conroy** *March 08, 2017 03:48*

On my k40 l had to make the slot the 2 screws in longer 


---
**E Caswell** *March 08, 2017 08:12*

I had the same problem with mine. I found that loosening the tube clamps there is enough movement in the screws  to slide the tube over towards the back of the machine and didn't need to drill any holes. 

Also found that on mine the tube needed lifting at the back with packers so as to hit the mirror anywhere near the middle of it. 


---
**Bryan Markovich** *March 08, 2017 11:14*

Thanks everybody for the help. That gives me a couple things to look at.


---
*Imported from [Google+](https://plus.google.com/103003938270950747642/posts/48fHWxUMt9E) &mdash; content and formatting may not be reliable*
