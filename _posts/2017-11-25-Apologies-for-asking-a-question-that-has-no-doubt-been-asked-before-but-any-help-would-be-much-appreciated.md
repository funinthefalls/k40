---
layout: post
title: "Apologies for asking a question that has no doubt been asked before but any help would be much appreciated"
date: November 25, 2017 19:58
category: "Original software and hardware issues"
author: "Ian Hastie"
---
Apologies for asking a question that has no doubt been asked before but any help would be much appreciated.

After almost a year of very light use my laser tube stopped firing today whilst in use. The water flow is good, the fan is running on the PSU and the green light is on. If I press the test button on either the console or the PSU with the power dial up off zero I get a very slight flicker on the ammeter and when looking at the tube I get color inside but no laser beam. I have given all connections a quick check and cant see any obvious cracks in the tube. Is my PSU or tube shot ?

Like

Show More Reactions

Comment





**"Ian Hastie"**

---
---
**Chris Hurley** *November 25, 2017 21:33*

Water change? 


---
**Terry Evans** *November 26, 2017 14:54*

**+Chris Hurley** Very possibly your PSU.  Here is a good starting point.  [k40laser.se - Troubleshooting a "no fire" issue - K40 Laser](https://k40laser.se/diy-how-to/troubleshooting-no-fire-issue/)




---
**Don Kleinschnitz Jr.** *November 26, 2017 15:35*

Is there light at the output of the laser? Put sticky note on first mirror. 


---
**Ian Hastie** *November 26, 2017 15:42*

Thanks for everyone's help with this, I have ordered a PSU to try.




---
*Imported from [Google+](https://plus.google.com/118400196286801940771/posts/REYMgGKJhHK) &mdash; content and formatting may not be reliable*
