---
layout: post
title: "Hey Pigeon FX , you'll get more comments if your post does not have comments disabled!"
date: May 03, 2018 16:48
category: "Modification"
author: "James Rivera"
---
Hey **+Pigeon FX**, you'll get more comments if your post does not have comments disabled!



<b>Originally shared by Pigeon FX</b>



Carbin filters to remove smoke? 



I'm in the process of moving, and it is impossible to run an extraction vent out of a window in the new location, I was wondering if a carbon filter would do the job? (mainly ctting acrylic and engraving 2 ply laminates, ratherthen woods.)

![images/95bd78916012ab86d6181de22916b3b9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/95bd78916012ab86d6181de22916b3b9.jpeg)



**"James Rivera"**

---
---
**syknarf** *May 03, 2018 17:52*

I'm using a filter like this (larger size) since a month ago, with good results. Smells are highly reduced, but don't know how many time it would last before the carbon saturates. But I put it on the exhaust outside the window, not inside the room (I want to minimize the fumes and smells that can affect to my neighbourhoods).


---
**Ned Hill** *May 05, 2018 14:18*

I've look in to possibly doing this before and there is a lot of info out there if you search for it.  To prolong the life of the filter it is advisable to have a prefilter to catch particulates that can foul the carbon filter.  Also, it's theoretically possible to regenerate activated carbon by baking it out at a high temp.  Just beware, if you do the regeneration, that all that "smell" will be coming back out, so your home oven is not recommended. 


---
**Eric Lovejoy** *May 28, 2018 22:54*

I havent done it yet, but I think you need to make sure the filter can handle the volume of air you will be sending it, and an inline fan, right before the filter will help. the fan, and filters will both say what thier liter per minute rating is. if you send more air to the filter than what it can handle, you'll get inadequate filtering.


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/XKhAS3Sgzp8) &mdash; content and formatting may not be reliable*
