---
layout: post
title: "When I use Laserdrw to cut out a project how can I decide what color I want it to actually cut?"
date: February 26, 2016 15:02
category: "Discussion"
author: "Jason Reiter"
---
When I use Laserdrw to cut out a project how can I decide what color I want it to actually cut? I thought I could just invert the color because I thought it always cut the black but that does not seem to be the case





**"Jason Reiter"**

---
---
**3D Laser** *February 26, 2016 15:28*

I think if you go to engrave you have to chose cutting.  To my knowledge you can't do both at the same time 


---
**Jason Reiter** *February 26, 2016 15:33*

no Im not looking to engrave. I just want to cut but when I have a file how can I choose weather I want it to cut the white portion or the black portion?


---
**Jim Hatch** *February 26, 2016 15:40*

I believe it cuts/etches any line - colors are ignored. I use colors in my Illustrator files for the large laser I use at our Makerspace because that machine's software (LaserCut) allows me to set each color to cut or engrave and the order so I can do it in one pass vs multiple files. That's a pretty standard approach for most higher-end lasers. 



When I want some things cut & other things engraved using my K40 I create an Illustrator file with different layers for the cuts vs engraves. Then when I export those to the bitmaps that LaserDRW will use to send to the laser I do each layer as a separate BMP. Then I import one BMP into LaserDRW, do the engrave and then leave the material in the laser, reload LaserDRW with the next "layer" that I want to cut and send that out to the laser. 



I can take the same Illustrator file and send it to my big laser's software and use the layer colors to define the cuts/engraves/order. If you ever get a bigger laser or more robust software you'll be able to use the same files you created for the K40 just in a more efficient way.


---
**Jason Reiter** *February 26, 2016 16:03*

Below is an example .what if I only want to cut out the white part of this logo? can I do that ?



[http://www.pycomall.com/images/P1/Volkswagen_Logo_Vector_File_in_AI_format.jpg](http://www.pycomall.com/images/P1/Volkswagen_Logo_Vector_File_in_AI_format.jpg)


---
**Jim Hatch** *February 26, 2016 16:08*

You need lines to cut, you can't cut chunks. So you need the lines (.001mm thickness) specified. It will engrave the big black blocks, it won't "cut out" the black (or the white). So you need to create lines.


---
**Ashley M. Kirchner [Norym]** *February 26, 2016 16:16*

Even easier, invert the image. Then it'll cut what you're expecting it to cut. 


---
**Jim Hatch** *February 26, 2016 16:23*

**+Ashley M. Kirchner**​ I don't think it will cut - that was my first reaction. I think the black "lines" will be too thick to cut.


---
**Ashley M. Kirchner [Norym]** *February 26, 2016 16:26*

They're not lines, it's just a chunk of material to remove. It looks for the edge between black and white and cuts that. At least, if I'm understanding what the OP wants to do here. 


---
**Jim Hatch** *February 26, 2016 16:35*

That's how my big laser works but I thought LaserDRW wanted the lines. I'm not home right now so can't do a quick test. If that were the case though wouldn't it have done that for him originally just with the "wrong" parts cut out?


---
**Ashley M. Kirchner [Norym]** *February 26, 2016 16:49*

No, unfortunately, LaserDRW uses bitmaps. I WISH it used vector lines. Internally it will trace the bitmap and figure out what areas to cut out. And yes, if the OP simply sent the image as is, it will cut all the black out, which I gather isn't the result they're after. That's why I suggested inverting the image. 


---
**Jason Reiter** *February 26, 2016 17:09*

Yes I thought the same thing that if I invert the image that it would cut the oppisite, but it still cut the same area out 


---
**Jason Reiter** *February 26, 2016 17:11*

so here is the image I am trying to do. the top one . No matter if I invert the colors or not it still cuts the same area 



[http://img1.123freevectors.com/wp-content/uploads/new/buildings/028-detroit-skyline-vector-silhouettes-free.png](http://img1.123freevectors.com/wp-content/uploads/new/buildings/028-detroit-skyline-vector-silhouettes-free.png)


---
**Jason Reiter** *February 26, 2016 17:15*

sorry I ment to add I only want the white cut out and then the ouotline 


---
**Ashley M. Kirchner [Norym]** *February 26, 2016 18:40*

You realize you are going to end up with loose pieces, right? So for something like this, I would do it in two cut steps. There are two images here that you can grab if you want. I'd make one for the outline cut, and another just for the white pieces you want to cut out. Make sure your layout is the exact size at the actual image, then drop the white cut in first, run that, then put the outline cut in and run that. [https://drive.google.com/folderview?id=0B8NExy6DoX2RUjFDSE9PeWhEaHc&usp=sharing](https://drive.google.com/folderview?id=0B8NExy6DoX2RUjFDSE9PeWhEaHc&usp=sharing)


---
**Jason Reiter** *February 26, 2016 18:52*

I did know there were loose peices I thought I could fill them in white to attach . But your method makes much more sence. thank you very much. Thank everyone this is a gre

at site for new people like me. !


---
**Ashley M. Kirchner [Norym]** *February 26, 2016 18:56*

On a side note: that image is a rather low resolution, so you may end up with some ugly jagged edges when you cut that.


---
*Imported from [Google+](https://plus.google.com/106551755070259873093/posts/29wLQR8R25V) &mdash; content and formatting may not be reliable*
