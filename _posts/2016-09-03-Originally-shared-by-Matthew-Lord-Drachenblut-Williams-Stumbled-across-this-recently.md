---
layout: post
title: "Originally shared by Matthew Lord Drachenblut Williams Stumbled across this recently"
date: September 03, 2016 17:00
category: "External links&#x3a; Blog, forum, etc"
author: "Alex Krause"
---
<b>Originally shared by Matthew “Lord Drachenblut” Williams</b>



Stumbled across this recently.  Could be a useful item for those building c-beam machines





**"Alex Krause"**

---
---
**greg greene** *September 04, 2016 00:26*

love to get the file for that


---
**Alex Krause** *September 04, 2016 00:31*

Check thingiverse


---
**greg greene** *September 04, 2016 00:50*

Got it - thanks !!!


---
**Robi Akerley-McKee** *September 06, 2016 06:21*

These like most drag chains that are laser cut are very sensitive to thickness.  So you might have to scale your file a bit for thicker or thinner stock.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/Qaz8vhku4iX) &mdash; content and formatting may not be reliable*
