---
layout: post
title: "My k40 laser tube has started making these loud \"spark-sounds\"..."
date: March 08, 2017 08:05
category: "Original software and hardware issues"
author: "Andreas Benestad"
---
My k40 laser tube has started making these loud "spark-sounds"... there is also a light flashing from the tube every time the sound comes. What could that be?? Yesterday I turned the tube a little to get the air bubbles out...

It's a fairly new machine that has not been used much yet.



Thanks for any input/advice!!





**"Andreas Benestad"**

---
---
**Anthony Bolgar** *March 08, 2017 08:22*

Sounds like the HV line is arcing to the case.


---
**Andreas Benestad** *March 08, 2017 08:24*

yeah, there seems to be some arching to the case!! Anything I can do about that? Is it broken..?


---
**Anthony Bolgar** *March 08, 2017 08:29*

If the arcing is coming from the cable or connections to the tube, you can insulate them with silicon , most k40's ship with a small tube of silicon. If the arcing is caused by a cracked tube, it will need replacing.


---
**Andreas Benestad** *March 08, 2017 09:06*

thanks **+Anthony Bolgar**. I have insulated (temporary with a piece of mdf, an electrician friend said that would work) and the problem seems to have disappeared. And the tube seems to be working still. Puh! 


---
**Anthony Bolgar** *March 08, 2017 09:12*

So I would go over every HV connection and make sure you insulate each one well with the silicon. Glad you tube is still OK. Do not operate it until the arcing issue is resolved, as it will rapidly degrade the tube.


---
**Andreas Benestad** *March 08, 2017 09:32*

Would you say the arcing issue is (temporarily) resolved with what I have done now? Insulating with mdf? I am working a job right now and there is no (visible) arcing or noise.


---
**Anthony Bolgar** *March 08, 2017 09:37*

I would not trust just using a piece of mdf, it can be knocked out of the way by machine vibrations. I seriously think you should permanently resolve it by using the silicon before continuing to operate the machine. Remember , the HV line can kill you


---
**Andreas Benestad** *March 08, 2017 09:41*

I hear you. The mdf is taped to the output where the arcing was taking place. So I don't think it will move very easily. Having said that, you are right about not taking any risks. Thanks also for the heads up about the power involved here... easy to forget how dangerous it is.


---
**Anthony Bolgar** *March 08, 2017 09:48*

Can get scary on some machines, I have a redsail LE400 that had an originall power supply that had all the HV circuitry exposed in the electronics bay, no case or protection of any kinf. First thing I did was to rip out that supply and replace it with a safer enclosed PSU.


---
**Don Kleinschnitz Jr.** *March 08, 2017 14:48*

Do you actually see external arching? What type of coolant are you using?




---
**Madyn3D CNC, LLC** *July 08, 2017 21:33*

Hi **+Andreas Benestad**, what happened with your arcing problem? Has the MDF worked out or have you found another solution?



Wondering because my LPSU started arcing to the metal case. 

[plus.google.com - #K40LPSU #K40PSU #K40Troubleshoot #K40Arcing So I accidently deleted my post...](https://plus.google.com/u/0/+vapetech/posts/7jRZTAmtRpj)


---
*Imported from [Google+](https://plus.google.com/112581340387138156202/posts/hHd846peXqD) &mdash; content and formatting may not be reliable*
