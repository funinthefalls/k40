---
layout: post
title: "Legally Inspiring! Everyone enjoys what is trending in society and share it with laser inspired work"
date: August 08, 2018 15:46
category: "Discussion"
author: "HalfNormal"
---
Legally Inspiring!



Everyone enjoys what is trending in society and share it with laser inspired work. Unfortunately, most is copyright material that to use and sell legally  requires licensing and permission which can be onerous. One place to look is in the public domain. Even though Wizard of OZ and Alice in Wonderland have been made into movies, the actual books and pictures contained therein can be used without the need for permission. 



Alice came to a fork in the road.  “Which road do I take?” she asked.

“Where do you want to go?” responded the Cheshire cat.

“I don’t know,” Alice answered.

“Then,” said the cat, “it doesn’t matter.”

– Alice in Wonderland







**"HalfNormal"**

---
---
**James Rivera** *August 08, 2018 23:56*

On a related note, I have found that many great classic pieces of literature are in the public domain, and if you search specifically for it on Amazon, you can download the public domain original work by <insert famous author here> for free to your Kindle account. (e.g. "On the origin of species", by Charles Darwin, Kindle Edition)

[amazon.com - Robot Check](https://www.amazon.com/origin-species-Charles-Darwin-ebook/dp/B008478VE8/ref=pd_cp_351_1?pf_rd_m=ATVPDKIKX0DER&pf_rd_p=fcaa6d12-8b2b-4ad7-b277-864b2da79f6e&pf_rd_r=46NPXFR9WJFXSEA465K1&pd_rd_wg=7lRFM&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&pd_rd_w=zOmaQ&pf_rd_i=desktop-dp-sims&pd_rd_r=70751251-9b66-11e8-89c8-8341361af5a1&pd_rd_i=B008478VE8&psc=1&refRID=46NPXFR9WJFXSEA465K1)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/TbEPB8fXUWc) &mdash; content and formatting may not be reliable*
