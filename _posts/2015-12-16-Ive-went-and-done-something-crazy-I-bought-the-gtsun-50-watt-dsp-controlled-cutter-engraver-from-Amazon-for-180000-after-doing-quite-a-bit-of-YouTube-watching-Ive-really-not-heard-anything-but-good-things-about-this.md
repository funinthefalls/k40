---
layout: post
title: "I've went and done something crazy, I bought the gtsun 50 watt dsp controlled cutter/engraver from Amazon for 1800.00, after doing quite a bit of YouTube watching I've really not heard anything but good things about this"
date: December 16, 2015 00:00
category: "Discussion"
author: "Scott Thorne"
---
I've went and done something crazy, I bought the gtsun 50 watt dsp controlled cutter/engraver from Amazon for 1800.00, after doing quite a bit of YouTube watching I've really not heard anything but good things about this machine, I've found a buyer for my engraver, 600 is what I'm selling it for....that what I've got in it.....it will be here in a week....wish me luck!





**"Scott Thorne"**

---
---
**ChiRag Chaudhari** *December 16, 2015 00:11*

Nice! Good luck man. Merry Christmas to you!!!


---
**Scott Thorne** *December 16, 2015 00:15*

**+Chirag Chaudhari**​...thanks brother....merry Christmas to you and your family as well my friend....I'll keep you guys posted....am I gonna get kicked out of the forum now....Lol


---
**Timothy “Mike” McGuire** *December 16, 2015 00:18*

 Good luck Scott .. and Merry Christmas to you!!!... And keep us posted we all love new toys


---
**Scott Thorne** *December 16, 2015 00:33*

**+Timothy McGuire**....merry Christmas to you and your family too....I'll give you guys a shout when it comes in....I'll post a video Friday off the display gearbox when the dowels come in.


---
**Stephane Buisson** *December 16, 2015 12:15*

;-))

Seem to have all mod already built in. (make the price OK)

Lucky you: large XY Table, rotary, air assist and software accepting dxf file.

check the tube before signing the delivery note.

you are more than welcome to tease us with your new toy. it's always good for comparison sake. (but unfortunatelly not improving our budget)


---
**Coherent** *December 16, 2015 13:42*

Cool.. been looking at those myself on Ebay.  Be sure to let us know how it compares. Nice self-xmas gift! Merry Christmas!


---
**Scott Thorne** *December 16, 2015 17:18*

Thanks **+Coherent**....I will...merry Christmas to you too.


---
**Scott Thorne** *December 16, 2015 17:19*

**+Stephane Buisson**....I'll keep you posted on how it performs and arrives....thanks.


---
**ChiRag Chaudhari** *December 16, 2015 17:25*

**+Scott Thorne** Hey sorry man, did not mean you to leave community by wishing you early Christmas. I just meant The upgrade laser machine is early Christmas ! But ya stick around please, would love to see what you make with the new laser cutter.


---
**Scott Thorne** *December 16, 2015 22:06*

**+Chirag Chaudhari**...Lol....I knew youth didn't....I'm still gonna keep you guys busy with all my post...Lol....and videos.....I'm curious to see what the supposed auto focus is going to be on this laser.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/XRywADQZL2U) &mdash; content and formatting may not be reliable*
