---
layout: post
title: "Engraving of a 1940s photo with my Gerbil k40 controller"
date: July 26, 2017 07:23
category: "Object produced with laser"
author: "Paul de Groot"
---
Engraving of a 1940s photo with my Gerbil k40 controller. [http://awesome.tech/redirect1/](http://awesome.tech/redirect1/)

![images/84c52afa48edd59c717d7cada70e02ca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/84c52afa48edd59c717d7cada70e02ca.jpeg)



**"Paul de Groot"**

---
---
**Don Kleinschnitz Jr.** *July 26, 2017 13:31*

**+Paul de Groot** can you post your settings along with these beautiful engraving.



I wonder, do you have comparisons of engraving with your technology vs other configurations of controller and software?



Do you think your engraving quality is superior to others and if so why?


---
**nayeem khatib** *July 27, 2017 03:56*

Beautifully done ... If you could kindly share the photo pre processing and settings 


---
**Paul de Groot** *July 27, 2017 05:55*

The settings are the strong and the weak part of the gerbil proposition. Basically the 10bits own is nicely aligned with the tube's power range. So it is basically like for like. If you would change the contrast in the picture I'm sure you would get better results. The Inkscape plugin translates the grayscale of the picture into gcode. There is no translation from black and white pictures. Colour pictures are converted into grayscale via 0.21r 0.72 b0.07 formula. The resolution is 380dpi and speed is 790mm/m although I never really timed it. I can go via the plugin up to 1400mm/m . I believe that quality also depends on the wood. My plywood is very soft and that's why you see small ridges. On harder plywood the picture is way lighter so I adjust the max black setting in grbl '$30' from 2700 to 2000. Further there is not much else to set. It's very automatic and balanced. If you set the max black to beyond black than you get really carving which is very interesting.


---
**Paul de Groot** *July 27, 2017 07:47*

**+nayeem khatib** you can find the photo on Pinterest search for 'wasp plane' the pilot is Elizabeth L. Gardner. I used the photo as is without any enhancements or modifications 


---
**Jared Roy** *July 27, 2017 19:21*

**+Paul de Groot**   The picture looks great. Can you tell me more about this Inkscape plugin that can generate gcode from grayscale images?


---
**Paul de Groot** *July 28, 2017 01:56*

**+Jared Roy** the original engraving plugin was written by 308engineering. I have cleaned up the gcode so grbl can understand it. Next to this I have extended the resolution from 250 to 381 DPI. The other extension is from jtech phototechnics. The only change I made was the removal of the M18 command at the end since grbl does not support that command. You can find the plugins at my GitHub account see [github.com - paulusjacobus/Gerbil](https://github.com/paulusjacobus/Gerbil)

Also there is an article online at [https://diyode.io/002fnkv](https://diyode.io/002fnkv) on the 1st of August 😁


---
**Scorch Works** *July 29, 2017 02:46*

Oh my! 790 mm/min... I had no idea you were going that slow.  I see now that the slow speed is how you get the jet-black in the engraving.


---
**E Caswell** *July 29, 2017 12:03*

Looks good **+Paul de Groot**.  Probably the best I have seen done on a K40. I have supported the kickstarter.  Look forward to the development.  Hope it reaches the target now. 


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/DPzQeCqpguT) &mdash; content and formatting may not be reliable*
