---
layout: post
title: "so this happened yesterday... Looks like it is a glued on piece, and glue came loose"
date: June 20, 2018 13:19
category: "Original software and hardware issues"
author: "Frederik Deryckere"
---
so this happened yesterday... Looks like it is a glued on piece, and glue came loose. Can I just re-glue this on? And if so what glue would be recommended?

Thanks!

![images/3113de75ee0cf502b4435e386e57c5b4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3113de75ee0cf502b4435e386e57c5b4.jpeg)



**"Frederik Deryckere"**

---
---
**Don Kleinschnitz Jr.** *June 20, 2018 18:38*

Amazing what the search function will find :) ;) 



[plus.google.com - What to do?? My water pump stopped working and I didn't notice for quite a wh...](https://plus.google.com/112581340387138156202/posts/Jz9gAKuSfTc)


---
**Frederik Deryckere** *June 20, 2018 19:51*

thanks! Sorry I didn't know the nomenclature to describe it for a search ;-)




---
**David Davidson** *June 21, 2018 20:27*

**+Don Kleinschnitz** Just curious - what search term did you use to find that?




---
**Don Kleinschnitz Jr.** *June 22, 2018 03:34*

**+David Davidson** I think it was "glue laser mirror". You can also often find these in in my blog by subject.


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/6jSDS8RcpRg) &mdash; content and formatting may not be reliable*
