---
layout: post
title: "Has anyone here tried lightburn with their k40 or k50?"
date: November 09, 2018 19:01
category: "Software"
author: "Nathan Thomas"
---
Has anyone here tried lightburn with their k40 or k50?



I bought it and it's very good so far, but I can't figure out how to consistently get the CD files to open when imported into LB. Has anyone come across this issue?



I do all my design in CD as I have done for yrs but instead of using rdw or corel laser...I'd like to export from CD in AI format...then import into LB. but some files open with no problems and others won't...and there are some that open, I can preview in LB but the machine just beeps and doesn't engrave. 



Any ideas? Guidance...?

Not sure if it's the fonts, AI conversion or something else entirely





**"Nathan Thomas"**

---


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/KFXYGkhZeqL) &mdash; content and formatting may not be reliable*
