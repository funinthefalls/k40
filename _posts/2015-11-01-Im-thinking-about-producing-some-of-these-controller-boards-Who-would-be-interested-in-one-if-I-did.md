---
layout: post
title: "I'm thinking about producing some of these controller boards: Who would be interested in one, if I did?"
date: November 01, 2015 23:38
category: "Discussion"
author: "Sean Cherven"
---
I'm thinking about producing some of these controller boards:



[https://github.com/chimera/laser-cutter-control](https://github.com/chimera/laser-cutter-control)



Who would be interested in one, if I did?

I need a head count to decide if it's worthwhile.



The price will be approx. $99





**"Sean Cherven"**

---
---
**adam hirsch** *November 02, 2015 00:40*

let me know if you put some together. from what i read it was a great board to convert to Mach3 and easy to setup.. thanks


---
**Sean Cherven** *November 02, 2015 00:47*

Will do! Is anyone else interested?


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/MKmvhBRgSKf) &mdash; content and formatting may not be reliable*
