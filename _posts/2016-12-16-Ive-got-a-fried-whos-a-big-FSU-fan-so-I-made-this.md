---
layout: post
title: "I've got a fried who's a big FSU fan so I made this"
date: December 16, 2016 15:40
category: "Object produced with laser"
author: "Jeff Johnson"
---
I've got a fried who's a big FSU fan so I made this.

![images/b31e8b8f7cb029e9a5aa0c5d604f9f00.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b31e8b8f7cb029e9a5aa0c5d604f9f00.jpeg)



**"Jeff Johnson"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2016 18:00*

I have no idea what FSU is, but I'm liking the piece. Interesting combination of engraving, cutting & layering. Nice work.


---
**Jeff Johnson** *December 16, 2016 18:02*

**+Yuusuf Sallahuddin** FSU is Florida State University. And thank you!




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2016 18:02*

**+Jeff Johnson** Ah cool. Makes more sense now. Living on the other side of the world these things elude me at times.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/YuaeCsKBkun) &mdash; content and formatting may not be reliable*
