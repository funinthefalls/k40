---
layout: post
title: "Word clock made with my k40, Christmas gift for my brother, based on Brent Grahams design"
date: December 08, 2017 23:08
category: "Object produced with laser"
author: "Kevin Lease"
---
Word clock made with my k40, Christmas gift for my brother, based on Brent Graham’s design

![images/1995c0bc63c91ce23151354d3ad1bdd1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1995c0bc63c91ce23151354d3ad1bdd1.jpeg)



**"Kevin Lease"**

---
---
**Don Kleinschnitz Jr.** *December 08, 2017 23:24*

Pretty cool, are the characters acrylic inserts or open cutouts?


---
**Kevin Lease** *December 08, 2017 23:28*

Letters are filled with clear epoxy which holds the internal pieces so it doesn’t need a stencil font


---
**HalfNormal** *December 09, 2017 00:18*

This is been on my to-do list since I bought my laser. Now I need to get off my bottom and just do it!


---
**LightBurn Software** *December 10, 2017 02:58*

That’s very pretty.  Love the resin idea!


---
**David Allen Frantz** *December 13, 2017 12:46*

That is total wicked! Are you gunna put the code up anywhere?


---
**Kevin Lease** *December 13, 2017 13:00*

[imgur.com - WordClock](https://m.imgur.com/gallery/dtLSy)


---
**David Cook** *December 15, 2017 19:52*

badass !!!!




---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/43C4YoqEmrw) &mdash; content and formatting may not be reliable*
