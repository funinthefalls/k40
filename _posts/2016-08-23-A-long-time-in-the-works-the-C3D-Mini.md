---
layout: post
title: "A long time in the works, the C3D Mini"
date: August 23, 2016 22:21
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
A long time in the works, the C3D Mini. The "full castle" configuration as I call it. 

The ribbon cable is directly on board, but there are various configurations of the k40 as far as power cables and endstops go, so these adapter boards help make this a direct drop in upgrade no matter what. I'm also working on sourcing the cables used so that it really is just a matter of plugging in a few cables to get switched over. No cutting, crimping, and stripping wires or having to wire up level shifters and middleman boards, it's all taken care of right here. 

It runs smoothie, we've got 4 sockets for swappable stepper drivers so you could hypothetically have your XY and a Z bed and a rotary attachment all controlled from gCode. 6 endstops for maximum expandability, a kill button on the side next to the robust full size USB Jack and microsd card, and it's got some fairly heavy duty mosfets which make this a great choice to smoothiefy a single extruder 3D Printer with heated bed. 

Working on getting these mass produced in a factory, pre-orders launching soon at the incredible price of $79 for the board. 



![images/b448bfc75698f24d8a27c2864597f780.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b448bfc75698f24d8a27c2864597f780.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Robi Akerley-McKee** *August 23, 2016 22:30*

Will there be a ethernet port add on available? $79 is a great price.  I'll be looking at getting this board in a couple of months.  The MKS Sbase I'll have will get repurposed to the 3040 cnc mill.


---
**Ray Kholodovsky (Cohesion3D)** *August 23, 2016 22:40*

I do love my 3040... (you got one with ballscrews right?)  This board will drive a CNC as well - I've been meaning to hook it up to a 24x48 OX CNC, but if you've already got something else that's certainly less fiddling to wire up than the laser...


---
**Jonathan Davis (Leo Lion)** *August 23, 2016 22:47*

Is It compatible with a Elekes Maker machine?


---
**Ariel Yahni (UniKpty)** *August 23, 2016 22:54*

**+Jonathan Davis**​ yes but you still have the issue of driving the diode


---
**Jonathan Davis (Leo Lion)** *August 23, 2016 23:01*

**+Ariel Yahni** understandable aka the laser as its commonly referred to!


---
**Robi Akerley-McKee** *August 24, 2016 02:39*

**+Ray Kholodovsky** Nope, no ball screws..  Right now I'm replacing the Yoocnc 6560 driver board.


---
**Carl Fisher** *August 24, 2016 15:23*

Have one on order with Ray. Can't wait to move away from the Nano that is currently in there and see what expanded capabilities and usability these machines can offer.


---
**Frank Hommers** *August 24, 2016 16:05*

Where can I (pre) order this?


---
**Ray Kholodovsky (Cohesion3D)** *August 24, 2016 17:34*

**+Frank Hommers** Thanks for your interest.  I have been making these by hand so far but am currently working on getting them mass produced in a factory.  I will post here when they are ready for pre-orders.


---
**Robi Akerley-McKee** *August 24, 2016 23:26*

Are we there yet?  Well I have an older laser without the flat cable... so I'm good with a MKS Sbase right now.  I want to get another laser for at home and it'll definitely be a ribbon cable type.. so I'm going to need your board.


---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2016 00:49*

**+Robi Akerley-McKee** vetting assemblers for volume production as we speak... 


---
**Rodney Huckstadt** *August 25, 2016 03:32*

look forward to seeing the end result :)


---
**Kirk Yarina** *August 31, 2016 18:39*

How's it coming?  I'd hate to miss out on the pre-order...


---
**Ray Kholodovsky (Cohesion3D)** *August 31, 2016 18:44*

Hey **+Kirk Yarina**!  I've been vetting factories.  I'm having a prospective factory run 10 samples of my larger board right now to verify their quality.  The mini is a little further out, but I'm working on it.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/U3ufB4c4AUL) &mdash; content and formatting may not be reliable*
