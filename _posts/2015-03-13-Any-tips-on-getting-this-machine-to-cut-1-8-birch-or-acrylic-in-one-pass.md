---
layout: post
title: "Any tips on getting this machine to cut 1/8 birch or acrylic in one pass?"
date: March 13, 2015 04:56
category: "Materials and settings"
author: "Seth Mott"
---
Any tips on getting this machine to cut 1/8 birch or acrylic in one pass?  Seems that after adjusting the mirrors, the best I can do to cut clean through is 2 passes with a current of 15. I am using air assist as well. 





**"Seth Mott"**

---
---
**Bill Parker** *March 13, 2015 05:40*

Have you cleaned your mirrors and lens? It does make a big difference if they are even slightly dirty.


---
**Jim Coogan** *March 13, 2015 05:40*

I have mine set at about 7-10 mA, speed 350mm, and I make 2 passes on the acrylic or baltic birch plywood.  I don't have air assist hooked up yet but this seems to keep the smoke stains down to zero.  I recently cut out a couple of acrylic washers for my air assist this way.  If I slowed it down quite a bit I could probably do it in one pass but since I needed these washers dead on I ran faster to dissipate the heat and prevent any  kind of distortion.


---
**Seth Mott** *March 17, 2015 12:49*

So I got my LightObjects air assist nozzle(I was using a 3d printed nozzle). I am using the standard 12mm lens with the bottom air assist  piece/nozzle, the factory silver middle piece, and the black air assist top mirror holder piece and I am able to slice through 1/8 birch with one pass at 5ma. 1/8 Acrylic with one pass at 10ma, but could probably go lower. I need to order a 18mm lens to complete the setup, any recommendations? ﻿


---
**Jim Coogan** *March 17, 2015 16:37*

Do you want to order the new lens because the old one is too small for the air assist unit?  If you are then I would suggest making a 1/8" acrylic ring that is 18mm OD and 12mm ID.  That is what I did and the 12mm rings fits nicely into the center.  I have not bought a new lens yet but I have been told this laser uses three types of lens.  38.1mm for engraving, 50.8mm for engraving and cutting (your stock lens by the way), and a 63.5mm for cutting. 


---
**Seth Mott** *March 17, 2015 16:38*

I am going to make a ring adapter, but I might order a new 12 or 18mm lens as mine has a few scratches on it.


---
**Jim Coogan** *March 17, 2015 17:35*

I had planned on ordering the 18mm but right now I am realigning the laser tube and all the mirrors so that needs to get done first :-)  I wasn't too happy to find cardboard shims under my laser tube and they were not there for packing.  After that I will do some more upgrades to the exhaust and internal lighting. 


---
**Seth Mott** *March 17, 2015 18:05*

That's no good, I did not have any shims under mine.  I have my tube and mirrors aligned about the best I can get them.  I went from cutting 1/8 birch in two passes @ 16ma to cutting the same stock in one pass at 5ma.  Only thing I changed was going from a 3d printed air assist nozzle with a tiny airbrush compressor to the LightObject air assist nozzle and my bigger garage air compresser(125psi max, but I regulated it down).  I also had some gunk on the top of my lens that I cleaned off.


---
**Jim Coogan** *March 17, 2015 19:06*

I thought it was interesting when I got it and thought the cardboard was just for shipping.  But it wasn't.  The laser was dead on with them but the laser was hitting the very top edge of the mirror which is not good.  So I removed the cardboard and everything went south.  Today is alignment day :-)  I have the Light Object air assist on mine but I am using an airbrush compressor.  I have 2 shop compressors but after I changed to fish tank hoses and a fish tank regulator it was more than enough.  I have all this on a portable base I built to make it as potable as possible.


---
**Seth Mott** *March 17, 2015 20:34*

I have minimalcharring with my garage air compressor, and noticeable char with my airbrush compressor at full blast.  Maybe my airbrush compressor is just weak, I am not sure what it is rated at but I wish my garage air compressor was as quiet as it!


---
*Imported from [Google+](https://plus.google.com/108197211464469447407/posts/EtiYi3oJLdP) &mdash; content and formatting may not be reliable*
