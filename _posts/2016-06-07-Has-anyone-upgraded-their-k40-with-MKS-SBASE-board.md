---
layout: post
title: "Has anyone upgraded their k40 with MKS SBASE board?"
date: June 07, 2016 00:34
category: "Discussion"
author: "Derek Schuetz"
---
Has anyone upgraded their k40 with MKS SBASE board? It's different then smoothie but uses the same firmware. But while looking at it I noticed there's a jumper for 3.3volt and 5 volt would this mean I don't need the logic converter?





**"Derek Schuetz"**

---
---
**Alex Krause** *June 07, 2016 00:40*

**+Anthony Bolgar**​ **+Ariel Yahni**​ 


---
**Ariel Yahni (UniKpty)** *June 07, 2016 00:42*

**+Derek Schuetz**​ I have a azsmz board that does not appear to need the logic converter. I have being able to run the laser without power control and in the middle of connecting power to finish.  Just at the point where laser always fires


---
**Derek Schuetz** *June 07, 2016 00:48*

**+Ariel Yahni** could you send me some picture or a quick diagram I can follow. That board is different then mine. I didn't get that board because it was a 12v so i got the other one that can do 24v


---
**Ariel Yahni (UniKpty)** *June 07, 2016 00:53*

mine is 24v also. Post a pic of your PSU and nano board


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/g7Qnctrcwxu) &mdash; content and formatting may not be reliable*
