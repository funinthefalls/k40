---
layout: post
title: "Hi, after a couple of weeks reading this community I decided to get a K40 laser"
date: February 27, 2018 22:19
category: "Discussion"
author: "Ismael M"
---
Hi, after a couple of weeks reading this community I decided to get a K40 laser.

I want to check with you guys if my machine have a problem, the laser doesn't fire if I set the pot below 3-5ma, is that the minimum current for the laser to fire? Between 3-5ma the power supply makes a hissing noise, but over that current it doesn't make any noise and I think the power is correct. I don't know if the power supply is faulty.

I'm using distiled water as coolant, I've checked the conductivity and it's 400-600k ohms, is it too low?



Thank you





**"Ismael M"**

---
---
**Ned Hill** *February 28, 2018 00:30*

That sounds typical.  Below a certain current the laser won't fully start lasing and you will hear some hiss.


---
**Don Kleinschnitz Jr.** *February 28, 2018 03:47*

The lower limit of ionozation is about 4 ma.


---
**Ismael M** *February 28, 2018 21:20*

It should be okay then, thank you for the answers 


---
*Imported from [Google+](https://plus.google.com/106377641048700127254/posts/YYJLpNYUxsf) &mdash; content and formatting may not be reliable*
