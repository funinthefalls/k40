---
layout: post
title: "Guys where are you getting your 40Wish tube from?"
date: April 10, 2017 17:14
category: "Material suppliers"
author: "Ariel Yahni (UniKpty)"
---
Guys where are you getting your 40Wish tube from? I'm not getting anywhere with my previous tube seller regarding that really weird situation where the tube does not fire.



This could be a good resource for future users so please post seller / cost / packing and overall experience 





**"Ariel Yahni (UniKpty)"**

---
---
**Abe Fouhy** *April 10, 2017 17:21*

Haven't used them but the laser depot usa has recis on sale till the 17th


---
**Stephane Buisson** *April 10, 2017 17:27*

This topic is an hard one.

inconstant origine and price on ebay.

some tube are poorly re-labeled (2 stickers on top of each other 40w over 35w).

some other have no sticker at all.



Reci brand seem to be challenge by some fake one.



take care of what you are getting.

best people to answer have laser meter reader.


---
**Anthony Bolgar** *April 10, 2017 21:37*

If you can fit an 830mm tube in your machine, Lightobject has a 35W tube for $99.00 right now. [lightobject.com - JW 35W CO2 Sealed Laser Tube (850mm)](http://www.lightobject.com/JW-35W-CO2-Sealed-Laser-Tube-850mm-P46.aspx)


---
**Paul de Groot** *April 11, 2017 04:26*

I recently bought the reci tube on ebay from the factory. Ebay seller was reci-laser. I'm amazed how much better it works than the original tube. They state that tubes ages while in storage and that they sent a newly fresh manufactured tube from the factory. Delivery was within 4 days from china to Australia.


---
**Anthony Bolgar** *April 11, 2017 04:38*

I have a 40W rated reci tube that actually measures 51W at the second mirror using a laser power meter.


---
**K** *April 12, 2017 00:35*

**+Anthony Bolgar** That's a great deal! Do you know anything about this tube and how it performs compared to the stock K40?


---
**Bob Buechler** *April 12, 2017 01:52*

**+Anthony Bolgar** do you have a specific laser power meter that you recommend? Sounds like a good thing to have.


---
**Anthony Bolgar** *April 12, 2017 03:20*

**+Kim Stroman** A true 35 W tube is about 5 W more than a K40 with a so called 40W tube. To get 40W, they use a 30W tube and run it past the reccomended max mA, which shortens tool life.  **+Bob Buechler** I use a Mahoney 0 - 200 W meter available at [bell-laser.com - Mahoney CO2 Laser Power Meter Probe 0-200 watts &#x7c; Laser Repair , Upgrades and Accessories - Bell Laser](https://www.bell-laser.com/product-page/mahoney-co2-laser-power-meter-probe-0-200-watts) for $119.00 USD. It will also let you track your tube degridation and make max power level adjustments to compensate., as well as making sure the tube vendor was honest about the tube rating.


---
**Anthony Bolgar** *April 12, 2017 03:44*

**+Kim Stroman** The Lightobject tube for $99 out preforems most standard OEM k40 tubes.


---
**Cesar Tolentino** *April 17, 2017 15:31*

**+Anthony Bolgar**​, but the 99$ tube is not recommended for k40? It says on the light object website. Anybody actually bought this tube and use it in k40? I'm looking to replace mine.


---
**Ariel Yahni (UniKpty)** *April 17, 2017 15:33*

**+Cesar Tolentino**​ I contacted them and told me it is suitable 


---
**Anthony Bolgar** *April 18, 2017 19:36*

The only thing that might be an issue is tube length, you can always make a cutout on the side of the k40 and box around it.




---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/8sdnXjJRVfL) &mdash; content and formatting may not be reliable*
