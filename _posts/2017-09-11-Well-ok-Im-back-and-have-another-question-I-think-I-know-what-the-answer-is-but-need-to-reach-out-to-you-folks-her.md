---
layout: post
title: "Well ok I'm back and have another question, I think I know what the answer is but need to reach out to you folks her"
date: September 11, 2017 00:11
category: "Discussion"
author: "Padilla's custom leather"
---
Well ok I'm back and have another question, I think I know what the answer is but need to reach out to you folks her.  I had received the k40 a few weeks back knowing that I would need to make or wanting to make some upgrades before even using it.  So yesterday I installed the blue air cutting head that came from LO, I went out today to get my computer set up with the proper serial number off of the board and after several attempts it finally took the correct number.  Now keep in mind that I have not up to this point has it been tested, I got eh water pump all set up, the air cooling nozzle, and thought it would be all set to go.

However when I went to fire the laser much to my dismay it would not fire, I went back to recheck everything checked the water pump it was running, the air compressor on, it responded to the computer and docked at the home location is the upper left corner.  BUT the laser would not fire, I traced all the wires there is a blue that goes to the potentiometer, the red wire that goes to the laser tube, the all looked good and were tight.  I do not hear so well, old Army thing so I went out with my son to listen to the power supply and also looked but no fan in the PS.  This particular PS has a little red diode indicating that there is power, there is also a test button next to the red diode, we pushed the little test button and NADA no fire, no bright flash no buzzing sound nothing.  Now let digress for just a moment before I went back out with my son I was going through all them same steps, and even tried to engrave something and everything was running the motors were moving all the hardware but just no laser.

SO here is the big question do all of you, most of you think it is a dead tube?  Now for personal reasons that I wont mention here the K40 was purchased through amazon, so if any of you have gone that route is there hope here that if it is a dead tube I might have some recourse.

Thanks in advance for all the responses I hope to get!

Ron





**"Padilla's custom leather"**

---
---
**Don Kleinschnitz Jr.** *September 11, 2017 02:15*

You have to determine if tube is bad or LPS is bad. 

1. Did you say the fan is not running on the LPS? 

2. Did this machines laser ever fire?

3. Is the red led lit on the lps.



Please post a picture showing the front of the supply with main power on




---
**Padilla's custom leather** *September 11, 2017 05:11*

I took a flashlight to illuminate the interior of LPS so that I could see if it did have a fan and I could not see one, as I noted above I sent an image to print and the LPS powered all of the motors and everything moved as if it were printing, the red little light that is the front of the LPS also lights up, and I was watching the milliamp meter as I hit the test button and it shows sending power to the meter and like it sends power to the LT but it does not fire.  So as far as I can tell the  LPS powers up and everything acts like it is working but not firing the laser tube.  If after reading this response I can tomorrow Monday get a photo of the LPS while powered up.

TIA 


---
**Don Kleinschnitz Jr.** *September 11, 2017 05:17*

You press the test button on the LPS and the miliampmeter reads a current yet the tube does not ionize???

What current does it read???


---
**Don Kleinschnitz Jr.** *September 11, 2017 05:18*

Have you checked the tube for cracks etc?


---
**Padilla's custom leather** *September 11, 2017 05:25*

no the tube does not ionize I have used different levels of current to see if it would fire from the lowest to mid range on the dial.  I did check the tube to see if the wires looked ok and they were, but I did not check the tubes for cracks I can do that again tomorrow afternoon to see if there are any.  Is there anything else I can check while there?



 


---
**Don Kleinschnitz Jr.** *September 11, 2017 14:56*

I am unclear on: 

"I was watching the milliamp meter as I hit the test button and it shows sending power to the meter and like it sends power to the LT but it does not fire..."



<b>Does this mean that when you push the test button on the supply you get a current reading? If so what is the value?</b>


---
**Padilla's custom leather** *September 12, 2017 01:36*

may be I didn't make that very clear but when I hit the test button it shows on the meter that power is being sent to the tube I have dialed it anywhere from minimum about to 10-15 of the meter but the laser does not fire, I did go back out and look and did not find any cracks in the tube at all.  I will post some photos

![images/4cd86f0d2463bcfa948a39dfc02028b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4cd86f0d2463bcfa948a39dfc02028b7.jpeg)


---
**Padilla's custom leather** *September 12, 2017 01:38*

![images/b9795e7d08149e4fc7f848e898e14f60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b9795e7d08149e4fc7f848e898e14f60.jpeg)


---
**Padilla's custom leather** *September 12, 2017 01:46*

in this photo you can see the power button on the LPS the 2nd photo up the only abnormality I saw with the laser tube is the little silver ring inside that looks a little twisted.



![images/ed60ee693921510f0c4b19c2731ddb7c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ed60ee693921510f0c4b19c2731ddb7c.jpeg)


---
**Don Kleinschnitz Jr.** *September 12, 2017 13:03*

Interesting, that supply is not one that I have seen in that it does not appear to have a fan.

BTW: The pictures are very small and hard to see detail :).



 Can you also post a picture of the control panel pls?


---
**Padilla's custom leather** *September 12, 2017 15:27*

yes I will get that a little later today and what size photo can be uploaded?


---
**Don Kleinschnitz Jr.** *September 12, 2017 16:20*

I use HR on my droid camera and it seems to work fine!


---
**Padilla's custom leather** *September 12, 2017 17:06*

![images/3f00212c34258ec0930dfc2424afdecb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f00212c34258ec0930dfc2424afdecb.jpeg)


---
**Don Kleinschnitz Jr.** *September 12, 2017 17:48*

**+Padilla's custom leather** 

Picture is WAY better :).

On that <b>current</b> meter you are saying that the needle moves when you push the red test button down on the LPS and reads between 10-15 ma?



I am being persistent on my understanding because I have never seen a laser not fire yet draw current on that meter !!! 


---
**Padilla's custom leather** *September 12, 2017 23:22*

Yes when the test button is pushed the meter will show a swing indicating that current has been sent but no fire of the tube and I know that it is reaching the tube.  I don't know what more to add I have worked on panels of different nature of the years so I am not a complete dummy to trying to follow wiring layout, beginning with the most rudimentary thoughts, is there current, is there current to the switch, does it go though the switch, does the tube fire, no ok lets do this again and follow all the wires etc, and I know the current is making it to the tube, why, because after I left the machine off for a while to dissipate all current or power I removed the little attempts at covers at the wire ends at the tube and guess what, there was a little current left because it went up my arm!!!! Bad thing but not a lot thank God.  


---
**Don Kleinschnitz Jr.** *September 13, 2017 03:21*

**+Padilla's custom leather** do not misinterpret my attempts as understanding your problem to be suggesting you are a "dummy" I am simply trying to understand an unusual situation so that I can help.



Before we proceed I need to insure that you are aware that the LPS can output lethal voltages in excess of 20,000 VDC.  



Your problem could be two things, you have a bad LPS or a bad tube. It seems that the LPS is ok since it is producing current although if it can't produce enough that could also be the culprit.



There is no real and safe way to test one or the other.



As I recall this is a new machine so I would try next to get the vendor to provide replacement parts. If you can't get help from the vendor then we can proceed with some other tests.


---
**Padilla's custom leather** *September 13, 2017 06:22*

O k then 


---
**Don Kleinschnitz Jr.** *September 13, 2017 12:02*

Btw if you decide you want to proceed without the vendors help let me know and we will keep plugging at this problem.


---
**Padilla's custom leather** *September 13, 2017 15:57*

is there really much can be done I hate to assume but I have to figure it is the tube that is DOA.  With that being said I guess it is a tube that is needed?


---
**Padilla's custom leather** *September 13, 2017 16:03*

I think I mentioned early on that this machine came from Amazon but I was the recipient and not directly the payer so I don't know how it will work out. 


---
**Padilla's custom leather** *September 16, 2017 23:12*

Ok so I found something out today that I didn't know the other day, this came about due to the Amazon seller asked if the tube was firing and I was also asked that here as well, but in the past I was doing the testing with just 2 hands when I could have used 3, so come to find out the laser is firing but looks like it goes to ground at the end of the tube and not send the light to the mirror as I am typing this on my laptop I am uploading the video we shot of this to my utube page.  it has about 3 minutes to go to complete the upload if Y'all want to take a look to hep me figure this thing out.  And it does look like I will get some support from the seller.

Thanks


---
**Don Kleinschnitz Jr.** *September 17, 2017 16:53*

OK, the tube it lighting up [ionizing] with a purpleish glow when you press fire right?



That is why when you said the tube was not firing but there was current on the meter I was puzzled :)!



<b>"laser is firing but looks like it goes to ground at the end of the tube and not send the light to the mirror"</b>



I am not clear on what "going to ground means"? Do you mean that the the light is not cumming out of the laser?



Have you done an optical alignment? 



Please be careful as the light from these lasers is invisible and can blind you. I assume you are wearing protective glasses when working with optics on the K40.




---
**Padilla's custom leather** *September 17, 2017 17:08*

no I have done an optical alignment, the light does not appear to exit the tube and looks like it goes to the metal ring in stead of going out the end of the tube, if you look up my utube page you should be able to find the video as there at only 2 that I have uploaded.  That shows what the light is doing and can probably explain it better than me trying to type it out here,




---
**Padilla's custom leather** *September 17, 2017 18:07*


{% include youtubePlayer.html id="3spL_L3msvs" %}
[youtube.com - Laser cutter](https://youtu.be/3spL_L3msvs)




---
**Don Kleinschnitz Jr.** *September 17, 2017 18:39*

**+Padilla's custom leather** that tube is operating properly.



The laser light in your K40 operates in the infrared frequency which means that it is invisible to the human eye. You cannot see it exiting the tube.

The purple light you see in the tube is the CO2 gas ionizing and it is what creates the laser light that light is NOT the laser light.



My guess is that laser light is coming out of your tube but one or more of the mirrors are misaligned keeping it from getting to the head inside the bay.



The invisible light exits your tube reflects off a mirror to the left (facing the machine) in the laser compartment and then proceeds into the main bay where it is reflected to the right on a mirror on the gantry finally it is reflected downward through a focus lens into the table through the moving head.



Seldom will you get a new K40 that does not need alignment. Here is a link to one of the best procedures. 



[k40laser.se - Mirror alignment - no more sweat and tears - K40 Laser](https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/)



<b>Warning:</b> 

The voltages in the laser compartment and the LPS that power it can KILL you.

The light from this laser can BLIND you.



Use the proper laser safety glasses when you are making adjustments on an active machine...

Stay well away from the LPS, its wiring and connections when they are on.




---
**Nate Caine** *September 18, 2017 18:09*

I watched your video.  

The tube looks like it is operating correctly.



In the video, you turned the dial up about half-way, and it showed about 10ma on the meter.  As a check, turn the dial up further, and verify that there is a similar increase on the meter.



Put a slip of paper at the output of the tube.  When you hit the "Test" button for an instant, do you get a burn spot?  What was the current reading?



Once you've verified your tube is burning paper, you need to do an alignment check and adjustment again.



<i>I didn't hear your water pump.  You can do brief tests like this, but you really should have your coolant water pump running.</i>



(<i>Some</i> laser tubes have internal mirrors that can be adjusted, but <i>yours</i> is not that type.  So if the tube was working before, and you have that pink beam, it's shows the tube is in tact.)




---
**Padilla's custom leather** *September 19, 2017 17:12*

I did all of that after that post prior to your and the laser is firing and needs alignment and yes the water pump is running and circulating water, the pump is very low in volume and hard to hear but I make sure it is running.  Thanks for the additional input!


---
*Imported from [Google+](https://plus.google.com/106971026516976154588/posts/4duSXeNc7nn) &mdash; content and formatting may not be reliable*
