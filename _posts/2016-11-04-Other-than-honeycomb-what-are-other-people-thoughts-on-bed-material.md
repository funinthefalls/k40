---
layout: post
title: "Other than honeycomb what are other people thoughts on bed material"
date: November 04, 2016 20:00
category: "Modification"
author: "Andy Shilling"
---
Other than honeycomb what are other people thoughts on bed material. I was thinking about 10mm Perspex and engraving a 300x200 grid on it with marks to every 5mm or so then flip it over and fit so I can utilize the grid as reference for templates and make sure everything is aligned etc.



Will this work??????





**"Andy Shilling"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 04, 2016 20:26*

If you are intending on cutting, probably not... as acrylic absorbs the 10600nm wavelength better than most materials (from what I've read) & you'll probably end up cutting holes in it.



I'm using a perforated galvanised steel piece for my cutting bed. Works well so far. Only real issue is sometimes get a bit of sooty sticky gunk on the back side of pieces (from buildup on the steel).


---
**Andy Shilling** *November 04, 2016 20:36*

Yes that is a concern but easily replaceable.


---
**K** *November 04, 2016 20:40*

I'm using 1.5" tall pop rivets and some 1/8" birch ply.


---
**Cesar Tolentino** *November 04, 2016 22:15*

I bought mine from goodwill. Cookie rack or anything wire mesh stainless steel cut to fit.  Because of the thickness. I have to lower my bed a few mm to compensate. I replaced the screws from stock with 10-24 thread 3 inches length. And it sits perfect like the original. But this time it's adjustable. 


---
**Bill Keeter** *November 04, 2016 23:15*

I'm installing a honeycomb bed tonight. If it's good enough for Universal and Epilog lasers... well then i'm assuming it'll be just fine for me. And it was only $15 or $20.




---
**Phillip Conroy** *November 05, 2016 08:05*

]i made my pin bed by attaching long screws to 10mm long magnets as i only cut 3mm mdf i sit these scresw/magnets in a shallow tin removable tray in the cutting area of laser bed .I have to remove tray and screws weekly and clean gunk build up under hot running water for 5min to clean.Mdf is verg dirty to cut.i only needed 12 magnets screw combos to hold up the mdf and just move them around as needed.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/AfgpL7EC1AT) &mdash; content and formatting may not be reliable*
