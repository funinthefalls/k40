---
layout: post
title: "Morning guys!! Does anyone has setup the CorelDrawX7 with the driver of the LIHUIYU STUDIO LABS pcb?"
date: August 08, 2015 15:25
category: "Software"
author: "Jose A. S"
---
Morning guys!!



Does anyone has setup the CorelDrawX7 with the driver of the LIHUIYU STUDIO LABS pcb? It is there any guide for this purpose?



Thank you!...Have a great day!





**"Jose A. S"**

---
---
**Joey Fitzpatrick** *August 08, 2015 20:41*

Install coreldrawX7 first.  Then install laserdraw.  After both are installed open laserdraw.  On the top right of the screen you will see a tool bar for the laser/engraver.  Click on the Corel settings icon in that tool bar.  Find the main board pull down menu and select the main board that is in your machine.  (The number will be silkscreened on your main board.  Most of the lihuiyu boards have a M2 board). Make sure your dongle is plugged in and your serial number is correct. Once this is done, you can cut and engrave directly from coreldraw. I use X7 and it works great.﻿


---
**Jose A. S** *August 09, 2015 01:17*

**+Joey Fitzpatrick** thank you!!. Then, corel draw will recognize the driver by itself?


---
**Joey Fitzpatrick** *August 09, 2015 22:56*

You do not need to install any other drivers.  Windows should automatically install USB drivers as soon as you plug the machine into your PC.  Once you have the Coreldraw and Laserdraw software installed, The K40 should work normally.


---
**Maartje Kroesbergen** *September 15, 2015 20:40*

USB is not recognized by my laptops either, have windows 7, 8 and 10....  Could you help?


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/TF73sSwzpcu) &mdash; content and formatting may not be reliable*
