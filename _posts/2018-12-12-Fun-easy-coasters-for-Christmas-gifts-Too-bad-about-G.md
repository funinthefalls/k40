---
layout: post
title: "Fun, easy coasters for Christmas gifts. Too bad about G+;"
date: December 12, 2018 01:30
category: "Object produced with laser"
author: "Mike Meyer"
---
Fun, easy coasters for Christmas gifts.



Too bad about G+; I'm going to really miss this forum.



![images/c68dc93081c859b2b17e898557dd2c92.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c68dc93081c859b2b17e898557dd2c92.jpeg)
![images/e39d06231215d433b827ad5b36ab18f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e39d06231215d433b827ad5b36ab18f8.jpeg)
![images/412c5ccbe1e8716ec90db53e69e48a40.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/412c5ccbe1e8716ec90db53e69e48a40.jpeg)
![images/d5a5251a03c7be7a2c175b25251d404e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d5a5251a03c7be7a2c175b25251d404e.jpeg)

**"Mike Meyer"**

---
---
**Ned Hill** *December 12, 2018 01:41*

Nice work!




---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/ZZrWMekKGCv) &mdash; content and formatting may not be reliable*
