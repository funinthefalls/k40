---
layout: post
title: "This is a little vid of the tube when it has the problem"
date: November 10, 2016 18:13
category: "Original software and hardware issues"
author: "Andy Parker"
---
This is a little vid of the tube when it has the problem.  Any ideas?

![images/d42b98d03201e38f684bb3c7a6e55e44.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d42b98d03201e38f684bb3c7a6e55e44.jpeg)



**"Andy Parker"**

---
---
**yosyp mindak** *November 11, 2016 21:25*

lift up the opposite side of the machine.

running out of air.

and let him return.


---
**stargeezer L** *November 13, 2016 07:22*

Please pardon a side question, but in your video I see water in the outer glass tube - in mine the water seems to be inside the inner glass tube and air in the outer tube - do I have a problem. I just got my laser and have only tested power-on and movements. I'm also totally green on lasers.



Thanks, Larry


---
**Andy Parker** *November 13, 2016 17:22*

the water should not be in the same part of the tube as the beam. It may look like the water fills the outer part in my vid but it is isolated from both the outer and the beam. if it is in with the beam i expect you have a cracked / leaky tube.


---
*Imported from [Google+](https://plus.google.com/100121420236877030725/posts/gysqjN5uUv8) &mdash; content and formatting may not be reliable*
