---
layout: post
title: "Hello all, Im new in K40 laser and have maybe stupid problem, but I dont find any solution - how is possible to left laser continuously cut material in LaserDRW3 to prevent stop between every repeate of cutting movement?"
date: November 13, 2016 16:58
category: "Discussion"
author: "MobileEffect"
---
Hello all, Im new in K40 laser and have maybe stupid problem, but I dont find any solution - how is possible to left laser continuously cut material in LaserDRW3 to prevent stop between every repeate of cutting movement? I set for example 15x repeats for cuting wood and needs to press enter between every single movement. Thanks all.





**"MobileEffect"**

---
---
**Kelly S** *November 13, 2016 17:10*

I think you have to add task and repeat, vs doing multiple runs.


---
**Cesar Tolentino** *November 13, 2016 17:12*

How about copying the drawing several time on top of each other. Not sure if this is possible with laserdrw3... But in CAD, if I want 3 passes, i will copy them three times, so there will be 3 lines on top of each other... then i send it to cut.


---
**MobileEffect** *November 13, 2016 18:05*

**+Kelly S** I try it already and still stoping between tasks


---
**MobileEffect** *November 13, 2016 18:06*

**+Cesar Tolentino** Interesting, Ill try this way.


---
**greg greene** *November 13, 2016 19:09*

What wood are you cutting that needs that many reps?


---
**MobileEffect** *November 14, 2016 15:16*

**+greg greene** 10mm plywood


---
**MobileEffect** *November 14, 2016 15:19*

**+greg greene** is it even possible to cut such a thick material with K40? (I dont have idea and experience with laser) maybe is just too much for laser.


---
**Cesar Tolentino** *November 14, 2016 15:28*

I think this is too much for k40 running max of 10ma.  Anything more than 10ma is not good for the tube.  Even if you flipped the material.  That's why I'm building a cnc machine for thicker materials. 



Anything more than 6mm, there is big chance of your material being charred instead of being cut. 


---
*Imported from [Google+](https://plus.google.com/109578446792362388576/posts/hFysfgjKan2) &mdash; content and formatting may not be reliable*
