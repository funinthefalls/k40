---
layout: post
title: "Aligning this square was a fun task..."
date: May 19, 2017 06:32
category: "Object produced with laser"
author: "Alex Krause"
---
Aligning this square was a fun task... I will post a video of how I did it when I wake up in the morning... The cross was cut on my router first 



<b>Originally shared by Alex Krause</b>



cranking out a last minute graduation present request for a co-worker, his daughter's high school graduation reception is this weekend 

![images/0b2376606d29005a49f25f547b527e2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0b2376606d29005a49f25f547b527e2b.jpeg)



**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2017 09:00*

Looks great. Not sure if that is how you spell steadfastly in America, just over here in Aus we have an A in it too...


---
**Alex Krause** *May 19, 2017 11:28*

It's how it's spelled in one of the versions of the bible


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2017 12:13*

**+Alex Krause** Well that's all good then :) Overall a very nice piece. It's interesting how the laser affects on different areas of the wood.


---
**Richard Vowles** *May 19, 2017 18:15*

Is it a vampire fighting school?


---
**Mark Brown** *May 20, 2017 12:15*

Walnut, huh?  The engrave turned out better than I'd have expected on something so dark. 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/4WVZM1ifVRS) &mdash; content and formatting may not be reliable*
