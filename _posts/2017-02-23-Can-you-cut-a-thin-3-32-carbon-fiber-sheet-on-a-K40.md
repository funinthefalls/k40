---
layout: post
title: "Can you cut a thin 3/32\" carbon fiber sheet on a K40?"
date: February 23, 2017 18:50
category: "Discussion"
author: "Bill Keeter"
---
Can you cut a thin 3/32" carbon fiber sheet on a K40? Maybe with multiple passes? Have a possible project from a friend if it is.





**"Bill Keeter"**

---
---
**Phillip Conroy** *February 23, 2017 19:29*

Not safely if coated,just the mat yes 

[atxhackerspace.org - Laser Cutter Materials - ATXHackerspace](http://atxhackerspace.org/wiki/Laser_Cutter_Materials)


---
**Eric Flynn** *February 23, 2017 19:59*

No , not composite sheet.  Just raw resin free cloth.




---
**Bill Keeter** *February 23, 2017 20:11*

yeah, it's a finished sheet he wants cut. I think the issue is the parts he needs cut are too small for their CNC router. Oh well. 



Maybe I can talk them into using Delrin.


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/QYwGPb2ASJh) &mdash; content and formatting may not be reliable*
