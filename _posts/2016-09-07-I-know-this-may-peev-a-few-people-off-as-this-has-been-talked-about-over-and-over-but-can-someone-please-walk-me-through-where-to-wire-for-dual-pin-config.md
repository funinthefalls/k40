---
layout: post
title: "I know this may peev a few people off as this has been talked about over and over but can someone please walk me through where to wire for dual pin config?"
date: September 07, 2016 13:13
category: "Smoothieboard Modification"
author: "Jez M-L"
---
I know this may peev a few people off as this has been talked about over and over but can someone please walk me through where to wire for dual pin config?  I understand that the PWM goes to IN on the PSU and then the second ttl switch goes to L but which pin do I connect L to on the smoothieboard?  I want to get rid of the pot control and just have the smoothie control everything.





**"Jez M-L"**

---
---
**Jez M-L** *September 07, 2016 13:25*

Am I right in saying that all i need to do is solder a header to, say, pin P2.7 (currently have a header soldered into P2.5 for PWM) and then add the following to my config:



# Switch module for laser TTL control

[switch.la - is coming soon](http://switch.la)ser.enable                            true

switch.laser.input_on_command                  M3

switch.laser.input_off_command                 M5

switch.laser.output_pin                        2.7


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 07, 2016 20:11*

I can't say precisely for dual pin config. On mine I just soldered a bunch of headers to the pins 2.6,2.5,2.4,etc (the bunch of 5 there). I am using single pin & pin 2.4! (inverted).


---
**Steve Wilks** *September 08, 2016 05:53*

The latest firmware supports a ttl pin configuration. Here's an extract from my config file....

## Laser module configuration

laser_module_enable                          true             # Whether to activate the laser module at all. All configuration is



                                                              # ignored if false.



laser_module_pwm_pin                         2.2              # this pin will be PWMed to control the laser. Only P2.0 - P2.5, P1.18, P1.20, P1.21, P1.23, P1.24, P1.26, P3.25, P3.26



                                                              # can be used since laser requires hardware PWM

laser_module_ttl_pin                         0.26^

laser_module_maximum_power                   0.8                # this is the maximum duty cycle that will be applied to the laser

laser_module_minimum_power                   0.0              # This is a value just below the minimum duty cycle that keeps the laser

                                                              # active without actually burning.

laser_module_default_power                   0.5              # This is the default laser power that will be used for cuts if a power has not been specified.  The value is a scale between

                                                              # the maximum and minimum power levels specified above

laser_module_pwm_period                      20               # this sets the pwm frequency as the period in microseconds


---
**Jez M-L** *September 08, 2016 08:37*

Had a play last night and I now have PWM into IN (not inverted), no pot and original laser activation switch that enables the laser. Think I will run with this setup for a while and see what happens. 


---
**Jez M-L** *September 12, 2016 13:56*

**+Steve Wilks** I have this now but what are you wiring pin 0.26 to on the psu?


---
**Steve Wilks** *September 16, 2016 01:34*

Sorry for the late reply...I'm pretty sure my PSU is different as I have the K40 with the digital power panel. I'll confirm my wiring when I get home.


---
*Imported from [Google+](https://plus.google.com/103448790120483541642/posts/cemt6w6JoLP) &mdash; content and formatting may not be reliable*
