---
layout: post
title: "I have bit of bad news, my laser has some issue for sure"
date: December 19, 2015 04:27
category: "Discussion"
author: "ChiRag Chaudhari"
---
I have bit of bad news, my laser has some issue for sure. Like the video I posted it loses power in 3-5 seconds. I plug in the Pot back to the PSU and try to control Amps with that, same results. the mA jumps from ~3.5mA to like 9-10mA just by turning the pot tiny bit. And if I keep the test fire button depressed for 3-5 seconds the power drops from ~9-10mA to ~3.5mA. So definitely something wrong with either PSU or Tube. I am leaning more towards the tube, as many people have experienced same issue after using the Laser cutter for 1-2 years, depending upon their use. They did not fill enough CO2 I think :P





[https://plus.google.com/u/0/+ChiragChaudhari/posts/8KfcGXiNC19](https://plus.google.com/u/0/+ChiragChaudhari/posts/8KfcGXiNC19)





**"ChiRag Chaudhari"**

---


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/ih3WRnih2aE) &mdash; content and formatting may not be reliable*
