---
layout: post
title: "Unbelievable site for plans! Get ready to have your mind blown!"
date: May 15, 2018 04:01
category: "Repository and designs"
author: "HalfNormal"
---
Unbelievable site for plans! Get ready to have your  mind blown!



[http://3axis.co/](http://3axis.co/)





**"HalfNormal"**

---
---
**Anthony Bolgar** *May 15, 2018 04:27*

Nice find.




---
**HalfNormal** *May 15, 2018 04:41*

Inkscape will open the CDR files.


---
**Paul de Groot** *May 15, 2018 06:39*

Will definitely use it for some demo stuff. Great find


---
**Don Kleinschnitz Jr.** *May 15, 2018 11:27*

Just Wow!

#K40ART


---
**Ned Hill** *May 15, 2018 13:26*

That's awesome!  Thanks for sharing the link. :)


---
**Stephane Buisson** *May 15, 2018 18:59*

moved to repository &designs




---
**Nitro Zeus** *May 16, 2018 16:00*

Thank you, that's great. Im going to try to back them all up in case something happens to this site.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/3t1tRTcWgvo) &mdash; content and formatting may not be reliable*
