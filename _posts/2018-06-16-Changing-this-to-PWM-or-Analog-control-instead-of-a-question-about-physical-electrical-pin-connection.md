---
layout: post
title: "Changing this to PWM or Analog control instead of a question about physical electrical/pin connection"
date: June 16, 2018 04:43
category: "Modification"
author: "Doug LaRue"
---
Changing this to PWM or Analog control instead of a question about physical electrical/pin connection.



I've learned that the current preferred way to do laser power level control is via the L control line of the LPS(laser power supply) instead of the analog IN control which the front panel uses for manual control.  It seems to work since people are using it but it seems too dependent on your project DPI setting <i>and</i> your head speed setting. See Don's great write-up on this;



[https://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html](https://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html)



What I'm wondering is why not just use a 12-bit DAC or even a 256 position digital POT on the IN control line and have no concern for DPI nor speed?



I've not seen these options mentioned and they don't seem to be an expensive addon with a 12-bit DAC board from [Adafruit.com](http://Adafruit.com) listed at only  $5. [https://learn.adafruit.com/mcp4725-12-bit-dac-tutorial/overview](https://learn.adafruit.com/mcp4725-12-bit-dac-tutorial/overview)







**"Doug LaRue"**

---


---
*Imported from [Google+](https://plus.google.com/103281768421101175277/posts/NH6WZJA6QT3) &mdash; content and formatting may not be reliable*
