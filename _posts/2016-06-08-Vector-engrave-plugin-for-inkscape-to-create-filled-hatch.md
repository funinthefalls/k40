---
layout: post
title: "Vector engrave: plugin for inkscape to create filled hatch"
date: June 08, 2016 12:23
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Vector engrave: plugin for inkscape to create filled hatch. [http://wiki.evilmadscientist.com/Hatch_fill](http://wiki.evilmadscientist.com/Hatch_fill)

If you have an alternate solution please post it.





**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 08, 2016 13:29*

I don't use Inkscape, but I'm curious does this plugin allow you to adjust the spacing between the lines?


---
**Alex Krause** *June 08, 2016 13:38*

Vectric software has this feature and it works pretty well you can adjust your hatch... down side is its not open source


---
**Ariel Yahni (UniKpty)** *June 08, 2016 13:39*

**+Yuusuf Sallahuddin**  Yes


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/hFkx4TrZGUd) &mdash; content and formatting may not be reliable*
