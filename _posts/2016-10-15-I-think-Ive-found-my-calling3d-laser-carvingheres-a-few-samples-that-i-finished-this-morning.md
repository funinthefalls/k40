---
layout: post
title: "I think I've found my calling...3d laser carving...here's a few samples that i finished this morning"
date: October 15, 2016 18:21
category: "Object produced with laser"
author: "Scott Thorne"
---
I think I've found my calling...3d laser carving...here's a few samples that i finished this morning. 



![images/bd0ae66451493ec581dd5c1785d43b2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bd0ae66451493ec581dd5c1785d43b2b.jpeg)
![images/100cc6baf0e011f87ec50837fbcc7ddc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/100cc6baf0e011f87ec50837fbcc7ddc.jpeg)
![images/fcd03bd79d873785e51f75551156754d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fcd03bd79d873785e51f75551156754d.jpeg)

**"Scott Thorne"**

---
---
**Ric Miller** *October 15, 2016 18:25*

Amazing. Laserweb?


---
**Scott Thorne** *October 15, 2016 18:32*

No....rd works...ruida dsp controller.


---
**Ariel Yahni (UniKpty)** *October 15, 2016 18:35*

How many passes? Speed and power? I've had some results with the celtic nut, but yours is lighter 


---
**Anthony Bolgar** *October 15, 2016 18:37*

Nice work. Really turned out nice.


---
**Scott Thorne** *October 15, 2016 18:41*

Thanks guys....3 passes...1st pass...min power sat at 5...max power at 55%...speed at 500mm/s...2nd pass min power 7 max at 65%...speed 500mm/s....3rd pass min power 0 max at 30%..speed at 500mm/s.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 18:43*

Wow bro, that's absolutely awesome. Great work.


---
**Scott Thorne** *October 15, 2016 18:58*

I've missed you guys....I've been off the grid for a bit but im back now...lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 19:11*

**+Scott Thorne** I thought I hadn't seen you for a while. Great to see you back & with amazing work to boot.


---
**Jim Hatch** *October 15, 2016 19:11*

Awesome work!


---
**Ariel Yahni (UniKpty)** *October 15, 2016 19:15*

Your are back with an earthquake, raised the bar for all


---
**Scott Thorne** *October 15, 2016 19:44*

 Lol...thanks **+Ariel Yahni**


---
**Scott Thorne** *October 15, 2016 19:44*

Thanks Yuusef.....great to be back. 


---
**greg greene** *October 15, 2016 20:06*

what machine are you using to do that?


---
**Scott Thorne** *October 15, 2016 20:34*

50 watt ruida dsp machine.


---
**Anthony Bolgar** *October 15, 2016 20:47*

What is the deepest distance in the engraving?


---
**Alex Krause** *October 15, 2016 21:05*

**+Peter van der Walt**​


---
**Alex Krause** *October 15, 2016 21:08*

Noice!!!! **+Scott Thorne**​... I'll ask the question no one else has :P what type of wood?


---
**Alex Krause** *October 15, 2016 21:08*

And what kind of post process cleanup did you use?


---
**Todd Miller** *October 15, 2016 23:03*

Awesome !




---
**Tony Schelts** *October 15, 2016 23:26*

Scott I have a x700 clone running RD works 50w.  Can you do a tutorial and explain what the procedure was please.  Thanks 




---
**Scott Thorne** *October 15, 2016 23:47*

Tony these are all samples that i downloaded. In rd works output direct has to be selected to use the grayscale as power levels, using minimum and maximum values for depth....go to thingiverse and search 3d grayscale image.


---
**Scott Thorne** *October 15, 2016 23:48*

**+Alex Krause**....just dish soap and hot water and a soft scrub brush


---
**Scott Thorne** *October 15, 2016 23:49*

**+Anthony Bolgar**....it's probably 3/16 of an inch....it could have been deeper if i went with 4 passes.


---
**Scott Thorne** *October 15, 2016 23:51*

**+Alex Krause**...this is alderwood cooking planks i ordered off of amazon....5.5 x 14 inches 6 pieces for 16.00 bucks...not bad.


---
**Scott Thorne** *October 15, 2016 23:54*

I've just ordered some 12 x 12 maple sheets 1/2 inch thick...12 bucks a sheet but I've read that maple is best for the 3d carve...I'll repost some pics when it arrives. 


---
**Scott Marshall** *October 16, 2016 05:22*

Wow, you're going to have a lot of router guys looking at lasers.

Nice work, and I think you're qualified to do a tutorial on the art if you feel like doing it. I know I would learn a lot. 



I'd also like to know your impressions on RD Works and some of the details on that.



Thanks, Scott


---
**Dennis Fuente** *October 16, 2016 17:51*

Hi Scott that is nice work, RD works software dose it generate g-code or is it a direct connection software can it be run on a smoothie board K40



Thanks Dennis 


---
**David Cook** *October 16, 2016 18:07*

Awesome results wow


---
**Scott Thorne** *October 16, 2016 18:52*

 **+Dennis Fuente**...it is proprietary to the dsp controller...only works with ruida controller. 


---
**Alex Krause** *October 16, 2016 19:04*

For those who are curious this is the type of image **+Scott Thorne**​ is working with

![images/41948e9a2ce83fe06e3b85402f6fd818.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41948e9a2ce83fe06e3b85402f6fd818.jpeg)


---
**Alex Krause** *October 16, 2016 19:06*

Besides looking for just 3d grayscale you can also search for depth map when looking for images to test on


---
**Alex Krause** *October 16, 2016 19:08*

I did one a few months back but lack the detail of yours scott

![images/e9c6a2db8d07816f9b598ee8e89b6297.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9c6a2db8d07816f9b598ee8e89b6297.jpeg)


---
**Scott Thorne** *October 16, 2016 19:45*

**+Alex Krause**...not bad.....try the grayscale.


---
**Todd Miller** *October 16, 2016 19:52*

Do you move the Z table/focal point, or is it fixed ?


---
**Scott Thorne** *October 16, 2016 20:02*

,**+Todd Miller**....if your were asking me...no i kept ît at the same height 


---
**Dennis Fuente** *October 17, 2016 16:23*

Scott 

How long did the machine have to run to make that design 



Thanks  Dennis 


---
**Scott Thorne** *October 17, 2016 17:12*

About 30 minutes...i lost quite a lot of detail because sized the original down to 4.5 inches...but it's the only hardwood i had. 


---
**Dennis Fuente** *October 17, 2016 18:55*

Still looks fantasic great work



Dennis


---
**Scott Thorne** *October 17, 2016 19:58*

**+Dennis Fuente**....thanks bro. 


---
**Coherent** *October 18, 2016 18:23*

Really nice .. and glad to see you're back!


---
**Scott Thorne** *October 18, 2016 19:49*

**+Coherent**...thanks man.


---
**Tony Schelts** *October 19, 2016 08:40*

scott what were your settings.  I have an x700 clone which i think is 50w.  what speed please




---
**Scott Thorne** *October 19, 2016 09:27*

300mm/s at 5%minimum  power 45% maximum power for 1st pass, the next 2 passes i increased the maximum power level by 10% each pass


---
**Tony Schelts** *October 19, 2016 10:01*

 thanks Scott


---
**Scott Thorne** *October 19, 2016 11:29*

**+Tony Schelts**...anytime man. 


---
**Scott Thorne** *October 19, 2016 14:31*

**+Tony Schelts**...something to keep in mind....don't run the 3d carves from rdworks...

Download them to your controller...they run flawless that way....I've had troubles running them straight from rdworks...x and y axis start doing crazy things.


---
**Mae Stratta** *October 20, 2016 05:52*

Fantastic Work, Awesome!


---
**Scott Thorne** *October 20, 2016 09:40*

**+Marilia Stratta**...thx


---
**Bob Damato** *October 20, 2016 14:18*

Every time I see some amazing work like this, it makes me realize that with my stock k40, I can probably never do something like this... Excellent work Scott!


---
**Scott Thorne** *October 20, 2016 14:28*

 **+Bob Damato**...thanks brother. 


---
**Tony Schelts** *October 21, 2016 08:00*

Scott I just want to give you a big thumbs up for all your help and inspitration.  Keep them coming thanks


---
**Scott Thorne** *October 21, 2016 09:17*

**+Tony Schelts**...thanks man. 


---
**your patio** *September 28, 2017 18:10*

I want to do that. I have a K40 with a Cohesion MiniBoard, can I?


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/WERBdFk4c5u) &mdash; content and formatting may not be reliable*
