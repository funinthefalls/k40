---
layout: post
title: "So, I bought a 1/6hp airbrush compressor capable of 40l/min (it says) to do air assist"
date: May 12, 2015 18:22
category: "Modification"
author: "Chris M"
---
So, I bought a 1/6hp airbrush compressor capable of 40l/min (it says) to do air assist. The improvement in cutting is quite amazing - 3mm MDF was 10mm/s @ 65% power, with air I can do 15mm/sec @ 40%.



There's a downside. After an hour I burnt my fingers on the compressor. I guess it wouldn't last long if I wanted to run for 6

hours a day.



What does anyone else use/suggest that might last better?





**"Chris M"**

---
---
**Sean Cherven** *May 12, 2015 18:27*

You could probably cool the compressor with a heatsink and/or fan


---
**Jon Bruno** *May 12, 2015 18:31*

It's probably.... Let me rephrase that... It IS overkill but I run a little GAST rotary vane compressor.

Other than being a little noisy it pushes a nice steady column of air.﻿

It'll also pull down to 20"Hg but that's not what I'm using it for. (vac forming)﻿


---
**Jon Bruno** *May 12, 2015 18:35*

For an airbrush compressor I used to use a modified mini fridge compressor.

﻿perhaps that's something you may want to investigate.


---
**Chris M** *May 12, 2015 18:58*

Sean, I'm not really interested in cooling the compressor I have. It would take more power, make more noise, and still supply the same airflow. I can use it in other less continuous applications. What I'm after is something that can be run continuously and is quiet. Would a koi pond air pump work?


---
**Jon Bruno** *May 12, 2015 19:00*

No I was referring to the mini fridge compressor in a modified form as an air compressor not to cool your existing compressor. Lol﻿

But I suppose it would get hot as well.. Just not as loud being a major plus




---
**Chris M** *May 12, 2015 19:56*

Jon, I was referring to the idea of putting more power and money at my existing compressor which is evidently not suited for the job. It needs to be modulated, and for now I'd rather not put that effort in.


---
**Jim Coogan** *May 12, 2015 20:33*

I have a 1/5 hp airbrush compressor and it really does get hot.  But it is still going strong.  I did rig up a computer fan, which draws very little current, to draw the heat away from it.  It still gets hot but I have run it all day and have no issues, yet.  I use it for both my laser and wood finishing on small projects so it pulls double and triple duty.  I do have several other compressors I use with my nail guns and one is pretty large.  I have seen people hook those up to their machines but I thought that was overkill for me.  I added a fish tank valve in the line to restrict the air flow a tiny bit and the result is more pressure out of the tip.  I have the air assist tip but I and starting to think about moving the air hose right next to the tip on the outside with a smaller diameter hose or a small aluminum tube.  That works well on scrollsaws and I see not reason it should not work on a laser.


---
**Chris M** *May 12, 2015 20:42*

Jim, maybe I'll carry on with the compressor I have. See if it stands up to a few hours of use before I go for something else..


---
**David Richards (djrm)** *May 12, 2015 21:01*

I bought a 140 l/min koi pump, it does the job well but is quite noisy. I should run it in a sound reducing box.


---
**Jim Coogan** *May 12, 2015 21:02*

That is pretty much what I am doing.  I had the compressor and it was not being used a lot so I figured I would give it a shot.  It's pretty cheap and if it dies I have other compressors to fill in.  I do flip it off between setups so that does help a little. 


---
**David Richards (djrm)** *May 12, 2015 21:11*

I'm using a solid state relay to turn on the air pump when the laser is energised, that helps.﻿


---
**Jonathan Lussier** *May 13, 2015 16:04*

Why not use a "real" compressor?  Airbrush compressors are not designed for continuous use.


---
**Jon Bruno** *May 18, 2015 13:08*

I guess then the three major issues we face are, Volume, Noise, and Duty Cycle. I think it's the same situation as the Iron triangle. Cost, Quality, Access. Pick two of your choice, and the remaining will not be in your favor.


---
**Jose A. S** *May 30, 2015 08:11*

Guys, could you post the links of the compressors?...thank you!


---
*Imported from [Google+](https://plus.google.com/108727039520029477019/posts/2YT3HcsaQrV) &mdash; content and formatting may not be reliable*
