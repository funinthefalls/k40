---
layout: post
title: "Here is an update on the LPS control research I have been doing"
date: October 21, 2016 13:55
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
Here is an update on the LPS control research I have been doing.



We are finding that there are at least two types of supplies and they are NOT internally the same. The way that they are controlled is DIFFERENT and this exacerbates our attempts to get a consistent design approach for controlling the laser supply.



---------

We now have schematics for two instances of supplies that we have full or partial . One schematic created by **+Paul de Groot** and one that I did using the supply **+Kim Stroman** sent me. 

BTW: thanks again to these two for providing the foundation of what follows in this post.



Surpirse ...NOT!

These two supplies (green only screw connectors) look the same on the outside but are different layouts and circuits on the inside. The way they control the supply is also different internally.



I also already know that my supply (green and white connectors) is internally different than either of the above.



To fully resolve how to properly control these supplies in all configurations I am going to have to get one dead one (I am assuming no-one wants to give up a good one) of each type. 



Therefore I still need one with the green-white connectors. 

Note: I have some K40 stock stuff like a M2Nano and dongle if that is an attractive trade.



-------

Preliminary conclusions from my circuit tracing research:



The pot is necessary to get full power as the current control connects (directly in some and through an  opto in others),  to the power supplies PWM. Leaving the pot out floats the input to the supplies PWM and could be susceptible to noise. If you do not want a pot I would pull it to 5V (use the supplies 5vdc) rather than leaving it open.



Based on my analysis of the actual internal circuits I still believe that the best PWM control is on the "L" signal. 



The control of "L" MUST be an open drain with no pullups etc. It needs to be a low true signal or said more simply, it must provide a "gnd". L is connected to the cathode of a LED which controls enabling the supplies internal PWM. IN most cases it is the same or directly connected to the "Test" terminal. The open drain must have the same  grnd as that of the of the supply.

 

Level shifters may work but aren't the best interface method because:



A. the L signal simply needs to be pulled to ground it does not need nor want a pull up nor any voltage on that pin. If the L pin is in any other state (voltage) the firing may be unpredictable. We want the LED on the internal optocoupler to be full on to fire.



B. Level shifting on the IN with the pot installed will work but I expect it to give complex power control results as the pot's position is summed with PWM values issued from the smoothie. The IN pin wants a voltage from 0-5vdc and uses it as an analog signal to set the internal PWM of the supply. You can connect a digital 0-5V PWM to the IN without the pot but it essentially does the same thing that the L signal does with the disadvantage that you loose the pots power control. Keep in mind that the M2Nano's only control connection to the LPS is the "L" signal.



The good news is that from what I am seeing the control of this supply is pretty simple and I think we will find a common denominator of these configurations.

-----

I know that some may have tried to control "L" without a level shifter and it did NOT work, others have not gotten L control to work at all. Even others found interference with the TEST switch when L is used. 

I do not yet know why, but there are multiple reasons other than the use of a level shifter that are at play, like the polarity of the L signal and its association with gnd that could cause it not to work.



What am I recommending:



-If what you have works then I would leave it alone until I get a full set of rules defined and some actual testing using those rules.

-If you are just setting your smoothie control up, try the "L" control without shifter and if it doesn't work connect with me before abandoning it.

-I think we should try some kind of survey of the community to see if we can get a handle on the LPS and configuration that are running out there

-Help me get a green-white connector supply to trace :)



I will put the technical details of what I have found on my blog sometime from  today to Monday. 





**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *October 21, 2016 14:06*

Awesome work. Have you posted your need of a PSU in FB group?


---
**Don Kleinschnitz Jr.** *October 21, 2016 14:07*

**+Ariel Yahni** No .... excellent idea though ...


---
**David Richards (djrm)** *October 21, 2016 14:40*

Useful research Don. I see some of the older LPS had an external ballast resistor, I wonder if the later ones have another arrangement for limiting the current or if the ballast is somehow built into the EHT rectifier. I'm eagerley awaiting too see the  schematics of the input circuits. Kind regards, David.


---
**Nick Williams** *October 21, 2016 19:26*

From my experience all of these lasers have 6 basic inputs (TL or L, TH or H, GP or P, IN, Grnd, 5v). Quite often there are additional signals or there are easy connects as for the pot connection that has (g,5v, in) next to each other with another G and 5V along the connection strip but functionally it is a mater of tracing these three signals and I have not seen a LPS that does not have them. The labeling may be a little different but I have seen and worked with about a half dozen LPS and they all have these signals. 



The laser power supply is designed to have two firing mechanisms one being wired to the control system and the other being wired to a test fire button. These two controls are either (L or TL) which is an active low, works when tied to ground. The outer is active high (H or TH) works when connect to a +5V. The idea is that different control system may use an active low or active high to fire the laser so depending on your control system you should use either T or L. 



(GP or just P) stands for ground protection if this is not connected to ground then the laser will not fire. The GP is usually wired to the door and water sensor in serial manor then to ground. 



IN is the input signal and will accept a signal between 0-5 volts. You can control this without a control system by adding a pot to (grnd, +5, and IN) or you can connect a PWM coming from your controller directly to this IN. Keep in mind that your controller must produce a 0-5 volt PWM signal if instead like the smoothie the signal is 0-3.3 V then you must condition it to get full power. Also keep in mind that the Controller must be connected to the ground of the LPS.



Test Fire Button. As stated above the controller will either use (H or L) signal so the Test Fire will use the other. It is very easy to implement a Test Fire and only requires a that there be a voltage on the IN line > 0. So if the controller is using the L signal then wire a contact switch from the LPS +5V to the H or TH signal. On the other hand if the controller is using the H signal then wire you contact switch from L to grnd.



1) TL or L=> Active Low Fire Enable 

2) TL or L=> Active High Fire Enable

3) G or GP => Ground protection circuit must be wired to grnd or laser will not fire.

4) IN => 0-5 V signal used to control laser power this can be generated by a PWM.

5) +5 volt=> Out put generated by LPS

6) Grnd=> Must be wired to the controller   



I really like what your doing Don hope this helps.

I also have a dead 40 watt LPS it is not from a K40 it is from a FULLSPECTRUM but I would not be surprised if this was also used in some of the K40 lasers.

PM me I will gladly donate it.

![images/fdbc53cfef6532843b83d5ea3ff9990a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fdbc53cfef6532843b83d5ea3ff9990a.jpeg)


---
**Don Kleinschnitz Jr.** *October 21, 2016 20:19*

**+Nick Williams** Looks like we are in sync on the interface. I do however see supplies with another label which is -K and +K (these have green and white connectors. The "K"s however as you outlined are "Test" inputs. I will be interested in you critique on my technical version of these findings. 

I surely would like to get that dead supply ..... for the life of me I cannot figure out how to PM you :(.


---
**Nick Williams** *October 21, 2016 20:52*

**+Don Kleinschnitz** Sending a PM is just like writing a post. You must be following the person you want to PM. Click on the pencil icon in lower corner of google plus.

This will bring up a dialog on top it will probably show the K40 group. To the right of this is button that allows u to redirect the message. Now select the person or person u which to send your PM to. Write your message hit Post :-)  


---
**Nick Williams** *October 21, 2016 21:31*

Here is a PDF manual for a more traditional LPS sold by Light Objects

[http://www.robtechnika.com/docs/User%20manual%20EC-P100.pdf](http://www.robtechnika.com/docs/User%20manual%20EC-P100.pdf)



You will notice the different variations for wiring the LPS to a control system. Which are inline with what is described above.



This may be useful

[robtechnika.com - www.robtechnika.com/docs/User%20manual%20EC-P100.pdf](http://www.robtechnika.com/docs/User%20manual%20EC-P100.pdf)


---
**Ariel Yahni (UniKpty)** *October 21, 2016 21:38*

**+Don Kleinschnitz** could you translate this into kindergarden English " The control of "L" MUST be an open drain with no pullups etc. It needs to be a low true signal or said more simply, it must provide a "gnd"."


---
**Don Kleinschnitz Jr.** *October 21, 2016 23:31*

**+Ariel Yahni** sorry if I made it confusing. It means that literally only the drain pin on a FET who's gate is controlled by PWM should be connected directly to the "L" pin on the LPS. Nothing else is in or connected to that circuit. The ground associated with that FET should be connected to the LPS ground. 

Here is a drawing of a smoothie implementation. 

I think you are using a different controller. If this still doesn't work for your controller you can point me to a schematic and I will customize it for you.

![images/80ed3a95628450e74c0e2f31b8e5e457.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/80ed3a95628450e74c0e2f31b8e5e457.jpeg)


---
**Don Kleinschnitz Jr.** *October 21, 2016 23:32*

**+Ariel Yahni** here is a photo of my setup which is under test. This is the edge of the smoothie opposite the motor driver outputs.

![images/7a9fb5b3384ee22d75f49a64e3497a9d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a9fb5b3384ee22d75f49a64e3497a9d.jpeg)


---
**Ariel Yahni (UniKpty)** *October 21, 2016 23:53*

**+Don Kleinschnitz**​ thanks, I'm just trying to simplify the terminology for other readers. This is by far the most intimidating parts of the process but I'm sure language is the main reason. I have run 2 different boars. 1) azsmz where L goes to the negative pole on the heat board FET 2) Remix. Board where L goes into signal on PIN 1.18


---
**Don Kleinschnitz Jr.** *October 21, 2016 23:57*

**+Ariel Yahni** did I succeed :) is it simple enough? I am working on a write up that I hope helps both those skilled and those not and you can critique is as well. I think the best will be if I just get a picture of the wire with each end connected and the associated configuration setting that goes with it. Then the builder doesn't need to know the why.


---
**Don Kleinschnitz Jr.** *October 21, 2016 23:59*

**+Ariel Yahni** I think the real problem is that folks are using internal pins that are NOT open drains. I need to confirm such .....


---
**Ariel Yahni (UniKpty)** *October 22, 2016 00:04*

**+Don Kleinschnitz**​ please mind that this is a very positive critic. What you are doing is going to once and for all give piece of mind on in doing the convertion. Even if you provide a layout people need to understand the why, is just human nature


---
**Don Kleinschnitz Jr.** *October 22, 2016 00:12*

**+Ariel Yahni** I value your and the communities critique questions and challenges and inputs. That is how you get problems like this solved. 

I agree that we need the HOW and the WHY. I never engage in the HOW till I know the WHY that is how come my K40 conversion is going so slow .. :)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/dr5TbnVZiaF) &mdash; content and formatting may not be reliable*
