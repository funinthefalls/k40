---
layout: post
title: "Good morning. one more issue Im fighting"
date: August 02, 2016 14:05
category: "Original software and hardware issues"
author: "Bob Damato"
---
Good morning. one more issue Im fighting. When I Cut, I seem to get 2 cuts per line and it seems too thick of a cut now. Like its trying to keep the actual black line so it cuts each side of it. Am I doing something wrong or preparing the images wrong? Im using laserDRW but hopefully moving to coreldraw soon with the corellaser plug in.

This issue is making details difficult.

Thank you

bob

![images/1e7b1568ba073612a3ce5af3774fc1b9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e7b1568ba073612a3ce5af3774fc1b9.jpeg)



**"Bob Damato"**

---
---
**Bob Damato** *August 02, 2016 15:45*

I just wante to point out that the track outline SHOULD be two lines like that, but should be half as thick..


---
**Ulf Stahmer** *August 02, 2016 20:52*

Adjust the line width to 0.01 mm.  If the line is too thick, the laser treats it as a solid and cuts on both sides of the line.


---
**Bob Damato** *August 03, 2016 12:47*

Thanks Ulf. I dont see that option on LaserDRW. Sounds like its time to upgrade my software to something good.


---
**Ulf Stahmer** *August 10, 2016 12:45*

Hi **+Bob Damato**, for images imported into LaserDRW, adjust the line width before import.  For images drawn in LaserDRW, the line width is controlled by a setting called "pen width".


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/iU4qUB7EGuR) &mdash; content and formatting may not be reliable*
