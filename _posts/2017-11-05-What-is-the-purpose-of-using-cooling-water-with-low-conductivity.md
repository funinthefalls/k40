---
layout: post
title: "What is the purpose of using cooling water with low conductivity?"
date: November 05, 2017 20:29
category: "Hardware and Laser settings"
author: "Lars Andersson"
---
What is the purpose of using cooling water with low conductivity?

As I understand it the cooling water has no connection anywhere with the laser tube high voltage.



I can see the benfit of using very soft or demineralized water to prevent scale deposits in the cooling channel.



It seems popular to use distilled water with bleach added. That should be plenty conductive, so there must be other factors.





**"Lars Andersson"**

---
---
**Jim Hatch** *November 05, 2017 21:17*

Because you do have current moving in the proximity of the water. You can get induced currents that way if the water has sufficient metallic ion content. 


---
**Adrian Godwin** *November 05, 2017 21:35*

I think the initial reason is for avoiding scale. However, several people (including me) have noted a problem with the tube when using tapwater - the discharge avoids the electrode at the high-voltage end and there is a poor or ineffective output beam. Replacing the tapwater with deionised water (it doesn't need to be distilled) solves this. 



Some people have used tapwater without problems. I'm not sure why this is - it may depend on whether the body of cooling water is earthed or not. Presumably there's a capacitative coupling through the tube glass. It may also depend on how soft your tapwater is - mine is fairly hard.

   


---
**Ned Hill** *November 05, 2017 21:48*

You are right in that bleach can increase the conductivity which is why it is recommend to only add a small amount. Like a capful in 5 gal.  Also aquarium algaecide can be used to keep algae growth down, but again keep the concentration low.   Personally I just do regular water changes with distilled water and keep the recirculating tank protected from direct or indirect sunlight.


---
**Don Kleinschnitz Jr.** *November 06, 2017 00:20*

[donsthings.blogspot.com - K40 Coolant](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
*Imported from [Google+](https://plus.google.com/117177573126096232373/posts/GXzWaihHshv) &mdash; content and formatting may not be reliable*
