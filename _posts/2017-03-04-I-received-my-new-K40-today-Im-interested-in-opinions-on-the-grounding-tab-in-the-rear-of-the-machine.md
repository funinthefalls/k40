---
layout: post
title: "I received my new K40 today. I'm interested in opinions on the grounding tab in the rear of the machine"
date: March 04, 2017 22:54
category: "Discussion"
author: "Nathan Thomas"
---
I received my new K40 today. I'm interested in opinions on the grounding tab in the rear of the machine. How many of you are actually using it...and how? Earth grounding or something else? Would you mind posting pics or how to's as well? I see different opinions on whether to use it or not depending on where you live and your home setup. 





**"Nathan Thomas"**

---
---
**greg greene** *March 04, 2017 23:08*

14 Gauge stranded copper to 1 Inch copper Tube to 14 Gauge stranded to 8 foot ground road.  All my ham gear is also grounded through the 1 inch copper pipe.


---
**Jim Hatch** *March 04, 2017 23:08*

I'm using it. I ran a heavy wire from it to a 10 ft grounding rod I drove into the ground outside the garage. The rod is a 10 ft piece of solid copper from Home Depot. Cost me about $10 or $12 I think. I attached the wire to the rod using a saddle clamp (you'll find it in the aisle with the grounding rod) and crimped a spade end connector onto the other end to attach to the K40.



Some people will argue that it's not needed if you have properly grounded outlets in your house and claiming adding another rod will cause some kind of cross-current effect through the earth. I do and yet I still would get a low level leak I could measure if the garage floor was damp. The house ground is about 100 ft of wire away from my garage outlet and the floor is 6 ft from the K40 - it's easier for any current leakage to skip the long trip to the house ground and go through the case to me and the floor.



Since I put in the rod I've had no issue. Make sure you check to see that the grounding pin of the plug is attached to the grounding wire from the power supply in the K40 box (some aren't). 


---
**Ned Hill** *March 04, 2017 23:16*

Mine is in the house and I'm just using a grounded outlet plug.  If you are not sure if your outlets are grounded you can buy a cheap outlet tester to check.  If there was a convenient way for me to do a secondary grounding I probably would, but I don't have any problems with just using the plug.


---
**Mark Brown** *March 05, 2017 00:57*

You should take the grounding post loose and scrape the paint off the frame there.  It didn't look like mine was actually making contact with the frame.



Probably a good idea even if you're not going to connect a grounding rod to the post, I'm not sure there's really a solid ground to frame connection other than that.


---
**Steven Whitecoff** *March 06, 2017 04:09*

As the guys say, just do two things; clean the connection inside the case(That makes the case be grounded) and test your outlet with a fault tester. Its not that uncommon for outlets (especially garage/shops etc) to not be properly grounded on older houses. The ground post on the back is merely a duplicate tied right into the power socket. IF you test and find a ground fault on your chosen receptacle, then its either fix the ground or run a dedicated ground rod as shown in the instructions. The instruction I saw are worded as though the ground terminal will fix some issues- thats only true IF  your outlet isnt grounded since both are tied directly together.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/JevoYSb4qXV) &mdash; content and formatting may not be reliable*
