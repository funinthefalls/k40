---
layout: post
title: "I would like to switch out my controller board on the k40 to a CNC shield v3 I have the Motors And end stops Working ButI don't know which pin to use to fire The laser Is it the spindle on off pin on the shield I have the CNC"
date: December 08, 2018 10:20
category: "Hardware and Laser settings"
author: "Timothy \u201cMike\u201d McGuire"
---
I would like to switch out my controller board on the k40 to a CNC shield v3 I have the Motors And end stops Working ButI don't know which pin to use to fire The laser Is it the spindle on off pin on the shield I have the CNC shield flashed to GRBL 1.1 any help would be appreciated The Chinese control Board has got to go it is too  Flaky..  If the CNC Shield Is not a good pic for this purpose what would you recommen

Thanks for your help





**"Timothy \u201cMike\u201d McGuire"**

---
---
**Don Kleinschnitz Jr.** *December 08, 2018 14:06*

I would recommend C3D **+Ray Kholodovsky** if you want a hassle free install.


---
**Stephane Buisson** *December 08, 2018 14:36*

**+Timothy McGuire**, well it's a bit more than power the laser on/off. what board do you have ? (firmware). 

What software will you be using ? (compatible with your board ?)

will you be able to raster ? (fast enough)



that is the kind of questions you need to ask yourself.


---
**Ray Kholodovsky (Cohesion3D)** *December 08, 2018 16:14*

The new Cohesion3D LaserBoard is now in stock and the best value we have offered yet. 



More info and link at bottom of post: 

[plus.google.com - 2 years ago, on Thanksgiving evening, we launched our storefront for sale wit...](https://plus.google.com/+Cohesion3d/posts/BTHYvjcib2G)


---
**Tom Traband** *December 08, 2018 16:51*

On the version of thes CNC shield you (and I) have with grbl firmware 1.1 the laser power pin is the one marked  Z min endstop. The firmware had to juggle pins around to put the laser fire on one with pwm but the labeling of the older (and cheaper clone) boards doesn't match.

You should also be warned that the cnc shield/firmware and the laser are exact opposite as far as the power scale goes. When the laser sees 0v it fires at full power, including during the arduino boot cycle. You can invert the power scale behavior by editing the firmware but i still found the firing on startup a bit scary (I'm not as diligent using the disable switch as i should be) so i put together a simple voltage mod circuit (a couple of resistors, a transistor, and an led) that goes between the cnc shield pwm pin and the laser power supply pin. You should be able to find my post by searching this forum.


---
*Imported from [Google+](https://plus.google.com/109629943464502534866/posts/2ptEzDHLBQS) &mdash; content and formatting may not be reliable*
