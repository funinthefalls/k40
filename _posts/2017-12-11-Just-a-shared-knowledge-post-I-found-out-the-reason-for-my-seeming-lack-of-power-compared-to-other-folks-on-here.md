---
layout: post
title: "Just a shared knowledge post: I found out the reason for my seeming lack of power compared to other folks on here"
date: December 11, 2017 15:49
category: "Discussion"
author: "Chris Hurley"
---
Just a shared knowledge post: I found out the reason for my seeming lack of power compared to other folks on here. I mostly cut 3mm hardboard (a version of mdf) and it off gases a resin like substance. It was coating my lenses and mirrors. I used airbrush cleaner to remove it.





**"Chris Hurley"**

---
---
**Steve Clark** *December 12, 2017 02:15*

Thanks for the info. Just wondering, do you have a air assist head in use?


---
**Chris Hurley** *December 12, 2017 02:18*

Yes. But it was not always on as I use a airbrush compressor and it over heats if I use it all day. So for etching I didn't use it. 


---
**Chris Hurley** *December 12, 2017 02:19*

![images/35d2f0ffcec054d9a5ca14dc731ecd64.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/35d2f0ffcec054d9a5ca14dc731ecd64.jpeg)


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/L1Fw8Sh4vRZ) &mdash; content and formatting may not be reliable*
