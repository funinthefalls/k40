---
layout: post
title: "So I have noticed my laser isn't cutting as well as what it use to"
date: October 18, 2016 15:39
category: "Discussion"
author: "3D Laser"
---
So I have noticed my laser isn't cutting as well as what it use to.  The tube seems fine and it seems I have plenty of power but I'm getting a few dead spots while cutting even acrylic.  My focus and alignment seems to be good.  



My question is how much does a scratched lens affect cutting ?  My lens has seen better days



I also have SI mirrors and the coating is coming off does that mean they should be changed 





**"3D Laser"**

---
---
**greg greene** *October 18, 2016 15:56*

Short answer is Yes deterioration in the lens and mirrors will cause problems. 


---
**Scott Marshall** *October 18, 2016 18:01*

The reason you are seeing dead spots is a combination of your alignment being off a bit and optical element defects, most probably in the lens.



When the alignment is off, the beam passes through the lens (and hits the mirrors) at slightly different places. Certain places on the bed will cause the beam to hit the defects, weakening the end result. 



A fresh lens and even mirrors may be in order. If you have good aftermarket moly mirrors, they're pretty rugged and can usually be cleaned. Stock ones are junk and never seem to last very long. No matter what kind you have, if you run them dirty very long, they absorb energy instead of reflecting it. And it damages them. If there's any visible defect that won't wipe off with alcohol, it's time to change them (glue the old ones on a stick for awesome inspection mirrors). Lenses are pretty fragile and it doesn't take much to damage one. If run at all dirty they usually fry. If there's a visible scratch, it's junk.



 I've been running the same set of Saite moly mirrors for almost a year and they are like new. I clean and align periodically or whenever they seem like they may be 'off'.



Scott


---
**3D Laser** *October 18, 2016 18:04*

**+Scott Marshall**  why moly over so mirrors just curious 


---
**3D Laser** *October 18, 2016 18:06*

I think I might have tighten my lens mount to much on the lens as there is a visible circle on the lens where the lens mount screws down on the lens there is also a scratch on the lens.  I have the light objects lens mount is there a way to keep it from marking the lens 


---
**Phillip Conroy** *October 18, 2016 18:09*

For the cost of a sparebset if mirriors and focal len whybwouldnt you haveva spare set ready to use.i have 6 mirriors  and 6 50.8mm focal lens and 2 75mm focal length lens spare.

My tube has over 300 hours on the clock and is still outputting 35-40 watt going by my laser beam power meter.i am starting to look for a spare tube or replacement laser machine a larger cutting bed red 60 watt maching for around $2000  with 300mm by 500mm cutting bed.not so much for bed size but for hopefully faster cuttingv.


---
**Scott Marshall** *October 18, 2016 18:13*

**+Corey Budwine**



 Durability,  they can take repeated cleaning and not lose reflectivity. There are other materials that have slightly higher reflectivity, (a few tenths of a percent), but they degrade quickly in use. The moly ones last quite well, better than I expected.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 18, 2016 20:36*

**+Corey Budwine** I don't have the LO mount, but I recall seeing here people using little fibre gaskets to protect the lens when screwing up the mount. e.g. [https://www.bunnings.com.au/kinetic-12mm-fibre-body-washers-5-pack_p4920306](https://www.bunnings.com.au/kinetic-12mm-fibre-body-washers-5-pack_p4920306)

[bunnings.com.au - Kinetic 12mm Fibre Body Washers - 5 Pack &#x7c; Bunnings Warehouse](https://www.bunnings.com.au/kinetic-12mm-fibre-body-washers-5-pack_p4920306)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/bcJh4ZZhvom) &mdash; content and formatting may not be reliable*
