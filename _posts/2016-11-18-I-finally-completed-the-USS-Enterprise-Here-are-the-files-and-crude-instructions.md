---
layout: post
title: "I finally completed the USS Enterprise. Here are the files and crude instructions"
date: November 18, 2016 23:14
category: "Object produced with laser"
author: "Jeff Johnson"
---
I finally completed the USS Enterprise. Here are the files and crude instructions. [http://www.thingiverse.com/thing:1902315](http://www.thingiverse.com/thing:1902315)





**"Jeff Johnson"**

---
---
**Cesar Tolentino** *November 18, 2016 23:33*

Thank you for sharing. Live long and prosper. 


---
**K** *November 19, 2016 03:22*

Awesome! I'm going to cut one tonight!


---
**Gunnar Stefansson** *November 19, 2016 12:01*

Thank you very much Jeff. Thanks for sharing. Cutting one ASAP :D


---
**K** *November 21, 2016 19:43*

I've downloaded the DXF and this is what the blue looks like. I'm assuming that's not correct?

![images/b3412bb738c92873e48a9cedd708c466.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b3412bb738c92873e48a9cedd708c466.png)


---
**Jeff Johnson** *November 21, 2016 20:14*

**+Kim Stroman** No, that looks distorted. I'm exporting the DXF from CorelDraw so maybe it's causing problems, though it looks good when I open the file in Corel. I will see what other formats I can export and add them.


---
**K** *November 21, 2016 20:15*

**+Jeff Johnson** Thanks! 


---
**Jeff Johnson** *November 21, 2016 20:35*

Ok, try again. I've added PDF and DWG as well. Still trying to figure out how to export SVG properly. Are there any other formats you recommend?


---
**K** *November 21, 2016 20:41*

Thank you! The PDF downloads and opens. I'm on Illustrator CC, if that matters at all. The circles are complete, but they had a weird shape to them similar to how they look in the photo above. Reapplying the strokes fixed the issue and now they look as they should. In the DXF I couldn't even select the blue stroke for some reason, even with that layer active. The only issue I see with the PDF is the NCC-1701 in blue outline isn't converted to curves/outlines so the font drops out. It's not a big deal as the red is converted and I can copy that and apply the blue stroke. As for SVG from Corel, I haven't the foggiest. I could never get it to work properly. Thanks again for uploading new files!


---
**K** *December 31, 2016 06:00*

I finally had a chance to cut your design. My husband assembled it. Throughout the build we both kept commenting on how fantastic the design is. Thanks again for sharing it!

![images/75e1ecdad466171dfae210aa4f673cc3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/75e1ecdad466171dfae210aa4f673cc3.jpeg)


---
**Anthony Bolgar** *December 31, 2016 09:09*

Thanks for the files **+Jeff Johnson** 


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/JJVLeGPTSGA) &mdash; content and formatting may not be reliable*
