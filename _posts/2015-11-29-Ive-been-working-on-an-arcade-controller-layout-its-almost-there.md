---
layout: post
title: "I've been working on an arcade controller layout, it's almost there"
date: November 29, 2015 16:05
category: "Object produced with laser"
author: "Nathaniel Swartz"
---
I've been working on an arcade controller layout, it's almost there.  The player one button is still to close to the stick.  I also need to create some slots on top for reinforcements up from the bottom.﻿

![images/6c1604b02079df0115d8098af1fd5a5c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c1604b02079df0115d8098af1fd5a5c.jpeg)



**"Nathaniel Swartz"**

---
---
**Stephane Buisson** *November 29, 2015 16:28*

very clean job !


---
**Gary McKinnon** *November 29, 2015 17:16*

That looks great :)


---
**Sean Cherven** *November 29, 2015 18:12*

Nice! How does it interface?


---
**Nathaniel Swartz** *November 30, 2015 03:36*

**+Sean Cherven** I plan on using a Xin Mo board which breaks out pins for 10 buttons, and a joystick for two players.  Windows just shows it as a generic two player joystick which is fantastic for mapping to games.


---
**Nathaniel Swartz** *March 26, 2016 12:56*

**+thanh tran**​ The picture is just cardboard but the final design will be 1/4 inch plywood


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/V4fWf4NshZT) &mdash; content and formatting may not be reliable*
