---
layout: post
title: "I recently figured out the issue with RASTER via inkscape exporting plugin thanks to members in this community"
date: March 13, 2016 08:05
category: "Modification"
author: "Andrew ONeal (Andy-drew)"
---
I recently figured out the issue with RASTER via inkscape exporting plugin thanks to members in this community. It involved comparing their firmware to mine and adjusting variables accordingly. Superposition there were allot of differences between the two even though both were same builds and electronics (ramps 1.4 + arduino). Once I got this all figured out I tested another software I loved using for RASTER during which time inkscape was not working. Here's the thing this other software wouldn't work with the same firmware config.h which now allows for RASTER via inkscape, so I went back into config.h and checked code again and the one variable that stood out as a big difference was laser_pwm Hertz variable previously 25000 and now 50000. So I changed it back to 25000 and now both RASTER software programs work! 



Now if you follow what I've laid out above then here's my question. Can this pwm Hertz value be set to a variable like ;#define laser_pwm 25000-50000? Or is there a sweet spot for this variable which allows for best results? What does the Hertz do other than control frequency or what does frequency do in regards to end result (laser contact with objects)? 



Sorry if this is hard to follow but I am learning as I go and my current understanding of all this is very limited.





**"Andrew ONeal (Andy-drew)"**

---
---
**Stephane Buisson** *March 13, 2016 12:52*

**+Andrew ONeal** thank you  for sharing !


---
**Francis de Rege** *March 14, 2016 12:06*

Thanks! I had to go do some other things and when I got back to this you solved my problem. Works great.


---
**Anthony Bolgar** *March 15, 2016 06:26*

I assume you are actually refering to the PPM (pulse per minute) setting. I do not use any PPM settings, I leave it as a continuous wave form that is not pulsed


---
**Andrew ONeal (Andy-drew)** *March 15, 2016 06:35*

No I'm referring to the 

#define LASER_PWM (I've seen 8k, 20k,25k, and 50k values here) // HERTZ found under configuration.h tab just above thermal settings.


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/T76uXcjbzLh) &mdash; content and formatting may not be reliable*
