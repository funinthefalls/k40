---
layout: post
title: "Hey all, Just wondering what would be considered the maximum speed when engraving?"
date: September 09, 2015 01:43
category: "Software"
author: "I Laser"
---
Hey all,



Just wondering what would be considered the maximum speed when engraving? I've tried 500mm but worried I might cause issues (burning out stepper motors, etc) if I set it too high.



Cheers





**"I Laser"**

---
---
**Michael Bridak (K6GTE)** *September 09, 2015 01:59*

I engrave at 800+ mm/min.


---
**I Laser** *September 09, 2015 04:31*

Thanks, I assume you mean 800mm/s :) 



I did a quick run at 1,000mm/s and it seemed to handle it okay but too chicken to push it further at the moment.


---
**Kirk Yarina** *September 09, 2015 12:12*

Are you increasing the configured max speed?  Don't know where it is in Moshi and others, but in LaserDRW there's a setting in the options dialog box.  Grbl, etc, will have similar options.  LaserDRW will automatically change entered speeds set over the max, and will limit the absolute max speed to 500 mm/sec.  When I first got my 3D printer a couple years ago I did a lot of speed testing, and was really impressed until I discovered the limit prevented it from actually reaching those speeds.



What's the maximum vector speeds that you're seeing?  My K40 will start missing steps somewhere over 50 (didn't write the limit down, was just playing around).  My guess is my board doesn't handle acceleration very well, and there's no configuration option I can find.


---
**Jon Bruno** *September 09, 2015 16:46*

Yeah, you won't burn out the motors but the accuracy will degrade because the pulses will be too quick and the motor will begin missing steps or lose enough torque to overcome the load of the belts and carriages.

I don't engrave with my k40 much over 400-450. I found that any higher and I lost steps and it spoils the work.


---
**I Laser** *September 09, 2015 23:07*

Thanks for the replies. I hadn't even thought about there being a limit. You're right Kirk, upon checking it's set to 500mm/s in corellaser. Strange though, it did finish a small job quicker when set to 1000 compared to 500 mm/s!?!! 3 minutes compared to 3.5 minutes respectively.



Not sure how to check the vector speed, if you have a link or info happy to check that. Haven't had any skipping issues thus far.


---
**Kirk Yarina** *September 12, 2015 12:49*

My K40 starts losing position somewhere above 50mm/s, it's pretty obvious when it starts since line segments start moving around.  I didn't try to find a good vector file to test it, or see what the actual maximum speed was, but it's a lot lower than I expected it would be.  I'll add it to my ever-lengthening to-do list.



Smoothie, tinyg, marlin on ramps, etc should handle this a lot better - my guess is the bare-no-details cheap controller doesn't handle acceleration very well where all of the above do (and tinyg's method is especially interesting).


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/ZbyHczNfoAc) &mdash; content and formatting may not be reliable*
