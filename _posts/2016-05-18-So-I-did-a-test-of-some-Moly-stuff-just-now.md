---
layout: post
title: "So I did a test of some Moly stuff just now"
date: May 18, 2016 09:03
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I did a test of some Moly stuff just now. I purchased [http://www.supercheapauto.com.au/online-store/products/Liqui-Moly-Mos-2-Anti-Friction-Engine-Treatment-300mL.aspx?pid=156706](http://www.supercheapauto.com.au/online-store/products/Liqui-Moly-Mos-2-Anti-Friction-Engine-Treatment-300mL.aspx?pid=156706) from SuperCheap Auto here in Australia.



It's not an aerosol spray, it's a liquid grease kind of substance. Similar consistency to gravy. Anyway, I rushed into it as I wanted to see if it works. Had trouble getting it to dry quickly, as hitting it with the heatgun just wanted to make it blow away/run off the material.



So, I just lasered it wet to see whether it does anything. Surprisingly it does, however very faint. I've coated the same piece again (on the opposite side) with a very liberal quantity & have left it to dry overnight, so hopefully it will be dry tomorrow.



Power settings: 10-15mA. Speed: 50mm/s.

Might be worth dropping the speed to about 10-20mm/s & using my vector engrave method to speed up the process.



![images/aa74bdf99dc8e70b22740bb4f0d33d95.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa74bdf99dc8e70b22740bb4f0d33d95.jpeg)
![images/343b2041a0ee298c21a7ddd8b7ee16e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/343b2041a0ee298c21a7ddd8b7ee16e6.jpeg)
![images/e1ddb236148f7ffa152a4cc792f657b9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1ddb236148f7ffa152a4cc792f657b9.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 18, 2016 09:04*

**+Phillip Conroy** First test done, with the lube still wet (as I just wanted to have a go). Marked it, doesn't wipe off with cloth at least. But very faint.


---
**Eric Rihm** *May 18, 2016 09:13*

Demonstration of some RZ40 
{% include youtubePlayer.html id="-5nf5wl0j1k" %}
[https://youtu.be/-5nf5wl0j1k](https://youtu.be/-5nf5wl0j1k) heard ﻿CRC 03084 Moly works too 


---
**Scott Thorne** *May 18, 2016 10:31*

**+Eric Rihm**...whoever did the video has his power at 80%...I have the same machine and that's 20mA output to the tube...way to high to get any life out of the tube. 


---
**Scott Marshall** *May 18, 2016 10:51*

I think you have the Moly Grease us motorheads use to break in flat tappet cams. It's designed to hold to the pores in the cam caused by the parkerizing and allow the cam and lifter to lap onto each other. It's NOT intended to dry at all, it has an oil base. (I'm suffering a cable problem here and can't seem to get your link to work)



What you want is the spray which is dry powdered moly in a solvent carrier. It's the modern high tech replacement for dry Graphite spray. Wonder if that would work, I've got a case out in the shop we used to use on industrial ovens...



I've got some moly spray in the shop,been meaning to try it,  but have been busy working on my new products. The now named "Water Safety" kit for your K40 laser... protects against low flow, temp and low level, Early warnings on all but low flow.All plug in install, and it's programmable.My 1st all SMD product.  Very cool. Very small too....



I also have a system going together that allows solder free Smoothieboard or ramps install in a k40. It includes everytihng except the board.Mount the new power supply, and plug in. Reversible and you can even leave your M2Nano board in place. Comes with dimmable lighting, and a Water Safety kit...



Hope i sell some, I've got a bunch of money in parts and a bizillion hours engineering, prototyping, etc.



Between that and my Drs appts, I've been to busy to play...



How goes the nozzle project?



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 18, 2016 11:03*

**+Scott Marshall** Yeah I think what I have got doesn't seem to want to dry.  I just grabbed what I could find locally that had MoS2 in it. I tried searching for a "dry" lube version, but I had about 5 minutes before the shop shut. Should possibly have waited for tomorrow, but I like to rush things a lot haha.



Nozzle project is a bit up in the air at the moment. I have it sent through 3d hubs to the guy to print it, but I haven't heard back from him this week regarding starting the job. I might have to give him a ring & see what's up.



Hopefully I can get a working version this week (or early next week) of the nozzle & give it a thorough testing. If it works out well, then I'll look at Shapeways & getting metal ones done.



You water safety/smoothie projects sound interesting. Will have to see what's up with them when I get around to doing an upgrade.


---
**Scott Marshall** *May 18, 2016 11:21*

I've been wondering when you'd have to turn it off to change the tube. You must have some serious hours on that one. You must have a warehouse full of projects... 



On the other hand, mine is getting too much rest. I keep taking it apart to use it for testing, it's never running when I want to burn something... I'll be glad when it's finally hooked up and running, permanently.  Maybe I can budget a "test" model in the next year or so if this stuff sells a bit. Then I can play when I feel like it. 



Been up all night, ought to get some rest, catch you later.



Scott.


---
**Anthony Bolgar** *May 18, 2016 12:23*

**+Scott Marshall** I have a K40 that is fully upgraded and functional (within the limits of Ramps/Marlin) that I am currently using as the machine that is always ready to go. My Redsail LE400 is the "lets play and try this insane idea" machine that is in a constant state of flux. But I need to get it finished with a Smoothie upgrade and make it the production machine as it is a larger, higher quality laser than the K40. So once I get it finished, I will start using the K40 as my R&D machine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 18, 2016 12:37*

**+Scott Marshall** Not sure if you are meaning my machine or someone else's. I honestly have no idea how many hours I've used my machine, but I'd say since September 2015 when I got it, I average about 1 hour every 2 days. Sometimes I go days without using it, others I use it for 5 or more hours in the day. I'm not looking forward to when the tube runs out & I have to get a new one (more costs). Not sure about a warehouse full of projects, but I've got a serious amount of "test" pieces laying around. That's about all I've got. I do all these tests just to see what I can do. Then move onto the next "I wonder if I can do this" idea.



**+Anthony Bolgar** An R&D machine is a great idea. It would be nice to have 1 always functioning machine & another for tinkering. I hate when I am tinkering & realise that I need to put the whole laser back together in order to cut the piece for the tinker I am doing. Then pull apart & make sure it fits, put back together & recut (if I made errors).


---
**Josh Rhodes** *May 18, 2016 13:05*

**+Scott Marshall**​ you should make a g+ post in this community about your products. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 01:23*

I've found a supplier semi-locally that sells a Dry Moly Lube that is much higher percentage of moly than the CRC one. [http://www.minehanagencies.com.au/lubricants.html#circlespray](http://www.minehanagencies.com.au/lubricants.html#circlespray)



Molybdenum disulphide 40-60%

Isopropanol 10-20%

Butane 40-60%

See the MSDS ([http://www.minehanagencies.com.au/Final%20Website%20Update%202011/Products/Circle%20Spray/Circle%20Spray%20MSDS.pdf](http://www.minehanagencies.com.au/Final%20Website%20Update%202011/Products/Circle%20Spray/Circle%20Spray%20MSDS.pdf))



Compared to the CRC Dry Moly:

Molybdenum disulphide 1-3%

& CRC Quick Dry Moly:

Molybdenum disulphide 5-10%



I have contacted the nearest office on their list to see what the prices are for it & see about where I can purchase some.



I'm curious as to whether higher quantity of MoS2 makes a difference?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 02:24*

Bloke just got back to me from Minehan & said $22.95 for a can. I'm going to head up shortly & pick one up & then I'll give it a test this afternoon. Interested to see if the higher % of MoS2 makes any difference compared to other's results.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/UTbrgatucRj) &mdash; content and formatting may not be reliable*
