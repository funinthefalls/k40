---
layout: post
title: "So this might be a simple process for most of you, (electronics isn't my strong point) but does anyone have a tutorial/instructions on how to add a microswitch to the lid of the laser so it won't fire if door is open?"
date: August 23, 2016 17:14
category: "Discussion"
author: "Nigel Conroy"
---
So this might be a simple process for most of you, (electronics isn't my strong point) but does anyone have a tutorial/instructions on how to add a microswitch to the lid of the laser so it won't fire if door is open?



I have the 50watt version, k50?



I get the basic concept but want to make sure I do this right.



These are the ones I'm looking at getting, not sure what spec I need so suggestions welcome.



[https://www.amazon.com/AC125-Lever-Action-Micro-Switch/dp/B0147XS4NA/ref=sr_1_1?rps=1&ie=UTF8&qid=1471972330&sr=8-1&keywords=microswitch+long+lever&refinements=p_85%3A2470955011](https://www.amazon.com/AC125-Lever-Action-Micro-Switch/dp/B0147XS4NA/ref=sr_1_1?rps=1&ie=UTF8&qid=1471972330&sr=8-1&keywords=microswitch+long+lever&refinements=p_85%3A2470955011)





Thanks





**"Nigel Conroy"**

---
---
**Ariel Yahni (UniKpty)** *August 23, 2016 17:52*

I would connect it in series to the enable laser button. Thi switch has 3 legs,  2 of the marked as NC / NA and another COM.  You should then connect the NC and the COM to those on the fire laser enable button. NC stand for normally closed so when the lid is closed its enabled 


---
**Nigel Conroy** *August 23, 2016 17:58*

Thanks **+Ariel Yahni** , are the ones I've linked above suitable?


---
**Ariel Yahni (UniKpty)** *August 23, 2016 18:05*

Yes


---
**Robi Akerley-McKee** *August 23, 2016 18:27*

**+Ariel Yahni** NC normally closed when switch is untripped.  NO normally open switch is untripped.  NO is the one you want to use for lid switches so when it's activated (the lid closed) it is in the closed or conducting position.  On my Laser power supply, I unhook the G wire going to the laser switch, solder a wire on the wire I just took off, cover the joint in heat shrink, and route the wire to the lid switch and solder is to the NO connector.  I solder another wire from the COM on the switch and run it to the G terminal where you took the wire out of.  So now the connections are: power supply to laser switch on control panel, to the lid switch and then back to the power supply.



You can also add a keylock in there as well up on the panel.  You just unsolder a wire on the laser switch, move it to a leg of the key switch, and run a little wire between the key switch and the laser switch.  Putting the key on a hook high up on the wall keeps little fingers from grabbing it.  


---
**Robi Akerley-McKee** *August 23, 2016 18:33*

This is what I used on my 1st K40, and I'm going to add to the second. 



[http://www.ebay.com/itm/Zinc-Alloy-Electronic-On-Off-Two-Terminals-Keys-Switch-Lock-Switch-2-Keys-DG-/252465725301](http://www.ebay.com/itm/Zinc-Alloy-Electronic-On-Off-Two-Terminals-Keys-Switch-Lock-Switch-2-Keys-DG-/252465725301)


---
**Ariel Yahni (UniKpty)** *August 23, 2016 18:41*

**+Nigel Conroy**​ my mistake is the inverse of the naming, ​ **+Robi Akerley-McKee**​ is correct


---
**Nigel Conroy** *August 23, 2016 18:44*

I like that a lot **+Robi Akerley-McKee** ,as mine is in a classroom setup. 



You'll have to take photo's or draw me a diagram.....



No worries **+Ariel Yahni** , I appreciate you helping me out.


---
**Nigel Conroy** *August 23, 2016 19:07*

I've already bought the key switch **+Robi Akerley-McKee** 


---
**Robi Akerley-McKee** *August 23, 2016 22:01*

**+Nigel Conroy** Mine is in a maker space, so being able to disable with key is a plus.


---
**Robi Akerley-McKee** *August 23, 2016 22:07*

**+Nigel Conroy** Tomorrow I will be taking pictures. I'll post a picture of the keylock hookup right now.


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/AmpbBdjzKU2) &mdash; content and formatting may not be reliable*
