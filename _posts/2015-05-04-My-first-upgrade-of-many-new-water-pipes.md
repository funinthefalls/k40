---
layout: post
title: "My first upgrade of many, new water pipes"
date: May 04, 2015 22:58
category: "Modification"
author: "Stuart Middleton"
---
My first upgrade of many, new water pipes.

When I got my cutter, the tube was broken. They shipped me a replacement to fit. I had lots of problems fitting the think pipes that came with it. Far too small for the tube and not long enough for me. This is 8mm internal diameter pipe which fits perfectly and gives improved water flow.



![images/0e11a8e31e919deec2e444fac3a6bd68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0e11a8e31e919deec2e444fac3a6bd68.jpeg)
![images/45e872bc228306616756666a2e5f6901.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/45e872bc228306616756666a2e5f6901.jpeg)
![images/90483c1c619c302ab84a8932c1df4ea9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90483c1c619c302ab84a8932c1df4ea9.jpeg)
![images/9480a05165381601af3024cdc0b77be3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9480a05165381601af3024cdc0b77be3.jpeg)
![images/be3c893a7b1def7d616ecab1ca85c878.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be3c893a7b1def7d616ecab1ca85c878.jpeg)

**"Stuart Middleton"**

---
---
**Jim Coogan** *May 05, 2015 00:05*

The water pump that comes with the unit is ok but if you replace it do not get a really large one.  The only purpose of that pump is to circulate water so the laser stays under a certain temperature.  Move the water too fast and you actually generate a small vibration as the water hits corners in the tube.  That will result in interesting results when you engrave that you do not want.


---
*Imported from [Google+](https://plus.google.com/+StuartMiddleton1/posts/g8qfFxRCBuy) &mdash; content and formatting may not be reliable*
