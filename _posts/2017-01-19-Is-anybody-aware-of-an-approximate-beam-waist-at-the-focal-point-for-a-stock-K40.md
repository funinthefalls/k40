---
layout: post
title: "Is anybody aware of an approximate beam waist at the focal point for a stock K40?"
date: January 19, 2017 06:53
category: "Hardware and Laser settings"
author: "Austin Baker"
---
Is anybody aware of an approximate beam waist at the focal point for a stock K40? Am curious if anybody has worked this out -- based on my first few days of testing, it looks like maybe 0.5 to 0.75mm? I understand that with its Gaussian distribution, the ablation will be wider at higher powers. Any spec sheets on the focusing lens that comes with the K40? Am wondering what the theoretical minimum is on the beam, as this plays into the systems overall level of detail / "resolution". Thanks!





**"Austin Baker"**

---
---
**Anthony Bolgar** *January 19, 2017 06:54*

I set my beam width at .5mm in LaserWeb


---
**Alex Krause** *January 19, 2017 07:06*

Since all optics,alignment, and tube strengths vary it's always going to be an approximation. The best I could get with stock k40 equipment with tuned in alignment was about .4mm based of measurements of circles and squares I made while testing in wood and acrylic while I was first getting started 


---
**Alex Krause** *January 19, 2017 07:07*

The k40 is manufactured by several companies so tubes mirrors and lenses vary in spec and quality so it's all guess work


---
**Austin Baker** *January 19, 2017 07:58*

I see. I imagine with custom optics, the beam waist could be brought down. Or truncating the beam?


---
**Phillip Conroy** *January 19, 2017 08:13*

Best Igot was 0.03 mm measured at bottom of 3mm mdf,read this     

[parallax-tech.com - Frequently Asked Questions about CO2 laser lenses for
      cutting](http://www.parallax-tech.com/faq.htm)


---
**Don Kleinschnitz Jr.** *January 19, 2017 13:18*

 Some interesting data here also: 

[troteclaser.com - Focus lenses for optimal laser engraving, laser marking and cutting USA](https://www.troteclaser.com/en-us/laser-machines/laser-accessories/focus-lenses/)



This video is informative: 
{% include youtubePlayer.html id="61bGzWicRRk" %}
[https://youtu.be/61bGzWicRRk](https://youtu.be/61bGzWicRRk)


---
**Ned Hill** *January 19, 2017 18:29*

If you do a ramp test with a piece of card stock you should be able to figure it out fairly easily.


---
**Austin Baker** *January 19, 2017 20:02*

**+Ned Hill** Good idea -- I could do a series of lines with increasing power levels. How can I have the laserdrw software draw a single "dot"? If I draw a very small circle, it still seems to try and draw/pass over it a couple of times instead of firing once and moving on to the next dot.


---
**Austin Baker** *January 19, 2017 20:04*

**+Don Kleinschnitz** Thanks. Are the trotec lenses compatible with the K40 or is a separate head needed?


---
**Ned Hill** *January 19, 2017 22:22*

**+Austin Baker** I was thinking that you would take a piece of cardstock and place it at an angle with the bottom below the focus and the other end above the focus.  Then vector draw a line up the card. Would need to play with the power and speed to get a good sharp mark around the focus without too much extra burn.  Then measure the linewidth at the narrowest spot.


---
**Don Kleinschnitz Jr.** *January 20, 2017 00:01*

**+Austin Baker**  any lens that is designed for 10600 and will fit the lens mounting will theoretically work. What are you wanting to accomplish by getting spot size and lens changes.


---
**Don Kleinschnitz Jr.** *January 20, 2017 00:04*

**+Austin Baker** you can find the kerf (dot size) by cutting a square and then measure the inside piece and outside piece, subtract the two and then divide by two. That will give you the kerf not the optical spot size? Depends if you want the optical size or the imaged size. Material makes a big difference here.


---
**Austin Baker** *January 20, 2017 19:13*

**+Don Kleinschnitz** That might yield useful information. Increasing the power increases the effective beam waist. Can anything done at high speed high power be done at low speed low power with the same quality? As long as the ratio is maintained, right?


---
*Imported from [Google+](https://plus.google.com/100996155784545993018/posts/LyY6cRphoU6) &mdash; content and formatting may not be reliable*
