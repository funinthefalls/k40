---
layout: post
title: "What a bargain! I'm thinking of getting a few as spares :)"
date: October 07, 2016 19:58
category: "Discussion"
author: "Mircea Russu"
---
[http://www.ebay.co.uk/itm/40W-CO2-USB-Port-Laser-Engraver-Cutter-Engraving-Cutting-Machine-High-Precise-/291872199450?hash=item43f4f0531a:g:YjYAAOSwDJhXMUso](http://www.ebay.co.uk/itm/40W-CO2-USB-Port-Laser-Engraver-Cutter-Engraving-Cutting-Machine-High-Precise-/291872199450?hash=item43f4f0531a:g:YjYAAOSwDJhXMUso)



What a bargain! I'm thinking of getting a few as spares :)





**"Mircea Russu"**

---
---
**Norman Kirby** *October 07, 2016 20:27*

i'll have a couple aswell, just in case they go up in price 




---
**Mircea Russu** *October 07, 2016 20:49*

Going up in price is not a problem for us. What should we do if they go down? :)


---
**Ariel Yahni (UniKpty)** *October 07, 2016 22:29*

Is there someone in the world that could really really buy this? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 08, 2016 05:06*

I think I might have to list mine to undercut them... at about 90% haha.


---
**Mircea Russu** *October 08, 2016 07:52*

Leaving the joke aside. All normally priced K40 in Europe are gone from Ebay. The G350(30x50) has the same price but shipping to Romania went from 160 to 400GBP. X700 clones are priced the same including shipping so I guess in not the GBP-USD rates. 


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/F1XqpjNmTBD) &mdash; content and formatting may not be reliable*
