---
layout: post
title: "Cutting vectors from a program outside of Laserdrw?"
date: November 19, 2017 17:52
category: "Software"
author: "James Lilly"
---
Cutting vectors from a program outside of Laserdrw?  I typically use Adobe Illustrator to create art, now that I have a K40 with Laserdrw, how do I get an irregular shape, draw lets say drawn with the pencil tool or the outline of shapes combined with the pathfinder out of Illustrator and into Laserdrw?  I can't seem to figure it out.  Of course it does cut vectors, since there are pre-set shapes in the program, but I need more than circles, stars, squares, etc.





**"James Lilly"**

---
---
**James Lilly** *November 20, 2017 01:13*

Not exactly answering my own question, but Inkscape looks promising.  I just imported one of my AI files right in.  Lets see if I can get it to both engrave and cut.




---
**James Lilly** *November 20, 2017 03:22*

Wow! Got it going with not too much hassle.  A little burning, and I think the bed is slightly tilted.  I have an air assist nozzle I can install. I'm pointed in the right direction. Inkscape with the extension was the ticket.  I couldn't get the Whisperer software to run.

[https://plus.google.com/photos/...](https://profiles.google.com/photos/114642009939368433927/albums/6490331855208563489/6490331854495850050)


---
**Joe Alexander** *November 20, 2017 09:06*

try Laserweb4 if you decide to go open source with your controller board, it works well with these machines.


---
*Imported from [Google+](https://plus.google.com/114642009939368433927/posts/hBSuSPHchxS) &mdash; content and formatting may not be reliable*
