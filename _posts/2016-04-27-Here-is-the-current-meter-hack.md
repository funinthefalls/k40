---
layout: post
title: "Here is the current meter hack ....."
date: April 27, 2016 16:37
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Here is the current meter hack .....


**Video content missing for image https://lh3.googleusercontent.com/-lUdEE9TVd8w/VyDq0PiD1VI/AAAAAAAAY38/L_7dSyo7m8osGBCe2L1Hw9A9fC4IyHkyw/s0/20160112_155641.mp4.gif**
![images/36e7c9d27da13337fa1be00b4be3bacf.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/36e7c9d27da13337fa1be00b4be3bacf.gif)



**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *April 27, 2016 16:44*

Nice where did you get it? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 16:51*

That's exactly what I want to do, after the numerous other things that I want to do. Great hack.


---
**Don Kleinschnitz Jr.** *April 27, 2016 18:31*

[http://www.amazon.com/DROK-Ultra-small-Voltmeter-Motorcycle-Detector/dp/B00C58QIOM?ie=UTF8&psc=1&redirect=true&ref_=oh_aui_detailpage_o02_s00](http://www.amazon.com/DROK-Ultra-small-Voltmeter-Motorcycle-Detector/dp/B00C58QIOM?ie=UTF8&psc=1&redirect=true&ref_=oh_aui_detailpage_o02_s00)




---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 19:04*

**+Don Kleinschnitz** I was wondering why the reading only went to 4.9 at max. Makes sense now. It's the voltage.


---
**Anthony Bolgar** *April 27, 2016 20:06*

I have a couple dozen of these little volmeters, in various colours. Think I will add it in to my K40 and Redsail LE400


---
**Don Kleinschnitz Jr.** *April 27, 2016 20:42*

Yup **+Yuusuf Sallahuddin** its a relative measure of power but its just reading the voltage off the pots center wiper which is what is controlling the laser supply?




---
**Vince Lee** *April 27, 2016 21:48*

From what I recall, the current meter is typically  connected to ground on the return line from the laser.  So theoretically if you put one of these voltmeters in 

parallel with a 1k resistor then your voltmeter could replace the existing ammeter and read the current in mA.


---
**Don Kleinschnitz Jr.** *April 27, 2016 21:57*

I would assume you can just put a digital ammeter where the analog meter is. The current version of these meters have an internal shunt. I did not like a digital circuit in a HV ground ..

But the problem we are solving is that the current meter doesn't show anything until the laser fires. The pot controls the HV PS by applying a 0-5v value whereas the voltmeter shows you it relative position all the time.


---
**I Laser** *April 27, 2016 23:26*

Stop doing these cool things so I feel the need to yet again mod my machine!!!! lol


---
**Vince Lee** *April 28, 2016 00:40*

**+Don Kleinschnitz** ah, that's cool.  Perhaps then a meter like this that claims to be "adjustable" could let you rescale the number displayed up enough to display either a milliamp value or a percentage?  [http://www.amazon.com/gp/product/B0154KFZFU](http://www.amazon.com/gp/product/B0154KFZFU)  


---
**Don Kleinschnitz Jr.** *April 28, 2016 01:28*

**+Vince Lee** if by adjustable they mean that you can change the scale that would work. They don't explain adjustable though. I suspect it is the same as this one [https://www.adafruit.com/products/705](https://www.adafruit.com/products/705) 

which is not what you want. This type of meter isolates the display voltage from the measured voltage and that is why there is three wires. It is not scale adjustable.

Certainly you could put a circuit between the pots center wiper that would scale the display on a voltmeter. You could do it with a op amp or get really fancy and put an arduino in between. I originally was going to do that but I decided to use this until I just upgraded to a new DSP as there were so many other uglies that needed to be fixed. Turns out for me whether I set it 0-4 for power or had a nice 0-100% they are both relative anyway. 

The right way is for the DSP to control the power anyway.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2016 01:34*

**+Don Kleinschnitz** Yeah it wouldn't be too  problematic to convert 0-4 to a 0-100 in your head.  As you said, it's all relative anyway. I like the way that it is always telling you the value, not just when you fire the laser. +1 that.


---
**Vince Lee** *April 28, 2016 03:19*

**+Don Kleinschnitz** ok. I guess you can always make a voltage divider with two resistors to scale down the number to say from 0 to 1.0 


---
**Don Kleinschnitz Jr.** *April 28, 2016 10:21*

**+Vince Lee** you can make a voltage divider but you have to isolate it from the input to the power supply as the pots wiper is controlling the laser power supply (our meter is just measuring that). That's the reason for the op amp in between the pot and the display:)


---
**Don Kleinschnitz Jr.** *April 28, 2016 10:31*

FYI here is how Light object puts a digital meter ammeter in series with the tube

[http://www.lightobject.info/viewtopic.php?f=16&t=1759](http://www.lightobject.info/viewtopic.php?f=16&t=1759)


---
**Vince Lee** *April 28, 2016 13:42*

**+Don Kleinschnitz** A similar meter I looked up says it has a 100k internal resistance so can't you just add say around 400k resistor in series it would divide down the measured voltage.


---
**Mishko Mishko** *May 01, 2016 14:38*

Why not just use a double pot, take any reference voltage you can get from anywhere in the machine (say 5V DC), add a tr54immer to calibrate it to whatever you like?


---
**Pigeon FX** *May 06, 2016 09:38*

Sorry for the silly question, is this wired in between center lug of the pot and ground?


---
**Don Kleinschnitz Jr.** *May 06, 2016 12:29*

Looking from the back of the panel and back of pot and the solder lugs are downward facing. Connect as follows. I pulled off existing lugs soldered these on and then pushed lugs back on.

Meter black on left

Meter white in middle

Meter black on right



I will post a picture .....


---
**Don Kleinschnitz Jr.** *May 06, 2016 12:36*

Here is picture: [https://plus.google.com/113684285877323403487/posts/7tKY3Yg6yD6](https://plus.google.com/113684285877323403487/posts/7tKY3Yg6yD6)




---
**Pigeon FX** *May 06, 2016 12:48*

**+Don Kleinschnitz** Ah its the 3 pin meter (VCC/VIN/GND?), could this be done with a 2 pin meter (VIN/GND) as I have half a dozen of them sitting in a box doing nothing!


---
**Don Kleinschnitz Jr.** *May 06, 2016 19:42*

Point me to the one you are using?


---
**Pigeon FX** *May 06, 2016 20:15*

Thanks Don. I have a few of these ones 

[http://www.banggood.com/0_28-Inch-2_5V-30V-Mini-Digital-Voltmeter-p-974258.html](http://www.banggood.com/0_28-Inch-2_5V-30V-Mini-Digital-Voltmeter-p-974258.html)


---
**Don Kleinschnitz Jr.** *May 07, 2016 14:14*

Those meter will work i.e. but not get you what you want.

The pot we are measuring across adjusts the input to the LPS from 0-5VDC. This is measured at the center tap.

The meter you pointed me to measures from 2.5- 30VDC. This means that the DVM will likely not turn on until the pot is about 1/2 way through its travel. (not actually 1/2 cause the pot is nonlinear). These meters use the input voltage to power them so they cannot read anything below the voltage needed to turn them on.

That is why I use the 3 wire. In the 3 wire DVM the voltage to power the meter is independent of the voltage it is measuring therefore you can measure from 0.


---
**Don Recardo** *June 24, 2017 17:46*

If you take the wiper of the pot to an analogue input on an arduino say A0, the 0-5v input will give  0 - 1023 when you read A0  but then using the arduino's map command you can easily map 0-1023 to 0-100 and there you have your power as a %



make two variables 

laserValue ( to hold raw value from input)

laser( to hold remapped value)

laserVal = analogRead(A0);         //read the pot value 0-1023 from analogue 0

 laser=map(laserVal,0,1023,0,100);  // remap it 0 - 100 

now just ouput the variable laser to an LCD

I have a 4 line LCD that shows 

Laser power

Coolant Flow Rate

Ambient  Temp

Coolant temp 

and Delta T ( the difference between the two temps)




---
**Don Recardo** *June 25, 2017 19:08*

here it is working  [https://www.dropbox.com/s/0g4v44z9vqbzmfy/20170625_193838.mp4?dl=0](https://www.dropbox.com/s/0g4v44z9vqbzmfy/20170625_193838.mp4?dl=0)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/M8PX3HvY1YD) &mdash; content and formatting may not be reliable*
