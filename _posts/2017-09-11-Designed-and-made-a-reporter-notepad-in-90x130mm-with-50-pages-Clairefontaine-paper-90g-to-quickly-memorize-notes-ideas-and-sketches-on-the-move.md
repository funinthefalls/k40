---
layout: post
title: "Designed and made a reporter notepad in 90x130mm with 50 pages Clairefontaine paper (90g) to quickly memorize notes, ideas and sketches on the move"
date: September 11, 2017 18:29
category: "Object produced with laser"
author: "Bernd Peck"
---
Designed and made a reporter notepad in 90x130mm with 50 pages Clairefontaine paper (90g) to quickly memorize notes, ideas and sketches on the move. 

A spiral binding allows the pages to be folded over completely and allows for opening and closure as often as required to refill with paper or to mix paper variants.

Polypropylene covers with a thickness of 0.8mm protect the sheets on the front and back.

I have put it on Etsy to see if this is going to sell - [https://www.etsy.com/de/listing/543140930/reporter-notizblock-a5-blanko-unliniert?ga_order=most_relevant&ga_search_type=all&ga_view_type=gallery&ga_search_query=edc%20notepad&ref=sr_gallery_3](https://www.etsy.com/de/listing/543140930/reporter-notizblock-a5-blanko-unliniert?ga_order=most_relevant&ga_search_type=all&ga_view_type=gallery&ga_search_query=edc%20notepad&ref=sr_gallery_3)  



The 50 pages refill (3,50 Euro) and the pen holder (2,50 Euro) will be on Etsy from Saturday night on this week.



![images/08828fe56962ebe0e2a9370c9fa0784f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/08828fe56962ebe0e2a9370c9fa0784f.jpeg)
![images/2d401c6aa1884953fc7f8fab69db7f5d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2d401c6aa1884953fc7f8fab69db7f5d.jpeg)
![images/b0781a3a1ec22fdd94c953ab81979cc1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b0781a3a1ec22fdd94c953ab81979cc1.jpeg)
![images/c5257d1ebbd750fbbdd768341bf43ca6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c5257d1ebbd750fbbdd768341bf43ca6.jpeg)
![images/017a81ab1535d06baeeb78531af9694e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/017a81ab1535d06baeeb78531af9694e.jpeg)

**"Bernd Peck"**

---


---
*Imported from [Google+](https://plus.google.com/100238441539373475210/posts/jCAe9gDVTaa) &mdash; content and formatting may not be reliable*
