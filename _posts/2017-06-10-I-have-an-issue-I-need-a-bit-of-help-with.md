---
layout: post
title: "I have an issue I need a bit of help with"
date: June 10, 2017 21:50
category: "Discussion"
author: "3D Laser"
---
I have an issue I need a bit of help with.  I finally said good buy to my k4O and upgraded to the k50 big blue laser.  I have some laser draw files that I want to use on my new machine.  Laser draw only lets me export bitmap images and I can not get them to cut in rd works.  Any ideas?





**"3D Laser"**

---
---
**1981therealfury** *June 13, 2017 18:06*

The best i have been able to come up with is to load the bitmaps into Inkscape and then use the trace bitmap tool, then save the file as a .DXF file in Inkscape which should then allow you to import it into RDWorks.  Sometimes i have issues with it deciding to skip little bits of the file i have converted though, and sometimes it works flawless, but i have not found out whats the trigger for it failing yet.   Most of the time i just create the file again from scratch where possible.


---
**3D Laser** *June 13, 2017 18:16*

**+1981therealfury**  I actually figured it out.  Rd works has an outline bitmap function.  I just import the bitmap it's already the right size.  Then choose it map handle then outline bitmap.  It works great and very easy


---
**1981therealfury** *June 14, 2017 13:15*

Ahh ok, thanks for the heads up, ill look into that :)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/E641pPoxTiJ) &mdash; content and formatting may not be reliable*
