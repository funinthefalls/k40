---
layout: post
title: "I have an SVG file that I seem to be having issues with"
date: May 08, 2016 12:52
category: "Software"
author: "Anthony Bolgar"
---
I have an SVG file that I seem to be having issues with. I am runniny Turnkey Tyrannys ramps 1.4 setup, and was hoping someone on here who is also running this could give the file a try so I can check my Gcode against yours for errors.

Thanks in advance



[https://drive.google.com/file/d/0B23r3Hp4r5U7NjJrM1o2dllzYjg/view?usp=sharing](https://drive.google.com/file/d/0B23r3Hp4r5U7NjJrM1o2dllzYjg/view?usp=sharing)





**"Anthony Bolgar"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 19:02*

Not sure what Turnkey is etc, however I just grabbed the file & opened in Adobe Illustrator. First thing I notice is that your text has changed font (from the image I see above). I would consider converting the text to outlines/paths for a starter. No idea how G-code works, but could that be the problem?


---
**Anthony Bolgar** *May 08, 2016 19:46*

It is the raster module in the Inkscape plugin that is giving me issues, vectors are OK


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 20:18*

**+Anthony Bolgar** Hope you can sort the issue. I don't use Inkscape or Turnkey/ramps etc, so have no other suggestions for you.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/6Wg8C2hkPGS) &mdash; content and formatting may not be reliable*
