---
layout: post
title: "So, on the K40 that my hackerspace got from FullSpectrum -- the UI had both power/speed, as well as an analog control on the front panel"
date: April 28, 2016 01:23
category: "Modification"
author: "ThantiK"
---
So, on the K40 that my hackerspace got from FullSpectrum -- the UI had both power/speed, as well as an analog control on the front panel.  If I wanted to do this, would I simply hook up the 5v PWM up to the other side of the analog dial?





**"ThantiK"**

---
---
**Alex Krause** *April 28, 2016 02:30*

What is the means of switching between the analog and digital controls 


---
**ThantiK** *April 28, 2016 03:25*

There isn't any switching.  It's in series so to speak. 


---
**Alex Krause** *April 28, 2016 03:38*

So to use the UI with the full range you would need to set the pot full open? Could you add a switch and a relay and run them parallel with a set of N/O contacts on the pot and N/C contact for the pwm for the UI? 


---
**ThantiK** *April 28, 2016 11:34*

I don't want to switch between them.  Having the dial allows me to get precise settings and adjust things while cutting/engraving. 


---
**Joe Alexander** *April 28, 2016 18:17*

I think the issue Alex is trying to point out is that if they are in series and the pot is at 50% then you might only get 50% of whatever setting you set in the UI. Without some circuit or switch to toggle control to either side both will limit the current. But it would give you potentially finer control over the output power.


---
**ThantiK** *April 28, 2016 21:03*

**+Joe**, that's exactly the point of what I'm trying to do.  So for example if I'm cutting card stock, and my power is too high - instead of aborting the entire thing and redoing settings, I just dial the power back while it's going.



This also means if I use a piece of software that's designed to vary the <i>power</i> to the laser, and not just pulse width, I can adjust it for darkness, etc.


---
**Alex Krause** *April 28, 2016 21:27*

**+ThantiK**​ sorry, I misunderstood what you were tryin to accomplish seems like a pretty awesome way to dial in a piece of work since materials like wood can vary in density from piece to piece and fine tuning my be required to get the results you need. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/TJg7ZsZcCpo) &mdash; content and formatting may not be reliable*
