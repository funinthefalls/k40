---
layout: post
title: "Hi I haven't purchased a k40 laser cutting machine but I am seriously thinking about it Can anyone please tell me how you get the cutting file to the laser cutter do you have to have your computer attached to the laser"
date: April 13, 2016 22:05
category: "Discussion"
author: "Donna Gray"
---
Hi I haven't purchased a k40 laser cutting machine but I am seriously thinking about it Can anyone please tell me how you get the cutting file to the laser cutter do you have to have your computer attached to the laser cutter? Also I have a program called SCAL4 which makes cutting files to use with a brother scan n cut machine will this program work with a laser cutter or do I have to have coral draw???





**"Donna Gray"**

---
---
**Gee Willikers** *April 13, 2016 22:12*

The machine will come with cutting software and a USB hardware lock. No other software will send files to it. I design in Adobe Illustrator myself, others use Corel, AutoCAD or Inkscape.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:22*

In addition to what Gee Willikers said, the k40 will connect to your computer via a USB cable to send the cut/engrave data to the k40.


---
**Donna Gray** *April 13, 2016 22:25*

OK but do you tell the machine to cut from the software program meaning computer would have to be linked with a USB cable?


---
**Gee Willikers** *April 13, 2016 22:26*

My DSP upgrade allows standalone use using a USB stick. I don't think any stock k40s offer that feature though.


---
**Gee Willikers** *April 13, 2016 22:28*

**+Donna Gray** yes, for a stock k40 you will need a computer connected to it via USB.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:29*

**+Donna Gray** Yeah, without plugging the computer in via USB, you cannot send the file to the machine. As Gee Willikers said again, with upgraded controller boards, there are other options for transferring files to machine (e.g. SD card, ethernet). But you're looking at some extra $ for that & having to pull the machine apart & make the modifications.


---
**Donna Gray** *April 13, 2016 22:43*

I have never used a laser cutter before Are they really easy to use? I make hand made cards and wedding invitations etc I would only be using it to cut up to 280gsm cardstock So with the program inkscape can you send cutting directions with that program as I have it on my computer


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:50*

**+Donna Gray** Yeah, a lot of people use Inkscape to draw the files, then they import them into the software that comes with the laser (Corel Draw 12 & a plugin called CorelLaser). You can't directly cut/engrave from Inkscape with the default software/hardware setup, but importing the file is a couple of second process.



The laser itself is reasonably easy to use. Basically choose your files, import into CorelLaser & then select the objects you want to cut/engrave, choose cut/engrave, set settings (speed, starting position, etc) & basically hit GO. From there, pretty reasonable.



Unfortunately, with the K40, the support & original setup instructions are basically non-existant, however this group was amazing for helping me learn what to do when I got started (in September 2015).



edit: additionally, for your 280gsm cardstock, it will cut it really easily. Very low power (like 4-6mA & very fast cutting speeds around 30-50mm/s). Only issue, is you will have slightly blackened edges. There is not a great deal you can do about that, so it may not be what you would like for the appearance of your cards.


---
**Donna Gray** *April 13, 2016 22:56*

I have a brother scan n cut machine but I find it struggles with very intricate designs that is why I thought a laser cutter would be better Even at $580 from ebay this laser cutter looks like it would do the job but it is a lot of money for me to outlay if it is not what I need It is just hard to get a lot of onfo about it that is why I thought I would ask the questions here before I jump in and purchase one


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 23:58*

**+Donna Gray** It would definitely cut what you want & do it with fairly reasonable precision, however intricacy is something that may be sacrificed due to the laser burning the material. If you wish to share an example of something intricate, I could perform a test on some 200 or 300 gsm cardstock I have (can't remember which) when I have some time & show you the results to assist in determining if you want to go ahead with the purchase.


---
**Donna Gray** *April 14, 2016 00:02*

How do I get a picture to come up in comments I just tried copy and paste and it didn't work


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 00:08*

You can't put a picture in specifically, however you can put a link to the picture into comments. Only other way is to create a whole new post on your main page & tag people into it by using **+Yuusuf Sallahuddin** for eg.


---
**Jim Hatch** *April 14, 2016 00:15*

**+Donna Gray**​ Depending on where you live, there may be a Makerspace near you that has a laser cutter. They'll likely be a bigger 60-100w machine but they could show you what's possible. Those machines are simply bigger versions of the K40 for your purposes (just a matter of how thick materials they can cut vs the K40 and paper/cardstock is nothing to worry about with the K40).


---
**Donna Gray** *April 14, 2016 01:24*

I just uploaded a photo under the discussion section


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 01:40*

**+Donna Gray** In future, maybe avoid that on this K40 group, as we try to keep posts to things of interest to everyone. As this is a more personal interest (i.e. testing this file), it is better to post privately to whoever or publicly on your profile page. Just for future reference.


---
**Donna Gray** *April 14, 2016 02:29*

ok sorry new to this I didn't know I did anything wrong


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 02:34*

**+Donna Gray** That's fine, just sometimes we get too many posts & makes it difficult for people to find stuff they are looking for. I have posted a few photos on my profile of the test cut, so you can see how well it cuts & the effect on the edges I was mentioning.


---
**Donna Gray** *April 14, 2016 03:03*

how do I get to your profile???


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 03:05*

Click on my name in the comment. Or click the Orange moon & star logo of mine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 03:07*

**+Donna Gray** alternatively here is the link to the post

[https://plus.google.com/+YuusufSallahuddinYSCreations/posts/aZ3WrzvHNbg](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/aZ3WrzvHNbg)


---
**Jim Hatch** *April 14, 2016 12:06*

**+Donna Gray**​ here's a post by Norman with a nice fairly intricate cut.



[https://plus.google.com/103798744920995011573/posts/YqDg7dqbtAR](https://plus.google.com/103798744920995011573/posts/YqDg7dqbtAR)




---
**Coherent** *April 17, 2016 14:46*

I use SCAL 4 with a vinyl cutter, I don't think it will work with laser cutters. I've used both the laser cutter and the vinyl cutter on card stock and in my opinion the vinyl cutter does a better job. The edges are cleaner and sharper corners. If my purpose was  to cut invitations and such I'd choose a vinyl cutter with the card stock on a carrier. Of course there are things you can do with a laser that you can't with a vinyl cutter. If you could figure out SCAL and your scan and cut, you'll have no issues with a laser cutter and it's software. The learning curve is similar.


---
**Donna Gray** *April 17, 2016 20:12*

Thanks for your comment Coherent Do you think I could design in Scal4 and then transfer my file into the software program that comes with the laser cutter As someone mentioned that you can design in inkscape and then copy file into the corellaser program


---
**Donna Gray** *April 17, 2016 20:12*

**+Coherent**

Thanks for your comment Coherent Do you think I could design in Scal4 and then transfer my file into the software program that comes with the laser cutter As someone mentioned that you can design in inkscape and then copy file into the corellaser program﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 17, 2016 22:29*

I'm not familiar with SCAL4. I just did a google to see what it is & it looks like you can output the file as a SVG when you save it? Is this correct? If so, then you can definitely import that into CorelLaser


---
**Donna Gray** *April 17, 2016 22:58*

Yes it can save file as svg What file formats does the corelLaser program support I can save as svg fcm bitmap jpeg PNG & TIFF format in Scal4???


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 17, 2016 23:32*

SVG or PNG are the ones you will want to use, as it is a vector format. Meaning it is perfect for cutting. You can also use it for engraving (as I do) but the software converts it to Raster before engraving it anyway.



Raster formats like BMP & JPG & TIFF are suitable for engraving. You can use them for cutting also, however vector is better for cutting.



I will take a printscreen of all the formats that are able to be imported into CorelLaser & post the link here.


---
**Donna Gray** *April 17, 2016 23:45*

Ok thanks In my program I can take a photo then in my program I can trace the image and modify it then save as the file formats I mentioned in my last message I am used to designing things in this program But I have had to delete a lot of detail as my cutting machine could not cope with very small circles as it uses a blade to cut so I was thinking the laser my cut the smaller circles etc better


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 17, 2016 23:49*

**+Donna Gray** Here is a screenshot (or a bunch combined together) to show all the formats that are available for importing into CorelDraw/CorelLaser software that comes with the K40.

[https://drive.google.com/file/d/0Bzi2h1k_udXwQ2JtbmlES3lnTFE/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwQ2JtbmlES3lnTFE/view?usp=sharing)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 17, 2016 23:59*

**+Donna Gray** See my above post for file formats screenshot. In regards to the cutting small circles, the smallest I have been able to cut with the laser is 0.5mm diameter circles. But it just looks like a burned hole/dot. I use 0.5mm circles quite frequently to cut stitching holes for my leatherwork (because I seriously hate punching them manually by hand). Works quite well & gives precision for the spacing. Only downside is the burn/char on it. Alex Krause (in this group) has mentioned to me about a thing called a drag-knife. It seems probably similar to the bladed setup on your cutting machine, but may be able to handle more precision. You may find something like that more suitable for your needs (due to the lack of charred edges). I plan on eventually modifying my K40 to include a drag-knife, for cutting purposes. Rather than lasering & having charred edges, will be smooth edges cut by blade.



Here is a video on the drag-knife in action:


{% include youtubePlayer.html id="YqvgA1P5hWg" %}
[https://www.youtube.com/watch?v=YqvgA1P5hWg&ab_channel=BrianOltrogge](https://www.youtube.com/watch?v=YqvgA1P5hWg&ab_channel=BrianOltrogge)



(Skip to 8:40, prior to that he is just showing how he made the drag-knife attachment)


---
**Coherent** *April 18, 2016 12:40*

Looks like your question's been answered, but SCAL is about the worst drawing/cad program I have used. It's fine for basics but not very user friendly and limited as far as what you can do with it. Corel Draw is much more powerful and a bit older version comes with most of the smaller laser machines. The learning curve is a bit steeper than SCAL, but once you  practice with it there isn't much you can't accomplish easily.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2016 13:22*

**+Donna Gray** I just remembered that I have the software for the K40 uploaded to my google drive. If you are interested in downloading it & having a look at it & play around, you can grab it from here:

[https://drive.google.com/file/d/0Bzi2h1k_udXwamRBZkduekxlMHM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwamRBZkduekxlMHM/view?usp=sharing)
 (~700mb).



note: some users have mentioned that the software they received (which is the same as mine) flags as a virus with some antivirus software. Not sure if it actually is though, as I've had no issues. I think it's just because the version of CorelDraw they give is a pirated version.



It's a bit of a bother to install it all (as lots of stuff in chinese with minimal explanations), however most of us here have had experience installing it & could give pointers. Basically it is just CorelDraw12, then you install the CorelLaser plugin afterwards. 


---
*Imported from [Google+](https://plus.google.com/103145403582371195560/posts/b7hb5ab4kZj) &mdash; content and formatting may not be reliable*
