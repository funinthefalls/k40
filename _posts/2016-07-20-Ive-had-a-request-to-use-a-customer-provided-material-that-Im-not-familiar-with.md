---
layout: post
title: "I've had a request to use a customer provided material that I'm not familiar with"
date: July 20, 2016 14:36
category: "Materials and settings"
author: "Carl Fisher"
---
I've had a request to use a customer provided material that I'm not familiar with. Does anyone have any thoughts based on the following information?



He's going to bring me a piece to test with but he's basically looking for cutting, not engraving.



This is from the MSDS



Common / Product Name: Fiber Reinforced Plastic (FRP) Unisub



The manufacturer lists no ingredients as hazardous according to OSHA 29 CFR

1910.1200.



Composition comments: Panels are solid sheets composed of a mixture of polyester

resin, inorganic fillers, pigments, processing additives, and fiberglass reinforcement.

During the manufacturing process, this mixture is cured or hardened into a stable, solid

material that is non-hazardous when handled or processed in accordance with good

manufacturing and industrial hygiene practices.

Surface finishes are factory applied. 



These products are classified as an “article”

according to 29 CRF 1910.1200(c). They do not release any hazardous chemical under

normal conditions of use.





**"Carl Fisher"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 20, 2016 14:40*

It's got resin inside it.  That's what makes MDF smell bad when you cut it: you are essentially melting the glue and it gets into the air. 



Tread carefully. 


---
**Scott Marshall** *July 20, 2016 14:51*

It's fiberglass pressed into a panel, As Ray says, very nasty fumes, and it doesn't cut or engrave well at all. It's 85+% glassfiber, and we know how well glass cuts with a laser.



"Normal use" doesn't include laser cutting, it mean sanding, sawing etc.

The binder is polyester resin, similar to nylon but created with a crosslinking catalytic reaction (MEKP is the catalyst).



If you want to do a dry run, get a piece of FR4 fiberglass printed circuit board, it's essentially the same stuff, turn the fan on high and have a try. Don't expect a lot. I suspect your customer thinks a laser is a magic beam that cuts through anything (a common misconception) and didn't research the material. It's commonly cut with a waterjet, frequently one with abrasive loading. A diamond saw is the next most popular method.



Good luck, Scott


---
**Pippins McGee** *July 22, 2016 06:20*

**+Scott Marshall** nasty fumes because of the resin you mention is used in mdf?

i breath mdf smoke everyday - even with 270cfm fan - (smoke swirls around inside machine before being sucked out -ie. some seeps out into room and into my young lungs) should I be concerned?



couldnt find anything on the net about mdf smoke being toxic or not.



sorry to hijack. but scotts a boss and knows everything about everything.


---
**Scott Marshall** *July 22, 2016 14:14*

**+Pippins McGee**

No, it's not the same resin that is used in MDF, it's closer to what they make boat hulls from.



MDF still isn't something you want to breathe though. It usually uses a  urea based resin. It's nasty in it's own right, but not so "choke you right now" as the polyester stuff. Don't be fooled by it's tolerable odor, it is indeed bad stuff to breathe.



I'd advise getting a good exhaust system going or at least use a NIOSH approved respirator  while cutting MDF until you can get a good exhaust set up.



Didn't you just get done building a new exhaust system?



If you're still getting fumes out of the laser cabinet, you may need to provide make-up air. That's usually a duct drawing outdoor air into the laser to replace the air your're drawing out. If there's not any make up air and you have a powerful fan, you'll actually pull a slight vacuum on your building/room. Sometimes the fix is as easy as cracking a window in your laser room.



Play with it and see what it takes to fix the problem. I'ts possible the boat blower isn't adequate. They aren't really designed for the application, they are used to draw fuel vapors out of a small (16cu ft or so) engine compartment with very short duct runs. They come in many sizes too. 



I real good blower (120Vac) will cost you about 80 bucks and is worth every cent.



Scott


---
**Carl Fisher** *July 26, 2016 16:41*

So the end result was that I could get engraving depth but not a chance on cutting. 



Like I said, I didn't have high hopes but since this customer is also a friend I had to at least give it the effort. Respirator on and I have a relatively strong blower pulling the fumes and dumping them outside so all is well.



Thanks for the feedback.


---
**Scott Marshall** *July 26, 2016 22:18*

Nothing ventured...


---
**Pippins McGee** *July 27, 2016 02:40*

**+Scott Marshall** 

thank's scott.. i must stop breathing this smoke then.

yes I seem to find that with the lid opened a fair bit, the air is sucked out the back much quicker.

with it closed, it essentially whirlwinds around inside - allowing it to seep out many exits.



I think i will cut holes in the front panel and install some PC fans to 1) provide more intake air and 2) give the air direction towards back of the machine



thanks


---
**Scott Marshall** *July 27, 2016 02:43*

Good idea on the fans., it will probably help reduce smoke deposits on wood too.


---
**Ray Kholodovsky (Cohesion3D)** *July 27, 2016 02:55*

Agreed. There was a post in this group very recently with someone who did the PC fans on the front thing. Other option, or do both, is to get a stronger exhaust fan instead of the super weak included one. 


---
**Carl Fisher** *July 27, 2016 13:29*

As for what the material was like in person, it was basically a fiberglass flat panel with a smooth coat similar to a gel coat on both sides that they do sublimation on. So what was happening is that the laser would mark the top smooth coat with no issues at all but once it hit the reinforced layers it was stopped in it's tracks and never went any deeper. Adding more power simply caused it to char.



For the MDF discussion, MDF is nasty stuff in general. I wear a respirator when cutting it as the dust it puts off has been touted as pretty nasty stuff to breath. I used to work with it pretty heavily in custom audio fabrication and a respirator was a must. But it's definitely not the same as the poly resin or epoxy resin used in composite materials.


---
**Scott Marshall** *July 27, 2016 16:09*

The resin used in MDF is a toned down (but still the same chemical) version of the urea formaldehyde resin that caused a big stir about 15 or 20 years ago because people were getting sick in newly built homes. The culprit there was particle board and OSB (oriented strand board). They've since re-formulated the glue to release less formaldehyde gas and cure it more before shipping.



The appeal of this resin is that besides being very strong and economical, it will sit for hours (even days) in a plywood lay-up or particleboard/mdf mixture, but can be cured in a few minutes with a microwave curing system.



As you pointed out Carl, even the dust is nasty stuff and deserves more respect than it seems like it deserves. It seems like plain old fine wood dust, but carries the binder (the UF Resin) with it right into your lungs. Burning it does the same thing.



Years ago I used to "not make a big deal" about spraying cars and handling chemicals ("you're only immortal for a limited time"- Gold star if you can tell me who said that, but hurry).

The rule used to be "If you can smell it, it's causing harm"

Then a fellow who I used to race with died from spraying  Isocyanate hardened auto paint which was quite new at the time. (son of a body shop owner). You can't smell Isocynates, but they will kill you.



Anyway since, I'm a fanatic about quality respirators, Heavy duty exhaust fans, interlock switches etc. You can't buy new a new body, trust me I know.



Sorry to run on about safety, it's a boring, downer kind of subject, but Laser art shouldn't be fatal, and there's many ways the little blue box can hurt or kill you. Just like the innocent appearing sawdust, nothing SEEMS real dangerous there, but you have un guarded laser light waiting to blind you, exposed unguarded mains voltage, exposed unguarded 15kv, and of course most of us will try lasering almost anything once. Many seemingly safe material release some very nasty byproducts when lasered.



Sorry again for repeating warnings you probably have heard before, but I constantly field questions from people running lasers vented into the room, poking around the high voltage section with a meter rated at 600v and so on. SOME new users don't do any real research on what they just bought.



Every now and then a crafter buys K40 expecting a Cricut that cuts all sorts of new stuff expecting to plug it and and go. They show up here, usually a couple of times a year. I worry about the ones that don't find our community. Better bad news than serious injury, fire or worse.



 As nice as the K40 is, it's analogous to selling a car with no seatbelts, emergency brake or door latches. That leaks gas.



It CAN be made into a safe, useful tool, but used out of the box, it's asking for trouble.



Scott


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/BbBsj6dfYET) &mdash; content and formatting may not be reliable*
