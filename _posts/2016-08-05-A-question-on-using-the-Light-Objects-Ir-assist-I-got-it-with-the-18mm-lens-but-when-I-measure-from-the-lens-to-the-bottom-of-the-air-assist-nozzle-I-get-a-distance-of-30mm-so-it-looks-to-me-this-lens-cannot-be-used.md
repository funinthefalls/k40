---
layout: post
title: "A question on using the Light Objects Ir assist, I got it with the 18mm lens - but when I measure from the lens to the bottom of the air assist nozzle - I get a distance of 30mm - so it looks to me this lens cannot be used -"
date: August 05, 2016 19:39
category: "Discussion"
author: "greg greene"
---
A question on using the Light Objects Ir assist, I got it with the 18mm lens - but when I measure from the lens to the bottom of the air assist nozzle - I get a distance of 30mm - so it looks to me this lens cannot be used - I'd have to get a lens with a longer focal point.  Anyone else found this - or - not having good results using this combo for the same reason?



Also - I've put some 12V led strips in the machine - is there a 12V tap on the poser supple - or do I need a separate 12V supply?





Thanks all !





**"greg greene"**

---
---
**greg greene** *August 05, 2016 19:47*

Found the data for the lens - it is 50MM - same as stock 


---
**Anthony Bolgar** *August 05, 2016 19:51*

I use a separate PSU for the lights, fan, and waterpump on mine. The lens /air assist combo works great, just make sure to remeber that the distance from the end of the nozzle to the work piece is different than the stock head.


---
**Jim Hatch** *August 05, 2016 20:24*

And remember you're measuring the 50mm from the <b>lens</b> not the nozzle tip.


---
**Bart Libert** *August 05, 2016 20:57*

never tap extra power from the PSU. Use a cheap extra one for anything else like led lighting




---
**Bart Libert** *August 05, 2016 20:57*

greg, is it 50mm or 50.8mm ?




---
**Anthony Bolgar** *August 05, 2016 21:01*

All standard lenses sold are 50.8mm

 from the center of the lens to the workpiece.


---
**Bart Libert** *August 05, 2016 21:09*

thats why I asked it. So if you use the lightobjects air assist laser head with a 50.8mm lens, then hoem much space is there aproximately between the bottom of the laser nozzle and the place where your beam is focussed to the smallest point? I know it is 50.8mm from the lens, but I don't know how far the lens is in the laser head.


---
**Anthony Bolgar** *August 05, 2016 21:13*

On mine it is approximately 20.3mm (I have seen others at 20.8, and 21.2 Best to do a ramp test (If not sure what that is search this community for the info on how to do it). Then you will know exactly and can make a small spacer block to speed up focusing to the workpiece.


---
**Jim Hatch** *August 05, 2016 21:14*

**+Bart Libert** Just measure the 50.8mm from the bottom of the knurled ring that is above the cone portion of the nozzle but under the silver mounting plate. That's the bottom of the lens. Once you get your bed/material at the right place you can measure the space between the nozzle bottom and the material and use that as a quick measurement. I use a 50.8mm stick I cut and I sometimes make it up above the bottom of the ring because I want to adjust the bed in the middle of the material instead of the surface so a fixed block size under the nozzle while quick won't work for me. I can easily adjust it up an 1/8" if I'm doing 1/4" material.


---
**Bart Libert** *August 05, 2016 21:33*

Anthony, I know what a ramp test is. I built my own 80W cutter from pieces. I just want to learn a little more about this lightobjects head to design a new 40W cutter I want to make. That is why those distances matter because I need them to design the gantry system + the motorised up/down table BEFORE firing any beam at all.


---
**Bart Libert** *August 05, 2016 21:35*

Jim, actually I have another question for you too. Can you tell me how that head is mounted onto the gantry ? If I look at pictures of spare gantry, then I see the hole where the head should fit, but I don't see any thread on that hole. Do you just unscrew the head's top part from the bottom and then screw it back together so that you actually friction fit it into that hole ?




---
**Anthony Bolgar** *August 05, 2016 22:07*

OK, the one thing I find with my LO head is I need to use a fairly high PSI for it to be real effective (around 30psi is my setting, running a small compressor with an air tank)


---
**greg greene** *August 06, 2016 13:30*

Thanks to all !


---
**Jim Hatch** *August 06, 2016 14:20*

**+Bart Libert**​ The LO head is a 4 part assembly but it seems like it's only 3. The nozzle cone is a slip on fit with a setscrew on the side that holds it in place just under the knurled ring. The top piece screws onto the knurled ring section over a threaded fitting sized to fit the stock K40 mounting plate.



The knurled ring piece remaining actually is 2 parts - the threaded section and the ring section. That is where the lens goes. The threaded section is basically 2 different diameter threaded ends with no real tube or anything in between. The top part is the smaller diameter to go into the mounting plate hole, the bottom thread section is substantially larger and fits into the knurled ring section leaving room for the lens to be sandwiched in between. Since the LO lens is bigger (18mm vs 12mm) than the stock lens that piece has to be larger than the piece that sticks up through the mounting plate to secure it to the top mirror portion which just screws on and holds by sandwiching the mounting plate in between the bottom of the head with the lens and the top with the mirror.



Lots of people find that one or the other ends unscrew from the threaded piece fairly easily. If it's the top part then they see how that fits into the mounting plate and holds on. But then they can't figure out how to get the lens in.



If it's the bottom large screw thread section that comes apart easily then they see where to put the lens but think they need to drill out the hole on the mounting plate because the threaded section is too big.



You need to use pliers with a cloth wrapped around whichever threaded piece is exposed and <b>light</b> pressure to hold the threaded part while you unscrew either the top or the knurled ring to get it all apart.



And then you see how it all fits together and the angels sing 🙂


---
**Bart Libert** *August 06, 2016 14:57*

**+Jim Hatch** I was going to reply to you (thanks for that info by the way) with a picture, but it looks like I cannot add a picture here. Do you know how I could make a picture visible here ?


---
**Jim Hatch** *August 06, 2016 17:56*

**+Bart Libert**​ you can only add pictures when you're first creating a topic. After that they have to be links to Dropbox or other picture hosting site.


---
**Bart Libert** *August 06, 2016 18:11*

OK, I posted my picture here. [https://s8.postimg.org/vxmfii5ad/lohead.jpg](https://s8.postimg.org/vxmfii5ad/lohead.jpg) So if I understand well (English is not my native language), then the nozzle cone tip is just sliding a little up and down a small cilindric piece just below the knurled ring and it is held there with a set screw. The lens is somewhere inside the knurled ring and the piece marked with "knurled ring threaded section" is actually making sure the lens stays in place because it is sandwiched between the knurled ring piece and the threaded piece. The top piece rcrews in too. So, am I right to say that the "mounting plate hole" on the K40 laser is the same diameter as the outer diameter of the smallest threads on the threaded section ? And then you just put the assembly (without the top piece) from the bottom up through the hole and by screwing the top piece onto the assembly, you  actually screw in untill the head is tighteinng around the hole on top and bottom ? (like if you would take a screw and a bolt to mount in a hole ?). So it is the responsability of the person who mounts to make sure that the window through which the laser beam enters the head is really correctly oriented and you have to make sure you have enough pressure on the screws so your hear doesn't come loose ? Forgive me the maybe stupid questions, but I'm more use to this kind of laser head in my machine. [https://s8.postimg.org/me2x93q0l/s_l1600.jpg](https://s8.postimg.org/me2x93q0l/s_l1600.jpg)


---
**Jim Hatch** *August 06, 2016 19:24*

**+Bart Libert**​​ Okay. You have the other version of the LO head. Yours is adjustable I think. The tube that goes into the square section can go up & down and is held by that screw coming out of the side of the square piece. 



The thin square piece I believe unscrews from the big square piece. The thin one goes under the mount and the big one goes on top of the mounting plate. That sandwiches the halves together and holds it to the mounting plate.



The knurled ring is where the lens goes so can be unscrewed. 



Because the long tube between the ring and the square section is just held in place by the screw you can slide it up or down to get the clearance you need for different materials. That's easier than shimming the bed up & down. It does have limited range but if it's like the Saite head it appears to be modeled after it's enough adjustability for 3/4" difference in material thickness. That tube should slide up inside the mounting plate hole.



This is the one I have. [http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx) 



It's the one most people get. Yours is the upgraded "pro" model I believe. It's not listed in their K40 section though so it might not be sized for your mounting plate so you may need to fashion another one or drill the hole out larger like you need to do with the Saite head.



Yours looks to be the Cole head. [http://www.cncoletech.com/Laser%20Head.html](http://www.cncoletech.com/Laser%20Head.html) 



The pics there are pretty clear as to how the parts all fit together.




---
**Bart Libert** *August 06, 2016 20:05*

Yes, the PRO head I use now is on a self built 80W machine. But I'm looking to build a new 40W machine too and for that one I'm now looking at the  smaller head and not the Cole head (actually, my laser parts for the 80W cutter all came from cncoletech and it uses a 80W RECI tube. Top quality to be honest). Only that pro head is a little big to put in a 40W machine ;)


---
**Jim Hatch** *August 06, 2016 20:36*

Okay. So your post 2h ago was spot on. You're right about everything you wrote. The threaded piece above the knurled ring will fit up through the mounting plate hole and the top piece with the mirror screws on like a nut.



You align the hole to face the mirror on the left side (Y axis gantry). Then slip the nozzle section on & tighten the screw. That lets you get the air hose where you want it for routing.



When you align it you twist the whole head to get the head's mirror lined up correctly. 



Since you've built a bigger one you know about possibly needing to shim the mounting plate screws so you get the beam going into the head mirror parallel to the tube and perpendicular to the Y-axis so it comes out the center of the nozzle. If they're misaligned the beam can hit the nozzle and not your material.


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/QXgT3Yuhpuv) &mdash; content and formatting may not be reliable*
