---
layout: post
title: "So my K40 has a 6c6879 controller board"
date: November 23, 2014 19:11
category: "Discussion"
author: "Henner Zeller"
---
So  my K40 has a 6c6879 controller board. And I want to operate my laser cutter with Linux.



Since I don't have Windows I can't run whatever software they deliver with it (Also, I don't have Corel Draw what they require). Does anybody know what protocol this device expects on the USB input ? Is it simply GCode or HP-GL ? If so, then it would be simple to use on Linux.



Looks like there is some USB to serial chip on that board, so if someone has a Windows machine to send stuff to the printer, it should be possible to hook up a passive protocol sniffing just with a serial input, Bus Pirate or similar to watch the protocol. I haven't found yet any reverse-engineering 'on the internet', but maybe someone already did ? (In the image, I marked the Rx/Tx lines to sniff on).



If it is some proprietary protocol, then it should be easy to reverse-engineer and possible to convert whatever polygon/SVG/... input to that; maybe I'll replace it with BeagleG/Bumps long term anyway, but would be good to get these machines running out of the box on Linux.

![images/bb80ee8dcd9c2dec2b6695055e71ee9e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb80ee8dcd9c2dec2b6695055e71ee9e.jpeg)



**"Henner Zeller"**

---
---
**Anton Fosselius** *November 23, 2014 20:29*

I have the same problem. I think the communication is encrypted. I have been thinking of sniffing the raw data sent when manually moving the laser from the software and then write a linux wrapper to operate it


---
**Henner Zeller** *November 23, 2014 20:52*

The nice thing is, that it is just a simple serial communication, so it should be easy to sniff just with a simple serial interface line (as opposed to needing a full USB protocol sniffer).



I don't think they do encryption or something strange - this is just a simple microcontroller (I didn't find any information on that, seems to be some Chinese brand). I suspect they just have a simple ASCII protocol (such as GCode or HP-GL or similar), or a very simple binary representation of the commands - should not be a big deal to figure out.



I don't have Windows or Corel Draw so for me it would be a huge hassle to install the host-software just to do the reverse-engineering. If someone could do that and post what they find 'on the wire' when moving the head around, doing homing, switching on laser and so on, we could figure it out.


---
**Anton Fosselius** *November 23, 2014 20:57*

I have windows on an old laptop. Have been thinking about sniffing on the comunication for about a year, but have not come around to do it.


---
**Muccaneer von München** *November 24, 2014 10:19*

**+Henner Zeller**  Maybe our **+Mr Beam** Shield is an option ?

**+Stephane Buisson** has asked me about this, but I do not have a K40 to try that out.

The shield basically combines 3 stepper axes (pololu drivers) with a laser diode driver. If you don't need the laser diode driver, there is a pwm output for the laser intensity as well. There you probably could attach your co2 tube. The Arduino under the shield runs our customized fork of grbl which can be feeded with gcode via Universal GCode Sender. So far everything is platform independent. On our machines we serve a web-interface with a raspberry pi in addition to make them accessible via tablet as well. 

Checkout [http://shop.mr-beam.org/product/mr-beam-shield](http://shop.mr-beam.org/product/mr-beam-shield) for more details.


---
**Anton Fosselius** *November 24, 2014 12:24*

**+Muccaneer von München**  looks nice :D does it support engraving?


---
**Muccaneer von München** *November 24, 2014 14:23*

**+Anton Fosselius** With laser diodes: yes. See [https://www.kickstarter.com/projects/mrbeam/mr-beam-a-portable-laser-cutter-and-engraver-kit/posts/855653](https://www.kickstarter.com/projects/mrbeam/mr-beam-a-portable-laser-cutter-and-engraver-kit/posts/855653)

That was done with a common "M140" laser diode


---
**Stephane Buisson** *November 24, 2014 17:31*

reverse-engineering could be fastidious, for limited results (hardware specs), is it worth it ?


---
**Henner Zeller** *November 24, 2014 17:40*

Would be fun to get it to run out of the box, but yeah, it is simpler to use BeagleG/Bumps or other stepper controller boards.


---
**Anton Fosselius** *November 24, 2014 20:03*

**+Stephane Buisson** I will do it some time just for the lolz and because i can. But the limiting factor right now is time, got two small ones to take care of.


---
*Imported from [Google+](https://plus.google.com/+HennerZeller/posts/VHkMjY79EkF) &mdash; content and formatting may not be reliable*
