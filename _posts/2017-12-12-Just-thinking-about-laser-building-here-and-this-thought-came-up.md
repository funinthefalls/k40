---
layout: post
title: "Just thinking about laser building here and this thought came up"
date: December 12, 2017 16:45
category: "Discussion"
author: "Steve Clark"
---
Just thinking about laser building here and this thought came up. Is there any reason that you could not  mount a tube vertically? It would give you more room to build your table. As a side note to this, I would suspect it would help in keeping the air pocket out of the tube. The first mirror would also have to be flipped 90 degrees also.





**"Steve Clark"**

---
---
**Phillip Conroy** *December 12, 2017 19:14*

Ask a tube maker v this question, I gave only seen a tube used vertical once


---
**Don Kleinschnitz Jr.** *December 12, 2017 19:18*

Here is a vertical large format:


{% include youtubePlayer.html id="136HqrXOMpw" %}
[youtube.com - 80 Watt Vertical Laser XL 4'x8' Laser Cutter and Engraver](https://www.youtube.com/watch?v=136HqrXOMpw)



Thought about a 40-60 watt version like this?


---
**Adrian Godwin** *December 12, 2017 22:00*

I read somewhere that the bottom mirror in the tube gets dirty.




---
**HalfNormal** *December 13, 2017 17:32*

It is all about room and safety as in not damaging the tube that could be possibly exposed.


---
**Steve Clark** *December 13, 2017 18:46*

It be in a hard housing just vertical.  i.e. an aluminum sqr tube . 


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/iFRgB2vYKp4) &mdash; content and formatting may not be reliable*
