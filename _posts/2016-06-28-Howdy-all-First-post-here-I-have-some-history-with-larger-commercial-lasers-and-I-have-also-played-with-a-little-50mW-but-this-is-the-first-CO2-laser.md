---
layout: post
title: "Howdy all. First post here. I have some history with larger commercial lasers and I have also played with a little 50mW but this is the first CO2 laser"
date: June 28, 2016 19:31
category: "Air Assist"
author: "Gary Hamilton"
---
Howdy all.  First post here.



I have some history with larger commercial lasers and I have also played with a little 50mW but this is the first CO2 laser.  I have built a few things with RAMPS and will probably move that direction pretty quickly, I have an older board that I was going to put on a router that I may use for this.  



ANYWAY, if you read all of that.  Is there a site that consolidates all of the different types of upgrades?  I was looking for some info on air assist parts.  I have an air compressor I can tie in but I have not see PSI and CFMs that people are using.  Any recommendations would be helpful.





**"Gary Hamilton"**

---
---
**Gary Hamilton** *June 28, 2016 19:33*

Sorry just found the Air Assist area.  Not overly familiar with Google+


---
**Stephane Buisson** *June 28, 2016 19:51*

welcome here **+Gary Hamilton** 

output really depend on the nozzle size, good amount of air but not too much pressure is good. the main idea is to carry smoke away to keep your lens clean for good performance.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 20:08*

Also adding extra oxygen to the cut area assists with the cutting power. For e.g. prior to air-assist you may require 10mA @ say 10mm/s to cut something. With air-assist you may end up with a better cutting result like 10mA @ say 15mm/s.



I'm using my own designed head currently, with 2 low airflow aquarium pumps. The airflow would be fine if it was funnelled into a tiny outlet like most use, but my design is a larger output (focused into a cone) so requires a much larger airflow to retain decent pressure/volume of air to the cut.


---
**Gary Hamilton** *June 28, 2016 20:12*

Thanks for quick comments all.  **+Yuusuf Sallahuddin** I understand the Oxygen will make it burn with less energy but don't you also lose some control (resolution)?  And depending on medium wouldn't you increase the risk of fire?




---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 20:19*

**+Gary Hamilton** I've not noticed any loss of control/resolution in anything I've done. It could be the case (I can't really say) but not something I've experienced at least.



In regards to risk of fire, strangely it seems to be the opposite. What you say makes sense, but it just seems to not be the case. Without air-assist, your jobs are prone to flare-ups (small flames) appearing at the cut point. These result in soot/smearing along the edge of the cut (usually in the direction of the exhaust as it sucks the smoke that way). With air-assist active you will find that you end up with no flare-ups as it blows the flames out quicker than they start.



edit: in regards to the control/resolution I think the main thing determining your resolution is your focal point (50.8mm for the stock lens) & secondly your power level. I tend to use 4mA for everything I do, as at higher mA values I find that the beam is wider. Albeit a very small difference, but one that seems noticeable to me. Also, if you defocus your machine (i.e. material not at the focal point) you will find that you definitely lose resolution. I've read that some people here do it on purpose to "soften" the edges of their engrave.


---
**Phillip Conroy** *June 28, 2016 20:23*

i use full size shop compressor regulated down tp 10psi at the laser cutter when cutting 3mm mdf,and 2psi when engravieing .When  i first set up the air assist i had problems with water on the foocal lens and had to clean the lens every 20mim of cutting.i fixed the water problem by having the first 10 meters of airline at a angle so that when the air cooled a bit in this first 10 meters the moisture condensed out of the air and runs back to the compressors air tank,which has a auto drain valve fitted on the drain tap[set to drain the air tank for 2 seconds every 45 min of power to the air compressor.i have 2 water trap/air pressure regulators fitted at the laser cutter which catch 99.5% of the water that has made it that far.i also fitted a 1inch wide by 1 meter long pvc pipe filled with silca gel to trap the last 0.5% of the water in the air.


---
**Ned Hill** *June 28, 2016 20:56*

I'm using a light objects air assist laser head made for the K40.  [http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx) With this head I find that 15 lpm is plenty sufficient for most things so far.


---
**dstevens lv** *June 28, 2016 21:05*

Phillip's air settings match what I was using from the shop air.  I have a Motorguard M60 water filter/trap ( [http://motorguard.com/air_2_2.html](http://motorguard.com/air_2_2.html) )



I've since moved the machine and didn't have a drop in that area.  I started using the small pump that came with the machine.  It works well so I didn't plumb in another drop.  Mine is a 6040, more or less entry level pro and not a K40 but the basic principles remain the same.



Air assist helps prevent fires as it minimizes the blow back.  I've had a fire but that was due to the backing paper and adhesive of a polycarbonate sheet quickly igniting.  I use air assist on everything I do with the machine.


---
**Gary Hamilton** *June 28, 2016 21:44*

**+Phillip Conroy** Thanks for that detail.  I did not even think about a better water trap.  Shop air has a basic dryer/oiler system that probably need to be separated.  I have a smaller compressor that I may use if it only needs 10PSI.  Very useful info.



**+dstevens lv** thanks for the comment and the link  

**+Ned Hill** thank for the link

**+Yuusuf Sallahuddin**  The part of your comment that concerned me was the word "oxygen" I thought you might have been using bottle oxygen rather than just compressed air.  I think adding compressed air improves resolution but I was worried that compressed oxygen may create more of a burn.  I think now it may have just been a terminology misunderstanding on my part.



Thanks all for the active (and quick) comments.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 22:16*

**+Gary Hamilton** Oh yeah, I was just referring to "air" I suppose. I imagine compressed oxygen could cause major burn.


---
**dstevens lv** *June 28, 2016 22:36*

You're absolutely going to need to feed it from a drop that doesn't have an oiler.  Oil and/or water will be a big negative impact on your work.  Use the same fundamentals you would if it was a spray booth or a plasma table.  Your 10 psi small guy should work well.


---
**Jim Hatch** *June 28, 2016 22:57*

**+Gary Hamilton**​ Don't stress too much about the water trap. **+Phillip Conroy**​ seems to be the only one here who has had the level of water issue he encountered. It could be due to his hardware or even his locale. The rest of us just push air into the head 🙂


---
**dstevens lv** *June 28, 2016 23:19*

**+Jim Hatch**

You guys may not be seeing a condensation issue as you aren't using a traditional shop air system.   However, if you have a larger, more powerful compressor and tank (I have a 60 gal, 3.5 hp and that's still pretty small) and it's plumbed throughout your shop there will absolutely be an issue with condensation.   Most hobby users don't have that.  Regardless, Gary has an oiler on the line and that's got to go.  There will be oil in his air with that device because that is what it does to lube your pneumatic tools.


---
**Gary Hamilton** *June 28, 2016 23:22*

Yeah, I will use my smaller compressor.  I have a few different line filters. I should be able to use the one for paint and be fine.  I will use a traditional shop air since I have it available and cleaning the air is a good practice even if not needed.


---
**Jim Hatch** *June 29, 2016 00:29*

**+dstevens lv**​ you're right the oil will be bad - residue on the material. I wonder if it would inject enough oil to catch fire or feed the fire at the laser burn point.


---
**Stephane Buisson** *June 30, 2016 10:14*

more condensation come with more pressure, not really an advantage in our case.


---
**Phillip Conroy** *June 30, 2016 10:35*

I also cut for up to 3 hours a day ,all mdf, for this month i have cut for 40 hours.even with all the hassels with moisture on lens i would never go back to small air pump,as i do not belive i can get as clean a cut esp on the bottom of the cut-is is very very clean,with no marks at all.Top has minumal smoke marks only 1-2 mm from cut at most


---
*Imported from [Google+](https://plus.google.com/106817371418018865666/posts/Zxjws3nSucM) &mdash; content and formatting may not be reliable*
