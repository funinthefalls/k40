---
layout: post
title: "When using corel laser my engravings and cutting are shrunken down by 2.5% on the x axis and 2.1% on the the y axis"
date: October 11, 2016 01:21
category: "Discussion"
author: "larry champagne"
---
When using corel laser my engravings and cutting are shrunken down by 2.5% on the x axis and 2.1% on the the y axis. My laser settings are set to use WMF. The weird thing is when I switch the cutting settings to the "plotter" one it cuts out to perfect dimensions but it doesn't work for engravings.



Is this normal or am I missing something? 





**"larry champagne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 11, 2016 12:02*

Doesn't sound normal to me. When using CorelLaser if I cut/engraved a 100mm square it would give me a resultant 99.something mm square.



I wonder have you set the board model & device ID into the corellaser settings? Things can act strange if you haven't done that yet.


---
**larry champagne** *October 12, 2016 15:25*

I have set the board model but didn't know you could put in a device ID, I'll look into it. Thanks for the help




---
**Scott Marshall** *October 17, 2016 05:53*

There is a setting for that in there somewhere. It's name is confusing. I don't have one hooked up right now, but I clearly recall getting mine adjusted. it seems to me it was on the lower right part of the dialog box.


---
*Imported from [Google+](https://plus.google.com/113953362128101179522/posts/8RavLd2mEXg) &mdash; content and formatting may not be reliable*
