---
layout: post
title: "I just got my K40 laser and everytime I try to import an image into corel draw, it crashes!"
date: March 06, 2016 15:38
category: "Software"
author: "Brian Phan"
---
I just got my K40 laser and everytime I try to import an image into corel draw, it crashes! This makes the program practically useless and I would have to draw everything.  I'm on windows 10 but multiple users are using Win10 here without a problem.  





**"Brian Phan"**

---
---
**ThantiK** *March 06, 2016 16:27*

"without a problem"...lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 06, 2016 16:32*

I had similar issues with importing to begin with. I am running Win10, using CorelDraw & CorelLaser. Now, I design all my files in Adobe Illustrator CC 2015, however if I save files as anything higher than Adobe Illustrator 9, then it will crash on import into Corel. So maybe check that it is not a similar issue with whatever software you are drawing your original images in.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 06, 2016 16:34*

Oh, if you aren't drawing your original images, then might be worthwhile trying to convert them to a file format that does work. Have a test with different file formats & different versions. Something will work eventually (I hope).


---
**Brian Phan** *March 06, 2016 16:52*

Can I use another version of coral draw and coral laser or do I have to use the one provided in the cd? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 06, 2016 17:09*

**+Brian Phan** I'm not totally certain in regards to that, although I seem to recall reading that someone else used a legitimate version of Corel Draw (one that they already owned) in conjunction with the Corel Laser that came on the cd. Not sure whether the version they were using was a different Corel Draw version or the same version (I got Corel Draw 12 on my cd).


---
**Brian Hopper** *March 06, 2016 23:33*

I am a newly joined K40 owner and still vert much a novice... I had a lot of issues with Corelaser - my Notron antivirus kept quaranteening files. I was worried this would leave me dead in the water with an un-usable machine. I managed to get laserdrw to work... and then dabbled with trying an Inkscape workflow for a while.  I am most familiar with using Photoshop and now I just work my files in Photshop. I create seperate layers of my project for engraving and cutting...  save them as a psd and then each layer seperately as a jpeg.  I then just open the jpeg in laserdrw and run. First run the  engrave then second pass run the cut.


---
**The Technology Channel** *March 07, 2016 17:46*

I use version x5 of CorelDraw and installed corel laser and it works fine.....

the copy of corel draw 12 that came with the laser is a pirate copy, but works ok on xp.


---
*Imported from [Google+](https://plus.google.com/113144958297739833176/posts/hA27VtYkJMj) &mdash; content and formatting may not be reliable*
