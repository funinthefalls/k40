---
layout: post
title: "My new front panel with Arduino and I2C LCD module"
date: July 05, 2015 21:22
category: "Modification"
author: "David Wakely"
---
My new front panel with Arduino and I2C LCD module. Haven't wired up the sensors that's why it saying -1000 degrees lol!

![images/2aa2fe2b9f4260bdcf26624977ebe4af.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2aa2fe2b9f4260bdcf26624977ebe4af.jpeg)



**"David Wakely"**

---
---
**Jon Bruno** *July 09, 2015 16:14*

Nice David! I2C is the Way2GO


---
**Jon Bruno** *July 09, 2015 16:14*

Say.... Can I grab a copy of that panel file?

lol


---
**quillford** *November 14, 2015 09:13*

How do you set up the switches for things like the water pump that plug directly into an outlet?


---
**Jon Bruno** *November 14, 2015 17:17*

Relays are the safest way to do it but if you're not comfortable with working with electric circuits I'd say don't mess with mains stuff.


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/SoVPueo4jvU) &mdash; content and formatting may not be reliable*
