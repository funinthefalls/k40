---
layout: post
title: "I can barely contain myself. I have ordered a Redsail x700 clone"
date: July 27, 2016 13:54
category: "Discussion"
author: "Ben Walker"
---
I can barely contain myself.  I have ordered a Redsail x700 clone.  My local fedex freight department told me yesterday that they said they expect it sooner and wanted to know if I can take delivery a day sooner (tomorrow!).  I am going to have to do some heavy lifting to make room.  And my door going to the studio is only 25"  so I am going to have to trek it around the house and bring it through the patio door or take out a window to get it inside.  I am not sure my garage can contain it.  Talk about shooting first and trying to work out the details later....





**"Ben Walker"**

---
---
**Jim Hatch** *July 27, 2016 14:19*

Woohoo! I am so close to doing something similar - 100W+ (I'd really like 130W) and a 3'x4' bed. But I think I need to arrange for the basement to house it first. Shouldn't be too hard to get it in there but I need to do it before the snow flies 😁


---
**Chris Boggs** *July 27, 2016 15:22*

Which one did you get..  I'm about a week from buying a bigger laser also..  I'm looking a few different ones..  I have about $2500-$3000 I'd like to spend..  Any recommendations would be great


---
**Ben Walker** *July 27, 2016 15:38*

Can't recommend but I upgraded to the 60 watt version.  Make sure you inquire with the seller before ordering to make sure the tube is 1100mm long because several sellers are claiming the 800mm tubes are 60 watt in very similar looking machines.  I am amazed at what I can make in the K40 no frills but I tend to run out of room so the 500x700 bed and the adjustable level bed were the two things I was really interested in.  Instead of sinking the equivalent into upgrades.  I will now take the K40 on the road.  It's a lot more portable considering.  also make sure the seller can deliver to a residential with lift gate if appropriate.  I was smacked with an extra $100 charge on top of the free shipping.  Interestingly it seems that all the different sellers on ebay are the same people under different names.  I have checked my cc and they all are charges from bayamon (sp).  I ordered the rotary tool separate for less than 250 delivered compared to paying 400-600 as a bundle.


---
**Ben Walker** *July 27, 2016 15:41*

**+Jim Hatch** If I had the room and the $$$ I would be all over a 100-130 (or more!) model.  I can dream for the future for sure.

We refer to our driveway as the launch pad in winter especially so I hear ya!  And happily handed over the extra delivery fee to NOT find a 400# box sitting at the bottom of it.


---
**Jim Hatch** *July 27, 2016 15:41*

I have a K40 and a Redsail 60W clone (Legacy Laser 450). The Legacy has an 18x24" bed with adjustable (9"?) Z axis. It also has a commercial chiller (not a pump in a bucket of water :-) ). The big ones (3'x4' bed) I'm looking at on ebay look to be in the $4K range for a 100-130W machine. I want both size & power increases over what I have. The size is the critical one because I can't make it bigger whereas if I have to sacrifice power I can always upgrade the tube later. I'm likely to switch it to a Smoothie controller and LaserWeb anyway so I am keeping that in mind (first I want to get comfortable with the Smoothie/LW3 combo on the K40). 



The reason for the power bump is that 120W+ can do some direct marking on metals (without needing Thermark or Moly) and because it means I can do projects faster. I just finished something for a friend that took 23 minutes for each of 55 plaques and 3 minutes for each of 100 challenge coins. With the 130 I'd be able to cut that time in half. Lots of hours :-)


---
**Ben Walker** *July 27, 2016 15:46*

**+Chris Boggs** I got this one - and the price dropped since I did - by 60$.  [http://www.ebay.com/itm/60W-CO2-Laser-Engraving-Machine-Engraver-Cutter-Cutting-Tool-USB-/201622988549?hash=item2ef1aaa705:g:c7YAAOSwj2dXkBgF](http://www.ebay.com/itm/60W-CO2-Laser-Engraving-Machine-Engraver-Cutter-Cutting-Tool-USB-/201622988549?hash=item2ef1aaa705:g:c7YAAOSwj2dXkBgF)

for the rotary I got [http://www.ebay.com/itm/Rotary-Axis-For-60W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/222164312017?hash=item33ba065fd1:g:HEEAAOSwhMFXlDBJ](http://www.ebay.com/itm/Rotary-Axis-For-60W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/222164312017?hash=item33ba065fd1:g:HEEAAOSwhMFXlDBJ)

two different sellers but oddly same company charged the card and their emails had the same phone number listed (same number I was given by two different sellers of the three K40's I bought as well).


---
**Chris Boggs** *July 27, 2016 16:17*

Very nice..  I'll be glad to hear about your experience with it.. Good luck


---
**Ben Walker** *July 29, 2016 17:24*

Well now we have no idea when it is going to be delivered.  I am furious with the seller for forcing me into paying extra for shipping then put it on the back of a mule (cheapest shipping FedEx offers).  FedEx says the delay was not with their system but there was a problem with the pickup.  No surprise as they sent me tracking numbers for other peoples purchases from the get-go.  I have asked for a refund on the extra shipping charge because they failed.  And left me scrambling to find help I already secured for yesterday and today but won't have next week (plus I took yesterday and today off my regular job).


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/A8s9zt61TBE) &mdash; content and formatting may not be reliable*
