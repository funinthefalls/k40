---
layout: post
title: "For the last week or so there has being many of us asking about converting from stock board to something else"
date: June 07, 2016 15:39
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
For the last week or so there has being many of us asking about converting from stock board to something else. Regardless the objective there seems to be the same questions on how to achieve this. Many with a lot of knowledge have come to the rescue of the few with answers and solutions to each but it seems that we are following orders and not understanding the actual process and what's involved. I would like to invite those with experience and expertise in this matter to please explain in plain English ( no code,  letters, symbols, etc)  what needs to be achieved,  what's the outcome expected in operation and how is it done. Consider this a public service post and maybe will all learn from it.  :-) 





**"Ariel Yahni (UniKpty)"**

---
---
**Don Kleinschnitz Jr.** *June 07, 2016 19:40*

If I understand your request, that is exactly what I am trying to accomplish. Convert my K40 to smoothie while documenting the exact steps. As I am learning I have to compile it in pieces ... .:) My plan it to end up with a step by step set of instructions that I can pass on or add to what it already there. 


---
**Ariel Yahni (UniKpty)** *June 07, 2016 19:43*

**+Don Kleinschnitz**​ yes that's the point.  But more over I want to clear the air as to what's the objective regardless the board. 


---
**Don Kleinschnitz Jr.** *June 07, 2016 19:50*

Mmmm ...... my main objective is to get to a rational and open tool chain. Right now with the software that comes with the K40 I have to tinker with everything that comes from my CAD tools. 

I can cut in my shop by hand  faster than I can setup and cut on my K40 and that is not what I bought it for. I mainly want to cut plastics ...... 



In the process of doing this I also want to have adequate documentation and source so that I can improve on, troubleshoot and or repair the entire system.



When I am done with this I may convert my UP printer, but that's another insanity epic....


---
**Ariel Yahni (UniKpty)** *June 07, 2016 20:06*

**+Don Kleinschnitz**​ that's the goal of anyone trying to convert, but how to get there is the hassle. Either people get afraid with the technical details or else, what I know is that when you decide to jump there is no clear English word on this


---
**Ben Walker** *June 07, 2016 20:47*

Exactly.  I gave up - the seller is accepting the one I was trying to convert (I was thinking I was having a logics problem) as a return.  I plan to go with a Smoothie soon and the arduino can always be used for something - not sure replacing the arduino in the 3d printer is advantageous or not....

I hope that swapping to the smothie board will be a little more documented.  it seems easy enough - my nerves usually get in the way of progress when it comes to lethal doses of electricity.


---
**Ariel Yahni (UniKpty)** *June 07, 2016 21:06*

**+Ben Walker**​ it's easy but we don't understand the logic thus makes you afraid. That's where I want to arrive


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/hrSzJEwbGFJ) &mdash; content and formatting may not be reliable*
