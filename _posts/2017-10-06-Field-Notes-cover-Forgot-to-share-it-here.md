---
layout: post
title: "Field Notes cover. Forgot to share it here"
date: October 06, 2017 14:44
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
Field Notes cover. Forgot to share it here.



![images/e76227a63eee78d7017ba784c3c3009d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e76227a63eee78d7017ba784c3c3009d.jpeg)
![images/8f208da19f6d0593ba6659d2bcd744f4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f208da19f6d0593ba6659d2bcd744f4.jpeg)
![images/01c214051ff4de3858fe720bb8967c24.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/01c214051ff4de3858fe720bb8967c24.jpeg)
![images/153f16ff2adc214aa4a64b9aaad99aef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/153f16ff2adc214aa4a64b9aaad99aef.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *October 06, 2017 14:45*

File and info here

[openbuilds.com - Field Notes Cover &#x7c; OpenBuilds](http://www.openbuilds.com/projects/field-notes-cover.74/)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/6NCVcavsADw) &mdash; content and formatting may not be reliable*
