---
layout: post
title: "I really wanted to post this in multiple communities (Smoothie and LaserWeb) but I am lazy"
date: October 26, 2016 21:56
category: "Hardware and Laser settings"
author: "Don Kleinschnitz Jr."
---
I really wanted to post this in multiple communities (Smoothie and LaserWeb) but I am lazy. 



Is there a place that smoothie and LaserWeb configuration files are/can-be kept. 

It would be useful if they could be indexed by build configuration and/or special processes that need special setup, like laser carving??







**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *October 26, 2016 22:02*

I can host them in my collection if you want to. Lots of followers there. 


---
**Anthony Bolgar** *October 26, 2016 22:07*

I was just thinking about the same thing. Just a file repository, no forum chat. Maybe a google drive shared to the K40 and LaserWeb communities?


---
**Anthony Bolgar** *October 26, 2016 22:10*

Great idea Peter, I will set up a template that people can use.


---
**Don Kleinschnitz Jr.** *October 26, 2016 22:32*

**+Peter van der Walt** I like it ....


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/123MqWxJyyY) &mdash; content and formatting may not be reliable*
