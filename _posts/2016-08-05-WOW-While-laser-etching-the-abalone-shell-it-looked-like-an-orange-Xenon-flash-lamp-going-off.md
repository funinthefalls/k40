---
layout: post
title: "WOW While laser etching the abalone shell it looked like an orange Xenon flash lamp going off"
date: August 05, 2016 16:06
category: "Object produced with laser"
author: "David Cook"
---
WOW   While laser etching the abalone shell   it looked like an orange Xenon flash lamp going off.  it was EXTREMELY bright.  I could not even look at it through the viewing window,  I had to view it through my phone screen.  I never seen this before with other materials.  It was a brilliant flash of light.



really cool,  just wanted to share the little video.


{% include youtubePlayer.html id="VM0wEqGDSYc" %}
[https://www.youtube.com/watch?v=VM0wEqGDSYc&feature=em-upload_owner](https://www.youtube.com/watch?v=VM0wEqGDSYc&feature=em-upload_owner)





**"David Cook"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 16:26*

That's interesting. I haven't come across anything like this as yet, but will keep my eye out for it too.


---
**David Cook** *August 05, 2016 16:55*

**+Yuusuf Sallahuddin**  it's hard to describe just how intense the light was lol.   it was pretty awesome though.  LaserWeb & Smoothie is making this machine soo much fun to operate.  I'm enjoying trying different materials.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 17:07*

**+David Cook** I'm still in the process of procrastinating on doing my Smoothie/LW upgrade. I'm waiting on the ACR kit that Scott Marshall has made to make the connection simple, as I really don't want to attempt wiring & destroy anything lol. It should be here within a week & then I can join the fun :)


---
**David Cook** *August 05, 2016 18:03*

**+Yuusuf Sallahuddin** his ACR kit looks great.  Also interested in his WaterWorks kit


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 18:50*

**+David Cook** Yeah, the site should be up in a few days (working on it now) so you'll be able to see all the products there once we get them all listed. Just starting with the ACR to begin with to minimise Scott's work on retelling the same story to everyone. Then we can spend the time he usually does that getting other products up. I'm interested in most of his products, the Switchable in particular. Will make life easy to have a backup controller to switch to in case I ever need it.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/96z98Mfg27X) &mdash; content and formatting may not be reliable*
