---
layout: post
title: "This may be a domb question when operating a laser cutter machine should laser glasses be used i don't see much on laser safety in the forum Thanks Dennis"
date: March 05, 2016 19:09
category: "Discussion"
author: "Dennis Fuente"
---
This may be a domb question when operating a laser cutter machine should laser glasses be used i don't see much on laser safety in the forum



Thanks

Dennis 





**"Dennis Fuente"**

---
---
**Gee Willikers** *March 05, 2016 19:16*

They just about anything between you and the beam makes it safe. The light it puts out isn't in our visual spectrum. That being said, I still use correctly rated glasses most of the time.



If you have Facebook, there is a fantastic K40 user / general laser group there. The info there is better than any forum or website around.


---
**Dennis Fuente** *March 05, 2016 19:19*

I do have face book  what color glasses would be the best to use i see there are a number of different colors dose it matter


---
**Jim Hatch** *March 05, 2016 19:21*

**+Dennis Fuente**​ Theoretically the acrylic window should stop any stray reflections and the bed will absorb direct hits. But there are no safety features built into these and there's always a chance it will fire when the lid is open or some freak refraction pattern could expose you to the beam. Not a big risk but also not a big effort (wear glasses) to eliminate the risk.



I wear co2 laser rated safety glasses.


---
**Gee Willikers** *March 05, 2016 19:28*

CO2 rated for 10600nm wavelength.



[https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
**Dennis Fuente** *March 05, 2016 19:29*

so can i get these on ebay what to look for

Thanks 

Dennis 


---
**Gee Willikers** *March 05, 2016 19:33*

Ebay search "10600 laser glasses" or similar. Something like [http://www.ebay.com/itm/UVEX-SAFETY-LASER-GLASSES-/111926055069?hash=item1a0f50109d:g:kN8AAOSw~OVWvPg5](http://www.ebay.com/itm/UVEX-SAFETY-LASER-GLASSES-/111926055069?hash=item1a0f50109d:g:kN8AAOSw~OVWvPg5)


---
**Jim Hatch** *March 05, 2016 19:33*

I got mine on Amazon.


---
**Gee Willikers** *March 05, 2016 19:33*

fyi I paid about $60usd for 2 pair with cases.


---
**Dennis Fuente** *March 05, 2016 19:47*

Thanks Guy's i am looking for them.



Dennis 


---
**Jim Root** *March 06, 2016 00:54*

I bought a pair but I generally don't operate the laser with the door open and don't spend a lot of time watching it cut. 


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/1QRaKePyhrd) &mdash; content and formatting may not be reliable*
