---
layout: post
title: "Any recommendations on grounding the k40? I noticed the poorly worded manual stressed grounding the unit, but from what I could tell it looked good"
date: January 28, 2017 00:56
category: "Modification"
author: "matt s"
---
Any recommendations on grounding the k40? I noticed the poorly worded manual stressed grounding the unit, but from what I could tell it looked good.





**"matt s"**

---
---
**HP Persson** *January 28, 2017 11:34*

If you have good ground in the wall outlet, no need to add anything extra.

Just check the cable, i have seen machines with 3 prong cables, where only 2 was connected inside the cable.




---
**matt s** *January 28, 2017 15:07*

**+HP Persson** That's what I assumed being in the states, but this is my first laser so I"m extra paranoid. Thx for the assistance.


---
**HP Persson** *January 28, 2017 20:37*

With electricity you can never be too careful :)


---
**** Smith** *February 06, 2017 01:22*

Just saw this video today. 




{% include youtubePlayer.html id="6vztMvWUnmM" %}
[https://youtu.be/6vztMvWUnmM](https://youtu.be/6vztMvWUnmM)


---
**** Smith** *February 06, 2017 01:22*

Just saw this video today. 




{% include youtubePlayer.html id="6vztMvWUnmM" %}
[https://youtu.be/6vztMvWUnmM](https://youtu.be/6vztMvWUnmM)


---
**** Smith** *February 06, 2017 01:22*

Just saw this video today. 




{% include youtubePlayer.html id="6vztMvWUnmM" %}
[https://youtu.be/6vztMvWUnmM](https://youtu.be/6vztMvWUnmM)


---
**** Smith** *February 06, 2017 01:26*

Sorry for the triple post it was saying it couldn't ad comment. 🤔


---
*Imported from [Google+](https://plus.google.com/106809986839735143620/posts/hTPAFMnph7V) &mdash; content and formatting may not be reliable*
