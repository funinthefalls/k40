---
layout: post
title: "A couple of things I was playing around with last night, the photo turned out better than I expected"
date: November 26, 2015 10:26
category: "Discussion"
author: "Scott Thorne"
---
A couple of things I was playing around with last night, the photo turned out better than I expected.



![images/7ecfe01f1e37a81045d10e187edee4c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7ecfe01f1e37a81045d10e187edee4c6.jpeg)
![images/0c6cf6ff79a9213d8e58e459512bf215.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c6cf6ff79a9213d8e58e459512bf215.jpeg)

**"Scott Thorne"**

---
---
**Gary McKinnon** *November 26, 2015 10:41*

Very nice results :)


---
**Scott Thorne** *November 26, 2015 10:46*

Thanks Gary.


---
**Sean Cherven** *November 26, 2015 13:32*

You using the stock LaserDRW board?


---
**Scott Thorne** *November 26, 2015 13:37*

Yeah work corellaser


---
**Scott Thorne** *November 26, 2015 13:38*

Oops....yes with corellaser


---
**Sean Cherven** *November 26, 2015 13:43*

Nice!


---
**Scott Thorne** *November 26, 2015 14:50*

I edited the photo with Corel photo and imported it into corellaser.


---
**Coherent** *November 26, 2015 16:01*

Looks great!


---
**Scott Thorne** *November 26, 2015 17:10*

Thanks Marc G.....I just read a thread saying that laser draw Is only for making stamps so I'm trying to figure out corellaser......it's going ok....hard program to figure out without the manual.


---
**Gary McKinnon** *November 26, 2015 17:13*

I use Sketchup, export to PDF then import into Corellaser and it works well.


---
**Kirk Yarina** *November 26, 2015 21:14*

LaserDRW works fine standalone.  The stamp only story isn't true.


---
**Gary McKinnon** *November 28, 2015 12:48*

Ye i thought this machine is a bit overkill for making rubber stamps, anti Chinese propaganda!


---
**Scott Thorne** *November 28, 2015 13:08*

Gary...where can I find sketchup?


---
**Sean Cherven** *November 28, 2015 13:20*

Google lol. Sketchup is a Google product.


---
**Scott Thorne** *November 28, 2015 13:22*

Lol...shows how new to this I am...lol


---
**Sean Cherven** *November 28, 2015 13:23*

It's all good. ^_^


---
**Scott Thorne** *November 28, 2015 13:49*

Thanks for the tip....lol....I'm gonna give it a try a little later, providing my wife don't start tripping about me spending more time in the garage than in the house....lol


---
**Gary McKinnon** *November 28, 2015 13:59*

"Get out of that garage!!!" lol :)



Sketchup has an unusual interface in that it's a more natural style to put together forms, it's well worth watching the how to videos and is a really easy workflow once you get used to it.


---
**Stephane Buisson** *December 02, 2015 09:56*

the trick with Sketchup is to keep well organized, and group a lot paying attention at witch level you are into. (add a line into a group which is itself into another group and so on)

**+Scott Thorne** **+Gary McKinnon** 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/Mf4K2Q2xozY) &mdash; content and formatting may not be reliable*
