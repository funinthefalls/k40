---
layout: post
title: "Submersible UV sterilization lights. I like watching a UK vlogger named \"Big Clive.\""
date: September 03, 2018 14:47
category: "Discussion"
author: "Jamie Richards"
---
Submersible UV sterilization lights.  I like watching a UK vlogger named "Big Clive."  He had a couple videos on a certain destructive UV light he used to sterilize stuff, so I wondered if there were any waterproof versions.  Needless to say, my water tank has one in it now.  If this works out, I may never have to worry about algae or bacteria again.





**"Jamie Richards"**

---
---
**greg greene** *September 03, 2018 15:07*

Available at any big box store - called UV water sterilizers


---
**Jamie Richards** *September 03, 2018 15:18*

A lot of people are having good luck clearing algae in fish tanks with one particular unit called "Green Killing Machine."


---
**Alex Hayden** *September 04, 2018 01:29*

Put a silver coin in the tank.


---
**Jamie Richards** *September 05, 2018 17:05*

Will it add conductivity over time?  And is it enough?


---
**Alex Hayden** *September 05, 2018 17:31*

I am not sure if it will add conductivity. I doubt it. And I am not sure if it will be enough. I just know some companies that make water cooling loops for PC offer a silver item to put in the loop and claim it kills bacteria. I have no proof this works but I would think it would be worth trying. Doesn't cost anything to run. You can easily get a silver coin from your bank. Just ask for 50 dollars worth of 50 cent coins. Any of them older then 1964 are about 90% silver.


---
**Jamie Richards** *September 08, 2018 19:49*

I make rings out of them.  They don't turn your fingers green even though they are 2% below Sterling.  The silver state proof coins were pretty popular.  I have a WV ring I need to make for someone, but I still have a few weeks before I'm allowed to swing a hammer since spine surgery.


---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/MUK2x5yB4vk) &mdash; content and formatting may not be reliable*
