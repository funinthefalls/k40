---
layout: post
title: "More fun with new tile. The wife was bored with white and black tiles so she picked up some 6\" x 6\" travertine style tiles at Home Depot for 89 cents each"
date: December 05, 2018 01:01
category: "Object produced with laser"
author: "HalfNormal"
---
More fun with new tile.



The wife was bored with white and black tiles so she picked up some 6" x 6" travertine style tiles at Home Depot for 89 cents each. Colored it in with a Magnum Sharpie. (my new favorite!).



![images/a29b0070acb8765f2467e8639846159a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a29b0070acb8765f2467e8639846159a.jpeg)



**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/SMQwn5HU9fu) &mdash; content and formatting may not be reliable*
