---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) So I've cut & engraved the outer for my wallet"
date: December 14, 2015 20:49
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



So I've cut & engraved the outer for my wallet. I did it using the 1mm kangaroo hide also, as I want to test it for strength as an outer. Was very difficult to find an engraving setting that was not too deep but still showed the detail. I ended up with ~4-5mA @ 500mm/s. Was pushing it for depth though. I reckon it went about 1/2 way through the leather.



Anyway, this is the design for my outer. Next step is some dyeing (which will be in multiple colours: black, tan, orange). That is going to be difficult I imagine.



![images/97ef48b3bda2e6e2e0fc1fdb904f6af1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97ef48b3bda2e6e2e0fc1fdb904f6af1.jpeg)
![images/fca2c4aad5c700c678130a9d6994a3d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fca2c4aad5c700c678130a9d6994a3d4.jpeg)
![images/a8071ab0afd2378b305c4cb4afbbd527.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a8071ab0afd2378b305c4cb4afbbd527.jpeg)
![images/d33e1492fd16b68949bc198cda6ecc31.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d33e1492fd16b68949bc198cda6ecc31.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Anthony Coafield** *December 14, 2015 22:07*

Very nice.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2015 03:36*

**+Anthony Coafield** Thanks Anthony.


---
**ChiRag Chaudhari** *December 15, 2015 18:42*

Wow man, YOUR wallet is gonna come out pretty darn good.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2015 18:51*

**+Chirag Chaudhari** Thanks Chirag. I'm looking forward to finishing the dyeing & having it completed.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/7mbobg7ciEw) &mdash; content and formatting may not be reliable*
