---
layout: post
title: "My power supply is making the Exact same noise a this one: anyone else had this and knows what it is?"
date: November 16, 2018 12:30
category: "Discussion"
author: "Yorkshire Fox"
---
My power supply is making the Exact same noise a this one:




{% include youtubePlayer.html id="mbf4W-A2114" %}
[https://www.youtube.com/watch?v=mbf4W-A2114](https://www.youtube.com/watch?v=mbf4W-A2114)



anyone else had this and knows what it is?





**"Yorkshire Fox"**

---
---
**Don Kleinschnitz Jr.** *November 16, 2018 13:58*

Typically this is the High Voltage Transformer in the LPS arching. Although it could also be a HV leak in the wiring.

You can replace the HVT but now days you can find LPS for $40-70 and a HVT is about $30.



Turn out the lights and see where the system is arching.


---
**Yorkshire Fox** *November 16, 2018 14:01*

**+Don Kleinschnitz Jr.** Thanks again Don. Will i better off removing the psu case first?


---
**Don Kleinschnitz Jr.** *November 16, 2018 17:59*

**+Yorkshire Fox** 

<b>WARNING removing the K40 LPS cover can expose lethal voltages! You do so at your own risk!</b>



That said; 

-after unplugging the K40's main power plug 

-wait 30 min

-then you can remove the LPS cover

...

-plug the power back in and run the machine in the dark with the LPS visible

-stay well away from the machine while it is running 

-do not put your hand near the LPS and do not touch any conductive surfaces while it is running

...

-after unplugging the K40's main power plug 

-wait 30 min

-then you can replace the LPS cover



Videos may help. If the HVT is bad you may see arcing or corona around the HVT (white transformer).  


---
**Yorkshire Fox** *November 16, 2018 20:01*

**+Don Kleinschnitz Jr.** I was very careful but had to get the camera close enough. No visible arcing when the lights were off. 



I replaced my laser tube due to the other arcing issue i had which resolved it but i dont feel like im getting enough power. It take 10 passes with power at 90% to cut through 3mm acrylic. 




{% include youtubePlayer.html id="rtu2xdRS6-k" %}
[https://youtu.be/rtu2xdRS6-k](https://youtu.be/rtu2xdRS6-k)






---
**Kelly Burns** *November 17, 2018 16:58*

What speed are your running and do you feel like your alignment is good, mirrors and lenses are clean?


---
**Yorkshire Fox** *November 17, 2018 17:12*

**+Kelly Burns** running 6mm/s cleaned and aligned mirrors. Although I wil be buying some new mirrors as the machine was second hand 


---
**Don Kleinschnitz Jr.** *November 17, 2018 17:33*

Is there any arching in the laser tube compartment?


---
**Yorkshire Fox** *November 17, 2018 20:53*

**+Don Kleinschnitz Jr.** no arcing in the back. the noise is defo coming from the white flyback




---
**Don Kleinschnitz Jr.** *November 17, 2018 23:09*

**+Yorkshire Fox** You do not always see the corona so I am guessing the HVT is bad. You can replace that or the supply.




---
*Imported from [Google+](https://plus.google.com/100999477299382644053/posts/afZ6S8aUPaz) &mdash; content and formatting may not be reliable*
