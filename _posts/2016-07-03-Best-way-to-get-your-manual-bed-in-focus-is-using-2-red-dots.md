---
layout: post
title: "Best way to get your manual bed in focus is using 2 red dots"
date: July 03, 2016 15:14
category: "Discussion"
author: "Mircea Russu"
---
Best way to get your manual bed in focus is using 2 red dots. You calibrate to get a single point where the beam is in focus, you get two separate dots if you are lower or higher. You can then even estimate how high you are by the distance between the points, useful when you want focal distance to be in the middle of the material to be cut.



![images/33b25693d31383955a88a20401f511f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/33b25693d31383955a88a20401f511f3.jpeg)
![images/7f240a6753d9f5af18024cd725778b36.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7f240a6753d9f5af18024cd725778b36.jpeg)
![images/bd3805bb82278ff426ffa22c4352521f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bd3805bb82278ff426ffa22c4352521f.jpeg)

**"Mircea Russu"**

---
---
**Evan Fosmark** *July 03, 2016 17:27*

Where'd you find the adapter to hold the lasers?


---
**Mircea Russu** *July 03, 2016 20:35*

[http://www.aliexpress.com/item/K40-Co2-Laser-Head-Focus-Adjustable-Focal-Diode-Module-Red-Dot-Position-Engraver-Cutter-Dia-22mm/32498241689.html?spm=2114.13010708.0.68.AYOlSF](http://www.aliexpress.com/item/K40-Co2-Laser-Head-Focus-Adjustable-Focal-Diode-Module-Red-Dot-Position-Engraver-Cutter-Dia-22mm/32498241689.html?spm=2114.13010708.0.68.AYOlSF)


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/i4AwkdXyUHm) &mdash; content and formatting may not be reliable*
