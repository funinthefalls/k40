---
layout: post
title: "that's how it sounds when the machine works , you think the normal sound , or they think it lacks grease ?"
date: July 16, 2015 11:48
category: "Hardware and Laser settings"
author: "Al Tamo"
---
that's how it sounds when the machine works , you think the normal sound , or they think it lacks grease ?


**Video content missing for image https://lh3.googleusercontent.com/-my_j2ZBH52k/VaeaGBIxYRI/AAAAAAAAAN0/sYpAE5JLiiE/s0/581a8c5f-629a-4605-8eb6-9831c345b3a3.mp4.gif**
![images/452ef5e03bb05d876abf7a97c49a1f8d.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/452ef5e03bb05d876abf7a97c49a1f8d.gif)



**"Al Tamo"**

---
---
**Joey Fitzpatrick** *July 16, 2015 13:46*

My K40 did something similar when I first got it.  The #2 mirror bracket was dragging on the outer case/housing  causing the Y stepper to skip steps and rattle.  You may want to check to see if the X and Y axis move freely when the unit is off.  any binding will cause the motors to Pop/Rattle.  The belts shouldn't be excessively tight or loose either.  Also, you may want to check to see if all the screws that hold the motor to the axis are tight.  If everything is mechanically ok,  there might be a driver issue on the mainboard


---
**Jon Bruno** *July 16, 2015 14:13*

Kinda sounds like mine.. It's a fairly rattly machine.. Or mine has the same issue. lol


---
**Al Tamo** *July 16, 2015 14:48*

**+Joey Fitzpatrick** the truth is that it is a bit strange, because if I cross the axis "x " from beginning to end without moving the axis "and" does not sound bad , and vice versa too, but if I put the machine to work if you do, cut well but if I move the two axes at once it is when does the noise.


---
**Jim Coogan** *July 16, 2015 15:35*

That's normal for mine.  Just the sound of the gears, belts, rollers, and motors as it changes direction quickly.


---
**Al Tamo** *July 16, 2015 15:46*

Ooooooook :-)


---
**William Steele** *September 09, 2015 23:56*

It's actually the sound of the motor controllers poor quality signal getting to the motors.  (Most likely due to 1/2 or whole stepping.)  I upgraded to a Marlin based 3D printer board and the printer completely went silent compared to the old electronics.


---
*Imported from [Google+](https://plus.google.com/112166512335650872249/posts/3uWpAeK2YSX) &mdash; content and formatting may not be reliable*
