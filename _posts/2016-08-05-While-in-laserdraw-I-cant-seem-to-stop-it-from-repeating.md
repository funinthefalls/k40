---
layout: post
title: "While in laserdraw I can't seem to stop it from repeating"
date: August 05, 2016 00:06
category: "Original software and hardware issues"
author: "Jeremy Hill"
---
While in laserdraw I can't seem to stop it from repeating.  The setting for repeating is constantly at 1.  I tried to lower it to 0 but it reverts back to 1.  Anyone have a way to stop it from doing that?  Thank you.





**"Jeremy Hill"**

---
---
**Jeremy Hill** *August 05, 2016 00:07*

just to add clarity- this is while I am in the 'cutting' mode


---
**HalfNormal** *August 05, 2016 00:11*

**+Jeremy Hill** Make sure your line widths are less than 0.001 in or mm.  The thinner the better.


---
**Jeremy Hill** *August 05, 2016 00:23*

will that help to stop it from repeating or just minimize the issue?  I will give it a try on my  next cut.


---
**HalfNormal** *August 05, 2016 00:25*

Should stop it altogether. 


---
**Eric Flynn** *August 05, 2016 00:35*

Also make sure you have no double lines.


---
**Jeremy Hill** *August 05, 2016 01:08*

Ok.  Thanks!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 02:02*

It should always be on repeat 1, as that means it will do 1 cut. Lower is not possible as it would do 0 cuts. The wording is incorrect for the option. It should really be called "Passes" rather than "Repeat".


---
**Jeremy Hill** *August 05, 2016 11:42*

Thanks again. I appreciate the help


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/FtahUTdPmmQ) &mdash; content and formatting may not be reliable*
