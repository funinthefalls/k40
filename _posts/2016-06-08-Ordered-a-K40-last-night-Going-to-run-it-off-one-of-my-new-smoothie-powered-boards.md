---
layout: post
title: "Ordered a K40 last night. Going to run it off one of my new smoothie-powered boards"
date: June 08, 2016 12:38
category: "Smoothieboard Modification"
author: "Ray Kholodovsky (Cohesion3D)"
---
Ordered a K40 last night. Going to run it off one of my new smoothie-powered boards. Still not sure what I need to do as far as level shifting the pwm (and enable) pins. I'm hearing everything from simple Chinese level shifter board that uses transistor and 2 resistors, to complex op amp circuits. Please advise. It's important to maintain the pwm frequency as we shift up. 





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Stephane Buisson** *June 08, 2016 13:15*

;-)) 

please share your components detail (PSU, board, ...)

look how the stock board is connected before disassembly (fire on L or IN), that could help later.


---
**Ray Kholodovsky (Cohesion3D)** *June 08, 2016 13:20*

Will do. The machine won't be here for at least a few days. Probably after the weekend. 


---
**Ariel Yahni (UniKpty)** *June 08, 2016 14:13*

**+Ray Kholodovsky**​ we need to get to the bottom of this. Only one pin is needed to run the laser. The second for on/off is really not needed. As **+Stephane Buisson**​ said to me I find it much better and secure to run the max power of the tube from the Pot and use the laser switch as a security button to shut of the laser


---
**Ray Kholodovsky (Cohesion3D)** *June 08, 2016 14:17*

Interesting. You want to pulse the laser from the enable pin?


---
**Ariel Yahni (UniKpty)** *June 08, 2016 14:24*

**+Ray Kholodovsky**​ look here [https://plus.google.com/+ArielYahni/posts/FuzsbjuMCQM](https://plus.google.com/+ArielYahni/posts/FuzsbjuMCQM)


---
**Scott Marshall** *June 08, 2016 15:57*

I just placed a board order with Oshpark for my 1st K40 support product prototype boards.



It's a Plug in Controller Retrofit Board. It includes 2 Bidirectional level shifters using Mosfets that can be used as desired (I/O brought to a terminal block). The board is designed to go with my tri-voltage power supplies (5,12,24) and has an onboard 3.3V regulator (800ma max) also brought out to  a terminal block. On the end of the board are Red, White and Blue Leds as pilots for the 3.3v, 5v, & 24v lines. It has a full compliment of connectors to hopefully accommodate most versions of th K40.



In my research on the level shifters, I ran several translator chips, but most are overkill for the K40 application(many channels, tri-state etc), and only add price and complexity. 

The FET solution is simple and serviceable. It's good to 10mhz+ and overload tolerant.



Drop me a line when you get there, I hope to have prototypes running in 2-3 weeks.


---
**Ray Kholodovsky (Cohesion3D)** *June 08, 2016 16:00*

My smoothie control boards are ready and work.  The only thing is that I want to incorporate a level shifter into it and wanted to verify the best way to do it.


---
**Don Kleinschnitz Jr.** *June 09, 2016 12:58*

Looks like a lot of us are in the same phase, getting our LPS design established. I am going to use a lever shifter breakout from Adafruit because I keep them around and they are cheap $3.00. Yes it is overkill but even if you do it in discrete's it will require a breakout of some sort. Ideally this would be on the middleman or built into a smoothie version. I considered designing my own level shifter but the cost of a breakout (which certainly have enough bandwidth) did not make it worth it.

Still seems to be confusion about what we are doing here. So risking being redundant and speaking to the choir... **+Peter van der Walt** and I went around on this some time ago and I my understanding and design approach is as follows. 

The stock K40 uses a 0-5v signal on the LPS to adjust the power to the laser. That's the knob on the top of the machine. We are going to connect a PWM signal to this from the Smoothie and dynamically make the same adjustment as the Pot but from the Smoothie.

My concern was the interface to the LPS and what its response would be to a PWM signal. The expectation is that the input to the LPS is AC coupled through an RC network that will integrate the PWM signal.

However the Smoothie outputs a 0-3V PWM signal and that needs to be shifted to 0-5VDC, ala a level shifter.

I wish I at least had a typical schematic for these LPS.

Once implemented I plan to test this setup and eventually characterize the LPS control transform.


---
**Ariel Yahni (UniKpty)** *June 09, 2016 13:36*

**+Don Kleinschnitz**​ I'm leaving the pot untouched. I just use the Fire signal from the connector that goes to the nano board. That way pot and laser switch can act as a security and max power or to adjust power on the fly


---
**Don Kleinschnitz Jr.** *June 09, 2016 14:38*

I am planning the same but have not looked at what having a fixed voltage on that pin does to the output of the level shifter.....


---
**Vince Lee** *June 10, 2016 23:06*

I PWM control on the enable line as well instead of the IN line.  This line is active low and inactive when open, so a level shifter isn't absolutely necessary, but I've seen the suggestion to add a level shifter anyway to isolate the smoothie board from whatever current is being sinked on that line.  My board already had mosfets draining to ground connected to the two primary PWM lines, so I just tied to their output directly.  I had originally planned to use a bidirectional level shifter (which I found was itself a mosfet with some resistors) but an onboard mosfet worked fine for me so I never tried it.


---
**Don Kleinschnitz Jr.** *June 11, 2016 01:55*

+Vince Lee 

Enable line? My LPS has:

P+ & grnd: this is. "Laser Switch" button

K+ & K-: this is, "Test Button"

G & IN & 5v this is the pot?



Are you using the P+ input?


---
**Vince Lee** *June 13, 2016 03:42*

The enable line I use is the one that originally went to the line marked fire on the moshiboard to turn on and off the laser.  Sounds like p+


---
**Ariel Yahni (UniKpty)** *June 13, 2016 03:53*

I'm using the L that was going to the nano2 board


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/SsqubnzPmr9) &mdash; content and formatting may not be reliable*
