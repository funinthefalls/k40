---
layout: post
title: "Just getting started with my laser. Anyone know how to solve mirror burn out?"
date: August 26, 2017 18:52
category: "Hardware and Laser settings"
author: "Ray Lufkin"
---
Just getting started with my laser. Anyone know how to solve mirror burn  out? In less than 1 hour of use I have gone thru 4 mirrors.





**"Ray Lufkin"**

---
---
**Kevin Lease** *August 26, 2017 19:38*

Keep mirrors clean with isopropyl alcohol

Don't stick directly to mirror surface when aligning you canstick to mirror frame ok

Use adequate ventilation to have less matter deposit on surfaces


---
**Anthony Bolgar** *August 26, 2017 20:54*

What type of mirrors are they ?


---
**Alex Krause** *August 27, 2017 05:01*

What material are you cutting?


---
**Stephane Buisson** *August 27, 2017 08:25*

original mirrors are the very cheap model, when cleanning  you can wipe away the coating then your mirror will lose efficiency and then will not  reflect enought then crack. it happen to me, I change them and discover the big difference thats make. (look for my old posts)


---
**Ray Lufkin** *August 27, 2017 15:21*

Got some replacement Mo mirrors first and they burned almost immediately. Got some gold znSe mirrors from US and they did same. I'm going to check potentiometer and current at laser next.


---
**Ray Lufkin** *August 27, 2017 15:23*

I have contacted the supplier for advise. Never got to cut or even complete an etch. I'm using k40 whisperer. Like it very much.


---
**Nate Caine** *August 28, 2017 13:31*

Ray, I didn't know they made ZnSe mirrors.  Why would they do that?



Even the low cost K9 mirrors are adequate for the 40W of the K40 machine.



Any photos of your burned mirrors?  Which mirrors get burned?  All three?


---
*Imported from [Google+](https://plus.google.com/103781383742091041702/posts/Wr3G6kQENsB) &mdash; content and formatting may not be reliable*
