---
layout: post
title: "So, I bought this cute little 8L/min aquarium air pump (diaphragm type) for my future air assist.."
date: February 25, 2016 13:18
category: "Modification"
author: "Linda Barbakos"
---
So, I bought this cute little 8L/min aquarium air pump (diaphragm type) for my future air assist.. It has 4x 2L/min outlets which I've connected to make one outlet, a manual speed adjusting dial and is relatively quiet. Because I live on a tiny little island in the Gulf, I'll have to wait 45-60 days to receive my air assist nozzle. Does anyone know of any temporary hacks/designs I could utilise in the meantime? I'm sick of the flambé...

![images/a2ec3d12df11e11090707c4190a12175.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a2ec3d12df11e11090707c4190a12175.jpeg)



**"Linda Barbakos"**

---
---
**Anthony Bolgar** *February 25, 2016 14:22*

If you have access to a 3d printer you can print one out. I would be happy to send you  a 3d printed one if you cover the postage. (not sure if it would arrive any quicker)


---
**Jim Hatch** *February 25, 2016 14:39*

or grab a piece of hard tubing and warm it up to bend it at an angle then zip tie it to the current head. stick the air pump tubing on the other end and that should cover you until your new head comes in.


---
**Stephane Buisson** *February 25, 2016 17:24*

Same goal, quiet is gold:

I bought a 15l/mn 220v 32db, I will report when i get it.

just a bit worry about vibrations, don't want the material to move on the table.



[http://www.aliexpress.com/item/LF-2208P-Electromagnetic-air-pump-Fruit-and-vegetable-detoxification-machines-ozone-generator-aquarium-fish-parts-shipping/1128865153.html](http://www.aliexpress.com/item/LF-2208P-Electromagnetic-air-pump-Fruit-and-vegetable-detoxification-machines-ozone-generator-aquarium-fish-parts-shipping/1128865153.html)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 25, 2016 22:22*

I've used a ball-inflation needle to focus the air & zip-tied it at an angle to focus right on the cutting area. Seems to funnel the airflow out with a bit more pressure due to it's thin size.


---
**Linda Barbakos** *February 29, 2016 11:32*

**+Anthony Bolgar**  Thanks so much for the offer! I don't think it will arrive much quicker but a spare head would come in handy.


---
**Anthony Bolgar** *February 29, 2016 15:44*

Send me your info Linda and I will send one off to you if you want. I can calculate the postage after seeing the contact info.


---
*Imported from [Google+](https://plus.google.com/107262725887494713165/posts/2N2yKAH8BF1) &mdash; content and formatting may not be reliable*
