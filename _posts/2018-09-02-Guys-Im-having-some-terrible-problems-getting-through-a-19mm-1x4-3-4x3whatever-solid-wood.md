---
layout: post
title: "Guys, I'm having some terrible problems getting through a 19mm 1x4, (3/4\"x3.whatever\") solid wood"
date: September 02, 2018 11:02
category: "Discussion"
author: "Jamie Richards"
---
Guys, I'm having some terrible problems getting through a 19mm 1x4, (3/4"x3.whatever") solid wood.  it's taking 3 passes at 17ma and one side had to be forced out.  I think my mirrors are dirty.



(humor me, I'm drunk)



Getting serious now, this was done with a 38.1mm focal lens. First pic is a little charred because I slowed it down to 2mm/s and 2 passes.  Second pic is 4mm/s and 3-4 passes. (totally forget if 3 or 4 because I was on the phone, will duplicate tomorrow.)  17ma is about as high as I ever want to go, but after a few passes, I would have thought the second one would be charred more. 



Don, it's safe to say my replacement tube and new power supply are working correctly.



![images/64d736da54da4e74826df8237cf65dcd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/64d736da54da4e74826df8237cf65dcd.jpeg)
![images/b700d42ed6c3d270425d696010ca196f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b700d42ed6c3d270425d696010ca196f.jpeg)

**"Jamie Richards"**

---
---
**Stephane Buisson** *September 02, 2018 12:31*

38.1 focal lens and 3 - 4 passes in 19 mm material, I think you are lucky to have the job done. it's a lot of time out of focus, and 2mm/s a good time to start a fire. but  you certainly have a tube to be proud of (but for how long @17mA ?)


---
**Don Kleinschnitz Jr.** *September 02, 2018 12:54*

Wow, thickest material I have seen cut. Do be careful of fires...


---
**Jamie Richards** *September 02, 2018 15:10*

It flamed up once from some excess sap I would say, but the air assist blew it out.  My tube should be able to do 17mA for quite some time.  I ran my original tube at 17mA all the time for cuts and after a year, it only died because I didn't have a flow switch and the pump unknowingly got unplugged; have the switch now!  Russ at RDWorks learning lab said this was possible with a 38.1 lens, so when I made an adjustable bed, I had to test it for myself.  I am, to say the least, amazed.  I like the 4mm/s cut, it looks much better.


---
**Jamie Richards** *September 02, 2018 16:11*

It's not cutting as good with the grain.  This is at 3mm/s.  Gets through against the grain with no problem.


---
**Jamie Richards** *September 02, 2018 16:55*

![images/c1dbfc80463a20a438e8868d7581215b](https://gitlab.com/funinthefalls/k40/raw/master/images/c1dbfc80463a20a438e8868d7581215b)


---
**Jamie Richards** *September 02, 2018 16:56*

I went in front so you could see the entry point and exit.


---
**James Rivera** *September 02, 2018 17:18*

Wow! I didn’t realize a 40 watt tube could cut that thick. You’re making me want to consider buying a new lens! 🤓


---
**Jamie Richards** *September 02, 2018 17:29*

I think I'm driving it at 34 watts.  If the power supply is putting out 20,000 volts anyway.


---
**Jamie Richards** *September 02, 2018 17:31*

Russ gave a live video demonstration of the 38.1 outperforming the lenses that are supposed to cut deeper, but some still resisted it being fact.


---
**Jamie Richards** *September 02, 2018 19:00*

Comparison to the 5mm wood I usually cut.

![images/aea3c71462413763af968328585135e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aea3c71462413763af968328585135e9.jpeg)


---
**HalfNormal** *September 02, 2018 20:14*

I am really curious about the angle of the kerf.


---
**Jamie Richards** *September 03, 2018 03:08*

James Carpino in the facebook group has a theory: "I think the wood does some waveguiding, thought that is usually credited only in acrylic cutting. 10600nm is a large wave! And if the cut is fine enough to start, it's going to make it pretty far down. "


---
**Don Kleinschnitz Jr.** *September 03, 2018 03:58*

**+Jamie Richards** 10,600nm = .0004"


---
**Jamie Richards** *September 03, 2018 05:56*

Ok.  That was a direct quote from him.   I never really thought about it.


---
**Jamie Richards** *September 03, 2018 14:34*

Now that I think about it, it should have hit me on the width.  I used to be very familiar with wavelengths.  Even had a chart on the wall many years ago.  I'm a HAM operator, but haven't been on air since my house burned down awhile back.  I still renew my license though, just in case I ever decide to start up again.


---
**Jamie Richards** *September 04, 2018 12:59*

2 passes!  Had it set for 3 and noticed it separating at 2, so I tried to pull it away and I think I caused the belt to slip because it started mid way.  Weird phenomenon these 38.1 lenses.  

![images/2698be81ab9e6095e5bbd7313b247fe5](https://gitlab.com/funinthefalls/k40/raw/master/images/2698be81ab9e6095e5bbd7313b247fe5)


---
**Timothy Rothman** *September 06, 2018 03:47*

Yup, Russ is a treasure indeed.


---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/dSgLmpvk9SG) &mdash; content and formatting may not be reliable*
