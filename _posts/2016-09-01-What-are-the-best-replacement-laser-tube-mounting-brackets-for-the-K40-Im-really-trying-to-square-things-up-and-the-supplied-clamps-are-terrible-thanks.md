---
layout: post
title: "What are the best replacement laser tube mounting brackets for the K40, I'm really trying to square things up and the supplied clamps are terrible, thanks"
date: September 01, 2016 02:11
category: "Modification"
author: "Eric Rihm"
---
What are the best replacement laser tube mounting brackets for the K40, I'm really trying to square things up and the supplied clamps are terrible, thanks





**"Eric Rihm"**

---
---
**Anthony Bolgar** *September 01, 2016 02:22*

There is a set on thingiverse you can print. The guy who designed them modded a 50 w holder to fit the K40 tube and actually fit inside the case at the back. They are 3 way adjustable, makes squaring up the tube a breeze. [http://www.thingiverse.com/thing:1581069](http://www.thingiverse.com/thing:1581069)


---
**Anthony Bolgar** *September 01, 2016 02:28*

If you do not have a 3d printer **+Eric Rihm** , I would be happy to print you out a set.


---
**Phillip Conroy** *September 01, 2016 03:42*

+ 1 for a set printed and posted to vic3555 australia ,how much?


---
**Anthony Bolgar** *September 01, 2016 07:26*

I could print them for $25.00 plus whatever shipping would be. Your choice of Red, black, white, yellow, gray, orange or lime green colour.


---
**Jon Bruno** *September 01, 2016 14:29*

haha.. lime green




---
**Anthony Bolgar** *September 01, 2016 14:39*

My grandaughter made me buy it....lol


---
**Eric Rihm** *September 01, 2016 18:24*

Thanks Anthony, I'd love a set my printer isn't aligned properly and still figuring out firmware. I'll take a set in whichever color works for you, do you wanna send me a paypal invoice or whichever way you prefer to be paid?


---
**Anthony Bolgar** *September 02, 2016 14:45*

I will paypal an invoice once I have them made, should be Monday or Tuesday, just send me your paypal email.


---
**Eric Rihm** *September 05, 2016 08:02*

you can send me an invoice at ericrihm@gmail.com thanks!


---
**Phillip Conroy** *September 05, 2016 09:12*

Can you do a set for me as well,i am in Austraia ,where are you in the world?


---
**Anthony Bolgar** *September 05, 2016 13:08*

**+Phillip Conroy** I would be happy to make you a set as well, but I will check on shipping costs before I do that. I would not want you to have to pay some ridiculous amount for shipping. If shipping is too much, I am sure we can find someone in the K40 community closer to you who could make them for you.


---
**Anthony Bolgar** *September 05, 2016 13:09*

Let me know what colour you want them.


---
**Phillip Conroy** *September 05, 2016 22:06*

Any color


---
**Anthony Bolgar** *September 05, 2016 22:14*

OK, everybody gets Yellow (it was what was in the printer this morning...lol)


---
**Anthony Bolgar** *September 06, 2016 08:14*

Oh, i just realized I never answered your question Philip, I am in Canada.


---
**Anthony Bolgar** *September 08, 2016 12:43*

Update.....parts are all printed, will send paypal invoice to you today Eric, Philip, I will have a quote for you in the next hour or so.


---
**Anthony Bolgar** *September 08, 2016 13:37*

**+Eric Rihm** Please send me your shipping info.


---
**Eric Rihm** *September 09, 2016 01:30*

Could you send me your email or reply to my address above and I'll get you the details and payment asap


---
**Anthony Bolgar** *September 09, 2016 01:42*

anthonybolgar@live.com  I have already sent the paypal invoice.


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/Y634T9qUd3z) &mdash; content and formatting may not be reliable*
