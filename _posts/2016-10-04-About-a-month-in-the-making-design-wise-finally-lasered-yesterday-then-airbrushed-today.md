---
layout: post
title: "About a month in the making (design wise) & finally lasered yesterday, then airbrushed today"
date: October 04, 2016 15:07
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
About a month in the making (design wise) & finally lasered yesterday, then airbrushed today.



Way too much work involved haha.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Well, here's another part to my current project. Thanks again #LaserWeb for your spectacular job on engraving the imagery.



Image was processed in Photoshop first, from the colour image (see photos) to a greyscale image using Threshold (value 50, arbitrary value chosen by what looks good).



So, the size was just shy of 7 inch tall, 10 inch wide. Took about 3-6 hours engraving, as certain regions I had to redo (after super careful re-alignment) as the image came out too light for my liking. I think the main engrave (1st run) was about 3 hours (something like 2.9 million lines of gcode).



So, after all the engraving (yesterday), today I sat down for the last 3 hours airbrushing it. 4 colours, leather-dye (thinned 10:1 with Methylated Spirits:Dye).



I will share Original Image, Pre-Processed Image, After Engrave, After Airbrushing.



#Ortelius #1570 #WorldMap #LaserWeb #Leatherwork #Airbrush







![images/c26dee73c4c7f8553019b08ca4471480.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c26dee73c4c7f8553019b08ca4471480.jpeg)
![images/559e8b2a44afd76ea2ecc33701e67972.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/559e8b2a44afd76ea2ecc33701e67972.jpeg)
![images/9ba3c686ab1d0b36a988fdcf9f5ee992.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9ba3c686ab1d0b36a988fdcf9f5ee992.jpeg)
![images/a7c1e47ecd25d580bb0b964980e14478.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a7c1e47ecd25d580bb0b964980e14478.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Don Kleinschnitz Jr.** *October 04, 2016 15:17*

Stunning....


---
**Jim Hatch** *October 04, 2016 15:20*

Outstanding job! 👏


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 04, 2016 15:27*

**+Don Kleinschnitz** **+Jim Hatch** Thanks guys :) I'm thoroughly satisfied, although the colours only vaguely resemble the original.


---
**Anthony Bolgar** *October 04, 2016 18:25*

That is great work....very detailed, I love it!


---
**Alex Krause** *October 04, 2016 19:27*

Epic! Won't the dye darken up over time?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 05, 2016 05:50*

**+Anthony Bolgar** Thanks Anthony.



**+Alex Krause** Not this particular dye, it basically holds its colour well. Usually dyes that darken are known as "antiques", but they eventually stop darkening (when you wipe it off or seal/lacquer the leather).


---
**Bob Damato** *October 05, 2016 09:14*

Wow, thats beautiful! Im going home and throwing all my gear away. <b>waves white flag</b>


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 05, 2016 15:46*

**+Bob Damato** Send it my way ;) hahaha.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/3VsiqtHrwzi) &mdash; content and formatting may not be reliable*
