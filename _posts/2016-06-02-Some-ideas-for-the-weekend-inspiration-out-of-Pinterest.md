---
layout: post
title: "Some ideas for the weekend (inspiration out of Pinterest)"
date: June 02, 2016 15:41
category: "Object produced with laser"
author: "Stephane Buisson"
---
Some ideas for the weekend (inspiration out of Pinterest)



![images/2d8b8e99c7eadc7285038cca1cc7fac2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2d8b8e99c7eadc7285038cca1cc7fac2.jpeg)
![images/5e4654baab63dec16c8f2217006c59ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5e4654baab63dec16c8f2217006c59ce.jpeg)
![images/1402ab7c548a9f1f3b715e7f39bccea4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1402ab7c548a9f1f3b715e7f39bccea4.jpeg)
![images/f22f13786f50722a5b5fbb8a1f497d12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f22f13786f50722a5b5fbb8a1f497d12.jpeg)
![images/c740c422a45a2d58361308dc48f4b51c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c740c422a45a2d58361308dc48f4b51c.jpeg)
![images/150b3c1bc9ff65ba878508c9cdfee9e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/150b3c1bc9ff65ba878508c9cdfee9e9.jpeg)
![images/1ee60b59fe2dc21e85848da8d07bcc34.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ee60b59fe2dc21e85848da8d07bcc34.jpeg)
![images/c403d9208f617f186dfbee0b4c40722a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c403d9208f617f186dfbee0b4c40722a.jpeg)

**"Stephane Buisson"**

---
---
**Jim Hatch** *June 02, 2016 16:05*

Nice. A couple I'm inspired by now. So many potential things to make, so little time to make them.


---
**Ulf Stahmer** *June 02, 2016 16:52*

I love the bird house. My two cats, however, would fight over which one would dine there first!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 02, 2016 17:58*

The coat-hangers are cool. Looks like you could even put hats on the top of them.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/f42TT2AGtJG) &mdash; content and formatting may not be reliable*
