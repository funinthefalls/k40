---
layout: post
title: "Nate Caine Continuing discussion here...... ...... One open question in my mind is the tubes current regulation"
date: June 06, 2017 16:58
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
**+Nate Caine**

Continuing discussion here......



......

One open question in my mind is the tubes current regulation. It has been suggested that these supplies regulate the tubes current.



Its clear that the feedback circuit measures the current in the flyback primary and that adjusts the PWM. However I don't think that it really measures the actual current in the tube, unless there is some reflected effect from secondary to primary that I do not understand.



It seems to me that the current in the primary is a proxy for the current in the tube but not the real current, especially since the tube starts out as a high impedance and then goes negative resistance. 



Also I do not know if you have seen this post regarding my attempts to see what is in the flyback.



[https://plus.google.com/+DonKleinschnitz/posts/Knvjp8mcG2U](https://plus.google.com/+DonKleinschnitz/posts/Knvjp8mcG2U)





**"Don Kleinschnitz Jr."**

---
---
**Nate Caine** *June 06, 2017 17:13*

**+Don Kleinschnitz**​ I think that's what I mentioned before.  The HVPS control circuitry processes a variety of inputs (some for control, some for fault detection) and modulates the drive to the HV transformer accordingly.  



As mentioned in my description of some <b>OLD</b> HVPS, they <i>do</i> <b>directly</b> sample the laser tube return current.



On several <b>NEW</b> supplies they <b>indirectly</b> monitor the drive current to the HV transformer as a proxy for the actual tube current.  Remember, it is a <i>transformer</i>.  So just as it steps <b>up</b> the voltage (by the turns ratio), it also steps <b>down</b> the current (by the same turns ratio).  This seems imprecise, but they calibrate out much of the sloppiness at the factory.  That's what one of those adjustment pots do.  



As mentioned, some HVPS use both schemes (indirect transformer drive current, and direct tube current monitoring), and allow the user to select between the two options.



Regardless, most supplies monitor the drive current to the HV transformer anyway as a quick response to a faults--mainly a high current response to an HV arc to the chassis.  They also need to respond to an open circuit failure.  


---
**Nate Caine** *June 06, 2017 17:24*

Yes, I did see your "autopsy" posting, and was about to post.



I did a similar dissection of a HV transformer about a month ago, but got sidetracked and didn't have time to document my findings.



Anyway here's a photo for your amusement.



WARNING:  This is NOT that same as your transformer.

Details will vary.





What you see is a cross-section of the HV transformer.  

(Sorry, google seems to randomly rotate my photos.)

The transformer ferrite was removed.

The transformer milled down and cut in half.

The face wall milled down further, then sanded and lapped.



The turns on the right are the primary.  

There are 3 cables wound in parallel.  

26 turns each cable.

78 turns total.

These were counted and continuity checked.

Each cable is comprised of 32 strands.



The HV is divided into 4 sections on the left.

Roughly, 600 turns each section (estimated).

They partition the 4 sections for HV isolation.

Each section should be about 5KV.

The outer-most winding of one section, becomes the inner-most winding of the next section.

There is a groove to isolate the wire from section to section.

One turn would calculate to about 8V.

One row of turns would be about 170V---layer-to-layer.

![images/8667094b8b3662ff683618a464bf43c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8667094b8b3662ff683618a464bf43c0.jpeg)


---
**Nate Caine** *June 06, 2017 17:29*

View from behind.



At the top you can see 3 parallel HV rectifiers.

On the right you can see an additional 3 parallel HV rectifiers.

(More details in next comment--only one photo per comment.)



![images/55e98500c683988d911b5ad35131e11f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55e98500c683988d911b5ad35131e11f.jpeg)


---
**Nate Caine** *June 06, 2017 17:31*

View of integrated HV bridge rectifier and capacitor.



This is 1 of 2 series connected transformers in a two transformer 80W power supply.  

(This is <b>NOT</b> the single transformer 40W K40 supply.)



In this view, the lower left (blue wire) is the HV(-) output.

The upper right (red wire) is the HV(+) output.

[Because of the series connection, the HV(+) of one transformer is the HV(-) of the next.]



The bridge is the rectangle of HV rectifiers.

Each location is actually 3 HV rectifiers connected in parallel.

There is a total of 12 HV rectifiers.

The parallel connection allows for greater current capacity, and power dissipation.



The filter capacitor is diagonal and shown in blue and the internal dielectric material (cut thru by mill) is the "gold" color within the blue.





![images/e03824d7afa8462af720de45ac46d1f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e03824d7afa8462af720de45ac46d1f0.jpeg)


---
**Nate Caine** *June 06, 2017 17:48*

Cross-section of a single HV rectifier.

To achieve the HV result, 20 individual diodes are connected in series and packaged as a 20KV HV rectifier.

(The actual part number could not be read.)

  

For example, 20 diodes at 1KV each would create a 20KV PIV HV rectifier.  

However, the forward voltage drop would also be 20 diodes.

One would expect, perhaps 0.7V x 20 = 14V drop.  The measured value was around 34V, so there is more going on here than meets the eye...



![images/b3cfb31c1c1ec87b6361215564c3204d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b3cfb31c1c1ec87b6361215564c3204d.jpeg)


---
**Nate Caine** *June 06, 2017 19:12*

Technically speaking, this is <i>not</i> a "flyback" transformer, because (unlike a TV or CRT monitor) it is not being operated in flyback mode.



The drive circuity in the HVPS is a "push-pull half-bridge", because it uses two power transistors operating out of phase (the "push-pull").



As the schematic shows, inside the HV Transformer encapsulated housing is a full-wave bridge-rectifier with filter capacitor.



![images/f3c78b47140d4af8ce94c8fc0cc16dfb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3c78b47140d4af8ce94c8fc0cc16dfb.jpeg)


---
**Don Kleinschnitz Jr.** *June 06, 2017 20:30*

**+Nate Caine** nice work. I thought about a cross section but was afraid I would not see the underlying circuit. You solved the mystery of what the internal circuit is and saved me from opening another transformer.  

I imagined a double but the parallel diodes didn't make sense.

I hadn't considered a full wave bridge. 

BTW agree this is not a flyback I should stop using that term, but who noticed ;).




---
**Don Kleinschnitz Jr.** *June 06, 2017 20:40*

**+Nate Caine**​ I'm not sure I still have my head wrapped around the regulation schema.

I understand that the current in the secondary is smaller then the primary by the ratio. 

So are you saying that the primary current is set and therefore the secondary current is assumed?  



So when the tube discharges the secondary current is limited to a ratio of the controlled primary current? 


---
**Don Kleinschnitz Jr.** *June 07, 2017 11:02*

**+Nate Caine** Note: while reviewing this info last night I realized that the K40 HVT I disected has only 2 diodes in parallel unlike your 80W unit. I am going to cross section one of those diodes to see what it reveals.


---
**Nate Caine** *June 07, 2017 15:11*

**+Don Kleinschnitz**​ I was going to encourage you to continue your tear apart.  Now that you've seen photos of what's inside my similar transformer, you know where to dig and what to expect with yours.   At this stage I was using a handheld Dremel with a router bit (sort of) and was removing encapsulation material to follow each component wire lead.  Slow process and dusty (wear a mask!).



If you choose to cross-section the transformer  windings, initially I used a bandsaw, and then a hacksaw, before milling the face flat and sanding and polishing it (to count the wires).



You know the capacitor and rectifier component count for mine.  The number of diodes and capacitors you mention in your post don't match mine, so perhaps there's a different topology involved?  Especially the 2 capacitors.



I hope you do better than I did regarding component part numbers.  I'd love to know the HV rectifier number.  



Your HV transformer may use only 2 rectifiers in parallel, while mine uses 3, because of the difference HVPS rating.  My 80W claims 27mA (with 3 rectifiers in parallel).  A typical 40W is good for 20mA (which might correspond to 2 rectifiers in parallel).  That could be one explanation.



BTW, what is the manufacturer's part number on the transformers you have?



Do you have more photos?  In the past I saw many more photos of your power supplies (beyond those used in your blog posting) in your photo stream, but I don't have a working link to your photos now.  (Private Message me if you want.) 



We await the results of the autopsy!


---
**Nate Caine** *June 07, 2017 15:19*

**+Don Kleinschnitz** That's my current theory: laser tube current out of the the transformer, tracks the regulated drive current into the transformer.  I suspect they put on a test load, and then tweek the calibration pot (labeled "PL" for Power Level?).  That makes up for all the component tolerances.  The goal is to provide a transfer function such that a *IN *("intensity") input at 5V would produce a 20mA tube current output.  Note that there are a lot of other nonlinearities involved here, but they may not be important, as the laser user visually adjusts the machine pot to achieve the desired result.  So it's probably more important that the operation is <i>stable</i> rather than <i>linear</i>.


---
**Don Kleinschnitz Jr.** *June 08, 2017 12:38*

**+Nate Caine** funny how much on the same page we are. Yesterday I changed my mind and decided to do a second transformer. 



Guess what I found out? Paint stripper seems to dissolve the potting ..... our resident genius chemist **+Ned Hill** gave me that idea and at least on one sample it seemed to work. Needs to soak for quite a while, use gloves and leave outdoors. Multiple rinses and re-soaks are necessary. Will post pictures later today.



Now that I am on the same page on the current control I need to noodle how this drivers regulation acts when the tube enters ionization and the effect on the laser's power and in turn cutting and engraving. 



What still makes we wonder is that when the tube ionizes the secondary must see a low impedance and very high current that is not really controlled by the primary drive. To me this is like suddenly putting a short on the output of a HV power supply.



Then again, perhaps the bridge and filter cap would reduce the effect on the secondary.

 

..... 

This starts to close the chapter on my hunt for proof around the response of the K40 laser control system. I suspect it is much longer than what the Smoothie-like controllers can dish out. Running PWM control signals shorter than about 400us significantly reduces the effective resolution when engraving. However increased PWM control freq. indirectly increases effective cutting power by repeatedly dumping high power pulses.



Here is the link to my LPS album: 

[photos.google.com - K40 LPS](https://goo.gl/photos/iHXMYgDSd8BDyDyG8)


---
**Ned Hill** *June 08, 2017 13:37*

**+Don Kleinschnitz**  Glad the paint stripper worked.  Maybe it's not a pure phenolic resin after all.  Anyway, here's the chemical compatibility chart I was looking at if you ever need to for future reference.  [https://drive.google.com/open?id=0B9v2uXCddSXkczk0V0ZOakQ2YjA](https://drive.google.com/open?id=0B9v2uXCddSXkczk0V0ZOakQ2YjA)


---
**Don Kleinschnitz Jr.** *June 08, 2017 13:39*

This is what I used.

![images/4163609923b5ca9ce43a0702e8185711.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4163609923b5ca9ce43a0702e8185711.jpeg)


---
**Don Kleinschnitz Jr.** *June 08, 2017 13:40*

Disolving......

![images/6f75d45860e0ddad4bb3ee78be50a272.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6f75d45860e0ddad4bb3ee78be50a272.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/9n4YrQPNBRi) &mdash; content and formatting may not be reliable*
