---
layout: post
title: "Does anyone have any experience with the light object 600 water chiller?"
date: June 26, 2018 17:22
category: "Modification"
author: "Adam Hied"
---
Does anyone have any experience with the light object 600 water chiller? I gave up on the cw-3000 and have that unit on the way. Their site has good reviews but I’d like to hear from some other people. It is a refrigerant unit.





**"Adam Hied"**

---
---
**LightBurn Software** *June 27, 2018 01:07*

We have one on our K40 and have been quite happy with it. It’s relatively loud when the fan is running, and the fittings provided to attach the water hose were loose and required a lot of Teflon tape (provided) to keep it from leaking. That is a small thing you only need to deal with on setup - since then it’s been a solid unit, and we’re considering buying another for our larger laser.


---
**Adam Hied** *June 27, 2018 01:57*

Awesome!! Thank You!


---
*Imported from [Google+](https://plus.google.com/101438782220666562093/posts/5MGGYjvhMNs) &mdash; content and formatting may not be reliable*
