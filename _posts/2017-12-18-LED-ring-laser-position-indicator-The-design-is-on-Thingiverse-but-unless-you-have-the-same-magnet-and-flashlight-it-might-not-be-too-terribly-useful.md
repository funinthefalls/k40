---
layout: post
title: "LED ring laser position indicator. The design is on Thingiverse but unless you have the same magnet and flashlight it might not be too terribly useful"
date: December 18, 2017 03:13
category: "Modification"
author: "Scorch Works"
---
LED ring laser position indicator.


{% include youtubePlayer.html id="r4GfdS4GDqs" %}
[https://youtu.be/r4GfdS4GDqs](https://youtu.be/r4GfdS4GDqs)



The design is on Thingiverse but unless you have the same magnet and flashlight it might not be too terribly useful.  I think the concept is the gem here:

[https://www.thingiverse.com/thing:2720345](https://www.thingiverse.com/thing:2720345)



My controller board is dead so I spent the weekend doing what I could without it while I wait for a replacement to arrive.





**"Scorch Works"**

---
---
**Tech Bravo (Tech BravoTN)** *December 18, 2017 03:20*

pretty cool!


---
**BEN 3D** *December 18, 2017 07:47*

good idea!


---
**Jorge Robles** *December 18, 2017 07:53*

I was doing similar last weekend. Magnet is genious!


---
**Adrian Godwin** *December 18, 2017 09:01*

Very neat !

The dimmer leds on one side might be because your laser isn't exactly aligned with the centre of the last mirror - good enough for the narrow laser but not for the wider ring of leds


---
**Don Kleinschnitz Jr.** *December 18, 2017 11:30*

Clever.

Just a note; LED and Laser diodes do not necessarily point parallel to their body. Found this out when trying to use either as alignment tools. 

I have seen a similar design where a laser diode swings into the optical path (where it exits the laser compartment) when the cover is opened.


---
**Scorch Works** *December 18, 2017 15:46*

**+Adrian Godwin** that is very possible.


---
**Umar Haque** *December 18, 2017 19:55*

Excellent! Intelligent design! Can you let us know where you got the magnet and the LED's? And what you wired it to?


---
**Scorch Works** *December 19, 2017 02:55*

**+Umar Haque**​ The LEDs came from a "Guidesman 210-1004 14 LED" flashlight. (I am pretty sure my mother in-law got if free after rebate at a hardware store.) The magnet was salvaged from an old microwave oven.  The power supply was a 5V DC power supply from a flip style cell phone.  I wired a 180 Ohm resistor in series with the LEDs (the LEDs are all in parallel with each other). This was completely made with things I had in my parts bins.  The intent was to show the concept not the best way to make it.    


---
**Umar Haque** *December 21, 2017 15:24*

**+Scorch Works**: I love how you reuse and recycle parts! Just used an old Little Tikes bounce house fan to help evacuate the K40 after printing a fairly interesting attachment. Just needed to cut the plastic off of the bottom.

![images/cd2537024cebb547674dbc8543696140.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cd2537024cebb547674dbc8543696140.jpeg)


---
*Imported from [Google+](https://plus.google.com/+ScorchWorks/posts/4d77BF6PmZX) &mdash; content and formatting may not be reliable*
