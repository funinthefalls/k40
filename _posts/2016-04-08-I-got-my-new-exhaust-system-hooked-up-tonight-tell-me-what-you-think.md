---
layout: post
title: "I got my new exhaust system hooked up tonight tell me what you think"
date: April 08, 2016 03:26
category: "Discussion"
author: "3D Laser"
---
I got my new exhaust system hooked up tonight tell me what you think

![images/53086d9dbbf94e2c6098ff1e403b3789.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/53086d9dbbf94e2c6098ff1e403b3789.jpeg)



**"3D Laser"**

---
---
**Phillip Conroy** *April 08, 2016 06:41*

Nice ,love the smooth metal pipes ,with my set up i do not have to use air assist when marking the work at 2-4ma the suction is that good ,how is your fan


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 08:44*

Wow, That looks professional.


---
**3D Laser** *April 08, 2016 10:31*

To be honest I haven't tested it yet that is for this weekend.  I got it all ducted in last night about 11:45 and it was time for bed 


---
**I Laser** *April 08, 2016 10:39*

Looks very impressive. Hopefully works as well as it looks!


---
**David Cook** *April 08, 2016 15:01*

**+Custom Creations**


---
**Shane Graber** *April 08, 2016 15:47*

Can you give us some details on the setup?




---
**Whosa whatsis** *April 08, 2016 15:58*

My instinct would have been to keep the tube narrower, at least through the vertical section. That should increase air speed inside the tube and make any particles that find their way inside more likely to be carried through rather than pooling at the bottom and eventually obstructing airflow.


---
**3D Laser** *April 08, 2016 16:00*

The fan is a 8 inch 732cfm vortex S series fan.  Great set up the duct work mounts into the bracket so that the fan can be removed without taking the whole thing apart that way the fan can be easily cleaned.  I have the 8 inch pipe reduced to 4 inches at the vent and machine


---
**3D Laser** *April 08, 2016 16:21*

**+Whosa whatsis** I thought that to but I would advised it may actually cut the power I am going to try it this way and if I need to reduce the pipe I can 


---
**Whosa whatsis** *April 08, 2016 16:24*

It will reduce the overall airflow somewhat, but the air that does flow will have more power to carry particles with it rather than letting the heavy stuff settle to the bottom. I think that's a good tradeoff in this case.


---
**3D Laser** *April 08, 2016 16:52*

I can always return the 8 inch connectors for 4 inch if I find I need more power I am less concerned about partials and more concerned out fumes 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/9wqiYSQHp1A) &mdash; content and formatting may not be reliable*
