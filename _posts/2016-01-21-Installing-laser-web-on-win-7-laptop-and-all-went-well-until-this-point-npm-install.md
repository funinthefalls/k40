---
layout: post
title: "Installing laser web on win 7 laptop and all went well until this point npm install"
date: January 21, 2016 03:21
category: "Software"
author: "Andrew ONeal (Andy-drew)"
---
Installing laser web on win 7 laptop and all went well until this point npm install. As seen in pic when typed in cmd as suggested it gives an error npm not recognized as an internal or external command, operable program or batch. Any help would be awesome and thank you in advance for any assistance.

![images/dbb551bd7ab115b76b98e06f8f877592.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dbb551bd7ab115b76b98e06f8f877592.jpeg)



**"Andrew ONeal (Andy-drew)"**

---
---
**Tom Nardi** *January 21, 2016 03:30*

npm is part of Node.js, did you install that first?


---
**Andrew ONeal (Andy-drew)** *January 21, 2016 03:35*

Yes




---
**Andrew ONeal (Andy-drew)** *January 21, 2016 03:39*

Will reinstalling node.js, hurt anything or do I have start over? 




---
**Andrew ONeal (Andy-drew)** *January 21, 2016 03:58*

Checked install on node.js and found that it do not install correctly, thank you for the help. I'm going to start over with install process now.


---
**Andrew ONeal (Andy-drew)** *January 21, 2016 04:15*

All good, thanks again Tom. Can I ask why these programs laserweb, visicut, and others want to connect to web cam?


---
**Andrew ONeal (Andy-drew)** *January 21, 2016 04:50*

Thanks Peter, I'm all about the ground floor operations


---
**Manuel Conti** *January 21, 2016 07:52*

Install last version of node and git. After open prompt of git (not of windows) and follow the wiki instructions 


---
**Andrew ONeal (Andy-drew)** *March 26, 2016 03:17*

Peter I am just now messing around in laser web but realized I'm not connected to my laser. When I try to connect no serial ports appear, I can however connect through arduino. I've spent the last few hours scouring the web for a solution or common prob with another user but find nothing. What am I missing?


---
**Andrew ONeal (Andy-drew)** *March 26, 2016 05:22*

Awesome will check and ensure no other interface is running and I'm using win 7 on this project. Also do you know why Inkscape and actual print out are so off? Example 8"x8" in Inkscape prints on laser 4"x4", perhaps a setting I've got off?  It only does this when generating code via Inkscape plug-in?


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/GPgQEHna4ki) &mdash; content and formatting may not be reliable*
