---
layout: post
title: "Hello everyone, Anyone engraving leather here and could give me some tips?"
date: January 20, 2018 13:24
category: "Object produced with laser"
author: "Andrei Voicu"
---
Hello everyone,

Anyone engraving leather here and could give me some tips? 

Im engraving brown leather wallets and after engraving the is a lot of black residue and if i clean it the engraving is not as clear as it is with the residue i mean you can barely see anything. i read on other forums that they are cleaning the residue and the engraving is still clear, bu it looks like it doesnt work for me.



thanks. 



![images/65e747449f562b140080931ab06b1243.png](https://gitlab.com/funinthefalls/k40/raw/master/images/65e747449f562b140080931ab06b1243.png)
![images/5894a522f233516be59c7e567d3a080a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5894a522f233516be59c7e567d3a080a.png)
![images/64bd104f13df80c5d0ab48ae5cc66551.png](https://gitlab.com/funinthefalls/k40/raw/master/images/64bd104f13df80c5d0ab48ae5cc66551.png)
![images/fb19f790eea23fe5828a2e7ab0631c16.png](https://gitlab.com/funinthefalls/k40/raw/master/images/fb19f790eea23fe5828a2e7ab0631c16.png)

**"Andrei Voicu"**

---
---
**Andy Shilling** *January 20, 2018 13:40*

Lay some masking tape on it that will help.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2018 17:57*

I've done plenty of engraving on leather. Almost solely what I used it for when I first got my K40. From the imagery you provided, it looks like you're engraving on already dyed & sealed leather. That's likely the cause of your issue. All the leather I engraved was undyed, unsealed. So I would engrave, then wash the residue off with water & a soft brush. It would lose some of the detail, but nowhere near as much as what yours is showing lost.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2018 17:59*

Side note: I have tried engraving/cutting chrome dyed black leather with the K40 & other already dyed cowhide before. I found that a) it struggles to cut through it with same efficiency as undyed/unsealed pieces & b) the results were less than desirable.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2018 18:00*

Lastly, what method are you using to clean the residue off? You may be overcleaning it too.


---
**Andrei Voicu** *January 27, 2018 11:40*

**+Yuusuf Sallahuddin** i sell them and a person said it gone of in 1 day... strange but it never happened to anyone since then... 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 28, 2018 11:51*

**+Andrei Voicu** I would highly suggest after engraving, gentle cleaning with water & soft brush or cloth (that is not going to leave fluff everywhere). Then after that, seal the leather with a lacquer or sealer.


---
*Imported from [Google+](https://plus.google.com/113571233865169446047/posts/EQQHWk5GzNu) &mdash; content and formatting may not be reliable*
