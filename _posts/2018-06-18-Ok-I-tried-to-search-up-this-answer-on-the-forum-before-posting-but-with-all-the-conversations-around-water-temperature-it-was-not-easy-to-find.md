---
layout: post
title: "Ok I tried to search up this answer on the forum before posting but with all the conversations around water temperature, it was not easy to find"
date: June 18, 2018 20:26
category: "Discussion"
author: "Jeff Bovee"
---
Ok I tried to search up this answer on the forum before posting but with all the conversations around water temperature, it was not easy to find.



Can anyone tell me what the temperature of the laser is at the focus point?



Assuming laser is in good working order and properly aligned and focused.





**"Jeff Bovee"**

---
---
**Phillip Conroy** *June 18, 2018 20:50*

[https://www.eurolaser.com/customer-service/faq/how-hot-is-actually-the-laser-beam/](https://www.eurolaser.com/customer-service/faq/how-hot-is-actually-the-laser-beam/)

[eurolaser.com - How hot is actually the laser beam?](https://www.eurolaser.com/customer-service/faq/how-hot-is-actually-the-laser-beam/)


---
**Phillip Conroy** *June 18, 2018 21:06*

[https://www.answers.com/Q/How_hot_is_a_laser_beam](https://www.answers.com/Q/How_hot_is_a_laser_beam)

[answers.com - How hot is a laser beam](https://www.answers.com/Q/How_hot_is_a_laser_beam)


---
**Phillip Conroy** *June 18, 2018 21:07*

Google search knows everything


---
**Don Kleinschnitz Jr.** *June 19, 2018 00:39*

It depends on what's in the beam and for how long...

Why do you want to know? :)


---
**Jeff Bovee** *June 19, 2018 12:19*

Ok so I was watching a video on metal printing using laser and was wondering if a similar process would work with silica sand to create glass with laser.



[dms.licdn.com - dms.licdn.com/playback/C4D05AQESz8GcXna9Qg/0d684b47c97f4488bd9f5bf6271b822c/feedshare-mp4_500-captions-thumbnails/1507940118923-hysdc8?e=1529413200&v=beta&t=hJOiiAWoeIFk4e42vlwa2z_3CBscPAVP0fdTS-0kSec](https://dms.licdn.com/playback/C4D05AQESz8GcXna9Qg/0d684b47c97f4488bd9f5bf6271b822c/feedshare-mp4_500-captions-thumbnails/1507940118923-hysdc8?e=1529413200&v=beta&t=hJOiiAWoeIFk4e42vlwa2z_3CBscPAVP0fdTS-0kSec) 






---
**Phillip Conroy** *June 19, 2018 19:43*

Give it a try,can not harm laser machine 


---
**Ned Hill** *June 19, 2018 23:56*

I thought I remembered something being posted about this before.  [plus.google.com - Melting Sand With A Laser! Very interesting. http://www.williamosman.com/20...](https://plus.google.com/u/0/+HalfNormal/posts/5VgqR2k8YE3)


---
**Doug LaRue** *June 20, 2018 21:50*

if you can crank up the power just a bit, you can make some cool sand art - [blogs.scientificamerican.com - What Really Happens When Lightning Strikes Sand: The Science Behind a Viral Photo](https://blogs.scientificamerican.com/but-not-simpler/what-really-happens-when-lightning-strikes-sand-the-science-behind-a-viral-photo/)




---
**Doug LaRue** *June 20, 2018 22:34*

Can't you just put a thermistor on a piece of black anodized Al and measure it?


---
**Mark Brown** *June 21, 2018 02:05*


{% include youtubePlayer.html id="0O0k05_h89o" %}
[youtube.com - Can a Laser Melt Sand?](https://www.youtube.com/watch?v=0O0k05_h89o)


---
*Imported from [Google+](https://plus.google.com/115193415731522119744/posts/iSc2a9Wgza5) &mdash; content and formatting may not be reliable*
