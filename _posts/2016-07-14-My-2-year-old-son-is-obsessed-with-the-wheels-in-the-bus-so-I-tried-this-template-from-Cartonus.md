---
layout: post
title: "My 2-year-old son is obsessed with \"the wheels in the bus\" so I tried this template from Cartonus"
date: July 14, 2016 04:55
category: "Object produced with laser"
author: "Tev Kaber"
---
My 2-year-old son is obsessed with "the wheels in the bus" so I tried this template from Cartonus. Came out pretty good. 

![images/b2022406926259dc0002d34564cdce87.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2022406926259dc0002d34564cdce87.jpeg)



**"Tev Kaber"**

---
---
**Pippins McGee** *July 14, 2016 06:54*

good job - is the lines engraved or 'cut' with a low power so as to only score the surface


---
**Tev Kaber** *July 14, 2016 11:30*

They are engraved but I will try a low power "cut" next time, vector is so much faster. 


---
**greg greene** *July 14, 2016 12:48*

we should set up a repository for all these good designs !


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/1Rc39ZUoN5q) &mdash; content and formatting may not be reliable*
