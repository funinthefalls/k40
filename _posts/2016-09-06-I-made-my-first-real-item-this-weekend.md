---
layout: post
title: "I made my first real item this weekend"
date: September 06, 2016 14:55
category: "Object produced with laser"
author: "Jeff Johnson"
---
I made my first real item this weekend. I earn a little extra toy and vacation money with leather work. Wallets are the most common things I make and I've been wanting to replace some paper templates with acrylic. After hours of testing I came up with this - it's one of my more popular designs. I've had several requests over the years for templates so I suppose I'll be selling these as well once I perfect them.



for the text and instruction images, I converted the vector to bitmap and then back to line drawing for a nice low res single line vector. I cut it fast, 25mm per second at about 2.5ma and it makes a nice engraving without having to wait 10 minutes or more for an actual engraving.

![images/636990097ed86a9c5f475ea5f7ebe8d3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/636990097ed86a9c5f475ea5f7ebe8d3.jpeg)



**"Jeff Johnson"**

---
---
**Ariel Yahni (UniKpty)** *September 06, 2016 15:03*

I am addicted to wallets, the latest I bought is made of tyvek and it's just about to die. I would love to make my own ones. 

![images/c869958ef4a370c0296094dfa6c7a702.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c869958ef4a370c0296094dfa6c7a702.jpeg)


---
**Jeff Johnson** *September 06, 2016 15:11*

They're fun to make, for me anyway. I started by taking apart some cheap wallets and seeing how they're made. Then I discovered all the wonderful videos on YouTube.


---
**Ariel Yahni (UniKpty)** *September 06, 2016 15:20*

So you use those templates to mark on top of leather and the cut it manually? 


---
**Jeff Johnson** *September 06, 2016 15:23*

yes, I mark the back side of the leather and then cut with a razor. I use a steel ruler to make sure I cut straight lines. If you're careful you can use the template to brace the razor but you have to make sure you don't cut into it.A really sharp razor is important. I use a new blade every couple wallets.


---
**Bob Damato** *September 06, 2016 17:41*

To follow up with **+Ariel Yahni**'s comment.... how about using the laser to cut the leather directly?




---
**Jeff Johnson** *September 06, 2016 20:15*

 I'm still testing that out. Right now it leaves the leather with an awful burning smell but it gets a little weaker after a few days. I'm still playing with power and speed to see if I can find the right combination to make this job easier. No one will buy a smelly wallet just because it took me less time to make.


---
**Ariel Yahni (UniKpty)** *September 06, 2016 20:17*

**+Yuusuf Sallahuddin**​ could have good imput on leather treatment for the smell


---
**Jeff Johnson** *September 06, 2016 20:20*

I would love some tips on removing that burnt smell. I primarily use oil tanned leather for my wallets and that may be partly to blame for the burnt smell.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 07, 2016 10:14*

**+Ariel Yahni** **+Jeff Johnson** I find the smell is particularly bad with lasering suede. I tested bi-carbonate of soda in a container (can't remember who suggested it here, possibly Alex Krause) & it did weaken the smell, although I left it in there a week or so & forgot about it.



Usually the smell doesn't exist after dyeing, lacquer/sealing & using the Bick4 Leather Conditioner (I always put some on my job after it's completed).



edit: I generally use veg-tan leather. Thicker leathers are a prick to cut through. My leather supplier has an epilog laser & tells me he uses max power on his laser & whatever speed is the minimum mm/s that he can get through the material. He says he can get through up to 3mm with no problems. I find certain pre-dyed leathers will not cut without like 20 passes (or more sometimes). Thinner stuff, like 1mm kangaroo hide, will cut easily @ 20mm/s & 6mA.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/DrNKK3pj6Kr) &mdash; content and formatting may not be reliable*
