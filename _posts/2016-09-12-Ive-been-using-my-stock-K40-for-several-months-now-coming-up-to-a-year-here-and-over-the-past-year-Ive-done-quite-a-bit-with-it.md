---
layout: post
title: "I've been using my stock K40 for several months now, coming up to a year here and over the past year I've done quite a bit with it"
date: September 12, 2016 20:18
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
I've been using my stock K40 for several months now, coming up to a year here and over the past year I've done quite a bit with it. The only modification I've done was to remove the exhaust funnel that was getting in the way, and I've added an airbrush pump for an air assist. However, I quickly realized that the airbrush pump overheats (and shuts down) when I'm running a long cutting job. But I managed to deal with it for this long by attaching a regular 80mm PC fan on the back to help keep it cool.



However, I have several items on the list to be done, and I am having a hard time putting them in a order of importance ... what first, second, etc., etc. The air assist for me was important, however now that I have the airbrush pump, it's ok and I can deal with it, but it's on the list to be upgraded as well.



So I'm wondering, if you were to put the following on a list of most to least importance, <b>knowing that they will all get done eventually, just not all at once</b>, how would you proceed (and why would you sort them the way you did):



- Install Z-table

- Upgrade controller (plus other electronics, such as flow sensor, internal lights which probably also means a secondary PSU)

- Upgrade air assist (a larger tank that won't need the pump to run continuously)

- water chiller (using frozen bottles of water only gets me so far)

- upgrade exhaust



There's probably more that I'm forgetting right now ... but let's start with that.





**"Ashley M. Kirchner [Norym]"**

---
---
**Scott Marshall** *September 12, 2016 20:37*

#1 Exhaust AND safety mods - interlocks etc, #2 Air Assist, #3 Electronic Mods. 



Table depends on what you do. I've found shims and some expanded metal suit me fine (but that's with a nice adjustable focus air assist head) It's my opinion an autofocus table on a k40 is like radar on a rowboat. (but that's fine if you like it)



SImple seems to work well (for me anyway) 

The controller really is a new software option, as the big difference isn't effected by the controller you add, but by the software it allows you to run. Play with different software 'offline' and see if it seems it suits you, and will make a big difference to your capabilities. If you're already able to do what you want, you probably won't be able to justify a change. If you would love to do super detailed photo engraving or such, the controller upgrade is the path there.



If you enjoy small electronic mods, watch my site, I've got some cool stuff coming, some convenience oriented, other stuff simple safety and then there's the just plain cool stuff. Lights, meters, alarms, all will be available in kits from bare board to plug in. (There are a couple of different cooling systems ready to go and even a table kit coming soon) Schematics will be free on my site if you want to roll your own. It's taking longer than I want to get this stuff all out there, but it will be showing up soon. If you have a project going in the meantime, and need a hand or parts, send me a message and I'll do what I can to help you out.



Above all, have fun with it.



Scott


---
**Ashley M. Kirchner [Norym]** *September 12, 2016 21:02*

Yeah, I don't care for the door interlock to be honest with you. It'll just get in my way. However the flow sensor and water temperature are way more important to me. Being able to prevent the thing from running if the water isn't flowing or too hot is a big one. And while I can hack the thing and add in my own controller to cut off power of those requirements aren't met, I figure there has to be something already built for this. Not that I don't want to spin up Eagle and create my own boards but why re-invent the wheel? :)



My biggest thing is the controller and subsequent software as you pointed out. I work exclusively with Autodesk products (in CAD), and Illustrator. And right now, while this works, it seems rather dumb to me that the LaserDRW software wants a BITMAP instead of a vector file for cutting. I'd be happy if I can either push a file out directly out of Illustrator, or be able to import vector files directly, such as a DXF file. And if I can have the thing visible on my network so I can transfer files directly to it, that's even better. But those are bonuses ...


---
**Ashley M. Kirchner [Norym]** *September 12, 2016 21:03*

By the way, thanks for your input, I really appreciate it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 12, 2016 22:14*

Since you already have a semi-suitable air-assist system, I will put that at the bottom of my list. So my order of importance (in my opinion) is as follows:



- Water Chiller, for me the temperature of my water is never really an issue as it is in my garage (below the house) & super cold down there most of the time anyway. But for you, if temperatures require frozen bottles of water, then something able to maintain a stable temperature would be good & a high priority.

- Controller, as you mention you would like to be able to directly import vectors. You could do that with the m2nano if you used the Corel Draw/CorelLaser plugin combo. (I used to use AI9 format for Corel Draw 12, then SVG format for Corel x5 & x7).

- Z-table would be next on my list, due to the fact that I do cut/engrave on thicknesses of material from 1mm to 18mm. I would probably engrave on even thicker stuff if I could easily adjust my focal point (either Z-table or adjustable focus lens mount like Scott mentioned).

- Exhaust would come next, as for me I haven't really got too much of a problem with exhaust currently since I am working in a 3 car garage with plenty of ventilation & rarely cut anything more toxic than wood/leather (although the thicker leather stinks like burning hair).

- Lastly, Air-Assist, since I also have a similar issue with pump cutting out after a certain amount of time. I bought an airbrush compressor with 5L tank to use but isn't suitable, so I'm still using my two weak aquarium pumps which shut off after about 10 mins. I just have to keep an ear out & turn them back on if it's in the middle of a job (luckily they are loud as all hell).


---
**Ashley M. Kirchner [Norym]** *September 12, 2016 22:29*

Yeah, CorelDRAW isn't an option for me. One, I won't run pirated software, and two, even when I tried, the CorelLaser plugin crashed as soon as I opened it. And there's really no sense in buying another graphic program when I already have the full Adobe quite. And I'm happy exporting DXF files straight out of CAD. There's no reason the machine would need a bitmap file to do vector cutting, that's just dumb.


---
**greg greene** *September 13, 2016 00:13*

I did the following



1. Air assist head better lens and mirror from LO

2. Air pump - industrial model - noisy as heck - but made for constant running

3. Tossed the old bed, made new one out of mesh on paint stands

4. Chiller - not sure if better to cool the bucket then pump it - or run from the bucket through the chiller into the tube

5. Ordered a mechanical z table - will look at putting a stepper on it if I get a smoothie board - or just a servo and use a pwm generator.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/1Db9y4ZnqR6) &mdash; content and formatting may not be reliable*
