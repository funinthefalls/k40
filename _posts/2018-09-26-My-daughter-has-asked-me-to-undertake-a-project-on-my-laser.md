---
layout: post
title: "My daughter has asked me to undertake a project on my laser"
date: September 26, 2018 14:33
category: "Discussion"
author: "timb12957"
---
My daughter has asked me to undertake a project on my laser. Now granted what she wants I could just buy her one for about $35. But we all know that is not the way of us laser geeks! But I need some direction on how to arrive at a working pattern. The item she wants is a set of Mickey Mouse style ears, with cutouts in the ears of Mickey and Minnie, and Cinderella's castle. I have found online silhouettes  that would fit the bill for the cutouts. I was able to use inkscape to do a bitmap trace of the silhouettes. I used Draftsight to do a dxf of the headband and the ear frames. I imported the dxf frame into inkscape, and the silhouettes.  Then I got stuck. I have not been able to figure out how to integrate the individual components and create the desired cut path. Can someone offer some guidance or maybe even suggest a better software to accomplish my goal? I will need to end with a SVG file to use with K40 Whisperer. Thanks



![images/1d05ac290afaac5dde9c09aa51b43fff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1d05ac290afaac5dde9c09aa51b43fff.jpeg)
![images/f0ae7c28f68dd349bae28b35bb1a9ca1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f0ae7c28f68dd349bae28b35bb1a9ca1.jpeg)
![images/232d19e842403447dc09df9199103217.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/232d19e842403447dc09df9199103217.jpeg)

**"timb12957"**

---
---
**James Rivera** *September 26, 2018 18:49*

Are you using K40 Whisperer? If so, this link should have all the directions on how to get the Inkscape file configured.

[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
**timb12957** *September 26, 2018 18:58*

My issue is not really with getting compatible files from Inkscape to Whisperer, rather how to join the dxf and bitmap traced portions of my project in Inkscape


---
**Luis Sol** *September 26, 2018 18:59*

hello, just trace the outlines, convert the traced lines to paths selecting them all and put them in small line weight and rgb red 255,0,0


---
**timb12957** *September 26, 2018 19:55*

That has not worked for me either. May I ask that someone try this. Save the silhouette of Mickey and Minnie.  Import it into InkScape. Select it, then do a Trace Bitmap, Edge Detection.  Delete the original black silhouette. I get an inner and outer tracing when all I want is an single line tracing. Try changing the line weight to something small, say .005 inches.  That in itself does not work for me. Also note the fingertips are open. I want to close them.


---
**Justin Mitchell** *September 27, 2018 09:12*

Why are you using edge detection for the trace ? use brightness cutoff.  You get a single solid object then, if you really need it outline set a line colour and change the fill to none. 




---
**timb12957** *September 27, 2018 23:15*

**+Justin Mitchell** I think you just hit on my solution! As far as why I was using edge detection, I thought that was the way to detect only the edges. Made sense to me! But your reply put me on the right track! Thanks!!!


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/jSqQNWz9cgK) &mdash; content and formatting may not be reliable*
