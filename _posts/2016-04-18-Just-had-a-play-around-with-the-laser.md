---
layout: post
title: "Just had a play around with the laser"
date: April 18, 2016 23:06
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Just had a play around with the laser. I created a new carriage plate for it yesterday to mount a webcam on. Unfortunately, didn't take into account the webcam needs angling to be able to see where the laser is cutting/engraving. Will have to make a new revision later.



Anyway, in the process of testing, I decided to do another Vector Engraving test. **+giavonni palombo** you can check it out here.



I found that this works reasonably well (for Adobe Illustrator CC 2015):



- Create rectangles, 0.25mm height x 300mm width (the entire artboard)

- Space them apart 0.05mm, covering the entire area you need to work on (I used only about 80mm of height worth)

- Select all of them & combine them into single path (Object > Compound Path > Make [Ctrl + 8])

- Create your vector image (I used text as an example/test)

- Again, combine together into single path (Object > Compound Path > Make [Ctrl + 8])

- Set the image layer as the top most layer

- Select both the image layer & the rectangles layer & create a clipping mask (Object > Clipping Mask > Make [Ctrl + 7])



- Save as AI9 format, import into CorelLaser.

- Use cut, try keep speeds lower than 30mm/s (as I used that & it vibrates a lot on small objects). You may have to lower your power to something like 4-6mA, as it burned all the way through 3mm ply @ 10mA & 20mm/s.



![images/78bcc141f5416c2e5b6ace33332066f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/78bcc141f5416c2e5b6ace33332066f0.jpeg)
![images/6a07526e1d737d2f9fed3c121b6d6107.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6a07526e1d737d2f9fed3c121b6d6107.jpeg)
![images/a4be46c40e47608a3c17d8d8404dd964.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4be46c40e47608a3c17d8d8404dd964.jpeg)
![images/b9689af82a72a741ebed9e7f149f7cf9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b9689af82a72a741ebed9e7f149f7cf9.jpeg)
![images/83867fd718b9469825da2066773c023b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/83867fd718b9469825da2066773c023b.png)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2016 23:12*

I forgot to mention, I added a second copy of the image/text directly over the top of the clipping mask version. The 2nd copy was no fill, 0.01mm black border. This is to give smoother edges on the vector-engrave, as it looks pixelated if you don't.


---
**Blake Ingram** *May 01, 2016 18:50*

Hello Yuusuf, It's my first day here and I just received my k40 this past Friday (not even up and running yet)... I've noticed in two different post where you posted images of your engravings that all your lines are wavy and spotty in the fill areas, I was wondering, is this what's to be expected from all k40's out of the box? How do you get straight clean lines with this machine? I have previous experience with other more expensive Co2 lasers and have never seen this in those machines...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 22:36*

**+Blake Ingram** Hi Blake. Ah, that's me playing around with "vector engraving" which is not a feature that K40 supports. Normal engraving or cutting from the K40 is actually fairly straight. When I was doing the "vector engraving" (which is just a mock up by cutting with low power & high speed instead of using the engrave function) the machine likes to vibrate quite a lot (at any speed > 25mm/s) because the lines that it is cutting (using the method I was testing) are so small & close & it goes forward-stop-back-stop cycle too quickly & suddenly stopping. This vibration then seems to cause major issues (i.e. mirrors can vibrate out of alignment, x or y belts can come loose & require tightening). I believe in the case of mine having wavy lines, this is due to the x or y belts coming slightly loose & needed tightening.



What you can expect from the K40, straight out of the box, is firstly that you'll probably need to align all the mirrors & possibly even need to flip the lens up the other way (mine & a few other people's came upside down). After that is sorted, you can expect that the lines will be quite straight when engraving/cutting. Engrave max speed is 500mm/s (which I tend to use because it's so slow otherwise) & cutting speeds are something to be careful on. Some pieces I can use cutting speed of say 50mm/s (e.g. just cutting a plain rectangle). But cutting small objects (i.e. <10mm) and curves I'd advise 30mm/s or less (unless you want to realign mirrors/tighten x-y belts regularly).


---
**Blake Ingram** *May 01, 2016 22:43*

Thanks


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XkHUWqPnPLF) &mdash; content and formatting may not be reliable*
