---
layout: post
title: "Has anyone used one of these connectors, and if so, how does it assemble correctly?"
date: July 26, 2016 12:26
category: "Modification"
author: "Anthony Bolgar"
---
Has anyone used one of these connectors, and if so, how does it assemble correctly?

![images/565c3da94c4798d2703bac146d42c651.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/565c3da94c4798d2703bac146d42c651.jpeg)



**"Anthony Bolgar"**

---
---
**Scott Marshall** *July 26, 2016 15:39*

Hard to tell from that shot, but it looks like a psuedo mil-spec style common with IBM production equipment (Tools they call them) 20years ago anyway.



 Does it have small gold pins (usually male in the connector body and female in the panel mount half (they do sell inline versions too)?



If those are the ones, the pins have a solder bucket and you need a press in punch that has a tubular tip. It looks a lot like very tiny nutdriver. The pins are arranged in concentric circles and versions up to 90 pins or so are available. There are blank pins available (plugs) to create specific non-interchangable connectors.



The pins and tool are quite pricey. (connectors aren't real cheap for what they are either)



For assembly of 1 or 2, you can make do with a small screwdriver and a lot of patience. Removing the pins once installed is a bear. The tool for that is a godsend, but also insanely priced.



They're sold by AMP, JST and others. I've seen them in Black a lot. 

 I'm not sure who actually created them, over the last 20 years everybody is making copies of everybodys stuff without any foul so it seems.



We called that style - econo-mil, but the actual name is just a 14 to 20 digit part number, as is the case with most styles theese days. NJo longer can you say DIN plug and expect the other guy know what you mean.



Hope what I have helps a little, running down unknown connectors is one of the hardest jobs in electronic design these days, There's NO standardization beyond individual users.



Scott


---
**Anthony Bolgar** *July 26, 2016 16:12*

It is supposed to be to connect your HV line to the laser tube (easier to put the conneector in than rewiring a tube if the PSU fails) Just not sure if I need to fill the inside with silicon (the black boot part) to prevent arcing.


---
**Scott Marshall** *July 26, 2016 16:24*

Ok, I've seen those. Basically they are the same housing as the Semi-mil multi pin but with a single pin, Some have a gland in the body and silicone o-ring between the halves to keep the HV from arcing out. If asssembled with clear silicone grease (Automotive dielectric grease or Dow Corning Labcock grease), they should work quite well. I agree something is needed to make disconnecting the power supply easier for those of us who take them apart a lot. 



These would seem to be a good fit, if the price is reasonable, not the $40+ of the ones I was talking about.



I think I'd wrap it with silicone self vulcanizing electrical tape (the grey stuff with the liner) just to be sure it won't arc out and maybe get me if I reach in there with the power on (not that you (or I) should).



If you could please let me know where you found them, I'll try a pair and see how they work.



Thanks, Scott


---
**Anthony Bolgar** *July 26, 2016 16:35*

[http://www.ebay.ca/itm/182006489716?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.ca/itm/182006489716?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Anthony Bolgar** *July 26, 2016 16:36*

That is the one I purchased.


---
**Scott Marshall** *July 26, 2016 16:36*

Thank you Sir!



Ordered one. For 8 bucks I'll bite.


---
**Sunny Koh** *July 26, 2016 18:29*

**+Scott Marshall** It seems that my 50W PSU has one already connected so it is a standard, now if I can get a few for the 40W Laser, it would be good to get a proper wiring guide on how to do one of these.


---
**Scott Marshall** *July 26, 2016 22:11*

If they work out, I'll be glad to publish a short how--to on the connector assembly. It's not that complicated. I have your e-mail and I'll make a note to drop you a line when it comes in.



Good to know they are standard on some of the larger units,  that may make power supply selection easier. It would be nice to eliminate the wind and silicone deal unless you are changing a tube.



Good find Anthony!


---
**Scott Marshall** *July 26, 2016 22:15*

Here's a rough guide of the materials to look for for a extra secure install. Grease the inside and wrap the outside (belt and suspenders for high voltage is good with me)



Scott



[http://www.ebay.com/itm/Permatex-22058-Dielectric-Tune-Up-Grease-3OZ-tube-/281645382247?hash=item41935f7a67:g:OnAAAOSwqu9VF3Ya&vxp=mtr](http://www.ebay.com/itm/Permatex-22058-Dielectric-Tune-Up-Grease-3OZ-tube-/281645382247?hash=item41935f7a67:g:OnAAAOSwqu9VF3Ya&vxp=mtr)



[http://www.ebay.com/itm/Silicone-Self-Vulcanizing-Fusing-Sealing-Tape-Red-1X10-Made-In-USA-/370575600727?hash=item564806ec57:g:A1UAAOSwkZhWTAZW&vxp=mtr](http://www.ebay.com/itm/Silicone-Self-Vulcanizing-Fusing-Sealing-Tape-Red-1X10-Made-In-USA-/370575600727?hash=item564806ec57:g:A1UAAOSwkZhWTAZW&vxp=mtr)


---
**Mike Mauter** *July 27, 2016 01:20*

Looks like they have the Silicone Tape at Home Depot and Lowes.  



[http://www.homedepot.com/p/Nashua-Tape-1-in-x-3-33-yd-Stretch-and-Seal-Self-Fusing-Silicone-Tape-in-Clear-1210364/203534911](http://www.homedepot.com/p/Nashua-Tape-1-in-x-3-33-yd-Stretch-and-Seal-Self-Fusing-Silicone-Tape-in-Clear-1210364/203534911)






---
**Scott Marshall** *July 27, 2016 02:41*

They do. I've seen it in NAPA and other car places as well. It used to be an industry specific material, and kind of pricy, but since the infomercial guys started selling it in colors for fixing your garden hoses and 1001 uses, the price has come down and they sell it all over.



For this use, I'd prefer the real thing however, not the buy one get one in 12 sexy colors kind.



Scott


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/VWhd9ksAtZh) &mdash; content and formatting may not be reliable*
