---
layout: post
title: "Cutting acrylic hooo ! The odor Today was my first day with my first laser machine"
date: December 06, 2015 05:07
category: "Discussion"
author: "Gilles Letourneau"
---
Cutting acrylic hooo ! The odor



Today was my first day with my first laser machine. 



I wanted to start my tests but I didn't have a six inches pipe for the vapour evacuation.   I took instead a 3 inches with a reducer and start cutting acrylic. 



Well it look like my idea was not good.  I knew that 3 inches was not enough but I was surprise to see how bad acrylic smell during cutting. 



Question 



Are you using your laser machine in your home and do you find that the six inches evacuation pipe is good enough ? 



To install the six inches pipe I will have to modify a window and replace the glass by a plywood and cut a six inches circle in it for the pipe to go out. 



I hope it's going to be good enough.  



If somebody find that's it's not enough and have a better setup, tell me.









**"Gilles Letourneau"**

---
---
**Sean Cherven** *December 06, 2015 12:57*

That's not acrylic your cutting. It's most likely plexiglass. Acrylic is hard to find at local hardware stores.


---
**Scott Thorne** *December 06, 2015 13:32*

I have to order from Amazon...I can't find it locally.


---
**Scott Thorne** *December 06, 2015 13:33*

I've never used mine in my house...I use it in my garage so I would not know...sorry.


---
**Sean Cherven** *December 06, 2015 13:49*

I've used it in my house, and acrylic doesn't have much of an odor. Plexiglass or polycarbonate, on the other hand produces harmful fumes, and smells horrible. Don't breath that crap in if you wanna keep your lungs.


---
**Brooke Hedrick** *December 06, 2015 14:23*

This is interesting.  I am cutting acrylic in my garage and it does smell pretty bad - especially compared to cardboard and wood.  I am using the sized vent supplied and vent it outside via a hole in the garage wall.



I know the main issue is that the fan that comes with most k40 machines is pretty weak. I bought a replacement on Amazon and just need to build a bracket to fit the back of the k40.


---
**Sean Cherven** *December 06, 2015 15:16*

Are you sure it's Arylic? If you goto a hardware store, and ask for Acrylic, most people think of Acrylic as the same thing as Polycarbonate (Plexiglass).



The fact is Acrylic and Polycarbonate all look the same, but in reality, you don't wanna be cutting Polycarbonate without some serious ventilation.


---
**Scott Thorne** *December 06, 2015 15:18*

To be honest....I can't even cut poly...or plexiglass very well....I can cut acrylic just fine...but the black soot that comes from plexiglass is deadly.


---
**Brooke Hedrick** *December 06, 2015 17:28*

I am sourcing my acrylic from multiple eBay sellers.  I don't have any info to dispute it being acrylic, nor can I prove it is.  I don't see black smoke or soot.  I see fine wisps of white smoke.  Maybe my version of a bad odor would pale in comparison to plexiglass burning.  I have no reference.



Is there a way to verify the acrylic I have purchased is acrylic?


---
**Sean Cherven** *December 06, 2015 17:31*

Not really.. sometimes on the plastic/paper backing it will say what it is, but not always.


---
**Jim Fong** *December 06, 2015 19:16*

Wait I thought plexiglas is acrylic according to Wikipedia.  Polycarbonate also know as lexan is what I was told not to cut on the laser. 


---
**Sean Cherven** *December 06, 2015 19:22*

Plexiglass is a brand name, they probably make both. Lexan is also a brand name, and may also make both.


---
**Jim Fong** *December 06, 2015 19:46*

The stuff I bought is labeled "Plexiglas Acrylic". I assume it is safe to cut. 


---
**Brooke Hedrick** *December 06, 2015 19:50*

Hey Sean,



Where do you get your acrylic.  A low odor option is interesting to me.


---
**Sean Cherven** *December 06, 2015 21:41*

I get mine from here:

[http://www.amazon.com/gp/product/B009AEAFIE](http://www.amazon.com/gp/product/B009AEAFIE)


---
**Brooke Hedrick** *December 07, 2015 00:46*

Thanks for the link Sean.


---
**Sean Cherven** *December 07, 2015 01:03*

**+Carl Duncan** Plexiglass (Polycarbonate) will cut, but it produces very toxic fumes.


---
**Gilles Letourneau** *December 07, 2015 02:47*

Hi,



About the acrylic odor.



I was out for the day and spend the evening working on my new evacuation system so that,s why I took that long to get back to you multiple reply.





My original question was more about:



Do you use your laser in your house ?



Do you have a good evacuation system ?



My laser come with a 6 inches hole but I install a 3 inches hose like my 3D printer.



the result was really not good so today I went to the hardware store and bough everything I need to install a six inches hose evacuation system and it's perfect.



Now if I cut acrylic or wood there is almost no smell at all.



Bottom line, if your laser is equipped with a six inches hole and fan, install a six inches hose :-)





Acrylic



The acrylic was sold to me by the seller of the laser machine.  I don't know if it's real acrylic.



My home hardware (Home Depot) sell acrylic. I bough some transparent (4.5 mm 24 X 36 inches) 20 $  I didn't try to cut it yet but I will tell you soon if it smell as bad as the acrylic I received with the machine.



I will post some pictures of my installation very soon.  For now I'm testing the engraving wood with different power settings and speed.  I'm a rookie so I start from scratch.



Thanks for your feedback.


---
**Sean Cherven** *December 07, 2015 04:15*

If you bought it from Home Depot, it's likely Polycarbonate. The home depots near me never carry Acrylic.


---
**Sean Cherven** *December 07, 2015 04:16*

And yes, I run the system inside my home. And yes, i have upgraded the exhaust system.


---
*Imported from [Google+](https://plus.google.com/101789423601866268108/posts/j5kg9E9Aj1p) &mdash; content and formatting may not be reliable*
