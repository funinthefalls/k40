---
layout: post
title: "I've been looking at new psu's for when I upgrade and was wondering is it worth going for a 50watt?"
date: November 22, 2016 11:53
category: "Modification"
author: "Andy Shilling"
---
I've been looking at new psu's for when I upgrade and was wondering is it worth going for a 50watt?

 I've seen a few comments online about fitting a more powerful tube. Is this do-able and is there any gain in doing it? I know the case would need modification but that's the simple bit.







**"Andy Shilling"**

---
---
**Paul de Groot** *November 23, 2016 05:48*

Apperantly our 40 watt tubes are actually 30 watt. So upgrading might be useful especially when cutting thick materials. Beware these tubes are longer so you might need to adapt the laser case.


---
**Andy Shilling** *November 23, 2016 07:19*

**+Paul de Groot**​ Thank you, I thought as much but being new to this it's always best to ask 


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/C5raXWcZFWi) &mdash; content and formatting may not be reliable*
