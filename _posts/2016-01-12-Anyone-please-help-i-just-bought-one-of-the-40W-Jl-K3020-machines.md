---
layout: post
title: "Anyone please help, i just bought one of the 40W Jl-K3020 machines"
date: January 12, 2016 22:10
category: "Hardware and Laser settings"
author: "Randy Powell"
---
Anyone please help,

 i just bought one of the 40W Jl-K3020 machines. Went through the unboxing and setup. I Also go a tiny envelope with a limit switch in side and a note that says. If laser head cant go back to its original start position please replace this part. included was a mini limit switch. 

Sure enough when i power on the machine the laser is trying to go way beyond the limit of the Y axis. My problem is i have no idea where to find the installed limit switch to replace it.



machine is by Lihuiyu studio Labs



![images/11b7be71af58bafccdd81cc152d0b313.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/11b7be71af58bafccdd81cc152d0b313.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-0099wxFdpSw/VpV5j0B8UjI/AAAAAAAAAJA/QK68wJ0DP4c/s0/20160112_165217.mp4.gif**
![images/04d44e9ad38bf1640ec1cfdd6cc32cb9.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/04d44e9ad38bf1640ec1cfdd6cc32cb9.gif)

**"Randy Powell"**

---
---
**Coherent** *January 12, 2016 22:49*

The D40's normally have optical (not mechanical)  limit switches that (when the unit is turned on), set your home or zero for the X & Y. (in your video that would be the top left corner). The unit then uses the settings in the software to know the extents/limits it can travel in each direction.  First make sure you have the correct machine selected in the software. (most of the recent models end in M2) Then set the X & Y distance limits lower. I'm not at home right now or I would give more specific instructions but it should be easy to find the boxes with these settings.


---
**Stephane Buisson** *January 12, 2016 23:47*

hi **+Randy Powell** , welcome here.

the symptom shown in your video is not related to limit switch. the limits switches are for X min Y min (0,0) or home, top left position. your video show a problem with X max, down left.

I would bet for a motor connected the wrong way around.

the first thing the machine should do when power on is to home (0,0). Not going down.

When you are sure about your switches, you can use your spare one as a security door switch ;-))


---
**Sunny Koh** *January 13, 2016 06:59*

Check if the arm is square to the rail, had the same problem and on Monday they told me to twist it to square it.


---
**Randy Powell** *January 13, 2016 15:42*

Im currently using LaserDraw and Ive made the changes to set the origin of X&Y to 0,0. and that hasnt solved the problem.

+Stephane Buisson, id like to test your theory of the motor connected the wrong way but i wouldnt know where to begin to fix that.


---
**Stephane Buisson** *January 13, 2016 16:05*

**+Randy Powell** to understand how stepper motor work, look wiring section here : [http://smoothieware.org/laser-cutter-guide#toc3](http://smoothieware.org/laser-cutter-guide#toc3)

and just follow the cable, to find out the pin. depending on some K40 model (some have wires, other a combination of wires for 1 motor and a flat cable for the other). if you have a controller, it should be easy to find out.


---
*Imported from [Google+](https://plus.google.com/113462808763272747282/posts/4Eb1S25HX4z) &mdash; content and formatting may not be reliable*
