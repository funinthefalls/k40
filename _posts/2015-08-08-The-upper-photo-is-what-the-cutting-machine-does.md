---
layout: post
title: "The upper photo is what the cutting machine does"
date: August 08, 2015 16:55
category: "Hardware and Laser settings"
author: "Federico Zolezzi"
---
The upper photo is what the cutting machine does. The photo below is the one I print in the HP laserjet and is the same as I made in CorelDraw. 

How I can correct this problem?

![images/58d2123851dad509206892963619f7d3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58d2123851dad509206892963619f7d3.jpeg)



**"Federico Zolezzi"**

---
---
**ThantiK** *August 08, 2015 20:06*

Make sure the paper isn't moving due to draft.  Also, you likely have a loose X belt. (There was someone who posted almost this exact issue further down)


---
**Federico Zolezzi** *August 09, 2015 01:38*

The paper is not moving, I wll check the X belt.


---
*Imported from [Google+](https://plus.google.com/106116682925106415916/posts/H18HQ88Veud) &mdash; content and formatting may not be reliable*
