---
layout: post
title: "HV LPS LAB: Well the LPS HV lab construction is progressing"
date: February 15, 2017 19:44
category: "Original software and hardware issues"
author: "Don Kleinschnitz Jr."
---
HV LPS LAB: 



Well the LPS HV lab construction is progressing.

Some equipment procured. The cheap stuff, cause I'm not using my good stuff around 20KV@24ma :(.

<s>--------</s>

Thanks so far to:

**+HuitZiloP KZ**, **+Kim Stroman**



I now have 2x vintages of supplies in house.

The one on the left is a G-G-G style with replaceable flyback and SMD PCB technology

The one on the right is G-W-W style with through hole PCB and soldered in fly-back.

............

Thanks also to those who up to now have donated some beers.  

............

Keep the dud supplies coming as I need a bunch of samples.

.................

I start the fab of the testing chamber. 

As soon as HV wire, connectors, milli-amp meter and mat arrive it will go live.

Then I have to come up with a way to simulate the Laser tube ;). I have and exciting idea but dont want to spoil it....





![images/853e87ede58b9838b7c308c9662e490a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/853e87ede58b9838b7c308c9662e490a.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Don Kleinschnitz Jr.** *February 15, 2017 20:51*

**+Corey Budwine**  Thought I sent you the adr by private post??


---
**3D Laser** *February 15, 2017 21:09*

**+Don Kleinschnitz** just got it 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 16, 2017 00:45*

**+Don Kleinschnitz** I am curious, have you actually used your laser for anything yet? You seem to do an awful lot of experimenting & modification (quite valuable for the group here). But I hope you are getting to have fun zapping things with the laser too.


---
**Don Kleinschnitz Jr.** *February 16, 2017 03:43*

**+Yuusuf Sallahuddin**​ lol, my wife asks me to the same! Not to much, mostly test and acrylic parts. 

Every time I'm about to start a real project a problem distracts me.

Waiting for LW4 to get to serious fun ...



My Woodturning also takes allot of time.  

www.turnedoutright.com






---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 16, 2017 04:16*

**+Don Kleinschnitz** Some nice work you have there. Love the name of the site too. Amusing play on words. I'd love to get into more woodworking when I can afford the tools. Wood has such an amazing character & a timeless classical beauty.


---
**Don Kleinschnitz Jr.** *February 16, 2017 04:49*

**+Yuusuf Sallahuddin** wood is a fun and challenging media, every price is different. Takes years before it will "talk to you". 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/dXdSWpFYpdz) &mdash; content and formatting may not be reliable*
