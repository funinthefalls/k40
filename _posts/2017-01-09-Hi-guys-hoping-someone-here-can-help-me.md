---
layout: post
title: "Hi guys, hoping someone here can help me !"
date: January 09, 2017 22:47
category: "Original software and hardware issues"
author: "Jason Farrell"
---
Hi guys, hoping someone here can help me ! My power supply failed. I ordered a new one but it doesn't match the old one and I'm not sure how to connect it up, does anyone have any ideas ? 

Any help greatly appreciated. The blue one is the old one in the photos . 



![images/50d992999dbab2faaf289d47e3dabaaf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/50d992999dbab2faaf289d47e3dabaaf.jpeg)
![images/cdc1a4ecf0774b9c1e8bf4ad3582d37a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdc1a4ecf0774b9c1e8bf4ad3582d37a.jpeg)
![images/f329d5a98b5342f51e33ca940357873c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f329d5a98b5342f51e33ca940357873c.jpeg)
![images/eaad60a9bf45fc67f4c181d5518fe21a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eaad60a9bf45fc67f4c181d5518fe21a.jpeg)

**"Jason Farrell"**

---
---
**Alex Krause** *January 10, 2017 03:39*

Where did you order the PSU from?


---
**Kostas Filosofou** *January 10, 2017 10:36*

**+Jason Farrell**​ Check this link..



[cnczone.com - Help ! Upgrade chinese 40w laser to 50W](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/256006-help-upgrade-chinese-40w-laser-50w.html)


---
**Jason Farrell** *January 10, 2017 10:43*

I ordered it from eBay 


---
**Don Kleinschnitz Jr.** *January 10, 2017 13:35*

**+Jason Farrell**  Can you tell us and provide purchase links to: 

What exact machine do you have (looks like a K40)?

Where on ebay did you buy the LPS?

How do you know your LPS failed?

<s>-------------</s>

At best guess this supply is not a direct replacement. One problem as **+Konstantinos Filosofou**'s link above shows is it is missing a 24V supply. It may be best to return this one for a better match?


---
**Jason Farrell** *January 10, 2017 14:35*

My machine is a k40. The laser just stopped firing completely. I thought it was the tube so ordered a new one but still didn't work so I then ordered the power supply but now don't know how to wire it in. My k40 has no 24v supply. Only the power supply and controller card and laser tube . 


---
**Don Kleinschnitz Jr.** *January 10, 2017 14:42*

**+Jason Farrell** the K40 power supply has an internal 24V and 5V supply which are output on the right most connector. The replacement supply you bought does not. Therefore you would have to provide a separate 24VDC supply to use this one.

I suggest returning this one and get a direct replacement.


---
**Jason Farrell** *January 10, 2017 14:48*

Oh crap, not gonna be easy to return and all the matching power supplies have to come from China. If I bought a 24v power supply would it be possible to wire up then ? 


---
**Don Kleinschnitz Jr.** *January 10, 2017 16:10*

**+Jason Farrell** 

I don't do much with ebay, wont they handle the returns? And then get a  K40 supply from ebay?



Yes, most all replacement parts come from china. That is why I buy from ebay or amazon to shield me from them.



Yes you can buy a 24VDC supply and wire this supply in. Please supply me to link on ebay for this supply.



The mod would at least include: 

1. a female connector to interface with the current LV connector on your controller. (the rightmost connector on your current supply)

2. A 24VDC supply with a min of 1amp. I would go larger so that you can add other devices like a Z lift table or convert to smoothie.

3. Modifications to the current harness to accept the new supply.


---
**Jason Farrell** *January 10, 2017 16:54*

How many amps would you suggest ? Lots of sizes available and I will send you on the link then , and thank you so so much for helping me out 


---
**Don Kleinschnitz Jr.** *January 10, 2017 17:01*

**+Jason Farrell** This is the supply I am using for my K40-S conversion.

[amazon.com - Amazon.com: DMiotech® DC 24V 5A 120W Power Supply Switching Converter for LED Strip: Home Audio & Theater](https://www.amazon.com/gp/product/B018TG7I4W/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Jason Farrell** *January 10, 2017 20:36*

I have one of those ordered now Don. Is there much to me wiring it all in ? 


---
**Don Kleinschnitz Jr.** *January 10, 2017 20:39*

How electronics savvy are you :)


---
**Jason Farrell** *January 10, 2017 21:08*

Oh not very much 


---
**Don Kleinschnitz Jr.** *January 10, 2017 21:48*

Then we will have to be careful take it slow!

I would need some time to draw it up for you.


---
**Jason Farrell** *January 10, 2017 22:07*

Don you are an absolute gentleman,  thank you so so much 


---
**Don Kleinschnitz Jr.** *January 10, 2017 22:12*

**+Jason Farrell** where are you located.


---
**Jason Farrell** *January 11, 2017 08:21*

I'm in Ireland Don 


---
**Don Kleinschnitz Jr.** *January 11, 2017 16:18*

**+Jason Farrell** oh thought maybe I could get your failed supply .... thats a bit far ;(


---
**Jason Farrell** *January 11, 2017 18:55*

Where are you based Don ? 


---
**Don Kleinschnitz Jr.** *January 13, 2017 13:56*

I'm in Utah,USA :)

After you send me a link to the supply's info I can sketch you up a drawing...


---
**Jason Farrell** *January 14, 2017 20:10*

I will see how much it would cost me to post it to you Don 


---
**Jason Farrell** *January 14, 2017 20:12*

I got the same 24v supply you sent me a link for Don , do you need details for the other power supply ? 


---
**Don Kleinschnitz Jr.** *January 14, 2017 20:30*

**+Jason Farrell** yup need specs on the LPS you bought. Perhaps the vendors page.


---
**Jason Farrell** *January 15, 2017 10:25*

I can't seem to post it here. I have it in PDF but only able to post photos here ? 


---
**Don Kleinschnitz Jr.** *January 15, 2017 14:20*

You have to put it on your Gdrive and share it. Can you paste the link to the ebay place you bought it>


---
**Jason Farrell** *January 15, 2017 17:54*

![images/af743bb30203ff18207eabf2bb53fe63.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/af743bb30203ff18207eabf2bb53fe63.jpeg)


---
**Jason Farrell** *January 15, 2017 17:54*

Is that enough Don ?


---
**Jason Farrell** *January 15, 2017 17:58*

![images/743a0afc805cefe72a32877bf6cb9d0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/743a0afc805cefe72a32877bf6cb9d0f.jpeg)


---
**Don Kleinschnitz Jr.** *January 16, 2017 00:46*

Yes, that's good am I correct that you are interfacing this to a standard Nano controller? Can you post a  picture of it pls to make sure.






---
**Don Kleinschnitz Jr.** *January 20, 2017 01:28*

**+Jason Farrell** below is the wiring diagram. 

The fuse holder I recommend is here:

[https://www.amazon.com/uxcell-Screw-Panel-Mounted-Holder/dp/B00AQWXW9S/ref=sr_1_4?ie=UTF8&qid=1484875638&sr=8-4&keywords=fuse+holder](https://www.amazon.com/uxcell-Screw-Panel-Mounted-Holder/dp/B00AQWXW9S/ref=sr_1_4?ie=UTF8&qid=1484875638&sr=8-4&keywords=fuse+holder)



![images/527b897bc2a282a5d90838d2d33c4028.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/527b897bc2a282a5d90838d2d33c4028.jpeg)


---
**Don Kleinschnitz Jr.** *January 20, 2017 14:09*

**+Jason Farrell** I modified the drawing, added in the current meter.



![images/4c2c4a80ecdc298f88f8feae3ea3caf4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c2c4a80ecdc298f88f8feae3ea3caf4.jpeg)


---
**Jason Farrell** *January 21, 2017 10:15*

Sorry Don I had 2 of my kids birthdays this week. Has been crazy. Thank you so so much for those plans. I will try to do it today and report back ! 

![images/8621444d092fff868b4e9e7dc0520d88.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8621444d092fff868b4e9e7dc0520d88.jpeg)


---
**Don Kleinschnitz Jr.** *January 21, 2017 16:03*

**+Jason Farrell** no problem, family first. I suggest that you wire a step at a time and check the names on what you are connecting to and the voltage before you connect the endpoint.

Good luck.


---
**Jason Farrell** *January 26, 2017 15:28*

Finally got around to rewiring ! Powers up fine and test button works and laser fires. However I'm not getting anything on the power gauge. There was originally a green wire going from the tube directly to the back of the gauge. I tied the blue cable from my new power supply into the green cable. Was this a stupid mistake ? 


---
**Don Kleinschnitz Jr.** *January 26, 2017 16:55*

**+Jason Farrell** I am unclear on what you mean by the blue wire. My meter has 2 green wires on it one goes to the gnd terminal on the supply. On the drawing above that is FG. The other goes to the cathode side of the tube.


---
**Jason Farrell** *January 26, 2017 17:07*

Sorry Don  , I'm a bit dumb with stuff like this. On the back of the power supply. Thick red cable (HV+) ? And also a thin blue wire ? 




---
**Jason Farrell** *January 26, 2017 17:09*

The laser head doesn't move when turned on though. I think the laser head used to reset when switched on but not sure. 


---
**Don Kleinschnitz Jr.** *January 26, 2017 17:20*

**+Jason Farrell** 

I do not know what the blue wire is I did not see that in the LPS specs, did you? I would leave it disconnected for now but insulate it. Connect the meter as in the drawing. Picture of the blue wire coming out of supply?

 

"The laser head doesn't move when turned on"..... by this do you mean that the head and gantry does not move? 

Check that 5V and 24v are correct on the moshi board.


---
**Jason Farrell** *January 26, 2017 17:44*

This is the blue wire Don . gonna check my 5v and 24v now. Sorry for being such a pain and thank you so so much 

![images/84ff7ecab5a3a75d95ee5f56c4c37fec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/84ff7ecab5a3a75d95ee5f56c4c37fec.jpeg)


---
**Don Kleinschnitz Jr.** *January 26, 2017 17:51*

**+Jason Farrell** That does look like what would be a ground. To verify disconnect it and measure its resistance to the FG pin on the front. If its 0 ohms then it is the same connection. 



Your not a pain, these machines are :)


---
**Jason Farrell** *January 26, 2017 17:55*

Thanks a mil Don, will report back shortly 


---
**Jason Farrell** *January 26, 2017 20:02*

I just tried that and no laser when test button pressed. Had a look at the instructions and it says that blue cable is current return ? 


---
**Don Kleinschnitz Jr.** *January 26, 2017 20:07*

Did you measure resistance between wire and FG as suggested above.


---
**Jason Farrell** *January 26, 2017 20:57*

No Don sorry, I don't have any equipment here . I will try to borrow a meter tomorrow 


---
**Jason Farrell** *January 27, 2017 12:49*

Hi again Don, measured across and got 0 .   I have tried connecting it to chassis. No laser, left it hanging, no laser. If I connect it to the ammeter I can fire laser with test button but no reading on ammeter.

When I turn on main power button the 24v supply powers up but main PSU doesn't. When I press laser on button the power supply turns on and the wp led lights up. Then when I press laser test button the wp led goes off and laser fires and laser led lights up.

The gantry does not reset though and when connected to laptop it says device not connected.

I checked and am getting 5v and 24v to the nano board. There is also a tl led on the main PSU but it never lights up. 

Sorry, I'm prob not making any sense with my ramblings here ! 


---
**Don Kleinschnitz Jr.** *January 27, 2017 14:26*

**+Jason Farrell** to make sure we are on the same page regarding the LPS (I will discuss the DC in next post), here is a summary of what you found:



1.  FG and the blue wire read zero ohms. They are connected together!



2. If you connect the blue wire to the meter. [I assume the other side of the meter is connected to the lasers cathode]. The laser fires but meter reads nothing.



3. If you connect the meter to FG. [I assume the other side of the meter is connected to the lasers cathode]. The laser does not fire.



# 3 does not make sense if #1 is true. 

.....................

I wonder if the meter is connected backward and the needle is moving to the left into its stops and therefore looks like it is not moving? 



This still doesn't explain why connecting to FG vs the blue wire does not allow the laser to fire?

........................

If you look carefully on the back of the meter there is a "-" and "+" molded into the plastic. I had to use a flashlight to see mine.



A. The blue wire should be connected to "-". 



A.1: After connecting it this way measure the resistance from "-" on the meter to FG and post results.



A.2: If the blue wire was connected correctly post that  ..... if so I'm temporarily stumped.

............................

Here is how mine is connected.





![images/69d3e3bcfef3cece3979bdf076ba85d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/69d3e3bcfef3cece3979bdf076ba85d8.jpeg)


---
**Don Kleinschnitz Jr.** *January 27, 2017 14:37*

**+Jason Farrell** from your post above (you can reference answers by letter designation):



" When I turn on main power button the 24v supply powers up but main PSU doesn't." 



B.1: What are you calling the main PSU and how do you know its not powering up?



"When I press laser on button the power supply turns on and the wp led lights up. Then when I press laser test button the wp led goes off and laser fires and laser led lights up."



B.2: What is WP? Post picture

B.3: What is the laser led. Post picture.



"The gantry does not reset though and when connected to laptop it says device not connected.

I checked and am getting 5v and 24v to the nano board. "



B.4: Are you measuring the 5V and 24V across the nano's connector. I.E. probing ground on that connector to each of the voltages on that connector.



"There is also a tl led on the main PSU but it never lights up. "

B.4 What is the tl. Post picture

B.5: Post picture of control panel.


---
**Jason Farrell** *February 09, 2017 10:33*

Hi again Don , sorry I was away with work. Back to annoy you again lol. By main PSU I mean the new one I bought. The large black one for the laser.  I am attaching pictures, if you notice on bottom left there are 2 holes marked TL and WP. When I press laser switch on control panel the WP lights up. Then on the bottom right of picture there is another led marked laser, When I press laser test on control panel the laser led lights and the WP led dims. Hopefully you can see that from the photos. I also checked on the nano board and it is getting 5v and 24v 


---
**Jason Farrell** *February 09, 2017 10:43*

![images/100730d5ce4bc44b3b6bc12e30feda73.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/100730d5ce4bc44b3b6bc12e30feda73.jpeg)


---
**Jason Farrell** *February 09, 2017 10:44*

![images/2801db4da15d9ed6d5aab6313b3c4862.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2801db4da15d9ed6d5aab6313b3c4862.jpeg)


---
**Jason Farrell** *February 09, 2017 10:44*

![images/285036da1579470d4117eba40f7cb3de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/285036da1579470d4117eba40f7cb3de.jpeg)


---
**Don Kleinschnitz Jr.** *February 09, 2017 18:26*

**+Jason Farrell** so we can make sure we are on the same page can you verify my understandings above and answer the questions. Please index your answers using the # and letters as identifiers in your answer. That way we can tackle these one at a time.


---
**Jason Farrell** *February 09, 2017 23:37*

Will do Don , thank you. 

B1 - main PSU being the new large black replacement. The fan inside is coming on but no LED's lighting. 



B2- wp is an led on the power supply as in photo.

B3- laser is another led to the bottom right of the power supply. 



B4- I measured on the board itself using ground on the board and 5v and 24v and getting voltage ok . 



B5- tl is another led on power supply. Directly beside wp led. Shown in photo. 


---
**Jason Farrell** *February 09, 2017 23:39*

![images/d4dfc28c6c82ace98e1065795038a6e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d4dfc28c6c82ace98e1065795038a6e4.jpeg)


---
**Don Kleinschnitz Jr.** *February 09, 2017 23:54*

**+Jason Farrell** ok good its a standard K40. Pls look further up this thread there are #'d and A questions :).


---
**Don Kleinschnitz Jr.** *February 10, 2017 23:54*

Ok lets do this one problem/symptom at a time. 

please answer these using the #'s as reference



power on the machine



1. When you push the red test button on the power supply does the laser fire (i.e. lights up)?

2. If the laser fires, does the current meter show current?


---
**Jason Farrell** *February 13, 2017 10:07*

Hi again don, yes to both of those questions now. I have wire from cathode going to + on ammeter and blue wire from rear of power supply going to - on ammeter. Laser powers up fine now and test fires and shows current on ammeter and adjusts using the knob on control board. 



Problem now is gantry not resetting when laser is powered on 


---
**Jason Farrell** *February 13, 2017 10:11*

I used the meter on the board and using ground on the board I'm getting 5v and 24v and on the last pin marked lo I'm getting 4.96v 


---
**Jason Farrell** *February 13, 2017 10:25*

I'm wondering if its a ground issue. I have taken ground from my mains cable in. If I connect to chassis also the laser won't power up or fire . also the max reading on the ammeter is 12ma at full power 


---
**Don Kleinschnitz Jr.** *February 13, 2017 13:14*

**+Jason Farrell** sorry to repeat stuff but we have to be clear on the symptoms so I don't take us down a rat hole :)

1.0 The laser, the current meter and the "Test" function all now works?

2.0 The gantry won't reset.

2.1 What do you mean by reset, not homing?

2.2 Can you post video?

3.0 On the controller card the voltages read:

3.1 5v

3.2 24V

3.3 LO 4.96 (that should be correct when not printing)

4.0 I dont understand the ground symptoms.

4.1 if you connect the mains ground to the chassis the laser won't fire? Is the mains ground not connected?

4.2 on a separate issue the current does not go higher than 12ma?


---
**Jason Farrell** *February 13, 2017 16:11*

Sorry Don, 

#1 yes they do. 

#2 /2.1 does move/doesn't reset. 

#3/3.1/3.2 all correct.

#4/4.1 I am only using ground taken from mains, nothing to chassis. 

When connected to chassis laser won't fire. 

#4.2 Correct. Does not go higher than 12ma 




---
**Don Kleinschnitz Jr.** *February 13, 2017 16:43*

**+Jason Farrell** 

It does seem like there is a ground problem. 

1.0 with machine off measure OHMS from LPS gnd to chassis.


---
**Don Kleinschnitz Jr.** *February 15, 2017 15:45*

**+Jason Farrell** anything I can do to move you forward to a solution?




---
**Jason Farrell** *February 16, 2017 15:46*

Sorry don , only getting a chance between work. I don't know if I did it properly but took readings. 

200- bat 

2000- 983 

20k- 0.99

200k- 01.0

2000k-001



I attach photos, I took readings between fg  on power supply to chassis. 


---
**Jason Farrell** *February 16, 2017 15:54*

![images/742ed73c54d899afc669a2a43f2c164d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/742ed73c54d899afc669a2a43f2c164d.jpeg)


---
**Jason Farrell** *February 16, 2017 20:13*

Don you are not going to believe this ! Its sorted, I am a total dumbass. I read your diagram wrong ! 

I reversed the wiring on the m2 nano. Completely stupid ! 

Thank you so so so so much for your help and your massive patience in listening to me ! 


---
**Don Kleinschnitz Jr.** *February 16, 2017 21:16*

**+Jason Farrell**​ you reversed power? Is board OK?



Everything working?




---
**Jason Farrell** *February 17, 2017 08:44*

Seems to be don , test fires ok and can be adjusted ok, goes up to 30ma now also. Going to focus it today and get back to using it hopefully ! 


---
**Don Kleinschnitz Jr.** *February 17, 2017 14:43*

**+Jason Farrell** excellent.


---
*Imported from [Google+](https://plus.google.com/110053066297964291796/posts/PP1PTbj94zT) &mdash; content and formatting may not be reliable*
