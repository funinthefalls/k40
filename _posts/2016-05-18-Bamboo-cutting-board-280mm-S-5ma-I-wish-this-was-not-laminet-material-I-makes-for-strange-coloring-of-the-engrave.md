---
layout: post
title: "Bamboo cutting board 280mm/S 5ma. I wish this was not laminet material I makes for strange coloring of the engrave..."
date: May 18, 2016 05:13
category: "Object produced with laser"
author: "Alex Krause"
---
Bamboo cutting board 280mm/S 5ma. I wish this was not laminet material I makes for strange coloring of the engrave... but we will see what we get after some hot soapy water and a rub down of mineral oil. **+Peter van der Walt**​ Lemonade!

![images/14b782c9362db0d0f2e266424e08ddac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14b782c9362db0d0f2e266424e08ddac.jpeg)



**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 18, 2016 08:40*

That's cool. I like the Let's Cook.


---
**Scott Thorne** *May 18, 2016 10:43*

Nice job. 


---
**Brandon Satterfield** *May 18, 2016 10:43*

My wife would trip over that!! 


---
**Jim Hatch** *May 18, 2016 17:17*

Regardless of the striations this looks really good. We're you using something like PhotoGrav to prep the picture or did you manually adjust the dithering? It really turned out nicely.


---
**Miguel Alvarado** *May 18, 2016 18:45*

cool


---
**Alex Krause** *May 20, 2016 04:17*

**+Jim Hatch**​ it's a just 2 simple black and white drawings one from deviant art and the other from Google images rastered no prep work other than resize and alignment ﻿


---
**Alex Krause** *May 20, 2016 04:19*

I did some bed leveling for my work piece and ended up with better results on my next attempt that can be seen here [https://plus.google.com/109807573626040317972/posts/LFM8n9Sk1oB](https://plus.google.com/109807573626040317972/posts/LFM8n9Sk1oB)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/W4Wtg6fNctS) &mdash; content and formatting may not be reliable*
