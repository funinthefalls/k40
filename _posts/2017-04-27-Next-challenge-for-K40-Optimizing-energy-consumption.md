---
layout: post
title: "Next challenge for K40: Optimizing energy consumption"
date: April 27, 2017 09:01
category: "Modification"
author: "Stephane Buisson"
---
Next challenge for K40: Optimizing energy consumption.



K40, water pump, air assist compressor, exhaust fan, ice/peltier/fridge = a lot of energy.



what about using the stream of exhaust fan for cooling (radiator) !

or coupling the exhaust fan with air assist (a bit like a car compressor) !



Will the community come up with some nice solution to save energy & noise???







**"Stephane Buisson"**

---
---
**HP Persson** *April 27, 2017 11:33*

2-4 fans on a rad. is maybe 0.3A on full speed.



But if you really want, adding some charcoal filter to it and have it inline with the exhaust would probably work fine, make some kind of adapter for the hose to fasten in the holes of the radiator :)

You don´t need much airflow to transfer heat from a radiator, but it restricts the flow pretty good so it may be a issue with that.

I have my machine optimized for low noise, my exhaust would not take care of both radiator cooling and exhaust.



Money wise there is more savings on the exhaust fan itself, i see many users with fans that would fill a hot air ballon :P Optimizing the air flow inside and trough the machine can lower the exhaust fan needs by 70% or more.


---
**Stephane Buisson** *April 27, 2017 12:15*

how does the charcoal filter with flow restriction ? (sincerely no idea)

this little radiator could be inserted just at the back of the K40 (short water hose) and before inline fan. the radiator area should be about the same size as the exhaust itself so minimum flow restriction.



my attempt is to combine actions when possible to reduce energy consumption.


---
**HP Persson** *April 27, 2017 12:25*

I´m using a double layered filter, universal size for kitchen fans i cut out, it slows down the air a bit but not too much.

I use a 110cfm inline fan, but with a crazy optimized airflow inside the machine and optimized intake fans.

(relieving the exhaust from both pulling air into the machine as well as taking it out increases the efficiency a lot).



Smart solution though, as we all have different types of exhausts it would probably work fine for some of you :)

Test it, either it works or you may have to tweak it some more. (download hole-pattern for 120mm radiators and build a adapter for the back) :)


---
**Don Kleinschnitz Jr.** *April 27, 2017 13:26*

**+Stephane Buisson** I have wondered about blowing the fan output through a water cooling radiator like that to keep the water at ambient. Figured it would get pretty dirty. Perhaps also vent the fan through the laser tube cavity?




---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2017 15:09*

Interesting thoughts. Following to see what people come up with & in case I have an idea later.


---
**Jorge Robles** *April 27, 2017 19:22*

I have to go to scrapshop to get a fridge radiator. I'm worried about high temps this summer.


---
**Steve Clark** *April 28, 2017 03:37*

Here is the problem, the systems we are using are wholly dependent on the ambient temperature and the amount of moisture in the air (humidity) that can be held at that altitude.



 If the humidity is high then the ability to move the heat out of the coolant becomes very difficult and  maybe even pretty much impossible.



 In example… The desert climates are dry so it may be possible to draw 20 or even 30 degrees of heat out.  Humid environments like on the coast less a lot less, possibly none.



So in this effort to do so, without outside help the best you could expect out of one of these systems is to dump the heat produced into the ambient temperature which may already be higher than we want.



 The only low energy possibility is to use some of the passive evaporative techniques such as they are.



 Again an example is water evaporative coolers.



[https://www.google.com/search?q=how+evaporative+cooling+works&rlz=1C1CHFX_enUS600US600&oq=how+eva&aqs=chrome.4.69i57j0l5.8837j0j8&sourceid=chrome&ie=UTF-8](https://www.google.com/search?q=how+evaporative+cooling+works&rlz=1C1CHFX_enUS600US600&oq=how+eva&aqs=chrome.4.69i57j0l5.8837j0j8&sourceid=chrome&ie=UTF-8)



One other possibility is ultrasonic vaporization cooling…



[https://www.thespruce.com/humidifiers-ultrasonic-vs-evaporative-humidifiers-1908160](https://www.thespruce.com/humidifiers-ultrasonic-vs-evaporative-humidifiers-1908160)



But we are back to the same problem your using energy to produce both evaporative and ultrasonic cooling.



One other passive cooling system I’ve come across is deep well or deep pond, stream or river cooling. There your limited to your location though… and pumps…they use a fair amount of energy too.



[https://en.wikipedia.org/wiki/Deep_water_source_cooling](https://en.wikipedia.org/wiki/Deep_water_source_cooling)



Finally… There may be less energy used in a solar panel system that runs all day energizing a pump and a Peltier to cool a calculated and sized to usage insulated water tank… basically free energy usage. 

	




---
**Jeff Bovee** *April 28, 2017 21:06*

I have a little radiator like that in a mini fridge with tiny USB powered fan to keep the air moving.


---
**Steve Clark** *April 28, 2017 22:56*

Jeff, that will work... unfortunately the power to cool the coolant still comes from your electric plug.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/YVvaCxeCkT8) &mdash; content and formatting may not be reliable*
