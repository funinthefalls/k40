---
layout: post
title: "I must be missing something really basic"
date: April 09, 2018 04:32
category: "Modification"
author: "Tom Traband"
---
I must be missing something really basic.



Now that homing is working, homing leaves the laser in the upper left corner (where the switches are) minus the post-homing offset.



The machine now thinks it is at X=0, Y=00 when it is really at X=0,Y=MAX. How do I correct the machine's misapprehension?





**"Tom Traband"**

---
---
**Joe Alexander** *April 10, 2018 01:30*

are you using the original controller board or a replacement? I know with smoothie boards you change the config to home to max and it will show as 0,200. it doesn't home to bottom left as there is no limit switch there to tell the machine qhen it has reached the end.


---
**Tom Traband** *April 10, 2018 04:23*

Sorry - I should have stated this. I've replaced the controller with an Uno and a cnc shield. I've got X defined as the "wider" left-to-right axis and Y as the front-to-back axis. All axes and limit switches are working correctly and they follow the "right hand rule" which means I've had to reverse the homing direction on X using $3=1 since that switch is at min travel rather than max travel.



When I launch Lightburn and home the machine the position is reported as 0,0 when it should be reporting 0, 210.




---
**Tom Traband** *April 10, 2018 04:24*

Oh, and Lightburn is set with origin in front-left as recommended.


---
**Tom Traband** *April 10, 2018 05:33*

Just got this figured out. Needed to reset the machine origin (used Universal GCode sender since command didn't seem to work from Lightburn console) and proper setting of $10.


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/ULUWkN4Nz2r) &mdash; content and formatting may not be reliable*
