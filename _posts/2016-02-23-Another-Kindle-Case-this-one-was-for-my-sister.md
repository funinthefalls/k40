---
layout: post
title: "Another Kindle Case, this one was for my sister"
date: February 23, 2016 20:53
category: "Object produced with laser"
author: "David Cook"
---
Another Kindle Case,  this one was for my sister.  came out good.  Still stinks while burning it lol.



I really need to get a handle on the PPM vs CW settings and when to use which one.



![images/6b0eec0b433081b9a669acc4861a94f7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b0eec0b433081b9a669acc4861a94f7.jpeg)
![images/b7cb37e1cf1f5c872d33ef2be604f47b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7cb37e1cf1f5c872d33ef2be604f47b.jpeg)
![images/8bdb7e39ee2f4cd05898199c8f1c5ce6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8bdb7e39ee2f4cd05898199c8f1c5ce6.jpeg)
![images/023bbe024afc4c6b6b5f7ecf9e5eb32e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/023bbe024afc4c6b6b5f7ecf9e5eb32e.jpeg)
![images/c1bbb2f8a0b25cf18cbc87a92efb632c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1bbb2f8a0b25cf18cbc87a92efb632c.jpeg)
![images/896904e10cd0ab3d0816375518ddd95c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/896904e10cd0ab3d0816375518ddd95c.jpeg)

**"David Cook"**

---
---
**3D Laser** *February 23, 2016 23:57*

Where did you get your air assist guide


---
**3D Laser** *February 23, 2016 23:57*

Where did you get your air assist guide


---
**3D Laser** *February 23, 2016 23:57*

Where did you get your air assist guide


---
**David Cook** *February 24, 2016 00:14*

**+Corey Budwine**  the chain ?  I traded a friend of mine a much larger chain I bought from Ebay for it.  he got it from some 3D printer shop online I think.  I'll ask him and report back


---
**Tony Sobczak** *February 24, 2016 01:44*

I think he's talking about the laser cross hairs. 


---
**3D Laser** *February 24, 2016 02:05*

The chain is what I am talking about I wonder what is cheaper a self retracting cool or that chain 


---
**David Cook** *February 24, 2016 03:37*

Lol. OK. The laser crosshairs and air assist nozzle are a mix of two different thingiverse prints..  The chain is from [http://www.smw3d.com/cable-chain-10mm-x-15mm/](http://www.smw3d.com/cable-chain-10mm-x-15mm/)


---
**David Cook** *February 24, 2016 16:42*

[http://www.thingiverse.com/thing:1063067](http://www.thingiverse.com/thing:1063067)   and I cut the nozzle off

[http://www.thingiverse.com/thing:890705](http://www.thingiverse.com/thing:890705) for the cable chain mounts and nozzle

I was lazy and did not want to 3d model anything custom for it yet.  Seems to work good so far.

I still need to find my correct focal length and to the honey comb bed and some other items.






---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/HNt1NWv4wac) &mdash; content and formatting may not be reliable*
