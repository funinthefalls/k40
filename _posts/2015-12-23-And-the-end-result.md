---
layout: post
title: "And the end result ..."
date: December 23, 2015 17:02
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
And the end result ...





**"Ashley M. Kirchner [Norym]"**

---
---
**Ashley M. Kirchner [Norym]** *December 23, 2015 18:51*

I should point out that several changes were made here:

- the bucket/scoop has a pointed edge as opposed to the original straight edge

- the mounting plate that the front gets mounted on has "end stops', so the front only swivels 20 degrees (10 degrees each side). this is to avoid the wheels from hitting the side and rubbing

- the bucket arm also has 'end stops' so it can not go backwards when raised to the top (and fall into the cab). the same thing with the lower end, it stops before hitting the mounting plate for the hinge.

- the bucket itself also has an 'end stop' at the bottom that prevents it from completely folding underneath the vehicle.

- the bucket hinge has been redone to include side locking disks which provide friction to hold it in place as opposed to just flopping down

- all of the matching connecting slits have a little 'bump' on them (0.5mm tall) that adds friction to the parts once put together, so they don't easily slide part (some of them are tight enough that i had to gently tap them in with something heavy - really nice, tight fit.)



I forgot what else I did to the original design ... but it was drawn for a 3mm ply, and the wood I have is 2.89-ish. So I had to offset outer lines by 0.1mm and inner ones (think holes and slits) by the same amount inward. Combined with the mishap with the nozzle, I am really happy how this came out.


---
**Ashley M. Kirchner [Norym]** *December 23, 2015 18:58*

Oh, all the holes that have something rotating in them have been done to exact measurements, unlike the original plans where they were larger than the piece that went in them. This causes things like the front hinge to be very loose and wobbles left to right. The locking pin was about a millimeter too long. All of those things were fixed/adjusted before the final cutting was done. It's one of the reasons i like to recreate things in 3D space, so I can check for those tolerances. I have been called OCD or anal retentive before ... it shows. :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 24, 2015 08:16*

**+Ashley M. Kirchner** It seems like your OCD or anal retentive tendencies paid off with a thoroughly planned & executed design modifications that turned into a great final piece.



I remember you mentioned that you purchased the design from somewhere? Maybe you should upload your adjusted design to that place & sell it as "BETTER than the original" design haha.


---
**Ashley M. Kirchner [Norym]** *December 24, 2015 08:56*

Well, I got it from [makecnc.com](http://makecnc.com) .... I don't think they'll want to sell mine when they have their own. :)


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/LXb3ZZ3v7K1) &mdash; content and formatting may not be reliable*
