---
layout: post
title: "As someone who has suffered from anxiety for most of my life, I've decided it is not welcome in my house anymore"
date: July 01, 2016 10:35
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
As someone who has suffered from anxiety for most of my life, I've decided it is not welcome in my house anymore.



4 passes, 1st pass was 4mA @ 350mm/s @ 5 pixel steps. Barely even marked the surface.



2nd, 3rd, 4th pass was 10mA @ 350mm/s @ 5 pixel steps.



Even after all that, the engrave still ends up being only about 0.5mm (or less) deep. Merbau hardwood is literally very hard.



 #noanxietyallowed   #merbau  



![images/fafffab5256adee00e1cdb0d3a3184ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fafffab5256adee00e1cdb0d3a3184ed.jpeg)
![images/68ee3374bc2875dc7b274623cadd7782.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/68ee3374bc2875dc7b274623cadd7782.jpeg)
![images/6b5abf3706cb95152c1f8f43b91cfc3e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b5abf3706cb95152c1f8f43b91cfc3e.jpeg)
![images/0a83714347499da02404077de99e5a76.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a83714347499da02404077de99e5a76.jpeg)
![images/f4e9c9aeacd2207315f33560a4d3a2e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4e9c9aeacd2207315f33560a4d3a2e1.jpeg)
![images/53ac098502f37973736c7e05cd0368e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/53ac098502f37973736c7e05cd0368e5.jpeg)
![images/a0e34654deafeb3d7fd674e0fa599366.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0e34654deafeb3d7fd674e0fa599366.jpeg)
![images/b2ba0ea9a6d07fb0df6f1fcd3a939359.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2ba0ea9a6d07fb0df6f1fcd3a939359.jpeg)
![images/edb9502137935549601cf18c14be7cd9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/edb9502137935549601cf18c14be7cd9.jpeg)
![images/a8b71cf1fb22cff44d68344094462699.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a8b71cf1fb22cff44d68344094462699.jpeg)
![images/4999cc1da0d4c4cce11947fcf0072efa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4999cc1da0d4c4cce11947fcf0072efa.jpeg)
![images/1e0bf19de7ec9569ffdd12dccab2e137.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e0bf19de7ec9569ffdd12dccab2e137.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Scott Marshall** *July 03, 2016 08:11*

I NEED one of those. I'm buried with so much work, I haven't bee able to use my laser for weeks, I get to work on it a lot, but to just sit down and play, ha.

These kits are sure keeping me busy. Odds and ends, Still writing the manual. etc. I enjoy the design work more, but there's no turning back now.



ACR boards expected monday..



Realized this morning I never sent the letter I wrote back to you. I offered you a 12 pin FFP,  but I see you got one (I was over on the 'other' K40 section this morning and see you're almost ready to start wiring. Too bad I can't beam you one of the prototype build ups. They're just about ready to go, but are mounted to the required power supply. I was thinking of selling them next week if someones in a hurry.

I also didn't get back to Ray K, lost a whole bunch of emails and don't have his address - so if you're out there, please, email me again. sorry.



See - Stress, anxiety.



Nice sign. Love that Merbau!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 03, 2016 09:29*

**+Scott Marshall** I'm still looking forward to the ACR/Switchable. I think I'd much rather go that route than my dodgy electronic wiring skills haha. Something more plug & play is definitely better suited to my skill level.



Hope all's well & keep up the good work on the kits :)


---
**Pigeon FX** *July 05, 2016 03:45*

As a fellow sufferer of anxiety, I sincerlly hope this works for you!


---
**Scott Marshall** *July 05, 2016 06:16*

Progress...

I'm expecting my ACR Boards in a few hours, I'll be posting a pre-order price list for the community if they look OK (no missing traces etc.)



 Switchable prototype run is 10 boards, they're due to ship to me about the 14th. I'm pretty busy trying to get the manual finished, come up with a testing method that won't kill my poor abused K40. 



Large quantities of expensive parts incoming, 20 Power supplies, 2000ft of shielded cable, etc. Trying to buy for the 1st 20 kits but between the long lead times and the quantity discounts, I have to buy more than  I'd really like to at this point. Credit card is stressed as much as I am.

I"m hoping I'll feel better about the situation when the cash flow chart shows anything greater then zero in the incoming column.



Next time it's operational I'm going to burn a copy of your sign for inspiration. Is that "Anxiety" text your handiwork, or is it  a font?



 Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 05, 2016 08:19*

**+Scott Marshall** That's great to hear that the ACR is pretty much ready & the switchables are not far off. Unfortunate that you have to order in such large quantities, but I guess that is always the way with getting bulk discount.



The Anxiety text is actually a font I have downloaded somewhere a while back. It's called XXII Scratch. The other font was something I randomly chose, turned white, added a 3mm border. Can't remember which font it is, but seemed nice & blocky.



I haven't had much time to play with my laser lately & am kind of dreading the Smoothie/LaserWeb switch (not for any other reason than I actually have no idea what I'm doing haha). I'm looking forward to the end result & being able to use LW instead of the horrible software, but I'm kind of in a "better the devil you know" mood at the moment, since I understand what I'm doing & know how to do it currently. With Smoothie/LW it's like a whole new world.


---
**Scott Marshall** *July 05, 2016 11:13*

That's the beauty of the Switchable. I expect they will out sell the standard ACR for just that reason. In 15 sec you can have your old devil back. Too bad too, I was really looking forward to using the M2nano as an aerial target. It would just about fit in my little clay thrower...



I've been familiarizing myself with LaserWeb and Visicut. I figure I ought to be able to help people get it loaded and running anyway.  Peter and Stephane have some serious work in their software. It's killing me just learning how to use it. There Will Be a learning curve, for sure.

At least this time it will be more productive in the end, so I hope.

It was my reason of pushing ahead on the switchable, just because I personally want one. I hope people really want them as bad as they seem to.



I AM enjoying being 'back to work"  even on a very limited basis. I've missed it for a long time, and it's in my field, but I'm still learnnig a ton of new stuff. Things have changed a lot in the 20 years since I was put out of the game.



Posting Price list and Pre-order offer shortly on ACR kits and light kit.



I noticed you've been busy, you've haven't been posting your average 4 projects a day. 



Hope it's nothing bad that's taking you away form the Laser.



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 05, 2016 13:04*

**+Scott Marshall** I'm hoping the ACR & Switchable both work out for you, but I have a feeling you're correct that the Switchable will be the more popular choice. At least for Nongineers like myself.



Nothing bad taking me away from the laser, other than winter & being busy with other projects. My laser is in the garage (under the house) which has no heating, so it's quite cold down there, so I've been staying upstairs in the warm haha. (Although my definition of cold is 10 degrees C or less).



I'll keep my eyes peeled for your posts :)



Have a good one Scott. I'll reshare your pricing posts once they're up, publicly on my wall to get some more exposure for them :)


---
**Scott Marshall** *July 06, 2016 03:26*

Nongineer! Ha, I like it! I may have to use that.



Forgot you folks are not only out of phase with us, (or we with you?) on day vs night, 12 hours ahead or so, I believe,  but on seasons as well.



My butt drags in winter too, but we see more like -30c here in balmy Central New York.



I'll email you the Documents, Thanks for the support and help.



I think I'm just going to have to put up a page with a quick description, and estimated shipping dates. I'll tell people to email for a full list "catalog' it's kind of growing into.



I just found out my boards have been lost on n UPS truck since last week, supposedly it's corrected and they will be here tomorrow. That's a relief.



Laser on and keep warm, Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 06, 2016 09:08*

**+Scott Marshall** Yeah, where I am I'm on +10GMT for time, so it's definitely a bit different from yours. I honestly couldn't imagine living anywhere that is -30c. I may as well move to Antarctica for temps like that. I think worst I've experienced is about -10c. Gotta love it when your shipment gets "lost" at the bottom of the pile haha. Glad they found it though :)



Have a good one, Yuusuf


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/9cxosZ6JNBd) &mdash; content and formatting may not be reliable*
