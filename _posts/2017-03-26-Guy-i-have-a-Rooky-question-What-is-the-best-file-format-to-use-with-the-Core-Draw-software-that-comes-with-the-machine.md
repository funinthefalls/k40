---
layout: post
title: "Guy i have a Rooky question. What is the best file format to use with the Core Draw software that comes with the machine?"
date: March 26, 2017 02:48
category: "Original software and hardware issues"
author: "Chris Hurley"
---
Guy i have a Rooky question. What is the best file format to use with the Core Draw software that comes with the machine? I'm trying to import stuff with not having much luck with pdf.  Or is there better software to use? Or do I need to be hitting up the books etc? 





**"Chris Hurley"**

---
---
**Ariel Yahni (UniKpty)** *March 26, 2017 02:58*

svg, bmp, jpg, png should work better than pdf


---
**Ned Hill** *March 26, 2017 03:30*

Yeah, If they gave you the typical coreldraw 12 (very old version) it won't handle a PDF correctly.  Everything that Ariel mentioned is fine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 26, 2017 11:07*

Or if it's CorelDraw 12 I found that Adobe Illustrator version 9 format worked well too. Any newer AI formats wouldn't work in CD12.


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/KwMomuq4nVU) &mdash; content and formatting may not be reliable*
