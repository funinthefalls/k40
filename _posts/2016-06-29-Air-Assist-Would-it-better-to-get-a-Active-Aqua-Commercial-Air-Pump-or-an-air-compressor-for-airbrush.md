---
layout: post
title: "Air Assist: Would it better to get a Active Aqua Commercial Air Pump or an air compressor for airbrush ?"
date: June 29, 2016 19:50
category: "Discussion"
author: "Robert Selvey"
---
Air Assist: Would it better to get a Active Aqua Commercial Air Pump or an air compressor for airbrush ?





**"Robert Selvey"**

---
---
**Ariel Yahni (UniKpty)** *June 29, 2016 20:01*

Airbrush compressor ( 3L tank more or less)  will only give you flow and not pressure. 


---
**greg greene** *June 29, 2016 20:08*

An Aqua pump is built for constant operation airbrush is not - also they are a lot quieter.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/jSqnZxfbMDm) &mdash; content and formatting may not be reliable*
