---
layout: post
title: "I am having problems keeping my water cool"
date: February 25, 2017 16:22
category: "Discussion"
author: "george ellison"
---
I am having problems keeping my water cool. I have a lot of products I need to make at the moment but I use the laser for about 1 hour and my water is up to 20.0 c. I have used ice packs etc but they hardly bring it down enough to use for 5/10 minutes. Any suggestions would be appreciated thanks





**"george ellison"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 25, 2017 16:38*

How large is your reservoir of water to begin with & what sized ice packs are you utilising?



I have minimal experience with this (as to be perfectly honest I've never really kept an eye on my temp or bothered to check it), but I'd assume you would want something in the 20 litres range as your minimum quantity & then when using ice packs I would probably use at least 25% quantity of ice (so e.g. 5L worth of ice packs). No idea if that is enough ice or too much ice.



From what I understand, 15-20c should be pretty decent operating temperature for the tube. So seems like after 1 hour to be hitting 20c then you're obviously doing something right.


---
**Mark Brown** *February 26, 2017 14:58*

The manual that came with mine says to add ice if the water gets up to 35c.  So ~IF~ we trust the Chinese on this you're nowhere near harming the tube (which makes sense).



Now more ideal conditions would be 15-20c for optimal laser power. So if you can keep the water around 20 while operating you should be fine?



Don't take me too seriously though, I've only been into this for a week.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 26, 2017 15:53*

**+Twelve Foot** Seems the ideal temperatures you mention are the same I've been reading regarding optimal performance (15-20c).



I guess 35c would be a point where I'd start getting concerned of the water temp, but I'd probably be concerned earlier than that if I actually kept track of my temps.



There are others in this group that do use iceblocks quite a lot & some use chillers etc. Hopefully they can weigh in at some point with some pointers.


---
**george ellison** *February 27, 2017 18:51*

Thanks for all the advice, when I start cutting my temp can be around 7-10 c and i cut up to it gets to 20 c and then stop cutting. I am already on my 2nd tube so being very cautious. Another problem maybe not using enough de-ionised water, I only have enough in a bucket to cover the top of the water pump so i may double to quantity and see if I can get longer cutting hours Thanks


---
**Otto Souta** *March 16, 2017 14:05*

I made Peltier - TEC water chiller. I have read and found out, that the CO2 laser tube behaves best at 15 deg C. So I made one. It is using two TEC modules, each  80W, temperature controlled to +/- 0.1 deg C. It keeps the water temperature at 15 deg C constant. It is integral unit, with the heat exchanger assy, water reservoir, pump and external mains PSU. The whole unit size is similar to 5l can + the PSU.

Anybody interested on one? Thanks

![images/2b406640c7cbd4dc08050168a4a38e7e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b406640c7cbd4dc08050168a4a38e7e.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 16, 2017 14:09*

**+Otto Souta** That's pretty awesome. How much water is contained in your system? You mention that it keeps the water at a constant 15C, but I'm curious does it do so even with large quantities of cutting & engraving (e.g. 2 hours)?


---
**Otto Souta** *March 16, 2017 14:24*

Water container is 1.8L, I keep it at 3/4 full, then there is the piping and tube - not much more. It is not large volume to keep the temp. low, it is the fact of cooling the water. I looked at so called "chillers", they are nothing more than radiator with a fan! When I start, I switch the cooler 5-10 mins beforehand so it drops to 15 C from ambient and I can start cutting. The temperature is nearly constant, if you do heavy cutting, it may go to 16C, but I usually do not cut at full 100% to keep and eye on the life of the tube. Also the engraving is more consistent. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 17, 2017 00:16*

**+Otto Souta** That's pretty good. Was just curious because I know some other members who use their laser for really significant amounts of time & need to take breaks every couple of hours to let things cool down. Roughly how much did the entire setup cost you?


---
**Otto Souta** *March 17, 2017 01:22*

This is my third iteration, which can run laser 24/7. I recon parts cost about £150. How many do you think would be needed? I think I could make it for around £200 in a small batches. I have a bigger beast on the drawing board, with 2 x 142W peltiers, total 284W, 2x more powerful. For the K40 with 40W laser the existing cooling power is more than sufficient. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 17, 2017 02:40*

**+Otto Souta** I'm not sure if there would be a big need for this kind of thing, although it is cool. Most people here tend to be makers of some kind & make their own. But, you never know.


---
**Otto Souta** *March 17, 2017 02:47*

Yes, I understand. Thanks for your comment. I have been involved in thermoelectric cooling for the last decade, so I know it inside out. There can be many dead ends on the way to a practical device. 


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/eaaLAiGveKk) &mdash; content and formatting may not be reliable*
