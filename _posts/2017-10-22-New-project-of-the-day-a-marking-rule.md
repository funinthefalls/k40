---
layout: post
title: "New project of the day, a marking rule"
date: October 22, 2017 00:18
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
New project of the day, a marking rule.



<b>Originally shared by Ariel Yahni (UniKpty)</b>



I have always wanted one of this so I made one. Marking Rule out of 3mm acrylic.



![images/bac806c4b4282c9f988a92f432ca319e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bac806c4b4282c9f988a92f432ca319e.jpeg)
![images/51771ffe16de4234b8fb4e416a046d92.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/51771ffe16de4234b8fb4e416a046d92.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**Ned Hill** *October 22, 2017 01:31*

Neat idea.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/KzKPgCoWxNC) &mdash; content and formatting may not be reliable*
