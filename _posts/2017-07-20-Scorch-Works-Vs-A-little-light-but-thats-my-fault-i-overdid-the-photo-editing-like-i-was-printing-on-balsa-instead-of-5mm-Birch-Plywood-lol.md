---
layout: post
title: "Scorch Works Vs. A little light, but thats my fault i overdid the photo editing like i was printing on balsa instead of 5mm Birch Plywood lol"
date: July 20, 2017 01:35
category: "Object produced with laser"
author: "Kenneth White"
---
**+Scorch Works**  #K40Whisper Vs. #LaserDRW   

A little light, but thats my fault i overdid the photo editing like i was printing on balsa instead of 5mm Birch Plywood lol.



Soooo, Settings:

300mm/s

3 mA power

All used the Exact same photo, Upper left of board is Laser DRW with Sunken button checked next to engraving,  Upper right of board was Laser DRW with Sunken Unchecked. Neither worked.  Right below that was a Test of Laser DRW with default settings (150mm/s, unSunken) and starting at 3ma reducing to 1ma because it was burning 3-4mm deep instead of engraving the surface.



And the bottom of the board was using K40Whisper.  The winner is pretty clear lol.



Will Do tests tomorrow to compare Vector Cutting and Raster Cutting between the two.

![images/badbab38f0a5b75fc461fa93946c1bbd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/badbab38f0a5b75fc461fa93946c1bbd.jpeg)



**"Kenneth White"**

---


---
*Imported from [Google+](https://plus.google.com/100692370397257622708/posts/Xxox1R3Seti) &mdash; content and formatting may not be reliable*
