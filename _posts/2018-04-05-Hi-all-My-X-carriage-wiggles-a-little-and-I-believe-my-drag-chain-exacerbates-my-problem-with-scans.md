---
layout: post
title: "Hi all! My X carriage wiggles a little and I believe my drag chain exacerbates my problem with scans"
date: April 05, 2018 22:19
category: "Original software and hardware issues"
author: "Travis Sawyer"
---
Hi all!



My X carriage wiggles a little and I believe my drag chain exacerbates my problem with scans.  If I'm burning towards max X, the scans are very clear, toward the middle or min X, my scans are a bit wide.



Is there any way to tighten the X carriage? My belts are at good tension, no skipped steps that I can detect.



I'm going to try to print a better (looser) drag chain, but I'd like to get the carriage tighter.  I wish the wheels were on eccentrics!



Thank you!





**"Travis Sawyer"**

---
---
**Travis Sawyer** *April 05, 2018 22:50*

Answering my own question - maybe it will help someone.  The rollers for the x carriage are on eccentrics.  Loosen the horizontal screws on the bottom of the carriage and turn the screw for the nylon rollers.


---
*Imported from [Google+](https://plus.google.com/+TravisSawyer/posts/UHasgz8uaUu) &mdash; content and formatting may not be reliable*
