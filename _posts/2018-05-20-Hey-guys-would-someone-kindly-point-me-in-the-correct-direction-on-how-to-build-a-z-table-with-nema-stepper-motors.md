---
layout: post
title: "Hey guys, would someone kindly point me in the correct direction on how to build a z table with nema stepper motors.?"
date: May 20, 2018 09:10
category: "Modification"
author: "James poulton"
---
Hey guys, would someone kindly point me in the correct direction on how to build a z table with nema stepper motors.? I'm going to have a learning day. 





**"James poulton"**

---
---
**Fernando Bueno** *May 20, 2018 11:44*

I do not know if it's worth doing a motorized mechanism on a machine with these characteristics, but here you have one: [https://www.thingiverse.com/thing:2731038](https://www.thingiverse.com/thing:2731038)



And if you want a manual lifting system and 100% operational, look at this other one: [thingiverse.com - K40 Chinese Laser adjustable build plate by peterrsanchez](https://www.thingiverse.com/thing:2321473)


---
**Jake B** *May 20, 2018 12:26*

I just built a Holgamods variant.  You can find the post below somewhere.  
{% include youtubePlayer.html id="3XWOvtc_Jo8" %}
[youtube.com - Holgamods Powered Z Bed for K40 and other small Lasers](https://www.youtube.com/watch?v=3XWOvtc_Jo8)


---
**James poulton** *May 21, 2018 23:12*

Cheers guys, I've got my head around it a bit now. Just need to find someone to laser print me some bits. That does look like a great kit Jake, just I want to build one from scratch so I'm learning more. 


---
*Imported from [Google+](https://plus.google.com/113949094171942001104/posts/VmF8Lwc5Aqf) &mdash; content and formatting may not be reliable*
