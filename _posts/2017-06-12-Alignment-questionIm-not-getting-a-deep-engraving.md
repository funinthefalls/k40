---
layout: post
title: "Alignment question...I'm not getting a deep engraving"
date: June 12, 2017 22:48
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Alignment question...I'm not getting a deep engraving. I have to increase the power to do so and I'm hoping it's because of an alignment issue on this new k40 and not a bad/old tube. 



I got all the mirrors aligned and working on the last one. Is this good enough? Any ideas what I can do to make it better if not?

![images/651b77654f7bd0b23c02b7cd14702447.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/651b77654f7bd0b23c02b7cd14702447.jpeg)



**"Nathan Thomas"**

---
---
**Anthony Bolgar** *June 12, 2017 22:57*

Is that dot at all locations on the bed?


---
**Ned Hill** *June 12, 2017 23:01*

if you wanted to get more center you would need shim the head plate  up a bit with some washers.  Also engraving depth is going to depend on both power and speed.  Slow it down and or increase power for more depth.


---
**Nathan Mueller** *June 12, 2017 23:03*

The dot looks correct to me, centered and slightly above halfway. I assume that was tested from multiple locations? Also make sure your lens and mirrors are clean. The lens should have the curved side facing UP, not down. That was a mistake I made myself.



When you do a test fire, does the scorch mark look perfectly circular, or is there any sort of oblong or ghosting pattern? That indicates you have alignment issues. 


---
**Nathan Thomas** *June 12, 2017 23:33*

**+Anthony Bolgar** yes


---
**Nathan Thomas** *June 12, 2017 23:34*

**+Ned Hill** yeah that's the problem. On my other k40 I don't have to go as high on the power to get the same depth as on this one. I use the same settings and get different results 


---
**Nathan Thomas** *June 12, 2017 23:36*

**+Nathan Mueller** that was my main question...did you guys think it was too high because it's not perfectly centered. I didn't know if that mm or two would make a big difference?


---
**Anthony Bolgar** *June 13, 2017 01:20*

The stock head has a 12mm lens that has a very small sweet spot. I upgraded to the lightobject air assist head with the 18mm lens. Allows for a little bit of wiggle room on the alignment as it has a much larger sweet spot (Plus the added bonus of an air assist nozzle)


---
**Don Kleinschnitz Jr.** *June 13, 2017 11:32*

Another check is to verify that the beam is coming out perpendicular to the bed. Misalignment of the laser or other optics can cause this and can reduce power traversing the objective lens off center.

Didn't catch whether or not this machine worked better before or not and if this is a new problem on a working machine or a new machine trying to get going?

Is this the same machine you are having PWM issues with and replaced the lens on etc. (looking at your other posts).



Suggest it might be good to give us a complete view of the machines config all in one place. 



Then again maybe I am not following effectively :).



Configuration ?

1. Controller? 

2. PWM connection method?

3. Lens type and FL, verified by ramp test?

4. Setting of bed from objective lens?

5. Speed and power that you are running the test with? 

6. Current that the meter reads during the test

7. Driving software (LW or others)?


---
**Don Kleinschnitz Jr.** *June 13, 2017 12:14*

**+Nathan Thomas** ignore my question on the PWM problem I got two different Nathan's confused.


---
**Nathan Thomas** *June 13, 2017 12:38*

**+Don Kleinschnitz** this is a new machine fresh out the box that I'm  trying to get going. 



The beam is hitting the burn hole at the same spot all over the bed. The issue is I don't feel like I'm getting enough power and have to increase the power to get a good engraving. I was thinking it was because it's hitting the lens holder 1-2 mm too high, but maybe that's not it. I saw a YouTube video that said it should hit it dead center. 



I measured the x axis bar and it seems straight in reference to the bed. Had to check that first because it wasn't when it arrived...was off a half inch because of all the things they stuffed into the machine for delivery. 



I do have another k40 but the tube is blown and I'm awaiting the delivery of that. But that's how I know what engraving settings work and I'm not getting the same results on this new machine.


---
**Joe Alexander** *June 13, 2017 13:05*

first thing I would try is flip your focus lens. even if you think its right, trust me these things defy logic. 6/10 times this resolves the issue. 

    Next I would make sure you are getting a perfectly round dot exiting the focus lens, showing that no beam "clipping" is occurring. 

    Third i would verify what the pot is set to with a multimeter to ensure that the same power level you are familiar with your other machine is being used. Could always be a bad pot. I added a small voltmeter to mine so I can use the voltage in as my power settings (0-5V).

   Finally I would double check/re-insulate the laser tube connections, and change my water/flush out the lines/replace with  distilled water only. If that doesn't resolve it I can only assume the laser PSU is factory-tuned differently, the tube is weaker, or a mirror/lens is sub-par.


---
**Don Kleinschnitz Jr.** *June 13, 2017 13:21*

**+Nathan Thomas** one other thing? Have you run a ramp test. My machine came with a non standard lense and I flailed with power for a while until I found the FL was not normal.


---
**Nathan Thomas** *June 13, 2017 13:46*

**+Joe Alexander** this is what I'm getting, looks round on test firing and the engraving is straight while running tests. Just seems to be lacking power. 



I will try flipping the lens. 



What is the pot? I can buy a multimeter but what am I testing?

![images/6c52eb9ce9ec73e5e02a34f89503f55a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c52eb9ce9ec73e5e02a34f89503f55a.jpeg)


---
**Nathan Thomas** *June 13, 2017 13:51*

**+Joe Alexander** **+Don Kleinschnitz** 

I'm still measuring. Found the y axis bars on the gantry to be off relative to body of the laser about 1/8". The laser tube is off about 1/16" relative to the body and therefore x axis. My question now is that 1/16" enough to cause this issue?



I read somewhere that it's more important to have the tube parallel to the body than the gantry. But not sure if that 1/16 is detrimental enough to make a difference.


---
**Don Kleinschnitz Jr.** *June 13, 2017 15:32*

**+Nathan Thomas** never heard that the gantry/laser needs to be aligned to the frame? My view is that the laser/optics need all to be referenced to the gantry. 


---
**Joe Alexander** *June 13, 2017 15:44*

beam dot looks nice and round so that cuts out beam clipping. By the pot I mean the analog potentiometer on the control panel unless yours has the digital one. And the multimeter is used to measure the voltage between 5V and IN on the laser PSU (this voltage controls the beam power output )5V, IN, and GND are normally connected to the pot on the front control panel.

  As to the laser tube being off: definitely true it up to the gantry. Also this would be a great time to see if you can also shift it down a hair so it hits dead center on the laser head(or go all out and add 3D printed tube mounts!)


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/4af8SKAxwcw) &mdash; content and formatting may not be reliable*
