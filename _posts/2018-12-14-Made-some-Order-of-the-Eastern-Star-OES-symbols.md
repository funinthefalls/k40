---
layout: post
title: "Made some Order of the Eastern Star (OES) symbols"
date: December 14, 2018 03:31
category: "Object produced with laser"
author: "Ned Hill"
---
Made some Order of the Eastern Star (OES) symbols. 3mm birch ply. Coloring was done with sharpie markers except for the white which was a paint pen. Finished with a spray lacquer. 



![images/d2c89f5a037a1ddf73c612d01d68e0fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d2c89f5a037a1ddf73c612d01d68e0fb.jpeg)
![images/0f3027ff5f3d6673023b434037c25d23.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f3027ff5f3d6673023b434037c25d23.jpeg)

**"Ned Hill"**

---
---
**Travis Sawyer** *December 14, 2018 12:01*

Looking good, brother. I know a few ladies that would love those. Could you link in the source?


---
**Don Kleinschnitz Jr.** *December 14, 2018 17:39*

Man u have this dialled in!


---
**Ned Hill** *December 14, 2018 17:50*

**+Don Kleinschnitz Jr.** Thanks! 😁


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/18z5RLahTAJ) &mdash; content and formatting may not be reliable*
