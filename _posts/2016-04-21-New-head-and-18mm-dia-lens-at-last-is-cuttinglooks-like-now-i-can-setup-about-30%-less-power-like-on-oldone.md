---
layout: post
title: "New head and 18mm dia lens at last is cutting,looks like now i can setup about 30% less power like on oldone"
date: April 21, 2016 12:22
category: "Discussion"
author: "Damian Trejtowicz"
---
New head and 18mm dia lens at last is cutting,looks like now i can setup about 30% less power like on oldone 

![images/d1bb705097562b57565f1039fd05399c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1bb705097562b57565f1039fd05399c.jpeg)



**"Damian Trejtowicz"**

---
---
**Phillip Conroy** *April 21, 2016 16:07*

are you using air assist and what psi rate?,what type if bed are you using?


---
**Damian Trejtowicz** *April 21, 2016 16:17*

Yes im using air assist an temporary was on stock bed

Today im cutting some slats for bed(same type like in Trumpf machines,but in micro scale)


---
**Phillip Conroy** *April 21, 2016 19:36*

Lots of fume marks on cut items lower left looks good -is this one showing the rear of the cut,with my setup the rear of the cut is noramly cleaner than the top,if you can try increasing air pressure-i am running full size air compressor amd using 10 psi.l to found that the 18mm focal lens heaps better than stock lens size.C an you post some photos of the work peace still in the surounding material ,top and bottom


---
**Damian Trejtowicz** *April 21, 2016 19:46*

This diry side is bottom,i was cutting on solid plate


---
**Phillip Conroy** *April 22, 2016 05:55*

i was using a solid plate for awhile then switched to 10mm wide by 15mm  long magnets with 50mm long screws held on top ,this way i can use it as a pin bed as well as move them around ,was going to make 1 dozen magnets/ screw combos however only needed  4 so far- this will help lots with the fume marks and the cut parts just drop down  clean


---
**Phillip Conroy** *April 22, 2016 05:56*

i was thinking about a honeycomb bed then thought i would be forever cleaning it


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/bQhDydgXHPK) &mdash; content and formatting may not be reliable*
