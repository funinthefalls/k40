---
layout: post
title: "Ok folks, I need some help here"
date: November 24, 2015 17:57
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Ok folks, I need some help here. I had previously purchased some 8x12 Maple wood veneer and did some test engraving and cuts and everything was fine. Recently I went back to the same vendor and ordered larger pieces (12x24) which I would then cut down to the required 8x12 for the laser cutter. However, for some reason, the wood is different. They're telling me there may have been a vendor switch which explains the difference. The bad news is, I can not cut through it. It's the same thickness, same settings as the original test pieces I had, but with this new batch of wood, it barely gets to half way deep into the wood. Literally nothing has changed. I even put a previous piece back and tried cutting that and it worked without a problem. It's the new batch of veneer that I got, it simply won't cut through.



So at this point I'm looking for suggestions so I could still work with this wood because I need to get the job out the door. Cranking up the power results in the wood just burning up and charring. So what do I do? Run multiple cut passes? Slower passes?





**"Ashley M. Kirchner [Norym]"**

---
---
**Ashley M. Kirchner [Norym]** *November 24, 2015 18:19*

It's 0.105 in thick (just under 3mm) and it's <b>supposed</b> to be Maple, however it looks different than what I had received before, it feels different, it has a shiny surface, all are characteristics that are different from what I had ordered before, even though on both orders, the wood was labeled 'Maple Wood Veneer' ... The only explanation that their customer service had for me was that 'there may have been a vendor change for the wood' ... gee, thanks.



I did not check moisture because I don't have the means to do that. And I do have air assist on the machine. I'm about to try doing a multi-pass test cut, see if I can get through the thing.


---
**Coherent** *November 24, 2015 19:47*

It's likely been treated or coated differently than your previous piece. Not much you can do except multi-pass cut it. Turning the power up will more than likely just burn the wood more and widen the cut kerf. You can try slowing it down a little but multiple passes will prob give the better results.


---
**DIY3DTECH.com** *November 24, 2015 19:56*

Likely as Marc G points out, they might have treated it differently.  Also you can pick up a cheap moisture meter at Harbor Freight to test how much water is in the wood (just a handy tool to have) as I have better luck cutting dryer wood than wet wood.  For grins and giggles, maybe try two things (1) attempt to dry it out and see what happens and (2) sand a piece and try it.  Agin your milage might vary as I am only rambling aloud...


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/jaqYTzvcvdi) &mdash; content and formatting may not be reliable*
