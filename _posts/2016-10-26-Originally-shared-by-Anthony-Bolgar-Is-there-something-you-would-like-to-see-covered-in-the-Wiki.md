---
layout: post
title: "Originally shared by Anthony Bolgar Is there something you would like to see covered in the Wiki?"
date: October 26, 2016 20:44
category: "Discussion"
author: "Anthony Bolgar"
---
<b>Originally shared by Anthony Bolgar</b>



Is there something you would like to see covered in the Wiki? Or have a great walk through on doing something in LaserWeb that has not been added to the Wiki yet? Send me a PM and I will try to get it posted.





**"Anthony Bolgar"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2016 21:17*

How to attach laser to shark! Just kidding, but keep up the good work.


---
**Claudio Prezzi** *October 26, 2016 23:35*

Configuration for GRBL v1.1 would be nice 😉


---
**Anthony Bolgar** *October 27, 2016 01:13*

Will work on it. Thanks


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/hPSeNEUBZTT) &mdash; content and formatting may not be reliable*
