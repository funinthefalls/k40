---
layout: post
title: "Can someone explain to me technically what's going on here?"
date: September 28, 2016 02:10
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Can someone explain to me technically what's going on here? Is not burning the top but yes the bottom?  WTH



![images/ae8201d8b8d00aa8ba111f0c8bddcece.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae8201d8b8d00aa8ba111f0c8bddcece.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/--Te2_LZM3qM/V-smg1nEWII/AAAAAAABvR0/vtI_Tix6l6gJczP-2jpf3eMyYVThOHLHQCJoC/s0/20160927_210818.mp4.gif**
![images/c376b3416da5622d34dcd590bbf3717b.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/c376b3416da5622d34dcd590bbf3717b.gif)

**"Ariel Yahni (UniKpty)"**

---
---
**Scott Marshall** *September 28, 2016 02:44*

The acrylic has a very high transmission rate at the lasers wavelength, so it doesn't absorb any energy. The wood blocks all wavelengths, so absorbs the laser energy, burning it.



The principle is the reason a k40 doesn't destroy it's own lens, even though it has very poor wideband transmission (so looks nearly opaque) at many wavelengths, it has a very high transmission rate at 10,8 micron.



I'm not 100% up on the molecular physics involved, but it's sort of like a screen which gravel will pass through, and you dump a bucket full sand thru and it has little effect, but if you dump in a bucket full of rocks, it tears the screen right out of the frame. Think of the acrylic in the video as haveing sub molecular holes in it which let the short wavelength violet or blue light thru. If you hit the same piece of acrylic with the longer IR from a CO2 laser, it will be absorbed and vaporize the acrylic.



It's not really that simple, but the real explanation makes my head hurt...



Scott


---
**Anthony Bolgar** *September 28, 2016 02:52*

I take it you are using a laser diode?


---
**Don Kleinschnitz Jr.** *September 28, 2016 03:49*

This isn't 10600 nm is it? 445 nm laser diode? Its transparent .....

![images/f3309502806c73cbcbb558c1495b4737.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3309502806c73cbcbb558c1495b4737.jpeg)


---
**Randy Randleson** *September 28, 2016 09:38*

yes, the acrylic, should have its opaque adhesive protectant layer

 on it so the laser doesnt pass through it.


---
**Ariel Yahni (UniKpty)** *September 28, 2016 11:52*

Ok so keeping the protective layer make sense but as I need multiple passes to cut it it will not help in  the long run. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 28, 2016 14:31*

I guess this is the same reason that not all laser goggles are suitable for all types of laser wavelength. Interesting to see it actually in action, so thanks for sharing Ariel :)


---
**Ariel Yahni (UniKpty)** *September 28, 2016 14:34*

**+Yuusuf Sallahuddin**​ it was like a nightmare / magic trick 


---
**Scott Marshall** *September 28, 2016 16:48*

**+Yuusuf Sallahuddin** 

Much more elegant way of putting it.!


---
**Claudio Prezzi** *September 29, 2016 07:21*

The graph from **+Don Kleinschnitz**​​ shows, that normal Plexiglas (Acrylic) should be a good eye protection for CO2 Laser ;)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/5sSCTZuiKq1) &mdash; content and formatting may not be reliable*
