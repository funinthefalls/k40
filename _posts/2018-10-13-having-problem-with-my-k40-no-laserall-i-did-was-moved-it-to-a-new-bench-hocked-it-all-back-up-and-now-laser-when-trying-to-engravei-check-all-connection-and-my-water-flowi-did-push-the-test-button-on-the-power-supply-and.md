---
layout: post
title: "having problem with my k40 no laser.all i did was moved it to a new bench hocked it all back up and now laser when trying to engrave.i check all connection and my water flow.i did push the test button on the power supply and"
date: October 13, 2018 11:04
category: "Discussion"
author: "heath finfrock"
---
having problem with my k40 no laser.all i did was moved it to a new bench hocked it all back up and now laser when trying to engrave.i check all connection and my water flow.i did push the test button on the power supply and it does work.so now i am lost please help me out.thanks





**"heath finfrock"**

---
---
**Don Kleinschnitz Jr.** *October 13, 2018 14:04*

if you have a water flow sensor?,  that usually is the problem.



Otherwise try the guide that is here:

[donsthings.blogspot.com - Troubleshooting a K40 Laser Power System](https://donsthings.blogspot.com/2018/05/troubleshooting-k40-laser-power-system.html)


---
**heath finfrock** *October 13, 2018 16:08*

**+Don Kleinschnitz Jr.**  thanks for the help that was the problem the flow sensor.


---
**Don Kleinschnitz Jr.** *October 13, 2018 16:35*

**+heath finfrock** 👏👏👏 ..... nailed it!


---
**Don Kleinschnitz Jr.** *October 13, 2018 16:37*

**+Don Kleinschnitz Jr.** Can you post a picture of your flow sensor in the machine. I will create a post about this problem. 

How did you fix it?




---
**heath finfrock** *October 13, 2018 16:41*

**+Don Kleinschnitz Jr.** ![images/edef87d451515c20aa54b27e0b206cd8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/edef87d451515c20aa54b27e0b206cd8.png)


---
**Don Kleinschnitz Jr.** *October 13, 2018 18:50*

**+heath finfrock** How did you fix it?


---
**heath finfrock** *October 13, 2018 18:54*

**+Don Kleinschnitz Jr.** i pulled it off and cleaned it out .




---
*Imported from [Google+](https://plus.google.com/115526585190265031410/posts/57yWib4bnCv) &mdash; content and formatting may not be reliable*
