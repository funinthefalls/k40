---
layout: post
title: "What's the most common use of your laser"
date: June 29, 2016 04:44
category: "Discussion"
author: "Alex Krause"
---
What's the most common use of your laser. Im asking for input so I can make suggestions for new features for laserweb. Please choose a category that most closely resembles your use of your laser and if other please specify your main use.





**"Alex Krause"**

---
---
**Alex Krause** *June 29, 2016 05:52*

**+Ariel Yahni**​ I can't figure out how to do a statistic post on FB can you help me out and post this on the laser group there or msg me how to do it on FB


---
**Alex Krause** *June 29, 2016 06:00*

**+Ariel Yahni**​ I figured it out... sorry I tagged you twice 


---
**Jeremie Francois** *June 29, 2016 06:32*

How do I know the results without skewing them? I own no laser cutter but I wish knew the results... Can click "other" but it should be "waaant" :p


---
**Alex Krause** *June 29, 2016 06:40*

Lol **+Jeremie Francois**​ Google has only 4 options for statistics max :P click other you haven't skewed you followed the directions of the post. Thank you


---
**Venuto Woodwork** *June 29, 2016 11:37*

I use mine to make wood lapel pins and wood pocket square and thin 1/16 strips for inlay for my wood Bow Ties.


---
**Tev Kaber** *June 29, 2016 21:22*

One thing I will probably use my K40 for the most is board game mods.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/K2xEE2DtCgu) &mdash; content and formatting may not be reliable*
