---
layout: post
title: "Is there anyone that would be able to help me with my mirror alignment?"
date: November 01, 2016 10:41
category: "Original software and hardware issues"
author: "photomishdan"
---
Is there anyone that would be able to help me with my mirror alignment? I've done as much as I can, seems to be in focus, seems to cut where i want it to, the only thing is, its cutting at a slight bevel. What do I need to do to get a cut thats straight and true? At the moment the only thing I'm cutting is 3mm ply.

I'm currently using stock mirrors and lens (no funds to upgrade)

Any help with this would be amazing, or pointing me in the right direction. I've tried searching for answers but to no avail.

Regards, Dan.





**"photomishdan"**

---
---
**Andy Shilling** *November 01, 2016 10:55*

I'm new to all this too but from my understanding it seems you have your ply to far away. Thing I've read say you need to raise your ply up towards the f focus lense. 



As the laser cuts it opens up as a cone like a beam of light from a torch so you need to hit the optimum position which I believe to be 50.2mm. 



I hope this helps and I've not made a complete arse out of myself.



Andy 


---
**photomishdan** *November 01, 2016 11:00*

**+Andy Shilling** hi Andy, I currently have my ply at around the 50.4 mm mark, so I don't think it's due to that. Thank you for your reply.


---
**Andy Shilling** *November 01, 2016 11:03*

No problem in just glad you understood what I was trying to say lol. 



The only other thing I could think would be your bed is set at an angle do not cutting at a 90 degree angle.



I hope you get it sorted 


---
**photomishdan** *November 01, 2016 11:18*

**+Andy Shilling** you're a genius... well my bed is certainly not level, need to do a test cut but I think you've actually figured it out! Why do I always over look the simple things??


---
**Andy Shilling** *November 01, 2016 11:22*

Great, I'll do the simple thinking and you can do the rest lol. I really hope that solves it for you.



Andy 


---
**Ariel Yahni (UniKpty)** *November 01, 2016 11:46*

In  case is not the bed and for future reference the head carriage could also be at an angle, in that case you need to shim it at the posts.


---
**photomishdan** *November 01, 2016 11:50*

**+Ariel Yahni** oooooh I may have to get back to you about that after my tests with a newly levelled bed


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 01, 2016 12:01*

Another possibility is the mirror on the carriage. It has been known to not be at a direct 45 degrees to the bed/rail at times. So, you can shim under the top part of the head (with pieces of something thin) to raise it & level it out. Sometimes the carriage plate could not be level also. That could be sorted by adding washers to the 3 posts that hold it on the carriage.


---
*Imported from [Google+](https://plus.google.com/105674876465839652573/posts/h2tyrE8CNrD) &mdash; content and formatting may not be reliable*
