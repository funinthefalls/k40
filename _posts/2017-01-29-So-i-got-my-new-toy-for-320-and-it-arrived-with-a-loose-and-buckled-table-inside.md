---
layout: post
title: "So i got my new toy for 320 and it arrived with a loose and buckled table inside"
date: January 29, 2017 07:53
category: "Original software and hardware issues"
author: "David Rowlinson"
---
So i got my new toy for £320 and it arrived with a loose and buckled table inside. Seller is offering me £30 to repair it myself (i asked for 160 due to having to take all the internals out and re assemble and setup)



Do i just ask for a new one or take the 30 and fix it, knowing i will have to do lots of work on it in future anyway?



Also the sprung table doesnt open far, is there a common replacement for the table and how is the laser calibrated to the table?



Thanks

David

![images/ccc4fdb9f35ede8c3fbcce0fc53c21c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ccc4fdb9f35ede8c3fbcce0fc53c21c9.jpeg)



**"David Rowlinson"**

---
---
**Stephane Buisson** *January 29, 2017 09:43*

take the all x/y assembly out, remove the bed and make yourself a new one with honeycomb (at focal point hight). Most important is check your x/y assembly is square, and parrallel with tube when refit.



Enjoy ! welcome here  **+David Rowlinson** by the way.


---
**Ian C** *January 29, 2017 11:56*

Hey buddy. You're going to have to remove the bed and carriage anyway to check for alignment and squareness so I'd say take the £30 and buy a honeycomb bed. Just check the tube and any other major components aren't damaged as £30 won't cover them


---
**David Rowlinson** *February 02, 2017 09:09*

Hey guys thanks, ive taken the £30. Dragged out an old PC and have set everything up but not fully tested yet.

Is there a guide for laser/mirror alignment and checking correct focal point for table height? 


---
*Imported from [Google+](https://plus.google.com/109306984619175394015/posts/Y6xphjJvJiw) &mdash; content and formatting may not be reliable*
