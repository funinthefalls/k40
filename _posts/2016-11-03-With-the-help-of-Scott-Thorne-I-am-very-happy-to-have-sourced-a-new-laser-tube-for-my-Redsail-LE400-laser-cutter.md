---
layout: post
title: "With the help of +Scott Thorne, I am very happy to have sourced a new laser tube for my Redsail LE400 laser cutter"
date: November 03, 2016 23:18
category: "Modification"
author: "Anthony Bolgar"
---
With the help of +Scott Thorne, I am very happy to have sourced a new laser tube for my Redsail LE400 laser cutter. It was the last thing I needed to finish upgrading this laser. I have already Smoothified it, installed a powered Z table, installed a wifi interface, touchscreen panel, made a cooling system with a PC radiator and fans, and a safety system. The tube I have been able to get measured 58Watts of output power at the 2nd mirror using a laser power meter. This is virtually twice the output power of a standard k40 tube. And it fits inside the tube bay without having to make an extension box. I just need to print out some 50mm adjustable tube hangars for it. I can't wait to see how thick of acrylic or plywood that it will be able to cut in one pass. And I am sure it will make some awesome 3d laser carvings.





**"Anthony Bolgar"**

---
---
**greg greene** *November 04, 2016 00:14*

Where did the tube come from?  Did it require a different PSU?


---
**Anthony Bolgar** *November 04, 2016 00:30*

The tube is a Reci that Scott had extra. I had already upgraded the PSU in the Redsail,. originally think about using a 50W tube, the PSU I had purchased can handle this new tube.


---
**Paul de Groot** *November 04, 2016 05:06*

Post some pictures ☺


---
**Anthony Bolgar** *November 04, 2016 05:25*

Will do once I have the tube installed, will be a couple of weeks at least, have knee surgery in 7 hours.


---
**Scott Thorne** *November 04, 2016 09:41*

Good luck with the knee bro.


---
**Anthony Bolgar** *November 04, 2016 10:08*

Thanks Scott


---
**Don Kleinschnitz Jr.** *November 04, 2016 10:25*

BTW, hint on the knee surgery. Its all about the rehab, do it religiously and take it seriously and your knee will be better than new. My wife is very happy with hers. Good luck ....


---
**Anthony Bolgar** *November 04, 2016 10:33*

Thanks for the good wishes **+Don Kleinschnitz** . This is just some repairs to the meniscus (3  tears, a hole the size of a nickel, and 2 flaps that are getting folded under) and fixing a torn ACL (Torn at halfway, and 2/3 along its length)

The total knee replacements both right and left, will be about 18 months away. This will be my 3rd stopgap fix on my knees, just had the left done 4 weeks ago, right today, and had the right done 16 months ago.


---
**Don Kleinschnitz Jr.** *November 04, 2016 10:36*

AH, my wife went through the same and the knee replacement ended it all :).


---
**HalfNormal** *November 04, 2016 18:18*

I am an Operating Room nurse and our hospital does dozens of knee replacements a week. Do your homework and find the best surgeon available. Do what Don said, you will not be sorry. If you have questions, I will answer them the best I can.


---
**Ulf Stahmer** *November 04, 2016 20:15*

Good Luck **+Anthony Bolgar**! But I thought this was a laser cutter forum and not a laser surgery one! :) 


---
**greg greene** *November 04, 2016 20:33*

does makerbot have replacement knee files?


---
**Scott Thorne** *November 04, 2016 21:08*

Lol...you guys crack me up! 


---
**Don Kleinschnitz Jr.** *November 05, 2016 11:43*

Make sure that they don't use a level shifter the knee may not align correctly .....lol


---
**Anthony Bolgar** *November 05, 2016 11:54*

All my fears were for not, other than the surgery (most likely the anesthetic) helped me go into early congestive heart failure (fluid around the heart and n the lungs). At least there was a quick indicator of my calves, ankles and feet swelling up to 2-3 times their regular size Treated it with stong diuretics and a catheter (I HATE CATHETERS!!!) Spent fro 2 am to 6 am being treated. Back at home fortunately.


---
**Don Kleinschnitz Jr.** *November 05, 2016 11:58*

Whoa, sorry to hear it wasn't smooth. I hope all is well now....


---
**Anthony Bolgar** *November 05, 2016 12:46*

Getting there, have to pee every 10-15 minutes because oth the powerful diuretics they gave me.


---
**Scott Thorne** *November 05, 2016 13:29*

Get well soon brother!


---
**HalfNormal** *November 05, 2016 15:37*

At least your knee is working now and you can make it to the bathroom! 


---
**Scott Thorne** *November 05, 2016 18:20*

**+HalfNormal**...

He might be bedpanning it...lol


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/EJmLc8sGEAP) &mdash; content and formatting may not be reliable*
