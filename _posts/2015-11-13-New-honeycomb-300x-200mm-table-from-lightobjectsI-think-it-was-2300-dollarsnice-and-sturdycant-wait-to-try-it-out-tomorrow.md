---
layout: post
title: "New honeycomb 300x 200mm table from lightobjects....I think it was 23.00 dollars....nice and sturdy...can't wait to try it out tomorrow"
date: November 13, 2015 15:30
category: "Hardware and Laser settings"
author: "Scott Thorne"
---
New honeycomb 300x 200mm table from lightobjects....I think it was 23.00 dollars....nice and sturdy...can't wait to try it out tomorrow.

![images/c75725820e8069013bd8f3a94073189f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c75725820e8069013bd8f3a94073189f.jpeg)



**"Scott Thorne"**

---
---
**Scott Thorne** *November 15, 2015 12:44*

I haven't installed it yet.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/NqVw1R5bF11) &mdash; content and formatting may not be reliable*
