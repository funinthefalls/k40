---
layout: post
title: "Martin Dillon For some reason I could not reply to this post?"
date: March 24, 2017 15:32
category: "Modification"
author: "Don Kleinschnitz Jr."
---
**+Martin Dillon**

For some reason I could not reply to this post?



All the interlocks are in series and connected across your LPS enable which depends on what LPS you have? i.e P+/gnd, WP....

On the site you can find links to the original schematics if you want better quality images.

A complete protection wiring schema for a LPS with P+ is: P+, front cover, back cover, water flow switch, temp alarm, anything else that you want to shut off the laser if not working , gnd. The contacts of these devices are in series.



The current is small about 5-10 ma. 



Relevant sections of the blog where there are links to the parts and detailed schematics:

[http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html](http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html)

[http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)



<b>Originally shared by Martin Dillon</b>



Help.  I don't like to ask newbie questions but that is what I am. 

I joined the group about 1 month ago and received my k40 Monday.  I want to make sure I do everything right and not make any stupid mistakes.  Right now, I am looking at setting up the cooling and safety lockouts.  I looked through the post topics but they are not sorted very well.  I have been looking through [http://donsthings.blogspot.com/](http://donsthings.blogspot.com/) and there is a lot of useful stuff there but his interlock diagram didn't make since to me and was low resolution and hard to read. I read through the post on what type of water to use and plan on using distilled water and algaecide.  I am having trouble finding the right flow meter/switch to use for my safety circuit.  

Are all the switches just put into series, if so, how much current flows through the circuit?  I also was not able to find where it hooks to the control board.



Feel free to add any advice you think I might need.  I have read alot about laser alignment and downloaded some of the recommended guide and I don't think I will have problems with that.





**"Don Kleinschnitz Jr."**

---
---
**Stephane Buisson** *March 24, 2017 16:42*

I don't know what happen with that post. not a moderator work.



**+Martin Dillon** the switches are connected to PSU not control board.

most of the work is done in PSU. control board is mainly for motion.




---
**Mark Brown** *March 24, 2017 19:52*

Your interlock switches will be connected in series with the "laser enable" switch on the dash, at least if you have one of those.



There's an option to disable comments by clicking the ... when posting a new thread.  He must have bumped it somehow.





![images/abfe66b46e4ef1a2050d86a2520bf00b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/abfe66b46e4ef1a2050d86a2520bf00b.png)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/CBWVJX2cXEd) &mdash; content and formatting may not be reliable*
