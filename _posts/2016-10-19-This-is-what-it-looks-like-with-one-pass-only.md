---
layout: post
title: "This is what it looks like with one pass only"
date: October 19, 2016 15:27
category: "Object produced with laser"
author: "Scott Thorne"
---
This is what it looks like with one pass only.

![images/2aa8d44451ac2d7c5ffcb478e2e3d10a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2aa8d44451ac2d7c5ffcb478e2e3d10a.jpeg)



**"Scott Thorne"**

---
---
**Ariel Yahni (UniKpty)** *October 19, 2016 15:35*

I did get something similar to that at one point, but when I say similar I mean "please Scott stop you are making me feel bad"  type


---
**Scott Thorne** *October 19, 2016 15:39*

**+Ariel Yahni**...lol...sorry. 


---
**Todd Miller** *October 19, 2016 22:56*

so your max burn depth is 3/16  or so ?

What focal length ?



Oh and Awesome !




---
**Scott Thorne** *October 19, 2016 23:00*

**+Todd Miller**...I'm using a 1.5 inch hd lense right now....I'm gonna try a 2 inch tomorrow and I'm gonna repost the results.


---
**Scott Thorne** *October 19, 2016 23:01*

**+Todd Miller**...the depth is determined by how many passes i do.


---
**Tony Schelts** *October 20, 2016 07:44*

Hi Scott I realise this may be a dumb question,  but does the size really matter :) Lens I mean.  How are you using different lenses. New laser head ????




---
**Scott Thorne** *October 20, 2016 09:39*

**+Tony Schelts**...yes shorter focal lenght is a smaller dot size..so its better definition on the work.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/K9yuAYopbfM) &mdash; content and formatting may not be reliable*
