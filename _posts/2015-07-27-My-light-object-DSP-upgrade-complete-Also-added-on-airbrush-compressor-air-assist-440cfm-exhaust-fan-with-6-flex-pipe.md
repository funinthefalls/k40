---
layout: post
title: "My light object DSP upgrade complete. Also added on airbrush compressor air assist, 440cfm exhaust fan with 6\" flex pipe"
date: July 27, 2015 16:29
category: "Modification"
author: "Dreamsforgotten"
---
My light object DSP upgrade complete.  Also added on airbrush compressor air assist, 440cfm exhaust fan with 6" flex pipe.



[http://imgur.com/a/UeNB1](http://imgur.com/a/UeNB1)





**"Dreamsforgotten"**

---
---
**Fadi Kahhaleh** *July 28, 2015 00:26*

Congrats on the upgrade...  it is money well spent :)


---
**Dreamsforgotten** *July 28, 2015 00:27*

**+Fadi Kahhaleh** thank you, and yes it is! 


---
**Fadi Kahhaleh** *July 28, 2015 01:57*

**+James DeVesci** I have a draft design for mounting the DSP controller and other accessories without cramping things in there. (my setup has more gauges and buttons)

I'll share it here in a few days, just gotta find the time to finalize the design as I did notice a thing or two when I cut the draft.


---
*Imported from [Google+](https://plus.google.com/101938482052980748689/posts/EAkRzqpnTzX) &mdash; content and formatting may not be reliable*
