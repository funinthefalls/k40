---
layout: post
title: "Made a video about polypropylene laser cutting as I only found different opions on how you can cut polyproplene with a lower power, cheaper laser cutter like the K40"
date: June 18, 2017 08:08
category: "Materials and settings"
author: "Bernd Peck"
---
Made a video about polypropylene laser cutting as I only found different opions on how you can cut polyproplene with a lower power, cheaper laser cutter like the K40.



Hope you find my results worthwhile and use it for your own projects.




{% include youtubePlayer.html id="bppNQfhxqXc" %}
[https://youtu.be/bppNQfhxqXc](https://youtu.be/bppNQfhxqXc)







**"Bernd Peck"**

---
---
**Jorge Robles** *June 18, 2017 10:08*

Very interesting. I tried a couple of times but need to find a better material (abs?). Also need to try different pwm period using grbl-lpc $33


---
**Bernd Peck** *June 19, 2017 07:17*

According to these people ABS tends to only melt and catch fire in the laser 

[atxhackerspace.org - Laser Cutter Materials - ATXHackerspace](http://atxhackerspace.org/wiki/Laser_Cutter_Materials)


---
**Martin SL** *September 04, 2017 03:29*

I have cut ABS with no problem. 11mA/10mms  No fire and no melting.

![images/0c66fe135113002a6fbca404cf01fcce](https://gitlab.com/funinthefalls/k40/raw/master/images/0c66fe135113002a6fbca404cf01fcce)


---
*Imported from [Google+](https://plus.google.com/100238441539373475210/posts/Tc9LEXYsc8x) &mdash; content and formatting may not be reliable*
