---
layout: post
title: "Hi guys, just noticed this crack in my laser tube!"
date: April 19, 2015 19:05
category: "Discussion"
author: "David Wakely"
---
Hi guys, just noticed this crack in my laser tube!

is this game over for my laser or can it be repaired?

![images/226d93ba51f2b4434f86333356bc7137.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/226d93ba51f2b4434f86333356bc7137.jpeg)



**"David Wakely"**

---
---
**Tim Fawcett** *April 24, 2015 12:13*

Did you run it without water and then turn the water on? That can crack the tube. I would see how it goes as the crack looks like it is in the water jacket only.


---
**David Wakely** *April 24, 2015 17:29*

No, I think it could have been damaged in transit. I've since put thermal silicone around it and I am being sent a replacement tube from China free of charge. Result!


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/Ba1QuPxp38L) &mdash; content and formatting may not be reliable*
