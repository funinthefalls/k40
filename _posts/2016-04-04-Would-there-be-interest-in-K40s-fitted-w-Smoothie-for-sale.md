---
layout: post
title: "Would there be interest in K40's fitted w/ Smoothie for sale?"
date: April 04, 2016 20:10
category: "Discussion"
author: "Anderson Ta"
---
Would there be interest in K40's fitted w/ Smoothie for sale?





**"Anderson Ta"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2016 20:25*

You should search for posts tagged #smoothiebrainz, designed by **+Peter van der Walt**. Keep an eye out in the coming weeks as there might be a module/ shield for lasers. 

Let me know if you're interested, as I am doing a small run of the brainz boards right now if you want to get one in your hands for testing. You can also check my profile for my all-in-one variant being designed in eagle as we speak. 


---
**Anderson Ta** *April 04, 2016 20:27*

**+Ray Kholodovsky** shortly after posting, I reached out to Peter! Something will be available soon it seems. 


---
*Imported from [Google+](https://plus.google.com/+AndersonTa/posts/bHHD3FHoUmQ) &mdash; content and formatting may not be reliable*
