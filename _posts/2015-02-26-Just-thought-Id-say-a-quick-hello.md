---
layout: post
title: "Just thought I'd say a quick hello"
date: February 26, 2015 21:28
category: "Discussion"
author: "Darren Walsh"
---
Just thought I'd say a quick hello. I've had my K40 up and running for a week or so now, and I'm going to be doing lots of the usual upgrades (air assist, better cutting bed etc) as and when funds allow. I've learned a lot from the posts in here already - thank you!





**"Darren Walsh"**

---
---
**Bee 3D Gifts** *March 01, 2015 18:22*

Hello! We look forward to the better cutting bed !!


---
**Alessandro Milano** *March 03, 2015 15:45*

Enjoy !


---
*Imported from [Google+](https://plus.google.com/+DarrenWalsh/posts/hRez5bAkTZu) &mdash; content and formatting may not be reliable*
