---
layout: post
title: "As it could make sense to use the best tools for the right job, why not add a laser diode to your CO2 laser machine"
date: January 04, 2016 13:20
category: "Modification"
author: "Stephane Buisson"
---
As it could make sense to use the best tools for the right job, why not add a laser diode to your CO2 laser machine.

Cutting with the tube, Engraving with the diode.

it would save on your laser tube life expectancy, as engraving is slow.

make sense financially as laser diode is cheap (2w+ driver is about  30 £/40€/45$ on ebay), but a laser tube is not.

Could be easy to fit with a beam combiner (look my quick draft).

my only question is does the lens 10.6 μm would also work with the diode wavelength ? any expert around ?



![images/a07525393c500adb52b693eebbfc12b4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a07525393c500adb52b693eebbfc12b4.jpeg)
![images/51541a2e38f0fa70feafd3ff76c47bd5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/51541a2e38f0fa70feafd3ff76c47bd5.jpeg)

**"Stephane Buisson"**

---
---
**Stephane Buisson** *January 04, 2016 13:23*

Diode - driver - beam combiner



[http://www.ebay.co.uk/itm/2W-2000mW-Blue-Laser-Diode-TO-18-5-6mm-445nm-M140-/221979669115?hash=item33af04f27b:g:W3kAAOSw3KFWgOdW](http://www.ebay.co.uk/itm/2W-2000mW-Blue-Laser-Diode-TO-18-5-6mm-445nm-M140-/221979669115?hash=item33af04f27b:g:W3kAAOSw3KFWgOdW)



[http://www.ebay.co.uk/itm/12V-TTL-1W-2W-3W-445nm-450nm-Laser-Diode-LD-Driver-Power-Supply-Stage-Light-/181743154743?hash=item2a50bc7637:g:PI4AAOSwhwdVUcql](http://www.ebay.co.uk/itm/12V-TTL-1W-2W-3W-445nm-450nm-Laser-Diode-LD-Driver-Power-Supply-Stage-Light-/181743154743?hash=item2a50bc7637:g:PI4AAOSwhwdVUcql)



[http://www.ebay.co.uk/itm/IIVI-ZnSe-Beam-Combiner-Optic-Er-CO2-Laser-Red-Aiming-Beam-CNC-Engraver-Cutter-/221676806978?hash=item339cf7a342:g:9bsAAOSwRLZUF93s](http://www.ebay.co.uk/itm/IIVI-ZnSe-Beam-Combiner-Optic-Er-CO2-Laser-Red-Aiming-Beam-CNC-Engraver-Cutter-/221676806978?hash=item339cf7a342:g:9bsAAOSwRLZUF93s)


---
**Jim Hatch** *January 04, 2016 15:06*

How many hours do people usually get out of a laser tube? It looks like a replacement is in the neighborhood of $150 delivered (some are more & some less on ebay but it seems like there are a lot at the $140-150 price point). The 3 items needed are 75 pounds or $110, plus you'd need a new head or some mounting part to get the diode on the existing head...seems like they're a wash costwise, ramps up the complexity and would need some software changes (or use the diode 100% of the time and just turn off the laser power for engraving). Would it make economic sense?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 15:35*

Interesting idea.


---
**Jim Hatch** *January 04, 2016 15:51*

**+Stephane Buisson**​ - the combiner won't work with the diode you spec'ed as the diode is a 445nm blue diode and the combiner is 10,600/675. We'd need to find another diode or combiner pair. And another maybe another focusing lens.


---
**Stephane Buisson** *January 04, 2016 16:04*

**+Jim Hatch** it's more a proof of concept idea, just to open the debate. choice of parts had hardly begin.

Maybe China will be first to react ... as they improved the k40 offer already with our suggested mods.


---
**Jim Hatch** *January 04, 2016 16:07*

It's a good idea - just have to see if the technology exists. I'll look for a matching laser diode and combiner. I can 3D print the new mounting head. Need to give some thought to the cooling of the diode.


---
**ChiRag Chaudhari** *January 04, 2016 16:34*

Pretty neat little concept, I like it. I may not use combiner but may use the diode assembly along side. 


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/fX4C7eu5y2G) &mdash; content and formatting may not be reliable*
