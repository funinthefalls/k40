---
layout: post
title: "Christmas came early this year...i bought a 100 watt yag laser with galvo head...be here next monday"
date: December 14, 2016 23:35
category: "Discussion"
author: "Scott Thorne"
---
Christmas came early this year...i bought a 100 watt yag laser with galvo head...be here next monday😆





**"Scott Thorne"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2016 23:38*

Does it need a new board? :)


---
**Alex Krause** *December 15, 2016 00:06*

Gonna start working on Metal?


---
**Ariel Yahni (UniKpty)** *December 15, 2016 00:09*

You always come with a punch


---
**Joe Alexander** *December 15, 2016 06:41*

Feeling the Christmas Spirit! no, wait...that's just me going green with envy :)


---
**greg greene** *December 15, 2016 13:43*

So your available to make all our new cases then - right?


---
**Scott Thorne** *December 15, 2016 21:39*

**+Ray Kholodovsky**....yup...I'm going to need a new board for this...when it gets here I'll send you the pics and setup.


---
**Scott Thorne** *December 15, 2016 21:40*

**+Alex Krause**...you bet...time to multi-task...lol


---
**Ray Kholodovsky (Cohesion3D)** *December 15, 2016 21:40*

**+Scott Thorne** you know where to find me. 

(And if you don't: [cohesion3d.com](http://cohesion3d.com))

:)[cohesion3d.com - Cohesion3D: Powerful Motion Control](http://cohesion3d.com)


---
**Scott Thorne** *December 15, 2016 21:41*

**+Ariel Yahni**...i was very lucky with this setup...found it by mistake on ebay for 440.00 with free shipping...lol


---
**Ariel Yahni (UniKpty)** *December 15, 2016 21:43*

You got to be kidding me are does four hundred American made dollars? 


---
**Alex Krause** *December 15, 2016 21:48*

**+Scott Thorne**​ can you post a link to the seller?


---
**greg greene** *December 15, 2016 22:44*

Yes - please do !!!!


---
**Scott Thorne** *December 15, 2016 22:46*

**+Alex Krause**...it won't let me link it because it's not available anymore...the seller is prism electronics

![images/64db2e5aa2728ee82f6ae4eaac47d9e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/64db2e5aa2728ee82f6ae4eaac47d9e5.jpeg)


---
**Scott Thorne** *December 15, 2016 22:46*

**+Ariel Yahni**...435.00...but yeah u.s buckaroos


---
**greg greene** *December 15, 2016 22:48*

Looks like they're an 'Asset recovery' place - likely a one of deal


---
**Scott Thorne** *December 15, 2016 22:48*

**+Ray Kholodovsky**..it should be here by saturday so I'll give you a shout when i see what it's going to come with and what interface it has...probably rs232. 


---
**Scott Thorne** *December 15, 2016 22:56*

**+greg greene**..you are right...a toshiba 100 watt yag laser usually start at around 8000.00


---
**greg greene** *December 15, 2016 22:58*

Hopefully it all works if not I'm sure he'll have no problem selling it !


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/hYqKpW1D3TR) &mdash; content and formatting may not be reliable*
