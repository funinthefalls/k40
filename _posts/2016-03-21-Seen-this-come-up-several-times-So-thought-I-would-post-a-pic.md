---
layout: post
title: "Seen this come up several times. So thought I would post a pic"
date: March 21, 2016 00:16
category: "Discussion"
author: "Brian Bland"
---
Seen this come up several times.  So thought I would post a pic.  The threaded part screws out of both pieces.

![images/2cbf1c736dea0d96b9a6cf7bd9e853d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2cbf1c736dea0d96b9a6cf7bd9e853d4.jpeg)



**"Brian Bland"**

---
---
**Anthony Bolgar** *March 21, 2016 02:37*

The air inlet is also threaded into the cone, check once in a while to make sure it is not going to fall out of the cone


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/hAiXZknzzhx) &mdash; content and formatting may not be reliable*
