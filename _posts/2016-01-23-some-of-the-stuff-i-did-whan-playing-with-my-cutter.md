---
layout: post
title: "some of the stuff i did whan playing with my cutter"
date: January 23, 2016 08:52
category: "Object produced with laser"
author: "beny fits"
---
some of the stuff i did whan playing with my cutter



![images/7aa2e4f35963c3699f64d04360bd3a13.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7aa2e4f35963c3699f64d04360bd3a13.jpeg)
![images/3ce5f580f40556fb67977d3c2d017314.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ce5f580f40556fb67977d3c2d017314.jpeg)

**"beny fits"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 23, 2016 16:10*

Nice work. I like the dinosaur the most.


---
**Joseph Midjette Sr** *January 24, 2016 01:53*

Do you have a link for the dinosaur file? I would like to make that for my Grandchildren


---
**beny fits** *January 26, 2016 08:12*

**+Joseph Midjette Sr** I'll see what I can do when I get home 


---
**beny fits** *January 27, 2016 02:53*

[http://www.filedropper.com/dino1_1](http://www.filedropper.com/dino1_1)


---
**beny fits** *January 27, 2016 02:54*

[http://www.filedropper.com/dino2_1](http://www.filedropper.com/dino2_1)


---
**beny fits** *January 27, 2016 02:55*

[http://www.filedropper.com/dino3_2](http://www.filedropper.com/dino3_2)


---
**beny fits** *January 27, 2016 02:56*

**+Joseph Midjette Sr** i have put the 3 files you need for the dino 


---
**I Laser** *January 29, 2016 09:46*

Thanks for sharing, going to make one for my nephew who's dinosaur obsessed! :)


---
*Imported from [Google+](https://plus.google.com/116814442468846937788/posts/4Kquz5fSpSJ) &mdash; content and formatting may not be reliable*
