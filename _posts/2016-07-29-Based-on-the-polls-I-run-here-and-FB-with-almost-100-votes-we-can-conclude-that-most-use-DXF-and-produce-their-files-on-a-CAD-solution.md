---
layout: post
title: "Based on the polls I run here and FB, with almost 100 votes, we can conclude that most use DXF and produce their files on a CAD solution"
date: July 29, 2016 18:21
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Based on the polls I run here and FB, with almost 100 votes, we can conclude that most use DXF and produce their files on a CAD solution. But if we consider that CDR, SVG, AI as a graphic design solution, then they are the most. Now the questions would be if they where given the option ( because they are obligated to use CDR)  would the use/learn a CAD solutions? 





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *July 29, 2016 18:22*

Poll, results [https://plus.google.com/+ArielYahni/posts/gUcihTHZXRz](https://plus.google.com/+ArielYahni/posts/gUcihTHZXRz)


---
**Bruce Garoutte** *July 29, 2016 18:39*

I do all of my vector creating in CAD and import to Corel. There I may add raster images for engraving, or make minor adjustment changes. Once finished, I then save the final product as CDR. In the future, if I want to re-use the drawing, it is easy to open the CDR, and go from there.


---
**Jim Hatch** *July 29, 2016 18:47*

If you don't have a background in any other application, then I would suggest a serious look at Corel (the new X8 especially). Corel was designed and developed as a 2D design tool first & foremost.



I also use AI and there isn't anything that can do Corel can't. AI's design roots came out of Photoshop so it's orientation is a bit different with more of an image and bitmap handling & modification approach.



Inkscape is more of a 3D base orientation from its design and just the ticket for a CNC device (and yes I know lasers are CNC machines).



They all do 2D design well but unless you're already a devotee of one of the others (or even tools like Rhino, 123Make, etc) Corel is probably all you need. The Home & Student version won't work with the K40's add-in to drive the laser. The Educational version if you qualify is okay. For LaserWeb any version is good.


---
**Ariel Yahni (UniKpty)** *July 29, 2016 18:53*

Agree with you **+Jim Hatch**​. Also i would say it's all about what you allready are comfortable with but most solution of the same genre will provide you the same options. 


---
**Don Kleinschnitz Jr.** *July 31, 2016 13:36*

It's SketchUp and Inkscape for me....


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/gGzbRaUaFDW) &mdash; content and formatting may not be reliable*
