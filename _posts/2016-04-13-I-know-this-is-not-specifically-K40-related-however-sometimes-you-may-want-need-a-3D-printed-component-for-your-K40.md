---
layout: post
title: "I know this is not specifically K40 related, however sometimes you may want/need a 3D printed component for your K40"
date: April 13, 2016 17:24
category: "External links&#x3a; Blog, forum, etc"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
I know this is not specifically K40 related, however sometimes you may want/need a 3D printed component for your K40.



I just found this 3D printing service where you can find someone capable of 3D printing for you in your nearby region. Looks like it is spread throughout a lot of the world. I think it is individual's who list their own printers here to make some $ on the side.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Stephane Buisson** *April 13, 2016 18:15*

another way is to try to find out a makerspace hackerspace/fablab near you


---
**Thor Johnson** *April 13, 2016 18:50*

That's what I do ([https://www.3dhubs.com/atlanta/hubs/thor](https://www.3dhubs.com/atlanta/hubs/thor)); I got my Robo3D because I always wanted one, and it's idle ~60% of the time, so I try to print things for other people.



The last year or so, print orders have really slowed down as the number of printers in the wild has gone up.  But I've made the following pieces for my K40 -

  Air Assist

  Laser Pointer ([http://www.thingiverse.com/thing:1002341](http://www.thingiverse.com/thing:1002341))

Up next: Adjustable Z-Table...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 19:08*

**+Stephane Buisson** Unfortunately no real makerspace or fablab or the likes in my local region. Although, I found one that just started up. So I thought this 3dhubs is a handy alternative for people that don't have access to fablab/makerspace/3d printer if they need stuff made for their K40.



**+Thor Johnson** If shipping was probably not going to be a killer, I'd prefer to support someone in this group. I find that locally I can get done what I want (about 10km away) & can pickup. So no shipping on that, but depends on their prices. May be high anyway (everything in Australia is excessively priced).


---
**Thor Johnson** *April 13, 2016 19:27*

Well... shipping inside the US is easy and not usually a deal breaker (PLA/ABS is light/cheap) [I'll put the laser pointer in a small box for $15, $20 if you want me to put the laser in it -- maybe less if I can find the right box].  Outside the US... ick (iirc $20+, so $40 :( ).  If I had an answer for that.... (my rates are cheap because my day job is building air/water sensors, so the printer / laser / cnc / etc only real job is to support themselves and give me beer money -- not support me or a real shop and employees).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 19:32*

**+Thor Johnson** Yeah, sounds reasonable. If it turns out that the prices here are unreasonable, I will check with you for a quote on what it would cost. I just have a few small parts (40mm x 40mm x 10mm for the current idea) that I want to print, so not very large. At 10mm thick, AusPost would allow it to post as a document (<20mm), but maybe not due to the rigidity of it. Not sure about where you're from how your shipping works. I'll be in contact if I need :)


---
**Thor Johnson** *April 13, 2016 19:39*

Another place to look (not quite as fancy) is MakeXYZ, eg: [https://www.makexyz.com/printer/thormj](https://www.makexyz.com/printer/thormj)

I think MakeXYZ was there first, but 3DHubs has put a lot more into it (I can make decent pricing models, they have a newsletter for trends, and they created the "Marvin" character and use that as fuel for their marketing machine).


---
**HalfNormal** *April 13, 2016 21:33*

If you can afford it, this is the printer I have and it has been great. They are also posting a discount to AU. They have plenty of support on YouTube and Thingiverse.

[https://www.3dprintersonlinestore.com/full-acrylic-reprap-prusa-i3-kit](https://www.3dprintersonlinestore.com/full-acrylic-reprap-prusa-i3-kit)

Believe me, if you have it, you will use it!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:01*

**+Thor Johnson** Thanks again Thor. I will check that out too.



**+HalfNormal** Currently can't afford it as I am wanting to do the smoothie upgrade & have limited funds spare after bills, so have to budget, but I will keep that model in mind for when I can afford one :) I definitely will use it if I have one. I can't help but use all the tools & gadgets I have.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/ijAtoHRia3p) &mdash; content and formatting may not be reliable*
