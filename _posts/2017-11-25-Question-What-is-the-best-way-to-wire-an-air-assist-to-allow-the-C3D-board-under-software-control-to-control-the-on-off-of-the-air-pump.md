---
layout: post
title: "Question: What is the best way to wire an air-assist to allow the C3D board, under software control, to control the on/off of the air pump?"
date: November 25, 2017 02:28
category: "Air Assist"
author: "Alex Raguini"
---
Question: What is the best way to wire an air-assist to allow the C3D board, under software control, to control the on/off of the air pump?







**"Alex Raguini"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 25, 2017 04:06*

If it's an AC powered pump then I'd recommend a power tail, or an SSR. 

The power tail has a relay inside, and you need to use a flyback diode on the board side. 


---
**Don Kleinschnitz Jr.** *November 25, 2017 14:39*

As an alternate. We recently designed a driver for an air solonoid for a user that wanted to switch the air instead of the compressor. Parts, design and config are logged here:



[plus.google.com - Can anyone here recommend a solenoid for air assist? How's you go about it an...](https://plus.google.com/117314700387764776689/posts/jE3WeGwaaEM)


---
**Alex Raguini** *November 25, 2017 15:58*

Thanks.  Ray, I'm not exactly sure what an SSR is.



I'm preparing to add air assist to my system.  I have both a large tank compressor available and a small air pump as well.  Now, I'm trying to decide on the best way though I'm leaning away from my large compressor as I would like to be somewhat portable.




---
**Don Kleinschnitz Jr.** *November 25, 2017 18:02*

Here is an example Solid State Relay (SSR). It is activated by a DC voltage and switches on side of an AC line. If you decide you want to go this direction we can help you hook it up.

There are cheaper ones than this if you look around on Amazon.



[amazon.com - Robot Check](https://www.amazon.com/Lerway-Solid-State-SSR-40DA-24-380V/dp/B00HV974KC/ref=pd_bxgy_469_2?_encoding=UTF8&psc=1&refRID=8X5GTQNK4RCQ3NF9Z4Z7)


---
**Alex Raguini** *November 25, 2017 18:14*

I think that is for the future.  Right now, I'm considering this:  [amazon.com - Robot Check](https://www.amazon.com/POWERSWITCHTAIL-COM-PowerSwitch-Tail-II/dp/B00B888VHM/ref=sr_1_3?ie=UTF8&qid=1511633625&sr=8-3&keywords=power+tail)



I probably still need some help with the trigger connections.




---
**Terry Evans** *November 26, 2017 14:58*

First, I would recommend setting up your K40 to have a second 24 volt power supply to take the load off of the cheap one that comes with it.  Then you can add things like solid state relays, lighting, etc without worry.  Here is the SSR's that I'm going to use.  You wire 24v power through your switch and then to the input side of the SSR, the output of the SSR controls the 110vac to your air assist pump.  [amazon.com - Robot Check](https://www.amazon.com/MYSWEETY-SSR-40DA-Single-Semi-Conductor-24-380V/dp/B073B4R4LS/ref=pd_sim_328_2?_encoding=UTF8&psc=1&refRID=7YSRVB03V7TX59HH0KYY)


---
**Don Kleinschnitz Jr.** *November 27, 2017 04:06*

**+Alex Raguini** you probably can drive this the same way **+Chuck Comito**​ did his solonoid in the post I referenced above.


---
**Chuck Comito** *November 27, 2017 04:14*

**+Alex Raguini**​, although my setup isn't complete it does work. I'm still working on cleaning up the wiring and such but pictures are soon to come. In the meantime the solenoid version that **+Don Kleinschnitz**​ mentioned is a perfect fit. I run a small pancake compressor and then just fire the solenoid at the start of the job and kill it at the end with m116 and m117. Let us know if you need help.  


---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/PT5Co6VSdw7) &mdash; content and formatting may not be reliable*
