---
layout: post
title: "Had these delivered this morning they lot bigger then expected was going put them in the case to push smoke back to the vent now thinking to turn them into extractor fan 1 powers out at 2300 rpm thinking both together and"
date: November 05, 2016 10:45
category: "Modification"
author: "Rob Turner"
---
Had these delivered this morning they lot bigger then expected was going put them in the case to push smoke back to the vent now thinking to turn them into extractor fan 1 powers out at 2300 rpm thinking both together and build case at back would they be better then the fan I have now 

![images/92442178b4709c9b3b726ce124ae3c72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/92442178b4709c9b3b726ce124ae3c72.jpeg)



**"Rob Turner"**

---
---
**Andy Shilling** *November 05, 2016 10:57*

I've just got my extractor too. I've gone for an in-line centrifugal bathroom extractor with flexible hose.  it will be interesting to see the difference between yours and mine. Please keep us posted.


---
**HP Persson** *November 05, 2016 11:35*

Two fans mounted on top of each other will give less static pressure than having them side-by-side. But the air flow may increase. Test it :)


---
**Rob Turner** *November 05, 2016 11:53*

I was going to do side by side 


---
**Jim Hatch** *November 05, 2016 13:51*

Unless you've also opened up the intake side of things (get more air into the box) all the exhaust mods after getting rid of the rectangular exhaust manifold will be of limited value. The stock K40 usually has a single 2" hole in the bottom for air intake to replace what you're sucking out of a 4" square hole. Open the lid an inch or two while using the stock exhaust and see how much better it is.


---
**K** *November 05, 2016 22:52*

Ditto on **+Jim Hatch**'s recommendation. Cracking the lid made a huge difference for me.


---
*Imported from [Google+](https://plus.google.com/114544098138525257938/posts/igBBbD628VW) &mdash; content and formatting may not be reliable*
