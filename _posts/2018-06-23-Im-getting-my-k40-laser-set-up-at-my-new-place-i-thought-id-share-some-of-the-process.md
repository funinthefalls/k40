---
layout: post
title: "Im getting my k40 laser set up at my new place, i thought id share some of the process..."
date: June 23, 2018 03:37
category: "Hardware and Laser settings"
author: "Andrew Sanders (pinsetter1991)"
---
Im getting my k40 laser set up at my new place, i thought id share some of the process...


{% include youtubePlayer.html id="2fw6cOa5JcA" %}
[https://youtu.be/2fw6cOa5JcA](https://youtu.be/2fw6cOa5JcA)





**"Andrew Sanders (pinsetter1991)"**

---
---
**Don Kleinschnitz Jr.** *June 23, 2018 11:46*

Welcome to K40...

If your machine does no have these I would suggest their addition:

1. INTERLOCKS on front and rear cover to make the unit safer

2. A water flow sensor that interlocks the laser power

3. An overtemp sensor interlock to protect the tube from overheating.



All these can be found in this community and my blog.


---
**Joe Alexander** *June 25, 2018 00:35*

where is the probe for your temp meter affixed? can you show a pic of that?


---
*Imported from [Google+](https://plus.google.com/101788445554224184336/posts/MKQScqjJ9gr) &mdash; content and formatting may not be reliable*
