---
layout: post
title: "you can see it in action here"
date: December 30, 2016 09:12
category: "Modification"
author: "Stephane Buisson"
---
you can see it in action here


{% include youtubePlayer.html id="nAqu-836F5E" %}
[https://www.youtube.com/watch?v=nAqu-836F5E](https://www.youtube.com/watch?v=nAqu-836F5E)





**"Stephane Buisson"**

---
---
**Stephane Buisson** *December 30, 2016 09:22*

Pay attention to not use more than 0.5Amp Nema stepper to stay in stock PSU limit. Motor to be plug in place of one of your axis.


---
**HalfNormal** *December 30, 2016 12:34*

Here is the link to the GrabCad files

[https://grabcad.com/library/laser-rotary-attachment-for-k40-1](https://grabcad.com/library/laser-rotary-attachment-for-k40-1)


---
**Bill Parker** *January 08, 2017 01:27*

Why has a link been put in the comments box for the grabcad files have they changed from the original files??


---
**Bill Parker** *January 08, 2017 01:37*

Hi Can you tell me how you work out the drive wheel diameter to the amount of steps per revolution of the motor I have searched how to work this out but can not find anything.




---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/heqSpEjbFsn) &mdash; content and formatting may not be reliable*
