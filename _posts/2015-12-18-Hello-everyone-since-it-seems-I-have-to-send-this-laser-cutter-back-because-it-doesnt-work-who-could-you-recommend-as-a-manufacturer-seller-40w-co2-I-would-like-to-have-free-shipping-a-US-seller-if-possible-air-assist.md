---
layout: post
title: "Hello everyone since it seems I have to send this laser cutter back because it doesn't work who could you recommend as a manufacturer / seller 40w co2 I would like to have free shipping a US seller if possible air assist"
date: December 18, 2015 00:58
category: "Discussion"
author: "Timothy \u201cMike\u201d McGuire"
---
Hello everyone since it seems I have to send this laser cutter back because it doesn't work who could you recommend as a manufacturer / seller 40w co2 I would like to have free shipping a US seller if possible  air assist would be nice  for about $500 any help would be appreciated since I didn't do very well the first time 

Thanks







**"Timothy \u201cMike\u201d McGuire"**

---
---
**Scott Marshall** *December 18, 2015 15:43*

I don't have the unit yet, but I just ordered one from a ebay seller known as 'Globalfreeshipping' for just under $400 delivered.

 They are out of Irvine Ca, and you'll see several sellers in that area with nearly identical listings. According to posts on the CNC forum, they are all the same company, and also sell 'Redsail' Laser and Vinyl cutters (and god knows what else). While not known for customer service (as few of the 'crate shifters' are), they are somewhat reliable, by the accounts I've read.



There's a company also on ebay called 'Saite' selling nice aluminum air assist heads for $35, and spare lens sets for $45ish. Lookout for the cheap 3d printed heads that look like blackened steel. Almost ordered one by accident.



I'm an industrial engineer with 40+ years in manufacturing machinery experience. Is there anything I may be able to talk you through. Many times, it's easier in the end to fix this stuff yourself, and I'll help you if I can.



Scott


---
**Anthony Bolgar** *December 18, 2015 16:24*

I got mine from the same place as Scott. No complaints, well packed for shipping.




---
**Scott Marshall** *December 18, 2015 16:25*

Makes me feel better anyway. Thanks Anthony!


---
**Bryon Miller** *December 21, 2015 23:59*

I just ordered one from the same seller on ebay.  I'm glad to hear they packed well.


---
*Imported from [Google+](https://plus.google.com/109629943464502534866/posts/JM3EdrnbkuF) &mdash; content and formatting may not be reliable*
