---
layout: post
title: "Where can i find a k40 middleman on europe?"
date: February 16, 2017 23:14
category: "Modification"
author: "Jorge Robles"
---
Where can i find a k40 middleman on europe?





**"Jorge Robles"**

---
---
**Don Kleinschnitz Jr.** *February 16, 2017 23:42*

No idea as I am in the US.



.......Here are some options:

Make your own using vector board, or one sided homemade PCB. My blog has the schematic and I think I have a layout for a simpler version.

Get a C3D board **+Ray Kholodovsky** its built in

Convert the ribbon cable to wire and mechanical end stops



What kind of conversion are you doing?




---
**Jorge Robles** *February 17, 2017 06:44*

Thanks, I got the the ffc connectors ([tme.eu - Transfer Multisort Elektronik S.L.U. - Componentes electrónicos](http://tme.eu)), but are 1.25 and have bad soldering skills 😁.




---
**Jorge Robles** *February 17, 2017 06:47*

Hahahah


---
**Jorge Robles** *February 17, 2017 06:49*

Electronics are my final frontier 😃


---
**Jorge Robles** *February 17, 2017 07:09*

Like this **+Peter van der Walt**​

![images/fe9e84e4d5314e769e32d2c2342bf162.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe9e84e4d5314e769e32d2c2342bf162.jpeg)


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/Es8s4u7SdqX) &mdash; content and formatting may not be reliable*
