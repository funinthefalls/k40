---
layout: post
title: "Hello All, I am tearing my hair out!"
date: May 21, 2018 10:09
category: "Discussion"
author: "Amanda Temple"
---
Hello All, I am tearing my hair out! My K40 laser head moves but no laser and yet if I press the 'test' button I get the image I need....any suggestions would be greatly appreciated!





**"Amanda Temple"**

---
---
**Kelly Burns** *May 21, 2018 11:54*

The wires on the laser enable and test switches might be reversed.  Try swapping them on power supply


---
**Don Kleinschnitz Jr.** *May 21, 2018 12:54*

What **+Kelly Burns** said but if that is not it:



Tell us something about your machine:

1. Stock or converted?

2. What test button; on the panel or down on the LPS

3. New machine or has been running ok till now?

4. Are you pushing the test button while running a job?


---
**Amanda Temple** *May 21, 2018 13:20*

Thank you! After weeks of trying literally everything, a simple swap of wires worked. Now I can have a play with it!


---
**Don Kleinschnitz Jr.** *May 21, 2018 13:29*

**+Amanda Temple** wow was this machine always wired wrong?




---
**Kelly Burns** *May 21, 2018 13:37*

Glad that was it.  Happy lasering.


---
**Amanda Temple** *May 22, 2018 13:48*

**+Don Kleinschnitz** To be honest, I am used to working with LG900 G Weike laser engraving machine. I bought 1 of these Chinese table top lasers to continue lasering but as a hobby. I purchased it second hand. The test button was jammed, so I unjameed it and stopped lasering designs. Fired when I pressed the test button. Did not think that changing around the 2 wires would work! I guess I am lucky after weeks of pulling out circuit boards, wires and all sorts!




---
**Amanda Temple** *May 22, 2018 13:50*

**+Don Kleinschnitz** 1. Stock or converted? - second hand stock.

2. What test button; on the panel or down on the LPS - test button on panel.

3. New machine or has been running ok till now? - running perfectly (with the test button jammed, ex-owners attempt)

4. Are you pushing the test button while running a job? - laser fired when test button was pressed on its own.



Hope this helps?

REPLY






---
**Don Kleinschnitz Jr.** *May 22, 2018 13:56*

**+Amanda Temple** your all good now right?


---
*Imported from [Google+](https://plus.google.com/103810405525985670318/posts/aQof6f9KBd8) &mdash; content and formatting may not be reliable*
