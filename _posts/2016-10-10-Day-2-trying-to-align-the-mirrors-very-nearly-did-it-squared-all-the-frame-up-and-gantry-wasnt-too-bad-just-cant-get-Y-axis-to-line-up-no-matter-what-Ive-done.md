---
layout: post
title: "Day 2 trying to align the mirrors, very nearly did it, squared all the frame up and gantry (wasnt too bad) just cant get Y axis to line up no matter what Ive done"
date: October 10, 2016 20:28
category: "Hardware and Laser settings"
author: "J DS"
---
Day 2 trying to align the mirrors, very nearly did it, squared all the frame up and gantry (wasnt too bad) just cant get Y axis to line up no matter what Ive done







**"J DS"**

---
---
**Scott Marshall** *October 10, 2016 20:56*

You know it's an uphill or down hill problem. That's something. If you only adjust the top 2 screws equally, it SHOULD come in. Make sure you're not bottomed out and keep the faith, it will happen.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 10, 2016 21:06*

Is it still out of alignment in the horizontal for the Y-axis like your previous post? Or the vertical?


---
**J DS** *October 10, 2016 21:57*

![images/39cc24565204bf4a8a94121bb4bf4844.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/39cc24565204bf4a8a94121bb4bf4844.gif)


---
**Phillip Conroy** *October 11, 2016 07:45*

Slow down,find the alignment guild that is posted here someware that   surgets u only move the screws in pairs,ie top 2 ,side 2,-and move thescrews  1/8 of a turn max -mark the screw heads with texta so you have a visable guild to how far you are turning them.if after 4 hour,then pack one end of laser tube with elecl tape and see if that helps,it took me 6months to get aligned,i had only half of cutting area cutting good so didnt need to be perfect.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 11, 2016 11:49*

**+J DS** The mirror closest to the laser tube tip is the one you're going to have to spend time adjusting (at least for this step). What seems to be happening is that it is hitting decently on the Y-axis mirror at the close position & then the beam is moving to the right (when looking towards front of machine) as you move the Y-axis closer to the front of machine. So, the 2 screws on the right of the 1st mirror (at tube tip) will need extending out somewhat.


---
**Scott Marshall** *October 15, 2016 08:25*

**+Phillip Conroy** 

[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



The wombat is good, the wombat is wise....

[floatingwombat.me.uk - www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
*Imported from [Google+](https://plus.google.com/102322879087562424255/posts/fdcS2nVtPyD) &mdash; content and formatting may not be reliable*
