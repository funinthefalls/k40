---
layout: post
title: "I need some help here. I was doing some test on a bottle and suddenly the laser start to act funny, not engraving continuously, then is stopped fireing"
date: November 18, 2018 08:27
category: "Original software and hardware issues"
author: "Vlad Isari"
---
I need some help here. I was doing some test on a bottle and suddenly the laser start to act funny, not engraving continuously, then is stopped fireing.

See the attached images and videos.

The power supply looks alright inside but the power led flashes. 

The tube is about 3-4 months old but i didn't used it to much.



PSU, TUBE? ..I will change the water today to take one off the list




**Video content missing for image https://lh3.googleusercontent.com/-xXws_iwuE-I/W_EiXti3KHI/AAAAAAAANRw/BxKzvIdkafAk-XXkT95P9nzZY7wdBsCeQCJoC/s0/gplus3949537268546341387.mp4**
![images/e1b47a51cef1d93f49024dea3c185db3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1b47a51cef1d93f49024dea3c185db3.jpeg)
![images/14b6bbc275aabaa207597bcd01a6d203.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14b6bbc275aabaa207597bcd01a6d203.jpeg)
![images/1608eaf62f4e703d800e4b852bc0bb00.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1608eaf62f4e703d800e4b852bc0bb00.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-CnAjrDxBylI/W_EiXr8MT7I/AAAAAAAANR0/9K0XONKYgJw9IeNf_IrQkTENKqGBwT5HQCJoC/s0/gplus8523513962134470176.mp4**
![images/f53509c3be011e6237bcdad6be874a9a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f53509c3be011e6237bcdad6be874a9a.jpeg)

**"Vlad Isari"**

---
---
**D Myers** *November 18, 2018 18:03*

Original tube?  I’ve already replaced the power supply and tube in mine. The OEM parts are pour quality sadly. Do you smell ozone or hear any high pitch sound. Hold the door up a bit to keep it dark in there and test fire the laser. Look to see if you see any blue arcing. I had a waterfall of blue from the high voltage transformer to the power supply case. I replaced both of mine with the cloud ray brand, trued up the tube alignment, realigned all the mirrors and noticed a boost in output power with the ability to cut through plywood with half as many passes. In all this you need to be careful with reconnecting everything since you are dealing with extremely high voltage. 


---
**D Myers** *November 18, 2018 18:29*

Your green LED is not blinking as much as it is dimming. Looks like the output of the supply is shorted and pulling down the supply when you try to test fire the laser. More like a bad supply first. 


---
**Vlad Isari** *November 19, 2018 12:06*

Thank you Myers. The tube looks fine, no arching or colour dimming. I have ordered a new power supply. I will try this one first.


---
**D Myers** *November 19, 2018 16:15*

**+Vlad Isari**  If you ordered the cloud ray supply you’ll find that to be of much better quality. Connectors may be different and you will have to figure out the minor pin out differences. The trick will be how to connect the hot wire from the tube to the supply or supply to the tube. The ground side is less of an issue.  Are you setup to do this properly?  Be careful that the hot side is not charged when messing around with the supplies and testing; it can bite you bad. Let me know if you need any help thinking through it. 


---
**Vlad Isari** *November 19, 2018 17:01*

**+D Myers** I already ordered a regular PSU from ebay, i couldn't find anything to deliver in less than 3-4 days. Sometime is so hard to get parts in the UK or EU. Claudray will take about 1week or more.


---
**Don Kleinschnitz Jr.** *November 20, 2018 16:35*

The power LED actually is connected to the 5v output of the LPS. I doubt is should fade like it is. You may have a bad LPS or you may have to large a load on the 5V. The 5V can handle 1A. I would check its load.


---
**Vlad Isari** *November 20, 2018 18:57*

**+Don Kleinschnitz Jr.** Hi Don, I've went through you work when I've done the config with the added cohesion board. Could you tell me more precise what I should look for or what I should change? I am not good at all with electrics. Thanks


---
**Don Kleinschnitz Jr.** *November 20, 2018 19:33*

**+Vlad Isari** first: what do you have connected to the 5V line on the lps?




---
**Vlad Isari** *November 20, 2018 19:59*

By LPS you mean the 24Vpower supply? I have a microstep driver connected to run the rotary.


---
**Don Kleinschnitz Jr.** *November 21, 2018 02:06*

The Laser Power Supply has 3 outputs: 20,000v, 5v and 24v. How much load (current) are you drawing from the ,5v and 24v. They each can handle one amp. 


---
*Imported from [Google+](https://plus.google.com/114019829123284097632/posts/H4bCbnyVRUJ) &mdash; content and formatting may not be reliable*
