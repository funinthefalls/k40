---
layout: post
title: "For those looking to mark /etch stainless steel here is a short video on the process I use"
date: November 15, 2018 14:55
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
For those looking to mark /etch stainless steel here is a short video on the process I use. It's not the same as cermark but can be an alternative .




{% include youtubePlayer.html id="AYl_E-4m60E" %}
[https://youtu.be/AYl_E-4m60E](https://youtu.be/AYl_E-4m60E)





**"Ariel Yahni (UniKpty)"**

---
---
**James Rivera** *November 15, 2018 18:08*

This?[amazon.com - CRC 03084 11oz Dry Moly Lubricant Aerosol Spray: Power Tool Lubricants: Amazon.com: Industrial & Scientific](https://www.amazon.com/03084-11oz-Lubricant-Aerosol-Spray/dp/B07FXSGXZQ)


---
**Ariel Yahni (UniKpty)** *November 15, 2018 18:26*

**+James Rivera** nope, this one [https://amzn.to/2K5s5CG](https://amzn.to/2K5s5CG)


---
**Nigel Conroy** *December 04, 2018 00:46*

Why do you let it sit for a day?


---
**Ariel Yahni (UniKpty)** *December 04, 2018 01:15*

**+Nigel Conroy** nothing special on the time, it just feels very dry at that point


---
**Nigel Conroy** *December 04, 2018 01:20*

Sounds good. I've done the same but not let it sit overnight![images/0475b9dd8c00af2a5c42ab7c8f295a75.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0475b9dd8c00af2a5c42ab7c8f295a75.jpeg)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/X8UhSXx9DHV) &mdash; content and formatting may not be reliable*
