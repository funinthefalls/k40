---
layout: post
title: "Don Kleinschnitz are you still looking for power supply variants?"
date: February 25, 2017 15:14
category: "Discussion"
author: "Anthony Bolgar"
---
**+Don Kleinschnitz** are you still looking for power supply variants? I have a 220V open PSU from a redsail LE400 I converted. It is yours if you want it.





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *February 25, 2017 15:23*

Definitely want it. Where are you located?


---
**Anthony Bolgar** *February 25, 2017 15:24*

I'm in the Honeymoon capital of the world. (Niagara Falls, Ontario Canada)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/czYgneScqHk) &mdash; content and formatting may not be reliable*
