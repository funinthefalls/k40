---
layout: post
title: "Hey guys wanted to ask a question on co2 laser sourcing, and if it could be possible to attach a Co2 laser to my OX CNC?"
date: September 11, 2016 23:01
category: "Material suppliers"
author: "Matt Herrera"
---
Hey guys wanted to ask a question on co2 laser sourcing, and if it could be possible to attach a Co2 laser to my OX CNC? Would it be cheaper to just buy a K40 and dismantle it for parts or is there a good cheap source for just the tube? Could a 40W co2 laser be controlled by a TinyG with PWM signal? I know there are multiple questions here but appreciate any info anyone can provide. 

I have a JTech photonics laser hooked up to my OX but it's not powerful enough for some applications and is only max 2.8w. I figure 40w should handle my needs with ease.  





**"Matt Herrera"**

---
---
**Matt Herrera** *September 11, 2016 23:46*

Or maybe it would be easier to just build a freeburn and pick up an 80w laser from SDZ, they seem to be pretty affordable. 


---
**Brandon Satterfield** *September 12, 2016 13:31*

**+Matt Herrera** it may be possible. Check out the length of a 40w tube in comparison to your X axis. If you added another brace in the rear you might be able to support this. The main thing would be the lack of protection. The K40 would be a better solution as it comes enclosed, freeburn would also be a great selection. 


---
**Matt Herrera** *September 12, 2016 18:15*

Thanks **+Brandon Satterfield** very good point on the enclosure and safety. I like my eye balls and plan to keep them. I think I'm leaning more towards freeburn. It has the larger footprint, I can enclose it, and I should be able to add an A axis to rotate the bottles I'm engraving. The JTech 2.8 watt struggles to effectively burn off the paint coating on the bottles. It works but I end up spending allot of time cleaning up the area and removing leftover paint with acetone. If I build from scratch I can purchase an 80w laser and have some serious cutting power. I also learned that you can make your own laser mirrors with old hard drive disks. The aluminum alloy disks should be easy to cut on the OX. ![images/235ef90ea1b962f0c63602159d790be9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/235ef90ea1b962f0c63602159d790be9.jpeg)


---
**Nigel Conroy** *September 14, 2016 14:35*

There is a very good video on youtube about making your own mirrors from copper. He also made them from the harddrive disks and found the power loss was a bit on the high side. Here is one video but there is another on the hard drive mirrors.


{% include youtubePlayer.html id="ELeqP3tHlp0" %}
[youtube.com - RDWorks Learning Lab 46 Copper Mirrors+HQ Lens=20% More Power](https://youtu.be/ELeqP3tHlp0)




---
**Matt Herrera** *September 15, 2016 00:03*

Thanks Nigel that was an interesting video. I may have missed it but I wonder where he got those copper rounds from? Darn I was hoping the hard drive would have been a good solution but I knew that may be far fetched. 


---
*Imported from [Google+](https://plus.google.com/103196351153669305992/posts/BBkNvCEyAiH) &mdash; content and formatting may not be reliable*
