---
layout: post
title: "How come when using corellaser, when I do a cut, each cut happens twice?"
date: October 26, 2016 20:24
category: "Hardware and Laser settings"
author: "Bob Damato"
---
How come when using  corellaser, when I do a cut, each cut happens  twice? If I were to cut a square, it goes down each side twice?





**"Bob Damato"**

---
---
**Alex Krause** *October 26, 2016 20:26*

Change your line width to .001


---
**greg greene** *October 26, 2016 20:33*

happens with laserweb too if line width is not .001


---
**Bob Damato** *October 26, 2016 21:03*

OK I know Im a newbie to this kind of thing. But for the life of me I cant find a place to set line width!




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2016 21:16*

**+greg greene** There was a bit of a discussion about that recently for LaserWeb whereby I think the line width was only making that happen when the SVG was less than version SVG1.1. Although, you may be referring to a different file format.



**+Bob Damato** You may have to enable the correct panel in Corel Draw first to allow you to access the line width. Or, you can customise a toolbar by adding a line-width button. 

Here's a screenshot to maybe help: [https://drive.google.com/file/d/0Bzi2h1k_udXwZ3h0UmhkUW1vUnc/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZ3h0UmhkUW1vUnc/view?usp=sharing)

[drive.google.com - CorelDraw - Line Width.png - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwZ3h0UmhkUW1vUnc/view?usp=sharing)


---
**Bob Damato** *October 26, 2016 21:26*

**+Yuusuf Sallahuddin** Thank you! yes I definitely dont have that! Lets see if I can get that up there, thank you!




---
**greg greene** *October 26, 2016 21:41*

Your correct Peter - I was thinking of LaserDRW - but put down LaserWEB which I hope to move to soon !


---
**Jim Hatch** *October 27, 2016 01:19*

Look for "stroke" in the Help


---
**Kelly S** *November 16, 2016 19:45*

I have this same issue, only it'll go 5 or 6 times, I export in Fusion 360, and I am not sure if I can set a line width in there.   Is there a easy way to edit a file afterwards and adjust it?  




---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/Ak7EEjSjhr4) &mdash; content and formatting may not be reliable*
