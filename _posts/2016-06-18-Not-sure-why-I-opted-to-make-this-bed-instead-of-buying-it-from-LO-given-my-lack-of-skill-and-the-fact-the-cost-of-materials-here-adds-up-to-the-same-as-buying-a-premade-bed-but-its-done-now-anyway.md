---
layout: post
title: "Not sure why I opted to make this bed instead of buying it from LO given my lack of skill and the fact the cost of materials here adds up to the same as buying a premade bed but it's done now anyway..."
date: June 18, 2016 15:26
category: "Modification"
author: "Pippins McGee"
---
Not sure why I opted to make this bed instead of buying it from LO given my lack of skill and the fact the cost of materials here adds up to the same as buying a premade bed but it's done now anyway...

Also not convinced about using lab jack as a good means of adjusting bed but we'll see..

Smoke duct removed (PITA...have to unbolt and remove entire rails to get it out.......), and led strips installed also - just need more powerful blower now.



one other thing. not a fan of the expandable aluminium honeycomb mesh. so fragile - rips so easily if you drop something on it or press on it etc. (downside to having a thin contact area I suppose) & costs a fortune from the UK



![images/97f1f434a99719c80035c21ce5475ff0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97f1f434a99719c80035c21ce5475ff0.jpeg)
![images/c0545657bc40ae1d59a98326624d25ee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c0545657bc40ae1d59a98326624d25ee.jpeg)

**"Pippins McGee"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 18, 2016 15:49*

I made a similar bed using fishing leader wire & wove it every inch vertically & horizontally with an under/over pattern. Took forever, but the wire is extremely strong & thin. Similar principle with the low contact area but the thing I find problematic is how to do I hold my materials flat (e.g. leather & fabric which needs to be held taut). Works fine for wood/solid objects. Wire cost about $5 for 30 foot of it. About $8 worth of aluminium L for the frame.


---
**Corey Renner** *June 18, 2016 15:51*

You might have had better luck with the honeycomb if you had gotten some with smaller cells.  Yours looks a bit delicate compared to what is usually seen.  IE: Too much air, not enough metal.


---
**Tony Sobczak** *June 18, 2016 18:35*

How do you adjust the height.  Where's tha access to the adjuster??


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/3JPhMQjLLtC) &mdash; content and formatting may not be reliable*
