---
layout: post
title: "I am stumped trying to align this k40"
date: June 13, 2017 19:51
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
I am stumped trying to align this k40. I get good and aligned burn marks from the fixed mirror to the y axis mirror and from the y axis mirror to the laser head mirror. But there's nothing going from the focus lens to the bed??? If I put masking tape directly on the bottom of the laser head, I see a small burn mark...kind of skewed. But when I put the tape directly on the bed, there are no burn marks.



Any ideas would be appreciated...





**"Nathan Thomas"**

---
---
**Andy Shilling** *June 13, 2017 20:01*

Are you sure your lens is the right way round?


---
**Steve Clark** *June 13, 2017 20:14*



No lens?



Very very dirty lens?



Lens mounted but not flat to base surface and or upside down?



More than one lens? I know crazy...



Piece of paper, tape, ash, spiderweb or something occluding the beam in the head?



Mirror in head upside down, cocked, cracked, dirty, broken, ect...



Head plate rotated or bent to the point of casting the beam out of field.





If your still getting a good burn at the entry point, then I would think it has to be something on that list. 


---
**Don Kleinschnitz Jr.** *June 13, 2017 20:18*

No mirror in head :)...

If you take out the lens do you then get a burn mark on the bed?


---
**Nathan Thomas** *June 13, 2017 20:34*

**+Andy Shilling** yes I even took the lens out of another k40 I have that was the right way up and still nothing 😐


---
**Nathan Thomas** *June 13, 2017 20:37*

Yes lens



Did clean the lens



Getting same result with a different lens, but I can double check if it's flat to the base



Only one lens



I agree, it has to be something with the lens...just have no idea what?


---
**Nathan Thomas** *June 13, 2017 20:42*

Yeah double checked that as well 



I took out the lens and got the same result...no burn marks


---
**Andy Shilling** *June 13, 2017 20:49*

You don't have a cracked mirror and your laser is being split by the crack? Hence a weaker spot hitting your head and not doing anything


---
**Nathan Thomas** *June 13, 2017 21:20*

**+Andy Shilling** I'm dumbfounded. I took it off earlier to double check the alignment and it hasn't engraved since. 

![images/4fac66b34ddf4215571e6b42b4e769a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4fac66b34ddf4215571e6b42b4e769a8.jpeg)


---
**Nathan Thomas** *June 13, 2017 21:20*

![images/1b96a3c6b635dd62300b9ae78ab03348.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b96a3c6b635dd62300b9ae78ab03348.jpeg)


---
**Andy Shilling** *June 13, 2017 21:24*

Did you remove the mirror in the head? I remember taking mine out and putting that back the wrong way. Cost me a new mirror, well one made from a HDD lol


---
**Nathan Thomas** *June 13, 2017 21:28*

Yes I did.



How did moving the mirror cost you a new one...did you drop it?


---
**Andy Shilling** *June 13, 2017 22:03*

No put it in backwards and the laser broke it.


---
**Nathan Thomas** *June 13, 2017 22:05*

**+Andy Shilling** What do you mean broke it, 



Cracked It?



Or you couldn't get it to focus again?


---
**Andy Shilling** *June 13, 2017 22:09*

It actually cracked it right through but it took a few minutes to do it. I'm presuming the back of the mirror stores up the lasers heat until it's too late, whereas if the orientation is correct it will just reflect it.


---
**Nathan Thomas** *June 13, 2017 22:12*

Mine isn't cracked but I'm wondering if something similar is going on because once the beam leaves the mirror it sure isn't hitting the table


---
**Andy Shilling** *June 13, 2017 22:16*

I'm not sure how to tell if the mirror is the right way out not other than just swapping it around. If you do try make sure the laser is on very low power and just test for a split second at a time.


---
**Nathan Thomas** *June 14, 2017 01:51*

Got it figured out finally. In case anyone comes across this at a later date with the same issue.



It was an alignment issue. I was getting a good beam from the fixed mirror to the y axis mirror and from the y axis mirror to the laser head mirror. But wasn't getting any marks from the laser head to the lens. To solve this I had to reposition all of the mirrors...not re align, reposition to get a straight beam going into the laser head and down at a 90 degree angle. I wasn't getting that before even though the burn marks indicated I was. Once all the mirrors were repositioned and then realigned all was well. 


---
**Steve Clark** *June 14, 2017 05:03*

I'm not sure I understand the problem yet? Any way you can explain it differently? 


---
**Andy Shilling** *June 14, 2017 05:05*

**+Nathan Thomas**​ glad you got it sorted 👍


---
**Phillip Conroy** *June 14, 2017 07:54*

If you have air assist then beam hitting side of nozzle 


---
**Nathan Thomas** *June 14, 2017 11:01*

**+Steve Clark**​​ sure. The problem was that I wasn't getting any power or a burn mark to the bed/table when I test fired. I was getting them at every other position and in alignment on all the mirrors except from the laser head mirror to the lens to the bed. I couldn't understand why because it appeared I was lined up perfectly. 



It wasn't until I removed the laser head mirror, replaced it with tape and test fired that I saw the beam was going in the front center hole but hitting the back (the actual mirror) at an acute angle. That is where and why there was a loss of power/no burn marks on the table.



Once I discovered that, it was about repositioning the first two mirrors, not just realigning, just to get the beam it hit that last mirror and reflect down at 90 degrees.



On the flip side, I have also gained about a half inch at the top of the table and now have to cut part of the blue vent to utilize it.


---
**Don Kleinschnitz Jr.** *June 14, 2017 13:09*

**+Nathan Thomas** does this illustrate the problem so we can capture it for others?

Left it looking down on optical path, right is looking into the machine from the front.

Arrows show correction movement.



![images/07ef93624981b84808ba005b22e94e67.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07ef93624981b84808ba005b22e94e67.jpeg)


---
**Nathan Thomas** *June 14, 2017 14:22*

**+Don Kleinschnitz** Yes sir. Concerning the diagram on the left, it will look like all is perfect because the beam is coming off the y axis mirror and hitting the laser head dead center...see pic from earlier post.



It's the diagram on the right that was the epiphany. Once I thought to remove the mirror to see exactly where it was hitting I saw that even though it was coming in dead center on the front...it hit the mirror at an angle more to the left, not centered. Which as you illustrated, altered the angle to the lens which led to no burn marks.

![images/f1af3af84e354aba1fcc62fd9c909db5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1af3af84e354aba1fcc62fd9c909db5.jpeg)


---
**Steve Clark** *June 14, 2017 16:20*

Great post,  thanks'


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/i7dcwJsNzDZ) &mdash; content and formatting may not be reliable*
