---
layout: post
title: "This is my first laser cutter and I'm pretty impressed with it's quality for the price"
date: October 30, 2015 18:08
category: "Hardware and Laser settings"
author: "Tim Scheck"
---
This is my first laser cutter and I'm pretty impressed with it's quality for the price. The only issue I am having is that it seems to drift a bit on both the x and y axis while traveling between cuts. For instance, exterior and interior cuts will be perfect measurement wise but not aligned properly. I get the same problem when I try to repeat a cut. It's always a little bit off. Anyone encounter this problem? Any suggestions would be appreciated!





**"Tim Scheck"**

---
---
**Stephane Buisson** *October 30, 2015 18:13*

Maybe you should check your belts. 

it could be loose belt, or twisted belt tensioner, or partly out of wheel (during transport).

belt tensioner holes are on the front side for X.

check my yesterday video @ 9s, each side of the "mouth".


---
**Phillip Conroy** *October 31, 2015 04:35*

How are you holding the work pice down?


---
**Tim Scheck** *November 01, 2015 17:30*

Thanks for all the suggestions. I'm happy I found this community. I fixed the problem by checking what board was selected in Corel Laser. Somehow the wrong board got selected. I changed it and now the machine is cutting perfectly. 


---
*Imported from [Google+](https://plus.google.com/116039992860468803736/posts/MbhdYq8zxfr) &mdash; content and formatting may not be reliable*
