---
layout: post
title: "ANYBODY GOT ANY IDEAS ON HOW TO MANUFACTURE A MANUAL ADJUSTABLE Z AXIS TABLE, NOT WANTING TO HOOK THIS UP TO ELECTRONICS OR ANYTHING BUT WHAT TO CHANGE THE HEIGHT MANUALLY?"
date: October 02, 2015 09:31
category: "Modification"
author: "george ellison"
---
ANYBODY GOT ANY IDEAS ON HOW TO MANUFACTURE A MANUAL ADJUSTABLE Z AXIS TABLE, NOT WANTING TO HOOK THIS UP TO ELECTRONICS OR ANYTHING BUT WHAT TO CHANGE THE HEIGHT MANUALLY?





**"george ellison"**

---
---
**Stephane Buisson** *October 02, 2015 10:21*

I am thinking about it, but one step a time ...

ideally working with/without stepper.


---
**Sean Cherven** *October 02, 2015 11:34*

I've been wondering the same thing. 


---
**Anthony Bolgar** *October 02, 2015 12:32*

I will have some pictures and drawings of the manual table I am currently building available in a few days. It uses aluminum C channel and threaded rods with 3d printed nut caps to make the adjustment easilbt hand.


---
**george ellison** *October 02, 2015 12:36*

Sounds good cant wait


---
**Michael Bridak (K6GTE)** *October 08, 2015 04:31*

a zero cost scrap onhand solution: [https://plus.google.com/+MichaelBridak/posts/GrcsFNXPAbY](https://plus.google.com/+MichaelBridak/posts/GrcsFNXPAbY)


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/S6SAuGR7ZV9) &mdash; content and formatting may not be reliable*
