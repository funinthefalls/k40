---
layout: post
title: "Is there anyone here using their laser to develop silk screen for tee shirt printing using the laser at really low power to do so?"
date: September 11, 2016 07:12
category: "Discussion"
author: "Alex Krause"
---
Is there anyone here using their laser to develop silk screen for tee shirt printing using the laser at really low power to do so?





**"Alex Krause"**

---
---
**Alex Krause** *September 11, 2016 07:57*

**+Peter van der Walt**​ do you think this is possible?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 10:44*

I actually tested laser cutting the heat-press vinyl sheets I have recently... the edges were slightly rough even using 4mA power & about 50mm/s. Maybe vibrations in the machine at the time (from my airpumps) so I will test again later sometime with all the vibrating parts moved elsewhere. On the positive note, I managed to cut through the vinyl only & leave the backing paper intact.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 10:44*

**+Yuusuf Sallahuddin** Actually, next time I'm at the laser I will get the settings from that cut & post here (as I wrote them on the piece I cut).


---
**Scott Marshall** *September 11, 2016 18:07*

I could have sworn I saw someone selling a special coated screen just for laser imaging, but I can't seem to track them down now.



 I was interested in it for doing control panel nomenclature, but had not yet got around to looking into it. I think I ran into it looking for PCB photoresist sheets so you may want to try that search route.



If I recall they were orangish coated screens, kind of like the rubber they sell for making stamps - that led me to think they were using EPDM as a coating, but that's just s guess.

Most of the 'normal' screens cure with UV, not how an IR laser would work as an exposure medium. I think the laser cut ones were an EPDM (stamp rubber) coated brass or stainless steel screen and the process was simply laser cutting away the rubber coating.



That's what attracted me, no developer or fooling around, burn and print.



The ones I saw weren't cheap, 3 for $20 if I remember correctly, but I could live with that if I were able to make screens to print my front panels.



Please let me know if you track them down, I'm still interested...





Scott

PS, got thinking, I have plenty of transfer vinyl, I'll try cutting it on the K40.

I'm thinking the edges may not end up as sharp as needed, but you may be able to cut smaller transfers on the Laser. I use a Silhouette Cameo drag knife cutter to do my T shirts, and it works great. They're not too expensive and do a good job once you sort through the crappy software (sounds pretty familiar huh?)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 21:08*

Here is results for the Vinyl Heatpress Paper (Jet Pro Opaque by Neenah). Slower speeds produce less jagged edges but require much lower power to not cut through the backing paper also.

![images/3fc2be180c22ac8f2a4e18f1081f298c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3fc2be180c22ac8f2a4e18f1081f298c.jpeg)


---
**Scott Marshall** *September 11, 2016 21:46*

**+Yuusuf Sallahuddin** I should have figured you had already tried it. 

Not much gets past you  without getting a pass under the laser.

(run away little roo)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 22:57*

**+Scott Marshall** Haha, yeah I've actually been meaning to try it since I got the laser & only just remembered recently when someone I know mentioned they wanted to get a particular design put on a t-shirt. Then I thought to myself, "oh yeah, I've been meaning to test that" & immediately went & tested it. However, I did a new test this morning for it because I have my focus point correct now & the difference was significant compared to the previous tests.


---
**Ulf Stahmer** *September 12, 2016 04:09*

**+Alex Krause**, I would use this: [dickblick.com - Speedball Diazo Photo Screen Printing Emulsions - BLICK art materials](http://www.dickblick.com/products/speedball-diazo-photo-screen-printing-emulsions/)



Make a screen out of 1" x 2"s, staple the corners together and stretch a screen over the frame.  Then apply the photo emulsion as directed on both sides of the screen (thin coating).  Photocopy your black and white image (positive) onto an overhead transparency and place it on the screen.



To expose, place the screen in direct sunlight for about a minute or two until you see the emulsion changing colour.  (It helps to place a sheet of glass over the transparency to keep it really close to the screen.)  Then remove the transparency and wash out the screen (exposed side only) in your laundry sink.  Once the screen has been completely washed out, expose the other (unexposed) side to the sun to cure that side too.



Use masking tape to block out the edges between the screen and the frame.  And print.  You should be able to do this for about $40 including the price of the emulsion and the emulsion should be able to cover at least 10 screens!


---
**Alex Krause** *September 12, 2016 04:24*

I have done silk-screening before I just couldn't remember if it was UV or IR to develop since it's been 14 years since the last time I made a shirt.... 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 12, 2016 06:49*

I've been doing some work with a group (Boomerang Bags) lately that screen-prints a lot of logos for their bags. Someone there told me you can use a lightbox, but if we need UV to cure the emulsion, then I guess we'd need a UV tube in the lightbox?


---
**Scott Marshall** *September 12, 2016 17:26*

I built a new light box (my 30 year+ old mercury vapor  tanning lamp 'PCB developer' died) from a cheap tool case (the black and silver ones you see on ebay).



Get a roll of the 5050 LED strips you see also on the bay, (they are available in UV for nearly the same price as white), and I lined the case with the whole 4 meters of light strip, top, bottom and sides. 



Gut the tool case, removing the foam and cloth, leaving only plywood. I added a stick on mirror product (from ebay of course) but it's highly optional, and I don't think I would bother if I did it again.



Cut the LEDs into appropriate length strips, attach using thick CA to supplement the peel and stick adhesive (a dab at each end and every 20cm will do. Hit it with baking soda or accelerator to cure fast.



Solder 20awg (or so) wire from strip to strip, putting them in parallel.



Any 12v (3+amp) power supply works, I put a PWM motor speed control on as a dimmer, but that's an option too.



Install 2 sheets of acrylic to hold your PCB (or silkscreen).



All up I think I have about $40 in it.



Case - $15, LED Strip - $14, PWM module $4, all the other parts were from my junkbox, and I just put a socket to use a handy laptop charger (make sure it's 12v), but those can be had (you KNOW where) for about 6 bucks or less.



I would have done a 'thing' on it, but don't know how to use the software on Pinterest and Instructables and all the other DIY sites.



It works great, exposes both sides at once (put ply over the acrylic on side to use it one sided - or you could put in a switch (if you thought of it in time).



It actually exposes photoresist faster than my old mercury vapor light and you don't have to worry about burning your eyes out (I still don't run it open, once your eyes have been flash burned, you'll never forget it)



You could easily scale it up or down for specific jobs, the trick is knowing the UV LED strip exists, and now you do....





Scott


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/PjVTvXHcjtk) &mdash; content and formatting may not be reliable*
