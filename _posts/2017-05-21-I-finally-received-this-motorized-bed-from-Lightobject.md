---
layout: post
title: "I finally received this motorized bed from Lightobject"
date: May 21, 2017 18:51
category: "Modification"
author: "Andy Knuts"
---
I finally received this motorized bed from Lightobject. ( [http://www.lightobject.com/Power-Z-Table-Bed-Kit-for-K40-Small-Laser-Machine-P722.aspx](http://www.lightobject.com/Power-Z-Table-Bed-Kit-for-K40-Small-Laser-Machine-P722.aspx) ). 

Now, the one I received has a bed, metal, plated in silver. At least, that's what LO says.

Now, it's not a honeycomb bed, nor a pinned bed or anything. Just a plate with holes in it.

Am I supposed to source some pins that fit inside those holes before I start acrylic cutting on this bed?

I'm thinking the bed might be reflecting the beam and cause damage to the acrylic.



If there's someone here who's using a bed like this, can you please tell me how you're doing it ?



Thanks





**"Andy Knuts"**

---
---
**Gee Willikers** *May 21, 2017 19:01*

I use tapered roll pins in the holes and some random methods to hold work above the table, like old cpu heat sinks.


---
**Gee Willikers** *May 21, 2017 19:04*

I bought it with the intent to modify it, but its a low priority project for me.


---
**Andy Knuts** *May 21, 2017 19:12*

I could order pins like this : [amazon.com - Amazon.com: Anladia 100x15mm Screwback Bullet Stud Spike Belt Bag Leathercraft Clothes Rivet Silver](https://www.amazon.com/Super-More-100x15mm-Screwback-Leathercraft/dp/B00LAYPNIY?ie=UTF8&psc=1&redirect=true&ref_=oh_aui_detailpage_o07_s00)




---
**Andy Knuts** *May 21, 2017 19:13*

I can also order a honeycomb bed and put that on top of it.. just not sure what would be the better choice :/




---
**Steve Clark** *May 21, 2017 21:05*

On mine I had to remove the table and flatten it as it had a pretty significate bow in it. you might check that.



I didn't buy from these guys but found them afterwards.



[ebay.com - hcr561 &#x7c; eBay](http://www.ebay.com/sch/hcr561/m.html?item=252866892756&hash=item3ae00a93d4:g:k8QAAOSw-7RVGYZ-&rt=nc&_trksid=p2047675.l2562)


---
**Tony Sobczak** *May 21, 2017 21:22*

**+Andy Knuts** I have honeycomb on mine and outta gummed up. Going tho remove tube honeycomb, clean the bed and pot some 12 gauge wire under it hc to lift so the smoke can escape under the hc too.


---
**William Steele** *May 22, 2017 04:28*

I use mending plates from Home Depot.  Cheap and easy to support your parts.



[homedepot.com - Simpson Strong-Tie MP 2 in. x 4 in. 20-Gauge Mending Plate-MP24 - The Home Depot](http://www.homedepot.com/p/Simpson-Strong-Tie-MP-2-in-x-4-in-20-Gauge-Mending-Plate-MP24/100375189)




---
**Terry Taylor** *May 22, 2017 20:57*

Posted this on FB, but I seem to get a LOT more reliable information from the folks here.



 I just ordered my Cohesion board and accessories and found a couple of TB6600s on eBay. Does anyone, know what the switch settings are for the TB6600s for the LightObject Z table and rotary?




---
*Imported from [Google+](https://plus.google.com/101382109936100724325/posts/NjFmFhA4h7g) &mdash; content and formatting may not be reliable*
