---
layout: post
title: "Has anyone got any feedback/thoughts on the slightly larger machines?"
date: February 28, 2017 15:55
category: "Discussion"
author: "Nigel Conroy"
---
Has anyone got any feedback/thoughts on the slightly larger machines?



Like this one with a 500 x 700 work area



[http://www.ebay.com/itm/111713953085?_trksid=p2060353.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/111713953085?_trksid=p2060353.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



I'm considering purchasing one for personal use/ small side business. 







**"Nigel Conroy"**

---
---
**Ian C** *March 02, 2017 09:01*

Hey buddy.



Yes, I went from a K40 to this same unit but in 80 watt guise within 3 months. I always needed the larger bed but grabbed a K40 as it was cheap to test the usefulness of laser for my application.



Mines been great, you can get the typical Chinese build issues and during some simple wiring upgrades on mine, I saw 240 volt wires twisted together and insulation taped :-/

But other than the usual bed/laser aligning it's been great.



I have changed my focus lense and mirrors for better quality items, but that's usual I gather for the cheap lasers.



I'm by no means a seasoned expert but I'll try and answer any questions you have.



Ian


---
**Nigel Conroy** *March 03, 2017 15:00*

Thanks **+Ian C**

Do you have a link to the one you purchased?

Can you pass through material?



I have seen this one

[ebay.co.uk - Details about  New 60W 110V CO2 Laser Engraving Machine Engraver Cutter w/ USB Interface](http://www.ebay.co.uk/itm/252730728666?euid=c1cb4c0910c24ed1b7c07774b861f245&bu=43400404672&cp=1&sojTags=bu%3Dbu)



and contacted seller to ask about passing larger material through and having it in focus.



They said you "You can pass through a 20 cm height object"



Any thoughts on that?



If I could get that functionality from the machine that would be a nice added bonus.


---
**Steven Whitecoff** *March 03, 2017 16:28*

Not a bad price although with freight its starting to get up there....are you up for a bit of DIY action? If so you can get an awesome mechanical system to use your K40 tube. I got quoted $789  shipped for a setup with a 1220x610 workspace(48x24"). CLOSED LOOP NEMA 23 steppers, drivers, powersupply from other source-$250 total. You then need mirrors(maybe included) and laser power supply ~$50 and lastly an enclosure.  Several suppliers can get you this at a little more or less cost. My choice is the British designed CCM which is the price mentioned, they are custom size so  the smaller the lower the cost to a point. 

FWIW, the freight cost shown there is for right now delivery, via Fedex my systems freight is $138. 



[aliexpress.com - CCM W40-06 working area 9060 CO2 laser single head belt toothed linear guide rail for laser machine](https://www.aliexpress.com/item/CO2-laser-9060-focus-lens-mirror-single-head-double-head-laser-mechanical-kit-XY-table-machinery/32737434661.html?spm=2114.12010108.1000013.4.r1N0NV&scm=1007.13339.33317.0&pvid=285dda9b-772a-4126-9067-8ec7e0f62732&tpp=1)


---
**Nigel Conroy** *March 03, 2017 16:44*

That's an interesting option **+Steven Whitecoff**

I wouldn't be against DIY assembly but the electronics aspect of it would take me a long time to get right.

Does that come with a controller board?



The total cost is approaching what the machine I linked above costs as the shipping is free on that one. I also dont' have a tube so would need to factor that cost in also.



It is a very good option to consider. But I think the time it would take me to put it together would be a big factor and having the worry of paying customs on it. 


---
**Ian C** *March 03, 2017 17:42*

Hey. Don't forget you're also probably going to want a better controller than the K40's Moshi or Corel Laser setup so bargain on another £250 for controller and then some for associated stepper drivers.

In all honesty unless you're starting with the kind of equipment inside the red and black units you'd be wasting money doing a DIY option. You'd also need a nice frame to mount it all to so it's rigid, that takes time and money unless you are a fabricator with welding and cutting equipment. 

Factor in import duty as well perhaps.



The laser tube in the K40 will most likely be anywhere between 30/35 watts, it's well known Chinese tubes are over driven. Indeed my 80 watt is probably only a 60/70 watt, you'd need to look at Reci or other branded tubes for actual rated specs. 



Stick to your original plan I'd say, as you will then have a great platform from which to improve on with better mirrors etc and eventually a branded laser tube, the latter my plan as and when mine drops off.



Ian


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/g4sAjaPW3dX) &mdash; content and formatting may not be reliable*
