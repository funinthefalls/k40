---
layout: post
title: "Whats going on my laser peeps?! I just recently got a K40 and decided that I needed a rotary axis for engraving shotglasses!"
date: October 21, 2017 08:39
category: "Discussion"
author: "Sean Foster"
---
Whats going on my laser peeps?!



I just recently got a K40 and decided that I needed a rotary axis for engraving shotglasses!



 Now I have the mini2 stock board and am trying to get the rotary axis working properly but it is giving me headaches. The shotglass is directly mated to the stepper motor and spins at a 1:1 with the stepper. The shotglass averages about 37.5mm in diameter. What settings do I need to change in CorelLaser to get this dang thing to work right?? 



 Thanks for your help!!





**"Sean Foster"**

---
---
**Ned Hill** *October 21, 2017 15:45*

There might be a few people using the stock board with a rotary but I can't recall anyone specifically off the top of my head.  I know in the corellaser engrave window there is a rotary tool option, in the bottom right, you will probably need to check and it gives you other parameters to set.  Out of curiosity, what are you using as a rotary?


---
**Sean Foster** *October 22, 2017 19:30*

**+Ned Hill** I am using a standard nema 17 stepper with a 3d printed cone pressed on to the shaft and a bearing in the rear to allow it to rotate smoothly. I haven't seen any rotary chucks that are direct drive so I may be on my own with this one. I have fooled with the rotary setting but none of the extremes get me any closer. 


---
**Mark Brown** *October 25, 2017 20:47*

Is there an extra drive on that board for the rotary or did you borrow the y axis?


---
**Sean Foster** *October 26, 2017 02:20*

**+Mark Brown** I just borrowed the Y axis slot. I also have a very small stepper that I can put in the current ones place if it is a power issue. 


---
**Mark Brown** *October 27, 2017 12:59*

Well, having never tried the rotary settings...  It seems to me that if the "tire" that rotates the workpiece were a size (or geared down) so that it was the same steps/mm as the y axis it would work.



In other words if the tire was the same size as the sprocket that the belt rides on, the software wouldn't need to know the difference.


---
**Sean Foster** *October 28, 2017 03:41*

**+Mark Brown** do you know what the stock ratio is on the K40?


---
**Mark Brown** *October 28, 2017 22:01*

Have a look around this topic:

[plus.google.com - I have been going my reading and would like to know from someone how the ste...](https://plus.google.com/114782552977074073979/posts/NYWBtrj6w1J)


---
**Sean Foster** *October 28, 2017 23:09*

Is there some way I can poke around and change these steps per rev in software? It looks like my steppers are really different from the stock ones.


---
*Imported from [Google+](https://plus.google.com/102954215477798568714/posts/6MC1QmqG3Jd) &mdash; content and formatting may not be reliable*
