---
layout: post
title: "I am thinking about buying one of these laser engraving machines"
date: July 13, 2015 13:34
category: "Discussion"
author: "Michael Marsden"
---
I am thinking about buying one of these laser engraving machines.  Primarily to burn a logo and names on the lids of some small wooden boxes I make.  



Couple questions:



1.  How big of a work area does it allow?  Or how big of a piece of wood can I use?



2.  How reliable are these machines?



3.  How well does it burn on to wood?



4.  How hard/easy is it to set up and get running?



Thanks!!





**"Michael Marsden"**

---
---
**tony alexander rico cera** *July 13, 2015 13:38*

La k40 tiene un area de trabajo de 20*30cm y el laser es de 40. Corta mdf de hasta 4mm


---
*Imported from [Google+](https://plus.google.com/108955291942526207665/posts/4iTUs6otUbB) &mdash; content and formatting may not be reliable*
