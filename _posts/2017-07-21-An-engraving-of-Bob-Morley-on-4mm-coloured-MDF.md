---
layout: post
title: "An engraving of Bob Morley on 4mm coloured MDF"
date: July 21, 2017 05:26
category: "Object produced with laser"
author: "Isa Nasser"
---
An engraving of Bob Morley on 4mm coloured MDF.







![images/0f2a78ceafbdd5b6c261911899ad9467.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f2a78ceafbdd5b6c261911899ad9467.jpeg)



**"Isa Nasser"**

---


---
*Imported from [Google+](https://plus.google.com/112675346235331499232/posts/cuqrvNsr8K7) &mdash; content and formatting may not be reliable*
