---
layout: post
title: "Remodelled the Air Assist that I would like to test, with AutoDesk 123d Design"
date: April 21, 2016 14:07
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Remodelled the Air Assist that I would like to test, with AutoDesk 123d Design. Worked a lot better this time around. If anyone is interested, here is the STL file for it [https://drive.google.com/file/d/0Bzi2h1k_udXwOGlqNGY5dGV2U2M/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwOGlqNGY5dGV2U2M/view?usp=sharing)

![images/0c30e475461d4c5ba321f2fcf6024da0.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0c30e475461d4c5ba321f2fcf6024da0.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Shane Nunn (HeyThatGuy R16)** *April 21, 2016 15:28*

very interesting and nice design. would like to hear how well it works when someone prints and tries it out


---
**Phillip Conroy** *April 21, 2016 15:57*

I to like the design,with this design air does not hit the focal lenes at all ,would have prevented any moisture in compressed air forming on focal lenes,a problem i had entill i added a silica gel mosture trap.


---
**Dennis Fuente** *April 21, 2016 16:35*

Yuusuf i like your design 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 17:37*

Thanks for the comments guys. I am currently in discussion with some makers on 3D Hubs (as I don't have a 3D printer) to determine if they are capable of printing it with the precision required (as the nozzles are 1mm outlets). Once I find someone capable of printing it with that precision, I will get one made up. I came up with the idea because I really don't like the design of the lightobjects (or others) air assist lens/mount head things. I will post back with info & results once I get it printed (unless someone else beats me to it).



**+Phillip Conroy** Silica gel seems like a good solution to collect the moisture. Especially good since you can get heaps of packets of that stuff in products you purchase. I find them in taco/pita bread packets all the time.


---
**Phillip Conroy** *April 21, 2016 19:10*

This may not work as well  as you think as what you need is air flowing straight through the cut taking the fumes away from the work peace as well as helping the combustion /vaperation process


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 20:52*

**+Phillip Conroy** You may be correct on that, generally speaking. However, for when I am cutting leather, the air can't flow straight through the cut because it takes multiple passes to cut through the leather. So I get a lot of smearing of the soot towards the back of the machine (as the exhaust drags it away & the single beam of air pushes it). So I'm hoping for cutting leather this will disperse the smoke more evenly around the cut, instead of just on the rear of the machine side. We'll have to wait & see after I get one printed up to see if it makes any difference, or turns out to be a failure.


---
**Dennis Fuente** *April 21, 2016 22:52*

Yuusuf  I don't know of a 3d printer that can print such a small hole it may be solid and you will have to drill it out to the desired Diameter 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 23:22*

**+Dennis Fuente** Yeah, that is not a huge issue if that is the case, just it will be less precise on the angle (as I can guarantee that I can't get it precisely on 17.25 deg for all 18 nozzles). I can maybe modify the design to cater for larger nozzles/outlets if required. It will just drop the pressure out of each nozzle.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 22, 2016 02:44*

As per my discussion with the printer on 3D Hubs, he has suggested a few edits to the original design. He uses an acrylic resin to print the parts & seems to be of the opinion that it will be doable with a few minor changes (see here [https://drive.google.com/file/d/0Bzi2h1k_udXwR2xTNWhCNEpZcU0/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwR2xTNWhCNEpZcU0/view?usp=sharing)). Unfortunately the file is upside down, so needs rotating to view correctly.



He mentioned that the entire piece needs to be rotated to have the first air outlet nozzle to be parallel with the printing bed so the resin can drain out while it's printing. I have no idea what this means, but I guess he knows what he is doing.


---
**Phillip Conroy** *April 22, 2016 06:10*

how thick is the leather you are trying to cut as i just tryed 3mm leather belt and it took 2 passes on 15ma at 4mm/sec to cut through.smell allful burned leather.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 22, 2016 07:08*

**+Phillip Conroy** Depends, because mostly I cut leather. Anywhere between 1-5mm thickness currently. I can cut it with a few passes for the thicker stuff, just I use raw natural vegetable tanned leather mostly which picks up the scorch marks from the smoke easily. So I tend to cut with faster speed & more passes, to minimise the amount of smoke per run.



Also, natural veg tan leather smells a bit funny when cutting, but not so bad in my opinion. Some of the tanning methods use horrible chemicals (which I'd assume some will poison you if you breathe it).



Leather that is already dyed/lacquered/etc takes a lot more effort to cut. I recently was cutting a 2mm thick piece of red leather. Absolutely horrible to cut. Took >10 passes & scorched the edges really badly. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 22, 2016 07:44*

So I've ordered one from a guy on 3D Hubs. AU$20 setup & $10.51 materials costs. It will be $10 setup + $10.51 materials for any further prints of this object. I've over-written the original STL file with the revised part (only change is fixed the undercut area on the air intake nozzle & rotated the whole piece to have the outlet nozzle closest to the intake nozzle parallel to the working bed, see: [https://drive.google.com/file/d/0Bzi2h1k_udXwZXJiY0R0UUhXcms/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZXJiY0R0UUhXcms/view?usp=sharing)).


---
**Phil Willis** *April 22, 2016 08:22*

Nice I will give it a try. I have the light objects head but using my own air assist nozzle.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 22, 2016 08:42*

**+Phil Willis** You might want to wait until I've verified that it works, save wasting materials. Also, if you do give it a go, it is designed that the flat ring (where the small nozzles come out) is to be flush with the bottom of the lens. As the nozzles are angled for a 50.8mm focal point from the lens base. You will have to space between the head metal plate & the air-assist as there is around 17mm from the plate to the lens base. I didn't bother with adding that to the model as I am not 100% sure how much spacing was necessary until after I see it printed. It should slip right over the stock lens holder. I've made the inner circle 26mm, so it will be a close fit.


---
**Don Kleinschnitz Jr.** *April 24, 2016 15:19*

Anotther consideration is that the air assist helps keep the lense clean as well as push vapors away from the cut.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 24, 2016 15:31*

**+Don Kleinschnitz** Yes, that is the intention of this design. To keep the vapours/smoke away from the lens always. In my imagination (not sure about reality yet), the 18 nozzles will create a cone of air, that no smoke should be able to get through. Thus, the lens (which is inside this cone) should never get dirty. At least in my imagination.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 26, 2016 07:56*

So, I've had it printed & went to pick it up this afternoon. It was printed with Resin, using the DLP 3D printers.



I was extremely satisfied with the result. The precision was wonderful & could print the 1mm diameter outlet nozzle holes with no problems.



Only issue I have with it is from my end. A design flaw that saw that the inlet pipe was attached very weakly to the main body & it snapped off before I even managed to get a photo of the piece. So a design revision is in order to fix that issue & then off for a reprint.



Photos of the print result (after I snapped it) are here:

[https://drive.google.com/folderview?id=0Bzi2h1k_udXwaHppR2phR2hyQjg&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwaHppR2phR2hyQjg&usp=sharing)



edit: the guy that printed it will reattach that piece using dental acrylic (as he prints dental cat scans also) & reinforce that section, free of charge (even though it's my design flaw). That will work out well for being able to test it before I make the necessary revisions to the design.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/fGZZWWjCQUF) &mdash; content and formatting may not be reliable*
