---
layout: post
title: "Where do I find the cable with the connector on it for the external driver expansion?"
date: December 27, 2018 01:29
category: "Hardware and Laser settings"
author: "Richard Vieira"
---
Where do I find the cable with the connector on it for the external driver expansion? What is the proper pin out for the A & Z headers? What is the pin out for the air assist and how it operates? I guess do you have detailed documentation on the new LaserBoard?



<b>Originally shared by Cohesion3D</b>



2 years ago, on Thanksgiving evening, we launched our storefront for sale with several general purpose control boards. Little did we know that our little board for laser cutting would represent the vast majority of our sales. Over this time we have learned what the needs are for a laser cutter controller, what the pitfalls are in all of these cheaply made machines, and we're super excited to announce our new, honed in, Cohesion3D LaserBoard!

LaserBoard should make upgrading a Laser Cutter even easier than it was before:



It has 4 embedded Trinamic drivers that can go to higher currents, meaning you can drive your XY head AND a Z table AND a rotary directly from the board (you'll want to double check the current your motor uses to be sure though).



It's now super easy to connect external stepper drivers if you do need the extra current, and there's no firmware or config changes, it should just work.



There's a Jack so that our power supply will plug directly into the board, rendering the limited and unstable 24v rail out of the laser no longer a problem.



It is isolated and protected in a bunch of different ways, making it harder for the big bad laser with an ungrounded frame and power spike happy LPSU to kill the board.



It can drive CO2 laser PSU's, diode lasers, and CNC machines too.



LaserBoard joins our lineup as the big brother to the Cohesion3D Mini - it is more resilient, easier to use, and a better value especially for the people that want to use a Z Table and Rotary with their laser.



LaserBoard is available for pre-order now and expected to ship in about 2 weeks. The Mini remains available, and is in stock for immediate shipping.



Head on over to the product page to learn more. We can't wait to see what you make with your machines, our boards, and the amazing LightBurn Software!

[http://cohesion3d.com/cohesion3d-laserboard/](http://cohesion3d.com/cohesion3d-laserboard/)



![images/59d0ac9cb29d08f039a4b2fdafb5e8f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/59d0ac9cb29d08f039a4b2fdafb5e8f5.jpeg)
![images/4d0fd29f81911cb714b330d0151dd3a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4d0fd29f81911cb714b330d0151dd3a2.jpeg)
![images/2021827c018c613df0b5bb05eb54ce8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2021827c018c613df0b5bb05eb54ce8c.jpeg)
![images/5339b0ebcdedbeee3298a24cb6b639de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5339b0ebcdedbeee3298a24cb6b639de.jpeg)
![images/ff0aaac3cd157c3e2918efab311157e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff0aaac3cd157c3e2918efab311157e9.jpeg)
![images/81989ce7086d8a27d0146c074d746c8d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/81989ce7086d8a27d0146c074d746c8d.jpeg)

**"Richard Vieira"**

---
---
**Fook INGSOC** *December 27, 2018 02:58*

Nice Photos!!!...and a Pretty Comprehensive Little Board You've Got There!!! I don't do any laser but I do some (novice) 3D printing and I can tell a good board when I see one!!! Regarding the laser power spike, is that related to the reverse/back EMF of switching the laser on and off???...if so, something like that should be able to be filtered out with a fairly straightforward toroid choke & capacitive circuit?!🤔


---
**Richard Vieira** *December 27, 2018 15:34*

Where do I find the cable with the connector on it for the external driver expansion? What is the proper pin out for the A & Z headers? What is the pin out for the air assist and how it operates? I guess do you have detailed documentation on the new LaserBoard?


---
**Cohesion 3D** *December 27, 2018 21:26*

**+Richard Vieira**  you've managed to make this a reshare into the K40 group, so it is lucky that we saw this post. 





The items you ask about are available as options at the time of LaserBoard purchase, and on the peripherals page of our shop:  [https://cohesion3d.com/product-category/peripherals/](https://cohesion3d.com/product-category/peripherals/)



[https://cohesion3d.com/shop/peripherals/cohesion3d-laserboard-external-stepper-driver-cable-4-pack/](https://cohesion3d.com/shop/peripherals/cohesion3d-laserboard-external-stepper-driver-cable-4-pack/)



[https://cohesion3d.com/shop/peripherals/external-stepper-driver-4-amp/](https://cohesion3d.com/shop/peripherals/external-stepper-driver-4-amp/)



Wiring is pretty straighforward: the pinout is labeled on the bottom of the board, but you just plug the cable into the LaserBoard (it will only go in one way) and run 6 wires straight down to our driver, as shown in the attached pic.





As for air assist, there are 2 control options, both in the upper right corner of the board.  The 2 pin header supplies a 5v signal, and the screw terminal provides a 24v Signal.  



For both of these things the important question is: what are you trying to control?  That will better dictate the responses. 



![images/1953138023a9962cbbdfca8c35b67e3f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1953138023a9962cbbdfca8c35b67e3f.jpeg)


---
**Richard Vieira** *December 28, 2018 02:03*

**+Cohesion 3D** for the cables i found these as i have all the peripherals.



[amazon.com - Amazon.com: 6pcs/lot XH2.54 Dopont Cable 4pin Stepper Motor Wire F/F White Black Terminal Motor Connector Cables 3D Print Dupont,Length 100cm: Industrial & Scientific](https://www.amazon.com/gp/product/B07D5RCY62/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1)



As for the air assist you have more than sufficiently answered my question as i will activate a relay (I have both 5v and 24v relays) which will then turn on 110VAC air pump.





My only other question is when i click the start button in LightBurn software why does the rotary connected to the A axis spin so fast it spin the cup off the rollers then it spins a normal speed required to engrave?




---
**Cohesion 3D** *December 28, 2018 02:10*

You still haven't told me what kind of Z Table/ Rotary you want to drive - with LaserBoard you might just be able to use the built in drivers. 



Any further questions would be best suited for the C3D or LightBurn support community on FB. 


---
**Richard Vieira** *December 28, 2018 02:21*

i have everything working now. just don't understand why when i click on start it spins really fast then it rotates at what appears to be a regular speed.




---
**Cohesion 3D** *December 28, 2018 02:22*

And that is a question best suited for the C3D or LightBurn support community on FB. 


---
**Richard Vieira** *December 28, 2018 02:32*

this is the rotary I'm using 

[ebay.com - Details about HM V.2.0 Fully XYZ Adjustable Laser Rotary Attachment for K40 Engraver & Larger](https://www.ebay.com/itm/HM-V-2-0-Fully-XYZ-Adjustable-Laser-Rotary-Attachment-for-K40-Engraver-Larger/223232715586?ssPageName=STRK:MEBIDX:IT&_trksid=p2060353.m2749.l2649)




---
**Richard Vieira** *December 28, 2018 02:33*

Thank you for your help




---
*Imported from [Google+](https://plus.google.com/116764978409385040525/posts/4H2oPFCPj7p) &mdash; content and formatting may not be reliable*
