---
layout: post
title: "Modifying the power supply to change frequency"
date: July 19, 2016 18:19
category: "Modification"
author: "HalfNormal"
---
Modifying the power supply to change frequency.



So in looking around the web to see how to minimize charring or burning of paper, Epilog suggests a frequency between 1000 and 1200 Hz. Has anyone here seen a power supply that has variable frequency or come across a how to modify the power supply for frequency control for our lasers? Enquiring minds want to know!





**"HalfNormal"**

---
---
**Custom Creations** *July 19, 2016 18:32*

Interesting


---
**Scott Marshall** *July 19, 2016 19:32*

It's not hard to vary the frequency within a limited range. There's a resonant frequency based on the inductance of the output transformer and you have to stay within about 15% or so of it's "happy place".



Most of the high voltage supplies used for Co2 lasers (low end ones anyway) use a design method known as "flyback" which is basically a free running oscillator feeding a tank circuit (parallel L-C arrangment) which is what determines the frequency. You can move it up a bit by decreasing the capacitor size and vice versa, but you can't go too far or you will move out of the efficient range and burn up the output transformer (Flyback Transformer). On most HV power supplies the Flyback arrangement gets you part of the way there, then they use a "tripler" which is a 3 stage rectifier/capacitor  circuit that converts the output to DC @ 3X the AC input voltage.



What we lack above all else, is documentation. It's often harder to figure out the factory intent without a drawing than building one from scratch.

Designing a power supply from scratch with digital frequency control wouldn't be that hard, but would require some expensive custom made transformers.



The expensive CO2 lasers are often driven with RF frequencies, I understand they are much more effiecient then the meager 9% we get on the K40. They are however quite expensive and dangerous.



I don't know what changing the pulse frequency of the laser supply would do, but I could do it, given a diagram and a tolerant output xfmr.



Scott


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/9zGoiox9zo8) &mdash; content and formatting may not be reliable*
