---
layout: post
title: "Could someone tell me what this pasta tube that comes with the machine might be?"
date: March 21, 2017 11:38
category: "Original software and hardware issues"
author: "Fernando Bueno"
---
Could someone tell me what this pasta tube that comes with the machine might be? Trying to translate what is in the box, it is a paste for cooling circuits, but in the picture of the box it looks like they are pouring the paste on a PCB board. Did someone else come with his machine?

![images/26cc3111432caa2532f45d3852cb376b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/26cc3111432caa2532f45d3852cb376b.jpeg)



**"Fernando Bueno"**

---
---
**Rich Sobocinski** *March 21, 2017 11:41*

Why don't you use Google translate on it? Use the camera feature


---
**Fernando Bueno** *March 21, 2017 11:44*

Yes, I have already done that, but I have stayed the same as I was. I find translation much more incomprehensible than the original Chinese text.


---
**Stephane Buisson** *March 21, 2017 11:55*

it is silicone to cover the hight voltage connexion on the tube in case you need to change it. (to avoid arcing to case)


---
**Fernando Bueno** *March 21, 2017 12:02*

**+Stephane Buisson** Ok. Perfect. Then it is a thermal sealer to coat the contacts in the tube. Would it be logical to use it also on the PCB board in the fuse and the AC contacts?


---
**greg greene** *March 21, 2017 13:17*

No - those are not HV


---
**Don Kleinschnitz Jr.** *March 21, 2017 13:32*

**+Fernando Bueno**  its not a thermal sealer it is High Voltage insulation used during tube replacement as **+Stephane Buisson** suggests.


---
**Brett Cooper** *March 21, 2017 13:42*

It's a liquid rubber. 



703 rubber silicone anti lighter arcing spirit high pressure cap seal glue 35 g

 [http://s.aliexpress.com/M3Ibuuqu](http://s.aliexpress.com/M3Ibuuqu) 

(from AliExpress Android)


---
**Don Kleinschnitz Jr.** *March 21, 2017 13:55*

never looked this up before, and this link has some specs:



Interesting: Dielectric Strength of 16KV per mm. 



[ebay.com - 703 adhesive silicone rubber insulation/potting/electronic waterproof sealant  &#x7c; eBay](http://www.ebay.com/itm/703-adhesive-silicone-rubber-insulation-potting-electronic-waterproof-sealant-/181671018066)




---
**Brett Cooper** *March 21, 2017 14:54*

I've used it for anti vibration on my tower case. The sides come off and when on they would still rattle, so a few drops on the facing edges fixed that. Also used dots for rubber base on stone sculpture. 


---
**Fernando Bueno** *March 21, 2017 17:20*

Perfect. It is understood. Thank you very much to everyone for your help.


---
**J Perry** *March 21, 2017 20:27*

I used it as tooth paste.....now I have really slick teeth!  :)




---
*Imported from [Google+](https://plus.google.com/+FernandoBueno/posts/FvC3Pn9AuzC) &mdash; content and formatting may not be reliable*
