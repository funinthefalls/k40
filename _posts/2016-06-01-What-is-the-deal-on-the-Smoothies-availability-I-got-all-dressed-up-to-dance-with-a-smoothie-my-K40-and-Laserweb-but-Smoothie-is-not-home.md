---
layout: post
title: "What is the deal on the Smoothie's availability, I got all dressed up to dance with a smoothie my K40 and Laserweb but Smoothie is not home?"
date: June 01, 2016 02:55
category: "Modification"
author: "Don Kleinschnitz Jr."
---
What is the deal on the Smoothie's availability, I got all dressed up to dance with a smoothie my K40 and Laserweb but Smoothie is not home? Should I be contacting someone?

:).







**"Don Kleinschnitz Jr."**

---
---
**Alex Krause** *June 01, 2016 03:58*

**+Arthur Wolf**​ any updates on the Availability at uberclock and robotseed?


---
**Alex Krause** *June 01, 2016 06:38*

That was my suggestion on another post **+Peter van der Walt**​ :)


---
**Arthur Wolf** *June 01, 2016 06:42*

Back in stock in a few days.


---
**Don Kleinschnitz Jr.** *June 01, 2016 15:39*

**+Arthur Wolf** pls ping when avail...


---
**Arthur Wolf** *June 01, 2016 19:54*

**+Don Kleinschnitz** Email uberclockllc@gmail.com and you will be pinged when they are available. It's just a few days away now.


---
**Joe Alexander** *June 02, 2016 10:40*

Did they give you a date on when they would be in stock **+Arthur Wolf**? Spread the info, save them email spam! :) Also waiting on a 4xC myself


---
**Arthur Wolf** *June 02, 2016 10:41*

**+Joe** Boards are in DHL's hands, they still need to go through customs, then they'll be available.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/JttH7rTvUEy) &mdash; content and formatting may not be reliable*
