---
layout: post
title: "Im thinking of cutting down the exhaust in the back"
date: December 01, 2015 23:45
category: "Modification"
author: "Tony Schelts"
---
Im thinking of cutting down the exhaust in the back. Is this a good idea and is it safe to use a workshop vacuum to get rid of smoke?  or is the fan that comes with it enough? 

thanks







**"Tony Schelts"**

---
---
**Brooke Hedrick** *December 02, 2015 07:31*

Cutting the exhaust back so it doesn't protrude into the cutting area is common.



On using a shop vac...  You would want to make sure it had a good filter on the exhaust if it is in the same space you are working - maybe something like hepa.  You really want to get the fumes out of the room.


---
**Vince Lee** *March 01, 2016 01:53*

I used a shop vac for awhile and found that it worked great but was noisy and the smoke very quickly clogged up the filter.  It might be better to remove the filter altogether if the shop vac can be vented to the outside.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/hV4kZ4rnbTZ) &mdash; content and formatting may not be reliable*
