---
layout: post
title: "I just got my K40 up and running, any suggestions as to where I find the honey comb base in Australia?"
date: May 16, 2015 21:18
category: "Discussion"
author: "Troy Baverstock"
---
I just got my K40 up and running, any suggestions as to where I find the honey comb base in Australia? Or what type of business I would try for it? And if it's too expensive what cheaper alternatives have been tried?



Also what methods have people successfully used for adjusting bed height?





**"Troy Baverstock"**

---
---
**Jim Coogan** *May 17, 2015 00:03*

Junk automotive radiators are a great source or Ebay for honeycomb.  For adjusting the bed you can buy a Z-axis table, what the call a lab jack off Ebay, or you can make a scissor jack out of wood or whatever you like that you can manually adjust. 


---
**David Wakely** *May 17, 2015 11:45*

I am going to try egg crate filter media for aquarium use. It's basically a plastic grid but I've seen some good reviews on people using egg crate for cutting. Plus it seems to be fairly cheap on eBay around £7-10. For bed height adjustment I was thinking either lab jack or threaded bar with nuts and washers. I would say the lab jack is probably better though


---
**Sean Cherven** *May 18, 2015 20:12*

Wouldn't the laser cut through the plastic egg crate?


---
**Troy Baverstock** *May 20, 2015 23:51*

Has anyone tried using aluminium flyscreen supported with some thin vertically standing aluminium lengths


---
*Imported from [Google+](https://plus.google.com/+TroyBaverstock/posts/DJk2bdXoDVU) &mdash; content and formatting may not be reliable*
