---
layout: post
title: "Got my air assist working, too. Don't laugh too hard at me, but I am using my 39 year old IBM \"power cleaner\" (just a small air compressor) that I was issued when I started with the IBM Office Products Division as Customer"
date: July 11, 2016 03:12
category: "Modification"
author: "Terry Taylor"
---
Got my air assist working, too. Don't laugh too hard at me, but I am using my 39 year old IBM "power cleaner" (just a small air compressor) that I was issued when I started with the IBM Office Products Division as Customer Engineer in 1977. Works just fine!





**"Terry Taylor"**

---
---
**Anthony Bolgar** *July 11, 2016 03:32*

It doesn't have to be pretty, it just has to work ;)


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/4qw6koK99fp) &mdash; content and formatting may not be reliable*
