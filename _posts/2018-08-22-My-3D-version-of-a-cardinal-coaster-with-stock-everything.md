---
layout: post
title: "My 3D version of a cardinal coaster with stock everything"
date: August 22, 2018 23:20
category: "Object produced with laser"
author: "Mike Meyer"
---
My 3D version of a cardinal coaster with stock everything. Wifey likes cardinals...



![images/a710d220db004028b1df7382c50f00e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a710d220db004028b1df7382c50f00e9.jpeg)
![images/c69b9e81613335b3d05232f0697f8122.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c69b9e81613335b3d05232f0697f8122.jpeg)

**"Mike Meyer"**

---
---
**Ned Hill** *August 25, 2018 13:07*

That's very cool.  Nice job :)




---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/43ioZqkoJy5) &mdash; content and formatting may not be reliable*
