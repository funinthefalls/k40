---
layout: post
title: "Just received my K40 today. So far it looks good but haven't powered it up yet"
date: November 11, 2015 03:31
category: "Discussion"
author: "Embeddedtronics"
---
Just received my K40 today.  So far it looks good but haven't powered it up yet.  



![images/683f1e4197ba37bfcbdef3d4ba569a07.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/683f1e4197ba37bfcbdef3d4ba569a07.jpeg)
![images/1d8df011da604d30c9db13e0c6eb22f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1d8df011da604d30c9db13e0c6eb22f1.jpeg)
![images/fa3a0d96b6ba6702729b78b58dca746a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fa3a0d96b6ba6702729b78b58dca746a.jpeg)
![images/b1457053fa4e945657748a18b60c9e2f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b1457053fa4e945657748a18b60c9e2f.jpeg)
![images/9645b8aeedcac559999700d234900b74.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9645b8aeedcac559999700d234900b74.jpeg)
![images/6794bddce8f2fcbc25f7145101d9adee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6794bddce8f2fcbc25f7145101d9adee.jpeg)

**"Embeddedtronics"**

---
---
**Phillip Conroy** *November 11, 2015 09:58*

make sure you have water tank at same level as laser tube,invest in a water flow switch before you make same mistake as me and forgot to turn watef pump on,cost new tube at$200 and a power supply $160 and a wait of 3 weeks for parts


---
**Embeddedtronics** *November 11, 2015 16:09*

Hooked up pump and laptop this morning. Did a test cut on paper and the laser works!!!!  


---
**Scott Thorne** *November 11, 2015 17:30*

At least yours worked, I had to change power supply and tube as soon as I got mine.


---
**Scott Thorne** *November 11, 2015 17:30*

At least yours worked, I had to change power supply and tube as soon as I got mine.


---
**Embeddedtronics** *November 11, 2015 18:04*

Yes I was very worried about the laser arriving damaged. I got lucky.  




{% include youtubePlayer.html id="8eLXTQMsbLA" %}
[http://youtu.be/8eLXTQMsbLA](http://youtu.be/8eLXTQMsbLA)


---
*Imported from [Google+](https://plus.google.com/110430811460241251216/posts/DQVp8apWz5P) &mdash; content and formatting may not be reliable*
