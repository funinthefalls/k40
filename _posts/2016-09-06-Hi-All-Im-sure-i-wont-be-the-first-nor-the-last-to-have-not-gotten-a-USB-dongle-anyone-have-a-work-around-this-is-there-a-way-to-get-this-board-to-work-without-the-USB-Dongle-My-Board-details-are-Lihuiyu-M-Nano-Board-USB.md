---
layout: post
title: "Hi All I'm sure i wont be the first nor the last, to have not gotten a USB dongle, anyone have a work around this, is there a way to get this board to work without the USB Dongle My Board details are: Lihuiyu M Nano Board USB"
date: September 06, 2016 13:55
category: "Original software and hardware issues"
author: "Ben Bilbao"
---
Hi All



I'm sure i wont be the first nor the last, to have not gotten a USB dongle, anyone have a work around this, is there a way to get this board to work without the USB Dongle 



My Board details are:



Lihuiyu M Nano Board 

USB Dongle B - Orange color 



Any guidance would be much appreciated 



Cheers 

Ben 





**"Ben Bilbao"**

---
---
**Ariel Yahni (UniKpty)** *September 06, 2016 14:05*

**+Hobo Jo**​ as Chinese are very worried about pirating software they made this key to prevent the use of the plugging. I don't beleive it's possible to make it work without it. Ask the seller to ship you one inmediatly 


---
**Ben Bilbao** *September 06, 2016 14:17*

Thanks for the reply unfortunately I bought mine from Gumtree locally, is there a third party software I could use, or will i have to buy the USB key separately?


---
**Anthony Bolgar** *September 06, 2016 14:22*

If you are using the nano board you are stuck needing the key, nothing else works with the nano board.


---
**Ariel Yahni (UniKpty)** *September 06, 2016 14:26*

**+Hobo Jo**​Either by the usb key separately eBay maybe or consider investing in the future to upgrade the board to smoothie, by doing that you are free to use many open-source softwares including the amazing LaserWeb. More info here www.laserweb.xyz


---
**Robi Akerley-McKee** *September 06, 2016 17:20*

Or replace your board with a smoothieboard (like ray's c3c board) or Arduino board (RAMPS 1.4 + mega 2560)


---
**Ben Bilbao** *September 07, 2016 04:14*

Thanks for the advice guys, I think the upgrade to an open source board should be worth it. 


---
*Imported from [Google+](https://plus.google.com/115468827205579295952/posts/ionbp5amRaV) &mdash; content and formatting may not be reliable*
