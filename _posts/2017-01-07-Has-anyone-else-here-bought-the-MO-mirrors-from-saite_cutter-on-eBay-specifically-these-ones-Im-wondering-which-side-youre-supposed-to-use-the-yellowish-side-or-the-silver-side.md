---
layout: post
title: "Has anyone else here bought the MO mirrors from saite_cutter on eBay, specifically these ones I'm wondering which side you're supposed to use, the yellowish side, or the silver side"
date: January 07, 2017 22:27
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Has anyone else here bought the MO mirrors from saite_cutter on eBay, specifically these ones [http://r.ebay.com/bKyJ1Q](http://r.ebay.com/bKyJ1Q)



I'm wondering which side you're supposed to use, the yellowish side, or the silver side. Mine are polished on both sides so I suppose I can use either. However, I swapped them out this morning, using the yellow side as the "face" or reflective side, and after making sure everything is aligned and focused, same power as before, and I can't cut through 3mm Birch to save my life. Even with two passes, it barely gets to the other side at 5mm/s 12mA. I'm used to cutting Birch at around 8mm/s and 10mA with only the occasional piece not all the way through. But even at 5mm/s and 12mA, two passes should've gone through.



I'm about to go flip them to the silver side and try it again, before I pull them out and put the old ones back in. It just seems odd that they won't work. Did I miss something?





**"Ashley M. Kirchner [Norym]"**

---
---
**Ned Hill** *January 07, 2017 23:25*

I have Mo mirrors from lightobject and they have only one mirror finished side and that side has a yellowish tint to them.  I'm pretty sure they are Mo deposited on Si.  Coloring is probably related to the thickness of the Mo coating.


---
**Ian C** *January 08, 2017 11:27*

Hey buddy. The yellow'ish side is the reflecting side. I this coating is to help promote reflection of the laser beam with minimal power loss


---
**Bill Keeter** *January 09, 2017 02:24*

yeah, yellow/gold color is the mirror side. The silver colored side is the back and isn't polished. As mine isn't.


---
**Ashley M. Kirchner [Norym]** *January 09, 2017 02:47*

Mine are all polished on both side. The old mirrors are only polished on one side.


---
**Ned Hill** *January 09, 2017 04:24*

Did you end up trying the other side?


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/RzsEFxxWJgr) &mdash; content and formatting may not be reliable*
