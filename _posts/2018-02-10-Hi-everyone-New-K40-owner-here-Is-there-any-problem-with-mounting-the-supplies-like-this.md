---
layout: post
title: "Hi everyone. New K40 owner here. Is there any problem with mounting the supplies like this?"
date: February 10, 2018 13:33
category: "Modification"
author: "Jake B"
---
Hi everyone.  New K40 owner here.  Is there any problem with mounting the supplies like this?



Been working through the standard upgrades. Just finished installing the Cohesion3D controller, but I'm having issue with the display garbling when powered up from the LPSU.  So next up is a dedicated 24v power supply to provide cleaner power. 



I selected a Mean Well 24v 6.5a supply, mainly because it was enclosed and I've used Mean Well stuff in the past. ([https://www.amazon.com/gp/product/B00DECU4VQ](https://www.amazon.com/gp/product/B00DECU4VQ))



I've seen some vertical mounting solutions, but I'm not sure if airflow/cooling is a concern.  (I do have a ~200CFM fan pulling air through during use)



Thanks

![images/034a27cb5f1521ad66d4100bbee620a6.png](https://gitlab.com/funinthefalls/k40/raw/master/images/034a27cb5f1521ad66d4100bbee620a6.png)



**"Jake B"**

---
---
**Jorge Robles** *February 10, 2018 13:38*

Not a problem. I've got the same layout


---
**Fook INGSOC** *February 10, 2018 16:00*

If you've got only one fan, add another if possible, for redundancy!....fans are CHEAP!!!


---
**Don Kleinschnitz Jr.** *February 10, 2018 17:20*

Cut a hole in the side so the PS fan has opening and mount it 180 from that position on the wall.

That way the PS fan sucks only clean air :).


---
**Jake B** *February 10, 2018 17:27*

This LSPU doesn't have a fan. I guess they don't make 'em like they used to.  All things being equal, I do prefer this orientation so that the connectors are more accessible.


---
**Don Kleinschnitz Jr.** *February 10, 2018 19:50*

**+Jake B** NO Fan, not even inside the supply??? The LPS is the #1 failure item in these boxes, good idea, lets remove the fan..... 



All the more reason to mount it on the side and add a fan :).


---
**Jake B** *February 10, 2018 20:43*

Hard to see but no fan.  I’ll dig out at 24v fan and blow some air between the PSUs once I figure out how I’m mounting them![images/5512574941f54424440059e05fda163c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5512574941f54424440059e05fda163c.jpeg)


---
**Jake B** *February 11, 2018 12:12*

**+Don Kleinschnitz** One note for you on this fan-less PSU in case you're interested (likely already know): The K+ and K- are reversed!  Confusingly K+ is ground and K- is the test fire signal.  The JST-XH connector has only 1 pin  installed, coming from the digital PSU control.  The digital control must get its ground from the G/Pwr/5v header.

![images/39a74dafbac82ba0dec4989a154731e2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/39a74dafbac82ba0dec4989a154731e2.png)


---
**Don Kleinschnitz Jr.** *February 12, 2018 04:36*

**+Jake B** in the supplies I have traced,  K+ is connected to the L on the right power connector. So if L is grounded then so is K+.

K- normally provides ground to the K+ through a "Test" switch.



What is on "L" on the right connector?  You can see if K+ and L are connected together with the DVM. 



Schematic is here if you have not already seen it.

[digikey.com - K40 LPS-2](https://www.digikey.com/schemeit/project/k40-lps-2-EFKO7C8303M0/)




---
**Jake B** *February 12, 2018 11:58*

In my case, K- and L are connected internally as verified by the meter.  K+ is ground.  My guess is the silkscreen is reversed.


---
**Don Kleinschnitz Jr.** *February 12, 2018 13:45*

**+Jake B** interesting does your silkscreen look like this? I can barely see it in your picture and I think you are correct its reversed.



So to improve the LPS they removed the fan and messed up the silk screen :(!



Having these reversed would not cause a problem when using a simple test button because one supplies the ground for the other. However when expecting to get connected to L, using K+ would cause a problem. Another reason to use the "L" pin for PWM.

![images/9bda3ecf375d1617adf102d1a5490214.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9bda3ecf375d1617adf102d1a5490214.jpeg)


---
**Jake B** *February 12, 2018 17:43*

**+Don Kleinschnitz** mine is San Serif. Your K+ is where my K- is.  You can kind of see it in the picture above with the fluke meter.   Seems to me that it’s just a silkscreen issue because the pin (rightmost or closest to power connector header) is the same.


---
*Imported from [Google+](https://plus.google.com/101176403058148207601/posts/2UFTktaKHgb) &mdash; content and formatting may not be reliable*
