---
layout: post
title: "I stared at this for a long time before I decided I do not need another laser project"
date: September 13, 2018 13:52
category: "Material suppliers"
author: "Don Kleinschnitz Jr."
---
I stared at this for a long time before I decided I do not need another laser project. Pretty cheap pretty small.



[https://www.gearbest.com/3d-printers-3d-printer-kits/pp_242557.html](https://www.gearbest.com/3d-printers-3d-printer-kits/pp_242557.html)





**"Don Kleinschnitz Jr."**

---
---
**HalfNormal** *September 13, 2018 14:17*

Looks like they're repurposing old DVD parts


---
**Ray Kholodovsky (Cohesion3D)** *September 13, 2018 14:22*

The running joke is that someone found a warehouse of obsolete cd drives, and here we are. 


---
**Don Kleinschnitz Jr.** *September 13, 2018 14:50*

Doesn't a 500mw diode, supply, driver and controller cost more than $84?


---
**Ray Kholodovsky (Cohesion3D)** *September 13, 2018 15:01*

Not if you cut 5 corners out of a possible 4. 


---
**James Rivera** *September 13, 2018 18:17*

I actually bought one of these before I bought my K40. Mine is a 500mw blue laser (445nm) and it looked like mine was actually repurposed <i>FLOPPY</i> drive mechanical parts along with an Arduino Nano. I think the laser itself is probably a from blu-ray writer. The engraving area is so small it can hold a business card, but probably not a playing card (e.g. 5 of clubs). EDIT: I'm probably wrong; it probably is from CD or DVD drives.



Aside: After I bought it I read up on the safety issues. Holy s**t! This little sucker can blind you in a few milliseconds (you might think you're ok, but then 48 hours later you'd be permanently blind). Don't f**k around with lasers without proper eye protection!!


---
**James Rivera** *September 13, 2018 18:19*

All that being said, I've been meaning to take it out of its box again and see if it will engrave on the tiny anodized aluminum dog tags I bought a while back.


---
**Tim Gray** *September 13, 2018 21:29*

**+HalfNormal**  they are. 


---
**Jonathan Davis (Leo Lion)** *September 14, 2018 06:20*

I got screwed by them when I bought my Eleks Maker A3 and I bought the higher priced version.


---
**Don Kleinschnitz Jr.** *September 14, 2018 15:29*

**+James Rivera** I had planned on putting on a cover and interlock. I wonder how many folks are blinded by these things.....:(


---
**James Rivera** *September 14, 2018 22:27*

**+Don Kleinschnitz** I was so scared of it when I got it that I kept it in its box while using it and only viewed its progress with an old webcam!


---
**Ray Kholodovsky (Cohesion3D)** *September 14, 2018 22:29*

Should have used a steel box :) 


---
**Kirk Yarina** *September 18, 2018 12:20*

Be careful dealing with GearBest, their warranty is worthless even for DOAs.  I purchased a slightly more upscale engraver that arrived with a partially soldered microUSB connector, after considerable back and forth was offered a refund if I returned it.  Shipping was more than I paid.  No way to buy a replacement controller on my model.  Best they would do was $20 store credit on a $100+ device.  They said free shipping on returns, refused to honor it


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/7bdG53yL83H) &mdash; content and formatting may not be reliable*
