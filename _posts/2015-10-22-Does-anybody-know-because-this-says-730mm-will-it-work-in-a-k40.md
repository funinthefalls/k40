---
layout: post
title: "Does anybody know because this says 730mm, will it work in a k40?"
date: October 22, 2015 06:39
category: "Discussion"
author: "george ellison"
---
Does anybody know because this says 730mm, will it work in a k40? The seller says it does but k40 tubes are around 700mm. Also does it have the right connectors a either end. Many thanks



[http://www.ebay.co.uk/itm/Water-Cooling-40W-Sealed-Laser-Tube-for-CO2-Laser-Engraver-machine-3000-4500-H-/111801328950?hash=item1a07e0e536:g:qJwAAOSwAYtWJJkx&autorefresh=true](http://www.ebay.co.uk/itm/Water-Cooling-40W-Sealed-Laser-Tube-for-CO2-Laser-Engraver-machine-3000-4500-H-/111801328950?hash=item1a07e0e536:g:qJwAAOSwAYtWJJkx&autorefresh=true)﻿





**"george ellison"**

---
---
**Stephane Buisson** *October 22, 2015 10:59*

I don't have an answer. but some K40 are fitted with a  35W.

[http://www.ebay.co.uk/itm/35W-47mm-x-700mm-CO2-LASER-TUBE-GLASS-TUBE-FOR-LASER-ENGRAVING-MACHINE-/261601557151](http://www.ebay.co.uk/itm/35W-47mm-x-700mm-CO2-LASER-TUBE-GLASS-TUBE-FOR-LASER-ENGRAVING-MACHINE-/261601557151)?


---
**Stephane Buisson** *October 22, 2015 11:02*

look at that one (in EU)

[http://www.ebay.co.uk/itm/CO2-Laser-40W-Tube-f-Engraving-cutting-Engraver-machine-Water-cooling-70CM-BEST/141769659601?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D20140122125356%26meid%3Dc332eb235c65438ca96f06c0ec255f13%26pid%3D100005%26rk%3D4%26rkt%3D6%26sd%3D261601557151](http://www.ebay.co.uk/itm/CO2-Laser-40W-Tube-f-Engraving-cutting-Engraver-machine-Water-cooling-70CM-BEST/141769659601?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D20140122125356%26meid%3Dc332eb235c65438ca96f06c0ec255f13%26pid%3D100005%26rk%3D4%26rkt%3D6%26sd%3D261601557151)


---
**Gee Willikers** *October 22, 2015 12:22*

Fwiw mine is 720mm end to end.


---
**george ellison** *October 22, 2015 15:15*

should this 730mm work then. Do you think the extra 10mm will make a difference? Thanks everybody :)


---
**Gee Willikers** *October 22, 2015 15:32*

Plenty of room in mine.


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/HiB4h3EHAtC) &mdash; content and formatting may not be reliable*
