---
layout: post
title: "Doing my smoothie swap today! Ray Kholodovsky Here goes...."
date: December 10, 2016 14:39
category: "Smoothieboard Modification"
author: "rick jarvis"
---
Doing my smoothie swap today! **+Ray Kholodovsky**​

Here goes....

![images/5a2b923e1810c66ea06b25167bc744ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a2b923e1810c66ea06b25167bc744ed.jpeg)



**"rick jarvis"**

---
---
**ben ball** *December 10, 2016 14:42*

I'm sure you've got your hands full right now but what's the point of doing this swap?


---
**Ariel Yahni (UniKpty)** *December 10, 2016 14:48*

**+ben ball**​​ independence, easy of use, greyscale among other thing. Look at the LaserWeb community for stuff you can do and will be able to do on the new version [http://plus.google.com/communities/115879488566665599508﻿](http://plus.google.com/communities/115879488566665599508%EF%BB%BF)

[LaserWeb / CNCWeb](http://plus.google.com/communities/115879488566665599508)


---
**greg greene** *December 10, 2016 15:17*

I'm sure it will go Smoothly Bigly ! :)  If I can do it, Anyone can do it.


---
**ben ball** *December 10, 2016 15:17*

I didn't realize grayscale wasn't an option with the old board. Hope it goes smooth(ie) for you! (Puns)




---
**rick jarvis** *December 10, 2016 18:13*

It did go smoothly.. BUT.... I'm having issues getting it to connect to laserweb.. Any suggestions please? 


---
**greg greene** *December 10, 2016 19:11*

What OS are you using? I had a very hard time doing it with Win 8 had to upgrade to Win 10 to get the Smoothie board drivers to work - did you have any problem installing them?


---
**Anthony Bolgar** *December 10, 2016 20:07*

I did not think that under WIN10 you needed drivers. **+Arthur Wolf** ?


---
**Ariel Yahni (UniKpty)** *December 10, 2016 20:22*

**+rick jarvis**​ did you follow step by step the installation instructions. It must be exactly in the same order or it will not work


---
**rick jarvis** *December 10, 2016 20:31*

i did follow exactly yes, and i did have to install drivers. i get laserweb up theres no errors showing for drivers.. i just cant get laserweb to connect to the machine. thanks




---
**greg greene** *December 10, 2016 20:54*

What com port shows in LaserWeb? Check with Device manager and see if that port is working ok  Are green lights flashing by the USB port on the Cohesion Board?


---
**Anthony Bolgar** *December 10, 2016 21:23*

Yes, make sure it is the smoothie com port and not the smoothie sd reader com port


---
**Robert Norton** *December 10, 2016 22:01*

Good luck .


---
**Wolfmanjm** *December 11, 2016 00:04*

win10 does not require drivers for the serial, in fact installing drivers will make things wobbly.


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 02:31*

So, going down the list:

Installing Drivers if necessary. Windows 10 - no.  Other earlier Windows - Yes.  Other OS - no.  [http://smoothieware.org/windows-drivers](http://smoothieware.org/windows-drivers)

Putting the config file from the dropbox link provided to you onto a microsd card. Insert card into board.  Plug in USB.  Green LEDs should turn on, 2 should flash on/off.  Board/ memory card should show up as flash drive.  Start Menu --> Devices and Printers should have a 'Smoothieboard' entry and double click that, go to the hardware tab, and confirm the COM Port for the board. 

Confirm this is the value in LW, along with the proper baudrate (default in config is 115200 aka 115k), and hit the green connect button. 


---
*Imported from [Google+](https://plus.google.com/105230811165966980201/posts/ERfTXH77Ry6) &mdash; content and formatting may not be reliable*
