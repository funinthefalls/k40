---
layout: post
title: "Replacment PSU. My original PSU went out and just put in the replacement today"
date: November 18, 2016 20:12
category: "Original software and hardware issues"
author: "giavonni palombo"
---
Replacment PSU.  My original PSU went out and just put in the replacement today. The replacement is only putting out 5 mA max. Is there a pot that is adjustable? Looks like 3 small blue "somethings" sorry not a electronic guy, with brass screws on top. Could one be it? Labeled "DL", "PL" and "24vadju".

![images/80de2aa44da017426211c729737c5247.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/80de2aa44da017426211c729737c5247.jpeg)



**"giavonni palombo"**

---
---
**Don Kleinschnitz Jr.** *November 18, 2016 22:06*

Those are trim pots and I doubt is your problem.


---
**greg greene** *November 18, 2016 22:11*

Is it set for 220 V (europe) or 110 NA input ?

Check to see if there is a switch setting on the outside of the case


---
**giavonni palombo** *November 18, 2016 22:33*

Its not on 220 volt


---
**greg greene** *November 18, 2016 22:36*

Does that mean - the PSU switch is not set for 220V or the power plug you using is not 220?


---
**giavonni palombo** *November 18, 2016 22:36*

Don  Kleinschnitz your thoughts of what it could be? 


---
**Don Kleinschnitz Jr.** *November 18, 2016 22:58*

Why did you replace it? Was it an exact replacement? I.e. the same plugs?



﻿


---
**giavonni palombo** *November 19, 2016 00:03*

Greg Its set for 110v first thing i did out of the box.


---
**giavonni palombo** *November 19, 2016 00:11*

Don I replaced it because it quit on me. The old one wouldn't fire the tube at all, this one does to an extent so I 99% sure it's the PSU and not the tube. The replacement had all the same plugs and advertised as a 40 watt laser replacement PSU. Now it dose to be slightly different internal. Far as I can tell only by looks.


---
**greg greene** *November 19, 2016 01:00*

Ah, well hate to say it - but it seems you got a new one only slightly better than the old one :(


---
**Paul de Groot** *November 19, 2016 01:02*

The psu models are sometimes different but that should not make a difference.  Check your tube connections. Mine were just wrapped with silicone paste on it.  After proper connection it worked well again.


---
**giavonni palombo** *November 19, 2016 02:02*

Paul, I cleaned the small metal connection, cleaned with 1500 grit paper, cut a little of the wire and then wrapped it tightly around the metal connection even use small pliers to make sure it was tight. 


---
**Paul de Groot** *November 19, 2016 02:16*

I  actually split the copper wire and twisted both ends again will on the connection post and soldered the twisted ends together without heating the metal connection post


---
**giavonni palombo** *November 20, 2016 00:47*

It working for some reason now, but cant cut like before, half the speed and 5 ma more just to cut the same exact material. So, before just a few cuts before it died I was cutting 3mm birch Ply at 8.5mm and 11mA, now 4mm and 16mA. Oh same material, same lot.


---
**Paul de Groot** *November 20, 2016 01:07*

Probably a dirty lens.  Check it and make sure the concave side is up. After that check mirror  alignment. I had this problem a few times. It easily can get out of alignment. After that cut a ramped piece of birch and see where is cuts at best height 


---
**giavonni palombo** *November 20, 2016 02:33*

Nope, I cleaned all 3 mirrors and the lens. Convex is down and I did relined the machine, even got the bottom right in the same spot as the other 3 corners. Spent 2 hours with the mirror [alignment.Funny](http://alignment.Funny) thats probably it there. I did try the lens to work surface at 20mm, 23mm best for my machine, and 25mm no real difference. Ill recheck tomorrow. Man I spend more time fixing and troubleshooting than cutting,  getting old fast.


---
**greg greene** *November 20, 2016 02:49*

The bump should be UP on the lens


---
**giavonni palombo** *November 27, 2016 15:35*

Working again.  Completely tore the machine down and started over. Just have one issue the kerf along the X axis is cutting at a slight angle. 


---
**Paul de Groot** *November 27, 2016 20:15*

That angle happens when the beam does not hit the lens dead centre. Had that as well and took me a lot of adjusting. It still has a slight angle but i am happy with it.


---
**greg greene** *November 27, 2016 21:40*

This is why you need to align the beam at the centre of the mirrors


---
**giavonni palombo** *November 28, 2016 14:42*

Ok I checked my mirrors. The first, and second are as close to center as possible, hitting the same spot on each mirror in all 4 corners. Now the second mirror as it goes to the 3rd is dead on in the top left, top right and bottom left. In the bottom right the beam dot is over lapping slightly. I found that the beam is hitting very close to the air assist outlet hole. I know this is the problem but I can't figure how to correct it.



By the way I have had my mirrors way out of adjustment before  and it was cutting better than now, far as the kerf goes. But it was exiting the focus lens in the center. So I'm not sure what I did when I tore the machine down.﻿


---
**giavonni palombo** *November 28, 2016 14:46*

Oh forgot. My mirrors and focus lens are upgraded and have been before all this.


---
**greg greene** *November 28, 2016 16:04*

I had to use washers to adjust the head to be perfectly perpendicular to correct the beam hitting the side of the nozzle, also check that the rails are not slightly twisted or have a bend in them that would cause the head to twist slightly where the beams go out of alignment.


---
**giavonni palombo** *November 29, 2016 03:31*

Ok thanks everyone. I think its all good now. It was the 3rd mirror holder, if its is turned it affects the placement of the beam. Clockwise to the back of the machine, counterclockwise to the front.


---
**Madyn3D CNC, LLC** *December 10, 2016 15:56*

maybe a day late and a dollar short but perhaps for the future... hope this helps  

![images/6efd4d7042f7581ded11a09726f74122.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6efd4d7042f7581ded11a09726f74122.jpeg)


---
**greg greene** *December 10, 2016 17:02*

Thanks for that - we will all likely have to replace the PSU at some point


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/WSWjmcF26qx) &mdash; content and formatting may not be reliable*
