---
layout: post
title: "I'm just wondering but has anyone ever worked with pewter using the lost foam method?"
date: August 24, 2016 01:17
category: "Discussion"
author: "Jonathan Davis (Leo Lion)"
---
I'm just wondering but has anyone ever worked with pewter using the lost foam method? Because I have 6.2lbs of it and I'm wanting to work at making a crown (to wear or be used for decorative purposes) using it. I'm going for more of a Middle Ages theme but with something fitting for say a lion! 

[https://plus.google.com/+JonathanDavisLeo-Lion/posts/JcKcWFLunb4](https://plus.google.com/+JonathanDavisLeo-Lion/posts/JcKcWFLunb4)





**"Jonathan Davis (Leo Lion)"**

---
---
**Thor Johnson** *August 24, 2016 02:24*

I've found that pewter isn't hot enough for lost foam (but last time I tried that, I was using foam from Scotty's [If anyone remembers them!] and a Coleman Stove for melting, and I don't remember the density of the foam).



It might work with a bit more flux and you have a good kush head -- just a funnel shape in the sand wasn't good enough.



OTOH, Aluminum works well (Propane melting) with a 3" kush head.



OT3H - You can't do that with HIPS prints.  HIPS is too dense and won't melt out of the way.



OT4H - You can cast pewter into plaster of paris easily -- bake it out 2-4hrs  or so and you're golden [though foam makes an incredible stench, so do that outside! -- PLA or wax is much nicer!!] (Aluminum is just-barely-too-hot for plaster of paris) -- but if using plaster, make sure you have a bunch of vents (waxed broom straws work) - air doesn't escape through plaster.


---
**Jonathan Davis (Leo Lion)** *August 24, 2016 02:26*

**+Thor Johnson** well from various videos on YouTube I've seen, lost foam looked like a simple method. Plus I don't exactly what to spend to much cash!


---
**Jon Bruno** *August 24, 2016 02:35*

I did a lost PLA casting of an intake manifold and airbox for my micro car resto. The PLA must be burned out of the investment before pouring.

(Slowly ramped up and held at 730c for 5 hrs)Needs a PID to do it right.

You can't direct burn PLA. Foam is definitely easier but the quality usually suffers and requires too much post processing.

the plaster investment is a slurry of Plaster and fine play sand. that improves its durability to potential temp or physical shock.


{% include youtubePlayer.html id="ZfJK5s7V-Zc" %}
[https://youtu.be/ZfJK5s7V-Zc](https://youtu.be/ZfJK5s7V-Zc)


---
**Jonathan Davis (Leo Lion)** *August 24, 2016 02:45*

**+Jon Bruno** ok well I'm working from a limited budget and with a limited tool set, I would like to work with the lost foam method. Plus I'm just wanting to reuse the 6.2lbs of pewter I have.


---
**Thor Johnson** *August 24, 2016 02:49*

I'd try it.  Get as light of foam as possible, and make a 3" kush head to increase the head pressure.  Don't breathe the fumes during casting.



If it doesn't work, you can always re-cast (just like if my weld isn't good, I have a die grinder that can clean it up -- or destroy all evidence I tried).


---
**Jonathan Davis (Leo Lion)** *August 24, 2016 02:52*

**+Thor Johnson** ok well I have a craft foam with the measurements; 1.8in. X 11.8in x 17.8in 


---
**Jon Bruno** *August 24, 2016 03:03*

It's worth a try... Worst case scenario is that you fail and stink up the shop trying.



You can use an old soup can as a kush head if you want to keep with the "on the cheap" theme. Just make sure you partially bury it in the sand or it'll try to float away on you during the pour.


---
**Jon Bruno** *August 24, 2016 03:05*

I'd imagine some craft foam might prove to be too dense to vaporize properly. come to think of it pewter mets at around 338–446F depending on composition.. That's going to be a tough one ...


---
**Jonathan Davis (Leo Lion)** *August 24, 2016 03:08*

**+Jon Bruno** I'd figured doing that anyway, outside of course. One question though would a basic stainless steel 12-quart stock pot with glass lid work for something like this melting 6.2lbs of pewter?


---
**Jonathan Davis (Leo Lion)** *August 24, 2016 03:10*

**+Jon Bruno** the foam is what Walmart had in stock at the time, brand; FloraCraft. 


---
**Jon Bruno** *August 24, 2016 03:32*

Oh that decorative white stuff they use for floral arrangements.. The balls and hoops not the green water holding foam. That foam is real airy and light.


---
**Jonathan Davis (Leo Lion)** *August 24, 2016 03:37*

**+Jon Bruno** well it did say that it was craft foam and not for flower related stuff, FloraCraft is the company who made it and the label is Make it fun foam. 


---
**Jon Bruno** *August 24, 2016 14:58*

Cool. i hope it works out for you.

good luck!


---
**Jonathan Davis (Leo Lion)** *August 24, 2016 15:40*

**+Jon Bruno** thank you, now how to figure out how to carve into a crown shape. 


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/XrVPMcEuk47) &mdash; content and formatting may not be reliable*
