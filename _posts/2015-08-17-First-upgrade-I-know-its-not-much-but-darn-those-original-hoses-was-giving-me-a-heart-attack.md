---
layout: post
title: "First upgrade, I know it's not much but darn those original hoses was giving me a heart attack"
date: August 17, 2015 20:20
category: "Modification"
author: "Niclas Jansson"
---
First upgrade, I know it's not much but darn those original hoses was giving me a heart attack.  

![images/ca874f4a930578436ad2ddf8fc193c70.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca874f4a930578436ad2ddf8fc193c70.jpeg)



**"Niclas Jansson"**

---
---
**Sean Cherven** *August 17, 2015 20:36*

I like your "duct tape" job. Pun intended, lol


---
**Niclas Jansson** *August 17, 2015 21:09*

Haha, well, the factory fan was not that good of a fit, so I made a hillbilly fix :P


---
**Haotian Laser** *August 18, 2015 07:27*

hi, you upgrade K40 machine? We are manufacturer for K40, anything need our help? My email is amber@htlasercnc.com


---
**Stephane Buisson** *August 18, 2015 08:52*

**+Haotian Laser**  We know your Email by now you publish it several time. Please keep your post in a constructive way to the forum (with solutions) and not looking like spam. you do not need to post your email any more. I did block your 2 other posts today for that reason. 


---
**Haotian Laser** *August 18, 2015 09:02*

**+Stephane Buisson** ok, thanks for your kindly remind.


---
**I Laser** *August 18, 2015 09:58*

I thought the tubes were bent like that by design, to stop too much pressure reaching the tube. (kidding!)


---
*Imported from [Google+](https://plus.google.com/105892779133697678832/posts/WuEga26Q2Td) &mdash; content and formatting may not be reliable*
