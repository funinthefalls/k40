---
layout: post
title: "Shared on June 10, 2016 02:42...\n"
date: June 10, 2016 02:42
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
#phonetattoo

![images/b2f48484972329c6dafd6511bafcc787.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2f48484972329c6dafd6511bafcc787.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Derek Schuetz** *June 10, 2016 02:51*

I'm so tempted to do this when I'm 100% sure it works perfectly


---
**Ariel Yahni (UniKpty)** *June 10, 2016 02:53*

**+Derek Schuetz** Its perfect. The details is amazing. Its 15mm x 30mm


---
**Simon Dyjas** *July 18, 2016 21:55*

that's brave:)


---
**Ariel Yahni (UniKpty)** *July 18, 2016 21:58*

**+Simon Dyjas**​ no harm there it's a very quick and low power. Phone is working perfectly


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/3es7iaKfFyQ) &mdash; content and formatting may not be reliable*
