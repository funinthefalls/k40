---
layout: post
title: "Hey everyone, Just purchased a smoothie board 3x and I am ready to upgrade, I notice that most of the images from the smoothie conversion are no longer available on their site"
date: February 13, 2018 19:01
category: "Discussion"
author: "Jason S"
---
Hey everyone, Just purchased a smoothie board 3x and I am ready to upgrade, I notice that most of the images from the smoothie conversion are no longer available on their site. My board does not have the large and small mosfets that most of the guides refer to.

 does anyone else have a clear wiring diagram? I am using a separate 5v power supply and mechanical end stops if that helps. 





**"Jason S"**

---
---
**Martin W** *February 14, 2018 10:34*

Hey, so you have 2 small mosfets ;) just get a ground from the Power supply to the smoothieboard and wire the PWM to the PSU from the smoothie.


---
**Martin W** *February 14, 2018 10:34*

![images/8911cfce256a3b80ac9efeb82231142c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8911cfce256a3b80ac9efeb82231142c.jpeg)


---
**Don Kleinschnitz Jr.** *February 14, 2018 16:51*

You might consider posting on the smoothie forum to get the right schematic. I have not looked at the 3X but there are other mosfets you can use.


---
**Martin W** *February 14, 2018 17:14*

**+Don Kleinschnitz** yes, the 3X has one PWM0 so it might be like the same wiring as the 4X/5X. :)




---
**Jason S** *February 15, 2018 00:32*

thank you guys, I will try wiring it with PWM0 and also post this to the smoothie forum as well just to double check. 


---
**Don Kleinschnitz Jr.** *February 15, 2018 00:40*

**+Jason S** Make sure that you connect to the drain of whatever mosfet you choose. Usually the Px.x - connection.


---
*Imported from [Google+](https://plus.google.com/105990581470257048411/posts/7CbUMv9gPsU) &mdash; content and formatting may not be reliable*
