---
layout: post
title: "Im sure this is a horrible idea but what are you thought about having a k40 laser in a bedroom"
date: February 29, 2016 00:35
category: "Discussion"
author: "Bharat Katyal"
---
Im sure this is a horrible idea but what are you thought about having a k40 laser in a bedroom. Im in a college APT. I only cut 2mm acrylic and basically run for 1 hour a day. I need tips for proper ventilation for sure before I start





**"Bharat Katyal"**

---
---
**3D Laser** *February 29, 2016 00:54*

I think it would be more annoying than anything and cutting acrylic can smell really bad if not vented properly (found this out the hard way). I would place it as close to the Window as possible replace the crappy blue tube with something better.  And maybe replace the fan something better. I have mine in the basement now that I moved the machine closer to my exhaust outlet I hardly smell anything and I still have the stock fan granted I use painters table to seal all around to to help as well


---
**Jim Hatch** *February 29, 2016 01:22*

Cut a piece of plywood or acrylic :-) for a hose outlet, get one of those dryer vent thru-the-wall fittings so you can mount that in the plywood. Cut the plywood to fit the window (if you've got double-hung windows that's great because you can wedge the plywood in the bottom of the window like an air conditioner). Then when you want to use the laser just hook the hose to the vent fitting and you'll vent the smells outside. Of course you'll want to seal around the exhaust fan housing and if you can get more air to flow (either bump the exhaust fan size or add intake fans) it'll leave less to smell.


---
**The Technology Channel** *February 29, 2016 03:10*

Also Seal the door..


---
**Bharat Katyal** *February 29, 2016 03:16*

I basically need a vacuum in the middle. I can't have a permanent solution since I have to move it around. I want to stick something to the black of it to seal it and the simply attach a tube to it. Do you get where I'm going with this? I basically need a fan or vacuum in the middle but I'm not sure where to find a good solution 


---
**Bharat Katyal** *February 29, 2016 03:19*

[http://imgur.com/rTZS0sy](http://imgur.com/rTZS0sy)



I have my laser short distance from window.... But my vent is currently a bathroom one I keep close to it


---
**Bharat Katyal** *February 29, 2016 03:23*

Also Ive heard marine coolant is good option for cooling.. any opinions?


---
**Richard Taylor** *February 29, 2016 07:36*

I use an in-line duct fan (search hydroponics for cheap ones!) cooling is a closed loop system using a double 120mm fan radiator designed for pc cooling. Works well for approx. 70-80% duty cycle depending on the ambient temperature.


---
**Ben Walker** *February 29, 2016 12:35*

I used this [http://amzn.com/B005KMTYFK](http://amzn.com/B005KMTYFK) at the window with a 3.25 x 10" exhaust cap (allows the built in window locks to engage and has a flap to prevent birds and wasps from setting up shop in the laser!) and some connecting pipes.  I tried to use the 6 inch semi rigid hose but that ended up being too unweildy.  The 4 inch vinyl (the silver version) seems to evacuate the space very well.  Before I did all this I was suffering with that lingering odor (I basically cut wood) and the haze.  Don't need to overdo it on the fan - I picked up one of those 8inch 1800 CFM fans and its in the return pile now.  The size of the hose is more important that that rating.  All totaled I spent less than 60USD and there is NO ODOR.  I even cut some cast acrylic (I think the protective film is really nasty smelling) and no odors  at all.  guests have no idea I am cutting up a storm unless they go to the garden.  


---
**Jim Root** *February 29, 2016 14:32*

 I'm not sure what marine coolant is I just use distilled water with a little automotive antifreeze.  I tried RV antifreeze, the problem with it is that it does not stop mold growth. 


---
**Bharat Katyal** *February 29, 2016 18:46*

Do you think the 4" would be sufficient for me?the cutter is literally next to window.



Ive also been trying to make a seal in the gaps of the cutter, I tried using some type of foam with adhesive backing but it didn't hold. Will try to see if I can find better solution


---
**Jim Hatch** *February 29, 2016 18:52*

**+Bharat K**​ I used rubber window weatherstripping that's sold here in the U.S. at Home Depot. I think I used 3/8”. It's a self-stick strip in a roll about 10 or 15ft long. I placed a strip around the four sides of the plastic housing for the stock exhaust and the slid it into the channels on the back of the machine. It was a snug fit. For the top where the stock housing extends taller than the curve of the cabinet back I laid on another strip to cover the gap that remained. Worked great. They sell larger & smaller versions as well as foam vs rubber so it's just a matter of getting the right one.


---
**Ben Walker** *March 02, 2016 12:54*

If you can get away with the 6-8 inch venting then that does create a great deal of draw.  I am saying in my experience I had a TON of complaints about the lingering odor.  My window is within 3 foot of the cutter and now there is zero odor or haze.  You don't know the cutter is running or even in the house unless you are hovering directly over it.  I am impressed with my venting skills.  lol.  I also cut a rectangle of wood with a 4 inch hole and fed the hose through it and mounted it flush against the back of the machine and used a little duct tape to seal it.  When I vape even that is getting sucked through every crack int he machine and out to the garden as well.  win win win.  


---
*Imported from [Google+](https://plus.google.com/109809891684909604662/posts/GJc92qwNj7U) &mdash; content and formatting may not be reliable*
