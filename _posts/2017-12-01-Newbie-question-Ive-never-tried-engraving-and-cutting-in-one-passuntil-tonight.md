---
layout: post
title: "Newbie question. I've never tried engraving and cutting in one pass...until tonight"
date: December 01, 2017 03:13
category: "Original software and hardware issues"
author: "Chris Kehn"
---
Newbie question.



I've never tried engraving and cutting in one pass...until tonight. It seems like something is off in the CorelDRAW12 software with the plugin. For whatever reason, what I see on the screen doesn't come out in the K40. You can see it in the two pictures. The text is in different locations. 



Can anyone explain what is happening?







![images/e8d55799d54bb24085807e9428ea2e0d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8d55799d54bb24085807e9428ea2e0d.jpeg)
![images/321cbfae3d90ab94bad449e0cfdd4b1c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/321cbfae3d90ab94bad449e0cfdd4b1c.jpeg)

**"Chris Kehn"**

---
---
**Alex Raguini** *December 01, 2017 04:36*

These are on two separate layers.  If memory serves, you need to put a very small "." in the upper left corner of each layer registered to each other.  This locks each layer on the same spot.  Otherwise, laserdraw will move the layers to the left and mess up the registration.




---
**Chris Kehn** *December 01, 2017 13:58*

I think your right. Last night I came across a website basically explaining the same technique used to keep the registration correct for the different layers. 



I'm thinking about dumping the LaserDraw and going with K40 Whisperer. It seems much easier to use and less likely to give me headaches in the future.






---
**Alex Raguini** *December 01, 2017 23:42*

There is K40 Whisperer, LaserWeb, and a new one to be released in January called LightBurn.  Anything is better than the stock  controller board and the included Laserdraw as it is too limiting.






---
**BEN 3D** *December 02, 2017 14:10*

LaserWeb does not support the onstock, K40 Whisperer is good and you should use Inkscape to create svg files. Take a look to the k40 whisperer totorials on youtube.


---
**Martin O'Hanlon** *December 02, 2017 20:16*

What **+Alex Raguini** says. as weird as it sounds, it always works!


---
*Imported from [Google+](https://plus.google.com/108027001410327769629/posts/YvsQ7E488Gp) &mdash; content and formatting may not be reliable*
