---
layout: post
title: "My family has a whiteboard calendar on the refrigerator that we use to keep track of things that everyone is doing during the month"
date: March 06, 2017 02:49
category: "Object produced with laser"
author: "Ned Hill"
---
My family has a whiteboard calendar on the refrigerator that we use to keep track of things that everyone is doing during the month.  It's also magnetic, so starting this past Nov I started making these Holiday themed magnets out of 1/8" alder.  They are generally around 25mm (1in) on a side for most of them.  

![images/ac8dddf0c4aa87af9174690c6708a017.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ac8dddf0c4aa87af9174690c6708a017.png)



**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 06, 2017 03:16*

Very nice & colourful. That's a cool idea, unfortunately my fridge refuses to let magnets stick on it (even neodymiums).


---
**Ashley M. Kirchner [Norym]** *March 06, 2017 03:35*

Nice! Very detailed painting.


---
**Robert Selvey** *March 07, 2017 00:42*

Nice, do you use 50.8 or a 38.1 lens ?




---
**Ned Hill** *March 07, 2017 00:44*

50.8


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/Zvo2bPrVwJ1) &mdash; content and formatting may not be reliable*
