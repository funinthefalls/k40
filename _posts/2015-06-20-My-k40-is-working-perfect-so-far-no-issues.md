---
layout: post
title: "My k40 is working perfect so far no issues"
date: June 20, 2015 18:19
category: "Object produced with laser"
author: "Venuto Woodwork"
---
My k40 is working perfect so far no issues. I'm going to be using it to engrave tie clips that I make.

![images/ef9705b7b2dabecd0d0b9bbcb1c7e3d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef9705b7b2dabecd0d0b9bbcb1c7e3d7.jpeg)



**"Venuto Woodwork"**

---
---
**Henner Zeller** *June 20, 2015 19:16*

Looks nice!


---
*Imported from [Google+](https://plus.google.com/112529181035283151172/posts/9yUtj7NXKhE) &mdash; content and formatting may not be reliable*
