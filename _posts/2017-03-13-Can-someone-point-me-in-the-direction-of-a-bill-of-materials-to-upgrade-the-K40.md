---
layout: post
title: "Can someone point me in the direction of a bill of materials to upgrade the K40 ?"
date: March 13, 2017 20:23
category: "Discussion"
author: "Lucian Depold"
---
Can someone point me in the direction of a bill of materials to upgrade the K40 ?





**"Lucian Depold"**

---
---
**Bob Buechler** *March 13, 2017 20:31*

There isn't one source for something like that, and K40s can be upgraded a few different ways to suit the user's specific needs. 



Can you share some of your goals for the K40? The community will be better able to guide you from there.


---
**Lucian Depold** *March 13, 2017 20:38*

Yes, of course. 

I would like to cut soft foams and acrylic sheets. Right now i am using a stepcraft cnc with a 3.8W laser from jtechphotonics to do that, but the cutting is painfully slow. I  would prefer an upgrade involving a ramps & arduino, because i could reuse several parts from my old 3d printer.


---
**Lucian Depold** *March 13, 2017 20:42*

i would like to have a machine that is safe & reliable in the end,if possible...


---
**Don Kleinschnitz Jr.** *March 13, 2017 20:46*

**+Lucian Depold** not a single bill of materials per se, but most everything you need to understand an upgrade.

[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Lucian Depold** *March 13, 2017 21:03*

thank you !!




---
**Stephane Buisson** *March 13, 2017 23:18*

Welcome in the community **+Lucian Depold**.

My advice would be to go for the simplest K40 (the one with the pot) and a c3d board

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



will save you a lot of effort and a lot more powerful than Arduino. (in fact the good way is to choose first the right software 4 U like Visicut or Laserweb4 then paired the board, screen, ethernet adapter).

other mods are simple and inexpensive, (air assist, honeycomb bed).

So about 180 $ on top of the k40 is your budget. ( with a little compressor for air assist would be another good addition so maybe 250$).


---
**David Komando** *March 14, 2017 04:15*

Another good resource:

[lightobject.com - D/K40 Upgrade Parts](http://www.lightobject.com/mobile/DK40-Upgrade-Parts-C68.aspx)


---
**Lucian Depold** *March 14, 2017 08:38*

@stephane thank you for the notice on cohesion3d, knew only about the lightobject x7 mod...






---
*Imported from [Google+](https://plus.google.com/105895976959355796398/posts/DyKVwG5Hgfi) &mdash; content and formatting may not be reliable*
