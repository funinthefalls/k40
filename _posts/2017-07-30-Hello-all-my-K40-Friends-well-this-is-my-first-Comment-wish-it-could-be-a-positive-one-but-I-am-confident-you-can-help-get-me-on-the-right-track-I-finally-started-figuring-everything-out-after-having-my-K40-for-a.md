---
layout: post
title: "Hello all my K40 Friends :-) well this is my first Comment :-) wish it could be a positive one, but I am confident you can help get me on the right track, I finally started figuring everything out after having my K40 for a"
date: July 30, 2017 02:14
category: "Discussion"
author: "Lawrence Kelley"
---
Hello all my K40 Friends  :-)  well this is my first Comment  :-)  wish it could be a positive one, but I am confident you can help get me on the right track, I finally started figuring everything out after having my K40 for a few weeks, and today, right in the middle of a Project, my Laser stopped, the machine kept running, but no Laser, water running good, cool, fans on, air assist, but still no laser ??  checked everything out, seen no cracks and no light inside, where is the best place to purchase a replacement Laser tube ??  and are they hard to replace ??

  And most importantly of all, thank you all so much in advance for all the post and great information you all share  :-)   respectfully  Doug







**"Lawrence Kelley"**

---
---
**HalfNormal** *July 30, 2017 21:13*

It could be the tube or power supply.  Check out Don's blog for a lot of help in troubleshooting.


---
**Don Kleinschnitz Jr.** *August 01, 2017 14:17*

1. Is the LED down on the LPS "ON" when the power is on to the machine?

2. When you push the test button located down on the LPS does the laser fire? <b>Caution</b>: this test button bypasses all interlocks and the laser will fire if the LPS and Laser is good. Insure all covers are closed.

3. Post picture of your LPS.




---
**Lawrence Kelley** *August 02, 2017 01:47*

I will send a photo in the morning, when the machine is on, the Green light on the LPS is on,,  pushing the Test button does not do anything, Also the Fan in the LPS is not on ??  maybe that means something  :-) I cleaned all the mirrors, still nothing ??


---
**Don Kleinschnitz Jr.** *August 02, 2017 14:42*

**+Lawrence Kelley** fan should be on if the 12v section of the supply is working. If the 12V is dead the LPS will not work, sounds like a bad supply. 

Will continue when we see picture of supply....


---
**Lawrence Kelley** *August 04, 2017 03:24*

Here's the photo, Green light on, Fan not working



![images/97d740eb99f23e0efa56969c74e2d059.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97d740eb99f23e0efa56969c74e2d059.jpeg)


---
**Don Kleinschnitz Jr.** *August 04, 2017 12:53*

**+Lawrence Kelley** Whoops made a misteak, the fan runs on 24VDC not 12VDC. So this suggests the fan is bad or the 24V is not working. 



Is it possible the fan is disconnected inside the supply?



I am somewhat perplexed in that if 24vdc was not working the machine would not be driving motors.



Are you handy with a DVM? We can measure the DC voltages on the LPS as a start but I expect they are ok?



[photos.google.com - New photo by Don Kleinschnitz](https://goo.gl/photos/YAuXvTnvZQKMSy6i9)




---
*Imported from [Google+](https://plus.google.com/108241240546580115383/posts/Ck5ToJ8HUvs) &mdash; content and formatting may not be reliable*
