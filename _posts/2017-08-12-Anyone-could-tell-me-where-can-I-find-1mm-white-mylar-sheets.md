---
layout: post
title: "Anyone could tell me where can I find 1mm white mylar sheets?"
date: August 12, 2017 16:23
category: "Materials and settings"
author: "Jorge Robles"
---
Anyone could tell me where can I find  1mm white mylar sheets? Thanks





**"Jorge Robles"**

---
---
**Martin Dillon** *August 13, 2017 00:10*

Do you want opaque white or translucent?  Mylar for drafting is white but you can see through it.


---
**Martin Dillon** *August 13, 2017 00:13*

Sorry, I was thinking mil not mm.  That is thick mylar.


---
**Jorge Robles** *August 13, 2017 06:54*

I'm looking for opaque white if possible. 0.05 inches :)


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/YL9bFGuGpvJ) &mdash; content and formatting may not be reliable*
