---
layout: post
title: "Hi all, just wanted to let you know that I just officially launched a dedicated LAYZOR website"
date: July 12, 2018 16:40
category: "External links&#x3a; Blog, forum, etc"
author: "Frederik Deryckere"
---
Hi all,

just wanted to let you know that I just officially launched a dedicated LAYZOR website.



For those that don't know, the LAYZOR is a complete rebuild of a K40, but I recommend you read the full story on my blogpost:

[www.manmademayhem.com/?p=3440](http://www.manmademayhem.com/?p=3440)

and the website itself:

[www.manmademayhem.com/layzor](http://www.manmademayhem.com/layzor)



![images/50ee2d0aeb47b72998c76977ada2179b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/50ee2d0aeb47b72998c76977ada2179b.jpeg)



**"Frederik Deryckere"**

---
---
**Chris Hurley** *July 12, 2018 18:34*

So will you be selling compete units? 


---
**Frederik Deryckere** *July 12, 2018 19:22*

I will sell kits if there is enough interest. I may post some sort of waiting list and when X people sign on I can make a batch.



if you mean completely built units, no that would be nuts for me ;-)


---
**James Rivera** *July 13, 2018 05:29*

Feed through slot of 20mm? A bit tiny! 😉 (you have a typo)


---
**Joe Alexander** *July 13, 2018 05:53*

**+James Rivera** I'd imagine that is in reference to the height of the slot, not the width(as it says full width). Plus how often will you use boards thicker than 20mm? :P


---
**James Rivera** *July 13, 2018 06:03*

**+Joe Alexander** Doh! You’re probably right! 🤦‍♂️ (it’s late and I’m tired)


---
**Frederik Deryckere** *July 13, 2018 07:52*

that is correct ;-)


---
**Jeff Kes** *July 14, 2018 14:56*

Will you be sharing the design files or only selling the kits?


---
**Frederik Deryckere** *July 14, 2018 15:12*

You can pre-order the plans here:

[manmademayhem.com - Shop](https://manmademayhem.com/layzor/myshop/#forsale)


---
**Jeff Kes** *July 15, 2018 02:38*

Cool thanks for the response. 


---
**Tony Sobczak** *July 15, 2018 15:22*

Following 


---
**Jeff Kes** *July 15, 2018 16:26*

Can you provide a STEP or parasolids file also?  I opened it in SolidWorks and all I got was the parts and no panels or table. I’m sure this is a translation issue. Thanks


---
**Frederik Deryckere** *July 16, 2018 17:55*

Hey, I'll see what I can do, but I tried it just a moment ago and  my laptop ran out of memory and crashed. The file is extremely demanding even for my high-end pc...


---
**Jeff Kes** *July 20, 2018 20:40*

**+Frederik Deryckere** any luck?


---
**Frederik Deryckere** *July 22, 2018 10:58*

not yet, I'll have to try on my desktop pc as my laptop isn't working out... Give me a few days ;-)


---
**Jeff Kes** *July 22, 2018 21:48*

**+Frederik Deryckere** no problem 


---
**Frederik Deryckere** *July 24, 2018 19:11*

Hey **+Jeff Kes**, I managed to export (turns out Rhino defaulted to opening that specific file version in 32bit, which caused the RAM issue) But alas not everything got exported, and opening the step file took about half an hour building the meshes. Then when opened it ran terribly. So it really needs to be opened in native Rhino format or else it just gets too demanding for even a well specced pc. Sorry but you'll need to find a workaround. I tried ;-)




---
**Rene Munsch** *September 13, 2018 18:33*

just a question: Your Plan is really cheap. But, what will the Lazor cost at the end, with all suggested components?




---
**Frederik Deryckere** *September 14, 2018 00:16*

**+Rene Munsch**  i suggest reading the faq on my website. Can be done for about 500 euro but depends on what tools you have access to. 


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/QgzTu7Nirgq) &mdash; content and formatting may not be reliable*
