---
layout: post
title: "My turn. I was preparing a kit to ship the other day and powered on the laser tube to burn the now traditional bat (if you bought one, you've seen my mascot) and there was a Poof from the rear of the laser (I have to plead"
date: September 20, 2016 09:18
category: "Discussion"
author: "Scott Marshall"
---
My turn. 

I was preparing a kit to ship the other day and powered on the laser tube to burn the now traditional bat (if you bought one, you've seen my mascot) and there was a Poof from the rear of the laser (I have to plead guilty on having the rear cover off as I'm testing pumps) and out flies a mortally wounded giant moth.



He generated quite the orange ball of fire and an audible pop.  It was hot in my lab and I had the windows open a bit the evening before, and he must have liked the warm laser tube (water is warm from pump testing) and it seems he moved into the recess in the business end of the tube. little did he know it was fixing to get a bit warmer...



Poof! Worthy of Starwars if properly filmed. I'm sure... Death Star fires on Mothra... 



Anyways, directorial dreams aside,  It was still lasing, but weak (but at near normal current - I burn bats on drawing paper so it's only just firing, maybe 2ma) - turning it up didn't help much. A quick look revealed something didn't look right, but I didn't feel well and just now am looking into it.



I have NO idea how a moth in the beam could do this, but the mirror has come loose and is tilted in the tube which it's mounted. The edge of the cylinder is blued from heat. It had plenty of water flowing, I'm running a pump large enough for any 3 or 4 of these lasers, throttled down. I had a new PE-1 on it a few hours before the boom and all was working well. I was increasing tubing to 3/8" to see what gains could be had from that change, and switched back to my "adjustable" S1200 bypass setup. It was flowing about 3gpm when it happened, and the 1st thing I checked was the hot end and it was fully flooded.



Anyway I just ordered another Laser (Hoping for the cool red and gray one in the picture) from Globalfreeshipping for $315. They have tubes for $135, but    I'm hoping to try my hand at making "how to" upgrade videos - which is probably more than I have time for, realistically. If it ends up in the budget or lack thereof, I'll probably freshen up the ole blue box, stencil a moth on her door, and keep right on running her for testing duty. 



Lots of new goodies in the works, available now are the "Quick Disconnect" - a switchbox that goes on your K40 and allows automatic/manual disconnection of your USB so you don't have to keep unplugging and switching off the K40 to do a reboot. The manual version is $24.95, the man/auto version is $29.95. $5 shipping. Also to go with it is a nice double shielded USB cable 10 feet long transparent insulation like 'hospital grade' devices, with Ferrite on both ends. Arguable the quietest USB cable for a fair price anywhere. 10 bucks. Buy both for $34.95

That stuff will be up on the website soon, as will my power supplies with ACTUAL voltage and current displays (replaces panel insert) in your choice of digital (choice of backlight color LCD) or very cool retro corner hung analog meters.



I want to thank everyone who has had faith in me and ordered a kit, I hope you are all very satisfied with them, and I will continue to provide the best service I can manage, given my health slowing me down from time to time. Things aren't moving as fast as I'd like, but at least I'm working again after almost 20 years being sidelined. 

Thanks Guys, This group is a shining star on the internet.



Scott



I've got some good shots of the mirror/ground electrode, but seem to have lost my ability to post photos. It's not size related. I also have a Marked up K40 PSU photo that answers all the How to connect questions, but can't post that either. 

I click on the photo, the wheel goes around, stops, and it goes right back to the text only post. Any ideas?





**"Scott Marshall"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 20, 2016 10:23*

Hey Scott, try posting the photos into Google Drive or Google Photos first, then see if you can link from there. I'll step you through it on email if you need a hand.


---
**Scott Marshall** *September 20, 2016 10:23*

Now that I have it out, it's not the mirror that's tipped, it's just discoloration where the electrode tube touches the glass. Mirrors look good, seeing 20+ bounces looking down the output mirror, some sediment in the water jacket. What really surprised me is burn marks on the adhesive tape on the rubber mounting ring (ground end) where it's been arcing through to the mounting strap. The other end has aluminum chips (not rusty) embedded in the rubber and no evidence of arcing. (gotta love the care that goes into these) - I have a mental image of their assembly area I'd best not share.



It seem as if the ground wasn't a good connection (which is very hard to believe) and the tube surface carried a portion of the current to the mounting ring where it arced through the rubber ring, then along under the tape and finally penetrated the tape to the ground strap.



I don't see anything outwardly wrong with it. The discoloration is chemical/electrostatic, NOT heat, and the mirrors are fine. Guess I clean, re-install and wait for my new, RED laser to show up. If this was having problems due to a bad factory ground wrap (looked pretty iffy, silicone was gummy and silicone should never be gummy/sticky), I just bought a laser for no reason. Should have pulled the tube 1st. Maybe the Moth was in the electrical path, not the laser path. That would make sense, he exploded pretty hard for just being lasered. I high voltage arc much more fits the sound and light show.

The moth may been on the tube surface, in the path of the high voltage leak, Think 400watt bug zapper. Ouch...



Come to think of it, that moth (or his pieces) is the poster child for why you don't put your hands in a K40, even places where there's no (or so you think) high voltage. If a person (me) had touched the dirty tube surface instead of an insect....



It's always something weird with me....



Scott



Well, if it works, I'll at least have a usable laser while my crash test dummy one is burning bats...


---
**Scott Marshall** *September 20, 2016 10:32*

I wiped out google drive and the whole deal when I found it was sharing every photo on my machine with everyone on the planet. Glad my life is so boring, mostly machinery pictures.....



I USED to be able to just drag them into the post and they would work, now not even a little bitmap will work. I'm, thinking I changed a setting? I've been at it for a long while, 630am here, been here most of the night. Did a write up on QD and Cables, was going to add pioctures and send to you, but got side tracked.



If you're still here, I'll be back in an hour or so, need to take meds and have a break.



Thanks, Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 20, 2016 10:39*

**+Scott Marshall** Yeah I'll still be floating around for a few more hours at least. I'm just doing some screenshots for what you can do to test if it works this way. I'll swing your way via email.


---
**Ariel Yahni (UniKpty)** *September 20, 2016 11:57*

Oh man, you director skill are very good


---
**Alex Krause** *September 21, 2016 03:18*

**+Scott Marshall**​ I came in from getting some stock that I had in the garage... little did I know I had a stow away on my pants... the minute I stepped in front of my laser the culprit began to attack violently beating his wings in an attempt to suicide into the laser... I think it's a conspiracy I franticly swatted at the enemy #TheMothSquad


---
**Scott Marshall** *September 21, 2016 06:07*

**+Alex Krause** 

ah ha, a consipiracy! I knew they were in it with the mud wasps that ruin my air tools, plug my weedeater muffler and then move on to fuel tank vents.

I once repaired a projector damaged by a moth, and back in the tube tv days, we'd get TVs infested with cockroaches in at the repair shop.



I'm giving the tube a good cleaning, and fresh tubing links.  I'm going to put on some fresh insulators and will give it another try. I'm hoping it was just a bad factory connection. It's amazing just how sloppily this tube was installed. What carried the leakage during it's last couple of inches to ground was a piece of white medical adhesive tape used to shim the tube up on the discharge end. they didn't even bother to wipe away the drill shavings before applying it,  then both the  rubber rings were wrapped with about 4' of electrical tape. The rubber rings are butyl foam, which is a know moisture attractor. 



While I was cleaning off the epoxy preserved fingerprints with a scalpel, I discovered the tube has 2 'false starts' where it is scored around it's circumference. It looks like they scored it to heat cut it, realized it was scored wrong, and just left the incorrect score in place and moved to where it was rally supposed to be cut. Twice - there's 2 score marks, along with several other deep scratches. I better handle it VERY carefully.



I'm really amazed it worked at all. It also explains the in transit failure rate of the tubes, to some degree anyway.



 I've got good photos of the leakage path and will put them up once I figure out why my photos posting isn't working.

I'm not sure If I can photographically reproduce the score lines, not that it matters really at this point.



film @ 11 ... maybe.



Scott


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/XmaHgmXBAzc) &mdash; content and formatting may not be reliable*
