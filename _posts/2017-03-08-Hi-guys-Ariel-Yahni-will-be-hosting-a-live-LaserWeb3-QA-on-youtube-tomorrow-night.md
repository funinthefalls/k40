---
layout: post
title: "Hi guys, Ariel Yahni will be hosting a live LaserWeb3 Q&A on youtube tomorrow night"
date: March 08, 2017 01:57
category: "Software"
author: "Chuck Comito"
---
Hi guys, Ariel Yahni will be hosting a live LaserWeb3 Q&A on youtube tomorrow night. This is not an end all be all tutorial but will help get our feet wet with the software should you be interested. The link to the live stream is: 
{% include youtubePlayer.html id="41tcIys7t20" %}
[https://www.youtube.com/watch?v=41tcIys7t20](https://www.youtube.com/watch?v=41tcIys7t20) and will start roughly 9pm eastern standard time on Wednesday March 7th. If you have any questions that you've been itching to ask have them ready and Ariel will do his best to answer. At the end of the basic Q&A he plans to introduce us all to LaserWeb4 (which isn't released yet). Thanks and hope to see you all online tomorrow night.





**"Chuck Comito"**

---


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/SMv94inJuKD) &mdash; content and formatting may not be reliable*
