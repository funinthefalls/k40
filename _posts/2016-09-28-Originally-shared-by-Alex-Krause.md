---
layout: post
title: "Originally shared by Alex Krause"
date: September 28, 2016 18:01
category: "Modification"
author: "Alex Krause"
---
<b>Originally shared by Alex Krause</b>





**"Alex Krause"**

---
---
**Don Kleinschnitz Jr.** *September 28, 2016 18:39*

Excellent video. Will the gantry rotate up and to the left to get to the x endstop?


---
**James Rivera** *September 29, 2016 04:21*

Concerning that the belt teeth wore out so quickly.


---
**Alex Krause** *September 29, 2016 04:28*

**+James Rivera**​ I have had alot of hours on this and had a few oops moments when I first setup laserweb with endstops and an error that I found in LW when an object was imported to the upper left when I first started I had some over travels where the head got stuck and the steppers kept running


---
**James Rivera** *September 29, 2016 05:20*

Ahhh...ok, they got chewed up.


---
**Er Brad** *September 29, 2016 09:22*

**+Alex Krause** how did u stop the over travel l have the same problem


---
**Alex Krause** *September 29, 2016 13:14*

**+Er Brad**​ you are still experiencing over travels?


---
**Er Brad** *September 29, 2016 19:21*

Yes still wants to goo outside the bound


---
**Alex Krause** *September 29, 2016 19:22*

Are you running smoothie or grbl board


---
**Er Brad** *September 29, 2016 21:01*

Smoothie and add on board from scott


---
**Alex Krause** *September 29, 2016 21:07*

Can you post a copy of your config file on Google drive or onedrive


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/FdUZ6Vh7Emz) &mdash; content and formatting may not be reliable*
