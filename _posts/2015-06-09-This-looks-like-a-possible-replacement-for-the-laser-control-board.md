---
layout: post
title: "This looks like a possible replacement for the laser control board"
date: June 09, 2015 06:08
category: "Discussion"
author: "David Richards (djrm)"
---
This looks like a possible replacement for the laser control board. If only the connectors were correct.





**"David Richards (djrm)"**

---
---
**Troy Baverstock** *June 09, 2015 06:44*

How good is the arduino control software for laser cutters these days?


---
**Mauro Manco (Exilaus)** *June 09, 2015 06:57*

sorry david for less price buy ramps + arduino + lcd2004 with sd card....and make real good dsp mod for your k40 and remove pc from laser too. .. normaly need same this need add a converter from flat to pins.


---
**David Richards (djrm)** *June 09, 2015 06:59*

That is a good question, I dont know the answer. There is  grbl and laseraur software available but I have not tried them. I am hoping to upgrade to an arm based system myself when I see suitable open source software / hardware which has both raster and vector capabilities, in the mean time I'll continue with Corel Laser. I know the arduino software struggles to keep up on my 3d printer and I'd like to replace that with faster processing too.


---
**Troy Baverstock** *June 09, 2015 07:34*

This looks cheaper to me, the ramps, drivers, arduino, lcd setup cost me about 45$﻿


---
**Mauro Manco (Exilaus)** *June 09, 2015 07:55*

27$ [http://goo.gl/5wVo5L](http://goo.gl/5wVo5L)


---
**Muccaneer von München** *June 09, 2015 08:19*

does it have a laser diode driver on board? 


---
**Muccaneer von München** *June 09, 2015 08:25*

**+Troy Baverstock** many diy laser cutters are just using the universal gcode sender to stream gcode to the arduino. Then there is the lasersaur project which has a web interface, but I don't know in which state this currently is. VisiCut is a java program aiming to be a laser cutter software for every laser cutter. Finally allow me to point to our own **+Mr Beam** software, an open source web interface for grbl driven lasercutters. 


---
**Troy Baverstock** *June 09, 2015 08:50*

Nice Mauro, worth an experiment at that price. 



Does the k40 just need PWM for laser control or is there more to the story?



Also, how bad is Corel laser vs the alternatives? I've used trotec lasers and yes the software is good, acceleration control, multiple power levels in the one cut file, sequencing etc. Do the alternatives do these things?



Thanks for the heads up on Mr beam, curious to see how it develops


---
**Muccaneer von München** *June 09, 2015 10:18*

**+Troy Baverstock** You're welcome. Development is slowed down right now until tax declaration is done :(


---
**Stephane Buisson** *June 09, 2015 12:47*

one day Visicut will have a modern ARM board and be able to have a driver developped for. **+Arthur Wolf** 


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/EgYkhBZNqkL) &mdash; content and formatting may not be reliable*
