---
layout: post
title: "Here are some pictures of the x and y axis from the Redsail LE400 I am upgrading"
date: April 05, 2016 14:04
category: "Modification"
author: "Anthony Bolgar"
---
Here are some pictures of the x and y axis from the Redsail LE400 I am upgrading. It is so much better than the K40, nice and stiff, very little play in all directions.



![images/0c68aac15c1b4cb813c248850f35177f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c68aac15c1b4cb813c248850f35177f.jpeg)
![images/da6160a4994f6bee3510635d5afdf82a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/da6160a4994f6bee3510635d5afdf82a.jpeg)
![images/baae0043b930122d888eeff4b395e520.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/baae0043b930122d888eeff4b395e520.jpeg)
![images/0431ba4cf8b2d603bac09a8ae6260029.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0431ba4cf8b2d603bac09a8ae6260029.jpeg)

**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *April 05, 2016 14:06*

Notice that it has an adjustable bed, built as part of the structure. When installed there is room behind it, enough that if I slot the back piece of aluminum, I should be able to put a longer belt in and drive it with a stepper, making it power driven instead of manual like originally built.


---
**Anthony Bolgar** *April 05, 2016 14:10*

Usable work area is 300mm X 420mm. Bed travel is 70mm. Going to install an 845mm tube (fits inside the existing tube area) that is rated at 50W (probably really only 40-45W).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:17*

It does look like a much more solid construction than my K40. I like the adjustable bed. It'd definitely be worthwhile adding the stepper if you have the room to do so.


---
**Anthony Bolgar** *April 05, 2016 14:21*

The aluminum extrusions are very beefy and the unit is perfectly square. When installed in the case it is exactly parallel to the laser tube. The tube area of the machine can handle up to an 850mm tube without any modifications. But as I think about it more, I may add a tube extension casing and put a 1000mm tube in.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:23*

**+Anthony Bolgar** They do look a lot sturdier than what the K40 is constructed of. Also looked to be slightly taller in height, so I am curious does it bolt directly to the base of the casing? Unlike how the K40 bolts to metal brackets to lift it higher.


---
**Scott Thorne** *April 05, 2016 14:32*

Man that looks like it's built solid and true...you got lucky with that purchase  **+Anthony Bolgar**.


---
**Anthony Bolgar** *April 05, 2016 14:39*

Still trying to decide what controller I want to install. SO many choices to choose from, but am leaning towards a Smoothieboard (If they ever have stock again in North America) I know I could get a Smoothie clone, but I would rather support Arthur for all his hard work.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:47*

**+Anthony Bolgar** Someone posted recently saying they had contacted the maker & stock would be available again soon. Unless that was you that posted that.


---
**Anthony Bolgar** *April 05, 2016 14:58*

Just chatted with Arthur, he said about 1 week for availability in North America.


---
**Jim Hatch** *April 22, 2016 14:35*

**+Anthony Bolgar** **+Yuusuf Sallahuddin** Ordered my Smoothie last week and got it yesterday. I was thinking I'd swap it out this weekend but I'm tweaking the squaring up of the movable X rail and I don't want to make more than one change at a time :-) so it'll be another week before I move to Smoothie & Laserweb.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 22, 2016 17:11*

**+Jim Hatch** Awesome. Hopefully all goes well for you with the upgrade.


---
**Jim Hatch** *April 22, 2016 17:22*

I am grabbing the info out there on Smoothie and cleaning it up a bit in preparation. I think I"ll get some wiring connectors that will let me keep the existing wiring connectors and convert them to the Smoothie connections - if I don't find them locally I'll make some myself. That way if I have issues I can just unplug the Smoothie board and plug the Nano one back in. I'll be updating Stephanie's doc on the K40 to Smoothie conversion and will post it here and over at Smoothie for the next guy who wants to do it.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/gBQZ6ghnLtz) &mdash; content and formatting may not be reliable*
