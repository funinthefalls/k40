---
layout: post
title: "I am able to put a piece of wood into the K40 and cut or engrave with no issues"
date: December 08, 2016 19:42
category: "Hardware and Laser settings"
author: "Carle Bounds"
---
I am able to put a piece of wood into the K40 and cut or engrave with no issues. My question is if you have like a small piece of precut wood like for a coaster or whatnot. How are you determining exactly where to place it for cutting or engraving?  How are you determining exactly where the laser will start the job? For instance if I want to engrave an image right in the center of a precut piece of wood. It would have to be pretty exact.... I am using laserweb3.﻿





**"Carle Bounds"**

---
---
**greg greene** *December 08, 2016 19:57*

What I did was make a jig - by using the piece that I cut the coaster out of.  I align the original piece with the side of the carriage rails so it can always be placed back in that exact position, then I jog the head into position - marking down the x and y axis readings so I can duplicate it later, then I cut the coaster.  Now I have a jig because any other coaster I cut later from other pieces needs only to be placed into that original piece to go back to the same spot - plus or minus the width of the beam.  


---
**Ned Hill** *December 08, 2016 22:38*

Not using laserweb3 yet, but If it's precut what I would do is create a shape the exact same size as the precut piece and place it at the origin.  Then center your design in that outline.  In your example if I had a round coaster say 90mm in diameter I would create a circle in the program that's 90mm in diameter.  Place the physical piece and the digital piece at the origin and center my engraving image in that circle I created.  May take some playing to make sure it's lined up.  I Usually will create a blank out of cardboard and do a low power test engrave to see if everything looks good first.  I've also used **+greg greene** method as well.


---
**Stephane Buisson** *December 08, 2016 22:50*

with a cam

[github.com - LaserWeb3](https://github.com/LaserWeb/LaserWeb3/wiki/Settings:-Remote-Webcam)


---
**greg greene** *December 08, 2016 22:53*

Lots of ways to do it - what ever will work best for the particular job your doing. I like the Cam set up


---
**Ned Hill** *December 09, 2016 00:57*

Now that's cool **+Stephane Buisson**, I didn't realize that laserweb supported a cam.  Really can't wait for my Cohesion3D to arrive  :)




---
**Stephen Sedgwick** *December 09, 2016 12:48*

**+Stephane Buisson** I was curious if there was a video showing what it looks like through the cam so we can see the real advantage of this kind of setup.  IE what will we be looking at through laserweb looking at the cam etc..


---
**Stephane Buisson** *December 09, 2016 13:02*

I am not operational at the moment, maybe **+Peter van der Walt** or **+Ariel Yahni**   would be able to point you to a video showing that.


---
**Stephen Sedgwick** *December 09, 2016 15:13*

**+Peter van der Walt** where is this button you are speaking of?  on the website?


---
**greg greene** *December 09, 2016 16:09*

Jog menu, there is a line of buttons like  Home, run gcode etc - I believe the rightmost in that line is check outline - it moves the head but doesn't fire the laser


---
**Carle Bounds** *December 09, 2016 16:49*

OK so why are people mounting led diodes to the laser head? Is that some way to tell you where the laser will start cutting or something?


---
*Imported from [Google+](https://plus.google.com/106012823225343259431/posts/fWsbZFCsEAL) &mdash; content and formatting may not be reliable*
