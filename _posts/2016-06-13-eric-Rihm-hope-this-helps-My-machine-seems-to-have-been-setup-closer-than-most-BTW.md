---
layout: post
title: "+eric Rihm hope this helps. My machine seems to have been setup closer than most BTW"
date: June 13, 2016 20:36
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
+eric Rihm hope this helps. 

My machine seems to have been setup closer than most BTW. 

Its closeness to the mirror really doesn't matter but the tube needs to be parallel to the gantry's path. 

I wish mine was further back so I could put an alignment device in that space....

This is the gap between the end glass and the edge of the mirrors circular mount.

![images/2f91e23575a372caab231dc8c1379129.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f91e23575a372caab231dc8c1379129.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**HalfNormal** *June 13, 2016 21:16*

That is about what I have on mine


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 02:48*

Mine is more like 2 inches away from the first mirror.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/6e9byTwsEzL) &mdash; content and formatting may not be reliable*
