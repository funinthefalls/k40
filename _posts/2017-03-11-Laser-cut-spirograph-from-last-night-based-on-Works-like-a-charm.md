---
layout: post
title: "Laser cut spirograph from last night based on Works like a charm"
date: March 11, 2017 08:27
category: "Object produced with laser"
author: "Bernd Peck"
---
Laser cut spirograph from last night based on [http://www.thingiverse.com/thing:322919](http://www.thingiverse.com/thing:322919)



Works like a charm



![images/eef6e418d5c5e99533ea1b7422ec76c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eef6e418d5c5e99533ea1b7422ec76c0.jpeg)
![images/22295dd76a16897b516472b343f9f5e8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/22295dd76a16897b516472b343f9f5e8.jpeg)

**"Bernd Peck"**

---
---
**Anthony Bolgar** *March 11, 2017 09:46*

I made a similar one for my 6 yr old granddaughter last month. She loves playing with it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 11, 2017 10:48*

I remember these from when I was a kid. That's a cool project :)


---
**Mark Brown** *March 11, 2017 13:38*

Oooh, I'm going to have to make one of these eventually.


---
**Anthony Bolgar** *March 11, 2017 13:40*

No time like the present **+Twelve Foot**




---
**Mark Brown** *March 11, 2017 15:58*

**+Anthony Bolgar** I appreciate and agree with the sentiment.  But my shop is unheated and it's currently below freezing, and drops near my "this is too cold to be fun working in" range in the evenings.



Plus my laser currently doesn't have a bed in it.


---
**Mark Brown** *March 11, 2017 16:01*

Seeing this reminded me of something else that I saw a while back, and might make "eventually".



[http://leafpdx.bigcartel.com/product/cycloid-drawing-machine](http://leafpdx.bigcartel.com/product/cycloid-drawing-machine)

[images.bigcartel.com](http://images.bigcartel.com/product_images/161101582/cycloid_cart_P4217642.jpg?auto=format&fit=max&h=1000&w=1000)


---
*Imported from [Google+](https://plus.google.com/100238441539373475210/posts/C1prFUjqsR4) &mdash; content and formatting may not be reliable*
