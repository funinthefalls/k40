---
layout: post
title: "I am using my laser in my garage in SC \"Its hot\" I have the water piped through a mini fridge"
date: September 18, 2016 21:08
category: "Discussion"
author: "Robert Selvey"
---
I am using my laser in my garage in SC "Its hot" I have the water piped through a mini fridge. If I don't run the fridge the water temps rises and the power of the laser drops so I need to cool the water without the condensation buildup. Would  keeping a fan blowing on the laser tube  keep the condensation down ? 





**"Robert Selvey"**

---
---
**Anthony Bolgar** *September 18, 2016 23:40*

It would help to reduce the condensation, as well it will help cool the tube.Just make sure that if you have the tube bay door open, try and protect the tube from things falling on it or bumping into it.


---
**Robert Selvey** *September 19, 2016 11:30*

I have a spare room in my house thinking of moving the laser in there to get it out of the hot garage... What every ones thoughts about having it in the house. Would it be fine as long as I vent it out the window real well ?


---
**Anthony Bolgar** *September 19, 2016 12:08*

I have mine in the house,I just vent it out the window and do not have any fume issues (You can smell it a little, but you will never get perfect extraction)


---
**Stephen Sedgwick** *September 19, 2016 13:36*

They actually make a insulation wrap that can help with some of the condensation....not sure how it would do that close to the laser... as it is a bubble wrap material... so might not be best....


---
**Robert Selvey** *September 19, 2016 13:52*

I think I'm just going to move it in a spare room in the house and vent it real well.


---
**Anthony Bolgar** *September 19, 2016 15:30*

That would be the best thing to do. Just seal up all pipe connections with the aluminum foil tape, do not use duct tape, it works on everything BUT duct work.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/UtUbAAAfAHy) &mdash; content and formatting may not be reliable*
