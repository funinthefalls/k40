---
layout: post
title: "I would like to cut out a section of the bottom of the K40 to allow recessing in my rotary attachment to allow bigger diameter objects to be engraved"
date: June 05, 2016 12:21
category: "Modification"
author: "Anthony Bolgar"
---
I would like to cut out a section of the bottom of the K40 to allow recessing in my rotary attachment to allow bigger diameter objects to be engraved. I want to make a set sheet metal sides to raise up the K40 to allow for this, what gauge sheet metal do you think I will need to support the weight of the K40? (I would make it out of one long piece folded at the corners with a top and bottom lip folded at a 90 degree angle)





**"Anthony Bolgar"**

---
---
**Jim Hatch** *June 05, 2016 13:34*

Can you do it a bit differently where you make a cut out in the table or bench you have the K40 on and then build a shelf under that for the rotary attachment? You could put the shelf on threaded rods that would allow you to raise and lower it like an adjustable Z-table. Then you only need to remove enough of the bottom of the K40 to be able to allow the rotary attachment to drop or raise. The existing sheet metal of the k40 would be fine for continued support - there are a couple of horizontal stiffeners under there now that are outside the laser able area so shouldn't need to be cut for the rotary access hole.



If cutting into the supporting bench isn't something you want to do, you could build a box that gives you the machine support with the same kind of hole built in for the adjustable shelf.


---
**Alex Krause** *June 05, 2016 14:29*

Build a frame under it with Vslot and skin it with some sheet metal


---
**Anthony Bolgar** *June 05, 2016 16:31*

I like the V-slot idea. Thanks **+Alex Krause** 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/TpE8gPptmga) &mdash; content and formatting may not be reliable*
