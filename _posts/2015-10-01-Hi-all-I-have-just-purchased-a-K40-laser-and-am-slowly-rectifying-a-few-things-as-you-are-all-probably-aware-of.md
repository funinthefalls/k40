---
layout: post
title: "Hi all, I have just purchased a K40 laser, and am slowly rectifying a few things as you are all probably aware of"
date: October 01, 2015 08:02
category: "Hardware and Laser settings"
author: "Leanne Purnell"
---
Hi all,

I have just purchased a K40 laser, and am slowly rectifying a few things as you are all probably aware of.

The question I have is I live in Australia, and all of our plugs here are earthed. I was just wanting to know do I still need to use the ground earth wire clip as well. If anyone here from Oz can let me know that would be great.





**"Leanne Purnell"**

---
---
**ThantiK** *October 01, 2015 11:59*

Ask yourself if you'd like to ground out 15kV through all of your home electronics.


---
**Joey Fitzpatrick** *October 02, 2015 02:55*

as long as your earth/ground is good, you should be alright. If you want extra precaution, add the extra ground rod.  You can never have too many grounds


---
**Leanne Purnell** *October 02, 2015 10:55*

Thanks Joey,

yes can never be to cautious, will take your advise and add the extra ground.



Just one more question. what do you recommend for the earthing rod, will copper tube be ok and how thick should the earth wire be.


---
**Joey Fitzpatrick** *October 02, 2015 14:57*

Here in the US, A solid grounding rod is required.  Not sure what the requirements are for your country.  I went to the local home improvement store and asked the salesman what the electrical contractors buy.  


---
*Imported from [Google+](https://plus.google.com/107025904733514724932/posts/1yEDuBPuCMp) &mdash; content and formatting may not be reliable*
