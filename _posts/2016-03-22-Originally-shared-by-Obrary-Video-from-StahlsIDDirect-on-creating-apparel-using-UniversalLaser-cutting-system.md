---
layout: post
title: "Originally shared by Obrary Video from @StahlsIDDirect on creating apparel using @UniversalLaser cutting system"
date: March 22, 2016 17:10
category: "Object produced with laser"
author: "Stephane Buisson"
---
<b>Originally shared by Obrary</b>



Video from @StahlsIDDirect on creating apparel using @UniversalLaser cutting system [http://hubs.ly/H02rwRZ0](http://hubs.ly/H02rwRZ0) 





**"Stephane Buisson"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 22, 2016 20:30*

Good share Stephane. I've been considering testing this with my laser cutter & my heat transfer paper (that you print onto with inkjet). The main reason I stopped making shirts using it was I hated cutting it out with scalpel. Took forever. That laser cutter can minimise the time required to do this. Thanks for the share.


---
**Anthony Bolgar** *March 23, 2016 00:01*

I find it easiest jut to use my 54" vinyl plotter. I ended up buying one when I investigated getting my business info onto my work van. It ended up being half the price of paying someone to do it. The plotter and vinyl supplies paid for themselves by making my own signs, and now everything I do with it makes me money without needing to outsource any of it.


---
**Randy Randleson** *March 23, 2016 16:12*

I would find it easier too, but just think NO WEEDING, lol


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/AezcDqWzWwN) &mdash; content and formatting may not be reliable*
