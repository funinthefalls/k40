---
layout: post
title: "A friend wanted something simple for the members of a band he plays with"
date: December 01, 2016 20:17
category: "Object produced with laser"
author: "Jeff Johnson"
---
A friend wanted something simple for the members of a band he plays with. He sent me a list of the instruments they played and this is what I came up with. This is 1/8 plywood with a mahogany veneer. This is before sanding and a clear coat.

![images/0e6b2483a78e560b52c707b7325f2d2d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0e6b2483a78e560b52c707b7325f2d2d.jpeg)



**"Jeff Johnson"**

---
---
**Jim Hatch** *December 01, 2016 20:33*

Didn't want to engrave the names on the instruments? 


---
**Jeff Johnson** *December 01, 2016 20:34*

I suggesred it but he wanted them simple 


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/7LP6yLXj5mi) &mdash; content and formatting may not be reliable*
