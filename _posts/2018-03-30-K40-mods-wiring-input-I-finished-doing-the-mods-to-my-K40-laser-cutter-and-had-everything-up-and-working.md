---
layout: post
title: "K40 mods wiring input. I finished doing the mods to my K40 laser cutter and had everything up and working"
date: March 30, 2018 01:53
category: "Discussion"
author: "Dan Stuettgen"
---
K40 mods wiring input.



I finished doing the mods to my K40 laser cutter and had everything up and working.  added digital amp meter, temp gauge, switches to turn on the air assist, pump, exhaust and lights.  I yanked out the original board and sold it off to someone that needed a replacement for thiers that got toasted.  I installed a Light Object DSP LO-E5 controller along with new stepper motor controllers..  Also added the Light Object Z-table and got it all working without the laser power supply connected, as I didn't want the laser to fire while I was testing everything.



Loaded the latest version of laserCad and  connected the laptop to the laser and it  connected ok and I set up all the setting.



I wanted to start off with something simple to test the laser, so I just draw a simple box to cut the edges, just 4 simple lines. 



I uploaded the box to the laser cutter and then turned on the air assist, pump and exhaust fan.  when I pressed the laser button to start the laser, it started up and within a couple of seconds, everything when off., no power  at first I thought I blew a circuit breaker, but the light that's plugged into the same outlet was still on.



I traced the problem down to a blown fuse in the socket that the power cord plugs into.  all of the power for both the original Laser power supply and the 24V power supply for the DSP controller and Stepper motor controllers and all the accessories are all run through that one power cord.  



Is this how everyone else is running power to their machines or using separate power cords?  I am assuming that all the additional power supply and controllers as well as the fans and pump overloaded the fuse.



Like to hear how other solved the power problem.  Dan







**"Dan Stuettgen"**

---
---
**Phillip Conroy** *March 30, 2018 02:18*

I run everything from one pwr point, to 8 switched power board,


---
**Don Kleinschnitz Jr.** *March 30, 2018 02:44*

One input cable with the input fuse sized accordingly.


---
**Andrew Maley** *March 31, 2018 15:51*

I got the same board but I used a separate power brick/transformer to supply the stepper motors. I figured the motors would pull a fair amount of power. The controller board is running off the original power supply that came with k40.


---
*Imported from [Google+](https://plus.google.com/116093665495871650574/posts/FDcsi51THBi) &mdash; content and formatting may not be reliable*
