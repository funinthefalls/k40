---
layout: post
title: "First time ever posting on G+ so sorry if I do something wrong or take a while to reply"
date: July 13, 2017 16:41
category: "Modification"
author: "Marcus Hawkins"
---
First time ever posting on G+ so sorry if I do something wrong or take a while to reply.



Earlier this year I bought the Smoothieboard x4 and installed it into modified K40 (new gantry) followed a few guides online and got it installed and software setup and started cutting, after a few minutes the control board would crash the leds would show VBB (on) 3v3 (on) 4 (on) 3 (off) 2 (on) 1 (off) none of them flashing. Hit the reset on the control board, restart job and if I am lucky I could get it done without it crashing again (70% chance of it crashing)



Recently had enough and contacted RobotSeed and they sent me a replacement and upgraded it to the 5x board, just installed the board, sorted out the config and start to cut and.... crashed again.



I can leave the control board on for hours and 0 crashes, start cutting and it crashes, seems to only crash when the laser is firing so its either a level shifter issue, power supply or the tube.



Does anyone use a level shifter? I am using a 3.3v <> 5v Arduino one, seen a few guides without one, and 2 with one.





Not sure what to do next. 



Anyone have any ideas?





**"Marcus Hawkins"**

---
---
**Don Kleinschnitz Jr.** *July 13, 2017 17:05*

I would start by loosing the level shifter.... go direct from board to LPS "L">

[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)


---
**Joe Alexander** *July 13, 2017 18:18*

What don said.

   The level shifter is the old method of connection, using a pin as open-drain is much more reliable. I started out going the LS way but quickly swapped.


---
**Paul de Groot** *July 14, 2017 02:26*

Maybe there is interference in the power. Add a ferrite bead and capacitor to the controller's power supply and see if it improves 


---
**Marcus Hawkins** *July 14, 2017 17:03*

**+Don Kleinschnitz** So I would ditch the 5v line and just use L and G from the PSU to the control board?  I am using a PSU that is labelled "G-G-G-R:" in that guide.




---
**Joe Alexander** *July 14, 2017 19:09*

you will need the 5v unless you have the on board regulator for 5v logic. but yes you are essentially toggling L to ground through the mosfets as pwm so the signal is only L and G as shown above.


---
**Marcus Hawkins** *July 14, 2017 20:13*

**+Joe Alexander** what I have at the moment is 24v to the board, 5v to the level shifter and a 5v via USB. 



Would this regulator be ok? - [ebay.co.uk - Details about  1 x Recom R-78E3.3-1.0 DC-DC Switching Regulator 7-28V dc Input, 3.3V Output, 1A](http://www.ebay.co.uk/itm/1-x-Recom-R-78E3-3-1-0-DC-DC-Switching-Regulator-7-28V-dc-Input-3-3V-Output-1A-/222304750012?hash=item33c26549bc:g:w-YAAOSwB09YHOGW)





I am using the pins on the board (near middle MOSFET)  for L and G should I switch to the screw downs on the side?



Tomorrow I will be removing the LS and going direct, if that doesn't work I plan to power the control board from 12v directly from a spare pc power supply, leaving the existing psu to power just the laser tube.



This issue is doing my head in, going away on Thursday, and have 5 customer orders to get done before I leave. :(


---
**Joe Alexander** *July 14, 2017 20:58*

if you have 5v coming in to the board you'll be fine, I just find that using the on board regulator makes it easier by pulling it from VBB. and i would use this one [shop.uberclock.com - Switching Regulator DC to DC 5V](https://shop.uberclock.com/collections/smoothie/products/switching-regulator-dc-to-dc-5v) as I know its compatible with the board and a GLCD addon.


---
**Don Kleinschnitz Jr.** *July 14, 2017 21:57*

**+Marcus Hawkins** 



<i>I am using the pins on the board (near middle MOSFET)  for L and G should I switch to the screw downs on the side?</i>



YES use the screw terminals.



The PWM and gnd that is going to the LPS from the Smoothie must be coming from the screw terminal as my blog post illustrates. This is the drain of the driving mosfet. 



The onboard pins are 3.3v and cannot drive the "L" pin. 



Your config file must match that configuration, as explained in my blog post.



Please post a picture when you get it wired like the post shows.



I also recommend a 5V regulator on-board. I use the uberclock one.



..................

<i>Tomorrow I will be removing the LS and going direct, if that doesn't work I plan to power the control board from 12v directly from a spare pc power supply, leaving the existing psu to power just the laser tube.</i>



You need to power the Smoothie with 24V as that is needed to drive the steppers????



[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20DC%20Power)




---
**Marcus Hawkins** *July 14, 2017 22:38*

**+Don Kleinschnitz** I replaced the stepper motors as the ones with it didn't work smoothly, the ones I have now are 12v.


---
**Joe Alexander** *July 14, 2017 22:52*

regardless of the stepper rating most can handle up to 24v, and most run smoother with higher voltages usually 24 is ideal. just FYI.


---
**Nate H** *July 16, 2017 12:52*

I had the same issue with the control board (I am using a Arduino grbl board) randomly crashing ONLY when the laser was running.  I was also getting what I originally thought was static electricity shocks from the machine.



The problem was a machine grounding issue.  The k40 case was not tied to ground and the power supply ground was floating.  I solved by making sure the ground point at the back of the case was actually contacting the metal of the case (removed some paint) and did the same for the power supply ground.  Both problems have not been seen since.


---
**Marcus Hawkins** *July 16, 2017 13:02*

**+Nate H** You are the first person I have seen that experienced the same, I will start by checking that. Thank you for posting. :D


---
**Don Kleinschnitz Jr.** *July 16, 2017 14:08*

**+Marcus Hawkins** 

#K40NoiseProblems



...these kind of noise problems are pretty common, especially with the Arduino configurations. Search the community and you may find many cases to learn from. 



Turns out that every case I have worked on needed its only custom solution but they are usually grounding and/or power supply noise related.



<b>The grounding hygiene that I use:</b>

Insure the the incoming safety ground is solidly connected to the frame. Clean off paint under fasteners and use star washers to bite into metal surfaces.



Insure that DC power (both power and ground) is connected directly to each destination from the source. Do not daisy chain either power or ground from one power supply destination to the other.



Bring all power supply grounds to a single point. Fasten a single ground post (screw) to the frame as close to source power supplies as possible. Use soldered ring tongues and fasten them in a stack on the post with star washers in between. Tighten solidly. 



Insure that the LPS is grounded and that the FG wire is as short as possible attached into the frame. Insure that the frame of the LPS is fastened to the machines frame.



[photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/vcxu0JX0per9PuVb2)


---
*Imported from [Google+](https://plus.google.com/109498476392134538148/posts/jDkqnZXw2yG) &mdash; content and formatting may not be reliable*
