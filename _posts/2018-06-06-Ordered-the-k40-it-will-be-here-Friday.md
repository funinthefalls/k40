---
layout: post
title: "Ordered the k40 it will be here Friday"
date: June 06, 2018 13:04
category: "Modification"
author: "Adam Hied"
---
Ordered the k40 it will be here Friday.



Also ordered:

Air assist head

18mm lense

Cohesion 3d board with lcd and z axis driver

Water chiller. Just the radiator type

Air pump

Tubing

Better exhaust fan

Various switches, bus bars, wiring etc..



I plan on ordering the holgamods z kit today and whatever is needed to run it.



Is there a beam combiner that isn’t too terribly difficult to setup? Is it worth having one?



Anything I’m missing for mods? I’d rather just make it right as soon as I get it than continually modify it. Should I put a raspberry pi to use for shtp?







**"Adam Hied"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2018 13:15*

What’s the Pi for? 



Typically you’d hook the C3D up to your computer running Lightburn, and run job over usb cable to the board. 



You could potentially put octoprint on the Pi and save gCode from Lightburn and upload it to octoprint, but that’s a different can of worms. 



Ideally you should have a computer with your laser. 


---
**Adam Hied** *June 06, 2018 13:18*

I just have it sitting here and need a use for it. I figured wireless upload of files, but I could just use the computer next to it.


---
**Don Kleinschnitz Jr.** *June 06, 2018 14:23*

I advise doing the conversion in steps. The problems experienced with these machine can be complex in nature. Troubleshooting multiple problems can be frustrating.

Sometimes the base machine has faults at arrival such as a bad tube, mirrors or LPS.

Its a good idea to make sure it all works in the stock configuration so that you can get remedies (maybe) from the seller. This also gives you a working starting point.



I have not found a beam combiner that is simple and low cost. Typically you add more complexity than it is worth.

Actually I have not found adding a laser diode beam into the optics for alignment to be that useful (if that is what you are thinking). The alignment of the laser diode is more onerous than the alignment of the CO2 Laser. K40 alignment is a skill and art and after a few times it will be pretty straightforward.



I have found that a wireless attached laptop next to my k40 using Dropbox as the storage and Lightburn as the K40 driver, to be simple and reliable. 



For mechanical cad design I use F360 and export dxf to lightburn. For non-mechanical designs and engravings I use lightburn direct.



Whatever approach you use there is plenty of help here...




---
**Ned Hill** *June 06, 2018 16:57*

I would also recommend going ahead and upgrading the mirrors.  The stock ones are typically kind of crap.  I got moly mirrors from lightobject and 2+ years in they are still in great condition.


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2018 16:59*

We've got high quality MO (molybdenum) mirrors available at Cohesion3D as well!


---
**Ned Hill** *June 06, 2018 17:03*

**+Ray Kholodovsky** Excellent!  I didn't realize you were selling those and they are way cheaper than the lightobject ones. :)


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2018 17:07*

When I checked recently, I did not find MO Mirrors at Lightobject.  I am using the same factory as Hakan and according to his testing these should be at least as good if not better than LO.  


---
**Adam Hied** *June 06, 2018 18:46*

Will do. **+Ray Kholodovsky**can you combo the shipping now that I ordered the mirrors? Assuming the orders haven’t shipped yet. It’ll be my third order since last night. Last name is Hied.


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2018 19:25*

You got it!  I'll deal with it after I ship.  Currently working through the 2 weeks of backorders we got :)


---
**Adam Hied** *June 06, 2018 20:24*

No worries I’ve got quite a bit to do first anyway.


---
**Christopher Leger** *June 06, 2018 20:25*

You got all those parts from one vendor?  Can you share source?


---
**Adam Hied** *June 06, 2018 20:43*

**+Christopher Leger** I got it all from several vendors. 

Ebay for the laser and water chiller.

Cohesion 3d for the board, lcd, external stepper driver, adapters and mirrors.

Light object for the air assist head and 18mm lenses

Holgamods for the z table

Walmart for fire extinguisher...lol

Amazon for everything else including the additional power supplies, exhaust fan, switches, eye protection etc..

Also, I took a “mental health day” off work today to source everything.



Plan is to make sure everything functions properly this weekend. Align then beam.Then begin the mods as they arrive



It’s about $1k all in give or take.


---
**Doug LaRue** *June 08, 2018 02:16*

[https://makerware.thingiverse.com/thing:2915288](https://makerware.thingiverse.com/thing:2915288)

[makerware.thingiverse.com - K40 laser cutter Combiner Mount for red dot laser by DOugL](https://makerware.thingiverse.com/thing:2915288)


---
**Adam Hied** *June 08, 2018 22:46*

Looks like I lucked out. It came double boxed, very clean. Connections were good, ground tested good,  laser was tested prior to shipping, and it was aligned? I had assumed I would be putting in a ton of work....


---
**Doug LaRue** *June 08, 2018 23:31*

Fantastic.  It seems to be rare but mine was also delivered that way. Tested and verified and found everything a-OK.  Then there were weeks of mod'ding the house for exhaust, setting up the cooling system and cooling system cooler(drinking water cooler) and then that laser beam combiner hack.  Lots of fun to be had.




---
**Eric Lovejoy** *June 15, 2018 10:29*

Get one of these:

[adafruit.com - Controllable Four Outlet Power Relay Module version 2](https://www.adafruit.com/product/2935)



then wire in your logic board, to the controller, of the receptacle, and plug in you air compression. This will make it so your air compressor only runs when it is needed. Ill get the exact pins to connect in a minute, but my board has a plug right on it for that. 


---
**Eric Lovejoy** *June 15, 2018 10:32*

You might also get a receptacle, with USB,  and wire it internally.  I have done this, to keep all my wires neat. 


---
**Adam Hied** *June 15, 2018 12:45*

That’s pretty cool!


---
*Imported from [Google+](https://plus.google.com/101438782220666562093/posts/cQs5k3CxYJ9) &mdash; content and formatting may not be reliable*
