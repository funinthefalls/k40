---
layout: post
title: "Well... FML. My laser tube cracked for no apparent reason"
date: April 30, 2015 22:40
category: "Hardware and Laser settings"
author: "Jon Bruno"
---
Well... FML.

My laser tube cracked for no apparent reason.

I had water flow, no air bubbles, and it was cool to the touch.

I just started to test the machine and sparks began to fly around in the right rear of the unit.

I opened the cover and took a video.

Then I applied the included silicone to the terminal and tried it again no sparks this time but no laser either. I looked into the tube and saw it slowly filling with water.

I hope A: they'll warranty this and B: the power supply is ok. It's not arcing and there is no current shown in the meter which is good I guess. It's all shut down now and unplugged. I don't need 15Kv running into a bucket of water.



![images/08c448858a6a6e059eaaaaef805ec410.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/08c448858a6a6e059eaaaaef805ec410.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-JyUzQb_XM9Q/VUKvao570AI/AAAAAAAABOI/6BK5PzYrGNM/s0/20150430_175820.mp4.gif**
![images/1d3da3906d815fc8d44488fb8e0737ff.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/1d3da3906d815fc8d44488fb8e0737ff.gif)

**"Jon Bruno"**

---
---
**ThantiK** *May 01, 2015 02:37*

Holy mother fuck.  You are <i>NOT</i> joking about arcing!


---
**Jon Bruno** *May 01, 2015 02:38*

Lol! Now you can visually see why you should have a good ground.﻿


---
**David Wakely** *May 02, 2015 08:48*

Holy shit!!! That's an amazing light show, not so good for the laser machine though!


---
**Jon Bruno** *May 02, 2015 17:04*

I'm still waiting to hear back from the seller...

I hope I'm not being dodged..

Well, it is the weekend in China too.....


---
**Jon Bruno** *May 02, 2015 19:02*

did some damage control today and some testing.. as I suspected, I will be changing the two MOSFET's that drive the transformer. Fortunately IRFP450's are only a few bucks a peice. 

I'm opting to fix the power supply than to deal with trying to get a replacement.. after all I killed it playing with the sparks. ;-)


---
**Eric Parker** *May 16, 2015 23:13*

OH SHIT.  Okay, next purchase is going to be a copper clad ground rod.  Thankyou for posting this.


---
*Imported from [Google+](https://plus.google.com/105850439698187626520/posts/6eq6syytMMA) &mdash; content and formatting may not be reliable*
