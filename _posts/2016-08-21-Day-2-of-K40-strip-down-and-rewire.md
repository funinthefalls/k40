---
layout: post
title: "Day 2 of K40 strip down and rewire...."
date: August 21, 2016 17:36
category: "Hardware and Laser settings"
author: "Jez M-L"
---
Day 2 of K40 strip down and rewire.... Thought I would attempt to disconnect the HV wire to tube today as 1. It's bloody long and wrapped all around the end of the tube with cable ties and 2. I wanted to get the psu out.  Glad I did as I found that the cable was just wrapped around the anode and then glue gunned in place and the anode is black with what I can only think is arcing.  So, my question is.. Can I shorten the fat HV wire and then solder it back in place? Also the return cathode cable for me looks drastically thin for a HV return.  I have some beefy household earth cable that i can use to replace them both, is this a wise thing to do?



Thanks again everyone,



Jez





**"Jez M-L"**

---
---
**greg greene** *August 21, 2016 17:47*

HV Works Just fine on thin wires - it's just a few Ma max so in this case - size doesn't matter !


---
**Jez M-L** *August 21, 2016 17:54*

Cheers Greg... Would it be wise to solder them back on though and wrap with something more suitable as an insulator, say self amalgamating tape? Does anyone know if there is a quick release connector as well so that I can disconnect this when I get round to modding it more?


---
**Derek Schuetz** *August 21, 2016 17:57*

**+Jez M-L** do not solder the wire to the tube heat cause metal to expand and you crack your tube.


---
**Jez M-L** *August 21, 2016 18:00*

So how do I connect it?  Is this the first connection on this thing that is correct? Surely not?


---
**Derek Schuetz** *August 21, 2016 18:07*

**+Jez M-L** ya that is how you do it tightly wrap the wire around the point a couple of times throw some tubing over it and fill with silicone 


---
**Jez M-L** *August 21, 2016 18:09*

Wow... Thanks Derek.  I have taken it all off now so will get the silicone gun out once I have re-routed and shortened the wires.


---
**Derek Schuetz** *August 21, 2016 18:09*

Make sure you use the right silicone apparently the typical stuff you buy at home depot is not sufficient enough. Your k40 should have come with a tube of it


---
**Jez M-L** *August 21, 2016 18:14*

Wondered what that was for! Cheers!


---
**Scott Marshall** *August 21, 2016 22:18*

Home Depot silicone is fine. Just make SURE it's 100% Clear silicone. They sell a lot of cheap caulking products that are silicone diluted with  latex or acrylic caulk. These are not High voltage safe.



Stick with 100% clear, because the fillers used to color the clear base silicone are conductors (Carbon in black, Aluminum powder in silver, Titanium dioxide in white, all BAD as insulators).



In Industrial high voltage applications GE or Dow Corning Silicone are often specified, and are getting hard to find on the consumer market.

I've never had a problem with other brands, just triple check it is 100% silicone. The most expensive stuff, of course.



If you have any doubt, head to the auto parts store - the gasket sealer version is rarely polluted and tell you right out what's in it.



The cost of a squeeze tube is about the same as a caulker tube, but the caulker tubes don't keep well at all. Once opened 2 weeks max. ( they leak air at the plston once it's moved)



At least the squeeze tubes are re-sealable, and you only need a teaspoon or so to do the job. (With any kind of luck, it too will be all dried out and useless next time you need to disconnect your tube)



Scott


---
**Jez M-L** *August 22, 2016 15:38*

Thanks Scott


---
*Imported from [Google+](https://plus.google.com/103448790120483541642/posts/Rfz2bq3wtgE) &mdash; content and formatting may not be reliable*
