---
layout: post
title: "Even though the forum has been full of distracting, albeit interesting, PWM questions and problems I have managed to spend some time with my machine"
date: January 30, 2017 23:12
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
Even though the forum has been full of distracting, albeit interesting, PWM questions and problems I have managed to spend some time with my machine. 



For the first time ever I engaged in the dreaded optical alignment process which immediately frustrated me beyond words.



While standing on my head, tweaking, burning, opening and closing covers  I had a brain fart. An idea developed which enticed me to take a completely different approach to understanding and aligning the optics than I have seen up to now.



<s>---------------------</s>

Of course on the way there I just had to pause and design a new head, all acrylic and laser cut-able.

<s>--------------------</s>



I completed the first test of the new alignment approach today and I am excited with the results enough to share. Lots more to test as I complete the alignment.



Some teaser pictures attached.



The full detail in two posts:

[http://donsthings.blogspot.com/2016/06/k40-alignment-tools.html](http://donsthings.blogspot.com/2016/06/k40-alignment-tools.html)

[http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html](http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html)



The sketchup models which include a dimension-ed optical path are shared in these posts.





![images/0b283080755180614a8573177be6da12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0b283080755180614a8573177be6da12.jpeg)
![images/83f461e9e0455ee14ed2ea5e2017baa2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/83f461e9e0455ee14ed2ea5e2017baa2.jpeg)
![images/1f9845461d4ea7a9fbb6ec096a995389.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f9845461d4ea7a9fbb6ec096a995389.jpeg)
![images/8f235955dc7cba1e3936c25136cc0342.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f235955dc7cba1e3936c25136cc0342.jpeg)
![images/db0947f933e935fcf64526beb0f59021.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db0947f933e935fcf64526beb0f59021.jpeg)
![images/0596f0f8d8735b681df15c1da5f5a697.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0596f0f8d8735b681df15c1da5f5a697.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**HalfNormal** *January 30, 2017 23:28*

**+Don Kleinschnitz** Very interesting read. I have a couple of questions, in order to have a parallel axis, the assumption is made that the laser is parallel and then all measurements are made to this plane(s). Second, the mirrors have to be exactly 45 degrees, so where is this verified? The alignment process has always vexed me in that it is only as good as the originating plane that everything is then aligned to. Because the laser is mounted in a separate area on a separate mount to the gantry, verifying parallel alignment is next to impossible. Yes we get very close but it is more about luck than skill! 


---
**Don Kleinschnitz Jr.** *January 31, 2017 00:01*

**+HalfNormal** the impossibility of alignment in 3 dimensions was what got me going this way.



The first is to prove that the laser beam is parallel to the X-Yaxis:

...The bench becomes a plane that is synonymous with the x-y movement of the head because it is mounted to the gantry in x-y.

Then a set of precisely and identically made targets are mounted perpendicular to the bench and the beam is shot through them. 

If the beam penetrates multiple targets that are all on the same plane (the bench) and at the same height in the same place then the beam has to be running parallel to it.  

I don't care where the laser tube is as long as when the beam traverses the gantry's x-y plane (the bench) it stays parallel. If the laser is not parallel to the bench it will not hit multiple targets at the same height. 

The key is in the precise nature with which the bench is attached to the gantry and the design of the targets and holders. Without the model I could not have fabricated it this precise. 



It #2 at a 45:

...The targets are laid in line along the Y axis in front and in line with mirror #2 and along and in line with the X axis after #2 and in front of mirror #3. If the beam impacts all the targets at the same height, landing finally on the #3 mirror on center then its aligned close enough to a 45.  



The final proof. Is the objective lens perpendicular;

The light going through the objective lens is checked for perpendicular-ism by lowering the bed, pulsing and lowering again multiple times. If the beam mark stays in the same place it is perpendicular. If it moves off center it is not. One of these targets can be used for this as well.



I just completed the alignment through mirror #3 and I think this works. Only two adjustments needed after the first test. If nothing else I feel like I can see what is happening and I have a record of results :).

![images/c4fafc27c89583d82274c69f0dd666a0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4fafc27c89583d82274c69f0dd666a0.jpeg)


---
**HalfNormal** *January 31, 2017 00:06*

Thanks for the explanation. 


---
**Don Kleinschnitz Jr.** *January 31, 2017 01:10*

The final proof the objectve mirror. Notice only a small error (slight variation in the box) when the lift table lowered 3 inches.



![images/33040cb9507dc6e35925ac2627e52518.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/33040cb9507dc6e35925ac2627e52518.jpeg)


---
**Vince Lee** *February 08, 2017 23:08*

Interesting.  This seems to very nicely visualize and illustrate how the normal alignment process (when rarely done right) is supposed to work.  Most people seem to hopelessly confuse the alignment process by trying to get the beam to strike the center of each mirror, when its primary goal should be to align the beams parallel to the axes of movement instead (so that the same spot is always hit on each mirror even if off-center).  By using two separate targets for each leg instead of moving one target (the tape) back and forth, the process seems less confusing, even if largely equivalent.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/hWeeG9xfFqD) &mdash; content and formatting may not be reliable*
