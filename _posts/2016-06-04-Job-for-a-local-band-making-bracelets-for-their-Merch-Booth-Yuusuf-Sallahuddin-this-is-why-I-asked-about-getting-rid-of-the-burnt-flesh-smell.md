---
layout: post
title: "Job for a local band making bracelets for their Merch Booth Yuusuf Sallahuddin this is why I asked about getting rid of the burnt flesh smell"
date: June 04, 2016 06:02
category: "Object produced with laser"
author: "Alex Krause"
---
Job for a local band making bracelets for their Merch Booth **+Yuusuf Sallahuddin**​ this is why I asked about getting rid of the burnt flesh smell



![images/43c450bedf92b3905fa8fec24feebb40.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43c450bedf92b3905fa8fec24feebb40.jpeg)
![images/ac79ee33593200cd552ae0397e7daf94.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ac79ee33593200cd552ae0397e7daf94.jpeg)
![images/f0b50b36c643017616173ed59139bd41.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f0b50b36c643017616173ed59139bd41.jpeg)
![images/2f57d9f69ac828ff096d79002f2d4718.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f57d9f69ac828ff096d79002f2d4718.jpeg)
![images/404cf825f13b88de39fb4b719997c837.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/404cf825f13b88de39fb4b719997c837.jpeg)

**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 04, 2016 14:30*

Ah cool. I would have maybe used natural beeswax to condition it if they were staying natural colour like that. It would have given a nicer smell, slightly like honey, and also adds a bit of "antique" looking colouring too it.


---
**Ned Hill** *June 04, 2016 18:19*

Doing a quick search for removing smell on laser etched leather turns up several options.  People say applying a leather conditioner will help.  Also some people like to use  Febreze  odor eliminator.  Febreze contains a Beta-cyclodextrin which binds to volatile odor producing hydrocarbons.  A 3rd option comes from this patent [http://www.google.com/patents/EP2464756B1?cl=en](http://www.google.com/patents/EP2464756B1?cl=en) which uses the odor absorbing agent Zinc ricinoleate (Zn salt of the ricinolinic fatty acid).  The preferred invention (section 51) uses a water solution (by weight) 2-20% Zn ricinoleate and 2-10% Na iminodisuccinate.  Zn ricinoleate is not water soluable so they are using the surfactant Na iminodisuccinate as a solubility enhancing agent.  Dip or spray and then dry.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 04, 2016 18:28*

**+Ned Hill** I generally find that dyeing or finishing with beeswax, followed by leather conditioner, tends to remove all the burnt smells. Unfortunately, some products, such as suede can't use the conditioner/dye/beeswax. I'm interested in that Zn ricinoleate. I'll have to look it up & see what I can find. Thanks for the share.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/KMg8HtdS5v6) &mdash; content and formatting may not be reliable*
