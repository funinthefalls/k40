---
layout: post
title: "Someone asked \"What do we make with our K40's\" So here you go some robots!"
date: December 05, 2015 20:53
category: "Object produced with laser"
author: "Phil Willis"
---
Someone asked "What do we make with our K40's" So here you go some robots! The blue one came second in its class today at the 2015 Pi Wars in Cambridge UK.  [http://piwars.org/2015-competition/](http://piwars.org/2015-competition/)



Designed (not by me) in Fusion 360 and output to Corel Draw. 



(The are some 3d printed components in there as well!!)



The blue one is called TractorBot and us powered by a Raspberry Pi and programmed in C by myself and a colleague. Enjoy!



![images/e8100c98f12735b45651ffd4af0ba790.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8100c98f12735b45651ffd4af0ba790.jpeg)
![images/8a3035b4a9cad7cf136f42919c002e5d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a3035b4a9cad7cf136f42919c002e5d.jpeg)
![images/5d3e364e2c169d3a38cf1c8bd5cd1086.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5d3e364e2c169d3a38cf1c8bd5cd1086.jpeg)
![images/54fe7e1d58601284eb073371e4cd678f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/54fe7e1d58601284eb073371e4cd678f.jpeg)
![images/1f1ef8ffe7d581119875d20775a9e6e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f1ef8ffe7d581119875d20775a9e6e9.jpeg)
![images/a5e7a24758a40649696eb3abf3f9aa91.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a5e7a24758a40649696eb3abf3f9aa91.jpeg)
![images/20161e752be96c8ba4a1cc334043843f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/20161e752be96c8ba4a1cc334043843f.jpeg)
![images/6f7d5dffdc319866a906a14ea04fbb98.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6f7d5dffdc319866a906a14ea04fbb98.png)

**"Phil Willis"**

---
---
**Gary McKinnon** *December 05, 2015 23:11*

Great stuff :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 06, 2015 03:10*

That looks cool. What does it do though, other than drive? I see what looks like eyes on one end of the robot.


---
**Scott Thorne** *December 06, 2015 04:16*

Lol....cool....I love it.


---
**Phil Willis** *December 06, 2015 17:31*

**+Yuusuf Sallahuddin** Little black one runs around avoiding obstacles. The big blue one had distance, line following and remote control, for the challenges in Pi Wars (see the link above)  More pictures here. [https://goo.gl/photos/JBt9XMcjgCsopSWi7](https://goo.gl/photos/JBt9XMcjgCsopSWi7)


---
*Imported from [Google+](https://plus.google.com/+PhilWillisAtHome/posts/dpL6s9TS7ZV) &mdash; content and formatting may not be reliable*
