---
layout: post
title: "Hi all, done my smoothie conversion and working pretty well now, here is my very first 3d carving (im really impressed with this ....."
date: October 26, 2016 16:44
category: "Object produced with laser"
author: "Mike Grady"
---
Hi all, done my smoothie conversion and working pretty well now, here is my very first 3d carving (im really impressed with this .....

![images/def47df6be9d84b8c67ec013be428567.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/def47df6be9d84b8c67ec013be428567.jpeg)



**"Mike Grady"**

---
---
**Anthony Bolgar** *October 26, 2016 16:52*

What power and how many passes?


---
**Ariel Yahni (UniKpty)** *October 26, 2016 16:53*

Very good job. Should go up very high on the best 3D carving with smoothie 


---
**Mike Grady** *October 26, 2016 17:01*

Pot set at 9ma (0-60% in laserweb) light feedrate 200mms dark 150mms first pass, second and third pass just reduce dark feedrate by 50mms each time


---
**Loren Brandenburg** *October 26, 2016 17:01*

That's awesome... Nice work!




---
**Anthony Bolgar** *October 26, 2016 17:03*

Thanks for the info **+Mike Grady** 


---
**Ariel Yahni (UniKpty)** *October 26, 2016 17:08*

**+Mike Grady**​ Wood type for reference please


---
**Mike Grady** *October 26, 2016 17:12*

No problem **+Anthony Bolgar** and the wood is 3mm birch plywood from ebay


---
**Ariel Yahni (UniKpty)** *October 26, 2016 17:14*

**+Mike Grady**​ one more thing, can you post your smoothie config file somewhere for us to review. Also the image used to do the above. That's would be very helpful for others


---
**Charles Laser Lee** *October 26, 2016 19:39*

May I ask how long this job takes？time


---
**Mike Grady** *October 26, 2016 20:30*

**+Peter van der Walt** yes i just imported the bitmap into laserweb(no pre-processing except made image smaller), i had to change the BMP resolution in laserweb to 100dpi(really small at 300dpi) put in my settings, made the gcode then started the job, when first pass finished revised my settings and done second pass then once again revise and third pass


---
**Mike Grady** *October 26, 2016 20:32*

I cant remember how long it took, but pretty sure it was less than 20 minutes


---
**Anthony Bolgar** *October 26, 2016 20:42*

Sure thing, send me by email or PM your write up and I can clean it up and post it to the wiki,,, I am sure it will be popular. Great work **+Mike Grady** !


---
**Mike Grady** *October 26, 2016 20:43*

Here is my config file and the image i used

[drive.google.com - Laserweb - Google Drive](https://drive.google.com/drive/folders/0Bys0PMSWaHZhbVg4amF3eUtIVWc?usp=sharing)


---
**Mike Grady** *October 26, 2016 21:01*

I just added a small video to my google drive link too, i think its the final third pass before cleanup, a write up hmmm i can but will have to wait a day or two got work for 6am tomorrow


---
**Ariel Yahni (UniKpty)** *October 26, 2016 22:25*

Ok thanks **+Mike Grady**​ I see you are using one of **+Scott Marshall**​ ACR Kits. I'm sure you are a very happy customer. Config it pretty much the smoothie standard in relation with acceleration, speed and power.  Thanks for sharing


---
**Mike Grady** *October 26, 2016 22:48*

Yea i am really happy with the ACR kit, only problem i had was the x motor was really noisy and jerky so i dropped the current to 0.3 in the config file, thats the only mod i did to the file, everything else is just as it was originally


---
**Ariel Yahni (UniKpty)** *October 26, 2016 23:16*

**+Mike Grady**​​ thanks for the tip I could replicate, just much much smaller 



![images/d9571dfb5e96ccf83171598a1c6966d0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d9571dfb5e96ccf83171598a1c6966d0.jpeg)


---
**Mike Grady** *October 27, 2016 07:05*

**+Ariel Yahni** decrease bitmap resolution from original 300 dpi to 100


---
**Ariel Yahni (UniKpty)** *October 27, 2016 10:37*

**+Mike Grady**​ Yeah I know, did not want to :) 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2016 10:32*

Love the job, I might have to have a look through your image & settings & see if I can replicate this some time in the near future. I'd love to be able to do some wonderful carvings like this.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2016 10:47*

Wow, just watched your video of the engrave in action. I've definitely got to check through your settings. Much smoother motion than I've been getting. I'm for sure doing something wrong.


---
**Mike Grady** *October 28, 2016 10:51*

To be honest **+Yuusuf Sallahuddin** i think i just dropped lucky with my settings, if you have watched the video you can see a tiny engraving in the top left corner, that was my first ever attempt at carving, dropped the bmp resolution and this was the result, second attempt lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2016 10:53*

**+Mike Grady** Yeah, well your settings are working well, so I think whatever I did to my config file needs fixing :D Great results for a 2nd attempt.


---
**Mike Grady** *October 28, 2016 11:17*

My actual config file only had one change, i decreased the current for the x axis motor to 0.3 because the movement was noisy and jerky


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2016 11:23*

**+Mike Grady** That may be what my actual issue is too... since I think I have it set at 0.5 for the x-axis motor. I will give that a test & see if it improves my performance :) I'll report back if that is the case once I give it a test.


---
**Mike Grady** *October 28, 2016 12:10*

ok, hope it works out for you :)


---
*Imported from [Google+](https://plus.google.com/106807531578115449401/posts/d5Z3D7a4FAF) &mdash; content and formatting may not be reliable*
