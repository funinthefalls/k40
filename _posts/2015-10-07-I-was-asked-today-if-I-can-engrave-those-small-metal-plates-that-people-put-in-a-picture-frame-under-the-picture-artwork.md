---
layout: post
title: "I was asked today if I can engrave those small metal \"plates\" that people put in a picture frame, under the picture/artwork"
date: October 07, 2015 19:11
category: "External links&#x3a; Blog, forum, etc"
author: "Ashley M. Kirchner [Norym]"
---
I was asked today if I can engrave those small metal "plates" that people put in a picture frame, under the picture/artwork. Can these be done on the K40? Is there a process to do that, like having to cover them with a spray (for 'marking' instead of engraving)?





**"Ashley M. Kirchner [Norym]"**

---
---
**David Wakely** *October 07, 2015 19:56*

I've been reading about people using  molybdenum disulfide to spray on stainless steel then laser engrave and the wipe off. I think the product is called Dry Moly Lube.



There is a proper commercial product called cermark but it's mega expensive (£60-£100 for a can!!!) and apparently is the same active ingredient as dry moly. 



I've never tried either so I don't know how it will come out but at £15 a can for some dry moly it's worth a shot!



[http://m.instructables.com/id/Laser-Marking-Stainless-Steel-1/](http://m.instructables.com/id/Laser-Marking-Stainless-Steel-1/)



If you do try it let the community know - I'm sure it's something we'd all love to see!


---
**Eric Parker** *October 17, 2015 18:30*

Cermark also has vanadium in addition to the Molybdenum disulfide they have in common.



I use the dry moly spray myself, but I hear that although the cermark is expensive, you get many many uses out of the can.  The general consensus is that thte return on investment is rather high, if you operate commercially and sell your work.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/AssoxF8MpAi) &mdash; content and formatting may not be reliable*
