---
layout: post
title: "Idea I just had for a modification for the K40 Laser Cutters"
date: October 21, 2015 06:01
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Idea I just had for a modification for the K40 Laser Cutters. I was thinking about a conveyor belt kind of system that runs on the y-axis.



I've seen that **+Allready Gone** recently posted pictures & information about a rotary attachment he built & I thought using the same method he used for that, you could create a conveyor belt system to run the material through the laser cutter & keep feeding more pieces in (provided they are aligned in correct position to allow laser cuts/engraving to occur in correct spot).



Anyway, thought I'd share my idea with you guys (in case nobody else has done/thought of this).

![images/cdf16e704fae5f57c8414d0ae463a44b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdf16e704fae5f57c8414d0ae463a44b.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 21, 2015 07:08*

Oh, I forgot to mention that the left side of the picture is looking at the machine from the left side of machine. Right side of picture is as though you are looking at the machine from the front.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 21, 2015 08:30*

**+Allready Gone** Looking at your 3d printed rotary fixture diagram I have a feeling that it could be modified to provide both abilities in one device; i.e. rotate & conveyor. Since your rotary fixture uses rotating axles & the conveyor would use rotating axles too. Basically would just need to attach a belt to it for conveyor function & remove belt & adjust spacing from each other for rotary function.


---
**Stephane Buisson** *October 21, 2015 09:24*

**+Yuusuf Salahuddin** ask yourself what problem a conveyor belt will solve? is it for a K40 ? what is doable for a rotary maybe not work for a conveyor (fixed Y vs loss of precision in Y), more tricky that it look like isn't ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 21, 2015 09:49*

**+Stephane Buisson** I was not meaning to use the conveyor at the same time as the rotary (if that's how my previous explanation came across). But you're right, a conveyor may introduce some other issues. The problem I would like to solve with a conveyor belt is to be able to work on larger materials (e.g. a 1.5 metre length of leather for a belt) in one go. Rather than having to do step by step in the area that is usable. Technically we don't have a fixed Y-axis in the stock model though (correct me if I'm thinking of this wrong). The Y-axis moves front-to-back of the machine. So we have a fixed material that we are working on & a moving Y-axis. If we had a fixed Y-axis and a moving material would be basically the same (in my imagination at least), although maybe not the same from an electronics/mechanics perspective?


---
**Stephane Buisson** *October 21, 2015 12:02*

I like your belt idea,  you should be able to do it with rotary mod (fixed Y), but not a conveyor (not precise enought).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 21, 2015 13:18*

**+Stephane Buisson** Oh so that's what you meant by fixed Y. The rotary is "fixed" as in it doesn't move around.


---
**Stephane Buisson** *October 21, 2015 13:37*

yes the rotary is "Fixed" in Y, I mean the laser head always run on the same Y line because the rotary is placed into special position on the table. what you don't want is a little meccanic play into conveyor loosing the Y position and f*ck it all.



you can always have a go squeezing your belt under rotary but over a piece of cloth (small friction on table)


---
**Ashley M. Kirchner [Norym]** *October 21, 2015 18:18*

This is kinda interesting in that I do see some systems designed that way. For example, here at my office we have large flat bed printers (and when I say large, I mean LARGE, full size 4 feet by 8 feet beds). You can either put a large flat piece on to the vacuum bed and the head will move both in the XY direction, or you can do a roll feed in which case the head will only move in the X direction while the material moved in the Y direction.



It's the same way a roll-fed kiss-cutter functions: the material is fed on a roll while the cutter head moved left to right. By moving the material with the head you can get curves done.


---
**Stephane Buisson** *October 21, 2015 22:36*

attention the stepper power is low in both axis, 0.6 and 0.5 amp, limited torque.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 22, 2015 01:22*

**+Stephane Buisson** I see what you're saying now. Makes sense to try squeeze the belt under the rotary fixture to "push" it along the bottom & do the engraving in between the rotary "wheels" (or whatever you call them). In regards to the stepper power being low, couldn't we increase that? (note: I have limited electrical experience haha).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 22, 2015 01:23*

**+Ashley M. Kirchner** I agree with what you're saying. I've seen large scale laser cutters when I was researching before purchasing that run on a conveyor system for the Y-axis. Albeit probably out of my price range for now though.



(edit) In my original design the y-axis was supposed to be fixed & not moving, but instead the conveyor will move.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/VyjtbKXhYnE) &mdash; content and formatting may not be reliable*
