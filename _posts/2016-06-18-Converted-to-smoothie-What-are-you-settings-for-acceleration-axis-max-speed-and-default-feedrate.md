---
layout: post
title: "Converted to smoothie? What are you settings for: acceleration, axis max speed and default feedrate"
date: June 18, 2016 04:13
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Converted to smoothie? What are you settings for: acceleration, axis max speed and default feedrate. Comparing you engraving speed you would say





**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 18, 2016 07:05*

Follow, I'll post back once I've finalised the smoothie conversion.


---
**Ariel Yahni (UniKpty)** *June 18, 2016 16:19*

Guys it's important to get your speed and acceleration settings from the config file


---
**Vince Lee** *June 18, 2016 19:00*

The speed is adjustable on both so there really isn't much difference.  Although smoothie supports acceleration so theoretically you could run a faster speed if the laser is powerful enough to still engrave sufficiently.  On the down side,  gcode is really inefficient for storing bitmaps compared to the likely native format so there might be a practical limit on the Max speed when engraving over live serial connection


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/b6ZrgHqgwwm) &mdash; content and formatting may not be reliable*
