---
layout: post
title: "Engraved a picture i took in the Mercedes museum in Stuttgart about 2 years ago"
date: July 20, 2017 00:02
category: "Object produced with laser"
author: "Paul de Groot"
---
Engraved a picture i took in the Mercedes museum in Stuttgart about 2 years ago.

380dpi with gerbil controller 

![images/6a2f55b641895fd8c3d301948dd92e59.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6a2f55b641895fd8c3d301948dd92e59.jpeg)



**"Paul de Groot"**

---
---
**Paul de Groot** *July 28, 2017 01:58*

The Inkscape plugins for Gerbil are at [https://github.com/paulusjacobus/Gerbil](https://github.com/paulusjacobus/Gerbil)


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/X93WzsK2LEz) &mdash; content and formatting may not be reliable*
