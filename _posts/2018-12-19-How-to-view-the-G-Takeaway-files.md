---
layout: post
title: "How to view the G+ Takeaway files"
date: December 19, 2018 01:57
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
How to view the G+ Takeaway files.



**+Don Kleinschnitz Jr.** I found this HTML file viewer that allows you to drag and drop your files so that you can review them.



You can find the files under ..\Takeout\Google+ Stream\Posts



[https://www.ghacks.net/2014/05/20/quickly-browse-html-files-mass-html-viewer/](https://www.ghacks.net/2014/05/20/quickly-browse-html-files-mass-html-viewer/)





**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/M3PBYizjwpP) &mdash; content and formatting may not be reliable*
