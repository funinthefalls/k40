---
layout: post
title: "Anyone devise a way to monitor/record usage hours for their tube?"
date: June 21, 2016 13:47
category: "Discussion"
author: "Ben Marshall"
---
Anyone devise a way to monitor/record usage hours for their tube? (other than manually recording time firing). I would like to have a readout or tracker for total hours the tube has fired as it would help determine when I should start looking at replacement tubes before my current one burns out. Average is 1000-1300 hrs I've read. 





**"Ben Marshall"**

---
---
**greg greene** *June 21, 2016 14:04*

Sure - use an hour meter


---
**Ben Marshall** *June 21, 2016 14:09*

**+greg greene** wouldn't that just monitor how long the machine has been running? Not necessarily how long the tube has been firing 


---
**greg greene** *June 21, 2016 14:18*

Depends on what circuit you use to trigger it, in any case it may be close enough,


---
**Scott Marshall** *June 21, 2016 14:19*

I just happen to have designed a plug in Air assist driver/hour meter kit for the k40. they're not very expensive.  I could do one with just the hour meter for you, if that's all you need. If you're interested and want more info, send me an e-mail  - Scott594@aol.com



You can wire in an hour meter yourself, but it takes some know how and some circuitry to make it work. If you decide to go that route, I'll help talk you thru it.



Scott


---
**Tony Schelts** *June 21, 2016 14:20*

dosent the power settings make a differnece.  If you only fire at 5-7ma most of the time, wont that last longer that 15-18MA most of the time ?


---
**Stephane Buisson** *June 21, 2016 14:36*

if you use usb, maybe a simple script on your computer to get each print time. But I do agree on the fact different materials need different settings. It will give a rough idea, but also pretty sure the tube lifespan is also a rough estimated. is it to help on how charge your work ?


---
**Vince Lee** *June 21, 2016 15:17*

Sounds like a great feature for software **+Peter van der Walt**​.  Anybody know a good reference for how power level affects life?


---
**Ben Marshall** *June 21, 2016 16:16*

**+Peter van der Walt** ok thanks. Just a consideration that I have. I plan on selling pieces and the last thing I want to happen is have a high production need and a tube die on me


---
**Stephane Buisson** *June 21, 2016 16:34*

**+Peter van der Walt**  to bounce on a script idea, for laserweb, not for laser cutting, but plasma cutting (to stay within the duty cycle max time 6/10mn)


---
**Phillip Conroy** *June 21, 2016 21:04*

I used a cheap $10 induction pickup[wire raped around thick wire going to tube] hour meter i got on ebay ,untill i changed power supply and it could no longer pickup the laser firing ,now i use an hour meter for trucks/boats that works by 12volts going to it,i know it is not accuarate as it doent measure firing time but in my case firing time is about 1/2 machine on time.l use the hour meter to know when to change air comoressor oil[every 100 hours on time] as well as when to recharge my silica gel water trap[100 hours] and a rought tube life gauge.


---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/Jxm3ZnZ9UuY) &mdash; content and formatting may not be reliable*
