---
layout: post
title: "Done with my Smoothiefied K40. Plus some leather dye & a brush"
date: August 26, 2016 08:52
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Done with my Smoothiefied K40. Plus some leather dye & a brush.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



 #LaserWeb3  Dragon engraved on 18mm pine using LaserWeb3.



Played around with a bit of leather dye on the wood afterwards. Could do with watering down a lot or using a dye specifically suited for wood maybe.



![images/d25e90e34a3eda361b771ba48b832512.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d25e90e34a3eda361b771ba48b832512.jpeg)
![images/a54e71b43424d4d4f3c05b2a8c94bf8f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a54e71b43424d4d4f3c05b2a8c94bf8f.jpeg)
![images/69acfc5c9274f83af509076bb4d2e144.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/69acfc5c9274f83af509076bb4d2e144.jpeg)
![images/89532eed996cfefde03a0d9d92b004b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/89532eed996cfefde03a0d9d92b004b1.jpeg)
![images/8623199bd1f89cfbec9c81a88d63a00a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8623199bd1f89cfbec9c81a88d63a00a.jpeg)
![images/268a221658f4200e70719aceb0155aa8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/268a221658f4200e70719aceb0155aa8.jpeg)
![images/d005be4997c8bee72c186b6f172cc9b0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d005be4997c8bee72c186b6f172cc9b0.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Jim Hatch** *August 27, 2016 00:19*

Nicely done.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/gLjZk4wbdsd) &mdash; content and formatting may not be reliable*
