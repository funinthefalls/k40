---
layout: post
title: "I did some test to compare the engraving through standard Moshidraw with Grbl"
date: March 21, 2017 02:20
category: "Modification"
author: "Paul de Groot"
---
I did some test to compare the engraving through standard Moshidraw with Grbl.





**"Paul de Groot"**

---
---
**Alex Krause** *March 21, 2017 02:30*

My best engrave with the stock software...

![images/460d96e2160ec382963670792c477a55.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/460d96e2160ec382963670792c477a55.jpeg)


---
**Alex Krause** *March 21, 2017 02:31*

One of my better engraves with smoothie board + Laserweb

![images/ad03618efb3332fc190a1da56f528b68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad03618efb3332fc190a1da56f528b68.jpeg)


---
**Paul de Groot** *March 21, 2017 02:41*

**+Alex Krause** fabulous 😊


---
**Paul de Groot** *March 21, 2017 02:44*

**+Alex Krause**​ would it be possible to share the original image so we can do a compare? Would love to see how it would stand up to grbl.


---
**Cesar Tolentino** *March 21, 2017 15:01*

Can you share your workflow on how you achieved this. I also use inkscape and grbl. But using a form of grbl because I'm using lasersaur board


---
**Paul de Groot** *March 21, 2017 21:26*

**+Cesar Tolentino** I googled a picture of the corvette and saved it. I imported it into Inkscape with at least 300dpi settings (default in inkscape is 90dpi). Then I resized the picture and resized the canvas in document properties to match the picture. Then I used the raster plugin 305-engineering (with slight mods) to generate the g-code which I streamed via [streamer.py](http://streamer.py) (see grbl github if you want to download it). The settings were 1400mm/min, 256 grey shades, 382 dpi. My controller setup is an ArduinoR4 + Grbl1.1e with a slight modification to enable 10-16 bits range for engraving (not 8 bits see link for more info).

[awesome.tech - Manufacturing an Arduino R4](http://awesome.tech/manufacturing-an-arduino-r4/)


---
**Cesar Tolentino** *March 21, 2017 21:27*

Thank you sir


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/c7mXb1fPFFM) &mdash; content and formatting may not be reliable*
