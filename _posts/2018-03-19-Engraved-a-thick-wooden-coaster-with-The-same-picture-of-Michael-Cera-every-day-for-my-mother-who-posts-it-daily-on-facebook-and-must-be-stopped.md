---
layout: post
title: "Engraved a thick wooden coaster with \"The same picture of Michael Cera every day\" for my mother, who posts it daily on facebook and must be stopped"
date: March 19, 2018 05:22
category: "Object produced with laser"
author: "Kiah Connor"
---
Engraved a thick wooden coaster with "The same picture of Michael Cera every day" for my mother, who posts it daily on facebook and must be stopped. 



Amazed at how big a difference bottom to top engraving makes (less smokey horribleness!). 



Waiting on an inflateable bouncy castle blower for ventilation to get delivered so I can play with acrylic without poisoning myself! 

![images/a3e098a6b5e595f68c85c9e32fbe159a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3e098a6b5e595f68c85c9e32fbe159a.jpeg)



**"Kiah Connor"**

---
---
**Fabiano Ramos** *March 19, 2018 21:00*

Dude, I was just wondering if this guy was Michael Cera. ahhaahahahah!



You have a great humorous spirit.



About the smoke really is that. I had the same problem as you, even buying the blessed Air Nozzle. After I took the smoke out of my Woking Area, everything ended well.


---
**Kiah Connor** *March 20, 2018 01:33*

Was talking about the plan with my girlfriends father who it turns out had the exact model of blower I was looking at buying, gathering dust in his backyard, so hopefully my smoke will also be very removed very soon.






---
**Timothy Rothman** *April 05, 2018 03:33*

Just unfriend your moms.  Problem solved.


---
**Kiah Connor** *April 05, 2018 04:24*

**+Timothy Rothman** I've done that before. Didn't end well, would you believe it.

Might just delete my facebook, that seems to be trending at the moment.



Also I've added air assist and replaced the bed with a mesh since this.... Wow, what a difference! 


---
*Imported from [Google+](https://plus.google.com/100262059485602837936/posts/YEtCTBy9Lks) &mdash; content and formatting may not be reliable*
