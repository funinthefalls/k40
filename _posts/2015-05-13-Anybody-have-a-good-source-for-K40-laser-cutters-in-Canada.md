---
layout: post
title: "Anybody have a good source for K40 laser cutters in Canada?"
date: May 13, 2015 16:11
category: "Discussion"
author: "Jonathan Lussier"
---
Anybody have a good source for K40 laser cutters in Canada? Also what modifications are required to use a 60W laser?

thanks





**"Jonathan Lussier"**

---
---
**Stephane Buisson** *May 14, 2015 05:13*

Hi Jonathan,



The K40 is generally sold on ebay. Not always with this reference, try simply something like "40W CO2 Laser Engraving Cutting Machine Engraver Cutter USB Port/CE"



price is moving a lot, today is average at 325GBP, got mine at 275GBP 6 months ago.


---
*Imported from [Google+](https://plus.google.com/+JonathanLussier/posts/XENXzqBvh4S) &mdash; content and formatting may not be reliable*
