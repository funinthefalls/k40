---
layout: post
title: "Finally got this running . yay. Just a file I found on thingiverse for now"
date: November 02, 2015 05:56
category: "Smoothieboard Modification"
author: "David Cook"
---
Finally got this running . yay. 


{% include youtubePlayer.html id="FgxU_5MdruQ" %}
[https://youtu.be/FgxU_5MdruQ](https://youtu.be/FgxU_5MdruQ)



Just a file I found on thingiverse for now. 









**"David Cook"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 02, 2015 07:12*

I notice in the video that certain lines in your design were done slightly slower & look like deeper cuts. Is there a reason for this?


---
**Stephane Buisson** *November 02, 2015 08:49*

HIHAA!!! well done.

will you try the Fusion 360 way for smoothie?

( free for unprofit use, download demo then you should have a mail after 30 days)


---
**David Cook** *November 02, 2015 17:40*

**+Yuusuf Salahuddin** yes I have no idea why it is doing that. Need to look at the gcode a little later.  Might be an issue with inkscape and or the plugin


---
**David Cook** *November 02, 2015 17:41*

**+Stephane Buisson** good idea. I'll try the fusion 360 tool soon.  


---
**David Cook** *November 02, 2015 20:07*

**+Allready Gone**  Thanks man,   I am not 100% successful until I have complete beam control lol.  as of now  and as seen, their is something odd happening with it making repeatable random dark lines lol. Going to try some other software workflows and learn more about all of this.   was neat so far.     the test last night was good to prove to my wife that I did kind of know what I was doing when I gutted the machine initially.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 02, 2015 20:14*

Are the lines that are randomly darker/slower cut actually random? I wonder is it just that particular file (since you said it's just a file you found on thingiverse). Have you tested with different files?


---
**David Cook** *November 02, 2015 20:31*

**+Yuusuf Salahuddin**  & **+Allready Gone** 

the darker lines  in that location on every try.   So it could be the file.   I need to try my own files next.  That makes sense  about if it slows down it would draw darker lines,  I just do not know why some of it "prints" fast and some lines slow,  could be part of the file,   Will experiment more tonight and this week.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2015 08:34*

**+David Cook** Any luck fixing that issue with the random darker/thicker lines?


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/cVxo8539duu) &mdash; content and formatting may not be reliable*
