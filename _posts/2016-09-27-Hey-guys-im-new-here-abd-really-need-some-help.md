---
layout: post
title: "Hey guys im new here abd really need some help..."
date: September 27, 2016 10:48
category: "Discussion"
author: "jason personette"
---
Hey guys im new here abd really need some help...



I have a k40 with no upgrades.  I had a little over 1k hours on the tube and my image quality started dropping.  I engrave inages to a anodized aluminum mayerial with my laser.  So i decided to order a new laser tube after some research and talking to people i decided on a new tube from light object that was to be a direct replacement per light object and was told by them it would be a higher quality more powerful tube.



Tube arrived.  I pulled the old tube out.  



I rinsed the new tube with distilled viniger to clean it and then with distilled water until all odors were gone.  



I installed the tube and wrapped the wire around the posts as tightly as i could using plyers to make sure it was uber tight.  Placed the rubber hose over the posts abd added silicone like it was from the factory.  



I cleaned all the mirrors and inspected them to make sure they were in good shape.



I cleaned the lense and inspected it as well.



Turned on the water pump and ket it run for a lil bit to work out all air bubbles.



Turned on laser and ran a test on masking tape and it burns the tape.  So i loaded up a anodized blank i have lasered onto multuple times the laser would no even leave a mark on the finish.



I checked the output of the powervsuply with a multimeter and it checks out fine.



I checked the power to the tube and it checks out fine as well 



Only thing i can see off is pictured here the laser seems to be hitting the internal metal collar.



I have placed the metal blank directly in front of the end of the laser tune and it wont even mark the material there either.....



I really need my lassr up and working should i get a new tube and replace the new one i just purchased or just say screw it abd order a new nachine since only like 100 bucks difference to get a new one im out of ideas on why this tube isnt working my old tube was working when it was removed so no reason the new one isnt unless its a bad tube correct?

![images/bf467b01bb91c91f45ea8db30d5fda40.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf467b01bb91c91f45ea8db30d5fda40.jpeg)



**"jason personette"**

---
---
**Phillip Conroy** *September 27, 2016 12:00*

Which country do live in,try new focual lens-even if old one looks good


---
**jason personette** *September 27, 2016 13:07*

Usa and i put the old tube back in and it marks the metal put the new one back and it wont mark but light object customer service answer is sorry naybe its not as powerful as your old one sorry....no other help even though i was told its a direct replacememt and higher quality 


---
**greg greene** *September 27, 2016 13:22*

Like vacuum tubes, there are only a few manufacturers of laser tubes in the range of sizes we use - guess what country supplies them.  Quality control is not high on their list of manufacturing goals.


---
**jason personette** *September 27, 2016 14:13*

So im screwed then order another tube and eat the price of this one


---
**greg greene** *September 27, 2016 14:14*

If LO won't give a refund or replace it - there aren't a lot of alternatives


---
**jason personette** *September 27, 2016 14:15*

:-(


---
**jason personette** *September 27, 2016 14:16*

I will never order from them again


---
**Alex Krause** *September 27, 2016 17:54*

I would send this picture to LO support  see what they have to say


---
**jason personette** *September 27, 2016 17:59*

I did they said well they cant control quality control in another company and they are sorry i paid via paypal im starting a claim with them they dont want to give my money back or send me a new tube.   Horrible customer service


---
**greg greene** *September 27, 2016 20:41*

Paypal will work for you - had a couple of incidents where the message is "Give the customer his money, replace the defective part - or you'll never receive another paypal transaction"



Very effective.


---
**jason personette** *September 27, 2016 20:42*

Nice


---
**Claudio Prezzi** *September 29, 2016 08:23*

The laser beam at the output of the tube is not focused an can not mark metal. But the metal plate in front of the tube can reflect the beam back in the tube an distroy it! Was the picture made with the metal in front the tube?


---
**jason personette** *September 29, 2016 08:49*

No the pucture was taken with the metal placed on the bed as where u would normally place things to be engraved 


---
*Imported from [Google+](https://plus.google.com/100446143268188419897/posts/LkMNZSArfLL) &mdash; content and formatting may not be reliable*
