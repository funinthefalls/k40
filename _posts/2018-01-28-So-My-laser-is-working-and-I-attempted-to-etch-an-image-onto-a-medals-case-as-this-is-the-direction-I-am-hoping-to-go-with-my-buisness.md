---
layout: post
title: "So. My laser is working and I attempted to etch an image onto a medals case as this is the direction I am hoping to go with my buisness"
date: January 28, 2018 08:24
category: "Discussion"
author: "Jason Smith"
---
So. My laser is working and I attempted to etch an image onto a medals case as this is the direction I am hoping to go with my buisness. The cases are made from leatherette fabric.. see pictures and lightburn settings used. 



How can I make this better then?

These boxes will be for veterans to put the war medals in. 



![images/16ac9c1db519161776b162605498a56c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/16ac9c1db519161776b162605498a56c.jpeg)
![images/eac9621cbe147d1a37e2e1572c752740.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eac9621cbe147d1a37e2e1572c752740.jpeg)

**"Jason Smith"**

---
---
**Jason Smith** *January 28, 2018 08:26*

[https://lh3.googleusercontent.com/KIVNxLmid6mHgiaepBgGVrr0nXDGTfHqGFCG8BWEZd9ghxNJVrH_Ji8HcpSsxdbX7TGSwV1LPA](https://profiles.google.com/photos/108083115075731642273/albums/6516015134953770737/6516015137571942914)


---
**David Davidson** *January 28, 2018 15:51*

My first thought is more speed and less power. On my little bit of testing  (still don't have proper ventilation in place) 10% and 100 speed is enough to get through masking tape and etch acrylic.


---
**Jason Smith** *January 28, 2018 21:42*

Thanks David. I will try your suggested settings. 


---
**John-Paul Hopman** *January 29, 2018 02:30*

Just saw on YouTube that laser etching fake leather can release toxic gases. Be safe.


---
**Jason Smith** *January 29, 2018 05:29*

Thanks John 


---
**HalfNormal** *January 29, 2018 15:24*

I always try low power, fast engrave first. I then bump up the power a little, then slow the engrave speed a little, back and forth until I get the results I am looking for.


---
*Imported from [Google+](https://plus.google.com/108083115075731642273/posts/6cTJPwCoNr6) &mdash; content and formatting may not be reliable*
