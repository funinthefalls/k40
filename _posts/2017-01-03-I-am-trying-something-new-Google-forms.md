---
layout: post
title: "I am trying something new, ... Google forms"
date: January 03, 2017 14:45
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
I am trying something new, ... Google forms. 



We all spend time a great deal of our time helping those that have problems with their K40.



One thing I notice is that sometimes trouble shooting information is lacking or inconsistent making providing help difficult. 



We also repeat a lot of questions every time a problem surfaces or alternately we forget to ask important questions that takes us and the victim on wild goose chases.



Its annoying to the victim to keep answering the same question and its frustrating for the helper to not have enough information to help.



Lastly, we are not capturing symptom and solution so that we can improve how we help or improve the design. 



I wanted to try out "Google Forms" and I wanted to have a standard set of questions to ask. To that end I decided to start an experiment using "Forms" in this community. 



Attached is a questionnaire for troubleshooting a LPS. The questionnaire is just a prototype so ignore the logic.



I intend to understand the limitations of this form tool and judge if it is useful for this application.



If you want to experiment with me please fill out the form as if you had a LPS problem. These are expected to be fake.





**"Don Kleinschnitz Jr."**

---
---
**Jim Hatch** *January 03, 2017 16:02*

Good job on this. Great idea.


---
**Richard Vowles** *January 03, 2017 17:36*

Agreed, superb idea.


---
**Ned Hill** *January 03, 2017 17:38*

Great Idea. 


---
**Phillip Conroy** *January 03, 2017 17:56*

Great idea


---
**HalfNormal** *January 03, 2017 22:12*

**+Don Kleinschnitz** Please enlighten me on flow of this process and how the results would be available for everyone to benefit from. I apologize if this questions seems snide or sarcastic. I am just really curious on the thought process.


---
**Don Kleinschnitz Jr.** *January 04, 2017 00:02*

**+HalfNormal** I was hoping for something like:



1. Member is victim of a problem. We send the set of pre-built questions that pertain to characterizing that problem.

2. Victim fills out form capturing the symptoms for the problem. 

3. The data is available to everyone to aid in trouble shooting without repeating questions and answers.

 4. That data is added to the summary statistics for that type problem.

3. Data about that type problem is kept for analysis for improvement.


---
**Tony Sobczak** *January 05, 2017 19:09*

Following




---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/LAJx1nuhf7p) &mdash; content and formatting may not be reliable*
