---
layout: post
title: "Hi Guys well again i need a little help i am using corel laser i seam to have lost one of the tool boxes and i don't know if it has an effect on the laser cutting but i am having a problem cutting a file in corel laser when i"
date: April 11, 2016 19:40
category: "Discussion"
author: "Dennis Fuente"
---
Hi Guys well again i need a little help i am using corel laser i seam to have lost one of the tool boxes and i don't know if it has an effect on the laser cutting but i am having a problem cutting a file in corel laser when i selected cutting a popup box used to come up where it had radial buttons for cutting outside , inside, and i think center well thats gone and i can't seam to get it back also it would good to know if it even has an effect on cutting thanks

Dennis 





**"Dennis Fuente"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 02:27*

I didn't even know that tool box exists. I'd imagine it doesn't make a great deal of difference, because the CorelLaser plugin reads it as a 0.01 line width, so that it can cut the line only once (instead of both sides), so I'd assume that it is running off the vector points for the line (which would be considered centre of the line).


---
**Dennis Fuente** *April 12, 2016 16:50*

Yuusuf 

Only reason i asked is i am having a problem with one file but i rescanned it and i am going to try it again thanks .



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 16:56*

**+Dennis Fuente** I would suggest you check your paths are all fully joined for your cutting.



However, what specific problem are you having with cutting the file? Might help narrow down a solution.


---
**Dennis Fuente** *April 12, 2016 18:17*

Yuusuf 

Thank you for your kind offer let me see if it's something i might have missed first and if i still have aproblem i will get back to you on it.

 I see you are thinking of upgrading your machine to a smoothie board 



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 18:54*

**+Dennis Fuente** Definitely considering it in the near future, as the LaserWeb software by Peter Van DeWalt is just a whole lot better & has a couple of features I've been wanting since day 1 (e.g. being able to engrave with different power levels, to create gradient like effects where some areas are deeper than others). Just a matter of $ & when I can afford it, but I'll definitely upgrade.



Also, I think it's worthwhile me learning more about these machines, as I have a few other ideas rattling around in my head for upgrades/total builds from scratch that currently I don't know enough of how things work in order to do them, but learning things like upgrading/replacing components will get me in the right direction for being able to make these other upgrades I wish to do.


---
**Dennis Fuente** *April 12, 2016 21:15*

Well i have done my share of changing out CNC machines hardware and software it's not all that easy but the rewards are great as you can use the machines with easier to learn software also more modern in application i just went through this with my 3d printer  but i was able to retain all the orignal componets and just flash the firmware and then use a slicing program to make my parts  I 3d printed an air assist nozzle for my laser machine works great it did help and i just placed an order for the light objects mirrors as the orignals were badly scracthed up when i did a target check of the laser alignment i found the beam actualy being split in two thanks for the help as i get used to this machine.



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 21:18*

**+Dennis Fuente** Ooo, you mention 3d printer. I don't have one, but I do have need of some parts printed in future. Would you be willing to do for a fee? I have a concept for an air-assist that I would like printed up at some point. Bonus is, you have a K40 so would be able to test it to see if it works. (I am in process of 3d modelling the part at the moment).


---
**Ben Walker** *April 13, 2016 11:20*

The problem with printed air assist is they tend to catch fire quite easily.  I printed every version in Thingiverse and wasted a LOT of filament in the process of melting my work.  Purchased the air assist head and new lens from lightobjects and found it is WELL worth cost and convenience of not having to worry about the head.  No more plastic parts in the bed for me.  Just my two pence


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 13:43*

**+Ben Walker** Thanks for sharing that Ben. The concept I have for an air-assist is not going to be in the beam path. It is a ring that attaches around the lens casing, that focuses the air in a cone shape down towards the focal point. Nothing will be in the path. I've seen the lightobjects ones, but I want to test this idea out as I believe it will be better for the dispersal of the smoke. When I finalise the 3d model I will post it to the group to show. Since you obviously have a 3d printer, can you tell me what is the minimum precision I can expect? As I don't have one, I don't know this. I am wondering can I print for e.g. a 1mm circular nozzle with 0.33mm walls & 0.33mm hole in the centre?


---
**Ben Walker** *April 13, 2016 13:54*

**+Yuusuf Sallahuddin** it's really depending on the nozzle size as far as how precise you can print.   .33 should not be difficult even with a .4 nozzle depending on how the print is positioned on the bed.   Of course one must also take into account shrinkage as the plastic cools.   And even when using ABS there may be a slight bit of melting.  Especially if you fail to use a raft.   I sure wish I had use for my 3d printer anymore.   I am so focused on laser designs.   For reference I bought the midrange monoprice printer.   I'm sure there are much nicer versions out there.   I am happy to have limited experience printing,  but sort of sorry for that purchase.   It's just taking space right now.   But that seems to be the case with creative people.   


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 14:26*

**+Ben Walker** Thanks for that clarification. I wasn't aware there was shrinkage in the 3d print. I'll take a look into shrinkage rates & rafts to attempt to cater for that. You sound like me, with all the random stuff I have laying around. I have many tools & materials from half-finished or unstarted projects. I'd love a 3d printer, but as with your situation, I imagine that if I had it, I would only use it occasionally & the rest of the time it would be a bit of a waste of space. Sometimes creativity can be a curse.


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/CgQHihhN5Di) &mdash; content and formatting may not be reliable*
