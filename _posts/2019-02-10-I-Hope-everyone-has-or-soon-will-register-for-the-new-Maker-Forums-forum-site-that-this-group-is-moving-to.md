---
layout: post
title: "I Hope everyone has, or soon will, register for the new Maker Forums forum site that this group is moving to"
date: February 10, 2019 03:56
category: "Discussion"
author: "Ned Hill"
---
I Hope everyone has, or soon will, register for the new Maker Forums forum site that this group is moving to.  I know change is hard but the Discourse platform is actually pretty nice with lots of features that I never saw in the older forums I have been a member of in the past.



I am a Moderator over there and I've been taking some time to learn how it all works to help you make the transition easier.  So, after you register please do the following:



1. Discobot - I know it may seem silly but please run through the Discobot new user tutorial as it will help you hit the high spots for how things work. Plus you will earn a cool badge when you are done.



2. Go to the "Site Help / Feedback" category and look through the How-To posts I have been creating for more tips on site features.



If you have any questions you can ask a question in the Site Help / Feedback category or message me directly (username Nedman) in the forum (yes, there is a How-To on messaging) :)  If you have trouble registering, post on here and I or one of the other mods will work it out for you. 



Thank you and I look forward to seeing you on the Maker Forums.





**"Ned Hill"**

---
---
**Ned Hill** *February 10, 2019 03:58*

Sorry forgot to add the Logo.  Definitely will not miss this part of G+.  On the Maker Forums platform you can go back and add pics anytime and at any position within the text of the post.

![images/1bbe7c6d6f7a6f3b8d38844f61de9819.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bbe7c6d6f7a6f3b8d38844f61de9819.jpeg)


---
**James Rivera** *February 10, 2019 22:54*

I’m going to miss the G+ notification icon to let me know that new replies to my posts/comments are there. 😕


---
**Ned Hill** *February 10, 2019 22:57*

I know it’s going to be a sad day ☹️

But you can still get notifications for the new forum if you use the Discourse Hub app. 🙂


---
**James Rivera** *February 10, 2019 22:58*

**+Ned Hill** Ok, I’ll take a look at it. Edit: does it do anything the web browser doesn’t?


---
**Ned Hill** *February 11, 2019 01:36*

**+James Rivera** yes. It gives notifications but when you tap the forum in the app it opens it in web browser mode. 


---
**James Rivera** *February 11, 2019 19:20*

**+Ned Hill** Ok, thanks. I neither need nor want notifications on my phone, so I’ll just bookmark the website.


---
**Martin Dillon** *February 14, 2019 02:10*

I signed  up for the weekly digest and got my first one today.  Its great.  One email a week to keep up with what is happening.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/RvGnXTAvY11) &mdash; content and formatting may not be reliable*
