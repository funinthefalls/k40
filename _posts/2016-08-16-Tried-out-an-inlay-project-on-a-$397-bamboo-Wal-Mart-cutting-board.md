---
layout: post
title: "Tried out an inlay project on a $3.97 bamboo Wal-Mart cutting board"
date: August 16, 2016 18:23
category: "Object produced with laser"
author: "Jeremy Hill"
---
Tried out an inlay project on a $3.97 bamboo Wal-Mart cutting board.  The inlay was from a sample pack of veneer - in this case a piece of oak veneer.  I did three passes on the laser for engraving with a pretty high setting for laser power and about 400mm/sec.  That set me up for the inlay but I believe a few more passes or slower would have been better.  The inlay would have been deeper and likely closer to flush with the board had I done that.  I just engraved the quote under the name with a single pass.  The veneer was a bit of a challenge trying to glue it in place but overall it came out ok for my first try.

![images/619bc117fe53d30e313b750eb842d930.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/619bc117fe53d30e313b750eb842d930.jpeg)



**"Jeremy Hill"**

---
---
**Bart Libert** *August 16, 2016 19:25*

Isn't it so that you can now get almost perfect quality by SANDING ? That way you will (with small grit) not see the difference where the woods touch




---
**Jeremy Hill** *August 16, 2016 19:28*

In this case, the veneer was so thin that additional sanding out basically just take the inlay out of the area where I had it.  I did try to do some sanding but found that I was starting to remove the material.  Again- my first shot at it, so I think I learned a few things and will have to try again sometime.


---
**VetGifts By G** *August 17, 2016 20:46*

What do you mean the power was high? I just tried the same bamboo board at 150 mm/sec and power about half way between 0 and 5 on the dial. The result was kind of light. I'm thinking of turning it up to 5 and speed to 300. Any advice?


---
**Jeremy Hill** *August 17, 2016 21:39*

I don't have marks on mine to give you a relative value. It was about 3/4 around on my dial


---
**VetGifts By G** *August 17, 2016 21:46*

Oh ok, so on mine is about a 1/4, if that. Im scared of turning it up, don't want to burn some stuff(still very new at this). Thanks!


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/25fLW4PfNGc) &mdash; content and formatting may not be reliable*
