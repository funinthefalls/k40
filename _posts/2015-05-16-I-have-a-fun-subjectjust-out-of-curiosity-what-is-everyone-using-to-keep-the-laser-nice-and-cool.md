---
layout: post
title: "I have a fun subject..just out of curiosity, what is everyone using to keep the laser nice and cool?"
date: May 16, 2015 18:32
category: "Hardware and Laser settings"
author: "Bee 3D Gifts"
---
I have a fun subject..just out of curiosity, what is everyone using to keep the laser nice and cool? We got a small mini fridge with a jug of distilled water inside. We drilled a hole in the fridge for the tubes. Our regular jug of water kept getting really hot, which caused the laser cutter to not cut well AT ALL.





**"Bee 3D Gifts"**

---
---
**ThantiK** *May 16, 2015 18:54*

A big portion of cooling is having <i>enough</i> water.  Our K40 uses a 5-gallon bucket of water on a concrete floor.


---
**Jim Coogan** *May 16, 2015 21:07*

My K40 also uses a 5 gallon bucket with 1 gallon of antifreeze and 3 gallons of distilled.  I have the pump in there and also a fish tank heater that keeps the water at 75 degrees F all the time.  I don't focus on keeping it cool as much as I focus on not thermal shocking the tube by having water that is too cold running through it.  That's a great way to crack the tube.  So my regular routine is to leave the pump and water heater on since they draw very little power and then before I use the laser I switch it on and leave it for 10 minutes or more.  I have not had an issue with it being too hot yet.  I am getting ready to order a temperature gauge and flow meter for the water so I can keep better track of it.  One reason my unit stays cooler is because I dumped the old fan and got a new one.  It creates a really nice breeze in both the electrical bay and in the laser tube bay.  I think having the laser tube bay enclosed causes a lot of heat to build up in the summer so I may ventilate the bottom of the laser tube bay so the fan can draw heat out and cooler air in from the other side. 


---
*Imported from [Google+](https://plus.google.com/101949715741571451458/posts/ZpwxpCaqV14) &mdash; content and formatting may not be reliable*
