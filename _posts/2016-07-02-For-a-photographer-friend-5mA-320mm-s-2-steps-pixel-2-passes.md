---
layout: post
title: "For a photographer friend. 5mA, 320mm/s, 2 steps/pixel, 2 passes"
date: July 02, 2016 17:29
category: "Object produced with laser"
author: "Tev Kaber"
---
For a photographer friend. 



5mA, 320mm/s, 2 steps/pixel, 2 passes. 1/4" poplar. 

![images/62232bbdbf323f90b06e3d168aca23ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/62232bbdbf323f90b06e3d168aca23ae.jpeg)



**"Tev Kaber"**

---
---
**Travel Rocket** *July 04, 2016 09:40*

great job! corellaser or laserdraw?

Where can you find the poplar setup?


---
**Tev Kaber** *July 04, 2016 11:50*

**+R Ninte** CorelLasr, I got the poplar from Lowes, they have an assortment of craft wood, though I think 1/4" is the thinnest they have, so cutting will take a couple passes. 


---
**Travel Rocket** *July 04, 2016 12:04*

cool. ive been having a hardtime engraving stuff coz they engrave per layer of the pixel very very slow even if my speed is 320 already. any tips?


---
**Tev Kaber** *July 04, 2016 12:45*

My CorelLaser etch defaulted to 2 motor steps per pixel, which I used for this, but if it's set to 1 step per pixel it will make a darker engraving.  I set my power to around 5 or 6mA for engraving.



Overall it is a bit slow, but I'm willing to wait the 10 minutes it might take.


---
**Travel Rocket** *July 04, 2016 12:54*

Oh i see,  but this one is engrave and you uncheck the sunken?


---
**Tev Kaber** *July 04, 2016 19:28*

You could do that, but in this case, the illustration was white text on black.


---
**Travel Rocket** *July 05, 2016 01:44*

Noted! Thanks Tev!


---
**Travel Rocket** *July 05, 2016 05:57*

what is your board model? M2 or M1?


---
**Tev Kaber** *July 05, 2016 11:13*

M2


---
**Travel Rocket** *July 05, 2016 11:35*

Same here, cool. what is the maximum power and speed you're setting on your laser. Im quite scared to make it 320 m/s lol


---
**Tev Kaber** *July 05, 2016 16:00*

I find 5mA or so at 320mm/s works quite well. Some people have gone up to 500mm/s, so 320 seems like a safe speed.


---
**Travel Rocket** *July 06, 2016 00:56*

Thank you, great work by the way


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/9c9gb62iVb8) &mdash; content and formatting may not be reliable*
