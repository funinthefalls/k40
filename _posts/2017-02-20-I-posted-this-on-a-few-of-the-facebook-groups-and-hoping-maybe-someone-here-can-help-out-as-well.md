---
layout: post
title: "I posted this on a few of the facebook groups and hoping maybe someone here can help out as well"
date: February 20, 2017 16:34
category: "Original software and hardware issues"
author: "Frank Dart"
---
I posted this on a few of the facebook groups and hoping maybe someone here can help out as well.



Hey folks, finally back with some good news and a new dilema.

After getting nowhere with the seller (Amonstar), paypal dispute and claim (paypal agreed to refund if I shipped it back to china :( ), I finally got a refund from paypal after filing a complaint with my local consumer fraud... Thats the good news, full refund plus I keep the laser.



Now for the new dilema... I want to order a new power supply, but do not want the original seller (Amonstar) to get another dime out of me.

Does anyone know of any of the sellers that A) Accept credit card as payment as I will not be using paypal, and B) Are not just another name being used that invoices back as Amonstar?

Thanks in advance!





**"Frank Dart"**

---
---
**Alex Krause** *February 20, 2017 16:37*

[www.lightobject.com](http://www.lightobject.com)


---
**Frank Dart** *February 20, 2017 16:44*

I've looked at that and besides the hefty price tag, if I'm correct, that one also doesnt include the 24v needed for the steppers does it?

I realize the higher price will surely be reflected in the quality versus the stock, but I also dont want to go down another rabbit hole after what I went through already.

Not to mention that my rewiring skills may not be quite up to the task :)


---
**Don Kleinschnitz Jr.** *February 20, 2017 18:27*

**+Frank Dart**​ are you running a stock K40 and controller​?

There are lots of options on eBay and Amazon that are direct replacements. However I can't vouch for any of them. I buy this stuff from Amazon as it's easier to get returns. 

How do you know your current LPS is bad? LO is the best choice but you will need a 24vdc supply that's not that hard to add. The benefit to adding  a 24v supply is extra capacity needed for accessories like rotary. 


---
**Frank Dart** *February 20, 2017 19:09*

**+Don Kleinschnitz** it is all stock.  I had posted back a while ago a video of me testing it and it got removed... Im assuming it was because it wasnt the safest way of doing it, but I believe it was you that agreed that it did prove that the hv output wasnt putting anything out, even if the method was madness.  From a quality standpoint, Im sure LO is the way to go for quality, however the cost is the biggest part of the equation... well if you dont count the pissed off wife that would rather me just cut the losses now and throw it in the nearest dumpster.

If I had it my way, I would have just went with the 50w to begin with, but as I said, cost is a huge factor and the reason most of us have a k40 to begin with :)


---
**Don Kleinschnitz Jr.** *February 20, 2017 23:11*

**+Frank Dart** I am not endorsing these per se because I do not know anything about their quality. They are cheap and are replacement supplies.   

Do you have green and white or all green connectors on your supply?



Others have purchased from eBay but I have never had any trouble with returns from amazon. 



[amazon.com - Amazon.com: Iglobalbuy 40W AC 110V/220V Co2 Laser Power Supply for Laser Engraving Engraver Cutting Cutter](https://www.amazon.com/Iglobalbuy-Supply-Engraving-Engraver-Cutting/dp/B01N4K82TC/ref=pd_sbs_201_1?_encoding=UTF8&psc=1&refRID=XRM4K3G39F9YDVQXH1FW)




---
**Frank Dart** *February 21, 2017 00:43*

Thanks Don, I actually did have that one bookmarked and may be the one I end up getting, but for some reason I was thinking that same seller name on fleabay tied back to amonstar as well... I may be wrong, but will probably be up until 2 am researching it lol... Out of curiousity, has anyone had any experience with [automationtechnologiesinc.com - Stepper Motor &#x7c;CNC Router &#x7c; Laser Machine &#x7c; 3D Printers For Sale Stepper Motor &#x7c; Stepper Motor Driver &#x7c; CNC Router &#x7c; Laser Machine &#x7c; 3D Printers For Sale](http://www.automationtechnologiesinc.com)

I see some good reviews of them on some cnc based forums, but nothing really recent.  They have one there for $150 but the wiring diagram shows the connectors there for the 24v but says not used???  


---
**Frank Dart** *February 21, 2017 01:17*

Well at least I wont be up until 2 looking up Iglobalbuy... found that both Iglobalbuy and Amonstar are both trademarks of Guangzhou Moutainnet Trading Co.,Ltd.  So I think its safe to assume they are indeed one and the same and will be getting no money from me lol.  








---
*Imported from [Google+](https://plus.google.com/116294342889465656724/posts/Rp2QoN4wKWa) &mdash; content and formatting may not be reliable*
