---
layout: post
title: "any ideas what is going on here?"
date: October 12, 2017 14:19
category: "Discussion"
author: "Ron Paquin"
---
any ideas what is going on here?  you can see the workspace trace.. then the cut travels outside the work area, and then the cuts  are on top of eachother...

![images/44e6b26a2a3c2e0013a8e65c45846e0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44e6b26a2a3c2e0013a8e65c45846e0f.jpeg)



**"Ron Paquin"**

---
---
**Ned Hill** *October 14, 2017 18:32*

Can you give a little more background?  E.G. New machine? Stock setup and software?


---
**Ron Paquin** *October 16, 2017 13:18*

**+Ned Hill**  had to widen the air assist outlet as i had ring reflection compounded by a split mirror 


---
**Ron Paquin** *October 16, 2017 13:19*

![images/ee0b846ef826b6f99e5072d95a183faa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee0b846ef826b6f99e5072d95a183faa.jpeg)


---
**Ned Hill** *October 16, 2017 14:36*

Ah yes it definitely looks like you found the problem.  Sucks about the mirror though.  


---
*Imported from [Google+](https://plus.google.com/+RonPaquinTESOL/posts/TWkFXNNJPop) &mdash; content and formatting may not be reliable*
