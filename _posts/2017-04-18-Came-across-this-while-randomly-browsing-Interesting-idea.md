---
layout: post
title: "Came across this while randomly browsing. Interesting idea"
date: April 18, 2017 18:14
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Came across this while randomly browsing. Interesting idea. Thought someone else may want to have a go at this.



[https://www.instructables.com/id/Pewter-Cast-Coins-From-Laser-Cut-Molds/](https://www.instructables.com/id/Pewter-Cast-Coins-From-Laser-Cut-Molds/)





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ned Hill** *April 18, 2017 18:28*

That's a cool idea.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2017 19:10*

**+Ned Hill** Yeah, looks pretty cool. I also thought that the same process could be used to create moulds to cast other things you may also need (e.g. metal tag/nameplate/keytag/etc).


---
**Ned Hill** *April 18, 2017 19:27*

Best price I could find for pewter was about $11/lb on Ebay.  Might also try to hit some local charity stores to see if I can find some cheap pewter pieces to scrap.  Got a burner and an old cast iron pot to melt them in.  Just need to get a casting ladle.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2017 19:57*

**+Ned Hill** Sounds like a pretty decent price at $11/lb. But I guess it depends how many pounds you are going to be needing. Will be interesting to see results once you have a play around.


---
**Ulf Stahmer** *April 19, 2017 10:30*

Thanks for posting **+Yuusuf Sallahuddin**! I've got to give this a try. I tried some Zinc Antimony casting many years ago in a silicone mold with limited success. My little guy loves coins and his birthday is coming up. Yet another thing is added to my bucket list!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 19, 2017 13:44*

**+Ulf Stahmer** Hopefully it works out well for you when you try make something for the little fellow. Good luck.


---
**Maker Cut** *April 20, 2017 18:31*

I have cast aluminum parts from 3d printed patterns. Creating a laser engraved pattern would work as well. Thanks for sharing this for the idea!


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/AZvmeEDcqAt) &mdash; content and formatting may not be reliable*
