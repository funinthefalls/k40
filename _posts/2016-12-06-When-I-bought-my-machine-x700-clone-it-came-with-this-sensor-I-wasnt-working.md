---
layout: post
title: "When I bought my machine, x700 clone it came with this sensor, I wasn't working"
date: December 06, 2016 23:45
category: "Discussion"
author: "Tony Schelts"
---
When I bought my machine, x700 clone it came with this sensor,  I wasn't working.  I turned it around etc but still no good.. Anyone in the uk have any idea where to get one. 

![images/22b55bf0613c422315dc52f19050a233.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/22b55bf0613c422315dc52f19050a233.jpeg)



**"Tony Schelts"**

---
---
**Jonathan Davis (Leo Lion)** *December 06, 2016 23:54*

Probably defective.


---
**Gee Willikers** *December 07, 2016 00:01*

I would thing any pressure switch would work. Most people get them from Amazon or eBay. I have this one: [amazon.com - White Plastic Shell Magnetic Water Flow Switch w Inner Outer Thread - Electrical Outlet Switches - Amazon.com](https://www.amazon.com/gp/product/B00AKVEGTU)


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/QCLvzyaq1Rs) &mdash; content and formatting may not be reliable*
