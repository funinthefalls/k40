---
layout: post
title: "I finished connecting my K40 to an AZSMZ Mini with an adapter board that allows switching back to the Moshiboard"
date: April 03, 2016 19:50
category: "Smoothieboard Modification"
author: "Vince Lee"
---
I finished connecting my K40 to an AZSMZ Mini with an adapter board that allows switching back to the Moshiboard.  As promised, here is a wiring diagram.  



I posted up a more complete buildlog and source files at [http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html](http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html)

![images/b7d5abf7b3a418fb3f782f8a7ba82d10.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b7d5abf7b3a418fb3f782f8a7ba82d10.png)



**"Vince Lee"**

---
---
**Vince Lee** *April 03, 2016 20:22*

I haven't gotten to the software much yet, as I burnt enough midnight oil this weekend getting this wired up and need to hang with the family ;).  So what besides laserweb should I try?  I already have inkscape, and have previously exported vectors to Moshidraw.  I understand there is a gcode exporter I can use for smoothie.


---
**Vince Lee** *April 03, 2016 20:26*

Awesome!  I am sure it would be better than Pronterface, which is what I was using while testing.


---
**ThantiK** *April 03, 2016 21:35*

Yaknow, I've been looking at this thing on Aliexpress for a while, wanting one pretty badly -- given that it has broken out stepper drivers, I think I might go ahead and pull the trigger on it.  Do you really need to keep the old moshi board though?


---
**Stephane Buisson** *April 03, 2016 21:41*

**+Vince Lee** we have a french expression (in a restaurant), saying "ce n'est pas fromage OU dessert", literally "it's not cheese or desert", (as you can have both).

My point is try Laserweb and Visicut and report back on both to community.

for your attempt with Visicut, look my 2 youtube video.

no need of gcode, inkscape to Visicut directly.


---
**Vince Lee** *April 04, 2016 00:24*

I probably didn't need to keep the Moshiboard, but for that matter I've gotten pretty used to it and have a lot of existing project files so I didn't really need to transition to the smoothieboard right now either.  But it sounded like a fun project to do, and now I can start using the amoothieboard at my leisure without any risk or pressure to switch to a new work flow right away.  It wasn't that hard to keep both, I figured, so why limit myself when I can maximize my options?


---
**HalfNormal** *April 04, 2016 03:45*

I have been thinking of doing the same. When my Nano board failed, I had a Ramps ready to go, and it did not! I also have an arduino and Gsheild that I put in and played with. Thinking of having both available.  I was going to write up my experiences but have not had time. Lets just say it was easier to use the Nano and Corellaser than the alternatives. I have a CNC so it was not a learning curve just a lot of work. LaserWeb was a whole other issue! Learned a lot and will share soon.


---
**Vince Lee** *April 10, 2016 07:26*

**+HalfNormal** I have a bunch of extra switcher boards.  I can send you one if you might find it useful.  Originally, I was going to use a panel-mounted switch to change controllers and ordered a big rotary 16pdt Russian military surplus switch to do the job, but decided the corresponding spaghetti in the control compartment would be too much.  I really like the double-throw dip switches I ended up using.


---
**Ariel Yahni (UniKpty)** *April 29, 2016 02:40*

**+Vince Lee** do you still have spare boards available?


---
**Vince Lee** *April 29, 2016 02:58*

**+Ariel Yahni** yup both a middleman board and some of my custom switcher boards.


---
**Ariel Yahni (UniKpty)** *May 01, 2016 23:17*

**+Vince Lee**​ would you sell me one of those switcher boards? 


---
**Jerry Hartmann** *May 16, 2016 18:09*

Im interested in a board as well


---
**Dan F** *May 19, 2016 21:19*

If you still have them I would be interested in purchasing a switcher board also.


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/YsNCc5Bnrzq) &mdash; content and formatting may not be reliable*
