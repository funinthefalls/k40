---
layout: post
title: "My first marquetry inlay attempt with the laser"
date: June 10, 2016 23:18
category: "Object produced with laser"
author: "Carl Fisher"
---
My first marquetry inlay attempt with the laser. Raster engraved a scrap piece of pine and then did a vector cut on a piece of veneer. Was a pretty good fit without much fuss but I'll have to do a larger sample to see how much laser kerf comes into play.



I'll turn the power down a bit on the next engrave as it was a bit too deep in the soft pine. I had it about 1.4v on the meter (didn't pay attention to the mA) and 350mm/sec speed and 1.1v on the cut at 25mm/sec.

![images/ee366ce9d9e8f038c846e50d6530a68c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee366ce9d9e8f038c846e50d6530a68c.jpeg)



**"Carl Fisher"**

---
---
**Anthony Bolgar** *June 10, 2016 23:47*

Looks nice!


---
**Josh Rhodes** *June 10, 2016 23:50*

That is amazing! I should try this..


---
**Don Kleinschnitz Jr.** *June 11, 2016 01:50*

Definetly on mybucket list ...nice


---
**Carl Fisher** *June 11, 2016 01:54*

Thanks all. It was really easy actually. I did a 3 second search on google images for dragon svg. Grabbed one for testing and imported it into Corel X5. Sized it roughly where I wanted and that was it. I did one engrave operation on one material and one cut operation on the veneer.



I didn't mess with inverting the image or allowing for kerf or angle on the burn since it was a pretty thin piece of veneer. Figured it wouldn't much matter and it really didn't in this case.



Brushed some wood glue into the engraved pocket and carefully fit the inlay in. Had one small piece break off and lost but otherwise went uneventful.


---
**Stephane Buisson** *June 11, 2016 07:08*

'Marqueterie' is definitely an very good application for our K40.

Laserweb from **+Peter van der Walt** manage kerf, a definite cool option

(haven't check LW3), the man himself would confirm it.(also needed for plasma cutting)


---
**Carl Fisher** *June 11, 2016 18:08*

Both Marquetry and Marqueterie are correct actually. That said I can't use Laserweb on a stock board.




---
**Tony Schelts** *June 11, 2016 18:36*

I as thinking of giving this a go but now you have done such a good job that i will definitely give it a o well done mate.


---
**Stephane Buisson** *June 14, 2016 14:59*

[https://plus.google.com/117750252531506832328/posts/LU751cenzNL](https://plus.google.com/117750252531506832328/posts/LU751cenzNL)


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/GiAAjVUUVHn) &mdash; content and formatting may not be reliable*
