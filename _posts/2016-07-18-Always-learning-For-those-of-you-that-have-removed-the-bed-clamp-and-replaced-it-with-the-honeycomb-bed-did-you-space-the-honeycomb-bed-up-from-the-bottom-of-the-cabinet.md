---
layout: post
title: "Always learning.... For those of you that have removed the bed clamp and replaced it with the honeycomb bed, did you space the honeycomb bed up from the bottom of the cabinet?"
date: July 18, 2016 19:54
category: "Modification"
author: "Terry Taylor"
---
Always learning....

For those of you that have removed the bed clamp and replaced it with the honeycomb bed, did you space the honeycomb bed up from the bottom of the cabinet? Seems like if you take the clamp and standoffs out it would be too low.





**"Terry Taylor"**

---
---
**Jim Hatch** *July 18, 2016 20:45*

It will be too low. You'll need to come up with an alternate bed and a method to adjust the height.



I generally only work with 3 different thicknesses of material so I chose a 3 layer bed - the first is a sheet of steel that will absorb any laser light vs reflect it. On top of that I have a pin bed I made with Strong-ties and then an expanded grill mesh. I then cut a set of MDF pieces sized so I could drop the steel plate on it and I was focused for my thickest material (1/4") I normally use. I have a couple of sheets of acrylic that I can slip in under the pin bed that raises it in 2 steps for the other thicknesses I use.



If I were doing it again I'd skip the supports and just drop the steel plate in the bottom. Then I'd use hexhead bolts of various lengths with magnets stuck on the steel and let the bolts support the expanded mesh. Easy as pulling bolts off the magnet to swap out sizes.



Or get a lab jack or electrically controlled Z bed (LightObject has one for the K40). I'll likely do that with the Smoothie upgrade I'm doing as I'll probably start using it for more things and will want the adjustability.



By the way, I also cut a piece of acrylic to use to gauge the space between the nozzle and the material. Easier than measuring 50.8mm from the lens. The little piece is something like 15 or 20mm.


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/PfDKqp7A3Z8) &mdash; content and formatting may not be reliable*
