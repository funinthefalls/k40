---
layout: post
title: "Can I get some links for some advised addons/mods"
date: April 30, 2016 02:38
category: "Modification"
author: "Alex Krause"
---
Can I get some links for some advised addons/mods 





**"Alex Krause"**

---
---
**Ariel Yahni (UniKpty)** *April 30, 2016 02:42*

I got the middleman ( 3 sets if you  end one an can wait will send it to you)  this [http://pages.ebay.com/link/?nav=item.view&id=401063450998&alt=web](http://pages.ebay.com/link/?nav=item.view&id=401063450998&alt=web)  and the LO with lense


---
**Alex Krause** *April 30, 2016 02:46*

I'm not really sure what the middle man board is for? Lol


---
**Ariel Yahni (UniKpty)** *April 30, 2016 02:52*

Me neither Lol


---
**Ariel Yahni (UniKpty)** *April 30, 2016 02:53*

**+Alex Krause**​ [https://weistekengineering.com/?p=2556](https://weistekengineering.com/?p=2556)


---
**Ray Kholodovsky (Cohesion3D)** *April 30, 2016 02:56*

The K40 uses a ribbon cable for the X motor and certain endstops, as these pieces move.  Middleman breaks them out to regular headers you can attach to your control board.  No controller so far has the ribbon cable connection built in.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 02:58*

That middleman seems to be 5v to 3.3v conversion (or the other way around).


---
**Ray Kholodovsky (Cohesion3D)** *April 30, 2016 03:01*

I haven't seen level shifting circuitry on any of them. In fact the conversion pics I've seen use a separate level shifting pcb. 


---
**Ariel Yahni (UniKpty)** *April 30, 2016 03:12*

**+Yuusuf Sallahuddin**​ conversion is done by a separate pcb ^^^^^ link


---
**Ariel Yahni (UniKpty)** *April 30, 2016 03:14*

Mind that this is also a very good idea [https://plus.google.com/107471533530265402525/posts/YsNCc5Bnrzq](https://plus.google.com/107471533530265402525/posts/YsNCc5Bnrzq)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 03:53*

**+Ray Kholodovsky** **+Ariel Yahni** Thanks for that. I read something in it's description about voltage, but only had a brief glance as I had visitors arrive.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/TrSWZcK6Ajm) &mdash; content and formatting may not be reliable*
