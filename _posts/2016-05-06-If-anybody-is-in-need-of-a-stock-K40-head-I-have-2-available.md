---
layout: post
title: "If anybody is in need of a stock K40 head, I have 2 available"
date: May 06, 2016 02:58
category: "Material suppliers"
author: "Anthony Bolgar"
---
If anybody is in need of a stock K40 head, I have 2 available. One has the mirror and lens, the other only the mirror. I am asking $350 for the complete one, and $20 for the one without the lens. Neither unit has air assist, but I can provide 3D printed air assist nozzles at no extra charge. These came out of lasers that I upgraded. They had very little use.





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *May 06, 2016 02:59*

Sorry, the complete one is $25.00 not $350...just a typo.


---
**Joe Spanier** *May 06, 2016 03:38*

Lol was gonna say, does it come with a laser? Haha


---
**Anthony Bolgar** *May 06, 2016 03:41*

No, but I will include an acre of Florida swamp land, or a 30% share in the Brooklyn Bridge.


---
**Alex Krause** *May 06, 2016 05:28*

Need to step up your game **+Anthony Bolgar**​ I already own 1:250000 share of a private island and was King for one minute of a castle that was rented in Europe :P


---
**I Laser** *May 07, 2016 03:19*

Sell them a used gold mine, there's plenty out your way :P


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/CAzv2BK6joB) &mdash; content and formatting may not be reliable*
