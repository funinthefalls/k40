---
layout: post
title: "I just got a 952GPH air pump to supply my air assist"
date: February 14, 2017 05:10
category: "Modification"
author: "timb12957"
---
I just got a 952GPH air pump to supply my air assist. I have heard that it is best to run it into a tank to avoid pulsing of the air stream. Can those of you who have this set up share what you are using as a tank?





**"timb12957"**

---
---
**Phillip Conroy** *February 14, 2017 05:37*

Watch out for moisture getting onto focal lens,wwhen Istarted using a shop 2hp 50 liter tank air ccompressor, Ihad to cclean lensevery 20 mminutes of cutting. 


---
**Ned Hill** *February 14, 2017 12:30*

**+timb12957** I use a 5 gal portable air tank I got from Harbour Freight.  When I am doing really long jobs I sometimes do have some moisture come through like Phillip is talking about, but it hasn't been a big deal for me.  I added an air filter post tank to catch any big drops.  I want to add an air drier eventually though.


---
**timb12957** *February 22, 2017 20:53*

Ned Hill, what is the inlet and outlet configuration? If the pump is straight in and the line straight out, don't you still get pulsing?  If the tank outlet is metered somehow,  wouldn't that cause back pressure on the aquarium air pump? (which I have read is bad for the that type of pump)


---
**Ned Hill** *February 22, 2017 21:29*

I have an airbrush compressor and I bring the line into the outlet hose side of the tank and out through the pressure gauge port.  Works fine for me with no pulsing and I work with at least some back pressure.  I think that most people with the aquarium pumps don't meter and don't worry with pulsing.  I would just try running it straight.


---
**timb12957** *February 23, 2017 12:15*

I am waiting on a drag chain to arrive. When it does I will try it with out a tank first. Thanks!


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/bhLWzzbx5f7) &mdash; content and formatting may not be reliable*
