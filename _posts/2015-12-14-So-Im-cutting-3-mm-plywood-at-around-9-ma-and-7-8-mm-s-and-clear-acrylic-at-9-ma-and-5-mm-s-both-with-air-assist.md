---
layout: post
title: "So I'm cutting 3 mm plywood at around 9 ma and 7-8 mm/s and clear acrylic at 9 ma and 5 mm/s both with air assist"
date: December 14, 2015 17:24
category: "Modification"
author: "Nathaniel Swartz"
---
So I'm cutting 3 mm plywood at around 9 ma and 7-8 mm/s and clear acrylic at 9 ma and 5 mm/s both with air assist.  Does that seem normal?  I clean my mirrors and lens before cutting but I'm still using the stock optics.  Should I upgrade that next?





**"Nathaniel Swartz"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 14, 2015 18:11*

My tests with leather 4mm thick goes at about 7mA & 2.5mm/s to cut through it. So 9mA @ 7-8mm/s sounds reasonable to me for 3mm plywood. Although I haven't done any ply yet myself.


---
**Scott Thorne** *December 14, 2015 20:11*

I'm cutting 1/8 plywood at 7ma at 7mm/s.....that's with an upgraded 18mm lens.


---
**Scott Thorne** *December 14, 2015 20:12*

I'm cutting the 3.3mm acrylic at the same speed as the plywood.


---
**Nathaniel Swartz** *December 14, 2015 20:22*

Thanks, I really need to upgrade my lens


---
**Scott Thorne** *December 14, 2015 20:28*

Anytime man.


---
**ChiRag Chaudhari** *December 15, 2015 18:41*

Hey guys none of you have mentioned PPM. Can you please tell me what PPM settings you have been using to make these cuts. And also what is the highest PPM you can use ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2015 18:50*

**+Chirag Chaudhari** What exactly is PPM? I didn't mention it as I haven't noticed it as a setting/value.


---
**ChiRag Chaudhari** *December 18, 2015 19:56*

**+Yuusuf Sallahuddin** You know what, my bad. I have upgraded to Rapms and that is why i kept asking for PPM. And you guys never mentioned PPM because you simply dont use that number. 


---
**Nathaniel Swartz** *December 27, 2015 20:03*

Thanks to those that recommended a new lense.  I'm now cutting 3 mm plywood at 5 ma and 8 mm/s.  The cuts are cleaner and show much less scorching.


---
**Scott Thorne** *December 27, 2015 20:04*

Your welcome man!


---
**Vince Lee** *March 02, 2016 00:58*

I replaced my lens with a less cloudy one, but still kept the original (~10mm) size because it was only

 $20.  I'm only getting about 8 mm/s for 1/8 birch plywood at 18ma (almost full power).  Is this normal for the smaller lens?  Seems like everyone else is getting much better results.  Does increasing the size make that much of a difference?  


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/Rmufz7uaVUo) &mdash; content and formatting may not be reliable*
