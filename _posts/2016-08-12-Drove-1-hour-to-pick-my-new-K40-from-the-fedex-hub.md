---
layout: post
title: "Drove 1 hour to pick my new K40 from the fedex hub!!!"
date: August 12, 2016 23:25
category: "Discussion"
author: "Scott Pollmann"
---
Drove 1 hour to pick my new K40 from the fedex hub!!!  Maybe I don't know what I'm doing, but it appears that my mirrors are aligned.  I can burn paper.



Question.  It is burning/cutting line by line, dot matrix style, is there a setting in corel draw 12 that I can use to change this?



How does one cut around the perimeter of an object, rather than the entire object line by line?



My laser tube doesn't have a wavelength stamp.  Shall I assume it's 10600nm and get some of these...[http://www.ebay.com/itm/161000996215](http://www.ebay.com/itm/161000996215)

or is there a more cost effective  (or better protecting!!!) pair?



If I must upgrade to cut better (or use mac), is this the gear I would need?



[http://www.ebay.com/itm/291795377796](http://www.ebay.com/itm/291795377796)





**"Scott Pollmann"**

---
---
**Eric Flynn** *August 12, 2016 23:55*

Firstly, yes it is 10.6.  Secondly, no need to order special glasses.  ANY ole safety glasses, or even normal eyeglasses (if you wear them) are more than adequate for 10.6.   Actually, there is little risk of eye damage from the light itself at 10.6, but rather the heat generated.  This wavelength does not enter the eye to burn the retina.  It has to burn through your cornea/lens first, and that is unlikely to happen.



Even though it burns paper, doesnt mean it is aligned properly.  It very likely is not.



If cutting, it will follow the vector path. 



If engraving, it will go line by line in whatever fashion you have it set to do.  IE back and forth, one direction, horizontal or vertical, etc.  it will do engraving sort of "dot matrix" style, and the density is set by the DPI setting.



To cut around the perimeter of an object, you draw a path around the object you want it to cut, then choose the cutting method.



In CorelLaser there are two options in the tool bar, Engraving, and cutting.  Cutting mode will follow a vector path, engraving will raster.


---
**Jim Hatch** *August 13, 2016 00:52*

Regardless of the setting (cutting vs engraving) if you want it to cut, make sure the line thickness (stroke weight) is 

.001mm  or it will cut multiplier times and potentially raster it (back & forth like an inkjet printer). Thick lines are raster objects and not really "lines".



Also, I don't believe safety glasses or eyeglasses in the U.S. are 10.6nm safe. But that may be different in other countries. Remember it is IR and invisible.



BTW, with the lid closed there's no real danger from the laser light but the light from the burn can be really bright so don't stare for hours at it tracing objects 😀




---
**Eric Flynn** *August 13, 2016 01:00*

**+Jim Hatch**  Sure they are.  Just do a little research.  Its well known in the laser industry/community outside this hobbyist venue, that normal safety glasses, or eyeglasses are more than adequate.


---
**Scott Pollmann** *August 13, 2016 02:08*

Thanks guys.  I appreciate it.


---
**Jim Hatch** *August 13, 2016 03:47*

**+Eric Flynn**​​ Do you have a source for that? ANSI Z136.1 and EN207/EN208 don't seem to apply to standard safety glasses that are usually Z87 which is about shatter protection vs Z136.1 being about light transmissivity. There's also wavelength and OD to consider. It'd be great if standard safety glasses would work but I can't find anything to suggest that's the case from an official source.


---
*Imported from [Google+](https://plus.google.com/103382513064902122956/posts/1Uzhy98t2BV) &mdash; content and formatting may not be reliable*
