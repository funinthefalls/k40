---
layout: post
title: "K40 whisperer takes several minutes to load file and also takes several minutes before it starts to print"
date: February 18, 2018 22:15
category: "Software"
author: "Peter Alfoldi"
---
K40 whisperer takes several minutes to load file and also takes several minutes before it starts to print. What am I doing wrong? I use an older laptop, though using Corel and chinese software it was working just fine. I have tried exporting DXF in various versions, but does not seem to make any difference.





**"Peter Alfoldi"**

---
---
**Anthony Bolgar** *February 18, 2018 22:36*

That is normal. If you look at the status bar at the bottom of the program window, it lets you know what it is doing after the file loads (Scanning rasters, creating packets etc.)


---
**Scorch Works** *February 19, 2018 21:05*

It might go faster if you convert it to an SVG file before bringing it in to K40 Whisperer.  I would be iterested to know if is faster as an SVG.  If it is faster I might be able to speed up DXFs.


---
**Anthony Bolgar** *February 19, 2018 22:08*

I find it is the loading that takes the most time, generating the toolpath is decent, but I am running a 6 core Phenom @3GHZ with 16GB RAM




---
**Peter Alfoldi** *February 19, 2018 22:15*

Yep, it is loading that takes a while - which would be OK if there would be some loading bar or something - currently it acts like the application is frozen :)

Also, once the design file is loaded, any interaction with the K40 (either homing, or initializing) takes some time too (10+ seconds).

If there is no file loaded, the response is instant.


---
**Anthony Bolgar** *February 19, 2018 22:19*

I dio not have any initializing or homing delays with the file loaded. How big is the average file you are using?




---
**Peter Alfoldi** *February 19, 2018 22:27*

Interesting. File is DXF, about 1MB


---
**James Rivera** *February 20, 2018 17:58*

I’m using a very old laptop (it says, “Vista Capable” on it) running Windows 8.1 (it refused to install Windows 10), and K40 Whisperer has been quick for me, but I’ve only used SVG files created in Inkscape.


---
*Imported from [Google+](https://plus.google.com/105128850753393988085/posts/G8rKS9jqNEe) &mdash; content and formatting may not be reliable*
