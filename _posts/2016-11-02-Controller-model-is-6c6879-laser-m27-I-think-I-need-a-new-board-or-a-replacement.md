---
layout: post
title: "Controller model is 6c6879 laser m2:7 I think I need a new board or a replacement"
date: November 02, 2016 15:30
category: "Original software and hardware issues"
author: "John von"
---
Controller model is 6c6879 laser m2:7 I think I need a new board or a replacement. Where can I find one? I appreciate your advice. John von

![images/26f3c36e444a87e467f70a6650df6c1d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/26f3c36e444a87e467f70a6650df6c1d.jpeg)



**"John von"**

---
---
**Anthony Bolgar** *November 02, 2016 15:44*

Ebay usually has some, but now might be a good time to upgrade it to a smoothieboard and use LaserWeb. All depends on your skill level, but the upgrade is fairly easy, quite a few of us have done it, so help is available.


---
**John von** *November 02, 2016 23:25*

Anthony,

how expensive is the smoothieboard? Thanks for the advice.


---
**Anthony Bolgar** *November 03, 2016 02:45*

Depends on the model but a 4x runs about $135.00 A replacement of the crappy Chinese controller will set you back about the same, so from a value perspective you will get a much better product with the smoothieboard for about the same price.


---
**Timo Birnschein** *November 05, 2016 02:18*

**+John von** I completely agree with **+Anthony Bolgar**! Change the board and free the laser from this Corel Laser stuff that is likely on the edge of legality.


---
**Anthony Bolgar** *November 05, 2016 02:29*

The version of Corel the K40 ships with is definitely an in the center, no where near the edge of legality. it is an ILLEGAL COPY of the software. And think of the irony, the controller card is hardware dongle protected so it can't be pirated, but only works with pirated software?


---
**Timo Birnschein** *November 05, 2016 02:32*

Yeah, that BS drove me into building my own board for the K40 laser. I was forced to "test" a newer version of Corel Laser (because the "original" one was so buggy that almost nothing worked) and it just quit work after some time because self of destruction. I will never use it again, lesson learned. LaserWeb 3 or Inkscape or other software works better and is completely legal.




---
**Anthony Bolgar** *November 05, 2016 02:33*

Now if money is an issue, you can also use a GRBL shield running on an Arduino UNO for around $20 -$30 dollars. It will work with LaserWeb almost as well as the Smoothieboard, and it requires the same level of technical skill to install. And since the only thing different in the upgrade would be an grbl shield instead of a Smoothieboard, you could start with GRBL, use Laserweb and the in the near future once you are comfortable with LaserWeb and your k40, you can always quickly upgrade to a Smoothieboard then. Hope this helps you make a decision, if you have any other questions or concerns, please feel free to ask questions.


---
**Timo Birnschein** *November 05, 2016 02:43*

That is precisely what I did :)

Click on my profile and you'll see the board and everything...


---
*Imported from [Google+](https://plus.google.com/112642592493783290997/posts/BZ7Fj5XakWZ) &mdash; content and formatting may not be reliable*
