---
layout: post
title: "I know that most of the people here use LaserWeb but any experience with VisiCut?"
date: November 10, 2016 23:06
category: "Software"
author: "Damian Trejtowicz"
---
I know that most of the people here use LaserWeb but any experience with VisiCut?

I like there mapping by layers or colors and how we can change order of cutting(internal,smaller first ,etc)







**"Damian Trejtowicz"**

---
---
**Alex Krause** *November 11, 2016 03:37*

How about you post a feature request on the Laserweb github?


---
**Damian Trejtowicz** *November 11, 2016 06:26*

I seen laserweb4 and cant wait to use it.

Big positiv of laserweb is control over machine


---
**Stephane Buisson** *November 11, 2016 06:57*

Look like you already see my Youtube video


{% include youtubePlayer.html id="lbTTPkDEhOg" %}
[https://www.youtube.com/watch?v=lbTTPkDEhOg](https://www.youtube.com/watch?v=lbTTPkDEhOg)

**+Damian Trejtowicz**


{% include youtubePlayer.html id="lbTTPkDEhOg" %}
[youtube.com - Workflow Visicut](https://www.youtube.com/watch?v=lbTTPkDEhOg)


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/a7fcBv2ML3Q) &mdash; content and formatting may not be reliable*
