---
layout: post
title: "Does anyone have an exhaust fan recommendation for Europe?"
date: April 20, 2016 18:24
category: "Modification"
author: "Michael Ang"
---
Does anyone have an exhaust fan recommendation for Europe? I tried a 125mm "mixed flow" fan but it's not sufficient. I was looking at this 150mm or 200mm radial plastic fan. Any thoughts? Thanks. 150mm - 460 m³/h (272cfm) [http://www.amazon.de/DALAP-Rohrventilator-dalap-Turbine-P-150mm/dp/B00JMB9H4S](http://www.amazon.de/DALAP-Rohrventilator-dalap-Turbine-P-150mm/dp/B00JMB9H4S) - 200mm - 780 m³/h (460cfm) [http://www.amazon.de/DALAP-Rohrventilator-dalap-Turbine-P-200mm/dp/B00Y0BFHXU](http://www.amazon.de/DALAP-Rohrventilator-dalap-Turbine-P-200mm/dp/B00Y0BFHXU) Photo is of my current setup with 125mm flex tubing

![images/07cae3e08a737e62c5b33d8bfb9d6548.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07cae3e08a737e62c5b33d8bfb9d6548.jpeg)



**"Michael Ang"**

---
---
**Tony Schelts** *April 20, 2016 18:51*

This is the one I got of amazon it works amazingly well. hardly any smell including when cutting Acrylic

[https://www.amazon.co.uk/gp/product/B00MWD284O/ref=oh_aui_detailpage_o08_s01?ie=UTF8&psc=1](https://www.amazon.co.uk/gp/product/B00MWD284O/ref=oh_aui_detailpage_o08_s01?ie=UTF8&psc=1)


---
**Michael Ang** *April 20, 2016 20:25*

Thanks, ordered!


---
**Tony Schelts** *April 21, 2016 22:57*

Let me know how you get on with it.. What are you using your laser for.  hobby or trying to make money. ??


---
*Imported from [Google+](https://plus.google.com/101545054223690374040/posts/A76djZ4R7cA) &mdash; content and formatting may not be reliable*
