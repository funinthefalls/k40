---
layout: post
title: "Got my ACR+Smoothie today, cant wait to start putting it in tomorrow, thanks Scott Marshall"
date: October 11, 2016 22:31
category: "Modification"
author: "Mike Grady"
---
Got my ACR+Smoothie today, cant wait to start putting it in tomorrow, thanks Scott Marshall





**"Mike Grady"**

---
---
**Ariel Yahni (UniKpty)** *October 11, 2016 22:35*

Great combo, you should have a very smooth upgrade path


---
**Mike Grady** *October 11, 2016 22:45*

I have been using the stock M2 nano for a couple of months, it does work well but too restricted, i want to do grey scale engraving and hopefully some 2.5d work using reliefs, i dont know if thats really possible on the K40 but i'll give it a shot and see what happens


---
**Ariel Yahni (UniKpty)** *October 11, 2016 22:49*

Sure you can, just need to find the sweet spot. Are you familiar with LaserWeb?


---
**Mike Grady** *October 11, 2016 22:51*

thats what i'll be using with the smoothie, i have got it installed already


---
**Bill Keeter** *October 11, 2016 23:04*

Nice man, I've got Scott holding one of his ACR kits for me. Just waiting on him to have some X4 boards in.


---
**Mike Grady** *October 11, 2016 23:44*

Yea he told me he had run out of the x4 so he asked if i would take the x3 instead, i couldnt see it being a problem so took the x3 instead


---
**Scott Marshall** *October 11, 2016 23:44*

I just added a "Quick start" setup list for the latest rev of Laserweb3  to the manual. It's not tough stuff, but some people have had crashing and laser firing  problems because they forgot to go in and add the setup numbers or didn't know what the correct ones were. It's just a starting place, but gets you running quick and easy. Anyone that would like a copy just send me an email and I'll send it out to you.(it's a table and I'm not sure if it will post here)



Mike, Glad to hear it made the trip, That's 2 in the UK. I'll send you the LW3 settings table.



Bill, Yours is on deck, Just sent out the last 'routine order" and will build yours up so it's ready to go when the 4x boards arrive.



I DO have a couple boards left, if anyone would like one, please let me know. ETA on the next batch is 2-3 weeks (it's only 3 to verify my board design, then I'll be placing a larger order)



Ariel - Yours follows Bills, for real this time. Unless I experience a calamity. I apologize for the long wait. Have not forgotten your blog either. Hoping to 'catch up" here between orders, but I have a couple gentlemen who are patiently waiting for those from back in June (I think, it's a blur)



Thanks Guys, Scott


---
**Mike Grady** *October 11, 2016 23:58*

Thanks Scott


---
**Ariel Yahni (UniKpty)** *October 12, 2016 00:46*

**+Scott Marshall**​ customer first. Keep pushing


---
**Er Brad** *October 12, 2016 06:29*

Can you send me a quick start setup guide**+Scott Marshall** 


---
**Scott Marshall** *October 13, 2016 11:00*

**+Er Brad** I need your email, send it to me @ ALLTEK594@aol.com, or use the link on my site [ALL-TEKSYSTEMS.com](http://ALL-TEKSYSTEMS.com)



[all-teksystems.com - all-tek-systems](http://ALL-TEKSYSTEMS.com)


---
*Imported from [Google+](https://plus.google.com/106807531578115449401/posts/P8vfRwPqDQd) &mdash; content and formatting may not be reliable*
