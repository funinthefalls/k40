---
layout: post
title: "Ok, with a lot of the G+ maker groups I follow moving to separately run Discourse forums I went to see what apps might be available to make keeping tabs on things easier"
date: February 04, 2019 22:43
category: "Discussion"
author: "Ned Hill"
---
Ok, with a lot of the G+ maker groups I follow moving to separately run Discourse forums I went to see what apps might be available to make keeping tabs on things easier.  



The very basic app is Discourse Hub.  This app doesn't require a sign up and does very basic notification for each of the forums you add.  When you click on the forum in the app it opens the forum in your mobile browser.



There are other apps (tapatalk, Imore forums) with more in app features, but they require a signup and the forums themselves have to also sign up to allow you to use the apps.  



So for right now I'm am going to use the Discourse Hub app.  Looks like it's available in iOs and Android.





**"Ned Hill"**

---
---
**Don Kleinschnitz Jr.** *February 05, 2019 02:27*

I downloaded the Droid version of discourse. Not terrible but I already miss G+?


---
**Jamie Richards** *February 05, 2019 17:54*

Discourse isn't bad. 


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/Bnh8CqVw7p7) &mdash; content and formatting may not be reliable*
