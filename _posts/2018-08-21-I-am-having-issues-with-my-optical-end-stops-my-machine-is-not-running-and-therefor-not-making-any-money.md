---
layout: post
title: "I am having issues with my optical end stops, my machine is not running and therefor not making any money.."
date: August 21, 2018 16:15
category: "Original software and hardware issues"
author: "Jason Vanderwaal"
---
I am having issues with my optical end stops, my machine is not running and therefor not making any money.. I am wondering if there is a step by step guide on converting them to mechanical end stops, IE: what kind of end stops to buy, do I need the cohesion3d board? my Y axis moves fine, buy X axis does not and the optical sensor seems to be split, im not sure if it slammed into the stop..



Any help would be appreciated, I need to get my machine up and running again soon..





**"Jason Vanderwaal"**

---
---
**S Th** *August 21, 2018 17:05*

Mine came with mechanical end stops that look just like the ones in the link below. It uses the same M2 Nano board that most versions do, and the end stops plug in to the same place. I would imagine you could just plug in the mechanical end stops with no problem.

[aliexpress.com - 10PCS 3 pins Micro Touch Switches NO+NC 3a/5a 125VAC 250VAC Mini limit Switch Micro Switch Microswitches With 29mm Long Lever](https://www.aliexpress.com/item/10PCS-MICROSWITCH-LIMIT-SWITCH-MICRO-SWITCH-with-long-lever-free-shipping/1911059086.html?spm=2114.search0104.3.8.70342dfaeWIhAm&ws_ab_test=searchweb0_0,searchweb201602_5_10152_10065_10151_10344_10068_10130_10342_10547_10343_10546_10340_5724715_10548_10341_10545_10696_10084_5724015_10083_10618_5724315_10307_5724215_5724115_10059_100031_5725015_10103_5724915_10624_10623_10622_10621_10620,searchweb201603_2,ppcSwitch_5&algo_expid=617e6544-51a2-44bf-9f1f-cde83a17f30c-1&algo_pvid=617e6544-51a2-44bf-9f1f-cde83a17f30c&priceBeautifyAB=0)


---
**Don Kleinschnitz Jr.** *August 21, 2018 19:51*

Check my blog for repair proceedures for optical end stops. 


---
**Tech Bravo (Tech BravoTN)** *August 21, 2018 20:27*

[donsthings.blogspot.com - Repairing K40 Optical Endstops](http://donsthings.blogspot.com/2017/08/repairing-k40-optical-endstops.html)




---
**Tech Bravo (Tech BravoTN)** *August 21, 2018 20:29*

as for replacing them with mechanical switches, the controller was built to handle the mechanical switches. The power and logic needed for the optical switches comes from the optical daughter board so fitting mechanical endstops would be easy to do.




---
**Jason Vanderwaal** *August 21, 2018 21:41*

Yeah, I'm not greatly technologically inclined, but the sensor under the gantry is split down the middle, it's like the stop didn't stop and slammed into the sensor, after it did that the x axis belt came off.


---
**Jason Vanderwaal** *August 21, 2018 21:43*

As for "easy to do" how easy? Especially with the ribbon cable is there a step by step tutorial on converting to mechanical end stops?


---
**Tech Bravo (Tech BravoTN)** *August 21, 2018 22:06*

Here is a kit with what you need to convert it: [https://k40laser.se/k40-parts/gantry-diy-kits/end-stop-upgrade-mechanical/](https://k40laser.se/k40-parts/gantry-diy-kits/end-stop-upgrade-mechanical/)

[k40laser.se - End stop upgrade - mechanical - K40 Laser](https://k40laser.se/k40-parts/gantry-diy-kits/end-stop-upgrade-mechanical/)


---
*Imported from [Google+](https://plus.google.com/103606219181253590128/posts/BonmFwXnt55) &mdash; content and formatting may not be reliable*
