---
layout: post
title: "Does anybody has made some efforts to reduce fumes?...from cutting?"
date: March 26, 2016 00:26
category: "Discussion"
author: "Jose A. S"
---
Does anybody has made some efforts to reduce fumes?...from cutting?





**"Jose A. S"**

---
---
**Brien Watson** *March 26, 2016 02:32*

It's essential to remove the fumes is the best way you can.  My cheap laser come with the big plastic fan that doesn't really fit.  I am going to modify this with an inline fan inside a four inch pipe


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 26, 2016 04:36*

A lot of people in this group have modified their exhaust system with different fans/blowers, venting through clothes dryer vents, or special panels made to fit windows, or other methods.



Myself, I am still using the stock exhaust fan but I have extended the exhaust hose to outside my house (I work in garage, so I go out under the roller-door).


---
**Tom Spaulding** *March 26, 2016 22:00*

Look at the bilge fans on ebay. For $25 you can move a lot of exhaust fumes.


---
**Don Kleinschnitz Jr.** *March 29, 2016 14:19*

I tried lots of stuff and finally bit the bullet got a 450 cfm fan off amazon, a nozzle pump (HF), and modified my K40 exhaust using Lowes floor ducts, screwed to the back. I vented a 4 " pipe to an outside window. I am more than satisfied. My cutting is stellar and my unit is in my upstairs makershed with no fumes in the house. My advice is don't undersize the evacuation system.


---
**Jose A. S** *March 29, 2016 14:53*

**+Don Kleinschnitz** Thank you for your advice. Can I see pics? please!


---
**Don Kleinschnitz Jr.** *March 29, 2016 15:09*

How the heck do I attach a picture?




---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 07:09*

**+Don Kleinschnitz** You technically cannot attach pictures to replies. Unless you upload to Google Drive/Dropbox/image hosting & paste the link to the file. Other option is to create a new post & tag whoever it is for in it.


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/FrELrgfTk4k) &mdash; content and formatting may not be reliable*
