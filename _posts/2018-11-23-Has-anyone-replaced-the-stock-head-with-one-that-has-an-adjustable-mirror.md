---
layout: post
title: "Has anyone replaced the stock head with one that has an adjustable mirror"
date: November 23, 2018 03:24
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Has anyone replaced the stock head with one that has an adjustable mirror.

If so what one did you use?





**"Don Kleinschnitz Jr."**

---
---
**Joe Alexander** *November 23, 2018 03:53*

I haven't done it yet but I got one of these hoping to replace the LO one i bought:

[sign-in-global.us - [$ 45.69] CO2 Laser Cutting Head with Air Assist and Laser Dot Frame for Installing Dia 20mm Lens and Dia 25mm / 1" Mirror](https://www.sign-in-global.us/products/20681/co2_laser_cutting_head_with_air_assist_and_laser_dot_frame_for_installing_dia_20mm_lens_and_dia_25mm_1_quot_mirror.html?ex=USD&utm_source=Googleshopping_us&utm_medium=Googleshopping&utm_campaign=Googleshopping&gclid=Cj0KCQiAxNnfBRDwARIsAJlH29DsEvASsLg4nFtHTctkK9HoeO5qZxsBTa1PG_aEO6J9866w3h3yf4YaAs-JEALw_wcB)


---
**Don Kleinschnitz Jr.** *November 24, 2018 03:10*

I'm thinking about this one but not sure how to set up for 38mm lens. 



Cloudray CO2 Laser Head Dia 18mm FL 50.8mm for CO2 Laser Cutter DIY [https://www.amazon.com/dp/B07K2X6F43/ref=cm_sw_r_cp_apa_Rcm-BbDC5MPHJ](https://www.amazon.com/dp/B07K2X6F43/ref=cm_sw_r_cp_apa_Rcm-BbDC5MPHJ)


---
**Don Kleinschnitz Jr.** *November 25, 2018 19:09*

**+Joe Alexander** I emailed cloud ray and they recommended the unit that you bought. 

Is that unit adjustable for using different FL lenses. I am still confused as to how you use different FL lenses in that unit.


---
**Joe Alexander** *November 25, 2018 20:23*

it does have an adjustable tube on the bottom to change the distance but the lens mounts kinda within that section from what I discerned, or just above it. maybe they have different length tube pieces for the various cutting lens depths to compensate?


---
**Don Kleinschnitz Jr.** *November 27, 2018 14:46*

**+Joe Alexander** this is what cloudray reccomends:



#K40Optics



Buy the head for 50.8mm and then get the nozzle for 38.1

Note: the mirror is 25 mm.

I haven't checked if it will fit into the K40 yet!



[https://www.cloudraylaser.com/collections/laser-head-mirror-base-1/products/co2-laser-head-for-focus-lens-dia-20-fl-50-8-101-6mm-mirror-25mm?variant=45520553096](https://www.cloudraylaser.com/collections/laser-head-mirror-base-1/products/co2-laser-head-for-focus-lens-dia-20-fl-50-8-101-6mm-mirror-25mm?variant=45520553096)



[cloudraylaser.com - Short Nozzle with Fitting for D20 F38.1mm 1.5" Lens](https://www.cloudraylaser.com/collections/nozzles-lens-tube/products/short-nozzle-with-fitting-for-d20-f38-1mm-1-5-lens)


---
**Don Kleinschnitz Jr.** *November 28, 2018 13:15*

**+Joe Alexander** I looked at the machine and dimensions of these tubes. I do not see any way they can fit, way to long. Moving toward designing my own.




---
**Joe Alexander** *November 28, 2018 15:55*

good to know. I guess ill save mine for the one im building, albeit very slowly lol


---
**Nigel Conroy** *December 04, 2018 00:39*

Have you looked at the replacement head that Russ Sadler put on his? 

You need an adjustable table for it to work but I just finished the upgrade on my k50 (China blue) machine and very happy with performance![images/e953ca75e28847f434dd92926d06480e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e953ca75e28847f434dd92926d06480e.jpeg)


---
**Don Kleinschnitz Jr.** *December 04, 2018 15:40*

**+Nigel Conroy** Yes I am looking at it and may use it to model one for the K40. Did you find Rus's plans I could not?


---
**Nigel Conroy** *December 04, 2018 17:02*

**+Don Kleinschnitz Jr.** I did contact Russ directly, but that was because I was buying the bracket from him. He sent me files and data sheets and has a tutorial for the build and install.

The files are uploaded on 3dpassion (link below)



I did have to make adjustments to the mirror position and edit the head in order to get perfect alignment with my machine. He said about 2 of them have not lined up perfectly. This is designed for the k50 so obviously would require modification for the K40.



I've only had mine installed for a few days but the performance improvements on cutting alone are awesome. 9mm/sec on 1/4 mdf to now cutting at 13-14mm/sec at the same power. 



[3dpassion.com - ChinaBlue Head Transplant 3D printing model](https://www.3dpassion.com/model/chinablue-head-transplant/7504)


---
**Don Kleinschnitz Jr.** *December 04, 2018 18:26*

**+Nigel Conroy** thanks for this info it will be much help me get moving on this. 

Was the increase in performance from just the need head or did you change the lens's?

What lens configuration are you using?



Again thanks...


---
**Nigel Conroy** *December 04, 2018 18:31*

The performance increase on the cutting was the ability to have the focal distance of a 2in lens 4mm from surface. Forcing the air assist into the kerf. Also got the cutting nozzle with a small bore in the end, again forcing air into kerf and therefore smoke out of it.




---
**Don Kleinschnitz Jr.** *December 04, 2018 19:22*

**+Nigel Conroy** ah, got it thanks.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/g6QHpog1rjF) &mdash; content and formatting may not be reliable*
