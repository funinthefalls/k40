---
layout: post
title: "Hi all, Until another board arrives, I have some UNO CNCshield v3 lying around"
date: January 30, 2017 20:30
category: "Modification"
author: "Jorge Robles"
---
Hi all,



Until another board arrives, I have some  UNO CNCshield v3 lying around. Is there any good setup to beef up a K40 with that? Also, any schematic to do a middleman adapter.

I've lost an while searching out there but I only find "chitchat videos" of how cool is this or that but no a good old blog with connections and schematics.



Thanks!





**"Jorge Robles"**

---
---
**Ariel Yahni (UniKpty)** *January 30, 2017 20:38*

**+Don Kleinschnitz**​ is the man for that


---
**Don Kleinschnitz Jr.** *January 30, 2017 21:12*

**+Jorge Robles** here is an index:

[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Jorge Robles** *January 30, 2017 21:15*

Thanks **+Don Kleinschnitz** I will take a look :)


---
**Jorge Robles** *January 30, 2017 21:22*

Seems you are using smoothie, any chance where to get info on cncshield?  Seems it uses inverted pwm signal


---
**Ariel Yahni (UniKpty)** *January 30, 2017 21:24*

If I'm not mistaken the Z endstop will fire the laser. I need to do it myself


---
**Don Kleinschnitz Jr.** *January 30, 2017 22:04*

**+Jorge Robles** if you point me to a schematic and code I can see if I can advise you on wiring PWM


---
**Paul de Groot** *January 31, 2017 05:41*

You can invert the pwm signal in grbl. You need to change the code spindle.c where you can initialise the pwm timer register. Also atmel has a datasheet on how to do that on their website 


---
**HalfNormal** *January 31, 2017 12:57*

**+Jorge Robles** Here is a good link about interfacing that board;

 [diy3dtech.com - Search Results for “cnc sheild” – DIY 3D Tech](http://diy3dtech.com/?s=cnc+sheild)

Also when you say "invert the PWM" are you referring to interfacing with the "L" input? If you control the laser with the "in" input (where the pot is) you do not need to invert the PWM signal but will need to invert the laser "on" signal to trigger the power supply "L" input. You can use Peter's inverter or if you are comfortable with programming, this can be done in the GRBL config.h file. Also be aware that the signal pins have changed from GRBL v0.8 so if you are going to use the newer GRBL then you will need to adjust the board's pin out accordingly. 


---
**Jorge Robles** *January 31, 2017 13:05*

Thanks **+HalfNormal**, yes "Inverting PWM" was about the L input. I think will use the inverter, but have to get the model of transistor and resistance values. Anyway, I'm doing this all on my head. I'm sure I will have a bunch of questions when unit arrives.



When you say interfacing with "in" input,  I guess L controls Enable (M3), and IN is the PWM (Sxxx) real signal?


---
**HalfNormal** *January 31, 2017 13:14*

Yes on interfacing. 


---
**Jorge Robles** *January 31, 2017 13:15*

And a so noob question. The pot limits the voltage delivered to the laser. Do I have to connect the PWM signal to the pot and then to the IN?


---
**HalfNormal** *January 31, 2017 13:15*

Best to remove the pot.


---
**Jorge Robles** *January 31, 2017 13:17*

ok, remove, but keeping the mA meter? So you have a reference o maximum mA (tube lifespan). I'm going offtopic, sorry ;)


---
**HalfNormal** *January 31, 2017 13:20*

Yes keep the meter


---
**Jorge Robles** *January 31, 2017 15:51*

hmm I've seen Smoothie boards use 3.3v PWM and arduino Uno GRBL 5v, should it be a problem? Should I have a level shifter?


---
**HalfNormal** *January 31, 2017 15:53*

"In" uses 5 vdc


---
**Jorge Robles** *January 31, 2017 15:55*

I mean if I ever get a Smoothie one, is needed a Level shifter to take in account isn't it? (sorry the confusion)




---
**Paul de Groot** *January 31, 2017 21:01*

**+Jorge Robles** i use a switch so i can choose between the pwm and pot which is useful when cutting. However most cnc software let you tune the pwm output in steps of 10% increase on the fly


---
**Paul de Groot** *January 31, 2017 22:16*

**+HalfNormal** grbl1.1e works really nice in laser mode.

![images/4b6eb65e6760c023f48609041de1f5b5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b6eb65e6760c023f48609041de1f5b5.jpeg)


---
**Jorge Robles** *February 01, 2017 07:15*

Wow, Paul, that's a very good result :O


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/UbhWDedMxfX) &mdash; content and formatting may not be reliable*
