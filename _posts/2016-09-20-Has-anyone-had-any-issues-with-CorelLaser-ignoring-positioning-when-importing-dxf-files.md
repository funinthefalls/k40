---
layout: post
title: "Has anyone had any issues with CorelLaser ignoring positioning when importing dxf files?"
date: September 20, 2016 14:32
category: "Original software and hardware issues"
author: "Gage Toschlog"
---
Has anyone had any issues with CorelLaser ignoring positioning when importing dxf files? 



I am creating a lid for a 3D printed box. So, I exported the top layer sketch to a DXF file in Inventor and imported it into CorelDraw. In CorelDraw, I added some text to the center of the lid that I wanted to engrave, hid the cut lines, and engraved it. I then re-added the cut lines, hid the text, and cut. However, the laser cut in the top left most corner (not where the engraving happened). It seems no matter where I position the cut lines, it cuts at the very top left corner. I can see this behavior in the preview window as well.



I have tried the one pixel reference in the top left corner with no luck.



Thanks in advance!





**"Gage Toschlog"**

---
---
**David Spencer** *September 20, 2016 14:49*

You need to put a register mark in the corner to give it something to reference



[https://plus.google.com/118373815141190144064/posts/4Nebd9xWnLi](https://plus.google.com/118373815141190144064/posts/4Nebd9xWnLi)


---
**greg greene** *September 20, 2016 14:55*

Don't import it - open it


---
**Gage Toschlog** *September 20, 2016 19:07*

Greg, sorry I meant open. David, I have tried the pixel trick with no luck. I may try a surrounding box or using a center reference as someone suggested in your thread.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 20, 2016 21:46*

**+Gage Toschlog** Do you have your settings for "Corel Draw Settings" set to this:

[drive.google.com - Check CorelDraw Settings.png - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)



Some people have found that no matter what they do the engrave/cut layers will be out, then turns out they didn't have everything set to WMF.


---
**Gage Toschlog** *September 21, 2016 14:47*

**+Yuusuf Sallahuddin** They are indeed set WMF. Thanks for the suggestion though. I'll attach a few screen shots of what I'm seeing here shortly.


---
**Gage Toschlog** *September 21, 2016 14:58*

**+Yuusuf Sallahuddin** Screen shots can be seen here. Any help would be greatly appreciated!

[dropbox.com - Laser SS](https://www.dropbox.com/sh/i3l39azd0gtq3le/AAC8N6ftE2dvXKHC8iFyV6JQa?dl=0)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 22, 2016 05:04*

**+Gage Toschlog** I think I know what is going on. There are two things that spring to mind... first is that you are actually using Text as is... I usually convert all text to paths/shapes before lasering (is a good practice if you are designing on different machine to the laser machine, in case the laser machine doesn't have the fonts).



Second, is probably what is actually causing the issue. I will drop a screenshot of what I usually do. Whilst what I do is similar, you need to create some similar reference point (for Top-Left) that both layers are having in common.



Basically what I do is create 2 separate layouts... 1 for the engrave & 1 for the cut.

In my image below you will see that both have an orange rectangle around them. This should be blank fill & no line width (so basically invisible to laser, but it is visible to the software for alignment purposes). Also, I forgot to remove the speech-bubble box in the engrave (top), so pretend it isn't there.



So when I go to engrave, I select the rectangle around it & the engrave objects (the text in this example). This should align your engrave to the exact position it needs to be in.



Next, for cutting, I select the Speech-Bubble box & the rectangle around it, hit Cut. Again, it should be aligned perfectly for where the engrave has been placed.



Alternatively, you can do it all via the 1 layout (like the top where the speech bubble & engrave text are all in 1). You can just hide/show whichever elements you want to engrave/cut & do the required function. Just make sure to always select the outer no-fill/no-line rectangle to ensure alignment is the same for both.

![images/5e2ff8ad426f160bfd1da7653f8f4f15.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5e2ff8ad426f160bfd1da7653f8f4f15.png)


---
**Gage Toschlog** *September 22, 2016 12:34*

**+Yuusuf Sallahuddin** You are the freaking man! That worked perfect. I put a bounding box around the entire laser cutting area on each layer and now I can position items wherever I want. Thanks a ton!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 22, 2016 13:55*

**+Gage Toschlog** Glad that fixed your issue. I have almost 12 months trial & error experience (along with a lot of help from this community) for issues such as this one. The lack of documentation for the stock software has caused many a person here to bang their head on the wall for a few days.


---
*Imported from [Google+](https://plus.google.com/105155515364049131807/posts/GEUduVMtHBy) &mdash; content and formatting may not be reliable*
