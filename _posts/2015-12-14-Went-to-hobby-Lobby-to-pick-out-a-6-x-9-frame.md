---
layout: post
title: "Went to hobby Lobby to pick out a 6\" x 9\" frame"
date: December 14, 2015 23:33
category: "Object produced with laser"
author: "Todd Miller"
---
Went to hobby Lobby to pick out a 6" x 9" frame.

I found a cool Lion image and thought I would etch it

in 3mm plywood and frame it for the wife for xmas.



Then I found a 1/2" - 3/4" thick log cut off of Bass wood.

It was close to the size I wanted and cooler that a frame.



I got home and it wouldn't fit on the Z-table, to big :-(



I took the top frame of the Z-table off and was able

to fit the material !  It just fit !

![images/8a7faded180dfa06231c3c1b40fda3c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a7faded180dfa06231c3c1b40fda3c4.jpeg)



**"Todd Miller"**

---
---
**Scott Thorne** *December 14, 2015 23:56*

Looks awesome!


---
**Tony Schelts** *December 15, 2015 00:11*

Thats really nice


---
**Todd Miller** *December 15, 2015 02:07*

Thanks !



I'd like to seal/shellac/varnish it because

the bark is fragile and comes off with handling.



I just don't know if I should roll, brush or

spray it with something, I just want to coat it.



I don't want a heavy coat like Decoupage,

but I should do something......



But what ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2015 03:46*

That looks great. Not really sure on the varnish but I'd suggest if the bark comes off with handling to spray something on.


---
**ChiRag Chaudhari** *December 15, 2015 18:47*

really Kewl!


---
**Scott Marshall** *December 18, 2015 17:35*

Polyethylene Glycol (PEG) is the wood preservative used by artists for bark attached turnings like lamps and Bedframes.  It replaces the moisture in the wood, preventing the drying, shrinking, and ultimate cracking.

There's a book on it which I own... somwewhere...

Google it.....



I'll post the book specs if I find it.


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/ifdL5NDceVg) &mdash; content and formatting may not be reliable*
