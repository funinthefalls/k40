---
layout: post
title: "I finally got some cast acrylic to try out and I am having some serious issues trying to cut it"
date: May 16, 2017 01:07
category: "Materials and settings"
author: "Anthony Bolgar"
---
I finally got some cast acrylic to try out and I am having some serious issues trying to cut it. I have no problems cutting 1/4" extruded acrylic, but I can not cut the 1/4" cast acrylic even with multiple passes, it actually looks like it is burning the acrylic, , it has blackened areas and a very ragged kerf cut, as if the acrylic is melting back upon itself. Any ideas out there on how to fix this issue? 



Thanks in advance.





**"Anthony Bolgar"**

---
---
**Joe Alexander** *May 16, 2017 02:31*

I have the same issue and a stack of acrylic that i cant cut. just burns and gets black-yellowish and smells like crap.


---
**Alex Krause** *May 16, 2017 02:40*

Sounds like you got polycarbonate ... Try an engrave if it turns yellow,brown,black or any color but frosty white then it's Polycarbonate 


---
**Anthony Bolgar** *May 16, 2017 02:48*

That appears to be what I have then. Thanks for the info Alex, time to open a dispute with paypal then. Do you have a reputable source for cast acrylic **+Alex Krause**


---
**Alex Krause** *May 16, 2017 02:54*

Who did you get you materials from? And did it start anything like makrolon or Lexan on the protective sheet?


---
**Alex Krause** *May 16, 2017 02:57*

**+Anthony Bolgar**​ this is where I got my last sheet of 1/4" from and it cut and engraved nicely... Normally I order 8 packs of 1/8" inch 


---
**Alex Krause** *May 16, 2017 02:57*

Cast Acrylic Sheet 0.25" Thick 12"x12" Clear ( One Sheet ) [https://www.amazon.com/dp/B01LLRK59Q/ref=cm_sw_r_cp_apa_HOMgzbR1Z27G4](https://www.amazon.com/dp/B01LLRK59Q/ref=cm_sw_r_cp_apa_HOMgzbR1Z27G4)


---
**Anthony Bolgar** *May 16, 2017 03:08*

Thanks Alex.


---
**Cesar Tolentino** *May 16, 2017 14:58*

In my experience polycarbonate that I found from home depot is like plastic with embedded gasoline in it. It's just catches fire so easy so I included it in my ban list of materials


---
**Anthony Bolgar** *May 16, 2017 17:50*

It was polycarbonate, they tried to get rid of all references to pc but missed the word in one spot of the clingwrap. I took pictures and started a paypal dispute, it was not the cast acrylic I ordered and paid for. Not a total loss though, I am sure I can cut it with my OX.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/Hr9nV4LggpG) &mdash; content and formatting may not be reliable*
