---
layout: post
title: "I made up something in CorelDRAW. When I pull it up in Corel laser it shows lines in the drawing that are not really there and it wants to cut them, anyone know why it might be doing that?"
date: March 28, 2017 22:50
category: "Discussion"
author: "Robert Selvey"
---
I made up something in CorelDRAW. When I pull it up in Corel laser it shows lines in the drawing that are not really there and it wants to cut them, anyone know why it might be doing that?





**"Robert Selvey"**

---
---
**Ned Hill** *March 28, 2017 23:02*

CorelLaser sometimes has a problem with cutting open curves and tries to close them.  Try switching your cut file output to EMF.


---
**Robert Selvey** *March 28, 2017 23:08*

ok will give that a try in the morning thanks.




---
**Robert Selvey** *March 29, 2017 21:37*

**+Ned Hill** Changing to EMF fix the problems with the lines,  only problem now is on the same file I have an engrave and because they are both not set to WMF it's miss aligned. Is that something that can be fixed in CorelLaser or maybe better to use LaserDraw ?


---
**Ned Hill** *March 29, 2017 21:56*

I use BMP for engrave and EMF for cutting without any problems.  I'm also using coreldraw X8 so I'm not sure if that has an impact.  I have no experience with laserdraw.


---
**Robert Selvey** *March 29, 2017 22:44*

So the parts that you want to engrave, you just export them to JPG?


---
**Ned Hill** *March 29, 2017 23:01*

No, I just changed the corellaser engrave output format to BMP from WMF.  Don't recall having an alignment issue when it was set to WMF but I found certain type of vector files behave funny and BMP is pretty much what you see is what you get.  Again I'm using CD X8, because CD 12 was just too flaky, so it may or may not make a difference for you.


---
**Robert Selvey** *March 29, 2017 23:09*

I'm using x7 I will give it a try tomorrow see how it works out. thanks.




---
**Robert Selvey** *March 29, 2017 23:24*

If you change the output to BMP on the stock board the engrave and the cut will be miss aligned correct?


---
**Ned Hill** *March 29, 2017 23:30*

With stock board I'm using BMP for engrave and EMF for cut without any alignment issues.


---
**Mark Brown** *March 30, 2017 02:55*

Is this alignment issue where you need to put a reference pixel in the top-left corner of both the cut and engrave layers?  Or is this different?


---
**Robert Selvey** *March 30, 2017 03:07*

Different.




---
**Robert Selvey** *March 31, 2017 21:01*

**+Ned Hill** Did the engrave today sent to BMP and it aligned fine with the cut set to EMF but it still wants to cut twice with the cut lines set to 0.001?


---
**Ned Hill** *March 31, 2017 22:02*

**+Robert Selvey** Make sure that's in mm and not inches. (0.01mm or less)


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/WiZwtHpmjwN) &mdash; content and formatting may not be reliable*
