---
layout: post
title: "This was posted a few days ago: TWO-PIECE BOXES THANKS TO LASER-CUT FLEX HINGES"
date: April 07, 2017 14:43
category: "Object produced with laser"
author: "Nate Caine"
---
This was posted a few days ago:



TWO-PIECE BOXES THANKS TO LASER-CUT FLEX HINGES



[https://github.com/vanillabox/OpenSCADBoxes](https://github.com/vanillabox/OpenSCADBoxes)



[http://hackaday.com/2017/04/06/two-piece-boxes-thanks-to-laser-cut-flex-hinges/](http://hackaday.com/2017/04/06/two-piece-boxes-thanks-to-laser-cut-flex-hinges/)



![images/8ad5608f492805b0712aa5c645daaa97.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ad5608f492805b0712aa5c645daaa97.jpeg)



**"Nate Caine"**

---
---
**Alex Hayden** *April 07, 2017 17:00*

How do you draw these for cutting? I am interested in using this on my 3d printer housing. Its rather large though, so I think I will have to find a bigger laser cutter machine.


---
**Nate Caine** *April 08, 2017 15:28*

Google around for "laser flex hinges" and similar terms.  Serveral sites have canned projects.  But others have online programs that will lay out the hinges for you, give the box size.  



Sadly, a few good links I bookmarked are now out of date.  One guy (British) has mathematically analyzed the various hinge designs and rated them for durability, flexibility, versatility, etc.  Some flex in on dimension, others in two.



[http://www.deferredprocrastination.co.uk/blog/tag/#Lattice](http://www.deferredprocrastination.co.uk/blog/tag/#Lattice) Hinges



Several YouTube videos under the category "living hinge" (but a YouTube bug is showing them black so I can't find the one I wanted to recommend).



There is a Dutch site that had a wide range of clever designs.   



Here's an interesting article on Instructibles:

[http://www.instructables.com/id/Curved-laser-bent-wood](http://www.instructables.com/id/Curved-laser-bent-wood)/



[instructables.com - Curved Laser Bent Wood](http://www.instructables.com/id/Curved-laser-bent-wood)


---
*Imported from [Google+](https://plus.google.com/107379565262839168986/posts/KDdUyBLWxCq) &mdash; content and formatting may not be reliable*
