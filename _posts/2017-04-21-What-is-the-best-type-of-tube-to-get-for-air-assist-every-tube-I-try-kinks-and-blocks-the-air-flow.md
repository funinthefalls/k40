---
layout: post
title: "What is the best type of tube to get for air assist, every tube I try kinks and blocks the air flow?"
date: April 21, 2017 09:02
category: "Air Assist"
author: "Phillip Meyer"
---
What is the best type of tube to get for air assist, every tube I try kinks and blocks the air flow?





**"Phillip Meyer"**

---
---
**E Caswell** *April 21, 2017 10:03*

**+Phillip Meyer** this is the stuff I have used. 

Also installed a track and fed the pipe through it so it doesn't kink. This is silicone air pipe and more flexible. 

[amazon.co.uk - Elite Airline Tubing, 6m/ 20 ft, Blue:Amazon.co.uk:Pet Supplies](https://www.amazon.co.uk/gp/aw/d/B0002AQI9K/ref=mp_s_a_1_fkmr1_1?ie=UTF8&qid=1492768881&sr=8-1-fkmr1&keywords=elite+silicone+air+hose)


---
**Joe Alexander** *April 21, 2017 10:33*

standard 1/4" aquarium tubing works well, doesn't kink when bent by my cable carrier.use the edge of your hand to test the bend, any pet store or Home Depot should carry it.


---
**Mark Brown** *April 21, 2017 13:39*

The silicone kind is more flexible than the other (whatever it is).


---
**Adrian Godwin** *April 21, 2017 14:00*

Here's a chart of minimum bend radii for various silicone tube thicknesses.



[http://www.advantapure.com/apst-silicone-tubing.htm](http://www.advantapure.com/apst-silicone-tubing.htm)



And here's one for nylon tubing (commonly used for pneumatics)



[http://www.smcpneumatics.com/nylon.html](http://www.smcpneumatics.com/nylon.html)


---
**Ned Hill** *April 21, 2017 14:10*

I also recommend the silicone aquarium tubing.  


---
**Phillip Meyer** *April 21, 2017 18:14*

Thanks, I'm in the UK so the [Amazon.co.uk](http://Amazon.co.uk) option should work well


---
**E Caswell** *April 21, 2017 18:22*

**+Phillip Meyer** if you have a "pets at home" close by they sell the same tubes Philip.


---
**Phillip Meyer** *April 21, 2017 18:42*

**+PopsE Cas** Thanks, we have one just around the corner.


---
**Ashley M. Kirchner [Norym]** *April 22, 2017 01:34*

And if possible, use an actual cable carrier/cable chain for it. It will prevent those kinks.


---
**Phillip Meyer** *April 22, 2017 11:23*

I am using a cable chain but the pipe is kinking where it comes out and loops around to join the laser head, very frustrating.


---
**Mark Brown** *April 22, 2017 12:10*

Try to leave some more slack there, or if possible turn the head so the fitting is coming off at a different angle.


---
**Ashley M. Kirchner [Norym]** *April 22, 2017 17:18*

What he said. This is how mine looks:

![images/28b2ed8db5135330998f8efb923f6830.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/28b2ed8db5135330998f8efb923f6830.jpeg)


---
**Joe Alexander** *April 22, 2017 18:09*

**+Ashley M. Kirchner** I love how your lcd is still on the bubble wrap and production in the bed :P nice bed btw!


---
**E Caswell** *April 22, 2017 18:38*

**+Phillip Meyer** This is mine, chain going towards the back rather than the top. Just wanted to be different than everyone else. 😁

![images/348a945476bf5811c20e50d8ca0a7a26.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/348a945476bf5811c20e50d8ca0a7a26.jpeg)


---
**Steve Clark** *April 22, 2017 19:26*

I think this is the same as Pops & Phillip's tubing and it's the tube I used.  Just a hint with Silicone tubing it has a sticky feeling and can collect FOD when handling it... a little mild soap in water on a papertowel and cleans right up.



[https://www.mcmaster.com/#catalog/123/128/=17b7tte](https://www.mcmaster.com/#catalog/123/128/=17b7tte)


---
**Ashley M. Kirchner [Norym]** *April 22, 2017 19:29*

+Joe Alexander, lol, yeah it's been sitting there because I keep procrastinating on redoing the panel on the machine. 


---
**Ashley M. Kirchner [Norym]** *April 23, 2017 05:03*

**+PopsE Cas**, actually, almost everyone does it like yours. I'm one of the few that has it on top. I didn't like how it flops around when I had it the way you do.


---
**Phillip Meyer** *April 23, 2017 08:14*

Strange my existing pipe kinks with such a small curve, will buy another pipe this week


---
*Imported from [Google+](https://plus.google.com/117194752728291709572/posts/aLoiTNCrz4S) &mdash; content and formatting may not be reliable*
