---
layout: post
title: "Oyyy. Hey guys, bought the K40 used with no software"
date: August 27, 2016 19:04
category: "Software"
author: "Scott Zeiger"
---
Oyyy. Hey guys, bought the K40 used with no software. I'm up and running with Moshidraw 2012 but need the USB driver for the MS10105 V4.1 controller. Can someone point me in the right direction please?





**"Scott Zeiger"**

---
---
**Vince Lee** *August 27, 2016 21:23*

What color dongle did u get, red or orange?


---
**HalfNormal** *August 28, 2016 00:28*

 **+Scott Zeiger** Here ya go!

[http://www.moshidraw.com/English/html/738520352.html](http://www.moshidraw.com/English/html/738520352.html)


---
*Imported from [Google+](https://plus.google.com/113096251219627788105/posts/DQ1n2ZHLrL1) &mdash; content and formatting may not be reliable*
