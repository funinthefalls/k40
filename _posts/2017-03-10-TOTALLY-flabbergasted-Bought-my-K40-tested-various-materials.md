---
layout: post
title: "TOTALLY flabbergasted. Bought my K40, tested various materials"
date: March 10, 2017 21:47
category: "Object produced with laser"
author: "Steven Whitecoff"
---
TOTALLY flabbergasted. Bought my K40, tested various materials. Conclusion- excellent tool seemingly perfect for the job. Yesterday and today I spent upteen hours figuring out how to make a simple wheel with 8 cutouts to duplicate laser cut ones ones I purchased but at half size. After much struggling with with Qcad trial(worthless in demo mode), Nano cad(produced object several KM in size- huh? who designs the worlds largest bridges with nanocad?), Freecad(could not scale and the forum geeks described a process akin to voodoo), etc I finally downloaded DraftSight and suddenly life was good. Put in Laserweb and of course, the K40 either incinerated the cut(and did not cut thru) or fails to cut even after literally 10 passes. This uber material??????? 1/64" birch plywood. this is with air assist too. There has got to be a trick....





**"Steven Whitecoff"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2017 23:14*

1/64"? That's like paper thickness. What speed & power settings are you using? Should be able to cut that very easily (@4mA @100+ mm/s I think).



Also, focal point matters a fair amount (make sure workpiece is at 50.8mm from the base of the lens, for the stock lens at least).



Check lens orientation (bump side up).


---
**Steven Whitecoff** *March 11, 2017 00:51*

EDIT: hold the presses,,,,,,, IRC the bump was down when I checked the lense prior to first fireup as they appeared at first glance to have greasy finger prints but they were just showing smut that fooled me. Anyway I'll check now...

and its wrong side up AND work surface is 59.45mm from bottom of lens. 



 I'm new so there are a lot of things I dont get, like why repeated low power passes dont seem to cut more. 

Also I do not know how beam size relates in LW3, what the minimum actually is and why would you not want that all the time for cutting. Engraving I get has different requirements. 


---
**Mark Brown** *March 11, 2017 02:04*

Cut width setting is so that it can compensate by cutting outside the line.  If you were to draw a 10mm circle, with a cut width of 1mm and the cut centered on the line the piece you'd get would only be 9mm.



Of course that's an exaggeration, we're working with cut widths of  0.3-5 mm if I remember correctly.


---
**Mark Brown** *March 11, 2017 02:06*

And as for the lens, you should only be able to see your reflection on one side (the curved side) that's the side that goes up.  Handy to know because I once had the lens flip over as I dropped it into the holder.


---
**Steven Whitecoff** *March 11, 2017 02:52*

Well **+Twelve Foot**, that some good info on the width, I was wondering how in the +<b>*</b> it could affect the beam in software given the way lasers work. So by setting that on the high side I should get more meat left in the cut. Perfect, as kerf is a mofo to deal with in the drawings...already had a discussion on that in the RC Groups forums. 

Next week, after I fix the dishwasher, I'm going to spend some quality time looking thru the docs, blogs, forums etc so I don't have so many hole in my knowledge. Regardless, the cheapness of the K40 is starting to show, the mirrors/lens are truly crap but theres no denying that it works pretty well despite that.



Well its sure looking like the plywood IS an uber material. The laser breezed right thru 3mm lite ply at 20%. At 75% its charring its way thru the 1/64th. I tried wetting it and that helps with the charring but so far its looking like that plywood is just too tough somehow. I notice that I can clean the lens and cut, then its got something that wipes off on the top surface. 


---
**Anthony Bolgar** *March 11, 2017 03:11*

The glues and resins in some plywoods are extremely difficult to get through. Masonite is one material that is brutal, 1/8" takes me about 20 passes, where as I can cut 1/4" MDF in one pass neatly and cleanly.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 11, 2017 04:29*

I've actually used 2 different varieties of 3mm plywood that had totally different results. A cheap 3mm plywood cut through reasonably easy (e.g. 10mA @ 10mm/s) in one pass. A more expensive plywood (with what seems to be a hardwood core) of the same thickness took like 20 passes to get through & charred the hell out of the material around the cut. I guess in addition to what Anthony mentions, regarding the glues & resins, that the composition of the actual plies matters significantly too.


---
**Cesar Tolentino** *March 11, 2017 15:58*

Just to add here. My home depot have the 5mm ply for cheap but the issue is it has so many knots on it. Laser will not just cut through it. Then I tried the IronPly which is designed for underlayment. No knots and cuts beautifully. But it has x mark on one side. Too bad. But I still use it anyway thinking I will just paint it over.



![images/0cd365e08f32ce8b7b2e2b5d68ced5d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0cd365e08f32ce8b7b2e2b5d68ced5d4.jpeg)


---
**1981therealfury** *March 13, 2017 08:01*

I have some 0.8mm Plywood sheet that i bought for cheap thinking it would be perfect for leaser cutting business cards, I'm sure they specifically used laser proof adhesive to stick the layers together as i have NEVER successfully cut through it on my K40, wither any combination of high power or multiple low power passes.  I can cut through 6mm ply in 1 pass but not this stuff.  The adhesive they use to stick the layers together is the single biggest differentiator to laser cutting effectiveness i have found.



Also it seems that the thinner the ply, the worse the adhesive they use is to cut through so i tend to try stick to 3mm minimum now unless a job really needs thinner.


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/CtkpCqdNdvF) &mdash; content and formatting may not be reliable*
