---
layout: post
title: "I bought a K40 off of Ebay a few months ago and got it in the mail finally last week"
date: July 16, 2017 03:43
category: "Software"
author: "Christopher Elmhorst"
---
I bought a K40 off of Ebay a few months ago and got it in the mail finally last week. When I bought it, it said it came with the software, and of course when it got here it didn't, and the description on Ebay changed to say that it didn't come with it. SO, now I have a laser cutter, and no software. I've seen upgraded boards and software that allow you to import Jpg's and bam, Engrave. That is what I need (I design everything in photoshop). Is there any software out there that Is known to work with a stock K40 laser to do this sort of thing? It's a stock K40 so I don't plan on cutting anything really (Since it's not really "strong enough" to do so) and just engrave on items. 



THANK YOU all in advance, ANY advice will be greatly appreciated!

-CE





**"Christopher Elmhorst"**

---
---
**Jim Hatch** *July 16, 2017 13:49*

Check out Scorch Works and his new K40Whisperer project. Stock Nano M2 board support and DXF or SVG support. 


---
**Christopher Elmhorst** *July 16, 2017 14:57*

A DXF AND SVG file will have to come from another program correct? 


---
**Jim Hatch** *July 16, 2017 15:19*

Yep. But Inkscape is Opensource so it's free to use. Pretty much like Illustrator or CorelDraw in functionality. You'd be able to just drop the jpg on the Inkscape drawing page, save as SVG and you can engrave it. 


---
**Christopher Elmhorst** *July 16, 2017 15:29*

Oh cool! Thank you! I've been watching YouTube videos (my best friend it seems lately) and was looking for a single program to do it all. But I guess it's not a real thing haha. I just downloaded the CorelDraw 2017 Suite package for free (don't ask me where, because I have no idea how I got here). And I'll try what you were talking about, thanks! 


---
**HalfNormal** *July 16, 2017 15:43*

If you search this forum, you will find plenty of links to the original software and to the manufacturer's website for their software too.


---
**Scorch Works** *July 16, 2017 16:51*

**+Christopher Elmhorst** Did you get a USB key with your K40? (looks like a thumb drive but it isn't) 



You will need to have the USB key to us the original software (i.e. LaserDRW assuming you have the M2 Nano board)



I recommend you make sure you can run on the standard software before you move on to something else.  If you have a Moshi controller with no key you may need $ to move forward.  You only chance to push the vendor for what you need is now (they will not care about you in six months).

![images/b2a99bbcd98a4645f820e2cdab5b4bf6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2a99bbcd98a4645f820e2cdab5b4bf6.jpeg)


---
**Christopher Elmhorst** *July 16, 2017 16:52*

So I plugged in the laser and the Dongel thing, and it automatically installed to my computer. I go to my control area on the computer and I see it's connected, but it doesn't pop up as a printer. Nor when I go to print from CorelDraw it doesn't pop up as an option, I can't find anywhere on how to set it up as a printer


---
**HalfNormal** *July 16, 2017 17:02*

**+Christopher Elmhorst** that is your problem. The Laser does not show up as a printer. Corellaser is the gateway to the laser functions. When you install all the software including Corel, you have to run Corellaser to get the laser functions. It will start Corel. It will initially show up as a bar at the top of Corel and then become an icon in the system tray. Right click the icon in the tray to get to the laser functions.


---
**Christopher Elmhorst** *July 16, 2017 18:00*

**+HalfNormal** okay I'll spend all day playing with it to get familiar haha


---
**Christopher Elmhorst** *July 16, 2017 21:48*

alright, So I downloaded Inkscape, and I downloaded K40 Whisperer, I saved an SVG file, and inported it into K40 Whisperer. I can hit Initialize Laser, and it homes itself, and I can hit Home, and it also homes itself. I can't find anywhere to Print, and if I click anywhere on the white space on the right the printer goes crazy and makes god awful noises (I'm guessing it's making the belt slip....)




---
**HalfNormal** *July 16, 2017 22:16*

There are three buttons at the bottom left of the program  window.  Pick the one you want to do


---
**Scorch Works** *July 16, 2017 22:44*

If you hit the right or down arrow does it also make the terrible noise?



Please look at your controller board (the one the USB cable plugs into).  See if it says M2 Nano on it anywhere.


---
**Scorch Works** *July 16, 2017 22:51*

I will reiterate, you should get LaserDRW working before you try anything else.  You are currently on a path no one has gone down before so it is more difficult to give solid advice.  That being said there are things to learn from the path you are on but it may take some time to get you up and running.



To get LaserDRW working at this point you will need to uninstall the libUSB driver install with Zadig.

Removal instructions: [scorchworks.com - K40 Whisperer USB Driver Setup](http://www.scorchworks.com/K40whisperer/k40w_setup.html#windowsRemoveUSB)






---
**Christopher Elmhorst** *July 16, 2017 23:11*

Yes it says M2Nano


---
**Christopher Elmhorst** *July 16, 2017 23:33*

![images/c51c41ea4d6a0fc3fd08a1be3d4a5167.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c51c41ea4d6a0fc3fd08a1be3d4a5167.jpeg)


---
**Christopher Elmhorst** *July 16, 2017 23:34*

I did this at 20% power, and 250 mm/s. 


---
**Scorch Works** *July 16, 2017 23:42*

With LaserDRW?


---
**Christopher Elmhorst** *July 16, 2017 23:44*

No I did it in InkScape, saved it as a SVG, and then imported it into Whisper and cut it at 250 mm/s and the laser at 20% power


---
**Christopher Elmhorst** *July 16, 2017 23:46*

Engraved* haha


---
**Scorch Works** *July 16, 2017 23:47*

What happened to "goes crazy and makes god awful noises"?


---
**Christopher Elmhorst** *July 16, 2017 23:48*

I don't touch the buttons that make it "go crazy and make god awful noises" haha. I can make a video and send it to you


---
**Scorch Works** *July 17, 2017 00:03*

So if I understand correctly 

1. the arrow buttons make the bad noise?

2. Moving the image makes the bad noise.



Sounds like rapid moves are the issue.  



Did it make the bad noise at the end of the engraving? (When it rapid moved back to the start position)


---
**Christopher Elmhorst** *July 17, 2017 00:05*

Nope, as long as I don't click in the blank space or touch the arrows it doesn't do it. If I just import the image and click engrave it works perfectly, and re-homes. If I click the blank areas or the arrows it seams like it wants to go back left, 


---
**Scorch Works** *July 17, 2017 01:20*

I have an idea but I need a few hours to try some stuff out on my machine.  I will let you know when I have something for you to try.


---
**Scorch Works** *July 17, 2017 04:04*

Looks like it is not going to happen tonight. I will update you tomorrow.


---
**Christopher Elmhorst** *July 17, 2017 19:09*

**+Scorch Works** do you want me to record a video and post it of what it does? 


---
**Scorch Works** *July 17, 2017 19:44*

**+Christopher Elmhorst** It might be usefull if you have the time.  I don't think it would change what I am working on for you to try. 


---
**Christopher Elmhorst** *July 17, 2017 19:48*

Okay sounds good!


---
**Scorch Works** *July 18, 2017 03:08*

**+Christopher Elmhorst**​​​

 

Here is a link to a K40 Whisperer zip file that has three test items in the USB drop down menu.



Steps

1. Initialize the laser

2. select the first test option (Test 1)

3. check for the bad noise by clicking and arrow button



4. select the third test option (Test 3)

5. check for noise by clicking and arrow



6. select the second test option (Test 2)

7. check for noise by clicking and arrow



Report back...

<b>EDIT: link deleted</b>


---
**Christopher Elmhorst** *July 20, 2017 04:14*

Hey sorry for getting back to you so late. Long days at work. So I downloaded and opened the zip file, but I don't know what I should do after that. I see one item is labeled as an application, but it will not run.




---
**Christopher Elmhorst** *July 20, 2017 04:19*

Alright never mind I figured it out. None of them made a bad noise. Now I'll try and upload an svg file and see if I can move the head around.




---
**Christopher Elmhorst** *July 20, 2017 04:21*

Yes the same thing is happening. Once I load a file, it wants to go back negative coordinates.


---
**Scorch Works** *July 20, 2017 11:00*

OK, I didn't know this only happens after you open an SVG file.  Can you send me  a SVG file that you are using.  My e-mail address is in the K40 Whisperer menu under "Help"-"about".


---
**Scorch Works** *July 20, 2017 16:30*

**+Christopher Elmhorst**​ I am guessing your SVG page size is bigger than the laser area.  You need to adjust the SVG size in Inkscape under 'file'-'document properties'.  It is easiest to use 'resize page to drawing or selection' to adjust the page size to fit the design.



I will also fix K40 Whisperer so it does not try to move up when the SVG is too big. (I think that is what you are seeing.)


---
**Christopher Elmhorst** *July 21, 2017 03:21*

**+Scorch Works** you're a genius. I know you hear that a lot but thank you seriously. 


---
**Scorch Works** *July 21, 2017 03:49*

Thanks.

Although, I was the one that programmed it to do that (not intentionally).


---
*Imported from [Google+](https://plus.google.com/+ChristopherElmhorst/posts/KJ7JBQjUVLf) &mdash; content and formatting may not be reliable*
