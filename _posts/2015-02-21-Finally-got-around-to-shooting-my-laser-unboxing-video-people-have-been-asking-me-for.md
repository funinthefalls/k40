---
layout: post
title: "Finally got around to shooting my laser unboxing video people have been asking me for"
date: February 21, 2015 23:30
category: "Discussion"
author: "Jim Coogan"
---
Finally got around to shooting my laser unboxing video people have been asking me for.  Except it was unboxed days ago.  Hard to leave a new tool alone when it is just sitting there :-) 





**"Jim Coogan"**

---
---
**Sean Cherven** *February 22, 2015 03:08*

Do you have a link?


---
**Jim Coogan** *February 22, 2015 03:49*

They are still being edited and will be on my YouTube channel in the next day or two at [https://www.youtube.com/channel/UCGZUbRUw8_Sv4f1YxV-AEyg](https://www.youtube.com/channel/UCGZUbRUw8_Sv4f1YxV-AEyg)


---
**Sean Cherven** *February 25, 2015 02:29*

Did you get around to posting the video yet?


---
**Jim Coogan** *February 28, 2015 22:23*

Sorry.  I did the video but never got around to posting it.  Guess I will be busy this weekend.  Have 3 videos to post.  One is the laser unboxing.


---
*Imported from [Google+](https://plus.google.com/+JimCoogan_CoogansWorkshop/posts/2PSXdJnaM4n) &mdash; content and formatting may not be reliable*
