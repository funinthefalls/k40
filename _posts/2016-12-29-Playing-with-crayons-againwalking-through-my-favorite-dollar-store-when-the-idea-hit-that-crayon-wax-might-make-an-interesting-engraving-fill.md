---
layout: post
title: "Playing with crayons again....walking through my favorite dollar store when the idea hit that crayon wax might make an interesting engraving fill"
date: December 29, 2016 00:58
category: "Materials and settings"
author: "Mike Meyer"
---
Playing with crayons again....walking through my favorite dollar store when the idea hit that crayon wax might make an interesting engraving fill. I've been playing around with it this afternoon and I think this has potential...For those who are interested, I top coated the crayon wax with super glue to toughen up the finish. I'll post again if and when I refine the process.

![images/bf64fbbf721fd4e653221bc5e42f4613.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf64fbbf721fd4e653221bc5e42f4613.jpeg)



**"Mike Meyer"**

---
---
**Jim Hatch** *December 29, 2016 02:39*

How do you get the melt into the engrave?


---
**Mike Meyer** *December 29, 2016 03:06*

I used one of those cheap little Harbor Freight butane torches and melted the crayons over the engraved letters.


---
**Jonathan Davis (Leo Lion)** *December 29, 2016 03:20*

neat effect.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 29, 2016 10:16*

Interesting idea. Gives a nice end result. Using similar technique you could also use candle wax & mix up your own colours/marbled effects.


---
**Natalie Ball** *December 29, 2016 19:02*

Great idea! I'm interested to see how you are able to perfect it. 




---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/78FMLF7bo7b) &mdash; content and formatting may not be reliable*
