---
layout: post
title: "Morse code trainer with laser marked mount in blue EM30 GRP"
date: September 09, 2016 17:15
category: "Object produced with laser"
author: "David Richards (djrm)"
---
Morse code trainer with laser marked mount in blue EM30 GRP.

I've never tried marking this GRP before, the contrast is a little low but quite acceptable for the purpose. The settings used were something like 200mm/s at 10mA. The material is 10mm thick so the focus was way out as I didn't adjust for that.

![images/62c8ea053eb139938478043a413500e3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/62c8ea053eb139938478043a413500e3.jpeg)



**"David Richards (djrm)"**

---
---
**greg greene** *September 09, 2016 19:44*

straight key - or bug?

73 VE7GPG


---
**David Richards (djrm)** *September 09, 2016 20:20*

**+greg greene** The key is of WWII vintage and known as a Bath Tub, it is not a bug. See here for more information. [vanzwamcs.com - Bathtub Morse key Ref. N°. 10A/7741](http://www.vanzwamcs.com/greenpages/keys/Bathtub_10A-7741/Bathtub_10A-7741.htm)



btw the micro and display are used to drive the sounder and decode the morse and display it on the screen. There is also a serial output. The design is taken from here:  [https://create.arduino.cc/projecthub/rayburne/magic-morse-on-arduino-f48633](https://create.arduino.cc/projecthub/rayburne/magic-morse-on-arduino-f48633)




---
**greg greene** *September 09, 2016 20:28*

Thanks!  Interesting key, I like that is a mass fits - or looks like it fits, the adult hand well - so less fatigue.  I happen to have a spare arduino so I'll givr that circuit a try.  I tried a couple of chinese made decoders - terrible pieces of junk.


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/YQnoXLhfw5r) &mdash; content and formatting may not be reliable*
