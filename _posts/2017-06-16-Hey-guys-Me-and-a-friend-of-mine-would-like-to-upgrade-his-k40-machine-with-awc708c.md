---
layout: post
title: "Hey guys Me and a friend of mine would like to upgrade his k40 machine with awc708c!"
date: June 16, 2017 04:04
category: "Hardware and Laser settings"
author: "Cristian Huanquilef"
---
Hey guys

Me and a friend of mine would like to upgrade his k40 machine with awc708c! . Has anyone done it? I've seen some tutorials  but i couldnt find one with the correct laser power supply. This machine has a different power supply so im not sure if i can use the same power supply or try with a another one. 

I appreciate any answers





**"Cristian Huanquilef"**

---
---
**Phillip Conroy** *June 16, 2017 05:02*

You will need to add a separate 24volt 3amp power supply $22 on ebay


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 05:13*

Any particular reason why you want to upgrade to that specific controller? 


---
**Paul de Groot** *June 16, 2017 10:15*

It's usd399 upgrade on a usd250 machine so probably a bit of over investment. I would recommend a proper laser cutter. 


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 14:01*

... or a different controller such as the Cohesion3D Mini. 


---
**Don Kleinschnitz Jr.** *June 16, 2017 14:32*

Post a picture of the laser supply please.



Start editorial:

You might think through the tool chain using that controller as you will end up with a closed set of software and proprietary firmware. 

I started going that way but ended up with smoothie due to its open posture and Gcode protocol. 

As an example I could not tell you what is in that controller so if it pukes your on your own with the vendor. With a smoothie we have full source code and schematics.

To be fair LO has good support.

End Editorial:


---
**A “alexxxhp” P** *June 16, 2017 15:01*

Did that upgrade on my old k40 works perfectly never had any issues. now I use it as a second machine mostly with acrylics.




---
**Cristian Huanquilef** *June 16, 2017 15:56*

**+A P**   



Awesome! 

Did your machine have  this conecttion:  k+ k- g in 5v d+ d-  24 GND 5v?????

I've seen some machines and they dont have this configuration so i dont know if this gonna work with the awc708c.

Thanks 


---
**Cristian Huanquilef** *June 16, 2017 18:00*

**+Phillip Conroy** I know. Just wondering if the laser power supply will work with the awc708c


---
**Cristian Huanquilef** *June 16, 2017 18:04*

**+Don Kleinschnitz** I'll do it when i get home. 

Thanks 


---
**A “alexxxhp” P** *June 16, 2017 18:20*

**+Cristian Huanquilef** post a photo of your power supply


---
**Cristian Huanquilef** *June 19, 2017 04:01*

**+Don Kleinschnitz**  **+A P** . 

![images/4b57b0a22a082dfbdcb64f2595e32ab0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b57b0a22a082dfbdcb64f2595e32ab0.jpeg)


---
**Cristian Huanquilef** *June 19, 2017 04:03*

![images/4d0f4c4b52c7b53bd207800aaa2a06a0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4d0f4c4b52c7b53bd207800aaa2a06a0.jpeg)


---
**Paul de Groot** *June 19, 2017 04:22*

**+Cristian Huanquilef** never seen this model before. How old is it?


---
**Don Kleinschnitz Jr.** *June 19, 2017 04:29*

**+Paul de Groot** I have seen two of these. It works the same as the white connector type except the D+ and D- which had some thing weird about it. I would have to it up in my my notes....

**+Cristian Huanquilef** it would do you well to draw a schematic of how this is connected before you do a conversion. Also take pictures of all the connectors with their labels showing. Especially the control and DC power connectors. 


---
**Cristian Huanquilef** *June 19, 2017 13:52*

**+Paul de Groot** Not sure but it is pretty much the same as every chinese machine

![images/0ef799bd3a32149aa15836e20b2e2982.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0ef799bd3a32149aa15836e20b2e2982.jpeg)


---
**Don Kleinschnitz Jr.** *June 19, 2017 13:54*

**+Cristian Huanquilef** certainly similar but not the same panel as the majority of machines.


---
**Cristian Huanquilef** *June 19, 2017 13:55*

**+Don Kleinschnitz**  OK. I' ll  do it as soon as i can. So what you reckon abou it ? Is it possible to do ? 


---
**Cristian Huanquilef** *June 19, 2017 13:56*

Im pretty sure i can do it but my doubt is if i can use the same laser power supply with the awc708c


---
**Don Kleinschnitz Jr.** *June 19, 2017 14:25*

**+Cristian Huanquilef** when you say possible to do do you mean:

Upgrade this to awc708c?



#K40awc708c



I am only roughly familiar with the awc708C. 

The LPS in you machine appears to have equivalent functions to a standard supply. Standard supplies are used in upgrades to AWC708C so I do not see why yours would not work. 



This forum is always here to help but this is not the typical upgrade we see..



Looks like +A "alexxxhp" P could provide some help and verify the below.



From some quick research it looks like you will have to also:

...Replace the optical end-stops with switches

... Add stepper drivers

...Add a 24V supply



Did you look at the C3D controller? **+Ray Kholodovsky** it is drop in and does not require drivers, end-stop or PS changes. I think it is also less expensive. 

Is it because you want to use "Laserworks" software? 



*Search awc708C+K40 and you can review some wiring diagrams that can provide hints



Also search the Light Object support forums as they sell similar DSP's and have good documentation.



[img.banggood.com](https://img.banggood.com/thumb/water/oaupload/banggood/images/90/71/13d96753-070a-4f65-b77c-cd55b05da130.jpg)



[https://ae01.alicdn.com/kf/HTB1HEZQRpXXXXc9aXXXq6xXFXXXZ/Trocen-Anywells-AWC708C-LITE-Co2-Laser-Controller-System-for-Laser-Engraving-and-Cutting-Machine-Replace-AWC608C.jpg](https://ae01.alicdn.com/kf/HTB1HEZQRpXXXXc9aXXXq6xXFXXXZ/Trocen-Anywells-AWC708C-LITE-Co2-Laser-Controller-System-for-Laser-Engraving-and-Cutting-Machine-Replace-AWC608C.jpg)


---
*Imported from [Google+](https://plus.google.com/112954580144252608880/posts/eUcHg5tLA67) &mdash; content and formatting may not be reliable*
