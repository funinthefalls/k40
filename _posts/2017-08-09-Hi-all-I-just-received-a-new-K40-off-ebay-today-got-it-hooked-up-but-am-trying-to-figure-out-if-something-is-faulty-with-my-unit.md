---
layout: post
title: "Hi all, I just received a new K40 off ebay today, got it hooked up, but am trying to figure out if something is faulty with my unit"
date: August 09, 2017 03:04
category: "Discussion"
author: "Brian Li"
---
Hi all,



I just received a new K40 off ebay today, got it hooked up, but am trying to figure out if something is faulty with my unit.  Apologies if this is a common question, I couldn't find the answer after searching for about an hour.



I can't seem to get the alignment on the cutting bed correct and never finds the home position.  Every time I turn the machine on, it goes to a different spot.  Also, when dragging things around in CorelDraw, the laser sometimes goes "out of bounds" and makes a terrible grinding/jackhammer noise.  I've uploaded a video here of what happens when I turn it on (sorry it's upside down).  Any help would be appreciated!  If it's faulty, I'd like to exchange it ASAP with the ebay seller.



Thanks, this forum is a lifesaver!

![images/74b548596f829ec1098fc1eb74137493.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74b548596f829ec1098fc1eb74137493.jpeg)



**"Brian Li"**

---
---
**Ashley M. Kirchner [Norym]** *August 09, 2017 03:52*

Check the X-axis end stop, behind the stepper on the left of the gantry. Verify that it's connected properly, all the way to the board. Might also be dirty.


---
**Brian Li** *August 09, 2017 04:09*

Thanks! Just checked and it appeared to be connected properly- I couldn't follow the wire all the way to the board without taking things apart however. 


---
**Ashley M. Kirchner [Norym]** *August 09, 2017 04:13*

Hopefully you noticed that there are two near each other there. I'd check both.


---
**Don Kleinschnitz Jr.** *August 09, 2017 10:56*

I'd say something is faulty. See if you can get them to help. 

Seems something is wrong with the stepper direction. It should home to upper left. 

The carridge should go up then to the left in the video it goes right?


---
**Martin Dillon** *August 09, 2017 15:23*

I am wondering if all the settings are correct in Corellaser?  Does this happen if you choose the wrong board in the setup?


---
**Brian Li** *August 09, 2017 15:26*

Thanks guys - I am going to return it and order another on Amazon Prime so that I can return it more easily if needed again!  I did input the correct control board information into CorelLaser before starting it up.  I think something is faulty with the unit.


---
**Ashley M. Kirchner [Norym]** *August 09, 2017 17:22*

We all know it should home to the left, however my experience has been that it will move slowly to the right when the end-stop isn't working. That happens when my optical end-stop gets dirty. It sees it as "triggered" and does a slow crawl to the right to "un-trigger" it.


---
**Brian Li** *August 09, 2017 18:01*

Hi Ashley - i'm brand new to lasers in general - do you have instructions on how I might clean the end stop optical?  Or pictures of what I should be looking for?  Thanks!


---
**Ashley M. Kirchner [Norym]** *August 09, 2017 18:19*

I don't know if you have optical or mechanical ones. Mine are optical. Under the X gantry, on the left side, there should be a small green board with wires on it. If you have an optical switch, look on that board for two black protrusions, about 4mm apart. That's the switch. Try blowing compressed air between them. Also verify the wires, make sure they're connected, that none of them broke during shipping.

![images/0229caa242ca63c28177e6472da36fb7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0229caa242ca63c28177e6472da36fb7.jpeg)


---
**Ashley M. Kirchner [Norym]** *August 09, 2017 18:20*

And that's a shot in the dark honestly. But you can try.


---
**Brian Li** *August 09, 2017 19:00*

Thanks Ashley!  I'll take a look at that when I get home - maybe a piece of packing styrafoam is in the way or something.  Does it look like there is only an issue with the x-axis sensor? is there such a thing as a y-axis sensor?


---
**Ashley M. Kirchner [Norym]** *August 09, 2017 19:03*

There is a Y sensor as well, in my picture you can see some of the wires for that stick out on the right side of the metal. It looks like your Y movement is homing correctly, it's just the X that isn't. But it's worth checking both.


---
**Brian Li** *August 09, 2017 22:19*

I got home and checked the x-axis optical sensor - seems to look like yours - everything i can tell looks normal!  Any connections in particular you'd suggest checking?

![images/b91558213105c5601189256c05083dcc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b91558213105c5601189256c05083dcc.jpeg)


---
**Ashley M. Kirchner [Norym]** *August 09, 2017 22:23*

Right, so blow some compressed air in between those 2 black protrusions next to the white connector. Then that white ribbon cable, you want to make sure it's seated properly, on both ends. The other wires you see are likely for both the y-sensor and stepper motor, check them too while you're at it. Then trace the wires/ribbon that comes out of that area and goes back to the board. Mine has a single ribbon cable, some people have individual wires. Make sure nothing's broken or nicked anywhere, push on the connectors or re-seat the ribbon cable.



If all of that still fails, then there's a good chance that sensor is dead.


---
**Don Kleinschnitz Jr.** *August 10, 2017 14:49*

Also check that the sensor interposer is positioned correctly. I have see machines that slammed into the tab and damaged the sensor. 



Here is some info:



[http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html?m=1](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html?m=1)


---
**Brian Li** *August 10, 2017 14:51*

Thanks Don - what is the sensor imposer?  Is that the "tab" under the laser head the slots in between the black sensor?


---
**Brian Li** *August 10, 2017 14:53*

I tried blowing out the sensor with compressed air as well as checking all connections.  Still had the same problem.  I am trying to return the unit on ebay but the seller is trying to sneak out of of it....I would rather fix it, but doesn't seem to be an obvious issue.


---
**Don Kleinschnitz Jr.** *August 10, 2017 15:03*

Yes it's a little Al tab. Check to see if it's not aligned or bent, or maybe damaged it and/or sensor.

See videos in the link above. There are also schematics in the link. If you can use a dvm we can make some measurements.

I have also seen cases where there are poor solder joints and intermittent connections on the daughter card.

If nothing is obvious pull out the card and inspect it and post close-ups of both sides. 


---
**Ashley M. Kirchner [Norym]** *August 10, 2017 16:39*

And as a last resort, you might be able to replace it for a better, mechanical one. Someone here can help you connect those to a stock board. 


---
**Brian Li** *August 10, 2017 18:44*

Even though the x-axis sensor is off, the cutter head does seem to cut correctly as long as I "manually" home it to the top-left.  I cut a few patterns after doing this.  Does this mean that I could technically run projects as long as I physically move the cutter head back to top left before each cut?


---
**Don Kleinschnitz Jr.** *August 10, 2017 22:24*

**+Brian Li** I think we are talking about the y sensor ...no?

Where do you live?


---
**Brian Li** *August 11, 2017 03:42*

It is the one that controls left/right movement (I think that is x-axis correct?). I am in Minneapolis. 


---
**Don Kleinschnitz Jr.** *August 11, 2017 08:30*

**+Brian Li** you are right it is the x sensor that seems to be misbehaving. If nothing else works I think you have to pull that daughter card out. 


---
**Brian Li** *August 11, 2017 16:12*

If i were to attempt repairing this sensor or replacing with a mechanical, are there instructions on this process easily accessible? Thank you!




---
**Brian Li** *August 12, 2017 13:07*

The seller is offering a $100 refund if I keep the machine - I don't kind fixing it, but anyone think that's worth it and know what parts I would need?


---
**crispin soFat!** *August 12, 2017 14:02*

endstops can cost under $5 each so I'd suggest that you take the $100 and spend it on upgrades (booze).


---
**Ashley M. Kirchner [Norym]** *August 12, 2017 16:34*

I concur. End stops are cheap. 


---
**Brian Li** *August 12, 2017 17:10*

Thanks guys - do you have any recommendations on where to purchase?


---
**Don Kleinschnitz Jr.** *August 12, 2017 17:18*

I can repair them or show you how and have parts list on my blog. 



Somewhere I have seen a conversion to mechanical but have to find it. Will do when I get home tomorrow.




---
**crispin soFat!** *August 12, 2017 17:37*

**+Brian Li** you should probably just send that whole hundo over to **+Don Kleinschnitz**. :)


---
**Don Kleinschnitz Jr.** *August 12, 2017 18:30*

**+Brian Li** where do you live?


---
**Brian Li** *August 12, 2017 21:20*

**+Don Kleinschnitz** I'm in Minneapolis! 


---
**Don Kleinschnitz Jr.** *August 12, 2017 21:30*

**+Brian Li** your call, we can try to  troubleshoot remote

 or 

remove x and y daughter card and white flat cables and send here. 



Your cost is only;

Shipping both ways

Patience as I get it in que and get parts if needed :)


---
**Brian Li** *August 13, 2017 02:09*

**+Don Kleinschnitz** hi don, is there a way to start a private conversation? Sorry, I'm new to google communities. Thanks!


---
**Don Kleinschnitz Jr.** *August 13, 2017 14:12*

**+Brian Li** sure, I sent you a PM which is just a G+ post only to you.


---
*Imported from [Google+](https://plus.google.com/113792581999914867877/posts/K8ZSsrum9fR) &mdash; content and formatting may not be reliable*
