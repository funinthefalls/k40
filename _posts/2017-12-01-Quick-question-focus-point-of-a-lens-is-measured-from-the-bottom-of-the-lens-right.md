---
layout: post
title: "Quick question: focus point of a lens is measured from the bottom of the lens right?"
date: December 01, 2017 06:00
category: "Hardware and Laser settings"
author: "Chris Hurley"
---
Quick question: focus point of a lens is measured from the bottom of the lens right? Eg 50.8 from the lens is the focus point. 





**"Chris Hurley"**

---
---
**Phillip Conroy** *December 01, 2017 06:19*

Yes ,however to findbest cuttingdistance do a ramp test cut,ie have the scrap material a bit below and a bit above this distance to find the ideal distance


---
**Anthony Bolgar** *December 01, 2017 09:36*

Once you have the optimal focus distance determined it is a good idea to make a focus block that goes from the bottom of the laser head to the surface of the material. on my 60W laser this is 25mm. Makes it very easy to set the focus, just raise the material up until it traps the focus block between the bottom of the laser head and the top of the material.


---
**Chris Hurley** *December 01, 2017 13:40*

If I'm cutting 3mm mdf should I aim to have the focus point in the middle, top or near the bottom of the material? 


---
**Anthony Bolgar** *December 01, 2017 13:42*

I use the top of the material for 3mm or thinner, middle of the material if 4mm or thicker. Your mileage may vary. Try different settings on a few test pieces to see what works best for you.


---
**Bill Keeter** *December 05, 2017 20:26*

**+Chris Hurley** you want the focus in the middle of the material. So for 3mm material you want the focus at 1.5mm above the bed. So it hits the middle of the material. I have 3d printed focus blocks for each of the materials thicknesses I cut. So I can quickly adjust the focus.


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/drk1mySyqFi) &mdash; content and formatting may not be reliable*
