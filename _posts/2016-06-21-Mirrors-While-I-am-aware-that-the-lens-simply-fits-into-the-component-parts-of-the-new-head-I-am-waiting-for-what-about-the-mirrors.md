---
layout: post
title: "Mirrors. While I am aware that the lens simply fits into the component parts of the new head, I am waiting for, what about the mirrors?"
date: June 21, 2016 11:13
category: "Modification"
author: "Pete Sobye"
---
Mirrors. 



While I am aware that the lens simply fits into the component parts of the new head, I am waiting for, what about the mirrors?



How do you change the mirrors on a k40? 



Also, once I have fitted my new Saite head and MO mirrors, what settings should I be expecting to use for cutting 3mm acrylic? I.e. How many Milliamps and at what speed?





**"Pete Sobye"**

---
---
**Phillip Conroy** *June 21, 2016 11:28*

I haves used 7.5 mm/sec and 10ma power in the past.do a small test cut first


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 21, 2016 11:35*

Where did you get your MO mirrors & what price? I've been delaying getting a set but think it might be worthwhile doing so soon (since the stock are starting to look pitted).


---
**Phillip Conroy** *June 21, 2016 11:53*

I have ordered a cheap ebay dimond coated 22mm hole saw to make my own mirrirs out of old hard drive platters which i have a few, i belive the fumes from cutting mainly mdf are causing my gold coated mirror at the head to deterate after only 100 hours of cutting,the hole saw was only 5 bucks delivered from china and i have 4 or 5 old 3.5 inch hard  drives and 20 laptop 2.5 inch hard drives lieing around.will compare wattage with diy and store brought with my lsser power metre and post


---
**Imnama** *June 21, 2016 12:00*

Have a look at this if you want to make your own mirrors. Copper is better then hard drive plaaters


{% include youtubePlayer.html id="ELeqP3tHlp0" %}
[https://www.youtube.com/watch?v=ELeqP3tHlp0](https://www.youtube.com/watch?v=ELeqP3tHlp0)


---
**Imnama** *June 21, 2016 12:02*

Here is one using hdd platters


{% include youtubePlayer.html id="HHagynL4zqs" %}
[https://www.youtube.com/watch?v=HHagynL4zqs](https://www.youtube.com/watch?v=HHagynL4zqs)


---
**Pete Sobye** *June 21, 2016 12:40*

These are the mirrors I bought:-

[https://www.ebay.co.uk/itm/172050707677](https://www.ebay.co.uk/itm/172050707677) 


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/GkHcb5JpzcD) &mdash; content and formatting may not be reliable*
