---
layout: post
title: "Has anyone had any luck getting the K40 to work with Windows 10?"
date: November 14, 2015 08:56
category: "Software"
author: "Anthony Coafield"
---
Has anyone had any luck getting the K40 to work with Windows 10? 





**"Anthony Coafield"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 14, 2015 09:13*

Yep, I run it on Macbook Pro, running Windows 10 through Bootcamp with no dramas. I use the CorelDraw/LaserDRW to engrave/cut. (edit: only issue I have is when I open LaserDRW it decides to popup error messages about Creative Cloud is restarting, twice; but that doesn't really affect anything).


---
**Anthony Coafield** *November 14, 2015 09:58*

Every time I plug in my cutter the usb dings to say it's plugged in, then it disconnects itself and says that the engraver is not connected. Might need to search for drivers!


---
**Scott Thorne** *November 14, 2015 12:17*

I've not had any problems running Windows 10


---
**Sean Cherven** *November 14, 2015 13:45*

Try a different USB Port.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 14, 2015 15:04*

**+Anthony Coafield** Are you plugging in the little USB dongle they give as well? As far as I'm aware you need to have that plugged in too or it won't work.


---
**Anthony Coafield** *November 14, 2015 21:22*

Thanks **+Scott Thorne**, I'll keep on trying then. Good to know it should work. I couldn't find anything online about it.



**+Sean Cherven** Will do. I didn't think of that. It was plugged into a hub so I'd tried different ports on that, but will find my usb extension and try going straight into the computer. Thanks for the idea.



**+Yuusuf Sallahuddin** I did have the dongle plugged in, although I'll try with that in a different slot also. Thank you.


---
**Anthony Coafield** *November 15, 2015 02:38*

**+Sean Cherven** You solved it! Plugged into the back of my computer rather than the hub and working great off Windows 10. Thank you very much.


---
**Sean Cherven** *November 15, 2015 03:05*

I'm glad I could help! You'd be surprised at the difference between USB Ports.. On my pc, even the front USB Ports don't handle high speed stuff.


---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/FAjpRbRVnbP) &mdash; content and formatting may not be reliable*
