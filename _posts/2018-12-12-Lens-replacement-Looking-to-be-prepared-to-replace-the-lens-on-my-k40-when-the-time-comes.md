---
layout: post
title: "Lens replacement: Looking to be prepared to replace the lens on my k40 when the time comes"
date: December 12, 2018 09:36
category: "Hardware and Laser settings"
author: "'Akai' Coit"
---
Lens replacement:



Looking to be prepared to replace the lens on my k40 when the time comes. Wondering if there are better lenses than the stock 18mm lens that I could purchase along with either a replacement head that drops in or face where the lens "sits" that could give me a sharper focus point, better lifespan, closer focus length or any/all of the above improvements. Suggestions or ideas?



Note: I should be fine at the moment... I'm thinking ahead a little with this post.





**"'Akai' Coit"**

---
---
**Joe Alexander** *December 12, 2018 12:06*

the focus lenses determine your focal length not the laser head just FYI, commonly being 1", 1.5" and 2". note the smaller the depth the finer the dot but the more hourglass the cut becomes, thicker materials are better with a longer focal length.


---
**'Akai' Coit** *December 12, 2018 15:51*

**+Joe Alexander** thank you for that info.


---
**Ned Hill** *December 16, 2018 01:42*

Cohesion3D sells good optic upgrades. 

 [cohesion3d.com - Optics - Cohesion3D: Powerful Motion Control](http://cohesion3d.com/optics/)


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/H8SQT9onxvp) &mdash; content and formatting may not be reliable*
