---
layout: post
title: "Got bored by the fuzzy original current adjustment pot, so I replaced it with a precision multi turn pot, that I inherited from my grandfather ;) Similar part here:"
date: May 24, 2017 17:01
category: "Modification"
author: "Claudio Prezzi"
---
Got bored by the fuzzy original current adjustment pot, so I replaced it with a precision multi turn pot, that I inherited from my grandfather ;)



Similar part here: [http://www.ebay.com/itm/112378358906](http://www.ebay.com/itm/112378358906) 



![images/527d62a102a4705ccf86aca6658aadbd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/527d62a102a4705ccf86aca6658aadbd.jpeg)
![images/933f3d1b027c2da03223d5dcf0c5c74a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/933f3d1b027c2da03223d5dcf0c5c74a.jpeg)

**"Claudio Prezzi"**

---
---
**Alex Krause** *May 24, 2017 18:07*

Nice pot you have there :)


---
**Anthony Bolgar** *May 24, 2017 19:50*

I put a 10 turn in one of my K40's and will be changing out the other two I have as well. So much finer control.


---
*Imported from [Google+](https://plus.google.com/+ClaudioPrezzi/posts/7c1PVhvU7ou) &mdash; content and formatting may not be reliable*
