---
layout: post
title: "Repo for laser cutting and CNC dxf files ( but not only)"
date: January 18, 2017 08:50
category: "Repository and designs"
author: "Stephane Buisson"
---
Repo for laser cutting and CNC

 dxf files ( but not only)



[http://www.dxfplans.com/index.php](http://www.dxfplans.com/index.php)



![images/30d5a531ab285ee9391b06685930c9d6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/30d5a531ab285ee9391b06685930c9d6.jpeg)
![images/7f742cda898ec3942713af2a50a20482.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7f742cda898ec3942713af2a50a20482.jpeg)
![images/c73edfbc093a5b5ae6ad26fd52bc7df8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c73edfbc093a5b5ae6ad26fd52bc7df8.jpeg)

**"Stephane Buisson"**

---


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/LcawqKEVhgY) &mdash; content and formatting may not be reliable*
