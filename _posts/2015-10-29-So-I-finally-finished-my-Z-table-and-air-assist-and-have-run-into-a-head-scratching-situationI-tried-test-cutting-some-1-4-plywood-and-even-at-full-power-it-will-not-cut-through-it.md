---
layout: post
title: "So I finally finished my Z table and air assist, and have run into a head scratching situation.....I tried test cutting some 1/4\" plywood and even at full power it will not cut through it"
date: October 29, 2015 00:28
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
So I finally finished my Z table and air assist, and have run into a head scratching situation.....I tried test cutting some 1/4" plywood and even at full power it will not cut through it. However, before I had the air assist it would cut through at only 7-8 mA. Any ideas of what has gone wrong?





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *October 29, 2015 05:24*

Well I figured out the problem....it was a dirty mirror. Now I know to clean my mirrors more often.


---
**Ashley M. Kirchner [Norym]** *October 30, 2015 20:05*

I ran into that myself not too long ago when I recalibrated mine. Burning up pieces of paper in front of the mirrors got them dirty and caused me to scratch my head for a while trying to figure out what I had done to mess up the machine. Quick cleaning and I was back in business. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/Y71p7qxPXox) &mdash; content and formatting may not be reliable*
