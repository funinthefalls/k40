---
layout: post
title: "if I want to replace the mirrors on my K40 what kind would you suggest?"
date: February 29, 2016 01:48
category: "Modification"
author: "3D Laser"
---
if I want to replace the mirrors on my K40 what kind would you suggest?





**"3D Laser"**

---
---
**Phillip Conroy** *February 29, 2016 05:40*

I brought them in a set ,note normal focal lens is 12mm -this one is 19 mm -i had a worn mirror on the movable laser head and a worn focal lens,even with air assist and water trap on air line still have to clean bottom of focal [http://www.ebay.com.au/itm/281745543380?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/281745543380?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT) every 3 hours of cutting 3mm mdf


---
**HP Persson** *March 02, 2016 08:21*

I bought from Ray´s optic store on Aliexpress, ordered feb 24th (3 mirrors, 2 focus lenses) and arrived today. I paid $45 + shipping. 

For mine it is 20mm mirrors and 12mm lens.



Store URL: [http://www.aliexpress.com/store/218002](http://www.aliexpress.com/store/218002)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/KhpWZxnxZHT) &mdash; content and formatting may not be reliable*
