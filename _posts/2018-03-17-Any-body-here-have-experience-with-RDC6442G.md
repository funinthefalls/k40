---
layout: post
title: "Any body here have experience with RDC6442G?"
date: March 17, 2018 07:35
category: "Discussion"
author: "Alisha kansha"
---
Any body here have experience with RDC6442G? I set the machine home position in the bottom left. Everything is works fine except the preview image on the RDCS6442G screen is mirrored. This happend when Y mirror is disable in vendor setting of RD Works.



If i enable Y mirror in vendor setting, the preview in RDC6442 screen is correct but the laser draw the object as mirror.



Any one know what is wrong?  





**"Alisha kansha"**

---
---
**LightBurn Software** *March 17, 2018 20:05*

Disable the mirror Y in the software, then press the Z/U button on the controller to enter the menu. There’s a screen origin setting on the controller itself that you need to change.


---
**Manuel Tejero** *April 01, 2018 20:20*

Hello ALISHA KANSHA I have updated my k40 with the controller rdc6442g but I have not been able to use it, for configuration reasons or at least I think you can publish your configuration I would really appreciate it thank you



Hola ALISHA KANSHA he actualizado mi k40 con la controladora rdc6442g pero aun no he podido utilizarla, por motivos de configuracion o al meno eso creo me puedes publicar tu configuracion te lo agradeceria de verdad gracias


---
**Alisha kansha** *April 02, 2018 01:43*

**+Manuel Tejero** what did you mean ypu have not been able to use it. What thing is not working? For me i just follow the user guide of rdc6442 for the setting and wiring. 


---
**Alisha kansha** *April 02, 2018 06:41*

**+LightBurn Software**  Anyway thanks for your help. Now everything is works perfectly


---
*Imported from [Google+](https://plus.google.com/112428355097604312279/posts/1eGdsNhu4xr) &mdash; content and formatting may not be reliable*
