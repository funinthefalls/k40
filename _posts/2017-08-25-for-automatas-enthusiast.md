---
layout: post
title: "for automata's enthusiast"
date: August 25, 2017 15:29
category: "Object produced with laser"
author: "Stephane Buisson"
---
for automata's enthusiast 





**"Stephane Buisson"**

---
---
**Stephane Buisson** *August 25, 2017 16:02*


{% include youtubePlayer.html id="hY423UJVJmg" %}
[youtube.com - Penn Mechanisms for Design 2014](https://www.youtube.com/watch?v=hY423UJVJmg)


---
**Stephane Buisson** *August 25, 2017 16:02*


{% include youtubePlayer.html id="DukvsPRvagM" %}
[https://www.youtube.com/watch?v=DukvsPRvagM](https://www.youtube.com/watch?v=DukvsPRvagM)


---
**HalfNormal** *August 26, 2017 16:38*

**+Stephane Buisson** Darn you! I can waste days watching the videos on moving mechanisms! ;-) Now if only I can put a computer and stove in the shower!


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/5jPMWzC6kVW) &mdash; content and formatting may not be reliable*
