---
layout: post
title: "The begining of my z axis table project"
date: April 10, 2016 18:38
category: "Modification"
author: "Manuel Maria Organv\u00eddez Rodr\u00edguez"
---
The begining of my z axis table project

![images/eded95e55c4deef543f477a20a50f28a.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/eded95e55c4deef543f477a20a50f28a.gif)



**"Manuel Maria Organv\u00eddez Rodr\u00edguez"**

---
---
**Jim Hatch** *April 10, 2016 19:13*

You might want to look at a scissors jack for inspiration. That way you only need enough room under the table for the scissors mechanism and there's nothing above the table. Using the simple screw method you're using leaves you with the tall screws extending over the bed.



The screw action works well if you can move the screws to the back or both ends and mount the screws into the machine case bottom & top. Then the table will ride up & down like 3D printer beds do.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 19:18*

**+Jim Hatch** As you only really want to travel up a certain distance (i.e. no closer than focal length of the lens) you could do the whole setup in reverse, so the z-axis table actually goes down instead of up.


---
**Norman Kirby** *April 10, 2016 19:29*

where did you get the gt2 closed loop drive belt????? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 19:32*

**+Norman Kirby** If you look closely, I don't think it is a closed loop, cos it slips oddly as it goes over where it has been joined.


---
**Norman Kirby** *April 10, 2016 19:57*

well spotted Yuusuf




---
**Jim Hatch** *April 10, 2016 20:08*

**+Yuusuf Sallahuddin**​ you're correct. At the 0:02 mark you can see the overlapped joined part in the lower left as it gets ready to go over the roller.


---
**Ariel Yahni (UniKpty)** *April 11, 2016 02:28*

What would be the best way to join that belt? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 02:42*

**+Ariel Yahni** I'm not familiar with this myself, however a quick google suggests it is not wise to do so unless absolutely necessary, but you are introducing a planned failure point.



See this link for discussion on this same topic:

[http://bbs.homeshopmachinist.net/archive/index.php/t-60510.html](http://bbs.homeshopmachinist.net/archive/index.php/t-60510.html)


---
**Jarrid Kerns** *April 11, 2016 02:53*

I've often thought about trying to figure that out, but never got around to it. Figure you could glue the ends then a piece glued to the back over the seam if the belt was going to be very tight. I have seen a tool that melts round hose together and was wondering if you could do something like that with belts too.


---
**HP Persson** *April 11, 2016 07:49*

I tested some with glue for bike tyres, when you fix holes in the tubes on your bike. and put the patch on the back of the belt.

And after that sand it slightly to still allow it to bend around the pulleys.

I have a 3D printer with a broken belt, i used that method until my new belt arrived. But it still works so the new belt is unused yet :)


---
**Anthony Bolgar** *April 14, 2016 02:54*

If you set the ratio of the stepper pulley to the corner pulleys high enough, you can get the fill travel without a join going over a pulley.


---
*Imported from [Google+](https://plus.google.com/104340601585724034705/posts/a1FrwQLhCRd) &mdash; content and formatting may not be reliable*
