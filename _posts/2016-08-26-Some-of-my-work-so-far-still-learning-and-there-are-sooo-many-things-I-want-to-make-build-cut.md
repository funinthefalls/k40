---
layout: post
title: "Some of my work so far, still learning and there are sooo many things I want to make/build/cut :)"
date: August 26, 2016 23:19
category: "Object produced with laser"
author: "Rodney Huckstadt"
---
Some of my work so far, still learning and there are sooo many things I want to make/build/cut :)



![images/d23afc628577096747256af1b7e527c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d23afc628577096747256af1b7e527c5.jpeg)
![images/9e6d270c116765c41156c243e73e1596.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e6d270c116765c41156c243e73e1596.jpeg)
![images/f5b473edb8119ff3fbc9e2620d2826a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f5b473edb8119ff3fbc9e2620d2826a7.jpeg)
![images/41ae588fc3aa07083fb16977240356a5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41ae588fc3aa07083fb16977240356a5.jpeg)
![images/9c06cb5bb674a99f4f05d2e387ebe1d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9c06cb5bb674a99f4f05d2e387ebe1d7.jpeg)

**"Rodney Huckstadt"**

---
---
**Rodney Huckstadt** *August 26, 2016 23:20*

the cue ball was done on a spherebot and the stand was laser cut :)


---
**Chris Sader** *August 27, 2016 03:24*

**+Rodney Huckstadt** could the spherebot be connected to the laser as a rotary axis?


---
**Rodney Huckstadt** *August 27, 2016 09:14*

possibly :) you only need to attach the stepper to the axis i guess


---
**Robi Akerley-McKee** *August 27, 2016 21:38*

Looks like you could get a 4th+5th axis table and run it as X and Y for sphere's  Just have to fiddle with steps per mm for the radius of the sphere. or play with DPI.



$400 there abouts...



[http://www.ebay.com/itm/CNC-Engraving-Machine-Rotary-Table-H-Style-A-Axis-B-Axis-4th-5th-Rotational-Axis-/262408923307](http://www.ebay.com/itm/CNC-Engraving-Machine-Rotary-Table-H-Style-A-Axis-B-Axis-4th-5th-Rotational-Axis-/262408923307)


---
**Rodney Huckstadt** *August 28, 2016 23:37*

I am building a rotary axis at the moment,  found on thingiverse, all laser cut from 3mm ply, needs a lot of tweaking yet to be viable


---
*Imported from [Google+](https://plus.google.com/109243783228349869845/posts/KjG5zYPsxHQ) &mdash; content and formatting may not be reliable*
