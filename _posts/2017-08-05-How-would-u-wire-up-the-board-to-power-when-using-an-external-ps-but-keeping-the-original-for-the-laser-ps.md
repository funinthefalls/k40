---
layout: post
title: "How would u wire up the board to power when using an external ps but keeping the original for the laser ps?"
date: August 05, 2017 23:55
category: "Smoothieboard Modification"
author: "William Kearns"
---
How would u wire up the board to power when using an external ps but keeping the original for the laser ps? 





**"William Kearns"**

---
---
**Ariel Yahni (UniKpty)** *August 06, 2017 01:00*

Stepper and endstops wired into external PSU. Can you identify those?


---
**William Kearns** *August 06, 2017 01:02*

I'm using C3D mini board 


---
**Ariel Yahni (UniKpty)** *August 06, 2017 01:10*

So you have already everything connected to it, the just change the board input voltage to the external PSU


---
**William Kearns** *August 06, 2017 01:17*

**+Ariel Yahni** what about the power for the laser? It's still powered by the stock psu? 


---
**Ariel Yahni (UniKpty)** *August 06, 2017 02:10*

**+William Kearns**​ laser power itself never goes through the board ( any board ) that goes directly to the tube


---
**Ashley M. Kirchner [Norym]** *August 06, 2017 03:32*

**+William Kearns**, please see the C3D Documentation link. I posted a lengthy step-by-step instruction how to wire two external drivers and steppers. The wiring will be the same as what you are trying to achieve here.


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/VuR4bwCbS5H) &mdash; content and formatting may not be reliable*
