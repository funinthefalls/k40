---
layout: post
title: "how to remove the exhauct flue on my K40"
date: May 29, 2017 17:14
category: "Discussion"
author: "bbowley2"
---
how to remove the exhauct flue on my K40





**"bbowley2"**

---
---
**Ariel Yahni (UniKpty)** *May 29, 2017 17:58*

The complexity will depend on your machine. Some with just removing the screws in the back is enough. Other need to also remove the stock bed and gantry


---
**bbowley2** *May 29, 2017 18:22*

I've removed almost everything.  The gantry seems to be connected to the X

axes.


---
**Ariel Yahni (UniKpty)** *May 29, 2017 18:38*

Look at this pictures, maybe will help you 

[plus.google.com - This is no upgrade but since I removed the gantry I feel it will be very usef...](https://plus.google.com/+ArielYahni/posts/RgraoksVvbo)


---
**Martin Dillon** *May 29, 2017 21:46*

has anyone cut out the back and removed it that way?  I was going to try that but with the exhaust system I have now the everything seems to we working fine with very little smell escaping. 

I use a blower like this one [grizzly.com - Great for a small shop/garage!](http://www.grizzly.com/products/1-HP-Light-Duty-Dust-Collector-Polar-Bear-Series/G1163P)  it is very quiet.


---
**Joe Alexander** *May 29, 2017 22:53*

the older machines had the exhaust flues tab-welded on, the newer ones are easier with 4 bolts. either way, dremel with a cutoff wheel is your friend here.


---
**Steve Clark** *May 31, 2017 04:22*

I had to remove the xy table to get mine off. Plan on spending a couple of hours. 



Your grizzly blower will work fine. I have a system not quite that big. But be aware that they will suck the blue flat if not supported inside and the white drier tubes for sure. Ask my how I know...



I'm using one of these to slow mine down.



[jet.com - Jet.com - Prices Drop As You Shop](https://jet.com/product/detail/535569b804314aeea8d70a80cc3de299?jcmp=pla:ggl:NJ_dur_Gen_Patio_Garden_a3:Patio_Garden_Gardening_Tools_Gardening_Gloves_a3:na:PLA_784744539_40568370666_pla-498264183512:na:na:na:2&code=PLA15&pid=kenshoo_int&c=784744539&is_retargeting=true&clickid=8771df0b-2717-4560-ae16-89e7b34f83f2)



 I used an aluminum drier tube to prevent collapsing but you may want to ground it to earth because I found that static/mystery voltage or what have you was building up on it  (air flow causing static?? but 6 volts only?) I'm not sure what or where it was coming from because both end connections were plastic.



Anyway it wasn't there when the blower was off... showing up only when it was on. Grounding it to earth took care of it showing less than 1 volt. Go figure...  


---
**Martin Dillon** *May 31, 2017 05:05*

I use standard dust collector hose.  In fact I blow it out through my dust collection system vent because my collector is outside. 


---
**Steve Clark** *June 07, 2017 00:07*

**+Martin Dillon**  Is the standard hose  polyethylene?

 Somebody had a story on the web about hooking up his laser exhaust to one then having a little fire in his dust collection system... possibly poor cleaning maintenance? That would not be fun!


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/5BsSFmwYCBX) &mdash; content and formatting may not be reliable*
