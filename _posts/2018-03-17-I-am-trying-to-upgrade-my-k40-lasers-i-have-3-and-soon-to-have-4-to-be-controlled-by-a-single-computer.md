---
layout: post
title: "I am trying to upgrade my k40 lasers (i have 3 and soon to have 4) to be controlled by a single computer"
date: March 17, 2018 15:49
category: "Smoothieboard Modification"
author: "Brass Honcho"
---
I am trying to upgrade my k40 lasers (i have 3 and soon to have 4) to be controlled by a single computer.   I purchased a cohesion 3d mini smoothie controller and light-burn.  The problem im having is, is that the laser will now only raster engrave a max of 100 mm/s.  This is five times slower than the stock board allowed.  The cohesion 3d support group told me that the smoothie will only raster engrave a max of 100mm/s.  Is this accurate?  if so is there a control board that i can get that will allow faster raster engrave?  thanks for the help





**"Brass Honcho"**

---
---
**Wolfmanjm** *March 17, 2018 16:12*

If you use the [fast-stream.py](http://fast-stream.py) it will do about 1000pixels/sec or more. the issue is the way the host sends the data, not the board itself. (FWIW 100mm/s is meaningless I presume you meant 100pixels/sec) as raster is sent as a lot of 0.1mm lines.


---
**HalfNormal** *March 17, 2018 16:17*

[awesome.tech - Buy Gerbil](http://awesome.tech/buy-gerbil/)


---
**Ned Hill** *March 17, 2018 17:06*

**+Wolfmanjm** he actually does mean mm/s.  With the stock controller you can set the raster scan rate up to 500 mm/s.   The resolution is also typically set to 1000 dpi.   So, if I have this right, at 1000 dpi you have ~ 39.4 pixel/mm which at 100 mm/s scan rate would translate to ~  3940 pixel/s.  I'm getting ready to upgrade to a C3D mini so any insights you have are greatly appreciated :).


---
**Jim Fong** *March 17, 2018 17:17*

Post your Lightburn raster engraving file and I’ll run a test on my k40/c3dmini/smoothie. You should have no problem doing raster engravings atleast 200mm/sec but it really depends on the image resolution.  If the x axis is skipping steps, it very well could be the stepper driver needs to be adjusted for more current.  The motors are rated about 1amp current.  








---
**Ray Kholodovsky (Cohesion3D)** *March 17, 2018 19:31*

In that thread I linked you the firmware file for the grbl-lpc firmware that will run on the Cohesion3D board. Can you please try this, using the instructions I provided there? 


---
**LightBurn Software** *March 17, 2018 19:52*

On Windows, even fast-stream.py caps at about 700 to 800 gcodes per second, and LightBurn’s sender actually beats it slightly. 



Typical DPI for laser engraving is 250 to 300. Your beam width is the limiting factor here - you can output 1000 dots per inch, but they overlap so much there’s little point in it.



At 254 DPI, 100mm/sec is 1000 gcodes / sec exactly. Smoothieware will not keep up with that consistently which is why we added the Newsprint dither mode - it gives good shading while reducing the GCode command count.



GRBL-LPC, on identical hardware, using the same sender, processes GCode at roughly triple the rate or better, so I have a hard time believing it’s the communication channel that’s the issue.



And Wolfman is right on one count - mm/sec is meaningless unless you also include the DPI value. It’s the number of gcodes processed per second that’s important. Smoothie will run 1000mm/sec no problem if all you’re doing is a solid line.


---
**Ned Hill** *March 17, 2018 20:25*

**+LightBurn Software** If you are only doing photo engraving then I would agree.  But if you want deeper engraving The overlapping allow you to get deeper.  You could up the power to compensate but that's not always practical.  You are then left with having to do multiple passes. 


---
**LightBurn Software** *March 17, 2018 20:28*

I’d be surprised if there was any functional difference between that and going slower. With lower DPI you’re not overriding all the internal calcs trying to adjust the output to compensate for beam width.


---
**Don Kleinschnitz Jr.** *March 18, 2018 03:52*

**+Ned Hill** the published specs are 0--350mm/sec???



New info on motors:

[photos.google.com - Google Photos](https://photos.google.com/share/AF1QipOjDFGt-AYFpQdJCOXeoUBoS6vy_9EuYa41j_m_CB8JHUzWwOCEvGgTRRbUMjgBmQ/photo/AF1QipO5KtK0yjBMYFZmNjx0h4ofhyCesobZ_1PlmITG?key=dU9UTnFpRkUyTTRnTUJ1SmtXWUVPTmVmVmZLNEhR) 


---
**Ned Hill** *March 18, 2018 04:06*

**+Don Kleinschnitz** all the documentation I've seen for the stock setup has always said up to 500 mm/s.  Wouldn't be surprised though if the max has been overstated. 






---
**LightBurn Software** *March 18, 2018 04:22*

The protocol info that I've seen suggests the max is indeed closer to 350.


---
**Jim Fong** *March 18, 2018 04:38*

**+Ned Hill** 300mm/sec was the fastest reliable speed on my M2nano setup as I recall. 



I’ve done 500mm/sec with grbl-lpc on the c3d.  That’s about 738rpm.  Not much motor torque available at that speed.  



I set the firmware max speed at 400mm/sec to play it safe. 


---
**Ned Hill** *March 18, 2018 04:43*

**+Jim Fong**  I routinely run at 350 - 400 mm/s, even up to 550 mm/s for large engravings, without any issues on an M2nano.  Lol probably been lucky not to burn out a motor, but they have been going strong for almost 2 years now.


---
**Jim Fong** *March 18, 2018 04:59*

**+Ned Hill** that’s pretty good.  I had to take mine apart to square up the gantry.  Some are just better constructed.



I run my nema23 motors on my gantry at 1500rpm.  Most steppers can handle high speed pretty well. 




---
*Imported from [Google+](https://plus.google.com/117535072813354948352/posts/f4oxye9CDvk) &mdash; content and formatting may not be reliable*
