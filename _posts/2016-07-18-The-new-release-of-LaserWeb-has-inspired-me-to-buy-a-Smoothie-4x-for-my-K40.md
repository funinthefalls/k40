---
layout: post
title: "The new release of LaserWeb has inspired me to buy a Smoothie 4x for my K40"
date: July 18, 2016 18:54
category: "Smoothieboard Modification"
author: "Corey Renner"
---
The new release of LaserWeb has inspired me to buy a Smoothie 4x for my K40.  Where do I order the level-shifter that everyone talks about?  Anything else I need to get while I wait for that board to arrive?





**"Corey Renner"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 18, 2016 19:07*

You can get the level shifter from SparkFun. [https://www.sparkfun.com/products/11771](https://www.sparkfun.com/products/11771)



You may also need a Middleman board (if your K40 has the ribbon connector for the X-stepper). Unless you want to rewire all of that.



Alternatively, if you're not as electronically inclined as some here (like myself), **+Scott Marshall** is soon to be shipping his ACR kits (plug & play for the smoothie to K40). They basically have everything needed to plug in your K40 electronics into the ACR & bridge to the Smoothie.


---
**Jim Hatch** *July 18, 2016 19:16*

+1 on Scott's kit - one stop shopping and ready to install. Even some if us tinkerers are looking forward to it because it saves a lot of time ordering parts from different places, having them shipped from all over the globe and then piecing it all together. 😀 Time is money but the ACR will also reduce frustration which is pricelezz👏


---
**Ariel Yahni (UniKpty)** *July 18, 2016 20:13*

Also **+Ray Kholodovsky**​ has a solution available 


---
**Derek Schuetz** *July 18, 2016 20:23*

You could get and azteeg x5 mini and mini Viki 2 lCD for about the same price but in a much smaller form package toninstall


---
**Arthur Wolf** *July 18, 2016 22:12*

You don't need a level shifter, smoothie supports "open-drain" pin configuration, which means the pin is 5V already ( just inverted ). Level shifter makes it a tad less confusing if you don't understand how open-drain works, but it's not required.


---
**Ariel Yahni (UniKpty)** *July 18, 2016 22:20*

**+Arthur Wolf**​ is this the same as just connecting the fire cable to the 2.4 pin? 


---
**Arthur Wolf** *July 18, 2016 22:21*

**+Ariel Yahni** A good explanation of open-drain is the external driver example : [http://smoothieware.org/general-appendixes#external-drivers](http://smoothieware.org/general-appendixes#external-drivers)


---
**Ariel Yahni (UniKpty)** *July 18, 2016 22:28*

So **+Arthur Wolf**​ that means laser fire  to ground pin on pwm connector? 


---
**Arthur Wolf** *July 18, 2016 22:29*

**+Ariel Yahni** something like that, I've never tried it, and laser psus confuse the hell out of me, i'd have to look at a manual for one again.


---
**Ariel Yahni (UniKpty)** *July 18, 2016 22:31*

**+Arthur Wolf**​ the reason I ask it's because that the way I have it. Different I don't have an original smoothieboard :(


---
**Ariel Yahni (UniKpty)** *July 19, 2016 03:29*

**+Ray Kholodovsky** **+Alex Krause** Read Link above from arthur


---
**Corey Renner** *July 20, 2016 00:44*

I'll try it without the level shifter then.  My smoothie shipped today, excited.  I got the smoothie because I believe in supporting hardworking developers rather than Chinese pirates.  How does the azteeg fit in, is it a Chinese clone, or is it its own thing?


---
**Ariel Yahni (UniKpty)** *July 20, 2016 01:13*

**+Corey Renner**  Azteeg is from a company called Panucatt manufacturer of smoothie compatible board and its located in california and owned by **+Roy Cortes** 


---
*Imported from [Google+](https://plus.google.com/116200173058623450852/posts/65eb5zk3Y6o) &mdash; content and formatting may not be reliable*
