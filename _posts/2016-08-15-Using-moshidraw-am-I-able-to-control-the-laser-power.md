---
layout: post
title: "Using moshidraw am I able to control the laser power?"
date: August 15, 2016 03:01
category: "Discussion"
author: "Evaristo Ramos"
---
Using moshidraw am I able to control the laser power? I have the k40 version with a knob for the current regulation.





**"Evaristo Ramos"**

---
---
**HalfNormal** *August 15, 2016 03:09*

You cannot control the laser with Moshidraw. You can only use the knob for manual control. You would need to have the upgraded Moshi board to do so. You are better upgrading to the smoothie and using LaserWeb.


---
**Vince Lee** *August 19, 2016 17:28*

There are power settings in MoshiDraw, but I believe they only work on the rare K40 units with the digital power control/meter, not units with the analog meter and potentiometer knob.


---
*Imported from [Google+](https://plus.google.com/117937735107415918607/posts/G17GBQf8Fjs) &mdash; content and formatting may not be reliable*
