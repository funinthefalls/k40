---
layout: post
title: "Control your laser with your phone While cruising the interwebs, came across this app in the Google Play Store"
date: April 09, 2016 05:41
category: "Software"
author: "HalfNormal"
---
Control your laser with your phone



While cruising the interwebs, came across this app in the  Google Play  Store. It is in Vietnamese but Google translate made it possible to figure out what was going on. It is called kLaserCutterController. 

What do you think?

[https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=7&cad=rja&uact=8&ved=0ahUKEwj-h7WL7IDMAhVI0GMKHQIgCHUQFggwMAY&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.ionicframework.k574783%26hl%3Den&usg=AFQjCNG97wTENbpyYDkUxPRA1Dzd3Un0TA&sig2=1GpnO7KaiOdi4QxjvZ-Xbw](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=7&cad=rja&uact=8&ved=0ahUKEwj-h7WL7IDMAhVI0GMKHQIgCHUQFggwMAY&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.ionicframework.k574783%26hl%3Den&usg=AFQjCNG97wTENbpyYDkUxPRA1Dzd3Un0TA&sig2=1GpnO7KaiOdi4QxjvZ-Xbw)





**"HalfNormal"**

---
---
**Asa Jota** *April 13, 2016 12:20*

I would love to be able to cut from my tablet! Looks like it uses an Intel Edison to connect to the machine.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/WcefLpcKbk1) &mdash; content and formatting may not be reliable*
