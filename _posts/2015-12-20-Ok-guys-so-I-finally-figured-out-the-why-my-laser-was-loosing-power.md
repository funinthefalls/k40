---
layout: post
title: "Ok guys, so I finally figured out the why my laser was loosing power"
date: December 20, 2015 04:42
category: "Hardware and Laser settings"
author: "ChiRag Chaudhari"
---
Ok guys, so I finally figured out the why my laser was loosing power. It does that only on lower power levels but if I go little over those power settings the Ampmeter reads 10mA but it stays pretty stable. Laser is loosing power only at lower power levels, and I can hear it arching on Anode side too.



Not just that I can cut through 1/8" hobby plywood with 43[feed=900,ppm=0 to 4000]. The cuts are nice and thin on the bottom but on top they are black pretty burned up and wide. Have not installed the air assist head yet. But it seems like the PPM is not changing anything for me. 



Can anyone tell me if you have K40 upgraded to Ramps, what PPM you use to cut different meterials?





**"ChiRag Chaudhari"**

---
---
**I Laser** *December 20, 2015 09:29*

"The cuts are nice and thin on the bottom but on top they are black pretty burned up and wide."



Not sure about your other issues, but that would be caused by an incorrect focal length.


---
**Andrew ONeal (Andy-drew)** *December 22, 2015 05:54*

Got my first line to cut this evening but I'm struggling to understand inkskape and visicut won't communicate with laser error com port already in use? Set up a photo in Corel then imported into inkskape but don't know how to set layer, or settings for print job. My head hurts from trying to figure this out, but I also see the issue above in my laser where lower power struggles to maintain.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 24, 2015 08:23*

I am still using the original controller board & CorelLaser with my K40 & I notice that anything under 10mA flickers unstably (on the ammeter) when it is firing, but when I tested 10mA & above it actually stays pretty much spot on the value that is set. I wonder, does yours have a potentiometer or the digital power setter thing? Mine has the pot.


---
**Andrew ONeal (Andy-drew)** *December 24, 2015 17:43*

Potentiometer, mine came with moshidraw board 4.6 but couldn't get it to work. In all I bit of way more than I could handle in this modification. I've had a blast learning though.


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/CH98D2u9rin) &mdash; content and formatting may not be reliable*
