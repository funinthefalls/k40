---
layout: post
title: "Hey all. for some reason my LaserDRW isnt cutting from 'inside out'"
date: August 01, 2016 23:20
category: "Original software and hardware issues"
author: "Bob Damato"
---
Hey all. for some reason my LaserDRW isnt cutting from 'inside out'. Its cutting the border first, then it drops in and Im screwed for the inside cuts!  It matter if its jpg or bmp or whatever? Otherwise its behaving perfectly, I just  need to fix that one item. I tried rebooting both the pc and the cutter but no go.





**"Bob Damato"**

---
---
**greg greene** *August 01, 2016 23:42*

Isn't there aq checkbox for that?  Look on the engrave/cutting/marking menu for box you check to get that or not get that


---
**Bob Damato** *August 02, 2016 00:07*

Yes, there is. I forgot to mention that the checkbox is checked. I tried it checked and unchecked and both ways gave me the virtual middle finger


---
**greg greene** *August 02, 2016 00:12*

what is the weight and width of the lines?


---
**Alex Krause** *August 02, 2016 00:31*

Have you tried using the coreldraw plugin


---
**greg greene** *August 02, 2016 00:36*

You can try making the inside cuts task 1 and the out side cuts task 2


---
**Bob Damato** *August 02, 2016 01:01*

Thanks guys. Im not using the corel plug in because it says it needs corel draw installed, and the whole  suite is $500. Ouch. That wont be happening any time soon.  I didnt know you can have different tasks with laserDRW, Ill have to check into  that!


---
**greg greene** *August 02, 2016 01:04*

a copy supplied with the machine - or - you can contact the seller for a copy.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 02, 2016 07:06*

Or there is a copy of CorelDraw x5 (with keygen) hosted in my dropbox. A few have been having issues with it saying it is non-genuine serial number lately though. I don't have that issue myself (blocked it in Windows Firewall).


---
**Bob Damato** *August 02, 2016 13:26*

Thank you Yuusuf. Any chance I can try it?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 02, 2016 13:32*

**+Bob Damato** Hi Bob. You can find the files available here:



[https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0](https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0)



You will want to grab all 3 files (2 x .rar archive, 1 for CorelDraw x5 & the other for CorelLaser & 1 readme.txt that I wrote to explain the installation process).



After you install, you want to go into Windows Firewall with Advanced Security (windows 10, not sure what it is called in previous versions). In there go to the outbound rules & block the following two files (see image):

[https://1drv.ms/u/s!AqT6a-6Vl15Nh5AfwubiG4PfCttIVQ](https://1drv.ms/u/s!AqT6a-6Vl15Nh5AfwubiG4PfCttIVQ)



That way it can't connect & check if it is a valid Corel install.



edit: side note, you will still need the supplied USB key that came with your machine plugged into the computer to operate the laser.


---
**Bob Damato** *August 02, 2016 13:33*

thank you sir, Ill report back!


---
**Alex Krause** *August 02, 2016 14:15*

**+Yuusuf Sallahuddin**​ my corel got flagged yesterday... it was less headache to start my smoothie conversion than trying to get the m2nano going


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 02, 2016 14:18*

**+Alex Krause** Yeah, I haven't had need or time to do much lasering lately & I'm waiting on Scott Marshall's ACR kit to arrive (it's on the way) so I can get onto my Smoothie conversion. How goes your conversion? All done?


---
**Bob Damato** *August 02, 2016 23:54*

Yuusuf, thanks for the download. When I go to run it the first time it is asking for an email address and password to create an account or log in using an existing account. No matter what I put in there it wont take it. It says an account using that password has already been made and tells me to log in with it. If i log in with it, it says wrong password. I cant seem to get past this. Any ideas?

Bob


---
**Bob Damato** *August 03, 2016 00:57*

Ahhhh got it, and was able to get to the point where it asks for the serial, but the app is saying its an invalid serial number from the keygen. :(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 03, 2016 02:54*

**+Bob Damato** Hey Bob. I'll give it a test again on my end & see if I can come up with any solution to the issue. I'll let you know in a few hours.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 03, 2016 03:15*

**+Bob Damato** I've just uninstalled my version of X5 & have reinstalled. I now encounter the same error you are receiving with "invalid serial number" constantly also. It never used to do that, so now I also have to figure out how to make it work haha.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 03, 2016 03:59*

Okay, I've found another way to get it to work with Corel Draw Technical Suite x7 & a different keygen. Currently working.



Get the keygen here:

[http://free.appnee.com/corel-all-products-universal-keygen-by-x-force-for-win-32-64/](http://free.appnee.com/corel-all-products-universal-keygen-by-x-force-for-win-32-64/)



I used the first download link available (from zippyshare).



Then get Corel Draw Technical Suite x7 trial (1.5 gb) here:

[http://www.coreldraw.com/us/free-trials/?topNav=us#trial_cdtsx7](http://www.coreldraw.com/us/free-trials/?topNav=us#trial_cdtsx7)



I chose 64 bit version.



- Install Corel Draw x7, making sure to untick the option for "auto-update", open the keygen when asked for serial number.

- Generate Serial Number, keep the keygen open.

- Upon completion of install, open Corel Draw. It will prompt you for activation. Choose other activation, & phone Corel. Then type the Installation Code into the open keygen (in correct space, making sure to put the -'s).

- Generate Activation Code (make sure you don't generate serial again).



Should be activated then.



Then, you want to block it in Windows Firewall. I use Windows 10, so instructions may differ for your windows, but you want to go into Windows Firewall with Advanced Security.



- Go to Outbound Rules

- Create new rule, type program (you will do this twice).

- Point to the following 2 programs:



C:\Program Files\Corel\CorelDRAW Technical Suite X7\Programs64\CorelDRW.exe



& 



C:\Program Files\Corel\CorelDRAW Technical Suite X7\Programs64\DIM.exe



- Select Block from the list of options for what to do with network connections.

- Name the rules as something like Corel Draw Block & Corel DIM Block.

- Now it shouldn't connect to Corel to verify.


---
**Bob Damato** *August 03, 2016 12:29*

Thanks again, Ill give it a shot. I just went to download the trial software from Coreldraw and that link is for x8 and it is an 11mb installer. will that work ok too?


---
**Bob Damato** *August 03, 2016 12:31*

hows this? [http://www.coreldraw.com/us/product/technical-suite/](http://www.coreldraw.com/us/product/technical-suite/)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 04, 2016 05:04*

**+Bob Damato** Nah, the x8 link didn't work. Tried it first. There is a link on the right hand side somewhere for x7 technical suite or something. It did work. Otherwise, I can put the files needed in my dropbox later (actually they're already there, I just haven't synced it yet).


---
**Bob Damato** *August 04, 2016 13:22*

Will coreldraw x8 work with our laser? Not the technical suite just x8? Ill actually buy it, I was able to get a discount code which makes it affordable.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 04, 2016 13:29*

I can't comment directly, but I have a feeling that some others are using x8. I suggest either doing a quick search in the community or make a post & ask if it works. Someone will know for sure. I'd hazard that it will, provided it is not a home & student version, as there have been reports that home & student versions (not x8, but older x?).



edit: I found 1 post where someone was trying to get x8 to work. I tagged you into the post so you can see his reply to check if it works with x8.


---
**Bob Damato** *August 04, 2016 13:51*

Thanks again Yuusuf.  For some reason I dont see a tag (im new to the google communities) but Ill still keep trying to figure it out. I know you cant use home and education as they dont support plugins. id be really surprised if x8 isnt supported but you never know! Ill keep looking for the post and do a little more digging too. Thanks :) 

bob


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 04, 2016 13:59*

I'll drop the link here so you can check in & follow it if need be. [https://plus.google.com/+StevieRodgerCambyCrafts/posts/XhYFBN9XS6x](https://plus.google.com/+StevieRodgerCambyCrafts/posts/XhYFBN9XS6x)



Hopefully Steve can get back to you & let you know if he managed to get it to work.


---
**Jim Hatch** *August 04, 2016 17:04*

I have the plug-in working in my copy of X8.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/Fne3eR1SFPc) &mdash; content and formatting may not be reliable*
