---
layout: post
title: "Hello, I was installing a K40 Laser Engraver yesterday for a friend"
date: June 29, 2016 08:13
category: "Discussion"
author: "Eoin Kirwan"
---
Hello,



I was installing a K40 Laser Engraver yesterday for a friend. I had one or two issues, The first one was that a message would come up saying 'Invalid Device-id, can not use it'. Can anyone help me with solving this? I have also installed three softwares, 'Coral Laser 2013, LaserDRW 2013, WinsealXP. I have an orange coloured USB. Does the oranges USB allow for use on a certain software?



![images/ad0d04bbd081d628967e3869da7a685a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad0d04bbd081d628967e3869da7a685a.jpeg)
![images/1e9302a55365ad388ac71c7f367a332a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e9302a55365ad388ac71c7f367a332a.jpeg)
![images/c4a62ad102c11f6630a4dd97ab887475.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4a62ad102c11f6630a4dd97ab887475.jpeg)

**"Eoin Kirwan"**

---
---
**Ned Hill** *June 29, 2016 15:43*

See this post [https://plus.google.com/101709945506358389673/posts/YTovuRUzHE8](https://plus.google.com/101709945506358389673/posts/YTovuRUzHE8)  should get you going.  Let us know if you have any questions.


---
**Ned Hill** *June 29, 2016 15:51*

The dongle is for the corelLaser.  The winsealXP is the CorelLaser boot program.   When you open corelLaser, winsealXP will automaticly launch.  Don't bother with LaserDraw in my opinion.


---
*Imported from [Google+](https://plus.google.com/103438187016886371314/posts/7R1HFnYuWaP) &mdash; content and formatting may not be reliable*
