---
layout: post
title: "First results with the freshly modded mashine"
date: November 20, 2016 23:26
category: "Smoothieboard Modification"
author: "ViciousViper79"
---
First results with the freshly modded mashine. Cut out a new control panel for the GLCD of the Smoothieboard. Worked pretty well. Also 3D carving tests but I need mode practice and tests to find the right numbers for movement speed and laser power. PWM seems to work though so no rewiring required to have 1 PWM line and one enalbe line. All done with PWM on the L line of the power connector.



![images/4f13cf9763913b03776ca8aa02cb48e2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f13cf9763913b03776ca8aa02cb48e2.jpeg)
![images/25666e256e8bf84973a85547a6c775a4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/25666e256e8bf84973a85547a6c775a4.jpeg)
![images/e9a394262911ee9d3c0094d52e2b4576.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9a394262911ee9d3c0094d52e2b4576.jpeg)

**"ViciousViper79"**

---
---
**Kelly S** *November 20, 2016 23:42*

Looks good, I will have to make one of them panels soon myself.  


---
**ViciousViper79** *November 20, 2016 23:50*

This is the SVG file. It has the holes for the power switch, amp meter, amp pot and push buttons. Also the mount for the GLCD. Please note that the mounting holes of the panel it self are off! The did not fit 100% and I had to adjust the holes a bit. The other measurements for the mountings are on point.



[https://drive.google.com/file/d/0B6CjTtc2SvU1N0F1M3p4WkhxeU0/view?usp=sharing](https://drive.google.com/file/d/0B6CjTtc2SvU1N0F1M3p4WkhxeU0/view?usp=sharing)


---
**Kelly S** *November 20, 2016 23:54*

Thanks much.  :D


---
**Alex Krause** *November 21, 2016 07:03*

Are you using g1 g0 to turn on and off the laser or M3 M5?


---
**ViciousViper79** *November 21, 2016 15:49*

I am using G0 and G1. That is also well supported on LaserWeb.


---
*Imported from [Google+](https://plus.google.com/+ViciousViper79/posts/W5YJcqM3YQv) &mdash; content and formatting may not be reliable*
