---
layout: post
title: "What is the cheapest place to get acrylic?"
date: January 26, 2016 01:04
category: "Materials and settings"
author: "3D Laser"
---
What is the cheapest place to get acrylic?





**"3D Laser"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2016 01:08*

Estreetplastics.com has been good to me. 


---
**Anthony Bolgar** *January 26, 2016 01:15*

They only ship within the USA.


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2016 01:16*

Yupp, that's where I am.  Looking around for a local place may be best otherwise.  


---
**ThantiK** *January 26, 2016 01:20*

There's a place local to me that sells surplus aluminum/plastics, etc.  May want to look for a place like that near you.


---
**Sunny Koh** *January 26, 2016 03:21*

For me, my uncle told me that his supplier is Dama Trading for his plastics business in Singapore. Where in the world are you located?


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/iHbFx7u8Z2o) &mdash; content and formatting may not be reliable*
