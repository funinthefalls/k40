---
layout: post
title: "What should I pick for my laser controller model in laser drw settings?"
date: August 23, 2017 16:28
category: "Hardware and Laser settings"
author: "Jeff C"
---
What should I pick for my laser controller model in laser drw settings? I have a Nano 2016G Nano or KETAI GOLD 5. That is the only model # I can find. I have been using the B1 selection but it really seems slow. It auto finds the right s/n and the b1 is what it uses. Any help would be great





**"Jeff C"**

---
---
**Scorch Works** *August 23, 2017 16:39*

I would guess M2.  The '2016' implies it is relatively recent and M2 is the highest rev and most common.


---
**BEN 3D** *August 23, 2017 17:37*

This is my Board , on the top left Stands m2nano may you can Compare this with yours![images/b250fd528d76c499e976e7cf590ab1cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b250fd528d76c499e976e7cf590ab1cd.jpeg)


---
**Jeff C** *August 23, 2017 22:34*

Thanks for getting back Ben, your link is bad. Here is mine


---
**Jeff C** *August 23, 2017 22:41*

I'll try M2, thanks scorch works




---
**BEN 3D** *August 23, 2017 22:42*

Yeah! Gz!


---
**Jeff C** *August 23, 2017 22:43*

Cant figure out how to post pic here, did you see the pic I sent you Ben


---
**Jeff C** *August 23, 2017 22:54*

![images/1f78f51f972c163bf521a9f31cc4b111.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f78f51f972c163bf521a9f31cc4b111.jpeg)


---
**BEN 3D** *August 24, 2017 08:14*

Sorry to tell but it seems this is not the same board as i have. May some other guy can told you more.


---
**Jeff C** *August 25, 2017 00:25*

Well tried them all. Only B1 seems to work (sort  of) It is very slow though


---
**Scorch Works** *August 25, 2017 01:10*

Wow, I am shocked by that.  I was confident M2 would work. Thanks for the update.


---
*Imported from [Google+](https://plus.google.com/101295519284925316410/posts/Wz6BSUYNkMZ) &mdash; content and formatting may not be reliable*
