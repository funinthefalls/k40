---
layout: post
title: "(EDIT 12/12/17- Solved, see comments ) Hello my fellow laser experts and enthusiasts"
date: December 09, 2017 19:03
category: "Hardware and Laser settings"
author: "Madyn3D CNC, LLC"
---
(EDIT 12/12/17- <b>Solved, see comments</b> )



Hello my fellow laser experts and enthusiasts. Hope everyone is staying busy for the holidays! Unfortunately for me, I have encountered a problem while firing up my #K40 laser for the first time in a few months. Prior to this, it has been working fine. 



There is some sort of buzzing/humming/vibrating noise when the machine is powered on. The noise happens perpetually every 4-5 seconds or so. I've heard plenty of noises that have been new to my ears, coming from a K40, but not this one. Best way to describe it is to shoot a video, so here it is. 



If anyone has encountered this noise before, or if you think you might know what it is, I would be grateful for your suggestions and/or analysis of what this noise could be.  I honestly cannot tell if it's coming from the LPSU, the steppers, or what. I'm a little hesitant about sticking my head in the machine while it's making this noise. There does not seem to be any LED's dimming or fans slowing/stopping. 



.. <b>Machine type: [K40]</b>

.. <b>Controller type: [M2 Nano]</b>

.. <b>Firmware: [grbl .corel]</b>

.. <b>Problem tenacity: [new problem]</b>

.. <b>Suspected source of problem:[LPSU or Steppers]</b>

.. <b>Symptoms: [Buzzing/humming @ startup perpetually every 5 seconds or so]</b>

.. <b>Photos/video:[Video w/ Sound Attached With Post]</b>



#K40brokenoise

#noise

#K40broke

#K40coolingfan

#K40fan

#K40farmanimal

#K40troubleshoot

#K40dcfan



![images/110e6a9e879a7c8eb05a3efb6105c31c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/110e6a9e879a7c8eb05a3efb6105c31c.jpeg)



**"Madyn3D CNC, LLC"**

---
---
**Madyn3D CNC, LLC** *December 11, 2017 18:47*

**+Don Kleinschnitz** have you heard this noise coming from anyones machine before? I'd imagine you would be the guy, if anyone, to hear something like this coming from a K40. 



Happy Holidays. 


---
**HalfNormal** *December 12, 2017 00:12*

**+Madyn3D CNC, LLC** That is the bearings/bushings going bad in either the exhaust fan or the fan for the power supply. As loud as it is, I would bet it is the AC powered exhaust fan on the back of the machine.


---
**Don Kleinschnitz Jr.** *December 12, 2017 13:44*

**+Madyn3D CNC, LLC** I think you have a farm animal in your machine :!



I have never heard such a sound. It sounds mechanical like a bearing or stepper as **+HalfNormal** suggests. 



I doubt it is from the LPS but I am continually  surprised by what it can do...



Since the sound happens regularly I would unplug suspects one at a time, power the machine on and then listen until you have isolated it.



Let us know what you find .....


---
**Madyn3D CNC, LLC** *December 12, 2017 18:27*

**+HalfNormal** **+Don Kleinschnitz** Speaking of farm animals, one of the first things I checked for were indications that a critter may have found a new home, but there's no critters! 



I'm going to check out both the exhaust fan and the LPSU fan now. Thanks for your help, I'll report back once I isolate where it's coming from. I probably should have done that first, but I have my water, lights, and air all harnessed into one main power breaker, each on it's own gate of course. I liked the idea of everything turning on at once, but I failed to consider how that might make troubleshooting a little more work. 


---
**Madyn3D CNC, LLC** *December 12, 2017 19:43*

You guys are the best. 



The noise is coming from the cooling fan in the back of the machine. NOT the main exhaust fan, NOT the LPSU fan, but the larger fan mounted to the back of the machine. I specify because I've noticed not all models have that cooling fan mounted in the back of the machine. 



I'll go ahead and edit this post with some hashtags to help any potential users in the future that come across this sound. 



As cold as it is in my workshop right now, I don't even need that fan running right now with small jobs, but I do want to replace it ASAP. This fan has NO markings on it, aside from a Chinese QC sticker. It's housed in a black metal frame rather than a plastic frame. I'm wondering if I can just replace it with a standard DC  PC cooling fan. I have a whole drawer full of different sizes. 


---
**Don Kleinschnitz Jr.** *December 12, 2017 19:59*

**+Madyn3D CNC, LLC** is it the muffin fan mounted on the back wall of the box?

Isn't that an AC fan? Where is it connected to?


---
**HalfNormal** *December 12, 2017 20:01*

That is normally an AC fan


---
**Don Kleinschnitz Jr.** *December 12, 2017 20:17*

**+HalfNormal** that what I thought.

**+Madyn3D CNC, LLC** should be easy to get an AC fan replacement on ebay/amazon?




---
**Madyn3D CNC, LLC** *December 12, 2017 20:31*

Metal frame, ground terminal at mounting holes. No markings on this fan, I'd like to know the specs for replacement. 

![images/ff683bb0ce4529b6a5bb62ffc638b80b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff683bb0ce4529b6a5bb62ffc638b80b.jpeg)


---
**Madyn3D CNC, LLC** *December 14, 2017 14:49*

**+Don Kleinschnitz** That's actually the first time I've heard that term "muffin fan" lol! I had to google it. I've always just called them AC fans for a PC. I like muffin fans better. 


---
**Timothy Rothman** *December 16, 2017 17:14*

Are you using a stock fan, I'm also having a fan making buzzing, lots of noise intermittently.  When you first hear it, it is really disconcerting, and since you've made a lot of mods you may or may not have the same issue. I place my finger on the hub (carefully of course) and it goes away.  I will be replacing with a fan with ball bearings.


---
**Madyn3D CNC, LLC** *December 17, 2017 15:33*

**+Timothy Rothman** Yes it's a stock fan. Won't be for long though! I was going to just get a cheap replacement PC fan but I salvaged a nice aluminum fan from an old custom PC. 



After tearing down the stock muffin fan to find the actual problem, it's no wonder these things go out. The bushings are basically held in place by grease lol


---
**HalfNormal** *December 17, 2017 15:36*

One thing that I did with my exhaust fan was to turn it around so that it actually drew air into the cabinet. This way I did not draw the air from the cut area into the power supply and electronics area. This also helped put a positive pressure into the cabinet.


---
**Madyn3D CNC, LLC** *December 17, 2017 17:28*

**+HalfNormal**  That's a great idea, keeps the PSU clean(er) and gives a little boost to the positive pressure. I'm probably going to use that advice when I install my new one. Thanks!




---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/fLVTZWBenzp) &mdash; content and formatting may not be reliable*
