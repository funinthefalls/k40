---
layout: post
title: "Hello all, K40 machine with ramps/marlin modification and using LaserWeb running on a raspberry pi: Originally the machine was regulating laser power by a separate analogic power selector, i removed it and the connected the"
date: February 23, 2016 09:54
category: "Hardware and Laser settings"
author: "Franco \u201cnextime\u201d Lanza"
---
Hello all,

K40 machine with ramps/marlin modification and using LaserWeb running on a raspberry pi: 



Originally the machine was regulating laser power by a separate analogic power selector, i removed it and the connected the input to the ramps to be used as PWM.



Now i want to align my laser, but i don't know how to set the PWM to 10% of power and then use the "test" push button on the panel to fire it manually, is there any way to do that?





**"Franco \u201cnextime\u201d Lanza"**

---
---
**Anthony Bolgar** *February 23, 2016 11:28*

If you are running Arduino/Ramps 1.4 with Turnkey Tyranny's Marlin firmware, it is in the Laser Functions Menu--->Test Fire-->Dropdown menu with various power and time settings.


---
**varun s** *February 23, 2016 15:58*

**+Peter van der Walt**​


---
**Franco “nextime” Lanza** *February 23, 2016 17:18*

i don't have the LCD/sdcard yet, i think you were talking about some menu there?



As host i'm using laserweb, and there is no this menu as i can tell.



Anyway, i found the test button was having a broken wire, so, after sobstitution, it works and my issue is solved, thanks anyway




---
*Imported from [Google+](https://plus.google.com/+FrancoLanzanextime/posts/dE4ZQXvKY7u) &mdash; content and formatting may not be reliable*
