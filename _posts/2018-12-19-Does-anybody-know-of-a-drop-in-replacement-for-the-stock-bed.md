---
layout: post
title: "Does anybody know of a drop-in replacement for the stock bed?"
date: December 19, 2018 22:41
category: "Modification"
author: "Sean Cherven"
---
Does anybody know of a drop-in replacement for the stock bed?

Either honeycomb or something similar? 

I want something that'll go in without much modification.





**"Sean Cherven"**

---
---
**Stephane Buisson** *December 20, 2018 08:29*

well it's 2 way to look at the problem when dealing with distance. moving bed or moving lens. (I will soon look into this option if playing with 2 lens 50.8/38.1) re-desiging a  movable head with 2 lens).



About bed other things to consider : openable to leave space for object to be engraved, reducing contact to object to limit fume marks.



so I do like the honeycomb tray bed because you could remove it, leaving a gap.



 aluminium honeycomb arrive folded flat strip into in envelope (ebay). Just insert it into a self made frame.


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/gGZmv6RCZ5i) &mdash; content and formatting may not be reliable*
