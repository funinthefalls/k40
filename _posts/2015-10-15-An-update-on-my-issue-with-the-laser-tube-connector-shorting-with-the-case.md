---
layout: post
title: "An update on my issue with the laser tube connector shorting with the case"
date: October 15, 2015 15:31
category: "Hardware and Laser settings"
author: "george ellison"
---
An update on my issue with the laser tube connector shorting with the case. I have re-soldered the connection and covered it with silicone. When i press the test fire the tube is not firing a laser as the connection is still arcing with the case? Any idea on what to do next and why this is happening? Thanks





**"george ellison"**

---
---
**Joey Fitzpatrick** *October 15, 2015 17:01*

Tube has most likely failed.  Very common problem if the tube has ever overheated. They seem to crack around the coolant tube seams(inside the tube)


---
**Gregory J Smith** *October 16, 2015 02:13*

Hmm. Might still be a dud cable or connector. Make sure the cable isn't routed near metal. It's 22kv so weird things can happen. Silicone sealant isn't great for high voltage. Self amalgamating rubber tape is better I've found. Tubes are expensive so try to track the actual arc route down in darkness. Don't get too close. I'd hate to add an RIP to this group.


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/6kGAyB6zEe8) &mdash; content and formatting may not be reliable*
