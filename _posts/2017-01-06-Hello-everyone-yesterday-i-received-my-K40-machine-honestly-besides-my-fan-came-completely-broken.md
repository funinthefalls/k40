---
layout: post
title: "Hello everyone, yesterday i received my K40 machine, honestly besides my fan came completely broken"
date: January 06, 2017 22:05
category: "Hardware and Laser settings"
author: "Krist\u00f3f Gilicze"
---
Hello everyone, yesterday i received my K40 machine, honestly besides my fan came completely broken. Everything seems okay.

So i aligned all the mirrors, and cleaned them. But i am not able to cut plywood  (3mm with 5mm/s 20mA), i have seen someone wrote for reference he is cutting 3mm with 10mm/s 10 mA.

What should i check?





**"Krist\u00f3f Gilicze"**

---
---
**greg greene** *January 06, 2017 22:14*

Alignment - search for the flying wombat alignment on the web - there are many ways to align the beam - only a few work well.


---
**Ariel Yahni (UniKpty)** *January 06, 2017 22:15*

That was me. There are many things to check: recheck alignment, water cooling temp, lens correct position.


---
**greg greene** *January 06, 2017 22:27*

and focus


---
**Phillip Conroy** *January 07, 2017 00:13*

Stock focal lens in k40 and clones is 50.8mm ,so aim for that ddistance to mmiddle ofwhat you are cutting




---
**Jim Hatch** *January 07, 2017 01:45*

And check that the lens is installed correctly - bump side should be up. Lots of times they come installed upside down.


---
*Imported from [Google+](https://plus.google.com/107969900550483335330/posts/RE2wt9zssmP) &mdash; content and formatting may not be reliable*
