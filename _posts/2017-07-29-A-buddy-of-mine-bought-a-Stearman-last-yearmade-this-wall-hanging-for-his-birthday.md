---
layout: post
title: "A buddy of mine bought a Stearman last year...made this wall hanging for his birthday..."
date: July 29, 2017 18:21
category: "Object produced with laser"
author: "Mike Meyer"
---
A buddy of mine bought a Stearman last year...made this wall hanging for his birthday...

![images/d28c68f61035d0e87f0c3cd287b70c72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d28c68f61035d0e87f0c3cd287b70c72.jpeg)



**"Mike Meyer"**

---
---
**Kevin Lease** *August 04, 2017 17:30*

That is beautiful

Can you explain how you made this?

Do you cut each part out and paint separately and reassemble or do you cover with tape and laser cut mask?


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/C9kuTWJ6J9T) &mdash; content and formatting may not be reliable*
