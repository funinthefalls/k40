---
layout: post
title: "okay, so im having trouble finding any information on this"
date: July 26, 2016 08:27
category: "Modification"
author: "Somerset Coker"
---
okay, so im having trouble finding any information on this.



and i cant find anyone else who's done it.



i want to make the working area larger. what and where can get find the rail that the carriage runs on?





the strange B shaped extrusion. any idea what i would search for to find a 1m length on ebay?





**"Somerset Coker"**

---
---
**Anthony Bolgar** *July 26, 2016 11:08*

You could just buy a length of v-slot, 4 wheels and a gantry plate from OpenBuilds part store, and just swap out the axis completely


---
**HalfNormal** *July 26, 2016 12:51*

**+Somerset Coker** A quick google search will net you plenty of xy gantries but they are pricey. There are a lot of DIY solutions if you are mechanically inclined. Look for CNC and laser builds. Good Luck!


---
*Imported from [Google+](https://plus.google.com/112243326507741730318/posts/5BsEfxnT68o) &mdash; content and formatting may not be reliable*
