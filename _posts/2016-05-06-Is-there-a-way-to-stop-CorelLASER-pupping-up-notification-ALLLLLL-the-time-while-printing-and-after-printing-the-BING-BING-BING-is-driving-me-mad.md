---
layout: post
title: "Is there a way to stop CorelLASER pupping up notification ALLLLLL the time while printing and after printing the BING, BING, BING is driving me mad!"
date: May 06, 2016 18:57
category: "Discussion"
author: "Pigeon FX"
---
Is there a way to stop CorelLASER pupping up notification ALLLLLL the time while printing and after printing the BING, BING, BING is driving me mad!



Also, Cut lines are going around two passes when I select just one? (this happens with Vector shapes drawn in Corel or imported form Illustrator)





**"Pigeon FX"**

---
---
**Pigeon FX** *May 06, 2016 19:00*

Guessing the second issue of two passes  can be resolved by selecting 1 for Pixel size when cutting! 


---
**Anthony Bolgar** *May 06, 2016 19:07*

set all vector lines to hairline in Corel, should solve the double cut issue.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 19:36*

Or if hairline doesn't work, you can set to 0.01 by typing it manually & it may work (does for me, hairline doesn't for me).



In regards to the Bing Bing Bing all the time, there is only 1 way to avoid that. Firstly, you want to have "Only Selected" chosen in the engraving/cutting window. Next, you want to have "Method: All Layers" set instead of dialog. Then, when you are wanting to do multiple tasks, click "Add Task", making sure that "Starting" tickbox is off. Keep adding tasks (the toolbar may disappear from top of Corel window, so if so use the icon in the windows notifications area). Once you've added the amount of tasks you want, then right click notification icon, then choose [1] task waiting (with the play button) & it should go. Note, I haven't managed to avoid the BING BING when doing setting multiple passes using the "repeat" option in the cutting/engraving dialog. If I want to cut multiple repeats without the BING BING & having to click "start" every repeat, then I just do 1 pass & add it X amount of times to the queue (time consuming, but saves me having to come back & forth to the machine every couple mins).


---
**Pigeon FX** *May 06, 2016 20:25*

Thanks Yuusuf, I'v been having the vanishing tool bar issue, but can get back into it via the systems try (when Bing Bing notifications are not obscuring it!). 



thanks for the setting, think I'm starting to get to grips with the quirks, thanks to all the help here and have been having fun cutting and engraving acrylic......it funny how everything starts looking like a laser cutting job once you got one!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 21:01*

**+Pigeon FX** Yeah the software is a bit problematic mainly due to the total lack of support or documentation in English (or a language that any of us understand). I imagine that even in Chinese the minimal documentation provided probably doesn't make much sense. Also, I totally agree that everything starts looking like another project to laser. I keep finding all sorts of things that NEED to be lasered.


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/eBRUKMEJNpM) &mdash; content and formatting may not be reliable*
