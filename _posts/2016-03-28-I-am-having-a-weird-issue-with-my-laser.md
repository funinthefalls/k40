---
layout: post
title: "I am having a weird issue with my laser"
date: March 28, 2016 01:48
category: "Discussion"
author: "3D Laser"
---
I am having a weird issue with my laser.  overall it has been cutting great.  but there have been two or three spots where it fails to cut through.  at first I thought it was the material but it has been consistant over the several pieces that I have cut.  any ideas what it could be?  I am cutting 1/8 birch ply





**"3D Laser"**

---
---
**Thor Johnson** *March 28, 2016 02:11*

On mine, I got it "close" to aligned, but at certain spots on the bed, the beam would hit the edge of the final mirror assembly -- it was causing a "ghost beam" that was about 3mm "under" the main beam at certain points.  I recalibrated and have been having fun ever since.  40W power, 5mm/s for cuts.


---
**3D Laser** *March 28, 2016 02:14*

I am thinking it is something like that but its two spots less than a half on an inch it cuts both before and after.  guess I have some more fine tuning to do


---
**Thor Johnson** *March 28, 2016 02:18*

What speed/power are you running (I might be overkill)?


---
**3D Laser** *March 28, 2016 02:18*

10 speed at 10ma.  it takes me two passes


---
**Thor Johnson** *March 28, 2016 02:25*

I have 0-99% instead of a mA meter... what's your max mA?


---
**3D Laser** *March 28, 2016 03:09*

25-30% roughly


---
**Phillip Conroy** *March 28, 2016 04:08*

I cut 3mm mdf at 10ma and speed 7.5-clean focal lens-even with air assist and 2 water traps i still get water on focal lens-it must be clean or the water spreads the beam- i have to clean my focal lens every 20 cuts,i can not aford a 2 thousand dollar air dryer. A goog inducator of dirty focal lens is the cut on the top is wider than normal 


---
**Alex Krause** *March 28, 2016 05:46*

**+Phillip Conroy**​ might try these after your other two mist separators Motor Guard DD1008-2 Mini Desiccant Filter, 2-Pack [https://www.amazon.com/dp/B0014DEV6Q/ref=cm_sw_r_other_awd_0tm-wbJM1WFX0](https://www.amazon.com/dp/B0014DEV6Q/ref=cm_sw_r_other_awd_0tm-wbJM1WFX0)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 28, 2016 05:48*

I'm curious if it is at the same spot on the bed every time. As I've noticed that sometimes when I cut through leather or MDF, certain spots don't cut thoroughly all the way through & it seems to be related to the specific points on the perforated metal sheet I am using as a bed (i.e. where there is the metal supporting the material the cut is less powerful & not all the way through, where there is perforation in the bed the cut is all the way through). So I'm wondering if it is certain spot on your laser bed, then maybe you have some issue with the bed supporting your material in those specific places.



Alternatively, if it is the same spots every time, your bed may not be the exact same focal distance from the lens, so some spots would be better in focus & thus cut better whilst others cut slightly less well.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/7Y8uQowLVeL) &mdash; content and formatting may not be reliable*
