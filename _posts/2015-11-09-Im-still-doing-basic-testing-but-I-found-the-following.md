---
layout: post
title: "I'm still doing basic testing, but I found the following..."
date: November 09, 2015 03:31
category: "Software"
author: "Todd Miller"
---
I'm still doing basic testing, but I found  the following...



My laser worked best running  LaserDRW doing  'trial'

etching <s>VS</s> Corel12's etching...



Using Corel12, engraving was a disaster but cutting

worked fine.  



Today I discovered the property box/settings  were

different between the two programs regarding the

MainBoard / Machine ID.



In Corel12, I changed the MainBoard setting to;



6C6879-LASER-M2  (same as set in LaserDRW)



and now Corel Engraving manager is working better.





**"Todd Miller"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 09, 2015 09:03*

Seems a lot of people had this same problem early on. I also had this same issue with the mainboard ID being incorrect in the CorelDraw software.


---
**Anthony Coafield** *November 17, 2015 04:13*

I had the same issue. Set up and worked fine in LaserDRW, but had to set it up again in CorelLaser. 


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/5wDxJTpP2Pv) &mdash; content and formatting may not be reliable*
