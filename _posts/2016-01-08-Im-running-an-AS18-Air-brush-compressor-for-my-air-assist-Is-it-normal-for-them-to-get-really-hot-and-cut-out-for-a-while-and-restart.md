---
layout: post
title: "Im running an AS18 Air brush compressor for my air assist, Is it normal for them to get really hot and cut out for a while and restart"
date: January 08, 2016 10:32
category: "Discussion"
author: "Tony Schelts"
---
Im running an AS18 Air brush compressor for my air assist,  Is it normal for them to get really hot and cut out for a while and restart.   Is it a case of keeping it cool or is normal for these to do this.  Im in the Uk so its not scorching temperatures anyway





**"Tony Schelts"**

---
---
**Stephane Buisson** *January 08, 2016 11:51*

**+Tony Schelts** 

"Im in the Uk so its not scorching temperatures anyway"



Damn you are right ! but we can't really complain with this mild winter.


---
**Coherent** *January 08, 2016 18:16*

It's not unusual for the small diaphragm type compressors to get hot. By nature of how most all compressors work they create pressure & friction and will get hot. Even large pump driven compressors get hot.  A lot has to do with the quality of the compressor itself. As parts and bearings get worn, the friction increases more and may overheat more. The larger better quality airbrush compressors will have a larger tank that allows the unit to shut down and not have to run constantly which can help. There are a few good low cost compressors, but like everything else, you get what you pay for. The reviews on that model look pretty good but you didn't say if it was new or how old. If it's new I'd return it for a replacement.. if not it unfortunately sounds like yours may be in it's final days & requires repair if possible or replacement.


---
**Tony Schelts** *January 08, 2016 22:58*

Bought it on the 4 novenber 2015 so only a couple of months old


---
**Tony Schelts** *January 08, 2016 23:02*

This the model that I got [http://www.ebay.co.uk/itm/New-Professional-Air-Brush-1-6hp-Compressor-Airbrush-Hose-Stencils-Set-Craft-Kit-/310809786577](http://www.ebay.co.uk/itm/New-Professional-Air-Brush-1-6hp-Compressor-Airbrush-Hose-Stencils-Set-Craft-Kit-/310809786577)?  What is Auto Start Stop function???  pressure never moves anyway?>


---
**Todd Miller** *January 09, 2016 00:23*

I can say mine runs HOT, but has yet to cut-off.

I only turn it on when I "Laze" and then turn off.



I like the idea of a bigger tank to allow the unit to cool between cycles.


---
**Phil Willis** *January 09, 2016 13:53*

My airbrush (assist) compressor gets hot and has warning stickers all over it!! I try and keep it switched off when not burning.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/9LXEjWokPBH) &mdash; content and formatting may not be reliable*
