---
layout: post
title: "Hi I have recently gone of adventure I shall call it with a new K40 over the past month"
date: July 26, 2017 12:11
category: "Hardware and Laser settings"
author: "Tammy Mink"
---
Hi I have recently gone of adventure I shall call it with a new K40 over the past month. Anyways to the point mirror alignment issues getting the dot to remain even when it travels. After much trying again and again and again the and eventually realizing the y axis was not level somehow I shimmed the gantry up on both sides in the back by ¼ inch and then great it was finally level and went straight. The problem is I can’t get the beam coming out of mirror #2 to go straight to mirror #3 all the way down the path AKA the x-axis. The beam keeps wanting to go upwards, I took apparent the the bar twice and I have even been tinkering with the bracket for mirror #2 that is attached to the motor. Seems no matter what I do it wants to go up..I even bypass the laser head and use a piece of cardboard and still it goes up in case that was uneven. The very best I can align it is I can make it hit center in the middle by aligning the closest end to the bottom of the laser hole in the moving laser head, but by the time it gets at the end the beam splits because it’s reflecting off the edge. This only gives me a cutting and focused square in the center of about 2 inches and then it’s gone. Short version, y-axis was fixed with 1\4 shim on the back bolts, x-axis is the wild west, and I am running out of ideas.





**"Tammy Mink"**

---
---
**HalfNormal** *July 26, 2017 12:56*

**+Tammy Mink** You are on the right tract. Out of all the alignment tutorials, I found this one to be the best.

[k40laser.se - Mirror alignment - no more sweat and tears - K40 Laser](https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/)

I would start from scratch and un-do all the shims. One other thing to consider with all of this, the newer machines are flimsily made and have flex in the bottom. Make sure you are on a solid surface and do not move the machine around during setup.


---
**Tammy Mink** *July 26, 2017 13:18*

Yes that tutorial has been helpful, how I realized my gantry is not level (it also needed to be squared too a smidge). Though now that I have Y axis perfectly aligned using the shim, the X axis the light path is just not going straight. Though I guess you are right, I should start all over again, I am just not sure what I haven't reied already but I am sure there options..I hear sometimes peoples tubes aren't level.


---
**Don Kleinschnitz Jr.** *July 26, 2017 13:19*

Sounds a lot like your laser tube is not parallel with the X gantry movement? Have you adjusted the laser mounts?



Might be useful to post pictures of where the beam hits your targets on each mirror.


---
**Tammy Mink** *July 26, 2017 13:29*

Thanks for the responses. I have not touched the position of the laser tube itself, yet. I have tinkered with all the mirrors. Any tips or links on adjusting and aligning the laser tube correctly? 


---
**Tammy Mink** *July 26, 2017 13:36*

As a note I <b>highly</b> suspect my "new machine" was really a refurbished one. While the electronics are new and clean and work great some of the screws are rusty and it came literally covered in dust except for the electronics, power supplies and motors etc. I wonder if they just chucked a new tube in there without proper alignment. 




---
**HalfNormal** *July 26, 2017 13:52*

You should either adjust the Gantry or the tube but don't do both. You either adjust the tube to the Gantry or the Gantry to the tube.


---
**Tammy Mink** *July 26, 2017 14:03*

I think I am going to take the shims off and try adjusting the tube, I have have zero idea what I am doing there and I am nervous even touching it. I think it's off enough that I can never fix it with adjusting the gantry so I don't have much choice. Orginally the light went up as it went away on the y axis, the 1/4 inch shims on the back fixed that but even with the y-axis straight the light on the X-axis goes up. Maybe I need to take off the shims from the gantry and raise on the of sides of the laser tube, maybe it's not flush? Maybe the back end of tube needs to be raised? Who knows LOL




---
**Nate Caine** *July 26, 2017 19:46*

<i>I include the following as a sanity check, since you can waste a lot of time trying to do an optical alignment for an mechanically un-level xy-mechanism.</i>



(1) To start with, I'd return the machine to the original (un-shimmed) condition.  Most of what you've described can be fixed by proper adjustment of the mirrors, etc.



(2) Verify that the plane of the X-Y mechanism is parallel to the bottom of the case (since that's the plane that you work pieces will be laying on...or your work platform, etc.). 



(3) Here's how to check:  The flat plate that contains the final down-turning mirror and lens is a convenient location to check from.  Pick a corner near the front where the laser beam goes in.  Use a vertical ruler to measure the distance to the bottom of the case.  Move the optics carriage head to each of the four corners, and repeat the same measurement.  <i>(Back-Left, Back-Right, Front-Left, Front-Right.)</i>  



In a perfect world the numbers would be identical.  [In my case the numbers are 154.5mm +/- 0.25mm.]  Which is flat and parallel enough.  Your numbers will differ, as no two K40 machines are the same.  The important part is that they are very close.   Which means Flat and Parallel.



(4) If your numbers are good, your x-y mechanism is flat and level and you should go thru the laser, mirror, alignment procedure.  If you have more than about 2mm difference, between your <b>left and right</b> numbers, you should make adjustment before you do the optical alignment.   <i>(Ideally, you should be aiming for no more than 0.5mm differences.)</i> Otherwise you're trying to make precision optical adjustments to a crooked frame.



(5)  [<b>Only If Necessary!</b>]  If the numbers are off, you need to adjust for a parallel plane before you start fiddling with the mirrors.  The height of the gantry is determined on the left by the silver rod that the gantry slides on front-to-rear, i.e. the Y-axis.  However, the right side of the gantry height is determined by an idler roller which can be adjusted if it happens that the right side of the gantry is slightly high, or low.  This is a common problem as this adjustment can get knocked out of alignment. 



(6)  The idler adjustment, if needed, is at the far right of the x-axis.  See photo.  On some machines the adjustment can be accessed thru the "porthole" on the divider panel that separates the laser work compartment from the electronics compartment.   (<b>Ignore most the screws</b>:  <i>The top two screws attach the end plate to the end of the gantry extrusion.  The next two screws adjust the tension of the x-axis belt.  The bottom two screws attach the plate to the y-axis drive belt.  _</i><b>Don't touch any of them.</b>_)    



(7) In the center is a screw with some washers and a vertical slot.  Inside it is attached to an idler roller.  Moving the screw high of low in the slot will adjust the hight of the right hand of the gantry to match the height of the left hand (previous mentioned as fixed by the silver slide rod). 



(8) Once you've made the adjustment, go back and measure the heights again at each of the four corners.   (described in step 3, above).



(9) If it turns out that the height numbers are off in the <b>front-to-rear</b> direction, this means that the silver guide rod is off.  That's an entirely different problem, and rarely an issue.



















![images/707c5ac399085523d8fdaf823f0fdf92.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/707c5ac399085523d8fdaf823f0fdf92.jpeg)


---
**Nate Caine** *July 26, 2017 19:47*

Second photo showing the inside idler roller and the screw slot adjustment.

![images/af14f4802ca6fcd0db924524d0fad31c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/af14f4802ca6fcd0db924524d0fad31c.jpeg)


---
**Nate Caine** *July 26, 2017 20:13*

**+Don Kleinschnitz** If the tube is not parallel, it doesn't matter.  Things gets squared up at the first mirror.


---
**Tammy Mink** *July 27, 2017 00:48*

I will definitely have to check all this thanks all for taking time trying to help me...as right now I've moved about everything in the laser path and tinkered with the tube, the gantry etc and I never can get both straight. I don't think I lost too much since it came so badly aligned it was barely hitting the laser head if at all. Guess time to measure and align and cross my fingers in hopes I get it right eventually


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/Ezuc4iSycGZ) &mdash; content and formatting may not be reliable*
