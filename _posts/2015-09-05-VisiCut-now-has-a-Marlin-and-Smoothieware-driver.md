---
layout: post
title: "VisiCut now has a Marlin and Smoothieware driver"
date: September 05, 2015 21:04
category: "Software"
author: "quillford"
---
VisiCut now has a Marlin and Smoothieware driver.





**"quillford"**

---
---
**Stephane Buisson** *September 07, 2015 06:54*

It's so nice to see the good progress there ;-))


---
**Kirk Yarina** *September 09, 2015 12:16*

Has anybody reverse engineered the Moshi or M2nano protocol?


---
**Stephane Buisson** *September 09, 2015 13:01*

+Kirk Yarina No reverse engineering proprietary hardware, and go with fully open source have a lot of advantages, in no time your softwares workflow will be well ahead of Moshi/Laserdraw. it will cost you only the K40 modification payed by far less material wastes.

I would rather go for a Smoothieboard than a cheaper compatible board for it's ethernet network connexion.


---
*Imported from [Google+](https://plus.google.com/115654175984463400645/posts/22CN2qsxRWS) &mdash; content and formatting may not be reliable*
