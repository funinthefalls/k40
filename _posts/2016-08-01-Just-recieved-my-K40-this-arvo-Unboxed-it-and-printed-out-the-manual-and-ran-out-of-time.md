---
layout: post
title: "Just recieved my K40 this arvo. Unboxed it and printed out the manual and ran out of time"
date: August 01, 2016 10:44
category: "Discussion"
author: "1lupus"
---
Just recieved my K40 this arvo. Unboxed it and printed out the manual and ran out of time. After reading the manual I am astounded at how little info is given. Maybe I am looking at the wrong file? My initial thoughts are that I have been given a software key/dongle I am assuming that that must be plugged into a USB port on whatever computer I am running the software on? There is also a USB cable supplied one end is a standard USB A type and I see there is a place to plug that into the laser cutter, but the other end is a type B and I dont have anywhere to plug that into on my PC? I am sure that it is simple, but why didn't they give me a printed setup guide to answer these basic questions? Is there any resource online that anyone knows of?





**"1lupus"**

---
---
**I Laser** *August 01, 2016 11:33*

I nearly fell off my chair when I read you got a manual!



Yes dongle must be plugged in, the Chinese apparently don't like unauthorised people using the coreldraw plugin they created, yet they supply an illegal copy of corel with every purchase... Go figure! :|



USB should be standard? One side into the machine the other side to your PC.



As far as I'm aware this is <b>the</b> online k40 resource...


---
**1lupus** *August 01, 2016 11:48*

**+I Laser**  

Thanks for the reply I Laser. The USB cable supplied has a type A (Bog Standard rectangle shape) on one end and a type B on the other. The problem is the Laser cutter has a standard type A port. This means that I can plug the cable into the laser cutter but have no way of plugging the cable into the computer. . Ever other peripheral I have ever installed has a type B on the device/Printer/Scanner side and a standard on the computer end. 

whatever But this devil is type A?﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 11:56*

The USB cable I received is a standard printer cable. Got the small square with 2 angled corners that plugs into the laser cutter, then the other end is standard USB to plug into the pc.



There is basically a really crap "manual" that came with the K40. Super basic & makes not much sense. Any questions you have on the setup are best to be asked here (or the equivalent facebook group) as you will get answers from one of us who have all been there & done that before.


---
**HalfNormal** *August 01, 2016 15:12*

**+1lupus** Sounds like you have a a Moshi board in your system. You can purchase the correct cable or make one if you have a couple of spare cables lying around. All you need to do is connect the like colored wires together. I had to do this with the used system I purchased. 


---
**I Laser** *August 01, 2016 22:48*

Moshi? NOOOOOOOOOOOOOOOOOOO!!!


---
**1lupus** *August 02, 2016 02:33*

**+HalfNormal** Thanks for the reply. I am a new customer is the Moshi Board a step up or a step down? Should I be happy or sad? I will tell the seller to provide the correct cable.


---
**I Laser** *August 02, 2016 04:15*

I haven't used Moshi personally, but this forum is littered with posts from those that have and if it's half as bad as others make out you're going to probably want to replace the board in the near future. 



Corellaser is the preferred choice for those wanting to stay stock. Doesn't matter much if you plan going opensource.


---
**HalfNormal** *August 02, 2016 04:25*

**+1lupus** There are two boards used with the lasers, the Moshie and the M2Nano. Having used both, the M2Nano and associated software seems to be easier to use. There are some replacement boards available when you are ready to upgrade. 


---
**Robi Akerley-McKee** *August 08, 2016 06:13*

My Moshidraw board lasted 3 days before I started reseaching on how to replace it. On first machine I started with a grbl board, replaced that with a arduino 2560 and RAMPS 1.4 board, and on the second K40 I went straight to a MKS board.  1st machine is getting a new tube and looking at a arduino Due and RAMPS-FD board, though I'm still looking for firmware for that.  It may get a MKS as well.  I'm using a American DJ relay board 9 outlets, 8 of which are switched with 12v relays.  Laser K40 goes into plug 0 (always on) and 1 through 4 are 1 water pump, 2 exhaust, 3 air assist, and 4 is spare at the moment. 5 through 8 aren't used right now.  M80 turns on 1 and 2, M106/M107 turn on/off air assist on 3. 4 is wired to the MKS/RAMPS but not used.


---
*Imported from [Google+](https://plus.google.com/117731748014602155460/posts/iAmRdte2dod) &mdash; content and formatting may not be reliable*
