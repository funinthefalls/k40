---
layout: post
title: "How do I bypass a bad optical end stop (Y axis)and use a mechanical with the M2 nano V9?"
date: December 08, 2017 20:09
category: "Original software and hardware issues"
author: "Martin SL"
---
How do I bypass a bad optical end stop (Y axis)and use a mechanical with the M2 nano V9?

I have everything else figured out and working except how to tell the board to ignore the optical.

Remove the sensor from the card? snip a wire in the ribbon? move a jumper/bridge?





**"Martin SL"**

---
---
**HalfNormal** *December 09, 2017 03:27*

Don has a lot of info on end stops. Start here and follow the links.

[donsthings.blogspot.com - K40-S Middleman Board Interconnect](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html)


---
**Don Kleinschnitz Jr.** *December 09, 2017 13:55*

I would try and repair the end-stop as we have no access to M2 microcode and I do not think you can bypass anything from the stock driver.



[donsthings.blogspot.com - Repairing K40 Optical Endstops](http://donsthings.blogspot.com/2017/08/repairing-k40-optical-endstops.html)



I am guessing that you could replace it with a switch without having to disable anything but to my knowledge no-one has done that on a stock machine. You could use my schematics and knowledge of electronics to retrofit with a switch. 



Note: there is an end-stop tester that you can build to verify whatever you decide to do works.

I have also been known to repair these for US folks that are stuck, as a last resort :).



Let us know the direction you want to go?




---
**Martin SL** *December 09, 2017 20:34*

Thanks, I actually got it figured out not long after this post. I Had all the parts in stock to make a replacement harness, it gets rid of the ribbon cable and both optical end stops.

The replacement is 2 mechanical end stops and matching socket for the stepper motor and it plugs into the factory m2 nano v9 board. Still have to work on mounts when I get a chance.


---
*Imported from [Google+](https://plus.google.com/108406098480319659580/posts/bZ9JAfYSdE6) &mdash; content and formatting may not be reliable*
