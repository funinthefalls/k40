---
layout: post
title: "A little venting but with a happy ending!"
date: November 24, 2018 16:53
category: "Discussion"
author: "HalfNormal"
---
A little venting but with a happy ending!



Yesterday I was working on a two sided sign for a customer. When I went to turn on the chiller, it made an awful racket and stopped. Well the weather is nice and cool so not a big deal. I finished the first side and it came out great. Checking the water temp, I noticed it was a milky color. When I put my hand in the water, it was oily! My chiller had failed where the compressor motor oil was now in the cooling chamber! Removed the chiller and cleaned everything with a good dose of Dawn dishwater soap and a good rinse. Went to start the second side of the sign and noticed it was loosing power. As soon as I got close to the power supply, I could hear it arcing then silence. This is a new power supply just over 90 days old. I just knew the eBay seller would not honor a return. I sent them an email which was replied to in a few hours with the offer of a replacement if I payed $15.00 shipping fee! OMG! Better than $120.00 for a new one. (which I had already ordered to get up and running to finish the job.)

So a quick shout out to orionmotortech eBay seller.



So, I know you are wondering if the oil could have caused the power supply failure. I do not believe so. A quick glance points to the coil which is a big point of failure in laser power supplys. I had a previous one fail which was sent to **+Don Kleinschnitz Jr.** for evaluation. I also know of other users of this laser who have had PS failures too. Now to research a PS that can handle my system.



Thanks for listening!





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *November 24, 2018 18:15*

Is the oil by any chance conductive???


---
**HalfNormal** *November 24, 2018 18:20*

**+Don Kleinschnitz Jr.** Did not do a test due to trying and get things running again. Even if it was, I would expect the coil to be the failure point.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/LsZz4StKPzR) &mdash; content and formatting may not be reliable*
