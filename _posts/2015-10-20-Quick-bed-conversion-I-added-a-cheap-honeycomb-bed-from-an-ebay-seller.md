---
layout: post
title: "Quick bed conversion..... I added a cheap honeycomb bed from an ebay seller"
date: October 20, 2015 17:44
category: "Modification"
author: "Phil Willis"
---
Quick bed conversion..... I added a cheap honeycomb bed from an ebay seller. I will replace the whole thing at some point with a Z adjustable bed but this will do for the moment. I also intend to keep a close eye on the rotary bed posted earlier as that looks a fun project.

![images/1a26469673c07e3c627f89d9250a9181.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1a26469673c07e3c627f89d9250a9181.jpeg)



**"Phil Willis"**

---
---
**David Cook** *October 20, 2015 18:12*

Looks decent!


---
*Imported from [Google+](https://plus.google.com/+PhilWillisAtHome/posts/2r2GKgfGhm3) &mdash; content and formatting may not be reliable*
