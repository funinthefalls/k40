---
layout: post
title: "I need a middleman board,air assist, and honeycomb bed any good links for suppliers with a know track record of good products?"
date: May 02, 2016 02:45
category: "Discussion"
author: "Alex Krause"
---
I need a middleman board,air assist, and honeycomb bed any good links for suppliers with a know track record of good products?





**"Alex Krause"**

---
---
**Jean-Baptiste Passant** *May 02, 2016 06:18*

Middleman : [https://oshpark.com/shared_projects/3W1BpcNl](https://oshpark.com/shared_projects/3W1BpcNl)

FFC connector : [http://www.digikey.com/product-search/en?KeyWords=A100331-ND&WT.z_header=search_go](http://www.digikey.com/product-search/en?KeyWords=A100331-ND&WT.z_header=search_go)

Air assist ? (**+Stephane Buisson**  got the 15l/min and I got the 25l/min, I still haven't tested it but Stephane did) [http://www.aliexpress.com/item/LF-2208P-Electromagnetic-air-pump-Fruit-and-vegetable-detoxification-machines-ozone-generator-aquarium-fish-parts-shipping/1128865153.html?aff_platform=aaf&aff_trace_key=f12a300c085040d69eddd79304606a24-1462169824097-07574-2vnUZVbeY&sk=2vnUZVbeY%3A&cpt=1462169824097&af=cc](http://www.aliexpress.com/item/LF-2208P-Electromagnetic-air-pump-Fruit-and-vegetable-detoxification-machines-ozone-generator-aquarium-fish-parts-shipping/1128865153.html?aff_platform=aaf&aff_trace_key=f12a300c085040d69eddd79304606a24-1462169824097-07574-2vnUZVbeY&sk=2vnUZVbeY%3A&cpt=1462169824097&af=cc)

I have not yet found a good honeycomb bed, the one I got is too small and get the focus to 40.8 instead of 50.8...


---
**Mishko Mishko** *May 02, 2016 09:05*

I've bought this honeycomb, haven't tested it yet, but the price seemed OK to me. It came in an envelope, and at first I didn't know what I'm looking at, as it comes non-expanded, you have to do it yourself, see the explanation at the bottom;) Mine is 10mm thick, costs GBP 17,00



[http://www.ebay.com/itm/141965045485](http://www.ebay.com/itm/141965045485)


---
**Don Kleinschnitz Jr.** *May 02, 2016 11:37*

I bought a honeycomb off of amazon convinced I did not need a z axis adjustment. As I learned more about the focus change requirements to get good cutting I ended up replacing it with a LO lift table which I am just now fitting to the machine.


---
**Jean-Baptiste Passant** *May 02, 2016 12:45*

So far I have neither the honeycomb bed nor the automatic Z table, but I will probably set an automatic bed leveling with a bed leveling system and a BLTouch.


---
**Ariel Yahni (UniKpty)** *May 02, 2016 12:46*

I have created a collection in Gplus for many upgrades, just to keep track


---
**Jean-Baptiste Passant** *May 02, 2016 13:15*

**+Ariel Yahni** Pretty good list so far, I am myself preparing a guide similar to the Blue-Box guide with some upgrades recommendations. Would be cool if we had something big where everyone coul add upgrades.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/SVsr3j1WPoo) &mdash; content and formatting may not be reliable*
