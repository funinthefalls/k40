---
layout: post
title: "Happy Pi Day! A Pi day button made from 3mm alder with the first 100 digits of Pi on the rim"
date: March 14, 2018 05:02
category: "Object produced with laser"
author: "Ned Hill"
---
Happy Pi Day!  

A Pi day button made from 3mm alder with the first 100 digits of Pi on the rim. 

![images/5dbe9013fba0578bfdd206d1c581bd54.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5dbe9013fba0578bfdd206d1c581bd54.jpeg)



**"Ned Hill"**

---
---
**HalfNormal** *March 14, 2018 05:11*

Only to the hundredth digit?! ;-)


---
**Ned Hill** *March 14, 2018 14:04*

**+HalfNormal** It was a nice round number :)




---
**HalfNormal** *March 14, 2018 14:05*

LOL!


---
**Fabiano Ramos** *March 15, 2018 13:56*

for you too brother!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/HyBpKw98Nec) &mdash; content and formatting may not be reliable*
