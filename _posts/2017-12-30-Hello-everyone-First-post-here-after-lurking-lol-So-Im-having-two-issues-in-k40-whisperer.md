---
layout: post
title: "Hello everyone, First post here after lurking lol So I'm having two issues in k40 whisperer"
date: December 30, 2017 18:37
category: "Software"
author: "Kris Mitroff"
---
#K40Whisperer



Hello everyone,



First post here after lurking lol So I'm having two issues in k40 whisperer. First is when importing the design into the software my circles are not completely round like in inkscape and when cutting its shows those imperfections. Second is the font not importing blue (the font was done in photoshop and colored blue, that my be the issue). Any help will be appreciated! Maybe **+Scorch Works** can help me out? Thanks in advance, this is a great community!



![images/a08e4ec857834c6cf392347927eb0e88.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a08e4ec857834c6cf392347927eb0e88.png)
![images/5d6fa849386bba18bc56dc1d0ef55772.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5d6fa849386bba18bc56dc1d0ef55772.png)

**"Kris Mitroff"**

---
---
**Scorch Works** *December 30, 2017 19:18*

If you made the text in photoshop then it is a raster image not vector, K40 Whisperer does not convert raster images to vector. (however, it does convert vector data to raster in SVG files.)



Can you show a picture of the cut circle?


---
**Kris Mitroff** *December 30, 2017 19:51*

Thanks for the quick reply  **+Scorch Works**. I made sure the air assist tube had plenty of slack so it wasn't pulling the head away as well.  EDIT: I think I may have found a solution. I watched a youtube video that said the usb dongle key was not necessary to run k40 whisperer, but I plugged it in, tried the cut and now my circles come out pretty round. Still a bit confused lol

![images/46aa33e610c1b3fa16a593067d8a0953.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/46aa33e610c1b3fa16a593067d8a0953.jpeg)


---
**Scorch Works** *December 30, 2017 20:32*

**+Kris Mitroff** The circle is not the only problem.  If you look at the letters the lines are jagged.  So the issue is both with the vector cutting and raster engraving.  My best guess is that there is a mechanical problem with the x-axis.  I would offer to do a test cut using your design to be sure but my machine is broken (I am waiting for a replacement part.)


---
**Kris Mitroff** *December 30, 2017 20:36*

**+Scorch Works** what do you suggest about troubleshooting the x axis? 


---
**Scorch Works** *December 30, 2017 20:42*

**+Kris Mitroff** I don't have a lot of insight into that.  I would check the belt tension and search this groups discussions for x-axis discussions.


---
**Kris Mitroff** *December 30, 2017 21:01*

**+Scorch Works** Okay. Thank you for the quick replies and amazing job on your software! Continue the great work!


---
**Scorch Works** *January 07, 2018 00:32*

**+Kris Mitroff** did you ever get this resolved?


---
**Kris Mitroff** *January 07, 2018 03:01*

**+Scorch Works** Yup! It was quite a few things I had to tinker with the axes, focal length etc, but its cutting well now. Thanks for the info and checking up!


---
*Imported from [Google+](https://plus.google.com/118071484912279813107/posts/7oSkYR2kn9C) &mdash; content and formatting may not be reliable*
