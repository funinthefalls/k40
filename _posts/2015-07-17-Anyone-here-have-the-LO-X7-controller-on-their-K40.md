---
layout: post
title: "Anyone here have the LO-X7 controller on their K40?"
date: July 17, 2015 02:38
category: "Modification"
author: "Steve Moraski"
---
Anyone here have the LO-X7 controller on their K40?  If so has anyone configured output functions such as air assist?  I have been searching all over google and lightobject and the farthest I got is that you can but I am unable to find out how to configure it.





**"Steve Moraski"**

---
---
**Stephane Buisson** *July 17, 2015 10:44*

Hi not sure, but seem to be the same product so have a look at their download section here ->

 [https://www.sinjoe.com/index.php?route=product/product&product_id=96](https://www.sinjoe.com/index.php?route=product/product&product_id=96)


---
**Steve Moraski** *July 18, 2015 16:55*

Thanks Stephane, those manuals are similar to the ones provided by LO.  I find out that Out1 is on while the job is running and that Out2 is tied to laser output. Out1 is the one to use for air assist.  Unfortunately there is no control for if you don't want it on while engraving which sucks. 


---
*Imported from [Google+](https://plus.google.com/116409795527711620932/posts/XBPh1PmwLpZ) &mdash; content and formatting may not be reliable*
