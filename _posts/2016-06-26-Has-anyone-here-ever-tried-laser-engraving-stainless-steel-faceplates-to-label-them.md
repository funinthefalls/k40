---
layout: post
title: "Has anyone here ever tried laser engraving stainless steel faceplates to label them?"
date: June 26, 2016 01:05
category: "Materials and settings"
author: "Jonathan Ransom"
---
Has anyone here ever tried laser engraving stainless steel faceplates to label them?





**"Jonathan Ransom"**

---
---
**Anthony Bolgar** *June 26, 2016 01:12*

The only way to do it is to use a spray like Cermark (Dry moly lube). You can not engrave metal with a 40W CO2 laser. The laser will cause the cermark to bond to the metal, that is how it marks it.


---
**Jonathan Ransom** *June 26, 2016 01:14*

Thanks for the info **+Anthony Bolgar**​. Do you know if the moly lube will clean off easily without damaging the look of the stainless steel?


---
**Ariel Yahni (UniKpty)** *June 26, 2016 01:19*

**+Jonathan Ransom**​ yes it will


---
**Anthony Bolgar** *June 26, 2016 01:20*

Yup, lots of people do it all the time.


---
**Jonathan Ransom** *June 26, 2016 01:22*

Thanks guys. Wanting to label cat6 faceplates to make them look nicer than slapping on sticker labels. I don't have a laser yet. This may be the reason i needed to justify getting one. :-)


---
**Jim Hatch** *June 26, 2016 02:15*

You'll want DRY Moly lube - not the wet kind. The wet will never dry enough to fuse well. Also, the Dry Moly Lube (CRC is the brand I got from Amazon) is about 10% the cost of the comparable Cermark/Thermark spray ($11 vs $100).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 03:15*

Yeah, I second "not the wet kind" as I purchased some wet moly & it barely marked the surface (as the air-assist kept blowing it around). I used an industrial Dry Moly lube for tests so far & it works wonders. Cleans off very easily (just wipe with cloth/water). Just make sure whatever you get has MoS2 (Molybdenum Disulfide) present in the mixture.


---
*Imported from [Google+](https://plus.google.com/+JonathanRansom/posts/cwoZEVuHZxu) &mdash; content and formatting may not be reliable*
