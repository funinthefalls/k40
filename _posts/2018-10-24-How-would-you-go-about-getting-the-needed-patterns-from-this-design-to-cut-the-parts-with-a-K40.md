---
layout: post
title: "How would you go about getting the needed patterns from this design to cut the parts with a K40?"
date: October 24, 2018 03:09
category: "Discussion"
author: "timb12957"
---
How would you go about getting the needed patterns from this design to cut the parts with a K40? 





**"timb12957"**

---
---
**Don Kleinschnitz Jr.** *October 24, 2018 12:34*

#K40Projects


---
**timb12957** *October 27, 2018 02:10*

Wow, this must be impossible, because by now I would have expected some one to have posted something! lol


---
**Don Kleinschnitz Jr.** *October 27, 2018 15:52*

I dont think its impossible at all. It would be a fun project?

I am not sure what you are asking, the site appeared to have all the designs needed?

I would download the source files compatible with your design tools, import them into Lightburn and let it cut??


---
**timb12957** *October 27, 2018 19:14*

None of the source files are in a format I am familiar with. The only design tools I have access to is Draftsight, CorelDraw, Inkscape,  and I do have access to Autocad Fusion 360 but almost zero knowledge of how to use it. Maybe the project is just over my head.


---
**Don Kleinschnitz Jr.** *October 28, 2018 11:43*

**+timb12957** do any of your tools import dwf files? It also has a sketchup file and that tool is free.


---
**timb12957** *October 28, 2018 23:10*

No none of my tools use dwf files. I will look in to sketchup. Thanks


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/jSjkcwnmRkZ) &mdash; content and formatting may not be reliable*
