---
layout: post
title: "Made my first test printing plate today!"
date: December 05, 2016 03:31
category: "Object produced with laser"
author: "Ulf Stahmer"
---
Made my first test printing plate today! Engraved a Christmas scene onto some MDF, locked it up in the press, inked it up and printed it! Worked like a charm! Now I want to try some other materials to see how they print.



![images/ae800629d8aad1a28964e0c24aa84e11.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae800629d8aad1a28964e0c24aa84e11.jpeg)
![images/6093387229a6bc1f734014c4c642b142.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6093387229a6bc1f734014c4c642b142.jpeg)
![images/6275a1473b8225643c58f772b8ba96fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6275a1473b8225643c58f772b8ba96fb.jpeg)
![images/e9857c5111ae8a131e0a99f3441f951b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9857c5111ae8a131e0a99f3441f951b.jpeg)

**"Ulf Stahmer"**

---
---
**Ulf Stahmer** *December 05, 2016 03:43*

No. The ink should seal it fairly well. The texture of the MDF does show in the print. I'd like to try some acrylic (smooth surface). But I was happy about the detail. I ran two cuts on the plate: the engrave and the a vector to clean up the edges. It worked as expected, but the vector cut was sized slightly too large, thus I had some artifacts (misalignment between the two cuts), but it also showed me the detail I can get. I also should have engraved slightly deeper as I am getting some of the texture in the engraved portion to print. In this case, I sort of like it. It looks like snow! Now I want to try some greyscale to see how that prints!


---
**Richard Vowles** *December 05, 2016 18:02*

That's a serious press!


---
**Ulf Stahmer** *December 05, 2016 22:29*

**+Richard Vowles** It's a Vandercook SP15 [http://www.vandercookpress.info/years54-09.html#sp15](http://www.vandercookpress.info/years54-09.html#sp15)  built in 1963.


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/SqTeapSEjgN) &mdash; content and formatting may not be reliable*
