---
layout: post
title: "Hello I have problem with LaserDRW running on Win 8.1 - when I want cut/engrave something small everything is ok, but now I created graphic with width 327mm and height 200mm and I have 2 problems when I want to cut this"
date: November 20, 2016 16:12
category: "Discussion"
author: "MobileEffect"
---
Hello I have problem with LaserDRW running on Win 8.1 - when I want cut/engrave something small everything is ok, but now I created graphic with width 327mm and height 200mm and I have 2 problems when I want to cut this graphic - first it takes forever when I click to "starting" in LaserDRW and PC load graphic into laser and really start to work, it usually stuck on 84% - is it normal? When I try to resize graphic in smaller dimension, its start do job much more quicker.



And second problem is centering graphic in program - I cannot achieve to center graphic to my wood plank in laser. I have set laser out Layout like on photo down below and I need to move whole graphic to the left, but in program I cannot, when I start to cut the graphic is whole moved to right and when the laser go on left it crash the X axis and motor start to skip steps... :( How can I add some correction to work space? 

![images/ca28b473a823f2aa701c7315f80ee439.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ca28b473a823f2aa701c7315f80ee439.png)



**"MobileEffect"**

---


---
*Imported from [Google+](https://plus.google.com/109578446792362388576/posts/e7Lg37Y3qzn) &mdash; content and formatting may not be reliable*
