---
layout: post
title: "Hey Ray Kholodovsky , does either the Cohesion3D or a Smoothieware board have the option to add things like flow sensor or water temperature sensor?"
date: December 01, 2016 07:02
category: "Smoothieboard Modification"
author: "Ashley M. Kirchner [Norym]"
---
Hey **+Ray Kholodovsky**, does either the Cohesion3D or a Smoothieware board have the option to add things like flow sensor or water temperature sensor? And can either be programmed to use those additional components or other stuff, like controlling a digital air valve on some pins? (all I would need is a HIGH/LOW signal, the valve won't be powered by those pins)





**"Ashley M. Kirchner [Norym]"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 07:05*

So you have any particular sensors in mind you can show me?  Depending on what the input is (if it's a simple high/ low) then we can write some switches in config to toggle the mosfets or other gpio pins. 


---
**Alex Krause** *December 01, 2016 07:09*

Personally my idea of interlocks is they should be hard wired and not software controlled


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 07:11*

Oh I didn't read that like it was interlocks, just some simple if this then that. 

For interlocks we have some stuff in mind. **+Don Kleinschnitz** this is your territory :)


---
**Jonathan Davis (Leo Lion)** *December 01, 2016 07:12*

**+Ray Kholodovsky** wouldn't making a extension board that could sit on top be more effective? Like raspberry pi does with its different "hat" extensions or even the ability to layer them one on top of the other. 


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 07:13*

Not quite a hat, but a compatible board that handles the interlocks and reports back to Smoothie. That's what we're discussing. 


---
**Jonathan Davis (Leo Lion)** *December 01, 2016 07:16*

**+Ray Kholodovsky** that to me would seem like a considerable option, or even having a version of the board that could utilize the processing power of the computer it's connected to 


---
**Alex Krause** *December 01, 2016 07:17*

It would be nice to be able to monitor the error via a glcd or console error but the function itself should de-energize the ability to fire completely 


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 07:19*

**+Alex Krause** yepp, that's what "reports back to Smoothie" would entail. We're still trying to figure out <b>a</b> way to do it. 


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 07:22*

So right now, what I have is a secondary controller (an Arduino MEGA) that has a color LCD on it. It reads the flow meter (hall effect) as well as two digital thermometers and displays that info on the screen. That's code I wrote myself. It does NOT interface with the laser control board and this is the part I want to change as I want it to be a single controller than can do that as it would allow for proper warnings and cutoffs. (Right now the MEGA will emit a screeching noise if something's wrong which will get my attention, but it won't actually stop the laser.)



Thinking bigger, I'd like to be able to turn on the air FLOW when the laser is actually working, and turn it off when it's sitting idle. And thinking even bigger, I want to control two digital air valves (one high and another low pressure.) But in order to do that, I need to be able to control whatever pins are available for the end-user to play with.



Ultimately if the firmware is closed, this is a moot point.


---
**Alex Krause** *December 01, 2016 07:23*

**+Ray Kholodovsky**​ the next time I have a few days off from work I will PM you some ideas I have for that 


---
**Anthony Bolgar** *December 01, 2016 07:23*

Jon Bruno and I are reworking the LaserSafety System I have on github to do just what you said Alex. Should be out in January sometime. All Interlocks will be of the mechanical deenergizing type, with visual and auidble feedback via an LCD display, LED lights and a buzzer.


---
**Jonathan Davis (Leo Lion)** *December 01, 2016 07:25*

Okay? Because when you think about a computer already has something of a 32bit processor and with the fact that it can handle multiple programs means that like say running a series of RGB lights via the USB port while the computer would handle the processing of changing the colors of the lights via a software program. So to me it seems like it could be done. 


---
**Jonathan Davis (Leo Lion)** *December 01, 2016 07:26*

**+Anthony Bolgar** interesting idea but of course please don't forget us Banggood gearbest laser engraving machine owners as well 


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 07:26*

**+Ashley M. Kirchner** 

Here's the big picture points:

The Cohesion3D boards all run smoothie. Which is open source. And highly configurable just thru the config.txt file so we could totally do things like a switch module to turn on the air assist pump from a start and end gcode.  And possibly tie the output of your mega as a "pause" pin to smoothie. 



The mini has Mosfets that are not used in the stock laser configuration so you can use one to run a 12v DC pump like I do or a relay/ ssr for something larger. 


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 07:31*

I prefer keeping things isolated, so it'll likely be driving relays or other custom circuitry to trigger larger loads. But from what I'm understanding, in my case, there's probably no way to get rid of the MEGA and have the Smoothie or Cohesion run everything, including the color touch screen. That would require compiling in the screen's library and writing code for it.


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 07:35*

**+Ashley M. Kirchner** I guess so, everyone is using the RRD GLCD with my boards, that's one of several panels that Smoothie supports. No touch screens are in the list yet. 


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 07:38*

Aww mang ... I like my color touch screen. :)  Ok, no big loss, I can simply continue to use the MEGA as is and maybe do what you suggested, and tie it's ALARM pin to the Cohesion/Smoothieware ... This is still progress.


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 07:39*

Cool. And as with all open source software, pull requests welcome :)


---
**Stephane Buisson** *December 01, 2016 07:40*

following you all with attention ;-))


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 07:41*

Cohesion3d.com when you are ready. I will look forward to your orders. [cohesion3d.com - Cohesion3D: Powerful Motion Control](http://Cohesion3d.com)


---
**Jonathan Davis (Leo Lion)** *December 01, 2016 07:41*

**+Ray Kholodovsky** I would wonder about the possibility of having a wireless display like from using a old tablet or cellphone and then the glcd display port having a wireless transmitter over Bluetooth or something. 


---
**Anthony Bolgar** *December 01, 2016 08:04*

**+Jonathan Davis** It will also work with non CO2 lasers, just not all the functions like water temp or door interlocks, those can be hardwired closed.


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 08:15*

Huh, can't checkout from Cohesion website ... keeps telling me an invalid order was found. Tried it twice now.


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 08:17*

**+Ashley M. Kirchner** Debugging now.  Message me on hangouts please.




---
**Ashley M. Kirchner [Norym]** *December 01, 2016 08:18*

Used a different browser ... success. Guess I had to force my money down the pipe three times for it to take. :)


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 08:19*

I see the order.  Awesome!  Can you still contact me privately please?


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 08:28*

Assuming HO is working ...


---
**Stephane Buisson** *December 01, 2016 08:38*

You got my order too, no issue placing it ;-))


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 08:43*

I see :) Thanks!


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 08:45*

Now help spread the word please :)


---
**Stephane Buisson** *December 01, 2016 08:48*

**+Ray Kholodovsky** this community is the best support I could offer to you (spread the wor(L)d)


---
**Don Kleinschnitz Jr.** *December 01, 2016 16:49*

**+Ray Kholodovsky**  and I have been talking about an auxiliary multipurpose board that handles all the interlocks and machine level errors. 

Excuse this long post ....



I wanted to make the installation of interlocks easier as I think there are WAY to many people running without them. There isn't much excuse cause they are the easiest electronics hack one can do to a K40. Therefore I wanted it to work with the stock K40 also so this could be installed without a controller conversion.



Here is what I have implemented so far in my machine. Note: the mounting of the middleman on the interlock breakout turned out to be a nuisance and I will remove it.



[http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html](http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html)

----------------------------

Our thinking goes something like this:

...A stand alone auxiliary board that collects all the interlocks and any device that needs to inhibit the laser firing into an easy to wire central point. Uses screw terminals so that no connectors are needed. Comes as a kit with the micro-switches and mounting bracket designs which are open source laser cut acrylic.

... LEDs on the edge of the board that tell you what interlock is open. [this could be a display or a remote LED panel]. I like the remote led panel idea so it can be mounted in a vertical manner next to other displays.

... An alert pin that goes to the Smoothie and is processed as a switch. It activates if any machine error exists.

... An optional 3 bit alert error code (we really did not want to make the use of 3 smoothie input pins mandatory though, and this would require firmware changes)

... plugs in between the laser power supply and the stock or smoothie controller so it is powered by the 5VDC of the LPS. Compatible with stock K40 LPS connectors or screw terminals.

... inexpensive; target $25 max $50.

...  options: 

...... fuses and indicators on LPS DC voltages

...... PPI circuitry [if a simple approach works]



I have seen some processor based controllers that can/do similar things so i do not know how much of a duplication this may be. I am not really motivated to duplicate anything.



We can interface with the controller to read and in turn control the response but that would require firmware and testing to insure that has the bandwidth to process these along with all the other stuff it does. It also would mean that you would have to change the firmware in every configuration of controller boards firmware (non smoothie)  it is used with. I concluded the system should be standalone and have the ability to report and aggregate error signal to the controller.



Design wise the interlock system cannot be solely controlled by a processor. For safety reasons it must be a hard wired circuit. It can however be monitored by a processor for reporting and display.



I am going to split this post and discuss the AC control system next.


---
**Don Kleinschnitz Jr.** *December 01, 2016 17:19*

I also concluded that the control of AC accessories should be a separate subsystem. 

[donsthings.blogspot.com - K40-S AC Power Systems Design](http://donsthings.blogspot.com/2016/11/k40-s-ac-power-systems-design.html) ..... outlines the design I am using. 



I wanted AC devices to be controllable from the controller and also  from a set of switches on a panel. Initially I had everything plugged into a power strip that turned everything on/off.



I planned to control all AC devices from SSR's but found out that some AC devices cannot be controlled with a SSR, I still do not know why. My blower is one that will not work with an SSR so I decided to use relays.



In the current design I have used an inexpensive relay control board that is controlled by panel switches and can also be connected via open drain to the controller if processor control is warranted. 



I plan to eventually turn on/off the vacuum and air assist with the smoothie using a couple of open drain outputs and assigned G codes. 



For now the air assist and led finder are turned on from the panel because I am not sure I want them on for every job. The water pump always turns on with the main switch and is not controllable. However I am considering going to a DC powered external pump as I dont like an AC cord in a water bucket and I also want to control the flow rate.



My experience with the current design led me to believe that a standalone AC control PCB breakout might be appreciated because sooner or later the control of external AC devices will raise its head on a K40 users list of hacks. Wiring all these AC plugs is annoying and doing it wrong can be dangerous. 



So I had imagined an AC PCB that contains the plugs, the relays and an interface connector. A second PCB that  contains the AC control panel switches.



This kind of PCB design needs to consider:

...... the mounting of the AC assy to the machine

...... circuit breakers

...... accommodate different plug configurations



"Maybe instructions on how to hack a power strip to make it controllable"



I'm Interested in what folks think?




---
**Stephane Buisson** *December 01, 2016 17:35*

**+Don Kleinschnitz** keep it simple as a K40.

the only thing I would do, is a waterflow sensor check to be able to fire ( I bought it and never done it). keep other interlock direct on the PSU.




---
**Don Kleinschnitz Jr.** *December 01, 2016 17:42*

**+Stephane Buisson** yup simple is important. My waterflow and temp sensors in series with the interlock loop independent of AC control??


---
**Wolfmanjm** *December 02, 2016 00:13*

FWIW Smoothieboards have thermistor sensors and mosfets already. And the temperatureswitch module can be used to monitor temps and switch stuff on or off accordingly. It is already there.


---
**Don Kleinschnitz Jr.** *December 02, 2016 02:11*

**+Wolfmanjm** can we also see the temp somehow in the interface and shut down the laser without a firmware change 


---
**Ashley M. Kirchner [Norym]** *December 02, 2016 02:23*

It sounds like what **+Wolfmanjm** is saying that it's possible, using the circuitry already present on the Smoothieboards, to do exactly that. We might not be able to actually get to see the temperature displayed, but the board can detect when that fault occurs and shut things down accordingly. At least, that's how I understand his comment.


---
**Don Kleinschnitz Jr.** *December 02, 2016 02:28*

**+Ashley M. Kirchner** that would be cool how do we do it? 


---
**Wolfmanjm** *December 02, 2016 04:12*

do you mean in the LCD display or in LW? in LW yes you could add a panel that queries the temp like pronterface does. For the panel it would need to be added or use the 3d printer firmware and that will display the temps.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/W18KJhdZvTz) &mdash; content and formatting may not be reliable*
