---
layout: post
title: "Public Domain Day! January 1, 2019 is (finally) Public Domain Day: Works from 1923 are open to all!"
date: January 03, 2019 13:00
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Public Domain Day!



January 1, 2019 is (finally) Public Domain Day: Works from 1923 are open to all!



Legally use works from 1923 in your projects to sell.



[https://law.duke.edu/cspd/publicdomainday/2019/](https://law.duke.edu/cspd/publicdomainday/2019/)





**"HalfNormal"**

---
---
**Ned Hill** *January 03, 2019 16:14*

Yep, I was reading about this the other day.  Mickey Mouse enters the public domain in 2024.


---
**HalfNormal** *January 03, 2019 16:34*

Not if Disney has something to do with it!


---
**Ned Hill** *January 03, 2019 17:09*

True, you would still have to deal with trademark law.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/cTvo1yxRbqd) &mdash; content and formatting may not be reliable*
