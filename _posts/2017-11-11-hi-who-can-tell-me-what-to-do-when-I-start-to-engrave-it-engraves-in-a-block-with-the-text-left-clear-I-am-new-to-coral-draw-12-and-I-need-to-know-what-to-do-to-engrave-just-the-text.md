---
layout: post
title: "hi who can tell me what to do when I start to engrave it engraves in a block with the text left clear I am new to coral draw 12 and I need to know what to do to engrave just the text"
date: November 11, 2017 13:10
category: "Original software and hardware issues"
author: "allan allan knapper"
---
hi who can tell me what to do when I start to engrave it engraves in a block with the text left clear I am new to coral draw 12 and I need to know what to do to engrave just the text





**"allan allan knapper"**

---
---
**Sebastian C** *November 11, 2017 14:57*

I think I understand your problem. You have to mark/demark the >sunken<  engraving style.

Generally: read this instructable :

[instructables.com - K40 Laser Cutter: How to Cut and Engrave in One Job](http://www.instructables.com/id/How-to-Cut-and-Engrave-Using-a-K40-Laser-Cutter/)

Step 17 will change your life!!  ;)

Look at the image of the Engraving Manager. In the Top middle section you will find the Style options with the needed checkbox.



Sunken means, the dark text will be burned, otherwise the remaining square will be sunken.




---
*Imported from [Google+](https://plus.google.com/117270776197388719277/posts/fcZBtU1zDVb) &mdash; content and formatting may not be reliable*
