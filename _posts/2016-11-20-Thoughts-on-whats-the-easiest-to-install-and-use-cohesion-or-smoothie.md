---
layout: post
title: "Thoughts on what's the easiest to install and use cohesion or smoothie?"
date: November 20, 2016 13:55
category: "Discussion"
author: "Charlie Thcakeray"
---
Thoughts on what's the easiest to install and use cohesion or smoothie? I need to upgrade my controller but I'm a rookie to all this. 





**"Charlie Thcakeray"**

---
---
**Anthony Bolgar** *November 20, 2016 14:11*

**+Ray Kholodovsky** Mini board is a plug and play setup. should be the easiest way to make the switch to Smoothie.


---
**greg greene** *November 20, 2016 14:30*

I went with Ray's board - just remember that you also need to buy the stepper motor driver modules too.  They are real cheap 5 for 10 bucks - but Ray may not have them in stock to send with the board - so you might need to get them from the net.  Ray is a great help if you run into any problems - as is Scott with his setup.  Either way - you'll be happy.


---
**Charlie Thcakeray** *November 20, 2016 14:45*

Does anyone know the price for one of **+Ray Kholodovsky** boards? (U.K.)


---
**Anthony Bolgar** *November 20, 2016 14:58*

Until tyhe new factory built boards arrive, **+Ray Kholodovsky** has been hand making them and selling the complete kit (board, cable, glcd adapter) for $100 I think.


---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2016 17:01*

Thanks guys. 

**+Charlie Thcakeray** we're going to be opening for pre orders this week for the mini board. The larger board the remix will be in stock and up for sale this week too. 


---
**Kelly S** *November 20, 2016 19:03*

Also going to say go with **+Ray Kholodovsky** and I don't even have my boards yet. Haha, but plug and play convince can never be beat.


---
**Charlie Thcakeray** *November 21, 2016 09:12*

**+Ray Kholodovsky** I'm interested in your boards, what sort of software are they capable of running? 


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 02:06*

**+Charlie Thcakeray** and anyone else interested, the Cohesion3D Mini (and Laser Upgrade Bundle you'd need) are now available for pre-order at [cohesion3d.com](http://cohesion3d.com)  

We also have the ReMix board for sale if you have a higher power application in mind.


---
*Imported from [Google+](https://plus.google.com/109895480370746543865/posts/A2gD8PXg7Vr) &mdash; content and formatting may not be reliable*
