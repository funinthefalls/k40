---
layout: post
title: "Anyone have a template for a plate stand or something like that?"
date: November 02, 2016 08:56
category: "Object produced with laser"
author: "Bob Damato"
---
Anyone have a template for a plate stand or something like that? Im looking for a little stand to hold the 4x6" granite engraving I did (see previous post). I can cut it out of wood or plexi or something like that.  Thanks!

bob





**"Bob Damato"**

---
---
**Anthony Bolgar** *November 02, 2016 09:06*

Try searching google for tablet stands, they work just as good as plate stands.


---
**Bob Damato** *November 02, 2016 09:15*

Thanks **+Anthony Bolgar**  I tried plate stands for hours and found nothing I could use so Ill try tablet...


---
**HalfNormal** *November 02, 2016 12:47*

What about this?

[http://wellcraftyme.blogspot.com/2012/02/little-plate-stand.html](http://wellcraftyme.blogspot.com/2012/02/little-plate-stand.html)

[wellcraftyme.blogspot.com - Little Plate Stand](http://wellcraftyme.blogspot.com/2012/02/little-plate-stand.html)


---
**Bob Damato** *November 02, 2016 13:17*

Thank you!


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/5nYpjeCKaGB) &mdash; content and formatting may not be reliable*
