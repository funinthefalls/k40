---
layout: post
title: "Hey everyone I am new here, me and my dad just got our K40 laser engraver in the mail"
date: February 21, 2016 14:16
category: "Hardware and Laser settings"
author: "joshua miller"
---
Hey everyone I am new here, me and my dad just got our K40 laser engraver in the mail. We want to etch glass but as much as i google i cant seem to find any specific advice on how to do it. Anyone here have any ideas? 





**"joshua miller"**

---
---
**Dave M** *February 21, 2016 17:38*

This info is from Epilog, but the approach looks like employs techniques independent of the machine used: [https://www.epiloglaser.com/assets/downloads/whitepapers/settings_whitepaper.pdf](https://www.epiloglaser.com/assets/downloads/whitepapers/settings_whitepaper.pdf).  Maybe it will be of use to you.




---
**Jim Hatch** *February 21, 2016 22:27*

**+Dave M**​ Some nice tips in there. Thanks for sharing.


---
**Joseph Midjette Sr** *February 22, 2016 02:05*

Don't google how, YOUTUBE.com how....


---
**Jim Bennell (PizzaDeluxe)** *February 22, 2016 06:14*

I've used a plain white sheet of printer paper wet/damp on top of a piece of flat glass and the results where great!



I'd imagine a wet paper towel on top of curved glass would work just fine too, I think its to help soak up a little of the heat generated when etching. 



That epilog white paper looks great ill have to read through it! Thanks for the link Dave. 


---
**Tony Sobczak** *February 22, 2016 22:23*

The white paper will help alot. 


---
*Imported from [Google+](https://plus.google.com/109031952403321787945/posts/NGEZGs7AiXW) &mdash; content and formatting may not be reliable*
