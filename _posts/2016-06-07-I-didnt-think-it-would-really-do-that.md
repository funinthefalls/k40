---
layout: post
title: "I didn't think it would really do that..."
date: June 07, 2016 20:11
category: "Materials and settings"
author: "Christoph E."
---
I didn't think it would really do that... but with some MoS2 and Cutting @5mm/ 75% power it does engrave metal.



However, that doesn't work on any material. Tried a brushed Zippo and even with the Marking-Setting @3mm/second and 75% it didn't work (although it took almost 45 minutes for a small graphic i could still wash it off easyly <s>.</s>)



But the knife is good... don't know what material it is, it just says "rostfrei" (rustfree)



![images/93036991aa6cd37c663427b0aa43789f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/93036991aa6cd37c663427b0aa43789f.jpeg)
![images/b9128e324023d85e408ddf2bf55550bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b9128e324023d85e408ddf2bf55550bd.jpeg)

**"Christoph E."**

---
---
**Ulf Stahmer** *June 07, 2016 20:14*

"rostfrei" is German for stainless steel.


---
**Christoph E.** *June 07, 2016 20:20*

**+Ulf Stahmer**

thanks for the correct translation. :-)



But that still doesn't tell what exact material it is.. I'll try some V2A this week, that may be also working.

Zippo + swiss army-knife doesn't work (except burning the grip on your swiss army knife. :( )


---
**Scott Marshall** *June 08, 2016 02:27*

Zippo is Chrome Plated. Tough and Highly reflective.

All sorts of Stainless seem to work with Moly spray, but I'm not surprised the Chrome laughs at the long wave laser.


---
*Imported from [Google+](https://plus.google.com/100193166302371572888/posts/WgDuTQ3AmcL) &mdash; content and formatting may not be reliable*
