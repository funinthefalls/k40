---
layout: post
title: "I'm new to the K40, it's my first laser"
date: February 11, 2017 04:45
category: "Original software and hardware issues"
author: "Brian Crowe"
---
I'm new to the K40, it's my first laser. I'm using Laser Draw and tonight my laser quit working for engraving. The laser works while cutting, but the laser is very weak in engrave. I'm not sure what I did wrong, any suggestions?





**"Brian Crowe"**

---
---
**Ned Hill** *February 11, 2017 14:21*

Usually the first thing to do in these cases is to check the mirror alignment.  Also what power have you been using for cut and engrave?


---
**Brian Crowe** *February 12, 2017 01:47*

Thank you, I was able to figure it out. I had put a straight line in me templet which confused the laser as I was wanting engraving. The laser won't engrave a line it thinks it should cut.


---
**Ned Hill** *February 12, 2017 01:55*

Ah yes, if the line is too thin it will not do a raster engrave.  You can engrave a line as a vector though.  You use the cut function but use less power and/or more speed.


---
*Imported from [Google+](https://plus.google.com/115665123936931789543/posts/Hj274pYzbmP) &mdash; content and formatting may not be reliable*
