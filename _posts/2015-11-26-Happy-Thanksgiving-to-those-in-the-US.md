---
layout: post
title: "Happy Thanksgiving !! (to those in the US)"
date: November 26, 2015 16:33
category: "Discussion"
author: "Coherent"
---
Happy Thanksgiving !! (to those in the US). Take your friends and family out to the shop and impress them with your laser after dinner! 





**"Coherent"**

---
---
**Gary McKinnon** *November 26, 2015 16:57*

Happy thanksgiving US folks :)


---
**Scott Thorne** *November 26, 2015 17:12*

Happy Thanksgiving to you and your family too man.....and to everyone that is a member of this community....I've learned so much from you guys....Thanks for everything!


---
**Gary McKinnon** *November 26, 2015 17:19*

What sort of stuff will you be cutting Scott ?


---
**Scott Thorne** *November 26, 2015 17:23*

I usually cut 1/8 to 3/16 birch plywood and acrylic.....at a power of 6mA and a speed of 8mm/s......I just started a nice piece I'm going to post it.


---
**Gary McKinnon** *November 26, 2015 17:27*

That's quite low power, do you have to go so low because of the possibility of burning ?


---
**Scott Thorne** *November 26, 2015 18:59*

I cut at 8mm/s....on everything I cut.


---
**I Laser** *November 26, 2015 22:10*

Power not speed ;)



I'm impressed too, I'm cutting at 6.25mm @ 8mA.


---
*Imported from [Google+](https://plus.google.com/112770606372719578944/posts/DLRxHzWbTva) &mdash; content and formatting may not be reliable*
