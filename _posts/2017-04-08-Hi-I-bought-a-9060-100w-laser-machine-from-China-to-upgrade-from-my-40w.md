---
layout: post
title: "Hi, I bought a 9060 100w laser machine from China to upgrade from my 40w"
date: April 08, 2017 18:12
category: "Modification"
author: "Gerard Mullan"
---
Hi,



I bought a 9060 100w laser machine from China to upgrade from my 40w. It has a Ruida controller inside, and uses RDWorks software.



Is this a good board or would I be best changing to something else?



Thanks﻿





**"Gerard Mullan"**

---
---
**Phillip Conroy** *April 08, 2017 20:18*

 You can get a corel draw plug in ,as well as autocad plug in, I have same controller, it can handle etching and cutting by setting layers and colors, the head zooms from one cut to the next at a very good speed,cutting so far much more smooth than k40 at higher speeds ,ie 2.7mm plywood at 18mm/second and 80% power which is 20ma on my machine.,

I have already hooked up a amp meter as I do not like only knowing power as a %.mine has been set so that 100% power is 24.5 ma.

The first thing I have noticed compared to the k40 is the laser sees and cuts everyone ,ie with k40 2 black filled objects overlapping the k40 would see it as one object and cut around outside ,the new controller sees the line in between the object s and cuts it,you just need to weld the 2 objects into one .


---
*Imported from [Google+](https://plus.google.com/117648854578727329148/posts/4r2hhR4Fvfn) &mdash; content and formatting may not be reliable*
