---
layout: post
title: "Hello everyone. I just got this laser of Amazon"
date: July 22, 2016 04:04
category: "Discussion"
author: "Peter Lewkowicz"
---
Hello everyone. I just got this laser of Amazon. Great unit. After using it for awhile, it basically "shoot" itself apart. The mirror were not screwed down all the way and got completely misaligned. 



When I first ran the Laser out of the box, I was able to cut 3/16 acrylic at about 20mA, speed of 2mm/s and 2 PASSES.



Now after realigning the mirrors, I cannot cut the same acrylic until wbout 20th pass!!!



Can someone post their software settings

for different materials and the Amperage used?



Thanks in advance.





**"Peter Lewkowicz"**

---
---
**Paul de Groot** *July 22, 2016 05:11*

Probably you need to clean the lens in the cutter head. It gets soot and deposits on it after cutting.  Most users install an air assist to keep it clean. Also check the height of the item you need to cut. Too high or too low won't cut either. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 22, 2016 06:29*

20mA & 2mm/s & 2 passes seems a bit excessive for cutting ~4mm thick acrylic. I haven't cut any myself, but from reading here I am fairly certain others cut it much easier than that. So it seems that to begin with you were not operating optimally. Hopefully others can give their specific settings to help you out.



A few things I'd suggest though is to realign the mirrors as precisely as possible (making sure all 4 corners of the cutting area hit at same point). Then, check your lens orientation (bump/convex upwards). Then make sure that when cutting your material is centred at 50.8mm.


---
**Ariel Yahni (UniKpty)** *July 22, 2016 11:22*

I could say also that the beam could be hitting the cone on the inside. Try rotating it. 4mm acrylic should cut at 8mms with 8mA in to passes


---
**Mircea Russu** *July 22, 2016 14:53*

5mm transparent acrylic 12ma 9mm/s. Maybe can go lower power or higher speed but I like the flame polished edges. 


---
**Peter Lewkowicz** *July 23, 2016 02:26*

Thanks to all for quick reply. I'll test some different settings this weekend. 



Is there a way to just do lets say 5 passes, or TASKS in a row without the confirming box popping up after every task??


---
**Paul de Groot** *July 23, 2016 05:34*

Unfortunately moshidraw is very unfriendly. I took me a while to get it cutting. Engraving never worked. Btw i cut acrylic 3mm with 5mA in one go (inkscape and grbl) With moshidraw it never worked that well.


---
**Peter Lewkowicz** *July 23, 2016 06:47*

It was the mirror alignment. I got it down to less than a mm away so now it's not perfect but it cuts better than before. n  

I am using the LaserDRW, and Corel for designing, and engraving. sometimes the grayscaled image only comes out as black and white.  Then I'll re-run the same task and it prints in grayscale...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 23, 2016 08:25*

**+Peter Lewkowicz** Assuming you are using CorelDraw/CorelLaser plugin, there is a way to avoid the popping up confirmation between tasks, but it is slightly annoying. To do so, you need to Add Task, with the "Starting" checkbox unchecked. You also need to have "All Layers" selected in the Method dropdown. Basically continue adding the same task 5 times, to be able to run it 5 times. Then, click the Start/Play button on the toolbar (or using the icon in System Tray near the clock, right click & Start). It should tell you how many tasks are waiting before you click Start. I found that to be the only way to do so without the stupid confirmation popup, although it's more troublesome (in my opinion) than just pressing Enter at the end of each task.


---
*Imported from [Google+](https://plus.google.com/114623084926134511521/posts/AhvM6eZdFxK) &mdash; content and formatting may not be reliable*
