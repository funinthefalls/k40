---
layout: post
title: "I have a question about cleaning the mirrors on my K40"
date: July 06, 2017 17:16
category: "Discussion"
author: "bbowley2"
---
I have a question about cleaning the mirrors on my K40.  Can I use any optic solution?





**"bbowley2"**

---
---
**Nate Caine** *July 06, 2017 17:30*

Here's a good link from Epilog Laser (maker of high end laser engravers/cutters).  



They actually mention "Everclear" alcohol by name as a preferred cleaner.  So now you have an excuse.



[support.epiloglaser.com - Cleaning Your Optics](http://support.epiloglaser.com/article/8205/11940/cleaning-epilog-optics)






---
**Fernando Bueno** *July 06, 2017 17:45*

Isopropyl alcohol is the best and cheapest you can use, applying it with sticks to the ears


---
**Steve Clark** *July 06, 2017 18:10*

I also use Isopropyl  alcohol and blow off with clean air...lightly wash and dry with the Q-tips  


---
**Don Kleinschnitz Jr.** *July 06, 2017 18:44*

#K40cleaningingoptics


---
**bbowley2** *July 06, 2017 22:43*

Wow, Everclear.  Thanks.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/C9Ve6FWpCcC) &mdash; content and formatting may not be reliable*
