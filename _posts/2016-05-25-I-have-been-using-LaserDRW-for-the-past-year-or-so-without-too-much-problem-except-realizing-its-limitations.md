---
layout: post
title: "I have been using LaserDRW for the past year or so, without too much problem except realizing its limitations"
date: May 25, 2016 05:52
category: "Original software and hardware issues"
author: "Ian Hayden"
---
I have been using LaserDRW for the past year or so, without too much problem except realizing its limitations.  Then i noticed in the CD that came with the K40 included CorelDRAW X4.   Installed it to give it a go, with the included CorelLASER.  Surprisingly it worked first go, though one has to launch it from CorelLASER rather than the corelDRAW app itself. 



 Two issues predominate:  the menu and dialog boxes are  in Chinese.  Second the menu bar is not visible, though hovering over the right area does display it.  I am running Windows 10, chinese language added/enabled



I tried to custom install to see if there was a language option, but alas no option. Also looked to see if there is a Tools/Options/settings choice, but they dont seem to be in the normal place. Has anyone succeeded to convert to English ? 









**"Ian Hayden"**

---
---
**Anthony Bolgar** *May 25, 2016 06:34*

My disk came with a pirated version of CorelDraw X12, haven't heard of anyone getting X14 until you mentioned it. If you want I can upload a copy of my install disk to a google drive for you to grab, it installs CorelLaser and CorelDraw in english :)


---
**Tony Schelts** *May 25, 2016 06:36*

Heres a link to my copy that is in english

[https://onedrive.live.com/redir?resid=4C8F421A9D9DFB8D!3291&authkey=!AKbDzZJ03zkjSMA&ithint=file%2crar](https://onedrive.live.com/redir?resid=4C8F421A9D9DFB8D!3291&authkey=!AKbDzZJ03zkjSMA&ithint=file%2crar)

I got this off someone on here but for the life of me I cant remember who it was or I would give him credit,  He also has a copy of CorelDraw x5 that has worked for me also.


---
**Anthony Bolgar** *May 25, 2016 06:40*

Thanks **+Tony Schelts** , saves me from having to create the upload.


---
**Tony Schelts** *May 25, 2016 06:40*

Yuusuf  Sallahudin posted a link to 5x,

[https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XcCXxJgh3hT](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XcCXxJgh3hT)

Im sure if you ask he wouldn't mind. As it was posted perhaps you don't need to ask.



Thanks again Yuusuf


---
**Tony Schelts** *May 25, 2016 06:43*

CorelDraw and corel laser are so much better to use.


---
**Anthony Bolgar** *May 25, 2016 06:58*

And once you upgrade to a smoothieboard, it gets even easier to use with LaserWeb v2 :)


---
**Tony Schelts** *May 25, 2016 07:09*

Where are you at.


---
**Anthony Bolgar** *May 25, 2016 07:11*

**+Tony Schelts**   Whom are you asking for their location?


---
**Tony Schelts** *May 25, 2016 07:19*

Ok good question,  any of you guys in the UK


---
**Anthony Bolgar** *May 25, 2016 07:20*

Not me, I am in Niagara Falls, Ontario, Canada


---
**Tony Schelts** *May 25, 2016 07:29*

what time is it there.?


---
**Anthony Bolgar** *May 25, 2016 07:33*

3:33 A.M.


---
**Tony Schelts** *May 25, 2016 07:35*

Its 8;33 am here,


---
**Ian Hayden** *May 25, 2016 07:44*

Anthony Bolgar  - Its X4, not X14 .  Tony Schelts - Thanks, i will give it a try and let you know.  




---
**Anthony Bolgar** *May 25, 2016 07:47*

Sorry, X14 was a typo, I have a legit copy of every version of CorelDraw since version 3, I am a beta tester for Corel, I get a free copy of every edition.


---
**Tony Schelts** *May 25, 2016 07:49*

you are so lucky, send me one, :) you obviously know what your doing with it.


---
**Anthony Bolgar** *May 25, 2016 07:52*

I spent a few years teaching Corel products at Niagara College Canada, landed the teaching gig because they new I would always be up to date with new releases. Too busy these days to teach, however, I do miss it.


---
**Tony Schelts** *May 25, 2016 07:55*

Good to speak to you, got to start work now. talk again some tiime.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 09:34*

Yeah I did post a link to CorelLaser (with English) & Corel Draw x5 (non-genuine version) hosted on my G Drive. Feel free to grab it if you wish. I also added some instructions & a slightly changed .ini file for the language (as there were spelling issues driving me nuts).


---
**Ian Hayden** *May 25, 2016 09:38*

Tony Schelts - i am in japan but from UK.  I agree with you  about CorelDraw. Dont know why i stuck with Laser Drw for so long - It did the job (eventually) though not without wasting much of my time.Thank you Yuusuf  Sallahudin for the corel draw X5  download.  After i copied the manufacturer info and tried it, it came up with a different laser category, (mine is from the LIUHIU boys) Getting the original CD and reinstalling LaserDraw corrected that issue.  Anthony Bolgar - i will get around to the smoothieboard in due course.  One step at a time.  Having just blown a vacuum tube, i think that next step will be a while away!




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 09:44*

**+Ian Hayden** Mine is also a Lihuiyiuyiuyiyiuyuiy whatever it is spelled. I think the manufacturer info is not really necessary, just basically an "about" page. The important thing to remember is to set your Board ID & your Model in the Hardware settings of the CorelDraw toolbar. Also note, the toolbar will disappear randomly. I don't think it works for anyone. But there will be a system tray icon that you can access all the engrave/cut/run/stop/etc from.


---
**Ian Hayden** *May 25, 2016 10:08*

+Yuusuf Sallahuddin - thanks - hadn't noticed about the tray icon.  Corel seems to work fine on the first tests.  So far so good.  


---
**HalfNormal** *May 25, 2016 13:01*

If your unofficial copy complains about not being legal and not working, uninstall, reinstall and block the program at the firewall from phoning home.


---
*Imported from [Google+](https://plus.google.com/111645426566096956650/posts/1BmP2YNkWek) &mdash; content and formatting may not be reliable*
