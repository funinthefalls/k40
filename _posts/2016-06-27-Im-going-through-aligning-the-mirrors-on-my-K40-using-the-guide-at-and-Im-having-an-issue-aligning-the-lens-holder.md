---
layout: post
title: "I'm going through aligning the mirrors on my K40 (using the guide at ), and I'm having an issue aligning the lens holder"
date: June 27, 2016 07:52
category: "Original software and hardware issues"
author: "Jack Sivak"
---
I'm going through aligning the mirrors on my K40 (using the guide at [http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)), and I'm having an issue aligning the lens holder. The instructions state how important it is to get the beam in the center of the lens holder, but as you can see from the pictures that is not the case with my machine. The beam hits the far right edge of the aperture, but there's no space on the mounting plate to move it more to the right so that the beam is center. So I guess me questions are:



• Does the beam really need to be centered on the lens holder?

• If so, are there any easy modifications to move the lens holder inline with the beam? Or do I just have to move the fixed mirror and re-align everything? If that's the case, any idea which way I should move the fixed mirror?



![images/75adda863cc39535d009b0c4c47b7c17.png](https://gitlab.com/funinthefalls/k40/raw/master/images/75adda863cc39535d009b0c4c47b7c17.png)
![images/bb66e6af16fa2fe25a75ea9ac7c8fc09.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb66e6af16fa2fe25a75ea9ac7c8fc09.jpeg)

**"Jack Sivak"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 08:00*

You can actually rotate the head a bit. Or the head unscrews from the plate & can shift slightly to the right (front of machine) as the hole is bigger than the thread that screws in.


---
**Stephane Buisson** *June 27, 2016 08:08*

as **+Yuusuf Sallahuddin**  underline here, it's not all to try to center into the hole. what matter is to be parrallel to X and Y axis (90degrees angles, origin being the tube).


---
**Jim Hatch** *June 27, 2016 11:39*

If you get an air assist, it will be critical to get it centered or your beam will hit the inside of the nose cone on the nozzle. Might as well get used to centering it now 😃


---
**Don Kleinschnitz Jr.** *June 27, 2016 12:27*

It is important for the beam to hit the mirror in the center (actually a bit off vertical as the mirror is at an angle) so that it travels perpendicular to the bed as it exits. In addition to rotating the head, what is stopping you from moving it to the center by adjusting the previous mirror?

Also insure that the beam hits the center when the carriage is positioned on the right and left extremes.


---
**Ned Hill** *June 27, 2016 14:03*

Is your x-axis rail perpendicular to the y-axis?   If not that could be your problem if the right side of the x-axis rail is too high.  Several of us that recently bought machines had this issue.


---
**Jack Sivak** *June 27, 2016 16:36*

**+Yuusuf Sallahuddin** How would I unscrew the head so that it can rotate? The only screws I can see are the ones that are keeping the plate in.



**+Jim Hatch** This is the sort of information I came here for! Thanks for the heads-up.



**+Don Kleinschnitz** The y-axis stage doesn't allow me to move the mirror in those directions. I would have to reposition the fixed mirror and re-align everything to change where the beam is hitting - just adjusting the alignment screws on the y-axis stage would throw off the fact that the beam is centered at both extremes.



**+Ned Hill** Just checked, and it seems like they're as perpendicular as I can make out. I'll keep in mind mechanical misalignments though when checking for future issues.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 20:10*

**+Jack Sivak** The head actually is 3 parts that unscrew from each other. If you grab the top part (above the mounting plate) & the lower 2 parts (below the mounting plate) then you can turn them in opposite directions to unscrew them from each other. Check this previous post of mine: [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/iTbTG7u75DC](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/iTbTG7u75DC)



There are some photos there showing the parts of the head pulled apart.


---
**Ned Hill** *June 27, 2016 23:08*

you may also be able to slide the y-axis mirror mount toward back a bit as there is usually a little bit of play.  If it came down to it you could also always drill or otherwise mill some extra room on the y-axis mirror mount to let it slide back even more.  Just a thought.


---
*Imported from [Google+](https://plus.google.com/106960809851059243398/posts/9Rphx6S4Fct) &mdash; content and formatting may not be reliable*
