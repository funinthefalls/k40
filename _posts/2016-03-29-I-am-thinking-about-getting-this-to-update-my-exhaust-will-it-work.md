---
layout: post
title: "I am thinking about getting this to update my exhaust will it work?"
date: March 29, 2016 17:23
category: "Discussion"
author: "3D Laser"
---
I am thinking about getting this to update my exhaust will it work? 



 [http://m.ebay.com/itm/iPower-4-6-8-10-12-inch-Inline-Fan-Carbon-Air-Filter-Ducting-Combo-HPS-Grow-/181153049913?nav=SEARCH&varId=480242763030](http://m.ebay.com/itm/iPower-4-6-8-10-12-inch-Inline-Fan-Carbon-Air-Filter-Ducting-Combo-HPS-Grow-/181153049913?nav=SEARCH&varId=480242763030)





**"3D Laser"**

---
---
**Richard** *March 29, 2016 17:35*

That's essentially the set up I used, except instead of the giant filter I just used a dryer exit port. I cut a piece of plywood for my window, and cut a hole in it for the dryer exit piece. If you like, I can link that for you (bought it on Amazon). I also bought a "dust hood" for the back of the laser that fits both the laser and the duct perfectly.


---
**Richard** *March 29, 2016 17:50*

On second thought... I'll link them now in case anybody needs. Dust Hood (it is slightly loose, use double-back tape to hold it in solid): [http://amzn.com/B003NE59HE](http://amzn.com/B003NE59HE)


---
**Richard** *March 29, 2016 17:51*

Exhaust Vent (I specifically chose one that would prevent bugs/critters from climbing inside): [http://amzn.com/B0085UZLMY](http://amzn.com/B0085UZLMY)


---
**Anthony Bolgar** *March 29, 2016 18:14*

You van get a 12V bilge exhaust blower that moves around 300-350 CFM of air for about $25.00 on ebay, or places like [banggood.com](http://banggood.com)


---
**Sunny Koh** *March 29, 2016 19:03*

You can get the dust collector cheaper from [http://www.toolorbit.com/Kaufhof/kaufhof-kwy162.html](http://www.toolorbit.com/Kaufhof/kaufhof-kwy162.html) , got my for USD$5.49 shipped when they were in stock.


---
**3D Laser** *March 29, 2016 19:06*

I doubt I will get this one as I thought it was a through filter and not an end cap filter and I do have an exhaust port.  


---
**Phillip Conroy** *March 29, 2016 19:15*

The air flow of that one is 290cfm the one i linked to was 25m a second =900cfm 3x better than the one u found-do not be fooled by size go by flow rates


---
**Phillip Conroy** *March 29, 2016 19:30*

This is also a good fanhttp://[www.ebay.com.au/itm/750W-HVLP-Dust-Collector-High-Volume-Suction-Wood-Working-Vacuum-Cleaner-35L-/221993659793?hash=item33afda6d91:g:c4IAAOSwHaBWldcU](http://www.ebay.com.au/itm/750W-HVLP-Dust-Collector-High-Volume-Suction-Wood-Working-Vacuum-Cleaner-35L-/221993659793?hash=item33afda6d91:g:c4IAAOSwHaBWldcU)


---
**Phillip Conroy** *March 29, 2016 19:40*

Found another one as i think u are in the usahttp://[www.ebay.com/itm/13-Gallon-Industrial-Dust-Collector-Portable-1-HP-Wood-Heavy-Duty-/301885496988?hash=item4649c71e9c:g:1BEAAOSwzgRW1Sna](http://www.ebay.com/itm/13-Gallon-Industrial-Dust-Collector-Portable-1-HP-Wood-Heavy-Duty-/301885496988?hash=item4649c71e9c:g:1BEAAOSwzgRW1Sna)

This is the one to buy esp if you have a long run of piping,i know it is not cheap however  the fans under 100 dollars i have found to be not much better than stock


---
**Anthony Bolgar** *March 29, 2016 20:31*

Those large dust collectors are way overkill for what is needed. You would have to make a lot of holes in the case to allow fresh air to fill the void made by sucking out all the air. There will be a distinct possibilty of the air currents moving materials around on the bed while trying to engrave or cut them, ruining the item. As well they take up a lot of room. Trust me, a 300CFM inline bilge blower will move more than enough air (might still need to add intake vents), fits into a small space, and is reasonably quiet, and very inexpensive.


---
**Phillip Conroy** *March 29, 2016 21:53*

have a close ook at the insides of the k40 it is so full of places thst air can enter it is a joke-ie power supply through to laser bed.do not listen to the above person i have tryed a 300cfm fan and it was junk-do not forget the length of pipe ,is your pipe smooth walled or   ribbed ,as well as a fan like a water pump works best pushing air not sucking so u loose efficancy there as well. I spent 2hours blocking every hole i could find in the case and between power suppy and laser bed to try and get the stock fan to clear my bed when cutting mdf-still had to wait 10 seconds or i would smell the fumes. And mt pipe was only 1 meter long .......i used to use a flat sheet of tin as the top of my cutting bed and hold the work peaces down with strong magnets as i only had air assist through a small hose attached to the side of the laser head so that the air airassist wass blowing across my work ,now that i brought a proper air assist setup that blows around the laser beam straight down i no longer need to hold my work down with magnets .i also now use a pin bed to hold work so no a 900 cfm blower will not move the work peace






---
**Anthony Bolgar** *March 30, 2016 03:46*

Corey, obviously Peter and myself have different opinions on what is required. I based my recommendation on my own personal experience with the K40, and an engineering degree. If we do some quick math it may explain why I recommended the bilge blower. The K40 has an approximate physical volume of 5 cubic feet. So at 300cfm you would be making a complete air change of the entire volume once per second. At 900 CFM you are replacing the entire air volume 3 times per second. This obviously does not take into considerations like length of duct run, number of elbows, smooth vs corrugated pipe, and a host of other inter-related items. But it is a reasonable start to determining the required CFM at the most basic level. There is no way that all the openings that exist on the K40 can supply that amount of air required to replace 900cfm , so you are starving the blower for air, it can only move what is available to it. This creates a condition in which the blower can not operate at its rated capacity, as well as putting the motor under stress, which can lead to premature failure.



Good luck in your adventures with the K40. It can be frustrating as hell some days, but when you see the object that existed only in your mind minutes before become a reality, it is a great feeling, makes up for the crappy days.






---
**3D Laser** *March 30, 2016 04:04*

I ended up getting an eight inch vortex s series 720 exhaust fan for 130 on eBay.  Mainly because the s was for silence I am also going to get a speed controller so I don't run into the problem you are describing Anthony and I figured if I get my second k40 up and running it might be enough to run both  


---
**Anthony Bolgar** *March 30, 2016 06:26*

Sounds like a plan. Let me know how the fan works out once you get it. BTW, are you interested in parting out the other K40 you have?


---
**Phillip Conroy** *March 30, 2016 07:39*

Try the new fan with out the speed controler,going from 8 inch to whatever size pipe u use in my case 4 inch halfs ths cfm from 900 to 450, i do not not hsve an enginering deg ,however i have pro ed enginers wrong in the past


---
**Phillip Conroy** *March 30, 2016 07:42*

Also rember all fans are rated without any piping attached when tested


---
**Justin Mitchell** *March 30, 2016 10:27*

We use a similar setup, it is way better at fume/smoke extraction than the supplied extractor was. but the carbon filter does little to nothing to remove the smells and smoke, so we stick the pipe out of the window when possible instead


---
**3D Laser** *March 30, 2016 17:21*

**+Phillip Conroy** good to know.  I plan on making my dryer vent a a six inch vent then use a collar to connect to the eight inch Run the pipe to the machine and reduce it to 4 inches at the machine.  Figured I will loose some cfm but hope not half


---
**Richard** *March 30, 2016 20:05*

After reading the comments here, I'm thinking about upgrading my exhaust system. How does this look? [http://www.harborfreight.com/13-gal-1-hp-industrial-portable-dust-collector-61808.html](http://www.harborfreight.com/13-gal-1-hp-industrial-portable-dust-collector-61808.html)


---
**Phillip Conroy** *March 30, 2016 20:35*

 Looks good ,if too powerful you can always instal a motor speed control and cut the rpm down


---
**Sparks and Splinters** *April 19, 2016 02:40*

exact setup i run, my fan is good for 600cfm and made a giant difference after removing the tiny airduct in the case. 


---
**Frank Jones** *August 08, 2017 17:01*

Does the one pictured above knock down the smell from cutting acrylic? I just bought a k40 and I dont want my neighbors complaining that I am trying to kill them. 




---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/b1zPB3gJnVT) &mdash; content and formatting may not be reliable*
