---
layout: post
title: "I've just moved to London; however my K40 is 110V"
date: September 24, 2017 11:02
category: "Discussion"
author: "Dushyant Ahuja"
---
I've just moved to London; however my K40 is 110V. Now I have two options:



1. 220-110 V Transformer

2. New power supply





Need opinions on what's the best option. Also - please provide links of reliable suppliers if possible. 

Thanks





**"Dushyant Ahuja"**

---
---
**Anthony Bolgar** *September 24, 2017 11:12*

For ease of conversion I would just get a power converter, that way you just need to plug it in, no rewiring involved. However it may not be the most cost effective method, replacing the power supply would probably be cheaper. If you have basic electronics skills, I would then say replace the PSU.


---
**BEN 3D** *September 24, 2017 11:20*

Did you shure that there is not a little hiding switch on your power supply, to set the 110 voltage?


---
**Adrian Godwin** *September 24, 2017 11:26*

Some of the power supplies do have a voltage change - if a switch, as Ben says, that's easy. If it's soldered links, you need to be braver. 



If you don't want to do that, I'd say go with the transformer - you probably have other appliances that also need it.



Get one rated at least 500W, which should cost you under £50. In fact for £50 you can get a secondhand 'site transformer' : this is a big heavy yellow plastic-housed transformer used by builders to run their 110V tools and rated at 3300W or more. Very tough, nice to have on hand. 


---
**Adrian Godwin** *September 24, 2017 11:51*

Come along to London Hackspace - we've got a big laser (80W, A0) for your large work, plenty of people interested and a few people own K40s too. Or South London Maker Space, if that's more convenient for you.


---
**Stephane Buisson** *September 24, 2017 11:52*

Check the ground connection if transformer ...


---
**Phillip Conroy** *September 24, 2017 12:54*

Check outside of current power supply ,most have 110 v and 240v switch,just switch it to 240


---
**Dushyant Ahuja** *September 24, 2017 18:15*

**+BEN 3D** doesn't have one. I checked :-(


---
**Dushyant Ahuja** *September 24, 2017 18:17*

**+Adrian Godwin** will definitely come in one of these days. I'm staying in Stratford. Site transformer sounds like a good idea. Will check. Thanks. 


---
**Paul de Groot** *September 24, 2017 21:11*

The switch is hidden behind the panel so you might need to unscrew the power supply switch it and remount. It is not a flip switch but an simple black slider. You will overlook it easily and take it for a label.


---
**Don Kleinschnitz Jr.** *September 25, 2017 03:19*

All K40 LPS that I have seen have this switch :)


---
**Wolfmanjm** *September 25, 2017 17:14*

I just moved to the UK. My K40 had a switch in the PSU and I switched it to 220v and it worked fine, but beware the fan at the back of the chassis is 110v, and you need to remove it and/or replace it with a 220v fan (or a 12v fan with a wall wart). Mine popped before I realized the fan was 110v, but no harm was done except a broken fan which is easily replaceable. One issue is I can't find a source of distilled water, only deionized water, hopefully that is ok to use. works so far.


---
**Dushyant Ahuja** *September 25, 2017 19:40*

**+Paul de Groot** I'll look again. 


---
**Paul de Groot** *September 25, 2017 22:28*

**+Dushyant Ahuja** it's just a black slider with 110/240 engraved. Hard too see since it's all black maybe **+Don Kleinschnitz** has some photos of it.


---
**Don Kleinschnitz Jr.** *September 26, 2017 12:54*

#K40LPS

#K40ACPower



On left hand side of supply. Post pictures of your supply please showing connectors and this side.



![images/28ff4d47c895953e8747ba1ad6a37588.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/28ff4d47c895953e8747ba1ad6a37588.jpeg)


---
**Don Kleinschnitz Jr.** *September 26, 2017 13:08*

**+Wolfmanjm** 

#K40Coolant



I think de-ionized is ok as it is just purified with a different method. However the forums suggest that de-ionized can react with metals.



**+Ned Hill** what is your take???



[https://en.wikipedia.org/wiki/Purified_water](https://en.wikipedia.org/wiki/Purified_water)





<b>I found these references:</b>

[ngceoservice.com - www.ngceoservice.com/Data/DOC96/SVC-FSB-0008%20Water%20as%20Coolant%20for%20Lasers.pdf](http://www.ngceoservice.com/Data/DOC96/SVC-FSB-0008%20Water%20as%20Coolant%20for%20Lasers.pdf)


---
**Ned Hill** *September 26, 2017 13:32*

De-ionized is fine as it, as the name implies,  removes the ions from solution.  Not sure why the "forums" think it can react with metal.  De-ionization doesn't remove uncharged organics or bacteria/fungus so you just need to make sure you change it out on a regular basis.  DI water is the common type of water used in most chemistry labs as general use water.


---
**Dushyant Ahuja** *September 30, 2017 08:22*

Photos of the power supply 

![images/e5d2a051f19b55dbc3eeda2a64535e04.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e5d2a051f19b55dbc3eeda2a64535e04.jpeg)


---
**Dushyant Ahuja** *September 30, 2017 08:22*

![images/0618f8b8563772c657e6601f21100f20.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0618f8b8563772c657e6601f21100f20.jpeg)


---
**Dushyant Ahuja** *September 30, 2017 08:22*

The switch is missing. 


---
**Don Kleinschnitz Jr.** *September 30, 2017 12:41*

**+Dushyant Ahuja** wow!, can you open the supply (make sure that it is unplugged) and take a picture of where the switch was supposed to be? Also be good to take some other pictures of the insides so we can see what else is weird.


---
**Dushyant Ahuja** *October 06, 2017 06:41*

I bought a site transformer. Got a good new one for 60 quid. Sorts out a lot of other issues as well - now I can use my bread maker :-)


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/3weuaBLPzHS) &mdash; content and formatting may not be reliable*
