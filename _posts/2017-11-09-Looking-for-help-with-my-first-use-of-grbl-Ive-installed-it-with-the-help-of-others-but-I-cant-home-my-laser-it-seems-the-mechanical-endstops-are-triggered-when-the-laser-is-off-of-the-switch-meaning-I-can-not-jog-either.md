---
layout: post
title: "Looking for help with my first use of grbl, I've installed it with the help of others but I can't home my laser, it seems the mechanical endstops are triggered when the laser is off of the switch meaning I can not jog either,"
date: November 09, 2017 21:30
category: "Modification"
author: "Andy Shilling"
---
Looking for help with my first use of grbl, I've installed it with the  help of others but I can't home my laser, it seems the mechanical endstops are triggered when the laser is off of the switch meaning I can not jog either, is there a way I can invert the endstops or will I have to replace them with opposite type. Ie N/C or N/O.





**"Andy Shilling"**

---
---
**Paul de Groot** *November 09, 2017 22:29*

Hi Andy you need to clear the alarm first with $X than you can home the machine with $H and jog the x y axis.😁


---
**Andy Shilling** *November 09, 2017 22:35*

I did clear the alarm but it kept coming up with an error every time I tried to home or jog. I then pushed my switch in and pressed home and the laser moved until I released the switch hence why I think I need to invert the switches.


---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 00:16*

Check what $5 is set to? 


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/UtSuEsNxqSJ) &mdash; content and formatting may not be reliable*
