---
layout: post
title: "After months of stopping and going, cursing in desperation and several false \"Eureka!\""
date: July 20, 2017 20:59
category: "Discussion"
author: "Reinier Rossen"
---
After months of stopping and going, cursing in desperation and several false "Eureka!" moments... I've decided I won't fix this on my own. Hopefully someone here can shine some light in my wavy darkness :-)



[lens clean, tightened, mirrors tightened, belts are tight (and tried various tightnesses), pulleys are press fitted on the shafts from what I can tell - so no weeble wobble there, the file is "clean"]



So, the picture illustrates the problem I have. I have a kind of but not entirely rhythmic deviation in my x-axis. This is a "bar code" sample I drew with vector lines and then engraved in LaserWeb. The same thing happened when I engraved from LaserDRW prior.



As far as I can tell the problem first occurred after I tried to swap out the moshi board for a ramps board - which was a disaster to say the least. I reverted back to moshi before buying a Smoothieboard. Hoever I can't properly engrave anymore since.



I have checked all the mechanical components as much as I can, to the point of trying other motors and such. Right now I'm waiting on parts to replace the entire carriage with v-slot rails and stuff, but I'm not sure that will be the solution. My best guess is that the laser is firing too slow, but I don't have the equipment to test it...nor does it make a lot of sense. The only lead I have/had is that the tube should be " hot stand by " in between actual firing to agitate the plasma and stuff... but from what I can measure and trial and error, the Smoothieboards min-value doesn't do anything.



I used Don's wiring schematic with the only exception I'm using a big mosfet as I've managed to somehow blow up the small one and that isn't replaced yet.



Who has any suggestions on how to test where the culprit lies :-)?



Thanks in advance and please do ask if I need to run some tests beyond the usual.

![images/8101ad958820579f84349cceeb00d80e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8101ad958820579f84349cceeb00d80e.jpeg)



**"Reinier Rossen"**

---
---
**Stephane Buisson** *July 21, 2017 08:38*

one of the motor coil?

to split the difficulties, I would try some test: try to invert X/Y motor (cabling, not the motor itself (not equal, one being dual shaft)) to see if the wobble appear on the other axe. if so, it would confirm motor isn't the trouble, or let you know it is.




---
**Steve Clark** *July 21, 2017 16:24*

**+Stephane Buisson** I agree, This is a very common approach used with commercial multi-axis machines... flipping axis to see if the problem follows.


---
**Mark Brown** *July 21, 2017 20:46*

Does it do anything screwy when cutting vectors, or is it only on engraves?


---
**Reinier Rossen** *July 21, 2017 21:04*

Hi guys,



Thanks for the suggestions. I did a lot of googling if it's possible to ruin a stepper motor, but couldn't find anything. I'll definitely going to  give your suggestion a shot over the weekend. 



**+Mark Brown** It seems only on engraving. My gut feeling tells it has trouble turning on fast enough, but yeah...can't really test that. Cutting goes fine from what I've observed.


---
*Imported from [Google+](https://plus.google.com/118425210559785776545/posts/B9C2yYjhDWG) &mdash; content and formatting may not be reliable*
