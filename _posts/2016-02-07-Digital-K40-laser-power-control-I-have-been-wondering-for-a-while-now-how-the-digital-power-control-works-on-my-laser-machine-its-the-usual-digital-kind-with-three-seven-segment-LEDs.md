---
layout: post
title: "Digital K40 laser power control. I have been wondering for a while now how the digital power control works on my laser machine, its the usual digital kind with three seven segment LEDs"
date: February 07, 2016 13:34
category: "Discussion"
author: "David Richards (djrm)"
---
Digital K40 laser power control.



I have been wondering for a while now how the digital power control works on my laser machine, its the usual digital kind with three seven segment LEDs. I have heard people on here refer to the control board as a digital potentiomenter which I thought was unlikely.



The reason for doing this is because I'm wondering if it will be possible to incorporate the digital control board into an upgrade using smoothiware which also has PWM control. I imagine the two PWM controls could interfere with each other. I may decide to ditch the digital board altogether and replace it with a preset potentiometer to set the absolute maximum current at 18mA. I think this could be used as the input voltage to the smoothie PWM level converter input, I'll have to try it and see.



 Today I have connected an oscilloscope to the control input terminal on the power supply to measure the signal. It is a PWM signal. The video shows it when I increase the power in steps of 10% from 0 upto 90%, finishing back at 0%. The noise is my air pump which is fed from the PWM and only turns on if there is a PWM signal present. 



An observation is that when the control board was set to 40% the pwm signal had a 50% mark to space ratio. The control does not appear to be linear, I wonder if this is intentional. Also it is not possible to achieve 100%, 99.9 is the maximum achievable - good enough for all practical purposes I suppose.



hth David.


**Video content missing for image https://lh3.googleusercontent.com/-hXRdc4j9IPw/VrdH1p7D8xI/AAAAAAABvzQ/OcKhHHFI7aI/s0/VID_20160207_125928435.mp4.gif**
![images/12da1069be750d8fdcbf2198d18c9c82.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/12da1069be750d8fdcbf2198d18c9c82.gif)



**"David Richards (djrm)"**

---
---
**Stephane Buisson** *February 07, 2016 13:39*

thank you **+david richards** to go to the bottom of it. (you are better equip than me).



One way I can see this thing could go, is a daughter board for a  #Smoothiebrainz  (**+Peter van der Walt** ) with smoothieware (**+Arthur Wolf** )


---
**Stephane Buisson** *February 07, 2016 13:58*

a daughter board have the merit to be noob friendly, and could act as a middle man board (connectors).


---
**HalfNormal** *February 07, 2016 17:16*

Found this info and it might be of some help.

[http://redmine.laoslaser.org/projects/laos/wiki/Moshi-to-laos-conversion-SH-K40-1V](http://redmine.laoslaser.org/projects/laos/wiki/Moshi-to-laos-conversion-SH-K40-1V)


---
**Stephane Buisson** *February 07, 2016 17:26*

it's a different version of MYJG40W same reference.

it's a lot to be learn from laos convertion.


{% include youtubePlayer.html id="yJImenhGE58" %}
[https://youtu.be/yJImenhGE58?t=1356](https://youtu.be/yJImenhGE58?t=1356)﻿

**+Peter van der Walt** 


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/Zm89ciSxvNh) &mdash; content and formatting may not be reliable*
