---
layout: post
title: "after 600 hours laser tube only outputting 15 Watts -time for new tube,18 months old tube.measured with power meter, wish I could find adjustable tube mounts for the k40 laser tube,still using stock m2 board and very happy"
date: December 08, 2016 05:53
category: "Discussion"
author: "Phillip Conroy"
---
after 600 hours laser tube only outputting 15 Watts -time for new tube,18 months old tube.measured with power meter, wish I could find adjustable tube mounts for the k40 laser tube,still using stock m2 board and very happy with k40 so far





**"Phillip Conroy"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 08, 2016 07:14*

Good to know 600 hours odd before a significant decrease in power.


---
**Alex Krause** *December 08, 2016 07:30*

Where did you measure at with the power meter?


---
**Alex Krause** *December 08, 2016 07:31*

Also don't you mainly do cutting of MDF? Or am I thinking of someone else


---
**Phillip Conroy** *December 08, 2016 09:15*

Measured at tube,and yes mainly cut 3mm and,temps never agent over 25 degrees. C of under 12 degressc.cut for 3 hours blocks rested laser tube for an hour then cut again


---
**Phillip Conroy** *December 09, 2016 09:23*

'Orded new tube on the 6 dec arived on the 9 dec 3 hoursatter instaled ,aligned and tested power ,back to 40 watts ,very happy now


---
**Josh Speer** *December 09, 2016 21:02*

What sort of tube mounts are you looking for? I've 3d printed some from thingaverse that are working well.


---
*Imported from [Google+](https://plus.google.com/108005343562094867099/posts/4dGsJgPh6Zk) &mdash; content and formatting may not be reliable*
