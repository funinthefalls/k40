---
layout: post
title: "Hello, I am a newbie to laser cutting and I need a bit help"
date: June 13, 2016 18:41
category: "Hardware and Laser settings"
author: "Mikkel Steffensen"
---
Hello,



I am a newbie to laser cutting and I need a bit help. I have some problems with my laser cutter K40 I bought on Ebay in March.

•	My first problem is that I can’t cut in 3mm thick cast acrylic only if I run with full power and a speed of 1.75 mm/second

o	I have upgraded to air assist head and I think it could be the distance from the lens to the material there is the problem. What should the distance be?

•	My second problem is that when I cut, there comes some marks at the acrylic in the areas, where it have been placed on the plate in the machine.

Best Regards

Mikkel Steffensen





**"Mikkel Steffensen"**

---
---
**Anthony Bolgar** *June 13, 2016 19:05*

If you have the most common lens, then the focal length is 50.8mm from the center of the lens to the work surface. Your problem cutting acrylic at full power probably means your mirrors are out of alignment and the full laser beam is not reaching the lens, ir the beam is not perpendicular to the work surface and is partial being blocked by the air assist cone.


---
**Derek Schuetz** *June 13, 2016 20:11*

Definitely sounds like an alignment issue maybe focal distance also


---
**Jim Hatch** *June 13, 2016 21:47*

The 50.8mm distance should be from the lens and not the point of the air assist cone (the bottom of the head). It's the bottom of the lens that counts. 



Also make sure the lens is in correctly (bump or convex side up). People will sometimes flip them over/upside down.


---
**Vince Lee** *June 14, 2016 00:11*

Instead of directly measuring the distance, you probably want to run a "ramp" test, cutting a line onto an strip of plywood to angled in a way so that the cutting distance varies along its length. Where it cuts deepest (and thinnest) would indicate an optimal distance. This made the most difference to me. Also insure (using paper or tape) that the beam is hitting close to the center of all mirrors, and especially the hole in the side of the head.


---
**Scott Marshall** *June 14, 2016 19:58*

What they said plus:

[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
*Imported from [Google+](https://plus.google.com/117825343986845004843/posts/RxKuGf7smhz) &mdash; content and formatting may not be reliable*
