---
layout: post
title: "The power/test light is always on now"
date: February 24, 2018 17:02
category: "Hardware and Laser settings"
author: "Ray Rivera"
---
The power/test light is always on now. I have the digital controls board - buttons to control laser % and test/cutoff buttons. There is no laser firing, the cutoff switch is on off, I've checked all the cable connections and everything looks great and seated properly. Thoughts?

![images/3d8994b1d28510e8361b3b3300348868.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d8994b1d28510e8361b3b3300348868.jpeg)



**"Ray Rivera"**

---
---
**Don Kleinschnitz Jr.** *February 24, 2018 17:21*

Can we see a picture of the whole front edge of the board?

That light is usually the power on light and is lit as long as the machine is on.



Does the laser fire if you push the test button down on the LPS (not the button on the panel)?


---
*Imported from [Google+](https://plus.google.com/117639914879018784950/posts/b5E69DGVvqA) &mdash; content and formatting may not be reliable*
