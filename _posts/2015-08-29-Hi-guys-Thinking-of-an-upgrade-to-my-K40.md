---
layout: post
title: "Hi guys, Thinking of an upgrade to my K40"
date: August 29, 2015 15:39
category: "Discussion"
author: "David Wakely"
---
Hi guys,



Thinking of an upgrade to my K40.



I am looking at a SH-350 which is a 50w from Shenhui.



[http://pages.ebay.com/link/?nav=item.view&alt=web&id=111639792412&globalID=EBAY-GB](http://pages.ebay.com/link/?nav=item.view&alt=web&id=111639792412&globalID=EBAY-GB) 



It's a good price. Just wondered if anyone has used one or know of any good or bad points?



Cheers





**"David Wakely"**

---
---
**Fadi Kahhaleh** *August 30, 2015 05:06*

That is a good price indeed. However I have opted out from buying a bigger machine, I am seriously thinking of building one. (using [openbuilds.com](http://openbuilds.com) build logs, I have my eye set on FreeBurn v2 probably)



But again, my specs require a machine with a much bigger cut bed, so I am looking easily at 5-8K for such a machine, that is why building it myself makes sense to me.


---
**Flash Laser** *September 02, 2015 00:55*

can not send you pic here, show I will publish the pics about the 40W mini laser machine with honeycomb, please pay attention on it. thanks.


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/5kc27KYFGSs) &mdash; content and formatting may not be reliable*
