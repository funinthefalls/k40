---
layout: post
title: "This is my first attempt on slate"
date: May 23, 2018 16:39
category: "Object produced with laser"
author: "syknarf"
---
This is my first attempt on slate. Engrave on this material looks so good.

![images/d7883dc4a4d5f45076e21067ea003b20.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d7883dc4a4d5f45076e21067ea003b20.jpeg)



**"syknarf"**

---
---
**Duncan Caine** *May 23, 2018 17:01*

That looks really good.  Is that natural slate or composite slate?


---
**syknarf** *May 23, 2018 17:06*

**+Duncan Caine** didn't know actually.... I throwed the packaging... but in a round piece that I have it says it's 100% natural slate. Think this was the same brand.


---
**Roberto Marquez** *May 23, 2018 20:21*

Nice design. Do you the graphic available for download somewehre?




---
**syknarf** *May 23, 2018 20:33*

**+Roberto Marquez** the design is available in the K40 Facebook group


---
**Roberto Marquez** *May 23, 2018 20:40*

thanks!




---
**Andy Shilling** *May 23, 2018 21:25*

That will be natural slate, it gives a fantastic finish and is even dishwasher safe. I do a lot of placemats and coasters in slate and in some you will find the engraving will actually come through with a gold tint.![images/9042abe0fc3fd39cac92a2c9f02d6651.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9042abe0fc3fd39cac92a2c9f02d6651.jpeg)


---
**Don Kleinschnitz Jr.** *May 23, 2018 22:31*

Where do you get the slate?


---
**syknarf** *May 23, 2018 22:37*

**+Don Kleinschnitz** I purchased it on a local chinesse bazar.


---
**Rob Mitchell** *May 24, 2018 13:08*

**+syknarf** what is the exact name of the FB group. Thanks


---
**syknarf** *May 24, 2018 13:30*

**+Rob Mitchell** Chinese k40 laser group


---
**Nigel Conroy** *May 24, 2018 14:12*

If you apply a spray gloss before engraving it can help the white of the engraving pop more


---
**syknarf** *May 24, 2018 15:09*

**+Nigel Conroy** thanks, I will try it


---
**Sebastian C** *May 25, 2018 14:50*

+skynarf can you give another source for people like me without facebook or at least the link to the specific group? thx


---
**syknarf** *May 25, 2018 15:07*

**+Sebastian C** Didn't know of another source... this is the link to the group: [facebook.com - Chinese K40 Laser Group.](https://www.facebook.com/profile.php?id=888394261237059&ref=br_rs) search on the files section for aztec wars


---
**Travis Sawyer** *May 26, 2018 17:31*

There are a lot of slate shingled roofs in my area. I should find some take off and try this!


---
**Kelly Burns** *May 27, 2018 11:48*

 I find used slate roofing tiles on Craigslist.  


---
**Nigel Caddick** *December 10, 2018 10:33*

**+syknarf** May I ask if you vector, or raster engraved it? It looks fantastic! I use K40 Whisperer and often wonder if I should blue line a piece or black line :)




---
**Don Kleinschnitz Jr.** *December 10, 2018 15:39*

**+Andy Shilling** **+Ned Hill** note the "gold tint"...


---
**Ned Hill** *December 10, 2018 17:14*

**+Don Kleinschnitz Jr.**  Doesn't really look gold on my monitor.  I accept that it was just a trick of the lighting.  I was really surprised that you were able to get some gray scaling when you engraved that coaster.  Something I'm going to play with myself.


---
**syknarf** *December 11, 2018 01:28*

**+Nigel Caddick** hi! It was vector engrave, but raster looks also very fine on slate.


---
*Imported from [Google+](https://plus.google.com/113055105026792621247/posts/2uBH2RXKqsG) &mdash; content and formatting may not be reliable*
