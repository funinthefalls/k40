---
layout: post
title: "There has been a lot of discussion on here about lens orientation....after installing the 50 watt 1000mm puri tube I noticed that I had lost serious power....after a week of different test and playing with cutting power and"
date: April 05, 2016 13:13
category: "Discussion"
author: "Scott Thorne"
---
There has been a lot of discussion on here about lens orientation....after installing the 50 watt 1000mm puri tube I noticed that I had lost serious power....after a week of different test and playing with cutting power and speeds I decided to turn my lens over....mind you that I have a lens that is curved on one side and flat on the other...I've been running it with the flat side down....I could barely cut 1/4 inch acrylic at 8mm/s at 50% power that way.....after turning the lens over with the curved side down...it Cuts 1/4 acrylic at 14mm/s at 45% power.....I was entirely baffled by this...this goes against everything that I've learned...so what did I do...I put the old reci tube back in and noticed something strange...the beam size is way bigger on the new tube...6mm...the beam on the reci tube is under 4 mm..

I guess that has an impact on lens...any thoughts on this? 





**"Scott Thorne"**

---
---
**Scott Marshall** *April 05, 2016 13:25*

This is a company that does the big lasers, and the best optics info I've seen anywhere.

[http://www.ophiropt.com/co2-lasers-optics/focusing-lens/knowledge-center/tutorial/cutting-head-optics](http://www.ophiropt.com/co2-lasers-optics/focusing-lens/knowledge-center/tutorial/cutting-head-optics)


---
**Scott Thorne** *April 05, 2016 13:45*

**+Scott Marshall**...great info..thanks man....I still don't know why it's cutting better with it in the wrong way...I've changed the lens with different focal length lenses and the same effect...that's with this tube only though....work the reci tube back in it Cuts better with the flat side down. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 13:53*

From what I've seen when reading up about the lasers, the larger the power (or higher you are running it) the thicker the beam will become. Even with my own tests, I notice a difference between kerf-width of 2 passes @ 5mA & 1 pass @ 10mA. That's interesting that your 2 different tubes using the same lens prefer different orientations. You mention that 1 side is curved & the other is flat. Is it a convex or concave curve? It would be really nice to be able to see the beam. I know the beam has a specific wavelength (10600nm or something?) so I wonder if we can get equipment to "see" the beam (for a reasonable price)? That would make visualising what is happening really simple.


---
**Scott Marshall** *April 05, 2016 13:54*

Whatever works, you're certainly not doing any harm.



 Maybe a out of spec lens?



I DO know what you mean, you gotta know, because....

I'm betting you figure it out eventually


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:00*

I'm curious what happens if we focus the output of one lens through a smaller lens. Will we end up with a super powered nanometre width beam?


---
**Scott Thorne** *April 05, 2016 14:01*

**+Scott Marshall**...I'm leaving it in upside down...it can cut through 10mm acrylic at 5 mm/ at 50% power...I've never been able to do that before.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:04*

**+Scott Thorne** That's pretty decent to get through that thickness with 50% power. I'd be fairly happy about that. I just noticed when reading that article **+Scott Marshall** linked, that the temperature absorption of the lens can cause additional surface curvature of the lens. This may be related to your lens not working so well in the normal orientation. Possibly the heat from the larger/thicker beam is causing it to curve more, hence changing your focal point (up or down from the normal focal point).


---
**Scott Marshall** *April 05, 2016 14:15*

I just thought of something, Polarization.



I've been doing some reading on the  physics of the Co2 laser, and the practices of large Co2 machines, and there's a couple things we don't worry about with the diminutive K40, but they are still in effect. 



Polarization comes in directional and circular, and they use polarizing filters on large cutters for a couple of reasons, one being a linearly polarized beam cuts better and narrower along it's long axis, and that makes for different behavior on the X axis than the Y axis etc. Not good. 

Circularly polarized beams move through the mirrors more efficiently too, a big consideration when you're using $3000 water cooled copper mirrors....  1% loss  of 5 KW is 50watts, more than my laser puts out...  burning up your expensive mirrors.



Our lenses and mirrors cause polarization via the internal stresses in the substrate and just plain poor quality, but it's unintentional and ignored. If you get a “lucky stack up” of random polarization, you may well end up with a beneficial result – a high percentage of circular polarization maybe?



The other overlooked subject is beam diameter. It the large lasers, they actually strive for a large beam diameter out of the tube, then INCREASE it with a beam expander (this is where they circular polarize it too). The reason is to keep the watt density lower on the mirrors to reduce the damage from heating. They often will run a ¾” beam through the optics, then reduce it at the head. Your new tube may be throwing a narrow beam, and flipping the lens widens it out to where it cuts better?



Those are my deep thoughts for the day. Now it's your turn, you get to see if there's something there.....



Scott


---
**Scott Thorne** *April 05, 2016 14:16*

**+Yuusuf Sallahuddin**...that very well could be the case...I'm on vacation this week and the wife and I are just relaxing at home so I think more test are needed.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:18*

**+Scott Thorne** I'd consider attaching a thermal probe to the side of the lens somehow to monitor it's temperature & see if there is any difference/distortion of the cut width at different temperatures. Another idea to add to the list of things I will do to my machine as time/$ allows.


---
**Scott Thorne** *April 05, 2016 14:19*

**+Scott Marshall**...another thing about this puri tube that I noticed...at low power when I test fire the laser at tape...like I'm going to do some alignment the spot on the tape looks like a donut..burned on the outside but not in the middle....it does this at the tube also so I know that nothing is in the way of the beam.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:20*

**+Scott Thorne** That sounds exactly like when I deliberately moved my workpiece closer to the lens to defocus it (so it wouldn't engrave all the way through the thin leather at lowest power/highest speed). So it makes me think that it definitely is a focus issue.



edit: I just realised you said it does this at the tube also. So that is different, because there is no lens focusing it at that point. So it must have some kind of dead/weak spot in the centre of the beam. Like the "eye of the storm" sort of thing.


---
**Scott Thorne** *April 05, 2016 14:26*

**+Yuusuf Sallahuddin**...I just posted a pic...it does it all the way to the tube.


---
**Scott Marshall** *April 05, 2016 14:26*

**+Scott Thorne**

That sure could make it act weird. Optics isn't as simple as most people think. I'd have to do some research to figure out what a ring shaped beam will do,  but that's sure sounds like at least part of it.



Edit-

 Yuusuf may have answered it. If moving in past the focal length will create a ring (which makes sense), then flipping the lens may move the energy back in to the center, thus filling out the spot again with even energy density all the way across.


---
**Scott Thorne** *April 05, 2016 14:31*

**+Scott Marshall**...look at the photo that I've posted about the beam characteristics of my tube...notice the hollow spot?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:32*

**+Scott Thorne** Yeah is very odd. A serious dead spot in the middle of the beam. I wonder if it is a tube fault or if this to be expected with higher powered tubes.


---
**Scott Marshall** *April 05, 2016 14:38*

**+Scott Thorne** Cross posted again.



Fault. But as long as it lives, you have the fix. Run it.


---
**Phillip Conroy** *April 05, 2016 19:23*

Talk to the makers of your laser tube about it.

Also you could have a faulty/damaged/worn out  focal lens

If you have a spare try that,what size is the focal lens in mm and what is the focal length?

I too have noticed thaat my beam is donut shaped beam on my k40.on my old 12mm focal lens i did try flat side up and could not notice any diffrance,however latter found damage to the curved side-so maybe you have a damaged focal lens however the damage is not visable yet........my new18mm focal lens is cutting very well with a beam width of 0.1mm . on the back side of the work peaces .

I 2 have been thinking about how to make the laser beam visable/photographable ,from what i have read i would need a infrared camera to do this.

Also do you have air assist that blows air the focal le lens?maybe the beam is hitting the side of the air assist nozzel.on mine ths nozzel was compltly bolocking the bean untill i aligned the laser head .


---
**Scott Marshall** *April 05, 2016 20:00*

Me too Mr Conroy, Been in search of that for years.

I have a credit card sort of tool that has a coating that fluoresces under IR. It's designed for testing IR opto-interuppters, retro-reflectors and remote controls. Been a few years since I've seen it, but it's probably in the shop somewhere unless one of my employees needed it more then I did. They must still be available. I got it from an Effector salesman if I remember correctly. 

That would show the beam shape clearly, but only at very low power levels.

FLIR cameras have come way down, you can get one for your phone for about $250. Then you could shoot a target and film it.



Edit

Not easy to find, but I found one on ebay after much digging.

I'm NOT paying 40 bucks for a strip of treated paper.



[http://www.ebay.com/itm/Infrared-Laser-Detector-and-Alignment-Card-/381584655438?hash=item58d837c44e:g:9D8AAOxy8HlSdExI](http://www.ebay.com/itm/Infrared-Laser-Detector-and-Alignment-Card-/381584655438?hash=item58d837c44e:g:9D8AAOxy8HlSdExI)


---
**Scott Thorne** *April 05, 2016 20:14*

**+Phillip Conroy**...as I've posted...this is happening way before the lens...it's happening coming out of the tube.


---
**Phillip Conroy** *April 06, 2016 04:05*

Mine is dognought shaped out of the tube on low power and before any mirrors/lenes ,still cuts fine


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 07:37*

**+Phillip Conroy** From what I remember reading a while ago, most webcam has an IR filter on it & if you remove it you can see IR. But, I tested this with an old webcam I had laying around & it worked for seeing the IR from the end of tv-remote, however it doesn't allow me to see the laser beam (unfortunately), so I assume it is something to do with the wavelength of the laser beam being outside the range that the webcam will pick up. It would be great to have some ability to see the beam though. Would make it so much easier for alignment and the likes.


---
**Phillip Conroy** *April 06, 2016 08:55*

Laser power meters 100 watt are on sale for $95 [http://www.bell-laser.com/#!product-page/c1iym/1cd1a226-cf6b-7b63-26a7-2d551eb6821a](http://www.bell-laser.com/#!product-page/c1iym/1cd1a226-cf6b-7b63-26a7-2d551eb6821a)


---
**I Laser** *April 06, 2016 10:41*

Not sure, but most webcams are IR sensitive, they usually have a filter that you can remove (with a little effort). Not sure whether, it would work or not. But did work for a homemade tracking headset I made years ago using IR diodes. :\


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 15:18*

**+I Laser** Yeah I tested it already & couldn't see the beam. However, I did a bit of reading earlier & found that you will never see the beam anyway. You will see the point that the beam reflects off objects (e.g. cutting material or dust/smoke in the air). So, it's possible that what I had could maybe pick up the laser beam if I added dust/smoke/fog/etc to the air in the beam path.


---
**Joel Kunze** *April 06, 2016 22:37*

CMOS and CCD sensors in webcams are only sensitive in the lower near IR up to around 1 micron wavelength. That is after removing the IR filter. They will not pick up the long 10.6 micron wavelength of CO2 lasers. That includes reflections from dust, smoke or anything else. Replace the webcam with a thermal imager like the systems from flir and you could view the beam reflecting off some surfaces a la video. Donut transverse mode (TEM01*) happens and it's possible there will be nothing you can do about it since the cavity is fixed on the tube. If the vendor indicated the tube is TEM00 then you might have recourse. That being said, as long as the focused spot size is small enough for what you need, that mode likely has higher power output than the same tube operating in TEM00. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/8WSPrwYHCdE) &mdash; content and formatting may not be reliable*
