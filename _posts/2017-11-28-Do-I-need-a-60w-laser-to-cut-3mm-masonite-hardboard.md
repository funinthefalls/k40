---
layout: post
title: "Do I need a 60w laser to cut 3mm masonite/hardboard?"
date: November 28, 2017 06:09
category: "Discussion"
author: "Doug Harriman"
---
Do I need a 60w laser to cut 3mm masonite/hardboard?





**"Doug Harriman"**

---
---
**Phillip Conroy** *November 28, 2017 06:21*

A k40 should cut it if correctly aligned 


---
**Chris Hurley** *November 28, 2017 13:17*

I'm doing it. It's pretty much all I do. I use 40w. I'm still working on alignment too. You NEED air assist if you want to do it right. And a better vent fan. 


---
**Doug Harriman** *November 28, 2017 16:19*

I've already ordered a 60w machine that has a wing that sticks out 17 inches. Are you doing the cutting at full power?


---
**Chris Hurley** *November 28, 2017 18:21*

Nope 12.5mms/10ma with 4 passes. But I can do it in two in 5mms. 


---
**Doug Harriman** *November 28, 2017 23:31*

**+Chris Hurley** **+Chris Hurley** 

![images/952780f38b75f6e79619b88a494d3a40.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/952780f38b75f6e79619b88a494d3a40.jpeg)


---
**Doug Harriman** *November 28, 2017 23:32*

I bought this. $880 for a 60w


---
**Doug Harriman** *November 28, 2017 23:36*

That includes shipping. I don't know anything about using it. I hoping I'll get enough help, on this site to make it work


---
**Chris Hurley** *November 29, 2017 00:14*

880 is a sweet deal. American right? 


---
**Doug Harriman** *November 29, 2017 00:51*

Dollars. I need to make these. 

![images/969c0b6c6bc2929107d29e806a8c2174.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/969c0b6c6bc2929107d29e806a8c2174.jpeg)


---
**Chris Hurley** *November 29, 2017 02:01*

So Canada then. 


---
**Doug Harriman** *November 29, 2017 02:24*

No, its $1110 with shipping


---
**Doug Harriman** *November 29, 2017 15:22*

What is the best way to get tutored in the ins and outs of how to go about operating one of these laser. This not for hobby use, I need it for work.


---
*Imported from [Google+](https://plus.google.com/110395382520478186845/posts/PxMX8dQyaBN) &mdash; content and formatting may not be reliable*
