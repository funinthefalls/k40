---
layout: post
title: "Hi Guy's Question i have my k40 up and running i have engraved some lettering as a test but now i want to cut out a part that i have scand i have saved it as an dxf file and impoted it into corel draw but can't figure out how"
date: March 23, 2016 22:17
category: "Discussion"
author: "Dennis Fuente"
---
Hi Guy's 

Question i have my k40 up and running i have engraved some lettering as a test but now i want to cut out a part that i have scand i have saved it as an dxf file and impoted it into corel draw but can't figure out how set the file as a cut out looked all over utube no video i could find showing how to do this thanks for any help



Dennis 





**"Dennis Fuente"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 23, 2016 23:28*

Hi Dennis. If you are using the CorelLaser plugin for CorelDraw, I will post a picture to show how to get it to cut instead of engrave. Bear with me, I'll take a few screenshots & then post to my Google Drive so you can see them.


---
**Dennis Fuente** *March 23, 2016 23:30*

Hey thanks a bunch i have been driving my self nut's trying to figure this out realy appreciate it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 23, 2016 23:35*

Okay, here's an image that shows which button on the CorelLaser plugin to click to open the Cutting Dialog



[https://drive.google.com/file/d/0Bzi2h1k_udXwRFhOSVJCb1BfQTA/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwRFhOSVJCb1BfQTA/view?usp=sharing)



Note: You will see in this example that for the Square I am cutting, I have made the square black fill, with no border. This works for solid objects so that it only cuts the line once.



If the item you want to cut has a border of anything greater than 0.01mm then you end up with the laser attempting to cut the line twice (once for both sides of the line). You don't really want this, as it causes extra burn on that area.



Check through some other posts on this group & you will find information about the cutting of lines/shapes & borders/solid fill issues.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 23, 2016 23:37*

**+Dennis Fuente** You're welcome. Any more questions regarding cutting, post it here in these comments & I will reply when I've got time (or someone else will).


---
**Dennis Fuente** *March 23, 2016 23:44*

Yuusuf

Thanks i will give it a try was also wondering if i were to scan an image in and wanted to cut it out how do i get the laser to perform this thanks again



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 24, 2016 00:02*

In regards to scanning & cutting an image out, you need to be aware of a couple of things. Firstly, apologies if my explanation is very long.



So first thing to be aware of is that the laser will cut all Black shapes (that you have selected) along the outline of the black. I'll draw a quick image to show what I mean & link it here: [https://drive.google.com/file/d/0Bzi2h1k_udXwamQ2Q085SUZ5eFk/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwamQ2Q085SUZ5eFk/view?usp=sharing)



Secondly, the other method is to set the fill to "none" or "white" & set a border of black with 0.01mm thickness. The laser will cut along those lines. Again, I have drawn it in that same image to show what I mean.



Hopefully, that makes sense.


---
**Dennis Fuente** *March 24, 2016 00:12*

thanks yuusuf

i will try and wrap my head around this



Dennis


---
**Dennis Fuente** *March 24, 2016 01:11*

Yuusuf 

I have had the same problem where i to make the outline very thick in order for the software to read it , so i f turn the back round black and leave the part color alone and tick the cut to the inside it should cut the part i scanned  have i got that right .

Thanks

Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 24, 2016 01:32*

I am unsure as to whether the format you are using imports as vector lines or raster pixels. If it imports as vector lines you can change the line thickness in CorelDraw itself to 0.01mm. But, if using the solid fill color method (with black backgrounds) the laser will cut along all edges between black & white. My suggestion is any shapes that are inside other shapes (e.g. Circle inside square) you make them the opposite colour of the outside shape. I.e. Square will be black background & circle will be white background. If you have shapes inside shapes inside shapes (e.g. Triangle inside circle inside square) you would do same. I.e. Square is black, circle is white, triangle is black. When cutting you need to make sure that all shapes are selected before you tell it to cut. It's a fiddly process to set up your designs, but once you work it out it will be easy. If you could upload a sample of what you're trying to do to your google drive & share the link, I'd be happy to take a look & try sort out a file to show you how it needs to be set up.


---
**Dennis Fuente** *March 24, 2016 16:07*

Yuusuf

Thanks i have not done a goggle drive upload yet just never needed it i will give it a try thanks for the help i am going to try and figure out this cut thing myself first and then if i fail i will ask for your help in figuring it out.



Dennis  


---
**Dennis Fuente** *March 24, 2016 22:10*

Yuusuf 

So i got the machine do a cut just a simple box with a circle inside the machine started cutting the circle but did not cut all the way through the material i am still getting used to the power setting so what i did was set the cut file to 3 passes what i found is that the machine dose one pass and then i have to release the next pass before it will go again ok so i kept trying to cut out the circle increasing the power setting each time well i never got the circle cut out what happened was the software down in the lower left corner stated data not correct so i take it that the software crashed is this what you have to deal with and i never got it to cut the outter box i will try it again many thanks.



Dennis  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 24, 2016 22:27*

**+Dennis Fuente** Hi Dennis. I've never had that issue with the software saying "data not correct" before.



In regards to cutting, first thing I recommend is to check the orientation of the lens. Mine came upside down & made it very difficult to cut through anything. Even 10 passes it would not cut. Once I put it the correct way, I could get through in 1-2 passes, depending on the material thickness.



Check this post of mine regarding the lens orientation: [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/SKB2gqE8WmE](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/SKB2gqE8WmE)



Next, I would recommend making sure your mirrors are as well aligned as possible (for all four corners, starting with Top Left, Bottom Left, Top Right, Bottom Right). If the are all hitting in the exact same spot then you are good. If not, you will have power drops the further the laser head moves away from the Top Left position.



Also, some form of air assist (to blow out flames & blow smoke away from the Lens) is useful. It makes cutting a lot easier by keeping the Lens clean from soot. If the lens gets dirty, you will find power drops again.



I would suggest for your first test runs of cutting, use some kind of thick paper or thin cardboard as your material. It is easy to cut through & will give you quick results for checking if it is working. Usually something in the range of 6mA power & 20mm/s cutting speed should get through thick paper/thin cardboard.



Then work your way up to different thickness & materials. I suggest you keep track of your power/speed settings you use for future reference. I've made a few "power/speed" reference pieces out of leftover/scrap materials & tested numerous different power/speed combinations on them. That way I have a visual representation of what happens at certain power/speed. e.g. this is my reference for cutting through 1-2mm thick leather ([https://drive.google.com/file/d/0Bzi2h1k_udXwWmFJYkdDVWtCSHM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWmFJYkdDVWtCSHM/view?usp=sharing)).



Any more questions, feel free to ask & I'll try help to the best of my knowledge. I'm still learning a lot of different things myself.


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/1N5Hd7QHWsK) &mdash; content and formatting may not be reliable*
