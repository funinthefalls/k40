---
layout: post
title: "Interesting development. I received today this video from the manufacturer sent by the engineer regarding my tube issue"
date: February 17, 2017 11:50
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Interesting development. I received today this video from the manufacturer sent by the engineer regarding my tube  issue. It seems like its focusing the lens on the tube?



Doesn't it look a little terrifying???



Still i dont know what to do :(


**Video content missing for image https://lh3.googleusercontent.com/-muvX_DIDyCU/WKbjb2XhppI/AAAAAAAB1Mk/zrOP6gSpqTwMe9gKzgve--mzayjW1eCIwCJoC/s0/laser%252Bpath%252Badjustment.mp4**
![images/cc99d7c116e67b9590281fc52b30d356.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc99d7c116e67b9590281fc52b30d356.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Andy Shilling** *February 17, 2017 12:02*

Health and safety at it's best.


---
**Ned Hill** *February 17, 2017 12:24*

Wow


---
**Roberto Fernandez** *February 17, 2017 12:28*



You just have to see how the wall has holes.


---
**Philipp Tessenow** *February 17, 2017 13:25*

Yeah, like the holes in the wall 😬


---
**Don Kleinschnitz Jr.** *February 17, 2017 14:49*

Those bricks are cut for 10,600 nm response I suspect:)

 I guess you just start turning stuff till it works.....


---
**Ariel Yahni (UniKpty)** *February 17, 2017 15:06*

Interesting info regarding this at the FB group [https://www.facebook.com/groups/441613082637047/permalink/830842153714136/](https://www.facebook.com/groups/441613082637047/permalink/830842153714136/)


---
**Don Kleinschnitz Jr.** *February 17, 2017 15:10*

**+Ariel Yahni** can't see it I left that group ... got overwhelming :(




---
**Ariel Yahni (UniKpty)** *February 17, 2017 15:12*

**+Don Kleinschnitz**​ I get you. Just disable notification.


---
**Ned Hill** *February 17, 2017 15:17*

Lol "on the piss", I like that description of the tube in the FB post. It's a UK thing apparently **+Ariel Yahni**.


---
**Ariel Yahni (UniKpty)** *February 17, 2017 15:18*

Made me laugh a lot. I did not know that was a possible description but make sense and is good info for referencing. If your plasma looks like a drunk man piss then something is bad


---
**Don Kleinschnitz Jr.** *February 17, 2017 15:26*

**+Ariel Yahni** if your research is done I would start adjusting the screws a small amount keeping careful track of how much you turn them. Maybe put a dot on the screw with "white out" to show location. 

I would turn each screw in a sequence around the circumference followed by a pulse of laser to a target. If that adjustment does not work put the screw back in the original position and go to the next screw.

If nothing works going from screw to screw in one sequence reverse the sequence and try again.

Looking at their test setup I am guessing that there isn't much precision needed. 

Another option is to turn laser on continuous @ low power and tweak screws one at a time watching target. Careful of cooling!


---
**Ariel Yahni (UniKpty)** *February 17, 2017 15:31*

I'm waiting until tomorrow and see if a get a explanation from this manufacturer as what exactly they are doing. 



**+Don Kleinschnitz**​ thanks for the test process


---
**Paul de Groot** *February 17, 2017 23:25*

Happened to me today. No laser beam but full glow in the tube. 😢


---
**Don Kleinschnitz Jr.** *February 17, 2017 23:27*

**+Paul de Groot** the same kind of tube? Is it a ionization virus lol.


---
**Paul de Groot** *February 17, 2017 23:30*

**+Don Kleinschnitz** yes looks like it and just now i have to produce a lot of materials for the makerfaire. My tube doesn't have screws at the end but just a cap


---
**Ariel Yahni (UniKpty)** *February 17, 2017 23:35*

Whats happening!!!!!!!!! Over at FB they are giving cero chances on getting the tube working.


---
**Paul de Groot** *February 17, 2017 23:50*

**+Ariel Yahni** ohhh no. I was just thinking if I could take off that cap. No screws and i will post a photo when i get home.


---
**Ariel Yahni (UniKpty)** *February 17, 2017 23:52*

Follow my link to the Facebook group and you'll understand some of the technical details regarding this issue


---
**Ariel Yahni (UniKpty)** *February 18, 2017 03:03*

Interesting 

![images/735b09f46ecf90149b5fd3573775307a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/735b09f46ecf90149b5fd3573775307a.png)


---
**Don Kleinschnitz Jr.** *February 18, 2017 03:06*

**+Ariel Yahni** yah pull out your metal laser cap rotation tool and adjust.


---
**Ariel Yahni (UniKpty)** *February 18, 2017 03:13*

![images/8ccfbb8217f379ab53421611e19848f9](https://gitlab.com/funinthefalls/k40/raw/master/images/8ccfbb8217f379ab53421611e19848f9)


---
**Claudio Prezzi** *February 18, 2017 08:56*

I would let the suplyer replace the tube! They seem to have serious quality problems.


---
**Mircea Russu** *February 18, 2017 14:35*

Second video... Not even a pair of gloves... looool. Where can I find those laserproof, heatproof, voltage proof fingers with +90 Luck? And those are some pretty big tubes he's got there.


---
**Don Kleinschnitz Jr.** *February 18, 2017 14:59*

**+Mircea Russu** not that I am endorsing touching it naked handed but that end of the tube is very low voltage :).


---
**Ariel Yahni (UniKpty)** *February 18, 2017 15:05*

Well this guy keeps telling me to rotate that cap


---
**Don Kleinschnitz Jr.** *February 18, 2017 15:13*

**+Ariel Yahni** whats the down side to trying it. Not working anyway. Make sure that that end is really gnd. I would even disconnect from meter and connect gnd directly.


---
**Paul de Groot** *February 19, 2017 08:32*

**+Ariel Yahni** did you manage to get it to work again?i have given up on it and ordered a nee tube 😢


---
**Madyn3D CNC, LLC** *February 20, 2017 20:20*

omg that video from the mfg...  I'm guessing they don't do workmans comp/insurance plans at that factory lol.. 



So you're getting Co2 excitment inside the tube indicated by the visible purple beam, but nothing for laser output? Sounds quite similar to my current nightmare I've been dealing with....    
{% include youtubePlayer.html id="gVpyHNwwy3o" %}
[youtube.com - K40 issue- arcing and inadequate laser output](https://youtu.be/gVpyHNwwy3o)




---
**Ariel Yahni (UniKpty)** *February 20, 2017 20:28*

**+Madyn3D CNC, LLC** Factory says the i only need to rotate the cap, but the screws are so tight im having trouble


---
**Madyn3D CNC, LLC** *February 20, 2017 21:45*

**+Ariel Yahni** Ahh, yea judging by the video they sent you, I can see in the background they sure like using their lock-tite!


---
**Don Kleinschnitz Jr.** *February 20, 2017 22:40*

**+Ariel Yahni** if you haven't already try:

1. Acetone

2. Heat the screw with a soldering iron


---
**Paul de Groot** *February 20, 2017 22:42*

**+Don Kleinschnitz** my outputlens is glued on and i wonder how to remove it. I tried scrapping the glue away with razor blade but it is still stuck on. Any suggestions? 


---
**Ariel Yahni (UniKpty)** *February 20, 2017 22:47*

**+Paul de Groot**​ we are going were no man has gone before :)


---
**Don Kleinschnitz Jr.** *February 20, 2017 23:19*

**+Paul de Groot** does yours have screws or just a metal ring that is glued? I think the permanent ones are put on with epoxy :(.


---
**Paul de Groot** *February 20, 2017 23:28*

**+Don Kleinschnitz** just a copper ring. I don't think its possible to remove it. It's just that I'm in such a time pressure to complete things. I've ordered  a new tube anyway with expedited delivery but still slow from china sigh 


---
**Paul de Groot** *February 20, 2017 23:29*

**+Ariel Yahni** yes it feels like a crazy adventure. If i wasn't such in a rush i would just wait for the new tube to arrive. 


---
**Don Kleinschnitz Jr.** *February 21, 2017 00:04*

**+Paul de Groot** UGGH!


---
**Paul de Groot** *February 21, 2017 00:06*

**+Don Kleinschnitz** yes i should be just patient, throw away the old tube and wait for the new one, sigh


---
**Ariel Yahni (UniKpty)** *February 25, 2017 16:44*

Removed screws but the end cup does not rotate, at least with hand pressure

![images/b6ad2f94f7e04235fa81db193aa50fba.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6ad2f94f7e04235fa81db193aa50fba.jpeg)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/UMspRTvrTaw) &mdash; content and formatting may not be reliable*
