---
layout: post
title: "So I was thinking about getting this to add to the end of my exhaust system to add a little more pull as there is about 12 feet from the machine to the exhaust port but I don't know if it will be enough any other ideas I"
date: February 23, 2016 20:50
category: "Discussion"
author: "3D Laser"
---
So I was thinking about getting this to add to the end of my exhaust system to add a little more pull as there is about 12 feet from the machine to the exhaust port but I don't know if it will be enough any other ideas I could use 



[http://m.homedepot.com/p/Suncourt-Inductor-4-in-Corded-In-Line-Duct-Fan-DB204C/206584727](http://m.homedepot.com/p/Suncourt-Inductor-4-in-Corded-In-Line-Duct-Fan-DB204C/206584727)





**"3D Laser"**

---
---
**Jim Hatch** *February 24, 2016 02:26*

Based on your other post where the smell of burning plastic is an issue I think you need something bigger. 65CFM to 80CFM isn't much different than what you've got with the stock blower. With that stretch of pipe you likely need some sort of HVLP blower/fan. We have a similar length in our Makerspace so I can check to see what we've got for a blower on the big laser because it doesn't stink things up too much. My K40 is setup in the garage and has a short hose so it's not a big problem for me (but I've also added 4 small fans to the front panel to pull air in from the garage and provide more air volume for the exhaust fan to extract).


---
**ThantiK** *February 24, 2016 06:59*

It needs to be a squirrel cage fan, like the one that came with the laser.  Double your cfm at least. 


---
**Bob Steinbeiser** *February 24, 2016 14:51*

I just bought one of these in-line fans, its not even as powerful as the cheapo fan that comes with the K40. It's definitely going back!


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/X57cDAX3oJe) &mdash; content and formatting may not be reliable*
