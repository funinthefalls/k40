---
layout: post
title: "Got my 40W machine running for the first time yesterday"
date: February 01, 2016 13:39
category: "Software"
author: "William Klinger"
---
Got my 40W machine running for the first time yesterday. Using CorelLaser and Corel Draw. Everything is coming out 1/4 size. A 2" wide, one inch high logo is 1/2" wide and 1/4" high.  Anyone run into this before? 

Thanks for any help,  WK





**"William Klinger"**

---
---
**Anthony Bolgar** *February 01, 2016 16:54*

Make sure you have the right board selected in the CorelLaser plugin


---
**William Klinger** *February 01, 2016 18:34*

Thanks, it is set correctly but I have not tried other board settings yet. That was the first place I looked after watching a couple of the YouTube videos. WK


---
**Scott Marshall** *February 02, 2016 03:20*

In LaserDRW Go into "engrave", and then at the lower right there is "laser head step by" it comes with a default setting of .1mm, change it to 1.0mm.



Worked for me.

Also make sure your work area is set to 300 x 200



Drove me nuts for a couple days sorting that one out. 

I've found nothing useful for instructions on either LaserDRW or CorelLaser



Speaking of that ...  maybe you can help me?



I can't get my Corel Laser working, It starts, goes through the key verification, then gives me an "CorelLaser not installed" . I tried reloading it several times, and it searches for a missing website on the 1st cold boot, then errors after that. Any ideas?






---
**William Klinger** *February 02, 2016 03:45*

I installed both Corel Laser and LaserDRW.  The first time I ran LaserDRW with the dongle plugged in, I got a warning which I over-rode. It connected to a Chinese site that advertised parts, etc. Once I closed that, every time I start Corel Laser now, the LaserDRW logo comes on the screen first, then the Corel Laser starts as an add-on to Corel Draw. WK


---
**I Laser** *February 02, 2016 04:16*

That's crazy, I've never had the software try to connect to a web site. Maybe that was the virus that's been mentioned, it's definitely what I'd consider malware.



If it wasn't so big I'd upload the version of the software I'm using. 


---
**William Klinger** *February 02, 2016 04:24*

I checked and the setting for "Laser Head step by" was 0.000. I changed it

to 1.00 but am still only getting 1/4 size of what ever is on the screen.

That setting is for moving the image around with the left-right-up-down

buttons below the setting.  WK


---
**Scott Marshall** *February 02, 2016 13:20*

Sorry about that, William.

 I had several problems at start up, and position was indeed one of them. I'll try totretrace my steps and see if it jogs my memory as to what was causing the shrinkage. I really have to start a log for this thing.



I laser. The site it attempts to open looks to be a part of the liyuha (or whatever it is) that does the security dongle. It comes on as an all chinese site for a few seconds, then it jumps to the standard "website not found" screen. Once done, it won't repeat unless I wipe CorelDraw and load fresh. I think it's looking for updates.



Is the supplied software a plug-in only, or a complete version of Corel Draw?



I don't have Corel Draw, and if it's a plug in to run the laser, that woud explain it. 



I looked for a free version of Corel Draw to test my theory, but didn't find a legitmate one. 



I know very little of Corel Draw, and any  help would be appreciated. It' very clumsy and limiting trying to operate thru LaserDrw as Willaim and I can attest.






---
**Jim Hatch** *February 02, 2016 22:14*

**+William Klinger**​ Not to state the obvious but are you using the right units? Corel often is set for inches but LaserDRW works in mm.



Also, there are 2 machine specific entries you need to set in the LaserDRW machine setup or config screen. The first is at the top and is the model of the board (silk screened or printed on the board) & is likely to be the only something-M2 option available in the drop down list. The second is the serial # of the machine & is the field on the bottom of the config screen. You'll find that one usually printed in black on a white label or background on the control board and is an alphanumeric of 10-16 characters (IIRC). There are no dropdown field entries but it likely has something entered already when you first install it and it's likely wrong.


---
**William Klinger** *February 03, 2016 00:31*

I went in and changed the units from inch to metric and saved the file as a new version. Still getting the same more or less 1/4 size from the laser output. I tried a couple of files with the same results. It is almost like the stepper drive is using the wrong steps per unit setting, except there is no way I can see to change that.  Even with everything set to metric, the 'dpi' stays 300.  WK


---
**Jim Hatch** *February 03, 2016 01:24*

**+William Klinger**​ Are you saying in LaserDRW File|Device Initialize the Resolution field (3rd down on left - at least on mine) won't let you enter something else like 400 or 1000? If it lets you enter that do you click Apply and then Ok? I do that, come back in and it's 400.



You can get to the same initialization screen by clicking Engrave|Device Initialize. 



In the Properties Panel on the far right I have 0.100 mm set for Step when i n the main drawing view (not the Engrave window). If you don't see that panel it's displayed by clicking View|Properties Panel (or F6).



And you verified that the Main board setting in the config is the same as on your controller (the board the USB cable plugs into, not the one under the power supply)? Mine is a 6C6879-LASER-M2.



The Device ID field is the one I said matches the serial # on the controller board.



BTW, the "Laser head step by" that you changed from 0 to 1 is used to manually move the head using the arrow buttons under the field (I believe). Mine is set at 0 because I don't move the head that way when setting up a cut (I use the Refer-X and Refer-Y parameters).



I'm using LaserDRW 2013.02




---
**William Klinger** *February 04, 2016 02:14*

Further tests sending a 1" OR 25.4mm square from Corel to the engraver both results in a 15mm square being cut. Changing the resolution moves the head but the square tests still cut 15mm on a side.  WK


---
**Jim Hatch** *February 04, 2016 03:07*

Are you using the CorelLaser software? The version of Corel with the plugin for the laser so you don't have to save as a BMP in Corel & then import into LaserDRW?



What happens if you just open LaserDRW and draw a 25mm square and engrave that? If that works then your problem will be in the Corel software. If it doesn't work then the problem is likely somewhere in the machine config. 


---
**William Klinger** *February 04, 2016 04:59*

Hello Jim,



Yes, I am trying to use the CorelDraw12 they sent with the machine.



I tried drawing a 25mm box in LaserDRW and after putting the board serial

no. into the configuration screen, it engraved a perfect 25mm. box.



I guess there is something amiss with the CorelLaser configuration.

Everything in both the engrave and machine configuration windows is now set

the same but the CorelLaser is producing a 3/5 size file from the drawing

screen.



Thanks for the help,  W. Klinger


---
**Jim Hatch** *February 04, 2016 13:30*

**+William Klinger**​ that's good news. It means the laser cutter is okay. I can't help with the Corel plugin because I couldn't install it on my PC due to the virus my install file had. Someone else who is successful in using the Corel solution might be able to help further. I go from Adobe Illustrator or Inkscape to BMP to LaserDRW. A little kludgy but it doesn't seem to slow me down much but that could be because I don't know what I'm missing :-)


---
**Bob Steinbeiser** *February 15, 2016 17:29*

**+William Klinger**, Did you find a solution yet?  I have the same problem.  Corel output is about half scale while LaserDRW works perfectly.  Keep thinking it must be a Corel setting but can't find anything that works!




---
**Bob Steinbeiser** *February 18, 2016 16:26*

I finally found the problem!  I had the resolution set wrong under "Engraving Machine Properties", this should e be set 


---
**Tony Sobczak** *February 19, 2016 08:36*

Lots of good info here for us noobies. 


---
**Tony Sobczak** *February 20, 2016 09:18*

Where did you find 'Engraving machine properties'  **+Bob Steinbeiser**​?  I've checked everything suggested and it's still engraving smaller than it should. I have a 50mm insignia that engraves @ at 25mm. Driving me crazy.  I've only had it 3 days now. 


---
**Bob Steinbeiser** *February 21, 2016 22:17*

Hey Tony, Its the gear icon in Corellaser, or find it in LaserDRW under 'Engrave' on the tool bar, at the bottom 'Device Initialize'.  Hope this helps!


---
**Mike McConnell** *November 03, 2016 20:49*

Set resolution to 1000dpi in device initialize. Works great now!!!


---
*Imported from [Google+](https://plus.google.com/107701048118958946399/posts/i9Lx5MsnDUm) &mdash; content and formatting may not be reliable*
