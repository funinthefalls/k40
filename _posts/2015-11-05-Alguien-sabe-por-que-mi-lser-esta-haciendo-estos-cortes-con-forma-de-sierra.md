---
layout: post
title: "Alguien sabe por que mi lser esta haciendo estos cortes con forma de sierra?"
date: November 05, 2015 22:12
category: "Hardware and Laser settings"
author: "Al Tamo"
---
Alguien sabe por que mi láser esta haciendo estos cortes con forma de sierra? Al principio no lo hacia, algo debe estar poco tenso o desgastado, pero no se lo que puede ser.



OS agradecería la ayuda.

![images/0119e3d112b5c417194bcf1a3c909419.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0119e3d112b5c417194bcf1a3c909419.jpeg)



**"Al Tamo"**

---
---
**Peter** *November 06, 2015 15:49*

Maybe a loose mirror or lens?

Maybe belt to loose/tight?


---
**Al Tamo** *November 06, 2015 15:52*

Mmmmm, interesante, podría ser un espejo .... Lo comprobaré, gracias por la idea


---
*Imported from [Google+](https://plus.google.com/112166512335650872249/posts/554ESmic2T9) &mdash; content and formatting may not be reliable*
