---
layout: post
title: "Hi, can anyone confirm if this is ok to use on the cables on the tube ?"
date: December 18, 2017 12:17
category: "Discussion"
author: "Frank Farrant"
---
Hi, can anyone confirm if this is ok to use on the cables on the tube ?

Servisol Silicone Adhesive Sealant:



Ready to use, one part•	Neutral cure, non-corrosive•	Suitable for use on metals and plastics•	Excellent insulating and sealing properties•	Prevents arcing on EHT connections•	Tack free in 40 minutes•	Operational from -50°C to +230°C•	Ideal for potting electronic components and keeping gaskets in place•	Supplied in a 75ml tube





**"Frank Farrant"**

---
---
**Steve Clark** *December 18, 2017 17:43*

I just read this link, see if it helps you decide if it is ok to use.

[electronics.stackexchange.com - insulation - Can silicone sealant be used to insulate and waterproof electronic components? - Electrical Engineering Stack Exchange](https://electronics.stackexchange.com/questions/32798/can-silicone-sealant-be-used-to-insulate-and-waterproof-electronic-components)


---
**Don Kleinschnitz Jr.** *December 19, 2017 13:45*

**+Steve Clark** 

#K40Silicon


---
**Frank Farrant** *December 20, 2017 15:12*

Seems to work well.  No arcing.  


---
**Don Kleinschnitz Jr.** *December 20, 2017 16:03*

**+Frank Farrant** are you saying that you did nothing else but put new silicon on it and now its not arching?



Is your machine running again?


---
**Frank Farrant** *December 20, 2017 21:44*

I changed the tube and used the Silicone I bought and all is great now.  Adjusted and cleaned the mirrors and she cuts like a dream.

Many thanks for your help over the last few days.




---
**Don Kleinschnitz Jr.** *December 20, 2017 21:51*

**+Frank Farrant** I am happy for you but shocked that the arc was simply the silicon?? 

Never cease to be amazed at the K40....


---
**Frank Farrant** *December 20, 2017 21:58*

**+Don Kleinschnitz** I think the tube that was arching had a fault as after I removed the cables from it, I could see that the round metal clip under the Anode was loose.  It's on its way back to the supplier for a refund.  


---
**Don Kleinschnitz Jr.** *December 21, 2017 21:05*

**+Frank Farrant** Ah, I missed that you put the other tube back. Now it at least makes a bit more sense.

Could you try and take a photo of the portion you thought was a fault. We can use for future reference.


---
*Imported from [Google+](https://plus.google.com/110419041310362555971/posts/GJ2TGNWH9Vp) &mdash; content and formatting may not be reliable*
