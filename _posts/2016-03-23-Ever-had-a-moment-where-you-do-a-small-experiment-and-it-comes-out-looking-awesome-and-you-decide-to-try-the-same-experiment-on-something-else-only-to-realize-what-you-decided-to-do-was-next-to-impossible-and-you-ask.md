---
layout: post
title: "Ever had a moment where you do a small experiment and it comes out looking awesome and you decide to try the same experiment on something else only to realize what you decided to do was next to impossible and you ask"
date: March 23, 2016 07:28
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
Ever had a moment where you do a small experiment and it comes out looking awesome and you decide to try the same experiment on something else only to realize what you decided to do was next to impossible and you ask yourself, 'What was I thinking?!'



The story is that I tried to make the little heart first. It's about 60x60mm. I covered the wood with painter's tape first, engraved, then I airbrushed it red before removing the tape. Neat huh? So I thought I'd try it on one of my koozie designs. Boy that was the wrong thing to pick. All of the itty bitty bits and pieces of tape that I had to peel off. Urgh ... BUT, it still looks pretty cool!



I also did a test with cutting the pieces different, one face up and one face down. You can clearly see the difference. Some folks that have seen some of the koozies when I get my morning coffee tell me they prefer the darker lines on the face, and others prefer the thinner lines so the graphic pops more. Either way, this coloring part is not be a process I'll be doing very often.



![images/ee55ac0e3b47a0d423aa35190eb060a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee55ac0e3b47a0d423aa35190eb060a9.jpeg)
![images/aa0d1acfdf979fca41634b6d051129dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa0d1acfdf979fca41634b6d051129dc.jpeg)
![images/9061231de8a955a22b0b6525052212f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9061231de8a955a22b0b6525052212f0.jpeg)
![images/952d45bbdbe2c2c50288e89b74a231bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/952d45bbdbe2c2c50288e89b74a231bd.jpeg)
![images/a72f6ff148662507e09dafbd8074eae3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a72f6ff148662507e09dafbd8074eae3.jpeg)
![images/c10a28643233fc803c810a8aced0fd8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c10a28643233fc803c810a8aced0fd8c.jpeg)
![images/d3a7d53cd6d5f84da052d3e6a39f9944.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d3a7d53cd6d5f84da052d3e6a39f9944.jpeg)
![images/327154bea4a21a0a0dfba781f382f174.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/327154bea4a21a0a0dfba781f382f174.jpeg)
![images/3588577db9f6cde564ca5ae51d44ab5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3588577db9f6cde564ca5ae51d44ab5b.jpeg)
![images/5f03aaf351c830159ae38fce28a4552b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5f03aaf351c830159ae38fce28a4552b.jpeg)
![images/a494192d6652c89f3cc66c13908edc9f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a494192d6652c89f3cc66c13908edc9f.jpeg)
![images/77b9d35b1193a530bdd83eb1514b0b15.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/77b9d35b1193a530bdd83eb1514b0b15.jpeg)
![images/2335297870023349b56a3374140dacec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2335297870023349b56a3374140dacec.jpeg)
![images/27cafffbb2cb2f2379ec68cb62906a24.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/27cafffbb2cb2f2379ec68cb62906a24.jpeg)

**"Ashley M. Kirchner [Norym]"**

---
---
**Stephane Buisson** *March 23, 2016 08:13*

Nice work/experiment, well done

-living hinges

-hight detailed masking/painting



thank **+Ashley M. Kirchner** for posting


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 23, 2016 09:03*

That's really nice Ashley. I will have to try this method out in future for some pieces.



edit: I have considered using low-tack contact adhesive, like the stuff people cover kid's school/text books with. Not sure how the adhesive will go when engraved though, so a test is in order.


---
**Scott Thorne** *March 23, 2016 13:09*

Great work man. 


---
**Ashley M. Kirchner [Norym]** *March 23, 2016 14:10*

**+Yuusuf Sallahuddin**​, keep in mind that that stuff is plastic and could create bad fumes. What I use is a paper tape. When I engrave it, it burns and smells like someone is smoking nearby. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 23, 2016 15:29*

**+Ashley M. Kirchner** That's a point I didn't consider. Thanks for mentioning it.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/Zhv4Qt92s8B) &mdash; content and formatting may not be reliable*
