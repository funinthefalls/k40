---
layout: post
title: "So I am dealing with my vendor on this issue, but thought I would send it out to the greater group for additional input"
date: October 22, 2016 13:26
category: "Discussion"
author: "Bradley Blodgett"
---
So I am dealing with my vendor on this issue, but thought I would send it out to the greater group for additional input. I've been using this cutter for about 4 months without too much issue. The other day the laser totally stopped working. The machine moves, but the current meter does not move when the laser is supposed to be firing. I did notice that the fan on the power supply was jammed when I opened it up, so I expect that might have been the issue. I uploaded a video at the vendor's request. Luckily I have 2 years of warranty on the unit.


{% include youtubePlayer.html id="kuphjq0IPIE" %}
[https://youtu.be/kuphjq0IPIE](https://youtu.be/kuphjq0IPIE)





**"Bradley Blodgett"**

---


---
*Imported from [Google+](https://plus.google.com/+BradleyBlodgett/posts/XGrGYXP4mmV) &mdash; content and formatting may not be reliable*
