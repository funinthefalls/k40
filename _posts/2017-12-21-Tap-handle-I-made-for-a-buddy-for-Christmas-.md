---
layout: post
title: "Tap handle I made for a buddy for Christmas "
date: December 21, 2017 17:14
category: "Object produced with laser"
author: "David Allen Frantz"
---
Tap handle I made for a buddy for Christmas 🎄. Hardware will be here tomorrow. 

![images/bb6c9f53de94680084c5862723e7790b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb6c9f53de94680084c5862723e7790b.jpeg)



**"David Allen Frantz"**

---
---
**Ned Hill** *December 22, 2017 00:01*

Very cool.


---
**David Allen Frantz** *December 22, 2017 03:44*

Thanks Ned 


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/9waqAYE7mBf) &mdash; content and formatting may not be reliable*
