---
layout: post
title: "Has anyone had to fill out an \"ISF 10+2\" to import their machine from China via sea?"
date: March 09, 2016 00:00
category: "Material suppliers"
author: "Stuart Rubin"
---
Has anyone had to fill out an "ISF 10+2" to import their machine from China via sea? The seller says I need to do it, but I'm not sure, and definitely don't know how to do it!





**"Stuart Rubin"**

---
---
**HalfNormal** *March 09, 2016 13:45*

I had this issue with a 3d printer. I filled out the paperwork and when it came, it had already been done. The shipper had taken care of it.


---
**Stuart Rubin** *March 09, 2016 18:11*

HalfNormal, was your machine shipped by boat, a "consumer" carrier (DHL, UPS, etc.) or some air carrier? All the fees imposed by the "customs agent" are adding up to hundreds of dollars (on top of the steep shipping fee itself).


---
**HalfNormal** *March 09, 2016 21:06*

It was FedEx who contacted me. The shipping was free. No other fees were assessed. 


---
*Imported from [Google+](https://plus.google.com/104069580748008438325/posts/XfBjHu1Q96T) &mdash; content and formatting may not be reliable*
