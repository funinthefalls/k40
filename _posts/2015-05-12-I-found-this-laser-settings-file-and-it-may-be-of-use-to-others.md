---
layout: post
title: "I found this laser settings file and it may be of use to others"
date: May 12, 2015 21:11
category: "Materials and settings"
author: "Jim Coogan"
---
I found this laser settings file and it may be of use to others.  Helped me start my own.

![images/c732e377e84cafb8d68679e0e071a5f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c732e377e84cafb8d68679e0e071a5f3.jpeg)



**"Jim Coogan"**

---
---
**Stephane Buisson** *May 13, 2015 08:00*

interesting note about air (assist):

Acrilic : little air

MDF/Pli increase air volume



Does that mean it would be a parameter we would need to take in consideration during a hardware mod ?


---
**Jose A. S** *August 30, 2015 19:51*

Jim, I have cut acrylic 2mm thickness at 8m/s and 8mA.


---
**Jim Coogan** *August 31, 2015 00:54*

I would definitely take air assist into account during hardware mods.  It's one of those things I do recommend to people that do not have it on their machines.  It tends to allow you to cut faster and even knocks down little flame ups when working with something like MDF or Ply.


---
**Jim Coogan** *August 31, 2015 00:55*

I think I cut thin acrylic at about the same speed and power.  I also have a tendency to make two fast cuts instead of one big cut as the material gets thicker.  I get straighter through cuts without any undercut.


---
*Imported from [Google+](https://plus.google.com/+JimCoogan_CoogansWorkshop/posts/4gfHdoABrEZ) &mdash; content and formatting may not be reliable*
