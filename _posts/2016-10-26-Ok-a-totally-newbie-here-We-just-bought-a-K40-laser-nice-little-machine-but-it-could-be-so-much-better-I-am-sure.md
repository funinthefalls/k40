---
layout: post
title: "Ok, a totally newbie here. We just bought a K40 laser, nice little machine, but it could be so much better I am sure"
date: October 26, 2016 08:44
category: "Modification"
author: "Peter Jacobsen"
---
Ok, a totally newbie here. We just bought a K40 laser, nice little machine, but it could be so much better I am sure. 

So now I would like some suggestions from you guys, what should be a priority?



I am thinking about doing the following: 

Replace the controller board, getting the Smoothieboard instead. 

Is there a guide to replacing the board?



Doing something about the suction, it appears it takes up some of the cutting area. 

Mounting a honeycome buildplate. Any ideas where to get that from?



What about air assist, what is that, and what will it do for the laser?



Any other obvious things we should do?



Thanks in advance :-)



Peter



 





**"Peter Jacobsen"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2016 10:18*

Hi Peter. All your ideas for replacements are useful & handy to have.



There are a lot of posts regarding air-assist & what seems to be best (in the history for this community), so I'd suggest checking through them (either use the "modifications" category or do a search for "air assist"). Seems most people are having good luck on that front with what looks to be a large aquarium pump (can't remember the litres/min or gallon/hour on it).



Honeycomb can be got from [LightObjects.com](http://LightObjects.com) or even ebay (if I recall correct).



Plenty of help & assistance is available within this group (& also the LaserWeb/CNC Web group) regarding upgrading to the Smoothieboard. Also, if you're not as technically minded about electronics (like I'm not), Scott Marshall offers a kit to make it more plug & play for the upgrade (which I am using & a few others). Check [all-teksystems.com](http://all-teksystems.com) for that. Also, I think it is still pinned to the top of this group a post by Stephane showing "Blue Box Guide" which will link to a bit of a run down on upgrading to the Smoothie



The suction is horribly annoying & a lot of us have either cut it back a bit or totally removed that section from the K40. Some have even gone as far as to make nice 3d-printed mounts that replace it. Also, I've seen plenty of people replace their exhaust fan (the stock one is pathetically weak).



Other things to consider looking into for upgrade are the lens & mirrors. Better lenses & mirrors are available (I still haven't upgraded mine though) & can be found again at [LightObjects.com](http://LightObjects.com) or ebay (probably aliexpress too).



That's all I can think of for now.

[all-teksystems.com - all-tek-systems](http://all-teksystems.com)


---
**Ariel Yahni (UniKpty)** *October 26, 2016 11:40*

**+Peter Jacobsen**​ all said above is correct.  Here is a collection I have with details and link to some of the upgrades [plus.google.com - K40 Laser Cutter Upgrades](https://plus.google.com/collection/EndMTB) I highly recommend All Tek solutions for upgrading the board 


---
**Mark Kirkwold** *October 26, 2016 19:22*

Good summary here.  I would just add that as far as cutting ability the biggest bang for the buck is the air assist. 


---
*Imported from [Google+](https://plus.google.com/117580774600105315145/posts/4D5fwm8XVFL) &mdash; content and formatting may not be reliable*
