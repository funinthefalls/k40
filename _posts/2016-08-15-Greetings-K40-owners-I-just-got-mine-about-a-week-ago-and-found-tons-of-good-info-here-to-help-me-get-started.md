---
layout: post
title: "Greetings K40 owners! I just got mine about a week ago and found tons of good info here to help me get started"
date: August 15, 2016 21:07
category: "Hardware and Laser settings"
author: "VetGifts By G"
---
Greetings K40 owners! I just got mine about a week ago and found tons of good info here to help me get started. I lasered some wood already with some pretty good outcome. My question is there a way to get the machine to engrave up and down(along the y axis) instead of left and right along the x axis working down toward the front. I get better results if the K40 is not engraving with the wood grain, but there is not much room for the piece I'm trying to do, so it has to go along the y axis.





**"VetGifts By G"**

---
---
**Ariel Yahni (UniKpty)** *August 15, 2016 21:28*

I don't think it's possible (but there is a reason  why) but depending on the size you could remove the metal exhaust.  I can place now 12"x12".  


---
**Eric Flynn** *August 16, 2016 00:43*

Yes, there is a way.  In CorelLaser, there is a box with X<-->Y next to it, check that.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 16, 2016 01:29*

**+Eric Flynn** I never noticed that. I'll have to give it a try sometime too :) Thanks for pointing it out.


---
**Eric Flynn** *August 16, 2016 01:30*

I plan on making a document that explains CorelLaser in depth sometime soon.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 16, 2016 02:25*

**+Eric Flynn** That would be a handy resource for this community. Good idea :)


---
**VetGifts By G** *August 16, 2016 02:34*

I did the x<>y thing and it started engraving at the wrong spot, I will mess with it some more tmrow. It seems like it just flipped the work price 90 degrees in the software but the physical object was still where I placed it.


---
**VetGifts By G** *August 16, 2016 02:45*

It did go in the direction I want to engrave, just have to figure out how to center everything. Already wish I have the laser pointer upgrade, or some mad math skills!


---
**Eric Flynn** *August 16, 2016 03:36*

Placing a single pixel square (or a very very small one) at the reference position you want to use in your drawing will help you line things up properly.  That is a MUST when you are doing multiple tasks, or they will not register to one another.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 16, 2016 06:13*

Alternatively I place a rectangle around everything (with no fill & no border) & it also assists with lining up cuts/engraves/multiple tasks. The laser won't cut or engrave it, so it's better than the 1 pixel in my opinion.


---
**Eric Flynn** *August 16, 2016 06:41*

Yea, I do both.  Depends on what I am doing.


---
**Robi Akerley-McKee** *August 16, 2016 18:02*

I built a honeycomb bed for mine and then taped a piece of paper on the bed and cut a 220mm y at x0 and a 320mm X at y0. and took off the cut off paper. now I have a 0,0 point and lines on my bed.  0,0 for me is lower left corner.  Just ordered a MKS Sbase board to replace my Arduino 2560/RAMPS 1.4 controller.


---
**VetGifts By G** *August 16, 2016 18:51*

That make a lot of sense, I will try that! Thanks


---
*Imported from [Google+](https://plus.google.com/113361677380464824789/posts/5nT8yNGGLaN) &mdash; content and formatting may not be reliable*
