---
layout: post
title: "Mr. stephane director of department , I suggest you bring up there friends in other software easier to use each other's own files to share And used the experience to develop professionally friends"
date: June 12, 2016 00:18
category: "Discussion"
author: "Mr Shahrouz"
---
Mr. stephane  director of department , I suggest you bring up there friends in other software easier to use each other's own files to share

And used the experience to develop professionally friends





**"Mr Shahrouz"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 12, 2016 02:06*

Hi Mr Shahrouz. I'm not 100% certain what you are asking, as there seems to be a bit of a language translation issue here.



Are you suggesting that we share our design files? Or something else?


---
**Mr Shahrouz** *June 12, 2016 20:14*

Hello dear friend , you are correct just because I'm not fluent in the language you 'll use an interpreter .I think it 's important that the people establish a healthy relationship namely : the disappearance of the small boundaries


---
**Mr Shahrouz** *June 12, 2016 20:18*

I suggested that friends are better able to specialists within the group gathered together to share their files and use each other's experiences


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 13, 2016 01:24*

**+Mr Shahrouz** Hi again brother. There is a lot of work going on between some of the members of this group, where they share results of projects/material settings/project files. Most here would be happy to share their files/settings/etc in general, if you asked. Also, there is quite a lot of work going on with people upgrading their K40 controller electronics to different boards (Ramps, Arduino, Smoothie, etc.) & most of that is being documented/shared/discussed to ease the process for others who wish to do the same. It is very helpful to be able to use each others experiences to better the results of our own work.


---
*Imported from [Google+](https://plus.google.com/117220309894105053098/posts/LYpNNjGN7ep) &mdash; content and formatting may not be reliable*
