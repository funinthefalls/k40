---
layout: post
title: "Hello , first I introduce myself, my name is Alberto and I'm from Spain , so ask forgiveness for my bad English ."
date: July 15, 2015 21:20
category: "Hardware and Laser settings"
author: "Al Tamo"
---
Hello , first I introduce myself, my name is Alberto and I'm from Spain , so ask forgiveness for my bad English .

recently bought a laser cutter k40 , and I have a doubt, as you engrasais the machine? the x axis makes noise by rubbing and not whether to put oil, grease, etc.

Thank you for your help.





**"Al Tamo"**

---
---
**Bill Parker** *July 15, 2015 21:27*

I would use graphite power it will not collect dust like oil or grease but only use small amount.


---
**David Richards (djrm)** *July 15, 2015 22:54*

Greetings Alberto, I suggest that you try and identify the cause of the rubbing. Are you sure it is not the noise of the stepper motors during the home seeking function at power up? If in doubt then post a video recording for a second opinion. Kind regards, David.


---
**Al Tamo** *July 16, 2015 07:13*

**+Bill Parker** and the dry lubricant is safe? not damage the rubber or the bar?


---
**Al Tamo** *July 16, 2015 07:14*

**+david richards** ok David, I'll do it later. Thanks.


---
**Troy Baverstock** *July 16, 2015 07:31*

I will also do this next chance I get, I've always had a bit of a rough vibration that seems bad to me over 50mm/s. Have not had a chance to track it down but would love to know if it's K40 normal.


---
*Imported from [Google+](https://plus.google.com/112166512335650872249/posts/fyeHWBFgtQN) &mdash; content and formatting may not be reliable*
