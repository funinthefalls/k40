---
layout: post
title: "When using the corel laser plug in with coreldraw x6, the toolbar disappears after the job is finished and I have to close down coreldraw and open it back up to use it again"
date: March 08, 2017 11:46
category: "Software"
author: "Kustomizable Kreations"
---
When using the corel laser plug in with coreldraw x6, the toolbar disappears after the job is finished and I have to close down coreldraw and open it back up to use it again.  Does anyone have any ideas on how to fix this or doc the toolbar?  



Thanks 





**"Kustomizable Kreations"**

---
---
**Andy Shilling** *March 08, 2017 12:01*

Mine used to minimise to the bottom left of the screen, check the first. I think windows button and alt would bring it back up.


---
**Kustomizable Kreations** *March 08, 2017 12:02*

**+Andy Shilling**​, thanks I will check that.


---
**HalfNormal** *March 08, 2017 12:28*

**+Kustomizable Kreations** Actually it is on the bottom right next to the time and date area. Right click the icon to see the menu options.


---
**Kustomizable Kreations** *March 08, 2017 12:34*

+HalfNormal, thanks.


---
*Imported from [Google+](https://plus.google.com/+KustomizableKreations/posts/h7GBt2U98Nz) &mdash; content and formatting may not be reliable*
