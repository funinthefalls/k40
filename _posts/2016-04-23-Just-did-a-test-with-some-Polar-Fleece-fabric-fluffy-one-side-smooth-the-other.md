---
layout: post
title: "Just did a test with some Polar Fleece fabric (fluffy one side, smooth the other)"
date: April 23, 2016 02:02
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Just did a test with some Polar Fleece fabric (fluffy one side, smooth the other). This is the sort of fabric that jumpers/sweaters are made of.



I am making some pants, so wanted to add some decals to them. Thought the laser may help in cutting precision decals. Turns out pretty decent.



So here is a test of a simplified "Punisher" skull. Size is approximately 65 x 80mm. So even some of the tiny details (gaps between teeth etc) turn out very good.



Settings used were:



Power: 6mA

Speed: 25mm/s Cut

Passes: 1

![images/800bd202b278ded5bc3d2a9354edd067.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/800bd202b278ded5bc3d2a9354edd067.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**David Cook** *April 23, 2016 06:03*

Looks good


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/ZyfU4SWXsyn) &mdash; content and formatting may not be reliable*
