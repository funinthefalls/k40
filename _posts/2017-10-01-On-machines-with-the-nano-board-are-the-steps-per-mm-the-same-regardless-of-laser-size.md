---
layout: post
title: "On machines with the nano board, are the steps per mm the same regardless of laser size?"
date: October 01, 2017 19:33
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
On machines with the nano board, are the steps per mm the same regardless of laser size? I see no way of changing the steps/mm that the nano board outputs, so my assumption is all the lasers using that board would be the same. Am I correct, or is there a way to change the steps/mm?





**"Anthony Bolgar"**

---
---
**HalfNormal** *October 02, 2017 01:49*

All of the setting are in firmware and unavailable.


---
**Jason Dorie** *October 02, 2017 08:04*

Laser size wouldn't change the step rate as long as you use the same pitch diameter pinions on your update.  The bed size would change of course, but not steps/mm.


---
**Jason Dorie** *October 02, 2017 08:07*

Also, I'm going to start poking at this soon for LightBurn, at which point I can do scaling internally to whatever size you like.  :)  Been figuring out how to use WireShark, have done a few traffic captures, and started looking at the disk-based files.  They're bizarre, but hopefully won't take too long.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/CXhKDMLRMMZ) &mdash; content and formatting may not be reliable*
