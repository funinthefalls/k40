---
layout: post
title: "I found this cross joint that can be assembled and deassembled at Hope you find it useful for your projects"
date: March 26, 2018 19:04
category: "Repository and designs"
author: "Bernd Peck"
---
I found this cross joint that can be assembled and deassembled at [https://snijlab.nl/en/n/443/the-better-lap-joint](https://snijlab.nl/en/n/443/the-better-lap-joint)



Hope you find it useful for your projects

![images/f5ad8e03b924655b7956fe9763e0f492.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f5ad8e03b924655b7956fe9763e0f492.jpeg)



**"Bernd Peck"**

---
---
**Gunnar Stefansson** *March 27, 2018 11:06*

Wow thats a nice touch. Thanks for sharing Peter


---
**Fabiano Ramos** *March 27, 2018 13:42*

good idea dude, seems cool and functional.




---
*Imported from [Google+](https://plus.google.com/100238441539373475210/posts/2UYiodndvAU) &mdash; content and formatting may not be reliable*
