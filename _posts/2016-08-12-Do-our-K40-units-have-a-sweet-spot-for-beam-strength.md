---
layout: post
title: "Do our K40 units have a sweet spot for beam strength?"
date: August 12, 2016 17:12
category: "Hardware and Laser settings"
author: "Bob Damato"
---
Do our K40 units have a sweet spot for beam strength? I see some have adjustable tables for 'focus' etc.  Is my beam strength as good if I have a 1mm thick object vs a 10mm thick object? 





**"Bob Damato"**

---
---
**Eric Flynn** *August 12, 2016 17:37*

The sweet spot is the focal length. The delivered beam will carry the same amount of power regardless.  When its focused down, the power/area increases dramatically.   The amount of that power reaching a certain point in the material thickness will vary with focal length, and focus.  The beam is convergent up to the focal point, and divergent after , so the beam width is increased, and the power/area is decreased before, and after the focal point.  The amount of power that reaches the back side of a 10mm object, will be far less than a 1mm object , that is if you are cutting.   If you are engraving , it really doesnt matter unless you are trying to mark stainless.


---
**Jim Hatch** *August 12, 2016 18:17*

That's not exactly how lasers work. The laser is focused at a point approximately .02mm wide at a point a fixed distance from the lens. In most cases unless you've gotten a shorter focal length lens, that's 50.8mm from the center of the lens (or the base of the lens - it's not that significant a difference at the powers we're using).



That means the most concentrated power from the laser light will be at 50.8mm from the lens regardless of the material being engraved or cut. How much power is being delivered at that point depends on 2 factors - the power of the laser light itself (adjusted by the pot on the K40 control panel - usable power ranges from about 4ma to 20ma). The other factor is the speed of the head as it moves the beam across the material. The faster the travel, the less time the beam is focused on any single point and thus the lower the delivered power.



For the K40, 10mm stock is probably outside its ability to cut for wood. You can likely do it for foam or some plastics but may need multiple passes. Typical wood for cutting is 1/4" (6mm) or thinner.



 You can engrave any thickness material you can get in the housing as long as the surface can be in the 50.8mm focus area).


---
**Don Kleinschnitz Jr.** *August 12, 2016 21:33*

[http://www.parallax-tech.com/faq.htm#tem](http://www.parallax-tech.com/faq.htm#tem)




---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/58QkjQ3qu1a) &mdash; content and formatting may not be reliable*
