---
layout: post
title: "Sorry no idea what to do in G+ when you post to the wrong community"
date: June 01, 2016 02:50
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Sorry no idea what to do in G+ when you post to the wrong community. So I am repeating it here :).

-----------------------------

I am on my way to converting my K40 to a smoothie (once they are available again) and elected to get the middleman board (already arrived). The board requires a FFC connector and it turns out there are various connectors that can fool you (first time I have used FFC/FPC connectors).



So that you all do not have an ah s*&t like me...

----------------------

ITS EASY TO GE CONFUSED AND GET THE WRONG ONE LIKE I DID! They look the same in the images.



--------------------------------------------------------

Somewhere on the web I got this part no., don't remember where...

--------------------------------------------------------

THIS IS NOT THE RIGHT ONE .......

CONN FPC VERT 12 POS 1.00MM PCB

Digi-Key p/n WM1184-ND

Mfg. p/n 0520301299

 --------------------------------------------------------

THIS IS THE CORRECT ONE:

CONN FFC VERT 12POS 1.25MM PCB

Digi-Key p/n A100331-ND

Mfg. p/n 1-84534-2
---------------------------------------------------------

The info and link here is the correct one: [https://weistekengineering.com/?p=2556](https://weistekengineering.com/?p=2556)



Now all I have to do it figure out the correct orientation :).

From the data sheet I think pin 1 is the corner opposite the angle on the part. This puts the connector onto the layout below with the slot side facing to the right and the parts angle feature at the bottom left.

........I think. Anyone with middleman confirm this ....





**"Don Kleinschnitz Jr."**

---
---
**Derek Schuetz** *June 01, 2016 03:19*

You could just resolder the one off the old board and solder wires off of it. Or just solder it directly to the  middleman board


---
**Don Kleinschnitz Jr.** *June 01, 2016 03:46*

Yup I could, but the connectors are like 20c and pulling connectors like this from boards can cause problems.... not worth the 20c, intermittent s are a b*&%h. I also like to leave myself a return path in these builds :)


---
**Alex Krause** *June 01, 2016 04:03*

Do you have the model that uses the ribon cable I was set up to buy one then realized I didn't need it


---
**Stephane Buisson** *June 01, 2016 08:23*

sometime it's just easier just to re-run some cables from origin to smoothie (4 per steppers, 4 (3 common ground) end stop). not to much hassle really if you take XY assembly out.


---
**David Schick** *June 05, 2016 01:58*

Thank you!! This is very helpful I actually scrubbed OSH last week over a couple of lunches looking for something like this. I started with a DB25-EB ([http://www.electroboards.com/connectors.html](http://www.electroboards.com/connectors.html)) board ￼to prototype my RAMPS mod￼ but I just ordered a set of these boards since I have 2 friends who want the RAMPS/Inkscape option also and I want a repeatable / consistent set of mods. I'm scrubbing the notes on the build-log as well as others before I hook things up and I'll post images (especially of the power supply) here hopefully later this week. Thanks again!


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/48UDg3t1zir) &mdash; content and formatting may not be reliable*
