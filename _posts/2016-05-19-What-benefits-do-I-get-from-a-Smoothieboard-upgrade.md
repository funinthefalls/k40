---
layout: post
title: "What benefits do I get from a Smoothieboard upgrade?"
date: May 19, 2016 17:33
category: "Smoothieboard Modification"
author: "Carl Fisher"
---
What benefits do I get from a Smoothieboard upgrade? What other costs are associated beyond the board?



I'm currently using the newer M2 board and it's working fine for what it is which is basic engraving and cutting, but I'm interested in being able to do gray scale engraving varying the laser power based on color saturation, color coded engrave/cut lines, etc... Will a switch to Smoothie enable this? 



Also am I required to use a web based package (LaserWeb for example) or can I hit the laser directly from Inkscape or Fusion 360 (print style) vs exporting to g code and using a web interface? One of my biggest complaints about going TinyG with my CNC is the need to use web based software and don't want to lock into the same issue with the laser.



Is there another option besides Smoothie that is worth looking at? Ideally I need 4 axis support. X and Y plus I'm working on a Z axis table and eventually a rotary.









**"Carl Fisher"**

---
---
**Eric Rihm** *May 19, 2016 17:57*

Waiting on my smoothie in the mail,   but I know it supports 5 axis and if you get an lcd you can run jobs straight from the SD slot no computer needed 


---
**Ariel Yahni (UniKpty)** *May 19, 2016 18:24*

**+Carl Fisher**​ not use what the issue would be to use the Web interface (benefit platform independent) but u can use boards that support grbl or even tinyG with LaserWeb. Btw laserweb can also be run locally (no Internet)  but still is a Web interface


---
**Carl Fisher** *May 19, 2016 18:27*

It's not the web interface that I have a problem with, it's the reliance on cloud hosted (Internet connectivity) while in my shop. If it runs 100% on the local network I'm fine with that.


---
**Ariel Yahni (UniKpty)** *May 19, 2016 19:13*

**+Carl Fisher**​ it will run 100% locally


---
**Carl Fisher** *May 19, 2016 19:17*

So will switching over allow me to do the things mentioned above? color coded instructions (cut vs engrave) and PWM controlled gray scale photo engraving? Right now the M2 board seems to be all or nothing on the engraving part with no shades.




---
**Ariel Yahni (UniKpty)** *May 19, 2016 19:22*

Look under my profile youtube account and youll find some videos of shading with laserweb. At the moment you cannot work with layer but its in the roadmap


---
**Alex Krause** *May 19, 2016 19:24*

For the time being you can take a gray scale image into Gimp or Photoshop and adjust the brightness and contrast and apply a dither to it and use the resulting image to make what appears to be gray scaled images 


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/4jTiAP9QBPB) &mdash; content and formatting may not be reliable*
