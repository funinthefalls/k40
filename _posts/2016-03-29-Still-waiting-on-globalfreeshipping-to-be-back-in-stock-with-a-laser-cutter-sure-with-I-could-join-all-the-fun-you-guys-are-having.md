---
layout: post
title: "Still waiting on globalfreeshipping to be back in stock with a laser cutter :( sure with I could join all the fun you guys are having..."
date: March 29, 2016 02:27
category: "Discussion"
author: "Alex Krause"
---
Still waiting on globalfreeshipping to be back in stock with a laser cutter :( sure with I could join all the fun you guys are having...





**"Alex Krause"**

---
---
**Scott Marshall** *March 29, 2016 08:25*

Keep a lookout here, on CNCzone, Openbuilder, and Sawmill creek. I see about 1 per week for sale where someone bought one without enough research, then decided it's not for them. Craigslist and the college/maker boards are good too. Shipping is a killer, so driving distance is almost a prerequisete. 

 Hang in there, they're probabaly enroute. 

There are a couple other ebay sellers that have been vetted here as well, you may want to try them and see is they have any available around $400. (I paid $386 shipped from GFS). It may be worth it to you to get one sooner at a slightly higher price, and you never know what GFS is going to be asking this time around.



Here's a few on ebay, go by feedback, there;'s an element of risk no matter what.

 

[http://www.ebay.com/itm/High-Precise-40W-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Wood-Cutter-/200902245694?hash=item2ec6b4fd3e:g:nrUAAOSw2s1U0J02](http://www.ebay.com/itm/High-Precise-40W-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Wood-Cutter-/200902245694?hash=item2ec6b4fd3e:g:nrUAAOSw2s1U0J02)



[http://www.ebay.com/itm/High-Precise-40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/260825065645?hash=item3cba62a8ad:g:nVUAAOSwqu9U0J0b](http://www.ebay.com/itm/High-Precise-40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/260825065645?hash=item3cba62a8ad:g:nVUAAOSwqu9U0J0b)



[http://www.ebay.com/itm/40W-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Cutter-woodworking-crafts-/111215845517?hash=item19e4fb208d:g:VGMAAOSwstxVJdXL](http://www.ebay.com/itm/40W-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Cutter-woodworking-crafts-/111215845517?hash=item19e4fb208d:g:VGMAAOSwstxVJdXL)



There's more, most +-$20 

They're all the same machine from what I can see and has been said here.


---
**Maxime Favre** *March 29, 2016 10:23*

Anyone has a link for Europe shopping? The best I found is 550$ plus shopping. When I see 400$ shipped in usa, I think I'm missing something.


---
**Mishko Mishko** *March 29, 2016 11:01*

I've bought mine here:

[http://www.ebay.com/itm/252015202498](http://www.ebay.com/itm/252015202498)



I see they don't have any on stock at the moment, but it may be worth contacting them. They ship from the warehouse in Bremen. True they used the wrong country abbreviation, so instead to Slovenia, it traveled all over Europe before it was delivered, but I didn't pay any customs duties, the UPS driver just brought it to my doorstep. I don't know if this was just by chance or not.

When they realized they made a mistake, they immediately offered me a refund, which I declined, and in the end they even refunded me € 20,00 for the trouble. Not much, but anyway, I ended up paying €430,00 for it. Not bad, considering I've expected more;) And the seller is OK, at least compared to what I had to deal with in the past...


---
**MNO** *March 29, 2016 18:06*

Look now on [ebay.de](http://ebay.de) i just got one for 420Euro


---
**Alex Krause** *March 29, 2016 18:09*

**+Scott Marshall**​ my concern is that I am getting the k40 with the better controller in it. 


---
**MNO** *March 29, 2016 18:14*

I will change it anyway. And that one looks not like moshi

Of course more updates will be necessary i will start with temperature measurements on water and tube. Also some cover switch + water flow indycation. All on arduino nano + sensors+ lcd+delay. Should be easy as fun..﻿


---
**MNO** *March 29, 2016 18:19*

So no laser if no water or temp to high or switch not pressed also perhaps later airsupport starting on starting of cutting not constant...﻿


---
**MNO** *March 29, 2016 18:23*

[http://vi.raptor.ebaydesc.com/ws/eBayISAPI.dll?ViewItemDescV4&item=191687087676&category=83671&pm=1&ds=0&t=1459275651414](http://vi.raptor.ebaydesc.com/ws/eBayISAPI.dll?ViewItemDescV4&item=191687087676&category=83671&pm=1&ds=0&t=1459275651414)


---
**Scott Marshall** *March 30, 2016 01:13*

**+Alex Krause**As far as what board you get, I believe they are all the same at this point. Others here can confirm it, but I'm pretty sure the Older "moshi" board is obsolete. If it comes with Coreldraw and LaserDRW, it's the same version as I have (bought mine last Nov).



If it's an issue, there's lots of factory boards/software kits available on here from people who have gone to a smoothie or Ramps setup. I may be selling mine in the next few months for the same reason. You can  buy replacement boards for the Corel setup on ebay for $60 or less. The software is available here, just ask. Someone I know has posted the software in his Dropbox and made it public (but ask and I'm sure he'll respond (YS)), the serial number is the same for all, but you need the USB key to run it (also available on ebay or as take outs here).



Hope it helps,

Scott








---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/Wnnpt3oQaa9) &mdash; content and formatting may not be reliable*
