---
layout: post
title: "WHAT IS IN YOUR PLAYLIST? I thought I would make this thread so people can discover new and old music, podcasts ect"
date: December 16, 2018 22:29
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
WHAT IS IN YOUR PLAYLIST?



I thought I would make this thread so people can discover new and old music, podcasts ect. to listen to while they are playing with their toys.



[https://www.everythinglasers.com/forums/topic/what-is-in-your-playlist/](https://www.everythinglasers.com/forums/topic/what-is-in-your-playlist/)





**"HalfNormal"**

---
---
**Ned Hill** *December 17, 2018 02:21*

Podcast wise I enjoy:

 Adam Savage's  "Still Untitled" 

 Tested's  "This is Only a Test"

 Roman Mar's "99% invisible"

 "EOS 10" radio drama

 "The Story Collider"


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/9ZgBVRbbarE) &mdash; content and formatting may not be reliable*
