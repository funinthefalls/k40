---
layout: post
title: "Anybody know where I can get a datasheet for 17HM3448N-15AD stepper motor?"
date: January 20, 2017 09:59
category: "Smoothieboard Modification"
author: "Kris Backenstose"
---
Anybody know where I can get a datasheet for 17HM3448N-15AD stepper motor?





**"Kris Backenstose"**

---
---
**Ned Hill** *January 20, 2017 14:06*

**+Don Kleinschnitz** has some motor info over on his blog [donsthings.blogspot.com - K40-S Motors](http://donsthings.blogspot.com/2016/06/k40-s-motors.html)  Doesn't look like he has posted info on the 15AD but does for the 21AD


---
**Don Kleinschnitz Jr.** *January 20, 2017 14:10*

haven't found that motors specs yet. Must be a "SECRET STEPPER".


---
**Kris Backenstose** *January 21, 2017 00:37*

**+Don Kleinschnitz** Don did you just guess on yours or did you throw it out and replaced them on your smoothie upgrade?


---
**Kris Backenstose** *January 21, 2017 00:52*

[aliexpress.com - Free shipping 1 PCS 17HS2408 4-lead Nema 17 Stepper Motor 42 motor 42BYGH 0.6A CECNC Laser and 3D printer](https://www.aliexpress.com/item/Free-shipping-1-PCS-17HS2408-4-lead-Nema-17-Stepper-Motor-42-motor-42BYGH-0-6A/32590189283.html?spm=2114.01010208.8.9.IGCnWy)




---
**David Piercey** *July 04, 2018 22:23*

**+Kris Backenstose** Is that the one you replaced yours with in the end?  If so, how has it worked out?




---
**Kris Backenstose** *July 04, 2018 22:31*

That came with my k40. I ended up just using the same config from Don. Works good. I'm sure there are better/faster setups thoigh.


---
*Imported from [Google+](https://plus.google.com/113059362277072411228/posts/5hGDEThMGCE) &mdash; content and formatting may not be reliable*
