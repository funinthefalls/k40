---
layout: post
title: "As promised, I completed testing the two wire, no level-shifter, Open Drain approach to PWM control"
date: November 14, 2016 01:06
category: "Modification"
author: "Don Kleinschnitz Jr."
---
As promised, I completed testing the two wire, no level-shifter, Open Drain approach to PWM control. Tests were performed under various power and controller transitions. I found no cases where the laser fired erroneously.



Keep this in mind:

1. These testing results should apply to all machines with EXACTLY the same configuration as mine. Your results may vary. YOU are responsible to test your own machine. Let me know if you get different results.

2. Good control design does not eliminate the need for interlocks. There are failure and error modes that can cause a laser to fire erroneously. You must still have interlocks installed and operational for a machine to be capable of "SAFE" operation. 



By reading this you acknowledge that you use this information at your own risk.









**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *November 14, 2016 01:15*

Excellent news, thanks


---
**Ariel Yahni (UniKpty)** *November 14, 2016 02:32*

Can  this setup be used with those cnc shield that run grbl? 


---
**Don Kleinschnitz Jr.** *November 14, 2016 03:55*

**+Ariel Yahni** not sure I understand? Are you asking if this configuration can be used by CNC shields with an Arduino running grbl.

If the controller wants to PWM a K40 style supply then yes this will work provided it has an Open Drain MOSFET or Open collector transistor that can be connected to L.

Point me to a specific one and I can check it for you.


---
**Ariel Yahni (UniKpty)** *November 14, 2016 03:57*

This one for example OSOYOO 3D Printer kit with CNC Engraver Shield Expansion Board + UNO R3 Board + 4PCS A4988 Step Motor Driver with Heatsink for Arduino [https://www.amazon.com/dp/B0157ZZ3F4/ref=cm_sw_r_cp_apa_Oytkyb2F2VZ3P](https://www.amazon.com/dp/B0157ZZ3F4/ref=cm_sw_r_cp_apa_Oytkyb2F2VZ3P)


---
**Don Kleinschnitz Jr.** *November 14, 2016 06:10*

**+Ariel Yahni** I glanced at the above board wow pretty cheap!



The key question is how do you control the laser with GRBL. I suspect using the spindle and its speed codes.



Whatever that output is can be easily connected to an external breakout with an MOSFET that does what we are doing with smoothie to interface to L on the LPS.



[https://www.amazon.com/gp/product/B00V9XF1RY/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00V9XF1RY/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)



This one looks like it used a Z end stop output. In place of the laser diode driver in this diagram you could use the breakout above.



[https://www.hackster.io/macinblack/laser-engraver-with-arduino-719d14](https://www.hackster.io/macinblack/laser-engraver-with-arduino-719d14)

[hackster.io - Laser engraver with arduino](https://www.hackster.io/macinblack/laser-engraver-with-arduino-719d14)


---
**HalfNormal** *November 14, 2016 19:57*

**+Ariel Yahni** **+Don Kleinschnitz** The newest GRBL 1.1 is now very laser friendly. The output is a PWM 5vdc signal.I have used it on in input without any thing between but not on L.

In a few weeks when I get back from vacation, I plan to put together a middle man shield for the arduino. 

There are a lot of members running arduinos.


---
**Ariel Yahni (UniKpty)** *November 14, 2016 20:01*

Awesome **+HalfNormal**​, that's what I was trying to achieve. We're did you connect the L on the shield? 


---
**Don Kleinschnitz Jr.** *November 14, 2016 20:16*

**+Ariel Yahni** looks like **+HalfNormal** is using the IN pin not the L pin.


---
**Ariel Yahni (UniKpty)** *November 14, 2016 20:20*

Would it differ in something technically? 


---
**HalfNormal** *November 14, 2016 23:55*

**+Ariel Yahni** I have manually enabled the L pin and just used 0 power to turn the laser off and have used the COOLANT ENABLE in GRBL on L input as an alternative. You just have to play with the G-Code to get what you want. You can set the parameters with some of the G-Code senders too.

**+Don Kleinschnitz** I am going to play with a PPI implementation that will use L input. Using both inputs should give me the best overall control. This is courtesy of **+Joachim Franken** [https://github.com/JoachimF/PPI](https://github.com/JoachimF/PPI) 

[github.com - JoachimF/PPI](https://github.com/JoachimF/PPI)


---
**Don Kleinschnitz Jr.** *November 15, 2016 13:31*

**+HalfNormal** fascinating PPI approach. 

I have been thinking about PPI for some time due to its purported power efficiency for cutting. The K40 has plenty of power for engraving but marginal power for cutting IMO. Also this method argues that it will extend the tubes life.

From the code I am guessing he uses the stepper control signals to calculate position/direction and modifies the laser fire to create position-ally based on off pulses that are logically AND'd with the PWM signal? 

I take it that an Arduino is placed in series with the controller and the Laser Powers control signal?

I also want to play with PPI (like i need another project) but I plan to simply "AND" the PPI pulse with the PWM signal going to 'L' ... I think.

This would make a cool add on board !


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/NQ55zk1jpxa) &mdash; content and formatting may not be reliable*
