---
layout: post
title: "Anyone having a problem with the laser vibrating a lot and moving the material around?"
date: September 10, 2016 15:09
category: "Discussion"
author: "Cody Snyder"
---
Anyone having a problem with the laser vibrating a lot and moving the material around?





**"Cody Snyder"**

---
---
**greg greene** *September 10, 2016 15:19*

Nope, check your bed and make sure all the wheels on the head are turning free - sounds like one is binding - and/or - the scres need tightening


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 10, 2016 15:21*

I do have the issue of vibration, but that's caused by my air-pumps & the fact that I have the laser on a foldable table. But, my material doesn't move as I always magnet my material in place (unless it is thicker than 6mm).


---
**Scott Marshall** *September 10, 2016 17:02*

Anything causing that much vibration is bad. It could be as simple as your head speed is too high, bad bearing wheels/adjustment as Mr Greene suggests, or any of a variety of mechanical issues. Check your belt tensions, make sure all the fasteners are tight (these frequently shake apart, even in transit as they have no threadlocker or lockwashers).



The 1st thing I'd try is dropping your speed (especially if you're raster engraving).



A subtle shake as the head reverses is normal, but as long as the laser is on a solid level surface, it won't be enough to move your workpiece around. If you're bed is especially slippery I suppose it could, but I'm understanding that you've got more than 'normal' vibration levels. 

If that's all it is, magnets as Yuusuf suggests or tape would do the job.



To run down vibration sources, I'd suggest:



1st check to make sure the motion control assembly is not loose on it's mounting screws (4 screws thru the the floor of the cabinet).



With the power off, the carriage should move smoothly in both directions with no roughness or drag. It should have little to no (preferred) play in any direction. 

If it's dragging find out why and address it, if it's loose, the carriage guide wheels are on an eccentric and can be adjusted by loosening the set screw and rotating the brass shaft (it has an offset which moves the wheel in or out as you rotate it).



Belts are a little harder to get to. The Y belts are located inside the left and right guide rails and you can feel for tension by reaching down the outside of the guide rails (you can see the left one). Tension should be snug but not so tight as to "twang". The X belt is inside the X rail, and can be checked similarly. (the bottom of the rail is open)



 Both too tight and too loose on the belts will create vibration, as will missing teeth or a loose pulley/motor or bearing.



Tension adjustment pretty much requires removal of the motion control frame for access, which requires a mirror re-alignment upon replacement. Not as bad as it sounds, but it takes an hour or more the 1st time through.



Good luck, and I hope it's a simple and easy fix.



Scott


---
*Imported from [Google+](https://plus.google.com/112489272735986702749/posts/J98m9oxfxkQ) &mdash; content and formatting may not be reliable*
