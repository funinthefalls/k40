---
layout: post
title: "Is there a mechanical auto focus add on for this little laser?"
date: August 05, 2015 02:38
category: "Discussion"
author: "James McMurray"
---
Is there a mechanical auto focus add on for this little laser?  I am thinking of some sort of plunger that touches the material surface that would somehow either signal(LED) the correct focal length or if the Z was somehow automated just stop when it reached the correct focal length.  I tried searching the group but found nothing.





**"James McMurray"**

---
---
**Jose A. S** *August 05, 2015 03:56*

I am working in a little device to use a visible laser to adjust the mirrors...


---
**Stephane Buisson** *August 05, 2015 10:05*

originally, the K40 has no Z setting and come with fix focal.

the lens focal is 50.8 mmm, the distance  could be adjusted by raising the material manually with the help of a piece of wood mesuring about 50.5 mm (the difference is the material thickness).


---
*Imported from [Google+](https://plus.google.com/115183135050042620815/posts/ChAGCMAN85p) &mdash; content and formatting may not be reliable*
