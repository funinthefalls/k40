---
layout: post
title: "Could someone measure the distance the first mirror is from their laser tube, maybe with a picture so I know what corner your measuring to"
date: June 13, 2016 20:13
category: "Modification"
author: "Eric Rihm"
---
Could someone measure the distance the first mirror is from their laser tube, maybe with a picture so I know what corner your measuring to. I have a bunch of epoxy of the back on the back of my tube to seal a crack in the water section and it moved my tube forward a bit and I'd like to figure out where to redrill the mirror mounts. I have an idea with how I have it calibrated but wanted to start from the stock spot and I'm offset now.





**"Eric Rihm"**

---
---
**Don Kleinschnitz Jr.** *June 13, 2016 20:37*

**+Eric Rihm** so annoying that you can't post a pict. I re-posted it in the forum from my Photos alblum.


---
**Eric Rihm** *June 13, 2016 20:43*

You can post an image url link where it's hosted. Thanks 


---
**Don Kleinschnitz Jr.** *June 13, 2016 20:45*

Can you get to this?: [https://goo.gl/photos/y8Yavyttt1v1VB3eA](https://goo.gl/photos/y8Yavyttt1v1VB3eA)


---
**Eric Rihm** *June 13, 2016 20:57*

Yup! Exactly what I needed, much appreciated for the help.


---
**Stephen Sedgwick** *June 14, 2016 11:59*

Honestly that shouldn't matter as long you can center the beam and send it back on to the other mirrors while keeping it centered.


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/g6jTvVcPsKN) &mdash; content and formatting may not be reliable*
