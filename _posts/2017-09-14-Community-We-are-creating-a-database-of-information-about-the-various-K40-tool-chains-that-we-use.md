---
layout: post
title: "Community We are creating a database of information about the various K40 tool chain(s) that we use"
date: September 14, 2017 04:02
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
<b>Community</b> 



We are creating a database of information about the various K40 tool chain(s) that we use. 



<b>Please take a few minutes and complete this survey.</b>



I encourage you to include your G+ ID (we all know them anyway) where it asks, but of course that is optional.



The summary data from the survey will be available to the whole community.



<b>Results are here:</b>

[https://docs.google.com/forms/d/e/1FAIpQLSchQSs13diQL3l7vHz0wzUYx7t9lP2-gxL64581k4s_FOJEjQ/viewanalytics](https://docs.google.com/forms/d/e/1FAIpQLSchQSs13diQL3l7vHz0wzUYx7t9lP2-gxL64581k4s_FOJEjQ/viewanalytics)



<b>Form is here:</b>



[https://goo.gl/forms/0Ma5Wv8e9NlCPnms2](https://goo.gl/forms/0Ma5Wv8e9NlCPnms2)







**"Don Kleinschnitz Jr."**

---
---
**Anthony Bolgar** *September 14, 2017 04:16*

Done.


---
**Ray Kholodovsky (Cohesion3D)** *September 14, 2017 04:41*

C3D Mini for K40 bundle comes with Smoothie CNC Build. Not sure how people may interpret, may want to clarify your options. 


---
**Don Kleinschnitz Jr.** *September 14, 2017 11:41*

**+Ray Kholodovsky** Done


---
**Jason Dorie** *September 14, 2017 20:32*

**+Don Kleinschnitz** - If you're interested in taking a look at Lightburn let me know - I noticed that a few people have noted that it's their principal interface now, so if you're curious, either myself or Ray can give you access to the beta group.


---
**Anthony Bolgar** *September 14, 2017 20:42*

**+Don Kleinschnitz** take Jason up on his offer, I think you will be quite impressed with his software, I know I am.




---
**Don Kleinschnitz Jr.** *September 14, 2017 20:56*

**+Jason Dorie** certainly. I assume it can work with any smoothie compatible board??



What type files does it import? If I could use a SU file directly I would be in heaven :)!


---
**Jason Dorie** *September 14, 2017 21:06*

I currently use a VendorID / ProductID system to identify boards, but the eventual plan is to allow you to just point me at a COM port and I'll take your word for it.  For now, this allows me to detect boards without a complicated UI or protocol vetting.



Current imports are AI, PDF, EPS, with DXF and SVG in the plan.  Probably PLT and HPGL as well.  I've never dug into a SketchUp file.  Is the format documented?  I'm open to supporting pretty much anything as long as people will use it.


---
**Jason Dorie** *September 14, 2017 21:08*

**+Don Kleinschnitz** I FB Msg'd you a link to gain group access.


---
**Don Kleinschnitz Jr.** *September 14, 2017 22:50*

**+Jason Dorie** we can PM on G+ or hangouts also.

 

Never mind I got it thanks.....


---
**Jason Dorie** *September 14, 2017 22:57*

I looked for that, but didn't see a link for it, presumably because we're not connected.  I'm less used to the G+ interface.  :)


---
**Don Kleinschnitz Jr.** *September 14, 2017 23:31*

**+Jason Dorie** you have to follow me and then you can select me from a list when you post.




---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/i3K3GDYMe2X) &mdash; content and formatting may not be reliable*
