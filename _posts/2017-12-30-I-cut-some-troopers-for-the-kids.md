---
layout: post
title: "I cut some troopers for the kids"
date: December 30, 2017 19:07
category: "Object produced with laser"
author: "BEN 3D"
---
I cut some troopers for the kids.





**"BEN 3D"**

---
---
**BEN 3D** *December 30, 2017 19:07*

![images/2cd9d187a3a062a19a867091a90c3c98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2cd9d187a3a062a19a867091a90c3c98.jpeg)


---
**BEN 3D** *December 30, 2017 19:07*

![images/d8ca7d4e85b5c38cfabc7a68250be526.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d8ca7d4e85b5c38cfabc7a68250be526.jpeg)


---
**BEN 3D** *December 30, 2017 19:39*

And I found some doubled lines depending on the orientation of the trooper! May a Software bug, I had have forgot to group all elements in Inkscape before I cut and engraved it with k40 whisperers.



In the left side is my original blueprinted trooper.![images/954ba975d45cd41e579a8a1a2d5ad3d5.png](https://gitlab.com/funinthefalls/k40/raw/master/images/954ba975d45cd41e579a8a1a2d5ad3d5.png)


---
**BEN 3D** *December 30, 2017 22:09*

I create a taler one and replaced Raster engraved in filled vector grafic with vectored engrave border lines, this comes out without any doubled lines! That means it was an software bug. ![images/c4afda47a950ccfde220fc11fd8450a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4afda47a950ccfde220fc11fd8450a2.jpeg)


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/T47uV35Sqer) &mdash; content and formatting may not be reliable*
