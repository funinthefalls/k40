---
layout: post
title: "Hello all, I'm looking into getting the K40, but after seeing so many of the 40W machines available, I wanted to make sure that this one is the \"correct\" one that a majority use here"
date: December 12, 2016 12:19
category: "Discussion"
author: "James G."
---
Hello all,



I'm looking into getting the K40, but after seeing so many of the 40W machines available, I wanted to make sure that this one is the "correct" one that a majority use here.

[https://www.aliexpress.com/item/40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-High-Precise/32722536840.html?scm=1007.13446.47369.0&pvid=9058cd4f-898e-4f65-bd6a-27c5b74898c5&tpp=1](https://www.aliexpress.com/item/40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-High-Precise/32722536840.html?scm=1007.13446.47369.0&pvid=9058cd4f-898e-4f65-bd6a-27c5b74898c5&tpp=1)





**"James G."**

---
---
**greg greene** *December 12, 2016 12:39*

Looks exactly like mine - price is the same too


---
**James G.** *December 12, 2016 12:40*

Awesome, thanks.  Can't believe that includes FedEx Shipping too!


---
**greg greene** *December 12, 2016 12:43*

It's a bargain - after you get it - get hold of Ray at cohesion 3d - his board (And any of the other smoothies) will really improve the ways you can use it.  It opens up a whole new range of software for you.  The stuff you get with it is OK for learning but you'll soon be wanting more.


---
**James G.** *December 12, 2016 13:12*

Yeah, I'm definitely seeing that here, in this community.  Smoothie board will be first upgrade :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 13:14*

Wow, that's cheaper than I paid. Great deal. Looks to be pretty much the same as my machine too.


---
**stephen whitman** *December 13, 2016 22:01*

I have had my machine over 2 months now same as yours never got a manual with machine and the dvd that came with it for some reason does not [install.Trying](http://install.Trying) to get help so would appreciate any tips if people in the group have any thanks .


---
**Madyn3D CNC, LLC** *December 14, 2016 02:03*

you'll probably end up making a few changes either way, check out Don's conversion...  [donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
*Imported from [Google+](https://plus.google.com/114312362882399477330/posts/J9ARYDt6mSD) &mdash; content and formatting may not be reliable*
