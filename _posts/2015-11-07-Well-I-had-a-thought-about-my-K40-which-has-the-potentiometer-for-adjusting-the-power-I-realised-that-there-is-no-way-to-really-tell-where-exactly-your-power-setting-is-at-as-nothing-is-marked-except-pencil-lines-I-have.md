---
layout: post
title: "Well I had a thought about my K40 (which has the potentiometer for adjusting the power) & I realised that there is no way to really tell where exactly your power setting is at as nothing is marked (except pencil lines I have"
date: November 07, 2015 10:45
category: "Hardware and Laser settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Well I had a thought about my K40 (which has the potentiometer for adjusting the power) & I realised that there is no way to really tell where exactly your power setting is at as nothing is marked (except pencil lines I have drawn on my machine haha).



So I measured up the knob, that is on top of the pot, checked positioning for 0 & 100%. Turns out the knob is 25mm diameter and the 0% position is at 0 degrees (the bottom of circle) & 100% is at 270 degrees (the right of circle). So basically 27 degree steps for 0-100.



So here I've designed up a label that can slide over the knob. Centre hole will be 26mm diameter (so we have ~1mm of space away from the knob) & the entire thing will be 59.631mm diameter (according to Adobe Illustrator's calculations).



Feel free to use if you also would like to be able to determine your power % position.

![images/793709ca78425b755350c3c32a08416c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/793709ca78425b755350c3c32a08416c.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Eric Parker** *November 09, 2015 22:44*

I'd put the second pot inline, on the back of the panel, then adjust that to 0, the front pot to 100, then turn up the 2nd (backside pot) so that the meter reads 100%(18mA).  that way, I can turn the front pot all the way up and not have to worry about blowing the laser, and this would allow for recalibration as the tube and power supply age.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 10, 2015 05:36*

**+Eric Parker** Ah, that's a good idea. I guess you could actually put the 2nd pot inside where all the electrics are, so no-one adjusts it after you've set it to what you want?


---
**Eric Parker** *November 10, 2015 08:01*

thats the plan


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/bt5a1o9pisf) &mdash; content and formatting may not be reliable*
