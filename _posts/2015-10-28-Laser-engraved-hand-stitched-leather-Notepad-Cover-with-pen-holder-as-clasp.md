---
layout: post
title: "Laser engraved, hand-stitched, leather Notepad Cover (with pen holder as clasp)"
date: October 28, 2015 13:00
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Laser engraved, hand-stitched,  leather Notepad Cover (with pen holder as clasp).



Settings/Material: 6mA, engrave @ 500mm/s, 3mm vegetable tanned leather.



Finish: Natural beeswax



![images/279bb3013dc03315e6b2f062cf0d9929.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/279bb3013dc03315e6b2f062cf0d9929.jpeg)
![images/ffc526bef69e4b1a4de70c0b23b174fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ffc526bef69e4b1a4de70c0b23b174fc.jpeg)
![images/ee2b7ff686f29ef55699da0f6c4b873a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee2b7ff686f29ef55699da0f6c4b873a.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Stephane Buisson** *October 28, 2015 14:03*

I never used leather, thank you for indicating your settings.

do you cut it too? do you "drill" to ease stitching ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2015 14:11*

**+Stephane Buisson** Yeah I will indicate settings whenever I do something, that way if other people are doing similar they can use similar settings.



I didn't cut with the laser, as I am still having issues with that on leather. I cut it manually with a craft knife.



I don't drill for stitching, however I use an awl to basically "stab" holes where I want the stitches (which I did little dots with the laser to indicate the stitching spacing). Then I use 2 blunt needles to stitch it up.


---
**David Cook** *October 28, 2015 19:49*

looks cool !


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 29, 2015 09:25*

**+David Cook** Thanks David.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 29, 2015 23:47*

**+Allready Gone** That's so true. Women's products are the best to sell because they spend their own money on stuff & their husband/boyfriend/father's money too hahaha.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/139N2LgvtWo) &mdash; content and formatting may not be reliable*
