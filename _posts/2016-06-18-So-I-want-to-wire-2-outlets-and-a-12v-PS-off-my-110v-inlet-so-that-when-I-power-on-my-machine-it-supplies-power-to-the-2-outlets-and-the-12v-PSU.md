---
layout: post
title: "So I want to wire 2 outlets and a 12v PS off my 110v inlet so that when I power on my machine it supplies power to the 2 outlets and the 12v PSU"
date: June 18, 2016 02:03
category: "Discussion"
author: "Derek Schuetz"
---
So I want to wire 2 outlets and a 12v PS off my 110v inlet so that when I power on my machine it supplies power to the 2 outlets and the 12v PSU. 



Essentially the 2 outlets will go to the water pump and air pump and the 12v PSU will run some accessories.



How would the wiring for this look and would it be safe since I'm coming off all on inlet. I ass you me its the same as using a power strip like I am today





**"Derek Schuetz"**

---
---
**Ben Marshall** *June 18, 2016 02:25*

I would call a local electronics/hardware retail; they typically have some knowledgeable people. I did the same today 


---
**Derek Schuetz** *June 18, 2016 02:36*

Any electricians here?


---
**Anthony Bolgar** *June 18, 2016 04:15*

You would run the 110V lines in parallel with the entry plug, you would be adding 3 sets of lines ( 3 wires to a set  1 Hot - usually black, 1 Neutral - usually white and and 1 Ground - usually green) one for each plug (assuming they are separate plugs and not like a house plug that has two outlets powered by one set of wires) and one set to the 12V PSU power in. But if you are unsure of anything, please get help with it as 110V can kill.


---
**Ray Kholodovsky (Cohesion3D)** *June 18, 2016 04:48*

Look at something called the powertail. 


---
**Derek Schuetz** *June 18, 2016 05:02*

Here is my schematic if it makes any sense. Everything seems good and safe but I'm no professional

[https://imgur.com/a/uyngl](https://imgur.com/a/uyngl)


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/7gJKNTrNjfZ) &mdash; content and formatting may not be reliable*
