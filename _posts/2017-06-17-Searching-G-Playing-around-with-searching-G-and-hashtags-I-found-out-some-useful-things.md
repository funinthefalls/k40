---
layout: post
title: "Searching G+ Playing around with searching G+ and hashtags I found out some useful things"
date: June 17, 2017 14:03
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40search



Searching G+ 

Playing around with searching G+ and hashtags I found out some useful things. 



They seem obvious to me now but ....



When searching I have been using the "search community" dialog but you can also just use the search bar at the top. If you prefix your search term with K40 you can eliminates lots of noise. 



Adding a hashtag to a post or collection helps to drive the search to a specific subject and post...... duh. So adding hashtag [K40+subject] helps find relevant K40 stuff ... another doh!



When posting or adding to a collection hashtaging with [K40+subject] gets the search content focussed .... last doh.



Try this search term: [K40pwm], elliminate brackets.









**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2017 17:08*

Smart. Might be worthwhile adding similar tags whenever I post something new. Thanks for the clarification Don :)


---
**HalfNormal** *June 17, 2017 18:43*

A quick search found this information that expands  on how to search by Don.

[websighthangouts.com - Search tips within Google Plus - WebSIGHT Hangouts](http://www.websighthangouts.com/google-plus-search-tips/)



And here is a Chrome extension that seems to work well too. (yes I installed and tried it out.)

[https://hellboundbloggers.com/articles/how-to-search-google-plus-posts/25315/](https://hellboundbloggers.com/articles/how-to-search-google-plus-posts/25315/)


---
**Don Kleinschnitz Jr.** *June 18, 2017 12:44*

**+HalfNormal** are these search tips still valid? I do not have a search UI anything like these instruction, or do I need morning coffee?


---
**HalfNormal** *June 18, 2017 17:05*

**+Don Kleinschnitz** The ui is only if you want to design your own custom search. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/5BE57XijdiY) &mdash; content and formatting may not be reliable*
