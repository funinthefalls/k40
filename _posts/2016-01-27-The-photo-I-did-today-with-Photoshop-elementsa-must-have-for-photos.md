---
layout: post
title: "The photo I did today with Photoshop elements....a must have for photos!"
date: January 27, 2016 23:12
category: "Object produced with laser"
author: "Scott Thorne"
---
The photo I did today with Photoshop elements....a must have for photos!

![images/4e2e96078e1d289fd7cbae4b869f3648.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e2e96078e1d289fd7cbae4b869f3648.jpeg)



**"Scott Thorne"**

---
---
**Todd Miller** *January 27, 2016 23:39*

Looks Awesome !


---
**Scott Thorne** *January 27, 2016 23:40*

Thanks **+Todd Miller**​


---
**Joseph Midjette Sr** *January 28, 2016 00:08*

Very Nice!!!


---
**Ariel Yahni (UniKpty)** *January 28, 2016 00:15*

What specific workflow did you use? ﻿


---
**Scott Thorne** *January 28, 2016 00:50*

Photoshop elements 12.


---
**Michael Schroeter (MiSc)** *January 28, 2016 06:52*

**+Scott Thorne** How did you do that, step by step please. What kind of filters etc. Nice engraving!


---
**Justin Mitchell** *January 28, 2016 09:06*

I've had some success with producing half-toned images using inkscapes 'Create Tiled Clones - Trace' feature. e.g. [https://goo.gl/photos/eLwPAp2XWxwuzyna8](https://goo.gl/photos/eLwPAp2XWxwuzyna8)




---
**Scott Thorne** *January 28, 2016 10:23*

**+Michael Schroeter**...I first used the unsharpen mask...then posterize to darken the shaded areas then converted to greyscale...then converted to bitmap with dithered selected.


---
**Coherent** *January 28, 2016 16:48*

Looks great Scott. Looks like you've gotten the new machine figured out. I was going to order one a couple weeks ago (Amazon AndyKing) but they are out of them. Emailed them and was told they should have more soon. So instead ordered a larger milling machine from grizzly and the parts to CNC it. Gotta quit buying tools & toys before my wife divorces me!


---
**Scott Thorne** *January 28, 2016 17:39*

**+Coherent**...I hear you on that...I was glad that mine actually wanted me to get the bigger one...it's sweet man...problem is now I'm spending all my time in the garage...lol


---
**ED Carty** *January 30, 2016 00:59*

Wow scott they are getting better each time. Fantastic job man.


---
**Scott Thorne** *January 30, 2016 10:36*

**+ED Carty**...Thanks man


---
**ED Carty** *January 30, 2016 16:03*

Have you thought of making a how to video ? show everyone what you do from start to finish ? teach newb like me the tricks you have learned along the way. That would be awesome to watch


---
**Scott Thorne** *January 30, 2016 21:41*

That's a good idea...I'll make one tomorrow when I get off...I'll send you the YouTube link.


---
**ED Carty** *January 31, 2016 15:13*

thanks i look forward to watching it


---
**Joseph Midjette Sr** *February 01, 2016 00:54*

Man that is awesome! I have been trying to do a photo of my Grand babies and can't get it to work. All I have is that crummy corel that came with the printer. I do have the Vcarve pro & vcarve photo that i purchased for the CNC Router I built but don't think that will work on my laser unit.



If you make a tutorial to do a photo like that can you please share it with me. I will purchase the software I need to do a photo like that! 



Keep up the good work!


---
**Justin Mitchell** *February 01, 2016 10:13*

Corel is a vector art program, not really meant for bitmaps, so it has limited features there. If you dont have/cant afford photoshop then free software like Gimp will do the job. all the filters mentioned above are standard and can be found in any half decent bitmap editor.


---
**Scott Thorne** *February 01, 2016 10:50*

**+Justin Mitchell** is right, I bought Photoshop elements 12 on Amazon for 70.00....great price considering its 170 at Wal-Mart's.


---
**Joseph Midjette Sr** *February 01, 2016 11:24*

70 bucks is minscule compared to what I have invested in my cnc softwares. I will get PE today. I just need some tutelage as I am not at all familiar with it.


---
**Joseph Midjette Sr** *February 01, 2016 11:26*

Scott, if you have made that video for YouTube, can you share the link please? Thanks


---
**Joseph Midjette Sr** *February 02, 2016 02:21*

I got Photoshop Elements 14 today Brand New for $60 + Free Shipping on ebay! Should be here Thursday! WooHoo!


---
**Scott Thorne** *February 02, 2016 10:59*

Sorry guys I haven't had time to do it yet but I will try and get it done one day this week.


---
**ED Carty** *February 03, 2016 13:58*

Your work is amazing scott. 


---
**Scott Thorne** *February 03, 2016 14:04*

Thanks **+ED Carty**...much appreciated man


---
**Scott Thorne** *February 03, 2016 14:05*

It appears that my video recorder is broken, I'm going to have to get a new one this week to do the video with guys...sorry but YouTube has a few good ones that use Photoshop...that's where I got my ideas from.


---
**ED Carty** *February 03, 2016 23:34*

Ok I will look. 


---
**your patio** *September 28, 2017 18:47*

did you convert your pic to 1 bit?




---
**Scott Thorne** *October 04, 2017 20:39*

**+your patio** yes, it was converted to 1 bit.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/ELB87FqKKnG) &mdash; content and formatting may not be reliable*
