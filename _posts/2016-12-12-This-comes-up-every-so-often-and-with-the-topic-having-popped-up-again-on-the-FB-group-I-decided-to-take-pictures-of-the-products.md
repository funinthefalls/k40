---
layout: post
title: "This comes up every so often and with the topic having popped up again on the FB group, I decided to take pictures of the products"
date: December 12, 2016 17:51
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
This comes up every so often and with the topic having popped up again on the FB group, I decided to take pictures of the products. I'm talking about the tape I use for masking. While you can use a variety of tapes, from blue painter's tape to your regular masking tape, because I happen to work at a sign shop, I use the same tape that the guys use on vinyl work.



For my wood cutting/engraving, I use RTape Conform 4075RLA which is a high tack paper application tape. A while ago I posted a picture of how I mask a bunch of material in one pass. This stuff sticks well to Birch wood that I work with. Usually a single pressure pass on the roller is enough, but occasionally, I will run a 4in squeegee over it by hand and press down a bit harder. If I do it too much, when I lift the tape up, it will take a very minute amount of fiber with it, enough to make the surface a bit rough. Some times it actually results in a nice rough finish, though generally I will sand the finished pieces smooth.



For delicate material such as paper or diffuser film, I will use the 4050RLA version which is a medium tack tape.



With both of them, if I do a very intricate engraving job, I will use something like duct tape or the stronger gaffer tape to remove the masking tape. Just wrap a piece around my finger and start peeling. I posted a few pictures of this process too not too long ago.



Another product that's used at the shop which I have NOT tried yet, but it appears to be very similar in both material and tack level, is TransferRite Ultra.



![images/4fa737718b113dfd5e8aeb8ff8d68be9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4fa737718b113dfd5e8aeb8ff8d68be9.jpeg)
![images/04b6ce8fc3eaa0d57d59f447081accc1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/04b6ce8fc3eaa0d57d59f447081accc1.jpeg)
![images/b1bff9a6a18df6cf7d4e910e23b41076.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b1bff9a6a18df6cf7d4e910e23b41076.jpeg)
![images/ad92a9800b1e61b7a84acf784fea0a47.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad92a9800b1e61b7a84acf784fea0a47.jpeg)

**"Ashley M. Kirchner [Norym]"**

---


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/MLv91QP9GrC) &mdash; content and formatting may not be reliable*
