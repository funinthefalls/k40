---
layout: post
title: "What are other good options for a coolant tank besides the Home Depot 5 gallon bucket?"
date: July 13, 2016 17:07
category: "Hardware and Laser settings"
author: "Corey Renner"
---
What are other good options for a coolant tank besides the Home Depot 5 gallon bucket?  I've had my laser for a few weeks and finally found a place for it, a 2-drawer horizontal file cabinet, which takes it off the dining room table.  However the 5 gallon bucket is a bit large for the laser's new place.  What else works well?  Also, the hose clamp that came with the pump has rusted itself into a ball of crap after only 3wks in distilled water.  I'm changing my water and replacing the hose clamp with a zip-tie.  To avoid a bucket of rusty water, I recommend tossing that crap hose clamp right away.





**"Corey Renner"**

---
---
**Jim Hatch** *July 13, 2016 17:42*

Try a Rubbermaid bin - their oblong shape might fit better in the file cabinet drawer. 


---
**Todd Miller** *July 13, 2016 22:54*

I used a 'fish' tank ;

[https://sites.google.com/site/litterbox99/home/laser/20160107_10.JPG](https://sites.google.com/site/litterbox99/home/laser/20160107_10.JPG)


---
**Pippins McGee** *July 14, 2016 02:53*

**+Todd Miller** nice. what do you put in water to stop algae from growing?


---
**Ned Hill** *July 14, 2016 03:06*

**+Todd Miller** Nice heat exchanger set up.  What's that radiator from?


---
**Scott Marshall** *July 14, 2016 09:48*

**+Pippins McGee** Alcohol, 10% is plenty


---
**Todd Miller** *July 14, 2016 13:36*

I use distilled water and RedLine WaterWetter additive.

The radiator/fan set up is stuff they use to cool PC's


---
**Pippins McGee** *July 15, 2016 00:27*

**+Scott Marshall** thank you for the response Scott.

any specific alcohol? as in 99.9% isopropyl alcohol or just pour a bottle of vodka into my bucket? haha.

and when you say 10% I assume you  mean, 10% of my bucket should be filled with alcohol? or are you meaning alcohol that is 10% strength should be added.

sorry.


---
**Ned Hill** *July 15, 2016 02:44*

He probably means 10% by volume denatured alcohol you can get at the hardware store.  It's ethyl alcohol that has additives to make it so you can't drink it.  You can also try using an aquarium algaecide.  I don't have a problem with algae myself, but I worked in a pet store during college.


---
**Scott Marshall** *July 15, 2016 07:48*

**+Pippins McGee** Actually, I recommended cheap Vodka to someone a while back in lieu of commercially available stuff. Any type is fine, IPA (Isopropyl), Methanol (wood alcohol also sold as "denatured alcohol", which is also sold in hardware stores as "Shellac Thinner/lamp fuel".

Whatever is cheap. 

I doesn't take much, just enough to keep the bugs from getting a hold. As low as 3% which would be 2 oz per gallon for 100% alcohol. (That's 2 in 128 or 1 in 64). 

I personally use about double that or 4oz/gal (I use Methanol as I have it around for use as a model aircraft fuel and all purpose metal shop cleaner.



I've not seen any evidence of slime or discoloration since I started using the alcohol mix. I keep a re-usable coffee filter (cup with very fine brass screen in the bottom) as a return filter, allowing the return line to cascade thru the filter as it drops back into the tank. This keeps any dirt etc from accumulating in the tank.

It's a very simple system, but works great.



You don't have to worry about the alcohol evaporating as it molecularly links up with the water, and stays put.



(make sure you cover the tank if you have cats or dogs, the alcohol solution is very poisionous should they try to drink from your tank. (maybe a case for using Vodka, just in case?)



Scott


---
**Ned Hill** *July 15, 2016 13:54*

The chemist in me would recommend staying away from methanol just to be on the safe side.  If you go this route stick with ethanol or perhaps IPA.  Also make sure whatever you add is chemically compatible with your tank and tubing (e.g. IPA is not recommended with silicon tubing).


---
**Scott Marshall** *July 15, 2016 17:57*

That's a good point Ned. I agree with your inner chemist.



Methanol is by far the most aggressive of the group, having corrosive/etching effects on a wide variety of materials. It cleans so well, it can cause all the garbage in a previously gasoline fuel system to wash away and plug up filters, air bleeds etc. It can me rough on submersible  pump bearings too. (NO lubricating properties)



Makes a ton of horsepower though, and is very easy on internal engine parts (no carbon abrasives produced and nearly impossible to detonate)



The ethanol is the "friendliest" of the gang, non-poisonous (sort-of) and least corrosive, at least in low or high dilutions. Almost as good as methanol for hp production, but has less btu/lb.



(I was involved in a Federal gasahol research project in the early 80s - and used to drag race/build racing engines.)



Scott


---
**Pippins McGee** *July 16, 2016 07:10*

**+Scott Marshall** thanks for clearing that up scott. & **+Ned Hill** 

I can buy methylated spirits locally, available at retail store.. it is a 'denatured alcohol' which is apparantly 90% ethenol and roughly 10% methanol + other additives to make it too disgusting to drink.



will this suffice?

or will those 'additives' they add to make it undrinkable gum up everything up over time?

they don't seem to publicly say what the additives are.


---
**Scott Marshall** *July 16, 2016 12:21*

**+Pippins McGee** 



It will work fine.



I understand Methylated Spirits are , as you say denatured alcohol, which is usually about 95% ethanol, and about 5% (or so) "Methyl" (or methanol), thus the name. This is strictly to make it undrinkable, and thus not subject to government alcoholic beverage taxes.



The name baffled me the 1st time I heard it, but makes complete sense once you understand exactly what it means.



I'd suggest a secure lid and labeling to help prevent children and animals (labeling probably less effective there) from being poisoned. I have a cat that will lick anything that 'tastes', good or bad. I've caught it in everything from wheel bearing grease to Franklin's Hide glue (destroyed a 1/2 finished model airplane on me by licking the joints to destruction), so am particularly aware of there risk there.



Scott


---
**Pippins McGee** *July 17, 2016 07:09*

**+Scott Marshall** Haha - fair enough! No pets or children here, just me myself and I, I'm afraid - so that's OK - Thanks Scott, appreciate the help.


---
**Scott Marshall** *July 17, 2016 08:34*

Ok, Maybe I'm a little cautious. I was never the kind of parent who put the outlet blockers in or locked the cabinet up. I taught my kids that electricity can kill you and detergent is bad eats, and they got it.



But my cats aren't as educable, and I'd have to guess most aren't. If they will drink from the toilet, they will drink from a coolant tank....


---
*Imported from [Google+](https://plus.google.com/116200173058623450852/posts/czKerZ7DVBz) &mdash; content and formatting may not be reliable*
