---
layout: post
title: "Lasered Slippers for around the house. Top layers are double layer of polar fleece (black & red)"
date: June 18, 2016 22:51
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Lasered Slippers for around the house.



Top layers are double layer of polar fleece (black & red). Sole is moccasin suede (purple) lined with denim on the inside.



My sewing skills leave much to be desired.

 #yearofthelaser  



![images/f99312e9c876cc4bdf812adfdfa29119.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f99312e9c876cc4bdf812adfdfa29119.jpeg)
![images/f419e05c5347e59aca5c237f0e7cb868.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f419e05c5347e59aca5c237f0e7cb868.jpeg)
![images/e248198c575bd5bc807d6dcdd0096ddf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e248198c575bd5bc807d6dcdd0096ddf.jpeg)
![images/bcf7cced8542a909671a6adc059b3523.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bcf7cced8542a909671a6adc059b3523.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Stephane Buisson** *June 19, 2016 09:25*

Love your creativity !

So many materials to work with ...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 19, 2016 10:17*

**+Stephane Buisson** It's always fun testing different materials to see how they handle lasering. I'm pretty happy with how fabric lasers, helps minimise fraying of the edges before sewing too. Also allows for precision cuts on pattern pieces. Unfortunately the k40 can't fit an entire 150cm width roll... YET! (it's on my to-do-sometime list).


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/WcxPu52fegV) &mdash; content and formatting may not be reliable*
