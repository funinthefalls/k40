---
layout: post
title: "About to make the jump from K40 to 60watt"
date: November 14, 2017 04:51
category: "Discussion"
author: "Bill Keeter"
---
About to make the jump from K40 to 60watt. Going the openbuild tslot/vslot route with Cohension3D for motherboard.



Any suggestion on a trusted 60watt laser + power supply vendor? I'm looking at [sinjoe.com](http://sinjoe.com) just cus it was mentioned in the FreeBurn discussion on OpenBuild.



Here's the build i'm going to start from. I'll be adjusting the frame size and how the x and y are setup. With Vslot i'm hoping I can stick with NEMA17 motors so I can run them directly from the motherboard.



[https://openbuilds.com/threads/v-slot-co2-laser-60-100w.617/](https://openbuilds.com/threads/v-slot-co2-laser-60-100w.617/)



Anyone done this already? Comments and suggestions are greatly appreciated. I'm probably a wk away from starting to order parts. :D





**"Bill Keeter"**

---
---
**Don Kleinschnitz Jr.** *November 14, 2017 15:15*

A note on the supply: it does not provide 24vdc so you will need a external 24 vdc which is a good idea anyway :).


---
**Bill Keeter** *November 14, 2017 15:37*

**+Don Kleinschnitz** Wait, what? I guess I was under the impression I could use the 60w power supply to run everything. Similar to how the K40 is setup. But yeah, it would make sense to run everything beside the laser on a separate supply..


---
**Don Kleinschnitz Jr.** *November 14, 2017 16:37*

**+Bill Keeter** the supply type you have linked is not a direct replacement for the K40-60 it provides only 5V.



Not to worry, your new machine probably needs more current anyway depending on the steppers you use. I would not scrimp on the stepper sizes but make sure the C3D can drive whatever you select.



You may also need 24 vdc for steppers in a lift and/or rotary.

I also recommend a 12 vdc external because there is a lot of cheap stuff (lights, temp meters) that runs on 12 vdc.



I would take the time to create a power budget for all the DC things you want now and in the future before you buy DC supplies.



Having a separate 24 vdc supply is a much better choice anyway, get the largest power you can afford that at least meets your power budget and make sure to fuse it.



I'm interested in your build as I may convert my K40 if I find I use along with my new OX mill.


---
**Bill Keeter** *November 14, 2017 17:12*

**+Don Kleinschnitz** Yeah, originally I wanted to expand the cutting area of the K40, but realized I wanted something bigger than the box could handle (~24"x18" or more)... and once you go bigger why not switch to a 60w tube. I originally learned on a 60w Universal anyway.



60w tube i'm looking at is: [https://store.sinjoe.com/index.php?route=product/product&path=86_60_67&product_id=231](https://store.sinjoe.com/index.php?route=product/product&path=86_60_67&product_id=231)



60w power supply: [store.sinjoe.com - Co2 Laser Power Supply T60 For 40-60W Co2 Laser Tube](https://store.sinjoe.com/index.php?route=product/product&path=71&product_id=209)



Was hoping Nema17 motors could still drive the system since I would be using V-slot. But yeah I could change to Nema23 but i'm assuming the Cohesion3d couldn't run them directly. But **+Ray Kholodovsky** would know :D


---
**Steven Whitecoff** *November 15, 2017 17:33*

Cant see the C3D handling 7a @36-48v which is Nema23 -10 and -20 requirement. That would be why he offers external driver breakout boards and this how to wire:

[cohesion3d.freshdesk.com - Wiring a Z Table and Rotary with External Stepper Drivers : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000739904-wiring-a-z-table-and-rotary-with-external-stepper-drivers)


---
**Steven Whitecoff** *November 15, 2017 17:43*

The normal way to do this is with a seperate powersupply for the stepper drive. You could perhaps be ok with 400w(2 axis) on a low mass Vslot setup. But there are downsides to going minimal with the powersupply, chief reason is they can only handle modest back EMF and when you move the mechanism by hand or worse hit the E stop while running, you risk frying the drive controller. Cures are, using a large external capacitor to handle it, a fancy shunt circuit OR a HD(relative to stepper size) drive controller(the larger the drive, the larger its internal capacitors). So I recommend being generous in component capablities AND running seperate drive controllers and a powersupply for each. Especially for the PS, its actually cheaper to use two than one large one.


---
**Bill Keeter** *November 15, 2017 20:36*

**+Steven Whitecoff** are you saying if I go the Nema23 route? or even if I go 17? I was just looking at the steppers on OpenBuild and their Nema17 has a torque of 76 oz*in. So i'm pretty sure I can run at least the X axis off one of those. Maybe even Y. As the Y won't be moving weight anywhere near it's max torque. Or are there advantages to using the NEMA23? Would I be able to engrave faster with one motor over the other? Blaaaah, sometimes I wish I was a engineer so I would now this stuff already.



As for power, based on Don's feedback i'm planning for the laser to have it's own supply and i'll be running at least one other 24v power to supply everything else. Plus probably a circuit breaker for peace of mind.


---
**Ariel Yahni (UniKpty)** *November 15, 2017 20:48*

I run my upgraded K40 with nema17 from OB. I also run minimill with nema23 with in xpro board from them. If you are doing 1 stepper per axis I don't why you would not be able to run them from a mini, unless in that particular case the math don't add uo


---
**Steven Whitecoff** *November 15, 2017 23:20*

You can get steppers in many voltages, 2 or 3 phase, different angles per step, different RPM capabilities, different microsteps, and different torque ratings. 



Then one can also run them direct or geared to increase torque at the sacrifice of speed. So the number of variables is extreme. 



Based on what I see offered in large format machines(lets say any axis over 1000mm) that are not obvious budget kludges, is mostly Nema 23, which is a format that does not include lenght of stepper so you can have little 23s or fairly huge. 

So i for example have two sized of Leadshine 23 one a -10 and one a -20 which is twice as long... my laser head is moved by 142in/oz rated stepper with 1:3 gearing via belt drive. Tha t means my 1.8*/step is actually 0.6* at output. With my stepper capable of 3000rpm its still able to move pretty fast. 

The point of all these details is trying to understand every aspect of this does require an engineer but since this stuff is not big money its best to not skimp on size or power. Buying too small will result in you buying again, buying over sized will just cost you a few dollars more you will likely get back in reliablity and realizing its full performance potential.


---
**Bill Keeter** *November 16, 2017 04:03*

**+Steven Whitecoff** thanks for the added explanation. Can you share a pic of how you have the gearing setup? I'm curious if you're doing it for the added torque or adding detail when engraving? Do you have the gearing on both the X and the Y? Or is the 1:3 gearing for rotary and bed?



What speed are you averaging for engraving with those motors?


---
**Steven Whitecoff** *November 16, 2017 18:26*

First, I cut(with a stock C3D K40) not engrave. I'm still trying to understand the "why" for some of this. For example my system came with an ~  1:2 gearing for the short axis which moves the long, heavy axis which has 1:3 gearing for moving the laser head. So for torque thats backwards and for accuracy its still a "why?" have them different. I assume they have a reason and I just got a bigger stepper to help with moving the heavy axis. I may have to convert to a 1:3 to make life easier, meanwhile I've asked the question to my supplier. FWIW, most of what I've said is based on looking at virtually every chinese laser kit on Aliexpress, so not exactly definitive but there does seem to be a lot of commonality in stepper sizing for similar size machine. So for a 1300-1600mm long axis, 200in/oz geared 1:2 or  400 output torque seems to be the norm. At this point I've just run each motor via RS-232 and a PC to control it via setup software. My drivers are closed loop and use software config instead  of dip switches. I just finally figured how I'm going to build a 4'x6' enclosure without breaking the bank. Once I have that built I can actually mount it so it can run at real speed. 




---
**Steven Whitecoff** *November 16, 2017 20:26*

Took a look at the Sinjoe site, their SJ-139 is the closest to the size of my mechanics and surprise surprise they are using closed loop "easy servo" drivers/steppers rated the same as my Leadshine gear. So I guess I got it right, although I cant tell if they are geared or direct and their carriage system is likely a bit heavier than mine. I about choked when I saw it weights 440 kilos...with my proposed aluminum framed aluminum minimal enclosure I hope to be around 65 kilos. 


---
**Steven Whitecoff** *November 16, 2017 23:20*

Quite the eye opener: my long axis weights 18.5lbs...thats a lot of mass to be juking around


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/9k6V1V7VWMC) &mdash; content and formatting may not be reliable*
