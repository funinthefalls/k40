---
layout: post
title: "So where do the US guys like getting their K40 from right now?"
date: January 12, 2016 16:39
category: "Discussion"
author: "Brandon Satterfield"
---
So where do the US guys like getting their K40 from right now? 





**"Brandon Satterfield"**

---
---
**3D Laser** *January 12, 2016 16:46*

Seems the best and cheapest place is eBay I am trying to find an updated k40 in the US with no such luck though 


---
**Brandon Satterfield** *January 12, 2016 16:49*

Thanks Corey I'm looking to resolve that issue, but since this one has taken off so well and this community is managed so well, figured I'd start with a K40. 


---
**Joe Spanier** *January 12, 2016 17:30*

Brandon, I've been talking with some people about doing an open source v-slot laser project. You came into my head last night as to someone to talk to about it. Have any interest? Feel free to hit me up on hangouts if you would like. 


---
**Ariel Yahni (UniKpty)** *January 12, 2016 18:03*

Is it cheaper to do it with vslot? Well the extrusion is not that expensive. The sourcing of the rest of the parts is what worries me


---
**Joe Spanier** *January 12, 2016 18:31*

Its more your can build a great machine with a bigger build area and less mechanical issues than the k40. Maybe not cheaper but much better for not much more. 



Right now for the cost of tube and power supply though its almost worth it to buy a k40 and gut it and send the rest of for scrap. Unless someone like **+Brandon Satterfield**​ can work some supplier connections. 



I'm happy to start a project, design/build a machine. But the business/supplier stuff isn't my forte. 


---
**Brandon Satterfield** *January 12, 2016 19:21*

**+Joe Spanier** you are the man. 



A quick background. I've hovered over freeburn for a while. There are things I simply need to put my hands on to understand. Another restriction was the interface, but as Peter has been knocking this out with ease and class I'm ready to run. As far as experience though I'm restricted, not by CNC but by the actual laser parts, mirrors, focal lengths, tubes, etc. so this is where my question above arises. I wanted to buy one of these machines to become more familiar with the process. 



I would be very excited to build with you Joe. I just need to get my feet wet. 



Suppliers: :-), I have already lined them up. 40 - 150w with power supplies, ready to ship. 


---
**Joe Spanier** *January 12, 2016 20:33*

Ill hit you up on hangouts but lasers are pretty easy. **+Peter van der Walt** 's work really makes some awesome stuff doable now. I really want to make project a thing.


---
*Imported from [Google+](https://plus.google.com/111137178782983474427/posts/G2aWs9HwuJS) &mdash; content and formatting may not be reliable*
