---
layout: post
title: "Two of my Windows 10 PC's just did an update"
date: April 02, 2016 01:53
category: "Software"
author: "Anthony Bolgar"
---
Two of my Windows 10 PC's just did an update. This caused my Gcode exporter for Marlin (Turnkey Tyrannys) to stop working on both machines. I had to do a native install of python to get it to work again. I have no idea what the update did, but if anyone else has this problem, just do the native install as per the instructions at [https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin](https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin)





**"Anthony Bolgar"**

---
---
**Andrew ONeal (Andy-drew)** *April 02, 2016 06:33*

Hey, does inkskape plugin only raster true black and white photos? I generated the raster code with success for a grey scale image but laser would not fire when I printed?


---
**Andrew ONeal (Andy-drew)** *April 02, 2016 06:36*

I had th e same issue with win10 which is why I kept win7 on my two project laptops, better options when dealing with programs like python, arduino, and others. At lease this is my belief.


---
**Anthony Bolgar** *April 02, 2016 07:36*

Not sure about the raster options


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/KMtAPBRNW56) &mdash; content and formatting may not be reliable*
