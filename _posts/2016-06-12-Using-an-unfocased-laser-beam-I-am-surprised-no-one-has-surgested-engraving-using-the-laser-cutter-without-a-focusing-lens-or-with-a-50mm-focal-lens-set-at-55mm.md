---
layout: post
title: "Using an unfocased laser beam. I am surprised no one has surgested engraving using the laser cutter without a focusing lens ,or with a 50mm focal lens set at 55mm ."
date: June 12, 2016 07:05
category: "Discussion"
author: "Phillip Conroy"
---
Using an  unfocased laser beam. I am surprised no one has surgested engraving using the laser cutter without a focusing lens ,or with a 50mm focal lens set at 55mm .





**"Phillip Conroy"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 12, 2016 07:43*

Have you tried it? I have & the results are very thick beam & look horrible to be honest.


---
**Jim Hatch** *June 12, 2016 08:14*

I use an unfocused beam on acrylic sometimes. Not 5mm unfocused though, just a little. The wider beam softens the edges of engraves. Then I refocus for the middle of the material for the cut.


---
**Phillip Conroy** *June 12, 2016 09:05*

there has got to be a use for unfocased beam,ie solder surface mount componets,


---
**Anthony Bolgar** *June 12, 2016 09:43*

It can be used to heat the acrylic so it can be bent.


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/cNpVweBDmxG) &mdash; content and formatting may not be reliable*
