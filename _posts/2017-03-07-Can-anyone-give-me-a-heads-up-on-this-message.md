---
layout: post
title: "Can anyone give me a heads up on this message?"
date: March 07, 2017 23:59
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Can anyone give me a heads up on this message? 



It appeared intermittently while I was running a cut job in corellaser. It never stopped cutting though. The message would appear for a couple secs and leave...come back a few secs later. 

![images/01b2c424fe7a688cfa43ee41e7cc2059.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/01b2c424fe7a688cfa43ee41e7cc2059.jpeg)



**"Nathan Thomas"**

---
---
**Mark Brown** *March 08, 2017 00:06*

Are you using the stock USB cable?  I'm not sure about this exactly, but I've heard of people having trouble with the cable failing.


---
**Nathan Thomas** *March 08, 2017 00:41*

Gotcha, I was going to ask about that cause it feels loose. Can I just get a line from Best Buy etc...or does it have to be some special line that only fit this machine from the manufacturer?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2017 01:07*

The USB cable is basically just a printer cable. Should be able to replace with any decent quality cable. Shielded cables are recommended.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2017 01:08*

Another thought, make sure your USB cable isn't running close to any power lines (either on desk or in the laser casing). Could be interference causing intermittent connection issues.


---
**Nathan Thomas** *March 08, 2017 01:29*

**+Yuusuf Sallahuddin** Thanks....the power to the laser is on the back left corner...the usb in the front left side. Got it plugged into my laptop and it's powered too. I'll grab a good quality printer line tomorrow...hopefully that'll solve the issue 


---
**Abe Fouhy** *March 09, 2017 12:37*

These cables are USB male to USB male, try monoprice or amazon. You probably can't find that local easily 


---
**Mark Brown** *March 09, 2017 14:31*

Mine's USB A  male(standard) on one end to USB B male (squarish) on the other.  The same as the cable that's come with every printer I've ever seen.



Shouldn't be too hard to find locally?


---
**Abe Fouhy** *March 09, 2017 15:41*

**+Twelve Foot** Oh, in that case your good. Mine is setup different. :)


---
**Mark Brown** *March 09, 2017 22:56*

I know I am.  :)



But I'm not the one with the issue here. Depends on what Nathan's machine is like.


---
**Abe Fouhy** *March 09, 2017 23:03*

Oh yeah haha


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/L2S53E2q8sB) &mdash; content and formatting may not be reliable*
