---
layout: post
title: "Built custom bed lift. Bolted machine to table to keep it from moving, cut hole in bottom of machine and table, attached 12 inch 1/2 inch pipe with flange on bottom and inset screws on top, added support 2x4 below to keep it"
date: July 22, 2016 02:02
category: "Modification"
author: "Michael Otte"
---
Built custom bed lift.  Bolted machine to table to keep it from moving, cut hole in bottom of machine and table, attached 12 inch 1/2 inch pipe with flange on bottom and inset screws on top, added support 2x4 below to keep it lined up and am using small hydraulic jack to raise/lower bed.  So far it works great, total cost was about 40$ from home depot. 

 Note, had to install the pipe off center because of that support bar on the bottom, but it still came in very level and sturdy.



![images/a67c94b8577bed6604f7527b24d10a4e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a67c94b8577bed6604f7527b24d10a4e.jpeg)
![images/e01be86636d8f2294d6108fb3e70b644.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e01be86636d8f2294d6108fb3e70b644.jpeg)
![images/4e99136e34fcf23279125bab9de4b4c7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e99136e34fcf23279125bab9de4b4c7.jpeg)
![images/962a39890e9dbdd99ece571e4ee323d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/962a39890e9dbdd99ece571e4ee323d8.jpeg)

**"Michael Otte"**

---
---
**Ariel Yahni (UniKpty)** *July 22, 2016 02:21*

Very clever


---
**John Austin** *July 22, 2016 06:11*

nice idea, do you have a video of it in use?

Does it keep level when raising?


---
**Robert Selvey** *September 06, 2016 21:22*

Where did you find your honeycomb metal for you bed ?


---
**Michael Otte** *September 07, 2016 02:37*

**+Robert Selvey** [lightobject.com - 300X200 Honeycomb. Fit K40 machine](http://www.lightobject.com/300X200-Honeycomb-Fit-K40-machine-P705.aspx)




---
*Imported from [Google+](https://plus.google.com/115689970456066406491/posts/ioz4dRbp7Wb) &mdash; content and formatting may not be reliable*
