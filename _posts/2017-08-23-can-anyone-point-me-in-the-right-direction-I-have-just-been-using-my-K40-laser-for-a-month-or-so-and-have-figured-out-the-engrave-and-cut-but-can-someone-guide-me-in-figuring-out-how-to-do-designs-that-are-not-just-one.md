---
layout: post
title: "can anyone point me in the right direction - I have just been using my K40 laser for a month or so and have figured out the engrave and cut, but can someone guide me in figuring out how to do designs that are not just one"
date: August 23, 2017 18:58
category: "Discussion"
author: "Doug Western"
---
can anyone point me in the right direction - I have just been using my K40 laser for a month or so and have figured out the engrave and cut, but can someone guide me in figuring out how to do designs that are not just one laser power.  I guess what I mean is trying to give a layered look.  For example I have an anchor on top overlapping a solid cross.

![images/206b593a1fb62753704a97d0bbad7aef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/206b593a1fb62753704a97d0bbad7aef.jpeg)



**"Doug Western"**

---
---
**Ned Hill** *August 24, 2017 00:16*

The best you can do with the stock controller and software is to break the image into the cross and the anchor as separate objects.  Then engrave the cross and anchor together at one power and then the anchor by itself at a higher power.  Using Coreldraw to break that image up is going to be difficult as the outline trace tool isn't going to like the color gradient on the anchor.  Would need to manually trace out the anchor as a set of curves and then edit out the anchor in the image.  There might be an easier way, but I'm not aware of it.  AI would probably do a better job if you have the program.   To do variable power engraving on the fly you need to upgrade to a different controller and software. (However I think the new "K40 Whisperer" software might let you do it with the stock controller but I haven't really looked into the program yet.)  Hope this helps.


---
**Doug Western** *August 24, 2017 00:55*

Thank you, I can edit in AI or Photoshop or Fireworks, I just wasn't sure about the power settings.


---
**Mark Brown** *August 24, 2017 01:44*

K40 Whisperer still can't technically control the power level, since that isn't a function of the default hardware.  But it does support dithering, which gives surprisingly good (to me at least) results.


---
*Imported from [Google+](https://plus.google.com/102298503204290496961/posts/icvDX1K8SdY) &mdash; content and formatting may not be reliable*
