---
layout: post
title: "Just in time for Christmas, a snowflake generator!"
date: December 06, 2018 03:22
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Just in time for Christmas, a snowflake generator!



[http://paulkaplan.me/SnowflakeGenerator/](http://paulkaplan.me/SnowflakeGenerator/)





**"HalfNormal"**

---
---
**James Rivera** *December 06, 2018 05:49*

I think I like this one better. 🤔 [thingiverse.com - The Snowflake Machine by mathgrrl](https://www.thingiverse.com/thing:1159436)


---
**HalfNormal** *December 06, 2018 12:30*

**+James Rivera** There are quite a few on thingiverse that use openscad but not everybody here knows how to use it. I do agree that parametric ones are a lot better.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/ZwQsrUuy6x6) &mdash; content and formatting may not be reliable*
