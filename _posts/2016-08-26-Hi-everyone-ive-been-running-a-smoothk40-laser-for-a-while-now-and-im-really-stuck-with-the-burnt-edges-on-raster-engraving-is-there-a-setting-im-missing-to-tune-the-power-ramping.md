---
layout: post
title: "Hi everyone, i've been running a smoothk40 laser for a while now and i'm really stuck with the burnt edges on raster engraving, is there a setting i'm missing to tune the power ramping?"
date: August 26, 2016 11:14
category: "Smoothieboard Modification"
author: "Marc Rogers"
---
Hi everyone, i've been running a smoothk40 laser for a while now and i'm really stuck with the burnt edges on raster engraving, is there a setting i'm missing to tune the power ramping? i'm using visicut to generate the gcode which doesn't have an acceleration space option for smoothie. any thoughts?

![images/48bf19a193666ce41b44f0dc1c0e1dd5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/48bf19a193666ce41b44f0dc1c0e1dd5.jpeg)



**"Marc Rogers"**

---
---
**Bart Libert** *August 26, 2016 15:54*

wow thats a disaster. Do you have air assist by the way?




---
**Jeremy Hill** *August 26, 2016 17:05*

Not sure about settings, but I have had some luck using painters tape on the material and that helps reduce the burned edges.  Good luck!


---
**Marc Rogers** *August 27, 2016 18:09*

its a software thing, even if i do a whole sheet of engraving it just burns the acceleration zone, it gets better if i turn the acceleration down but it's going to shake the laser to pieces


---
*Imported from [Google+](https://plus.google.com/100818067744008604646/posts/anmYWmxhEpp) &mdash; content and formatting may not be reliable*
