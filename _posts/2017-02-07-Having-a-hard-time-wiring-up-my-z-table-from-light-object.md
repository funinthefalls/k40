---
layout: post
title: "Having a hard time wiring up my z table from light object"
date: February 07, 2017 03:20
category: "Modification"
author: "ben crawford"
---
Having a hard time wiring up my z table from light object. I also bought the recommended Mini 2 Phase 2A 1-axis Stepping Motor Driver. I am trying to use an Arduino uno to send commands to the driver but nothing is working. I don't even know if I am wiring the stepper motor to the driver correctly. Does anyone own a z tabel and have it set up correctly? Is there a better way to send commands to the controller then an Arduino uno? 





**"ben crawford"**

---
---
**Don Kleinschnitz Jr.** *February 07, 2017 04:22*

**+ben crawford** see this post. I am using LO drivers and controller but perhaps this will provide hints to using an arduino.



[donsthings.blogspot.com - Stand alone K40 Zaxis Table & Controller Build](http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)


---
**Ben Crawford** *February 07, 2017 15:26*

Mmm i was trying to use an arduino as a controller but if i get the one this post is recommending then i will get it.


---
**Don Kleinschnitz Jr.** *February 07, 2017 16:36*

**+Ben Crawford** i started down the arduino but I think the LO controller was only something like $20


---
**ben crawford** *February 07, 2017 16:41*

I ordered the ECNC-SC04 stepper controller mentioned in the article as well as the power supply listed. That way all my stuff is the same and i can use the diagrams he posted.


---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/hvFn5vNwdkU) &mdash; content and formatting may not be reliable*
