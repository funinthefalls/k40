---
layout: post
title: "So I'm about to make a purchase of a larger machine and thought I would look for affirmation from this wise and helpful group"
date: April 11, 2017 15:20
category: "Discussion"
author: "Nigel Conroy"
---
So I'm about to make a purchase of a larger machine and thought I would look for affirmation from this wise and helpful group.



So here is the machine I'm looking at

[http://www.ebay.com/itm/60W-110V-CO2-Engraver-Cutter-Laser-Engraving-Machine-w-USB-Interface-New-/252796092487](http://www.ebay.com/itm/60W-110V-CO2-Engraver-Cutter-Laser-Engraving-Machine-w-USB-Interface-New-/252796092487)?



I've been in touch with the seller and they have confirmed that you can pass through material. There'll be a $150 lift gate service charge as I'll have it delivered to a residence and not a business. 



They have a yellow version of this machine that is more expensive and they said the only difference is color!!!!



Any feedback or comments are welcome



Thanks









**"Nigel Conroy"**

---


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/eDrnoZnk7WZ) &mdash; content and formatting may not be reliable*
