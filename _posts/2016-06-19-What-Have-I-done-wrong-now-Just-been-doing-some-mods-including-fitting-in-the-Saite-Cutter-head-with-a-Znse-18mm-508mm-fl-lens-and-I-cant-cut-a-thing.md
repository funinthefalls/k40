---
layout: post
title: "What Have I done wrong now? Just been doing some mods, including fitting in the Saite Cutter head, with a Znse 18mm 50.8mm/fl lens and I can't cut a thing?"
date: June 19, 2016 17:07
category: "Discussion"
author: "Pigeon FX"
---
What Have I done wrong now? 



Just been doing some mods, including fitting in the Saite Cutter head, with a Znse 18mm 50.8mm/fl lens and I can't cut a thing?



I actually cannot even mark cardboard! 



First I though the mirrors were unaligned, so went round and checked them, all fine.



So I took the lens out and, test fired.....and that worked .......So, what ever I'm doing wrong seems to be with the lens and I though I would ask here before trying anything else (as its tea time anyway!). 



Have I just put the lens in up side down? (convex facing up, flat facing down)



Or have I mounted it in the saite cutter head wrong? (I unscrewed the inner sleeve, popped the lens inside the bottom of the head, and then screwed the inner sleeve back in till it was holding the lens in place, then inserted the whole assembly into the rectangular block.   





**"Pigeon FX"**

---
---
**Anthony Bolgar** *June 19, 2016 18:13*

Your focal point has most likely moved with the new head as the lens is most likely at a different position in the head than the stock one. I had the same issue going to the Lightobjects head. If you have a 50.8mm lens then the focal point is 50.8mm from the bottom of the lens to the work piece. Check and see what your current measurement is.


---
**Pigeon FX** *June 19, 2016 18:33*

Think the focal length is set right. I measured from the lens to the end of the air assist and then cut a wooden block to make the distance 50.8mm when the block and nozzle were touching a work peace.....and could easily be adjusted for multiple thickness of workpiece.



Also tried a few mm up and down, but could not even mark the cardboard I was testing on, using the full sweep of the power pot. 



But, will double check my mesurment.  


---
**Pigeon FX** *June 19, 2016 18:46*

Yep, measurements are good!? 



[https://goo.gl/photos/1PsmeUY59nTSqhMe9](https://goo.gl/photos/1PsmeUY59nTSqhMe9)

[https://goo.gl/photos/fKFr4R2BDq7Bs8Q59](https://goo.gl/photos/fKFr4R2BDq7Bs8Q59)

[https://goo.gl/photos/ub8hpFnj9GpQ1WsU8](https://goo.gl/photos/ub8hpFnj9GpQ1WsU8)


---
**3D Laser** *June 19, 2016 19:01*

I know I as issues when I changed my cutting head.  The beam was hitting the nozzle because the cutting head wasn't straight and the beam was to low that might be something to check out 


---
**Anthony Bolgar** *June 19, 2016 19:21*

You could always try putting the lens in with the convex down and flat side up. Won't hurt to try.


---
**Pigeon FX** *June 19, 2016 19:27*

**+Corey Budwine** I had that issue when I first installed the head, but was able to line it all using a rubber O'ring. though I didn't do any test cut then as I didn't have all the part for air assist. 



Think everything laser wise is lined up as if i take the new 18mm Znse lens out the laser will pass through and hit the bed......its just when I put that lens back in....nothing!?!? 



Wondering If i got totally the wrong lens?



[http://www.ebay.co.uk/itm/20mm-Reflection-Mirror-Mo-18mm-Znse-Lens-CO2-Laser-Stamp-engraving-40W-K40-3020-/252371628368?hash=item3ac2857150:g:2fwAAOSw9r1V-67k](http://www.ebay.co.uk/itm/20mm-Reflection-Mirror-Mo-18mm-Znse-Lens-CO2-Laser-Stamp-engraving-40W-K40-3020-/252371628368?hash=item3ac2857150:g:2fwAAOSw9r1V-67k)


---
**Anthony Bolgar** *June 19, 2016 19:32*

Could you have put one of the mirrors in backwards? It will still reflect a bit of the beam, but would render it ineffectual.


---
**Pigeon FX** *June 19, 2016 19:37*

**+Anthony Bolgar** Wish I had, but nope. I have also put the old stock head and lens back in and that works fine. 



So, its got to be something wrong with the 18mm lens, or more likely how I'm using it!


---
**Anthony Bolgar** *June 19, 2016 19:50*

If you have a 3d printer, print a spacer ring to allow you to use the original 12mm lens in the head, and see if that works. If it does, then it is a bad lens. Other than that, I am out of ideas.


---
**Anthony Bolgar** *June 19, 2016 19:51*

If you want to throw out the head, I am willing to divert it from the trash <grin>


---
**Pigeon FX** *June 19, 2016 20:16*

**+Anthony Bolgar** I think the new head actually came with a couple of milled out washers for fitting the stock lens....if not I 3D print one and give it a go.......if it enrages me so much I go to throw it in the bin, I will divert it into an padded envelope addressed to you! :)



But I hope its just a case of me doing something really wrong, or a bad lens that can be replaced.  


---
**Anthony Bolgar** *June 19, 2016 20:19*

How much travel can the tube move in the mounting block? I have always been curious about this.


---
**Mircea Russu** *June 19, 2016 21:19*

You might have gotten another focal like 101.6mm, 63.5mm or 38.1mm. Jut put a piece of paper/cardboard angled to get from 20mm from the head to over 110mm from the head (remove the bed and support it on something on the other side) and try to cut a line at 5-6ma. You might discover the real focal length of the lens.


{% include youtubePlayer.html id="j-b3pomqKkI" %}
[https://www.youtube.com/watch?v=j-b3pomqKkI](https://www.youtube.com/watch?v=j-b3pomqKkI)

Or use this method: 
{% include youtubePlayer.html id="v0znprj3xsw" %}
[https://www.youtube.com/watch?v=v0znprj3xsw](https://www.youtube.com/watch?v=v0znprj3xsw)


---
**Pigeon FX** *June 19, 2016 22:02*

**+Mircea Russu** Thanks Mircea, had no luck with cutting a line on an angle,  so took the lens out and used the second method you posted......Focal length came out as 38mm (not the 50.8mm), So my focal point is inside the air assist, and totally useless! 


---
**Anthony Bolgar** *June 19, 2016 22:45*

At least you know what the issue is and you can contact the seller for a refund or replacement. And have the knowledge that you did everything right. I should have thought of the idea that they shipped the wrong lens, I got focused on what you could have done wrong and forgot that the seller could have been the issue.


---
**3D Laser** *June 19, 2016 23:14*

**+Pigeon FX**  is the lens connected to the air assist?  






---
**3D Laser** *June 19, 2016 23:17*

One you get the cutting head all set up can you put a piece of tape where the laser should come out and test fire.  If it doesn't hit the tape your alignment is off 


---
**3D Laser** *June 19, 2016 23:18*

**+Pigeon FX** even if it is out of focus you should get a line I get a burn mark on wood five inches away and I have a 32mm focal length lens 


---
**Pigeon FX** *June 20, 2016 00:02*

**+Anthony Bolgar** Yes, I so used to it the problem being something I have done! Didn't even think to test the focal length, just assumed what was written on the box was in the box! a lesion learnt.  


---
**Pigeon FX** *June 20, 2016 18:22*

**+Corey Budwine** No burn mark at all, even right up to the nozzle with the wrong lens in.



It however dose mark (burn through tape) without the lens installed, and with the old lens in.....so think I have a problem with the lens other then it just being totally the wrong focal length. 



Have ordered another one in the right focal lenth, but its going to take a while to get here as I could't find anyone selling the right lens here in the UK, so ordered from China (would have liked to try out some LightObjects stuff, but their shipping is ridiculously height to the UK) 


---
**Anthony Bolgar** *June 20, 2016 18:38*

Pigeon, I would be willing to let you order one and have it sent to me in Niagara Falls, then as son as I get it, ship it to you for about $5-$7 Don't know if this would help or not, just thought I would offer.


---
**Pigeon FX** *June 20, 2016 19:01*

**+Anthony Bolgar** That would actually be extremely helpful if you wouldn't mind? 



Feel free shoot me over a email to info@pigeonfx.com and we can work everything out. 



And of of course, if I can ever help out send anything from Europe back across the pond, just let me know.


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/Apv8R3PC9pj) &mdash; content and formatting may not be reliable*
