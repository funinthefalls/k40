---
layout: post
title: "Here is a quick and simple set up for a z-axis adjustable bed"
date: June 22, 2016 03:41
category: "Modification"
author: "Ned Hill"
---
Here is a quick and simple set up for a z-axis adjustable bed.  This is something I just came up with to start with as I learn about the machine and will probably do something better later on.  Materials: (4) adjustable furniture glides, (4) 1/4" hex nuts, 3/4" dia wooden dowel rod, (4) #8-32 brass wood insert lock nuts, (4) #8-32 x 1/2" machine screws and associated washers and lock washers.  

In order to determine how long to cut the wood posts you need to measure from the floor of the machine to the bottom of the lens and measure the length of the glide (subtract about 4mm from the length of the glide because some will need to remain threaded into the insert).  Subtract those 2 numbers and the thickness of your bed material to get the height of the posts to cut.

 

I cut 4 lengths of 2.25” dowel and drilled holes for the furniture glide plastic insets and the wood insert lock nuts as the packaging recommended. The glide plastic inserts are just pounded in while the wood inset lock nuts have to be screwed in.  The easiest way to screw in the insert lock nuts is to make an insertion tool by cutting the head off a longer screw, add two hex nuts and thread on the insert.  Tighten the hex nuts against the insert (see pic) and chuck the other end into a drill.  Easy then to just drive the insert in and then loosed the nuts to remove the screw.  

To the machine screws going into the inserts I added a hex nut tight up against screw head just to give something to put a wrench on to hold while installing posts in the machine.  On my machine I used two holes already present but had to drill 2 new ones on one side to move away from the rail.  Added washers and lock washers to the screws and installed the posts into the holes.  Added the ¼” hex nuts to the posts of the furniture glide posts and screwed into the plastic inserts.  The added hex nuts are used to lock the screw post in place and eliminates any wiggling of the post.  

I get about 9/16” (14mm) adjustment with this set up which should be good for most materials.  I’m using an expanded metal sheet as a bed and I am locking on to the posts using small Neo magnets.  

The main drawback of this design is that you have to adjust the height of each post individually and it has limited travel.  However, it’s simple to make (only about an hour after I had the materials even with some design fiddling) and the posts cost only about $10 USD to make.



Edit - Originally I made my wooden posts 1.75" but realized I calculated wrong.  I could have cut new legs, but I decided to make longer screw posts instead using 1/4" all thread and gluing 1/4" nuts to a 1" washers to make new feet.  This now gives me the option to switch out the screw posts for the shorter ones if I have something taller I want to engrave.  Also using a small a 6" combination square to set post height.



![images/c581ef5cddaf9347b732783a44d05642.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c581ef5cddaf9347b732783a44d05642.jpeg)
![images/a5969d76bb5701180b2853b8a71d8c53.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a5969d76bb5701180b2853b8a71d8c53.jpeg)
![images/6d5b427210e1fc300e65e5066dff60f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d5b427210e1fc300e65e5066dff60f0.jpeg)
![images/a65e795078d7d822b55d8dfcb148e276.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a65e795078d7d822b55d8dfcb148e276.jpeg)
![images/13f7e44c6efb67c2930f29bd76311ef9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/13f7e44c6efb67c2930f29bd76311ef9.jpeg)
![images/b8270c5c88ed1206a6b50ebffc53e073.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8270c5c88ed1206a6b50ebffc53e073.jpeg)
![images/418f2447ef1c5f06780dbff9b95b3050.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/418f2447ef1c5f06780dbff9b95b3050.jpeg)

**"Ned Hill"**

---
---
**Jim Hatch** *June 22, 2016 04:06*

Nice simple design. I'd make a couple of sets of spacers - nylon bushings cut in half so you can slip them over the bolts. Make a set for the materials you use mist commonly that leaves the bed at the right focal length. Then when you're adjusting it you just have to spin the glides out far enough to slip the bushings in (which are now U shaped since you cut them in half lengthwise).


---
**Ned Hill** *June 22, 2016 04:07*

**+Jim Hatch** Great idea, thanks!


---
**Pippins McGee** *June 22, 2016 06:46*

**+Ned Hill** I like it Ned.

Where did you buy the mesh material? and do you have any problem with laser reflection to the back of the materials you are cutting?

The aluminium honeycomb bed I made has support bars running along the bottom of the honeycomb, even though it is 10mm distance away from the material, it still reflects back and burns underside of material I cut.



is your material any different in reflection properties compare to flat aluminium?


---
**Ned Hill** *June 22, 2016 12:55*

**+Pippins McGee**  I obtained the mesh from Home Depot (local big box home improvement store).  I do get some back burn but it's not too bad, still experimenting.  The mesh is mild steel which will have a lower reflectance than Aluminium (Al).  For you, I would try sanding your Al with like a 200 grit sandpaper.  Roughing the surface will increase absorption (less reflectance) by the Al and also cause scattering which will make the return bounce beam more diffuse.  Here's a chart of reflectance vs sandpaper grit for Al and Cu. [https://1drv.ms/i/s!AtpKmELEkcL8i3prOOcsnaoaSCZm](https://1drv.ms/i/s!AtpKmELEkcL8i3prOOcsnaoaSCZm) 


---
**Michael Otte** *June 22, 2016 23:34*

Good idea, i have been considering using a Laboratory Scissor Jack with a twist knob that moves up and down.  it goes down to 3inches - thinking about extending the knob out the front of the machine with a longer screw bolt ($30 on amazon)


---
**Pippins McGee** *June 23, 2016 01:41*

**+Michael Otte** I did this the other day btw. (with the exception of removing the short screw and replacing it with a longer one so that it can stick out the front of the machine. Haven't done that part YET because

1) haven't worked out how to remove the current screwbar yet. doesn't just keep unwinding until it falls out. may need to drill it out or something, and

2) placing a 12"x12" bed on top of the 4" top plate of the scissor jack makes for a wobbly bed that is very hard to keep level for me...

Still thinking of ideas, maybe need to glue a 12" plate to the 4" plate, so that my 12x12" bed isn't just balancing on such a small surface area in the middle.

3) also the bottom of my k40 machine isn't flat, the sheet metal has a bulge in the middle that flexes out, and then in if you put enough pressure. so the scissor jack itself may not be sitting on a level surface if placed in the middle.

in theory I have faith it can be made to work well but little bit of work and creativity required...



i am probably going to put the scissor lab jack aside and do what ned has suggested in this post even though it isn't as easily adjustable.


---
**Jim Hatch** *June 23, 2016 03:07*

Actually depending on what thickness materials you usually use a dozen bolts and some super magnets would work. I usually only do 1/8, 1/4, 3/4" stock (the latter is engrave only).



A set of bolts with one of those round super strong magnets from Home Depot (US based large hardware & building supply store) glued to the head in the right length (a Dremel with a cutoff wheel may be needed to trim the bolt to length) and a piece of expanded aluminum on top would work.



Or just leave the magnets in place against the base and peel off the bolts when switching heights. Might want to go with 6 or 7 of each size for support.



To change the heights just swap out the bolts. I'm thinking that they just lay down like chess pieces when captured and are left in the bottom - tip over one set and flip up the other and put the top plate back on. Should be less time to switch than diddling around with an adjustable table. For the times I do some other thickness I'll cut a support out the height I need. If I were doing more different thickness pieces I might feel differently. But right now simple bolt sets work.


---
**Tony Sobczak** *June 24, 2016 18:58*

Follow


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/hGcsG3Hav33) &mdash; content and formatting may not be reliable*
