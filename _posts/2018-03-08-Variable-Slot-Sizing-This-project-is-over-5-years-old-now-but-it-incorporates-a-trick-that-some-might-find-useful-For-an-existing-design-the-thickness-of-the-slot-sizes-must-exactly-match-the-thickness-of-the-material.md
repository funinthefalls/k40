---
layout: post
title: "Variable Slot Sizing (This project is over 5-years old now, but it incorporates a trick that some might find useful.) For an existing design, the thickness of the slot sizes must exactly match the thickness of the material"
date: March 08, 2018 16:46
category: "Object produced with laser"
author: "Nate Caine"
---
<b>Variable Slot Sizing</b>



<i>(This project is over 5-years old now, but it incorporates a trick that some might find useful.)</i>



For an existing design, the thickness of the slot sizes must <i>exactly</i> match the thickness of the material.  For example, the 5mm thick foam core dinosuar (photo) must match 5mm wide slots in the design.



But what happens if you change to 3mm thick acrylic?  <i>(That won't match the 5mm slots!)</i>  

And what happens when you scale your design by 80%?   <i>(Your drawn 5mm slots are now 4mm when cut.)</i>  



What do you do?



<b>***</b>



For this project I downloaded a dinosaur from Instructables (I think).  Using CAD software, I created a generic "slot" library part, and I overlaid each exiting slot in the original design with the library part.  I duplicated the dinosaur on a new layer and then manually edited out all the original slots.  <i>(It's not at tedious as it sounds.)</i>  This became <b>Layer0</b> in my system.



The generic "slot" library part is actually a <i>stack</i> of several rectangles, each on a different layer.  The <b>length</b> of the rectangle match the length in the original design.  The <b>width</b> of the rectangle is different.  For simplicity I chose to make Layer1 to be 1mm wide.  Layer2 was 2mm wide.  Layer3 was 3mm wide, etc. 



<b>***</b>



To use:



You simple turn on <i>only</i> <b>Layer0</b> (the dinosaur) and a <i>single</i> additional layer--in this case <b>Layer5</b> <i>(to create 5mm slots since I was using 5mm foam core)</i>.  Using your existing laser cutter software, sequence to have <b>Layer5</b> (the slots) cut out <i>first</i>, then <b>Layer0</b> (the dinosuar) cut out <i>last</i>.  Your design is done.



Should you choose to use 3mm acrylic, simply use <b>Layer0</b> (dinosuar) and <b>Layer3</b> (3mm slots).  This assumes at full scale, 100% size dinosaur.



Suppose you wanted to scale the design by 80%.  That means if you chose <b>Layer5</b> (5mm) it will cut at an actual size of 4mm (80% of 5mm), so the result would work well for 4mm thick material.



Suppose you scaled the dinosaur by 50% (because you like that size dinosaur), and intended to use 3mm acrylic.  Then choose <b>Layer0</b> (dinosaur) and <b>Layer6</b> (6mm).  That means the drawn 6mm slots, when scaled by 50%, will correctly cut at the desired 3mm.



Obviously there's a lot of limitations here, but you can create a "slot" stack with a lot more sizes than I've shown in this example.  A little effort when you create your design will pay off later should you have a popular design that you re-use at a variety of scales or with different thickness materials.



Enjoy!





![images/8356777b663c742ef88c77c1fd322c05.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8356777b663c742ef88c77c1fd322c05.jpeg)
![images/ef4c5f16836538c91074decf4652f18e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef4c5f16836538c91074decf4652f18e.jpeg)

**"Nate Caine"**

---
---
**Don Kleinschnitz Jr.** *March 08, 2018 17:08*

Nice technique, thanks for sharing.



Recently I have been learning F360. A unique element of its architecture is "parametric design". All the parameters of the design can be defined in a table of variables that then can be assigned to objects in the design. Objects within the design can then be constrained in various ways with each other. 

When parameters and constraints are setup correctly a simple change in a parameter changes the entire design. So a change in thickness can change all the geometries that are associated with that parameter.



I wonder if this would also work for this application?


---
**Nate Caine** *March 08, 2018 17:23*

**+Don Kleinschnitz** The Fusion360 approach is a lot more flexible, but also more complicated for our end users.  We had several canned demo designs on hand at the laser computer for people to experiment with and share.



When a Girl Scout troup stopped by, it was simple enough to grab some material from the scrap bin, chose the DXF file of a design they liked, then select the appropriate layers.  Cut and done.



(BTW, SolidWorks supports parametric design as well.)


---
**Nate Caine** *March 08, 2018 18:27*

Here's the missing pic...

...<i>(because G+ is too F'd up to allow me to add or update pics in the original post.)</i>![images/3a2ab0fdbcc46b2b51981cc7c104f814.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3a2ab0fdbcc46b2b51981cc7c104f814.jpeg)


---
*Imported from [Google+](https://plus.google.com/107379565262839168986/posts/MC2qs9dQdit) &mdash; content and formatting may not be reliable*
