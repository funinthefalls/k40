---
layout: post
title: "A winosaur. Saw this on Instructables. Pretty interesting concept"
date: April 23, 2017 08:48
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
A winosaur. Saw this on Instructables. Pretty interesting concept. Thought maybe some might be interested in making something similar for themselves (or gifts) using their K40s.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Cesar Tolentino** *April 23, 2017 15:03*

Haha. Cute.


---
**Miguel Angel Cubides Gonzalez** *April 26, 2017 23:01*

it's so clean!!!, how can I do it? I always burn the wood!!! did you changed the laser tube?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2017 04:00*

**+Miguel Angel Cubides Gonzalez** This is not something I did. I found it on Instructables. The guy who did it actually used a jigsaw or scroll saw to cut the pieces. I just thought it would be something others may be interested in making.



edit: I also always burn the wood, especially if it is thick like the wood used in this project.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/7GMNEF5b15D) &mdash; content and formatting may not be reliable*
