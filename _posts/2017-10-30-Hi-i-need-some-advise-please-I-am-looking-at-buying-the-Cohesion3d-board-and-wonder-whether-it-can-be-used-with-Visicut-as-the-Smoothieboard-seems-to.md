---
layout: post
title: "Hi, i need some advise please. I am looking at buying the Cohesion3d board and wonder whether it can be used with Visicut as the Smoothieboard seems to"
date: October 30, 2017 23:35
category: "Smoothieboard Modification"
author: "Bernd Peck"
---
Hi, i need some advise please. I am looking at buying the Cohesion3d board and wonder whether it can be used with Visicut as the Smoothieboard seems to.

Thanks

Peter





**"Bernd Peck"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2017 02:24*

Yep. And Lightburn which is coming out soon, and any other program that outputs gCode - so if you want to spend on Vectric and Photograv you're welcome to do so :)

But most of our users and documentation are on LaserWeb. 


---
**Bernd Peck** *October 31, 2017 07:12*

Thanks for your help


---
*Imported from [Google+](https://plus.google.com/100238441539373475210/posts/2n5gEodrAfo) &mdash; content and formatting may not be reliable*
