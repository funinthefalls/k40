---
layout: post
title: "Hello everyone, I just bought a second-hand K40 machine, and am starting to get to grips with it"
date: April 26, 2015 18:40
category: "Discussion"
author: "Chris M"
---
Hello everyone,



I just bought a second-hand K40 machine, and am starting to get to grips with it. Everything seems to work OK, I have cut some basic shapes in card, wood and perspex using Corel DRAW v12, but engraving has me a bit confused. Although the dialog is set to engrave at 250mm/sec, the machine moves very slowly, taking 14sec to cover a raster line of about 50mm length. Am I missing something, or is this just the way it is? I also have the LaserDRW and LaserSeal programs, but I've only taken a quick look at them and think I'd prefer not to use them.



Chris





**"Chris M"**

---
---
**Sean Cherven** *April 26, 2015 19:24*

I haven't tried engraving from CorelDraw yet, but I know in LaserDRW it runs really fast. Like 2 lines per second fast.


---
**Jon Bruno** *April 26, 2015 20:07*

I ran into the very same issue today. Also cutting runs along at over 100mm/s regardless of what you set it to in laserdrw after Corel sends it over.

I hope someone has an aswer to this one too.


---
**David Wakely** *April 26, 2015 21:44*

I am using Corel Draw X7 on Windows 7 and the CorelLASER plugin. Rastering seems to be fine. Could it be a compatibility issue with your Corel version?


---
**Jon Bruno** *April 26, 2015 21:52*

I'm using whatever came from China on the bootleg dvd


---
**David Wakely** *April 27, 2015 07:44*



Try downloading the latest version of CorelLASER from the manufacturers



[http://www.3wcad.com/download.asp](http://www.3wcad.com/download.asp)



It's all in Chinese but it's the first download option




---
**Chris M** *April 27, 2015 11:06*

I'll give LaserDRW a try. I was wanting to use CorelDraw (aka CorelLASER, I think) on both my desktop PC for design work and on the netbook I'm using to control the cutter. Looks like the first move is to attach a bigger monitor to the netbook.



I don't think there's a CorelDRAW incompatibility, as it's what came in the "Laser Cutter -Easy" zip file which I think came from someone in this forum, but I haven't ruled that out.



I have CorelLASER 2013.02, so that is up to date.


---
**Jon Bruno** *April 27, 2015 11:16*

LaserDRW behaves properly when cut or engrave jobs are sent directly from there.

I think CorelLaser is overriding the settings in the job somehow. I also notice that the machine information is corrupted when viewed via corel. But looks fine when queried from laserdrw.

Is yours doing this too?﻿


---
**Sean Cherven** *April 27, 2015 13:08*

I made the Laser Cutter-Easy, I named it easy because I removed all the Chinese text and apps that were in Chinese, that have no real use to us.



The LaserDRW and CorelLaser plugin were both up to date when I posted the file.



The Chinese website says that CoralLaser is compatible with up to X6, so maybe X7 is not compatible? 



I'll test CorelLaser on CorelDRAW X6 later today, and see if I have this issue.



And just a fyi, I get the same corrupted machine info from CorelDRAW X6 on my machine also. 

I think it's more Chinese text without the Windows language pack installed, than a corruption issue. But I could be wrong.


---
**Jon Bruno** *April 28, 2015 02:47*

Any luck trying it out ?


---
**Sean Cherven** *April 28, 2015 02:55*

Oh wow, I totally forgot to test it out today. I'll try to remember to do it tomorrow. Sorry about that


---
**Sean Cherven** *April 29, 2015 16:34*

Okay, I tested it out last night, and raster works at the proper speed in CoralDraw. So i'm not sure why your having the issues.


---
**Jon Bruno** *April 29, 2015 19:05*

Ok thanks for checking.

I'm building a fresh Windows 7 machine right now. I hope to test tonight and see if it's just my little crappy laptop causing the issues


---
**Jon Bruno** *April 30, 2015 11:21*

I'm still experiencing the same issue.

Question, does laserdrw interpret colors for functions? I.e. red =cut ... Or are there predefined settings based on color somewhere? Maybe that's the problem I'm having? Whatever the case, Corel imports still  override the manual settings.﻿


---
**David Wakely** *April 30, 2015 12:00*

As far as i know it interprets everything other than white as cut/engrave. It cannot do color seperation


---
**Jon Bruno** *April 30, 2015 20:22*

I just read somewhere someone mentioning the same issue. his resolve was to note and confirm the main board setting is correct when using corel. apparently they can change between CorelLaser and Laserdrw. I'm still at work But I will check this out when I get home to see if thats the problem I'm having.


---
**Jon Bruno** *April 30, 2015 22:37*

yup that was the fix.. make sure you select the proper board model (mine is M2) also fix the device ID with the one on your board.


---
**Chris M** *May 01, 2015 19:38*

Jon, thanks for that. It fixed my issue too. Now on to the next set of challenges.



And Sean, thanks for posting the Easy zip file, it's very useful.



Chris


---
**Sean Cherven** *May 01, 2015 19:43*

Your welcome Chris.


---
**Jon Bruno** *May 01, 2015 21:26*

You bet man! Glad it worked for you too.


---
*Imported from [Google+](https://plus.google.com/108727039520029477019/posts/VagQUY4gaNX) &mdash; content and formatting may not be reliable*
