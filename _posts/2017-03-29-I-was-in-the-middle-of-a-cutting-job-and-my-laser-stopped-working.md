---
layout: post
title: "I was in the middle of a cutting job and my laser stopped working"
date: March 29, 2017 20:55
category: "Hardware and Laser settings"
author: "josh mozug"
---
I was in the middle of a cutting job and my laser stopped working. The lens head was still moving, so I know the electronics are still working in that regard, but the lens itself stopped firing. The gauge also stopped displaying the mA. I don't know if my laser tube needs replaced or if I might be experiencing an electrical issue. Any ideas?





**"josh mozug"**

---
---
**greg greene** *March 30, 2017 00:00*

Could just be HV Portion of Power supply - or the connection to the tube


---
**Phillip Conroy** *March 30, 2017 06:45*

Check for cracks in tube,are you sure waterpump was pumping?


---
**Stevie Rodger** *March 30, 2017 09:28*

I had the same fault, I changed the tube, but still the same, I then changed the power supply, and it worked great.

The low voltage was still working, allowing the head to move etc, But the high voltage was gone, stopping the tube firing.

Hope this helps.


---
**josh mozug** *March 30, 2017 13:30*

**+Phillip Conroy**  yeah there's no indication of the tube being cracked and water seems to be flowing fine!


---
**josh mozug** *March 30, 2017 13:33*

**+Stevie Rodger** where did you find a new power supply? I had searched Amazon and eBay and had a tough time finding one. Any suggestions on how to ensure that that in fact is the problem? Thanks!


---
**Stevie Rodger** *March 30, 2017 13:36*

Sorry I have a 3 year warranty, so I got mine from the supplier.  

There isn't any way to test the high voltage side unless you have specific test equipment, 

But if everything else is ok. It looks like the power supply. 


---
**josh mozug** *March 30, 2017 13:48*

Ah OK, will look into that more!


---
**Don Kleinschnitz Jr.** *March 30, 2017 19:57*

**+josh mozug** 

Is this a standard K40?



Please post a picture of your supply:



1.) will the laser fire if you push the "Test Sw" with the "Laser Switch" engaged?

2.) does the led on the supply come on?

3.) does the laser fire if you push the test switch on the laser power supply with the "Laser Switch" engaged?



Do you have and know how to use a DVM? 


---
**josh mozug** *April 01, 2017 00:54*

1 - the laser will not fire when i push test

2 - the led does come on

3 - the laser does not fire

and i'm not familiar with DVMs.





![images/ffbe88d6d85ba0467324d5dd2cf57c93.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ffbe88d6d85ba0467324d5dd2cf57c93.jpeg)


---
**Stevie Rodger** *April 01, 2017 15:19*

the sympums you have, were the same as mine, and the power supply was at fault. My tube was fine. Hope this helps you get the machine back in working order.

Good luck


---
**josh mozug** *April 01, 2017 15:58*

**+Stevie Rodger** thanks for your help! I'll give that a try here!


---
**Frank Jones** *August 08, 2017 17:19*

I bought mine used and I had the same problem the stepper motors moved but the meter would be eradicate or just zero. I found one of the leads on the potentionmeter 

was loose. I would start looking to make sure nothing is loose. 


---
**Don Kleinschnitz Jr.** *August 10, 2017 14:15*

This sounds like a bad supply. The test switch on the lps will fire the laser at whatever power the pot is set at irrespective a any other controls.



You can try one more thing. Tie the "IN" pin to 5v and push the test button on the lps to see if it fires. Warning it will fire at full power irrespective of interlocks etc. 


---
*Imported from [Google+](https://plus.google.com/114871982644693721973/posts/7RZjh2cKrX2) &mdash; content and formatting may not be reliable*
