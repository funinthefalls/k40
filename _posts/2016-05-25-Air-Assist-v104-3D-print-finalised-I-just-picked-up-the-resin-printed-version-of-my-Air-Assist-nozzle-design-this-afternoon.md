---
layout: post
title: "Air Assist v1.04 3D print finalised. I just picked up the resin printed version of my Air-Assist nozzle design this afternoon"
date: May 25, 2016 09:11
category: "Air Assist"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Air Assist v1.04 3D print finalised.



I just picked up the resin printed version of my Air-Assist nozzle design this afternoon. I have yet to test it, however I am fairly impressed with the precision in the air pathways (the visible ones at least). Hopefully in the next few days (or maybe even later today) I can get some tests run to determine if it functions as desired.



UPDATE: [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/iTbTG7u75DC](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/iTbTG7u75DC)



![images/200c2090721ed9273acf7bc5ef773063.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/200c2090721ed9273acf7bc5ef773063.jpeg)
![images/4949ff5af93896ab439370877ea5b665.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4949ff5af93896ab439370877ea5b665.jpeg)
![images/b2ebc223632e9248ee74092a601e1704.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2ebc223632e9248ee74092a601e1704.jpeg)
![images/c48ea4a51172cc655c9a5a1bc41692e7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c48ea4a51172cc655c9a5a1bc41692e7.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Phillip Conroy** *May 25, 2016 09:49*

Looks good,hope it works for u


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 09:52*

**+Phillip Conroy** Thanks, I'll report back results when I get around to testing it. I don't feel like going downstairs to the laser now because it's cold here tonight.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 16:09*

UPDATE: I've assembled it & will post link in the top to show assembled.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/QdNWz8SC3iy) &mdash; content and formatting may not be reliable*
