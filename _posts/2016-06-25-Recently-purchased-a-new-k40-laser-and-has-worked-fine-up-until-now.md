---
layout: post
title: "Recently purchased a new k40 laser and has worked fine up until now"
date: June 25, 2016 04:59
category: "Discussion"
author: "Austin Bowden"
---
Recently purchased a new k40 laser and has worked fine up until now. Laser functions as should but all of a sudden is now etching at half scale of what size I send it as. Ex. If I set my laser layout as 150mm in laser draw the laser will print the image as 75mm. Not sure if I accidentally changed a setting but I can't seem to find anything that would change the scale of the image to the laser. Does anybody know what might be causing this issue?





**"Austin Bowden"**

---
---
**Anthony Bolgar** *June 25, 2016 05:29*

Are you using the stock controller card?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 06:48*

I am pretty sure I read someone else had similar issue a while back. If I recall correct it is related to not having the correct Board Model set or not having the correct Board ID set. Check those two in the Device Initialise settings of CorelLaser (both values can be found on your controller board). Or if you are using LaserDRW, I can't say what would be causing it as I don't use it.


---
**Jim Hatch** *June 25, 2016 13:43*

**+Yuusuf Sallahuddin**​ Same issue with LaserDRW as Corel. You have to change the machine settings in both - it's not updating a single centralized settings place (even though they look the same). 



But I agree. Seems likely the Machine info is wrong.


---
**Austin Bowden** *June 26, 2016 01:31*

Thanks for the quick responses will check to see that those are set correctly and go from there


---
*Imported from [Google+](https://plus.google.com/111690874111370432441/posts/K48Vux3899Q) &mdash; content and formatting may not be reliable*
