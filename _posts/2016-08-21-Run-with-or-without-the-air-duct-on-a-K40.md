---
layout: post
title: "Run with or without the air duct on a K40?"
date: August 21, 2016 18:51
category: "Modification"
author: "Jez M-L"
---
Run with or without the air duct on a K40?





**"Jez M-L"**

---
---
**Robi Akerley-McKee** *August 21, 2016 22:10*

Middle of warehouse... kinda hard to vent outside


---
**Bob Damato** *August 23, 2016 11:25*

Depends what Im doing. plastic/wood I vent. Granite I do not


---
**Jez M-L** *August 23, 2016 16:19*

I was on about taking out the duct on the inside of the machine. Still gonna vent the system as you be liable to be in hospital after breathing the crap in!!


---
*Imported from [Google+](https://plus.google.com/103448790120483541642/posts/UqjJvPfaSoE) &mdash; content and formatting may not be reliable*
