---
layout: post
title: "I ordered a GRBL CNC shield recently, my question is the following: Do i need this middle man board?"
date: January 14, 2017 15:08
category: "Modification"
author: "Krist\u00f3f Gilicze"
---
I ordered a GRBL CNC shield recently,  my question is the following:

Do i need this middle man board? Anyone has a setup with this board and uno cnc shield? or i can just wire everything directly to my board? (i guess so)







**"Krist\u00f3f Gilicze"**

---
---
**Don Kleinschnitz Jr.** *January 14, 2017 16:03*

The Middleman is a connector converter. It allows you to convert the white ribbon cable and FFC style connectors to more commonly available interconnects. It facilitates interfacing with the gantry electronics.



[http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html)



[donsthings.blogspot.com - A Total K40-S Conversion Reference](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)






---
**HalfNormal** *January 15, 2017 04:41*

Ditto what Don said. You can rewire or connect directly to the ribbon cable if you think you will never need it again. I purchased the ribbon connectors for about 50 cents a piece. See Don's blog for ordering info.


---
*Imported from [Google+](https://plus.google.com/107969900550483335330/posts/f8Qd86CAWro) &mdash; content and formatting may not be reliable*
