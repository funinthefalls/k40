---
layout: post
title: "I have struggled with an issue with my K40"
date: October 07, 2016 02:54
category: "Discussion"
author: "timb12957"
---
I have struggled with an issue with my K40. Through other posts we have not been able to resolve the issue. So I have been attempting to communicate with the seller who promoted his 2 YEAR WARRANTY as a selling point. They made a few poor suggestions for a resolution. One was sending me a partial refund an I keep the non working laser. Not what I want at all. When I began requesting that they allow me to return it for a replacement, they went silent. In the last week, I have emailed them 4 times, and they do not reply at all.  As I suspected when I bought this machine, a promise of a 2 year warranty is only worth the cyber space it is written on!  But I had to wonder, has anyone from the community purchased from a seller who actually honored a warranty claim outside the first 30 days? If so, I would like their name since I may wind up buying another new one.





**"timb12957"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2016 03:45*

How long ago did you buy the machine? You can open a PayPal dispute up to 180 days after purchase. Something like that for credit cards too if you want to jump right in to a chargeback. 


---
**timb12957** *October 07, 2016 03:49*

It was purchased July 3 this year. So just over 90 days. But can a PayPal dispute be opened because seller fails to honor a warranty?


---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2016 03:53*

"Item not as represented" or defective. 

Chargeback on a cc is much more effective. 


---
**timb12957** *October 07, 2016 03:56*

Not sure that can work. My PayPal account is funded by my bank account.  My Visa debit card is on the PayPal account as a backup method of payment. But since the funds were in my bank account, I don't think a cc chargeback could be done.


---
**Ariel Yahni (UniKpty)** *October 07, 2016 04:06*

I would just issue a claim and let paypal decide. You must have proof they stoped responding and prepare to argue. Im very confident this sellers get their inssurence to pay for this stuff


---
**greg greene** *October 07, 2016 12:26*

If it's a Chinese seller - they are on their annual 2 week vacation - maybe they'll return your call after that


---
**timb12957** *October 07, 2016 15:17*

That is very helpful info Greg. It is a Chinese seller.  I like the vacation explanation as to why they stopped responding, rather than thinking they have decided to abandon me and my broken machine. Time will tell.


---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2016 15:18*

Yeah, give them till after the weekend, although their holiday was supposed to be over today.  But then they'll be inundated with emails from everyone else too once they get back.


---
**greg greene** *October 07, 2016 15:18*

For some reason - It's like the whole country shuts down for a week or two.


---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2016 15:19*

**+greg greene** and this happens there every single season! But seeing as how they work unimaginable hours 6 or more days a week, it's understandable why it has to happen.


---
**Vince Lee** *October 07, 2016 19:48*

Beware.  I had a dispute with a Chinese seller in which they sent the wrong size item.  In order to receive a refund, PayPal required that I send the item back to the seller at my expense via trackable post.  I found it would have cost me $45 to do so.  The item only cost $10 so paypal was useless in that case.  For a large item like a k40 it would probably be more worthwhile to sell it for parts.  There are a lot of really smart and helpful people on this site, however, so with a little effort I'm sure you can fix the problem and get the seller to reimburse you for any replacement parts.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/EhUzo9JbLc3) &mdash; content and formatting may not be reliable*
