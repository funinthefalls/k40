---
layout: post
title: "Can someone please provide me with the various voltage readings I should get from the terminals on my standard power supply?"
date: January 28, 2017 16:05
category: "Hardware and Laser settings"
author: "timb12957"
---
Can someone please provide me with the various voltage readings I should get from the terminals on my standard power supply? 





**"timb12957"**

---
---
**Anthony Bolgar** *January 28, 2017 16:07*

There should only be 2 DC voltages that you can measure 5V and 24V. Do not attempt to read the voltage of the high voltage connection to the tube.


---
**timb12957** *January 29, 2017 13:35*

So none of the other terminals can be tested to be working properly?


---
**Don Kleinschnitz Jr.** *January 29, 2017 14:45*

Sure they can:



Look here and see if the values in  brackets [ ]  help?

If not let me know.... 

I do not remember what vintage your LPS is.



Are you having a LPS issue I know that you have other issues.




---
**timb12957** *January 30, 2017 03:51*

DON, I just wanted to test my LPS for any potential problems that might be a contributing factor to my OTHER issue. Your post above, Not sure how to identify my vintage, my laser was purchased July 2016. I was expecting a link in your "Look Here"  comment? I found nothing to look at?


---
**Don Kleinschnitz Jr.** *January 30, 2017 04:45*

**+timb12957** LOL did not leave you a link.



[http://donsthings.blogspot.com/2017/01/k40-lps-configuration-and-wiring.html](http://donsthings.blogspot.com/2017/01/k40-lps-configuration-and-wiring.html)


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/VqZzQm4Wo8o) &mdash; content and formatting may not be reliable*
