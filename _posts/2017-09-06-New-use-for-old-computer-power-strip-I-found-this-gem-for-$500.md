---
layout: post
title: "New use for old computer power strip I found this gem for $5.00"
date: September 06, 2017 01:03
category: "Modification"
author: "HalfNormal"
---
New use for old computer power strip



I found this gem for $5.00. I am able to turn on all my peripherals separately.

 (water pump, air assist, smoke evac, ect)

![images/3e31117c419b4010a6c198eb56a462ec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3e31117c419b4010a6c198eb56a462ec.jpeg)



**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/46wRydoA8kx) &mdash; content and formatting may not be reliable*
