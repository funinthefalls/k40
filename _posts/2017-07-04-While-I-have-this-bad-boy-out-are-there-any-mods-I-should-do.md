---
layout: post
title: "While I have this bad boy out are there any mods I should do?"
date: July 04, 2017 21:15
category: "Modification"
author: "William Kearns"
---
While I have this bad boy out are there any mods I should do?

![images/34ef68943776dc16ff0e20b5b11bebee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/34ef68943776dc16ff0e20b5b11bebee.jpeg)



**"William Kearns"**

---
---
**Ariel Yahni (UniKpty)** *July 04, 2017 21:20*

Make sure it's square as possible. That's the most important mod


---
**William Kearns** *July 04, 2017 21:38*

**+Ariel Yahni**  I put a square in it and it looks about .9mm off any suggestions on how to square it up![images/b00a4119bf27a2e661fda3257e63e290.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b00a4119bf27a2e661fda3257e63e290.jpeg)


---
**Ariel Yahni (UniKpty)** *July 04, 2017 21:45*

Mind that squerness of the gantry it just a  half the story. 



Every piece is different due to the flex of the case so you can also shim it.


---
**William Kearns** *July 04, 2017 21:49*

**+Ariel Yahni** what part would I shim? The blac frame is what's a bit out of square?


---
**Ariel Yahni (UniKpty)** *July 04, 2017 21:54*

It all depends, I cannot tell you where but a small disaligment will cause power loss.



So if you are hitting the center when aligning the lens and able to get fast cuts with small power you are ok


---
**Chris Hurley** *July 04, 2017 22:12*

Is this normal for the machine to need to be squared up? 


---
**William Kearns** *July 04, 2017 22:13*

Flexing the frame I was able to bring it back pretty darn close


---
**Ariel Yahni (UniKpty)** *July 04, 2017 22:21*

**+Chris Hurley**​ you would be surprised of the horror stories out there


---
**Jorge Robles** *July 05, 2017 05:58*

Take advantage and strip fume duct  or cut its beak 2inches. Also if you want cut the bottom of the case if you foresee big pieces to be used.


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/GCsBL5LGD8M) &mdash; content and formatting may not be reliable*
