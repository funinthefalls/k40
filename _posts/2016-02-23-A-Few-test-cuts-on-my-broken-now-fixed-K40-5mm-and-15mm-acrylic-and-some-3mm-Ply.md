---
layout: post
title: "A Few test cuts on my broken now fixed K40, 5mm and 15mm acrylic and some 3mm Ply"
date: February 23, 2016 02:14
category: "Object produced with laser"
author: "The Technology Channel"
---
A Few test cuts on my broken now fixed K40, 5mm and 15mm acrylic and some 3mm Ply.



![images/8fc9bb45960a628a669f7f7f30c80d54.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8fc9bb45960a628a669f7f7f30c80d54.jpeg)
![images/61a1c9323d90d234942ef58a43f68014.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/61a1c9323d90d234942ef58a43f68014.jpeg)
![images/24eaf7e98d2dc6a98619d1115373b679.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/24eaf7e98d2dc6a98619d1115373b679.jpeg)
![images/ec15dc9d4beb9c02f28c8e7127fc448a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec15dc9d4beb9c02f28c8e7127fc448a.jpeg)

**"The Technology Channel"**

---
---
**Kelly Burns** *February 23, 2016 02:21*

You are making me sad! Can you give the settings for each of these?  I keep going backwards.  What was wrong with your laser? 


---
**The Technology Channel** *February 23, 2016 02:47*

It came from china this morning and the laser head was smashed off, large dent in the case and table bent up.... so had some work to do today...





You can cut 5mm at around 5-10mm p/s at 15ma, 15mm is a bit tougher thou, I got through it on the 3rd attempt by cutting at 0.5mm/ps and 15-16ma...

And plywood 3mm at 10mm/ps and 12ma

Etching my dog I did at 150mm/ps and 10ma because I wanted it quite deep...

These are only my tests so if they are wrong.....



Time to order some mirrors and a new lens and the all important air assist, me thinks...





Does anyone use coral laser, mine seems to crash when selecting the object manager..

Laser draw is ok, but I don't think it supports layers..??


---
**The Technology Channel** *February 23, 2016 02:49*

Why are you sad Kelly?


---
**Kelly Burns** *February 23, 2016 02:53*

happy for you, Sad for me. I'm really struggling with the laser.  I have built 2 CNCs (one from scratch), but Im really struggling with getting the optics right on my K40.  Thought I was close last week, but seem to keep going backwards.


---
**Kelly Burns** *February 23, 2016 03:02*

Thanks for the info Nick. Mine is definitely not where it should be power wise.  I thought I was getting close but seem to be going backwards.



Mine has a Moshi board and the MoshiLaser software.  I have a ton of the same kind of issues with crashing.  If I can get optics right, I'm going to upgrade to Ramps/Marlin setup or possible even a DCS.


---
**3D Laser** *February 23, 2016 03:26*

how in the world are you getting through something that thick!? i can't even get through 1/8 stuff


---
**The Technology Channel** *February 23, 2016 09:45*

I was glad mine didn't come with the moshi software, remember even thou these things are ment to be 40w they are closer to 20w. I aligned mine in prob 30mins.

All of the mirrors came off after I got it to first clean them, and then adjust them one by one... I will order a laser power meter and get some accurate power mesurments.


---
**The Technology Channel** *February 23, 2016 09:53*

Corey, its all down to speed and power, all those items were cut with a single pass.


---
**Joe Keneally** *February 23, 2016 14:56*

So how slow are you cutting?  I too am having issues with thicker materials like 1/8 inch melamine.


---
**The Technology Channel** *February 23, 2016 15:33*

For 5mm I am cutting between 7 and 10mm per second


{% include youtubePlayer.html id="XnwNJdum31Q" %}
[https://www.youtube.com/watch?v=XnwNJdum31Q](https://www.youtube.com/watch?v=XnwNJdum31Q)



For some reason now I have upgraded to coraldraw 5x it makes 2 passes. The first pass it cuts it out fine no need for a second pass.... still playing

One other thing make sure your lens is the right way up, mine was reversed when I got it....


---
**The Technology Channel** *February 23, 2016 16:07*

And just for kicks


{% include youtubePlayer.html id="RUSFWWDaurU" %}
[https://www.youtube.com/watch?v=RUSFWWDaurU](https://www.youtube.com/watch?v=RUSFWWDaurU)



15mm cutting with a single pass!!


---
**3D Laser** *February 23, 2016 16:29*

**+nick aldridge** what version on the k40 do you have and what are the cutting settings


---
**The Technology Channel** *February 23, 2016 16:52*

The one that's pictured at the top of the forum..

Which settings?


---
**Mark Finn** *February 23, 2016 20:58*

**+nick aldridge** your video is private.


---
**David Cook** *February 23, 2016 20:58*

**+nick aldridge** your video is private  :-(     amazing you are cutting 15mm !!  wow


---
**I Laser** *February 23, 2016 22:20*

**+nick aldridge**  the second cutting is most likely caused by line thickness. Check your lines are .01mm


---
**The Technology Channel** *February 23, 2016 22:52*

Ha I will have a look, Video now public, sorry...


---
**The Technology Channel** *February 23, 2016 22:54*

You can see the smoke coming from the bottom, normally showing a cut right through...




---
**David Cook** *February 23, 2016 23:03*

Impressive **+nick aldridge**  Thanks for sharing


---
*Imported from [Google+](https://plus.google.com/105609664958110832961/posts/Ywxs5BofWXZ) &mdash; content and formatting may not be reliable*
