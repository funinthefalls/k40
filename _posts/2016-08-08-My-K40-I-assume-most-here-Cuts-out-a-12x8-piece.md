---
layout: post
title: "My K40 (I assume most here?) Cuts out a 12x8 piece"
date: August 08, 2016 00:42
category: "External links&#x3a; Blog, forum, etc"
author: "Bob Damato"
---
My K40 (I assume most here?) Cuts out a 12x8 piece. As it turns out, my first task is to cut out an 8x10 piece. I can do the 10" dimension OK but not the 8. It seems the fan intake is about 1/2 or so into that 8" cut area. If I move my drawing down, it gets to the bottom of the travel, hits the limit and makes a terrible sound which I cant imagine is good for it. 

Seems like my only option is to take out the fan duct, cut it, and put it back in. Looks like a major pain to do.  

Am i correct or am I missing something obvious?





**"Bob Damato"**

---
---
**Alex Krause** *August 08, 2016 00:54*

Cut it out with a dremel with a cutoff wheel or you will need to take the entire X,Y apart


---
**Scott Marshall** *August 08, 2016 00:58*

Take out the duct and never put it back in. 

This DOES require removing the motion control (brown) frame, then re-align your mirrors after re-installing it. but the improvements are worth it.



You'll be rewarded with vastly improved air flow, allowing the stock fan to actually work, less smoke deposits on your work, and  (of course) it won't be in the way.



Mark the location of the frame with a Sharpie before loosening the frame, and be careful unplugging the cables (2) so you don't damage them. (Pull on the connector with needle nose pliers, not on the wires with anything)  The duct is held in place with 4 screws at the corners of the exhaust outlet. make sure to put back the fan mounting rails.



Scott


---
**Robert Selvey** *August 08, 2016 01:08*

I just cut mine out with a dremel last weekend and it was real easy. Didn't have to remove anything just went around it.


---
**Eric Flynn** *August 08, 2016 01:45*

Yea, just cut it back to the rear rail.  Its easy enough to remove the motion system, and remove the duct to cut it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 08, 2016 05:31*

I also removed the front rail guard from mine to gain an extra 20mm on the Y-axis, so I can get 220mm now.


---
**Scott Marshall** *August 08, 2016 07:02*

**+Yuusuf Sallahuddin** 

230mm on mine 

I know because I've zeroed it about 200 times today.

2 100mm jogs, and 3 10mm jogs..There's another 2 or 3 mm to go yet.



I hadn't even though about removing the rail, I took it off to trace wiring and never put it back. It's been in my pile of stuff here. 



Cool,  A Mod for NOT putting it back on. 



Scott


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/DZUckqzWRK8) &mdash; content and formatting may not be reliable*
