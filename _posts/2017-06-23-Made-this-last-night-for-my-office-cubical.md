---
layout: post
title: "Made this last night for my office cubical"
date: June 23, 2017 18:16
category: "Object produced with laser"
author: "3D Laser"
---
Made this last night for my office cubical.  Nothing fancy just a simple two tier shelf for my funko pops and Star Wars x wing ship.  It hangs from the glass divider.  Like I said simple but effective  

![images/0f7ba980f2baa8a0076f05ebbed914d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f7ba980f2baa8a0076f05ebbed914d2.jpeg)



**"3D Laser"**

---


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/MwPQsUDqxRu) &mdash; content and formatting may not be reliable*
