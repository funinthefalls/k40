---
layout: post
title: "Nice set of rules to consider Originally shared by Make: Getting started with laser cutting?"
date: September 29, 2016 19:38
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Nice set of rules to consider 



<b>Originally shared by Make:</b>



Getting started with laser cutting? Here some 5 basic tips to remember when finalizing your designs. [http://ow.ly/yVgj304H3mI](http://ow.ly/yVgj304H3mI)





**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *September 29, 2016 22:02*

Too bad they're not all true. Like the first one. Or later when they then acknowledge you can do both raster and vector, they say you need to make your fonts into lines in order to get them to output. 



Warnings about what not to cut (like PVC) and how to create and use calibration files would have been handy.



There's enough confusion for newbies between raster & vector processing without these guys adding to it. A lot of what they were talking about is dependent on the CAM software that drives the laser (tons of difference in how vector & rasters are handled in LaserDRW, Laser Cut and RD Works for instance, not to mention our own Peter's Laser Web 🙂).






---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/TjTJepkUxFk) &mdash; content and formatting may not be reliable*
