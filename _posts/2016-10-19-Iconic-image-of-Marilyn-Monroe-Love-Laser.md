---
layout: post
title: "Iconic image of Marilyn Monroe. Love Laser"
date: October 19, 2016 18:30
category: "Object produced with laser"
author: "Nick Williams"
---
Iconic image of Marilyn Monroe. Love Laser.



![images/3c74778a5050e826c4be77542a5fa014.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3c74778a5050e826c4be77542a5fa014.jpeg)



**"Nick Williams"**

---
---
**Scott Thorne** *October 19, 2016 21:38*

Sweet image


---
**Alex Krause** *October 19, 2016 21:59*

Maple?


---
**Scott Thorne** *October 19, 2016 22:01*

**+Alex Krause**...i just got my maple in today...i am trying a 3d carve on it....stinks like crazy while lasing!


---
**Alex Krause** *October 19, 2016 22:02*

**+Scott Thorne**​ I love the smell of engraving maple :P like burnt pan cakes 


---
**Scott Thorne** *October 19, 2016 22:33*

Lol...you bet...funny you mention that...i was trying to get a fix on what it smelled like...you just nailed it...lol....thanks **+Alex Krause**


---
**Nick Williams** *October 20, 2016 01:00*

**+Alex Krause** No Baltic birch:-) done with a 3 watt diode laser:-) running grbl


---
*Imported from [Google+](https://plus.google.com/107520953345127905250/posts/drPXH3qsr1o) &mdash; content and formatting may not be reliable*
