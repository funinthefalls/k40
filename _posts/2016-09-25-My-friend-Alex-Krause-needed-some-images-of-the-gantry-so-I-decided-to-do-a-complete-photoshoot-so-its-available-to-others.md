---
layout: post
title: "My friend Alex Krause needed some images of the gantry so I decided to do a complete photoshoot so it's available to others"
date: September 25, 2016 14:13
category: "Modification"
author: "Ariel Yahni (UniKpty)"
---
My friend **+Alex Krause**​​ needed some images of the gantry so I decided to do a complete photoshoot so it's available to others. By no means I beleive this is the only configuration available out there but will give you an idea. 



<b>Originally shared by Ariel Yahni (UniKpty)</b>



This is no upgrade but since I removed the gantry I feel it will be very useful for other to have a good look to what there. 



![images/cdeff072aeb9f76f09e75becd5f8c476.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdeff072aeb9f76f09e75becd5f8c476.jpeg)
![images/2edad67974ddea87455a2c94d64a5786.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2edad67974ddea87455a2c94d64a5786.jpeg)
![images/82aa765eef857699e98082ee6abae08e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/82aa765eef857699e98082ee6abae08e.jpeg)
![images/657c27a242b88711750e29fc8823a519.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/657c27a242b88711750e29fc8823a519.jpeg)
![images/aa84863d0c5001804b72fdab8b475c22.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa84863d0c5001804b72fdab8b475c22.jpeg)
![images/e3ec324811d04eafd973e2de5b60d80b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e3ec324811d04eafd973e2de5b60d80b.jpeg)
![images/22092f242261a1828d8ac4ac209e11f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/22092f242261a1828d8ac4ac209e11f6.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**Don Kleinschnitz Jr.** *September 25, 2016 15:39*

So what I have learned (is this correct):

-To remove the Y stepper the gantry has to come out or the partition removed.

-The belt adjusters are in the back of the machine, two holes. I conveniently ran a air fitting through one of them not knowing what they are for :(.



Any idea what the slots in the end of the frame (made from multiple holes) is for?



I added comments to one of the pictures not knowing if they show up for everyone.



I think this is the same gantry?: [amazon.com - Amazon.com: 400x600 Xy Stage Table Bed Plotting for K40 Co2 Laser Cutting Engraving Machine](https://www.amazon.com/400x600-Plotting-Cutting-Engraving-Machine/dp/B013B1R0BQ/ref=sr_1_2?s=arts-crafts&ie=UTF8&qid=1474817762&sr=1-2&keywords=k40+laser)




---
**Ariel Yahni (UniKpty)** *September 25, 2016 16:04*

No idea on the slots, could be to attached the gantry to something maybe?  The linked gantry I will say is identical and could fit in the case but you won't be able to use the depth 


---
**Don Kleinschnitz Jr.** *September 25, 2016 16:10*

**+Ariel Yahni** this is immensely useful thanks for doing it. This post represents a critical set of knowledge that should be indexed for future use. This causes me to get on a soapbox.



begin soapbox;

{

All, I am struggling with our G+ K40 instance in that I see lots of stuff that I want to bookmark so that I can return and review when I get to that phase of my conversion. The alternative is to ask questions that have already been answered, and that is annoying to authors of the answers.

I have been copying links to my Google Keep to preserve them.

An example is the LW and configuration file posts that I know I will need to check against when I soon power up this beast.

Another is the wiring for LPS and their troubleshooting or repair.

---------

Am I missing something in regard to tagging posts or searching and the proper use of G+, because I find it very hard to find anything in an organized fashion once a conversation closes.

---------

It seems we need a searchable place that has pictures, documents, schematics, disassembly, repair, troubleshooting, tools and mechanical drawings for each assy. of a converted K40. That site somehow needs to be refreshed from the growing G+ knowledge.... 



or is G+ the right place and we just need to be more diligent on picking tags for posts and/or updating one of the project forums or open build sites?



or, Is it that I do not know how to use G+ efficiently?



or do we need to have more granular tags for this G+ site?

----

While I am at it I am constantly confused and frustrated with G+ and our Spaces. I seem to have to post both places.

----

Final note: I am not suggesting we leave G+ (I like the conversations) just wanting to find a way to organize what I feel to be critical amounts of K40 and K40 conversion information that feels unorganized. 

-----

Sooner or later we will get annoyed with answering the same questions over and over not to mention rediscovering what others already have.

}

end soapbox;


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 26, 2016 01:54*

**+Don Kleinschnitz** You mention tagging posts, that seems like a possible good idea to assist with finding information through the search features. So if we could tag our posts (as users) with things like #airassist or #LPS then you could search in the searchbox easier for #LPS when you wanted to find all relevant posts related to the LPS. Might be a job for the moderators to go through old posts & tag them with relevant tags to assist in this searching/indexing task.


---
**Mike Mauter** *September 26, 2016 15:53*

I share the frustration of the G+ groups. I have spent hours trying to go back and find something that I have seen posted. I did stumble onto being able to save things into a "saved collection" . It seems that may be be an option as it can be made Public. Or how about separate G+ group of "K40 Greatest Hits" No discussion allowed-this group only links back to the best posts in the main group?


---
**Ariel Yahni (UniKpty)** *September 26, 2016 16:03*

**+Mike Mauter** I agree but its all about curating and someone need to be in charge of that, if not then you will get random post of stuff that has already being discussed or posted. What im doing since a couple of weeks ago is a list of problems and solutions to repetitive questions, soon ill post the link to that.


---
**Don Kleinschnitz Jr.** *September 26, 2016 16:14*

**+Mike Mauter** I learned about collections from **+Ariel Yahni** and created collections based on K40 subsystems to test if this solves my frustration. I think if you follow me you can see them?


---
**Ariel Yahni (UniKpty)** *September 26, 2016 16:18*

**+Don Kleinschnitz**​ user can follow a collection also and interact with the post but cannot post into the collection. Al this if it's public 


---
**Don Kleinschnitz Jr.** *September 26, 2016 16:23*

**+Ariel Yahni** do you know if you share a post to a collection, will the collection also include any comments made into that post after it is added to the collection. I would think the post is just a link so that it will ?


---
**Ariel Yahni (UniKpty)** *September 26, 2016 16:25*

**+Don Kleinschnitz** Yes but the preview does not.


---
**Don Kleinschnitz Jr.** *September 26, 2016 16:49*

**+Ariel Yahni** thks


---
**J.R. Sharp** *September 27, 2016 19:29*

I've often wondered what you can do by taking the gantry out and just running that way...or using longer rails.




---
**Ariel Yahni (UniKpty)** *September 27, 2016 19:46*

**+J.R. Sharp**​ press here #UK40OB


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/E6cyegn6RRM) &mdash; content and formatting may not be reliable*
