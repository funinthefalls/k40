---
layout: post
title: "Has anyone else seen this error? I got both Laser DRW and CorelDRAW with my laser (along with the orange dongle)"
date: November 26, 2015 13:02
category: "Software"
author: "DIY3DTECH.com"
---
Has anyone else seen this error?  I got both Laser DRW and CorelDRAW with my laser (along with the orange dongle).  Installed both and all seemed fine as far as the install process. However when I run CorelDRAW this is the error it comes back with.  I asked the seller and you can guess the answer too I had to turn off virus protection as it did not like either (Laser DRW / CorelDRAW).  Now LaserDRW does work, but CorelDRAW no.  I have also tried it on different computers (laptop / Desktop) as well as both Windows 7 & XP.  I also downloaded a version from the sellers web site and still the same...

![images/1efb7241c91b986cfdadde31551b9e1e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1efb7241c91b986cfdadde31551b9e1e.jpeg)



**"DIY3DTECH.com"**

---
---
**Sean Cherven** *November 26, 2015 13:42*

You must have CorelDraw installed before you can install CorelLaser.



CorelLaser is just a plugin for CorelDraw.


---
**Mauro Manco (Exilaus)** *November 26, 2015 13:49*

Sean....this message appears with coreldraw too...need make default installation if remove some componenet receive this error.


---
**Sean Cherven** *November 26, 2015 14:38*

I've never had this issue, so idk much about that.


---
**DIY3DTECH.com** *November 26, 2015 15:55*

Sean makes sense, however does CorelDraw come with it or do you buy it extra (its expensive I know).  I also Googled the error with no luck, however again think your correct that it is looking to launch CorelDraw and its not there...  Hmmm another reason to give it a new brain :-)




---
**Coherent** *November 26, 2015 16:41*

1) Uninstall all CorelDraw and LaserDrw items.

2) Install CorelDraw (12 came with your machine most likely but X5, X6 etc work ok). Close CorelDraw and make sure it's not running.

3) Install Corel Laser it will know CorelDraw is on your computer and install the plugin.

4) Install LaserDrw

5) Open LaserDrw and make sure the correct machine model is selected in the configuration.

6) Close LaserDrw, open Corel Laser (it will start CorelDraw with the plugin icons available on top right) Check the configuration icon (up by the cut and engrave icons) again from CorelDraw to set the correct machine model again.

7) Do your designs, importing and cutting/engraving by starting Corel Laser, it willload Corel Draw. Not by starting/using the CorelDraw icon. It will not load the plugin.  



Also you may get errors or failure to load the program if you try to open LaserDrw and Corel Laser at the same time.

 


---
**Sean Cherven** *November 26, 2015 16:54*

It comes as a separate installation file. Mine came with CorelDraw 12. Install that first, then install CorelLaser.


---
**DIY3DTECH.com** *November 26, 2015 19:53*

Thanks Marc & Sean will give a try after dinner today and let you guys know...


---
**DIY3DTECH.com** *November 28, 2015 12:34*

No go on this one, I simply do have Corel Draw in any of the files from the seller only the plugin :-(


---
**Gary McKinnon** *November 28, 2015 13:02*

My CorelDraw install files were corrupt when reading from CD so i downloaded the official free CorelDraw 12 ISO from here : 



[http://getintopc.com/softwares/development/download-corel-draw/](http://getintopc.com/softwares/development/download-corel-draw/) 


---
**Sean Cherven** *November 28, 2015 14:43*

I put together everything of importance from the CD, and organized it to be easier to read and navigate. You can download it here: [https://www.dropbox.com/s/86ib5q5i9jvk4xs/Laser%20Cutter%20-%20Easy.zip?dl=0](https://www.dropbox.com/s/86ib5q5i9jvk4xs/Laser%20Cutter%20-%20Easy.zip?dl=0)


---
**Gary McKinnon** *November 28, 2015 14:49*

Nice one Sean that's handy :)


---
**Sean Cherven** *November 28, 2015 14:54*

I made that a while ago lol, it's been posted a few times, but keeps getting lost in the shuffle.


---
**Gary McKinnon** *November 28, 2015 15:23*

Ye the communites interface needs threads.


---
**DIY3DTECH.com** *November 28, 2015 15:58*

Many thanks!  Just downloaded it!


---
**DIY3DTECH.com** *November 29, 2015 03:32*

Installed this and didn't have to uninstall the plugin!  All works fine now, many thanks!


---
**Derrick Armfield** *August 13, 2016 16:45*

Just got my K40 yesterday and am having the same issues.  Downloaded the Dropbox files and when I click on "CorelDRAW Graphics Suite 12" I get an error message saying "1. Failed to install ISKernel Files.  Make sure you have appropriate priveledges on this machine".   Have tried this on 3 different laptops,  Vista, Win 7, and Win 10.    Help


---
**Derrick Armfield** *August 14, 2016 06:03*

Finally got the CoreDraw to download to my laptop!   What a pain.   Used the Dropbox files further up in this thread.  Downloaded then UnZip'd,   clicked on Setup and away it went! 


---
*Imported from [Google+](https://plus.google.com/+DIY3DTECHcom/posts/eQxHCkAo2V1) &mdash; content and formatting may not be reliable*
