---
layout: post
title: "Laser done laser'd itself...."
date: February 20, 2015 18:30
category: "Discussion"
author: "Justin Pontius"
---
Laser done laser'd itself....



![images/0b85523466ec4fef2f120c9ff054e60d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0b85523466ec4fef2f120c9ff054e60d.jpeg)
![images/59054455a2daa0a450eace86ad82afac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/59054455a2daa0a450eace86ad82afac.jpeg)

**"Justin Pontius"**

---
---
**Other World Explorers** *February 20, 2015 20:14*

Ouch! 


---
**Stephane Buisson** *February 20, 2015 22:11*

Look like the ray was focusing straight on the mirror. mirror and lens are easy to find on ebay, but maybe you need a laser tube too.


---
**Other World Explorers** *February 20, 2015 23:26*

Check out [lightobject.com](http://lightobject.com) they have the parts and other things. Plus the forum is very helpful. 


---
**Tim Fawcett** *February 21, 2015 19:59*

I would be surprised if you needed a new tube if it burnt through the mirror :-) However you can get replacements off e-Bay for both. The mirror  in your photo looks to be metal evaporated onto glass - probably not robust enough for a laser cutter. Go for a Silicon or Molybdenum lens.


---
**Bill Parker** *March 13, 2015 06:16*

I have just read about using the disk out of an old 3.5" hard drive to cut circles out of that can be used as mirrors and they are double sided and get you a good few out of one disk. Not sure if it is worth messing but if anybody is on a tight budget might be worth a look.


---
*Imported from [Google+](https://plus.google.com/+JustinPontius/posts/d3Ub6ktuW5R) &mdash; content and formatting may not be reliable*
