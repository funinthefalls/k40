---
layout: post
title: "Well here is some tests I did today on ~3-5mm natural vegetable tanned leather"
date: November 22, 2015 09:10
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Well here is some tests I did today on ~3-5mm natural vegetable tanned leather.



I tested different power levels & different engrave/cut speeds. I also tested to see what the pixel steps option actually does (basically the more pixel steps, the less it actually lasers, so the image ends up faded slightly).



So now I have a reference for what difference it makes doing different power levels/speeds/pixel steps.



Personally, I think I will stick with 4-10mA power, 500mm/s engrave, 1 pixel step.



And I still haven't managed to cut through the 3-5mm thick leather. Closest I got was 10mA @ 2mm/s 1 pixel step. Looks horrible though and actually charcoaled the leather haha. So that's a no go. I think another test with 4mA @ 10mm/s 1 pixel step & multiple passes is in order.



![images/9a1aaeea55aee68834abea7e5d66f2ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a1aaeea55aee68834abea7e5d66f2ae.jpeg)
![images/2044be1d1de6b7a9422313f885117ae5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2044be1d1de6b7a9422313f885117ae5.jpeg)
![images/3f6744d5ee9064023e3a4a1c0e44ca6c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f6744d5ee9064023e3a4a1c0e44ca6c.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/41G8TTQ7Znq) &mdash; content and formatting may not be reliable*
