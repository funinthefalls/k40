---
layout: post
title: "Hello All, I decided it would be fun to add air assist and in the process decided to remove the XY to remove the exhaust vent"
date: December 09, 2017 20:16
category: "Original software and hardware issues"
author: "Kyle Reed"
---
Hello All,

I decided it would be “fun” to add air assist and in the process decided to remove the XY to remove the exhaust vent. Now I’m getting a lot of grinding, especially along the Y axis. I don’t know if it’s out of square (I was able to align the beam) or if the belt is slipping.



If I run the gantry down the Y axis by hand, there does seem to be a little more resistance in the middle. Also, I have no idea where the tensioner for the Y axis is.



Any thoughts? Thanks.







**"Kyle Reed"**

---
---
**Kyle Reed** *December 09, 2017 20:23*

One more thought, I haven’t used it since the summer (it’s in my garage) and now it’s cold, probably in the high 30s F in there. Does temperature affect belts?


---
**Wolfmanjm** *December 09, 2017 21:30*

add  a little oil or wd40 to the smooth bar, it'll make a lot of difference.


---
**Kyle Reed** *December 09, 2017 22:08*

I’ll try that. Also, I took off the white y-shaft cover and that fixed the grinding. I think it was slightly deforming the sheet metal at the back causing some binding. I think it’s just a little smaller than the drilled holes so it’s pulling the tops of the left and right sides together.


---
*Imported from [Google+](https://plus.google.com/106608373662281835438/posts/Sz2nuDn8unE) &mdash; content and formatting may not be reliable*
