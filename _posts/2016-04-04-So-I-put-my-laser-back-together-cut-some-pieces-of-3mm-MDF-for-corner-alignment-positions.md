---
layout: post
title: "So I put my laser back together & cut some pieces of 3mm MDF for corner alignment positions"
date: April 04, 2016 16:04
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I put my laser back together & cut some pieces of 3mm MDF for corner alignment positions.



I must have done something great when putting it back together, because it only took me 2 passes @ 5mA @ 7mm/s cutting through the MDF. Previously I was cutting through same stuff at somewhere like 5 passes & horribly charred edges.



I adjusted my air-assist whilst reassembling, as I tested a few different things (I was trying to see if I could reverse it as a vacuum & suck the smoke out instead of blowing it away, but my air-pump was not powerful enough for that).



Anyway, I did some tests after reassembling on some bamboo paddle-skewers. Thought I would test making plant labels for my garden stuff. Bit of a fail & success at the same time. Writing is too small, but it is nicely clear. 1 pass engrave @ 10mA @ 300mm/s.



![images/8569a918d24ff5361a6ae1ee3111cec3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8569a918d24ff5361a6ae1ee3111cec3.jpeg)
![images/160f4ea8b73fbb18260a3f0e6cf5076b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/160f4ea8b73fbb18260a3f0e6cf5076b.jpeg)
![images/2f056a221a68481d40f2a6bb9450ea34.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f056a221a68481d40f2a6bb9450ea34.jpeg)
![images/911c7d733ec69239527797541e9ec783.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/911c7d733ec69239527797541e9ec783.jpeg)
![images/92ff3306cb203f128792120a09cca7e3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/92ff3306cb203f128792120a09cca7e3.jpeg)
![images/b3aedff0a5a20ff6daab0c600b8e031c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b3aedff0a5a20ff6daab0c600b8e031c.jpeg)
![images/66b07019b1e1f8c1c3d44e6cff685cea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/66b07019b1e1f8c1c3d44e6cff685cea.jpeg)
![images/6b4cdb3bf5b9f6a7c8749bd399bbd16c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b4cdb3bf5b9f6a7c8749bd399bbd16c.jpeg)
![images/98bd1e4ec3f4c931eeaf05d9ca4fb2b9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98bd1e4ec3f4c931eeaf05d9ca4fb2b9.jpeg)
![images/c33644edf0a94036e2d8060b1c34f37b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c33644edf0a94036e2d8060b1c34f37b.jpeg)
![images/9021015936648ac947db756f3fa77556.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9021015936648ac947db756f3fa77556.jpeg)
![images/f3a1d88447692c130f2fce58409fc865.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3a1d88447692c130f2fce58409fc865.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**ED Carty** *April 04, 2016 16:14*

looks good


---
**Phillip Conroy** *April 04, 2016 21:25*

I notice you holding work down with magnets.i also used magnets when i just had air assist by a pipe next to the laser head as it was blowing at a diffrent angal than at 90 deg ton the cutting bed.when i upgraded to a proper air assist nozzel i no longer have to hold work as the air is holding down and the work does not move.

As for cutting 3mm mdf i am cutting through in 1 pass at 7.5mm/s speed and 10ma power.you must either have mis aligned mirrors,dirty mirrors/focal lens, has it ever cut through in 1 pass?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 22:31*

**+Phillip Conroy** I use the magnets mainly to hold things in place so I don't move it by accident. With leather, I use the magnets because it likes to curl when sitting there, so I magnet it to hold it taut. I don't generally have problems with the air-assist blowing things, except with stuff like 200gsm paperstock. I intend to get myself a proper air-assist head, but I've been considering whether I should upgrade lens at the same time.



I didn't attempt to cut through at 10mA power. I used 5mA & 2 passes @ 7mm/s. I was watching it as it was cutting & I could see even after 1 pass it was about 90% the way through the MDF, so I imagine a slightly higher power (I dont think it would need 10mA) or a slightly slower speed would get through in 1 pass. I'll give it a go later today to see. Previously, before I pulled machine apart, I was having multiple passes at higher power to cut through the 3mm MDF. Seems whatever I did when I put it back together made it a lot better. Mirrors & lens are clean/aligned pretty much spot on centre. I made sure of that before bothering to cut/engrave anything. Although, I think I could tweak slightly for hitting 3rd mirror more precisely (<0.5mm out on bottom-right-most position).


---
**Phillip Conroy** *April 05, 2016 00:35*

at least with the air assist next to the laser head and not blowing onto the focal lens you will not have water and oil getting onto and dirtying the focal lens like mine did 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 03:37*

**+Phillip Conroy** That may be why I've never experienced that issue you mentioned. I was wondering why when you posted about the moisture you were getting. Makes sense now. I seem to have fluked it & got the air-assist beam hitting pretty much right on the cut point, so I've noticed 0 smoke now & much cleaner cuts.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/LRQMpzAvtoT) &mdash; content and formatting may not be reliable*
