---
layout: post
title: "1). Clamping table 2.0 design progress. Working on a tension mechanism and then plan to build one"
date: March 24, 2017 10:17
category: "Modification"
author: "Don Kleinschnitz Jr."
---
1). Clamping table 2.0 design progress. Working on a tension mechanism and then plan to build one.

[http://donsthings.blogspot.com/2017/02/k40-clamping-table.html](http://donsthings.blogspot.com/2017/02/k40-clamping-table.html)



2). Thought I would try embedding Sketchup design in a G+ post



[https://3dwarehouse.sketchup.com/model/5f03acd4-1061-466e-aef7-cd10e8bd48f5/K40-Manual-Clamping-Table](https://3dwarehouse.sketchup.com/model/5f03acd4-1061-466e-aef7-cd10e8bd48f5/K40-Manual-Clamping-Table)







**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 24, 2017 12:44*

Looks good, but what's the handle for?


---
**Don Kleinschnitz Jr.** *March 24, 2017 12:53*

**+Yuusuf Sallahuddin** the knob is to adjust the forward/backward position. 

Just completed changes that puts a locking knob on top so you do not have to go through the machines front:

[3dwarehouse.sketchup.com - K40 Manual Clamping Table](https://3dwarehouse.sketchup.com/model/5f03acd4-1061-466e-aef7-cd10e8bd48f5/K40-Manual-Clamping-Table)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 24, 2017 13:06*

**+Don Kleinschnitz** You know, it should have been obvious looking at it haha. My excuse: I was having a blonde moment.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/4rZosYeYd14) &mdash; content and formatting may not be reliable*
