---
layout: post
title: "I think I am having an issue with the motor driver"
date: June 12, 2017 14:10
category: "Original software and hardware issues"
author: "Lee lim"
---
I think I am having an issue with the motor driver.  Any suggestions? 




**Video content missing for image https://lh3.googleusercontent.com/-TSVvabA8Pt4/WT6g7LArqrI/AAAAAAAASjA/FuE5hJau8Esx0DYAG7fd18DMSa-F3Y4hQCJoC/s0/20170612_064042.mp4**
![images/9f238169eefe49aef19cf73151ee9d42.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9f238169eefe49aef19cf73151ee9d42.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-7q_ykXX0EC0/WT6g7FMFPTI/AAAAAAAASjU/kFX0HcBlv9wDuQr4XJf8hKnQGC3dYsA8QCJoC/s0/20170612_063443.mp4**
![images/7d4cb37c902bea0301c7d048b65bf5fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d4cb37c902bea0301c7d048b65bf5fc.jpeg)

**"Lee lim"**

---
---
**Don Kleinschnitz Jr.** *June 12, 2017 14:17*

Tell us more about your machine and its controller configuration etc.

Is this a new problem and did it work previously.


---
**Chris Hurley** *June 12, 2017 14:29*

No sure. Did you check the sensor on the y-axis? If it gets dirty it will on return to the zero point. I would also check the belt for damage or wear. 


---
**Lee lim** *June 12, 2017 14:33*

It was working fine, I was finding the best focal length to use, then it started doing this.  All stock except for the air asist


---
**Chris Hurley** *June 12, 2017 14:34*

Try taking the air line off and power cycling. 


---
**Don Kleinschnitz Jr.** *June 12, 2017 14:46*

In addition to the above I would check the following:

Cables to the x and y endstop sensors

Clean the x & y endstop sensors

Check the 24V and 5V DC supplies for correct voltage and any variation.


---
**Lee lim** *June 12, 2017 15:31*

To the control board I am getting 1.3v 5v and 20v where is the adjustment? 


---
**Don Kleinschnitz Jr.** *June 12, 2017 16:24*

**+Lee lim** where are you measuring these voltages?

What adjustments? There are no power supply adjustments that are necessary?


---
**Lee lim** *June 12, 2017 16:27*

I measured the blue wires to the control board


---
**Don Kleinschnitz Jr.** *June 12, 2017 20:00*

**+Lee lim** that does not help :). I have no way of knowing what blue wires you are referring to. 

Can you post pictures etc of exactly where you are measuring?

Measure the 24V/5V on the laser power supply.


---
**Lee lim** *June 12, 2017 20:48*

These going from the power supply to the control board, it's now measuring 25.6v  

![images/3d78f5204b155f384695af4570a5e229.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d78f5204b155f384695af4570a5e229.jpeg)


---
**Don Kleinschnitz Jr.** *June 12, 2017 21:54*

**+Lee lim** what does the 5V read. 25.6 is a bit high it should be 24, but I doubt that is your problem. 

Also can you measure the 24V and 5V while moving the gantry.


---
**Lee lim** *June 14, 2017 00:24*

I swapped out the steppers to the control board.  The issue seems to follow the plug in the board.   So I think something is wrong with the board.  I have a smoothie board from cohesion3d on order on Thursday.  So I am going to try and be patient and not install the ramps or GT2560 I also have.


---
*Imported from [Google+](https://plus.google.com/109926073411939667229/posts/BCBcZxUPvxh) &mdash; content and formatting may not be reliable*
