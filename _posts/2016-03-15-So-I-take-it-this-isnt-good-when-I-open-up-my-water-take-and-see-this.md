---
layout: post
title: "So I take it this isn't good when I open up my water take and see this"
date: March 15, 2016 23:44
category: "Discussion"
author: "3D Laser"
---
So I take it this isn't good when I open up my water take and see this 

![images/019843f1db5105c5121076b2ddd4342d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/019843f1db5105c5121076b2ddd4342d.jpeg)



**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 15, 2016 23:59*

Yeah looks like some kind of algal growth or mould? Whatever it is doesn't look good. I imagine that might be where that brown ring on your tube comes from (some of that algae/mould getting into the tube & heating up/sticking to the side).



I'm using a transparent rectangular container for my water, to allow me to see into it at all times (as I thought this may happen). Others mention putting antifreeze or similar products into their water to prevent the growth.


---
**3D Laser** *March 16, 2016 00:00*

Will that ruin the tube 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 16, 2016 01:23*

**+Corey Budwine** Personally, I am not sure. However I can't imagine it is good for the tube to have residual buildup of algae/mould inside it, as this possibly increases heat in that region (which may eventually weaken/destroy the glass in that section). Also, it could eventually cause a full blockage/issues with water flow through the tube, again causing excess heat. Hopefully someone with more experience can share their knowledge with you (and me too).


---
**I Laser** *March 16, 2016 01:49*

You might want to do a search Corey. There's a number of posts regarding additives for laser coolant. I use an antifreeze that has algae inhibitor but it's only available in Australia. I think the guys in the US use RV antifreeze.



Your coolant needs to be able to flow freely through your pump and tube. Unfortunately, as you may have already found out, if the water stops your tube blows!


---
**3D Laser** *March 16, 2016 01:52*

I have RV antifreeze in there.  right now I dont see any cracks in my tube but i can't get it fire.  I think its my power supply the tube seems intact but I am not for sure


---
**I Laser** *March 16, 2016 02:07*

Well it shouldn't be algae then. To be honest, although it's hard to tell just by looking at a pic, it looks like tissue to me.



Is your tank tightly sealed? Looking at your profile pic your son looks as old as my daughter is. My daughter just loves to deposit anything she finds into boxes, containers, etc!


---
**HP Persson** *March 16, 2016 10:01*

I have seen similar stuff in my watercooled pc´s due to hoses making a oily surface.

Also got this on my new laser, only run about 20min so i removed all hoses and cleaned them good.



Now 3hrs more runtime it´s gone.



Touch the hose from the laser, is it oily on the outside?

Your´s a magnutude more in amount that i got though.


---
**Scott Thorne** *March 16, 2016 13:38*

I use a clear square storage container with a snap on lid...hoses go in through the top along with the thermocouple....works out great...I've never had anything cloud my water...I'm only using distilled water though. 


---
**Heath Young** *March 16, 2016 22:27*

I use demineralised water with a small amount of methlyated spirits - and an opaque container, which helps reduce algal growth as it needs light.


---
**Bob Steinbeiser** *March 17, 2016 14:53*

I used distilled water with a table spoon of chlorine bleach, no problem yet.


---
**MNO** *March 17, 2016 23:57*

i would use distilled water isopropyl alc. 

so now time to clean, change water,  flush with new distilled water + isopropyl or chlorine  


---
**I Laser** *March 18, 2016 09:54*

The RV anti freeze others have recommended has algae inhibitors. I assume it was mixed to recommended ratios.


---
**Vince Lee** *March 19, 2016 15:50*

Before I switched to Rv antifreeze I used distilled water and got algae.  I bought some sterilizing tablets of the kind used for breast pumps and used it to clean everything before the switch.  I figure it would be more gentle to the plastic parts than bleach and could be used as an occasional boost if needed.


---
**Boris Camelo** *September 21, 2017 15:23*

How often do I need to change the demineralised water?




---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/AcocYwaqh7Y) &mdash; content and formatting may not be reliable*
