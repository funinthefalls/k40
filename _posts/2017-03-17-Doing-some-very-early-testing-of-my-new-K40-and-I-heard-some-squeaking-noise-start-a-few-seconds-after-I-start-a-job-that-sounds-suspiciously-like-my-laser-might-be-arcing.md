---
layout: post
title: "Doing some very early testing of my new K40 and I heard some squeaking noise start a few seconds after I start a job that sounds suspiciously like my laser might be arcing"
date: March 17, 2017 01:01
category: "Discussion"
author: "Bob Buechler"
---
Doing some very early testing of my new K40 and I heard some squeaking noise start a few seconds after I start a job that sounds suspiciously like my laser might be arcing. 



Give it a listen and let me know what you think? Normal, or do I have another fun project on my hands?




{% include youtubePlayer.html id="JT9Bca9EFv0" %}
[https://youtu.be/JT9Bca9EFv0](https://youtu.be/JT9Bca9EFv0) 





**"Bob Buechler"**

---
---
**Gee Willikers** *March 17, 2017 01:09*

My makes a similar noise when the laser is powered, at times. I've never seen any ill-effects so I've always just attributed it to the cheap high frequency power supply.


---
**Bob Buechler** *March 17, 2017 02:02*

**+Gee Willikers** well, that's a relief. Thanks. With everything that can go sideways with these, especially when you're still learning, I just wanted to get opinions and make sure. 


---
**Don Kleinschnitz Jr.** *March 17, 2017 02:59*

What coolant are you guys using.



Even though they are cheap they should not squeal. 


---
**Gee Willikers** *March 17, 2017 03:04*

RV antifreeze and RO/DI water. Which have both actually wreaked havoc on my pump.


---
**Gee Willikers** *March 17, 2017 03:04*

And now that you mention it that issue is ringing a bell...


---
**Bob Buechler** *March 17, 2017 03:20*

Just purified water for me so far. But then it's only been a couple weeks. Best long term option I've seen so far is just a capfull of bleach and regular water changes.


---
**Don Kleinschnitz Jr.** *March 17, 2017 03:51*

I suggest you change your coolant using distilled water and 6ml (.2oz) of Clorox and lets go from there.



If you have a conductivity meter I would be interested in the reading before and after changing it.



Anything else is highly suspect to be too conductive for healthy coolant.



[donsthings.blogspot.com - Laser Tube: Protecting, Operating, Cooling & Repairing](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
**Bob Buechler** *March 17, 2017 14:58*

Purified water and distilled water are widely different in terms of conductivity?



I have a multimeter. Would that do?


---
**Bob Buechler** *March 17, 2017 15:37*

Also, since we're on the subject, I've struggled to design a reservoir system that can be sealed to avoid contamination and evaporation. I was thinking of using a 5 gallon bucket with a standard lid and installing two bulkhead fittings with barb fittings for the in and out flows, but then the submersible pump power cord needs a way out and I'd rather not make anything permanent so I can swap parts etc which makes sealing that hole a challenge. So I thought next about using an external pump instead... and on down the rabbit hole I go.



**+Don Kleinschnitz**​ what is your setup like? 


---
**Don Kleinschnitz Jr.** *March 17, 2017 15:48*

**+Bob Buechler** like many others I have a 5 gallon HD bucket (orange). 

Nothing special I hang my water flow switch tubes and cables through a slot I cut in the lid. The cover is not air tight but keeps the BIG stuff out. My future plan is to a fix fittings to the lid. 



Prior to this discovery I was using tap water and not doing anything about algae growth. 



Pictures under: flow sensor in this post.



[donsthings.blogspot.com - Laser Tube: Protecting, Operating, Cooling & Repairing](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
**Bob Buechler** *March 17, 2017 16:15*

Thanks, **+Don Kleinschnitz**​. I'd be curious to know the uS difference between purified water + 6mL of Clorox and distilled + the same. I don't have a conductivity meter to test this myself, unfortunately. Reason being that purified (highly filtered) water is much easier to get and less expensive than distilled (evaporated and recondensed) water.


---
**Don Kleinschnitz Jr.** *March 17, 2017 16:28*

**+Bob Buechler** what exactly do you mean by purified? What would you buy?




---
**Bob Buechler** *March 17, 2017 17:19*

These are pretty common in my area (for example): [custompure.com - Water Quality](http://www.custompure.com/Water-Quality-W24C7.aspx)


---
**BeenThere DoneThat** *March 18, 2017 21:08*

I noticed a funny click from mine, poor cutting. Changed the distilled water and seems fine. 


---
**Don Kleinschnitz Jr.** *March 18, 2017 21:13*

**+BeenThere DoneThat** the distilled water stopped the clicking and improved the cut???




---
**BeenThere DoneThat** *March 18, 2017 21:56*

Yes. 


---
**Don Kleinschnitz Jr.** *March 18, 2017 22:12*

**+BeenThere DoneThat** Sweet yet another proof point! Thanks.


---
**BeenThere DoneThat** *March 18, 2017 22:37*

Yours sounded like a whine, mine sounded like a crackle.


---
**Bob Buechler** *March 19, 2017 06:17*

Quick update. I've moved to a 5 gallon bucket and some bulkhead mounts with hose barb fittings, distilled water and 1/4 teaspoon of bleach per gallon of water, per **+Don Kleinschnitz**​'s recommendations. I flushed the tube for about 15 mins so far. 



I'll do a few more test jobs and see if the sounds change.


---
**BeenThere DoneThat** *March 19, 2017 08:05*

I was considering a 50/50 propylene glycol to distilled water. Or even 100% PG. I'm no chemist, I'm no laser expert. I have worked with a lot of water cooled laboratory test equipment, and I've got some concerns about the bleach. I don't know what it does to the dielectric strength. Maybe thats not important? Assuming the bleach is to prevent things living in the fluid such as bacteria and fungus. The chlorine will evaporate fairly quickly into gas, not in solution with the water. We used propylene glycol because of its low toxicity, and compatibility with the common materials found in lab equipment. And it doesn't cause rust. 


---
**Don Kleinschnitz Jr.** *March 19, 2017 14:53*

**+BeenThere DoneThat** our resident chemist **+Ned Hill** has looked at this and we think at this dilution the Clorox it will be an ok additive. 



Honestly we do not know the exact failure mechanism but we know it fails at high conductivity. We have had MANY users correct arching noise and reduced power, like you had, by replacing their coolant with distilled water.



The water is insulated from the ionization chamber by the water jacket but apparently if the coolant gets to conductive it will allow an arc (usually around the anode) and draw current from the supply. So the coolant is not breaking down, just conducting. This situation might be different than other lab coolant configurations because of the unusually high voltage and current needed to ionize.



We have plenty of evidence backed by measurements that antifreeze will increase the conductivity 400x and that this conductivity level will cause problems. 



So we are recommending that users keep the conductivity of their coolant at levels close to distilled water and change it as often as possible if the levels get elevate. The levels typically get high with algae growth. We do not know what the maximum acceptable levels are but we know that in the 400uS is bad. 



We have not measured distilled water and algae-cide yet to see if it is a good formula. That's another option.



Details and measurements are on my blog post on the subject.


---
**Don Kleinschnitz Jr.** *March 20, 2017 23:34*

**+Bob Buechler** purified water measured 12uS so its 3-4x more conductive but still very low. 

I am guessing it would be ok. **+Ned Hill**?

Google distilled vs purified and you get a lot of interesting info. In general distilled is well "distilled" but then often purified as well ..heh!

 Purified is cheaper cause it is usually purified by reverse osmosis which is cheaper.  Go figure...


---
**Bob Buechler** *March 21, 2017 05:02*

Went ahead with Distilled. Was able to find an affordable source for it. I found that research, which only added to my confusion, given that they both seemed like they should have roughly comparable uS levels. In the end, I definitely defer to you and Ned on this. :)


---
**BeenThere DoneThat** *March 21, 2017 05:41*

**+Bob Buechler** - my water tank holds about 1 gallon. It's less than a dollar at the grocery store. 


---
**BeenThere DoneThat** *March 21, 2017 05:50*

Sorry for my ignorance. The units "uS" is that a "micro siemens"? 



How are you measuring that?



I've worked with fluid resistivity meters before, but not those units. 


---
**Don Kleinschnitz Jr.** *March 21, 2017 12:15*

**+BeenThere DoneThat** yes its micro-siemens: [en.wikipedia.org - Siemens (unit) - Wikipedia](https://en.wikipedia.org/wiki/Siemens_%28unit%29)



I am using this unit: [https://www.amazon.com/gp/product/B000VVVEUI/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B000VVVEUI/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Bob Buechler** *March 21, 2017 18:30*

**+BeenThere DoneThat** I'm using a 5-gallon bucket.


---
**BeenThere DoneThat** *March 21, 2017 19:48*

Ive been monitoring my temperature with no noticeable increase in temperature. Im engraving at 7ma for about 2.5min then a minute or so of reloading the machine. 


---
**Bob Buechler** *March 23, 2017 18:48*

Finally had a moment to run some new test cuts using the distilled/bleach approach, and while I was running with the ventilation fan on, I couldn't really tell a difference in sound. I'll turn off the fans and record a fresh video for comparison's sake.


---
**Don Kleinschnitz Jr.** *March 24, 2017 10:05*

**+Bob Buechler** so are you saying you still have the squeaking noise of above?


---
**Bob Buechler** *March 24, 2017 17:24*

I believe so, yes. I'll kill my fan temporarily, re-record and post it for review.


---
**Adrian Godwin** *April 12, 2017 09:02*

**+Don Kleinschnitz** There's a procedure here to measure water conductivity. Could you try it and see if it gives similar results to your meter  ?

 

[sciencing.com - How to Measure the Conductivity of Water with a Multimeter](http://sciencing.com/measure-conductivity-water-multimeter-8523350.html)


---
**Don Kleinschnitz Jr.** *April 12, 2017 12:36*

**+Adrian Godwin** I have tried it. DC measurement does not result in an accurate measurement because of polarization effects, probe materials etc.



A DC approach may still be useful with some experimentation but since a conductivity meter is so inexpensive I moved on to other pressing investigations. I also wanted measurements that would accurately correlate to published materials.



[http://www.analytical-chemistry.uoc.gr/files/items/6/618/agwgimometria_2.pdf](http://www.analytical-chemistry.uoc.gr/files/items/6/618/agwgimometria_2.pdf)


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/eGJK9oqcTdr) &mdash; content and formatting may not be reliable*
