---
layout: post
title: "I have junk growing in my 5 gallon bucket"
date: July 27, 2017 00:09
category: "Discussion"
author: "Bill Keeter"
---
I have junk growing in my 5 gallon bucket. I've been using the RV chemical but I guess I don't swap out for new water often enough. Should I drop some bleach in the water tonight? Leaving the water flowing overnight? Then tomorrow replace with new water and RV stuff?



Suggestions? Anything I can add to the water to keep this from happening in the future?





**"Bill Keeter"**

---
---
**greg greene** *July 27, 2017 00:19*

rubbing alcohol


---
**Steve Clark** *July 27, 2017 00:56*

Tetra Pond Algae Control or the same for fish tanks.

[petsolutions.com - Tetra Pond Algae Control](https://www.petsolutions.com/C/Pond-Algaecides/I/Tetra-Pond-Algae-Control.aspx?catargetid=520009670000580989&cadevice=c&gclid=Cj0KCQjw--DLBRCNARIsAFIwR25Q32W-M2QXUOzkxUULMctwJ6Qf6CkNRbpXKPl2f_mbtosTH0u_T1MaAsZIEALw_wcB).. One drop per gallon.


---
**Don Kleinschnitz Jr.** *July 27, 2017 12:34*

The conductivity of your water matters.

Distilled water and nothing other than Clorox or algaecide, preferably the later. Do not use antifreeze.



[donsthings.blogspot.com - K40 Coolant](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
**Bill Keeter** *July 27, 2017 12:41*

**+Don Kleinschnitz** if I add the Algae control do I still need to use the RV Antifreeze?


---
**Don Kleinschnitz Jr.** *July 27, 2017 13:05*

**+Bill Keeter** No! DO NOT USE ANTIFREEZE at all :)!  

Distilled water + algaecide in the qty specified in the post .....

Do you have any loss of power or crackling/arcing noises?


---
**Bill Keeter** *July 27, 2017 13:11*

**+Don Kleinschnitz** Even the red RV antifreeze stuff suggested in this group from when I joined last year? I thought that stuff was safe. When I start up the laser I might get a pop or arching noise one or twice then it stops. hmmm.. guess this could be the reason (always bugged me). Anyway, i'll probably go get new hoses, pump, bucket, algae control and distilled water today. What flow rate should I look for with a new pump?


---
**Don Kleinschnitz Jr.** *July 27, 2017 13:46*

**+Bill Keeter** did you get a chance to read the post lots of data there? Most of us have gotten rid of additives other than algaecide + distilled water.

......

Various opinions on other than stock pumps so others can chime in. I have never seen empirical test data that maps temp and pump capacity seems like the stock pump capacity is ok.



If I ever change I am going to get a little giant. 



Mainly I am still noodling the cooling problem at 100F ambient :(.



Here is some info captured:

[donsthings.blogspot.com - K40 Coolant Pumps](http://donsthings.blogspot.com/2017/05/k40-coolant-pumps.html)




---
**HP Persson** *July 27, 2017 21:28*

Similar data as Don collected, too much to paste so i paste the link instead ;)

There´s a chart in the article too describing the different mixes, + and - about them.



[k40laser.se - Watercooling, different coolants - K40 Laser](https://k40laser.se/watercooling/watercooling-different-coolants-for-your-laser/)


---
**Bill Keeter** *July 31, 2017 02:02*

**+Don Kleinschnitz** hey quick question. I flushed the system and added 4 new gallons of distilled water. Then added the Algae Control. Doh, didn't realize the stuff was going to pore out like water. I'm sure I now have 2 or 3 times what you guys suggested. Now i'm a little worried about me added frozen water bottles etc to chill the water. The warning label about skin contact with the Algae control got my attention. Should I flush the system again and be more careful with the chemical?


---
**Don Kleinschnitz Jr.** *July 31, 2017 03:05*

**+Bill Keeter** **+Ned Hill** Ned what do you think your the chemist!


---
**Ned Hill** *July 31, 2017 03:10*

**+Bill Keeter** Probably not a problem, but what algae control are you using so I can check the ingredients?


---
**Bill Keeter** *July 31, 2017 03:19*

**+Ned Hill** Tetra Pond Algae Control. It's the one Steve mentioned near the top of this post


---
**Bill Keeter** *July 31, 2017 03:27*

**+Ned Hill** Here's the amazon page. The 2nd photo shows everything on the back label. 

[amazon.com - Amazon.com: TetraPond Algae Control Treatment, 16.9-Ounce, 500 ml: Pet Supplies](https://www.amazon.com/gp/product/B000HHSD4G/)


---
**Ned Hill** *July 31, 2017 03:34*

Yeah the label doesn't say what's in it so I had to pull up the MSDS.  It uses a polyquant antimicrobial  called Polyquaternium WSCP.  An occasional exposure to a dilute solution shouldn't be an issue.  I think even if you got some of the concentrated on your skin it would just irritate it if you left it on too long.  


---
**Bill Keeter** *July 31, 2017 03:50*

**+Ned Hill** okay, that's what I was hoping. I use frozen water bottles to chill the tank. Which means me sticking my hand in the tank every once in a while. So the label saying any skin contact requires washing the area continually for 15 min had me thinking I would need to wear gloves and completely clean the bottles before adding them back to a freezer.


---
**Ned Hill** *July 31, 2017 03:53*

Nah, it's not anywhere near that toxic.  Just don't bathe in it or drink it and you'll be fine.




---
**Don Kleinschnitz Jr.** *July 31, 2017 13:03*

One note: the water conductivity we measured is at the concentration that was specified for the algeacide specified. 


---
**Bill Keeter** *July 31, 2017 13:18*

**+Don Kleinschnitz** Good point. I might just flush 2 gallons and add 2 more. Just to lower the concentration of the algae control.


---
**Don Kleinschnitz Jr.** *July 31, 2017 18:44*

**+Bill Keeter** it might be useful to get a conductivity meter as that removes some of the mystery. I think there are some cited on the post.


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/QrtT97uLe4e) &mdash; content and formatting may not be reliable*
