---
layout: post
title: "Not really certain where to put this, but it's an addon script for Adobe Illustrator to automate the process of adding dimension measurements to shapes/objects within images"
date: October 01, 2016 14:47
category: "Repository and designs"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Not really certain where to put this, but it's an addon script for Adobe Illustrator to automate the process of adding dimension measurements to shapes/objects within images.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



For anyone using #Adobe #Illustrator when you want to get dimensions onto shapes in an image is a painstaking process to do it manually.



Just found this script that automates the process & makes it a lot less time consuming.



[http://adamdehaven.com/blog/2015/05/dimension-adobe-illustrator-designs-with-a-simple-script/](http://adamdehaven.com/blog/2015/05/dimension-adobe-illustrator-designs-with-a-simple-script/)





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 01, 2016 16:59*

I've made some modifications to this addon, to fix this Issue: [github.com - Multiple Labels, Single Command · Issue #3 · adamdehaven/Specify](https://github.com/adamdehaven/Specify/issues/3)



So, my post in the comments has the fix as a .txt file (you can either copy & paste it's contents into the original .jsx file or rename it to Specify.jsx). This will enable to choose which sides to get dimensions for (via checkbox) & can do all selected sides at once, instead of having to manually do 4 times for 1 object.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 18, 2017 05:25*

Update on this... there is a new (much more improved) version available now. [https://github.com/adamdehaven/Specify](https://github.com/adamdehaven/Specify)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/KmrV3tozaRj) &mdash; content and formatting may not be reliable*
