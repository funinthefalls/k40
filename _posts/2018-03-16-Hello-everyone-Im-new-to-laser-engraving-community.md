---
layout: post
title: "Hello everyone I'm new to laser engraving community"
date: March 16, 2018 20:39
category: "Discussion"
author: "Inland Empire Engravings"
---
Hello everyone I'm new to laser engraving community. I recently bought a Senfeng laser engraver 220 volt. It can with a 110v to 220v converter The guy that sold it to me said that that's what it was. It also came with a 250v power strip and no software. 



Does anyone know what software works with this machine? How do I connect this to each other? Any information would be appreciated thank you for your time.

![images/1b8a9b473f349cb943ec5a2f2b4333bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b8a9b473f349cb943ec5a2f2b4333bc.jpeg)



**"Inland Empire Engravings"**

---
---
**HalfNormal** *March 16, 2018 21:13*

What control board is in it? Post a pic if you do not know. 


---
**Inland Empire Engravings** *March 16, 2018 22:40*

**+HalfNormal** 

![images/596babee9e1976b52a98db6956f7b7ec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/596babee9e1976b52a98db6956f7b7ec.jpeg)


---
**Inland Empire Engravings** *March 16, 2018 22:49*

**+HalfNormal** 

![images/bc9c4abf60d407ea71359dad0dad2818.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bc9c4abf60d407ea71359dad0dad2818.jpeg)


---
**Ned Hill** *March 16, 2018 22:53*

Ohh  an old school Moshi board.  I think LaserDrw may work with it.  


---
**Ned Hill** *March 16, 2018 22:57*

Did it come with a  USB dongle?


---
**HalfNormal** *March 16, 2018 23:16*

Yep, that is a Moshi board. Search here for Moshi and you will find what you need. As Ned stated you need the USB dongle to use it and the software. 


---
**HalfNormal** *March 16, 2018 23:17*

**+Ned Hill** only Moshidraw software  will work.


---
**Fabiano Ramos** *March 16, 2018 23:19*

haha! Same of Mine! If you need help, i can help you.




---
**Fabiano Ramos** *March 16, 2018 23:23*

The software depends exclusively on the type of Dongle that came in it. The Orange Dongle (even the rarest) gives you access to the latest version of Moshidraw, provided you install the drivers correctly. There is the "Red" version of the dongle that gives access to the Moshidraw 2014.3



Already that's no big deal. Moshidraw is by far one of the worst software for engraving on this machine, and the 30x20 working area is quite limited.




---
**Ned Hill** *March 17, 2018 00:03*

 **+Ray Kholodovsky** Just out of curiosity has anyone upgraded a Moshi board laser with a C3D?








---
**Ray Kholodovsky (Cohesion3D)** *March 17, 2018 00:04*

Yes, quite a few people. Slightly more work to handle the wiring. 


---
**Ned Hill** *March 17, 2018 00:09*

So **+Inland Empire Engravings** according to Ray you could replace your Moshi control board with a Cohesion3D mini board and use either laserweb or lightburn to run the laser.  Would get a lot more functionality out of the laser.


---
**Fabiano Ramos** *March 17, 2018 01:54*

**+Ray Kholodovsky**  How  buy it?  exist any guide for conversion of this machine?




---
**Ray Kholodovsky (Cohesion3D)** *March 17, 2018 01:56*

Yes, [cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/) and [cohesion3d.com/start](http://cohesion3d.com/start)


---
**Inland Empire Engravings** *March 18, 2018 02:45*

**+Ray Kholodovsky** 

Is this the best to replace it with Cohesion3D mini board and use either laserweb or lightburn


---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2018 02:52*

You would get the laser upgrade bundle I linked above and use Lightburn. 


---
**Inland Empire Engravings** *March 18, 2018 04:57*

**+Ray Kholodovsky**

Thank you. Do you know if you can upgrade this machine to engrave on metal or aluminum? 


---
**Ned Hill** *March 18, 2018 05:07*

**+Inland Empire Engravings** Marking/engraving raw metal is way beyond what these machines are capable of.  You can engrave coatings off metal, like anodized aluminium or power coatings.  You can also engrave coatings onto metal with products like Cermark, or even a dry moly lube spray.  For real metal marking/engraving you typically have to use a fiber laser which operates at a different wavelength.


---
**Anthony Bolgar** *March 18, 2018 20:50*

Inland Empire ? Are you from California?


---
**Inland Empire Engravings** *March 19, 2018 03:24*

**+Anthony Bolgar**  

Yeah


---
**Anthony Bolgar** *March 19, 2018 04:20*

A lot of hard working blue collar folks in your neck of the woods. Not to mention some great race tracks!




---
**Inland Empire Engravings** *March 19, 2018 06:55*

**+HalfNormal** 

Would you know we're I can download Moshidraw software  so I can at least see if the machine works be for I do the upgrade


---
**HalfNormal** *March 19, 2018 15:07*

Search here or the web for Moshidraw. It is not hard to find. I also posted a cleaned up manual from chinese to english.


---
**Fabiano Ramos** *March 19, 2018 16:07*

**+Inland Empire Engravings** 



The best source of internet:

[maximsoft.blogspot.com.br - MoshiDraw relises.](http://maximsoft.blogspot.com.br/2013/11/moshidraw-relises.html)



Is in Russian. use the Google translate tool. the site is better than the official moshidraw website because it has older versions as well as drivers for x64 version of Windows 7.



I think I'll download all the drivers from that site and keep a backup. It took me weeks to find him, after suffering like an idiot looking uselessly on moshisoft's website.


---
**HalfNormal** *March 19, 2018 16:09*

**+Fabiano Ramos** if you do download all the information, I'd be happy to post it on my Google Drive for people to have.


---
**Fabiano Ramos** *March 19, 2018 21:08*

**+HalfNormal**  yeap. i will provide. 


---
**Inland Empire Engravings** *March 20, 2018 04:52*

Where can I get steps with pictures of how to upgrade from  a Moshi control board to Cohesion3D mini board 


---
**Fabiano Ramos** *March 20, 2018 18:45*

**+Inland Empire Engravings** 

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



[cohesion3d.com/start](http://cohesion3d.com/start)












---
**Inland Empire Engravings** *March 20, 2018 22:16*

**+Fabiano Ramos** 

That doesn't show how to convert a moshi control board to Cohesion3D mini board my wires look different than the ones in the picture


---
**Fabiano Ramos** *March 20, 2018 23:32*

**+Cohesion3D**


---
**Inland Empire Engravings** *March 20, 2018 23:48*

**+Ray Kholodovsky** 

Any schematics on how to do the conversion of a Moshi board to a c3d board


---
**Ray Kholodovsky (Cohesion3D)** *March 21, 2018 01:55*

It would be some combination of what is already shown in our install guide for the M2Nano board at [cohesion3d.com - Getting Started](http://cohesion3d.com/start)




---
**Inland Empire Engravings** *March 21, 2018 22:37*

**+Ned Hill** 

Do I need a dongle or can I use a USB cord?


---
**Ned Hill** *March 21, 2018 22:43*

The computer-laser connection is made with a usb cable but you need a moshi board dongle to use the moshidraw software.   It’s a stupid software key lock. 


---
**HalfNormal** *March 21, 2018 22:52*

When you convert to the Cohesion 3D board you will not need a dongle. You only need a USB cable.


---
**Inland Empire Engravings** *March 21, 2018 23:10*

**+HalfNormal** 

I'm I better off getting a new 40w laser engraver than trying to upgrade with cohesion 3d? Because I can find the right wire diagram to upgrade from Moshi board to cohesion board 3d?


---
**HalfNormal** *March 21, 2018 23:21*

Purchasing a new K40 will not take care of the main issue that the board supplied with these lasers are not high end and you have to use proprietary software. Go to Don's blogspot and take a look at the power supply section. You will find your power supply there and it will explain what everything does. Because these have changed often over the years there will be some slight variances. You will need to be diligent and trace the wires to find out where they go and what they do. Unfortunately there is no one-size-fits-all. If you are worried about the cost. You could always go with a simple Arduino and driver board. That will set you back about $20.


---
**Ned Hill** *March 21, 2018 23:27*

The other question to consider is if that’s an old machine is what condition is the tube in. 


---
**Dale Capen** *March 27, 2018 17:15*

coreldraw graphics suite x8 you can get off ebay for 70.

which goes for 400. also download corellaser which is free down load [scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html#download)




---
**Ned Hill** *March 27, 2018 17:26*

Keep in mind that a coreldraw X8 off eBay for $70 is a copy where they are selling multiple copies of the same license key. This can get the program locked if the software checks into Corel.   Most people go with Inkscape which is free. 


---
*Imported from [Google+](https://plus.google.com/102177636117481238975/posts/R1oLsffDFEc) &mdash; content and formatting may not be reliable*
