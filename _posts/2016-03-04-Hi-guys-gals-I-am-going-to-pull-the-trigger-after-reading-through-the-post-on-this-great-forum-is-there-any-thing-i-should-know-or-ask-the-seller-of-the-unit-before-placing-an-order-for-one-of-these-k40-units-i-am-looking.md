---
layout: post
title: "Hi guy's & gals I am going to pull the trigger after reading through the post on this great forum is there any thing i should know or ask the seller of the unit before placing an order for one of these k40 unit's i am looking"
date: March 04, 2016 18:37
category: "Discussion"
author: "Dennis Fuente"
---
Hi guy's & gals 

I am going to pull the trigger after reading through the post on this great forum is there any thing i should know or ask the seller of the unit before placing an order for one of these k40 unit's i am looking at Ebay they seam to be all the same price the sellers play with the price a little by changing the shipping cost.

l thanks to all for sharing there experince and info.



Dennis 





**"Dennis Fuente"**

---
---
**Dennis Fuente** *March 04, 2016 18:47*

laser drw ok thanks would that be listed in the supplied items or do i have to ask the seller i take it that  laser drw is the better of the 2 software 


---
**Jim Hatch** *March 04, 2016 18:53*

**+Dennis Fuente**​ the Moshi based ones will spec a Moshi board.


---
**Dennis Fuente** *March 04, 2016 18:53*

So i am looking in the add discriptions and i don't see where it say's moshi drw or laser drw ?


---
**Dennis Fuente** *March 04, 2016 18:55*

ok thanks glad i asked thank's 


---
**Jim Hatch** *March 04, 2016 18:58*

**+Dennis Fuente**​ I haven't seen any listings mention the software but have seen them spec the board - Moshi and Nano. The Nano board comes with LaserDRW and I think the Moshi board ones come with Moshi software and LaserDRW isn't an option but don't know for sure.



You can send the seller a question - they all seem to be pretty customer centered and answer questions.


---
**Dennis Fuente** *March 04, 2016 19:05*

Thanks guy's i am looking now through all the adds sure are a lot of them 


---
**Tony Sobczak** *March 04, 2016 19:40*

I got mine with laserdw from globalfreeshipping. I live in TN and fit it in 4 days via FedEx. Their customer service is outstanding. Received in great condition, mirrors aligned and is working perfectly. Highly recommend them. 


---
**Dennis Fuente** *March 04, 2016 22:53*

Thanks for the recommendation 


---
**Dennis Fuente** *March 04, 2016 23:41*

hi guy's again looking through the ads on ebay i saw only 2 sellers listing the cutter with laserdw also it was a higher price for the unit then the mosidraw and i saw a listing for a unit with an updaterd board from japan but it did not list it as a mosiboard or a laserdrw board why is this.



thanks for the help


---
**Ben Walker** *March 05, 2016 02:12*

I would personally be suspicious of no listing of the software.   It may be old stock and moshidraw.  I used equipmentwhosaler1 on ebay (Ontario CA).   Not had any luck getting them to actually do what they keep saying they will do but mine arrived with the nano board and everything was aligned correctly.   No damage.  Double boxed and shrink wrapped to keep the Styrofoam out.  FedEx did an amazing job getting it to KY in just a few days.   Delivered on a Saturday.   


---
**Tony Sobczak** *March 05, 2016 02:15*

All units from globalfreeshipping come with laserdw. 


---
**Dennis Fuente** *March 05, 2016 02:26*

so i did order a unit from them ok this is what i had tried to find so the older versions had the moshidraw and the newer version has the laserdraw thanks for the info


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/Wd4Kdg1yrik) &mdash; content and formatting may not be reliable*
