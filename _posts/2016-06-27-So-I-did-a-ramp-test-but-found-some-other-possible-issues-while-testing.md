---
layout: post
title: "So I did a ramp test but found some other possible issues while testing ."
date: June 27, 2016 19:10
category: "Hardware and Laser settings"
author: "giavonni palombo"
---
So I did a ramp test but found some other possible issues while testing . 1. No laser untill 2.5 mA. 2. The power fluctuates, some times lower sometimes higher, while set and running by 1.5mA. 3. My power seems to max out at 13mA then fall of from there.



What would cause the power fluctuate? It was doing this while I was cutting a straight line, so there shouldn't be an issue about line thickness, curves or arcs even if it matters for the k40. My outside air temp 65° and coolant is the same. All my test were short bursts more more than 30 seconds at a time with approximately 30 seconds between. I only did 20 of these tests. I don't think its water temp.





**"giavonni palombo"**

---
---
**Ariel Yahni (UniKpty)** *June 27, 2016 19:13*

Regularly people get laser action at around 4to5mA


---
**giavonni palombo** *June 27, 2016 19:16*

So could that be why mine laser is maxing out sooner?


---
**Derek Schuetz** *June 27, 2016 19:30*

I noticed this also when I set my power to 30% inkscape I'm only doing 7ma. I know for a fact though my pin can provide 5v pwm signal so maybe the percent calculation in the firmware is wrong and really the 100% is actually only 60%~


---
**Thor Johnson** *June 27, 2016 19:31*

Before 4-5 ma, often times it won't lase properly; better commercial units have a "tickle mode" on the laser power supply to keep the tube operating a low powers.  As far as maxing out early, it almost sounds like there's something wrong with the cooling, the tube, or the optics (yeah, what else is there?).



Make sure you don't have any air bubbles near the mirrors.

Lift the return line of the cooling system and make sure the water is flowing at a reasonable rate (some of the coolant pumps have issues if you have the laser on a table and the bucket below it -- only a trickle of water -- you should have a good stream of water coming out.



Check to make sure your mirrors & lens are in good condition -- a mirror could be ok/marginal at 20W, but try to burn itself at full 40W.


---
**Derek Schuetz** *June 27, 2016 19:37*

**+Thor Johnson** lens and mirrors wouldn't affect what is mA meter is reading. Also would cooling affect it since the meter is reading the power the PSU is pumping into the tube and it wont be less if the tube is at different temperatures.


---
**Jim Hatch** *June 27, 2016 20:50*

**+Derek Schuetz** I agree. I don't think it's mirror/cooling. I think you've got either a power supply or a tube issue. The peak at 13ma before falling off and the fluctuations while cutting a straight line suggests power issue.


---
**HalfNormal** *June 27, 2016 20:57*

Verify all electrical connections are tight.


---
**giavonni palombo** *June 28, 2016 01:58*

I did cut a few different objects, I use corel, When watching the beam at the wood there are spots that the kerf is thinner that is also where I doesn't cut through, but the mA do not change in those spots. It does make a screening noise at times but not always at the same power level. I do not see any arcing at the tube.



Ill check wire connections tomorrow. Anything I should test for, far as power supply? How?



No air bubble in the tube one of the things I check first.


---
**giavonni palombo** *July 01, 2016 00:04*

Ok I believe the mA gauge is bad and causing the fluctuations in the power. How do I know because it was arcing and caught on fire amazing it was working. It could be the pot as well. So I just want to replace both. Has anyone changed the pot? What did you use and where did you get it?


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/fWV2mxaYWJn) &mdash; content and formatting may not be reliable*
