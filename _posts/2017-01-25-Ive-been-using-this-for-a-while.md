---
layout: post
title: "I've been using this for a while"
date: January 25, 2017 03:58
category: "Discussion"
author: "Bill Keeter"
---
I've been using this for a while. Curious if i'm the only one. 



It's a remote on/off switch used for Halloween or Christmas lights. I have my water pump, exhaust fan, and K40 plugged up through a surge protector to it. Easy way to make sure everything is on at the same time. Plus taking the remote away makes it a little harder for someone else to turn on my machine (kids).

![images/77030049a9a594475ff9d9beb105d056.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/77030049a9a594475ff9d9beb105d056.jpeg)



**"Bill Keeter"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 25, 2017 08:39*

Nifty idea. I don't use anything like that, but have my entire setup plugged into the same powerboard which I then use an extension lead to connect to the power outlet, thus everything comes on at when I plug it in (except the laser which I manually have to switch on).


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/7wGfnQzrnTe) &mdash; content and formatting may not be reliable*
