---
layout: post
title: "I broke my m2nano board. Does anyone have a working one they want to sell?"
date: December 11, 2017 15:25
category: "Original software and hardware issues"
author: "Scorch Works"
---
I broke my m2nano board.  Does anyone have a working one they want to sell?  The price of a new m2nano is $44 including shipping.





**"Scorch Works"**

---
---
**Anthony Bolgar** *December 11, 2017 15:56*

Willing  send you one for $30 if you still need it.


---
**Scorch Works** *December 11, 2017 18:22*

**+Anthony Bolgar** Do you have 2 of them to sell?  From your laser list (posted earlier today) you have had at least 2 K40s.


---
**Anthony Bolgar** *December 11, 2017 20:09*

I have 2 boards one is a nano the other is a HT-Xeon.


---
**Paul de Groot** *December 11, 2017 21:19*

I have a spare one and you can have it for free 😁


---
**Scorch Works** *December 12, 2017 00:28*

**+Anthony Bolgar**​ The m2nano for $30 sounds reasonable.  I will assume you intended US $ because that works out better for you.



The XT-Xeon would work but the connectors are different so it isn't worth the trouble.  Thanks for the info though.



I received your e-mail payment sent. Thanks.


---
**Scorch Works** *December 12, 2017 00:32*

**+Paul de Groot**​ Thanks, I could really use a spare if you are willing to ship it.  The least I could do is pay for shipping.



Thanks Paul, I sent my shipping info to you.


---
**Anthony Bolgar** *December 12, 2017 02:38*

Will mail it out to you tomorrow **+Scorch Works**


---
**Timothy Rothman** *December 16, 2017 17:08*

Good to see Paul and Scorch are connected!  I've contemplated sending you both an email to make intros.  I am anxiously awaiting Paul's Awesome.tech controller which will give me PWM capabilities for true greyscale engraving, but I'm really hooked on K40 Whisperer which only works with my nano controller which has no PWM and requires dithering (converting to black/white).  One can only hope ;)


---
**Scorch Works** *January 03, 2018 04:18*

**+Paul de Groot** Thanks Paul, I received the M2Nano board today and I am up and running again. I am really surprised it arrived before the board from **+Anthony Bolgar**.


---
**Paul de Groot** *January 03, 2018 05:23*

**+Scorch Works** no worries, happy I could help you out. Maybe you should set up a donate button on your webpage since so many people have downloaded the application. Cheers and happy new year!


---
**Anthony Bolgar** *January 04, 2018 01:36*

You haven't got it yet?!!!! Something has gone wrong then. If you don't get it by Friday please let me know and I will refund you.




---
**Scorch Works** *January 04, 2018 02:13*

**+Anthony Bolgar** Thanks, I will keep watching for it.


---
*Imported from [Google+](https://plus.google.com/+ScorchWorks/posts/MrouE8SUFnY) &mdash; content and formatting may not be reliable*
