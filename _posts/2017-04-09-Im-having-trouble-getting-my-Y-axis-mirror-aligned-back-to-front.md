---
layout: post
title: "I'm having trouble getting my Y axis mirror aligned back to front"
date: April 09, 2017 04:51
category: "Original software and hardware issues"
author: "Bob Buechler"
---
I'm having trouble getting my Y axis mirror aligned back to front. I've tried everything I can find in instructions online to get the beam to hit the same spot, but the beam hits very high when the mirror is at its furthest position, and I've pushed the adjustment screws to their limits and could only achieve closing about half the distance. I've checked to make sure the chassis is level front to back and side to side, as well. Any other ideas I can try?





![images/1a4fe0bfcb7a1731775046aeb23fa48f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1a4fe0bfcb7a1731775046aeb23fa48f.jpeg)



**"Bob Buechler"**

---
---
**Joe Alexander** *April 09, 2017 05:01*

is your laser tube parallel to the gantry and perpendicular to that mirror? is it also hitting dead center on the first mirror? your tube could be slightly unlevel causing it to climb over the distance


---
**Joe Alexander** *April 09, 2017 05:16*

also, is the mirror in the pic the one your adjusting?  it looks maxed out to aim as high as it can go. I cannot imagine your getting a beam to your laser head at that setting...remember when adjusting you want to adjust the last bounce point not the target.(IE mirror 2 off so adjust mirror 1)


---
**Bob Buechler** *April 09, 2017 05:21*

**+Joe** that's.... probably what I'm doing wrong. :/ heh -- I always seem to miss that logic for some reason. I keep thinking I have to adjust the current mirror to fix the current alignment problem. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 09, 2017 05:37*

My Mirror #1 was doing something similar originally & took me 3 days to work it out. There are 2 screws at the back of that mirror mount that protruded under the back of the mount & touched the U shape bracket beneath it, angling the beam downwards at the furthest position. I fixed it by packing some washers under the area that I was bolting that mount into the U shape bracket.



edit: I mean my Mirror #1 (near tube) was what was causing bad vertical alignment issues on Mirror #2 (Y-axis).


---
**Ian C** *April 09, 2017 08:46*

Hey buddy. Is your bed even level in the X and Y axis? When I had my K40 I had to shim the bed 3mm up in one corner alone and slightly less in others. If the bed isn't level no amount of beam path tweaking is going to help, as the laser head isn't moving in the same plane.

Align/level the bed first and then switch to aligning the beam,  which might entail raising or lowering the tube.



Speaking of tube it could even be that the tube isn't level, if it's not then it's going to be shooting at an angle.



It's a tinkering machine thr K40 but once done it works great.


---
**Ned Hill** *April 09, 2017 14:40*

In case you haven't seen it, see this post for a link to the floatingwombat guide for alignment.  It's pretty good.  [plus.google.com - - Laser Alignment Guide - With all the laser alignment questions we get, here...](https://plus.google.com/108257646900674223133/posts/LGfT6SS3Tcc)


---
**Bob Buechler** *April 09, 2017 16:23*

**+Ned Hill**​ thanks, Ned. That's the guide I have mainly used, I just keep forgetting the adjustment logic. I'll take a look at the G+ thread as well. 


---
**Bob Buechler** *April 09, 2017 16:26*

**+Ian C** I seem to have been pretty lucky in terms of leveling. Still tinkering, obviously, but so far that much hasn't been bad.


---
**Ian C** *April 09, 2017 17:24*

Hey Buddy. Ah cool. I soon learnt the best way to align the mirrors is to manually move the mirror to get the spot central when in the clostest position to the previous reflector and then use the thumb screw adjustments for the far alignment. The problem is you can't use the thumb screws for the near and far adjustments as doing so will always put one position out of alignment


---
**Bob Buechler** *April 09, 2017 17:30*

**+Ian C**​ what do you mean by manually move the mirror? Only adjustment options I see are the thumbscrews, or packing/unpacking the mounting bolts to raise/lower the whole assembly, and/or sliding the mirror mount left/right on the assembly by loosening its two screws. Is that what you're talking about?


---
**Ian C** *April 09, 2017 17:55*

**+Bob Buechler** Yes that's it, you need to actually move the mirror mounting bracket that holds the mirror and pack it accordingly to raise or lower it. Sadly with the K40 mirror adjustment is very crude and limited, but you have to slacken the holding bracket and physically move the mirror, then nip up the securing screws. 

Once you've done this for the near firing position, then move the carriage away to the furthest point, zap and adjust the thumb screws.


---
**Bob Buechler** *April 09, 2017 19:26*

**+Ian C** So if I pack both my fixed mirror and my Y axis mirror to raise them up slightly, would I also raise the laser head if needed before fine tuning with the Y axis thumb screws?


---
**Ian C** *April 09, 2017 19:40*

**+Bob Buechler** Yes, you would need to in order to maintain the beam path. But and here is the tinkering part, it might be better to lower the tube (while keeping it level) as this will lower the beam path overall. 

It might be worth leaving the first fixed mirror alone, making the laser tube right for this mirror, then adjusting the other mirrors to suit.

Or, seeing as the laser head height is fixed, setting the laser path to that height and then adjusting all other mirrors inbetween to this new beam height. 


---
**Don Kleinschnitz Jr.** *April 12, 2017 12:52*

**+Bob Buechler** 

Some stuff that may help.... or not! There is an investment to get this setup but I find that it gets everything level and aligned pretty fast in all axis. Probably not needed once the tube is aligned.



[http://donsthings.blogspot.com/2016/06/k40-alignment-tools.html](http://donsthings.blogspot.com/2016/06/k40-alignment-tools.html)



While you are at it you an check and see if the #2 mirror is unstable. 

[donsthings.blogspot.com - K40 Optical Path Improvements](http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html)


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/2XjPxGNZJE3) &mdash; content and formatting may not be reliable*
