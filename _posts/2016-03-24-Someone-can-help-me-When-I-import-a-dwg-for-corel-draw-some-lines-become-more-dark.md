---
layout: post
title: "Someone can help me? When I import a dwg for corel draw some lines become more dark"
date: March 24, 2016 01:00
category: "Software"
author: "Pedro David"
---
Someone can help me? When I import a dwg for corel draw some lines become more dark. After the laser will spend more time on those lines.

Thank you!

![images/ca64ae8d7e5cf331aa155947e94b43a3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca64ae8d7e5cf331aa155947e94b43a3.jpeg)



**"Pedro David"**

---
---
**Scott Marshall** *March 24, 2016 02:11*

You're probably  having 1 of 2 issues,

# 1 double pass on those lines. It's possible to get lines drawn 1 on top of another, and they will import into LaserDRW  or Corelaser, carrying that attribute with no visible evidence once converted.  Watch to see if the lines are indeed being cut twice (just do a dry run with the laser off)



#2 Mirrors out of alignment, usually manifests itself as a certain quadrant(s) being lighter/heavier than others. This is because as the distance between the mirrors gets larger, so does the amount of misalignment. Depending on chance, this shift either increased or decreases the net intensity.



If you havn't done a full alignment, Start there.

Here's a helpful rundown on a popular method for alignment.



[http://www.floatingwombat.me.uk/wordpress/?attachment_id=30](http://www.floatingwombat.me.uk/wordpress/?attachment_id=30)




---
**Pedro David** *March 24, 2016 10:32*

I think it's the software configuration. I Make the drawing in AutoCad (.dwg format) and then send to Corel Draw 12. In autocad apparently everything is fine.


---
**Scott Marshall** *March 24, 2016 12:45*

You're right, it's in the importing process, but I've not found anything that fixes it other than the process below. I can't get Corel to import .dxf or .pdf outputs properly either.



Try erasing one of the  offending elements in Autocad with and see if they re-appear when you perform a zoom or redraw. If so, you have the double line thing going. Also, Corel doesn't work well with anything other than layer 0(keep settings default). Even if you freeze and turn off the other layers, they cause problems in Corel. The only other possibility I can think of is you may have a line width setting (for those elements) other than default.



That's about all I can think of. You are using the same workflow as I am (except I'm currently using Draftsight)



The Corel converter takes everything in the drawing even if it's not visable. I've had to re-do drawings because of this, I've not been able to do cut and engrave on the same drawing by turning on and off layers, but sure would like to. Right now I draw a master, then copy it to the "engrave" drawing and erase the cut elements, than I copy the master to the "cut"version and erase only the engrave elements. You also have to make the switch in Corel when you move to the cut drawing. I think it's going to require switching boards and loosing Corel to get it doing both on an imported drawing (it may work with a native drawing in Corel, but I hate drawing in it - used to Autocad)



If you (or anyone) DOES figure out how to make it behave properly, I would really appreciate a heads up, otherwise it's a new controller for me - it's just a matter of time till I get fed up with whole annoying process.

Good luck, 

Scott



﻿


---
**Coherent** *March 25, 2016 18:23*

I had a similar issue with an Autocad drawing. First I reloaded the original in Autocad and deleted the suspect portions and redrew them to ensure I didn't have any double lines. While I had the drawing open, Autocad has a couple ways to delete duplicate items. I think one is under the modify tab. Another is under tools, "drawing cleanup", and something is in the express tools if i recall as a command ("overkill" I think). Anyway, I'm not sure if my re-drawing, using the delete duplicate or, or simply the fact that re-converting them in Corel worked correctly, but the final result was the issue was gone. 


---
*Imported from [Google+](https://plus.google.com/113379323980322370039/posts/7EbHj9SsgDY) &mdash; content and formatting may not be reliable*
