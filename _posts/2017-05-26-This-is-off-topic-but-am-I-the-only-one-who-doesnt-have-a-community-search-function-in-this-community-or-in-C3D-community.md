---
layout: post
title: "This is off topic but am I the only one who doesn't have a community search function in this community or in C3D community?"
date: May 26, 2017 01:24
category: "Discussion"
author: "Steve Clark"
---
This is off topic but am I the only one who doesn't have a community search function in this community or in C3D community? Not only that the one LaserWeb is there but doesn't work!



I looked through settings and everywhere else no luck finding out why it's this way??





**"Steve Clark"**

---
---
**Mark Brown** *May 26, 2017 01:42*

On my phone search is in the "..." button in the top right corner. 



On my pc it's in the bottom right of the window, but that section doesn't scroll. So I can only barely see it if I stretch the window to full screen. 


---
**Steve Clark** *May 26, 2017 07:09*

**+Mark Brown**  thanks' for the info...can't see it at all here and in Coheasion3D the search doesn't work.


---
**Don Kleinschnitz Jr.** *May 26, 2017 11:21*

Hmmm, mine is on the left in the sidebar just above "about community" and it seems to work?


---
**Anthony Bolgar** *May 26, 2017 14:51*

Mine is hidden and I can not scroll to it. Every community for me has a different placement but most are usually hidden from me.


---
**Steve Clark** *May 26, 2017 15:56*

It's really wierd... I have the most current Chrome and on win10.  The fact that two communities search options are but don't work is odder.


---
**Mark Brown** *May 26, 2017 21:25*

Here it is on a Mac, running Opera.  On pc/firefox I can only see the top half of it.  Google+ is kinda poorly designed.  I just now realized (reading Don's post) that the search bar is in a little scrolling box, with community links and sections under it.



If I narrow the window a little I can see more of it, you might try that.



If I narrow the window even more it switches to a vertical layout (banner at top, single line of posts under) and then the search is in the "..." button.

![images/95a5e28cde6e8d319c9e561a778e09b8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/95a5e28cde6e8d319c9e561a778e09b8.png)


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/SiEouEv6r1c) &mdash; content and formatting may not be reliable*
