---
layout: post
title: "- Laser Alignment Guide - With all the laser alignment questions we get, here is the link to the floating wombat alignment instructions that I found useful when I started"
date: January 13, 2017 00:37
category: "External links&#x3a; Blog, forum, etc"
author: "Ned Hill"
---
- Laser Alignment Guide -

With all the laser alignment questions we get, here is the link to the floating wombat alignment instructions that I found useful when I started.  I'm putting this in the links section so hopefully it will be more easily found. When using this guide I found it helpful to use a marker and label the mirrors screws as given in the guide.   If anyone else has a good instruction set please feel free to add it to the comment section. #K40Alignment

[https://drive.google.com/open?id=0BwnmNmBaSoaGOWUtWFI3X2ZrSDQ](https://drive.google.com/open?id=0BwnmNmBaSoaGOWUtWFI3X2ZrSDQ)





**"Ned Hill"**

---
---
**Lorenz Hoffmann-Kuhnt** *January 18, 2017 14:27*

Have many thanks for this instruction!


---
**1981therealfury** *March 26, 2017 18:28*

Link doesn't appear to be working any more, just getting server not found error.


---
**Ned Hill** *March 26, 2017 18:34*

Thanks **+1981therealfury** for letting me know.  I updated it with a link to my onedrive.


---
**Don Kleinschnitz Jr.** *September 28, 2017 11:33*

#K40Optics


---
**Denisse Bazbaz** *May 14, 2018 18:20*

Hi 

I can't enter to the link, I don't know if you can upload it again

Thanks


---
**Ned Hill** *May 14, 2018 18:29*

**+Denisse Bazbaz** Ok, updated the link.


---
**Denisse Bazbaz** *May 14, 2018 20:02*

**+Ned Hill**Thanks




---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/LGfT6SS3Tcc) &mdash; content and formatting may not be reliable*
