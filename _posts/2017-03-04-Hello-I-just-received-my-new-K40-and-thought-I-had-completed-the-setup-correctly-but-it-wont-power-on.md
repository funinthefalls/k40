---
layout: post
title: "Hello, I just received my new K40 and thought I had completed the setup correctly, but it won't power on"
date: March 04, 2017 22:08
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Hello,

I just received my new K40 and thought I had completed the setup correctly, but it won't power on. At all. I turn on the power button and nothing happens. Any ideas of what I'm doing wrong?



Thanks in advance





**"Nathan Thomas"**

---
---
**greg greene** *March 04, 2017 22:23*

Check the wiring, these machines have a reputation for not having the electrical connections properly done some wires just twisted together or not attached to switches.  MAKE SURE IT IS GROUNDED VIA THE GROUNDING POST AT THE BACK !


---
**Don Kleinschnitz Jr.** *March 04, 2017 22:36*

Please post a picture of the control panel.

Also there may be a fuse in the rear AC plug.


---
**Nathan Thomas** *March 04, 2017 22:41*

Yes you were right, it was a loose wire to the power supply inside the machine. I haven't figured out the grounding part yet. Are you all running a copper wire grounding outside your homes?


---
**greg greene** *March 04, 2017 22:44*

Absolutely !


---
**Don Kleinschnitz Jr.** *March 04, 2017 22:44*

**+Nathan Thomas** I am not, as my house is properly grounded and 3 wire therefore not needed. Your needs may be different.


---
**greg greene** *March 04, 2017 22:45*

My house is the same - but I'm also running a separate ground


---
**Jim Hatch** *March 04, 2017 23:11*

Ditto Greg. My house ground is 100 ft from my K40's outlet. The K40's leaky case is 6 ft from the damp floor in the garage. A separate grounding rod outside the garage connected to the K40 has eliminated any current leaks through the operator :-)


---
**Nathan Thomas** *March 04, 2017 23:13*

**+Don Kleinschnitz** Yes sir, I was thinking the same thing since that's my same setup as well. 


---
**Nathan Thomas** *March 04, 2017 23:15*

Also thinking about getting a dedicated RIDGID cord to use only for the K40 and run it to the outside GFCI power supply we have.


---
**Don Kleinschnitz Jr.** *March 05, 2017 00:22*

**+Nathan Thomas** My K40 is on the second floor of my house so running a line to earth is no shorter than my houses wiring. Also I live in mountainous region where the grnd rod is buried deep. I doubt I could do better than what they did.

If I had an out building I would consider if the path through the earth to the transformer was electrically shorter than using the ground wire  to the building. Then add a rod if the earth path is shorter. 

.....

I wouldn't be a bit surprised if your K40 trips a GFCI circuit due to noise. K40's have no line filters and GFCI are susceptible to common mode noise. We really should install filters.

......

In addition to verifying external ground connections insure that all the grounding in the machine is properly done especially your cabinet. 

Actually measure the resistance to ground at the supplies, the cabinet surfaces and the input plugs. 

Frame grounding should be done with star washers on both sides of lugs. I went as far as soldering or replacing push on connectors for grounds. Fastons are notorious for being crimped wrong and corroding. 

You want to make sure that if for some reason you get a short to operator surfaces it blows a fuse. 

.....

The most unusually dangerous part of these machines is the lasers high voltage and the lasers optical output. 

Grounding your machine prevents your machine cabinet or controls from being at the wrong potential however even if your machine is grounded and you get exposed to the HV output, with a path to its return, you will get hurt. A ground rod does not prevent that.

This is why I emphasize the importance of LPS interlocks, it protects you from both optical and HV exposure.

.....

The key consideration is that if the resistance from the source through your body back to its return is lower through your body than any safety system, you get electrocuted. 



[http://www.system-safety.org/conferences/2011/papers/How%20to%20Avoid%20Common%20Pitfalls%20in%20Electrical%20Grounding.pdf](http://www.system-safety.org/conferences/2011/papers/How%20to%20Avoid%20Common%20Pitfalls%20in%20Electrical%20Grounding.pdf)


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/YjtMj6XtmUp) &mdash; content and formatting may not be reliable*
