---
layout: post
title: "I managed to remove the old bed all in one go, many of these machines have slight construction differences so not sure if everyone can do this"
date: November 28, 2015 12:27
category: "Modification"
author: "Gary McKinnon"
---
I managed to remove the old bed all in one go, many of these machines have slight construction differences so not sure if everyone can do this.



Mine was held down by 5 x 7/64 hex screws, 3 at front, 2 at back.



I can't remove the extractor ducting without removing part or all of the frame so i'll wait until my set square arrives so that i can do the square check at the same time.﻿



Part of me wants to see if i can laser-cut off the unneeded length of extractor ducting but i'm not sure if that's a good idea or even possible ;+}



![images/f44ab2dc00b0f497b71b0c35807cf597.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f44ab2dc00b0f497b71b0c35807cf597.jpeg)
![images/21f6051b7c0c3ee0944f72d93b7c9d28.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/21f6051b7c0c3ee0944f72d93b7c9d28.jpeg)
![images/5c536e62132874782710ea0778532cbf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c536e62132874782710ea0778532cbf.jpeg)

**"Gary McKinnon"**

---
---
**Gary McKinnon** *November 28, 2015 12:28*

I was surprised to find that the hex-screw poles were rusty! China is definitely a nation of recyclers ...


---
**DIY3DTECH.com** *November 28, 2015 12:32*

Interesting mine is the same bed however it has brackets drilled into the sides of the rails (ugh) rather than the stanchions. Also have a lot of rust in mine too (likely from the humidity setting in a warehouse in China)...    


---
**Sean Cherven** *November 28, 2015 12:39*

Mine has these standoffs as well. Should be easy to remove this way.


---
**Gary McKinnon** *November 28, 2015 12:41*

**+DIY3DTECH.com** 

 What board is yours? Also, are you able to do a video on how to check if the machine is grounded properly ?


---
**Scott Thorne** *November 28, 2015 13:18*

My bed was exactly the same as yours Gary....I bought the honeycomb table and use 1/2 inch spacers to get it to whatever height I need it.


---
**Scott Thorne** *November 28, 2015 13:25*

I've changed everything in mine but the control board...lol


---
**Scott Thorne** *November 28, 2015 13:25*

New power supply, puri 40 watt tube.


---
**Brooke Hedrick** *November 28, 2015 14:19*

Mine was the same too - including the rusty stand offs.


---
**Ashley M. Kirchner [Norym]** *November 28, 2015 15:32*

You can not laser cut the duct with a 40W. I took it out completely. As for the rails, it comes out in one piece and does not need to be taken apart. There are four bolts under the rear and front rails. The one on the front right is a bit of a pain because it's right next to the stepper motor. Do make sure to locate the wires for the front stepper (it's not the ribbon cable) and disconnect it from the board before you try to lift the rails out of the enclosure. I've removed the rails once to remove the air duct then again because I redrilled the holes for the stands as mine were all bend sideways to match the holes on the bed. They were so far off that it was bending the bed out of shape. I'll be replacing it for a z-bed anyway but for the time being I wanted a flat bed to work with.


---
**Ashley M. Kirchner [Norym]** *November 28, 2015 15:35*

Oh, and before I put the rails back in, I moved the fixed mirror bracket that's on the left. I pushed the rails as far left as they could go to give me more room on the right between the rail and the separator wall so I can drop a cable chain in that space. Having done that, the right fixed mirror no longer aligned with the laser beam, so I had to adjust the bracket that the mirror is in. Simple two screws to move it over and re-align the mirror.


---
**Gary McKinnon** *November 28, 2015 15:45*

**+Ashley M. Kirchner** Thanks, very helpful.


---
**Gee Willikers** *November 28, 2015 18:23*

The most the laser will be able to do is burn the blue paint off that piece of duct work. To remove it you need to remove the whole xy stage (four 10mm bolts). Not a difficult project but you will need to realign the laser once you reinstall the stage.


---
**DIY3DTECH.com** *November 29, 2015 03:42*

My duct slides out of the back with the removal three small bolts and it went to visit the band-saw too.  Also what I also did was 3d print an insert and made the duct air tight so it sucks only from the duct and not around it.  Works well...


---
**Sean Cherven** *November 29, 2015 05:22*

That's interesting.. My duct is welded to the frame. Can't be removed at all.


---
**Sean Cherven** *November 29, 2015 05:23*

Could you take some pics or video of your duct and how it mounts?


---
**Ashley M. Kirchner [Norym]** *November 29, 2015 18:21*

Honestly, just remove the thing. It's too long and gets in the way of the full area that you can use, it forces the airflow right over the cutting piece, it's just not ideal. I removed mine completely. Now, with air assist, it blows the smoke downwards through the cut and it gets sucked out then from underneath.


---
*Imported from [Google+](https://plus.google.com/+GaryMcKinnonUFO/posts/314ctx66CHR) &mdash; content and formatting may not be reliable*
