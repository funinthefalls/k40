---
layout: post
title: "Working on a custom made adjustable bed for my K40, i just made it out of aluminum angle, nuts and screws"
date: September 12, 2016 12:45
category: "Modification"
author: "Ben Crawford"
---
Working on a custom made adjustable bed for my K40, i just made it out of aluminum angle, nuts and screws. I still need to attach the mounts for the lead screws and the mount for the motor.



![images/a1a98507d0dc89f92da4391910947645.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a1a98507d0dc89f92da4391910947645.jpeg)
![images/0503d4c53c4a42205e2896b89f633a95.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0503d4c53c4a42205e2896b89f633a95.jpeg)

**"Ben Crawford"**

---
---
**Carl Fisher** *September 12, 2016 17:10*

I so need to do this. 


---
**Robert Selvey** *September 13, 2016 00:31*

Where did you find the honeycomb ?


---
**Ben Crawford** *September 13, 2016 00:37*

i bought the black platform online.


---
*Imported from [Google+](https://plus.google.com/110714520603831442756/posts/QTaSRF2v2g8) &mdash; content and formatting may not be reliable*
