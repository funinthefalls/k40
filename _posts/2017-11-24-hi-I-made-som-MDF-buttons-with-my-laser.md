---
layout: post
title: "hi! I made som MDF buttons with my laser"
date: November 24, 2017 10:00
category: "Materials and settings"
author: "Dag Elias S\u00f8rdal"
---
hi! 



I made som MDF buttons with my laser. sold about 60 of them but afraid that they need sealing of some sort to with stand being washed? 



Anyone have experience with this?

![images/b7470f5522b348a6757e82ea81a143d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7470f5522b348a6757e82ea81a143d9.jpeg)



**"Dag Elias S\u00f8rdal"**

---
---
**Mark Dolan** *November 24, 2017 10:18*

They definitely need to be sealed.  The only thing that MAY stand up to washing might be a marine grade epoxy, any other finish is bound to wear off rather quickly #IMHO.


---
**Chris Hurley** *November 24, 2017 12:09*

Switch to using acrylic. 


---
**Martin Dillon** *November 24, 2017 16:36*

I coated some wooden buttons, for my wife, with West System epoxy and they held up fine going through the washer.  They lasted  longer than the dress and I think they are in her button box now.


---
**Dag Elias Sørdal** *November 24, 2017 21:12*

**+Martin Dillon** so did you just "paint" the buttons with epoxy? Would be sooo thankful if you explained the procedure to do this :)




---
**Martin Dillon** *November 25, 2017 16:47*

West system epoxy is thinner than other expoxies and penetrates the wood better than some other epoxies.  I mixed up the epoxy and dipped them into the resin and then hung them to dry.  There will be a drip on the lowest part so hang them in a way that it won't matter and it will be easy to sand off.  You can brush it on as well.


---
**Dag Elias Sørdal** *November 25, 2017 18:32*

**+Martin Dillon** nice, thanks for the info! need to test this :) 


---
**Denisse Bazbaz** *May 06, 2018 18:57*

**+Dag Elias Sørdal** Hi I want to engrave something similar, but I can't get it, are wooden cases, can you help me please




---
**Dag Elias Sørdal** *May 14, 2018 06:18*

**+Denisse Bazbaz** what do you need help with? :)


---
*Imported from [Google+](https://plus.google.com/+DagEliasSørdalurm8/posts/Tf3anrDVK8Q) &mdash; content and formatting may not be reliable*
