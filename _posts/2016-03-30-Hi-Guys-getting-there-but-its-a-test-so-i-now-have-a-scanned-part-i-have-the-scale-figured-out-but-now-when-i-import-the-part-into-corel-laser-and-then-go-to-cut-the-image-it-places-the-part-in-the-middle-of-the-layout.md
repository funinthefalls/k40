---
layout: post
title: "Hi Guy's getting there but it's a test, so i now have a scanned part i have the scale figured out but now when i import the part into corel laser and then go to cut the image it places the part in the middle of the layout"
date: March 30, 2016 20:25
category: "Software"
author: "Dennis Fuente"
---
Hi Guy's getting there but it's a test, so i now have a scanned part i have the scale figured out but now when i import the part into corel laser and then go to cut the image it places the part in the middle of the layout screen i then pick the part and move it to where i want it to cut the material but the machine cut's it some where else  on the material i hope i am making sense thanks for any help.



Dennis 





**"Dennis Fuente"**

---
---
**Phillip Conroy** *March 30, 2016 20:43*

You do jot have to move  the part to the top left ar all -the larer cutting plugin will do that automaticily, ie it allways starts cutting on the back left side of the cutting bed,if you do not want to cut the part here you just change the start od the cut by changing x and y in the laser cutting plugin-what pops up when you go to cut.


---
**Dennis Fuente** *March 30, 2016 21:31*

I follow what you are saying but when i move the location in the laser cut pop up it moves the part as if it where when i saved the part file in inkscape i did get it to cut where i wanted the machine to cut the part on the material by moving the part in the orignal file and then saving it again i thought i would be able to move the part in the cut window but ti won't do it and i can't figure out why.

Thanks for the response

Dennis  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 23:33*

Some people design their file with a small square or dot in the top left corner, as a reference point for the software so that it starts at top left corner of the machine when cutting/engraving, but because the part is actually spaced away from that reference dot, it will cut where you want it to. Something like this: [https://drive.google.com/file/d/0Bzi2h1k_udXwenVWdThHRk5BZGM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwenVWdThHRk5BZGM/view?usp=sharing)



Hope that makes sense & helps.


---
**Phillip Conroy** *March 31, 2016 05:32*

The laser has the most power in the rear left hand side    


---
**Phillip Conroy** *March 31, 2016 05:36*

Bugger hit post by mistake ,as i was saying the k40 laser cutter has the most power to the laser beam in the rear left hand corner as the beam has shorter to travel and even with perfect mirror alignment  i would guess at lest 5-10% from the start postion to front right


---
**Dennis Fuente** *March 31, 2016 16:58*

Thanks guy's for your response, Phillip i don't know why but my machine has more power toward the front of the machine away from the tube i can set the power to engrave a part at this point and as it moves toward the front of the machine i have to reduce it or it just scorches the part.

 Yuusuf 

What was happening was when the file was created in my graphics program where ever the part was placed on the screen that's how it came into the corel laser i think i have it figured out at least for now.



Thanks guy's 



Dennis 


---
**Phillip Conroy** *April 01, 2016 01:35*

sounds like you have a mis aligned mirror


---
**Dennis Fuente** *April 01, 2016 16:05*

nope checked them out with targets spot on the money machine works great so far thanks for your response.



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 01, 2016 17:40*

It could be possible that the cutting bed is not level in comparison with the laser head's x/y rail movement. Imagine if the bed was slightly closer (or further away) from the laser head at the front for example. This would cause some kind of out of focus issue, which would result in thicker beam/scorching. Might be worth checking the focal distance at all points on the cutting bed (e.g. top left, top right, bottom left, bottom right) & doing a comparison to see if level at all the positions.


---
**Dennis Fuente** *April 02, 2016 23:41*

i did put a level on the bed looks pretty close i gues i could make a gauge for it what is the exact length for  a gauge thanks for the for the help



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 23:53*

For the stock lens, the focal point is 50.8mm. That should be hitting half-way through your material you are cutting. I see on your other post that you are cutting 3mm ply, so your gauge (at the top of the ply) should be 49.3mm. Give or take. I think a small margin of error is probably not that big a problem.


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/jUgBZaFHjSf) &mdash; content and formatting may not be reliable*
