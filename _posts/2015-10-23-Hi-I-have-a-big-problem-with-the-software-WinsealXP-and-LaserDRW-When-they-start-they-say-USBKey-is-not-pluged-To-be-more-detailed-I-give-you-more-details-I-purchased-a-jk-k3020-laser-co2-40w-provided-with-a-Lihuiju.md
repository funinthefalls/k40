---
layout: post
title: "Hi, I have a big problem with the software WinsealXP and LaserDRW When they start they say \"USBKey is not pluged\" To be more detailed I give you more details : I purchased a jk-k3020 laser co2 40w provided with a Lihuiju"
date: October 23, 2015 14:16
category: "Software"
author: "Alessandro Milano"
---
Hi,

I have a big problem with the software WinsealXP and LaserDRW

When they start they say "USBKey is not pluged"

To be more detailed I give you more details :

I purchased a jk-k3020 laser co2 40w provided with a Lihuiju Studio labs HT-MASTER-5 Nano mainboard dated 2015-06-05

I set the ID from the mainboard to the control software and I can communicate with the machine to set the starting position, speed, make a preview....

The problem is when I try to engrave a project : WinsealXP 2013.02 says to me that "USBKey is not pluged". It display the same message when it start and also if I click on Help.

When I insert the USB Dongle (B model) in the PC (I tryed with 3 different PC with Win7, WinXp and Win8) the operating system install correctly the new hardware with no errors, the USB Dongle light up (blinking led) but the software cannot recognize it.... (see photo)

Any idea ???

PLEASE HELP ME

Best regards



![images/925d01e00aada1f8c2b3dee50d2ba8d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/925d01e00aada1f8c2b3dee50d2ba8d4.jpeg)
![images/a0e23efb7d3c0e5d48385516c38749c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0e23efb7d3c0e5d48385516c38749c6.jpeg)

**"Alessandro Milano"**

---
---
**Sean Cherven** *October 23, 2015 14:32*

That key does not work with winseal, but it should work fine on laserdrw


---
**Joey Fitzpatrick** *October 23, 2015 15:53*

Like Sean said^^  You need the red USB key(C- Lock) to work with winseal LaserDRW and Corellaserl.  You have the B-Lock key  which only works with Corellaser and LaserDRW. The Blue USB key (A-Lock) will work with Winseal only.  Here is a link to their web site.  Look at the top of the page.  It will explain what the different USB keys work with.  [http://www.3wcad.com/](http://www.3wcad.com/)


---
**Sean Cherven** *October 23, 2015 16:09*

The B Lock should also work with LaserDRW.


---
**Phil Willis** *October 23, 2015 17:23*

The key I have with LaserDRW looks exactly like the one in the picture.


---
**Alessandro Milano** *October 26, 2015 07:43*

Hi,

I solved !!!

After your comments I have installed only CorelLASER and it works so thank you very much !!!!!!


---
**Eoin Kirwan** *June 28, 2016 21:12*

Can you operate the Laser Engraver fully from CoralLaser?


---
**Eoin Kirwan** *June 29, 2016 08:06*

Hi, How did you get an ID from the main board?


---
*Imported from [Google+](https://plus.google.com/100341006317202452626/posts/EnUkYgNwaKx) &mdash; content and formatting may not be reliable*
