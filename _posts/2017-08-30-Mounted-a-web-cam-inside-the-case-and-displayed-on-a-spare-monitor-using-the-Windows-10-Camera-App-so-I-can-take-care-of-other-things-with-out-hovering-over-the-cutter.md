---
layout: post
title: "Mounted a web cam inside the case and displayed on a spare monitor using the Windows 10 \"Camera App\" so I can take care of other things with out hovering over the cutter"
date: August 30, 2017 04:23
category: "Modification"
author: "Ray Rivera"
---
Mounted a web cam inside the case and displayed on a spare monitor using the Windows 10 "Camera App" so I can take care of other things with out hovering over the cutter. This especially helps with the long engraving and cutting jobs.


**Video content missing for image https://lh3.googleusercontent.com/-DXA25YtJdf0/WaY9o4IR3aI/AAAAAAAAOAM/1DNZxVpwb_wof-embs2ieRKIW1WFVXCtQCJoC/s0/WIN_20170830_00_19_00_Pro.mp4**
![images/c51a9d3694d1031e567add7d89ad4d52.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c51a9d3694d1031e567add7d89ad4d52.jpeg)



**"Ray Rivera"**

---
---
**Ray Rivera** *August 30, 2017 04:26*

![images/54ada9a316ea0541f7cc2384f55164d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/54ada9a316ea0541f7cc2384f55164d8.jpeg)


---
*Imported from [Google+](https://plus.google.com/117639914879018784950/posts/RFyX5R16QYE) &mdash; content and formatting may not be reliable*
