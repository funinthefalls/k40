---
layout: post
title: "Originally shared by Ariel Yahni (UniKpty) Again using Rhino to create wireframe 2D files from STL"
date: March 02, 2017 04:13
category: "Repository and designs"
author: "Ariel Yahni (UniKpty)"
---
<b>Originally shared by Ariel Yahni (UniKpty)</b>



Again using Rhino to create wireframe 2D files from STL. This are the ones needed to make those acrylic lamps. I cannot test them myself as i have a dead tube. If anyone is interested i can make them for you.

![images/a3cdbbeb736db0bc38be930dd16c3034.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a3cdbbeb736db0bc38be930dd16c3034.png)



**"Ariel Yahni (UniKpty)"**

---
---
**Paul de Groot** *March 02, 2017 05:32*

**+Ariel Yahni**​ i know that feeling of having a dead tube😦 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/aUaYaEKqFYn) &mdash; content and formatting may not be reliable*
