---
layout: post
title: "For those who use CAD, Jsketcher is a parametric 2D and 3D CAD modeler written in pure javascript"
date: December 26, 2016 16:08
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---


For those who use CAD,

Jsketcher is a parametric 2D and 3D CAD modeler written in pure javascript. Runs in a browser. Example link on site.



[https://github.com/xibyte/jsketcher](https://github.com/xibyte/jsketcher)





**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/1GH7aavt7p5) &mdash; content and formatting may not be reliable*
