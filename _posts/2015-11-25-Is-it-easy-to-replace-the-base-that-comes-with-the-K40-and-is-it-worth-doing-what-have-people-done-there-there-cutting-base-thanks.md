---
layout: post
title: "Is it easy to replace the base that comes with the K40 and is it worth doing, what have people done there there cutting base thanks"
date: November 25, 2015 12:07
category: "Modification"
author: "Tony Schelts"
---
Is it easy to replace the base that comes with the K40 and is it worth doing, what have people done there there cutting base thanks





**"Tony Schelts"**

---
---
**Gary McKinnon** *November 25, 2015 12:23*

Hi Tony, these search results have some helpful links : [https://goo.gl/sFzT2Z](https://goo.gl/sFzT2Z) 


---
**Gary McKinnon** *November 25, 2015 12:26*

And DIY3DTECH did a great video on this




{% include youtubePlayer.html id="ixx-YyAJhfs" %}
[https://www.youtube.com/watch?v=ixx-YyAJhfs&list=PLInTrkIbj69kPO_UP81yxX-xov4SVyPlD&index=4](https://www.youtube.com/watch?v=ixx-YyAJhfs&list=PLInTrkIbj69kPO_UP81yxX-xov4SVyPlD&index=4)




---
**Stephane Buisson** *November 25, 2015 12:42*

What you are looking for is a bed allowing the air to go trough, so minimum contact with the material, but having enough strength to hold the material. -> honeycomb is the best for that.


---
**Phil Willis** *November 25, 2015 22:30*

I removed the clamp attachment and added a honeycomb bed so it fits below the hole left by the clamp device. Seems to work and the laser focus is essentially in the same place. My intention is to get the Light Objects bed at some point, but being in the UK I will have to pay import duty and shipping is a bit expensive. Its a shame they don;t have a European reseller. Unless someone knows different?


---
**Tony Schelts** *November 25, 2015 22:55*

how much do you think the duty will cost.


---
**Phil Willis** *November 26, 2015 11:52*

**+Tony Schelts** Its effectively the VAT (20%) on the value of the goods and shipping if its under £135. £135 - £630 then there might be a 2.5% duty to pay as well! For more details [https://www.gov.uk/goods-sent-from-abroad/tax-and-duty](https://www.gov.uk/goods-sent-from-abroad/tax-and-duty)


---
**Gary McKinnon** *November 26, 2015 12:25*

The shipping is $72, so $232 all in to ship to UK.


---
**Tony Schelts** *November 26, 2015 23:29*

Thanks I better start saving up


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/cyBWpQXiBzq) &mdash; content and formatting may not be reliable*
