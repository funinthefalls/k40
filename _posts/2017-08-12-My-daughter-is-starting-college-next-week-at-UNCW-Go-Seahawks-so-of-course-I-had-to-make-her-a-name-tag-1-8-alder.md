---
layout: post
title: "My daughter is starting college next week at UNCW (Go Seahawks!) so of course I had to make her a name tag :) 1/8\" alder"
date: August 12, 2017 00:03
category: "Object produced with laser"
author: "Ned Hill"
---
My daughter is starting college next week at UNCW (Go Seahawks!) so of course I had to make her a name tag :)  1/8" alder.

![images/46828977a8c08036302a47a7949c9829.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/46828977a8c08036302a47a7949c9829.jpeg)



**"Ned Hill"**

---
---
**Cesar Tolentino** *August 12, 2017 14:21*

Nice... Yup go Hawks. 


---
**Ned Hill** *August 12, 2017 15:13*

**+Cesar Tolentino** In this case the Seahawk is the mascot of UNCW. If we are talking US football then it's GO Panthers! ;)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/bePxPsFXgah) &mdash; content and formatting may not be reliable*
