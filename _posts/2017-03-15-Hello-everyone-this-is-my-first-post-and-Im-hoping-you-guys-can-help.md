---
layout: post
title: "Hello everyone, this is my first post and I'm hoping you guys can help"
date: March 15, 2017 15:59
category: "Original software and hardware issues"
author: "Jeffrey Shorr"
---
Hello everyone, this is my first post and I'm hoping you guys can help. I've learned a lot from just lurking in here over the past year.



Anyway, I went to do some engraving in leather this morning and I set everything like I usually do and walked away for a second. When I came back I noticed the leather was burned up rather than engraved like it should have been. Come to find out I am unable to adjust the power of the laser using the stock adjustment dial. Could this part have gone bad or is it the controller board? How do you guys recommend proceeding?



Thanks,

Jeff





**"Jeffrey Shorr"**

---
---
**Mark Brown** *March 15, 2017 17:31*

Most likely the dial failed.  **+Don Kleinschnitz** has a little guide on them...



[donsthings.blogspot.in - When the Current Regulation Pot Fails!](http://donsthings.blogspot.in/2017/01/when-current-regulation-pot-fails.html)


---
**Jeffrey Shorr** *March 15, 2017 17:34*

Thank you for the link and information!


---
*Imported from [Google+](https://plus.google.com/108413984549339074475/posts/MmfsmVcEWnc) &mdash; content and formatting may not be reliable*
