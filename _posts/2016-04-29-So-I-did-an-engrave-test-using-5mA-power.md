---
layout: post
title: "So I did an engrave test using 5mA power"
date: April 29, 2016 14:51
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I did an engrave test using 5mA power. I was originally intending to do it using the Vector engrave method, but I had major dramas with it where for every one of the 4 layers (out of total 14) that I was engraving at different speeds, the X-alignment would be perfect, but it was stepping away from the Y-origin (vertical). So the first layer started at say 50mm Y. Then the second layer was supposed to be at 50mm also, but once completed it turned out to have been at ~52mm Y. Then 3rd seemed to finish at ~54mm, 4th ~55mm. So it looked to be getting less of a distance as I went through the layers. I started with layer 1 at 45mm/s, then layer 2 was 44mm/s, layer 3 was 43mm/s & layer 4 was 42mm/s.  It seemed that the vibrations from those speeds were causing it to skip steps (or something else I am unaware of). 30mm/s & less, which I had tested about 1/2 hour before hand seemed fine (no skipped steps or Y-mispositioning). Unfortunately, even at 4mA, 30mm/s & less basically burned straight through 3mm plywood. So I scrapped that idea for now (until I can get some thicker plywood like 6-9mm).



So instead, I just did the same image, reduced in scale to 25% size (as original was 280 x 180mm & would have taken forever to engrave using standard engrave). So this one, I did 14 layers, with varying speeds again (from 500mm/s down to 240mm/s, stepping by 20mm/s difference between each layer). The intended effect was to show the different shades in the image. Unfortunately, due to the tiny size of the piece, it is not as noticeable as I would have liked. Also, standard engrave is soooooo slow. Took 15-20 minutes per layer. x14 layers = somewhere around 210-280 minutes, or 3 - 4 2/3 hours. Way too long for such a tiny piece.



All this after many hours of designing the original image anyway + breaking it into shades, clipping masking it with rectangles (for vector engrave), etc.



Anyway, the overall result is interesting, to say the least. It works, showing some layers deeper than others but is very subtle in such a small size. I would love to test as the original size (280 x 180mm) but I honestly can't be bothered sitting there for 16+ hours waiting for it to finish.



One thing I'm curious on, for those of you running Smoothie + LaserWeb, when you are able to set multiple power levels for different objects using colours, can you engrave it all in one run with it adjusting the power as it needs to? (i.e. doesn't need 14 runs to complete the task like this did?)



![images/578ee4fe6da4ba147f82e15b19ea7f1b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/578ee4fe6da4ba147f82e15b19ea7f1b.jpeg)
![images/06da9123a10fe46ace1c8950ecf8ff24.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/06da9123a10fe46ace1c8950ecf8ff24.jpeg)
![images/90eed70f5a946dfdc7fd10b462a84fd9.png](https://gitlab.com/funinthefalls/k40/raw/master/images/90eed70f5a946dfdc7fd10b462a84fd9.png)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Richard** *April 29, 2016 15:27*

I don't know if this applies to smoothie board, but I read somewhere that the K40-like machines have trouble lining up layers. One work around I saw mentioned was in each layer put a single pixel in the upper left corner (aka origin). It's too small to burn so it won't show up in the final product, but it somehow gets the machine to align properly.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 15:42*

**+Richard Beline** Thanks Richard. It's not that issue in this case, as I have a 1x1mm square aligned in the top left corner of every layer (I always do that, outside the area of print, e.g. at 0,0 & start print at 1,1). Unfortunately for this case I think it was related to something else (if only it was so simple). I don't have smoothie yet, but am planning the upgrade (when $ permits), hence my question.


---
**Ariel Yahni (UniKpty)** *April 29, 2016 17:00*

You can use LW1 and it will detect color vectors and apply speed and power to each one. That's no possible with LW2 yet ( is that correct **+Peter van der Walt**​​ this haven't being ported yet, correct)?  Or you could use Raster on a greyscales image on LW and define a range for white and black and let it do its magic. ﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 17:17*

**+Ariel Yahni** Thanks Ariel. Just wondering if I'd be capable of this sort of imagery once I get around to upgrading to smoothie. The fact that LW1 supports it & LW2 will be getting it ported/added is promising.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 12:33*

**+Peter van der Walt** I don't really know much about the coding side of things, but can you include the old lib with the new lib (& use features that you like from both)?


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/PUyv2LDRFj8) &mdash; content and formatting may not be reliable*
