---
layout: post
title: "Tired getting inkscape plugin to work. Downloaded gcode file to test for the first time and now SD Card not working"
date: December 03, 2015 21:48
category: "Discussion"
author: "ChiRag Chaudhari"
---
Tired getting inkscape plugin to work. Downloaded gcode file to test for the first time and now SD Card not working. Tried FAT and FAT32 formats. It's Nokia 4gb class6. Any suggestions???



![images/736544cb7ac7ee6fa2be7983ac75c611.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/736544cb7ac7ee6fa2be7983ac75c611.jpeg)
![images/5b5fa2d60a616b7ad851dc86ddd39f9b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5b5fa2d60a616b7ad851dc86ddd39f9b.jpeg)

**"ChiRag Chaudhari"**

---
---
**Sean Cherven** *December 03, 2015 22:12*

The filename of the gcode file should end in ".g". Such as "file.g"



The SD card might be an issue, try another one if all else fails.



Try a 2GB Card if you have one laying around.


---
**David Richards (djrm)** *December 03, 2015 23:01*

Hi Sean, My Marlin controller will only recognise 8.3 filenames on the SD card, hth David.


---
**Sean Cherven** *December 03, 2015 23:10*

I don't know about the marlin for laser cutters, but ik for 3D Printers, it will only show .g and .gco filename


---
**David Richards (djrm)** *December 03, 2015 23:13*

I think that is exactly what I was trying to say Sean but it didn't come out that way ;-) I use Marlin on my 3D printer.


---
**Sean Cherven** *December 04, 2015 00:47*

It's all good lol


---
**ChiRag Chaudhari** *December 04, 2015 00:58*

**+Sean Cherven** it took me about 2 hours to find memory cards from my electronics junk. Found couple of Canon 32MBs :O and a Sandisk 2GB. Both working perfectly. Also both .gcode a .g working exactly the same. Now time to  get the Damn plugin working. 



For some reason Turnkey plugin doesn't show up under Extension>Export. Tried J Tech Photonic plugin. And it does shows up and create s gcode too.



One question M3 to turn laser ON and M5 to turn laser off ? 


---
**Sean Cherven** *December 04, 2015 02:23*

Well atleast you got the SD Card working. As for the rest, i don't have a clue, as I'm still running the stock controller.


---
**ChiRag Chaudhari** *December 04, 2015 02:26*

**+Sean Cherven** Yeah that was one hurdle out of the way. I still cant get Turnkey Plugin to work, but used online gcode generator by Lancing Makers group. Does the job but all everything is mirrored/inverted. So I guess that is Inkscape issue. working on that atm.


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/VgSeKuKGSfN) &mdash; content and formatting may not be reliable*
