---
layout: post
title: "Wow, I thought I had this build beaten this weekend"
date: March 26, 2017 23:50
category: "Smoothieboard Modification"
author: "Bradley Blodgett"
---
Wow, I thought I had this build beaten this weekend. I had to replace me X-Axis stepper and I have great movement control down which seems to be a moderate challenge. My big problem now is not getting any sort of laser fire. I've traced my wires and checked my connections. Should I be able to test fire without any sort of fire control from the smoothie? Seems like it should be independent unless there is code being sent. I've been using Don's guide all along the process, but I am at a loss at the moment. I have a replacement tube and power supply, but I would rather troubleshoot what I have in the box now before I start swapping components. I don't really have the capability of testing High Voltage output, but I do have a standard multimeter. It looks like I have a G-G-G or a Type "C" power supply. Any help would be appreciated!





**"Bradley Blodgett"**

---
---
**Don Kleinschnitz Jr.** *March 27, 2017 00:24*

You should be able to fire the laser without any controller as long as you have the laser enable asserted. 

Is this a STD K40 panel and wiring?


---
**Joe Alexander** *March 27, 2017 03:02*

as long as the WP circuit is shorted then the test button on the front panel should fire (or the one on the LPS itself). I don't know if this also works for the software button in LW as that is by code not just a hardware circuit.


---
**Don Kleinschnitz Jr.** *March 27, 2017 03:11*

On your C type the enable loop should be the left two pins on the 6 terminal connector.


---
**Bradley Blodgett** *March 27, 2017 15:56*

Thanks for the guidance, and thank you Don for for all your hard work on this project. Here is a picture of my setup. I will check the wiring back to the PS to make sure that I should be able to fire. Honestly, I'm almost ready to hang it up. The stock setup work so well for me for like 6 months, and it's been a time and money pit since it broke down.

![images/36763420671d5d70feeb0719094ae9f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/36763420671d5d70feeb0719094ae9f3.jpeg)


---
**Don Kleinschnitz Jr.** *March 27, 2017 16:47*

**+Bradley Blodgett** can you also post the LPS end of the wiring. Btw I forgot the background. Is a stock machine that broke?


---
**Bradley Blodgett** *March 27, 2017 21:06*

Thanks **+Don Kleinschnitz**​. This was stock and since the stock board went out I am replacing with a smoothie.

![images/e41d67ee04a247801d62a9cf07edb538.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e41d67ee04a247801d62a9cf07edb538.jpeg)


---
**Don Kleinschnitz Jr.** *March 27, 2017 21:40*

**+Bradley Blodgett** ok, sorry for all the questions that will follow. Don't hang it up yet ....I'm not :). 



Below when I refer to "Test Sw"  insure that the "Laser Sw" is on. Terminals are numbered right to left. Connectors are right/middle/left going right to left.



1.)You have added a smoothie and the laser stopped firing with the test button since that time, is that right? 

2.) It worked before you added the smoothie, is that right?

3.) Does the test button on the LPS  fire the laser with "Laser Sw" on?

4.) Is the green PWR light on the LPS illuminated?

5.) Where do the 2 black wires on the rightmost connector (terminal 4) connect to?

6.) Where does the white wire on the rightmost connector (terminal 3) connect to?

7.) Where does the black wire on the middle connector (terminal 1) connect to?

6.) Pull off the L wire (green) on the first terminal of the rightmost connector and try the "Test Sw", does the laser fire?


---
**Bradley Blodgett** *March 27, 2017 22:32*

Hi **+Don Kleinschnitz** thanks for the response! 

If I take a couple steps back I can paint a complete picture. The stock system stopped working last fall. Movement but no laser fire. (No reading on the meter on the control panel) I tried replacing the LPS and the tube with the same results. I decided to replace the last component left which was the controller, and it just made sense to replace it with something more functional like the Smoothieboard. 

Fast-forward some months later and I finally got the gumption to assemble the middleman and get everything put back together. I think that answers questions 1&2. Moving on to the rest:

3.) Does the test button on the LPS  fire the laser with "Laser Sw" on?

      No, just tried that, no activity

4.) Is the green PWR light on the LPS illuminated?

      Yes

5.) Where do the 2 black wires on the rightmost connector (terminal 4) connect to?

      One goes to the main gound on the smoothie, and the other to the ground for the pwm (2.4) on the smoothie



6.) Where does the white wire on the rightmost connector (terminal 3) connect to?

   Connected to main + on the smoothie



7.) Where does the black wire on the middle connector (terminal 1) connect to?

   To the Neg on the pot



6.) Pull off the L wire (green) on the first terminal of the rightmost connector and try the "Test Sw", does the laser fire?

  Removed the green wire from LPS and no laser fire on test from the LPS.



Other notes, I am supplying 5v to the smoothie from a secondary atx power supply. Also I have another LPS that I have not tried any of these tests with that is an option if these results don't add up.

Thanks again for the help!




---
**Don Kleinschnitz Jr.** *March 28, 2017 02:03*

**+Bradley Blodgett**



Ok lets start here:



8). Your power supply should be this configuration:

[i.ebayimg.com](http://i.ebayimg.com/images/g/YY4AAOSwEzxYVyIH/s-l1600.jpg)



... is this right?



5.) Where do the 2 black wires on the rightmost connector (terminal 4) connect to?

      One goes to the main gound on the smoothie, and the other to the ground for the pwm (2.4) on the smoothie



<b>******************</b>

OOPS, That wire is 24VDC??? If you ground it the LPS will likely not work and may have blown the 24VDC output. 

<b>******************</b>

6.) Where does the white wire on the rightmost connector (terminal 3) connect to?

   Connected to main + on the smoothie



<b>*****************</b>

OOPS, that's ground,  your smoothie shouldn't have any power?



I thought you were supplying 5VDC to the smoothie from a separate supply. 

The smoothie needs either: 

24vdc AND 5vdc 

OR

24vdc and an onboard regulator



Look at "Logic Power" under this link:

[http://smoothieware.org/laser-cutter-guide](http://smoothieware.org/laser-cutter-guide)

<b>****************</b>

7.) Where does the black wire on the middle connector (terminal 1) connect to?

   To the Neg on the pot



That looks OK ....

...............................



NEXT STEPS: 



Disconnect all DC power from the smoothie we will deal with that later. 



OK. lets pull ALL the wires off the rightmost LPS connector and check it.



Starting from the right set your DVM to DC volts and measure each pin identified below with the black lead on ground.



Turn on the power ....



8.) check the second terminal for 5vdc when measured to grnd. Is it?

9.) check the 3rd terminal for 0vdc when measured to ground. Is it?

10.) check the 4th terminal for 24vdc when measured to ground. Is it?





After these tests we will assess whats next.


---
**Bradley Blodgett** *March 28, 2017 02:47*

Here's the 24v

![images/a8b57bb7a061f6807ca52f33ca869818.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a8b57bb7a061f6807ca52f33ca869818.jpeg)


---
**Bradley Blodgett** *March 28, 2017 02:47*

The 5v to ground 

![images/909889202fd77fe7345d227fe0d83560.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/909889202fd77fe7345d227fe0d83560.jpeg)


---
**Bradley Blodgett** *March 28, 2017 02:49*

And finally, the L to ground 

![images/c449e9dda51358834d10e91e0569960f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c449e9dda51358834d10e91e0569960f.jpeg)


---
**Don Kleinschnitz Jr.** *March 28, 2017 04:26*

10.) So the 24vdc was connected to ground right?



Leave all the wires off that connector and see if

11.) the Test Switch fires the laser with the Laser Switch on?

12.) the test switch on the supply fires the laser 


---
**Bradley Blodgett** *March 28, 2017 14:13*

Yes, that's correct. I tried the test fire button and the test button on the LPS with no result. I also checked the pot voltage while I was at it, and I get 5v from pos to neg and I get the expected variable output from neg to the yellow (i think) output wire from the pot.




---
**Don Kleinschnitz Jr.** *March 28, 2017 15:21*

**+Bradley Blodgett** 



Its helpful if you answer with reference to the question # so I don't get confused. 



I have found that when doing this remote I have to be clear as we cross each step, so sorry for lots of obvious and redundant questions :(. 



Lets summarize where we are:

... the 24vdc was shorted to ground 

... the 24vdc and 5vdc are still ok

... the laser still does not fire with the local button or the test switch while the laser switch is engaged.

13.) are all the above correct?

14.) did you rewire the LPS?



Now we need to measure the center connector. Don't change any wiring and  ......



Starting from the right terminal on the center connector and measuring vdc to ground with your DVM...



15a.) terminal 1:  does this measure 5vdc?

15b.) is this connected to one side of the pot?

16a.) terminal 2:  does this measure some voltage between 0-5vdc?

16b.) does this wire connect to the center terminal of the pot?

16c.) does the voltage vary with the pots rotation?

17a.) terminal 3: is this terminal read 0vdc

17b.) does this connect to the other side of the pot?

18a.) terminal 4. this terminal should be empty where does the yellow wire go to?

19a.) terminal 5: with the "Laser Switch" engaged what does this read?

20a.) terminal 6: what does this read?




---
**Don Kleinschnitz Jr.** *March 28, 2017 20:42*

**+Bradley Blodgett** I answered my own question for:

18a.) ... it should go the the Test switch?


---
**Bradley Blodgett** *March 28, 2017 23:50*

Hi **+Don Kleinschnitz**​, not giving up, but I got called out of town for my day job. I'll be picking this up Wednesday night/Thursday morning. I really appreciate all of your help on this!


---
**Don Kleinschnitz Jr.** *March 29, 2017 01:02*

**+Bradley Blodgett** np gotta make the bacon, connect when u return ...


---
**Bradley Blodgett** *March 30, 2017 21:13*

Ok picking this back up where we left off. I'll start posting answers inline as to not get off track:) 

+Bradley Blodgett 



Its helpful if you answer with reference to the question # so I don't get confused. 



I have found that when doing this remote I have to be clear as we cross each step, so sorry for lots of obvious and redundant questions :(. 



Lets summarize where we are:

... the 24vdc was shorted to ground 

... the 24vdc and 5vdc are still ok

... the laser still does not fire with the local button or the test switch while the laser switch is engaged.

13.) are all the above correct? This is correct

14.) did you rewire the LPS? Definitely not, I wouldn't try something that has a high chance of killing me 🤣



Now we need to measure the center connector. Don't change any wiring and  ......



Starting from the right terminal on the center connector and measuring vdc to ground with your DVM...



15a.) terminal 1:  does this measure 5vdc? Yes

15b.) is this connected to one side of the pot?yes

16a.) terminal 2:  does this measure some voltage between 0-5vdc? Yes

16b.) does this wire connect to the center terminal of the pot?Yes

16c.) does the voltage vary with the pots rotation? Yes all the way from 5 down to 0

17a.) terminal 3: is this terminal read 0vdc yes

17b.) does this connect to the other side of the pot? Yes

18a.) terminal 4. this terminal should be empty where does the yellow wire go to? It connects to the test fire button 

19a.) terminal 5: with the "Laser Switch" engaged what does this read? .015 v

20a.) terminal 6: what does this read?0v

I'll post some pictures inline as well


---
**Don Kleinschnitz Jr.** *March 30, 2017 21:55*

**+Bradley Blodgett** uggh....

The only thing that looks a bit weird to me is that terminal 6 should connect to terminal 5 through the Laser Switch and there is a .015v difference. Perhaps the switch has a bit of resistance which is ok. Just verify that terminal 5 and terminal are connected to either side of the Laser Switch respectively.



This is looking like a bad LPS :). I expected it to be OK since the 24V and 5V are OK but the supply also has an internal 12V that runs the output section that could have been damaged with the shorted 24VDC. 

.................

There is one more thing we can try to make sure we haven't missed anything. This will entail removing some of the LPS wires but you will have to do that anyway if the supply is bad. This test eliminates any bad switches, weird wiring etc.



WITH THE POWER OFF :)...



On the LPS middle connector:

21.) Disconnect terminal 4-6. 

22.) Short out terminal 5-6 by inserting a shorting wire in the screw terminals. Caution: this will make the laser HOT but not on when it comes up.

23.) Check with a DVM (on ohms) if terminal 4 on the middle connector is connected to terminal 1 on the rightmost connector?



24.) TURN ON POWER verify the LPS led is lit

25.) Connect terminal 4 on the middle connector to ground (terminal 3) and see if the laser fires.



If this does NOT fire the laser I expect the LPS is bad :) and we can talk about next steps.






---
**Bradley Blodgett** *March 31, 2017 01:07*

OK, I did the steps 21-23 I read .1 OHMS with my DVM set at 200. 

Shorted out 5-6 and connected to try and fire laser (terminal 4-3)with no result. I have this other power supply, which is the original LPS now that I look at it. I assume that we would run the same battery of tests to verify its operation? 

![images/e07a9db279fbd83f0cc62e12a0c9774d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e07a9db279fbd83f0cc62e12a0c9774d.jpeg)


---
**Don Kleinschnitz Jr.** *March 31, 2017 11:26*

**+Bradley Blodgett** excellent I did not realize you have two of them.

Attached it a test procedure. Decided to document to make it easier than reading text :).



Essentially your are doing the last test again on the other supply. Why do you have two of these?



![images/883e16417961b85cbf8110b7dab3c0b5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/883e16417961b85cbf8110b7dab3c0b5.jpeg)


---
**Bradley Blodgett** *March 31, 2017 13:07*

**+Don Kleinschnitz** basically when the stock setup quit working I got an LPS and a laser tube. Having these on hand is still less expensive than a higher-end laser unit. I'll be working through this today to see where I get. Thanks for the help!




---
**Bradley Blodgett** *March 31, 2017 21:07*

**+Don Kleinschnitz**​ so is it possible that I have 2 bad LPS, or should I swap my tube? Also, I traced my ground wire from the end of my tube and it terminates at the volt meter on my front control panel. Would that impact the ground of the tube, or am I misunderstanding  what that connection does?

![images/00ac4da8bbe562c9a780ecf73a4bb02c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00ac4da8bbe562c9a780ecf73a4bb02c.jpeg)


---
**Don Kleinschnitz Jr.** *March 31, 2017 22:28*

**+Bradley Blodgett** Are you saying the other supply flunked the test??



The wire (green) should as you suggest come from the  cathode (like the picture) to one side (+) of the meter and the other side of the meter is wired to l- on P3.



Are either of these supplies new i.e. never been connected?



Did they both have the 24VDC grounded?



Perhaps the tube is bad but freaky that it would do nothing?


---
**Bradley Blodgett** *April 01, 2017 20:03*

They both do get 24v to ground. The first one we worked with was new. This last one was the original one used for about 6 months 


---
**Bradley Blodgett** *April 14, 2017 01:22*

**+Don Kleinschnitz**, what are the odds that I have two bad tubes and/or two bad LPS? I finally got a spare minute so I changed tubes to the original one that came with the unit, with the same results as before. Green light on the LPS, no result pushing the test button. I can't really believe that all of these components are bad. I register nothing on the meter on the front panel for any sort of output. Is it time to send it to the scrap heap? I could have bought another unit almost at this point trying to repair the original.




---
**Don Kleinschnitz Jr.** *April 15, 2017 13:04*

**+Bradley Blodgett** I certainly get your frustration. The problem is likely your PS(s) but I am shocked (pun?) that you have two bad supplies. 



Although I do not know what shorting the 24VDC will do to the supply internally. The fact that they still read 24VDC and 5V leads me to believe that they are OK.  



I guess we need to verify somehow that the supplies really are bad?



Reading the thread above did you run the tests on the second supply?



Bottom line is that neither supply will fire the laser with power applied to the LPS and pushing the test button on the LPS, right? 




---
*Imported from [Google+](https://plus.google.com/+BradleyBlodgett/posts/4Ecka9h7jX7) &mdash; content and formatting may not be reliable*
