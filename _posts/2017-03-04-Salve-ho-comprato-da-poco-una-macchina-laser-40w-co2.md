---
layout: post
title: "Salve, ho comprato da poco una macchina laser 40w co2 ."
date: March 04, 2017 21:17
category: "Software"
author: "max mess"
---
Salve, ho comprato da poco una macchina laser 40w co2 .

ho provato a tagliare  una immagine su  compensato di 4mm,  ripetendo il taglio più di 5 volte.  Ho usato il programma corellaser e laserdraw ma il taglio non è riuscito.

Qualcuno potrebbe aiutarmi  a capire come procedere affinchè il taglio si realizzi?

Ho osservato i vostri lavori di taglio su legno, sono bellissimi e mi fate venire l'invidia: anch'io vorrei riuscirci!!!

Grazie

Mx





**"max mess"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 05, 2017 01:52*

When you say "the cut failed", are you just meaning that it could not cut all the way through the wood? Or something else?



If it's that it couldn't cut all the way through, you probably need to work on the 1) the alignment of the 3 mirrors, 2) lens orientation (curved side facing up).



Aligning the mirrors can be a tricky process, but ideally you want the laser to enter the 3rd mirror/lens at the exact same spot in all 4 corners of the machine cutting area (top-left, top-right, bottom-left, bottom-right). There are a few guides for this around. I used the floatingwombat guide when I learned to align mine ([http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)) but it seems that link is dead now.


---
**max mess** *March 05, 2017 09:02*

il mio problema è che non taglia in profondità il legno.. le lenti sono allineate perfettamente. infatti l' engraving funziona bene.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 05, 2017 12:24*

**+max mess** If it is cutting too deep then you need to either 1) reduce the power or 2) increase the engrave speed. When I was using CorelLaser I would use 350-500mm/s for engraving speed at around 10mA power (~35% power).


---
*Imported from [Google+](https://plus.google.com/110382244465280432347/posts/8VQ973wZkGf) &mdash; content and formatting may not be reliable*
