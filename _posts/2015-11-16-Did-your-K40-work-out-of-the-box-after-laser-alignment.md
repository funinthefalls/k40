---
layout: post
title: "Did your K40 'work' out of the box after laser alignment?"
date: November 16, 2015 21:20
category: "Discussion"
author: "Joseph Coco"
---
Did your K40 'work' out of the box after laser alignment? If not, where did you purchase it from and does it matter?





**"Joseph Coco"**

---
---
**Anthony Coafield** *November 17, 2015 04:12*

Mine worked for a few hours but I'm pretty sure the laser died. Laser manufacture date is Sep 2013 and apparently they should be used or they die so thinking that may be it. Bought off eBay seller in Australia who has been great so far...


---
**Gary McKinnon** *November 18, 2015 14:48*

Mine worked out of the box without having to re-align. It came with the M2 Nano board, CorelDraw, LaserDraw and Winseal (for vector graphics). Will definitely be modding it, already ordered air-assist plus laser-dot, will be chopping that extractor vent intake to get more bed room.


---
**Jim Coogan** *November 19, 2015 00:21*

Mine worked out of the box.  No alignment needed but I did tweak it a little anyway.


---
*Imported from [Google+](https://plus.google.com/+JosephCoco/posts/bcatC8GrMz1) &mdash; content and formatting may not be reliable*
