---
layout: post
title: "What's the best/safest method for firing K40 with PWM control using grbl?"
date: April 09, 2018 05:01
category: "Modification"
author: "Tom Traband"
---
What's the best/safest method for firing K40 with PWM control using grbl?



I've read a couple of different methods for managing the "0v is max power" issue wih the K40 laser power supplies and am looking for some guidance.



Some folks say you can mod the firmware, but the laser will still fire (if enable button is on) at 100% of the power allowed by the potentiometer when the arduino is booting up. Doesn't sound like the safest method to me. Although the danger would be minimized by always booting the arduino (plugging in to laptop/pc) before turning on the main laser power switch, and always disabling the laser enable button when not actively running a job, I know myself well enough that sooner or later I'll do something in the wrong order or forget to unpunch the enable button and something I don't want zapped will get zapped.



The other method I've seen is to build a little transistor logic inverting circuit. The specific edition I was looking at tonight at [http://www.foxrobotics.com/2018/03/19/upgrading-a-k40-laser-cutter-part-1-control-board-upgrade/](http://www.foxrobotics.com/2018/03/19/upgrading-a-k40-laser-cutter-part-1-control-board-upgrade/) specifically mentions a 2N222A BJT transistor which I don't have on hand. I do have a 2N3904. Comparing the data sheets, the only substantive difference i see is that the 2N222A can handle 600 mA, where the 2N3904 is limited to 200. Is this comperable to the value I'm seeing on the ammeter? If so, I'd think the 2N3904 is more than adequate for the task, but I don't want to risk a failure mode where the laser would fire at full power unexpectedly.



I did look through Don's blog, but I'm not using a smoothie board, so I'm not sure where any differences might lie.



Thanks again for everyones' time and support.







**"Tom Traband"**

---
---
**Don Kleinschnitz Jr.** *April 09, 2018 12:09*

To insure that I understand the problem. 

Are you saying that if you leave the Enable on, at power up the laser fires because the PWM [connected to L on the LPS]  is off and sitting at 0v? 



I am not running a grbl arduino configuration but looking at the foxrobotics link above apparently the signal needs inverting. In the smoothie we can do that in firmware. 





<i>Is this comperable to the value I'm seeing on the ammeter?</i>

The 2N3904 (NPN)should work fine. The current is not high as the L pin on the LPS is pulled up with a 1K to +5V through the input optocouplers LED. 

BTW Having this transistor buffer is a good idea anyway as I do not recommend running processor pins directly off board to control things like the LPS.



The L drive current on the LPS is not the same as the laser current you are seeing on the K40 meter, however it turns that current on/off under program control.






---
**Tom Traband** *April 09, 2018 12:16*

Yes, the laser is firing when PWM is off. I've been concentrating on motion problems lately and just leaving the laser enable off. I've  also got a switch in series in the main door so the laser is disabled when that door is open.


---
**Don Kleinschnitz Jr.** *April 09, 2018 12:44*

**+Tom Traband** I would definitely add the buffer invert-er ....


---
**HalfNormal** *April 09, 2018 15:37*

I agree with Don on all counts. Safety is priority. The transistor buffer will ensure this and the current draw is minimal. 


---
**Tom Traband** *April 10, 2018 04:17*

Built the circuit and put it in tonight. It's ugly on some perf board but it works.

Thanks for the assistance.


---
**Don Kleinschnitz Jr.** *April 10, 2018 12:12*

**+Tom Traband** ugly perf board is the new "pretty" for makers.


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/1HaNP5egvZd) &mdash; content and formatting may not be reliable*
