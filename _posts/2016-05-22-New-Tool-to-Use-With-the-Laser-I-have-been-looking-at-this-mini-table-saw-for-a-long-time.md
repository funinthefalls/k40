---
layout: post
title: "New Tool to Use With the Laser I have been looking at this mini table saw for a long time"
date: May 22, 2016 16:58
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
New Tool to Use With the Laser



I have been looking at this mini table saw for a long time. It has finally come down in price and the store had a 20% off coupon and a free DVM with any purchase so....

I bought one and also bought the carbide tip blade for it.

Total price was $37.00 USD. It works fine for what it is. Cut 6mm plexiglass without chipping. Leaves a smooth edge on 3 mm ply. It does not have a lot of torque but if you take your time it does a great job. A lot less hassle than my battery circular saw and can cut small items quickly.

It looks like plastic in the picture but the whole thing is made from cast metal.

[http://www.harborfreight.com/4-in-mighty-mite-table-saw-with-blade-61608.html](http://www.harborfreight.com/4-in-mighty-mite-table-saw-with-blade-61608.html)





**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Mkv9gZ7E5Du) &mdash; content and formatting may not be reliable*
