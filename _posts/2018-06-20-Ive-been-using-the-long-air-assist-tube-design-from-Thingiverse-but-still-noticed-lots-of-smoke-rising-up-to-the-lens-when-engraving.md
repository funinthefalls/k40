---
layout: post
title: "I've been using the long air assist tube design from Thingiverse( ) but still noticed lots of smoke rising up to the lens when engraving"
date: June 20, 2018 16:14
category: "Air Assist"
author: "Doug LaRue"
---
I've been using the long air assist tube design from Thingiverse([https://www.thingiverse.com/thing:2421971](https://www.thingiverse.com/thing:2421971) ) but still noticed lots of smoke rising up to the lens when engraving. The lens path based laser pointer helps show it too. So I mashed up 2 parts, that  nozzle with a cone shaped lens based nozzle( air_nozzle_V1.scad, IIRC obtained from Scorch of K40 Whisperer fame ) and voila.  I have a .2mm diameter hole feeding the air in the lens nozzle since all I want that for is to keep smoke off the lens.  Will post to Thingiverse once I get permission to use the OpenSCAD design for the lens nozzle.

![images/8d6e0a101d2034a4b7962485766ce25b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d6e0a101d2034a4b7962485766ce25b.jpeg)



**"Doug LaRue"**

---
---
**Doug LaRue** *June 20, 2018 17:32*

now published on Thingiverse - [thingiverse.com - Merged Air Nozzle V1 for K40 by DOugL](https://www.thingiverse.com/thing:2970371)


---
**Stephane Buisson** *June 21, 2018 12:07*

take care, plastic is flamable. you should at least wrap it in foil  to reduce risk.


---
**Doug LaRue** *June 21, 2018 14:32*

Indeed, I've been thinking about electro-plating it.




---
**Doug LaRue** *June 21, 2018 18:17*

Shapeways does stainless steel 3D printing so that might be the way to go.


---
**Mark Brown** *June 21, 2018 20:54*

Does your laser pointer use one of those mirrors that reflects visible, but lets the co2 laser through?


---
**Doug LaRue** *June 21, 2018 21:45*

**+Mark Brown** yes, that was added recently after I designed a mount. Also list on Thingiverse - [thingiverse.com - K40 laser cutter Combiner Mount for red dot laser by DOugL](https://www.thingiverse.com/thing:2915288)


---
**Kyle Kerr** *June 27, 2018 06:19*

Slightly off topic. Have you seen Sabarmultimedia on YouTube? The guy really seems to know his stuff. You might find some useful laser info on his channel. Fair warning. He can be long winded. 40+ minute videos are not uncommon.


---
*Imported from [Google+](https://plus.google.com/103281768421101175277/posts/82tp7eHJDBK) &mdash; content and formatting may not be reliable*
