---
layout: post
title: "Where can I find the plug in for Inkscape to make laser files"
date: January 28, 2016 12:00
category: "Software"
author: "3D Laser"
---
Where can I find the plug in for Inkscape to make laser files





**"3D Laser"**

---
---
**Stephane Buisson** *January 28, 2016 16:59*

Well, maybe your question isn't well formulated !

it's depending what is your hardware situation and software you use.

in my video  I used a plugin which came with Visicut, (bridge inkscape->visicut).


{% include youtubePlayer.html id="lbTTPkDEhOg" %}
[https://www.youtube.com/watch?v=lbTTPkDEhOg](https://www.youtube.com/watch?v=lbTTPkDEhOg)


---
**Tony Sobczak** *January 28, 2016 18:01*

Thanks 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/gxWyJ6LyMnT) &mdash; content and formatting may not be reliable*
