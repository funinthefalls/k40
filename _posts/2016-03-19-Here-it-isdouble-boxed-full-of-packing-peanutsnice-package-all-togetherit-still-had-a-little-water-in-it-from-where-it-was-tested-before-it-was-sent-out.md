---
layout: post
title: "Here it is....double boxed full of packing peanuts...nice package all together....it still had a little water in it from where it was tested before it was sent out"
date: March 19, 2016 09:39
category: "Discussion"
author: "Scott Thorne"
---
Here it is....double boxed full of packing peanuts...nice package all together....it still had a little water in it from where it was tested before it was sent out. 

![images/9bf328deb13b97c1dd06d591d5cef33d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9bf328deb13b97c1dd06d591d5cef33d.jpeg)



**"Scott Thorne"**

---
---
**Pedro David** *March 19, 2016 23:50*

Price?


---
**Scott Thorne** *March 21, 2016 12:15*

225.00


---
**Pedro David** *March 21, 2016 12:48*

Ok


---
**Phillip Conroy** *March 25, 2016 22:37*

What did the seller say about it?


---
**Scott Thorne** *March 25, 2016 23:55*

I think the tube is fine...I think the problem is the power supply....I've reordered a milliamp gauge that will be here tuesday...I'll know then **+Phillip Conroy**...I'll keep you posted.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/fSmwf1UoqZH) &mdash; content and formatting may not be reliable*
