---
layout: post
title: "Getting everything set up and tuned in"
date: February 15, 2016 00:30
category: "Discussion"
author: "3D Laser"
---
Getting everything set up and tuned in.  I took the old bed out and I am trying to make sure everything is square which is a pain.  Still haven't cut to much 



![images/4e7c3b58df019fff8972f0d706626ee6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e7c3b58df019fff8972f0d706626ee6.jpeg)



**"3D Laser"**

---
---
**ThantiK** *February 15, 2016 01:20*

I'm noticing that the fan is still at the laser cutter side of things.  Generally, you'll want to buy a replacement inline duct fan, and put it at the <i>other</i> end of the tube.  What happens is that while you have vacuum at the laser cutter side, all that is getting pushed into the positive pressure side (the tube) and any little nook and cranny will release those fumes.  If you put an inline duct fan at the end of the tube, the whole thing from laser cutter all the way to the end is a vacuum, and you'll ensure that you don't get fumes in the work space.


---
**3D Laser** *February 15, 2016 01:22*

Thanks for the advise 


---
**Scott Marshall** *February 15, 2016 04:33*

Nice work Corey!

Super work area!

I bet you find the workbench right next to it handy.



I've got mine jammed into my kitchen right now (but I'm long divorced and my 2 sons are away at college, so it's very "do what you like"). I don't heat my shop since I retired since it costs more than the house, so a lot of stuff graviatates into the house.



My kitchen also currently contains a mini mill, lathe, and a 427 small block chevy alcohol motor (eat your heart out Wolfgang Puck!), oh, yeah, and a 40w CO2 Industrial laser for cutting cool shapes out of potato chips....

(now watch that be the next Big thing)



While out in the garage servicing my compressor (by "servicing" I mean resetting the breakers and swearing at it while it warms up - thick oil, 20 below zero, popping breakers deal) I nooticed a wooden 30 gal aquarium stand I forgot I had. Perfect fit, and there's 2 doors uderneath designed to enclose filter, supplies etc. It's now the "cooling bay"!



Over against the wall is a water damaged piece of MDF with just enough good wood left on it to make a mounting deck. 



I love it when a plan comes together.



I've got to get some pictures up, still havn't put up the ones of my billet carriage. 



I keep putting off taking it apart again to get the mods done, having too much fun making plywood boxes plaques etc.



I just put the lights in and am mounting the regulator to it. - hey, like yours, it's foward progress. I've got "ptototype wiring hanging all over, keep the cats away... Don't want em  to lose a tail...

 The space shuttle started by butting 2 pieces together...



Anyway, I'll be watching how yours comes together, I'll post mine up if you promise not th laugh the techno-junkyard I live in..

It's all about having fun with it. That's why I enjoy it.


---
**3D Laser** *February 15, 2016 12:27*

I was lucky enough that our house came with a full basement.  One half will be finished.  The other half three fourths is laundry and storage leaving me with about 200sf of work space.  I have my work bench shelves two work tables and display cabinets for all of my minis.n I have to rebuild my k40 it got dinged during shipping and as I was planning on taking it apart anyway I opted for 75 dollars off instead of a new machine as it that bad of an issue 


---
**Ashley M. Kirchner [Norym]** *February 15, 2016 18:36*

The other thing to keep in mind is that over time, that entire duct will fill with particles. I have to remove mine every few weeks and hold it vertical over the trashcan to get stuff out ... mainly wood ash from all the engraving/cutting. I'm not sure if moving the fan to the end will help minimize that (It will help with the fumes though, so that's still worth doing.)


---
**Scott Marshall** *February 16, 2016 04:31*

Straight pipe IS better, less restrictive and easier to clean. Wait till you find a sale or pie of it at a garage sale. Unless it bothers you.  Looks laike a reasonable length, so it will probably be fine, it will for sure with a fan upgrade.

From what I've seen, these little machines are VERY forgiving....


---
**Ben Walker** *February 16, 2016 15:57*

Really nice set up.  I am installing the duct fan as soon as I get off work (if UPS has dropped it yet) and the adapters are printing away as I work.  I cut down the exhaust pipe finally and readjusted my mirrors and lens.  Man does that make a difference and now I really need good exhaust because it cuts up a storm (literally).  The new air assist and upgraded lens arrives today and I am still sitting on the fence with the rotary tool.  I have it but I also have the return label printed.  I am so nervous about frying the board in the K40 and am not sure how to install a driver board so I am lost - again.  But I seriously want your work space.  If only my garage was just a bit wider...


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/XUwpPncxhjr) &mdash; content and formatting may not be reliable*
