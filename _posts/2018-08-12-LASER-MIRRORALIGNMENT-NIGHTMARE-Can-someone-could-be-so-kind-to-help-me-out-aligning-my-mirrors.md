---
layout: post
title: "LASER MIRRORALIGNMENT NIGHTMARE! Can someone could be so kind to help me out aligning my mirrors?"
date: August 12, 2018 05:43
category: "Hardware and Laser settings"
author: "Mario Gayoso"
---
LASER MIRRORALIGNMENT NIGHTMARE!



Can someone could be so kind to help me out aligning my mirrors?



Laser beam shoots in the center of the fixed mirror. With this been said, every part has been taken apart for mods and I'm sure that there is were I messed everything .



Mirror one hits in the middle.

Y AXIS mirror doesn't get hit. Laser comes to low below the mirror aluminum ring. 



A few pictures 



![images/b00efd647a3145be39ca8803f0f87604.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b00efd647a3145be39ca8803f0f87604.jpeg)
![images/0d0556e2b167a94bdeb867973f482f81.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d0556e2b167a94bdeb867973f482f81.jpeg)
![images/0a362fe7fcf7be0984415708cd7739ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a362fe7fcf7be0984415708cd7739ed.jpeg)
![images/74217d7141b9c3f6a009ed214218e372.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74217d7141b9c3f6a009ed214218e372.jpeg)
![images/77b7353623fa958ca4b94909b54440fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/77b7353623fa958ca4b94909b54440fb.jpeg)
![images/faeca480587563ee12e44982a00d0b11.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/faeca480587563ee12e44982a00d0b11.jpeg)

**"Mario Gayoso"**

---
---
**HalfNormal** *August 12, 2018 06:17*

Here is a link to several alignment guides



[everythinglasers.com - Great alignment sources - Everything Lasers](https://www.everythinglasers.com/forums/topic/great-alignment-sources/)


---
**Joe Alexander** *August 12, 2018 07:54*

also verify that your laser tube is parallel to your gantry, then go mirror by mirror. **+HalfNormal** linked a great source for alignment just expect it to take an hour or two the first time to dial it in.


---
**Stephane Buisson** *August 12, 2018 08:39*

**+Mario Gayoso**, don't try to catch up and correct. As **+Joe Alexander** well said, it's to be parrallel/perpendicular to tube. so even before your start, check the gantry position. 

the error is just trying to have the beam into the center of the mirrors, you will just increase the error further you are in the path.


---
**Mario Gayoso** *August 14, 2018 21:28*

I bought two clamps to align the laser tube. The original k 40 are low quality and don't allow me to proper align them![images/7094e124004387ee7e07fc148847ecfd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7094e124004387ee7e07fc148847ecfd.jpeg)


---
**Djinn per se** *August 16, 2018 22:07*



looks like the tube is so far from the mirror.. mine are much more closer


---
**Mario Gayoso** *August 20, 2018 22:36*

**+Djinn per se** how many mm? thanks!!


---
*Imported from [Google+](https://plus.google.com/+MarioGayoso/posts/7n8fZSnNjAG) &mdash; content and formatting may not be reliable*
