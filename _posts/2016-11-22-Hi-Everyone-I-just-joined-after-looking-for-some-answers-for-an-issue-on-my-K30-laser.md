---
layout: post
title: "Hi Everyone, I just joined after looking for some answers for an issue on my K30 laser"
date: November 22, 2016 22:25
category: "Original software and hardware issues"
author: "Michael Anderson"
---
Hi Everyone,

I just joined after looking for some answers for an issue on my K30 laser.

Everything has been going nearly perfect over the last year until I tried to cut some gaskets this last week and got this for the output. These are simple circular gaskets that we have cut before but the X-Y controls have gone haywire. Has anyone else experienced this with the Moshi board?

![images/ac3e13abd46502d035a8e2b97a02a3cd.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ac3e13abd46502d035a8e2b97a02a3cd.png)



**"Michael Anderson"**

---
---
**greg greene** *November 22, 2016 22:38*

check your belt tension


---
**Cesar Tolentino** *November 22, 2016 22:43*

Also try to clean your rails.  With mineral oil or something.  


---
**Alex Krause** *November 22, 2016 23:12*

I have seen this from mirrors that that the adjustment screws vibrated loose on ... also have seen this with a loose lens mount. Cutting to fast also creates this


---
**Michael Anderson** *November 22, 2016 23:19*

Thanks for the responses Greg, Cesar.

I cleaned the X and the Y rails and looked at the belt tensions and it looked fine. I did find a burr that i thought might be able to catch on a belt tooth and smoothed it off with a file. It seems to do squares ok but the curves are what fail. 


---
**Michael Anderson** *November 22, 2016 23:59*

We have a winner!



Thank you Alex. I didn't even think about the lens but that's what it was. I'll have to admit that I didn't check because it meant removing the air assist cover.  Won't make that mistake again.


---
**Cesar Tolentino** *November 23, 2016 00:01*

Bingo. We have a solution


---
*Imported from [Google+](https://plus.google.com/106378532374264113664/posts/XnMUbb6oyzA) &mdash; content and formatting may not be reliable*
