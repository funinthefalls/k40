---
layout: post
title: "Which way is the Focus Lens supposed to be installed?"
date: March 16, 2015 15:50
category: "Hardware and Laser settings"
author: "Sean Cherven"
---
Which way is the Focus Lens supposed to be installed?





**"Sean Cherven"**

---
---
**Jim Coogan** *March 16, 2015 16:30*

Reflective side facing up


---
**Stephane Buisson** *March 16, 2015 17:42*

curve up, flat down  ;-))


---
**Sean Cherven** *March 16, 2015 19:00*

Apparently,  the lens was installed backwards at the factory. Go figure... lol


---
**Seth Mott** *March 17, 2015 00:30*

I had that question today as well. Thanks! 


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/ZbMmVw5R9Xk) &mdash; content and formatting may not be reliable*
