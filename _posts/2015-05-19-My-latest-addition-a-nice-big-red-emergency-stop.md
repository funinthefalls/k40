---
layout: post
title: "My latest addition, a nice big red emergency stop!"
date: May 19, 2015 19:05
category: "Modification"
author: "David Wakely"
---
My latest addition, a nice big red emergency stop! It kills the power to the machine and all the accessories like pumps and extractors. Next step is interlocks for cover and flow!

![images/057257a96bbecb676214130c043e6122.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/057257a96bbecb676214130c043e6122.jpeg)



**"David Wakely"**

---
---
**Eric Parker** *May 19, 2015 20:42*

Sweet.  I'm still waiting on mine to arrive.


---
**David Wakely** *May 19, 2015 22:11*

I hate waiting for shipping! Hope you enjoy it when it arrives!


---
**Eric Parker** *May 20, 2015 01:27*

Cutter is here, button is not


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/QapcbnTSr5U) &mdash; content and formatting may not be reliable*
