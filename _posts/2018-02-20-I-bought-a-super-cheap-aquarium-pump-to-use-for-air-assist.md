---
layout: post
title: "I bought a super cheap aquarium pump to use for air assist"
date: February 20, 2018 00:28
category: "Hardware and Laser settings"
author: "James Rivera"
---
I bought a super cheap aquarium pump to use for air assist. After feeling the flow coming out of the hose, I think I could push more air by waving my hand (read: I wasted some money). I've seen mention of a more industrial air pump (costs about $69 on Amazon) and I've also seen people using tire inflation compressors. I already have one of the latter from Harbor Freight and it is way too loud for my taste. I'm wondering if I can get away with mounting a small blower fan (e.g. squirrel cage fan for part cooling on a 3D printer) on the cutting head bracket? Is this crazy talk? 





**"James Rivera"**

---
---
**David Davidson** *February 20, 2018 00:59*

I got this one. Works fine. Now I'm trying to figure out a good fume extractor. 



[amazon.com - Robot Check](https://www.amazon.com/gp/product/B06XDKRYC6/ref=oh_aui_detailpage_o07_s00?ie=UTF8&psc=1)






---
**Ned Hill** *February 20, 2018 01:00*

I've seen some people  use small fans like on a 3D printer and swear by it.  I haven't tried it personally but it does seem to be a viable option.


---
**Ned Hill** *February 20, 2018 01:03*

If you are using an air assist head like the Light Object one I've found that you need at least 10 L/min flow rate to be effective.


---
**Don Kleinschnitz Jr.** *February 20, 2018 02:16*

I think **+HalfNormal** uses a fan. I am happy with my compressor see my blog. 


---
**HalfNormal** *February 20, 2018 02:23*

**+Don Kleinschnitz** I use a BIG pump! 


---
**Don Kleinschnitz Jr.** *February 20, 2018 03:35*

Ha, somebody out here uses a fan on the gantry? 


---
**Ned Hill** *February 20, 2018 04:59*

**+Don Kleinschnitz**  Finally found one of the old posts.  Looks like **+Paul de Groot** uses a fan. [plus.google.com - I'm a complete noob to laser cutters and have a random question about Air Ass...](https://plus.google.com/u/0/106809986839735143620/posts/GtSxtvuFeih)


---
**James Rivera** *February 20, 2018 07:25*

Ha! It looks like he just screwed a radial fan on using the mirror hold down screw. If it works, that's great. The next comment after his uses a radial fan on a more permanent mount. However, in both cases the smoke could get up into the head and leave deposits on the lens and/or mirror. My idea for a blower fan  would have the same drawbacks. When my new head/lens arrives, I think I will try using the wimpy pump I have now first (it will push smoke away from sensitive part$), and then go from there based on the performance. The pump I have is super quiet. I might try keeping it <i>and</i> using the blower fan mounted on the head bracket.


---
**BEN 3D** *February 20, 2018 07:56*

I think you could not use a fan, because you need a way to get fresh air from outside into it. I use a 50€ paintgun compressor that i bought on amazon.


---
**James Rivera** *February 20, 2018 18:07*

**+BEN 3D** How loud is it?


---
**BEN 3D** *February 20, 2018 18:14*

It is louder then the waterpump, but not louder then my dust fan. And the complete set is louder then my 3D Printer. It is may so loudly as my clothes washing mashine.


---
**BEN 3D** *February 21, 2018 06:25*

This is my one![images/5a153fd904eadc036f9e310c8ceb5780.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a153fd904eadc036f9e310c8ceb5780.jpeg)


---
**Don Kleinschnitz Jr.** *February 21, 2018 13:34*

I had a air brush compressor but it was loud and did not put out as much air as this.

[amazon.com - Amazon.com : EcoPlus 1300 GPH (4920 LPH, 80W) Commercial Air Pump w/ 8 Valves &#x7c; Aquarium, Fish Tank, Fountain, Pond, Hydroponics : Aquarium Air Pumps : Garden & Outdoor](https://www.amazon.com/gp/product/B002JLGJVM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)



Very quiet.... and this flowmeter is a nice addition:



[https://www.amazon.com/gp/product/B01CKR39XA/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01CKR39XA/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)




---
**Tom Traband** *March 18, 2018 17:30*

I'm just starting out so I got an aquarium pump as well. To concentrate the air stream I screwed a 0.035" mig contact welding tip into the aquarium tubing. Haven't tried it on the machine yet (still sorting out the exhaust) but it definitely creates an aimable jet of air. 


---
**James Rivera** *March 18, 2018 21:24*

Well, it turns out the super cheap aquarium pump kind of works! I got my bed figured out and tried my first serious cuts today on 1/8” plywood. It all worked perfectly!


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/J3uLcFwtWu9) &mdash; content and formatting may not be reliable*
