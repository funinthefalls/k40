---
layout: post
title: "Hello, First of all: as a manual for the k40 is.."
date: June 05, 2016 17:20
category: "Discussion"
author: "Christoph E."
---
Hello,



First of all: as a manual for the k40 is.. well just not available I'm thankful that this group does exist. :-)



My K40 just arrived and I was wondering if I can make things like these (laser engraved acrylic plant marker, acrylic businesscard).

I've tried a lot of settings and have exactly the same material but it doesn't even get close to that.



Any Help is Appreciated. :-)



![images/9f24eca82fd6522d69165be457dd8e2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9f24eca82fd6522d69165be457dd8e2b.jpeg)
![images/2f3ae6e83634d08048d316b5f6335aff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f3ae6e83634d08048d316b5f6335aff.jpeg)

**"Christoph E."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 20:52*

I have yet to work with acrylic, but it would be nice to see results of your previous tests to aid in determining how far off you are from the desired results & see if we can all help in getting to that point.


---
**Christoph E.** *June 05, 2016 21:46*

Thanks for your answer. Will upload some pics tomorrow. Also got cards that company made, will upload a pic too. 


---
**Ashley M. Kirchner [Norym]** *June 05, 2016 23:20*

Make sure you're using cast acrylic, not extruded. 


---
**Christoph E.** *June 06, 2016 09:02*

Here are the pictures. :-)

 

The job they did on the cards is amazing, the first one is an original card, the marks on the second one is the best results i could achieve so far (at around 350-420 mm/s playing with the power), last one is just a cutting test (2/3 power 7 mm/s). 



It also doesn't seem to be misaligned as the power doesnt change all over the bed. If I run it really fast at almost 0 power (sorry, lack of a scale -.-) the result is almost invisible because it is clear then. 



Like I said, not a matter of material and not even close..



[http://imgur.com/a/eB5Bu](http://imgur.com/a/eB5Bu)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 06, 2016 18:24*

**+Chris E** Can't quite tell with the third picture, but is the cut suitable? It looks it to me although I am not sure.



In the second photo, with what looks to be circles that you have tested, I see what you mean about the difference compared to the original manufacturer. I do see one of your circles that looks to be very similar to the original (on the left of the 3 in the centre).



I would suggest take that particular one's settings as a starting point. Use the same power you used for it & do a variety of tests at different speeds (in steps of say 50mm/s, so 400/350/300/250/etc). Usually I perform a series of these tests on an offcut of all materials I will be working with, to determine best settings/effects. Normally about 30 mins worth of testing for each new material I work with. Retain the piece you tested on (& write the settings) for future reference.



So I create a 10mm square, test at 4mA, 6mA, 8mA, 10mA & at speeds stepping from 100-500mm/s. Also, for engraving you can set the Pixel Steps option. 1 pixel step = 1000dpi image, 2 = 500dpi, 3 = 333dpi, 4 = 250dpi, 5 = 200dpi. The setting you use here will affect the overall "darkness" of image normally, but with acrylic I am unsure as to what it would do. Maybe the overall cloudyness?


---
**Christoph E.** *June 07, 2016 14:03*

**+Yuusuf Sallahuddin**

Yep, the cut is okay, except that the edges aren't exactly 90° but for now I'm good with that. :-)



I will go on testing if i receive new material, already burnt like 10 of my cards and these were just too expensive to destroy all of them. :D


---
*Imported from [Google+](https://plus.google.com/100193166302371572888/posts/MoFKLrJoE6P) &mdash; content and formatting may not be reliable*
