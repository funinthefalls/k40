---
layout: post
title: "Paul de Groot , Nate Caine Guys, I had the low voltage power go bad on a LPS I am repairing so I took a shot at verifying the 24/12/5V power on our schematic"
date: June 11, 2017 19:27
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
**+Paul de Groot**, **+Nate Caine**



Guys, I had the low voltage power go bad on a LPS  I am repairing so I took a shot at verifying the 24/12/5V power on our schematic. 



Notes:

The LV transformer is electrically correct but not sure about its physical /magnetic configuration :(.



I was driven insane until I figured out that  the 7812 and TL431 parts are SOT-89 whose pin-outs are literally backward from standard parts.



.....

The new section + some updates are on the shared schematic.



[https://www.digikey.com/schemeit/project/k40-lps-7S9UFV0201F0/](https://www.digikey.com/schemeit/project/k40-lps-7S9UFV0201F0/)









**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/iuqjxcxM9yS) &mdash; content and formatting may not be reliable*
