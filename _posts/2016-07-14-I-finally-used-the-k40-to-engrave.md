---
layout: post
title: "I finally used the k40 to engrave!"
date: July 14, 2016 03:24
category: "Discussion"
author: "3D Laser"
---
I finally used the k40 to engrave!  It turned out awesome.  Though it took longer than expected 

![images/fb8a3deb4d701ecccec395db3452b8fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb8a3deb4d701ecccec395db3452b8fd.jpeg)



**"3D Laser"**

---
---
**Anthony Bolgar** *July 14, 2016 04:25*

Looks really good.


---
**Pippins McGee** *July 14, 2016 04:45*

nice work


---
**Phillip Conroy** *July 14, 2016 08:45*

With beam width of 0.01 at the focal length of 50.05mm the bean needs lots of passes to cover any area,that is why i surgested using a unfocased beam a while ago ,you could try work at 48mm ans a step of 10


---
**3D Laser** *July 14, 2016 10:53*

**+Phillip Conroy** can you explain the "step" to me?


---
**Phillip Conroy** *July 14, 2016 10:58*

The iddea i have is to make the laser beam 10 time thicker than normal =1mm wide by lifting the work peace up a few mm ,step 10 would than make the laser head jump 9 steps [back and forward moves]


---
**Phillip Conroy** *July 14, 2016 10:59*

It would loose some detail however finish 10 times faster


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/DBqRG1A8QiV) &mdash; content and formatting may not be reliable*
