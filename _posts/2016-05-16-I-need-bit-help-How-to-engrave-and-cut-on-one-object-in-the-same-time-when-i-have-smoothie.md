---
layout: post
title: "I need bit help How to engrave and cut on one object in the same time when i have smoothie?"
date: May 16, 2016 09:14
category: "Smoothieboard Modification"
author: "Damian Trejtowicz"
---
I need bit help

How to engrave and cut on one object in the same time when i have smoothie?

Also how to cut internal holes before external contour?

On ramps i was using layers,how about now?





**"Damian Trejtowicz"**

---
---
**Damian Trejtowicz** *May 16, 2016 09:56*

i already sort it out this, i find it this in visicut :)


---
**Vince Lee** *May 16, 2016 12:07*

I like visicut a lot but when engraving sadly it doesn't seem to have an option to add padding around an image to let the head accellerate to full speed, so edges engrave deeper than the rest of the image.  I still switch to moshidraw when engraving because of this.


---
**Vince Lee** *May 16, 2016 14:31*

**+Peter van der Walt** I am on smoothie.  Perhaps it could be improved with a pwm calibration feature, though an option to overshoot would still be nice.


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/QEEDM5SXkiM) &mdash; content and formatting may not be reliable*
