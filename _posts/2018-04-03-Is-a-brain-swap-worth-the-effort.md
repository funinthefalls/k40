---
layout: post
title: "Is a brain swap worth the effort?"
date: April 03, 2018 05:57
category: "Modification"
author: "Tom Traband"
---
Is a brain swap worth the effort?



I've had good luck with K40 whisperer on some simple projects. I've got an Uno and a CNC shield and am considering flashing grbl, swapping the control board out and trying LaserWeb. I'm wondering whether the results will be worth the effort. Here are some specific questions:

1. Will PWM through grbl give me capabilities that K40 whisperer doesn't? (e.g. variable power engraving for lithophanes)

2. I've got a 2-up relay board to control air assist and exhaust fan. My initial look at grbl seems to indicate a shortage of control pins. Will I be able to use both relays without sacrificing other functions?

3. I'm thinking about adding a rotary table (swapped with Y axis) and a motorized Z lift (to support multi-pass cutting of thicker materials). Will I be able to home Z if using PWM to manage laser power?

4. I've seen posts stating getting raster tuned in is more complex. Is this a one-time tuning thing, or is there constant adjustment needed (and would it be any different from K40 whisperer in that respect)?



Thanks for your insight.







**"Tom Traband"**

---
---
**Joe Alexander** *April 03, 2018 06:39*

I would recommend giving it a shot, most of the m2nano boards dont seem to be able to do engraving much beyond a basic dither ( could be wrong, I ripped mine out immediately after initial test). 

1: It should be able to give better engraving results.

2: I would recommend hard wiring the exhaust fan and water bump to be on whenever you power up the machine (either internally, using a power strip). You will need them on every job regardless, and they don't contribute a lot of noise. The air assist is the one that is best controlled by relay, I currently have mine on a wireless relay and manually engage it(lazy) as needed.

3: Im not sure about how the grbl shield would manage that but most can control 4 motors. That means each gets its own stepper driver chip and pinout, and I know Lsserweb can control both z tables and rotaries. Shouldn't have to swap motor connections imo.

4: The tuning is rather easy but changes with various materials. The settings part is rather easy, mostly its about the powqer range you work with, preparing the photo for the best image, and finding a sweet spot on the potentiometer. I have mine set @15ma constantly for cuts, and occasionally down to 10ma for engraving.


---
**Don Kleinschnitz Jr.** *April 03, 2018 11:03*

In addition with other than the nano you get open source everything especially hardware so you know what is going on.

Don't forget to try out Lightburn when you get a compatible controller.



The chart in here may be helpful in thinking about upgrades.

[https://plus.google.com/+DonKleinschnitz/posts/bsa2PUhQhf1](https://plus.google.com/+DonKleinschnitz/posts/bsa2PUhQhf1)


---
**Jim Fong** *April 03, 2018 11:54*

On a Arduino Uno, the two free output pins are the coolant and spindle direction.  You can use them to control your relay.  



M2nano is only dither.  Grbl1.1f supports variable pwm laser power output.  



No problem homing Z. Lightburn does per pass Z level for thicker material cutting.  You can set the amount the z will raise for each pass. 



PWM raster engravings can be harder to tune in.  Material type and power levels affect the image outcome.  It gets easier after you done a few.  





I use a Cohesion3d mini board in my laser since it is pretty much plug and play with the original k40 cable connectors.   Even though I have a bunch of uno/stepper Shields laying around, I took the easy route. 



Smoothieware on Cohesion3d supports 4axis rotary so no need to swap with Y axis and changing configuration values.  

Grbl/uno only supports 3axis.



I stopped using Laserweb4 and moved to LightBurn, much nicer. 


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/9RRqMKeb2hB) &mdash; content and formatting may not be reliable*
