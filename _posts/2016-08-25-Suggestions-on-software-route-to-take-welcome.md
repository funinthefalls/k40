---
layout: post
title: "Suggestions on software route to take welcome"
date: August 25, 2016 18:24
category: "Software"
author: "Nigel Conroy"
---
Suggestions on software route to take welcome.

My machine came with RDWorks but trying to install on school computers and it flags it as having a Trojan.

Can't find a download that doesn't cause the same issue.



We currently use Paint.Net in class for image editing and graphics etc.. so my understanding is that we could use that to create/edit images. Assuming this is what Inkscape is used for.



So I think I'm just looking for a replacement/alternative to RDWorks.



So any suggestions? (free/opensource options are obviously preferred)



Can LaserWeb3 be used without a smoothieboard?



Thanks





**"Nigel Conroy"**

---
---
**Ariel Yahni (UniKpty)** *August 25, 2016 18:29*

Laserweb needs either a grbl or smoothieware compatible board at the moment. 


---
**Ariel Yahni (UniKpty)** *August 25, 2016 18:30*

Any painting or vector software is fine for generating content is ok. 


---
**Nigel Conroy** *August 25, 2016 18:34*

Is it safe to assume the default board on my K50 is neither of those?


---
**Ariel Yahni (UniKpty)** *August 25, 2016 18:40*

 I would say yes. 


---
**Ariel Yahni (UniKpty)** *August 25, 2016 18:42*

Software being detected as Trojan most likely as the files are pirated, so why you don't first run them inside a VM just to be sure no harm is done. 


---
**Nigel Conroy** *August 25, 2016 18:44*

Network manager suggested a standalone pc but I'll want classes to prep files so one pc would create bottle neck.


---
**Ariel Yahni (UniKpty)** *August 25, 2016 18:46*

That could be another option so create in any drawing solution ( paint, Inkscape, etc) and then pass the file to the controller PC


---
**Nigel Conroy** *August 25, 2016 19:05*

Yes but they would have to use the controller PC and RDWorks to set the cut speed and power, etc...


---
**Nigel Conroy** *August 25, 2016 19:16*

Well with a bit more searching the Tech support has managed to find a download of RDWorks from rd-acsDOTcom that doesn't get flagged as a Trojan. It's also the most up to date version so that's good.



Thanks for input **+Ariel Yahni** 


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/HRsUpXPFxsW) &mdash; content and formatting may not be reliable*
