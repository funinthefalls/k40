---
layout: post
title: "Well, looks like I'll be getting a smoothieboard after all !"
date: March 29, 2016 12:22
category: "Hardware and Laser settings"
author: "Jean-Baptiste Passant"
---
Well, looks like I'll be getting a smoothieboard after all !



Someone was selling one near me, so I pulled the trigger and took it.



There is no LCD though, so I am looking for an adapter (if I take the "pcb shield with unsoldered connector" from robotseed, do I receive the pcb and connector and just solder it myself or is that only the pcb without connector ?) 



Also, how can I check if the smoothieboard is genuine ? The seller told me he use it a few times and went with another board for 128 microstep, but was selling it for dirt cheap (80€ for a 5XC).



Thank you !









**"Jean-Baptiste Passant"**

---
---
**Stephane Buisson** *March 29, 2016 16:11*

don't forget the voltage regulator, or the LCD will not have enought juice (display without contrast, so you can't see anything and think it's not working)


---
**Jean-Baptiste Passant** *March 29, 2016 17:48*

Yes, It's already in the cart, thank you ;)


---
**Jean-Baptiste Passant** *March 29, 2016 17:48*

**+Stephane Buisson** I should have listened to your tips since the beginning :D


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/6JfEiiHDYF6) &mdash; content and formatting may not be reliable*
