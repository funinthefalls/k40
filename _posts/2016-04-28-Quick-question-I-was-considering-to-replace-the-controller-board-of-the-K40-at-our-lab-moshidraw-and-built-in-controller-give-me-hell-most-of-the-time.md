---
layout: post
title: "Quick question: I was considering to replace the controller board of the K40 at our lab (moshidraw and built-in controller give me hell most of the time)"
date: April 28, 2016 14:37
category: "Discussion"
author: "Miguel S\u00e1nchez"
---
Quick question: I was considering to replace the controller board of the K40 at our lab (moshidraw and built-in controller give me hell most of the time). But I think what is wrong with the board is the software so I thought maybe I could leave the board (stepper motors + glue electronics and just place the new control signals over the proper pints of the 40 DIP socket actually used by the current microcontroller. Did anyone follow that path? Is it a bad a idea? One of the reasons is the flexible band socket I do not have elsewhere. 





**"Miguel S\u00e1nchez"**

---
---
**Scott Marshall** *April 28, 2016 15:48*

That's sure not the worst idea I've heard, but there are a couple of big issues that make it impractical. The 1st being the root of the whole problem, the lack of any documentation on the hardware, firmware and even the software supplied. This is the case with at least with the later boards, and I'm pretty sure the Moshi setup which preceded the current 'generation' is undocumented as well.



I can only speak for the 2nd generation board(current), the one which uses the single board, no ballast resistor & LaserDRW/Corel v12, but the controller there is just fine, with no major drawbacks (except possibly the lack of software laser level control), sad to say, all it needs is some way to write a translator or new firmware so it can be made compatible with decent software. The problem being the manufacturers have not/will not release the information required to do so. To my knowledge, no one has attempted a reverse engineering solution.



The Bottom Line:

Catch#1 - This lack of documentation is what would make your idea a monumental task. Once done, there's a market for sure. 

I'd expect, once you pick it apart, you would probably would end up with a way to flash the processor or replace it with one that can be driven with commonly available software.

Catch #2 -  I think what discourages people with the expertise to do this is that once you invest the time and money, the K40 manufacturers could change to a different board at any time, rendering your "fix" more or less worthless.



The other options:

So the main reason people change the controller is simply to run good quality and/or modern (and constantly improving) open source software.



It would be great if, as you suggested, you could just connect a new controller board to the motor drivers on the board and tie in the limit switches in a similar fashion. If we had available drawings on the stock board/s, you could look it over and see if it's feasible (and where to connect). Unfortunately, I am pretty sure you'd have to trace the whole board out, device spec sheets in hand and generate a schematic and layout drawing for the board. It's doable, but in my opinion impractical.



 The Smoothieboard  is the most popular conversion, (at least here on the Google K40 community) is not the cheapest, but nearly so, and probably the best setup for the money.  It's compatible with at least 3 open source software packages, includes stepper drivers.and all the I/O required. In addition, the developer has a forum, and is right there for you  if you run into trouble.  (look to the Wiki for details).



 There are quite a few general purpose motion control boards available as well, and then there are dedicated controllers and the commercial software usually packaged with them.



 These packages usually cost significantly more than the K40 intially does, and the Smoothie comes in at just over $200 for the typical (at least my) install. In addition to the board it's a good move to add dedicated 5 &  24v power supplies, then there's some signal conditioning for the laser level and adapters for the 'FF'  style flat cables)



Scott


---
**Miguel Sánchez** *April 28, 2016 16:01*

Thanks a lot Scott for a very informative answer. I just learned about this page: [http://wtfmoogle.com/?p=3809](http://wtfmoogle.com/?p=3809)


---
**Scott Marshall** *April 28, 2016 17:09*

Thanks Miguel, That's huge, and the 1st I've heard of it. 



You may have hope for piggybacking something in after all. It's still far from an "easy" deal, but it's a start.



In addition, it will help a lot of members here.

I'll get it put up on our new Reference section.



I sure wish I could find that for my version, I'm just about ready to remove the factory board an install a Smoothie 5X.



Scott


---
**Ulf Stahmer** *April 28, 2016 19:13*

**+Scott Marshall**, just curious, why a 5X and not a 4X?  I realize that the 5X is only about $35 more, but even with a z-table, there are only 3 steppers on a K40 and both the 4X and 5X have Ethernet. (And $35 will buy a nice roast for a family dinner :-) )


---
**Scott Marshall** *May 02, 2016 00:24*

**+Ulf Stahmer** If you make a mistake and pop a motor driver or output, it's either SMD work or you're buying a new board. If you have an extra output or 2, you can move some wires, edit the setup and you're back on line.

For most people, with all the resources extended it's oops, fork over another $150.....

For me, the extra features are worth it. I did price out the parts and DO have SMD equipment, but you can't buy the parts and add them yourself for the difference. That's why I bought a 5X. As with most things, opinions vary. I personally like spares.



Plus I may use Smoothieboards for other machines, and the 5 will do that too. For sure.


---
**Ulf Stahmer** *May 02, 2016 16:38*

Thanks **+Scott Marshall**.  Your reasoning has influenced my decision in purchasing a Smoothie.  Like you, I think I will go with a 5X.


---
*Imported from [Google+](https://plus.google.com/113179837473309823193/posts/LWYGuRwGmQ5) &mdash; content and formatting may not be reliable*
