---
layout: post
title: "Haven't posted in a while as I've been really busy"
date: November 21, 2017 16:39
category: "Object produced with laser"
author: "Nigel Conroy"
---
Haven't posted in a while as I've been really busy. Partly with trying to get a side business going utilizing the laser to craft things. Had our first craft fair on Saturday and it when really well so hopefully good signs for the future.

Thought I would post some of the things we've been making.



Slate is engraved with a Gaelic saying that means one hundred thousand welcomes.







![images/043b19d5c521eda7f603caa2395d1038.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/043b19d5c521eda7f603caa2395d1038.jpeg)
![images/7e59e053906850c1e3955e2a13b109d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e59e053906850c1e3955e2a13b109d4.jpeg)
![images/6049e79721456a82eb92728b35fa2819.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6049e79721456a82eb92728b35fa2819.jpeg)

**"Nigel Conroy"**

---
---
**Tim Macrina** *December 19, 2017 20:39*

What settings did you use for the slate?


---
**Nigel Conroy** *December 20, 2017 14:12*

**+Tim Macrina** 300mm per sec and 30% power (60w laser)



The key I found is to put a finish on the slate first and then engrave.



I used a spray gloss enamel or lacquer.


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/bom5NGK6ip3) &mdash; content and formatting may not be reliable*
