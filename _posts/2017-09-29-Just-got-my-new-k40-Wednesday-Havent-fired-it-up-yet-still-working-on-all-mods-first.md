---
layout: post
title: "Just got my new k40 Wednesday. Haven't fired it up yet still working on all mods first"
date: September 29, 2017 21:20
category: "Discussion"
author: "Space Ronin"
---
Just got my new k40 Wednesday. Haven't fired it up yet still working on all mods first. Trying to wire up the interlocks. I have the laser with the digital power adjust and test button on a small board attached to the lid. I wasn't about to wire interlocks to lasers power wires. Is there a lower voltage wire on the stock board or the psu I can use for my interlocks. Any help would be appreciated. I couldn't find much online with my same laser. 





**"Space Ronin"**

---
---
**Martin Dillon** *September 30, 2017 00:59*

You use the wire to the laser switch button.  Not sure what that looks like on the digital ones.




---
**Space Ronin** *September 30, 2017 01:15*

This is the back of the board for laser switch

![images/0d71a639d3aac03c7783c5bad9144221.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d71a639d3aac03c7783c5bad9144221.jpeg)


---
**Space Ronin** *September 30, 2017 01:25*

Mine looks similar to this but the pink wires on this go to a switch on the front of the maccieb labeled power switch. Not sure power for what. There's also a separate switch labeled switch machine. So I guess the switch machine is the power for whole unit and the power switch is  what I want my interlocks on ?

![images/03141f9bb143d2631e662a5063e904bb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/03141f9bb143d2631e662a5063e904bb.jpeg)


---
**Don Kleinschnitz Jr.** *September 30, 2017 04:13*

Just add your interlock switches in series with the "laser switch" shown in the power supply picture above


---
**Space Ronin** *September 30, 2017 13:31*

Ok thanks. Doesn't hurt to double check. Since some of these are made slightly different 


---
*Imported from [Google+](https://plus.google.com/103130365677659082439/posts/56i8KweSAzH) &mdash; content and formatting may not be reliable*
