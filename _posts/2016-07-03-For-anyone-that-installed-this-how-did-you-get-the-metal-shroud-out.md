---
layout: post
title: "For anyone that installed this, how did you get the metal shroud out?"
date: July 03, 2016 00:30
category: "Modification"
author: "Terry Taylor"
---
For anyone that installed this, how did you get the metal shroud out?



[http://www.thingiverse.com/thing:1627210](http://www.thingiverse.com/thing:1627210)





**"Terry Taylor"**

---
---
**Ariel Yahni (UniKpty)** *July 03, 2016 00:53*

You need to remove the gantry first to be able to remove the factory exhaust


---
**Eric Rihm** *July 03, 2016 01:11*

I just used a dremel with a cutting wheel so I didn't have take anything apart.


---
**Derek Schuetz** *July 03, 2016 02:22*

You need to remove the whole gantry to remove the old and install the new


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 03, 2016 02:40*

I did what Ariel & Derek did.


---
**Gary Hamilton** *July 03, 2016 02:52*

Sorry to jump on someone else post but do the do any more than simply cutting down the existing one?  I hate the way the motor mounts in the back but do not see a real issue on the inside.




---
**Ned Hill** *July 03, 2016 03:21*

It's kind of a pain in the ass for the newer machines apparently.  See my post here [https://plus.google.com/108257646900674223133/posts/TWpTcM9gQeY](https://plus.google.com/108257646900674223133/posts/TWpTcM9gQeY) as it may be of help to remove it.


---
**Corey Renner** *July 03, 2016 04:28*

<snip> 

It's kind of a pain in the ass for the newer machines apparently

<snip>

Exactly correct and well-put.  Cheers, c


---
**Don Kleinschnitz Jr.** *July 03, 2016 13:56*

I am missing something, what is it that folks dont like about the stock one it seems to work fine on mine?


---
**Ned Hill** *July 03, 2016 14:13*

**+Don Kleinschnitz**  It's more about the length as it restricts the size of the object you can put in the machine.  Also, at least for some,  it makes removing the carriage a major hassle if you ever need to replace or tighten the belts.


---
**Don Kleinschnitz Jr.** *July 03, 2016 17:26*

**+Ned Hill**  Ah ok I get the length restriction, makes sense. I am actually thinking about .... eventually a different cabinet that lets you slide in larger pieces and the Z axis table fits below the unit that will solve both problems.


---
**Thor Johnson** *July 04, 2016 00:47*

I was able to remove it by loosening the bolts from the back side.

A power screwdriver (in my case drill with an apex) will spin it fast enough so you can put a small plastic prybar (like for removing automotive fasteners) between the head and the case and have the friction be enough to remove the nut.


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/Jq7Qga1QAjc) &mdash; content and formatting may not be reliable*
