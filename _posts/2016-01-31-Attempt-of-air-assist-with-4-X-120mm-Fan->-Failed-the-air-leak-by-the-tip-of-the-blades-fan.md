---
layout: post
title: "Attempt of air assist with 4 X 120mm Fan -> Failed the air leak by the tip of the blades fan"
date: January 31, 2016 13:07
category: "Modification"
author: "Stephane Buisson"
---
Attempt of air assist with 4 X 120mm Fan -> Failed

the air leak by the tip of the blades fan. not enough pressure build up.

only 2.1 m/s best reading against around 7m/s with compressor.

I lost my saturday ...



![images/70f6ec117cd11f80fd691f64f3bffc08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/70f6ec117cd11f80fd691f64f3bffc08.jpeg)
![images/41799029051d0d24f97dc65e2b353b7d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41799029051d0d24f97dc65e2b353b7d.jpeg)
![images/c85e91346dd76c73bf6f7e570b96556e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c85e91346dd76c73bf6f7e570b96556e.jpeg)
![images/2078bf69f2822df7caa99dbb5b9ed51a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2078bf69f2822df7caa99dbb5b9ed51a.jpeg)
![images/5bf16e7285a6012145b28d0728ddfe98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5bf16e7285a6012145b28d0728ddfe98.jpeg)

**"Stephane Buisson"**

---
---
**David Cook** *January 31, 2016 16:43*

Good attempt. Sometimes at work a design may need a higher pressure fan so I double stack fans to achieve that. It does not double the pressure bit it does increase it.  


---
**HP Persson** *January 31, 2016 21:54*

Play around with the fans a bit, you should atleast see 3-5m/s from four decent 120mm fans.

Probably the fans are working against each other in that setup, if not divided on the insides.



I tried two 140mm fans on a metal funnel with a hose on, almost blew the fish out of the fish tank :P

My laser arrives next week, but i was curious to test air assist :)


---
**Stephane Buisson** *February 01, 2016 00:14*

**+HP Persson** it's divided inside. and I love to have 5m/s @ hose. just very cheap fan not suitable for that job. the size of the gap between blades circonferance (red) and enclosure (black) is too big compare to the size of the hose output. Lot of pressure is lost here (4x2w energy need to go somewhere)


---
**Joseph Midjette Sr** *February 01, 2016 00:42*

I would say with that setup you are doing not much more than creating turbulence and not forcing air where it needs to go. It appears that you are pointing 4 fans inwards instead of just using 1 or 2 sufficient fans to blow the air in the "direction" it needs to go. Also using that type of fan and attempting to force the air into a restricted tube you will lose a lot through the blade gap essentially causing it to even slow down... If it were me  I would attempt using a turbine for an RC jet plane. Smaller footprint, less reduction steps and more air flow!

Just a thought.... Check this out >>> [http://www.ebay.com/itm/64mm-Duct-Fan-RC-4500KV-Brushless-Outrunner-Motor-For-RC-Model-EDF-Jet-AirPlane/331034860809?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D33878%26meid%3Db9fcda43305e4688bfbc9adcdb8b0e6c%26pid%3D100005%26rk%3D3%26rkt%3D4%26sd%3D121875127889](http://www.ebay.com/itm/64mm-Duct-Fan-RC-4500KV-Brushless-Outrunner-Motor-For-RC-Model-EDF-Jet-AirPlane/331034860809?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D33878%26meid%3Db9fcda43305e4688bfbc9adcdb8b0e6c%26pid%3D100005%26rk%3D3%26rkt%3D4%26sd%3D121875127889)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/TctZssPQ1tV) &mdash; content and formatting may not be reliable*
