---
layout: post
title: "There's a theatrical showing of The Neverending Story coming up, so I figured I'd make AURYN pins for my wife and I to wear to the movie"
date: August 28, 2016 23:31
category: "Object produced with laser"
author: "Tev Kaber"
---
There's a theatrical showing of The Neverending Story coming up, so I figured I'd make AURYN pins for my wife and I to wear to the movie. 



Initially I just grabbed an image and etched it in a circle, but then I made a few iterations to refine it, adding a shape cutout and partial cuts to strengthen some lines. Then I used two stains and colored each snake with a q-tip.



Final version, 3 passes:

etch (engrave): 5mA @ 400mm/s

enhance (cut): 5mA @ 30mm/s

cut out: 9mA @ 10mm/s



![images/5f499c497ca3e63bb9615e954044f7d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5f499c497ca3e63bb9615e954044f7d9.jpeg)
![images/a4ac06fa57c312f320a91ef68f792497.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4ac06fa57c312f320a91ef68f792497.jpeg)
![images/f6befd51e2f47697c51ddf3045ca0feb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6befd51e2f47697c51ddf3045ca0feb.jpeg)

**"Tev Kaber"**

---
---
**HalfNormal** *August 29, 2016 02:03*

Very nice 3D effect.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 29, 2016 02:42*

They look really cool. Did you paint them afterwards to get the two colour effect?


---
**Tev Kaber** *August 29, 2016 02:46*

**+Yuusuf Sallahuddin** Yes. Well, not paint but 2 different color minwax stains, "Golden Pecan" and "Red Mahogany". 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 29, 2016 02:47*

**+Tev Kaber** Gives it a beautiful effect. I like it :)


---
**Jeff** *August 29, 2016 04:59*

They look awesome, you did a really nice job!  What wood and thickness did you use?


---
**Tev Kaber** *August 29, 2016 05:02*

**+Jeff Watkins** Thanks! It was 3mm birch plywood. 


---
**Tony Schelts** *August 29, 2016 07:30*

Nice work are your files available, wouldn't mind giving it a go?


---
**Stephen Sedgwick** *August 29, 2016 12:31*

These are awesome!!  Love to see the posts


---
**Tev Kaber** *August 29, 2016 14:04*

Sure thing! The engrave image is one I found unsourced on Pinterest, probably safe to assume it is copyrighted and so this file is for personal use only.



CorelDraw X5 file:

[https://s3.amazonaws.com/tevk/laser/neverending-story-auryn.cdr](https://s3.amazonaws.com/tevk/laser/neverending-story-auryn.cdr)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 29, 2016 14:08*

**+Tev Kaber** I found the same image earlier today when searching for Ouroboros.

[https://yallambie.wordpress.com/2016/01/13/ouroboros/](https://yallambie.wordpress.com/2016/01/13/ouroboros/)

I think possibly the image is an old one that can't really be copyrighted, but maybe it is & was just used as an example of Ouroboros.


---
**Tev Kaber** *August 29, 2016 14:18*

**+Yuusuf Sallahuddin**  Certainly there are other old ouroboros designs out of copyright, but I'm pretty sure that double snake/celtic knot design is specific to Neverending Story, the book came out in 1983 and the movie in 1984, well within copyright. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 29, 2016 14:32*

**+Tev Kaber** Yeah, I realised that it might be specific to Neverending Story after I posted my last reply. Either way, it's a cool design & turned out really nice how you did it.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/Nm4A8FagBtq) &mdash; content and formatting may not be reliable*
