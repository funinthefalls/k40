---
layout: post
title: "Does anyone have a schematic for a MYJG40w power supply?"
date: June 20, 2017 13:42
category: "Original software and hardware issues"
author: "Calum Stirling"
---
Does anyone have a schematic for a MYJG40w power supply? . Have emailed JNMYDY.com but no reply.



when the unit arrived middle green block of the supply was not connected .When I connected that block and switched the unit ON, its went bang and seems to have fried an IC and a couple of resistors right at the edge of the board at the green terminal block, see pic. Alas this is not a returnable item. The board might be repairable if I have a schematic. 



The bigger question is why did it go bang and where in the 240v wiring is the central issue.

![images/04c7efc66498658cafc61e46cf9a482b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/04c7efc66498658cafc61e46cf9a482b.jpeg)



**"Calum Stirling"**

---
---
**Andy Shilling** *June 20, 2017 13:54*

Not sure if it's the same as mine but all my labelling was under the green connectors so I had to carefully lift them to see it.


---
**Calum Stirling** *June 20, 2017 14:00*

Thanks. I think I can work out the wiring in and out of the PSU the problem i've got is identifiing and replacing the exact components on the board, if at all. Am I right in thinking your talking about the lifting the connector block only? 


---
**Calum Stirling** *June 20, 2017 14:01*

The board fuse seems fine but I can see there is a blown IC5 and two others diodes not looking to good..




---
**Andy Shilling** *June 20, 2017 14:10*

Yes sorry I was on my phone and I didn't see all of your question.


---
**Anthony Bolgar** *June 20, 2017 14:47*

**+Don Kleinschnitz**


---
**Joe Alexander** *June 20, 2017 15:08*

probably a redundant question but you did remember to set the switch on the side to either 110 or 220v's depending on your supply right? And that sucks having it blow right out of the gate like that...


---
**Don Kleinschnitz Jr.** *June 20, 2017 15:11*

**+Calum Stirling**



#K40LPS



<b>*Warning this supply creates lethal voltages internally and externally do not attempt repair unless you are trained in dealing with HV circuits*</b>



Yes we have a schematic no need to pull connectors:



[digikey.com - K40 LPS](https://www.digikey.com/schemeit/project/k40-lps-7S9UFV0201F0/)



The part that failed is one of the input opto-couplers and probably the pull-ups and isolation diodes. See schematics but realize that part identifiers are different on individual supplies so the schematic part labels may not match your board.



Also see this repair post with parts purchase data: 

 [http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html](http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html)



Not obvious why it would have failed except there are very high voltages and if the low side got connected to the high side ..SMOKE!,



The other option is some form of short on the inputs. I would suspect the cable that was left off and would check the wiring before hooking the inputs back up after repair.



An old wiring diagram of a K40 is here: [https://www.digikey.com/schemeit/project/k40-wiring-GCKOUK0100Q0/](https://www.digikey.com/schemeit/project/k40-wiring-GCKOUK0100Q0/)


---
**Calum Stirling** *June 20, 2017 15:24*

Thanks Don and everyone else contributing to this discussion. That's great you have a schematic. I'm not experienced enough to replace these components but I know someone who is.  



The optocoupler looks like an Orient 817B but no idea about those diodes (4701 possibly)



I bought the unit secondhand. Apparently it had only been used twice but the guy couldn't align the mirrors. It did work though and the tube appear to be intact. I see the burn marks relate to the wires going to the potentiometer and laser on switch, all from correspondent block inputs from the cable that was left off . 



I also notice that the laser switch is currently in the ON position meaning when it possible was in ON position when the machine was switched on.  Would this lead to an overload? 



Thanks for help


---
**Calum Stirling** *June 20, 2017 15:34*

Post switch on picture. 

![images/566c778abef52d33731ca095be4c99dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/566c778abef52d33731ca095be4c99dc.jpeg)


---
**Calum Stirling** *June 20, 2017 15:35*

Pre switch on showing cables and block layout. Issues are with the central cables. I'll take everything you've supplied Don to my electrics guy and do some tests..

![images/0d548a9c835dcf1c954f006181f014b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d548a9c835dcf1c954f006181f014b3.jpeg)


---
**Don Kleinschnitz Jr.** *June 20, 2017 15:58*

**+Calum Stirling** 

The smt diodes are probably <b>A4W</b> [<i>I think BAV70 is equivalent</i>]



The laser switch being left on when you powered on should not have been a problem but that wiring is suspect since that is the circuit that smoked?

The cable that is unplugged are control signals that should not create this kind of damage. I suspect a serious miswire.


---
**Calum Stirling** *June 20, 2017 16:10*

thanks for diode heads up. We need to go through the schematic and check all the wiring. Your right it definitely looks like a miswire somewhere and the cabling is the first thing to check. Just so i'm clear here, I plugged that middle block in before switch on, there seemed no reason not to. The seller told me he only switched the machine on twice. If he did so with the middle block in and it worked what went wrong between it being packed to be sent to me and switch on here? i'll let my electrical pal run the checks..


---
**Anthony Bolgar** *June 20, 2017 16:42*

Thanks for helping out **+Don Kleinschnitz** All those hours of tearing apart old supplies sure have come in handy to help quite a few people.


---
**Calum Stirling** *June 20, 2017 16:45*

Indeed, thanks very much out **+Don Kleinschnitz**


---
**Don Kleinschnitz Jr.** *June 20, 2017 17:07*

**+Anthony Bolgar**  ...... and thanks also to **+Nate Caine** and **+Paul de Groot** for their expert help. Finally for the community donating $ to buy parts and test equipment while sending bad supplies...keep em coming!


---
**Nate Caine** *June 20, 2017 17:08*

(0) You have a serious problem, probably related to the AC wiring.  Very dangerous.  I hope you have a well-grounded machine.



(1) What is that RED HV wire just laying there in the photo?  Only grief can come from that!



(2) That's a pretty dramatic failure to blow the tops off the opto-isolators.  Fortunately, they may have saved the rest of the board.  



(3) I suspect an AC wiring problems.  Is it possible the power supply was replaced and the wiring not checked?



(4) Take some more photos of the power supply with the cover off.  Especially around EACH connectors showing the wire hook-up sequence that is silkscreened on the board.  



(5) No two power supplies seem to be the same, but a common sequence at the mains power connector is [L-] [FG] [AC] [AC].  However, in a different supply the [AC] [AC] terminals are in the middle of the connector.  What does <b>your</b> power supply say?  Does the machine wiring match?



(6) The diodes D1 and D12 are completely non-critical, and can be replaced with nearly anything handy such as 1N4148 or even 1N4001.  The cathode ends are connected to the TEST switch (labeled "TEXT").  When the TEST switched is pressed one diode turns on "L" (to turn on the laser), and the other diode turns on "P" (to turn on protection).  The laser will turn on, but only if the Intensity pot is dialed to some intermediate position or connected to the +5V pin.  (+5V = 100% laser power).



(7) You also said the wires to the potentiometer were damaged?  

Photos?



(8) Regardless you should draw a accurate wiring diagram for YOUR machine.  With signal names and wire colors.


---
**Don Kleinschnitz Jr.** *June 20, 2017 17:37*

**+Nate Caine** **+Calum Stirling**

Nate your right.... that looks like the High Voltage lead which is supposed to be connected to the lasers anode lying there.

**+Calum Stirling** Don't power the machine up until you find out what is going on with the red wire.....



You said the Laser Enable switch was on when you powered up. If that is the case that HV lead could have arc'd (if a fire signal happened) to one of the input pins. If so you may have lots of components bad .... sorry!



9. As Nate asked. was this supply replaced and never completely hooked back up and tested?



10. What is connected to the anode of the laser.

11. What is connected to the other end of the red wire laying in the photo.



Nate is right you can use almost any diode but you must get them in the circuit the right way round. If you don't install them the LPS may still work but the on board test button wont.


---
**Calum Stirling** *June 20, 2017 18:36*

Ok since I started this conversation i've noticed something, which might illuminate the origin of the problem. 



The unit arrived with the PSU unconnected or partially unconnected. (it is set correctly to 240v by the way). The three green blocks were then connected as the wiring seemed to naturally lie (not information available in the cd manual anyway). See pic.

Looking at picture I took at the time it arrived I think the two outer 4 pin wiring blocks are the wrong way round, left should be right and visa versa! 

I just switched the wiring now to show you to see if you think which of the two wiring layouts is correct. (The central block unconnected in the first picture was connected at switch on. I will of course be checking the HT side of things before switch on, am aware of arcing issues and proper grounding and the high voltage. 



Does this look like the likely cause?!

![images/43fcde73c8862d7c45e3fff06f2c1e93.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43fcde73c8862d7c45e3fff06f2c1e93.jpeg)


---
**Nate Caine** *June 20, 2017 18:56*

**+Calum Stirling**​ Yeah, big time.  



This is the kind of dramatic high powered event that would blow the top off the opto-isolators and vaporize the resistor.  You have applied 240VAC to what should have been the (+5V) and the (LaserOn) pins.  I suspect that took out the two TEST switch diodes (mentioned earlier) as well.  And likely the other opto.  Replace 'em all.  



However, not knowing the way your box is grounded in your country, there may be additional damage to the Low Voltage section of the power supply, especially the +5V section.  Did you observe any damaged parts there?



Depending on those grounds, this may have impacted the Intensity potentiometer, and the Laser Enable Switch which you also mentioned.



You have your work cut out.



p.s.  On many other power supplies, the AC connector has very different pin spacings.  That prevents the exact "swapped-connector" problem you experienced.  The K40 is a <b>very</b> marginal design.




---
**Calum Stirling** *June 20, 2017 19:08*

I've check the low voltage side of things tomorrow.  The visible damage I can see is to IC5, (maybe IC3 also), C24 and C23. see pic, after i'd cleaned them up a bit. 

I'll check in when i've had time to explore. Carefully!

![images/fec20567a77cc0faae875b182b6ee285.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fec20567a77cc0faae875b182b6ee285.jpeg)


---
**Nate Caine** *June 20, 2017 19:29*

**+Calum Stirling** C23 and C24 are mis-labeled.  They really are resistors.  1K-ohm.   (You should confirm that the two associated diodes are acting like diodes as well.)


---
**Calum Stirling** *June 20, 2017 19:37*

Thanks Nate, this is very helpful appreciating all your advice. 


---
**Don Kleinschnitz Jr.** *June 20, 2017 20:28*

**+Calum Stirling** yup definely the problem you got 220V on the L and 5V.

You might be better off to consider a new supply!


---
**Paul de Groot** *June 20, 2017 20:52*

If the opti coupler blew then the rest of the psu should be okay. Try as suggested to replace the opti coupler and resistors.


---
**Calum Stirling** *June 20, 2017 21:07*

Yeh planning on trying those things first. Thanks all!


---
**Don Kleinschnitz Jr.** *June 20, 2017 22:25*

**+Paul de Groot** if only the opto-coupler and resistor(s) is all that blew I would think that the on-board green led and fan will come on when power applied properly? If not there is a lot more wrong.


---
**Paul de Groot** *June 20, 2017 22:29*

**+Don Kleinschnitz** Yes you're right about that. The post was,very long so must have missed that part. The most likely scenario is indeed that someone swapped the connectors and blew the lowest voltage part. So probably most of the psu is toast. I'm like my father who repaired the washing machine 30 times. Sometimes you just have to replace it.


---
**Don Kleinschnitz Jr.** *June 20, 2017 22:34*

**+Paul de Groot** i find these are tough to repair because when something fails it usually takes a few parts with it. If you don't find and replace all of them at once you just start lighting money on fire. You can get these supplies for <$70.


---
**Calum Stirling** *June 27, 2017 21:52*

Well I am very pleased to report that after replacing two optocouplers and two resisters and reconnecting a blow circuit track the PSU was back in action. I connected it up and everything seems to work as it should. Tried a test engrave and that seemed ok too. 

Feeling very lucky!!


---
**Paul de Groot** *June 27, 2017 21:55*

**+Calum Stirling** that confirms our hypothesis that the seller swapped the connectors😁


---
**Don Kleinschnitz Jr.** *June 28, 2017 13:30*

**+Calum Stirling** nice another LPS victory under our belt, I have a LPS in my stack with a blown diode and never could logic why it blew, perhaps the user plugged the cables in incorrectly as well. Need to check out the opto-couplers probably.


---
**Calum Stirling** *June 28, 2017 13:33*

**+Don Kleinschnitz** Thanks for your advice Don, was very pleasing to see it working. 

It does seem however that a new problem has appeared today. See post


---
**Don Kleinschnitz Jr.** *June 28, 2017 13:44*

**+Calum Stirling** what post?


---
**Calum Stirling** *June 28, 2017 13:48*

**+Don Kleinschnitz** This one here, just 10mins ago. One giant leap forward and one back..

[plus.google.com - Thanks to everyone who offered advice recently about my faulty k40 PSU. I was...](https://plus.google.com/106058704145699553667/posts/bjK9GE9WR5r)


---
**Anthony Bolgar** *June 28, 2017 13:58*

I responded to your other post.


---
*Imported from [Google+](https://plus.google.com/106058704145699553667/posts/ATGPDxwsDCd) &mdash; content and formatting may not be reliable*
