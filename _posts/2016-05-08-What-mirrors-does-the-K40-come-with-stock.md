---
layout: post
title: "What mirrors does the K40 come with stock?"
date: May 08, 2016 20:35
category: "Discussion"
author: "Custom Creations"
---
What mirrors does the K40 come with stock?





**"Custom Creations"**

---
---
**Jim Hatch** *May 08, 2016 21:04*

What do you mean? Manufacturer? or size? I think they're 23mm.


---
**Custom Creations** *May 08, 2016 21:20*

Meaning Si, MO, Gold Plated. Which is better?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 21:25*

Mine are 20mm x 2mm thickness. I imagine they are probably just cheap quality.


---
**Jim Hatch** *May 08, 2016 21:26*

ah. In order of their ability to reflect more (and absorb/diffract less) of a CO2 beam, it's Gold, Si, MO. So for any given size you get a better reflection for MO vs gold. A gold one that can handle 40-60W would be able to do a couple hundred watts if it were an equally sized Moly mirror. Si is in the middle. Gold works fine for ours - we're only halfway thru its range. You can step up in terms of quality but the % difference isn't as great as it is in spending the money on a better, larger lens.


---
**Custom Creations** *May 08, 2016 23:18*

**+Jim Hatch** A better, larger lens? Please elaborate. I have been looking at the Lightobject 18mm Air Assist laser head and the 18mm ZnSe F50.8mm Focusing lens.


---
**Jim Hatch** *May 09, 2016 01:50*

The standard lens is 12mm. The LO 18mm Air Assist head can use the stock lens (you'll need to make a small 18mm ring with a 12mm hole in it as an adapter). They have 3 lenses that are better - an "improved" ZnSe, a "high quality" ZnSe lens and a GaAs one as well. Go with the high quality ZnSe if the extra $20 won't strain the budget. Much better lens. The larger size allows you to capture a bigger spread beam or off center beam from the mirrors. Remember the area of the lens increases with the square of the radius. The 18mm one has a 50% larger diameter but has a 224% larger surface area. They're ground & finished better than the stock lenses which gets you a better (smaller point) focused beam on the material side. The higher quality ones have less spherical aberrations in them to improve the cohesiveness of the converged beam. 



GaAs is a slightly better medium for higher powered lasers but not likely that much better for us. We're way on the low end of the capability of the LO high quality lenses. 



If you really want to go overboard, you can spend $150 on a lens (LO doesn't have them but you can chase them down on the web) but that seems like overkill on a $350 machine.


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/XoiLCfUsTYP) &mdash; content and formatting may not be reliable*
