---
layout: post
title: "Hello, anyone able to offer any advice on cleaning lens please?"
date: May 26, 2016 07:43
category: "Hardware and Laser settings"
author: "Pippins McGee"
---
Hello, anyone able to offer any advice on cleaning lens please?



I purchased 'Diggers' brand Isopropyl Alcohol (contains 100% isopropanol) and 100% cotton buds but it still leaves streak marks on the lens.



1) have I purchased the right solution for cleaning the lens? 

2) I only very very lightly and gently wipe the lens with the wet bud, almost putting almost 0 pressure on it at all. Is it ok to put a little pressure when wiping it?

3) any trick for avoiding streak marks? personally I don't understand I thought 100% isopronanol should evaporate 100%...





**"Pippins McGee"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 08:03*

Not sure if it will work, but can you submerged the lens in a small container of the isopropyl & then clean it whilst submerged (with your cotton buds) & then take it out (by the edges only) & allow it to dry/evaporate off?


---
**Alex Krause** *May 26, 2016 08:14*

Use a dry cotton swab to wick up the residue you my have disolved solids that appear on the lens after evaporation. I start in the center of the mirror and work my way out in a circular pattern to wipe the mirror clean applying little to no pressure


---
**Phillip Conroy** *May 26, 2016 08:36*

i do not use cotton bud i spray the Isopropyl Alcohol  onto lens then rub it back and forward on a lint free cloth ,holding by the edges and using a fair amount of pressure[you may have to repeat ] ,shine a torch at right angle to check if clean then turn over and repeat


---
**Alex Krause** *May 26, 2016 08:38*

I have also heard that eye glass lens cleaner and a microfiber cloth work really well


---
**Stephane Buisson** *May 26, 2016 08:39*

it so little reflective coating on stock mirrors, it's wise to go lightly, but you would go for better ones soon, air assist and good exhaust is helping a lot.


---
**Phillip Conroy** *May 26, 2016 09:02*

I have cheap chinese focal lens and have cleaned it about 50  times [every 20min] in about 100 hours cutting and it is still good


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 09:12*

**+Phillip Conroy** every 20 minutes? or every 20 hours? The math doesn't add up haha.


---
**Phillip Conroy** *May 26, 2016 09:19*

I added silca get water trap 40-50 cutting hours ago and no longer clean every 20 min,have not needed to clean since adding filter


---
**Jim Hatch** *May 26, 2016 12:03*

I second the use of eyeglass or camera lens cleaner wipes. Also, try starting with plain water. You may find you don't then need to use the alcohol. Lastly, check the cotton swabs to make sure there are no additives (perfumes) that can end up as deposits/streaks.


---
**Philip Ashton** *May 26, 2016 12:24*

I have been very impressed with cotton buds on both the lens and mirrors.


---
**Philip Ashton** *May 26, 2016 12:25*

Forgot cotton buds and Acetone!


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/JhtWU3NVYEP) &mdash; content and formatting may not be reliable*
