---
layout: post
title: "Owing to a failed psu I am looking to possibly install a 60w tube and PSU into my k40 cabinet, obviously the cabinet will need to be modified to accept the tube but I would like to hear from anybody that had already made this"
date: December 19, 2017 07:12
category: "Discussion"
author: "Andy Shilling"
---
Owing to a failed psu I am looking to possibly install a 60w tube and PSU into my k40 cabinet, obviously the cabinet will need to be modified to accept the tube but I would like to hear from anybody that had already made this modification.



I would like to know what psu you have used?



Is it possible to run a 60w psu with a 40w tube rather than throwing it in the bin?



Is there a significant difference in having the extra 20w to warrant the extra effort/outlay.



Thanks guys any input greatly received





**"Andy Shilling"**

---
---
**Phillip Conroy** *December 19, 2017 07:39*

It will be fine ,I am running a 50watt tube with a 40 to 80 watt pwr supply [aliexpress.com - Intelligent Z80 Co2 Laser Power Supply 80W For Laser Engraving And C Machine](https://www.aliexpress.com/item/Z-Series-Intelligent-Z80-Co2-Laser-Power-Supply-80W-For-Laser-Engraving-Machine-With-LCD/32806003257.html?spm=a2g0s.9042311.0.0.Gfuy)


---
**Andy Shilling** *December 19, 2017 09:44*

**+Phillip Conroy** thank you. Do you run a seperate PSU for the 24v supply or am I missing something?


---
**Phillip Conroy** *December 19, 2017 09:45*

I have the k50 ,yes it has separate 24 volt pwr supply 


---
**Andy Shilling** *December 19, 2017 09:59*

Cheers Phillip, I'm wired up for a single psu at the moment but this might be the way to go. As the low voltage side of my psu is still working ok I guess I could continue to use that to power the steppers and C3D.


---
**Joe Alexander** *December 19, 2017 10:15*

you could but as it is only rated for 1 amp of output your better off getting an external 24v 3-5Amp psu to run your steppers and controller board. Takes up less space also :P


---
**Andy Shilling** *December 19, 2017 10:26*

**+Joe Alexander** do you have any links to the seperate units?


---
**Joe Alexander** *December 19, 2017 10:29*

[amazon.com - Amazon.com: Aiposen 110V/220V AC to DC 24V 5A 120W Switch Power Supply Driver,Power Transformer for CCTV camera/ Security System/ LED Strip Light/Radio/Computer Project(24V 5A): Electronics](https://smile.amazon.com/Aiposen-Transformer-Security-Computer-Project/dp/B01B1PRE7E/ref=sr_1_4?s=electronics&ie=UTF8&qid=1513679348&sr=1-4&keywords=24v+power+supply+5a) like this guy


---
**Andy Shilling** *December 19, 2017 10:40*

**+Joe Alexander** thank you, I need to check but I think I might already have one of those. I feel a rewire coming on.


---
**Don Kleinschnitz Jr.** *December 19, 2017 12:56*

**+Phillip Conroy** do you have the display for your LPS working? If I upgrade I like that supply as well, although we do not have any schems for it. Have you by any chance asked the vendor for them?



**+Andy Shilling** I think I would go for at least 10A on the 24V if you can afford it :). Also before you finalize your LPS you should pick and get the specs for the laser you intend on using.


---
**Don Kleinschnitz Jr.** *December 19, 2017 13:20*

#K40LPS

#K40parts



Some interesting links, I wonder if we could buy direct from Q-laser?



The mirror mounts look like a nice upgrade don't know if they fit?



[https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20171219050247&SearchText=intelligent+laser+power+supply](https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20171219050247&SearchText=intelligent+laser+power+supply)





[qd-lasers.com - Shenzhen Qida Laser Technology Co., Ltd.](http://www.qd-lasers.com/en/)




---
**Andy Shilling** *December 19, 2017 13:26*

**+Don Kleinschnitz** I was looking at the cloudray HYT60 until Phillip put his 2 pence worth in. I like the idea of the z80 of it really does monitor what it says but being an aliexpress description I'm not so sure. **+Phillip Conroy**​ we need a full review on this please 😉


---
**Andy Shilling** *January 27, 2018 20:29*

**+Don Kleinschnitz** So after waiting for what seems forever I have received my new LPS. I went for the MYJG-60W and I'm now rewiring the machine but I wondered if you have a diagram for the wiring? I'm a little confused as what terminals what and don't want to wreck it before I start.The only diagram I can find is the one below so any help would be great.

![images/618b0a333770ea2e12964a957acc5421.png](https://gitlab.com/funinthefalls/k40/raw/master/images/618b0a333770ea2e12964a957acc5421.png)


---
**Andy Shilling** *January 27, 2018 21:57*

I've just found this also and I'm thinking I use the lower wiring. It's the High and low level light control that is throwing me.



![images/4059d1087a44647395799a69ff91cbd3.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4059d1087a44647395799a69ff91cbd3.png)


---
**Don Kleinschnitz Jr.** *January 28, 2018 12:21*

**+Andy Shilling** I am not sure how you were setupd before i.e. running a smoothie, C3D or Nano.

This picture shows how a Nano is connected with an external supply with a LPS of this type. 



Hope this helps, let me know if you have any problems with this setup.



[photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/kriOTz1aw0J6rgrH2)



For a smoothie style board replace the Nano below with the smoothie wiring you had. The only difference should be that the PWM should connect to TL on the LPS vs L on the old standard supply. I assume your smoothie wiring will be:



<b>To Smoothie:</b>

VBB = 24V from external supply

GND= single point connected to 24V supply gnd 



<b>From Smoothie to LPS:</b>

PWM= from an open drain on smoothie to TL on LPS.(this assumes you were not using a level shifter and connected to IN).

Smoothie gnd: from smoothie board gnd to LPS gnd



<b>Safety and noise wiring suggestions:</b>

Make sure all supplies have grounds from their source running directly to the destination ground them all to a single point at the supply. Do not daisy chain gnds from one destination to another. 



Make sure the LPS has FG tied to the frame and that all supplies safety gnd is also tied to the frame. Test with DVM.






---
**Andy Shilling** *January 28, 2018 17:38*

**+Don Kleinschnitz** Thank you very much good sir, I have been outside cutting, measuring and wiring making the case ready for when this tube goes and I upgrade to the 60w tube. I will sort out this final wiring later and hope I've got everything in place that I need.


---
**Andy Shilling** *January 28, 2018 19:06*

**+Don Kleinschnitz**​ I'm pretty much finished with the wiring but I just want to check that I must ground the C3D to the LPS and not the seperate 24v supply I have?


---
**Don Kleinschnitz Jr.** *January 28, 2018 19:55*

**+Andy Shilling** how were you using pwm, to the L or IN?


---
**Andy Shilling** *January 28, 2018 19:57*

To L on the old PSU


---
**Andy Shilling** *January 28, 2018 19:57*

This was the old one.

![images/c521599e34bdb17ea8093c2b60bb0f84.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c521599e34bdb17ea8093c2b60bb0f84.jpeg)


---
**Don Kleinschnitz Jr.** *January 28, 2018 20:17*

**+Andy Shilling**​ 

24vps to C3D cable

...+24v to C3D vbb

...24v gnd to C3 gnd



C3D PWM to LPS cable. Twisted pair the best.

...C3D pwm (wherever you got it from before) to TL.

...C3D gnd (from pin near pwm) to LPS gnd. 




---
**Don Kleinschnitz Jr.** *January 28, 2018 20:24*

BTW, did your old supply die?


---
**Andy Shilling** *January 28, 2018 20:24*

Ok I was using P2.5 screw terminal for pmw. So I can use the Gnd terminal just along from it to connect to Lps. I'm guessing as long as they are ground together it doesn't really matter what pin I use from the C3D.






---
**Don Kleinschnitz Jr.** *January 28, 2018 20:29*

**+Andy Shilling** generally yes, ideally you just want the grd current for the PWM driver to return back to the driver's gnd. 


---
**Andy Shilling** *January 28, 2018 21:05*

Excellent, and yes the old one died just before Xmas so I ordered the upgraded 60w and a new flyback for the old one just it the hope that was the problem. I now have the 40w sat as a spare with the new flyback.


---
**Andy Shilling** *February 04, 2018 21:22*

**+Don Kleinschnitz** I am now nearly up and running now but as it stands I have no laser fire from laserweb/Cohesion3d board. I am using grbl as before and using pin 2.5 from the board but nothing. Test fire works well although being a 60w lps I can only put max 1.5v through otherwise I'll be burning out the laser in no time. 

Any idea if I should use the pwm pin from the board instead?


---
**Don Kleinschnitz Jr.** *February 05, 2018 16:06*

**+Andy Shilling** 

What PWM pin from what board?



How is the C3D connected to the LPS?

Via the TL pin on the LPS? 

 

If so, try grounding the TL pin on the LPS without it connected to the C3D and with the Laser Switch on and all interlocks and water sensors closed. 

If the laser fires then the problem is at the controller otherwise the LPS is not enabled.


---
**Andy Shilling** *February 05, 2018 16:31*

**+Don Kleinschnitz** I have the 

p2.5 on c3d connected to TL on lps

Ground on c3d to ground on lps.



I have just checked the TL to Ground and that fired, does that mean I need to change the pin on the C3D or possibly the ground location on the C3D?




---
**Andy Shilling** *February 05, 2018 16:33*

![images/bfa5d71ec52eab290f5f398df3cc3fa1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bfa5d71ec52eab290f5f398df3cc3fa1.jpeg)


---
**Andy Shilling** *February 05, 2018 16:34*

**+Don Kleinschnitz** this is the ground I used should I change it to the Ground on the pwm terminal?




---
**Don Kleinschnitz Jr.** *February 05, 2018 16:42*

Which exact pin do you have the TL and ground connected to. Not clear which of these you used. Something must not be connected like it used to be :)?


---
**Andy Shilling** *February 05, 2018 16:46*

The TL is on (p2.5) in the image above and Ground is connected to the terminal 2 screws to the left.

![images/15d15ccef7ccb796907068b7fdd8ad93.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15d15ccef7ccb796907068b7fdd8ad93.jpeg)


---
**Don Kleinschnitz Jr.** *February 05, 2018 16:59*

That gnd pin looks ok but try disconnecting it and see if that changes anything. 

Is that where it was connected before you swapped out the supply?



Did you change the config file at all?



What do you have that you can look at the 2.5 pin with?



Does everything else on the C3D look like its working correctly, power, lights etc?


---
**Andy Shilling** *February 05, 2018 17:04*

No it was wired differently, I'll see if I've still got photos to refer to, the config remained the same and what do you mean look at the pin?


---
**Andy Shilling** *February 05, 2018 17:15*

**+Don Kleinschnitz** Thank you kind sir you've done it again. I just needed to remove the ground wire altogether.




---
**Don Kleinschnitz Jr.** *February 05, 2018 17:29*

**+Andy Shilling** ok that means that that minus is not really ground (I wondered).

Find a real ground near there. You can tell by measuring it to the C3D power supply ground with power off and a DVM on ohms ....


---
**Andy Shilling** *February 05, 2018 19:05*

I'm out at the moment be will check when I'm back but I think both C3D, psu and lps all give ground continuity. Any any G pin that I've checked on the c3d is the same.



On a side note maybe **+Ray Kholodovsky** could answer this one, I'm getting a disconnect sound from my pc when the laser fires, it doesn't actually disconnect. Could this be something to do with the ground problem?


---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2018 19:12*

Is it a separate 24v psu in addition to the new lpsu? 



You need a common ground between them, I'd prefer you do that directly instead of through the C3D. 


---
**Andy Shilling** *February 05, 2018 20:19*

 **+Ray Kholodovsky** **+Don Kleinschnitz** 

Ok so I have run a new ground wire between the 24v psu and the lps and I am still getting the disconnect sound when the laser is fired. Link below to the video file of that. what else could I try because I'm now quite confident everything is as it should be regarding the wires and the laser is actually firing and giving me the pwm control.



[drive.google.com - VID_20180205_200608.mp4](https://drive.google.com/open?id=1CKhmgstZ9xJXzMcEBV5Md0sVmKeUx9hb)


---
**Don Kleinschnitz Jr.** *February 05, 2018 20:24*

**+Ray Kholodovsky** ideally the ground for the PWM signal should be derived from the same grnd the driver is connected to and as close to the driver as possible so that the return current does not flow through a ground loop to the DC supply and then the C#D and finally the driver.



+Ray is the gnd connection that +Andy disconnected actually gnd or is it the drain of a mosfet? 



**+Andy Shilling** is the "disconnect sound"  occurring with the ground wire missing to the LPS? Your symptoms sound like you have a ground loop problem.


---
**Andy Shilling** *February 05, 2018 20:35*

**+Don Kleinschnitz** Both with it connected to the lps and the 24v psu and not connected at all????


---
**Don Kleinschnitz Jr.** *February 05, 2018 20:47*

**+Andy Shilling** 

Best I can tell where you had the ground connected for the PWM is correct i.e. that is ground on the C3D (see board layout). **+Ray Kholodovsky**???



[https://photos.app.goo.gl/UUYOKVtDLVSSHSKC2](https://photos.app.goo.gl/UUYOKVtDLVSSHSKC2)



Since removing it allowed the PWM to work something is "wrong" as that should not have made any difference. 



Since you have a disconnect (reset) problem I suspect a ground loop exists.



Here is how I recommend (and have mine wired). That is not to say that all grounding acts the same :(.



[https://photos.app.goo.gl/90j8PEhHhNU3PDHl1](https://photos.app.goo.gl/90j8PEhHhNU3PDHl1)



Also check that your LPS frame is screwed to the body of the K40.








---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2018 21:05*

The circled "GND" is indeed a Ground and the other circled one 2.5 is the output of the N FET which turns L on. 



I would concur with Don that something in the wiring is causing the reset issue. 


---
**Andy Shilling** *February 05, 2018 21:06*

I'm just sketching up my wiring  for you to see.




---
**Andy Shilling** *February 05, 2018 21:16*

Sorry for the crap drawing but hopefully you get the idea

![images/36a62c5098d8c426cf9b1186290af342.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/36a62c5098d8c426cf9b1186290af342.jpeg)


---
**Andy Shilling** *February 05, 2018 21:18*

Should I try this wiring? I'm not sure what high level light control or low level light control is.



![images/96c2d08c9644ed2d1e286e91d1a1b027.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96c2d08c9644ed2d1e286e91d1a1b027.jpeg)


---
**Don Kleinschnitz Jr.** *February 05, 2018 22:36*

**+Andy Shilling** high level light control is the same as TL but it is high true.

Let me look over this tonight, in shop now and I'll get back to you. 

In the mean time try and take some pictures that show both ends of working in the same photo. 


---
**Andy Shilling** *February 05, 2018 22:44*

👍 thanks **+Don Kleinschnitz** and **+Ray Kholodovsky** speak to you soon.


---
**Don Kleinschnitz Jr.** *February 06, 2018 00:18*

**+Andy Shilling** try running a wire from the 24v power supply ground (V-) to the frame.



I am assuming that E on your drawing it FG on the connector?



Where is the return from the lasers cathode connected? 


---
**Andy Shilling** *February 06, 2018 06:02*

Yes Don E is the chassis earth, this LPS has the high voltage and Cathode return seperate at the back of the unit. 



So what your saying is to run a new wire from V- to earth from the 24v PSU correct.


---
**Don Kleinschnitz Jr.** *February 06, 2018 12:30*

**+Andy Shilling** yes. If that does nothing then I would go around and measure (with power on) the voltage from the single point ground to all the other grounds and from ground to ground to see if there is voltage between any two ground points.

Try the above static and also while pulsing the laser test button. 




---
**Andy Shilling** *February 06, 2018 12:34*

**+Don Kleinschnitz** ok I will be home from work in 5/6 hours, I will check then and get back to you.


---
**Andy Shilling** *February 06, 2018 18:17*

**+Don Kleinschnitz** Ok I'm in with the machine, I have just checked the lps psu and c3d for ground and it seems when I check the 24v psu I have 0.04v going to ground but neither the lps or c3d do. I have tried replacing both the earth lead and running a new wire from V- to the chasis earth but this has made no difference to it.



The 24v psu is only 4 weeks old but am I right in saying it shouldn't be sending any voltage to ground no matter how small.


---
**Andy Shilling** *February 06, 2018 18:56*

New update I feel a fool but also very confused. I am running a small job on the laser to see if it does disconnect completely and thought I'd watch the usb ports in device manager. Well let me tell you it's not the bloody laser disconnecting it's my mouse whenever the laser is firing🤯🤯🤯. So if anyone could tell me why that happens I would be very greatful lol. 



I don't have another mouse to try at the moment but I am very happy that is not actually the laser having the problem.


---
**Don Kleinschnitz Jr.** *February 06, 2018 19:48*

I'm guessing it's your usb port. **+Ray Kholodovsky** didnt we have a problem with certain usb configs. 

**+Andy Shilling** do you have the usb powering the c3d?


---
**Andy Shilling** *February 06, 2018 20:08*

No it's not Don, I cut the usb power when I was using my Mac. The strange thing is my web cam keyboard and the flash drive in using atm don't seem to have this problem.


---
**Don Kleinschnitz Jr.** *February 07, 2018 13:42*

**+Andy Shilling** where and how EXACTLY are you measuring the .04V? 



To retract a bit. If you connect a ground from the C3D to the LPS like you originally had does the PWM still stop working?



My guess it that somehow you are injecting noise into your PC, or lifting its ground from the large energy dump in the laser. That is why USB things are disconnecting. 

I think this is a byproduct of some other grounding problem as signified by the fact you cannot connect rational grounding between the C3D and the LPS.



This still does not make much sense ....


---
**Andy Shilling** *February 07, 2018 14:11*

**+Don Kleinschnitz** right let me try and get this in the correct order. 

1. no laser fire on the first start up with C3D to Lps ground



2. Removed ground from C3D and lps, laser fires.



3. Check LPS ground to earth shoes 0v

Check C3D ground to earth shows 0v

Check PSU V- to earth shows 0.04v.



4. Run a job with C3D only connected to PSU V-,  job completes but with disconnection sounds throughout. 

At that point I realised it was the mouse disconnecting not the C3D.



5 I think I then rewired the C3D to the lps ground and tried again and this time it run ok but still with the disconnect.



6. To be continued because I have no idea what's happening now, I am in your control.


---
**Don Kleinschnitz Jr.** *February 07, 2018 14:16*

**+Andy Shilling** 

Checking #5 says that you now have the gnd from the C3D to the LPS and the laser fires ok, but the disconnect happens???






---
**Andy Shilling** *February 07, 2018 14:24*

**+Don Kleinschnitz** yes the ground at the lps is crimped in with the test switch so rather than cut the wire I just disconnected it from the C3D screw terminal and taped it up while testing your instructions. Once I found the 0.04v I automatically just untaped it and connected it back up and it worked?

I have no explanation to it but yes I still get the disconnect from the C3D usb.



 I have ordered a new shielded usb lead just to make sure that's not the problem.


---
**Andy Shilling** *February 07, 2018 14:25*

Would it be worth running an earth strap between the laser and pc chassis?


---
**Andy Shilling** *February 17, 2018 13:04*

**+Don Kleinschnitz** Sorry for the very late response on this thread, It seems that a new usb lead has stopped the connecting/reconnecting. Thank you once again for all your help.


---
**Don Kleinschnitz Jr.** *February 17, 2018 13:42*

**+Andy Shilling** thats "Shilling" news :), noise never ceases to surprise me.

Can you post what USB cable you bought?


---
**Andy Shilling** *February 17, 2018 13:50*

**+Don Kleinschnitz** I went with this one



Check this out: LINDY 2m CROMO USB 2.0 Type A to B Cable LINDY [amazon.co.uk - Robot Check](https://www.amazon.co.uk/dp/B007PKPVDO/ref=cm_sw_r_sms_awdb_t1_fkdIAb43P0335)


---
**Bill Keeter** *March 12, 2018 11:42*

**+Andy Shilling** Which wiring diagram did you end up following for this Laser PSU? Are you a 100% up and running now?



I'm almost finished building a 60w laser and I have the same terminals on the LPS. I was just about to use the first diagram with the Test fire btn set to TL to G. But wasn't sure which wire needed to go to the Cohesion3d. So if was reading along correctly, did you do Test fire as TH to 5V and wire TL to the C3D terminal P2.5?


---
**Andy Shilling** *March 12, 2018 12:14*

**+Bill Keeter** if you scroll back up the conversation you will see my really bad writing sketch. I don't think I changed it after that but could confirm it later when I get home from work if needed.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/SxpDYoqXibb) &mdash; content and formatting may not be reliable*
