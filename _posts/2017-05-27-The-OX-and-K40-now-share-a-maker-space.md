---
layout: post
title: "The OX and K40 now share a maker space ....."
date: May 27, 2017 13:49
category: "External links&#x3a; Blog, forum, etc"
author: "Don Kleinschnitz Jr."
---
The OX and K40 now share a maker space ..... in my shop.... not in my den. My wife is happier!



The K40 is down for cleanup improvements after the move....  

The OX needs to be wired up....



<b>Originally shared by Don Kleinschnitz Jr.</b>



Completed most of the DOX table build.

Now all my CNC builds are in one place, on mobile tables and down in the shop.



A main feature of this table is the integrated lift. I wanted to be able to move it easily, yet still be strong and rigid.

The integrated cams lifts the table 1/4-1/2" up on casters so that it can be rolled to a place where it is then lowered.

.....

Next, making space for the PC and then wiring!

....

Almost complete with the wiring harness (its a WIP): [http://donscncthings.blogspot.com/2017/05/wiring-tinyg.html](http://donscncthings.blogspot.com/2017/05/wiring-tinyg.html)

....

DOX table detail here:

[http://donscncthings.blogspot.com/2017/05/building-table-for-ox.html](http://donscncthings.blogspot.com/2017/05/building-table-for-ox.html)







![images/59902bd19c94e4e72ffcab57d26178f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/59902bd19c94e4e72ffcab57d26178f3.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-wixJ_pPEWGg/WSmAiCTRTDI/AAAAAAAAnf8/x470_jgKVqgfedtZI_06L08BoUdMC8hfACJoC/s0/20170526_114345.mp4**
![images/45f8636c41c521920d04422687dd0220.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/45f8636c41c521920d04422687dd0220.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Joe Alexander** *May 27, 2017 18:00*

looks good, and nifty table wheel setup!


---
**Ulf Stahmer** *May 27, 2017 18:51*

Seems like our wives have similarities. Mine didn't like the K40 on the coffee table next to the fireplace either :)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/QdxJrngS9EJ) &mdash; content and formatting may not be reliable*
