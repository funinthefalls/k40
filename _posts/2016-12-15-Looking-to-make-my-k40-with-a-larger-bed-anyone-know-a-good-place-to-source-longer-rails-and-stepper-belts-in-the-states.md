---
layout: post
title: "Looking to make my k40 with a larger bed, anyone know a good place to source longer rails and stepper belts in the states?"
date: December 15, 2016 17:31
category: "Modification"
author: "Kelly S"
---
Looking to make my k40 with a larger bed, anyone know a good place to source longer rails and stepper belts in the states?  :)  Thank you.





**"Kelly S"**

---
---
**Kelly S** *December 15, 2016 17:35*

That looks like a great place to shop at, thank you!  Now to come up with some plans. :o


---
**Kelly S** *December 15, 2016 17:36*

K40 is great for what I do, mostly 2.5mm acrylic, only drawback is bed size.  But can easily relocate the electronics and extend it over.  Next year may just build a cutter from scratch.  :)


---
**Ned Hill** *December 15, 2016 18:01*

Here's **+Ariel Yahni**'s build blog over at   openbuilds [openbuilds.com - UK40OB](http://www.openbuilds.com/builds/uk40ob.3988/)


---
**Ariel Yahni (UniKpty)** *December 15, 2016 18:08*

Guys just note I would / want to do many mods to this and that it was a desicion to use the casting itself as the lateral support. 


---
**Kelly S** *December 15, 2016 18:40*

Wow thanks for all the feed back guys, I love this group.  :D 


---
**Kelly S** *December 15, 2016 18:45*

**+Ariel Yahni** How is your cutter sitting currently?  I was thinking of using the 8x8 lab jack under the machine (cut a hole of course) to make the entire larger bed area easy to adjust.  Did you use the original steppers? If so, know the tooth type the belt requires?  Will be putting together a shopping list later and ordering tonight I think as I finished my Christmas projects and did a lot of wood and entire machine needs a good tear down and clean lol. 


---
**Ariel Yahni (UniKpty)** *December 15, 2016 19:03*

No bed yet, but the idea was to have it motorized. Stepper are openbuilds. The link above has the parts from the store I beleive. Still lots o changes I want to do


---
**Kelly S** *December 15, 2016 19:10*

Okay **+Ariel Yahni** I am following your parts list on the V-Slot Linear Rail 20mm x 40mm how long of a rail was required before cutting? 


---
**Ariel Yahni (UniKpty)** *December 15, 2016 19:19*

Y movement is restricted to bellow 500mm. I


---
**Kelly S** *December 15, 2016 19:24*

Appears they are out of stock of the stepper you used.  :(  And wow them screws are expensive lol.


---
**Kelly S** *December 15, 2016 19:25*

never mind seen they are packs of 25, I put 50 of them in lol


---
**Ariel Yahni (UniKpty)** *December 15, 2016 19:36*

I will recommend that you build a complete gantry inside and not use the machine sides, similar to this


---
**Kelly S** *December 15, 2016 20:05*

Another question.  Anyone know the proper way to discharge the PSU?  I looked around and did not see anything useful. But plan to relocate it to a box outside the machine with the controls also moved and use that door as an extended window.  


---
**Kelly S** *December 15, 2016 21:25*

Do you still happen to have the files for the parts you cut, that I may be able to get?  Then I can make them before I disassemble it lol.**+Ariel Yahni**


---
**Ariel Yahni (UniKpty)** *December 15, 2016 21:30*

**+Kelly S**​ don't laugh lol :). Please check the openbuilds page in sure I posted the sketchup file, I don't Have the here with me


---
**Kelly S** *December 15, 2016 21:37*

I will have to source a program to open the skp.  I use fusion 360.


---
**Kelly S** *December 15, 2016 21:39*

Think I got it, had to upload 




---
**Ariel Yahni (UniKpty)** *December 15, 2016 21:40*

Me to lol but I have found the Fusion is to slow for me when dealing with vectors only. I can export that into svg so you can open it in fusion


---
**Kelly S** *December 15, 2016 21:43*

That would be awesome if you could.  I did get it open, but the faces are very odd.  


---
**Kelly S** *December 17, 2016 16:15*

Now I am debating just using the guts and building a whole new machine.  Would the Lil k40 tube be worth it?  


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/WbYiQZp2kZm) &mdash; content and formatting may not be reliable*
