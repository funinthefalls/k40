---
layout: post
title: "A SOFTWARE nightmare, it all started over the weekend when I tried to install Whisperer to cut a project that another kind member her made for me"
date: October 17, 2017 01:06
category: "Discussion"
author: "Padilla's custom leather"
---
A SOFTWARE nightmare,

it all started over the weekend when I tried to install Whisperer to cut a project that another kind member her made for me.  It was a complete failure when it came to installing whisperer, I will say that it could have been me when I attempted the install but followed all of the steps only to have the software not work.  I then went through all of the steps to remove whisperer and attempt to get the stock software to work only to run in to more problems,  I have removed all of the software and going to the root file on the c drive to make sure all files had been removed after doing the proper steps to remove the software.  after doing a fresh install of the stock software all 3 then ensuring that they are all in the proper place I have gone on to try send a file to either engrave or cut and I am now getting the window that pops up that indicates that the dongle is not installed on the computer.  I have done restarts to make sure that the computer see's all of the software, and hardware properly, the window also pops up indicating that the machine has been recognized and initialized and I also checked to make sure that the board serial number has been typed back in.  I have also gone to the software writers website to look for anything that would help, but unfortunately I don't read Chinese, so there has been no help there.  I hope that I have covered all the bases that would need to be covered to describe the issue.  Oh buy the way now after doing a reload of all the software only one of the 3 programs will open, previously I could open the program that had all the samples, but now it wont open, also when I open the main program and go to the cut or engrave window it will let me initialize the machine and the cutting head moves, however when I go to send everything to cut or engrave that is when I get the message that the dongle is not connected.  If you read any of the post with the help from the other member you will be able to glean a little of the frustration.  I hope that someone can help I don't want it to come across that whisperer is not a good way to go, for most it might be for me well, !! 





**"Padilla's custom leather"**

---
---
**Scorch Works** *October 17, 2017 02:15*

Here is something to try:

1. insert the usbkey into the computer

2. go back to the "Windows libUSB Driver Removal" instructions.

3. Follow the instructions to step #3.

4.  Do you see anything under "libusb-win32"?  There may not be a entry for "libusb-win32".  If there isn't an entry you can stop here.

5. If there is something there you probably need to remove it using the remaining steps in the instructions. (unless you specifically installed something else.)

![images/7cd957f36d06633655e0c43a629f8289.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7cd957f36d06633655e0c43a629f8289.png)


---
**Padilla's custom leather** *October 17, 2017 02:48*

i an in front of the computer now I havw 2 computers running so that I can read what you have posted and do the operation on the other system I am looking at the screen and what it showes under that setting is the following intel wireless bluetooth but nothing else.  should that be there?


---
**Scorch Works** *October 17, 2017 03:02*

I can't know for sure because I don't know what other software/hardware you have installed.  If you unplug the USB key does the bluetooth item go away? (this is a bit of a long shot)


---
**Padilla's custom leather** *October 17, 2017 03:13*

when I pulled the dongle the other item is still there the intel wireless bluetooth is still there




---
**Padilla's custom leather** *October 17, 2017 03:15*

when I plug the dongle in it shows the lib-win32 devices shows back up


---
**Scorch Works** *October 17, 2017 03:20*

Do new devices show up under Human interface Device when you plug in the key?



It might look different under a different version of windows. what Windows are you using?

![images/4f75737dfb606b777b5526df13c0b061.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4f75737dfb606b777b5526df13c0b061.png)


---
**Scorch Works** *October 17, 2017 03:24*

I didn't see this before my last post 

"when I plug the dongle in it shows the lib-win32 devices shows back up."

what goes away when you pull the dongle?


---
**Padilla's custom leather** *October 17, 2017 03:40*

when pull it nothing goes away


---
**Padilla's custom leather** *October 17, 2017 03:51*

I noticed your icons on your board are different than how they show on my screen under the human interface icon a usb device ahows up there when the dongle is plugged in when I remove the dongle does go away


---
**Scorch Works** *October 17, 2017 03:58*

That is probably just a difference in the version of windows.



It sounds like all of the K40 Whisperer stuff is successfully removed. I am not sure why LaserDRW is giving you trouble.  You mentioned that you removed and reinstalled LaserDRW.  That is the best course of action I can think of.



Make sure when you remove the software you do it with the "Programs and Features" window where you can select a program and choose uninstall.  I seem to recall someone else having a problem with the dongle I will see if I can find the reference on the google group (I don't think they were using K40 whisperer)  If I find it I will link to it tomorrow.


---
**Padilla's custom leather** *October 17, 2017 04:08*

most likely the most resent version of windows 10 I bought this computer in August for the sole purpose to run the laser


---
**Padilla's custom leather** *October 17, 2017 04:11*

that sounds good I am starting to get a little cross eyed looking at this, I cant check back till after noon my time


---
**Printin Addiction** *October 17, 2017 09:56*

**+Padilla's custom leather** Make sure to check the scaling / shape if you are going to use the file I posted, I just eye-balled the shape, but didnt touch the defaulted scale, so it may be huge......If you send me the dimensions I can edit the file to make it more accurate.


---
**Scorch Works** *October 17, 2017 11:32*

**+Padilla's custom leather** I could not find any examples of the same issue you are seeing.  I don't have any additional ideas for your computer other than reinstalling LaserDRW again.  


---
**Padilla's custom leather** *October 17, 2017 15:24*

I will go through the steps again later today and post screen shots of what is appearing in the event I reinstall and face the same issues, I hope someone reads this that might have a solution, or I guess I hold off till I can afford the board from the guys in Australia. 


---
**Printin Addiction** *October 17, 2017 16:22*

What about going back to K40 Whisperer, the warning that keeps popping up is called SmartScreen and that can be disabled in control panel. 



I have found K40 very easy to use and the fact that you can raster engrave, vector engrave and vector cut from the same input file using different colors versus layers makes it a time saver as well. (You can see what it will do without stepping through layers) 


---
**Padilla's custom leather** *October 18, 2017 17:47*

I haven't had the chance to get back to working on that in the last day and a half with high hopes I can work on it today but I need to work on some leather orders first but am looking forward to resolving this


---
**Padilla's custom leather** *October 18, 2017 22:46*

Printing addiction, I did get back out today to work on the software for a couple of hours, I tried to load the programs again and thought I has some success, I will post photos at the end,  the computer let me do 2 functions and then I got the dreaded pop up window that indicated that the dongle was not plugged in, I went back and closed the program again and might have even done another restart, with the same results so I think at this point I am going to hold off till I can get the board from Paul De Groot and plug ins for Ink Scape I think in the long run it will save me a lot of frustration and it will also give me time to take the pointers you gave me on how to create the items that you created for me, and I do thank you for that.  I asked me son if he would mind holding off for just a little while and he is good with that.  Following are the screen shots and photo of the test I ran of your creation on paper, and again THANKS for ALL the help.  And I guess that it will only let me do one image at a time.

![images/f3f2dafc4d1a4906b19be217a7a0b6c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3f2dafc4d1a4906b19be217a7a0b6c4.jpeg)


---
**Padilla's custom leather** *October 18, 2017 22:48*

**+Padilla's custom leather** 

![images/702baa298567bb2c689645de8623dbc1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/702baa298567bb2c689645de8623dbc1.jpeg)


---
**Padilla's custom leather** *October 18, 2017 22:49*

![images/2b79e0051ef89cb2594d590636404fe4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b79e0051ef89cb2594d590636404fe4.jpeg)


---
**Padilla's custom leather** *October 18, 2017 22:51*

this is the result when sent to either cut or engrave

![images/e0a14ae4809d5e7a5e28be16364ed8c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e0a14ae4809d5e7a5e28be16364ed8c4.jpeg)


---
**Scorch Works** *October 18, 2017 23:05*

Those pictures show Winseal not LaserDRW.  I am not sure but I don't think all of the USB keys work for Winseal.  If you have the gold color USB key one you might only be able to use LaserDRW.



Here is a link to a discussion with a picture of the cold one.

[plus.google.com - Hi, I have a big problem with the software WinsealXP and LaserDRW When they s...](https://plus.google.com/100341006317202452626/posts/EnUkYgNwaKx)


---
**Padilla's custom leather** *October 19, 2017 01:14*

I did only load 2 of the programs and one worked maybe when I went back to try again I selected the other I will try again tomorrow to see if one versus the other works.


---
*Imported from [Google+](https://plus.google.com/106971026516976154588/posts/GtpNFYehJ64) &mdash; content and formatting may not be reliable*
