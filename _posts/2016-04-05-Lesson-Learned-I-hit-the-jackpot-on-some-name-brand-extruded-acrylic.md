---
layout: post
title: "Lesson Learned I hit the jackpot on some name brand extruded acrylic"
date: April 05, 2016 02:14
category: "Discussion"
author: "HalfNormal"
---
Lesson Learned



I hit the jackpot on some name brand extruded acrylic. I live next to a large university that has a surplus shop available to the public. I found 12 sheets, 3 mm thick, 18 x 24 inches for a total of $20!  Anyway I started to cut them and was surprised that the settings I was using on thicker acrylic was not working. I need to mention that I had left the protective paper on the acrylic as not to scorch it. Increasing the power and slowing the speed did not do much good. Multiple cuts no go.  I removed said protective paper and it cut like butter! I am not sure what the paper is made of but it sure messes with the process. Took a few cut to figure it out.





**"HalfNormal"**

---
---
**Alex Krause** *April 05, 2016 02:52*

More than likely a UV protection film to keep it from discoloring and scattering your beam


---
**ThantiK** *April 05, 2016 03:46*

**+Alex Krause**, the K40 is a C02 laser.  Unlike the other lasers producing visible or short near-IR light, the output of a CO2 laser is medium-IR radiation at 10.6 um.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/NR1XQnpmyys) &mdash; content and formatting may not be reliable*
