---
layout: post
title: "With the stock K40 is it just Laserdrw and coreldraw that you can use"
date: April 23, 2016 08:18
category: "Discussion"
author: "Tony Schelts"
---
With the stock K40 is it just Laserdrw and coreldraw that you can use.

I have been using a dodgy copy of Coreldraw and cant afford a legit copy.  Confession going on here.  What are other people using is simply upgrade the board etc.

 





**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 23, 2016 08:46*

I'm in the same boat, still using the dodgy CorelDraw that came with the K40. I'm fairly certain there is no other software that works with the stock K40. In order to use other software, you need to swap out the controller board for something else (e.g. Smoothie). It is my plan to do that swap (when I have saved enough to get a smoothie) so that I can use Peter's LaserWeb software.


---
**Phillip Conroy** *April 23, 2016 08:51*

I am also just using coreldraw and thevplug in,i do not etch only cut and for what i need stock board is fine .the stock gear has no control of power so you cant etch somethi g and cut in the same job or vary the power when etching .i cut embeshments out for wifes scrapbooking busnies 


---
**Scott Thorne** *April 23, 2016 12:49*

**+Tony Schelts**...I've purchased corel draw x7 student on amazon for 45.00.


---
**Scott Thorne** *April 23, 2016 12:51*

I just checked..it's 54.99 on amazon now


---
**Mike Mauter** *April 23, 2016 13:18*

I thought that the student version required a student email for verification? Also does the Lawerdraw plugun work with the Student Version? I thought that I had seen that it doesn't.


---
**Scott Thorne** *April 23, 2016 13:38*

I don't know about that. **+Mike Mauter**...but it doesn't require a student email address...I don't have a k40...I've got a 50 watt machine with dsp.


---
**andy creighton** *May 06, 2016 00:36*

I use autocad and save as a dxf and things work great.  I would also recommend Librecad as a great own source option over autocad. 



There is also a YouTube video out there that demonstrates how to use layers to etch and cut in one setup and it is really easy. 


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/BWnPa3qPJ2f) &mdash; content and formatting may not be reliable*
