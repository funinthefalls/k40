---
layout: post
title: "My edited version of the LaserSafetySystem firmware by Anthony Bolgar This is set up for the Dallas DS18B20 digital temperature sensor - a sensor you can find on ebay for like $4 with a 3ft cord that's already waterproofed"
date: April 10, 2016 03:17
category: "Modification"
author: "ThantiK"
---
My edited version of the LaserSafetySystem firmware by Anthony Bolgar



This is set up for the Dallas DS18B20 digital temperature sensor - a sensor you can find on ebay for like $4 with a 3ft cord that's already waterproofed and everything.  I also moved some of the pinouts due to the DallasTemperature library already using D2 (and not wanting to screw around with changing it)



[https://drive.google.com/open?id=0ByxvvRr2A-puVjd1cWJDVEJxVTA](https://drive.google.com/open?id=0ByxvvRr2A-puVjd1cWJDVEJxVTA)





**"ThantiK"**

---
---
**Anthony Bolgar** *April 11, 2016 22:38*

Do you mind if I upload this to the repo (Of course I will give you credit for your work on upgrading it!)?


---
**ThantiK** *April 11, 2016 22:56*

That's fine - these use standard libraries except for the modification to the write function of the lcd library.  I want to do more work on it so I can set flags and swap between them.  Maybe put it as a different branch?


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/4gUF91bjNuP) &mdash; content and formatting may not be reliable*
