---
layout: post
title: "I was glad to find you guys n gals, I want to ask a question here that I also asked somewhere also"
date: August 23, 2017 17:38
category: "Modification"
author: "Padilla's custom leather"
---
I was glad to find you guys n gals, I want to ask a question here that I also asked somewhere also.  I have the K40 it arrived about 1 1/2 weeks ago but have not fired it yet as I knew that I would be making mods to it before using it.  I have already installed a 3d printed air nozzle and now need to move the cutting table and that is going to be replaced with an expanded metal, I understand that the table cannot go more than 2 inches below the bottom of the nozzle what have some of you found to be a good way of remounting the table at the correct height?  





**"Padilla's custom leather"**

---
---
**Martin Dillon** *August 23, 2017 18:01*

most people make/buy some sort of adjustable table.  Here is a picture of mine.  I then mounted a piece of honeycomb on top of the scissor table.  You will find many other examples in the forum if you search.

![images/a7080870bb6e20849a4119f7b0c7a235.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a7080870bb6e20849a4119f7b0c7a235.jpeg)


---
**Martin Dillon** *August 23, 2017 18:09*

I just searched the community for "table" and came up with many examples.  I really like **+Ulf Stahmer** table design for simplicity 


---
**Steve Clark** *August 25, 2017 16:05*

There is a company that makes them called LightObject. They have good and bad points but if installed correctly work well.  Do a search here and on google or other for more info on like and dislikes.



[lightobject.com - Power Z Table/ Bed Kit for K40 Small Laser Machine](http://www.lightobject.com/Power-Z-Table-Bed-Kit-for-K40-Small-Laser-Machine-P722.aspx)


---
**Steve Clark** *August 25, 2017 16:14*

As added note be sure to properly ground your new machine as there are some really high voltages being produced by these machines. Some of the manufacturers are sloppy in this respect  and a machine that is not earth grounded properly could actually kill you. Better safe than  sorry.



This video gets the idea across.




{% include youtubePlayer.html id="6vztMvWUnmM" %}
[youtube.com - K40 eBay Chinese CO2 Laser - Electrical Warning This Can Kill You!](https://www.youtube.com/watch?v=6vztMvWUnmM)


---
**Padilla's custom leather** *August 25, 2017 20:58*

I do plan on doing that I have taken some jumper cables out of an road bag we have and cut the clamps off one side and going to use those for a ground to incoming water pipe in the garage.  I think that should ground it


---
**Padilla's custom leather** *August 27, 2017 06:46*

one other question I have is, I am going to changing the nozzle, at 1st I thought that I would get and in fact did get one of the 3d printed cups that slide over the nozzle and now I am thinkin I should have held off for the one of the air nozzles from light Object,  but would someone please clarify for me a question I have. In addition to buying the complete air nozzle is there a need to also buy additional lens and mirror in order for it to work properly.  I would like to just buy everything in one shot vs having to go back and order additional items need after the fact.  Thanks in advance for the info.


---
**Martin Dillon** *August 27, 2017 15:39*

If you buy the LO air assist, I would buy and new lens that fits.  The stock one is small and doesn't fit well in the nozzle.  Many people make a ring to center the stock ring and that works.  If you can, I would buy a new lens because you can bet that the one that came with the K40 is the cheapest possible lens available.  I have heard that people have gotten noticeable improvement with a new lens.


---
**Padilla's custom leather** *August 27, 2017 18:56*

ok thanks is it safe to say that the people at LO would know what would work the best or is that to much of a presumption?


---
**Steve Clark** *August 28, 2017 16:02*

Yes, They are knowledgeable. I would suggest looking up and understanding "focal lenght"  



[support.epiloglaser.com - Focus Lens 101](http://support.epiloglaser.com/article/8205/42831/focus-lens-101)



Most housing and or lens changes will effect your focal length so you nee to make bed height changes. 



Some very slight changes in the height of the lens housing may also be needed to to realign the beam. 


---
*Imported from [Google+](https://plus.google.com/106971026516976154588/posts/coYmv3Dv7mH) &mdash; content and formatting may not be reliable*
