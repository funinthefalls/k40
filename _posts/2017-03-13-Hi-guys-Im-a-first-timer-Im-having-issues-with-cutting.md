---
layout: post
title: "Hi guys I'm a first timer. I'm having issues with cutting"
date: March 13, 2017 01:21
category: "Hardware and Laser settings"
author: "Chris Hurley"
---
Hi guys I'm a first timer. I'm having issues with cutting. First a little history, I have had to replace the tube in this machine as the first one was broken in shipping. I also noticed that one of the screws was bent. I replaced it with a tube from ebay (china). What I want to cut is 3mm mdf for wargaming. I've spent weeks trying to get the machine to cut like I've seen on youtube. I'm in the 10+ hours getting the mirrors right (I think they are right). Is there anything you guys can suggest?





**"Chris Hurley"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 13, 2017 01:39*

Not sure what you've seen on youtube, but from my experience 3mm MDF should cut reasonably easy. I used approximately 10mA power, 10-20mm/s.



One thing you might want to check is your lens orientation. Mine arrived upside-down. You want it so the concave side is down, reflective side facing up.



Also, having air-assist significantly helps with cutting ability.


---
**Jim Hatch** *March 13, 2017 01:44*

The 3 things that have to be right are Alignment, Lens and Focal distance.



If you've got all the mirrors aligned to their centers and you're getting a dot, not a crescent on your material then go to step 2.



Make sure the lens is properly oriented with the bump facing upwards. 



Then make sure the material is 50.8mm from the bottom of the lens. Measure to the lens not to the bottom of the head - sometimes there's a fair amount of distance between the lens & bottom of the head.



That should be all you need (& power at 18ma and speed of 10mm/s for 3mm Birch cuts).


---
**Chris Hurley** *March 13, 2017 01:45*

Well I may have found the issue. The end came off. Now to battle the Chinese ebay guys. What the best tubes for this machine?  (update they are asking me to glue it back on with glass glue.....should I?)



![images/f4a483c606a371a378de01023a83c682.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4a483c606a371a378de01023a83c682.jpeg)


---
**Chris Hurley** *March 13, 2017 01:51*

**+Jim Hatch** I was getting Crescent right from the tube on and before mirror one. Does this mean I have a issue? Actually it's more of a line. But it is static as the beam hits each mirror. 




---
**Chris Hurley** *March 13, 2017 02:27*

This is the marks from the first mirror (before the end came off) 

![images/fc35ce3b118bc19a2ac90875bc50bf8e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fc35ce3b118bc19a2ac90875bc50bf8e.jpeg)


---
**1981therealfury** *March 13, 2017 08:42*

I'm no expert, but I'm pretty sure that this is not what the beam should look like as it comes out of the tube.  On both mine and my friends K40 the beam on the primary mirror is a perfect circle of about 5-6mm diameter.  If you get the power just right it should burn a ring onto the paper.



As for the tube end, personally i wouldn't accept that and i wouldn't try and fix it myself unless they were going to give me a full refund regardless.


---
**Chris Hurley** *March 15, 2017 02:57*

well I fixed it but the beam still splits right from the tube. 


---
**Chris Hurley** *March 15, 2017 03:21*

the Chinese manufacture is sending a new one.  


---
**1981therealfury** *March 15, 2017 07:38*

Well that's something at least then, hopefully the new one will work correctly when it arrives.  That's the main problem with Chinese manufactured and bought goods, they just don't have the Quality Control that we do so its a bit of a gamble sometimes.


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/1Dd14GnYBLM) &mdash; content and formatting may not be reliable*
