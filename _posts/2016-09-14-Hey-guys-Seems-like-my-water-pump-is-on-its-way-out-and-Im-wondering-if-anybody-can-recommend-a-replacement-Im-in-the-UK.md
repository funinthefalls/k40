---
layout: post
title: "Hey guys, Seems like my water pump is on it's way out and I'm wondering if anybody can recommend a replacement (I'm in the UK)"
date: September 14, 2016 00:28
category: "Discussion"
author: "Adam J"
---
Hey guys,



Seems like my water pump is on it's way out and I'm wondering if anybody can recommend a replacement (I'm in the UK). I hear too powerful flow pumps are not a good idea. There's a bunch of pumps on eBay of course, but I wonder if their performance figures are actually accurate?





**"Adam J"**

---
---
**Scott Marshall** *September 14, 2016 00:56*

Little Giant 140gph (PE-1or PE-140) is sized about right and very high quality.



They are commonly used in parts washers and fountains, and run forever. You can even get rebuild kits. 



Available worldwide, cost here is about $60us



You are right about going too big, too much pressure can blow off tubing and stress the tube - although I'm not sure if you could actually break the tube before blowing the tubing off, why push the issue?



The Little Giant is about twice the power of the stocker, and that's really just about right. Enough flow to flush out the bubbles, but it has a shut off head of about 12' so can't damage your tube.



Another thing about going too big is remember your water has to cool the pump as well as the Laser tube. Every watt you put into your water tank has to go somewhere.

A very large pump generates excessive heat from cooling it's motor and thru fluid friction if you have to valve down or bypass the output to get the flow down to a Laser-safe level.


---
**Adam J** *September 14, 2016 15:36*

Thanks for the reply Scott. I will look into sourcing one.



I've been searching through the forum regarding the use of distilled water. It seems to have started vanishing from our supermarket shelves some time ago (in the UK). De-ionsed water is available from automotive stores but will this be good enough? I know the processing is different but is the final product the same?



I've ordered some alcohol to add to the mix, thanks to yours and others recommendation. I'm also wondering how often you guys change the water? I'm guessing the alcohol will slow it from going slimey / stale?



I live in a limestone rich area, (surrounded by disused limestone quarries!) the water is hard and harsh, so I think it's about time I addressed this rather than using water straight from the tap!


---
**Adam J** *September 14, 2016 16:03*

So, I should add I've found an aquatics place near me selling RO water cheaply. Should I go for this? I read it's essentially distilled water?


---
**Dan Stuettgen** *September 14, 2016 17:32*

I thought you are supposed to use Distilled Water to cool the laser tube.  If you do you should not have any issues with hard water.  Keeping a closed system with distilled water, should eliminate any issues with water gunking up to.


---
**Scott Marshall** *September 14, 2016 18:46*

Most "distilled water" sold in stores is RO (reverse osmosis) water. It's essentially the same thing produced by a different (more energy efficient) process.



RO water is pumped through a membrane with pores so small only the water molecule fits, it leaves the hardness (carbonate ions) behind.



De-ionized water get run through a mixed bed of anion and cation resins, stripping away any remaining non-H2o molecules. It's only necessary for sensitive chemical work, semiconductor wafer manufacture and that kind of stuff. It actually "goes bad" with age (draws contaminates which increase it's conductivity -  DI water is judged by it's lack of conductiveness - 100% pure water has a resistance of over 18 megohms per cc)



(I used to build industrial water treatment equipment)



If you have good quality soft tap water, it works fine. It's all I use.

(And I actually HAVE an RO, it just not worth putting a membrane in it for laser water)



A touch of Chlorine bleach (avoid the non-splash and scented stuff, you want just sodium hypochlorite diluted in water) or alcohol will keep the slime at bay.



The bulk RO water from the aquatics shop is fine.

If you can get a friends tap (city) water, that's fine - and cheaper. 



Scott


---
**Adam J** *September 14, 2016 23:32*

Thanks again for the advice guys, I love this group!


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/EZHZKe7h8tc) &mdash; content and formatting may not be reliable*
