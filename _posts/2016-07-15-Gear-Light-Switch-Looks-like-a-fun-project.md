---
layout: post
title: "Gear Light Switch Looks like a fun project!"
date: July 15, 2016 03:18
category: "Object produced with laser"
author: "HalfNormal"
---
Gear Light Switch



Looks like a fun project!




{% include youtubePlayer.html id="mNutmTOFNR4" %}
[https://www.youtube.com/watch?v=mNutmTOFNR4](https://www.youtube.com/watch?v=mNutmTOFNR4)





**"HalfNormal"**

---
---
**Phillip Conroy** *July 15, 2016 04:46*

Nice


---
**Pippins McGee** *July 15, 2016 06:22*

can you post .cdr file please


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 15, 2016 07:43*

That's pretty cool, although slightly impractical haha.


---
**Jon Bruno** *July 15, 2016 15:44*

they've got to work on the gear ratio.. two cranks at least to change the switch state. Nifty, But I'd break it trying to get some crank satisfaction.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/17x6SC5JmTJ) &mdash; content and formatting may not be reliable*
