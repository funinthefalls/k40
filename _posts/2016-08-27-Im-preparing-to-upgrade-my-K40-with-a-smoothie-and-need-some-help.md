---
layout: post
title: "I'm preparing to upgrade my K40 with a smoothie and need some help"
date: August 27, 2016 12:49
category: "Smoothieboard Modification"
author: "Ulf Stahmer"
---
I'm preparing to upgrade my K40 with a smoothie and need some help.  From left to right, my power supply has the following connections:



L- FG AC AC     P+ G     K- K+     G IN 5V     24V G 5V L



My question is about the middle connector that has the pinouts K- and K+.  I'm assuming that K+ is the laser fire and laser test line and K- is the ground.  If this is the case, why is it labeled K- and not G?  Or am I missing something here?

![images/1c90e5b6fc0c65bdfe45bd807909197d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c90e5b6fc0c65bdfe45bd807909197d.jpeg)



**"Ulf Stahmer"**

---
---
**Ariel Yahni (UniKpty)** *August 27, 2016 13:28*

Are you going to connect something on those?


---
**Ulf Stahmer** *August 27, 2016 13:44*

According to [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide) the  k terminals on my psu look to be the same as the L terminal, and it is used for the laser fire, so I thought that's what you connect there.


---
**Vince Lee** *August 27, 2016 13:56*

Power supplies can vary but stock controller boards generally do not.  You can identify everything using your original wiring harness or do what I did and make a universal adapter that the harness plugs into.


---
**Ariel Yahni (UniKpty)** *August 27, 2016 14:02*

**+Ulf Stahmer** I would go with the easier L which is on your far right, most new conversions are using it


---
**Ulf Stahmer** *August 27, 2016 14:34*

Thanks **+Ariel Yahni**, I'll give that a go.  I'm drawing a wiring diagram first to make sure it all makes sense.



Not being too familiar with LaserWeb3 yet, is there a test fire button?  Or will I still be able to test fire with the stock control?


---
**Ariel Yahni (UniKpty)** *August 27, 2016 14:40*

As you are not touching the test button it will still operate. In LW you can create a custom button for such also. Look at the wiki at [laserweb.xyz](http://laserweb.xyz) for detail


---
**Ulf Stahmer** *August 27, 2016 15:03*

OK.  Thanks **+Ariel Yahni**,  that makes sense.  The L terminal on the 24V connector (right hand side of psu) is connected to my stock main controller board.



One other question, as stock, the IN terminal is connected to the controller board on the lid (presumably, it is used to adjust the laser power level - I have a digital control and display, not a pot).  Do I disconnect the stock IN wire, or leave it in and add a new wire from IN to my level shifter?  Or do I hook them up in series some other way?


---
**Ariel Yahni (UniKpty)** *August 27, 2016 15:08*

The reason we are choosing to use that instead of messing with anything else is exactly that, that's the only connection  you actually have to the stock board. I would no touch anything else and keep that the same as the pot, as the ceiling of power. You would be the first I know to have thay


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/5o2sFqxrZPz) &mdash; content and formatting may not be reliable*
