---
layout: post
title: "Is there a minimum distance between the anode end of the laser tube and the first mirror?"
date: August 06, 2016 18:36
category: "Discussion"
author: "Brandon Smith"
---
Is there a minimum distance between the anode end of the laser tube and the first mirror? I received a replacement Puri 50w tube for my k50, and I am getting it swapped in right now. I figured as long as there is enough room for me to get a slip of paper in to align, that it could be as close as I want. Either way I will have to put together an extension as the new tube is much longer than the crappy tube it came with. Any thoughts would be greatly appreciated.





**"Brandon Smith"**

---
---
**Phillip Conroy** *August 06, 2016 19:18*

If it was mine i would be going for a gap off at least 5mm to 20mm


---
**Scott Marshall** *August 06, 2016 21:34*

You can put them as close as needed as long as there's room to adjust the mirror.



The raw beam doesn't have the watts/sq cm the focused one does and is relatively tolerant of minor imperfections and such, especially at only 30w. 



Things get touchier with high wattage lasers where water cooled mirrors, beam expanders, enclosed beamways and that sort of thing are necessary to keep the machine from lasing itself to death. 



Shifting mirrors on the K40 won't hurt a thing as long as the beam gets where it's supposed to and doesn't hit anything it's NOT supposed to.



Scott


---
**Brandon Smith** *August 06, 2016 22:22*

Thanks for the input guys. I put it about 1" from first mirror and that seems to work fine. I aligned it as best as I could for now until I get some tube brackets that have adjustment built in. Then I can really get the tube square and have a nice starting point to align from.


---
*Imported from [Google+](https://plus.google.com/109949255412543943799/posts/CxpVN5ift8k) &mdash; content and formatting may not be reliable*
