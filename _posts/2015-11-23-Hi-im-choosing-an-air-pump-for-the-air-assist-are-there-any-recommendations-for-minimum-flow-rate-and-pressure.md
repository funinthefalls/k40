---
layout: post
title: "Hi, i'm choosing an air pump for the air-assist, are there any recommendations for minimum flow-rate and pressure ?"
date: November 23, 2015 15:05
category: "Modification"
author: "Gary McKinnon"
---
Hi, i'm choosing an air pump for the air-assist, are there any recommendations for minimum flow-rate and pressure ?





hanks.







**"Gary McKinnon"**

---
---
**Scott Thorne** *November 23, 2015 19:16*

I bought a airbrush compressor and it was a waste of money, I'm now using my big compressor and running it at 25 psi, works great.


---
**Gary McKinnon** *November 23, 2015 20:03*

Thanks Scott, what's the flow rate on your unit ?


---
**Scott Thorne** *November 23, 2015 20:04*

I have no idea what the flow rate is.


---
**Scott Thorne** *November 23, 2015 20:05*

Just a 40 gallon shop compressor. 1/2 horse motor.


---
**Gary McKinnon** *November 23, 2015 20:42*

Okay, thanks.


---
**Phillip Conroy** *November 23, 2015 21:58*

I am using a 1/2 hp shop air compressor with adjustable regulator with built in water trap-otherwise u will get water coming through the air line sometimes


---
**Gary McKinnon** *November 24, 2015 12:54*

Thanks for info all :)


---
*Imported from [Google+](https://plus.google.com/+GaryMcKinnonUFO/posts/FEUWE1oMwxh) &mdash; content and formatting may not be reliable*
