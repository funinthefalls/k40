---
layout: post
title: "Salve a tutti mi occorre una mano per collegare la scheda SMOOTHIEBOARD x4 al mio laser k40...."
date: December 12, 2018 00:55
category: "Modification"
author: "gionata cannavo"
---
Salve a tutti mi occorre una mano per collegare la scheda SMOOTHIEBOARD x4 al mio laser k40.... Ho seguito diverse guide senza nessun risultato, chi mi può aiutare? Vi metto le foto dell'alimentatore e dello schema.

Hi all I need a hand to connect the SMOOTHIEBOARD x4 card to my laser k40 .... I followed several guides with no results, who can help me? I put the photos of the feeder and the scheme.



![images/80822bf0a98c7688ebf3eaa9b535bfe9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/80822bf0a98c7688ebf3eaa9b535bfe9.jpeg)
![images/6b643e426b47cb6048332f9584aa6167.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b643e426b47cb6048332f9584aa6167.jpeg)

**"gionata cannavo"**

---
---
**Don Kleinschnitz Jr.** *December 12, 2018 04:30*

You can find info on this site see if it helps: 

[http://donsthings.blogspot.com/](http://donsthings.blogspot.com/)

[donsthings.blogspot.com - Don's Laser Cutter Things](http://donsthings.blogspot.com/)


---
**Joe Alexander** *December 12, 2018 06:17*

Credit to Don for this photo from his blog, a great and recommended resource :)

![images/a0325fe59daa5f131fa785eeaf1f01f6.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a0325fe59daa5f131fa785eeaf1f01f6.png)


---
**gionata cannavo** *December 12, 2018 08:28*

**+Joe Alexander** **+Don Kleinschnitz Jr.** grazie della risposta, stavo vedendo il blog ma ci sono delle cose che mi creano confusione.

1 connettore 

1=24v

2=gnd ( powerin)

3=5v

4= L ( pin 2.4)

secondo connettore 6 pin

g-p =conect sicurezza nc

l=?

g:=

in=?

5v:?

sullo schema non viene dichiarato le connessione del secondo connettore , ed usando solo quelle dello schema postato non si accende il laser.

da notare che la mia k40 e la versione con regolazione di potenza ad lcd, non quello analogico.



thanks for the answer, I was seeing the blog but there are things that make me confused.

1 connector

1 = 24v

2 = gnd (powerin)

3 = 5v

4 = L (pin 2.4)

second 6-pin connector

g-p = conect security nc

l =?

g: =

a =?

5v:?

the scheme has not been solved, but only that of the scheme is used.

the truth is my k40 and the version with power and lcd adjustment, not the analog one.




---
**Joe Alexander** *December 12, 2018 12:03*

truth be told the analog ones are better as they show actual power vs supposed power. Most are wired like so:

G:goes to one side of laser enable button.



P:goes to other side of laser enable button. this is your protect loop so any safety switches go in series.



L:goes to one side of the laser test button.



G:usually 2 wires go to this one, one to the other side of the laser test button and ground for the power level control(pot on analog machines)



IN: The power level of the laser, relative to 0-5V, with 5V being 100%. Goes to middle leg of potentiometer on analog machines.



5V: Goes to the last pin on potentiometer on analog machines



Then the controller card pulses the laser on/off using typically a MOSFET that toggles L and G on the rightmost connector. When L is grounded the laser is enabled.

Hope that helps, and I recommend installing an ammeter between the tube return and the laser psu to see actual lasing power.


---
**gionata cannavo** *December 12, 2018 15:51*

**+Joe Alexander** Excuse my ignorance in the matter .... But there is a scheme of how to be made of all the connections? Because I'm afraid of doing random tests.


---
**Don Kleinschnitz Jr.** *December 12, 2018 17:00*

**+gionata cannavo** Sorry but I am not sure what exactly you need. Are you just trying to connect the smoothie to the Laser Power Supply?


---
**gionata cannavo** *December 12, 2018 17:05*

**+Don Kleinschnitz Jr.** You want to connect the card correctly with the ttl and pwm. Eliminating all the old electronics and having power managed automatically through the software I want to buy, + Lightburn


---
*Imported from [Google+](https://plus.google.com/106692309321531884270/posts/45feAzzz6KX) &mdash; content and formatting may not be reliable*
