---
layout: post
title: "LaserWeb Some macros in action"
date: July 30, 2016 03:11
category: "Hardware and Laser settings"
author: "David Cook"
---
LaserWeb  

Some macros in action. 







**"David Cook"**

---
---
**Randy Randleson** *July 30, 2016 10:59*

Thats awesome! I cant wait to try it out!


---
**Joe Spanier** *July 30, 2016 15:25*

What are you doing to fire the beam? Tiny G1 moves?


---
**David Cook** *July 30, 2016 17:01*

**+Joe Spanier**​ hi. Yes. The fire button uses G91 \n G1 Z1 F800 S0.2 \n G92 Z0 



To fire the beam when the Z moves. (I don't yet have any plans for a Z axis , so this works good for my setup)


---
**Joe Spanier** *July 30, 2016 19:58*

Oh that's awesome. I didn't even think of using z. I was moving the axis the mirror was inline with a tiny amount. That's a great idea. 


---
**David Cook** *July 31, 2016 00:29*

**+Joe Spanier**  it was  **+Peter van der Walt**  idea


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/YV4ddV5tx6q) &mdash; content and formatting may not be reliable*
