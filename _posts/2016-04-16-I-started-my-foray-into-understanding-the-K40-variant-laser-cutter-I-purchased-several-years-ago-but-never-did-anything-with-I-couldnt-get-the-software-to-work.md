---
layout: post
title: "I started my foray into understanding the K40 variant laser cutter I purchased several years ago but never did anything with (I couldn't get the software to work)"
date: April 16, 2016 03:50
category: "Modification"
author: "Richard Vowles"
---


I started my foray into understanding the K40 variant laser cutter I purchased several years ago but never did anything with (I couldn't get the software to work). 



Inside it I found this platform (and a honeycomb layer on top) - which was quite pleasing .The downside is that I found the X stepper uses a flat flex cable coupled with the end stop (at least I think it is, I can't really make it out), the Y doesn't appear to have an end stop. I've decided to get some end stops I can more easily put in the places I think they should go.



![images/ffacea3b659eb2075ff02eb6507b096a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ffacea3b659eb2075ff02eb6507b096a.jpeg)



**"Richard Vowles"**

---
---
**beny fits** *April 16, 2016 10:28*

I want this 


---
**Anthony Bolgar** *April 17, 2016 03:46*

One suggestion I have is to upgrade the absolute crappy stock controller with a Smoothie board, or Ramps 1.4 controller. You will be glad you did.


---
**Richard Vowles** *April 17, 2016 06:48*

The only available Smoothie Boards (the 5-axis ones) are way overkill. I missed the boat on those, maybe when there is another kickstarter for the next version. I looked around for something on Aliexpress and found a board called an MKS-SBASE which seems a bit better, so I have ordered and will be trying that.


---
**Anthony Bolgar** *April 17, 2016 07:58*

The MKS Sbase is a clone of the smoothieboard, I have one on order right now.


---
**Alex Hodge** *April 17, 2016 15:32*

I've got two MKS SBASE boards at the moment. I'll be wiring up my k40 using a middleman board to keep the original wiring intact. Going to use the other for my MP CNC.


---
**Anthony Bolgar** *April 17, 2016 20:34*

Just be careful when ordering the MKS Sbase board, there are two versions, one is a ramps 1.4 all in one, and the other is the smoothie compatible board. The smoothie one uses a 32 bit processor, vs the 8 bit on the ramps version .


---
**Alex Hodge** *April 17, 2016 21:32*

**+Anthony Bolgar** the ramps board is called MKS-BASE and the smoothie is MKS-SBASE. If you order from Makerbase (the guys who make the board) on aliexpress, you'll be fine.


---
**Anthony Bolgar** *April 17, 2016 23:04*

Some of the ebay sellers are naming it the same, that's why I mentioned it.


---
**Anthony Bolgar** *April 17, 2016 23:09*

I was able to find one that was only $59.00 It was a good price, just hoping it  a quality board. I am looking forward to upgrading my K40 from Ramps to Smoothie and my Redsail LE400 from its proprietary late 90's board. Stephane has created a great set of upgrade instructions for the K40, I will need to do some detective work to figure out the conections for my Redsail laser.


---
**giavonni palombo** *April 25, 2016 00:13*

Where can I find a table like that? I don't want light objects $206.00 powered one.


---
*Imported from [Google+](https://plus.google.com/+RichardVowles/posts/NrM9CCWmub9) &mdash; content and formatting may not be reliable*
