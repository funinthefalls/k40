---
layout: post
title: "Was busy this past weekend :) This time of year is the annual Azalea festival where I live"
date: April 03, 2017 18:27
category: "Object produced with laser"
author: "Ned Hill"
---
Was busy this past weekend :)  This time of year is the annual Azalea festival where I live.  (FYI-Azaleas are a flowering bush).  My daughter is an Azalea Belle this year which is like a festival ambassador and they wear these big US antebellum/ victorian style hoop dresses (think "Gone With the Wind").  I wanted to come up with a name tag she could wear with her dress and originally came up with the middle tag which pulls from part of the festival logo.  Decided to design some other tags that had more of a victorian period styling.  She ended going with the smaller one on the left.   Made from 3mm alder with magnetic backs.



![images/098e5f49d8b5b257f192e026beaa8e3d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/098e5f49d8b5b257f192e026beaa8e3d.png)
![images/0e9f06a8f235f72a4b1a448d991d433d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0e9f06a8f235f72a4b1a448d991d433d.png)
![images/e0a5b0cb5a9ddf887b437cdf02889bcd.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e0a5b0cb5a9ddf887b437cdf02889bcd.png)
![images/03efb397260b0f10701d332973f26180.png](https://gitlab.com/funinthefalls/k40/raw/master/images/03efb397260b0f10701d332973f26180.png)

**"Ned Hill"**

---
---
**Don Kleinschnitz Jr.** *April 03, 2017 19:56*

Impressive very crisp!




---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2017 20:29*

Very nice. Not sure when the festival is on exactly, but how about an azalea shaped name tag?


---
**Stephane Buisson** *April 03, 2017 22:16*

Thank you **+Ned Hill** to keep this category lively, much appreciated.


---
**Ned Hill** *April 03, 2017 23:30*

**+Stephane Buisson** Thank you, it's my pleasure.   Ever since I got my laser my maker side has really come out.  I love making things and the laser is really letting me explore new ways of making. Lol,  I've got a sketchbook full of ideas I want to try.  People like **+Alex Krause** and **+Yuusuf Sallahuddin**  are always inspiring me to do more.  My moto now is Keep Calm and Laser On. :)


---
**Ned Hill** *April 03, 2017 23:45*

One of my first projects :)

![images/5fd45c68258ad3c2de611765efdf56c8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5fd45c68258ad3c2de611765efdf56c8.png)


---
**Nigel Conroy** *April 04, 2017 13:17*

**+Ned Hill** you should do a tutorial on the coloring technique you use. 

I think I remember you saying that you used sharpies or permanent markers? 

I know you also did some pre-painting and then engraved to remove the excess.

I'd be interested to know more on how you achieve such a nice result.


---
**Ned Hill** *April 04, 2017 14:58*

**+Nigel Conroy** Thank you.  Maybe I'll do a more in depth post about what processes I use, but you can see my tools of the trade as it were below.  



Basically If I can pre paint/stain before cutting /engraving I'll do it because it always gives a more crisp and even finish. I pretty much mask everything I engrave to prevent the wood residue from staining the piece. Just makes clean up easier and when painting it offers you the ability to selectively remove parts of the mask while leaving other areas protected.   If I'm going to do color infill in engraved areas I always like to seal the surface first (usually a clear spray) to help prevent edge bleed. 



 For some small detail, oil based paint markers can be fast and usefull but I don't like them for areas that take more than a few strokes because it's hard to get an even finish. 

 Sharpies are good for a wood stained effect and they are quick, but on larger areas it can be difficult to get an even finish.  However you get some shading with them and it's possible to even the finish out with a little brushed on solvent. 

 

However for a lot of things it's good old craft store acrylic paint.  It's cheap, lots of color options and easy clean up.  For application I use good lighting, a head mounted magnifying visor (for detail work) and a good selection of brush sizes.  

For the painted tag in this post it was done with a very fine tip paint brush and a steady hand.



It's also on my list to eventually add an airbrush to my tool kit.

![images/11af2a1bdea7bded1253b8849b9fd627.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/11af2a1bdea7bded1253b8849b9fd627.jpeg)


---
**Nigel Conroy** *April 04, 2017 16:16*

**+Ned Hill**

Thanks for the detailed respond. A very steady hand indeed.

You have a very good handle on the process and I for one would love to see a detailed post/video/tutorial on the subject.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/iiJd54PckNe) &mdash; content and formatting may not be reliable*
