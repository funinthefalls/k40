---
layout: post
title: "Here's a holiday card I created with some red and white cardstock on a lightobject DSP upgraded eBay 40w k40"
date: January 07, 2016 06:25
category: "Object produced with laser"
author: "Brooke Hedrick"
---
Here's a holiday card I created with some red and white cardstock on a lightobject DSP upgraded eBay 40w k40.  The image started from a card on Pinterest.  From there I used Corel Draw 7 to turn it into a dxf and then import into LaserCAD.  The cardstock is 90lb+.  I cut using 15% power at 20mm/s.

![images/e81a365212993003fcd51276b50a1cfc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e81a365212993003fcd51276b50a1cfc.jpeg)



**"Brooke Hedrick"**

---
---
**ED Carty** *January 07, 2016 23:28*

Nice work


---
**Todd Miller** *January 08, 2016 01:24*

Happy tree's ;-)


---
*Imported from [Google+](https://plus.google.com/+BrookeHedrick/posts/Ud36dVXuHNf) &mdash; content and formatting may not be reliable*
