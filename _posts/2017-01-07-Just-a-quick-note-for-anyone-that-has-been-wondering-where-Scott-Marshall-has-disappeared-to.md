---
layout: post
title: "Just a quick note for anyone that has been wondering where Scott Marshall has disappeared to"
date: January 07, 2017 04:09
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Just a quick note for anyone that has been wondering where **+Scott Marshall** has disappeared to.



I've had an email from Scott over the night where Scott has explained that he is currently very unwell & looking at needing to undergo more surgery.



He has asked me to forward his apologies to anyone that is/was interested in his ACR kits or to anyone needing assistance with their existing kit. He will endeavour to resolve all issues at the earliest possible convenience as his health permits.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Don Kleinschnitz Jr.** *January 07, 2017 04:26*

Tell him I wish him the best and hope he gets well. Also I am willing to help anyone having problems with his kits, on his behalf,  if I had the schematics and install documents.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2017 04:55*

**+Don Kleinschnitz** Thanks Don. I'll pass along your wishes & offer for help.



edit: might be a while before he sees my reply as he mentioned he has 4552 unopened emails at this stage. I'll let you know when I hear back from him.


---
**Bill Keeter** *January 09, 2017 01:15*

**+Yuusuf Sallahuddin** thanks for the update! I've been concerned for him. Like everyone else, I haven't received any email responses in a few months.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 09, 2017 01:17*

**+Bill Keeter** Hopefully he can be back in high health in the near future. Is there anything in particular that you needed help/assistance with? It is possible (however low the chance) that I or someone else may be able to assist in the meantime.


---
**Bill Keeter** *January 09, 2017 01:31*

**+Yuusuf Sallahuddin** Afraid not. I'm emailing him now that I had to cancel my custom ACR kit from him. It's probably sitting on his work bench finished. But it's been months. Sigh. And I couldn't wait any longer. ...ah, well. I've offered to compensate him for his time.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/SrB8Dmng1zq) &mdash; content and formatting may not be reliable*
