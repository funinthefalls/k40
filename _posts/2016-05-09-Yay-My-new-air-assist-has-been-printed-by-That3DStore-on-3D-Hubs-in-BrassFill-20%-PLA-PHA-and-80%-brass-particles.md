---
layout: post
title: "Yay! My new air-assist has been printed by That3DStore on 3D Hubs in BrassFill (20% PLA/PHA and 80% brass particles)"
date: May 09, 2016 05:09
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Yay! My new air-assist has been printed by That3DStore on 3D Hubs in BrassFill (20% PLA/PHA and 80% brass particles).



It is on the way now in the mail. Looks good & I'm excited to put it to the test.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Scott Marshall** *May 09, 2016 05:29*

That's NICE! 

I didn't know about that material. Looks Great, Awaiting performance testing..... (have an idea brewing.. )


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 05:40*

**+Scott Marshall** It looks interesting the material. Not sure about it either as I've never seen it before now, but hopefully due to being partially brass it gives it extra rigidity. The print quality is slightly less (100 microns) than the previous one (25 microns), but looks good enough for the purposes.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/ddbvDRWxKHg) &mdash; content and formatting may not be reliable*
