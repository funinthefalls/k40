---
layout: post
title: "Machine is not engraving, test button does not work on panel but does work from test button on psu"
date: January 22, 2017 23:14
category: "Original software and hardware issues"
author: "tyler hallberg"
---
Machine is not engraving, test button does not work on panel but does work from test button on psu. Bad psu? I've taken it out no obvious damage to any components. 





**"tyler hallberg"**

---
---
**Don Kleinschnitz Jr.** *January 23, 2017 00:20*

Disconnect then check across the terminals of the test switch on the panel with an ohm meter while pushed. Should be zero ohms.



While connected short the terminals on the test switch. Should fire...


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/RS5WSq9ihVc) &mdash; content and formatting may not be reliable*
