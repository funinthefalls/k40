---
layout: post
title: "HELP!! So I have been using the K40 for a month now testing and playing..."
date: November 25, 2017 03:21
category: "Original software and hardware issues"
author: "John Warren"
---
HELP!!



So I have been using the K40 for a month now testing and playing... Tonight I finished a new bed for the system and started to run a dxf vector cut to make a new rotary.  It ran through once but not all the way through the mdf.  1/4 way through the 2nd time the power just cut out of the laser.  All electronics appear to work fine.  When I put the setting to less than 10% you can hear the test fire and a glimmer or light through the tube.  if i put the settings above 10% the tube lights up solid and there seems to be no output.  attached is a picture when it just lights with no real output.

![images/8988933405188cf8b02b326a3c138576.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8988933405188cf8b02b326a3c138576.jpeg)



**"John Warren"**

---
---
**HalfNormal** *November 25, 2017 03:31*

Check your mirrors and Alignment. Looks like the laser is firing but not getting to the work


---
**John Warren** *November 25, 2017 04:31*

ok well that may definitely be part of the problem.  looks like somehow ( i have no idea how)  it burned through the one attached to the Y carriage.  Does anyone know of a good place to get mirrors that wont break the bank? 

![images/9da9e8ca6348f83cdca57997717387b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9da9e8ca6348f83cdca57997717387b7.jpeg)


---
**Ned Hill** *November 25, 2017 04:32*

Had a similar issue not long after I first got my laser where one of the springs on the 1st mirror broke so the laser didn't even leave the tube cabinet.  


---
**John Warren** *November 25, 2017 04:42*

so are these 20mm mirrors?  


---
**Ned Hill** *November 25, 2017 04:44*

Yes 20mm


---
**Ned Hill** *November 25, 2017 04:46*

I've bought replacements from Light Objects.


---
**John Warren** *November 25, 2017 05:30*

I have to say THANK YOU!!! to all for the quick response and ideas.. pulled the mirror which disintegrated in my hand.  I was worried about the tube but i think it is ok.  although i did notice a water bubble so i need to address that,  have a couple people waiting on me so i have to rush order the mirrors.  3 mirrors from amazon being delivered overnight for free shipping(One of the times Prime pays for itself).  fingers crossed i will be back and running by sunday night.   


---
**Anthony Bolgar** *November 25, 2017 06:22*

The stock mirrors on most chinese lasers are garbage, my 60W burned through both stock mirrors in less than 5 hours of run time. Replaced them with some good quality MO mirrors.




---
*Imported from [Google+](https://plus.google.com/114420019102377897867/posts/UKreBUadohA) &mdash; content and formatting may not be reliable*
