---
layout: post
title: "I recieved my K40 yesterday and after a couple hello world engraves for testing i have replaced the electronics with a Re-arm + ramps combo"
date: February 11, 2017 06:00
category: "Hardware and Laser settings"
author: "David Gray"
---
I recieved my K40 yesterday and after a couple hello world engraves for testing i have replaced the electronics with a Re-arm + ramps combo. I have x-y movement and laser working but I'm stuck on getting the endstops working. I have the "correct" pins from the ribbon cable connected to x min and y min (1.24^ and 1.26^ in the config) but no luck. Anything I'm missing?





**"David Gray"**

---
---
**Philipp Tessenow** *February 11, 2017 08:09*

Try defining all endstop pins in the config, even if you have not installed them physically.  That worked out for me.



I first only defined xmin and ymax, and uncommented/deleted the config rows of the other endstops in the config file. Could get movement, but issuing G28.2 resulted in a board crash. 



Now everything works. 



Oh and use G28.2 for homing (change in laserweb G-code settings). And make sure you use the firmware-cnc.bin 



Hope that helps.


---
**David Gray** *February 11, 2017 08:13*

Cheers. Will give that a shot in the morning. I am using firmware-cnc FWIW..






---
**Wolfmanjm** *February 12, 2017 22:02*

Use M119 to test endstops, you only need to define the ones you have. Also the K40 optical endstops require 5v not 3.3v so make sure you are providing 5v to them and not 3.3v/


---
**David Gray** *February 13, 2017 04:34*

I couldnt figure out the issue, but i removed the ribbon cable and wired in my own and now its all working :/ 


---
*Imported from [Google+](https://plus.google.com/117535437951142576212/posts/GVxo5p68422) &mdash; content and formatting may not be reliable*
