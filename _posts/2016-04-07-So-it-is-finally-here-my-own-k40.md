---
layout: post
title: "So it is finally here... my own k40"
date: April 07, 2016 22:54
category: "Discussion"
author: "MNO"
---
So it is finally here... my own k40.

Any advise what not to do... 





**"MNO"**

---
---
**Stephane Buisson** *April 07, 2016 23:05*

... forgot the water pump. (for the tube sake)

check the wiring for loose connection. does the the lens in the right direction (lack of power) ?


---
**ThantiK** *April 07, 2016 23:11*

Water pump -- After you get it running, make sure (SURE!) there are no air bubbles in the tube.  If there are no leaks, and you have some hard bubbles to get at, leave the pump on overnight -- I had just time manage to wisk away the air pockets overnight.  Careful with this - you risk breaking the tube if you drop the machine.


---
**I Laser** *April 07, 2016 23:55*

Also don't run the tube too hard, I don't push mine higher than 10mA. 



If it's not cutting properly, make sure your mirrors & lens are clean and also check alignment.


---
**Phillip Conroy** *April 08, 2016 00:03*

Add a water flow switch wired in series with the laser enable switch-i blow a tube and power supply r$400 latter all because  the plug adaptor didnt hold the water pump lead in correctly and water was not going to the tubethe bugger of it all was i had a water flow witch sitting on my desk for the previse 2 weeks ,and a safetyswitch on the laser bed lid


---
*Imported from [Google+](https://plus.google.com/111888830486900331563/posts/c6Jk9FstU4U) &mdash; content and formatting may not be reliable*
