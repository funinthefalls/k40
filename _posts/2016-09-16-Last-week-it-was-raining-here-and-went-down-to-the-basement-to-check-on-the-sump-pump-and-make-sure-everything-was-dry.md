---
layout: post
title: "Last week it was raining here and went down to the basement to check on the sump pump and make sure everything was dry"
date: September 16, 2016 02:14
category: "Discussion"
author: "Jim Fong"
---
Last week it was raining here and went down to the basement to check on the sump pump and make sure everything was dry.  To my horror, the laser was all covered with a thin layer of moisture on the INSIDE.  The laser is connected with a 4" dryer vent hose about 6 feet long going up the wall and out through a hole in the wall.  I'm using a standard dryer exhaust vent wall cover with a built in flap. All caulked and sealed so no water can get into the house.   Looks like the exhaust vent flap got stuck open and cold wet moist air traveled down the vent hose and accumulated on the inside of the laser.  I unhooked the vent hose and tried wiping the inside surfaces dry but everything was wet including the the power supply.  I let it dry for a week before I turned it on today. Laser seems to work just fine!!!! To solve this problem, I bought a 4" blast gate used to hook up dust collection hose to woodworking machinery.  I now manually close the blast gate when done using the laser. This should keep any moist air from getting to the laser.  



Sorry for the long post but just wanted to share what happened to my k40 laser.  I'm just glad it still works. 





**"Jim Fong"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 02:57*

Thanks for sharing that Jim. I hadn't even considered that as a possibility & it is good to know to keep an eye out for it in future with my setup.


---
**Jim Fong** *September 17, 2016 02:49*

Just a little warning for everyone.  One of those freaks of nature with windy conditions and right dew point temperature to make it happen. Luckily nothing was damaged.  Those with really expensive lasers should make sure this couldn't happen to them. 


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/gp8eQU9KfSM) &mdash; content and formatting may not be reliable*
