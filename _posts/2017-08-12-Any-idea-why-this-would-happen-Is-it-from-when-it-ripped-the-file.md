---
layout: post
title: "Any idea why this would happen? Is it from when it ripped the file?"
date: August 12, 2017 23:52
category: "Discussion"
author: "William Kearns"
---
Any idea why this would happen? Is it from when it ripped the file? See inside red circles

![images/3fd87ebd2d0734deb9fa5fec278eb716.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3fd87ebd2d0734deb9fa5fec278eb716.jpeg)



**"William Kearns"**

---
---
**Ariel Yahni (UniKpty)** *August 13, 2017 00:05*

Could be missed steps, the belt slipped


---
**Don Kleinschnitz Jr.** *August 13, 2017 00:19*

Have seen this error on this forum many times. Usually a large engraving job. Does it shift both directions?


---
**William Kearns** *August 13, 2017 00:33*

it only happens on the x axis.


---
**Ray Kholodovsky (Cohesion3D)** *August 13, 2017 00:39*

Have you adjusted the little pots on the green drivers on the Cohesion3D Mini? 

Have you added a separate psu or still on the original laser psu? 


---
**William Kearns** *August 13, 2017 00:41*

I have nit adjusted the pots and I have an external psu for the z axis


---
**Ray Kholodovsky (Cohesion3D)** *August 13, 2017 00:45*

Right, I remember that bit about the external psu now. You have it powering just the Z external driver but not the board itself. 



1 of 2 things is happening, and they are intertwined. 

Either your A4988 driver is not providing enough current for the speed you are running at and you got the skip, or your laser psu cannot provide enough power and browned out. 



So... turn the pots on the green drivers 45 degrees clockwise for now. 



Eventually we should get your Mini running off the external psu. This just requires connecting 24v and Gnd from it to the main power in terminal of the Mini (top left corner). 

At this time you would need to cut/ disconnect the 24v wire on the k40 - Mini power connector (large white plug going into Mini with 4 wires, we want to keep L and Gnd but disconnect +24v) 


---
**Don Kleinschnitz Jr.** *August 13, 2017 13:53*

**+William Kearns** what I meant to ask does it shift only one direction in the X ie: to the right or does it sometimes shift left also. 

Are you engraving from the top to the bottom or visa versa?


---
**Erdem Official** *August 29, 2017 11:24*

kayış sıkı olabilir. biraz gevşet kaydırma yapar. 

toprak hattı yaptır. statik elektrik sebep oluyor. 




---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/1hais8d4yrA) &mdash; content and formatting may not be reliable*
