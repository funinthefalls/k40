---
layout: post
title: "Registration issue.....could do with some help! Hey guys, I wanted to engrave on some 30mm tall boxes, as I don't have a powered table"
date: May 20, 2016 00:04
category: "Discussion"
author: "Pigeon FX"
---
Registration issue.....could do with some help! 



Hey guys, I wanted to engrave on some 30mm tall boxes, as I don't have a powered table. its not like I can raise the bed, cut out a jig and then lower it again while keeping registration. 



So..... I was wondering could I set my bed to the 30mm + focal length (using wooden blocks) and then l mark something like masking tape on the bed despite being 30mm out of focal length?



Would love to hear any other suggestions as I'm struggling here when it comes to thinking of ways to register work pieces as I don't have anything square like a right angle on the laser itself to butt thing against .





**"Pigeon FX"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 20, 2016 02:13*

I would suggest cutting a piece of ply or mdf at correct focal point in an L shape, that butts up against the back left corner of the machine. So just cut a 300x200mm rectangle out of the ply/mdf at origin 0x 0y. Then use this as a starting point for all your cutting/engraving. You can then create individual jigs (at normal focal point) that you could put up against that to move objects to their correct positions. Hope that makes sense, but that is what I would do.



edit: Even when you drop the table 30mm (for e.g.) those jigs should keep your item's top at the exact same spot as the jig would be (if the table is 30mm higher).


---
**Pigeon FX** *May 20, 2016 13:11*

Think I get what you mean, but any chance of a photo?  


---
**Thor Johnson** *May 20, 2016 14:20*

I did this: [http://www.thingiverse.com/thing:1002341](http://www.thingiverse.com/thing:1002341), and now I fire the main laser at  2 points (making a hole/char mark), and then for realignment, I line up the red dot so that it "falls into" the holes.



If you're out of focus, your "dots" are going to be large... maybe even very large.  If you can take out the lens, you can probably get a smaller char mark than if you're 30 cm below focus.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 20, 2016 14:27*

**+Pigeon FX** I can do one later on for you.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 22, 2016 08:09*

**+Pigeon FX** I've posted a few photos for the registration jig explanation. It is in Discussion page here (or you can find it on my profile page).


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/Y8ey9Smk6RJ) &mdash; content and formatting may not be reliable*
