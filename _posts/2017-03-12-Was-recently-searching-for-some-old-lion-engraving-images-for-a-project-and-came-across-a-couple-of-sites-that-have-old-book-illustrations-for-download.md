---
layout: post
title: "Was recently searching for some old lion engraving images for a project and came across a couple of sites that have old book illustrations for download"
date: March 12, 2017 17:41
category: "Repository and designs"
author: "Ned Hill"
---
Was recently searching for some old lion engraving images for a project and came across a couple of sites that have old book illustrations for download.  Lots of great images for engraving.  

[https://www.oldbookillustrations.com](https://www.oldbookillustrations.com)

[http://www.fromoldbooks.org](http://www.fromoldbooks.org)





**"Ned Hill"**

---
---
**HalfNormal** *March 12, 2017 22:13*

**+Ned Hill** I have been looking for just this type of drawings.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/GFxcJicoDcJ) &mdash; content and formatting may not be reliable*
