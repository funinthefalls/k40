---
layout: post
title: "My K40 has been 95% Smoothified! Just need to work out the limit switches and homing directions and it is good to go"
date: October 16, 2016 01:19
category: "Smoothieboard Modification"
author: "Anthony Bolgar"
---
My K40 has been 95% Smoothified! Just need to work out the limit switches and homing directions and it is good to go. I even got the GLCD screen installed today. Nice to be able to use the machine standalone if I want.





**"Anthony Bolgar"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 16, 2016 01:29*

Which board is this with? 


---
**Anthony Bolgar** *October 16, 2016 01:34*

With an original bonafied smoothieboard, courtesy of Arthur


---
**Ric Miller** *October 16, 2016 01:56*

I really want to go standalone "headless".


---
**greg greene** *October 16, 2016 14:42*

What did you need to get besides the smoothie board, I'm going to do the conversion - but want to be sure I get all the parts I need first


---
**Anthony Bolgar** *October 16, 2016 15:34*

I used a middleman breakout board to get the ribbon cable to usable wires, a glcd adapter board from Robotseed.com (also available at Uberclock.com in the U.S), a RepRapDiscount Full Graphic Smart Controller, and a levelshifter to convert the 3.3V PWM signal to a 5V signal the PSU needs. Use the firmware.cnc.bin file from [https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-cnc.bin?raw=true](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-cnc.bin?raw=true) and rename it firmware.bin I wired it with pin 2.4! (inverted) to the levelshifter that then goes to the L input on the laser PSU. i used a single pin setup, the same one **+Alex Krause** used. If you need help, feel free to hit me up for any advice you may need, it is all fresh in my mind right now.


---
**greg greene** *October 16, 2016 16:03*

Thanks very much !


---
**James Rivera** *October 16, 2016 20:08*

Wait..you can use it standalone this way? Dumb question: How do you get the project data files into it?


---
**Anthony Bolgar** *October 16, 2016 23:04*

There is an SD card reader built into the GLCD display, as well as the tft card in the smoothieboard


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/8SfBoFEgUF6) &mdash; content and formatting may not be reliable*
