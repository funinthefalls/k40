---
layout: post
title: "Made a custom engraved bottle opener from laminated maple and black walnut"
date: December 02, 2018 18:50
category: "Object produced with laser"
author: "Ned Hill"
---
Made a custom engraved bottle opener from laminated maple and black walnut. Another thing for the silent auction for my college fraternity’s 50th chapter anniversary get together. 



The party was actually last night and was a blast.  My slate coasters and holder set was popular and ended up fetching $180 in the silent auction. The bottle opener went for $80. Very happy with the result with the money going to a good cause.  Was also great hearing all the positive comments on my work :)



![images/7cb921ad2e1ecd56dfb8249e7b78844b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7cb921ad2e1ecd56dfb8249e7b78844b.jpeg)
![images/bf58d6b18236479d3b5051ff6ca0ede1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf58d6b18236479d3b5051ff6ca0ede1.jpeg)
![images/08438b103c88839dc3ee30d258fd66b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/08438b103c88839dc3ee30d258fd66b1.jpeg)

**"Ned Hill"**

---


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/6maPgh6pent) &mdash; content and formatting may not be reliable*
