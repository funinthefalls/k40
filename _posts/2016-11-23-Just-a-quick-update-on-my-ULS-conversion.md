---
layout: post
title: "Just a quick update on my ULS conversion..."
date: November 23, 2016 17:49
category: "Modification"
author: "David Cook"
---
Just a quick update on my ULS  conversion...    I swear doing this mod feels like flipping off a nice old lady..       It does not make much sense at all. lol.    But It is saving me about $2K   that it would cost to have the air-cooled RF tube repaired..



So I just decoded all the wires. so I now know the cables that go to each motor,   the end stop cables,  the autofocus cables.



I ordered a bunch of Molex connectors  so I can make adapter cables to interface into the SBASE.    I am doing this mod in such a way that all the OEM components are un-touched so I can easilly go back to RF mode later.   Since the HV power supply has the return grounded to the chassis, I did decide to remove the OEM computer from this case. lol.



I think I will do a nice cooling system for this.   I'm interested in Scotts WaterWorks setup.





**"David Cook"**

---
---
**Kelly S** *November 24, 2016 19:56*

Following, like seeing how people make items themselves as they go.  :)  Hope it works good for you!


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/2MyD4VvCMoF) &mdash; content and formatting may not be reliable*
