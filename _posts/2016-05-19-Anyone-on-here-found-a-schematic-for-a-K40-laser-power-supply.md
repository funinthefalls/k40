---
layout: post
title: "Anyone on here found a schematic for a K40 laser power supply?"
date: May 19, 2016 13:27
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
Anyone on here found a schematic for a K40 laser power supply?





**"Don Kleinschnitz Jr."**

---
---
**Jon Bruno** *May 19, 2016 14:05*

There are way too many variants of the LPSU. But they are all pretty similar in design.  Do you have a damaged LPSU that you need to fix?


---
**Don Kleinschnitz Jr.** *May 20, 2016 12:01*

Someone on the FB forum did and I realized that I do not have a schematic if mine were to go bad.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/9YzxeXTUWyW) &mdash; content and formatting may not be reliable*
