---
layout: post
title: "K40 arrived this weekend two days after I ordered"
date: October 04, 2016 03:39
category: "Discussion"
author: "Bill Keeter"
---
K40 arrived this weekend two days after I ordered. Wow. But on later inspection the 40w glass tube is cracked. 



While testing the water cooling today I realized the glass tube has a crack going all the way around. So I guess that means a new tube and having to calibrate. Does this mean i'm officially part of the club now?



Also what's the common response from the ebay sellers? Will they replace the tube or will I be off hunting a replacement myself? 





**"Bill Keeter"**

---
---
**Alex Krause** *October 04, 2016 03:48*

Where are you located **+Bill Keeter**​?


---
**Ariel Yahni (UniKpty)** *October 04, 2016 03:50*

**+Bill Keeter**​ inmediatly place a claim. They will try to offer you to send a tube but as they cannot guarantee the rest work I suggest ask for a full refund


---
**Bill Keeter** *October 04, 2016 03:52*

I'm in the US.


---
**Alex Krause** *October 04, 2016 03:53*

Take videos and pictures of the problem they will demand it. Don't close the claim until you have received a replacement and it's tested as working. Try to work it out with the seller first before opening a PayPal/eBay claim and if you are within driving distance to the warehouse in California offer to pick it up from them for the replacement I have seen some on the Facebook group able to get it settled in days since they were in driving distance... double/triple check for any other issues such as leaving the laser tube off and test driving the axis' to make sure they home properly and all electrical connections are good


---
**Alex Krause** *October 04, 2016 03:57*

If you ask for a full refund as **+Ariel Yahni**​​ suggested (he has gone thru this) they will counter offer a low ball amount. The more issues you can uncover in the machine the higher your return will be if they can't get you a replacement tube


---
**Ariel Yahni (UniKpty)** *October 04, 2016 03:59*

I agree with **+Alex Krause**​ on the process just mind that as this is very common they have insurance on the shipping so issuing a refund is no biggie and that's why they do it often. 


---
**Bill Keeter** *October 04, 2016 04:01*

Photos and a youtube video have already been sent to them. They just told me the photos are fuzzy. Which shouldn't matter cus the video is in HD. Blah. I'm only a state away from globalfreeshipping's Tennessee warehouse. Hopefully I can get this taken care of quick.


---
**Alex Krause** *October 04, 2016 04:02*

The main thing is don't close the claim until you are satisfied, don't let them tell you they can't do anything about it till the claim is closed. And don't let them drag their feet to let the claim time run out escalate the claim if they drag their feet


---
**Don Kleinschnitz Jr.** *October 04, 2016 04:09*

God this is awful. Anyone want to start a company and design, build, ship and support high value laser. It's not that hard

.....


---
**Ariel Yahni (UniKpty)** *October 04, 2016 04:11*

Global is the sameone I got. Funny guys will do anything for a good review 


---
**Ariel Yahni (UniKpty)** *October 04, 2016 04:12*

**+Don Kleinschnitz**​ I'm in. 


---
**Bill Keeter** *October 04, 2016 04:28*

![images/1282c24ea8b2f4e4b920a4d11d4827da.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1282c24ea8b2f4e4b920a4d11d4827da.jpeg)


---
**Bill Keeter** *October 04, 2016 04:29*

![images/40ec39659b548cdb5fbeaa3cbbc9cf08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40ec39659b548cdb5fbeaa3cbbc9cf08.jpeg)


---
**Don Kleinschnitz Jr.** *October 04, 2016 04:33*

**+Ariel Yahni** careful I may not be kidding ;)


---
**Ariel Yahni (UniKpty)** *October 04, 2016 04:37*

**+Don Kleinschnitz**​ I'm not 


---
**Don Kleinschnitz Jr.** *October 04, 2016 04:46*

So why hasn't anyone done this ???


---
**Ariel Yahni (UniKpty)** *October 04, 2016 04:47*

US law I beleive regarding laser operations and safety 


---
**Don Kleinschnitz Jr.** *October 04, 2016 04:59*

**+Ariel Yahni** I will have to look that up.


---
**Alex Krause** *October 04, 2016 05:05*

CE,ISO,UL,NSF... the FDA approval stamps you see pertain to uses of cosmetic laser and vision correcting lasic and radiation exposure. Those would be starting phrases I would look for for laser compliance info


---
**Don Kleinschnitz Jr.** *October 04, 2016 05:17*

Doesn't look that onerous (class 3B)

.. Fail safte interlocks 

...Emission power limit control

...Remote interlock

...Key lock

...Operating indicator

...Viewport design



Actually all stuff we should consider anyway.



This might be old:

[http://www.fda.gov/downloads/MedicalDevices/DeviceRegulationandGuidance/GuidanceDocuments/UCM095304.pdf](http://www.fda.gov/downloads/MedicalDevices/DeviceRegulationandGuidance/GuidanceDocuments/UCM095304.pdf)

[fda.gov - www.fda.gov/downloads/MedicalDevices/DeviceRegulationandGuidance/GuidanceDocuments/UCM095304.pdf](http://www.fda.gov/downloads/MedicalDevices/DeviceRegulationandGuidance/GuidanceDocuments/UCM095304.pdf)


---
**Don Kleinschnitz Jr.** *October 04, 2016 05:18*

What would we offer that Full Spectrum does not?


---
**Ariel Yahni (UniKpty)** *October 04, 2016 05:25*

Price. K40 sell because of it. The same way people buy $200 3d printer


---
**Don Kleinschnitz Jr.** *October 04, 2016 05:27*

Newer but says the same .

[http://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfcfr/cfrsearch.cfm?FR=1040.10](http://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfcfr/cfrsearch.cfm?FR=1040.10)


---
**Don Kleinschnitz Jr.** *October 04, 2016 05:28*

Well I guess that is why I started my k40 my analysis showed I could have and Epilog for 1/2 the price.


---
**Don Kleinschnitz Jr.** *October 04, 2016 05:32*

Can we do it for $1750. Why do we think we could do it cheaper than them?


---
**Ariel Yahni (UniKpty)** *October 04, 2016 05:37*

Well it's easy to sum way bellow when you are only a counting on you


---
**Don Kleinschnitz Jr.** *October 04, 2016 05:37*

I see they have a new desktop one "Muse"


---
**Bill Keeter** *October 04, 2016 13:19*

Global sent a note that a new laser tube should be on the way in the next day or so. Hopefully this one makes it. <b>crosses fingers</b>


---
**Ulf Stahmer** *October 04, 2016 16:33*

Welcome to the group, **+Bill Keeter**!


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/1ENFd4sBVoJ) &mdash; content and formatting may not be reliable*
