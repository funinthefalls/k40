---
layout: post
title: "After reading the message from Jessica, here is a link to a document I generated on how to align the cutter"
date: February 09, 2015 21:54
category: "External links&#x3a; Blog, forum, etc"
author: "Tim Fawcett"
---
After reading the message from Jessica, here is a link to a document I generated on how to align the cutter. After doing this mine went from just about cutting 3mm MDF at 7mm/s to slicing through it at 12mm/s to 16mm/s.





**"Tim Fawcett"**

---
---
**Stephane Buisson** *February 10, 2015 08:14*

Excelent link, thank you Tim


---
**Jose A. S** *April 04, 2015 06:11*

Do the link still working?


---
**Tim Fawcett** *April 04, 2015 21:05*

Certainly Does!

This should take you direct:



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
*Imported from [Google+](https://plus.google.com/113171525751920354895/posts/DzVw4MuY1r6) &mdash; content and formatting may not be reliable*
