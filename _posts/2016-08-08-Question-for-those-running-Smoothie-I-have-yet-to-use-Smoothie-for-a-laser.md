---
layout: post
title: "Question for those running Smoothie. I have yet to use Smoothie for a laser"
date: August 08, 2016 16:43
category: "Smoothieboard Modification"
author: "Eric Flynn"
---
Question for those running Smoothie.   I have yet to use Smoothie for a laser.   I was wondering if laser mode still retains the ability to read temperatures etc, and control relays etc. based on temp like it does with other modes?





**"Eric Flynn"**

---
---
**Ariel Yahni (UniKpty)** *August 08, 2016 16:49*

Do you currently read temperature or relays with the stock board? 


---
**Eric Flynn** *August 08, 2016 16:57*

Uh, no.  Not sure what that has to do with the question.


---
**Ariel Yahni (UniKpty)** *August 08, 2016 17:10*

Ok just wanted to clear up the question. Laser mode will not impact in any way the ability to use pins for other functions. 


---
**Eric Flynn** *August 08, 2016 17:33*

Ok, I wasnt sure if it disabled support for temperature control of an output since the hotend features will be disabled.


---
**Ariel Yahni (UniKpty)** *August 08, 2016 17:35*

Remember with smoothie you can create any switch with its relevant hardware pin and us it accordingly 


---
**Ariel Yahni (UniKpty)** *August 08, 2016 17:36*

You don't need to disable ( most cases)  other features unless you intend to you the same pin ﻿


---
*Imported from [Google+](https://plus.google.com/107613521854079979112/posts/XehTWHRPnMA) &mdash; content and formatting may not be reliable*
