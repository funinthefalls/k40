---
layout: post
title: "1/4\" MDF is my new favorite material to laser cut"
date: January 20, 2016 05:59
category: "Materials and settings"
author: "Joe Spanier"
---
1/4" MDF is my new favorite material to laser cut. So clean, such a thin kerf, and consistent. I've been putting MDF off as it just seemed so dense but its so much nicer than the 1/4" ply ive been cutting. 





**"Joe Spanier"**

---
---
**Brooke Hedrick** *January 20, 2016 06:32*

Hey Joe,



Is MDF safe to cut with a laser?


---
**I Laser** *January 20, 2016 07:37*

You don't want to be breathing in the fumes, it still contains small amounts of formaldehyde. 


---
**Joe Spanier** *January 20, 2016 17:17*

I dont think breathing the smoke of anything is healthy though. Proper extraction is Key with any material


---
**Jim Hatch** *January 20, 2016 18:34*

@Brooke: It's no worse than the glues used in some plywood. It's usually on the approved materials list for all the laser operations I've encountered (makerspace, university, etc). Just make sure you've got it vented. An air assist is useful too in keeping it from catching fire at high power/low speeds.


---
**I Laser** *January 20, 2016 20:32*

**+Joe Spanier** I thought that would go without saying. ;)



Not sure about in the states, but here the hardware stores will cut ply down for you but refuse to cut MDF. Probably a dust thing, but still it says something for the level of concern they have regarding formaldehyde.



Don't get me wrong, the most common material I use in the machine is MDF, but just be a bit more careful.


---
**Brooke Hedrick** *January 20, 2016 20:33*

Thanks!  I have been avoiding it due to the formaldehyde concern.


---
**Joe Spanier** *January 20, 2016 20:55*

Really? Ive never had an issue getting MDF cut. I was a car audio installer for a lot of years. Ive used more MDF than I care to think about. Most of it cut on hardware store panel saws


---
**Pete OConnell** *January 20, 2016 21:11*

MDF is classified as a carcinogen in the UK/EU which some places will try avoid which is why some places wont cut


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/VQZTeVC4sDo) &mdash; content and formatting may not be reliable*
