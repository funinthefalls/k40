---
layout: post
title: "Howdy all, I have upgraded to the Cohesion Smoothie board - with LaserWeb3 have a problem with getting the 'Test Fire' button working"
date: December 11, 2016 17:40
category: "Discussion"
author: "greg greene"
---
Howdy all, I have upgraded to the Cohesion Smoothie board - with LaserWeb3 have a problem with getting the 'Test Fire' button working.  I have set the strength to 0.4 and duration to 500ms in the settings menu - and before trying the button issued the M3 Laser on command from the console as required by the cohesion board - but nada - test fire button on panel is still hooked up - but also doesn't work.  Anyone have or solved this problem?



Thanks !





**"greg greene"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 00:50*

Running laserweb's test fire button requires firmware-cnc.bin to be flashed onto the board. I'll find some links on how to flash smoothie and get back to you. Pretty much, download firmware-cnc.bin, rename to firmware.bin, put it onto the Microsd card, and reset the boards. The LEDs should "count up" in binary and then resume flashing the normal way. 


---
**greg greene** *December 12, 2016 12:33*

Thanks Ray !


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/WSyjJfRMJVr) &mdash; content and formatting may not be reliable*
