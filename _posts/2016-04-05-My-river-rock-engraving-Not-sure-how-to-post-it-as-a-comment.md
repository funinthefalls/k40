---
layout: post
title: "My river rock engraving. Not sure how to post it as a comment"
date: April 05, 2016 16:52
category: "Object produced with laser"
author: "Ben Walker"
---
My river rock engraving.  Not sure how to post it as a comment.   The lighter rocks don't work well.   You can't really burn them.   Etch bleaching

![images/ac2dd85013d9d26e50cc6dc87b574c3b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ac2dd85013d9d26e50cc6dc87b574c3b.jpeg)



**"Ben Walker"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 16:58*

That's interesting & looks good. You can't really post any images as a comment to a post. Unless you paste a link to the image/post. What sort of settings did you use to achieve this result?


---
**Ben Walker** *April 05, 2016 17:01*

**+Yuusuf Sallahuddin** very low setting.   Maybe 5-10%.  And probably 400mms.  I went over it a few times.   I tried drilling s hole too.  Got about 1/4 inch deep cutting same circle over and again.   The rock glowed red.  Then simply exploded so I do not recommend going too high on power.   


---
**Jim Hatch** *April 05, 2016 18:14*

**+Ben Walker**​ That is a point I failed to make in another thread about marking rocks. You can't really cut rock with a 40w laser and overheating some kinds of rocks (like sedimentary) that can contain a lot of water which will boil and potentially explode. Marking/etching should be the objective.


---
**Scott Marshall** *April 05, 2016 18:26*

Do Animals,  Faces, & Zombies and create the Pet Rock of the new millennium.



Remember me when you have 20 lasers running 24/7...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 06:35*

**+Ben Walker** Thanks Ben. I actually laughed about the rock exploding. I think drilling a hole would be easier with traditional methods (e.g. a drill & some water to cool it as you go). But I do like the idea of etching/marking the rock/stone.


---
**Donna Gray** *April 24, 2016 22:38*

Hi Ben I have just purchased a K40 Laser Cutter You commented on a post of mine that it will cut card stock without charing I have tried with my mA current meter only registering about 4mA and I have tried with the speed increased up to 1000mm and it is still charing I am using CorelDraw and CorelLaser when I go into the cut settings it has the power set as 75% and it is light grey so I am unable to reduce it to 30% power like you said in my post Do you know how I can get the Power setting to be able to be changed??


---
**Ben Walker** *April 24, 2016 22:50*

**+Donna Gray** are you making sure the laser is focused?   If you don't assist the table height you will notice a wider cut path.   My laser just has a dial that I set to approx 30%.  I made a little spacer so I can check that I'm cutting at the correct distance from the lens


---
**Donna Gray** *April 24, 2016 22:58*

I don't know how to assist the table height??? Sorry I am very new at this


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 01:27*

**+Donna Gray** I am fairly certain Ben is meaning "adjust the table height". With the default table inside the K40, it isn't adjustable. You either have to remove it & replace it with a Z-Table (which is basically a cutting area that raises up or down), or have to remove it & use some other method of setting your objects to the correct height.


---
**Jim Hatch** *April 25, 2016 03:34*

**+Yuusuf Sallahuddin** I think you're right. The way I did it is after taking out the table, I have a multi-layer set of spacers so I can adjust it in increments of 1/8" - a triangular support of 1/4" MDF that's 2" high & each of the sides is 10" long forms the base. Then I have a 1/4" aluminum plate (it won't refract the laser light if it hits like steel will. Then a set of 1/4 & 1/8" MDF boards depending on the height I need. On top of that is a set of mending plates used in construction that have the sharp pointy ends up. Then a piece of decorative cutout aluminum that's about 1/32". All stuff available at the building supply store (Home Depot or Lowes here in the States). I add or subtract the layers of MDF depending on how thick a material I am using. Since I usually only do 1/4 & 1/8" stock it's pretty easy to slip one or the other spacer in there. 



I also made a 50.8mm cut out rectangle I use as a spacer from the material it should come to the bottom of the lower ring in my focusing lens holder (where the air assist cone meets the knurled section that holds the lens). Makes checking the height pretty quick.



If I did more stuff of various thicknesses I'd probably build a motorized Z axis adjustment table. 


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/UVY8gd32cfQ) &mdash; content and formatting may not be reliable*
