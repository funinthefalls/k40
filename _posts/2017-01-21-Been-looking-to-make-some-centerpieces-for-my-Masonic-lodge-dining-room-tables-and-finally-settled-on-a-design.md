---
layout: post
title: "Been looking to make some centerpieces for my Masonic lodge dining room tables and finally settled on a design"
date: January 21, 2017 00:40
category: "Object produced with laser"
author: "Ned Hill"
---
Been looking to make some centerpieces for my Masonic lodge dining room tables and finally settled on a design.  Made from 1/4" birch ply and 1" pine board.  It's double sided with gold leaf paint in the lettering.  Lol my lodge is loving my laser machine :)



![images/c69053c908a00ae29b7b9399bb2d3d00.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c69053c908a00ae29b7b9399bb2d3d00.png)
![images/e539d9f4db56d152dafb54ceb4929d94.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e539d9f4db56d152dafb54ceb4929d94.png)

**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 21, 2017 13:04*

Wow, that's pretty spectacular. Nice work Ned.


---
**Ned Hill** *January 21, 2017 15:34*

Thanks **+Yuusuf Sallahuddin**.  I'm rather pleased myself on how it well it came out :)  

One of the biggest concerns I had with making this was the mounting of the top symbols to the base.  The mounting area is recessed into  the base but the mating surface is still relatively small, and especially with it being a plywood corner, I was worried about the long term strength of a glue bond.  What I ended up doing was taking my dremel, with a 1/16" ball bit, and carving a series of small undercut holes in both sides of the mating surfaces.  I then glued them together using some Gorilla glue, which expands while it cures, which completely locked the pieces together.  The end result is a bond  that would require breaking wood to get them apart.  


---
**Nick Hale** *January 22, 2017 04:12*

That looks amazing brother!  The checkered base is a nice touch. 


---
**Ned Hill** *January 22, 2017 04:13*

Thanks **+Nick Hale**.  Where do you travel from?


---
**Nick Hale** *January 22, 2017 04:51*

West to the East


---
**Ned Hill** *January 22, 2017 04:52*

What lodge?


---
**Nick Hale** *January 22, 2017 04:53*

910, what about you? 


---
**Ned Hill** *January 22, 2017 04:56*

Lol, Wilmington Lodge 319.  It's in Wilmington NC.  Where is 910?


---
**Nick Hale** *January 22, 2017 05:11*

Perryton texas

![images/274d215885d6ea56eb571a46f5d9e75e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/274d215885d6ea56eb571a46f5d9e75e.jpeg)


---
**Ned Hill** *January 22, 2017 05:12*

Nice, is that your work?


---
**Nick Hale** *January 22, 2017 05:15*

Yes sir, it's to mount over our door at the lodge. Plasma art is the main thing my shop does.


---
**Helcio Xavier** *January 23, 2017 16:03*

Congratulations, my brother, I'm still suffering from the best price search, but when I'm in possession of my K40, I'm going to do some parts for my lodgr. Triplice and Fraternal Embrace.


---
**timb12957** *February 06, 2017 12:08*

My son-in-law is a mason. I would love to try something similar for him. Would you share the graphic you used?


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/NQS6CCrnNrz) &mdash; content and formatting may not be reliable*
