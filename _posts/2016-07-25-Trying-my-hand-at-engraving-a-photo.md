---
layout: post
title: "Trying my hand at engraving a photo"
date: July 25, 2016 13:00
category: "Object produced with laser"
author: "Michael Otte"
---
Trying my hand at engraving a photo. 

![images/f4e5c70d27aaead2d9f358712d8ced83.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4e5c70d27aaead2d9f358712d8ced83.jpeg)



**"Michael Otte"**

---
---
**greg greene** *July 25, 2016 13:48*

what did you use for the software LaserDRW or CorelLaser?


---
**Alex Krause** *July 25, 2016 15:37*

**+Michael Otte**​ what material did you use?


---
**David Cook** *July 25, 2016 19:04*

looks great




---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 25, 2016 21:10*

That came out pretty good.


---
**greg greene** *July 25, 2016 21:18*

I don't get the grey scales though with either LaserDRW or Corel


---
**Michael Otte** *July 26, 2016 05:07*

**+greg greene** I am using CorelLaser and used the instructions from this page [http://support.epiloglaser.com/article/8205/50104/photo-laser-engraving-on-wood-with-CorelDRAW](http://support.epiloglaser.com/article/8205/50104/photo-laser-engraving-on-wood-with-CorelDRAW)

My speed was 150 and 50% power on the stock K40 machine.


---
**Michael Otte** *July 26, 2016 05:08*

**+Yuusuf Sallahuddin**

Thank you...


---
**Michael Otte** *July 26, 2016 05:08*

**+Alex Krause**

its just some thin board I bought from Home Depot, not sure what kind exactly.


---
**Alex Krause** *July 26, 2016 05:12*

**+Michael Otte**​ is it solid or ply?


---
**Coherent** *July 26, 2016 18:46*

Very nice job with the photo engraving!


---
*Imported from [Google+](https://plus.google.com/115689970456066406491/posts/TXtpNKvjn4e) &mdash; content and formatting may not be reliable*
