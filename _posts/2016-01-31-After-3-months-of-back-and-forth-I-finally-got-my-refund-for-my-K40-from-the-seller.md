---
layout: post
title: "After 3 months of back and forth I finally got my refund for my K40 from the seller"
date: January 31, 2016 22:13
category: "Discussion"
author: "Anthony Coafield"
---
After 3 months of back and forth I finally got my refund for my K40 from the seller. Thank goodness for PayPal conflict resolution.



I really want a laser, can't afford anything bigger than the K40, but after the bad experience I'm hesitant to try one again. 



Would a few of you mind sharing your positive stories to counteract the negativity I've been dealing with?





**"Anthony Coafield"**

---
---
**Jim Hatch** *January 31, 2016 23:11*

I got mine last year - took less than 2 weeks to arrive. Although it was left by the UPS guys on its side despite multiple "this end up" labels pointing not in the direction they left it up, there was no damage. The thing was packed very well. After assembling (lousy manual but there was a reminder to pull the packing from the back around the laser tube) it worked out of the box. Didn't really need me to align the mirrors but I futzed with those anyway. Software sucks (the CorelLaser install has a virus) but Adobe Illustrator or Inkscape and the included LaserDRW works fine. I've spent the last couple of months modding it but mostly non-critical. It's a cheap (<$400) laser for me to practice with before upgrading to a serious multithousand $ commercial machine. I'd do it again.


---
**Joseph Midjette Sr** *February 01, 2016 00:11*

Don't let one bad experience cause you to lose interest. I got mine and it was packaged well with the exception of the cd hidden, jammed in place under the tube plate, I noticed the edge of the plastic case and upon further investigation, realized what it was and luckily got it out without breaking it.



My mirrors required no adjustment. The instructions, or whatever they call them are useless, it is best to go to youtube, facebook, google and bing or search out venues such as here to learn what you need.



The board is apparently proprietary so you are limited with the software you can use with the stock board. As such, have patience. LaserDraw in my opinion was just used to test the unit out before shipping and Corel is very frustrating for me as I am used to a more GUI interface like my Repetier which I use exclusively for my 3D printers. I am getting the hang of Corel though through the help of the internet communities.



After researching what it would take to make this less than $400 unit pretty awesome by adding an air-assist nozzle w/laser pointer, air pump, adjustable bed and different control board with user friendly software!, it would cost around $600 additional give or take. Not a bad price to pay to make the K40 do just about anything the $4 to 6 thousand dollar units can do.



I am fairly new to this but have a good mechanical and electronic background so it all seems pretty minuscule to me.



I would try it again if I were you. Just do a search on ebay and make sure they have good feedback! Also, make sure you pay attention and get the unit with the digital power display as mine came with an analog, which is going to be changed real soon.



Don't deny yourself something you "Really Want", due to one bad experience. After you make your first thing with it you will be so happy!

You can do it! Go for it! We all are here to help you succeed too! 


---
**quillford** *February 01, 2016 03:08*

The first one I bought was a scam, but the next one came in two days. 


---
**HalfNormal** *February 01, 2016 03:16*

Bought mine from a company that ships from US. Shipped promptly and even sent an email making sure everything was ok.


---
**Anthony Coafield** *February 01, 2016 03:50*

Thanks heaps everyone. I'll do it again.



I'm in Australia. Would it be worth waiting for the upgraded model with build in air assist and red dot to arrive? Save me a bit of messing around...




---
**quillford** *February 01, 2016 06:21*

Built in air assist is definitely nice. The red dot is relatively easy to add though.


---
**Maldenarious** *February 01, 2016 10:35*

I got mine from a seller on amazon, arrived with a minor dint in its side (no big deal really),  took me a couple of days to get the mirrors aligned, and then i thought it was broken as it wouldnt cut and made some odd noises which i discovered was it arcing 20k volts from the laser into the case! Turned out to be an easy fix, as it was just missing insulation around the electrics - i snipped open a bit of 6mm tubing and siliconed it into place around the electrical joint.  Was a bit of a nightmare i guess, but thanks to the help i got from people on here i did get my issues sorted, and it works much better than i expected it too.


---
**Scott Marshall** *February 02, 2016 03:50*

I got mine from Globalfreeshipping on ebay (Banyan Imports) for <$400shipped. It came in damaged. They settled for $200 refund to cover the repairs. They were fast responding, fair, and professional. The hidden damage was caused by poor packing (loose fan and pump in cutting cavity) in China and was not their doing or fault, and there was no way they could have known, but they settled without complaint.

 I would buy from them again without hesitation. In fact I plan on buying a larger machine in the near future, and will buy it from them.


---
**beny fits** *February 02, 2016 22:24*

**+Anthony Coafield** im in Australia also i got mine within a week as it came from Sydney 

It broke down a week later but the company sent me a new laser tube to fix it 

Turned out to be the power supply but at least I have a spare tube now lol 


---
**Anthony Coafield** *February 03, 2016 01:21*

**+beny fits** Who did you get yours from? I got mine really quickly when it came, it took broke down after a week. How long did it take for them to send you a new laser? Did they send you the power supply also?


---
**beny fits** *February 03, 2016 01:50*

I got mine from ebay 

The tube came from China and took a month i than got the power supply from aliexpres and had it in a week


---
**Anthony Coafield** *February 03, 2016 02:59*

**+beny fits** Thanks beny. They told me the tube would take a month, but still hadn't sent it after 45 days despite saying they would/had/hadn't etc. They then couldn't tell me when they would send it, and it would take 30 more days on top of that...



Do you know which seller you purchased on ebay from? Since they looked after you it would be rather reassuring!


---
**Anthony Coafield** *February 03, 2016 03:06*

**+beny fits** I checked your profile, Quirindi High! You still out that way? I started taking a group of kids out there a few years back to do some community service and a kids club and stuff. Someone else run it now (little kids of my own to look after) but I had a great time out there. 




---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/CHwjeyjnS4Y) &mdash; content and formatting may not be reliable*
