---
layout: post
title: "OK. Got my new K40 and hooked it up"
date: July 10, 2015 17:48
category: "Discussion"
author: "Lars Mohler"
---
OK. Got my new K40 and hooked it up. It is now 2 days old with less then 40 minutes on it. Everything went fine with the set up. Aligned the mirrors, set the current to 10mA and ran a test.

It did fine for about 2 minutes then the engraved image became fainter and fainter until nothing. The laser tube was still firing but with little power showing on the amp meter. This happens no matter where in the print area I run. Pressing the TEST button gives me a strong beam with the correct power. If I let the machine sit for a while, the power comes back but then fades out again. I can burn all 4 corners of the bed with the TEST button, so it is not an alignment issue. The cooling water is not even warm. Yes, I do have good water flow. I checked all the connections. The only thing I noticed was a 5 watt resistor on the Moshi MS10105 v4.85  board was hot.  Any ideas on this one?





**"Lars Mohler"**

---
---
**Jon Bruno** *July 10, 2015 17:50*

How long was it from when it faded out that you did a test fire and got full power?


---
**Jon Bruno** *July 10, 2015 17:55*

I believe the resistor on the moshi board is for current limiting for the steppers. It's functionality would be unrelated to the current of the laser.. 


---
**Lars Mohler** *July 10, 2015 18:05*

I test fired it within 5 seconds of stopping the job.  I also test fired it during a job that was faded to nothing. The beam came up to power when I pressed the button but only fired on the borders of the job.


---
**Jon Bruno** *July 10, 2015 18:35*

Interesting... I was leaning towards the current setting pot as being faulty but if you're getting full power a when you use the TEST fire button then your moshi board is probably faulty. The board commands the laser to fire through a separate line than the test Fire switch. I don't have a moshi board so I'm not certain as to the typical temps of the resistor on the board but if it is getting hotter than expected there could be a faulty component on the board causing the voltage rails that power the laser switching circuits to go low you might just see this happening. I would contact the seller and see if they can warranty the board. Sorry I couldn't be of much more help on this one.


---
**Lars Mohler** *July 10, 2015 18:48*

I noticed that the test fire button did not connect to the Moshi board but directly to the PS. I was leaning toward the same conclusion as you. I'll try to squeeze a Moshi board out of the seller, if not then I will upgrade. Thanks for the help.


---
**Fadi Kahhaleh** *July 10, 2015 21:41*

I doubt the Moshiboard has anything to do except give a "Fire" or "No-Fire" signal to the PSU, think of it as an automatic version of the TEST button.



hmmm, I'd take out the POT from the equation by shorting the IN with 5V directly (i.e going full power) on a small job to see if it suffers from the same symptoms or not.


---
**Jon Bruno** *July 11, 2015 01:57*

Thats the point exactly Fadi. the Moshi board sends a fire signal to the LPSU via separate circuitry. if the resistor on his moshi board is getting excessively hot and is drifting it's value that could cause adverse affects on the power to other components/circuits on the board. namely the triggering circuit that drives the laser fire no fire command.  he already eliminated the POT by testing with a manual fire event and getting a full power result. 


---
**Jon Bruno** *July 11, 2015 02:00*

without a scematic or a moshi board in front of me I can't say for sure but there is probably a transistor being under driven 


---
**Fadi Kahhaleh** *July 11, 2015 04:45*

**+Jon Bruno** , I understand your thought now.. hmm interesting. I do not have a MoshiBoard, and I bought my K40 upgraded already to a LightObject DSP AWC608, my board is of a different brand ...another china-dongle board!



**+triac777** , you wouldn't happen to have an oscilloscope handy would you? 


---
**Lars Mohler** *July 11, 2015 14:13*

No scope :(  Oh wait, my father-in-law has one. What should I scope out?


---
**Lars Mohler** *July 11, 2015 14:50*

I did some V-Meter checks last night. Power from PSU to Moshi board remains stable at 23.9 to 24.3 VDC. Laser trigger voltage from Moshi is stable at 3.8VDC even when the laser fades. It loos like the triggering circuit, which is driven by the Moshi, in the PSU is fading.


---
**Lars Mohler** *July 11, 2015 14:51*

Anybody have the SAMS on this PSU? (Preferably in English).


---
**Jim Bennell (PizzaDeluxe)** *July 11, 2015 16:38*

When you do the mirror alignment usually the burned paper gets the mirrors and lens pretty dirty you might even have some ash laying on the lens inside the laser head. 



You might be surprised how weak the laser gets with dirty lenses and mirrors. I know it seems like a simple answer but give it a once over with a cotton swab and some iso alcohol. 



I had some ash and soot laying on the inside of the lens after my first mirror alignment , had to take the laser head apart and clean the lens and it was fine after that. Good luck hope its a simple fix.


---
**Fadi Kahhaleh** *July 11, 2015 18:46*

**+Jim Bennell**  I had a similar issue, and it took me 15 min of light and gentle scrubbing to get most of it out. God forbid we scratch them lenses hehe...


---
**Jim Root** *July 12, 2015 02:18*

I know it's a long shot but mine worked fine for a short time and then started acting up. Turned out that the model number and serial numbers were not entered correctly in the software. Mine does not have the moshi board though. 


---
**Lars Mohler** *July 13, 2015 15:10*

I tried it again. Re-aligned the mirrors, cleaned the mirrors and lens and no-go. Beam still fades. I would suspect the tube, but immediately after it fades to nothing, I can press the TEST button and get a strong beam. It has to be in the power supply. Probably a transistor or IC in the firing circuit. If the seller doesn't come through with a new power supply, I'll have to reverse engineer the PS. Being that the laser is triggered by  differents input from the TEST and the Moshi board, it shouldn't be too difficult to trace (he said to himself as he reached for his Quaaludes and rum). 


---
**David Richards (djrm)** *July 13, 2015 22:42*

I've not been able to find a schematic but this manual is better than most. [https://www.google.co.uk/url?sa=t&source=web&rct=j&url=http://img.tradeusd.com/100/20140324/b301064c-9c3d-41c9-bd18-342c72298642.pdf&ved=0CDIQFjAFOApqFQoTCJm47rmV2cYCFfIX2wodw-UL9w&usg=AFQjCNH6vN8SxkIOTpV2BamyJlNSbktX3g&sig2=CJSvh-w1TwDKm-WzuZVQZA](https://www.google.co.uk/url?sa=t&source=web&rct=j&url=http://img.tradeusd.com/100/20140324/b301064c-9c3d-41c9-bd18-342c72298642.pdf&ved=0CDIQFjAFOApqFQoTCJm47rmV2cYCFfIX2wodw-UL9w&usg=AFQjCNH6vN8SxkIOTpV2BamyJlNSbktX3g&sig2=CJSvh-w1TwDKm-WzuZVQZA) perhaps one of the ground confections is not made properly and an enable signal is drifting around. Hth David.


---
**Lars Mohler** *July 13, 2015 22:50*

Thank you for the diligent work, David.


---
**Jose A. S** *August 03, 2015 04:43*

Any update on this?...my láser aparently has lost its power with less than 10hrs working...


---
*Imported from [Google+](https://plus.google.com/108272305441097782007/posts/TjFrL14Dvh2) &mdash; content and formatting may not be reliable*
