---
layout: post
title: "Added 4 80mm 20cfm 5vdc fans to my K40"
date: January 17, 2016 21:20
category: "Hardware and Laser settings"
author: "Jim Hatch"
---
Added 4 80mm 20cfm 5vdc fans to my K40. Preliminary results look good but I'm waiting on my new air assist head and upgraded lens from Lightobjects to do full testing. 



I mounted them to the outside front of the cabinet since there's a rail running up against the inside of the cabinet. I didn't want to sacrifice build area by moving the rail away from the front. I could have mounted them on the sides inside but would have had to be careful of the mirror & laser focus heads as they travelled on the XY rails and would then get crosswise airflow vs the exhaust. I wanted to maintain a front to back air intake to exhaust so that put them on the outside.



I have some 40mm fans that would fit inside above the rails but they only run 4.2cfm so 4 of them give me less airflow than just one of the 80mm fans. So exterior mount on the front wins.



I made a template to get the spacing correct and get the center holes right for the hole saw. I used a 3" hole saw to maximize air flow. The fans I got had the grates on the exhaust side so I had to swap them around. I used #8 x 3/4" pan head screws with lock washers and nuts to attach to the housing. The lower holes are tricky as you have to come at them under the interior front rail or skip them and just attach at the tops - going to depend on whether you've already removed the stock bed to get more depth.



I added a 3/8” hole with a grommet (dip it into boiling water for a few minutes before inserting the grommet so it goes in easier than when cold). I'll run the wires through there and into the control side of the box. There's a 5v power connection on the control board mounted to the front of the box so once I get the right connector I'll run the wires inside. Right now I'll just run them to a dual feed USB wall wart. Each pair of fans came wired to a USB connector. The outer pins of the USB connector are power so it'll be easy to club the connectors and swap them to the one I need to mate with the board in the control side of the cutter.



I did not put any filter on the fans. If I decide to in order to prevent any possibility of laser reflections I'll use double sided sticky tape on the inside and use the pink foam blocks that were packed around the laser tube for shipping. They seem to pose little in the way of air restriction. Not sure if I really need it as the fan blades are a major block to anything escaping the cabinet. 



I get appreciable airflow in the box with the fans on and the exhaust off. I can feel air movement from the exhaust port so they do create some positive pressure although it's not a lot. With the exhaust on I do get a nice flow. Unfortunately I don't have a way to measure it with & without the fans on so I'll have to rely on the view of the smoke I create when engraving.



Here are some pics of the mod. 1st one shows the final result. 2nd shows the fans I used and the smaller 40mm ones above them. The 3rd photo shows the fans as well as the template I made up. I spaced the 4 fans evenly across the 19ish inches of the front centered under the lid and 10mm from the top lip under the lid.



Next mod is the new head, gold mirror and upgraded focusing lens. They should be in this week so that will be next weekend's project.





![images/3815447b739db88afd611cd4d1885aa0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3815447b739db88afd611cd4d1885aa0.jpeg)
![images/7f66c23ee3e72f20d298695e4432c9d3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7f66c23ee3e72f20d298695e4432c9d3.jpeg)
![images/8309126fa0311ed46b82e29ddecb43b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8309126fa0311ed46b82e29ddecb43b1.jpeg)

**"Jim Hatch"**

---
---
**ThantiK** *January 18, 2016 00:52*

Sorry to burst your bubble, but this is a really bad mod to do.  You never want positive pressure in the cutting chamber.  All this does is swirl gasses around and every little bit of particulate matter is going to escape from every crevice you have.  You always want vacuum in the cutting chamber, and you want it far enough down your vent pipe that the positive pressure side of the vent is outside. 


---
**ChiRag Chaudhari** *January 18, 2016 00:56*

**+ThantiK** So just the vent without fan and some kind of filter to keep dirt out will do the job correct? 


---
**ThantiK** *January 18, 2016 01:22*

**+Chirag Chaudhari**, yeah - if you need some airflow, a vent with a filter of some sort would help.  You want to keep negative pressure (vacuum) in that area - but allow enough ingress for shop air to flow in and displace the now contaminated air.


---
**ChiRag Chaudhari** *January 18, 2016 01:28*

**+ThantiK** cool thanks, that makes perfect sense to me now. So what type of exhaust you are using. I want to install something just enough so one its not expensive and two not so noisy. I am also planning to install the existing cheap o fan at the very end of exhaust pipe. Say right where the hole in the wall is to help the first one.


---
**Jim Hatch** *January 18, 2016 01:40*

**+ThantiK**​ I think I'll stick with the mod. Since the large lasers (60 & 100W) that I use at our makerspace all have front assist fans, I'd guess there's some value to it. 



I get a lot of smoke & swirl now because the exhaust is restricted by a ridiculously small intake under the back rail and a small hole in the base of the cabinet with only a half inch clearance over the bench. As you noted we need enough ingress for replacement air and these boxes simply don't allow that in stock form.



I don't expect I'll be creating a lot of pressure once it's running with the modified exhaust - I'm cutting out the 8 sq in exhaust to open up the exhaust capacity. The stock exhaust on mine is rated for about 70cfm which will be close to the 80cfm the 4 new fans provide. If I have an issue I can always take one fan offline. I've also got a 120cfm fan I can swap into the exhaust housing.



I'll post real life results when I test it with a sizeable engraving & cutting project.


---
**Jim Hatch** *May 03, 2016 22:50*

I forgot to update this with my real life vs theoretical experience. Contrary to Thantik's warning, I don't suffer any of the swirling or blowing particles he mentioned. It might be that I'm not building enough pressure up to have that happen. What I do get is all of the smoke blowing back towards the exhaust where it is captured by the exhaust fan & hose. 



It's still how my local Makerspace laser is setup also with no issue so I think I'll keep it.



YMMV


---
*Imported from [Google+](https://plus.google.com/114480049764906531874/posts/LXgh5GJoNAH) &mdash; content and formatting may not be reliable*
