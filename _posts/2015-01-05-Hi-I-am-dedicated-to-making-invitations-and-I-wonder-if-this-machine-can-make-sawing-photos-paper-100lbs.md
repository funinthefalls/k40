---
layout: post
title: "Hi I am dedicated to making invitations and I wonder if this machine can make sawing photos, paper 100lbs?"
date: January 05, 2015 02:58
category: "Discussion"
author: "Martha Flores"
---
Hi I am dedicated to making invitations and I wonder if this machine can make sawing photos, paper 100lbs?



![images/632388212415556bfe49829dbee32e69.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/632388212415556bfe49829dbee32e69.jpeg)
![images/3db84ba28c8f184edd46ebbcf3669087.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3db84ba28c8f184edd46ebbcf3669087.jpeg)

**"Martha Flores"**

---
---
**Mauro Manco (Exilaus)** *January 05, 2015 06:49*

use cameo shiulette for this type of cutting....laser can burn papers.


---
**Martha Flores** *January 05, 2015 10:53*

Thank You i have a silhouette cameo but i'm very disappointed with the fabricant, It does not solve my problem and buy her a 6 months ago, i want other machine


---
**Stephane Buisson** *January 05, 2015 13:54*

**+Martha Flores**  Welcome here Martha, about laser cutting , you should know that the intense laser spot burn your material, some burn mark could be left on your model, but all depend on the material (2 different papers could react differently, but you could go for vinyl, etc...). Laser cutting is a test and trial adventure.

as well the spot have a minimum size, so it's a limit in fine details size.


---
**Martha Flores** *January 06, 2015 00:31*

Thankyou 


---
*Imported from [Google+](https://plus.google.com/109844328450154366472/posts/8wqvcqdg2Xs) &mdash; content and formatting may not be reliable*
