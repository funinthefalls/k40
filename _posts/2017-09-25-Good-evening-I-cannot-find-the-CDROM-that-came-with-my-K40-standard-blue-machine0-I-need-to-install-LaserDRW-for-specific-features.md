---
layout: post
title: "Good evening! I cannot find the CDROM that came with my K40 (standard blue machine0, I need to install LaserDRW for specific features"
date: September 25, 2017 05:38
category: "Software"
author: "Samantha Bengtsson"
---
Good evening! 



I cannot find the CDROM that came with my K40 (standard blue machine0,  I need to install LaserDRW for specific features. 



Normally I use ScorchWorks K40 Whisperer,  but for some projects the LaserDRW works better.









**"Samantha Bengtsson"**

---
---
**ki ki** *September 25, 2017 08:48*

Hi Samantha, What board do you have? I ask as I think (please correct me if this is not the case) that the software is different for different cards and dongle colours. I have the nano m2 with the bronze dongle if that can help?


---
**ki ki** *September 25, 2017 08:56*

could this help? 

[plus.google.com - Original software downloads Here is the link to download laserDRW and Winsea...](https://plus.google.com/+HalfNormal/posts/jHwbCDYFKQ4)


---
**Printin Addiction** *September 25, 2017 11:07*

Hi Samantha, 



I received my unit a few weeks ago, and only used k40 Whisperer, what type of jobs are better on LaserDRW?


---
*Imported from [Google+](https://plus.google.com/100018135605268336657/posts/dmLEj7rGNmB) &mdash; content and formatting may not be reliable*
