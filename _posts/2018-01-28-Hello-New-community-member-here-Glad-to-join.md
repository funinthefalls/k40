---
layout: post
title: "Hello! New community member here! Glad to join!"
date: January 28, 2018 02:28
category: "Original software and hardware issues"
author: "Heather Leigh Leishman"
---
Hello! New community member here! Glad to join!



I just pulled out my K40 laser after 7 months of zero use. The tube is getting power and i think its a healthy beam, but nothing makes it to the working plane. Maybe a momentary light makes it initially - but nothing that is able to mark the wood.



Does anyone have a step by step diagnostic process that can help me determine what the issue is? If not, what information should i collect to illuminate the situation.





**"Heather Leigh Leishman"**

---
---
**Kevin Lease** *January 28, 2018 12:40*

Maybe alignment is off 

Put a 3m post it on 1st mirror and pulse laser to see if it makes a spot or not

If it does then proceed with mirror alignment 


---
**Don Kleinschnitz Jr.** *January 28, 2018 12:42*

Try out the links on here:

[plus.google.com - GETTING STARTED?..........NEED HELP? IF YOU ARE NEW TO THIS COMMUNITY, G+, A...](https://plus.google.com/u/0/+DonKleinschnitz/posts/JTjvo89qJEP)



Do a search on laser alignment there are a few guides referenced in the forum on optical allignment.



I would start by checking the beam at each mirror with a sticky note in front of the lens to see what the beam looks like and where it is on the mirror.


---
*Imported from [Google+](https://plus.google.com/105511042398121688544/posts/G55ouWXkMWy) &mdash; content and formatting may not be reliable*
