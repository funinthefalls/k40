---
layout: post
title: "WOW what has happened to the price on the K40?"
date: December 02, 2016 13:34
category: "Discussion"
author: "timb12957"
---
WOW what has happened to the price on the K40? What a dramatic increase in price in such a short period. I paid $318 shipping included, back in July. I just looked on eBay and the best price I found was $759. 





**"timb12957"**

---
---
**Tim Gray** *December 02, 2016 14:27*

Holiday price gouging.   in February they will be back down to $350




---
**Stephane Buisson** *December 02, 2016 14:59*

K40 arrived by batch, temporary run out at entry level price range in your area. (still availble from EU)

Wait for the Market to refresh. 

I also notice a New chinese year effect on the last 2 years.


---
**Alex Krause** *December 02, 2016 15:36*

Supply and demand possibly... Christmas and Chinese new year is comming up... I suspect the price will subside after the first of the year and production increases again in China 


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/8HQsFLdp4ny) &mdash; content and formatting may not be reliable*
