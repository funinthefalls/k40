---
layout: post
title: "Has anyone had any contact with Scott Marshall in the last couple of weeks?"
date: November 28, 2016 18:38
category: "Discussion"
author: "Steve Prior"
---
Has anyone had any contact with **+Scott Marshall** in the last couple of weeks?  I'm still trying to get the limit switch function of my brand new ACR board working and I suspect it's got a solder bridge somewhere, but I have been unable to reach Scott via email or phone for over a week and don't know if he's out due to his health issues.



The problem is as follows:

I've confirmed that both homing switches are normally closed.  The behavior I'm seeing is that Y homing always works as expected, however X homing only works when the Y homing switch is NOT triggered.  I confirmed this electrically - the Y homing output on the ACR is low when the Y is not triggered and high (+5V) when triggered.  But here's where it gets weird - the X homing output on the ACR is low when neither limit switch is triggered, but high if EITHER X or Y homing switch is triggered.  I'm wondering if there is a cross connection on the ACR board, but don't know where to look.





**"Steve Prior"**

---
---
**Alex Krause** *November 28, 2016 21:27*

Optical or mechanical endstops?


---
**Steve Prior** *November 28, 2016 21:56*

Mechanical switches.  It's been a few days so I don't remember exactly, but I checked both switches with a meter and they seemed to work fine.  I've got to go back and see if the 5V limit outputs on the ACR work differently than the 3.3V set that come off the level shifters.  I'm trying to figure out where a short could possibly be for Y to affect X, but for X not to affect Y.


---
**Ulf Stahmer** *November 29, 2016 02:55*

Could it be something with your config file? I know that my y-endstop wasn't working as expected and it was a config issue.


---
**Alex Krause** *November 29, 2016 02:57*

Also what firmware build are you using I know that different builds handle homing differently


---
**Alex Krause** *November 29, 2016 02:59*

If you are using the edge firmware from the cnc build use G28.2 as your homing sequence in Laserweb


---
**Steve Prior** *November 29, 2016 03:59*

Hooray, I figured it out!  As I said in my OP I knew it wasn't a firmware config or homing command issue because I had verified the problem electrically with the machine off - just checking continuity with a meter.  What I discovered was that the ACR might be plug and play for some version of the M2, but not mine - the pinouts on the limit connector are different.  Here's a picture of both.  The way the plug connected to the ACR board had the common wire from both switches plugged into Y instead of GND.  I rearranged the pins in the connector and now the machine homes correctly.

![images/35a25086c36224704211bdccb6f0ddfb.png](https://gitlab.com/funinthefalls/k40/raw/master/images/35a25086c36224704211bdccb6f0ddfb.png)


---
**Steve Prior** *November 29, 2016 04:00*

Just for reference, here's a picture of the M2 board that came with the machine.

![images/c64e7b45fa98a57ffd39f219351d4c6c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c64e7b45fa98a57ffd39f219351d4c6c.jpeg)


---
**Steve Prior** *November 29, 2016 04:01*

I am still concerned that Scott has been out of contact.  There are some other things he was supposed to send me, but since I got things working I can deal with that.  I'm more concerned for whether his health problems have sidelined him.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 29, 2016 06:46*

Hey **+Steve Prior**. I've been working with Scott on his website & there are many more things he was wanting to add up just a while back, but even I've been out of contact with him for a while. I've tried to get in contact also with no luck, so I have a feeling his health has pulled him out of the loop again. Hopefully nothing too serious & we see him again soon.


---
**Mike Mauter** *November 29, 2016 12:39*

The last email that I had from Scott was on the 12th. His health issues were getting progressively worse and limiting how much he was able to do. His regular Doctor had been out of town. They were wanting to put him in the hospital but he was trying to resist going.


---
**Ulf Stahmer** *November 29, 2016 12:49*

Hope Scott's health is on the mend and that he'll be back on the forum soon!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 29, 2016 13:16*

Thanks **+Mike Mauter**. Last I'd heard from him was 10th. Hopefully things are not too bad for him & we'll hear from him the near future. He's a great guy, an asset to the community & wealth of knowledge on a variety of topics.


---
**Bill Keeter** *November 29, 2016 18:10*

yeah, **+Steve Prior** I'm in the same boat. Last I talked with him he was wrapping up my Smoothie ACR for my laser upgrade. But he was saying the docs were trying to put him in the hospital. I've sent him several email since.. no response. Hopefully all is okay with him.


---
**Steve Prior** *November 29, 2016 18:16*

Bill, make sure you look at the diagram I posted last night in this thread and check it when you get yours - could save you a LOT of head banging.


---
**Steve Prior** *December 12, 2016 06:34*

**+Yuusuf Sallahuddin** still no contact with Scott?  Since you work with him do you have an alternate contact to let you check on him?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 06:38*

**+Steve Prior** Unfortunately I haven't heard from Scott still. We did all of our contact via email, so other than that I have no way of finding out if he's okay.


---
**Bill Keeter** *December 12, 2016 19:24*

**+Steve Prior** **+Yuusuf Sallahuddin** That's too bad that no one has heard from him. I've been waiting on the ACR upgrade for quite a few months now. And I would prefer to buy from him. As he's been such a great person for the community. guess I'll continue to hold out until at least the new year.


---
**tyler hallberg** *December 12, 2016 22:26*

Wish I had seen this before I ordered my stuff. Hopefully someone hears from him soon


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 23:58*

**+Tyler Hallberg** Did you recently order through the website? Unfortunately I cannot reverse anything regarding payments (as I don't have access to Scott's payment methods), however I will make a note on the main page & mark all items as "out of stock" temporarily until we can hear from Scott. Apologies from my end, I totally didn't even think of that.



Edit: It seems that all items have been marked as out of stock on the website by myself since prior to Scott's mysterious disappearance. In this case, have you received your items Tyler or are you referring to some other stuff?


---
*Imported from [Google+](https://plus.google.com/+StevePrior/posts/8T3TRqr8eh5) &mdash; content and formatting may not be reliable*
