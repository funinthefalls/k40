---
layout: post
title: "My laser porn"
date: April 05, 2016 19:29
category: "Discussion"
author: "Damian Trejtowicz"
---
My laser porn

![images/a8d86eab7cb42485aab807cea857b25a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a8d86eab7cb42485aab807cea857b25a.jpeg)



**"Damian Trejtowicz"**

---
---
**Damian Trejtowicz** *April 05, 2016 19:31*

And another 
{% include youtubePlayer.html id="iCNiDMMgZkw" %}
[http://youtu.be/iCNiDMMgZkw](http://youtu.be/iCNiDMMgZkw)


---
**Scott Marshall** *April 05, 2016 19:46*

Nice Machine, yours? or you want it to be? Work?


---
**Damian Trejtowicz** *April 05, 2016 20:06*

From work

We have few trumpf's

6kW co2 3020 

5kw 5020 fiber

4kw co2 3530

5kw 7040 5axis 

3.2kw tubematic

4000 bar byjet smart

And few other

So mainly i have contact with laser all days since 1999


---
**Jim Hatch** *April 05, 2016 20:13*

LOL. That looks like the bed I put in my K40. I used those mending plates from Home Depot - they have pointy spikes you hammer into the joist. [http://m.homedepot.com/p/Simpson-Strong-Tie-3-in-x-6-in-20-Gauge-Mending-Plate-MP36/100374920](http://m.homedepot.com/p/Simpson-Strong-Tie-3-in-x-6-in-20-Gauge-Mending-Plate-MP36/100374920) 


---
**Damian Trejtowicz** *April 05, 2016 20:34*

**+Peter van der Walt** i need to do closer look over your p3steel variant,now i have two working graber's.weak point of them is x axis


---
**Scott Marshall** *April 05, 2016 20:37*

The Trumpfs seem to be popular in the US,  I used do industrial control work, and ran into a lot of them, never a 6kw though. Fiber?  I was sidelined by a health issue in 98, so left the business when you started. A kw was a big deal then. You see them in HGR used equipment emails now and then. Still out of my range even used and abused.


---
**Damian Trejtowicz** *April 06, 2016 07:20*

**+Scott Marshall** 

In this year we will get another fiber one

[http://www.uk.trumpf.com/en/products/machine-tools/products/2d-laser-cutting/laser-cutting-machines/trulaser-5030-fiber-5040-fiber.html](http://www.uk.trumpf.com/en/products/machine-tools/products/2d-laser-cutting/laser-cutting-machines/trulaser-5030-fiber-5040-fiber.html)




---
**Damian Trejtowicz** *April 06, 2016 07:47*

Im in uk.its always possible to do jobs

[http://www.intec.uk.net](http://www.intec.uk.net)



Or if its small job and you have dxf i can do straight


---
**Damian Trejtowicz** *April 06, 2016 07:56*

Yes we do that


---
**Scott Marshall** *April 06, 2016 09:17*

**+Damian Trejtowicz** That's quite the machine. 20mm capacity in steel and full sheet capacity. Probably the ultimate tool for cutting sheet stock for manufacturing. That'll take a couple months to get installed and running. Enjoy it, I really miss the work, saw some wild stuff and learned a lot. I didn't really appreciate it until I got sick and it was over.



Guess that's why I play with with small machines. 



Scott


---
**Damian Trejtowicz** *April 06, 2016 09:20*

When we order machine and arive ,its take about two weeks to start using.the biggest one what we have its 3050 l20 from trumpf integrated with fully automated loading and unloading system trustore and liftmaster sort


---
**Scott Marshall** *April 06, 2016 09:58*

You must have quite the team. 13000 Kg is a bear to set. 



Looks like a great company to work for. 



Scott


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/dQmqvDtAK5g) &mdash; content and formatting may not be reliable*
