---
layout: post
title: "Hello, I'm using a k40 since a few months now"
date: January 30, 2019 21:17
category: "Hardware and Laser settings"
author: "Fr\u00e9d\u00e9ric Vieren"
---
Hello,



I'm using a k40 since a few months now. 

It worked very well so far. (around 1 hour daily)



But a few days ago my machine started to work strangly.

I'm trying to know if, I should replace the tube or the Power unit.



Here is how it behave.



At first the cut was more powerfull at 20% thas at 70%. 

(Strange, unconviniant but I lived with it for a week)



I Controlled the signal at the exit of the power control panel and it looks fine. The input value at the entry of the PSU was Around 1.8v DC at 20% and 3.8v at 70%.

My version is the one with Digital display with E-stop, main switch, temp meter;

So the % I mention are the values selected on the red digital panel.  



Now my machine cut in very weak and discontinued, cutting dotted line.



What test may I do ? 

What would be your recomendation ? 

Replace the Tube or the laser Power supply.



Best regards Fred







**"Fr\u00e9d\u00e9ric Vieren"**

---
---
**Anthony Bolgar** *January 30, 2019 21:29*

To me it sounds like it is the power supply. That is a good thing since it is the cheapest option to replace.




---
**Frédéric Vieren** *January 30, 2019 21:44*

Thanks a lot, Anthony for your reply. Do you know any way to test my PSU ? Preferably without killing myself in the process ;-b




---
**Eric Lovejoy** *January 31, 2019 03:35*

**+Frédéric Vieren** the output should be around 2000 Volts (I remember that from some calculations I dide a while ago.)

I forget however what the load of the lamp was you could just guess, I mean I would guess, 40Watts at like 25ma... your typical meter, can maybe handle 600V. So you need  a voltage divider. you can try using the potentiometer, to measure a voltage division, at about 1/4 power you would be looking at 500V. ideally. you want to look at your meter, and know if your meter can handle that Voltage. 

Here is a tutorial on voltage division.

[learn.sparkfun.com - Voltage Dividers -](https://learn.sparkfun.com/tutorials/voltage-dividers/all)



you can then calculate and test your power supply. 




---
**Stephane Buisson** *January 31, 2019 12:31*

Nope the voltage scale is with  a extra zero. Around 15/18 000V. Don't mess with the output for few $. Your life is worth more. Just be patient for your parts to arrive.

Do you check water temp ?


---
**Don Kleinschnitz Jr.** *January 31, 2019 12:51*

**+Eric Lovejoy** **+Frédéric Vieren** DO NOT stick a meter on the output of this supply it runs in excess of 20,000 volts @ 30ma. 

You cannot calculate voltage using simple ohms law on a negative resistance device that is very electrically power inefficient. It is not a simple resistance.

The output of this supply will kill you and any device other than a laser you connect to it!


---
**Don Kleinschnitz Jr.** *January 31, 2019 12:53*

**+Frédéric Vieren** Your best bet is to test all the controls on your LPS and if they are all good then you likely have a bad one. Very common failure.



There is a guide in this post:

[donsthings.blogspot.com - Troubleshooting a K40 Laser Power System](https://donsthings.blogspot.com/2018/05/troubleshooting-k40-laser-power-system.html)



If the controls check out you can most likely assume that the LPS is damaged.



You can do an arc test but I do not recommend it as it is dangerous and sometimes inconclusive.



Do you hear a crackling-hissing sound when you run?



<b>"At first the cut was more powerfull at 20% thas at 70%. </b>

<b>(Strange, unconviniant but I lived with it for a week)"</b>



The supply may run fine at lower powers and when the power is increased the HVT arcs. That may be why it runs better at lower power levels.




---
**Stephane Buisson** *February 01, 2019 12:30*

On top of **+Don Kleinschnitz Jr.** comments, I ask about water because the laser reaction into the tube would decrease with higher temp.  big air bubble reducing waterflow and heat removal also affect efficiency, could explain better result at 20% than 70%. focusing in your daily routine and you could forgot to check your waterline.


---
*Imported from [Google+](https://plus.google.com/112989140429010579534/posts/DXhqchM1Gy6) &mdash; content and formatting may not be reliable*
