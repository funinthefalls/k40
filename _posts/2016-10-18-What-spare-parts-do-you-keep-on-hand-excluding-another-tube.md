---
layout: post
title: "What spare parts do you keep on hand (excluding another tube)?"
date: October 18, 2016 18:47
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
What spare parts do you keep on hand (excluding another tube)? I'm looking for a spare water pump at the moment. My stock K40 came with a Dymax PH1200 pump which has been working just fine. Every few months I pull it out of the bucket and rinse it out when I also rinse the bucket and add fresh water. Do folks recommend a different pump? Looking on eBay, that one appears to run about $50





**"Ashley M. Kirchner [Norym]"**

---
---
**Scott Marshall** *October 18, 2016 18:58*

Pumps are pretty reliable. Optics are the main expendable. All together, these hold up OK once you've got the trouble spots fixed, The Little Giant PE-1 is a good replacement pump.


---
**Ashley M. Kirchner [Norym]** *October 18, 2016 19:36*

Yeah, optics is also on my list of things to buy ... little by little. I haven't spend a whole lot of time researching replacement optics yet, but it's on the short list. Together with an adjustable bed and replacement/upgraded controller.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 18, 2016 20:32*

The only spare parts I keep are materials to laser!!! I just assume that my machine is going to be perfectly good haha. The power of positive thinking!


---
**Ashley M. Kirchner [Norym]** *October 18, 2016 20:43*

Yeah, there's that too ... I have a proverbial truckload of wood being delivered tomorrow. Okay, it's not as much as most, but I also don't have the storage for a huge quantity, and I can get new shipments delivered within a week, so I just keep an eye on the stock and order when it gets low. :)


---
**Ashley M. Kirchner [Norym]** *October 18, 2016 20:44*

Right now I'm researching optics ... again (I say again because everything I researched a few months ago has been forgotten.) Gotta figure out what's best, and doesn't cost an arm and a leg.


---
**Scott Marshall** *October 19, 2016 09:06*

Get the 3 mirror set from Saite Cutter on ebay, The Moly ones are good quality and hold up great and the price is quite reasonable. I've got a year or so on the only set I've had to buy, and they perform like new.



 The lenses they sell are also very good, they have a basic lens, and a 'premium' with no explanation as to what's "premium" about them. I'm quite satisfied with the standard ones and I have one in each focal length which adds versatility to the laser.



 The air assist head they sell has adjustable focus and makes lens changes a snap. I'd imagine you already have an air assist head, I've seen your work and it looks very nice. (If you cut that all with a stock head, you don't know what you're missing)


---
**Ashley M. Kirchner [Norym]** *October 19, 2016 20:01*

Are the MO mirrors noticeably better than the SI ones? As for air assist, I have my own 3D printed nozzle for a fixed lens. I'm happy with it. With an adjustable bed, it works for me. It keeps the flying head light(er).


---
**Scott Marshall** *October 20, 2016 10:24*

**+Ashley M. Kirchner** 



The Moly mirrors are harder than the Silicon ones and last much longer.

 I have not done any side by side comparisons, except to the stock mirrors (which I believe are Silicon) The stock mirrors degraded just from periodic cleaning (and I know how to treat optics, being a competitive shooter for 30+ years) and it didn't take too long.



They gradually fell off until you could start to feel a little warmth on the mount after running. I had maybe a couple of months on them. I read up on the subject and found the Moly ones give up a few tenths of a percent in reflectivity to other types, but are the Moly mirrors are the most durable and usually net better performance than the ones with a higher reflectivity index after a few cleanings. I've seen zero degradation over the last year or so and the surfaces are still visibly flawless.



 I just placed an order for a set for my new laser and paid $24.29 for the set of 3 20mm Moly mirrors from Saite Cutter.



Scott



PS - I haven't forgotten the photos of my farm equipment miniatures, I pulled them out a couple days ago for my next photo session, which should be soon.


---
**Ashley M. Kirchner [Norym]** *October 20, 2016 16:07*

Thanks **+Scott Marshall**​. Reading the descriptions, it says the MO ones are best suited for 60-100W lasers, while the SI ones are for 40-60W. Being that this is a stock tube that's also likely being over driven to 40W (from 35, though I have no data on that), would I lose (some) power using the MO ones? 


---
**Anthony Bolgar** *October 22, 2016 01:46*

Just for info, it is not reccomended to keep a spare laser tube in stock. They have a shelf life, and by the time you need to reolace your current tube, the spare is probably down to half its life expectancy.


---
**Ashley M. Kirchner [Norym]** *October 22, 2016 01:50*

Yeah, that's why I said 'excluding tube' in my post ... I'll get one when I need it ...


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/5aUqePPRc6z) &mdash; content and formatting may not be reliable*
