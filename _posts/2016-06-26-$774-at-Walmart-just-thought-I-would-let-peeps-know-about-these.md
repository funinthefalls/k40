---
layout: post
title: "$7.74 at Walmart ...just thought I would let peeps know about these :)"
date: June 26, 2016 00:50
category: "Material suppliers"
author: "Alex Krause"
---
$7.74 at Walmart ...just thought I would let peeps know about these :)

![images/2492f7ba0f578fefded493f2d6d630fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2492f7ba0f578fefded493f2d6d630fc.jpeg)



**"Alex Krause"**

---
---
**Ned Hill** *June 26, 2016 00:52*

Nice, been needing a new coffee mug. :)


---
**Alex Krause** *June 26, 2016 00:52*

Some dry moly lube and they are a laser canvas :)


---
**Ned Hill** *June 26, 2016 00:54*

Lol was just thinking that as well :)


---
**Ned Hill** *June 26, 2016 01:15*

If you don't have a rotary table can you still etch these as long as you don't make the etching to wide?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 03:12*

**+Ned Hill** Within reason you'd be able to do a small area around the circumference. Could etch the entire length of it though.


---
**Ned Hill** *July 03, 2016 03:33*

Got some CRC moly spray and I went to Walmart this morning and they were all sold out :(


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/YGBdvX7jBDt) &mdash; content and formatting may not be reliable*
