---
layout: post
title: "For those of you that use Fusion 360 to generate the tool or cut path ( maybe old news for some ) , there is a plug in that will export DXF Splines to Polyline which in turn will let you load them into LaserWeb without issues"
date: April 29, 2016 15:36
category: "Software"
author: "Ariel Yahni (UniKpty)"
---
For those of you that use Fusion 360 to generate the tool or cut path ( maybe old news for some ) , there is a plug in that will export DXF Splines to Polyline which in turn will let you load them into LaserWeb without issues . [https://apps.autodesk.com/FUSION/en/Detail/Index?id=4611814297957846949&appLang=en&os=Mac](https://apps.autodesk.com/FUSION/en/Detail/Index?id=4611814297957846949&appLang=en&os=Mac)



![images/bb54013bdea187c455dca32d42e63b84.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bb54013bdea187c455dca32d42e63b84.png)
![images/300614a94055d86b693944f922e118fb.png](https://gitlab.com/funinthefalls/k40/raw/master/images/300614a94055d86b693944f922e118fb.png)

**"Ariel Yahni (UniKpty)"**

---


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/3YLBq6t211Y) &mdash; content and formatting may not be reliable*
