---
layout: post
title: "Hello All..... Fairly new to all this"
date: October 16, 2016 02:05
category: "Original software and hardware issues"
author: "Jared Fontaine"
---
Hello All.....



Fairly new to all this. Doing a lot of cool stuff with laserdraw, but I want to get the plugin for Corel Draw. I have the disc of files, but after I installed corellaser I cannot get the plugin with the engraver options to show up. I have Corel 12. Can someone walk me through how to get the plugin working?





**"Jared Fontaine"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 16, 2016 04:42*

Hi Jared. Is the plugin toolbar appearing up the top right of the CorelDraw or at least down the bottom right near the system clock? Also, you need to run the CorelLaser.exe (after install) instead of running CorelDraw to have the plugin appear at all. It will open an instance of CorelDraw with the plugin toolbar attached (top right of window).


---
**Jared Fontaine** *October 16, 2016 12:31*

**+Yuusuf Sallahuddin** 

Thanks for trying to help! 



No the plugin toolbar isn't appearing anywhere. After I installed CorelLaser I tried to start it and nothing happened. The mouse timer went around like it was going to do something but just don't. LaserDraw and WinSeal will both start, but CorelLasee will not. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 16, 2016 13:27*

**+Jared Fontaine** Did you install CorelDraw before you installed CorelLaser?


---
**Jared Fontaine** *October 16, 2016 20:48*

Thinking about it I am not 100 percent ok which I did first. I am thinking I installed corellaser first, but only the first time. I have tried installing CorelLaser since with Corel draw already on the computer, but the first time was CorelLaser first. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 16, 2016 21:20*

**+Jared Fontaine** Oh, I am not 100% sure whether the order in which you install the two matters, I just phrased it wrongly. What I really meant was "do you also have CorelDraw installed?". So if you have CorelDraw & CorelLaser installed, I suggest opening the CorelLaser.exe file (mine is found in windows 10 in the following location: "C:\Program Files (x86)\3WCAD\CorelLASER\CorelLASER.exe").



That should open up a dialog saying "Winseal XP", then CorelDraw should open with the CorelLaser plugin toolbar located up in the top right corner of the window (near the close). Also, an icon should appear near the windows system clock that you can right click to access all functions.



If that doesn't work, I'd suggest uninstalling everything, go through the install process again starting with CorelDraw first, then CorelLaser.


---
**Jared Fontaine** *October 16, 2016 23:29*

**+Yuusuf Sallahuddin** 

OK. This is what I tried. I uninstalled EVERYTHING that came on the disk. Corel Graphics (Including Corel Draw), CorelLaser, Laserdraw, and WinsealXP. I then extracted the Corel file my desktop and installed from there. I then installed Corellaser from the disk and it still won't work! I re installed Laserdraw and WinsealXP and they both work fine. Could this be an issue with the USB key? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 17, 2016 01:49*

**+Jared Fontaine** I've dropped you a message to try resolve this.


---
**Jared Fontaine** *October 17, 2016 04:09*

**+Yuusuf Sallahuddin** 

Apologies for being hard to work with, but i am new to google+.I can't find a way to check messages on here?? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 17, 2016 04:26*

**+Jared Fontaine** I tagged you in a post on my page. So you should have a notification for it (or just visit my page by clicking my name & you should see it near the top of my posts list)...



edit: unless I tagged the wrong **+Jared Fontaine**


---
*Imported from [Google+](https://plus.google.com/108110207876288908217/posts/2CxUTvYEj3Z) &mdash; content and formatting may not be reliable*
