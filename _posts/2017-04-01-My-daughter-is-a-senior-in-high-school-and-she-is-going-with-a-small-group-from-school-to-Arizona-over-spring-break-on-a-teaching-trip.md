---
layout: post
title: "My daughter is a senior in high school and she is going with a small group from school to Arizona over spring break on a teaching trip"
date: April 01, 2017 01:14
category: "Object produced with laser"
author: "Ned Hill"
---
My daughter is a senior in high school and she is going with a small group from school to Arizona over spring break on a teaching trip.  So I decided to make them some special laser cut luggage tags.  The shape of AZ with the stage flag is on the left and the high school logo is on the right.  Since I only need to make 13 of them I went all out on coloring them.  Cut from 3mm birch ply.  Picked up some leather tag straps cheap from amazon.  [https://www.amazon.com/gp/product/B00AH2M7P2/](https://www.amazon.com/gp/product/B00AH2M7P2/)



![images/d671aa9330f227c67aeaa95df9423f3d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d671aa9330f227c67aeaa95df9423f3d.png)
![images/5a12d08e3d6c164965720a27fb9341c7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5a12d08e3d6c164965720a27fb9341c7.png)
![images/bf5dbec2666c50810e79220947bc760e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf5dbec2666c50810e79220947bc760e.jpeg)

**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 01, 2017 01:55*

I love how the grain of the wood still shows through the colour. Very nice work :)


---
**Ned Hill** *April 01, 2017 01:58*

Thanks :) I used colored sharpie markers so it would be easy.  You can get a nice stained wood effect with them.


---
**Phillip Conroy** *April 01, 2017 02:14*

Nice


---
**josh mozug** *April 09, 2017 01:17*

Those look great!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/bheDTCgQreR) &mdash; content and formatting may not be reliable*
