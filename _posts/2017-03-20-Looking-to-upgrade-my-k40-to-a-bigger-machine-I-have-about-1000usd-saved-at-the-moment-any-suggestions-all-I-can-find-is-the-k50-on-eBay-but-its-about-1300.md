---
layout: post
title: "Looking to upgrade my k40 to a bigger machine I have about 1000usd saved at the moment any suggestions all I can find is the k50 on eBay but it's about 1300"
date: March 20, 2017 02:12
category: "Discussion"
author: "3D Laser"
---
Looking to upgrade my k40 to a bigger machine I have about 1000usd saved at the moment any suggestions all I can find is the k50 on eBay but it's about 1300









**"3D Laser"**

---
---
**Alex Krause** *March 20, 2017 03:03*

Look at a x700 clone... Depending on the time of year it's 1900-2300$  but it should have the size to last you awhile 


---
**Alex Krause** *March 20, 2017 03:08*

If you have proven track record with sales and a down payment a credit union would finance the rest at a reasonable rate... My Ox cnc kit cost me about 30$ a week for 9 months and I financed about 1000 of it and paid for the rest  


---
**Fernando Bueno** *March 20, 2017 10:12*

A few days ago I saw this 50W and 600x400mm work area, but I have no references.[http://www.east-signs.com/products/advertising-equipment/laser-engraver-cutter/1152.html](http://www.east-signs.com/products/advertising-equipment/laser-engraver-cutter/1152.html)

[east-signs.com - 600*400mm working area CO2 laser engraving and cutting machine](http://www.east-signs.com/products/advertising-equipment/laser-engraver-cutter/1152.html)


---
**Mark Brown** *March 20, 2017 12:15*

Do you want more power or just a larger working area?  You might be able to use components off the K40 and just buy a bigger gantry for cheap.  But it might still be hard to beat the cost/value of what Fernando just posted.


---
**3D Laser** *March 20, 2017 12:37*

**+Fernando Bueno** it seems like a great deal until they want to charge you 2k for shipping 


---
**Fernando Bueno** *March 20, 2017 12:41*

Ummm ... I already told you that I had no references to the machine or the store. I simply saw the web looking for spare parts and found them interesting prices, but those shipping costs seem too much.


---
**3D Laser** *March 20, 2017 19:25*

**+Fernando Bueno** I know it just made me laugh when I went to check out is all


---
**3D Laser** *March 20, 2017 19:27*

**+Mark Brown** I would like both to be honest as good as my k40 has been to me I would love to have something bigger and deeper.  Plus my local high school I a chomping at the bits for me to upgrade as I said they could have my k40 once all is said and done 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/63E5qV47QLt) &mdash; content and formatting may not be reliable*
