---
layout: post
title: "I need to be able to flip what I am cutting and accurately burn a matching mirror image on the reverse"
date: May 10, 2017 08:56
category: "Object produced with laser"
author: "Phillip Meyer"
---
I need to be able to flip what I am cutting and accurately burn a matching mirror image on the reverse.

How can I make sure the item stays in exactly the right place when I flip it?



In greater depth: I want to user the laser to engrave on one side of a material, then flip that material and engrave and cut onto the other side but so it exactly matches the first side. The item isn't huge so tolerances are relatively small.



I have come up with the idea that if I have two screws coming up out of the bed and I can very accurately find the centre position between the two screws, I should be able to flip what I am cutting/engraving and work on the other side very accurately. However, I wondered if anyone had tried this or similar before and has a better process already?





**"Phillip Meyer"**

---
---
**Joe Alexander** *May 10, 2017 09:19*

easiest jig iv'e used is a scrap piece of material and cutout perimeter so you have a "slot" to drop the piece in, keeping its location from one run to the next.


---
**Richard Vowles** *May 10, 2017 09:58*

In cnc we use dowel holes.


---
**Phillip Meyer** *May 10, 2017 10:00*

**+Richard Vowles** Yes, I think dowel holes are more or less what I am thinking. I just have to figure out how to rig something up in the K40 so that it doesn't move


---
**Phillip Meyer** *May 10, 2017 10:01*

**+Joe Alexander** I thought of that but the source material I use isn't always the same size. Also, with that method, the 'burn' has to be exactly centred to what you are burning or it won't work.


---
**Ned Hill** *May 10, 2017 11:49*

Maybe I'm not following you clearly, but If you are wanting to etch, cut, flip, and etch again the easiest way is like Joe said.  Since you would be cutting the piece out of another piece that then becomes your instant jig.  If it's something precut that you want to etch, flip and etch again you could always have a steel bed (or just a drop in plate) and use magnets as dowels to hold the piece in place.


---
**Alex Hayden** *May 10, 2017 13:05*

**+Ned Hill**​ that only works if the cutout is the same when you mirror it. If it has say a wedge shape when you mirror it the wedge is then the wrong direction, and thus will not fit back in the cutout.


---
**Ned Hill** *May 10, 2017 13:18*

Ahh good point.  That's tough one.  I've done it before but the piece you are cutting from has to be symmetric and the piece has to be centered in the flip direction that way you flip the whole thing.


---
**Cesar Tolentino** *May 10, 2017 14:17*

**+Alex Hayden**​, what if you don't totally cut through the irregular shape like in CNC where you have tabs. Then cut through a bigger shape just outside of the irregular shape? Then that regular shape can be flipped? Then when done, cut again the irregular shape to be separated from the bigger regular shape.


---
**Jeff Johnson** *May 10, 2017 14:18*

I do something similar with keychains. I engrave and cut on the front then flip and engrave on the back. I cut a tiny hole at the top left and right corners to use as an index point. I reverse the design for the back and make the necessary changes to engrave on the back. Before engraving, I set the laser to really low power and line it up with the index hole after flipping it over and placing the keychains back in reversed. 



I made a video of this and before I posted it, my phone died . . . I can make another simple video I suppose. It's really easy to get precise reverse engraving using this method.


---
**Jeff Johnson** *May 10, 2017 14:32*

Here's the font. Red engraves and black cuts. The index holes are at the top left and right. 

![images/36e97b6d06eb4224ad5ae383c59b7016.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/36e97b6d06eb4224ad5ae383c59b7016.jpeg)


---
**Jeff Johnson** *May 10, 2017 14:35*

This is the back. I copied the front, mirrored it, then changed the engrave layer. I cut on the front because that leaves a cleaner look. I flip the plywood then replace the keychains, back side up. Next I carefully line up the index hole that's now on the top left with the laser head's starting position. Also important here is to have some sort of jig to properly position things. I have a wooden jig that squares off the X and Y and lets me place the material in the exact same position each time.

![images/fc6c7b40597ab1cd28e8b0af6d3b42aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fc6c7b40597ab1cd28e8b0af6d3b42aa.jpeg)


---
**Ashley M. Kirchner [Norym]** *May 10, 2017 15:04*

For irregular shapes, I will lay it out by alternating one row "front" and one row "back". That way when I flip the pieces I can swap their location in the cut out. The file to be engraved is also set up that way of course, one row with the design that goes on the front and the next row with what goes on the back. Then flip all the pieces and swap the row they're in and run the same file again. 


---
**Ashley M. Kirchner [Norym]** *May 10, 2017 15:06*

With this method I don't need an index, don't need to think about how to line up the pieces again. The waste material will serve as the jig. 


---
**Ashley M. Kirchner [Norym]** *May 10, 2017 15:32*

This is what I mean. There are 4 alternating rows here, two of them red (front), two of them blue (back). The red ones get the engraving that goes on the front while the blue get the engraving that goes on the back. Then I cut all the pieces out, flip them over and swap their location. The ones in the red row now go in the blue, and the blue ones in the red row. The waste material stays in the machine till both sides are engraved as it acts as the jig holding the pieces in place. I've done hundreds like this with perfect alignment.

![images/ba7aedab6cf7908aab675d655622993d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ba7aedab6cf7908aab675d655622993d.png)


---
**Phillip Meyer** *May 11, 2017 14:51*

Thank you everyone, I'm going to try flipping the cut out inside, that is a really good way. Exactly half of what I cut is symmetrical and the other half isn't so I will experiment and let you know how it goes.


---
*Imported from [Google+](https://plus.google.com/117194752728291709572/posts/SMRPQGZNy7Y) &mdash; content and formatting may not be reliable*
