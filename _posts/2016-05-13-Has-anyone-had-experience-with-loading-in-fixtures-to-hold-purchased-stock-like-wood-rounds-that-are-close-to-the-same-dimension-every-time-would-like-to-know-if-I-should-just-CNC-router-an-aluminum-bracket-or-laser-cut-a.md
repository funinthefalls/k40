---
layout: post
title: "Has anyone had experience with loading in fixtures to hold purchased stock like wood rounds that are close to the same dimension every time would like to know if I should just CNC router an aluminum bracket or laser cut a"
date: May 13, 2016 06:01
category: "Materials and settings"
author: "Alex Krause"
---
Has anyone had experience with loading in fixtures  to hold purchased stock like wood rounds that are close to the same dimension every time would like to know if I should just CNC router an aluminum bracket or laser cut a guide out of 3/8 inch mdf





**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 06:09*

As I don't have a cnc I would go the laser cut out of ply/mdf method. But, an aluminium bracket would be better in the long run as it won't warp over time.


---
**Larry Baird** *February 08, 2017 04:24*

I make leather coasters and use two layers of thin plywood cutting circles out of the top layer that hold the parts. The layers are held together and to the table using two sided tape.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/6po5c2EDjKG) &mdash; content and formatting may not be reliable*
