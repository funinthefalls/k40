---
layout: post
title: "Hi All. Small question. When i cut with LaserDRW3 cutter cuts simle line with only one pass"
date: April 11, 2016 15:56
category: "Discussion"
author: "MNO"
---
Hi All.



Small question. When i cut with LaserDRW3 cutter cuts simle line with only one pass. But with CorelLaser it does simple line 2 times.



I like corel laser because it reads my QCAD drawings. 

Is there an option to disable second pass?





**"MNO"**

---
---
**Gary McKinnon** *April 11, 2016 16:03*

Yes, you'll find that setting on Google under 'corel laser number of passes', amazing! 



[https://www.google.co.uk/search?num=100&site=&source=hp&q=corel+laser+number+of+passes&oq=corel+laser+number+of+passes&gs_l=hp.3..33i21.499.4257.0.4347.29.28.0.0.0.0.160.2942.11j16.27.0....0...1c.1.64.hp..2.15.1532.0.4qskj3x-Zwg](https://www.google.co.uk/search?num=100&site=&source=hp&q=corel+laser+number+of+passes&oq=corel+laser+number+of+passes&gs_l=hp.3..33i21.499.4257.0.4347.29.28.0.0.0.0.160.2942.11j16.27.0....0...1c.1.64.hp..2.15.1532.0.4qskj3x-Zwg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 16:14*

You may find that this is an issue with the line width. Set the line width to 0.01 in CorelLaser. Any higher & it will cut on both sides of the line.



Another option, is for solid shapes, have no border & solid fill colour.


---
**MNO** *April 11, 2016 16:44*

that is not really helpful. And Yes i did use google. :)

I know by using fill i can cut some things out

But my case is little different... I'm trying to cut button like shapes. So it looks like 

rectangle missing one wall. For those i need to cut shape but not full. 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 16:54*

**+MNO** Can you attach a link to a picture/sketch of what you're trying to do? Then maybe it makes it easier for us to determine what you need help with. Your explanation vaguely makes sense to me, but I understand things 1000x easier with images.



When you say "button like shapes", are you referring to buttons like on a shirt? or buttons like on a remote control/keyboard? or something else entirely?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 16:58*

Are you referring to something like this?

[https://drive.google.com/file/d/0Bzi2h1k_udXwdWVzT0NndFZ3bms/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwdWVzT0NndFZ3bms/view?usp=sharing)


---
**MNO** *April 11, 2016 17:01*

yes that is exactly what i mean 


---
**MNO** *April 11, 2016 17:02*

i hate that I cant paste photos ... 








---
**MNO** *April 11, 2016 17:04*

I found that it is problem with data setting . when using WMF for cutting it makes 2 time pass but when using EMF it draws only one time i need to check only if it holds dimensions...



edit...

Yes it is ok now setting it to  EMF it cuts it only with one pass



Question is there other forum or wiki?  It would help noobs like me to get info like that, and i would gladly share those infos when learning...



  


---
**Gary McKinnon** *April 11, 2016 17:14*

**+MNO**

Sorry i was glib but it was helpful, this was one of the search results, linewidth has to be less than 0.13 : [https://tkkrlab.nl/wiki/Laser_Cutter](https://tkkrlab.nl/wiki/Laser_Cutter)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 17:42*

**+MNO** I've left it at WMF, as it works. Also, 0.01mm line width & it only cuts once on the line. I've tried 0.1mm line width & it cuts twice. Again, set your line width on all lines/shapes that you are trying to cut to 0.01mm.



**+Gary McKinnon** I'm not certain if that 0.13 is in pt or mm or some other scale. I set my line width to 0.01mm in Adobe Illustrator before I import into CorelLaser. 0.1mm cuts twice.


---
**MNO** *April 11, 2016 20:43*

i will test it tomorrow. and post some more info.

Till now i read a little about EMF. it is newer version of WMF with some addons and 32 bit. ok i think i need to do some testing


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 02:28*

**+MNO** Let us know how it goes with testing & with the EMF. I'd be interested if there are different/better options to set for the software.


---
*Imported from [Google+](https://plus.google.com/111888830486900331563/posts/N5hX64YjNnC) &mdash; content and formatting may not be reliable*
