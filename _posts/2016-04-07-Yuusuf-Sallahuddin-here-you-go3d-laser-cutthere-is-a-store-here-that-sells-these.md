---
layout: post
title: "Yuusuf Sallahuddin ...here you go....3d laser cut...there is a store here that sells these"
date: April 07, 2016 17:52
category: "Object produced with laser"
author: "Scott Thorne"
---
**+Yuusuf Sallahuddin**...here you go....3d laser cut...there is a store here that sells these.

![images/2af3e17526c57d12372f469651673300.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2af3e17526c57d12372f469651673300.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 18:23*

That's pretty cool. Looks very christmassy.


---
**Scott Thorne** *April 07, 2016 18:32*

**+Yuusuf Sallahuddin**...we are in Helen Georgia....it's an Alpine modeled town in the mountains...lots of arts and crafts shops here.


---
**HalfNormal** *April 07, 2016 20:50*

**+Scott Thorne**  Spent 18 years in Dunwoody GA a few hundred yards off the  Chattahoochee river. 


---
**Scott Thorne** *April 07, 2016 21:08*

**+HalfNormal**...I'm looking at the hooch right now brother...love it here....I live in Newnan Ga....where are you now? 


---
**HalfNormal** *April 07, 2016 21:14*

**+Scott Thorne**​  Born and raised in Phoenix Az. Moved to Ga in 1983. Now live in Flagstaff Az.


---
**Scott Thorne** *April 07, 2016 21:17*

I travel to Phoenix around 3 times a year...my company sends me there to do work at one of our plants off of the 10 and 51st...small world...lol


---
**I Laser** *April 07, 2016 22:49*

Here's the local competition, laser cut scenery and models for war gaming. Some absolutely crazy stuff done in MDF



[https://www.miniaturescenery.com/index.asp](https://www.miniaturescenery.com/index.asp)



Check this one out for instance, in 3mm MDF: [http://bit.ly/1oGSJ8k](http://bit.ly/1oGSJ8k)


---
**Scott Thorne** *April 07, 2016 23:16*

**+I Laser**...absolutely amazing...my problem is learning the drawing software...once I learn that....there's no stopping me.


---
**I Laser** *April 07, 2016 23:29*

**+Scott Thorne** I know the drawing software. My problem is a lack of artistic skill... :P 



Though after seeing Yuusef's creation, I have the bug now and am going to try making something.


---
**Scott Thorne** *April 07, 2016 23:38*

**+I Laser**...I'm just the opposite....I have the skills...I just don't know how to draw them up...lol...I'll be home tomorrow I'm gonna give it a shot. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 01:04*

**+I Laser** Wow, that is absolutely hectic those miniature wargame models.



Fortunately for me I am both artistic/creative & have the skills to draw things up in computer software. Except 3D software currently. I always hated 3D drawing on PC. Also don't ask me to add colour to anything. I am horrible at choosing colour/mixing them.



I'm looking forward to see what you create also.



**+Scott Thorne** What software do you use for drawing?


---
**Scott Thorne** *April 08, 2016 01:07*

**+Yuusuf Sallahuddin**...corel x7....inkscape.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 01:24*

**+Scott Thorne** Aside from using the CorelLaser/CorelDraw12, I haven't used CorelDraw since I was about 15. Also, haven't really had much use of Inkscape as yet either. Might have to give Inkscape a go & have a trial of Corel x7-8 sometime in near future.


---
**Scott Thorne** *April 08, 2016 01:45*

**+Yuusuf Sallahuddin**...I'm new to these drawing programs...I've never had a need to use them until now...I watch a lot of YouTube tutorials...but I'll get the hang of it. 


---
**Ulf Stahmer** *April 08, 2016 02:53*

**+Scott Thorne** Here's a good guide for Inkscape [http://tavmjong.free.fr/INKSCAPE/MANUAL/html/](http://tavmjong.free.fr/INKSCAPE/MANUAL/html/)
It's quite a powerful piece of software!

I love the laser cut architecture!  I going to have to try to make some myself!


---
**ED Carty** *April 08, 2016 08:39*

**+Scott Thorne**  Great work Bro


---
**ED Carty** *April 08, 2016 08:39*

**+Ulf Stahmer** Good info


---
**ED Carty** *April 08, 2016 08:39*

**+Yuusuf Sallahuddin** You produce nice things already


---
**Scott Thorne** *April 08, 2016 11:49*

**+ED Carty**...these pieces aren't my work...I saw them at a store...I only took the photo because we were talking about 3d objects made with the laser cutter...I wish I could make something like that...lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 11:51*

**+Scott Thorne** In amongst the conversations, Josh Rhodes linked a software called Autodesk 123D. [http://www.123dapp.com/make](http://www.123dapp.com/make)



If you take a look at the short video there, this software makes it look extremely easy to make things like this from 3d models.


---
**Scott Thorne** *April 08, 2016 11:52*

**+Ulf Stahmer**...thanks bro...I needed this. 


---
**Scott Thorne** *April 08, 2016 11:55*

**+Yuusuf Sallahuddin**...that autodesk program looks great...I think I'm going to try it. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 12:14*

**+Scott Thorne** Yeah I just tested it now on the sphinx that I previously tested with other methods. Works extremely great. Can output cut plans as PDF, which I believe could then be used to create laser cut files.


---
**Scott Thorne** *April 08, 2016 12:36*

I'll be home in a few hours...I'm gonna try it out.


---
**Ulf Stahmer** *April 08, 2016 19:31*

**+Yuusuf Sallahuddin** Inkscape can import pdf files as editable vector images.  About 20 Years ago I did some illustrations using AI 3.  The new version of AI wouldn't open them, so I reinstalled AI 3 in an emulator and saved the original files as a pdf (version 1).  I was able to open these files in Inkscape and edit them!  I was happy and impressed.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 19:36*

**+Ulf Stahmer** Adobe Illustrator CC 2015 is what I am running & it allows me to import PDF also. Inkscape looks to be a decent software too. And much cheaper than Adobe suite. It's just a matter of switching over at some point & learning the differences.


---
**Coherent** *April 09, 2016 13:29*

**+HalfNormal**

Ahh a fellow Arizonian! I work in Flagstaff daily, but live down toward Sedona. Scott, search the CNCZone forums. I've seen a few threads where folks share drawings/files of 3d models like these and of animals & vehicles etc. I've also seen links to places that sell the plans and cad files for this kind of stuff. This may have already been listed elsewhere, but they have thousands of plans here...(this link is to the houses section) [http://www.makecnc.com/browse-store/premium-patterns/buildings/houses/](http://www.makecnc.com/browse-store/premium-patterns/buildings/houses/)


---
**Scott Thorne** *April 09, 2016 13:36*

**+Coherent**...man I missed going to Sedona last time I was in Phoenix..I'll be back out there in a month or so. 


---
**Coherent** *April 09, 2016 13:50*

Scott, I Just added a link to place that has lots of plans.. you may already be aware of it.


---
**Scott Thorne** *April 09, 2016 13:56*

**+Coherent**...thanks man..I'm going to check it out. 


---
**HalfNormal** *April 09, 2016 15:15*

**+Coherent** I had a place in Sedona for a year-and-a-half before finally moving to Flagstaff. I work at the hospital and have to be on call which means at the hospital within 20 minutes. That's a little rough when you're down in the Sedona area! We'll have to get together and compare notes sometime. When **+Scott Thorne** is in town might even go get a beer!


---
**Scott Thorne** *April 09, 2016 15:21*

**+HalfNormal**..**+Coherent**...that sounds like a plan brother...I'll let you guys know when my travel plans are set in stone. 


---
**Off-grid Denmark** *April 11, 2016 08:57*

What's the stores name/url? :)


---
**Scott Thorne** *April 11, 2016 09:47*

**+Jens Chr. Bøgh-Boysen**...this is not an online store.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/g8drsRUXN3x) &mdash; content and formatting may not be reliable*
