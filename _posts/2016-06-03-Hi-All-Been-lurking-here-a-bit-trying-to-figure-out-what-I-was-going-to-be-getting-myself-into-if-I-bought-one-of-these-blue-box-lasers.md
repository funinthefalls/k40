---
layout: post
title: "Hi All, Been lurking here a bit trying to figure out what I was going to be getting myself into if I bought one of these blue box lasers"
date: June 03, 2016 22:54
category: "Discussion"
author: "Ned Hill"
---
Hi All,

Been lurking here a bit trying to figure out what I was going to be getting myself into if I bought one of these blue box lasers.  Thanks to all the great info and support I've seen in this group I final decided to pull the trigger and bought one off of Ebay.  [http://www.ebay.com/itm/291776502952](http://www.ebay.com/itm/291776502952)

Seems to have more bells and whistles then I have seen in other of these types of lasers.  Ordered an air assist, high quality lens and Mo mirrors from LO to upgrade it based on what I've seen recommended here.  I'll post what the actual quality of the stock setup turns out to be relative to the upgrades.  :D





**"Ned Hill"**

---
---
**Jim Hatch** *June 03, 2016 23:12*

Nice set of additional features - the master power switches/plugs and the digital display (with temp monitoring display capability), LED lights are good. I paid the same price for one without any of those and added them myself. You've got the right initial upgrades from LightObject ordered too.


---
**Jarrid Kerns** *June 04, 2016 01:37*

Screw it, I guess I'll buy one too! Freeburn2 on the back burner again for now. Spent about 750 so far on it. Now 389 on this! I'll make the freeburn2 much better after I learn mistakes with this


---
**Steve B. (Thomas delbroc)** *June 04, 2016 02:55*

Which specific upgrade items are you purchasing from the LO store?  (Just trying to collect data for a future purchase.)


---
**Ned Hill** *June 04, 2016 03:32*

LSR-LH1820B	18mm Laser head w/ air assisted. 



LSR-ZNSE1850HQ	High quality 18mm ZnSe Focus lens (F50.8mm)	



 LSR-MIR20MO	20 mm Molybdenum (MO) Reflection Mirror (x3)	

 




---
**Ned Hill** *June 04, 2016 03:39*

The price is cheaper on Ebay for lens and mirrors but I decided to go with the LO items since this group seems to attest to quality of LO items.  I'm a newbie on laser cutter/engravers so I'm looking for the least amount of hassle as reasonably possible for an entry level machine.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/E3MS9pG3d7J) &mdash; content and formatting may not be reliable*
