---
layout: post
title: "Does anyone have a GRBL config and setup for a K40 laser?"
date: October 07, 2016 22:36
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
Does anyone have a GRBL config and setup for a K40 laser?





**"Anthony Bolgar"**

---
---
**HalfNormal** *October 07, 2016 23:35*

**+Anthony Bolgar** 

Search for Timo. Here is his work with GRBL[https://github.com/McNugget6750/K40-grbl-board](https://github.com/McNugget6750/K40-grbl-board)

[github.com - McNugget6750/K40-grbl-board](https://github.com/McNugget6750/K40-grbl-board)


---
**Anthony Bolgar** *October 07, 2016 23:36*

Thank you for the link.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/JpkDNarxj6G) &mdash; content and formatting may not be reliable*
