---
layout: post
title: "Hi Everyone, This post is a review on my experience with upgrading my standard K40 to an aftermarket Controlboard"
date: November 17, 2017 07:59
category: "Modification"
author: "Andrew Brincat"
---
Hi Everyone, 

This post is a review on my experience with upgrading my standard K40 to an aftermarket Controlboard. I had used a standard K40 for about 5 years until it died one me, and with the newly purchased one having issues, I decided to go aftermarket and not deal with the Chinese “support” I was getting from eBay



After posting on here for help, **+Paul de Groot** was kind enough to offer me one of his test boards ([http://awesome.tech/](http://awesome.tech/)) to see how I went with performing the upgrade myself.

The physical upgrade was very easy, only one extra wire to install (used an old CPU fan cable as it had the right female socket) an Endstop sensor flag and rotating the Endstop sensor. Everything else plugged directly into the board, no fuss!



Connection to PC is via USB (micro – same as Samsung phones) and the software used is Inkscape + extensions within Inkscape (both Raster engraving and cutting) - [http://awesome.tech/using-the-gerbil-plugins/](http://awesome.tech/using-the-gerbil-plugins/).



I mainly use my K40 for engraving woodwork, which I have not been doing much of recently, so have only now just gotten around to really getting stuck into playing with the setup and getting to know the new process and controls. 



All in all I can say it has been a very easy process, Paul has been great with helping me out with all my questions (I work in IT but have 0 experience in anything like this, GRBL, GCode and CNC type stuff in general) and although I am still playing around with the fine tuning of settings, can say that the amount of extra control and detail I can get now is amazing! There is plenty of information available for beginners like me at awesome.tech and GitHub ([https://github.com/paulusjacobus/grbl/wiki/Configuring-Gerbil-Grbl-v1.1e](https://github.com/paulusjacobus/grbl/wiki/Configuring-Gerbil-Grbl-v1.1e))



Attached some pics of my most recent tests



Again, massive thanks to **+Paul de Groot** for helping me out, so glad I went this route! 



Cheers





![images/43a8e994bcb1052130860fd20d571969.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43a8e994bcb1052130860fd20d571969.jpeg)
![images/e4d589da9b3ce90b6b5da4e57131db36.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e4d589da9b3ce90b6b5da4e57131db36.jpeg)
![images/61e9a2aa49fe9782ef14ff12e94587d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/61e9a2aa49fe9782ef14ff12e94587d2.jpeg)
![images/7f54f848f4bebd4aaccdb73eaa302022.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7f54f848f4bebd4aaccdb73eaa302022.jpeg)

**"Andrew Brincat"**

---
---
**Paul de Groot** *November 18, 2017 03:07*

Thanks Andrew for giving me your feedback which helped setting up the configuration and installation pages! Love the darth vader.


---
**Graham Kendall** *November 26, 2017 23:03*

This is good to hear, I am about to order one of these boards. One question. I understand you use inkscape, but it does not have the laser functions on it. which program do you transfer the image to, to actually get the laser to cut/engrave it ?


---
**Paul de Groot** *November 27, 2017 00:10*

My latest Inkscape plugins do have the ability to stream the g-code straight to the controller. However you can stream the g-code file separately if needed. I have used CNCjs which is free and open source. The advantage of CNCjs is that there is a command line to interrogate the controller for its settings and that you can jog the laser head to a certain position


---
**Ned Hill** *December 01, 2017 21:32*

**+Andrew Brincat** So, are you making those pens  from the Shawshank Tree or is that a pen you just bought made from the tree?  




---
**Andrew Brincat** *December 01, 2017 21:45*

Hey Ned, I actually have several legit pieces from the Shawshank tree (Oak). Did a bit of research before buying but turned and etched this pen myself :)


---
**Ned Hill** *December 01, 2017 21:46*

**+Andrew Brincat** that’s awesome.  I love the inscription you engraved. 


---
**Andrew Brincat** *December 01, 2017 21:50*

**+Ned Hill** Thanks Ned! 


---
**Timothy Rothman** *December 17, 2017 02:21*

How'd you make the Darth Vader, was it on the K40 with the Awesome.tech board?  What were your settings?  Can you share a similar file so I can experiment with it and understand the basic setup.  


---
*Imported from [Google+](https://plus.google.com/+AndrewBrincat/posts/6cKbKsDCdPA) &mdash; content and formatting may not be reliable*
