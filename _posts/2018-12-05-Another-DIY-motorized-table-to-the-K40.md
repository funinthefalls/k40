---
layout: post
title: "Another DIY motorized table to the K40"
date: December 05, 2018 03:08
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Another DIY motorized table to the K40



[https://www.thingiverse.com/thing:3251638](https://www.thingiverse.com/thing:3251638)





**"HalfNormal"**

---
---
**James Rivera** *December 05, 2018 04:08*

Nothing against 3D printers (I have built more than one) but it would be nice to see people use their laser cutters to make their upgrades. 🤔


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/U7tHVd8Gq1H) &mdash; content and formatting may not be reliable*
