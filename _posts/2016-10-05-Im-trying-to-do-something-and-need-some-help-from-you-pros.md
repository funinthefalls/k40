---
layout: post
title: "Im trying to do something and need some help from you pro's"
date: October 05, 2016 22:58
category: "Object produced with laser"
author: "Bob Damato"
---
Im trying to do something and need some help from you pro's. I want to make a wood laminate (marquetry) table of my car from a photo I took. That means I need to really dumb it down and posterize it to only a couple of colors. The plan is to get the veneer and cut out the image then another color, do it again etc then put it all together like a puzzle.  



I have a generic laser, stock board, and use corellaser. I have photoshop to manipulate the image though and Im pretty good in that. 



final effect should be something like this (Some photo I got off the net from google)

[http://www.woodfrog.ca/media/catalog/product/cache/1/image/88b52d71fa1e9f47b99e912c8c4fb00e/k/7/k798_corvette_1959.jpg](http://www.woodfrog.ca/media/catalog/product/cache/1/image/88b52d71fa1e9f47b99e912c8c4fb00e/k/7/k798_corvette_1959.jpg)





Ill post my photo so far in the next comment.



My issue is getting it into corel laser as a decent vector format. No idea. If you look at the front bumperette for example, in my picture, it is too complex for the laser. It would never work. I could probably use photoshop and simplify it but Id really love to find a plug in or process to really posterize it, vectorize it and import it into corel laser  to cut.



Any tips greatly appreciated. Otherwise Ill have a lot of kindling this winter. :)







**"Bob Damato"**

---
---
**Bob Damato** *October 05, 2016 22:59*

This is what I have so far. Check out the front. Too complex for a laser.

![images/b4b9ac11ecb1e0755eaa8026544e6203.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b4b9ac11ecb1e0755eaa8026544e6203.jpeg)


---
**Vince Lee** *October 06, 2016 00:07*

Through an online search I found [vectormagic.com](http://vectormagic.com).  It seemed to do what you might need.

![images/29f8276ece6bc370cbfe8361b1d5c5f2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/29f8276ece6bc370cbfe8361b1d5c5f2.png)


---
**Jim Hatch** *October 06, 2016 00:09*

You might want to try one of the image effects like pen & ink which will make it look like a drawing.



Or (& I know this is sacrilege in a world of CAD software) print it out and do a tracing by hand with a sharpie marker. It'll let you simplify the design while retaining the basic details. Then scan that in and do an outline trace.


---
**Bob Damato** *October 06, 2016 00:14*

Thank you both. **+Vince Lee** I had no such luck in my searches but that looks great! Ill try both methods and see how it goes!




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 06, 2016 12:39*

If you have Illustrator, you can use the LiveTrace feature & there are various options (e.g. 6 colours, 16 colours, Shades of Grey, Silhouette, etc) which will then basically posterise & create vector for you.


---
**Bob Damato** *October 06, 2016 22:25*

**+Yuusuf Sallahuddin** I dont really have illustrator. I did for a while, but you might as well explain differential calculus to my goldfish. I just wasnt getting it.  Granted I cant say I gave it the college try but it seemed like it would be a steep learning curve.  Maybe I should try again? Too bad adobe wants $20/month to play with it though. That kind of hurts.


---
**Jim Hatch** *October 06, 2016 23:07*

Corel (at least the X series versions) has a similar feature. You could always pick up an older version on eBay that's installable (just not the Home & Student version). You still run Corel on your machine and don't have to pay monthly charges for the privilege.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 07, 2016 03:22*

**+Bob Damato** I am fortunate enough to have a subscription for the Adobe suite until sometime in 2017 (then I have to look at whether it is worth me paying for it anymore). I found AI reasonably easy to learn, but I was switching from previous Adobe Fireworks software (which has similar vector editing features) & was very similar to that (except better). But as Jim says, there is a similar feature in Corel Draw, so might be cheaper to go that route. It's possible (although I don't know) that there may be a similar feature in Inkscape? Anyone know if LiveTrace exists in Inkscape?


---
**Nick Williams** *October 10, 2016 07:11*

**+Yuusuf Sallahuddin** 

Ink Scape:

Goto Path menu:

"Trace Bitmap"

Cheers


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/3VH7kd28LMX) &mdash; content and formatting may not be reliable*
