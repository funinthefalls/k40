---
layout: post
title: "K40 laser internal lighting. What has everyone done about internal lighting on there k40?"
date: February 15, 2017 13:28
category: "Modification"
author: "Rob Kingston"
---
K40 laser internal lighting.



What has everyone done about internal lighting on there k40? I got a 12v LED strip and stuck it on but they constantly fall off!





**"Rob Kingston"**

---
---
**greg greene** *February 15, 2017 13:44*

Peel off the backing and stick it on the edge of the plexiglass


---
**Jim Hatch** *February 15, 2017 13:49*

This stuff sticks to anything :-)



[amazon.com - Amazon.com: Scotch Exterior Mounting Tape, 1-Inch by 450-Inch: Home Improvement](https://www.amazon.com/gp/product/B001KC08ZE/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)




---
**Ned Hill** *February 15, 2017 14:18*

I attached a couple of strong magnets to back of my lighting strip and it doesn't go anywhere.


---
**Jeff** *February 15, 2017 15:14*

I had the exact same problem with the light strip adhesive.  I ran a thin strip of Elmer's clear clue along the length of the light strip and pressed it in place. Then I put the same length of blue painters tape over it from end to end to hold it in place for a day or two.  It hasn't fallen off since.  Be sure to clean the inside of the laser surface with alcohol first to get all the laser residue off.


---
**Richard Vowles** *February 15, 2017 17:47*

Mine came with an interior bulb


---
**Antonio Garcia** *February 16, 2017 18:42*

I put super glue in 3 or 4 points in each strip.... 


---
*Imported from [Google+](https://plus.google.com/104868032632666700277/posts/7fDTV6Hh4Up) &mdash; content and formatting may not be reliable*
