---
layout: post
title: "The Redsail LE400 conversion is almost complete"
date: September 17, 2016 14:02
category: "Discussion"
author: "Anthony Bolgar"
---
The Redsail LE400 conversion is almost complete. I managed to get the 3 axis of motion all working (will be so nice having a motorized Z axis ) and the limit switches installed. Last major hurdle is now to get the laser tube hooked up and tested (Still not sure if the tube is good or bad). If it works, great, if not, there is room to replace the 40W tube with a true 50W tube. Once the tube is figured out, I just need to make a new controill panel and button it up. The end is actually in sight finally. For now  I will use the water pump and 5 gallon bucket to cool the tube, but I have a 240mm PC radiator and twin 120mm fans on the way to replace the cooling system with, it will be a closed loop system. And of course I need to install door interlocks, flow sensor and water temp sensor using [https://github.com/funinthefalls/LaserSafetySystem](https://github.com/funinthefalls/LaserSafetySystem)





**"Anthony Bolgar"**

---
---
**Ulf Stahmer** *September 17, 2016 19:28*

Great to hear that the Redsail conversion is progressing.  I'm in the process of installing your safety system ever since I inadvertently test ran my K40 without the coolant system or the fan plugged in.  Fortunately, I saw the smoke coming out of the front vents and hit the stop switch after about 15 seconds of lasering at low power.



I couldn't get the arduino libraries you included to work.  Had compile errors, but I found these libraries: [bitbucket.org - fmalpartida / New LiquidCrystal / Downloads — Bitbucket](https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads) and they seem to work for me.  I'm also hoping to change out the temperature sensors with waterproof DS18B20 sensors.  I'll let you know when I get it working.  It'll be at least a week as I've just arrived in Japan for business.



One other note, you may want to add a comment in your code that the LCD connections are from A4 on the nano to the SDA pin on the I2C LCD board and A5 to the SCL pin.


---
**Anthony Bolgar** *September 17, 2016 21:03*

Thanks for pointing that out, will comment the code. As to the libraries, it seems hit and miss wether or not they will work for people. Glad to see you found some that worked for you, I will add them to the repo as an alternative for others who may have problems. Thanks.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/jih5YvSiwLN) &mdash; content and formatting may not be reliable*
