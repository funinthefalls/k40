---
layout: post
title: "I am considering getting this to power my air-assist, as it has a 3L tank & supposedly outputs 20-23L per minute"
date: June 27, 2016 22:07
category: "Air Assist"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
I am considering getting this to power my air-assist, as it has a 3L tank & supposedly outputs 20-23L per minute. Price seems reasonable @ AU$106 with free shipping.



Any thoughts on whether this seems decent enough to power air-assist? I imagine that it would be much better than my current setup of aquarium pumps.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 22:11*

Actually, found the exact same thing from another seller for only AU$69.99.

[http://www.ebay.com.au/itm/NEW-1-6-HP-AIR-BRUSH-COMPRESSOR-SPRAY-GUN-1-8HOSE-TANK-/200657637579?hash=item2eb82090cb:g:zGIAAOSwdG9XRCEa](http://www.ebay.com.au/itm/NEW-1-6-HP-AIR-BRUSH-COMPRESSOR-SPRAY-GUN-1-8HOSE-TANK-/200657637579?hash=item2eb82090cb:g:zGIAAOSwdG9XRCEa)



edit: couldn't resist at that price, so I've ordered one. Expected delivery Mon-Weds next week.


---
**Ariel Yahni (UniKpty)** *June 27, 2016 22:15*

I don't think it works I have similar one. It's spits the same flow as a blower and no constant psi. I think air pump for fish tank it's best. I'm still researching


---
**Ariel Yahni (UniKpty)** *June 27, 2016 22:17*

EcoPlus 728457 5 to 80W Single Outlet Commercial Air Pump, 1300 GPH [https://www.amazon.com/dp/B002JLGJVM/ref=cm_sw_r_cp_apa_RxACxbZFTYD8K](https://www.amazon.com/dp/B002JLGJVM/ref=cm_sw_r_cp_apa_RxACxbZFTYD8K)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 22:21*

Maybe I should have waited a bit before hitting BUY then. Oh well, it should still come in handy for airbrush or the likes. We'll see how it goes when I get it.


---
**Kirk Yarina** *June 27, 2016 22:51*

I have the 35W 1030GPH model.  While it worked great as a chip blower on my small CNC router it's also pretty anemic as an air assist.   They're rather different, the aquarium pump moves a large volume at low pressure, while the airbrush compressor moves a smaller volume at higher pressure.   


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 22:58*

**+Kirk Yarina** Thanks Kirk.



I'm trying to find something that will be suitable for my air-assist head that I designed to force a larger volume through the air-assist head (it's similar in design to how the dyson bladeless fans work). Currently I have 2 air pumps attached to it pumping air in, but the flow is super weak. Maybe I just need larger airpump/s.


---
**Ned Hill** *June 27, 2016 23:14*

**+Yuusuf Sallahuddin** With the attached air tank it should have a relatively smooth flow.  I have an airbrush pump (23 lpm) plumbed into a 5 gal portable air tank and it has smooth flow with plenty of capacity.  Get a small ball valve  to regulate the flow a bit and provide a bit of back pressure.


---
**Scott Marshall** *June 27, 2016 23:48*

Saite has a decent looking air pump that is being sold for the purpose.

It looks like a quality unit to me.

Used with a Plug-in air assist controller/houmeter driver kit, it would be a nice setup.



Info on the former forthcoming.





Here's info on the pumps. They aren't cheap, and may be found elsewhere for a better price. Try to find a carbon vane pump, not a diaphragm pump. The vane pumps run forever and don't need oil or diaphragm replacement. pump. The vane pumps run forever and don't need oil or replacement diaphragms. (I'm not sure of these ones, hard to tell if they are diaphragm or vane.)

I'll look around, or maybe a user that found a good working vane pump will weigh in.



[http://www.ebay.com/itm/Hailea-Electromagnetic-Air-Compressor-Pump-Aquarium-Fish-Laser-Engraver-60W-/261754845663](http://www.ebay.com/itm/Hailea-Electromagnetic-Air-Compressor-Pump-Aquarium-Fish-Laser-Engraver-60W-/261754845663)

.

[http://www.ebay.com/itm/HQ-Hailea-Air-Compressor-Pump-Aquarium-Fish-Laser-Engraving-Cutting-80W-80L-Min-/252371628440](http://www.ebay.com/itm/HQ-Hailea-Air-Compressor-Pump-Aquarium-Fish-Laser-Engraving-Cutting-80W-80L-Min-/252371628440)



Here's an example of a vane pump, they are frequently used as vacuum pumps, and work well as either a vacuum or low pressure air air pumps.



One system that would work well if you want to put in the work is to get a small tank type compressor designed for air nailers, often a "pancake" style, and put it in a location well away from your work area so you don't have to listen to it,. run some 16mm PE tubing to rout laser, then regulate it down and use a solenoid valve to turn o/off.

I'm using this system, but have a 7.5hp 100gal commercial compressor in my shop. The High pressure is handy for using a blow gun etc for other work.

As many projects as you make, a little 2- or 3hp unit with a 10 gal tank could power small air tools like 1/8" grinders, and paint guns. It may be worth considering.



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 02:21*

**+Ned Hill** **+Scott Marshall** Thanks for weighing in & for your explanations.



I was considering a proper air-compressor but was concerned of the noise factor at night time (since I tend to do a lot of my laser work after midnight).



The thought of a proper 2hp/3hp air compressor for using other air powered tools was one of the main factors of my consideration for one. As I've never really used air-compressors I was unsure of whether they retain the compressed air for extended periods of time (e.g. over night). I thought that if they did, I could compress a full tank (during day-time) & have that available for my cutting during the night, noise-free.



Alternatively, I should just move out to the bush where nearest neighbour can't hear me using power tools at 3am haha.


---
**Ariel Yahni (UniKpty)** *June 28, 2016 02:29*

Compressor will provide psi for a period of time. Unless it's big enough to be continuous. Once it's open you loose pressure and get only flow. Power tool like airbrush, nail gun and similar just use a small burts, a quick open and close thus let's the compressor get pressure again. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 02:36*

**+Ariel Yahni** Yeah, that makes sense. I'm considering using some kind of system that only opens the flow when the laser is actually firing. So it would provide short bursts of air, like an air-brush. But, as usual with me, everything is just a "test" to see what happens if I do this. No idea if it will work haha.


---
**Ariel Yahni (UniKpty)** *June 28, 2016 02:44*

Could work if we can program a small burst every few seconds since openning it for regular cut patch would drain it. This can be easily be done I believe via smoothie and a valve that can be controlled 


---
**Ned Hill** *June 28, 2016 03:03*

**+Ariel Yahni** That's why you need to throttle the flow some to have some back pressure.  As long as you are not flowing wide open the system will have some pressure. For instance, if I build up 20 psi in my system I can put out 15 lpm and maintain that pressure. :)


---
**Ariel Yahni (UniKpty)** *June 28, 2016 03:05*

**+Ned Hill**​ What size of tank do you have? 


---
**Ned Hill** *June 28, 2016 03:07*

I have a 5 gal tank.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 03:08*

**+Ariel Yahni** I think Scott has been working on something to add in with his ACR board that controls your airflow, as I did see some "air-assist" pins on his board diagram.


---
**Ned Hill** *June 28, 2016 03:12*

That would be nice to have an automatic on/off for the air assist.  Lol I keep forgetting to turn my on before starting a job.


---
**Ariel Yahni (UniKpty)** *June 28, 2016 03:15*

5 Gal is a lot more than 3L from this airbrush compressor isn't it? 


---
**Ariel Yahni (UniKpty)** *June 28, 2016 03:16*

**+Ned Hill**​ if you change the board you can out a relay for on/off the compressor 


---
**Ned Hill** *June 28, 2016 03:29*

**+Ariel Yahni** Yeah 5 gal is about 19L. Changing the board is in my upgrade plans at some point.  I'll let Yuusuf figure it all out first ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 03:33*

**+Ned Hill** Currently my airpumps that I am using have some annoying feature where they turn off after a while. I imagine it is some overheat protection, but it happens about 1/2 hour into running. Fortunately as I have 2 hooked up running my air-assist, I usually hear the first one stop & have the other one still going for about 30 seconds before it will stop too, so I have time to switch the first one back on again & wait to switch the 2nd back on too.



Auto on/off would be great to be able to have it only blowing air when it's actually necessary (say +/- 1second on either side of the actual firing).


---
**Roberto Fernandez** *June 28, 2016 05:10*

If you can use an IP valve, you have all the control of the air pressure automatically. Only need a 10V or 4-20 mA signal.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 05:42*

**+Roberto Fernandez** I just tried to google what an IP valve is but no luck. What exactly is an IP valve?


---
**Roberto Fernandez** *June 28, 2016 07:11*

This valve transform an electrical signal 0-10V or 4-20 mA in a neumatic signal. Other name is IP Converter or proporcional valve. IP is intensity pressure.

Look for this model on google:

SMC PVQ 31-5G-40-01F

There are other manufactures like FESTO, SAMSON, etc...


---
**Roberto Fernandez** *June 28, 2016 07:15*

I think that it is posible to use a MOSFET output to control this valve, and if meassure the air pressure with a pressure sensor an read it with a termistor imput, voala, you have the perfect automáticamente system to control the air pressure autoregulated.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 07:44*

Thanks **+Roberto Fernandez**. All the electronics side of things are a bit past my understanding, but with a bit of research & maybe help from people here, I think I can get something going eventually.


---
**Phillip Conroy** *June 28, 2016 07:58*

I tryed a aquarium air pump for all of 5minutes then switched to my shop 2.5hp 50 liter tank air compressor with water traps (2) with built in air pressure regulator .so far havnt looked back ,was a bit nosiy untill i moved it 30 meters away into a brick garage,now can not even hear it running. So far got 100 hours up on it and only paid $120 for it new years ago.i change oil ever 50 hours with dedicated air compressor oil ,and have fitted a auto tank drain that drains the tank setable .5 to 5 seconds drain and does this every 1 to 45 minutes that the air compressor has power to ig.noe i get a very clean cut on bottom of my cut 3m mdf with no marks at all


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 08:04*

**+Phillip Conroy** Unfortunately I don't have the option to put it 30m away in the garage, as my laser is in the garage. I like the sound of very clean cuts. I will see how I go with the small compressor (it'll be a definite improvement over the aquarium pumps) & probably end up upgrading to a full sized compressor similar to what you're using. I saw a 2hp compressor at a local hardware store for about $120. Just a matter of finding somewhere to stash it away from my work area (maybe behind some of the garage walls to block some of the noise).


---
**Roberto Fernandez** *June 28, 2016 09:11*

For the noise, is posible to reduce it putting the compresor in a wood box with some aislant inside, like rockwool. And very importan, a thick carpeta or cork below of the compressor. It will reduce the noise alot


---
**Ned Hill** *June 28, 2016 12:30*

**+Roberto Fernandez** Would need to be careful doing that as it may cause your pump to over heat.  Small aquarium pumps might be fine, but bigger pumps heat up a lot.  However, vibration damping material under the pump is always a good idea :).


---
**Roberto Fernandez** *June 28, 2016 19:56*

**+Ned Hill** yes, ofcourse. It is a good idea to make some holes to the box in order to maintain free air circulation . The noise insulating boxes that sell for compressors are not fully closed also.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 29, 2016 22:26*

The one I ordered arrived today :) Unfortunately it has a broken dial on the regulator, so can't change the pressure without replacing the dial. It states in the item description also that it is a 1/8" BSP, which I think can't be correct as that is 3mm & the outlet looks to be more like 10mm (or 3/8"). I've contacted the seller to see about getting a replacement dial or a partial refund.



The noise of this pump is quite low. It says 48db max. Pretty happy with the noise. 23L/minute seems like a very low flow of air, but that is just running with nothing attached, so might be better once funnelled through a smaller hose (I will be using 4mm ID once I work out how to attach to the BSP fitting).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 30, 2016 04:14*

After plugging it in for a test, it's definitely no good for air-assist. I get <10 seconds of air at 10psi before the motor kicks in again to repressurise the tank. That would end up with the motor permanently on. I wonder how you go about air-brushing with it. I'd imagine that you want air for longer than 10 seconds.


---
**Ned Hill** *June 30, 2016 04:23*

The tank is mainly acting as a pulsation damper.  A airbrush pump will run continuously for an air assist as it doesn't move enough air to pressurize a tank and feed the air assist.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 30, 2016 05:02*

**+Ned Hill** Is it going to damage the pump to run it continuously? I did notice that the air pressure coming out of my air-assist was greater than my 2 pathetic aquarium pumps I am using. So the 23L/min that this does is better than whatever I currently have haha. Time to buy bigger air pumps like what others are using I think.


---
**Roberto Fernandez** *June 30, 2016 14:13*

Maybe a test to know how much your air tank need to be.


{% include youtubePlayer.html id="mx7_03ryFVQ" %}
[https://youtu.be/mx7_03ryFVQ](https://youtu.be/mx7_03ryFVQ)

Be carefull, it can be dangerous.


---
**Ned Hill** *June 30, 2016 14:36*

**+Yuusuf Sallahuddin** It could very well damage the pump, but but from what I've read there are a lot of people using them for this purpose without too many problems.  I guess it depends on the quality of the pump.  I have a small fan blowing on mine to try and keep it from getting too hot.  

A small pancake type air compressor is probably ideal, but it will be considerably louder than that the airbrush pump. 

If my airbrush pump every dies I will probably  give in and go through the hassle of plumbing from my big outside workshop air compressor into my inside laser work area.  


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/VC9iCwzrKt3) &mdash; content and formatting may not be reliable*
