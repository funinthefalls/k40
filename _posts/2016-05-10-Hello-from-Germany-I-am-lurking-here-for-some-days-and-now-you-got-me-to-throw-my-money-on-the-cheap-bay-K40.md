---
layout: post
title: "Hello from Germany :) I am lurking here for some days and now you got me to throw my money on the cheap-bay K40..."
date: May 10, 2016 14:54
category: "Discussion"
author: "Sebastian C"
---
Hello from Germany :)



I am lurking here for some days and now you got me to throw my money on the cheap-bay K40...



While the Machine is on its way (hopefully) I am a bit concerned about the topic of smoke and fume. If its discussed here, its more or less 'blow it outside' but what are your neighbors saying? If I would vent my laser smoke outside - even if its just a bit of burned wood - this would be a recognizable nuisance.  



I like to start a small discussion about this topic:



Is it, that its just too complicated/expensive for the use of a 400$ china-burner, or don' t you even mind? What would you do, if you couldn't vent outside?



I know some DIY air filters (best example: [http://www.instructables.com/id/Build-a-laser-cutter-fume-extractor/](http://www.instructables.com/id/Build-a-laser-cutter-fume-extractor/)) but are there alternatives without using replacable filters (=more costs)?



Thanks for your input  





**"Sebastian C"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 10, 2016 15:10*

Interesting thought. I never considered what my neighbours are saying about me blowing the smoke/fumes outside. However, once the fumes are outside they dissipate quite easily. A filtration setup might be a good idea if you are cutting horrible stinky/toxic materials.


---
**I Laser** *May 11, 2016 03:14*

Have you thought about checking a hydroponics store for carbon filters?


---
**Sebastian C** *May 11, 2016 12:24*

thx **+I Laser**, I think I am well informed about the commercial possibilities.

Of course the grow shops will present you the whole range of venting and removing suspicious smells, but there you don't have the large amount of particles as in the laser smoke/fume. Carbon filters will be clogged in hours, so you need pre filters or even a cascade of pre filters --> expensive, complicated.



My question is more about the discussion of DIY alternatives. I am quite sure I won't get any problems venting wood, PMMA and acrylic smoke outside. 



The whole topic is interesting to me and I am sure some people would benefit.



In the mean time I did some research of alternatives, but the only possibilities I found are all types of wet scrubbers which are largely used in the industry but even more complicated than filters -_-




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 22:57*

**+Sebastian C** I saw something on Instructables a while back where someone made a shower that recycles the water & they filter the water through sand/activated charcoal/etc. Not 100% sure, but if we had enough air pressure from the extraction, could we maybe filter through something like sand? Also I've seen, again on Instructables, some air purifiers that people have made that pass the air past some plants. I wonder is there a biological solution for filtration where we could extract the fumes etc past some kind of plants or even through water.


---
**I Laser** *May 11, 2016 23:32*

Learn something new everyday. :) 



I've never used a charcoal filter, just thought it might be suitable and the same question was posed in the comments section of your link.


---
**Sebastian C** *May 11, 2016 23:54*

+Yuusuf Sallahuddin (Y.S. Creations) , I think the filtration through sand wouldn't work, because of the high pressure loss. You would need a really strong blower and a huge area. And even bubbling it into water needs some strong pumps.

The plants would be a nice touch: we feed the nice trees the vaporized remains of their brethren :D. 

Or maybe algea are capable of filtering the fumes. 



The whole thing is about surface area: if the residence time is high enough (slow flow or long absorber), the separation efficiency will get high enough to have the venting even inside.

But now you have to find the best mix of turbulent airflow,  available space, complexity, and COSTS (and more) :( 

the problems of the empty pocket DIYers :D



The principle of wet scrubbers should be scalable. When my K40 is ready to burn, I have some ideas worth trying the water spray thingy.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 00:04*

**+Sebastian C** Wasn't quite sure what a wet scrubber was, but quick video (
{% include youtubePlayer.html id="BRNPgPBEzfM" %}
[https://www.youtube.com/watch?v=BRNPgPBEzfM&ab_channel=AniketKulkarni](https://www.youtube.com/watch?v=BRNPgPBEzfM&ab_channel=AniketKulkarni)) for anyone else who isn't sure. It seems like a possible solution. I wonder though with the slurry residue that is filtered out, what do we do with it then? I can't imagine it is a nice substance to handle (MDF, acrylic, etc) & would need proper disposal as it's more than likely toxic.


---
**Scott Stubbe** *May 24, 2016 16:49*

The amount of smoke and fumes a laser produces are a lot in terms of filtration, but not a lot in terms of outdoor ventilation. My shop is rural so ventilation is not a big issue, but when I was first considering setting up in a residential neighborhood I devised a vent pipe system from rain gutter downspouts that would release the fumes above the roof for maximum dispersion by the wind. 



It may be good to do some filtration of outdoor exhaust anyway to avoid annoying your neighbors, but I would never trust indoor venting through any "reasonably priced" filtration system. Smoke particles can be removed relatively easy, but too many laserable substances release fumes, which are a whole different beast. Even so, the smoke particles produced by a laser are more fine than you would see from a fire, and keeping those out of the air you are breathing is especially important for those of us with asthma or other respiratory issues. Even with my excellent ventilation system I still use an industrial grade N95 filter mask. 



Even if you only intend to use wood in your laser you still can't totally avoid fumes. Will your project turn out better if you apply a clear coat first? Fumes. Want to do a reverse etching of painted wood? Fumes. Plywood has glue - potential for fumes. Most of this isn't a concern out in the open but if you are venting into a closed workshop...  I guess my thought is if you chose to use a cheap laser because of price, you will not be willing to spend what you need to to get a filtration system that is safe to vent indoors. 


---
*Imported from [Google+](https://plus.google.com/108800242160866633862/posts/iNdMLPVybzd) &mdash; content and formatting may not be reliable*
