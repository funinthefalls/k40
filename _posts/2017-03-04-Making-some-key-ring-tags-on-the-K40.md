---
layout: post
title: "Making some key ring tags on the K40!"
date: March 04, 2017 06:51
category: "Object produced with laser"
author: "Paul de Groot"
---
Making some key ring tags on the K40!



![images/2444b95bf0d0c78546ee2c7067716b63.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2444b95bf0d0c78546ee2c7067716b63.jpeg)
![images/390228357db1d80a6f7ecfc2b9a9d83f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/390228357db1d80a6f7ecfc2b9a9d83f.jpeg)
![images/333b43a55e7b4387802ff15b8b106a59.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/333b43a55e7b4387802ff15b8b106a59.jpeg)
![images/29cf83cf9a2c2ccaba3f7dd393f505ab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/29cf83cf9a2c2ccaba3f7dd393f505ab.jpeg)

**"Paul de Groot"**

---
---
**Don Kleinschnitz Jr.** *March 04, 2017 16:47*

looks like that new laser is working great.


---
**Bernd Peck** *March 09, 2017 19:00*

How did you get rid of the awfull odor after cutting the leather?


---
**Paul de Groot** *March 09, 2017 19:49*

I used glenn20 an odour remover spray can 😊


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/RtxyVQZwtjn) &mdash; content and formatting may not be reliable*
