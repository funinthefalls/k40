---
layout: post
title: "I recently started using corel 8 and now my engraving and cutting do not line up, the cut is to the right by 1/2 inch?"
date: April 14, 2017 21:20
category: "Discussion"
author: "Michael Otte"
---
I recently started using corel 8 and now my engraving and cutting do not line up, the cut is to the right by 1/2 inch?  I am using a pixel on the top left corner of each layer, but no luck?  anyone run into this before, also what should my settings be in CorelDRAW 8 settings in corellaser (engraving data, engraving area, cutting data, cutting area, etc)





**"Michael Otte"**

---
---
**Ned Hill** *April 14, 2017 21:35*

Haven't seen an alignment issue personally, but these settings have worked for me with CD X8.

![images/9b7cb30fc7c6155a50f7ed6121cba8de.png](https://gitlab.com/funinthefalls/k40/raw/master/images/9b7cb30fc7c6155a50f7ed6121cba8de.png)


---
**Ned Hill** *April 14, 2017 21:42*

Sometimes need to use EMF with cutting open curves because the software sometime tries to close the curves when it generates the Gcode.


---
**Michael Otte** *April 14, 2017 22:42*

Thanks Ned, I think that may have worked.  Can you show me your machine properties from the cut/engrave window?  My x y are 400 each, I wonder if that needs to be 600 x 400?


---
**Ned Hill** *April 14, 2017 22:48*

With a standard K40 you are limited to about 300 x 228.  Note the Origin x and Y are 1mm here but they can be 0mm. It was a mistake on my part and I never corrected it because I didn't want to have to redo my origin jigs.

![images/66f93bc5e229b6d7c178e3e902c35e2e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/66f93bc5e229b6d7c178e3e902c35e2e.png)


---
**Michael Otte** *April 16, 2017 01:14*

Thanks so much, it was the bmp and wmf.  somehow mine were other file types and it was not lining up.  Changing it to bmp and wmf corrected everything




---
*Imported from [Google+](https://plus.google.com/115689970456066406491/posts/HdQNZFWD9GE) &mdash; content and formatting may not be reliable*
