---
layout: post
title: "How are you cooling your tube water?"
date: August 17, 2016 12:51
category: "Hardware and Laser settings"
author: "Ariel Yahni (UniKpty)"
---
How are you cooling your tube water?





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *August 17, 2016 12:53*

FB group poll for reference [https://goo.gl/EB5HYi](https://goo.gl/EB5HYi)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 17, 2016 13:33*

Mine is a 35L clear plastic tub. It's in my garage with the laser, so never sees any sunlight & it's quite cool down there. Also, I rarely use the laser during the day time.


---
**Ray Kholodovsky (Cohesion3D)** *August 17, 2016 13:34*

Ice in the bucket if the water is warm. 


---
**Ariel Yahni (UniKpty)** *August 17, 2016 13:39*

Are you guys reading the temp? 


---
**Ray Kholodovsky (Cohesion3D)** *August 17, 2016 14:05*

Nope. 


---
**Eric Flynn** *August 17, 2016 14:44*

I just built a chiller out of an old dehumidifier.  It will not chill the water directly though. It will be a "2 stage" system. I will have a closed system with a radiator that gets chilled by the chiller water.


---
**Stephane Buisson** *August 17, 2016 17:00*

Just cutting few parts each time, water don't have time to heat up much (missing answer)  ;-))


---
**Ariel Yahni (UniKpty)** *August 17, 2016 17:15*

For future reference what would be the recommended water temp? 


---
**Eric Flynn** *August 17, 2016 21:14*

A CO2 laser is best kept around 15c, or just above the dew point to prevent condensation.  I have seen temps get to 30C doing some "medium" time work.  Thats too hot for  decent life.  Not only that, as your tube heats up, the power decreases throughout a cut/engrave.  Its best to keep it at a constant temp , which is the main point of my cooling system.


---
**Ariel Yahni (UniKpty)** *August 17, 2016 21:17*

**+Eric Flynn**​​ great explanation for future users﻿


---
**Gunnar Stefansson** *August 18, 2016 08:51*

**+Ariel Yahni** When I bought my laser the seller recommended a water temperature between 15 to 30 degrees celsius, I have temp probe on mine also with an ambient temp probe, and I keep it at room temperature which is typically at around the 18 to 23.



Other note, I'm surprised to see how many people just use the Gallon Bucket :D 


---
**Phillip Conroy** *August 18, 2016 12:07*

I finallaly got around to testing laser beam power with my power meter at diffrent temps at 15 deg water temp and power meter after 2nd mirror the meter read 38watts,after cutting for an hour tsted again this time water temo was22deg and power was32wstts ,sk water temps make a huge diffrence amost a watt diffrence per deg water temp rise,power settings of 18ma.have to wait for warmer weather in victoria australia to doube check if it was temp change or running laser for 1hour that caused power drop.i could add hot water to tank and try when water temp is 22 deg


---
**Ariel Yahni (UniKpty)** *August 18, 2016 12:18*

Very good data **+Phillip Conroy** . Could you test at a more conservative power level for example 10mA?


---
**Gunnar Stefansson** *August 18, 2016 13:05*

**+Phillip Conroy** Wooow crazy... Now that is an eye opener... Please do follow us up on that cause, then I'll have to make my water colder for sure. Looking forward to hearing about it as I don't got a power meter...


---
**Phillip Conroy** *August 18, 2016 13:11*

Will redo tests at 10ma power and post results here


---
**Eric Flynn** *August 18, 2016 13:20*

A watt or more per/C rise is normal, and gets worse as temp increases.  I am surprised it wasnt worse actually.  You must have gotten a decent tube, as I did.   Hence why I said it is important to maintain temp.  Higher temps also affect beam quality, not just power , but this is harder to test.   Imagine starting a fairly lengthy cut/engrave that starts off fine, then part way through your job, it drops 10W in power, and ruins your work.  On some materials you may not notice , but some you will, and would make for a bad day if it was a paid project.


---
**Victor Hurtado** *August 18, 2016 13:37*

Cual es el mejor método para enfriar el tubo.


---
**Phillip Conroy** *August 18, 2016 19:07*

My tube is a replacement as well as the power supply ,tube has 300 hours work done on it,i run an undersink water chiller that i got for $20 second hand.The chiller is not full flow so i just run 2 pumps,chiller will take water temp down to 10 deg if i let it[must get around to programing and wiring temp theromstats to cut out chiller at 14deg and back on at 16 deg.my tank is only 10 liters and at tube height,thought about bigger tank however did not like tank on floor as water drains out of tube if below laser cutter height,

Also have flow switch that cuts laser enable line if flow stops,thinking it would be a good idea to have same thing for air assist air flow


---
**Phillip Conroy** *August 19, 2016 10:51*

Just be aware that the cw3000 chiller is jyst a water tank and radator/fan it is not refridgerated so can not go below ambant temps like the proper refridgerated chillers can,just look at the power usage of what you are buying ,if less than 100 watts than just water and radator cooling


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Q4GQbDD4Di5) &mdash; content and formatting may not be reliable*
