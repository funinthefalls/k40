---
layout: post
title: "I am using this blower: - it works great but is really loud"
date: July 05, 2016 21:17
category: "Modification"
author: "Tev Kaber"
---
I am using this blower: [https://www.amazon.com/gp/product/B000O8D0IC/](https://www.amazon.com/gp/product/B000O8D0IC/) - it works great but is really loud. Is there such a thing as a quiet ventilation fan?





**"Tev Kaber"**

---
---
**Evan Fosmark** *July 05, 2016 21:33*

I use one of these guys: [https://amzn.com/B00E4WKNAM](https://amzn.com/B00E4WKNAM) It's pretty quiet, and adjustable.


---
**Ariel Yahni (UniKpty)** *July 05, 2016 22:46*

Is it that load? Can you post a video? 


---
**Scott Marshall** *July 05, 2016 23:12*

I use a Greenhouse fan (the kind they sell for folks growing pot) from one of the greenhouse supplies on ebay.



I went with the 6" model, and put it on the far end of my duct (well away from my laser) I use a speed control and tone it down for the laser, as it could suck in a workpiece (literally) on full power. Makes a great welding fume/sawdust evacuator too. Look to Grizzly.com for a full assortment of exhaust duct, fitting & tool connector. I'm using a 55gal drum as a "trap" so can use it as a shop vac too. Works great for  pulling coolant mist off my milling machine. In the end, the $100 I spent on the blower and probably another $100 on plumbing was a good investment.

The safest way to do exhaust is to put the air mover on the discharge, that way the duct is under low pressure and leaks will not push noxious fumes into your living spaces. It also suppresses fires (which is a good reason to use aluminum ductwork, not plastic.



If you're going with a dedicated laser exhaust one of the 4" blowers would do the job well on a speed control. Get the better one, not the ones that are basically a fan in a tube. The better ones use a blower  wheel.



Those marine blowers are OK in a pinch but not a long term solution if you're going to run the laser much.





[http://www.ebay.com/itm/iPower-4-6-8-10-12-inch-INLINE-DUCT-FAN-blower-HIGH-CFM-cool-vent-exhaust-/160875914720?var=&hash=item2574f3a5e0](http://www.ebay.com/itm/iPower-4-6-8-10-12-inch-INLINE-DUCT-FAN-blower-HIGH-CFM-cool-vent-exhaust-/160875914720?var=&hash=item2574f3a5e0)


---
**Ariel Yahni (UniKpty)** *July 05, 2016 23:17*

You will always find exelent answers and experience from **+Scott Marshall**​.  Admire your dedication


---
**3D Laser** *July 06, 2016 00:47*

I use a vortex s series fan.  It's and 8inch fan and the s actually stands for silent it sounds no louder than a house fan I have it mounted under my daughters crib in the basement and you can't hear it and that thing sucks some air 


---
**Pippins McGee** *July 06, 2016 04:07*

I too bought 270CFM fan off ebay. very efficient at drawing smoke out but yes very loud.

can I buy a speed controller from ebay? if so what would I buy?

I am OK to solder it in but don't know what to buy,

cheers


---
**Scott Marshall** *July 06, 2016 06:05*

Need to know your electrical Specs. The type of motor is also important, some motor types won't tolerate s speed control. If you can put up a link to the blower I'll spec one for you.


---
**Pippins McGee** *July 07, 2016 00:14*

**+Scott Marshall** oh ok not as easy as I thought.

yes thanks it is this one

[http://www.ebay.com.au/itm/4-Hose-BOAT-BILGE-AIR-INLINE-BLOWER-UV-Stabilised-270CFM-12-Volt-FREE-POST-/301169428271?hash=item461f18c72f:g:QhoAAMXQLw1R30eT](http://www.ebay.com.au/itm/4-Hose-BOAT-BILGE-AIR-INLINE-BLOWER-UV-Stabilised-270CFM-12-Volt-FREE-POST-/301169428271?hash=item461f18c72f:g:QhoAAMXQLw1R30eT)



it is 12volt

I bought one of these to power it

[http://www.ebay.com.au/itm/281043734565?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/281043734565?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Scott Marshall** *July 07, 2016 00:36*

This one ought to do you well. It may seem overkill at 15 amp  rating for a 5 amp blower. but these Chinese devices are rated at the power they will handle for 5 seconds or less, if you want it to run at a safe temperature, you need to figure on about double the actual current you expect to use it for.



I searched ebay to find you the best deal, and it's only $4 US. I'm not sure but I'd expect they ship to Australia. 



I use these for speed controls on 90vdc motors on small machine tools and have decent luck, I also use them for the dimmer on my K40 lighting kits. (this is a better price than I usually pay)



Order quick to get the price, they show only a few left.



Good luck, Scott







[http://www.ebay.com/itm/DC-6V-90V-15A-DC-Motor-Speed-Control-PWM-Switch-Controller-1000W-62-59-28mm-New/291767824403?_trksid=p2047675.c100009.m1982&_trkparms=aid%3D777000%26algo%3DABA.MBE%26ao%3D1%26asc%3D35389%26meid%3Dbdb65fa610de446887ca82b2738db4b3%26pid%3D100009%26rk%3D1%26rkt%3D1%26mehot%3Dag%26sd%3D151761330814](http://www.ebay.com/itm/DC-6V-90V-15A-DC-Motor-Speed-Control-PWM-Switch-Controller-1000W-62-59-28mm-New/291767824403?_trksid=p2047675.c100009.m1982&_trkparms=aid%3D777000%26algo%3DABA.MBE%26ao%3D1%26asc%3D35389%26meid%3Dbdb65fa610de446887ca82b2738db4b3%26pid%3D100009%26rk%3D1%26rkt%3D1%26mehot%3Dag%26sd%3D151761330814)


---
**Pippins McGee** *July 07, 2016 05:55*

**+Scott Marshall** thanks scott, cheap as chips. ordered!


---
**Pippins McGee** *July 22, 2016 11:42*

**+Scott Marshall** took a few weeks to get here but hey, cheap.

installed

perfect

so much better having control over the fan speed 

thanks for this suggestion scott.


---
**Scott Marshall** *July 22, 2016 14:06*

**+Pippins McGee** Glad it worked out well!


---
**crispin soFat!** *August 09, 2016 17:11*

"Elicent" brand in-line fans are quiet and reliable.


---
**josh mozug** *August 08, 2017 02:23*

i picked up this fan but wasn't sure of how to go about providing power to it. can you help?




---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/PpCoE6svRsv) &mdash; content and formatting may not be reliable*
