---
layout: post
title: "How fast can you cut out shapes in regular printer paper with the K40 using air and the 38.1?"
date: October 02, 2018 21:47
category: "Discussion"
author: "Jamie Richards"
---
How fast can you cut out shapes in regular printer paper with the K40 using air and the 38.1?  Another extreme "Jamie" test. lol  200mm/s.  C3Dm.


**Video content missing for image https://lh3.googleusercontent.com/-ZduyjR-xFew/W7PnYYZ_CNI/AAAAAAAEHBE/DCRUMUg_Vg4qnIdc5xrQ9P_0ibu-nJw6gCJoC/s0/VID_20181002_134704.mp4**
![images/375facd200f5d66150ef82701f958018.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/375facd200f5d66150ef82701f958018.jpeg)



**"Jamie Richards"**

---
---
**Fook INGSOC** *October 02, 2018 22:22*

Damn Cool!😎👍🏻


---
**Don Kleinschnitz Jr.** *October 03, 2018 03:15*

You win the speed prize!


---
**Aleš Tomeček (Alandran)** *October 03, 2018 08:14*

Impressive. No problem with skipped steps? On higher speeds I always get noticable stairs-like pattern.


---
**Jamie Richards** *October 03, 2018 16:19*

No missed steps, but I'm using Ray's Cohesion3D Mini with a secondary 24v power supply.  His board is amazing in speed performance when using Grbl.


---
**salvatore sasakingsoft** *October 03, 2018 20:06*

ciao mi sai dire che cinghia monta la laser k40 ?


---
**Jamie Richards** *October 03, 2018 22:48*

Are you talking about the belt that moves the head/lens on the X axis?  MXL.




---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/TVDCuLV8Xk2) &mdash; content and formatting may not be reliable*
