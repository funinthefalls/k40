---
layout: post
title: "Hi! I have a problem with my machine k40 , amid a process of cutting out the laser stopped ."
date: May 30, 2015 00:01
category: "Discussion"
author: "Daniela Chac\u00f3n Bonett"
---
Hi! I have a problem with my machine k40 , amid a process of cutting out the laser stopped . Even when you press the test button it does not work the laser .

appreciate your help please!





**"Daniela Chac\u00f3n Bonett"**

---
---
**Jim Root** *May 30, 2015 03:54*

How is your water temp? Perhaps lens is dirty. Did the whole thing stop or just the laser beam? Double check the model number and serial number in the software. 


---
**Daniela Chacón Bonett** *May 30, 2015 14:31*

Hi! Only the laser beam is stopped. The model of my machine is k40 and the water temperature is low.

thanks


---
*Imported from [Google+](https://plus.google.com/103059135729476369901/posts/7yKJbc32ope) &mdash; content and formatting may not be reliable*
