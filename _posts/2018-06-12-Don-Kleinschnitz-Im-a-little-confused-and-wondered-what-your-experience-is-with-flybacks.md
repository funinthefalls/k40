---
layout: post
title: "Don Kleinschnitz I'm a little confused and wondered what your experience is with flybacks"
date: June 12, 2018 10:09
category: "Discussion"
author: "Andy Shilling"
---
**+Don Kleinschnitz** I'm a little confused and wondered what your experience is with flybacks.  My 6 month old Lps packed up a couple of weeks ago and I've been in conversation with the supplier, they are sending me a new one.

 The symptoms with mine is it will not fire from the test button on the machine nor the grbl shield I now have, but with the 6 pin plug pulled out will fire from the test button on the lps. Do you think the flybacks has gone or could it be the internal connections that are at fault?





**"Andy Shilling"**

---
---
**HalfNormal** *June 12, 2018 12:22*

That is a definite sign of a short somewhere. I would look at the simplest start which would be your limit switches. They use the same 5 volts that is used to enable the power supply.


---
**Andy Shilling** *June 12, 2018 12:39*

I thought about a short but didn't know where to start, I take it you mean the 5v from the lps. My limits are fed from the R4 Arduino Paul supplies with the gerbil shield which in turn is powered from a seperate 24v PSU.


---
**HalfNormal** *June 12, 2018 12:45*

I would start then from disconnecting each power supply separately, and see what happens.


---
**Don Kleinschnitz Jr.** *June 12, 2018 12:52*

I would focus on why it will not fire from the test button on the panel. 

If the test button down on the supply fires the laser the LPS would seem to be good.



You said: <i>but with the 6 pin plug pulled out will fire from the test button on the lps.</i> 

<b>Does it fire from the test button on the supply when the 6 pin plug is plugged in?</b> 

......something on the 6 pin plug is holding the control signals from going to grnd. 



Check your supply using this guide and let us know what you find: 



[drive.google.com - Troubleshooting A K40 Laser Power Subsystem 1.0.pdf](https://drive.google.com/open?id=1yAGfqw3VVKW91qEpCVFOuAwYoyI7PPuh)


---
**Andy Shilling** *June 12, 2018 12:56*

**+HalfNormal** basically the only way I've managed to get it to fire is with the H L P IN G 5V plug pulled out. The problem can't be the gerbil because I've tried both new boards that I have here.



Due to this I'm guessing it's got to be an internal problem with the lps.


---
**Don Kleinschnitz Jr.** *June 12, 2018 13:02*

**+Andy Shilling** all things are possible with a K40. But I would be surprised if it is internal to the LPS.

Most times there is a problem with the wiring or the panel connected to it.

Did you try the troubleshooting guide and did that find anything wrong?


---
**Andy Shilling** *June 12, 2018 13:06*

**+Don Kleinschnitz** yes Don if the 6 pin plug is connected it will not fire, even the LEDs on the lps will not light up when powered on; remove the plug and it will fire from the test button on the lps.  



What I'm thinking is the same as you, I'm going to remove all connections from the 6 pin other than the arm and test button to see if that makes a difference, if so I'm sure that will denote an earth problem but the question is why 2 weeks after fitting the gerbil shield.


---
**Andy Shilling** *June 12, 2018 13:14*

**+Don Kleinschnitz** sorry I'm running a answer behind you lol, the only trouble shooting I've done so far is what the seller requested, I'm like you in the fact I don't believe anything is broken until you know it can't be fixed.



I will do some testing later when I'm home and let you know what I find.


---
**Don Kleinschnitz Jr.** *June 12, 2018 13:28*

**+Andy Shilling** if the LED on the LPS is not lit (depending on your supply type) that suggests the 5V is not working or perhaps it is shorted to gnd by the connector.



What is connected to the 5V on the LPS?




---
**Andy Shilling** *June 12, 2018 13:39*

Off the top of my head I don't think I'm running anything from it, I was using it to power my laser pointer but I've not fitted that since preparing it for a 60w tube and renewing my extraction system.


---
**Don Kleinschnitz Jr.** *June 12, 2018 15:15*

**+Andy Shilling** picture of LPS with the LED's showing pls.


---
**Don Kleinschnitz Jr.** *June 12, 2018 15:17*

**+Andy Shilling** then one of the 5V connections on the 6 pin is shorted?

If your remove the 6 pin connector does the LED come back on?


---
**Andy Shilling** *June 12, 2018 18:27*

Forget everything lol I decided to have a quick look inside before possibly charging up the flyback and I could see a bad solder joint around the 5v connection. It looks like as the plug was pushing in to the socket it was enough to create a gap. I have just resoldered it and it seems I am back up and running.( I'm presuming this was the problem).



Thanks for your help Don I think I'll be more investigative next time something goes wrong then I won't have to bother you quite so much.


---
**Don Kleinschnitz Jr.** *June 12, 2018 18:48*

**+Andy Shilling** never a bother glad you are running again!


---
**Andy Shilling** *June 12, 2018 18:54*

**+Don Kleinschnitz** cheers pal, bonus of the story is I'm getting a new 60watt lps free of charge. Can't knock that one.


---
**HalfNormal** *June 12, 2018 20:14*

What am I? Chopped liver? 😁


---
**Don Kleinschnitz Jr.** *June 12, 2018 20:25*

**+Andy Shilling** 

**+HalfNormal** needs some kudos...



👍👍👍👍

He said first: <i>That is a definite sign of a short somewhere.</i>

👍👍👍👍

👊👊👊👊


---
**Andy Shilling** *June 12, 2018 20:31*

**+HalfNormal** I am so very very sorry kind sir, I got so excited by the thought of being able to cut something again I totally forgot you. Please accept my apologies. 


---
**HalfNormal** *June 12, 2018 21:55*

**+Andy Shilling** no worries. Just wanted to make you squirm! I'm just glad that you are able to get your system up and running again.

**+Don Kleinschnitz** you forgot I was the first one to say it was probably something to do with the 5 volts too.


---
**Don Kleinschnitz Jr.** *June 13, 2018 13:08*

**+HalfNormal** 

You said: <i>That is a definite sign of a short somewhere. I would look at the simplest start which would be your limit switches. They use the same 5 volts that is used to enable the power supply.</i>



Not exactly but we will give you the win! 

🏆🏆🏆



Besides no-one should be chopped liver! :)



I said: <i>if the LED on the LPS is not lit (depending on your supply type) that suggests the 5V is not working or perhaps it is shorted to gnd by the connector.</i>


---
**HalfNormal** *June 13, 2018 13:20*

**+Don Kleinschnitz** Thanks! I know better than to split hairs with you. You would think I would learn!


---
**Don Kleinschnitz Jr.** *June 13, 2018 13:39*

**+HalfNormal** .... that's no fun! I could stand on my head again?




---
**HalfNormal** *June 13, 2018 13:42*

**+Don Kleinschnitz**  Do not worry, I rarely know when to stop. Please, no more gymnastics! I do not you to get hurt! 


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/Cp8xnED5ZBn) &mdash; content and formatting may not be reliable*
