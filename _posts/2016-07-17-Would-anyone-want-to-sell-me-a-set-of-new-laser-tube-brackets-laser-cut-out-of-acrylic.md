---
layout: post
title: "Would anyone want to sell me a set of new laser tube brackets laser cut out of acrylic?"
date: July 17, 2016 10:05
category: "Object produced with laser"
author: "Eric Rihm"
---
Would anyone want to sell me a set of new laser tube brackets laser cut out of acrylic? My machine isn't running at the moment and it's because I need to focus the mirrors and align the tube and the new brackets would help square up everything. Can send paypal with shipping.





**"Eric Rihm"**

---
---
**Eric Rihm** *July 18, 2016 07:50*

$15 plus shipping if anyone wants a quick little job or whatever you're charging


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/gUN1xTQCGyd) &mdash; content and formatting may not be reliable*
