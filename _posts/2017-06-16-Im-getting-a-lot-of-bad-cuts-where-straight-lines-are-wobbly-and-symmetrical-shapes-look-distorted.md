---
layout: post
title: "I'm getting a lot of bad cuts where straight lines are wobbly and symmetrical shapes look distorted"
date: June 16, 2017 17:03
category: "Discussion"
author: "Adam J"
---
I'm getting a lot of bad cuts where straight lines are wobbly and symmetrical shapes look distorted. I've noticed my Y-Axis is occasionally getting stiff, could this be the cause? I've tried to take a video and wondering what I should do next to alleviate the problem? As you can see, I've taken off the side panel to take a look at the belt, but not sure what I should attempt. Please ignore the awkward air assist routing, this doesn't cause an issue and will be re-routed better eventually. When the power is off, the axis move freely (shown at end of video). I'm using a smoothie with Laserweb and jogging by 100mm in the video. Thanks in advance, this is a great community and I'd be stuck without the valued advice given here.





**"Adam J"**

---
---
**Adam J** *June 16, 2017 17:34*

Just noticed the Y-Axis belt on right (Controller compartment side), seems more slack than the one on the left. Where can I tighten this belt to see if that's the cause?


---
**Joe Alexander** *June 16, 2017 17:57*

there is a tensioner on the very back right of the gantry, identical to the one on the right end of the gantry for the x-axis, you should be able to tighten the 2 screws to tighten the belt tension.


---
**Adam J** *June 16, 2017 17:58*

Quite a lot of gunk on right hand rail. Cleaned it off as suggested to me in another post now I've finally realised where it collects up. Will see if this helps. I guess the video still serves as a reference to anybody with the same problem in the future...


---
**Adam J** *June 16, 2017 18:29*

This is still happening after cleaning, will try tightening the belt as guided, thanks Joe.


---
**Jim Fong** *June 16, 2017 19:10*

At what speed are you cutting at.  Moving the entire Y gantry takes more reliable torque than just the x axis laser head.  When I first recieved my k40, the gantry was out of square and both side belts were not tensioned equally.  This lead the gantry to bind a bit.  Adjusting made it smoother, however my y axis cannot go as fast as the x.  I think it's reliable to about 250mm/sec.  The x can go faster, around 300mm/sec. 



I normally don't go that fast. I get better looking raster engravings if I keep it under 150mm/sec.   I like a darker looking engraving. 






---
**Claudio Prezzi** *June 17, 2017 09:57*

Also, it seems that the accelleration is quite hight. And probably the air hose is pulling on the gantry too.


---
**Don Kleinschnitz Jr.** *June 17, 2017 13:43*

Highjacking this post to test hash tags searching.

#K40yaxis




---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/MdAws1mBYj2) &mdash; content and formatting may not be reliable*
