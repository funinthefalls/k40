---
layout: post
title: "A peek under the hood :)"
date: May 06, 2016 14:51
category: "Hardware and Laser settings"
author: "Alex Krause"
---
A peek under the hood :)



![images/444d837964296023246fa50d265d52a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/444d837964296023246fa50d265d52a2.jpeg)
![images/5375c65ffbab1336149a1f19ab894b61.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5375c65ffbab1336149a1f19ab894b61.jpeg)

**"Alex Krause"**

---
---
**ThantiK** *May 06, 2016 15:33*

You got an old one? 


---
**Ray Kholodovsky (Cohesion3D)** *May 06, 2016 15:41*

Are those motor connectors 2.54mm pitch?  Looks bigger. Will they plug into a smoothie board motor header as is?


---
**Ariel Yahni (UniKpty)** *May 06, 2016 15:50*

Don't tell me that. I got mine from the same supplier. It seems they are not using the ribbon cable


---
**Alex Krause** *May 06, 2016 15:50*

**+ThantiK**​ there want much in the description about the controller but from what I have seen there are 3/different boards when I Google it that they could either have a moshi board or one of 2 different nanos


---
**Alex Krause** *May 06, 2016 15:53*

**+Ray Kholodovsky**​ without popping off the glue on the connectors it's hard to tell but I believe the unpopulated connectors are 2.54mm it's a tight fit to get the calipers in there


---
**ThantiK** *May 06, 2016 15:54*

**+Alex Krause**​, did you look for "NewlyDraw" when you ordered? It looks like my old board, but I thought mine said it was an M4 nano, but I could be wrong. 


---
**Alex Krause** *May 06, 2016 16:10*

:) wonder what I should do with this smoothie board I have laying around ;)


---
**Ariel Yahni (UniKpty)** *May 06, 2016 16:12*

Do it do it do it


---
**Alex Krause** *May 06, 2016 16:13*

**+ThantiK**​ when I ordered I asked the community which seller they had the best customer service with on eBay and after looking at customer reviews and and the other sellers feedback percentages I went with the seller that had a 99.5 percent positive rating with thousands of items sold


---
**James Rivera** *May 06, 2016 17:23*

**+Alex Krause** Link to the item or seller you used?


---
**Alex Krause** *May 06, 2016 17:27*

**+James Rivera**​ [http://pages.ebay.com/link/?nav=item.view&id=201264099304&alt=web](http://pages.ebay.com/link/?nav=item.view&id=201264099304&alt=web) 


---
**James Rivera** *May 06, 2016 17:33*

Whoa. That's cheap. Does it work out of the box, or is updating the electronics (or other parts?) necessary for it to work?


---
**ThantiK** *May 06, 2016 18:07*

**+James Rivera**, mine worked out of the box but the software was <i>TERRIBLE</i>.  I mean, garbage garbage.  It's not absolutely necessary, but it's certainly a quality of life improvement.  I'm actually thinking of wiring up mech endstops to mine as well.


---
**Tony Sobczak** *May 07, 2016 02:06*

Globalfreeshipping is the best.


---
**Tony Sobczak** *May 07, 2016 02:09*

It's exactly the same as mine. No problems. Get a newer version of coreldraw until the smoothie is working.


---
**James Rivera** *May 07, 2016 17:08*

**+Tony Sobczak** why CorelDraw? IIRC that is not free/OSS. Do you have a cheap source? Also, would Inkscape suffice?


---
**ThantiK** *May 07, 2016 17:46*

**+James Rivera** CorelDraw is what the laser comes with.  Some pirated, Windows XP version of the software for the laser.


---
**James Rivera** *May 07, 2016 19:16*

**+ThantiK** Ok, thx. I'm trying to find ways to get away from Windows and move to Linux, so a CorelDraw program for Windows is kind of a step in the wrong direction. And it doesn't sound like Laserweb will work without changing the board...or will it?


---
**ThantiK** *May 07, 2016 20:12*

Nope. The protocol for the stock board is proprietary. If you have an extra ramps or other 3d printer board laying around, generally that will suffice for a controller. 


---
**James Rivera** *May 07, 2016 21:07*

**+ThantiK** Hmmm...I have another RAMPS (and a spare Printrboard).


---
**Tony Sobczak** *May 07, 2016 21:41*

Mine came only with corel laser, draw was not included.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/KcJ6Qje4a9m) &mdash; content and formatting may not be reliable*
