---
layout: post
title: "I am looking at upgrading my laser to a 50w or even 60w"
date: October 28, 2016 16:24
category: "Discussion"
author: "3D Laser"
---
I am looking at upgrading my laser to a 50w or even 60w. I am looking at the models on eBay for around 1200 is there any other option that maybe better for around the same price.  I am hesitant of aliexpress as I have never used it before but would love to hear your thoughts 





**"3D Laser"**

---
---
**Jim Hatch** *October 28, 2016 17:18*

Just a suggestion - if you have a choice between a 50W with a larger bed and a 60W with a smaller bed for the same price, go for the larger bed even though it's lower power. Sooner or later you'll need a new tube and you can upgrade to more power when you do (might require a small extension box but nothing difficult). It's really hard to add bed size 🙂



The difference in power won't make much difference in what you can cut and how fast but being able to do larger pieces makes a lot of things easier. Until you hit the 100W range you don't get the power to do single pass cuts in stuff much thicker than 1/4 or 3/8" material so power is nice but size is better.


---
**Anthony Bolgar** *October 28, 2016 18:22*

I like your logic **+Jim Hatch** 


---
**Scott Marshall** *October 28, 2016 18:36*

If you search reviews of Aliexpress, they're pretty bad.

It's hard to find a truly good opinion of them.

 I've ordered small stuff on occasion with no problem, but if I ordered a $1000+ laser cutter, I'd sure not sleep well until it arrived. 



Ebay with Paypal is probably the safest bet. Ebay backs the buyer over the seller, so I'm told by sellers. I don't sell much there, but buy a lot and they have saved me from being cheated several times.



 


---
**3D Laser** *October 28, 2016 19:07*

I am thinking about going with a 50w 300x500 I would love to find a 500x700 for a good price 


---
**Chris Boggs** *October 29, 2016 20:41*

I have 2 50w 300x500 with powered bed..  I purchased from seller "sunshinesmileservices"  on eBay..  Shipping from California warehouse..  Delivery via FedEx to KY  4-5 days..  Very good quality.. Very happy with my purchase..  I'd be glad to answer questions I can.. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/fPyRGdxRwY9) &mdash; content and formatting may not be reliable*
