---
layout: post
title: "Hey guys im kinda new to this but so excuse me if this is a obvious or dumb question but i am wanting to make a rotary on the stock k40 board i know i have seen it done on here and read i would beed same size motor as my y"
date: September 09, 2016 09:48
category: "Discussion"
author: "Jaime Menchaca"
---
Hey guys im kinda new to this but so excuse me if this is a obvious  or dumb question but i am wanting to make a rotary  on the stock k40 board i know i have seen it done on here and read i would beed same size motor as my y axis stepper , i have looked but cant find a deffinate answer on the specs of that . Is there anyone that knows or knows  the spec? Or where to look on my machine to find out which one my machine has ?





**"Jaime Menchaca"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 10:49*

You'll probably have to check the exact one in your machine. The Y-axis stepper is located at the front right of the cutting bay, underneath a metal cover (black in mine). Just undo the two bolts holding that cover in place & look beneath it & you should be able to get the part number from the particular stepper motor your machine has.


---
**Jaime Menchaca** *September 18, 2016 23:51*

Ok i looked and found this number  42-2100c08s/sh tried google but came up empty any one kniw the specs

![images/d9bbeb576b06afbd381f5f90b5190159.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d9bbeb576b06afbd381f5f90b5190159.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 19, 2016 00:38*

My steppers show model like this, on the drive shaft area. Note: both my steppers have different models.

![images/a1c955373bce3ade4c3b41f5c32a838b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a1c955373bce3ade4c3b41f5c32a838b.jpeg)


---
**Jaime Menchaca** *September 19, 2016 04:42*

Hmmm looked at both and that was the only number could i could find on either of the motors :/


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 19, 2016 05:03*

I also googled your model number but couldn't come up with anything either. Hopefully someone else here has the same or knows the specs for it.


---
*Imported from [Google+](https://plus.google.com/107959038053955154431/posts/Q8zbtTmqkER) &mdash; content and formatting may not be reliable*
