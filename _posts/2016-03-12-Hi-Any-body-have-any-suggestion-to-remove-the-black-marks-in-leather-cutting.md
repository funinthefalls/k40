---
layout: post
title: "Hi , Any body have any suggestion to remove the black marks in leather cutting ?"
date: March 12, 2016 14:25
category: "Discussion"
author: "Raja Rajan"
---
Hi ,



Any body have any suggestion to remove the black marks in leather cutting ? 

![images/b85c2f5d1ac9d9956f0f8c016528b185.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b85c2f5d1ac9d9956f0f8c016528b185.jpeg)



**"Raja Rajan"**

---
---
**Rick Sollie** *March 12, 2016 14:31*

I've seen people using blue painters tape to prevent marks, and it works to a degree.  On the epilog laser cutter I've used you can set the frequency to prevent burn marks on wood.  Is there a function like that on the k40?




---
**Raja Rajan** *March 12, 2016 14:33*

Thank Rick,Ya even i have seen those but the thing is i m planning to make leather goods commercial ,So each time I am unable to make blue tape all the time :(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 12, 2016 14:42*

Can't be avoided really. The laser is burning the leather. Only thing you can really do is incorporate it into your design as a feature. And, because it is on the edges, you usually seal the edges of leather somehow (I use beeswax, others use tragacanth gum, some use Fiebings EdgeCoat, etc). There's really nothing that can be done to prevent it. I've tried a million & one different things on my machine to prevent it. Nothing worked. Only thing I can suggest, is install air-assist to minimise the charring on the edges. One thing that I was considering trying was to wet the leather first to see if it burned the edges (or could even cut it).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 12, 2016 14:45*

Oh, one other point. If your lens is not correctly installed or the focus is not correct, then you will get more charring. I found that my lens in the K40 came upside down to begin with. Made it very difficult to cut through leather (even 1mm leather). Once fixed, I could cut through leather a lot easier & the resulting char on the edges was not quite as bad. Note, the thinner the leather you are cutting, the easier it is to cut, and the quicker you can cut it, which reduces the char on edges a large amount.


---
**Alex Krause** *March 12, 2016 15:26*

If you are wanting a product free of burning and looking for commercial use a CNC with a drag knife installed will leave a burn free edge with nice corners see attached video 
{% include youtubePlayer.html id="bMoRUZnvbXw" %}
[https://youtu.be/bMoRUZnvbXw](https://youtu.be/bMoRUZnvbXw)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 12, 2016 17:24*

**+Alex Krause** That's really good. Thanks for sharing that.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 13, 2016 03:12*

**+Nathan Walkner** I'd imagine it cannot cut something submerged in water due to the fact it doesn't really cut, but burn. Also, I think the beam might be dispersed strangely in the water, rendering it powerless. Still, might be worth a try.


---
*Imported from [Google+](https://plus.google.com/110717916181924497071/posts/76shZyTzeiF) &mdash; content and formatting may not be reliable*
