---
layout: post
title: "Just got my 3d printed laser tube adjustable support brackets, after looking for over 12 months for something like these i almost wish my tube would blow so i can start using them.Not going to stuff around with current tube"
date: June 09, 2017 05:52
category: "Discussion"
author: "Phillip Conroy"
---
Just got my 3d printed laser tube adjustable support brackets, after looking for over 12 months for something like these  i almost wish my tube would blow so i can start using them.Not going to stuff around with current tube as it is aligned good and i do not stuff with things that are working ok unless it is preventive maintance.....

![images/a0e4a0507dc558a191d6cb5a76f5bc2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0e4a0507dc558a191d6cb5a76f5bc2b.jpeg)



**"Phillip Conroy"**

---
---
**Kelly Burns** *June 09, 2017 13:47*

They look great.  I recently added these.  I believe it's the same design.  I ended up having to rework the design to get it work in my K40 with my Tube.



Mine aren't as adjustable as I would like, but a huge improvement over shims.   Good Luck.


---
**Bromide Ligand** *June 09, 2017 17:07*

where did you find these..




---
**Kelly Burns** *June 09, 2017 18:22*

Here is the one I printed and then later modified and reprinted.   [http://www.thingiverse.com/thing:420853](http://www.thingiverse.com/thing:420853))






---
**Phillip Conroy** *June 09, 2017 19:49*

For sg 350 laser [https://www.thingiverse.com/thing:1263096](https://www.thingiverse.com/thing:1263096)


---
**Bromide Ligand** *June 11, 2017 11:58*

ah kool. I printed mine today.


---
**Phillip Conroy** *June 12, 2017 00:52*

When you fit them can you lost a review on them thanks


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/UJHP5zhCuMe) &mdash; content and formatting may not be reliable*
