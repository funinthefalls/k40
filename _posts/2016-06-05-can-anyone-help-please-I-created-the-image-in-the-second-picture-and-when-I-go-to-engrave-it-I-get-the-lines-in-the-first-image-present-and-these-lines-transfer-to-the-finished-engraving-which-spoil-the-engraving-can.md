---
layout: post
title: "can anyone help please....... I created the image in the second picture and when I go to engrave it I get the lines in the first image present and these lines transfer to the finished engraving which spoil the engraving, can"
date: June 05, 2016 18:37
category: "Discussion"
author: "Norman Kirby"
---
can anyone help please....... I created the image in the second picture and when I go to engrave it I get the lines in the first image present and these lines transfer to the finished engraving which spoil the engraving, can anyone help me please



![images/089440c8622d12f20b56f8e135755907.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/089440c8622d12f20b56f8e135755907.jpeg)
![images/a6d205d950c013f2a481f98a90ff89e8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a6d205d950c013f2a481f98a90ff89e8.jpeg)
![images/4393bb4e83595585b3826084bee27ef8](https://gitlab.com/funinthefalls/k40/raw/master/images/4393bb4e83595585b3826084bee27ef8)

**"Norman Kirby"**

---
---
**Christoph E.** *June 05, 2016 19:07*

What Format do you use for engraving? And have you got the first one in higher resolution/vector?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 20:57*

By the looks of it that is related to unclosed paths. What software are you using?


---
**Norman Kirby** *June 05, 2016 22:00*

sorry i should have said I am using Corel Draw x7 with corellaser, the resolutions are exactly the same just one picture is bigger than the other due to screen shot, as for format I am using vector




---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 22:38*

**+Norman Kirby** I'm not certain the process for closing paths in Corel Draw, but that's what I would definitely look into. I've seen this happen before with imports from AI/SVG into Corel Draw/Laser & it's almost 100% of the time been related to the paths not being joined/closed.


---
**Norman Kirby** *June 06, 2016 14:15*

sorted it. it happens when I use the "group attribute"


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 06, 2016 18:39*

**+Norman Kirby** There is another option called "Combine" I think (I am using x5). You could try that to see if that actually joins the paths together to allow for easy manipulation. It will be problematic to not be able to group things.


---
**Norman Kirby** *June 07, 2016 16:40*

scratch the "group" attribute, its the "join" attribute, will have to play around with this till I get it right


---
*Imported from [Google+](https://plus.google.com/103798744920995011573/posts/B6i6WaYi8wK) &mdash; content and formatting may not be reliable*
