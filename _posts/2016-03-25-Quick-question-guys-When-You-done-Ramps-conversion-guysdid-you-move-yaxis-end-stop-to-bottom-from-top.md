---
layout: post
title: "Quick question guys When You done Ramps conversion guys,did you move yaxis end stop to bottom from top?"
date: March 25, 2016 08:21
category: "Modification"
author: "Damian Trejtowicz"
---
Quick question guys

When You done Ramps conversion guys,did you move yaxis end stop to bottom from top?





**"Damian Trejtowicz"**

---
---
**Jean-Baptiste Passant** *March 25, 2016 09:37*

No, you do not move the endstop when doing the conversion.



Check here for what you need to do to get it right : [https://github.com/openhardwarecoza/LaserWeb/issues/103](https://github.com/openhardwarecoza/LaserWeb/issues/103)



EDIT : Just checked, you ar ethe one who posted the answer lol



You can add more endstop if you want, two endstop for each axis is probably a good idea, not sure though.


---
**Damian Trejtowicz** *March 26, 2016 09:25*

I moved endstop to  bottom left,genuine arduino in machine and laserweb still not working :(.

Prontafece work fine with gcode from inkscape.

Laserweb give msg x endstop and y endstop hit msg when i try do anythig :(

After week still no progress i dont have any more ideas


---
**Damian Trejtowicz** *March 26, 2016 09:43*

Buffer is fine now,when i use machine in prontaface there is no delay and stops in printing

About laserweb i mean ,it show me server ok,machine connected but no control ober it.when i try reference nothing happen,just said command send from console.when i try cut it give me series msgs about hitting endstops


---
**Damian Trejtowicz** *March 26, 2016 11:44*

Just notice one thing now,so its definitly software problem

When i put G28 thru console before i do anything machine reference and i have control over.if i will try use reference widget , nothing happen


---
**Damian Trejtowicz** *March 26, 2016 11:47*

Yes i do(i hope ,its from his oficiall github)


---
**Damian Trejtowicz** *March 26, 2016 13:51*

I found it problem.

First one was display,it tooks to many resources,second one was me i thing because i was pressing zero machine g92 ,but i should press house icon :(


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/2wwCqkvzL86) &mdash; content and formatting may not be reliable*
