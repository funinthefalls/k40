---
layout: post
title: "HalfNormal Stephane Buisson Here's the results of that file that I shared earlier ( )"
date: June 13, 2016 13:27
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
**+HalfNormal** **+Stephane Buisson** 

Here's the results of that file that I shared earlier ([https://drive.google.com/file/d/0Bzi2h1k_udXwRVRlc0NVeFZDc1k/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwRVRlc0NVeFZDc1k/view?usp=sharing)).



2 rows of stitches on each edge.



I'm not too happy with this as this leather that I am using is ridiculous. I've had nothing but trouble laser cutting this particular leather. 



10 passes @ 4mA @ 20mm/s & still not through.

+3 passes @ 10mA @ 20mm/s & still not through.

+10 passes @ 4mA @ 20mm/s & still not through.

+ 10 more passes @ 4mA @ 20mm/s & 99.9% of the way through & I got sick of waiting so I cut it the rest of the way manually with a blade.



Normally I don't have this much trouble, but it must be something in the dye/finish of this particular leather, as natural vegetable tan leather of the same thickness cuts like butter.



About 10 minutes stitching, once you know how. It's a 2 needle process, where you have one needle on either side of the leather & zig-zag through (whilst securing at the same time). Provides a nice strong stitch that even if 1 link breaks, the rest will generally hold. Used waxed nylon thread.



Have fun!



![images/6cd9db08e38c60277d7ddb463ddb8c8b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6cd9db08e38c60277d7ddb463ddb8c8b.jpeg)
![images/88ea44f3c0b563350cd1f273d6747d14.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/88ea44f3c0b563350cd1f273d6747d14.jpeg)
![images/89d0d73d00109b0680b87d5ad6a6a9f7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/89d0d73d00109b0680b87d5ad6a6a9f7.jpeg)
![images/126f5a61f54851f0ed72cbe6d623fef8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/126f5a61f54851f0ed72cbe6d623fef8.jpeg)
![images/9e4917fb3ea683b3623b810d5a02b182.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e4917fb3ea683b3623b810d5a02b182.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Stephane Buisson** *June 13, 2016 13:35*

I admire your stiching skills !!!


---
**J R III** *June 13, 2016 14:18*

Could be that the tanning that was used in the leather was chromium I don't know if that could be a problem


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 13, 2016 15:21*

**+J R III** No idea, but I've tried chromium tanned belting straps in the past & had extreme difficulty cutting through them too. So you may be right on that.



**+Stephane Buisson** Thanks Stephane. With a little practice & patience you can have equally as good skills. There's a very simple technique to it. One of the best guys to watch is Nigel Armitage on youtube. Here is a clip of the stitching technique: 
{% include youtubePlayer.html id="7ue3zBg0bdA" %}
[https://www.youtube.com/watch?v=7ue3zBg0bdA](https://www.youtube.com/watch?v=7ue3zBg0bdA)


---
**HalfNormal** *June 13, 2016 15:35*

**+Yuusuf Sallahuddin** Looks like a great starter project. Nice work!


---
**Bruce Garoutte** *June 13, 2016 18:23*

Nice! how is that leather to etch on? Personalizing with a name would be pretty cool if it could.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 02:44*

**+Bruce Garoutte** This leather is actually pretty good to etch on. Horrible to cut, but great for etching. If you take a look at my previous posts on my page, somewhere there is a phone case I did with this leather that I etched my logo on.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/2haHcMpDLqw) &mdash; content and formatting may not be reliable*
