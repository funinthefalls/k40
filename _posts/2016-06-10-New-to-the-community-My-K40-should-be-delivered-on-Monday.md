---
layout: post
title: "New to the community. My K40 should be delivered on Monday"
date: June 10, 2016 01:56
category: "Software"
author: "Ben Marshall"
---
New to the community.  My K40 should be delivered on Monday. Trying to browse and prep required software (I've read the pirated software included is buggy). Looking forward to the grind of getting everything up and running. Air assist nozzle and pump should be in tomorrow. 





**"Ben Marshall"**

---
---
**Ariel Yahni (UniKpty)** *June 10, 2016 02:28*

Welcome **+Ben Marshall**​ you are in for a ride, buckle your seat belt.  Fun time is just around the corner 


---
**Pippins McGee** *June 10, 2016 02:29*

exciting times. what software did the seller say will come with yours?

out of curiosity, what air assist nozzle and pump did you buy?


---
**Ben Marshall** *June 10, 2016 02:30*

**+Ariel Yahni** I'm pumped. Thank you for the warm welcome. 


---
**Ben Marshall** *June 10, 2016 02:34*

**+Pippins McGee** seller did not specify the software; I'll try to reach out tomorrow and find out. I ordered the light object nozzle and an 18W 793 gph commercial pump


---
**Ben Marshall** *June 10, 2016 03:22*

According to the order information, the software included is shenzhouyi/laserDRW 


---
**Alex Krause** *June 10, 2016 03:40*

**+Ben Marshall**​ Cardboard is awesome to setup your work alignment until you get the hang of it


---
**Ben Marshall** *June 10, 2016 03:47*

**+Alex Krause** Thanks, Alex. I've got a bunch saved up already to practice with! 


---
**Alex Krause** *June 10, 2016 03:52*

**+Ben Marshall**​ also going to the big box stores (Lowe's/home depot) or your local lumber yard and see if they have any scraps they will give you or sell at a reduced rate I picked up 12-1foot squares of 1/8 acrylic for 50cents each and some 1/8inch ply for 4 dollars for a 4foot square that had some scuffing that sanded out all I had to pay was the cutting fee to get it in bits that would fit in my laser


---
**Ben Marshall** *June 10, 2016 11:18*

**+Alex Krause** Awesome! I'll swing by Lowes after work and see what they have. Thanks!


---
**Vince Lee** *June 10, 2016 22:45*

If you have a local Tap Plastics, they usually have a scrap bin for odds and ends.  One store in my area sells them individually priced (and somewhat high) at $3-$5 apiece, but another has a flat rate of $1 a sheet and a third sells them at $1.50 a pound.  Most pieces are around 1 sqft and come in different colors.  Most pieces are acrylic, but watch out for PVC or Polycarbonate, which aren't laserable.




---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/14X7oksrGbs) &mdash; content and formatting may not be reliable*
