---
layout: post
title: "I have started a Wikipedia page to house what I hope to be an unofficial set of test files for laser cutting and engraving"
date: June 11, 2016 00:26
category: "Repository and designs"
author: "Stuart Rubin"
---
I have started a Wikipedia page to house what I hope to be an unofficial set of test files for laser cutting and engraving. It's pretty bare right now, but I'm hoping people will add to it until we have a somewhat universally accepted set of canonical test files. [https://en.wikipedia.org/wiki/Laser_Cutting_and_Engraving_Test_Files](https://en.wikipedia.org/wiki/Laser_Cutting_and_Engraving_Test_Files)



Comments are welcome. Thank you!





**"Stuart Rubin"**

---
---
**Frank Fisher** *August 06, 2016 17:56*

That link doesn't work for me, and wiki gets no results searching for similar.


---
**Stuart Rubin** *August 10, 2016 13:15*

I wiki page was taken down, appropriately, as being "unfinished." I thought they would allow work-in-progress pages. I hope to finish it, or even better, get some more people to contribute, soon. Interested?


---
**Frank Fisher** *August 10, 2016 14:08*

He he when/if I get a <b>working</b> cutter.  All disputes and theory at the moment :/


---
*Imported from [Google+](https://plus.google.com/104069580748008438325/posts/c74N2WPhFbd) &mdash; content and formatting may not be reliable*
