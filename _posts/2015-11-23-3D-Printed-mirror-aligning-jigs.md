---
layout: post
title: "3D Printed mirror aligning jigs!"
date: November 23, 2015 01:58
category: "Modification"
author: "DIY3DTECH.com"
---
3D Printed mirror aligning jigs!





**"DIY3DTECH.com"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 23, 2015 09:32*

That's pretty nifty. Would be much easier to slot pieces of paper (with printed targets) into it rather than sticking/unsticking tape all the time.


---
**DIY3DTECH.com** *November 23, 2015 11:55*

**+Yuusuf Sallahuddin** It is!  Also because of the heaver stock, it does not tend to "burst" into flames as it would with sticky notes or something of the like. In addition the reticle target makes it easier to know which screw to turn on the mirror too... 


---
**Fred Padovan** *December 29, 2015 06:25*

They work great...    : )


---
*Imported from [Google+](https://plus.google.com/+DIY3DTECHcom/posts/56wPqcx2Gq8) &mdash; content and formatting may not be reliable*
