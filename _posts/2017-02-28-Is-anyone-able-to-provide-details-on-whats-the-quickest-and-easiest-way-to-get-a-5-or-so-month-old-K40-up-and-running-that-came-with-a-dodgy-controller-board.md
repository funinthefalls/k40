---
layout: post
title: "Is anyone able to provide details on whats the quickest and easiest way to get a 5 or so month old K40 up and running that came with a dodgy controller board?"
date: February 28, 2017 09:11
category: "Smoothieboard Modification"
author: "Andrew Brincat"
---
Is anyone able to provide details on whats the quickest and easiest way to get a 5 or so month old K40 up and running that came with a dodgy controller board? I live in Australia so hoping for something local but really want to get this up and running in the  next week or so if possible 



I have a smoothie board and GLCD but no middleman board (which i believe is needed for newer K40s) 



Would prefer direct swap if possible (dont mind cutting loses/selling smoothie on ebay) but also dont want to lose too much functionality. Would possibly like  to do a rotary upgrade eventually...



Thanks in advance





**"Andrew Brincat"**

---
---
**Andrew Brincat** *February 28, 2017 10:25*

Does this help? also have a 2nd power supply identical to this one 

![images/b7f3b043fbe3f493a59bf3b418c612b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7f3b043fbe3f493a59bf3b418c612b8.jpeg)


---
**Stephane Buisson** *February 28, 2017 13:30*

if you are against the clock, no need of middleman board if you rewire from end stops & motors with old cat 5 cable. nothing complex here, just from components end where the ribbon start, follow continuity. Your Smoothie is fine too (you just need to find a way for smoothie connectors side (most are with screws so easy, but a couple).








---
**Paul de Groot** *February 28, 2017 20:25*

**+Andrew Brincat**​ where are you located in Oz? I'm in Sydney so might be able to help you if you're near me.☺


---
**Andrew Brincat** *February 28, 2017 21:06*

I am based in melbourne thanks though Paul! Will have a look at trying this cat5 network cable thanks Stephane! Hopefully my IT skills come in handy lol


---
**Paul de Groot** *February 28, 2017 21:17*

**+Andrew Brincat**  you could use a fully operating controller based on Grbl from me if that helps? It's just plug over the wires inclusive of the flat ribbon cable, no soldering or adaptions required. How does that sound? Send me a PM and will discuss.


---
**Andrew Brincat** *February 28, 2017 21:36*

**+Paul de Groot** sent you a hangout msg, is that the same? lol cheers


---
**Vince Lee** *February 28, 2017 23:42*

A middleman is just a breakout board for the ribbon cable connector to make it easier to solder to the lines.  You could also just carefully cut apart the individual lines on the end of the cable with scissors to separate them and solder directly to the lines you need.  That would be quicker IMHO than adding new cabling. The ribbon contains four lines for one of the motors and power, ground, and signal lines for the endstops.


---
*Imported from [Google+](https://plus.google.com/+AndrewBrincat/posts/6VzrGghPX5a) &mdash; content and formatting may not be reliable*
