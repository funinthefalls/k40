---
layout: post
title: "my laser cutter is cutting too fast and not slowing down even form settings"
date: June 16, 2017 07:51
category: "Original software and hardware issues"
author: "just a hamster that codes"
---
my laser cutter is cutting too fast and not  slowing down even form settings





**"just a hamster that codes"**

---
---
**Joe Alexander** *June 16, 2017 08:05*

ok, thanks for sharing...



I would recommend including more information such as controller board info, any upgrades done, digital or analog pot, what settings you have changed, etc. One-line questions do nothing but frustrate those trying to help you.



<b>This public service announcement has been brought to you by Common Sense</b>


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 14:03*

Not even a question, more of a statement. 


---
**Don Kleinschnitz Jr.** *June 16, 2017 14:26*

**+Sakari Kauranen** We are all here to help but to do so we need to know more about the machines configuration and the problems symptoms. I also encourage you to search this forum  for similar problems first.


---
**Mark Brown** *June 16, 2017 22:07*

Is this a brand new machine that's been doing this from the start, or was the machine working fine and now acting up?


---
**just a hamster that codes** *June 17, 2017 08:45*

got it working

i had the wrong motherboard selected


---
*Imported from [Google+](https://plus.google.com/113893262651693586412/posts/RJ5FdqR17Uo) &mdash; content and formatting may not be reliable*
