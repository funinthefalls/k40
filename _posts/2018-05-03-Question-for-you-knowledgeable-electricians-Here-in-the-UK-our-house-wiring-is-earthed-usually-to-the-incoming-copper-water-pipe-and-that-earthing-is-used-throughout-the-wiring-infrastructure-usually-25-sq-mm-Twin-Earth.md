---
layout: post
title: "Question for you knowledgeable electricians. Here in the UK our house wiring is earthed, usually to the incoming copper water pipe and that earthing is used throughout the wiring infrastructure, usually 2.5 sq mm Twin & Earth"
date: May 03, 2018 15:38
category: "Discussion"
author: "Duncan Caine"
---
Question for you knowledgeable electricians.  



Here in the UK our house wiring is earthed, usually to the incoming copper water pipe and that earthing is used throughout the wiring infrastructure, usually 2.5 sq mm Twin & Earth cabling for power outlets.  So, do we still need a separate earth for the K40 Laser?





**"Duncan Caine"**

---
---
**Adrian Godwin** *May 03, 2018 15:47*

No, but do make sure the earth wiring inside the box is properly connected to case and incoming power cable. The build quality is a bit variable and many people have reported dangerous errors.




---
**Andy Shilling** *May 03, 2018 16:10*

It would never hurt to attach a wire to the screw terminal on the chassis if you have one and run the wire to an earth spike. It's not necessary by any means but if you feel the need to you can.


---
**Don Kleinschnitz Jr.** *May 03, 2018 17:48*

You want the shortest electrical path (resistance) from your machines safety ground to earth. Most houses do this if wired correctly, if not your entire house is unsafe. 


---
**Paul de Groot** *May 03, 2018 21:14*

Wire loops are the biggest problem in these machines which causes the usb communication with the controllers to fail. Mostly you see strange behaviour like a laser job stopping in the middle of a cut and a frozen controller.  Worst case I have seen was a fried controller and laptop. 


---
**Justin Mitchell** *May 06, 2018 14:18*

Just make sure that the internal earthing wires are actually connected to the power connectors earth pin. use a multimeter or similar tester and make sure theres low/no measurable resistance between the case( and other metal parts) and the earth pin.  your RCD/RCBOs will be able to pick up on any leakage then




---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/47za7m9fqS4) &mdash; content and formatting may not be reliable*
