---
layout: post
title: "Any idea how to import ai or eps file to inkscape?"
date: April 27, 2016 12:19
category: "Software"
author: "Damian Trejtowicz"
---
Any idea how to import ai or eps file to inkscape?

Thanks





**"Damian Trejtowicz"**

---
---
**Jim Hatch** *April 27, 2016 12:37*

File|Open - at least AI files are natively supported. At least with AI CS3 current Inkscape can open. Not sure about earlier versions.


---
**Ulf Stahmer** *April 27, 2016 16:24*

If **+Jim Hatch**'s solution doesn't work for you, I've had luck by saving the file as a pdf and then opening the pdf in Inkscape.  The imported vectors are even editable!


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/StiepLFEBqC) &mdash; content and formatting may not be reliable*
