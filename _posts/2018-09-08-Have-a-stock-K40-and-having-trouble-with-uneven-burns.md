---
layout: post
title: "Have a stock K40 and having trouble with uneven burns"
date: September 08, 2018 04:17
category: "Discussion"
author: "Elias Galvan"
---
Have a stock K40 and having trouble with uneven burns. Want to work on cutting boards but practicing on foams.  The yellow part is way lighter compared to the red parts. Any ideas?

![images/f49afd781da371af96aa6695ecfab1fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f49afd781da371af96aa6695ecfab1fb.jpeg)



**"Elias Galvan"**

---
---
**HalfNormal** *September 08, 2018 04:27*

Make sure your bed is level


---
**LightBurn Software** *September 08, 2018 05:45*

If the scan is starting and stopping at the dark spots, it’s likely slowing down there and not compensating for it properly. If the software you use allows for overscanning, enable it.


---
**Stephane Buisson** *September 08, 2018 07:23*

I don't remember if stock board manage acceleration. look like that could be the problem. setting on foam would be fast & low power.


---
**Elias Galvan** *September 08, 2018 13:38*

The bed is level, i checked.  I'm using LaserDRW but haven't seen anything about overscanning.  The settings i'm using are 400mm/s and 8% power.  but i've done 100mm/s at 6.2% and get a very similar result.  So i'm not using a "material" setting, the speed is manually selected in software.  Laser power is selected on machine.


---
**Kelly Burns** *September 08, 2018 16:32*

It’s difficult to do anything at those low power settings.  A CO2 Laser is just going to be too inconsistent. 



With the power and speed requirements for engraving cutting boards, you won’t have that much sensitivity to acceleration.  



Cutting boards are cheap.  A 3 pack at Home Depot was $8 last year.  They will give you a lot of surface area to test on.  






---
**Joe Alexander** *September 08, 2018 18:22*

I agree with **+Kelly Burns** try it on some scrap wood and see how it comes out. Foam is really sensitive to the heat generated.


---
**Ned Hill** *September 09, 2018 15:56*

Also need to mention that you really need to be careful cutting foam boards on the laser.  Foam can be really flammable and is a major cause for laser machines burning up.


---
*Imported from [Google+](https://plus.google.com/117619667940460353189/posts/gdAgALHuNUb) &mdash; content and formatting may not be reliable*
