---
layout: post
title: "Can someone provide me link with a reputable source for a K40 seems like everyone under the sun sells these things on ebay"
date: March 10, 2016 04:48
category: "Discussion"
author: "Alex Krause"
---
Can someone provide me link with a reputable source for a K40 seems like everyone under the sun sells these things on ebay. But I'm willing to source from other places like Amazon or a reliable E-retailer needs to ship to the USA in a reasonable amount of time as well





**"Alex Krause"**

---
---
**Scott Marshall** *March 10, 2016 05:11*

These folks treated me well, and several others here  have also had good luck with them:

They go by "GlobalFreeShipping" but are also known outside of ebay as "Banyan Imports"



Delivery was within a week or so - they shipped FedEx Ground to me here in Upstate NY and I had the unit in about 8 days. This was during the Christmas season as well, which may have made a difference. For the price (I paid $386 to my door) you sure can't complain on the shipping. Stock and price seem to vary depending on when they have a shipment come in. They sell everything from Commercial restaurant equipment to Milling machines. They are strictly "Crate Shifters" and have no parts or service support structure in the US (common with these operations)



There was some pretty severe damage to my Laser which was no fault of FedEx or Global, it was mis-packed in China. Despite this they settled up with me fairly and quickly. Very professional. I was refunded $250 and repaired the Laser well within that budget. I did have a fair number of hours in the repairs, but that included machining my own replacement parts. The parts are commercially available from other sources.



According to this source, they are part of "Cronus Corp of China" and operate in the US from the port of LA with offices in Lake Forest Ca

[https://www.importgenius.com/importers/banyan-imports](https://www.importgenius.com/importers/banyan-imports)



[http://www.ebay.com/itm/201264099304?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/201264099304?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Tony Sobczak** *March 10, 2016 06:41*

Globalfreeshipping is the best.  Top quality and great customer service. 


---
**Anthony Bolgar** *March 10, 2016 21:28*

I also purchased from GlobalFreeShipping and have had no issues, they seem to actually care about the customer.


---
**Alex Krause** *March 14, 2016 04:52*

So I did a search of eBay for this item from global free shipping....since they are out of stock it redirected me to their other items like import car parts and adult novelty items made of Silicone 


---
**Scott Marshall** *March 14, 2016 20:51*

Now that's product diversity!



I think they get a container of Lasers, deep fryers, and apparently some other goodies about once a month, so I'd expect them to be back in stock soon. I will look and see if I have their contact info so you can find out how long till the next batch.



EDIT:



No luck, I dealt with them thru ebay's message system.

I did see several listing for them using a google search.



These have become a pretty hot commodity in the last few months, we may have run out the supply lines.

Hope you locate one. If I hear of one for sale, I'll drop you a line



Scott


---
**Alex Krause** *March 28, 2016 05:30*

Keep checking on one from globalfreeshipping and still no luck...


---
**Alex Krause** *April 14, 2016 03:15*

Back in Stock !!!!!!!!!


---
**Scott Marshall** *April 14, 2016 19:34*

Cool. Bet they get a flood of orders.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/4nUBci8oS8z) &mdash; content and formatting may not be reliable*
