---
layout: post
title: "Quick easy way to coral all the flash drives I have around the laser"
date: May 10, 2016 18:01
category: "Hardware and Laser settings"
author: "andy creighton"
---
Quick easy way to coral all the flash drives I have around the laser. 

(2 layers of Luan cut and glued together.)

![images/3e284ceb3d9a289081b8cf381e962f51.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3e284ceb3d9a289081b8cf381e962f51.jpeg)



**"andy creighton"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 10, 2016 18:28*

Nice idea.


---
**Gunnar Stefansson** *May 11, 2016 14:53*

Hehehe a nice quick life hack :D I'm copy'ing this :D


---
*Imported from [Google+](https://plus.google.com/111039030319838939279/posts/DeauXNaxiqb) &mdash; content and formatting may not be reliable*
