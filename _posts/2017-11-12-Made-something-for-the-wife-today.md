---
layout: post
title: "Made something for the wife today"
date: November 12, 2017 18:37
category: "Object produced with laser"
author: "Chris Hurley"
---
Made something for the wife today. 

![images/f7a97749b4cb7705dbbc5f64b3d34870.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f7a97749b4cb7705dbbc5f64b3d34870.jpeg)



**"Chris Hurley"**

---
---
**Ned Hill** *November 13, 2017 04:51*

Nice job :)


---
**Buhda “Pete” Punk** *November 13, 2017 17:57*

If I did not know better I would say you are from Newfoundland. 


---
**Chris Hurley** *November 14, 2017 15:27*

Nah b'y. 


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/LHDFKFvyDpr) &mdash; content and formatting may not be reliable*
