---
layout: post
title: "Hi I am new to the K40 Laser cutting machine I have just purchased my machine but I am having trouble with the software I have the gold usb and the disc but for some reason I cant get CorelDraw to instal Does someone have a"
date: April 24, 2016 08:37
category: "Discussion"
author: "Donna Gray"
---
Hi I am new to the K40 Laser cutting machine I have just purchased my machine but I am having trouble with the software I have the gold usb and the disc but for some reason I cant get CorelDraw to instal Does someone have a you tube video of the software setup For some reason I don't think my computer is recognising the usb Please any help would be much appreciated





**"Donna Gray"**

---
---
**Donna Gray** *April 24, 2016 08:38*

Laser DRW is working but corelLaser wont work Help!!!!


---
**I Laser** *April 24, 2016 10:52*

There's usually a movie file 4.mpg (or something like that) on the provided disk. It's in Chinese, but you should be able to deduce the installation instructions using that.



You might need to be a little more specific as to what is happening when trying to use Corellaser...


---
**Jim Hatch** *April 24, 2016 13:37*

My CD came with two versions of the installs. One was called "latest software" or something like that. That worked.



My machine flagged a virus in it. I ended up using Inkscape and Adobe Illustrator until I found my old Corel 12 install DVD (legit copy).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 24, 2016 15:02*

The Gold USB doesn't have anything on it. It is basically just a security key.



What **+I Laser** mentions about the movie file is correct. I used it to figure out how to install stuff (although it doesn't have any English explanations anywhere).



So, for CorelLaser to work, you need to install CorelDraw first. Then install CorelLaser (as it is a plugin to CorelDraw).



I did have some slight issues installing to begin with, so it may be similar for you. What version of Windows are you running?


---
**Scott Marshall** *April 24, 2016 17:25*

I had the exact same problem.



If you try to install Corel by using the .exe file that is visible on the disk, it WILL load,  but NOT work. 

Drove me nuts for weeks.



The Installer program is hidden in a .RAR file which my file viewer could not see (Free Viewer). Once I switched to a file viewer that detected it, I installed it using the installer and all was well.

I ended up using WinRAR (also free, but it actually works!) and all the sudden a whole bunch of files appeared that were not there before.



And it WILL run without another version of Corel installed, I don't have any previous Corel software and it works fine.

 Apparently if you DO have a previous version, it installs the plug-in, but if you do NOT have Corel already, it loads the whole thing. (This may be why you HAVE to use the installer, it's decides which you are?)

You have to use the serial number to activate it, but are instructed NOT to register it, the whole deal is of questionable legitimacy - but it does work, albeit rather rough around the edges. The serial number is the same for all users.





I Laser and Yuusuf Sallahuddin were the folks who helped me through it, but they've helped so many people , they probably forgot that particular problem.



The short answer is - you have to use the installer to load Corel, and the installer is in a hidden .RAR file.



Give it a try, hope it works for you.



Scott



PS

This is a good example of WHY we need a real forum engine here.



There's so much info here, but lost in itself. 



Donna could have found answers to her question instantly, and most others IF we had a standard forum format here.

Currently, our kind users are repeatedly answering the same questions, and they can't do it forever. 



In any other format, you would look through the titles, pick "K40 Software", "Corel Install issues", and you would scroll down to my interaction with I laser and Yuusuf, and there would be the whole story, and other options if that's not the answer..



All the great info here is lost in a unsearchable morass, it's truly a waste of valuable time and resources for many people. 



I'm upgrading to a Smoothieboard and it took me nearly an hour last night to find Stephanies "Blue Box Smoothie info" post. I knew it was there somewhere, but had to read thru 2 years of her posts to find it. (by the way - thanks Stephanie, it's very helpful)



Hope you're listening Google, we could really use an "upgrade" here....


---
**Jim Hatch** *April 24, 2016 18:10*

**+Scott Marshall** What's the link to Stephanie's post? I have her Blue Box instructions (Googled "smoothie board K40" and found that). I'd love to see anything else she's got on it. I've got the smoothie board (and Smoothie's instructions on how to install it in a laser cutter) but am chasing after some cable modifiers. I want to keep the original cable ends in case I get rid of the K40 I can return it to stock and use the smoothie in something else or in the event the Smoothie doesn't work and I need to revert to the stock Nano board.


---
**Scott Marshall** *April 24, 2016 19:12*

**+Jim Hatch** 



Jim, 

Here's what I've got so far...



It's not all accurate, but a good guide - there's been a lot of versions of the K40, and some of the hardware info may not be current. A lot of it references the old Moshidraw board/s (the one that used the ballast resistor) which are obsolete.



 It's not a complicated from a hardware/wiring standpoint. Follow the "LASER" section (of the 3 icons at the page bottom) in the Smoothieboard "Documentation"  and it gives you an easy install guide. 



Some pinouts, and especially the laser control circuit in Stephanies diagram(and others) are wrong (Probably just dated - power supply is different) The PWM out from the smoothie is shown connected to the pot which is a voltage divider (0-5v) and it's shown connected directly to the +5v side of the pot and gnd, which would very likely be fatal to the Smoothie's output device and very probably the k40power supply as well. I need to look into that.



 In the next few days (when I have time), I'm going to put the scope on the K40 and see if there's any PWM going on there, but I doubt it. I DO think there's a PWM input on the power supply though, and it may be as simple as connecting the Smoothie PWM out there. I've got a sine/square generator to feed it a signal and see if it works, if so, we're all set,  If not we can either set the smoothie up to provide 0-5v out, or make a little RC network to convert the PWM out to 0-5v. I'll keep you up to date if you like. I'd guess somebody here that's done the swap on a newer K40 could tell us how they did it.



The rest of the connection is easy, once the flat cable is replaced with normal wire. There's a few options there, I may just "sacrifice" the controller and remove the connectors to make a middleboard adapter. If it's easy enough, I'll just run new wiring. Need to go take a look and scheme it up. If you want to save the cables, There's people selling "middleman boards" which are just adapters. I know I've seen them for sale, but can't remember where. (another easy find if we had a regular forum).



The flat cable is a PIA, don't know why regular ribbon wasn't used. maybe the flat is more tolerant of the flexing. You could adapt to regular ribbon, but it' lighter than I like to run motors thru.

The white flat cable  - It's known as "FFC" cable. As far as I can tell.

[http://sumida-flexcon.com/hp682/PANTA-FFC.htm?ITServ=C4fdad4eX14cbd81aa64XY7d8a&gclid=CK3htKb9p8wCFUEkhgodm7UGTQ](http://sumida-flexcon.com/hp682/PANTA-FFC.htm?ITServ=C4fdad4eX14cbd81aa64XY7d8a&gclid=CK3htKb9p8wCFUEkhgodm7UGTQ)

I think these may be the connectors - need to check. 

[http://www.ebay.com/itm/2-Molex-39-51-3143-14-Position-ZIF-FFP-Flat-Flexible-Cable-Connector-049-/350261327413?hash=item518d33b635:m:m_y4dX-sIfGJX4HN86psFqg](http://www.ebay.com/itm/2-Molex-39-51-3143-14-Position-ZIF-FFP-Flat-Flexible-Cable-Connector-049-/350261327413?hash=item518d33b635:m:m_y4dX-sIfGJX4HN86psFqg)



Hope that helps.



I'll start a thread here as soon as I get going on it. Maybe we can step through it together. Please let me know if you make any ground.



The software implementation is what concerns me, but we have this group for help....



If you follow the link in the Wiki to the original Epilog Zing software development thesis, it's useful (PDF)



Stephanies:

[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)



Wiki:

[http://smoothieware.org/smoothieboard](http://smoothieware.org/smoothieboard)



Purchase:

[http://shop.uberclock.com/collections/smoothie](http://shop.uberclock.com/collections/smoothie)





Scott


---
**Jim Hatch** *April 24, 2016 19:26*

Scott - Thanks for the info. I have the linked stuff already but as you noted it's not specific to some of the newer K40s. I have a Nano M2 board in mine and that's different than the old Moshi style. I've been using Mouser for cables and such but haven't dug through the catalog yet to find the right cables/connectors. If the gender-benders aren't readily available I figured I'd just swap the cables altogether leaving the old ones in reserve.



Peter has been great about helping people with the RAMPS conversion so I'm figuring I'll be on him like white on rice when it's time for software :-) Just Paypal him a 20 every once in awhile so we can keep him in pints down under.


---
**Donna Gray** *April 24, 2016 19:29*

Thanks for the help I ended up downloading a version of CorelDraw and then it all worked Now for the big learning curve trying to work out how to use the software to design an image to cut Any hints would be great

Thanks Again


---
**Stephane Buisson** *April 24, 2016 19:31*

hi guys ( **+Jim Hatch** **+Scott Marshall** )

yes my k40 is an ex 2014  moshi.

I will create new Categories when back in London. thinking about a smoothie one and software install help. for not repeating again and again.

the pot act ceiling to protect the tube.

if i remember. 15ma is about at 3,5V

So pwm at 3.3v via level shifter to 5V reduce by pot to not exceed 3.5V. (don't take the value for granted, just the idea, i am far from my k40 now)


---
**Scott Marshall** *April 24, 2016 19:33*

+1 on Peter, and contributing.

 

Let me know if you find the exact connector or cable assembly to get us from flat to smoothie.That would sure make for a ton less work.

 Molex is no fun to decipher. I have hundreds of connectors still on the shelf to prove it. 





Thanks, Scott


---
**Scott Marshall** *April 24, 2016 19:35*

**+Stephane Buisson**

So you were using the PWM part of the power supply as the actual drive and the pot as a Max limit?


---
**Scott Marshall** *April 24, 2016 19:38*

**+Donna Gray**

Patience. Incredible Patience...

I do most of my drawing in Draftsight and transfer into Corel - because I know how to use Draftsight mostly.



Then, eventually a Smoothieboard (sorry to take over your thread with the Smoothie stuff)



Good luck and have fun, Scott


---
**Stephane Buisson** *April 24, 2016 19:40*

**+Scott Marshall** yep, just copy the same funny psu layout used by Moshiboard.


---
**Stephane Buisson** *April 24, 2016 19:47*

the blu-box link is the same as K40+Smoothie=SmoothK40

posted in community description top right.

in front of your nose.


---
**Jim Hatch** *April 24, 2016 19:49*

**+Stephane Buisson**​ Yep, that's where I first found your Blue Box doc :-)


---
**Scott Marshall** *April 24, 2016 19:53*

Thank, I appreciate it.



Your diagram is great. I hope I can pass on some info on the newer arrangement soon. I'm not sure how many there are, I'd like to think it's just 2, but suspect somewhere in between other versions crept in.



I don't know of any model or revision history on the Blue Box (good name ). It would be worth trying to create one. The trick is accumulating the info in one place. 



Could we use a survey form (I think Google has one here?) for people to "register" their machine with info like PSU Number or description, motors (know you started that already), Software/controllers and other features that have evolved?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 01:04*

**+Donna Gray** As previously mentioned, you can continue to use your SCAL4 software to design your files, then save as SVG then import into Corel. I wouldn't bother using the CorelDraw software to design with unless you happen to have a newer version. Unless you're already familiar with it. I am not, so I just use what I am familiar with (Adobe Illustrator) & then import into CorelDraw/CorelLaser in a format that is supported (many). From there, all you really need to do in CorelDraw/CorelLaser is set the border width of all objects to 0.01 (otherwise it will cut the line twice, once on each side of the line). Then select all you want to cut, click the cut icon (either in the toolbar in the top right or if it disappears like it does for me then the icon down in the Windows System Tray > Right Click > Cut/Engrave).



**+Scott Marshall** I totally agree some better forum would be useful. Someone did set one up in the past (I can't remember who), although I think not many people went across there. I know I didn't because I don't like having to sign up for new accounts at new sites.



What would be useful is a forum based website, where people can login using their Google+ or Facebook (or whatever else) account.



Might be worth looking into if anyone else here is Web Design/Development skilled. I'd be happy to assist with design & setup & coding (although I am not proficient in database/sql management). Then maybe we can each pitch in $1 or something into the hosting costs (unless someone already has spare webspace to host it, I think I recall someone mentioning that at some point).



I might start a post to see if we have any other web-design/dev people here willing to have a go at making something for us all to benefit from.


---
**Stephane Buisson** *April 26, 2016 06:06*

**+Scott Marshall** do you know you can edit (please feel free to add) my bluebox link on Smoothie website. or ask Arthur to create a new one. Keeping with original allow this community to enjoy as linked on top of this page.


---
**Stephane Buisson** *April 26, 2016 06:08*

**+Yuusuf Sallahuddin** yes I will be abroad for another 2 weeks, with very poor connection (countryside). just trying to come daily to check on spam and free genuine post from spambox.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 26, 2016 07:44*

**+Stephane Buisson** That's cool. When you return I'll discuss things with you in regards to creating sticky posts etc. for more of the regular topics (alignment, mirrors, lenses, air assist, etc). Take care & talk more when you're back.


---
**Donna Gray** *April 26, 2016 20:27*

Can anyone tell me settings on my k40 laser cutter to cut 3 & 6mm MDF???


---
**Scott Marshall** *April 26, 2016 20:49*

For 3mm About 12mm/sec and as much power as needed - it will be up there around 18ma or so probably. Slow down to maybe 6mm/sec if needed.

these lasers aren't all the same, and alignment,air assist etc all make a big difference.

The K40 seem to have their own personalities, some tubes have odd shaped beams etc, so you DO have to play with it on some scrap to get a feel for it.

2 faster passes cut nicer than 1 slow one in my experience.





Reduce speed as required for 6mm. 6mm is tough for the k40 and may require multiple passes and re-setting the focal point after the 1st pass.



Keep it under 18ma on the meter or so for tube life (I run mine to 21ma, and it seems OK, but I may pay in the long run)



Oh, If  you don't have an air assist, watch for flare ups.



Have fun, Scott


---
**Donna Gray** *April 26, 2016 21:43*

Do I have to have air assist to cut MDF as I only have the basic k40 it is only new so it is just the standard laser cutter Will it catch fire without air assist


---
**Scott Marshall** *April 26, 2016 23:25*

You can get by without it. But I'd say the an air assist setup is probably the biggest bang for your buck on upgrades.



 I've heard of people using hair dryers in a pinch. For now, that may be a good option, just keep it back from the works and be careful. You will have to run the machine with the lid open. Unless you're comfortable with that, don't do it.



As I've mentioned before, the k40 is really a machine for experimenters who don't mind tinkering and are used to working around machines without safety devices. You can pinch, laser or electrocute yourself if you don't take precautions.



You'll find the lens gets dirty quickly without some airflow past it. If it gets too dirty, it will burn on the 'smoke' and destroy the lens pretty quickly. The air assist does a few things positive. It protects the lens by recessing behind the nozzle, keeps smoke and fumes from depositing on the lens, and if you get the rectangular one from Saite cutter or Lightobject, gives you a larger lens and (the big one) adjustable focus without moving your workpiece. (within 1/2" or so range.)



 There is some work involved, and you need a compressed air source, but they can be acquired pretty cheap.



 Beyond that the fan that comes with the k40 is pretty anemic.



One freebie upgrade you can do is to simply remove the rectangular "tube"  that the fan draws thru. Removal of the restriction increases the fans effectiveness quite a lot. 



It's held in place with 4 screws, just unscrew and it's removeable. I don't recall if you need to remove the framework to get it out or  not. It's been a long time since I removed mine.



Bottom line answer, keep something handy to blow out flames if they flare up and don't go back out, (blow dryer probably great) don't let them flare or you'll damage the lens/head. The flames will be minor, but CAN propagate and burn your house down (worst case)





The air assist head, requires minor metal work (enlarge hole) to install, and an 18mm lens.

[http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050?hash=item3abe742d92](http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050?hash=item3abe742d92)



The lens to fit (USE 18mm dia, 50.8mm focal length, and the basic one works fine)

[http://www.ebay.com/itm/HQ-ZnSe-GaAs-Focus-Focal-Lens-Si-Mo-K9-Reflective-Mirrors-CO2-Laser-Engraver-/261634544454](http://www.ebay.com/itm/HQ-ZnSe-GaAs-Focus-Focal-Lens-Si-Mo-K9-Reflective-Mirrors-CO2-Laser-Engraver-/261634544454)


---
**Jim Hatch** *April 26, 2016 23:50*

**+Scott Marshall**​ yes you need to remove the frame (4 nuts & bolts and 2 wires) to remove the exhaust manifold. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 01:34*

**+Scott Marshall** As Donna is wanting to cut MDF, I wouldn't suggest removing the exhaust tubing, because MDF fumes are not really something you want to be breathing.



**+Donna Gray** I am working through the process of a 3D printed air assist head (that goes around the lens) currently. I have just had one printed & picked it up yesterday, unfortunately I broke part of it (due to a flaw in my design) & cannot test it. But I am redesigning it. It runs off a simple air pump (like for fishtank). I am not 100% certain, but I feel like you were maybe in the Gold Coast/Brisbane region. If that is so, then when I get the next revision printed at 3dHubs (~$30 for the first print in the line & $20 for each print thereafter), I will be making a video to show it in action & if you're interested we can organise getting another one printed for you.


---
**Donna Gray** *April 27, 2016 01:50*

Yes I live about an hour and a half away from Brisbane I am closer to the gold coast I come from the northern rivers area I don't know much about machines as far as the mechanics of them so I am not able to do upgrades etc If it was a simple thing to setup I could maybe have a go at it Would the air assist stop charing of paper as well I tried the hairdryer but them it didn't cut through I have to be truthful I did cut through the 3mm mdf but I didn't know that a small flame was bad for your focal lense that is the only time it worked was when there was a small flame as the laser was cutting I feel like it is all getting a little hard now and I am very disappointed in outlaying the money as I am now finding I cant do much without upgrading things


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 03:53*

**+Donna Gray** You don't really need to upgrade a lot. The only real upgrades I have done is replace the cutting bed (just to make things easier for the things I tend to work with) & added a makeshift air assist. I just used an aquarium pump & zip tied the hose to the lens mount so that the air focused roughly on the cutting position. The 3D printed one I am working on at the moment is to increase the precision of that (i.e. air will hit right at the laser beam target). Air assist won't totally remove the charring, but it dramatically minimises the charring & also improves the ability to cut cleanly. As someone else mentioned, the laser vaporises the material, but as it causes heat, the material can burn, which is what causes the charring. I'm fairly certain it will happen always, but you can minimise it to be very small amount of charring (i.e. <0.5mm).



So, overall, only real upgrade you need will be to add an air-assist. As you are in the Northern Rivers area (& 1 1/2 hours from Brisbane) you should be reasonably close to me. I am in Nerang, so I gather you're maybe 30-45 mins away from myself. I have spare air pumps (exactly the same as what I am using) that I picked up for next to nothing from an auction (they are actually Tanning Airbrush airpumps). Very basic pumps, but I would be happy to give you one for free (as I have about 10 of them). Are you up Gold Coast direction often? If so, we could arrange a pickup & once I get my air-assist head 3D printed again (have to revise the design as there are problems with the current design) I could show you it in action.


---
**Scott Marshall** *April 27, 2016 14:52*

Might not have been clear on the exhaust. You remove the INTERNAL exhaust tubing. Once done, you re-connect to outside exhaust. I'd never suggest venting into a building. 

If cutting anything that burns, Replace  the Chinese exhaust tubing with aluminum flex tube (non-flammable) vented outdoors.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 15:26*

**+Scott Marshall** Oh, I must have missed the INTERNAL bit. Glad to know you were not meaning to vent the MDF fumes straight into the room.


---
*Imported from [Google+](https://plus.google.com/103145403582371195560/posts/1cpTh8PfrEf) &mdash; content and formatting may not be reliable*
