---
layout: post
title: "How would you go about finding someone local to work on a K40 laser?"
date: January 02, 2017 20:14
category: "Discussion"
author: "timb12957"
---
How would you go about finding someone local to work on a K40 laser? Not the laser tube itself, but the control. 





**"timb12957"**

---
---
**Jonathan Davis (Leo Lion)** *January 02, 2017 20:22*

From what I have seen from around here most people usually do the repair/conversion work themselves. But i could very well be mistaken.


---
**Andy Shilling** *January 02, 2017 20:25*

How about letting people know where you are and if anybodys local they might step up.


---
**timb12957** *January 02, 2017 20:51*

I am located in Lyman South Carolina. I could even ship it if not too far. I have posted in the past and several of the guys here have suggested things to test/try. But nothing has helped. 


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 21:13*

There's a bunch of laser groups on fb too. Post there saying you are looking for <what exact help you are looking for> and <where exactly you are located>


---
**Ned Hill** *January 02, 2017 21:13*

I'm kinda sorta close (300 miles) ;) over in Wilmington, NC.  I would actually recommend that you search out some maker/hacker space groups near you like in Greenville or over in Charlotte.  I'm sure someone connected with a group could get you connected with someone reasonably close with the right skill set.


---
**Ned Hill** *January 02, 2017 21:56*

**+timb12957** check out this markerspace in Greenville. [meetup.com - Greenville SC Makers @ Synergy Mill](https://www.meetup.com/synergymill/)

They are having an open house / help desk meet up next Saturday.


---
**Robert Selvey** *January 02, 2017 22:59*

Wow small world I am also in Lyman, SC. Hey "timb12957" maybe we could meet up and help each other out.


---
**timb12957** *January 02, 2017 23:34*

Cool Robert! That is a small world! And thanks Ned for finding makerspace in Greenville!


---
**timb12957** *January 26, 2017 21:19*

Robert Selvey I would be glad to meet up to help each other. I will private message you my phone number. 


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/Zsdicccm9ri) &mdash; content and formatting may not be reliable*
