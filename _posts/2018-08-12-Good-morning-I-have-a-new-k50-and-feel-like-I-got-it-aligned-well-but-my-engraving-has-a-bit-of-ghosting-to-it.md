---
layout: post
title: "Good morning, I have a new k50 and feel like I got it aligned well but my engraving has a bit of ghosting to it"
date: August 12, 2018 15:38
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Good morning,

I have a new k50 and feel like I got it aligned well but my engraving has a bit of ghosting to it. If u look at the S and the C in the name u can see what I'm talking about. 



Is there any other reason this could happen? Maybe a setting somewhere in the software for example? Or would misalignment be the only reason?



I'm using the lightburn software with stock controller

![images/0b806ba335c5aa068f80c18eef15ff82.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0b806ba335c5aa068f80c18eef15ff82.jpeg)



**"Nathan Thomas"**

---
---
**Travis Sawyer** *August 12, 2018 15:58*

Make sure your x carriage is tight on the rails. The rollers are on eccentrics.


---
**Nathan Thomas** *August 12, 2018 16:16*

**+Travis Sawyer** everything feels tight... the only thing that has a little bit of movement is the tension screw at the right end of the x axis cross rail


---
**Nathan Thomas** *August 12, 2018 16:19*

Looks like its engraving twice on top of each other just offset 1 mm to the right


---
**Travis Sawyer** *August 12, 2018 16:58*

Are you using scan and cut? Could it be possible that you get a belt skip? How fast are you engraving? Is the material jostling a little?


---
**Nathan Thomas** *August 12, 2018 17:03*

Thanks for the troubleshoot ....I'm only using scan...and it happens regardless of speed, but the slower the speed the less it shows, but it still shows up...I have the material taped down. Of it's not the alignment my next guess would be the belt but I've tightened the tension screw as well...no help


---
**Nathan Thomas** *August 12, 2018 17:04*

The alignment ![images/0a0f5f89dfa63fd1739f88cafb8f07f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a0f5f89dfa63fd1739f88cafb8f07f9.jpeg)


---
**Stephane Buisson** *August 13, 2018 08:15*

**+Nathan Thomas** Could the vertical laser beam (passing through the lens) bounce/reflect in anyway ? check verticality, cracked mirror/lens. Also a chance to clean them.


---
**Nathan Thomas** *August 13, 2018 20:40*

SOLVED 

just wanted to update this post in case anyone else comes across this issue



It was solved by using the reverse interval scan.



I'm using Lightburn so that is done by going to Edit ---- Device Settings



From there you can set some random intervals until your lines match up on the right hand side. There's a very detailed write up on how to do this in the LB Facebook group as well 


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/VP5AZKw57xk) &mdash; content and formatting may not be reliable*
