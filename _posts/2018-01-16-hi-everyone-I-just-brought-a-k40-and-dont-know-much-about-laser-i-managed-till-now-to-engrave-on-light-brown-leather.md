---
layout: post
title: "hi everyone, I just brought a k40 and don't know much about laser, i managed till now to engrave on light brown leather"
date: January 16, 2018 17:53
category: "Object produced with laser"
author: "Andrei Voicu"
---
hi everyone, I just brought a k40 and don't know much about laser, i managed till now to engrave on light brown leather. the seller of the laser provided as software laserdrw... on leather im using the option Engrave(sunken), other two are marking and cutting, and the power i use is 6  out of 30. i have tried Using the same options for glass engraving, didn't work and also have tried raising the power up to 30 from 6 with no avail. I tried using the other 2 options (marking and cutting) at various powers ,to engrave the glass but all it did was to brake it. If anyone could help me with a simple tutorial for different materials it would be great.

Thank you.





**"Andrei Voicu"**

---
---
**Chris Hurley** *January 16, 2018 19:27*

Have you read the sticky post at the top? It's a big topic with some art to it. So reading that will give you a primary knowledge base to ask more precise questions. 😀 Most of which will be answered with you need to calibrate more or 1 of 6000 different solution to 6000 possible problems. Welcome to the group! 


---
**Andrei Voicu** *January 16, 2018 20:27*

**+Chris Hurley**  read it after posting but didn't understand much on what i need help with... 

thanks for the heads up. ;) 


---
**Joe Alexander** *January 16, 2018 22:30*

check out [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com) its a great resource for these lasers


---
**Paul Mott** *January 17, 2018 07:06*

There is a lot to learn before the best results can be expected.

Do a test on a scrap piece of the required material. Try a relatively slow speed (10mm/sec. or less) and a very low laser power. Increase the power slowly until you get suitable results.

Practice with various different materials.




---
*Imported from [Google+](https://plus.google.com/113571233865169446047/posts/8YMoaLdtG9h) &mdash; content and formatting may not be reliable*
