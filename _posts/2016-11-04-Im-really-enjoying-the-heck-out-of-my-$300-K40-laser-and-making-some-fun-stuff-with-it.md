---
layout: post
title: "Im really enjoying the heck out of my $300 K40 laser and making some fun stuff with it"
date: November 04, 2016 12:50
category: "Smoothieboard Modification"
author: "Bob Damato"
---
Im really enjoying the heck out of my $300 K40 laser and making some fun stuff with it. I can see the limitations though especially after seeing what you guys can make with some mods.  One of my biggest limitations right now is size. I need to engrave things bigger than my 7x10 (or so) K40 can do, so Im thinking of just getting a much bigger and nicer setup.  That being said, Im considering the one below. But my question is, if I find I have software limitations etc on this type, can I still upgrade to smoothie in the future? Or is that just for our little $300 units? I am thinking of the 100W unit..





[http://www.ebay.com/itm/282108797749?_trksid=p2055119.m1438.l2649&var=581067588898&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/282108797749?_trksid=p2055119.m1438.l2649&var=581067588898&ssPageName=STRK%3AMEBIDX%3AIT)





**"Bob Damato"**

---
---
**Stephane Buisson** *November 04, 2016 13:24*

I can't see why not. a stepper is stepper just a question of driver (Amp). the PSU is the main thing (pwm), try to find out if you can identify it before purchase and get the manual.


---
**Ian C** *November 04, 2016 15:24*

Hey buddy. From what I've read about the 60 watt and above with DSP controllers you won't need to think about a smoothie again. They are that good and there is a great many tutorials and help on how to use them. Jump on Facebook and search "laser engraving and cutting" lots of users with them producing nice work after the usual Chinese wiring changes, bit mechanics and controller are commended


---
**Bob Damato** *November 04, 2016 16:16*

Thats actually really good info **+Ian C**  That was actually my first hunch since they talk about color layers etc. It seems those are the kind of things smoothie would do for me. Great, thank you!


---
**Ian C** *November 04, 2016 16:51*

You're welcome buddy. I don't have one myself, little K40 stock here, but have been doing the homework to decide bigger cnc router or laser. The X700 clone group on Facebook is also another good one


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/Hz5pBJ2T7n8) &mdash; content and formatting may not be reliable*
