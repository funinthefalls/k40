---
layout: post
title: "Hi friends, My troublesome new laser decided that it no longer wants to fire"
date: July 26, 2016 11:15
category: "Hardware and Laser settings"
author: "Anthony Santoro"
---
Hi friends, 

My troublesome new laser decided that it no longer wants to fire. 

It is 1 week old and has been nothing but trouble since it arrived. 

The machine is powered correctly, laser switch is on, when I press the test fire button I hear a whistling sound but no beam is fired. The mA gauge remains on 0. The power supply cooling fan is on and the power supply light is on. 

I have followed the wiring, nothing seems to have disconnected. 

What is the best thing to check? How do you recommend I troubleshoot this issue? What may it be - faulty power supply, failed tube? 

The support from my seller is poor, they sent me one email with a YouTube link to a alignment video. There is no beam to align. 

I've raised  a PayPal claim to prompt the seller to provide support, I don't know what else I can do... 

Your assistance will be greatly appreciated. 





**"Anthony Santoro"**

---
---
**greg greene** *July 26, 2016 13:19*

Have you aligned the mirrors yet?


---
**Scott Marshall** *July 26, 2016 16:13*

Sounds like a bad power supply. If the Ma meter isn't moving, There's about a 90% chance that's the issue. I'd price a new power supply, ( they're usually 90-$120 on ebay) and then go back to the seller. Most sellers are willing to give you cash back to fix the problem rather than pay to ship them back and forth. They also don't want the negative feedback, so usually make you a decent deal.



Mine arrived damaged and after a brief back and forth with photos, I was offered $250 on a $310 laser to fix it mayself.



This is all part of the "K40 experience" and not at all uncommon.



Scott


---
**Stephane Buisson** *July 26, 2016 16:17*

just a thought, if you can ear a sound when firing but nothing on vue meter (0mA), it could be a short or arcing  before the tube. check/redo silicone on Pin arriving on tube.


---
**Anthony Santoro** *July 26, 2016 21:12*

Thanks for the advice team. 

I will firstly try to re-silicone the tube terminals. If that fails, I will try to install the power supply from my old machine into my new machine. 

I appreciate everybody's help! 


---
**Harold Oney** *July 27, 2016 17:18*

Unfortunately good luck with paypal. They just denied my claim..probably to the same company.. with no reason given.  I have sent them a no so nice reply


---
**Anthony Santoro** *July 27, 2016 21:07*

I checked the connections to the pins, all seems okay. Even removed them and connected them to be sure. 

Next step is to replace the power supply I guess. I will wait until I have finalised my ticket with PayPal before I take anything apart. 

I hope PayPal isn't as dismissive with my claim as they were with yours **+Harold Oney**​. I hope yours gets resolved successfully also! 


---
*Imported from [Google+](https://plus.google.com/106649411786258336023/posts/cwk9XzLr3Sn) &mdash; content and formatting may not be reliable*
