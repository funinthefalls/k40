---
layout: post
title: "Received one more piece of puzzle today"
date: December 16, 2015 16:29
category: "Hardware and Laser settings"
author: "ChiRag Chaudhari"
---
Received one more piece of puzzle today.



![images/0eb647831ec35e42f6ba58dc7429b637.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0eb647831ec35e42f6ba58dc7429b637.jpeg)



**"ChiRag Chaudhari"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 19:00*

Does the air assist input go into the side perfectly perpendicular or is it at a slight angle?


---
**ChiRag Chaudhari** *December 16, 2015 19:28*

**+Yuusuf Sallahuddin** Its perfectly perpendicular.


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/UipftzVwSyp) &mdash; content and formatting may not be reliable*
