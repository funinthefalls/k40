---
layout: post
title: "Scott Marshall I was thinking about what you mentioned regarding 3rd & 4th axis for the K40 to enable angled cutting (I assume you meant mitre cuts on the edges?)"
date: April 08, 2016 11:30
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
**+Scott Marshall** I was thinking about what you mentioned regarding 3rd & 4th axis for the K40 to enable angled cutting (I assume you meant mitre cuts on the edges?).



Anyway, I've spent a little while considering this & drawing up an idea that came to my head in regards to how you could effectively do that.



Minus the mechanical/electrical components, this is more of an idea/concept at the moment. I do believe it would be a great modification, albeit a bit complicated to implement (well at least with my limited knowledge of mechanics/electrics haha).



**+Thor Johnson** Thought you might be interested in this idea as I noticed your thoughts also on Scott's idea, where you suggested that the angle of the cut would modify the dimensions of the cut object. I believe that can be neglected using a system like this, where the dimensions of the object would remain the same (at least for the top side of the object). However, one thing I would consider is that cut power would possibly require more power for angled cuts, as it is having to cut through extra distance.

![images/f8dde9bfd979680143cadaa671de0e80.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f8dde9bfd979680143cadaa671de0e80.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 11:32*

Note: values are based on the sizes I used for mirrors & lens. E.g. the mirror size in the image was 20mm2 x 4mm. The lens size was 20mm2  3.5mm. So those specific values of the movement would vary dependant upon actual sizes (in order to line beam up precisely with same cutting position).


---
**Phillip Conroy** *April 08, 2016 11:55*

Instead of having the laser head attached where it is now why not pivit it some way and extend down the focal lens- that way no more mirrors are required-laser head mirror stays where it is and botton of laser head mover


---
**Phillip Conroy** *April 08, 2016 12:03*

cont - laser head mirror would rotate and still stay in [alignmet.to](http://alignmet.to) test if the k40 has enough cutting power to cut 45 deg angle some scrap at 45 deg in the laser bed and hold it there with anything that works.keep in mind the cut may not be pretty as the efective cutting distance of the lens with a focal distance  of 50.8mm is not a lot


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 13:28*

**+Phillip Conroy** I have given some thought to this, as this was my original consideration (to rotate/angle the top mirror, Mirror 3). However, due to the nature of getting the beam to align in the exact same position on the workpiece, it wouldn't work (except with every angle change requiring the software to move the laser head to offset a certain X & Y value to compensate for the change of beam cut point). Unless there is something I am not understanding correctly in your explanation.



As the mitre angle increases, the beam moves exponentially from the origin point (of where the beam starts at a 0 degree angle). It will increase until infinitely far away from the origin point (i.e. when the mitre angle is 180 degrees, or the beam is just going parallel to the cutting floor). As such, the concept I drew up was to minimise modifications to the software required to control the cutting position, allowing for more "on-the-fly" angle changes.



I didn't take into consideration focal distance when sketching up this concept, because I didn't think to consider it at all (this is why 2 heads are better than 1). I'm not very mechanically/electrically minded however, so all my ideas are based on no actual theory, more like "maybe-this-will-do-the-job" sort of things.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 13:34*

Actually, I just registered that when rotating the "attachment" on the right, it won't have the beam hitting in the exact same position. It would be offset by whatever the distance is from Mirror 4 to Mirror 5 @ 0 degrees position. It would end up cutting a circle of that offset radius as it rotates.


---
**Thor Johnson** *April 08, 2016 15:50*

I was thinking of using 3 linear actuators, but then it only lets you bevel X-Wise.

  A) Just below M3, aligned with X rail

  B) Down below (someplace near lens), also aligned with X rail

 Both A & B are attached to the turret with rod ends; you'll adjust A & B to make a virtual circle with the center being the desired spot.

  C) since the distance to the work changes, C is along the turret to maintain a constant focal length for the circle.

You could get Y beveling by adding 2 more rod-ends... if you analyze everything right, I think you could actually do this with 3 actuators (X angle, Y angle, Focus) with 2 4-bar linkages to make the virtual circle (hell, you might even be able to make focus part of the 4 bar linkage setup).  A complicated bit of kit for putting bevels on 1/4" pieces (and remember, if you're beveling, don't get outside of your focus sweet spot -- the depth of focus isn't much with the stock lenses)...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 15:54*

**+Thor Johnson** All of that just went way over my head. Haha. Maybe some others here might understand that. I actually have no need (currently) for creating bevelled edge cuts, but it would be an interesting feature. I just was thinking about what Scott had mentioned & thought I would draw down some concept sketches.



Thanks for your explanation though, I'll give it a more thorough read & a bit of research to see if I can figure it out.



**+Thor Johnson** edit: after reading up a bit on what are linear actuators & four-bar-linkages, I have a slightly better understanding of what you were saying.



I was considering 4-bar-linkages attached to some wheels/cogs in order to move/position the mirror 5 & lens. However, I didn't know the name of it or how to calculate it so it does what I wanted it to do.



You're right & I agree that it is an overly complicated bit of machinery for something minor like bevelled edges on 1/4" materials.


---
**HalfNormal** *April 09, 2016 01:18*

What about laser galvo's?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 09, 2016 02:08*

So, I've made adjustments to my original design concept, to correct the issue I determined would exist when rotating (to change direction of mitre cut). It will be offset & create a mitred circle, instead of a hitting the same position & mitre-ing a dot. So, here is the link to my adjusted idea (required an extra 2 mirrors). [https://drive.google.com/file/d/0Bzi2h1k_udXwZ3lQRUxndVRrMFk/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZ3lQRUxndVRrMFk/view?usp=sharing)



**+HalfNormal** I'd never heard of laser galvos until you mentioned it just now, but upon checking what they are, I'd imagine they could be utilised in some manner.


---
**Thor Johnson** *April 11, 2016 03:57*

**+HalfNormal** The big problem with Galvos is that that our final focus lens has such a short depth-of-focus.... so if you use a glavo, you have to keep the distance the same +/- 0.2mm or you're out of focus (this gets lessened if you use a longer focal length lens, but then the focused size is larger.... the balance of optical design). 


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/P5LhFtRnA38) &mdash; content and formatting may not be reliable*
