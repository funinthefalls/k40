---
layout: post
title: "Used LaserWeb to process this job. Worked great"
date: August 05, 2016 05:07
category: "Object produced with laser"
author: "David Cook"
---
Used LaserWeb to process this job. Worked great.

50℅ power 20mm/s



Abalone shell that a co-worker core drilled and tumbled.



![images/3d712be79af1be1bd85ff50d8b3a9a77.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d712be79af1be1bd85ff50d8b3a9a77.jpeg)
![images/9535243de5cd4958dbe0cbea530cfd61.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9535243de5cd4958dbe0cbea530cfd61.jpeg)
![images/4ff6b73afd3dc631d2dd33ca756bbbcb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4ff6b73afd3dc631d2dd33ca756bbbcb.jpeg)

**"David Cook"**

---
---
**Brett Cooper** *August 05, 2016 07:56*

Nice, I wonder what it would do to shells.  They often have a dark exterior and light inertia.   

Also sandstone (high in silica), I've seen gas touches melt it as glass melts at 1300c


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/LrcwG7K4Wea) &mdash; content and formatting may not be reliable*
