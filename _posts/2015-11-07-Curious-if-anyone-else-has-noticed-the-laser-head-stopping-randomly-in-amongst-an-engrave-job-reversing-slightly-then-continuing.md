---
layout: post
title: "Curious if anyone else has noticed the laser head stopping randomly in amongst an engrave job, reversing slightly, then continuing?"
date: November 07, 2015 07:03
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Curious if anyone else has noticed the laser head stopping randomly in amongst an engrave job, reversing slightly, then continuing?



I just noticed it happening about 10 times in a two or three horizontal lines period.



Is there something wrong if it is doing this?





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ashley M. Kirchner [Norym]** *November 07, 2015 07:20*

Mine does it on large engraving jobs. Smaller ones doesn't do that. Some sort of recalibration.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2015 07:21*

**+Ashley M. Kirchner** Thanks Ashley. I was wondering was it some sort of problem.


---
**Joey Fitzpatrick** *November 07, 2015 16:33*

It is a rom overflow issue.   The stock controller runs out of space on larger more intricate engraving jobs.  It is a nuisance, but the engravings still come out ok.  Just takes longer than it should


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 08, 2015 15:29*

**+Joey Fitzpatrick** I originally read you comment as "a room overflow issue" and just re-read it as "rom overflow issue". So a memory issue?


---
**Joey Fitzpatrick** *November 08, 2015 15:48*

Yes, the microprocessor on the the stock controller has a memory limitation.  When engraving something that requires more memory that the micro can handle, the machine will pause as the laser head is in motion.(as it catches up with itself)  you will notice this happening on raster engravings when there is a lot of area to be engraved.  The engraving will still turn out good, it just takes longer due to the repeated pausing and re-starting.

 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 08, 2015 17:17*

**+Joey Fitzpatrick** Thanks Joey, that makes a lot of sense. I imagine it is kind of like steaming a video with slow internet connection, has to keep pausing to buffer new data.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/FbHxGxAFpfL) &mdash; content and formatting may not be reliable*
