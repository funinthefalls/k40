---
layout: post
title: "Can someone tell me the approximate beam travel lenghts from the tube to the focal lense for the following coords (Xmin,Ymin)(Xmin,YMax)(Xmax,Ymin) and (Xmax,YMax)"
date: April 04, 2016 01:36
category: "Discussion"
author: "Alex Krause"
---
Can someone tell me the approximate beam travel lenghts from the tube to the focal lense for the following coords (Xmin,Ymin)(Xmin,YMax)(Xmax,Ymin) and (Xmax,YMax)





**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 06:00*

I can give it a quick measure later on once I put my machine back together unless someone else can provide it beforehand.


---
**Stephane Buisson** *April 04, 2016 09:33*

by conception X and Y travel are in the same plan as the tube (all parallel) so the answer should be table size in X and  Y, plus Tube distance in X from the table + Y distance for tube to 1st mirror + Y distance 2nd mirror to table + vertical (head+focal(50.8)).

but why are you asking that ? what are you trying to do ? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 12:01*

I drew up an example of the approximate distances. [https://drive.google.com/file/d/0Bzi2h1k_udXwel94Y0hWRDRCUlE/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwel94Y0hWRDRCUlE/view?usp=sharing)



But that is to the centre point of the 3rd mirror (not including the distance from that mirror to the focal lens). Note: my cutting area is 300mm x 220mm (as I removed some bits to enable it to go slightly further on the Y-axis). Normally is 200mm Y-axis.


---
**Alex Krause** *April 04, 2016 13:55*

I keep reading how different areas of the bed have different cutting strength based off of beam travel just trying to wrap my head around how much strength is lost over the length of the bed 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 16:05*

**+Alex Krause** It would be nice to know how much strength is lost after a certain distance. Maybe would be possible to add an automatic power adjustment based on the distance from the origin point for **+Peter van der Walt**'s LaserWeb?


---
**Alex Krause** *April 04, 2016 16:09*

**+Yuusuf Sallahuddin**​ that is kind of the direction I was going in It would be nice to know if power loss is linear 


---
**Phillip Conroy** *April 07, 2016 10:41*

I have ordered a 100 watt  laser power meter and will meaxure and post results,not sure if ut can measure after the focal lens as this power is very concentrated[5mm round down to 0.1mm round]


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/XVQPpcDRBZa) &mdash; content and formatting may not be reliable*
