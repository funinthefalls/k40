---
layout: post
title: "Getting the hang of this. :) Made my first practical item today"
date: June 26, 2016 00:27
category: "Object produced with laser"
author: "Ned Hill"
---
Getting the hang of this. :)  Made my first practical item today.  Bought an air flow gauge  off of banggood  for my air assist and used my wonderful new laser machine to create a mounting plate.  Cut it out of 1/4" ply.  It was a great project to help me learn some of the basics of Corel Draw.



![images/533a73794c1b7bf10722c89bbc7a17c2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/533a73794c1b7bf10722c89bbc7a17c2.png)
![images/4ff498f480555938d5c23fb0ffa4ba8c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4ff498f480555938d5c23fb0ffa4ba8c.png)

**"Ned Hill"**

---
---
**Ned Hill** *June 26, 2016 00:29*

I also learned today that if you want to cut or engrave a layer the little print icon for the layer must be clicked on.  Spent 30 mins trying to figure out why my cut layer would not cut :P


---
**Anthony Bolgar** *June 26, 2016 00:44*

Nice job. And a practical addition to your K40.


---
**Jim Hatch** *June 26, 2016 00:59*

**+Ned Hill** The layer print icon is really handy. That way you can keep your entire project in 1 file. Separate the cut & engrave sequences by layer (just like other laser software lets you do it by color) and then output one layer at a time in the order you want. 



I still use colors because the other laser I use lets me use the colored lines to then set engrave/cut operational parameters (speed, power, engrave, cut, etc). I can use the same file on my K40 or the bigger 60W machine.


---
**Ned Hill** *June 26, 2016 01:08*

**+Jim Hatch** Yeah that's what I'm doing as well.  Had a layer for the engraving and one for the cutting with different colors  The engrave layer executed just fine but the cut layer would not execute and it was driving me crazy until I realized that the cut layer print icon was off.  I've been using the object manager to select the items to execute, but I'm guessing I could just select all and then have the layer I don't want just turned off.


---
**Josh Rhodes** *June 26, 2016 01:33*

Copy/paste from inkscape to corel draw usually works 100%..

There I solved the corel draw problem once and for all...﻿


---
**Robert Selvey** *June 26, 2016 02:26*

Looks really nice.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 03:10*

Cool add-on for your cutter. It would be cool if your little flow gauge was illuminated :D



I don't bother with the layers, as I just position all my separate engrave/cut objects all over the place in the CorelDraw document. Since I use a bounding box (no fill/no border) around every job-group to position it (instead of the 1px dot for orientation) my job-groups are always set to 320x220mm & positioned inside the bounding box where they need to be. I just click whichever job-group I want to cut/engrave, tell it to go, then click the next job-group & tell it to go, etc.


---
**Ned Hill** *June 26, 2016 03:41*

**+Yuusuf Sallahuddin** Great idea on the illumination.  Now if only I had some LED's....... [https://www.dropbox.com/s/t4lhr90w765bxr2/20160625_233211.jpg?dl=0](https://www.dropbox.com/s/t4lhr90w765bxr2/20160625_233211.jpg?dl=0)

Oh yeah I can make this happen :D

[https://www.dropbox.com/s/k2m7pjkutg83znj/20160625_233338.jpg?dl=0](https://www.dropbox.com/s/k2m7pjkutg83znj/20160625_233338.jpg?dl=0)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 05:05*

**+Ned Hill** That's awesome. Looks even better than I had imagined.


---
**Jim Hatch** *June 26, 2016 13:40*

**+Yuusuf Sallahuddin**​ The bounding box is a good idea. But, I use multiple lasers and they have different ways to get to the laser from the design program. One of them (a $5000 60w laser) doesn't have a direct driver from Corel/AI/Inkscape so I have to use their version of LaserDRW (LaserCut 6.1) as the intermediary. Using the layers & colored lines makes it relatively straightforward. Fortunately it has a bigger bed (18" x24") in addition to higher power so it's worth the extra effort 🙂



I have a Glowforge Pro slated for delivery the end of the year so am looking forward to the simplified job stream process it offers. I'll also be building an 80W (or 100W) large bed (36"x24") laser, Smoothie based this winter I hope. 



The layer technique I use keeps it pretty straightforward to move a job from one machine to another.


---
**Robert Selvey** *June 27, 2016 19:17*

what pump are you using for your air assist ?


---
**Ned Hill** *June 27, 2016 19:57*

**+Robert Selvey** I'm using this pump [https://www.amazon.com/gp/product/B001BJFHAW](https://www.amazon.com/gp/product/B001BJFHAW) with a 5 gal portable air tank to dampen pump oscillations.  This pump does about 23 lpm which is plenty.  I'm running my air flow between 10-15 lpm for most things so far.


---
**Tev Kaber** *June 29, 2016 21:27*

I just got this [https://www.amazon.com/gp/product/B002JPRNOU/](https://www.amazon.com/gp/product/B002JPRNOU/) set up yesterday, which claims to be 45 lpm but it doesn't feel like a lot of air... I have no idea contextually how much air to expect, though.  I guess I'll find out if it's enough when I do some cutting tonight.


---
**Robert Selvey** *June 29, 2016 22:16*

Can't wait to hear what you think about the pump.


---
**Tev Kaber** *June 30, 2016 03:18*

Seems to do the trick!  Before, cutting = fire, now... no fire!



It's not too loud either, well, the ventilation blower I have is so loud, everything else is drowned out...


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/hNsP1d5A7vj) &mdash; content and formatting may not be reliable*
