---
layout: post
title: "A quick question: what cooling pumps do you use?"
date: June 18, 2016 17:45
category: "Modification"
author: "Mircea Russu"
---
A quick question: what cooling pumps do you use?

Mine came with a 2000 l/h (528gph) and I also have a 3000l/h one(792gph) but using a much larger diameter output. I'm a little scared to use the bigger one since I don't want to blow the glass connectors or a hose from the larger pressure.





**"Mircea Russu"**

---
---
**Jim Hatch** *June 18, 2016 20:14*

I use the stock one with a 5 gallon bucket of water. Works fine in Connecticut (during the winter I leave the pump running when it's near freezing or below - mine is in the garage. 



I have a flow sensor and a standard electronic thermometer stuck thru the bucket lid. Grabbed the thermometer from the kitchen 😃


---
**Jason He** *June 19, 2016 04:48*

What is the make and model of your stock pumps?



My K40 came with the Dymax PH1200 (1200 LPH/317 GPH). I upgraded to a Hydrofarm AAPW400 (1514 LPH/400 GPH) and now I'm wondering if I should have gone even higher to the 550 GPH model after hearing yours comes stock with 2000 LPH.


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/UvTYX5xdEqD) &mdash; content and formatting may not be reliable*
