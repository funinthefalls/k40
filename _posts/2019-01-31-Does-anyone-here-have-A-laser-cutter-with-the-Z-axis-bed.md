---
layout: post
title: "Does anyone here have A laser cutter with the Z-axis bed?"
date: January 31, 2019 03:42
category: "Discussion"
author: "Eric Lovejoy"
---
Does anyone here have A laser cutter with the Z-axis bed?

I was wondering if the feature would be very helpful, as the focal point on my k40 is constant. Do the lasers with Z-axis beds have any means to adjust the focal point as well? Or is that all manual? 



on my laser I just removed the stock bed  entirely, and installer rails on springs, 

I push the rails down and adjust them manually, its pretty simple.  where there are only 2 rails,  I can also fit items in and prop them up with different size blocks to hit my focal point. Im just wondering if Z-axis control offers a worthwhile improvement to my laser use.





**"Eric Lovejoy"**

---
---
**Don Kleinschnitz Jr.** *January 31, 2019 12:39*

I have a powered Z axis and I would say that a powered  Zaxis is a convenience over what you are doing now. Sometimes it is a pain to find the right spacer (block) for different materials. Exact focus is important for quality.

 I use my a lot.



My table: [donsthings.blogspot.com - Stand alone K40 Zaxis Table & Controller Build](https://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)



There is also a clamping design out there that sounds like your rails.



I did this design but never built it: 

[https://donsthings.blogspot.com/2017/02/k40-clamping-table.html](https://donsthings.blogspot.com/2017/02/k40-clamping-table.html)




---
**Dan Stuettgen** *January 31, 2019 17:19*

What is the focal distance from the the lens to the surface material being cut or etched?


---
*Imported from [Google+](https://plus.google.com/107285166924069729045/posts/8KJ4iPeLvXz) &mdash; content and formatting may not be reliable*
