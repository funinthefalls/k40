---
layout: post
title: "So went to install a meter in my machine, do I just attach the ground to where it came out of the power supply?"
date: November 30, 2016 05:37
category: "Modification"
author: "Kelly S"
---
So went to install a meter in my machine, do I just attach the ground to where it came out of the power supply?  That is how I did it, but it does nothing.  Either I did it wrong or I got a dud.  :p





**"Kelly S"**

---
---
**Anthony Bolgar** *November 30, 2016 09:49*

Are you talking about an Amp meter? If so it goes between the ground side of the tube and the PSU.


---
**Ned Hill** *November 30, 2016 12:35*

Yep, goes between the cathode end of the tube and the PSU.  Here's a video of someone talking about it.


{% include youtubePlayer.html id="4tDK3yOhN3E" %}
[youtube.com - K40 eBay Chinese CO2 Laser - Measuring Laser Tube Current!](https://youtu.be/4tDK3yOhN3E?list=PLInTrkIbj69kPO_UP81yxX-xov4SVyPlD)


---
**Kelly S** *November 30, 2016 13:50*

My meter must be a dud, as the connections are solid.  Machine works great just nothing registers in the meter.


---
**Kelly S** *November 30, 2016 16:41*

I have the wrong type of meter, doh.  Got a regular amp meter instead of a mAMP.  My bad, lesson learned, and will have the right one tomorrow.   :)


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/Y35oZbvXdLQ) &mdash; content and formatting may not be reliable*
