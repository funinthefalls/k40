---
layout: post
title: "OK I just tried to engrave an acrylic sheet"
date: May 04, 2016 04:19
category: "Discussion"
author: "Tony Sobczak"
---
OK I just tried to engrave an acrylic sheet. Used 300 mm/sec and somewhere around 5ma. The result was it looked like someone used a soldering iron on it. The edges of the image and letters are above the surface of the acrylic. Help.  What did I do wrong?





**"Tony Sobczak"**

---
---
**HP Persson** *May 04, 2016 05:42*

What kind of acrylic?

I beleive extruded acrylic is poor to engrave, remember i have read that somewhere.

And casted acrylic should be better for that.



I don´t do engravings myself, so i do not have any experience on the topic, just remembered reading about it ;)


---
**Jim Hatch** *May 04, 2016 06:25*

**+HP Persson**​ is correct. It has to be cast acrylic. Otherwise it melts too much. Also, sometimes "acrylic" isn't - it's some kind of polycarbonate or lexan. Lots of people & stores use "acrylic" like "Kleenex".


---
**Anthony Bolgar** *May 05, 2016 15:08*

I have no issues engraving extruded acrylic, I usually engrave at 800mm/s with around 6-8mA


---
**Jim Hatch** *May 05, 2016 15:19*

**+Anthony Bolgar**​ That's the ticket for extruded acrylic - high speed & low power. That way it doesn't get a chance to melt back into the kerf and blob up. Cast acrylic tends to have more consistent molecular structures that make it more resistant to the melt/blobbing effect as well as higher transparency index which is why I prefer it.



The other key is to make sure the cut line is as thin as possible (I tell the software it's .001mm so I don't get double-line cuts).


---
**Jim Hatch** *May 06, 2016 01:26*

Well it should be labeled if it's cast - it's harder to make so it's more expensive. It is slightly heavier than an extruded piece of the same size but probably not something you can measure. If you laser engrave it, cast will turn a matte white but extruded will turn a grayish. Cast acrylic is poured in a mold and set vs extruded which is squeezed out of a machine so you may see faint lines in the extruded acrylic along the direction of extrusion. If you cut cast acrylic you should get a smooth edge. If you cut extruded you tend to get burrs on the back side of the laser cut.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/TZxegpkkj5S) &mdash; content and formatting may not be reliable*
