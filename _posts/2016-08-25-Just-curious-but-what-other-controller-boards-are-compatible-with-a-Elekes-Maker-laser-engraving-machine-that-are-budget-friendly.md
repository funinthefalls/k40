---
layout: post
title: "Just curious but what other controller boards are compatible with a Elekes Maker laser engraving machine, that are budget friendly?"
date: August 25, 2016 03:40
category: "Discussion"
author: "Jonathan Davis (Leo Lion)"
---
Just curious but what other controller boards are compatible with a Elekes Maker laser engraving machine, that are budget friendly? 





**"Jonathan Davis (Leo Lion)"**

---
---
**Anthony Bolgar** *August 25, 2016 03:59*

Most of the motion control cards will work, but you need to deal with the following:

1. Do you need external stepper drivers (Needed if the steppers draw more than 2A)

2. Do you know what wires go to the end stops, steppers, laser PSU etc. You will have to figure out how to interface the new controller with the existing machine. Usually involves some rewiring of the system.

3. What do you want to use the laser for. If it wiill only be used to cut, just about any motion controller will work. If you want to do engraving, the hands down best card is a smoothieboard. It does proportional laser power based on acceleration so that the edges of the engraving are not darker than the rest.

4. What is you tool chain, ie, what cad program do you use, what type of file does it create. If using a smoothieboard, you can easily use most cad programs, and import the files into LaserWeb3, which has great smoothieboard support, it will also work with a grbl controller as well.

5. What is you definition of budget friendly. Most open source controllers will be less that $200. Not that much when you consider the cost of a laser cutter/engraver.



If you could find out this info, it will be much easier to reccomend you a good controller.



Hope this helps, and if you have any questions, feel free to ask. We have a great community here with a ton of laser specific as well as general CNC knowledge. One great plus about a smoothieboard is you can have direct contact with the developers, they are always willing to help out and figure out solutions for just about any scenario.


---
**Jonathan Davis (Leo Lion)** *August 25, 2016 04:02*

**+Anthony Bolgar** ok because I'm currently in the process of fighting Gearbest to get a replacement Elekes maker controller board (value $30 according to Aliexpress) for my machine since the provided one in the kit was defective. 


---
**Ariel Yahni (UniKpty)** *August 25, 2016 04:13*

**+Jonathan Davis**​ has a similar situation as you. He needsto change the stock board to something else. But then. He needsto be able to drive the diode. I think **+Jose Salatino**​ solved this allready with ramps


---
**Jonathan Davis (Leo Lion)** *August 25, 2016 04:22*

**+Ariel Yahni** yep, or either pay $30 to get a new one from Aliexpress! Cause Gearbest is definitely not being like Amazon (customer centric) 


---
**Eric Flynn** *August 25, 2016 17:20*

Seriously, just buy a GRBL setup from Amazon


---
**Jonathan Davis (Leo Lion)** *August 25, 2016 17:50*

**+Eric Flynn** the machines controller board was compatible with the GBRL system 


---
**Eric Flynn** *August 25, 2016 18:55*

Yes, and its a dud, so buy a new one.


---
**Jonathan Davis (Leo Lion)** *August 25, 2016 19:07*

**+Eric Flynn** yeah, but still that's where Gearbest needs to be the ones who buy it or give me a full refund for my unit!


---
**Anthony Bolgar** *August 25, 2016 19:15*

If you paid by paypal, I would start a dispute, gearbest is not known for its customer service.


---
**Jonathan Davis (Leo Lion)** *August 25, 2016 19:34*

**+Anthony Bolgar** I have done a dispute yes, but PayPal is siding with Gearbest and not really getting anywhere with it. Saying that if I want a refund I have to pay and send it back to China! 


---
**Jonathan Davis (Leo Lion)** *August 25, 2016 19:37*

My evidence;

[https://goo.gl/photos/D3uTBhL31EPLqLsz6](https://goo.gl/photos/D3uTBhL31EPLqLsz6)




{% include youtubePlayer.html id="gwACCCYFsqs" %}
[https://youtu.be/gwACCCYFsqs](https://youtu.be/gwACCCYFsqs)


---
**Robi Akerley-McKee** *August 26, 2016 18:45*

Some sort of GRBL board with laser diode driver on it.  I've had nothing but troubles with GRBL boards. I switched to Mega2560/RAMPS 1.4 or MKS Gen v1.4 boards.  would need to get some sort of diode laser driver pwm board to go with it.  I use Build-master-marlin firmware on them. and the turnkey laser plugin for inkscape.  Pronterface to upload to the board.


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/DdAgacyRAsg) &mdash; content and formatting may not be reliable*
