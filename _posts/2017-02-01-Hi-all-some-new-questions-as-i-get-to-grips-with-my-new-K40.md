---
layout: post
title: "Hi all, some new questions as i get to grips with my new K40"
date: February 01, 2017 14:43
category: "Modification"
author: "Rob Kingston"
---
Hi all, some new questions as i get to grips with my new K40.



I have printed a air assist head for my K40 but I'm having trouble reliably mounting it, I've tried a screw in the hole in it and double sided tape but its not secure, how have other people attached theirs?



I have bought one of the air pumps pictured below, how have people mounted these? i was thinking about mounting it in the casing but worried about vibration from a noise and damage to other components point of view. Any recommendations?



Thanks again!





**"Rob Kingston"**

---
---
**Ned Hill** *February 01, 2017 15:10*

Might help if you can provide a pic(s) of the relevant parts.


---
**Rob Kingston** *February 01, 2017 15:27*

My mistake! Thought I had included this ![images/52014e42c57b90a5f3707909910310dd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52014e42c57b90a5f3707909910310dd.jpeg)


---
**Ned Hill** *February 01, 2017 15:33*

Definitely don't mount that inside the case.  The vibration can cause issues.  Also if you want help on the air assist head relevant pics would help there as well.


---
**1981therealfury** *February 01, 2017 15:48*

I agree with **+Ned Hill** on that one, i would not advise to install that in the case.  I have the same pump rated at 60lpm and it had a lot of vibration with it.  I actually have it suspended from a leg on the table that my K40 is on by strips of elastic, does a good job of negating the vibrations, but its still a tad noisy for my liking.


---
**Rob Kingston** *February 01, 2017 15:59*

Thanks for the replies guys, you've confirmed my thoughts, just need to find a suitable place to mount it. As for the head, I've temporarily rigged it up but I'm going to re design it and print it again 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 01, 2017 16:17*

**+Rob Kingston** With my 3D printed air-assist head, I designed it to be a super tight fit around the lens holder base (the bit that screws in). 



You can have a bit of a look here ([https://plus.google.com/u/0/+YuusufSallahuddinYSCreations/posts/QHZTHdcDHEV](https://plus.google.com/u/0/+YuusufSallahuddinYSCreations/posts/QHZTHdcDHEV)) or scroll back ages in my older posts (around April/May 2016).

[plus.google.com - Not the easiest to see, but here is a SMOKE TEST through the Air-Assist Nozzl...](https://plus.google.com/u/0/+YuusufSallahuddinYSCreations/posts/QHZTHdcDHEV)


---
*Imported from [Google+](https://plus.google.com/104868032632666700277/posts/fTPh3PHPLkf) &mdash; content and formatting may not be reliable*
