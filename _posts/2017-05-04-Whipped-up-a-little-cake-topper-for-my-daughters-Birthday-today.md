---
layout: post
title: "Whipped up a little cake topper for my daughter's Birthday today"
date: May 04, 2017 04:33
category: "Object produced with laser"
author: "Ned Hill"
---
Whipped up a little cake topper for my daughter's Birthday today.  Made from 3mm Alder.

![images/5cec9d9f6caae5b3ba35895efbca2e6a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5cec9d9f6caae5b3ba35895efbca2e6a.png)



**"Ned Hill"**

---
---
**Paul de Groot** *May 04, 2017 04:41*

**+Ned Hill**​ always amazed about the level of detail and the designs that immediately feel right.  How do you do that?😀


---
**Ned Hill** *May 04, 2017 04:46*

Thanks Paul :)  


---
**Cesar Tolentino** *May 04, 2017 05:27*

Agree with Paul. It's so natural for you


---
**Ned Hill** *May 04, 2017 11:04*

Also made her a birthday button to wear to school.  Lol, she "awe dad" and rolled her eyes when I gave it to her but she wore it :)

![images/fe829c7713f0d341427a81f63f4a0b50.png](https://gitlab.com/funinthefalls/k40/raw/master/images/fe829c7713f0d341427a81f63f4a0b50.png)


---
**Cesar Tolentino** *May 04, 2017 14:37*

Hahaha.  I can imagine her reaction. Hahaha


---
**Mark Brown** *May 04, 2017 15:06*

What, no pink glitter glue on the button?


---
**Ned Hill** *May 04, 2017 15:41*

Lol considered it, but that glitter paint/glue is a real pain the ass to paint on to small areas.  Plus it was a very last minute addition. :)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/TC59Ws85Yiw) &mdash; content and formatting may not be reliable*
