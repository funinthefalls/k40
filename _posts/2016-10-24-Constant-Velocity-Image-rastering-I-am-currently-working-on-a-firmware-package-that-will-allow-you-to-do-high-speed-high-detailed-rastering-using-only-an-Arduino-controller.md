---
layout: post
title: "Constant Velocity Image rastering. I am currently working on a firmware package that will allow you to do high speed high detailed rastering using only an Arduino controller"
date: October 24, 2016 23:10
category: "Discussion"
author: "Nick Williams"
---
Constant Velocity Image rastering.



I am currently working on a firmware package that will allow you to do high speed high detailed rastering using only an Arduino controller.



I have been discussing some of these issues with the community on another thread.  

[https://plus.google.com/117537980354391774283/posts/j711CAv2tN4](https://plus.google.com/117537980354391774283/posts/j711CAv2tN4)



In the mean time here is some video and the resulting image this was about 8 min in all and the image is 100x84 mm




**Video content missing for image https://lh3.googleusercontent.com/-cUehDCUSJBE/WA6U4b8dsFI/AAAAAAAACoY/zRZkOmx0ieUUmmowNyNUiYdY_sHhNU48QCJoC/s0/VID_20161024_152206.mp4.gif**
![images/4abda0e51804b8060d403f7be60d337b.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/4abda0e51804b8060d403f7be60d337b.gif)
![images/4a3e836719e1d1d03082acd3d2a556db.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4a3e836719e1d1d03082acd3d2a556db.jpeg)

**"Nick Williams"**

---
---
**greg greene** *October 25, 2016 00:24*

I'm in !


---
**Jeff Kes** *October 25, 2016 00:35*

Me too!


---
**Timo Birnschein** *October 25, 2016 02:09*

the grbl 0.9j in my repository allows that (with mods by Nick Williams) and the official beta release of grbl 1.1d allows that. The beta was published 5 or so days ago and has a specific laser mode for engraving.

[https://github.com/gnea/grbl](https://github.com/gnea/grbl)

(THIS IS A BETA RELEASE by the original grbl developers!!!)

[github.com - gnea/grbl](https://github.com/gnea/grbl)


---
**Timo Birnschein** *October 25, 2016 02:10*

Oh that was dumb. I didn't see, Nick, it was your post!


---
**Nick Williams** *October 25, 2016 02:18*

**+Timo Birnschein** Again this is at 200 mm sec with no jitter. Can the new grbl go that fast?? The whole peace was 8 minutes. Also the laser is firing at full power.


---
**Timo Birnschein** *October 25, 2016 07:55*

**+Nick Williams** That is remarkable! I'm eager to try this as well! :)


---
**Nick Williams** *October 28, 2016 00:51*

Check This out 

Epilog Constant Velocity running at 1750 mm/sec


{% include youtubePlayer.html id="4x5A7YF5rrQ" %}
[https://www.youtube.com/watch?v=4x5A7YF5rrQ](https://www.youtube.com/watch?v=4x5A7YF5rrQ)


{% include youtubePlayer.html id="4x5A7YF5rrQ" %}
[youtube.com - Slate Charcuterie Board / Cheese Platter Engraving](https://www.youtube.com/watch?v=4x5A7YF5rrQ)


---
**Nick Williams** *October 31, 2016 20:29*

**+Timo Birnschein** Hey I just downloaded your K40 Laser board from Git Hub. I have an othermill a small CNC designed to cut circuit boards. It is not reading your board file double checked with my own files and they are read just fine. Any ideas?? 


---
**Timo Birnschein** *October 31, 2016 22:40*

**+Nick Williams**  hm... I'm using the free version of the latest eagle layout tool. So it's a regular legal version of the software. I don't know what might be wrong. :(

I know Othermill and as far as I remember from a year ago, the software was a bit buggy sometimes and only really usable on Mac. Did that change?


---
**Nick Williams** *October 31, 2016 22:44*

It's been pretty stable for me I am not sure what version of Eagle I have but I know its about 2 1/2 years old so not sure. Tonight I will try and open your board file on my mac and re-save that may make a difference??


---
*Imported from [Google+](https://plus.google.com/107520953345127905250/posts/V7y9L6JRn9r) &mdash; content and formatting may not be reliable*
