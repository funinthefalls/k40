---
layout: post
title: "Acrylic hinges- for those that work in acrylic a lot"
date: April 05, 2018 14:32
category: "Materials and settings"
author: "Anthony Bolgar"
---
Acrylic hinges- for those that work in acrylic a lot.



[https://www.banggood.com/search/acrylic-hinges.html](https://www.banggood.com/search/acrylic-hinges.html)







**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *April 05, 2018 15:51*

Had no idea there was such a thing! Thanks!

Also on amazon, but more $$$

[amazon.com - Amazon.com: acrylic hindges: Industrial & Scientific](https://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Delectronics&field-keywords=acrylic+hindges&rh=n:172282,k:acrylic+hindges&ajr=0)


---
**Anthony Bolgar** *April 05, 2018 16:00*

My local acrylic dealer turned me on to these. His local price is $1.00 CDN for a 50mm hinge




---
**Anthony Bolgar** *April 05, 2018 16:00*

Project idea, MRI friendly book stand


---
**Don Kleinschnitz Jr.** *April 05, 2018 17:03*

**+Anthony Bolgar** project idea .... totally Acylic MRI! lol


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/Z6bYc4pibt4) &mdash; content and formatting may not be reliable*
