---
layout: post
title: "Today's fun mystery: What is this thing that I found stuck on the back wall of my K40 while installing a drag chain"
date: November 16, 2016 19:22
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Today's fun mystery:



What is this thing that I found stuck on the back wall of my K40 while installing a drag chain.



Its a very strong magnet in a "keeper" shell.....



Could it be a magnetic alignment field for the Laser ...... 



lol always an adventure with a K40.





![images/dfedb91d128b002d0319d198146e49f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dfedb91d128b002d0319d198146e49f1.jpeg)
![images/3a0333b2f13d6c8a2c1399402fd44af1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3a0333b2f13d6c8a2c1399402fd44af1.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Ashley M. Kirchner [Norym]** *November 16, 2016 19:37*

Oh no, you removed the interpolating laser magnetron!


---
**Don Kleinschnitz Jr.** *November 16, 2016 21:17*

No wonder, that's why my dysan relator is overflowing into the binary encoded IOT server!


---
**greg greene** *November 16, 2016 22:14*

I suspect it was used to set up the rough alignment of  the mirrors by clipping on a visible laser and using the beam to set them.  Assuming then, that it is mounted at a point where what it held would have a path to do it !!! :)






---
**ThantiK** *November 17, 2016 01:08*

You have to clip on the double field refluctor in order to bypass the phase array scope.  If you fail to do this, terezoidal trap waves will invert the flux field inside the array and you'll get a sinusoidal collapse.


---
**Paul de Groot** *November 17, 2016 05:35*

Nah just a magnet to hold the paper instructions for the junior staff who assembled it. The poor bugger sprobably got scolded when management found out that 1 magnet was missing.


---
**Joey Fitzpatrick** *November 19, 2016 03:04*

That appears to be a baseplate of Prefabulated Ammulite.


---
**George Fetters** *November 29, 2016 16:05*

We use these magnets as hold downs for the work piece.  We have a metal bed and put these under the work piece until it is the right height then one on top that holds the piece in place.  Not sure why there is a hold in it though.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/7wkds2todFn) &mdash; content and formatting may not be reliable*
