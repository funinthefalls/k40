---
layout: post
title: "How much air assist are you using"
date: February 11, 2016 16:18
category: "Discussion"
author: "Thor Johnson"
---
How much air assist are you using





**"Thor Johnson"**

---
---
**Stephane Buisson** *February 11, 2016 16:34*

So many units for pressure (metric&imperial) make your pool hard to answer.

Also it's depending also on your head output and target.

If your goal is just to get rid of smoke on the lens, or if you want to push more oxygen to boost combustion, your need in air will be different.


---
**Imko Beckhoven van** *February 11, 2016 17:20*

Dont use a standard setting, best practiche in my opinion. The least amount of air to het rid of the flame and smoke.  So verry dependable on what youre doing


---
**Scott Marshall** *February 11, 2016 20:21*

Shop Air (5hp commercial Compressor w/ dryer in another building so I don't have to listen to it) through a small regulator/filter through a paint spray filter, needle valve sets flow rate. Solenoid valve turns on thru a transistor watching the power meter and there's an 30 sec Off delay to prevent short cycling. This is provided using a diode and capacitor (with discharge resistor/Pot sized for time) on the transistor base to maintain he latch.

I'm planning on using this signal to control the exhaust blower as well. It will activate upon 1st firing of the laser, and shut down about 30 sec after job completion. Manual override switch. (On-Off-Auto)



Actually working on it today, along with a coolant flowmeter (got a nice paddle wheeler), radiator and temp monitors. Snowing and blowing here in Elbridge NY. Roads and schools are closing. This'll keep me busy  while it blizzards

. 

I'll post drawings when it's done.  It's not complicated or expensive.


---
**I Laser** *February 11, 2016 23:11*

As others have pointed out it depends entirely on what you're trying to achieve. I have a compressor from a industrial scanner, it has more than enough grunt though its a tad noisy.



Loving the automation there Scott, I can only dream of such a setup. Currently limited to plugging stuff I want on at the same time (ie water, air assist, etc) onto the same power board lmao!


---
**HalfNormal** *February 12, 2016 12:49*

I am using a pancake compressor I bought at Harbor Freight for $49.99 and an eleven gallon tank bought for $30. Same price of an airbrush compressor but more versatile. Pressure regulator and valve control at the laser end. Can get just a whisper of air or more if cutting acrylic. Much quieter and cycles less than my 33 gallon compressor.


---
**Scott Marshall** *February 12, 2016 13:46*

**+I Laser** When it's completed (which shouldn't be too much longer) I'll send you the BOM and drawings. It's all ebay and hardware stuff. I'll bet the whole lot of parts cost less than $50USD. It's not complicated, just sounds it. 



I'm currently doing the plug in/turn on deal and am tired of forgetting stuff. it's only a matter of time before I ruin something or laser my hand if I keep this up, so I'm doing it out of self-defense. Industrial controls is what I used to do. I find it amazing they are getting away with selling these in the US.


---
**I Laser** *February 12, 2016 23:47*

**+Scott Marshall** look forward to that!


---
*Imported from [Google+](https://plus.google.com/100850237464188460837/posts/VmGPAWrhc5T) &mdash; content and formatting may not be reliable*
