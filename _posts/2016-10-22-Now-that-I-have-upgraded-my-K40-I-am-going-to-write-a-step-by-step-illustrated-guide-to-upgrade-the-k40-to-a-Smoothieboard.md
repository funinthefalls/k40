---
layout: post
title: "Now that I have upgraded my K40 I am going to write a step by step illustrated guide to upgrade the k40 to a Smoothieboard"
date: October 22, 2016 17:45
category: "Discussion"
author: "Anthony Bolgar"
---
Now that I have upgraded my K40 I am going to write a step by step illustrated guide to upgrade the k40 to a Smoothieboard. I just need to button up a couple of things on my machine, and then I can start writing it. Hopefully it will explain a few of the most confusing parts of the process, as well as making it easier for the average tinkerer to accomplish.





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *October 22, 2016 18:51*

**+Anthony Bolgar** I am on the same track and I don't think two are needed so let me know if you want to collaborate or I can provide some content.


---
**Anthony Bolgar** *October 22, 2016 18:57*

Sounds good, I have 1/2 of the first draft done. I will send it to you in a day or so, just want to finish the rough draft.


---
**Don Kleinschnitz Jr.** *October 22, 2016 19:23*

**+Anthony Bolgar** I recently had a notion to explain the K40 conversion by making graphical "One-page Hacks" for each major areas of the conversion. I.E. PWM, end stops, steppers, DC power, AC power, smoothie power, display, accessory control, control panel, middleman, digital pot setting, packaging etc.



An example is here: [https://plus.google.com/u/0/113684285877323403487/posts/99hJJ1Vw32P](https://plus.google.com/u/0/113684285877323403487/posts/99hJJ1Vw32P)

[plus.google.com - Continuing to work on sharing what I am learning about laser control and…](https://plus.google.com/u/0/113684285877323403487/posts/99hJJ1Vw32P)


---
**Jeffrey Rodriguez** *October 23, 2016 16:13*

Sweet !! I have Board and everything awaiting step by step


---
**Marcus Hawkins** *October 23, 2016 18:53*

Awesome, I will be buying the smoothie board tomorrow, so this guide would help me out a lot.  Thanks.




---
**Andy Shilling** *October 24, 2016 09:18*

Sounds good hopefully it will be the help / push I need. I'm new to all this and running a moshi board so getting something like this will really help decide if I can warrant upgrading the board.


---
**Bob Damato** *October 24, 2016 12:13*

This is great. Ive wanted to do the smoothieboard upgrade for a while now but have never found good idiot-proof directions! (me, being the idiot)




---
**Andrew Brincat** *October 25, 2016 09:15*

Would love to see this  guide when done please!! great idea thank you


---
**Marcus Hawkins** *November 17, 2016 12:50*

Hey, just wondering if you managed to make the guide yet?




---
**Don Kleinschnitz Jr.** *November 17, 2016 14:20*

**+Marcus Hawkins** in the mean time this may help.

[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Anthony Bolgar** *November 17, 2016 19:11*

Working on it ....hopefully another day or 2


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/P5j2GX7hzeX) &mdash; content and formatting may not be reliable*
