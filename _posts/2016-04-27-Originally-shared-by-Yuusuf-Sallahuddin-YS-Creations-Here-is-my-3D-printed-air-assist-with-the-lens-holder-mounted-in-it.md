---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) Here is my 3D printed air assist, with the lens holder mounted in it"
date: April 27, 2016 04:23
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Here is my 3D printed air assist, with the lens holder mounted in it. As you can see, the lens holder is a perfect fit (very tight also), so the 4 bolt mounts that I added were unnecessary. Also, if you look at the photos, on the base you can see that 1 of the 18 outlet nozzles snapped off. And on the side, the inlet nozzle snapped off. This happened with almost 0 pressure applied to it (i.e. I just bumped it). Poor design on my part, as I wasn't aware of the weakness of the join on such small join areas.



Revising the design now to cater for these weaknesses. Also, have decided that rather than having the inlet nozzle just attached to the main ring, I will have it as an insert than can be removed or swapped with a different one (for different diameter air hose).



![images/e6b5ce4c016636752b37a52ced4d2ec6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e6b5ce4c016636752b37a52ced4d2ec6.jpeg)
![images/5cad15634407aa811a7b62d2eeeb38d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5cad15634407aa811a7b62d2eeeb38d2.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Phillip Conroy** *April 27, 2016 05:08*

Your design has a few advantages from stock air assist nozzels ,1 air dos not come in contact with lens ,so mositure or oil on focal lens and 2 you do not have to have worry aout the laser beam hitting side of air assist-so no aliging laser [head.it](http://head.it) may have 1 disadvantage of maybe moving work peace [stock air assist has the air going straight down


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 05:44*

**+Phillip Conroy** Thanks Phillip. I think it is possible that it may move the work piece, however as all 18 beams of air that exit are at the same angle & hit the same position, shouldn't the air pressure in each direction be equal (theoretically at least) and cancel each other out?


---
**Phillip Conroy** *April 27, 2016 06:16*

You may be right ,proberly overthinking things ,pressure may  be diffrent on oppeset side from air inlet,


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 06:50*

**+Phillip Conroy** Will never really know until I test it. Unfortunately cannot test this one since I snapped it's inlet. However, I am considering having 2 inlets, directly opposite each other. To even the pressure from both sides. Or, splitting the single inlet down the centre (so it funnels half the air right & half left, equalising the pressure on both sides).


---
**Phillip Conroy** *April 27, 2016 08:33*

when you get this working and if it does not move the work peace i would be interseted in buying one ,ps nice fingerprint on focal lens,are you using stock 12mm focal lens?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 08:41*

**+Phillip Conroy** Ah yeah the fingerprint is just because I was seeing if the lens mount fits in the internal hole. Wasn't being careful as my machine is not in use for a bit (until I get this sorted). I've made a new model for it & tagged you. I am still using the stock 12mm lens 50.8mm focal length. The air beams align to hit that 50.8mm position. I will have to let you know after I get the print done again. The guy I used prints in Resin & the cost for the previous one was $20 setup + $10.51 materials. For extra prints of the same object, it is $10 + $10.51 materials. So will be $20.51 (for the previous one). May be slightly different for the next one as the design and dimensions have changed.


---
**Phillip Conroy** *April 27, 2016 09:00*

Still cheaper than what i have spent to prevent water and compressor oils hitting focal lens- you should sell them for at least $40 AU 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 09:43*

**+Phillip Conroy** Can't hurt to try sell them for $40 if they work as desired. We'll have to wait & see.


---
**Don Kleinschnitz Jr.** *April 27, 2016 13:28*

I wonder if those fragile nozzles are really necessary? An open hole may work just as well. I also wonder if there is a way to make a head like this just cutting and drilling acrylic with the laser (that would help those with no 3D printer). 

I imagine a multi-layer structure with  a ring of holes in the bottom plate, a spacer plate that creates an air space and a top plate. The air would not exit at an angle but a straight curtain of air may work just as well. the air exiting the holes will diverge anyway. MM just pontificationg...


---
**David Cook** *April 27, 2016 13:51*

You want to add fillets to the base of each air port. This will add a lot of strength to those at the base. 


---
**Scott Marshall** *April 27, 2016 14:40*

You could just fuse the nozzles into an annular ring? 

Also print undersized holes and ream to size for smoother exit?

You can probably save the 1st one by pressing a piece of tubing in place or using a 1/16" pipe tap (but most people don't have one)



That should work great for engraving ply wood etc, maybe eliminate the need to tape (and remove ugh - the tape)?



Scott

(Gotta build one of them newfangled gadgets  - very nice piece, it would take me 2 days (at least) to make one on mill and lathe)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 15:24*

**+Don Kleinschnitz** Interesting thought to make with the laser. Unfortunately, the air not focusing on the sweet spot defeats the purpose for what I was going for, but it could be a makeshift option for those without 3D printer as you said (although I don't own a 3D printer).



**+David Cook** I'm not certain what fillets are, but I've modified the design totally to have more conical nozzles for all the outlets, to give extra strength. I have never done a 3D print before this one, so I was unaware of limitations/strength issues. If you check my profile there will be a recent post with the modified design.



**+Scott Marshall** I did consider fusing all the nozzles into a ring whilst doing my newest version of the design (I started numbering them so I can keep track of it... v1.02 now). I think the first one is savable, I will do something to bodge it up to be functional, but will get the new version printed once I nut out the design. I'm working with basic ideas as I have zero engineering/etc knowledge. Input from all of you who have this knowledge is much appreciated.



Also, what newfangled gadgets are you referring to?


---
**Don Kleinschnitz Jr.** *April 27, 2016 16:26*

**+Yuusuf Sallahuddin** I am not convinced that by the time this air gets to the surface it will matter ... yet :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 16:40*

**+Don Kleinschnitz** No-one really knows as yet, until I can do some tests of it. But I can tell you for a fact that the air does focus exactly into 1 point (designed for 50.8mm away from lens, however no way to really check if it is hitting this spot). But I do notice that it focuses in a V shape, then after that point where it combines together, it seems to defocus into an upside down V shape.



Like this:

\/

/\



Not sure if it will make much difference to cutting/engraving, but I dislike the heads that are currently purchasable. My main aim was to create something low-profile, with a cone of air to protect the lens from smoke. Secondary to that is the heat reduction at the point of laser impact & dispersal of smoke in all directions instead of just 1 (the back of the machine) as it leaves stains on leather when I cut it. We'll have to see if this actually performs what I was aiming to do (when I get a new one printed with the revised design I have done).


---
**Don Kleinschnitz Jr.** *April 27, 2016 20:46*

Don't get me wrong I like your idea... I am likely to make a flat acrylic one like I suggested and then just drill the holes at an angle ....:) I am also wondering about integrating a laser pointer with leds positioned the same way ....


---
**Don Kleinschnitz Jr.** *April 27, 2016 20:47*

another embodiment is to integrate a micro blower .... eliminate the whole pump thing....


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2016 01:20*

**+Don Kleinschnitz** On one of my other posts about this (on my main profile page) **+Anthony Bolgar** mentions an STL file he has for a conical air-assist head that has integrated laser pointer mounts also. You could check with him for the file/thingiverse link for it as it might give you some ideas for your acrylic cut one. Drilling the holes at angles is workable if you are capable of doing so. My concern with me doing that is that I for certain cannot get close to the same angle for all 18 holes.


---
**Scott Marshall** *April 28, 2016 16:29*

If you have access to a drill press, just shim one side of a plate up (fire up your High School Trig for how much), or tilt the table accordingly (if it's capable)and turn, drill, turn, drill, etc 18 times. 



There's a gadget machinists use that's an expensive version of this called a Sine Plate, actually, this is a cheap version of the Sine Plate....



It was just a thought in any case, I'd bet it works fine as printed, but I'm a machinist at heart, and you know the saying, "When your tool is a hammer, all the world looks like a nail....."



Wish it didn't cost so much to send me one, I'd buy one from you if you were a few thousand miles closer..



I may make an aluminum one some rainy night when I feel like making chips. Especially if you're kind enough to send me dimensions....



Just got an idea. Could you integrate a cone type single nozzle and a 2nd inlet to supply it?. Then you would have a software (or manual valves) select air assist system. Outer ring, Center, both. (Engrave, cut, super) The cone would also give protection to the bare lens. Seems to me a marketable device.



Again, just thinking out loud...

Sure wish I had time to try all these great ideas. I can get into trouble myself, now I have ideas pouring in from here.



Scott. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2016 17:56*

**+Scott Marshall** I do have a drill press & I get what you are saying about shimming up 1 side of the plate. My drill press is an extremely old one that belonged to my grandfather & the main platform is actually rotatable I think. Or maybe I stuffed something up when I did that haha.



The angle is very precise, I already know it because I had to get it in order to angle the nozzles in the first place. 17.25 degrees for this one. It has changed in the new revision (because the nozzles are slightly further away from the centre axis) to 18.4998 degrees (may as well be 18.5, but I thought that 0.0002 may make a difference lol).



You could actually get it 3D printed from a more local printer using 3D Hubs website. That's where I got it printed (as I don't have a 3D printer... yet). That would work out cheaper for you.



That's an interesting idea to integrate a conical nozzle as an extra. Could be done I suppose, can't see why not. Might have to have a bit more discussion about that idea with you & we can draw up some designs.



I've just done a test of the one that I had printed (even though I had snapped bits off). I managed to dodgy up a solution to use it, but I found that my airpump (aquarium one) is too weak to push much air pressure through all 18 nozzles. This is an issue I didn't consider to begin with. So I snapped off 10 of the nozzles & covered the holes with electrical tape. Using 8 nozzles with my low air pressure seemed to work okay. I've posted a youtube video that I recorded (with my LaserCam mod) of the air assist in action whilst testing some vector engrave stuff at the same time on 3 different materials (MDF, leather, ply). So 3 tests in 1 lol.



I'll share the vid to K40 group once I add some photos of the end results.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 03:00*

An update, I've clarified with the guy who prints it and the pricing actually works out that it is the same cost for subsequent prints on different days. The discounted rate applies for objects printed at the same time (literally multiple of the objects on the printing platform). So if anyone is interested (after I get the new version printed/tests done), I would aim to print multiple of them at the same time to reduce costs per piece. Although, I will share the STL files once completed & you could just arrange to get printed by a printer that is more local (or print your own if you have a 3D printer).


---
**Scott Marshall** *April 29, 2016 03:42*

**+Yuusuf Sallahuddin**

If you have any idea what shipping might cost to US, I am, and I'd think they are probably others interested on this side of the water.



Don't discount "grandpas" old tools, probably better than what you can by today short of commercial grade stuff. I'll bet it's all cast iron, and rigid as you know what. If you add the center jet, I think  the others will be less critical as to direction.



Your setup will use more air (maybe a twin aquarium pump?), but the advantage I expect it to provide in an outward 'wash' in all directions, keeping smoke staining from streaking toward the main exhaust point this is my big problem cutting and engraving plywood, and think the center jet would clear the kerf, and the 18 will wash away the smoke before it sticks, hence NO taping, untaping.



The newfangled gadget was the printer.



I've got a couple of posts that would be good to sticky, if you get a chance, drop me a line on how-to. or at least how to send to you so you can do it. Haven't had time to have a look at the links you sent yet. Got a ton of stuff going her, and having a hard time getting any of it done.

I'll get it, eventually.



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 05:02*

**+Scott Marshall** 

Regarding shipping, I'd have to verify the final weight, but you'd probably be looking at say 200-250g (although could be heavier, just feels like it weighs nothing). Which using auspost calculator, suggests au$15.85 for economy air postage (eta 6 business days). Anywhere up to au$82 (express courier). Standard (eta 5 business days) with tracking would be au$23.72 for up to 500g. So could probably send a couple at once using that method & whoever is on that side of the pond can redistribute locally (probably a lot cheaper that way).



Definitely not discounting grandad's old tools. They are 1000000% better than anything you can buy today. There is one old hand-drill, weighs about 5kg, body is made of solid metal. Extremely good tool. Probably older than me (i.e. > 32 years). Most of what you can buy today is plastic junk that will break in a few years time. Not built to last for sure.



Actually, if you check the video that I posted on youtube (there is a link on my page or on K40 group in Modifications category) you will see that it does exactly that, dispersing the smoke both directions (left & right) that the camera can see. I assume, but may be wrong, that it is also dispersing front & back too. It does seem to minimise the staining in 1 direction (to the back of machine) & spread out that staining on all sides. So it went from what used to be about 5-10mm of stain at the back, to about 1mm concentric staining around the cut. With higher pressure (more than my weak aquarium pump at least) it would probably clear all the smoke a lot better (as you suggested) & a conical nozzle would be great to focus directly downwards to hit the cut area & cool it, disperse the smoke under the piece too. I think the conical would be great for cutting, whilst the conical is nearly pointless for engraving (as the smoke can't pass through because it isn't being cut all the way through). Thus the ring would work better on engraves as you previously mentioned. Awesome idea & we should definitely nut out some ideas for a design using those principles. Next idea would be to have the outlet nozzles somehow adjustable angle to cater for the different focal length lenses. Lol... more work.



I haven't had much time to work on anything much as regards to the forum stuff yet, too many other things going on & me getting side-tracked (as usual). I've decided that in order to work on it, I need to schedule a specific day that has some hours to work solely on it (else I will do what I always do & move onto the newest idea in my head). In regards to stickying it, I don't think a "standard" user is able to set things to be sticky/pinned to top. But if you post it & put something like "pls sticky" into the title, I assume an admin (HalfNormal or myself) can set it to sticky/pinned. May be wrong, but I could sort it out afterwards. So anything you want stickied, just add something like that to the title & an admin can review it & sort it out.



Yuusuf.


---
**Don Kleinschnitz Jr.** *April 29, 2016 22:05*

Just thinking, at an angle these nozzles create a focus point. Does that mean that the actual pattern at the surface is different with different lenses set at different heights? The LO approach does not have that problem?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 00:04*

**+Don Kleinschnitz** Yes it does mean that with different lenses (i.e. with different focal points) & different distances away from the material the focal point of the beams of air would be off. If the LO approach you are referring to is the conical air assist head, then you would be correct in that. I have a few reasons why I don't like their approach, primary being the size of the LO cone. The one I designed is partially for low-height-profile, as I sometimes work with uneven materials (e.g. shape formed leather). If I had an adjustable z-axis or adjustable lens height it could relieve the issues associated with that. But this is a solution for me for the meantime.


---
**Don Kleinschnitz Jr.** *April 30, 2016 11:55*

Interesting, are you saying that the LO cone is longer than your shortest focal length? What lens are you using. One for sure advantage of your approach is that it will be easier to align as the cone demands that the beam be very perpendicular to the light path, then again that may be a good thing. The onlyway I could get it aligned exactly in the center of the cones output was to make an alignment fixture.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 12:19*

**+Don Kleinschnitz** I don't know for certain as I don't have the LO Cone, but I just don't like the way it protrudes down below the lens. I am still using the stock lens & mounts. Sometimes pieces I work on are variable in height of the material (i.e. some parts sitting at 10-20mm higher than the part I want to cut; I don't cut/engrave on the higher areas but I need to be able to clear them without hitting them). That is another point of the LO head that concerns me, getting it spot on with alignment. Probably not the biggest issue as I have mine aligned fairly close to spot on. You are correct that it is probably a good thing that LO head forces you to do this. Why have it out of alignment if you can align it precisely (giving better cutting/engraving results). Personally I can't warrant spending the price on the LO air-assist head as shipping to Australia is always ridiculous (although LO may not be too bad, I haven't checked) & the height issue that I've mentioned. I figured somewhere local can 3D print my design for cheaper & I can pick it up in person, making it overall cheaper for me & more suitable for clearance of odd height objects that I tend to work on. I do however believe that the LO head is probably much more suitable for certain purposes (e.g. if you are working with more level height materials), however another member here has had issues with moisture build-up on his lens (requiring him to add extra water traps on his air compressor) due to the air coming into direct contact with the lens. All in all, depends how much money you want to spend, what your purposes are, etc. I'm definitely not of the opinion that the LO head is bad, since a lot of people who have it seem to be producing great results, just it is unsuitable for majority of my cutting/engraving tasks.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/e7i3DmhKCL9) &mdash; content and formatting may not be reliable*
