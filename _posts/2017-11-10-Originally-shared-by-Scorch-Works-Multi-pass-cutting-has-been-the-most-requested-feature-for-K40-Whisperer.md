---
layout: post
title: "Originally shared by Scorch Works Multi-pass cutting has been the most requested feature for K40 Whisperer"
date: November 10, 2017 01:19
category: "Software"
author: "Scorch Works"
---
<b>Originally shared by Scorch Works</b>



Multi-pass cutting has been the most requested feature for K40 Whisperer.  K40 Whisperer version  0.12 includes multi-pass cutting and a few other new features it is now available for download:

[http://www.scorchworks.com/K40whisperer/k40whisperer.html](http://www.scorchworks.com/K40whisperer/k40whisperer.html)



![images/a91ebfdd39118c372bf54ecb4a15e711.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/a91ebfdd39118c372bf54ecb4a15e711.gif)



**"Scorch Works"**

---
---
**Paul de Groot** *November 10, 2017 05:52*

Would be really good to get an idea of how many k40s are out in the wild. Do you keep a,log of number of downloads?


---
**BEN 3D** *November 10, 2017 06:03*

cool


---
**Scorch Works** *November 10, 2017 11:13*

**+Paul de Groot** No, I do not.


---
**BEN 3D** *November 10, 2017 16:01*

Hi **+Scorch Works**, could I use Scorch Works on a Windows 8 OS?

In the Documentations no  Zagig driver for that OS.


---
**Scorch Works** *November 10, 2017 16:08*

**+BEN 3D** Yes,  use the one labeled 

"Zadig (Windows Vista,7,8,10)"


---
**BEN 3D** *November 10, 2017 16:23*

ahh 


---
**Printin Addiction** *November 10, 2017 23:35*

**+Paul de Groot** I don't know what I am going to do when I get my Gerbil, I'm very much used to K40 Whisperer :)


---
**Paul de Groot** *November 11, 2017 02:15*

**+Printin Addiction** that’s wonderful. Indeed amazing approach to rework around the existing solution and being able to work out a closed protocol from observing. Next step for me is to design a total replacement for the k40. The amount of steel and wasted space plus the laser tube lifespan of only 1000hr is not a great investment. Gerbil is not only about hardware, Check out my toolchain for the k40 within Inkscape. It’s a time saver and takes the complexity out of your work. I’m time poor so efficent and fast tool chains are a life saver.


---
**Anders Sandström** *November 12, 2017 11:45*

Any plans on putting source on Github?

I know you publish source along with releases, but it is just so much simpler contributing if there is a official repository


---
**Scorch Works** *November 12, 2017 14:29*

**+Anders Sandström** No, I do not.


---
**Timothy Rothman** *December 20, 2017 05:49*

**+Printin Addiction**  I too am very accustomed to K40 Whisperer.  It looks like Paul has shipped my board via Aussie Post a couple of days ago and it's well on its way.


---
*Imported from [Google+](https://plus.google.com/+ScorchWorks/posts/7cLoHWtYp1A) &mdash; content and formatting may not be reliable*
