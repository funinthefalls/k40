---
layout: post
title: "Update to how to export your G+ Site and what to do with the data"
date: January 08, 2019 00:36
category: "Discussion"
author: "HalfNormal"
---
Update to how to export your G+ Site and what to do with the data.



The newest version of Google+ Exporter is now available with blogger and WordPress export options.



Here is what it looks like on blogger which is also a Google (Alphabet) property.



[https://everythinglasersdotcom.blogspot.com/](https://everythinglasersdotcom.blogspot.com/)



Here it is on WordPress



[https://everythinglasersdotcom.wordpress.com/page/1/](https://everythinglasersdotcom.wordpress.com/page/1/)



Examples of what other exports look like;



[https://blog.friendsplus.me/export-google-plus-feeds-45926c925891](https://blog.friendsplus.me/export-google-plus-feeds-45926c925891)



You can find the latest version of Google+ Exporter here;



[https://gplus-exporter.friendsplus.me/](https://gplus-exporter.friendsplus.me/)





**"HalfNormal"**

---
---
**Jim Hatch** *January 08, 2019 01:23*

Did I miss an announcement of this site's move? Is someone moving it over to Blogger (or a WP site)? I have tons of links for stuff folks have done here and keep an eye on what Don's doing even though I've retired my K40.


---
**HalfNormal** *January 08, 2019 01:31*

G+ is closing down in April and these are ways to backup your data.


---
**Jim Hatch** *January 08, 2019 02:13*

I was hoping Stephanie or someone might be moving it altogether. I know there was some talk of that early on right after the announcement but haven't really seen much since. It seems a shame to let it disappear. I've learned a lot here over the past 5 years or so.




---
**Jim Hatch** *January 08, 2019 02:20*

Looks like you can export your posts but not all of the posts - the community owner needs to be the one who does that. Otherwise everyone else's posts don't come down. We need Stephanie to do it (I think she's the owner still).


---
**Stephane Buisson** *January 08, 2019 13:24*

No hurry, Google still offer the best exposure for the next 3 months and we need to be ready by April.

but it's not only about our community, I still think a larger Maker movement including a k40 group is the best way to go. using the same platform and be able to go from one communities to an others with the same indentifier/pwd would be the best.

(3D print, CNC, K40, OX, Smoothieware, Softwares, ...)



I would give a special thank **+HalfNormal**, for his posts. if the previous option doesn't materialised, I would be happy to try this solution and maybe hand over the communities keys. (it's also possible that G+ stay online, not able to post anymore, but all the knowledges and links would be still be alive).


---
**Don Kleinschnitz Jr.** *January 13, 2019 18:13*

**+HalfNormal** I tried exporting 

- <b>G+ Profile</b> ; and it looks like it gave me all my posts on all communities.



-- <b>K40</b> Laser Cutting Engraving Machine; this seemed to export everything on the community  ??  Then I ran out of free ..... not sure I want to pay $20 yet.



Any Idea of how I just get my posts for the K40 community???Am I downloading everything cause I am a moderator??



Maybe downloading by the moderator is a way to save everything?



Moderator downloads and then creates a mirror blogger site?


---
**Stephane Buisson** *January 13, 2019 18:37*

**+Don Kleinschnitz Jr.** I also did try, too big for free (usd 19.99), then as far I understand it will be hosting fee for the wordpress site. Well better to wait near this limit date to get all. I will keep it as a backup, if G+ goes dark (but I rather think Google will lock out new entry and keep the all thing online). then we will reconsider hosting need.



alternatively what about Reddit ? already a lot of lasercuting there.


---
**Don Kleinschnitz Jr.** *January 13, 2019 21:31*

**+Stephane Buisson** blogger is free. 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/TePaNYs1adn) &mdash; content and formatting may not be reliable*
