---
layout: post
title: "New control panel for my Redsail LE400"
date: January 04, 2018 05:37
category: "Object produced with laser"
author: "Anthony Bolgar"
---
New control panel for my Redsail LE400. Made from 3mm clear acrylic that has been back painted, engraved, and colour filled. I think it turned out pretty good. Just need to printy a knob for the power level control. The guage is a digital mA meter for 0-50mA Takes up a lot less realestate on the panel than an analog one. I might remake it to eliminate the holes for the GLCD buzzer and reset button for a cleaner look.

![images/5acc686a24f07ebeec618d8ca5b45890.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5acc686a24f07ebeec618d8ca5b45890.jpeg)



**"Anthony Bolgar"**

---
---
**Joe Alexander** *January 04, 2018 06:17*

looks great, and you barely notice those 2 lil holes. I'd say keep it 'till it doesn't look that good :)


---
**Anthony Bolgar** *January 04, 2018 10:13*

What I like is that from the front you can not damage the paint, all you can do is maybe scratch the acrylic a bit.




---
**Don Kleinschnitz Jr.** *January 04, 2018 12:05*

Super nice work.

I wonder if you will miss the high frequency fluctuations of the meter pointer using digital?



Is your cooling controlled from that switch? what if its left off ?


---
**HalfNormal** *January 04, 2018 12:31*

Wonderful job! Very professional looking.


---
**Anthony Bolgar** *January 04, 2018 13:58*

If the cooling switch is left off then the laser will not fire as a flow meter is on the WP circuit, along with door interlocks for the work area, tube bay, and electronics bay (which also has a keyed lock keeping the electronics locked up.) for the digital mA meter, I am only concerned about getting an average current, don't need it to be super accurate, as I am of the mindset that the tube is a consumable, I'll replace it when it dies, I would rather be able to cut or engrave at the fastest rate possible, I build tube replacement into my pricing structure. But if the mA meter proves not suitable, I'll make another panel with an analog meter.


---
**The Best Stooge** *January 12, 2018 03:45*

I never could get with that mindset that a laser tube is to be treated like a REALLY expensive ink jet cartridge.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/FkSVCwj5N1V) &mdash; content and formatting may not be reliable*
