---
layout: post
title: "Help. I don't like to ask newbie questions but that is what I am"
date: March 24, 2017 14:24
category: "Discussion"
author: "Martin Dillon"
---
Help.  I don't like to ask newbie questions but that is what I am. 

I joined the group about 1 month ago and received my k40 Monday.  I want to make sure I do everything right and not make any stupid mistakes.  Right now, I am looking at setting up the cooling and safety lockouts.  I looked through the post topics but they are not sorted very well.  I have been looking through [http://donsthings.blogspot.com/](http://donsthings.blogspot.com/) and there is a lot of useful stuff there but his interlock diagram didn't make since to me and was low resolution and hard to read. I read through the post on what type of water to use and plan on using distilled water and algaecide.  I am having trouble finding the right flow meter/switch to use for my safety circuit.  

Are all the switches just put into series, if so, how much current flows through the circuit?  I also was not able to find where it hooks to the control board.



Feel free to add any advice you think I might need.  I have read alot about laser alignment and downloaded some of the recommended guide and I don't think I will have problems with that.





**"Martin Dillon"**

---
---
**Martin Dillon** *March 24, 2017 22:51*

**+Don Kleinschnitz**  Mark brown is correct.  I was looking  to tag my post and didn't realize it pops up after you click post.

Your blog is great but I still can't find a link where to buy the flow meter.  When I search online all I can find are large industrial flow meters or automotive ones.  I still can't make sense of the circuit diagram.  the picture is very pixelated and I am not interested a temperature sensor.  Are there any other diagrams out there?


---
**Don Kleinschnitz Jr.** *March 25, 2017 00:57*

**+Martin Dillon** 



Essentially all these devices are switches that are wired in series with your P+ & gnd if you have that kind of LPS.



very strange I can see all the pics clearly and the links to the detailed schematic and parts are in the posts:



On post; [donsthings.blogspot.com - Laser Tube: Protecting, Operating, Cooling & Repairing](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)



Under "Flow sensor" there is a link to :

[https://www.amazon.com/gp/product/B00AKVEGTU/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00AKVEGTU/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)



....in that same section there is a link to "Build Schematics": [http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/](http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)



The interlock switches are here: [http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html](http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html)



...under "Interlock Switches"

[https://www.amazon.com/gp/product/B01CS82B8K/ref=crt_ewc_title_srh_2?ie=UTF8&psc=1&smid=A11A70Q280RHPK](https://www.amazon.com/gp/product/B01CS82B8K/ref=crt_ewc_title_srh_2?ie=UTF8&psc=1&smid=A11A70Q280RHPK)



Reply back if this is still troublesome...



Alternately post a pict of your LPS and I will custom draw you a diagram.


---
**Martin Dillon** *March 25, 2017 01:13*

I ordered a white flow sensor and I think the wiring should be easy.  I was thinking of using magnetic reed switches instead of micro switches because that is what I have left over from another project.  Do you think there would be any magnetic interference from the power supply or laser?


---
**Don Kleinschnitz Jr.** *March 25, 2017 01:19*

**+Martin Dillon** I dont think you will have magnetic problems, especially since you will likely mount them outside of the electronics box. But you can hook one up to a battery, resistor and LED and put it in the box while operating just to be sure.

You want to make sure that whatever switch you use does not have high resistance contacts.

If you are getting the same water sensor that I did, note in the post that I had to remove the guts and adjust it and put it on the output side facing downward.


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/CDyV995etYU) &mdash; content and formatting may not be reliable*
