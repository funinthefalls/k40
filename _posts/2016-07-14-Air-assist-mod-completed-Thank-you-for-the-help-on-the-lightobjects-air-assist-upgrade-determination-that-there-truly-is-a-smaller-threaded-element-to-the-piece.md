---
layout: post
title: "Air assist mod completed. Thank you for the help on the lightobjects air assist upgrade determination that there truly is a smaller threaded element to the piece"
date: July 14, 2016 23:17
category: "Modification"
author: "Jeremy Hill"
---
Air assist mod completed.  Thank you for the help on the lightobjects air assist upgrade determination that there truly is a smaller threaded element to the piece. 



![images/096e6a2c60b56cacd7877ad93bf55f4d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/096e6a2c60b56cacd7877ad93bf55f4d.jpeg)
![images/3f45e4b7b64637b1bf3f0d0140936bb3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f45e4b7b64637b1bf3f0d0140936bb3.jpeg)
![images/c89b4351eff2a89006ff6e899809d344.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c89b4351eff2a89006ff6e899809d344.jpeg)

**"Jeremy Hill"**

---
---
**Jonathan Lussier** *July 14, 2016 23:59*

I already have an air compressor (oil-less).  Is there a tutorial or part list I can follow?


---
**Jeremy Hill** *July 15, 2016 00:08*

My biggest challenge was to determine the LO air assist piece does fit the stock mount by breaking it down more than I had already done.  The piece was pretty tight but it does separate into four pieces.  Once I had that determined, I bought a recoil air spray hose ([https://www.amazon.com/Aerograph-Recoil-Airbrush-Deluxe-Compressor/dp/B01B8L33Z8/ref=sr_1_2?s=arts-crafts&ie=UTF8&qid=1468541131&sr=1-2&keywords=air+brush+hose+recoil](https://www.amazon.com/Aerograph-Recoil-Airbrush-Deluxe-Compressor/dp/B01B8L33Z8/ref=sr_1_2?s=arts-crafts&ie=UTF8&qid=1468541131&sr=1-2&keywords=air+brush+hose+recoil)) similar to this one.  I also went to Ace Hardware and bought a 3/8" male to male connector.  The only additional piece was the zip ties and two holes I had to cut.  I cut one on the lip near the lid and one between the laser space and the electronics space.  It does appear that I have a bit too much coiled hose so I may start to cut that back a bit to reduce how much hose is dragging around in the laser area


---
**Ned Hill** *July 15, 2016 01:37*

If you get a small personal electric fan to blow on the airbrush compressor it will keep it from getting too hot and will prolong the life of the pump.


---
**Jeremy Hill** *July 15, 2016 01:49*

Good idea.  Thanks.  I have a small fan that will work for this.


---
**Phillip Conroy** *July 15, 2016 05:07*

Also make sure u have a water flow switch fitted,i blow my tube and power supply not long after i first got my laser cutter[and the biggest bummer was i had a flow switch sitting on my desknfor 2 weeks before i didnt check the water pump was on and blow stuff -$350 latter and 1 month lost live and learn]


---
**Hilco Smit** *July 15, 2016 17:36*

my power supply died,how can i find out if my laser

is still good


---
**Ned Hill** *July 15, 2016 18:43*

**+Hilco Smit** It would be best if you created a new post and describe exactly what happened.  You will get a wider  member response.


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/GRyxeGBoKT7) &mdash; content and formatting may not be reliable*
