---
layout: post
title: "I have been having an issue with my laser"
date: October 01, 2016 15:35
category: "Hardware and Laser settings"
author: "timb12957"
---
I have been having an issue with my laser. We have pretty much exhausted solutions in another thread. My machine is under warranty, and the seller has instructed me to try to find someone local to repair the machine and they will pay for the repair. My question now is how do I find a "local Chinese laser repairman?". Or what type of individual do I look for with knowledge of working on this type of machine? 





**"timb12957"**

---
---
**Don Kleinschnitz Jr.** *October 01, 2016 15:48*

I am assuming that you just want the optical sensor removed and replaced on the existing end stop board?

If so you need someone that can de-solder (should have de-soldering tools) and replace and solder in the new sensor. Worse case if you absolutely cannot find someone you can send me the new part and the board and I will do it for you.


---
**timb12957** *October 01, 2016 15:51*

Thank you Don for the offer. However I can handle putting the new optical sensor on the board, I am wondering what to do next if that does resolve the problem.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 01, 2016 15:57*

If you purchased using PayPal, be wary of the end-date for the PayPal claim. They try run you out of time so then they will not be liable for any refund/replacement/etc.



I would suggest that an electrical repair shop would have someone capable of performing the required repairs, but not 100% certain (as I didn't see your previous post/thread yet).



Get a quote ASAP from someone capable of the repair & forward that quote to the Seller/open PayPal claim to make sure you get refunded the right amount to cover the costs of repair.


---
**Don Kleinschnitz Jr.** *October 01, 2016 16:04*

**+timb12957** do you mean, if that "does" or "does not" resolve the problem. Mistype?

 

You (or a technician) can do more tests to see if the repaired board and endstop is working by looking at the output of the endstop with a meter while pushing it in and out.



BTW **+Yuusuf Sallahuddin** makes a good suggestion so you get some compensation.


---
**Alex Krause** *October 01, 2016 16:38*

I would replace it with mechanical endstops/ limit switches but that's just my preference when it comes to stuff like this...


---
**Don Kleinschnitz Jr.** *October 01, 2016 17:10*

**+Alex Krause** that's and alternative but wont he have to pull the carriage or at least drill and tap? Need to insure that switch is wired such as to produce an equivalent signal to the optical which is 5V when at home. Also a means of wiring it to the Nano or the endstop breakout is necessary?


---
**timb12957** *October 01, 2016 17:13*

Sad news. I have replaced the optical sensor on the PCB. No luck. The machine is still malfunctioning. I guess I will have to press the seller to allow me to return the machine for a replacement.


---
**Alex Krause** *October 01, 2016 17:17*

Possibly a bad M2 nano board


---
**timb12957** *October 01, 2016 17:30*

I may be wrong, but have to wonder if the end stop is the issue. There is NEVEF any movement to the rear, so the end stop is never in question as to being triggered. Unless a bad end stop board would cause the "forward only" movement of the gantry. When working properly, after the gantry moves to the rear and triggers the end stop, what controls where the gantry stops as it moves forward to establish starting position?


---
**Don Kleinschnitz Jr.** *October 01, 2016 17:46*

I guess returning it is the best next step, but wish we could have found the problem. 



Sure would like to get my hands on that end stop board to see it something else is wrong and be able to understand if this is really the problem once and for all.



You still can have a problem with the cable or connectors that goes back to the M2 board. As a last ditch effort you can check for continuity between the pins of the ribbon cable connector on the end stop card and the other end at the M2 connector.



Can't answer how it works as its in the firmware of the M2 board but if I recall it goes to home and then the M2 board controls its starting position based on where you move the carriage in laserdraw? 



One scenario is that the bad board immediately gives it a home signal (an open on the +yhome signal would do that) and then the carriage tries to go to the last position.


---
**Alex Krause** *October 01, 2016 18:10*

Can you post a picture of the laser configuration settings? The one where you choose the correct motherboard and input your boards serial number


---
**timb12957** *October 01, 2016 18:19*

Here you go.

![images/f7438d48838c3c18a7a4ab7a1dbd87cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f7438d48838c3c18a7a4ab7a1dbd87cd.jpeg)


---
**Alex Krause** *October 01, 2016 18:20*

Are you running a stock 40w k40?


---
**timb12957** *October 01, 2016 18:33*

The continuity between the pins of the ribbon cable connector on the end stop card and the other end at the M2 connector all test fine.


---
**timb12957** *October 01, 2016 18:34*

Yes it is stock other than a removed exhaust vent.


---
**Alex Krause** *October 01, 2016 18:37*

Stock K40 page size X is 300 and page size Y is 200


---
**timb12957** *October 01, 2016 18:40*

If you are referring to the machine settings screen shot I posted, these are set as it came. I have made no changes, and the machine has worked with those settings for the first two months.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 01, 2016 18:46*

**+timb12957** Try set them to 300x200 & see if it makes a difference. I doubt it will affect it, but could do. Where are your end-stops located? For 0x & 0y? or for max-x & max-y?


---
**timb12957** *October 01, 2016 19:19*

Page size set to X300 Y200. No effect. End stops are optical and the Y stop is located at the back left corner. I can not even see a X end stop, however the X axis is orienting correctly.


---
**timb12957** *October 01, 2016 19:21*

I tried to disconnect the coupling and the belt from the Y step motor after it orients in the middle of the machine. I then manually moved the head to the correct start position. I reconnected the coupling and the belt to the Y step motor. I powered off and back on with the same failure.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 02, 2016 00:44*

**+timb12957** X-stop will be beneath the horizontal rail, on the left side of it. Mine has optical stops too & for X & Y there are small pieces of metal jutting out that go into the U on the optical endstop. Y axis has metal jutting from back rail, X axis has it coming from beneath the head carriage.


---
**timb12957** *November 28, 2016 22:36*

I hope this will be seen on this old post. The seller gave me a partial refund of $200. Since my purchase price was $318 including shipping I guess I should be happy. Anyway I found that by manually moving the head to the correct position after power up sequence, I was able to cut! That worked for awhile, but then during rapid moves, it started jumping out of position. I won the bid on eBay for a new  LIHUIYU M2 Controller board, only $15. It arrived today and I am wondering do I just pop it in and hope for the best? Any input would be appreciated.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/HWVMDHiQyJ6) &mdash; content and formatting may not be reliable*
