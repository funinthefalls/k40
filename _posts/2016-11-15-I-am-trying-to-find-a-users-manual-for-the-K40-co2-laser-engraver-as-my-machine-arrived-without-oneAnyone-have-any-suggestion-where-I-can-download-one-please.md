---
layout: post
title: "I am trying to find a users manual for the K40 co2 laser/engraver as my machine arrived without one.Anyone have any suggestion where I can download one please?"
date: November 15, 2016 11:10
category: "Hardware and Laser settings"
author: "stephen whitman"
---
I am trying to find a users manual for the K40 co2 laser/engraver as my machine arrived without one.Anyone have any suggestion where I can download one please?





**"stephen whitman"**

---
---
**Ariel Yahni (UniKpty)** *November 15, 2016 11:23*

**+stephen whitman**​ the truth is you are saved from reading that nonsense. If you need some info on using corellaser and stuff look at the bottom of this page as I have collected some resources [http://wp.me/P7RQqG-1J](http://wp.me/P7RQqG-1J)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 15, 2016 11:38*

I am fairly certain not one of us 2000+ members of this group received a manual. One of your best sources of information is this group here or I believe there is also a Facebook group too.


---
**Alex Krause** *November 15, 2016 19:18*

**+Yuusuf Sallahuddin**​ this group is a better source of help than the Facebook laser group... on FB there is alot more snoody attitudes to K40 owners by Epilog and trotec owners, along with drama revolving around design help and material supply concerns 


---
**HalfNormal** *November 15, 2016 19:32*

**+Alex Krause** FB snoody?! :-)


---
**Kelly S** *November 15, 2016 20:02*

What's a manual?  Lol...  I agree these did not ship with anything remotely close to a real manual.   However as stated, this is a wonderful place to read and learn more.  I just put in my order for a new brain for my box so will have even more learning to do.




---
**Ashley M. Kirchner [Norym]** *November 16, 2016 00:29*

Manual! (said with a Spanish accent) Manual, open laser. 


---
*Imported from [Google+](https://plus.google.com/113698164694365846556/posts/X7GPvDhmMWK) &mdash; content and formatting may not be reliable*
