---
layout: post
title: "I finally bit the bullet on Photograv"
date: April 17, 2017 22:34
category: "Software"
author: "Bob Damato"
---
I finally bit the bullet on Photograv. Looks like it can be pretty nice. I had super success with one image, then a while later I tried another image and wasnt as lucky. I looked around youtube for some really good in depth how to's for it but cant really find anything. They are just 2 and 3 minute videos that dont really explain everything.  Can anyone point me in the right direction for decent training videos?







**"Bob Damato"**

---
---
**Andy Shilling** *April 17, 2017 22:51*

Looks very expensive for a some software that does what smoothie and laserWeb already does or am I missing something? 


---
**Bob Damato** *April 17, 2017 23:39*

It really is. Plus its nothing that photoshop cant do too. But I needed something easy for my daughter to use too.


---
**Ned Hill** *April 18, 2017 14:25*

You can always ask the company or ask on a Facebook laser group if you're willing to wade through the snarky replies.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/PeTAdxgMbZf) &mdash; content and formatting may not be reliable*
