---
layout: post
title: "I have a question about focus. I have the stock head that came on the K40"
date: November 19, 2017 13:36
category: "Hardware and Laser settings"
author: "timb12957"
---
I have a question about focus. I have the stock head that came on the K40. I have added an adjustable bed to my laser. Can someone tell me a good method to use to focus the beam for optimal cut using this setup?





**"timb12957"**

---
---
**BEN 3D** *November 19, 2017 13:44*

I am at the same Issue, my plan is, try and error, to level the correct high. And after i found the perfect distance, I will add two Laserpointer and fix them on the focuspoint as I already seen here in the community. If the object is to low or to high, the two laserpointers will show two points, if the level is perfect, they will show just one point. Thats my plan.


---
**Don Kleinschnitz Jr.** *November 19, 2017 14:45*

A ramp test. Search the community. 


---
**timb12957** *November 19, 2017 19:15*

When I search for ramp test, I find many references to those having done one, but can not find what the procedure is to do it


---
**Don Kleinschnitz Jr.** *November 19, 2017 19:43*

Ok I will check or detail for you.  I'm Mobile just now.


---
**timb12957** *November 19, 2017 19:44*

Thanks Don!


---
**Joe Alexander** *November 19, 2017 22:01*

i personally use a small gauge block(scrap piece of metal) to check that fits just under the air nozzle when its at the right height. easy setup when changing pieces. for cutting i gauge then adjust up half the thickness of the material.


---
**Don Kleinschnitz Jr.** *November 19, 2017 23:42*

Measure the distance from the head to the tables surface, this is the base distance.

Lay a peice of material ( I use 1/4" mdf) propped up on one end at an incline horizontally under the head. I use a 6"x4" peice. 

Send a Gcode to move horizontally over the material making burning a dark line. 

Take the marked material out and put on a flat surface at the same incline. Look for a place on the material where the line is the thinnest, that point represents the correct focal point. 

Measure the distance between the inclines surface and the table at the lines thinnest point. Subtract that amount from the base distance and that's the correct focal length.



 If the line does not thin anywhere try turning down the power or increasing/decreasing the angle. 

 Hope this is not too confusing? 


---
**Kevin Lease** *November 20, 2017 00:54*

I have a file that simply draws a horizontal line 

For light object z table with c3d and glad

Use 3mm plywood piece about 3-4 inches wide

I start with z table at lowest position

I run the file then mark z table height setting with a pen then slide plywood piece away 2-3 mm raise height 1mm run again and mark height setting

Repeat iteratively

You will see line start fat when out of focus then gradually become thinner as you approach focal point then will widen as you move out of focus again


---
**Bill Keeter** *November 20, 2017 14:51*

I mostly cut 1/16, 1/8 and 1/4" acrylic. So once I found out my optimal height. I created a custom 3d printed cube for each of my material heights. Set the material on my bed. Put the cube on top in the middle of the material. Move the bed/head until the cube almost touches the laser head....  focused.


---
**Don Kleinschnitz Jr.** *November 20, 2017 19:00*

**+Bill Keeter** Yes once I get the right FL I made a gauge black as well.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/3nV4E5dCon8) &mdash; content and formatting may not be reliable*
