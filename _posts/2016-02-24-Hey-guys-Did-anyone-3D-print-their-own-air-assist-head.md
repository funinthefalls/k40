---
layout: post
title: "Hey guys ! Did anyone 3D print their own air assist head?"
date: February 24, 2016 10:08
category: "Modification"
author: "Martin Byford"
---
Hey guys !

Did anyone 3D print their own air assist head? 

I'd be willing to buy one from someone who wants to print one for me!! The air hook up and system I can sort out myself just looking for the Nozle. 

Thanks 

Martin 





**"Martin Byford"**

---
---
**Imko Beckhoven van** *February 24, 2016 11:15*

Where are you located?


---
**Arestotle Thapa** *February 24, 2016 12:16*

Someone was printing and selling in ebay for ~$15 so I tried that. The laser burnt hole in the side as soon as I started. I may have installed that  in angle. So I bought an aluminum one from LO. I've not tried that yet.


---
**Martin Byford** *February 24, 2016 12:42*

I'm in Dublin, Ireland. I do have friends with 3D printers alternatively. Il check out the aluminium ones !


---
**3D Laser** *February 24, 2016 16:37*

The LO on works great I have mine own but I had to take the air assist part off for the moment as the air line got in the way of the laser and burnt a hole in it


---
**I Laser** *February 25, 2016 09:29*

Stick with a aluminium one, the first time it's slightly off angle the laser will burn a nice hole through it rendering it useless.. I have two LO ones, the first was great the second was skewed so you have to sit it on an angle :\


---
**The Technology Channel** *February 27, 2016 08:36*

I have just bought a 3d printer to print one....


---
**3D Laser** *February 27, 2016 12:24*

**+I Laser** can't you adjust the head placement buy loss ending the screw.  I thought mine had the wrong placement for the air intake until I realized that was attention screw that could be move I felt really smart at that moment 


---
**I Laser** *February 27, 2016 21:58*

**+Corey Budwine**​ sorry, poorly worded on my part. It's the bottom bit where you plug the hose in I'm referring to. If you sit it flush the laser hits the side, the only solution I found was to tighten it up on an angle. Pain in the butt when you need to clean the lens as you have to go through the trail and error if getting it back at the right angle, was thinking of making a shim but haven't got around to it yet.


---
*Imported from [Google+](https://plus.google.com/109380924123528435976/posts/NAMqq3fAzBk) &mdash; content and formatting may not be reliable*
