---
layout: post
title: "Hello before i get the the issue I'm having I wanted to say a big thanks to Ray Kholodovsky for the cohesion (smoothie) board it made things really easy, also thanks to the Devs at laserweb I think they are doing brilliant"
date: May 10, 2017 07:15
category: "Smoothieboard Modification"
author: "Mike R"
---
Hello before i get the the issue I'm having I wanted to say a big thanks to **+Ray Kholodovsky** for the cohesion (smoothie) board it made things really easy, also thanks to the Devs at laserweb I think they are doing brilliant work. I have not posted much in these groups as I have found the answers to my many questions in previous posts.

The issue I'm having is with the limit switches once they have been tripped they will not move from them I get a m999 error and have to manually pull the head off the switch and reset the alarm.

I have also installed mechanical min switches.

I have posted a picture of my k40 and of a raster image (still need to tweak the settings).

Many thanks Mike





![images/927d57b4e13e46dea0f9adc0a5cb7ce1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/927d57b4e13e46dea0f9adc0a5cb7ce1.jpeg)
![images/5aa54d70a7210d2beab075102221b5fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5aa54d70a7210d2beab075102221b5fd.jpeg)

**"Mike R"**

---
---
**Joe Alexander** *May 10, 2017 07:23*

in Laserweb4 if I trip a limit switch (mine are also mechanical and min/max) it goes into alarm but I can clear alarm then i am able to move(clear alarm being the abort job button when error occurs). Does that not work for you?


---
**Mike R** *May 10, 2017 07:32*

Thanks for the reply. I can clear the alarm but when I go to jog it clicks then comes up with the alarm again.

I have tried the retract setting in config but no good.


---
**Joe Alexander** *May 10, 2017 07:39*

hmm ill have to test it on my rig when i can tomorrow. using most recent firmwares/builds/etc? (ie firmware-cnc.bin if smoothie) after an error can you clear then click home to see if that works?


---
**Mike R** *May 10, 2017 07:50*

I'll update every thing again tonight to make sure. 


---
*Imported from [Google+](https://plus.google.com/106051294887421131369/posts/J1i51yJCCGE) &mdash; content and formatting may not be reliable*
