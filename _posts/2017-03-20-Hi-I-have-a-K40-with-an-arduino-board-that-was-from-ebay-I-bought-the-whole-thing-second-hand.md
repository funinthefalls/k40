---
layout: post
title: "Hi, I have a K40 with an arduino board that was from ebay, I bought the whole thing second hand"
date: March 20, 2017 15:45
category: "Modification"
author: "Mike Donlon"
---
Hi, I have a K40 with an arduino board that was from ebay, I bought the whole thing second hand. The board seems to run LasaurGrbl firmware, and suggests Lasersaur web app as the interface. I would like to try out laserweb3 or laserweb4, I am wondering if anybody has any experience with these lasersaur boards?



Can I user this firmware and expect things to work as-is, or will I have to install a more generic grbl firmware?



What is involved configuration wise for setting up grbl on this board?



This is the board in use: [http://www.ebay.com/itm/152123110457?ul_noapp=true](http://www.ebay.com/itm/152123110457?ul_noapp=true)





**"Mike Donlon"**

---
---
**Cesar Tolentino** *March 21, 2017 15:24*

Just to let you know I have the same board that I installed late last year. In my case it's a plug and play. Nothing else is needed except hire to mount the board on your k40.



I really like it. It cuts wonderful. Workflow with inkscape then lasersaur web app is nice. 



Having said that, the board is not perfect. It has short comings on the engraving side. It will not do pwm engraving like the newer board that can do laserweb. This board is old and no longer supported. The board is slow in engraving side, 1500mm/min.



All in all, I still like it. I already have my workflow which is way better than my mohidraw.






---
**Cesar Tolentino** *March 21, 2017 15:25*

Oops, i think I misread your post.  If you are going to try to use it for laserweb, I want to know too. So please share what ever you discovered. Thanks


---
**Mike Donlon** *March 21, 2017 15:26*

**+Cesar Tolentino** but the board itself should support grbl, which is supported by LW according to their docs. I don't mind the board at all, just looking for more from the interface, eg PWM for different shades in a raster image


---
**Cesar Tolentino** *March 21, 2017 15:32*

I ask the same question to **+Peter van der Walt**​, who I think is the brain behind both software, he said that the firm ware is too old. And the grbl is a sub-grbl, like only prob works. Or something like that.


---
**Cesar Tolentino** *March 21, 2017 15:36*

**+Peter van der Walt**​. Since we already have the board, are we doomed? Can we do something so we can use your beautiful laserweb?


---
**Cesar Tolentino** *March 21, 2017 15:51*

OK thanks. But based on your links above. If I have Arduino and the buy that linky, then I should be able to do laserweb.


---
**Cesar Tolentino** *March 21, 2017 16:09*

No I meant the Banggood link above, plus Arduino and I should be able to run laserweb. 



**+Mike Donlon**​. Apology, I hijacked your post.


---
**Mike Donlon** *March 21, 2017 17:06*

**+Cesar Tolentino** lol no problem, this is the info I was looking for. So basically I just need to compile standard grbl firmware, with a custom pin map based on the pins used in lasaurgrbl?



Is there any thing else needed?



Is there any docs on doing this with this FW/Board combo?


---
**Mike Donlon** *March 21, 2017 17:08*

Is there an advantage to use the uno over the mega? I have several of both, but don't own a grbl shield, only ramps shields.


---
**Cesar Tolentino** *March 21, 2017 17:10*

I dont have grbl shield too. Just Arduino Uno and an old tb6560 as a driver. But i am using it on my CNC machine and not my K40.




---
**Mike Donlon** *March 21, 2017 19:29*

**+Peter van der Walt** what's the best "future proof" board to get for LW and do I need a new adapter board for the ribbon cable or can I use the one that came with that eBay listing I posted?


---
**Mark Brown** *March 21, 2017 20:31*

That Banggood link actually comes with a (knockoff) Uno... Hmm...


---
*Imported from [Google+](https://plus.google.com/+MikeDonlon/posts/A3pitBt862N) &mdash; content and formatting may not be reliable*
