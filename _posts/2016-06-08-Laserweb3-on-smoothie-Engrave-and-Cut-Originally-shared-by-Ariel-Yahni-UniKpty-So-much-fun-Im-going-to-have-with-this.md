---
layout: post
title: "Laserweb3 on smoothie. Engrave and Cut Originally shared by Ariel Yahni (UniKpty) So much fun I'm going to have with this"
date: June 08, 2016 04:23
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Laserweb3 on smoothie. Engrave and Cut



<b>Originally shared by Ariel Yahni (UniKpty)</b>



So much fun I'm going to have with this. 

![images/e3777afdc55f10814810e1acd3c3cef5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e3777afdc55f10814810e1acd3c3cef5.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *June 08, 2016 12:13*

Yay! Now we who are following in your Smoothie footsteps can deluge you with questions😃


---
**Ariel Yahni (UniKpty)** *June 08, 2016 12:16*

Bring them on :-)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Gx2VRDiMuDL) &mdash; content and formatting may not be reliable*
