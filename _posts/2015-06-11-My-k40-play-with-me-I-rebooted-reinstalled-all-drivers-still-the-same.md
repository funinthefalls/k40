---
layout: post
title: "My k40 play with me. I rebooted, reinstalled all drivers still the same"
date: June 11, 2015 09:52
category: "Hardware and Laser settings"
author: "Przemys\u0142aw Wac\u0142awek"
---
My k40 play with me. I rebooted, reinstalled all drivers still the same. Somtimes do half job and then go to a side. Like on video. Any ideas ???


**Video content missing for image https://lh3.googleusercontent.com/-sjtkDST3QJk/VXlZoLVmy_I/AAAAAAAAAE0/YUmQ_3HhNCw/s0/MOV_0297.mp4.gif**
![images/535c58b6fc5a68b1da4873f2411300a6.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/535c58b6fc5a68b1da4873f2411300a6.gif)



**"Przemys\u0142aw Wac\u0142awek"**

---
---
**Bee 3D Gifts** *June 12, 2015 19:51*

Thats a tough one...I cannot really tell too much from the video...maybe loose belts?


---
**Bee 3D Gifts** *June 12, 2015 19:53*

Or maybe in the set up? Hopefully someone else will reply too.


---
**Przemysław Wacławek** *June 13, 2015 16:02*

Belt cleaning did help :-) 


---
**Bee 3D Gifts** *June 13, 2015 18:22*

Ohh good!


---
**Jim Root** *June 16, 2015 00:08*

Double check the model and serial numbers in the software. Mine was wrong but the machine worked fine for a little while then it just started doing everything at about 5mm per second. Fixed the numbers and it's been working fine since. 


---
**Przemysław Wacławek** *June 16, 2015 18:42*

Sorted. The problem was some dirts on the belt


---
*Imported from [Google+](https://plus.google.com/115984551907439019172/posts/afARXmBNri8) &mdash; content and formatting may not be reliable*
