---
layout: post
title: "I upgraded the mirrors and lens from light object It made a huge difference improving the optics Previously it would take three passes with 15 A at 5 mm/s to cut through 3 mM Baltic Birch plywood consistently Now I can do the"
date: October 28, 2017 23:44
category: "Discussion"
author: "Kevin Lease"
---
I upgraded the mirrors and lens from

 light object 

It made a huge difference improving the optics

Previously it would take three passes with 15 A at 5 mm/s to cut through 3 mM Baltic Birch plywood consistently

Now I can do the same thing with one pass and much less charring





**"Kevin Lease"**

---
---
**Anthony Bolgar** *October 28, 2017 23:51*

Did you get an air assist nozzle as well? They make a world of difference.




---
**Kevin Lease** *October 28, 2017 23:57*

Yes hav e air assist


---
**Ned Hill** *October 29, 2017 00:51*

Yep I did the same thing when I first got my laser.  It's pretty much one of the first upgrades you should do.


---
**Kevin Lease** *October 29, 2017 13:03*

Unexpected side effect, since it's cutting so much better it generates a lot more smoke in the same amount of time so the air evacuation needs to be improved


---
**Steve Good** *November 03, 2017 10:35*

Could you post a link to the parts you ordered.




---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/dupBi5Dpy21) &mdash; content and formatting may not be reliable*
