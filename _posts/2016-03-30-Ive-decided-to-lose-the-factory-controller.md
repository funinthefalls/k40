---
layout: post
title: "I've decided to lose the factory controller"
date: March 30, 2016 17:35
category: "Discussion"
author: "Scott Marshall"
---
I've decided to lose the factory controller. I have a ethernet Smoothstepper and drivers in stock, but wanted to try a Smoothieboard since they have such a following here, and are an all in one board. (and look like a pretty slick setup to me)



The problem is after a fair amount of research, the only current source seems to be Robotseed, and they have only the 5 axis model at the rather steep  - $208.39US with $36.97 tax !!??(must be customs?) and $13.44 shipping (reasonable) which gets me in at $258.80usd compared to $179 plus shipping from Uberclock in the  US (if they had any). +80 bucks is a hard hit to take on what amounts to $258 vs $130 if they has a 3 axis in stock @ Uber. Almost exactly twice the price - ouch!

Additionally, there seem to be a series of delivery issues from Robotseed based on the Smoothie forum.



After some investigation, it seems the board is so nice, they were swamped with orders around June 2015 and have not yet recovered. The word is they can't even keep up with the email traffic.

I can't seem to find out when they will once again be available from Uberclock in the US.



Does anybody have any information on the status of production and if they have announced a availability date?



If I can't find one, I guess I'm going to use my Smoothstepper, and the 7amp drivers I have, extreme overkill for a laser pushing a mirror around.



Also if someone has an extra (3,4 or 5 axis) and would like to sell it, please let me know.



Thanks, Scott





**"Scott Marshall"**

---
---
**Manuel Conti** *March 30, 2016 18:13*

Hi,  i have bought this board: smoothieware mks sbase v1.2

[http://s.aliexpress.com/ai6V7zYJ](http://s.aliexpress.com/ai6V7zYJ)



I want convert it into k40 controller. It's compatible with laserweb of **+Peter van der Walt**​


---
**Damian Trejtowicz** *March 30, 2016 18:42*

I want upgrade to smoothie as well,but cant find any good tutorial how to do this


---
**Alex Krause** *March 30, 2016 19:03*

Lol you can blame **+Peter van der Walt**​ (just kidding) for not being able to buy one... Laser web is so awesome that he has all the smoothies sold out :P


---
**Alex Krause** *March 30, 2016 19:06*

I hope you have a referral program going with Mr. Wolf ;)


---
**Scott Marshall** *March 30, 2016 19:08*

**+Damian Trejtowicz** They have pretty decent info if your familar with the basics of CNC driver systems (Links below),

 The Smoothieboard is just a ethernet/usb driven (you can go parallel with an accesory board, but why go backwards?) with a 32 bit processor, 3 to 5 integrated 2a 4 lead capable stepper drives, and a bunch of FET outputs. It also had inputs and onboard expandibility for endstops etc. Pretty much a normal CNC panel on a board, sans power supplies. Slick, especially if you don't need big steppers and the price is right.



Here's a master link :[http://smoothieware.org/smoothieboard](http://smoothieware.org/smoothieboard)



If you click on "documentation" there's a basic how to for a Laser, Mill, and 3D Printer.



The trick is getting one.



 I'd rather not buy a chinese copy, as I spent 30 odd years as a Controls engineer with his own company, and wouldn't really do that to the developer. Since he's good enough to offer up his engineering and development time as open source, even hardware and parts as well as firmware, I'd like to buy it from him (Mark Cooper). 

If I was him I'd be pretty hostile towards chinese manufacturers stealing his work before he can even establish his supply lines. The one on alienxpress is such a blatent theft, they didn't even change the board layout. I've no doubt they bought one of his boards, copied it photographically and started banging out counterfeits.



 It's easy to beat Marks price when you have no development costs, use substandard parts and pay your laborers $2 a day. It'a also illegal and wrong, but nobody is currently doing anything about it. 



That's a large part of our economic problems here in the US, and I stop there before I cross into taboo subjects for a tech forum.



Sorry for getting on my soapbox, but that's the way it is.



Scott












---
**Scott Marshall** *March 31, 2016 06:01*

**+Peter van der Walt** 

Thanks Peter,

I'm aware of the great cache of info you folks have created, and it's partly responsible for selling me on the Smoothie over other choices. The other plus is the incredible documentation Mark Cooper has made available, you can mod a Smoothie to run most anything CNC thanks to the plethora of available I/O - all you need do is buffer it,  and you could control  shipyard machine tools. It's really amazing that a $180 open source board can run with the Fanuc and Okuma stuff with a little massaging. I think you are going to see a lot of this sort of equipment running with the big dogs in the near future.



I've been watching what you've been doing, and has indeed been a big step forward, especially the photo engraving  software you've written. I agree with Alex that You and Stephanie are indeed responsible for a lot of the Smoothieboards sold.



As many as the K40 people are using, it's only a small segment of the market the Smoothie has taken by storm.  I sure hope there are enough coming in this batch that I can grab a couple before they are gone.



I may have to buy a bare board and populate it or just plain wait out the production to catch up. I'm no fan of SMD stuff for experimenting, I'd rather be able to have room and ease of modification than a smaller board, but all the good chips are SMD only these days. My eyes are not what they once were, and small quantities of discrete SMD components are expensive so I don't really feel much like assembling a Smoothie.



That being said, looking the board over it doesn't look too bad.

I guess I'll wait it out  for a while and in the meantime keep looking for someone with a couple on the shelf they are willing to sell.



When I have some time I'll start sourcing the components to populate a board and see how it stacks up money wise. If it's cheap enough, I may build a couple. 

-------------------------------------------------------------------------------------------------



Edit PS, I just sent Aurthur Wolf an email asking whebn they are expected in. I'll post the info when/if he answers.



Scott

﻿


---
**Scott Marshall** *March 31, 2016 06:40*

**+Peter van der Walt** 

Thanks Peter, that's very useful, I can live with that. Just sent Mr Wolf an email, his email is posted over on Openbuilder.



Scott


---
**Scott Marshall** *March 31, 2016 09:18*

SMOOTHIEBOARD AVAILABILITY



Anyone who is waiting for Smoothieboards, I contacted Mr Wolf, and he responded:

////////////////////////////////////////////////////////////////////////////

Hey.



The new batch is ready at the factory, it should ship in a few days.

Then a few days more to travel.

Then a few days more in customs ( up to a week )

Then Uberclock will be in stock again.



Cheers !

 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

This was on 03/31/16



Warm up the Paypal account, the Smoothies are coming!



Scott


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/6qDNJAfGe72) &mdash; content and formatting may not be reliable*
