---
layout: post
title: "My 50W Chinese laser is back up and running after getting the tube and PSDU replaced under warranty"
date: October 01, 2017 13:53
category: "Discussion"
author: "Anthony Bolgar"
---
My 50W Chinese laser is back up and running after getting the tube and PSDU replaced under warranty. The tech did not speak much English, but enough that we could communicate. It is great having a local dealer that stands behind their warranty. I had him put in my 60W tube and leave the 50W replacement as a spare. I just need to make a tube extension housing as the new tube sticks out about 6" I also need to make a base as this unit has wheels but sits way to low to the ground.







**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *October 01, 2017 14:36*

**+Anthony Bolgar** did they do any tests to prove LPS or tube bad?? I am particularly interested in the resistor that some vendors use to simulate the load of the tube. Knowing its value would be interesting.


---
**Anthony Bolgar** *October 01, 2017 15:03*

He just temporarily hooked a PSU to the tube with test clips (dangerous as hell in my opinion) That showed the tube was bad. He then hooked my PSU to a good tube and it showed the PSU was faulty. He happily put in my 60W tube for me, which saved me the effort of removing the 50W tube and reinstalling the 60W tube. I cringed when I saw him hook the HV line to the tube with an alligator clip, I stepped back a good ten feet while he did this.


---
**Don Kleinschnitz Jr.** *October 01, 2017 15:26*

**+Anthony Bolgar** So he brought a good test LPS that he used to prove the tube was bad?

Wow that seems like a high risk job?


---
**Anthony Bolgar** *October 01, 2017 16:34*

Yup, not the way I would have done things (I like living), but I did get the LPS and tube replaced under warranty very quickly.


---
**Sebastian Szafran** *October 02, 2017 09:48*

**+Anthony Bolgar**​ Do you have same type LPS installed for 60W tube as it was originally installed for 50W?


---
**Anthony Bolgar** *October 03, 2017 15:07*

Yup, same lps


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/C7xDMEUtqX4) &mdash; content and formatting may not be reliable*
