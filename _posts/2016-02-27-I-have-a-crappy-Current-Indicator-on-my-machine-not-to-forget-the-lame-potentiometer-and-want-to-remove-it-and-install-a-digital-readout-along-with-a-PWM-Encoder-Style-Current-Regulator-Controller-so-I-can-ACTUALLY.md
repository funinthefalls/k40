---
layout: post
title: "I have a crappy \"Current Indicator\" on my machine, not to forget the lame potentiometer, and want to remove it and install a digital readout along with a \"PWM Encoder (Style) Current Regulator/Controller\" so I can \"ACTUALLY"
date: February 27, 2016 04:29
category: "Software"
author: "Joseph Midjette Sr"
---
I have a crappy "Current Indicator"  on my machine, not to forget the lame potentiometer, and want to remove it and install a digital readout along with a "PWM Encoder (Style) Current Regulator/Controller" so I can "ACTUALLY CONTROL THE POWER TO THE LASER"!



Can someone provide the proper input to my help me achieve my aspirations???



Please nobody tell me to just change the control board to a "Smoothie" or something else". There is nothing wrong with the board that's in here, it can be made to do a lot more with a little persuasion! I know there is a geek on this Group that can help.....



Thanks in advance for any input and suggestions!

![images/39860095548fbbf6c2679b7bac2a53b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/39860095548fbbf6c2679b7bac2a53b3.jpeg)



**"Joseph Midjette Sr"**

---
---
**Joseph Midjette Sr** *February 27, 2016 04:43*

**+Peter van der Walt** Thanks! That's was so UNHELPFUL!!! I love sarcasm though!




---
**Stephane Buisson** *February 27, 2016 06:55*

it's no point to update to smoothie if you are happy with your software. the all goal to to do the update is to go for better softwares (and that's easy to do better).

Changing your VU meter by a digital one , you take the risk to not be able to read anything as the value will change too fast.

or go for something which is able to show the peak only(+$). at the end you will realise, the Vu meter was not that bad and not worth to spend the extra money.


---
**Tony Sobczak** *February 27, 2016 07:10*

**+Peter van der Walt** which smoothie board would I need for the machine. What all is needed to complete the installation. 


---
**Richard Taylor** *February 27, 2016 18:37*

Unfortunately the characteristics change with temperature and I suspect tube life. I find I have to compensate and add more on the pot (higher voltage) in winter. It is important to monitor the max current as going over 18mA (IIRC) will shorten tube life with little extra gain in cutting power.

Just my 2p :)


---
**Mark Finn** *February 27, 2016 20:48*

**+Richard Taylor** Are you sure about that? Most of what I have seen written is that cooler is better for CO2 laser efficiency within a range of reasonable temperatures.


---
**Richard Taylor** *February 27, 2016 22:08*

**+Mark Finn**​ Yes, I have two marks on my Pot approx 10 degrees apart. I suspect it is more to do with temperature and humidity of the psu circuitry though. It's in a garage which can range from -5 to+30 and goodness knows what humidity.


---
**Justin Mitchell** *February 28, 2016 15:11*

I printed a graduated scale on a label, cut it out and stuck it around the knob. now i can easily repeat a number of settings to compare results.


---
**Mark Finn** *February 28, 2016 15:50*

I plan to replace mine with g-code controlled PWM, but in the meantime I also found marking the dial to be useful.



I also added a 330 ohm resistor to the top of my pot so that I couldn't go above 16  milliamps actual at the tube (18 on the meter).   From the factory I could hit about 24 milliamps on the meter which was About 22 actual. Limiting at 16 was just a guess for safety, anyone have any other numbers they recommend?


---
*Imported from [Google+](https://plus.google.com/104424492303580972567/posts/Ad9oaHdqxrX) &mdash; content and formatting may not be reliable*
