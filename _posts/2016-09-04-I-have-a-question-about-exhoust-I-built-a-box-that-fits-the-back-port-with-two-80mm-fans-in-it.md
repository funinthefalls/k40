---
layout: post
title: "I have a question about exhoust. I built a box that fits the back port with two 80mm fans in it"
date: September 04, 2016 01:16
category: "Modification"
author: "VetGifts By G"
---
I have a question about exhoust. I built a box that fits the back port with two 80mm fans in it. They are 12v .15a each and my power supply is 12v 200ma. They don't move air like they should. What power source do I need? .30a?

I also tried a boat bilge blower(turbo 4000) and when hooked up to the same power supply it barely moves. I know I need higher amperage, but how high?





**"VetGifts By G"**

---
---
**Anthony Bolgar** *September 04, 2016 02:14*

My bilge blower draws around 5 Amp


---
**Phillip Conroy** *September 04, 2016 04:49*

Do not waste your time and money on low power fans ,i use and highly recomencd this one [ebay.com.au - Details about  8" Heavy Duty 2900 RMP 200mm Portable Super-Speed Extraction Fan Ventilator Set](http://www.ebay.com.au/itm/111240702828?_trksid=p2055119.m1438.l2649&ssPageName=STRK:MEBIDX:IT)


---
**Bart Libert** *September 04, 2016 10:14*

Try this baby. But be aware, your power supply must be able to give 4A (preferably 5A) continuously . [ebay.com - Details about  Brand New Delta TFC1212DE 120mm 12cm PWM 252CFM 12038 DC 12V cooling fan](http://www.ebay.com/itm/Brand-New-Delta-TFC1212DE-120mm-12cm-PWM-252CFM-12038-DC-12V-cooling-fan-/252119691871?hash=item3ab381325f:g:Gz4AAOSwbdpWUB7-)


---
**Przemysław Wilkutowski** *September 05, 2016 23:30*

Guys - if i may ask - what flow rating is enough for the K40 ?

Will this:



[canfan.nl - cfrk100](http://www.canfan.nl/cfrk100.html)



140 CFM generaly - be to small / just right or overkill? 






---
**Anthony Bolgar** *September 06, 2016 00:26*

I run at 240 CFM. A lot depends on the length of the exhaust pipe, how well sealed the pipe is, how many elbows there are and more. But 140cfm on a 20'run should suffice as long as the pipe is well sealed (use aluminum duct sealing tape on all joints) and try to minimize bends or diameter changes in the pipe.


---
**VetGifts By G** *September 06, 2016 01:32*

I ended up with the turbo 4000 marine bilge blower, powered by Xbox 360 psu and homemade box to fit the back of the k40. Just painted it, but haven't tried it with the machine running. It seems like it moves air way more than the stock. I will do a full test in a about a day or two and let you all know the result


---
**Przemysław Wilkutowski** *September 06, 2016 13:34*

Hmm on Facebook group guys are saying that 720 barley makes it some times.. I do plan on perfect sealing, just not  sure what rating i should believe



Anthony do you have smell of 'ANY' odor whatsoever ?


---
**Przemysław Wilkutowski** *September 06, 2016 13:45*

Miro G



If you have an ammetter try connecting your fan to a 12v car battery (it will be able to supply as much current as the fan needs) and just measure the consumption this way


---
**Anthony Bolgar** *September 06, 2016 14:21*

Of course there is a small amount of odour. Totally unavoidable as the K40 is not sealed and even with the best fan you could think of, there are places it can escape. But there is not any long lasting noticeable odour and any fumes that escape are in such a low concentration that you are safer breathing that than driving down the highway and breathing the small amount of exhaust gasses that enter your car.


---
**Bart Libert** *September 06, 2016 15:34*

**+Przemysław Wilkutowski** Whatever you want to do, don't go for perfect sealing. Its the worst you can do. If you do perfect sealing, then your machine will be like a vacuum. Your exhaust fan needs to be able to suck in air to blow it away. You do need holes where the machine can suck air. A good place is at the front of the machine, since you suck away air in the back. That way it takes the smoke with it and also the smell. Of course you should not have TOO many holes, but with a good fan, a lot of holes are certainly possible because the more backpressure your fan has, the less air it will move, the hotter it will become and the less efficient it will be.




---
**Przemysław Wilkutowski** *September 06, 2016 16:10*

As for perfect seal i meant the exhaust system it self not a full seal on the unit but sure will keep that in mind



Ok Anthony, thanks for the info i think i know now what Fan to get


---
**VetGifts By G** *September 08, 2016 15:57*

Op tested my design yesterday and I get very faint burning smell, a lots more noise and enough vibrations to move my workpeice when the fan is running. I'm thinking about putting the fan outside and running the softer dryer vent hose to it. It moves air way better though.


---
*Imported from [Google+](https://plus.google.com/113361677380464824789/posts/BJTQxGE8hek) &mdash; content and formatting may not be reliable*
