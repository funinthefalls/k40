---
layout: post
title: "I received my CW-3000 today, and decided to open it up and take a look"
date: January 03, 2019 23:24
category: "Discussion"
author: "Sean Cherven"
---
I received my CW-3000 today, and decided to open it up and take a look.  It looks a lot different/better compared to other pics I've seen of this unit.



Here are the pics:



![images/e954252b66fce24c3429507bd78bb53b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e954252b66fce24c3429507bd78bb53b.jpeg)
![images/a4b87e3a9a00eb61c4786e8a966227de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4b87e3a9a00eb61c4786e8a966227de.jpeg)
![images/0af18172fcb7048159ec6ec89a77b307.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0af18172fcb7048159ec6ec89a77b307.jpeg)
![images/9f419149c44c0467f44f0186c361e929.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9f419149c44c0467f44f0186c361e929.jpeg)
![images/2eb11b09489dabff5cc6a671e1db69d5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2eb11b09489dabff5cc6a671e1db69d5.jpeg)
![images/1eff625fac22f4811ffc35da536b5b3c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1eff625fac22f4811ffc35da536b5b3c.jpeg)
![images/3b4e483729a5fb1ebdac157b634e704a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b4e483729a5fb1ebdac157b634e704a.jpeg)

**"Sean Cherven"**

---


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/e7FEmGTqgVG) &mdash; content and formatting may not be reliable*
