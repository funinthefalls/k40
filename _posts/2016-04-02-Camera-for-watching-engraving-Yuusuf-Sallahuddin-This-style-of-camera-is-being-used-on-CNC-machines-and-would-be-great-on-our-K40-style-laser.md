---
layout: post
title: "Camera for watching engraving Yuusuf Sallahuddin This style of camera is being used on CNC machines and would be great on our K40 style laser"
date: April 02, 2016 15:37
category: "Modification"
author: "HalfNormal"
---
Camera for watching engraving



**+Yuusuf Sallahuddin** This style of camera is being used on CNC machines and would be great on our K40 style laser. Borescope Endoscope Inspection Tube Camera



[http://www.amazon.com/DBPOWER-Waterproof-Borescope-Endoscope-Inspection/dp/B01DIQ6K2O/ref=sr_1_3?ie=UTF8&qid=1459611134&sr=8-3&keywords=usb+borescope](http://www.amazon.com/DBPOWER-Waterproof-Borescope-Endoscope-Inspection/dp/B01DIQ6K2O/ref=sr_1_3?ie=UTF8&qid=1459611134&sr=8-3&keywords=usb+borescope)



They have styles for use on PC and Cell Phones. There is also a wealth of attachments like different angle mirrors to get optimum picture alignment.





**"HalfNormal"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 16:38*

That's awesome. I've just finished my mod & combining the vid files so I'm about to upload it now.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/DFkQqeHtTXN) &mdash; content and formatting may not be reliable*
