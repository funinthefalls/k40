---
layout: post
title: "This project is really coming together. Just waiting on a few more things from China and then I'll be all set"
date: May 05, 2016 01:23
category: "Modification"
author: "Venuto Woodwork"
---
This project is really coming together. Just waiting on a few more things from China and then I'll be all set. Thank you **+Vince Lee**​ for all your help so far and your custom PCB.



![images/eb29a97f92f80ae31f6b54463be6377c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb29a97f92f80ae31f6b54463be6377c.jpeg)
![images/3de66797ace455f04f8e5f54eaa45454.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3de66797ace455f04f8e5f54eaa45454.jpeg)

**"Venuto Woodwork"**

---
---
**Carl Fisher** *May 11, 2016 01:29*

I'd love to hear more about the digital readout above the level set dial. Does that relate to the position of the dial or is that just a digital version of the analog next to it?




---
*Imported from [Google+](https://plus.google.com/112529181035283151172/posts/JsgB7D8mXVF) &mdash; content and formatting may not be reliable*
