---
layout: post
title: "Peter van der Walt Arthur Wolf Has anyone integrated a LO Z axis table with a smoothie setup"
date: November 16, 2016 16:42
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
**+Peter van der Walt** **+Arthur Wolf**



Has anyone integrated a LO Z axis table with a smoothie setup. 

I have been researching this but hit a "brick wall" 



I assumed that I connect:

 

1. The stepper to M3

2. The limit switches to Zmin and Zmax

3. An UP button to ......????

.......

The smoothie /switch page suggests using 1.7 

I cannot find 1.7 anywhere?

[http://smoothieware.org/lpc1769-pin-usage](http://smoothieware.org/lpc1769-pin-usage)



"switch.zplus10.enable                     true

switch.zplus10.input_pin                  1.7

switch.zplus10.output_on_command          G91G0Z10G90      # G90 and G91 switch to relative positioning then back to absolute."

.......

4.  How to add a down button?

.............

If I use input buttons and the SWITCH config options, how does the smoothie know enough to process an input button, check z limits and move the stepper. Is all this in the firmware? 

I am still a gcode idiot but it looks to me in the above switch module when the button is pushed it moves the Z azis 10 increments? Does it know a limit is reached?



If I am using the GLCD, do I even need up and down button or do I just use the Z-jog function on the panel? If I do will it check limits if zmin and Zmax are connected?







**"Don Kleinschnitz Jr."**

---
---
**Don Kleinschnitz Jr.** *November 16, 2016 17:57*

**+Peter van der Walt** do you know where 1.7 is on the board?


---
**Don Kleinschnitz Jr.** *November 16, 2016 18:10*

**+Peter van der Walt** yes I know I searched the data sheet and the schematics and board. It is actually referenced twice on that page. Is it an error?


---
**Don Kleinschnitz Jr.** *November 17, 2016 14:05*

We sorted this out and I added the Lift Table integration to the K40-S build index. [http://donsthings.blogspot.com/2016/11/k40-s-lift-table-integration.html](http://donsthings.blogspot.com/2016/11/k40-s-lift-table-integration.html)

[donsthings.blogspot.com - K40-S Lift Table Integration](http://donsthings.blogspot.com/2016/11/k40-s-lift-table-integration.html)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/J2cyLcKCCkE) &mdash; content and formatting may not be reliable*
