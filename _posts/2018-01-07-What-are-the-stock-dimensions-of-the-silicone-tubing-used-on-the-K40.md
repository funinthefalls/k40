---
layout: post
title: "What are the stock dimensions of the silicone tubing used on the K40?"
date: January 07, 2018 21:01
category: "Modification"
author: "ThantiK"
---
What are the stock dimensions of the silicone tubing used on the K40?  Mine is a little too short for where I need to place all my water cooling, and I'd like to add some inline flow meters, etc.





**"ThantiK"**

---
---
**Anthony Bolgar** *January 07, 2018 21:37*

Not sure about a standard, but mine is 8mm in diameter. Why not just measure the one you have and match it?


---
**ThantiK** *January 07, 2018 23:33*

**+Anthony Bolgar** asked because my calipers are at work and I'm not back to work until Tuesday, it's a hour drive. :P



Would 8mm OD, 5mm ID seem about right?


---
**Anthony Bolgar** *January 07, 2018 23:44*

That is what mine is. I standardized all my lasers to 8mm/5mm tubing.




---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/Pq9GwLyHEL3) &mdash; content and formatting may not be reliable*
