---
layout: post
title: "extremely pleased with my Veneer last delivery"
date: June 14, 2016 14:57
category: "Material suppliers"
author: "Stephane Buisson"
---
extremely pleased with my Veneer last delivery.

The first sample pack come with a indentification list, each peace numeroted. great UK seller.



the other one is long large pieces pre-glued veneer. (<30cm wide fit in K40)



[http://www.ebay.co.uk/itm/142018870311](http://www.ebay.co.uk/itm/142018870311)

[http://www.ebay.co.uk/itm/261347221538](http://www.ebay.co.uk/itm/261347221538)

[http://www.ebay.co.uk/itm/261434964508](http://www.ebay.co.uk/itm/261434964508)



![images/5b77f5113bd40fcfc268ee295e07b853.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5b77f5113bd40fcfc268ee295e07b853.jpeg)
![images/8a67863798c7b26c2546c57e7e37c045.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a67863798c7b26c2546c57e7e37c045.jpeg)
![images/afcc6c16ae4d74c43c6114306c60fd16.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afcc6c16ae4d74c43c6114306c60fd16.jpeg)
![images/2456d8484435c98db01512fea4073adf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2456d8484435c98db01512fea4073adf.jpeg)
![images/590f6b990b2cd3520f03b0a2220a152d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/590f6b990b2cd3520f03b0a2220a152d.jpeg)
![images/eb4dcdfdfa7ed18c1253c6a468385677.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb4dcdfdfa7ed18c1253c6a468385677.jpeg)
![images/3422516c7ebb938953feda3c89243020.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3422516c7ebb938953feda3c89243020.jpeg)

**"Stephane Buisson"**

---
---
**Stephane Buisson** *June 14, 2016 15:00*

related to +Carl Fisher post [https://plus.google.com/105504973000570609199/posts/GiAAjVUUVHn](https://plus.google.com/105504973000570609199/posts/GiAAjVUUVHn)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 16:21*

Really nice. I look forward to seeing what you get up to with it.



For curiosity sake, is it actual wood or some kind of laminate made to look like wood?


---
**Stephane Buisson** *June 14, 2016 16:35*

real veneer wood for marqueterie, with a layer of varnish some should be really really nice.

I will not waste them and take my time before using it (after my move in Italy), pilling up some stock now. (leather, veneer, acrylic, ..., OX parts, etc...).


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/LU751cenzNL) &mdash; content and formatting may not be reliable*
