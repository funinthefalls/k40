---
layout: post
title: "ben crawford Here are some pics of the bed I made"
date: May 09, 2016 02:12
category: "Modification"
author: "Anthony Bolgar"
---
**+ben crawford** Here are some pics of the bed I made.



![images/190d3e2a9ec86b8136726a085ae3df86.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/190d3e2a9ec86b8136726a085ae3df86.jpeg)
![images/52dcbb5c6e32619da51bfe286d6b44fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52dcbb5c6e32619da51bfe286d6b44fc.jpeg)
![images/2727cd13c67e65f74bc6d673f38d0e98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2727cd13c67e65f74bc6d673f38d0e98.jpeg)
![images/131a82c048aca0d9b17bd8b904f5d69c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/131a82c048aca0d9b17bd8b904f5d69c.jpeg)

**"Anthony Bolgar"**

---
---
**3D Laser** *May 09, 2016 02:51*

How do you adjust it while it's in the machine and keep the bed level


---
**Anthony Bolgar** *May 09, 2016 02:52*

I remove the top piece to adjust the nuts. But I am going to put springs between the top and bottom, and then move the adjusting nut to the top, sort of like a 3D printer bed setup.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 02:59*

**+Anthony Bolgar** That's a great idea to add the springs. Two small line levels mounted on it would be nice too.


---
**ben crawford** *May 09, 2016 03:49*

Good starting point, I need to do some designing still. I want to maybe make it motorized using a some Motors and an arduino.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 04:03*

**+ben crawford** Check out some of the others here that have created Z-tables. If I recall correct **+Chirag Chaudhari** was working on one a while back, but not sure if he got it finalised. A few others have too (just can't remember who).


---
**Ariel Yahni (UniKpty)** *May 09, 2016 12:12*

Like the design. It's a very good idea to get it motorized since in Laserweb2 you'll be able to create a macro where it will be possible to move the bed to a distinct focal point


---
**ThantiK** *May 09, 2016 12:49*

And <i>thaaaaaank you</i>.  I haven't even looked at beds yet because I'm still having troubles with smoothieware and the dual-input laser.  This looks easy enough to build though :D


---
**Anthony Bolgar** *May 09, 2016 18:47*

Took about an hour to build and total cost was about $30.00 and does the job just fine. I always love saving money!


---
**ben crawford** *May 10, 2016 01:33*

did you 3D print some of the parts?


---
**Anthony Bolgar** *May 10, 2016 13:39*

yes


---
**ben crawford** *May 10, 2016 14:09*

I can't wait until i get my 3D printer.


---
**ben crawford** *May 10, 2016 20:01*

**+Anthony Bolgar** Could you maybe print me some captive nut style gears for my Z table? They need to grip a 10mm threaded rod and be driven by a belt so all four corners turn at the same time. I can pay for shipping and cost of materials.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/Cm2NroFkRqQ) &mdash; content and formatting may not be reliable*
