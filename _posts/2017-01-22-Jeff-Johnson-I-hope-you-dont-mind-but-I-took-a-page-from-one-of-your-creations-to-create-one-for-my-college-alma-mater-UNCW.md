---
layout: post
title: "Jeff Johnson I hope you don't mind, but I took a page from one of your creations to create one for my college alma mater (UNCW)"
date: January 22, 2017 00:11
category: "Object produced with laser"
author: "Ned Hill"
---
**+Jeff Johnson** I hope you don't mind, but I took a page from one of your creations to create one for my college alma mater (UNCW).  :)  Made from 1/4" (6mm) birch ply.

![images/693adb16a770091fda1f7562bfe3b915.png](https://gitlab.com/funinthefalls/k40/raw/master/images/693adb16a770091fda1f7562bfe3b915.png)



**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2017 00:36*

Wow, you are really making some awesome stuff Ned. This looks very cool.


---
**Ned Hill** *January 22, 2017 00:40*

Thanks +Yuusuf Sallahuddin.  First time trying something stained and with black paint on the engraved area.  I love the contrast.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2017 00:42*

**+Ned Hill** Ah, did you stain after engraving? It looks very high contrast which is what made it stand out so much for me too.


---
**Ned Hill** *January 22, 2017 00:51*

The center piece and frame were cut from the same piece of wood.  So I actually stained the whole piece of wood first, put on a coat of lacquer to seal the wood and masked.  I Then cut the pieces out and painted the engraved area on the center piece.  I found I like using a stencil paste to do color infill on engraved areas.  It's thick so less likely to bleed, has good quick coverage and it dries very fast.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2017 00:53*

**+Ned Hill** Thanks for the info. Will have to have a go at something like this in future. It looks amazing to be honest.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2017 00:58*

Just looking for stencil paste at one of my local craft stores online & I came across this in the search & thought it might be interesting to test sometime too:



[http://riotstores.com.au/item/163202-glitzee-glitter-12gm-pastel-tiffany.html](http://riotstores.com.au/item/163202-glitzee-glitter-12gm-pastel-tiffany.html)


---
**Ned Hill** *January 22, 2017 01:05*

Hmm yes that does looks like it would cool try.  Looking at that also gives me an idea that it might be interesting to try some fluorescent or phosphorescent (glow in the dark) paint coupled with a black light or maybe some UV leds.  Hmmm ideas to kick around now. :)


---
**Jim Hatch** *January 22, 2017 02:14*

Really nice. Is it two layers?


---
**Ned Hill** *January 22, 2017 02:15*

Thanks Jim, yes it is.


---
**ViciousViper79** *January 23, 2017 13:19*

Impressive, most impressive!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 23, 2017 17:51*

**+Ned Hill** Some UV paint would be cool, so it would be invisible until coupled with the black light. Could have some secret messages in the piece haha.


---
**Jeff Johnson** *January 23, 2017 20:03*

Wow that looks fantastic! Glad to have inspired you.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/1GvZhXijAdj) &mdash; content and formatting may not be reliable*
