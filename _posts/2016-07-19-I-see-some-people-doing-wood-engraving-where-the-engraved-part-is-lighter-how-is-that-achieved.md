---
layout: post
title: "I see some people doing wood engraving where the engraved part is lighter - how is that achieved?"
date: July 19, 2016 19:30
category: "Materials and settings"
author: "Tev Kaber"
---
I see some people doing wood engraving where the engraved part is lighter - how is that achieved?  Do you stain the wood, then engrave to a depth that reveals unstained wood beneath?



Example:  [https://img0.etsystatic.com/134/0/9493600/il_570xN.854222626_q1sd.jpg](https://img0.etsystatic.com/134/0/9493600/il_570xN.854222626_q1sd.jpg)







**"Tev Kaber"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 19, 2016 19:54*

In that image you shared, I'd imagine you are correct that the wood is stained & the imagery engraved afterwards. As that looks like typical plywood that has definitely been stained.


---
**Alex Krause** *July 19, 2016 20:08*

Stain... mask...engrave... paint before removing mask


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/KXzBQjy36JC) &mdash; content and formatting may not be reliable*
