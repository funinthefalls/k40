---
layout: post
title: "Cake toppers to keep the family happy!"
date: January 28, 2018 02:22
category: "Object produced with laser"
author: "Paul de Groot"
---
Cake toppers to keep the family happy! For those who want the design, I have the inkscape svg files on awesome.tech/k40 forum



![images/c8948d558ad50964cf0e9dd43bb8c999.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c8948d558ad50964cf0e9dd43bb8c999.jpeg)
![images/41e45e21b3bb6d768fab83e2c1faa9b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41e45e21b3bb6d768fab83e2c1faa9b7.jpeg)

**"Paul de Groot"**

---
---
**BEN 3D** *January 28, 2018 11:38*

What kind of material is that ?


---
**Paul de Groot** *January 28, 2018 21:16*

**+BEN 3D** just 3mm acrylic sheet.


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/7X1RFwPrf8F) &mdash; content and formatting may not be reliable*
