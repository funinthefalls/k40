---
layout: post
title: "I am just full of questions this morning ;) If you have one of the air assist laser heads, what is the correct distance from the bottom of the air assist cone to the work surface?"
date: October 23, 2016 13:47
category: "Air Assist"
author: "Anthony Bolgar"
---
I am just full of questions this morning ;) If you have one of the [lightobjects.com](http://lightobjects.com) air assist laser heads, what is the correct distance from the bottom of the air assist cone to the work surface? I want to make a focus block.





**"Anthony Bolgar"**

---
---
**greg greene** *October 23, 2016 13:59*

50.8mm from the bottom of the knurled knob that holds the head onto the plate, the bottom of the cone distance may vary if you have a led spot holder above it


---
**Anthony Bolgar** *October 23, 2016 14:00*

I wasn't sure but thought that was were to measure from. Thanks for the confirmation.


---
**Joey Fitzpatrick** *October 23, 2016 14:51*

It is approximately 24mm.  +/- 1mm


---
**Anthony Bolgar** *October 23, 2016 15:16*

Thanks **+Joey Fitzpatrick** 


---
**Don Kleinschnitz Jr.** *October 23, 2016 15:37*

Just a note: I assumed that my K40 had the 50.8 mm FL and it did not. Created alot of frustration until I ran a ramp test. 


---
**greg greene** *October 23, 2016 15:39*

That is strange - what lens did it have then?


---
**Don Kleinschnitz Jr.** *October 23, 2016 16:48*

**+greg greene** 38.1 mm


---
**greg greene** *October 23, 2016 21:59*

Ah - not a stock one then I presume - I hadn't heard on any stock ones other than 50.8 for the K40.  So at that FL - you will need to be ver precise because it will diverge quicker than one with a longer FL.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/g5gpCsqAnh4) &mdash; content and formatting may not be reliable*
