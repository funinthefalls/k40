---
layout: post
title: "Anybody know where I can get a blade bed?"
date: May 05, 2017 22:35
category: "Discussion"
author: "3D Laser"
---
Anybody know where I can get a blade bed?





**"3D Laser"**

---
---
**Andrew Nichols** *May 05, 2017 22:50*

I'm guessing you mean this? (What I use)[amazon.com - Amazon.com: 300X200 Honeycomb. Fit K40 machine: Home & Kitchen](https://www.amazon.com/dp/B00EFFOF0W/ref=cm_sw_r_cp_api_Tfqdzb7C0V2KJ)


---
**3D Laser** *May 05, 2017 23:54*

More like this I have seen them on Ali express but not from a USA seller![images/4677526b810a633c179f464e8b765cca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4677526b810a633c179f464e8b765cca.jpeg)


---
**Phillip Conroy** *May 06, 2017 00:20*

I made my own using hacksaw blades

![images/9a2ad80d0bbc4dea34584eaddfa6cdcd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a2ad80d0bbc4dea34584eaddfa6cdcd.jpeg)


---
**Mark Brown** *May 06, 2017 01:29*

I haven't tried yet, but I've got a roll of galvanized steel flashing I was thinking of making a bed out of.


---
**Martin Dillon** *May 06, 2017 04:57*

I have a similar bed for my plasma cutter.  It works better if you have a center support and spring an arc into each piece.   I was thinking of doing the same for my laser cutter only using bandsaw blades.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/EaDvyPVpQZr) &mdash; content and formatting may not be reliable*
