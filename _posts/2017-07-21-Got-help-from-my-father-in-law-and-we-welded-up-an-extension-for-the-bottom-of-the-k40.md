---
layout: post
title: "Got help from my father in law and we welded up an extension for the bottom of the k40"
date: July 21, 2017 04:24
category: "Modification"
author: "William Kearns"
---
Got help from my father in law and we welded up an extension for the bottom of the k40

![images/f7ea9cba4b9829e3e73e11f78669b417.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f7ea9cba4b9829e3e73e11f78669b417.jpeg)



**"William Kearns"**

---
---
**Anthony Bolgar** *July 21, 2017 04:43*

That will be a great addition, lots of extra room for a rotary table if you want.


---
**Arion McCartney** *July 21, 2017 05:45*

That's a great idea! Thanks for sharing


---
**ThantiK** *July 21, 2017 14:23*

What did this cost you in parts?


---
**Don Kleinschnitz Jr.** *July 21, 2017 15:38*

#K40FrameModifications


---
**William Kearns** *July 21, 2017 15:54*

**+ThantiK** it's whatever 3/4 tubing cost these were scraps I got for free


---
**Don Kleinschnitz Jr.** *July 21, 2017 20:23*

I think 36" x 3/4 tubing at HD is about $8.

BTW if you do not want to weld I found these fasteners invaluable. Just push them in the end of the tube and then fasten a cross member with 1/4-20 bolts.

[mcmaster.com - McMaster-Carr](https://www.mcmaster.com/#94290a510/=18ljbyg)


---
**William Kearns** *July 24, 2017 02:25*

![images/1b59af1afb61a5ba1a0d0c9a8230f63e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b59af1afb61a5ba1a0d0c9a8230f63e.jpeg)


---
**William Kearns** *July 24, 2017 02:26*

It's mobile now![images/84cf98e540f1ce590b312b7235c4467a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/84cf98e540f1ce590b312b7235c4467a.jpeg)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/e6xgfa57SjJ) &mdash; content and formatting may not be reliable*
