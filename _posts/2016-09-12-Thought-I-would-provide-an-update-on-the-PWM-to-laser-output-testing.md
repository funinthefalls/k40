---
layout: post
title: "Thought I would provide an update on the PWM to laser output testing"
date: September 12, 2016 15:57
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Thought I would provide an update on the PWM to laser output testing. 

As you know I embarked on a task to measure the K40 dynamically at the lasers output. 



[http://donsthings.blogspot.com/2016/07/dynamic-laser-response-testing.html](http://donsthings.blogspot.com/2016/07/dynamic-laser-response-testing.html)



I built a PWM generator and that works fine as an adjustable source.



I also built a laser output sensor unit that used standard IR sensors like used in remotes.



As you know CO2 lasers operate at 10600 nm the high end of the IR spectrum. I had foolishly hoped that standard semiconductor optical devices (like those in remotes) would have SOME output at this wavelength and if I got the laser power attenuated,  I would have a cheap way of measuring the light dynamics.  



Nope.... no output at the CO2 wavelength. Although one can purchase a lab sensor at this wavelength for lots of money nothing in the available semiconductor technologies have any response this high up.



I haven't given up. I am now experimenting with some pyroelectric devices which I am not confident will have fast enough response, we will see.



Figured you were wondering what was taking so long :).



Suggestions are always welcome.







**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 12, 2016 17:04*

I can't say I understand most of what is in your article, but it is well written & presented. Glad to see someone doing some solid scientific analysis of the issues for the PWM. Keep up the good work Don :)


---
**Mircea Russu** *September 12, 2016 19:30*

I currently ditched the 2 pin setup in favor of the 1 pin, pwm at the L pin. Why? Because at 21-23 deg Celsius my laser won't fire under 5ma. At 4ma it fires but LPS screams weirdly. So I can safely set the pot at 5ma and use 50% power in LPS and guess what, I get lower output power, like 2.5ma, better for engraving at normal speeds which wont shake the machine apart. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/ZuFkfSzz96u) &mdash; content and formatting may not be reliable*
