---
layout: post
title: "Z table. Mesh or honeycomb? Adjustable Z table: manual or motorized?"
date: February 28, 2018 21:41
category: "Discussion"
author: "James Rivera"
---
Z table. Mesh or honeycomb? Adjustable Z table: manual or motorized? What do you use?





**"James Rivera"**

---
---
**Don Kleinschnitz Jr.** *February 28, 2018 22:05*

I had mesh, went to LO table. Manual clamp table waiting for a build.


---
**Anthony Bolgar** *February 28, 2018 22:25*

Honeycomb. Manual on my K40, powered on my 50 and 60W lasers.


---
**Phillip Conroy** *March 01, 2018 00:56*

Halfblade and half pin ,3 years so far and small stuff add metal fly wire on top,sitting on 3mm steel plate .all removed and sprayed with liquid oven cleaner weekly![images/06d2f7c97015f41cf96935b359f74452.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/06d2f7c97015f41cf96935b359f74452.jpeg)


---
**BEN 3D** *March 01, 2018 01:18*

I have currently a manual z table, but it is just a workarround. The top is a metal plate with rectangular holes. I have a honeycomb that I lay on top and tape it if needed. I want a motorised one as I had seen it in hackaday, I already cut a hole to the button of my laser to  have more place for a higher z table. To engrave bigger things  like a chest.



Cutting hole Video


{% include youtubePlayer.html id="dCD0nsWNDhk" %}
[https://youtu.be/dCD0nsWNDhk](https://youtu.be/dCD0nsWNDhk)[hackaday.io - Hackaday.io](http://hackaday.io)


---
**Ned Hill** *March 01, 2018 02:55*

Manual Z table with a primary honeycomb bed and a secondary steel mesh bed.  The steel mesh offers lower a profile and allows for locking things down with magnets, but sucks for cutting.


---
**Joel Brondos** *March 07, 2018 23:21*

Has anyone tried a "wire" bed, i.e. winding wire back and forth between posts across the workspace? If one were to try that, I wonder what type of wire would stand up well to the laser. 


---
**Ned Hill** *March 07, 2018 23:40*

**+Joel Brondos** I haven’t tied it but I I have seen people using those types of beds. Pretty much any type of metal wire will hold up against these lasers. 


---
**Don Kleinschnitz Jr.** *March 08, 2018 14:40*

**+Joel Brondos** I would guess that most any wire would likely work ok, as your not going to get much burn on metal at this power. 

I think the challenge would be to get the wire level and tight. 



Another approach is to make a clamping bed that holds the edges. Another user came up with this idea and built a proto. This design would work for all but materials that cannot be clamped. I like the idea because its focal point is fixed for all materials. For engraving its stable for cutting the pieces fall to the bottom. I mostly do engraving and cutting so this is a simple approach that requires no adjustment other than at install.



Its in my queue to build and I have a SU design that is untested and parts laying under my desk ;).



[donsthings.blogspot.com - K40 Clamping Table](http://donsthings.blogspot.com/2017/02/k40-clamping-table.html)




---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/bhY11YWhe24) &mdash; content and formatting may not be reliable*
