---
layout: post
title: "Hi All. Im trying to do a fairly large and complex cut with my laser, and I want to do it in pieces"
date: October 06, 2016 22:24
category: "Hardware and Laser settings"
author: "Bob Damato"
---
Hi All.  Im trying to do a fairly large and complex cut with my laser, and I want to do it in pieces. Total size is 9x11 but I want to do sections at a time.



When I try that, the first section cuts great. The second section cuts but it is no where close to being aligned with the first. Ive tried several settings in corellaser to no avail. Im doing refer as center. I tried  others but it made it worse.



Any suggestions much appreciated.

Bob





**"Bob Damato"**

---
---
**Ariel Yahni (UniKpty)** *October 06, 2016 23:33*

Can you post an image of the whole thing?  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 07, 2016 03:29*

I would suggest creating a no-border no-fill rectangle around each section. All refer off the top-left.

![images/3bcadd1a371a1b86866cb113fd7aeba0.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3bcadd1a371a1b86866cb113fd7aeba0.png)


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/31Avc9VKhxc) &mdash; content and formatting may not be reliable*
