---
layout: post
title: "I did my crazy rivet pin bet to get less reflections but it seems the laser pops off all the shims and rivet's bases and scores the back of my material"
date: July 07, 2016 07:46
category: "Discussion"
author: "Mircea Russu"
---
I did my crazy rivet pin bet to get less reflections but it seems the laser pops off all the shims and rivet's bases and scores the back of my material. Disturbing especially when using transparent acrylic. Now what can I paint it with? Or what can I add to the bottom? I've thought about a soft material which would allow the pins to push through and not require drilling again every hole. What about some mineral/basaltic wool? Or some resistant painting maybe?

![images/e5843898e961ff072ef7ea964425af17.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e5843898e961ff072ef7ea964425af17.jpeg)



**"Mircea Russu"**

---
---
**Phillip Conroy** *July 07, 2016 08:31*

What a lot of work you have put in to it .i would try hi temp car paint -the stuff people spray onto brakes ,the laser beam temp when it has defocased at the distance to the bottom of the rivit be within thats paint range.i usd a removable tin tray with 12 magnets with screws on top to hold my work up and just move them around ,using min number to hold work,  [http://www.ebay.com.au/itm/Eastwood-Aerosol-Hi-Temp-Factory-Gray-spray-can-paint-headers-manifold-cast-iron-/221866200315?hash=item33a8418cfb:g:orEAAOSwjVVV395I](http://www.ebay.com.au/itm/Eastwood-Aerosol-Hi-Temp-Factory-Gray-spray-can-paint-headers-manifold-cast-iron-/221866200315?hash=item33a8418cfb:g:orEAAOSwjVVV395I)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 07, 2016 10:48*

If the bed/rivets are stainless steel, you could try a can of dry moly on it. The moly will bond to it & may reduce reflections.


---
**Michael Audette** *July 07, 2016 11:11*

black stove paint maybe.


---
**Jeremie Francois** *July 07, 2016 11:11*

May be bbq spray paint (though what I bought was not up to my expectations). Anyhow thanks for letting us know how it went!


---
**Mircea Russu** *July 07, 2016 12:18*

I shall try and find stove/brake paint in my country. I bought today a large piece of acrylic 1m/1m(3ft/3ft) 3mm(1/8")  for only $30. Plan to cut it in smaller pieces and run some tests. Will keep you posted.


---
**Brian W.H. Phillips** *July 07, 2016 15:28*

I know you have gone to a lot of trouble to get  that, but the best method I found by trial and error is a cheap punched plate (the type that looks like expanded netting) and fit it with Punk studs (these are available on Ebay), It works almost 100% against flash back and reflections, the studs are cone shape so the beam is deflected, and the many holes in the expanded plate let the beam pass straight through. I've been using this method now for many hours of cuting and have never had a burn mark on the underside: For the small cost of buiding the new base I would recomend it!




---
**Jeremie Francois** *July 07, 2016 16:34*

**+Brian W.H. Phillips** "Punk studs" this is very smart ! :D


---
**HP Persson** *July 07, 2016 16:38*

I did three different beds in acrylic with regular black nails in them (different spacing, so i can choose upon type of work).

See image here -> [http://wopr.nu/laser/spikebed.jpg](http://wopr.nu/laser/spikebed.jpg)

No problems at all with reflections, and the beam hitting bottom doesnt reflect, just make it look bad (not the work piece) :)



If you have the hole pattern ready, do a acrylic piece to add onto that metal bed covering the rivets.


---
**Michael Audette** *July 07, 2016 16:41*

**+HP Persson** - I like the fan mount there for air assist.  How well does it work?  Are there cad files/stl/better pictures of that setup? <Sorry for the hijack>


---
**HP Persson** *July 07, 2016 16:52*

**+Michael Audette** That little 40mm fan is to help moving the smoke away from the piece, there is also another assist for keeping the cut clean. 

Helps alot! 



I can fix some CAD files if you want, it´s my own creation to get a adjustable head, instead of moving the bed i move the lens ;)

Made from 2/3mm acrylic, but can probably be 3D printed too.



Short vid here -> 
{% include youtubePlayer.html id="5Id7vQNlNAo" %}
[https://www.youtube.com/watch?v=5Id7vQNlNAo](https://www.youtube.com/watch?v=5Id7vQNlNAo)


---
**Mircea Russu** *July 07, 2016 19:36*

Thanks for all your kind advices. I've literally blasted through some 4mm plywood and 3mm acrylic at 8ma 8mm/s today to make an exhaust adapter for my 6" duct and some test pieces. Quite some power this little machine has. I haven't found the stove paint so I think I have to go to another store to search or order  it online.

+Brian I won't change this bed soon, my hand still hates me :)

**+HP Persson** If I can't find any painting/powdering to reduce the reflections I might cover it with some acrylic, but first some mineral wool, that thing won't burn even torched.


---
**Brandon Smith** *July 08, 2016 01:13*

Beautiful work. I am experimenting with both a screw based board and a blade base board right now (blade based board will be a nice low cost solution to share with community).



I wanted to comment on the thought of using acrylic. One of the people I follow on youtube that has the 50w laser like mine tried doing an acrylic based screw board, but still found that the laser effected the acrylic too much, and ended up going with a different solution.


---
**Craig** *August 24, 2016 09:33*

I always mask the back of my acrylic when cutting


---
**Mircea Russu** *August 24, 2016 10:41*

What do you use for masking?


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/Ysdo2SD8Uf5) &mdash; content and formatting may not be reliable*
