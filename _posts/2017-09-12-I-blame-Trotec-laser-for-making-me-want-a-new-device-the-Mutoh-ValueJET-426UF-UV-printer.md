---
layout: post
title: "I blame Trotec laser for making me want a new device, the Mutoh ValueJET 426UF UV printer"
date: September 12, 2017 05:40
category: "Discussion"
author: "Anthony Bolgar"
---
I blame Trotec laser for making me want a new device, the Mutoh ValueJET 426UF UV printer. It can print on just about anything, like back printing clear acrylic with full color, then it can cover over that with white, or any other color. They have a vid of making a jigsaw puzzle this way , printed on 6mm acrylic then laser cut into pieces. I want one. Any one have $20K to lend me?





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *September 12, 2017 13:18*

Is this a laser device? It looks like an ink jet?

Is this Trotec or Mutoch?




{% include youtubePlayer.html id="CYCAgmrYUfQ" %}
[youtube.com - Mutoh's ValueJet 426UF UV-LED Printer](https://www.youtube.com/watch?v=CYCAgmrYUfQ)


---
**Anthony Bolgar** *September 12, 2017 13:38*

Trotec Canada sells it, it is a UV printer.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/emKNiuZT2ZS) &mdash; content and formatting may not be reliable*
