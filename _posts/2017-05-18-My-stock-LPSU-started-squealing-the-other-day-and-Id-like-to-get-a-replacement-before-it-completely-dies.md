---
layout: post
title: "My (stock) LPSU started squealing the other day and I'd like to get a replacement before it completely dies"
date: May 18, 2017 18:33
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
My (stock) LPSU started squealing the other day and I'd like to get a replacement before it completely dies. It's a blue/white K40 however I wonder if getting a larger LPSU (for potential future upgrade to a 50 or 60W tube) is worth doing (without causing problems with the current 40W (35W?) tube). Are there recommendations on specific ones to get? **+Don Kleinschnitz**, I would appreciate any input you may have on this. If I'm not mistaken, you've spent a considerable amount of time reverse engineering one of those LPSUs.





**"Ashley M. Kirchner [Norym]"**

---
---
**Steve Clark** *May 19, 2017 03:01*

I sure this is redundant but have you change your coolant and do you use distilled water? Before I went out and got a new tube I would flush and refresh the coolant. No additives just distilled water.


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 03:07*

Yup and yup. I flush it out at least every other week. May sound a bit much but then distilled water is cheaper than a new tube.


---
**Don Kleinschnitz Jr.** *May 19, 2017 12:05*

**+Ashley M. Kirchner** 

Yes with $ help from the community we have an excellent handle on theoretically what is in these supplies. I am in the mode of testing and repairing a couple to better learn how theory related to reality. Lots of projects stacked up in my maker shed so its slow going.



For now, I recommend getting whatever supply is the cheapest that is plug compatible on amazon or ebay. 

It seems that the tube and the LPS in these things are considered "consumables" so they are relatively cheap. 



How old is the tube?

..........................

I have wondered about getting a 50W supply so that I can upgrade to 50W tube later. I have not gotten a good answer as to how reliable running a 40W tube with a 50W supply.

A 50W supply will work but I am not sure if it will shorten the life of the tube do to higher than normal voltage operation when run with a 40W tube. 

I am pretty sure, but not verified, that 50W machines have the same supply but with a 50W flyback. 

My strategy is that "when" my tube goes out I am getting a 50W, extend the cabinet, and get a 50W supply. Then retrofit the 40W supply with a 50W flyback for a spare.



I do not have a 50W machine nor supply so I cannot test any of this theory but I can work with you on whatever you decide.


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 17:47*

Aha, thanks for that explanation Don. I didn't think about the higher voltage. After having looked on the web, I did find a few forums where folks were saying that running a tube with a higher wattage PSU will work, however it will also shorten the tube's life the larger the difference in wattage is. Something like a 10W difference would be ok but going larger is not recommended. So I will stick with a 40W LPSU for now till I'm ready to get a larger tube. I'll look on Amazon/eBay for options. I got my machine just over a year and a half ago, it probably has about 100 hours on it, IF that. Unfortunately it doesn't have a manufacturing date on it so I don't know how old it truly is.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/G81Jjw5htEY) &mdash; content and formatting may not be reliable*
