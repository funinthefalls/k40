---
layout: post
title: "I am contemplating using this for fire detection in my laser cutters: It would trigger a quick dump valve t on a paintball CO2 tank that would flood the cutting chamber with CO2, effectively starving the fire of oxygen"
date: April 18, 2018 11:45
category: "Discussion"
author: "Anthony Bolgar"
---
I am contemplating using this for fire detection in my laser cutters:



[https://www.ebay.ca/itm/292194171030](https://www.ebay.ca/itm/292194171030)



It would trigger a quick dump valve t on a paintball CO2 tank that would flood the cutting chamber with CO2,  effectively starving the fire of oxygen. My only concern is how fast the thermocouple reacts to temperature changes, it is useless if it takes too long to heat up and trigger, the damage would already be done (but I guess it would help to mitigate any damage to the structure and contents, just not protecting the laser). I think I will purchase it and give it a try as it is under $20, and I can always use the 40A SSR for another project if it does not function like I want it to. I'll keep everyone posted as to the results.





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *April 18, 2018 12:07*

I put a PID sensor  in for the cabinet mounted on the head with its controller in series with the laser enable to shut down the laser.

Have no way of telling if this would detect heat quick enough, especially if a fire starts at one end and the head is over cutting at the other. :(.

My view is the same, something is better than nothing. 



I want to test a PIR sensor looking at the entire cut area. Not sure if the laser would light that off tho.... perhaps a time filter where it would not light off unless it sees a high value for a longer time. More ideas I have no time to test.



How do you plan on setting up the CO2 tank....




---
**Andy Shilling** *April 18, 2018 12:15*

Could you not run say a week's worth of jobs and track an average cabinet temperature, then based off these results set the detection 10° higher to narrow down the criteria. I'd rather it go if to often than not in time once. Co2 isn't going to damage anything after all.


---
**Anthony Bolgar** *April 18, 2018 15:02*

But at $40 a tank of CO2 it gets expensive fast. (Paintball canister at 800 PSI)


---
**Jim Fong** *April 18, 2018 17:30*

Thermocouple’s react really fast but that doesn’t mean the PID controller does.  It takes average readings over a programable set amount of time, atleast the one I have does. It’s a little slow. 



I use max6675 k type sensors boards connected to Arduino when I need to collect temp data. You can read several thousand samples per second if you need to. 



On my 3d printer I used a spare Dallas 18b20 temp sensor placed above the unit.  Right now it just sends me a text message if the temp is above 90F.  I have to hook up a AC power cutoff switch to it.  I only operate the printer when I’m home plus a smoke detector above unit. And IP camera.  



I was thinking of using several max6675 and placing thermocouples around the laser.  The Arduino will fire off a relay to turn of AC power and loud alarm.  I’m usually only a few feet away when operating the laser.  I can then quickly grab the fire extinguisher. I have several of them in my workshop.  I want to shutoff AC power to exhaust fan/air pump too since I don’t want that to feed any flames.  


---
**Phillip Conroy** *April 18, 2018 19:22*

So far for the fire alarm I have 3 thermal bimetal 45 deg c sensor that unfortunately take to long to trigger hooked up to pwr full time and 120 db external siren. 

This will be my backup system.

Also have temperature controler with sensor in the metal extraction pipe to monitor extract ion temps ,this controler is set to alarm 5 deg above rooms air con temp hooked up to buzzer.

Current problem is the temp controler is made for heating and cooling systems and the sample rate is to high,ie takes to long to read a jump in temperature. 

To overcome this issue I now have a rex c100 pid temperature controler with a 0.5 second sample rate.

I just need time to fit this pid control er as flat out cutting for x mass

When I get the time thinking of writing a insrtuctable and posting to most laser forums.

Their are far to many fires started by thses laser machines

Forgot to add also will be fitting a cutout for laser beam and alarm if air flow stops when cutting or engraving


---
**Mike R** *April 20, 2018 18:13*

[instructables.com - Arduino Modules - Flame Sensor](https://www.google.co.uk/amp/www.instructables.com/id/Arduino-Modules-Flame-Sensor/%3famp_page=true)





Came across these by accident, don't know if it would pick up laser as a fire 🌡️


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/7v7FPL9XBVJ) &mdash; content and formatting may not be reliable*
