---
layout: post
title: "Note to self...don't use basswood to 3d carve....after 3 hours of lasing i went to clean it in the sink...it split in half"
date: October 23, 2016 12:20
category: "Object produced with laser"
author: "Scott Thorne"
---
Note to self...don't use basswood to 3d carve....after 3 hours of lasing i went to clean it in the sink...it split in half. 





**"Scott Thorne"**

---
---
**Anthony Bolgar** *October 23, 2016 13:29*

That sucks! Did you see the link I posted for the program to generate the grayscale files need for carving?


---
**Scott Thorne** *October 23, 2016 13:55*

No...what is it? 


---
**Anthony Bolgar** *October 23, 2016 13:59*

Takes a photo and makes the type of greyscale file you have been using to carve/engrave with your 50W


---
**Anthony Bolgar** *October 23, 2016 14:01*

I tagged you in the post.


---
**Scott Thorne** *October 23, 2016 14:04*

Cool...I'll check it out 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/ZvmqD4tREGx) &mdash; content and formatting may not be reliable*
