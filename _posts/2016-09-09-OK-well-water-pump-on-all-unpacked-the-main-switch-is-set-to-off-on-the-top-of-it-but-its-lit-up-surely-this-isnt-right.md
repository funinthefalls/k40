---
layout: post
title: "OK well water pump on all unpacked the main switch is set to off on the top of it, but its lit up, surely this isnt right?"
date: September 09, 2016 16:48
category: "Hardware and Laser settings"
author: "J DS"
---
OK well water pump on all unpacked the main switch is set to off on the top of it, but its lit up, surely this isnt right?



Ive just done this video. 


{% include youtubePlayer.html id="ueEiVVE5xdo" %}
[https://www.youtube.com/watch?v=ueEiVVE5xdo](https://www.youtube.com/watch?v=ueEiVVE5xdo)

  







**"J DS"**

---
---
**Scott Marshall** *September 09, 2016 17:05*

Surely not. Check the wiring underneath, it's common for the factory wiring to "fall off" during transit. While your are inspecting, check the ground at the IEC connector at the rear, the ground is on with a quick slide connector, and very commonly comes off. It creates a serious shock hazard.



On the power switch, you should have 2 red wires on one side and 2 others (usually blue, but colors don't mean much to k40 folks) on the opposite side.



I wouldn't be surprised if the switch is in backwards (off to right), if so, just flip it.


---
**J DS** *September 09, 2016 17:55*

Thanks Scott, Ive just made a short video of the set up 
{% include youtubePlayer.html id="ueEiVVE5xdo" %}
[youtube.com - K40 problem](https://www.youtube.com/watch?v=ueEiVVE5xdo)




---
**Scott Marshall** *September 09, 2016 18:36*

I couldn't see well in the video, but it looks as though the power switch is operating (did the light go off when you flipped it to the left?), but you won't get any tube drive until you get the software setup and communicating. You can fire the laser by powering on and pressing the "TEST" button, but it will only burn a hole. 



Read the archives on the group and you'll find a bunch of startup procedures. Once the software is loaded (use the  Laserdrw to get started, then step up to the Corel install etc) You will have to enter your board serial number in the Laserdrw setup panel before it will take effect.



It's a learning curve for sure, and the factory info is useless, but it's doable, everybody here has been through it.



 If you're line power is locked on, it's got to be the line switch wired wrong. The mains (from the rear connection) should run to the center 2 terminals of the switch, and the other 2 terminals should go to the power supply's input terminals (left hand 4 place connector, pins 3 & 4)





If you can put up a photo of the underside of the panel, I may be able to tell you more.


---
**Joe Alexander** *September 12, 2016 05:02*

I had this same issue, swap the incoming AC wires on the bottom of the switch. The incoming feed is on the wrong side, thus illuminating the bulb constantly.


---
*Imported from [Google+](https://plus.google.com/102322879087562424255/posts/SsLqjN7HbgQ) &mdash; content and formatting may not be reliable*
