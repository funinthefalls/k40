---
layout: post
title: "After my C3Dmini dying on me what options are there for a replacement board?"
date: April 07, 2018 16:05
category: "Discussion"
author: "Andy Shilling"
---
After my C3Dmini dying on me what options are there for a replacement board?

I'm not sure whether to go with another c3d or go straight smoothieboard, also I understand people are using Ramps boards as well now.



As I have started to use GRBL recently what board supports that best? or do I go back to the smoothie firmware with lightburn?



any input welcome.





**"Andy Shilling"**

---
---
**Anthony Bolgar** *April 07, 2018 16:28*

Gerbil - A drop in GRBL shield that has enhanced timers allowing for higher resolution images. Available here: [awesome.tech - Buy Gerbil](http://awesome.tech/buy-gerbil/)


---
**Anthony Bolgar** *April 07, 2018 16:30*

But I still like the C3D boards, not sure why yours died, I have 5 minis and a remix and they all work great. For ease of use getting back up and running, I would just put another mini in. Just my 2 cents, YMMV.


---
**Andy Shilling** *April 07, 2018 16:40*

**+Anthony Bolgar** I like the ease of the mini but it has never been 100% for me. original problems of disconnection on my mac and then the skipping when running rasters on larger files with laserweb and now this. I went over to grbl because that stopped the skipping and seemed to work fine hence the question about a new board.



My problem really is the time it takes to come from the US to UK so I'm looking for a quicker solution really. I'll look at the GRBL shield though. thank you


---
**Anthony Bolgar** *April 07, 2018 16:45*

OK, the shield is Gerbil.....different from a GRBL shield (well the same, but better)




---
**Andy Shilling** *April 09, 2018 19:01*

I decided to go with the Gerbil so **+Paul De Groot** expect some questions being flung your way.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/SCyMgY1oEqV) &mdash; content and formatting may not be reliable*
