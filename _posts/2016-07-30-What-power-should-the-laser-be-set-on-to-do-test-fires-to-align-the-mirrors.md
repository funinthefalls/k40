---
layout: post
title: "What power should the laser be set on to do test fires to align the mirrors ?"
date: July 30, 2016 23:31
category: "Discussion"
author: "Robert Selvey"
---
What power should the laser be set on to do test fires to align the mirrors ?





**"Robert Selvey"**

---
---
**Jim Hatch** *July 30, 2016 23:47*

just until it lases works for me. About 4 or 5 ma. And it's a quick tap of the button too. Don't need a big spot to get it aligned.


---
**Robert Selvey** *July 30, 2016 23:53*

on the digital display what is considered 4 or 5 ma 00.5 ?


---
**Jim Hatch** *July 31, 2016 00:00*

Good question. If it's % of power it shows then it's 15%. Otherwise no clue on this side since I've got the analog version. Just sneak up on it by increasing it a bit at a time until you get a laser shot. Do it on the first mirror so there's no chance it's firing but way off somewhere and not hitting the mirror you're checking.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 31, 2016 02:13*

As low as possible, usually around 4mA on mine. Just enough to mark the paper/tape. Unfortunately I don't know the % side as I've also got the analogue meter.


---
**Ned Hill** *July 31, 2016 11:47*

I use the minimum needed for the tube to fire, which for me is 10% which is about 4mA.


---
**Robert Selvey** *July 31, 2016 20:23*

mine has a digital display and I have to put it on 12.0 just to get it to fire


---
**I Laser** *August 01, 2016 00:32*

I think mine fires about 12.5% (just over 4mA), if in doubt just start at 5% and work your way up.


---
**Robert Selvey** *August 01, 2016 00:56*

I'm new to laser I just got it setup yesterday and pulled out the bed changed out the head to air assist with a Focus: 50.8mm not sure what level the bed should be at for it I think 2" away from the head ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 01:16*

**+Robert Selvey** 2" away from the base of the lens.


---
**Robi Akerley-McKee** *August 08, 2016 06:16*

I use as little as possible.. just enough power for it to laser and mark the drafting tape I use for a target. Drafting tape is a low residue tape and I DON'T stick it to the lens, just in front.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/jVj6DD7aQNr) &mdash; content and formatting may not be reliable*
