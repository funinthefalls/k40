---
layout: post
title: "Please can anyone offer some advice? My laser is fantastic, standard apart from cutting table"
date: July 24, 2015 16:51
category: "Software"
author: "grahame dale"
---
Please can anyone offer some advice? My laser is fantastic, standard apart from cutting table. But just recently when engraving a project by the time it gets to the right or bottom the picture is out of alignment, the below was cut with Sino at the top and flying dino at bottom, the design is good but when etching the frying Sino is out of alignment . Please any help welcome. 

![images/1f3cfe0634a031fb041fd0eee9a80e4d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f3cfe0634a031fb041fd0eee9a80e4d.jpeg)



**"grahame dale"**

---
---
**Jason Barnett** *July 24, 2015 19:40*

This has to be a software issue. If it were a mechanical or calibration problem, the raster and cut would have the same issues, thus not be noticeable or there would be a hard shift in one or the other. This appears to be a gradual drift, like the cut and raster scaling are not the same.



What laser cutter and software are you using?


---
**grahame dale** *July 24, 2015 20:02*

Its a standard K40 with Coral Laser on Coral x7, i have a spare set up laptop so will try that out tomorrow :) 


---
**Joey Fitzpatrick** *July 25, 2015 02:11*

There was a previous post with a similar issue.  There appears to be a bug with the software.  The solution was to place a 1mm object in the top left corner in all layers.  This seems to help the software align the cut and engrave layers. Here is a link to the post. [https://plus.google.com/112890511120063209880/posts/Z2BhNJ6gkko](https://plus.google.com/112890511120063209880/posts/Z2BhNJ6gkko)﻿  I am not sure if this applies to your particular setup or not. It may only work for moshidraw boards but it may be worth a try. 


---
**grahame dale** *July 25, 2015 03:46*

**+Joey Fitzpatrick**   thank you unfortunately not the same issue, I did take the top from that thread and always use a 1mm square in the top left of all layers, I will try a diferent laptop tomorrow and see what happens. 


---
**David Wakely** *July 25, 2015 09:15*

In Corel laser click on the settings cog and change the cutting data from HGPL to WMF. Also check that your engraving data is WMF. This should fix your problem.


---
**grahame dale** *July 26, 2015 20:26*

**+David Wakely** i can see how this works and it does (Kind of) but when it cuts it cuts extra lines that arent in my design, i did get results setting them both as Jpeg but cuts everything twice :( i am feeling a lot more positive Thank you. 


---
**David Wakely** *July 27, 2015 07:25*

Are your objects solid fill in corel draw? if they are not it will cut twice when using WMF.



Check out my post at LO



[http://www.lightobject.info/viewtopic.php?f=70&t=2588](http://www.lightobject.info/viewtopic.php?f=70&t=2588)


---
**I Laser** *August 01, 2015 05:43*

That's interesting, never solid filled, I just make sure all lines are 0.7mm or lower otherwise the machine would cut twice. Stupidly, the hairline setting in Corel is over that, hence the double cuts.


---
*Imported from [Google+](https://plus.google.com/110693493789999414301/posts/8X5SL2x3sPG) &mdash; content and formatting may not be reliable*
