---
layout: post
title: "Scott Marshall , this came from MakeCNC, then I threw my own spin on it"
date: July 31, 2016 16:33
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
**+Scott Marshall**, this came from MakeCNC, then I threw my own spin on it. Several modifications were done to the design, including the removal of all the dowels and changing the wheel cover designs so it all rotates, without you seeing the static axles sticking through. I wanted something that can be made completely on a laser. And I'm still not done modifying it ... I have some more design changes to make. Bucket got stretched a bit so it doesn't look "squished", wheels spin (independently), cab swivels, cab door opens, bucket arm hinges in the middle, bucket moves, bucket doors open... I forgot what else.



![images/591996e81d4def80abe4d78cba9a9664.png](https://gitlab.com/funinthefalls/k40/raw/master/images/591996e81d4def80abe4d78cba9a9664.png)
![images/5300ed889427c01cbfc37fb2259f3850.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5300ed889427c01cbfc37fb2259f3850.png)

**"Ashley M. Kirchner [Norym]"**

---
---
**Ashley M. Kirchner [Norym]** *July 31, 2016 16:36*

I'm still debating adding the front bumper and hook, but ten I also need to rework the back to add the receiving end of those, and right now I'm liking the ladder on the back. Meh, we'll see. Maybe I'll make two different versions.


---
**Ashley M. Kirchner [Norym]** *July 31, 2016 16:37*

Oh and I'm not happy with the center swivel between the cab and back end ... too many rings. Same with the hinge on the arm ...


---
**Scott Marshall** *July 31, 2016 16:39*

Wow. Quite the undertaking.

I thought maybe a clock or something. Looks like a great project. You don't see a lot of scrapers modeled.



My boys would have loved it when they were younger. It will probably be rugged enough for kids to play with, built from plywood.



Very cool!

Looking forward to seeing the finished article.



Scott


---
**Ashley M. Kirchner [Norym]** *July 31, 2016 16:45*

At the moment it's weighing in at 257 individual pieces ... those dang rings take up a large chunk of that. That's why I want to redesign those parts.


---
**Scott Marshall** *July 31, 2016 17:08*

That's a lot of plywood, and a lot of laser time. It's going to weigh 20lbs!


---
**Ashley M. Kirchner [Norym]** *July 31, 2016 17:53*

Maybe not 20lbs, but it will certainly have some weight. Before modify it, it measured about 16" front to back. It may be closer to 18" now. But since I'm still making changes, it may change. 


---
**greg greene** *July 31, 2016 19:13*

Gorgeous! Are you doing this for commercial reasons or will the files be avail for folks to play with?


---
**Ashley M. Kirchner [Norym]** *July 31, 2016 20:07*

**+greg greene**, the original pattern can be obtained here: [http://www.makecnc.com/the-scraper.php](http://www.makecnc.com/the-scraper.php)



I'm not sure yet what I will do with my modified version.


---
**Scott Marshall** *July 31, 2016 22:19*

I would think you would be able to sell a few, especially all painted up.  I don't know as it would be strictly profitable when you consider the work that goes into one. 



I get it, You're doing it because you like it, it's there and a challenge.


---
**Ashley M. Kirchner [Norym]** *August 01, 2016 00:40*

The first one will always take the most time, testing the pieces, see where there are changes to be made, improvements, what works, what doesn't. But once the design is finalized, making additional ones will be much easier and faster. 


---
**Ashley M. Kirchner [Norym]** *August 01, 2016 02:11*

Also ... adding all of the little nodes in each slot ... very time consuming.


---
**3D Laser** *August 01, 2016 13:59*

**+Ashley M. Kirchner** quick question about your ply I am finding that the brand I am using at the moment kencraft I get spots that the laser will not get through how uniformly does this ply cut 


---
**Ashley M. Kirchner [Norym]** *August 02, 2016 01:44*

**+Corey Budwine**, what you are experiencing happens with any plywood, it's the voids in the wood. Lower quality wood will have voids in them. Manufacturers will put the better side on the outside face and the voids on the inside. Glue will pool up in those voids and you end up with some areas that are hard to cut through. Or you will find biscuits on the outside face (those oval shapes you sometimes see as an inlay in the wood). The ply I get from Ocooch is by far the best I've ever gotten from other vendors, it's clean, smooth, and I get very, very few knots, biscuits, or even those hard to cut through spots. And even when I do encounter one, it's a small spot. I used to order wood from Woodworker's Source ... their Baltic Birch is rough, darker, lots of knots and biscuits on almost every single piece ... you get what you pay for. Per square foot, Ocooch is slightly more ($1.00 versus $0.67), but it's worth it.


---
**Ashley M. Kirchner [Norym]** *August 02, 2016 03:20*

And because I can't leave "Good Enough" alone, while sitting in a conference today, I thought of a different way to do the bottom bucket door design. The MakeCNC plans call for two "barn door" style doors that open just above the scoop. In reality, those machines have a door, or gate, that gets lifted up to open the scoop so dirt get in the bucket.  In my head I have it figured out ... now i need to put it "on paper" so to speak. But first ... need to update AutoDesk to 2017, all 40-some odd GB worth of it. Wheee ....


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/U2CdxeDTCLn) &mdash; content and formatting may not be reliable*
