---
layout: post
title: "Vector Engraved Leather Wristband 1 pass @ 4mA @ 50mm/s - Orange 200gsm cardstock test cut 1 pass @ 4mA @ 20mm/s - Leather (~4-5mm) Vector Engrave - 0.5mm rectangles, 0.25mm spacing 3 pass @ 10mA @ 20mm/s - Leather Edge Cut"
date: May 01, 2016 09:43
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Vector Engraved Leather Wristband



1 pass @ 4mA @ 50mm/s	- Orange 200gsm cardstock test cut



1 pass @ 4mA @ 20mm/s	- Leather (~4-5mm) Vector Engrave

			- 0.5mm rectangles, 0.25mm spacing



3 pass @ 10mA @ 20mm/s	- Leather Edge Cut (accidentally had it on hairline border thickness)

10 pass @ 4mA @ 20mm/s	- Leather Edge Cut (fixed to 0.01pt border thickness)



Observations:

	- Vector engrave @ 20mm/s seems to work nicely

	- Almost 0 vibration

	- Minimal smoke when vector engraving

	- 10mA edge cutting created a lot of smoke (hence the soot residue on cuts)

	- 4mA edge cutting created minimal smoke but required a lot of passes to get through the leather

	- Very clean edge cuts

	- Could have done with 1-2 more passes (as the leather fibres were still attached at the bottom <1mm)

	- Could have spaced the rectangles closer, as on leather the gap is noticeable (maybe 0.125mm spacing)



Leather Finishing

	- Quick wipe with methylated spirits (to remove soot residue)

	- Boiled beeswax wiped over both sides & edges

	- Allowed to dry

	- Heat gun applied to set beeswax into leather fibres

	- Excess beeswax wiped off as soaking into leather



![images/e1c1920d187c065714f7bcaea03d3913.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1c1920d187c065714f7bcaea03d3913.jpeg)
![images/98b2ff04ebdd2b0c7609a1b284eb88de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98b2ff04ebdd2b0c7609a1b284eb88de.jpeg)
![images/3d7934381d94582d3c45296e33db7614.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d7934381d94582d3c45296e33db7614.jpeg)
![images/eed4a505db54ac3ccb3a05f1e32eb1ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eed4a505db54ac3ccb3a05f1e32eb1ce.jpeg)
![images/cacf939b30e7b921ade74c67c3a5064b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cacf939b30e7b921ade74c67c3a5064b.jpeg)
![images/627fb9e3315601ea95fea90197cb3111.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/627fb9e3315601ea95fea90197cb3111.jpeg)
![images/bef28d5c3cde08fe2d0fc985a169bbf4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bef28d5c3cde08fe2d0fc985a169bbf4.jpeg)
![images/6d3d6a80db0621b4e6494bf498beb059.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d3d6a80db0621b4e6494bf498beb059.jpeg)
![images/ae0521c8251e87b748b431e2549ccc12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae0521c8251e87b748b431e2549ccc12.jpeg)
![images/7eb95fb64b1c5d3bc5d3461d09c09af1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7eb95fb64b1c5d3bc5d3461d09c09af1.jpeg)
![images/fbd6e547da12272efc4a721975c4b32c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fbd6e547da12272efc4a721975c4b32c.jpeg)
![images/707a89ee4c16d06da4e3cca6206530f4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/707a89ee4c16d06da4e3cca6206530f4.jpeg)
![images/cef7dae78e6e40f904da6887d7c7a1eb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cef7dae78e6e40f904da6887d7c7a1eb.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Donna Gray** *May 01, 2016 21:45*

Where can you purchase leather like this from in Australia I tried looking on ebay but I am not sure what to type in for the search it all seem to be very thin leather I can fine


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 22:41*

**+Donna Gray** I purchase from a supplier in Capalaba (just south of Brisbane). There are a few places you can purchase it.



Online purchase, you want to go with TandyLeather ([https://www.tandyleather.com.au/en/](https://www.tandyleather.com.au/en/)). Basically what you will be looking for is "Natural Vegetable Tanned Leather". It is raw & uncoloured/finished. Starts off as a pink/cream kind of colour. Then you can dye/lacquer/beeswax (or other method) to finish it.



The supplier I go to is called MacLace ([http://www.maclace.com.au/](http://www.maclace.com.au/)), located in Natasha St, Capalaba. Another one in the Brisbane region was in Wooloowin, over near the airport, but off the top of my head I can't remember what they are called (as I barely shopped there). But they have some really cool stuff like Rainbow Perch leather, Croc leather, Snakeskin leather, etc.



edit: additionally, you have to be careful with leather as some is not actual leather. Some stuff is a combination of leather fibres (e.g. from smalls scraps) that has been mixed with a resin like stuff to hold it together. The good quality leather is known as "Full Grain". You will be looking at around $100+ per square metre for high quality leather, just for reference point.


---
**Donna Gray** *May 01, 2016 22:53*

OK thanks for your help I will check it out


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/i2phX4pmfJJ) &mdash; content and formatting may not be reliable*
