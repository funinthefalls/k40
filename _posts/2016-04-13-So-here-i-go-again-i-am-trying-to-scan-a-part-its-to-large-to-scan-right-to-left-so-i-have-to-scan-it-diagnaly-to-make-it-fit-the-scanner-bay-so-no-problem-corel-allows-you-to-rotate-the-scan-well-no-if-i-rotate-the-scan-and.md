---
layout: post
title: "So here i go again i am trying to scan a part it's to large to scan right to left so i have to scan it diagnaly to make it fit the scanner bay so no problem corel allows you to rotate the scan well no if i rotate the scan and"
date: April 13, 2016 21:07
category: "Discussion"
author: "Dennis Fuente"
---
So here i go again i am trying to scan a part it's to large to scan right to left so i have to scan it diagnaly to make it fit the scanner bay so no problem corel allows you to rotate the scan well no if i rotate the scan and send it out to the laser it dosent know what to do i have to import the image just as it was layed out in Gimp for it to cut i am already hating this software any help has any one else run into this.



Thanks 

Dennis  





**"Dennis Fuente"**

---
---
**Scott Marshall** *April 13, 2016 21:22*

The hate only grows. Sorry.



Smoothieboards will be in in about 3 weeks.  I gotta try it. It CAN"T be worse than Chinese Corel & Laserdrw....



[http://shop.uberclock.com/products/smoothieboard-3x](http://shop.uberclock.com/products/smoothieboard-3x)

Hundred bucks, free software, motor drivers onboard, - all good,

SUPPORT here-priceless!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 21:58*

**+Scott Marshall** Ooo that place is slightly cheaper than where I priced the 5xc (robotseed). Oh, but they ship for orders outside US from robotseed.


---
**Scott Marshall** *April 13, 2016 22:03*

I think they actually had some  though. Pretty sure it was just the 5 axis which is kind of overkill for the K40.



Are you going to do it?, It would be nice to know I've got a partner in confusion...



I just joined the Google Smoothieware Group. If I start reading now, I may have an easier time of it when I get one.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:21*

**+Scott Marshall** Yeah I will be definitely upgrading in the near future. It's just a matter of me saving the $. Works out to about au$260-270 including the shipping, so will probably be about 4-6 weeks before I have that spare.



I intended to get the 5 axis one because I have a few ideas/mods I would like to make on the K40 that require extra axis. E.g. attaching dremel head that needs to raise up & down between shapes when cutting (to make like a 2d cnc), or another idea for something similar related to my leatherwork, or another idea for a rotary cutting attachment, or even the idea for the bevelled laser cuts (by rotating the lenses/mirrors/etc similar to the concept drawing I recently posted). So even though 5 axis is overkill, it will be usable down the track for mods/add-ons or even for entire system builds for other machine concepts I want to try.


---
**Dennis Fuente** *April 13, 2016 22:38*

Looked at the smoothie board is this a direct plug and go or whats required also the web site says $135.00 for the board also whats the software that it runs on



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:45*

**+Dennis Fuente** Peter Van De Walt here in this group is developing a software currently called Laser Web 2.0. He has already done Laser Web, but 2.0 I guess is adding improvements etc.



From what I posted recently, it seems that you need the board + something called a "level shifter" (which converts or regulates voltage from what I can gather). From there I have also been suggested to get a "middleman board" to connect the ribbon cable stepper motor (X-axis). If yours has a ribbon cable, I'd assume you need that too.



Other than that, I'm not too certain at the moment. If you check for my post (from a couple of days ago) on my profile, +1 it to keep up to date with any comments/suggestions for what is needed for the upgrade.


---
**Dennis Fuente** *April 13, 2016 22:47*

is there a place to see it like running a laser cutter before i  jump into the boat i want to see if it floats first.



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:53*

**+Dennis Fuente** [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/DoFyNaubJSc](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/DoFyNaubJSc)



This is my post the other day where people suggested what you need to upgrade. There are a few people in this group that have done Smoothie upgrades to their machine. E.g. **+Ariel Yahni**. Check out his profile/youtube links as he has some videos of it in action, running Peter's LaserWeb software.


---
**Dennis Fuente** *April 13, 2016 23:03*

Yuusuf

Looked over the post sounds like there is a lot to change out to make it work i will have to look inot it further thanks



Dennis  


---
**Scott Marshall** *April 13, 2016 23:16*

All you need in addition to the Smoothieboard is a small 5v power supply, and a 24v 100w max motor supply . The 3x inputs via USB, and the 4x & 5x have Ethernet support as well.

It has the stepper drivers onboard (up to 2.5a I believe - plenty for the k40 motors). along with some high current (10a) and low current FET outputs and a bunch of inputs, no BOB needed.



All that should be involved in the upgrade it to connect the K40 motors and Home optoswitches, one of the outputs to the power supply "laser fire", load the software and you should be there. oh yeah, and wire up the 5 & 24v supplies. There is plenty of outputs for auto air assist etc if you want to play. Not sure if the software supports that kind of stuff, but it easy to do with relay logic if not.

. 

I'm running an Ethernet Smoothstepper on my mill which is about $160 plus $45 for a C25 Bob and around $150 worth of Gecko 251xs(got a deal on them)  so I'm in at about $350 plus supplies etc.  

Oh, yeah, almost forgot and $175 for Mach 3.... (worth it though) So into about $500 plus enclosures, spindle motor speed control, power supplies  etc. Ow, never added it up before.

That Smoothie for even $200 is looking pretty good!



That's for 3 axis , the laser is 2, but any way you stack it, even the 5x Smoothie is a great deal. add to it the Laserweb and it sure sounds like the way to go. 


---
**Ariel Yahni (UniKpty)** *April 14, 2016 00:03*

Just to clarify I run a laser diode not a k40


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 00:07*

**+Scott Marshall** I guess your background in engineering assists you with understanding all this stuff a bit more than me. I think I will be the one confused during the upgrading process, not you. Haha.



I like the idea of 3 axis smoothie, because we can then rig up z-axis for the z-table. I imagine Peter already has that ability in his Laser Web software (to control the z-height), although if not, I'm fairly certain he could add it to the list of improvements/features (maybe with a few drinks $ donation). One thing I like about the whole Smoothie/LaserWeb combo, is as Peter does the software, he talks & listens a great deal to this community as to needs/issues/etc. Which is great for providing us all with machines that will perform the tasks that we wish to perform.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 00:08*

**+Ariel Yahni** Oh, I didn't realise that. Thanks for the clarification though.


---
**Anthony Bolgar** *April 14, 2016 02:29*

I ordered an MKS clone of the Smoothieboard 5C for $59.00. I couldn't wait for stock at Robotseed or Uberclock here in North America. I will probably buy an original smoothieboard once they have stock for my 2nd laser cutter. I would like to support Arthur for his incredible job on the smoothie project.


---
**Dennis Fuente** *April 14, 2016 16:02*

Hi Scott i run Mach 3 on my mill with gecko drives i retro fitted a Lagun CNC servo mill it was bit of work but the results were great, so were can some one purchase these smoothie boards, also would i have to change out the power supply or just the main board whats a bob board.



Dennis  


---
**Anthony Bolgar** *April 14, 2016 16:18*

You can buy the Smoothieboards from Robotseed in Europe or Uberclock in North America. If you are OK with using a clone board of the smoothie, Ebay has the MKS Sbase (A clone of the 5x Smoothieboard) for $59.00 (Less than half the price of an original Smoothieboard.) Stephane has posted an upgrade procedure for the K40 that is very easy to understand.


---
**Dennis Fuente** *April 14, 2016 16:32*

Hi Anthony 

Thanks for the response are the clone boards any good i am in the US the price sounds great  any thing additionly i would need.



Dennis 


---
**Anthony Bolgar** *April 14, 2016 16:51*

I ordered a clone board but haven't received it yet. But from reviews I have read it is decent quality. Check out the upgrade guide Stephane made ([http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)) it will explain everything you need and how to install it.


---
**Scott Marshall** *April 14, 2016 19:04*

**+Dennis Fuente** I just got a 5X Smoothie from Ebay, full price, but it's going to be about 3 weeks according to the Smoothie guy until the new stock comes in, and I don't know if the existing price will hold. The 3X is all you need for the K40, but as I like to play, the 5x has extra outputs as well as 2 more stepper channels.



Bob is Breakout board, basically a bunch of opto isolators that take your PC parallel port outputs, (or an Ethernet converter - I use a Smoothstepper on my mill) and isolate and buffer them to drive stepper drivers to move your motion control motors.

Normal logic flow is PC parallel port to Bob to stepper drives (Gecko or Chinese, (I prefer Gecko) to motors. The Bob also buffers the limit switches or opto interuppters back into the PC.



The Smoothie does it all, it takes your PC output and takes it all the way to stepper drivers, all you have to do is power it. The 3X is USB, but the 4X & 5x models have Ethernet connectivity so you're not limited by parallel speed and line restrictions,.



You will need to add 2 power supplies, a 5v one, maybe 5A (more then enough) to run the logic, and a 24 to 48v one to run your motors. The K40 motors are quite small so a 100w supply would do it with room to grow. the 5v supply is around 20 bucks and the motor supply would be under $50. You could probably tap from the K40 supply, but it's advisable to make the controls standalone.



The US supplier is Uberclock. There's another 5x on Ebay starting bid of $89. It's opened, but the guy just bought it and never used it.



My Mill is a Seig X1 with a bit of upgrading, oversized table, belt drive, KBIC-240d DC drive, Belt drive  and other odds and ends. 

I went the X1 as the dovetail column is stiffer than the X2 setup which tilts. It's surprisingly rigid and runs a 5/8 emill thru aluminum shockingly well for a 'toy' mill.

I've been lusting after a Grizzly G705, but keep spending my money on lasers and such....


---
**Dennis Fuente** *April 14, 2016 22:04*

Well Scott 

I don't know how long i can stand this corel software it realy sucks but i just got the machine and already spent a bunch of money on it as it came with damaged mirrors another $50 so i have some warranty on it don't know what that amounts to probably not much iwas orignaly looking at putting a laser on my mill but the laser didn't have any power and the higher i went in power the more it cost so i bought the K40 i give the smoothie a look i would like to see what this laser web software looks like to use before i go and spend more dough on a new board dose anyone sell the   smoothie board out of the US



Dennis  


---
**Scott Marshall** *April 15, 2016 01:01*

**+Dennis Fuente** The K40 isn't a cheap version of a production laser, as some people mistakenly think, It's an experimenters kit, pre-assembled for shipping convenience.



As far as it coming with bad mirrors, complain to your seller, most of them are pretty free with cash solutions once you mention you'd like a replacement unit, or them to repair it. Mine was pretty trashed (shipped from China with the pump and blower loose in the cutting compartment). I settled for $250, made a billet carriage on the mill, fixed all the loose and broken fasteners and used the cash for improvements.



It's amazing the CPSC hasn't been all over them, but I'm not complaining, I don't think outlawing stuff due to safety issues is right, I do believe fair warning is adequate, and if they just slapped a sticker on it, it would be all fair as far as I'm concerned.

  I just hope somebody that has no idea of the danger doesn't just open the door grab on, and get electrocuted.  A couple of guys read somewhere they could test the power supply by jumping wires while it's running, and when I told of the of the danger were stunned. (Hey, it was on the internet...) The HV isn't caged, interlocked, with nary a warning sticker of a lightning bolt hitting a little guy with smoke coming from his ears.



That being said, it can be made into a pretty nice machine with some time money and work. When done, it's a lot of bang for the buck. You can get used to the software, and you can do quite a bit with it once you learn it's way. It's not  a polished user friendly system, and the controller's lack of compatibility with gcode driven systems is arguably the biggest issue with the k40. (Lack of interlocks being #2, lots of people have forgot to turn on the water)



The solid state blue lasers that you hang on a mill or plotter are pretty weak, and not worth the cash in my opinion (and lots of people who bought one) Decent ones  are a couple hundred bucks plus, they're somewhat hazardous as the wavelength can really get your eyes, where IR from a C02 laser is stopped quite well by plain glass. You can engrave some materials with them, but paper is about what they'll cut. It's 2w of blue short easily reflected light vs 30W of IR (Heat energy).

They're cool if you have a couple hundred bucks to play with, but that's half of a K40 (or a Smoothie upgrade)



There's a lot of different people using the K40 for different things, some people just like to mod and build, others like writing software, and others like to create art with them. Most people here are a combination of several different things. 



Whenever somebody asks me if they should buy one, I give the the unvarnished truth, if they still buy one, they're in the right place here on our little club, I can't imagine owning a K40 without it, heck they should be paying US!


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/TR96raJ9WeM) &mdash; content and formatting may not be reliable*
