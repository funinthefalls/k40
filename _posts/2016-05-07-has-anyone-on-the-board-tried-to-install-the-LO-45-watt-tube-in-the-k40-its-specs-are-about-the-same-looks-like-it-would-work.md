---
layout: post
title: "has anyone on the board tried to install the LO 45 watt tube in the k40 it's specs are about the same looks like it would work"
date: May 07, 2016 22:18
category: "Discussion"
author: "Dennis Fuente"
---
has anyone on the board tried to install the LO 45 watt tube in the k40 it's specs are about the same looks like it would work





**"Dennis Fuente"**

---
---
**Mishko Mishko** *May 08, 2016 00:36*

Have you checked why they explicitely state it's not designed for K40? I suppose there must be a good reason, otherwise they wouldn't say anything...


---
**Dennis Fuente** *May 08, 2016 01:01*

because it is not a drop in item you would have to open the case to handle the longer tube


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/GrM37KJextJ) &mdash; content and formatting may not be reliable*
