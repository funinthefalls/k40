---
layout: post
title: "Finally I've bought a proper fan, to remove the fumes"
date: August 25, 2016 18:50
category: "Discussion"
author: "Akos Balog"
---
Finally I've bought a proper fan, to remove the fumes. The original is not so bad, but weak. I've bought a 150mm axial fan (214 CFM, 36W),  with duct, and installed it using a 3D printed adapter ([http://www.thingiverse.com/thing:628269](http://www.thingiverse.com/thing:628269)) but it couldn't move as much air as the original, because as I've learned, aa an axial fan, it can't really make vacuum/pressure, so with any resistance it will have very low flow. Today I've bought a 150mm radial fan (370 CFM, 100 W), and installed, it made a huge difference. Now there is proper vacuum in the machine. This should be the first upgrade for everyone, to protect your lungs.







![images/993e02cf32f2e61dbdac4128f1f4a38b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/993e02cf32f2e61dbdac4128f1f4a38b.jpeg)
![images/d435c9c560a747f178bfb125347b1f08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d435c9c560a747f178bfb125347b1f08.jpeg)

**"Akos Balog"**

---
---
**Phillip Conroy** *August 25, 2016 21:20*

Well done-my upgraded fan is a 200mm 250 watt monster that works very well ,even after reducing pipe to 100mm and being at least 1 meter away from laser cutter.if i could have got my hands on a bigger fan i would have,thought about the fans they use in wood working shops.


---
**Ulf Stahmer** *August 25, 2016 21:30*

Where possible, you should consider using hard pipe (like heat duct piping) rather than the flexible pipe.  The airflow losses due to the roughness of the flexible pipe are very high.  The pipe should also be as short as possible and also minimize the number of bends.  It's best to put the fan close to the outlet to create a negative pressure from the K40 to the fan (it's easier to suck than to blow).  This also will minimize the likelihood of exhaust fumes entering into your work space.


---
**Timo Birnschein** *August 31, 2016 00:57*

Can you share a link to the fan?




---
**Akos Balog** *August 31, 2016 05:23*

I've bought it in Hungary, so I'm afraid it won't help a lot.

[http://www.legtechnikabolt.hu/index.php/ventilatorok/csoventilatorok/fkm-150-radialis-femhazas-csoventilator](http://www.legtechnikabolt.hu/index.php/ventilatorok/csoventilatorok/fkm-150-radialis-femhazas-csoventilator)

It is made by Ferono, and the model number is FKM 150


---
**Akos Balog** *August 31, 2016 05:24*

**+Ulf Stahmer** Thanks, I know it could be optimized, however as it is not in a proper workshop, I need the flexible, movable pipe. :(


---
*Imported from [Google+](https://plus.google.com/106984452297752036237/posts/RLYjHzXnfjV) &mdash; content and formatting may not be reliable*
