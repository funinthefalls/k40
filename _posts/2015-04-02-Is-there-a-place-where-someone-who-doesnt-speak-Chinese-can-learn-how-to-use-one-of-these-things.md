---
layout: post
title: "Is there a place where someone who doesn't speak Chinese can learn how to use one of these things?"
date: April 02, 2015 23:19
category: "Discussion"
author: "Bryun Lemon"
---
Is there a place where someone who doesn't speak Chinese can learn how to use one of these things?  I have it set up, configured, calibrated, etc.  Now I'm trying to learn the software (fat chance that), trying to figure our how to cut larger than a 2" x 6" piece of something, and trying to learn the basics of this machine.  The included instructions are useless, the included software is a joke, and I'm ready to scream.  HOW DO YOU LEARN THIS STUPID MACHINE?





**"Bryun Lemon"**

---
---
**Jim Coogan** *April 03, 2015 01:47*

There are some videos on YouTube.  Search for K40 laser.  There is also a Facebook group that I belong to and the members are extremely helpful to anyone on there and quite a few own the Chinese machine.  Its called Laser Engraving and Cutting.  [https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
**Stephane Buisson** *April 03, 2015 06:51*

Hello and welcome here.



One of the goal of this community is to improve the K40.

As you underline it, the main weakness is the proprietary hardware/software from Moshidraw.

with a bit of hacking it's possible to go another way.

I am personally waiting for the couple visicut/smothieboard solution to upgrade my machine. but if you go backward on this community, you will see other combinations.



thank you to join us, and share your experience.


---
**Jason Johnson** *April 03, 2015 11:06*

i purchased coreldraw x6. the program seems to mesh really well with moshi. i was the exact same way when i got my machine running, did a trail of coreldraw and have had no problems ever since. 


---
*Imported from [Google+](https://plus.google.com/101302813693319625526/posts/ddLCfbaDfGA) &mdash; content and formatting may not be reliable*
