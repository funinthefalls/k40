---
layout: post
title: "Well, just burnt another LPS. I was aligning mirrors and on my second test fire I heard a strange sound and saw a flash of light through the hole between the laser side and the LPS side"
date: February 27, 2018 18:24
category: "Discussion"
author: "bbowley2"
---
Well, just burnt another LPS.  I was aligning mirrors and on my second test fire I heard a strange sound and saw a flash of light through the hole between the laser side and the LPS side.  Couldn't see anything burned until I unplugged the flyback connector and there it was.  Don't know where it arced to but I'm now on my 4th LPS.  I bought an expensive LPS after the previous one blew.  I guess it doesn't matter the cost so I'm going to buy 2 of the $68.00 ones but because I have a spare, the installed one will last.

 

![images/60bc8264994f8e199f74982e9549b256.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60bc8264994f8e199f74982e9549b256.jpeg)



**"bbowley2"**

---
---
**Andy Shilling** *February 27, 2018 18:58*

Thats rather worrying considering that is a screwed plug, where the hell could that have arched to???. I have a similar connector on mine, maybe a touch of insulation tape once it's put together just to help it out.


---
**James Rivera** *February 27, 2018 19:13*

Proximity matters a lot with high voltage. It seems like it must have been very close to something, perhaps the case itself? So yeah, adding some kind of electrical insulator around it would probably help prevent future arcing. Apologies if it seems like I’m stating the obvious.


---
**Don Kleinschnitz Jr.** *February 27, 2018 19:16*

Perhaps the core problem is not the supply? 



When the supply is unloaded it will violently arc through/around almost anything.



Adding insulation almost never works when there is no load, I've tried. It can jump 2". 



Will the supply still arc if you position the output a couple of inches from ground? Be careful. 



What coolant are you using?


---
**Joe Alexander** *February 27, 2018 19:24*

I would also check the wire connection to the laser tube itself. It would make sense that if it has a poor connection the bulk of the load would land on the LPS itself (and any shorter path to ground.) also what power do you normally run your jobs at?


---
**Phillip Conroy** *February 27, 2018 20:11*

Buy a tds water test meter and check water conductive  ( less than 100 is ok) [https://www.aliexpress.com/item/Z-Series-Intelligent-Z80-Co2-Laser-Power-Supply-80W-For-Laser-Engraving-Machine-With-LCD/32806003257.html?spm=a2g0s.9042311.0.0.Gfuy](https://www.aliexpress.com/item/Z-Series-Intelligent-Z80-Co2-Laser-Power-Supply-80W-For-Laser-Engraving-Machine-With-LCD/32806003257.html?spm=a2g0s.9042311.0.0.Gfuy)

My power supply has more built in p rotection than mos t and is a high quality unit and will work on tubes from 40 to 80 watts.

Maybe you have a dud tube, what was the  length of time  you got with the other rpwr supplys?

[aliexpress.com - Intelligent Z80 Co2 Laser Power Supply 80W For Laser Engraving And C Machine](https://www.aliexpress.com/item/Z-Series-Intelligent-Z80-Co2-Laser-Power-Supply-80W-For-Laser-Engraving-Machine-With-LCD/32806003257.html?spm=a2g0s.9042311.0.0.Gfuy)


---
**Paul de Groot** *February 28, 2018 01:39*

I guess don would love to get the broken supply to investigate 


---
**bbowley2** *February 28, 2018 04:02*

I will check the connection at the tube.  I run 90% because I only cut 

acrylic.  I use a diamond drag bit with my CNC machine.


---
**bbowley2** *February 28, 2018 04:04*

I'm going to eliminate the ceramic connector, I only use distilled water.


---
**bbowley2** *February 28, 2018 04:05*

Thanks.


---
**bbowley2** *March 01, 2018 02:25*

On close inspection I found this burn spot behind the LPS si it arcked to ground.



![images/087be0a25574a70374d6625e8c5b35e2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/087be0a25574a70374d6625e8c5b35e2.jpeg)


---
**Joe Alexander** *March 01, 2018 04:43*

that means that path was easier than the tube itelf, one or more connections may be loose or the tube may be failing? how old is the tube itself?


---
**bbowley2** *March 01, 2018 13:20*

Less than a year old.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/CyZUUmtCyrD) &mdash; content and formatting may not be reliable*
