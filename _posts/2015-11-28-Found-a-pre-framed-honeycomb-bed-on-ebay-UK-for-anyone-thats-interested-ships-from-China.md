---
layout: post
title: "Found a pre-framed honeycomb bed on ebay UK for anyone that's interested, ships from China :"
date: November 28, 2015 14:18
category: "Modification"
author: "Gary McKinnon"
---
Found a pre-framed honeycomb bed on ebay UK for anyone that's interested, ships from China :



[http://www.ebay.co.uk/itm/DIY-Co2-Laser-Stamp-Engraver-Kit-Honeycomb-Work-Table-Shenhui-K40-3020-32x22cm-/171963291915?hash=item2809cf990b:g:8wEAAOSwv0tVK4NJ](http://www.ebay.co.uk/itm/DIY-Co2-Laser-Stamp-Engraver-Kit-Honeycomb-Work-Table-Shenhui-K40-3020-32x22cm-/171963291915?hash=item2809cf990b:g:8wEAAOSwv0tVK4NJ)





**"Gary McKinnon"**

---
---
**David Richards (djrm)** *November 28, 2015 17:16*

If you are prepared to fiddle around expanding your own honeycomb you can buy it here without a frame: [http://www.ebay.co.uk/itm/10mm-thick-cell-6-4mm-Honeycomb-aluminium-beds-Laser-machine-honey-comb-/141759884244?var=&hash=item21018c2bd4:m:mxp2yfJ1CTsxskQNXmPjXCQ](http://www.ebay.co.uk/itm/10mm-thick-cell-6-4mm-Honeycomb-aluminium-beds-Laser-machine-honey-comb-/141759884244?var=&hash=item21018c2bd4:m:mxp2yfJ1CTsxskQNXmPjXCQ) 


---
*Imported from [Google+](https://plus.google.com/+GaryMcKinnonUFO/posts/9F2vJwtv5nU) &mdash; content and formatting may not be reliable*
