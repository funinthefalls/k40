---
layout: post
title: "Hello, I should ask you a great deal"
date: December 30, 2017 20:00
category: "Discussion"
author: "Todoo adventure"
---
Hello,



I should ask you a great deal.



You could send me a manual to control this engraver machine (in PDF format)



[http://www.china-cncrouter.com/products/JK-3020-40W-Chinese-Mini-Desktop-CO2-Laser-Cutter-for-Sale.html](http://www.china-cncrouter.com/products/JK-3020-40W-Chinese-Mini-Desktop-CO2-Laser-Cutter-for-Sale.html)



Thank you very much.



Have a nice day





**"Todoo adventure"**

---
---
**Jon McGrath** *December 31, 2017 11:55*

Just another "K40" CO2 laser. You can buy them on ebay for ~$300 but plan on a few hundred more to get the best usability out of it... (air assist, z-axis, exhaust, water cooling, etc)



Not bad "starter" lasers if you haven't used a CO2 laser before though! You should be able to cut and engrave most materials up to about 1/4"


---
**Rouge Saphir** *January 04, 2018 22:50*

**+Jon McGrath** Hi and true, I bought this cheap machine 6 months ago to learn about this magical world : very exciting ! And I confirm : the laser tube stopped during a work and I have no support. I changed the tube for the same new one and it doesn't work either. I read some interesting technical data in this group, maybe it will help me to solve that. Otherwise I'll try to ask for help in a new post. Anyway, nice to read and see so many interesting things here ! :)


---
*Imported from [Google+](https://plus.google.com/102994589570644028924/posts/RfaetVKtp2W) &mdash; content and formatting may not be reliable*
