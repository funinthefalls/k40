---
layout: post
title: "I'm new to the laser cutters, and I'm just learning it..."
date: June 30, 2016 18:39
category: "Original software and hardware issues"
author: "Akos Balog"
---
I'm new to the laser cutters, and I'm just learning it... Is it normal, that the end of the plasma is curved, and meeting only 1 point of the cathode, like on my photo? 

![images/ce3317ac8aaf54c206971eea55bab570.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ce3317ac8aaf54c206971eea55bab570.jpeg)



**"Akos Balog"**

---
---
**Ariel Yahni (UniKpty)** *June 30, 2016 19:00*

Never had one but looks like an electrical arc. 


---
**Jon Bruno** *June 30, 2016 19:02*

Mine does the same thing.. 

The end of the plasma arc can also dance about.  AFAIK it's normal.


---
**Akos Balog** *June 30, 2016 19:42*

Thx


---
**Scott Marshall** *June 30, 2016 22:28*

Magnetic fields will push it around (that's how plasma is contained & controlled in sputtering machinery, SCMs and fusion experiments) 



The K40 housing is steel, which is electrically welded, which leaves residual magnetic charges. 

No harm will come of the minor deflection you see there, as long as it's not severe. Just don't use magnets right near the tube and you'll be fine.



I think you'd have to be trying to cause a real problem, like putting a strong magnet on or near the tube.


---
*Imported from [Google+](https://plus.google.com/106984452297752036237/posts/APjD5RuDpmD) &mdash; content and formatting may not be reliable*
