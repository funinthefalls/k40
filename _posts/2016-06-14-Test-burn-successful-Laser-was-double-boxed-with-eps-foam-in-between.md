---
layout: post
title: "Test burn successful! Laser was double boxed with eps foam in between"
date: June 14, 2016 23:09
category: "Smoothieboard Modification"
author: "Ray Kholodovsky (Cohesion3D)"
---
Test burn successful! Laser was double boxed with eps foam in between. Outer box has a dent. Inner box only pictured. 



![images/fe94f3d5513484e59e5515af01243470.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe94f3d5513484e59e5515af01243470.jpeg)
![images/04303f11b60c39a45be8d3581e5ba8d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/04303f11b60c39a45be8d3581e5ba8d7.jpeg)
![images/1904228f05cac6ace12cb0e3306d72bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1904228f05cac6ace12cb0e3306d72bc.jpeg)
![images/c84bdae502ded0e6c285934e43f6eec5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c84bdae502ded0e6c285934e43f6eec5.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/S15QXMWdHiC) &mdash; content and formatting may not be reliable*
