---
layout: post
title: "Does anyone have a source for laser alignment paper \"Burn paper\" for cheap??!!"
date: August 03, 2015 16:20
category: "Discussion"
author: "James McMurray"
---
Does anyone have a source for laser alignment paper "Burn paper" for cheap??!!





**"James McMurray"**

---
---
**Jason Johnson** *August 03, 2015 16:26*

just use an old McDonald's receipt or any kind of receipt that was printed on thermal paper. (test it by putting a lighter at the bottom, if the paper turns black from the heat, then you have thermal printer paper) 


---
**James McMurray** *August 03, 2015 16:30*

That is a good idea,  the paper I am used to using is black and will not burn through during alignment pulses if the power happens to not be turned down.  

I will try the thermal paper to see if it works better than regular bond.


---
**Joey Fitzpatrick** *August 03, 2015 17:54*

You can buy thermal paper at any office supply store if you need it quickly (Calculator paper, Cash Register Paper) .  If you don't mind waiting  check E-bay.  You can get a single roll for .99 cents + shipping.  


---
**I Laser** *August 07, 2015 07:34*

My laser won't reliably fire under 4mA and at that setting it obliterates thermal paper. Stuck using masking tape. :(


---
*Imported from [Google+](https://plus.google.com/115183135050042620815/posts/jTuELzqNXAq) &mdash; content and formatting may not be reliable*
