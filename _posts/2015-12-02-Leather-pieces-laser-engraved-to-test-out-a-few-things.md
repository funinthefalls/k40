---
layout: post
title: "Leather pieces (laser engraved) to test out a few things"
date: December 02, 2015 11:37
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Leather pieces (laser engraved) to test out a few things. Wanted to test airpump for air assist. Works well & prevents burning. Also wanted to test new dye colours I purchased. Also wanted to test airbrushing the dye onto the leather.



Engrave @ 500mm/s @ 4mA @ 1 pixel step.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Tested out airbrushing on my some pieces of leather today with different colours of dye. Then I decided to lacquer them & turn them into swatches.

From Top to Bottom: Black, Tan, Red, Orange, Lime, Natural (no dye).

![images/be3ff04fd2f31e4bb43913d2d76d98fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be3ff04fd2f31e4bb43913d2d76d98fd.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/2y6Raghuq2d) &mdash; content and formatting may not be reliable*
