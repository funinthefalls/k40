---
layout: post
title: "I am hoping to connect a manual pulse generator wheel (MPG) to a stepper driver to control a Z bed manually"
date: January 17, 2019 18:35
category: "Modification"
author: "Anthony Bolgar"
---
I am hoping to connect a manual pulse generator wheel (MPG) to a stepper driver to control a Z bed manually. Does anyone know how I would wire it up? The pulse wheel has 5V and GND connections and A/B and A-/B- connections on it. I am using a TB6600 stepper driver with a Nema17 stepper motor. **+Don Kleinschnitz Jr.** any ideas on how this is accomplished?





**"Anthony Bolgar"**

---
---
**Wolfmanjm** *January 18, 2019 10:44*

you will need an arduino to read the pulses as it is a quadrature encoded pulse. then use that to generate steps for the driver. I have a project that basically converts one of these MPG into a rawhid USB device. It may help you decide what to do. [github.com - wolfmanjm/mpg-usb](https://github.com/wolfmanjm/mpg-usb)


---
**Don Kleinschnitz Jr.** *January 18, 2019 11:59*

What **+Wolfmanjm** said. 



My LO table is driven by steppers but I use simple switches to move the table up/dwn. This is fine for my K40 as I set the focus with a gauge anyway. 

[donsthings.blogspot.com - Stand alone K40 Zaxis Table & Controller Build](https://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)



Not sure how you want the user interface to work but if you want to be able to control the amount of movement with the pendant or just insure that it takes a single step you need some kind of processor between the encoder and the motor driver.



This is a project with an arduino and encoder;

[https://create.arduino.cc/projecthub/mitov/arduino-visuino-control-stepper-motor-with-rotary-encoder-204440?ref=tag&ref_id=stepper&offset=7](https://create.arduino.cc/projecthub/mitov/arduino-visuino-control-stepper-motor-with-rotary-encoder-204440?ref=tag&ref_id=stepper&offset=7)


---
**Anthony Bolgar** *January 18, 2019 13:12*

Thanks guys!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/H5sRuJHyQuy) &mdash; content and formatting may not be reliable*
