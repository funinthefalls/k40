---
layout: post
title: "Got the stand made for my new machine when it gets here next week"
date: December 19, 2015 16:51
category: "Discussion"
author: "Scott Thorne"
---
Got the stand made for my new machine when it gets here next week.

![images/139c401d1c45f44051f3efa90bfc2875.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/139c401d1c45f44051f3efa90bfc2875.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 19, 2015 17:08*

Looks nice & sturdy :)


---
**Scott Thorne** *December 19, 2015 17:11*

Yes it is....I have a piece of 3/4 plywood to go over it...I just haven't cut it yet...Lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 00:51*

**+Scott Thorne** I was walking downstairs in garage earlier when I noticed a similar table to this (frame wise). It was one of those really old school (not old-school though) tables that was really solidly built. Has a thick laminated chipboard top on it. Thought for future reference if anyone else wants something similar they might be able to bypass the building it from scratch bit haha.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/L5uS4uYemFN) &mdash; content and formatting may not be reliable*
