---
layout: post
title: "Question on Electrical conductivity of coolant. I'm currently using the distilled water + biocide + dawn recipe"
date: October 15, 2018 17:25
category: "Discussion"
author: "Brent Dowell"
---
Question on Electrical conductivity of coolant.  I'm currently using the distilled water + biocide + dawn recipe.  This is giving me about 50us on a little meter I've got.  



Distilled water reads about 2us, so the meter seems like it's doing a good job. I'm assuming the dawn + biocide is what is raising it up to 50.



It's getting cold and I need to add something to prevent the liquid from freezing. I've got some propylene glycol  and I could add that.  But I've also got some pre-mix RV antifreeze that I measured at 80us.  80 does not see like that much higher of a number, and certainly a lot less than the +400us I've seen listed for rv antifreeze elsewhere.



Is there some sort of maximum level of conductivity or is it 'just keep it as low as possible'.



I like the idea of using the RV antifreeze as it's much cheaper than the propylene glycol. 









**"Brent Dowell"**

---
---
**Don Kleinschnitz Jr.** *October 15, 2018 18:40*

Unfortunately we do not know what an acceptable range is because we can't easily model or test the phenomena. I know that more than 300 is bad news but don't know what the lower limit is.

We also do not know what the long term effects of conductivity are.

So yes, keep it as low as possible. BTW many folks use aquarium heaters to keep their water from freezing.

I was using RV antifreeze and it was way above 300 and created a problem with my HVT!






---
**James Rivera** *October 15, 2018 21:13*

I just use distilled water and algaecide. +1 for using aquarium heaters. That's what I used last winter. I just set it to as low as possible (so it is well over freezing but not trying to keep it hot) and then make sure I've got the pump running the whole time, too. Fortunately, it doesn't drop below freezing too often here (Seattle, WA area) so I just keep an eye on the nighttime temperature forecast and only turn it on when it looks like it will be necessary.


---
**Brent Dowell** *October 15, 2018 21:19*

Thanks Guys.  Guess I'll just dump in the propylene glycoal and use the aquarium heater when it gets really cold and I'll save the rv antifreeze for my water cooled spindle.  



I appreciate the feedback.


---
**Don Kleinschnitz Jr.** *October 16, 2018 00:42*

Have you tested the water with Propylene glycol?


---
**Brent Dowell** *October 16, 2018 13:55*

Not together.  I did test propylene glycol and it registered 0us on its own, compared to distilled water at 2us




---
**James Rivera** *October 16, 2018 20:49*

Interesting...check the bottom of the table of physical properties.[dowac.custhelp.com - Dow Answer Center](https://dowac.custhelp.com/app/answers/detail/a_id/7213)


---
**Brent Dowell** *October 16, 2018 22:22*

If my interpretation of that is correct, thats a pretty small number.




---
*Imported from [Google+](https://plus.google.com/117727970671016840575/posts/4HkwJuus7Kx) &mdash; content and formatting may not be reliable*
