---
layout: post
title: "A cautionary tale... don't let your k40 freeze with water in the tube"
date: December 02, 2016 09:44
category: "Discussion"
author: "photomishdan"
---
A cautionary tale... don't let your k40 freeze with water in the tube. After a couple of sub zero nights I decided to have a look at my k40, and this is what I found. It seems the pressure got too much for it! 



So, any recommendations on where to get a replacement tube in the UK for a reasonable price. I was saving for a smoothie, but now this has happened that's on the back burner.

![images/0f850712f4313abd8bb2ec06c4b5281c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f850712f4313abd8bb2ec06c4b5281c.jpeg)



**"photomishdan"**

---
---
**Phillip Conroy** *December 02, 2016 10:22*

Bugger,


---
**photomishdan** *December 02, 2016 10:23*

**+Phillip Conroy** my thoughts were a little more explicit than that when I saw it! lol 


---
**Andy Shilling** *December 02, 2016 10:49*

[ebay.co.uk - Details about   40w CO2 Sealed Laser Engraver Engraving Cutting Tube Water Cool Standard 70cm](https://www.ebay.co.uk/itm/282145509090)





This is who I used, very quick service.


---
**photomishdan** *December 02, 2016 10:51*

**+Andy Shilling** Thanks for that Andy, Was the Tube just a drop in replacement? Did the item ship from the UK? I can only see a similar tube in stock in Germany at the moment. 


---
**Phillip Conroy** *December 02, 2016 10:54*

I cracked a tube when shirty plug converters  didn't make good electectral contact for the water [pump.also](http://pump.also) did power supply as well ,both in one [hit.the](http://hit.the) bugger of it all was I had a flow switch sitting on my workbench  for 2 weeks


---
**Andy Shilling** *December 02, 2016 10:57*

Im not sure if it actually came from Manchester or Germany but it turned up in two days from ordering it and yes just undo the clamps and wires and drop it in. Just make sure you insulate the wires properly when you put it back together.


---
**Kelly S** *December 02, 2016 19:11*

I suggest being cautious with that red wire.


---
*Imported from [Google+](https://plus.google.com/105674876465839652573/posts/LY2tEt46MdC) &mdash; content and formatting may not be reliable*
