---
layout: post
title: "The weird stuff just keeps coming....I go out to the garage and start to engrave a piece today and the beam is way of from the hole in the head....I don't get it...everything is good and tight...nothing loose...nothing....it"
date: December 06, 2015 18:27
category: "Discussion"
author: "Scott Thorne"
---
The weird stuff just keeps coming....I go out to the garage and start to engrave a piece today and the beam is way of from the hole in the head....I don't get it...everything is good and tight...nothing loose...nothing....it took me over an hour to get it back aligned...wow.





**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 06, 2015 18:51*

 That's strange indeed. I guess I would put this one down to the "gremlins". They're always up to mischief.



On a serious note though, is there anyone that may have adjusted a mirror (e.g. children) or bumped the machine?



Or alternatively, I wonder if significant ambient temperature changes could cause misalignment issues (as the metal expands/contracts on the framework)?


---
**Sean Cherven** *December 06, 2015 19:02*

Its the flimsy sheet metal frame. The desk it's setting on can settle or shift, and cause misalignment.. Heat/Cold (Expansion) of the metal can cause this also.


---
**Scott Thorne** *December 06, 2015 19:58*

**+Yuusuf Sallahuddin****+Sean Cherven**....I wish it were that simple....no children no-one comes close to the engraver....I've got it dialed in now....a little better than before but it's very troubling to me that yesterday it was cutting great and this morning it wouldn't cut anything....the desk it's sitting on is solid....expansion is out of the question because it was over 1/4 inch away from the hole in the head....all 3 screws were tight on both mirrors....GREMLINS!


---
**Gary McKinnon** *December 06, 2015 23:39*

That's crazy!


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/NRxHFY2GqmP) &mdash; content and formatting may not be reliable*
