---
layout: post
title: "Hi all. I'm soooo glad I found this community"
date: May 03, 2015 02:20
category: "Discussion"
author: "Stuart Middleton"
---
Hi all. I'm soooo glad I found this community. I've not long had my K40 and I'm just starting to test it out. I still need to do a proper align of the laser but for now it's good enough to cut the card for our wedding, which is the main reason I purchased it. I'm loving it so far (hating the software though).



I plan to add an air assist at some point soon and maybe even upgrade to a RAMP 1.4 board so I can use decent software and adjust the laser power from the PC. All that can wait though until after the wedding.



180 people all in one place who I can ask advice from and hopefully give advice back in time will be a great help in these early days. I've already got some ideas and had some questions answers from reading the posts.



I'll try not to ask too many stupid questions :)





**"Stuart Middleton"**

---
---
**Jon Bruno** *May 03, 2015 02:22*

I'm new too man. Welcome. These guys are a pretty helpful bunch.


---
**Dan Shepherd** *May 03, 2015 03:04*

This is a great place to find help, there is also a Facebook group that has almost 500 members  and a files area with a lot of info and projects [https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
**David Richards (djrm)** *May 03, 2015 06:50*

Welcome on board Stuart. I'm no expert but we are all on the same journey.

 I have added red laser cross hair guides, air assist, inline extractor fan. Lid interlock switches. Water flow interlock. Cabinet lamps, etc. I'm working on an adjustable table with honeycomb bed.

 My system came with Corel Laser which does everything I need. I was going to upgrade the driver board but havnt needed to yet - i think to do so would loose the engraving feature. 

I'd reccomend that you measure the laser current, my system can over drive the tube. 18mA is at the 80% setting﻿


---
**Stephane Buisson** *May 03, 2015 09:22*

Welcome Stuart to our comunity, happy wedding to you.


---
**Imko Beckhoven van** *May 03, 2015 11:20*

I cut out my wedding cards using my laser about 2 years ago. Made the front out of wood venieer. Goodluck with youre inventations


---
**Stuart Middleton** *May 04, 2015 09:52*

Hey thanks guys. I don't feel so alone with my laser cutter now :)


---
*Imported from [Google+](https://plus.google.com/+StuartMiddleton1/posts/BmTGE8BjkQZ) &mdash; content and formatting may not be reliable*
