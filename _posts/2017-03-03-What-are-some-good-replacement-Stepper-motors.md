---
layout: post
title: "What are some good replacement Stepper motors?"
date: March 03, 2017 04:27
category: "Discussion"
author: "tyler hallberg"
---
What are some good replacement Stepper motors? I have narrowed my problems down to a bad stepper motor on the x axis. I have tried everything from adjusting the power to cleaning everything, still missing random steps when printing. I am 95% sure a new stepper motor will fix my problems





**"tyler hallberg"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 04:46*

Bad stepper is pretty low on the diagnostic list, it doesn't happen often. 

For 48mm Nema 17's I go with openbuilds which is LDO or Motech, both good Chinese brands. 

Kysan is also good. 

But the k40 probably used a shorter motor. I have 40mm ones coming from stepperonline on eBay, I will let you know how they pan out. 

Let me know the height of the motor in the k40?


---
**tyler hallberg** *March 03, 2017 04:48*


{% include youtubePlayer.html id="avtrkZ_W2RI" %}
[youtube.com - K40 stepper motor problems](https://youtu.be/avtrkZ_W2RI)


---
**tyler hallberg** *March 03, 2017 04:51*

Video of the missing. Its at random spots, online says to turn down power so I turned the adjusters until it didnt move then slightly turned them little at a time, and happens 100% of the time. The missing is causing the pictures to still shift. I have tried same photo in ls3 and ls4 with different settings, tried doing just b&w photo as well as making it a bitmap, same problems no matter what. Changed the currents in the setup file for the cohesion board and no matter what it is it does not change.


---
**tyler hallberg** *March 03, 2017 04:53*

All the forums say that its the motor heating up and temperately shutting itself down to cool. Not sure how true all that is. I even tried moving the x axis over on the board to the DIR3 spot (Z axis) and changed all the settings in the setup file to rule out a bad connection on the board anywhere. Ill have to measure the motors tomorrow when I have some time to remove them from the unit.


---
**tyler hallberg** *March 03, 2017 04:56*

ran upstairs, looks like they are 42mm motors


---
**jonjon f** *March 03, 2017 05:07*

Try to replace the wire on your x motor.


---
**tyler hallberg** *March 03, 2017 05:08*

wires got into the motor, no plug on the wiring on the motor end.


---
**Claudio Prezzi** *March 03, 2017 08:57*

Your description sounds like you have set the current of the steper drivers to the minimum needed to move the axis. That's wrong. You want to set the maximum current before they get too hot (and shot down for self protection).


---
**Claudio Prezzi** *March 03, 2017 08:59*

Also, check that your acceleration settings are not too height.


---
**tyler hallberg** *March 04, 2017 02:51*

Ive decreased acceleration settings to almost nothing, I have tried to change current from just barely moving to all the way up and no changes


---
**jonjon f** *March 04, 2017 03:54*

try this [electronics.stackexchange.com - how to find out wether the stepper motor is working or not?](http://electronics.stackexchange.com/questions/25032/how-to-find-out-wether-the-stepper-motor-is-working-or-not)


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/7we6pDcZKGp) &mdash; content and formatting may not be reliable*
