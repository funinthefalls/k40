---
layout: post
title: "Sharing a vent. My laser will be living in a basement workshop with no windows for easy ventilation set up"
date: March 18, 2018 15:40
category: "Discussion"
author: "Tom Traband"
---
Sharing a vent.



My laser will be living in a basement workshop with no windows for easy ventilation set up. I've got an existing whole-house air-to-air heat exchanging system with 6" ducts, one drawing in outside air and the other exhausting. This runs all the time.



Can I get a 4" to 6" wye fitting and join the laser exhaust to the exhaust side? I'd do this as close to the exit as I can get so as to avoid putting in a new vent hole. I'm looking at a 4" inline 105 cfm fan that I would also put as close to the duct exit as possible.



Another option would be to tap into an existing 4" dryer vent but I'm hesitant to put the laser exhaust in with all that lint just in case an ember should get sucked up.





**"Tom Traband"**

---
---
**Anthony Bolgar** *March 18, 2018 20:47*

Just make sure you do not create a situation where your laser exhaust is stronger than the exhaust line you are tapping into, or else you can get some back flow of the fumes into the house. Best option is to drill a 4-6" diameter hole in the basement wall and vent directly outside. Fan is best placed as close to the exit as possible so that the majority of the piping is under suction, preventing fumes from entering the work space. A damper on the exhaust is also a good idea to keep cold (or warm) air from back flowing into the house when not in use. Hope this information helps.


---
**Tom Traband** *March 18, 2018 20:50*

Thanks Anthony. I will probably put in a dedicated vent once I've got a better idea of the leader's permanent home. For now the shop layout is in flux, so no point in making more holes in sub-optimal locations.


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/eX6y7gRYQeW) &mdash; content and formatting may not be reliable*
