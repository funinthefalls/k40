---
layout: post
title: "It works! I spent Tuesday evening cleaning and aligning the optics on my new machine"
date: July 23, 2015 22:53
category: "Object produced with laser"
author: "Gee Willikers"
---
<b>It works!</b> I spent Tuesday evening cleaning and aligning the optics on my new machine. Only the second mirror needed adjustment. I disassembled the final mirror and lens assembly, deburred the sloppy edges on it, cleaned and reassembled it. 



After work Wednesday I installed the software (Corel/Moshi). I'm finding it adequate for now. I've used worse. The machine worked right out of the gate. I carved one of the sample files first just to make sure it did something. The pic attached to this post was my second attempt - a photo of my best friend and I. (Note to self -  take down the white point in photoshop to clip the peaks).



The third item I cut was a cardstock dogtag, but I had an issue with my file - it was an outline tracing of a small vector image - very jagged curves. To my amazement the machine followed the individual pixels on the curves (photo 2).



So needless to say, I'm pretty thrilled. I'm going to try some cuts tonight, as well as some sample parts for work (metal paper tube ends and paper discs).



![images/f09998f33f9b7574bd9ae68bdd2bf0a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f09998f33f9b7574bd9ae68bdd2bf0a7.jpeg)
![images/2c7ebed65a58893f8b85c8497fce3cfc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2c7ebed65a58893f8b85c8497fce3cfc.jpeg)

**"Gee Willikers"**

---
---
**12vLedLights** *July 24, 2015 00:29*

looks good, can't wait till I get mine


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/HjZXaMvxZub) &mdash; content and formatting may not be reliable*
