---
layout: post
title: "Posted this on the FB group and was not able to get any suggestions"
date: October 11, 2016 20:57
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Posted this on the FB group and was not able to get any suggestions. Hoping someone here might have a suggestion:



Hey folks, I wonder if anyone has a suggestion for this. I'm looking for a type of material that I can use as encoder wheels. I want to cut my own wheels to use with encoders. The current material (that they come with) is a fairly thin, not too flexible black plastic with the slits cut through. Does anyone have any ideas of what I can try? Something that I can run fast and get no melting along the edges, and thin/fine slits cut through. Ideally something non-toxic ... I do want to try and reach 80 some day. :) Using a stock K40 by the way.





**"Ashley M. Kirchner [Norym]"**

---
---
**Jim Hatch** *October 12, 2016 00:45*

Delrin or nylon


---
**Ashley M. Kirchner [Norym]** *October 12, 2016 01:22*

It has to be thin though, like the lid on a soda cup ...


---
**Ashley M. Kirchner [Norym]** *October 12, 2016 03:30*

Hrm, I figured those things will just melt under the laser ...


---
**Scott Marshall** *October 13, 2016 11:30*

Might be hard to find these days, we used to call it "fish paper", it's treated cardstock and used to be used as High voltage insulators in Ham gear etc. Heathkit used s lot of it. Usually a dull gray-green color.



Maybe they have a modern equal. 



I know what you want to do, and the commercial wheels are laser cut with a fiber laser I believe and from sheet steel shim stock.



Even with a well behaved medium, you may have trouble getting the counts/rev of a commercial wheel.



Ultimately I think a paper product will be the winner for CO2 lasering.



You can get up to 1000 count commercial wheels surprisingly cheap, do you need something special, like an odd count or size? It would seem the with a modern quadature setup you can pretty much stop anywhere you want with ready made commercial stuff and a fairly simple controller. 

(I'm just curious what your building because I like cool projects)




---
**Ashley M. Kirchner [Norym]** *October 13, 2016 17:12*

It's more the product that they need to fit in rather than the availability of what's out there. I have a very tight and small spot to fit them in, custom designed everything.


---
**Scott Marshall** *October 15, 2016 07:34*

I see. They sell a coated mylar designed for cutting stencils.



 The coating keeps a stack form fusing so you can cut multiple layers at once. (I believe it's the same material as anti-scratch coating)



I posted the source once in the last few weeks, but can't remember it right now and have tossed the lab since and don't see the package anymore. Green package... Name is a play on.. Got it Stenceze, no, something like that, EZ-stencil maybe. When I find it I'll post again. It's clear to visible light, but obviously not to IR. They make quite a lot of them from clear material come to think of it.



Scott



Stencilease? - still not sure


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/ah7UMuP9wv2) &mdash; content and formatting may not be reliable*
