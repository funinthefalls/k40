---
layout: post
title: "For a lot of reasons that I don't want to get into here, I would like to add a rotary to my STOCK K-40"
date: May 05, 2017 00:19
category: "Modification"
author: "Terry Taylor"
---
For a lot of reasons that I don't want to get into here, I would like to add a rotary to my STOCK K-40. I DID search here, but did not find an answer. I have no interest (at this time) in upgrading the electronics. I have looked at the rotary the LightObject sells, but they warn that the stock drivers cannot support the stepper on the rotary.  So my question to the group is, has anyone used the LO rotary with the stock control board. If so, did you use a different driver board or did you find a lower power stepper motor. Related, I have not looked that closely at the stock board, so the related question is if there is a dedicated connector for the rotary or do you need to unplug the existing Y axis stepper and plug in the rotary stepper. Any help would be GREATLY appreciated!





**"Terry Taylor"**

---
---
**HalfNormal** *May 05, 2017 00:49*

**+Terry Taylor** A quick search turned up the video on youtube


{% include youtubePlayer.html id="nAqu-836F5E" %}
[youtube.com - DIY K40 laser rotary attachment](https://www.youtube.com/watch?v=nAqu-836F5E)

If you go to his first posts, he states he is just swapping out connections on his nano (if that is what you have) Somewhere I also saw someone use the y axis to manually drive a rotary attachment but I cannot locate the source again.


---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2017 01:06*

There's a 50/50 chance (depending on how powerful of a motor the rotary uses) that you'll blow the board or possibly even psu (can never be too safe about this one) due to current overdraw. If it's the LO rotary, it definitely needs the external driver. In which case you need to replace the board with something like my Cohesion3D which will do X, Y, Z table, and Rotary. 




---
**Ashley M. Kirchner [Norym]** *May 05, 2017 01:07*

Yup, if you don't want to upgrade the electronics, then your only option is swapping out the current connections on the stock board as it only has an X and a Y. Honestly swapping out the electronics is something you should be considering. The Cohesion3D Mini is a drop-in replacement, just swap connectors from one board to it and boot it up. But I can understand if that's not something you want to do right now.


---
**Stephane Buisson** *May 05, 2017 08:54*

**+Ray Kholodovsky** have a point about limited power out of motor & psu. if a mecanical mod of one axis could seem appealing (with a ratio 1/1 as no software is needed), the probability to blow your actual board is very hight. (the PSU is rated for 1 amp, and in accordance with the 2 weak motors, increasing load isn't reasonable).

but that just the hardware side, if different ratio you will need the soft to compute the difference.



if you go for more than 2 motors, you would need external PSU with a new board. Software option exist on the open source side. 


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/Hqx7PzzB9R4) &mdash; content and formatting may not be reliable*
