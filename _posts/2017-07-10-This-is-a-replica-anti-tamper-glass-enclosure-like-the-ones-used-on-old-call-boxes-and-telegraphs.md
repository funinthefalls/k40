---
layout: post
title: "This is a replica anti-tamper glass enclosure like the ones used on old call boxes and telegraphs"
date: July 10, 2017 00:38
category: "Object produced with laser"
author: "Madyn3D CNC, LLC"
---
This is a replica anti-tamper glass enclosure like the ones used on old call boxes and telegraphs. The glass boxes you would have to break to sound an alarm or open a box.  Materials used were HD 2.5mm acrylic, clear epoxy, neodymium barrel magnets, and 3D printed PLA



<b>Click the link below for full restoration project</b> 

[https://plus.google.com/u/0/collection/UBkRDE](https://plus.google.com/u/0/collection/UBkRDE)



<i>Box design produced with</i>  [http://www.makercase.com/](http://www.makercase.com/)



3D Printed corner brackets w/ magnets





<b>Originally shared by Madyn3D CNC, LLC</b>



Created a custom CAD design for replica of glass box. Laser cut the acrylic box and 3D printed the corner brackets. Magnets hold the box in place on the cast iron. 



![images/520f1bea7d259142cf2853381185cfb1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/520f1bea7d259142cf2853381185cfb1.jpeg)
![images/e7cc59e2bc2262b64ebc88f3982d3573.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e7cc59e2bc2262b64ebc88f3982d3573.jpeg)
![images/c0893353ec10f0c76308388114066285.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c0893353ec10f0c76308388114066285.png)

**"Madyn3D CNC, LLC"**

---


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/K55rZHW3UeC) &mdash; content and formatting may not be reliable*
