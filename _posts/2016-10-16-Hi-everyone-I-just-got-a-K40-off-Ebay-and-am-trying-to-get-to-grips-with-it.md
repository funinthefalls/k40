---
layout: post
title: "Hi everyone. I just got a K40 off Ebay and am trying to get to grips with it"
date: October 16, 2016 14:29
category: "Discussion"
author: "Ian Wright"
---
Hi everyone. I just got a K40 off Ebay and am trying to get to grips with it. I have a couple of questions which I don't see answers to in the community (although I may have missed them - sorry if that is the case) . 

1 I want to be able to cut paper and card cleanly - I have seen suggestions that aluminium honeycomb is a good way to go and is used on a lot of high end lasers but I have also seen comments that it can cause a lot of problems with reflections onto the back of the work and transfer of dirt. Anyone have any suggestions?

2. I have CorelLaser and Corel Graphics Suite 12 from the supplied CD but have a couple of problems.. as has been mentioned, the Graphics part is a pirated copy and, as far as I can see, couldn't be 'registered' with the hack provided as all the installation instructions and the Winpatch program are in Chinese. However, I can't see how the CorelLaser program/extension could be used with any other graphics program as, when it is started, it complains if it can't find Corel Draw..  Also, when I am using it on my laptop, Corel Laser has a nasty habit of disappearing after each cut so that, if I want to engrave a piece and then cut it out, I have to do the engraving, then close down Corel Draw completely and start again before the Corel Laser toolbar will re-appear to allow me to do the cut. Is this something anyone else has come across and found a solution to? Comments please.. thanks





**"Ian Wright"**

---
---
**Ariel Yahni (UniKpty)** *October 16, 2016 14:40*

**+Ian Wright**​ Corel plugin goes to the system tray at the bottom right corner next to the clock. 


---
**Anthony Bolgar** *October 16, 2016 14:41*

The toolbar actually just minimizes to the system tray, you do not need to restart Corel. For the bed, you can use a pin bed if you are afraid of reflections damaging the underside of your work piece. Hope this helps, and welcome to the wonderful world of lasers.


---
**Jim Hatch** *October 16, 2016 15:07*

You don't need to register the included version of Corel. However if you have a legally purchased version or buy a legal copy then the plugin will work for those as long as it's not the Home & Student version (that gets installed in a different directory with different executable names than all the other Corel versions so the plugin can't find it). I use Corel X8 (latest version) myself on Windows 10 so it does work with legal copies of Corel. (I don't like pirated software for a whole bunch of reasons.)


---
**Ian Wright** *October 16, 2016 17:01*

Thanks guys, I have seen comments about the plugin icon putting itself at the bottom of the screen but can't find it on my computer - it just seems to disappear completely. Hopefully my brother is going to donate another old laptop in a few weeks so that may behave better..


---
**Jim Hatch** *October 16, 2016 23:52*

**+Ian Wright**​ what version of Windows are you running? The plugin icons go to the notification tray. That's on the lower right on the taskbar on the bottom of the screen near where the time, network and other icons are. In Win 10 there's a little upward pointing triangle that displays the hidden notification icons (like your printer status, etc). Click on it and one of the things displayed will be the icons from the plugin.


---
**Scott Marshall** *October 16, 2016 23:56*

I use a nail board as a backup for delicate materials. (for most stuff actually) 

It's just a sheet of cheap Luan plywood (1/4") with 1' long 17 guage Wire Brads in it on a 1" (or whatever you need to provide adequate support. It goes together easy if you predrill holes about 70% the diameter of the brads



It offers several advantages over  commercial honeycombs. 

Cost - about a buck if you have plywood laying about (and most laserheads do) - any ply will work, 3mmbb is good too, but I hate to use it for such a basic purpose.



Reflections - there are none. By the time the laser hits the plywood, it's so defocused it hardly marks it, and scatters completely, with only incidental levels coming back at the workpiece. There seems to be little effect if you do directly hit a brad, but it's rare as well.



Cleanliness - the airgap under the workpiece carries away smoke quickly. The downside to this it if you have a fire start, it burns fast and hard because of the air availability. Make sure your air assist head is working well and it will keep fires at bay. Don't walk away.



Versatility - A custom nailboard can be built in a few minutes for special purposes like odd shaped objects or very flexible materials like felt.



Focus - easily shimmed or you can put jack screws in the corners (2" #8 or #10 wood screws work great -get the kind with the nut head and you can use a nutdriver to adjust) - then it's just a screwdriver turn from dialed in. Once leveled to your cabinet's 'twist' just count the turns and it will remain leveled.



Scott










---
*Imported from [Google+](https://plus.google.com/114833214086356299537/posts/CMmh9rMju4j) &mdash; content and formatting may not be reliable*
