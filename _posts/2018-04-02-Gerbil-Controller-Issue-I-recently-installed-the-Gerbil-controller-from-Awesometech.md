---
layout: post
title: "Gerbil Controller Issue I recently installed the Gerbil controller from Awesome.tech"
date: April 02, 2018 16:06
category: "Discussion"
author: "Corey Mousseau"
---
Gerbil Controller Issue



I recently installed the Gerbil controller from Awesome.tech.  Almost everything is working fine though with one major issue.  For some reason my machine will not travel in the -X direction.  



When using either LightBurn or LaserWeb, when I control the machine I can get it to move in + and -  Y directions as well as +X.  Whenever I send a signal to move in the -X direction the machine moves but +X rather than -



I have tried using the software controls as well as sending gcode.  the code sends properly but the movement is wrong.  This is NOT a reverse software or wiring issue as it moves in the positive direction whenever I send +X and -X commands. 



I am having a hard time searching for for this issue, though I have been able to fine folks having similar problems with other CNC machines, in all of those cases it appears the Arduino itself is the culprit.  



I would like to try a different Arduino on it (I have an UNO available), but I have no idea how to compile the Arduino properly to allow it to work with the Gerbil shield.  



Could anyone offer some help here?  I posted this in the Awesome.Tech forum as well though I feel this group has more traffic.  



Thanks!





**"Corey Mousseau"**

---
---
**greg greene** *April 02, 2018 17:02*

Same board - same problem - but on the y axis

X- ans X + work fine

Y- and Y+ both move in same direction

which is past the home poition on my machine

Home works - IF - the carriage is manually moved to a few inches of the correct spot - Top Left - other than that - just a lot of grinding.


---
**Dan Stuettgen** *April 02, 2018 17:26*

Try checking your stepper motor wiring.  You might have a loose or disconnected wire.


---
**Corey Mousseau** *April 02, 2018 18:28*

**+greg greene** interesting... Has this been like this since you installed it, or did this occur after at some point?  Mine has been wrong from the get go, I just did the install the other day.  


---
**Corey Mousseau** *April 02, 2018 18:29*

**+Dan Stuettgen** Unfortunately it does not appear to be the wiring.  I have gone through every wire and it is all well connected and secure.  The old controller works just fine with them, it is only when I connect it to the Gerbil does this happen. 


---
**greg greene** *April 02, 2018 19:10*

**+Corey Mousseau** 

From the get go with this board -worked better with the cohesion board


---
**Corey Mousseau** *April 04, 2018 13:56*

I still have this problem and cannot figure it out! Is it possible to use a normal Arduino Uno with this shield? If so how do I compile it?


---
**Anthony Bolgar** *April 05, 2018 14:33*

Normal Uno will not give you the enhanced engraving, but it would work like a standard uno with grbl.


---
**greg greene** *April 09, 2018 14:39*

any luck fixing this?


---
**Corey Mousseau** *April 09, 2018 20:10*

**+greg greene** no, not at all, I'm surprised too,  Paul seems to be very active helpful usually. I'll try to contact him directly if I can. I'll get back to you if I figure it out.


---
**greg greene** *April 09, 2018 20:48*

Paul is great, but he lives on the other side of the world with a 15 hour time difference from me 


---
**Corey Mousseau** *April 12, 2018 19:58*

**+greg greene** I ended up contacting Paul via email, which was likely the better option to begin with. He got back to me quickly. There is a fix! For me it was a bad stepper driver. I had an extra one I tested it with and works! You can test that yourself even if you don't have an extra one available, swap the two drivers (x and y) to see if your problem switches to the other axis, if so then you know it's a bad stepper driver.


---
*Imported from [Google+](https://plus.google.com/100237834106890099351/posts/PAvC81KFHmD) &mdash; content and formatting may not be reliable*
