---
layout: post
title: "If anyone is looking for a free editing program to creating 3d models and then slicing them into 2d parts i highly recommend Autodesk's 123d make program"
date: May 28, 2016 15:58
category: "Software"
author: "PrettyChill Chemistry"
---
If anyone is looking for a free editing program to creating 3d models and then slicing them into 2d parts i highly recommend Autodesk's 123d make program. Super simple to use and does an amazing job. They also have 5 other programs specifically designed to edit 3d models and create cutting files.





**"PrettyChill Chemistry"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 28, 2016 18:48*

They are a great suite of products. I've been using 123d design to design my air-assist nozzle & had a play around with 123d make for 3d sliced wooden models.


---
*Imported from [Google+](https://plus.google.com/106304220507326054761/posts/FXPnHxGbDyW) &mdash; content and formatting may not be reliable*
