---
layout: post
title: "Everytime i start a job, and it runs"
date: October 11, 2016 20:55
category: "Software"
author: "Kurtis Van Kampen"
---
Everytime i start a job, and it runs.  CorelLaser disappears and I have to close corel and restart.



Any thoughts on how to keep the tool active?





**"Kurtis Van Kampen"**

---
---
**greg greene** *October 11, 2016 21:05*

usually it moves itself to the toolbar down on the right side - just click it and it will appear again.


---
**Kurtis Van Kampen** *October 12, 2016 16:45*

nope, its totally gone




---
**greg greene** *October 12, 2016 17:54*

There is a button to click to show it, Under properties I think - but I'm not sure and I'm not at that particular computer - I know others have had this happen - perhaps search the K40 blog for an answer.


---
*Imported from [Google+](https://plus.google.com/109717621975017915534/posts/QhB2Suq6tLA) &mdash; content and formatting may not be reliable*
