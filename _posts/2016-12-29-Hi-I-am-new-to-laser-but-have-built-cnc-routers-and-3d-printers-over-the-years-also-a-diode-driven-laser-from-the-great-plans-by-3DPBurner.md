---
layout: post
title: "Hi, I am new to laser but have built cnc routers and 3d printers over the years also a diode driven laser from the great plans by 3DPBurner"
date: December 29, 2016 11:47
category: "Hardware and Laser settings"
author: "Richard Treherne"
---
Hi, I am new to laser but have built cnc routers and 3d printers over the years also a diode driven laser from the great plans by 3DPBurner. I have learnt a great deal from this excellent resource and have ordered the Cohesion 3d mini laser bundle to do the upgrade that seems to be the best to make the k40 a better machine, my question is what do you guys do about the bubbles that collect in the tube? is there an easy fix, is it ok to rotate the tube so that the outlet is at the top. Thanks for your patience with a newbie





**"Richard Treherne"**

---
---
**Don Kleinschnitz Jr.** *December 29, 2016 12:07*

Here is what I have heard on forums. I tip my machine to remove bubbles cause I do not want to realign it. Plan to rotate the tube first time alignment is necessary.



1. Keep drain side under water

2. Put tank above level of machine

3. Use a couple of drops of dish detergent to reduce surface tension

4. Rotate tube (this may cause arching if anode gets to close to case). Optics will likely need realignment. 



I have only implemented #1.


---
**greg greene** *December 29, 2016 13:32*

The bubbles will work themselves out over time - no worries


---
**Don Kleinschnitz Jr.** *December 29, 2016 13:44*

**+greg greene**​ huh! mine stays unless I tip the machine. Not good to run with bubbles.


---
**greg greene** *December 29, 2016 13:49*

My pump and reservoir are at the same level as the tube is, so I get full pressure, that seems to clear the bubbles quickly.  Is your pump below the machine on the floor?


---
**Stephane Buisson** *December 29, 2016 14:24*

you can also use something like that to push the buble out (with a slight tilt of the all K40) :

[aliexpress.com - 6MM Rubber And Aluminum Fuel Line Pump Primer Bulb Hand Primer Gas Petrol Pumps Generator Accessories free shipping](https://www.aliexpress.com/item/6MM-Rubber-And-Aluminum-Fuel-Line-Pump-Primer-Bulb-Hand-Primer-Gas-Petrol-Pumps-free-shipping/32654890456.html?spm=2114.01010208.3.1.f0iNIu&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10000009_10084_10083_10080_10082_10081_10060_10061_10062_10056_10055_503_10054_10059_10099_10078_501_10079_427_426_10103_10073_10102_10096_10052_10050_10107_10051_10106,searchweb201603_2,afswitch_5&btsid=752064a6-77cf-4d2d-9585-b5f0134c427b)


---
**Don Kleinschnitz Jr.** *December 29, 2016 14:33*

**+greg greene** my pump is below, will have to try that?


---
**greg greene** *December 29, 2016 14:35*

If you get your pump a little higher - the bubbles will naturally rise to that level instead of the tube


---
**Richard Treherne** *December 29, 2016 16:08*

Ah ok, so not just me being new then, I do have the pump under the cutter so I will lift it up and give that a try. I did try to shift the bubbles with an air pump, that did work initially but they returned the next time I fired it up, I will also try the trick of lowering the surface tension. Thanks guys at least I don't feel so bad about it now :-)


---
**greg greene** *December 29, 2016 16:11*

there is a product at the speed shops called water wetter that the hot rod guys use to reduce cavitation around the water pump impeller and keep bubbles from forming - haven't used it in this situation though


---
**Richard Treherne** *December 29, 2016 16:19*

Yes I bought some flow improver to mix with water to make acrylic paint flow better in airbrushes, it was expensive I remember and I suspect it was just detergent as Don said, so I'll give that a go, as well as lift the pump. Thanks again.


---
**Don Kleinschnitz Jr.** *December 29, 2016 16:21*

Just make sure whatever u use is not conductive 😊


---
**Richard Treherne** *December 29, 2016 16:50*

Hi Don is that why distilled water is best and not deionised?


---
**Don Kleinschnitz Jr.** *December 29, 2016 16:52*

**+Richard Treherne** yup! 


---
**greg greene** *December 29, 2016 16:53*

Yup Again !


---
**Richard Treherne** *December 29, 2016 17:24*

Ok :-)


---
**Mike Mauter** *December 29, 2016 19:03*

The last tube that I bought actually had the water outlet rotated so that it was at the top when the tube electrode was pointing up so the bubbles rose to the top and had an exit.


---
**Don Kleinschnitz Jr.** *December 29, 2016 19:09*

**+Mike Mauter** and problems with anode to close to case?


---
**Mike Mauter** *December 31, 2016 16:33*

No problems so far. The water outlet is actually formed/cast/blown into the glass where it is at the top of the tube when the anode is pointing up. Other tubes that I have had had the outlet on the side or bottom when the anode is pointing up.

![images/19b4c370b2737d8aed23b354c42ec37f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19b4c370b2737d8aed23b354c42ec37f.jpeg)


---
*Imported from [Google+](https://plus.google.com/117933873290891611475/posts/b98tAARkw5x) &mdash; content and formatting may not be reliable*
