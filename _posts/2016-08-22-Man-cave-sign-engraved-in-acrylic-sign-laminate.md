---
layout: post
title: "Man cave sign engraved in acrylic sign laminate"
date: August 22, 2016 03:48
category: "Object produced with laser"
author: "Vince Lee"
---
Man cave sign engraved in acrylic sign laminate. 

![images/a7fa983286c60bdffe016ac5674694ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a7fa983286c60bdffe016ac5674694ff.jpeg)



**"Vince Lee"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 22, 2016 04:05*

Love it :)


---
**Gee Willikers** *August 22, 2016 04:38*

Wonderful.


---
**Bob Damato** *August 22, 2016 14:10*

Oh this is really neat! Where do you get the material? care to share the process?


---
**Vince Lee** *August 22, 2016 17:07*

It's two-layer acrylic laminate designed for laser or rotary engraving.  I got it from [inventables.com](http://inventables.com).  They have lots of different acrylic and wood sheet materials and it's generally my go-to place as they sell them in 8x12 sheets perfect for the k40.


---
**Bob Damato** *August 23, 2016 13:45*

**+Vince Lee** did you have your spot out of focus to get this result? If so, by how much?


---
**Vince Lee** *August 23, 2016 23:00*

Nope I just raster engraved it although I did end up making two passes because the first was too shallow.


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/jLiaV8R9mUE) &mdash; content and formatting may not be reliable*
