---
layout: post
title: "Chuck Comito and I have been musing the practicality of running a 60W laser with a 40W LPS"
date: February 24, 2018 16:25
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
**+Chuck Comito** and I have been musing the practicality of running a 60W laser with a 40W LPS.



This blog post summarizes our thinking.

[http://donsthings.blogspot.com/2018/02/will-40-watt-lps-drive-60w-laser.html](http://donsthings.blogspot.com/2018/02/will-40-watt-lps-drive-60w-laser.html)



The jury is still out at to whether this configuration has associated problems but we thought we would share this info with the community. 



Will keep you updated as **+Chuck Comito** completes testing.









**"Don Kleinschnitz Jr."**

---
---
**Anthony Bolgar** *February 24, 2018 17:41*

I run a 60W on a 50W PSU without any issues.


---
**Matt Staum** *May 13, 2018 01:17*

**+Don Kleinschnitz** Are your numbers for a 1000mm or 1250mm tube? It seems like the reported wattage varies significantly between LO and various ebay sellers.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/AxvaivN6kmY) &mdash; content and formatting may not be reliable*
