---
layout: post
title: "Ray Kholodovsky This may be a rudimentary question, but what data will I be able to see on the LCD if I get the GLCD adapter and the LCD display with my cohesion3d mini laser upgrade bundle?"
date: February 22, 2017 06:49
category: "Smoothieboard Modification"
author: "Arion McCartney"
---
**+Ray Kholodovsky** This may be a rudimentary question, but what data will I be able to see on the LCD if I get the GLCD adapter and the LCD display with my cohesion3d mini laser upgrade bundle?  Thanks.





**"Arion McCartney"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 22, 2017 15:37*

Machines coordinates, laser fire status. It's a good heads up display to have. According to the smoothie guys the LCD is a requirement for the CNC firmware we are running so maybe that helps put things in perspective. 


---
**Arion McCartney** *February 22, 2017 15:40*

**+Ray Kholodovsky**​I see. Thanks.


---
**Wolfmanjm** *February 23, 2017 05:52*

actually I have never said the glcd is a requirement. in fact i do not run one at the moment on my laser. it is definitely optional!


---
*Imported from [Google+](https://plus.google.com/110543831642314750018/posts/TyHjzW7i9uN) &mdash; content and formatting may not be reliable*
