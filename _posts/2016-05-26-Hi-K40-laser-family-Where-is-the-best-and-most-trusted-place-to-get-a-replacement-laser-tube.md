---
layout: post
title: "Hi K40 laser family! Where is the best and most trusted place to get a replacement laser tube"
date: May 26, 2016 11:25
category: "Hardware and Laser settings"
author: "Anthony Santoro"
---
Hi K40 laser family! 

Where is the best and most trusted place to get a replacement laser tube. Are there stronger laser tube available that will fit in the K40 machine? 

I live in Australia, have any Aussies had experience with having a tube shipped to them? 

Thanks! 

Have a great night! 

Anthony 





**"Anthony Santoro"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 11:52*

I think **+I Laser** would be of advice as to who not to use. He recently purchased one (I think he's in VIC somewhere) & has had nothing but dramas. I'm in QLD, but I've yet to purchase a replacement as mine is still kicking along quite well.


---
**Anthony Santoro** *May 26, 2016 12:13*

Thanks **+Yuusuf Sallahuddin**​, you raise a good point. Finding out who NOT to buy from is just as important as who to buy from. There are some troublesome shops, best that I avoid the bad ones. 


---
**I Laser** *May 26, 2016 22:06*

**+Yuusuf Sallahuddin** Ha, can't provide who to buy from at this point! The tube I've got fires fine when the bubbles aren't there, the design is something I would recommend people stick clear of! Unfortunately there's only two listing for tubes on ebay.au and both are the same design. The other seller has pretty poor feedback.



The tube came in one piece, the packing wasn;t anything spectacular, which was surprising considering the horror stories I'd read previously.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 22:20*

**+I Laser** So, would you recommend Anthony get his tube from the supplier you did? Or nah?


---
**I Laser** *May 26, 2016 22:26*

NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO.............................................



Though unless you're purchasing from somewhere other than ebay you don't have any choice :|


---
**Anthony Santoro** *May 26, 2016 22:47*

Haha thanks for the tip **+I Laser**​. I read through your post regarding the bubbles. Sounds like a headache! Is the tube cutting okay now? Were the bubbles affecting the laser power? 


---
**I Laser** *May 27, 2016 08:01*

Haven't actually managed to test cutting yet, it's been a long arduous effort for sure. When I previously tested the tubes power it was good and would no doubt cut okay. If my assumptions are correct, it will mean replacing my existing flow switch arrangement too.



The bubbles don't affect the power, they can cause hot spots and fracturing of the tube, ie kill the tube completely. Might be a bit paranoid, but even with the minuscule bubbles I had, I wouldn't fire the tube.


---
**Greg Curtis (pSyONiDe)** *May 27, 2016 12:05*

I had a good experience with Laser Depot, they are in Las Vegas, NV USA. I got the 50w and put an extension on my cabinet.


---
**Anthony Santoro** *June 18, 2016 03:54*

Gents, is the degradation of the laser tube linear or exponential? 

I've found that in the last 2 weeks I've lost power significantly faster than I have in the last 6 months! What is your experience? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 18, 2016 07:01*

**+Anthony Santoro** No idea on that, although **+Scott Thorne** may be able to comment as he purchased a laser power meter if I recall correct.


---
**Scott Thorne** *June 18, 2016 10:41*

I purchased the power meter from bell laser for 100.00 u.s dollars.


---
*Imported from [Google+](https://plus.google.com/106649411786258336023/posts/4qCpHqggdyC) &mdash; content and formatting may not be reliable*
