---
layout: post
title: "Hopefully I am sharing this video correctly, however after some discussions I decided to make this video of squaring the gantry to the primary mirror"
date: November 27, 2015 18:40
category: "Discussion"
author: "DIY3DTECH.com"
---
Hopefully I am sharing this video correctly, however after some discussions I decided to make this video of squaring the gantry to the primary mirror.  Worth note is assumed that the sheet metal was close to being square, well in China "square"  does not seem to be square yet it did get me started as in the end  the secondary mirror moved over 1/4 inch inward from the top of the rail to the bottom.  I replaces the 3/8" bolts with #8 to give myself some extra wiggle room and after two plus hours of trial and error was down to a little less then a 1/8 however must admit getting past this  might be hard as there is close to 1/16 slop in the gantry itself.  Whew that was a long couple hours and hope this helps...





**"DIY3DTECH.com"**

---
---
**Scott Thorne** *November 27, 2015 19:13*

You misunderstood what I was taking about squaring up the rail that the head is on.....you are squaring up the whole carriage with the frame, that's not what I meant.


---
**Scott Thorne** *November 27, 2015 19:15*

Your head carriage might still be out of square causing serious power loss when the head is extended out to the right side of the machine.


---
**Scott Thorne** *November 27, 2015 19:16*

What you did can be compensated with mirror adjustments....the head carriage cannot.


---
**DIY3DTECH.com** *November 27, 2015 19:20*

Yupper :-) Understand now and did check and mines good from that aspect...


---
**DIY3DTECH.com** *November 27, 2015 19:31*

The head carriage is good as after a couple hours moving the gantry and firing the laser I have everything very close.   As noted in the video when I went to realign the mirrors after changing the gantry position I found I was still off from corner to corner (which is a big area for this machine).  So instead of measuring, would fire the laser and move the gantry, fire the laser move some more.  Well you get the idea.  I did a video on this but deleted it accidentally (insert me crying).  Any however after doing this I m down to like a ~1/16 error and can live with it.  Any one that follows this the gantry may look racked in the sheet metal the sheet metal (at least for me ) is not as square as I would have guessed


---
**Scott Thorne** *November 28, 2015 02:37*

I wish mine were...I spent over a week trying to align it with just the mirrors to no avail.....lol


---
**DIY3DTECH.com** *November 28, 2015 03:04*

Scott I have to thank you as I would have never though to look at this if it where not for your other comments.  While I did miss understand it did get me looking at this and this did make a big difference at the end of the bed (i.e. far corner)., I ordered the new lens and it has shiped along with I will likely convert the machine to GRBL.  With this I think I am going to design laser aligning jig using a low power laser to align the gantry to the primary mirror as as having a consent visible beam to align will make the job easier as frankly grew tired after two hours...  


---
**Scott Thorne** *November 28, 2015 03:06*

I know what you mean...it took me about 5 to square it and get it aligned...that was spread over  3 days....lol


---
**Gary McKinnon** *November 28, 2015 12:56*

Nice work both of you because it could be a major issue for a lot of us. I've ordered a set square. I like the sound of the laser-laser-aligner :) If you tell me what media your video was on i can point you to some free undelete software.


---
**DIY3DTECH.com** *November 28, 2015 16:01*

Was Galaxy S5 and i deleted the wrong file :-( That Murphy for you!  


---
**Scott Thorne** *November 28, 2015 16:05*

Thanks Gary....just trying to share what I've learned.....I downloaded the sketchup....nice!


---
**Gary McKinnon** *November 28, 2015 16:32*

**+DIY3DTECH.com** I can't find a free one for Android but this trial may help :



[http://www.easeus.com/android-data-recovery/recover-deleted-files-from-samsung-galaxy-s5.html](http://www.easeus.com/android-data-recovery/recover-deleted-files-from-samsung-galaxy-s5.html) 


---
**Scott Thorne** *November 28, 2015 16:34*

Ok guys...stupid question...how do highlight someone's name...like you just did with DIY3DTECH ?


---
**Gary McKinnon** *November 28, 2015 16:49*

Do a plus sign then start typing their name immediately after the plus with no space and it bings up a selection box


---
**Gary McKinnon** *November 28, 2015 16:49*

**+Scott Thorne** No such thing as a stupid question :)


---
**Scott Thorne** *November 29, 2015 01:10*

**+Gary McKinnon**.....got it....thanks lol.


---
*Imported from [Google+](https://plus.google.com/+DIY3DTECHcom/posts/53Ns1Zt75pj) &mdash; content and formatting may not be reliable*
