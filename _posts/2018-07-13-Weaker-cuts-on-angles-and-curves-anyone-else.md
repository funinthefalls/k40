---
layout: post
title: "Weaker cuts on angles and curves, anyone else?"
date: July 13, 2018 22:16
category: "Discussion"
author: "Joseph Rennie"
---
Weaker cuts on angles and curves, anyone else?



Hiyas,



I'm cutting 3mm mdf. I get a nice cut, going all the way through through @ 50% power @ 6mms. However this is only when the cuts are straight vertical/horizontal.



If the cut is diagonal or curved, to get the cut going all the way through I have to bump to power up to 70%.



Is this a common issue or is it particular to my set up?



- K40 whisperer

- Mirrors are pretty well aligned aligned (no noticeable movement at corners when doing alignment check) and clean.



My hunch is that the lens is moving faster when both stepper motors are engaged, providing a weaker cut?



(Hope I posted this in the right place, I'm new to using google+)





**"Joseph Rennie"**

---
---
**James Lilly** *July 14, 2018 02:12*

How old is the machine? Original mirrors? 50% @ 6 mm seems too much and you should burn right through and then some.  I can go around >25 % @ 11mm and usually burn through


---
**Joseph Rennie** *July 14, 2018 02:21*

**+James Lilly** Hiya James, thanks for replying. 



Less than a month old, original mirrors and lens. 



That's a lot faster than I'm able to cut - so there may be something faulty?


---
**James Lilly** *July 14, 2018 02:35*

I cracked a mirror the first time I used mine and bought a set of three.  I just replaced the one.  Started losing a little power and replaced the other two and it was way better.  Depending how you've set up your bed, the distance from the lens to the material to be cut could be off as well.


---
**Joseph Rennie** *July 14, 2018 06:56*

**+James Lilly** I've checked the focal distance, am at approx 50mm (lens is 50.8 focal distance). 



Any closer or further away the cuts are shallower. 


---
*Imported from [Google+](https://plus.google.com/116541129523734345186/posts/FmpSDNdBZiS) &mdash; content and formatting may not be reliable*
