---
layout: post
title: "because it's also apply to Lasercutting, you will find here a great way to dive in with Fusion 360"
date: November 09, 2016 17:19
category: "Software"
author: "Stephane Buisson"
---
because it's also apply to Lasercutting, you will find here a great way to dive in with Fusion 360. (also compatible Smoothie, like Laserweb and Visicut).





**"Stephane Buisson"**

---
---
**Anthony Bolgar** *November 09, 2016 17:35*

I have skimmed through the lessons and there is some great info in there. I posted this in the LaserWeb community this morning.


---
**Anthony Bolgar** *November 09, 2016 17:37*

A community member has also created a Fusion360 post processor for LaserWeb, it will be posted in the Wiki section of LaserWeb soon.

  Correction---It is a Vectric/Aspire post processor, sorry for the confusion.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 09, 2016 21:03*

Instructables are outdoing themselves lately, with all these awesome classes to join :) I've enrolled in many of them that I haven't even had time to look at yet.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/YoWEdN95En8) &mdash; content and formatting may not be reliable*
