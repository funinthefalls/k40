---
layout: post
title: "Does anyone help me to change moshidraw software?"
date: June 08, 2017 09:06
category: "Discussion"
author: "Moldes Vazados"
---


Does anyone help me to change moshidraw software? I can not finish a job.



Is there an upgrade kit I can buy? 

A complete, easy to install kit that I just need to install on the machine?





**"Moldes Vazados"**

---
---
**Ashley M. Kirchner [Norym]** *June 08, 2017 10:02*

You can upgrade the controller board, get rid of Moshidraw. Look into a Cohesion3D Mini bundle. 


---
**Moldes Vazados** *June 08, 2017 10:10*

**+Ashley M. Kirchner** Its simple to upgrade? Need soldering and stuff or just plug it in?




---
**Joe Alexander** *June 08, 2017 10:19*

same one i linked moldes :) ashley I beat you to it heh


---
**Ashley M. Kirchner [Norym]** *June 08, 2017 16:46*

It's a drop-in replacement. No soldering, just swap the connectors from one to the other. Put the SD card in and turn it on. Install LaserWeb on your computer and start importing your designs and get the laser to sing. 


---
**Ashley M. Kirchner [Norym]** *June 08, 2017 16:48*

**+Joe Alexander**​, no link, where see link? (think Johnny 5)


---
**Joe Alexander** *June 08, 2017 17:37*

lol NEED IN...PUT!!! lol on the bottom of my first post its there but just in case its [cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Ashley M. Kirchner [Norym]** *June 08, 2017 18:02*

Different post perhaps? (not that it matters, you've linked it now for Moldes.)


---
**Joe Alexander** *June 08, 2017 19:39*

very true, and on my google+ its the first post on this line. you know, right before yours? ;) totally semantics i know but how else do you make the day go by? heh. plus we all know that the C3D mini is the best drop in replacement to surface among the runners.(followed by smoothie then arduino/grbl in my opinion)


---
**Ashley M. Kirchner [Norym]** *June 08, 2017 22:16*

Is this where I say, "You mean there are other options besides C3D?" - that's like someone asking me what flavor ice cream I like. I always say, "You mean, there's something other than chocolate?"


---
*Imported from [Google+](https://plus.google.com/110389043467769074353/posts/1YodRcKErzj) &mdash; content and formatting may not be reliable*
