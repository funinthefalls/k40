---
layout: post
title: "i was following padillas coustom leather I think I have the same problem as he has I press the test switch and the meter shows that the laser has fired but there is nothing the laser tub does not light up and no output from"
date: September 18, 2017 15:54
category: "Hardware and Laser settings"
author: "Allan Knapper"
---
i was following padillas coustom leather I think I have the same problem as he has I press the test switch and the meter shows that the laser has fired but there is nothing the laser tub does not light up and no output from the laser,i had tape over the mirror but not a mark on the tape





**"Allan Knapper"**

---
---
**Allan Knapper** *September 18, 2017 16:00*

I am at a loss at what to do next any ideas


---
**Nate Caine** *September 18, 2017 16:17*

Work your way backwards.  

When was it last working?  

What happened since then?



(1) What does the <i>"Current Indication"</i> meter show?  What actual current number?  Is it steady as you press the test button?  



(2) When you say you pressed the <i>"test switch"</i> which do you mean?  

     (a) the <i>"TEST"</i> switch on the power supply?  (often mislabled as "TEXT"), or

     (b) the <i>"Test Switch"</i> on the front panel?  (Did you make certain that the <i>"Laser Switch"</i> on the front panel is in the enable position?)



(2) Did you set the <i>"Current Regulation"</i> dial all the way to maximum?  Have you verified the dial potentiometer works and the wiring is good?



(3) When you do a test fire, do you see or hear anything?  Arcs?  Sizzle, fizzle, snaps or cracks?  Smell any ozone?



Any photos of your panel and wiring?



Too often people post a problem here and forget to mention a key modification that they made because they're convinced their mod couldn't possibly affect anything.



(I'm perplexed that you say the meter shows current, yet the tube doesn't lite.  So I'd like to know what current the meter indicated.) 


---
**Allan Knapper** *September 18, 2017 21:22*

thanks for your reply the current meter shows about 10, 2 I pressed both and it does say text on the power supply,2 again I did not set the current reg to max but I will in the morning and be back to you, the wiring is good I have checked it pressing the test switch there is a definite sizzle will get photo to you tomorrow I have made no mods it is a brand new machine the instructions are absolutely rubbish and I keep thinking it something I have done or not done


---
**Jeff Lamb** *September 18, 2017 21:36*

The best way to test the laser is to put some tape on the end of the laser tube or on the first mirror (you may have done this but you haven't said which mirror you tested at). If you are getting current on the meter then it means the tube should be emitting something add the current meter is on the negative side of the tube. 


---
**Padilla's custom leather** *September 19, 2017 01:30*

during the weekend I did discover that the tube is firing and today I had a little time and I went out and put a piece of card stock in front of the 1st mirror as well as second to find that it is firing but is way off, but it could be we have 2 different problems, good luck in finding a solution to your problem, good people here to help out.  


---
**Allan Knapper** *September 19, 2017 16:05*

hi back again just done what you surgested and turned the dial to max and pressed the test button the reaction was a flash at the end wear the main red cable is connected to the tube I have taken all the silicon off and made the end again I waiting now for the silicon to set again and will try again


---
*Imported from [Google+](https://plus.google.com/110451180302563489337/posts/3iawxYBRrxN) &mdash; content and formatting may not be reliable*
