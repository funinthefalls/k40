---
layout: post
title: "Does anyone know if this Thingiverse design fits the 40W tube diameter?"
date: April 12, 2017 21:46
category: "Modification"
author: "Bob Buechler"
---
Does anyone know if this Thingiverse design fits the 40W tube diameter? 



[http://www.thingiverse.com/thing:1263096](http://www.thingiverse.com/thing:1263096)





**"Bob Buechler"**

---
---
**Mark Brown** *April 12, 2017 22:03*

That says it will fit tubes up to 55mm diameter.  The sort of orange layer in contact with the tube is cork, he says he used 1/16" but you can use thicker (or multiple layers I suppose) to shim up for smaller tubes.



My tube measures 51mm, so you'd need an extra 2mm of cork, which would get you up to <b>almost</b> 1/8" total (assuming it's 1/16" for a 55mm tube).  I assume between the cork and the 3d print there would be enough give that the slight (0.5mm) overage wouldn't matter.



Disclaimer: I know nothing about anything, listen to me at your own risk.


---
**Bob Buechler** *April 12, 2017 22:09*

LOL -- Thanks for the second opinion, **+Mark Brown** :) Interesting idea to just shim it up with more cork. Anyone think that's a bad plan?


---
**HalfNormal** *April 12, 2017 22:52*

**+Bob Buechler** Here is one made for the 40 watt tube

[thingiverse.com - 40 Watt CO2 Laser Tube Clip Remix by CallJoe](http://www.thingiverse.com/thing:1581069)


---
**Bob Buechler** *April 12, 2017 23:07*

Thanks, **+HalfNormal** -- Same design, just a 40W tube diameter? Or were other changes made?


---
**HalfNormal** *April 12, 2017 23:11*

diy3dtech has made it so it fits the K40. see the videos on the thingiverse link.


---
**Andy Shilling** *April 13, 2017 06:30*

I have that on mine but personally I think it would fit better if it was about 5% smaller. It's very tight in the casing and there isn't a lot of adjustment making it pointless IMO.


---
**Don Kleinschnitz Jr.** *April 13, 2017 10:58*

**+Bob Buechler** interesting subject, tube mounts. 



I planned to change mine to some decent mounts once the tube dies and I finally decide to rotate the tube to stop bubbles. I haven't rotated it in fear that it will arc.



Then again I pondered is that really necessary since I will adjust this once and lock it down??



Perhaps we should do what I have seen one other do is make the entire tube and mount one assembly that is removable and adjustable. 



This is an example of that approach.




{% include youtubePlayer.html id="NVzCMIq8dlc" %}
[youtube.com - RDWorks Learning Lab 98 A beaming new world](https://youtu.be/NVzCMIq8dlc)


---
**HalfNormal** *April 13, 2017 12:54*

**+Andy Shilling** Anything is going to be better than the stock crap that is on mine now. I have no way other than to shim and bend the metal bands that hold it in place which mean I have to take it apart each time I need to adjust it. Most of my issues come from the case being thin and flexible. Funny thing is that I have one that is about 3 years old and the case is thicker and heavier than the newer one. In fact the newer one is so thin that they had to add stiffening supports on the bottom to keep it from flexing. The tube holder is also better on the older machine. One day I will build a better case. One day....


---
**Andy Shilling** *April 13, 2017 13:06*

**+HalfNormal**​ the biggest problem I found with this was because it is such a tight fit I ended up taking the whole tray out to fit it but to do that i had to take out my bed and dividing plate between PSU and bed etc. 

Royal pain and I would like to come up with something better at some point if I find time.


---
**Don Kleinschnitz Jr.** *April 13, 2017 13:08*

**+Andy Shilling** thats why I am thinking that an acrylic sub plate like the video I posted, might be the best approach? You mount the tube on a plate that has adjustable mounts to get it parrallel and then the whole assy bolts into the frame. That way the tube is referenced to the plate not the freaky frame of the machine in two places.


---
**Bob Buechler** *April 13, 2017 16:27*

I've become increasingly convinced that decent tube mounts will greatly simplify beam alignment by greatly decreasing the need to perform mirror alignments. As DIY3DTECH mentions in those Thingiverse videos, if everything is square, true 45-degree mirrors should be all that's necessary. So true up the stock mirrors to 45 degrees, and perform tube alignment adjustments and mirror mount position changes from there.



I think I agree though that a position-adjustable bottom plate design that provides a small amount of vertical and horizontal play is the cleaner approach here. The tube would rest in a C-shaped cradle (open end facing up). Should be pretty easy to adapt the current Thingiverse adjustment wheel design to this idea.


---
**Steven Kalmar** *April 15, 2017 02:34*

I'm currently working on my own design.  I'm hoping it will offer plenty of adjustment.  The first test mount is printing now.

![images/c4a99edde36637163fadf75ac3bc3464.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c4a99edde36637163fadf75ac3bc3464.png)


---
**Bob Buechler** *April 15, 2017 02:55*

**+Steven Kalmar**​ Very cool! How will it work?


---
**Steven Kalmar** *April 15, 2017 03:48*

The small piece on top bridges both sides of the frame.  Same thing goes on the side in front, but the picture doesn't show it.  A couple screws are bolted to the tube clamps and extend out so they can be used for fine adjustments.  Nuts on the adjustment bridge ride on these screws and are use to adjust the tube on the 2 planes.  The 2 larger holes through each frame will have a couple bolts through them to tighten the frame ends against the tube clamp to make everything solid.  Then the adjustment bridge pieces can be removed, or just left in place.  They will snap on and off and slide in the slots on the side of the frames.  I have a couple frame pieces printed.  They are small, but appear to be quite rigid.  It should be very solid when everything is bolted together.  There are some slots in the base to allow for some movement there.  I may have to make the bottom base a bit shorter.  I'll have to see once it's all bolted together.  


---
**Bob Buechler** *April 15, 2017 05:45*

**+Steven Kalmar**​​ Thanks for the details! :) I'd love to see some pictures when you have it all in place. I think that'd help me visualize the system a bit better. 


---
**Bob Buechler** *April 15, 2017 06:30*

I'm seriously considering buying these, since I can't find a design like them on Thingiverse and I'm sadly not a 3D modeler. I'd love some community feedback though, especially if you've used this type of mount before: [http://www.ebay.com/itm/CO2-Laser-Tube-Fixture-Mounts-Stand-Holder-Adjustable-Tube-Dia-50cm-60cm-80cm-/232211009500](http://www.ebay.com/itm/CO2-Laser-Tube-Fixture-Mounts-Stand-Holder-Adjustable-Tube-Dia-50cm-60cm-80cm-/232211009500)


---
**Bob Buechler** *April 15, 2017 06:30*

[ebay.com - CO2 Laser Tube Fixture Mounts Stand Holder Adjustable Tube Dia: 50cm 60cm 80cm  &#x7c; eBay](http://www.ebay.com/itm/CO2-Laser-Tube-Fixture-Mounts-Stand-Holder-Adjustable-Tube-Dia-50cm-60cm-80cm-/232211009500)


---
**Don Kleinschnitz Jr.** *April 15, 2017 13:14*

**+Bob Buechler** I have been watching this design on ebay but never found one of these smaller than 60mm so this one looks promising.


---
**Steven Kalmar** *April 15, 2017 13:14*

**+Bob Buechler** I should have at least one of the mounts done this weekend, if not today.  Depends on what I need to change.  I'll post it to Thingiverse when I'm done.  



I considered purchasing those same mounts from ebay when I was designing a larger machine before I decided to learn on the K40 first.  They look rather large for the very small space of the K40.


---
**Steven Kalmar** *April 15, 2017 13:17*

**+Don Kleinschnitz** Am I looking at it wrong, or does it look like the ebay mount is one size fits all?


---
**Don Kleinschnitz Jr.** *April 15, 2017 13:24*

**+Bob Buechler** did some searching in the community 



[plus.google.com - Gutted my 50 watt today...installed the light objects adjustable tube mounts ...](https://plus.google.com/101614147726304724026/posts/ga4rFbaf6mQ)


---
**Don Kleinschnitz Jr.** *April 15, 2017 13:32*

**+Steven Kalmar** it suggests it fits up to 80CM and that makes me wonder how wide those mounts are and if they will fit in the tube bay. Is there a possibly it will take the beam off center enough to make the mirror #1 alignment difficult.

I sent the seller a question **+Bob Buechler** perhaps you should as well to get their attention:).


---
**Steven Kalmar** *April 15, 2017 14:38*

**+Don Kleinschnitz** they would have to be at least 100 to 110mm wide.  Even if they fit, there wouldn't be enough lateral adjustment to make up for the frame twist of these machines.  The mounts on these things need to be as small possible to allow for as much adjustment as possible, while still being strong enough to hold the tube securely. 


---
**Bob Buechler** *April 15, 2017 15:47*

**+Don Kleinschnitz** good call. Asked them too. We'll see what they say. Worst case someone with some design skills can probably use this as a reference. 


---
**HalfNormal** *April 15, 2017 16:22*

**+Don Kleinschnitz**​ **+Steven Kalmar**​ I considered those mounts when I originally was going to change mine out but did not see how they would fit without a lot of shoehorning. I was then going to see about redesigning one and printing one that would fit the K40 but then saw the other designs and decided to go with them instead. 


---
**Bob Buechler** *April 15, 2017 16:31*

All the other designs just look more complicated then they need to be. One vertical dial and a simple horizontal slide is all it should need...


---
**Don Kleinschnitz Jr.** *April 15, 2017 17:44*

**+Bob Buechler** I had some of the same idea about complexity + I wanted to make one that was laser cut.

I started that design but did not yet complete it because of other priorities.

Below is a link to the start of that design missing the horizontal mounts and the clamping schema.



I imagined it as 5 pieces:

1. 2 halves of a lower clamp with vertical adjusting slots

2. 2 halves of an upper clamp

3. Mounting base vertical with slots

4. Mounting base (lays horizontal) with slots



The clamps were to be made from 2 pieces of .22 acrylic glued together. 

The base was planned to be .22 acrylic with a vertical piece glued to a horizontal flange

There would be rubber around the tube like the stock mounting

Fasteners for the clamps were to be brass inserts in the bottom clamp.

Up down is adjusted by sliding the clamps in the base

Left-right is adjusted sliding the base in its slots

I was going to add nut inserts into the frame to mount the base .



Your welcome to finish the design if you want :). I won't get to it until I need a new tube.



[3dwarehouse.sketchup.com - K40 Laser mount](https://3dwarehouse.sketchup.com/model/eafab337-f208-4edd-bb99-2a43e734ef26/K40-Laser-mount)


---
**Don Kleinschnitz Jr.** *April 15, 2017 17:47*

**+Bob Buechler**, **+Steven Kalmar** I was wondering if you guys have to adjust your laser frequently. Mine came setup ok and I have never touched it. 

I figured once it was locked in it wouldn't need more tweaking but it sounds like you have to?

Any thoughts on what is moving?


---
**HalfNormal** *April 15, 2017 17:58*

**+Don Kleinschnitz**​ it depends on how often you are changing the arrangement of the laser. Anytime you remove a part or replace a part you have to go through the mirror alignment procedure. If the laser tube is parallel then it is quicker and easier to get your alignment back


---
**Bob Buechler** *April 15, 2017 18:07*

**+Don Kleinschnitz**​ I'm having trouble getting it aligned as it arrived. Mine isn't drifting, it's just not well aligned, and I think messing with mirror thumbscrews  is a good way to drive yourself crazy and waste a lot of time. 



In my case, the beam lands too high on the first mirror, and I want to try correcting that with a tube adjustment first, instead of packing all the mirror mounts as well as the laser head. I would rather square up my mirrors and mounts to 45° and 90°, adjust the tube position and only then get creative if I have to. Overcomplicating alignment is probably why it's such a problem for people. 



But rather than manually packing and unpacking the stock mounts and risk damaging the tube in the process, I'd rather get a simple but effective replacement set of mounts to start with.


---
**Bob Buechler** *April 15, 2017 19:21*

Looks like those eBay mounts are made by Chinese manufacturer Tecnr: [tecnr.com - 50MM to 80MM diameter Flexible CO2 laser tube support mount brackets ajusted for laser engraver](http://www.tecnr.com/sale-9475809-50mm-to-80mm-diameter-flexible-co2-laser-tube-support-mount-brackets-ajusted-for-laser-engraver.html)



Their product page says the height is adjustable from 85mm to 105mm. Based on that, and looking at the product photos, it looks like the mount should be around 85mm wide. Still, I've emailed their hotmail.com contact address and asked for dimensions there as well. I'll update if I get good answers.


---
**Steven Kalmar** *April 16, 2017 00:14*

So far I have 20 mm lateral movement and 14mm vertical.  There's room to increase the vertical span by another 10mm at least .  The whole assembly is quite rigid, more so than the frame it's bolted it.  I'll move on to the adjusters and see if I need to increase the vertical span later.  Quite happy with the initial print.  ![images/bb2e81d4fc2d0b528dcf200c9ee24eb5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb2e81d4fc2d0b528dcf200c9ee24eb5.jpeg)


---
**HalfNormal** *April 16, 2017 00:23*

**+Steven Kalmar**​ looks great! 


---
**Andy Shilling** *April 16, 2017 12:34*

**+Steven Kalmar** That looks much better than the other clamps I am using, Next time my tube goes I think I will be going over to your's :) 


---
**Steven Kalmar** *April 16, 2017 16:45*

**+Andy Shilling** Thanks!  It will be on Thingiverse when I'm done.  Printing another iteration now.  


---
**Bob Buechler** *April 18, 2017 03:20*

Tecnr got back to me on the dimensions of their adjustable tube mounts. Unfortunately, they are too wide for the stock blue and white K40 chassis: ![images/4a9c2c02821efb41555290f75d7474ec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4a9c2c02821efb41555290f75d7474ec.jpeg)


---
**Don Kleinschnitz Jr.** *April 18, 2017 12:23*

**+Bob Buechler** hey thanks,and darn. They got back to me and told me the tube size it supports lol.



Update: just got the same answer 


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/gS52kDb4V5n) &mdash; content and formatting may not be reliable*
