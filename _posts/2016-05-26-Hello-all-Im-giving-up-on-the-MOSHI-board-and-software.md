---
layout: post
title: "Hello all, I'm giving up on the MOSHI board and software"
date: May 26, 2016 12:54
category: "Modification"
author: "Tom Stern"
---
Hello all, I'm giving up on the MOSHI board and software. I invested so much time into this machine and it is still an unreliable piece of crap with potential. Any suggestions for what path to choose in order to get a working machine with good software?





**"Tom Stern"**

---
---
**Anthony Bolgar** *May 26, 2016 13:19*

Upgrade the controller to a smoothieboard and run LaserWeb v.2 This will give you a 32 bit controller that has ethernet connectivity along with some very advanced firmware (way better than Ramps or Grbl). The software is very easy to use.


---
**Tom Stern** *May 26, 2016 13:23*

Thanks for the answer. I had a feeling I'll be getting that answer from this group :)

Do you happen to know about someone who wrote up a guide for the complete upgrade?


---
**Anthony Bolgar** *May 26, 2016 13:31*

Yes, the owner of this group, **+Stephane Buisson** created a write up available at [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide). If you need any help with the upgrade just post your questions here in the group, one of the smoothieboard creators, **+Arthur Wolf** is a member of the group, and the development team for LaserWeb V.2 led by **+Peter van der Walt** are all members of the group as well. So any resources you need are just a click away.


---
**Tom Stern** *May 26, 2016 13:54*

WOW!

So in other words I've landed on a good spot :)

I'll start ordering stuff today so I can have them ready to installing within 2 to 3 weeks.


---
**Stephane Buisson** *May 26, 2016 14:58*

;-))


---
**Greg Curtis (pSyONiDe)** *May 27, 2016 12:08*

Can't wait to make the same upgrade.


---
**John-Paul Hopman** *May 27, 2016 13:36*

Curious, but which smoothieboard? My thought would be the 3 since you really only need X/Y and maybe someday Z. But the 3 doesn't come with ethernet. Is the board just not populated with the port? Or is it missing additional chips for ethernet to function.


---
*Imported from [Google+](https://plus.google.com/102626604769088820318/posts/AicPJgyyHv4) &mdash; content and formatting may not be reliable*
