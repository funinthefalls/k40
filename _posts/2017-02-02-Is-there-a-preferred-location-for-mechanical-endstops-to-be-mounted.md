---
layout: post
title: "Is there a preferred location for mechanical endstops to be mounted?"
date: February 02, 2017 13:32
category: "Hardware and Laser settings"
author: "Andy Shilling"
---
Is there a preferred location for mechanical endstops to be mounted? I've searched a couple of forums but can not find anyone talking about them or showing photos so I'm guessing it's just make sure they are clear of any moving parts.





**"Andy Shilling"**

---
---
**Ariel Yahni (UniKpty)** *February 02, 2017 13:43*

Look here and you'll see the default location for K40 with MEC endstops 

[plus.google.com - This is no upgrade but since I removed the gantry I feel it will be very usef...](https://plus.google.com/+ArielYahni/posts/RgraoksVvbo)


---
**Andy Shilling** *February 02, 2017 14:22*

**+Ariel Yahni** Thank you that will replace the x,y minimum/ 0,0 if set to top left but what about the maximum or do people not use these ?


---
**Ariel Yahni (UniKpty)** *February 02, 2017 14:24*

Standard config ( so 99% users ) have it a top left only 


---
**Andy Shilling** *February 02, 2017 14:25*

Ok sounds good to me. Thank you




---
**Paul de Groot** *February 02, 2017 20:26*

I swapped the y endstop from top to bottom so it aligns with open source software x.y start positions like inkscape. Alternatively i can flip the y axis in software 


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/QYB4og9Jfrx) &mdash; content and formatting may not be reliable*
