---
layout: post
title: "I would like to thank everyone that helped me with my living hinge questions..."
date: September 01, 2017 03:05
category: "Object produced with laser"
author: "Alex Krause"
---
I would like to thank everyone that helped me with my living hinge questions... Here is the finished results. I will be making up a couple more of these to send off to a benefit auction whose poceeds will go directly to a wood worker who has suffered the loss of his workshop due to Hurricane Harvey. This auction is being held by a pen turning group and my contribution are these pen display racks for craft conventions 

![images/7e3cd8463f7f5157bfdf4670960a9082.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e3cd8463f7f5157bfdf4670960a9082.jpeg)



**"Alex Krause"**

---
---
**Joe Kyle** *September 01, 2017 03:30*

Very nice!


---
**Ned Hill** *September 01, 2017 04:25*

Nice work.




---
**Tony Schelts** *September 01, 2017 07:57*

very good, 


---
**Don Kleinschnitz Jr.** *September 01, 2017 11:54*

#K40Projects

Excellent design and finish. Will you share the design?

I assume it comes apart and lays flat? 

I gotta make one some day ....


---
**HalfNormal** *September 01, 2017 12:37*

Great work and help supporting a great cause!


---
**Jim Hatch** *September 01, 2017 12:56*

I would anneal the acrylic. Living hinges in acrylic tend to crack easily and break otherwise - because it doesn't have the alternating plies for strength you get from plywood. You can anneal it by putting it in the oven at 170F for an hour per mm of thickness and then turning the oven off but leaving the piece in until it's cooled down.



Annealing relieves the inner stresses in the acrylic caused by cutting that lead to cracking and crazing.


---
**Mark Brown** *September 01, 2017 17:57*

Pretty cool.  I'm surprised living hinges work in acrylic.  I'd have expected it to just snap.


---
**Alex Krause** *September 06, 2017 02:59*

**+Jim Hatch**​ should I remove the protective paper before or after annealing?


---
**crispin soFat!** *September 06, 2017 03:10*

imma guess before?


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/EWRfyUujRaH) &mdash; content and formatting may not be reliable*
