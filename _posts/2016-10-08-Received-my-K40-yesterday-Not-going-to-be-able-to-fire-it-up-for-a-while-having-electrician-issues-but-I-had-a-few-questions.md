---
layout: post
title: "Received my K40 yesterday. Not going to be able to fire it up for a while (having electrician issues) but I had a few questions..."
date: October 08, 2016 16:50
category: "Discussion"
author: "photomishdan"
---
Received my K40 yesterday. Not going to be able to fire it up for a while (having electrician issues) but I had a few questions... 



What is the tube of stuff they sent and what is it for? 

![images/afc77575948f8afb8707904c9dd5666d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afc77575948f8afb8707904c9dd5666d.jpeg)



**"photomishdan"**

---
---
**photomishdan** *October 08, 2016 16:51*

Also does this all look present and correct? ![images/ffed438d1133ef0054eb006a59103537.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ffed438d1133ef0054eb006a59103537.jpeg)


---
**photomishdan** *October 08, 2016 16:51*

![images/1b4b133fdfb601fea036ec6a35541b5f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b4b133fdfb601fea036ec6a35541b5f.jpeg)


---
**photomishdan** *October 08, 2016 16:51*

![images/24ea933bf0abbeaa1a25037118fa9a10.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/24ea933bf0abbeaa1a25037118fa9a10.jpeg)


---
**photomishdan** *October 08, 2016 16:51*

![images/d8fa14402fbae4927501fd05b6880fd1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d8fa14402fbae4927501fd05b6880fd1.jpeg)


---
**photomishdan** *October 08, 2016 16:52*

![images/579b271d0f95d11bce19619ad5c384ad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/579b271d0f95d11bce19619ad5c384ad.jpeg)


---
**photomishdan** *October 08, 2016 16:52*

![images/a4ca4e33ba36ffc47fe5be3912ba387e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4ca4e33ba36ffc47fe5be3912ba387e.jpeg)


---
**photomishdan** *October 08, 2016 16:52*

![images/fdec233d1d111f380bf768ed2e3ee3f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fdec233d1d111f380bf768ed2e3ee3f6.jpeg)


---
**Ariel Yahni (UniKpty)** *October 08, 2016 16:56*

**+photomishdan**​ the paste is a silicon isolator for when you need to change the tube or if you have electrical arcs. It should be present where the 2 cables connect the tube. 


---
**Ariel Yahni (UniKpty)** *October 08, 2016 16:57*

**+photomishdan** if you ever plan on upgrading the stock board you will  end a middle man board or splice all the cabling 


---
**photomishdan** *October 08, 2016 17:00*

**+Ariel Yahni** ah that's great, thanks for that. I didn't know what to do with it. 

I will probably upgrade the controller at some point, but want to use the stock board for now, and then progress later, once things make more sense to me! 


---
**Timo Birnschein** *October 16, 2016 18:13*

**+photomishdan** If you ever want to upgrade the board, you might find these three links interesting:

[https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion](https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion)

[https://github.com/McNugget6750/K40-grbl-board](https://github.com/McNugget6750/K40-grbl-board)

[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)

[github.com - presentations](https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion)


---
**Stuart Middleton** *November 01, 2016 15:24*

It looks like you have an air bubble in the tube. Try to get it out before you start cutting so you don't get a hot spot. Tilt the machine while the pump is operating to coax it into the outlet tube


---
*Imported from [Google+](https://plus.google.com/105674876465839652573/posts/A3XQxJmvcrN) &mdash; content and formatting may not be reliable*
