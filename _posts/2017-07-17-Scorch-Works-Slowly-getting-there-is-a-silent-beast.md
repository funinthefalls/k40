---
layout: post
title: "Scorch Works Slowly getting there, is a silent beast ;)"
date: July 17, 2017 17:29
category: "Object produced with laser"
author: "Kenneth White"
---
**+Scorch Works** Slowly getting there, #K40Whisper is a silent beast ;) 



![images/31656eddde7342bab509d982454fd2d5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/31656eddde7342bab509d982454fd2d5.jpeg)
![images/5b6d8575428647359053f4f0f9ca6633.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5b6d8575428647359053f4f0f9ca6633.jpeg)
![images/d1cd1c686c757f75c2ea0b42d391fd42.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1cd1c686c757f75c2ea0b42d391fd42.jpeg)

**"Kenneth White"**

---


---
*Imported from [Google+](https://plus.google.com/100692370397257622708/posts/Sgx4vfYzdKb) &mdash; content and formatting may not be reliable*
