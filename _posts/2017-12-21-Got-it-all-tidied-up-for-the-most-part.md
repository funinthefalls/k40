---
layout: post
title: "Got it all tidied up for the most part"
date: December 21, 2017 01:15
category: "Modification"
author: "Tech Bravo (Tech BravoTN)"
---
Got it all tidied up for the most part. Except for...... I was being stupid and blew out mt LED strips that are supposed to light up my work....grrrr. BTW: the stock LED strip in the K40 is powered by 24vdc NOT 5! smh........
{% include youtubePlayer.html id="BPt0eQeyYGc" %}
[https://youtu.be/BPt0eQeyYGc](https://youtu.be/BPt0eQeyYGc)





**"Tech Bravo (Tech BravoTN)"**

---
---
**Joe Alexander** *December 21, 2017 09:51*

have you noticed any vibration causing issues from the air assist pump being mounted within the enclosure? I have the same one but keep it on the ground.


---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 16:48*

**+Joe Alexander** surprisingly no. i was actually surprised LOL. the panel it is mounted on has just the tiniest amount of vibration and it resonates a bit but its not much louder than when it was not mounted at all. no vibration is noticed anywhere else. that panel just has a few spot welds around its perimeter so there are not many contact points to transfer what little vibration there is. that's not to say i won't find the welds broken one of these days LOL. i have thought about applying dynamat to the panel to see if that would eliminate what little vibration is there.


---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/fvQCVs8amPn) &mdash; content and formatting may not be reliable*
