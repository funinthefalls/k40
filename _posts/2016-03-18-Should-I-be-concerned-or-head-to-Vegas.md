---
layout: post
title: "Should I be concerned or head to Vegas?"
date: March 18, 2016 18:48
category: "Discussion"
author: "Tom Spaulding"
---
Should I be concerned or head to Vegas?

I ordered a K40 from one of the California importers about a month ago. It arrived in less than a week and seemed to be in perfect physical condition. Since it arrived I've been reading about mods and all the tuning I'll need to do, but I decided to test it out before making any changes.

I cleaned the mirrors filled a bucket with distilled water and let the pump run overnight to remove bubble.

I did a quick test fire on my lunch hour and it made a pin point hole in a piece of mat-board. I decided to give it a shot with a my laptop connected. I drew a couple boxes in corel and after testing a couple passes with the laser off. I let the machine do it's thing and everything worked perfectly.  Should I still try to align mirrors or just be happy it's working keep using it? I guess when I start cutting acrylic I'll find out how well aligned it is.



-Tom





**"Tom Spaulding"**

---
---
**Imko Beckhoven van** *March 18, 2016 18:59*

If it aint broken....... Start using and if you find limitations then upgrade﻿. Make sure you use cast acrilic.


---
**3D Laser** *March 18, 2016 19:06*

**+Imko Beckhoven van** plus one on the cast I got some extruded in my last batch and it stunk up my whole house 


---
**Simon Jackson** *March 18, 2016 19:07*

if it is misaligned you will probably notice it more as you move away from the back left corner. If front right still cuts OK, your alignment is probably fine. 


---
**ED Carty** *March 18, 2016 22:36*

what company did you order it from out of curiosity ?


---
**Tom Spaulding** *March 19, 2016 01:02*

The seller was Majorsavings14



[http://www.ebay.com/sch/majorsavings14/m.html?_nkw=&_armrs=1&_ipg=&_from=](http://www.ebay.com/sch/majorsavings14/m.html?_nkw=&_armrs=1&_ipg=&_from=)


---
*Imported from [Google+](https://plus.google.com/112899082959742744382/posts/cbVGRQfR3uF) &mdash; content and formatting may not be reliable*
