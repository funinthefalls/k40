---
layout: post
title: "Totally Frustrated... \"The CorelDRAW(Ver >=11) software not installed!\""
date: June 12, 2018 04:12
category: "Software"
author: "Todd Vannote"
---
Totally Frustrated...  "The CorelDRAW(Ver >=11) software not installed!"

Corel X5 with CorelLASER 2013.02 on a Windows 7 Machine worked fine for about 6 months. Had to restore and now every time I open CorelLaser it tells me "The CorelDRAW(Ver >=11) software not installed!"

I even tried it on my other machine with Win10 and CorelDraw X8. Same result...

I have uninstalled and reinstalled CorelLaser numerous times to no avail.

Anyone know how to get these to work together again? 





**"Todd Vannote"**

---
---
**Anthony Bolgar** *June 12, 2018 06:37*

Only suggestion I have is to start with a fresh install of Windows 7


---
**Todd Vannote** *June 12, 2018 13:32*

**+Anthony Bolgar** thank you. I'll keep plugging away. It has to be something simple. Since it installed and ran for 6 months with no problems.




---
**James Rivera** *June 12, 2018 17:07*

Windows 10 blocks unsigned drivers. You probably need to disable that temporarily to fully install the software.


---
**Todd Vannote** *June 12, 2018 17:16*

thank you. I'll try that.

Do you think that would be the same on Windows 7?

The first time I loaded it on Win7 machine it worked great. Reinstall and no dice.




---
**James Rivera** *June 12, 2018 17:50*

No. It was added in Windows 8.


---
*Imported from [Google+](https://plus.google.com/111608418491136882596/posts/Xyaze81dsaK) &mdash; content and formatting may not be reliable*
