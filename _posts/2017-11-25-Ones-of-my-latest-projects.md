---
layout: post
title: "Ones of my latest projects"
date: November 25, 2017 17:49
category: "Object produced with laser"
author: "Tony Sobczak"
---
Ones of my latest projects.



![images/384afd90600618e118267b1ce312e483.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/384afd90600618e118267b1ce312e483.jpeg)
![images/9361abf5f6ed3dc1ee46ece0e414e894.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9361abf5f6ed3dc1ee46ece0e414e894.jpeg)
![images/2f0f2612fd1bb8df18e15569ae314f47.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f0f2612fd1bb8df18e15569ae314f47.jpeg)

**"Tony Sobczak"**

---
---
**Daniel Wood** *November 26, 2017 05:14*

That is a beautiful lamp and a fantastic idea. 


---
**Abe Fouhy** *November 26, 2017 07:30*

Gorgeous!


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/ciTXmjfPTkb) &mdash; content and formatting may not be reliable*
