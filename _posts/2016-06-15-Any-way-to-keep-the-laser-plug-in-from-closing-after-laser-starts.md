---
layout: post
title: "Any way to keep the laser plug-in from closing after laser starts?"
date: June 15, 2016 19:48
category: "Software"
author: "Ben Marshall"
---
Any way to keep the laser plug-in from closing after laser starts? Or do I have to keep restarting after every job?





**"Ben Marshall"**

---
---
**Ariel Yahni (UniKpty)** *June 15, 2016 19:52*

Look for an icon on the system tray ( small arrow next to the clock) 


---
**Ben Marshall** *June 15, 2016 19:58*

**+Ariel Yahni** thanks!


---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/cpVudUQXzZP) &mdash; content and formatting may not be reliable*
