---
layout: post
title: "What is your favorite material to raster engrave on?"
date: October 30, 2016 00:36
category: "Materials and settings"
author: "K"
---
What is your favorite material to raster engrave on? I'm using birch ply, but I want to order a variety of woods to experiment on.





**"K"**

---
---
**Ariel Yahni (UniKpty)** *October 30, 2016 00:46*

I use crap ply mostly, that's what I get down anyway


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 30, 2016 01:26*

So far my favourite engraving material would be either leather (since I do leatherwork) or merbau hardwood (get them as timber decking planks here in AU).


---
**K** *October 30, 2016 01:26*

**+Ariel Yahni**(null) Are you happy with your end results? I'm getting some pitting in the darker areas, and I'm not sure if that's down to the material or my settings or both. ![images/8d0880b35312b2ff912ccd9375e74955.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d0880b35312b2ff912ccd9375e74955.jpeg)


---
**K** *October 30, 2016 01:27*

**+Yuusuf Sallahuddin** I've never heard of merbau. I just googled it. It's really beautiful wood. 


---
**Ariel Yahni (UniKpty)** *October 30, 2016 01:34*

**+Kim Stroman**​ I see there a very good looking raster. Did you accent the board as vectors?  Not sure what do you mean by pitting. 


---
**K** *October 30, 2016 01:36*

**+Ariel Yahni**(null) If you look at the bottom left of homer's shoe you can see a darker spot. They're little pits/holes in the material. The borders are not vector. I just took it into photoshop and made things darker. 


---
**Alex Krause** *October 30, 2016 01:38*

Maple ply


---
**Ariel Yahni (UniKpty)** *October 30, 2016 01:43*

I will say that is a combination of the darker corner plus more heat for the same reason and material imperfection. I would be very sure that should not happen on let's say MDF


---
**greg greene** *October 30, 2016 13:49*

alder wood or slate tile


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/c2zCNcxys1c) &mdash; content and formatting may not be reliable*
