---
layout: post
title: "Have people noticed the prices of K40 machines on ebay?"
date: December 15, 2016 12:22
category: "Discussion"
author: "Surrey Woodsmiths"
---
Have people noticed the prices of K40 machines on ebay?  They have shot right up in the UK (now £700 to over £1000)



Do they normally fluctuate like this?  Appears to be all sellers.  Perhaps they are all low on stock or something





**"Surrey Woodsmiths"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2016 12:35*

There was a recent post about that, maybe a month ago. Similar thoughts regarding the possible low stock & the christmas period. Previously noted that the prices have shot up, but not to crazy prices such as you mention.



If you're in the market for one, there was a recent post from [aliexpress.com](http://aliexpress.com) (either here or in the LaserWeb/CNC Web G+ community) showing one for about AU$450, shipping included.


---
**Surrey Woodsmiths** *December 15, 2016 12:41*

There are even some up for £2,365.  

I bought mine a couple of months back for just over £300!  



Was going to buy a second but might wait for a while, lol.  



Tempted with a larger 100w version now


---
**Stephane Buisson** *December 15, 2016 17:06*

still some stock from germany

[http://www.ebay.de/itm/CO2-Laser-Graviermaschine-Gravurmaschine-Engraver-Cutter-Lasergravier-40W-/302166913471?hash=item465a8d31bf:g:VtIAAOSw-CpX~7TR](http://www.ebay.de/itm/CO2-Laser-Graviermaschine-Gravurmaschine-Engraver-Cutter-Lasergravier-40W-/302166913471?hash=item465a8d31bf:g:VtIAAOSw-CpX~7TR)


---
**Kelly S** *December 15, 2016 17:32*

I paid 500 some for mine, same seller now charging 1.5k lol... happy I didn't wait.


---
**Surrey Woodsmiths** *December 15, 2016 18:49*

Hahaha, imagine buying one now for that much then seeing the prices drop by 60-70%.  Must happen to some people


---
**Kelly S** *December 15, 2016 18:53*

I would wanna blow my brains out if that happened.


---
**Alex Krause** *December 15, 2016 21:09*

CHINESE NEW YEAR.... demand is high and production is limited


---
**Madyn3D CNC, LLC** *December 16, 2016 15:41*

Yes, Alex nailed it, Chinese New Year.. the entire globe feels the effect, most just don't know the cause. I learned this when I dabbled in global trade running an ebay store with electronics a few years back. 



The CNY is a 2-week long period of China coming to a complete HALT.  The factories, the ports, the government, EVERYTHING shuts down for 2 weeks. Why, you ask? Well, China can afford to do so. 



I use to get emails from my suppliers almost 3 months in advance, to remind me that CNY is approaching. They also get a huge boost in their economy before CNY because vendors all around the world are stocking more than they'd usually stock. It's the apocalypse of global trade, except it only lasts 2 weeks. 


---
*Imported from [Google+](https://plus.google.com/105599569700790157394/posts/ehsiTfm9qji) &mdash; content and formatting may not be reliable*
