---
layout: post
title: "Made a cake topper for my daughter's HS graduation today"
date: June 10, 2017 04:55
category: "Object produced with laser"
author: "Ned Hill"
---
Made a cake topper for my daughter's HS graduation today



![images/c478ba54d1b491a62322964191b45a1d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c478ba54d1b491a62322964191b45a1d.jpeg)
![images/8ae0efed6677accd8d519146e25235de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ae0efed6677accd8d519146e25235de.jpeg)

**"Ned Hill"**

---
---
**Cesar Tolentino** *June 10, 2017 13:04*

Ned that is fantastic. What material did you use and how did you make it food grade?


---
**Ned Hill** *June 10, 2017 14:19*

**+Cesar Tolentino** I made it out of 1/8" alder.   The stick is a bamboo skewer which is food grade and it's painted with simple acrylic.  I sealed it with a clear spray and for the small amount of time this is going to be sitting in a cake it's essentially food grade.  If you were really worried about it you could always just not paint the skewer, or at least not part of it that will go into the cake.



Also, I was wondering how to make the tassel cord stand out and I was wandering through the craft store, looking for glitter paint, and saw puffy paint and was like "Ah AH: :)




---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/ciYWfqXXqHD) &mdash; content and formatting may not be reliable*
