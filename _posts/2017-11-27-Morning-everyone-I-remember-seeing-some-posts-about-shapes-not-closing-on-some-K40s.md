---
layout: post
title: "Morning everyone, I remember seeing some posts about shapes not closing on some K40's"
date: November 27, 2017 15:51
category: "Discussion"
author: "Alex Raguini"
---
Morning everyone,



I remember seeing some posts about shapes not closing on some K40's.  I, too, had this problem.  The way I solved it was to tighten the guide rollers on the lens carrier of my k40.  These are the white rollers.  They have an offset axis.  I adjusted them until there was no play when twisting the carrier assembly. 



My issues with circles not closing properly were resolved after this.  I'm hoping this helps others with similar issues.





**"Alex Raguini"**

---
---
**Don Kleinschnitz Jr.** *November 27, 2017 16:21*

#K40UnclosedShapes


---
**Chris Hurley** *November 27, 2017 16:40*

Can you show a picture of the adjustment?


---
**HalfNormal** *November 27, 2017 16:52*

This is also the case if you see "wiggles" or uneven lines.


---
**Alex Raguini** *November 27, 2017 23:20*

**+Chris Hurley**  There are 4 rollers like this.  Two on each side in the front and two in the rear.  Turn the allen screw(s) until there is little, if any,  wobble in the lens carrier. They should not be so snug the lens carrier cannot move smoothly.



![images/46da1d2a0e7d5abd635acc44fadba4a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/46da1d2a0e7d5abd635acc44fadba4a9.jpeg)


---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/NMispqYeVGe) &mdash; content and formatting may not be reliable*
