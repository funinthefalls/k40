---
layout: post
title: "Hi! I am probably the newer member in this comunity, i have arrived here because i just finish to instal a 40W Chinese Laser Cutter and i just realize that i have a big problem, i have not a software to use the machine, could"
date: June 22, 2016 13:46
category: "Software"
author: "Doris jc"
---
Hi! I am probably the newer member in this comunity, i have arrived here because i just finish to instal a 40W Chinese Laser Cutter and i just realize that i have a big problem, i have not a software to use the machine, could somebody help me? Thanks





**"Doris jc"**

---
---
**Ariel Yahni (UniKpty)** *June 22, 2016 14:06*

Look for **+Yuusuf Sallahuddin**​ post as he has hosted the software Dropbox 


---
**Stephane Buisson** *June 22, 2016 14:06*

find out what is your controller board, and report here.

something like Moshi or M2nano ? do you have a usb stick (dongle) coming with your K40 ?


---
**Doris jc** *June 22, 2016 14:32*

I am not sure, but i believe that my machine is a moshi one, i have a usb stick but is not recognized by my pc, my usb stick has the next text "Lihuiyu Studio Labs 2015.03.15"


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 22, 2016 14:44*

**+Doris jc** The USB stick is actually a "key" that allows the software to function. It doesn't contain anything recognisable by the computer. All it does is that when you use the software you need the "key" (or usb stick) plugged in so that the software is allowed to run the laser.



If your machine has the M2Nano board you will be able to use the software that I have hosted in my dropbox. You can find the details of this on the controller board that you plug the usb cable into, see: [https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing) & look to the top left corner it says M2Nano



If yours is the M2Nano, then here is the software to install (with a brief guide on the installation)

[https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0](https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0)



While you are looking at the controller board, take note of both the Device ID (as seen in the previous picture) & the Board Model also (see: [https://drive.google.com/file/d/0Bzi2h1k_udXwQUxUbmh6bDltYzA/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwQUxUbmh6bDltYzA/view?usp=sharing)) as you will need these to enter into the software configuration.


---
**Stephane Buisson** *June 22, 2016 15:16*

Lihuiyu Studio Labs 2015.03.15 -> M2Nano

go to **+Yuusuf Sallahuddin** link and don't forgot to plug your usb key in  (as it's proprietary software)


---
**Pippins McGee** *June 22, 2016 15:23*

**+Stephane Buisson** sorry to Hijack, but how are you all making a copy of your USB sticks?



surely it is wise to clone it somehow, incase it is lost or damaged, otherwise entire machine is useless....



how to clone the USB key to another USB?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 22, 2016 16:49*

**+Pippins McGee** Never done that myself, but it would be wise. Unfortunately I don't think it is possible easily as it's a "hardware" key. There must be some way to do it, but I have no idea. I wonder has anyone pulled one apart to see what is inside it?


---
**Stephane Buisson** *June 22, 2016 19:05*

don't be confused by the expression "USB stick", in that case it's just a stick, but not a memory storage, just a piece of hardware with a electronic protection built in. it's made to be not copied easily. I am not saying it's possible, never done it, nor fancy to do it. My position is to go open source, not to promote those close source.


---
**Doris jc** *June 27, 2016 21:03*

Hi, I really want to say thanks for help me, now... i still need to learn to use my 40W Chinese Laser Cutter, could you help me with this?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 21:04*

**+Doris jc** This community is always happy to help. Do you have any particular things you need to know? If so, ask away & I'll see what I can do to help (others will also).


---
**Christoph E.** *June 30, 2016 01:08*

**+Pippins McGee** It's not as easy to copy dongles like that. There's a few comapnies out there who will copy it depending on the model.



A friend of mine for example has got one for some diagnostic-software for construction machinery and noone seems to be able (or wants) to copy that. :-(


---
*Imported from [Google+](https://plus.google.com/101709945506358389673/posts/YTovuRUzHE8) &mdash; content and formatting may not be reliable*
