---
layout: post
title: "Cutting head. I have, on order, both the Light Objects air assist head from the USA and also the Saite air assist head and mirror/lens kit from Asia"
date: June 21, 2016 09:01
category: "Modification"
author: "Pete Sobye"
---
Cutting head. 



I have, on order, both the Light Objects air assist head from the USA and also the Saite air assist head and mirror/lens kit from Asia. 



I do not plan to keep both heads so the Light Ibjects one will be available for resale when it arrives. 



If anyone is interested in buying it. You can contact me on 07809 154611 in the UK or 0044 7809 154611 from outside the UK or email pete.sobye@icloud.com. 





**"Pete Sobye"**

---
---
**Alex Krause** *June 21, 2016 18:53*

The stock bracket on the X axis that holds the head will need to be modified from what I have heard to accommodate the Saite head 


---
**Stephen Sedgwick** *June 22, 2016 00:11*

Do you have a link to the "Saite air assist head" kit?  I am here in the states and I am not seeing that specifically... at least with a quick search and would like to know more about that kit


---
**Alex Krause** *June 22, 2016 02:33*

**+Pete Sobye**​ is this the air assist head you ordered  [http://www.ebay.com/itm/262312382137](http://www.ebay.com/itm/262312382137)


---
**Pete Sobye** *June 22, 2016 07:48*

**+Alex Krause** no, it's this one [https://www.ebay.co.uk/itm/252303388050](https://www.ebay.co.uk/itm/252303388050) 


---
**Pete Sobye** *June 22, 2016 07:50*

**+Alex Krause** and this lens/mirror set. [https://www.ebay.co.uk/itm/172050707677](https://www.ebay.co.uk/itm/172050707677) 


---
**Stephen Sedgwick** *June 22, 2016 12:25*

Why did you feel this is better than the light objects one?  I just haven't gotten an air assist yet, trying to understand the reasoning behind the 2 items you are choosing so see what route I want to go.


---
**Pete Sobye** *June 23, 2016 12:07*

**+Stephen Sedgwick** I chose the Saite one over the LO one as the Saite one has focus adjustment.


---
**Stephen Sedgwick** *June 23, 2016 13:00*

so you are not planning a Z bed on yours?


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/e4rJpC4WL5r) &mdash; content and formatting may not be reliable*
