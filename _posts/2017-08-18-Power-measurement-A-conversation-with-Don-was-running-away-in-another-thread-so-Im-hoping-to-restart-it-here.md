---
layout: post
title: "Power measurement A conversation with Don was running away in another thread, so I'm hoping to restart it here"
date: August 18, 2017 19:55
category: "Discussion"
author: "Adrian Godwin"
---
Power measurement



A conversation with Don was running away in another thread, so I'm hoping to restart it here.



The assertion is that being able to measure laser power (and especially power loss at a mirror or lens) is extremely useful when optimising a laser cutter. But they're expensive, and the common ones are rather low power - intended for optical fibre measurements.



Here's a description of how to use one, and a possible cheap option :


{% include youtubePlayer.html id="lO9rWlobLZU" %}
[https://www.youtube.com/watch?v=lO9rWlobLZU](https://www.youtube.com/watch?v=lO9rWlobLZU)



Russ suggests this can cost a few hundred dollars, though there are a couple on ebay at present for £100-£150. 



There are other youtube guides :


{% include youtubePlayer.html id="R9s2hO-PCsA" %}
[https://www.youtube.com/watch?v=R9s2hO-PCsA](https://www.youtube.com/watch?v=R9s2hO-PCsA)



Here's a paper on building one that looks fairly feasible on a small budget :

[http://redlum.xohp.pagesperso-orange.fr/pub/PM.pdf](http://redlum.xohp.pagesperso-orange.fr/pub/PM.pdf)



I've got a Coherent one, but its calibration status is unknown and it has a huge heatsink that makes it very difficult to place in the beam on a K40. It looks like [http://www.emcgrath.com/catalog/images/LAB/Light/LBF061.jpg](http://www.emcgrath.com/catalog/images/LAB/Light/LBF061.jpg)



Anybody got other suggestions ?









**"Adrian Godwin"**

---
---
**Adrian Godwin** *August 21, 2017 14:28*

I think Russ's solution is pretty decent - I think he charges £30 and it comes with a (probably generic) calibration.



However, all you really need is a lump of  black metal and a temperature sensor.



How about a small black-anodised heatsink, an electronic temperature sensor (DS1820 or something similar with higher resolution) and an arduino ?



Fitting a resistor to the heatsink would allow self-calibration : it could be used to heat the device at a known rate which would then model the same objects response to laser heating. A second temperature sensor (not attached to the heatsink) could measure ambient for the necessary correction.



Better than measuring the temperature of the block when it reaches equilibrium would be to measure the rate of heat rise. This would then give a real-time measurement of power until the block got too hot to be useable.



Another possibility would be to operate it like a bridge circuit : have two similar heatsinks, separated so that they're in a similar environment but one doesn't heat the other. Heat one with a resistor, and the other with the laser. Control the resistor power such that both objects are the same temperature, and the DC power will equal the optical power. This is the thermal equivalent of instruments like a grease-spot photometer.



[http://www.phy6.org/outreach/edu/greaspot.htm](http://www.phy6.org/outreach/edu/greaspot.htm)

 


---
*Imported from [Google+](https://plus.google.com/100361775132886183172/posts/ZpR1hgLUW7D) &mdash; content and formatting may not be reliable*
