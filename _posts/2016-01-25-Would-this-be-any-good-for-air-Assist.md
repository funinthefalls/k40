---
layout: post
title: "Would this be any good for air Assist"
date: January 25, 2016 08:22
category: "Discussion"
author: "Norman Kirby"
---
Would this be any good for air Assist

[http://www.ebay.co.uk/itm/Dual-Action-Airbrush-Air-Compressor-Air-Brush-Spray-Paint-Gun-Hose-Nail-Art-Kit-/361368653126?var=&hash=item5423400d46:m:m3zOTgbUyExYHz4TJHQ9rDA](http://www.ebay.co.uk/itm/Dual-Action-Airbrush-Air-Compressor-Air-Brush-Spray-Paint-Gun-Hose-Nail-Art-Kit-/361368653126?var=&hash=item5423400d46:m:m3zOTgbUyExYHz4TJHQ9rDA)





**"Norman Kirby"**

---
---
**Anthony Bolgar** *January 25, 2016 13:16*

I think that it would be O.K. , but I am not an expert by any means so maybe others in the group might have a better idea of it's suitability.




---
**3D Laser** *January 26, 2016 00:18*

Any idea what the PSI is onit


---
**Anthony Bolgar** *January 26, 2016 00:34*

The specs say 10.5 Litre/min and PSI range is between 15 and 21 (Compressor starts when PSI drops to 15 and turns off when it hits 21)


---
**Scott Thorne** *January 26, 2016 14:18*

I had one of these, the psi is really low on these....it is no good on an open line like a air assist.


---
**Tony Schelts** *January 27, 2016 00:25*

This it the blower that I bought, I bought some flexible hose replacements and adaptors.  I decided to take the inards out of the old blower and hooked it through the top.  Works great very satisfied. 

[http://www.amazon.co.uk/gp/product/B00MWD284O?psc=1&redirect=true&ref_=oh_aui_detailpage_o08_s01](http://www.amazon.co.uk/gp/product/B00MWD284O?psc=1&redirect=true&ref_=oh_aui_detailpage_o08_s01)


---
*Imported from [Google+](https://plus.google.com/103798744920995011573/posts/4YifvL5kuMu) &mdash; content and formatting may not be reliable*
