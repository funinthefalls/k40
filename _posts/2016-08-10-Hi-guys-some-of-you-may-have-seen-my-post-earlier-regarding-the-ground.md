---
layout: post
title: "Hi guys, some of you may have seen my post earlier regarding the ground"
date: August 10, 2016 05:17
category: "Discussion"
author: "Jose Castellon"
---
Hi guys, some of you may have seen my post earlier regarding the ground. Well that issues was resolved. 



Now this one is kicking my ass! 



I was working on aligning the mirrors based on a video I saw on YouTube, the guy was using a sticky note to get the dot spot on. Well it was trying this method trying to align everything and all of a sudden the laser beam got all erratic, it's not a solid dot anymore. Someone please help? What did I do wrong? Is something maybe on one of the mirrors causing the beam to go crazy?

First day and this thing is driving me crazy 😳🍺





**"Jose Castellon"**

---
---
**I Laser** *August 10, 2016 05:25*

Yeah try cleaning your mirrors. What power are you testing with? At low power the beam tends to be a bit erratic.


---
**Jose Castellon** *August 10, 2016 05:34*

Can I just use a dry cotton swab? I kinda rubbed the one in the work area but made no difference. And I thought that too so I started messing with the power and it's the same. I put down a piece of ply wood and yeah the dot was elongated and it left a way separate burn Mark far from the dot. 😳

Anyone local in San Gabriel valley California that wants to give me a hand with this? 😁


---
**Anthony Bolgar** *August 10, 2016 05:38*

Sounds like a cracked mirror or len to me. Physically remove them from the mounts and inspect for damage,


---
**Jose Castellon** *August 10, 2016 05:50*

I pulled the one in the front off and it looked fine just kinda dirty. Cleaned it and now when I have the pointer closest to the power box it gives me a decent dot and when I move it away from the power box it gets erratic again. Would it be just a horrible alignment? I can't figure this out n I'm running out of patience here lol. Seriously anyone local to me I will pay for your help!!!


---
**I Laser** *August 10, 2016 07:24*

To clean it you'll need to use IPA or metho (denatured alcohol?). Rubbing with a dry cotton tip probably won't achieve much to be honest.


---
**Scott Marshall** *August 10, 2016 07:30*

It sounds as you may have something in the optical path loose. Make sure all your mirrors are solid and not rattling. They are sprung against the adjustment screws and if the spring comes loose, or you adjust them until they bottom out, the same thing will happen. If tension is lost on any mirror, in either axis, even the slightest vibration will cause the beam to move about wildly This effect is magnified the farther you get from the tube, as in the lower right corner. The longer the beam path, the  more effect a tiny angle change has.



Check your tube mounting to be sure it too, is secure.



Check to make sure your motion control rails are adjusted snugly. Grasp them and make sure they only move in the direction intended.

Then do the same for the carriage.

The guide rollers are on a cam arrangement and can come loose during shipping (as with most things in a K40 - they haven't yet discovered lockwashers or threadlocker) causing the carriage to 'rattle' . It's also possible the entire frame (heavy brown rails) is loose where it screws to the K40's cabinet floor.



Another possibility is that your beam is actually erratic. THis can be caused by loose elements within the laser tube. If your machine is brand new, the ionization path will sometimes 'hunt' for the easiest path. the result can be an unstable beam or a slight shift at low power settings

A few minutes at high power is usually all that is needed to correct this.



Last, if the beam is cutting in and out, but not shifting position (also can be the break in hunting) your power supply may be the issue. An intermittent power control (potentiometer) can cause this sort of erratic power fluctuations. Usually just rapidly turning the pot back and forth for 10-20 times will clean away the oxide layer from the resistance strip in the pot, thus restoring smooth and stable power settings. It's free.

There is a chance there's a problem with your power  supply(besides a dirty pot) but again these problems won't cause a position shift, just a level fluctuation or on/off action.



I know that's a lot, but with a new, freshly shaken (during it's boat ride) K40 there's a lot of things to come loose, and something frequently does.



The good news it once discovered, it's probably a free and easy fix.



Scott


---
**Eric Flynn** *August 10, 2016 07:57*

Did you have the coolant system running? If so, are you certain water was flowing?  



If the tube has been over temped, it may shift beam modes on you, and create an erratic beam.  



Is the beam erratic right out of the tube, before the first mirror?  If so, its not likely something that can be fixed. It could be the tube, or the power supply could be faulty and unstable. If the voltage is varying greatly, it CAN cause a mode change, and erratic beam. 



 If its changing beam mode, it will likely be observable as a variance in the plasma column in the tube that follows the change in mode.


---
**greg greene** *August 10, 2016 13:21*

Go to drug store and get some alcohol swabs. They come in a box, you open the little paks and there are alcohol soaked swabs inside.  Clean the mirrors and let the alcohol dry. This should fix it.


---
**Eric Flynn** *August 10, 2016 14:11*

I would not use normal alcohol swabs. They are too abrasive and will ruin your mirrors over time.  Get Zeiss lens wipes.  They are made of proper optical paper and is safe for FS coatings. The drug store should have these as well.


---
**Jose Castellon** *August 10, 2016 19:57*

Thank you all for your feedback I'm amazed by you guys honestly no one has said anything rude everyone has been very helpful I can honestly say this is the first group I've ever came across that's polite haha so thank you all so much. 



I had to take a break from the thing but I'm gonna get back on it and check for everything you all just mentioned. I hope I can figure this thing out soon. 

Thanks guys 

-Jose 


---
*Imported from [Google+](https://plus.google.com/111728896423789579330/posts/H5EfRy5N8wG) &mdash; content and formatting may not be reliable*
