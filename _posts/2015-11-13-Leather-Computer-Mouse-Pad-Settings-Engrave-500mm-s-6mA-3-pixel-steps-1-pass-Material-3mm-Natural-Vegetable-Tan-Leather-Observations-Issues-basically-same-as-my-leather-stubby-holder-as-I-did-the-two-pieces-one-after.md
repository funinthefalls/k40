---
layout: post
title: "Leather Computer Mouse Pad Settings: Engrave, 500mm/s 6mA, 3 pixel steps, 1 pass Material: 3mm Natural Vegetable Tan Leather Observations/Issues: (basically same as my leather stubby holder, as I did the two pieces one after"
date: November 13, 2015 03:42
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Leather Computer Mouse Pad

Settings: Engrave, 500mm/s 6mA, 3 pixel steps, 1 pass

Material: 3mm Natural Vegetable Tan Leather



Observations/Issues:

(basically same as my leather stubby holder, as I did the two pieces one after the other)

I noticed with this piece, as the laser moved to the bottom-right-most position on the leather piece, it started to engrave slightly weaker. It still worked, however the depth of the engrave is fractionally less. I am assuming this is a minor issue with alignment of my laser beam.



Also, once completed I am not so happy with the depth of the engrave. I would have preferred the entire engrave to be deeper. So, next time I will either double the power or slow the engrave speed to about 50% of what it was set at.﻿



![images/5c85f0943a6dee44b1875a5d65cbe0ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c85f0943a6dee44b1875a5d65cbe0ff.jpeg)
![images/681dc76325b62debb0f0a999b348f789.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/681dc76325b62debb0f0a999b348f789.jpeg)
![images/98dda17ba35681c9e2b4056bf7867a88.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98dda17ba35681c9e2b4056bf7867a88.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Scott Thorne** *November 13, 2015 14:21*

Wow....that looks great


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 14, 2015 00:11*

**+Scott Thorne** Thanks Scott. You have to check out [http://www.instructables.com/id/Enhance-Your-Image-when-Reverse-Laser-Engraving-on/](http://www.instructables.com/id/Enhance-Your-Image-when-Reverse-Laser-Engraving-on/) for the method suggested by **+Marc G** to dither the image. Works quite well.


---
**Gary McKinnon** *November 19, 2015 22:22*

Lovely finish to it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 19, 2015 23:28*

**+Gary McKinnon** Thanks Gary.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/AJCXBuJffLt) &mdash; content and formatting may not be reliable*
