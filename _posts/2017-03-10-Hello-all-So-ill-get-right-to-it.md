---
layout: post
title: "Hello all ! So ill get right to it"
date: March 10, 2017 05:14
category: "Discussion"
author: "DMESRQ"
---
Hello all ! So ill get right to it. I own a K40 , just as pictured here, and after about 10 months of use it suddenly stops. I thought it was the mirrors but after cleaning and testing, it fired for an hour then it stopped again. I read and watched everything available and it looks like it may be the end of the tube. I have a large air bubble right above the exit area .. whatever thats called.. and it looks like it may be arching to the round piece of metal in the tip.  Heres a link to the vid i took. Any and all help is appreciated. Thanks !!





**"DMESRQ"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2017 05:21*

1st step... get rid of the air bubble. Rotate the tube, lift the K40 on angles until the bubble goes away. Put your water container level with the tube (or preferably higher) to prevent air bubble buildups. Air bubbles are bad.


---
**Don Kleinschnitz Jr.** *March 10, 2017 15:52*

What coolant are you using and how old is it. In any case change it with distilled water and see if that helps.

While this arching is happening what is the current meter doing?


---
**DMESRQ** *March 10, 2017 18:35*

Already using distilled, and removed air bubble as directed. Placed test paper between aperture and mirror, test fire and no burn.


---
**Coherent** *March 11, 2017 15:11*

Hopefully someone here who has done so can tell you how to test/check your power supply or other electronics. You are testing with the test fire button? Sound like the tube itself or power supply is bad.


---
**Don Kleinschnitz Jr.** *March 11, 2017 17:02*

**+DMESRQ** what does current meter read when firing without burning. 


---
*Imported from [Google+](https://plus.google.com/117723282410602786578/posts/XFRLESyTdbF) &mdash; content and formatting may not be reliable*
