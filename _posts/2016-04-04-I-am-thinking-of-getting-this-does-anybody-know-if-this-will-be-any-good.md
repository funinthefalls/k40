---
layout: post
title: "I am thinking of getting this, does anybody know if this will be any good?"
date: April 04, 2016 09:06
category: "Discussion"
author: "george ellison"
---
I am thinking of getting this, [http://www.ebay.co.uk/itm/40W-CO2-Laser-Tube-Engraver-cutter-Water-Cool-700mm-Shenhui-K40-/321882425353?hash=item4af1b00809:g:zLwAAMXQlgtRuGGK](http://www.ebay.co.uk/itm/40W-CO2-Laser-Tube-Engraver-cutter-Water-Cool-700mm-Shenhui-K40-/321882425353?hash=item4af1b00809:g:zLwAAMXQlgtRuGGK) does anybody know if this will be any good? my tube packed up ages ago and i want to get it back up and running, thanks





**"george ellison"**

---
---
**Stephane Buisson** *April 04, 2016 09:19*

from china, take care at VAT + import taxe (collected for you by post office +8£), same if postage is free.


---
**george ellison** *April 04, 2016 10:13*

Im in the Uk, will i have to pay extra import tax? I think the listing says free to uk


---
**Stephane Buisson** *April 04, 2016 12:31*

I am in the UK too, **+george ellison** far less risky to buy from Europe (Germany at the moment)

example

[http://www.ebay.co.uk/itm/CO2-Laser-40W-Tube-laser-tube-for-CO2-Engraving-cutting-Engraver-machine-sale-/221904249504?hash=item33aa8622a0:g:tnoAAOSwBLlVP03E](http://www.ebay.co.uk/itm/CO2-Laser-40W-Tube-laser-tube-for-CO2-Engraving-cutting-Engraver-machine-sale-/221904249504?hash=item33aa8622a0:g:tnoAAOSwBLlVP03E)

or

[http://www.ebay.co.uk/itm/700MM-40W-Co2-Glass-Laser-Tube-Water-Cool-for-Engraver-Cutting-Machine-/191573200840?hash=item2c9aa70fc8:g:PVEAAOSwPhdVSBc~](http://www.ebay.co.uk/itm/700MM-40W-Co2-Glass-Laser-Tube-Water-Cool-for-Engraver-Cutting-Machine-/191573200840?hash=item2c9aa70fc8:g:PVEAAOSwPhdVSBc~)


---
**george ellison** *April 04, 2016 13:18*

Thanks will check them out


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/6SMb113yYvh) &mdash; content and formatting may not be reliable*
