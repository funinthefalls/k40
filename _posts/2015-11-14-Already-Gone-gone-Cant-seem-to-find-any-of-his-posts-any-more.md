---
layout: post
title: "Already Gone... gone? Can't seem to find any of his posts any more"
date: November 14, 2015 13:52
category: "Discussion"
author: "Coherent"
---
Already Gone... gone? Can't seem to find any of his posts any more. Wanted to review the DIY rotary setup he made and looks like everything is gone! :(





**"Coherent"**

---
---
**Joey Fitzpatrick** *November 14, 2015 14:08*

It looks like he is Already Gone!


---
**Stephane Buisson** *November 14, 2015 14:34*

+Already gone is effectively gone, no more member, his posts are gone too, his rotary design in thingiverse gone too... farewell !


---
**Sean Cherven** *November 14, 2015 15:07*

What happened? Why did he leave?


---
**Coherent** *November 14, 2015 15:39*

No idea, but he had a lot of great posts and information. Sorry to see he's gone.


---
**Joey Fitzpatrick** *November 14, 2015 22:32*

I am lucky to have downloaded his rotary design before it was deleted


---
*Imported from [Google+](https://plus.google.com/112770606372719578944/posts/1GUb1Md77yn) &mdash; content and formatting may not be reliable*
