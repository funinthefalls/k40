---
layout: post
title: "Hi there lads , i hope nobody gets offended if i post question thats not directly about k 40 laser , but i think someone here could be able to answer ."
date: February 06, 2018 12:03
category: "Discussion"
author: "jindrich stehlik"
---
Hi there lads  , i hope nobody gets offended if i post question thats not directly about k 40 laser , but i think someone here could be able to answer .

By the way , great community forum .

I ordered this small redsail machine ,just to learn basics of laser engraving , see link , please 

[http://www.redsailcnc.com/M3050.html](http://www.redsailcnc.com/M3050.html)

My question is : it claims on website that machine has dsp control system , but if i was googling correctly , control board is 6c6879-laser-m2 , but this control board is used in K40 machines and it is not dsp one ..... Am I missing something ?

Thanks for help . Jin







**"jindrich stehlik"**

---
---
**Anthony Bolgar** *February 06, 2018 12:32*

That link states that it is a DSP controller, I could not find any mention of the cheap K40 board.


---
**jindrich stehlik** *February 06, 2018 12:43*

I downloaded a user manual for software for M3050 , in manual there is how to set the machine and in one of the settings there is above mentioned control board 6c6879-laser-m2  ..... I think that board used in k 40 machines , i also tried to contact seller , we had normal conversation ,  but after this question he stopped answering , i think it says a lot ....

Before buying machine , checked with seller and  he confirmed that there is dsp control board ....

But while waiting for machine to arrive i was googling a lot to be ready , found user manual for machine , user manual for software and that board was first thing to hit me ....

Thanks for reading  and for help . 

Jin


---
**Don Kleinschnitz Jr.** *February 06, 2018 12:52*

**+jindrich stehlik** do you have the machine yet if so post a picture. 

Ask the vendor for a picture of the controller and then post it.


---
**Don Kleinschnitz Jr.** *February 06, 2018 12:53*

Brand new machine with an extended tube modification ...... seriously!


---
**jindrich stehlik** *February 06, 2018 13:02*

Machine coming next week on boat . In Rotterdam at the moment ....

I post picture then as seller stopped answering .....

But even if it will be without dsp , still can do plenty of great work 

,  there is lot of inspiration on this forum .....






---
**Adrian Godwin** *February 06, 2018 14:12*

DSP controllers seemed to be a thing a few years ago. I guess at the time the standard microcontrollers were a bit slow and PC-based software could judder a bit when the PC got busy. Both are faster now and I see less need for a high-speed dedicated DSP controller.



Your trader might be making it up, perhaps just echoing the sales line that was applied to an earlier model. None of these cheap machines are perfect, but I doubt the controller is going to be the biggest issue.


---
**Steve Clark** *February 06, 2018 17:18*

They did not say much about the water cooling so I'm guessing it's the same the K40. Might need a little improvement down the road . I see a PROJECT coming your way! :)



Here is a closer pic.



[alibaba.com - Desktop Paper Cnc Laser Cutting Cut Wedding Invitations Machine M3050 - Buy Desktop Cnc Laser,Paper Laser Cutting Machine,Laser Cut Wedding Invitations Product on Alibaba.com](https://www.alibaba.com/product-detail/desktop-paper-cnc-laser-cutting-cut_60699167306.html?spm=a2700.7724857.main07.105.ad1b5ba477H5uq)


---
**jindrich stehlik** *February 06, 2018 17:27*

Thanks for coments ,  i also think its the same .... Cannot wait to start , journey is sometimes more interesting than destination itself 😀 ...


---
**Mitch Newstadt** *February 08, 2018 19:56*

Looks like a nice case to work with... :D most of the info in this group would apply at least in general if not exactly.. 




---
**jindrich stehlik** *February 25, 2018 19:06*

Finally have the machine ..... It is using nano board , so it is basically slightly upgraded k 40 . So far i had time to unpack , set it up , align mirrors and do few engravings .  So far happy , no problems at all ....

Did not install their crappy software , downloaded and installed K40 whisperer , works great ...  Thinking about buying Cohesion board ....

Have to do some cuttings  to see how far can i go with it .

I am sure I'll find some problems along the way , but i take it as a form of active relax 😁






---
**Don Kleinschnitz Jr.** *February 26, 2018 01:36*

ON the site it says:  <i>Texas 32 Bit DSP control system, there is a Flash memory on the mother board, you can download the work file to the machine via USB cable.</i>



The M2 is hardly a DSP and certainly not from Texas. This would have pissed me off....


---
**jindrich stehlik** *February 26, 2018 05:45*

Not happy with seller at all , he was communicating until i asked question about DSP  .....

Just trying to make best  of it ,  machine is not bad at all , but i should know that dealing with Chinese  seller could be a bit tricky .






---
*Imported from [Google+](https://plus.google.com/118266158783995097899/posts/JrjxAmrJzPj) &mdash; content and formatting may not be reliable*
