---
layout: post
title: "Hey Guys Newbie here , I bought my K40(blue&awhile) machine months ago and recently got around to setting up and playing with it"
date: October 21, 2016 14:44
category: "Air Assist"
author: "Jeffrey Rodriguez"
---
Hey Guys Newbie here , I bought my K40(blue&awhile) machine months ago and recently got around to setting up and playing with it. Aligned all mirrors/lens wanted to cut 3mm wood that I purchased from Michaels Arts and Craft store could not cut it on 100%Power and 8mm/s speed , I slowed it down but all that does is burn the wood which I know is not good. I later moved on to cutting acrylic from Home Depot total Fail ! . After reading posts from Facebook and here I ordered Air Assist from LightObjects received that retrofitted my 12mm lens (by the way ordered 18mm lens today) as seen in videos of YouTube and what not still no luck I did see slight improvement but not nearly close to what I've seen everyone get . machine is completely stock only air assist mod and air compressor of a Pepboys that nflates Tires and Household appliances like air mattress and others .  Using LaserDRW as software 



Sorry for the long post just trying to get everything working and also help build community as I learn I can also help out others that are new by learning from my mistakes . 





**"Jeffrey Rodriguez"**

---
---
**Jim Hatch** *October 21, 2016 15:12*

It is likely that either your alignment/focus is not correct, the lens is upside down or the beam is not focused on the material (50.4mm from the material to the lens). K40s are certainly able to cut 3mm ply when setup correctly. 



If the wood you got is not something like birch ply, then it may be the issue - there are woods like purpleheart, ironwood, etc that are extremely hard to cut. 


---
**Jeffrey Rodriguez** *October 21, 2016 16:18*

Hey Jim, I will check height of material to lens . And for Lens - reflection(round) side up and flat side down towards bed. It engraves fine 




---
**Jeffrey Rodriguez** *October 21, 2016 16:22*

This was before air assist nozzle. 

Cut was at PWR 100% - 7mm/s . 

Repeated cut 20+ times. Then I found out laser and burned wood is not good . Lol

![images/670e9462302aebb3cf0d91c05fb1777d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/670e9462302aebb3cf0d91c05fb1777d.jpeg)


---
**Jim Hatch** *October 21, 2016 16:37*

Your lens is correct (bump up).


---
**Jeffrey Rodriguez** *October 21, 2016 17:45*

And still no Cut through , hahaha


---
**Jim Hatch** *October 21, 2016 19:05*

Okay, if you're sure the lens is 50.4mm from the work I've got to ask the obvious - did you pick "cutting" for the action when you sent it to the laser? You can't do both actions in a single job - you need to do your engraves and then send the file again with what you want cut. Can you toss the file you're doing up to Dropbox or somewhere so we can grab it to take a look at what you've got? 


---
**Don Kleinschnitz Jr.** *October 22, 2016 11:23*

Just a thought. My K40 FL is not 50.4. They must have installed a different lens by accident. I suggest doing a ramp test and verify where your FL actually is..... 


---
**Jim Hatch** *October 22, 2016 13:13*

Good catch Don. The other common FL lens is a 38.1 (a "short lens") although a 63.5 is found in some 60-100W machines. There are longer ones but those tend to be used in higher power machines so you can cut thicker stuff without the angling that you get as the thickness goes up. The long lenses (75/100/125) have a longer focus cone so you get much more of a straight cut through thicker material. Not an issue with the power/material limits of the K40 machines.


---
*Imported from [Google+](https://plus.google.com/111028342291028521666/posts/MJA4sYPAtyQ) &mdash; content and formatting may not be reliable*
