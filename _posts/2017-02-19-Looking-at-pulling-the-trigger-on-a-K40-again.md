---
layout: post
title: "Looking at pulling the trigger on a K40 again.."
date: February 19, 2017 20:53
category: "Discussion"
author: "Lance Ward"
---
Looking at pulling the trigger on a K40 again..  I always do this... I research and procrastinate until I talk myself out of buying it.  It's not like I need any other hobbies but I keep coming back as I started out my engineering career working with lasers and have designed control systems for several commercial laser cutting machines using multiple CO2 lasers upwards of 2000 watts. ...But that was a long time ago.  Still, $350. for a CO2 laser cutting engraving system?  Wow. Sounds like fun! :)



I was initially concerned about the motion control system but still plan on buying one eventually along with a Cohesion 3D board.



One of my other concerns has been z-axis adjustment.  There doesn't appear to be any on most of the systems.  However, I have seen a few systems (K40 type but more expensive) that advertise an "up and down" table in the unit.  I've attached a picture of the up and down table as seen in those  listings.  Does anyone have any feedback, pictures, drawings or any other information on this table?  Thanks.



[http://www.ebay.com/itm/Desktop-50W-300mm-x-200mm-CO2-Laser-Engraving-Cutting-Machine-with-Up-and-Down-/302166072798](http://www.ebay.com/itm/Desktop-50W-300mm-x-200mm-CO2-Laser-Engraving-Cutting-Machine-with-Up-and-Down-/302166072798)



![images/2e194013fc0ee9e99437ef9f117234e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2e194013fc0ee9e99437ef9f117234e1.jpeg)



**"Lance Ward"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 21:14*

That's substantially more than 350. I say get the base model and build a table yourself. Others have done it. 


---
**Andy Shilling** *February 19, 2017 21:15*

Looks like a standard k40 to me although there is a knob under the gantry to the right, not sure what that is. Anyway you would end up taking that bed out and replacing it as the adjustable clamp is awful.


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 21:16*

Even with the lightobject z table you're nowhere near the $700 mark. 



Btw you need an external stepper driver if you use the Lightobject table. So make sure to get the external stepper adapter with the C3D Laser Bundle


---
**Lance Ward** *February 19, 2017 21:39*

I don't plan on buying the unit with the up and down table.. It's too expensive for me.  I'm not sure the lightobject z table is right for me either.  I don't really need anything fancy and a manually operated z-axis would be fine for what I want.  I'm just looking for ideas or alternatives. 


---
**Don Kleinschnitz Jr.** *February 19, 2017 22:28*

I just used this for some time then I got the LO table. I got tired of constantly leveling this table.

These bolts go into a T nut that is embedded in the wood base.



![images/1fea267645a3229a6292a3965914661a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1fea267645a3229a6292a3965914661a.jpeg)


---
**3D Laser** *February 20, 2017 00:08*

I use a 40 dollar lab jack as my z table it works great for my purpose 


---
**Lance Ward** *February 20, 2017 00:09*

**+Corey Budwine** That's a pretty good idea!


---
**Madyn3D CNC, LLC** *February 20, 2017 20:41*

There's lot's of options for DIY z-tables. It can even be manual seeing as how it's going to be in a set position for cutting/engraving. Here's what I did for under ten bucks, prior to this I was using a lab jack as **+Corey Budwine** stated ....  [thingiverse.com - Madyn3D-CNC_LLC 3D printed Z Adjustable Bed for K40 Chinese Laser by Brianvanh](https://www.thingiverse.com/make:301581)


---
**Arion McCartney** *February 21, 2017 20:16*

**+Madyn3D CNC, LLC** What size belt did you use for the printed adjustable z axis design?  All I have is a length of belt, so I would need to order a loop of belt I imagine.  Didn't see that in the instructions.  Thanks.


---
**Madyn3D CNC, LLC** *February 22, 2017 17:14*

I used a  T5 belt (6mm width, 5mm pitch) and just attached it together with an extra belt tensioner from a 3D printer. To be fair, the design could definitely use some strength to it, but it works as long as it's not abused too badly.


---
**Arion McCartney** *February 22, 2017 21:02*

**+Madyn3D CNC, LLC**​ cool thanks for that info


---
*Imported from [Google+](https://plus.google.com/114422684423642232545/posts/iQx85hg2ucc) &mdash; content and formatting may not be reliable*
