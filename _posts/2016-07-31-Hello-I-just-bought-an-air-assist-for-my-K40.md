---
layout: post
title: "Hello, I just bought an air assist for my K40"
date: July 31, 2016 12:56
category: "Air Assist"
author: "Mikkel Steffensen"
---
Hello,



I just bought an air assist for my K40. But how much air pressure should I use for cutting in 3 mm acrylic?



Best regards

Mikkel





**"Mikkel Steffensen"**

---
---
**greg greene** *July 31, 2016 13:08*

Enough to clear away the smoke - trial and error as it depends on cutting speed and power.


---
**Scott Marshall** *July 31, 2016 15:38*

it's lower than you'd expect.


---
**Mikkel Steffensen** *August 02, 2016 07:56*

Thank you. I will try it out


---
*Imported from [Google+](https://plus.google.com/117825343986845004843/posts/1iRuAQF7dxF) &mdash; content and formatting may not be reliable*
