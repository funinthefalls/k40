---
layout: post
title: "Another arcing video. This one is more similar to what I experienced on one of my lasers when there was heavy condensation on the tube"
date: June 15, 2017 18:34
category: "Discussion"
author: "Nathan Thomas"
---
Another arcing video. This one is more similar to what I experienced on one of my lasers when there was heavy condensation on the tube.





**"Nathan Thomas"**

---
---
**Chris Hurley** *June 16, 2017 03:23*

The tube is busted. Right now the effect is is that it's easier for the electricity to Arc to the case and it is the flow down the tube. I ended up having to replace my tube. 


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/9LYvrYuCkMi) &mdash; content and formatting may not be reliable*
