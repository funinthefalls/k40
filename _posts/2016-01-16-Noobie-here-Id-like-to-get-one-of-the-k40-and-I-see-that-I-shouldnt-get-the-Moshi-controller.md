---
layout: post
title: "Noobie here. I'd like to get one of the k40 and I see that I shouldn't get the Moshi controller"
date: January 16, 2016 06:47
category: "Discussion"
author: "Tony Sobczak"
---
Noobie here.  I'd like to get one of the k40 and I see that I shouldn't get the Moshi controller.  How can I be sure the unit I purchase is not a Moshi.  I see some are listed as coming with moshIdraw some only list corell

Corel Draw.  Can anyone provide a seller that has a different controller. ﻿





**"Tony Sobczak"**

---
---
**Tony Sobczak** *January 16, 2016 07:19*

Just to be sure corel draw doesn't come with a mosh board? 


---
**Tony Sobczak** *January 16, 2016 07:27*

Cool thank you. Several out there with corel. 


---
**Stephane Buisson** *January 16, 2016 16:30*

**+Tony Sobczak** You should realise that both Moshiboard/Moshidraw or Nanoboard/Laserdraw are both close sources and proprietary (laserdraw being better than Moshidraw). if you want to open up to free softwares (some compatible MAC/LINUX), you will need to change your board, and in that case Moshi or Nano doesn't matter.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/MFr4byPJrU5) &mdash; content and formatting may not be reliable*
