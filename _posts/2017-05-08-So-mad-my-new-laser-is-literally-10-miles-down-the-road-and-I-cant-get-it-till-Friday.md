---
layout: post
title: "So mad my new laser is literally 10 miles down the road and I can't get it till Friday"
date: May 08, 2017 13:14
category: "Discussion"
author: "3D Laser"
---
So mad my new laser is literally 10 miles down the road and I can't get it till Friday.  My stupid road I live on does not all big trucks so they have to put it on a smaller on to get it to me and the soonest is Friday 





**"3D Laser"**

---
---
**HalfNormal** *May 08, 2017 15:07*

Can you rent a truck and pick it up yourself? 


---
**3D Laser** *May 08, 2017 15:38*

I could but that is money and time i don't want to spend 


---
**Steve Clark** *May 08, 2017 15:57*

What laser a K40? Do you have car? A couple of heavy blankets and a friend would get it into the back seat or your trunk ( if is packed in the card board box like mine was). Might need a rope or two to hold it in the truck though.


---
**3D Laser** *May 08, 2017 15:59*

It's a 250lbs k50 big blue eBay special


---
**Steve Clark** *May 08, 2017 16:05*

Ya I see that> I was just re-posting a " never mind" as I found your earlier post on the K50.


---
**Ned Hill** *May 08, 2017 16:25*

Well that does indeed suck.


---
**Nigel Conroy** *May 09, 2017 13:33*

I got the K50 in the back of my SUV. 

Volvo xc90 for reference


---
**3D Laser** *May 09, 2017 15:29*

**+Nigel Conroy** just a quick question what is the actual size of the cutting bed I want to get either a honey comb or blade table ordered and want to make sure it's not to small or to big 


---
**Nigel Conroy** *May 09, 2017 18:06*

**+Corey Budwine**

Probably a bit more then you want but here you go....

Red cross-hairs are extremes of laser head 

Black circles are the threaded rods the bed moves up and down on



A new surface that was 21 in x 14 in would fit in nicely and cover the working area of the laser. 



Something with adjustable leveling feet is a good idea as the surface of the table isn't flat as it's light aluminum



[docs.google.com - K50 Laser bed](https://docs.google.com/drawings/d/1GmB_JZZ7nojd7aOcR1yZNCnXumglOlq03bAOTiUdqeE/edit?usp=sharing)


---
**3D Laser** *May 09, 2017 21:17*

**+Nigel Conroy** thanks I am ordering a blade bed from Ali express 


---
**3D Laser** *May 09, 2017 21:46*

**+Nigel Conroy** I though about doing 21.5 by 17 so it is renting on the frame not the aluminum bed 


---
**Nigel Conroy** *May 10, 2017 14:54*

**+Corey Budwine** you'll need to explain a bit more by what you mean?



21.5 would be very tight to insert in mine as it wouldn't really fit in between the threaded rods






---
**Phillip Conroy** *May 12, 2017 19:11*

Unlike the k40 what you see on the  computer screen is not what gets cut with the k50 ,change view to wire frame is closer to what gets cut,the weld fuction will become you new best friend 


---
**3D Laser** *May 12, 2017 19:26*

**+Phillip Conroy**  it's finally here!![images/a2fad78c4e14e6e845bb5c447293be88.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a2fad78c4e14e6e845bb5c447293be88.jpeg)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/gkfZDALcjJx) &mdash; content and formatting may not be reliable*
