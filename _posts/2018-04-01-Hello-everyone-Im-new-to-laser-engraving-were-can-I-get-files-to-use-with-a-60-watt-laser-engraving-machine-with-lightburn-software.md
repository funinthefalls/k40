---
layout: post
title: "Hello everyone I'm new to laser engraving were can I get files to use with a 60 watt laser engraving machine with lightburn software?"
date: April 01, 2018 00:47
category: "Discussion"
author: "Inland Empire Engravings"
---
Hello everyone I'm new to laser engraving were can I get files to use with a 60 watt laser engraving machine with lightburn software? What materials work best with what speed and power any help would be appreciated thank you





**"Inland Empire Engravings"**

---
---
**Phillip Conroy** *April 01, 2018 01:31*

Every laser cutter is different, 3mm mdf and 3mm ply wood  for cutting  15 mm per second and 50% power ,etching these 350 mm per second and 40% power .

Best you get a ma fitted and untill you do don't go over 60% power as you can not tell what real ma power is going to the tube.

Clean focal lens once a week with achahol, bump goes upwards,Never leave machine when it is cutting or etching,have fire extinguisher close by and a water spray bottle to use first.

Keep an eye on water temperature, 30 max ,15 deg c min.

Acrylic cuts at around 10 mm per second and 60 % power,  no need to remove protection layer of paper ,etching acrylic 300 mm per second and 60 % power.

Check what materials are safe to cut, and what's not ie never cut vinyl 

Have a play with power and speed settings ,do test cuts


---
*Imported from [Google+](https://plus.google.com/102177636117481238975/posts/7AWHi31UFLJ) &mdash; content and formatting may not be reliable*
