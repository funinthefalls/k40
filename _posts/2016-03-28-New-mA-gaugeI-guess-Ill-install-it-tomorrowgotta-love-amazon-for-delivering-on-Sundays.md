---
layout: post
title: "New mA gauge....I guess I'll install it tomorrow...gotta love amazon for delivering on Sundays"
date: March 28, 2016 00:16
category: "Modification"
author: "Scott Thorne"
---
New mA gauge....I guess I'll install it tomorrow...gotta love amazon for delivering on Sundays. 

![images/05e323a3e7c96d487fedfbee1876775c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05e323a3e7c96d487fedfbee1876775c.jpeg)



**"Scott Thorne"**

---
---
**ED Carty** *March 28, 2016 01:18*

You could have etched what the gauge is for .. water pump.. air .. etc.. otherwise awesome..


---
**Scott Thorne** *March 28, 2016 02:25*

You're right **+ED Carty**..

This is a temp box...I will mount it on the cover when I'm sure it will work ok. 


---
**ED Carty** *March 28, 2016 02:37*

looks good as always my friend


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/YdEHLdsvBeq) &mdash; content and formatting may not be reliable*
