---
layout: post
title: "I've been collecting bits and pieces together to make a new motion controller, the first thing I shall try it on will be my K40 laser engraver"
date: August 19, 2015 21:56
category: "Smoothieboard Modification"
author: "David Richards (djrm)"
---
I've been collecting bits and pieces together to make a new motion controller, the first thing I shall try it on will be my K40 laser engraver. I think it will end up on my delta 3d printer eventually though. At the front of the photo is the little middleman board needed to breakout the laser ribbon cable to something easy to wire to. The main board and LCD are the AZSMZ Mini controller and display, it runs the smoothieware. it all just worked out of the box even without an SD card inserted, the software  defaults  match the hardware. I'm  tuning the configuration with my own setting right now however. Next job is to set the stepper current as its running into overload  when working for more than a short while.

![images/aad82b7d21b5b0f31460d2dc137b5324.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aad82b7d21b5b0f31460d2dc137b5324.jpeg)



**"David Richards (djrm)"**

---
---
**Flash Laser** *August 20, 2015 02:54*

waitting for your good news!!


---
**Stephane Buisson** *October 20, 2015 15:42*

any progress so far ?


---
**David Richards (djrm)** *October 20, 2015 17:26*

Ive beeen busy elsewhere so sadly very little progress to /date. I have the esp8266 wifi serial link working nicely. I made a display case using a 3D printer design I found on thingiverse.  All the board configuration looks good to go. I need to take the plunge and swap out the existing controller next. D


---
**adam hirsch** *November 03, 2015 04:43*

David,

is this like the Ramps setup? i have been thinking about taking the plunge and converting over to another type of controller on my K40 but want to do it the best way and can't afford 500.00 for a DSP. Can you share your setup and victories.  thank you in advance


---
**Stephane Buisson** *November 03, 2015 08:57*

**+adam hirsch** I will do a full post about my Smoothieboard 4XC and K40 by the end of this week. solution I highly recommend, as the software chain is a dream compare original Moshi. (Inkscape+Vivsicut). at the moment you need to know in Smoothieware board, you need one with ethernet connection (visicut). Smoothieboard 4XC conversion cost 1/3 of DSP.


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/Huw6vKr2GQs) &mdash; content and formatting may not be reliable*
