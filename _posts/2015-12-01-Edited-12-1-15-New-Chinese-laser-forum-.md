---
layout: post
title: "Edited: 12/1/15 New Chinese laser forum: "
date: December 01, 2015 15:15
category: "External links&#x3a; Blog, forum, etc"
author: "Coherent"
---
Edited:  12/1/15

New Chinese laser forum:

[http://k40d40lasers.freeforums.net/](http://k40d40lasers.freeforums.net/)﻿





**"Coherent"**

---
---
**Stephane Buisson** *December 01, 2015 16:11*

Sincere good luck !

it's easy at the beginning, but more complicated with 500 + members. ( spam, ego, ...)


---
**Stephane Buisson** *December 01, 2015 16:17*

**+Carl Duncan**  unfortunately, I don't think it's possible with G+, need a forum for that.


---
**Stephane Buisson** *December 01, 2015 16:55*

**+Carl Duncan** php bb + domain name + hosting, not much compare to the administration time and moderation.



1) like a flipper, the ball is always going down. it's hard to maintain if 100% public, and could be frustrating if going wrong.



2) but don't take me wrong, I am still happy to have here plenty of photos to support a interresting points to share. and I would encourage also the category "object made with laser".


---
**Stephane Buisson** *December 01, 2015 17:25*

Just ask me, what category do you suggest ?


---
**Coherent** *December 01, 2015 17:28*

The only way I can think of to associate a photo in a reply is to use your Googledrive (everyone has 15gb that is part with your gmail storage etc) Add the photo there, then put a link to the photo in your reply.


---
**Coherent** *December 01, 2015 18:08*

No problem whatsoever. It's too bad there is no simple way to search, reply with attachments/photos or navigate the communities. Google+ can be a bit cumbersome in some ways and great in others. I'm researching the possibility of starting an actual "forum" but still in the beginning stages of determining feasibility.

I've started the "draft" forum at the following link. It looks like it offers all of the benefits of a forum without some of the shortcomings that  the communities have. Comments, inputs and support/feedback are most welcome.


---
**Yuxin Ma** *December 12, 2015 17:21*

The lasercutting subreddit is somewhat active. You can just do imgur links in there


---
*Imported from [Google+](https://plus.google.com/112770606372719578944/posts/KhipH9w9WJ6) &mdash; content and formatting may not be reliable*
