---
layout: post
title: "Shared on September 27, 2018 12:03...\n"
date: September 27, 2018 12:03
category: "Object produced with laser"
author: "Francesco Primiceri"
---






**"Francesco Primiceri"**

---
---
**Don Kleinschnitz Jr.** *September 27, 2018 14:04*

**+Francesco Primiceri** thanks for the link.

#K40CADFiles

I really like the point system this uses. You upload files to get points which you can then use to download. Evidently points received are proportional to the complexity of the design. True sharing!


---
**Kelly Burns** *September 27, 2018 21:03*

Very nice.  Great exchange.


---
**Anthony Bolgar** *September 29, 2018 00:22*

Noticed that the site has quite a few files that are copyrighted by the original authors, and not the uploaders. I do not think they are screening very well for copyrighted material. As well, the posting function is disabled, you have to buy points right now to d/l any files. The cynic in me thinks it is just another site selling other peoples files illegally, and trying to make it look otherwise. But that is just my opinion, not a confirmed fact.


---
**Kelly Burns** *September 29, 2018 00:49*

**+Anthony Bolgar** yeah. That’s going to be an issue. Hopefully the claim process that they have in place will make it easy to fix 


---
*Imported from [Google+](https://plus.google.com/+FrancescoPrimiceri/posts/eEcPkXhC2t2) &mdash; content and formatting may not be reliable*
