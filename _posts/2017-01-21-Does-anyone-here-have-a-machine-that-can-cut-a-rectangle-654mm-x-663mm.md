---
layout: post
title: "Does anyone here have a machine that can cut a rectangle 654mm x 663mm?"
date: January 21, 2017 08:30
category: "Discussion"
author: "Alex Hayden"
---
Does anyone here have a machine that can cut a rectangle 654mm x 663mm?





**"Alex Hayden"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 21, 2017 13:05*

**+Scott Thorne** possibly? Can't remember the size of his machines but I know he has 1 larger than the standard K40.


---
**Scott Thorne** *January 21, 2017 13:10*

**+Yuusuf Sallahuddin**...mine is 600 x 400....I've been busy building a 70 watt Raman fiber laser machine that will cut and engrave metal...i got the laser on Ebay for the 116.50....won it in a auction. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 21, 2017 13:20*

**+Scott Thorne** Nice, I'll be interested to see results of your fibre laser when it's up & running.


---
**Alex Krause** *January 21, 2017 13:41*

What thickness of material? And does it have to laser cut? Or would a CNC router work?


---
**Scott Thorne** *January 21, 2017 13:50*

**+Yuusuf Sallahuddin**...thats the laser .

![images/701fed9b324e662e5e66cdfda2977aff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/701fed9b324e662e5e66cdfda2977aff.jpeg)


---
**Alex Hayden** *January 21, 2017 16:01*

**+Alex Krause**​ thickness 5mm or 3mm. Materials I am pretty open to anything you think is strong enough, but probably aluminum. Thought about some plywood just for this prototype. Its for a CoreXY/Hbot duel print head with a 434mm cubed volume build area. Here is a picture.



If you can cut, are you willing to give me a quote? Are you in the USA?

![images/952fe1f7f2800acd719800cb6fa16be2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/952fe1f7f2800acd719800cb6fa16be2.jpeg)


---
**Alex Krause** *January 21, 2017 16:04*

Let me verify tonight when  I get home that I that much usable cutting area... Are you making side covers or back covers?


---
**Alex Krause** *January 21, 2017 16:05*

Is it the bottom piece with the 3 linear motion mounting holes?


---
**Alex Hayden** *January 21, 2017 16:13*

**+Alex Krause**​ Currently just looking for the top and bottom and the build surface that has the 4 speed screw mounts.


---
**Alex Hayden** *January 21, 2017 16:21*

**+Alex Krause**​ thanks, looking forward to hearing from you later.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 21, 2017 21:25*

**+Scott Thorne** Wow, surprisingly smaller than I had imagined.


---
**Scott Thorne** *January 22, 2017 16:06*

Thats just fiber laser and controller.....its 24x24x8


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 23, 2017 17:11*

**+Scott Thorne** Ah yeah, I was meaning the fibre laser component. I imagined it would be much larger than that box, but after reading up how fibre lasers work, it makes sense that it isn't as giant as I had imagined. 24x24x8 seems like a decent sized bed :)


---
**Scott Thorne** *January 23, 2017 22:34*

**+Yuusuf Sallahuddin**...24x24x8 is the size of the fiber laser control cabinet...the bed will be 36x24 when i get finished with it....that's the length of the linear rails that i had at my shop...it will be done in a month or so...IPG Photonics is sending me a pinout for the 24 pin interface plug.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 24, 2017 01:55*

**+Scott Thorne** Oh right, so in that case the laser control cabinet is quite large. The photo is deceptive. Watching some vids on youtube of fibre lasers in action they seem amazing. I saw one cutting through inch thick steel like it was butter.


---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/D8fDVXDbBvv) &mdash; content and formatting may not be reliable*
