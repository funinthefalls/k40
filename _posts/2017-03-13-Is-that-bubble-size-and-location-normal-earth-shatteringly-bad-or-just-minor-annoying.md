---
layout: post
title: "Is that bubble size and location normal, earth-shatteringly bad, or just minor/annoying?"
date: March 13, 2017 00:18
category: "Original software and hardware issues"
author: "Bob Buechler"
---
Is that bubble size and location normal, earth-shatteringly bad, or just minor/annoying? 



I guess what I'm getting at is, should I not fire it at all, not even for alignment testing, without making it go away? Or is this par for the course?



This is, of course, the stock craptastic water pump.

![images/81e6861e1d073d043655fb3e05468c51.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/81e6861e1d073d043655fb3e05468c51.jpeg)



**"Bob Buechler"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 13, 2017 00:37*

Personally I wouldn't run the tube with a bubble right there. Considering that is very close to where the voltage line is, I'd assume it is likely to get hotter in that region than others (although I don't know for certain).



A solution for minimising/preventing bubbles is to have your water reservoir level or higher than the K40. In mine, I also have the return water flow hose under the water level (to minimise the creation of bubbles upon water re-entry & when the pump is turned off air cannot displace the water).



I found that by squeezing the inlet pipe shut (while the pump is operational) & then releasing it provides a small increase in pressure which is sometimes substantial enough to force the bubble into the outlet pipe.


---
**ThantiK** *March 13, 2017 02:41*

Mine had one of these, and I just left the water pump on for a couple days until it disappeared.  Don't fire the laser with that bubble there.  I suspect 50% of peoples initial problems with the machine are because of firing with that air bubble there and overheating the tube.


---
**Bob Buechler** *March 13, 2017 07:17*

Thanks for the advice! I will adjust my setup accordingly and make sure that thing is gone before firing again. Thankfully I haven't done almost any firing at all on it yet. :)


---
**Andy Shilling** *March 13, 2017 09:45*

Rotate your tube so the water outlet is sat at the top then the bubble will go.


---
**Steven Whitecoff** *March 13, 2017 15:51*

Since the laser and mechanism doesnt care about being level, just tilt ithe whole thing up till the bubbles leaves the tube, takes 15sec. and done. I run mine with the pump much lower than the tube and no bubble come back once you burp it. As Andy says, outlet up helps clearing and preventing bubbles.




---
**Bob Buechler** *March 13, 2017 23:21*

if you rotate the tube, does the laser have to be realigned? 


---
**Andy Shilling** *March 13, 2017 23:23*

It shouldn't do no, just loosen off the clamps enough to turn it by hand then tighten again.


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/99MPg3fZETf) &mdash; content and formatting may not be reliable*
