---
layout: post
title: ".... anyone used these mounts on a K40?"
date: November 08, 2016 16:49
category: "Modification"
author: "Don Kleinschnitz Jr."
---
.... anyone used these mounts on a K40?

I know that I can print or make my own but ....... :)

[https://www.smw3d.com/laser-tube-mount/](https://www.smw3d.com/laser-tube-mount/)





**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *November 08, 2016 16:52*

I need to print one, I think I saw one on thingiverse


---
**Don Kleinschnitz Jr.** *November 08, 2016 16:55*

[https://plus.google.com/113684285877323403487/posts/QJEdj4eTXgP](https://plus.google.com/113684285877323403487/posts/QJEdj4eTXgP)



[plus.google.com - Adjustable laser tube mounts .Any one found any of these that fit into a k40 ﻿](https://plus.google.com/113684285877323403487/posts/QJEdj4eTXgP)


---
**Alex Krause** *November 08, 2016 17:22*

**+Brandon Satterfield**​ do you have a picture of this in use?


---
**Ian C** *November 09, 2016 13:43*

Hey. Don't fit I'd read, shame as they look good for the price. I have 3D printed one of the remixed K40 laser tube hangers, need to make a 2nd and then fit them. Was going to wait until either I change the tube though a faulty one or the alignment moves. Currently machines working well so I'm leaving it alone. I can post the link later from my pc if needed


---
**Brandon Satterfield** *November 10, 2016 00:12*

**+Alex Krause** , **+Ian C** is correct they will not fit the standard K40 machine. We will continue to add parts with the K40 in mind. 


---
**Ian C** *November 11, 2016 13:30*

I've printed one so far, need to do a second. Had to make my own plastic nuts to fit M5 nuts, as the included one did not fit, was too big for metric nut. Not tried fitting it yet though so hope it fits :-/

![images/8a4638029e0139c6197237db5d747a56.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a4638029e0139c6197237db5d747a56.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/QB48Fm7mYTP) &mdash; content and formatting may not be reliable*
