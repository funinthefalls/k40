---
layout: post
title: "This is what a lightning strike to my house did to the m2-nano board"
date: April 04, 2017 16:27
category: "Smoothieboard Modification"
author: "Jim Fong"
---
[https://imgur.com/a/PKVRS](https://imgur.com/a/PKVRS)



This is what a lightning strike to my house did to the m2-nano board.  It popped the top off the chip by the USB connector and burned the resistor next to it.  The laser still fires if I press the top panel test button.  I installed the replacement Cohesion3d board, However I can't get the Cohesion3d board to fire the laser yet. I can jog both steppers.  



The PC that was connected to the Laser died too. 





**"Jim Fong"**

---
---
**Cesar Tolentino** *April 04, 2017 16:38*

oh wow. im so sorry to hear that. how can we prevent this? can surge protector prevent this from happening?


---
**Jim Fong** *April 04, 2017 16:51*

**+Cesar Tolentino** not much can stop a direct strike.  Lightning hit the metal rooftop furnace exhaust and blew that off the house.  All that energy then destroyed almost all the electronics and appliances in the house.  None of the surge protectors in the house helped.  






---
**Ashley M. Kirchner [Norym]** *April 04, 2017 17:15*

What are you using to talk to the C3D board? How are you trying to fire the laser? What's your wiring look like?


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 17:31*

Jim you're from the original batch so we have to make sure that your config file matches up with your wiring. 


---
**Jim Fong** *April 04, 2017 17:35*

**+Ashley M. Kirchner** I just installed it and did a quick test, then shut it down.  Have to go to work.  Ill look at it later when I have more time. 



Using laserweb4



Original wiring pics



[i.imgur.com](https://i.imgur.com/uAn4C1Z.jpg)



[https://i.imgur.com/AZk06lp.jpg](https://i.imgur.com/AZk06lp.jpg)


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 17:48*

Similar board as my original one, but I want to see your wiring to the C3D board. And your config file. Ray has the details for the original batch boards, which is also what I have.


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 17:52*

Yeah we pretty much need to cut the L wire and connect it to 2.5 - bed screw terminal, change the laser pin in the config from 2.4! to 2.5 

Keep the pot connected. 

Rest of it should work out fine. 


---
**Jim Fong** *April 09, 2017 17:49*

**+Ray Kholodovsky** had some time today to work on the laser.  Connected L to 2.5 and configured Smoothie/laserweb4.  Had to use 1.0 smoothie drivers.  1.1 wouldn't install right on my win7 PC.  



Laserweb4 can home, jog and test fire the laser.  Atleast I know the laser power supply survived without damage.  Kinda glad I didn't install your mini board back when I first received it.  Probably would have died in the power surge strike.  I'm really happy the laser works now.  



Also worked on my gantry CNC. It's now 100% operational again.   The other CNC machines still not working yet. 


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 18:11*

Very happy to hear that Jim. Please keep us posted. 


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/ZibnCARJNP8) &mdash; content and formatting may not be reliable*
