---
layout: post
title: "Adjustable Bed A BIG thank you to Kelly S for the design of the bed and Andy Shilling for being lazy!"
date: April 08, 2017 23:44
category: "Modification"
author: "HalfNormal"
---
Adjustable Bed



A BIG thank you to  **+Kelly S** for the design of the bed and **+Andy Shilling** for being lazy! (he found the drawings in his computer trash!)



I printed up the parts and mounted them to the old clamp plate that comes with the K40. This makes it rock solid and easily removeable. The only issue seems to be that not all ball chains are created equal. There can be a little slip on the gear heads. I find the chain slips when using the manual adjuster. If I pull on the chain, it works like a champ.



Bottom line, it is a great adjustable bed for the price of parts. I think I have less than $10.00 USD in it.



PS If you look to the left of the bed, those are parts for making an adjustable laser mount. Pics to come!

![images/e36fd5b10e970a795f19b30fd1f8c8d5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e36fd5b10e970a795f19b30fd1f8c8d5.jpeg)



**"HalfNormal"**

---
---
**Joe Alexander** *April 09, 2017 00:58*

I have those same laser mounts in my K40-S as we speak, tight fit but they work well. Make sure you put something to pad where the screws thread into the tube holder it can poke the tube otherwise.


---
**HalfNormal** *April 09, 2017 00:59*

**+Joe** thanks for the info!


---
**Rodney Huckstadt** *April 09, 2017 08:02*

can you share the 3d prints at all ? found the original but not the mod for the chain drive ?




---
**Shane Graber** *April 09, 2017 16:00*

Where are the original files?


---
**HalfNormal** *April 09, 2017 16:04*

Thanks to **+Andy Shilling**​​​ he posted the files. Original on Thingiverse. 

 [drive.google.com - Ballchain - Google Drive](https://drive.google.com/drive/u/0/mobile/folders/0B4PVxTUHzQtfYkh3QW1ZNGhIZm8)


---
**Andy Shilling** *April 09, 2017 21:05*

**+HalfNormal**​ I can only take credit for not deleting +Kelly S remix of the original 😁 but it seems your having the same problem i had with the balls slipping.



That is why I went back to the original with the gt2 6mm belt, mine now doesn't slip and i can adjust it one handed with the printable handle. I don't know what you use for a bed but I'm happy with this and the extra little orange washer things I printed hold the bed level and reduce friction when adjusting it.

![images/4cd23e96e38e3f73607a39767fb18f14.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4cd23e96e38e3f73607a39767fb18f14.jpeg)


---
**James Rivera** *March 19, 2018 17:28*

**+Andy Shilling** **+HalfNormal** can one of you please post the link to the files on Thingiverse?


---
**HalfNormal** *March 19, 2018 17:31*

**+James Rivera** 

[thingiverse.com - Z Adjustable Bed for K40 Chinese Laser by brianvanh](https://www.thingiverse.com/thing:1906231)


---
**Andy Shilling** *March 19, 2018 17:31*

**+James Rivera** Z Adjustable Bed for K40 Chinese Laser found on #Thingiverse [https://www.thingiverse.com/thing:1906231](https://www.thingiverse.com/thing:1906231)

[thingiverse.com - Z Adjustable Bed for K40 Chinese Laser by brianvanh](https://www.thingiverse.com/thing:1906231)


---
**Andy Shilling** *March 19, 2018 17:33*

**+HalfNormal** you beat me to it lol.



 **+James Rivera** I went back to the gt2 belt as it was better than the ball but joining a belt isn't easy.


---
**HalfNormal** *March 19, 2018 17:34*

Little slow today Andy? ;-)



Yes, making the belt a loop is a B**ch!


---
**Andy Shilling** *March 19, 2018 17:39*

Busy making planes buddy. I went for a wire reinforced belt going that would make it easier, let say it took me all day to get a semi flexible join in it. Just hope I don't need to make another one any time soon.![images/82d056454d1d6c21886bfc38c0f700df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/82d056454d1d6c21886bfc38c0f700df.jpeg)


---
**HalfNormal** *March 19, 2018 17:40*

Looks like good ol' plane fun!


---
**James Rivera** *March 19, 2018 17:41*

**+Andy Shilling** Excellent! I have a 3D printer and think I already have all the parts needed to make this. I’ve been pondering how to make a single point/manual adjustable platform and this looks like current favorite* design.



*opinion subject to change without notice! 😉


---
**James Rivera** *March 19, 2018 17:44*

**+Andy Shilling** I intend to use either GT2 or T2. I have some of the latter lying around from my first Printrbot LC Plus. Not sure if I have enough though. And it looks like I might need it to be a closed loop.


---
**Andy Shilling** *March 19, 2018 17:49*

**+James Rivera** yes it needs to be a loop. It may not be the best table or there but it works, I've made mine so I can remove the bed if I need to put bigger items in. 



I think the biggest draw back on this design is the threaded rods reduce your overall bed size of you want to engrave on a thicker peice of wood as they protrude as you do the bed down.


---
**HalfNormal** *March 19, 2018 17:50*

Cheaper than the LightObjects $160.00 bed!


---
**James Rivera** *March 19, 2018 18:34*

**+Andy Shilling** I noticed that. I'm going to think about what might be a better way; perhaps having the threaded rods outside the print area and having some kind of bed supports that reach under it from the periphery? Just a thought; I'm not done thinking about it yet.


---
**James Rivera** *March 19, 2018 18:35*

**+HalfNormal** True! But LO's powered bed looks really nice. Totally overkill for me though.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/PNyrev4xqS4) &mdash; content and formatting may not be reliable*
