---
layout: post
title: "Has anyone considered reworkinging the exhaust on a K-40 to allow a matetial pass-through slot?"
date: September 17, 2018 12:31
category: "Modification"
author: "Tom Traband"
---
Has anyone considered reworkinging the exhaust on a K-40 to allow a matetial pass-through slot? My first thought was to pull exhaust through the left side, but then I thought that might negatively impact power as it would be sticking the smoke through the besm before it got to the second mirror.







**"Tom Traband"**

---
---
**Stephane Buisson** *September 17, 2018 13:47*

not exactly the exhaust but have a look :


{% include youtubePlayer.html id="0r5sdS8zqY0" %}
[https://www.youtube.com/watch?v=0r5sdS8zqY0](https://www.youtube.com/watch?v=0r5sdS8zqY0)



the exhaust difficulty is you need to connect the fan, so air tight it need to be, which is not compatible with having a slot to let the material pass trough. this why I put it on the other side.



I could see an option on the right, the air flow could also cool the electronic.


---
**Tom Traband** *September 17, 2018 17:17*

I thought about that too but I'm not sure i want to pull the dirty air across the electronics in case the dirt/smoke should deposit there.


---
**Timothy Rothman** *October 07, 2018 18:41*

The exhaust setup is really bad.  Overall I think it's a design flaw. 

 Round fan on a rectangular hold half the size isn't really efficient and prevents inspection of the air bubbles in the tube with the fan on. Bubbles are a large source of fried tubes.



Every time my machine sits with the water off, before re-firing, I  disconnect the fan run the water pump for a while to clear bubbles, then reconnect and have to seal it all over again.  It's a big PITA, but IMO necessary to protect the tube.  I was planning on putting a sight window over the tube, but not enough time with other projects.



I have a 6" inline fan and an activated charcoal filter at the end- one of the benefits of living in a state with legal marijuana.  I think the fan and charcoal filter were $100 off CL and I can get new filters for about $150.  I don't worry about running a 4" line to a 6"fan because it's really not a 4" round opening anyway (as stated earlier)



Much like this ([https://www.instructables.com/id/Build-a-laser-cutter-fume-extractor/](https://www.instructables.com/id/Build-a-laser-cutter-fume-extractor/)) but without all the arts and crafts work.


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/5AVs5scvv3f) &mdash; content and formatting may not be reliable*
