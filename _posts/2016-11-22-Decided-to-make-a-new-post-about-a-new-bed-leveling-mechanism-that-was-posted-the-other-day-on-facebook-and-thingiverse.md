---
layout: post
title: "Decided to make a new post about a new bed leveling mechanism that was posted the other day on facebook and thingiverse"
date: November 22, 2016 06:21
category: "Modification"
author: "Kelly S"
---
Decided to make a new post about a new bed leveling mechanism that was posted the other day on facebook and thingiverse.  [http://www.thingiverse.com/thing:1906231](http://www.thingiverse.com/thing:1906231) it was originally intended to use a belt similar to a one used by a stepping motors.  As that is a hard item to find in my area and I am too impatient to order one I have edited the gear drive to use a bicycle chain.  Will post pictures later tomorrow of how it turned out.  :)

![images/7c0d8368fe0fa3f904c8495a050121fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7c0d8368fe0fa3f904c8495a050121fb.jpeg)



**"Kelly S"**

---
---
**Timo Birnschein** *November 22, 2016 18:05*

I love this idea! Should be precise enough for bed leveling a laser!


---
**Kelly S** *November 23, 2016 22:21*

So I got this to work with a ball chain, will upload a video later to YouTube and then share here, for now a picture.  :)  It seems to work really good, will adjust final tension when it is bolted into the machine.  Took a couple days of thinking, but really did not want to use rubber or plastic belts as the laser can reach the front.  Tried bike chain (too heavy) and just wondered around the hardware store till I seen this chain, haha.  **+Timo Birnschein**

![images/58f1d48f490defb8bbc9a4a78c9c0129.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58f1d48f490defb8bbc9a4a78c9c0129.jpeg)


---
**Kelly S** *November 23, 2016 22:22*

![images/946df37b2bd41a1eb12233f329d2df97.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/946df37b2bd41a1eb12233f329d2df97.jpeg)


---
**Kelly S** *November 23, 2016 22:53*

Here is a link to a short video on YouTube.  
{% include youtubePlayer.html id="eaFhdqUQaiI" %}
[youtube.com - DIY laser cutter bed that is adjustable](https://youtu.be/eaFhdqUQaiI) hope it can inspire someone else.  


---
**Timo Birnschein** *November 24, 2016 02:03*

I love this "how cheap can you get" approach! Excellent prove of concept! **+Kelly S**!




---
**Kelly S** *November 24, 2016 03:04*

Thanks, my main goal was to keep everything easily obtainable locally.   And I live in a very small town.  Will upload my remix to thingiverse next time I am in my office.  :)


---
**HalfNormal** *November 24, 2016 18:14*

**+Kelly S** I am with the family in CO for the holidays. I cannot wait to get back home to start printing the parts! I think I will add thrust bearings and washers on the adjustment nuts to smooth out the lift. Thanks for doing the hard work for us!




---
**Kelly S** *November 25, 2016 03:43*

Here is the link to my thingiverse file [thingiverse.com - K40 manual Adjustable Bed with Ball Chain by AntTopia](http://www.thingiverse.com/thing:1915794)


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/j8RaFxgWB3z) &mdash; content and formatting may not be reliable*
