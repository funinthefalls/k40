---
layout: post
title: "I have learned so much from reading all the posts in this awesome community!"
date: May 25, 2017 02:33
category: "Object produced with laser"
author: "Nick Winnard"
---
I have learned so much from reading all the posts in this awesome community! So I figured I would share a finished product faceplate produced with K40, Laserweb4 and C3D board. 

![images/0ea9bd06b12502cf67ec5b0ba67b8c4b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0ea9bd06b12502cf67ec5b0ba67b8c4b.jpeg)



**"Nick Winnard"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2017 02:39*

Coolio.  So how are you finding everything in the "new workflow"? 


---
**Nick Winnard** *May 25, 2017 02:54*

**+Ray Kholodovsky** I started working more seriously on learning LW3 just about a month or two before the launch of LW4. So I guess I haven't had much transition or relearning from any older version. But I personally have found the Lw4 workflow very intuitive. I'm sure I haven't even come close to unlocking the full potential in the time I've spent with it, but I'm very pleased so far, and enjoying the learning! 


---
**Anthony Bolgar** *May 25, 2017 03:07*

Do you make custom pedals **+Nick Winnard**?


---
**Nick Winnard** *May 25, 2017 03:27*

**+Anthony Bolgar** Yes sir! I've done quite a number of custom builds (pedals, tube amps, pickups, etc!) I love what the laser can do for the pedals, and amp plates! 


---
**Anthony Bolgar** *May 25, 2017 06:37*

Do you have a custom flanger pedal for sale? I am looking for a very warm flange effect, not so much reverb as a digital pedal would have.


---
**Nick Winnard** *May 25, 2017 16:31*

**+Anthony Bolgar** I don't currently have a flanger offering, but from your description I do have a couple that I have tried personally that I'd recommend. It sounds like you're looking for the warmth of a true analog flanger rather than the more metallic edge of a digital.



Vintage MXR or Ross flanger with the SAD1024 chip. 

Ibanez FL9 (the original not the reissue) 

Hartman analog Flanger

The Boss BF-2 with some modification can also be warmed up pretty well. 



And believe it or not, A surprisingly decent little analog sounding flanger is the Danelectro DJ-8. They are cheap and not the best built. But they sound quite good. Gotta love chasing tone! 


---
**Anthony Bolgar** *May 25, 2017 18:51*

Thanks for the rundown of flangers, it is much appreciated. I'll take a look and listen (if I can) at  them, hopefully the Danelectro would fit the bill, I like the idea of inexpensive, not too worried about build quality, it would not be gigged at all.


---
*Imported from [Google+](https://plus.google.com/112795894257485102297/posts/WNvLetNCyjT) &mdash; content and formatting may not be reliable*
