---
layout: post
title: "Should my tube be arcing like this at the end?"
date: April 10, 2016 02:53
category: "Discussion"
author: "3D Laser"
---
Should my tube be arcing like this at the end?  All of a sudden my alignment is off and I do t know what went wrong it was cutting great and then nothing 

![images/83224457b6f3b98dd141e35f54d45ebf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/83224457b6f3b98dd141e35f54d45ebf.jpeg)



**"3D Laser"**

---
---
**ThantiK** *April 10, 2016 03:29*

Is that air I see in your tube?


---
**3D Laser** *April 10, 2016 03:34*

A little bit it wasn't there when cutting I removed the pump line when trying to figure out what was wronf


---
**ThantiK** *April 10, 2016 03:36*

From what I can tell, looking for other pictures on the 'net - it shouldn't divert like that.  It should be rather evenly spaced around that ring.  Waiting for others to chime in, I'm still learning too.


---
**3D Laser** *April 10, 2016 03:43*

Yea I know it shouldn't be I don't know what is causing it 


---
**3D Laser** *April 10, 2016 04:08*

I'm thinking the tube isn't grounded out my water is charged making it do that but I don't know 


---
**Phillip Conroy** *April 10, 2016 04:29*

is the  machine earthed?.how many wire is on machines power plug


---
**3D Laser** *April 10, 2016 13:22*

My water has a charge causing it to arc


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/fkf1JC3H5Wu) &mdash; content and formatting may not be reliable*
