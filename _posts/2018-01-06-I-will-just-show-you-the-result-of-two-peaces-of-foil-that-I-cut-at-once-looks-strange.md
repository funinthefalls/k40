---
layout: post
title: "I will just show you the result of two peaces of foil that I cut at once, looks strange"
date: January 06, 2018 11:45
category: "Object produced with laser"
author: "BEN 3D"
---
I will just show you the result of two peace’s of foil that I cut at once, looks strange.

There are small hairy strings that’s let grate located between the fingers.

![images/77efe0532ab0d24faf09b75d56a4c118.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/77efe0532ab0d24faf09b75d56a4c118.jpeg)



**"BEN 3D"**

---
---
**Ned Hill** *January 06, 2018 16:13*

Weird, what exactly are you cutting?


---
**Martin Dillon** *January 07, 2018 05:19*

Looks like the air assist is blowing strands of plastic all over.  Try a small test cut with no air assist.


---
**Laurent Humbertclaude** *January 07, 2018 19:25*

Your are cutting some polystyrene or polycarbonate! This is for sure something not pmma.


---
**BEN 3D** *January 07, 2018 21:55*

Sorry my mother bought me this foil, so I did not know more about as already sayed. 

I have accidentally cut two slides, if I cut only one slide, everything is normal.


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/3npXJEw6ZT5) &mdash; content and formatting may not be reliable*
