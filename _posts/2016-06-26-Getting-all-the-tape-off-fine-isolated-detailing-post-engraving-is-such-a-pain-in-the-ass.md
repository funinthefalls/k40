---
layout: post
title: "Getting all the tape off fine isolated detailing post engraving is such a pain in the ass"
date: June 26, 2016 16:28
category: "Discussion"
author: "Ned Hill"
---
Getting all the tape off fine isolated detailing post engraving is such a pain in the ass.  Found that a sharp pair of tweezers with a head mounted magnifier works fairly well.  Also found that carefully dragging the sharp edge of a scalpel backward over the detailing can be a fairly effective way to scrape the tape off as well.  Any other tips or tricks out there?



![images/98dd38ce2bada12f5a460a712be07f00.png](https://gitlab.com/funinthefalls/k40/raw/master/images/98dd38ce2bada12f5a460a712be07f00.png)
![images/7fafacba2cb135e31478cca5d5a67522.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7fafacba2cb135e31478cca5d5a67522.png)

**"Ned Hill"**

---
---
**Trực Chính** *June 26, 2016 16:37*

99% alcohol could help. 


---
**Stephane Buisson** *June 26, 2016 17:07*

tape on tape and pull ???


---
**Ned Hill** *June 26, 2016 17:15*

**+Stephane Buisson** Tried that but it didn't work to well.  It may be because of the fact that this is plywood and the burnt plywood glue residue tends to glue the edges of the tape down a bit.  Tape on tape may work better with none plywood.  Will definitely try that when my order of solid thin wood arrives.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 17:23*

You could try using a soft scourer sponge/scrubbing pad & scrub it off. Your results look really nice. I tend to just use the tip of a stanley knife blade to lift the tape off or a sewing pin/needle for tiny areas.


---
**Ned Hill** *June 26, 2016 17:32*

Yeah I am using a good pair of needle sharp tweezers and they work reasonable well for the tiny areas.  Not sure how well a scrubbing pad would though, would be worried about breaking some of the thinner details and it also may cause some softening of the engraving edges.


---
**Jim Hatch** *June 26, 2016 18:57*

For those intricate ones I skip the tape and just sand it. I have to do the underside anyway to get rid of any underside residue so I just run a couple of passes of the orbital sander over the tops.



I use tape on other ones where I don't have as much detail or cleanup. No sense in adding an hour to a project just to get the tape off :-)


---
**Ned Hill** *June 26, 2016 20:43*

**+Jim Hatch** what grit sandpaper are you using?  220?


---
**Ashley M. Kirchner [Norym]** *June 26, 2016 21:01*

I use two different types of tape, a low adhesion tape on the piece which I engrave through, then a high adhesion one to lift. Works great. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 22:27*

**+Ashley M. Kirchner** That's an interesting technique. Sounds like a good idea & I'll have to test it.


---
**Jim Hatch** *June 26, 2016 22:33*

**+Ned Hill**​ 220 grit in a random orbit sander.



**+Ashley M. Kirchner**​ What's the high adhesion tape you use?


---
**Alex Krause** *June 27, 2016 01:41*

NICE square and compas :) I appreciate this since I'm a Mason 


---
**Ned Hill** *June 27, 2016 01:43*

**+Alex Krause** Thanks!  What lodge do you travel from?  I'm a member and PM of Wilmington 319 in Wilm. NC.


---
**Alex Krause** *June 27, 2016 01:50*

McPherson Kansas 172 AF&AM


---
**Alex Krause** *June 27, 2016 01:50*

**+Ned Hill**​ [https://plus.google.com/109807573626040317972/posts/73Ro6PH26aL](https://plus.google.com/109807573626040317972/posts/73Ro6PH26aL) done on my cnc


---
**Ned Hill** *June 27, 2016 01:53*

**+Alex Krause** Nice :)


---
**Jim Hatch** *June 27, 2016 01:55*

Valley Lodge #36, Simsbury CT; also 32^0 Scottish Rite


---
**Ashley M. Kirchner [Norym]** *June 27, 2016 17:18*

**+Jim Hatch**, uh, either duct tape or gaffer's tape. Really, anything would work as long at the two tapes are different adhesion types. The one I put down to engrave on is an RTape Conform series tape. Very low tack, engraves easy, and very easy to lift with another tape that's stronger. I've also tried blue painter's tape, the very thin one designed for fine stuff, and that works too. The regular painter's tape has a higher adhesion and doesn't come off as easy with this method.


---
**Jim Hatch** *June 27, 2016 19:04*

**+Ashley M. Kirchner**​ Thanks. I'll play with some different tape combos. I use blue tape now but wasn't aware there was a lower adhesion variety. I have seen a green painters tape I'll try.



For the high adhesion version I'll give it a go with various duct tapes (gaffer's tape us ridiculously expensive). 


---
**Ashley M. Kirchner [Norym]** *June 27, 2016 19:15*

If you're looking at the 3M ScotchBlue brand, then you're looking for the orange one. The green is in between the blue and orange, blue being the higher adhesion and textured, and orange being the low tack one.


---
**Ashley M. Kirchner [Norym]** *June 27, 2016 19:21*

I think the one I have, while blue colored, is simply labeled 'for delicate surfaces'. It's smooth, unlike the regular one which is textured. Feels very much like thin paper.


---
**Ben Marshall** *June 27, 2016 19:37*

I use a small steel brush to get the small taped areas 


---
**Travel Rocket** *July 01, 2016 00:18*

What setting did you use for engraving? (amperes and speed)


---
**Ned Hill** *July 01, 2016 00:23*

My machine only has a % power setting and I haven't yet taken the time to determine the correlation to mA, but my tube won't start lasing until 10% so judge from there.  This is 1/4" birch ply and I engraved with 25% power at 200 mm/s.


---
**Travel Rocket** *July 04, 2016 13:00*

Thank you so much, mine's engraving is very very slow even at 200ms speed, any tips? It burns the area more when there are more details that are close to each other


---
**Ned Hill** *July 04, 2016 13:03*

Sounds like you may have a misalignment or your piece isn't at the right focal height.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/YnUevqCK2C6) &mdash; content and formatting may not be reliable*
