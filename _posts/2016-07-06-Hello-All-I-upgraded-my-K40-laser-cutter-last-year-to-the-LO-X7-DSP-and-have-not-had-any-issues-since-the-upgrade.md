---
layout: post
title: "Hello All, I upgraded my K40 laser cutter last year to the LO-X7 DSP and have not had any issues since the upgrade"
date: July 06, 2016 02:43
category: "Discussion"
author: "Steve Moraski"
---
Hello All,



I upgraded my K40 laser cutter last year to the LO-X7 DSP and have not had any issues since the upgrade. The other day however I ran into an issue..... I was running a cutting job and the x-axis stopped moving. I power cycled the DSP, double checked parameters but found no issues. On a fresh power cycle I am able to jog the axis with no issues. After trying to run a file it stops working and then I am unable to move the axis even trying to jog. Again if I turn off power and try again all works until I try to run a file. Cut or engrave it produces the same result. I have not changed any setting so I am confident that is not the issue. I have also tried swapping the x/ y drivers and I see the same result on the x-axis with the other driver connected. Could I have a bad DSP card only after a few months use? Please help!





**"Steve Moraski"**

---
---
**Alex Krause** *July 06, 2016 02:46*

Does your drivers/DSP have thermal shutdown protection... meaning it shuts it self down when it senses to much heat on the driver


---
**Steve Moraski** *July 06, 2016 02:55*

It is possible, usually if they do there would be an error code flashed on the status LED correct?  The drivers are the ones that came in the lightobject upgrade package.


---
**Alex Krause** *July 06, 2016 02:55*

Have you contacted LO customer support?


---
**Scott Marshall** *July 06, 2016 03:08*

Put a high impedance meter (digitalmeter will work, but a scope is better if you have one) on the enable, direction and step lines (usually marked 'EN', 'DIR' and 'Step'  lines between your motor controller and Driver module to see which (if any) signal is dropping out. That will lead you to the issue.



If you have to use a meter, set it on DC for checking the Enable and direction lines, AC for step  Look for 5v on Enable (usually positive logic there), and reversing 0 & 5V signals on the direction.

Step is a rapid succession of pulses, a square wave while the axis in in motion so will show a AC voltage between 0 and 5V, usually about 2.5V.



From there you can check that part of the circuit. If you have all signals, it is probably the motor driver It may be thermalling as Alex suggested, but some controllers shut down if they sense a missed step (either for real or in error) 



From there you can move on to the stepper driver output. It's as simple as checking each phase winding (there are 2, one on each output bridge) and see if one is dropping out.



Good luck, Scott


---
**Steve Moraski** *July 06, 2016 13:53*

Alex- I haven't directly contacted LO support, I just asked the same question on their forum.  I will send a request to support when I get a chance.



Scott-  I have a small DSO scope I will check as you suggested when I get home tonight and post back.



Thank you both for the help!


---
**Scott Thorne** *July 07, 2016 09:45*

Check your end stop sensors 


---
*Imported from [Google+](https://plus.google.com/116409795527711620932/posts/MLghCoAaTet) &mdash; content and formatting may not be reliable*
