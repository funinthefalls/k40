---
layout: post
title: "So using CorelDraw X5 + CorelLaser and the stock controller seems to be working well for me, I was wondering, what advantages would swapping the board and software offer?"
date: July 05, 2016 18:43
category: "Modification"
author: "Tev Kaber"
---
So using CorelDraw X5 + CorelLaser and the stock controller seems to be working well for me, I was wondering, what advantages would swapping the board and software offer?





**"Tev Kaber"**

---
---
**Evan Fosmark** *July 05, 2016 18:57*

The two reasons I've been thinking about upgrading is so I can get an auto-focusing bed, and for the ability to do raster engraving.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 05, 2016 23:20*

Variable power output in the same job is the reason I decided to switch the board/software (although I haven't finished the process yet).


---
**Tony Sobczak** *July 06, 2016 21:18*

Are coredraw files Importable into laser Web or visicut?   I have too many files to loose or redo if not. I have everything I need now to do a straight conversion. Like to get **+Scott Marshall**​'s interface to use both.


---
**Scott Marshall** *July 06, 2016 21:21*

It won't be long Tony!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 07, 2016 04:26*

**+Tony Sobczak**  I'd imagine that you could export your Corel Draw (CDR) files as a SVG file, which is definitely importable into LaserWeb.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/MaEVXJJ6Mwa) &mdash; content and formatting may not be reliable*
