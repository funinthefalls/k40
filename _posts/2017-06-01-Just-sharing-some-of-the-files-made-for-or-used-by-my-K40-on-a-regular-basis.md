---
layout: post
title: "Just sharing some of the files made for or used by my K40 on a regular basis"
date: June 01, 2017 15:19
category: "Repository and designs"
author: "Joe Alexander"
---
Just sharing some of the files made for or used by my K40 on a regular basis. Includes a copy of the floating wombat alignment guide and tube mounts from thingiverse (these were not made by me just used) **+Ray Kholodovsky** collecting it together, and working on a write-up(LW4 for dummies?) for all those repetitive questions/initial setup. Helps that im process oriented and a bit OCD about protocol :)





**"Joe Alexander"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 01, 2017 15:23*

Woohoo. 


---
**Ned Hill** *June 01, 2017 15:36*

Awesome, thanks guys!


---
**Joe Alexander** *June 01, 2017 17:44*

added a spreadsheet with some of the FAQ's and initial LaserWeb4 setup values. let me know what you think and feel free to add to it fellas.


---
**Paul de Groot** *June 01, 2017 22:15*

Always amazed about the talent here on this forum.  Awesome 😀


---
*Imported from [Google+](https://plus.google.com/100077461927913318405/posts/SmgMf9EQhCN) &mdash; content and formatting may not be reliable*
