---
layout: post
title: "What type of glue is good for acrylic that won't fog it like crazy glue?"
date: November 28, 2017 02:28
category: "Materials and settings"
author: "Anthony Bolgar"
---
What type of glue is good for acrylic that won't fog it like crazy glue? Also, what are people using to get rid of the black char marks on wood (other than sanding)?





**"Anthony Bolgar"**

---
---
**Ned Hill** *November 28, 2017 02:39*

I almost always mask with painters tape or lettering transfer tape so I don't get the charring.  I've seen people post of using something like GOJO hand cleaner.


---
**Ned Hill** *November 28, 2017 02:43*

For acrylic I use Weld-On 3.


---
**Anthony Bolgar** *November 28, 2017 05:10*

The edge charring is what I was meaning, not on the surface.


---
**Ned Hill** *November 28, 2017 05:19*

Ahh, sanding is the only thing I know to deal with that then.  I also will seal the edges after some sanding if I don't take it down to the bear wood.  Keeps the residual char and smell sealed in.




---
**Justin Mitchell** *November 28, 2017 11:05*

There are specialist acrylic glues like Tensol 12 that set clear


---
**Lars Andersson** *November 28, 2017 11:20*

Acrifix 1S 0116 is another good glue for acrylic. 


---
**Joe Alexander** *November 28, 2017 17:41*

birch plywood cleans up pretty well if you use some mild soap and water with a brush on the edges. it also removes most of the smell. Then all you have to do is let it dry then seal it.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/89LKMeeM63j) &mdash; content and formatting may not be reliable*
