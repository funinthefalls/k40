---
layout: post
title: "Every once in a while I see a really cool 3D Christmas card or scene made with a laser, but can never find templates or patterns"
date: November 02, 2017 01:47
category: "Object produced with laser"
author: "Bob Damato"
---
Every once in a while I see a really cool 3D Christmas card or scene made with a laser, but can never find templates or patterns. If i were at all creative id make my own but.....  :) 



Something like this.... Any thoughts? 





[http://www.hummingbirdcards.co.uk/blog/wp-content/uploads/2015/11/Winetr-Wonderland-Christmas-card-manilla-1-MAIN-PHOTO-1024x1024.jpg](http://www.hummingbirdcards.co.uk/blog/wp-content/uploads/2015/11/Winetr-Wonderland-Christmas-card-manilla-1-MAIN-PHOTO-1024x1024.jpg)





**"Bob Damato"**

---
---
**Arjen Jonkhart** *November 06, 2017 03:45*

Here you go: [drive.google.com - Deer_In_Woods.svg](https://drive.google.com/open?id=1OdQGcY2ZUk-0wlAt9YrS71BNIkMR4hUd)




---
**Bob Damato** *November 07, 2017 21:28*

Wow, well thank  you!




---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/KGS9Cy8br9j) &mdash; content and formatting may not be reliable*
