---
layout: post
title: "Custom 60w laser in progress. Half the frame height built"
date: November 27, 2017 22:05
category: "Discussion"
author: "Bill Keeter"
---
Custom 60w laser in progress. Half the frame height built. The outer dimensions are 1360mm X 800mm. Would have finished the final top level of the frame but I ordered half the screws in the wrong size. doh.



**+Don Kleinschnitz** in a previous post of mine you mentioned a fuse on the power supplies. Any suggestions you can give me? I'm debating if I should just hide a regular outlet surge protector in the machine. Or if I should build some kind of fuse box (which is outside my experience)



For the drive train, I went with Nema23 and external drivers with a C3D motherboard.



Now i'm hunting for where I want to buy mirrors (25mm) and lens (20mm). Plus some good mounts.



Then i'll have to find a true 60w tube and power supply. I had one on my watch list on ebay but I think it sold.





![images/76d77bd214feb9de0079fc766b48afa3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/76d77bd214feb9de0079fc766b48afa3.jpeg)
![images/ed2da11ea88a3b9361b0448bbb800117.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ed2da11ea88a3b9361b0448bbb800117.jpeg)
![images/7aa4a4dbd23bb0e30d75dc8e2a9b7667.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7aa4a4dbd23bb0e30d75dc8e2a9b7667.jpeg)

**"Bill Keeter"**

---
---
**Ariel Yahni (UniKpty)** *November 27, 2017 22:54*

Looking good there


---
**Steve Clark** *November 28, 2017 00:12*

Here is a pic of what I did for my k40. Some of the stuff in mine will not apply to yours but it gives you some idea's of packaging your PS and so forth. The steel case came from Harbor freight. It's a key lock box.



[harborfreight.com - 100 Hook Key Box](https://www.harborfreight.com/100-hook-key-box-95318.html)



The power switch, gang terminals, fuses and other hardware came from McMaster Carr. 



I moved all but the laser PS and the C3D from the top down to the modified key box. I was having interference problems from the "other electronics" wires running all over the place. I got straighten out (in more ways than one) by people here on the site.



Plan for the other high (120 or 240v ac) and low voltage (12 and 24v dc) power sources and fuses you will be needing for like lighting, air pump, vent fan, water pump, little laser cross hair ect ...ect...



I'm not a electronics guy, but this project will have you doing some studying at times. It did me! Read through "Don's Things" and it was very helpful in many ways including safety awareness... so have fun and enjoy it. 



If you decide to play with Peltier Cooling let me know I've got that figured out (on a non-professional level). 








---
**Anthony Bolgar** *November 28, 2017 02:02*

I love my 60W laser, you should be very happy with yours. My next laser is going to be 100W. (I just need to rationalize why I need a 5th laser cutter :)


---
**Anthony Bolgar** *November 28, 2017 02:26*

**+Steve Clark** could you post the pic of your box? The only image you posted was the harbor freight  image.


---
**Don Kleinschnitz Jr.** *November 28, 2017 02:54*

**+Bill Keeter**​​​ I fuse the output of all DC supplies, a surge protector won't do that.

I like to use car blade fuses in a box like attached.



I mounted my DC supplies all on a separate modular frame including the fuse block so it's easy to service.



[https://photos.app.goo.gl/6sV4GMVUnt3G9g9w2](https://photos.app.goo.gl/6sV4GMVUnt3G9g9w2)



Also this may be helpful.



[http://donsthings.blogspot.com/2016/06/k40s-power-systems-design.html?m=1](http://donsthings.blogspot.com/2016/06/k40s-power-systems-design.html?m=1)



HELLA H84960091 6-Way Lateral Single Fuse Box [amazon.com - Amazon.com: HELLA H84960091 6-Way Lateral Single Fuse Box: Automotive](https://www.amazon.com/dp/B000VU9D1G/ref=cm_sw_r_cp_apa_.5mhAbMNSV7D7)


---
**Steve Clark** *November 28, 2017 04:30*

Opps...sorry. Here it is. Note the fuses. There is a fan in the door also.

![images/a1f96879c3a07a4106eb593fc59ba05e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a1f96879c3a07a4106eb593fc59ba05e.jpeg)


---
**Don Kleinschnitz Jr.** *November 28, 2017 16:09*

**+Steve Clark** nicely fabricated.


---
**Bill Keeter** *December 20, 2017 15:53*

**+Don Kleinschnitz** Thanks again for sharing your wiring for the power supplies. I'm using it as reference for building out my own system. What gauge wire did you use? I mean the 110v power coming from the outlet that i'll be running to a switch and then off to each PSU? 16 or 14 maybe? Also i'm assuming the output lines from the PSU's can go up to 20 or 22 gauge.


---
**Don Kleinschnitz Jr.** *December 20, 2017 16:30*

**+Bill Keeter** 



[https://www.engineeringtoolbox.com/wire-gauges-d_419.html](https://www.engineeringtoolbox.com/wire-gauges-d_419.html)





your cable size should match your power designs current capacity requirements and that depends on your specific design.



I use this practice:



Cabling design should include:

...Current capacity

...Over current protection

...Safety grounding

...Signal grounding



Its best to create a budget that starts at the endpoints and works its way back to the power cable. The budget includes current capacity and length of cable.



This budget will tell you what size cable you can use at what place in the design. It will also tell you how to size the DC fuses and eventually the AC input protection.



My general rule on power supplies it to use the largest practical size I can that is larger than the current draw dictates. This is because the inductance of  power supply cables matters.  



Wiring design should include safety and signal grounding. 



In general:

AC: 

...Fused or breaker

...Wire with current carrying capacity 10-20% bigger than the total of  downstream devices draw.

...Carefully considered safety grounding to single points with gas tight mechanical connections to frame.



DC supplies:

...Each output fused

...Shortest wire possible to sink..

...Largest practical diameter 

...Single point gas tight grounding to frame.






---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/LF6UQNuawrc) &mdash; content and formatting may not be reliable*
