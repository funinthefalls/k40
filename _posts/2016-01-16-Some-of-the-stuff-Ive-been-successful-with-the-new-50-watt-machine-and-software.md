---
layout: post
title: "Some of the stuff I've been successful with the new 50 watt machine and software"
date: January 16, 2016 13:26
category: "Object produced with laser"
author: "Scott Thorne"
---
Some of the stuff I've been successful with the new 50 watt machine and software.



![images/6dd40043296dfbadf53522b1082b8992.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6dd40043296dfbadf53522b1082b8992.jpeg)
![images/37e33e74df2f6a19fde9ee2b7eee04df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/37e33e74df2f6a19fde9ee2b7eee04df.jpeg)

**"Scott Thorne"**

---
---
**Anthony Bolgar** *January 16, 2016 15:24*

Nice work.


---
**Scott Thorne** *January 16, 2016 15:28*

**+Anthony Bolgar**​....thanks brother...hope all is going well with you and your family.


---
**Anthony Bolgar** *January 16, 2016 15:34*

The family is doing great, on the other hand I'm just surviving, it takes just over 10,000 pills a year just to stay somewhat healthy and functional, some days I can't do much, even with the 500mg of morphine a day I am on.


---
**ED Carty** *January 16, 2016 16:10*

Very Nice Work


---
**Scott Thorne** *January 16, 2016 16:38*

That's a lot to take in brother, you and your family are in our daily prayers **+Anthony Bolgar**, hope I'm not being too personal but what are all the pain killers for?


---
**Anthony Bolgar** *January 16, 2016 16:49*

Chronic pain as the result of 2 head on car crashes spaced less than 6 months apart(I am a good driver, I was the passenger in both wrecks)


---
**Scott Thorne** *January 16, 2016 16:57*

**+Anthony Bolgar**...that's sad to hear man....I hope your pain subsides soon my friend....I couldn't imagine being in your shoes bro...you are in our prayers man.


---
**Scott Thorne** *January 16, 2016 17:12*

**+ED Carty**....thanks man....hard to prep some of these for the final engrave.


---
**ED Carty** *January 16, 2016 17:45*

You did great. They are very beautiful. 


---
**ED Carty** *January 16, 2016 17:46*

What type of material is that ? wood i assume.




---
**Scott Thorne** *January 16, 2016 18:29*

It's Baltic birch plywood.


---
**ED Carty** *January 17, 2016 00:47*

Very nice choice. The coloring is a great match. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/KSykMGotxK3) &mdash; content and formatting may not be reliable*
