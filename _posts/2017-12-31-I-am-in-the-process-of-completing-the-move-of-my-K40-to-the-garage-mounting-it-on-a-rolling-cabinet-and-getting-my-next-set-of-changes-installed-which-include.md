---
layout: post
title: "I am in the process of completing the move of my K40 to the garage, mounting it on a rolling cabinet and getting my next set of changes installed which include ..."
date: December 31, 2017 14:31
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
I am in the process of completing the move of my K40 to the garage, mounting it on a rolling cabinet and getting my next set of changes installed which include

... mounting all my gauges, 

... new LCD control panel enclosure

... new temp sensors on laser and in the compartment.

... cover on the controller

... location for my laptop



Anyway here are some questions for the community before I start making more changes; 



1.) Problem: the unit has been siting all summer idle. The silicon tubing has white flaky stuff on the inside that is breaking free and floating in the coolant. I thought the tubes were white until I looked close.

a.)Has anyone else seen this?

b.)Do you know what it is?

c.)Do you know how to get it broken loose and flushed out?



2.) Problem: I have always had the problem that air bubbles form in the tube. I have been lifting the left side of the machine in the air and rotating it until the bubbles escape. I now have the machine tied to its resting surface. Also doing this with each exchange of water is annoying.

I have not wanted to rotate the tube because I do not want to mess up its alignment and I do not want to have problems with arcing, both are results others have reported.

a.) Have you rotated your tube and did it prevent bubbles.

b.) Has rotating the tube created arcing problems?

b.) Have you solved the bubble problem in a way other than rotating the tube or tipping the machine?

 

Will a higher volume/velocity pump remove or keep bubbles from forming? I have been thinking about going to a 12VDC closed cooling system anyway.

c.) Has anyone tried this.....







**"Don Kleinschnitz Jr."**

---
---
**Adrian Godwin** *December 31, 2017 15:23*

If new bubbles are forming (not just ones left over from changing the water) you probably have a leak. It might be only large enough for the pressure on the pump inlet to pull air in, or perhaps the pump is too close to the surface of the reservoir. A more powerful pump will make it worse.



I have a closed aquatics tank/filter unit (without the filters). It takes a while to get all the trapped air out but I have a vent above the level of the laser that catches a lot of it. This is essentially a header tank rather than a reservoir, but it still can't drive the air bubbles out of the laser tube. i squeeze the tube and tilt the cutter to get them out.



I will eventually rotate the laser but, like you, I don't want to muck up the alignment unnecessarily. I have done this on a bigger laser and it did help but there was more space around the tube and less chance of provoking an arc.


---
**Jim Hatch** *December 31, 2017 16:08*

Air bubbles may only be the result of air entrained in the water collecting and forming the bubble. Even in closed systems until all of that air is removed over a lot of time spent de-bubbling over & over again, you'll get congregated air bubbles forming. Water gets air in it when you pour it in an open flow so when filling your coolant bucket, you need to let it drain from a valve or spigot in your fill bucket through a hose that has no air in it (you'll need to let the water run and close off the end as soon as the tube is filled with water and then the tube end placed under the surface of your coolant bucket's water so more air doesn't get added. The return line from the laser tube also has to go under the surface of the water or it will get aerated as it is returned to the coolant bucket.


---
**HP Persson** *December 31, 2017 19:22*

1

A: Yes

B: Plasticizer from the tubing (spelling...) Reacts with the coolant.

C: Just push water trough, 50-50 vinegar and distilled worked for me.

It can be some kind of salt or calcium deposit if you used tap water, but you probably didn't :)

I switched to 6mm silicone hoses from the auto store, all problems stopped. Or go with medical grade hoses. (not food grade, they leach).

Visit a computer water cooling store for good proper hoses that won´t leach on you, or degrade with time :)



2

C: Yes, with "Water Wetter" from Redline, can be found in the auto parts store. It removes the surface tension completely and i have not seen a bubble since. Keeps the conductivity very low and acts as a lubrication and corrosive protection at the same time, and a small amount of anti algae stuff in it.

Keeping the return hose below the water level is also important bubble-killer :)



I have tested water flow up to 1000L/H, only thing happening is that i blew off the hoses from the tube.

Zip-tied them back, and after a couple of minutes i blew a glass elbow-connector :P

I keep my flow about 5-600L/H now without problems.

More flow/pressure wont help the bubble situation though And you can only gain heat transfer up to about 15-20L/min, after that it dies off. So no need to have higher flow than that.


---
**Don Kleinschnitz Jr.** *December 31, 2017 21:34*

**+HP Persson** Thanks I will try the vinegar for cleaning and some Water Wetter. What amount of wetter should I add to a 5 gal bucket and do you add it with each water change?




---
**HP Persson** *December 31, 2017 21:45*

**+Don Kleinschnitz** A little more than half bottle in a 20L bucket for me. I hook up the pump and circulate the water while adding water wetter and stop when the water stops to "bend" up on the inner wall of the coolant tank, that tells me the surface tension is gone.

Added every water change, i do that aprox. once every six months.

Keeping a lid on it, good anti algae additive and out of sunlight, return hose below surface, no fingers in the water and so on will keep the water good for months, depending on usage :)



Same thing can be achieved with dish soap, but it contains so much other stuff, like perfumes and junk i dont want in my coolant loop. So i spend a few bucks on Water Wetter.

I use the same in my watercooled PC´s :)



Pic of one of my tanks, it´s completely sealed except for a small hole with a coffee filter to keep air borne dust out. The connectors has a G1/4 thread inside and outside the tank, so i can add compression fittings for the hoses.

![images/4849385bdfe6f10cc9f432a1919ed5df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4849385bdfe6f10cc9f432a1919ed5df.jpeg)


---
**Steven Whitecoff** *January 03, 2018 14:14*

So you dont get tube arcing using Water Wetter? I mixed according to the directions and got arcing immediately. So I removed it(and saved it)and went back to straight water.




---
**Don Kleinschnitz Jr.** *January 03, 2018 17:01*

**+HP Persson**  Very strange results, nothing like yours. What different?



I tried adding "Water Wetter" @ 1oz per 3 gallons. It raised the water conductivity from 4.0us to 410us making the coolant unacceptably conductive. The bad part is that it did not remove the bubbles much at all.



Water Wetter measured without dilution is very conductive and read infinity on my meter. Even after I rinsed a bottle that had the wetter in it and then added distilled water it raised the conductivity from 4.0us to 40us. Took a few rinses to get it back to normal.



The bottle (see below) suggests 1oz per gallon so I was using only 1/3 the amount recommended for car cooling systems.



BTW: the vinegar worked great at cleaning the white stuff from the lines.

![images/07224f5b82fbc4066fcbed752eec9f94.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07224f5b82fbc4066fcbed752eec9f94.jpeg)


---
**HP Persson** *January 03, 2018 17:08*

**+Don Kleinschnitz** Wow, 400? Have to check what my bottle measures at unmixed. Do not have the machine working right now, but last time i tested it there was nothing above 100, or even 50μS in my 25L tank. 

4μS is normal for distilled water, but 400, you are almost at RV antifreeze levels :)



Should not be any difference on their product between US and EU, the bottle is identical though.



I´ll do some tests on mine and check conductivity and post my data.


---
**Don Kleinschnitz Jr.** *January 03, 2018 17:12*

**+HP Persson** Yah it acts like anit-freeze and does nothing with the bubbles :).


---
**HP Persson** *January 04, 2018 10:36*

**+Don Kleinschnitz** I found the bottle i used before, maybe 3-4 years old. Gives me 66uS if i dunk the meter in the water wetter without any dilution.

Going to the auto part store today to buy other stuff, i´ll grab a new bottle of water wetter and check a fresh one, maybe they changed something?



I also tested my file-server, which has the same coolant it has been since 2015, distilled + water wetter and that one is at 121uS.



Plain distilled gives me 4uS, distilled + a few drops of dish soap gives me 45uS so it´s not measuring wrong either.

I´ll be back :)


---
**Don Kleinschnitz Jr.** *January 04, 2018 11:56*

**+HP Persson** that stuff is diabolical once you get it on something it lowers the conductivity of anything you put in it. I emptied and rinsed my bucket, tested distilled water to be 4us and then when I put it in the bucket it raised it to 10us :(.

With all this neither wetter or detergent is removing bubbles :(.


---
**HP Persson** *January 04, 2018 11:58*

**+Don Kleinschnitz** How big bubbles do you have? i can see the tiny tiny ones in my mix when i run it by the pump, but the big ones, 3-5mm+ is gone.

Check on the side of the tank too, when it stops curling from the water surface the tension is gone and it should not in a million years be able to create a bubble.



Do a test in a small glass first, do try with another glass with dish soap and water aswell to compare with.


---
**Don Kleinschnitz Jr.** *January 04, 2018 12:09*

See bubbles in the video below.

Perhaps I do not comprehend what you mean by "curling" , what exactly am I testing for in the dish?



![images/32fd5d7538513cc503aef2f2ca4d2509](https://gitlab.com/funinthefalls/k40/raw/master/images/32fd5d7538513cc503aef2f2ca4d2509)


---
**HP Persson** *January 04, 2018 14:14*

**+Don Kleinschnitz** Oh wow, that one should be obliterated right away when the surface tension disappears.

Curling, or bending - inside a glass of water the surface tension makes the water creep up a bit on the edge around the wall, when the surface tension lowers this effect stops - see pic of a glass of regular distilled water, where the edge "curling/bending" can be seen.

In my tank, the water is 100% flat and does not bend like this.



I´m doing some more tests, just got back from the store with a new bottle of water wetter to test with.

But, check out the term "surfactant" on wikipedia or google, there might be some other kind of surfactant you can use that is easily available in the US.



I´ll be back with more findings :)

![images/9f097fa5720a4a55844e7e2580599bc9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9f097fa5720a4a55844e7e2580599bc9.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/fLy3kikiVxu) &mdash; content and formatting may not be reliable*
