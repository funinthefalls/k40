---
layout: post
title: "Can someone point me in the direction to figure this out"
date: August 22, 2017 01:39
category: "Software"
author: "Fat Dad Custom Designs"
---
Can someone point me in the direction to figure this out. I am using whisperer and I imported a design that was created in Corel into Inkscape. The design is black and I changed some elements by adding red and blue to rhe design. When importing to whisperer there is no red or blue. Any thoughts?





**"Fat Dad Custom Designs"**

---
---
**Printin Addiction** *August 22, 2017 02:03*

You have to make sure it's pure red and pure blue (i.e. 255,0,0 & 0,0,255) . Also make sure nothing is selected when saving.


---
**Josh Oxborrow** *August 22, 2017 02:42*

Make sure it is a vector file and using vector art work as well


---
**Scorch Works** *August 22, 2017 12:40*

Another thing to be aware if is that fill colors do not result in vector cut/engrave.  Vectors are only the result of the set stroke color.  A common misconception is that text is strokes but in fact most text is defined by a filled shape.  In addition normal text is a special kind of shape that needs to be converted to paths before it will be recognized as vectors.



The steps to make vector outline text:

1. Make the text

2. Set the stroke color to red/blue

3. Select the text and choose "Path"-"Object to Path" in the Inkscape menus.



To make single stroke (non-outline text):

1. In Inkscape go to "Extensions"-"Render"-"Hershey Text..."

2. Use the dialog that opens to create the text.

3. Change the stroke color to blue/red.




---
**Scorch Works** *August 22, 2017 12:40*

**+Printin Addiction** 

There is a little wiggle room in the red and blue colors.  The colors can be off by 10 from perfect red or blue.  For example (245,10,10) will still work for red.


---
**Printin Addiction** *August 22, 2017 15:25*

**+Scorch Works** Ah Ok, I must have been off by more than that....



The software works great, the only gotcha I have so far is that if you send another job before the head is back to original position (and stopped), it will lose it's connection and only receive timeouts from that point on. That just means I need to pay attention and wait until the ride comes to a complete stop :)



Again, awesome job on the software, I kinda feel like I cheated by starting with it versus using the stuff that came with the printer.


---
**Scorch Works** *August 22, 2017 16:43*

**+Printin Addiction** at least it is returning to the start position faster now. (I think that was a version .05 fix)  However, I have not figured out a good way to determine when the lazer is finished yet.


---
**Printin Addiction** *August 22, 2017 17:39*

Can you query the position of the head, or will any call kill the communication?



It is really very minor point, the software is great... any ETA on .05?



What??? your up to .06?? 



Wow, I need to keep up :) 


---
**Scorch Works** *August 22, 2017 17:53*

**+Printin Addiction** the machine only replies with a very limited response I am not sure what any of it means but I am pretty sure it never gives the position.  



The versions are moving quick because I am still working some of the bugs out.


---
**Printin Addiction** *August 22, 2017 18:26*

With the quicker return, this becomes a non-issue. 



What mechanism should we use to  submit bugs / requests?


---
**Scorch Works** *August 22, 2017 18:40*

Usually e-mail but I accept questions/requests by pretty much any means of communication.


---
**Fat Dad Custom Designs** *August 22, 2017 20:48*

**+Josh Oxborrow**  yep, made it in CD so I knew that was good to go. I'll change the stroke now and see how it goes.


---
**Josh Oxborrow** *August 22, 2017 20:48*

can you attach a copy? 


---
*Imported from [Google+](https://plus.google.com/111106297716917992918/posts/PR4kLJ64UdN) &mdash; content and formatting may not be reliable*
