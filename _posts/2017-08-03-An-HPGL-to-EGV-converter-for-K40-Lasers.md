---
layout: post
title: "An HPGL to EGV converter for K40 Lasers"
date: August 03, 2017 19:44
category: "Software"
author: "Scorch Works"
---
An HPGL to EGV converter for K40 Lasers.  It creates an EGV file that LaserDRW can send directly to the K40 Laser (M2 Nano). 



(This is not mine. I just happened to see it. It is called PLT2EGV and costs $90 on E-Bay)




{% include youtubePlayer.html id="GC4QuEbWKI0" %}
[https://youtu.be/GC4QuEbWKI0](https://youtu.be/GC4QuEbWKI0)







**"Scorch Works"**

---


---
*Imported from [Google+](https://plus.google.com/+ScorchWorks/posts/7qWpboK2Ebe) &mdash; content and formatting may not be reliable*
