---
layout: post
title: "The first laser cutter I worked on (not a K40) came from that factory in China as we've come to expect: (a) a heaping handful of metal filings tossed around inside, (b) mis-mated connectors, secured in place with hot-melt"
date: July 30, 2017 23:31
category: "Original software and hardware issues"
author: "Nate Caine"
---
The first laser cutter I worked on (not a K40) came from that factory in China as we've come to expect: 



(a) a heaping handful of metal filings tossed around inside,

(b) mis-mated connectors, secured in place with hot-melt glue,

(c) mis-matched power supply that killed the tube in 3 months,

(d) inline red laser pointer mirror that burned up on the second day,

(e) mis-wired machine shutdown loop.



The safety loop was supposed to be wired in <b>series</b> and would shut down the machine if <i>ANY</i> fault occurred (such as coolant pump failure, coolant over temperature, hood open interlock, etc.



As shiped from the factory it was wired, in <b>parallel</b>, thus, the only way the machine would shut down was if <i>ALL</i> faults occured simulaneously.  



<s>----------------------</s>

So over the weekend I was cleaning up some problems with my <i>(early production)</i> <b>K40</b>, and spotted a few things that amused me.



(a) the bracket that secures the x-belt to the carriage had a stripped screw, super-glued in place, that now was no longer glued at all, 

(b) just horrible job at milling the carriage assembly (see photo),

(c) insulated safety ground (the red binding post on the rear of the machine) is not bonded to the chassis,

(d) "insulation crimped" stepper motor connectors (see photo).

(e) bad bearing at left-rear belt tensioner.



I've included a close up of the <b>stepper motor crimps</b> for your viewing pleasure.



The back part of the crimp is supposed to grip only the insulation (for strain relief).

The center part of the crimp is supposed to pressure "weld" the connector to the wire strands.

<i>(In fact, connector manufacturers will tell you that a properly crimped connector is more reliable than a soldered one.)</i>



As implemented, wrapping the strands over the insulation and crimping that mess is not recommended.  As the soft insulation creeps, the connections becomes even poorer.  A more resistive connection, and some current thru it, heats up, which softens the insulation even further.  You get the picture...a cascade failure.



I'm in the process of rehabilitating all the stepper motor connectors, so won't know for a while if that makes a difference in some of the rough y-axis movements.  I suspect some of the y-axis inconsistencies is more likely due to the marginal bearing.

  

Elsewhere in the machine I've seen a lot of problems with the rest of the wiring.  High-Voltage power supply, Low-Voltage power supply, front panel, Moshi controller.  Several of the AC terminals at the power supplies were not tight at all.  The power connector at the Moshi controller was horribly crimped.  The ground signal terminal at the HVPS was "over-subscribed".  i.e. FIVE wires sharing one ground terminal.



Check your wiring!





 ﻿



![images/71b0a4d2b5e367d435ed0f7bdc14a855.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/71b0a4d2b5e367d435ed0f7bdc14a855.jpeg)
![images/821c8d581f1d6b7b3c9ae79d949bc125.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/821c8d581f1d6b7b3c9ae79d949bc125.jpeg)

**"Nate Caine"**

---


---
*Imported from [Google+](https://plus.google.com/107379565262839168986/posts/TN2zAPSqGEM) &mdash; content and formatting may not be reliable*
