---
layout: post
title: "I was thinking of upgrading my exhaust blower to this one: To power it I would use an ATX computer power supply (I have tons of them salvaged from old PC's)"
date: January 26, 2016 06:13
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
I was thinking of upgrading my exhaust blower to this one:



[http://www.ebay.ca/itm/Brand-New-4-In-Line-Marine-Bilge-Air-Blower-DC-12V-6Amp-270-CFM-Quiet-for-Boat-/121861740679?hash=item1c5f869887:g:zVMAAOSwEetV9xqs&vxp=mtr](http://www.ebay.ca/itm/Brand-New-4-In-Line-Marine-Bilge-Air-Blower-DC-12V-6Amp-270-CFM-Quiet-for-Boat-/121861740679?hash=item1c5f869887:g:zVMAAOSwEetV9xqs&vxp=mtr)



To power it I would use an ATX computer power supply (I have tons of them salvaged from old PC's). I would also use the ATX supply to power my air assist laser pointers, my air assist compressor (it is 12V), and the LED lighting I installed in the cutting bay. That way all my peripherals would be off loaded from my lasers PSU.





**"Anthony Bolgar"**

---
---
**Phillip Conroy** *January 26, 2016 06:35*

I have a 4inch 240 volt inline fan simler to your link and is not powerful enouch,i am thinking of [www.ebay.com.au/itm/8-Heavy-Duty-2900-RMP-200mm-Portable-Super-Speed-Extraction-Fan-Ventilator-Set-/111240702828?hash=item19e6766b6c:g:hPcAAOxywOtSXgJz](http://www.ebay.com.au/itm/8-Heavy-Duty-2900-RMP-200mm-Portable-Super-Speed-Extraction-Fan-Ventilator-Set-/111240702828?hash=item19e6766b6c:g:hPcAAOxywOtSXgJz)    and using it at the end of a 44 galon plastic drum and a 4 inch tube going to my laser cutter


---
**Phillip Conroy** *January 26, 2016 06:38*

Also thinking of using a vacum ,with a 1/2inch pipe going next to the laser head that moves with the head,that way it will not pull the fumes off mdf 3mm that i mainly cut ,over the work staining the top layer 


---
**Anthony Bolgar** *January 26, 2016 07:18*

I would have thought that 270 cfm would be plenty. The volume of the cutting bay is aproximately 2 cubic feet. That would be 135 complete air changes a minute,so at least 2 complete air changes a second.


---
**Phillip Conroy** *January 26, 2016 09:00*

You must take into account type of pipe used ,stock pipe is crap,and length off,i changed stock plastic pipe to a smoth wall aluminium exhust fan pipe,i just checked laser tube after cutting lots of mdf and could not see nto tube because of dust,,with my 4 inch fan after i cut mdf i have to wait 5 seconds for all the s oke to clear,what are u cutting


---
**Anthony Bolgar** *January 26, 2016 09:42*

Mostly acrylic


---
**I Laser** *January 26, 2016 11:20*

**+Phillip Conroy** that seems a pretty good find. Bit confused though because even with the crappy stock fan and a 90mm tornado PC fan I didn't have to wait for the smoke to clear (cutting 3mm MDF mostly).


---
**Phillip Conroy** *January 26, 2016 13:19*

I have laser cutter in bungalow and wife scrapbooks there she can not stand the smell of laser cut mdv


---
**Jason Johnson** *January 29, 2016 20:12*

Mine is very similar to that one. shown here: 
{% include youtubePlayer.html id="jnpnJ1FvcQU" %}
[https://www.youtube.com/watch?v=jnpnJ1FvcQU](https://www.youtube.com/watch?v=jnpnJ1FvcQU)


---
**Anthony Bolgar** *January 29, 2016 20:33*

Thanks for confirming my choice of blowes Jason.


---
**Phillip Conroy** *January 30, 2016 07:55*

 If blower is lowering the inside air pressure you need to add a duct from outside to laser cutter so that you are not pulling air out of the room ,will help the heater and air con when used


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/MGanT2FdK7h) &mdash; content and formatting may not be reliable*
