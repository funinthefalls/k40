---
layout: post
title: "Hi, I need to replace my top rail due to wear, anyone know what it's name is or where I can buy a length of it?"
date: August 16, 2017 10:31
category: "Original software and hardware issues"
author: "Andy Parker"
---
Hi, I need to replace my top rail due to wear, anyone know what it's name is or where I can buy a length of it?





**"Andy Parker"**

---
---
**Jim Fong** *August 16, 2017 15:24*

[http://www.lightobject.com](http://www.lightobject.com) Sells various replacement parts for lasers.  Not sure if they sell the rail but you can ask them.  Usually it's the white roller wheels that wear out and they do sell those. 




---
**Andy Parker** *August 17, 2017 08:48*

Still not having any joy with this, I can't believe it's so hard to source :(


---
**Andy Parker** *August 18, 2017 07:36*

You have any ideas +Paul de Groot ?


---
**Adrian Godwin** *August 23, 2017 21:47*

It's a fairly specific aluminium extrusion. I don't know if it's a common form or one that's produced specially for cheap laser cutters.



Ideally, you wouldn't replace it with the same nasty stuff but fit a precision ground linear slide instead. But that will involve a  fair bit of metalwork as it's not just a drop-in replacement.


---
*Imported from [Google+](https://plus.google.com/100121420236877030725/posts/GFFcqazDTvr) &mdash; content and formatting may not be reliable*
