---
layout: post
title: "I would probably change my control board but I still have nightmares about trying to build a CNC and attempting to connect the electronics (the dreaded TB6560 ) and I not convinced that any of these replacements such as"
date: September 11, 2016 13:50
category: "Original software and hardware issues"
author: "Bruce Golling"
---
I would probably change my control board but I still have nightmares about trying to build a CNC and attempting to connect the electronics (the dreaded TB6560 ) and I not convinced that any of these replacements such as smoothieboard or ramps are a simple drop-in conversion.

So even though the stock software is troublesome to say the least, I think I had better stick with because I know it does work.

That being said; here is my problem.  I am having a difficult just indexing where something will be engraved/cut on the k40.  Is there some sort way of setting the dimensions in corellaser and laserdrw to correcspond with the machine.  Or is it a matter of making test cuts 





**"Bruce Golling"**

---
---
**greg greene** *September 11, 2016 14:13*

Both programs have rulers that tell you where on the workpiece the work starts.  Home the head to the top left, measure from there. 


---
**Anthony Bolgar** *September 11, 2016 14:40*

Here is a solution to your board upgrade worries [http://www.all-teksystems.com/acr](http://www.all-teksystems.com/acr)

It is a plug and play solution to upgrading the K40 , designed by  a member of the community **+Scott Marshall** He knows his stuff, and if you had any issues getting it installed (highly doubtful, as it is very well designed) he would be available and willing to provide Tech support on his products.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 20:18*

I am using **+Scott Marshall**'s ACR kit for my Smoothieboard upgrade. It was a very simple process, literally plugging & playing. As I already had my Smoothieboard, I had to connect that to Scott's ACR & Scott stepped me through that. Scott also sells a kit with the Smoothieboard & ACR already preconnected together, so you basically just have to plug in the power connectors (from your existing PSU) & the stepper motor cables.



I was super concerned about the whole upgrade process, like yourself, because I am not very knowledgeable with electronics & was certain I would kill something. I highly recommend Scott's ACR & utilising the customer service he offers to step you through the process (to make sure you don't do anything wrong).


---
**Scott Marshall** *September 11, 2016 21:21*

I just finished with a phone call to a customer who had a corrupt "config" file. I really DO support my stuff. That's my 1st rule, I have a pet peeve about companies that sell stuff and then farm out support to an answering service. I answer my phone and emails personally, and as Mr Truman once said, "The buck stops here"



If I sell you a kit, I will make sure you have it running and are happy with it. 



Most installs are quite simple and involve only a few plug in cables and a couple of wires that need to be attached to a terminal block. If you run into problems, I will personally talk you through the problem. Learning the software seems to be the hardest part of the installation, and that is pretty well supported by the fine folks at the Google K40 Group.  If ultimately, you're not happy for any reason, I'll give you a 100% refund. (hasn't happened yet though).



Don't be pressured into making a change just for changes sake. While my kits allow you to use other software, all packages have their pluses and minuses. If Laserdraw and Corel work for you, stick with them. 

I have a new product coming out soon called the Switchable, and it allows you to keep your old (and proven for many people) M2nano board and change to any other aftermarket controller (like a Smoothieboard) with the flip of a switch. This is more expensive, but offers several advantages. The first is that you get to keep and continue to make all your old projects. The 2nd, less obvious advantage is that while your are learning the new system and it's software, you can continue to use the old system, thus not interrupting your work while you get a feel for the new system. You can always fall back to your 'old faithful' workflow set of software that works for you.



I expect these to be available in production form in a few months. There are prototypes still available if your're interested, (I ran 12 boards, but most are spoken for and it's still in development)



The ACR series is nearing the end of the prototype run (I made 26 boards and most are gone) and I will be doing a couple of short runs to verify the new improved design and then will have a larger run made around the end of the year. I'm also well on the way to a completely illustrated manual and even looking into producing installation videos on youtube (Lots of great ideas, so little time...)



The improvements to the ACR boards/kits will include LEDs on all I/O (Endstops, Laser fire translator in and output) , will include jumper selection to set motor wiring and direction (if needed - it's usually just a software change). In addition, the new design will include switch mode/heatsink free power supplies for both 5V and 3.3v (with 3 amp capacities). Most of these improvements are taken directly from the Switchable design.



While I tend to explain things in technical terms, don't sweat it if you don't know what it all means, These kits are designed to allow a non-technical person to easily complete an upgrade. Even if you struggle putting in batteries, you can do this. There are dozens of kits for experimenters and hobbyists out there, but these are the only kits designed for normal people to use. Just because your're not an electronics expert doesn't mean you shouldn't be able to upgrade your laser cutter.

The ALL-TEK line of kits is for the "nongineer" as Yuusuf, my webmaster says. You don't need to be an engineer to install them.



Scott


---
**Anthony Bolgar** *September 11, 2016 21:33*

I really like the way you have approached this Scott, it seems to be very well thought out. Even though I have the technical skills, if this had been around a year ago, I would have purchased one.


---
**Scott Marshall** *September 11, 2016 21:42*

I've been wanting to work again since I was disabled in '98 and it's done me a lot of good too. I sure miss working, and this seems like it's helping a few people out to boot. 



Thank to all here who have supported my efforts, and those who believed in me enough to try one of my kits.



I'd love to be working on some big industrial machine in an exotic location, but sometimes you just have to do what you CAN do. 



Scott


---
**Mike Mauter** *September 12, 2016 01:39*

I am the customer that Scott was talking about with the corrupt Config file. I can attest to the fact that Scott is there with you every step of the way. Saturday or Sunday does not matter. He will get back to you to help with your problem. The installation of the ACR with Smoothie board and power supply really only takes about 30 minutes and that is because you drill 2 holes. 


---
**Don Kleinschnitz Jr.** *September 12, 2016 14:03*

Although capable of rolling my own hardware and software, I bought my k40 to aid in fabricating flat work for prototypes. I did not buy it to convert it as a hobby and figured that the stock unit and its software "can't be that  bad". 



The mechanical-optics wasn't my problem, it was the software and DSP.



The software tool chain to get anything done was so arduous it was easier for me to cut parts by hand than to spend the hrs. it took to convert designs and then cut in seconds. The total fab time for using the K40 was at least 2x the time to do it by hand..... 



Certainly you can get help with Corel and Laswerdraw from the forums but there is scraps of documentation and the software is clunky (IMO). Corel Draw is not a CAD tool! Finally I had lots of Norton security problems show up with Win10.... and literally could not load Laserdraw reliably without opening up security holes.



The solution is to get to a tool chain that uses a non-proprietary command format i.e. G-code.



Although I am going slow and obsessing about doing the conversion so that it is repeatable and documented (as well as characterizing some of the less obvious operations (PWM)). I am excited about where I will end up. Once done (if that really is a thing) I will have a base to create any machine type I want. Probably will convert my UPS 3D printer to smoothie .....



I think it is good to work with the stock K40 for a while to get a handle on the mechanics, DSP, air systems and optics. If that configuration doesn't meet your needs nor amount of patience then don't hesitate to convert, there is lots of help here.



I researched a lot of controllers and settled on the Smoothie (from Uberclock) and LaserWeb was a pleasant surprise.



If Scott's board had been ready I would have used it and maybe will in the future. 


---
*Imported from [Google+](https://plus.google.com/105808932307807066174/posts/CUpYfZg8n2n) &mdash; content and formatting may not be reliable*
