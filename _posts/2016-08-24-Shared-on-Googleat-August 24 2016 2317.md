---
layout: post
title: "Shared on August 24, 2016 23:17...\n"
date: August 24, 2016 23:17
category: "Modification"
author: "Robi Akerley-McKee"
---




![images/bf186118d38b8def6e6b3427e345e5e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf186118d38b8def6e6b3427e345e5e1.jpeg)
![images/0c5e3e76279a23f761e807b2e901f415.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c5e3e76279a23f761e807b2e901f415.jpeg)
![images/ff5a5da38bbac1ebaf546989cef4056e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff5a5da38bbac1ebaf546989cef4056e.jpeg)
![images/cc86917df610e494705d31de11365609.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc86917df610e494705d31de11365609.jpeg)
![images/538f406aae78840b06720b7ed943318d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/538f406aae78840b06720b7ed943318d.jpeg)
![images/2921c1f2465430f10305b8ee4440bef1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2921c1f2465430f10305b8ee4440bef1.jpeg)
![images/f9d857a75368c9d77b03fc758d8a398c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9d857a75368c9d77b03fc758d8a398c.jpeg)
![images/a13a874e4ef1c3ab6c6c7a6640fdd533.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a13a874e4ef1c3ab6c6c7a6640fdd533.jpeg)

**"Robi Akerley-McKee"**

---
---
**Jonathan Davis (Leo Lion)** *August 25, 2016 00:02*

???


---
**Robi Akerley-McKee** *August 25, 2016 00:51*

Pictures for **+Nigel Conroy** adding lid switch and keylock


---
**Robi Akerley-McKee** *August 25, 2016 06:21*

Modded things a bit more...  Took the stepper motor cover off the front and moved my bed forward.  I now have 230mm (9.055") travel on Y now.  Looking at moving Y stepper motor to the outside of the gantry to gain another inch of travel, that would get me to 10" 254mm.  Looks like I'm going to have to build a new honey comb table -- 13x13.5" ish 12x12.5 inside...


---
**Nigel Conroy** *August 25, 2016 13:53*

Thanks for sharing this **+Robi Akerley-McKee** much appreciated, I'm sure I'll have more questions when I come to doing the install!!!


---
**Nigel Conroy** *September 27, 2016 16:01*

**+Robi Akerley-McKee** I got the key switch installed perfectly. I wired it in line with the Emergency Stop button so the machine won't power up unless it's in the on position. 



Going about installing the door switch and the laser power button has four connections on the back. So I've paused and come for some assistance/advice.  I don't want the machine to turn off completely every time the door opens so I'm not sure which wire to connect the switch to.



Any help appreciated.

![images/3d9fd36bb83786d3586211f377d4dd29.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d9fd36bb83786d3586211f377d4dd29.jpeg)


---
**Nigel Conroy** *September 27, 2016 16:04*

![images/12c1da422efddbc3fce279e25d7f9208.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/12c1da422efddbc3fce279e25d7f9208.jpeg)


---
**Nigel Conroy** *September 27, 2016 16:05*

![images/8d8cb0f85ece23f739cb67554b55be45.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d8cb0f85ece23f739cb67554b55be45.jpeg)


---
*Imported from [Google+](https://plus.google.com/103866996421752978311/posts/EBh7G49wYmU) &mdash; content and formatting may not be reliable*
