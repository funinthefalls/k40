---
layout: post
title: "Finally found some time to hook up my new laser cutter (a 400X500 50W machine)"
date: September 14, 2017 09:43
category: "Discussion"
author: "Anthony Bolgar"
---
Finally found some time to hook up my new laser cutter (a 400X500 50W machine). Was shocked and wonderfully surprised that all the mirrors were perfectly aligned. Build quality is very nice as well, no nightmare wiring to fix like many Chinese machines require. Now I need to build a base to put it on, it has casters on the bottom, but I find a machine basically sitting on the floor very hard to use. I will take the wheels off the laser place it on a custom built cabinet base and reuse the wheels on the bottom of the base. I like all my benches tables and machines to be on wheels so I can easily reconfigure my work area, or even just to be able to move things to clean under them.





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *September 14, 2017 11:45*

When you get time I would be interested in that LPS is in it :)?


---
**Anthony Bolgar** *September 14, 2017 14:21*

Same LPS as a K40 by the looks of things.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/YMJuBV3iLac) &mdash; content and formatting may not be reliable*
