---
layout: post
title: "So lets see, we take what we learned from the prior video and put it to use in this one so lets see how it works out!"
date: November 25, 2015 21:36
category: "Discussion"
author: "DIY3DTECH.com"
---
So lets see, we take what we learned from the prior video and put it to use in this one so lets see how it works out!  Also looking for suggestions where the best place is to order an 18mm lens from as I now want to try changing that up!





**"DIY3DTECH.com"**

---
---
**Coherent** *November 25, 2015 21:45*

Light Object has len's they state are better quality than original. I bought one (18mm 50.8 fl) and would say it cuts a bit better.

 You can find better prices on Ebay and elsewhere, but quality is an unknown. Some may very well be the same as LO len's for less.


---
**DIY3DTECH.com** *November 25, 2015 23:17*

Agreed, if I am going to swap want to know I am swapping for something better.  Will have to check them out...


---
**Scott Thorne** *November 26, 2015 10:22*

I agree with Marc G....I have one from Amazon...one from light objects....the latter is the better.


---
**Scott Thorne** *November 26, 2015 10:55*

Your focal length should be 2 inches exactly.....from the bottom of the lens to the top of your workpiece.


---
**Scott Thorne** *November 26, 2015 10:56*

50.8 mm = 2 inches


---
**DIY3DTECH.com** *November 26, 2015 12:27*

Placed the order with Light Objects as after seeing the aberrations (even in visible light) know the existing lens is junk so looking forward to the new one will a video on the results...


---
**Scott Thorne** *November 26, 2015 12:37*

When I get up and about today I will take a measurement on my rail to my mark and post it...see if yours is the same.


---
*Imported from [Google+](https://plus.google.com/+DIY3DTECHcom/posts/8HQzbyJgLSQ) &mdash; content and formatting may not be reliable*
