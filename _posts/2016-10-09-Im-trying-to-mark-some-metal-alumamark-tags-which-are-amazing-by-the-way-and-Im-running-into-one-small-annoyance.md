---
layout: post
title: "Im trying to mark some metal (alumamark tags) (which are amazing by the way) and Im running into one small annoyance"
date: October 09, 2016 14:42
category: "Hardware and Laser settings"
author: "Bob Damato"
---
Im trying to mark some metal (alumamark tags) (which are amazing by the way) and Im running into one small annoyance.  Using coreldraw and corellaser, on stock board, I cant seem to fill in lines or text.  Any text  that is somewhat large, or lines  that are thick, it just 'cuts' the perimeter. I need to be able to fill these.  Any recommendations?

Bob





**"Bob Damato"**

---
---
**HalfNormal** *October 09, 2016 15:04*

**+Bob Damato** you have to use the engrave function to fill areas.


---
**Bob Damato** *October 09, 2016 15:14*

I was afraid of that. Thank you


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 09, 2016 15:25*

Actually, another trick you could use that is faster than using the Engrave function (& may just well work for your purposes) is to turn your shapes into a series of lines, spaced apart about 0.5mm. Then instead of cutting the perimeter of the shapes, it will cut a ton of lines. Have a look on my profile (back a fair way) for examples of me testing a "mock-engrave" with the CorelLaser software.


---
**Jim Hatch** *October 09, 2016 16:18*

And if you defocus it (by dropping the bed a mm or 2) you'll get a wider beam so it "cuts" a thicker line.


---
**Bob Damato** *October 09, 2016 18:22*

Thank you Guys. :)  **+Yuusuf Sallahuddin**  I spent the last 45 minutes looking but no luck.  But I must say youve been having a lot of fun with yours!! :)




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 09, 2016 18:55*

**+Bob Damato** I will find it & link for you then. It is probably buried a long way back in my history haha.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 09, 2016 18:58*

**+Bob Damato** Here is the "mock-engrave" or "vector engrave" that I was testing with the stock hardware/software in the past: [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/3sky7g1fUZD](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/3sky7g1fUZD)


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/9PTzFUz4SFT) &mdash; content and formatting may not be reliable*
