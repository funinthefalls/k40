---
layout: post
title: "Have a question, I am losing power at the end of the x axis"
date: March 26, 2017 03:18
category: "Discussion"
author: "John Stevens"
---
Have a question, I am losing power at the end of the x axis. it is deeper at the beginning than the end. any help would be appreciated, thank you

![images/5eef3d22ccc123689b6ff0d18dcca4a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5eef3d22ccc123689b6ff0d18dcca4a2.jpeg)



**"John Stevens"**

---
---
**Ned Hill** *March 26, 2017 15:12*

You probably need to do an alignment of your laser.  See the floating wombat guide linked here. [plus.google.com - - Laser Alignment Guide - With all the laser alignment questions we get, here...](https://plus.google.com/108257646900674223133/posts/LGfT6SS3Tcc)


---
**Ned Hill** *March 26, 2017 18:36*

Note, someone let me know the link  referenced in that post was dead.  I updated the link in the referenced post to a host I control.


---
**James Rivera** *March 27, 2017 17:31*

Is your bed flat? (parallel w/r/t the laser carriage)  If not, then the beam focus would degrade, which might explain this.


---
**Phillip Meyer** *March 28, 2017 16:31*

(the link still seems to work for me)


---
*Imported from [Google+](https://plus.google.com/108124828909436986786/posts/jN4CdKWVkmm) &mdash; content and formatting may not be reliable*
