---
layout: post
title: "How about this?"
date: April 01, 2017 20:13
category: "External links&#x3a; Blog, forum, etc"
author: "Ariel Yahni (UniKpty)"
---
How about this?





**"Ariel Yahni (UniKpty)"**

---
---
**Ned Hill** *April 01, 2017 20:30*

Nice.


---
**Richard Vowles** *April 01, 2017 21:09*

I thought visicut was dead?


---
**Jim Hatch** *April 01, 2017 22:06*

That's Martin Raynsford's company (UK laser assemblers) - [justaddsharks.com](http://justaddsharks.com)


---
**Stephane Buisson** *April 02, 2017 09:53*

**+Richard Vowles** what make you think that ?

Visicut and/or Laserweb is the way to go. (same hardware platform)


---
**Richard Vowles** *April 02, 2017 09:54*

I have seen numerous mentions of development having stopped


---
**Stephane Buisson** *April 02, 2017 09:58*

[hci.rwth-aachen.de - The Media Computing Group :](http://hci.rwth-aachen.de/visicut-download)

last was 28/3/2017.


---
**Stephane Buisson** *April 02, 2017 11:51*

Crowd funding campain ... Glow Forge Campainer's still to be delivred, I have heard something like postponed again to July/August 17.


---
**Claudio Prezzi** *April 02, 2017 19:57*

The gantry construction look very similar to **+FabCreator**​


---
**Bonne Wilce** *April 05, 2017 08:22*

**+Claudio Prezzi** only by the fact they move the tube ;)

 

blacktooth laser were the first to do it, then us (FabCreator), GF and now VB, Its funny how many people think moving the tube is crazy and it will break it, yet never knew any flatbed co2 laser has been moving the tube for decades :P 

In reality you get easy alignment even power simpler design and more cutting space for machine size :) the only down side is more mass so less speed/acceleration. But with a 40w your not going to be cutting that fast the FabKit cuts 3mm poplar around 25-30mm/s with no issue, with the rastering on just the X axis, once the bug is fixed speeds of 500-1000mm/s should be no issue for the machine :) 



Its a nice project there is a demand for these low cost machines just because people don't have the money. however at 1500 pounds and from what i can see in the photos i would worry about the longevity of the machine. Most of it looks like off the shelve Chinese laser parts so not sure how it will compare to a traditional Chinese machine with a new board? 



What surprises me is they are not using laserweb or even mention it? 

That and they look like they may be using a smoothieboard clone? 

The design is also missing a few essential Machine directive components.






---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/7hxTxnhRSV6) &mdash; content and formatting may not be reliable*
