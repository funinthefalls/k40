---
layout: post
title: "Yuusuf Sallahuddin .....here is some small very detailed items that are available on google....Google scrollwork images and convert"
date: April 05, 2016 13:48
category: "Object produced with laser"
author: "Scott Thorne"
---
**+Yuusuf Sallahuddin**.....here is some small very detailed items that are available on google....Google scrollwork images and convert.

![images/b418e181ac09c43058c15ec9f0936cde.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b418e181ac09c43058c15ec9f0936cde.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 13:57*

Wow, that's pretty nice. I wonder does the bigger laser you have get the details as well as the K40 or better or worse?



I just did my first tests on 3mm ply today & had pretty decent results. Will definitely look around for some detailed images to test on. Are you running stock software or LaserWeb?


---
**Scott Thorne** *April 05, 2016 14:05*

**+Yuusuf Sallahuddin**....I'll be honest...the k40 I used to own with the m2 nano...it did just as good...the only thing about this machine is its bigger...it has a motorized hight table and rotary attachment...that I barely use by the way...it's built great and laid out great....but both machines will do the same thing...mine will just do it faster because I have a bit more power.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:07*

**+Scott Thorne** Ah yeah I thought that may be the case. The added cutting area would be an awesome bonus, however I have been sticking to smaller projects until I learn more what I'm doing then will create a conveyor belt style table (to give me more y-axis room). I can't really justify the extra expense for a bigger machine (unless I was making money off what I am creating with it).


---
**Scott Thorne** *April 05, 2016 14:12*

**+Yuusuf Sallahuddin**...that's why I bought a bigger machine...I make parts out of acrylic for my job and the company pays me for each part I design and cut...so it's paid for itself already.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:15*

**+Scott Thorne** Ah lucky you. That makes a lot of sense then.


---
**I Laser** *April 06, 2016 10:24*

Very lucky indeed. Like the left hand design, not that I have the skills but would like to try 3D laser cut art ie: http://blog.ponoko.com/wp-content/uploads/2013/09/seaserpent..jpg


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 15:16*

**+I Laser** Your link is not working for some reason, however I found it in the search ([http://blog.ponoko.com/2013/09/27/ornate-laser-cut-wood-with-depth/](http://blog.ponoko.com/2013/09/27/ornate-laser-cut-wood-with-depth/))



Very nice pieces & I'd love to have a go at them too. Quite cool & gives me ideas for other artistic multi-layered pieces now. Thanks for sharing that.


---
**Scott Thorne** *April 06, 2016 15:24*

Very nice pieces.


---
**I Laser** *April 06, 2016 21:41*

Yeah it's crazy what some people can do. Some of those contain over 20 layers! Well beyond my skill level...



Glad it inspired you **+Yuusuf Sallahuddin**​, look forward to seeing what you come up with.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 22:00*

**+I Laser** I think if you can cut a single layered piece, you can cut a 1000 layered piece. Just requires patience & practice I'd say. I'm going to start with something simple like 2-3 layers until I get the hang of it.


---
**I Laser** *April 07, 2016 00:01*

**+Yuusuf Sallahuddin** Yeah pretty confident about the cutting side of things, it's more the design. ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 00:01*

**+I Laser** **+Scott Thorne** I just noticed there are more of these works by the artist here: [http://thedancingrest.com/2013/09/19/laser-works-by-martin-tomsky/](http://thedancingrest.com/2013/09/19/laser-works-by-martin-tomsky/)



Some really great pieces. Just have to work out an idea for something to do similar with.


---
**I Laser** *April 07, 2016 00:13*

[https://www.etsy.com/shop/TomskyStore](https://www.etsy.com/shop/TomskyStore)



Some mind blowing items!!!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 00:13*

**+I Laser** Yeah, that's one of my stronger points the design. Been doing graphic editing since I was a teen & have always been reasonably arty/creative. Just can't draw by hand for the life of me. Need an "undo" button when I draw.


---
**I Laser** *April 07, 2016 00:20*

Well we'll all be expecting something like this in the next couple of days from you then: [http://bit.ly/1oDoyPg](http://bit.ly/1oDoyPg)



lol :D


---
**Scott Thorne** *April 07, 2016 00:21*

**+I Laser**...yeah...he's committed himself now...lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 02:26*

**+I Laser** **+Scott Thorne** I think that one is a little bit ambitious, but maybe a slightly less complicated version. More like this: [http://orig10.deviantart.net/17b2/f/2013/331/5/4/woodland_brooches_by_mtomsky-d6vurt4.jpg](http://orig10.deviantart.net/17b2/f/2013/331/5/4/woodland_brooches_by_mtomsky-d6vurt4.jpg)


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/ZoCYWeFBc7f) &mdash; content and formatting may not be reliable*
