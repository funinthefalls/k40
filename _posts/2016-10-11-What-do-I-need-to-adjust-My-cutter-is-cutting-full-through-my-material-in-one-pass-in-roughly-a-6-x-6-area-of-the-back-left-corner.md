---
layout: post
title: "What do I need to adjust? My cutter is cutting full through my material in one pass in roughly a 6 x 6 area of the back left corner"
date: October 11, 2016 13:34
category: "Hardware and Laser settings"
author: "timb12957"
---
What do I need to adjust? My cutter is cutting full through my material in one pass in roughly a 6 x 6 area of the back left corner. However in the right most and front quadrants, it only cuts some of the pattern through. I have went through the alignment procedure and the dot seems to hit right in all positions. Does it sound like a focus, or out of level bed issue? 





**"timb12957"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 11, 2016 13:42*

First thing I would check is if the rightmost & front quadrants are the same distance from the lens holder base to the cutting bed. Hopefully it is something simple like it being fractionally out for your right/front.



After that, I would make sure that alignment is spot on & exiting the lens holder in the centre (replace lens with a piece of paper or tape to check).



Realistically though, I think the power does degrade the further away from the tube it gets, but it shouldn't be that much over such a small area.


---
**greg greene** *October 11, 2016 13:51*

Stock Head or Air Assist? Is the beam hitting in the center of the mirrors in all cases? It has to or it will never hit the center of the hole in the head - and if it doesn't hit there then it will hit the wall of the head in some positions - and you will see what you are seeing.


---
**Ariel Yahni (UniKpty)** *October 11, 2016 14:03*

**+Yuusuf Sallahuddin**​ I agree with that there must be same distance from the bed to the head all over the machine. Not sure if the power goes down, I think since getting the beam on the center on the far end of the gantry is difficult, this could translate on power loss


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 11, 2016 16:00*

**+Ariel Yahni** I was just meaning over distance, it will eventually dissipate? The laser can't go forever can it? Unless... we focus through lenses at regular intervals... but I imagine there would be loss due to the lens every time & eventually would be no power... just a bit of guesstimation there on my part though. No idea really.


---
**Ariel Yahni (UniKpty)** *October 11, 2016 17:38*

Maybe **+Scott Marshall**​ can tell as the technical details on how far the beam can go from this tube


---
**timb12957** *October 11, 2016 23:32*

I replaced the lens with a small piece of white tape and fired the laser. It hit out near the edge of the lens circle. I assume that means I need to start over with the mirror adjustments, making sure it hits as close to center on each one as possible.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 11, 2016 23:55*

**+timb12957** Mostly you just need to make sure that it is coming out from the carriage mirror directly down. Sometimes the carriage/lens holder thing is not perfectly flat & can require shimming to get the beam exiting through the centre. Look into that before messing around with all the mirrors.


---
**greg greene** *October 12, 2016 00:11*

Yup - and you don't need to replace the mirrors - just cover them with a piece of painters tape - you will have to clean the residue afterwards however.


---
**timb12957** *October 12, 2016 02:20*

I had to shim the mirror on the head to make the beam strike the center of the lens holder. That did the trick!


---
**greg greene** *October 12, 2016 12:49*

Awesome!!!


---
**Scott Marshall** *October 13, 2016 11:56*

**+Ariel Yahni** 

That's a classic issue.



The farther you get from the upper left (home) the larger effect any misalignment has on the beam (which is exactly WHY they put Home in the top left instead of lower left)



The reason for this isn't the laser weakening with distance (is can travel quite a long distance before really degrading), but that a small angular error is increased with distance. At 20mm, a 1mm error is a 10mm error at 20cm.(200mm)



 It's entirely possible to completely miss the head in the lower right corner while the laser still functions (but probably not great) at the upper left.



One of the lasers powers is the lack of beam divergence. While a CO2 laser isn't the best in this regard, it's still diverges an incredibly small amount at distance. We're talking hundreds of meters before it widens much. Debris in the air is largely responsible for weaking and spreading of laser beams here on earth.

They bounce a green laser off a 24" mirror left on the moon by Apollo 11 routinely. Half a million miles and they still catch the reflection. This thought made me look for a photo of the mirror, The article I found claims often only a single photon returns to the emitter - isn't science amazing?



[https://science.nasa.gov/science-news/science-at-nasa/2004/21jul_llr/](https://science.nasa.gov/science-news/science-at-nasa/2004/21jul_llr/)


---
**Ariel Yahni (UniKpty)** *October 13, 2016 12:01*

**+Scott Marshall**​ understood, so if you have a really good alignment in place then cutting power should be equal in all corners correct? 


---
**Scott Marshall** *October 13, 2016 12:20*

Yes. Align for the farthest point, and beam quality will only be better throughout the rest of the bed.

The way I align my mirrors is to fire a spot close in , push the carriage all the way out, and spot again. I keep doing it until I have the mirrors adjusted so the beam doesn't change position at all - the 2nd shot shouldn't even burn paper. (use foil behind tape to keep smoke off the mirror and keep it from bouncing the beam back and screwing up your test) Adjust only the 2 top(vert) or 2 side (horiz) screws at a time to prevent chasing your tail. Once the Y axis is perfect move to the X, then adjust the head by raising/lowering it for X, and rotating it for the Y. If you have air assist, remove the lens and put your tape on the nozzle. Aim for the center and the beam will be very close to vertical.



Opinions vary, but that's how I do it. 

The Good old Flying (sorry floating) Wombat is where I started, and it's still a fine guide.



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)

[floatingwombat.me.uk - www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**greg greene** *October 13, 2016 13:28*

Hey Scott, thanks for the tip on using the foil - hadn't thought of that !


---
**Scott Marshall** *October 13, 2016 15:16*

**+greg greene** 

That's the great thing about this group, we all learn stuff all the time.

 

I just ran across a gentleman who suggested using Windshield Washer fluid for coolant. That's a great idea, I've been telling people to add alcohol to their water, with WW fluid, it's all pre-mixed, and the blue ought make it easier to see bubbles. You can get it cheaper than you can buy the alcohol if you catch the sales during our northeastern winters (here in the Syracuse area in January, you use more WW fluid than gas)



Scott






---
**greg greene** *October 13, 2016 15:40*

Another great tip - buying rubbing alcohol at the drug store is expensive !


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/PAfKmtgPZUS) &mdash; content and formatting may not be reliable*
