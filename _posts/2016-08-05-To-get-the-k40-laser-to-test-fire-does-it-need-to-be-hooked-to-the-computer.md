---
layout: post
title: "To get the k40 laser to test fire does it need to be hooked to the computer?"
date: August 05, 2016 20:54
category: "Discussion"
author: "John Austin"
---
To get the k40 laser to test fire does it need to be hooked to the computer?



I have water flowing through the laser tube and power supply is running.

I press the test button nothing happens the Ma gauge doesn't move.

 





**"John Austin"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 21:07*

Test fire doesn't need to be connected to the PC.



Did you press the Laser Enable switch in? If that is not switched on you will get no mA & no fire.


---
**John Austin** *August 05, 2016 21:21*

No I didn't,

Take it I need to turn laser enable [on.to](http://on.to) test fire?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 21:32*

**+John Austin** Yeah, with the Laser Enable button depressed it should do the test fire. Give that a test & hopefully it helps. At least on mine I have to have it depressed to do a test fire.


---
**John Austin** *August 05, 2016 21:34*

thanks finally got the cd to open going to go and print out the user manual. hopefully i can get the software to work with windows 10.


---
**John Austin** *August 05, 2016 22:03*

it worked for some reason I was thinking that laser enable was only for when you are going to run a pattern.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 04:27*

**+John Austin** The laser enable button is basically a safety feature. You generally switch it off any time you intend to open the laser lid so you don't zap yourself (it hurts) or blind yourself. Glad you got it going :)


---
**John Austin** *August 06, 2016 04:32*

Thanks, I am watching videos trying to figure out the software so I try and at least get it to cut something or engrave. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 04:37*

**+John Austin** If you need any assistance with how to get going, feel free to ask & I can share what I know (mostly from trial & error).


---
**John Austin** *August 06, 2016 04:41*

**+Yuusuf Sallahuddin** thanks is there a test file any where that I can use a a example?  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 05:15*

**+John Austin** None that I am aware of, but you can just grab any image to do a test for engraving. Cutting just draw a rectangle (20 x 20 mm is what I usually use for testing different materials & settings) in Corel Draw.



edit: are you using Corel Draw/CorelLaser plugin or the LaserDRW software?


---
**John Austin** *August 06, 2016 05:37*

Trying to get coral draw corellaser to load again. That's what came with the laser 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 06:05*

**+John Austin** Yeah, it's not the best software in the world, but it is the all you can use with the default controller (or LaserDRW). It is functional & does what it is supposed to nicely, albeit a bit confusing at times.


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/aBFcNmAkoP7) &mdash; content and formatting may not be reliable*
