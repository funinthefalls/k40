---
layout: post
title: "Don Kleinschnitz Scott Marshall any idea what the L out power is off the stock K40 M2nano board is ..."
date: August 15, 2016 05:40
category: "Original software and hardware issues"
author: "Alex Krause"
---
**+Don Kleinschnitz**​ **+Scott Marshall**​ any idea what the L out power is off the stock K40 M2nano board is ... by this I mean what voltage is ran from the Laser Fire pin that connects the M2Nano board to the laser PSU





**"Alex Krause"**

---
---
**Anthony Bolgar** *August 15, 2016 05:50*

I am pretty sure it is 5V


---
**Alex Krause** *August 15, 2016 05:53*

I'm running into an issue that no matter what I set any config file settings in smoothie/ my max power potentiometer setting... the highest power I cut according to my ma guage is 7ma


---
**Alex Krause** *August 15, 2016 05:54*

Which makes me think the power connection between the stock controller and psu is a higher voltage than 5v ... I may be wrong... been a long weekend


---
**Anthony Bolgar** *August 15, 2016 06:00*

When I did my Marlin conversion, I had marked down the voltages from each PSU output. Found the notes just now, and it was 5V

, but I have a dual pin set up for firing the laser, and if I remember correctly, yours is single pin?


---
**Alex Krause** *August 15, 2016 06:02*

Are you sure that is the voltage that runs from the nano board to the psu on the 4 pin connector... on my psu it's marked at L  (24v,G,5v,L) 


---
**Scott Marshall** *August 15, 2016 06:24*

**+Alex Krause**

The only control power on the M2 is 5V. the 24v is strictly motor drive (it's all tied to the Allegro A4895 drivers)



The only non 5v signal I ran into when dissecting the M2 was the 4.3v that runs the endstop optos (it's 5V thru a diode and bypass cap)



PSU output power is controlled by the 0-5v signal created by the pot. check the wiper and make sure it's hitting 5V at full CW. If you're tied in here make sure it's not  with a 3.3v Smoothie logic source.



The only other thing I might check would be that something isn't pulling down the 5vdc into the pot - it uses the same source you're using if you don't have an aux supply. The Stock 5v supply is pretty light.


---
**Don Kleinschnitz Jr.** *August 15, 2016 12:16*

**+Alex Krause** **+Scott Marshall**



The Nano uses the "LO"/"L" signal on the PS to turn the laser for digital control. This is the only digital connection between the M2 and the LPS.

The signal is "LO" on the M2 side and "L" on the PS side and the signal routes through the 4 position DC power cable.



On my supply the DC connector contains: LO:5v:GND:24V. I have a type A supply (my nomenclature) with this interface: 



[https://1.bp.blogspot.com/-vOmLiPap6OI/V18lmsdHb_I/AAAAAAAAZ3s/tBolbK-Ypg0bH-8kJATqTJPHiDn3ST43wCKgB/s1600/20160613_145220.jpg](https://1.bp.blogspot.com/-vOmLiPap6OI/V18lmsdHb_I/AAAAAAAAZ3s/tBolbK-Ypg0bH-8kJATqTJPHiDn3ST43wCKgB/s1600/20160613_145220.jpg)



Note: For testing (incomplete) I have a PWM generator on the "L" pin of the LPS. 



The "L" signal on the LPS is LOW true. I am assuming it is providing grnd to the LED on one side of an opto isolator in the LPS.



My prevailing plan is to connect the "L" pin to an open drain/collector output on the smoothie. If I recall it was P2.5. With this setup no inversion and no level shifter is necessary.



I will soon simplify my LPS interface post: 



[http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface_86.html](http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface_86.html)



The supply type below refers to the configurations outlined in the post above.



... recommending

 ------------------ 

For type A supply PWM control: use the "L" signal on the LPS DC connector for -PWM control.

The pot can be left in but will override smoothie control of power. Use an open drain from the smoothie.



For a type B supply PWM control: I do not yet know if an "L" signal is available. If so use it. If not use "TL". [UNTESTED]. Use an open drain from the smoothie.



What is tested: 

I currently have a PWM generator connected to the "L" signal on the LPS and it fires the laser visually ok. There are also multiple K40 users using this method.



[http://donsthings.blogspot.com/2016/07/dynamic-laser-response-testing.html](http://donsthings.blogspot.com/2016/07/dynamic-laser-response-testing.html)



I do not have a measurable light output from the laser working yet so I do not know what the light output of the laser looks like and therefore do not know if it is faithfully reproducing the PWM pulse. Turns out there are few means of electronically sensing a 10.6 um light source, most sensor cut off a 1um or less :(.



Hope this helps ....




---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/X3tUxh1qt46) &mdash; content and formatting may not be reliable*
