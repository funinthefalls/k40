---
layout: post
title: "cut I want to cut a rectangle (90 x 130mm) with rounded edges out of paper like a 100 times"
date: July 01, 2017 09:33
category: "Discussion"
author: "Bernd Peck"
---
#Rectangle cut 

I want to cut a rectangle (90 x 130mm) with rounded edges out of paper like a 100 times. The rectangle has a printed design on it.

As this is a prototype that shall be commercially printed at a printing house I have designed this in InDesign.

Could you please help how I can align the printed at home pieces of paper for the prototype so the K40 can accurately cut the rectangle.

Thanks







**"Bernd Peck"**

---
---
**Chris Hurley** *July 01, 2017 12:36*

You're going to need some form of registration. As in registration pins or a form or box that's going to hold it. Then it's actually super easy as long as the files in an SVG file type. Or you could just make the square in Corel Draw or laser draw. 


---
**Mark Brown** *July 01, 2017 22:11*

Lay a sheet of something thicker in slid up and to the left as far as possible.  Then make cuts across the very top and left of your work area.  After that you can lay a sheet of paper in and set it against the top/left each time.  Does that make sense/help?


---
**Bernd Peck** *July 02, 2017 06:40*

Thanks for your help. Will try both solutions and see how it works for me


---
**Bernd Peck** *July 05, 2017 18:46*

I have tried a combination of the two suggestions and it worked out very good! Thanks again


---
*Imported from [Google+](https://plus.google.com/100238441539373475210/posts/Qw2doYxPSCK) &mdash; content and formatting may not be reliable*
