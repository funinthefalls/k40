---
layout: post
title: "I know this isn't on my laser but I'm making another LED edge lit sign that I will be making on the laser"
date: October 08, 2016 04:47
category: "Discussion"
author: "Alex Krause"
---
I know this isn't on my laser but I'm making another LED edge lit sign that I will be making on the laser. 20mm solid birch with an amazing grain that will polish up to a reflective finish with 400 grit sand paper and a finish of bees wax and mineral oil. Sure you can purchase these bases online but crafting your own has a more personal touch when you giving it away as a wedding present.if anyone wants the files to cut these out on a cnc let me know I made this file for 14mm and 20mm thick material. The biggest downside about using a cnc is the noise they make :P the laser is so quiet compared to this


**Video content missing for image https://lh3.googleusercontent.com/-8fVb_YTAH00/V_h6UgdnTHI/AAAAAAAAGaE/lGqiX7qkDu4XRIT4rLU4glAV21qVdytowCJoC/s0/VID_20161007_233545919.mp4.gif**
![images/63b0baed3d5a928619e5da97fe3f13bd.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/63b0baed3d5a928619e5da97fe3f13bd.gif)



**"Alex Krause"**

---
---
**David Spencer** *October 08, 2016 05:13*

Alex I would be interested in the file.  What kind of cnc do you have?  I have an Xcarve


---
**Alex Krause** *October 08, 2016 05:18*

I'm running an openbuilds Ox with Chilipeppr I haven't upgraded the cnc to smoothie yet or I would use Laserweb/cncweb


---
**David Spencer** *October 08, 2016 05:22*

Nice


---
**Don Kleinschnitz Jr.** *October 08, 2016 11:37*

While we are NOT talking about lasers, **+Alex Krause** **+Peter van der Walt**, what CNC/router would you recommend? It is in que for my next adventure... but getting impatient :).


---
**Don Kleinschnitz Jr.** *October 08, 2016 12:06*

**+Peter van der Walt** like this one? [http://www.smw3d.com/ox-diy-cnc-kit/](http://www.smw3d.com/ox-diy-cnc-kit/)



[smw3d.com - OX DIY CNC kit](http://www.smw3d.com/ox-diy-cnc-kit/)


---
**HalfNormal** *October 08, 2016 15:41*

**+Don Kleinschnitz** I own a Shapeoko upgraded to X-Carve hardware. They have just come out with their newest version. A few others here are also X-Carve owners.


---
**Alex Krause** *October 08, 2016 17:35*

I cut aluminum on my OX


---
**Joe Spanier** *October 09, 2016 16:43*

I would personally build an R7 over an ox just for the lead screws and extra stiffness of the C-Beam. I've had a few friends build variations on it and they are awesome machines. 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/HFvFRkDj3Pb) &mdash; content and formatting may not be reliable*
