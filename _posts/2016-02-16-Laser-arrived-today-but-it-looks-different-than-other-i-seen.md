---
layout: post
title: "Laser arrived today, but it looks different than other i seen"
date: February 16, 2016 11:48
category: "Modification"
author: "HP Persson"
---
Laser arrived today, but it looks different than other i seen.

How about adding a air assist to this one? anyone tried and what modifications was needed?



Thinking of Lightobjects air assist head, but not sure it will fit properly.

Pretty sure i have to mod it, but if anyone already done it i would be glad to see how you did it :)



![images/88490ca979fb9d986b306ee1db384b12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/88490ca979fb9d986b306ee1db384b12.jpeg)



**"HP Persson"**

---
---
**Sean Cherven** *February 16, 2016 12:35*

Oh wow, interesting indeed. Could you take a few more pics at different angles? I wanna see how this thing is made.


---
**Stephane Buisson** *February 16, 2016 12:45*

more photos please (general view, PSU, board, mirrors)

where did you buy it, price tag ? link ?

thank you


---
**Stephane Buisson** *February 16, 2016 13:19*

the hole at the tip of lens holder look like a good candidate to have the air assit. (ball pump needle ?)


---
**HP Persson** *February 16, 2016 14:13*

More pics coming up later today. Was surprised, as it has a water-temp and psu-temp gauges on the top, + emergency stop and some more upgrades :)


---
**Sean Cherven** *February 16, 2016 14:19*

Nice! Be sure to post a link here when the pictures are up. I seem to miss half the stuff that gets posted here.


---
**Coherent** *February 16, 2016 17:20*

It definitely a different design than the K40's I've seen. Look under the plate that the lens is attached to. If it's threaded you may be able to screw in an air assist. It wouldn't hold the lens so lens size wouldn't be a factor. As long as the threads were the same it should work. If it's not threaded you'll have to get creative and figure out another way to mount it.


---
**Thor Johnson** *February 16, 2016 20:54*

If you'll tell me the size of the lens-holding-bar, I could 3d print you one...

Then again, I think I'd take some copper tubing (or automotive brake line) and put it through the hole on that bar and bend it so the air goes where you want it.


---
**Scott Thorne** *February 17, 2016 11:41*

It looks similar to my setup on the 50 watt...just missing the head...but the rail looks the same and the gold aluminum angle bracket is the same as mine...all the holes are in the same place.


---
**HP Persson** *February 17, 2016 12:44*

Pictures of the laser on the link below :)



Bought at Aliexpress for $604 with shipping to my door. (Sweden). Seller was "Wonderfull Power Machines" They had sold 10+ of them and good rating.



Only one small damage on the back edge (pinches the door to the laser tube a bit), but no harm to the moving parts, squared it up and checked.



Going to the store now to pick up some cooling parts, everything was included, but the pump wants a 12mm tube, and the tube in the machine is 6mm :)



Not started yet, so no clue if it works.. but sooon :)



[http://wopr.nu/laser/](http://wopr.nu/laser/)


---
**Stephane Buisson** *February 17, 2016 13:25*

**+HP Persson** thank you for the picts, very interesting indeed community wise. could you top it up with some electronic board details (& picts) could be interesting for potential update. Psu ref, Board ref, connectors type (to motors, end stop), same link is fine. inside photos of electronic side (under control board).

**+Peter van der Walt** 


---
**HP Persson** *February 17, 2016 13:26*

Darn, i forgot that. Coming up more pics in a few hrs :)

And some data on the boards. 

I looked into it yesterday, the board had a cover, but i can easily remove it and check whats below!


---
**3D Laser** *February 17, 2016 17:29*

Where did you get this version at and how much was it if you don't mind me asking


---
**HP Persson** *February 17, 2016 18:45*

**+Corey Budwine** CHeck out the details in my earlier posts above  about where i got it and name of the seller ;)

$600 shipped to my door



The images in his ad shows 3 different types, none of the version i got. Maybe just coincidence?


---
**HP Persson** *February 17, 2016 22:38*

4 more pics added to the link.

Could not access the board right now, it was bolted to a panel and that panel bolted to the frame wich needs to be removed to lift the case/top from the board.



Will do that tomorrow when i got some better tools avalible :)



Link again: [http://wopr.nu/laser](http://wopr.nu/laser)


---
**HP Persson** *February 17, 2016 23:37*

Found a tool ;)



Text on the board

HT-Master-5

Nano

lihuiyu studio labs


---
**Trực Chính** *February 19, 2016 01:47*

**+HP Persson** Do you have Aliexpress link that you bought machine?

thanks


---
**HP Persson** *February 19, 2016 06:39*

**+Trực Chính** [http://www.aliexpress.com/item/Hot-Sale-110-220V-40W-200-300mm-Portable-CO2-Laser-Engraver-Cutter-Engraving-Machine-3020-Laser/32461580904.html](http://www.aliexpress.com/item/Hot-Sale-110-220V-40W-200-300mm-Portable-CO2-Laser-Engraver-Cutter-Engraving-Machine-3020-Laser/32461580904.html)




---
**HP Persson** *February 22, 2016 22:15*

Laser fired up, water pump changed to a bigger (safer?) one, air system rebuilt, bed squared up, mirrors aligned and all electric connection checked, some soldered and re-crimped :)



**+Peter van der Walt** thanks for the idea about a small pipe, picked up some copper brake lines today (5mm) and grabbed one of my air pumps from the fish tank, probably too small but it´s a start :)

Tomorrow is the first engraving/cutting test!


---
**Bharat Katyal** *February 29, 2016 01:49*

Wow I'm impressed with the overall quality. This manufacture really went out of there way. 


---
**HP Persson** *March 05, 2016 21:49*

Here is what i did with air assist. Not tried it yet so dont know if it works really good, or just good :)



cant reply with pics, so here comes 2 URLs ;)



[http://wopr.nu/laser/airassist1.JPG](http://wopr.nu/laser/airassist1.JPG)

[http://wopr.nu/laser/airassist2.JPG](http://wopr.nu/laser/airassist2.JPG)


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/T1vxkUXm7M6) &mdash; content and formatting may not be reliable*
