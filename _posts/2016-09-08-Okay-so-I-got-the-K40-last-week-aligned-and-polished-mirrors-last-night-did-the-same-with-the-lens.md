---
layout: post
title: "Okay, so I got the K40 last week, aligned and polished mirrors, last night did the same with the lens"
date: September 08, 2016 13:26
category: "Hardware and Laser settings"
author: "J.R. Sharp"
---
Okay, so I got the K40 last week, aligned and polished mirrors, last night did the same with the lens. 



I just took the bed out, trim the duct and insert some mesh with steel rulers as a 0 position. However, I did some test cuts last night with some .2" plywood. After 4 passes at various power settings, it did not go through. I thought perhaps it was too thick, so I went to 3mm plywood...same result. 



Help!





**"J.R. Sharp"**

---
---
**greg greene** *September 08, 2016 13:34*

power and speed and focus - what were your settings?


---
**J.R. Sharp** *September 08, 2016 14:03*

I tried all power and speed, anything from 5 to 10, at 30mm/s all the way up to 400mm/s at full power. I think part of it is focal, which I am going to work towards upgrading the bed. 


---
**greg greene** *September 08, 2016 14:11*

Try 8 ma at 10mm/sec - are you using the air assist head?  If so, make sure the beam is not hitting the sides of the nozzle


---
**Jim Hatch** *September 08, 2016 14:13*

Focus is likely the issue. You need to have the material 50.8mm from the lens. Or if you want to tune it precisely, 50.8mm less 1/2 the thickness of your material. That will put the narrowest portion of the beam in the middle of your material and equalize the cut throughout. But for general purposes for 3 or 6mm plywood you can just measure from the surface to the lens.



Make sure the measurement is to the lens and not to the lens holder body, nozzle or something else. If you've got a stock lens holder that's probably the bottom of the lens holder but if you've got an air assist it's actually somewhere in the lens tube.


---
**Ariel Yahni (UniKpty)** *September 08, 2016 14:15*

**+J.R. Sharp**​ follow this guide if you haven't yet.  [https://plus.google.com/+ArielYahni/posts/ho5JAUMgTmE](https://plus.google.com/+ArielYahni/posts/ho5JAUMgTmE)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 08, 2016 15:41*

Also note, you may have put your lens in upside down or even it was shipped upside down (like mine was). The convex side goes up.


---
**Ian C** *September 08, 2016 17:08*

Hey buddy. Have you checked your alignment since refitting the bed? I took mine out to trim the air extract and then when I put it back, it was way out of alignment. All other ideas will cause issues to, but I've not seen mention of you checking the laser alignment to all 3 mirror since refitting the bed so thought I'd mention it


---
**J.R. Sharp** *September 08, 2016 23:17*

I'm doing all of this as we speak. I did learn that my tube was about 1/4" out of level with the chassis, now that I've tuned that, I have to redo all of the mirrors now. Gonna be a long night.




---
**Ariel Yahni (UniKpty)** *September 08, 2016 23:19*

Maybe longer


---
**greg greene** *September 09, 2016 00:04*

Ah - but the morning will dawn with new understanding and new horizons to conquer, the journey cannot begin until the ship is ready to sail.


---
**J.R. Sharp** *September 09, 2016 06:07*

Alignment is done and I made an adjustable bed. 3 passes on 3mm Baltic at 15mm/s and 10mw will now cut through. No hope yet on the .2 in stuff.


---
**Jim Hatch** *September 09, 2016 12:20*

Did you check your lens? Bump side up?


---
**J.R. Sharp** *September 09, 2016 14:24*

It's a very small bump, but yes.


---
**Jim Hatch** *September 09, 2016 16:33*

And you're sure you're 50.8mm from the bottom of the lens to the top of the material?


---
**Jim Hatch** *September 09, 2016 16:34*

Bump the power to 15ma and drop the speed to 10mm/s.


---
**Jim Hatch** *September 09, 2016 16:35*

Full safe power on these is 18ma so you don't shorten the life of the tube.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 20:04*

I have just fixed my focal point on my machine & can cut 3mm plywood in 3 passes @ 6mA @ 20mm/s with a very thin cut line.


---
**greg greene** *September 09, 2016 20:05*

Sounds like your closing in on success !


---
*Imported from [Google+](https://plus.google.com/116586822526943304995/posts/7JR55TNx6H4) &mdash; content and formatting may not be reliable*
