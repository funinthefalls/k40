---
layout: post
title: "I will be starting the upgrade of my 60W 5040 laser this weekend"
date: October 06, 2017 23:14
category: "Modification"
author: "Anthony Bolgar"
---
I will be starting the upgrade of my 60W 5040 laser this weekend. Out with the nano board and in with the C3D mini board. Will be adding a GLCD, interlock switches for the doors, and a temp sensor for the cooling water.There is already a flow sensor installed at the factory. I need to create a tube extension cover to protect the 150mm of tube sticking out of the case now that I upgraded the 50W tube to a 60W tube. A little creative shopping at Lowes should take care of this. Should be a fairly easy upgrade as the electronics are the same as the K40, nothing out of the ordinary to deal with.I will document this upgrade as much as possible so that others who want to attempt the upgrade can follow along. WIll be using this laser as my production machine so I want everything to be as solid as possible, both on the hardware and software side of things. My tool chain will be CorelDraw for design and LightBurn for control. Nice thing about LightBurn is that it has a built in vector graphics editor for creating simple projects as well as editing files on the fly. If anyone has any questions or upgrade ideas, feel free to post them here. And to all my Canadian friends, have a great Thanksgiving weekend.





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *October 10, 2017 17:25*

A little behind on this project, my daughter is in the hospital right now, so she takes precedence over this :)




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/8C3RPSHKQRY) &mdash; content and formatting may not be reliable*
