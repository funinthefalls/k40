---
layout: post
title: "Thinking about a z axis mod and wondering if it's more feasible to motorize the table/bed or the lens"
date: August 15, 2018 08:52
category: "Modification"
author: "'Akai' Coit"
---
Thinking about a z axis mod and wondering if it's more feasible to motorize the table/bed or the lens. Conceptually, I think the lens makes more sense in that there's less physical material to move. But I figure I'd ask to see what others think on this.





**"'Akai' Coit"**

---
---
**Don Kleinschnitz Jr.** *August 15, 2018 10:01*

Depends a lot on how the range you want the bed to move. How big & small of an object do you want to put under the head and what lens FL do you want to use for those materials.



Moving the head seems easier but small changes in mechanical positioning at the head can cause a larger optical misalignment. I would not try to move the head but the table. Moving the table up-down is not prone to much misalignment. 



Unlike milling machines the laser normally needs to move the Z to load material and focus. That said slightly change focus dynamically could create some unusual 3D effects.



Then again if you had a reliable way (not easy or cheap) to move the head you may gain the space a lift consumes in the compartment. 



Most people that want to etch/cut thicker/larger materials cut the bottom and front out of the machine and put the lift below the machine.


---
**Adrian Godwin** *August 15, 2018 13:23*

**+Don Kleinschnitz** I'm not sure it would add much error as the lens is already moving in 2d and this would be quite a short axis. However, the actuator would be an extra mass to add to the head - it's fairly critical there for speed whereas you can put anything to lift the table. I like the idea though - especially for small changes such as material thickness rather than placing large objects in the box. Maybe the mechanism from an automatically telescoping digital camera lens would be suitable.


---
**James Rivera** *August 15, 2018 17:08*

I already modded my bed (slightly, not moveable) and bought the lens/holder upgrade kit from LightObject.com, but if I had to do it again I'd probably get something like this:

[ebay.com - Details about Co2 Laser Head Mounts Mirrors Lens Integrative Set for Engraver Cutter FL: 1- 5"](https://www.ebay.com/itm/Co2-Laser-Head-Mounts-Mirrors-Lens-Integrative-Set-for-Engraver-Cutter-FL-1-5/222368277605?var=521236051513&_trkparms=aid%3D222007%26algo%3DSIM.MBE%26ao%3D2%26asc%3D52885%26meid%3De474587c5009496fabe12380b494140d%26pid%3D100005%26rk%3D4%26rkt%3D7%26sd%3D150896487394%26itm%3D521236051513&_trksid=p2047675.c100005.m1851)


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/cJ6GKbGxkUk) &mdash; content and formatting may not be reliable*
