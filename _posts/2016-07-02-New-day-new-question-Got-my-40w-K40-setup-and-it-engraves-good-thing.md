---
layout: post
title: "New day, new question. Got my 40w K40 setup and it engraves (good thing)"
date: July 02, 2016 23:59
category: "Hardware and Laser settings"
author: "Terry Taylor"
---
New day, new question. Got my 40w K40 setup and it engraves (good thing). Is there a chart somewhere that shows a correlation between the type of material, cutting speed and laser current? Also, I am engraving a piece of 1/8" Luan plywood (still running) and I see "smokey" areas around cut lines. Is that indicative of too much current, too slow a speed or will they just wipe away when I am done?



Sorry for the newb questions, but y'all have been so helpful.....





**"Terry Taylor"**

---
---
**Ariel Yahni (UniKpty)** *July 03, 2016 00:17*

**+Terry Taylor**​ look in this community for a poll I did not so long ago.  You will see many different setting for the same. That will tell you that is very difficult to have that kind of data. Also related is the smokey you see. Faster cut will leave less smokey but cuting that fast it's very difficult, so optional are: air assist and masking tape. Air assist will bring heat / flame down. Masking tape with prevent smokey a little


---
**Terry Taylor** *July 03, 2016 00:26*

OK, I will try to find the poll. I have an air source for air assist, just have not gotten it set up yet.


---
**Ariel Yahni (UniKpty)** *July 03, 2016 00:54*

[https://plus.google.com/+ArielYahni/posts/18ik5cWrScZ](https://plus.google.com/+ArielYahni/posts/18ik5cWrScZ)


---
**3D Laser** *July 03, 2016 01:05*

**+Terry Taylor** it really depends on your set up to be honest air assist helps a lot and if you take the time to get it lined up correctly and have your focal length spot on you can cut great with less energy and faster speed.  If you have a bit of a sloppy set up its going to take more power and less speed to cut the material and you will get a lot of burn marks 


---
**Jim Hatch** *July 03, 2016 01:48*

The smoke can be easily removed with a few passes of a random orbit sander & 220 grit sandpaper. You can also wash it by wiping with alcohol. 



I prefer the sanding if I'm going to finish with poly or something as it removes any smoke dust (vs smoke trails) from any cuts.



Blue painters masking tape is a good masking material and eliminates the staining.


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/WEbjDdpwEga) &mdash; content and formatting may not be reliable*
