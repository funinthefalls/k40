---
layout: post
title: "I have a question ... I have a 50w co2 power supply that sits.."
date: February 27, 2017 23:37
category: "Discussion"
author: "Kostas Filosofou"
---
I have a question ... 



I have a 50w co2 power supply that sits.. and I am thinking to fit a 60w laser tube (1000mm long) ..The prizes is really low at this moment in Germany... is it possible ? or I damage the psu? 





**"Kostas Filosofou"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 27, 2017 23:50*

Seeing as how those k40 psu like to pop with the slightest over current draw, I would think it is better to have a psu that can handle a much larger wattage. 


---
**Don Kleinschnitz Jr.** *February 27, 2017 23:57*

Unfortunately you need a 60W to get full power and not kill the supply.  I think you can get a 60-80 watt supply.


---
**Ray Kholodovsky (Cohesion3D)** *February 27, 2017 23:59*

Don, these don't have to be fitted to the tube size right?  So I could have a 100w PSU run a 60w tube?


---
**Kostas Filosofou** *February 27, 2017 23:59*

**+Ray Kholodovsky** you have a point...I blew mine in the first few hours(40w stock)...But.. I don't know if for example change some parts to the PSU can make it more durable.. maybe **+Don Kleinschnitz**​ enlighten this.


---
**Kostas Filosofou** *February 28, 2017 00:01*

**+Don Kleinschnitz**​ you are fast :)


---
**Don Kleinschnitz Jr.** *February 28, 2017 00:51*

The voltage requirements goes up with the length, so does the power so It does matter if it's undersized.

For a larger than needed supply. 

I am not sure if too high a voltage will matter or if you can turn it down. Never had a larger supply.




---
**Don Kleinschnitz Jr.** *February 28, 2017 00:56*

**+Konstantinos Filosofou** as you may know I am collecting dead supplies to  analyze failures and see if we can improve them.

So far I suspect that there are 3 types of failures: flyback, bridge rectifier and power transistors. Flyback may be the most frequent. 


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/CYzVFyUGRYQ) &mdash; content and formatting may not be reliable*
