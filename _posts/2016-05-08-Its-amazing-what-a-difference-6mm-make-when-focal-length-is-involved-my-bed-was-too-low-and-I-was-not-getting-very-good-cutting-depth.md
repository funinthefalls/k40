---
layout: post
title: "It's amazing what a difference 6mm make when focal length is involved, my bed was too low and I was not getting very good cutting depth"
date: May 08, 2016 04:27
category: "Discussion"
author: "ben crawford"
---
It's amazing what a difference 6mm make when focal length is involved, my bed was too low and I was not getting very good cutting depth. I need to work on making an adjustable laser bed so I can get the best cutting out of my k40.





**"ben crawford"**

---
---
**Phillip Conroy** *May 08, 2016 06:37*

I just use magnets holding  up screws ,i only cut 3mm mdf and use 6 screw/magnet combos to hold my work at correct hight,these act like a pin bed and with air assist my cuts are very clean top and bottom and work peace just drops down when finished7


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 06:58*

Yeah I had mine 4mm too low when I replaced the original bed & couldn't cut through anything at all. Phillips idea is a good one & a bonus is when the work piece drops you know it is cut through. No need for any more passes after that.


---
**Phillip Conroy** *May 08, 2016 07:32*

I also have a metal tray that is removable in the cutting area with 80mm high sides to 1 stop parts from dissapearing into the hard to reach areas of the k40 and 2 so that i can remove it to clean off the sticky buildup from the mdf fumes-which i have to do sbout every 20 hours cutting


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 07:39*

**+Phillip Conroy** Yeah I've noticed that sticky buildup from mdf on my perforated gal steel board that I use as my main cutting bed. Scrubbed it off the other day with some degreaser & brush. I also seem to get that residue (or something similar) from plywood.


---
**Jim Hatch** *May 08, 2016 13:40*

**+Phillip Conroy**​ If you cut a lot of MDF check your exhaust system (hose, fan, etc) regularly. MDF creates a lot of very fine residue that can settle in your exhaust run. We don't cut regular MDF on the big laser because of that. Also some nasty glues used (you see that with plywood too) so don't go breathing it. There is laser grade MDF out there but the lumberyard won't carry it.



It's too bad because it cuts so clean and consistently.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 18:51*

**+Jim Hatch** I rarely cut MDF anymore, as I hate it as a material. Just use it occasionally for "test" pieces due to it's cheapness compared to other materials & similarity in cutting/engraving effect (e.g. leather). Gives a good idea of what the result will be on the leather. Is there a way you clean your exhaust run etc to remove the MDF residue & the likes or just something simple like hosing out the exhaust tube with some soap?


---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/6o8r92oy51H) &mdash; content and formatting may not be reliable*
