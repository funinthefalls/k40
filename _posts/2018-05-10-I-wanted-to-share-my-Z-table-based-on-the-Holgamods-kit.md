---
layout: post
title: "I wanted to share my Z-table based on the Holgamods kit"
date: May 10, 2018 10:46
category: "Discussion"
author: "Jake B"
---
I wanted to share my Z-table based on the Holgamods kit.  I started with Randy's original kit but wound up re-engineering a few things:



1. Randy's kit is designed to be accessible for those with varying equipment.  The parts are all designed to be "loose" and glued together square and flat to adjust for variations in cutting.  I milled my tubes within 0.1mm of their proper length, so I re-printed the corners with tighter fitting press-fits.  Also, I use two skate bearings in each corner instead of a skate bearing and a bushing.



2. I replaced the idler pulleys with F695ZZ bearings like many reprap makers use.  The standard idlers weren't very smooth and don't have a spacer between the bearings, so you can "crush" them when tightening down.



3. I kept the bottom flat (no nuts sticking out) by 3d printing nut plates that slide down the tubes to hold the nuts inside the tubes.



4. Randy uses a beefy NEMA17 motor that involves cutting a hole in the bottom of the machine for the motor to stick out.  I wasn't quite ready to do this, so I tried a 20mm NEMA17 motor.  To compensate, I increased the timing pulleys on each corner to 40T to get a little mechanical advantage.  It can't lift the 20lbs Randy's can, but it can do about 8lbs as seen in the video.



I'm running it off the Z-axis of my Cohesion3D board wit a standard on-board driver at a little less than 1A.  The motor gets warm, but not hot when holding the position.



I like Randy's kit quite a lot, and I'm looking forward to playing with it more.

![images/dd79ad879ac0ccc5d23a55fc3574cc97.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dd79ad879ac0ccc5d23a55fc3574cc97.jpeg)



**"Jake B"**

---
---
**Jerry Price** *May 10, 2018 11:34*

very nice


---
**Fabiano Ramos** *May 12, 2018 14:21*

Share the link please. I will considerate upgrade my k40 in a near future.




---
**Jake B** *May 12, 2018 14:29*

You can contact Randy at his website Holgamods.com.  His business is primarily cameras, so I don't believe he has this on his website.



His kit contains the 3D printed parts, pulleys, belt, bearings, bushings, leadscrews, endstop, and motor.  You need to provide the 1" square aluminum extrusion and plate for the table.  You'll also need a motor controller suitable power source, not included.  (Again, the 3D printed parts he provides are based on his design, not on my modified design pictured above, of course.)



His design requires a hole in the bottom of the machine for the motor to stick out.  Also, you'll need to have cut back the vent duct as well (a common modification that most do already).



Randy's also very willing to chat on the phone about his kit and how to assemble it.  He's very helpful.  Even though I re-engineered most of it to my own specifications, I don't regret buying the kit as Randy figured out a bunch of the nuances already, so I didn't have to learn those lessons myself.



More info here: 
{% include youtubePlayer.html id="3XWOvtc_Jo8" %}
[https://www.youtube.com/watch?v=3XWOvtc_Jo8](https://www.youtube.com/watch?v=3XWOvtc_Jo8)

[holgamods.com - Holgamods.com](http://Holgamods.com)


---
**Fabiano Ramos** *May 12, 2018 18:42*

**+Jake B**  thx for information




---
**salvatore sasakingsoft** *May 13, 2018 16:18*

ciao oltre al motore nema17 cosa usi x manovrare la salita e la scesa?


---
**Jake B** *May 13, 2018 19:37*

In this video, I have an Arduino with a stepper motor shield was used for testing.  On my actual K40, I am using the Z motor controller on the Cohesion3D board.  I had to set the current a little higher, but it is less than 1A (and the 20mm NEMA17 has about a 1A limit anyway, so I'm a little under max current for the motor). I use the "jog" function in the smoothie menus to move the stage manually when needed.


---
**James poulton** *May 14, 2018 10:25*

Very nice.. 


---
**James poulton** *May 14, 2018 10:27*

What kind of weight can this hold?


---
**Jake B** *May 14, 2018 14:22*

**+James poulton** Randy's kit comes with a 96 oz-in stepper that can lift 20 lbs based on what I've seen him claim.  You need to cut a hole in the bottom (and possibly have taller feet on the laser) to have clearance for this motor.  I'm using a much smaller 18.4 oz-in stepper.  I also increased the pulley diameter on each corner to create some mechanical advantage.  With my setup it can lift 8 lbs.  After a little more than that, it starts skipping steps.


---
**Nitro Zeus** *May 16, 2018 16:07*

Could you find a pull system on thingiverse so give you more clearance, and possibly torque?


---
**Nitro Zeus** *May 16, 2018 16:08*

Also very cool


---
**Jake B** *May 16, 2018 20:44*

**+Nitro Zeus** its possible I guess.  You could probably go up a little more without extra linkages, but any more than 40T on the leadscrews and the pulleys will start to extend over the corners and frame.  Not the end of the world, it probably could be made to fit.  You could also put a 18T on the stepper motor, but I didn't do this because it would be a weirder 0.45 ratio, but again, not unworkable.  Anything more than this, and you'd be getting into multiple belts to do multiple reductions.  Not impossible, but I'm not sure its really needed.




---
**James poulton** *May 17, 2018 08:30*

**+Jake B** cheers Jake. 


---
**Eric Lovejoy** *June 04, 2018 01:58*

sexy




---
**LA12 Sports Images LA12 Sports Images** *June 19, 2018 10:57*

Nice work - my table will be here in a couple of weeks and this is a mod I want to make, I intend to run it straight from the C3D Z port and use the jog function as you are doing - how many teeth did you go up to on the leadscrews, 40T GT2's  ?


---
**Jake B** *June 19, 2018 17:02*

**+LA12 Sports Images LA12 Sports Images** the corners are T40s and the motor a T20 if I recall. The leadscrews are a standard 2mm pitch.  Randy had them made to a specific length as 100mm is too tall and can hit the carriage  or air assist nozzle


---
**LA12 Sports Images LA12 Sports Images** *June 19, 2018 19:30*

**+Jake B** Thanks 


---
*Imported from [Google+](https://plus.google.com/101176403058148207601/posts/gw4McAs8pqx) &mdash; content and formatting may not be reliable*
