---
layout: post
title: "Stephane Buisson , are you still using Visicut, and if so, does it play nicely with a smoothiebaord?"
date: March 09, 2017 22:40
category: "Software"
author: "Anthony Bolgar"
---
**+Stephane Buisson**, are you still using Visicut, and if so, does it play nicely with a smoothiebaord?





**"Anthony Bolgar"**

---
---
**Stephane Buisson** *March 09, 2017 22:52*

**+Anthony Bolgar** my k40 is in storage for some time now, I will take it back to life as soon as my new Makerspace (in south Italy) will be ready to receive it (about 2 weeks). Then I will put in my C3D mini in, and test it with Visicut as promised (can't see any reason it won't). 

But yes last time I used  my K40 (in London) was with  Visicut & Smoothie X4 and all was running fine.

And I can't wait to test LW4 as an Mac app.

Just hope my K40 survive the road trip 2500 Km without issue. 




---
**Anthony Bolgar** *March 09, 2017 23:52*

I have had some intermittent connection errors with visicut. Is there a specific version that works the best with a Smoothieware based board? Because I really like the ease of importing a drawing that contains vectors, rasters, and fonts all in one file, and just assigning colors to the different elements to set operations like cut or engrave. Sure beats having to take the original file and save it in 4 or 5 or more configurations and then having to import it 5 or 6 times to set the various operations for each element. I also like the great material and machine database that it has.


---
**Stephane Buisson** *March 10, 2017 09:43*

**+Anthony Bolgar** ethernet or usb ?

I run jobs with ethernet. and i didn't update visicut for a long time. (>1y).



maybe ask **+Thomas Oster** how he can help you (knowing you will share your results with community).


---
**Anthony Bolgar** *March 11, 2017 03:23*

Thanks **+Stephane Buisson** I will try to get in touch with **+Thomas Oster** and work out the minor issues. If you are interested in CNC CAM software, you should check out Estlcam V10 by [plus.google.com - Christian Knüll](https://plus.google.com/u/0/102102837336804755419)




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/L3Hzj3kVnJq) &mdash; content and formatting may not be reliable*
