---
layout: post
title: "Gotta love Japanese art"
date: June 04, 2016 22:55
category: "Object produced with laser"
author: "Scott Thorne"
---
Gotta love Japanese art.

![images/9bf3620e040942288a6087250eac5b72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9bf3620e040942288a6087250eac5b72.jpeg)



**"Scott Thorne"**

---
---
**Anthony Bolgar** *June 04, 2016 23:14*

Nice.


---
**Scott Thorne** *June 04, 2016 23:22*

Thanks **+Anthony Bolgar**.


---
**Anthony Bolgar** *June 04, 2016 23:34*

What kind of wood are you using to engrave on?


---
**Scott Thorne** *June 04, 2016 23:52*

**+Anthony Bolgar**...alder wood planks.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 00:06*

The definition of colour (or shades of black) is really quite high. I count at least 8 different shades in that engrave. Amazing. What's your file preparation method Scott?


---
**Scott Thorne** *June 05, 2016 00:25*

**+Yuusuf Sallahuddin**...I'm editing in paint shop pro.....resize then edit in grayscale histogram....on my machine 70 through 210 in the grayscale histogram works best if I stay in that range...over 210 is to dark ....under 70 is too light.


---
**Scott Thorne** *June 05, 2016 00:35*

The problem is that I've been using birch plywood trying to get the settings right on my machine...not knowing that no settings will work well doing photos on birch...it won't raster dark colors well....it either burns or is too light...alder wood is the way to go....I've had good luck with clear acrylic too


---
**Scott Thorne** *June 05, 2016 01:13*


{% include youtubePlayer.html id="O7vo7GPSevk" %}
[https://youtu.be/O7vo7GPSevk](https://youtu.be/O7vo7GPSevk)


---
**Brandon Smith** *June 05, 2016 01:43*

Would you mind pointing us to the alder wood planks you are buying specifically? I would love to see your settings as well. The pics have been really great to see.


---
**Scott Thorne** *June 05, 2016 01:48*

**+Brandon Smith**...amazon is where I buy the Alderwood....they are cooking planks...settings are 300mm/s at 35% power.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 02:24*

**+Scott Thorne** Thanks Scott. Yeah, I've found that every time I change materials to play around with I end up having to tweak settings again for suitability with that material. I haven't used PSP since around 2000, so I will have to look into what it's functions do then compare to Photoshop. I'm fairly certain I should be able to do the same with histogram/levels in Photoshop.


---
**Ariel Yahni (UniKpty)** *June 05, 2016 02:30*

**+Scott Thorne**​ can you post the image after adjustments.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 02:32*

**+Ariel Yahni** **+Scott Thorne** Before & after adjustments would be nice to see.


---
**Scott Thorne** *June 05, 2016 02:38*

I've got photoshop also...you can achieve the same results with it.


---
**Scott Thorne** *June 05, 2016 11:23*

**+Ariel Yahni**...sure


---
**Tony Schelts** *June 05, 2016 14:33*

Is this quality possible on the k40?


---
**Scott Thorne** *June 05, 2016 14:36*

**+Tony Schelts**...I would have to say yes. 


---
**Tony Schelts** *June 05, 2016 14:39*

I quess coreldraw or photo should provide the same result.


---
**Scott Thorne** *June 05, 2016 14:43*

They will...just take time and some scrap wood....one thing very important....once the image had been prepared you can't resize it without screwing up the image....it must be resized first then edited.


---
**Tony Schelts** *June 05, 2016 14:50*

good to know thanks.


---
**Pippins McGee** *June 06, 2016 13:52*

Hi Scott,

would you mind sharing your wisdom on photo prep before engraving?



I am well versed in Photoshop - just not sure of what is required to make an image as ready for engraving as yours are whilst keeping so much detail.



Please advise of the steps you take?

Be much appreciated.


---
**Scott Thorne** *June 08, 2016 10:34*

**+Pippins McGee**...I add a post when I get off work today. 


---
**Pippins McGee** *June 14, 2016 07:56*

**+Scott Thorne** hey scott any chance you had some free time to write up your file prep wisdom? :)


---
**Pippins McGee** *July 15, 2016 00:38*

**+Scott Thorne** hey scott did you get stuck in some machinery at work? that's a mighty long shift you're working there!


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/JNhs88eccMQ) &mdash; content and formatting may not be reliable*
