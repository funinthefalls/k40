---
layout: post
title: "I am wanting to get a dual purpose machine for cnc and 3dprint, I found one that was way up there in price and I am not wanting to spend a ton"
date: March 15, 2016 03:08
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
 I am wanting to get a dual purpose machine for cnc and 3dprint, I found one that was way up there in price and I am not wanting to spend a ton. I am good with getting separate machines if that is the better way to go. Any suggestions?





**"Andrew ONeal (Andy-drew)"**

---
---
**Alex Krause** *March 15, 2016 03:30*

I have been keeping my eye on this to run on my OX cnc [http://3dsupplysource.com/L-Cheapo](http://3dsupplysource.com/L-Cheapo) 


---
**Anthony Bolgar** *March 15, 2016 06:23*

3D printers are designed with very lightweight components. A decent CNC machine is built using much stronger components, as the forces involved are much greater. 3D printers are capable of having laser diodes attached to them (I personally find this very unsafe) but not a decent CNC spindle for the cutting bits, unless all you are doing is light engraving on something. My recommendation would be to buy separate machines.


---
**Stephane Buisson** *March 15, 2016 09:46*

one machine would be a expensive bit of kit with poor performance on each functions.

2 machines (could perform at once), could be cheaper and well designed for the tasks. I am to newbee to advised on CNC (I am just into OX), Prusa I3 design (plenty of compatible) is one of the most economical design for a 3D printer.


---
**HalfNormal** *March 15, 2016 17:30*

Multi-machines never tend to work as well as a single purpose machine. You will find that both 3D print and CNC take time to produce their product so you could be using one while working on the other.


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/PRHJPU2UXhX) &mdash; content and formatting may not be reliable*
