---
layout: post
title: "\"Little Prince\" images etched on ~6.5\"x8.5\" plaques"
date: October 29, 2016 06:03
category: "Object produced with laser"
author: "Tev Kaber"
---
"Little Prince" images etched on ~6.5"x8.5" plaques. A present for a friend to put in her kids' rooms. If I have time I'll stain and seal them. 400 mm/s 6mA. 



I sanded after etching to clean up the edges (detail shown). 



![images/9e9a0dfd53d3891c26861fc0836e2ae9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e9a0dfd53d3891c26861fc0836e2ae9.jpeg)
![images/aafebf29c89bc7b46f2f61165aae013c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aafebf29c89bc7b46f2f61165aae013c.jpeg)
![images/3bc5116414c35539ead5f653d890691b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3bc5116414c35539ead5f653d890691b.jpeg)
![images/65fb0e8f52103eaaa31d6148bcb0e46f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65fb0e8f52103eaaa31d6148bcb0e46f.jpeg)

**"Tev Kaber"**

---
---
**Jonathan Davis (Leo Lion)** *October 29, 2016 06:13*

Neat pieces but would creating them even be legal since it is a Netflix owned brand?  


---
**Tev Kaber** *October 29, 2016 06:23*

**+Jonathan Davis** Not sure Netflix owns the rights to original book illustrations, but they would be under copyright since it was published in 1943. I'm not too concerned with copyright though, since I am giving it as a gift and not selling it. 


---
**Vince Lee** *October 29, 2016 17:56*

FWIW, copyright varies by country.  The original book in French appears to be in the public domain except France and the USA, while subsequent translations may not be.   The cover illustration is in the public domain everywhere except France according to this link (guessing other illustrations are similar): [en.m.wikipedia.org - File:Littleprince.JPG - Wikipedia](https://en.m.wikipedia.org/wiki/File:Littleprince.JPG)


---
**K** *October 30, 2016 01:16*

Those are great. That's one of my favorite books. You might consider submitting this to the official Little Prince Facebook page. They post fan art on Fridays.


---
**Jonathan Davis (Leo Lion)** *October 30, 2016 15:04*

Ok then


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/LaEhB3ywtHu) &mdash; content and formatting may not be reliable*
