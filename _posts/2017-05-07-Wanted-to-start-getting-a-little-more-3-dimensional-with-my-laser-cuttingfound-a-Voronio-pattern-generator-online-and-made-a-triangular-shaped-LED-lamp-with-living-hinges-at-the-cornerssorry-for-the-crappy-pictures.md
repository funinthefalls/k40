---
layout: post
title: "Wanted to start getting a little more 3 dimensional with my laser cutting...found a Voronio pattern generator online and made a triangular shaped LED lamp with living hinges at the corners...sorry for the crappy pictures -"
date: May 07, 2017 14:26
category: "Object produced with laser"
author: "Mike Meyer"
---
Wanted to start getting a little more 3 dimensional with my laser cutting...found a Voronio pattern generator online and made a triangular shaped LED lamp with living hinges at the corners...sorry for the crappy pictures - old cellphone...



![images/fe8f2937c3b81f5d49a25775c2fe512e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe8f2937c3b81f5d49a25775c2fe512e.jpeg)
![images/bfe9f5b4478d08695d8240158cd09a00.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bfe9f5b4478d08695d8240158cd09a00.jpeg)
![images/04bd12346f70e8ad433d10c1bc21a4be.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/04bd12346f70e8ad433d10c1bc21a4be.jpeg)
![images/2ee4262b592635fb6e09490c180e9492.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2ee4262b592635fb6e09490c180e9492.jpeg)

**"Mike Meyer"**

---
---
**Ned Hill** *May 07, 2017 14:31*

That's very cool.  Nice job :)




---
**Ned Hill** *May 07, 2017 14:34*

How is the base made?


---
**Mike Meyer** *May 07, 2017 14:39*

Thanks! Appreciate the kind words! The base is just cheap rejoined pine that I had lying around...I used 3mm russian birch on the rest. 


---
**Ned Hill** *May 07, 2017 14:47*

You could even maybe add some frosted acrylic panels to the inside to disperse the light more.  Just a thought.


---
**Mike Meyer** *May 07, 2017 14:52*

Yeah, thought about that but after messing around with the g4 led, I decided on sandblasting a plastic shot glass from the 99 cent store as my diffuser...it's still a work in progress, however...


---
**Mark Brown** *May 07, 2017 20:01*

Source for the Voronio generator?


---
**Mike Meyer** *May 07, 2017 21:16*

There are many on the internet...this is but one:

[raymondhill.net - Javascript implementation of Steven J. Fortune's algorithm to compute Voronoi diagrams](http://www.raymondhill.net/voronoi/rhill-voronoi.html)


---
**Mark Brown** *May 07, 2017 22:37*

Thank you.


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/Mm2MghsLStF) &mdash; content and formatting may not be reliable*
