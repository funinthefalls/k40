---
layout: post
title: "Anyone has experience engraving these with a K40?"
date: June 30, 2016 00:32
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Anyone has experience engraving these with a K40? Is the cover SS or Alum? Is it imperative to use moly or Cermark? I may score a very big gig with the distributor to personalized them as corporate presents

![images/52096283e5b6f293b9f4f1d33df3eb72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52096283e5b6f293b9f4f1d33df3eb72.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Alex Krause** *June 30, 2016 02:43*

 what's the name brand?


---
**Ariel Yahni (UniKpty)** *June 30, 2016 02:54*

Bubba


---
**Alex Krause** *June 30, 2016 02:58*

I tried calling their customer service number but they are closed till tomorrow... I'm pretty sure they are stainless steel


---
**Ned Hill** *June 30, 2016 03:59*

The item descriptions on their website say stainless steel.


---
**Phillip Conroy** *June 30, 2016 04:13*

Try and drill a hole,if you can au


---
**Jim Hatch** *June 30, 2016 15:48*

If they're what's available here (U.S.) they're stainless steel and you'll need dry moly lube spray (or Cer/Thermark).


---
**HalfNormal** *June 30, 2016 17:38*

Remember that the consistency of the mark depends on the consistency of the spay applied. 


---
**Alex Krause** *June 30, 2016 17:39*

I have a stainless steel flask I want to engrave **+HalfNormal**​ any idea how much dry moly I need to apply?


---
**HalfNormal** *June 30, 2016 17:43*

**+Alex Krause**​ I have found that spraying a few light coats, waiting for it to dry between coats works the best. Practice makes perfect :-)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/UoAd2jFGA4k) &mdash; content and formatting may not be reliable*
