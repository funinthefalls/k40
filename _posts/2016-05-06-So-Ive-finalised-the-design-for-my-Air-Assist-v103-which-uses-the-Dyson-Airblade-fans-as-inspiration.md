---
layout: post
title: "So I've finalised the design for my Air Assist v1.03 (which uses the Dyson Airblade fans as inspiration)"
date: May 06, 2016 01:56
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I've finalised the design for my Air Assist v1.03 (which uses the Dyson Airblade fans as inspiration). Has dual air input, that funnels around each side to allow both sides of the air assist chamber to receive equal amounts of air. This then funnels through the interior & exits through the circle in the base on an angle to create a cone of air focused to 50.8mm from the base of the lens. The exit cone has been shrunk to minimise the exit area & hopefully increase the exit pressure whilst also hopefully minimising the required input airflow/pressure. We'll see how it goes after I get one printed off.



![images/cb0a040c1de19e010bf1be101c8f33c8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/cb0a040c1de19e010bf1be101c8f33c8.png)
![images/beb36f59fb03d50c5f2970fcbd1d882d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/beb36f59fb03d50c5f2970fcbd1d882d.png)
![images/a0254a07ffc2c98c8bd6b8f3212d15d5.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a0254a07ffc2c98c8bd6b8f3212d15d5.png)
![images/401fbb866f27860d15e9ef41dc719ca4.png](https://gitlab.com/funinthefalls/k40/raw/master/images/401fbb866f27860d15e9ef41dc719ca4.png)
![images/40b9325eaf0d05bc53f345e258f314c1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/40b9325eaf0d05bc53f345e258f314c1.png)
![images/607a85880ad3146ba96031a7ee46a067.png](https://gitlab.com/funinthefalls/k40/raw/master/images/607a85880ad3146ba96031a7ee46a067.png)
![images/1c7fa3927bbd7d08a9c99f7bdf18f40b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1c7fa3927bbd7d08a9c99f7bdf18f40b.png)
![images/fc39c97bf36dc3e74776e766a098ee2f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/fc39c97bf36dc3e74776e766a098ee2f.png)
![images/bfe366608553a8dd1199daf8a83cc0c5.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bfe366608553a8dd1199daf8a83cc0c5.png)
![images/948a2f408810c4dc5279c97581914895.png](https://gitlab.com/funinthefalls/k40/raw/master/images/948a2f408810c4dc5279c97581914895.png)
![images/7b0610816298d41a5783684598f71235.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7b0610816298d41a5783684598f71235.png)
![images/f4a8ef6269782b6c1799aca743a9d8e9.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f4a8ef6269782b6c1799aca743a9d8e9.png)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ariel Yahni (UniKpty)** *May 06, 2016 02:20*

I'm calling it Alien Starship


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 02:27*

**+Ariel Yahni** Hahaha, I like it. It does slightly look like some kind of alien ship.


---
**Donna Gray** *May 09, 2016 19:55*

Yuusuf Sallahuddin (Y.S. Creations) how does this air assist  attach to the laser head I know you have talked to me about it before if it was simple to attach and get working I may be interested in getting one off you


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 20:50*

**+Donna Gray** This one literally just slots over the existing lens mount. It's a perfect fit (design wise), but I will have to check it once it arrives (because I'm not 100% sure of the quality of this print). Also, I'll have to test if the design works as intended. Once it arrives, I'll take some photos & video of installation & tests of it in action to upload & share. Then if it is satisfactory we can sort that out. Total cost for the print was $31 including the shipping to GC (from Brisbane). Hopefully it all works as intended. I'll let you know when I know more.


---
**Ariel Yahni (UniKpty)** *May 09, 2016 20:54*

**+Yuusuf Sallahuddin**​ are you printing this on a special material or you don't have a 3D printer?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 20:54*

**+Donna Gray** I will take a photo of how the current one attaches (which is similar design on how it attaches) & post shortly.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 21:10*

**+Ariel Yahni** I don't have a 3d printer myself. But I used 3d Hubs service & got a guy semi-local to print it using a BrassFill filament (80% brass particles & 20% PLA). He used 100 microns layer slicing. It should arrive today or tomorrow (since it's only 70-80km away & was posted yesterday). I'll be able to give photos & the likes when it arrives.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 02:41*

Updated version: [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Ew93jx1Y4hv](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Ew93jx1Y4hv)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/B4FZcqTLGAT) &mdash; content and formatting may not be reliable*
