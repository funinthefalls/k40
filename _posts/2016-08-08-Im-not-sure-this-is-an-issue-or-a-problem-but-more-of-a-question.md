---
layout: post
title: "Im not sure this is an 'issue' or a problem, but more of a question"
date: August 08, 2016 13:32
category: "Original software and hardware issues"
author: "Bob Damato"
---
Im not sure this is an 'issue' or a problem, but more of a question.  I use my cutter a lot to make custom gaskets (mostly transaxle gaskets for GT40's) which are NLA. its awesome. 



But the question is, when I am ready to do a cut, I can never accurately tell where the design will fall on the material. I literally put in a piece of paper first and do a dry run (generally just the outside cut), then put in my gasket material. As they say in those cheesy infomercials, "There has to be a better way!".   



I have the cut preview on the screen and can drag it where I want but it isnt that accurate. When Im cutting an 8x9 gasket out of an 8x10 sheet, theres no room for error.  



Any tips much appreciated!

bob





**"Bob Damato"**

---
---
**Ariel Yahni (UniKpty)** *August 08, 2016 13:45*

I recommend you either do some fixed guides and match that to the head location in 0,0 or get yourself some led diodes for and create a crossmark that will show the exact location before starting the job


---
**greg greene** *August 08, 2016 13:49*

look for a led alignment holder for laser cutting on AliExpress - 17 bucks


---
**Derek Schuetz** *August 08, 2016 14:12*

Get a pin bed so that it can actually fall when it cuts troigh


---
**Bob Damato** *August 08, 2016 14:51*

I guess my issue is, where does the cut START.  Meaning I hit cut, then it brings up the preview and moves the cut head. But the cut head goes somewhere to the center (I have refer set to center so maybe thats why?).  Then when I actually hit 'starting' (hahaha I love that), it moves the head and starts somewhere completely different.



If I set refer to top left, then everything gets messed up as far as alignment goes when I cut different layers.


---
**Ariel Yahni (UniKpty)** *August 08, 2016 15:00*

I know that when you click on the screen while on the plugging that will be the origin


---
**Harvey Benner** *August 08, 2016 15:37*

I do what some other people in this group have mentioned in the past, I put a 1 pixel by 1 pixel square in the upper left of my drawing and leave that layer on. It gives the laser software a fixed reference so things don't shift when you turn the other layers on and off.


---
**Bob Damato** *August 08, 2016 16:16*

These are all great ideas, thank you. Seems like the 1x1 pixel layer would work great. Thank you.




---
**I Laser** *August 08, 2016 22:42*

Is 'do not back' checked in your preferences? 



The head shouldn't be at one point and then move when you start. I occasionally release the gantry and physically move it into position and then just click start and it starts from that position. 



Be careful though because doing this 'overrides' the limits. ie you can move the head to >150mm on the X axis and run a job that's 200mm wide... It will run, but will bash the head against the end of the gantry!



Just remember to click the reset button when you're finished.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/EPARvcSbWiR) &mdash; content and formatting may not be reliable*
