---
layout: post
title: "Fun with cheap colored pencils...I always thought vases turned out of a plug of colored pencils looked very cool...wanted to try and recreate something like it in a lasered sort of way...came up with this, an octagonal wooden"
date: January 08, 2017 03:25
category: "Object produced with laser"
author: "Mike Meyer"
---
Fun with cheap colored pencils...I always thought vases turned out of a plug of colored pencils looked very cool...wanted to try and recreate something like it in a lasered sort of way...came up with this, an octagonal wooden basket with epoxied colored pencil inlays...



![images/4f74e700051bcf2801c076fff78ba461.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f74e700051bcf2801c076fff78ba461.jpeg)
![images/38e8daeba2fb313434faf9aceb9568f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/38e8daeba2fb313434faf9aceb9568f6.jpeg)
![images/de6656fc6619402be0fc43f00304721d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de6656fc6619402be0fc43f00304721d.jpeg)

**"Mike Meyer"**

---


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/TBLq6uSMGE9) &mdash; content and formatting may not be reliable*
