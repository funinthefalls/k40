---
layout: post
title: "New to the discussion and new to the laser community"
date: February 23, 2016 21:08
category: "Discussion"
author: "Jason Reiter"
---
New to the discussion and new to the laser community. I just got my k40 a couple of weeks ago and I am learning . One of the problems I have is when useing Corel laser the laser speed is very slow no matter what speed I set it to it is the same slow speed. If I run the laser with LaserDRW then it is fine. Anyone have this problem?





**"Jason Reiter"**

---
---
**Patryk Hebel** *February 23, 2016 21:18*

I had the same problem. Check the in K40 board it's right where you connected the USB cable. There should be written ver and then set it in device settings in Corel Laser.


---
**Jason Reiter** *February 24, 2016 00:58*

That was it . Thank you!  So do you know how to keep the icons from disappearing when you start a cut?


---
**Tony Sobczak** *February 24, 2016 01:46*

They end up in the tray. It's a pain but at least they are still there. 


---
**Bob Steinbeiser** *February 24, 2016 14:45*

This is such a PITA!  The program notifications obscure the toolbar icon when it's running.  I sure wish there was a fix for the missing toolbar!


---
*Imported from [Google+](https://plus.google.com/106551755070259873093/posts/JqoM4md5iEF) &mdash; content and formatting may not be reliable*
