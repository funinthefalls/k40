---
layout: post
title: "A Petier cooling system build. I couldnt leave well enough alone Im rebuilding my coolant systemagain"
date: May 10, 2017 06:43
category: "Modification"
author: "Steve Clark"
---
A Petier cooling system build.

I couldn’t leave well enough alone I’m rebuilding my coolant system…again.  I have decided that it is impossible to keep the laser tube at less than 3 deg above ambient temp with my current one (See my earlier posts on cooling) .

 

In the next few weeks I’m going to post a series in building a Peltier cooling system and what I’ve researched  and led me to do this. Besides, it’s fun.



So how does this all work and what is the deal with the heat  caused by the tube? Here is what I’ve learned…



The C02 laser works by the collisional energy transfer that is between the nitrogen and the carbon dioxide molecule and this causes the carbon molecule to vibrate. This leads to a thing called “population inversion” ( the exciting of the nitrogen atom and carbon dioxide molecule’s electrons to move out of their normal vibratory path as molecules. During this “bouncing” the energy from helium atom momentarily and because of the current jumps to the carbon dioxide molecule giving it a higher state of unstable of energy…(my interpretation)). 



This leaves the nitrogen atom in a lower excited and unstable state, so they in turn ‘bump’ the cold helium atoms (that are in the tube also) so that they can go back to their normal state… which, in turn (I think because of their lower atomic weight) makes them unstably hot (the helium). So the now the hot helium has energy  has to be gotten rid of ( gotta dump that heat somewhere) so this ability that the nitrogen atom and the carbon dioxide molecule have to do a population inversion can happen again.



 Now what temp should our tube be working within in the realm of the current static temperature? And, what ambient temperatures are acceptable outside the tube during operation of it? Further problem, there is some argument that not only the temperature of the laser tube may affect performance,  but the internal dew point of the ambient temp could also… by causing  condensation on the surface of optical component inside the laser tube. Apparently there is a humidity issue also? Consequently causing it to quit working! So, not only can too hot cause damage… so can too cold.



It has been suggested by laser manufacturers that the tubes be kept within 20℃ - 31℃ and to avoid to high of indoor (ambient) temperatures by keeping them within  22℃ - 28℃ this allows for a differential to dump heat. I think the lower end at 20℃ is suggested to avoid the ‘internal’ mirror condensation.



The temperature outside the tube now seems to be of almost as much importance as the water temperature because of the possibility of the dewing on the tube’s mirrors. Things just got complicated!



At this point the components of cooling are:



Surface area



Circulation (flow rate minimum 2.5 to 3l/min)



Heat carrying capacity of the coolant



The coolants ability to not conduct current

 

The ability to dump heat out of the system based on amount generated



Control of outside influences (i.e. room temp)



What of these components can we do something about? Also should we try to manage all of these?





Other information I feel good to pass on:



“In the process of circulatory filling or water storing , low flow rate or bad flow will cause mode skip, laser beam would be scattered to form several dots and lead to a power decline; make sure the mouth of the water return pipe be kept in the middle of the cooling water tank. It should be avoided that the tube hasn’t be 100% filled up when turning on the laser. Turn off the laser when the cooling water flow rate is too low or the cooling water temperature is too high.”



A side note here…It’s pretty much a given from what I’ve read that when using C02 laser, a large or stationary gas cavity cannot be allowed to exist in the water-cooling tube, as it could result in explosion or cracks on the laser tube. I have a little one at the end of the tube that is about impossible to get rid of without turning laser on end. It’s about 5/32 in diameter and isolates’ around enough so it is never in the same spot. I live with it.



More to come in the next day or two…the build is ahead of what I’m posting here and I suck at writing and syntax so that is what slows me down. Pictures on the next post are of the actual build.  







**"Steve Clark"**

---
---
**Ned Hill** *May 10, 2017 11:11*

Thanks for the info.  I look forward to seeing what you come up with :)


---
**Nate Caine** *May 10, 2017 15:24*

Have you visited the tube manufacturer web sites?  What do <i>they</i> have to say?  (About tube temperatures, coolant, bubbles, etc.)    I know the English is sometimes confusing, but try to wade thru it.



Unless backed up with references, I'd tend to discount much of the random speculation collected from comments and forums off the internet.   Some of it is cleary wrong.


---
**Steve Clark** *May 12, 2017 15:30*

**+Nate Caine** Hi Nate I wrote you an answer but must not of posted it. I'll have to see if I can go back through my history and find some of them again. Sigh...



I'm in the middle of installing Cohestion3D right now.



But I do agree with you with respect to misinformation on the net.


---
**Steve Clark** *May 13, 2017 03:48*

Nate,These are a few of the references I read …I also looked a number of the authors reference on some of these.



[rp-photonics.com - Encyclopedia of Laser Physics and Technology - CO2 lasers, carbon dioxide laser](https://www.rp-photonics.com/co2_lasers.html)



[http://www.laserfocusworld.com/articles/print/volume-38/issue-7/features/ten-rules-to-guide-liquid-cooling-decisions.html](http://www.laserfocusworld.com/articles/print/volume-38/issue-7/features/ten-rules-to-guide-liquid-cooling-decisions.html)



[http://www.laserfocusworld.com/articles/print/volume-37/issue-6/features/instruments-accessories/keeping-your-laser-cool0151selecting-a-chiller.html](http://www.laserfocusworld.com/articles/print/volume-37/issue-6/features/instruments-accessories/keeping-your-laser-cool0151selecting-a-chiller.html)



[http://www.lytron.com/Tools-and-Technical-Reference/Application-Notes/Liquid-Cooling-a-Laser-for-System-Optimization](http://www.lytron.com/Tools-and-Technical-Reference/Application-Notes/Liquid-Cooling-a-Laser-for-System-Optimization)



I talked to the application guy at LightObjects when I was up there also.






---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/KXbXvWJnob1) &mdash; content and formatting may not be reliable*
