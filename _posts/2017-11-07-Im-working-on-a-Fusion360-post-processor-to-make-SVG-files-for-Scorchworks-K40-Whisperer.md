---
layout: post
title: "Im working on a Fusion360 post processor to make SVG files for +Scorchworks K40 Whisperer"
date: November 07, 2017 20:59
category: "Software"
author: "Lars Andersson"
---
Im working on a Fusion360 post processor to make SVG files for +Scorchworks K40 Whisperer.



For some reason K40 Whisperer draws the contours of my SVG file black and will not cut them. If I load the SVG file in Inkscape and resave it the contours come out red in  K40 Whisperer. What am I missing?



This is an postprocessor output sample:



<?xml version="1.0" encoding="UTF-8" standalone="no"?>

<svg xmlns="[http://www.w3.org/2000/svg](http://www.w3.org/2000/svg)" width="297mm" height="210mm" viewBox="0 0 297 210">

<g transform="translate(0, 210)">

<g transform="scale(1, -1)">

<g transform="translate(20.853, 3.875)">

<path d="M 101.2 151.251 A 50.2 50.2 0 0 0 51 101.051" fill="none"  stroke="#ff0000" stroke-width = "0.4" />

<path d="M 51 101.051 A 50.2 50.2 0 0 0 0.8 151.251" fill="none"  stroke="#ff0000" stroke-width = "0.4" />

<path d="M 0.8 151.251 A 50.2 50.2 0 0 0 51 201.451" fill="none"  stroke="#ff0000" stroke-width = "0.4" />

<path d="M 51 201.451 A 50.2 50.2 0 0 0 101.2 151.251" fill="none"  stroke="#ff0000" stroke-width = "0.4" />

</g>

</g>

</g>

</svg>









**"Lars Andersson"**

---
---
**Scorch Works** *November 07, 2017 23:31*

There are many idiosyncrasies in SVG files that I am still learning about.  This file shows one of them.  K40 Whisperer looks for the line color in a "style" inside of the path definition.  This SVG file does not put the color inside of a "style" so K40 Whisperer is not seeing it.  



K40 Whisperer will get better with SVG files over time. 



For now, opening a new Inkscape document and importing the SVG will solve 99% of the incompatibilities.  (I only of one oddball that requires changing the line color after it is imported into Inkscape.) 


---
**Lars Andersson** *November 08, 2017 07:37*

Can you show an example line on how you expect the style property to look? I can probably generate it in the Fusion360 post.


---
**Scorch Works** *November 08, 2017 12:08*

Here your SVG is with two quadrants working (one red one blue).  I also had to add an "id" value which is just a unique identifier for each path.



<?xml version="1.0" encoding="UTF-8" standalone="no"?>

<svg xmlns="[http://www.w3.org/2000/svg](http://www.w3.org/2000/svg)" width="297mm" height="210mm" viewBox="0 0 297 210">

<g transform="translate(0, 210)">

<g transform="scale(1, -1)">

<g transform="translate(20.853, 3.875)">

<path d="M 101.2 151.251 A 50.2 50.2 0 0 0 51 101.051" style="fill:none; stroke:#ff0000; stroke-width:0.4" id="path001" />

<path d="M 51 101.051 A 50.2 50.2 0 0 0 0.8 151.251"   style="fill:none; stroke:#0000ff; stroke-width:0.4" id="path002"/>

<path d="M 0.8 151.251 A 50.2 50.2 0 0 0 51 201.451" fill="none" stroke="#ff0000" stroke-width = "0.4" />

<path d="M 51 201.451 A 50.2 50.2 0 0 0 101.2 151.251" fill="none" stroke="#ff0000" stroke-width = "0.4" />

</g>

</g>

</g>

</svg>


---
**Lars Andersson** *November 08, 2017 20:37*

That worked fine when implemented in Fusion360 post.

Do you use the  "id=" property for something?

I tried without it and tried with the same id for all segments and that seemed to be accepted also.


---
**Scorch Works** *November 08, 2017 21:12*

When I used the same id for two of them they both ended up the same color even though one was red and one was blue.  The last color overrides all of the other colors if the id values are all the same.  so if you are only interested in red it will not be a problem to use the same id value for all of them. 


---
**Lars Andersson** *November 08, 2017 21:49*

Thanks for the clarification. I just implemented a running unique id= to be safe.


---
*Imported from [Google+](https://plus.google.com/117177573126096232373/posts/MLugsqW5Hxd) &mdash; content and formatting may not be reliable*
