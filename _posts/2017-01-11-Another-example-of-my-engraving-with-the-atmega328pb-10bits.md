---
layout: post
title: "Another example of my engraving with the atmega328pb (10bits )"
date: January 11, 2017 20:14
category: "Object produced with laser"
author: "Paul de Groot"
---
Another example of my engraving with the atmega328pb  (10bits ). Next time i do a compare of one identical picture in 3 resolutions:8, 9 and 10bits

![images/5c7c61c3d7c122c6a7a14687730f6e3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c7c61c3d7c122c6a7a14687730f6e3a.jpeg)



**"Paul de Groot"**

---
---
**Cesar Tolentino** *January 11, 2017 20:25*

Very nice. Is this one pass? Or several passes with different power settings


---
**Paul de Groot** *January 11, 2017 20:31*

**+Cesar Tolentino** just one pass.


---
**Bart Libert** *January 12, 2017 13:57*

Is this using grbl? If yes what version?


---
**Paul de Groot** *January 12, 2017 20:19*

**+Bart Libert** yes grbl1.1e


---
**Bart Libert** *January 12, 2017 20:22*

Nice Paul. So I suppose you use the laser modus in 1.1. How do you do the greyscale ? Just do a G1 move and constantly change laser power ? 




---
**Paul de Groot** *January 12, 2017 20:24*

**+Bart Libert** yes you use the lasermode $32=1 and issue just one M4 followed by the g1 codes. G0 or stops just switch off the laser. At the end of the file an M5. 


---
**Cesar Tolentino** *January 12, 2017 20:27*

Do you have a build log or website how you put this together?  Interested also on the interface you made to attach the OEM cables to the Arduino board. 




---
**Bart Libert** *January 12, 2017 20:28*

So for a line with for example 200 pixels, you make sure grbl is in laser mode with $32=1 (which grbl remembers in eeprom?) and then you g0 (rapid) to the start of your line, you enable M4 and you iterate 200 times over the sequence setting of setting power for the pixel and then move to the location of the next pixel ? Is this it ? Is it possible somehow to post 1 gcode file which can be analyzed then ? I currently run my 80W DIY cutter with linuxcnc but would love to experiment with grbl. Also... do you run 1.1e on an UNO or a MEGA ?


---
**Paul de Groot** *January 12, 2017 20:39*

**+Bart Libert** you're very close on the right track. Yes all settings are kept in eeprom. I run this on a atmega328pb. You can get a board 328pb explained from mouser for $14. I created a shield for  it and will publish this soon on a WordPress site. Having said this. 10bits is slow engraving 300mm/min. 9 and 8 are way faster over 1000mm/min. So still experimenting with it.


---
**Paul de Groot** *January 12, 2017 20:41*

So basically an uno on steriods. Same processor but with 2 more 16bits timers and it has 2 uart serial ports.


---
**Bart Libert** *January 12, 2017 20:44*

But grbl doesn't use that 2nd uart or extra timer ? 






---
**Bart Libert** *January 12, 2017 20:46*

I'd like to be able to read more about this. You have a blog about your K40 and how to hook it up to grbl etc ??? I am currently creating a galvo based engraver for small area. A little fast demo can be seen here 
{% include youtubePlayer.html id="uM6DReRwRKs" %}
[youtube.com - Galvo test smurfette](https://www.youtube.com/watch?v=uM6DReRwRKs&t=158s)


---
**Paul de Groot** *January 12, 2017 20:59*

**+Bart Libert** very smart remark. Indeed in grbl i replace the uart0 with uart1 vector and similar for the uart registers.


---
**Paul de Groot** *January 12, 2017 21:02*

**+Bart Libert** yes i have written a smal story about and will put it up on my friends website where blogs on his stem project. A mini particle collider based on an arduino uno! 


---
**Bart Libert** *January 12, 2017 21:04*

**+Paul de Groot** super! Always nice to read that stuff. Please post link when it is posted 




---
**Paul de Groot** *January 12, 2017 21:06*

**+Bart Libert** [awesome.tech - Science experiements and kits for makers and hobbyists](http://awesome.tech) my blog will be there within days😀


---
**Bart Libert** *January 12, 2017 21:08*

Paul, can I possibly get 1 of your gcode files (like the one you used for the engraving above) to study ? I can be mailed at educasoft@gmail.com 


---
**HalfNormal** *January 12, 2017 21:59*

There are a lot of free gcode senders out there which Laserweb is one of them. I use a product both with my cnc and laser called picsender. It does cost $ but has a lot of useful tools and settings for use with a laser and arduino. The support is second to none. The website is [www.picengrave.com](http://www.picengrave.com) 


---
**Bart Libert** *January 12, 2017 22:06*

I write my own software for that. What interests me is the gcode lines Paul used. Always nice to see such a file and analyze


---
**HalfNormal** *January 12, 2017 22:09*

**+Bart Libert**​ there is a lot of software that converts pics to gcode. That is where to find the info in the gcode output files.


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/XfERJwxxPw9) &mdash; content and formatting may not be reliable*
