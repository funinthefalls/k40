---
layout: post
title: "Tweaks to CorelLASER after installing? Had to install a fresh copy of CorelDRAW x7 and CorelLASER onto a new laptop, and remember there were a few settings changes needed, like selecting the right main board, but I think"
date: October 23, 2016 18:15
category: "Software"
author: "Pigeon FX"
---
Tweaks to CorelLASER after installing?  



Had to install a fresh copy of CorelDRAW x7 and CorelLASER  onto a new laptop, and remember there were a few settings changes needed, like selecting the right main board, but I think there were some other tweaks like setting "cutting data" to WMF? 





**"Pigeon FX"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 24, 2016 05:20*

Yeah, check these images:



[https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)



[https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)


---
**Pigeon FX** *October 24, 2016 11:26*

Thanks so much Yuusuf, really appreciated!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 24, 2016 11:59*

**+Pigeon FX** You're welcome. How goes your building of the little amp box things you were doing?


---
**Pigeon FX** *October 24, 2016 13:10*

Was going really well till the laptop i had dedicated to the cutter died! but all fixed now, and can get back to some laser cutting! had some really good results making face plates/control panels for projects using TroLase laminates, that are two colours one on top of another, and you laser engrave the top colour to reveal the bottom colour. The seem to be used mostly in sign making, but works really well for a bunch of projects:




{% include youtubePlayer.html id="VZBLqXs4-NY" %}
[https://www.youtube.com/watch?v=VZBLqXs4-NY](https://www.youtube.com/watch?v=VZBLqXs4-NY)




{% include youtubePlayer.html id="grQrAelRTdk" %}
[https://www.youtube.com/watch?v=grQrAelRTdk](https://www.youtube.com/watch?v=grQrAelRTdk)



And just about to try some of the aluminium labels:




{% include youtubePlayer.html id="KtH6PH_Y2Wg" %}
[https://www.youtube.com/watch?v=KtH6PH_Y2Wg](https://www.youtube.com/watch?v=KtH6PH_Y2Wg)



Saw your No-Sew Leather Tote Bag on instructables the other day, think I'm going to have to make one soon!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 24, 2016 21:06*

**+Pigeon FX** Those laminates are cool. Thanks for sharing that. Might have to try them out some time.


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/CzsnWGm3Pkq) &mdash; content and formatting may not be reliable*
