---
layout: post
title: "A few more K40 mods done today.."
date: May 08, 2016 01:48
category: "Modification"
author: "Custom Creations"
---
A few more K40 mods done today.. The laser line and cable chain mount are modified versions of ones that were on Thingiverse. I did not need all of the features they had (they were useless and over designed with no real functional purpose) so I K.I.S.Sed them..

![images/b57e05b752fcaa586c9a23790e2343dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b57e05b752fcaa586c9a23790e2343dc.jpeg)



**"Custom Creations"**

---


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/DY5Naiway36) &mdash; content and formatting may not be reliable*
