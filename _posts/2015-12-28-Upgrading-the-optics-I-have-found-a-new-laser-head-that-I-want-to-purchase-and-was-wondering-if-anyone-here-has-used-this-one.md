---
layout: post
title: "Upgrading the optics I have found a new laser head that I want to purchase ( ) and was wondering if anyone here has used this one"
date: December 28, 2015 19:18
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
Upgrading the optics



I have found a new laser head that I want to purchase ([http://www.ebay.ca/itm/262150928817](http://www.ebay.ca/itm/262150928817)) and was wondering if anyone here has used this one. I would also like to know how important it is to change the mirror holders, or if changing just the mirrors to a higher quality is all that is needed.



I like that the laser head is adjustable, easier to move the lens tube up or down to get to the proper focus distance than having to adjust the Z table height (Mine is manual so I have to set each of the 4 corners separately.)It can also use 2 different focal lengths, 50.8, and 101.6 





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *December 28, 2015 20:25*

Thanks for the heads up.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 29, 2015 01:49*

Aside from it's weight & size being a problem, it looks like a really well built laser head.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/5Cmcgm8Kdaw) &mdash; content and formatting may not be reliable*
