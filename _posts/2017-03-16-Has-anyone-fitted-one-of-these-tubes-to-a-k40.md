---
layout: post
title: "Has anyone fitted one of these tubes to a k40?"
date: March 16, 2017 23:17
category: "Modification"
author: "Andy Parker"
---
Has anyone fitted one of these tubes to a k40? [https://www.aliexpress.com/item/Co2-Glass-Laser-Tube-Metal-Head-700MM-40W-for-CO2-Laser-Engraving-Cutting-Machine/32780988059.html?spm=2114.13010208.99999999.262.O9QCvx](https://www.aliexpress.com/item/Co2-Glass-Laser-Tube-Metal-Head-700MM-40W-for-CO2-Laser-Engraving-Cutting-Machine/32780988059.html?spm=2114.13010208.99999999.262.O9QCvx) Look quite good quality but unsure of the cathode-anode terminals





**"Andy Parker"**

---
---
**Andy Parker** *May 14, 2017 22:07*

Quick update, I have been using this tube now for a couple of weeks, up to now it's simply awesome. The build quality of the tube is the best i have seen in the sub £300 bracket. Hopefully it will keep running the way it has been. Next thing to fix is the poxy motherboard :)


---
*Imported from [Google+](https://plus.google.com/100121420236877030725/posts/ZML8PhKi92f) &mdash; content and formatting may not be reliable*
