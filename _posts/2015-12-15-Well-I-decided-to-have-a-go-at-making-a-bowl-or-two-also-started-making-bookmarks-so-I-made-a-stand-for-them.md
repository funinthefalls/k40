---
layout: post
title: "Well I decided to have a go at making a bowl or two, also started making bookmarks, so I made a stand for them.."
date: December 15, 2015 00:16
category: "Object produced with laser"
author: "Tony Schelts"
---
Well I decided to have a go at making a bowl or two, also started making bookmarks, so I made a stand for them.. Having this machine has really

got my imagination going.



![images/c8f0ace5411b59c91d91567576a20a91.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c8f0ace5411b59c91d91567576a20a91.jpeg)
![images/eed106790532811b263ff58decdef637.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eed106790532811b263ff58decdef637.jpeg)
![images/44510c32c89fcd09dfd00012d7d64012.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44510c32c89fcd09dfd00012d7d64012.jpeg)
![images/9d04021f93f35ac6398ccb540d630cf4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9d04021f93f35ac6398ccb540d630cf4.jpeg)
![images/1968c318729d40c70fa29689b5668fd1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1968c318729d40c70fa29689b5668fd1.jpeg)

**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2015 03:47*

I'm liking the bowls. Pretty cool ideas. I totally agree that having the machine gets the imagination going.


---
**Scott Thorne** *December 15, 2015 10:59*

Great looking work!


---
**ChiRag Chaudhari** *December 15, 2015 18:49*

nice, you are only bind by your imagination


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/8tSuquEvn5j) &mdash; content and formatting may not be reliable*
