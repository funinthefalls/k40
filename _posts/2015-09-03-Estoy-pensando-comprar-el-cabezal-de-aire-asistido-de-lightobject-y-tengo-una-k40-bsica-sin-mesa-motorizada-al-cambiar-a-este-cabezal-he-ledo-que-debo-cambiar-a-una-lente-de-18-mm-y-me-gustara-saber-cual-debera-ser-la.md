---
layout: post
title: "Estoy pensando comprar el cabezal de aire asistido de lightobject, y tengo una k40 bsica sin mesa motorizada, al cambiar a este cabezal, he ledo que debo cambiar a una lente de 18 mm y me gustara saber cual debera ser la"
date: September 03, 2015 00:12
category: "Modification"
author: "Al Tamo"
---
Estoy pensando comprar el cabezal de aire asistido de lightobject, y tengo una k40 básica sin mesa motorizada, al cambiar a este cabezal, he leído que debo cambiar a una lente de 18 mm y me gustaría saber cual debería ser la distancia focal que debería tener la lente, un saludo y mil gracias.





**"Al Tamo"**

---
---
**Joey Fitzpatrick** *September 03, 2015 16:49*

2" (50.8mm). Seems to be the preferred lens for the K40.  You could always buy two different lenses(with different focal length) and swap between them.you would have to adjust your bed accordingly.  I would recommend the 50.8mm one to start with.  You should be able to just drop it in an go ....without any bed height changes.


---
**Kirk Yarina** *September 04, 2015 22:50*

You can use the K40 to make an adapter from your lens size (mine is 12mm) to 18mm for the Light Objects air assist nozzle.  You may need to try a few times to get the size exact; with my K40 I had to make the outer circle 18.3mm, and the inner 11.7 to allow for the laser beam width.


---
*Imported from [Google+](https://plus.google.com/112166512335650872249/posts/W19URLm9nV7) &mdash; content and formatting may not be reliable*
