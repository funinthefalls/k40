---
layout: post
title: "Had the installation of new officers at my Masonic lodge last night"
date: December 09, 2018 13:27
category: "Object produced with laser"
author: "Ned Hill"
---
Had the installation of new officers at my Masonic lodge last night. I was sporting a pair of cuff links I made along with a name tag and presentation box I made for the outgoing Master of the lodge.   The presentation box was a pine box,  I found at the craft store,  that I dressed up with a lacquer finish, blue flocked paper inside and an alder wood laser etched and cut  jeweled S&C on top.   The box contains a gold Past Master jewel on a tie chain. 



![images/35d43ed75a1b5570183c6041b66bdf35.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/35d43ed75a1b5570183c6041b66bdf35.jpeg)
![images/bce35c463a9c1552220274ca33675db9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bce35c463a9c1552220274ca33675db9.jpeg)
![images/068658328cd0e39595f322fc0b3d8c50.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/068658328cd0e39595f322fc0b3d8c50.jpeg)
![images/29cea9d03690ad8234b597d6ec4c5216.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/29cea9d03690ad8234b597d6ec4c5216.jpeg)
![images/3b84ab441831e5d19d4aac852acb1c0e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b84ab441831e5d19d4aac852acb1c0e.jpeg)

**"Ned Hill"**

---
---
**Ned Hill** *December 09, 2018 13:37*

I have recently discovered, and become a fan of, these pine boxes you can find at craft stores.  They are available in a variety of sizes and are cheap.  This 4x4" box with hidden barrel hinges and magnetic closure was only $1!  I found it at the AC Moore craft store.  The other craft stores have boxes as well but they are of slightly different shape and hardware.  



You can engrave them, but plan on darkening the engraving as pine doesn't darken when engraved.


---
**Ned Hill** *December 09, 2018 13:52*

Here is another presentation box I did for someone else where I engraved the top.  I did a black paint infill on the engraving.

![images/2529edd2fd57de975c75cb74bb405eec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2529edd2fd57de975c75cb74bb405eec.jpeg)


---
**Travis Sawyer** *December 09, 2018 15:36*

Nice Work Wor. Ned!


---
**HalfNormal** *December 09, 2018 15:46*

Nice craftsmanship.


---
**Don Kleinschnitz Jr.** *December 09, 2018 15:47*

 .... Very Nice!


---
**James Rivera** *December 09, 2018 18:34*

How did you make the cuff links?


---
**Ned Hill** *December 09, 2018 18:45*

**+James Rivera** cufflinks are easy.  You can get cuff link blanks with glue pads in a variety of finishes. I use blanks with 10mm glue pads and  use CA glue to attach. I rough the pads slightly first with a bit of sandpaper and they are next to impossible to get off without destroying the face. 


---
**Ned Hill** *December 09, 2018 18:49*

![images/89b0df65451dc6808000aed4bdb3f2f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/89b0df65451dc6808000aed4bdb3f2f3.jpeg)


---
**Ned Hill** *December 09, 2018 18:55*

Lots of places sell blanks, but I buy mine from this seller on Etsy. Great price, just takes a couple of weeks for delivery from China to the US. [https://etsy.me/2rvaMSV](https://etsy.me/2rvaMSV) 


---
**Chris Riley** *December 14, 2018 04:20*

SMIB!


---
**Travis Sawyer** *December 14, 2018 11:59*

We're everywhere ;)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/d8MqoBA2dMh) &mdash; content and formatting may not be reliable*
