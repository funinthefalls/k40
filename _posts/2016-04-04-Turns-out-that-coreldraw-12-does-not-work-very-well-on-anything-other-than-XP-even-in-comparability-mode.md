---
layout: post
title: "Turns out that coreldraw 12 does not work very well on anything other than XP even in comparability mode"
date: April 04, 2016 12:31
category: "Software"
author: "ben crawford"
---
Turns out that coreldraw 12 does not work very well on anything other than XP even in comparability mode. So i created a VM for XP on my windows 7 laptop and installed the software on that. It works wonderfully, the program no longer crashes. I might get a cheap computer and install XP on it just so it has a dedicated computer.



Just felt like sharing :)





**"ben crawford"**

---
---
**Stephane Buisson** *April 04, 2016 12:52*

Does it the hardware dictating the soft, or the other way around ? not tempted by Smoothie upgrade ...

good time to have a think, before buying old PC.


---
**ben crawford** *April 04, 2016 13:11*

It's the software having the issues, it does not work well on windows 7 but it ran just fine in my XP VM. I got some nice pieces created with it once i was using the XP VM. Does that answer your question?


---
**Stephane Buisson** *April 04, 2016 13:34*

I am happy if you are happy ;-))

just reacting on your future expense, ...and possibilty to do otherwise than old stuff.


---
**Heath Young** *April 05, 2016 02:35*

I'm using CorelDraw 11 on windows 7 starter, on a terrible atom based mini-PC - works fine.


---
**Anthony Bolgar** *April 05, 2016 04:00*

I have 12 running on Windows 10 without a problem


---
**I Laser** *April 05, 2016 04:58*

Running two copies. Host running win 7 and a win 7 vm, both x64, without any problems. Maybe yours is conflicting with other installed software.


---
**ben crawford** *April 05, 2016 12:14*

The program runs wonderfully on my windows xp VM. I am still learning how to use it correctly, learning the power levels and how to make things line up correctly.


---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/V3K8ebjJe51) &mdash; content and formatting may not be reliable*
