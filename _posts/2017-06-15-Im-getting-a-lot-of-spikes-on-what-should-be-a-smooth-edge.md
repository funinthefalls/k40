---
layout: post
title: "I'm getting a lot of \"spikes\" on what should be a smooth edge"
date: June 15, 2017 17:03
category: "Discussion"
author: "Adam J"
---
I'm getting a lot of "spikes" on what should be a smooth edge. Hopefully this picture captures it, the white bump along the edge is when the head moves to cut the next piece, the "entry point" I guess you could call it. Tried slowing down G0 moves (seek) to 1000 but still having problems. Tried moving the objects further apart to no avail. Any ideas? I'm using a C3D Smoothie with LaserWeb.

![images/c3497079a119bbbcb1a3c49af480be47.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c3497079a119bbbcb1a3c49af480be47.jpeg)



**"Adam J"**

---
---
**Jeff Johnson** *June 15, 2017 19:56*

Make sure the rails are clean, especially the steel bar on the left side. I had a small buildup of some residue from the MDF cutting. It caused the head to jump just a tiny bit in one spot. 


---
**HalfNormal** *June 16, 2017 00:24*

That could be an artifact in the file. I thought I had a smooth surface once but had a similar issue. I had a few pixels that where added and it was enough to show up on the cut.


---
**Adam J** *June 30, 2017 13:46*

The shape in question is repeated 4 times on a single sheet of acrylic. The job is to cut all four at 6mm/s. This spike only happens on two of the pieces at exactly the same spot (different areas on each) whenever I try, the other two are clean cuts. I'm still not sure why this is happening or how I can prevent it?


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/TcFzGkAS5SE) &mdash; content and formatting may not be reliable*
