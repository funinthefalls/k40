---
layout: post
title: "Found this tube from China"
date: June 14, 2017 10:30
category: "Material suppliers"
author: "Imnama"
---
Found this tube from China



[https://www.banggood.com/40W-CO2-Laser-Tube-700mm-For-Cutting-Engraving-Cutting-Machine-p-1161495.html?rmmds=search](https://www.banggood.com/40W-CO2-Laser-Tube-700mm-For-Cutting-Engraving-Cutting-Machine-p-1161495.html?rmmds=search)







**"Imnama"**

---
---
**Chris Hurley** *June 14, 2017 11:44*

I've prefer eBay due to PayPal protection. 


---
**Imnama** *June 14, 2017 12:06*

Well, at banggood you can also pay with payPal. I have not tried this tube, just found and shared it.




---
**greg greene** *June 14, 2017 12:56*

Bangood usually has very low shipping charges


---
**Don Kleinschnitz Jr.** *June 14, 2017 13:25*

**+greg greene** says free!


---
**Imnama** *June 14, 2017 13:55*

And the Air Parcel Register shipping method even has a ensurance that taxes will be refunded




---
**Imnama** *June 14, 2017 13:56*

For europe, I'm not shure about other continents




---
**Fernando Bueno** *June 15, 2017 08:25*

I usually buy both Banggood and Gearbest and are two stores that are 100% serious and reliable. Sometimes, it is true that they sell imitations of products of recognized brands, but it is also true that they put it on the product page (in very, very small letters, sometimes). According to the products, they are in different warehouses located in different countries and this lowers the costs, the times of shipment and the costs of customs. At least for Spain, it is a good choice of store to buy.



The doubt is in the quality of that tube in concrete. The price is very good and the look is also good, but it would be nice to know if someone has tried and what opinion.


---
*Imported from [Google+](https://plus.google.com/105177731458209028076/posts/GjW1E9rkW6X) &mdash; content and formatting may not be reliable*
