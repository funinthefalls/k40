---
layout: post
title: "After reading books after books about Co2 laser tubes, i was thinking about the recombination effect in these tubes, that keeps the gasses in good state"
date: March 06, 2017 09:34
category: "Discussion"
author: "HP Persson"
---
After reading books after books about Co2 laser tubes, i was thinking about the recombination effect in these tubes, that keeps the gasses in good state.



We all know, if we over power the tubes the recombination does not work as planned, as the breakdown is bigger than the recombination, and the tubes lifetime is shortened. (by how much is unknown though)



But, that got me thinking, if we use our tubes on very low power, is this dangerous too? or is the recombination even better the lower you go, and maybe prolonging the tube life, instead of shortening it.

If there is a golden limit that has to be reached to have the perfect recombination going?



Ideas? :)





**"HP Persson"**

---
---
**Don Kleinschnitz Jr.** *March 06, 2017 11:24*

My short answer is "I don't know" but I would like to :)

......

I have been trying to understand the opt-electric response of the tube and currently that has driven me to try and understand the relationship between the HV drive and the gas discharge characteristics of the tube.

I think your question could be answered if we took the understanding one step forward and understood the relationship between the tubes discharge and how it impacts the change in energy levels in the tube. 



I think this video (posted this weekend) discusses a similiar question to what you are asking. i.e. running the tube in pre-ionization state.


{% include youtubePlayer.html id="5CoTrLNUWTM" %}
[youtube.com - RDWorks Learning Lab 97 Understand Your HV Power Supply via Florida](https://youtu.be/5CoTrLNUWTM)



I think this video reveals to me a couple of important parts of the puzzle:

... the tube response is much slower than the speed of currents that I have measured but consistent with how I expected a gas discharge to behave.

.... to understand what is going on we need to measure both voltage and current in the tube

.... more information about gas discharge characteristic zones.

......

I would value knowing what books and references you are using to understand recombination and its effect on tube life, gass etc.



I pulled my research into this post to focus it:



[http://donsthings.blogspot.com/2017/03/laser-response-charcteristics.html](http://donsthings.blogspot.com/2017/03/laser-response-charcteristics.html)



 


---
**HP Persson** *March 06, 2017 11:42*

Yeah, it was Russ´s latest video that got me thinking a bit more about it :)



I´ll link the books and research papers i have been reading when im back at the home computer.


---
**Don Kleinschnitz Jr.** *March 06, 2017 11:43*

**+HP Persson** thanks much!




---
**Don Kleinschnitz Jr.** *March 06, 2017 16:08*

Did a lot of reading this AM searching for operational relationships between the voltage driving the tube, the gas mixture formulation and lasing.

..............

I am still learning so this is my oversimplification of the parts and their operation. 

BTW I do not know what the gas formulation in out tubes is? 

This stuff is from general reading about sealed gas CO2 lasers.



Common gas contents & their function (oversimplified):



13.5% "N2" :    excited by gas discharge (pink glow) collides with and moves CO2 molecules to energy level 3



9.5%   "CO2": molecule that lases (emits most of the photons) returning to energy level 3-2.



77%    "He":    collides with CO2 at level 2, helps to move CO2 to level 2 and then collides with tube walls providing cooling.



<b>2%      "H2":    gas discharge disassociates CO2 into CO and 02. H2 mixes with CO & O2 to regenerate CO2 .</b>



 takes the mixture to >100% so something else must be adjusted if H2 is used.  

.....

Ionization Voltage: 25 KV

Voltage drops to (at lasing):    13-15 KV

Negative resistance: 200-300K



........

So thinking about the above information:



1. "He" leak down (through the glass) would have significant effect on the ability to cool the tube and its output will reduce due to decrease in population inversion of CO2 at level 2. I assume that nothing can be done to slowdown He leakage. 

2. The gas discharge is what disassociates the CO2 so it seems logical that over driving the tube relative to its formulation will cause greater amounts of dissociation than the formulation can regenerate? 

3. Keeping the tube cooler would improve the output. I wonder if adding airflow in the laser compartment would help? We could measure the temperature drop across the tube, coolant to tubes outer surface. I noticed that some expensive sealed lasers have heat sinks vs water cooling.

..................

More information always creates more questions: 

a. Does the CO2 breakdown in the "dark discharge" phase, the "glow" phase or both? If its only in the dark discharge phase then operating on and off at the dark thresh hold would seem to wear the tube sooner. 

b. Is there a voltage at which CO2 breaks down? Is that = dielectric breakdown of the gas?

c. Someone in the community asked if it was OK to run a tube with a higher output HV supply than intended. My answer would still be NO.  








---
**Ned Hill** *March 06, 2017 18:04*

Interesting.  I like that RDworks video. 


---
**Nate Caine** *March 06, 2017 20:55*

A few additional notes:



(1) There are traces of other gases in the mix as well.  Water vapor and Argon for example.  Each addresses some secondary need of the laser,



(2) Over time some of the metal electrodes erode and that metal combines with the various gases to create new ingredients in the gas mixture.  (Especially the cathode.)



(3) One of the problems is CO2 disassociating into CO instead (affected by the catalyst in the helical tube).



(4) Another aging problem is erosion of the ZeSn window and internal mirror.  Both contribute to the loss of beam quality as the tube ages.  (loss of focus or creation of satellite beam spots).



(5) On my K40 (and the few others I've seen), the manufacturer chose to wire the WP and (TH and TL) reversed such that the WP was actually being driven from the controller board.  I've analyzed the internal circuitry of several <b>other</b> laser power supplies, and the WP line driven this way means the the laser never is used in pre-ionization mode.  When WP is off, the output current is off.  If properly used, WP being on, enables pre-ionization and the tube is running at a current just under that necessary to start lasing.  Then the combination of (TH and TL) driven by the controller board will shift the current from pre-ionization to full on (where full on is determined by the potentiometer setting).  This is more important for fine engraving than for cutting.



(6) With the ballast resistor used on many of these machines, if the tube is idling at preionization current levels, then the node from the ballast resistor to the tube return, can sit at several hundred volts, which could be dangerous. (But I don't have equipment to verify how high of voltage.)


---
**Don Kleinschnitz Jr.** *March 07, 2017 15:13*

**+Nate Caine** thanks for the info ....

(5).  Can you share any schematics you have created for HVPS. Here is the one that **+Paul de Groot** created.

[https://drive.google.com/file/d/0BxRBPjkKUvRUN1Q3OTlqNFhVQkJnYW5VTjRXUWduaEp5Wnd3/view](https://drive.google.com/file/d/0BxRBPjkKUvRUN1Q3OTlqNFhVQkJnYW5VTjRXUWduaEp5Wnd3/view). 

I am interested in the methods used for pre-ioninzation you suggest. In the circuits I have examined the enable digitally controls the internal PWM enable so I do not see how it would allow any current. 

Is your machine running pre-ionized? 

In machines configured like this does the milli-amp meter show current?

By running a current just below ionization do you mean there is a high voltage across the tube at all times (but no ionization and no current) and then it is raised to the discharge level when engraving starts?

[drive.google.com - psu.pdf - Google Drive](https://drive.google.com/file/d/0BxRBPjkKUvRUN1Q3OTlqNFhVQkJnYW5VTjRXUWduaEp5Wnd3/view)


---
**HP Persson** *March 08, 2017 13:18*

**+Don Kleinschnitz**, here´s my list of books and papers i have been reading. Some is on water conductivity, reflectivity, and not only lasers.

Missing one book, trying to find it where they talked about the coolant charge taking energy from the laser output.

I´ll add it up when i find it.



[https://www.princeton.edu/~spikelab/papers/book02.pdf](https://www.princeton.edu/~spikelab/papers/book02.pdf)

[https://www.princeton.edu/~spikelab/papers/book01.pdf](https://www.princeton.edu/~spikelab/papers/book01.pdf)

[http://www.aml.engineering.columbia.edu/ntm/level2/](http://www.aml.engineering.columbia.edu/ntm/level2/)

[https://www.labsphere.com/site/assets/files/2553/a-guide-to-reflectance-materials-and-coatings.pdf](https://www.labsphere.com/site/assets/files/2553/a-guide-to-reflectance-materials-and-coatings.pdf)

[https://www.repairfaq.org/sam/laserco2.htm](https://www.repairfaq.org/sam/laserco2.htm)

[http://www.laserk.com/newsletters/whiteCO.html](http://www.laserk.com/newsletters/whiteCO.html)

[http://www3.nd.edu/~sst/teaching/AME60637/reading/2003_PCPP_Kogelschatz_dbd_review.pdf](http://www3.nd.edu/~sst/teaching/AME60637/reading/2003_PCPP_Kogelschatz_dbd_review.pdf)



Advances and Applications of Co2 Laser

ISBN	1632400413, 9781632400413



Optoelectronics and Optical Communication

ISBN	9381159068, 9789381159064



Introduction to Laser Technology

ISBN	1118219481, 9781118219485



Volume 2 of Handbook of Laser Technology and Applications

ISBN 0750306076, 9780750306072




---
**Don Kleinschnitz Jr.** *March 08, 2017 15:12*

**+HP Persson** you rock ! Now I have something to add to my list of nighttime distractions.....


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/awgrrLsvht3) &mdash; content and formatting may not be reliable*
