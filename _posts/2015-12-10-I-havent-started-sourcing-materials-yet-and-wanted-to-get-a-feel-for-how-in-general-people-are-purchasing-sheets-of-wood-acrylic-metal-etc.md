---
layout: post
title: "I haven't started sourcing materials yet and wanted to get a feel for how in general people are purchasing sheets of wood, acrylic, metal, etc"
date: December 10, 2015 23:57
category: "Material suppliers"
author: "Joseph Coco"
---
I haven't started sourcing materials yet and wanted to get a feel for how in general people are purchasing sheets of wood, acrylic, metal, etc.



I don't know much about it, but I figure most people are either: purchasing locally from lumber yard/hardware store, online from maker site, online from factory warehouse, scrap materials from CNC shop, scrap from wood shop, locally from makerspace.



Let me know what might be worth pursuing. I'm in the US in a medium-sized city.





**"Joseph Coco"**

---
---
**Stephane Buisson** *December 11, 2015 00:49*

I often grab wooden box from the market (spanish orange come in nice 3 mm wooden box, and the season just start now),  but Ebay UK is fantastic (perspex, leather, coper, alu, ...), and DIY local shop


---
**Ray Kholodovsky (Cohesion3D)** *December 11, 2015 02:08*

Estreetplastics is good for acrylic. Onlinemetals or various eBay sellers for aluminum sheet. Home Depot for MDF. Got some Garolite Plates from McMaster a long time ago, haven't used it yet. Aluminum seems to be the safer bet from the airborne carcinogen perspective. 


---
**Gary McKinnon** *December 11, 2015 13:39*

I use ebay for clear acrylic.


---
*Imported from [Google+](https://plus.google.com/+JosephCoco/posts/Udzpw3pPgnf) &mdash; content and formatting may not be reliable*
