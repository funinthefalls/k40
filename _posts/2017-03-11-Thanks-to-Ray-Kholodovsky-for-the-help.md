---
layout: post
title: "Thanks to Ray Kholodovsky for the help.."
date: March 11, 2017 21:40
category: "Object produced with laser"
author: "Lance Ward"
---
Thanks to Ray Kholodovsky for the help..  A new control panel was my first project and it turned out well.  Done with C3D Mini and LW4.  Biggest problem I had was getting laser power low enough for vector engraving. Cutting was a breeze. Engraving speed was 1000mm/min at 3% (of 10ma) power. Cutting speed was 1000 at 50% power.  Naturally I had to scratch the panel installing it.... (doh!)﻿

![images/4dd24ea46010b529cb031e5c57ea2727.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4dd24ea46010b529cb031e5c57ea2727.jpeg)



**"Lance Ward"**

---
---
**Alex Krause** *March 11, 2017 21:47*

Is this Romark panel?


---
**Lance Ward** *March 11, 2017 21:49*

Not sure... Got it here: [amazon.com - Amazon.com: Brushed Silver/Black 1/16" Laser Engraving Material Plastic Premium - Set of 4: Home Improvement](https://www.amazon.com/gp/product/B01N5STGNU/)


---
**Alex Krause** *March 11, 2017 22:02*

4x 600mm by 300mm sheets?


---
**Lance Ward** *March 11, 2017 22:05*

Yes, four 600x300x1.5 sheets. Cuts and engraves well.


---
**Alex Krause** *March 11, 2017 22:24*

Awesome!!! Thanks for the link


---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 22:56*

If you're doing low power stuff set the pot to 5mA and adjust the LW pwm % accordingly. 



You might be able to get better pwm range resolution. 


---
**Abe Fouhy** *March 12, 2017 00:37*

That looks slick! Do you have the drawing file for it you could share?


---
**Lance Ward** *March 13, 2017 16:04*

**+Abe Fouhy** I'm not sure if there is a repository here or not...  However, I uploaded the cdr and svg file to dropbox here: [dropbox.com - Public](https://www.dropbox.com/sh/uy4knxfvd4relrh/AAD_Ymh-McfGvvt17uUCEBMoa?dl=0)




---
**Lance Ward** *March 13, 2017 16:06*

Final version:

![images/58fa393857201289413c43dff5af24fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58fa393857201289413c43dff5af24fb.jpeg)


---
**Abe Fouhy** *March 13, 2017 16:23*

Oo that's sexy! Nice work **+Lance Ward** !


---
**Abe Fouhy** *March 13, 2017 16:24*

**+Lance Ward** my laser is broken right now, what would you charge to cut one for me? I'll pay for postage.


---
**Lance Ward** *March 13, 2017 19:31*

**+Abe Fouhy** Be happy to.  I probably can't get to it until this weekend though.  How 'bout $10. shipped?  I'll also cut you a backing plate I make from 3mm ply.  It just goes behind the outside of panel to stiffen it all up a bit.


---
**Abe Fouhy** *March 13, 2017 20:05*

**+Lance Ward**​ Sounds great. Send me an email  with your contact info.


---
**Abe Fouhy** *October 23, 2017 15:05*

**+Lance Ward** would you still make this panel for me? I just got all my parts in and am building it all. 


---
*Imported from [Google+](https://plus.google.com/114422684423642232545/posts/iMoLNb1zaSk) &mdash; content and formatting may not be reliable*
