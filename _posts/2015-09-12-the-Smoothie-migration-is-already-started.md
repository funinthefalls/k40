---
layout: post
title: "the Smoothie migration is already started !"
date: September 12, 2015 12:39
category: "Smoothieboard Modification"
author: "Stephane Buisson"
---
the Smoothie migration is already started !





**"Stephane Buisson"**

---
---
**Stephane Buisson** *September 12, 2015 12:40*


{% include youtubePlayer.html id="OSnW8AVE2O4" %}
[https://www.youtube.com/watch?v=OSnW8AVE2O4](https://www.youtube.com/watch?v=OSnW8AVE2O4)



and also



[https://groups.google.com/forum/?#!forum/opensource-laser](https://groups.google.com/forum/?#!forum/opensource-laser)


---
**Flash Laser** *September 14, 2015 03:28*

Hi Stephane, look at your blogspot about the laser machine, so you finished modified the machine yourself, how is the machine working?? thanks for your reply!


---
**Stephane Buisson** *September 14, 2015 09:20*

**+Flash Laser** this is not my blog, but an other K40 user named ajf. (the other modified K40 is by zendesigner)

As you maybe don't know, It's several board using smoothieware, the Azteeg X5 is one of the cheap one, but do not include a network connexion (RJ45). I would rather go for the original Smoothieboard 4XC for that reason. 



at the moment a Smoothieware board is working with Visiscut (RJ45) or Fusion 360 (USB).


---
**David Cook** *October 09, 2015 22:56*

If I use the X5    does that work with the Power supply as is ? or do I need to find a Power supply with a PWM input for power control ?


---
**Stephane Buisson** *October 09, 2015 23:09*

I couldn't be 100% as I got a smoothie 4XC

it work for me today but not finished yet.

(I am able to move XY but learning smoothie to slow me down).

I needed to take 24V from PSU to VBB to have enought power for the LCD screen, usb power was't enough. Now still need to test if it take the 5V from laser PSU to work ethernet only. with no USB in, normally it should work ...



good chance the laser PSU is enough.

24V for sure, 5V to be confirmed.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/gKZ4ekFGced) &mdash; content and formatting may not be reliable*
