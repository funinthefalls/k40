---
layout: post
title: "Next question, how do I keep bubbles out of the laser tube?"
date: April 08, 2017 15:26
category: "Discussion"
author: "Martin Dillon"
---
Next question, how do I keep bubbles out of the laser tube?  Did someone say one drop of dish soap?  The only way I have found is to stand the machine on end.  I also heard rotate the laser tube.  I started to do that and then decided against it until I posted the question.





**"Martin Dillon"**

---
---
**Abe Fouhy** *April 08, 2017 16:09*

One way is to turn just the pump on and tap the reservoir continually until it clears. Since we're using distilled water in an open loop system maybe an air elimator could be used. You just install it at the highest point in the loop so all the bubbles go to it. [http://www.houseneeds.com/heating/hydronic-heat-supplies/taco-hy-vent-air-vent-400-4?gclid=CNiogv-eldMCFZFffgodHlQG7g](http://www.houseneeds.com/heating/hydronic-heat-supplies/taco-hy-vent-air-vent-400-4?gclid=CNiogv-eldMCFZFffgodHlQG7g)


---
**Abe Fouhy** *April 08, 2017 16:11*

I would stay away from dish soap.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2017 16:37*

Rotating the tube so the outlet is facing up is definitely a method I've seen described many times. Also running the reservoir at the same level or higher than the tube assists. In my setup, I have the return outlet tube in the reservoir's water, to prevent air getting in when the pump is turned off & to prevent bubbles from water splashing into the reservoir.


---
**HP Persson** *April 08, 2017 17:16*

Add something that breaks the surface tension, with that the bubbles cannot form.

I use water wetter myself.



Here´s a pic with conductivity meters, left is plain distilled, middle is distilled with water wetter, right is distilled + dish soap. The middle is a bit high too, think i added too much water wetter to the glass :)



![images/081238d994c26da2a0e9a9ac9a2e463c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/081238d994c26da2a0e9a9ac9a2e463c.jpeg)


---
**Martin Dillon** *April 08, 2017 22:14*

I filled the tank up until the flow switch was under water and that seems to be working better.  Most of the water had drained out of the tube since the last time I cut something.


---
**Joe Alexander** *April 09, 2017 01:01*

Keep your water reservoir higher than the tube and tip to get air out, done. As long as your reservoir stays full and the feed tube is submerged that tube will stay full. (think of a P-trap under a sink, my tube is the lower bend that always has water :)  )


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/gWrcjeQxbKC) &mdash; content and formatting may not be reliable*
