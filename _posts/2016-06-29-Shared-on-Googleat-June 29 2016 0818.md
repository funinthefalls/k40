---
layout: post
title: "Shared on June 29, 2016 08:18...\n"
date: June 29, 2016 08:18
category: "Object produced with laser"
author: "Glyn Jones"
---




![images/8d143247e95c4cf74878d4270547703c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d143247e95c4cf74878d4270547703c.jpeg)
![images/2b19695698da459a68662ee9ddc1f97c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b19695698da459a68662ee9ddc1f97c.jpeg)

**"Glyn Jones"**

---
---
**Anthony Bolgar** *June 29, 2016 08:27*

Looks nice. Good job!


---
*Imported from [Google+](https://plus.google.com/+GlynJonesCarlos/posts/XSkwn9bDfYC) &mdash; content and formatting may not be reliable*
