---
layout: post
title: "Making progress :) Today i purchased and installed CorelDraw x8 and installed Corellaser"
date: August 05, 2016 00:44
category: "Software"
author: "Bob Damato"
---
Making progress :)

Today i purchased and installed CorelDraw x8 and installed Corellaser. It seems to work OK with it. Thank you **+Yuusuf Sallahuddin**  :)



One  quick thing I notice, maybe someone has a cure. When I hit engrave, or cut, the CorelLaser toolbar up top disappears on me. When that happens, I can no longer pause or cancel a  job.  Even when the whole job is complete the bar never comes back, so if I want another copy, or want to cut a layer, I have to quit the app, and restart it. 



Any suggestions? Thank you!

Bob





**"Bob Damato"**

---
---
**Jim Hatch** *August 05, 2016 00:59*

The controls are in the system tray - lower right corner of the Windows toolbar by the clock. If they don't show, there should be a little triangle to click and display the hidden apps.


---
**Bob Damato** *August 05, 2016 01:16*

Wow, Thanks Jim. Didnt see that, I have that stupid engraver alert thing show up there every second or so. Thanks for the help!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 01:48*

Yeah, same thing happens to most of us. It seems that the toolbar disappears in versions of windows > win 7. It's actually quite annoying, but you learn to work with it. & the alert is very annoying showing up constantly. Sometimes makes it hard to access the controls from the system tray as the notification is blocking the right-click menu.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/ZPqZfBWRLJg) &mdash; content and formatting may not be reliable*
