---
layout: post
title: "This nozzle does not just drop in to the KW40 Chinese laser cutter"
date: December 03, 2015 00:55
category: "Discussion"
author: "James McMurray"
---
This nozzle does not just drop in to the KW40 Chinese laser cutter.  You need a completely different mirror/lens holder  and different lens to use this item.  I am not complaining, I am just letting folks know.  I couldn't find any information on this offered nozzle so I bought one to see if it would work.  I was hoping the the only thing I might need to do it make a lens holder.  But the threads on the nozzle don't fit anything on the K40 and the lens needed  for this to work is quite a bit larger.  





**"James McMurray"**

---
---
**Ashley M. Kirchner [Norym]** *December 03, 2015 01:28*

Myah, it's why I choose 3D print my own to press fit over the bottom retaining ring (the ring you unscrew to get to the lens). Much better.


---
**Gary McKinnon** *December 03, 2015 11:09*

I bought a 3d-printed air-assist that also has a holder for a red-dot sight :



[http://www.ebay.co.uk/itm/Air-assist-nozzle-kit-for-K40-40W-CO2-Laser-cutting-engraving-machine-/151517519998?var=&hash=item234725e07e:m:m1lmlgUg1szKb5ugIwEtTlQ](http://www.ebay.co.uk/itm/Air-assist-nozzle-kit-for-K40-40W-CO2-Laser-cutting-engraving-machine-/151517519998?var=&hash=item234725e07e:m:m1lmlgUg1szKb5ugIwEtTlQ)


---
**Gary McKinnon** *December 03, 2015 11:10*

Ebay UK has quite a few bits and pieces for the K40, i assume that Ebay US does too.


---
*Imported from [Google+](https://plus.google.com/115183135050042620815/posts/LaLDsPYQ53F) &mdash; content and formatting may not be reliable*
