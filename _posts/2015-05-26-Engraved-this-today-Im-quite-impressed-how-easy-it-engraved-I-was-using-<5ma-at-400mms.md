---
layout: post
title: "Engraved this today. I'm quite impressed how easy it engraved, I was using <5ma at 400mms"
date: May 26, 2015 21:29
category: "Object produced with laser"
author: "David Wakely"
---
Engraved this today. I'm quite impressed how easy it engraved, I was using <5ma at 400mms. I think it's come out pretty good. I am using a rubber wood chopping board as the material

![images/cebd093580a1c08d85001974a48deb84.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cebd093580a1c08d85001974a48deb84.jpeg)



**"David Wakely"**

---
---
**Venuto Woodwork** *June 20, 2015 03:22*

This looks really nice. But I'm wondering where did you have to start the laser for this project to work. We're you able to start in the center and it work around. It's don't know if you can with the program that comes with the laser if I can start a project from the center.


---
**David Wakely** *June 20, 2015 09:11*

Hi there. Is what I done is create a white square the same size of the chopping board and then lined it up with the top right corner of the board. To get alignment correct I used a square of clear acrylic and done test fires on the acrylic to make sure it was aligned correct. The size of the chopping board is 300x200 so it's at the limits of the cutting area so try to keep your design in the centre. Hope this helps!


---
**Fadi Kahhaleh** *July 07, 2015 03:25*

David, I couldn't help but notice, the restaurant name is "Loop" and it is written in Arabic as well. Am I right? :)


---
**David Wakely** *July 07, 2015 06:49*

I have no idea if it is in Arabic or not I just googled restaurant logo templates and it came up :)


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/ChHxRcFQiQa) &mdash; content and formatting may not be reliable*
