---
layout: post
title: "I have a 3D printer air assist nozzle coming soon"
date: October 27, 2016 21:18
category: "Modification"
author: "Gerard Mullan"
---
I have a 3D printer air assist nozzle coming soon. What pump do you guys recommend for pumping air?  Will another aquarium pump left out in the air work the same?





**"Gerard Mullan"**

---
---
**Timo Birnschein** *October 27, 2016 21:59*

I have this: [http://www.ebay.com/itm/O2-Commercial-Air-Pump-1746-GPH-Aquarium-Fish-Pond-Hydroponics-Aquaponics-/262234737912?hash=item3d0e688cf8](http://www.ebay.com/itm/O2-Commercial-Air-Pump-1746-GPH-Aquarium-Fish-Pond-Hydroponics-Aquaponics-/262234737912?hash=item3d0e688cf8)

Plenty of power! You just need to add a little air tank to dampen the vibrating air to a steady stream. That is easily done using HomeDepot parts.

[ebay.com - Details about  O2 Commercial Air Pump 1746 GPH Aquarium Fish Pond Hydroponics Aquaponics](http://www.ebay.com/itm/O2-Commercial-Air-Pump-1746-GPH-Aquarium-Fish-Pond-Hydroponics-Aquaponics-/262234737912?hash=item3d0e688cf8)


---
**K** *October 27, 2016 23:03*

Here's the same one on Amazon.  It's the one I bought.

[https://www.amazon.com/gp/product/B002JPPFJ0](https://www.amazon.com/gp/product/B002JPPFJ0)



Timo, could you detail what you did to build an air tank. I don't have one on mine, and I'm wondering if it will make a difference.

[amazon.com - Amazon.com : Active Aqua Commercial Air Pump, 12 Outlets, 112W, 110 L/min : Pressure Washer Pumps : Patio, Lawn & Garden](https://www.amazon.com/gp/product/B002JPPFJ0)


---
**Scott Marshall** *October 28, 2016 02:24*

To answer the pump question, No, a water pump will not work to pump air if left in the air. Good question.



The reason is because the 2 pumps use different methods of moving the fluids (air is a fluid in many of it's properties).



That was the quick answer, but those of you who know me well, know there's more to the story...

Caution - tech speak below:



The water pump uses a rotating impeller. This is basically a disk with blades on it.  The inlet feeds water to the center of the disk. As it spins, the water is carried along in the 'slots' and centrifugal force forces it to the outside where it's channeled to the discharge. For this to all work, the pump takes advantage of two of the waters properties (that air doesn't have). One is the mass. Water is heavy, so you don't have to spin it vary fast for it to exert a great amount of centrifugal force. The other is that it is relativity viscous (compared to air). This limits the amount of water that leaks thru the internal gaps necessary for the pump to function.  This is also why this style of pump "shuts off" at a rather low pressure. As the pressure increases, the leaks become larger until the volume of water escaping around the rotor is equal to the amount being pumped.



A typical air pump uses a different principle.  The general class of pumps are known as positive displacement  pumps. This is because they pump a given volume of material per operating cycle with no (or very little) internal leakage.



These pumps use valves to draw air into into a cavity and then move it thru and out another valve. The key to these pumps is an intake and discharge valve, and a sealed vessel which is changed in size to move the fluid (yes, these will often work for water if designed properly)



Common examples of these pumps are piston pumps and diaphragm pumps (like those above).



Piston and some diaphragm pumps use a crankshaft to operate the variable size chamber either by moving a piston within a cylinder or by moving the diaphragm in that pump.



When the chamber is at minimum volume, the intake valve opens, either by fluid pressure (like a check ball or reed valve) or mechanically (like a poppet valve in an IC engine). this valve remains open until the cavity reaches maximum volume, at which time it closes and the discharge valves opens. As the chamber reduces in volume, it moves fluid (compressing it if it's a compressible fluid like air) out the valve, and repeats.



The Pumps shown above use a reciprocating motor to operate a diaphragm (and occasionally you find one using a piston). This motor is a pair of electromagnets, which alternately draw an iron rod attached to the diaphragm toward opposite ends of the pump. There are many variations on this type of pump. Some use a single diaphragm, some use 2 diaphragms connected by a common rod, and some use a single magnetic coil and a return spring. An example of a very simple, yet effective air pump is the little vibrating version you see on small aquariums. This has a bar mounted on a rubber hinge, has a magnet on the opposite end of the and a rubber cup shaped diaphragm connected to the bar at it's midpoint. A simple electromagnetic coil swings the magnet back and forth at 50 of 60 times a second (line frequency), pumping a minuscule amount of air at each stroke, but doing it very fast, moving an impressive amount of air for a $5 pump. (but not enough for our purposes).



The one big disadvantage of these pumps is the noise. The pulsation is noise, and you can hide it, or minimize it, but it's unavoidable. The start and stop of the flow are basic to the operating principle.



The centrifugal style pump is smooth and quiet. It's weakness lies in it's internal leaks, which are also unavoidable. They are the reason it's efficiency suffers if it operates at any substantial pressure, and this also creates inefficiency. Incredibly, centrifugal water pumps are available which pump up to 400PSI and higher (these are used in Reverse Osmosis systems - a popular manufacturer is Tonka-Flo) and the trick is that these pumps have a long shaft with dozens of impellers on them, each outlet feeding the next inlet. Internal losses are reduced, pressure increased, but cost is also significantly increased.



There is probably more than you wanted to know about pumps.



I find this stuff fascinating (you may be able to tell) because it's physics put directly into practice, and a simple principle like centrifugal force can be practically used to create a device that will pump either huge volumes of water (a fire truck pump is centrifugal), or pump very high pressures. 



It's like a giant science experiment.



Scott






---
*Imported from [Google+](https://plus.google.com/117648854578727329148/posts/Uth2TTEepxu) &mdash; content and formatting may not be reliable*
