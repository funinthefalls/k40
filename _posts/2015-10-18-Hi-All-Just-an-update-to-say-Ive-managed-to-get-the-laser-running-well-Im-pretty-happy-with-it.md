---
layout: post
title: "Hi All! Just an update to say I've managed to get the laser running well & I'm pretty happy with it"
date: October 18, 2015 03:46
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Hi All!



Just an update to say I've managed to get the laser running well & I'm pretty happy with it.



I've noticed a lot of you have some modifications that you have added to your basic K40s & am wondering what you would recommend as priority.



I've seen some of you have air assist, others have laser pointer/guides, others have upgraded the mainboard, etc.



I'll give the basic details of my machine:



- K40 laser cutter/engraver

- 300 x 200mm cutting/engraving area (according to specs)

- Removed the clamp thing

- No air assist

- No laser pointers/guides

- No lighting

- Moshi Board



As I am limited in finances I would like to prioritise which upgrade I install first based on your opinions as to which would be my best option for first upgrade.



Main things I would like to be able to do with this machine is:



- engrave/cut leather (natural vegetable tanned hide, anywhere from <1mm - 5mm thickness)

- engrave/cut cardboard/paper (something in the range of 310-420 gsm)

- use Adobe Illustrator (.ai) files instead of having to use Corel Draw (is this even possible with this machine?)



For those of you who have performed upgrades, can you please share your opinion on which would be the most priority?



Thanks in advance :)





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Stephane Buisson** *October 18, 2015 09:29*

differrent way to prioritise.

some upgrade are soo cheap like cross hair laser line.

Air assist is one of the main you should consider it's perform a double function (increase combustion with O2, and blow the flame), it's cost a little compressor and some plastic pipe.

more costly will be to change your electronic, but yes it could allow you to work with illustrator.

You should have a look too inkscape (free).

the 4 main options to upgrade electronic are:

-DSP (expensive, proprietary)

-Laos (expensive, open source)

-Smoothieboard (my favorite, work with Visicut (so with illustrator and inkscape))

-Ramps (unexpensive, but cutting only, more complexe software chain)



as for category Other, I will add something soon, keep on reading this community.


---
**Gregory J Smith** *October 18, 2015 10:02*

Number 1. Lid switch to turn off laser when the lid is lifted. Of the others I've found air assist to be most valuable - especially cutting or engraving any sort of wood. It really cuts down the smoke marks.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 18, 2015 10:14*

**+Stephane Buisson** When you say the air assist just requires a little compressor, does that mean it has to be compressed air? Or would something like an aquarium air pump be enough?


---
**Stephane Buisson** *October 18, 2015 11:29*

**+Yuusuf Salahuddin** you don't need a lot of pressure, I personnaly  re-use my aircompressor for paint gun. go with what ever you have handy near you, you can always reduce the amount of air going in the K40.


---
**Phillip Conroy** *October 18, 2015 12:26*

Water flow switch is the number one thing i wish i had added,forgot to turn pump on and blow a $195 laser tube as well as a $160 dollar power supply,and the main thing that hurts the most was i had a water flow switch sitting on my computer desk for 2 weeks when it happened,,bugger,bugger, as well as lossing 4 weeks waiting for parts


---
**Phillip Conroy** *October 18, 2015 12:32*

i use a few different programs with my cutter,paintshop pro 5-for resizing and cropping files-illistrator to convert file formats from .ai to svg so that coral draw can open them,and coral draw to cut files.i upgraded the coral draw that came with the laser cutter to the latest coral draw X12 i think it is off hand


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/UvCW7MFGsUU) &mdash; content and formatting may not be reliable*
