---
layout: post
title: "New front panel to accomedate the GLCD"
date: September 04, 2016 15:23
category: "Smoothieboard Modification"
author: "Mircea Russu"
---
New front panel to accomedate the GLCD.

Love the rocket launch Laser Switch :)

![images/bf188e447da618f68cb0b1966a0c7074.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf188e447da618f68cb0b1966a0c7074.jpeg)



**"Mircea Russu"**

---
---
**Jez M-L** *September 06, 2016 10:37*

Mircea,

What are you using to produce the temps?  I am thinking of implementing the same thing as I keep forgetting to turn things on when starting a cut.



And what is that next to your K40? looks interesting!


---
**Mircea Russu** *September 06, 2016 10:48*

To read the temps I use an arduino and ds18b20 sensors. [https://github.com/executivul/LaserSafety](https://github.com/executivul/LaserSafety) To the right are the fan, the air-assist compressor and the chiller.


---
**Jez M-L** *September 06, 2016 11:33*

Thanks Mircea,  what flow meter do you use please?


---
**Mircea Russu** *September 06, 2016 14:50*

[http://www.aliexpress.com/item/G1-2-Water-Flow-Sensor-Fluid-Flowmeter-Switch-Counter-1-30L-min-Meter-E2shopping/32617842149.html](http://www.aliexpress.com/item/G1-2-Water-Flow-Sensor-Fluid-Flowmeter-Switch-Counter-1-30L-min-Meter-E2shopping/32617842149.html)


---
**Mircea Russu** *September 06, 2016 14:55*

I shall update the github code to include the flow sensor detection.


---
**Robi Akerley-McKee** *September 06, 2016 18:09*

What arduino are you using? This would be nice to use as laser peripherals in marlin.  If everything good on marlin i can read pin 11 i think for status.  


---
**Mircea Russu** *September 06, 2016 19:58*

I use a nano and keep the safety system independent of the controller. 


---
**Mircea Russu** *September 06, 2016 20:14*

You can check the github. I've updated the code to the latest.




---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/c31xN9WApgJ) &mdash; content and formatting may not be reliable*
