---
layout: post
title: "I am trying to download CorelLaser from the site but my virus scanner (and Chrome too) block it as a virus carrier"
date: December 24, 2015 05:17
category: "Discussion"
author: "Andrew Cilia"
---
I am trying to download CorelLaser from the [www.3wcad.com](http://www.3wcad.com) site but my virus scanner (and Chrome too) block it as a virus carrier.



Is anyone else experiencing this?

can someone put a clean CorelLaser.exe on a dropbox? I am stuck.





**"Andrew Cilia"**

---
---
**I Laser** *December 28, 2015 09:55*

Here's a link to the CD from the eBay seller I bought my latest machine from that is corel based: [https://www.sendspace.com/file/3z123n](https://www.sendspace.com/file/3z123n)



Let me know if that triggers your anti virus!


---
**Andrew Cilia** *December 28, 2015 17:58*

installed and worked! thanks.



now to the next issue: when cutting with CorelLaser the head is slow and changes on the speed setting only affect the in-between moves but not the cutting moves. 


---
**I Laser** *December 29, 2015 00:39*

Make sure you have the correct board selected in corellaser's settings. Just check for a sticker on the board in your cutter and select the matching board in corel.


---
**Jeremy A** *July 30, 2016 19:09*

I'm afraid that I have the exact same issue even when I downloaded your file from sendspace. The install software is clean but during the installation process it installs a backdoor trojan (assemble.dll) in the CorelLaser folder. My virus software catches it everytime & the program wont even open without it


---
**Sean Harrington** *August 01, 2016 16:55*

**+Jeremy A** So, what SW are you running then? Are you using a stand-alone Windows system, virus be damned, or what? I just picked up a K40 as well and am trying to determine the best course of action. I'm in no hurry to start cutting, so if I had to bite the bullet and upgrade to an RDWorks-compatible controller, I could... just would prefer to make sure everything works in stock config before I let the vendor off of the hook.


---
**I Laser** *August 01, 2016 22:49*

Fire it up in a VM if you're concerned about the virus flag.


---
**Sean Harrington** *August 01, 2016 22:55*

I haven't done that before. Not sure how to get started, and wondering whether it would be easier to rebuild one of my old HP Windows PCs from the RESTORE partition.


---
**Eric Flynn** *August 05, 2016 03:14*

The virus warning is a false positive.  Not uncommon with software written with Chinese compilers.


---
**I Laser** *August 05, 2016 03:58*

Making a virtual machine isn't hard. But if you have old hardware sitting around that can be re-purposed then probably better using that.



I've got an old Core 2 duo running my machines and never had any issues with the software even though it did flag an AV warning.


---
**Steven Whitecoff** *February 22, 2017 14:58*

AFAIK, the 'backdoor" just opens the 3wcad website. Should be able to block it from internet access but I havent tried yet.




---
*Imported from [Google+](https://plus.google.com/109808417827809372461/posts/QmE8jfHZt8m) &mdash; content and formatting may not be reliable*
