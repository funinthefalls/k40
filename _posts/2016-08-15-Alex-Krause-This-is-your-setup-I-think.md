---
layout: post
title: "Alex Krause This is your setup I think"
date: August 15, 2016 06:03
category: "Discussion"
author: "Anthony Bolgar"
---
**+Alex Krause** This is your setup I think.

![images/4aaa88c477ebe36d5d1fed6c7b186dff.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4aaa88c477ebe36d5d1fed6c7b186dff.png)



**"Anthony Bolgar"**

---
---
**Alex Krause** *August 15, 2016 06:07*

Nope wrong pin that's where the potentiometer hooks up I'm talking about the 4 pin molex connector that connects the nano board to the psu. I wired my unit with the single pin config... I don't have Pwm connected to the center tap of the pot it's connected to the fire pin that the nano board supplies the on off trigger for


---
**Anthony Bolgar** *August 15, 2016 06:09*

There are only 2 choices forr voltage on the PSU 5volt and 24volt. Pretty sure it is not 24v that you need;) So by process of elimination it would be 5V.


---
**Anthony Bolgar** *August 15, 2016 06:11*

See [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide) for PSU setups about 1/4 the way down the page. Every setup I have found on the web shows L being fed by 5 Volt


---
**Alex Krause** *August 15, 2016 06:24*

Sent you a offline message the L line I'm talking about is the one on the far right of the PSU on the blue box diagram that doesn't have any wires connected to it in the diagram... most confusing crap every marking 2 pins as L 


---
**Sebastian Szafran** *August 15, 2016 06:41*

**+Alex Krause**​ there are 2 L and one L- (ground for the laser tube). Am I right to understand that pot is connected to L on the right connector (marked red on the your picture on your other post)?


---
**Alex Krause** *August 15, 2016 06:46*

**+Sebastian Szafran**​ I left all the connectors alone from the stock board except the 4 pin connector that is normally ran from the stock controller to the psu the one listed as 24v,g,5v,L I'm running my smoothie pwm to that L. The other L to the left of the Potentiometer connections is the test fire push button on the unit... I assume that inside the PSU those two pins are connected 


---
**Don Kleinschnitz Jr.** *August 15, 2016 12:44*

**+Alex Krause** you are having problem getting the tube to full power? 

You have an Open Drain smoothie output connected to the "L" pin on the LPS dc connector? 

The "L" requires ground to fire. 

If you read it when open with a meter it will read + voltage. Min was 4.5vdc.

If the above is true you need to insure that your PWM signal is not inverted.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/9xhhmjxsHcV) &mdash; content and formatting may not be reliable*
