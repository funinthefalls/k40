---
layout: post
title: "What versions of CorelDraw are people using, is tne standard 12 any good"
date: May 03, 2016 23:05
category: "Discussion"
author: "Tony Schelts"
---
What versions of CorelDraw are people using,  is tne standard 12 any good. 





**"Tony Schelts"**

---
---
**Anthony Bolgar** *May 03, 2016 23:15*

I use X7, but I get a free copy every year as a beta tester. (Waiting for my X8)Version 12 has all the features you would ever need to create files for the laser. In reality version 3 or 4 would be fine.


---
**Jim Hatch** *May 03, 2016 23:18*

CorelDraw 12. But it's a legally licensed and registered version of my own, not the one that came with the laser.


---
**Mishko Mishko** *May 03, 2016 23:44*

X7, but as Anthony said, just about any version will do if you need it for laser stuff only...


---
**TinkerMake** *May 04, 2016 03:39*

Using Version 12 myself.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/BKxuE3wV7GT) &mdash; content and formatting may not be reliable*
