---
layout: post
title: "This was fun to make. I used the app Gear Generator then tweaked the exported file a little"
date: September 08, 2016 18:22
category: "Object produced with laser"
author: "Jeff Johnson"
---
This was fun to make. I used the app Gear Generator then tweaked the exported file a little. I still need to work on my power and cut times but I'm getting closer. Also, it seems that this is really inconsistent plywood. It's the cheapest 5mm I could find at the hardware store. It gets about a 95% cut with one pass at about 12ma and 20 mmps. the remaining 5% must be hardwood or other debris that can take 3 or 4 more passes to cut, burning everything else. I'll try this again on MDF or try to find better plywood.

![images/687cec3870b28b0e3efef0c337a657ad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/687cec3870b28b0e3efef0c337a657ad.jpeg)



**"Jeff Johnson"**

---
---
**Scott Marshall** *September 08, 2016 18:36*

It's the glue. 

When you get voids, they fill in with the glue, which seems laser resistant, to put it mildly.



There was a place Something "Hardwoods" on ebay that had very good 1/8" for a buck a square foot if you bought 25 or 50 12" squares, but they seem to have disappeared as of late.



If I could get more of that ply, I'd be happy to laminate my own 1/4" for gears etc. 

Right now, I 'm conserving my supply until the source shows back up. 



PS



Update:



This got me thinking, and I went out and dug through the box of good stuff.



I found a packing list 

KenCraft Co Inc

821 N Westwood 

Toledo Ohio 43607

HARDWOODS@Kencraftcompany.com



It's 40 sheets 12 x 12 (11 7/8) in a flat rate box for $49.95



I'm guessing they have other thicknesses.



It's good cutting plywood, puts Midwest to shame. 



Scott


---
**Jim Hatch** *September 08, 2016 18:37*

yeah, you'll find standard plywood is really hard to get consistent results out of. Baltic Birch is a lot better (but still can give you some issues with glue spots or other internal defects causing cut problems). MDF is very consistent but messy. Chipboard is really good but not as sturdy as MDF or plywood over time.




---
**Jeff Johnson** *September 08, 2016 18:49*

I really appreciate the tips. Thanks.


---
**greg greene** *September 08, 2016 19:16*

My local hardware store sells 5 x 5 foot baltic birch for 17 buckx - canadian - a few minutes on the table saw gives me all the sizes I need for any project that fits the cutter


---
**Derrick Armfield** *September 09, 2016 13:32*

Glad I saw this.   Have been puzzled about this phenomenon also!   It does make sense as the edges of the cut are sticky.   


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/RfDZ5uxCckY) &mdash; content and formatting may not be reliable*
