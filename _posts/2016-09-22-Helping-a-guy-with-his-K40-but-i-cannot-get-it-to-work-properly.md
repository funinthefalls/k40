---
layout: post
title: "Helping a guy with his K40, but i cannot get it to work properly"
date: September 22, 2016 15:14
category: "Original software and hardware issues"
author: "HP Persson"
---
Helping a guy with his K40, but i cannot get it to work properly.



1: Test-fire works, needle moves to ~12mA were its set (sometimes no matter if laser switch is on/off)

2: Running the software, tested both LaserDRW and Corellaser, head moves - no beam.



We have checked all cables, no loose ones.

Also checked the serial on the board, correct board is choosen, all other boards tested - no beam.



So i came to the conclusion, either is the board broken or some cable in the wrong spot. Hit me up with ideas if you have any to test :)



Here is his PSU, below is the cables listed, where they go.



Connector 1

Cable 1: mA-meter

Cable 2: Gnd

Cable 3: Power

Cable 4: Power



Connector 2, middle

Cable 1: Test fire button

Cable 2: Test fire button

Cable 3: Laser switch

Cable 4 to potentiometer and laser switch

Cable 5 and 6 goes to the potentiometer



Connector 3, right

Cable 1: 24v

Cable 2: Ground

Cable 3: 5V

Cable 4: LO



Can someone comfirm this is correct, or wrong? Regarding connector 2 and 3.

I have seen alot of PSU schematics, but none looks like this.



My PSU does not look like this, so i cant compare to my own laser either.



![images/2089c03624f68f181f15bc8cdc80a4c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2089c03624f68f181f15bc8cdc80a4c5.jpeg)



**"HP Persson"**

---
---
**Ariel Yahni (UniKpty)** *September 22, 2016 15:25*

Can you see the plasma on the tube while pressing the fire?


---
**HP Persson** *September 22, 2016 15:27*

**+Ariel Yahni** Yes, beam works perfectly, even burned the owners finger once when he didnt beleive the tube was working. 

Everything checks out, except the software cant fire the beam :)


---
**Ariel Yahni (UniKpty)** *September 22, 2016 15:42*

If you can see the plasma either by test fire button of software then my call it that the beam is not aligned and its not reaching or coming out from the focus lense


---
**Ariel Yahni (UniKpty)** *September 22, 2016 15:44*

If i did not understand correctly te beam exits correctly then the only cable you need to check is the LO from the last conector which is actually the only one that send the signal from the board to the laser


---
**HP Persson** *September 22, 2016 16:03*

Plasma is seen on test fire, not fire trough software, the head just dances around. 

We´ll inspect the LO a bit more.




---
**HP Persson** *September 22, 2016 16:26*

**+HalfNormal** It is, and the test-fire sometimes works without it beeing enabled. Not sure if it should be like that, only have digital panel myself.



Test-fire button - plasma seen, material is marked by the beam and mA-meter goes up to set value.



Running a job: - head moves, no plasma, no beam mark, no movement on the ma-meter needle.



Has been tested with settings and buttons in all different configurations, no beam.



This is why i´m confused :)


---
**Don Kleinschnitz Jr.** *September 22, 2016 17:11*

The LO from the board to the DC connector fires the laser during operation from the controller (if enabled). The test button operates independent of the controller. I would find out why the laser fires without enable as that is dangerous and also might provide a hint as to what is going on. 

Perhaps some weird wiring problem. Here is a link to the schematic for my K40. Although it has a different LP supply it works pretty much the same but wired with green instaed of white connectors.. [digikey.com - SchemeIt &#x7c; Free Online Schematic Drawing Tool &#x7c; DigiKey Electronics &#x7c; K40 wiring](http://www.digikey.com/schemeit/project/k40-wiring-GCKOUK0100Q0/)


---
**HP Persson** *September 22, 2016 17:14*

**+Don Kleinschnitz** Thanks Don! We are also checking all buttons now to verify they are working properly. I´ll have a look at the schematic you linked! 


---
**Don Kleinschnitz Jr.** *September 22, 2016 17:23*

One more hint. This shows how the buttons are wired on that type of supply. Ignore the smoothie stuff ...

[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)


---
**Matt Herrera** *September 22, 2016 17:37*

First problem is your still using the stock control board. Upgrade to a Smoothieboard and use laserweb. 


---
**HP Persson** *September 22, 2016 17:49*

**+Don Kleinschnitz** Yeah, i saw that earlier too, but his buttons are wired completely different.



In that schematic, laser enable/laser switch is cable 2 (P) and 4 (G), on this guy´s laser its 3 (L) and 4 (G)

May this be the issue ? Noticed this before but didnt want to start swapping cables around as step 1 :)


---
**HP Persson** *September 22, 2016 19:35*

Problem solved, cable 2 and 3 swapped, and now its working properly, the switches were connected all wrong.



Thanks everyone for hints and links :)


---
**Don Kleinschnitz Jr.** *September 23, 2016 00:45*

One more K40 comes alive ...lol!


---
**Don Kleinschnitz Jr.** *September 23, 2016 12:27*

BTW did the machine come wired wrong?




---
**HP Persson** *September 23, 2016 16:16*

Yeah, it was wired wrong on cable 2 and 3 in the middle connector. When we changed it, all worked good again.


---
**Don Kleinschnitz Jr.** *September 23, 2016 16:30*

Note to self. Don't assume K40 manufacturers test machines. 


---
**HP Persson** *September 24, 2016 10:10*

Oh yeah, worst i seen was a 3-prong power cable delivered with the laser, but the 3rd ground prong was not connected to the other side of the cable. Someone could have died.


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/LJ9M7wJqh4s) &mdash; content and formatting may not be reliable*
