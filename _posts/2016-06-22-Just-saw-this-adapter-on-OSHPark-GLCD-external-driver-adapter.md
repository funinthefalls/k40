---
layout: post
title: "Just saw this adapter on OSHPark, GLCD + external driver adapter"
date: June 22, 2016 07:47
category: "Smoothieboard Modification"
author: "Jean-Baptiste Passant"
---
Just saw this adapter on OSHPark, GLCD + external driver adapter.



[https://oshpark.com/shared_projects/AE7ljT4W](https://oshpark.com/shared_projects/AE7ljT4W)



Is anyone interested in one ?



They sell it by 3 and I only need one.



Total is 20$ per piece, I pay the shipping fee to anywhere in the world.





**"Jean-Baptiste Passant"**

---
---
**Stephane Buisson** *June 22, 2016 12:21*

available here, official and cheaper (with connector)

**+Jean-Baptiste Passant** support Smoothie as well.

[http://robotseed.com/index.php?id_product=23&controller=product&id_lang=2#/gcld_acc-pcb_shield_adapter_soldered/glcd_panel-shield_pcb](http://robotseed.com/index.php?id_product=23&controller=product&id_lang=2#/gcld_acc-pcb_shield_adapter_soldered/glcd_panel-shield_pcb)

**+Arthur Wolf** 


---
**Jean-Baptiste Passant** *June 22, 2016 12:59*

**+Stephane Buisson** Nope, only glcd on robotseed. This one have external motor controller.


---
**Stephane Buisson** *June 22, 2016 13:19*

what for ??? where this things come from ???


---
**Jean-Baptiste Passant** *June 22, 2016 13:34*

To get up to 128 microsteps with a TCM2100/silentstepstick.

I believe the one who designed it made it with the information you can find here : [http://smoothieware.org/general-appendixes#toc5](http://smoothieware.org/general-appendixes#toc5)


---
**Matt Wils** *June 27, 2016 03:20*

Ill try one out


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/jjoTn5TTqUs) &mdash; content and formatting may not be reliable*
