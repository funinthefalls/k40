---
layout: post
title: "Clean gantry, better drag chain setup, I'm happy... for now"
date: October 11, 2016 01:55
category: "Modification"
author: "Ashley M. Kirchner [Norym]"
---
Clean gantry, better drag chain setup, I'm happy... for now. 

![images/9b8bb834f71eacaa81ce16441db9cc62.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b8bb834f71eacaa81ce16441db9cc62.jpeg)



**"Ashley M. Kirchner [Norym]"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 11, 2016 02:08*

If there was a "K40+++" this would be it. 


---
**Anthony Bolgar** *October 11, 2016 11:39*

Looks very nice. Great job.


---
**greg greene** *October 11, 2016 13:41*

nice work hold down pieces


---
**Ashley M. Kirchner [Norym]** *October 11, 2016 16:36*

The chain is connected to the side of the moving gantry. Then there's another piece that's attached from there to the back of the machine where the air comes in. I 3D printed both brackets, one on the head and the other on the side. 


---
**Ashley M. Kirchner [Norym]** *October 11, 2016 16:36*

I'm take a picture when I get home after work. 


---
**Ashley M. Kirchner [Norym]** *October 11, 2016 17:49*

There are two completely separate chains here, one at the top, and one on the side. They both connect to the side plate that's attached to the moving gantry. Somewhat similar to how an X-Carve is setup: [https://discuss-assets.s3.amazonaws.com/optimized/2X/b/b584363ad64d612e1afd24313577624601125085_1_666x500.jpg](https://discuss-assets.s3.amazonaws.com/optimized/2X/b/b584363ad64d612e1afd24313577624601125085_1_666x500.jpg)


---
**Ashley M. Kirchner [Norym]** *October 11, 2016 17:50*

The bracket on the right is attached to the moving gantry, and it has two connecting points, one at the top to receive the upper chain, and one on the bottom, that's for the bottom chain. The airhose (and future wires) simply snake through it from the top to the bottom (or bottom-up, whichever you prefer.)


---
**Ashley M. Kirchner [Norym]** *October 11, 2016 18:26*

That would make for a rather limber piece of cable chain. :)


---
**Ashley M. Kirchner [Norym]** *October 11, 2016 20:44*

Modeling the bracket and attached cable chains and it looks like this, viewed from the side: [http://www.thekirchners.net/bracket.png](http://www.thekirchners.net/bracket.png)

[thekirchners.net - www.thekirchners.net/bracket.png](http://www.thekirchners.net/bracket.png)


---
**Ashley M. Kirchner [Norym]** *October 11, 2016 20:46*

The upper gantry has 4 screws on the side, the bottom two pull the idler that holds the belt tight, and the upper two hold the actual beam. The bracket is designed so only the upper two need to be loosened, the bracket slid over them, and screwed down. The bottom screws will be accessible through the larger holes.


---
**ALFAHOBBIES** *April 15, 2017 12:15*

Is this 3d printed? Do you have the files for it if it is?


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/cRAJFM4HAjK) &mdash; content and formatting may not be reliable*
