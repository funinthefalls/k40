---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) Having a bit of a play around with the laser & an idea I had for luggage tags"
date: April 06, 2016 15:28
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Having a bit of a play around with the laser & an idea I had for luggage tags. Couple of things slightly off on this attempt (e.g. I made the insert slightly too long & I also was intending to use a Chicago Screw to hold the strap to the main piece, however it is not long enough to go through 6 layers of leather + 3mm ply). I've used a small padlock temporarily to hold it shut for the purpose of the photos.



Ply insert: 10mA @ 300mm/s engrave + 10mA @ 12mm/s cut.

Leather exterior: 5mA @ 300mm/s @ 2 pixel steps engrave + 2 passes @ 10mA @ 20mm/s cut



![images/ba98e7cd85446ba2a7881624afc22f84.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ba98e7cd85446ba2a7881624afc22f84.jpeg)
![images/b8682e796ac8ae646f844a4bd441795b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8682e796ac8ae646f844a4bd441795b.jpeg)
![images/5cf35c809427977f367e4d3b5962e8bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5cf35c809427977f367e4d3b5962e8bc.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Scott Thorne** *April 06, 2016 16:11*

Lol...love the tags you made Yuusuf!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 16:25*

**+Scott Thorne** Thanks Scott. Few tweaks needed to the design.


---
**Scott Thorne** *April 06, 2016 17:04*

**+Yuusuf Sallahuddin**...that's where the red dot pointer comes in handy...you can do a practice run with the laser off to get everything aligned before you start. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 17:39*

**+Scott Thorne** Actually, it was me not taking into account the thickness of the plywood when determining the leather exterior size. But you're right, a red dot pointer would be helpful for doing test runs. Plenty of things I need to get to improve my machine/process. But I hate waiting for things to arrive in the mail haha.


---
**Scott Thorne** *April 06, 2016 18:33*

**+Yuusuf Sallahuddin**...I agree completely....there is a feature on my machine that allows you to do a  "frame" test....with the laser off and only the pointer on, you can see exactly where you will start and stop.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 18:52*

**+Scott Thorne** That's a nice feature. You know I've seen some machines that use a DLP projector (or something like that) to project the image of what you will be engraving/cutting onto the workpiece so you can see if it will put cut within the material/adjust the layout to suit for least wastage etc. I'd love to rig something like that up as a mod.


---
**Dennis Fuente** *April 06, 2016 19:25*

i like sheild also nice work 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 19:46*

**+Dennis Fuente** Yeah I got the S.H.I.E.L.D. logo to do another one later. This was just a test & I always find the Hail HYDRA quote amusing.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 01:05*

**+Carl Duncan** Awesome. The Captain America movies are great. Hail HYDRA!


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/E6RNLcBpy7t) &mdash; content and formatting may not be reliable*
