---
layout: post
title: "Hi all. New here, just got my K40 this weekend"
date: June 20, 2016 07:49
category: "Original software and hardware issues"
author: "Imnama"
---
Hi all. New here, just got my K40 this weekend. I'm using correldraw x7 and corelLaser. When I press the cut button, in the next screen you can set the cutting speed. No matter what I put there, the speed will always be something like 2 mm/sec. I'ts not possible to cit cardboard at his speed, it will catch fire.  Is there a way to increase this speed.?





**"Imnama"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 20, 2016 08:18*

Make sure you set the Board ID & the Model on the Device Initialise settings page. see: [https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)



Also, make sure to set the CorelDraw settings too. see: [https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)



Hopefully that will sort out the issue.


---
**Pigeon FX** *June 20, 2016 18:13*

**+Yuusuf Sallahuddin** Yes, this was the exact problem I had when I first used my laser, as I had not set the Board ID. 


---
**Imnama** *June 20, 2016 22:12*

Thanks for the help. It turns out that the wrong board was selected. LaserDRW had the correct board setting. After copiing that setting into corel everything works fine


---
*Imported from [Google+](https://plus.google.com/105177731458209028076/posts/L6kMSzsU1R3) &mdash; content and formatting may not be reliable*
