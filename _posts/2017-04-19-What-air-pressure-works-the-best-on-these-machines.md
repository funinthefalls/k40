---
layout: post
title: "What air pressure works the best on these machines?"
date: April 19, 2017 18:10
category: "Hardware and Laser settings"
author: "Jared Cochran"
---
What air pressure works the best on these machines? I am working on finally getting mine ready to fire up and this was one thing that I did not see much info about. Thanks





**"Jared Cochran"**

---
---
**Anthony Bolgar** *April 19, 2017 18:13*

I don't know what others are running, but I get good results at 10PSI


---
**Jim Fong** *April 19, 2017 18:38*

I use a airbrush compressor that I bought at harbor freight for around $50.  No complaints.  Usually leave it on full output but if I cut paper, have to turn it down.  There is enough air flow to shift smaller pieces of paper around. 


---
**Steve Clark** *April 19, 2017 20:08*

I'm running at about 10 to 12 PSI through a regulator but I can't tell you the air flow. I also have a needle valve to control that flow.


---
**Ned Hill** *April 19, 2017 21:36*

It depends somewhat on what air assist head you are using.  I'm using a lightobject head with a flow meter and I'm running about 15L/min for most things.


---
**Andrew Nichols** *April 19, 2017 21:48*

**+Ned Hill** What pump are you using with your lightobject head? I'm ordering one this week, so I'm trying to get opinions. Also what lens did you go with?


---
**Ned Hill** *April 19, 2017 22:03*

**+Andrew Nichols** I'm using an airbrush pump I got off amazon ([https://www.amazon.com/gp/product/B001BJFHAW/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B001BJFHAW/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1))  It maxs out at about 20l/min.  I also have it running to a 10gal portable air tank from harbour freight to dampen the pump pulsations and to provide some back pressure. These airbrush pumps aren't really meant to be run continuously for long periods so I recommend anyone that goes this route to buy a small personal fan to blow on it to help keep it cool.  A lot of people also go with big aquarium pumps but I don't have any experience with that.  A real air compressor is the best option, but my laser is inside in my study and I didn't want to deal with the noise and running a line from my shop compressor would be a major pain to do.

[amazon.com - Amazon.com: Master Airbrush Compressor with Water Trap and Regulator, Now Includes a (FREE) 6 Foot Airbrush Hose and a (FREE) How to Airbrush Training Book to Get You Started](https://www.amazon.com/gp/product/B001BJFHAW/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)


---
**Ned Hill** *April 19, 2017 22:07*

Oh,  and I got the "Improved 20mm ZnSe Focus lens (F50.8mm)" to go with my head.


---
**Andrew Nichols** *April 19, 2017 22:09*

**+Ned Hill** Thanks! I never thought about using my shop air compressor. I'll just get a smaller regulator and see how that goes. 


---
**Mark Brown** *April 20, 2017 13:46*

I'm not really set up yet, but I was just playing with a nozzle and regulator a few days ago.  The regulator would build to around 15psi if capped off, with the nozzle open it was down to about 5psi with quite a lot of flow, I'd probably end up turning it down some.


---
*Imported from [Google+](https://plus.google.com/101931823941206300500/posts/9aTmuCQjPwM) &mdash; content and formatting may not be reliable*
