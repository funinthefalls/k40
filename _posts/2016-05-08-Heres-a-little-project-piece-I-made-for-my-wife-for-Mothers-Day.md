---
layout: post
title: "Here's a little project piece I made for my wife for Mother's Day"
date: May 08, 2016 18:56
category: "Object produced with laser"
author: "Jim Hatch"
---
Here's a little project piece I made for my wife for Mother's Day. It's 1/4" Baltic Birch living hinge design with a Danish Oil finish. Hardware is from Woodcraft and chain is from Amazon. Glued & pinned (23 ga headless pins). Used embossed felt for the lining - cut to the inside dimensions (sliced off the finger joints for the felt) and attached using 3M heavy duty spray adhesive.



![images/99720430238de1654fe7b675154cc1f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/99720430238de1654fe7b675154cc1f2.jpeg)
![images/aed23647132a11d55b242457d15b1564.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aed23647132a11d55b242457d15b1564.jpeg)
![images/1cde240367d36c869c2db423630166f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1cde240367d36c869c2db423630166f9.jpeg)
![images/0d5e5c7e02329f7d81260f5678e63be3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d5e5c7e02329f7d81260f5678e63be3.jpeg)

**"Jim Hatch"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 19:09*

Wow that's really nice Jim. I hope the wife loves it.


---
**Jim Hatch** *May 08, 2016 19:12*

She did :-) 



I didn't post earlier since I didn't want her to see it before she opened it today. Sons sent flowers, daughter got her a bracelet so all in all she's had a good day (we're heading out to dinner later tonight).


---
**Ian Ferguson** *May 08, 2016 19:27*

That's really nice Jim 


---
**I Laser** *May 08, 2016 20:04*

^

Love the paw prints :)


---
**Jim Hatch** *May 08, 2016 20:34*

**+I Laser** My wife loves dogs - pugs & corgis. We have a corgi now that's a major part of the family.


---
*Imported from [Google+](https://plus.google.com/114480049764906531874/posts/d2bZNWVzyCM) &mdash; content and formatting may not be reliable*
