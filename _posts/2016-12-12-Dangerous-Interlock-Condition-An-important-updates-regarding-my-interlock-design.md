---
layout: post
title: "Dangerous Interlock Condition An important updates regarding my interlock design"
date: December 12, 2016 14:20
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Dangerous Interlock Condition

An important updates regarding my interlock design. If you have been using my approach a change is necessary.

..............

I have been on a tear to get K40 users to:

a. employ interlocks

b. test the interlock system regularly, especially after changes

c. wear protective glasses even if you have interlocks



Here is an example why the above is important: 

...............

While testing I found that due to a design error in the way I connected the "interlock LED" the laser could be fired without the "Laser Switch" engaged. 

Why: Connecting an indicator across a switch to show that it is on or off is a safe approach in most cases. Well, in this case the interlock input on the LPS is the cathode side of an optical isolator so connecting an led from it to gnd creates an indication that interlocks are open but it also biases the +P input pin. This allows the laser to fire when the interlocks are open ......DOH!. Not an obvious mistake!

The posts on the subject have been corrected: 

[http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html](http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html)



The led now indicates that the laser is armed which is better anyway.



 

![images/c7d573e5eccebab9419d5b94b806b22d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c7d573e5eccebab9419d5b94b806b22d.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**greg greene** *December 12, 2016 14:41*

haven't done mine yet - but the plan is a double microswitch  activated  by closing the lid and wired in series with the laser button, one side to the button, the other to a red indicator light.  Simple, direct and pretty much fool proof.


---
**Don Kleinschnitz Jr.** *December 12, 2016 15:05*

**+greg greene** not sure if I am clear on your approach. 

I created the interlock breakout mostly to make the serial wiring easy. 



Be advised that you cannot connect any device, circuit etc to the + side of the "Laser Switch" circuit that draws current or drops voltage. Otherwise interlocks will misbehave. I.E. the only current that that pin should see is the ground from the interlock switch closures. 



The "Laser Switch"  input circuit is not an input to some control logic like one might think. The + side (P+ on mine)  is actually the cathode of the LED transmitter of an opto-coupler. It is intended to be grounded by the Laser Switch to enable the LPS. So if there is any current flowing in/out of that pin other than the ground provided by the interlocks the laser may be enabled.



Hope I did not make this confusing ....:(


---
**Jerry Rodberg** *December 12, 2016 15:08*

I run into issues like this all the time industrially.  The simplest solution is a force guided relay that directly acts to close the relay.  Old nema relays often had a spring isolator in them.  These relays have an additional mechanical cage inside them. This cage implements what are referred to as mirror contacts.  Mirror contacts mean that you cannot have both a normally closed and normally open.  So you wire your arm to a nc. Contact and your indicator to an no contact.  [http://us.idec.com/Catalog/ProductSeries.aspx?SeriesName=RF1V_Series&FamilyName=Relays_And_Sockets](http://us.idec.com/Catalog/ProductSeries.aspx?SeriesName=RF1V_Series&FamilyName=Relays_And_Sockets)


---
**Jerry Rodberg** *December 12, 2016 15:08*

[us.idec.com - IDEC.com - RF Series Series Category](http://us.idec.com/Catalog/ProductSeries.aspx?SeriesName=RF1V_Series&FamilyName=Relays_And_Sockets)


---
**Darren Steele** *December 12, 2016 16:07*

I applaud you Don.  I accidentally test fired the laser into my hand a few days ago.  Not cool, definitely not cool


---
**greg greene** *December 12, 2016 16:08*

That works too, but it takes power from an already dicey PSU.  A dual pole dual throw micro switch take no power, is easy to mount and integrate and is simpler to work with.


---
**greg greene** *December 12, 2016 16:08*

Ouch !!!!!! hopefully it was set to just engrave - and not cut :)


---
**Don Kleinschnitz Jr.** *December 12, 2016 16:18*

**+greg greene** i was just testing the PWM at low power :).


---
**greg greene** *December 12, 2016 16:19*

Glad you were not seriously injured !


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/TVvphjoPTSz) &mdash; content and formatting may not be reliable*
