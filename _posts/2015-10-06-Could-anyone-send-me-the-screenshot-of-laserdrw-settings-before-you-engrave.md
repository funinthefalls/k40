---
layout: post
title: "Could anyone send me the screenshot of laserdrw settings before you engrave?"
date: October 06, 2015 15:20
category: "Discussion"
author: "Raja Rajan"
---
Could anyone send me the screenshot of laserdrw settings before you engrave?



I am struggling a lot ,i could test fire the laser but when i engrave or cut it s not doing anything.I just to want to know where i am missing.



Please help!!





**"Raja Rajan"**

---
---
**David Wakely** *October 06, 2015 15:51*

Hi there,



Have you checked the board type and board id under the settings (gear icon) ?



If so this could be your issue. 



The board ID and type are printed on the controller. The ID is on a white sticker and is quite long.




---
**David Wakely** *October 06, 2015 16:08*

check my latest post i have got a picture of where to find the model and id


---
**Raja Rajan** *October 06, 2015 20:35*

Hi David,yes, i enetred the same model number and my board id..but when i press starting in laserdrw isee the laser head is moving but i dont see any effect in my acrylic sheet..Is there any other setting i need to concentrate?


---
**David Wakely** *October 06, 2015 20:59*

There should be a dial and an mA meter on the panel. The dial should be turned and then test fire until the mA reads around 5.


---
*Imported from [Google+](https://plus.google.com/110717916181924497071/posts/8fCyBTzRZxv) &mdash; content and formatting may not be reliable*
