---
layout: post
title: "Anyone know what causes a single line cut to repeat twice instead of just once?"
date: August 10, 2015 00:35
category: "Software"
author: "chad too (dns)"
---
Anyone know what causes a single line cut to repeat twice instead of just once? I set up a cdr file with coreldraw x7 to just cut the left side off of my acrylic sheets to fit my honeycomb. I've tried different line thicknesses including hairline and it always does the cut down and then back instead of just one way. Any ideas?





**"chad too (dns)"**

---
---
**Cam Mayor** *August 10, 2015 01:07*

Make your area to be cut out a solid.


---
**Cam Mayor** *August 10, 2015 01:18*

Click the fill selection.

[https://drive.google.com/file/d/0B89loHnZjcHjOHU3T2ZWS3lhWUE/view?usp=sharing](https://drive.google.com/file/d/0B89loHnZjcHjOHU3T2ZWS3lhWUE/view?usp=sharing)


---
**I Laser** *August 10, 2015 01:29*

Check the line width is below .07mm, the hairline setting in Coreldraw is above this. Hence it thinks you have a solid object and is trying to cut it out.


---
**Troy Baverstock** *August 10, 2015 05:35*

The output type you choose changes that behavior also. From memory on WMF you need to make it a solid like Cam said or set to PLT it will do it with or without being a solid but seems to cut slighty under scale for me.


---
**Clifford Livesey** *August 10, 2015 05:35*

Change your line width to 0.001


---
**I Laser** *August 10, 2015 05:52*

Yeah my bad, have been quoting .7mm when its < .07mm. Just checked and I've set mine to .01mm


---
**chad too (dns)** *August 10, 2015 16:56*

Ok yeah i've been using WMF. If I don't then all of my dimensions get thrown off by many mm's. I'm not sure why on that either, because it worked perfectly with PLT for cutting before moving over to WIN 7.



Thanks for the advice guys. I'm going to try a solid object and then .01 and share results.


---
**chad too (dns)** *August 10, 2015 18:11*

Welp I still can't get it to just cut a single line pass with WMF. I tried .001 up to .2 mm line thickness, and then the same dimensions with the line converted to an object. It wouldn't even register it as a cut when going into corel laser.  I guess as of now my options are to either switch over to PLT or just stop it after the first pass when I need single line cuts.


---
*Imported from [Google+](https://plus.google.com/108472782099926789468/posts/7EKmrrVSdGE) &mdash; content and formatting may not be reliable*
