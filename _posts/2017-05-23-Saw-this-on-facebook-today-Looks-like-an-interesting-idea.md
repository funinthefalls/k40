---
layout: post
title: "Saw this on facebook today. Looks like an interesting idea"
date: May 23, 2017 01:51
category: "Discussion"
author: "Ned Hill"
---
Saw this on facebook today.  Looks like an interesting idea.  Sometimes get into pieces of  plywood that don't laser very dark and this might be a nice thing to try.  



![images/940ace4b0fb312bf44b94d1a7f5ff6eb.png](https://gitlab.com/funinthefalls/k40/raw/master/images/940ace4b0fb312bf44b94d1a7f5ff6eb.png)
![images/eca07e4be9260db59def3f2b6012e9b0.png](https://gitlab.com/funinthefalls/k40/raw/master/images/eca07e4be9260db59def3f2b6012e9b0.png)
![images/a5a3c5235ffc074353126431008d153d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a5a3c5235ffc074353126431008d153d.png)

**"Ned Hill"**

---
---
**Jim Fong** *May 23, 2017 01:58*

Good tip. What brand mask do you use?


---
**Ned Hill** *May 23, 2017 01:59*

That wasn't me, the pics were part of the FB post.


---
**Jim Fong** *May 23, 2017 02:13*

**+Ned Hill** sorry.   Well since we're on the subject.  Anyone here used laser safe mask before?  I use oramask 813 for my gantry CNC. Great stuff but it is pvc based which is a not laser safe.  Maybe blue tape? Have to look up the properties.  


---
**Gee Willikers** *May 23, 2017 02:13*

I had that same question about the op on Facebook. Light coats over transfer paper I'm assuming but I worry about the solvents bleeding through.


---
**Ned Hill** *May 23, 2017 02:26*

But personal I use 3M delicate surface tape for most small things and RTape Conform 4075RLA  for larger things.


---
**Ned Hill** *May 23, 2017 02:33*

**+Gee Willikers** as long as you don't soak it, the things I mentioned above usually hold up well with most solvent type sprays.



**+Jim Fong** Blue tape works fine for laser engraving.  I prefer the 3M delicate surface (60 day release) blue tape as it's easier to remove.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/Udw3vQCWmoW) &mdash; content and formatting may not be reliable*
