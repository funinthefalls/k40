---
layout: post
title: "Okay, I've been lurking for about three weeks"
date: August 08, 2016 20:56
category: "Modification"
author: "Sean Harrington"
---
Okay, I've been lurking for about three weeks. I ordered my K40-class laser (40W 12" x 16" Ten-High "orange" unit, M2 Nano driver board) back on July 12. It arrived on July 25, and I finally got the chance to uncrate it this past weekend to check for damage/breakage (none found so far).



I'll get the stock configuration up and running this week (summer break from grad school) and make sure the steppers, PSU, controller, and laser are all functional.



I've been planning since I ordered the unit on upgrading the controller, I just wasn't sure on which path to take: SmoothieBoard, D7, or Ruida controller.



I LOVE the work that's being done on the SmoothieBoard and on LaserWeb... so much so that I sent Peter van der Walt a donation because I want to support an OpenSource alternative for laser makers. I can go that route and work my way through the somewhat tedious workflows involved in getting from Adobe Illustrator to finished product (most of my laser designs are already in Illustrator), but honestly, it's not at a level of sophistication I can use in my shop just yet. I will likely upgrade to a larger machine in a year, and will consider going with a SmoothieBoard and LaserWeb then to tinker with... just not right now.



For the D7, I like LightObjects.com's offerings over buying from a random source on eBay. The price is higher than I'd like to pay, and I'm still not certain whether I need the stepper controller interfaces in addition to the D7 unit. Anyone? Finally, while I like LaserCAD, the workflow in getting from Illustrator to finished product is still tedious... seems like the version that ships with the D7 only supports importing of Illustrator files from version 8 or earlier. That can get weird with fonts, which I use as-is (not converted to beziers) in Illustrator so I can quickly edit/update/customize my designs. Not ideal, and fairly expensive, but well-regarded and supported within the laser community.



So, the workflow that is closest to what I am used to is with RDWorks/LaserWorks. It imports Illustrator (CC 2015!) directly, but it still has font issues (won't import them), so again, they must first be converted to filled shapes. Then, there is a bit of tweaking in RDWorks/LaserWorks to tell each object how it should be processed (and I'm still trying to figure out the difference between CUT and PEN mode). I'd prefer to do all of this from within Illustrator and then simply PRINT to the laser, as I do with the Epilog Zing at work, but then again, I don't have $10k+ to drop on a new laser of that expense.



What are your thoughts?





**"Sean Harrington"**

---


---
*Imported from [Google+](https://plus.google.com/113599761111695694509/posts/6nccGpkBgHV) &mdash; content and formatting may not be reliable*
