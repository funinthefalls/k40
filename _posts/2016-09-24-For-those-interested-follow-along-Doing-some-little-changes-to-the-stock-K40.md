---
layout: post
title: "For those interested follow along. Doing some little changes to the stock K40"
date: September 24, 2016 14:02
category: "Modification"
author: "Ariel Yahni (UniKpty)"
---
For those interested follow along.  Doing some little changes to the stock K40. Using Openbuilds profiles and hardware to extend the overall capacity.



<b>Originally shared by Ariel Yahni (UniKpty)</b>



Took a slight different route than planned. Need to fix some details still. #UK40OB ﻿

![images/24cc77def2f0271f612d2438d6ad8610.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/24cc77def2f0271f612d2438d6ad8610.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *September 24, 2016 14:29*

Clever. Get a larger bed for most things and only give up the extra size when you go deep for Z axis adjustment.


---
**Gideon Sauceda** *September 24, 2016 16:36*

KEEP ON POSTING!


---
**Scott Marshall** *September 24, 2016 17:48*

Looking good. Got enough headroom for the carriage when it goes all the way forward? Looks tight. 



You sure are squeezing out every mm....



Scott


---
**Ariel Yahni (UniKpty)** *September 24, 2016 18:50*

**+Scott Marshall**​ will no go all the way and some stop will be needed


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 24, 2016 19:56*

I'm liking the look of this **+Ariel Yahni**. Will be good to see the end results.


---
**Bill Keeter** *September 24, 2016 20:29*

Any estimate on cutting area with this mod?


---
**Ariel Yahni (UniKpty)** *September 24, 2016 20:30*

For now see 30x60


---
**Gunnar Stefansson** *September 25, 2016 00:10*

Interesting indeed, do keep us posted. 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/KgT8mBrPm85) &mdash; content and formatting may not be reliable*
