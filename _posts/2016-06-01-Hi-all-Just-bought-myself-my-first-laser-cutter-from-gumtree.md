---
layout: post
title: "Hi all! Just bought myself my first laser cutter from gumtree"
date: June 01, 2016 16:38
category: "Discussion"
author: "Josh Smith"
---
Hi all! Just bought myself my first laser cutter from gumtree. So grateful to have found this community to turn to!



It came with the golden dongle - but no CD!



My background is 3d printing. I know to draw a 3D file, export it as an STL, import it to slicing software and then send it to my printer via an SD card. But...



I have ZERO idea about how I communicate with the laser cutter, what software I need and the order in which to do things. The drawing I'll just find out for myself on the way. 



With no CD, what is my next step?



Thank you all in advance!



Josh 

![images/c1d2612dc95178ccc6363fcf13679869.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1d2612dc95178ccc6363fcf13679869.jpeg)



**"Josh Smith"**

---
---
**Stephane Buisson** *June 01, 2016 16:45*

open the top right, find the controler board, identify it, a good chance you have a Moshi or a Nano board.

then report here, (it use 2 different software)

Alternatively, you can update to open source hardware and softwares.


---
**Ulf Stahmer** *June 01, 2016 16:53*

 Check **+Yuusuf Sallahuddin**'s  post.  It might be helpful as well!



[https://plus.google.com/+YuusufSallahuddinYSCreations/posts/PPD3GDs6a5o](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/PPD3GDs6a5o)


---
**Josh Smith** *June 01, 2016 16:54*

Hi Stephane



I had a look - the board with the USB socket on it has the following:



Model: 6C6979-LASER-M2:6



Is this of any help?



Josh


---
**Stephane Buisson** *June 01, 2016 17:00*

yep, you have a M2 Nano, so your original software was laserdraw, head to Yussuf link post by **+Ulf Stahmer** to DL.

read other post to activated it correctly.


---
**Josh Smith** *June 01, 2016 17:02*

Fantastic. Thank you both :)


---
**Alex Krause** *June 01, 2016 22:39*

Don't forget you have to have the USB dongle plugged into your computer for it to work


---
*Imported from [Google+](https://plus.google.com/103613958984945836482/posts/9JBeQR7zfne) &mdash; content and formatting may not be reliable*
