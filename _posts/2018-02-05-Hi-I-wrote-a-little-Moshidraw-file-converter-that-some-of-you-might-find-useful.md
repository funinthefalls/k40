---
layout: post
title: "Hi. I wrote a little Moshidraw file converter that some of you might find useful"
date: February 05, 2018 03:29
category: "Software"
author: "Vince Lee"
---
Hi. I wrote a little Moshidraw file converter that some of you might find useful.



Even though I have a Smoothieboard too (on a switchable adapter board), I still use MoshiDraw a lot.  Since they added DXF import over a year ago, there isn't a lot of downside since I can still do my primary drawing in Inkscape but still do quick projects or last minute adjusments in Moshidraw.  The only downside is that I haven't been able to migrate those projects or changes back into inkscape.. until now.



I looked into the .mdr file format that Moshidraw saves in, and was pleased to find that it is a largely self-documenting text file.  So I whipped up a quick command line utility in perl to convert moshidraw files to svg, and compiled it to a standalone windows 32 .exe command line utility.



Usage:

Moshi2SVG inputfile.mdr outputfile.svg



Limitations: 

- This is a work in progress. Right now it converts just the raw geometry, but skips most things like layers, groups, colors and fills.  I dunno how important the rest of those things are.



- Basic text comes over kinda close, but sizing and transformations work differently so they're not exact.  I didn't try circular text, but you might be able to convert text to a path in Moshidraw first before conversion to get a more faithful conversion. 



I pushed up both the source code in perl and the compiled .exe at [http://www.tealpoint.com/_vince/k40](http://www.tealpoint.com/_vince/k40)

![images/e521358fe0bc73c231bbcc26ee9459c7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e521358fe0bc73c231bbcc26ee9459c7.jpeg)



**"Vince Lee"**

---


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/TwWDcyfb6bV) &mdash; content and formatting may not be reliable*
