---
layout: post
title: "hey everyone im new to the k40 laser, only had mine for a couple of weekends"
date: April 23, 2016 15:48
category: "Hardware and Laser settings"
author: "Chris Parks"
---
hey everyone im new to the k40 laser, only had mine for a couple of weekends 



![images/d21f1650afc24ce0fe14e393cf164bc8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d21f1650afc24ce0fe14e393cf164bc8.jpeg)
![images/df3030773fcef59d5960bb5da69dc4a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/df3030773fcef59d5960bb5da69dc4a2.jpeg)
![images/5d19094dd5441cf0490954e06ae11c89.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5d19094dd5441cf0490954e06ae11c89.jpeg)

**"Chris Parks"**

---
---
**Dinesh C Holla** *April 23, 2016 16:56*

what is the thickness of the material which is ready to engrave .. 


---
**Chris Parks** *April 23, 2016 17:02*

**+Dinesh Holla** any thickness depending on the speed and amp I'm run at 300 mm and 30 amps


---
**Dinesh C Holla** *April 23, 2016 17:31*

mine is same machine.. if we keep 1 inch thick wood..laser beam goes out of focus... thin letters will not looks good.. need add a height adjustment mechamism


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 23, 2016 22:50*

Welcome to the group **+Chris Parks**.



I don't think it actually is 30 amps Chris. As far as I'm aware, these K40s that come with the digital "ammeter" are actually showing a %, not a mA value. Have a look around the group posts, but if I recall correct, the laser fires at 18mA max (or maybe 18mA is the most you should fire at).


---
**Dinesh C Holla** *April 24, 2016 02:12*

@ Yuusuf yeah, i am very sure its not amps.. its a digital meter showing percentage thats it..


---
**Richard** *April 27, 2016 15:39*

I wish my machine had a digital percentage-power display. It would make things so much simpler.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 16:34*

**+Richard Beline** I'd say you could add a digital ammeter inline with the analog ammeter, however you would need to verify with others here that are more electrically competent than I. I think that you need to make sure the digital ammeter can handle the voltage & is capable of reading the correct range (somewhere between 0-20mA). But you would still have to adjust the power manually with the potentiometer.


---
**Richard** *April 27, 2016 18:31*

I was hoping it would be possible to replace the pot instead of the am, so I have a digital readout of the <b>percentage</b> of power with buttons up/down, which would be constant (the am fluctuates as power is turned on/off). That would be much more precise than attempting to guess where I have the knob turned each time I try to run it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 19:09*

**+Richard Beline** I would be less concerned about the digital buttons to up/down the power, as over time the tube gets weaker anyway. If you take a look, someone just posted a voltage meter hack that they added to their K40 ([https://plus.google.com/113684285877323403487/posts/M8PX3HvY1YD](https://plus.google.com/113684285877323403487/posts/M8PX3HvY1YD)). As they turn the pot, it tells how much voltage is being given. I am not sure how you would go about a percentage related gauge. There may be some simple existing solution for this that I am unaware of.


---
*Imported from [Google+](https://plus.google.com/105449012371416349164/posts/UR6aC8x41PB) &mdash; content and formatting may not be reliable*
