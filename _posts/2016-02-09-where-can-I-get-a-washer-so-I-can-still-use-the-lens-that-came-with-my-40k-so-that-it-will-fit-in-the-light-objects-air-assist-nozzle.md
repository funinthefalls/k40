---
layout: post
title: "where can I get a washer so I can still use the lens that came with my 40k so that it will fit in the light objects air assist nozzle?"
date: February 09, 2016 03:29
category: "Modification"
author: "3D Laser"
---
where can I get a washer so I can still use the lens that came with my 40k so that it will fit in the light objects air assist nozzle? 





**"3D Laser"**

---
---
**HalfNormal** *February 09, 2016 03:51*

This works great.[http://www.thingiverse.com/thing:1145526](http://www.thingiverse.com/thing:1145526)


---
**3D Laser** *February 09, 2016 04:08*

**+HalfNormal** only thing is I dont have a 3d printer


---
**HalfNormal** *February 09, 2016 04:12*

Great reason to get one! Seriously, would be happy to make one and send it to you.


---
**3D Laser** *February 09, 2016 04:13*

**+HalfNormal** that will be my next investment that would be great if you have paypal I could throw you a few bucks for your trouble 


---
**Scott Marshall** *February 09, 2016 05:53*

You could cut out a couple thin washers from fiber gasket material or similar with a Laser cutter (if only we had one of those...) and sandwich the smaller lens. 

The Larger lens is available from Saite Cutter on ebay  for around 20 bucks, or you can get a set of Moly mirrrors and the 18mm lens for about $50.That's what I did (along with the aluminum air assist head from there) Love mine.


---
**I Laser** *February 09, 2016 06:39*

Yeah I just cut a washer from acrylic which worked fine until my 18mm lens came in.


---
**3D Laser** *February 09, 2016 12:18*

**+Scott Marshall** can you post a link


---
**Scott Marshall** *February 09, 2016 19:41*

They deliver faster than promised. About 7 days to upstate NY when I ordered mine.

The set saves you a couple of bucks over buying each individually. 



These are the more rugged mirrors, scratch resistant. The lens fit the Air Assist head they sell perfectly, and works great. I put my old stuff away for backup duty.



Tighten the collar VERY lightly, it's aluminum on glass, and only needs to hold it in place. It wouldn't take much force to crack the lens, I've been meaning to cut out a couple of washers out of Tyvek, thin gasket paper or similar material to cushion the lens, and provide a little gasketing to minimize air leakage - not that it really matters.



I like ILaser's acrylic idea, but it's a tad too thick for 2 layers and the lens. maybe stencil cutting material (acetate - transparency material).



[http://www.ebay.com/itm/262191317402?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/262191317402?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)

Good luck



Scott


---
**3D Laser** *February 09, 2016 19:43*

**+Scott Marshall** where at are you in upstate NY I'm in Buffalo


---
**Scott Marshall** *February 09, 2016 19:47*

Elbridge - 20 miles west of Syracuse(Weedsport Exit on the thruway)

Back when I was in business I used to spend a lot of time there. Several customers, Keller Technology and Bausch & Lomb were frequent runs.

Howdy neighbor!






---
**3D Laser** *February 17, 2016 00:03*

**+HalfNormal** anyway you would still make me a washer


---
**HalfNormal** *February 17, 2016 03:28*

I would be happy to. Email me larrygon at gmail your address. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/ftfyuo9NcDk) &mdash; content and formatting may not be reliable*
