---
layout: post
title: "Would this pump be good for air assist?"
date: June 30, 2015 04:49
category: "Discussion"
author: "quillford"
---
Would this pump be good for air assist? [https://www.amazon.com/dp/B004PB8SMM/ref=cm_sw_r_awd_C9HKvbS88KS0A](https://www.amazon.com/dp/B004PB8SMM/ref=cm_sw_r_awd_C9HKvbS88KS0A)

[https://www.amazon.com/dp/B004PB8SMM/ref=cm_sw_r_awd_C9HKvbS88KS0A](https://www.amazon.com/dp/B004PB8SMM/ref=cm_sw_r_awd_C9HKvbS88KS0A)





**"quillford"**

---
---
**charlie wallace** *July 02, 2015 14:53*

probably not great,  hard to tell but it looks like a small tankless diaphragm pump so even with a long cable run they tend to have pulsed air flow, which isn't terrible but not ideal. you can improve it with an airtank but better with a tanked compressor, something that can run extended periods of time. you do want a decent amount of airflow and pressure to blow away debris and assist the cut.


---
**Jose A. S** *July 09, 2015 20:10*

I am gonna try with an airbrush compressor...


---
*Imported from [Google+](https://plus.google.com/115654175984463400645/posts/WZG3WpqmcxX) &mdash; content and formatting may not be reliable*
