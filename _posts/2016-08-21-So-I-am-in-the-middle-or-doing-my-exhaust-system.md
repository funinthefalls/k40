---
layout: post
title: "So I am in the middle or doing my exhaust system"
date: August 21, 2016 23:19
category: "Discussion"
author: "3D Laser"
---
So I am in the middle or doing my exhaust system.  Having everything in negative pressure for me isn't an option as I have to reduce my fab size from 8 to 4 inches so today I sent about two ours using high grade hvac tape on every joint and seam then used insulation around the top most pipes. 



I could tell it wasn't air tight as I took off the old tape I could see the soot stuck to the tape where it had been escaping on the positive side.  I think now I have it pretty tightly sealed on the positive side and should make a big difference with fumes fingers crossed 

![images/19e4e5803407f7d01e9617b31d12d570.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19e4e5803407f7d01e9617b31d12d570.jpeg)



**"3D Laser"**

---
---
**HP Persson** *August 22, 2016 05:27*

Did you try with active coal filters, removing the smell?


---
**3D Laser** *August 23, 2016 16:47*

**+HP Persson**  it helped a little bit most of the leaks where at the top of the fan where I had no filter so it did little good


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/JdaUDqGCKBx) &mdash; content and formatting may not be reliable*
