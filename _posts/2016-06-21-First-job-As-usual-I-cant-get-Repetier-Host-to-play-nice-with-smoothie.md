---
layout: post
title: "First job. As usual I can't get Repetier Host to play nice with smoothie"
date: June 21, 2016 21:16
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
First job. As usual I can't get Repetier Host to play nice with smoothie. So, LaserWeb2 to make the gcode (from an SVG), save, and Pronterface to send to the machine. I don't like the UI much, but the communication and functionality is rock solid. 

![images/510a47e1dcaae03797b643a532dbe91c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/510a47e1dcaae03797b643a532dbe91c.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Arthur Wolf** *June 21, 2016 22:01*

You could use Smoothie's web interface instead of pronterface.

Also laserweb3 has support for talking to smoothie over network right ?


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 22:16*

**+Arthur Wolf** no ethernet on my mini smoothie in here.  Need to get a dedicated computer in there before I go dealing with LW3, but yeah, Peter says that works.


---
**Arthur Wolf** *June 21, 2016 22:16*

Oh right sorry


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 22:18*

All good. I haven't publicized any details yet, so how could you have known? :)


---
**Ariel Yahni (UniKpty)** *June 21, 2016 23:54*

**+Ray Kholodovsky**​ do you have and RPI? 


---
**Ariel Yahni (UniKpty)** *June 21, 2016 23:55*

LW3 rockzzzzz


---
**Ray Kholodovsky (Cohesion3D)** *June 22, 2016 00:14*

Plenty of Pi **+Ariel Yahni**, yes. But I would need a tv screen for it and wifi in that corner is very very bad. 


---
**Ariel Yahni (UniKpty)** *June 22, 2016 01:10*

You don't need the TV, just run the node on the PI and then connect from another pc.  No WiFi it's a no go then


---
**Ray Kholodovsky (Cohesion3D)** *June 24, 2016 19:16*

**+Ariel Yahni** I just ordered one of these to see if my VGA monitors will be an option: [http://s.aliexpress.com/RFFJfEzu](http://s.aliexpress.com/RFFJfEzu)



I also picked up a Pi 3 and Zero along with a lot of filament yesterday. 


---
**Ariel Yahni (UniKpty)** *June 24, 2016 19:52*

That should work


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/PibF74hw8yr) &mdash; content and formatting may not be reliable*
