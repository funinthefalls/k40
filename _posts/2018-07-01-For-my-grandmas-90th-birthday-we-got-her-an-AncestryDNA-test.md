---
layout: post
title: "For my grandma's 90th birthday we got her an AncestryDNA test"
date: July 01, 2018 19:10
category: "Object produced with laser"
author: "Mark Brown"
---
For my grandma's 90th birthday we got her an AncestryDNA test.  So these are for table centerpieces, "Ninety" in different languages where her DNA originated from.  1/8" solid cherry, 12mms, 11ma.  The font is "Birds of Paradise", only took minor alterations to make everything link.

![images/0017df5789b1f9e02bcb6927903103b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0017df5789b1f9e02bcb6927903103b2.jpeg)



**"Mark Brown"**

---
---
**ED Carty** *July 01, 2018 23:56*

Nice work. 


---
*Imported from [Google+](https://plus.google.com/116643344835605995638/posts/DneeKiDAnBV) &mdash; content and formatting may not be reliable*
