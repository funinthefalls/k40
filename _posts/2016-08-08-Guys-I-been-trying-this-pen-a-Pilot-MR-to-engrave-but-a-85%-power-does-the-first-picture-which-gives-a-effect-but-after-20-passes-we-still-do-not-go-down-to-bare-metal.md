---
layout: post
title: "Guys, I been trying this pen, a Pilot MR to engrave but a 85% power does the first picture which gives a effect but after 20 passes, we still do not go down to bare metal"
date: August 08, 2016 15:39
category: "Materials and settings"
author: "Sunny Koh"
---
Guys,



I been trying this pen, a Pilot MR to engrave but a 85% power does the first picture which gives a effect but after 20 passes, we still do not go down to bare metal. Is there something I can do or do I need a more powerful tube?



![images/877cc3072be11d5cfd0207fbe01db243.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/877cc3072be11d5cfd0207fbe01db243.jpeg)
![images/3ed2b6d1f73883fd9bfb9ee00cc23c1d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ed2b6d1f73883fd9bfb9ee00cc23c1d.jpeg)

**"Sunny Koh"**

---
---
**Ariel Yahni (UniKpty)** *August 08, 2016 16:46*

I would say you are not focusing in the correct height


---
**Eric Flynn** *August 08, 2016 16:59*

For sure.  It should easily tear through that paint at fairly low power.


---
**Sunny Koh** *August 08, 2016 17:04*

The thickness is quite thick actually. Other pens went fine.


---
**Matt Herrera** *August 09, 2016 03:39*

With a 40watt laser you should have no problems getting through that paint. I have a 2.8watt laser and I can burn off powder coatings from a stainless steel water bottle. Is it a metallic paint?


---
**Sunny Koh** *August 09, 2016 06:11*

Feels like a rubber type coating to be honest


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/dykyDFMWbez) &mdash; content and formatting may not be reliable*
