---
layout: post
title: "For all the electronic tinkers out there"
date: May 08, 2017 13:06
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
For all the electronic tinkers out there.



This is for all the tinkers out there that also enjoy electronics. I found this gem on ebay for $2.00 each with free delivery!



[http://www.ebay.com/itm/Arduino-Shields-pin-compatible-50-MHz-ARM-controller/142343588082?_trksid=p2045573.c100033.m2042&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D41376%26meid%3Dc1bd7e6a7c75419c8d019bb374d67dc5%26pid%3D100033%26rk%3D1%26rkt%3D3%26sd%3D142343588082](http://www.ebay.com/itm/Arduino-Shields-pin-compatible-50-MHz-ARM-controller/142343588082?_trksid=p2045573.c100033.m2042&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D41376%26meid%3Dc1bd7e6a7c75419c8d019bb374d67dc5%26pid%3D100033%26rk%3D1%26rkt%3D3%26sd%3D142343588082)



When it came, it is actually from the manufacture and all the support can be found here;

[http://www.coridiumcorp.com/prod-specs4.html](http://www.coridiumcorp.com/prod-specs4.html)

The site says that they are selling them for $19.95



Anyway, just received them and starting to play with it. Great if you like this stuff!





**"HalfNormal"**

---
---
**Mark Brown** *May 09, 2017 01:07*

I have an odd urge to get a couple, even though I have no idea how/what I'd do with them. :p


---
**HalfNormal** *May 09, 2017 01:23*

That is the problem with cheap stuff!


---
**Mark Brown** *May 09, 2017 01:47*

I mean, I have done a couple little tech/soldering projects.  So I would eventually find something to do with them.  But I don't need them, and my pile of junk/parts is about big enough already.  But they're only $2... XD


---
**Paul de Groot** *May 09, 2017 04:16*

If you can port grbl to them we would have an excellent new k40 controller😁


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Zvo4VyUXodX) &mdash; content and formatting may not be reliable*
