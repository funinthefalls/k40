---
layout: post
title: "Hello everyone, I am new to this forum and need some help"
date: October 11, 2017 17:25
category: "Discussion"
author: "Alex Raguini"
---
Hello everyone, I am new to this forum and need some help.



I have a blue/white K40 with the digital percent control panel.  I am soon replacing the controller with a Cohesion3D controller.  Does anyone have any suggestions for a replacement laser power supply?  The problem I'm currently experiencing is that the digital control does not behave well.  When I use the buttons to increase or decrease the power, the change does not always follow the buttons.  For example, I change the percent by 1 - the decimal number changes rather than the unit.  When I change the 10's number, either the unit or decimal number changes.  It is annoying.  Is this a problem with the digital controller, the power supply or both? 



I've read in some posts that it is wise to have a separate power supply for the motors and a dedicated laser supply.  Can someone point me to wiring diagrams provide recommended power supplies for use with the Cohesion3D board?



Thank you all in advance.





**"Alex Raguini"**

---
---
**Ned Hill** *October 11, 2017 18:33*

Other's can help with the power supply question, but I can tell you the problem with your digital current controller isn't uncommon and isn't related to PSUs as far as I know.  


---
**Don Kleinschnitz Jr.** *October 11, 2017 21:42*

I doubt that the the power supply is the problem with the digital power control and display. 



We don't have schems for the digital control board nor its firmware so it's hard to troubleshoot and repair. 



Someone on here may have a spare or you may find one on eBay.



Another choice you have is to replace it with an analog meter and pot, many have done such and there are instructions on here. 



Let us know what direction you want to take: repair or replace with the analog setup.... Or something else. 


---
**Anthony Bolgar** *October 11, 2017 22:36*

I would suggest replacing it with a POT and analog meter, better idea of what power you are actually using.


---
**John Wasser** *October 12, 2017 00:40*

If your digital display board is like mine it is connected to the K+/K- connector for measuring output current and to the Gnd/In/5V connector for setting the output current (via PWM).  The button to the right of the display seems to switch between the two purposes.



If the PWM signal is not playing nice with your Cohesion3D board you can replace the PWM signal with a 5k potentiometer.  Keep the Gnd and +5 connected to the digital board (so you can still use it to measure current) and also connect them to the ends of the 5k pot.  The wiper of the pot replaces the PWM signal and connects to 'In' in its place.  The 0V to 5V analog signal form the pot sets the output current.


---
**Anthony Bolgar** *October 12, 2017 03:12*

The digital display board DOES NOT measure current, it displays a number which is the percentage of the max power. ie. 20 means 20% power not 20mA  A lot of people are confused over this. 




---
**Printin Addiction** *October 12, 2017 11:03*

My buttons act weird as well when I try to single step (push once) somestimes the 1.0 button will do a .1 increment, etc... but if I hold it down it doesnt seem to do that, annoying but not a big deal....


---
**Alex Raguini** *October 12, 2017 16:26*

Thanks all for the answers.


---
**Alex Raguini** *October 19, 2017 22:53*

Ok,  I've decided to dump the digital percentage panel for a pot.  I've searched but can't seem to hit the right keywords.   I have a current meter installed already - but not completely sure I've wired it correctly.  Now I just need to find the part and wiring info to proceed.  links?




---
**Don Kleinschnitz Jr.** *October 20, 2017 02:49*

**+Alex Raguini** see if this is what you are looking for:

[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Current%20Calibration%20Pot)




---
**Alex Raguini** *October 20, 2017 23:31*

 It is what I am looking for. Thank you. 


---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/55fqekc4DpY) &mdash; content and formatting may not be reliable*
