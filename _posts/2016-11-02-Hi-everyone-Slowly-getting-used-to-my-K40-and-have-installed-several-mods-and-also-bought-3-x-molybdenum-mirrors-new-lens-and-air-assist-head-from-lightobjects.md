---
layout: post
title: "Hi everyone. Slowly getting used to my K40 and have installed several mods and also bought 3 x molybdenum mirrors, new lens and air assist head from lightobjects"
date: November 02, 2016 18:11
category: "Discussion"
author: "Jez M-L"
---
Hi everyone. Slowly getting used to my K40 and have installed several mods and also bought 3 x molybdenum mirrors, new lens and air assist head from lightobjects. One of the mirrors they delivered had speckling on the surface of the mirror that I couldn't get off with isopropyl alcohol and cotton buds so, after many emails they sent another out but this mirror also has the speckling. The other two mirrors were flawless and I have been using these with no issues so my questions are... Has anyone else had this problem? And will this effect the laser power? Photo attached of first mirror. This was after several cleans. 

![images/2d8da5e85aba130e1a6cd99bf962d30a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2d8da5e85aba130e1a6cd99bf962d30a.jpeg)



**"Jez M-L"**

---


---
*Imported from [Google+](https://plus.google.com/103448790120483541642/posts/Le3bmRmkQkN) &mdash; content and formatting may not be reliable*
