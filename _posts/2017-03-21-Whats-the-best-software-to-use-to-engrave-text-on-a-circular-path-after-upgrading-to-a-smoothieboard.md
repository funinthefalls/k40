---
layout: post
title: "What's the best software to use to engrave text on a circular path after upgrading to a smoothieboard?"
date: March 21, 2017 22:31
category: "Software"
author: "Wes Treihaft"
---
What's the best software to use to engrave text on a circular path after upgrading to a smoothieboard? 





**"Wes Treihaft"**

---
---
**Ariel Yahni (UniKpty)** *March 21, 2017 22:38*

Illustrator , Inkscape all have the option for text on path, then use LaserWeb4


---
**Wes Treihaft** *March 21, 2017 23:10*

What I am trying to wrap my brain around is how the text is filled. How should I save the output of say illustrator to import it into Laserweb. Having issues finding documentation on how this is done.


---
**Ariel Yahni (UniKpty)** *March 21, 2017 23:31*

If you create vectors then export as svg and then select Laser Fill Path in LaserWeb4. That will only engrave the inside, [you.csn](http://you.csn) even apply an angle on how the path will be burned. You also can apply an operation to the outside path


---
**Ariel Yahni (UniKpty)** *March 21, 2017 23:33*

I have a long video of LaserWeb 4 if you want to see (2hr) and then this one shorter from Don that explains just that part [https://plus.google.com/113684285877323403487/posts/giPrqn6SMhk](https://plus.google.com/113684285877323403487/posts/giPrqn6SMhk)


---
**Jim Hatch** *March 21, 2017 23:47*

**+Ariel Yahni** What's the URL of your long video? Love to watch it.


---
**Ariel Yahni (UniKpty)** *March 21, 2017 23:48*

Here you go **+Jim Hatch**​ [https://plus.google.com/+ArielYahni/posts/L645WwRdcsx](https://plus.google.com/+ArielYahni/posts/L645WwRdcsx)


---
*Imported from [Google+](https://plus.google.com/104033989120920258895/posts/Gj91xnpfFhZ) &mdash; content and formatting may not be reliable*
