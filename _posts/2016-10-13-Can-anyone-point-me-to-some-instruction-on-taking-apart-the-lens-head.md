---
layout: post
title: "Can anyone point me to some instruction on taking apart the lens head"
date: October 13, 2016 14:45
category: "Original software and hardware issues"
author: "Dennis Beukelman"
---
Can anyone point me to some instruction on taking apart the lens head. I can get the air assis nozzle off, BUT can't get the rest apart to inspect the lens,



Thnx

Dennis





**"Dennis Beukelman"**

---
---
**Don Kleinschnitz Jr.** *October 13, 2016 15:06*

Does this help?



[https://lh3.googleusercontent.com/6y9HlqD9bWfcAaStNIwLQKHXoajr7_jyoSJTXvYwRLgQ4jEZZjLhUrAdhkkvuISSczANRVsvfm8=w1920-h1080-rw-no](https://lh3.googleusercontent.com/6y9HlqD9bWfcAaStNIwLQKHXoajr7_jyoSJTXvYwRLgQ4jEZZjLhUrAdhkkvuISSczANRVsvfm8=w1920-h1080-rw-no)

[lh3.googleusercontent.com - lh3.googleusercontent.com/6y9HlqD9bWfcAaStNIwLQKHXoajr7_jyoSJTXvYwRLgQ4jEZZjLhUrAdhkkvuISSczANRVsvfm8=w1920-h1080-rw-no](https://lh3.googleusercontent.com/6y9HlqD9bWfcAaStNIwLQKHXoajr7_jyoSJTXvYwRLgQ4jEZZjLhUrAdhkkvuISSczANRVsvfm8=w1920-h1080-rw-no)


---
**Don Kleinschnitz Jr.** *October 13, 2016 15:16*

Or this. Do search of this community with "Laser Head"

[https://lh3.googleusercontent.com/dQcvD0hfxo-o0j6MYukQlODVZbr-uWYjVWsR2ZjPVsGe7fMqiVxd7vLLGPSSHX_6ePnx7k_YCg=w1920-h1080-rw-no](https://lh3.googleusercontent.com/dQcvD0hfxo-o0j6MYukQlODVZbr-uWYjVWsR2ZjPVsGe7fMqiVxd7vLLGPSSHX_6ePnx7k_YCg=w1920-h1080-rw-no)

[lh3.googleusercontent.com - lh3.googleusercontent.com/dQcvD0hfxo-o0j6MYukQlODVZbr-uWYjVWsR2ZjPVsGe7fMqiVxd7vLLGPSSHX_6ePnx7k_YCg=w1920-h1080-rw-no](https://lh3.googleusercontent.com/dQcvD0hfxo-o0j6MYukQlODVZbr-uWYjVWsR2ZjPVsGe7fMqiVxd7vLLGPSSHX_6ePnx7k_YCg=w1920-h1080-rw-no)


---
*Imported from [Google+](https://plus.google.com/104196870231434096924/posts/2DVgWY7X9kh) &mdash; content and formatting may not be reliable*
