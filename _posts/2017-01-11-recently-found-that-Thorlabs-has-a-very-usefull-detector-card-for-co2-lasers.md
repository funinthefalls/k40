---
layout: post
title: "recently found that Thorlabs has a very usefull detector card for co2 lasers ..."
date: January 11, 2017 10:44
category: "Discussion"
author: "Ben Koch"
---
recently found that **+Thorlabs** has a very usefull detector card for co2 lasers ...



[https://www.thorlabs.de/newgrouppage9.cfm?objectgroup_id=296&pn=VRC6#7126](https://www.thorlabs.de/newgrouppage9.cfm?objectgroup_id=296&pn=VRC6#7126)





**"Ben Koch"**

---
---
**Todd Miller** *January 11, 2017 21:28*

That would be cool, but the spec's say:



Damage Threshold, Single 7 ns Pulse 1064 nm 	 35 MW/cm2 



I wonder how long it would last ?




---
**Scott Thorne** *January 12, 2017 16:57*

I'm not an expert...but co2 lasers are 10,640 nm...not 1064...that's just infrared...like on most galvo lasers.


---
**Ben Koch** *January 12, 2017 17:07*

They do have some for 10600nm which are for CO2, the last one on the page, they work perfectly if the beam is not focused ....


---
**Scott Thorne** *January 12, 2017 17:41*

Lol....i didnt see them


---
**Todd Miller** *January 12, 2017 23:46*

My bad, I missed a Zero. I may have to give one a try.


---
*Imported from [Google+](https://plus.google.com/+BenKoch/posts/HvKoR4wAGrm) &mdash; content and formatting may not be reliable*
