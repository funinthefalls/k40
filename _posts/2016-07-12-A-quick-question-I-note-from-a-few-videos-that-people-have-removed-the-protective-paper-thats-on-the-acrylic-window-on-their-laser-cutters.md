---
layout: post
title: "A quick question. I note from a few videos that people have removed the protective paper that's on the acrylic window on their laser cutters"
date: July 12, 2016 09:52
category: "Discussion"
author: "Matthew Norman"
---
A quick question.  I note from a few videos that people have removed the protective paper that's on the acrylic window on their laser cutters.  Is that safe to do?  Or once you've done that is it an absolute necessity to buy laser goggles?  Is the acrylic enough for reducing the harmful rays?





**"Matthew Norman"**

---
---
**Matthew Norman** *July 12, 2016 09:53*

I will get goggles... But just currently being very very cautious with this thing..


---
**Don Kleinschnitz Jr.** *July 12, 2016 11:17*

The film does nothing. Also adding a LED strip inside the cabinet helps being able to see that is going  on and it also reminds me the unit is on. Make sure you have installed interlocks in the lid and yes get goggles. **+Peter van der Walt**  do you know of anyone testing the acrylic material. Mine is tinted, is everyones? 


---
**Lance Robaldo** *July 12, 2016 12:52*

Plexiglass/acrylic is opaque to the beam emitted by the C02 laser.  It is all the shielding you need.



Were that not the case you wouldn't be able to etch plexiglass, the beam would simply pass through and burn whatever was underneath.


---
**Ned Hill** *July 12, 2016 16:16*

Let me start by saying that I'm an analytical chemist by profession so I'm very pro PPE (Personal Protection Equipment).  However, I'm sceptical of any significant scattered  laser radiation making it through the acrylic window since it's opaque to those wavelengths.

From the plexiglas website, "Colorless Plexiglas sheet is entirely opaque to infrared wavelengths from 2800 nanometers up to 25,000 nanometers in thicknesses of 0.118" or greater".

I do agree with Peter that secondary radiation can be a issue that is probably only partially offset by the tinting. However radiation intensity from a point source falls off as the square of the distance.  So in general I think the risk is probably minimal as long as you are not sitting with your face right up against the window.  (If you are watching through the window with your face close you definitely should be wearing laser goggles) 

A greater concern is if you are trying to view the laser tube while it is firing.  If you ever need to do that you definitely need some good laser goggles otherwise you could potentially cause some serious eye damage.


---
**Ned Hill** *July 12, 2016 16:43*

To be on the safe side, you can always add an extra layer of acrylic to the window to increase the thickness, and thereby increase the absorption of laser radiation, for a greater margin of protection.


---
**Brook Drumm** *July 12, 2016 22:48*

Don't need it. I've looked at lasercutter while cutting through an acrylic window more than anyone I know and my eyes are perfect. 



As a practice, glance to see its working and walk away- but stay close. Check often. Goggles won't hurt of course, just uncomfortable.


---
**Don Kleinschnitz Jr.** *July 13, 2016 13:27*

**+Ned Hill** "A greater concern is if you are trying to view the laser tube while it is firing." ....do you actually mean looking at the tube in the back compartment? Why would this be a problem?


---
**Ned Hill** *July 13, 2016 14:26*

**+Don Kleinschnitz**  Mainly because there is no shielding between you and the laser when it's firing.  Sometimes you may need to view the tube while it's firing for troubleshooting reasons.  You see people posting pics of this every now and then.   If everything is aligned and working correctly then probably ok,  but not worth the chance.  Not long after I got my machine one of the springs on the first mirror in the back failed so the mirror was way off alignment to the point that the beam never left the back.  If I had not noticed this and test fired the laser with the back open it could have been disastrous.


---
**Don Kleinschnitz Jr.** *July 13, 2016 16:22*

**+Ned Hill** Ok, your point isnt that looking at the "tube" itself is dangerous its that opening that area could result in a reflection. Note: that a refection in this area is full power. Makes a case for an interlock there as well. Thanks..


---
**Pippins McGee** *July 15, 2016 01:18*

which goggles to buy? can you link please? there is many types many wavelengths many materials, tint no tint, reflective not reflective.

ned has made me concerned now. to align mirror i am often looking at tube while it is firing


---
**Ned Hill** *July 15, 2016 02:29*

This is what I have [https://www.amazon.com/Lightobject-LSR-EP4-Protection-Glasses-nanometer/dp/B00VA7JQ0C/](https://www.amazon.com/Lightobject-LSR-EP4-Protection-Glasses-nanometer/dp/B00VA7JQ0C/)


---
*Imported from [Google+](https://plus.google.com/102654691727306435605/posts/JXpqSy8BJ6p) &mdash; content and formatting may not be reliable*
