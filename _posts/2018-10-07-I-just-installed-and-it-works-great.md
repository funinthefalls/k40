---
layout: post
title: "I just installed and it works great"
date: October 07, 2018 22:58
category: "Software"
author: "Martin Dillon"
---
I just installed #k40Whisperer and it works great.  Good by dongle although I like Coreldraw because I have used it for years.  I have one question.  It gives a warning message about replacing drivers and that you won't be able to use the stock program again.  I am assuming it means I can't use that computer.  If I have another computer with the stock software installed I can still use that one?  Or does it actually change drivers on the machine?





**"Martin Dillon"**

---
---
**Ned Hill** *October 09, 2018 14:31*

It actually changes the drivers.  You can undo it though if you want to go back to using the stock program.


---
**Scorch Works** *October 09, 2018 16:21*

The laser machine is unchanged by the driver install procedure.  You definitly can have one computer set up to run with the stock software and another to run K40 Whisperer.  You would just need to plug the laser USB cable into whichever computer you wanted to use.  


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/3L2h5fPyU23) &mdash; content and formatting may not be reliable*
