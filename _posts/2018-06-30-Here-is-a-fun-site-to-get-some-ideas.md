---
layout: post
title: "Here is a fun site to get some ideas"
date: June 30, 2018 18:17
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Here is a fun site to get some ideas.



52 projects has turned into 130 and counting!



About us from the site;

"52 Lasers is a collaboration between Pixelaser and Isette, showcasing one new project per month created with an industrial laser engraving & cutting platform. The blog was born out of a need for an artistic challenge and a huge list of things we want to try, but never seem to get around to doing. Expect to see a variety of different uses for laser engraving and cutting  here—trying new materials, making functional objects, creating unusual artistic pieces, even documenting the experiments we’ve been meaning to do and feel would be helpful to the laser community at large. To that end, for each project, we’ll select one thing from an ever-growing list of ideas and turn it into a finished project. Then we’ll take some pictures and write some things and share them with you here! While the name 52 Lasers came to be when we were posting one project a week, our current main project schedule is monthly."



[http://52lasers.com/](http://52lasers.com/)





**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Wv3aJmjAwMc) &mdash; content and formatting may not be reliable*
