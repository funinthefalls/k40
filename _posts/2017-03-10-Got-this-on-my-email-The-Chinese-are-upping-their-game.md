---
layout: post
title: "Got this on my email. The Chinese are upping their game!!!"
date: March 10, 2017 12:34
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Got this on my email. The Chinese are upping their game!!! specially for humans

![images/4e83fdfd167f1d89663aced79377db98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e83fdfd167f1d89663aced79377db98.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Maxime Favre** *March 10, 2017 12:51*

I like the shutterstock stamps on the background :D


---
**Claudio Prezzi** *March 10, 2017 18:15*

Do you have a link to this seller?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2017 18:55*

It's got "thousands of fronts" too!


---
**Richard Vowles** *March 10, 2017 19:39*

And LaserDrw!


---
**Ariel Yahni (UniKpty)** *March 10, 2017 19:51*

**+Claudio Prezzi**​ I will look for it and send to your email


---
**Steven Whitecoff** *March 10, 2017 21:52*

Ok I'll bite **+Don Kleinschnitz**, why are all digital panels shitty and useless? Not hereto argue, just wonder why you never miss a chance to bash it.


---
**Anthony Bolgar** *March 11, 2017 03:15*

Did **+Don Kleinschnitz** post something? I do not see him in the responses.




---
**Mark Brown** *March 11, 2017 04:10*

**+Anthony Bolgar**  Yes, Don was one of the first comments on this thread.


---
**Anthony Bolgar** *March 11, 2017 04:10*

Wierd that I can not see him


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 11, 2017 04:30*

**+Anthony Bolgar** It was previously there (I remember reading it) but it is now not visible to me either. Maybe Don deleted his response.


---
**Anthony Bolgar** *March 11, 2017 04:34*

I think that I have been blocked by a couple of users, hope Don isn't one of them, he always has great detailed info on the lasers we all use.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 11, 2017 04:43*

**+Anthony Bolgar** I would think that's not the case. You're right, he is always very informative & helpful.


---
**Anthony Bolgar** *March 11, 2017 05:01*

I find that we have a core group of regulars that are all knowledgeable and helpful to new users. One thing I love is see people get help even if it was a newbie stupid question. Not many people here get all uptight and start telling people to #RTFM (for those not familiar with this acronym it means Read The **cking Manual) I always cringe when I see vetran users tell a new person to do that, it is so much nicer to help the person by answering the question and politely telling the new user where to find the information.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 11, 2017 05:31*

**+Anthony Bolgar** I wasn't familiar with that term when I first started seeing it pop up around the place here. Google enlightened me as to its meaning. I can understand the frustrations of some when it comes to processes that are clearly described in the specific manuals (e.g. for Smoothie, GRBL, LW, etc) & especially when it seems to be the same questions arising again & again. However sometimes, try as we might, it's  just very hard to find the relevant info & where to start (hence a lot of newbie "stupid" questions). I personally agree with your approach to just help people if you are capable of doing so, or at least point them in the right direction.


---
**Don Kleinschnitz Jr.** *March 11, 2017 12:18*

All, 

....man I guess I went from one extreme to the other. 

I did not block anyone. I blocked my own poor judgement.



Yes, I deleted my "Shitty and useless" comment because I broke my cardinal rule of:



"Never criticize someone else's work, instead always provide technical facts over opinions". 



"Shitty and useless" was an  inappropriate description" that in itself is "Shitty and useless". 

 

I did not think about its potential to  insult someone who owned one.  

.......



A technical perspective. 

In effect the panel:

.....  replaces the pot with a digital one. Some control and granularity is lost but that method is more reliable. Especially with the lower quality pot that comes with these machines. The digital value suggest that it is a power setting but it really is a relative voltage to the power supply like the digital readout that I recommended myself on our analog pots. So it IS as useful as the pot.



..... needs to have a current meter and by removing that from the design it  makes the panel less functional than the previous one. It is difficult to manage/troubleshoot a K40 without knowing what the lasers current is because the laser is a consumable. Meaning that the power at a particular current, changes over time.  



Perhaps I an a bit annoyed that with this vintage of machines (no meter) it will be even harder to help those with K40 problems. 



This thread just proves my theory that editorial and judgmental comments just waste everyone's time ........


---
**Don Kleinschnitz Jr.** *March 11, 2017 12:41*

**+Yuusuf Sallahuddin** 



Another long post :(



I don't think that RTFM is a very helpful or respectful response. We have all been a newbie at some point and sometimes you do not know where to start learning and what questions to ask.



As a person that spends many hrs a day on this community responding to K40 problems I am more frustrated with the inefficiency of the process, not the "newbieness" of the players.



My view is that the easier we make the technology to use the less "newbie" problems we will have.



My frustrations with questions fall into two categories: 



1)...... is not when a "newbie" asks a question that has been answered before", its when an answer is provided and then the instructions are not followed and the information that is linked is clearly not read. Feels like some people just want it fixed and are not willing to invest in any learning.



2) ....... the question you ask in a post is clearly not read and not completely answered so you have to ask again.

<s>---------------</s>

Finding anything on this community is difficult at best and G+ is not an intuitive tool. That is why I have invested so much time on my blog.



I have thought a lot about how do we bring a newbie up to speed quicker and with less redundant effort.



My conclusion is:

1). Get all  the information in one place not spread over G+ or any other community. G+ organization can be created by building collections that can be shared by others. Create a "get started guide for newcomers" and that is the first thing they need invest time in".

2). Create test programs/patterns that can be run on K40's to help troubleshoot. They run the test and send us the results.

I have been waiting for LW4 so that we can create test patterns and make visual and other measurements.

3). Create simple DIY tools. Laser power, optical alignment gauges, PWM measurements belt tension, endstop tester etc. 

4). Create troubleshooting guides that uses the above resources. 



Sorry for the long post ....






---
**Anthony Bolgar** *March 11, 2017 13:33*

Actually, Thank You for the long post **+Don Kleinschnitz**. It brings up many valid points as well as backing up what I have been trying to get across to the community at large. I am currently working on a Unbiased review site for CNC hardware/software, laser hardware/software and 3d Printer hardware and software. I am using a google cloud hosted instance of wordpress for this, that way it can be easily scaled as demand for it increases. I would be more than willing to dedicate some space for people like **+Don Kleinschnitz**, **+Ariel Yahni**, **+Ray Kholodovsky**, and anyone else I feel can do a good job explaining the technical aspects of the machines and software. in the beginning I will cover all the hosting costs, as it develops I would hope that a little bit of either fundraising or paid content would be enough to keep the site going. What it will NOT be is a support forum for any of the machines, it is strictly for unbiased reviews and general guidelines and facts about the different making processes. No one machine or piece of software will be treated as a <b>**preferred**</b> product, and discusion of all products is welcome, no one will be banned because they mention one software release as an alternative to another one. All products and all software have their place in the maker ecosystem, and you may prefer one way of doing something while another user has a different preferred method. All designs, ideas, and discussion will be welcome. The company I have made to manage all this is called "Revision 13 Prototypes" If you have any interest in helping with this project, please send me a google hangout request, or drop me an email at anthonybolgar@live.com


---
**Anthony Bolgar** *March 11, 2017 13:38*

And **+Don Kleinschnitz**, you don't need to wait for LW4, Visicut supports smoothie (for over a year now) and **+Stephane Buisson** has been using it without any issues. I have just started to learn how to use it, and I can tell you it is a hell of a lot more intuitive than LW ever has been. Workflow is very simple, no need to save a design in multiple files so you can do different raster and vector operations. Just design using colors, import the one file with the colored line design, choose what each color does from a drop down and hit execute or save G code. That;s all there is to it.


---
**Ariel Yahni (UniKpty)** *March 11, 2017 13:52*

**+Anthony Bolgar** intuition is in the eye of the beholder. 16 months i gave visicut a look, i did not find easy of use, intuitive workflow, etc. I cannot make any sense out of it. LW4 does have color recognition and does have a ordered workflow, like any other tool you need to read and use it.

I can also honestly say at this point that my jugment is clouded but back then when i needed it, it was not even close


---
**Anthony Bolgar** *March 11, 2017 14:00*

You are definitely right that it is in the eye of the beholder. I am used to using color as a way to determine how an item gets processed. The software I used on my first DIY CNC milling machine I made with my father 20 years ago or so, used Corel vectors with different colors to process into Gcode. I guess I just like that style of software. But for CNC Cam, take a look at estlcam, most people can learn to use it in 1/2 hour to 1 1/2 hrs. It is some slick software. Would be nice if LW incorporated some of its features.


---
**Don Kleinschnitz Jr.** *March 11, 2017 14:24*

**+Ariel Yahni**, **+Anthony Bolgar** I have not tried Viscut yet and not sure that estlcam supports smoothie, or are you saying just use it to create Gcode.



I have been trying to find a good (free gcode editor also).



We should make a matrix of capabilities that these software have that shows a comparison of what functions are supported. 


---
**Ariel Yahni (UniKpty)** *March 11, 2017 14:35*

**+Anthony Bolgar**​ im sold on fusion 360. Been using it for more that 4 years I believe. If I learn something new there no sale in my brain so I need to first erase some data :)


---
**Anthony Bolgar** *March 11, 2017 14:37*

Thats my problem too...every new idea deletes 2 old ones in my filing cabinet of the mind. :)


---
**Steven Whitecoff** *March 11, 2017 15:58*

I think we each view software from the perspective of what we've used and know. With CAD, I find some programs have easy to use tools but poor selection tools, some have good selection but poor tools, some have both but are limited in file format, saving options, etc. My current favorite DraftSight appeared to have very limited saving, and then I found the "classic interface", VOILA!!! "Save As".

My take on that is sometimes you just have to stick with something to get past the perceived issues. That is probably why some say awesome program and other say garbage.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/DoF76BhCyNe) &mdash; content and formatting may not be reliable*
