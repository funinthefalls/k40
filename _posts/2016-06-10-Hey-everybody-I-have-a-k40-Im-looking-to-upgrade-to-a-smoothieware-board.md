---
layout: post
title: "Hey everybody, I have a k40 I'm looking to upgrade to a smoothieware board"
date: June 10, 2016 16:41
category: "Discussion"
author: "kyle clement"
---
Hey everybody, I have a k40 I'm looking to upgrade to a smoothieware board.  I am in a toss up between a smoothie board 4xc and an azteeg x5 mini v3.  Could yall give me any input on differences between them before I buy? I am looking at running the laser in pwm mode





**"kyle clement"**

---
---
**Anthony Bolgar** *June 10, 2016 17:00*

Personally I would choose the 4XC as it from the original developer. The azteeg is a clone. I would rather support **+Arthur Wolf** and the boys with the fantastic work they have done with smoothieware and smoothieboard.


---
**Derek Schuetz** *June 10, 2016 21:09*

Azteeg is an amazing smoothie board that is just simple and has an easy to follow layout


---
*Imported from [Google+](https://plus.google.com/106211725666880587072/posts/KHRnKmH6PL1) &mdash; content and formatting may not be reliable*
