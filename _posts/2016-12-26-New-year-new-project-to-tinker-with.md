---
layout: post
title: "New year, new project to tinker with!"
date: December 26, 2016 19:51
category: "Discussion"
author: "Robin Sieders"
---
New year, new project to tinker with!

![images/2c7b0f1dc2f19b5df4990783e27806d9.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2c7b0f1dc2f19b5df4990783e27806d9.png)



**"Robin Sieders"**

---
---
**3D Laser** *December 26, 2016 20:35*

You can get them cheaper on eBay


---
**Robin Sieders** *December 26, 2016 21:15*

Had $150 in Amazon credit, so brought the price down under what I could find on eBay at the moment. Plus the added protection of purchasing through amazon if any issues arise with the machine.


---
**Ned Hill** *December 27, 2016 15:41*

Congrats.  It's a wonderful addiction...I mean hobby ;D


---
*Imported from [Google+](https://plus.google.com/+RobinSieders/posts/EoRtbNhueLZ) &mdash; content and formatting may not be reliable*
