---
layout: post
title: "Hi I have misplaced my USB stick, Doh am I screwed?"
date: January 09, 2017 20:02
category: "Software"
author: "Brian Wylie"
---
Hi I have misplaced my USB stick, Doh am I screwed? or is there anything I can do?





**"Brian Wylie"**

---
---
**greg greene** *January 09, 2017 20:28*

I have upgraded to a Cohesion 3D and no longer need my Nano 2 controller and USB Dongle - you can have them for 50 bucks - but you should really upgrade if you can.   It really opens up a lot more software for you.


---
**Brian Wylie** *January 09, 2017 20:35*

Thanks for the prompt reply Greg, I am in the Uk so if that is a workable solution for me I can arrange paypal payment or whatever Regards Brian


---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2017 21:13*

This is the package Greg got. There's a reason people make the upgrade. 

We ship to the UK regularly. 

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**greg greene** *January 09, 2017 23:04*

We can do that Brian

I really recommend Ray's board though, I't something you will do sooner or later anyway

Let me know which way you want to go



Greg


---
**Andy Shilling** *January 10, 2017 16:54*

**+Brian Wylie**​ what control do you have at the moment? I'm waiting on one of Ray's boards because my control has broken leaving me with the usb. Mine is for the moshi boards if it's any help and I'm in the uk.


---
**Brian Wylie** *January 10, 2017 19:21*

Hi Andy I have just dropped the whole caboodle down to my son to have a look at, I think I found the USB stick but if not I will gladly take you up on the offer. Thanks I am in Yorkshire


---
**Andy Shilling** *January 10, 2017 19:23*

That's fine, but obviously it will only work if you have a moshi board. What software were you using?


---
**Brian Wylie** *January 12, 2017 18:41*

**+Andy Shilling**  Hi Andy when I booted the computer up it had Moshi on the desk top. it was bought pre used and I saw it all in working order so am letting my son who is an electronic engineer/programmer have a play and then he can show me Lol


---
**Andy Shilling** *January 12, 2017 18:55*

Hi Brian, that's fine then my new board should be here soon so if you need this usb just let me know.  I presume you found yours in the end. The latest software can be found at [moshidraw.com - 磨石软件](http://moshidraw.com), you might get a warning of a virus when you download it but there is nothing there, I think it's just because it's a .exe file.  Let me know if you need any help getting it working.


---
*Imported from [Google+](https://plus.google.com/117601610269144664172/posts/84xix1oPRSc) &mdash; content and formatting may not be reliable*
