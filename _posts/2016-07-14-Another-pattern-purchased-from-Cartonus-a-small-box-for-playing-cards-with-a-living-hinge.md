---
layout: post
title: "Another pattern purchased from Cartonus, a small box for playing cards with a living hinge"
date: July 14, 2016 14:29
category: "Object produced with laser"
author: "Tev Kaber"
---
Another pattern purchased from Cartonus, a small box for playing cards with a living hinge.



It was a bit snug, I had to sand a couple parts a little, but went together well. I changed the line thickness to .0011 for cuts but that may have thrown off the fit slightly. 



I also didn't engrave the inner notch quite deeply enough, so had to Dremel it a little. 



![images/e998c412aebb77ef8f08978e1e2096e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e998c412aebb77ef8f08978e1e2096e5.jpeg)
![images/d34deef2854eac21071f32b1d7a3aa69.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d34deef2854eac21071f32b1d7a3aa69.jpeg)

**"Tev Kaber"**

---
---
**Alex Krause** *July 14, 2016 14:34*

Are you having to adjust for laser kerf?


---
**Tony Sobczak** *July 14, 2016 19:13*

You paid for it?? 

It's freebie from Epilog.


---
**Tev Kaber** *July 14, 2016 19:25*

**+Tony Sobczak** Really? I thought Cartonus made their own templates... Do you have the link to the Epilog one?


---
**Tony Sobczak** *July 14, 2016 19:34*

check this link and they are all free.



[https://www.epiloglaser.com/resources/sample-club.htm](https://www.epiloglaser.com/resources/sample-club.htm)


---
**Tev Kaber** *July 14, 2016 19:44*

**+Tony Sobczak** Oh yeah, I've seen those, there is a hinge pattern there but just a hinge. 



The Cartonus pattern takes it a bit further, making it into a box and adding a latch mechanism. 


---
**Tev Kaber** *July 14, 2016 19:48*

I could probably design something similar, but if someone spent some time refining a design, I don't might paying a reasonable price ($7 in this case) for the pattern. 


---
**Pippins McGee** *July 15, 2016 05:17*

**+Tony Sobczak** 

good one tev, I had purchased and made this one also recently. see below.

[http://i.imgur.com/09O056M.jpg?1](http://i.imgur.com/09O056M.jpg?1)

As tev said the epilog template is just a hinge / cover.

the cartonus design expands much further on it, making it an enclosed box with a locking mechanism too.



id prefer these designs to be free but everyone wants to charge for designs haha. sharing is caring people!


---
**Bart Libert** *July 23, 2016 15:28*

Can you tell me more about "cartonus" ? Is that a webshop or something ?


---
**Tev Kaber** *July 23, 2016 21:38*

Yeah, Cartonus has an etsy shop with a handful of patterns. 



[https://www.etsy.com/shop/cartonus](https://www.etsy.com/shop/cartonus)


---
**Tony Schelts** *October 18, 2016 22:42*

**+Pippins McGee** I love the box,  where did you get the design its beautiful. What wood did you use and did you stain it?  Really good work. 


---
**Tev Kaber** *October 19, 2016 14:05*

Thanks! It's just 3mm baltic birch plywood, not stained.



Here's where I got it, if you want some.  Handy precut size for the laser.

[http://www.ebay.com/itm/1-8-3mm-x-8-x-10-Baltic-Birch-Plywood-for-CNC-Laser-Scoll-Saw-40-pieces/351780641491](http://www.ebay.com/itm/1-8-3mm-x-8-x-10-Baltic-Birch-Plywood-for-CNC-Laser-Scoll-Saw-40-pieces/351780641491)


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/avZyMxWYwUq) &mdash; content and formatting may not be reliable*
