---
layout: post
title: "So just a few days ago I saw a bunch of K40 machines on eBay for around $385 buy it now..."
date: December 11, 2018 21:48
category: "Discussion"
author: "Sean Cherven"
---
So just a few days ago I saw a bunch of K40 machines on eBay for around $385 buy it now...  Now today I go back to eBay to purchase one, and the cheapest I can find is $599... 



Am I going crazy, or did the price really jump up that high within just a few days?





**"Sean Cherven"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 11, 2018 22:19*

Someone said that they sold out.  A factory also told me yesterday that they were swamped with orders. 


---
**Sean Cherven** *December 11, 2018 22:21*

I see.. Any idea how long I will have to wait for? Lol


---
**Ray Kholodovsky (Cohesion3D)** *December 11, 2018 22:45*

Well I know historically the prices are lowest around June. 


---
**Ned Hill** *December 11, 2018 23:05*

I just looked on ebay and I see a  bunch of new listings at $385.  They all say stock available on Dec. 13th.

[ebay.com - Details about 40W CO2 Laser Engraving Machine 12"x 8" Engraver Cutter w/ USB Port](https://www.ebay.com/itm/40W-CO2-Laser-Engraving-Machine-12-x-8-Engraver-Cutter-w-USB-Port/192754597639?hash=item2ce111bf07:g:lQYAAOSw-2hcDyRA:rk:5:pf:0)


---
**Sean Cherven** *December 11, 2018 23:15*

**+Ned Hill** Those are for Auction, I was looking for something that is Buy it Now.


---
**Sean Cherven** *December 11, 2018 23:18*

Does anybody know of a place (that's not eBay) that sells the K40 units?


---
**Ned Hill** *December 11, 2018 23:20*

Ahh gotcha, your right, I don’t see any less than $585 for buy it now. 


---
**Ned Hill** *December 11, 2018 23:22*

Yeah the supply has historically tightened in Dec and the prices don’t drop back down until at least April. You have the Chinese New Year in feb so lots of Chinese workers on vacation. 


---
**Stephane Buisson** *December 12, 2018 09:18*



Actually available in EU !

My understanding is Ebay sellers work by batch. (important to buyer is location of the good as it could be more taxes). when batch run out, only bad offer remain. Now as best offer, I notice it's near chinese new year, so not so long to wait ;-))


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/69k4tuD21qh) &mdash; content and formatting may not be reliable*
