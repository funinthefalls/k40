---
layout: post
title: "Hi All, So i just got a K40 Laser form a Co-worker, Manged to set it up and get it working for about 2-3 hours"
date: November 05, 2015 06:55
category: "Hardware and Laser settings"
author: "Apolise"
---
Hi All,



So i just got a K40 Laser form a Co-worker, Manged to set it up and get it working for about 2-3 hours.



Now the Laser is not firing, and the Current indicator is not listing any Current. I am very new to this and not sure where/what to troubleshoot first honestly. Would anyone be willing to help.



I believe the PSU went up and it was just bad timing. but i'm not 100% sure how to test the PSU or the laser tube.



Regards,



Tony





**"Apolise"**

---
---
**Coherent** *November 05, 2015 15:13*

Evidently not with all machines because my laser "test fire" will not fire the laser if the laser enable button is not on/down. The power supplies are fairly stable so I'd look at other issues than the power supply being bad. If you do want to check it and have a volt meter it's easy to check, but which terminals depend on your specific power supply. Hunt on the web for a matching power supply diagram and check the 24v and 5v positive outputs and grounds. You didn't say so please don't take offense, but I'm assuming you hooked up the water pump and had water flowing through the tube before trying to fire/test it?


---
**Apolise** *November 05, 2015 15:44*

Test fire doesn't fire the laser, the Laser enabled button is on. I started to look for Power supply Diagrams, but didn't fine the one that matches my PSU yet. 



and no offense taken, I'm very new to the K40 laser and laser cutting in general. if it was a mistake i made, i'll learn and fix the thing for next time.



But I had the water pumping first thing, and also checked after the fact to make sure it was still flowing after the machine broke. Motors and Board too and act correctly and it will follow instructions coming from the PC, so all that is powered and working, the only issue i'm seeing is the laser not firing. I have no idea how to test the tube. PSU i can eventually test voltages when i find that diagram. I plan to take it out today to look for any Model/informaiton. but it looks like this one but with no fan.



[http://www.ebay.com/itm/231550508351?_trksid=p2060353.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/231550508351?_trksid=p2060353.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



i may just buy a replacement PSU, and even if it's not whats wrong i'd have a spare, if it's the controller(not sure how to test this yet), i would just upgrade at this time. then lastly would get the replacement tube i guess.



Thank you both so far


---
**Anthony Bolgar** *November 05, 2015 19:25*

That looks like my power supply. I have been searching for a pin out diagram for this power supply, but have been unable to find one. Does anyone have the pin out diagram for this power supply?


---
**Coherent** *November 05, 2015 22:47*

It's the same as mine also. I found a diagram of it that was used for an upgrade to a ramps controller. I checked all the pin outs and it matched mine anyway. I used it for wiring a few projects. I can't add the photo/diagram to this post, so I'll just post it in a new topic.


---
**Maldenarious** *March 20, 2016 04:26*

Did you get this resolved Tony? Was it the psu or the tube?


---
**Apolise** *March 20, 2016 04:27*

In my case it was the PSU.﻿ when I swapped out the PSU it immediately worked.


---
**Maldenarious** *March 20, 2016 04:44*

thanks


---
*Imported from [Google+](https://plus.google.com/103553586729776983111/posts/aDQStRrLbXx) &mdash; content and formatting may not be reliable*
