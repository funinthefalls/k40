---
layout: post
title: "Question: My k40 (C3D) just stopped working in the middle of a job"
date: April 06, 2017 09:53
category: "Original software and hardware issues"
author: "Andreas Benestad"
---
Question:

My k40 (C3D) just stopped working in the middle of a job. The head kept moving, but no laser action. The water pump is running and the temperature is good. The exhaust is working as it should.

I can control the movement of the laser head as normal, but the laser is not firing.

What would you reccommend for troubleshooting..? How can I figure out what has happened..?? Everything was working and then not. 



Kind of desperate for input, thanks for any help!





**"Andreas Benestad"**

---
---
**Andy Shilling** *April 06, 2017 10:05*

Sounds like a dead laser tube to me, can you fire it from the test button?


---
**Andreas Benestad** *April 06, 2017 10:12*

No response when I push the test button...

Is that normal that a tube just suddenly dies like that..? I was expecting it to slowly fade away. 


---
**Joe Alexander** *April 06, 2017 10:18*

no interlocks on the protect loop that are open? im sure you checked but you never know...otherwise i'd think dead tube also. Does the tube illuminate at all? does it mark a label or piece of paper at tube exit/first mirror?


---
**Andy Shilling** *April 06, 2017 10:21*

It could be your psu also but I'd put money on the tube as that's the more expensive option and that's how things work unfortunately


---
**Andreas Benestad** *April 06, 2017 10:25*

I have not checked interlocks on the protect look. What is that..? :-)

It does not leave a mark on anything at the first mirror. I cannot see it illuminating either.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2017 11:09*

Interlocks will be any safety switches you have in place (e.g. to prevent firing if the main lid is open, the tube lid is open, the water is not running, etc). If you have them installed, one of them may be faulty & triggered saying that a lid is open when it's actually not.


---
**Joe Alexander** *April 06, 2017 11:11*

if your machine is using the default control panel then the interlock loop is your "laser on" button. and no illumination will happen if this loop is not closed,  otherwise I fear a dead tube...


---
**Andreas Benestad** *April 06, 2017 11:25*

Yeah, as far as I can see the laser on button should be working as

normal...


---
**Andreas Benestad** *April 06, 2017 12:28*

So...someone on Facebook suggested I test the L wire. So I did. And I am confused. Can someone explain to me if this is right:

It has a constant current of 4V (when not being used). When I press the test fire button, the current drops to 0. 

Does that make sense to you?




---
**Joe Alexander** *April 06, 2017 12:56*

yea it does, the Laser PSU fires when L is grounded, driving it to 0v.


---
**Don Kleinschnitz Jr.** *April 06, 2017 13:51*

Picture of supply please?


---
**Andreas Benestad** *April 11, 2017 15:49*

**+Don Kleinschnitz**​ sorry for the delay. Here is a pic of the PSU.

![images/ccc172cff1cb809c7558c63ad7cd10b0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ccc172cff1cb809c7558c63ad7cd10b0.jpeg)


---
**Don Kleinschnitz Jr.** *April 12, 2017 00:31*

**+Andreas Benestad** Have you tried:





With the laser switch on



1. Grounding the L pin on the right connector

2. Pushed the test button down on the supply?

Without the laser switch on

3. Is the green led on the LPS on?

4. Measure 24V and 5V on the rightmost connector? 


---
*Imported from [Google+](https://plus.google.com/112581340387138156202/posts/GqYPxg8nDrF) &mdash; content and formatting may not be reliable*
