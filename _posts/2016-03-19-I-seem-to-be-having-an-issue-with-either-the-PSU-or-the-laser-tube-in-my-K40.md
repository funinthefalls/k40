---
layout: post
title: "I seem to be having an issue with either the PSU or the laser tube in my K40"
date: March 19, 2016 13:26
category: "Discussion"
author: "Anthony Bolgar"
---
I seem to be having an issue with either the PSU or the laser tube in my K40. When I first got it, 90% power was about 12-14mA. Now, only after about 50 hours use, I am only getting a current draw of 8-10mA. I was always running the tube at about 6-8 mA, unless cutting thicker MDF, where I ran it at about 10-12mA. Never overheated the tube, all connections are tight. Anyone have any ideas as to what is going on. I should mention that I am running an Arduino/Ramps control system.





**"Anthony Bolgar"**

---
---
**Scott Thorne** *March 19, 2016 14:05*

Where are you located **+Anthony Bolgar**?


---
**Anthony Bolgar** *March 19, 2016 14:09*

I am in Niagara Falls, Ontario, Canada. But I have a Niagara Falls, NY address that I can receive from as small as an envelope up to the size of a transport truck trailer. (I use it for my business so I can do my own customs brokering when I order parts or stock from the States.)


---
**Stephane Buisson** *March 19, 2016 14:28*

tube gaz leak ?


---
**Scott Thorne** *March 19, 2016 14:49*

Ok....when I get off today I will give you a shout. 


---
**Anthony Bolgar** *March 19, 2016 15:35*

Thanks Scott.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/RdiJJWjNYS1) &mdash; content and formatting may not be reliable*
