---
layout: post
title: "Sometimes I think I am the luckiest man alive"
date: March 22, 2016 16:52
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
Sometimes I think I am the luckiest man alive. WHile in the middle of a production run of some keyfobs, my air assist compressor snapped the piston connecting arm, rendering it useless. SO I shut everything down, and decided to go run some errands. While I was out I ran across an airbrush compressor, complete with a siphon style airbrush included. It was a special buy for the store (non stock item), and on sale for the fantastic price of $75 USD ($99.95 CDN). It was the last one left, so I snatched it up, and took it home to test. This compressor runs whisper quiet (my old one sounded like a car without an exhaust system) Great pressure and CFM rate, it out preformed my old one by orders of magnitude. Cut quality has improved, less lens cleaning now. Managed to finish the keyfob job on time, and with better quality. Can't believe the luck I had!



![images/4d41e5cf47c7d992e9a2b1ef5b16a235.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4d41e5cf47c7d992e9a2b1ef5b16a235.png)



**"Anthony Bolgar"**

---
---
**Brett Cooper** *March 22, 2016 17:09*

What was your previous air system?  Links? 


---
**Ashley M. Kirchner [Norym]** *March 22, 2016 17:33*

That's what I have on mine as well. The only issue I have with it is that in long runs, I'm talking a few hours of continuous work, it will get too hot and go in thermal shutdown. So for the time being, I have an additional fan on the back that's blowing in cool air to help the internal one. Eventually I will get a larger compressor that would build pressure faster and therefore also turn off every once in a while even while the laser is running.


---
**Ashley M. Kirchner [Norym]** *March 22, 2016 19:14*

As an added bonus to having that compressor ... I've been using mine as an actual airbrush as well on some of the pieces that I make. After engraving, I will airbrush the engraving before taking the tape off. Works great.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 22, 2016 20:26*

Aside from having the previous one break & cause you extra expense, it does indeed seem lucky to have come across this buy whilst out on your errands. As Ashley mentions, using the actual Airbrush also to finish some pieces could prove quite useful also.


---
**Tony Sobczak** *March 22, 2016 20:54*

Available in US @ Harbor Freoght for $89.99 less easily attainable 20% coupon. 


---
**Anthony Bolgar** *March 23, 2016 00:03*

My previous compressor was a 12V automotive tire inflate from the 1960's that I got a flea market 20 yrs ago.


---
**Anthony Bolgar** *March 23, 2016 00:06*

The airbrush was jut a bonus to me, I gave it to my daughter to use while crafting (she already has a small 1hp compressor to run it off of.) I already had 2 very high end Binks gravity fed airbrushes, as well as 2 touch up spray guns, and 2 full size automotive spray guns.


---
**Bob Steinbeiser** *March 25, 2016 17:38*

On the Harbor Freight 20% off coupons, check the fine print, they don't apply to compressors (and some other tools)!  (don't ask me how I know!) 


---
**Ashley M. Kirchner [Norym]** *March 25, 2016 17:50*

I bought mine during a 20% sale ... no coupon, just straight up sale.


---
**Tony Sobczak** *March 26, 2016 01:48*

**+Bob Steinbeiser**​ I just bought one from HF and they accepted the coupon. Maybe not on the big ones but on these yes they do.


---
**Pippins McGee** *June 22, 2016 02:39*

**+Anthony Bolgar** I bought same model. Look exactly the same, 23 LPM right?

Put finger under laser head (LO) can barely feel any air pressure coming out. Same for you?

Air, sure - there is air coming out, probably enough to stop smoke from entering the laser head and onto the lens. but not with any great volume or pressure to add oxygen to the cut or blow the smoke far away.



I find this pump a letdown tbh.. a small PC fan would blow more air onto the cut than this thing. Seems like such a lot of money and hassle setting up, just to puff the smallest amount of air to keep the lens clean when a fan could do the same job for 25 cents?

again, unless there's something wrong with mine.

Pumps at 0PSI btw (just doesnt produce enough volume for any pressure to build up in 5mm hose)



what are your thoughts now after a couple of months?


---
**Bob Steinbeiser** *June 22, 2016 12:27*

No, not a lot of pressure from the pump but it makes a huge difference for me, definitely cleaner cuts and no more flame ups. Plus any positive pressure should keep the smoke away from the lens.



The regulator shouldn't be an issue here since it should never shut off.  



I did have a problem with nozzle alignment.  It was difficult to get the beam centered, but that doesn't sound like your issue.



Good luck!


---
**Ashley M. Kirchner [Norym]** *June 22, 2016 16:49*

I have a similar one that I bought at Harbor Freight, 58 PSI 1/5 HP. It doesn't build up pressure while I'm using the laser because it's just dumping air out constantly, but I do get a steady stream. I 3D printed my own nozzle with a 1.5mm aperture which gets me a rather strong blast of air out of it. Strong enough that it blows all of the sticky goo out of wood out of the cut and down onto the bed, where previously that stuff just caught fire. Now I need to pull out the bed every so often and wash that nasty stuff off. But that's way better than having burned edges, flare ups, or possibly worse.



The thing I don't like about it is that it tends to overheat during long jobs, and if I don't watch it, the thermal protection will kick in and shut the air pump off. So I have a permanent computer fan stuck to the back of it, blowing air in. The internal fan is working, it's just not moving a lot of air. These things were meant for air brushing, short bursts of air, not continuous pumping.



But I'm also about to upgrade to a much larger one, a California Air 5.5 Gal 1HP. Those things are so darned quiet for what they are, it's amazing. And at that capacity, it won't be running constantly.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/HFnZjXM16EH) &mdash; content and formatting may not be reliable*
