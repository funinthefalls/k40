---
layout: post
title: "I have been looking for some simple tube clamps"
date: October 04, 2016 14:50
category: "Modification"
author: "Don Kleinschnitz Jr."
---
I have been looking for some simple tube clamps. 

I  found these over on:

[http://www.buildlog.net/cnc_laser/drawings.html](http://www.buildlog.net/cnc_laser/drawings.html)



[http://www.buildlog.net/cnc_laser/drawings/tube_clamp_rev_4.pdf](http://www.buildlog.net/cnc_laser/drawings/tube_clamp_rev_4.pdf)



Looks like you can buy them for $10 or cut your own.





**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 04, 2016 15:10*

Looks like there is plenty of detail in there :) Thanks for the share.


---
**Anthony Bolgar** *October 04, 2016 18:23*

The links to the kits are 404'd These are very old links (2010) Here is a Link for a 3D printed tube hangar. [http://www.thingiverse.com/thing:1581069](http://www.thingiverse.com/thing:1581069)  I have 2 of these printed out right now if you don't have a 3D printer. ($25 + shipping)


---
**Don Kleinschnitz Jr.** *October 04, 2016 19:49*

**+Anthony Bolgar** these links work fine for me???

Question on your brackets. Do they fit in the existing mounting holes and where do I have to ship them from?


---
**Anthony Bolgar** *October 04, 2016 20:05*

The brackets fit the existing holes, at least in mine they did. I am in Niagara Falls, Canada, but can ship from Niagara Falls NY if desired.


---
**Don Kleinschnitz Jr.** *October 04, 2016 20:22*

**+Anthony Bolgar** So in yours there were no mods to the case?



Any idea of the shipping cost to 84092 usa. I tried the online calc and couldn't find you.


---
**Anthony Bolgar** *October 04, 2016 22:41*

It is about $10 to ship it to you.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/FJYkLQVDRHq) &mdash; content and formatting may not be reliable*
