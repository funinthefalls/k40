---
layout: post
title: "Hi all!! Just thought I would introduce myself"
date: August 03, 2016 17:37
category: "Discussion"
author: "Eric Flynn"
---
Hi all!! Just thought I would introduce myself.  I am a long time laser enthusiast, and own many gas and solid state lasers of various types.  However, I just acquired a K40 , and will be putting in a MKS Smini, so I thought I would join!! 





**"Eric Flynn"**

---
---
**Scott Marshall** *August 03, 2016 17:51*

Welcome.



Watch out for loose parts and don't forget to turn on the water pump before firing. A quick workaround for the lack of pump interlock it to plug it into the same switchable outlet strip as the K40, that way, you have to turn on the pump to turn on the laser.

Another thing to check is the grounding of the IEC cord receptacle, The main ground wire is attached to it with a quick slide terminal and they often fall off in shipping, leaving themachine ungrounded with no warning. Solder it on for a permanent fix.



You are about member 2001, we just broke 2k this am.



Scott



PS, Being a Laserhead, you already know the ropes, but here is a "reprint" of some info particular to the K40. Weed thru it and use what you can. 



I finally saved a copy of this, I've typed the "New K40 'wiki' " about 100 times (or so it seems) It's not my best work, but here it is, glean what you can. Spacing and links are damaged from the copy process, sorry.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



Plug the water pump and the laser into a switched outlet strip. This will help prevent accidently firing the laser with no water flow. It will destroy the tube in seconds if run without flow, and once you realize what you just did, most people rush and plug in the pump, so the cold water hits the ripping hot glass with predictable results. There's a few things you should do right off, before starting using it. !st get it going to make sure it all works (so you can go back on the seller if needed with a untouched machine. If it works ok, shut it off and start taking it apart. Remove the silver "worktable" or clamp or whatever they call it. It's junk and actually prevents you from focusing the laser properly. While you're in there, mark the position of the brown rails on the bottom with a sharpie and carefully remove the entire frame. it's only 2 screrws and a couple of plugs. Then remove the "duct" from the rear panel by removing the 4 screws at the corners. Put the duct with the worktable in the recycle bin. replace the fan mounting strips with the 4 screws you took out. Re-install the motion control frame, getting it on the sharpie marks as closely as possible. This 'duct' performs no useful and restricts the already inadequate fan so that it's unusable. If you remove the duct, the stock fan works OK if you don't restrict the flow with ductwork that is too small or long. Add the factory blue plastic duct to the pile in the recycle, it's too small and a fire hazard. Use only non-flammable duct on the exhaust. 4" expandable aluminum duct is good for short runs, 6" for longer ones. Vent outdoors, laser fumes can be very nasty. Then (refer to guide linked below) align the mirrors. (they were out of alignment when you started, so it's no loss there. The 1st couple of time you align the machine, it will be confusing and slow. After a couple of times you'll be able to do it in under 10 minutes. Some 1" lo-tack masking tape (the 3m Blue stuff) and aluminum foil are all you need. I put the foil under the tape so the vapors from the tape don't pollute the mirrors. That will get you on the road. Pretty soon you'll want an air assist head and a better set of mirrors Saite cutter on ebay is a good place to deal with. To replace the factory workholder, there's a lot of choices. If you want to spend, you can buy a fancy honeycomb plate, but plain expanded metal or Walmart disposable grill toppers work well for a buck and a half. Another choice is a nail board, just a piece of 1/4 plywood with 17ga brads in a 1" grid. Adjust the height on these surfaces with a wooden shim assortment. The focus is critical to the performance of the laser, as the mirror alignment. This is accomplished by raising anfd lowering the workpiece, but the Site cutter air assist head has the advantage of having adjustable focus, so once you get the workpiece height close, you can dial it in from there. Other benefits of the air assist head are that it keeps fires damped out, keeps smoke from depositing on the workpiece, and protects the lens. You will need an air pump or compressor with regulator, but it's worth the trouble. Read thru the group's history, This is probably the 30th time I've typed this, this time I'm saving it for the next person. Wish we had Stickies here. There's a ton of info here, and the people are super helpful. Be careful around your new laser, there are no safety interlocks, so there is exposed line voltage, laser light that can burn or blind you, and 15,000 volts all unguarded. Do check the main ground wire it attaches to the top of the IEC line cord jack with a quick slide terminal, and they frequently fall off in shipping, ungrounding the whole laser. Solder it in place if you can, or just make it as tight as possible. [http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf) [http://www.ophiropt.com/](http://www.ophiropt.com/) [http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005](http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005) [http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005](http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005) The new head requires shimming and the hole opened a bit. You can buy a plate with a larger hole, ask here for details on the install when you get there, and it needs larger lens. The best way to do it is buy the bare head (no mirror or lens) and buy the mirror/lens kit listed here. Hope it helps, be safe and have fun!



Scott


---
**Eric Flynn** *August 03, 2016 18:03*

Thanks. Ha, yea. Like I said, I own many lasers, several sealed and flowing CO2. I know how to handle it. Thanks for the reminder though.  Already running for a few days. It actually wasn't too bad. A few loose screws. Alignment was surprisingly good.   It is a slightly different model. Its not the blue box one, but rather a white one with red doors. Identical in essentially every way though.  I've already modified it some.  I will be machining a new head for it in the next few days.  I will share my drawings and such for custom parts.  


---
**Scott Marshall** *August 03, 2016 18:42*

Hey, I finally copied that blurb and couldn't pass up a chance to use it.



Machined a head from scratch for mine too. It came in destroyed, the water pump and fan were loose packed in the cutting bay and pounded everything in there to junk. The seller gave me back $250 on a $311 bill, so I came out ok. I enjoy building anyway.  I could have bought one, but it was an excuse to fire up my little CNC mill. Beside the stock one looked as though it had been gnawed out of aluminum by rats.



I had planned on building the laser from scratch, but for 300bucks I decided to get a feel for it before committing big bucks to a tube, and I never imagined just how well the K40 could be made to work. Someday I'll get around to my big laser....



Glad to meet another such hobbyist.



I got so wrapped up in the K40 group, I started up a little company selling upgrade kits, mostly electronics plug in kits for folks that want to change boards but are put off by the wiring etc.

Just started shipping.

Now it's using up all my time and money, and my poor mill hasn't made chips in months. I think it's going to get a job making the mounting brackets for the kits. I'm getting tired of drilling holes by hand.



I look forward to seeing your work.



Scott


---
**HalfNormal** *August 03, 2016 18:58*

**+Scott Marshall** will try and get that posted to wiki. **+Yuusuf Sallahuddin** are you listening? 

We need to get working on it!


---
**Scott Marshall** *August 03, 2016 19:04*

Don't bother him! He's working on a website for a very important client!



We'll get the wiki updated. I need to sit down and Write a proper new  K40 guide, That ones kind of rambling, and far from complete. A bit longwinded too, but that's my nature...



Scott


---
**HalfNormal** *August 03, 2016 19:06*

**+Scott Marshall**​ I wonder who the VIP is? :-)


---
**Eric Flynn** *August 03, 2016 19:22*

**+Scott Marshall**  Great! Id like to see your work too. Links?   I feel the same about the K40  For $300 shipped, I can tinker a bit and have a very capable machine for my needs.  If i need more power, I have a 120W tube and supply I can hang out the end of it.  I have built cutters before, but they always took up too much space and end up selling them.  I have a fully equipped machine shop with a 14x40 lathe, a big BP2J2, a smaller CNC mill , a smaller cnc lathe, and many 3D printers, and other machinery.   I am an electronics engineer in the automotive under hood electronics industry.  I was formerly a manufacturing engineer, with a heavy background in machining and cnc programming, as well as electronics engineering in the EV industry.  Nice to see someone  else with some of the same capabilities.


---
**Scott Marshall** *August 03, 2016 20:38*

We've got very similar backgrounds. I think we'll get along fine. You ever drag race? Shoot competitively? If so, it wouldn't surprise me.



We gotta talk sometime, I play with Megsquirt system when I have time, seems like you probably know a bit on that subject...



My website will be up in a few days ALL-TEKSYSTEMS.com



I'll be posting a "Product update" here this evening Announcing that we're starting to ship orders for my ACR Kits. I can send you a line card/pricelist of the currently available kits if you send me your email. ALLTEK594@aol.com



 These kits contain a board that emulates the M2nano controller found in many of the Chinese lasers, breaks out and conditions the logic levels to drive any aftermarket controller. It also has 2 onboard regulators, 5 & 3.3v to power Arduino sytle RAMPS boards. The Smoothieboard is hot for K40s right now, and my most popular kit comes with an ACR (Aftermarket Controller Retrofit) board, power supply and The Smoothieboard, all set-up and tested for the K40. It's a "Plug & Play" install.



Glad to say they're finally shipping, it's been a lot of time and work to get them ready for prime time. There's a switchable line coming behind the ACR lione, and they keep your M2nano and let you flip back and forth between the aftermarket controls and the Stock M2nano.



Then there's a line of other kits... It's going to keep me busy for awhile.



Scott


---
**Eric Flynn** *August 04, 2016 00:10*

Nice.  I was planning on some sort of switching to switch between boards.  Looks like its already done for me :)



I have indeed played with the MegaSquirt.  Havent done any racing, but I have been involved somewhat in the IRL machining some parts for some race teams as a good friend was a chief engineer on a few teams. Im in Indy BTW.



Send me some info on your kits.  eflynn7778@gmail.com


---
**Scott Marshall** *August 04, 2016 01:12*

I expected we might have followed similar paths. You always seem to meet similar people in tech fields.



I'll send you my current price list and a couple of pictures.



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 04, 2016 05:08*

**+HalfNormal** Yep, I'm listening :) Been a bit busy with personal stuff & working on this website for the VIP :)



Have barely even turned my laser on in the last few weeks lol.



But yeah, we'll get the wiki fleshed out. Strangely, when I said "whenever someone asks me a question I'll write a wiki page & link it", since then I actually haven't come across any "detailed" questions yet.


---
**Scott Marshall** *August 04, 2016 07:54*

**+Yuusuf Sallahuddin** 

It always seems like it. Once you have the answer, nobody asks the question. 



That's why I hit Eric with my miniwiki, just had to use it.


---
*Imported from [Google+](https://plus.google.com/107613521854079979112/posts/JRTRaKY7BEF) &mdash; content and formatting may not be reliable*
