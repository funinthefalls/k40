---
layout: post
title: "So I was never able to get my K40 laser to work so i just gave up on it"
date: February 29, 2016 20:54
category: "Materials and settings"
author: "Randy Powell"
---
So I was never able to get my K40 laser to work so i just gave up on it. Im not a technical person and never cared to tried to figure it out.

However the reason i bought it was to try something out so i might as well just ask one of you guys.

Does anyone know if the laser can engrave PET(Polyethylene terephthalate) that's only 10mm thick. Is it too thin?  Will the laser just melt the material?





**"Randy Powell"**

---
---
**Jim Hatch** *February 29, 2016 21:09*

Should be able to do that. I'd use high speed and lowest power. Or try it with the "marking" setting - that's usually used to mark or discolor material. PET has a fairly low melt point but should be doable. I used to have some water bottles made of PET but don't think I've got any around. 10mm is a nice thickness. My worry would be whether there's any toxic component in its makeup.


---
**Anthony Bolgar** *February 29, 2016 21:11*

What did you end up doing with the laser? Is it for sale?


---
**Randy Powell** *February 29, 2016 22:18*

**+Anthony Bolgar** ya I'm going to be selling it. Dont really have a need for it anymore


---
**Randy Powell** *February 29, 2016 22:24*

**+Jim Hatch** Thanks well Im looking to give someone a project then seeing that my machine doesn't work. So anyone is this group who willing try this for me I'd appreciate that. Il pay for it to get done.


---
**Phillip Conroy** *March 01, 2016 03:23*

Why dont you see if somebody in your area can align a nd set up your laser cutter for you,where do you live?

Once the mirrors are aligned this is a very cheap good little machine- i use mine at least 20 hours a week cutting scrapbooking embulems of wife,




---
**Randy Powell** *March 01, 2016 04:23*

**+Phillip Conroy** I live in toronto. There isn't anyone around to set this thing up especially because I bought it online. 


---
**Anthony Bolgar** *March 01, 2016 04:26*

Randy, I am just 45miutes away in Niagara Falls, and am in the GTA quite often. I would be willing to buy it off you if the price is right, or I could give you a hand setting it up andf aligning it.


---
**Randy Powell** *March 01, 2016 04:31*

**+Anthony Bolgar** if you could help me set it up that would be great. If not we could negotiate a price 


---
**Anthony Bolgar** *March 01, 2016 04:52*

I can be reached at my business number:

 +++ Niagara Clock  905-371-9993 +++



Give me a call and we can work out a time that is convenient for both of us.


---
**Jeremie Francois** *March 01, 2016 07:19*

Community rocks :) It makes my day!


---
**Phillip Conroy** *March 01, 2016 08:53*

It may take a few hours to align everything and loots of tesf firing on min power and loots of masking tape,my first time 8 hours


---
**Phillip Conroy** *March 01, 2016 08:55*

I am happy you are getting help


---
*Imported from [Google+](https://plus.google.com/113462808763272747282/posts/AtTEXZTDNsK) &mdash; content and formatting may not be reliable*
