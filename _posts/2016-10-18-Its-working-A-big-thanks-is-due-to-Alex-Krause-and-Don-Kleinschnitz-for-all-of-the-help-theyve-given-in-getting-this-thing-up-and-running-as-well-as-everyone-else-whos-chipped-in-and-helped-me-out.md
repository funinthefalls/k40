---
layout: post
title: "It's working! A big thanks is due to Alex Krause and Don Kleinschnitz for all of the help they've given in getting this thing up and running, as well as everyone else who's chipped in and helped me out"
date: October 18, 2016 22:00
category: "Discussion"
author: "K"
---
It's working! A big thanks is due to **+Alex Krause** and **+Don Kleinschnitz** for all of the help they've given in getting this thing up and running, as well as everyone else who's chipped in and helped me out. I'm still working on getting used to LaserWeb3, but at this point any issues are all user error. My next step is to build my control box (which is what it's engraving right now), then I want to either have an adjustable motorized head or a motorized Z table. 



My setup: Upgraded bed to 260 Y, 575X using 12mm linear profile rails. Azteeg X5 Mini controller, LO PSU. I'm currently using a Mac to run LaserWeb3, but I just need to work through some issues with my RPi, then I want to go headless. 

![images/eb99131beb7799316eaeccdb12bda8f9.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/eb99131beb7799316eaeccdb12bda8f9.gif)



**"K"**

---
---
**Don Kleinschnitz Jr.** *October 18, 2016 22:09*

**+Kim Stroman**​ thanks to you for your dead LPS .... it arrived today and will be very useful in gaining better understanding it's interface... 


---
**K** *October 18, 2016 22:10*

**+Don Kleinschnitz** Glad to hear it! I'll get those terminal blocks out to you asap. Sorry I forgot them.


---
**Don Kleinschnitz Jr.** *October 19, 2016 14:39*

**+Kim Stroman** can you describe what happened just prior to your LPS failing and what the failed symptoms were. I see a failed component but in a part of the circuit I am surprised stopped it from working.


---
**K** *October 19, 2016 14:59*

**+Don Kleinschnitz** I was running a job and all the sudden the tube stopped firing. The test button on the control panel didn't work, so I hit the test button on the LPS. When I did the laser fired, and then I smelled burning electronics. I looked at the LPS and saw a flame coming out of the supply right behind the test fire button. I unplugged it and didn't use it again. I'm not competent enough with electronics to be comfortable trying to power up something that had caught on fire. 


---
**Don Kleinschnitz Jr.** *October 19, 2016 15:32*

**+Kim Stroman** the diode that interfaces with the test button fried .... no idea why yet!


---
**Bill Keeter** *October 20, 2016 00:07*

Anymore details you can share about this setup. This looks close to what I've been brainstorming for the X and Y rails. I just don't think I'll do 575mm lol. 


---
**K** *October 20, 2016 07:23*

**+Bill Keeter** Here's a video that shows the machine. I used 12mm HiWin knockoffs from Amazon for the X and Y. They're mounted to OpenBeam 1515 extrusion which sits atop some aluminum plates I cut for the machine. I added a second motor for the Y axis. I've also upgraded the controller to an Azteeg X5 smoothie compatible board. 



[drive.google.com - File_000.mov - Google Drive](https://drive.google.com/open?id=0B1kvMudpU9EMeThpZ05zNWdPZGc)


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/Ug8KtzETWna) &mdash; content and formatting may not be reliable*
