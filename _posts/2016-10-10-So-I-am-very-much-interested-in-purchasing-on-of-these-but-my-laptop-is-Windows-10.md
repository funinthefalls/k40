---
layout: post
title: "So I am very much interested in purchasing on of these but my laptop is Windows 10"
date: October 10, 2016 17:14
category: "Software"
author: "Lewis Hofeld"
---
So I am very much interested in purchasing on of these but my laptop is Windows 10. Can this be configured to be compatible with the laser? 





**"Lewis Hofeld"**

---
---
**greg greene** *October 10, 2016 17:24*

works with mine


---
**Jim Hatch** *October 10, 2016 17:27*

Yes. Many of us use Win 10 with our K40s.


---
**Lewis Hofeld** *October 10, 2016 17:43*

Thank you. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 10, 2016 17:51*

Yeah, Windows 10 is no problem. I am running Win10 x64 Pro on a Macbook via Bootcamp.


---
**greg greene** *October 10, 2016 20:01*

Lase on Fearlessly !!!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 10, 2016 20:49*

**+greg greene** A little bit of fear is handy ;) Don't want to lose an eye haha.


---
**greg greene** *October 10, 2016 20:56*

True !!!!


---
**Lewis Hofeld** *October 10, 2016 21:18*

Outside going through all of the wiring, attaching hardware, grounded circuit and mirror alignment, what else should I know before purchasing? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 10, 2016 21:35*

**+Lewis Hofeld** On the purchase side of things, make sure to report any faults/breaks/etc ASAP to the seller & escalate to a claim with PayPal before the time period runs out. Some sellers try stuff you around too long so that you cannot claim.



On the machine side of things, seems you pretty much have everything you want to know covered. Only things I can suggest is check what controller board the K40 ships with (more than likely m2nano, but if it's the moshi board you may want to look elsewhere) & also (when you get the machine) to check the lens orientation (convex up), as mine shipped upside down.


---
*Imported from [Google+](https://plus.google.com/100446815936907986884/posts/Lg28De7D6So) &mdash; content and formatting may not be reliable*
