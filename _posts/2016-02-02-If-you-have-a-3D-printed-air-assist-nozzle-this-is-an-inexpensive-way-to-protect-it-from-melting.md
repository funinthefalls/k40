---
layout: post
title: "If you have a 3D printed air assist nozzle, this is an inexpensive way to protect it from melting"
date: February 02, 2016 22:06
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
If you have a 3D printed air assist nozzle, this is an inexpensive way to protect it from melting. Just cover the plastic with this mettalic faux silver leafing. Put it on in about 3 - 5 layers for best protection.



[http://www.banggood.com/100-Leaves-Fake-Silver-Leaf-Sheets-For-Gilding-Decoration-Art-Work-Craft-14-x-14cm-p-1004221.html](http://www.banggood.com/100-Leaves-Fake-Silver-Leaf-Sheets-For-Gilding-Decoration-Art-Work-Craft-14-x-14cm-p-1004221.html)





**"Anthony Bolgar"**

---
---
**Norman Kirby** *February 05, 2016 18:42*

wish this had come up earlier mine has already melted on one side, any good ideas on how to remedy that???


---
**Arestotle Thapa** *February 07, 2016 01:55*

I ordered an aluminum after the laser poke hole on mine as well. May be you can put some glue.


---
**Anthony Bolgar** *February 07, 2016 05:06*

If you have a 3D printer, I would recommend reprinting it if it is damaged. If you don't have a 3d printer, I could make you a new one if you cover the postage to get it to you.


---
**Arestotle Thapa** *February 07, 2016 21:53*

**+Anthony Bolgar** I've the lightobject head - not installed yet - but I'm guessing the laser probably will hit the inner wall too.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/d2fqt4kreRw) &mdash; content and formatting may not be reliable*
