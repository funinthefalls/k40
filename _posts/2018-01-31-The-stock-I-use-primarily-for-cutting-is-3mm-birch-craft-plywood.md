---
layout: post
title: "The stock I use primarily for cutting is 3mm birch craft plywood"
date: January 31, 2018 19:34
category: "Materials and settings"
author: "Joel Brondos"
---
The stock I use primarily for cutting is 3mm birch craft plywood.



Does anyone have recommendations for stains, oils, or finishes which work especially well with this wood?





**"Joel Brondos"**

---
---
**Andy Shilling** *January 31, 2018 20:48*

Not sure where you are in the world but in the uk we have Mylands liquid fast stain, this is mid oak with several layers of natural bee's wax. I always try to finish with a natural wax rather than laquer as it's easier to repair if it gets knocked.

![images/cbbcd186aa9f7a0f69cffaf4495ffae3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cbbcd186aa9f7a0f69cffaf4495ffae3.jpeg)


---
**Don Kleinschnitz Jr.** *January 31, 2018 23:07*

I pretty much use Minwax products. Use a pre-stain if you are staining.


---
**Ned Hill** *February 01, 2018 02:34*

For stain I recommend the Minwax products as well.  The birch I've used  hasn't required a pre stain personally.  Just don't sand below 200 grit before hand. For finishes on birch ply I typically like to use lacquer and sometimes polyurethane.


---
**Don Kleinschnitz Jr.** *February 01, 2018 02:49*

**+Ned Hill** if the wood is good quality the pre-stain may not matter and the results not blotchy but I do it just to be safe since wood quality these days is so unpredictable. 




---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/9JPgsgFwzzr) &mdash; content and formatting may not be reliable*
