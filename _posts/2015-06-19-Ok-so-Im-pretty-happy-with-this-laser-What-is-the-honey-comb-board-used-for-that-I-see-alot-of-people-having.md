---
layout: post
title: "Ok so I'm pretty happy with this laser, What is the honey comb board used for that I see alot of people having?"
date: June 19, 2015 00:32
category: "Hardware and Laser settings"
author: "Bruce D (Cam & Dad)"
---
Ok so I'm pretty happy with this laser, What is the honey comb board used for that I see alot of people having? Also How are you cutting material bigger than the vise?





**"Bruce D (Cam & Dad)"**

---
---
**Steve Moraski** *June 19, 2015 01:17*

The honeycomb bed will allow you to use the entire cut area of the laser.  Also it will allow things to be cut outside in as moshi often does.   In other words if you cut out a donut starting with the outside circle first it will not fall through and allow the laser to finish the inside circle.  


---
**Bruce D (Cam & Dad)** *June 19, 2015 02:20*

That's what I was hoping. I ordered one from ebay


---
**Steve Moraski** *June 19, 2015 02:31*

Which one did you purchase? 


---
**Bruce D (Cam & Dad)** *June 19, 2015 11:24*

300 x 200 on ebay

[http://www.ebay.com/itm/400500435723](http://www.ebay.com/itm/400500435723)


---
**Steve Moraski** *June 19, 2015 23:04*

Nice, a bit expensive though.  For future part and accessories check out [lightobject.com](http://lightobject.com) I just ordered mine from them for $23. 


---
**Jon Bruno** *June 24, 2015 00:53*

Bah! LO is out of em! Dang...


---
**Steve Moraski** *June 24, 2015 01:37*

Yeah noticed that after I ordered mine.   


---
*Imported from [Google+](https://plus.google.com/104630767939271001198/posts/doZ9GMvx9Ed) &mdash; content and formatting may not be reliable*
