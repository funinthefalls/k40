---
layout: post
title: "Roofing slate - cut, washed, and clear coat drying"
date: July 27, 2018 20:07
category: "Object produced with laser"
author: "Travis Sawyer"
---
Roofing slate - cut, washed, and clear coat drying. My sister-in-law has a stack of about 30 tiles (20*12") for me.



Any suggestions on power and speed? 



Using **+LightBurn Software** and a c3d on my k40

![images/cae64c8cfdab75fc470180b9438c1ed2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cae64c8cfdab75fc470180b9438c1ed2.jpeg)



**"Travis Sawyer"**

---
---
**Travis Sawyer** *July 27, 2018 23:34*

I really love Lightburn. So easy to use.



I only have to inkscape if I'm bending words ;)![images/c697ce10e4e279727004abdfce67600c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c697ce10e4e279727004abdfce67600c.jpeg)


---
**Travis Sawyer** *July 28, 2018 01:13*

![images/14ae5e9a7a05b01014e5223537c22932.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14ae5e9a7a05b01014e5223537c22932.jpeg)


---
*Imported from [Google+](https://plus.google.com/+TravisSawyer/posts/Jt34oWophNA) &mdash; content and formatting may not be reliable*
