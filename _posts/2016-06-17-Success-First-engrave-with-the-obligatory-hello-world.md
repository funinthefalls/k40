---
layout: post
title: "Success! First engrave, with the obligatory \"hello world\""
date: June 17, 2016 05:39
category: "Discussion"
author: "Evan Fosmark"
---
Success! First engrave, with the obligatory "hello world". Didn't even need to adjust the mirrors.



Really easy to get running once I got the software installed. Thanks everyone for the help.

![images/833da307e9510766f5e3ed43336cc7c2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/833da307e9510766f5e3ed43336cc7c2.jpeg)



**"Evan Fosmark"**

---
---
**Alex Krause** *June 17, 2016 05:46*

now you MUST LASER EVERYTHING!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 06:09*

Nothing is safe!


---
**Travel Rocket** *July 01, 2016 00:21*

What is your setup for this? Ampere and speed?


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/VERNBwtFW2S) &mdash; content and formatting may not be reliable*
