---
layout: post
title: "I like how cherry engraves really dark without having to color fill with a darker pigment"
date: April 22, 2017 03:14
category: "Object produced with laser"
author: "Jim Fong"
---
I like how cherry engraves really dark without having to color fill with a darker pigment.  Single coat of General Finishes Danish Oil.  



80mm/sec @ 10ma

Laserweb4/Smoothie



[https://imgur.com/a/ossvj](https://imgur.com/a/ossvj)





**"Jim Fong"**

---
---
**Ned Hill** *April 22, 2017 03:21*

Nice


---
**Kelly Burns** *April 22, 2017 12:05*

Cherry has and will always be my wood of choice.   The color just gets better and better with age.


---
**Steve Clark** *April 23, 2017 22:11*

Nice! I gotta get some of that wood. Any other woods that are as spectacular? 



I just played around with some bleached buffalo bone... you can't cut it but it engraves nicely under low power.



What about  dried Tagua Nut? Anyone tried that?



  


---
**Ned Hill** *April 23, 2017 23:23*

**+Steve Clark** I do a lot with Alder wood.  It lasers fairly darkly and the tone deepens depending on how you finish it.  In the pic below the top piece is unfinished.   The middle piece has just a basic clear low sheen spray sealer and the bottom is finished with 2 coats of gloss polyurethane.  

![images/6067b3b79b3dd68cd420c3e26b653c07.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6067b3b79b3dd68cd420c3e26b653c07.png)


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/9Bcs8D549PH) &mdash; content and formatting may not be reliable*
