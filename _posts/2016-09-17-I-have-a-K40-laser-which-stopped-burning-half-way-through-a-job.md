---
layout: post
title: "I have a K40 laser, which stopped burning half way through a job"
date: September 17, 2016 19:23
category: "Discussion"
author: "Stevie Rodger"
---
I have a K40 laser, which stopped burning half way through a job. The cnc part of the machine is working perfect, but the tube isn't firing. I replaced the tube, but still nothing. Has anyone any idea what could be the fault. Any help would be greatly appreciated.

Thanks..... Stevie





**"Stevie Rodger"**

---
---
**greg greene** *September 17, 2016 21:15*

The circuit goes as follows as I understand it



PSU Supplies HV to tube

WHEN

the laser switch makes contact (is depressed)

AND

the power Pot is turned to a value (usually 4 ma or over) that causes the tube to lase.



Therefore

either PSU is not supply HV

OR

Laser switch is not making  contact

OR

Pot is faulty



given your symptoms ie: stopped in middle of a job I'm betting on the PSU being FUBAR



You can test the laser switch with an ohm meter and the unit off

you can also test the pot with an ohm meter and the unit off



DO NOT TRY to test the HV unless you know what your doing - the consequences  could be lethal



Hope that helps


---
**Stevie Rodger** *September 17, 2016 23:55*

Thanks greg 


---
**Rick Ross** *September 18, 2016 01:25*

Do you have any other safety switches, water flow, lid switch ect.. If they went bad and opened your circuit that will stop your tube from firing also.




---
**Stevie Rodger** *September 18, 2016 09:03*

No  I don't have any other safety switches Rick.


---
*Imported from [Google+](https://plus.google.com/+StevieRodgerCambyCrafts/posts/HDsvWrScaBu) &mdash; content and formatting may not be reliable*
