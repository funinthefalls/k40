---
layout: post
title: "Using CorelDraw12, I made a layout for a new control panel in Acrylic"
date: December 21, 2015 17:00
category: "Software"
author: "Todd Miller"
---
Using CorelDraw12, I made a layout for a new control

panel in Acrylic.  My first test cut worked fine except

for cuts in the lower right hand corner wont go all the

way.  I suppose my mirror(s) need adjusted, but not right

now !



Is there a way I can take the entire page/project and rotate 

it 180 (this way I have no cuts in the week area) I do not

want to mirror.





**"Todd Miller"**

---
---
**Anthony Bolgar** *December 21, 2015 18:56*

It is very easy to rotate the drawing in CorelDraw 12. Go to the layout menu and choose rotate page orientation. That will rotate it 90- degrees. If you want to rotate it 180 degrees then select all objects in all layers and then go to the arrange menu and choose rotate, then enter 180 in the box for how many degrees to rotate. Hope this helps. If you have any other questions feel free to ask me, I have been a CorelDraw beta tester since version 4 and also teach graphics (mostly  using Corel products) at Niagara College of Arts and Applied Sciences.


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/ixf8X9UWC5M) &mdash; content and formatting may not be reliable*
