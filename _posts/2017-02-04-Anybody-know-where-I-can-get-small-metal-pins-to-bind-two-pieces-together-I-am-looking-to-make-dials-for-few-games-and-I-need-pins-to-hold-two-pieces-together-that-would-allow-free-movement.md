---
layout: post
title: "Anybody know where I can get small metal pins to bind two pieces together I am looking to make dials for few games and I need pins to hold two pieces together that would allow free movement"
date: February 04, 2017 04:23
category: "Discussion"
author: "3D Laser"
---
Anybody know where I can get small metal pins to bind two pieces together I am looking to make dials  for few games and I need pins to hold two pieces together that would allow free movement 





**"3D Laser"**

---
---
**Ariel Yahni (UniKpty)** *February 04, 2017 11:40*

Maybe a double side rivet or a chicago screw




---
**Ned Hill** *February 04, 2017 20:20*

Something like this?  They have a variety of lengths and diameters.  "Steel dowel pin" seems to be a good search term.  If you can't find the exact size you want, you might could try finding round bar/rod stock of the correct diameter and then cut to length.  [amazon.com - Amazon.com: 100 Pcs Stainless Steel 2.0mm x 15.8mm Dowel Pins Fasten Elements: Home Improvement](https://www.amazon.com/Stainless-Steel-15-8mm-Fasten-Elements/dp/B00B3MFRRK)


---
**3D Laser** *February 05, 2017 03:40*

**+Ned Hill** thanks Ned found some of these on eBay in the size I need


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/FgcArk4QYUD) &mdash; content and formatting may not be reliable*
