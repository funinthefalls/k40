---
layout: post
title: "Ok guys I am really really struggling to grasp inkscape and all of its glory lol"
date: January 28, 2016 05:42
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
Ok guys I am really really struggling to grasp inkscape and all of its glory lol. This link for example, simple enough download files open svg file for main frame and then follow my steps to use the inkscape plugin. Not so simple, with this example I get an error that image is part of a group and would need to be un-grouped, so I follow steps and cant get it done. Can someone please assist in this issue, meaning tell me how to ready main frame svg in this link, so I can cut it out on laser? If I can figure this out then I can possibly create all kinds of stuff downloading from thingiverse.





**"Andrew ONeal (Andy-drew)"**

---
---
**Justin Mitchell** *January 28, 2016 08:57*

There are no groups in those svg files, i just had a look. each contains just a single 'path'  so it must be an issue with the plugin




---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 28, 2016 09:40*

I just had a look in Adobe Illustrator and it looks like the compound path was not done correctly. Some of the shapes are not properly punched out of the main shape. I suggest colouring the main shape in a solid colour & looking at the smaller shapes that should be punched out of it you will see that not all are actually punched out properly.



E.g. look at this file I just saved from the original Main_Frame file

([https://drive.google.com/file/d/0Bzi2h1k_udXwdzZzTTI4bm84V2s/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwdzZzTTI4bm84V2s/view?usp=sharing))



Should look like this (I imagine):

([https://drive.google.com/file/d/0Bzi2h1k_udXwX0NObXpHM0p3cG8/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwX0NObXpHM0p3cG8/view?usp=sharing))


---
**Justin Mitchell** *January 28, 2016 09:56*

**+Yuusuf Sallahuddin**

 Good spot.  Okay, repairing this in inkscape isnt too hard, just a little tedious. select the main object, click on he colour bar (eg red) to set a fill colour. you will see the mistakes yuusuf refered to. now Path -> Break Apart and it will turn back into a pile of seperate shapes.  You now need to select one small cutout shape and the main body (hold shift whilst clicking) then Path -> Difference  repeat for each cutout until done. you then have a single shape with holes in it.


---
**Andrew ONeal (Andy-drew)** *January 28, 2016 17:28*

Awesome thanks for all the help! Is this a common problem when looking at pre designed svg files? Also, I seem to have issues with inkskape on a regular basis,especially when using plugins. Yuusuf, do you use adobe illustrator only or do you also use inkskape? Id prefer to use Corel draw but then had to use inkskape for laser or so I thought, double work? When I started this venture it was supposed to be fairly simple lol, it's been a beautiful nightmare ha. I never factored in that design was such a huge part of laser/3d prints.


---
**Andrew ONeal (Andy-drew)** *January 28, 2016 19:37*

**+Justin Mitchell** [https://drive.google.com/open?id=0B5YapASZj2L1bjhZMngyelBpb28](https://drive.google.com/open?id=0B5YapASZj2L1bjhZMngyelBpb28) Here is the file I ended up with following the steps and suggestions above. Now how do I get this to work with the inkscape plugin, I tried several things but still got error with plugin wanting to be ungrouped? Any Ideas, and why is this so difficult to understand? lol


---
**Justin Mitchell** *January 28, 2016 20:09*

**+Andrew ONeal** I don't know what has caused that, but a very simple trick seems to fix it.  cut the object, open a new document and paste it into the new document.  seems happy then.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 29, 2016 03:02*

**+Andrew ONeal** I use Adobe Illustrator for all my designing of files (saved as AI9 version) as I am still using the default software & controller that came with the K40. I've never used Inkscape before, so can't comment on it specifically. After I design the file, I then import it straight into CorelDraw with the LaserDrw plugin. Then I just select the objects I want to cut/engrave & choose the correct option from the LaserDrw.


---
**Andrew ONeal (Andy-drew)** *January 29, 2016 05:24*

Awesome, thank you again for all the help. I have another question, are you two familiar with the k40 panel cut out? I downloaded the dxf file imported to inkscape, now what do I do? It has 42 objects when I select all. What's next?? Between the last example help and this one I think I will really have an awesome understanding of what the heck I'm doing lol. Here's the link [https://drive.google.com/file/d/0B8S25d-5M3YyYTB6WTh1TVBNcmM/edit?usp=sharing](https://drive.google.com/file/d/0B8S25d-5M3YyYTB6WTh1TVBNcmM/edit?usp=sharing)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 29, 2016 06:50*

I haven't seen that K40 panel cutout before. Is it for replacing your electronics panel? I'm not really certain what the steps involved are for Inkscape workflow & haven't worked with DXF files before.


---
**Andrew ONeal (Andy-drew)** *January 29, 2016 19:09*

Yes for my electrics LCD mainly. I'm not certain either as to workflow in inkscape. I do know every answered question here brings me closer to understanding ask this better. As stated, I did not take into account having to design or be well versed in photo editing and vector graphics lol.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 30, 2016 06:34*

**+Andrew ONeal** It does pose certain problems if you are unskilled in photo/vector editing, however at least you are learning a new skill. Hopefully you can figure it out for your electronics panel.


---
**Andrew ONeal (Andy-drew)** *January 30, 2016 06:57*

Here's another struggle that I've waisted an entire night working on ugh. I want to take a pic and engrave it, any ideas


---
**Justin Mitchell** *January 30, 2016 11:50*

Cant help there sorry, ive not actually used the plugin in anger, my cutter is still languishing in LaserDraw land waiting for me to order the right connectors to upgrade


---
**Andrew ONeal (Andy-drew)** *January 31, 2016 05:52*

Which connectors? I soldered mine straight to the ribbon, if that's what you're referring to. You've helped a great deal thus far man, thanks for that.


---
**Justin Mitchell** *January 31, 2016 13:04*

Trying to retain the original connectors on all the wires, incase we ever need to put the original control board back. Have identified what the ribbon connector is, some of the others are a bit trickier to find and i cant find any online references saying what they are.  once i figure them out i can build a suitable breakout board to connect to the RAMPS or whatever control board we go with


---
**Andrew ONeal (Andy-drew)** *January 31, 2016 18:30*

Check these out, this is what I followed and studied intently. The second link is the breakout board. 



[https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion](https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion)





[https://oshpark.com/shared_projects](https://oshpark.com/shared_projects)


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/UCzQRBznmLK) &mdash; content and formatting may not be reliable*
