---
layout: post
title: "I did some searching for an answer to this on the smoothie site but only found bits of info"
date: December 12, 2016 13:10
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
I did some searching for an answer to this on the smoothie site but only found bits of info. 



I am interested in learning what all the functions of the GLCD panel are including the "laser" module.



Is there some documentation that explains each  of the panel menu items including the sub-menus? 



I am looking for something that explains the machines behavior when  a menu item is selected or a parameter set.



I am kinda hacking my way through it but found out the hard  way you can slam the head to its extreme if your not careful.





**"Don Kleinschnitz Jr."**

---
---
**greg greene** *December 12, 2016 13:20*

I'm guessing your talking about the Laser Web 3 software?  If so there is a take a tour button that will explain what each button does - not completely, but enough to get you started.  You can ask about a button you need more info on, by joining the group.


---
**Don Kleinschnitz Jr.** *December 12, 2016 14:28*

No, I am referring to the GLCD that is local to the machine.


---
**greg greene** *December 12, 2016 14:30*

ah, my machine didn't come with one


---
**Don Kleinschnitz Jr.** *December 12, 2016 14:31*

Mine did not either I added it with my Smoothie :).


---
**greg greene** *December 12, 2016 14:38*

I'm adding one to mine and the Cohesion 3D board - will let you know what I find


---
**Kelly S** *December 13, 2016 04:07*

I have one on the C3D in my machine, and so far the only use I have used it for is to home, disable motors and then re-position the carriage to where I want the file to start after I hit play to the file transferd over via LAN.


---
**greg greene** *December 13, 2016 13:59*

Did you find it worth the investment Kelly - I know it is cheap and looks cool - and yes I am putting one in too.


---
**Kelly S** *December 13, 2016 14:35*

Yes as it allows the machine to be standalone.   Very much worth it.


---
**greg greene** *December 13, 2016 14:52*

great !


---
**Don Kleinschnitz Jr.** *December 13, 2016 15:15*

**+greg greene** I do most of my troubleshooting from that panel.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/68qpygwbLBj) &mdash; content and formatting may not be reliable*
