---
layout: post
title: "If your looking a beautiful, transparent finish for plexiglass (acrylic), check out this product from DupliColor"
date: September 09, 2018 16:48
category: "Material suppliers"
author: "Mike Meyer"
---
If your looking a beautiful, transparent finish for plexiglass (acrylic), check out this product from DupliColor. This stuff is awesome; goes on easily, drys fast, and appears to durable. I'm building a plexiglass wind spinner, and this stuff looks just great.



![images/3e9931dbac002e4b9a38eb17656a2c46.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3e9931dbac002e4b9a38eb17656a2c46.jpeg)
![images/a618fb18c5af1294eb201e55ce31a2f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a618fb18c5af1294eb201e55ce31a2f1.jpeg)
![images/4bf5a058638ffddce6c23059502796ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4bf5a058638ffddce6c23059502796ae.jpeg)

**"Mike Meyer"**

---
---
**Don Kleinschnitz Jr.** *September 10, 2018 18:10*

Do you mask, apply peel directly on the acrylic? Where did you buy it?


---
**Mike Meyer** *September 10, 2018 19:24*

I bought the paint at O'Rileys; I think most chain auto parts houses carry it.



Regarding the plexi, I initially painted the surface of the workpiece with 2 coats of paint. Next, I masked off the entire surface with blue painters tape and gently lazed the design into the blades using about 3ma of power. It was just enough to cut the tape without scoring the plexi. When complete, I pulled tape in the design that I wanted. The remaining paint was removed by lightly sandblast the blades with double ought sand. BTW, as you can see, the sandblasting leaves the plexi translucent (which I wanted).



Here's a picture of the blades prior to sandblasting...



Hope this helps.



Mike

![images/caa50ac5049d8224dda3d74651455b7d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/caa50ac5049d8224dda3d74651455b7d.jpeg)


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/DRUoDzY6WLq) &mdash; content and formatting may not be reliable*
