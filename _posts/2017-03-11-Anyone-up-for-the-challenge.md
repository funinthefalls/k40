---
layout: post
title: "Anyone up for the challenge?"
date: March 11, 2017 15:16
category: "Repository and designs"
author: "Ariel Yahni (UniKpty)"
---
Anyone up for the challenge? 

![images/8ed9737700c84350f65bda3c7ea05a1a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ed9737700c84350f65bda3c7ea05a1a.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**greg greene** *March 11, 2017 15:37*

I like it !


---
**Cesar Tolentino** *March 11, 2017 16:01*

There is also similar to this posted at 3dwarehouse for SketchUp users which he modeled a lot of different connections and locking mechanism without bolt it screw


---
**Ariel Yahni (UniKpty)** *March 11, 2017 16:02*

**+Cesar Tolentino**​ you know the name for reference so I can search for it?


---
**Cesar Tolentino** *March 11, 2017 16:03*

Give me a few


---
**Cesar Tolentino** *March 11, 2017 16:10*

Ok i found it.... it is called DIGITAL JOINTS....  For people who are not familiar with Sketcup, the free version is Sketchup Make. And then there is a plugin to make it STL, or a plugin to make it DXF.... Hope this helps....



[3dwarehouse.sketchup.com - 3dwarehouse.sketchup.com/collection.html?id=u0e981088-0ffa-432b-b126-6f3a14f3b78d](https://3dwarehouse.sketchup.com/collection.html?id=u0e981088-0ffa-432b-b126-6f3a14f3b78d)



and for the plugins, here is the repository...

[https://extensions.sketchup.com/](https://extensions.sketchup.com/)








---
**Cesar Tolentino** *March 11, 2017 16:10*

![images/6f5b0fc97ae43c7cab8ac2de1d85b1b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6f5b0fc97ae43c7cab8ac2de1d85b1b1.jpeg)


---
**Ariel Yahni (UniKpty)** *March 11, 2017 16:15*

I love Sketchup. Easy to learn and use, very powerful once you know how to use it. I even modify STL with it


---
**Cesar Tolentino** *March 11, 2017 16:20*

Haha...Ariel, you have to tell me how to modify STL with it. I always end up redrawing the whole thing.... what is your secret....




---
**Ariel Yahni (UniKpty)** *March 11, 2017 16:25*

If the STL is very complex, forget it and go to Fusion 360. If its more simple then i just delete all the tris ( cross lines  in the face ) and from there add, delete, etc. I should do a video of that


---
**Andy Shilling** *March 11, 2017 20:38*

**+Ariel Yahni** Yes you should do I video on that ;) I would love to lean a bit more of sketchup from somebody using the same tools as me.




---
**Stephane Buisson** *March 11, 2017 22:22*

to modify stl look at Meshmixer

[meshmixer.com - Autodesk Meshmixer free software for making awesome stuff Home ...](http://www.meshmixer.com/)


---
**Ariel Yahni (UniKpty)** *March 11, 2017 22:29*

**+Stephane Buisson**​ organics shapes yes, parts and similar I does not offer the same precision 


---
**Don Kleinschnitz Jr.** *March 12, 2017 12:47*

**+Cesar Tolentino** can you tell me what DFX plugin you use? I haven't found one yet that works correctly.


---
**Ariel Yahni (UniKpty)** *March 12, 2017 12:53*

**+Don Kleinschnitz**​ download the latest SketchUp and use native dxf export


---
**Don Kleinschnitz Jr.** *March 12, 2017 16:27*

**+Ariel Yahni** I don't have the pro version.


---
**Ariel Yahni (UniKpty)** *March 12, 2017 16:28*

**+Don Kleinschnitz**​ latest Make version also saves to dxf. I tried it yesterday to confirm


---
**Don Kleinschnitz Jr.** *March 12, 2017 16:37*

**+Ariel Yahni** wow that must be new?


---
**Ariel Yahni (UniKpty)** *March 12, 2017 16:39*

I think so, I was surprised myself


---
**Cesar Tolentino** *March 12, 2017 17:35*

Thanks Ariel for the follow up. Let me know if you also cannot find the import export of STL file


---
**Ariel Yahni (UniKpty)** *March 12, 2017 17:42*

**+Cesar Tolentino** i do see import stl and Export STL. Im not sure if stock has it as i got a pluging for that i believe




---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/3UnScnWkBjW) &mdash; content and formatting may not be reliable*
