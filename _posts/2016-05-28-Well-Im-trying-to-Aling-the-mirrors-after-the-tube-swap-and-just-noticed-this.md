---
layout: post
title: "Well I'm trying to Aling the mirrors after the tube swap and just noticed this"
date: May 28, 2016 04:06
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Well I'm trying to Aling the mirrors after the tube swap and just noticed this. Is this common on the lens? Seems like the costing is burned? 

![images/5c85ed6d935d94e34f6836dfa9d0d8c7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c85ed6d935d94e34f6836dfa9d0d8c7.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Phillip Conroy** *May 28, 2016 04:21*

Mirrors do wear out ,thet need checking weekly and cleaning if needed.

When they get dirty heat builds up and damages the mirror,that is why i keep sparesnmorrors and focal les,what are u using laser for?


---
**Ariel Yahni (UniKpty)** *May 28, 2016 04:29*

The laser is "brand new".  Came with a cracked tube,  just being replaces


---
**Ariel Yahni (UniKpty)** *May 28, 2016 04:52*

Is on the first fixed mirrror


---
**Alex Krause** *May 28, 2016 12:48*

Did you use the double sided tape to help align the mirrors? I had some residue on my mirror after using the double sided tape to align 


---
**ThantiK** *May 28, 2016 13:51*

Yeesh, am I the only one that got a K40 that was perfectly aligned, with non damaged mirrors, non damaged tube, that worked perfect out of the box?


---
**Alex Krause** *May 28, 2016 14:04*

All I had to do with alignment was adjust the mirror on the gantry a tiny bit. It was hitting slightly off center in the laser head. It worked out of the box for small pieces to engrave but I noticed there were a few spots to the end of the X travel where it wouldn't cut all the way thru my material


---
**Tony Sobczak** *May 28, 2016 19:17*

**+ThantiK** Same here, never touched anything, cut 1/4" plywood like butter. Had to raise the bed 1/4" to get it in focus though.


---
**Pippins McGee** *May 30, 2016 22:03*

**+Tony Sobczak** hi tony how does one raise the bed on the k40? cheers


---
**Tony Sobczak** *May 30, 2016 23:34*

**+Pippins McGee** Using standoffs. 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/54Wp4DQcZZu) &mdash; content and formatting may not be reliable*
