---
layout: post
title: "I got my custom adapter/switcher boards in the mail today"
date: March 30, 2016 01:31
category: "Modification"
author: "Vince Lee"
---
I got my custom adapter/switcher boards in the mail today.  Excited to finally hook up the smoothie.

![images/64e27c8d85346cba170410a0e4891f33.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/64e27c8d85346cba170410a0e4891f33.jpeg)



**"Vince Lee"**

---
---
**Will Travis** *March 30, 2016 03:12*

Vince it looks like this compliments your awesome looking panel.  Can you give us a little more information on what this board does.  It looks like it connects your Smoothieboard to the rest of your laser but what are the DIP switches for?  Do you have a schematic that we could look at?   


---
**Vince Lee** *March 30, 2016 04:35*

Hi.  It's designed to connect both to a smoothieboard and the original Moshiboard simultaneously and allow switching between the two using the three 5pdt dip switches.  I just finished populating one and tested passing thru to the moshiboard and powering up the smoothieboard, and am starting to make the other jumpers for the Smoothieboard.


---
**Tony Sobczak** *March 30, 2016 05:48*

Interested in hearing more. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 07:21*

That's interesting. I'm curious to know why you would want to be able to switch between the Smoothie & Moshi boards? Is there some benefit to this?


---
**Vince Lee** *March 30, 2016 14:17*

I have the board populated now and all hooked up except the endstops and fire pin; I'll make the jumpers for those shortly.  I'll post pics with a schematic soon.  I've gotten used to the Moshi so I want to keep it for flexibility, compatibility with past projects, and also as a backup in case the smoothie gives out.  I don't have any experience using the open source software yet but it seems like more steps, especially for quick engraving jobs or on-the-fly tweaks.


---
**Will Travis** *April 02, 2016 03:55*

Ah that makes sense.  I look forward to seeing the finished result.  Would also love to see the schematic when you are done.   Thanks for sharing.  


---
**Vince Lee** *April 02, 2016 16:53*

I got it all up and running late last night (er..early this morning) and will gather some pics to do a writeup and post shortly!


---
**Rob Thompson** *April 26, 2016 16:33*

Good stuff, I only just got my K40 and its got promise; I just can't stand the LaserDraw software and want to send vector/G-code to it (I'm a CAM software developer so it makes me cringe to not do so ;-). So even though its a week old, I've ordered the 4X Smoothie so I can hopefully correct this. I have an M2 board in mine, would this board suit this too - if I were to leave the "old" one in?


---
**Tony Sobczak** *April 26, 2016 19:37*

**+Vince Lee** waiting for the pics. 


---
**Vince Lee** *April 27, 2016 03:55*

I posted some pics in a separate post but the full write up is here. [http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html](http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html)


---
**Vince Lee** *April 27, 2016 03:57*

Btw After a few hiccups I got visicut working with it and have been very impressed.  


---
**Tony Sobczak** *April 30, 2016 02:04*

Ate you going to make your boatds available. 


---
**Vince Lee** *April 30, 2016 03:43*

**+Tony Sobczak** I have the files linked from my blog but if u are in the U.S. just message me your address and I can mail u one of my extras while they last.


---
**Mike Mauter** *October 17, 2016 01:00*

Hi Vince Do you have any of your Dual Boars Left? Thanks


---
**Vince Lee** *October 28, 2016 04:58*

Yeah.  I'm out of middlemen boards but I still have a few of the dual switchers.  Just private message me (post with only me as the recipient) where to send it and I can pop it in the mail.


---
**Mike Mauter** *October 28, 2016 12:44*

 **+Vince Lee** 




---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/Mj4ohKUnwQH) &mdash; content and formatting may not be reliable*
