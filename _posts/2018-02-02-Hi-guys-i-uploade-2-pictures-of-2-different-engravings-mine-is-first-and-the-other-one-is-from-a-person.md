---
layout: post
title: "Hi guys, i uploade 2 pictures of 2 different engravings mine is first, and the other one is from a person"
date: February 02, 2018 14:22
category: "Object produced with laser"
author: "Andrei Voicu"
---
Hi guys,

i uploade 2 pictures of 2 different engravings mine is first,

and the other one is from a person. is the 2nd one not laser engraved? and if it is... how is it done? mine is engraved with k40: power 6 mA and speed 

150 mm/s. 

Thank you



![images/a8a9a4022790239f0ee16147f11c2fb1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a8a9a4022790239f0ee16147f11c2fb1.jpeg)
![images/a10b37b01e980380191247a18d8223ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a10b37b01e980380191247a18d8223ac.jpeg)

**"Andrei Voicu"**

---
---
**Jim Hatch** *February 02, 2018 15:31*

Looks like the second one was a half-toned image (something like Floyd-Steinberg) and then lasered. You can do the same thing by preprocessing the photo using an image program like AI or even Gimp.


---
**Andrei Voicu** *February 02, 2018 16:57*

Hi,

so the first one which is mine , before engraving i either choose jarvis, stucki or floyd steinberg... just depends which one looks better... when looking the the 2nd image from my point of view it looks like the laser didnt engrave it, but created the picture out of dots.

i put more pictures

[https://plus.google.com/photos/...](https://profiles.google.com/photos/113571233865169446047/albums/6518002102879484017/6518002100906297202)


---
**Jim Hatch** *February 02, 2018 17:11*

You can adjust the dot pattern density in the half-toning step to spread it out more like the other person's picture. In Gimp you'd use the Filter, Distorts and Newsprint. Then go to Tools, Selection Tools, By Color and select one of the half-tone dots. Use bigger dots with more spacing to get the dotted look and eliminate big solid black blocks. Then export the path.


---
*Imported from [Google+](https://plus.google.com/113571233865169446047/posts/DR3VRPN5mqD) &mdash; content and formatting may not be reliable*
