---
layout: post
title: "I can see that I maybe need some kind of air cooling to my laser cutter?"
date: May 31, 2015 05:57
category: "Discussion"
author: "Nikolaj Hygebjerg"
---
I can see that I maybe need some kind of air cooling to my laser cutter? Anyone can help how to do that?

AND I have no idea how to adjust the focal lens - can anyone explain?





**"Nikolaj Hygebjerg"**

---
---
**Henner Zeller** *May 31, 2015 07:06*

You definitely <i>need</i> water cooling for your Laser, otherwise it is dead pretty quickly.



For the chamber, you might want some air-exhaust system to keep it smoke-free (and connect that to some filter or through a window).



You usually don't adjust the focal lens itself, but the distance between the lens and the stuff to cut; there are standard distances (one typical one is 7cm); once you have determined that, it is useful to make a template: a piece of wood or acrylic that you use as measurement gauge between the laser and the item to cut.


---
**Nikolaj Hygebjerg** *May 31, 2015 14:28*

Oh, I have water-cooling. I have a small pump cooling with water. But they said I need Air-cooling to be able to cut in plywood. 


---
**Henner Zeller** *May 31, 2015 16:07*

Moving air is needed to get the smoke out of the cutting area. First it makes sure that the laser has free sight and second it helps that the lens is not sudded up.


---
**Eric Parker** *June 03, 2015 18:05*

**+Nikolaj Hygebjerg** I believe more accurately it would be Air Assist.  Basically a nozzle that goes over the lens thats forces air down towards the work piece.  I'm not 100% certain why it helps, but my theory is it clears smoke from the beam path, allowing the full power of it to reach firther into the material.


---
**Henner Zeller** *June 03, 2015 18:14*

It just makes sure to not fog the lens over time. Plastic residue is nasty, so you might need to clean or replace the lens often otherwise.


---
**Eric Parker** *June 03, 2015 18:46*

I clean my optics once a day, at the very least.  I clean them immediately after cutting something especially smoky or nasty.


---
**Eric Parker** *June 05, 2015 15:06*

lint free wipes and acetone or denatured alcohol is what I use.


---
*Imported from [Google+](https://plus.google.com/104942606798861439290/posts/8VXK8UDSyM2) &mdash; content and formatting may not be reliable*
