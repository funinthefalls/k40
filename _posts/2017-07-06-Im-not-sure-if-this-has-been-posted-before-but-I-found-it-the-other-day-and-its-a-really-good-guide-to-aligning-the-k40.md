---
layout: post
title: "I'm not sure if this has been posted before but I found it the other day and it's a really good guide to aligning the k40"
date: July 06, 2017 21:20
category: "External links&#x3a; Blog, forum, etc"
author: "Jeff Lamb"
---
#k40alignment I'm not sure if this has been posted before but I found it the other day and it's a really good guide to aligning the k40.  I was having problems aligning since I added adjustable laser mounts but with this guide I managed I really good alignment in a couple of hours. 

[https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/](https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/)



Hope this helps someone. 





**"Jeff Lamb"**

---
---
**HalfNormal** *July 06, 2017 23:44*

Yep! [plus.google.com - A third laser alignment guide Another post by +Nathan Thomas referenced this...](https://plus.google.com/+HalfNormal/posts/5LpDnupS7fb)


---
**Alex Krause** *July 07, 2017 05:42*

**+HP Persson**​ is the author of this guide... And very active in the FB K40 group


---
*Imported from [Google+](https://plus.google.com/100451757440368369818/posts/AoPFSDn3DBs) &mdash; content and formatting may not be reliable*
