---
layout: post
title: "My solution to the no Z-Axis issue"
date: February 24, 2018 20:56
category: "Discussion"
author: "TheTopheezzy"
---
My solution to the no Z-Axis issue. 



![images/7876f14deb06ace78c5aa45e627942bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7876f14deb06ace78c5aa45e627942bd.jpeg)
![images/ac8363ded9e9edb4dbe72d968c4af895.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ac8363ded9e9edb4dbe72d968c4af895.jpeg)
![images/ddd997eb984ce53ce443d6f93d85ba8f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ddd997eb984ce53ce443d6f93d85ba8f.jpeg)
![images/2d2614e19d3696438d9af90c675d4281.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2d2614e19d3696438d9af90c675d4281.jpeg)
![images/c654122d14476b2e2e5cbfb257778bdb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c654122d14476b2e2e5cbfb257778bdb.jpeg)

**"TheTopheezzy"**

---
---
**Josh Rhodes** *February 24, 2018 21:06*

#genius 


---
**Seon Rozenblum** *February 24, 2018 21:53*

Oh, that is very clever!


---
**TheTopheezzy** *February 24, 2018 21:58*

**+Seon Rozenblum** it’s not the most sturdy thing. But I’ll fix it one day. But for right now it works like a charm. 


---
**Seon Rozenblum** *February 24, 2018 22:00*

**+TheTopheezzy** That's a cool looking tube for the air-assist. Where did you get it? And what is it plugged into? I have a big air-compressor for my air assist, but it's sooooooooo loud, neighbours get upset when I am using it.


---
**TheTopheezzy** *February 24, 2018 22:06*

I bought everything over 2 years ago and I don’t remember any of the names or brands of anything I bought . All I know is I bought it off of eBay ![images/e46dd87dff469a1d37958efbf8d6ea63.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e46dd87dff469a1d37958efbf8d6ea63.jpeg)


---
**Seon Rozenblum** *February 24, 2018 22:16*

**+TheTopheezzy** No problems.. that looks like a much better airflow pump. I might get something that size. Cheers :-)


---
**TheTopheezzy** *February 24, 2018 22:17*

**+Seon Rozenblum** it’s so quiet! I mean, you hear it, but it definitely is quiet haha


---
**James Rivera** *February 24, 2018 23:40*

LOL! While cleaning out my garage I came across an old scissor jack I don’t need but was reluctant to throw out. Not sure if I want to go as far as cutting such a big hole in my K40 case, but it gives me another option! 


---
**Claudio Prezzi** *February 25, 2018 10:36*

Doesn't that big hole in the bottom make the fume extraction inefficient? I will suck fresh air from the bottom insted of the fume from the top. 


---
**TheTopheezzy** *February 25, 2018 11:40*

**+Claudio Prezzi** if I’m lasering at the top in front of the extraction opening, I figured it wouldn’t matter right?? 


---
**Claudio Prezzi** *February 25, 2018 14:08*

Air takes the path of least resistance, so I guess most will come from the bottom.


---
**TheTopheezzy** *February 25, 2018 16:07*

**+Claudio Prezzi** which would pass by what I’m etching? Idk I think it’ll work haha


---
**James Rivera** *February 25, 2018 17:30*

I would expect the fumes to be slightly warmer than the air from the bottom, which should make it rise, correct? If so, the fine extractor is probably working fine.


---
**Steve Clark** *February 28, 2018 17:11*

**+Seon Rozenblum**  here is the air pump I've converted mine to. If you get one watch that you get the right sized one as they have different outputs. I'm happy with 951 gph. I also use this variable switch to control the air output.



[ebay.com - Details about Air Pump, Commercial 951 gph Elemental O2 Aquarium Hydroponics Aquaponics Pond](https://www.ebay.com/itm/Air-Pump-Commercial-951-gph-Elemental-O2-Aquarium-Hydroponics-Aquaponics-Pond/152924732827?epid=1226489763&hash=item239b063d9b:g:3bIAAOSwHnFVv312)



[https://www.ebay.com/itm/Variable-Active-Air-Duct-Booster-Fan-Speed-Controller-Hydroponics-Inline-Exhaust/382388053159?epid=2255392380&hash=item59081aa4a7:g:xSUAAOSwWVhajcWf](https://www.ebay.com/itm/Variable-Active-Air-Duct-Booster-Fan-Speed-Controller-Hydroponics-Inline-Exhaust/382388053159?epid=2255392380&hash=item59081aa4a7:g:xSUAAOSwWVhajcWf)


---
**James Doebbler** *March 03, 2018 02:25*

Nice idea. Maybe something similar but just four holes for the supports would allow better fume management (jack and wood below, rods running through bottom, expanded metal inside).


---
*Imported from [Google+](https://plus.google.com/108628104873176736569/posts/gSdSRwHzuqq) &mdash; content and formatting may not be reliable*
