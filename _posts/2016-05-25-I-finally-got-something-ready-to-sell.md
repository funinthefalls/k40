---
layout: post
title: "I finally got something ready to sell!"
date: May 25, 2016 08:55
category: "Discussion"
author: "Scott Marshall"
---
I finally got something ready to sell!

Here's the details:



The shirts listed on Ebay are packed and ready to ship in 1 business day or less (I go to the PO every afternoon, and if I get the order before noon, it will go out the same day.) If you want more than one, drop me note, and I'll give you a discount. (Sizes 2X & Up are 2 bucks higher, but it's cheaper to ship multiples. My policy not to make money on shipping.) 



If you want something custom made for you. Email me direct. 

Direct K40 Forum customers get a 10% discount. 

SCOTT594@AOL.COM



I'll do the art for your approval. You are not committed until you like what you see! Once you're happy I'll press your shirt and get it off to you in short order. I have Pocket Tees, Collared and Golf Shirts. Kids sizes (Kids LOVE T-Shirts), Even Jackets. Loud or 'tasteful', any way you like! On ebay my shipping is "No International", but if you want one shipped outside of the US, Email me with your info, and, as with ANY request, I'll see if we can make it happen!



K40 Owners, Members of the K40 Community or Hobby Laser Cutter Builders Show off your hobby!



The Google K40 Community is THE place to be for K40 owners, users and tinkerers. 



Here is a shirt to go with all of your laser products.



Great for Maker Fairs,Trade Shows, and Craft shows when you sell your Laser products!



It's fun to show off your Hi-tech Hobby, or just to let your friends and family know what you do in the basement all those evenings!



I do custom shirts with anything you like, Laser or not. Any sizes or colors available with FAST turnaround! 



Free one-off artwork, or we can use yours!



Check my  listings, I have a variety of my original artwork Laser and other tech related shirts in a variety of sizes and colors - ready to ship!



My shirts are the best you can buy! Made on a top of the line industrial duty press, from great raw materials, one at a time by hand with care, these are made with pride in the United States. 100% satisfaction guaranteed!



In the next couple days, I'll be releasing my K40 Community Artwork for our Community members to use free in their Lasers for making personal items (Please don't sell it)

It's in a proprietary vinyl-cutter plotter format and I need to export the files to something usable for the K40 so I can post it here.



![images/0a950b9a275afb139ed04ab2bc4385e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a950b9a275afb139ed04ab2bc4385e5.jpeg)
![images/4fe23b88c59aee2fd17f0149422da1bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4fe23b88c59aee2fd17f0149422da1bc.jpeg)
![images/89405f1b9c51fbcaf25553d47ebff6b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/89405f1b9c51fbcaf25553d47ebff6b3.jpeg)
![images/0714d58cfeaa23248404d409aa49a77b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0714d58cfeaa23248404d409aa49a77b.jpeg)
![images/8fd26bf9945fe182d048d227250ba149.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8fd26bf9945fe182d048d227250ba149.jpeg)
![images/f1f2636472e822aa1da0c8f19f57625e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1f2636472e822aa1da0c8f19f57625e.jpeg)
![images/5affbdde257d712c392c724ebb0ec03d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5affbdde257d712c392c724ebb0ec03d.jpeg)

**"Scott Marshall"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 09:38*

That's a great idea & I will have to get me one. I like the one that says Hide all raw materials! hahaha.



I second the QR code idea.



I'll have to get one off you in the near future (as $ permits).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 09:46*

Just for an artistic idea, laser an explosion hole into the shirt. Then put an arrow & say something like "this is what happens when you have a family member with a K40 Laser".


---
**Scott Marshall** *May 25, 2016 10:01*

That's a really good idea, I've got to do some experimenting, maybe cut the hole and seal it from the back with iron on style tape.....


---
**Scott Marshall** *May 25, 2016 10:02*

I hate to show my ignorance of the high tech world, but what IS a QR code?

Is that the square barcode deal? If so How do I create one?, That would be a great addition...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 10:06*

An example of a QR code is something like this:

[https://www.colourbox.de/preview/2250519-big-sale-data-in-qr-code-modern-bar-code-eps-8-vector-file-included.jpg](https://www.colourbox.de/preview/2250519-big-sale-data-in-qr-code-modern-bar-code-eps-8-vector-file-included.jpg)



It looks like a bunch of random pixels. But it actually follows a pattern. You use a generator to create the QR code which then when scanned with a QR reader (e.g. your mobile phone via the camera & an app) it provides the user with a direct link to a URL. So, basically you can point your phone at one of these QR codes & it will automatically load the corresponding website that goes with that code.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 10:07*

**+Scott Marshall** I will create one for the K40 URL, then I will share it with you once done. I have an addon for Adobe Illustrator that does it.


---
**Scott Marshall** *May 25, 2016 10:08*

Thanks for the link Peter, I'll be adding that on for sure. Seems it ought to cut in Transfer vinyl well enough...I could even add it to the existing shirts.


---
**Scott Marshall** *May 25, 2016 10:12*

**+Yuusuf Sallahuddin** 

Thanks, you guys are great. 



I'll see what's involved in getting it into the Cutter CAD, I may have to draw it, but that's not a big deal, once done, it will just copy into each drawing.



I've got a friend with a smartphone who can test it. I'm one of those guys with a four function phone..


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 10:12*

**+Scott Marshall** I've just created one & you should be able to access here. SVG format. [https://drive.google.com/file/d/0Bzi2h1k_udXwalhfRjM1ckdwZkE/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwalhfRjM1ckdwZkE/view?usp=sharing)



Although, let me verify that it works properly before you go cutting/transferring to shirts. I'd say you want to make it about 1 inch square minimum too, to assist with scanning, else people will have to put their camera within personal boundary zone haha.





edit: NOPE, that one isn't working. Getting a message "the requested URL was not found on this server. That's all we know"


---
**Scott Marshall** *May 25, 2016 10:14*

**+Yuusuf Sallahuddin**

Can you send one in .jpg or .bmp?, I don't have a lot of vector friendly software - yet... - never mind, I was able to export it. I'll have to draw it over the image and then I'll have it in a format I can put on a shirt or decal.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 10:18*

**+Scott Marshall** Four function phone? You mean: ring, answer, text & hammer? Haha.



I've just tested the code on my QR reader & it is giving the correct URL, according to K40 group main url at the top, but it is not working when the browser loads it. Once I figure out what is going on, I'll send a BMP version. JPG will get compression artefacts which could make it more difficult to recreate.


---
**Scott Marshall** *May 25, 2016 10:22*

It should be OK, I'm going to draw in in large scale, like a square foot, then reduce it. I know it's got to be square, so I can keep the aspect ratio locked as I go, once shrunk, it should be right on. Those readers must be pretty forgiving, especially if they'll read off a shirt, which I'd expect they will just fine.


---
**William Steele** *May 25, 2016 10:38*

I think a funny warning on the back would be cool... "Warning: Do not look into laser with remaining eye!"


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 10:40*

**+Scott Marshall** Yeah, you just need to be within a certain focusing distance. I would prefer not to have people putting their camera within an arms length of my body, but for the purposes of trade fairs etc, 2 inch would probably be a good size.



[https://drive.google.com/folderview?id=0Bzi2h1k_udXwRGN1QzdMNjF5WUU&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwRGN1QzdMNjF5WUU&usp=sharing)



There is a bunch of versions in there. Unfortunately I can't seem to get it to work linking directly to the main page for the K40 Group ([https://plus.google.com/u/0/communities/118113483589382049502](https://plus.google.com/u/0/communities/118113483589382049502)).



But I can get it to link directly to the members page ([https://plus.google.com/u/0/communities/118113483589382049502/members](https://plus.google.com/u/0/communities/118113483589382049502/members)).



Everytime it scans & loads the link for the K40 group main page it throws a 404 error. May just be my phone though (I have a Lumia Windows phone).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 10:50*

**+Scott Marshall** I've posted a specific post asking if people can test the QR code that links directly to the main page since it is returning 404 error for me. But the actual URL that has been put in the address bar on my phone is exactly what I see in the laptop address bar for this group.


---
**Greg Curtis (pSyONiDe)** *May 25, 2016 12:18*

+1 on the CO2 one


---
**Scott Marshall** *May 25, 2016 13:01*

Check my new post with the cad drawn version, it should be dead on geometrically, if I didn't get dyslexic by the end there, I'll be seeing .500" black squares awhile...



2 hrs ago I didn't know what a QR was, now I know more about how they work then 99% of the people out there. Gotta love Tech!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 15:17*

**+Scott Marshall** Funny that you mention the seeing squares for a while, as I've done some artwork called Kufi Squares (& other's that are based on the Japanese gago-in style artist name stamps) & they're very similar to a QR code in that they're basically made up of a bunch of squares either on/off in the grid. When you're zoomed in super close to draw Kufi square phrases (in Arabic) clockwise in a spiral it can really do your eyes in.


---
**HalfNormal** *May 25, 2016 17:46*

**+Scott Marshall** what would this look like on a t? 

[https://plus.google.com/108784098850913728119/posts/SVhfrLNvmg2](https://plus.google.com/108784098850913728119/posts/SVhfrLNvmg2)


---
**I Laser** *May 27, 2016 08:19*

I'm having one of those months, so apologies if I've just missed it, could you please post link to ebay listing Scott?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 27, 2016 08:25*

**+I Laser** Here's a link Scott shared with me for one of the shirts.



[http://www.ebay.com/itm/K40-Laser-T-Shirt-Ash-Grey-Medium-/272253242232?hash=item3f638ecb78:g:H0IAAOSwH6lXRKHT](http://www.ebay.com/itm/K40-Laser-T-Shirt-Ash-Grey-Medium-/272253242232?hash=item3f638ecb78:g:H0IAAOSwH6lXRKHT)



I'm not sure if it is something on my end, but I can't view the other listings. Just shows a blank page with 0 items for sale.


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/LtTZtQi9Udk) &mdash; content and formatting may not be reliable*
