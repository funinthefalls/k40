---
layout: post
title: "Those of you who've expanded your bed area into the whole machine case (removing electronics, etc), how are you dealing with the exhaust port being on one side?"
date: November 21, 2016 16:59
category: "Modification"
author: "K"
---
Those of you who've expanded your bed area into the whole machine case (removing electronics, etc), how are you dealing with the exhaust port being on one side? I cut my first item that stretched almost the length of my bed (550mm) and the right side had much more residue underneath than the left side which is directly in front of the port. Have you moved your exhaust to the center and cut a new hole? Cut a secondary hole and made some sort of Y adapter for the flexible hose?





**"K"**

---
---
**Don Kleinschnitz Jr.** *November 21, 2016 17:12*

interesting..... how mods create new challenges.


---
**Mark Smith** *November 29, 2016 00:54*

I saw one video where a guy cut his out with a dremel or some other cutting wheel, which also made more room... But he also put a different blower fan on the rig, so it might not "count". I'm probably going to 3d print an adapter that goes over the vent as it comes into the case, and expands out to a narrower gap, but one that extends further to the right corner, so the suction stays semi-constant across the entire back wall. Not sure if it'll work, though.




---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/UxcznxdPfPx) &mdash; content and formatting may not be reliable*
