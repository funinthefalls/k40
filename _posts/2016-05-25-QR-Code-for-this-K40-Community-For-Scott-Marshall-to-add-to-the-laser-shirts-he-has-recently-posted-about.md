---
layout: post
title: "QR Code for this K40 Community. For Scott Marshall to add to the laser shirts he has recently posted about"
date: May 25, 2016 10:47
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
QR Code for this K40 Community. For **+Scott Marshall** to add to the laser shirts he has recently posted about.



Can people please give this a test on their QR Code Reader apps. I have tried it on mine on my phone (Lumia 950) & when it loads the URL I keep receiving an error stating "404. That's an error" & "The requested URL was not found on this server. That's all we know."



However, the URL in the address bar is exactly what I see in the address bar on the computer (even copy & pasted it directly from there into my QR generator extension).



Please leave results if it works for you or not. Thanks.

![images/38c417d83bd3d420e6b76cf6c42a3b18.png](https://gitlab.com/funinthefalls/k40/raw/master/images/38c417d83bd3d420e6b76cf6c42a3b18.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Simon Jackson** *May 25, 2016 11:55*

Works fine for me using WeChat on a Sony Z1


---
**HalfNormal** *May 25, 2016 12:51*

Android bar code scanner app on Samsung Note works fine.


---
**Stephane Buisson** *May 25, 2016 12:52*

create your own logo embeded into QR code

[http://uqr.me/](http://uqr.me/)


---
**Don Kleinschnitz Jr.** *May 25, 2016 13:56*

works on my Samsung S5.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 14:38*

**+Stephane Buisson** I thought I'd seen QR codes with logos embedded before but then second-guessed myself & thought I had imagined it. Do we have an official logo for the K40 community here?



Thanks **+Simon Jackson** **+HalfNormal** **+Don Kleinschnitz** for testing. Seems I've managed to get it to work as well on my end, although I honestly did nothing but manually open the community page first, then scan the code & it decided to work. Some strange error.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/gyssM4WetCY) &mdash; content and formatting may not be reliable*
