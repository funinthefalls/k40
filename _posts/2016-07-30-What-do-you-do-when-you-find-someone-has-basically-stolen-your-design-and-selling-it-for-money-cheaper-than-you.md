---
layout: post
title: "What do you do when you find someone has basically stolen your design and selling it for money cheaper than you"
date: July 30, 2016 15:09
category: "Discussion"
author: "3D Laser"
---
What do you do when you find someone has basically stolen your design and selling it for money cheaper than you





**"3D Laser"**

---
---
**greg greene** *July 30, 2016 15:20*

Depends on what 'Basically' means.  If it has been changed by more than 10% your hooped.


---
**Gee Willikers** *July 30, 2016 15:45*

Is it that gentelman from England on the Facebook group? He's stolen a bunch of peoples work and then blocked us all from his pages. 



What to do? Short version - never post photos of your work. Beyond that there's not much you can do cheaply.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 30, 2016 15:56*

I'd suggest changing your design to something "improved" from the original, so you can continue selling & be the better product. Bit of a prick that someone stole your design though. Maybe out them publicly, name & shame sort of thing.


---
**3D Laser** *July 30, 2016 16:02*

Yea [miniduels.com](http://miniduels.com) stole my tray design.  They made the token section standing up rather than laying down.  Other than that it's the same tray


---
**3D Laser** *July 30, 2016 16:46*

**+Yuusuf Sallahuddin**  I think this is what I angling to do modify it with magnets 


---
**Roberto Fernandez** *July 30, 2016 16:49*

Drawing a water mark when you post your photos or designs.


---
**Jim Hatch** *July 30, 2016 18:39*

You can send a cease & desist letter threatening legal action if he doesn't comply but unless you're willing to spend $$$ on legal action, not much else.



Once you post anything online it's copyable. Once you sell anything or give it away for that matter it's copyable.



Patents & copyrights only protect those with either the resources to litigate or where the value of the thing is high enough to interest a lawyer for a contingency arrangement.



You can also file a notice with Customs to try to prevent him exporting it as they may stop his customers from being able to import it.



Bottom line, you just need to keep improving it and out him on your website if you have one. (Be prepared if you do that he'll complain & may get you litigated for slander.)


---
**Bruce Garoutte** *July 30, 2016 21:26*

You may be able to copyright your creations, which is small $$ v.s. patents which are big $$, then you would have a legal leg to stand on. Still like Greg said, if they change it by more than 10% you may be out of luck anyway. Still, it is a shame that some folks have to stoop that low.


---
**Jim Hatch** *July 30, 2016 23:58*

+Bruce Garoutte is right but the legal protection isn't worth anything if you can't follow it up with action. That will cost dollars and there's no guarantee you'll get anything other than enforcement of the cease & desist. Copyrights are cheap but getting money from violations is hard (even though a registered copyright has statutory protections where damages are not needed to determine an award). First you've got to get a lawyer who will take the case (at $$$/hr) then you make motions and they make counter motions and on and on usually for 3-5 years. The bills rack up and unless there's some significant value in it and the miscreant has resources you can take, it's going to be fairly useless. So you get a judgement of hundreds of thousands of dollars against the guy - if he doesn't have it, you've got nothing but the paper the judgement is written on. 



I had a giant toy store (they have a giraffe) using my software. All of the software and manuals and documentation was copyrighted. They decided they didn't want to pay me license & maintenance anymore so they sent out an RFP for someone to build a replacement system. And they attached my system's documentation.



We sued. Ironclad, open & shut case. In the end they asked everyone they sent the RFP package to send it back to them and destroy any copies. They admitted no guilt - just a "mistake" and we got nothing. They were willing to spend years delaying things in the hope we'd go away because they had big big pockets and we didn't. Guess who won? Are you able to incur years of legal bills in the hope of prevailing? Not many are.


---
**dstevens lv** *July 31, 2016 00:42*

That tray design isn't likely protected by US copyright and even with the means to patent the design it likely wouldn't qualify.  



The copyright/patent/trademark issue is one that still isn't well understood by many in the maker community.   More info is here  [http://www.uspto.gov/trademarks-getting-started/trademark-basics/trademark-patent-or-copyright](http://www.uspto.gov/trademarks-getting-started/trademark-basics/trademark-patent-or-copyright)





Jim's manual case is different than what you have. By law when Jim writes a manual, a copyright is automatically attached.  That's not the case in the design of a functional element, like a game tray.  If your tray has artistic stylings that differentiate it from others, it might be eligible for copyright protection.  In the examples on the site I didn't see anything that looked like it would qualify as a work of art.  It's very likely the unfortunately you have no legal basis for a case.




---
**Jim Hatch** *July 31, 2016 01:15*

**+dstevens lv**​ I would expect the best he could do is get a "Design Patent" which isn't awfully expensive but has the same issues of defense. One big difference between it and copyright law is the availability of statutory damages for registered copyrights. Although all written work is copyrighted on creation, it's the registration ($25 last I looked) that gets you statutory damages. Those are defined by law and not by having to prove the damages.



If the tray had text and/or graphics that were copied then he'd have a copyright case.



All in all though it's meaningless. It's not worth enough to pursue.


---
**HalfNormal** *July 31, 2016 01:21*

**+Corey Budwine** A few things I would do. Sell it cheaper if you can. It only has to be pennies but people will go with a few cents difference. Free shipping. Discount for sending business your way. The best is have the greatest customer service. Free replacement if broken. Answer your customers as quickly as possible. Alway be around where they use the product. Be seen and available! All you need are a few very satisfied customers to back you over the competition. Beat them at their own game.


---
**3D Laser** *July 31, 2016 01:29*

**+HalfNormal**  yea I have to go back to the board right now they are selling there's six dollars less than mine granted I am using ply vs mdf and have options for staining and painting but I think I will have to lower my profit margin so I don't loose sales which stinks but you got to do what you got to do.  As much as their trays look like mine they are still different enough.  Plus when you boil it down it really is just a piece of wood with shapes cut out kinda hard to copyright that 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/e9fryNorQFB) &mdash; content and formatting may not be reliable*
