---
layout: post
title: "Just got my K40 working with my SBASE board and Visicut"
date: November 24, 2016 21:39
category: "Software"
author: "ab1244"
---
Just got my K40 working with my SBASE board and Visicut. So far, I'm pretty happy with it. 



I noticed Visicam and it looks like a nice option to simplify use.



Not many mentions of it on this board, any experience?





**"ab1244"**

---
---
**Stephane Buisson** *November 24, 2016 21:56*

the Sbase is a chinese clone of smoothie, not contributing to smoothie project. this is why it isn't recommended.

Visicam is of course a very nice feature.


---
**HalfNormal** *November 24, 2016 23:25*

**+ab1244** If you do a search of this forum, you will find some people have had problems with the official smoothieboard firmware running on this board. Be careful what you load on it.


---
**Jon Bruno** *November 29, 2016 15:52*

I have removed my SBASE  board and reinstalled the original controller if that's any indication of how difficult they are to get working properly. I will be looking to get one of **+Ray Kholodovsky**'s Cohesion3D Mini Laser Upgrade Bundle setups. His drop in design and plug and play is so very appealing to me.. Basically it's the answer I've been waiting for and I'm sick of troubleshooting this junk knock-off board. 

I've been hacking at this machine for too long now.. I'm done. 

RAMPS, SBASE.. enough already!




---
**ab1244** *November 29, 2016 18:00*

I understand the concerns, but once I understood the concept behind setting it up, it was very straightforward. Maybe I'll make a post about my set up. Looks to be one less wire involved that the official Smoothieboard. I'm running the latest Smoothie firmware without problem so far. 



The caveat is that so far I've only done limited testing, but it works as expected with Visicut.



I'll probably buy a Smoothieboard for my next project, just to support the community for but those who are resource limited, the SBASE seems a decent solution so far. 


---
**Jon Bruno** *November 29, 2016 18:06*

That's fine..

But you'll soon see its really not that cost effective when you add in the hours spent troubleshooting and then of course it's eventual replacement.



Build once.



I'm already in the hole for a RAMPS setup and then this turd of a controller. I should have waited it out and gotten a properly supported board.


---
**chris B** *January 13, 2017 06:41*

**+ab1244** I was hoping you could help me with a wiring diagram for hooking up a mks sbase to a k40 and with a config file if you can as I have a spare sbase controller and I'd like to upgrade my k40


---
*Imported from [Google+](https://plus.google.com/111075041458030923509/posts/LnD1pNC8QJn) &mdash; content and formatting may not be reliable*
