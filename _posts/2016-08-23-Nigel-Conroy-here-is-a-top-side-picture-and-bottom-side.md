---
layout: post
title: "Nigel Conroy here is a top side picture and bottom side"
date: August 23, 2016 22:11
category: "Modification"
author: "Robi Akerley-McKee"
---
**+Nigel Conroy** here is a top side picture and bottom side.



![images/553b2ef4c3a2ae0f4dc114c0189662ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/553b2ef4c3a2ae0f4dc114c0189662ce.jpeg)
![images/0a6474cabbfb49b79faaa27a0a48b70e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a6474cabbfb49b79faaa27a0a48b70e.jpeg)

**"Robi Akerley-McKee"**

---
---
**Nigel Conroy** *August 23, 2016 22:53*

**+Robi Akerley-McKee** That looks great. What a great solution. Any more pictures of the opposite end and the switch on the lid would be much appreciated. 



Our students are back on Thursday and when they hear of this new addition they're going to be pumped. They won't be touching it for a while so I have a bit of time to get these additional safety features added. 

Thanks again.


---
**Robi Akerley-McKee** *August 24, 2016 01:09*

I'll be taking pictures tomorrow at the [http://creatorspace.us](http://creatorspace.us)


---
*Imported from [Google+](https://plus.google.com/103866996421752978311/posts/9oaG1VqAM29) &mdash; content and formatting may not be reliable*
