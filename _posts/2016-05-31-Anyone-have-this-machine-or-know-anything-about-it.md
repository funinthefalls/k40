---
layout: post
title: "Anyone have this machine, or know anything about it?"
date: May 31, 2016 00:38
category: "Discussion"
author: "Brandon Smith"
---
Anyone have this machine, or know anything about it? It has a lot of nice specs and features. I imagine it is of similar quality. I am considering some different projects/endeavors and the larger size will be needed. The additional features are just bonuses. Currently selling for $1899, I would see if I could get a better price if I pick it up in person.





**"Brandon Smith"**

---
---
**Brandon Smith** *May 31, 2016 01:22*

I was also considering this one on eBay. A little bit shorter in the y axis, but I think it would still serve my needs.



[http://www.ebay.com/itm/50W-Engraving-Cutting-CO2-Laser-USB-Machine-Engraver-Cutter-/221783666951?hash=item33a3563107:g:6J4AAOSwFqJWj36S](http://www.ebay.com/itm/50W-Engraving-Cutting-CO2-Laser-USB-Machine-Engraver-Cutter-/221783666951?hash=item33a3563107:g:6J4AAOSwFqJWj36S)


---
**Derek Schuetz** *May 31, 2016 01:49*

The one in the Craigslist ad is an old model


---
**Brandon Smith** *May 31, 2016 01:51*

Good to know, thanks. I was leaning towards the eBay one. Better price, same features.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 31, 2016 05:48*

**+Brandon Smith** I think Scott Thorne has a 50W model. Not sure if it's the same as this though.


---
**Stephane Buisson** *May 31, 2016 07:26*

3 sizes on the same ebay link for comparison

become a very big machine (box) very quickly.

[http://www.ebay.co.uk/itm/40W-50W-60W-CO2-USB-Laser-Cutter-Engraver-Laser-Cutting-Engraving-Machine-220V-/301949881928?var=&hash=item464d9d8e48:m:mKK-Xy4mnYxFBJsP3fK_AIA](http://www.ebay.co.uk/itm/40W-50W-60W-CO2-USB-Laser-Cutter-Engraver-Laser-Cutting-Engraving-Machine-220V-/301949881928?var=&hash=item464d9d8e48:m:mKK-Xy4mnYxFBJsP3fK_AIA)


---
*Imported from [Google+](https://plus.google.com/109949255412543943799/posts/RFaoMYEpKpy) &mdash; content and formatting may not be reliable*
