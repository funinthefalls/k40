---
layout: post
title: "I've done a bit of searching on here but can't seem to find these answers"
date: November 08, 2017 13:16
category: "Modification"
author: "Bill"
---
I've done a bit of searching on here but can't seem to find these answers. Is there a working and available upgrade on the market that will allow use of mac computer and inkscape with the k40? If there is, which k40 version works best with the upgrade (potentiometer vs digital.) Thanks!





**"Bill"**

---
---
**Michael Audette** *November 08, 2017 14:13*

I know GRBL Works.   I drive mine with LaserWeb4 using Inkscape SVGs. As for 100% drop-in you might need a "middleman" type board with most of them - not sure if there is a 100% drop-in.  I'm currently running GRBL 1.1 on Arduino with a CNCShield 3.0 and a custom "middleman" board to connect to the ribbon cable in the laser.  I'm pretty sure a couple of the LaserWeb4 devs use mac's as well.


---
**Don Kleinschnitz Jr.** *November 08, 2017 14:36*

Look at Cohesion3D should drop in and pretty sure you can run GRBL. 

**+Ray Kholodovsky**



The opinion on the digital vs pot control/meter varies. Many have converted. The meter tells you the actual current to the tube which is invaluable.


---
**Anthony Bolgar** *November 08, 2017 17:54*

You can run GRBL-LPC on the Cohesion board. Its a 32bit version of grbl.


---
**Bill** *November 08, 2017 20:27*

Thanks **+Michael Audette** **+Don Kleinschnitz** **+Anthony Bolgar** for your speedy replies! I've spent a couple hours looking up the above suggestions and from my understanding: GRBL is the firmware that could go on a Cohesion3d board, that can receive g-code from Laserweb4 on a mac computer, I could move my designs from inkscape to laswerweb4, and I should get a K40 with a meter on it?


---
**Anthony Bolgar** *November 08, 2017 21:28*

I prefer the meter because you get an accurate reading, the digital panel is just a percentage of total power which is much less accurate. As well, there will be a new software package being released this month (it is in Beta right now) that will work with GRBL called LightBurn (You can get a preview of the software by searching fore lightburn on facebook). But LW4 is a great package itself, and is actively being maintained/developed.


---
**Paul de Groot** *November 09, 2017 05:46*

William my solution works with a Mac and Inkscape and Laserweb. See [awesome.tech](http://awesome.tech) 


---
**Paul de Groot** *November 09, 2017 05:46*

[awesome.tech - Inspiring science for hungry minds](http://awesome.tech)


---
**Anthony Bolgar** *November 09, 2017 07:11*

Yup, I have one of Paul's boards in a K40, was a super simple install, and the photo engraving capabilities of his board are excellent.


---
**Bill** *November 09, 2017 09:51*

**+Paul de Groot** i did send an email about a week ago and left a message for you on fb a couple days ago for more info on your product. Is it currently available?


---
**Paul de Groot** *November 09, 2017 10:06*

Hi William I have not checked FB for ages... yes I have ordered a few more boards after repeated requests from fans. Tonight I will add a page with the instructions "setting up Gerbil". Was a massive amount of work and will probably have a few errors in it but will keep updating it.


---
*Imported from [Google+](https://plus.google.com/101833370360315105469/posts/UXkkpZEqkX7) &mdash; content and formatting may not be reliable*
