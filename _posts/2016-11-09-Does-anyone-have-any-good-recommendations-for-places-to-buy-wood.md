---
layout: post
title: "Does anyone have any good recommendations for places to buy wood?"
date: November 09, 2016 00:30
category: "Discussion"
author: "Gage Toschlog"
---
Does anyone have any good recommendations for places to buy wood? Looking for 1/8" or thicker





**"Gage Toschlog"**

---
---
**Ashley M. Kirchner [Norym]** *November 09, 2016 00:57*

Can only speak for the US: Ocooch Hardwoods.


---
**Tony Sobczak** *November 09, 2016 01:06*

1/8 or 1/16 mdf source? 


---
**greg greene** *November 09, 2016 02:42*

Every box box DIY store I know carries 5 foot by foot 1/8 baltic birch you can cut into any size you need - real cheap


---
*Imported from [Google+](https://plus.google.com/105155515364049131807/posts/RXDo5ZvUeSY) &mdash; content and formatting may not be reliable*
