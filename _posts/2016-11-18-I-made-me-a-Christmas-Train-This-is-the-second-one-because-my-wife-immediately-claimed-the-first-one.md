---
layout: post
title: "I made me a Christmas Train. This is the second one because my wife immediately claimed the first one"
date: November 18, 2016 23:10
category: "Object produced with laser"
author: "Jeff Johnson"
---
I made me a Christmas Train. This is the second one because my wife immediately claimed the first one. This took about 20 hours for me to design but only a couple to assemble. I'm so terrible at written instructions so I think I need to start making assembly videos.  The files are here [http://www.thingiverse.com/thing:1902374](http://www.thingiverse.com/thing:1902374) 

![images/9c690680813be1e2c68bfcf974bd07fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9c690680813be1e2c68bfcf974bd07fd.jpeg)



**"Jeff Johnson"**

---
---
**Jim Hatch** *November 18, 2016 23:21*

Outstanding job! 


---
**Anthony Bolgar** *November 19, 2016 00:00*

Thanks for sharing the files!


---
**giavonni palombo** *November 19, 2016 00:15*

That great! Love it.


---
**Jérémie Tarot** *November 19, 2016 01:14*

I case I wasn't already sure I need to get myself a laser cutter before my son makes its first steps... 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 19, 2016 05:24*

This is really cool :) Nice job.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/3BYaNrZ8HDJ) &mdash; content and formatting may not be reliable*
