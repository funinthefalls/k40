---
layout: post
title: "Halloween Tiger (with Man Bun)"
date: October 07, 2017 14:38
category: "Object produced with laser"
author: "Nate Caine"
---
<b>Halloween Tiger</b>

<i>(with Man Bun)</i>

![images/86ec64d969fbf15ac0dc547dc4d36bde.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/86ec64d969fbf15ac0dc547dc4d36bde.jpeg)



**"Nate Caine"**

---
---
**BEN 3D** *October 07, 2017 14:51*

cool


---
**Don Kleinschnitz Jr.** *October 07, 2017 19:05*

That fit in the machine :)?


---
*Imported from [Google+](https://plus.google.com/107379565262839168986/posts/Wob6t9PBkix) &mdash; content and formatting may not be reliable*
