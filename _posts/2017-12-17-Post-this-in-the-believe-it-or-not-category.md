---
layout: post
title: "Post this in the believe it or not category!"
date: December 17, 2017 02:41
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Post this in the believe it or not category! You can etch stainless steel using mustard!

Plenty of youtube videos but here is a statement from a reputable source.



[https://darklylabs.zendesk.com/hc/en-us/community/posts/115005973627-Marking-stainless-steel-with-lasers-and-mustard-Seriously-](https://darklylabs.zendesk.com/hc/en-us/community/posts/115005973627-Marking-stainless-steel-with-lasers-and-mustard-Seriously-)





**"HalfNormal"**

---
---
**greg greene** *December 17, 2017 02:54*

I say - do you have any Grey Poupon?


---
**Ned Hill** *December 17, 2017 03:32*

Interesting.  He's using a 5W blue laser so I'm not sure how well it would work with a higher power CO2 laser since they have different wavelengths.  If it is the isothiocyanates, as he speculates, you could always try mustard oil which is distilled allyl isothiocyanate from mustard seed.  May need to add some vinegar to acidify it some.  Probably forming a metal sulphide on the surface. 


---
**HalfNormal** *December 17, 2017 03:37*

**+Ned Hill**​ I found this on another CO2 laser forum. There were a few proof of concept pics.


---
**Gavin Dow** *December 20, 2017 19:47*

Stainless steel is about the only metal I CAN engrave/mark with my K40 without any sort of additives (dry moly, plaster of paris, etc.).


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/c8Xtk499Y5b) &mdash; content and formatting may not be reliable*
