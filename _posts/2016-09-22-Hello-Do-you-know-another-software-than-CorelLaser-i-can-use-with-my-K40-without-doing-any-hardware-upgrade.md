---
layout: post
title: "Hello, Do you know another software than CorelLaser i can use with my K40 without doing any hardware upgrade?"
date: September 22, 2016 13:09
category: "Software"
author: "Mikkel Steffensen"
---
Hello,



Do you know another software than CorelLaser i can use with my K40 without doing any hardware upgrade?





**"Mikkel Steffensen"**

---
---
**Jim Hatch** *September 22, 2016 13:27*

There isn't another that will do a direct "print" to the K40. You can use any design software and then export the file and open it in LaserDRW to send to the laser.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 22, 2016 14:01*

Unfortunately, as Jim mentions, the stock hardware communicates with the CorelLaser (or LaserDRW) using proprietary methods which means there is no option for other software for "sending to the laser" unless you upgrade the controller.



Fortunately, you can skip the CorelDraw or LaserDRW for design & utilise whatever software you are most comfortable in (e.g. for me it is Illustrator or Photoshop). Although, some file formats are not supported in the CorelDraw 12 that shipped with my K40, so you have to get around that by either using older file formats (e.g. AI v9 rather than the CC2016 version) or upgrade the CorelDraw component to a newer version of CorelDraw (I tried x5 & x7 with success with the CorelLaser plugin).


---
**Jim Hatch** *September 22, 2016 14:03*

I use Corel X8 with the plugin.


---
**Mikkel Steffensen** *September 22, 2016 14:39*

Okay. I have tried a laser in a fablab. They use a software called LaserCut 5.3. Can anyone tell me witch control board i need to use?




---
**Don Kleinschnitz Jr.** *September 22, 2016 14:43*

Bite the bullet and go to a controller that understands Gcode ....I.E. Smoothie :)




---
**Vince Lee** *September 22, 2016 14:47*

Note that changing controller boards has some other advantages but doesn't necessarily change this aspect much.  I use visicut with my smoothieboard but it is a control program not a drawing program so I still have to draw in inkscape and export files to visicut to engrave.  I've used the same process with inkscape and the native software.  Visicut has an inkscape export plugin to make the exporting take less steps, but i have found it kind of buggy, as it keeps opening new instances of visicut on every export. Thus, since I have a switchable adapter and despite having a smoothieboard, I still use moshidraw with the old board for most jobs since the workflow is faster.


---
**Ariel Yahni (UniKpty)** *September 22, 2016 15:01*

**+Vince Lee**​ what's the reason you don't use LaserWeb if I may ask? 


---
*Imported from [Google+](https://plus.google.com/117825343986845004843/posts/PMqw5sLM6zx) &mdash; content and formatting may not be reliable*
