---
layout: post
title: "Hello, I have just bought a K40 laser machine, and it has problems"
date: June 09, 2017 21:14
category: "Hardware and Laser settings"
author: "David Marcos"
---
Hello,



I have just bought a K40 laser machine, and it has problems. 



First problem is that if I set the current below 5 mA the PSU makes a high pitched sound. If I go above, it disappears. I does lase while making that noise.



Second problem is that it looks terribly underpowered. Setting the current to 12-15 mA I can't cut 3mm acrylic in one pass at 20mm/s , and it's very very unfocused. I get a 1mm deep trench and the cut is almost 1mm wide. Any clues?



By the way, I did align the laser path.





**"David Marcos"**

---
---
**Stephane Buisson** *June 09, 2017 21:33*

check if the lens isn't up side down in the holder (a classic) , large kerf clue would suggest it.

also check if you are at focal point distance ? 


---
**David Marcos** *June 09, 2017 22:48*

I don't think it's upside down, just unfocused. How do I find the focal point? I've tried cutting material above the holder and also clamped by the holder and I get the exact same result, a very wide (1mm-ish) trace and not too much penetration in the material.



Also, any clue about the whining noise while at low current? I've heard lots of stuff about arcing and such and I'm not sure how to check for it.


---
**Mark Brown** *June 09, 2017 23:48*

What are you using for coolant?  Mine made a whining noise at low ma when I was running tap water in it. Distilled (or reverse osmosis) is best/necessary. 



Double check the lens orientation first, it should be bump up, flat down. You should be able to see your reflection only in the bump side. 



As for focal length you can put a piece in the clamp at an angle so it slopes from 25mm-ish below the table to 25mm above. Then cut a line across it and measure the distance at the point where it cut the best.  This is commonly referred to as a ramp test. 


---
**Phillip Conroy** *June 10, 2017 06:12*

20mm/second is way to fast for acrylic, try 5 mm/ sec and 15ma


---
**David Marcos** *June 10, 2017 08:17*

**+Mark Brown** I am using distilled water, 7.5 liters of it. But to be fair, I didn't flush the piping and it might be contaminated. Now that you say it, the tubes had some water residue inside them and I just assumed it was distilled water from manufacturer testing. I'm going to swap the water and see what happens


---
**David Marcos** *June 10, 2017 08:28*

Also, do you have a guide for cutting speeds and materials? And I'm also concerned about the smoke extractor. I have sealed all around it with aluminium tape and also sealed the hose to fan union with tape, but it still doesn't evacuate the smoke properly. I see the smokes just rising and filling the whole machine and I can smell the disaster. I have seen people using from high CFM computer fans with adapters to people using 1000W high power blowers. Haven't found any comparative results guide, just people saying that a couple 12k RPM computer fans were enough for "absolutely no smoke" while others saying you absolutely need a jet engine and you will still smell it. My machine is 50cm away from a window and I dont want to make too much noise, and I want to have as little smoke inside as possible. I have a 3D printer to print adapter parts.


---
**Stephane Buisson** *June 10, 2017 10:39*

focus point it's one spec of the lens, the K40 come with 50.8 mm one. Place your material the way, the focal point is located in the middle of your stock. the ramp test is a way to find/confirm it.

the kerf shouldn't exceed 0.2 mm.

try to flip your lens (it cost nothing, and you can revert it later), but can save you time and exasperation.

Smoke could seriously affect laser power efficiency, look for air assist (remove smoke on laser path). smoke leave a layer on your mirrors, you should clean them regularly.

hope it's helping.


---
**David Marcos** *June 10, 2017 11:11*

**+Stephane Buisson** yes it's helping. You were 100% right, the lense was upside down and dirty. How much air flow/pressure is required for proper air assist? I have a very silent compressor but I don't know if it would work. The specs say 90l/minute at 160 mbar.

I need to solve the smoke issue before further playing with the machine. I have it in an apartment (spare working room, 50cm away from a window with no neighboring windows and in the last floor so I vent near the roof, and proper ground instalation (I checked)) and I still thought I was borderline, and now I think it was a horrible idea to buy the machine...


---
**Stephane Buisson** *June 10, 2017 11:17*

smoke: it's a hole at the bottom of the k40, it's a intake. is it free ? if not it's no point to increase power of blower or fan.


---
**David Marcos** *June 10, 2017 11:20*

**+Stephane Buisson** what do you mean by free? The machine sits on a table, the hole is opened and above the table, there's about 1cm clearance from hole to table. Is that enough or do I have to drill the table? There's also another hole in the right side


---
**Stephane Buisson** *June 10, 2017 11:29*

should be fine, but you can easily put something under the feet to see if it make a difference. my point was airflow is a input/output matter, as well as path/circulation. cutting/shortening the outtake could also help.


---
**David Marcos** *June 10, 2017 11:33*

**+Stephane Buisson** will try. My exhaust performance now is essentially zero. I can see the smoke rising uncontrolled inside the machine and not being ducted at all through the exhaust intake. I can feel a little bit of air flow at the tube exhaust and the fan's anti-return flap does open. 


---
**David Marcos** *June 10, 2017 12:48*

**+Mark Brown** I made a measurement of how good my water is right. I did it in an insultingly wrong way but I think I made a significant discovery. I just cleaned the tips of my multimeter, taped them together at a certain distance (random) and stuck them inside the water container to a certain depth while measuring in the megaohm range, I anotated the measurement. Then I put my contaminated distilled water back in the original containers and filled my water tank with tap water, and repeated inserting to the same depth and anotating the result. The number I got from tap water was 3 times larger than the one I got from my contaminated distilled water. I live in a place in which tap water is famous for its purity (nobody uses salt or anti-lime agents in appliances here) and seems like my contaminated distilled water is worse. Looks like 7.5l of <0.5uS/cm tap water + a few ml of piss-colored water residue inside the tube piping = something worse than tap water. Lesson learned, always flush the piping thoroughly before pumping in the equivalnt of $10 in water...


---
*Imported from [Google+](https://plus.google.com/100401114750200121359/posts/XCY2vSD5MWE) &mdash; content and formatting may not be reliable*
