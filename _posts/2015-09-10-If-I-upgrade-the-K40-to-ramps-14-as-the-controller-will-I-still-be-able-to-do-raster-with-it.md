---
layout: post
title: "If I upgrade the K40 to ramps 1.4 as the controller, will I still be able to do raster with it?"
date: September 10, 2015 00:09
category: "Discussion"
author: "Anthony Bolgar"
---
If I upgrade the K40 to ramps 1.4 as the controller, will I still be able to do raster with it?





**"Anthony Bolgar"**

---
---
**Sean Cherven** *September 13, 2015 11:55*

**+quillford**, Could you share the pictures and video? I'd like to see.


---
**Anthony Bolgar** *September 13, 2015 16:15*

Here is a good link to the upgrade process:

[http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/256422-cnc-3.html](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/256422-cnc-3.html)


---
**Anthony Bolgar** *September 14, 2015 10:09*

Just wondering if I can make this work with ramps-fd and an arduino due , that way it would be running on a 32 bit processor


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/6LaX163mvtH) &mdash; content and formatting may not be reliable*
