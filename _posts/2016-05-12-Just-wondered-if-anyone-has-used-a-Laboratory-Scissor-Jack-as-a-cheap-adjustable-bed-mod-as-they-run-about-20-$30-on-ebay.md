---
layout: post
title: "Just wondered if anyone has used a Laboratory Scissor Jack as a cheap adjustable bed mod (as they run about 20/$30 on ebay)?"
date: May 12, 2016 12:39
category: "Modification"
author: "Pigeon FX"
---
Just wondered if anyone has used a Laboratory Scissor Jack as a cheap adjustable bed mod (as they run about £20/$30 on ebay)?





![images/e11b29ad63236bc54b5499cbb4444af5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e11b29ad63236bc54b5499cbb4444af5.jpeg)



**"Pigeon FX"**

---
---
**Stephane Buisson** *May 12, 2016 13:11*

some did,

What problem are you you trying to solve ?

the K40 is fixed focal lens machine, the real + would be to create an upper stop celling. the best solution would be to squeeze up the material to the 50.8 mm up limit. (the limit would be slightly movable depending on cutting thickness). but why not something like that for squeezing up. Easy mechanic, but not very good as bed. I would prefer 4 springs in each corner.


---
**Pigeon FX** *May 12, 2016 13:27*

**+Stephane Buisson** The problem I'm trying to solve is being able to switch from etching and cutting acrylic/wood laminates (in themselves not a problem as my cutter head is adjustable by about 1/2", so its easy to adjust from say 0.8mm ply to 3mm Acrylic by moving just the head).



But, being able to drop the bed down to engrave on project enclosure boxes (I have had really good results engraving on powder coated aluminum boxes, etching the paint right off to bare aluminum) and being able to drop the honeycomb bed down a good couple of inches would be very handy, and looking to do it a little more refined then just wood shims between the base of the machine and the bottom of the bed. .


---
**Stephane Buisson** *May 12, 2016 13:40*

by "problem" i was more thinking, solving one (introducing larger object like box is a +), but would create another one which is to be able to find the focal point. that's why I am talking of upper stop celling, as an idea to develop. (comments welcome)


---
**3D Laser** *May 12, 2016 14:16*

Yep that is what I used.  It works great you will have to take one of the scissors pieces out to give you more depth but for 45 dollars vs 200 dollars it was worth it


---
**Pigeon FX** *May 12, 2016 14:43*

**+Stephane Buisson** To find the focal length right now Im using a small 10.8mm block of wood between the work and the end of my Air Assist Nozzle like a feeler gauge.



I know the lens is 40mmm from the tip, so with a 50.8mm focal length: 50.8 - 40 = 10.8....had planed on cutting some blocks for 1.5/ 3/ 6mm sheets too, as I think I'm right in thinking I want the laser focused in the center of the materiel. (I have only had the laser a few days so really wining it her!) 



But, I do like the upper stop ceiling idea! 


---
**Pigeon FX** *May 12, 2016 14:49*

**+Corey Budwine** Good to hear it working out for you. I just ordered a 100x100mm one to play with, and see if I can make it a little more rigid with mounting a larger size plate on the top and bottom with some rods in each corner on some bearings (as I have some 3D printer parts here, I may as well use!). 


---
**Brandon Smith** *May 12, 2016 15:12*

I am now :) Just ordered one for $22 on Amazon. Thanks for the tip!


---
**Stephane Buisson** *May 12, 2016 15:34*

focal distance an old post from this community :

[https://plus.google.com/101545054223690374040/posts/c54Zc7LcCQn](https://plus.google.com/101545054223690374040/posts/c54Zc7LcCQn)


---
**3D Laser** *May 12, 2016 16:49*

**+Pigeon FX** you might find that with the Jack all the way down its to high for the focal length mine was that big and I had to take it apart now I got a good 4 inches of play to hit that focal sweet sot


---
**Coherent** *May 12, 2016 18:13*

I got one a while back. You can unbolt to base and drill four holes and mount it to the bottom of the machine. Then remove the top platform and mount your honeycomb or perforated bed to it. It works well and I can lower it enough to accommodate all but very thick  materials, but I imagine that would be the case with any platform.


---
**Greg Curtis (pSyONiDe)** *May 12, 2016 18:24*

I got one of these, has about an inch worth of focus range



Laser Head for Engraver Cutter Cutting Machine [https://www.amazon.com/dp/B00SO64S5K/ref=cm_sw_r_cp_apa_gHmnxbB0YYD6Q](https://www.amazon.com/dp/B00SO64S5K/ref=cm_sw_r_cp_apa_gHmnxbB0YYD6Q)



Then made this, as I have exactly 10mm between the tip and the surface for focal length



Calibration widget [https://imgur.com/gallery/h2gKt](https://imgur.com/gallery/h2gKt)



[https://imgur.com/6UhIQnF](https://imgur.com/6UhIQnF)


---
**Anthony Bolgar** *May 12, 2016 23:20*

I bought one to put under my rotary attachment. I will be cutting a hole in the bottom of the case and recessing the labjack and rotary so that I will have room for a decent diameter that I can engrave on. Without the mod, I can only do about 3" diameter


---
**Greg Curtis (pSyONiDe)** *May 13, 2016 01:19*

That's a good idea. I haven't gotten the rotary yet.


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/ig44fe7mS5H) &mdash; content and formatting may not be reliable*
