---
layout: post
title: "My M2 Nano board for CorelLaser is going senile about a week after I bought the laser - Seller wants the whole thing sent back, which is ridiculous considering the board is about the size of a small letter"
date: March 11, 2016 01:00
category: "Discussion"
author: "Heath Young"
---
My M2 Nano board for CorelLaser is going senile about a week after I bought the laser - Seller wants the whole thing sent back, which is ridiculous considering the board is about the size of a small letter.



What is happening is that the laser, instead of homing to the left top corner, it homes into the middle, and then when you open corellaser, it sometimes decides to move again, sometimes to the end of its travel and the belts get chewed up. I've checked out the limit switches with a multimeter on the terminals and they seem to change state as expected.



Its also done something strange to how the test button works as well - you used to have to press both buttons, now the test one works without the other.



It also hangs in the middle of an engraving session, and sometimes fails to move the steppers at all.



Anyone have an M2 Lihuiyu Studio 6C6879-LASER-M2 board that they removed when upgrading to a RAMPS or DSP? I don't mind coreldraw (I actually bought a legitimate copy to use with the laser). 





**"Heath Young"**

---
---
**timb12957** *January 24, 2017 12:24*

Were you able to resolve this issue? My K40 is doing almost the exact same thing.


---
*Imported from [Google+](https://plus.google.com/102957946867870877048/posts/F79yV6tKdwf) &mdash; content and formatting may not be reliable*
