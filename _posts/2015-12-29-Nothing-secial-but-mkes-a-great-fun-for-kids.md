---
layout: post
title: "Nothing secial, but mkes a great fun for kids"
date: December 29, 2015 16:06
category: "Repository and designs"
author: "Tadas Mikalauskas"
---
Nothing secial, but mkes a great fun for kids. Just cut it from the thinnest translucent plastic you have (I used 1.5mm plexiglass but that is too thick) and glue the together into pyramid. Then go into dark and find a a hologramic video on youtube on your phone. Very quick and nice attraction :)

![images/132944be4944a56e6f517bf5f0982f29.png](https://gitlab.com/funinthefalls/k40/raw/master/images/132944be4944a56e6f517bf5f0982f29.png)



**"Tadas Mikalauskas"**

---
---
**Coherent** *December 29, 2015 19:09*

I'm intrigued by your post.  I'd like to see more information or photos on your concept... I've never seen a holographic video so will have to do some research. It sounds like a cool idea though.


---
**Tadas Mikalauskas** *December 29, 2015 19:45*

I'll post something more having a spare moment :)


---
**Scott Marshall** *December 29, 2015 23:01*

+1 here, Interesting!


---
**andmif** *December 30, 2015 00:30*

i think  it's similar to this able: [http://www.instructables.com/id/Amazing-3D-Projection-Pyramid-in-10-min-from-Clear/](http://www.instructables.com/id/Amazing-3D-Projection-Pyramid-in-10-min-from-Clear/)


---
**Scott Marshall** *December 30, 2015 01:54*

Cool! Not what I had envisioned. I was expecting a 3D split image of some sort where you put the pyramid opening to the screen. I see how it works though.



Thanks Tadas


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 30, 2015 02:19*

Nice work **+Tadas Mikalauskas**. When I saw these holographic videos recently I was thinking along the lines of what you have done too, to make a laser cut pyramid for it.

**+Coherent** It's actually pretty cool, check it out on youtube, 
{% include youtubePlayer.html id="7YWTtCsvgvg" %}
[https://www.youtube.com/watch?v=7YWTtCsvgvg](https://www.youtube.com/watch?v=7YWTtCsvgvg)


---
*Imported from [Google+](https://plus.google.com/113213933687083540785/posts/9YTYeePeWsM) &mdash; content and formatting may not be reliable*
