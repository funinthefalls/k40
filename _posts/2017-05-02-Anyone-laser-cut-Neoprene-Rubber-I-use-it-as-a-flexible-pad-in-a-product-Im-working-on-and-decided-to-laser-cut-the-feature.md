---
layout: post
title: "Anyone laser cut Neoprene Rubber? I use it as a flexible pad in a product Im working on and decided to laser cut the feature"
date: May 02, 2017 01:46
category: "Materials and settings"
author: "Steve Clark"
---
Anyone laser cut Neoprene Rubber? 



I use it as a flexible pad in a product I’m working on and decided to laser cut the feature. They are small 6.5mm x 8mm x .8mm thick and it has adhesive on one side with a plastic cover. 



Here are the settings… about 9ma with 15mm/sec goes through the rubber but mostly leaves the plastic so you can peel and stick off the original sheet. 

It is very sooty and likes to burn and put out quite a flame, so if you ever laser some be aware. I also suspect the flames don’t burn all the vapors that are flammable so use caution. The MSDS suggest that that might be the case and I am considering using an inert gas (as I have some) next time I cut it instead of air to avoid a flash back.



As my parts are small and the burn time is only about 4 to 5 sec. each the burn goes out each time it moves over. I used a acrylic mask to keep it flat.



This is the first time I've cut rubber of any sort so I'm interested in how other rubber materials cut.    





![images/f510ec820e16fdeba8512d48cb5b587e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f510ec820e16fdeba8512d48cb5b587e.jpeg)
![images/382b7cba3a67e0a2e62ccd0246dd6a9f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/382b7cba3a67e0a2e62ccd0246dd6a9f.jpeg)

**"Steve Clark"**

---
---
**Gee Willikers** *May 02, 2017 01:55*

Made with laserable rubber last week.

![images/1fd327be1df60540dd0d9ac7cd7dda66.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1fd327be1df60540dd0d9ac7cd7dda66.jpeg)


---
**Gee Willikers** *May 02, 2017 01:55*

![images/440048e1df4070d5ab8fe3ebcbd37356.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/440048e1df4070d5ab8fe3ebcbd37356.jpeg)


---
**Anthony Bolgar** *May 02, 2017 02:35*

Where did you get the laserable rubber **+Gee Willikers**?


---
**Gee Willikers** *May 02, 2017 02:41*

Ebay auction. It was a Laserbits sample kit that included 2 sheets of rubber and 8 pre-inked stampers. I don't think they offer it anymore. But the individual sheets are available on ebay.


---
**Anthony Bolgar** *May 02, 2017 03:56*

Thanks


---
**Jeff Johnson** *May 02, 2017 19:10*

I tried cutting some once. Once . . . 

It was a sooty mess.




---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/aGwpoeyCEAE) &mdash; content and formatting may not be reliable*
