---
layout: post
title: "Bringing in 2017 with a new panel design for my K40-S"
date: January 01, 2017 14:54
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Bringing in 2017 with a new panel design for my K40-S.



This is the design printed 1-1 from SU onto cardboard to test the fit before laser cutting and engraving.



And a logo to boot..... (any advice on how to cut a sticker for the logo?)







![images/095fc2343ff7f4f51dbe5cc2f477dc71.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/095fc2343ff7f4f51dbe5cc2f477dc71.jpeg)
![images/633abbb660bfdc2dcddd21273966296a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/633abbb660bfdc2dcddd21273966296a.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 01, 2017 16:29*

You could cut the sticker with a scalpel? Or craft knife? I'd imagine using the laser to cut it will melt the edges & be problematic for sticking.



Other options would be a vinyl plotter or something like that.


---
**Don Kleinschnitz Jr.** *January 01, 2017 16:43*

**+Yuusuf Sallahuddin** best I can tell the normal vinyl sticker material is PVC ... not cutting that. I will probably just go to a kiosk in the mall and have it cut on vinyl machine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 02, 2017 00:34*

**+Don Kleinschnitz** Yeah sounds like a good idea to take that route.


---
**Coherent** *January 03, 2017 14:07*

 You can print it on card stock, laminate it with clear self adhesive sheet.  If you glue it to a thin wood board or acrylic sheet (which you have cut with the laser) before you cut the paper/label panel holes, it's easy to run an exacto-blade around the holes for perfect cuts. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/XZqeenhVNwi) &mdash; content and formatting may not be reliable*
