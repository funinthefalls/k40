---
layout: post
title: "Here's what I've been doing with my laser cutter: - I've added internal water cooling loop"
date: March 18, 2015 17:53
category: "Modification"
author: "Sean Cherven"
---
Here's what I've been doing with my laser cutter:



- I've added internal water cooling loop.

- Replaced the exhaust fan with a better one, and routed the tubing to the window. 

- Designed new control panel for use with a Interlock System I am designing. (I'm waiting on the PCB to arrive.)

- Added Water Flow Sensor, Coolant & Tube Temp Sensors, Water Reservoir, 120mm Radiator, and lots more to come!



Let me know what you think, and if you suggest any changed or additions.



![images/6b7de6b944191dc4b836bb24976eab68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b7de6b944191dc4b836bb24976eab68.jpeg)
![images/7381c8fa5dc124beb457d1e28c75682f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7381c8fa5dc124beb457d1e28c75682f.jpeg)
![images/6ded2710deab154244b3d72ec174efa9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6ded2710deab154244b3d72ec174efa9.jpeg)
![images/07cc33594a213e9a5a7cf867cf32e4cb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07cc33594a213e9a5a7cf867cf32e4cb.jpeg)
![images/11226a4b27910f3ef41963c59f65a272.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/11226a4b27910f3ef41963c59f65a272.jpeg)
![images/6b8bdeb9eb37fd075f0fac64e1d6c923.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b8bdeb9eb37fd075f0fac64e1d6c923.jpeg)
![images/3b935ddcd9c0f3170338610ca037e517.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b935ddcd9c0f3170338610ca037e517.jpeg)
![images/f92fd72f6cbfe227eeb7fce16585f1e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f92fd72f6cbfe227eeb7fce16585f1e4.jpeg)
![images/b7f0fa4de19992a28796b56dbf7f1e6c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7f0fa4de19992a28796b56dbf7f1e6c.jpeg)
![images/9d65602d4f3144818734b97c390ea655.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9d65602d4f3144818734b97c390ea655.jpeg)
![images/b932d3301ecb65f6775003b0c6501d9e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b932d3301ecb65f6775003b0c6501d9e.jpeg)
![images/10c3516436f5dd933706c480840a7e94.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/10c3516436f5dd933706c480840a7e94.jpeg)

**"Sean Cherven"**

---
---
**Jim Coogan** *March 18, 2015 18:09*

Way ahead of me.  I am in the process of realigning everything inside the box.  Most of the changes are way above what I know how to do.  I still need to figure out how to add a laser pointer :-)  I need one of those how to guides to do most of that.  Great job though.  Those are good changes.  And all ones I should make too.


---
**Sean Cherven** *March 18, 2015 23:29*

Yeah, I'm just good with electronics. I can do PCB design and stuff too, plus I have a 3D Printer, which I used to make some parts for the laser cutter.


---
**Bill Parker** *March 19, 2015 17:49*

Nice job Sean I have been an industrial electrician all my life and have had thoughts about using a Siemens logo on mine. I am interested in what flow switch your using for water and have you got one on extract air flow?.


---
**Sean Cherven** *March 20, 2015 04:38*

Thanks Bill! I am using this flow sensor: 

[http://www.amazon.com/dp/B00HG7G2GG/](http://www.amazon.com/dp/B00HG7G2GG/)



And no, I don't have one installed for the exhaust air flow. I didn't really see the need for one, since the exhaust fan works exceptionally well! Plus I can hear it when it's running,



What exactly is a Siemens Logo? I looked it up quickly, but still not sure what it is used for.


---
**Bill Parker** *March 22, 2015 15:35*

Hi Sean it has inputs and outputs then you just drag items onto the work area say a timer and just click on inputs and outputs to set up link wires like input 1 goes to outpt 1 but set a delay timer so when input 1 goes off the timer would keep output 1 on for say10 minutes like a bathroom fan and light and you just click on the timer and panel opens to input your delay but lots of items inbuilt so you dont have to buy clocks timers and so on and no wiring of clocks timers relays and so on as it is all done internally aand you save your project software to program again anytime and also alter all the way your circuit works without adding anymore hardware or wiring. We have our ouside lighting working on one. Hope you understand my explanation they are a nice little unit.


---
**Mauro Manco (Exilaus)** *April 17, 2015 10:53*

Nice mod like too much your  new smoke collector. you Can share stl without we redisgn  it?

thx


---
**Sean Cherven** *August 15, 2015 00:44*

Sure, Here's the links:



[http://www.thingiverse.com/thing:703216](http://www.thingiverse.com/thing:703216)


---
**Sean Cherven** *August 15, 2015 00:45*

[http://www.thingiverse.com/thing:695226](http://www.thingiverse.com/thing:695226)


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/QbW931ighpF) &mdash; content and formatting may not be reliable*
