---
layout: post
title: "What are the major advantages of upgrading the controller on a K40 laser?"
date: September 13, 2016 13:52
category: "Modification"
author: "Ben Crawford"
---
What are the major advantages of upgrading the controller on a K40 laser? I know it lets your run Gcode but are there any other advantages? I get by just fine with the software that comes with the laser cutter. Just wondering what the advantages are and the cost to upgrade.





**"Ben Crawford"**

---
---
**Ben Crawford** *September 13, 2016 13:59*

what is grayscale control?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 13, 2016 19:30*

**+Ben Crawford** I would say advantages are a combination of things. 1) workflow is easier in LaserWeb (in my opinion); 2) can do shades of grey, which is much better for engraving photos that the stock software/controller.


---
**Dan Stuettgen** *September 13, 2016 23:57*

Another advsntage is that you can have you files on a thumbnail drive and dont have to have your computer connected to the laser cutter.  Also by pulling out the material holder and replacing it with a Z-table , you can increase your usable cutting area.  Plus upgrading to a DSP controller,  the software is easier to use as it is in english and you can import CAD dxf files directly into the LaserCad software.


---
**andy creighton** *September 14, 2016 01:37*

Fwiw, I have the factory card and use it to import cad dxf files through corel.  I have to play devil's advocate fir keeping the k40 stock. 


---
**Dan Stuettgen** *September 14, 2016 01:55*

Andy.  That is correct.  You can.  But if you dont have coreldraw and dont want to have to learn a new software program, being able to take a dxf file from what ever cad program you already know and use and then drop it into the laser cad program thats in english.  It is a lot simpler to do.  Dan


---
**andy creighton** *September 14, 2016 01:58*

I use autocad or scad and both import easily if saved as dxf, or bmp.



I have only used corel for design one time.   It wasn't good,  but it was usable. 


---
**Ben Crawford** *September 14, 2016 12:03*

I think i will upgrade eventually, my first priority is to finish my custom ajustable bed.


---
**Vince Lee** *September 15, 2016 16:08*

I get two primary advantages from my smoothiboard:  1) grayscale engraving, and 2) ability to engrave gcode from an SD card, working around usb connectivity issues.  For me, I have a moshiboard and since moshidraw supports both dxf import and native drawing, workflow is actually a little worse for the smoothieboard for simple jobs that I would draw in-app (which is why I made an adapter board to switch between both controllers).


---
**Ben Crawford** *September 15, 2016 17:33*

Just bought a ramps kit for my K40, not sure when i will be able to install it.


---
*Imported from [Google+](https://plus.google.com/110714520603831442756/posts/QCdhomDyCLZ) &mdash; content and formatting may not be reliable*
