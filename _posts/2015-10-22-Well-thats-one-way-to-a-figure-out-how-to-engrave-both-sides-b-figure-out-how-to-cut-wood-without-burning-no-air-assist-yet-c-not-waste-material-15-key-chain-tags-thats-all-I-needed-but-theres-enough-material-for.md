---
layout: post
title: "Well, that's one way to: a) figure out how to engrave both sides b) figure out how to cut wood without burning (no air assist yet) c) not waste material 15 key chain tags (that's all I needed, but there's enough material for"
date: October 22, 2015 23:28
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
Well, that's one way to:

a) figure out how to engrave both sides

b) figure out how to cut wood without burning (no air assist yet)

c) not waste material



15 key chain tags (that's all I needed, but there's enough material for 5 more), engraved both sides. The stock material did not move the whole time. I would engrave, cut, pick the pieces up and flip them over, then engrave the other side. The red stuff with holes that you see on the left in the second picture are the 3D printed guides. Since the X0 position for the laser head is so far from the rail, I decided to make those guides so the material is position just right for minimal waste. I'm currently printing another one for the back right side.



Next up is ripping the vent duct out and trimming it by 5mm or so. Unfortunately that requires the whole assembly to come out so I'll wait till I get the z-bed and do it all together. In the mean time I'm working on adding a flow and temp sensor to the water line. Had a couple of "warm" runs.



![images/7268c157a2759bf8bf4343d7bc7dfe68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7268c157a2759bf8bf4343d7bc7dfe68.jpeg)
![images/479808ee74d12e1e8a33db93a0e14307.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/479808ee74d12e1e8a33db93a0e14307.jpeg)

**"Ashley M. Kirchner [Norym]"**

---
---
**David Cook** *October 23, 2015 00:36*

Those look excellent !  nice work


---
**Anton Fosselius** *October 23, 2015 05:36*

Engrave,flip,engrave, cut would be awesome and less time consuming. You would just have to calculate the offset when flipped ;) 


---
**Ashley M. Kirchner [Norym]** *October 23, 2015 05:38*

Which is why engrave, cut, flip, engrave is easier as the stock material doesn't move and functions to re-align the pieces once flipped. Nothing in LaserDRW changes.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 23, 2015 08:00*

That's a very good idea for engraving both sides. I totally love the red stuff to guide where to place the job piece for minimal wastage. I've got to make myself something similar for a guide. Thanks for the idea.


---
**Peter** *October 23, 2015 08:31*

Depending on what u want to do with the vent, I'd thought to simply use a dremel to cut the top of the vent further back than the front with sloping sides from bottom to top.


---
**Phil Willis** *October 23, 2015 17:26*

What stock are they cut from?


---
**Ashley M. Kirchner [Norym]** *October 23, 2015 17:47*

**+Phil Willis** Maple thin wood veneer, [https://www.inventables.com/technologies/maple-thin-wood-veneer](https://www.inventables.com/technologies/maple-thin-wood-veneer) - specifically the 0.105" one.


---
**Ashley M. Kirchner [Norym]** *October 23, 2015 23:33*

First Saturday at Loveland (Colorado). It's for the world wide mobile game called Ingress ([http://www.ingress.com/](http://www.ingress.com/)) - I'm on the green team. First Saturday is a cross-faction event where both teams come together at a specified location to play together and help lower level players.


---
**Ashley M. Kirchner [Norym]** *October 24, 2015 00:13*

Capture the flag combined with geocaching.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 24, 2015 04:11*

**+Ashley M. Kirchner** That sounds pretty interesting, will have to have a look myself.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 24, 2015 04:20*

**+Ashley M. Kirchner** Wow, that is actually super cool. What would be even cooler if using something like Google Glasses you could actually see the "portals" in your HUD. That'd be fun.


---
**Ashley M. Kirchner [Norym]** *October 24, 2015 15:40*

If they can make the battery last, sure. Actively playing the game will drain your phones battery really fast. Everyone who plays Ingress walks around with extra batteries. Google Glass will last 10, maybe 15 minutes? Some of our larger operations take hours. 


---
**David Spencer** *July 25, 2016 20:32*

what size are those spacers


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/E2TgXvoB8uT) &mdash; content and formatting may not be reliable*
