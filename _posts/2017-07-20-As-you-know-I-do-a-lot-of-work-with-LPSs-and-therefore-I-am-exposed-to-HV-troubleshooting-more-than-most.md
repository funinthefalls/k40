---
layout: post
title: "As you know I do a lot of work with LPS's and therefore I am exposed to HV troubleshooting more than most"
date: July 20, 2017 02:16
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40LPS



As you know I do a lot of work with LPS's and therefore I am exposed to HV troubleshooting more than most. I never cease to be amazed at how much energy these machines have.



I do <i>NOT</i> recommend anyone that is un-trained in HV troubleshooting techniques to work on a <b>LIVE</b> laser power supply.



<b>HOWEVER</b>

There are sometimes when you need to work on, in or around your HV subsystem with the power off. So its is prudent to be safe even when not working on a <i>LIVE</i> K40.



Even an unplugged LPS can dump energy, believe me I have seen it first hand. The residual charge can significantly bite or harm you. 



I see a lot of broken supplies and HV problems and these are typical questions from users needing to troubleshoot them.



Q: How do you safely work on and around the HV in your hobby laser. 

Answer: You make sure all the energy in the LPS subsystem is discharged!



Q: How do you do that?



<b>Disclaimer: I am NOT guaranteeing your safety...you do this at your own risk...</b>



Unplug the machine and let the machine sit for a couple of hours with it unplugged before you inspect the anode wiring or root around in the laser compartment.  

BTW: if you do not have an interlock on the Laser compartment door... well shame on you for not listening and playing unsafe".



We are going to use a discharge stick to isolate yourself and insure that the LPS is discharged.



<b>Make a discharge stick ["chicken stick"] (see photos's below):</b>

See picture below, I think it is self explanatory. Mine is about 2ft long. You can use a dowel or PVC like mine. BTW I can sell you one for $200 ...:). Note I recommend PVC as wood can have high moisture content.



Ground the end of the wire <b>opposite</b> the taped end of the wire to bare metal on the cabinet. The terminal post on the back of your K40 is a good place after you insure that it is really grounded to the cabinet. You could replace the alligator clip on my example with a banana jack to make it more convenient.



Hold the end of the stick at the end <b>opposite</b> the taped wire. Put your other hand behind your back do not touch anything else with any part of your body.



<b>DO NOT TOUCH THE DISCHARGE STICKS WIRE</b>



Probe the bare wire end in and around the anode to discharge it before you enter the compartment. If you see a spark just silently say <i>"thank you Don, that woulda hurt!"</i>.



 

Be safe! 



[https://photos.app.goo.gl/3qO24eDaLjXR6ehW2](https://photos.app.goo.gl/3qO24eDaLjXR6ehW2)

[https://photos.app.goo.gl/IWnVbTBEIZJHhj9B3](https://photos.app.goo.gl/IWnVbTBEIZJHhj9B3)









**"Don Kleinschnitz Jr."**

---
---
**HalfNormal** *July 20, 2017 03:24*

My electronics background started with tube TV's which is what I learned electronic theory on. The first day of class, my instructor handed out wooden sticks and said, "This is the only tool to stick into a TV set until you are sure there is not any voltage left unverified!"


---
**Phillip Conroy** *July 20, 2017 07:42*

Don ,when working on alina radar system the hv was 80 kv,we called the discharge device a chicken stick,  always test stick before use


---
**Don Kleinschnitz Jr.** *July 20, 2017 12:19*

**+Phillip Conroy** Yes I have heard it called "chicken stick" you can find some <i>crazy</i> videos if you search on that term.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/XokJ66bJ8ki) &mdash; content and formatting may not be reliable*
