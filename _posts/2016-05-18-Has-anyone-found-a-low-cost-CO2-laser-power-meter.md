---
layout: post
title: "Has anyone found a low cost CO2 laser power meter?"
date: May 18, 2016 07:05
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
Has anyone found a low cost CO2 laser power meter? (under $100) idf possible. Or does someone have one I could borrow for a few days?



Thanks in advance

Anthony





**"Anthony Bolgar"**

---
---
**Eric Rihm** *May 18, 2016 07:26*

[http://www.instructables.com/id/How-to-measure-CO2-laser-power-on-a-Laser-Engravin/?ALLSTEPS](http://www.instructables.com/id/How-to-measure-CO2-laser-power-on-a-Laser-Engravin/?ALLSTEPS)


---
**Anthony Bolgar** *May 18, 2016 07:35*

Thanks for the link Eric. This seems to be the lowest cost Laser Power meter of a decent quality.


---
**Eric Rihm** *May 18, 2016 09:32*

Let me know if you measure the k40 curious what the laser tube actually outputs 


---
**Anthony Bolgar** *May 18, 2016 09:47*

Can't remember the exact number , but I saw someone else post in one of the facebook groups that the "40W" 70mm tube was only putting out about 25 W


---
**Don Kleinschnitz Jr.** *May 18, 2016 12:34*

Seems that they run about $200, however I have been thinking about making one. [http://donsthings.blogspot.com/2016/05/laser-power-meter.html](http://donsthings.blogspot.com/2016/05/laser-power-meter.html)

Then again if you want a true power measurement vs a relative one, a DIY version probably wont be accurate. 

I know of one user that measured his 50W at 40W and have heard that these cheap tubes are over specified.


---
**Anthony Bolgar** *May 18, 2016 12:35*

Ya, alot of so called 40W tubes are only putting out about 25W, the 50W blue boxes are really just overdriven 40W tubes.


---
**varun s** *May 18, 2016 13:45*

I purchased this one [http://m.ebay.com/itm/Mahoney-CO2-Laser-Power-Meter-Probe-0-200-watts-/322106472059?hash=item4aff0ab67b:g:Kq8AAOSwxp9W4N4J](http://m.ebay.com/itm/Mahoney-CO2-Laser-Power-Meter-Probe-0-200-watts-/322106472059?hash=item4aff0ab67b:g:Kq8AAOSwxp9W4N4J) 


---
**Jim Hatch** *May 18, 2016 17:15*

**+varun s**​ I have one of those. My K40 tests out at 30W at 18ma (32W if I leave it in the beam too long). The stated power on these is based on running the tube hot - over juiced until they die. So theoretically you could get 40W out of one but it wouldn't be long enough to cut/engrave anything.


---
**Eric Rihm** *May 18, 2016 17:17*

Do the Chinese light object tubes actually run at advertised wattage I want a real 50w


---
**varun s** *May 18, 2016 17:21*

Yup  **+Jim Hatch**​ and **+Eric Rihm**​ the actual power of tube is based on the length of the tube, these k40 comes with 30w and a real 40w tube is 800 or 850mm in length and has 50w peak output (so you can adjust it to use 40w)  I recommend SPT tubes really good quality and is around $110 a pc.


---
**varun s** *May 18, 2016 17:21*

Light object tubes are recommended.


---
**varun s** *May 18, 2016 17:26*

May be this will b helpful   [https://www.linkedin.com/pulse/how-buy-correct-co2-glass-laser-tube-alan-yan](https://www.linkedin.com/pulse/how-buy-correct-co2-glass-laser-tube-alan-yan)


---
**Jim Hatch** *May 18, 2016 17:27*

**+Eric Rihm**​​ Generally no. A 50W would come in a "60W" machine and cost $3000-5000 USD. A good way to check before you buy is to check on the length (or the width of the machine). A 40W tube runs about 830mm long, 50W is 1000mm (vs our "40W" tubes that are only 700mm).



That is a rule of thumb based on common sizes - tube lengths for a given power is dependent on things like diameter and internal configuration but for bon-laboratory/scientific stuff these are good lengths to use.﻿



LightObject tubes are more reliable. They're good at being truthful about their stuff. If you got one of theirs and they tested out low, LO would replace it. The Chinese eBay vendors will offer you $10-50 as a payment rebate instead.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/J12E2QhNG2u) &mdash; content and formatting may not be reliable*
