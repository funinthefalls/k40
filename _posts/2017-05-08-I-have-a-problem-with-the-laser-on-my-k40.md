---
layout: post
title: "I have a problem with the laser on my k40"
date: May 08, 2017 11:53
category: "Original software and hardware issues"
author: "Andreas Benestad"
---
I have a problem with the laser on my k40. Everything moves/works fine, except no laser beam coming out. I was sure it was the tube, so I have ordered/received a tube and installed it today. Still same problem. No laser firing. 

How can I check if it is the power supply? (never heard of a power supply partly failing...) 

I have a multimeter here. How should I test this..? thanks for your help.





**"Andreas Benestad"**

---
---
**Ned Hill** *May 08, 2017 12:05*

Assuming the tube's not firing at all, do you have any safety interlocks that could be stuck open?  Like for the door or for water flow.


---
**Andreas Benestad** *May 08, 2017 12:12*

As far as I know I have no such safety features. 


---
**Jim Hatch** *May 08, 2017 12:21*

What about the laser switch? Is it okay? You'll need a multimeter to check it but it's a quick test and cheap to replace.


---
**Andreas Benestad** *May 08, 2017 12:22*

Sorry for my lack of knowledge, what is the laser switch? How can I test it?




---
**Don Kleinschnitz Jr.** *May 08, 2017 12:28*

**+Andreas Benestad** does the laser fire when the test button down on the LPS is pushed?



Please post picture of your supply.


---
**Andreas Benestad** *May 08, 2017 12:32*

Here is the power supply. What test button do you mean? No response on the front panel or in laserweb. Or on this test button on the power supply. 

![images/674aee9fe948bdc5de9275b748713c5e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/674aee9fe948bdc5de9275b748713c5e.jpeg)


---
**Jim Hatch** *May 08, 2017 12:33*

It depends on the specific version you have but the standard panel has an Ammeter, a knob that turns, two push button switches and a rocker switch. 



The rocker turns power on/off. The knob turns the power up - that needs to be turned clockwise enough for it to lase. On mine that's about 4ma. But it will only show that when the laser is firing. So you have to be running a job or using the test button.



The two push buttons control power to the laser tube. One is momentary and only stays down while you push it. That's the test button and should be labeled Test Switch or something to that effect. On mine it's the right hand button. 



Next to that is the Laser Switch which also controls power to the laser. This one is the main one. If it's not switched on the test and power knobs won't have any effect. 


---
**Andreas Benestad** *May 08, 2017 12:35*

**+Jim Hatch** Right. Yeah, I have the version with a digital display. And yeah, the laser switch/button is turned on...




---
**Jim Hatch** *May 08, 2017 12:38*

To test it, turn the machine on, press the test and rotate the knob. See if the ammeter shows any power. It may take a bit of a turn (1/4 or more the way around) for it to show anything. If you make it all the way around and nothing happens, press the laser switch (I don't remember if pressing it down turns it on or if it's in when raised as I just leave mine and haven't touched it in a year). Repeat the test button & ammeter knob test. If the laser is getting power you should hear it & see the spot as it burns. The max you want to read on the ammeter is about 18-20ma to prolong the life of the tube. It will go higher but will shorten tube life.


---
**Andreas Benestad** *May 08, 2017 12:50*

I don't have an ammeter on my machine. No knob, just buttons and digital display.


---
**Andreas Benestad** *May 08, 2017 13:52*

Here is the inside of the power supply. The little fuse in the corner is not broken. I checked it with a multimeter.

Any other tests I can do to test that the power supply is working as it should...?

![images/ea9d4437652587ac7412fc40db8e30b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ea9d4437652587ac7412fc40db8e30b8.jpeg)


---
**Jim Hatch** *May 08, 2017 15:10*

You mentioned LaserWeb - have you upgraded/switched out the controller board?


---
**Sebastian Szafran** *May 08, 2017 15:12*

P+ and G is the safety interlock circuit here (where two pins are soldered together - this is 'Made in China' safety feature. You should have laser on switch on the panel enabling/disabling the laser. 



Lasers are not toys and they can make you blind or even kill you. If you don't know what to do, ask someone experienced to help you, don't take the risk.


---
**Nate Caine** *May 08, 2017 15:33*

In your photo you have a green AC power connector at the bottom of the page.  



Next above that is the green power LED.  That LED will will be lit when the internal +12V portion of the power supply is working.  That's a good sign.  Likely that means the other low voltages (+5V and +24V) are also working.



Next above that is the red push button TEST push button.  It is incorrectly labeled "TEXT".  When your machine is put back together and the AC is powered up, pressing the TEST/TEXT switch will cause the laser to fire.  However, if the Intensity input (normally a dial) is set too low, it won't fire.  And you said you have no pot (instead a digital design).   So, for the time being, this is a dead end.



BUT THIS MIGHT BE THE PROBLEM:



Next above that is a white connector labeled [P+  G].  And in the photo it looks kinda funny?  Looks black as if something might have arced, and the pins looks mashed together.  Especially compared to the next connector which looks normal. 



Can you take a better photo?  That connector is part of the "protection" circuit, and would certainly give the symptom you've described if the connect is damage, or if something zapped the input.  In the basic machine a front panel "Laser Switch" (push on/push off) will enable or diable the laser opertaion.  And a front panel "Test Switch" (momentary on) will cause the laser to fire as you press it...but only if the "Laser Switch" is on.



-------------------

If that doesn't pan out, and if you are confident working around dangerous voltages, you can start probing voltages into and out of the power supply.  Mainly the +5V and +24V outputs. 


---
**Don Kleinschnitz Jr.** *May 08, 2017 16:51*

**+Andreas Benestad** if the "test" button (red) down on the PCB does not fire the laser its likely you have a bad supply,  provided that you have AC power on the left connector.

Is the green power LED on, located on the power supply (next to the test button) when you power up the machine?

Some notes: 

...That PCB test button bypasses all the other protection mechanisms so it it does not fire with that test button the supply is fundamentally broken ... some how.

...LaserWeb is out of the picture for this level of troubleshooting.



You can test the DC voltages but whether they are correct will only give you a hint as to what is wrong in the supply. The DC voltages can be good (and often are) and the HV section is bad.



"Warning: there are life threatening voltages in this supply even when it is not working and the flyback is removed"



We can take a shot at replacing the flyback (common problem). That will cost about $30 but a new supply is $70-80.



Here is a test procedure to test external conditions but I doubt it will tell you anything new...



![images/448b94255163c308830ebe41cf042129.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/448b94255163c308830ebe41cf042129.jpeg)


---
**Andreas Benestad** *May 09, 2017 11:52*

Yeah the green light (power) is on when I turn it on.

I have re-soldered the connection between P+ and G. Still no difference.

Seems to be the Flyback transformer. But is there a way to double check this? Would hate to order a new flyback only to realise I should have gotten a whole new PSU...


---
**Don Kleinschnitz Jr.** *May 09, 2017 13:44*

**+Andreas Benestad** 

What you are doing is DANGEROUS!

......

I created a simple test instructed in the post below. This is not a definitive test but easy to do. I am curious as to your results since I have never identified a bad flyback with it since I do not have a bad flyback.

I am also working on a formal tester that we all can use but that is "in the que".



[http://donsthings.blogspot.com/2017/05/k40-high-voltage-flyback-tests.html](http://donsthings.blogspot.com/2017/05/k40-high-voltage-flyback-tests.html)



![images/8c74fa0ce20ad8338855b07735aecd19](https://gitlab.com/funinthefalls/k40/raw/master/images/8c74fa0ce20ad8338855b07735aecd19)


---
**Andreas Benestad** *May 10, 2017 08:17*

**+Don Kleinschnitz**​ Thanks! To be honest, I am a little reluctant to start playing with high voltages like this. I don't know enough to be confident here. Would rather not risk anything.



Question:

Is it safe to assume that since everything else works/moves correctly, and the only problem is no laser fire, that the issue is the flyback? And that the rest of the PSU is working normally? Apart from the tube being bad, can there be any other explanation to this?

Debating whether I should just order a new flyback, or an entire new PSU. 


---
**Don Kleinschnitz Jr.** *May 10, 2017 12:11*

**+Andreas Benestad** I get that question allot. Most power supplies that I have worked with have your exact symptoms. Only in one case has a  flyback repair attempted and in that case it worked. Most people just buy a new one any my source of information ends....



The typical parts that fail and the symptoms are:



...Rectifier: blows the fuse and the part is charred

...HV diodes or MOSFET: blows the fuse and/or charred parts.

...Flyback: your symptoms



In your case my intuition suggests that its the flyback but I cannot unequivocally prove that. 



This is part of the reason I am collecting bad ones. So that I can find out why they fail and make better recommendations on replacement/repair and perhaps improve the design or how we use them to the point they last longer.



If you are not budget and time constrained try the flyback first but be prepared to get a full supply if that does not work.



If you are time constrained get a new supply.



If you are budget and time constrained flip a coin and hope for the best....






---
**Andreas Benestad** *May 10, 2017 12:24*

I might try both. A flyback to attempt a repair and a full power supply just in case. I think I should be able to sell the power supply if I am successful with the flyback. They are quite expensive here in South Africa. :-)



These rectifiers; where are they located?? I can't find them.


---
**Nate Caine** *May 10, 2017 14:52*

Andreas, Looking back over some of your previous posts, you seem to have more than your share of problems with that machine.  Did you ever follow thru on Don's suggestion (outlined in his diagram--above) and connect the "IN" signal to the "+5v" and hit the "TEST" button?  Anything?



I'd suggest you thoroughly check your wiring for somethiing broken/intermittant.  And try to remove the controller from your debugging strategy.  Separate the controller from the power supply.   



Did your machine only come with the digital front panel, or do you have the old pot and meter still around?  Those could come in handy.



I'd suggest you purchase a new power supply, even if you plan on trying to repair the old one.  Doesn't hurt to have a spare lying around.


---
**Don Kleinschnitz Jr.** *May 10, 2017 16:46*

**+Andreas Benestad** 

In the attached picture:

The bridge rectifier is at the top right. It is a black rectangular device with a angle removed from it upper right corner. It will have + and - molded in its surface somewhere near the leads.

The Diode rectifiers are the 4 tubular black devices with silver lettering and a silver band around one end.

![images/69f76b64060f65ee755d7b8b1fa5a93f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/69f76b64060f65ee755d7b8b1fa5a93f.jpeg)


---
**Travis Hamilton** *August 23, 2017 04:43*

I have a K40 and a Full Spectrum Laser that is basically the laser they cloned to come up with the K40.  My FSL wasn't working when I bought it.  I thought it was the power supply so I bought a K40 PSU and still no laser so I took a closer look at the tube and it was all busted up inside. I should have looked closer, so I bought a new tube and now it works just fine with the original PSU.  I have a brand new K40 Power Supply just like the one you have sitting in my tool chest now. I would take 35$ if you wanted to solve your problem this weekend by installing my new one in your laser and be done with this issue.


---
**Don Kleinschnitz Jr.** *August 23, 2017 21:05*

**+Travis Hamilton** were do you live?




---
*Imported from [Google+](https://plus.google.com/112581340387138156202/posts/db24H1WnDKC) &mdash; content and formatting may not be reliable*
