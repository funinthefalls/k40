---
layout: post
title: "I've just been lent a K40 with a broken tube"
date: January 21, 2017 15:11
category: "Discussion"
author: "Gavin Morris"
---
I've just been lent a K40 with a broken tube.

Are there any tests I can/should do to see if its worth getting a new tube for it? We tested it (me and the owner of the machine) when we found the tube was broken and I know what the ammeter moved and the water pump worked. Can't remember much else.

The bed is slightly dented but I understand they are rubbish anyway!

I don't know much about which version it is or anything but it looks exactly the same as the one in the photo for this group.



Assuming it is worth getting a tube, these ones seem to be popular: [http://www.ebay.co.uk/itm/3000-4500-life-hours-Laser-Tube-to-40W-Laser-Engraving-Cutting-Machine-Engraver-/322192958907?hash=item4b043265bb:g:qJwAAOSwAYtWJJkx](http://www.ebay.co.uk/itm/3000-4500-life-hours-Laser-Tube-to-40W-Laser-Engraving-Cutting-Machine-Engraver-/322192958907?hash=item4b043265bb:g:qJwAAOSwAYtWJJkx)

I have been recommended this ones but it is shipped from China , takes longer and is twice the price. Is there any difference I wonder?

[http://www.ebay.co.uk/itm/Hign-Quality-40W-CO2-Sealed-Laser-Engraver-Tube-Water-Cool-70cm-Engraving-/230781149661?hash=item35bba0e1dd:g:MyAAAOSwux5YW86b](http://www.ebay.co.uk/itm/Hign-Quality-40W-CO2-Sealed-Laser-Engraver-Tube-Water-Cool-70cm-Engraving-/230781149661?hash=item35bba0e1dd:g:MyAAAOSwux5YW86b)



Finally, I'm wondering if there is there a page anywhere for noobies with a collection of links and info to upgrades, first steps and essential checks? Thanks







**"Gavin Morris"**

---
---
**Don Kleinschnitz Jr.** *January 21, 2017 17:36*

[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html?m=1](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html?m=1)


---
**timb12957** *January 24, 2017 13:48*

While I can not offer input about the tube replacement, I will tell you I have a K40 that needs parts (other than tube) I may be interested in buying your old unit depending on price. Contact me by personal message if interested.


---
*Imported from [Google+](https://plus.google.com/108653378710668517493/posts/1ynD7hBdo6S) &mdash; content and formatting may not be reliable*
