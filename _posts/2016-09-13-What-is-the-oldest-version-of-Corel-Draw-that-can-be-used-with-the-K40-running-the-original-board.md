---
layout: post
title: "What is the \"oldest\" version of Corel Draw that can be used with the K40 running the original board?"
date: September 13, 2016 06:37
category: "Original software and hardware issues"
author: "Joe Keneally"
---
What is the "oldest" version of Corel Draw that can be used with the K40 running the original board?  I am using the laserdrw software to good effect but I think I would benefit from Corel Draw and I am cheap.  Thoughts?





**"Joe Keneally"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 13, 2016 07:28*

Can't really say for anything prior to CorelDraw 12 as I don't know, but the dodgy software that shipped with mine was CD12 & it worked fine.


---
*Imported from [Google+](https://plus.google.com/100890899415096702555/posts/aBM3ikEzHQH) &mdash; content and formatting may not be reliable*
