---
layout: post
title: "15 Liters/mn give me 2m/s output air assit (at the lens, could be better if I shorten the 2.5 m pipe)"
date: March 20, 2016 13:08
category: "Modification"
author: "Stephane Buisson"
---
15 Liters/mn give me 2m/s output air assit (at the lens, could be better if I shorten the 2.5 m pipe).



far enought to protect the lens from the fume, nearly as good to improve combustion than the compressor (7.8m/s seem way to much). Still able to cut 3mm plywood.

 

Overall very happy, because the compressor was far too noisy. this air pump is quiet, a bit more loud than the stock exhaust fan, but less than the steppers. vibrations are not excessive (come with rubber feets).



+ point : AC, Quiet, cheap, allow built in with short pipe.

- point : maybe go for the 25 Liters, 15 Liters is a bit just.

 

[http://www.aliexpress.com/item/LF-2208P-Electromagnetic-air-pump-Fruit-and-vegetable-detoxification-machines-ozone-generator-aquarium-fish-parts-shipping/1128865153.html](http://www.aliexpress.com/item/LF-2208P-Electromagnetic-air-pump-Fruit-and-vegetable-detoxification-machines-ozone-generator-aquarium-fish-parts-shipping/1128865153.html)

![images/9d13285be1665935199e27e3d08c20df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9d13285be1665935199e27e3d08c20df.jpeg)



**"Stephane Buisson"**

---
---
**Stephane Buisson** *March 20, 2016 13:08*

Thanks **+Peter van der Walt** for the original link.


---
**Stephane Buisson** *March 20, 2016 13:14*

I can make a test for you, how much weight do you want me to lift.

(kind of test to mesure the force), I will try a SD card.


---
**Stephane Buisson** *March 20, 2016 13:22*

**+Peter van der Walt** not enought to lift a normal size SD. (maybe with a better rubber end), you should try more than 15L/mn (25L/mn?)


---
**Loïc Verseau** *March 20, 2016 21:04*

25L/min is enought for a 60W or 100W lasercut ?




---
**Stephane Buisson** *March 20, 2016 23:00*

**+Loïc Verseau** Should be depending on the output size. mine is quite small and concentrated, so I got good pressure.

25L/mn (15) is quite a lot of Oxygene for combustion of that size.


---
**Jean-Baptiste Passant** *March 21, 2016 07:38*

Thank you **+Stephane Buisson**, I was looking for something like that.



Amazon is playing with my nerves and my packages are stuck at their facility so no liquid coolant and no air assist. I will definitely check this item as I was looking for a good and silent air assist.


---
**Jean-Baptiste Passant** *April 26, 2016 11:31*

I've got the 25liter, will install it 7-8 may and tell you about it. I've tried it, it's noisy but bearable, airflow doesn't seems to be so powerful but since I did not try it in the machine I can't really tell.


---
**Stephane Buisson** *April 26, 2016 11:38*

all depend on the size of the air assist output (i think most of the poeple here are oversized so they need bigger compressor).


---
**Jean-Baptiste Passant** *April 26, 2016 11:51*

Output looks the same as yours, the description said there was two output, (can't affirm it as I can't access Aliexpress from work either) but there is only one. We'll see once I try it.


---
**Stephane Buisson** *April 26, 2016 13:09*

non je voulais dire a la sortie sur la tete (l'adaptateur 3d printed for me), c'est gros comme 2 aiguilles et directif sur mon design.

[https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser](https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser), mais bien orienter c'est efficace.


---
**Jean-Baptiste Passant** *April 26, 2016 13:34*

Ah ok, je n'ai pas verifié, j'ai recu la tête mais pas encore installée, j'etait déja bien content de faire marcher la smoothieboard avec le laser :P


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/SAQYBB3bDeS) &mdash; content and formatting may not be reliable*
