---
layout: post
title: "Here's my pen stand plans. My first plans so use at your own peril :) both jpg and cdr corel draw"
date: November 19, 2015 07:31
category: "Repository and designs"
author: "Tony Schelts"
---
Here's my pen stand plans. My first plans so use at your own peril :) both jpg and cdr corel draw

![images/96f2ed0f154ea9159d66e2ea1ba4cf51.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96f2ed0f154ea9159d66e2ea1ba4cf51.jpeg)



**"Tony Schelts"**

---
---
**Anthony Coafield** *November 19, 2015 08:53*

Thank you so much!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 19, 2015 21:32*

I like it. Simple, yet practical. Good work.


---
**Kirk Yarina** *November 20, 2015 19:59*

Thanks, Tony, looks very nice.  I'm a beginning pen maker and could use a display stand like yours .


---
**Tony Schelts** *November 20, 2015 20:37*

I hope it works for you, mines ok but keen to see if others find it suitable. 


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/1SX5PfpYSLs) &mdash; content and formatting may not be reliable*
