---
layout: post
title: "smoothieboard and level shifter ordered... I have 128x64 display from my ramp(can i use it) So what next do to with all this parts?"
date: April 29, 2016 16:02
category: "Smoothieboard Modification"
author: "Damian Trejtowicz"
---
smoothieboard and level shifter ordered...

I have 128x64 display from my ramp(can i use it)

So what next do to with all this parts?





**"Damian Trejtowicz"**

---
---
**Damian Trejtowicz** *April 29, 2016 16:12*

I hope i will cope with that,never use smoothie before,only ramps and arduino


---
**Vince Lee** *April 29, 2016 21:30*

I found that a level shifter wasn't necessary with my smoothie.  The laser enable on my PS is normally enabled with a pushbutton that drains it to ground when pressed and otherwise open.  Since the mosfets for bed and extruder heaters connect to ground the same way, I just connected the laser enable to one of them.


---
**Don Kleinschnitz Jr.** *May 01, 2016 15:56*

**+Vince Lee** for laser enable that makes sense, how did you connect to the laser power control? Do you have the G-IN-5V 3 pin control on the supply? i.e. will I connect a PWM control on the smoothie to the IN pin on the power supply, where the pot was connected to?


---
**Damian Trejtowicz** *May 01, 2016 16:06*

I will use stock psu what was with my k40


---
**Don Kleinschnitz Jr.** *May 01, 2016 16:15*

**+Peter van der Walt** to check my understaning. Is there two signals we are discussing here?

1. The laser enable which on my machine is accomplished by grounding the P+ pin on th LPS this is an on-off control.

2. The laser power control that on my machine is the center wiper on a 2K pot that adjust the IN pin between 0 and 5 v. 



#1 can be done without a level shifter from the smoothie.

#2 I am confused as to weather I can connect smoothie PWM to the IN pin. I think not because the IN pin expects a variable analog value. If I am right how does smoothie control power in this case as PWM is not the same as a variable DC value. 


---
**Don Kleinschnitz Jr.** *May 01, 2016 16:30*

**+Peter van der Walt** in #1 I assumed you could do this with a switching output i.e. MOSFET on the smoothie. Sorry but I am just learning about smoothie.



On #2 I am still not in sync. Since a PWM signal is a 0-5VDC pulse of varying duty cycle and my IN is looking for a voltage proportional to the power expected how does a PWM signal on IN work. You are saying that there is a RC filter on the IN that integrates the PWM signal to some analog value? Mmmm ... how do I determine if it does or not and is that a predictable way to control power. 


---
**Damian Trejtowicz** *May 01, 2016 22:47*

I will add level shifter to my circut and i hope on smootie laser will work better like on mega and ramps ,then i want order another smoothie an replace my ramps and mega in 3d printer


---
**Damian Trejtowicz** *May 03, 2016 11:04*

I hope this board this board is fine



[http://www.aliexpress.com/item/3Dpriter-control-board-MKS-SBase-V1-0-32-s-Motherboard-compatible-Smoothieware-open-source-firmware-support/32384368763.html](http://www.aliexpress.com/item/3Dpriter-control-board-MKS-SBase-V1-0-32-s-Motherboard-compatible-Smoothieware-open-source-firmware-support/32384368763.html)


---
**Damian Trejtowicz** *May 03, 2016 11:09*

I use Cinese copy as trial and for testing, i dont want learn how to connect and use on expensive genuine one,when i get all right and working state i will swap for right one like im always doing with my arduino projects


---
**Damian Trejtowicz** *May 03, 2016 16:42*

**+Peter van der Walt** whan i was lookin over schematics in blue box quide from **+Stephane Buisson** something is not right for me because looks like for me pwm and fire pin from laser is connected to tha same pin in PSU.

In my RAMPS fire pin is connected to L and PWM is connected to IN pin in PSU,in that way i have software controll over laser power.

Now its confusing to me how to connect level shifter :(


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/892vrfk87ne) &mdash; content and formatting may not be reliable*
