---
layout: post
title: "Guys how much power does this machine actually burn?"
date: November 16, 2017 18:14
category: "Discussion"
author: "Chris Hurley"
---
Guys how much power does this machine actually burn? (50% power for a hour) 





**"Chris Hurley"**

---
---
**Jason Dorie** *November 16, 2017 23:50*

Buy a Kill-a-watt or something similar and you’ll know for sure.  My 100w pulls 630w just for the laser itself on “full” power.  Not including exhaust or air.  I’d expect the K40 to be around 250w to 300w based on that, but it might come in lower or higher, depending on how efficient the power supply is and a hundred other things.  :)


---
**Paul de Groot** *November 17, 2017 05:40*

**+Jason Dorie** yes the power supply on the K40 is indeed 250watt


---
**Chris Hurley** *November 17, 2017 12:27*

Thanks guys. Just building a rough power budget. 


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/eCZcnLbYVrB) &mdash; content and formatting may not be reliable*
