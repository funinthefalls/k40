---
layout: post
title: "Just got my K40 machine today, but without CD and can't get the USB to work, anyone knows?"
date: September 15, 2015 20:49
category: "External links&#x3a; Blog, forum, etc"
author: "Maartje Kroesbergen"
---
Just got my K40 machine today, but without CD and can't get the USB to work, anyone knows?





**"Maartje Kroesbergen"**

---
---
**Sean Cherven** *September 15, 2015 21:03*

Here are the contents of the CD: [https://www.dropbox.com/s/86ib5q5i9jvk4xs/Laser%20Cutter%20-%20Easy.zip?dl=0](https://www.dropbox.com/s/86ib5q5i9jvk4xs/Laser%20Cutter%20-%20Easy.zip?dl=0)



And the USB is just a key which allows the machine to work. Think of it like a key to your front door... except in digital form.



Hope this helps!


---
*Imported from [Google+](https://plus.google.com/+MaartjeKroesbergen/posts/QdGxr6cbwvc) &mdash; content and formatting may not be reliable*
