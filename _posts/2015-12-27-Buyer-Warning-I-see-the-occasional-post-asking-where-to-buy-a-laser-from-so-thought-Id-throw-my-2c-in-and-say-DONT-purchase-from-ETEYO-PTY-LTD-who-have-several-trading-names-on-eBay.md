---
layout: post
title: "Buyer Warning - I see the occasional post asking where to buy a laser from, so thought I'd throw my 2c in and say DON'T purchase from ETEYO PTY LTD who have several trading names on eBay"
date: December 27, 2015 22:54
category: "Discussion"
author: "Anthony Coafield"
---
Buyer Warning - I see the occasional post asking where to buy a laser from, so thought I'd throw my 2c in and say DON'T purchase from ETEYO PTY LTD who have several trading names on eBay. The ones I know of are- amazingitem, happystoreshopping, anhd bestpricedigitalitem. 



I bought a K40 from them mid Oct. The screw to the laser was threaded and took a long time for me to get in to check the laser, which had no shipping protection, despite a piece of paper in the main part asking me to remove said protection before operation. Nor did it contain the software or manual. It took almost 2 weeks for them to sort these things out, then after less than two hours use the laser started to die when cutting. I checked the date on the laser and it was made in Sep 2013. As you know if not used these things degas and die. These things happen so I didn't mind waiting as they seemed to be trying to fix the problem quickly.



After 2 weeks nothing had been sent to me, they said they still hadn't ordered it, but promised me that if I didn't have a new laser within 15 days they would take the product back. 



At the end of that time they said a laser had been sent and gave me a tracking number but no link to track it or information on who the carrier is. I asked this and after a few days back and forth was told it hadn't been sent. The day after that I was told it had been sent to me but broke on the way so was going back to them and they'd sort a new one. They offered me $10 compensation (I paid $10 more than the current selling price).



We've used couriers a lot in our business, never have I had one decide that something was broken and send it back to the supplier. I asked for the tracking link again to they could prove it had been sent, they've refused to give it to me. 



By then I was rather frustrated and wanted to send it back, just like they had offered. We have spent the last 10 days trying to get how the want me to send it, and where to send it to while they just keep saying they'll try to get me another laser. 



It is too late for me to leave eBay feedback of have any eBay buyer protection.



Things will go wrong, that's just life. If they'd sorted it out quickly and with communication I would have been a convert sharing how great they were. Unfortunately now I can just try to warn others. 



Currently we're working through PayPal to sort the dispute, however it seems they just want to keep padding the process until it is too late to have any form of recourse.







**"Anthony Coafield"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 28, 2015 03:24*

Funnily enough, I purchased mine through ebay from amazingitem, also in mid October. They even gave me a $50 refund because I complained that no matter what I did I couldn't get it aligned. It came well packed & worked fine, just way out of alignment. Mine also came with no "shipping protection" that I was told on the piece of paper to remove before operating. Didn't really bother me as the tube & everything worked, but was odd, to say the least. My laser tube actually has no details for manufacture date or anything like that on it.


---
**Anthony Coafield** *December 28, 2015 05:51*

I'm glad yours is good. They initially offered me $50 for the inconvenience, then took that back and said that was only if I kept it with the faulty tube... They definitely have the best eBay description etc of the machine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 28, 2015 07:09*

**+Anthony Coafield** Yeah they offered me 3 options, 1) keep unit & accept refund (they offered $10, but I said that won't help me get it aligned), 2) they will pay for a technician to service/align it, 3) return the item. I said I will take option 1 if they increase it to $50 so I can at least pay 1 hour of labour to technician (I just spent 2-3 days doing it myself though).


---
**Bryon Miller** *December 30, 2015 19:36*

I sell on ebay and know about their protections.  There's essentially almost none for a seller and a good amount for buyers.  Your paypal case is most likely item not as described so at the end of this, paypal will make you pay for return shipping to send the laser back and force the seller to refund you.  It'll probably be costly, this thing came in a huge box.  You could also dispute with your credit card company, but paypal updated their policies to more closely mimick a credit card chargeback timeframe.  Or in other words, their merchant account holder got on their back for having too many chargeback disputes, so paypal made changes to reduce those on their end under the guise of offering better protection to their users.


---
**Anthony Coafield** *December 30, 2015 21:44*

Thanks **+Bryon Miller** . It's taken me 10 days but they've finally provided me with their return address, so I'll box it up. I've checked with a few couriers and it won't be too costly to return (around $45), and PayPal offer return shipping refunds (we'll see how well that works) so shouldn't be out of pocket, but that's less than 10% of the laser so a small price to pay as opposed to having a fancy paperweight taking up my whole desk!


---
**Chris Eslick** *May 18, 2016 10:19*

I am having the exact same problem with my 60w laser from Happystoreshopping. I have almost had the exact same experience word for word including the promise of shipping new parts and them being broken and returned by customs. And now I have had my PayPay dispute declined because they told me that I have to close the claim to get a refund. Now I am stuck with a dead laser cutter and no protection.

I have now placed a complaint against them using consumer affairs. PayPal gave me a return address of 1 Pennel Ct

Rowville VIC 3178 if anyone lives nearby to knock on their door.


---
**Anthony Coafield** *May 18, 2016 11:53*

Chris, did you close the claim? If not you can escalate it. I returned mine, sent proof with signature to them, then had them claim they hasn't received it. Because I had photographed it all and had the tracking signature PayPal granted me a refund. If you used your credit card through PayPal you could do a chargeback on it also.


---
**Chris Eslick** *May 18, 2016 12:08*

Yeah stupid me did close the claim by accident. PayPal won't reopen the claim unfortunately. I suspected that they would pull something like that with a return. These guys need to be shut down. What address did they give you? It was a pay after delivery deal without the credit card :(


---
**Bryon Miller** *May 18, 2016 15:08*

If your paypal purchase was funded by a credit card you can still contact them and do a chargeback as long as too much time hasn't passed.  They intentionally stalled you so you would lose your protection.  Ebay is a shady marketplace.  The management are beyond questionable, they send lists of the highest sought after goods via their search engine to the chinese for production.  The government here doesn't seem to care at all about questionable practices like that for some reason.  They've said their target base of customers are people with more time than money, they have a lax return policy that destroys US sellers profit and they bolster a money back guarantee program that you didn't get far enough into to realize the huge flaw.  Had this seller not stalled you, you would have found out that the portion of the Money back guarantee that atually convinces you to buy would not even apply to your purchase because it's over seas in china.  That would be the coverage of return shipping.  The MBG doesn't say anything about China not covering the return shipping does it?  Due to limitations in the system, certain international locations are not capable of creating an ebay label for the MBG.  China is one of those places.



I would not recommend anyone purchase ANYTHING from ebay without going through a credit card.  This means don't use your paypal balance, use the credit card.  That will extend you the Chargeback protection of the card which will override the ebay policies if the credit card doesn't agree with ebay's ruling.


---
**Spyro Polymiadis** *August 25, 2016 01:19*

I bought a 60W laser cutter from amazingitem... paid 3200AUD for it.. first, they were out of stock when i bought it, so i got a $20 'compensation' while i waited a few days for them to get stock in.. then when it arrived, they had wired 240VAC active power direct to earth on the laser PSU (eg, seems they wired the plug upside down) - so that blew all the fuses, and tripped all the rcd's in my house... so i got a refund for 240$ to buy a replacement PSU, then the laser wouldnt fire, so they are refunding me $500 to buy a new ruida controller... despite the issues.. ive been mostly fairly happy with the level of service im getting from someone who is clearly in china, and is a middle man to the product they are selling..  I have heard much much worse stories from people who have dealt with amazingitem...  who knows.. a different day could have been a different story


---
**Alex Glendinning** *October 11, 2016 07:16*

Looks like I'm not the first to be screwed over by ETEYO PTY LTD and their ebay shopfronts.  



50W unit purchased a August dead by September with no output from the tube... so either a dead tube or the HV driver.  I'm in remote part of WA Australia so shipping back this item is not really a financially viable option for me so i want the parts shipped. paypal dispute filed but even they want the item to be returned to  

Suite 234, 1 Queens Road, Melbourne, VIC 3004

Me just wanting a working machine I'm going to be 2K out of pocket with the return option but only say 700 with keeping it and repairing it myself (tube and HV driver) so its not going back... My plan was to repair it myself and with the money saved go to melbourne and put my foot up the ass of the rep there but looks like they give out multiple addresses so i might have to extend my stay if i go down and hunt them all down. 


---
**Chris Eslick** *October 11, 2016 11:24*

These guys need to be stopped!! So many faulty products. I have had to fully rebuild mine because of the shitty workmanship. Good luck on the hunt. Let us know how it goes. 


---
**Alex Glendinning** *October 12, 2016 00:21*

well looking at the tol ipec tracking records the thing was picked up at REGENTS PARK, NSW, 2143. so the ABN says Melbourne based so far I've got 2 addresses from there... Is it possible the whole operation is just out of china with no reps here at all?


---
**Chris Eslick** *October 12, 2016 00:36*

I found the same information when doing my own investigation. The only point of contact possible would be the warehouse where it was shipped from in Sydney. That's where I would go door knocking. They seem to ship most things from that central point. The rest seems to be a front. I bet that they don't even pay gst through that abn. The ato might be interested in that 😉


---
**Anthony Coafield** *October 12, 2016 00:59*

My return address was Regents Park. Judging from their lack of English skills I'm guessing they just have a warehouse they distribute from and no Aussie representation also.


---
**Alex Glendinning** *October 12, 2016 01:13*

yea that's what I'm starting to suspect, they always ask for videos of the fault and you put it up on youtube and they say its blocked in china so obviously no one in AUS can see it. Can you force them into small claims court? I know the costs and all will make it a dumb idea but I've got a friggin good paying job and they've just topped my enemy list. If they have no one here would they have to waste money coming from china or at the least bankroll some one over here to stand in for them. I thought i was in a remote part of the country but these bastards might not even be in the country so we could have the upper hand in putting pressure on their australian registered business.





Anyone in the future with issues from this goose of a seller keep the paypal dispute / claim going as long as possible. Hes getting very pissed at me for not closing the claim as it freezes his money, If you do go with this seller use a credit card with purchase protection to do so linked with paypal. Although paypal are being useless as fuck over this situation they have no say over what American Express rule in my case.... So ultimately I've got Paypal freezing his funds for the claim until that is resolved and AMEX hopefully ruling a charge back to cover the costs of the repair.  Now happystoreshopping is offering what i originally wanted to close this all out and offering the money "offline" to shut me up and to close the case. still not going to do what he wants I'll trust AMEX to give me the refund and not that prick.


---
**Alex Glendinning** *October 19, 2016 08:36*

**+Anthony Coafield**  you mind sending me that address? thats about the only place I think that would be worth while visiting and screaming at some one.


---
**Anthony Coafield** *October 20, 2016 06:26*

No worrries. All I was given is- 

c/o Block L

391 Park Road

Regents Park NSW 2143



Good luck!


---
**Anthony Coafield** *October 20, 2016 06:33*

That was valid Jan this year at least...


---
**Alex Glendinning** *October 21, 2016 05:39*

Sweet, I'm from Sydney originally so i know plenty of people to pay this guy a visit.


---
**Alex Glendinning** *November 04, 2016 00:36*

Well seems like he outsources his warehousing so going there and yelling at people is pretty much unjust and would be a case of 'don't shoot the messenger'.... a faceless company, AMEX wont cover it as its a "product defect" and that's not part of the purchase protection, fuck knows why. 



no other option than to take him to small claims court, being in WA his business is registered in VIC but he operates from china... no idea how the logistics of that would go so might just have to admit defeat on this one and just continue to annoy the shit out of him on ebay as petty revenge.


---
**Anthony Coafield** *November 04, 2016 02:21*

That sucks. Sorry dude.




---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/Tf3J5Ej2KGS) &mdash; content and formatting may not be reliable*
