---
layout: post
title: "So yesterday when I was engraving some leather for the phone case I came across an issue with my machine"
date: April 02, 2016 06:53
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So yesterday when I was engraving some leather for the phone case I came across an issue with my machine. If you notice my logo in centre seems to have slipped or something to the right twice during the engrave. It actually happened on 2 pieces of leather on 2 separate attempts. At the exact same spot on the engrave also. Nothing odd seems to happen when I am cutting at this point, so I'm wondering is this a software or hardware issue? Any suggestions what to look at to fix this?

![images/d9cb0584fbae9a0dd64b0c73ce8e9fe8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d9cb0584fbae9a0dd64b0c73ce8e9fe8.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**I Laser** *April 02, 2016 07:12*

I've had similar issues on certain files, seems like there has been some sort of corruption in the file data. Looks fine in corel, but skips at a certain points. I'd reimport your logo and see if that fixes things.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 08:12*

**+I Laser** Thanks I Laser. I'll give that a go & see if it fixes the issue.


---
**Scott Marshall** *April 02, 2016 09:12*

It sure could be a software glitch, knowing the software as it is, but, check the following too (the fact it's in the same place makes me think belt teeth  or binding 1st):



Looks like missing steps (a lot of them). Look for loose pulleys, loose belt,  missing belt teeth, and that sort of thing in the X axis. Binding of the head from bad rollers, guides, and idlers will do it too. Make sure the Axis moves smooth and easily with the power off.



It could also be an intermittent electrical connection in the X stepper motor wiring, like loose connector, fatigued "flat' cable or anything else that would generate loss of continuity in the 4 motor connections.



Lastly, if you're running on the edge of too fast, as it runs,  heat will reduce the stepper motor torque (due to reduction of the power supply and motor capacity) and cause it to "cross the line" and start loosing steps. 



The one big disadvantage to steppers is they have no feedback so there's no alarm or correction if they are interfered with, or fail. The controller just keeps going ahead as if nothing happened.



Nice cases BTW, love the kangaroo.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 09:15*

**+Scott Marshall** Thanks Scott. I will take a look at the pulleys/belts/teeth/connections/etc.



I was running at 500mm/s for the engrave, so would that be considered "on the edge of too fast"?


---
**Scott Marshall** *April 02, 2016 09:17*

I never go close to 500, 200 is my max. It may be able to do it, but it seems kind of fast. Maybe fast enough to use up any safety margin, which is your only protection against "soft failures"



Maybe slow it down 1st and see if the issue goes away?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 09:21*

**+Scott Marshall** I've been using 500mm/s since I got the machine, so maybe it isn't a suitable speed. I'll give it a check going slower to see if it improves the issue. Still, I need to pull machine apart (for a long time) to cut back the exhaust vent. Just been waiting for a real reason to pull it apart. Thanks for your suggestions.


---
**Scott Marshall** *April 02, 2016 09:28*

I'm not saying 500mm/s is too fast, I just never have tried it.

 if I was doing production engraving, I'd be running it as fast as I could without problems. Been ill, so haven't got the laser back together, but I've got a couple of products I'll need the speed for when I get back in the game here.

Good luck!


---
**I Laser** *April 02, 2016 10:00*

If your exhaust is welded on, I'd advise you cut it completely out from behind. I initially removed the gantry and cut as far back as I could, but the exhaust was still getting in the way! 



Completely removed it last week, no need to remove anything, cut from behind and slide it out the back. Wish I had of just done that in the first place... :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 11:06*

**+I Laser** Fortunately mine is bolted on. I've just pulled the machine apart & removed the exhaust vent totally.



**+Scott Marshall** 

I've got the entire gantry sitting on the table now doing a test run @ 500mm/s of the exact same file I had slips on. So far it seems no slips when the gantry isn't in the machine.


---
**Scott Marshall** *April 02, 2016 11:35*

You have my sort of luck. It's NEVER simple. Ia an air assist tube or something like that dragging when it's in the box?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 12:58*

**+Scott Marshall** That is a possibility actually, since my air assist tube wasnt held up by a drag chain or whatever, it just dragged along the cutting bed & may have got caught on my magnets I use to hold the leather in position. Since I've got the machine apart, it seems I may as well do a few mods while I'm at it.



Planning on attaching my old Lumia 520 somewhere to be able to film/watch via screen (using Project My Screen). I'm considering attaching it on the laser head, but concerned about the weight. I may pull it apart & just attach the circuit board with camera to the laser head (it would be much lighter).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 12:59*

**+Carl Duncan** I will give it a decent run & check if they seem to be getting hot. Good suggestion though.


---
**Scott Marshall** *April 02, 2016 13:10*

It happened to me, but I was right there and it made a helluva racket jumping steps, I was able to shut it down before it damaged anything. The tubing had wrapped around the head and I'm surprised it didn't get lasered. Lost my workpiece of course. And needed a alignment touch up.



I then dressed the tubing properly. If you attach it to the center of the rear of the door opening with it pointing to the right and leave the proper length, it runs up and away from the workpiece and doesn't catch on the works. I just tacked it in place with some CA to see if it indeed would work. It seems to using 1/4 (5mm) OD vinyl tubing. 



I'd like to use a cable chain, but here isn't really any room to.



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 13:24*

**+Scott Marshall** Thanks for that advice. I will try attaching to that position you suggested to see if it makes a difference for keeping it out of the way. Going to need to keep some cables out of the way too, so might be worth me getting a cable chain eventually.


---
**Scott Marshall** *April 02, 2016 16:10*

If you figure out where to put it, let me know!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 16:39*

**+Scott Marshall** Will do once I figure it out.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 15:58*

**+Scott Marshall** Hey Scott. Check my new post. I did a test with attaching the airtube where you suggested (in the centre of the lid at the back). I just attached it via a twisty-tie for a test & it seems like it is a perfect place to attach the airtube. Thanks for the suggestion.


---
**Scott Marshall** *April 04, 2016 17:50*

Glad it worked out for you, it was the best trial and error location I've found. Occasionally it makes me nervous if I'm engraving near the top, but it seems ok so far.



Thanks for the post on the image posting, gotta take some time and really understand it. I may have some questions.



I've had good luck engraving and cutting the large tongue depressors, the bulk ebay ones, don't know what kind of wood they are, but the laser loves it. It cuts like butter and engraves nice and dark with great detail. I made some business "cards" from them, but I started out using them as dividers for parts drawers. They're a little over 6" long, 11/16 wide and 1/16" thick. Bought 1000 of them for around $20us.



Focus makes a big difference with cutting the 3mm ply (it's my main medium) and going to a longer focal length lens helps a lot because it increases the focal length (range it's in focus), but if you go too far, the beam widens and doesn't engrave well. I've tried 2' (stock), 3" and 4", and the 3" seems to cut great with only a little loss of sharpness when engraving. With the head I've got, it's an easy fix, and I was thinking of making a couple more tips up to fit the head next time I'm doing lathe work.(I need to clean it off and fix a broken carriage holddown). Then I can change the lens in seconds without handling the lens.



With the 3mm ply and the stock lens, I've found 2 passes work best, focus on top, cut a pass, drop it 3mm by eye, and finish the cut. It chars less and the kerf is more parallel if you keep the power up and go to 18mm/s or so.



Thanks again, Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 17:58*

**+Scott Marshall** Thanks for that info about the lenses/cutting. I was considering tongue depressors or coffee stirring sticks as an alternative to increase the engrave-able area, thus making my wording larger & easier to read from further than 5cm away from it haha. I'm interested to get different focal length lenses. What would be nice is some kind of revolver style cartridge that allows you to just rotate it to select the lens you want to use. If I had a 3d-printer I'd be in heaven for modding. 


---
**Scott Marshall** *April 04, 2016 18:08*

Now THAT is a good idea, little stepper, hardware based driver, could be done...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 18:54*

**+Scott Marshall** Or even just manual rotation by hand. But I like your thinking of automating it. Much better.


---
**Scott Marshall** *April 04, 2016 19:37*

Can't help myself, it's a disease.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 20:20*

**+Scott Marshall** It's a very useful disease. But this place is great for bouncing ideas around. Someone will always have an idea & then another idea to improve that idea from someone else, then it just keeps going. Before we know it we'll have lasers that do all the design & work for us while we sit back & relax. Haha.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/gcXnSWHdExH) &mdash; content and formatting may not be reliable*
