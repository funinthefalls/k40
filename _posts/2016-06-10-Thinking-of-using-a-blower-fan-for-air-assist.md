---
layout: post
title: "Thinking of using a blower fan for air assist"
date: June 10, 2016 15:17
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Thinking of using a blower fan for air assist. I've read in the last couple of days someone using it,  but don't remember who. 





**"Ariel Yahni (UniKpty)"**

---
---
**Stephen Sedgwick** *June 10, 2016 15:51*

That is what I have been thinking of also... not sure why to use an air pump over a fan blower...other than you can direct it easier with a tube.



 I have also thought about a vacuum around the laser head to help suck that same smoke away from the center of the cutting area.


---
**Don Kleinschnitz Jr.** *June 10, 2016 16:17*

I tried a vacuum head and it does not come close to beating my 400CFM fan connected to the whole box. It is so powerful I cannot leave light stuff on the table or it ends up in the fan. Put a grill on the duct behind my cutting area.


---
**Ariel Yahni (UniKpty)** *June 10, 2016 16:19*

**+Don Kleinschnitz**​ that's for the exhaust. I'm looking for better cuts


---
**Anthony Bolgar** *June 10, 2016 16:34*

Air assist works best when focused as close to the target of the beam as possible. The main function of air assist is to allow for deeper, faster cuts. Keeping the smoke off the lens is secondary.

 The air stream provides additional oxygen for the laser to burn throught the material.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 10, 2016 16:51*

**+Anthony Bolgar** In this case, does more air at the point of cut cause this to work better? e.g. If I had say 30L/min focused at the spot or 100L/min, would the higher amount be better? Or would there be a point that it plateaus out?


---
**Anthony Bolgar** *June 10, 2016 17:02*

I am sure there is a plateau, not sure where it is, but basic science tells us that fuel + heat + oxygen= fire. And that is what the air assist is doing, supplying additional oxygen.


---
**Don Kleinschnitz Jr.** *June 10, 2016 22:17*

My machine has both air assist and high capacity evacuation. My cutting is pretty good, 1/4 inch acrylic but only after I installed both of them. 

My understanding of the theory was that they move the cutting smoke etc, rapidly out of the cutting area where it reduces the laser power. With side benefits of keeping things clean and not stinking up my house :)


---
**Ariel Yahni (UniKpty)** *June 10, 2016 22:19*

**+Don Kleinschnitz**​ what actually are u using for air assist


---
**Stephane Buisson** *June 10, 2016 22:44*

from my experience, 

the fire from the laser is small (don't need much extra o2) but blow the ashes away, closer is better, reducing the air output noozle increase the pressure (fan leak and  are not the best for high pressure)


---
**Ned Hill** *June 11, 2016 01:51*

Science also tells us that airflow will also conduct heat away, so more air is not necessarily a good thing.  At a high enough rate it could, at least theoretically, reduce the burn.


---
**Pippins McGee** *June 11, 2016 14:18*

**+Ned Hill** you make me feel better about the nugget of an air compressor I just bought haha. 23L/Min, can barely feel much pressure exiting the LO laser head. enough to blow smoke away obviously as that doesn't require much pressure at all but hardly a high pressure stream hitting the material being cut at all. also it blows in pulses... not steady stream... don't like that about it either.

note: This is what I bought as others have said it was fine.. But for its size it doesn't produce a lot of air haha.. I'l admit it is whisper quiet though. barely makes a noise especially in comparison to water pump and blower.

[http://www.ebay.com.au/itm/131682342405?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/131682342405?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Ned Hill** *June 11, 2016 18:09*

**+Pippins McGee** If you want a smoother air flow you need to add a air tank like this one. [https://www.amazon.com/Pro-Lift-W-1005-Grey-Air-Tank/dp/B000BO9TMU/](https://www.amazon.com/Pro-Lift-W-1005-Grey-Air-Tank/dp/B000BO9TMU/)

Remove the regulator from the pump and plumb the pump straight into the tank with a coiled air hose.  Come out of the tank with a small ball valve and then attach the regulator.  Your pump will pressurize the tank to the max and cutoff and come back on when it drops to the start pressure.  Smoother air flow and the pump won't run as much so it won't heat up as much.


---
**Vince Lee** *June 11, 2016 19:54*

I have a small 24v ducted fan mounted on the head and it works great to blow out flames.  I tried a couple to find one with high enough air flow.  I also updated my exhaust to use a 500cfm blower but run it at low speed because it is a maelstrom inside at full speed.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/D6XYgtMcuvT) &mdash; content and formatting may not be reliable*
