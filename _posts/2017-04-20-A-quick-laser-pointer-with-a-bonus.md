---
layout: post
title: "A quick laser pointer with a bonus"
date: April 20, 2017 04:57
category: "Modification"
author: "Steve Clark"
---
A quick laser pointer with a bonus.



 I got tired of  finding the left upper corner and did a quick fix with a little laser then discovered a bonus function!



As I said I was tired of doing a test fire to find that upper left corner so decided to whip up a prototype laser pointer.  I’ve seen two lasers used to get cross hairs but that seemed like way to much work and too much mass.



In  thinking about laser coherent  light properties.  Hmmm… that means there is almost no light scattering so a mechanical cross hair can be anywhere in the beam… up against the emitter… or a foot away and you still should get a crisp and  well defined shadow.

 

I had some cheap emitters from past projects so I pulled one them out to play with. Just like these:



[http://www.ebay.com/itm/Mini-6mm-3V-5V-650nm-5mW-Laser-Dot-Diode-Module-Head-WL-Red-With-Driver-Circuit-/112326987500?var=&hash=item1a2735ceec:m:mpyf5azZERbMddC7i9JlgnA](http://www.ebay.com/itm/Mini-6mm-3V-5V-650nm-5mW-Laser-Dot-Diode-Module-Head-WL-Red-With-Driver-Circuit-/112326987500?var=&hash=item1a2735ceec:m:mpyf5azZERbMddC7i9JlgnA)



I took a piece of approx .015” wire and cut two short pieces to fit over the front face of the laser diode and glued them down with super glue (I was in a hurry and doing it again I would use 2 part epoxy, less chance of messing up the front lens).  You can tell I did even center the wires well but as it turns out it doesn’t matter. I put 3 volts to it… and I’ll be… it works and is sharply defined at any distance.



So still being in a hurry. I took a piece of angled aluminum I had laying around and made a bracket by band sawing it out, a little quick filing and belt sanding. Drilled one hole to slip over a 4-40 screw and be tighten by a nut. Also I band sawed part of the bracket on the back thinning it so that I could adjust it in the Y axis ( fore and aft) just by bending it into position (just in that  y axis). The right to left (X axis) is adjusted by loosening the nut and swinging it. 



I mixed up some 5 minute epoxy, then setting it aside to thicken some… drilled a few small holes in the inside corner of the angle to act as anchors for the glue. Dropped some of the thickened glue into the corner and being careful not to get glue on the emitter face place it in the corner and adding more glue on the outside. Notice in the picture the base was filed square and glue wiped off those surfaces so I could use a crescent wrench to force the bend adjustment as needed.

 

I added a cheap 12v to 3v transformer and an on/ off switch I had then turned it on… hmmm?? Nothing no light… traced my wires and OK going too fast… forgot to hookup the  little transformer to the 12v power! 



[http://www.ebay.com/itm/122360468351?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/122360468351?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)

 

It worked very, very well… once power hooked and adjusted.  The adjustment consisted focusing the beam height first onto a piece of wood and bumping a burn spot. Then bending the y axis until centered and next loosening the nut and swinging the X axis to center. Burning another spot and readjust as necessary (it took about six burns for me to get it). Ta daa… finished.



Now for the bonus… Because the laser diode and bracket are parallel to the x axis and at about a 30 degree angle they move left or right only when the table is moved up or down,  you can just burning a very light spot and move the z table (you do have a power z table right?) until the cross hairs put the beam at focus when the burned spot is centered in the cross hair!



A little trig and rough measurements of the beam diameter to cross hair center can even help you adjust beam center up or down into you work piece. 



That’s it for now… pictures below, it was hard to get a good focus on the laser beam it’s dead sharp in real life… someday I might even machine a real bracket and diode housing.   





![images/5e0cf76a5d8fc525e226268ded5a2cfd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5e0cf76a5d8fc525e226268ded5a2cfd.jpeg)
![images/34e13aaf910833549bc877ba1cf1772b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/34e13aaf910833549bc877ba1cf1772b.jpeg)
![images/183ad66316b928090fc34e61bf696bbd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/183ad66316b928090fc34e61bf696bbd.jpeg)
![images/4d0acdcf5a10b8a60a229b49ab2c9d7e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4d0acdcf5a10b8a60a229b49ab2c9d7e.jpeg)
![images/2ef839f88f16c04bb0693aa38ee02a2d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2ef839f88f16c04bb0693aa38ee02a2d.jpeg)
![images/9eea50e55e356c816e9f25d75c2db6bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9eea50e55e356c816e9f25d75c2db6bd.jpeg)

**"Steve Clark"**

---
---
**Don Kleinschnitz Jr.** *April 20, 2017 11:58*

Thats a novel approach :)


---
**Stephane Buisson** *April 20, 2017 15:43*

with 2 laser point  (parallel X &Y, not crosshair), you don't need to burn to find if you are in focus. the intersection of the 2 rays show the focus point, so you can adjust your material up or down until you find a unique dot visible on it. this have been described here earlier.


---
**Steve Clark** *April 20, 2017 17:36*

Stephane, I looked for the earlier post you mentioned but can't seem to find it. Can you give a link or some key words? I understand your description about the two beams coming to a point... so my little pointer is just a tiny bit more complex to use. 



I approached it from this path because I did not have or found a cheap laser diode with a fine enough beam to accurately do what you described. 




---
**Stephane Buisson** *April 20, 2017 22:26*

**+Steve Clark** finally found it



[plus.google.com - I just finished the upgrading of my K40 Laser cutter with following topics: b...](https://plus.google.com/117750252531506832328/posts/HFneYiu57vP)



linking to



[http://johann.langhofer.net/lasercutter/](http://johann.langhofer.net/lasercutter/)


---
**Steve Clark** *April 20, 2017 23:09*

Great! Thanks Stephane. I'll look at it tonight.


---
**Steve Clark** *April 28, 2017 01:33*

Just a follow up and  a better picture of the cross hairs. I've used it for a  a week now and love the way it works! I can't knock using two beams but this one for the time I spent making it works very well. I just place a little piece of paper on top of the workpiece turn it on and make the dot then move the table up or down to center. 

The picture of the laser cross hair also shows some proto parts I made after the first was checked out, I just quickly jogged the cross hair over not bothering to go back and write in a step over repeat...I was done with them by the time it would have taken me to do the 10 pieces I needed.  

![images/7e906ca70db24b79227f31e5f4cfd09d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e906ca70db24b79227f31e5f4cfd09d.jpeg)


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/Vd5h8KtdMtM) &mdash; content and formatting may not be reliable*
