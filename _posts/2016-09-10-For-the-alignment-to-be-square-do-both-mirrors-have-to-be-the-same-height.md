---
layout: post
title: "For the alignment to be square do both mirrors have to be the same height?"
date: September 10, 2016 23:33
category: "Discussion"
author: "Robert Selvey"
---
For the alignment to be square do both mirrors have to be the same height? 





**"Robert Selvey"**

---
---
**greg greene** *September 10, 2016 23:58*

yes, the bean needs to hit as close as possible to the center of the mirrors, ans parallel to the travel of the head in both directions, so the mirrors have to be at the same height


---
**Robert Selvey** *September 11, 2016 21:03*

Got the mirrors at the same height from front to back on the left side the beam hits the center of the lens but when I move it to the right it become way off I have been on this for about 15+ hours really need help.


---
**greg greene** *September 11, 2016 21:29*

OK, first step - have you aligned them like it says at this web site, [theflyingwombat.com](http://theflyingwombat.com)?

The mirror closest to the laser is #1.  Check to make sure the beam from the laser hits it dead centre - if not - ADJUST THE BEDDING OF THE LASER till it does.



Getting the beam to hit the centre of the mirrors is absolutely crucial - that is the only way the reflected beam will not come off at an angle.  



The mirror that travels on the rail is mirror #2, check the beam strike when it is closest to #1 and furthest away, adjust the strike by adjusting mirror #1

When you get that done you need to adjust the strike onto the mirror in the head, start with mirror #2 close to Mirror #1 and the head close to mirror #2, then move the head to right - furthest away from mirror #2 and adjust the beam strike by adjusting mirror #2, don't worry if the strike is not precisely centred on the opening in the head - it is more important that the beam strike the same spot at the head while the head is in all four corners of the bed.  You may need to check that the rails are straight and the head travels freely on them.



When you have the beam hitting the same spot on the head in all four corners THEN adjust the HEAD so the beam strikes the centre of the opening in the head.

Here's some tips I've found usefull



Pickup a package of alcohol swabs from the drugstore to clean the mirrors with - especially if you use painters tape to see the beam strikes as the heat will melt some of the adhesive onto the mirror - scattering the beam.



Use the smallest strength required to get a mark on the tape - this is the centre of the beam - too strong a beam and the hole burned becomes too big to really fine tune with.



Observe the tape as the beam strikes it using a flashlight to see with AND THE LID CLOSED - NEVER FIRE THE LASER WITH THE LID UP - you will have a real hard time finding replacement eye balls !!!



Small steps are best, if the beam is way off on the furthest point, try aligning with the mirror halfway along.



Patience and repetition will get you there !!!



good luck !!!


---
**Robert Selvey** *September 12, 2016 01:38*

That website is about quad copters not laser alignment ?


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/YdUrUYxrRmQ) &mdash; content and formatting may not be reliable*
