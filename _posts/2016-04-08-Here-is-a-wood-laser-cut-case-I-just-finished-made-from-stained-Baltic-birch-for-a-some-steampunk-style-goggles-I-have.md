---
layout: post
title: "Here is a wood laser cut case I just finished made from stained Baltic birch for a some steampunk style goggles I have"
date: April 08, 2016 07:08
category: "Object produced with laser"
author: "Vince Lee"
---
Here is a wood laser cut case I just finished made from stained Baltic birch for a some steampunk style goggles I have.  I've got some CO2 laser rated plastic coming in to cut new lenses for the goggles so i can look ridiculous when laser cutting.



![images/d06400bd54ac49cbfb1e81b6c18b1ce2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d06400bd54ac49cbfb1e81b6c18b1ce2.jpeg)
![images/4cb6cee48c8c6726cd761ff1e60084a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4cb6cee48c8c6726cd761ff1e60084a9.jpeg)
![images/650b01f1549ed9949cf66ba64f811dfc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/650b01f1549ed9949cf66ba64f811dfc.jpeg)

**"Vince Lee"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 08:48*

Nice design, looks pretty awesome too. I like the idea of making some steampunk googles, where did you order the CO2 laser rated plastic from?


---
**Scott Thorne** *April 08, 2016 12:44*

**+Vince Lee**...gray job man...looks nice


---
**Vince Lee** *April 08, 2016 14:40*

**+Yuusuf Sallahuddin** The goggles were a gift and came with plain glass lenses. I got the laser window from [noirlaser.com](http://noirlaser.com). a little pricey for just plastic but cheaper than other places. I'll probably have leftovers if u plan to make your own.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 14:59*

**+Vince Lee** Is it the 10600nm stuff like this [http://noirlaser.com/ecw125.html](http://noirlaser.com/ecw125.html)? I notice their "filter" ability doesn't allow you to filter for anything higher than 2000nm, but they do have some products that supposedly filter for 10600nm.



If it's 10600nm stuff, I'd love to get a small amount of it from your leftovers (I'd be happy to pay what it's worth to you). Depending  where you're located though it might work out ridiculous for shipping costs. I'm in Gold Coast, QLD, Australia.



I like the idea of doing some steampunk style laser goggles with leather & laser protective lenses.


---
**Vince Lee** *April 09, 2016 06:55*

**+Yuusuf Sallahuddin** It's supposed to arrive this weekend, so I can confirm when I get it that I ordered the right thing.  If it's good I can send you a piece for a pair of lenses for a small amount ($5?) plus shipping.  I can check later what the shipping might be.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 09, 2016 07:10*

**+Vince Lee** Yeah that sounds great. Just let me know when the time comes & we'll sort it out. Thanks again.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 15:57*

**+Vince Lee** I just had a thought/query regarding this material. Can you actually cut it with the laser? Or does it totally block the beam?


---
**Vince Lee** *April 12, 2016 18:18*

**+Yuusuf Sallahuddin** I just got it this weekend.  I couldn't get the laser to cut all the way through even after a dozen passes.  When I previously tried to cut some polycarbonate it generated similar stringy ash so it's definitely not just acrylic.  As such it is great for safety glasses.  It cut easy enough on a band saw and is rated with a 5+ optical density for 10600nm.  I can send you some, though is there anything else you need to make it worth the shipping?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 18:52*

**+Vince Lee** You mentioned the middleman board for the Smoothieboard conversion. I am planning on getting the 5XC Smoothieboard (when I have the $ saved for it). Is the middleman board compatible with that? & If so, what sort of price are we looking at for that?



Good to hear that the stuff doesn't cut with the laser-cutter. Perfect for safety glasses as you said. I'm going to have to hold off on getting it until Monday week (18th April) as I don't get paid again until then.



Thanks again.


---
**Vince Lee** *April 14, 2016 03:33*

**+Yuusuf Sallahuddin** the cost of the middleman board is free (just shipping) as it is just lying around.  The middleman board just breaks out the lines from an ffc ribbon cable connector into individual pads so they can be soldered to easily.  With it one still has to figure out how to connect everything together.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 03:38*

**+Vince Lee** Awesome, that is great. Well combined together with some leftover of your lens material + the shipping, I'm happy to go for that. Thank you very much. If you can get a full quote together for the shipping of the lens material + the middleman + the cost, I can arrange a PayPal (or Stripe) transfer on payday (Monday next week).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2016 01:49*

**+Vince Lee** Payday has arrived :) Contact me on my email when you've got time (yuusuf dot sallahuddin at gmail dot com) to discuss postage/funds transfer.


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/WXTwkmYYeNY) &mdash; content and formatting may not be reliable*
