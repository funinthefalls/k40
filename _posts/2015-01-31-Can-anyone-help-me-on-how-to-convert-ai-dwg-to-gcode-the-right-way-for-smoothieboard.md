---
layout: post
title: "Can anyone help me on how to convert ai/dwg to gcode the right way for smoothieboard?"
date: January 31, 2015 06:31
category: "Discussion"
author: "Joshua Taft"
---
Can anyone help me on how to convert ai/dwg to gcode the right way for smoothieboard? I tried Cut2D but it's gcode(mm) doesn't have the S code for the laser pwm. Anyone know a better way? I just want to do a test run to see if my steppers are setup correctly (steps per rotatation to mm and all that). however i would like something that is easy to use for everyday engraving as well.





**"Joshua Taft"**

---
---
**Stephane Buisson** *January 31, 2015 09:54*

Sorry I couldn't help with Smoothiboard yet, I am waiting Thomas Oster development of Visicut for smoothie to mod my K40. See **+Thomas Oster** entry in Smoothieboard contest.


---
**ThantiK** *January 31, 2015 13:57*

Man, Visicut support for the smoothie would be superb!


---
*Imported from [Google+](https://plus.google.com/+JoshuaTaftToonamo/posts/e11VCUoTPUB) &mdash; content and formatting may not be reliable*
