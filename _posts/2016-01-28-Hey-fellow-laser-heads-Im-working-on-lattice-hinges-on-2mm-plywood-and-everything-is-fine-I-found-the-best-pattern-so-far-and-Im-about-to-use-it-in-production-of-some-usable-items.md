---
layout: post
title: "Hey fellow laser heads! I'm working on lattice hinges on 2mm plywood and everything is fine, I found the best pattern so far and I'm about to use it in production of some usable items"
date: January 28, 2016 11:54
category: "Materials and settings"
author: "Tadas Mikalauskas"
---
Hey fellow laser heads! I'm working on lattice hinges on 2mm plywood and everything is fine, I found the best pattern so far and I'm about to use it in production of some usable items. I was thinking of reinfocing the sheet by cutting the same pattern of some kind of very thin but strong plastic and glueing it on the inside part, because what I want to craft has to be able to constantly move, so any stronger bend and it will snap. I found a perfect material but it is PVC and we all know we cant cut it... I'm not sure if I was clear of what I want to do but maybe some of you have a suggestion of a material that could be cut by laser and does more less the same as the PVC sheet (like in a photo).

![images/7db7f4034c11a0a25ae952879e7f86e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7db7f4034c11a0a25ae952879e7f86e1.jpeg)



**"Tadas Mikalauskas"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 28, 2016 13:17*

Could you use some sort of fabric maybe?


---
**Tadas Mikalauskas** *January 28, 2016 13:37*

You see, my idea is that when I bend them both glued in one piece, the first that will give the resist and stop bending process would be the plastic so the part wont reach the point where the plywood will snap. So I think fabric will not do the part..?


---
**David Cook** *January 28, 2016 14:31*

I use polypropylene sheet at work and it's very resiliant. Can get it in .010" thickness.   The stuff I use is called Formex GK-10 can get it in black or white. And it can be laser cut. 


---
**Tadas Mikalauskas** *January 28, 2016 14:39*

David, I think that this might be my solution! I will look it up and let you know if it worked :)


---
**David Cook** *January 28, 2016 16:04*

**+Tadas Mikalauskas**  There is also 0.005" thick material  Formex GK-5    it's very resiliant and can pretty much bend back and forth the life of the product.  great for living hinges etc.   I use it as an electrical insulator for power conversion products we produce.   you can get the 0.010" thick sheets here   [http://electrical-insulation.espemfg.com/viewitems/formex-gk-sheets/formex-gk-10-2](http://electrical-insulation.espemfg.com/viewitems/formex-gk-sheets/formex-gk-10-2)



the 0.005" thick product comes on rolls and has a higher min buy   



[http://electrical-insulation.espemfg.com/viewitems/formex-gk-rolls/formex-gk-5bk](http://electrical-insulation.espemfg.com/viewitems/formex-gk-rolls/formex-gk-5bk)?


---
**Tadas Mikalauskas** *January 28, 2016 17:41*

I think I found some in my country :) just one question: there are two types: PP-H and PP-C what is the difference between them? which is better to use?


---
**David Cook** *January 28, 2016 18:53*

I was not able to find out which type the Formex material is that I use.  butI did find a website that describes the difference,  based on that I assume that the Formex is type -C



Also  I typically have a layer of 0.003"  3M  VHB  PSA applied to one side  ( PSA = pressure sensitive adhesive)


---
**Tadas Mikalauskas** *February 02, 2016 10:32*

Hey David, I tried working with PP all seems to be fine, but I have a hard time sticking it with wood  since it can't be glued.. Didnt try heating it on the wood, but dont have ability to do it for now...


---
*Imported from [Google+](https://plus.google.com/113213933687083540785/posts/PgMk1ojmgjL) &mdash; content and formatting may not be reliable*
