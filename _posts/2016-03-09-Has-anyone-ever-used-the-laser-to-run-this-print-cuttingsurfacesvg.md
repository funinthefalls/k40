---
layout: post
title: "Has anyone ever used the laser to run this print, cuttingsurface.svg?"
date: March 09, 2016 06:44
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
Has anyone ever used the laser to run this  print, cuttingsurface.svg? I just did for first time and all seemed ok but laser would not fire and the g7 code is all new to me I have never seen this before. I recently did a cmd install of the inkscape plugin which allowed for all errors I was getting to stop. Now I am not sure what I need to do to fix this issue and again I have never seen it before now due to improper install of inkscape plugin, which recently I resolved. Here is some of the code, the plugin generated  as test of cuttingsurface.svg file More testing proves I can still vector just fine, but anything raster in nature will move the laser to positions however will not fire laser. I have no idea why other than looking at this code example below shows laser fire command as M649 S10 B2 D0 R0.09406, not sure what that means. My understanding was M03 S is fire command? Please any help here would be awesome and did post this earlier in the week under software discussion and got nothing. I do know there is another member having the same issues, so any help would be welcomed.



G21 ; All units in mm

M80 ; Turn on Optional Peripherals Board at LMN



; Raster data will always precede vector data           

; Default Cut Feedrate 300 mm per minute

; Default Move Feedrate 4000 mm per minute

; Default Laser Intensity 30 percent

G28 XY; home X and Y



M5 ;turn the laser off



;(<b>********************************************************</b>)

;(<b>*</b> Layer: 10 [ppm=30,feed=2500]                     <b>*</b>)

;(<b>*</b> Laser Power: 10                                  ***)

;(<b>*</b> Feed Rate: 2500.0                                ***)

;(<b>*</b> Pulse Rate: 30.0                                 <b>*</b>)

;(<b>********************************************************</b>)

;(MSG,Starting layer '10 [ppm=30,feed=2500]')







;Beginning of Raster Image image4556 pixel size: 753x750

M649 S10 B2 D0 R0.09406

G0 X41.053 Y98.146 F2500



G7 $1 L68 DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAAAAAAAAAAAAMHCiA3Tl9xg42XoqOlp6CZkoZ5bFpINSQTAQEAAAAAAAAA

G7 L68 DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAhMkNUda

G7 L28 DbHmGkpmgp6WjopiNg3FfTjchCwcD



G7 $0 L68 DBQoQMVN1kKvG1eT09vn78efcybajiGxQNx0DAgEAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQIDHTdRbIikt8rd5/H8+fb05NXGq5B2

G7 L56 DUzEQCgUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=



G7 $1 L68 DAQMFFCQ0MzEwIBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACRIcMERYcImitMbY4u33+fv8

G7 L68 D9u/o287Br5yKdV9KOSkYEAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAACBAYKTpKX3SJnK/Bztvn7vX8+/n37ePYxrSiiXFYRDAcEgkAAAAAAAAA

G7 L36 DAAAAAAAAAAAAAAAAAAAAECAwMjM0JRUFAwE=



G7 $0 L68 DAwcMK0tqZ2ViQSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEiU5W3+iscHQ2eLr8fb7/P39

G7 L68 D+vbz7ebg183EtKOTc1IyIRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAAECExUnOTo7TEzdfg5u3z9/r9/fz79vDr4tnQwbGif1s5JRIAAAAAAAAA

G7 L36 DAAAAAAAAAAAAAAAAAAAAIUJiZGdqSioLBwM=



G7 $1 L68 DBQsRP2+gnJiUYzIBAAAAAAAAAAAAAAAAAAAAAAAAAAAAHDlWh7rt8vj/////////////

G7 L68 D////////////8+fcrHtLMhkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

G7 L68 DAAAAAAAAAAAAGDJLe6zb6PP+////////////////////////+PLsuodWORwAAAAAAAAA

G7 L36 DAAAAAAAAAAAAAAAAAAAAMWOTmJyfcUESCwUA﻿





**"Andrew ONeal (Andy-drew)"**

---


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/U5zfCqs4Am7) &mdash; content and formatting may not be reliable*
