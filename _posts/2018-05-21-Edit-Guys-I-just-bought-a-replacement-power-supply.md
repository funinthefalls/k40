---
layout: post
title: "Edit: Guys, I just bought a replacement power supply"
date: May 21, 2018 23:48
category: "Modification"
author: "Chuck Comito"
---
Edit: Guys, I just bought a replacement power supply. I found this image done by Jason Farell and wanted to make sure I'm ok following it. The only difference is that I don't run the m2nano but instead run a smoothie. The lo would be my pwm output but otherwise I think it's ok. Any thoughts??

![images/ff396e3e7bdabef4cf2caf24f2090e3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff396e3e7bdabef4cf2caf24f2090e3a.jpeg)



**"Chuck Comito"**

---
---
**Don Kleinschnitz Jr.** *May 22, 2018 00:13*

This was done by me for Jason lol. 


---
**Chuck Comito** *May 22, 2018 00:49*

**+Don Kleinschnitz**  it figures!! Lol. But.. is it still right. My data sheet for my power supply says to use the IN for the pwm signal. I thought I'd test both ways. 


---
**Justin Mitchell** *May 22, 2018 11:05*

I got the impression that the PWM signal either went to the IN pin, or if you wanted to combine them replaces the 5V line to the potentiometer 


---
**Chuck Comito** *May 22, 2018 11:23*

It's currently connected like shown and the controller fires the laser. I'm not sure if the pwm is controlling the power or not because I did a short test and wrapped it up for the night. I'll experiment again tonight and hopefully nail it down. 


---
**Don Kleinschnitz Jr.** *May 22, 2018 12:08*

**+Chuck Comito** the power supplies specs often say to connect to the IN. This method is ok if you use the right kind of driver for your purpose:

An analog voltage proportional to the power control needed and from 0-5V.

or

Direct pwm connection with a 0-5VDC digital signal and you don't care to control the power locally.



Most of us use the L pin to PWM and the pot on IN for manual power control. This will result in power control that TOTAL DF= DF(pot)*DF(pwm).

The schematic above will provide that kind of control if the TL pin is connected to the smoothie using the open drain method and the IN is connected in the conventional method to the POT. The 5V is unnecessary and the 24V comes from a standalone supply.

Was your PWM connected to L on the last supply?



**+Justin Mitchell** it is possible to put the pot and pwm in the same circuit. It results in the same TOTAL DF but leaves out the ability to control the laser without PWM on [such as test firing]. This could be solved using a switch [that can inadvertently be left on]. All in all I found it easier to use the IN with a pot and L or TL signals with a smoothie open drain.



DF= duty factor

TL = L

TH =-L




---
**Chuck Comito** *May 22, 2018 14:55*

Hi **+Don Kleinschnitz** , yes the pwm was connected to L previously. I'd like to maintain the ability to control the power via software as I did with the stock k40 supply. 


---
**Chuck Comito** *May 22, 2018 20:04*

Well, **+Justin Mitchell** and **+Don Kleinschnitz**, the schematic I posted (Don posted) seems to work and give me control of the laser power. It's pretty nice having full control of a 60 watt tube!!




---
**Don Kleinschnitz Jr.** *May 23, 2018 12:18*

**+Chuck Comito** 👍👍👍


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/f46v8j7vB7c) &mdash; content and formatting may not be reliable*
