---
layout: post
title: "I have been messing around trying to etch stainless steel but haven't had much luck"
date: April 30, 2017 23:02
category: "Object produced with laser"
author: "Martin Dillon"
---
I have been messing around trying to etch stainless steel but haven't had much luck.  It will look good but won't really stand up to a real good scrubbing.  I think surface prep might have a lot to do with getting a good contrast.  I am going to try again and be a little more scientific in how I do my tests.  

The dark etchings are just cleaned with alcohol (I find it works better than acetone). The lighter etchings are after I scrub with soap and a scotch bright pad.



![images/594c5014e4137c6fb7ede78a860ebc8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/594c5014e4137c6fb7ede78a860ebc8c.jpeg)
![images/a7bdd560f1e9667908b17e8b22c119fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a7bdd560f1e9667908b17e8b22c119fc.jpeg)
![images/922e69ab746782c84b108f774307768f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/922e69ab746782c84b108f774307768f.jpeg)

**"Martin Dillon"**

---
---
**Steve Clark** *May 01, 2017 01:31*

What laser are you using and are you coating the stainless with anything?



I just did a recent post on engraving number and letters on stainless. If I can figure out how to link it I will.

Here is what I coated the stainless with.

[ebay.com - Details about  CRC 03084 Net Weight: 11 oz. 16oz Dry Moly Lubricant Aerosol Spray](http://www.ebay.com/itm/CRC-03084-Net-Weight-11-oz-16oz-Dry-Moly-Lubricant-Aerosol-Spray/282331750428?_trksid=p2060778.c100290.m3507&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20160727114228%26meid%3Db62ef432c8a146908374b4d2de560273%26pid%3D100290%26rk%3D2%26rkt%3D4%26sd%3D172538546543)



It was just two days back and there is a picture...


---
**Martin Dillon** *May 01, 2017 04:35*

I am using a K40 and the same dry moly you list above




---
**Sebastian Szafran** *May 01, 2017 07:22*

Check this link: 
{% include youtubePlayer.html id="jP9ncWksEGw" %}
[https://youtu.be/jP9ncWksEGw](https://youtu.be/jP9ncWksEGw) 


---
**Sebastian Szafran** *May 01, 2017 07:23*

Check this link: 
{% include youtubePlayer.html id="jP9ncWksEGw" %}
[https://youtu.be/jP9ncWksEGw](https://youtu.be/jP9ncWksEGw) 


---
**Sebastian Szafran** *May 01, 2017 07:23*

Check this link: 
{% include youtubePlayer.html id="jP9ncWksEGw" %}
[https://youtu.be/jP9ncWksEGw](https://youtu.be/jP9ncWksEGw) 


---
**Sebastian Szafran** *May 01, 2017 07:24*

Did you see the link below?


{% include youtubePlayer.html id="jP9ncWksEGw" %}
[https://youtu.be/jP9ncWksEGw](https://youtu.be/jP9ncWksEGw)


---
**Steve Clark** *May 01, 2017 19:13*

Very interesting this last utube of Sebastin's is exactly what I'm seeing with engraving under the microscope as. I'll see if I can post a  picture. Martin, the only difference I can see is I'm using a different aftermarket lens a ZnSe- F50.8 mm.



[lightobject.com - Improved 18mm ZnSe Focus lens (F50.8mm)](http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx)


---
**Steve Clark** *May 01, 2017 20:34*

Martin and all, here is a microscopic view of the the lettering with CRC used. It's about 3.8mm tall. After RDworks lab 100's post It I get time today I'll try it without the coating.



I'll also revisit the power settings and feed rate.

![images/6f8bb6a079cb58543a748f41beceb4c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6f8bb6a079cb58543a748f41beceb4c9.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 02, 2017 00:48*

I used a product called Minehan Circle Spray (got in Australia, [http://www.minehanagencies.com.au/lubricants.html#circlespray](http://www.minehanagencies.com.au/lubricants.html#circlespray)). This has a significantly higher % of MoS2 compared to the CRC spray I see a lot using here (something like 90% compared to 8%). I found that almost all of my etchings onto stainless with this stuff came out quite dark (@around 10mA power & 200mm/s). Also, when cleaning up I literally just washed the excess off with water & a sponge & a little elbow grease.



edit: I forgot to mention that the etchings have held up pretty well over time too. Can't even scrub them off with a scourer/scotch pad. One, on a stainless bowl rim, has been through the dishwasher about 100 times too & is still visible & reasonably dark.

[minehanagencies.com.au - Minehan Agencies](http://www.minehanagencies.com.au/lubricants.html#circlespray)


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/Rbk9ygSqFvR) &mdash; content and formatting may not be reliable*
