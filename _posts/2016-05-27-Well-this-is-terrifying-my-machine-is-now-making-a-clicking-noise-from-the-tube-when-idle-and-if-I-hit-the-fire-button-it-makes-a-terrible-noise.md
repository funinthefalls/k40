---
layout: post
title: "Well this is terrifying my machine is now making a clicking noise from the tube when idle and if I hit the fire button it makes a terrible noise"
date: May 27, 2016 23:16
category: "Discussion"
author: "Derek Schuetz"
---
Well this is terrifying my machine is now making a clicking noise from the tube when idle and if I hit the fire button it makes a terrible noise. Any clue as to what it is





**"Derek Schuetz"**

---
---
**Scott Marshall** *May 28, 2016 02:43*

It's probably arcing. Don't do it anymore.



You didn't run it with the water pump off by any chance? It's a common cause of tube failure, which is a common cause of arcing.



If the tube is still good, you need to clean and insulate with RTV the high voltage terminal (the big wire from the power supply to the laser tube)  Continued arcing can crack the tube or damage the power supply (the other possible source of the arcing).



Search "CO2 Laser Repair"


---
**Derek Schuetz** *May 28, 2016 02:58*

**+Scott Marshall** water is always running in the tube I don't see any sign of damage tot he tube. What I did notice is the poping while idle stops when I unhook the signal wires from the ramps board


---
**Scott Marshall** *May 28, 2016 03:11*

Un-hooking the firing signal would stop the tube from firing.



It's possible the noise you are hearing is one of the cog belts jumping. Is the carriage trying to move?  Maybe you have a RAMPS board or driver issue and a motor is running amuck?  The sound of the drive jumping also fits your description of the sound, but it's usually obvious as the carriage will be against a stop trying to advance. None of that would be related to the "test fire"  which you said was causing the noise. I'm still betting on arcing.



If you live in the US, particularly the northeast, it's been hot and humid, perfect conditions for arcing. If you over chill the water you can get condensation on the tube.



Try firing the tube with the laser tube door open and the lights out.. (be careful high voltage etc in there) Look to see if you can any lightning bolt type of arcs accompanied with the snapping noise. If so there's where you need to clean/insulate. If you can see the tube glow, that's a good sign, it means it's got a chance of being good.



I'm afraid your symptoms usually end in a bad tube, but not always. It could be just a damp/dirty high voltage connection or bad power supply.



If you don't see arcing on the tube, and the sound seems muffled, it's probably in the power supply. Open the right side panel and repeat the test , this time examining the power supply.



Good luck.


---
**Derek Schuetz** *May 28, 2016 03:31*

Well I have confirmed no laser is coming out. I can do a test to see if I see an arc tonight. Can you send a how to clean and insulate


---
**Scott Marshall** *May 28, 2016 04:41*

Best to clean is paper towel and a non-waterbased solvent. 100% alcohol like Denatured alcohol or "Shellac Thinner " as it's sometimes sold. I've heard it's called "mentholated spirits" in the UK and Australia.

Lacquer thinner or MEK, Toulene or Acetone will all work fine, as will Carb Cleaner or Brake Cleaner. Or lighter fluid (Naptha). Just put onto the paper towel and wipe the tube around the high voltage connection well. Once cleaned, apply 100% Clear RTV Silicone rubber. Don't get the Silicone caulk which has the RTV diluted with latex (water based) caulking. The squeeze tube of clear Silicone gasket maker, sold as GE, Fel-Pro, Permatex, and I'm sure others (GE invented it 30+ years back)

Apply a 1/4 -3/8" thick coating to the Highvoltage terminal and an area about 1 " around it. apply it evenly, with as few bubbles in it as possible.

I worked at a TV repair shop years ago, and the summer time this was a 10 per day repair to the high voltage CRT terminal thru the humid season. The static charge draws smoke, cooking grease, anything airborne to the glass (it's the smoke products in these lasers, my tube is gray) then, when it gets humid, the moisture infiltrates the dirt layer, making it conductive, and the arcing starts. If let go, it will break the glass.



Often on these lasers, the tube looses it's gas, either from a slow leak at the terminal penetrations or from thermal shock. A common senario is when someone starts their laser up, it starts firing, then they realize a few seconds later they didn't power on the water pump, and quickly plug it in, only to have the cold water hit the now ripping hot glass and it's all over.



Anyways, once the gas leaves the tube, the electricity no longer has an easier path thru the gas, so takes the now easiest path to ground which is thru the air on the OUTSIDE of the tube. Thus the arcing.



The slow leak tube death is pretty common. Science has worked since Edisons day to develop a metal alloy that has the exact same coefficient of expansion as the glass used in light bulbs, electron tubes, and, Yes, Laser Tubes. Where metal passes thru glass, micro cracks form each time you heat cycle the tube because the metal and glass expand at different rates. This was so severe that back in the 60's a lot of expensive gear like optical comparators and transmitting tubes were kept on 24/7 as they lasted a lot longer that way.

The trick is get the expansion rate of the wire matching the expansion rate of the glass.  It's been done quite well, but is a trade secret of most tube manufacturers, hence these Chinese tubes don't use the best and greatest sealing methods, and as a result offer spotty at best reliability. Some folks get years out of them, but it's more common to get a year or so, and some unfortunate people get a few days or weeks.



Probably more then you wanted to know. I tend to run on about such stuff. Anyway, now you know the whole story. Hope it helps.



I'm hoping for you, you're one of the 10% or so that just has a dirty tube surface.


---
**Pippins McGee** *May 28, 2016 11:57*

**+Scott Marshall** hi scott, just out of curiousity where does one buy replacement  (chinese or better) tubes for the k40?


---
**ThantiK** *May 28, 2016 13:27*

Sounds like your tube is cracked.  You've gotta make sure to get all the bubbles out of it, keep it properly cooled, etc.  Otherwise you can develop a hot spot and damage the tube.


---
**Derek Schuetz** *May 28, 2016 14:46*

There's no visible cracks in the unit and I have a full 5 gallon bucket resovoir pumping water continuously. How do you get all the bubbles out I had a small one that disappeared over time 



Also part of me thinks the PSU is bad because it makes s crackling noise while idle but only if the d5 wire is connected to the PSU from the ramps board


---
**Derek Schuetz** *May 29, 2016 00:42*

Situation has gotten worse...now just with the machine sitting idle when laser safety switch engaged it is arcing randomly 



I measured the d5 pin on my ramps board and it's 5v idle when I send a m03 command it drops to 3.2v this seems weird to me can someone explain this pin and how it works


---
**Scott Marshall** *May 29, 2016 13:01*

**+Derek Schuetz** Never heard that one before. On those ramps opto-solated breakout boards you can use any pin for any function, you 'll have to trace it out if you didn't build it, or didn't keep records. There should be a parallel port pin map with the software you're using describing pin d5's connection.

Most of the drivers (like Mach 3) have user definable pinouts. If you bought it ready to run, the builder should have included the pin mapping.

All the logic signals should be well above 4.5 or well under 1v. 3.2v sounds like 3.3V logic high, which would be normal for Arduino drivers and if that's the case should never go to 5V.

It's confusing,...



I know of nothing you could do from the computer should be able to cause arcing, especially "random" arcing.

The ONLY thing I can think of is that your power supply is arcing internally, but even that doesn't make a lot of sense, because MOST high voltage supplies don't idle with HV on, they switch the low voltage side of the final stepup transformer to control the output.



By the way, I answered Mr McGee and commented on the tube situation yesterday, but it seems I must have not hit save.



Regarding the water not leaking, I don't think anyone has ever had an actual water leak with a bad tube (except those damaged in shipping). I'm pretty sure most tube failures are simply a gas leak from cracking around the wiring passthroughs, and those seem to be invisible. We've examined a lot of tube photos here, and I can't remember even one where you could say for sure where the leak was.



As far as buying a new tube goes, Mr McGee, I can't tell you exactly where the best place would be. There's several people here with good experiences (and some with bad) on tube buys, I'll let them chip in. If you search the past few months traffic, you find several discussions on the subject.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/YvCAeG8YYeY) &mdash; content and formatting may not be reliable*
