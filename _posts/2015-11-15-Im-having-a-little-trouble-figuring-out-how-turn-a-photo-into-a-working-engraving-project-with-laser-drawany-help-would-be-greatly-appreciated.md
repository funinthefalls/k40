---
layout: post
title: "I'm having a little trouble figuring out how turn a photo into a working engraving project with laser draw...any help would be greatly appreciated"
date: November 15, 2015 15:10
category: "Discussion"
author: "Scott Thorne"
---
I'm having a little trouble figuring out how turn a photo into a working engraving project with laser draw...any help would be greatly appreciated.





**"Scott Thorne"**

---
---
**Coherent** *November 15, 2015 16:21*

Basically you just convert it to black and white and increase the contrast. Dithering helps. Do a google search for how to engrave a photo with a laser. You tube has a few specifically using corel or photoshop. After that it's just asjusting your machine speed & power to get the wanted results. Here's a couple links but there are others, some better.


{% include youtubePlayer.html id="yllZTBA0HO0" %}
[https://www.youtube.com/watch?v=yllZTBA0HO0](https://www.youtube.com/watch?v=yllZTBA0HO0)


{% include youtubePlayer.html id="H2ULldN28V8" %}
[https://www.youtube.com/watch?v=H2ULldN28V8](https://www.youtube.com/watch?v=H2ULldN28V8)
 


---
**Scott Thorne** *November 15, 2015 17:10*

Thanks Marc G.


---
**Imko Beckhoven van** *November 15, 2015 17:52*

Change the color index to two, that way you get only black and whit pixels (dithering) 


---
**I Laser** *November 16, 2015 03:55*

This was posted recently [http://www.instructables.com/id/Enhance-Your-Image-when-Reverse-Laser-Engraving-on/?ALLSTEPS](http://www.instructables.com/id/Enhance-Your-Image-when-Reverse-Laser-Engraving-on/?ALLSTEPS)


---
**Julia Ju (bodor laser)** *November 16, 2015 05:09*

graphic format supported : DST,PLT,BMP,DXF,DWG,AI, LAS,etc.

compatible software:Coreldraw,photoshop,autocad, tajima,etc. 

for bodor laser cutting and engraving machine 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/gABpH8hafxU) &mdash; content and formatting may not be reliable*
