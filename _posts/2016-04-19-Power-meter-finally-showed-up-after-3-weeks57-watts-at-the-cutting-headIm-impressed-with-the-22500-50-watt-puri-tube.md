---
layout: post
title: "Power meter finally showed up after 3 weeks...57 watts at the cutting head...I'm impressed with the 225.00 50 watt puri tube"
date: April 19, 2016 23:35
category: "Discussion"
author: "Scott Thorne"
---
Power meter finally showed up after 3 weeks...57 watts at the cutting head...I'm impressed with the 225.00 50 watt puri tube.

![images/11e84cfd8f59cefbca6c484e223be284.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/11e84cfd8f59cefbca6c484e223be284.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 19, 2016 23:53*

Nice. Better late than never.


---
**Anthony Bolgar** *April 20, 2016 00:40*

How much is a power meter like that one?


---
**Scott Thorne** *April 20, 2016 00:48*

95.00 at bell laser


---
**Phillip Conroy** *April 20, 2016 10:57*

What ma was that on? I cut 3mm mdf on 10ma=15watts  at 7.5mm/sec 


---
**Phillip Conroy** *April 20, 2016 10:58*

How many second test


---
**Scott Thorne** *April 20, 2016 11:39*

That was 30 seconds @ 18mA ...the max for my tube...it was under the head ...I haven't taken a reading at the tube yet....that was with a 20 second cool down time also. 


---
**Phillip Conroy** *April 20, 2016 12:08*

I hope u didnot have the focas lens in as instructions say unfocased beam


---
**Scott Thorne** *April 20, 2016 12:09*

I can cut 6mm ply or acrylic at 40% power which is 10 mA's at 10mm/s...that's with the new lens I bought and paid out the kazoo for, but it was worth it **+Phillip Conroy**.﻿


---
**Phillip Conroy** *April 20, 2016 12:13*

Nice- i havent tryed ply yet,i must measure laser outputs at diffrent temp of water to find the sweat spot


---
**Phillip Conroy** *April 20, 2016 12:18*

I have only a cheap $250 laser tube and would have about 400 hours on it in just over a year s,my engine hour meter that picks up a signal by wire wraped around the hi voltage wire does not work on my replacement power supply so only guessing tube hours


---
**Scott Thorne** *April 20, 2016 13:21*

**+Phillip Conroy**...lol...that was with the lense out. 


---
**Scott Thorne** *April 20, 2016 13:35*

Aka...the Donut of Power...lol


---
**Todd Miller** *April 27, 2016 22:35*

I just received my 100w power meter but the needle is pointing straight down.  I figured it would be closer to zero than this.  Where was your needle sitting when you took yours out of the box ?


---
**Scott Thorne** *April 27, 2016 22:47*

It's going to go by the ambient air temp...if it's cool where you are then that's the case....at around 80 degrees it should be close to zero **+Todd Miller**.


---
**Todd Miller** *April 27, 2016 22:54*

it's cold and raining out, it was 70 at work when I opened it up and it's 67 here in the basement.

I'll zero it and try it on my 40w later tonight.


---
**Todd Miller** *April 28, 2016 01:19*

I got 35w @ 15ma, so I guess it's close.

I have a Reci S2 80W to arrive in a week.

Will be interested in what that reads.


---
**Scott Thorne** *April 28, 2016 23:16*

**+Todd Miller**...what are you installing an 80 watt tube in?


---
**Todd Miller** *May 02, 2016 21:57*

I'm building a 900 x 600 table using LightObject parts.  Sorry for the shaky video;




{% include youtubePlayer.html id="9MHasIukDmA" %}
[https://youtu.be/9MHasIukDmA](https://youtu.be/9MHasIukDmA)


---
**Scott Thorne** *May 02, 2016 22:21*

**+Todd Miller**...damn good job man...I'm seriously impressed.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/G4BjMow9Tma) &mdash; content and formatting may not be reliable*
