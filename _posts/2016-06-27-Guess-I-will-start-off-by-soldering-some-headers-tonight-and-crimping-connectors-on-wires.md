---
layout: post
title: "Guess I will start off by soldering some headers tonight and crimping connectors on wires"
date: June 27, 2016 02:07
category: "Smoothieboard Modification"
author: "Alex Krause"
---
Guess I will start off by soldering some headers tonight and crimping connectors on wires 

![images/db61beab1598d080f151cd20e66027bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db61beab1598d080f151cd20e66027bc.jpeg)



**"Alex Krause"**

---
---
**Alex Krause** *June 27, 2016 03:34*

Soldered headers to bi-directional level shifter (red chip to the right of the smoothie) and a header in pin location 2.4 and a ground pin... I think I'm ready to start crimping some connectors unless someone can tell me if I forgot to solder some headers


---
**Alex Krause** *June 27, 2016 04:09*

**+Peter van der Walt**​ 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 04:12*

Wow, I'm definitely going to need help with all this sort of stuff. I'm still waiting on the level shifter but I've been looking at the stuff I've got so far & have no idea what to do haha.


---
**Alex Krause** *June 27, 2016 05:03*

**+Arthur Wolf**​ any other insights to the above? Trying to make this the #YearOfTheSmoothieLaser just want to make sure I'm not forgetting something... I have a dedicated 5v/12v industrial power supply to control the logic end of the smoothie


---
**Don Kleinschnitz Jr.** *June 27, 2016 13:07*

If helpful, I have decided on the following PRELIMINARY K40S power design:

[http://donsthings.blogspot.com/2016/06/k40s-power-systems-design.html](http://donsthings.blogspot.com/2016/06/k40s-power-systems-design.html)

The link in this post to the schematic is constantly updated as I progress through the interconnect design.










---
**Arthur Wolf** *June 27, 2016 16:51*

**+Alex Krause** What above ?  I'm not sure what the question is. I'm ready to help with anything, but I'm not sure what you are looking for. 


---
**Alex Krause** *June 27, 2016 17:10*

**+Arthur Wolf**​ in the blue box guide ( [http://smoothieware.org/laser-cutter-guide](http://smoothieware.org/laser-cutter-guide) ) it lists pin 2.0-2.5 as Pwm pins... but it has been suggested to me that I use pin 2.6. I will be using m1 and m2 for now and m3 in the future for bed adjustment. Is there any other outputs or inputs I need to solder headers to before I start attaching things to the smoothie?


---
**Arthur Wolf** *June 27, 2016 17:11*

**+Alex Krause** It's likely you don't need more you should just go ahead as it is now I think. This page tells you what each pin does, for reference : [http://smoothieware.org/pinout](http://smoothieware.org/pinout)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/a6hMcym5tCi) &mdash; content and formatting may not be reliable*
