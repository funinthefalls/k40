---
layout: post
title: "Here is another of my paintings/sculptures. I haven't posted for a while"
date: June 22, 2018 17:44
category: "Object produced with laser"
author: "James Lilly"
---
Here is another of my paintings/sculptures. I haven't posted for a while. The rockfish, gauge parts and plaque were all cut and engraved.  The "screw heads" are birch plugs with a phillips head "X" engraved in them as well.  If you're in the Seattle area, I have a gallery show next month I'll post about another time.

![images/50abc129b26800d1496b1ed4ee0e8e37.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/50abc129b26800d1496b1ed4ee0e8e37.jpeg)



**"James Lilly"**

---


---
*Imported from [Google+](https://plus.google.com/114642009939368433927/posts/DPzXeq5mrY6) &mdash; content and formatting may not be reliable*
