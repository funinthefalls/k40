---
layout: post
title: "Hi, I've just got the K3020 model of this cutter and have had a weekend of learning, engraving, cutting, aligning and designing..."
date: June 27, 2016 13:44
category: "Original software and hardware issues"
author: "Peter Gisby"
---
Hi,



I've just got the K3020 model of this cutter and have had a weekend of learning, engraving, cutting, aligning and designing... During this I've noticed something, while the laser is firing the cooling water carries an electrical charge, i.e. sticking your fingers in the cooling water to check it's temperature gives you a shock. 

Then earlier today I powered the unit on and it blew it's fuse, there was a bit of a flash in the PSU section and the light on the power button went out. I checked the inlet fuse and this was very black and blown, so I replaced it, all this did was fixed the power light and not the rest of the machine. I'm guessing there is an internal fuse in the PSU that is also blown.



I've contacted the supplier, as I only received the machine on Friday, and am waiting for a response.

I could replace the PSU fuse myself but I'm worried about the water issue, as there must be something wrong with the machine that caused the fuse to blow.



What are your thoughts?? 

Is the water thing 'normal'??



I was about to embark on a series of upgrades, air assist, different control boards etc, but am unsure now as these would negate my warranty. What is the consensus regarding these machines and upgrades??



Thanks for your help.

Peter.





**"Peter Gisby"**

---
---
**greg greene** *June 27, 2016 13:55*

NOT NORMAL - These machines have a very poor grounding system.  They ABSOLUTELY MUST be grounded to a bonafide grounding system INDEPENDENT of the round wire running in your house.   Not to do so puts you at risk.


---
**Ned Hill** *June 27, 2016 14:13*

Usually if you are getting a shock from the water it's because the pump isn't properly sealed.  Second the fact that the machine and pump must be grounded.  Otherwise you risked killing yourself due to the high voltage from the tube. 


---
**Peter Gisby** *June 27, 2016 14:16*

This is the problem, if the water was always electrified I'd go with a faulty pump but it's only charged when the laser is firing. making me think it's something else. Not sure I can get a 'better' earth than the one already in the house as it will end up going to the same place.


---
**Ned Hill** *June 27, 2016 14:16*

You can test if it's the pump by unplugging the machine and just plug the pump in.  Then take your voltmeter, or other ac voltage checking device, and put one lead in the water and the other to a ground and see if there is any significant ac voltage.


---
**Ned Hill** *June 27, 2016 14:23*

**+Peter Gisby** Personally I would try and get a refund on that machine since it's so new rather than trying to fix the electrical.


---
**David Lancaster** *June 27, 2016 14:26*

Could be an electrode isn't sealed from the water jacket correctly, so high voltage is making it's way into the water, and from there, shorting to ground.


---
**Peter Gisby** *June 27, 2016 14:40*

It's not the pump, no charge in the water while running..


---
**Derrick Armfield** *September 09, 2016 13:39*

Did you ever get this issue figured out?   I did the same thing last night.   Felt like I touched an electric fence!    Only happens when the laser is Firing.   


---
**greg greene** *September 09, 2016 14:06*

You MUST ground these things to an independent ground.  That's why they stick a grounding lug on the back of the machines.


---
**Peter Gisby** *September 09, 2016 14:11*

I ran a earth wire from the terminal on the back of the unit to the water tank and that solved the issue.


---
**greg greene** *September 09, 2016 14:12*

Don't stop there - continue that ground wire to a genuine ground


---
*Imported from [Google+](https://plus.google.com/111055748369814573959/posts/4tdFqsQtG3Y) &mdash; content and formatting may not be reliable*
