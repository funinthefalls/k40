---
layout: post
title: "Finally came around to cutting the other 3 compartments so my Bed can get completed..."
date: September 26, 2016 05:59
category: "Object produced with laser"
author: "Gunnar Stefansson"
---
Finally came around to cutting the other 3 compartments so my Bed can get completed... just need to assemble it Now.



Cause the first one is holding up nicely, ofc it's getting some marks here and there but it doesn't affect acrylic, but when cutting mdf it is visible however, but as I've read here masking tape should do it :)





![images/88622eea2d13d1ec6e8d913ec718cc8b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/88622eea2d13d1ec6e8d913ec718cc8b.jpeg)
![images/11c899710cc14a05e0f649b87c4e59a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/11c899710cc14a05e0f649b87c4e59a2.jpeg)
![images/1c0856b341ee968375b39d5ffea4b9d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c0856b341ee968375b39d5ffea4b9d9.jpeg)
![images/d1279be8af9d101313ebbc60a653fce7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1279be8af9d101313ebbc60a653fce7.jpeg)

**"Gunnar Stefansson"**

---
---
**Anthony Bolgar** *September 26, 2016 06:12*

Great job on the bed. Would you be willing to share the design files for this?


---
**Gunnar Stefansson** *September 26, 2016 06:27*

Thx **+Anthony Bolgar**, I can share them, how would you like them? I can give you, CamBam file, dxf file and jpg file with the dimension included?


---
**Anthony Bolgar** *September 26, 2016 06:28*

DXF would be great, I can import that into LaserWeb.


---
**Gunnar Stefansson** *September 26, 2016 06:36*

You get them All... :) you might get some scaling issues when importing, so I've added Dimensions as Layers and pictures with absolut height&length. The 3 Layers are:

ProfileHolders(5mm Acrylic)

SharkTeeth_Hori(3mm Acrylic)

SharkTeeth_Vert(3mm Acrylic)



[drive.google.com - G+ Shares – Google Drev](https://drive.google.com/drive/folders/0B3-WzWhvXB8EQmZOVDdZbHdtazg?usp=sharing)


---
**Anthony Bolgar** *September 26, 2016 06:40*

Thank you.


---
**HP Persson** *September 26, 2016 09:15*

Looking good, i should try that too instead of moving 150 nails every time my acrylic bed is worn out :)


---
**Gunnar Stefansson** *September 26, 2016 09:21*

Woooow 150 nails... hehehe now that would get boring real fast, I imagine!!! :D well you can give it a try, for me I must say it worked better than I thought! although it was just a test ;)


---
**Sebastian Szafran** *September 26, 2016 14:50*

Thanks for sharing **+Gunnar Stefansson**​ :-) 


---
*Imported from [Google+](https://plus.google.com/100294204120049700616/posts/g4VnCYNn7ue) &mdash; content and formatting may not be reliable*
