---
layout: post
title: "Hey guys, having a little trouble with CorelLaser doing two passes on all cuts?"
date: October 31, 2017 17:10
category: "Software"
author: "Pigeon FX"
---
Hey guys, having a little trouble with CorelLaser doing two passes on all cuts? have tried adjusting all cut to hairline and 0.1mm, as advised is some other posts, but still getting double passes. 





**"Pigeon FX"**

---
---
**Phillip Conroy** *October 31, 2017 17:30*

Veiw what ever you are cutting in wirer view, if converted image to a vector most likly to have 2 lines close together that you can not see unless you change veiw to wire veiw and zoom in 


---
**Pigeon FX** *October 31, 2017 17:49*

Hit wireframe, and zoomed into max 1600% and still only one line, they are vector files for Illustrator if that's any help??



edit to add: see "repeat" is set to 1 in settings, should that be on 0?




---
**Phillip Conroy** *October 31, 2017 17:54*

I use e cut to remove hidden lines,works very well best $70 i have spent. 


---
**Justin Mitchell** *October 31, 2017 21:22*

simplest way is dont use lines for cutting, use solid shapes with cutouts where necessary. it will cut the outline of that solid shape.




---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/8MJcVXxz2yq) &mdash; content and formatting may not be reliable*
