---
layout: post
title: "Has anyone an idea of how use the exhaust as reflow to air assist?"
date: February 19, 2017 09:38
category: "Modification"
author: "Jorge Robles"
---
Has anyone an idea of how use the exhaust as reflow to air assist? No more compressors ;)





**"Jorge Robles"**

---
---
**Roberto Fernandez** *February 19, 2017 10:03*



Passing the air through a carbon filter, such as that used in cars as a filter of habitaculo. To re-introduce the air again in the machine for a new orifice. Is it what thinking these?


---
**Jorge Robles** *February 19, 2017 10:04*

Yees


---
**Roberto Fernandez** *February 19, 2017 10:10*

It is what some extractor bells do when it has not gone out of gases in the kitchen. You lose something of efficiency for crossing the filter. Make a box with a 3d or buy a second hand filter box in a chop shop (desguace)


---
**Jorge Robles** *February 19, 2017 10:12*

Yes, I was wondering if any physics effect (Venturi, etc) could help to better implement 'turbo' effect. :)


---
**Roberto Fernandez** *February 19, 2017 10:24*

forget it. Venturi is more to add an aditive, no for get more flow.

If what you want is a major effect of sweep, a mouthpiece of exit uses type duck billed, smoothed and very broad, it gets more speed of air exit


---
**Roberto Fernandez** *February 19, 2017 10:36*

si te quieres entretener leyendo: [airflow.es - www.airflow.es/descargas/manual/manualDifusion.pdf](http://www.airflow.es/descargas/manual/manualDifusion.pdf)


---
**Roberto Fernandez** *February 19, 2017 10:44*

Probably obtain some improvement with the effect coanda, directing the air jet the flat table to drag more air and to recover (get back) part of the flow lost for crossing the filter.

Hoy me siento inspirado :-)


---
**Roberto Fernandez** *February 19, 2017 10:55*

If you want to increase x4 the air flow, and you have compressor, the effect coanda can help with this SMC's element.

[smc.eu - www.smc.eu/smc/ProductsRepository/ZH_X185/catalogues/ZH_X185_cat_es.pdf](https://www.smc.eu/smc/ProductsRepository/ZH_X185/catalogues/ZH_X185_cat_es.pdf)


---
**Jorge Robles** *February 19, 2017 10:56*

Woow. I will take a look ;) thanks!!


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/KeW8fnjx1fx) &mdash; content and formatting may not be reliable*
