---
layout: post
title: "Tried using the cutting surface svg file to test on the laser, something I never did as I should have in the beginning lol"
date: February 29, 2016 04:11
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
Tried using the cutting surface svg file to test on the laser, something I never did as I should have in the beginning lol. Regardless when I export to the inkscape plugin I get this :



Built gcode for path4507-3 - will be cut as vector.

Built gcode for path4507-1 - will be cut as vector.

Built gcode for path4507 - will be cut as vector.

Built gcode for group text5153, item path5158 - will be cut as vector.

Built gcode for group text5153, item path5160 - will be cut as vector.

Built gcode for group text5153, item path5162 - will be cut as vector.

Built gcode for group text5153, item path5164 - will be cut as vector.

Built gcode for group text5153, item path5166 - will be cut as vector.

Built gcode for group text5153, item path5168 - will be cut as vector.

Built gcode for group text5153, item path5170 - will be cut as vector.

Built gcode for group text5153, item path5172 - will be cut as vector.

Built gcode for group text5153, item path5174 - will be cut as vector.

Built gcode for group text5153, item path5176 - will be cut as vector.

Traceback (most recent call last):

  File "turnkeylaser.py", line 1437, in <module>

    e.affect()

  File "C:\Program Files\Inkscape\share\extensions\inkex.py", line 268, in affect

    self.effect()

  File "turnkeylaser.py", line 1408, in effect

    data = self.effect_curve(selected)

  File "turnkeylaser.py", line 1194, in effect_curve

    for objectData in compile_paths(self, node, trans):

  File "turnkeylaser.py", line 1014, in compile_paths

    im = Image.open(filename).transpose(Image.FLIP_TOP_BOTTOM).convert('L')

  File "C:\Program Files\Inkscape\python\lib\site-packages\PIL\Image.py", line 1637, in transpose

    self.load()

  File "C:\Program Files\Inkscape\python\lib\site-packages\PIL\ImageFile.py", line 155, in load

    self.load_prepare()

  File "C:\Program Files\Inkscape\python\lib\site-packages\PIL\PngImagePlugin.py", line 339, in load_prepare

    ImageFile.ImageFile.load_prepare(self)

  File "C:\Program Files\Inkscape\python\lib\site-packages\PIL\ImageFile.py", line 223, in load_prepare

    [self.im](http://self.im) = Image.core.new(self.mode, self.size)

  File "C:\Program Files\Inkscape\python\lib\site-packages\PIL\Image.py", line 36, in <i>_getattr_</i>

    raise ImportError("The _imaging C module is not installed")

ImportError: The _imaging C module is not installed





I believe there is something I am missing that has everything to do with raster engraving via inkscape plugin, my question is what is it and how do I fix this? Do I need to do a cmd prompt install? I have done lots of research only to find really complicated solutions. Any help would be awesome, thank you.





**"Andrew ONeal (Andy-drew)"**

---
---
**Anthony Bolgar** *February 29, 2016 06:02*

According to the Turnkey Tyranny Inkscape plugin git repository:



If you are using 32 bit inkscape you can potentially use my precompiled version of 32 bit python 2.7.9 with all the needed libraries follow these steps, however this works for some people and not for others. Unsure why :



    Download my updated windows version of python from : [https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin/blob/master/files/Python27.rar](https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin/blob/master/files/Python27.rar)

    Navigate to C:\Program Files\Inkscape and rename the folder "python" to "python-old"

    Extract the folder 'Python27' from the archive you downloaded to C:\Program Files\Inkscape and rename it to 'python' (Note the lower case p)

    You are done, launch inkscape and run the plugin!

    If you run the export plugin and you don't get a popup diagnostic log telling you which items were exported successfully then unfortunately you need to install Python manually.  Try installing this way, if that does not work, he explains another way of installing his modified python file.




---
**Stephane Buisson** *February 29, 2016 12:19*

what is behind inkscape ? what's your software-hardware chain ? 


---
**Andrew ONeal (Andy-drew)** *March 01, 2016 03:16*

What do you mean Stephane?


---
**Stephane Buisson** *March 01, 2016 06:40*

it's not inkscape (plugin) to generate the final gcode. In my my worflow, inkscpape plugin for Visicut prepare the output (text, fonts, pixels, vectors) into a ready made SVG import to Visicut. Then Visicut prepare the final Gcode with cutting setting and machine parrameters. So I do understand you have another step after Inkscape, for cutting setting and machine parrameters.


---
**Andrew ONeal (Andy-drew)** *March 03, 2016 03:52*

Hey anthony, Hey I did a native install of python successfully and followed instructions to the T, but in the tyranny repo instructions it doesn't say what to do with new Python folder and old Python folder. After following instructions it says launch inkscape and run plugin, which I did but inkscape has the old Python folder which was renamed python-old as instructed and the new Python was placed on c:\🤔 any ideas?


---
**Andrew ONeal (Andy-drew)** *March 03, 2016 03:59*

Yeaaàaaaaaaaaaa I fixed it lol OMG IT FINALLY GENERATED RASTER VIA PLUGIN. HAD TO RENAME FOLDER FROM Python27 to python then copy folder from c:\ and paste it into inkscape folder. So so happy lol, it's the little things that get me haha


---
**Andrew ONeal (Andy-drew)** *March 03, 2016 03:59*

Thanks guys for advice, I love this forum!




---
**Andrew ONeal (Andy-drew)** *March 03, 2016 04:06*

Oh hey, in Marlin code is it necessary to change workspace size to do an image that's say 8.5x11 landscape?


---
**Francis de Rege** *March 04, 2016 12:59*

How do I download the precompiled version of 32 bit 2.7.9 Python version at github? Usually I can find a zip file but the only zip file is the "laser-gcode-exporter-inkscape-plugin".


---
**Andrew ONeal (Andy-drew)** *March 06, 2016 03:55*

Francis, hey sorry for the delay in response. The precompiled version and instructions are further down in the github page for laser gcode exporter. I opted to do the first option which was cmd install and it did solve my issues. Thanks again to Anthony's suggestion. 



I have question for those in this discussion, I tried using the cuttingsurface.svg file for first time but there is something I am not doing? I hit print and the laser moves to all points as told in gcode, however there is no laser firing? After looking into it a bit further the only differences I could find from this file and others I have successfully printed, are the stroke width setting and have yet to test that theory. Wanted to check here first since I am not at my laser at the moment. I also thought it could be the ppm setting, and last thought was the laser power setting of 10 not being enough to get laser to fire.Any ideas would be awesome.


---
**Francis de Rege** *March 06, 2016 20:52*

Andrew. Thanks...did the cmd install l and did get it to work. However I ran into the same problem you have with raster cuts...get that nonsense g code. Vector cuts are fine. So I'm hoping someone else has a solution and posts it here.


---
**Andrew ONeal (Andy-drew)** *March 08, 2016 01:59*

So far I still have no answers to this issue, please let me know if you figure it out. 


---
**Andrew ONeal (Andy-drew)** *March 09, 2016 07:56*

Francis de Rege, Hey listen I found this link [http://wiki.lansingmakersnetwork.org/equipment/buildlog_laser_cutter](http://wiki.lansingmakersnetwork.org/equipment/buildlog_laser_cutter) 



You will have to read through it a few times but check it against your current config.h file All that parsed info in generated code is all sensible info and can be decoded according to this link. See what you think, it took me forever to find anything on it at all so this is my best solution. Kinda stumble on it.


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/79ZVmKnKRD3) &mdash; content and formatting may not be reliable*
