---
layout: post
title: "Hey guys Ive been looking into water cooling solutions for my K40 and was wondering if anyone has used a beer chiller?"
date: August 14, 2018 18:58
category: "Discussion"
author: "STEVEN GIBBARD"
---
Hey guys

I’ve been looking into water cooling solutions for my K40 and was wondering if anyone has used a beer chiller?



Has anyone here successfully used a beer chiller as it seems a cheaper refrigerated alternative to a CW 5000. Apart from their bulky ness they seem readily available used from £120. My main concern is I’m struggling to find any specs on them. 



Many thanks. 



[https://rover.ebay.com/rover/0/0/0?mpre=https%3A%2F%2Fwww.ebay.co.uk%2Fulk%2Fitm%2F232887938375](https://rover.ebay.com/rover/0/0/0?mpre=https%3A%2F%2Fwww.ebay.co.uk%2Fulk%2Fitm%2F232887938375)





**"STEVEN GIBBARD"**

---
---
**Stephane Buisson** *August 15, 2018 08:38*

how much heat do you think you need to take away ? what is your local temp ? are you lasering in a sunny desert ?


---
**STEVEN GIBBARD** *August 15, 2018 21:30*

Seems excessive I know haha. The ambient room temp is around 20-25•c and I mostly use it for engraving, I’ve seen really 30 plus at the tube with my current bucket and ice setup. 

A cw3000 won’t be much use as I need it below ambient room temp. But £290 for a cw5000 from aliexpress is a bit more than I’d like to pay. 


---
**S Th** *August 17, 2018 15:56*

I think I've come up with a pretty ideal solution, but I haven't been able to implement it yet (parts on their way from China). I have to work outside and it can get hot, but worst of all it can be pretty humid too, so I needed something thermostat controlled to stay above the dew point and avoid condensation. So I wanted:

- temperature control

- cheap

- compact

- no compressor to turn on and off



Naturally I considered Peltiers, and read a ton about them. That is certainly a more elegant solution, but I think mine will be simpler and cheaper.



I plan on having two lunchbox coolers. The first will be my closed- loop distilled water circuit running through my flow sensor, temperature sensor, and laser tube. The second will be a cold, tap-water tank to which I can add frozen water bottles, ice packs, etc. 



A secondary pump in the distilled water cooler will operate a second circuit for the distilled water that passes through an aluminum cooling block located in the ice-water cooler.



The thermostat will control the secondary pump so that the extra cooling circuit only operates when necessary allowing temperature control.



This will allow me to isolate the distilled water from the ice water and keep me from having to reach in and out of the water going to the laser.



I'll be sure to post the results when all of my parts get here in a couple weeks, but what do you think?


---
**Jason Todd** *August 22, 2018 01:28*

I converted an old kenmore mini fridge into a chiller. I use a cheap marine bilge pump, copper tubing, and RV antifreeze (no water added). I actually had to turn the temp up some to keep the lines from sweating so much inside my house where I keep the temp at 69 F.

[https://plus.google.com/photos/...](https://profiles.google.com/photos/109653447995323725355/albums/6592350947682005169/6592350947186356834)


---
*Imported from [Google+](https://plus.google.com/117335652930676604279/posts/TSbkDWjUKx6) &mdash; content and formatting may not be reliable*
