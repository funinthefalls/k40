---
layout: post
title: "I don't know if anyone has posted this before, you cut and engrave without changing output screen"
date: December 05, 2017 13:28
category: "Software"
author: "james blake"
---
I don't know if anyone has posted this before, you cut and engrave without changing output screen.  You can set the colors on the cutting screen and only work with 1 at a time or multiple. 

![images/d5bb72377631a0065110ef72cd77f7ab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d5bb72377631a0065110ef72cd77f7ab.jpeg)



**"james blake"**

---
---
**Wild Bill** *December 05, 2017 22:38*

And what software are you talking about?




---
**HalfNormal** *December 06, 2017 01:06*

Looks like you are using Moshidraw software. You need to search the Moshidraw forums but I do not think it supports engrave and cut in one process. 


---
**Wild Bill** *December 06, 2017 01:27*

Never used that software so I had never seen that screen. I have been using K40 Whisperer and etch then cut acrylic all the time for LED end lit images.


---
**HalfNormal** *December 06, 2017 01:29*

Moshidraw is for the Moshi control boards found in some of the K40 units. K40 Whisperer will not work with the Moshi board.


---
*Imported from [Google+](https://plus.google.com/115955612231558356912/posts/73Z1bvNkgyk) &mdash; content and formatting may not be reliable*
