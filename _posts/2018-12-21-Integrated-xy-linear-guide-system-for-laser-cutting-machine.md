---
layout: post
title: "Integrated xy linear guide system for laser cutting machine"
date: December 21, 2018 02:32
category: "Discussion"
author: "Alan- FIVE LASER"
---
Integrated xy linear guide system for laser cutting machine. It makes the installation of XY linear motion system easier much, because the linear guide module has been assembled in single axis before delivery. Buyer just need to install X axis module and Y axis module together with shaft & coupling. More info please refer to five laser website by link: [www.fivelaser.com](http://www.fivelaser.com), or email: info@fivelaser.com 



![images/5a3d21cbc2787dc479e3e56b750df8fe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a3d21cbc2787dc479e3e56b750df8fe.jpeg)
![images/e3c11f75aa95e615aff1ddd790fec94d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e3c11f75aa95e615aff1ddd790fec94d.jpeg)

**"Alan- FIVE LASER"**

---


---
*Imported from [Google+](https://plus.google.com/107864781841729292977/posts/SRs7J1Jg3cN) &mdash; content and formatting may not be reliable*
