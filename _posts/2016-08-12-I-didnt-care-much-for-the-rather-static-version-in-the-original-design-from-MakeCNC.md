---
layout: post
title: "I didn't care much for the rather \"static\" version in the original design from MakeCNC"
date: August 12, 2016 19:00
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
I didn't care much for the rather "static" version in the original design from MakeCNC. And being the anal retentive, OCD person that I am, it had to be made better. :)  To note: there is absolutely nothing wrong with the MakeCNC version, it's a great, simple model. I just prefer to have a more detailed one.



There are stills of the current progress. I completely rebuilt the bowl part, complete with two pistons at the top that raises and lowers the bowl. The door on the apron opens and closes as you can see.



And this is why I love doing this stuff in 3D CAD. While all of the pieces are drawn in 2D, I can extrude them to the material thickness (in this case 1/8" material, and possibly some 1/4") and assembly the whole thing in 3D space. I can check that everything fits properly, make sure there aren't collisions anywhere, I can set the movements so I know how it would behave once built. Of course, now that I've rebuilt this part, I need to rebuild the rest of the vehicle as well to match it all. Ah the joys ...



And before you ask: I won't be releasing the plans for this. For one, I'm spending an incredible amount of time for something rather unique that I am gifting to someone. Second, a lot of the pieces are rather small and intricate (such as the pistons) and creating an assembly manual isn't something I want to do. However, I will entertain the possibility of selling complete models if there's interest. No idea what I'll charge for it though. Back to work ...



![images/1c3711be62da2d9e6fb7bc7eed84a58e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c3711be62da2d9e6fb7bc7eed84a58e.jpeg)
![images/6ff26f7a48fc5a9f0bef959c24ad340a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6ff26f7a48fc5a9f0bef959c24ad340a.jpeg)
![images/aff029f2f93307748bf32b09bab61a44.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aff029f2f93307748bf32b09bab61a44.jpeg)
![images/332d91797e32e6770de52d386b855389.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/332d91797e32e6770de52d386b855389.jpeg)
![images/764329acc572f4a006675a9a24278b83.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/764329acc572f4a006675a9a24278b83.jpeg)
![images/dba29cb75a16324d762544df813fd352.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dba29cb75a16324d762544df813fd352.jpeg)
![images/70ec246199f4b3400b60f4d82c4cff52.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/70ec246199f4b3400b60f4d82c4cff52.jpeg)
![images/b74f1c3e396a2fe9cb5e6066f7b6f612.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b74f1c3e396a2fe9cb5e6066f7b6f612.jpeg)

**"Ashley M. Kirchner [Norym]"**

---
---
**Scott Marshall** *August 12, 2016 21:19*

Offers from CAT, Komatsu, and Terex will be pouring in....



Now to build it in 2" hardface steel... - you're going to need a bigger Laser.


---
**Ashley M. Kirchner [Norym]** *August 12, 2016 21:54*

LOL!! 2" steel, that would be a feat, that's for sure.


---
**Ashley M. Kirchner [Norym]** *August 12, 2016 21:55*

Though I would probably use a water jet to cut that. 


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/MSyWvjU5rpw) &mdash; content and formatting may not be reliable*
