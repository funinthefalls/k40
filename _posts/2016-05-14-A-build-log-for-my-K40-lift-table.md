---
layout: post
title: "A build log for my K40 lift table"
date: May 14, 2016 12:17
category: "Modification"
author: "Don Kleinschnitz Jr."
---
A build log for my K40 lift table. Not sure if going public on G+ also updates this community.

[http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html](http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)





**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 14, 2016 21:35*

Posting publicly won't auto-update K40 community. You'd have to reshare each thing you want individually to this group.



Thanks for sharing this build log. Interesting read.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Enk1sGk8Ntm) &mdash; content and formatting may not be reliable*
