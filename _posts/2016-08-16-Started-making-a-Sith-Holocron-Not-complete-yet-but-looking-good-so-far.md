---
layout: post
title: "Started making a Sith Holocron. Not complete yet, but looking good so far"
date: August 16, 2016 19:10
category: "Object produced with laser"
author: "Eric Flynn"
---
Started making a Sith Holocron.  Not complete yet, but looking good so far.  I replaced the amber door window with a larger clear one, so i used the amber to make this, along with some black of course.



![images/97bf275ce00b6851296a6e6678461df4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97bf275ce00b6851296a6e6678461df4.jpeg)
![images/e9fdcf09e3b12d8bfdd132626fb6f5d6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9fdcf09e3b12d8bfdd132626fb6f5d6.jpeg)

**"Eric Flynn"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 17, 2016 04:47*

That's really cool. Looking good.


---
**David Cook** *August 17, 2016 19:15*

well that is really freeking cool.




---
**Ben Marshall** *August 18, 2016 00:44*

What are you using for illumination? 


---
**Eric Flynn** *August 18, 2016 01:07*

I'm using a 10W RGB LED


---
**Gunnar Stefansson** *August 18, 2016 08:53*

Awesome, Looks really good


---
**John-Paul Hopman** *August 18, 2016 20:15*

Wait. Did you laser cut this from the amber window on your K40? The one that is supposed to protect you from stray beams?


---
**Eric Flynn** *August 18, 2016 20:25*

Yes.  I replaced the windows with a larger clear one.  Clear is just as good at blocking the 10.6u IR from a CO2 laser as the amber is.  I honestly have no idea why they chose to use amber over clear.  It has no benefit at these wavelengths.


---
**John-Paul Hopman** *August 18, 2016 20:26*

Scary that you can laser cut the object intended to protect you from the laser.


---
**Petri Koskela** *October 31, 2016 10:13*

I too have an amber window to be used as raw material - in a new machine holes were drilled to wrong places and acrylic window had been forced  to screws. It was impossible to remove for tearing the protection papers off without cracking corners. 

Question: Is there any technical, chemical or optical reasons why I should not replace the window with a clear polycarbonate (Lexan) one? I am going to cut and drill it with legacy methods - or is it as evil as PVC anyway if cut with laser - does it cut?


---
**Petri Koskela** *October 31, 2016 10:18*

Found several posts here where Lexan was found unsuitable for laser cutting. I'll use drill and saw - if no one can prove it unsuitable for lid window too. I have a good sized sheet available...


---
**Petri Koskela** *October 31, 2016 10:27*

In fact, some "better" machines have Lexan window as standard feature: "The window of the laser cutter is made of Polycarbonate because polycarbonate strongly absorbs infrared radiation! " - [http://atxhackerspace.org/wiki/Laser_Cutter_Materials](http://atxhackerspace.org/wiki/Laser_Cutter_Materials) - still waiting for possible other opinions...


---
*Imported from [Google+](https://plus.google.com/107613521854079979112/posts/8GENpyF6kGM) &mdash; content and formatting may not be reliable*
