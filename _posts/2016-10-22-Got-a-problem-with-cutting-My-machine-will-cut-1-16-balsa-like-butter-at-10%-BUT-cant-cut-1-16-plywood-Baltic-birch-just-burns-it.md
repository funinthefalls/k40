---
layout: post
title: "Got a problem with cutting - My machine will cut 1/16 balsa like butter at 10% BUT can't cut 1/16 plywood (Baltic birch) just burns it"
date: October 22, 2016 22:34
category: "Discussion"
author: "Dennis Beukelman"
---
Got a problem with cutting - My machine will cut 1/16 balsa like butter at 10% BUT  can't cut 1/16 plywood (Baltic birch) just burns it. Here is what I have done so far;

- I have realigned the mirrors several times

- I have set the focal length using the brown plexiglass strip, as well I have tried setting it with a ruler (50.8mm) I set this from the lens bottom (measured by toothpick and lens top measured  by a toothpick and subtracting)

- I tried testing it by setting a thin board (balsa) at an angle to the photo length from 40mm to 70 mm

- I have cleaned the mirrors several times with lens cleaner and wipes

- I tried opening the lens holder (can't it is seized - cross threaded) Can't tell if lens is proper side up

- Tried cleaning lens with a cotton swab and lens cleaner - seems to be some crud I can't remove (hard to see)

- Reloaded software and initializing 



Cutting is only for 1/32 balsa anything bigger or on 3mm plywood / cardboard it won't cut through. I have tried recutting over the same line 4 or 5 times still won't cut through

Laser tube appears to be working fine, if I turn it up to say 90% it will set the wood on fire (not what I want)

X and Y axis seems to run fine with software.



Really frustrating, any suggestions?

Hope you can help

Thnx

Dennis





**"Dennis Beukelman"**

---
---
**Dennis Beukelman** *October 22, 2016 22:41*

Should also note I have tried setting the speed from 5 mm/s up to 50 mm/s - same results


---
**greg greene** *October 22, 2016 22:51*

Yup - been there - done that, if your absolutely sure the beam is hitting the mirrors IN THE CENTRE of each mirror, and hitting the opening in the head IN THE CENTRE of the opening, and the beam is exiting IN THE CENTRE of the lens - especially if you have an air assist nozzle - then check it again - because it isn't.  somewhere along the line, the beam is either off centre or the mirrors need cleaning,  These machines do cut through 3 mm on a single pass at around 6 - 8 ma at 8 to 10 mm/sec.


---
**Scott Marshall** *October 24, 2016 01:09*

It's probably your lens. If it's not pristine, it's junk. You need to get it out and have a good look. Focus isn't measureable from the lens very accurately, (the toothpick method will get it about as close as you can get that way) but it will get you in the ballpark.



A lot of people waste time trying to get the beam centered on the mirrors, but that doesn't matter at all. As long as the beam doesn't run off the edge of the mirror or hit the mount, it's fine. The mirror reflects the same all over it's surface) The thing that matters is that the beam is parallel to the axis, which means you see no change in it's position from the closest position of the most distant. I use lo tack masking tape with a disk of aluminum foil (shiny side to mirror) behind it so your spot isn't effected by the reflected beam. It also keeps you from fouling your mirrors. Turn down the power as low as you can and still get a mark. (the smaller the mark the more precise the test) adjust only one axis at a time and adjust only the top 2 or side 2 screws at each change. Start with Y and then X, then you need to adjust the head.Move the head in to about 2" from the pass through, fire a shot, then push the carriage out to the axis limit and fire a 2nd shot.  Get it PERFECT, you want the 2nd shot not to even burn tape. It takes some time, but it's worth it. It DOES matter where the beam hits the Lens and you want it dead center. You'll have to get that head apart both to check/replace the lens and to adjust the head. To adjust the head, remove the lens and replace it with tape. Raise/lower the head to adjust left/right position and rotate the head to adjust front/rear position. Once you get it perfect, replace the lens. Focus by adjusting for the smallest spot (the same as with the tape). Once you have the correct focus determined using this method, take a measurement from a convenient point on the head to the work piece and you can use that number for future set ups. For cutting, focus on the center (of the thickness) of the material.



You won't believe the difference once you have it set up properly.



Hope I helped a little.



You probably have already seen this, but here is the Wombat one more time...



Scott



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**greg greene** *October 24, 2016 14:01*

The wombat method is the one I used - it really turned the machine into a performer ! Be aware though - that the center of the hole in the head is set up mechanically to receive the beam as reflected from the center of the mirror.  If the beam does not hit the center of the mirror - or close to it, it may not hit the center of the hole at all distances.


---
*Imported from [Google+](https://plus.google.com/104196870231434096924/posts/h7qz3VEaQAv) &mdash; content and formatting may not be reliable*
