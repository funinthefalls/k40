---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) Happy New Year from Australia!"
date: December 31, 2015 15:34
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Happy New Year from Australia!



We beat most of you haha.



Hope 2016 brings everyone the good and happiness that we all would like to have in our lives.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Jim Hatch** *December 31, 2015 15:50*

LOL! I was just telling my daughter last night that someone should broadcast a live New Year tickover (like the New York City ball drop) from somewhere like Iceland so all the folks on the East Coast of America could celebrate at 9PM local and then get to bed at a reasonable hour ;-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 31, 2015 17:02*

**+Jim Hatch** Haha, that's funny. No idea why we all stay up to watch the clock though anyway. It will still be New Year in the morning when we wake up.


---
**Scott Marshall** *December 31, 2015 17:23*

You did at that! Probably will next year too....

12 more hours here. Have a good one.

Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 01, 2016 02:32*

**+Scott Marshall** There's not many places that beat us here. New Zealand/Fiji do as far as I'm aware. Have a good new years.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Jxjmg2p82ao) &mdash; content and formatting may not be reliable*
