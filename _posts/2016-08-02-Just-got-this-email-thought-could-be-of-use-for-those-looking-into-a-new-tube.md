---
layout: post
title: "Just got this email, thought could be of use for those looking into a new tube"
date: August 02, 2016 21:48
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Just got this email, thought could be of use for those looking into a new tube. 

![images/5fbfb163497ca3d74257bddf9b630c3b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5fbfb163497ca3d74257bddf9b630c3b.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Pippins McGee** *August 02, 2016 22:54*

these price US or AUD? ta


---
**Ariel Yahni (UniKpty)** *August 02, 2016 23:34*

Look for laserdepot in Google they are in the USA


---
**Ariel Yahni (UniKpty)** *August 02, 2016 23:35*

For reference I bought a 40w from Ali at the same price shipped via DHL to Panama


---
**Pippins McGee** *August 03, 2016 00:34*

Thanks.

Also, I've seen comments recently can't remember by whom, saying keeping a spare tube is a waste of money as its lifespan reduces even if not being used.

anyone care to comment on this with the science behind this claim?



I'd otherwise be interested in keeping spare parts so I have no down-time should my tube cark it.


---
**Jim Hatch** *August 03, 2016 00:41*

Pretty much same as LightObject's tube prices.


---
**Ariel Yahni (UniKpty)** *August 03, 2016 00:52*

Are they RECI at those prices? 


---
**I Laser** *August 03, 2016 02:23*

Their site shows the full price? ie 700mm 40W - $275 USD


---
**Ariel Yahni (UniKpty)** *August 03, 2016 02:41*

Well the email says live chat for questions 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/MZQJ3CaXu2c) &mdash; content and formatting may not be reliable*
