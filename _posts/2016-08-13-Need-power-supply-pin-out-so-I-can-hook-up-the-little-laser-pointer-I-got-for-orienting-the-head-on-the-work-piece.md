---
layout: post
title: "Need power supply pin out so I can hook up the little laser pointer I got for orienting the head on the work piece"
date: August 13, 2016 23:21
category: "Discussion"
author: "greg greene"
---
Need power supply pin out so I can hook up the little laser pointer I got for orienting the head on the work piece.  current draw is like 2 ma





**"greg greene"**

---
---
**Scott Marshall** *August 14, 2016 19:04*

Power Supply (big 4 pin connector on the right) Left to right --  Pin 1- 24V, Pin 2 - Gnd, Pin 3 - 5V, Pin 4 - Fire signal (low to fire)


---
**greg greene** *August 14, 2016 19:14*

So that is a plus 5 volts on pin3 then - thanks !!!


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/LALZS2Ahh47) &mdash; content and formatting may not be reliable*
