---
layout: post
title: "Azsmz mini with Ethernet: When I chose to buy the simple board without an ethernet interface to run smoothieware I did not realize that VisiCut needed a network connection to function properly"
date: November 25, 2015 08:42
category: "Smoothieboard Modification"
author: "David Richards (djrm)"
---
Azsmz mini with Ethernet﻿:



When I chose to buy the simple board without an ethernet interface to run  smoothieware I did not realize that VisiCut needed a network connection to function properly. To carry on I have adapted my Azsmz mini board by connecting an Ethernet phi module to the tiny pcb holes for this purpose available on the board.



I could not get this to work initially, there were two problems. the firmware needed to be updated but even then I had to set a static ip address in the configuration file.



The board is still not installed into my laser cutter cabinet but after some more testing this step may now be getting closer.



The board I used is this one: [http://www.waveshare.com/lan8720-eth-board.htm](http://www.waveshare.com/lan8720-eth-board.htm) which I found for sale on the internet, Attached is a photo of the wiring hack of it in use.

![images/08bb4e64612c59e593e12170ca0a02a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/08bb4e64612c59e593e12170ca0a02a8.jpeg)



**"David Richards (djrm)"**

---
---
**Stephane Buisson** *November 25, 2015 09:56*

Originally **+Thomas Oster** wanted to have both usb and ethernet as you can see here

[https://github.com/t-oster/LibLaserCut/pull/23](https://github.com/t-oster/LibLaserCut/pull/23)

USB wasn't implemented (maybe for time reason) before last september merge to main branch. (Smoothie driver isn't final)



you have a interesting answer by Thomas to this post here:

[https://github.com/t-oster/VisiCut/issues/312](https://github.com/t-oster/VisiCut/issues/312)



**+david richards** **+David Cook** **+Arthur Wolf** 


---
**Thomas Oster** *November 30, 2015 08:46*

That is not true. USB is implemented. It is not yet confirmed to work on all platforms, but it should. Just set the hostname and upload URL to "" and you can use the serial-port setting. On Linux is should be something like "ttyACM0" (not /dev/ttyACM0) and on Windows I think COM1, but this needs more WIndows people to test.


---
**Stephane Buisson** *November 30, 2015 09:21*

 Thank you **+Thomas Oster**, for this precisions, it was not much info in the wild about the path.


---
**David Richards (djrm)** *December 01, 2015 19:07*

**+Thomas Oster** Hi, useful to know visicut should work using serial but I havnt got it to work myself that way yet. I've tried both usb serial second serial. I'll try and capture the traffic and see whats going wrong. Kind regards, David.


---
**David Richards (djrm)** *December 01, 2015 21:41*

I've tried capturing serial data and although the port opens I see no data being sent from visicut when I execute a small circle test job. 



The tool used to capture is Virtual Serial Port Emulator. I can redirect pronterface through it without a problem. visicut holds the port open after failing to communicate, I then have to close visicut to try again.



The second serial port is connected to an ESP8266 serial to wifi board soldered under the LCD display, this is working well enough in Pronterface too. I'm using Jlabs ESP-Link software for that.



n.b. Nothing has been tried on the laser yet, ymmv. David.


---
**Thomas Oster** *December 02, 2015 07:39*

Could you set up a VisiCut dev environment as described here [https://github.com/t-oster/VisiCut/wiki/Development:-Getting-started](https://github.com/t-oster/VisiCut/wiki/Development:-Getting-started) and then test what exactly happens?Also make sure you are using the latest and greatest VisiCut and maybe delete (after backup) your .visicut folder, so there are no leftovers from old config files.



Did you try to run VisiCut from command line in order to see the debug output?


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/DNPNtPMuBTY) &mdash; content and formatting may not be reliable*
