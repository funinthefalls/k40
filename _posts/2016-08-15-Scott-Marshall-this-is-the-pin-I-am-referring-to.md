---
layout: post
title: "Scott Marshall this is the pin I am referring to..."
date: August 15, 2016 06:28
category: "Smoothieboard Modification"
author: "Alex Krause"
---
**+Scott Marshall**​ this is the pin I am referring to... I set mine up as a single pin setup to use the Pot as a max output... have you put a volt meter on this connection while the m2 nano board is firing to see if the nano board is supplying a voltage to that line other than 5v?

![images/759ab5a859d6f6394abbd508c7618267.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/759ab5a859d6f6394abbd508c7618267.jpeg)



**"Alex Krause"**

---
---
**Scott Marshall** *August 15, 2016 06:30*

Yep that's 100% digital - low to fire.



I'm saying check the 3 pin white XH sytle connector the pot feeds. You may just have a bad pot.


---
**Scott Marshall** *August 15, 2016 06:34*

To do a quick check,  unhook it from your controls and ground it. you should have full control with the pot. if not, there's something wrong with the psu, not your hookup.


---
**Alex Krause** *August 15, 2016 06:38*

My Pot has always been pretty crappy with stock setup I had about 30 degrees of rotation from 0 - 20ma


---
**Scott Marshall** *August 15, 2016 06:40*

That's not right. I've heard of the knob or pot guts slipping and limiting travel. That would do it.


---
**Alex Krause** *August 15, 2016 06:41*

I can do a "test fire"button I have full control over the power with the pot


---
**Scott Marshall** *August 15, 2016 06:42*

That eliminates that problem


---
**Scott Marshall** *August 15, 2016 06:43*

Try turning off PWM in config. Let it revert to digital (like mine still stuck on)


---
**Scott Marshall** *August 15, 2016 06:43*

you don't have a scope do you?


---
**Scott Marshall** *August 15, 2016 06:49*

I figured this out, but it's kind of complicated. Can you call me?


---
**Alex Krause** *August 15, 2016 06:56*

**+Scott Marshall**​ unfortunately I can't on my way to bed. Have to work the next 2 days 13 hour shifts and I'm already 4 hours past my bed time... going to be a long day tomorrow with 4 hours of sleep :(


---
**Scott Marshall** *August 15, 2016 07:00*

OK, the best quick explanation is:



The l pin is connected internally to the ground pin on the pot -  when pulled low, it pulls down to the setting of the pot, (the pot IS the pullup resistor). I know in your circuit, you've used the mosfet output on the smoothie, but didn't use a jumper or tie the drain to Gnd, so it's floating within the limits of the mosfet circuit. You don't have a solid 0v reference to pull down to.


---
**Don Kleinschnitz Jr.** *August 15, 2016 12:21*

**+Alex Krause** I answered this on your other post on the subject...... the M2 pulls "L" low for firing the laser.


---
**Alex Krause** *August 16, 2016 02:33*

**+Scott Marshall**​ moved my pwm wire to the breakout pin of 2.4 on the board. I have full use of the pot now while cutting thanks for your help


---
**Scott Marshall** *August 16, 2016 03:18*

**+Alex Krause** 

That's Karma at work.



 If you hadn't tried to help me, I wouldn't ever have seen your circuit, or analyzed it on the Smoothie schematic and wouldn't have been able to help you.



Kinda makes your head hurt if you think about it too much.



Scott


---
**Alex Krause** *August 16, 2016 03:21*

Lol **+Scott Marshall**​ when **+Ariel Yahni**​ said you needed help my first reaction was... what can I make to grab his attention that I have working gray scale... will send you a message real quick what I came up with ... never posted it because it's kinda silly


---
**Ariel Yahni (UniKpty)** *August 16, 2016 03:24*

**+Scott Marshall**​​ does this helps your own cause? ﻿I mean have you solver your yet? 


---
**Alex Krause** *August 16, 2016 03:27*

**+Scott Marshall**​ I really would like to take one of your boards for a test drive see if I can get the pwm to work 


---
**Don Kleinschnitz Jr.** *August 16, 2016 19:15*

**+Alex Krause** **+Scott Marshall** Guys what was the conclusion on all this, I am dizzy? What pins were used for what?


---
**Alex Krause** *August 16, 2016 19:40*

**+Don Kleinschnitz**​ I will send you a breakdown of my setup tonight or tomorrow... is there a email to contact you on your Don's Blog?


---
**Don Kleinschnitz Jr.** *August 17, 2016 12:10*

**+Alex Krause** don_kleinschnitz@hotmail.com


---
**Darren Steele** *September 04, 2016 19:12*

Can you please post it here somewhere?  I've read the whole conversation with much interest and now I don't know what the conclusion was/is.  


---
**Ariel Yahni (UniKpty)** *September 04, 2016 20:27*

**+Darren Steele**​ what do you need


---
**Darren Steele** *September 05, 2016 08:49*

I'm keen to see which smoothie pins **+Alex Krause** connected to which PSU pins and what the associated config file looks like.  I seem to be going in circles trying to get PWM going.


---
**Ariel Yahni (UniKpty)** *September 05, 2016 11:24*

**+Darren Steele**​ he used pin 2.4 ( not the Mosfet, but he actually used both) through the level shifter to the L displayed in the image above


---
**Alex Krause** *September 05, 2016 15:15*

**+Darren Steele**​ - [https://plus.google.com/109807573626040317972/posts/EwUxBEkC3vp](https://plus.google.com/109807573626040317972/posts/EwUxBEkC3vp)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/UtLt6jfsA7C) &mdash; content and formatting may not be reliable*
