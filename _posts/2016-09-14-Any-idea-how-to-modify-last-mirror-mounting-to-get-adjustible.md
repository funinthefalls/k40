---
layout: post
title: "Any idea how to modify last mirror mounting to get adjustible ?"
date: September 14, 2016 07:06
category: "Modification"
author: "Damian Trejtowicz"
---
Any idea how to modify last mirror mounting to get adjustible ?

To get better quality and full laser power laser need to be on lens center but with design its not possible?





**"Damian Trejtowicz"**

---
---
**Alex Krause** *September 14, 2016 07:10*

I had to take my laser head bracket off and file the burs they left durring machining after that beam exits air assist perfectly center before that it hit the front of the air assist cone


---
**Alex Krause** *September 14, 2016 07:11*

Had metal burs between the bracket and the standoffs that hold it in place


---
**Damian Trejtowicz** *September 14, 2016 07:19*

Well,but this in not right mirror adjustment,you angle your laser head,if will be able to set up mirror ,head can be fixed in right position.

In way what you said ,you just nake beam 90 degree to cutting surface ,but beam is still out of lens center


---
**Alex Krause** *September 14, 2016 07:20*

The beam should actually hit a bit high on the last mirror... not directly centered 


---
**Scott Marshall** *September 14, 2016 07:20*

Up/down can be done with shims (washers that shifts the target on X axis) and rotating  the head shifts the Y. It takes some fussing  but it's doable. (I'm using the Saite square body head). 



I've got a little mill and lathe, but just hack about... 

Maybe some of the machine shop guys can build us some nice micrometer adjustable ones.....


---
**Alex Krause** *September 14, 2016 07:23*

![images/0437fcb106ae889914b8f9aa675f44a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0437fcb106ae889914b8f9aa675f44a6.jpeg)


---
**Alex Krause** *September 14, 2016 07:25*

^^^^^ that is why alignment of beam shouldn't be directly centered in the head for the last mirror


---
**Scott Marshall** *September 14, 2016 07:28*

As long as it leaves vertical, nothing else matters....



I don't fuss trying to hit the fixed mirrors anywhere in particular, just keep away from the edges. If the beams are parallel to the motion they stay fixed, then you work it vertical (both axis) with the final mirror and you've got it nailed. Seems to hold quite well once you get it dialed in..


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 14, 2016 12:03*

I imagine on those air-assist heads with a very small output nozzle that getting it exiting as close to centre is important. My air-assist doesn't block any of the lens, so vaguely centred is good enough for mine.


---
**Jim Hatch** *September 14, 2016 13:44*

**+Yuusuf Sallahuddin**​ spot on. Mine would hit the inside of the air assist cone until I got everything centered (& I did in fact have to shim a mirror mount with tiny washers).


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/2fEX4HtFNzk) &mdash; content and formatting may not be reliable*
