---
layout: post
title: "I discovered a weird bug in laser web 3 where would i repoty it or get an explanation as to why its doing it?"
date: July 10, 2016 04:21
category: "Discussion"
author: "Derek Schuetz"
---
I discovered a weird bug in laser web 3 where would i repoty it or get an explanation as to why its doing it?





**"Derek Schuetz"**

---
---
**varun s** *July 10, 2016 04:27*

**+Peter van der Walt**​


---
**Derek Schuetz** *July 10, 2016 04:29*

**+Peter van der Walt** it won't cut circles that are in .DXF files in the correct location it moves them all to the origin when a gcode is generated 


---
**Ariel Yahni (UniKpty)** *July 10, 2016 04:34*

**+Derek Schuetz** please post you issue here [https://github.com/openhardwarecoza/LaserWeb3](https://github.com/openhardwarecoza/LaserWeb3). Include your file. I can confirm your finding


---
**Derek Schuetz** *July 10, 2016 04:53*

I'm generating .dxf in fushion 360 can you do poly lines that way? **+Peter van der Walt** 


---
**Derek Schuetz** *July 10, 2016 05:02*

just tried an addon same issue


---
**Derek Schuetz** *July 10, 2016 05:20*

**+Peter van der Walt** all posted with several examples on the control panel is the one i tried to use poly line adon


---
**Derek Schuetz** *July 10, 2016 05:45*

No rush I already left it aside before I go insane 


---
**Derek Schuetz** *July 10, 2016 06:57*

**+Peter van der Walt** that is really weird I even opened another window and it did it also


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/j3g7UZYcfhG) &mdash; content and formatting may not be reliable*
