---
layout: post
title: "Ok, I got off on another binge this morning regarding buying and upgrading a K40"
date: July 23, 2017 19:11
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40GettingStarted



Ok,  I got off on another binge this morning regarding buying and upgrading a K40. I recall my journey quite painfully (like its over ....not) and started thinking of how to combine a massive amount of info into a gauge. A gauge that those considering a laser machine could use to understand what they are getting into  and consider what it would take to get to a machine that meets their needs.



The approach is to create a post that contains some high level considerations and then include a linked chart that sorts K40 upgrades into categories. The  upgrade chart summarizes the needed changes and their benefits. 



Turns out this is pretty complex... go figure.



......................



<b>Community </b><i>THIS POST IS A WIP</i><b> that would benefit from your input</b>



<b>1.)</b>  If you are so inclined; posting changes you would recommend and I will add it to the final post and/or the chart.

You can also add "comments" to cells on the chart but please do not change the cell contents as I am sure some of these will be debatable.

Wherever possible include a link to related information. 



<b>2.)</b> When we are happy with the content I will link it into the "Getting Started .... Need Help?" post and remove this header.





............ The main content the "post to be".............................



<b>Introduction</b>



This post is designed give those thinking of purchasing a K40 a full understanding of what purchasing, modifying and operating a K40 machine will entail.



<b>Overview of the stock K40 Experience</b>



The K40 machine is fundamentally a very capable machine but in general you should <b>NOT</b> expect it work "out of the box". Its out of box operation is dependent on its quality of shipping and manufacture, both of which can vary widely from vendor to vendor and for each shipping event. 



If you are a DIY'r that is patient and willing to learn, you can successfully operate a stock machine or like most of us modify your machine to take advantage of its advanced functionality. 



If you are not a DIY'r and/or want a better "hands off" kind of experience we suggest you purchase a machine from a vendor that has better quality and support.



If you want to take the time and spend some more $ you can successfully upgrade your K40 to some pretty impressive additional abilities. This journey will not be without its frustrations but at the same time it cures many of the frustrations that the stock machine inherently possess. 



There are many users in this community that operate stock machines as well as heavily modified ones. What changes you make in your machine are dependent on what level of safety you want to operate under, what quality of marking/cutting you want and finally, what kind of tool chain you want to operate with.



<b>Support</b>



K40 machines are purchased from eCommerce sites that source these machines from China. Your "initial" support will vary wildly by vendor and situation and after sales support is usually problematic.



That said this community is backed up by many professionals (engineers, chemists, tradesman, artists, craftsmen etc ....) as well as DIY'rs that can apply a gaggle of experience to help you solve any problem you may encounter with your K40.  



<b>Upgrading your K40</b>

Thanks to this community, upgrading your K40 can be successfully accomplished at many levels. The attached spreadsheet chart attempts to categorize and identify these upgrades from the simplest "safety" to the most complex controller types. 



<b>Supporting your K40 upgrade</b>

Support for DIY machine upgrades and purchased controller upgrades is excellent from our linked G+ communities. In addition to this community you are linked into communities like LaserWeb, C3D with responsive members willing to help.



Ultimately what you choose to undertake should be based on; what you want to do with your machine aligned with your skills and your desire to learn from the communities collective experience.



Here is the K40 Upgrade chart's prototype:



[https://docs.google.com/spreadsheets/d/1tQYiqoDSvdqNZgWxHidU3Ku4eHd-Jeco886ll-Ut0qo/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1tQYiqoDSvdqNZgWxHidU3Ku4eHd-Jeco886ll-Ut0qo/edit?usp=sharing)



............. END of the main content the "post to be".............................









**"Don Kleinschnitz Jr."**

---
---
**Anthony Coafield** *September 14, 2017 05:54*

This looks great. Is there also a list that states which upgrades constitute A, B, etc? 




---
**Don Kleinschnitz Jr.** *September 14, 2017 11:44*

**+Anthony Coafield** it starts at ROW 62??? 


---
**Anthony Coafield** *September 15, 2017 03:56*

**+Don Kleinschnitz** <b>face palm</b> Thank you!




---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/bsa2PUhQhf1) &mdash; content and formatting may not be reliable*
