---
layout: post
title: "Printed a photo on my laser printer, decoupage'd it to 3mm plywood with Mod Podge, then cut it with a puzzle pattern, 2 passes at 20mm/sec ~10mA to try to minimize burning..."
date: August 06, 2016 03:21
category: "Object produced with laser"
author: "Tev Kaber"
---
Printed a photo on my laser printer, decoupage'd it to 3mm plywood with Mod Podge, then cut it with a puzzle pattern, 2 passes at 20mm/sec ~10mA to try to minimize burning... except somehow I forgot to switch on air assist, so there was some flame! 



I had protective tape on it, though, so not much damage to the image.  The image did peel up a little in a couple places (shown here), so I Mod Podge'd em back down, not sure how durable it will be.



Durability test will be: give it to my 2-year-old, who loves puzzles.

![images/7f5bb48ede37af3a5096401e2627ed18.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7f5bb48ede37af3a5096401e2627ed18.jpeg)



**"Tev Kaber"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 04:34*

Nice work. Seems to have come out pretty well with minimal charring on the edges.


---
**Robi Akerley-McKee** *August 08, 2016 05:41*

Air assist really helps keeping the the charring down.  I changed both of my moshidraw boards over to a MKS 2560 (Arduino 2560/RAMPS 1.4).  I put a 10x15mm drag chain with a silicon hose to a 3d printed air nozzle. Using a Select Comfort air bed pump.  I've also modded the Turnkey Laser Exporter Inkscape extension to also handle repeat=x in the layer names.  So I have vector and raster cutting and engraving.


---
**Bob Damato** *August 08, 2016 16:44*

That looks really great! What if you cut from the other side? Make the first pass kind of heavy, then when you are about to cut through, lower the power so you dont char the paper.




---
**Tev Kaber** *August 08, 2016 16:52*

It actually didn't char too badly cutting from the front, despite my somehow forgetting to turn on air assist (first time for everything, I guess!)



I also need to align my mirrors, something I've been putting off since I got the K40, I noticed the cut is not as deep in the lower right.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 08, 2016 21:49*

**+Tev Kaber** It turned out nice. The cut being not as deep on the lower right could be alignment, but could also be the distance between the head & the cutting area is not level in that corner.


---
**Tev Kaber** *August 08, 2016 21:52*

Oh good point, I should make sure my bed is level. I just have 4 long screws with leveling nuts holding up a honeycomb.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 08, 2016 21:56*

**+Tev Kaber** Hopefully it is just slightly out of level in comparison to the carriage movement. Would be much easier to adjust than an entire alignment.


---
**David Cook** *August 09, 2016 20:22*

this is very cool !   nice work


---
**Er Brad** *September 06, 2016 06:37*

Where did u get the jijsaw file


---
**Tev Kaber** *September 06, 2016 18:24*

[boxdesigner.frag-den-spatz.de - Boxdesigner - Vorlagengenerator für Holzkisten mit Fingerverzahnung und vieles mehr](http://boxdesigner.frag-den-spatz.de)



A great site with a lot of pattern generators for boxes and things. UI is a little clunky but functional.  It's €13 to enable download as SVG, but well worth the price. 


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/h1rMELiKL2c) &mdash; content and formatting may not be reliable*
