---
layout: post
title: "New question what causes ghosting, I only few one line"
date: July 23, 2016 04:05
category: "Original software and hardware issues"
author: "David Spencer"
---
New question what causes ghosting,  I only few one line. 

![images/077e79ecd503d6a8aa72d73a8accffa5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/077e79ecd503d6a8aa72d73a8accffa5.jpeg)



**"David Spencer"**

---
---
**Gee Willikers** *July 23, 2016 04:07*

Likely the beam is off center reflecting around in the head on the carriage, or in the air assist head if you have one.


---
**David Spencer** *July 23, 2016 04:10*

I have checked it and it is in the center.  I will check it again


---
**David Spencer** *July 23, 2016 04:12*

I have checked these mirrors so many times today, I can't see straight anymore.  I will check then again in the morning. 


---
**Gee Willikers** *July 23, 2016 04:13*

Oh, I've been there.


---
**Phillip Conroy** *July 23, 2016 05:05*

Make sure the thing u are cutting is a soild object or has very thin lines 0.01mm ,chekt that it is only cutting the lines once .chect didtace from bottom of focal lens to middle of work peace is 50.5mm


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 23, 2016 08:18*

Isn't focal point 50.8mm? Either way, ghosting tends to be caused by misalignment or as Gee Willikers said from reflections. Could also be some dirt/dust or a scratch on the lens splitting the beam?


---
**greg greene** *July 23, 2016 13:03*

I've seen this happen with excess flame/smoke


---
**David Spencer** *July 23, 2016 20:09*

to much power and smoke was the culprit


---
**Evan Fosmark** *July 25, 2016 17:11*

I had this exact same thing happen to me. The problem was the focusing lens. First, I had it installed upside-down. The flat side should be on the bottom. Second, I didn't have the focal length set up to the correct height.



After fixing both of those, the ghosting went away and the cuts were MUCH cleaner.


---
**greg greene** *July 25, 2016 20:16*

was it a LO Lens?


---
**David Spencer** *July 25, 2016 20:18*

no stock lens


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 25, 2016 21:09*

**+greg greene** LO lens is one from LightObjects.com. You can get various sizes of lens with different focal points. Larger lens is supposed to be better (although I can't say from experience as I still use stock).


---
**greg greene** *July 25, 2016 21:17*

I habe the 18mm lens - didn't know there was a top and bottom to it though


---
**Robi Akerley-McKee** *August 08, 2016 06:22*

18mm lens is probably the diameter, it could still be a 2" (50.8mm) focal length.  My lens are 20mm in diameter. I have my stock 50.8 focal and I also bought a 101.6mm focal length as well.


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/ddRHowj3aoh) &mdash; content and formatting may not be reliable*
