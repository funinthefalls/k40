---
layout: post
title: "Thanks to pressure from HalfNormal I am now the proud owner of a second slightly pale looking M2-nano powered K40"
date: February 22, 2018 06:06
category: "Discussion"
author: "Andy Shilling"
---
Thanks to pressure from **+HalfNormal** I am now the proud owner of a second slightly pale looking M2-nano powered K40. At a a very reasonable price it now seems this little fella has never actually been fired. Bed size seems to be 330 x220, the usual K40 safety devices (not) installed. None pwm power supply. 



I quickly made up a male to male usb lead and installed K40 Whisperer, a few moments later I was jogging the head around and test firing the laser, I'm pleased to report everything works as it should and even the mirrors are aligned.



Soooooo the question is 



1 do I bother to do anything with this or keep it for spares



2. Is K40 Whisperer any good and should I stick with it



3. Do I upgrade the hell out of it like my first one or just keep life simple.





**+HalfNormal** this is all your fault so I expect some input from you 😉





![images/821134c0b5e45957ecd3e39baccd5f0b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/821134c0b5e45957ecd3e39baccd5f0b.jpeg)



**"Andy Shilling"**

---
---
**Anthony Bolgar** *February 22, 2018 06:45*

Keep it simple if you already have a tricked out one. K40 Whisperer is pretty good, easy to use but very basic.




---
**Andy Shilling** *February 22, 2018 06:53*

**+Anthony Bolgar** thanks I did see it was basic but I kept getting a usb timeout error whilst jogging, whilst I understand my homemade usb lead isn't great I want sure if the m2-nano warrantied a new usb lead or just replacing. 



Does Whisperer do pwm? I can fit my old 40w psu in if it does.


---
**Anthony Bolgar** *February 22, 2018 07:02*

No, it uses dithering, but is quite good. For true greyscale you would need to change the M2Nano to something like Ray's C3D mini board. The Nano does not do greyscale.


---
**BEN 3D** *February 22, 2018 07:24*

Shure? I think You could set it in 40k whisperer, but have not tried if it works.


---
**HalfNormal** *February 22, 2018 15:43*

**+Andy Shilling** laser hoarders unite! I kept my second K40 stock because I was always trying something new on my tricked out one. That way I had one available to use while the other was unavailable. I also would rob parts now and then when I needed to. I had actually purchased it to remove the bottom so that I could engrave larger areas, but I never did that. Sounds like you got a real bargain. Enjoy!


---
**Andy Shilling** *February 22, 2018 16:25*

**+HalfNormal** that sounds like a good idea, I might just stick with whisperer but see if I can connect both machines to the same extractor and water reservoir just to keep the for taking up to much space. Luckily being here in the uk keeping the water cool isn't a problem lol


---
**James Rivera** *February 22, 2018 18:56*

**+Andy Shilling** If I understand it correctly, the main thing that upgrading the board gets you is PWM control, which is only necessary if you want better raster engraving, right? If my assertion is correct, then you already have that covered, so I’d only make minor changes to improve efficiency, like mirrors, lenses, and especially air assist. But you are free to go crazy with it, too.  😉


---
**Andy Shilling** *February 22, 2018 19:01*

**+James Rivera** yes everything is covered and I have just realised I have made my old one 60w ready so I'll keep the new one for lighter jobs I guess.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/Mj9VpDnwqZ1) &mdash; content and formatting may not be reliable*
