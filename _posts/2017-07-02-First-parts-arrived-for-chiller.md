---
layout: post
title: "First parts arrived for chiller"
date: July 02, 2017 18:26
category: "Modification"
author: "William Kearns"
---
First parts arrived for chiller

![images/d3360500d1ec5b549b50cfd5c39c87f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d3360500d1ec5b549b50cfd5c39c87f2.jpeg)



**"William Kearns"**

---
---
**Steve Clark** *July 02, 2017 19:58*

Are you doing a Peltier system?  If so, what is your layout?


---
**William Kearns** *July 02, 2017 20:54*

**+Steve Clark** I am doing that. I haven't yet decided on the flow yet but to keep the system from working to hard I am adding the radiator to help take the temp Down before it hits the chiller. It will be in an air condition room in the mid 70's 


---
**Steve Clark** *July 02, 2017 21:10*

Got it. How much functional Peltier wattage are you starting out with? I'm just curious as I'm building a system right now and have posted here a lot on the subject. I'm always interested in what others are using and doing.


---
**Joe Alexander** *July 03, 2017 02:33*

your not concerned that the radiator might counter the cooling effect of your peltier by warming your water back to ambient? was one of my concerns as I am also building a peltier loop,


---
**James Hughes** *July 03, 2017 03:44*

You don't want to get it too cold. In a room cooled to the 70s the radiator should be sufficient. I  have a full spectrum pro with one of their chillers and I'm convinced it's just a radiator and a fan. Works great but when the ac was turned off I saw the water temperature creep up a few degrees. Turned on the ac and it dropped back down.  Too cold can cause condensation and sparking, not something you want. 


---
**William Kearns** *July 03, 2017 03:48*

**+James Hughes** it will be regulated by a temp controller so I can set the temp and monitor the humidity 


---
**Joe Alexander** *July 03, 2017 04:02*

gotcha, and yea i knew about the condensation risk but as I live in california its usually not an issue.


---
**HP Persson** *July 03, 2017 07:26*

A radiator in a peltier loop will actually heat the water.

As you are heat exchanging the room temperature trough the radiator and can never get below ambient temps in it.



So whatever your ambient temps are - is what you will be heating/cooling the water to trough the radiator + a few degrees as it´s not 100% efficient.


---
**William Kearns** *July 04, 2017 07:54*

**+HP Persson**  couldn't the radiator work to bring the temp down before it hits the peltier ?  


---
**HP Persson** *July 04, 2017 08:14*

**+William Kearns** It depends



Example:

Room temp: 22c

Water temp: 22c (before running)

Peltier out temp: 15c



If you pre-cool the tank, down to lets say 18c before starting the machine. 

You will add 22c temperatures to the radiator and heat the water.

With time, as the hot-side of the peltier is air cooled the room temp will go up, and you will heat the water with hotter and hotter air trough the radiator.



Radiators works good to delay the heat up of the coolant water, but if you have a input of cooler than ambient water, it will act as a heater.

Example: coolant is 20c, room temp is 22c  - you are blowing 22c air on 20c water.



But, it´s all about time too, how long runs you do and what ambient temps you have within that time span.

But you can never get the radiator cooler than the room temperature + some degrees.



Only solution where it works is when the peltier output is not enough and the coolant-temperature is above room temp, in that scenario the radiator will help a bit.

Example: Room temp: 22c

Water temp: 25c

Peltier out temp: 15c

here is a option, where the output of the laser tube could go trough the radiator before hitting the peltier. You´ll maybe remove a degree or two on the peltier output temp. Instead of in-temp on the peltier is 25, the radiator cools it to 23-ish.



I would suggest this, have temp-probes on the loop to see what temperatures you have where.

One in the coolant tank, to see a baseline of the water

One on the outlet from the tube

One on the inlet, or directly after the peltier.



You could have the radiator installed 24-7, and only run the fans when coolant temp is above room temp, to make sure you are not heating the water :) (you could use w1209 thermostat to automatically solve this, $4 item from ebay)




---
**Steve Clark** *July 04, 2017 15:52*

**+HP Persson**   I agree, I considered leaving my ambient radiator system inline with the new but decided against it for the very reason HP stated. 


---
**William Kearns** *July 04, 2017 19:20*

**+Steve Clark** one could almost just run the radiator in an air conditioned work space then?


---
**Steve Clark** *July 04, 2017 19:55*

**+William Kearns** I monitored the differences between the ambient and water temp during runs and found that as long as my shop temp stayed below 28-27C the radiator  the water (5 gallons) temp moved up to around 2.5 to 3 degrees above that room temp (3 hrs. of run in this case)



I think it's a lot dependent on how long of a cycle your running the laser, starting time of the water and power used.



If my budget was tight I would use a frozen plastic water bottle(s) in a 5 gallon tank and just make sure my temp was above the dew point.






---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/2tncwZwy5UC) &mdash; content and formatting may not be reliable*
