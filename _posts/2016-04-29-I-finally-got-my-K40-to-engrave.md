---
layout: post
title: "I finally got my K40 to engrave"
date: April 29, 2016 12:19
category: "Discussion"
author: "John von"
---
I finally got my K40 to engrave. However, the text on the wood is wavy. Is the speed too fast? I set the pixel to 2 and what is the best speed?





**"John von"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 12:39*

Pixel to 2 increases the speed but reduces the resolution of the engrave. I rarely set it to anything but 1 as I don't like the spacing between dots. 2 is not too bad, but that could be the cause of the wavy text. I nearly always use 500mm/s for the engrave speed, so I highly doubt it is your speed being too high.



Depends what you are engraving, but I would use about 6mA power & 500mm/s on 3mm ply. I am doing a piece currently as a test that is using 5mA power & anywhere from 500mm/s - 240mm/s (14 levels of engraving, different speed equates to stronger or lighter burn). I'm onto the 8th layer now, (after about 2 hours) so still a while to go before I can share the results.



Also, you could do what I just did earlier on. Make a 5mm x 5mm square & set the power to say 4mA. Then engrave 1 at 500mm/s, another beside it at 480mm/s, another at 460mm/s etc etc. Then I did the same for 6mA. Do it on a bit of scrap of the material you are wishing to engrave & write the settings down on it once completed & keep it as a reference swatch.


---
**Jim Hatch** *April 29, 2016 13:08*

**+Yuusuf Sallahuddin** Yuusuf has a good tip with the reference swatch. I have one I use for cuts and one for engraving. That lets me run a quick standardized set of cuts & engraves to the K40 (or any other laser I  use) when I try new materials. It's also a good way to see how your machine is performing as the tube ages or you do upgrades of optics.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 13:48*

**+Jim Hatch** I never thought of those extras (checking performance over time or as you do upgrades). Great points to note & I'll have to keep that in mind.


---
**John von** *April 30, 2016 00:13*

I sincerely appreciate your advice. Since I feel like a novice at using my K40, this greatly helps.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 00:43*

**+John von** We've all been there. You'll get the hang of it as you practice & play around. It's not the easiest machine to work with & has some annoying points in the workflow (namely the CorelDraw/CorelLaser issues) but it is functional. I got mine back in September 2015 & I'm still playing around learning things.


---
*Imported from [Google+](https://plus.google.com/112642592493783290997/posts/aNDnfc4mz4R) &mdash; content and formatting may not be reliable*
