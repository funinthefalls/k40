---
layout: post
title: "Does anybody know why my laser makes a squeling noise (high pitch) when it cuts but not all the time"
date: July 21, 2015 14:07
category: "Hardware and Laser settings"
author: "george ellison"
---
Does anybody know why my laser makes a squeling noise (high pitch) when it cuts but not all the time. Sounds like it hitting metal, honeycomb or laser head? Not sure.


**Video content missing for image https://lh3.googleusercontent.com/-ROC_kI4NZKE/Va5Ry-hqbFI/AAAAAAAAAGY/cbpCIIotlLU/s0/video-2015-07-21-13-36-40.mp4.gif**
![images/4990b4d81ca2b11c4006d433468264a7.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/4990b4d81ca2b11c4006d433468264a7.gif)



**"george ellison"**

---
---
**David Wakely** *July 21, 2015 15:16*

It could be the noise of the gases inside the co2 tube being energised - mine also makes similar noise normally when the laser fires for the first few seconds


---
**Joey Fitzpatrick** *July 21, 2015 15:43*

Mine makes the same noise when cutting or engraving only when a lot of material is being removed at one time(on one line).  I believe it is just the sound that is made when a lot of material is being vaporized by the actual laser beam.  If it only makes this noise during cuts or long engraving moves, that is most likely what is happening.


---
**george ellison** *July 22, 2015 07:42*

Thanks for the feedback, so is it ok to carry on using it or is it damaging the machine? Thanks


---
**Joey Fitzpatrick** *July 22, 2015 15:02*

As long as nothing is binding or rubbing you should be OK.  These k40 machines are all slightly different in their construction , so yours may have it own quirks.  Just monitor it closely to make sure the noise doesn't get louder.  As you use it more and more, you will become used to its own unique operation.


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/ctkQ1QGrvf5) &mdash; content and formatting may not be reliable*
