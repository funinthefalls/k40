---
layout: post
title: "Hi guys! hope you are all having a good day"
date: March 24, 2018 02:33
category: "Software"
author: "Ricky Ricardo"
---
Hi guys! hope you are all having a good day.  As for me its been a bit stressful.  I got this orangeish usb says Lihuiyu studio labs and I also got this blue silicone substance, have no clue what that is for.  but I am missing the CD for my unit can any one help with a link.  or is the software on the orangeish usb??





**"Ricky Ricardo"**

---
---
**Ned Hill** *March 24, 2018 02:56*

Hi Ricky.  The silicone is for sealing the electrical connections to the laser tube if needed.  Most people never use it.  The USB dongle is a software key lock which you have to have plugged in to run the  stock software.  Sounds like you have a M2Nano control board which comes stock with either LaserDrw or CorelLaser control software.  I would actually recommend using the K40 Whisperer software which can be found Here:  [scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)  You don't need the dongle for this.  Let us know if you have any problems. 


---
**HalfNormal** *March 24, 2018 03:36*

You can find the original CD here. Download the file that is marked RAR files.

[drive.google.com - K40 Software - Google Drive](https://drive.google.com/drive/folders/0Bzi2h1k_udXwUWJwM29COUNxSUU)


---
**Ricky Ricardo** *March 24, 2018 03:56*

**+HalfNormal** Thanks for all your information.  Sounds good so I will download the soft ware I will actually go with k40 whisper which is what you recommended.  Thanks my friend. 




---
**HalfNormal** *March 24, 2018 03:57*

Cannot go wrong with K40 Whisperer.


---
**Ricky Ricardo** *March 24, 2018 03:59*

**+HalfNormal**, Also I must add you where on point I just checked the board it is a M2nano :D


---
**HalfNormal** *March 24, 2018 04:01*

**+Ned Hill** Gave you all the original info so I cannot take credit! ;-) (he just beat me to it.)


---
*Imported from [Google+](https://plus.google.com/103955230338803856695/posts/W6S8ot57fAY) &mdash; content and formatting may not be reliable*
