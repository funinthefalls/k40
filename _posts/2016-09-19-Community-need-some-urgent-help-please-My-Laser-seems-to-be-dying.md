---
layout: post
title: "Community, need some urgent help please! My Laser seems to be dying!"
date: September 19, 2016 05:55
category: "Original software and hardware issues"
author: "Fadi Kahhaleh"
---
Community,

need some urgent help please!



My Laser seems to be dying!



Background:

After being busy with work, I didn't use my K40 for several months.

Recently I needed to cut a part, and I had a lot of issues doing so.



Granted it was a 6mm plywood (usually it requires two passes and 98% @ 7mm/sec settings)



However, I noticed something after fiddling away with alignment, cleaning the lens/mirrors...etc.



Observation:

The intensity of the DOT/Light at the wood piece is very dim.

And actually you can see it get dimmer and dimmer until it is almost gone!

My AMPs meter shows 18.5, so I know that we are hitting the 98% I setup.



Is this a bad CO2 Laser tube? probably nearing end of life!



Thanks





**"Fadi Kahhaleh"**

---
---
**Alex Krause** *September 19, 2016 06:42*

Not using for so long... are you sure your tube isn't an algee farm?


---
**Alex Krause** *September 19, 2016 06:43*

Lack of cooling water flow will deminish power


---
**Gunnar Stefansson** *September 19, 2016 09:27*

Had something similar happen to me, try and see if the Mirror in the back of your Tube is burnt as alot of the power will escape that way!


---
**Fadi Kahhaleh** *September 19, 2016 13:15*

Thank you both. Will check. I didn't see any blockage or greenish stuff inside so I guess no algee :)


---
**Scott Marshall** *September 20, 2016 02:07*

One thing not mentioned is the head which contains the final stage mirror and lens, It's got an opening into which insects can crawl. (just had a moth in the discharge end of my laser tube, think he fried it - he did die in a very starwars manner, but then again so did my laser tube).



Run it until it starts to dim, shut it down completely and fast, and check 1st the head (both lens and mirror) and each mirror. The one that's warm is the culprit and probably had a damaged or dirty surface absorbing the energy.



If your tube was bad, it's doubtful you would be getting 18ma thru it. Most probably optics dirty, misaligned or invaded by moths.... ugh. Hate the Invasive little life forms... Like those mud wasps that build homes in your air chucks.... and you don't know until you pull the trigger, then everythings grinds to a halt in a cloud of mud dauber/air vane dust.



Sorry, just lost a laser tube to a bug, still not happy with insects in general...



Be safe, and good luck.



Scott


---
**Fadi Kahhaleh** *September 21, 2016 03:15*

Thanks Scott.

I did take a look when I noticed this issue and didn't see any bugs thankfully :)

However very valid point.



I'll need to realign everything, take a look at optics again and clean. Hope for the best and update here for feedback.




---
*Imported from [Google+](https://plus.google.com/115068648817066933731/posts/7Raj6dLXnuh) &mdash; content and formatting may not be reliable*
