---
layout: post
title: "Been tinkering with my machine again :P This time i had a idea of removing heat from the case, the compartment for the tube to be clear"
date: July 14, 2017 21:55
category: "Modification"
author: "HP Persson"
---
Been tinkering with my machine again :P

This time i had a idea of removing heat from the case, the compartment for the tube to be clear.



What i did, i cut a hole in each side of the machine (had a lid on one side, only needed one hole though), and mounted two computer fans, one in each side. One pushing, one pulling.



Have tried the machine now and then, and the temperatures on the tube is of course down, but i see a power increase too.

If i let the machine rest and cool down both the power supply and the water to ambient, and run it again without the tube fans - i get lower power on the same settings.



If someone wants to try it out, i would be glad if you could report back with your data, not sure yet if i´m dreaming or just found out one more trick for our machines ;)



My data so far

Started with machine, water and all temps at ambient temps.

Four 50x50mm squares were cut on each run, the machine rested after each cut (of the four squares).

The machine was pre-cooled on the water and the tube-fans for 15min before each run. Power setting was identical for all runs.



With fans on

Ambient temp - water temp - power

23c - 17c - 10mA

23c - 18c - 10mA

25c - 19c - 9mA



Without fans on

Ambient temp - water temp - power

22c - 18c - 6mA

23c - 19c - 6mA

27c - 22c - 5mA



I was stupid enough to forget to put a temp sensor inside the tube compartment though...

What is going on here, can the ambient temp of the tube actually be this sensitive? Can the tube output be optimized with removing radiating heat from the outside, while the real heat is deep inside the tube?



Not found anything on any laser page or fact supporting this theory.

Cooling the tube is important, but i did/do not think the heat transfer of fans on a short run about 10min could do this, at all.

So i´m asking for your help and theories :)





**"HP Persson"**

---
---
**Phillip Conroy** *July 14, 2017 22:48*

Good idea,check your water flow rate,most pumps that come with the laser machines do not pump anywhere near the required 2 liters per minute that is needed for 40 to 50 watt [tubes.my](http://tubes.my) blue and white 50 watt sg h350 or k50 was only pumping 1 liter per min.i added and calabrate ed the flow sensor and changed to a 150 watt pond pump ,now get just over 2 liters per minute and more power at set currents 


---
**HP Persson** *July 14, 2017 22:50*

I have a dual pump setup with other pumps, so that could be ruled out. Good idea though, the pump included is craaap :)


---
**Don Kleinschnitz Jr.** *July 14, 2017 23:08*

If you recall I poked at this idea months back but no uptake :).

I was prompted by Full Spectrums use of air cooling using a heat sync and no water. 



Per your advice I got a temp meter for the tube and fans to try it.... you beat me to it and now I am overly curious ....



I am surprised at this much improvement by just adding air flow in addition to water. 



Thinking ..........

 


---
**HP Persson** *July 15, 2017 10:12*

**+Don Kleinschnitz** Yeah, i have a idea-book where i write down all stuff i need to try, probably you who delivered the idea when we spoke about tube temps last time in the group :)

Doing more tests today.



The only theory i have is that the core is very hot, the water cools it but it radiates and heats up the gasses in the outer shell of the tube. The airflow helps to cool off (remove heat) around the tube lowering the gasses temps, increasing efficiency.

Just a theory right now, partly based on science.

(cooler gasses = better efficiency = better regeneration)

But how, and if air cooling helps, needs more study on :)


---
**Nathan Thomas** *July 15, 2017 10:52*

Do you pics of the setup?


---
**HP Persson** *July 15, 2017 11:04*

**+Nathan Thomas** It´s simple, a fan on each side of the tube.

Changed now so both fans are pushing for more tests. (to prevent dust/smoke pulled out from the machine, or on to the mirror)

Won´t win any design awards though :P

![images/8d9b14d7a05042c3d18595ee6a0320ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d9b14d7a05042c3d18595ee6a0320ac.jpeg)


---
**HP Persson** *July 15, 2017 11:08*

Now when i think about it, one fan may be enough, pressurizing the chamber and with that pushing out the hot air...

Think i need to look into some solution to seal the lid, much air escaping there.


---
**Nathan Thomas** *July 15, 2017 11:08*

I like the idea and design, will add this to my to do list for one of my k40s


---
**Don Kleinschnitz Jr.** *July 15, 2017 12:19*

**+HP Persson** I have been thinking about mounting my laser in a separate removable enclosure. Then install and adjust that entire enclosure for alignment to the gantry. Perhaps the fan could be added to that assy. at one end. 


---
**Phillip Conroy** *July 15, 2017 23:50*

Do a flow test on pump anyway ,it is needed so other can compare complete [setup.the](http://setup.the) laser chamber on k40 are crap ,my tube was covered with crappie from mid that I couldn't even see into it 


---
**HP Persson** *July 16, 2017 13:06*

Swapping out the tube, hoses and pump for the next test.

I did two tests yesterday, they were the same as the first ones.

Maybe should try with another PSU too, and changing mirrors and lens, to make sure nothing interferes with the testing.


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/RpeaZTLZ1wC) &mdash; content and formatting may not be reliable*
