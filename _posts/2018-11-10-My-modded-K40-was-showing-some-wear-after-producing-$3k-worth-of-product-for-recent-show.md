---
layout: post
title: "My modded K40 was showing some wear after producing $3k worth of product for recent show"
date: November 10, 2018 17:30
category: "Discussion"
author: "Kelly Burns"
---
My modded K40 was showing some wear after producing $3k worth of product for recent show.  An hour of design and a few hours of 3D printing and she’s back to being a smooth work horse.  Next Show!

![images/b71f27678522454540c49e0b8b764d59.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b71f27678522454540c49e0b8b764d59.jpeg)



**"Kelly Burns"**

---
---
**Joe Alexander** *November 11, 2018 07:10*

what did you have to design/replace to get it smooth again? its not clear from the video.


---
**Kelly Burns** *November 11, 2018 14:19*

I made a new carriage for the lens and lens holder.  It’s similar to original design but with tighter tolerances on the wheels.  My original design was loose from m the start and the got extremely loose with wear. I also printed in PETG which should hold up better.  


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/GqDYP7VTYCy) &mdash; content and formatting may not be reliable*
