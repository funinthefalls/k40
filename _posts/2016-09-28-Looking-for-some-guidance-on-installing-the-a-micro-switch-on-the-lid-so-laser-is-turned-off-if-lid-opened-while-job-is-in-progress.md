---
layout: post
title: "Looking for some guidance on installing the a micro-switch on the lid so laser is turned off if lid opened while job is in progress"
date: September 28, 2016 16:17
category: "Modification"
author: "Nigel Conroy"
---
Looking for some guidance on installing the a micro-switch on the lid so laser is turned off if lid opened while job is in progress. 



**+Robi Akerley-McKee** posted some pics previously to help me out and I managed to install a key switch that needs to be turned on for the machine to start.



The images for the switch are a little different than mine so I'm just looking for confirmation so I don't get it wrong. 



I'm looking to take the wire from the on/off laser switch and run that to the lid switch first and then to the on/off switch. 



There are two blue and two red to the on/off switch. one red and one blue goes to the switch above. My question is should I connect the switch with the blue wire or the red?

Or do I need to trace the wire to figure out exactly what the wires are.



Any help appreciated. Thanks





![images/03b7101d3485bda51b5670f10bf475b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/03b7101d3485bda51b5670f10bf475b7.jpeg)
![images/a4c06a085bce163cd97ba03223425f64.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4c06a085bce163cd97ba03223425f64.jpeg)
![images/f5c7c059028bf6ddc8d26c31bd586766.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f5c7c059028bf6ddc8d26c31bd586766.jpeg)
![images/3e9bf2ee82436f87ee53f27a44ab9614.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3e9bf2ee82436f87ee53f27a44ab9614.jpeg)
![images/1cae2ed7d6be90b4498a588135c42026.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1cae2ed7d6be90b4498a588135c42026.jpeg)
![images/0f75f7411dffbc639b5f011167a1ff29.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f75f7411dffbc639b5f011167a1ff29.jpeg)

**"Nigel Conroy"**

---
---
**Scott Marshall** *September 28, 2016 16:31*

Don't wire it there. There's several reasons, the voltage is one, but the K40 guys actually designed in a place for this function (too bad they didn't implement it)



Put it in series with the "LASER SWITCH"  - this is a loop designed to shut down the laser and is for door switches, flow  switches etc. 

They used it only as a manual enable/disable switch, but it can shut down the laser on a variety of error conditions, you need only add the appropriate switches (closed for "normal" open for "fault") ans daisy chain them all. The switch is a handy place to cut into for door switches.



If you run the wire down the divider wall, you can interlock both the main door and Electrical box cover with 2 switches right next to each other. A 3rd on the Laser tube door wouldn't hurt (or put a screw in it)



It's a 5V loop, so is safe to work with and very low current so fine gauge wire can be used (22awg or heavier would be best) 



(the terminals it's hooked to on that style power supply are WP (the input which has to be pulled to ground to enable the laser) and Gnd



Use the "C" and "NO" connections on your switch(es)



Scott


---
**Nigel Conroy** *September 28, 2016 16:47*

**+Scott Marshall** thanks for reply. 




---
**Nigel Conroy** *September 28, 2016 17:41*

**+Scott Marshall** thanks for this, I think what I was suggesting in a roundabout way was what you've said. I like the idea of adding switches to the other doors and will definitely do that as I got a pack of 4...



Hopefully this drawing is clear and is actually what you mean?



[docs.google.com - Laser Switch / Lid Switch Diagram](https://docs.google.com/drawings/d/1LJpoKxb1n6sv_idFyrn7wsK1_UO03XX2QgTb8p4RJ3Q/edit?usp=sharing)






---
**Sebastian Szafran** *September 28, 2016 18:49*

You should wire the switch in series with Laser Switch. Use NO (normally open), so once the lid is closed, laser circuit is closed and when you open the lid switch will change to OPEN state and swith the laser OFF (the same way as you would set Laser Switch to OFF. Check wiring and search for most convenient connection. 


---
**Sebastian Szafran** *September 28, 2016 18:55*

**+Nigel Conroy**​ I got your post on my phone but not updated with earlier comments. I think **+Scott Marshall**​ already answered the question. BTW: this is a very nice machine.


---
**Nigel Conroy** *September 28, 2016 20:02*

**+Sebastian Szafran** thanks, yes I'm really happy with it so far. 



If you get a chance to look at the drawing I've linked and see if that's the best way to do it that would be great. 


---
**Sebastian Szafran** *September 28, 2016 21:01*

Your PSU for the laser looks just like mine. Assuming it is the same:

- BLACK wire (3rd from left side on the middle connector) is Laser Enable

- BLUE wire (4th from left side) on the same connector is GND



Please double check it first with the multimeter and make sure my guess is correct. Connecting BLACK and BLUE to GND will enable the laser, this is what actually Laser Enable switch does.



You can freely connect in series additional switches or sensors e.g. water flow, cooling water temperature, lid sensor etc. Make sure that each of the switches connected in series is closed during normal machine operation. Disconnecting any of the switches connected in series will disconnect the circuit and disable the laser.


---
**Scott Marshall** *September 28, 2016 21:16*

I can't tell from that diagram exactly what the 2 switches on the left are, but it looks as though they are 2 pole ac line switches (DPST) and are hooked to the AC mains. One is main power on and the other the power on for the water pump?



If so, that is wrong and will destroy your power supply. The "WP/Gnd" loop (called P+/P- on the other PSU variation) is a 5 volt DC loop which is self powered. By connecting the WP to gnd, an internal optoisolator is operated, enabling the laser fire circuit. Do NOT introduce any voltage to this terminal, especially line power. The ONLY line power going to the PSU is the Power IN, on pins 2,3,&4 of the left hand 4 pin connector. ALL control  circuits are 5 volt.



If you were to draw a vertical line down the center of the drawing, you would have the 2 red wires going to the right. cut one of the wires going the the "LASER SWITCH" switch and connect these 2 wires to the exposed ends. (either one goes either way). 



The switches on the left side are line power (breaking both lines with a DPST switch for each function - required in Europe and where they use 220v mains with 2 hot wires (no neutral) - this works fine in US 120v systems too, so is used on all K40s (that I have encountered) 



The Line voltage only goes to 'power in' on the PSU and to the water pump. 



Hope that makes sense (and that I understand what you are intending to illustrate with the diagram)



Scott




---
**Nigel Conroy** *September 28, 2016 22:07*

The switches on the left are there to represent the 4 switches on the outside of the machine (photo 4).



The wiring and colors are there to represent what is currently there.(photo 3) I'm not sure of origin of wires as I've not unraveled and traced them back to origin. 



Here is the switch outside the machine. KCD2 is what it says on switch.



Are you assuming it's AC from wire color or from how it's wired?



![images/20399e1957e1d74af8322fd19303f2c8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/20399e1957e1d74af8322fd19303f2c8.jpeg)


---
**Scott Marshall** *September 29, 2016 14:30*

I'm going by how it's wired.



Seeing the switch confirms they are line voltage switches, don't cut into those circuits at all, they are indeed line voltage.



 K40 wire colors are totally random.


---
**Nigel Conroy** *September 29, 2016 15:35*

**+Scott Marshall** Thanks for helping me with this. 

I was hoping that wasn't your response... So does this mean that the wiring is wrong and will destroy the PSU in it's current state?



There is already a water flow sensor built into the machine, could I use the loop that it is on to wire in the microswitches?



I'm scared to ask now, but I wired the key switch in with the emergency stop button, is that ok?


---
**Scott Marshall** *September 29, 2016 16:41*

If you wired it as per that diagram, don't turn it on.

It could indeed destroy the power supply, and possibly other things as well.  The input of the power supply uses an optoisolator to prevent inadvertent current flow to external device. 



An optoisolator works by using a photo transistor (light sensitive transistor) to turn and off the load (in this case enable the fire circuit)

This phototransistor is illuminated by an LED, which (in this case) is powered by an internal 5V power supply. Grounding the LED's negative terminal causes it to light. illuminating the phototransistor, thus enabling the firing portion of the power supply.



Applying 120v to enable input will probably destroy the OptoIsolator, and possibly blow the ground traces off the PSU circuit board. While damage may be limited by the optoisolator. The PSU will likely be damaged and at least need the services of a skilled repair tech.



The key switch should be OK if wired into the EMO switch as long as it's rated to safely carry the line voltage at the  rated current of the laser.



K40s were never built (to my knowledge) with a water flow sensor, so be wary of how it's wired (or maybe it's NOT a k40?) Either way do your homework and verify how the flow switch is wired before cutting into that circuit. I've seen some pretty scary stuff on this Chinese  gear (which is sold worldwide with NO inspections, NO UL, CSA, or CE Mark.)



Be safe, and have fun!



Scott


---
**Nigel Conroy** *September 29, 2016 19:19*

**+Scott Marshall** I truly appreciate all your input and time. I have not changed any of the wiring as per the diagram as I wasn't confident in what I was doing so sought advice here. 



The key switch I used is linked below.



The machine is not a K40 but a K50 (for want of a better name).



Listed here under 50w model

[http://www.ebay.com/itm/Co2-Laser-Engraving-Machine-Artwork-Cutter-Crafts-Cnc-Rotary-Axis-Cutting-/391502248971?var=&hash=item5b275a3c0b:m:mVq7SnyVv_bxpPOdnEwhl6g](http://www.ebay.com/itm/Co2-Laser-Engraving-Machine-Artwork-Cutter-Crafts-Cnc-Rotary-Axis-Cutting-/391502248971?var=&hash=item5b275a3c0b:m:mVq7SnyVv_bxpPOdnEwhl6g) 



I'll trace the water flow sensor and see where it connects. 

[ebay.com - Details about  Zinc Alloy Electronic On Off Two Terminals Keys Switch Lock Switch + 2 Keys](http://www.ebay.com/itm/152067236937?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Sebastian Szafran** *September 29, 2016 20:46*

**+Nigel Conroy**​​​ Possibly a better picture of the Laser PSU with connection names (this may require disconnecting wires and removing PSU cover, so maybe leave it for later time) as well as the picture of the white controller box with readable description could help. Even if the Laser Enable switch is hot-wired, the Laser-control information has to be sent to the Laser Enable pin on the Laser PSU (with some extra electronic to realize that function. 

I have K40 and all security related mods (lid switches, water flow sensor, water temperature sensors) are all hooked up in series to the Laser Enable pin and GND. By default my K40 Laser Enable pin has been soldered to GND pin without any security add-ons.



I'm located in Europe (CET timezone) but if we find convenient time sync I could assist you to figure out (Hangouts/Skype/WhatsApp/other) this stuff on Saturday.



EDIT: I accidentally removed part of the text while typing on my phone and updated after posting. 


---
**Nigel Conroy** *September 29, 2016 20:48*

Thanks **+Sebastian Szafran**

I'm away this Saturday and machine is at work but I'll try get the photo's you are talking about. I definitely appreciate the offer of assistance and will definitely take you up on it. 


---
**Nigel Conroy** *September 29, 2016 20:51*

![images/25e0841840e0789191dd90ba6f1b67b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/25e0841840e0789191dd90ba6f1b67b1.jpeg)


---
**Nigel Conroy** *September 29, 2016 20:51*

![images/2233b46c090fa44d14d684f8b50f54af.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2233b46c090fa44d14d684f8b50f54af.jpeg)


---
**Sebastian Szafran** *September 29, 2016 20:56*

**+Nigel Conroy** The ON/OFF key-switch you have shown should work fine, I installed identical on my K40 and plan to add another one to my little CNC. 


---
**Nigel Conroy** *September 29, 2016 21:23*

**+Sebastian Szafran** are those photo's the right ones?


---
**Nigel Conroy** *October 13, 2016 13:33*

Looking to get these interlock switches installed and I had the thought that I could use the wiring for the water flow sensor that came preinstalled on the Laser. 



If I cut into this and added all the switches in series with this would that work?


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/U4TasHPojH7) &mdash; content and formatting may not be reliable*
