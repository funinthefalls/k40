---
layout: post
title: "Hi all. I'm getting an issue with my K40 (the one with digital panel) with a Cohesion board and other mods"
date: April 11, 2018 11:15
category: "Original software and hardware issues"
author: "syknarf"
---
Hi all.

I'm getting an issue with my K40 (the one with digital panel) with a Cohesion board and other mods.

This morning when engraving as usual, suddenly the machine stops engraving, but movement was ok.

I made some test, start running a engrave test at very low power and slowly increasing the power setting. 

At low settings (ampmeter reads 3 - 4 mA) laser was firing fine, but when I increase the power,  there is a moment that it stops firing, and output current drops to 2mA and the lase was not firing at all.

Any idea of what could be wrong, tube or power supply?

Are ther any other tests that I could do to identify the problem?

Thanks and regards.

![images/1806e4445ad73cc83f0e77b055c77994.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1806e4445ad73cc83f0e77b055c77994.jpeg)



**"syknarf"**

---
---
**Andy Shilling** *April 11, 2018 11:46*

That's the flyback transformer in the PSU, you will find it will pack up completely very soon. You can either replace it if you can find a matching one or just replace your PSU.


---
**syknarf** *April 11, 2018 14:23*

**+Andy Shilling** Thanks. At least the flyback is cheaper than a new tube...


---
**syknarf** *April 12, 2018 12:10*

I made a video, where you can see the tube and output mA reads when firing at different values. Please take a look and tell me what do you think. (turn english subtitles ON).


{% include youtubePlayer.html id="j7POoh3r3NY" %}
[youtube.com - K40 co2 Laser, not firing at high power values.](https://youtu.be/j7POoh3r3NY)




---
**Don Kleinschnitz Jr.** *April 12, 2018 15:42*

**+syknarf** 

<b>WARNING</b> opening the LPS and or messing with the LPS internals is dangerous and potentially <b>LETHAL</b> and you do so at your own risk.



<b>YOU CAN KILL YOURSELF!</b>



What type of coolant are you using? If it is not pure distilled water or the water is old, change it with fresh distilled water and see if the problem continues. 



IF NOT....



The skreetching sound is the flyback shorting internally and you cannot always see it.



There are repair instructions and part # etc. on my blog if you accept the risks. Otherwise time for a  new supply.




---
**syknarf** *April 12, 2018 15:53*

**+Don Kleinschnitz** Hi. I changed all the coolant for new distilled water just before the video. 

Until the failure I was using distilled water with some antifreeze. I was using this mix since last november and always worked fine... could it be the source of the problem?

The PSU was open only to see if there was a visible arcing, and to identify the flyback model in case I need to replace it. 

So you also think the flyback is damaged?



Thanks for answer, and for the warning !


---
**Don Kleinschnitz Jr.** *April 12, 2018 16:20*

My guess is the flyback is bad. 

There is evidence that antifreeze can damage the supply.




---
**syknarf** *April 14, 2018 00:44*

**+Don Kleinschnitz** Just ordered a new flyback. Hope this were the problem. 

Thanks.


---
**Don Kleinschnitz Jr.** *April 14, 2018 09:44*

Let us know how it goes....


---
**syknarf** *April 20, 2018 10:41*

Yeah!! Working again. Installed a new flyback from k40laser (super fast shipping, thanks) and solved the issue. 

Now I notice that need less power settings to reach the same mA output than before. 


---
**Andy Shilling** *April 20, 2018 15:19*

**+syknarf** Great news.




---
**syknarf** *May 28, 2018 11:10*

Damm! Again same issue! Only 5 weeks after substituting the flyback. While running a job, machine stops and homing. Now only fires at very low power settings. 

I think there maybe something hidden that causes the issue, but don't know what can be...


---
**syknarf** *May 28, 2018 11:13*

There was no intensive use, and never raise the 8mA output power,  coolant is only distilled water with no other additives (no antifreeze this time)....


---
**Don Kleinschnitz Jr.** *May 28, 2018 12:25*

ah %$#!



Tell us more about this: <i>While running a job, machine stops and homing</i>



Try and run the machine in the dark and see if you can detect or hear any arching?








---
**syknarf** *May 28, 2018 13:02*

I was running a job from the Cohesion SD (cutting MD wood at 7mA  20mm/s with air assist), suddenly the air assist compressor stops (it is controlled by the cohesion board), the cut was interrupted and the laser head made a home movement. 

The file was a repeating job, in fact this was the third cutting with this file this morning, so the file is fine.

There is no arcing noise, as the laser only fires when I set the max power below 2 mA

I'm realizing that during the previous cuts, there was some little noise that I associated with the fall of the small pieces from the holes cutted.... perhaps these sounds was shorts arcing instead...

Tomorrow I'm going to open the PSU and take a look if I see anything wrong, but I suspect from the flyback again.


---
**Don Kleinschnitz Jr.** *May 28, 2018 14:31*

**+syknarf** it sounds like an arc is occuring. Check your tube connections under load for arcs. 

How did you wire in the HV leads on the new HVT?


---
**syknarf** *May 28, 2018 17:47*

Wiring is the original. When I replaced the flyback, I put a fast plug on the HV wire 


---
**Don Kleinschnitz Jr.** *May 28, 2018 18:05*

**+syknarf** I'm stabbing at stuff. Are you sure it is not arc-ing in at the plug. Unfortunately it is possible for a single arc to damage the HVT or supply. 


---
*Imported from [Google+](https://plus.google.com/113055105026792621247/posts/Shu1jWrBL3e) &mdash; content and formatting may not be reliable*
