---
layout: post
title: "Probably a stupid question, but... Has anyone gon the way of venting cutting fumes from the beam by setting up a line of 12v PC fans along the front of the bed blowing back towards the exhaust pipe instead of mounting an air"
date: May 25, 2016 19:26
category: "Modification"
author: "Purple Orange"
---
Probably a stupid question,  but... 

Has anyone gon the way of venting cutting fumes from the beam by setting up a line of 12v PC fans along the front of the bed blowing back towards the exhaust pipe instead of mounting an air assist on the laser head? 



Why would this approach not work? 





**"Purple Orange"**

---
---
**Ariel Yahni (UniKpty)** *May 25, 2016 19:27*

Air assist primary purpose is adding oxygen to the cut path to burn faster,  cleaner and deeper


---
**Anthony Bolgar** *May 25, 2016 19:41*

It also prevents flare ups of flames, fan idea would not.


---
**Thor Johnson** *May 25, 2016 19:41*

Some people have... on my machine, however, the cutting plane is the same as the Y axis shaft, so I'd have to make it an angle.



I was hoping that if I put a nozzle at the lens, I could get away with a lower cfm requirement, but I find that I like ~10 m/s velocity to blow out flames on wood, so I'm using my air compressor instead of my aquarium pump (which is good enough to keep soot off the lens, but it won't blow out a flame).  I'm not sure oxygen would be a good thing!


---
**Purple Orange** *May 25, 2016 19:51*

Thanks for the pointers,  so adding fans would do no good then 


---
**Jim Hatch** *May 25, 2016 19:54*

ROTFLMAO! I did. There are those here who believe that they don't work. Warned me about all of the bad things that would happen. Things like negative pressure (or positive pressure depending on the opinion provider), swirling air currents, etc. etc.



My fans don't agree with any of those opinions so they're working fine. I have 4 small 5VDC computer fans (I have these: [http://www.amazon.com/Coolerguys-Dual-80mm-Cooling-Fans/dp/B002NVC1DS?ie=UTF8&psc=1&redirect=true&ref_=oh_aui_search_detailpage](http://www.amazon.com/Coolerguys-Dual-80mm-Cooling-Fans/dp/B002NVC1DS?ie=UTF8&psc=1&redirect=true&ref_=oh_aui_search_detailpage)). I cut a line of 3" holes into the front of the body and mounted them on the outside of the unit. I had some ones that were 1/2 that size that I considered mounting inside the box drawing the air in but they didn't have a lot of air flow. The 80mm ones don't fit inside the box because of the rail in the front so they mount outside on the front and push the air. I just flipped the guards so I don't stick my fingers in them accidentally (they're on the output side of the fan when they come).



I also have air assist. Air assist keeps the smoke away from the lens. These extra fans provide input to the box because there's really only a small hole in the bottom of the box to let air in (plus whatever can make it past the seams in the lids) to replace what's exhausted out the box. They keep the smoke from swirling around the work piece which helps cut down the smoke staining and the extra airflow keeps the fumes moving out the box through the exhaust so I don't get complaints from the other residents of the house about burning smells.



Of course, it could be that adding fans is a bad thing to do. I'll let the proponents of that point of view speak for themselves and the theory behind their opinions. I'm just going with what I know works. (I made this mod because I use a big "real" laser at our local Makerspace where the $5K price included a half dozen of these fans mounted on the front - it's a 60W laser so it's wide enough for more than I could fit on my K40 and it had way better smoke dispersion than my stock K40 - now they're the same.)


---
**Anthony Bolgar** *May 25, 2016 19:55*

Upgrading your exhaust fan would sort of accomplish what you are after, the stock fan really does not have enough CFM to work properly. I replaced mine with a 12V 4" diameter bilge blower that moves 720 CFM of air, only payed about $15.00 for it on ebay. Was a huge difference in clearing the smoke and fumes. It actually moves so much air that I will need to add a vent to the front to let air in, it is starved for air the way it is now, so it isn't even working at its rated capacity.


---
**Purple Orange** *May 25, 2016 19:58*

**+Jim Hatch**​ thanks for the input.  I was under the impression that adding fans would do something along those lines,  however I had not grasped the concept of air assist for adding ox to make a cleaner and faster cut.  



Got any photos to show your fan-mod? ﻿


---
**Anthony Bolgar** *May 25, 2016 19:58*

I agree with you **+Jim Hatch** , I may even need to add fans to mine to feed the exhaust more air. It definately helps with the swirling smoke, with the addition of an air assist nozzle, you have hit upon the right combination of ideas to create an excellent air assist/exhaust combo.


---
**Ariel Yahni (UniKpty)** *May 25, 2016 20:17*

I don't see any reason why I cross airflow would be wrong. I/O


---
**Jim Hatch** *May 25, 2016 21:00*

**+Purple Orange**, **+Anthony Bolgar**​​ I have pics of my fan setup that I posted in a "mods I've made" topic but can do a repeat post or send you PMs with them if you can't find my other post.


---
**Anthony Bolgar** *May 25, 2016 23:47*

I just took a look at my old PC junk closet, and found some beautiful 12V fans, 4" super silent with clear acrylic fan blades in an Aluminum frame. I think I will install them in the front of the K40, if only just to bling it out :)


---
**Vince Lee** *May 26, 2016 05:34*

I added a small 24v fan to the laser head connecting it with cable chain, bringing it closer to the actual cut. It works great both blowing out flames and clearing smoke.  Before I put it in I used to point a big desk fan under the lid and this worked too but pushed smoke out everywhere.  I suspect this might also happen with a row of added fans if their output exceeded the vacuum created by your exhaust fan.


---
**Anthony Bolgar** *May 26, 2016 05:40*

My need for the fans is the opposite of what happened with yours **+Vince Lee** My exhaust is starved for air right now,


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 06:10*

I feel like **+Jim Hatch**'s suggestion of the exhaust requiring more intake of air is reasonable. I've noticed severe swirling of smoke around my workpieces, especially with the design for air-assist that I am using (it causes the smoke at the cut point to disperse in all directions). As Jim mentions, a tiny little hole at the base of the machine is not enough intake to compare to the exhaust hole size. Also, the hole on the base means that the exhaust fan seems to draw a lot of it's air from the base of the machine, straight through that intake & out the exhaust. Which is basically pointless because all the smoke is at the top of the machine. If, like most of us, you've removed the stock exhaust funnel, it will no longer draw the air just from the top of the machine. So cutting a row of holes at the top of the front of the machine & covering the hole in the base may be something that encourages the airflow to be mostly along the top of the workpiece, rather than mostly underneath it.


---
**Stephane Buisson** *May 26, 2016 08:57*

all depend why for ? will not replace air assist. adding fan  to help building pressure why not (i could see a negative point is to keep some smoke spinning sideway near the mirror1&2, while a mainstream go straight to exhaust front/rear). I would prefer a better exhaust sucking all (maybe adding an air take on the side).


---
**Jim Hatch** *May 26, 2016 12:12*

**+Vince Lee**​ What you suspect might happen doesn't with the 4 fans I linked to. They match pretty well with the CFM rating of the exhaust. They're not pushing anything close to the air volumes a desk fan would - more of a light breeze. Just enough to keep the interior machine volume supplied with replacement air.



**+Stephane Buisson**​ you're right. There are two solutions needed. The air assist keeps the nozzle clear, smoke away from the lens and flames from starting. The front fans provide replacement air for the volume being pulled by the exhaust while also directing airflow across the work piece (something the manufacturer tried to accomplish with that little metal exhaust duct we all take out - it's too small but it's in a better place than the 2" hole in the bottom of the machine).


---
**Vince Lee** *May 26, 2016 14:55*

**+Anthony Bolgar** I should add that I also replaced my outgoing blower with a ridiculously high volume unit and installed spacers to prop open the lid a bit to allow in more air.  I consider increasing the exhaust vacuum preferable to adding input pressure to assist it as it is less likely to create local high pressure areas that can push fumes out the seams of the machine.  Also, I found a gentle breeze is not enough to extinguish flame.  I had to find a good small fan to mount on the head and make a shroud around it to get decent results.  I just didn't want to have to get a compressor and worry about drying its air, etc.


---
**Don Kleinschnitz Jr.** *June 06, 2016 16:26*

I tried a lot including a vacuum cleaner. Ended up with 400CFM 6" fan .... nothing stays in the cutting chamber :). Best change I have made so far.


---
**Jim Hatch** *June 06, 2016 17:19*

**+Vince Lee** Operating the laser with the lid propped open even just a little changes the machine to a Class IV laser and disallows its use in the home or without safety equipment (like glasses). With the lid open, there's a (remote) chance that the beam can reflect outside the box. Class I laser machines don't allow the beam to escape and can be operated without safety equipment.



Not that anyone is likely to chase you down for operating an invalid machine in your home, but there is the possibility that insurance might be affected for those that care. And the possibility of a popping eyeball exists even if it's a remote one (the first sign you've been struck by the laser is the burn and sound of your eyeball popping). 



I generally wear my laser glasses anyway since I'll run it with the lid off sometimes when I'm tweaking the machine or futzing with alignment (mirrors/optics and materials) issues.


---
**Vince Lee** *June 07, 2016 04:03*

**+Jim Hatch** I have spacers that prop the lid open 1/8 of an inch.  I don't know if there is a direct path out that space but I suppose I could add a strip of metal to block it.  However I see some of the newer units have vents cut in the front of the lid.  Seems like that could be really bad.


---
**Jim Hatch** *June 07, 2016 13:01*

**+Vince Lee**​ the opening is what changes it to a Class 4. I've got a row of fans cut into the front of my box so there's a possibility of reflection through the fan which does the same for me. The likelihood is extraordinarily small that we'll get any laser reflection out of the box but because it's not sealed the chance is not zero so Class 4 it is 😃


---
**Vince Lee** *June 08, 2016 03:31*

**+Jim Hatch** Hmm.  I guess with the original half moon hole in my lid for a handle it was already a class 4 to begin with.


---
**Jim Hatch** *June 08, 2016 12:25*

**+Vince Lee**​ I have a small half moon shaped finger cut out on the lid of mine too but there's a lip on the lower panel that the lid closes over so the cutout isn't exposed from the inside of the machine. If yours is open to the machine's internals then you're right it's class 4. On the other hand I'm generally suspicious of any claimed "certification" by these manufacturers considering the claims for laser power and some of the wiring I've seen people get with their machines.


---
**Vince Lee** *June 11, 2016 02:34*

**+Jim Hatch**  out of the box my unit worked okay but I wouldn't have classified it as Class 4 or Class 5... Class Action Suit more like it.


---
**Jim Hatch** *June 11, 2016 03:00*

**+Vince Lee**​ I understand people's frustration with the K40. It's not a consumer commodity product. At best it's a tinkerer's box. Not unlike my first PC - a Commodore 64 with a cassette drive.



For a properly supported, professionally built laser cutter there are many options out there - ULS, Epilog, Trotec, etc. They're not $400 though. Or $4,000 either. $10K is about the going entry price for getting into laser cutting and engraving. (An entry-level Epilog not much different in capabilities than the K40 is $8,000.)



Anyone who expects the quality of an $8,000 machine in a $400 one is kidding themselves. 



The K40 does what it claims. Not as easily or reliably as an Epilog but the claims are generally true (even the 40W claim is defensible). Not class action material. For that you just need to look to Theranos or Takata.


---
**Don Kleinschnitz Jr.** *June 11, 2016 13:16*

Are you guys on this post using a duct fan along with these other air devices or are you trying to avoid the fan?

Here is a post on my setup for reference.

[http://donsthings.blogspot.com/2016/06/k40-air-systems.html](http://donsthings.blogspot.com/2016/06/k40-air-systems.html)


---
**Jim Hatch** *June 11, 2016 14:08*

**+Don Kleinschnitz**​ I have both. I have the front muffin fans to add air to the box. Otherwise there isn't enough incoming air to keep up with the exhaust - just a small hole in the bottom and what can be pulled through the gaps in the lid & seams.


---
*Imported from [Google+](https://plus.google.com/112469922804049225807/posts/P6sGMoJer8W) &mdash; content and formatting may not be reliable*
