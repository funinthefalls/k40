---
layout: post
title: "FYI For you guys in the United States maybe Canada Home Depot is selling 3 board bamboo sets for $7.88"
date: November 19, 2017 18:16
category: "Discussion"
author: "Gee Willikers"
---
FYI For you guys in the United States maybe Canada Home Depot is selling 3 board bamboo sets for $7.88.



![images/b80ef988f2d0ffc1dc9ff8dedc93a0b5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b80ef988f2d0ffc1dc9ff8dedc93a0b5.jpeg)
![images/f37c0b5c0ab01f17a337a56cbd79e54a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f37c0b5c0ab01f17a337a56cbd79e54a.jpeg)

**"Gee Willikers"**

---
---
**Jim Hatch** *November 19, 2017 20:57*

It's a great deal. I bought a bunch last year and picked up 4 more yesterday. I break them up and engrave different stuff on them. Then I give or sell them separately.



You can sometimes find them on Amazon if your local HD runs out.


---
**Nigel Conroy** *November 21, 2017 16:40*

The Christmas Tree store had some small ones for a dollar each.


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/g1zFmCBK43g) &mdash; content and formatting may not be reliable*
