---
layout: post
title: "Soooo I think I'm going to pull the damn lever here ..."
date: October 19, 2016 05:49
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Soooo I think I'm going to pull the damn lever here ... or push the button ... or whatever it is you want to call it: I've decided to go with a Smoothie board instead of a DSP or RAMPS. For all intents and purposes, all one needs is X, Y, and Z control for a laser, which would translate to an x3. However, I'm somewhat giddy about also having ethernet, which means an instant upgrade to at least the x4 which would give me an additional stepper driver. Is there any reason at all to get an x5, which only gives me yet another stepper port compared to the x4?﻿





**"Ashley M. Kirchner [Norym]"**

---
---
**Scott Marshall** *October 19, 2016 08:52*

Not really. It gives you an extra motor controller, but you already have 2 extra with the 4X. 

You don't use one of the motor controllers, so even the 3X has an extra motor controller when driving a K40.

The real plus with the 4x is the ethernet. 


---
**Ashley M. Kirchner [Norym]** *October 19, 2016 15:46*

Don't you need all three controllers for an X, Y, Z setup? That's all of them on an X3. 


---
**Scott Marshall** *October 20, 2016 10:39*

If you're going to add a Z, you'll want the 3rd. Never seen it done well, travel is short and the cabinet isn't straight or rigid. It CAN be done, but it's going to be a lot of work for the limited benefits. 


---
**Ashley M. Kirchner [Norym]** *October 20, 2016 17:24*

Yeah, I'm needing an adjustable bed. While I'm only working with 1/8 and 1/4 wood right now, I have put thicker stuff in for engraving, and always end up removing everything from the cabinet then rigging something to hold up the piece at the approximate depth. Not ideal, but it works. The cabinet is bad, I know that, but it's also not that hard to fix, either by internal bracing and straightening, or by adjusting the bed (with shims) to get it level in relation to the moving gantry (which is also not perfectly level.) Or, alternatively, either rip everything out and build a new cabinet, or build a custom laser from scratch. Neither of them are practical at the moment ... in the future, sure.


---
**Ashley M. Kirchner [Norym]** *October 20, 2016 18:42*

And come to think of it, I may just buy the cheapest K40 I can find on eBay and gut it. It's hard for me to have the current, working one, down for any period of time. That has been the main reason why I haven't done the controller upgrade yet, simply because I don't know what issues I will run into, what additional parts I would need, and possibly what I may need to cut for the upgrade (like a new panel?) So yeah ... it's still "pending" to be honest. I'm getting closer to just starting to buy parts and try to figure it out from there before I touch anything, but I would feel so much more comfortable if I have a second one to do that with. Once that one is done, up and running, then I can repeat the process with the primary machine.


---
**Scott Marshall** *October 21, 2016 11:16*

Probably a good way to go. I bought a 2nd one because my 1st one always has a customers kit in it for test and setup. I haven't had a afternoon of enjoying my laser since I started designing the ACR kit last February, I miss it, and have a bunch of things to finish and make,



Either way, a $300 investment is worth it even if it's just a parts donor. I'm tempted to get another one and put it out in the shop because the k40 deal could disappear anytime, especially here in the US if the CPSC gets wind of it.



 Plans change after you get into a project like that after you start it anyway. It doesn't pay to plan things out to the last screw for a 1 off. I'd advise on getting the basic components, removing what you know needs to change, then decide how you want it go back. These days you can get almost anything you need in a week or so, so waiting for parts isn't usually too bad, and there's usually work to do while waiting anyhow.



I've been toying with the idea of the movable bed/autofocus off and on and even played with a few powered heads.



I cut the bottom out of my 1st one, using a body slitter and it was a pretty painless affair. Focusing isn't an issue for me, because I have the Saite head and it gives you about a 3/8" range of focus with the turn of a screw. I just use bathroom dixie cups (just the right height) or a nailboard and I'm cutting in 5 minutes or less. (my "bed" is the table the k40 sits on) I don't engrave much so the head weight isn't an issue. (actually I don't laser much anymore on account of time)

Once I get the new one online, I think I'm going to TIG in a square tubing frame to mount all the motion control gear to. That way it will hold it's relative position pretty close. There will be something beside sheetmetal to build from.

Then my plans are to use 8mm linear bearings on 4 columns for the bed to run up and down on. I already bought some of that stuff. Control is easy from there. Trying to squeeze a decent moving bed into the available space in a stock k40 is an exercise in frustration. Once you're below the floor, you have room for your gear, and that seems the way to go.I figure to make the actual 'elevator' frame as big as needed to work well, and make the actual platform small enough to fit through the cutout have it stepped up so it is about 3-4" above the moving frame. That will allow the actual bed to move righ up into the cutting bay for normal thickness materials. Then you can lower it for engraving the tops of large boxes, baseball bats and that stuff.



 I'm afraid it's going to be a lot of work no matter how we go about it. The only practical way to autofocus a stock cabinet K40 is to fit it with an adjustable head and drive that. Even that is a lot of scheming and cramming in small parts. Add to that the need to do it with off the shelf parts (if you want to sell them) and it borders on practicality. I think we are right on the ragged edge of scratch building a new laser. It may well be the way to go, just use the tube, psu and optics from a donor K40 and build the rest from V channel. It might cost a bit more, but it would be less work and you would have a much nicer laser in the end.



Good luck, and since I probably won't have time to do any of it, I'll be watching your progress jealously.



Scott






---
**Ashley M. Kirchner [Norym]** *October 21, 2016 15:33*

Myah, you and me both. I'll be watching my own "progress" from the side lines. All of my electronic projects have been put in hold. And I just had someone recently ask me to resurrect one. It needs a board redesign, components verified and sourced again, I need to fix the reflow oven.... I need to start living a 37 hour day. 


---
**Scott Marshall** *October 25, 2016 08:09*

That how it starts...


---
**Ashley M. Kirchner [Norym]** *October 25, 2016 16:12*

BWAH! It started about a decade ago for me ... I've been trying to stay above the pile of projects since then ... not doing a good job of it.


---
**Scott Marshall** *October 28, 2016 10:08*

It's a vicious circle, as you get older, your interests widen, in inverse proportion to available time...



Welcome to the machine....


---
**Ashley M. Kirchner [Norym]** *October 28, 2016 17:32*

Not only as we get older, but also the desire to have other (residual) income, or just plain something else to do than the every day hum-drum of work.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/4X6uRKC6MXN) &mdash; content and formatting may not be reliable*
