---
layout: post
title: "Hello k40-population, My k40 (m2 nano) came with CorelDraw 12/CorelLaser & LaserDRW I want to know, how can I upgrade the CorelDraw to x7 for example, and still maintain the interface between CorelDraw and the k40?"
date: June 07, 2016 01:58
category: "Software"
author: "Pippins McGee"
---
Hello k40-population,



My k40 (m2 nano) came with CorelDraw 12/CorelLaser & LaserDRW



I want to know, how can I upgrade the CorelDraw to x7 for example, and still maintain the interface between CorelDraw and the k40?



CorelDraw needs to be launched using the special 'launcher' (comes up with a splash screen saying WINSEAL XP) in order for it to communicate with the laser, before it opens CorelDraw 12.



So I doubt just uninstalling CorelDraw 12 and installing X7 would work.



is there anything special one needs to do to achieve upgrade version of CorelDraw?



Any advice appreciated, cheers.

(x7 is better in some ways that I need)





**"Pippins McGee"**

---
---
**Ariel Yahni (UniKpty)** *June 07, 2016 02:00*

Assuming that they are compatible change the version should work. If it doesn't then in corellaser there must be a file to the location  of the required version of corel


---
**Derek Schuetz** *June 07, 2016 02:19*

I tried a hacked latest version of corel draw and it worked with corel laser fine I can't remember the exact version at the time


---
**Pippins McGee** *June 07, 2016 02:34*

thank you for your replies.



**+Derek Schuetz** would I just uninstall corel12 and install the newer coreldraw? is that all you did?



as **+Ariel Yahni**  mentioned above, I figured maybe something special would need to be done to tell CorelLaser to open the CorelX7 install instead of the Corel12 install that its pre-programmed to try and open.


---
**Eric Rihm** *June 07, 2016 03:12*

Yeah uninstall and install x7 then install corelaser


---
**Pippins McGee** *June 07, 2016 03:20*

cheers - will give it a go tonight


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 07, 2016 07:40*

I am using x5 & all it took was uninstall v12 & install x5. Nothing else required.



edit: it looks like CorelDraw adds the CorelDRW.exe to the windows namespace so you can just type it into cmd from any directory & it will run. That's probably how CorelLASER manages to find it even though it's a different version.



edit2: it's not necessary to uninstall/reinstall CorelLASER. I managed to just uninstall v12 then install x5. Although if you have issues you could always try that.


---
**Pippins McGee** *June 07, 2016 08:51*

**+Yuusuf Sallahuddin**

thanks yuusuf & all others.

I just tried that it (uninstall v12, and installed x7) seems to be working fine so far on x7

thanks all for your replies.

corellaser is opening x7 and it is communicating with the laser OK.



thank god. was stressing out on the v12 (it wouldn't import some SVG files I needed to cut from. But x7 imports them no problem.


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/QgTimSFkcFc) &mdash; content and formatting may not be reliable*
