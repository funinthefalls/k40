---
layout: post
title: "Still having issues where I need to warm up the machine for 10-20 mins before getting stable readings on the meter"
date: July 16, 2017 21:25
category: "Original software and hardware issues"
author: "Frank Fisher"
---
Still having issues where I need to warm up the machine for 10-20 mins before getting stable readings on the meter.  I replaced the pot in the dial, the tube is reasonably new for the hours it has run (low 3mA at that), so could the power supply be at fault?  I am having problems getting results now.





**"Frank Fisher"**

---
---
**Bob Buechler** *July 16, 2017 21:34*

Tubes age even when not in use. What is it's manufacture date?


---
**Frank Fisher** *July 16, 2017 21:46*

Will have to open it back up tomorrow for that, how long is too long?


---
**Bob Buechler** *July 16, 2017 22:15*

I'm no expert, but I understand internal components can oxidize over time contaminating the gas. Manufacturers warranty their tubes for 6-10 months and estimates for useful lifespan is several thousand hours. But these are estimates, and vary widely based on how they're used, how long (and how) they're stored between uses, etc. 


---
**Frank Fisher** *July 18, 2017 10:19*

Dated May 17 2016, but some say the bigger tubes die fast these 35/40W last years.  The laser tube is a nice purple colour, again some have said it fades to orange once the CO2 is no longer pure?  I don't know.  Am giving it a good clean, fresh water, nice and cool and see what happens.




---
**Frank Fisher** *October 16, 2017 17:05*

Looks likely to have been the power supply, I have replaced the tube but neither the old or new fire :(


---
*Imported from [Google+](https://plus.google.com/114228670405979897634/posts/YXyMXpY1Jgy) &mdash; content and formatting may not be reliable*
