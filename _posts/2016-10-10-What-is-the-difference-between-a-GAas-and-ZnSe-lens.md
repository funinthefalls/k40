---
layout: post
title: "What is the difference between a GAas and ZnSe lens"
date: October 10, 2016 22:49
category: "Discussion"
author: "3D Laser"
---
What is the difference between a GAas and ZnSe lens 





**"3D Laser"**

---
---
**greg greene** *October 10, 2016 23:01*

reflectivity and heat absortion - don't know which is best for the wave length of our lasers though


---
**Jim Hatch** *October 11, 2016 00:04*

ZnSe has the lowest absorption rate for the CO2 laser wavelength. GaAs has slightly higher absorption rates but is harder than ZnSe so it's better for use in dirty environments (and its thermal conductivity keeps crud deposits from heating/burning the lens).



GaAs does not pass visible light - like the red dot lasers that are used inline with the CO2 laser (using a beam splitter). It's not common but I know someone here was using a red laser through the lens to get his aimpoint precisely aligned. A GaAs lens would make that light disappear.


---
**3D Laser** *October 11, 2016 00:09*

**+Jim Hatch**  is one better than the other for cutting ply


---
**Jim Hatch** *October 11, 2016 00:16*

in our lasers - no (if you have air assist). In high power lasers (>100W) due to its better thermal ability and when doing a lot of MDF cutting, the GaAs might be a better choice. In clean environments (including high power ones) ZnSe is better for delivering more of the beam power to the material since the thermal properties of GaAs are only needed to handle the heat caused by deposits on the lens - a clean environment (air assist) will keep the deposits away. 



Frankly, it's a minimal difference though and comes down to whether you need the toughness of the GaAs not as much as which delivers more power.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/gCjj4rQ8Cqr) &mdash; content and formatting may not be reliable*
