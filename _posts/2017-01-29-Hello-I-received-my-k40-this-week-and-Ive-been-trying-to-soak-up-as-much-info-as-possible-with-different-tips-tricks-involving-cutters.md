---
layout: post
title: "Hello, I received my k40 this week and I've been trying to soak up as much info as possible with different tips/tricks involving cutters"
date: January 29, 2017 20:27
category: "Hardware and Laser settings"
author: "matt s"
---
Hello, I received my k40 this week and I've been trying to soak up as much info as possible with different tips/tricks involving cutters. From my understanding if you keep your water nice and cool it can help prolong the life expectancy of your tube. I'm currently using 3 gallons of distilled water in a 5 gallon bucket. I'm in the process of turning an old mini fridge into a water cooling unit for the laser, but I'm unsure of how much volume of water I should be using to cool a 40w laser. Any info will be greatly appreciated.







**"matt s"**

---
---
**Ariel Yahni (UniKpty)** *January 29, 2017 20:31*

I use about 3 gal ona 5 gal bucket. If I place cold packs I can go down to 20c. Best temp should be between 15c to 20c. Beware a fridge could create condensation and I don't believe that's good


---
**matt s** *January 29, 2017 20:58*

thx for the info. I planned on sealing my current bucket with all the hoses/pump inside with the hoses running out from the top. then running those hoses out of the side of the mini fridge with a new hole I create. I know very little about laser cutters so in my noob brain I assumed hooking up an old mini fridge to cool the water for the laser seemed like a good idea lol.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 30, 2017 04:43*

**+matt s** The fridge would be a good idea, but as Ariel mentions, condensation could form on the laser tube if the water gets too cool, which won't be good. I have a feeling that if you increased the water volume to a point where the fridge barely keeps it at 15-20c, you would probably minimise the condensation issue. I'd say get a large container, rig up your fridge unit, then test with varying amounts of water until you find the "sweet spot" for quantity of water & you should be good from there.


---
**Coherent** *February 03, 2017 22:25*

There's a bunch of info on the web regarding using small fridges to cool water. Many have tried for aquariums or even tig welders. Options include a tank inside like you suggest, or copper tubing coils inside it.The reviews that I've read were pretty dismal results. The tank or coils inside the fridge work for a bit but as the water warms the fridge simply can't cool the water faster than it's being warmed. The cheapest option I've seen are some of the small commercial aquarium water cooler setups, but none are really really cheap. Your results may be better and if so I'm sure the folks here would be interested. Have fun.


---
*Imported from [Google+](https://plus.google.com/106809986839735143620/posts/RYkMVPxBNCa) &mdash; content and formatting may not be reliable*
