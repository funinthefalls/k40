---
layout: post
title: "What could be the best mod for you ?"
date: November 24, 2014 17:17
category: "Hardware and Laser settings"
author: "Stephane Buisson"
---
What could be the best mod for you ? (your expectations)





My first idea is a web app (drop in file, a bit like octoprint) hosted on a little pcDuino and the MrBeam shield. if we are able to keep the PSU/laserdriver (MYJG40W) the mod could stay in a reasonable price to match the K40 entry level price tag.



in fact the ideal solution I could see for now, could be simply send a pdf where the content should be interpreted, like in this example (final format to be created): 



a red hairline shape mean a cut,

a gray level image will be a raster.

all the rest will be ignored. (limited material loss)



Cutting and rastering could be mixed in the same job.





The advantage of this solution is, you are OS/Software free. the K40 is A4 landscape size so pdf make full sense.



No more battle with origin, positive/negative, layers or task/job and other complications like into MoshiDraw. (what a loss of time and material ! bloody Moshidraw. )





[http://shop.mr-beam.org/product/mr-beam-shield](http://shop.mr-beam.org/product/mr-beam-shield)

[http://www.pcduino.com/pcduino-v3/](http://www.pcduino.com/pcduino-v3/)





**"Stephane Buisson"**

---
---
**Muccaneer von München** *November 24, 2014 17:44*

**+Stephane Buisson** "... a bit like octoprint ..." 

we will release our octoprint fork quite soon - maybe tonight ;)


---
**Stephane Buisson** *November 30, 2014 15:17*

I am very tempted to go your path, but before to rush into it, I would like to have a general view of  all software chain to go with it. You need a bit of time, and Christmas period isn't favorable in term of time and money, hopefully soon ...


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/53rLEwLCvWA) &mdash; content and formatting may not be reliable*
