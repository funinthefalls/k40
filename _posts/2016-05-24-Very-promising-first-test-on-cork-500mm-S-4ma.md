---
layout: post
title: "Very promising first test on cork 500mm/S 4ma..."
date: May 24, 2016 06:16
category: "Object produced with laser"
author: "Alex Krause"
---
Very promising first test on cork 500mm/S 4ma... I think some coasters are in my future 

![images/00a6a5b1f1c2659c32d55c087af93549.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00a6a5b1f1c2659c32d55c087af93549.jpeg)



**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 24, 2016 06:53*

That looks really awesome. I'm curious how deep did the engrave go & does it make the cork more brittle?


---
**Tony Schelts** *May 24, 2016 07:40*

Are you using the K40 how did it cope with 500mm/s


---
**Justin Mitchell** *May 24, 2016 07:57*

Our k40 works (mostly) fine at 500 as well, in the rasterizing engrave mode as long as the job isn't too small. It just zips back and forth quite happily. You couldn't do anything like that on vector cuts though, or anything where it had to change direction a lot


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 24, 2016 09:41*

**+Tony Schelts** I am using mine at 500mm/s for engraving 99% of the time. Ever since I got the machine I put it at that speed & it's run fine. Occasional hiccup (missed steps) when the belts weren't tight enough, but once tightened it's all good.



As **+Justin Mitchell** mentions, vector cuts changing direction a lot, anything above 25mm/s & it vibrates ridiculous amounts (to the point I had to realign all my mirrors). Straight simple vector cuts (e.g. rectangle) I've been able to run at around 50mm/s without much issue.


---
**Alex Krause** *May 25, 2016 16:35*

**+Yuusuf Sallahuddin**​ it looks as if it's only marked a few Thousands of an inch deep I believe it is because cork has a very pourus cellular structure allowing the air cells to insulate the surrounding material. As for making it any more brittle I plan on bonding it to a leather or wood backing to improve structural integrity 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 16:38*

**+Alex Krause** Nice. It might be worth applying a coat of lacquer/sealer over the top also, to prevent the engraved area damaging over time (especially when used as coasters where people are putting things on & off it a lot).


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/isBrMzFdoFU) &mdash; content and formatting may not be reliable*
