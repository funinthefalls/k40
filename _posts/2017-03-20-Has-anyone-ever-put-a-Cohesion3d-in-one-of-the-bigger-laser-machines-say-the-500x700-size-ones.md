---
layout: post
title: "Has anyone ever put a Cohesion3d in one of the bigger laser machines, say the 500x700 size ones?"
date: March 20, 2017 22:07
category: "Discussion"
author: "Jason He"
---
Has anyone ever put a Cohesion3d in one of the bigger laser machines, say the 500x700 size ones? Just wondering how different the process would be from a K40, if at all.





**"Jason He"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 21, 2017 06:46*

Definitely talked to a few people about it. Got pics? Some of them run at 36v which is a bit higher than typical boards can handle (24v max). So if that's the case you may want to get external drivers. We have adapters for that. 



I've also seen some of the larger machines with a ribbon cable. Can't say if it's the same as the k40 or not. 



Ultimately though it's motors, endstops, and a laser fire signal. 


---
**Jason He** *March 23, 2017 22:42*

Ahh. I was curious. I have a K40 myself and was just wondering what it would take to get smoothified if I upgraded to a larger machine.


---
*Imported from [Google+](https://plus.google.com/109350981963273445690/posts/1gjtXHi56JF) &mdash; content and formatting may not be reliable*
