---
layout: post
title: "Anybody missing some gremlins?? Ok, new power supply on Monday and waited 18 hours for the silicone to dry on the tube before even trying a test fire"
date: August 25, 2017 19:05
category: "Original software and hardware issues"
author: "Martin SL"
---
Anybody missing some gremlins??

Ok, new power supply on Monday and waited 18 hours for the silicone to dry on the tube before even trying a test fire.

Everything was fine so I did a test piece to check if my pot needed a new overlay and I made a base and engraved some acrylic with no issues.

Engraved a couple small wood test pieces and noticed it getting just a bit lighter on the second, checked water temp and it was 19.4 so I just stopped the job and let it circulate for half an hour before shutting it off. (Water was changed 1 week ago,5 gallons distilled)

Weds I got everything going and when I went to do a job There was nothing happening at 2.5mA so I turn it up to 4 and hear a loud arcing noise from the tube. Cut the laser off and looked for black marks but didn't see any so I made a short video to catch it.

Someone suggested redoing the silicone, I did and used the white that came with it this time. Let it dry 22 hours and tried again while someone watched, they said it looks like the arcing is coming out of the end of the tube.



Next step I should take or part to test?  I already have a new tube being delivered today.


**Video content missing for image https://lh3.googleusercontent.com/-tRkRphyoMpg/WaB0_lMgGPI/AAAAAAAAAZg/IQLeIBL29R4CIYm-j0L6gd6C-lkn5xyNQCJoC/s0/20170823_131600-2.mp4**
![images/dc705c4ff49a553e8e3fbafbdf24b00a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc705c4ff49a553e8e3fbafbdf24b00a.jpeg)



**"Martin SL"**

---
---
**Don Kleinschnitz Jr.** *August 26, 2017 12:52*

Can't see much in the video seems low resolution.

At first guess I would say bad tube as these supplies will arch like this when there is no load if the anode is within 2-3" of ground.


---
*Imported from [Google+](https://plus.google.com/108406098480319659580/posts/KCkDkNN3hGs) &mdash; content and formatting may not be reliable*
