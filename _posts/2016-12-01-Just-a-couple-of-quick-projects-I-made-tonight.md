---
layout: post
title: "Just a couple of quick projects I made tonight"
date: December 01, 2016 02:57
category: "Object produced with laser"
author: "Anthony Bolgar"
---
Just a couple of quick projects I made tonight



![images/e95088d07d87d6a43a7ad4ddc0bb1d5f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e95088d07d87d6a43a7ad4ddc0bb1d5f.jpeg)
![images/27448f37e3b1a9424e4e6a54c07d715a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/27448f37e3b1a9424e4e6a54c07d715a.jpeg)

**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *December 01, 2016 02:57*

**+Alex Krause** A little bit of Whovian art for you.


---
**Kelly S** *December 01, 2016 03:57*

That is awesome!  


---
**Anthony Bolgar** *December 01, 2016 06:38*

Thanks **+Kelly S** 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/bZ5HbkJSrYb) &mdash; content and formatting may not be reliable*
