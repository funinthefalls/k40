---
layout: post
title: "Knocked out this one off piece for a friends baby shower gift"
date: December 13, 2017 03:56
category: "Object produced with laser"
author: "David Allen Frantz"
---
Knocked out this one off piece for a friends baby shower gift. 1/4” acrylic with a deer hide hanging loop. 



![images/493158108a2221dc444ca753d9b58d7d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/493158108a2221dc444ca753d9b58d7d.jpeg)
![images/e0f0b24fba29f6fb047716ef5d6c23db.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e0f0b24fba29f6fb047716ef5d6c23db.jpeg)

**"David Allen Frantz"**

---


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/ZNQJkQ8VKE7) &mdash; content and formatting may not be reliable*
