---
layout: post
title: "Help please. My K40 has failed half way through a big job (Whisperer estimated 9hr 13m - raster engraving at 100mm per sec and current set at 20% drawing about 2 ma)"
date: October 13, 2018 16:24
category: "Original software and hardware issues"
author: "Duncan Caine"
---
Help please.  My K40 has failed half way through a big job (Whisperer estimated 9hr 13m - raster engraving at 100mm per sec and current set at 20% drawing about 2 ma).



The laser has stopped firing, all I'm getting now on test fire is a sparking at the rear of the CO2 tube, but nothing through the tube and it's not drawing any current.  I'm guessing that I need to buy a new tube.



But my worry is that the failure was caused by something other than the tube itself and if I fit a new one will it fail again.



All advice welcome, thank you. 





**"Duncan Caine"**

---
---
**Ariel Yahni (UniKpty)** *October 13, 2018 16:45*

That sound like arcing. 


---
**Stephane Buisson** *October 13, 2018 17:06*

like **+Ariel Yahni** say it's look like arcing.

this mean the current found a shorter way to go. check insulation on the tube connection (remember the silicone tube you got with your K40).


---
**Don Kleinschnitz Jr.** *October 13, 2018 17:53*

Can you post a video of the arching? Best to do it in a darkened room.



Is it arching inside the tube?



What coolant are you using?



How old is the tube, how hard do you run it?



My guess is the tube is bad. When these supplies have no load the LPS can arc a few inches. 


---
**Duncan Caine** *October 14, 2018 13:23*

**+Don Kleinschnitz Jr.** Video here as requested 
{% include youtubePlayer.html id="5mgOPKu3jak" %}
[youtube.com - K40 laser tube arcing](https://youtu.be/5mgOPKu3jak) coolant is clothes dryer condensate (distilled water) and the tube is about 9 months old


---
**Don Kleinschnitz Jr.** *October 14, 2018 14:06*

Its hard to see the arc. Is it inside the tube or arching to the case anywhere?

Looks more like inside the tube?

This could be either a tube or a power supply, its hard to know which.

Can you see any arching inside the power supply. 



Logically:

-- I do not see the tube ionizing.

-- Since this was a sudden failure my bet would be the LPS. Further I would suspect the HV transformer. 



You can try testing it this way:

<b>WARNING: the LPS outputs lethal voltage. At all times stay away from the laser tube while power is applied</b>



<b>YOU DO THE FOLLOWING TEST AT YOUR OWN RISK</b>



<b>With power off:</b>

You can connect one end of a wire to ground and tape the other end to the tube about 1" away from the anode. 



<b>With power on and your positioned away from the tube</b>:

Turn on the machines power and <b>briefly</b> push the test button to fire the laser. Caution: Do not hold down on the button. Arching to ground for long periods can damage the LPS.



<b>Results</b>

If you get a violent arch from the anode to the ground wire then your problem is likely your tube. 



If you don't get a strong arc or any arc at all your LPS is bad.

You can buy a new LPS or replace the HVT in your current unit.


---
**Duncan Caine** *October 14, 2018 15:39*

**+Don Kleinschnitz Jr.** Hi Don, the arcing is definitely inside the tube. To do the above test, should I remove the silcone insulation on the anode?


---
**Don Kleinschnitz Jr.** *October 14, 2018 17:22*

**+Duncan Caine** removing the insulation (or pull it back) would be better. Before you touch or get near the anode; with the power off ground it with a chicken stick. 


---
**Duncan Caine** *October 15, 2018 13:55*

**+Don Kleinschnitz Jr.** Hi Don, this what I have done. 

1. Connected an earth wire to my domestic wiring earth circuit

2. Stripped off the silicone anode insulation so that the tip of the anode is visible.

3. Taped the earth wire to the tube so that the exposed end was close to the tube anode, approx 15mm

4. Connected the power

5. Pressed the test switch



The arcing inside the tube continued as before but there was no arcing between the anode and the 'taped on' earth wire.



So I guess my power supply needs replacing, and that poses another problem, which one.



I have googled these 2 which have the same connections as mine and with the same composite plug arrangement, one is a GC model and the other is a GS model and I can't see any difference, can you?



[https://www.aliexpress.com/item/MYJG-40W-T-CO2-Laser-Power-Supply-110V-220V-High-Voltage-For-Engraving-Cutting-Machine-Matched/32792975618.html?spm=2114.search0104.3.16.60533c6fXFQdor&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10130_318_10547_319_10548_10696_450_10192_10190_10084_10083_10618_452_535_534_533_10307_10820_532_10301_10821_10303_204_10059_10884_10887_100031_320_10103_448_449,searchweb201603_60,ppcSwitch_0&algo_expid=9ac7ac44-8ef2-467b-97cf-b0b96b9c9d3a-2&algo_pvid=9ac7ac44-8ef2-467b-97cf-b0b96b9c9d3a](https://www.aliexpress.com/item/MYJG-40W-T-CO2-Laser-Power-Supply-110V-220V-High-Voltage-For-Engraving-Cutting-Machine-Matched/32792975618.html?spm=2114.search0104.3.16.60533c6fXFQdor&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10130_318_10547_319_10548_10696_450_10192_10190_10084_10083_10618_452_535_534_533_10307_10820_532_10301_10821_10303_204_10059_10884_10887_100031_320_10103_448_449,searchweb201603_60,ppcSwitch_0&algo_expid=9ac7ac44-8ef2-467b-97cf-b0b96b9c9d3a-2&algo_pvid=9ac7ac44-8ef2-467b-97cf-b0b96b9c9d3a)

[aliexpress.com - 40W-GS MYJG-40W CO2 Laser Power Supply 40W 25W 30W For CO2 Laser Tube 110V 220V High Voltage Engraving Cutting Machine](https://www.aliexpress.com/item/40W-GS-MYJG-40W-CO2-Laser-Power-Supply-40W-25W-30W-For-CO2-Laser-Tube-110V/32841422186.html?spm=2114.search0104.3.2.60533c6fXFQdor&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10130_318_10547_319_10548_10696_450_10192_10190_10084_10083_10618_452_535_534_533_10307_10820_532_10301_10821_10303_204_10059_10884_10887_100031_320_10103_448_449,searchweb201603_60,ppcSwitch_0&algo_expid=9ac7ac44-8ef2-467b-97cf-b0b96b9c9d3a-0&algo_pvid=9ac7ac44-8ef2-467b-97cf-b0b96b9c9d3a)


---
**Don Kleinschnitz Jr.** *October 15, 2018 15:04*

Good price, not much more expensive than repair! No I do not see any difference? You could email the vendor to be safe.

I assume you live across the pond?


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/Q473AuwAchv) &mdash; content and formatting may not be reliable*
