---
layout: post
title: "Hi guys, today arrived my new air assisted head from lightobject, but ive got some issues"
date: November 25, 2016 21:18
category: "Discussion"
author: "Antonio Garcia"
---
Hi guys,

today arrived my new air assisted head from lightobject, but i´ve got some issues.

When i removed the original head i saw my lens was slightly broken, but it works good in the old head. It has a small scratch (half circle) and when I put it in the new head, and test it produces a half circles rather than a point.

Then i moved the lens for avoiding the scratch, and finally  i got a point rather than half circle, but now the real issue, the laser hasn´t got the same power.

After cleaning the mirror and the lens and installed in the new head with the same parameters, my old head can cut, and new head just engraving.

Anyone have got this head, could you give some advice??

and where can i buy a new lens?? lightobject is fine, but the shipping cost to europe are quite expensive..

UPDATED:

Yes, i was mounting the lens in wrong way, but with the old head i can still cut faster, around 12mm/s vs 8mm/s (MDF 5mm)



![images/7dfba3bd876c70615ba48182bc24b4e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7dfba3bd876c70615ba48182bc24b4e6.jpeg)
![images/85550902bfd3e8d8109abcc1ae9d3509.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/85550902bfd3e8d8109abcc1ae9d3509.jpeg)
![images/52e6ef3bbc37b92d90a85c5770f505a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52e6ef3bbc37b92d90a85c5770f505a9.jpeg)

**"Antonio Garcia"**

---
---
**Antonio Garcia** *November 25, 2016 21:42*

I think I´m mounting the lens wrong.... lol


---
**greg greene** *November 25, 2016 22:33*

Check to see that the head is not skewed so that part of the beam hits the wall of the air nozzle.  Because the nozzle comes after the lens, if the beam is not hitting the center of the lens - then it can hit the inside of the nozzle - reducing the power - that's what I found with mine.  This is not a problem on the stock head.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 25, 2016 23:21*

Check your focal point. The new lens mount may seat the lens at a different distance from the material. Base of lens to centre of material should be 50.8mm (if it's a stock lens) for optimal performance. Do a ramp test to find your thinnest point for cutting.


---
**Antonio Garcia** *November 25, 2016 23:30*

**+greg greene** **+Yuusuf Sallahuddin** Thanks guys, i´m gonna do more testing!!!


---
**loriandlory ES** *December 13, 2016 19:23*

**+Antonio Garcia**  do you speak Spanish? 

I'm very lost in the K 40 settings 



Creo que hablas español me encantaría poder preguntarte un par de cosas en castellano, mi inglés es algo justo, y en el grupo me pierdo con tanta info despredigada. 





Muchas gracias por tu tiempo! 


---
*Imported from [Google+](https://plus.google.com/+AntonioGarciadeSoria/posts/TfQBeDX5iXj) &mdash; content and formatting may not be reliable*
