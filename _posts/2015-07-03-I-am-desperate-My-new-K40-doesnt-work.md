---
layout: post
title: "I am desperate. My new K40 doesn't work"
date: July 03, 2015 23:47
category: "External links&#x3a; Blog, forum, etc"
author: "irresistiblecam"
---
I am desperate. My new K40 doesn't work. No laser fire and NO ammeter movement, the gantry works ok.

I think that it may be the power supply, so I replace it with new one, but nothing.

When I power it on, the fan on the back the machine start to spin, the red led on power supply is always on, and if If I push the test microswitch on power supply board, nothing happen. I checked the wire connection, and 220V from A an C is connected by the red wire to the pin 3 and 4. I let free the Ground wire Joint (D), just because it is internal connected to Groun pin (B) that is connected to pin 2. The pin 1 is connected to ammeter (inline) and than continue to laser tube negative. Between the pin 1 and pin 2 there is 1 ohm resistance.

Instead the positive laser tube (red cable) is connected to power supply internally without a plug. How could I test if on the tube arrive the correct voltage/current? I am prepared to buy some test tools to measure high voltage or to check the co2 tube. What is good for K40?

Please help me!



![images/47999428a7e75e22fc0c871c82397bbf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/47999428a7e75e22fc0c871c82397bbf.jpeg)
![images/a3a3c1620f76d07bf643f5a5ffca0381.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3a3c1620f76d07bf643f5a5ffca0381.jpeg)
![images/8d78d026a049a12512e3752f1047bcc1](https://gitlab.com/funinthefalls/k40/raw/master/images/8d78d026a049a12512e3752f1047bcc1)

**"irresistiblecam"**

---
---
**ThantiK** *July 04, 2015 03:17*

"I let free the Ground wire Joint (D), just because it is internal connected to Groun pin (B) that is connected to pin 2." -- You must have a proper ground connected.  You risk damage to the machine, or even death to yourself if you do not properly ground the machine to earth.


---
**quillford** *July 04, 2015 04:35*

Make sure the laser switch is on.


---
**irresistiblecam** *July 04, 2015 09:34*

please some better reply that "make sure the laser switch is on"...


---
**Jim Root** *July 04, 2015 13:41*

U.S. Or European?


---
**quillford** *July 04, 2015 16:56*

**+irresistiblecam** I haven't had mine for long, so all I can really say is make sure that the switch in the picture ([http://i.imgur.com/vngnipp.jpg](http://i.imgur.com/vngnipp.jpg)) is depressed. When the switch is open, the laser will not fire.


---
**irresistiblecam** *July 04, 2015 22:20*

quillford thank you for reply. I checked with tests that when the laser switch is down, it is short circuit. I also try to press the red microswitch on power supply board. I am European, Italy.


---
**Imko Beckhoven van** *July 04, 2015 23:01*

Did you replaced the power supply with a excact same unit?


---
**Imko Beckhoven van** *July 04, 2015 23:06*

[http://m.instructables.com/id/How-to-test-and-set-a-Chinese-China-made-CO2-Laser/?ALLSTEPS](http://m.instructables.com/id/How-to-test-and-set-a-Chinese-China-made-CO2-Laser/?ALLSTEPS) this might help you in how to make sure you measure right


---
**irresistiblecam** *July 05, 2015 00:02*

I replace the power supply with new one from the same seller, so there are similar, the last is just a new revision.

But I think that the first one was working, but the problem may be the tube or the connections.


---
**Fadi Kahhaleh** *July 05, 2015 03:07*

**+irresistiblecam** this might be helpful in troubleshooting your PSU.


{% include youtubePlayer.html id="dz4ln4_vT3g" %}
[https://www.youtube.com/watch?v=dz4ln4_vT3g](https://www.youtube.com/watch?v=dz4ln4_vT3g)



Good luck, and always be very careful, these are no joke Power Supplies :)


---
**SteamM0nkey MC** *July 10, 2015 13:17*

When you say the laser switch causes a short circuit, what do you mea? Do you mean there's no resistance between the two poles of the switch? Because that's normal. My laser switch was pretty bad, I had to mess with it quite a bit to get it to actually engage properly and turn the laser on. Go simple first, make sure that switch is operating properly, you can bypass it by disconnecting the leads to the switch and connecting those wires directly to turn the laser on (unplug the entire device before you do that!) 



Also, the laser won't fire unless you hit the "test" button or run a job, obviously. (sorry, had a history in tech support so I always state the obvious :P)


---
**irresistiblecam** *July 16, 2015 21:03*

yes, the resistance between the two poles is zero.


---
**Cam Mayor** *August 01, 2015 13:38*

Mine had the connectors that attach to the Moshi board not crimped properly. I had to take the terminals out of the connectors and re-crimp them. It drove me crazy trying to find the issue as the voltage checks were all good but there was not enough of a connection to carry any current, Since the re-crimp the machine has worked perfect.


---
**Bogdan Tudor** *March 04, 2016 15:51*

#irresistiblecam Do you sort it out because I had the same problem . Just I buy this laser and after 2 days of work no tub lighting or cracking the power supply make a noise when I switch the test laser , current indication dead . It's the power supply , laser tub or both . Should I send it back .


---
**irresistiblecam** *March 04, 2016 23:38*

The ebay seller sent me a new power supply, but same problem, so I think that the problem is the tube, in everyway I got a total refund by paypal. You can not test the power supply or the laser tube, just the connection but it is easy, so if does not work you have 50-50 probabilty on laser tube or power supply. The replace parts cost is hight so I suggest to buy a new k40 laser machine, with the new k40 parts you could test the broken machine by exclusion. Use paypal or similar to buy it because of refund.


---
**Bogdan Tudor** *March 05, 2016 11:08*

 Indeed the cost of  part exchange are nearly a new one . They have send me a email to buy the broken part  on them behave . 

Thank you for replay .


---
*Imported from [Google+](https://plus.google.com/117845185832211596014/posts/RiYMqjgfj4X) &mdash; content and formatting may not be reliable*
