---
layout: post
title: "Hello ...Please help, totally new to this, and can't get further than switching it on, can't find driver for machine or software, thanks in advance Lorne"
date: December 17, 2016 12:52
category: "Original software and hardware issues"
author: "Lorne Mosley"
---
Hello ...Please help, totally new to this, and can't get further than switching it on, can't find driver for machine or software, thanks in advance





Lorne







**"Lorne Mosley"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 17, 2016 14:41*

Did you not receive a CD with the machine?



I have the contents of the CD that I received with mine shared here:



[drive.google.com - K40 Software CD.rar - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwamRBZkduekxlMHM/view?usp=sharing)


---
**Kelly S** *December 17, 2016 16:16*

Hope it came with the usb key at least if your using it stock.  


---
**Lorne Mosley** *December 17, 2016 18:19*

cam with everything, but finding it impossible to set laser


---
**Ned Hill** *December 17, 2016 20:43*

**+lorne mosley**

First steps....do you have coreldraw and the corellaser plugin installed?  Also the USB key plugged in?


---
**Lorne Mosley** *December 18, 2016 11:16*

YES


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2016 14:32*

**+lorne mosley** Not sure if I understand correct, so correct me if I'm wrong.



So, you've got CorelDraw & CorelLaser plugin installed. You've got the USB key plugged into the system. You've got the laser plugged into the system.



Now you need to open CorelLaser.exe file (not CorelDraw itself) to basically get it to open CorelDraw & the plugin. 



You will need to open the CorelLaser plugin options & set the board model (M2) & the board ID (sticker on the controller board inside the electronics bay of your machine). Without doing this, nothing will work correctly. It may seem to, but the machine will do stupid things at the most inopportune times.



Once done, you should be able to draw something & choose cut/engrave from either the toolbar in the top right of the screen (which sometimes disappears) or the system-tray icon (down near the clock in windows) by right clicking on it & choosing the appropriate function.



If all of this is going on, I'm not certain what you're having trouble with & request a bit more details.


---
*Imported from [Google+](https://plus.google.com/105312178201711703472/posts/16pQYymBxnm) &mdash; content and formatting may not be reliable*
