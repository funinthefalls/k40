---
layout: post
title: "k40 seems to be working fine, but my engraving is cropped!"
date: March 20, 2017 03:52
category: "Original software and hardware issues"
author: "John Tran"
---
k40 seems to be working fine, but my engraving is cropped!



seems to always crop my image that i am zapping.

Whats the deal?

using corel draw X7



See pics.



![images/c6a9a0dc106437a34e2c3731cf8c460a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c6a9a0dc106437a34e2c3731cf8c460a.jpeg)
![images/06dbb8d90e6438937013f39c9458a887.png](https://gitlab.com/funinthefalls/k40/raw/master/images/06dbb8d90e6438937013f39c9458a887.png)

**"John Tran"**

---
---
**Ned Hill** *March 20, 2017 12:59*

I would say check and make sure in your settings you have the right mainboard selected and the right device ID (Your device ID number is written on your controller board).  That can cause weird things like this to happen.

![images/7e30e8b8e3548a8412145680f907d8c7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7e30e8b8e3548a8412145680f907d8c7.png)


---
**John Tran** *March 20, 2017 17:39*

Yep, Ned tried that too... super weird. It'll do this once in a while.



ive completely un-installed and re-installed.


---
*Imported from [Google+](https://plus.google.com/113069068176332761588/posts/dFWt8SzZqKG) &mdash; content and formatting may not be reliable*
