---
layout: post
title: "Dialing in my work space by using a template to define my actual usable cutting area"
date: November 23, 2015 23:22
category: "Discussion"
author: "Todd Miller"
---
Dialing in my work space by using a template to

define my actual usable cutting area.



Best I can get is 290 x 190.  I'm OK with that.

I already know I want a bigger table/laser :-)

![images/8f3a7fe351bfc511245806f037baf249.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f3a7fe351bfc511245806f037baf249.jpeg)



**"Todd Miller"**

---
---
**Todd Miller** *November 25, 2015 00:42*

You can get the parts from light object;



ECNC-2M415     Mini 2 Phase 1.5A 1-axis Stepping Motor Driver



ECNC-SC04     Stepping motor controller board & tester



EPS-D24V5APS     DC 24V 5A Switching Power Supply





Oh and a DPDT switch (up/dwn) and some limit switches. 



[http://www.lightobject.info/viewtopic.php?f=16&t=2112](http://www.lightobject.info/viewtopic.php?f=16&t=2112)


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/bjBSazUgCQ3) &mdash; content and formatting may not be reliable*
