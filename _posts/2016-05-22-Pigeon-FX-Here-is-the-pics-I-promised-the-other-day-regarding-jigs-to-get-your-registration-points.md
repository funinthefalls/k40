---
layout: post
title: "Pigeon FX Here is the pics I promised the other day regarding jigs to get your registration points"
date: May 22, 2016 08:05
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
**+Pigeon FX** Here is the pics I promised the other day regarding jigs to get your registration points. As far as I'm aware, it shouldn't matter if you raise or lower the cutting bed (provided your beam is coming down perpendicular to the cutting bed). Each photo has individual comments. So, the main jig sits in place permanently, to always give your 0,0 origin point. Then you use secondary job-specific jigs to butt up to that origin point to give you your job-piece position. I would write the X-refer & Y-refer points onto that jig, so you know how far from origin (0,0) that you need to start the job, e.g. X: 25, Y: 25. Then whenever you cut/engrave it should always hit the spot that your job-piece is positioned in.



Hopefully that makes it make more sense.



![images/52061166163765944bc6ec3d622efd2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52061166163765944bc6ec3d622efd2b.jpeg)
![images/a82648cc9515db2c497e10c40c49bc5e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a82648cc9515db2c497e10c40c49bc5e.jpeg)
![images/eddd888b7f29533d442f7e4c3385867f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eddd888b7f29533d442f7e4c3385867f.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Pigeon FX** *May 22, 2016 08:53*

Thank you so much Yuusuf, that makes perfect sense, is ingeniously simple, and VERY much apprechiated!  


---
**Jim Hatch** *May 22, 2016 13:55*

I burn the sizes/coordinates into my job specific jigs since I tend to do similar things. Make mine from 1/4" stock to give some meat to them - I found 1/8" would sometimes make it easy to slip the work piece.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 22, 2016 16:31*

**+Jim Hatch** Burning them in is a good idea. I considered it, but was too lazy to setup an engrave/wait for it to do it so just used pencil.


---
**Jim Hatch** *May 22, 2016 17:28*

**+Yuusuf Sallahuddin**​ 😀 You've gotten past the "must laser everything" stage. Yesterday my daughter needed 2 10" rounds of cardboard for a cake base. When I said I'd cut it with the laser she rolled her eyes and sighed. 😁


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 04:49*

**+Jim Hatch** Yeah some things are just too time consuming & more standard methods do the job a lot quicker. I have got to the point where I just laser what requires precision. Strangely enough, most of my lasering is just testing what happens if I laser this, or what settings do I need to laser that.



edit: I wonder if you could cut the cake with the laser? hahaha


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/dcNNLYs74Ku) &mdash; content and formatting may not be reliable*
