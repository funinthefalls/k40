---
layout: post
title: "wheres the best place to get a new 40watt laser tube?"
date: September 12, 2016 14:51
category: "Discussion"
author: "John Austin"
---
wheres the best place to get a new 40watt laser tube? 

I am in the US.



My water pump that came with the laser quit running while running the laser. We heard a pop noise and went to investigate and found water leaking out of the tube and the pump not running.

Have less than a hour on this.



What is a recommended pump to go with other than the supplied pump?

I have contacted the seller and see what they will do, since the supplied pump quit working. 





**"John Austin"**

---
---
**greg greene** *September 12, 2016 15:04*

LIGHT OBJECTS SELLS THEM


---
**John Austin** *September 12, 2016 15:26*

Tube cost just about what I payed for the hole laser. 


---
**greg greene** *September 12, 2016 15:30*

True - but it is supposed to be a better tube - might be an opportunity to upgrade to the 50W


---
**Ariel Yahni (UniKpty)** *September 12, 2016 15:58*

I bought from these guys, very good quality tube, packaged in a wood box. Had it with me in the than 5 days 700mmx50mm 40W CO2 laser tube warranty 3 monthes 2500-3000 Hours life span for 40W CO2  engraving/rubber stamp machine plotter

 [aliexpress.com - 700mmx50mm 40W CO2 laser tube warranty 3 monthes 2500-3000 Hours life span for 40W CO2 engraving/rubber stamp machine plotter](http://s.aliexpress.com/U73iI7V3) 

(from AliExpress Android)


---
**John Austin** *September 13, 2016 21:42*

looks like the ebay seller is going to send me a replacement tube. see how long it takes. 


---
**Ariel Yahni (UniKpty)** *September 13, 2016 21:53*

**+John Austin**​ please ask them how are they packing it. This are very very fragile 


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/HD5d2gfGU9n) &mdash; content and formatting may not be reliable*
