---
layout: post
title: "One problem solved, another rears its head"
date: February 26, 2018 13:04
category: "Original software and hardware issues"
author: "Duncan Caine"
---
One problem solved, another rears its head.



Further to my wavy lines problem which I have now solved.  It was a loose Y axis stepper motor drive coupling.  The stepper motor was only driving one side of the gantry, the right side.  I had the frame out 3 times before I figured it out.



Now my next problem and any and all help would be appreciated. When I press Home (K40 Whisperer) the Y axis homes to top left, enters the limit switch and stops correctly, at which time the X axis moves left to right 50mm.  If I press Home again it moves another 50mm to the right and so on.  I should add that if I manually push it home I can engrave a piece as normal and at the end of the job it returns to where it started.  One other clue just discovered, the rails don't lock.



Any ideas?





**"Duncan Caine"**

---
---
**Don Kleinschnitz Jr.** *February 26, 2018 14:16*

Sounds a little like the X homing is not working properly. Could you have damaged the sensor, misadjusted the flag etc. when you were in there.

Did it do this before your other fix?


---
**Duncan Caine** *February 26, 2018 14:27*

**+Don Kleinschnitz** Don, it was working fine before.  I had the frame out, flat on the bench all wired up, and it was working just fine. I unplugged the ribbon cable and the small white main board plug prior to putting the frame back.  As I lifted it, I knocked the left hand stepper motor slightly (nothing severe, just a gentle nudge)on the way back into the case so I could have damaged one of those 2 small boards attached to the stepper motor.  What do you think?


---
**Duncan Caine** *February 26, 2018 16:28*

**+Don Kleinschnitz** Don you've nailed it I have damaged the limit switch in re-assembly.  Do you know where can I buy a new board?

![images/8357903e78dd70be9ef6c3e30d5cfcc2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8357903e78dd70be9ef6c3e30d5cfcc2.jpeg)


---
**Don Kleinschnitz Jr.** *February 26, 2018 20:09*

I have not found a source for replacement end stops however you can repair them. I have been known to repair them for those who cannot :)



[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Optical%20Endstops)


---
**Duncan Caine** *February 27, 2018 16:30*

**+Don Kleinschnitz** Thank you so much for the information.  I Araldited the 2 bits back together and hey presto everything now working as normal.  Thanks, Duncan


---
**Don Kleinschnitz Jr.** *February 27, 2018 18:46*

**+Duncan Caine** nice, I'm surprised that worked.


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/5eRwYRkgfgR) &mdash; content and formatting may not be reliable*
