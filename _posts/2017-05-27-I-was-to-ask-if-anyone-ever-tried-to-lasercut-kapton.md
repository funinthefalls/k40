---
layout: post
title: "I was to ask if anyone ever tried to lasercut kapton..."
date: May 27, 2017 09:27
category: "Materials and settings"
author: "Jeremie Francois"
---
I was to ask if anyone ever tried to lasercut kapton... But I see it is already being done! Since I already mill my PCBs, it would be one more reason for me to buy an overkill K40 ;) But the secondary question is the support. The Kapton below looks more like "peeling sheets" than rolls. But could it be secured onto a non-adhesive support surface, so it can be removed easily after being cut but without losing too much of its stickiness? I would understand that bed materials like HDPE are out of question as they would melt also in the process. Soapy glass? Do you have any clue? (below is a 6W laser doing the job).





**"Jeremie Francois"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 27, 2017 10:13*

Not exactly sure what kapton is or is used for, but judging from that video you can probably secure it on the K40 using a bed that is magnetic & use small magnets to hold it in place (that's how I secure all my workpieces).



Second point I'd note is that said 355nm laser. From what I am thinking, that is a diode laser of some kind which are a lot weaker in general than a K40 (40W as opposed to something like 2-5W). I'm wondering whether the overkill you mention will be to the point that it will melt significant amounts of your kapton instead of cutting cleanly. The power will only go so low on the K40 & still be able to fire. Also the speed will only go so high without issues too.



Maybe someone else has tried it, so hopefully they can weigh in.


---
**Jeremie Francois** *May 27, 2017 10:53*

**+Yuusuf Sallahuddin** thanks! Kapton is/was used a lot as a bed surface for 3D printers. Initially it is a highly temperature-resistant film and an excellent electrical insulator ([https://en.wikipedia.org/wiki/Kapton](https://en.wikipedia.org/wiki/Kapton)).

Since I have rolls of them b/c of 3D printing that I no more use, I could better use them as a protective layer for homemade milled PCB (in place of lacquer, which is harder to put right and cannot be undone). Obviously, holes must be made at the exact often tiny places, just like "stencils" for spreading solder paste for surface-mount components (note that the stencils are often either stainless steel - no way, probably even with a K40 - or vinyl, which I think is toxic to laser cut).



Using magnets seems enough indeed, I am not used to the laser cutting paradigm where there is no force exerted on the material!



And yes, I easily understand the K40 is overkill. The biggest issue regarding power may be to tune it down enough ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 27, 2017 12:07*

**+Jeremie Francois** Being temperature resistant material may work in your favour here. The K40 may be able to cut it without any dramas or too much melting issue. Not sure where you're located, but it might be an idea to see if one of the other users here is in your general region & willing to do a test run for you on a sample. Before you go ahead & buy a machine that may not be suitable for your intention.


---
**Jeremie Francois** *May 27, 2017 12:46*

**+Yuusuf Sallahuddin** thanks again, it makes sense. The nearby fablab is 40+km away, and the big lasercutter there is 100% used all the time... But I certainly have other uses for a K40 (not 40km!). It is probably the machine I eyed most for the last years, and so my house becomes a fablab on its own - minus a lathe, admittedly :)


---
*Imported from [Google+](https://plus.google.com/+JeremieFrancois/posts/BYBU6uvgZzZ) &mdash; content and formatting may not be reliable*
