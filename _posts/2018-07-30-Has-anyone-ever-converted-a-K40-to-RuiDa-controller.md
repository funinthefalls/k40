---
layout: post
title: "Has anyone ever converted a K40 to RuiDa controller?"
date: July 30, 2018 21:29
category: "Modification"
author: "E Caswell"
---
Has anyone ever converted a K40 to RuiDa controller?



I have RuiDa 6442G already I'm just looking at options.. 



I know I will have to do:



1, change out the ribbon cable, X stepper and both end stops so they can be wire direct to the steppers and board.



2, Install external steppers (Already have them)



I know some may say it's an expensive conversion, but when you put in the fact that I have wasted around £100 worth of materials so far this year due to drop outs.



I know I can run from the Glcd screen, but only when on smoothie.  I have to keep switching back from Grbl to do that.. Getting fed up with forever having to work around the machine rather than the machine working for me.



I am just going through the wiring details and working out a schematic so I know what and where it all goes.



I will possibly change over the ribbon cable and end stops on the the C3D board first that I can make sure those lll works before I go for the full conversion.



Any thoughts and advice welcome







**"E Caswell"**

---
---
**George Walker** *July 30, 2018 22:10*

I know a guy who swapped K40 internals (in bigger machine though) for a Ruida 6442G motherboard and the display. Works flawlessly according to him and I only saw the end result. He used the schematics he found online and just rewired the whole thing. As far as I know, he did not change anything else. He's an electrician by trade, so that went to his favour. Apart from that, I do not know much else, I just chimed in here to let you know that such conversion is very possible and it might involve much less hassle than you thought. :)


---
**Kelly Burns** *July 30, 2018 23:57*

Definitely not a value proposition.  Unless the controller is free.  Go with Cohesion 3D controller and Lightburn software.   About 1/4 of the cost and just as functional.  



Edit:  I’m unclear after reading the entire post... do you already own a c3d controller?  If so, what the issue?  



K40s are made from the cheapest components available.  Even with a new controller, they require constant care and feeding.  Meaning, aligning, tightening screws and belts.  When you treat them well. It’s pretty amazing what these sub $500 machines can do.


---
**E Caswell** *July 31, 2018 07:52*

**+Kelly Burns** thanks for the reply Kelly. I did think you hadn't read the full post.. 

 I do have a C3D already in. The issue I have is the drop outs and stopping in the middle of jobs.  It's much better now that it was when I first had it but stil happens.



I have had to throw away around £100 worth of materials so far this year because of the drop out issue. 



I know I can use it direct from the Glcd but that's only if running Smoothie. Even that I have had it stall and stop mid job, so I'm not certain it's even the so called USB issue. 



I have had the board at least 18months and I have inline filters, new USB ferrite protected USB cables, replaced the USB board with PCIe and own powered card. Don't get me wrong I couldn't even get one job out when I first had it, even turning a light on in the workshop killed it. That's been resolved but even my wife stating washing machines in the house last week killed it.



I'm just fed up with working around other issues and not being able to start the job without going through a pre flight checklist.

:-(



I already have the RuiDa.  




---
**Tom Brennan** *July 31, 2018 12:20*

All I know is that the C3D has major Brownout issues in the UK apparently its a 240V issue. I have found it unusable Ray has tried to help but never got around to a fix. I have a new machine coming this week with a Ruida maybe I can get some work done now and junk the C3D


---
*Imported from [Google+](https://plus.google.com/106553391794996794059/posts/266qxPRf51W) &mdash; content and formatting may not be reliable*
