---
layout: post
title: "Picked up a nice lab jack on eBay for 14 - perfect addition to my laser cutter!"
date: June 30, 2015 19:56
category: "Discussion"
author: "David Wakely"
---
Picked up a nice lab jack on eBay for £14 - perfect addition to my laser cutter!

![images/fe6dc287a9fe6d009ddcb983c7b3f9b0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe6dc287a9fe6d009ddcb983c7b3f9b0.jpeg)



**"David Wakely"**

---
---
**ThantiK** *June 30, 2015 22:40*

How's that 3D printed air assist working for you?


---
**David Wakely** *July 01, 2015 12:41*

Seems to be working good - I've ordered a new one from ebay with a red dot laser on it to help with alignment


---
**Troy Baverstock** *July 04, 2015 08:35*

What's the bed levelling like on this thing? Is it consistent when raising?


---
**David Wakely** *July 05, 2015 21:22*

It does seem to be pretty good. I haven't noticed anything drastic


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/XVxArQpqCDB) &mdash; content and formatting may not be reliable*
