---
layout: post
title: "First attempt at stencils. Got my laser power tuned in, but totally need an air assist to keep the material from burning"
date: June 21, 2016 22:19
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
First attempt at stencils. Got my laser power tuned in, but totally need an air assist to keep the material from burning. 

![images/93878438f3cc23294539c9ab17091bc9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/93878438f3cc23294539c9ab17091bc9.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Stephen Sedgwick** *June 21, 2016 22:28*

What are you making?  templates for circuit boards?


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 22:29*

Yep.  I do a lot of PCBs.


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 22:30*

To clarify, it's a solder stencil for the application of solder paste.


---
**Evan Fosmark** *June 21, 2016 22:41*

This is great. Been thinking about this same thing. No more orders to OSH Stencils :) What material did you go with? I'm guessing transparency film?


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 22:45*

Yeah, this is from staples online.  100 pieces for 16 bucks.  Unfortunately I think I'll go through all of it a little too quickly :)

I'm too stubborn to pay for OSHStencils each time I get a new board/ revision made.


---
**Jim Hatch** *June 21, 2016 23:54*

What speed are you using? High speeds can reduce/eliminate burning. Of course that may not give you enough power to cut but worth playing with your speed/power settings.


---
**Ariel Yahni (UniKpty)** *June 21, 2016 23:59*

Use a blower fan if you have one. Build a 3d printed bracket and place it on gantry. For me it has the same effect or better as an airbrush compressor I have.


---
**Alex Krause** *June 22, 2016 01:37*

Multiple low power passes?


---
**dstevens lv** *June 22, 2016 05:56*

I've made that same one...  ;-)



I use low power (10%), moderate speed (80%) on an FSL/GWieke 80 wqtt 6040.  I raster them, not vector cut at the suggestion of Ryan O'Hara by way of and Adafruit post. [https://learn.adafruit.com/smt-manufacturing/laser-cut-stencils](https://learn.adafruit.com/smt-manufacturing/laser-cut-stencils)



I flow with a Rocket Scream/Uno controller ( [http://www.rocketscream.com/blog/product/reflow-oven-controller-shield-arduino-compatible/](http://www.rocketscream.com/blog/product/reflow-oven-controller-shield-arduino-compatible/) )and $30 toaster oven.  Works better than the low end Chinese oven at Techshop.


---
**Justin Mitchell** *June 23, 2016 09:19*

I have it seen suggested heavily that you should use mylar sheet for stencils like that, not acetate. It's a bit more expensive but supposedly behaves much better.


---
**Richard** *June 23, 2016 18:23*

That thin and flimsy of a material, air assist could potentially cause it to vibrate or move. Just something to be aware of and prepare for. Thicker/heavier material might be better, or you might need to make some sort of fixture to hold it down. That said, this is the air assist nozzle I use and I'm happy with it: [http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)  You will need an air pump of some sort, I recommend one for an aquarium as they are a lot smaller than a compressor and less likely to include oil/water in the air line which could potentially affect your cut.


---
**dstevens lv** *June 24, 2016 05:23*

If you are using a machine larger than a K40 the exhaust is going to move the sheet more than the air assist.  Depending on my table height (which can set the size of the exhaust slot) the exhaust can pull the sheet toward the slot.   Even with a minimal opening it still ruffles the sheet.



When I cut a stencil I use masking tape on the corners, pulling the sheet as tight as I can against the bed.  I've used a wood backing/bed at first but now just tape the sheet to the honeycomb.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/aA3SpXLQwk6) &mdash; content and formatting may not be reliable*
