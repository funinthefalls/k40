---
layout: post
title: "After trial and error these are some i did last night"
date: October 19, 2016 14:07
category: "Object produced with laser"
author: "Scott Thorne"
---
After trial and error these are some i did last night. 



![images/a81f02621d5003ce42e7ab9a3e2340a5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a81f02621d5003ce42e7ab9a3e2340a5.jpeg)
![images/8f0816a47c2110ef0ae9e48a6db36b11.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f0816a47c2110ef0ae9e48a6db36b11.jpeg)
![images/0c11c663c1b186b17fd474274c3b7b07.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c11c663c1b186b17fd474274c3b7b07.jpeg)

**"Scott Thorne"**

---
---
**Don Kleinschnitz Jr.** *October 19, 2016 14:33*

Wow!


---
**Nigel Conroy** *October 19, 2016 15:11*

Great work. Same procedure you described in previous post or have you tweaked your technique?


---
**Scott Thorne** *October 19, 2016 15:21*

Same technique...just more passes at lower max power settings


---
**greg greene** *October 19, 2016 16:46*

DSP or Smoothie?


---
**Scott Thorne** *October 19, 2016 16:47*

Ruida RDC6442g DSP


---
**greg greene** *October 19, 2016 16:50*

Laserweb ? or proprietary software - it sure looks like you get very fine control with the Ruda - I wonder of the Smoothie can do that.


---
**Scott Thorne** *October 19, 2016 16:56*

Its proprietary software...RDWorks version 8.01.019


---
**greg greene** *October 19, 2016 17:01*

Thanks - I'm wondering if the Ruda board would work with the K40 - I believe you have a different laser.


---
**Scott Thorne** *October 19, 2016 17:09*

Yeah i don't have a k40 anymore...i have a shenhui 50 watt 500mm x 300mm machine now...i just like the forum here and a lot of the people on here i consider friends...lol


---
**greg greene** *October 19, 2016 17:10*

Thanks Scott !


---
**Scott Thorne** *October 19, 2016 17:13*

You bet.


---
**greg greene** *October 19, 2016 17:19*

Is your machine one like this?

[https://wholesaler.alibaba.com/product-detail/hot-sale-CO2-3d-laser-inside_60537318613.html](https://wholesaler.alibaba.com/product-detail/hot-sale-CO2-3d-laser-inside_60537318613.html)


---
**Scott Thorne** *October 19, 2016 18:24*

[https://www.amazon.com/gp/aw/d/B01HUW50UK/ref=mp_s_a_1_1?ie=UTF8&qid=1476901408&sr=8-1&pi=AC_SX236_SY340_FMwebp_QL65&keywords=50+watt+laser+engraver&dpPl=1&dpID=41uP%2B9QRqJL&ref=plSrch...that's](https://www.amazon.com/gp/aw/d/B01HUW50UK/ref=mp_s_a_1_1?ie=UTF8&qid=1476901408&sr=8-1&pi=AC_SX236_SY340_FMwebp_QL65&keywords=50+watt+laser+engraver&dpPl=1&dpID=41uP%2B9QRqJL&ref=plSrch...that%27s) mine here


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 20, 2016 04:48*

Wow, love the Koi fish. Looks great. You're becoming a pro at this stuff :)


---
**Scott Thorne** *October 20, 2016 09:41*

**+Yuusuf Sallahuddin**....thanks my friend...this all comes with a lot of wasted time and wood too....lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 20, 2016 11:18*

**+Scott Thorne** Yeah I bet, I've had similar experiences with just engraving things normally.


---
**Scott Thorne** *October 20, 2016 11:37*

I guess it's par for the course


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/RMKkctVab71) &mdash; content and formatting may not be reliable*
