---
layout: post
title: "FYI: $293.00 w/ free shipping. Same unit (and seller) I just bought for $349"
date: February 27, 2017 21:54
category: "Discussion"
author: "Lance Ward"
---
FYI:  $293.00 w/ free shipping.  Same unit (and seller) I just bought for $349. and was delivered a few days ago.  Damn...





**"Lance Ward"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 27, 2017 22:10*

Credit card purchase? Price protection feature? 


---
**Ray Kholodovsky (Cohesion3D)** *February 27, 2017 22:16*

I paid $340 shipped for mine last spring. That was actually pretty good at the time.  A few people snagged it for 320 shortly after that.  I think approaching 280 is the lowest we've ever seen, and that may not be with the $10 a lot of these guys charge for shipping included. 

This is a good price to buy just to gut for parts.  That said, I think you'll need $350 - 400 for a good tube, optics, and psu since everything else included here is even moreso junk. And by spending individually you'll get brand name stuff like a reci tube... Anyways, rant over.  Good deal for actual K40.


---
**Cesar Tolentino** *February 27, 2017 22:43*

This is very good. I need a new tube for my 4yr old k40. Might buy this instead


---
**Mark Brown** *February 28, 2017 01:49*

D:  Dammit, aI just paid $345 two weeks ago.



There's another one in the "related items" section for the same price, wonder what's up?


---
**Wolfmanjm** *February 28, 2017 06:03*

says it is $400 now :(


---
**Stephane Buisson** *February 28, 2017 13:35*

how many of them will be lost in shipping then refunded a month later ? (a classic to get cashflow)  until you got it, don't shout 'Victory'


---
**Madyn3D CNC, LLC** *February 28, 2017 16:51*

it's up to $400 now, I was ready to buy it too. For that price, it's a steal. (and a back up laser/parts laser)


---
**Steven Whitecoff** *February 28, 2017 17:54*

Getting the older analog panel/conrol system makes sense pehaps if you getting a Smoothie board and GLCD anyway otherwise I'd vote for the digital panel/control w/Nano board version at $475 delivered.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 17:57*

Steven makes a valid point, the digital display models do not make the Cohesion3D/ Smoothie conversion any easier. 


---
*Imported from [Google+](https://plus.google.com/114422684423642232545/posts/AMW3DtaLRuk) &mdash; content and formatting may not be reliable*
