---
layout: post
title: "New GLDC display housing/"
date: January 01, 2018 16:22
category: "Modification"
author: "Don Kleinschnitz Jr."
---
New GLDC display housing/

[https://plus.google.com/+DonKleinschnitz/posts/BydQsxU4BQH](https://plus.google.com/+DonKleinschnitz/posts/BydQsxU4BQH)





**"Don Kleinschnitz Jr."**

---
---
**Alex Krause** *January 01, 2018 16:47*

How did you get the crackle finish?


---
**Don Kleinschnitz Jr.** *January 01, 2018 18:41*

**+Alex Krause** well I'd like to take credit for knowing what I was doing. Or that I could repeat it .....



1.) sprayed with cheap flat black rattle-can, assuming it lacquer

2.) sprayed it with Minwax Helmsman spar urethane (wanted to protect it and get a matt finish.

4.) didn't like the urethane and sprayed it with Rustoleum K40 color (" sail blue") from a rattle can (assume it was lacquer). The urethane coat was still tacky.....



From my research it seems this happens when the first coat it not dry. I also painted in a pretty cold shop. The areas that crackled the most were the least dry. 



**+Ned Hill** any chemistry ideas??


---
**Ned Hill** *January 01, 2018 20:08*

Nothing chemistry wise.  It's a cool effect though.  


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/63kXvN7D12y) &mdash; content and formatting may not be reliable*
