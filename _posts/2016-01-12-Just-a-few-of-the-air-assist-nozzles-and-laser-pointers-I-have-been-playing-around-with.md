---
layout: post
title: "Just a few of the air assist nozzles and laser pointers I have been playing around with"
date: January 12, 2016 03:15
category: "Object produced with laser"
author: "Anthony Bolgar"
---
Just a few of the air assist nozzles and laser pointers I have been playing around with. Also my new air assist compressor (runs on 12V), and a couple of other toys (3d printer, vinyl plotter) I had the perfect place to put my K40, on a desktop that was built into an unused fireplace recess, was able to exhaust it straight up through the unused chimney.



![images/b4e0cdf9fe78340e180c25fb3b4abe01.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b4e0cdf9fe78340e180c25fb3b4abe01.jpeg)
![images/a453ca4ba790969d695ba0199130346b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a453ca4ba790969d695ba0199130346b.jpeg)
![images/890ce75e3ecbd39afbca360639555899.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/890ce75e3ecbd39afbca360639555899.jpeg)
![images/202f04af2dc9cc86e20dc6111df7cddf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/202f04af2dc9cc86e20dc6111df7cddf.jpeg)
![images/7b4590e6e2ebf2ca29af5fd4407e04c3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b4590e6e2ebf2ca29af5fd4407e04c3.jpeg)
![images/3eb23a3a5d499384ed1ed5df6c6b59b9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3eb23a3a5d499384ed1ed5df6c6b59b9.jpeg)

**"Anthony Bolgar"**

---
---
**Juan Gianni** *January 18, 2016 18:22*

hi Anthony. I have a problem and maybe you can help me. 

buy KC40 3020 laser machine but the USB key does not work. you have the files on the USB key? Thanks very much !!




---
**Anthony Bolgar** *January 19, 2016 01:54*

The USB key only activates the software, the files are on the CD thacame with the machine.


---
**Scott Thorne** *January 19, 2016 23:54*

Nice set-up +Anthony Bolgar!


---
**Anthony Bolgar** *January 19, 2016 23:55*

Thanks Scott


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/GxhNzLSav64) &mdash; content and formatting may not be reliable*
