---
layout: post
title: "can anyone help me with my k40, i'm trying to make a rubber stamp blah blah, but it keeps on burning it instead of engraving, (changed the board to M2) but it engraves too shallow"
date: February 11, 2016 13:06
category: "Discussion"
author: "Mike Maglaque"
---
can anyone help me with my k40, i'm trying to make a rubber stamp blah blah, but it keeps on burning it instead of engraving, (changed the board to M2) but it engraves too shallow





**"Mike Maglaque"**

---
---
**Justin Mitchell** *February 11, 2016 14:26*

In my experience it will only engrave quite shallow in one pass, you need to do multiple passes and a medium power, and if the stamp is big it can take aaaaaages.


---
**Justin Mitchell** *February 11, 2016 14:28*

An example i cut [https://goo.gl/photos/6yg1Px66nvF7yZZAA](https://goo.gl/photos/6yg1Px66nvF7yZZAA)


---
**Anthony Bolgar** *February 11, 2016 14:28*

Air assist head makes a big difference, to engrave the rubber without flames it is almost a necessity.




---
**Alexander Krasner** *February 17, 2016 12:16*

The most important thing is the right foc us distance to rubber, speed, resolution and 2-3 pathes. Check my laser educational video how to do laser stamp at  [https://www.youtube.com/user/katavasik](https://www.youtube.com/user/katavasik)


---
**Anthony Bolgar** *February 17, 2016 14:16*

Nice video....where did you get the grey rubber, I can only find the pink stuff that flames up easily.


---
**Alexander Krasner** *February 17, 2016 14:18*

I have a STORE of engraving supplies and i have in stock this rubber in different thickness and purpose , my store in germany [http://www.gravierbedarf.de/epages/63201300.sf/de_DE/?ObjectPath=/Shops/63201300/Categories/StempelGummi](http://www.gravierbedarf.de/epages/63201300.sf/de_DE/?ObjectPath=/Shops/63201300/Categories/StempelGummi)


---
*Imported from [Google+](https://plus.google.com/108423110688510228345/posts/KBM7udpokRc) &mdash; content and formatting may not be reliable*
