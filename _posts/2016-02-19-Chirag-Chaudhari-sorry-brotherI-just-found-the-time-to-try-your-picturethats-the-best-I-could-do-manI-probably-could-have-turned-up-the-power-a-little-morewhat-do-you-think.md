---
layout: post
title: "Chirag Chaudhari .....sorry brother..I just found the time to try your picture....that's the best I could do man.....I probably could have turned up the power a little more....what do you think?"
date: February 19, 2016 23:41
category: "Discussion"
author: "Scott Thorne"
---
**+Chirag Chaudhari**.....sorry brother..I just found the time to try your picture....that's the best I could do man.....I probably could have turned up the power a little more....what do you think?

![images/405b6ac386f02bb6ade5e51ab41a4df2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/405b6ac386f02bb6ade5e51ab41a4df2.jpeg)



**"Scott Thorne"**

---
---
**Sean Cherven** *February 19, 2016 23:50*

That's still not bad. I'd turn the pwr up a little too..


---
**Scott Thorne** *February 20, 2016 00:07*

That's birch plywood...not going to get a dark burn on it no matter what I do....if I do then the light places are to dark too....I need to buy some alderwood.


---
**Scott Thorne** *February 20, 2016 00:07*

It looks like a photo though....Lol


---
**Sean Cherven** *February 20, 2016 00:12*

I see. I still have yet to engrave anything other than acrylic lol, so I lack in that area


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 20, 2016 01:01*

The result is quite nice. A touch darker would be a bonus, but as is it looks pretty clear.


---
**Scott Thorne** *February 20, 2016 12:30*

I still have to find the sweet spot with the power for the birch....I'm going to try and find some alderwood today.






---
**Nathaniel Stenzel** *February 23, 2016 00:57*

Would results differ if stained before lasing?


---
**Scott Thorne** *February 23, 2016 16:59*

**+Nathaniel Stenzel**...it's just this cheap Baltic birch plywood. ..good for cutting not engraving. ...stained wood doesn't do so well. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/g3cbDfTRVes) &mdash; content and formatting may not be reliable*
