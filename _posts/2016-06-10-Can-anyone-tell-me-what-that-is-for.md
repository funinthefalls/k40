---
layout: post
title: "Can anyone tell me what that is for?"
date: June 10, 2016 12:48
category: "Discussion"
author: "Christoph E."
---
Can anyone tell me what that is for? I found it in the package with the laser when I wanted to dispose it. It says "package silicine adhesive sealant" and I have no idea what it's for.  :-)

![images/8f2c1bb4ae615b037beaad6a72074b81.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f2c1bb4ae615b037beaad6a72074b81.jpeg)



**"Christoph E."**

---
---
**Ulf Stahmer** *June 10, 2016 12:55*

Silicone is used to seal the electrical connections on the laser tube.  On a new machine, they should be ok.  It will be handy to have for when you replace your tube.


---
**Christoph E.** *June 10, 2016 12:58*

**+Ulf Stahmer** Thank you. :-)



So hopefully I'm not going to need it soon. :D


---
**Stephane Buisson** *June 10, 2016 13:24*

**+Chris E** sometime (like 4 me) the tube come in a different package and you have to install it yourself. but as far as I know everybody got 1 tube.


---
*Imported from [Google+](https://plus.google.com/100193166302371572888/posts/UsV5tWSztxq) &mdash; content and formatting may not be reliable*
