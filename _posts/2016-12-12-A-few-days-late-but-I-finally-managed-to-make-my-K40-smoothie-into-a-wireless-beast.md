---
layout: post
title: "A few days late, but I finally managed to make my K40 smoothie into a wireless beast"
date: December 12, 2016 15:05
category: "Smoothieboard Modification"
author: "Darren Steele"
---
A few days late, but I finally managed to make my K40 smoothie into a wireless beast







**"Darren Steele"**

---
---
**Alex Krause** *December 12, 2016 15:53*

Can't wait to see your projects on the LaserWeb Community


---
**greg greene** *December 12, 2016 16:12*

Great Mod - Looks like it's Pi time - with a slice of cheese :)


---
*Imported from [Google+](https://plus.google.com/+DarrenSteele/posts/AQfdiLSYxPi) &mdash; content and formatting may not be reliable*
