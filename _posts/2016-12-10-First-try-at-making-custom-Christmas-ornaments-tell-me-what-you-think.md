---
layout: post
title: "First try at making custom Christmas ornaments tell me what you think"
date: December 10, 2016 23:04
category: "Object produced with laser"
author: "3D Laser"
---
First try at making custom Christmas ornaments tell me what you think

![images/16dc62b8fc3d8400505075820785652f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/16dc62b8fc3d8400505075820785652f.jpeg)



**"3D Laser"**

---
---
**Bob Damato** *December 10, 2016 23:09*

Those are great. Ive thought of a similar type of thing but have no discernible painting skills.


---
**Jim Hatch** *December 10, 2016 23:40*

Mask the wood with painters tape, lightly (or more) etch the parts you want painted, spray them blue when they're done. Remove painters tape. ☺


---
**3D Laser** *December 10, 2016 23:44*

The blue is actually an inlay so I stained an entire sheet masked it and cut out all the shapes then glued them in place


---
**Jim Hatch** *December 11, 2016 00:07*

Nice inlay work. Looks like you're spot on with your kerf adjustment.


---
**3D Laser** *December 11, 2016 00:23*

**+Jim Hatch** honestly I didn't adjust anything. up close you can see the spaces but for what it is they turned out nice 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 04:04*

Nice. I see you've worked out your masking/painting issues a bit better :)



edit: nevermind me. I should read comments first. I see now it's an inlay. Looks great though.


---
**3D Laser** *December 11, 2016 04:28*

**+Yuusuf Sallahuddin** still having issues with masking when I stain the wood tape doesn't want to stick to it so I am still getting bleed but I did order liquid masking to give that a try... btw I finally responded to our google hangout about a box drawer design I needed help with 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 05:09*

**+Corey Budwine** Ah, I've noticed recently that my masking jobs are having a bit of bleed through too. I wonder if using an airbrush or spray paint instead of a paintbrush/roller will prevent that. I'll give it a test some time (when I'm not lazy) & let you know how it goes.



I'll check the hangouts & see :)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/ZepqLY96pnj) &mdash; content and formatting may not be reliable*
