---
layout: post
title: "What's difference between Corel Laser and Laser DRW?"
date: May 15, 2016 20:33
category: "Software"
author: "Blake Ingram"
---
What's difference between Corel Laser and Laser DRW?

Do you need both?

Is one preferred over the other?

I really prefer to use Adobe Illustrator and Photoshop over Corel Draw But if it's only a gateway to the laser cutter its self, then I'm fine with that.. Any and all feedback is greatly appreciated, thanks.





**"Blake Ingram"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 15, 2016 21:01*

CorelLaser is a plugin for CorelDraw. LaserDRW is a standalone software that is pretty average. You don't need both. Stick with CorelLaser.



I use AI to design, then either save as AI9 format or SVG. Then import into CorelDraw/CorelLaser, hit cut/engrave.


---
**Tony Schelts** *May 15, 2016 23:09*

CorelLaser is much better than LaserDrw.  There is a corelDraw 5x download with a crack on here somewhere.  I have used it and works great.  I bought CorelDraw X7 home and student but CorelLaser doesnt work unless its a corporate version for some reason..



Anyway CorelDraw and CorelLaser will do all you need pretty much


---
*Imported from [Google+](https://plus.google.com/101957775596878882298/posts/XCRAsdFiuCA) &mdash; content and formatting may not be reliable*
