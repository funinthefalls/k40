---
layout: post
title: "Originally shared by John Milleker Jr. I wanted to share this test photo engrave made on a Chinese K40 Laser running the Cohesion3DMini controller (running GRBL-LPC, but Smoothie can do this too, I changed firmware just"
date: July 31, 2017 14:58
category: "Object produced with laser"
author: "Stephane Buisson"
---
<b>Originally shared by John Milleker Jr.</b>



I wanted to share this test photo engrave made on a Chinese K40 Laser running the Cohesion3DMini controller (running GRBL-LPC, but Smoothie can do this too, I changed firmware just because skipping issues with my machine). 3mm Baltic Birch plywood, two passes. Lightobjects head with air assist. LaserWeb4 on a Raspberry Pi 3.



I think I'm darned near 'there'. This is as dark as I can get birch without really tanking my speed. This is two passes at 150mm/s.



![images/e963097e79ce62ffb844960fec08d373.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e963097e79ce62ffb844960fec08d373.jpeg)
![images/ebc8c039314d8b1701b8cb95924c9140.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ebc8c039314d8b1701b8cb95924c9140.jpeg)

**"Stephane Buisson"**

---
---
**greg greene** *July 31, 2017 15:11*

Looks great - I have the same board and also have 'skipping' issues - how did you change the firmware?

Thanks


---
**Paul de Groot** *August 01, 2017 06:52*

Where can I find the original photo of this engraving. I would love to try this out on my convered trotec laser.


---
**nayeem khatib** *August 02, 2017 03:56*

Great work ...

What's the pre processing done ?

If you could share that would be of great help 


---
**Paul de Groot** *August 03, 2017 22:03*

I like the hard contours on the arms. Did you enhance the photo ? I just engraved like for like. Still like the result 😁

![images/87741d0645ced718cc7bf39aed3fa1d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/87741d0645ced718cc7bf39aed3fa1d2.jpeg)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/T3sTeQxQ2wx) &mdash; content and formatting may not be reliable*
