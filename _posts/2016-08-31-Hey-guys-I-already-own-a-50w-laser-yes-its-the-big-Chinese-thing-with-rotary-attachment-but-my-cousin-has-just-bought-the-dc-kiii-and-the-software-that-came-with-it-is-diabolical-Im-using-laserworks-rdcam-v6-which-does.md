---
layout: post
title: "Hey guys I already own a 50w laser yes it's the big Chinese thing with rotary attachment but my cousin has just bought the dc-kiii and the software that came with it is diabolical I'm using laserworks (rdcam v6) which does"
date: August 31, 2016 18:23
category: "Discussion"
author: "dave wilkes"
---
Hey guys I already own a 50w laser yes it's the big Chinese thing with rotary attachment but my cousin has just bought the dc-kiii and the software that came with it is diabolical I'm using laserworks  (rdcam v6) which does everything I need .

The question is does laserworks work with his model and if not what other alternatives are there.



He was thinking of sending it back and getting the k40 but once again does laserworks work on that .



Cheers 





**"dave wilkes"**

---
---
**Alex Krause** *August 31, 2016 19:55*

Would need to know the control board installed in his machine. Laserworks and Rdworks are for Ruida controllers I believe 


---
**Alex Krause** *August 31, 2016 19:57*

Just googled the dc-kiii looks like it's a K40 see if it's a M2 nano or Moshi controller


---
**dave wilkes** *August 31, 2016 20:38*

I'm not sure of the board as it's at my cousins house but the software was coral draw 12 and laserdrw or something like that I know that the board when I looked was pretty small and was kind of attached to the front on the machine with one usb port 


---
**Alex Krause** *August 31, 2016 20:40*

He has a k40 he is stuck with the laserdrw software with corel plugin unless he buys a DSP controller which is 400$/ switches to a smoothie board which is about 100$ or buys a different laser with a different controller 


---
**Alex Krause** *August 31, 2016 20:45*

You can purchase a smoothie board from [www.uberclock.com](http://www.uberclock.com) or [www.smw3d.com/smoothieboard](http://www.smw3d.com/smoothieboard) if you live in the USA if you live in the UK [www.robotseed.com](http://www.robotseed.com) has smoothie boards... a DSP can be purchased from [www.lightobject.com](http://www.lightobject.com)


---
**dave wilkes** *August 31, 2016 23:35*

Thanks for the advice I'll tell him oh do you know if smartcarve would work 


---
**dave wilkes** *September 02, 2016 15:41*

Anyone ? ^^^^


---
*Imported from [Google+](https://plus.google.com/100840041252238699532/posts/5HswveRgfm8) &mdash; content and formatting may not be reliable*
