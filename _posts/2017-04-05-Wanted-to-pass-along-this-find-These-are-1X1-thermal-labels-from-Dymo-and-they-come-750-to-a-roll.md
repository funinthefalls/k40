---
layout: post
title: "Wanted to pass along this find. These are 1\"X1\" thermal labels from Dymo and they come 750 to a roll"
date: April 05, 2017 00:56
category: "Materials and settings"
author: "Bob Buechler"
---
Wanted to pass along this find. These are 1"X1" thermal labels from Dymo and they come 750 to a roll. 



At the moment, Amazon is selling them for $11 which they claim to be a 43% discount: [https://www.amazon.com/dp/B00004Z60O/](https://www.amazon.com/dp/B00004Z60O/)



I took a couple test shots at my lowest fireable power and included it in the photo below. They don't burn through (saving your mirrors), and they make a big, clear thermal mark with a little bubbling at the exact point of impact. This combo should give you great visibility at a distance as well as accurate enough detail up close.



They stick really well, so they won't fall off during testing either. Get em while they're cheap.

![images/45a580d20571ab832bc446a495688a40.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/45a580d20571ab832bc446a495688a40.jpeg)



**"Bob Buechler"**

---
---
**Don Kleinschnitz Jr.** *April 05, 2017 01:30*

Got some,  thanks for post. 


---
**HalfNormal** *April 05, 2017 02:34*

You just reminded me that I have a bunch of thermal labels from an old thermal label printer. Now I just have to remember where I stashed them!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 04:59*

That's interesting. Going to have to see if my local Officeworks has these. Will be much better than cutting 1000 pieces of masking tape haha.



edit: For anyone in Australia, this may be a similar product: [https://www.officeworks.com.au/shop/officeworks/p/zebra-thermal-transfer-label-roll-100-x-50mm-1000-labels-inz1294548](https://www.officeworks.com.au/shop/officeworks/p/zebra-thermal-transfer-label-roll-100-x-50mm-1000-labels-inz1294548)


---
**E Caswell** *April 05, 2017 08:41*

I have one of those label makers but never thought of using them for that use.. 

Link for UK buyers..they use a different pack code in Europe.



[amazon.co.uk - Dymo S0929120 LabelWriter Multi-Purpose Labels, Self-Adhesive, 25 x 25 mm, Roll of 750 - Black Print on White: : Office Products](https://www.amazon.co.uk/Dymo-S0929120-LabelWriter-Multi-Purpose-Self-Adhesive/dp/B004G6PBL2/ref=sr_1_2?ie=UTF8&qid=1491381574&sr=8-2&keywords=dymo+25x25+thermal+labels)


---
**Bob Buechler** *April 05, 2017 17:39*

Note: Because the adhesive on these is so good, there's no need to stick them on too heavily, unless you enjoy Goo Gone'ing your mirror mounts. :P As sticky as they are, they come off easily if they aren't left on for a long time.


---
**Tammy Mink** *July 21, 2017 12:08*

Thanks, these may help me since I am having a heck of a time aligning my mirrors :)




---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/iLZayRczLZM) &mdash; content and formatting may not be reliable*
