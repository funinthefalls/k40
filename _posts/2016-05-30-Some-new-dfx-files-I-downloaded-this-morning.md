---
layout: post
title: "Some new dfx files I downloaded this morning"
date: May 30, 2016 13:38
category: "Object produced with laser"
author: "Scott Thorne"
---
Some new dfx files I downloaded this morning.

![images/c11a7ab446896d4269b5b703e2e90c12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c11a7ab446896d4269b5b703e2e90c12.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 30, 2016 15:04*

I like the lion one. Reminds me of those family crest shields.


---
**Scott Thorne** *May 30, 2016 15:10*

I think it is a family Crest **+Yuusuf Sallahuddin**...lol


---
**Alex Krause** *May 30, 2016 15:46*

Where did you find the "end of the trail"


---
**Alex Krause** *May 30, 2016 15:47*

They use the lion as a family crest for the Lannisters in Game of Thrones :P


---
**Scott Thorne** *May 30, 2016 15:58*

[http://mydxf.blogspot.com/2010/07/dxf....it's](http://mydxf.blogspot.com/2010/07/dxf....it's) in here somewhere **+Alex Krause**


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 30, 2016 19:56*

**+Scott Thorne** Might be a product that could be sold. Custom family crest multi-layered ply artwork?


---
**Scott Thorne** *May 30, 2016 19:59*

**+Yuusuf Sallahuddin**...it could be at that...lol...I'm ordering some cherrywood sheets today...some alder wood too....I've got more plywood than I know what to do with. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 30, 2016 20:01*

**+Scott Thorne** Yeah some other woods might be a lot nicer than ply. Ply is great for test products (due to it's ease of cutting).


---
**Alex Krause** *May 30, 2016 21:04*

**+Scott Thorne**​ Lol I can find uses for that plywood you don't know what to do with ;) where are you ordering your cherry from?


---
**Scott Thorne** *May 30, 2016 21:37*

**+Alex Krause**...I'm going to try amazon....I just ordered some alder wood from there....6 pieces 15 x 6 x1/4 for 20 bucks


---
**Scott Thorne** *May 30, 2016 21:38*

I might go to lowes or home depot for the cherry....couldn't find it on amazon.


---
**ED Carty** *June 01, 2016 22:02*

**+Scott Thorne** Nice work Bro


---
**Scott Thorne** *June 01, 2016 22:06*

**+ED Carty**...thanks man


---
**ED Carty** *June 01, 2016 22:18*

**+Scott Thorne** I watch for your postings. you always come up with cool stuff. Like all the stuff i would want to do.


---
**Scott Thorne** *June 01, 2016 22:36*

Thanks **+ED Carty**


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/e6nQEfggNzV) &mdash; content and formatting may not be reliable*
