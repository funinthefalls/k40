---
layout: post
title: "Cross posting from Facebook. If you already saw it on there, my apologies: Question on mirror and tube mounts: what do you guys recommend?"
date: December 10, 2015 16:36
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Cross posting from Facebook. If you already saw it on there, my apologies:



Question on mirror and tube mounts: what do you guys recommend? To say that the mounts that come with the K40 are crap is an insult to the word 'crap'. So I'm looking to replace them, ideally without also incurring the cost of replacing the current mirrors at the same time - I'd like to do those at a later time. The ones that came with my machine are actually metal mirrors, not glass coated. So they're not total trash.



This came up yesterday after a thorough cleaning and recalibrating. All this time I've been using the machine in a so-so state, it works, I'm making it work. But, I know things aren't perfectly aligned like they should. The beam is too high and by the time it reaches the head, it's hitting the upper edge of the hole (going into the head.) Going all the way to the tube and the first (rear) mirror, the beam isn't hitting it centered, it's actually pretty high already. There is no adjusting the tube nor mirrors. The tube is clamped down with metal straps.

Since I'm coming up to a short lull in work, I'd like to tackle this and do it right. Put better mounts on the tube that I can adjust the height on, as well as replacing all the mirror mounts.



Suggestions/recommendations? LightObject? Someone/Something else? Should I just 3D print my own?





**"Ashley M. Kirchner [Norym]"**

---
---
**Gary McKinnon** *December 10, 2015 16:51*

i'VE HEARD GOOD THINGS ABOUT THE LIGHTOBJECT MOUNTS. Sorry, caps-lock!



It's also worth searching the big 3d printing sites like thingiverse that offer free downloads, there are a few K40 bits and pieces available.


---
**Ashley M. Kirchner [Norym]** *December 10, 2015 16:57*

That's kinda what I'm wondering, buying ready made (and ideally proven to work) versus printing my own. Doing it myself would obviously give me the ability to make my own changes and adjustments, but ...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 10, 2015 17:26*

I like the idea of 3D printing your own, however I would do like Gary suggested & check some big 3D printing sites first, as you may be able to use them as a base model & then make adjustments as you see fit to suit your own purposes/designs.


---
**Ashley M. Kirchner [Norym]** *December 10, 2015 18:51*

I'd also like to hear from folks who have replaced/upgraded their mounts, which is why I posted. So if you are one of those folks, please could you comment on them? What did you get, how are they working out for you, etc., etc ....


---
**Gary McKinnon** *December 10, 2015 22:49*

You'll have to keep bumping this Ashley, rubbish interface for posts so it will get lost ...


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/XHt9JKLZr7j) &mdash; content and formatting may not be reliable*
