---
layout: post
title: "The overwhelming saturation of endorphins flooding the reward mechanism in our brains is an experience I'm sure we are all familiar with"
date: December 23, 2016 17:24
category: "Discussion"
author: "Madyn3D CNC, LLC"
---
The overwhelming saturation of endorphins flooding the reward mechanism in our brains is an experience I'm sure we are all familiar with. 



For instance, taking a $400 POS Chinese "stamp machine" and turning it into a fully capable CNC laser cutter that can keep right up with a $15,000 epilogue, (ok, maybe a little exaggeration there) would be a great example of this endorphin saturation I speak of. 



It's just a great feeling. If you can identify with this statement, then you may be interested in this article. I don't think an explanation is necessary, once you read the article, the relevancy to K40 modders alike should come naturally. Happy Holidays everyone. Laser tubes are sold out everywhere, it's that time of year. 



[http://www.digikey.com/en/articles/techzone/2016/dec/get-on-the-fast-path-to-sensor-fusion-design?WT.z_sm_link=TZSNSR](http://www.digikey.com/en/articles/techzone/2016/dec/get-on-the-fast-path-to-sensor-fusion-design?WT.z_sm_link=TZSNSR)





**"Madyn3D CNC, LLC"**

---


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/LK7vYxJ3nHT) &mdash; content and formatting may not be reliable*
