---
layout: post
title: "Originally shared by Edward Morbius The Death of Google+ Is Tearing Its Die Hard Communities Apart | Steven T"
date: December 24, 2018 22:37
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
<b>Originally shared by Edward Morbius</b>



<b>The Death of Google+ Is Tearing Its Die Hard Communities Apart | Steven T. Wright</b>



<i>...For these Google+ enthusiasts, the process of digital relocation has become a very serious endeavor. Just ask John Lewis, the software developer who built a 3,500-member group on Google+ that’s dedicated to finding a replacement for these uprooted users, dubbed the “Mass Migration” community. A cursory scroll reveals intense discussions about what social media platform the users will flock to next, with debates that dwell on the relative strengths and weaknesses of individual platforms. Lewis and his raft of dedicated moderators have even organized Reddit-style AMAs for the CEOs of these sites to essentially pitch themselves to the skeptical community....</i>





[https://medium.com/s/love-hate/the-death-of-google-is-tearing-its-diehard-communities-apart-ad8332f4200d](https://medium.com/s/love-hate/the-death-of-google-is-tearing-its-diehard-communities-apart-ad8332f4200d)





**"HalfNormal"**

---
---
**James Rivera** *December 24, 2018 23:18*

Wow.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Jv5LZgiwYb3) &mdash; content and formatting may not be reliable*
