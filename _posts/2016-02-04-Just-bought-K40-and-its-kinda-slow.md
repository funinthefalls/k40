---
layout: post
title: "Just bought K40 and it's kinda slow"
date: February 04, 2016 09:48
category: "Hardware and Laser settings"
author: "Patryk Hebel"
---
Just bought K40 and it's kinda slow.


**Video content missing for image https://lh3.googleusercontent.com/-Mofp1SbjAmI/VrMeVvjL1sI/AAAAAAAAALI/qyZTsb0n2Bg/s0/20160203_211024.mp4.gif**
![images/50e76a9eda5812562eb450aed502475f.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/50e76a9eda5812562eb450aed502475f.gif)



**"Patryk Hebel"**

---
---
**Justin Mitchell** *February 04, 2016 10:07*

Well what speed did you tell it to do ?


---
**Patryk Hebel** *February 04, 2016 10:08*

**+Justin Mitchell** Well the software was kinda dumb, so when I set it for 5.00 mm/s it was faster but not fast enough.﻿


---
**Justin Mitchell** *February 04, 2016 10:43*

It looks like your doing engraving, for which i usually set either 400 or 500 mm/s.   it still takes forever, but the head should be moving pretty fast.  For vector/cutting work its usualyl in the 10-20mm/s range. going too fast on that makes it skip and mess up


---
**Patryk Hebel** *February 04, 2016 10:49*

**+Justin Mitchell** Yes but the speed on the video is set on 400mm/s and when I switched to 5.00mm/s it was a lot faster o.0



Maybe the software is crap.﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 04, 2016 11:00*

**+Patryk Hebel** I've heard of a lot of people having issues with speeds when they first get their K40 & it seems that in most cases it was something to do with the board model being incorrectly selected in the software. Check the model # on your board (where the USB cord plugs in) & make sure it is correct in the settings of your software.


---
**David Lancaster** *February 04, 2016 12:23*

Yeah, if you have a "close but not quite" board selected, it'll cut at very strange speeds, occasionally pause for no reason, and sometimes have "jumps" or "skips" in the cut.


---
**David Malpas** *February 05, 2016 10:17*

yes in properties, chnage the model to M2


---
**Patryk Hebel** *February 05, 2016 10:19*

**+David Malpas** I changed yestarday and everything works like charm :)


---
**Mike Maglaque** *February 11, 2016 13:03*

hmm same here, i changed mine to M2, but how come it engraves quite shallow, i tried to make a rubber stamp, does Amp meter

has to do with it?


---
**Patryk Hebel** *February 11, 2016 13:09*

**+Mike Maglaque** Depends on engraving speed I guess.


---
**Mike Maglaque** *February 11, 2016 13:11*

i did mine 320mm/s, should i make it higher or lower? anyone ever tried engraving on rubber sheet?


---
**Justin Mitchell** *February 11, 2016 13:15*

Yes, i've made rubber stamps using laserable rubber sheet. i tend to use 400mm/s at half power, it needs two or three passes to get a good depth, more power or slower speeds dont seem to improve things


---
**Mike Maglaque** *February 11, 2016 13:24*

uhm so may i ask? what are your current settings for making rubber stamp?


---
**Justin Mitchell** *February 11, 2016 14:06*

We have a 0-11 marking on our power adjustment pot, for rubber we use around 6 or 7 on that scale.  You can see all the 'best practice' settings we've found on this table: [http://swansea.hackspace.org.uk/Equipment/LaserCutter#settings](http://swansea.hackspace.org.uk/Equipment/LaserCutter#settings)


---
**Mike Maglaque** *February 11, 2016 15:14*

thanks for the info, gonna go trial and error and alota practice, thanks again :)


---
*Imported from [Google+](https://plus.google.com/102509166997588398801/posts/g7FaB92KFms) &mdash; content and formatting may not be reliable*
