---
layout: post
title: "Corellaser question #2. Yesterday I imported a file and it cut close to its original size"
date: April 03, 2016 01:58
category: "Software"
author: "giavonni palombo"
---
Corellaser question #2. Yesterday I imported a file and it cut close to its original size. Today now its cutting /engraving approximately  50%. When I import it corel is telling me that it is 1.75 inches x 2 but it not cutting /engraving to true size, I did nothing different than I did yesterday.  I have similar problems when I use laser drw3 as well. Any suggestions or tips?





**"giavonni palombo"**

---
---
**Phillip Conroy** *April 03, 2016 03:03*

When you go to cut screen select properties and select what version boad[ that is on its side and has the usb dongle plugged into it] you have,mine was set to m1 something and my versikn board is m2 something so i had to change the board selection to get speed varations to work


---
**giavonni palombo** *April 03, 2016 13:41*

I did change it the day before to m2 and I did check again yesterday and believe it still was on m2. Ill double check again. Is it something that needs changed once or each time before engraving?


---
**Phillip Conroy** *April 03, 2016 19:42*

Just once and save it i think ,been awile since i did change mime...


---
**Mishko Mishko** *April 04, 2016 10:00*

Did you check the model on your board? Is it Nano2 board? The model is printed on the board.

If nothing else helps, reinstall CorelLaser, I had the same problem, cutting 10x3mm instead of 10mm square, and it worked fine again after reinstalling it.


---
**giavonni palombo** *April 04, 2016 14:20*

Its a M2 nano board. Im going to mess with it this morning  and try to figure it out. If not I'll ask again after lunch.


---
**giavonni palombo** *April 05, 2016 01:45*

Well I tried all different settings, different file types it cuts 50% smaller than specified. It is also cutting 2 times on the cut line no matter what Ive tried. Help please 


---
**Mishko Mishko** *April 05, 2016 11:32*

Did you reinstall the plugin? No joy?


---
**giavonni palombo** *April 06, 2016 01:10*

I figured it out it was the dpi was wrong


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/dtJUSg9wBdB) &mdash; content and formatting may not be reliable*
