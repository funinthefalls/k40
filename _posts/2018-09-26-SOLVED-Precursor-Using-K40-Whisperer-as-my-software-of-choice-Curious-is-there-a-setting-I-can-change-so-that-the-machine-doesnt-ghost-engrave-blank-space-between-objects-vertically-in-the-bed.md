---
layout: post
title: "SOLVED Precursor: Using K40 Whisperer as my software of choice Curious, is there a setting I can change so that the machine doesn't \"ghost engrave\" blank space between objects (vertically in the bed)?"
date: September 26, 2018 22:41
category: "Software"
author: "'Akai' Coit"
---
<b>SOLVED</b>



Precursor: Using K40 Whisperer as my software of choice



Curious, is there a setting I can change so that the machine doesn't "ghost engrave" blank space between objects (vertically in the bed)? When it goes from a lower object to a higher object (from front to back in the machine) and there is white space between those objects, the machine goes back and forth in a really tight "wiggle" from the end of the bottom object to the beginning of the top object, then goes back to normal. This isn't a major issue for me, but I was just wondering if there was a way to get rid of that (in the long run, it can be time consuming when doing multiple jobs back to back). Again, not a game changer for me, but would like to "fix" if possible.





**"'Akai' Coit"**

---
---
**Scorch Works** *September 27, 2018 00:46*

K40 Whisperer will skip blank areas in an image without the "wiggles" if the image is clean.



My best guess is that there is a very small light feature in your image.  Sometimes when an image is edited there are artifacts at the image edges that are not visible under normal conditions but cause issues like what you are describing when engraving. 



The other thing that will keep K40 Whisperer from skipping blank areas is the use of JPG images in the SVG file.  If an image has ever been in the JPG format there is almost always a soft edge or halo of light pixels around the darker design.



A good way to test  for the artifacts is to tried to remove the white background of the image in an image editor like Gimp.  You select the background using the select by color function and you will see the areas that were not selected because the color is a bit different from the rest of the background.


---
**'Akai' Coit** *September 27, 2018 02:56*

So how does it work when the file is all set vectors made in the image in question? Most of the stuff I engrave is drawn up by me from scratch in Inkscape. I usually open a blank file to copy/paste the images into, arrange them as best I can to maximize space usage, then engrave and cut. I still get that same experience.


---
**Scorch Works** *September 27, 2018 04:36*

Can you send an example file?  It would be good to have an example to discuss in detail.  (My e-mail address is in the K40 Whisperer help menu.  I think you have used it before.) 


---
**'Akai' Coit** *October 04, 2018 06:52*

Sorry for taking so long to get back to you.. It's been a busy week. 



[drive.google.com - Example.svg](https://drive.google.com/file/d/1KbWoTUUJgXO7QZGGyQ0IE4TEsQYw6k33/view?usp=sharing)


---
**Scorch Works** *October 05, 2018 20:31*

I ran your file but I didn't get any really tight "wiggle" in the open space.  The only open space is between the "O" and "Oxygen" and it jumped that space as it should.  I am not sure why you are seeing something different.


---
**'Akai' Coit** *October 05, 2018 20:43*

Maybe there's a glitch with my copy of the software or my machine. I just got a hard drive in the mail to fix my stronger laptop, so I'll test that theory out soon.


---
**'Akai' Coit** *January 14, 2019 04:48*

Didn't realize this had gone unresponded to for so long. With the current version of the software (.27?) the issue seems to no longer be there. I am still keeping an eye on it, just in case.


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/QzaYR3nufz8) &mdash; content and formatting may not be reliable*
