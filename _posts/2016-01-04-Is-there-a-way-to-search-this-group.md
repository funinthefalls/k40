---
layout: post
title: "Is there a way to \"search\" this group?"
date: January 04, 2016 02:40
category: "Discussion"
author: "Sean Cherven"
---
Is there a way to "search" this group? Because I can't find what I'm looking for, even though I know it's been posted here before.



This is what I need to know:

What size tubing comes stock on the K40 for the water cooling? I know it's a metric size, but I can't remember what size it is..





**"Sean Cherven"**

---
---
**Gee Willikers** *January 04, 2016 02:54*

I used 5x8mm


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 03:20*

I am not sure if you are using the "new Google+" or on computer/mobile, however I am using the old style Google+ (because the new one is horrible in my opinion) & there is a "Search Community" textbox just below the picture of the K40 & just above the "All Posts" on the main page of the group.


---
**Scott Marshall** *January 04, 2016 03:42*

tubing is around 5/16" or 6mm ID. I used 5/16 barbs to extend it and they fit fine,  it's pretty forgiving. Jeff said 5mm fits, and I don't doubt it. It's silicone and pretty flexible.



This forum is quite different than most I belong too. I'm still not used to it either. More like an email correspondence site than a real forum. Best K40 guys anywhere though. Guess it's the people that make the site, not the framework. 






---
**Sean Cherven** *January 04, 2016 04:09*

I'm just not used to Google+ to begin with. And yeah I'm using the new layout, but I think I'm gonna switch back. I just downloaded the Google+ Android App to see if its any better.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 04:18*

**+Sean Cherven** I think the Android app looks like the new Google+ too (although I don't use my phone for it's smartphone capabilities haha). As far as water hose goes, I just purchased some a week or so back. I am pretty sure it was 6mm I.D. & 8mm O.D. for the stuff I used. I found however that the hose that joins the laser tube is soft & somehow squeeze over the glass tube bits. I didn't want to mess with that & maybe accidentally break it, so I cut the hoses about 4-5inches from the glass & joined my new hose to there with joiners/silicon.


---
**Gee Willikers** *January 04, 2016 07:43*

Fwiw the 5mm is tight, I do wish it was slightly larger.


---
**Sean Cherven** *January 04, 2016 13:18*

So 6mm fits on the tube itself? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 15:32*

**+Sean Cherven**  I couldn't get 6mm (inner diameter) to fit onto the little glass bits sticking out of the tube. I left about 3-4 inches of the original piping hanging off the end & attached onto that.


---
**Sean Cherven** *January 04, 2016 15:37*

Well thats the problem, i had to trash my stock tubing and start all over, but I still wanna go with the same type of tubing, and I forgot the measure the tubing before I threw it out.



This is the tubing that LightObject says should fit most laser tubes:  [http://www.lightobject.com/8X12-Silicon-Flex-Tube-for-CO2-Water-Cooling-P752.aspx](http://www.lightobject.com/8X12-Silicon-Flex-Tube-for-CO2-Water-Cooling-P752.aspx)



Do anyone know if that will fit well without using clamps??


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/jAsqX25V6Dq) &mdash; content and formatting may not be reliable*
