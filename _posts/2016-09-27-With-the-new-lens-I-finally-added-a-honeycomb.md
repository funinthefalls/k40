---
layout: post
title: "With the new lens I finally added a honeycomb"
date: September 27, 2016 19:35
category: "Modification"
author: "Jeff Johnson"
---
With the new lens I finally added a honeycomb. This is a .5 inch pattern that's .5" thick. It's resting on a steel welding mesh that's covered with an aluminum screen mesh to catch the tiny parts I'm often cutting. There is no noticeable marking from the laser reflecting off of the mesh. I created a few sets of standoffs from 5mm plywood in varying heights. This lets me set the focus for common materials that I cut, specifically 5mm, 3mm and paper. I just picked up a steel right angle ruler that I'll install as a guide to allow easy placement at 0,0. This will also make it easier to turn Tie Fighter panels over to texture both sides. Finally, I stalled a strip of LEDs around the interior. It's pretty bright in there!

![images/9c1e347150c949bc73550c5bb3e27fd4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9c1e347150c949bc73550c5bb3e27fd4.jpeg)



**"Jeff Johnson"**

---
---
**Anderson Firmino** *October 01, 2016 23:09*

Whats the difference in this design?


---
**Jeff Johnson** *October 01, 2016 23:25*

When I bought it this is what it looked like.  No lights,  no air assist and a bed designed to hold rubber stamps.

![images/c9f41a25e296cd30a76f78b74d5c36d1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c9f41a25e296cd30a76f78b74d5c36d1.jpeg)


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/Kj4AKr11QVH) &mdash; content and formatting may not be reliable*
