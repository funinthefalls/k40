---
layout: post
title: "Made this for the out going Master of my Masonic Lodge"
date: December 09, 2016 01:28
category: "Object produced with laser"
author: "Ned Hill"
---
Made this for the out going Master of my Masonic Lodge. (The Master is basically the president of the Lodge, typically serves a one year term and then become a Past Master).  Made from 1/4" alder.  I'm really happy with how the engravings came out. The banner element at the top has a lot more fine line detail then the other image so I engraved it at a lower power setting then the rest.  Some of the line elements were so close that they got slightly over engraved by subsequent passes, but this actually gave it a bit of a cool carved 3d look.  Still have to clear coat it so the alder will darken a bit.



![images/c96f70d333155df1154f6c76bf30088d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c96f70d333155df1154f6c76bf30088d.png)
![images/4f6045d40edafeb160484d77e0f22651.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4f6045d40edafeb160484d77e0f22651.png)
![images/2b6c530c405ef4d94407306ca14ed452.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2b6c530c405ef4d94407306ca14ed452.png)

**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 09, 2016 08:00*

That looks great. Would love to see after you clear coat it too.


---
**greg greene** *December 09, 2016 13:39*

what software did you use?


---
**Ned Hill** *December 09, 2016 13:42*

Still using CorelDraw X8 with the laser plugin until my Cohesion3D ships.


---
**greg greene** *December 09, 2016 13:49*

Great work Ned !


---
**Ned Hill** *December 10, 2016 00:53*

**+Yuusuf Sallahuddin** Here's the piece after clear coating.  

![images/ed1365d8cdb752364976318036770e26.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ed1365d8cdb752364976318036770e26.png)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 10, 2016 05:42*

**+Ned Hill** Oh yeah, it is slightly darker. Brings out the grains in the wood nicely. Looks good :)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/8Cs9dqd6143) &mdash; content and formatting may not be reliable*
