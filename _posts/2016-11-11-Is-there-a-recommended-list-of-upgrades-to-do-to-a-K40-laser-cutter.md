---
layout: post
title: "Is there a recommended list of upgrades to do to a K40 laser cutter?"
date: November 11, 2016 18:54
category: "Discussion"
author: "Shane Graber"
---
Is there a recommended list of upgrades to do to a K40 laser cutter?  Winter is Coming and I'll have some time on my hands to finally do the upgrades to mine...





**"Shane Graber"**

---
---
**Ariel Yahni (UniKpty)** *November 11, 2016 19:41*

In order of preference: smoothie, air assist, exhaust, lense, mirrors, Z table


---
**Shane Graber** *November 11, 2016 19:59*

Do you have recommended links for each of these updates? 


---
**Ariel Yahni (UniKpty)** *November 11, 2016 20:37*

Couple of link in this collection [https://plus.google.com/collection/EndMTB](https://plus.google.com/collection/EndMTB)


---
*Imported from [Google+](https://plus.google.com/+ShaneGraber/posts/jZuSvDrfgj3) &mdash; content and formatting may not be reliable*
