---
layout: post
title: "So I recently got a k40 laser cutter off of ebay and notice it is a lot different then the ones people are using on here"
date: November 11, 2016 18:48
category: "Smoothieboard Modification"
author: "Kelly S"
---
So I recently got a k40 laser cutter off of ebay and notice it is a lot different then the ones people are using on here.   I am interested in using a smoothie board, however my cables are very much different.  I have one ribbon cable coming in for a limit switch and the stepper motor on the left, and a normal cord for the stepper motor at the bottom.  Will that create an issue when getting the board?  Also Should I order an unpopulated board? 





**"Kelly S"**

---
---
**Kelly S** *November 11, 2016 18:49*

This is how the board looks on my unit.  Uses coreldraw.

![images/4f8684050ed080223869467a9d84f419.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f8684050ed080223869467a9d84f419.jpeg)


---
**Ariel Yahni (UniKpty)** *November 11, 2016 19:04*

You have one of the two fairly new board types. As you see this uses a ribbon so to use Smoothie you would need to splice that with what is called a middleman board. I would recommend you talk to **+Scott Marshall**​ as he sells a kit that will take that and give you a easy connection to a smoothieboard that he also sells.  This is the easiest path available.


---
**Jim Hatch** *November 11, 2016 23:56*

Looks like mine (it's a Nano board). No worries upgrading to Smoothie. 


---
**Kelly S** *November 12, 2016 22:16*

ks for the feedback all, will look into a middle man board.  Plan to get an updated nozzle and new lens for said nozzle soon.  Then I will be looking into doing the smoothie upgrade.


---
**Kelly S** *November 13, 2016 02:36*

**+Ray Kholodovsky** That is very interesting, is it available for purchase or testing?  :)




---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2016 02:38*

**+Kelly S** yes of course.  I am hand assembling the first set of PCBs. You can see some more pics of the conversion here:[https://plus.google.com/u/0/+RayKholodovsky/posts/UrNpWddMZok](https://plus.google.com/u/0/+RayKholodovsky/posts/UrNpWddMZok)

Please let me know if you are interested.


---
**Kelly S** *November 13, 2016 04:05*

I am definitely interested, will it come with some simple instruction to sending code to it as well?  ;) And Still looking at 2 weeks delivery time?  (I am located in the states). 


---
**Kelly S** *November 13, 2016 16:43*

Also may be interested in 2 actually.  **+Ray Kholodovsky**


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/cH6KvyuK1Hf) &mdash; content and formatting may not be reliable*
