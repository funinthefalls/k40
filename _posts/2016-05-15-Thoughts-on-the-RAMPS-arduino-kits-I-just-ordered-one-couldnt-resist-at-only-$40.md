---
layout: post
title: "Thoughts on the RAMPS arduino kits? I just ordered one, couldn't resist at only $40"
date: May 15, 2016 00:34
category: "Modification"
author: "Eric Rihm"
---
Thoughts on the RAMPS arduino kits? I just ordered one, couldn't resist at only $40. I was curious as how they compare to smoothie boards. The install seems fairly easy, I ordered the middleman board and will have some extras I'll give away to the group.

![images/8bf747ecfc9b25494468a2a3abced9bb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8bf747ecfc9b25494468a2a3abced9bb.jpeg)



**"Eric Rihm"**

---
---
**Eric Rihm** *May 15, 2016 04:27*

Hmm maybe I'll cancel and switch I thought smoothie boards were $150


---
**Francis Lee** *May 15, 2016 04:28*

I got a similar kit for $35 with Prime shipping. It works well so far, and wicked easy to install the firmware via Arduino IDE.


---
**Eric Rihm** *May 15, 2016 04:35*

Hmm are there any US resellers of the azsmz I liked being able to get the ramp quickly. I'd like to use LaserWeb so I might ditch the ramp board


---
**John-Paul Hopman** *May 15, 2016 19:45*

I have been toying with the idea of a laser cutter and having a use for my old RAMPS board. In that case, I am okay with 8-bit versus 32-bit.



Obviously I would buy 32-bit if getting something new, but as laser cutters essentially only have two axis instead of the three on a 3D printer and don't have to do anything as complex as controlling a delta printer, why wouldn't an 8-bit controller be perfectly acceptable?


---
**Greg Curtis (pSyONiDe)** *May 15, 2016 22:24*

So much info packed in this thread already, I can't wait to see how it pans out, as I really didn't want to drop the $ on the LightObject controllers. I'm glad there are options out there.


---
**Anthony Bolgar** *May 16, 2016 00:20*

I used Ramps for a while., although it is better than the stock board, Smoothie is the way to go!


---
**Vince Lee** *May 16, 2016 02:04*

I think I paid just over $80 shipped for an azsmz mini with screen and 4 drivers (of which I only really needed 2).  I can't answer what limitations an 8 bit board may have never having used one for a laser, bit the extra address space might be useful for buffering when engraving raster images.


---
**Alex Krause** *May 16, 2016 04:29*

If you are not wanting to go the cheap chinese import method but still want a quality smoothie based board the azteeg boards from [www.panucatt.com](http://www.panucatt.com) are somewhere in between smoothie and azsms in price point


---
**Anthony Bolgar** *May 16, 2016 05:21*

I am using an MKS SBASE board, only because all the reall smoothies were out of stock. I need a second one, which will be a true smoothie, especially since I have strted working on the smoothie project with Arthur. As well as helping out Peter with LaseWeb wherever I can. I want to support all the open source projects properly, they work very hard for very little return on their investment of time and money. Alos, by purchasing an authentic board you will have great access to tech support right from the developers themselves.


---
**Alex Hodge** *May 19, 2016 14:05*

**+Anthony Bolgar** I am using the same board. Also because I couldn't get my hands on a real smoothieboard. Just think how many they would sell if they ramped up production and put them up on amazon!


---
**Arthur Wolf** *May 19, 2016 14:38*

We'll be back in stock in a week or two.


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/3UiGpwZGzE5) &mdash; content and formatting may not be reliable*
