---
layout: post
title: "So we have a modified K40 with a beta release of the Cohesion 3d mini board and glcd at RCL"
date: January 24, 2017 06:12
category: "Smoothieboard Modification"
author: "Ryan Branch"
---
So we have a modified K40 with a beta release of the Cohesion 3d mini board and glcd at RCL. I tried doing a test cut with various power and speed settings but I ran into a problem that I couldn't get the laser to fire with anything lower than 20%. It's not that it just wasn't cutting or etching, but the controller wasn't sending any current to the laser until I set it to 30% power. I checked the gcode, and it was sending the correct s values to the laser, so I am thinking that its a setting in the config.txt or through the glcd. 



Does anyone know what setting might be the cause? From the top of my head in the config.txt we have the min power set to 0.0 the max power set to 0.8 and the default power set to 0.2. On the glcd the laser pwer scale was set to 100% and the s value was at 20%. 



Also for future reference, what are those two settings on the glcd? I couldn't find any documentation on them and how they effect any other settings. I know the s value is a power setting, but does that affect the gcode, or just the test fire through the lcd, or is that the default power? Do any of these settings effect the 0.8 or 80% maximum power I have set in smoothie? **+Ray Kholodovsky**​ 





**"Ryan Branch"**

---
---
**Alex Krause** *January 24, 2017 06:14*

Are you running with the potentiometer removed?


---
**Ryan Branch** *January 24, 2017 09:06*

Yeah, I didn't do the initial wiring but I believe it's wired almost the same as in this tutorial. [https://cohesion3d.freshdesk.com/support/solutions/articles/5000721345-carl-s-guide-to-installing-the-c3d-mini-into-a-k40-laser-with-ribbon-cable-](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721345-carl-s-guide-to-installing-the-c3d-mini-into-a-k40-laser-with-ribbon-cable-)  I thought it might be the case with the center pin just floating, but I don't know enough about the settings on the lcd or smoothie, so I was going to check that first. Would you recommend pulling the wire for the center pin high, or just wire the pot back in and fine tune it?﻿


---
**Don Kleinschnitz Jr.** *January 24, 2017 13:46*

**+Ryan Branch** if you pull the center pin of the pot up to 5v the laser will be able to fire up to full power when you "Laser Switch" and issue PWM. Although that may be good for a quick test that is not a good idea for longer term.

I would just put the pot back in, you will need it. Is your PWM wired to the "IN" or the "L" pin?

After putting the pot back in push "Laser Switch" at various positions of the pot to insure that the laser is working. 

Then ground the L pin and verify it fires.

Then use the "L" wiring and setup the config properly.




---
**Joe Spanier** *January 24, 2017 16:38*

I did the initial wiring on the laser. We pulled the pot for two reasons. 



1. the pot is bad. It's pretty variable on how much power you get from it.

2. Ray asked me too  



I can't remember if we did "L" or "IN" for pwm. I could probably look back in my pictures though. 



I've pretty normal that the laser doesn't trigger below 20%. I've never seen a tube yet that would trigger a stable arc that low. 



Also **+Ryan Branch**​ there's a **+Cohesion3D**​ group specifically for support on that board that Ray monitors much more heavily. 






---
**Don Kleinschnitz Jr.** *January 24, 2017 16:47*

These pots go bad and mine just did. I am replacing with this pot and **+Kim Stroman** already did... 

[https://www.amazon.com/gp/product/B00VG93UD8/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00VG93UD8/ref=oh_aui_search_detailpage?ie=UTF8&psc=1). 



Best I can tell these ionize around 4ma. So if the tube put out 24ma that would be marginal at 4.8ma.



Also if you are connected to "IN" there is a poor correlation between the controllers PWM value and what the LPS power is actually set to.


---
**Bill Keeter** *January 24, 2017 16:52*

**+Joe Spanier** Yeah, I have the new Cohesion board myself. I was having engraving problems. Last night I changed from the 3 wire PMW setup to the L wire running to Pin 2.5 (bed) on the PCB. It fixed my engraving issues. I also discovered my laser doesn't fire on fine details if engraving at 20% power in Laserweb. It was suggested that I change the config file's default Laser Min power to something just above 0. Which would help the laser to fire at lower values. Something like 0.05 or 0.08. Just note I haven't tested this yet (will tonight)


---
**Joe Spanier** *January 24, 2017 16:53*

Im 95% sure its connected to L from what I can see in my build pictures. Im not near the laser now. 



What is gained/missed from having or not having the pot. I liked having it for on the fly fine tuning but at the same time, this is in a makerspace so its nice to have the software limiting out the tube output.


---
**Don Kleinschnitz Jr.** *January 24, 2017 17:06*

**+Joe Spanier** here is the whole novel on the subject. The short story is that the laser is a consumable and wears out. So unless you like constantly adjusting software settings and config files you need a 'intensity" control.

Also there are many other variable factors from the software's pwm to the actual power that need tweaking and if you connect to L all that tweaking can be done with that pot.



[http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)


---
**Pablo Verity** *January 24, 2017 17:51*

I have problems with the stock board not firing at anything below 20%... you're not on your own. See other posts.


---
**Joe Spanier** *January 24, 2017 18:00*

**+Don Kleinschnitz** so how is all that handled on DSP based machines and machines that never had a pot to begin with. Ive always just adjusted power levels based on results. I know enough to know its never going to be a solid number.


---
**Don Kleinschnitz Jr.** *January 24, 2017 18:32*

**+Joe Spanier** Its hard to answer that question without knowing the specific design of the machine, below here are my suspicions.

What pot-less and DSP machines are you thinking of.



..There are controls similar to the pot in a machines laser control (not teh DSP) software. Maybe an adjustment pot somewhere used during alignment but not on the panel.

..Ignore the problem with laser wear-out. I have found that many design challenges, in cheaply designed machines, are just ignored and we do a lot of tweaking. Machine dies over time

...Some cheap machines do not print grey scale they just dither the image so the intensity is not as critical. The standard K40 does this.

....They assume you will keep changing the parameters of the job to account for machine changes.

.... The software uses an image power DF to image its grey-scale range ignoring actual power levels (today we tweak max power). It then adds to that value an intensity bias before it sends it to the LPS as an adjusted PWM. In this case the software would have a pot object :).



We have hacked the K40, it was not really designed to print grey shade, we are mod-ing the power control to do that. 



If I did this from scratch I would have digitally controlled the LPS rather than PWM controlling the LPS PWM control, which is what we are doing. The software would have a PWM setting and an intensity pot :).



And ........ I could be wrong :)


---
**Joe Spanier** *January 24, 2017 18:53*

Everything you typed basically fits my assumptions and experience. And I'm often wrong hehe. 



I do know that when I removed the Pot the machine became much more consistent. But like you said, those pots suck.


---
**Ryan Branch** *January 24, 2017 19:27*

**+Bill Keeter**​​ Let me know how that goes. I am curious if that will do anything.﻿


---
**Ryan Branch** *January 24, 2017 19:28*

BTW, does anyone have any info on those GLCD settings? "Power scale" and "s value"?


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 21:28*

Hey Ryan (and Joe).

This is a tricky one.  Maybe you should put the pot back into play and try running PWM to L.   Read up the Cohesion3D Group: [Cohesion3D](https://plus.google.com/u/0/communities/116261877707124667493)



There's a bunch of people with whom keeping the pot and pulsing L seems to work better.  

Hook it up to bed mosfet pin 2.5, change the laser pin in config from 2.4! to 2.5, I've covered this in 10 separate threads in the group. 



And yeah you've got an early board in there so don't take any board specific advice from anyone besides Carl or myself :)  It should be fairly transferable though, I didn't change the board <b>that</b> much between revisions. :)




---
**Wolfmanjm** *January 24, 2017 22:33*

FWIW if you are using the L pin for PWM I had to set laser_module_pwm_period 200 before I would get any noticeable affect from Sxxx


---
**Ryan Branch** *January 24, 2017 23:58*

So I tried hooking the pot back up and then ran L to the bed like you said. I changed the config file and reset the board and made sure it was saved as well. Unfortunately I wasn't able to get the laser to fire at all after that. No matter what I had the potentiometer set at. I put it back the way it was and it still only fires at 30%. Another thing to note is that I ran some gcode that had different power levels throughout. 21%, then 22%, 23%, 24% etc. And it is exactly at 30% that the laser will fire (current meter doesn't even budge until then). That's why I was thinking that it was a firmware issue. I may try changing the minimum power in the config to see if that does anything.


---
**Ryan Branch** *January 25, 2017 00:16*

Good news! Changing the "laser minimum power" to 0.1 from 0.0 in the config seems to have solved it! I have it hooked back up the way it was originally as well without the pot. Not 100% sure that I won't run into another problem, but I will keep a closer eye on the cohesion 3d g+ group. Thanks!


---
**Joe Spanier** *January 25, 2017 00:28*

Interesting. .1 should be a fine min number. Good to know!


---
**Ryan Branch** *January 25, 2017 07:22*

Well, I ran into amother problem, but I will take it to the Cohesion 3d g+ group


---
**Robin Sieders** *January 25, 2017 20:36*

What issue are you running into now **+Ryan Branch**? I just made the change to my config, and can confirm that I'm getting better results with engravings now as well. I'm going to test adding the pot back in again, with it connected I seemed to be able to get back up to 250mm/s engraving speeds without the stutter.


---
**Ryan Branch** *January 27, 2017 10:03*

Sorry for the late reply **+Robin Sieders**​​​ I moved the conversation here. [https://plus.google.com/101842495658501740623/posts/3D3RksJ5ZQa](https://plus.google.com/101842495658501740623/posts/3D3RksJ5ZQa)



I am curious about your setup though and what your settings are in smoothie! I don't think we have done any raster engravings that fast yet.


---
**Wolfmanjm** *January 27, 2017 19:21*

**+Ryan Branch** I cannot answer in that forum for some reason. First K40 are definitely not linear in any shape or form. Do not expect that the darkness will be proportional to the PWM sent. Second did you set the PWM period to a reasonable value? I find 200 seems to work better than smaller values.

laser_module_pwm_period 200


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 19:26*

**+Wolfmanjm** Not sure why you can't answer in the Cohesion3D Group, I checked and you are definitely a member.  Feel free to put up a test post or comment on the hardware FAQ thread there to test.


---
**Don Kleinschnitz Jr.** *January 27, 2017 21:51*

**+Wolfmanjm** when you say that the K40 vs PWM is not linear do mean because of all the physical variables (wood etc) or is the non-linearity in how the PWM is set in the firmware.

Btw here is some theoretical validation on the 200us setting you suggest: 

[http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html](http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html)


---
**Robin Sieders** *January 27, 2017 22:01*

**+Ryan Branch** I finally got around to testing with the various things connected/disconnected. I was mistaken in thinking I had hit 250 mm/s since upgrading to the cohesion board. My max seems to be 100 mm/s @ 5 to 25 % power if I want to keep lighter details. I don't get very good variation when I have the pot connected, the dark areas seems to engrave at whatever power I have the pot set to, no matter what settings I enter in Laserweb, so I removed the pot from the equation again. I'll upload a picture of my tests so far, when I get home, to show what I'm talking about. Going to test out adjusting the PWM period as **+Wolfmanjm** suggested as well to see what difference that makes. Overall I'm pretty happy with the results I'm getting right now though.


---
**Wolfmanjm** *January 27, 2017 22:09*

**+Don Kleinschnitz** afaik the shade is not proportional to the pwm. the pwm is linear,  but shade is not proportional to the pwm  i see this also in led based lasers although they seem far more proportional than the co2 lasers. also i got the 200us period from your work :) before that I got no power variance at all, it was either full on or full off.


---
**Don Kleinschnitz Jr.** *January 27, 2017 22:22*

**+Wolfmanjm** your saying that the non-linearity is in the laser power supply or laser ionization characteristics or both. Also how materials burn is not linear. Makes sense ....

BTW connecting to 'IN"with a level shifter creates another non-linearity" ......couldn't resist.


---
*Imported from [Google+](https://plus.google.com/101842495658501740623/posts/dE4k3aAVhkn) &mdash; content and formatting may not be reliable*
