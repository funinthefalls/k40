---
layout: post
title: "Anyone with a power probe tell me what sort of loss you're getting from your mirrors?"
date: August 09, 2016 04:22
category: "Hardware and Laser settings"
author: "I Laser"
---
Anyone with a power probe tell me what sort of loss you're getting from your mirrors?



Tests I've done have pretty consistently shown a loss around 10-12% per mirror. 



For instance, first number is directly after tube, second is before head (after two mirrors).



10mA - 19w / 15w

15mA - 24w / 18.5w





**"I Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 09, 2016 05:47*

Isn't 19w down to 15w only a 78.95%? Thus a loss of 21.05%?

Also, 24w down to 18.5w is 77.08%, thus a loss of 22.92%.



Unless my math is wrong or I am misunderstanding your measurements.



edit: Oh, nevermind me... I just realised after 2 mirrors... so approximately 10-11% each. Although I'd imagine the loss will be exponential the more mirrors it passes through?


---
**I Laser** *August 09, 2016 07:41*

Yes it's probably not linear, I'm just a bit surprised the loss is that high to be honest. Wondered if that was acceptable or whether my homemade hard drive mirrors aren't as good as the real thing!


---
**Phillip Conroy** *August 09, 2016 11:02*

I will test mine in morning and post


---
**I Laser** *August 09, 2016 11:06*

Thanks :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 09, 2016 11:18*

**+I Laser** That could be the case. It would be interesting if the loss on your hard-drive platter mirrors is less than the stock or upgraded mirrors.


---
**Ari Diacou** *August 09, 2016 13:51*

The laser operates at 10.6um, so this graph says that a 4-7% loss is to be expected for a thin-film silver mirror:

[https://www.thorlabs.com/images/TabImages/Protected_Silver_Reflectance_45deg_800.gif](https://www.thorlabs.com/images/TabImages/Protected_Silver_Reflectance_45deg_800.gif)


---
**Paul Hayler** *August 09, 2016 21:40*

You may be interested in this guys youtube channel he has proven that home made polished copper mirrors are the beat way forward with the least amount of power loss.  I will also add that his continued testing I have found invaluable covering all things Chinese laser related so he is worth bookmarking.  
{% include youtubePlayer.html id="kJe16YQlIM0" %}
[https://www.youtube.com/watch?v=kJe16YQlIM0](https://www.youtube.com/watch?v=kJe16YQlIM0)


---
**I Laser** *August 10, 2016 00:30*

Thanks all, very interesting!



Especially **+Paul Hayler**  link(s). Having watched two of RDWorks videos now they are definitely worth watching.



Russ (RDWorks) demonstrated aluminium, which hdd platters contain (and most beds are made from), aren't the most effective choice of mediums though still reflect nearly 80%!



Also **+Ari Diacou** graph shows my loss is much higher than it should be.



So it seems the HDD mirrors are robbing me of power. Given I have hole saws now, I shall be sourcing some copper to make a few mirrors.



I have a few projects on the go, but hope to update soon.


---
**Paul Hayler** *August 10, 2016 13:47*

I'm in the process of making my copper mirrors now!


---
**I Laser** *August 11, 2016 00:34*

What thickness are you using? 



I've looked at a few local suppliers, around  1mm thick seems about standard but I'm concerned it's not thick/hard enough and it wont provide a perfectly flat surface. :|


---
**Paul Hayler** *August 11, 2016 22:31*

Russ (rdworks) shows in one of his videos how he hones them flat and then gets a mirror polish on them.  I do a slight variation on this as I didn't have a chunk of steel but it gets the job done.  its also worth noting once you have them polished up if they are not quite flat you would notice straight away by the reflection showing distortion.


---
**I Laser** *August 12, 2016 01:50*

Ah, to be honest I'd only watched videos 1 & 2, must have missed him talking about making them at the end of two.



Another thing to consider possibly, if copper oxidises would it be better to make them out of something else? Might not be as efficient but you wouldn't have to pull them out and polish them every couple of months and totally realign the machine.


---
**Paul Hayler** *August 12, 2016 21:41*

mmmm I wonder if there is a way of slowing down the oxidisation process.....a bit of research I think...


---
**I Laser** *August 13, 2016 01:37*

A couple of videos further on in the comments he mentions he's had his mirrors in for 5+ months and they haven't oxidised.



Russ thinks it's the polish he's using. If it's only a matter of polishing them every 6 months I can handle that!


---
**Paul Hayler** *August 13, 2016 11:44*

I guess its off to the shops then...cant wait to see what kind of results I get...


---
**Phillip Conroy** *August 14, 2016 02:32*

Finaly got around to testing  power output,straight out of tube at 18ma was 38 watts [meter is calabrated to 54 seconds-etrched on back] 54 seconds laser on and 2mintutes rest,with power probe insulated from all metal[which would act like a heatsink and result in a lower reading] as well as probe at 90 to beam.After the first mirror[which is the silver type and new] power output was 38 watts .After 2nd mirror[used but freshly cleaned] 37 watts .After 3rd mirror power 36 watts[again used mirror ,freshly cleaned.all reading taken in an air conditioned room at 22 degrese.water temp at start of tests was 14 degreas,and finish 16 degrease .

These readings are what i would have expected and tube was a replacment with approx 300hours on it.


---
**Phillip Conroy** *August 14, 2016 02:38*

Forgot to add had just finished full alignment and end spot was almost perfect[best i have ever got it]


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 14, 2016 02:38*

**+Phillip Conroy** Those seem like pretty good results. Minimal loss through the silver mirrors.


---
**I Laser** *August 14, 2016 03:02*

That is an amazing result **+Phillip Conroy**  I'll be happy if I can get it down to a loss of 5-10% across the lot but it seems a bit of a dream at the moment lol.



After watching a lot more of Russ's videos I wondered whether you could even grind down the standard mo mirrors and polish them back up (all of mine were scratched when they arrived). I assume they have a coating on them though.


---
**Phillip Conroy** *August 14, 2016 03:33*

Also replacement power suppy with built in fan ond ,when i get a chance next time i lots of cutting to do will measure power at diffrent temps,spent last 2 days cleaning/aligning/modifying laser cutter,air compressor and air dryer


---
**Phillip Conroy** *August 14, 2016 05:22*

i can now cut 6mm mfd at 18ma and 4mm/second


---
**Phillip Conroy** *August 15, 2016 09:11*

I haad a look at this guy here 
{% include youtubePlayer.html id="kJe16YQlIM0" %}
[https://www.youtube.com/watch?v=kJe16YQlIM0](https://www.youtube.com/watch?v=kJe16YQlIM0)  and he made a huge mistake when measuring last mirror power ,if you watch it you wil see in mirrors 1 and 2 he was holding power probe at the dial end which is ok ,however when he measured the last mirror he rested the power probe on a lump of steel,which would have acted like a heatsink drawing heat away from the dial,resulting in lower power readings,i made this mistake myself do i know how much this trows the readings out


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/JfD6weVMem2) &mdash; content and formatting may not be reliable*
