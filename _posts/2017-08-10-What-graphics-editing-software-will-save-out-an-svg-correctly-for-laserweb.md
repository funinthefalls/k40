---
layout: post
title: "What graphics editing software will save out an svg correctly for laserweb?"
date: August 10, 2017 18:35
category: "Discussion"
author: "William Kearns"
---
What graphics editing software will save out an svg correctly for laserweb? Illustrator even saved at 72 dpi it's off for me?   





**"William Kearns"**

---
---
**Ariel Yahni (UniKpty)** *August 10, 2017 18:45*

Really AI is off ? I assume you setup that on settings? 


---
**William Kearns** *August 10, 2017 18:46*

When it saves out? Should I set the file up like that from the beginning? Before I start drawing?


---
**Ariel Yahni (UniKpty)** *August 10, 2017 18:58*

No, in LaserWeb settings there an option to set it to 72. AI default export is 72. Can you give me an example of how it does not match? I use AI all the time.

Are you running latest version of Laserweb


---
**Ashley M. Kirchner [Norym]** *August 10, 2017 19:33*

Strictly AI just here, for all vector work. Never had an issue. Export, import, set operations, generate gcode and send. 


---
**William Kearns** *August 10, 2017 23:18*

**+Ariel Yahni** where is the setting for 72dpi in LW4


---
**Ariel Yahni (UniKpty)** *August 10, 2017 23:31*

Where it says PX per inch

![images/97da003c040346fdd9e0e779a9912606.png](https://gitlab.com/funinthefalls/k40/raw/master/images/97da003c040346fdd9e0e779a9912606.png)


---
**Ashley M. Kirchner [Norym]** *August 10, 2017 23:31*

I recommend you spend a few minutes looking at every tab, every settings, and learning about the software. It's under Settings -> File Settings


---
**Anthony Bolgar** *August 11, 2017 00:58*

I have started to work on the documentation again for LaserWeb. The official docs will now be on the LaserWeb4 wiki. Should be a little better documented in about 2-3 weeks.


---
**William Kearns** *August 11, 2017 02:53*

That's done the trick change that to 72 in lw4


---
**Ariel Yahni (UniKpty)** *August 11, 2017 03:04*

**+William Kearns**​ glad it's solved


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/iwZ3bFpayfs) &mdash; content and formatting may not be reliable*
