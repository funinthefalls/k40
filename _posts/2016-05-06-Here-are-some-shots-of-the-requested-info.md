---
layout: post
title: "Here are some shots of the requested info"
date: May 06, 2016 14:53
category: "Hardware and Laser settings"
author: "Custom Creations"
---
Here are some shots of the requested info. **+Anthony Bolgar** **+Ariel Yahni** 



![images/0a4da3a699534a66329fc7aff5024728.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a4da3a699534a66329fc7aff5024728.jpeg)
![images/5cea57f774f8b3729efe80750a0b711d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5cea57f774f8b3729efe80750a0b711d.jpeg)

**"Custom Creations"**

---
---
**Ariel Yahni (UniKpty)** *May 06, 2016 14:54*

I can't read the port number on the terminal. It must match the dropdown on LW. It is reading the arduino so serial seems to be ok


---
**Custom Creations** *May 06, 2016 14:58*

**+Ariel Yahni** it says COM5 just like LW does but it says Firmware Detected: Undefined, Port ID No: 0


---
**Ariel Yahni (UniKpty)** *May 06, 2016 15:03*

Have you used this board before? What firmware does it have? 


---
**Custom Creations** *May 06, 2016 15:07*

**+Ariel Yahni** I am currently using it with Inkscape so I know it works. It is Marlin FW.


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/7BWK8m5wJiA) &mdash; content and formatting may not be reliable*
