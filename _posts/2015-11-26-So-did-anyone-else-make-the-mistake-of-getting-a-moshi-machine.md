---
layout: post
title: "So did anyone else make the mistake of getting a moshi machine?"
date: November 26, 2015 22:56
category: "Software"
author: "Nathaniel Swartz"
---
So did anyone else make the mistake of getting a moshi machine?  If so does anyone have instructions on using the moshiLaser corel plugin?  I can engrave and cut just fine in moshi, but it's a pain.  Also I do plan on upgrade to ramps or smoothie at some point but for now right I'm going to continue to trudge through moshi land.





**"Nathaniel Swartz"**

---
---
**Stephane Buisson** *November 27, 2015 00:01*

getting a Moshi machine isn't a mistake if you intend to upgrade the board, those are the cheapest and regularly discounted ;-))

You understand why I am not discussing Laserdraw, I don't have it ... nor Corel (I am a Mac Os user)



Maybe one day on ebay you will be able to choose between those options

-no board (personal install, open source software, visicut,...)

-Moshi (moshidraw)

-Nano (Laserdraw)

-DSP



;-))


---
**Tony Schelts** *November 27, 2015 09:36*

Stephane,  Have you upgraded your K40 and how much did it cost and how difficult was it to do.  I no nothing about electronics???


---
**Stephane Buisson** *November 27, 2015 10:36*

**+Tony Schelts** all in the document  named "	K40 Modifications & improvements"  (top right corner)

[dl.dropboxusercontent.com/u/65700374/K40%20Modification%20improvements.pdf](http://dl.dropboxusercontent.com/u/65700374/K40%20Modification%20improvements.pdf)


---
**Gary McKinnon** *November 27, 2015 11:30*

**+Nathaniel Swartz** I design in Sketchup, export to PDF then import it to Corel for cutting via the CorelLaser plugin. I've not seen Moshidraw but if it is a predecessor of Laserdraw then this may help : [http://www.daycounter.com/Articles/LaserDrw/LaserDrw-Tutorial.phtml](http://www.daycounter.com/Articles/LaserDrw/LaserDrw-Tutorial.phtml)


---
**Kelly Burns** *February 08, 2016 23:14*

I'm curious if you had better luck?.  I'm very familiar with Corel Draw, but the MoshiLaser plugin is definitely junk and MoshiDraw is even worse.  I can get it to Cut/engrave and outline, but can't get it to engrave.   


---
**Stephane Buisson** *February 08, 2016 23:27*

**+Kelly Burns**  answer in 2 videos here :


{% include youtubePlayer.html id="playlist" %}
[https://www.youtube.com/playlist?list=PL7vuA6bqcevE07tcktGXQpzPLXqOhG3Zf](https://www.youtube.com/playlist?list=PL7vuA6bqcevE07tcktGXQpzPLXqOhG3Zf)


---
**Kelly Burns** *February 08, 2016 23:31*

**+Stephane Buisson** I have plans to upgrade/mod, but want to at least make an attempt to get it working as is.  Thanks for the info.  I looked at Doc you provided earlier and very thorough.  If I go the Ramps route, it will come in very handy.


---
**Stephane Buisson** *February 09, 2016 08:33*

ramps not very powerfull (old), better to wait a bit for Smoothiebrainz (smoothieware) to go out.


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/KTb99QDYqeo) &mdash; content and formatting may not be reliable*
