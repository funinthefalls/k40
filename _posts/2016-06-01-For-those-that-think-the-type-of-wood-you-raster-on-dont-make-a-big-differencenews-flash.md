---
layout: post
title: "For those that think the type of wood you raster on don't make a big difference....news flash!"
date: June 01, 2016 21:50
category: "Object produced with laser"
author: "Scott Thorne"
---
For those that think the type of wood you raster on don't make a big difference....news flash! The left raster is done on alder wood cooking planks sold on Amazon.com. for 20 bucks...the raster on the right is Baltic birch plywood...40 sheets 12x12 for 35 bucks....do the math!

![images/3f9b7efb11dd23548785339599003504.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f9b7efb11dd23548785339599003504.jpeg)



**"Scott Thorne"**

---
---
**Ariel Yahni (UniKpty)** *June 01, 2016 21:59*

I love that lady


---
**Scott Thorne** *June 01, 2016 22:05*

**+Ariel Yahni**...me too bro...that's why I keep rastering her....my wife is not to happy but what the hell...lol


---
**Alex Krause** *June 01, 2016 22:07*

WOW nice detail!


---
**Alex Krause** *June 01, 2016 22:08*

Is that a dithered image? Or does your unit support PWM


---
**Scott Thorne** *June 01, 2016 22:38*

**+Alex Krause**...it's dithered....same settings with both types of wood. 


---
**Ariel Yahni (UniKpty)** *June 01, 2016 23:58*

**+Scott Thorne**​ can you post settings please,  I want to approximate 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 02, 2016 00:17*

Quite a big difference between the two. I'm looking into various different species of hardwood decking lately. Will be doing some tests once I get the wood (have to wait til local supplier has a big enough order from the manufacturer to add my small quantities onto it).


---
**Rob Morgan** *June 02, 2016 03:38*

Where do i get 40 sheets for 35$?


---
**Scott Thorne** *June 02, 2016 09:14*

**+Ariel Yahni**..power 35%...speed is 300mm/s


---
**Scott Thorne** *June 02, 2016 09:15*

**+Allready Gone**...photoshop


---
**Scott Thorne** *June 02, 2016 09:15*

**+Rob Morgan**...amazon


---
**Mr Shahrouz** *June 02, 2016 13:01*

Very nice 


---
**Lance Robaldo** *June 03, 2016 10:31*

Best pricing I can find on amazon is $49 for 40 pieces of 12x12 .  Do you have a link to the $35 item? 


---
**Scott Thorne** *June 03, 2016 12:35*

**+Lance Robaldo**....I ordered it 6 months ago....that might be the price now...I'll check when I get off. 


---
**Ariel Yahni (UniKpty)** *June 03, 2016 12:43*

This one seems fine [https://plus.google.com/+ArielYahni/posts/RVAjkCzomYS](https://plus.google.com/+ArielYahni/posts/RVAjkCzomYS)


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/XPPjQyNj9XU) &mdash; content and formatting may not be reliable*
