---
layout: post
title: "Hello, Is there anyone on the group interested in selling an rotary attachment for a basic 40w chinese laser which I just bought?"
date: June 21, 2018 17:36
category: "Discussion"
author: "Vlad Isari"
---
Hello,



Is there anyone on the group interested in selling an rotary attachment for a basic 40w chinese laser which I just bought? This is where I bought it from: [https://www.ebay.co.uk/itm/152960792863](https://www.ebay.co.uk/itm/152960792863)



I leave in UK, and I couldn't found any around so the best option are you guys , or to make a replica after the sketch share on the group, but I am not very confident.





**"Vlad Isari"**

---
---
**Stephane Buisson** *June 22, 2018 12:12*

hardware is one thing, software another.

will you modify your controller board or stay stock ?

what software ?


---
**Vlad Isari** *June 22, 2018 13:15*

To be honest i'm new in this and i think the best option is to see how is going, and then make some upgrades.

Obviously , i will make the necessary upgrades for the rotary to work. But i need to find one and see what are the requirements. For the moment i just fitted the laser, and i am waiting for deionized water, another smoke fan etc.

I am open to any advice. 




---
*Imported from [Google+](https://plus.google.com/114019829123284097632/posts/ABGSTorAFAq) &mdash; content and formatting may not be reliable*
