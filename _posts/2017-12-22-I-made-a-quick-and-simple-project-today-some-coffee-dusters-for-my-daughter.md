---
layout: post
title: "I made a quick and simple project today, some coffee dusters for my daughter"
date: December 22, 2017 21:44
category: "Object produced with laser"
author: "Bob Damato"
---
I made a quick and simple project today, some coffee dusters for my daughter. She wanted something to put designs on the top of coffee, and should make a good stocking stuffer.



.050" acrylic.  Should be fine for dusting coffee.

![images/a18b4b9388baa40ef28c0db027737b3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a18b4b9388baa40ef28c0db027737b3a.jpeg)



**"Bob Damato"**

---
---
**Ned Hill** *December 23, 2017 15:14*

Very cool idea.  May have to steal this :D


---
**HalfNormal** *December 23, 2017 16:43*

How thick is the acrylic? 


---
**Bob Damato** *December 23, 2017 18:09*

Ned, of course! I use photoshop and corel laser since my system is pretty much stock but you or whoever are welcome to what I have for files.




---
**Bob Damato** *December 23, 2017 18:10*

HalfNormal, .050"  so they are still fairly flexible.




---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/UoXXYDzPrjv) &mdash; content and formatting may not be reliable*
