---
layout: post
title: "Could someone give me a hand with a vital step my brain is missing?"
date: December 12, 2016 02:59
category: "Software"
author: "Kelly S"
---
Could someone give me a hand with a vital step my brain is missing?  I am trying to make a tree decoration, with the name in the middle and snow flakes holding it into place.  I am using inkscape but for the life of me cannot find a way to delete paths, and it is driving me crazy, trying to set it up to be engraved and cut, but cannot get it set even for the cut, grr.  Photo for demo of where I am at, any help would be greatly appreciated. 

![images/aa42b926d5539d611b4b959348b9d91e.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/aa42b926d5539d611b4b959348b9d91e.gif)



**"Kelly S"**

---
---
**Alex Krause** *December 12, 2016 03:13*

Try doing it in Photoshop first then use the trace option of inkscape... might be quicker


---
**Ulf Stahmer** *December 12, 2016 03:16*

To delete an individual path, click somewhere on the path you want to delete to select it. And then hit delete. When a path is selected, Inkscape will highlight it with a dotted outline and the arrows at the control points. In the illustration above, all the paths have the dotted line boxes, thus all are selected. Hitting delete now would delete all paths. If a group of paths is to be deleted, click and drag to select all paths completely bounded by the box. Hope this makes sense.


---
**Kelly S** *December 12, 2016 03:16*

I am worse in photoshop.  :P 

Last artsie program I used was Jasq.


---
**Cesar Tolentino** *December 12, 2016 03:18*

hmmm im no expert but i use inkscape too for my lasersaur... but can u explain more what you mean by delte paths?  can you not simply click it and hit delete button? if you are selecting it and it is selecting all... then it is grouped? then right click enter group? or did i mis-understood you?




---
**Ulf Stahmer** *December 12, 2016 03:20*

Here's a good guide for Inkscape [tavmjong.free.fr - Inkscape](http://tavmjong.free.fr/INKSCAPE/MANUAL/html/) It's quite a powerful piece of software!


---
**Cesar Tolentino** *December 12, 2016 03:20*

Press F2 ???


---
**Ulf Stahmer** *December 12, 2016 03:22*

F2 turns the "edit paths by nodes" tool on.


---
**Kelly S** *December 12, 2016 03:29*

I am trying to find a way to get it to merge them all, but have one fluid outline so it is good to be cut.  Inside and outside.  I have tried joining, and combining but the larger shapes eat up the inside shapes and I get a nice mess of nothing. 


---
**Cesar Tolentino** *December 12, 2016 03:38*

Kelly im with you. I dont know much... I use CAD or Sketchup for that... I want to hear the solution too... so i can learn...




---
**Kelly S** *December 12, 2016 03:54*

Caved took a screen shot and editing in paint, ha...




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 04:13*

From what I gather, you're wanting to merge the snowflakes shape with the outer ring & the inner name plate? 



First things, you want to "punch" out the 2nd outer ring of the most outer ring. I usually work with semi-opaque solid fills to begin with, so you can see what is happening to the path/shape.



So, start off by selecting the most outer ring. Choose Option > Lower to Bottom (END). Then while it is still selected, Shift + click on the 2nd most outer ring. Now, to punch it out, choose Path > Difference (Ctrl -). This should now have a ring with another ring punched out of it. Verify by giving it a fill colour.



Next, select this + your snowflakes + your nameplate & choose Path > Union (Ctrl +). This will combine all the shapes into one shape. 



Although, if your snowflakes are still made up of multiple shapes, you will need to combine them & difference them too... 



Hope this helps.


---
**ThantiK** *December 12, 2016 04:40*

**+Yuusuf Sallahuddin**​ beat me to it.   You've gotta use the boolean path tools while selecting multiple paths to combine/carve/union/difference the paths together.  Surprised it took so long for someone to suggest that. 


---
**Cesar Tolentino** *December 12, 2016 04:44*

AHA...never used that before... but i think im getting it... thanks? how about you Kelly? Image below is using Path-Union



![images/b56becabbd73798da75f735716488c0a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b56becabbd73798da75f735716488c0a.png)


---
**Kelly S** *December 12, 2016 04:47*

**+Yuusuf Sallahuddin** Thank you I will not be ably to try this tonight, but will definitely take another swing tomorrow.  :D 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 05:36*

**+Cesar Tolentino** They're handy features once you work out how to use them. I am always using Union/Difference/Intersect & sometimes also using Exclusion. I wouldn't be able to do what I do without these features.



**+ThantiK** I was also surprised no-one mentioned yet, but that's why a community is great. What someone overlooks, someone else may see sticking out like the proverbial dog's balls.


---
**Stephane Buisson** *December 12, 2016 09:27*

post moved to Software category


---
**ThantiK** *December 12, 2016 14:11*

Additionally use the fill tool to see what/where is considered a solid shape. This should help visualization of how to use the boolean tools. 


---
**Kelly S** *December 12, 2016 19:54*

**+Yuusuf Sallahuddin** Got back to it today, seems to be working well!  Thank you thank you. 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 23:56*

**+Kelly S** You're welcome. Usually if you get stuck on something you want to do in some commonly used graphics suite you'll be able to find a tutorial on youtube too. Sometimes a lot easier to understand when you can see them doing it too.


---
**Kelly S** *December 13, 2016 00:35*

I did try looking, but found so many unrelevant things haha...


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/Q37JVMgxpRc) &mdash; content and formatting may not be reliable*
