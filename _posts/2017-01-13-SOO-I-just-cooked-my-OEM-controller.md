---
layout: post
title: "SOO.. I just cooked my OEM controller"
date: January 13, 2017 11:13
category: "Original software and hardware issues"
author: "Abe Fouhy"
---
SOO.. I just cooked my OEM controller. So I will be getting that new motherboard upgrade, but I think my faulty PS cooked it. When hitting the test button I was hearing some crackling, so I took the PS case off to look at it and see if I could see if anything was getting hot. Hit the test button and LED 2 and LED 3 where blinking as if it had a error/misfire and then a second later the OEM board released the magic smoke. 



Does anyone know of a good place to buy a high quality PS since I am getting the new controller board?





**"Abe Fouhy"**

---
---
**Don Kleinschnitz Jr.** *January 13, 2017 13:54*

Uh oh :( sorry. You have something seriously wrong to blow the board. That means something likely happened to the DC supply in the LPS. 



I am not familiar with that supply so I am not sure how the dc is connected. (picture of supply).



I suggest you take copious pictures of all the  components and th wiring back at about a foot and then at each connector so that you know what was the stock configuration. Include the control panel and its wiring.



What controller are you thinking of getting?



You will need to be sure you get the right supply.  **+Kim Stroman** I think you bought a new supply. Do you have a recommendation for a vendor you are happy with?


---
**K** *January 13, 2017 13:56*

**+Don Kleinschnitz** I bought this one from LightObject.com: [http://www.lightobject.com/20W45W-PWM-CO2-Laser-Power-Supply-AC110V-P71.aspx](http://www.lightobject.com/20W45W-PWM-CO2-Laser-Power-Supply-AC110V-P71.aspx)


---
**Abe Fouhy** *January 13, 2017 18:08*

Did that have the 24v supply built in?


---
**Abe Fouhy** *January 13, 2017 18:12*

I was thinking of the cohesion3d mini as it looks like an easy swap or the smoothie v4.



The light object looks solid, but its almost 3x the price of ebay. Would you guys suggest upgrading to a larger capacity such as a ps that could supply up to 60w to allow case mods for a 60w later?﻿



Any luck with ebay ps?


---
**Don Kleinschnitz Jr.** *January 13, 2017 18:57*

**+Abe Fouhy** I think the Cohesion 3D takes minimal effort and a good choice for you. **+Ray Kholodovsky** 

I haven't bought a LPS yet but if I did I would size it for a 50-60W tube, if affordable.

**+Kim Stroman** did you also buy a bigger tube.


---
**Abe Fouhy** *January 13, 2017 19:03*

Cool bud. Thanks for the quick reply.


---
**K** *January 13, 2017 23:52*

The PSU doesn't have 24v for the controller. And I did not buy a bigger tube. 


---
**Abe Fouhy** *February 19, 2017 06:42*

So I finally will be getting some money to do this right. After thinking about it, I'm going to replace the PS, get the cohesion3d board and a new interface for it and replace the stock digital input power meter with test button. Any suggestions for a new interface and should i get two PS's a 24vdc and hv ps for the laser or a combined one like mine?


---
**Abe Fouhy** *February 19, 2017 06:43*

![images/94ef221ad734c157cd869d8367ea2f85.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/94ef221ad734c157cd869d8367ea2f85.jpeg)


---
**Abe Fouhy** *February 19, 2017 06:44*

![images/1d86f104749311a9f99ca266f252df9b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1d86f104749311a9f99ca266f252df9b.jpeg)


---
**Don Kleinschnitz Jr.** *February 19, 2017 14:33*

**+Abe Fouhy** below are some choices for LPS, DC power & control panel to select from depending on your budget and longer term plans for the K40:

..................

1.0 Buy a new quality LPS supply without 24VDC. 



1.1.0 Quality replacement: 

[http://www.lightobject.com/20W45W-PWM-CO2-Laser-Power-Supply-AC110V-P71.aspx](http://www.lightobject.com/20W45W-PWM-CO2-Laser-Power-Supply-AC110V-P71.aspx)



1.1.1  Buy LPS wattage for future expansion of laser power. Depending on your budget and the possibility to upgrade your tube to 50W. I would consider buying this one for $35 more. (a 50W tube requires cabinet modification).

[lightobject.com - 40W~60W PWM CO2 Laser Power Supply (AC110V)](http://www.lightobject.com/40W60W-PWM-CO2-Laser-Power-Supply-AC110V-P72.aspx) 

........................

2.0 Buy cheaper direct replacement with 24VDC. You can get these LPS cheaper but I would expect typical Chinese quality:



2.1 [https://www.amazon.com/Iglobalbuy-Supply-Engraving-Engraver-Cutting/dp/B01N4K82TC/ref=sr_1_fkmr3_3?s=pc&ie=UTF8&qid=1487512300&sr=8-3-fkmr3&keywords=K40+laser+power+supply](https://www.amazon.com/Iglobalbuy-Supply-Engraving-Engraver-Cutting/dp/B01N4K82TC/ref=sr_1_fkmr3_3?s=pc&ie=UTF8&qid=1487512300&sr=8-3-fkmr3&keywords=K40+laser+power+supply)



2.2 [http://www.ebay.com/itm/40W-Power-Supply-K40-CO2-Laser-Engraver-Cutter-110V-220V-H-wire-Express-Free-/331970211154?hash=item4d4af76d52:g:rIMAAOSwYmZXHIlk](http://www.ebay.com/itm/40W-Power-Supply-K40-CO2-Laser-Engraver-Cutter-110V-220V-H-wire-Express-Free-/331970211154?hash=item4d4af76d52:g:rIMAAOSwYmZXHIlk)



2.3 [http://www.ebay.com/itm/New-HQ-40W-Power-Supply-for-CO2-Laser-Engraver-Cuttier-110V-Expedited-HV-Wire/322085595582?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D2%26asc%3D41375%26meid%3D540ea7efd54740b8a9be60ce96bce037%26pid%3D100005%26rk%3D4%26rkt%3D6%26sd%3D331970211154](http://www.ebay.com/itm/New-HQ-40W-Power-Supply-for-CO2-Laser-Engraver-Cuttier-110V-Expedited-HV-Wire/322085595582?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D2%26asc%3D41375%26meid%3D540ea7efd54740b8a9be60ce96bce037%26pid%3D100005%26rk%3D4%26rkt%3D6%26sd%3D331970211154)



You could poll this community to see ab out the best place to buy the cheaper supply.

...............

3.0 Use the internal LPS 24 vdc supply. If you are upgrading the controller to a smoothie you can save some $ by using the internal 24VDC. Many have converted this way but the DC supply in these LPS are marginal (some conversions would disagree with me). If you do not plan to add anything to the 24-5 VDC (like rotary etc) you may be OK. Also I do not like the DC power on my controller coming from a HVPS could fry your controller, like just happened :(?.  If you do this I would add a fuse when you install it.

................

4.0 Add a 24 VDC supply. This is the route I used because I get plenty of 24 VDC capacity for expansion and isolation from the HV LPS. 



[https://www.amazon.com/gp/product/B018TG7I4W/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B018TG7I4W/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)

...............

5.0 Reuse the current panel. I do not have a wiring diagram nor schematic for that panel (does anyone?). However if you are careful taking it apart we can likely be successful with the conversion without changing the panel or its switches. 

5.1 Take a clear front and back picture of the circuit board, from above, so that we can trace or create a schematic if needed.

5.2 Trace the wiring from the panel to the current LPS and other K40 points and draw that on a piece of paper, using the labels on the current LPS. Label/tag each wire that connected to the LPS or other points in the machine as you remove them so we can put them back in the right place.

...................

6.0 Design a new panel alike the typical K40 without the PCB for the control panel. I or others can provide you information to do so. 



I feel a bit like an ambulance chaser. That supply is a vintage that my LPS lab does not have. If you are in the US you could elect to donate it to the research work I am doing on LPS where I can diagnose what happened.



Hope this is helpful.....


---
**Abe Fouhy** *February 19, 2017 15:42*

**+Don Kleinschnitz**​ you are awesome, you took quite a bit of time to work out the details in this post. I truly appreciate it. It is hard to know what to do next starting out, especially in a new hobby and then to have your maching crap out was unfortunate.

I think I am going to try for the 60w light object HVPS, get the 24vdc amazon LPS to isolate the two, the cohesion3d and buy a standard digital control board as it had a blown solder joint and may have other issues (mostly because its $25 and i don't want to be worried it's the issue, when i could go all new). So I'm looking at $235, $17, $25 and going to fuse all my components for another $20. $303, that's not too bad to have more control and higher quality components. 



I may be able to budget more, what would be handy to have for the interface? I have a pi and arduino if that helps. Do you have a link to the 50/60w case modifications? For the fuses should I use slow blow ceramic, fast blow automotive blades or fast blow single filament tube fuses to protect the gear?



You can absolutely have the PS, you've been a huge help bud. You can have all the components I am replacing. :)


---
**Don Kleinschnitz Jr.** *February 19, 2017 15:43*

**+Abe Fouhy** after looking at this machine more closely it seems not to have a laser current measurement. I would definitely add that in any of the cases above. 


---
**Abe Fouhy** *February 19, 2017 15:45*

With a standard ammeter or is there something specific?


---
**Don Kleinschnitz Jr.** *February 19, 2017 16:00*

**+Abe Fouhy** I recently got this meter for my HV lab setup. I have not tested it. 

I recently got this meter for my HV lab setup. I have NOT tested it. 



It is bigger than the stock K40 but that may be a good thing. 



[amazon.com - Amazon.com: uxcell DMiotech 85C1-A Analog Current Panel Meter DC 30A Ammeter Ampere Tester Gauge: Automotive](https://www.amazon.com/gp/product/B0050GIN2Q/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)




---
**Abe Fouhy** *February 19, 2017 16:04*

Is that too big to see any difference, doesn't the laser shoot at 20-30mA?


---
**Abe Fouhy** *February 19, 2017 16:06*

I saw an old post to about using a voltmeter and cooling regulation, guess thats going on the list :)

How important is cooling at this level? Thats another $200 I may for go unless critical.


---
**Don Kleinschnitz Jr.** *February 19, 2017 16:17*

**+Abe Fouhy** that meter is 0-30ma and so is the stock K40? FYI: this meter is a totally separate function than the rest of your digital panel.

.......

I posted the voltmeter whose purpose it to  shows the pots position more granularity. I am guessing that your digital panel already does that??

I think the cooling regulation post that you referring to is not "regulation" rather measurement and alarms. 

.......

Adding a cooler will improve you tubes life if you are planning heavier (higher duty cycle) use of your machine but depends on your ambient. You can run low duty cycle stuff with standard bucket cooling and enhance that with ice etc. 

........

Can you post pictures (from above) of the front and back or your panel PCB. Also a clear picture of its functions.



This may be the post on the digital meter and temp controller add.



[https://plus.google.com/+KonstantinosFilosofou/posts/GQXMU6rv6MZ](https://plus.google.com/+KonstantinosFilosofou/posts/GQXMU6rv6MZ)


---
**Abe Fouhy** *February 19, 2017 17:06*

**+Don Kleinschnitz** here are some pictures of the unit. 

![images/4fd058e05fd61c0d2bfbfec43928a0dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4fd058e05fd61c0d2bfbfec43928a0dc.jpeg)


---
**Abe Fouhy** *February 19, 2017 17:06*

![images/3af1a038fd3a2def8ba57edb70b36f05.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3af1a038fd3a2def8ba57edb70b36f05.jpeg)


---
**Abe Fouhy** *February 19, 2017 17:06*

![images/942cb7f135e48a9ca5e61dc6015e7d70.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/942cb7f135e48a9ca5e61dc6015e7d70.jpeg)


---
**Abe Fouhy** *February 19, 2017 17:07*

Ps and the resistor we talked about

![images/0e971a3bc50546ee2c3d7880bf2a47f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0e971a3bc50546ee2c3d7880bf2a47f6.jpeg)


---
**Don Kleinschnitz Jr.** *February 19, 2017 17:48*

**+Abe Fouhy** ok the panel supports various steps of current control. Likely a digital pot function in place of an actual pot. That means that you could use this panel with your new setup. It kind of gives you an indication of current and post position similar to the digital meter + pot in a standard K40. The value in the display has to be a relative setting as there is no actual current input to this panel.



I do not see an "enable" function and the "D" function on the LPS is disabled (see your picture). This makes this machine really unsafe since there are no interlocks and no water protection.

The K function has only one wire on the connector which suggests to me that it gets ground up on the pcb. This could also allow erroneous "test" events. 



If it was me I would not keep this panel because:

.... you can get this and better control from a 10 turn pot and the digital voltmeter. See my post on replacing pot.



....There is no lasing current indicator, it must assume that the current setting = the actual laser current which is not actually true.



....Its unsafe ....



......................

The resistor may be a ballast resistor to help control the current to the laser. When the tube ionizes the voltage drops and the current increases and this resistor helps takes up the slack .  I don't see the need for this in a switching supply. By inspection it looks like a switching supply so that resistor confuses me. If this is not a switcher a new supply would give better results and does not need this resistor.

......................

I would love to see this supply close up :).

 

Where is this resistor connected from the cathode to gnd???



Can you tell me its value?



.........................

Thanks for the pictures and info. although I cannot read the traces and chip #s on the pcb as its out of focus :)


---
**Abe Fouhy** *February 19, 2017 18:00*

Yea it was really hard to find a spot to focus my camera, the room is a bit dim. You'll soon have better info with it in your hands :D

I'll check the ohm value in a bit, I am starting to wonder about this little machine.



Everything I read here shows me that the previous user upgraded these components, is the head LO like yours? In the process of upgrading he may of blew a PS and replaced it with a switching and kept the resistor, i don't know. I couldn't tell you the difference by looking at the two. 



I am leaning toward a panel redesign to put some digital/analog volt and ammeters there as I think it will be really helpful for me to learn how the laser cuts/engraves according to the power levels. Also more data is always better. Plus if they are analog I could make this a cool steam punk laser setup haha. 


---
**Don Kleinschnitz Jr.** *February 19, 2017 18:12*

**+Abe Fouhy** 

I agree that you would be better off fixing all this stuff especially adding the safety features......"PLEASE" with a more functional adn COOL panel.

I appreciate the info as I have seen these digital panels but did not know what they did ..... not impressed :(.



Getting that D supply would be awesome ...... after seeing that close up picture I realized I worked with one other, **+Chris Menchion**, that has the same supply. It was difficult to get working right.



I noticed that on the back of the supply the frame gnd looks disconnected. Be careful around an un-grounded HVLPS  .....



Where in the world do you live .......




---
**Abe Fouhy** *February 19, 2017 18:21*

I live in Oregon, yea I was wondering about that! Ungrounded PS's in general need to be grounded, but this machine looks sketchy on the electrical.. it'll be good to get it all cleaned up with new documented hardware with UL listings. :)


---
**Abe Fouhy** *February 19, 2017 18:21*

And yes, I am putting interlocks on first thing too!


---
**Don Kleinschnitz Jr.** *February 19, 2017 18:31*

**+Don Kleinschnitz**  some questions I forgot to answer:



I have everything fused using automotive blades. This post has details and part numbers of fuse holders.

[donsthings.blogspot.com - K40-S DC Power Systems Design](http://donsthings.blogspot.com/2016/06/k40s-power-systems-design.html)



I simply connect mine to a PC so nothing needed special to interface.



I will find the reference to the cabinet extensions.



BTW there a many k40 panel designs on thingiverse.





In case I have not shared earlier. Here is the main index for all the conversions, designs etc I have done.


---
**Don Kleinschnitz Jr.** *February 19, 2017 18:32*

**+Abe Fouhy** Nice I am in Utah. These machines especially need good gnding because of the lethal HV.



Some cabinet extensions: 

[http://www.thingiverse.com/thing:1263233](http://www.thingiverse.com/thing:1263233)

[http://www.thingiverse.com/thing:765057](http://www.thingiverse.com/thing:765057)

[http://www.thingiverse.com/thing:2045703](http://www.thingiverse.com/thing:2045703)



I was thinking of making one from PVC with a flange to mount it. 




---
**Abe Fouhy** *February 19, 2017 18:34*

Absolutely, btw - does my laser head look like an upgrade or standard part? My mirrors are silver, and I know that others are gold, so I didn't know if the previous owner may of hooked me up.


---
**Abe Fouhy** *February 19, 2017 18:39*

Checked your site, looks like I will be on that quite a bit more. That fuse block is exactly what I was thinking about!


---
**Don Kleinschnitz Jr.** *February 19, 2017 18:39*

**+Abe Fouhy** it looks a standard but with a port for air assist and laser pointer bracket added.


---
**Abe Fouhy** *February 19, 2017 18:56*

Ok cool. You don't by chance have a sweet hack for testing the power of the tubes with one of those calibrated oven thermometers ([http://www.2laser.com/laser_power_meter_probe_and_laser_monitor](http://www.2laser.com/laser_power_meter_probe_and_laser_monitor)) do you? Ie a standard one wire sensor enclosed in 1/4 copper tubing and then temp converted to power in watts?


---
**Don Kleinschnitz Jr.** *February 19, 2017 19:14*

No I don't (yet) the problem is the surface has to be one that absorbs 10,600 NM consistently and then you need a calibration means. 



I have done a few experiments with peltier devices and that produces an output but nothing serious yet.



The real question is why do you want to know the power and what do you do with the # when you get it? 


---
**Abe Fouhy** *February 19, 2017 19:20*

Oh I want to know the power to create a engraving and cut chart on my tube. From some basic research it looks like 100% power may not actually give you more cutting depth and that every tube is different. A few experiments I saw said that at 85% was their optimal cutting depth and had a lot less electrical noise generated, giving a bit more life to the tube, while another was down at 70% being their max. They could both jack up the power, but it didn't yield better results. 



I was just thinking if we had a cheap DIY meter we could all get a good baseline on where the power curve is on our tubes and save some life out of them. We could then take the data and create a power chart ring around our pots on the control panel to show engraving depth/cut depth and speed or chart for quick reference on materials we would like to cut.


---
**Abe Fouhy** *February 19, 2017 19:22*

I didn't even think about the peltier, that is a great idea! Does it have to be an absorption material or could we use a IR transmitter/receiver to absorb the spectrum around a beam and plot that live while cutting to a controller to interpret the voltage map?


---
**Don Kleinschnitz Jr.** *February 19, 2017 19:42*

**+Abe Fouhy** 

The digital meter gives me a position on the pot that I can return to so that I do not have to pulse the laser to get to a specific current.



The problem is that lasers are a consumable and that over time at the same current they output different power. That is why there is an adjustment. Over time you will have to increase the current to get the same output until at max current you cannot get enough power. 



Then also the quality of engraving and cutting is dependent on many factors other than power: the original image quality and tuning, the type of material, the moisture in the material, the response of the material to 10,600 nm, speed just to cite a few.  



Laser failure mode is also many factored:

 

...From heat damaging the enclosure, seals and especially the output mirror.  As the gas ionizes it creates heat that has to be removed so running cooler is better. 



....From the gas leaking down (all tubes leak all the time). 



I can't see how electrical noise has anything to do with failure??? 



Manufacturers recommend to keep current below 18-20 ma. This is only because the tube is rated for 1000 hrs including a de-rating over time. To get this life the tube can actually operate initially @ >24 ma and higher max power.  Essentially its design output starts at a higher power than is needed and therefore it takes longer to get to an unacceptable level.



I don't know that its practical to expect all these conditions can be consistent enough that everyone can use the same data effectively. Its a "your results may vary" kind of problem. Perhaps we can get some range of value though.



I agree that it would be nice is there was a cheap way to measure light power. BTW its a pain to go through a power measurement even with a lollypop every time you do a job :).


---
**Abe Fouhy** *February 19, 2017 19:58*

This post is perfect for me, as I am an engineer at heart and love to over complicate the issue. This is essentially saying to me, fix your machine, do some lasing then let's try to see if there is really a problem with something rather than assuming with you 2 days of lasing haha. :)


---
**Don Kleinschnitz Jr.** *February 19, 2017 20:01*

**+Abe Fouhy** in regard to measuring light output. 

I tried using IR sensors and there is a few basic problems:

1. They do not have any response in the FAR region 10,600 nm.

2. Its hard to keep them cool and not fry them. I even used a water drop to attempt attenuation.

3. The sensors that do have response in the Far infrared by definition are slow. i.e thermo-couples. Heat changes are generally slow so the devices like are in PIR sensors and peltier don't have the needed response. 

.....

[http://donsthings.blogspot.com/2016/07/dynamic-laser-response-testing.html](http://donsthings.blogspot.com/2016/07/dynamic-laser-response-testing.html)

......

I settled on looking at the current from the tube while operating and syncing that with the printing. I was using this to try and characterize optical response to better understand PWM control for engraving. I was not trying to measure power. 



Some recent information I have received suggests that the light output may not be well represented by the ionizing discharge current and my conclusions may be off by 130 us or so. Appendix C: Laser Response Characteristics



I do think that a relative indication of power is possible if we put a fixed duration of power into a peltier. I do not know what the light will do to the raw surface (ceramic) of the peltier and if that will create an error over exposures. One idea is to thermally glue an anodized block of aluminum to the face of the module.  Anodizing is a good Far IR absorb-er and it is hard.



[donsthings.blogspot.com - Engraving and PWM Control](http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html)


---
**Don Kleinschnitz Jr.** *February 19, 2017 20:05*

**+Abe Fouhy** I am often accused of over-engineering things, but in the end most forgive that in leu of getting a real solution :)....


---
**Abe Fouhy** *February 19, 2017 20:06*

And aluminum has an excellent heat transfer of 2400k to that peltier and help to shield it from getting cooked directly. Are there seebecks designed for measurement we could use? It is my understanding that peltier works better e- --> to heat/cooling rather than the other way around (around 2/3 less then it normal operation)


---
**Abe Fouhy** *February 19, 2017 20:26*

**+Don Kleinschnitz** Yup. Same here about over engineering. Sometimes in my favor, sometimes not, always fun


---
**Don Kleinschnitz Jr.** *February 19, 2017 20:28*

I sent you hangout invite its sometimes more convenient to chat.

What is a "seebeck"


---
**Abe Fouhy** *February 19, 2017 20:48*

I didn't get the invite. Seebeck is the reverse of peltier, different type of thermoplastic from my understanding 


---
**Don Kleinschnitz Jr.** *February 19, 2017 22:20*

do you have hangouts installed?




---
**Abe Fouhy** *February 19, 2017 22:48*

Yup. No invite.


---
**Abe Fouhy** *March 03, 2017 07:37*

Got to thinking about the tube power. What if we manufactured a calibrated multilayer acrylic with different colors based on thickness/hardness. If the tube goes through x depth you could see the color gradient and use a chart to determine the depth and compare it to a fully calibrated laser.  Could be made cheap and expendable and easy to cut. Could be mounted to the front of the exit of the tube or to any mirror.


---
**Don Kleinschnitz Jr.** *March 03, 2017 15:22*

**+Abe Fouhy** that might work the challenge always is to get a good calibration and the variance of the materials and optics. I have wondered about just measuring the depth of a fixed pulse into a piece of acrylic. After exposing it; cross section it, polish it and measure the depth. 

I always come back to the question: what would you do with and absolute power measurement if you had one? Seems what you really want to know is what is the relative power at each point in the optical system from the time you got a new tube.


---
**Abe Fouhy** *March 03, 2017 17:58*

For me, I want a power meter, because I bought my machine used and don't know if the tube has all it's life in it. I want a test so I know if I need to drop $240 bucks more into it after I put $300 to get it working again. I sure hope the tube is all good! :)


---
**Don Kleinschnitz Jr.** *March 03, 2017 18:29*

**+Abe Fouhy** in the absence of a meter then I would suggest doing some cutting on whatever material you want to use and see if it will do it or not.

These tubes are consumable meaning that they will all wear out with time and usage. Even if your tube is lower in power than a new one I would guess you will use it up until it wont do what you want :)


---
**Abe Fouhy** *March 03, 2017 18:39*

Thats true it was cutting before it broke. Getting paranoid haha


---
**Don Kleinschnitz Jr.** *March 03, 2017 18:46*

**+Abe Fouhy** Is it still broke? What kind of coolant were you using and how old was it?




---
**Abe Fouhy** *March 03, 2017 19:40*

I only used my machine for 2 days before it broke, I was using distilled water and used it for about 2 hours or so..

Anxious to get it up and running again!


---
**Don Kleinschnitz Jr.** *March 03, 2017 21:27*

**+Abe Fouhy** sorry I lost track what is broken?


---
**Abe Fouhy** *March 03, 2017 22:55*

Oh the PS is broken. It shorted out my controller. So I all of the electronics. New PS, new cohesion3d card, customizing the front panel to analog/digital devices. Installing interlocks, water flow & temp sensor, volt and amp sensor/display, separate DC supply, breaker/fused DC and AC side panel, fans mounted inside to keep everything nice and cool. Switches on panel to turn on accessories. Big upgrade.


---
**Don Kleinschnitz Jr.** *March 04, 2017 00:18*

**+Abe Fouhy** nice it only took me a year. refer to the blog often lol.


---
**Abe Fouhy** *March 04, 2017 00:20*

I hear ya, i need to do some more work to pay for this :)


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/QCoFWd5Niqe) &mdash; content and formatting may not be reliable*
