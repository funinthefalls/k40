---
layout: post
title: "In fact of a stupid mistake I have broken my K40 :-( First of all does anybody have an idea or knowledge what kind of electronic part these AG1519 or AG1508 is?"
date: July 14, 2018 09:02
category: "Original software and hardware issues"
author: "Torsten M"
---
In fact of a stupid mistake I have broken my K40 :-(

First of all does anybody have an idea or knowledge what kind of electronic part these AG1519 or AG1508 is? Schottky diode or so? It is between 5V and ground and my one is exploded.



The effect right now is everything looks good and the laser is firing with the test switch. But running with K40whisperer the head is moving without any laser fire..



Any hint?



![images/5838344cb37c95f7c663df6ea99b52bf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5838344cb37c95f7c663df6ea99b52bf.jpeg)
![images/e35c6ab165b4c626008177ed9ca0da5f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e35c6ab165b4c626008177ed9ca0da5f.jpeg)

**"Torsten M"**

---
---
**Daniel Kruger** *July 14, 2018 11:58*

Did you apply reverse voltage to the board?


---
**Torsten M** *July 14, 2018 12:14*

**+Daniel Kruger** unfortunately yes :( 

For installing a current gauge I unplugged the Laser power switch. At reinstalling the switches I was not concentrated and replugged the cables wrong. I think I gave 24V onto the 5V line and that probably in forward direction for this Diode or whatever. 


---
**Don Kleinschnitz Jr.** *July 14, 2018 13:55*

If that is a protection diode in the power circuit I would imagine any general purpose diode will work such as 1N2001-2004. That panel does not draw much current.



<s>---</s>

These two resources may allow you to find out what signal is not present on the LPS from the panel, stopping the laser from firing. I suspect that there is no 5VDC power on the control panel [open diode].



[donsthings.blogspot.com - Troubleshooting a K40 Laser Power System](http://donsthings.blogspot.com/2018/05/troubleshooting-k40-laser-power-system.html)





[http://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html](http://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html)




---
**Torsten M** *July 14, 2018 19:50*

Thanks Don! I will try a standard diode and will check with your troubleshooting guide. 

 It would be nice to know what kind of diode it was originally.... I think the manufacturer is vishay, the marking symbol looks like there ones...


---
**Torsten M** *July 16, 2018 10:40*

I have a diode 1N4004 soldered and tested with your great checklist! 1st run through the checklist without success, then checked really every cable and found a wobbling one the data (USB) interface PCB. It's running now :-) 

Thanks everybody!! 


---
**Don Kleinschnitz Jr.** *July 16, 2018 13:14*

**+Torsten M** Great your fixed and running.

It was your USB connector on the controller that was at fault?



....was the troubleshooting guide useful and did you find any errors?


---
**Torsten M** *July 16, 2018 20:00*

**+Don Kleinschnitz**  It wasn‘t the USB connector itself. It was the 4-Pin connector to the LPS. One socket was pressed out of the white plastic connector and therefore without connection to the pin. Unfortunately not easy to see, but with touching/wobbling/pulling every each cable I found it. 

Your troubleshooting guide was helpfull, of course! For newbies in the K40 world like me a good guidance how to search in a structured  and effektive way. Additionally I have learned i.e. a bit about the PWM and how it works. Cool side effect.


---
**Don Kleinschnitz Jr.** *July 16, 2018 20:45*

**+Torsten M** excellent ....


---
*Imported from [Google+](https://plus.google.com/104585701321643651218/posts/am7JiNqo4sV) &mdash; content and formatting may not be reliable*
