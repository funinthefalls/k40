---
layout: post
title: "Hi all, I did something VERY stupid on the controller side (Cohesion3D) and on the X axis stepper motor something went smoking at this breakout board side"
date: February 26, 2018 14:54
category: "Original software and hardware issues"
author: "Maurizio Martinucci"
---
Hi all,  I did something VERY stupid on the controller side (Cohesion3D) and on the X axis stepper motor something went smoking at this breakout board side. I am not really an electronics expert but, beside the ribbon connectors, I only see two components that seem to be a fuse and a diode.  



Can anybody there give me more precise specs for this little board and maybe also if it's available for purchase new somewhere?



I still  can/want to try and  fix it resoldering the right components there.

Thank you very much in advance!



![images/38bb611ea41741672ef9647c70b09213.png](https://gitlab.com/funinthefalls/k40/raw/master/images/38bb611ea41741672ef9647c70b09213.png)
![images/7d6386de77423d8f5190fa3fe0920555.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7d6386de77423d8f5190fa3fe0920555.png)
![images/f1da58db5052e2f1be31bb55a46ead5c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1da58db5052e2f1be31bb55a46ead5c.jpeg)
![images/70e78e79977893f18e092a0957455b0c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/70e78e79977893f18e092a0957455b0c.jpeg)
![images/c98c22ef06bd44de2c76c9db46370857.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c98c22ef06bd44de2c76c9db46370857.jpeg)

**"Maurizio Martinucci"**

---
---
**HalfNormal** *February 26, 2018 15:24*

[http://donsthings.blogspot.com/search/label/K40%20Optical%20Endstops](http://donsthings.blogspot.com/search/label/K40%20Optical%20Endstops)


---
**Maurizio Martinucci** *February 26, 2018 15:32*

wow, thanks a lot, that is VERY useful!


---
*Imported from [Google+](https://plus.google.com/117027581754233433770/posts/dU8USZRDUeH) &mdash; content and formatting may not be reliable*
