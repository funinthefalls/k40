---
layout: post
title: "Margot Robbie....damn goddess!"
date: November 06, 2016 00:02
category: "Object produced with laser"
author: "Scott Thorne"
---
Margot Robbie....damn goddess! 

![images/f092fbd302154693996beeda24d49f1e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f092fbd302154693996beeda24d49f1e.jpeg)



**"Scott Thorne"**

---
---
**Ariel Yahni (UniKpty)** *November 06, 2016 00:12*

OMFG2


---
**Scott Thorne** *November 06, 2016 00:15*

Lol


---
**Scott Thorne** *November 06, 2016 00:16*

**+Ariel Yahni**...are you in the u.s?


---
**Ariel Yahni (UniKpty)** *November 06, 2016 00:20*

No Panama,  :)  should o move? 


---
**Scott Thorne** *November 06, 2016 00:21*

Lol...amazon deliver there? 


---
**Ariel Yahni (UniKpty)** *November 06, 2016 00:27*

I use a forwarder in Florida for whatever I buy 


---
**Scott Thorne** *November 06, 2016 00:45*

**+Ariel Yahni**...look on amazon for alderwood grilling planks ...they are 14x5.5..

About half an inch thick...you get 6 for 18 bucks...it's the best for photos that I've came across...try it. 


---
**Ariel Yahni (UniKpty)** *November 06, 2016 00:47*

Will do


---
**Tony Schelts** *November 06, 2016 00:50*

In the UK Amazon they are 9.86 pounds each. 






---
**Scott Thorne** *November 06, 2016 00:56*

**+Tony Schelts**..

Wow that's high. 


---
**Scott Thorne** *November 06, 2016 01:02*

[https://www.amazon.com/gp/aw/s/ref=is_s_ss_i_3_10?k=alderwood+planks&sprefix=alderwood+](https://www.amazon.com/gp/aw/s/ref=is_s_ss_i_3_10?k=alderwood+planks&sprefix=alderwood+)

[amazon.com - Amazon.com: alderwood planks](https://www.amazon.com/gp/aw/s/ref=is_s_ss_i_3_10?k=alderwood+planks&sprefix=alderwood+)


---
**Timo Birnschein** *November 06, 2016 08:06*

This runs on the K40 with the smoothie board? That is amazing!!


---
**Scott Thorne** *November 06, 2016 11:48*

**+Timo Birnschein**...sorry i have a dsp


---
**Bob Damato** *November 06, 2016 15:02*

Found them! [https://www.amazon.com/Alder-Grilling-Planks-PACK-absorption/dp/B013SMT57U/ref=sr_1_1?ie=UTF8&qid=1478444366&sr=8-1&keywords=alderwood+grilling+planks](https://www.amazon.com/Alder-Grilling-Planks-PACK-absorption/dp/B013SMT57U/ref=sr_1_1?ie=UTF8&qid=1478444366&sr=8-1&keywords=alderwood+grilling+planks)



[amazon.com - Robot Check](https://www.amazon.com/Alder-Grilling-Planks-PACK-absorption/dp/B013SMT57U/ref=sr_1_1?ie=UTF8&qid=1478444366&sr=8-1&keywords=alderwood+grilling+planks)


---
**takitus ᵗᵃᵏᶦᵗᵘˢ** *February 20, 2017 07:20*

What DSP are you using?


---
**Scott Thorne** *February 20, 2017 10:12*

Ruida


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/U5w3tn8oUra) &mdash; content and formatting may not be reliable*
