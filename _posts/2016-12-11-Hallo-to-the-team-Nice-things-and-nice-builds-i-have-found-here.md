---
layout: post
title: "Hallo to the team! Nice things and nice builds i have found here!"
date: December 11, 2016 18:44
category: "Modification"
author: "Kostas Filosofou"
---
Hallo to the team!

Nice things and nice builds i have found here! 

Time to show mine ...Before two months ago i buy the famous 40w Laser from Ebay and i get to work immediately, increase the cutting area in about 660mm x 300mm and now i work on the bed and i cut new laser adjustable head with air assist on the lathe.



Hope you Like it...









**"Kostas Filosofou"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 19:14*

Looks pretty spectacular :) The red is better than the blue :D Sounds like it moves really smoothly too. Interesting to see your Y-stepper at the back rather than front.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 19:30*

Very nice. Where do you plan to place the second mirror? Are you sharing your part design? 


---
**Kostas Filosofou** *December 11, 2016 19:34*

**+Yuusuf Sallahuddin**  is Orange! and Yes indeed looks way better than the blue...




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 19:36*

**+Konstantinos Filosofou** I didn't notice the lack of 2nd mirror that **+Ariel Yahni** noticed. I'm imagining you will have some mount floating to the right of the x-stepper?


---
**Kostas Filosofou** *December 11, 2016 19:36*

**+Ariel Yahni**  **+Yuusuf Sallahuddin** I place it above the motor .. Yes after i finish i share the parts in thingiverse or in the Grabcad maybe .. 


---
**Kelly S** *December 11, 2016 21:10*

**+Konstantinos Filosofou** I love this, I must must must  do this to mine.  Please please please wrote up the parts required and maybe a simple setup but looks pretty self explanatory.   :D


---
**Kostas Filosofou** *December 11, 2016 21:13*

**+Kelly S** Still work in progress :) ... After i finish i publish the parts in thingiverse or in Grabcad with BOM ... Stay Tuned.


---
**Kelly S** *December 11, 2016 21:17*

Will do.  Already did all my other mods, smoothie, light objects head and air, aligning lasers this is the next logical upgrade.  :)  have you tried firing yet at the furthest distance?  Nevermind just noticed it is a mirror off yet, anyway great work hope it finishes smoothly. 


---
**Kostas Filosofou** *December 11, 2016 21:27*

**+Kelly S** Next job is to install the mirrors and align them ...I hope all works good. I make some rough test with paper tape and seems that the axis is quite straight.


---
**Kelly S** *December 11, 2016 21:53*

**+Konstantinos Filosofou** that is awesome.  So looking forward to seeing how you did it and what was used.  Was thinking a while back I wanted to to something like this when I first got it.  


---
**Antonio Garcia** *December 11, 2016 23:12*

Come on guys!!!! please don´t do more upgrades!!!! now i´ve just finished my last upgrades, and now i´m thinking in the next ones!!!! this machines is a project itself hahaha cut or engrave something is the tip of the iceberg...

**+Konstantinos Filosofou** well done!!! i hope you release those plans soon  :)


---
**Kostas Filosofou** *December 11, 2016 23:30*

Here some photos from the build [photos.google.com - Co2 Laser 40w Modification](https://goo.gl/photos/Yg9YypmkULTCeg3ZA)


---
**Jérémie Tarot** *December 12, 2016 08:21*

With the files and bom to come, surely a reference build ! 👍


---
**Kelly S** *December 13, 2016 04:13*

Curious are you going to raise it at all, so it can to thicker materials, or leave it as is?  


---
**Kelly S** *December 13, 2016 04:14*

Or are my eyes playing tricks and that is the same height it already was?  o___O


---
**János Vilmos** *December 13, 2016 10:54*

Pretty Nice! :)



I'm considering sg similar with my machine, but with built in Z adjustment - same colour btw.




---
**Kostas Filosofou** *December 13, 2016 11:37*

**+Kelly S** With the grid on i can engrave to 6cm without the grid i have a lot of space until the bottom of the casing .. and I will also have adjustable head


---
**Kelly S** *December 13, 2016 14:39*

Great update.  Yep I gotta do this to mine lol.


---
**Kelly S** *December 15, 2016 22:03*

Any updates to this?  


---
**Kostas Filosofou** *December 16, 2016 06:39*

**+Kelly S** Still strangling with the lens mounts .. I believe that this weekend I will have finished


---
**Kelly S** *December 16, 2016 14:32*

**+Konstantinos Filosofou** wonderful, I hope to hear good news and i am able to order soon and your build so far seems to be the best yet.  Look forward to your parts list.


---
**Kelly S** *December 17, 2016 04:47*

Looking at the video again I am going to guess the struggle is with the mirror on top the stepper it is too high to direct to the head?


---
**Kostas Filosofou** *December 17, 2016 05:41*

**+Kelly S** Yes exactly .. But i finally manage to adjust it ! Final step the Laser head...


---
**Kelly S** *December 17, 2016 14:57*

Looking good.  :)  


---
**Kelly S** *December 20, 2016 22:07*

Bump, any updates?  :)


---
**Kelly S** *December 26, 2016 22:45*

**+Konstantinos Filosofou** any updates to this?  Maybe a BOM?


---
**Kostas Filosofou** *December 26, 2016 23:19*

Give me a day or two and I publish the BOM and the stl files


---
**Kelly S** *December 27, 2016 00:28*

Thanks!


---
**Kostas Filosofou** *December 30, 2016 09:24*

**+Kelly S**  Check this out [docs.google.com - BOM Co2 Laser 40w Mod](https://docs.google.com/spreadsheets/d/1gDk9LYs5OQN3TCuJoDO_25_NhIbTeQGMYid7GOvC7qo/edit?usp=sharing)



Early BOM .. i add more


---
**Kelly S** *December 30, 2016 19:10*

Thanks **+Konstantinos Filosofou** 


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/iRtYhvvr4nv) &mdash; content and formatting may not be reliable*
