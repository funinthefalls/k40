---
layout: post
title: "Question about corellaser. When I hit the engrave on the upper top right side all the laser icons that control the k40 disappear"
date: April 02, 2016 02:57
category: "Software"
author: "giavonni palombo"
---
Question about corellaser. When I hit the engrave on the upper top right side all the laser icons that control the k40 disappear. Is this normal?  Now  if I want to do another part I have to close corellaser and reopen to get the laser controls back. What am I missing? 





**"giavonni palombo"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 06:37*

This happens for me regularly also. But if you check down the bottom right corner near the clock on the taskbar, there will be an icon there for the CorelLaser plugin. You can right click on it to add things to the task queue or start tasks etc. It's annoying having to do it that way, but not much that can be done about it really.


---
**giavonni palombo** *April 03, 2016 01:50*

Found that, thank you !


---
**Tom Spaulding** *April 03, 2016 14:39*

Is this a windows 8/10 thing? My windows 7 laptop doesn't have the issue, but Microsoft keeps trying to force the update to 10 on me :(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2016 15:22*

**+Tom Spaulding** I can't be certain, as I have only been running windows 10 since I got the K40. And it pretty much has been doing it to me since I got it. I figured it probably is a Win 10 thing, since it is not supposed to be compatible with Win 10.


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/KCGozQbfP1G) &mdash; content and formatting may not be reliable*
