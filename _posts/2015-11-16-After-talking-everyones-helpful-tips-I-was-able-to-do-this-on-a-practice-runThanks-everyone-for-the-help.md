---
layout: post
title: "After talking everyone's helpful tips, I was able to do this on a practice run....Thanks everyone for the help!"
date: November 16, 2015 15:37
category: "Discussion"
author: "Scott Thorne"
---
After talking everyone's helpful tips, I was able to do this on a practice run....Thanks everyone for the help!

![images/7b5c686fdfc0b05dfe827ab524c15612.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b5c686fdfc0b05dfe827ab524c15612.jpeg)



**"Scott Thorne"**

---
---
**David Cook** *November 16, 2015 16:27*

looks good Scott


---
**Scott Thorne** *November 16, 2015 16:33*

Thanks David.


---
**Anthony Coafield** *November 17, 2015 04:08*

Nice work.


---
**Scott Thorne** *November 17, 2015 04:13*

Thanks


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/fSWe5H6hVW3) &mdash; content and formatting may not be reliable*
