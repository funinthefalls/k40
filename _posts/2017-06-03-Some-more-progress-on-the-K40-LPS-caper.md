---
layout: post
title: "Some more progress on the K40 LPS caper"
date: June 03, 2017 20:55
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
Some more progress on the K40 LPS caper.



Thanks again to **+Phillip Conroy** for shipping parts from down under.



Key take away's from recent work including the post linked below:

.... we now have a rough idea what is in a flyback transformer. Yah I know.."so what"?

.... my first scope measurements inside the LPS show internal PWM period of about 400 us. 



More interesting info about fly-backs in the post.







<b>Originally shared by Don Kleinschnitz Jr.</b>



<b>K40-Flyback Autopsy</b>

Background The High Voltage Flyback Transformer (HVFT) is the business end of the K40 LPS. To date we have little knowledge of what is inside that "potted" assembly. That is about to change as the result of the communities contributions to the HV lab and th...





**"Don Kleinschnitz Jr."**

---
---
**Don Kleinschnitz Jr.** *June 03, 2017 21:00*

this is out put with only 12V drive!

What do you think 400V drive looks like ..... yikes :)!

![images/a88bdf41c06ec261e2897c69efbbd7c4](https://gitlab.com/funinthefalls/k40/raw/master/images/a88bdf41c06ec261e2897c69efbbd7c4)


---
**Ned Hill** *June 04, 2017 16:35*

Interesting work.  Usually when I'm trying determine chemical compatibility for an unknown material I'll put small pieces of the material in test tubes, or other small glass containers.  Then add different solvents and see how they behave.  Do they start dissolving (hold it up to the light and look for a refractive index gradient from the material surface) or does it soften the material as it seeps in.  I would try acetone, mineral spirits and lacquer thinner.  Those will give you a nice range of polarities.  


---
**Don Kleinschnitz Jr.** *June 04, 2017 16:50*

**+Ned Hill** I tried all three, nothing touches it :( :(.


---
**Ned Hill** *June 04, 2017 17:12*

Yeah it can be tough with some plastics.  It's probably going to be some kind of thermoplastic.  Depending on the type it's possible it can be attacked by a strong acid like muratic (HCl) with some heating.  Sulfuric would be better but that will react with metal components. It maybe possible to narrow down the type of plastic using a burn test.  [boedeker.com - Boedeker Plastics : How to Identify Plastic Materials Using The Burn Test](http://www.boedeker.com/burntest.htm)


---
**Steve Clark** *June 04, 2017 23:37*

Epoxies tend to act like what you are describing when heated past their temperature rating. When I was working in the aerospace industry some of the epoxy products could be softened using Jasco Gjbp00203 Premium Paint & Epoxy Remover. That might be something to try… as I recall the items were coated with the jel , covered with a damp cloth, then plastic wrap to keep the remover from drying out. They were left overnight and whatever was softened was scraped away then recoated  again as needed. Gloves were of course used.


---
**Don Kleinschnitz Jr.** *June 05, 2017 04:00*

**+Ned Hill** what do you think. I am thinking Phenolic. What would dissolve that?



![images/7516db476dde14595c1a63a95d651487](https://gitlab.com/funinthefalls/k40/raw/master/images/7516db476dde14595c1a63a95d651487)


---
**Ned Hill** *June 05, 2017 04:28*

**+Don Kleinschnitz** Phenolic resins are pretty distinctive by odor.  Does it smell like a toasted, drilled or cut PCB? (smells of phenol with hints of formaldehyde)   Without getting too exotic, Phenolic resins can be attacked by strong bases (caustic solution)  or chlorinated solvents like Dichloromethane (aka Methylene Chloride).  Dichloromethane is common in paint strippers.  


---
**Ned Hill** *June 05, 2017 04:40*

I would try soaking some in chlorinated paint stripper, probably over night.  Should get all gummy.  You could even drill some small holes to increase the surface area to speed it up.


---
**Don Kleinschnitz Jr.** *June 05, 2017 11:18*

**+Ned Hill** yes it does smell like PCB. I liken it to smelling like something electronic is burning. Is there a specific kind/brand of paint stripper?


---
**Ned Hill** *June 05, 2017 12:54*

**+Don Kleinschnitz** Actually forget the paint stripper, I misread a chart.  Looks like a strong basic solution is your best bet to try.  Probably like an aqueous solution of sodium hydroxide (lye) of  10% (w/v) or maybe higher.


---
**Ned Hill** *June 05, 2017 12:59*

Looks like you can get lye from Amazon for about $12 for 2 lbs.  Just make sure you wear gloves with the solution as it will literally start dissolving your skin.




---
**Ned Hill** *June 05, 2017 13:07*

Oh, you could also try some liquid drain cleaner like Draino if you have some laying around.


---
**Don Kleinschnitz Jr.** *June 05, 2017 13:10*

**+Ned Hill** thanks will try.




---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/596rdXH327C) &mdash; content and formatting may not be reliable*
