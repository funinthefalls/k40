---
layout: post
title: "I'm looking for Water Flow sensor and the pulse receiver electronics to go with it to interrupt the laser beam..."
date: April 22, 2017 22:56
category: "Modification"
author: "Steve Clark"
---
I'm looking for Water Flow sensor and the pulse receiver electronics to go with it to interrupt the laser beam... or? 









**"Steve Clark"**

---
---
**Romans Pikarevskis** *April 22, 2017 23:46*

Welkome to arduino




---
**Ned Hill** *April 23, 2017 00:14*

You could go that route or just wire a simple on/off flow sensor into the laser enable wire.


---
**Steven Kalmar** *April 23, 2017 01:23*

I'm going the Arduino path with one of these - [ebay.com - Details about  1/2" Hall Effect Flowmeter Control Water Flow Sensor for Arduino *REAL US SELLER](http://www.ebay.com/itm/181562348669?_trksid=p2057872.m2749.l2649&ssPageName=STRK:MEBIDX:IT)




---
**Steve Clark** *April 23, 2017 01:24*

So just ignore the signal wire and run it in series thru the disable switch... Makes sense. Thks. The only reason I was thinking of it was maybe being able to come up with pulses per liter info on a digital readout. It would be nice to have when comparing flow to cooling to ambient temp. to duty cycle.  


---
**Don Kleinschnitz Jr.** *April 23, 2017 10:31*

**+Steve Clark**  this method has been working for me for some time.

Knowing the flow rate might be "cool" :). 



[donsthings.blogspot.com - Laser Tube: Protecting, Operating, Cooling & Repairing](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
**Adrian Godwin** *April 23, 2017 11:57*

I much prefer a rotating sensor like the one Steven refers to. I noted elsewhere a failure of the moving-flap type due to an obstruction that made it stick open.


---
**Don Kleinschnitz Jr.** *April 23, 2017 15:25*

**+Adrian Godwin** in my system setup if the temp gets too high it shuts down the laser independent of water flow sensor :).


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/LkAJPmXbvw7) &mdash; content and formatting may not be reliable*
