---
layout: post
title: "Hi all, no one has ever thought about how to add an hours meter for monitoring the laser life?"
date: October 19, 2016 11:20
category: "Discussion"
author: "Walter White"
---
Hi all, no one has ever thought about how to add an hours meter for monitoring the laser life? Hours intended as laser on, not machine power on.





**"Walter White"**

---
---
**Scott Marshall** *October 19, 2016 14:20*

I have one all designed and it's an air assist driver too. I even stocked in the parts, but never ordered the boards because the  guy that asked me for it disappeared and I got busy.



If you're interested I'll dig out the stuff (been meaning to to that anyway). I also heard someone say they were going to do a software version, but I don't think that went anywhere.



You might be able to find a meter that will work on the 5v LO signal, but most are 12v and the firing logic is inverted, so it' not a simple hookup. It's best done with buffering circuitry which is why I added the air assist feature.(it even has adjustable post flow and will drive a pump or solenoid valve whichever you need)



 My board drives a standard 12v hour meter only when the laser is firing (and adjusts for PWM use).



If you'd like to beta test it, I'll make you a great deal on it. It's plug and play (except for mounting the hour meter)



Scott


---
**Walter White** *October 19, 2016 14:53*

Thanks **+Scott Marshall** 

Can you give me more information about your board with hour meter?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 20, 2016 04:46*

The laser on time has been added into LW for software version. Not 100% sure if it works as "tube on" time or what, but it does vaguely keep track of job times & accumulates over time.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 20, 2016 04:47*

The laser on time has been added into LW for software version. Not 100% sure if it works as "tube on" time or what, but it does vaguely keep track of job times & accumulates over time.


---
**Scott Marshall** *October 20, 2016 09:33*

**+Walter White** 

I don't have any pictures or anything on it, but it's a circuit that monitors the Laser fire signal, and buffers it thru an into a 5v logic signal. The signal then goes thru a short period 'off delay' to prevent short cycling and is split. one side gets amplified to 12v and drives a conventional hour meter, and the other side goes to an adjustable post-flow timer/longer anti short cycle to drive an optional air assist relay. The relay is driven with an opto isolator  for safety (that isolates any line voltage from the low level control lines). The air assist will have an Off-On-Auto switch and a post flow time setting

The relay can be jumper selected to drive a 120/220volt air pump or to operate a DC (or line voltage solenoid valve to control a compressed air supply).



I'm planning on offering it as u build kit for people who wish to build it themselves (anyone with reasonable soldering skills will be able to build one) or as a finished unit ready to plug int the laser. The hardest part of the install would be cutting the opening for the hourmeter. You won't necessarily have to mount the hourmeter on the outside of the enclosure, it can mounted internally where you can see it and that would be adequate to check tube hours when you want to.



The Plug and Play unit will be available with a AC receptacle so that your air pump will  just plug in, as will the rest of it



If you're just looking for the hour meter, only the required components can be installed, keeping the price down, and you could add them later  if desired.



That's the basic plan. I need to look it over and maybe tweak it a bit, but it's essentially ready to produce. I've been meaning to do so anyhow, so I will put together a prototype run of 3. (if there's interest, I'll add them to the product line, if not, I'll leave them as a special order item)They'll probably take about 2 weeks to have ready and I still need to work out pricing, but I'm shooting for $20 on the hour meter only and $30-35 for the air assist. Less in kit form. 



Scott


---
*Imported from [Google+](https://plus.google.com/118017715722776268014/posts/9t8Zvy74s2e) &mdash; content and formatting may not be reliable*
