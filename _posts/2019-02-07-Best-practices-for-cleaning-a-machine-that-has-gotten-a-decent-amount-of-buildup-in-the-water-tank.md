---
layout: post
title: "Best practices for cleaning a machine that has gotten a decent amount of buildup in the water tank?"
date: February 07, 2019 03:27
category: "Discussion"
author: "'Akai' Coit"
---
Best practices for cleaning a machine that has gotten a decent amount of buildup in the water tank?





**"'Akai' Coit"**

---
---
**Don Kleinschnitz Jr.** *February 07, 2019 11:09*

In the past after cleaning the tank, I flushed the system by circulating white vinegar for a few hrs. Flush it multiple times with clean water after the vinegar.


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/fQsQMWwazJe) &mdash; content and formatting may not be reliable*
