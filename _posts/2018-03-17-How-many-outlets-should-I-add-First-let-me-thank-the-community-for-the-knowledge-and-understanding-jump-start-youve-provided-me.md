---
layout: post
title: "How many outlets should I add? First let me thank the community for the knowledge and understanding jump-start you've provided me"
date: March 17, 2018 04:07
category: "Modification"
author: "Tom Traband"
---
How many outlets should I add?



First let me thank the community for the knowledge and understanding jump-start you've provided me.



My K40 arrived today and I've started the demolition by removing the exhaust box (bolted not welded in this case), work clamp/platform and related standoffs to make room for an eventual z-table. Next up is lid safety interconnects, then plumbing the exhaust out of the basement shop.



I'm not impressed with the 2 panel-mount outlets on the back and am considering replacing them with grounded outlets where the hot line is controlled by relays from the (due any day now) grbl shield. I've got a 4-relay board ready to roll.



My question is how many outlets should I plan for? I see immediate use for three (water pump, exhaust fan, aquarium pump air assist), but the outlets come in pairs so if I need three I'm going to end up installing four. I also don't know how many pins are open on the arduino/grbl shield, and what other buttons I may want (e.g. is a hardware pause/resume useful or should I rely on the gcode sender for that?)



Assuming the multiple hots split off the main power plug won't have any negative impact on the laser power supply, would you recommend I relay-control all 4 outlets, or wire one always hot (and labelled as such)?





**"Tom Traband"**

---
---
**Joe Alexander** *March 17, 2018 04:21*

hardwire the water pump as it should be on whenever the machine is on and not controlled independently. then use a gland pass-through fitting for the wire to the pump.


---
**Don Kleinschnitz Jr.** *March 17, 2018 11:08*

I do not see how adding outlets will uniquely impact the laser power supply. 



You must insure the capacity of the cord, plug and fuse in the main outlet for the additional AC load it has to handle.



I use a 4 relay board and 5 outlets as seen in this schematic on the _Power Control* tab:



[https://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/](https://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)



The relays can be controlled from my Smoothie (some day) and 4 control panel switches. 



<b>Relay control:</b>

...Air assist

...Exhaust Air

...Spare 1

...Spare 2

<b>Hard wired:</b>

...Water pump (hard wired plug not controlled by relay)



I elected to make my K40 controller modular and mechanically  independent from the K40. That is because, someday I plan to build a 60 watt from scratch and will just move the controller module intact. I can also connect the relay to some form of digital control later if I want.



In this video you can see how it all goes together.



[photos.app.goo.gl - photos.app.goo.gl/4LR0gfflHGuy4rLw1](https://photos.app.goo.gl/4LR0gfflHGuy4rLw1)


---
**Tom Traband** *March 17, 2018 15:21*

With the water pump hard-wired then it looks like 2 outlets will be enough. I'm still going to replace the existing sockets with standard grounded outlets.


---
**Don Kleinschnitz Jr.** *March 17, 2018 16:06*

**+Tom Traband** yes grounded outlets are good. While you are in there check all the other grounds they are usually lousy!


---
**Adrian Godwin** *March 18, 2018 22:43*

The US outlets on mine terrify me (and I'm generally pretty blase about mains due to long experience). They were mounted so that if they fall out a little way, a bit of metal dropped over the back of the cutter would short the power out. 



I realise the US outlets have this problem by design (and putting the ground at the top helps) but given how much more likely that accidental bit of metal is on something like this (a ruler, a bit of mirror acrylic ..) I'm going to replace mine with decent IEC outlets. No danger of shorts and a nice positive latch.



UK style 13A outlets might be a bit better but should really be separately fused.





 


---
**Tom Traband** *March 19, 2018 05:13*

The panel mounted one's looked to be a hybrid that would accept pin or blade style plugs, but they didn't really hug the blades, if you know what I mean. They're gone now.



I've mounted a pair of US grounded duplex outlets (so 4 outlets total). Two are hot whenever the machine is turned on (water pump and an auxiliary for future use) and the other two will be controlled by relays (air assist and exhaust fan) once my grbl conversion is complete.


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/i1AoJiEebhA) &mdash; content and formatting may not be reliable*
