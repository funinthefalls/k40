---
layout: post
title: "Laser etched dice! I cut a little holder to help with alignment"
date: June 26, 2016 21:03
category: "Object produced with laser"
author: "Tev Kaber"
---
Laser etched dice!  I cut a little holder to help with alignment. Since there are 6 sides, I can set the job to repeat 6 times, before continuing each job, I can rotate each die and move it to the next slot. So after 6 passes, I have 6 complete custom dice!



Settings: 10mA, 320mm/s.



Blank dice on Amazon

White: [https://www.amazon.com/gp/product/B00LW5CBL8/](https://www.amazon.com/gp/product/B00LW5CBL8/)

Color: [https://www.amazon.com/gp/product/B00BNWGVDO/](https://www.amazon.com/gp/product/B00BNWGVDO/)





![images/7bab4917db43d36a378ca21762455c6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7bab4917db43d36a378ca21762455c6a.jpeg)
![images/6f6c8a5b360a9b70ef7d1bac19809827.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6f6c8a5b360a9b70ef7d1bac19809827.jpeg)
![images/b26c8ae321e6964b8b4a7ec6dbda81a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b26c8ae321e6964b8b4a7ec6dbda81a2.jpeg)
![images/f3dd107d4762f9b6cb9f83a09e801414.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3dd107d4762f9b6cb9f83a09e801414.jpeg)
![images/0ed038bfe35e33becdb30b32157f7bc6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0ed038bfe35e33becdb30b32157f7bc6.jpeg)
![images/d5dd6b478d180e290c68f6b7fdf9e9f0.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d5dd6b478d180e290c68f6b7fdf9e9f0.png)

**"Tev Kaber"**

---
---
**Tev Kaber** *June 26, 2016 21:06*

Also, if I want to make less than 6 dice, I have little stone inserts to take the hit in the unused slots. Still takes 6 passes.


---
**Jim Hatch** *June 26, 2016 22:30*

Looks very nice. I'd set it up so there are 6 of each side in the design file and send the file - does the whole lot of them. Then rotate the dice and do the next side on all of them. No repeats, just 6 sends. I'd have all 6 sides as layers in Corel and then send 1 layer at a time. 



I really like these and will probably do custom dice for a Cataan board I'm making.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 22:38*

Well done on the alignment. Dice look great.



If you wish to do less than 6 dice, you will be wasting CO2 from the tube every time you fire at the stone inserts. You are better off creating more copies of your 6 icons that omit the number of dice you are not wanting to do.



E.g. supposing A|B|C|D|E|F represent your icons, you could create as follows to make just 4 dice. / represents a blank image. 1, 2, 3, 4 represent each blank dice.



...A B C D / /

...1 2 3 4

.../ B C D E /

......1 2 3 4

.../ / C D E F

........1 2 3 4

...A / / D E F

...4.......1 2 3

...A B / / E F

...3 4.......1 2

...A B C / / F

...2 3 4.......1



So you would have 6 layers, first layer with icons A B C D active & icons E F hidden. Each successive layer will deactivate different icons, as you will be moving the blank dice (1, 2, 3 & 4) one to the right every time & rotating to get the next side done.



You would still be doing 6 passes, but wasting less of your tube's CO2. Hope that makes sense.



edit: grr my spaces I put in the diagram were not displaying correctly to align the numbers where I wanted so I added . instead of spaces. Ignore them.


---
**Tev Kaber** *June 28, 2016 19:44*

Good suggestions! 



I was doing the "repeat 6 times" for convenience, but queuing up 6 tasks with different layers active, while a few extra steps, would be more efficient. Another subtle difference is that way all the dice would have the same faces on the same sides.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/c82jwWHyKTX) &mdash; content and formatting may not be reliable*
