---
layout: post
title: "How can I do dove tail joints in laser draw"
date: June 07, 2016 17:32
category: "Discussion"
author: "3D Laser"
---
How can I do dove tail joints in laser draw





**"3D Laser"**

---
---
**Ariel Yahni (UniKpty)** *June 07, 2016 20:38*

You want something parametric? Drawing seems easy


---
**Ulf Stahmer** *June 07, 2016 22:24*

You could try this [https://github.com/hellerbarde/inkscape-boxmaker](https://github.com/hellerbarde/inkscape-boxmaker) and export the inkscape file as an emf and load it into LaserDRW.  Haven't tried it, but in theory, it should work.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/Ws6LY1EiHxd) &mdash; content and formatting may not be reliable*
