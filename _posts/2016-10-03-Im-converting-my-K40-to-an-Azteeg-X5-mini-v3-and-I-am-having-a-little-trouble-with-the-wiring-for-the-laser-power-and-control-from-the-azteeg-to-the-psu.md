---
layout: post
title: "I'm converting my K40 to an Azteeg X5 mini v3 and I am having a little trouble with the wiring for the laser power and control from the azteeg to the psu"
date: October 03, 2016 21:03
category: "Smoothieboard Modification"
author: "kyle clement"
---
I'm converting my K40 to an Azteeg X5 mini v3 and I am having a little trouble with the wiring for the laser power and control from the azteeg to the psu.  Could anyone give me a hand on the wiring diagrams?





**"kyle clement"**

---
---
**Ariel Yahni (UniKpty)** *October 03, 2016 21:59*

**+kyle clement** Do you have a Level shifter? 


---
**Don Kleinschnitz Jr.** *October 03, 2016 23:18*

Here is a schematic of my smoothie conversion which may help. Note I am not using a level shifter, pwm to L on laser supply. 

[https://plus.google.com/113684285877323403487/posts/66DXUBsGqUK](https://plus.google.com/113684285877323403487/posts/66DXUBsGqUK)


---
**Ariel Yahni (UniKpty)** *October 03, 2016 23:26*

**+Don Kleinschnitz** have you fired your laser yet? Interested in the result of not using the shifter


---
**Don Kleinschnitz Jr.** *October 03, 2016 23:31*

**+Ariel Yahni** not yet, but there are a number of folks on here who have this config working with the right config file. Days away from my own verification.


---
**Ariel Yahni (UniKpty)** *October 03, 2016 23:33*

mmm Original smoothie without level shifter? not sure about that but maybe im wrong. What im sure is the one cable setup, L from the PSU


---
**Don Kleinschnitz Jr.** *October 04, 2016 00:08*

Actually although I have not tested it with the smoothie I have with a pwm circuit directly into "L" with no smoothie.


---
**Don Kleinschnitz Jr.** *October 04, 2016 00:09*

**+Ariel Yahni**​ not sure I understand your

 one cable with L meaning. I have one cable to L from smoothie open drain, no other cables.


---
**kyle clement** *October 04, 2016 01:45*

i haven't ordered the level shifter yet but was planning to


---
*Imported from [Google+](https://plus.google.com/106211725666880587072/posts/HrYLYW7JFLD) &mdash; content and formatting may not be reliable*
