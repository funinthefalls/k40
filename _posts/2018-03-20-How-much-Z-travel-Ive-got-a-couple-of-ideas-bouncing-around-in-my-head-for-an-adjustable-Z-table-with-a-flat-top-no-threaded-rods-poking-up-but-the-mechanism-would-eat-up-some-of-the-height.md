---
layout: post
title: "How much Z travel? I've got a couple of ideas bouncing around in my head for an adjustable Z table with a flat top (no threaded rods poking up) but the mechanism would eat up some of the height"
date: March 20, 2018 17:20
category: "Modification"
author: "Tom Traband"
---
How much Z travel?



I've got a couple of ideas bouncing around in my head for an adjustable Z table with a flat top (no threaded rods poking up) but the mechanism would eat up some of the height.



What amount of Z travel do folks find useful? I'm primarily expecting to compensate for different thicknesses of sheet goods. I'd remove the Z table for larger objects or to place a rotary attachment.





**"Tom Traband"**

---
---
**BEN 3D** *March 20, 2018 17:58*

I cut a hole into the button for the Z Table, so i could put a chest or some similar in it.


{% include youtubePlayer.html id="dCD0nsWNDhk" %}
[youtube.com - I cut a hole in the metal botton of my k40 china laser.](https://youtu.be/dCD0nsWNDhk)


---
**Ned Hill** *March 20, 2018 18:57*

For most things you typically only need up to 25mm (1") of travel.  This covers cutting / engraving on 3mm to 6mm sheet goods and Engraving on 25mm boards.  50mm would be even better.  To be sure I also do things that range all the way down to the floor of the laser for engraving large objects.  But 0-25mm covers 90% of things I work on. 


---
**Don Kleinschnitz Jr.** *March 20, 2018 19:05*

...for sheet goods you only need the thickness of the material?

<s>----</s>

This design does not use an adjustable table, rather it clamps the sheet goods below the focal point. 



**+Scorch Works** came up with the idea and built one and this design was derived from that. I have all the parts but its in que to build.





[donsthings.blogspot.com - K40 Clamping Table](http://donsthings.blogspot.com/2017/02/k40-clamping-table.html)




---
**Tom Traband** *March 20, 2018 21:43*

**+Scorch Works** has certainly come up with an elegant concept. Knowing myself and my propensity for pushing to the limit, I expect I'll eventually want to try multiple passes to cut thicker materials, so I'll want to lower the table height without shifting the position of the material in the X or Y directions. I promise to share the idea as soon as I get a model pulled together.


---
**HalfNormal** *March 21, 2018 02:37*

There have been a few scissor lifts tried but as you point out, there is not much room for adjustment. It is hard to have our cake and eat it too!


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/TumJFH6UMqp) &mdash; content and formatting may not be reliable*
