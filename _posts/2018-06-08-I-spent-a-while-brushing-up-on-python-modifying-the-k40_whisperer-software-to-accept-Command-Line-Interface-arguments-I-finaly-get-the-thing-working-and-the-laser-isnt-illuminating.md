---
layout: post
title: "I spent a while brushing up on python, modifying the k40_whisperer software to accept Command Line Interface arguments, I finaly get the thing working, and the laser isnt illuminating"
date: June 08, 2018 21:06
category: "Discussion"
author: "Eric Lovejoy"
---
I spent a while brushing up on python, modifying the k40_whisperer software to accept Command Line Interface arguments,  I finaly get the thing working, and the laser isnt illuminating. 



Test button on the PSU, still nothing. Ugh.... hod do i test this thing to make sure the bulb isnt blown?



modified source k40_whisperer python source:

[https://pastebin.com/ktUaLVhQ](https://pastebin.com/ktUaLVhQ)





**"Eric Lovejoy"**

---
---
**Don Kleinschnitz Jr.** *June 08, 2018 22:33*

Try this guide and let me know if it helped.

[https://drive.google.com/file/d/1yAGfqw3VVKW91qEpCVFOuAwYoyI7PPuh/view?usp=drivesdk](https://drive.google.com/file/d/1yAGfqw3VVKW91qEpCVFOuAwYoyI7PPuh/view?usp=drivesdk)

[drive.google.com - Troubleshooting A K40 Laser Power Subsystem 1.0.pdf](https://drive.google.com/file/d/1yAGfqw3VVKW91qEpCVFOuAwYoyI7PPuh/view?usp=drivesdk)


---
**Joe Alexander** *June 08, 2018 22:52*

if the test button on the laser PSU doesnt work then linking the code does no good. Check your wiring and verify that the protection loop is complete and try test firing again. Was it working prior to your tweaks? exactly what did you change?


---
**Don Kleinschnitz Jr.** *June 08, 2018 22:56*

Test button on LPS bypasses the interlock loop. If it doesn't fire with test button something is wrong at the supply. Guide should cover that.


---
**Eric Lovejoy** *June 15, 2018 10:45*

it turns out I still have som faulty wiring. the wire harness on mine has been troublesome.

but i have it figured out and working.... with cli. now to make a web interface! 


---
*Imported from [Google+](https://plus.google.com/107285166924069729045/posts/eMdbvaAAqv3) &mdash; content and formatting may not be reliable*
