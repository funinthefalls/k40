---
layout: post
title: "when lasering the back of a mirror I assume you go through the backing completely"
date: August 26, 2016 02:50
category: "Discussion"
author: "David Spencer"
---
when lasering the back of a mirror I assume you go through the backing completely. yes or no





**"David Spencer"**

---
---
**Alex Krause** *August 26, 2016 02:56*

Yep


---
**Gee Willikers** *August 26, 2016 03:00*

Yes. The glass should end up with an even frost.


---
**Alex Krause** *August 26, 2016 03:01*

I like to paint the mirror backing after engrave makes the image pop


---
**David Spencer** *August 26, 2016 17:58*

Thanks


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/VDRyTArPYfb) &mdash; content and formatting may not be reliable*
