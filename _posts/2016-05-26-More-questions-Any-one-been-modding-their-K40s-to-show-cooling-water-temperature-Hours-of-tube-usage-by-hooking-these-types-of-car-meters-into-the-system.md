---
layout: post
title: "More questions ;) Any one been modding their K40s to show cooling water temperature & Hours of tube usage by hooking these types of car meters into the system?"
date: May 26, 2016 10:56
category: "Modification"
author: "Purple Orange"
---
More questions ;)



Any one been modding their K40s to show cooling water temperature & Hours of tube usage by hooking these types of car meters into the system?



![images/0928919b7a8d7711b3e15356c0d1ebd4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0928919b7a8d7711b3e15356c0d1ebd4.jpeg)
![images/3f32591c34f04846c72fcd3d71c4aae7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f32591c34f04846c72fcd3d71c4aae7.jpeg)

**"Purple Orange"**

---
---
**Anthony Bolgar** *May 28, 2016 19:56*

I was thinking of adding an hour meter to mine.


---
*Imported from [Google+](https://plus.google.com/112469922804049225807/posts/M7uX9Sw5tKm) &mdash; content and formatting may not be reliable*
