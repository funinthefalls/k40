---
layout: post
title: "ED Carty ...check this out"
date: February 14, 2016 14:35
category: "Discussion"
author: "Scott Thorne"
---
**+ED Carty**​...check this out.





**"Scott Thorne"**

---
---
**Anton Fosselius** *February 14, 2016 15:23*

I use LibreCad to do this ;)


---
**Scott Thorne** *February 14, 2016 15:27*

I've never even heard of it....is it for Windows...and is it easy to use like inkscape?


---
**ED Carty** *February 14, 2016 16:04*

its for Linux. (Ubuntu) I haven't loaded it yet


---
**Scott Thorne** *February 14, 2016 16:22*

Oh ok....it's useless to be then...but check out the video **+ED Carty**


---
**Anton Fosselius** *February 14, 2016 16:59*

Librecad is for Windows,  Mac and Linux. See [http://librecad.org/cms/home.html](http://librecad.org/cms/home.html)


---
**ED Carty** *February 14, 2016 19:14*

Inkscape ROCKS. I have already followed the video and made  box. nice


---
**Scott Thorne** *February 14, 2016 20:04*

Great....I'll post some of the shadow box I made for my wife today...it's 12x6 x4....it turned it great.


---
**Joseph Midjette Sr** *February 14, 2016 21:43*

Pretty cool! Thanks!


---
**Scott Thorne** *February 14, 2016 22:23*

 Anytime.


---
**ED Carty** *February 15, 2016 00:58*

Check this out




{% include youtubePlayer.html id="arjRtCjI9AQ" %}
[https://www.youtube.com/watch?v=arjRtCjI9AQ](https://www.youtube.com/watch?v=arjRtCjI9AQ) 


---
**Scott Thorne** *February 15, 2016 10:18*

Cool


---
**ED Carty** *February 16, 2016 21:22*

scott thorne it has come a long way


---
**Scott Thorne** *February 17, 2016 10:21*

**+ED Carty**...I've just started using it about a month ago...so I'm new to this...Lol


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/GopAj4WYASj) &mdash; content and formatting may not be reliable*
