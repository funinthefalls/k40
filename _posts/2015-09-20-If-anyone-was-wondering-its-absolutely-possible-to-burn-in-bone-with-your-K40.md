---
layout: post
title: "If anyone was wondering it's absolutely possible to burn in bone with your K40"
date: September 20, 2015 16:16
category: "Materials and settings"
author: "Niclas Jansson"
---
If anyone was wondering it's absolutely possible to burn in  bone with your K40.

I'll do a necklace out of this one :)



![images/eb032b8cd561edc9b556f40b25f2ee78.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb032b8cd561edc9b556f40b25f2ee78.jpeg)
![images/f9247d75f5c6250ac01684fcf6b94ed8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9247d75f5c6250ac01684fcf6b94ed8.jpeg)
![images/5208cd3c50f21276990b618bec1dd6ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5208cd3c50f21276990b618bec1dd6ce.jpeg)
![images/48c6309c9fe6e7e884152d40306064aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/48c6309c9fe6e7e884152d40306064aa.jpeg)
![images/1a15346227fa0bd0100e3f8b10dd7b2a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1a15346227fa0bd0100e3f8b10dd7b2a.jpeg)

**"Niclas Jansson"**

---
---
**Fadi Kahhaleh** *September 21, 2015 01:54*

nice.. but where did you get BONE?


---
**Niclas Jansson** *September 21, 2015 06:44*

**+Fadi Kahhaleh** I get mine at the local butcher. There is also the pet store, they have marrow bones for dogs, at least here in Sweden. If it is not obvious, this is cow bone, not human ;)


---
**Fadi Kahhaleh** *September 21, 2015 14:38*

**+niclas Jansson** Ok that makes sense... I have never thought about it like that.. perhaps here in the US you can also get bones in the butcher store.. I have no idea though, I never asked lol


---
**Scott Marshall** *December 18, 2015 17:07*

I'll bet it calls for a good fan. It has to smell awful. Nice results though!



Scott


---
*Imported from [Google+](https://plus.google.com/105892779133697678832/posts/EX2HA4HcKzd) &mdash; content and formatting may not be reliable*
