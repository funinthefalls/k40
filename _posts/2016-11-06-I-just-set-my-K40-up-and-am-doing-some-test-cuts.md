---
layout: post
title: "I just set my K40 up and am doing some test cuts"
date: November 06, 2016 01:45
category: "Original software and hardware issues"
author: "Kelly Queener"
---
I just set my K40 up and am doing some test cuts.  Everything comes out half as big as I draw it.  1 inch squares are 0.5", 2" squares are 1".  Is there a setting I'm missing?  



Thanks in advance....









**"Kelly Queener"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 06, 2016 07:36*

Look into setting the Board Model & Board ID in the settings for Corel Laser or LaserDRW. When they're incorrect (which is default) strange things can happen.


---
**Kelly Queener** *November 06, 2016 19:15*

My settings match the pcb silk screen for the model and ID setting matches the sticker.   Will it run if a different model is selected?  I can try running through the different models...



Model number for me is 6c6879-laser-m2, FW is 2016.01.01 if that helps at all.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2016 02:08*

**+Kelly Queener** Next thing I can think is this:



[https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)


---
**George Fetters** *November 07, 2016 18:26*

You can set the steps per mm for alpha and beta in your config file.  I would just adjust that up by a factor of 2. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2016 21:43*

**+George Fetters** I think Kelly is running stock setup. I don't recall a steps per mm in the stock software options.


---
*Imported from [Google+](https://plus.google.com/105979760956302527849/posts/MaMn9oLMJkz) &mdash; content and formatting may not be reliable*
