---
layout: post
title: "Hi to all! anybody had change the controller to a different one in order to run LIGHTBURN?"
date: June 13, 2018 22:37
category: "Modification"
author: "Mario Gayoso"
---
Hi to all! anybody had change the controller to a different one in order to run LIGHTBURN? Please let me know!



Also, where do you have your machine installed at? any setup photos? :)



Thanks in advance!





**"Mario Gayoso"**

---
---
**HalfNormal** *June 13, 2018 22:50*

The only  alternative software that can be used with the stock controller is K40 Whisperer


---
**Adam Hied** *June 15, 2018 00:08*

Yep did it last night. Got the cohesion 3D and running the Lightburn trial for now. So far It’s great. Granted the machine just got delivered a week ago.


---
**Kiah Connor** *June 15, 2018 04:39*

My Cohesion3d laser board literally got delivered today. SOON.




---
**Mario Gayoso** *June 15, 2018 18:47*

**+HalfNormal** How do you like the software? can you import from Illustrator?




---
**Mario Gayoso** *June 15, 2018 20:13*

**+Kiah Connor** Let me know how does it go! GOOD LUCK!!


---
**Mario Gayoso** *June 15, 2018 20:14*

**+Adam Hied** Better than the original setup?


---
**Adam Hied** *June 15, 2018 20:35*

**+Mario Gayoso Noguerol**  id say it’s a must do upgrade. I honestly decided to not do the original board as soon as I realized I couldn’t set the laser power in software. Lightburn and Cohesion3D is a must have upgrade I think. Now I got my K40 last Friday added air assist the 18mm lense. Trashed the exhaust that came with and setup a bilge exhaust fan on a speed controller. I also just got some aluminum honeycomb in the mail today. 






---
**Adam Hied** *June 15, 2018 20:37*

First cut on the setup. I still need to fine tune the power and speed, but it’s not too bad

![images/ad325e2428d75c4ef2b897c18c66fd11.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad325e2428d75c4ef2b897c18c66fd11.jpeg)


---
**HalfNormal** *June 15, 2018 21:50*

**+Mario Gayoso Noguerol** If you are not going to change out your controller anytime soon, it is the best to use. You will need to convert your files to SVG format.


---
**Doug LaRue** *June 16, 2018 04:47*

I'm in the process of installing an MKS Sbase v1.3 board. I have an analog K40 so all I needed was a simple adapter cable to break out the 3 pins of the 5pin JST used for end stops, into  2 pairs of jumper connectors. Now I'm looking into the best way to implement the laser control part.


---
**Kiah Connor** *June 16, 2018 05:15*

I might need to change my original statement...



My Cohesion3d laser board got MARKED as delivered yesterday.



But didn't actually reach my mailbox. Time for a lengthy Australia Post dispute process.



:(


---
**Mario Gayoso** *June 17, 2018 03:29*

**+Kiah Connor** Any links for this cohesion 3d laser and for the exhaust upgrade **+Adam Hied**  ? thank you guys for your support!


---
**HalfNormal** *June 17, 2018 03:33*

[http://cohesion3d.com](http://cohesion3d.com)

[cohesion3d.com - Cohesion3D: Powerful Motion Control](http://cohesion3d.com)


---
**Doug LaRue** *June 17, 2018 04:04*

for the K40 laser cutter, the Cohesion3D mini is the one you want.  I've seen people say it's a 30 minute upgrade.




---
**Kiah Connor** *June 18, 2018 10:18*

**+Doug LaRue** is right - I just set mine up in 30 minutes! :D


---
**Doug LaRue** *June 18, 2018 14:41*

Took me a bit longer, quite a bit, to get the MKS SBase working. To keep the option of swapping back the M2, I made a small adapter to split 3 pins from a JST connector into 4 sockets for end stops and then added jumper wires for GND and L connections on the LPS. More researching time than conversion time.

  


---
**Mario Gayoso** *June 20, 2018 07:59*

**+Adam Hied**  It looks awesome!

**+HalfNormal**  Thanks for the Link!! what software you used with this?




---
**HalfNormal** *June 20, 2018 12:34*

LightBurn is what I use. It is not free. Laserweb is free. 


---
**Doug LaRue** *June 20, 2018 14:16*

LightBurn is only $30 though.




---
**Adam Hied** *June 20, 2018 15:50*

**+Mario Gayoso Noguerol** Lightburn is what I’m using. The cohesion took me 10 minutes to install and maybe an hour to find the sd card that came with it.. dropped it somehow behind my desk.


---
**Mario Gayoso** *June 27, 2018 17:33*

**+Adam Hied**  Can you post some pics? I think I will do it! I haven't used my machine yet. Im more focused 3d printing! 




---
**Adam Hied** *June 27, 2018 21:37*

**+Mario Gayoso Noguerol** I’ll take a video tonight and upload it


---
**Adam Hied** *June 28, 2018 02:46*

**+Mario Gayoso Noguerol** my setup so far. I still have some work to do, but I’m cutting parts.


{% include youtubePlayer.html id="KknBUB0AOuY" %}
[youtube.com - K40 Laser Cutter Tour](https://youtu.be/KknBUB0AOuY)


---
**Mario Gayoso** *July 12, 2018 06:18*

**+Kiah Connor** how is cohesion working for you so far? Any other upgrades on your machine?


---
**Kiah Connor** *July 12, 2018 07:06*

Warning: MANY WORDS.



Cohesion3d & Lightburn:

**+Mario Gayoso Noguerol** Fantastically! It's a huge leap in convenience and quality. And it doesn't turn off usb connection every time power fluctuates slightly (i turn on a fan in my office and it used to interupt my jobs)  Lightburn also blew my socks off. I'd consider it worth it just for allowing you to use Lightburn.



They're not kidding when they say it's quick and easy to replace. Just be sure to read the instructions, and be ready to hit the kill switch when you first test (it might home to the front corner instead of the back if you have things the wrong way around, and make horrible noises because there's no endstop there to stop it! Be ready)



Upgrades:

I've just finally swapped my K40's bed. Was using basically chicken wire before. Now I've got some threaded levelling feet ([https://www.admerch.com.au/product/m8-steel-fixed-levelling-feet-lvr089/](https://www.admerch.com.au/product/m8-steel-fixed-levelling-feet-lvr089/), cut down to size with a bolt cutter) on the base holding up metal honeycombed expandable mesh (purchased on ebay from a german fellow [https://www.ebay.com.au/itm/132697048886?ViewItem=&item=132697048886](https://www.ebay.com.au/itm/132697048886?ViewItem=&item=132697048886)). No more worrying about poisoning myself with zinc from galvanized metals, and technically height adjustable if I can be bothered.



I've got air assist - An aquarium air pump provides that - I'll link the model when I get home in case anyones interested - I've found it to be a much better option than a compressor - quiet and consistent. Slightly pricier to get a model with enough power though! This cost me over 150AUD.

I've also 3d printed an air assist nozzle ([https://www.thingiverse.com/thing:2421971](https://www.thingiverse.com/thing:2421971))



I've replaced the provided water cooling aquarium pump with a grounded australian one. Looking at some non-conductive algaecide options though so I don't have to change water as much.



I'm currently looking at mounting options for a webcam for the cool camera option lightburn has.



I'm redoing my exhaust system hopefully soon. I've  had a new vent shroud delivered that apparently fits the k40 a lot better - will get rid of the old k40 bathroom fan -  Right now it runs to a bouncy castle blower that I pick up and place outside my office window whenever I want to cut. (I'm just overwhelming everything with so much air pressure that it can't even think of leaking acrylic fumes at me)



I'm off at a LAN party this weekend, but some time in the next few weeks when I have some free time I might put up a video of my janky Australian setup.


---
**Mario Gayoso** *July 13, 2018 21:36*

**+Kiah Connor**  Wow so much help!! Thanks so much!! I look forward to see this video! I will check this links and start working on this project. I haven't cut anything yet with the machine as it is. I'm afraid to cause a fire or something like that.



I will get the controller, lightburn a new honeycomb bed, air assist, exhaust, vent and fan and even maybe a chiller... I realized that buying the more expensive machine at the end was cheaper than this one, but the fun is in the process of upgrading it!



I look forward to seeing your video Kiah! 



Thanks again!!




---
**HalfNormal** *July 13, 2018 21:41*

Even with every conceivable add on and modification, I doubt you would come close to a third of the cost of a name brand.


---
**Adam Hied** *July 13, 2018 22:18*

**+HalfNormal** 100% agree. I’ve got $1500 into mine. With replacing the cw3000 with the 600watt light burn chiller, z table, air assist, LightBurn, cohesion 3D, glcd, better optics, external power supply, and upgraded exhaust. 


---
**Adam Hied** *July 14, 2018 01:01*

heres the upgrade list you asked about on youtube. I think just about everything i did is in it.



12 volt speed controller

[https://www.amazon.com/gp/product/B00DVGGWC0/ref=oh_aui_detailpage_o09_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00DVGGWC0/ref=oh_aui_detailpage_o09_s00?ie=UTF8&psc=1)



Aluminum Plate

[https://www.amazon.com/gp/product/B07CRVWHML/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B07CRVWHML/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)



honeycomb grid

[https://www.amazon.com/gp/product/B01AYK3M2S/ref=oh_aui_detailpage_o08_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01AYK3M2S/ref=oh_aui_detailpage_o08_s00?ie=UTF8&psc=1)



12volt step down

[https://www.amazon.com/gp/product/B01EFUHFDU/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01EFUHFDU/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)



air pump

[https://www.amazon.com/gp/product/B06XDKRYC6/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B06XDKRYC6/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1)



24 volt 15 amp power supply

[https://www.amazon.com/gp/product/B01IOK5FM0/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01IOK5FM0/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)



Air extraction Hood

[https://www.amazon.com/gp/product/B003NE59HE/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B003NE59HE/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1)





12volt blower(buy the 24 volt version..)

[https://www.amazon.com/gp/product/B000O8D0IC/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B000O8D0IC/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1)



Air assist head

[https://www.amazon.com/gp/product/B0094WLPYK/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B0094WLPYK/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)



600 watt water chiller(looks like the price went up $40)



[https://www.lightobject.com/Lightobject-600W-Mini-Water-Chiller-for-CO2-laser-machine-AC110V-60Hz-P999.aspx](https://www.lightobject.com/Lightobject-600W-Mini-Water-Chiller-for-CO2-laser-machine-AC110V-60Hz-P999.aspx)



Mirror and lense upgrade

[http://cohesion3d.com/mirrors-lens-upgrade-bundle-for-k40-laser-cutter/](http://cohesion3d.com/mirrors-lens-upgrade-bundle-for-k40-laser-cutter/)



Cohesion 3d bundle w/GLCD, usb cable, motor driver, external stepper

[http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



z-table -

[holgamods.com - Lasers](https://www.holgamods.com/holgamods/Lasers.html)



Varaious wires, switches, airlines, and exhaust tubing as well.


---
**HalfNormal** *July 14, 2018 01:32*

**+Adam Hied** very nice job documenting your upgrades.


---
**Adam Hied** *July 14, 2018 02:20*

**+HalfNormal** thanks it took me a few weeks getting it setup. Really having fun now.


---
**Mario Gayoso** *July 15, 2018 06:09*

**+Adam Hied** THANKS A LOT! what an amazing help!! keep it up with the youtube videos and your projects! I'm sure they wil help lots of people :) ... so you got the 600w water chiller? how does it run?




---
**Mario Gayoso** *July 15, 2018 06:49*

**+Adam Hied**  I just checked all the upgrades and put them in my shopping cart. They are many items that are Z table related right? which ones are those? the last one for example and what else?


---
**Mario Gayoso** *July 15, 2018 07:18*

**+Adam Hied**  what do you think about this? [amazon.com - Robot Check](https://www.amazon.com/gp/product/B00EFFOF0W/ref=ox_sc_act_title_1?smid=AGJOB9QIKR6CC&psc=1)




---
**Adam Hied** *July 15, 2018 12:51*

**+Mario Gayoso Noguerol** the aluminum plate, the honeycomb grid, and the 24volt power supply are for the z table. You will also need 1” square tube 1/16” thick if you get the holes mods z table. Give Randy a call or email about the z table he can walk you through it. It’s not hard to build but did take me a few hours. That being said this isn’t the first time I’ve messed with cnc machines.



I wouldn’t get the grid you looked at if you get that z table. The overall size may not be correct.


---
**Mario Gayoso** *July 18, 2018 15:01*

**+Adam Hied** at the moment I'm not looking at getting the Z table. I got the chiller, duct, the 12 volt speed reducer, the 12 v step down (assuming for the fan for the duct), cohesion 3d mini, new lenses, chain, fan, air assist, air exhaust hood and some other things.  Now my focus is into GROUND the machine the right way. What did you do with yours? So many scary videos about death!!  My wife is concern haha.


---
**Adam Hied** *July 18, 2018 16:46*

**+Mario Gayoso Noguerol** I just made sure it was making good contact. Then Verified the ground on my outlet was good. For sure watch the YouTube vids, but If the machine you get looks like it’s in good shape like mine you’ll probably be all set. Mine was even properly aligned.



Oh I did buy a fire extinguisher..I think something your cutting catching fire when you’re first trying to figure stuff out might be the biggest problem.



Also, the z table is probably my favorite thing. I can go from cutting paper for wedding invitations to engraving a knife handle and in seconds with just a few clicks.



Did you get an external 24 volt supply and external stepper driver?


---
**Mario Gayoso** *July 22, 2018 02:16*

**+Adam Hied**  I will post everything I got! I will need your help man!! Haha  I'm thinking about that table you mention. I got the honeycomb bed but maybe  I should get the z table.i went all in and got the chiller too. I also cut the exhaust and I did a pretty decent job.![images/3d238cafab1baa7604e6a32e990625f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d238cafab1baa7604e6a32e990625f5.jpeg)


---
**Mario Gayoso** *July 22, 2018 02:18*

**+Adam Hied** this is how I cut the duct![images/aa7cd29a21a563e3b4e0fbdfebf263e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa7cd29a21a563e3b4e0fbdfebf263e1.jpeg)


---
**Adam Hied** *July 22, 2018 02:35*

Lookin good!!




---
**Adam Hied** *July 22, 2018 02:39*

It's all a lot easier than you think. Just takes some time.


---
**Adam Hied** *July 22, 2018 02:46*

Pretty soon you’ll be making all kinds of stuff. The glider and it’s wing jig was my first project. Now my soon to be wife has me laser cutting our wedding invitations this week, and a small company asked me to laser cut and engrave custom name tags at $10 a piece. So that’s a nice plus. You’re gonna love it. The hardest part is the software learning curve.

![images/add6e8f4728b2e2a9e2276b32bcb5841.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/add6e8f4728b2e2a9e2276b32bcb5841.jpeg)


---
**Mario Gayoso** *July 22, 2018 06:44*

**+Adam Hied** that is amazing!! tonight Im adding the air assist "nozzle" we should try a google hangout one of these nights while installing the news stuff to be sure I'm doing it the right way! If they are some tutorials you based your projects on, please share :)



It's great that you are doing the wedding invites!! What a way to justify the investment haha :P Just kidding. 



Awesome that you got the tags to do!! I'm looking forward to start using my machine.


---
*Imported from [Google+](https://plus.google.com/+MarioGayoso/posts/UjYMn6ForGn) &mdash; content and formatting may not be reliable*
