---
layout: post
title: "A couple of great laser lens references"
date: May 04, 2016 15:24
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
A couple of great laser lens references. First simple, second in depth.

**+Scott Thorne** The Basic Laser Optics has your beam pattern in it. It is totally normal!



[http://www.lasercomponents.com/de/?embedded=1&file=fileadmin/user_upload/home/Datasheets/lc/applikationsreport/lens-theory.pdf&no_cache=1](http://www.lasercomponents.com/de/?embedded=1&file=fileadmin/user_upload/home/Datasheets/lc/applikationsreport/lens-theory.pdf&no_cache=1)



[https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=9&cad=rja&uact=8&ved=0ahUKEwjNnc3Y1MDMAhUU-GMKHdF_BgQQFghBMAg&url=http%3A%2F%2Fwww.springer.com%2Fcda%2Fcontent%2Fdocument%2Fcda_downloaddocument%2F9781849960618-c1.pdf%3FSGWID%3D0-0-45-993418-p173947146&usg=AFQjCNFuMEqMO-Tm5z57qdKq4hU-t_2KqA&sig2=D1e7bJFnzyJOH5TYwylRgQ&bvm=bv.121099550,d.cGc](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=9&cad=rja&uact=8&ved=0ahUKEwjNnc3Y1MDMAhUU-GMKHdF_BgQQFghBMAg&url=http%3A%2F%2Fwww.springer.com%2Fcda%2Fcontent%2Fdocument%2Fcda_downloaddocument%2F9781849960618-c1.pdf%3FSGWID%3D0-0-45-993418-p173947146&usg=AFQjCNFuMEqMO-Tm5z57qdKq4hU-t_2KqA&sig2=D1e7bJFnzyJOH5TYwylRgQ&bvm=bv.121099550,d.cGc)





![images/0ba5c662d43cd96fd53ad71c5403a974.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0ba5c662d43cd96fd53ad71c5403a974.jpeg)



**"HalfNormal"**

---
---
**Scott Thorne** *May 04, 2016 19:18*

Yup...tem10....donut of power...

Lol


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/9ocbqMEAPA6) &mdash; content and formatting may not be reliable*
