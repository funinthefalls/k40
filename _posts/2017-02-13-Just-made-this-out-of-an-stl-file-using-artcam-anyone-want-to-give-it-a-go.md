---
layout: post
title: "Just made this out of an stl file using artcam, anyone want to give it a go ?"
date: February 13, 2017 22:26
category: "Discussion"
author: "Mike Grady"
---
Just made this out of an stl file using artcam, anyone want to give it a go ? Im away from my laser for a couple of days so cant try it myself yet :)



![images/40f6b153a316f589ba8a5495f2b3d9c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40f6b153a316f589ba8a5495f2b3d9c5.jpeg)
![images/9cbc9314f28e761ac3fb06fa373bca7d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9cbc9314f28e761ac3fb06fa373bca7d.jpeg)

**"Mike Grady"**

---
---
**greg greene** *February 13, 2017 22:35*

File link?


---
**greg greene** *February 13, 2017 22:38*

got it - will let you know


---
**greg greene** *February 13, 2017 22:44*

ARTCAM is pretty pricey though !


---
**Mike Grady** *February 13, 2017 22:59*

thanks **+greg greene**, try [cgpeers.com](http://cgpeers.com), a private site but new registrations are open beginning of every month, search for autodesk ;)


---
**greg greene** *February 14, 2017 23:15*

Cuts well on the CNC carver, not at all in LW3 on the laser - there it just scorches everything with the settings 20% light to 80% Dark


---
**Mike Grady** *February 15, 2017 18:11*

ok thanks **+greg greene** will have another play around with the file :)


---
**greg greene** *February 15, 2017 18:27*

cute carving though !


---
*Imported from [Google+](https://plus.google.com/106807531578115449401/posts/ZVywkvAkqL5) &mdash; content and formatting may not be reliable*
