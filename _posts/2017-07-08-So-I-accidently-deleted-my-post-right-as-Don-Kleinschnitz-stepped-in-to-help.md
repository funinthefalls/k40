---
layout: post
title: "So I accidently deleted my post right as Don Kleinschnitz stepped in to help"
date: July 08, 2017 20:39
category: "Hardware and Laser settings"
author: "Madyn3D CNC, LLC"
---
#K40LPSU

#K40PSU

#K40Troubleshoot

#K40Arcing



So I accidently deleted my post right as **+Don Kleinschnitz** stepped in to help. I just wanted to delete the irrelevant photo but it deleted my entire post, sorry for any confusion. 



I am having some arcing going on behind my LPSU. Research is telling me it's a bad tube, which makes sense because the tube is over a year old and has a lot of miles. The LPSU was just swapped out for a brand new one 2 months ago and has been working great, even better than the stock LPSU. 



There seems to be no loss of powder according to the machines ammeter, when the laser is engaged. The arcing seems to happen anywhere in between 30%-100% power. 



With an alligator clip securely fastened to the machines frame, then to my multimeter, and the other MM lead going to the water in my water bucket, the current was 0.00 mA while idle, and then jumped around between 0.003/0.005 mA when the laser is engaged.  



It's quite the arc, I've only been testing with short pulses since discovering this problem, out of fear the arcing may cause more damage. Here is a video of the arcing in action, skip to 
{% include youtubePlayer.html id="RooYJ8qmFNA" %}
[2:30](https://youtu.be/RooYJ8qmFNA?t=2m30s) for the actual arcing. 



Any other tests or info needed to help confirm the issue, please let me know and I will be happy to get more info. I don't however, have a high voltage probe. Just Fluke DMM's and an Analog MM. 




{% include youtubePlayer.html id="RooYJ8qmFNA" %}
[https://youtu.be/RooYJ8qmFNA](https://youtu.be/RooYJ8qmFNA)





**"Madyn3D CNC, LLC"**

---
---
**Don Kleinschnitz Jr.** *July 09, 2017 04:42*

Not yet convinced its the tube.....



Couple of things to try:

Can you unmount the LPS and move it forward to see if you can tell  where the arc is coming from. 



Is it possible that there is defective wiring?



Did you look in the laser tube compartment to see if the arc is in there. Arc's are pretty bright and can fool you as to where they are coming from :).



How did you install the new supply and did you have any breaks in the leads from the HVT to the anode (the typically red wire).



On a separate note can you measure voltage (not current) in the bucket.



Does the current ma meter jump at all when you operate it at the minimal current the arc occurs at? 






---
**Madyn3D CNC, LLC** *July 09, 2017 19:06*

After reading up some more on arcing, I'm also not so sure it's the tube.



I removed the back cover for the tube compartment and there's definitely no arcing on or around the tube probes. It's coming from behind the LPSU. 



When I swapped out the original LPSU, I DID have to splice the new HV line back into the existing HV line. I twisted the wires together and soldered, and then covered with 3-4 layers of heat shrink tubing. <b>Can the high voltage be sneaking out somehow?</b> If so, I'll be impressed, I wrapped it up real good. I was sure to insulate the wire back up as best as possible when making the splice. I thought I may have went overboard a little with the insulation, but HV is a funny thing sometimes and I could use some more experience in that department to really understand it's behavior. 



I'm heading down to my shop now-



First I am going to remove the LPSU and try to pinpoint the exact spot where it's arcing, then I am going to add a few more layers of marine grade heat shrink tubing, and see if the arcing is still there. I wish the wire was long enough to run directly to the tube, but unfortunately for some reason, the HV line was not as long as it should have been when I purchased the new LPSU.  



I'm also going to test the voltage in the water bucket, and also analyze the ammeter at low current, and report back. I appreciate the help. 


---
**Don Kleinschnitz Jr.** *July 09, 2017 22:00*

**+Madyn3D CNC, LLC** heat shrink will NOT keep it from arcing!

Use the materials and general techniques used to connect to the anode [in the post below] to make a splice using a piece of tubing filled with the silicon. I suggest the tubing be 1/2 ID and 2" long (1 inch longer than the insulated portion of the splice) wire. When you solder avoid sharp transitions, use ball soldering.



<b>Ball Soldering</b>

[https://www.spellmanhv.com/en/Technical-Resources/FAQs/Technology-Terminology/What-is-Ball-Soldering](https://www.spellmanhv.com/en/Technical-Resources/FAQs/Technology-Terminology/What-is-Ball-Soldering)



[donsthings.blogspot.com - K40 LPS repair and test](http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html)


---
**Madyn3D CNC, LLC** *July 09, 2017 23:03*

Yea it's got to be the wire. I dismounted the LPSU and moved it forward to see what's going on behind there, and once I fired up the laser I couldn't get it to arc anymore. First time I fired it there was an audible arc noise but I couldn't see a flash anywhere, and then it just wouldn't arc anymore I guess since the wire was situated differently.



WOW! When I spliced that line I even used liquid electric tape over the solder joint, then used 3 pieces of shrink wrap, heating each one by one. 



I suppose I'll let some electrons leak from the caps for a day or two then re-wire the HV line direct to the laser tube. I checked the length again and I think there's just enough to make it.  I wish there was a way to discharge the PSU before I work on it, but as far as I'm aware, there really is no way. So I usually just let it sit for a day or two. 



Here's a photo of the HV line, you can see the red shrink wrap over the splice. 



 

![images/fa700e86fd2fb779775462304d53e5d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fa700e86fd2fb779775462304d53e5d8.jpeg)


---
**Madyn3D CNC, LLC** *July 10, 2017 15:28*

Thank you **+Don Kleinschnitz**, it never fails to amaze me how much you know about these machines. If and when the blue box mystery gets cracked, I'm certain it will be done by you. I have yet to come across anyone with even half the drive and dedication you have at demystifying these lasers. 



I thought a bad tube was somehow backing up current, leading to the arcing. I'm glad I came by to check first, before spending money I don't have on another tube. 



I also have a K40 flyback removed from a LPSU PCB, I'm not sure if it works or not. If it can be of any use to you I'll be happy to send it your way. 




---
**Don Kleinschnitz Jr.** *July 10, 2017 17:34*

**+Madyn3D CNC, LLC** I accept all parts donations, sure send it my way. 



Did you fix your arcing problem, ??



BTW what mystery is left uncracked lol?


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/7jRZTAmtRpj) &mdash; content and formatting may not be reliable*
