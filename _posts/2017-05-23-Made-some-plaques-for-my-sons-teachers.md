---
layout: post
title: "Made some plaques for my son's teachers"
date: May 23, 2017 19:11
category: "Object produced with laser"
author: "Arion McCartney"
---
Made some plaques for my son's teachers.  Wife doesn't like the font at the bottom, but I think it adds a "kids" touch to it. 



![images/68bc2d771486ac48d198f45c85ed3728.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/68bc2d771486ac48d198f45c85ed3728.jpeg)
![images/68e1a71a4ce151e058420bb8d3158bfd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/68e1a71a4ce151e058420bb8d3158bfd.jpeg)

**"Arion McCartney"**

---
---
**Ned Hill** *May 23, 2017 20:51*

Great job.  If you do this again next year it might be cool to have your son write out the bottom in his own hand on a piece of paper, scan or photograph it and etch it as a picture.


---
**Arion McCartney** *May 23, 2017 21:02*

**+Ned Hill** That is a GREAT idea!  Thanks!


---
**Anthony Bolgar** *May 23, 2017 21:10*

Yup,Ned nailed it perfectly. Great idea.


---
**Arion McCartney** *May 23, 2017 21:14*

I have 1 more to make for his kindergarten teacher, so I'm going to try the scan. I'll post a pic when done!


---
**Anthony Bolgar** *May 23, 2017 21:22*

Pleas do I would love to see the finished product. Do you mind if I steal the idea for my Granddaughter?


---
**Arion McCartney** *May 23, 2017 21:28*

**+Anthony Bolgar**​ not at all! I could send the svg if you want.


---
**Anthony Bolgar** *May 23, 2017 21:32*

No need, I'll whip up a quick drawing myself (I have been using CoreldRAW since v3 and was a beta tester for over a decade) But thanks for the offer, I am sure people in the K40 group would like a copy though.




---
**Arion McCartney** *May 23, 2017 21:35*

**+Anthony Bolgar** awesome.  I'll have to post a google drive link when I get home.  Don't see a files section here unless I am missing something.


---
**Ned Hill** *May 23, 2017 21:58*

**+Arion McCartney** No file section here, so most people use their google drive or upload it to Thinkiverse.


---
**Nigel Conroy** *May 24, 2017 00:29*

Very nice


---
**Arion McCartney** *May 24, 2017 00:50*

Files link: [drive.google.com - Teacher Apple - Google Drive](https://drive.google.com/drive/folders/0B3A9wmWsyWK1ZWV6ellqcXdxaDA?usp=sharing)


---
**Arion McCartney** *May 26, 2017 04:46*

Here is the hand writing engraving. Thanks **+Ned Hill**​ for the suggestion!

![images/0878cf6664c4b8cadfff25c6de8fc6a4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0878cf6664c4b8cadfff25c6de8fc6a4.jpeg)


---
**Arion McCartney** *May 26, 2017 04:46*

![images/bc021e41b0c113e0085fb969b8abbd74.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bc021e41b0c113e0085fb969b8abbd74.jpeg)


---
**Anthony Bolgar** *May 26, 2017 14:49*

**+Arion McCartney**, those turned out amazing. **+Ned Hill**s suggestion brought it to the next level.


---
**Ned Hill** *May 26, 2017 14:54*

That's awesome.  Thanks but that's what I love about this group.  Sharing to make each other better makers.  


---
*Imported from [Google+](https://plus.google.com/110543831642314750018/posts/PUZc4e7UNZn) &mdash; content and formatting may not be reliable*
