---
layout: post
title: "I really need an adjustable Z axis table for my K40, Im getting really sick of propping things up etc"
date: November 11, 2016 14:55
category: "Modification"
author: "Bob Damato"
---
I really need an adjustable Z axis table for my K40, Im getting really sick of propping things up etc.  I see a couple that all appear to be the same (see below) but they talk about motor drivers. I really want something plug and play. I dont even care if its power, I dont mind cranking it up and down if I have to.  Anyone have experience with this one, or a different one?



[http://www.ebay.com/itm/Power-table-bed-kit-for-K40-small-laser-machine-/121799353986?hash=item1c5bcea682:g:JtMAAOSwo6lWL93Q](http://www.ebay.com/itm/Power-table-bed-kit-for-K40-small-laser-machine-/121799353986?hash=item1c5bcea682:g:JtMAAOSwo6lWL93Q)





**"Bob Damato"**

---
---
**greg greene** *November 11, 2016 15:05*

I got a lab table on ebay - 15 bucks - just turn the knob to set the height - easy and cheap


---
**Alex Krause** *November 11, 2016 15:05*

I think it may be cheaper on lightobjects site right now


---
**Bob Damato** *November 11, 2016 15:11*

**+greg greene** do you have a link? Sounds like its more up my alley


---
**greg greene** *November 11, 2016 16:42*

[http://www.ebay.com/itm/162186925342?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/162186925342?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Don Kleinschnitz Jr.** *November 11, 2016 16:42*

**+Bob Damato** check this out. [http://donsthings.blogspot.com/search?q=lift+table](http://donsthings.blogspot.com/search?q=lift+table)

I built this so that it is totally modular. The only connection to my K40 is the 24vdc. 

BTW you will need a separate 24VDC supply as the LPS cannot reliably power this and your machine. 

In my case I converted my K40 to a smoothie with a new 24VDC supply then I used the LPS 24v for the lift.

[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)

[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search?q=lift+table)


---
**Ashley M. Kirchner [Norym]** *November 11, 2016 17:06*

Search for 'lab scissor jack' on ebay. Plenty of them in various sizes. 


---
**Bob Damato** *November 11, 2016 17:12*

Found it, thank you much!!


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/XTDhBKFmEcM) &mdash; content and formatting may not be reliable*
