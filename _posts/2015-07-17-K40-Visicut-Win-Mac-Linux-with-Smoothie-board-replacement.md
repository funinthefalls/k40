---
layout: post
title: "K40 + Visicut (Win/Mac/Linux) with Smoothie board replacement"
date: July 17, 2015 12:44
category: "Software"
author: "Stephane Buisson"
---
K40 + Visicut (Win/Mac/Linux) with Smoothie board replacement.

One big step forward for a usable solution ! 

Thank to **+Thomas Oster**  (Visicut developer)

Community call for help - Beta testers needed



Go there to find out about Visicut open source software -> [https://hci.rwth-aachen.de/visicut](https://hci.rwth-aachen.de/visicut)

You will need a Smoothie board to upgrade your K40 -> [http://smoothieware.org/getting-smoothieboard](http://smoothieware.org/getting-smoothieboard) 

Then please test all Thomas changes and report bugs and stuff in the pull-request [https://github.com/t-oster/LibLaserCut/pull/23](https://github.com/t-oster/LibLaserCut/pull/23) from where you can already download the first build files.



Visicut for Win/Mac/Linux already existing, Visicut driver for Smoothie board (LibLaserCut) was the last missing link for a fully operational chain, hardware to software to mod our cheap K40 laser cutter at reasonable price. (Smoothie is cheaper than DSP board and benefit of Smoothie community support).



**+Arthur Wolf** did told me sometime ago a K40 was modified with a Smoothie board. A hardware Mod tutorial would also be needed for our K40 community, any candidate ?





**"Stephane Buisson"**

---
---
**David Richards (djrm)** *July 17, 2015 13:41*

Thanks for the heads up on this. Lookin around I see a chinese board which may be suitable - AZSMZ, I'll start doing some research, something similar could end up on my 3D printer or K40. See here: [http://thinkl33t.co.uk/smoothieware-compatible-mainboards/](http://thinkl33t.co.uk/smoothieware-compatible-mainboards/)


---
**Arthur Wolf** *July 17, 2015 13:59*

**+Stephane Buisson** We are working on conversion kits for Smoothie/K40, including a full panel with a LCD etc. And documentation of it. No release date yet though.


---
**Don Kleinschnitz Jr.** *September 15, 2017 18:50*

#K40smoothieVisicut


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/UqDqWRB8waH) &mdash; content and formatting may not be reliable*
