---
layout: post
title: "I have done this before with a 3D printer, 0.6mm thick but it takes a while"
date: March 07, 2017 20:05
category: "Repository and designs"
author: "Ariel Yahni (UniKpty)"
---
I have done this before with a 3D printer, 0.6mm thick but it takes a while. What would be a good thing yet hard material to cut.



There are many designs and sizes for this, Im away from my computer but I will post sources later.



![images/d1cb9521d2c6e0b0652f7d6c0d5533ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1cb9521d2c6e0b0652f7d6c0d5533ed.jpeg)
![images/f6a943914364a72fdb0a7c803b91f09b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6a943914364a72fdb0a7c803b91f09b.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**Alex Krause** *March 07, 2017 20:37*

Polystyrene sheets 


---
**Ariel Yahni (UniKpty)** *March 07, 2017 20:43*

Do they come in color?


---
**Ariel Yahni (UniKpty)** *March 07, 2017 20:45*

HIPS, I have 5 filaments rolls of that.


---
**Ned Hill** *March 07, 2017 20:53*

Might be interesting to try with wood veneers or even maybe lattice hinge cuts.


---
**Ned Hill** *March 07, 2017 20:57*

You could probably even use thin acrylic sheets and just heat them with a heat gun to get them to bend.


---
**Jose Salatino** *March 07, 2017 21:54*

Cuando puedas mandame el archivo con diseño. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 07, 2017 22:10*

Leather? Flexible enough to bend & durable enough to last a lifetime... Would look pretty classy too.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/ABK17BqcySH) &mdash; content and formatting may not be reliable*
