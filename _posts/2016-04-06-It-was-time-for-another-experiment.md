---
layout: post
title: "It was time for another experiment ..."
date: April 06, 2016 17:55
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
It was time for another experiment ... this time all the (slit) cutting was done prior to the two-tone paint job. Doing all of the cutting prior to painting prevents the paint charring that I was getting. It's cleaner, sharper and the color doesn't look burned. And I also matched the pattern where the edges join up. Not bad for an hour's worth of playing around.

![images/f0f52177deba8c8b9cfbc79cfc9727ea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f0f52177deba8c8b9cfbc79cfc9727ea.jpeg)



**"Ashley M. Kirchner [Norym]"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 18:49*

That's really nice Ashley. Perfect matching of the pattern at the join too. Well done.


---
**Ashley M. Kirchner [Norym]** *April 06, 2016 18:56*

Thanks. Process reversal .. and documenting for myself. Once I finish redoing my spare room and convert it into a work room, I can start making more of them and start putting them up for sale. A million dollars each! Ok, maybe in rupees ... or yen ... <grin>


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 19:04*

**+Ashley M. Kirchner** Hahaha. I remember having a 1 billion Zimbabwe $ note years ago. Worked out to be about equivalent to 20c at the time lol. I'll buy them all with that.


---
**Ashley M. Kirchner [Norym]** *April 06, 2016 19:14*

Hey, for someone who collects foreign bank notes, that's worth more than 20c to them!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 19:40*

**+Ashley M. Kirchner** That's true indeed. I used to collect coins as a child/teenager & I know I paid way more than some stuff was worth, just because I wanted it.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/Qy6NqZQ5tTZ) &mdash; content and formatting may not be reliable*
