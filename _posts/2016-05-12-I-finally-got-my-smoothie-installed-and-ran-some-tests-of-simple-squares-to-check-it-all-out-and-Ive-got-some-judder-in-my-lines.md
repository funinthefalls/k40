---
layout: post
title: "I finally got my smoothie installed and ran some tests of simple squares to check it all out, and I've got some judder in my lines"
date: May 12, 2016 06:23
category: "Smoothieboard Modification"
author: "Adam Dalmatoff"
---
I finally got my smoothie installed and ran some tests of simple squares to check it all out, and I've got some judder in my lines.  Anybody have any advice to correct this?



I've tried rolling through the stepper current settings since I could not find any specs but no changes there helped.  I thought it could be acceleration, as it is mostly a mess at the start of the engrave, but isn't always true as it still has some squirm throughout.  I've slowed it down and sped it up, but end up with the similar results.  Appreciate any feedback, thanks.



BTW, the pic was taken at a slight angle so the edges seem to look warped a bit, they aren't, they are straight, but the judder is real.

![images/5fe09a02c59426125ad834869800ce55.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5fe09a02c59426125ad834869800ce55.jpeg)



**"Adam Dalmatoff"**

---
---
**Jean-Baptiste Passant** *May 12, 2016 06:43*

Were the line straight with the old controller ?


---
**Adam Dalmatoff** *May 12, 2016 06:47*

Yeah, they were clean and straight. Was surprised once I got this in and it wasn't.


---
**Jean-Baptiste Passant** *May 12, 2016 06:52*

Can you please check your belt are tight ?

There is two big philips screws on the right, you can easily access them through a hole in the "command" section.

Just tighten them and check the belt.



Your belt should always look straight, even when the motors are moving, check that by moving the head with your head.



If you have checked the current settings (better be safe and leave the default settings) then it should be the problem.


---
**Alex Krause** *May 12, 2016 06:58*

How fast were you moving when you engraved these lines?


---
**Adam Dalmatoff** *May 12, 2016 07:19*

**+Jean-Baptiste Passant** 

I will check the belts, they seemed fine but this is one thing I did not verify. Thanks, this sounds hopeful.

I've put the current back down to 0.4 but even 0.6 was the same. Anything higher just started to heat the motors up.


---
**Adam Dalmatoff** *May 12, 2016 07:27*

**+Alex Krause** 

This was at 5000 mm/min but have the same issue down to 1000. Previously I was normally running at 320 mm/s. Hoped to go even faster with the smoothie.

I've also dropped the acceleration to 2000. 1000 seems to be the bottom limit as any slower and it leaves gaps.


---
**Alex Krause** *May 12, 2016 07:33*

Lightly lift up on the X axis rail... Is there slack towad the electrical cabinet side? An is there a possibility your mirrors are wiggling as it's moving so fast


---
**Kornel Varga** *May 12, 2016 10:37*

My smoothieboard did like this when I set the cutting speed too high.  But it is still extremely fast cutter I love it. 



BTW how can you use rotary attachment with smoothie?  Does Visicut or LaserWeb support rotary axis? 


---
**Stephane Buisson** *May 12, 2016 13:20*

look like some slack in the gantry. (as it appear near the corners) I don't think it's related to Smoothie. speed is a user setting.


---
**Jean-Baptiste Passant** *May 12, 2016 13:34*

Well, Speed is IMO not a problem here (it may be at high speed but definitely not here), but probably the gantry like **+Stephane Buisson**  say.

Keep us updated on how it goes.

**+Kornel Varga** You probably have the same problem, once everything is square and tightened you will be able to get the speed you would like.


---
**Jean-Baptiste Passant** *May 12, 2016 14:34*

**+Nathan Walkner** Well, I believe **+Adam Dalmatoff** use the plain simple smoothie firmware and did not modify anything that could be the cause of the problem. Also I am not aware of such a setting :S.


---
**Stephane Buisson** *May 12, 2016 14:36*

**+Nathan Walkner** Smoothie manage Acceleration (the only board to do it to my knowledge). So definitely not that the issue.


---
**Adam Dalmatoff** *May 13, 2016 05:40*

**+Alex Krause** **+Stephane Buisson** **+Jean-Baptiste Passant** 

Yes, I can lift the X-axis bar a bit as there is some play in there (maybe 1mm), not sure how to remove that.

I tried to tighten the belt and I think it helped some, but it is still wobbly.  I then put some slight pressure on the axis as it engraved to keep it from possibly bouncing, but still had wiggle.

I even tried to loosen the belt quite a bit, just to test, and it changed slightly but not enough.

Unfortunately, I am still trying to track it down.  In order to remove some variables, I may hook the moshi back up and test to see if it produces judder as well now.

Thank you all for the suggestions thus far.  Would be nice if it was one tweak, but it probably is a combination of a few.


---
**Adam Dalmatoff** *May 13, 2016 05:41*

**+Nathan Walkner** I did try this setting, from 0.03 to 0.5 (default is 0.05) and didn't seem to help.


---
**Alex Krause** *May 13, 2016 05:47*

With power off move gantry to where it is inline with the hole that leads to the controller section. Once there look thru the hole you will see a grouping of 6 screws the screw that is in the elongated slot is the top tension roller... Loosen it slightly lift up gently on the gantry and let the roller fall into place before you tighten it down... If your machine is on a level surface you can toss a level on it to make sure your head is parallel. Please Note mirror adjustment will be needed if you do this﻿


---
**Adam Dalmatoff** *May 15, 2016 05:27*

To follow up, I made all the adjustments with the same results.  So, I swapped back to the moshi and ran a test and it was fine.  I noticed how the engrave was running and realized it was the gcode; I checked visicut and I was using the 'mark' profile.  Which apparently, is more like a wobbly cut.  I switched it to do an 'engrave' and it ran more like I expected, however it was horrible quality.  I've since tried the 3d engrave and all the DPI settings and algorithms and, unfortunately, I cannot get any of them to produce what coreldraw was spitting out.  I was hoping the switch would give me better cuts, engraves, and software to drive it.  I've only barely gotten slightly better cuts since corel's are also offset from my engrave.  Disappointing process overall, as I had pretty high hopes.

If anybody has any pointers on visicut engraving vectors, or a different software, please let me know. (I tried LaserWeb, but that had its own issues for me).

Thanks again for all the help, but looks like I'll be going back to moshi.


---
**Brian W.H. Phillips** *July 06, 2016 21:06*

that is interesting, I am on the way to maybe install a smoothy board and visicut, and so I will look out for comments to your story,


---
*Imported from [Google+](https://plus.google.com/108226692226539063645/posts/NfU6J2sYxpN) &mdash; content and formatting may not be reliable*
