---
layout: post
title: "New files...someone with a smoothie please try these!"
date: October 25, 2016 23:24
category: "Object produced with laser"
author: "Scott Thorne"
---
New files...someone with a smoothie please try these! 

![images/b82bdcb1759eea913541347affe9929d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b82bdcb1759eea913541347affe9929d.jpeg)



**"Scott Thorne"**

---
---
**Alex Krause** *October 25, 2016 23:34*

I can try out the rose


---
**Carl Fisher** *October 26, 2016 00:00*

I can give them a try as well - I have a C3D board which runs smoothie firmware.


---
**Mike Grady** *October 26, 2016 00:15*

I would love to try these, upgraded my k40 with Scott Marshalls ACR smoothie conversion a couple of weeks ago


---
**Mike Grady** *October 27, 2016 22:16*

Hi guys try this link

[http://www.google.com/search?sourceid=navclient&ie=UTF-8&rlz=1T4GGNI_enUS476US476&q=%e7%81%b0%e5%ba%a6%e5%9b%be++](http://www.google.com/search?sourceid=navclient&ie=UTF-8&rlz=1T4GGNI_enUS476US476&q=%e7%81%b0%e5%ba%a6%e5%9b%be++)



and click on images, dont know how good they will be :)


---
**Scott Thorne** *October 28, 2016 13:08*

**+Alex Krause**....email me...scottthorne23@yahoo.com


---
**Anthony Bolgar** *November 02, 2016 00:05*

I would love to test out the images.


---
**Scott Thorne** *November 02, 2016 00:43*

**+Anthony Bolgar**....message me and I'll send them to you tomorrow. 


---
**Anthony Bolgar** *November 02, 2016 01:12*

Sounds good Scott, btw, do you still have that spare tube?


---
**Scott Thorne** *November 02, 2016 09:44*

Yeah...but ups wants 55 to send it because its glass.


---
**Anthony Bolgar** *November 02, 2016 09:46*

How much would the total be for tube and shipping? I need one for my redsail LE400


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/9tGhiTRcGr9) &mdash; content and formatting may not be reliable*
