---
layout: post
title: "Laser Box Maker Question there is a very handy program on the net that will give you an SVG file for making any size finger joined box"
date: September 03, 2016 21:36
category: "Discussion"
author: "greg greene"
---
Laser Box Maker Question



there is a very handy program on the net that will give you an SVG file for making any size finger joined box.  I've been playing with it and found I have a problem with setting the right KERF size.  What happens is that if I set it a zero the joints are too lose,  so  I measured the spacing and input half that size (1.5mm/2 = .75mm ) but now the joints are 1.5mm too big ie" the tabs are too big to fit - so what am I doing wrong here?  Has anyone found a value that seems to work?



thanks All



FYI 5.16 mm Cedar cuts even better than 3mm birch ply !





**"greg greene"**

---
---
**Jim Hatch** *September 03, 2016 22:10*

Try using the 1.5mm kerf size instead of dividing it in 2. The program you're using is likely adjusting each piece separately. Remember you're going to see the 1.5mm kerf on all sides of your cut.


---
**greg greene** *September 03, 2016 22:25*

Hmm, I think that may be the opposite direction than what I need to go, a larger kerf size should result in making the tab bigger to account for the material the kerf removes wouldn't it?


---
**Jim Hatch** *September 04, 2016 01:55*

How did you measure your kerf? Did you cut a known square (say 10mmX10mm) and then measure the size of the hole and the size of the square? In this example the square should be 10mm and the hole should be 13mm (if your 1.5mm kerf setting is correct). Your kerf would then be 3mm/2 or 1.5mm - if the size of the hole is 11.5 then your kerf size is only 0.75mm (1.5mm/2).


---
**greg greene** *September 04, 2016 13:40*

I measured the difference between the size of the tab - and the size of the slot with a micrometer


---
**Jim Hatch** *September 04, 2016 21:38*

Are you using [makeabox.io - Make A Box](http://makeabox.io) to create the box layout?


---
**greg greene** *September 05, 2016 12:57*

yes - the one on the net


---
**Jim Hatch** *September 05, 2016 14:49*

Makeabox generates PDF files not SVGs. Which site are you using? I just created a makeabox version that I'll cut later today or tomorrow to check but the PDF looks right.


---
**greg greene** *September 05, 2016 14:56*

Your right - it was maker case


---
**greg greene** *September 07, 2016 01:36*

number is .08 mm


---
**Jim Hatch** *September 07, 2016 01:45*

The one I did from [makeabox.io](http://makeabox.io) worked correctly tonight. Tight fit but pretty much on the money. Give that one a try. It's an extra step if you want SVG as it outputs PDF but Corel/AI and Inkscape (I think) can open it and kick it back out as an SVG.


---
**greg greene** *September 07, 2016 01:47*

Thanks!


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/MneF5NCmDvK) &mdash; content and formatting may not be reliable*
