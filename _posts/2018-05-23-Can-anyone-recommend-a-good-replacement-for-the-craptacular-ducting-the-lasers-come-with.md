---
layout: post
title: "Can anyone recommend a good replacement for the craptacular ducting the lasers come with?"
date: May 23, 2018 20:36
category: "Original software and hardware issues"
author: "Bob Damato"
---
Can anyone recommend a good replacement for the craptacular ducting the lasers come with? its pretty terrible.







**"Bob Damato"**

---
---
**Andy Shilling** *May 23, 2018 21:20*

I'm in the UK and I've used manrose pvc ducting, you can convert from round to rectangle and back again with their adapters and it makes it more low profile.


---
**greg greene** *May 23, 2018 21:32*

any reason smooth dryer venting would not work?


---
**Bob Damato** *May 23, 2018 21:59*

Thanks Guys. **+greg greene** my duct is 3.5" and I cant seem to find dryer vent that size at my local big box stores




---
**Don Kleinschnitz Jr.** *May 23, 2018 22:30*

I screwed a floor duct on to the back and went with 4" corregated flexible pipe.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/fPZph68vYqv) &mdash; content and formatting may not be reliable*
