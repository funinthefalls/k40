---
layout: post
title: "Just curious how are people chilling their lasers?"
date: May 20, 2017 19:50
category: "Discussion"
author: "3D Laser"
---
Just curious how are people chilling their lasers?  





**"3D Laser"**

---
---
**Ariel Yahni (UniKpty)** *May 20, 2017 20:44*

Some time ago [https://plus.google.com/+ArielYahni/posts/ZpPNukwM2X5](https://plus.google.com/+ArielYahni/posts/ZpPNukwM2X5)


---
**Don Kleinschnitz Jr.** *May 20, 2017 21:29*

If I don't turn mine on it just sits there and "chills".....:)


---
**Ashley M. Kirchner [Norym]** *May 20, 2017 21:46*

In style ... :)

![images/93c7be5cfd02f3aad1acd6f5e05258b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/93c7be5cfd02f3aad1acd6f5e05258b8.jpeg)


---
**Ashley M. Kirchner [Norym]** *May 20, 2017 21:53*

And when my tube isn't chilling with frozen lemon slices, I have a 2 gallon water reservoir that I keep cold with frozen water bottles.


---
**A “alexxxhp” P** *May 20, 2017 23:00*

Portable A/C from LG


---
**Stephane Buisson** *May 21, 2017 09:30*

with a tea bag ;-))


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/L1ftua2Pwj8) &mdash; content and formatting may not be reliable*
