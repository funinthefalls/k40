---
layout: post
title: "Slanted edges of cuts. Guys, when I go to cut 1/4 plexi, I am noticing that the edges are slanted"
date: March 23, 2017 05:09
category: "Discussion"
author: "J Perry"
---
Slanted edges of cuts.  Guys, when I go to cut 1/4 plexi, I am noticing that the edges are slanted.  What do I need to adjust?  The mirror over the lense or the entire nozzle?  Thanx.





**"J Perry"**

---
---
**Alex Krause** *March 23, 2017 05:21*

Focus... Set focus to the middle of the material


---
**Phillip Conroy** *March 23, 2017 06:54*

Laser beam out of focal lens is hour glass shaped, set middle of cut material o focal lens length  ie stock lens focal legth50.8mm 


---
**Don Kleinschnitz Jr.** *March 23, 2017 11:29*

Depends on the machine (what optics are adjustable) and how (direction and shape) the sides are cut. 



Is the cut at an angle on all sides?



If the focus is set at 1/2 thickness as +Alex suggests and you still have angular cuts check that the beam is perpendicular to the table.



A simple test is to burn a dot with the laser, lower/raise the table and see if the dot moves in the X-Y. 



Note: longer DOF lenses are expected to increase the depth of straight cutting length therefore reducing the effect of the beams divergence. Usually setting the focus at 1/2T and insuring the beam exits the objective lens perpendicular to the table is sufficient for most users.


---
**Jim Hatch** *March 23, 2017 11:37*

You're always going to get something of an angle due to the shape of the beam. When it's focused in the middle of the material thickness it will be beveled in at the top and bottom a bit. The cone shape of the beam spreads outward at the top until you hit the focus point and then again after - like an hourglass.  Not usually discernible by eye unless you're butting a piece against another and trying to glue it. If it's noticably slanted then there's an issue with the beam not hitting the material on the perpindicular. That can be tray or head tilt.


---
**J Perry** *March 23, 2017 13:17*

Wow, great info guys.  I'll try adjusting the head angle and the height and I'll report back....thanx!


---
*Imported from [Google+](https://plus.google.com/107694592875565680026/posts/GWFPpuRL1Xc) &mdash; content and formatting may not be reliable*
