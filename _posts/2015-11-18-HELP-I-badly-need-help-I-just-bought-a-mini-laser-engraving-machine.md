---
layout: post
title: "HELP.... I badly need help... I just bought a mini laser engraving machine"
date: November 18, 2015 06:41
category: "Discussion"
author: "Dimple ABAD"
---
HELP.... I badly need help... I just bought a mini laser engraving machine. I will use it to make Rubber stamps(rectangular in shape). Can somebody teach me how to do it?? I'm just using Laserdrw since my Coreldrw is not working. I tried several times, but it did not engrave but the machine just made a solid burnt line/rectangle. PLEASE HELP... Please teach me the correct settings to be used..



Thank you.﻿









**"Dimple ABAD"**

---
---
**Imko Beckhoven van** *November 18, 2015 11:52*

i dont have Laserdrw but some googling ... [http://www.daycounter.com/Articles/LaserDrw/LaserDrw-Tutorial.phtml](http://www.daycounter.com/Articles/LaserDrw/LaserDrw-Tutorial.phtml)



scroll to the lower half.. 


---
**Coherent** *November 19, 2015 19:27*

Just remember you are removing/engraving everything "other" than the text or design (mirrored). What you will leave. What left will get inked and transfer/stamp.  Your specific machines power and speed will be adjusted to an adequate unwanted material removal depth. There are a number of tutorials and videos on the web regarding rubber stamps & LaserDrw/Corel. Just search on Google.


---
**Dimple ABAD** *November 20, 2015 07:57*

I tried already But my machine is just leaving a black rectangle on my rubber. This morning I tried again and it burned my rubber. Sorry for this question, i'm just so newbie on the machine. I will click on the mirror and then unclick the engrave box??


---
**Dimple ABAD** *November 20, 2015 07:57*

Do you think there's something wrong with my machine? 


---
**Kirk Yarina** *November 20, 2015 19:55*

Start with something simple, like cutting paper or cardboard at low power, and learn how to use the software and how the power settings work before trying to cut actual stamps.   Lots cheaper, and won't smell like burning rubber.


---
**Caroline Reliablelaser (Caroline)** *December 31, 2015 09:00*

Which kind of main board and software do you use?


---
**Dimple ABAD** *December 31, 2015 10:25*

I use Coreldraw. And my machine is just the 40watts chinese laser machine. 


---
**Dimple ABAD** *December 31, 2015 10:28*

I tried with speed of 350, power 49 and DPI 1000. But when i engrave rubber with so many letters, the stamp print is not good. 


---
*Imported from [Google+](https://plus.google.com/102611621305449797348/posts/5pD7SCwAMbv) &mdash; content and formatting may not be reliable*
