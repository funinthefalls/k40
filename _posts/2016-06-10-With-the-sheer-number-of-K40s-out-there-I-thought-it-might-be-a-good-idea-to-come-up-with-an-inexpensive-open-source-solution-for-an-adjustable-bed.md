---
layout: post
title: "With the sheer number of K40's out there, I thought it might be a good idea to come up with an inexpensive open source solution for an adjustable bed"
date: June 10, 2016 17:10
category: "Modification"
author: "Anthony Bolgar"
---
With the sheer number of K40's out there, I thought it might be a good idea to come up with an inexpensive open source solution for an adjustable bed. Quite a few of us have made our own designs, and I figure if we analyze the best features of each design we could have a great design in no time at all. Anyone interested in helping with this project? Or just let us have a peek at your design if you have made an adjustable table.





**"Anthony Bolgar"**

---
---
**Ariel Yahni (UniKpty)** *June 10, 2016 17:28*

I'm in. Are we looking for motorized or manual? 


---
**Anthony Bolgar** *June 10, 2016 17:30*

I would like it to be motorized if possible. However, we could design it in such a way that a manual knob could replace the stepper to offer a lower cost alternative (Not that steppers are really that expensive)


---
**Ariel Yahni (UniKpty)** *June 10, 2016 17:32*

Do you have the drawing for the current bed surface on the k40


---
**Anthony Bolgar** *June 10, 2016 17:54*

Not yet. Don't think it is necessary though, I am sure nothing of the original bed will be used.


---
**Stephen Sedgwick** *June 10, 2016 19:52*

I am thinking of building one myself, the big thing I see as a draw back on a lot of them, is the room the motor takes up.  With this in mind I am thinking of a scissor table, much like a scissor jack.  I can then place the motor within the control area after creating a hole between the 2 locations.  This will allow it to spin and take up as little room in the build area as possible.  I am also looking at cutting a good size hole in the bottom of the unit or I should say front facing edge, so I can put in larger material, and have that opening big enough to support the full raise and lower height of a work bed.  (unfortunately it is mainly money and time I am against right now as I am saving for a few other toys as I am working towards a plasma table also)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 10, 2016 21:07*

I like this idea. Although I haven't done any z-bed, I'm interested in the final product.


---
**Sebastian Szafran** *June 10, 2016 22:03*

I am with you, guys.


---
**Don Kleinschnitz Jr.** *June 10, 2016 22:05*

I would start by looking at the LO table design (my final choice) and consider what is good and bad about it. Not sure how we would do better? Should we just clone that into a DIY version? I paid $165 for it on amazon.

I looked at other designs. I concluded that scissors cannot get as low as the belt design and may cost more. In the LO design the stepper is in the back out of the way. I added the electronics and driver also in the back out of the way. [http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html](http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)

With the smoothie I am planning to eliminate all the z axis electronics anyway.

I also saw cutting the bottom and front of the machine out as an advantage but then figured at that point I should just repackage the current X & Y axis into a clam shell type of design that would allow larger material to slide in horizontally and thick materials to stick out the bottom. I figured in that design the lift table is built into slides in the frame and below the machine.

The electronics is outside the cutting area in a separate module.


---
**Stephane Buisson** *June 10, 2016 22:11*

problem is not to go up or down, but to be in focus.

maybe a solution with beam sensor...


---
**Stephen Sedgwick** *June 11, 2016 01:26*

The scissors wouldn't be exactly like a car jack but same idea then you can get a z axis to get the focus is think you can get down to about a inch depth as we are not lifting more than a few lbs at a time.  I will see if I can get some time to draw up my idea this weekend


---
**HalfNormal** *June 11, 2016 03:44*

There are a lot of sissor lift designs out there. I am thinking of one that the screw comes out the end and could be manually or motor driven. The screw would not move up and down with the lift.


---
**Anthony Bolgar** *June 11, 2016 05:22*

That is exactly what I was thinking **+HalfNormal** I am rebuilding a 4" x 4" lab jack to put the screw on the bottom of the jack, eliminating the up and down of the screw, which makes motorizing it a hell of a lot easier.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 11, 2016 07:25*

**+Anthony Bolgar** **+HalfNormal**  Would a dual screw system be more stable or possibly introduce issues? i.e. a screw on the left & right of the bottom as you both mention.


---
**Don Kleinschnitz Jr.** *June 11, 2016 11:41*

I think that, screws that lift all four corners in sync (cog belts) is more likely to be easy for the bed to be slewed up and down without alignment problems.


---
**Anthony Bolgar** *June 11, 2016 12:11*

I was leaning towards a 4 post lead screw type. That allows you to even place the stepper outside the case if necessary for room.


---
**Don Kleinschnitz Jr.** *June 11, 2016 13:15*

**+Anthony Bolgar** that is how the LO table works.


---
**Anthony Bolgar** *June 11, 2016 13:19*

The lead screws would not have to be that long, we are only talking about a couple of inches of movement.


---
**Don Kleinschnitz Jr.** *June 11, 2016 13:50*

I posted a pict of the shaft on mine. So annoying that we cannot attach photo!


---
**Anthony Bolgar** *June 11, 2016 13:58*

I figured it would be pretty short, 4"


---
**Don Kleinschnitz Jr.** *June 11, 2016 14:42*

Yep, 4"


---
**Bob Buechler** *March 10, 2017 05:27*

Hi All. Ressurecting this thread because I'm curious how far along the group ultimately got to completing an open source version of LO's motorized Z-Axis table. Is this basically the current winner? [https://hackaday.io/post/51315](https://hackaday.io/post/51315)


---
**Abe Fouhy** *April 07, 2017 07:39*

**+Bob Buechler** Bump. What's the status of the clone? 


---
**Bob Buechler** *April 07, 2017 23:01*

I haven't heard any updates on this. The community regulars like **+Don Kleinschnitz** and **+Scorch Works** seem to be focusing on perfecting an easily replicable, spring-loaded clamping table first. It's lower tech, lower cost and requires less expertise to create. As such, it stands to provide a lower barrier to entry for new K40 owners (like myself, admittedly). From there, people can graduate to either a purchased powered table like LO's design, or building/refining that [hackaday.io](http://hackaday.io) solution.



Scorch Works's Design: [plus.google.com - Adjustable platform I made for my K40. (Freshly cleaned for the video.) http...](https://plus.google.com/+ScorchWorks/posts/UkrvPtra7UK?fscid=z12kellrwormyby5504cj5wwawqpslwwdis.1490350839700586) 


---
**Abe Fouhy** *April 08, 2017 06:43*

I was seeing that, but I was just curious. Thinking of cutting out the bottom and putting an auto z and focuser, but trying to see how.


---
**Don Kleinschnitz Jr.** *April 08, 2017 11:29*

**+Bob Buechler** **+Abe Fouhy**

Latest version of the clamp table is here, still waiting for shop time.

[donsthings.blogspot.com - K40 Clamping Table](http://donsthings.blogspot.com/2017/02/k40-clamping-table.html)



I find myself mostly doing .092 and .22 acrylic some 1/8" and 1/4 wood materials mostly cutting and some engraving.





I find for most of the above my lift table is overkill and annoying to work with. I know of two that have built the simpler Scorch table and like it.



However I do have the notion of engraving some bowls that I make which means a cut out of the bottom and a lift like the bigger machines is in the future ......far future1


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2017 11:37*

**+Don Kleinschnitz** For doing bowls I tested cutting a bowl shaped circle out of plywood & raising the plywood to the correct height. Allowed me to do the rim of the bowl easily. Unfortunately, deep bowls seemed like there was no way I could figure out how to etch the base without it being severely out of focus (due to the wall height coming too close to the laser head/rail). Only option I could think for that was to have a z-axis on the head to lower the head (or at least lens) to the level required to get it into focus after getting past the walls of the bowl.



edit: To clarify, that's the interior base I'm talking about, not the exterior.


---
**Don Kleinschnitz Jr.** *April 08, 2017 12:43*

**+Yuusuf Sallahuddin** I was thinking of the edge and the bottom mainly. I current sign the bottoms by hand pyrography. I would like the edges engraved as you suggested. I also want to use the laser to mark patterns on the edges for subsequent carving.



Then I imagine that some day a device that goes on my lathe that engraves the body. There is one project that engraves drum bodies with a diode laser I see as moving that way.



OMG this is an awe-full hobby for a ADHD person like me. 


---
**Abe Fouhy** *April 08, 2017 15:56*

**+Don Kleinschnitz**​**+Yuusuf Sallahuddin**​ for a bowl you could cut plywood it into 1/8-1/4" layers with the bottom layer having a hole for a rod to act as a spindle. Then have each layer attach to the spindle with support spokes on each layer going up, laminate them all together, cut the spokes and sand smooth.


---
**Bob Buechler** *April 08, 2017 17:06*

**+Don Kleinschnitz** re: "still waiting on shop time": Is there a parts list anywhere? I like the current design enough to try a test build, I just don't know what materials to buy. 


---
**Don Kleinschnitz Jr.** *April 08, 2017 18:32*

**+Bob Buechler** 

Not a formal parts list as its still a "prototype wannabe".



Warning: I haven't built one and haven't checked the drawings.



However you could build one from the Sketchup drawings.

Some parts # and sources are marked in their properties.



Roughly.....

Spring: MC Master Carr, spring rate 1lb/inch (see drwg). Make your own with music wire. Check YouTube for how.

1/4 Bolts: home depot 

Alum angle and flats: home depot

Handle: eventually 3D printed, you could substitute or print from SU.

#6 threaded rod: home depot

Extrusion: servo city (see drwg)

Extrusion hold down: servo city (see drwg)

Hold down bracket: Servo City (see drwg)

e-clip: Choose one to fit on machined groove.



The bolts need some machining for the eclip. 

... I have a lathe :)

... I have done this by hand in a drill or drill press cut the groove with a small file or hacksaw.

...You could use a pin vs an e-clip

...You can get an e-clip that fits tight on 1/4 shaft

... Take to a machine shop 



I updated the drwgs with a machining drawing for the bolt.



[3dwarehouse.sketchup.com - K40 clamping table 2.1](https://3dwarehouse.sketchup.com/model/3d278253-046b-4669-b05d-56a590a00662/K40-clamping-table-21)


---
**Bob Buechler** *April 15, 2017 20:39*

It occurred to me today that the stock K40 stamp engraving bed comes with two long springs that have a wide enough diameter built into the stamp clamp. I'm planning to salvage and reuse them on my forthcoming clamping bed. I will, of course, take lots of build photos and measurements as I go :)



![images/a0c071199d21837fd8ef282f9d930e4c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0c071199d21837fd8ef282f9d930e4c.jpeg)


---
**Don Kleinschnitz Jr.** *April 15, 2017 20:58*

**+Bob Buechler** LOL...

I have been staring at my original clamping table and asking myself "whats so wrong with this approach". If it was adjustable up and down it would create similar function to what we are doing.



Well it does not hold as large a piece and I don't have access to those kind of extrusions :).Thats a good enough excuse for me ...



Got my parts this week and now it has to wait until after my travels. Wondered what I was going to do about springs, Never did I think about using the friggin springs from the stock bed.... thanks. 





![images/7d3c7862eab6860eee13f2321f538fdf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d3c7862eab6860eee13f2321f538fdf.jpeg)


---
**Bob Buechler** *May 01, 2017 20:51*

Saw this video on Openbuild's G+ community, and thought it relevant to our interests here: 
{% include youtubePlayer.html id="cefOJ7PQHjg" %}
[youtube.com - C Beam GTC V2 Clamping Mechanism Demo](https://www.youtube.com/watch?v=cefOJ7PQHjg) 



Also, I'm midway through my own design's build and discovered a small design improvement. Good news is, the build can be achieved with off-the-shelf parts, a manual hacksaw, an electric drill (or press), a common vice and typical metric and phillips head screwdrivers. Additionally, the number of modifications are minimal. A couple holes in the aluminum, and cutting the materials to the appropriate lengths, and that's about it. 



I'm painfully slow still in Sketchup, so it's taking me a long time to get the design drafted. But I'll post a build log once it's done and installed.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/gG5FDYfXeMf) &mdash; content and formatting may not be reliable*
