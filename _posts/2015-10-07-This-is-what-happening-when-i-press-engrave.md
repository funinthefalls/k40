---
layout: post
title: "This is what happening when i press engrave.."
date: October 07, 2015 03:13
category: "Hardware and Laser settings"
author: "Raja Rajan"
---
This is what  happening when i press engrave.. Am i missing something? I ll post the laser drw settings in next post

![images/f04206e6c369308b2c91e001163ec537.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/f04206e6c369308b2c91e001163ec537.gif)



**"Raja Rajan"**

---
---
**Joey Fitzpatrick** *October 07, 2015 03:28*

Try jumping  the two wires that go to the laser switch (under the panel)  You may have a bad switch or bad connection to the power supply.  Also make sure that your power knob is not turned all the way down.  The laser will not fire under around 4ma .  You may need to turn the power up slightly until it will fire.


---
**Raja Rajan** *October 07, 2015 14:34*

Hi Joey,thanks for your reply! Yes the power know i set around 10-15 mA and i see the indicator is still in "0" when i click engrave  and the laser head is moving but the power is still in "0" .is that normal?


---
**Joey Fitzpatrick** *October 07, 2015 14:43*

No, it should show some current when engraving.  It sounds like the power supply is not receiving the Laser on signal from the mainboard.  It could be a bad connection or a bad mainboard.  Does the laser work if you try to cut something?


---
**Raja Rajan** *October 07, 2015 15:40*

No Joey,it's not cutting or engraving anything ,other than test switch.



When i press the test witch i see the laser beam is hitting my acrylic sheet and i see some spot in the sheet .do you know how to check whether laser is receiving the power supply from my mainboard? thank you so much for your kind replies


---
**David Wakely** *October 07, 2015 18:00*

Disconnect the machine from the mains and then Look on the main board and check all of the connectors are seated properly. Gently tug the individual wires inside the connector blocks carefully to see if they have come loose. Repeat the same steps on the power supply (ignore the connector with the AC power going into it - yellow red and black)



I think the cable you need focus on is labeled LO on the main board and G on the PSU. Not 100% sure though as I'm not near my machine!


---
**David Wakely** *October 07, 2015 20:09*

Actually I think on the PSU the laser fire is labelled K and is green


---
**Raja Rajan** *October 07, 2015 20:20*

Thanks David! Let me check and post you the updates !!


---
**Joey Fitzpatrick** *October 08, 2015 04:19*

Next thing you should check is the signal to the power supply(from the mainboard)  if the laser fires when you hit the test switch, but does not fire when cutting or engraving, the mainboard may not be sending the fire signal(or it may not be reaching the power supply)  The circuit in question would be labeled with a "L" on the power supply(it may be labeled with a K+ on your power supply).  This is the TTL input (active low, I believe)  you should check this circuit with a meter or oscilloscope, when cutting or engraving, to see if there is a signal being sent from the mainboard.(should be 5v when the laser is not firing and should be less than 1v when firing)  If you do have a signal, the power supply is the problem, if you do not have a signal, the mainboard or wiring is the problem.


---
**Raja Rajan** *October 09, 2015 18:56*

Thank you so much for the detailed reply Joey!

Let me check that and update you!


---
*Imported from [Google+](https://plus.google.com/110717916181924497071/posts/7nnanH2UBbi) &mdash; content and formatting may not be reliable*
