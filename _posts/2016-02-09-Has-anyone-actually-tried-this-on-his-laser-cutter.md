---
layout: post
title: "Has anyone actually tried this on his laser cutter?"
date: February 09, 2016 11:46
category: "Object produced with laser"
author: "Jeremie Francois"
---
Has anyone actually tried this on his laser cutter?

Now, I am not sure 40W is enough, it may take to long to be practical, but if it could melt solder paste it would be a nice and fully automated way to avoid cooking the PCB and components.





**"Jeremie Francois"**

---
---
**Stephane Buisson** *February 09, 2016 12:01*

**+Jeremie Francois** never heard something like that.

don't think laser power would be an issue, but the timming.

Finding the correct time exposure relative of the soldering past mass (which would vary for each point) would be a nightmare.


---
**Gary McKinnon** *February 09, 2016 13:58*

Isn't there a critical threshold with reflow soldering whereby the number of contacts you have to manually paste would take longer to do than the laser system ?


---
**Gary McKinnon** *February 09, 2016 14:01*

No, not a masochist, just new to reflow.


---
**Stephane Buisson** *February 09, 2016 14:27*

[https://plus.google.com/117750252531506832328/posts/ZMV7SnftDKu](https://plus.google.com/117750252531506832328/posts/ZMV7SnftDKu)


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2016 15:10*

I can 3D print a solder stencil that works decently well but I've been soldering my 0805s lately using a soldering iron and very thin solder wire.  Then I go over with some hot air to make sure everything is positioned properly. Not that bad at all.  


---
**Jeremie Francois** *February 09, 2016 15:59*

Thanks all! Given **+John Lauer** results, I guess it would need some tweaking indeed :D I'm not sure it would be nightmarish to compute the time needed according to surface for regular pads and components (hence mass of solder paste). But most probably not worth a cheap oven indeed... :)


---
**Stephane Buisson** *February 12, 2016 10:00*

**+Jeremie Francois** **+Peter van der Walt**  could be a fun project with a led laserdiode and a solder paste extruder. with that setting you could apply appropriate amount of energy to melt the amount of paste you extrude. (an extra tool for pick & place machine ?)


---
**Jeremie Francois** *February 12, 2016 11:43*

**+Stephane Buisson** one more reason to try and make my other half admit I need a laser cutter ;) -- she fears so much for my eyes, I should not have told her :D


---
*Imported from [Google+](https://plus.google.com/+JeremieFrancois/posts/SzESeS1Dkwk) &mdash; content and formatting may not be reliable*
