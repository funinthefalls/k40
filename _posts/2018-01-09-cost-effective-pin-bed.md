---
layout: post
title: "cost-effective pin bed:"
date: January 09, 2018 04:54
category: "Modification"
author: "Tech Bravo (Tech BravoTN)"
---
cost-effective pin bed: 
{% include youtubePlayer.html id="NTtNYks32e0" %}
[https://youtu.be/NTtNYks32e0](https://youtu.be/NTtNYks32e0)





**"Tech Bravo (Tech BravoTN)"**

---
---
**Scorch Works** *January 09, 2018 17:32*

How did you attach the studs to the holes in the plate?  Are they press fit?


---
**Tech Bravo (Tech BravoTN)** *January 09, 2018 17:32*

**+Scorch Works** the studs are threaded and come with screws


---
**Andy Shilling** *January 09, 2018 22:01*

I've got a perforated plate on mine and use these studs. I found that if I screw the 3mm thread in and cut the head off I can just position then where I need for cutting acrylic of wood and remove them when I can't get card to sit flat to cut.


---
**java lang** *January 11, 2018 19:27*

I'm using the same principal with an even more cheaper solution: using a tile (bottom side up) and super-glue nails with totally flat nailheads. Optionally you can fix the nails with a thin layer of  plaster. I'm very happy about this solution because I have practiacllly no reflection destroying my workpiece . The (black) nails are coming from a hardware-store in germany (BAUHAUS).  

![images/8ee76de0e61abbe9e957cac64ebfea8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ee76de0e61abbe9e957cac64ebfea8c.jpeg)


---
**java lang** *January 11, 2018 19:28*

Nails

![images/32226a49c133dd827344a9a8a497f1c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/32226a49c133dd827344a9a8a497f1c1.jpeg)


---
**Andy Shilling** *January 11, 2018 19:30*

**+java lang** now that's dedication, how long does that take to make?


---
**java lang** *January 11, 2018 19:34*

**+Andy Shilling** Well, it was a hard hour or maybe two, but it was worth, I'm using the bed one year now... 


---
**java lang** *January 11, 2018 19:41*

**+Andy Shilling** One more advantage of using a tile was that I  didn't have to mark the nail positions, because there was already a (diamond) structure with the wanted spacing. So I could immediately start with gluing :)


---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/Re65SZmepqo) &mdash; content and formatting may not be reliable*
