---
layout: post
title: "Single pass laser carving, love the smoothie"
date: December 25, 2016 16:56
category: "Object produced with laser"
author: "Mike Grady"
---
Single pass laser carving, love the smoothie

![images/364dfed1eff8023dab95be97d72f03c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/364dfed1eff8023dab95be97d72f03c9.jpeg)



**"Mike Grady"**

---
---
**Damian Trejtowicz** *December 25, 2016 17:11*

I'm trying to carving but never get effect like this

What software You use?Maybe you can share your workflow please?


---
**Mike Grady** *December 25, 2016 17:20*

Hi, i came across this forum post [forum.darklylabs.com - 3D Engraving using PicEngrave Pro 5](http://forum.darklylabs.com/index.php?p=/discussion/362/3d-engraving-using-picengrave-pro-5)



while i was checking out the emblaser2, in one of the comments he says he uses corel photo paint to edit the greyscale height map and adds a bump map so i tried it and voila!! single pass 3d carving. Use this link to find height maps [https://www.google.co.uk/search?q=%E7%81%B0%E5%BA%A6%E5%9B%BE&rlz=1T4GGNI_enUS476US476&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjikuz9__vPAhVJCsAKHTJkCKgQsAQIHQ&biw=1536&bih=735](https://www.google.co.uk/search?q=%E7%81%B0%E5%BA%A6%E5%9B%BE&rlz=1T4GGNI_enUS476US476&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjikuz9__vPAhVJCsAKHTJkCKgQsAQIHQ&biw=1536&bih=735)


---
**Damian Trejtowicz** *December 25, 2016 17:26*

I will try all this when weather allow me to put water to chilling circut,i dont want risk ice in my tube


---
**greg greene** *December 25, 2016 19:12*

I have to try this in LW4


---
**Mike Grady** *December 25, 2016 21:26*

I didnt know laserweb 4 was a working release ?


---
**greg greene** *December 25, 2016 23:21*

Still in Alpha but you can try it on the github site


---
**Mike Grady** *December 26, 2016 00:14*

Thanks **+greg greene** but i wouldnt know where to start to get that installed and running lol


---
**greg greene** *December 26, 2016 00:20*

Directions are on the site - it is a one click install


---
**Mike Grady** *December 26, 2016 00:23*

Cool have you got a link please ?


---
**greg greene** *December 26, 2016 00:31*

[https://drive.google.com/file/d/0B-UxBll2MapjX1NWMDVzWWhUbms/view?usp=sharing](https://drive.google.com/file/d/0B-UxBll2MapjX1NWMDVzWWhUbms/view?usp=sharing)


---
**Mike Grady** *December 26, 2016 00:53*

thats lw3 lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 01:08*

That's pretty spectacular. Going to have to take a look & give it a try sometime.


---
**Mike Grady** *December 26, 2016 01:22*

thanks **+Yuusuf Sallahuddin** it was a pain doing like 3 passes for 1 engraving, this works really well, my settings for the first engraving was 50 and 50 for black and white, laser power 10 to 100 percent, for the second was 50 white and 25 black same power, dont need to spend £1000+ for an emblaser now :)


---
**Mike Grady** *December 26, 2016 01:35*

forgot to ad pot set to 10ma


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 01:51*

**+Mike Grady** Thanks for sharing the settings Mike. By 1st & 2nd I'm assuming you are referring to Left & Right in your image? Personally I like the clarity/contrast in the right hand piece more, although the left piece still looks quite nice.


---
**Mike Grady** *December 26, 2016 02:11*

Yea youre right Yuusuf, left was first but i on the other hand prefer the first, maybe just a clean up issue with the right one :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 02:38*

**+Mike Grady** To me, the 2nd one looks like the darker (or lower) areas have been etched deeper. Based on your speed settings I would say that actually is the case too. Even if just fractionally, it gives a better perception of depth in the photo at least. That is, to my eyes anyway. I guess beauty really is in the eye of the beholder.


---
**Cesar Tolentino** *December 28, 2016 00:18*

So how do you clean it? Any images before u clean it?


---
**Mike Grady** *December 28, 2016 04:58*

**+Cesar Tolentino** a soft bristle brush and fairy liquid


---
*Imported from [Google+](https://plus.google.com/106807531578115449401/posts/SuRsMBgtGZz) &mdash; content and formatting may not be reliable*
