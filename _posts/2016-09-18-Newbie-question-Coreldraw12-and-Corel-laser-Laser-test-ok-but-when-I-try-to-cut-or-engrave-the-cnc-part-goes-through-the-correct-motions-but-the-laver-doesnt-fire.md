---
layout: post
title: "Newbie question. Coreldraw12 and Corel laser. Laser test ok but when I try to cut or engrave the cnc part goes through the correct motions but the laver doesn't fire"
date: September 18, 2016 19:51
category: "Original software and hardware issues"
author: "Ken Swanson"
---
Newbie question. Coreldraw12 and Corel laser.  Laser test ok but when I try to cut or engrave the cnc part goes through the correct motions but the laver doesn't fire.  The laser properties shows the board as laserB1 but I have no corresponding "printer" in Corel draw. The only choices on the printer properties is wmf. How do I get Coreldraw to recognize the laser?  I've tried hardware install in Windows but it won't recognize plug and play nor will it let me use setup for the inf. file. I've tried quite a few different things which are all a blur now. I figure it's some little check box somewhere but where?  Thanks





**"Ken Swanson"**

---
---
**greg greene** *September 18, 2016 20:10*

is the 































'laser







' button depressed and the pot turned up a little?

One way to check if the laser works is to depress the laser button and then the test button while turning the control pot clockwise, if the meter needle moves all is ok


---
**Jim Hatch** *September 18, 2016 21:35*

Did you get the CorelLaser driver to install? If you did, you should start CorelLaser <b>not</b> Corel Draw. That will enable the laser interface and fire up Corel Draw for you. Once there you'll see a handful of icons on the upper right icon bar for laser settings, cutting, etc. You click on the one that you want. Note - after it displays another dialog for other settings, those little icons will probably disappear but they are down on the bottom of your Windows taskbar over on the right near the date, time etc or in the menu that shows when you click on the little up arrow if you authorize your icons in Windows.



Also, if you have a Nano board the setting should be the only one with M2 in it in the drop down in the settings. You also have to get the serial number off the board (it's alphanumeric) and put that in the settings too.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 18, 2016 22:24*

See: 

[drive.google.com - Check CorelDraw Settings.png - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)

&: 

[https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)



for screenshots on setting the settings for CorelLaser.


---
**Ken Swanson** *September 19, 2016 15:41*

See I told you it was probably something simple =ME. The mirrors came nicely aligned from the factory (set and forget) except that in my zeal to vacuum every last bit of styrofoam beads out I had dislodged the middle mirror. Once I fixed that every thing was fine. Let the fun begin!  Thanks for help


---
**greg greene** *September 19, 2016 15:42*

And the smoke shall rise !


---
*Imported from [Google+](https://plus.google.com/114163734058037735666/posts/3EsH6cPVveC) &mdash; content and formatting may not be reliable*
