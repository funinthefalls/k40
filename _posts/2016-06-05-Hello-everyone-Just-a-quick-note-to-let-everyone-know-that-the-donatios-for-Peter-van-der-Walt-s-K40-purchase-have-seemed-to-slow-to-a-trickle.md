---
layout: post
title: "Hello everyone, Just a quick note to let everyone know that the donatios for Peter van der Walt 's K40 purchase have seemed to slow to a trickle"
date: June 05, 2016 16:39
category: "External links&#x3a; Blog, forum, etc"
author: "Anthony Bolgar"
---
Hello everyone,



Just a quick note to let everyone know that the donatios for **+Peter van der Walt** 's K40 purchase have seemed to slow to a trickle. We only need to raise another $150, so all it would take is 30 people out of the 1500 members here to donate $5 to achieve the goal. I know we can get this wrapped up quickly, so that Peter will have a K40 to provide us with some great open source software (and probably hardware, know him) to use with our K40's



Here is the link: [https://openhardwarecoza.github.io/donate/](https://openhardwarecoza.github.io/donate/)





Thanks in advance for your support,

Anthony Bolgar





**"Anthony Bolgar"**

---
---
**Ned Hill** *June 05, 2016 21:10*

Just donated $25 so $125 left.


---
**Anthony Bolgar** *June 05, 2016 22:30*

Thanks Ned, it is much appreciated.


---
**Anthony Bolgar** *June 05, 2016 22:31*

Every dollar helps Susan. Again, thank you.


---
**HalfNormal** *June 06, 2016 01:20*

Check is err, actually done by PayPal. 


---
**Anthony Bolgar** *June 06, 2016 01:58*

Thanks **+HalfNormal** 


---
**Joe Alexander** *June 08, 2016 01:02*

just donated a nice chunk, time to update the page :)


---
**Anthony Bolgar** *June 08, 2016 13:08*

Thanks a lot Joe!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/gMc9QKE2p8C) &mdash; content and formatting may not be reliable*
