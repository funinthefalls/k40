---
layout: post
title: "Gotta love stock mirrors. Only marginally better after cleaning"
date: June 16, 2016 00:24
category: "Original software and hardware issues"
author: "Ned Hill"
---
Gotta love stock mirrors.  Only marginally better after cleaning.  Thankfully I had already purchased new optics from LO.

![images/789ed537b1f08ee5d72b98f1a66da9e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/789ed537b1f08ee5d72b98f1a66da9e0.jpeg)



**"Ned Hill"**

---
---
**Ned Hill** *June 16, 2016 00:29*

Just for the record, that's even before firing my first test shot.


---
**Jim Hatch** *June 16, 2016 00:46*

Wow. Amazing. Saltwater corrosion from shipping?


---
**Ned Hill** *June 16, 2016 00:56*

Fairly certain the dark spot in the middle and the halo around it was from something being on the mirror when they were testing at the factory and got cooked by the laser.  The dark spot came off with cleaning, but the halo is still there.  Not sure about the other stuff around the edges which only partially came off with cleaning.


---
**Alex Krause** *June 16, 2016 00:59*

I thought you were lasering pvc at first glance


---
**Pippins McGee** *June 16, 2016 03:28*

I checked the mirror inside laser head and found this also

[http://i.imgur.com/I9z1mks.jpg?1](http://i.imgur.com/I9z1mks.jpg?1)



the other 2 mirrors were spotless.

I used the machine for a week before checking this mirror. Is it possible I caused this or is this long-term corrosion that the seller obviously tried to hide inside the laser head out of sight?



Should we open ebay disputes to have the seller refund the cost of new mirrors?

Or just wear this as a loss and buy new mirrors



Edit: i bought new mirror from China - going to take a few weeks to get here.. 

[http://www.ebay.com.au/itm/252091384203?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/252091384203?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Ned Hill** *June 16, 2016 03:59*

Yeah the laser head mirror is the one I'm showing as well.  The other 2 were in better condition.  I looked at the mirrors under my microscope and it appears there is some reaction going on between head and the mirror blanks.  From what I understand the coating on these mirrors is suppose to be gold.  Not sure what the bulk of the mirror (the blank) is made of other than it appears to be some metal.  The laser head appears to be aluminium but I'm not sure what grade.  The laser head has the most contact area between the mirror and the mount as compared to the other two mounts so that appears to be why the laser head mirror shows the most damage.  Could possibly be some galvanic corrosion between the head and the mirror blank as there is moisture present from the residual in the laser tube jacket.


---
**Ned Hill** *June 16, 2016 04:26*

**+Alex Krause** Yeah the HCl vapor from the PVC would play hell with the aluminium.  Lol I wonder if they tested the laser at the factory using a piece of PVC?  


---
**Alex Krause** *June 16, 2016 04:41*

**+Ned Hill**​ chlorine gas is nasty stuff I work in a PVC plant and we machine it... it off gasses in the lathes and corrodes all the metals inside after 6 months looks like our lathes are 15 yrs old


---
**Ned Hill** *June 16, 2016 12:48*

**+Pippins McGee** I went into the purchase knowing I was going to replace the mirrors from the start because this groups consensus was that they were crap.  If you open a dispute with the seller let me know and I may follow suit.


---
**Pippins McGee** *June 16, 2016 13:43*

**+Ned Hill** I didn't open a dispute, I just messaged the seller letting them know that I found a mirror is corroded and damaged and asked if they kindly refund the cost of a new mirror, as the most convenient solution.

They agreed to refund me the $16 I paid for a new mirror from China.



it's more so the principle, sellers have to take responsibility for the quality of their items. if something is damaged it should be out of their pocket not ours.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/SZADLhtq3oC) &mdash; content and formatting may not be reliable*
