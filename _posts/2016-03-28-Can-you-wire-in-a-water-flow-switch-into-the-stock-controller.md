---
layout: post
title: "Can you wire in a water flow switch into the stock controller?"
date: March 28, 2016 23:28
category: "Modification"
author: "Tony Sobczak"
---
Can you wire in a water flow switch into the stock controller?  If so how would I do it? +5 , GND AND signal line out.





**"Tony Sobczak"**

---
---
**HP Persson** *March 29, 2016 00:07*

Flow meter just gives one (or is it two?) signals per revolution of the impeller, not much to do with that signal without a IC taking care of it.

My k40 cannot take care of that signal, but there is alot of versions out there, so never say never ;)



A Arduino Mini clone is $5 on Fleebay, and the code is openly avalible, and a relay is about $1.

I am working on a simpler controller for the flow, it checks if there is enough flow and lights a LED and if it´s not, it closes the relay and blinks the LED. 

There is some posts here about other code that do alot more, with displays and whatnot you can edit, if you are into programming.



Personally i keep everything added to the K40 far far far away from the stock controller and cabling, just to be sure i dont introduce more points of failures :)

All electric stuff i added have it´s own 5A PSU.


---
**Anthony Bolgar** *March 29, 2016 00:11*

There are simple flow switches that close a contact while water is flowing, opens contact when no flow. You would wire it across the P1 and P2 terminals on the power supply (THat is how the laser enable button works, it closes the P1 to P2 circuit. So just wire the sensor in series with the laser enable switch)




---
**HP Persson** *March 29, 2016 00:18*

I was thinking of flow meter, not flow switch. (doh!)



**+Anthony Bolgar** That´s a easy and good solution to control the water flow.


---
**Stephane Buisson** *March 29, 2016 08:34*

**+Anthony Bolgar** I did my mod a year ago, but if I remember well it's the other way around. if open laser switch is able to fire, if closed (5V) then the it's at the same level than IN so no fire.


---
**Anthony Bolgar** *March 29, 2016 12:08*

It must depends on the power supply, on mine, you must CLOSE the circuit between P1 and P2 on the power supply to enable the laser.


---
**Don Kleinschnitz Jr.** *March 29, 2016 14:09*

Put  a flow switch in series with the "Laser Switch" Button that is on the panel. You can also use this circuit to add a cover interlock. On mine I wired a PTC temp controller in series with both of those. Now the cover, the switch, the flow and the temp mus be right to fire the laser. If I knew how to add an attachment I would share my K40 schematic.


---
**Stephane Buisson** *March 29, 2016 16:00*

**+Don Kleinschnitz** use a service like dropbox or google drive to share a file, then post the link here


---
**Anthony Bolgar** *March 29, 2016 16:31*

Don, that is exactly what I was suggesting. On my laser the switch is labeled Laser Enable, and that connects to PO1 and P2 on my powersupply.


---
**Will Travis** *March 29, 2016 23:58*

**+Don Kleinschnitz**, I would love to see the schematic. Thanks. 


---
**Don Kleinschnitz Jr.** *March 30, 2016 13:02*

It is ludicrous that we cannot post files on G+ and I don't like giving public access to my shares. Email me at don_kleinschnitz@hotmail.com and I will send it to you.


---
**Tony Sobczak** *March 30, 2016 13:52*

The item is called a switch but as I Said in the original post it has a signal line out. Is there any place that that can be wired in.﻿ ﻿ This is the switch someone else suggested (can't find that post again).



[http://m.ebay.com/itm/Mini-G1-4-quot-Water-Coffee-Water-Purifier-Flow-Hall-Sensor-Switch-Meter-Flowmeter-/390944279498?txnId=633952331026](http://m.ebay.com/itm/Mini-G1-4-quot-Water-Coffee-Water-Purifier-Flow-Hall-Sensor-Switch-Meter-Flowmeter-/390944279498?txnId=633952331026)


---
**Stephane Buisson** *March 30, 2016 16:59*

nope **+Tony Sobczak** , it was in my mod with a Smoothie, but this is HALL EFFECT not a switch, So not possible with controller stock.

you need more electronic and the software to make it reconized. possible on Smoothie but I never finish it ...


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/e2a3KHjwMcy) &mdash; content and formatting may not be reliable*
