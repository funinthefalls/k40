---
layout: post
title: "Can I clean the lenses on my K40 with methylated spirits???"
date: April 27, 2016 02:05
category: "Discussion"
author: "Donna Gray"
---
Can I clean the lenses on my K40 with methylated spirits???





**"Donna Gray"**

---
---
**Anthony Bolgar** *April 27, 2016 02:20*

Not sure about that, I use denatured alcohol 90%


---
**Donna Gray** *April 27, 2016 02:23*

Where do you buy that type of alcohol


---
**Anthony Bolgar** *April 27, 2016 02:29*

I get it at the pharmacy.


---
**Donna Gray** *April 27, 2016 02:33*

OK thanks will see if I can get some how often should I clean the lenses?? Oh and is a small flame while cutting MDF really bad for the focal lense


---
**Rob Morgan** *April 27, 2016 02:57*

Isopropel alcohol works.  Pretty sure flames and soot just muddy the lense so you have to clean it more cut quality could diminish during cutting if the lense gets dirty. 


---
**Anthony Bolgar** *April 27, 2016 02:57*

I tend to clean the mirrors and lens almost every time I use it. I highly suggest getting an air assist nozzle, this will keep flames and flare ups to a minimum, helps with cutting thicker materials, and keeps the majority of smoke away from the lens. Lightobject.com has a great replacement laser head for $18, you need to upgrade to an 18 mm lens if you use this air assist head, around $30 for a lens. You can use a small airbrush compressor as the air supply, but I have heard people having success using an aquarium air pump which can be had for around 20 -30 dollars, so for under $100 you can do the upgrade, you will be amazed at the difference it makes.


---
**Donna Gray** *April 27, 2016 03:09*

is the air assist head easy to assemble in the machine I am not mechanical I tried with cool air from a hairdryer while cutting but then it didn't cut Rob Morgan Anthony Bolgar there was no flame but it only cut about 1 mm down not all the way through 3mm MDF


---
**Anthony Bolgar** *April 27, 2016 03:17*

The head is very easy to install, I use a coiled piece of 1/4" air hose to feed the nozzle, it keeps the air line out of the nozzles way.I will try to post a picture tomorrow morning for you.


---
**Vince Lee** *April 27, 2016 03:47*

Methylated spirits is another name for denatured alcohol, aka ethanol plus poison additives so you cannot drink it. [http://www.thesodacanstove.com/alcohol-stove/denatured-alcohol.html](http://www.thesodacanstove.com/alcohol-stove/denatured-alcohol.html)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 03:56*

I had been cleaning my lens & mirrors with Methylated Spirits. However, I found that over time I would end up with some buildup (that looked like toothpaste splashed on mirror) from it. I then started cleaning it with plain water & seems to have removed the buildup.


---
**Anthony Bolgar** *April 27, 2016 04:01*

**+Vince Lee** Thanks for the info on the names. Never new they are the same thing.lol!


---
**Donna Gray** *April 27, 2016 05:15*

I have just cut some more 3mm MDF and just shot the hairdryer in if a flame appeared seems to be working  Can anyone give me a hint on speed and power settings power eg mA  I have been using about 6mA and 80mm speed is this OK?


---
**Donna Gray** *April 27, 2016 05:16*

And thanks about the method I will they it as I already have some to clean the lenses


---
**Donna Gray** *April 27, 2016 05:17*

Sorry that,should say metho 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 05:42*

**+Donna Gray** I use about 10mA @ 25mm/s speed & 2 passes to get through 3mm MDF/Plywood.


---
**Vince Lee** *April 27, 2016 05:58*

**+Anthony Bolgar**​ my wife corrected me that methylated spirits is a kind of denatured alcohol, the latter just meaning ethanol with additives to make it undrinkable.﻿  Methanol is commonly the primary additive (hence methylated) though not always.  Though I think the term is often used generically too.


---
**Ben Walker** *April 27, 2016 13:30*

[https://plus.google.com/100981051586034517976/posts/K5eazfE7LJ3](https://plus.google.com/100981051586034517976/posts/K5eazfE7LJ3)

this is 100lb card stock with the original lens.  I was practicing with paper before moving on to other materials first.  No charring at all.  Don't know why ymmv.


---
*Imported from [Google+](https://plus.google.com/103145403582371195560/posts/KiS7NHA5pgg) &mdash; content and formatting may not be reliable*
