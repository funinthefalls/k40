---
layout: post
title: "Hi All, I love this place thank you for being here!"
date: September 24, 2016 19:19
category: "Modification"
author: "Digory S"
---
Hi All, I love this place thank you for being here!  I am having some weirdness and hope maybe someone here can help me out.   I swapped out my original board for a RAMPS board, added air assist and put in a limit switch between the lid and the power for the Laser PSU.  The last step was to align the laser.   Oh well..... fried the first mirror :)  So here is the weirdness.  With the Laser Switch on, if you turn up the Current Regulator Pot about half way the laser turns on without pressing the test switch.  It turns on and stays on!  Is this normal?  

My PSU looks like the picture below and is hooked up as follows. H is not connected to anything.  L is connected to Pin D5 on the RAMPS board and to one side of the Test Switch.  WP is hooked up to one side of the Laser Switch . G is hooked up to one side of the Current Regulator Pot, one side of the Laser Switch, one side of the Test Switch and a ground post on the 24V PSU.  (oddly the two ground posts on the 24V PSU do not show continuity.)  IN is connected to pin D6 on the RAMPS board and center pin on the Current Regulator Pot. 5V is connected to the remaining pin on the Current Regulator Pot.  If you got this far thank you!  Any ideas?

![images/8e0cee5d17b0bde8f13cddcf55c632b7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8e0cee5d17b0bde8f13cddcf55c632b7.png)



**"Digory S"**

---
---
**Scott Marshall** *September 24, 2016 19:57*

The PSU is negative logic. 0v fires. 5V shuts it down. If your RAMPS is feeding 3.3V, it's not a full "off" command.



I have an full PSU breakdown, but can't post it here. If you  send me your email, I'll send it to you.



ALL-TEKSYSTEMS.com


---
**HalfNormal** *September 24, 2016 20:10*

You should not have the pot in the circuit at all unless it is on a by pass switch. This link should show you how to properly connect the ramps to your powersupply. It might be a bit different but you can find all you need with a quick search.

[github.com - presentations](https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion)


---
**K** *September 29, 2016 21:19*

So the RAMPS/ExplodingLemur setup should NOT use the pot or the test fire switch, correct? 


---
**HalfNormal** *September 30, 2016 03:31*

They can be used in an OR configuration not AND. This means wired up to use one control OR the other not AND which means together. 


---
*Imported from [Google+](https://plus.google.com/111252549481082686266/posts/GJE2XtisXjT) &mdash; content and formatting may not be reliable*
