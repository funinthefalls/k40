---
layout: post
title: "Project from this weekend with my daughter"
date: June 27, 2017 03:19
category: "Object produced with laser"
author: "Ned Hill"
---
Project from this weekend with my daughter. We made a shadow box from 3mm birch ply to hold a pair of angle wing shells we found at the beach.   



![images/35447ace143f3dcb81de3da63c587bda.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/35447ace143f3dcb81de3da63c587bda.jpeg)
![images/ff656d885a34ed31c7eddb66a89beaf5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff656d885a34ed31c7eddb66a89beaf5.jpeg)

**"Ned Hill"**

---
---
**Ariel Yahni (UniKpty)** *June 27, 2017 03:30*

Another amazing project. Well done


---
**Ned Hill** *June 27, 2017 03:31*

Thanks :D




---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/DE7UwHpwc1e) &mdash; content and formatting may not be reliable*
