---
layout: post
title: "Need help.. running cermark on ss and not getting good adhesion after about 40 hours of running"
date: March 29, 2018 02:29
category: "Original software and hardware issues"
author: "Mark Harper"
---
Need help..  running cermark on ss and not getting good adhesion after about 40 hours of running.   Black rubs off the parts.  If change to a new tube, it works well again.  It cant be the tube getting weak that quickly, can it?  Is there a water issue?  Cleaning? Power supply?  





**"Mark Harper"**

---
---
**Anthony Bolgar** *March 29, 2018 03:00*

Check the temperature of the cooling water. As the temp increases the power output of the tube decreases.


---
**Phillip Conroy** *March 29, 2018 05:57*

What ma you running


---
**Mark Harper** *March 29, 2018 12:16*

I think the ma is the problem.  I just read last night about running no higher than 16.  I have a machine with no meter on it.  Been running this job at 95% power.  Installed a meter last night only to find out that's 30 milliamps.  Looks like I've been frying the tubes and was not aware that I was frying them.


---
**Mark Harper** *March 29, 2018 12:16*

Has anyone put together a basic quick-and-dirty guide to these machines since they come with nothing?


---
**Don Kleinschnitz Jr.** *March 29, 2018 12:35*

**+Mark Harper** 



#K40DigitalPanel

#K40TubeFailures



...there are many "guides" linked in this community and my blog has info organized in an index. What kind of guide are you looking for? Perhaps we need to add something.



Did you see the "Getting Started" post? 



What kind of machine? 

Laser Power?

Controller configuration?

How many tubes have you used?


---
**Mark Harper** *March 29, 2018 12:48*

K40 stock machine, stock controller (M2), have used 4-5 tubes now.  Currently running a CloudRay 40W tube.  I poked around on here but havent gotten far as links were broken, ran out of time, etc. 


---
**Don Kleinschnitz Jr.** *March 29, 2018 13:57*

**+Mark Harper** 

Wow, that,s a lot of tubes depending on what time span that includes.



If you get some time, letting us know what links are broken enables us to try and fix them.



.............

Have you looked at this blog and where there broken links?:



[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



.....................



I add some warnings about the impact of not watching tube current when using the digital meter, to this post.



[donsthings.blogspot.com - Understanding the K40 Digital Control Panel???](http://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html)






---
**Mark Harper** *March 29, 2018 14:04*

Tube life has been horrible.  40-60 hours for each one.  The tubes still fire and work for wood engraving and lighter operations, but not for Cermark which is what I need.  Now that I installed the ammeter I can stop smoking the damn things.  Thanks for the links - I will do some more browsing this weekend. 


---
**Anthony Bolgar** *March 29, 2018 15:15*

There must be something else affecting tube life than just the current, as I run tubes at 95% power and am into the hundreds of hours without failure. When running at high mA, adequate cooling is a MUST!


---
**Mark Harper** *March 29, 2018 15:28*

I am running in my garage (Chicago) and monitor water temperature (by feel) throughout the day.  It never gets above luke warm.. even then I will add ice just to be safe.    Another part of this - it's not 100% failure.. I end up with lines that wipe away in the middle of the text.  Not uniform by any means.  When the tube is new, I get consistent black.  40+ hours later, I get streaks, and some peices may completely wash away.  


---
**Mark Harper** *March 29, 2018 15:30*

I am using distilled water, btw.  Is there a better alternative?


---
**Don Kleinschnitz Jr.** *March 29, 2018 15:52*

**+Mark Harper** distilled water is the right choice.... I would get a temp monitor though and interlock the laser power with it. Again, links on my blog.


---
**Don Kleinschnitz Jr.** *March 29, 2018 15:54*

**+Anthony Bolgar** true that, but 30 ma is a lot for a 40W laser??? I did not think the LPS would go that high with a 40W.



**+Mark Harper** you have a stock LPS?


---
**Mark Harper** *March 29, 2018 16:21*

Yes, stock PS.  Put in the 30ma ammeter like in your blog, at 99% power it hits 30ma.


---
**Mark Harper** *March 29, 2018 16:25*

Even hits 30ma with the tube I am questioning.  Is it possible the PS is not sending a good signal to the tube and giving me these inconsistent results?  Controller?  I have a spare PS and controller from a parts machine I picked up. 


---
**Don Kleinschnitz Jr.** *March 29, 2018 17:58*

**+Mark Harper** the reading of 30 ma, unless it's not reading correctly, suggests the power supply is working. Albiet that higher than I have seen in normal operation. 

Could there be an intermittent optical problem other than the tube, that is making the power at the surface change like this? 

. 

This is were a power meter would be valuable. 


---
**Mark Harper** *March 29, 2018 18:05*

Not sure... I will try running some parts tonight with the new ammeter and watch it.  Should I still try to run at 16ma?


---
**Don Kleinschnitz Jr.** *March 29, 2018 22:49*

**+Mark Harper** have you done an optical alignment? I would keep the current below 20 ma until you figure out what is going on.



Try some wood cutting at 20 ma and see what it will do.



You could hold a piece of acrylic (1/4") in the path at various points and pulse the laser a fixed period to see if the beam penetrates the same amount in front of each mirror an at the table. 



What are you using for the table?


---
**Mark Harper** *March 29, 2018 23:23*

Yes, I have aligned it.  I made my own adjustable height table from expanded metal and made a height gage for setting focus.  



I have past experience with a Trotec machine so I can make my way around ok.. K40 is definitely a different animal though


---
**Mark Harper** *March 30, 2018 22:23*

Put in a new tube, aligned and testing.  38% power = 16ma.  Not sure what to think.  ![images/a0004fb64da653b22faed745efbeb98d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0004fb64da653b22faed745efbeb98d.jpeg)


---
**Don Kleinschnitz Jr.** *March 30, 2018 22:30*

**+Mark Harper** lets make sure the panel is working as expected. When you set the power to 50% what voltage does the IN pin on the LPS read to ground. Should be about 2.5VDC?


---
**Mark Harper** *March 30, 2018 22:42*

2.95vdc at 50%


---
**Don Kleinschnitz Jr.** *March 30, 2018 23:14*

**+Mark Harper** nothing seems wrong but there is! :( 


---
**Mark Harper** *March 31, 2018 00:47*

Well, I'm making parts again, 38% power.. seems to be working fine.  Maybe now that I'm not over driving I won't have odd missing lines 


---
**Mark Harper** *March 31, 2018 00:55*

What do you think - I have a hot LPS?  Mis-calibrated control?   


---
**Mark Harper** *April 09, 2018 00:25*

It's been a week, still can't get the Cermark to stick.  Swapped the LPS, no change.  Tried all different settings.. previously 80mm speed and 95% power worked.  I'm at 97% power and 20mm speed.  I can get some to stick, but not the entire thing.  I'm about mad enough to chuck this thing, but somehow I have to finish this job.


---
**Mark Harper** *April 09, 2018 11:50*

What brand of tube are you guys using?  Beginning to think the switch to CloudRay was not a good one.


---
**Don Kleinschnitz Jr.** *April 09, 2018 12:43*

Frustrating ...... Nothing seems obviously wrong here other than progressive loss of optical power. 



Is this problem now only with Cermack marking? Can the machine cut properly?






---
*Imported from [Google+](https://plus.google.com/114616626267283601603/posts/9rn3tcXAkNm) &mdash; content and formatting may not be reliable*
