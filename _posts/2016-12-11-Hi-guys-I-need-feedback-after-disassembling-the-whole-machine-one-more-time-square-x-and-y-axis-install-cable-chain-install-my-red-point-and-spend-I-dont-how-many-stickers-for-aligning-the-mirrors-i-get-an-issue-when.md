---
layout: post
title: "Hi guys, I need feedback!! after disassembling the whole machine one more time, square x and y axis, install cable chain, install my red point, and spend I dont how many stickers for aligning the mirrors, i get an issue when"
date: December 11, 2016 18:25
category: "Discussion"
author: "Antonio Garcia"
---
Hi guys,

I need feedback!! after disassembling the whole machine one more time, square x and  y axis, install cable chain,  install my red point, and spend I don´t how many stickers for  aligning  the mirrors, i get an issue when i put the final cup of the air assistant head. 

It seems my bean is not completely straight, from the lens to the material there is a different around 1 cm (if i put a paper just below the lens is fine, but when i put it in the same level of material, there is an offset (1cm)) and if put the cup for connecting my air assistant the beam hit in any part of the cup and does not come out :(, if I remove the cup work really well.

The beam in almost all parts of the bed is almost in the center of the head (+/- 0.1-0.05mm) (see picture), so any recommendation before make a bigger hole in the cup??

![images/cef66e51795ca781319d1f39f85ffc23.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cef66e51795ca781319d1f39f85ffc23.jpeg)



**"Antonio Garcia"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 18:44*

From the sounds of your explanation, the beam is exiting the lens on an angle. Possibly shim your cup or head piece (the opposite direction) or check that the carriage plate is level.


---
**Antonio Garcia** *December 11, 2016 20:18*

I think the carriage plate is reasonability leveled, so I´m gonna try to shim the cup, but the position that I have to place it in order to beam come out, is not easy task!!!! 

but it doesn´t make sense to me, if the beam enters in the head almost straight, and the carriage is reasonability leveled, why do i have 1 cm offset?? it´s weird... at least I can´t understand it, the only idea could be the lens (lightobjects) is wrong, but that idea is unlikely....

![images/0db396e08548a58b3dfa9d92ce663281.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0db396e08548a58b3dfa9d92ce663281.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 20:48*

**+Antonio Garcia** So it's appearing offset on the left-to-right of the material? Maybe the lens has a scratch? Lens orientation correct (convex side up)?


---
**Ariel Yahni (UniKpty)** *December 11, 2016 20:50*

**+Antonio Garcia**​ the carriage can be on level but is it parallel to the work piece? 


---
**Phillip Conroy** *December 11, 2016 21:01*

I also had this problem ,tryed simming laser head however could not get it working,s i just drilled out hole ,then to keep air assist working placed a couple of layers of ele trical tape over hole and laser beam cut a small hole for the air assist,not very neat but works


---
**Antonio Garcia** *December 11, 2016 21:06*

**+Ariel Yahni** there is +/- 0.05 between the beginning of x axis and the end of x axis, i could say yes, it´s leveled!!!

![images/38df25da30ac0042c540aa35119db8cf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/38df25da30ac0042c540aa35119db8cf.jpeg)


---
**Antonio Garcia** *December 11, 2016 21:06*

![images/85f8f0715e3c61910414ca675d712c66.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/85f8f0715e3c61910414ca675d712c66.jpeg)


---
**Antonio Garcia** *December 11, 2016 21:11*

**+Phillip Conroy** That´s my last option, but i would like understand why it´s happening :) because it does not make sense to me, 1 cm offset is too much...


---
**Ariel Yahni (UniKpty)** *December 11, 2016 21:41*

Maybe the mirror holder is not at 45? Have you placed a small paper inside the cone, out the cone in, do a small fire to see the in the cone is hitting? Maybe do that in all 4 corner to se if it hits the same spot. Also rotating the mirror holder will/can change the direction of the beam 


---
**Antonio Garcia** *December 11, 2016 23:01*

**+Ariel Yahni** I´m gonna try that... Thanks for the tip!


---
*Imported from [Google+](https://plus.google.com/+AntonioGarciadeSoria/posts/f3FPaPzvhNy) &mdash; content and formatting may not be reliable*
