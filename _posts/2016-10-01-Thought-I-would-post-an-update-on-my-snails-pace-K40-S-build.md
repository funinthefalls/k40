---
layout: post
title: "Thought I would post an update on my \"snails pace\" K40-S build"
date: October 01, 2016 00:31
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Thought I would post an update on my "snails pace" K40-S build. As you recall I posted the Sketchup model for this approach which keeps the power supplies internal and adds an external controller module with everything else.



The controller, DC power, control panel, interlock interface and AC control panel are all modular and can be assembled, tested and dissembled separately. The controller and associated packaging design can be used for other projects .... my CNC?



Most everything is installed and all wiring but the interlocks is complete. I am hoping for "power up" tomorrow.



I plan to paint the inside surface of the enclosure blue but for this version elected to keep it clear so I can see everything. The front cover (not shown), swings down for access to the SD card and cables. All the enclosures/modules parts can be laser cut.



Note: The temp meter below the controller will be moved to the main panel next to the current meter and that panel will be repainted. 



This modular design supports interlocks with easy wiring and interlock open indicator, temp measurement, smoothie with cable strain reliefs and access to SD card, LCD panel with SD access,  modular lift table, water pump, cabinet lights,  4 relay controlled AC outlets that can be control panel & processor controlled (air, vacuum and two spares) , one outlet that comes on with main power (water pump), two spare AC outlets. 6 fused DC circuits with plenty of 24V and 12VDC power. A few spare connections and buttons for stuff I forgot.



The lift is also modular with its controls on the lift. The K40 24VDC supply powers the lifts stepper drivers and controller.





![images/4f67fd9c36d30d445f69f2abb2df9b34.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f67fd9c36d30d445f69f2abb2df9b34.jpeg)
![images/d589797dbd3ebd99a0c63a2d3cf1ac60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d589797dbd3ebd99a0c63a2d3cf1ac60.jpeg)
![images/5fb5cb6f6ea3b4cc8f08005b2b0792b9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5fb5cb6f6ea3b4cc8f08005b2b0792b9.jpeg)
![images/58d1a9b82d7c472a236a9632ff5ed74b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58d1a9b82d7c472a236a9632ff5ed74b.jpeg)
![images/9606ab1ac08189f286ce0caa6229c4bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9606ab1ac08189f286ce0caa6229c4bd.jpeg)
![images/0cfc791be8a427d0b2b9b5128a2ad9a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0cfc791be8a427d0b2b9b5128a2ad9a8.jpeg)
![images/c90c764e9d698fc50c035a136dab663d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c90c764e9d698fc50c035a136dab663d.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *October 01, 2016 00:47*

Amazing **+Don Kleinschnitz**​. I will need something like that for my setup


---
**Don Kleinschnitz Jr.** *October 01, 2016 00:59*

The packaging and schematics are avail to use if you need them, returning the help I got so far.

Ya you used up all your internal space ...:)


---
**Ray Kholodovsky (Cohesion3D)** *October 01, 2016 01:23*

That's still a faster snail than mine Don :)


---
**Phillip Conroy** *October 01, 2016 04:55*

Why use the crap stock extractor housing when you could have straight down draft extraction,thus preveting fumes running acrss and marking the work peace 


---
**Don Kleinschnitz Jr.** *October 01, 2016 10:48*

**+Phillip Conroy** Do you mean the extractor that is inside the laser compartment (there is not pictures above of that)? I plan to remove it after this hack is working as the gantry will need to come out. I dont know yet if I will just remove it and leave the rear exit or extract out  the bottom. I plan to eventually cut out the bottom to accommodate a lower lift table.


---
**Phillip Conroy** *October 01, 2016 20:38*

Down draft extraction is the best way to avoid any fume marks on rear of cut stuff,if i did nog have a wooden desk i would have this mod in a heatbeat,it also helps to hold the wprk peace in place 


---
**Phillip Conroy** *October 02, 2016 04:43*

Yes the extractor fan in cutting bed,i had a look at your old posts of yours -nice custom  exit of laser cutter fumes,if i didnt have a wooden desk i sit my laser on i would have down draft extraction in a heartbeat


---
**K** *October 08, 2016 14:18*

Do you have a schematic of how you wired your AC stuff with switches?


---
**Don Kleinschnitz Jr.** *October 08, 2016 14:33*

**+Kim Stroman** yup. Look at the power control tab. I used a 4 channel relay module and switches from Adafruit. This approach allows me to connect these relays to the smoothie later on.

[http://www.digikey.com/schemeit/project/k40-s-20-HRJ3GD82020G/](http://www.digikey.com/schemeit/project/k40-s-20-HRJ3GD82020G/)

[digikey.com - K40-S 2.0](http://www.digikey.com/schemeit/project/k40-s-20-HRJ3GD82020G/)


---
**K** *October 08, 2016 15:49*

Thank you! Question. What's the J36 block? Is it just a terminal block? I want to wire mine up without actually connecting it to my board. Just a simple way to plug in my accessories and add a switch to turn them on and off. I was just going to splice in a switch into the hot wire, but if I can avoid actually cutting the power cords to my accessories I'd prefer that.


---
**Don Kleinschnitz Jr.** *October 08, 2016 16:48*

 **+Kim Stroman** Sorry the previous one was an old version: Use this one: [http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/](http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)

[digikey.com - K40-S 2.1](http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)


---
**Don Kleinschnitz Jr.** *October 08, 2016 17:37*

**+Kim Stroman** can't find J36?? Currently my setup is not wired to the Smoothie control. 

The relay board is controlled by the switches and the relays control connecting the line to the AC sockets. AC control currently is independent of smoothie. I will post some wiring pictures.


---
**Don Kleinschnitz Jr.** *October 08, 2016 17:39*

This shows the AC source. I disconnected the LPS and put a terminal block in so I could connect multiple things to the switched AC. The black and white wire goes through the cabinet and out to the AC control.

[https://goo.gl/photos/gTM7cJAdQyVq7tTD6](https://goo.gl/photos/gTM7cJAdQyVq7tTD6)


---
**Don Kleinschnitz Jr.** *October 08, 2016 17:41*

The AC control seen in this picture has the switches wired through a ribbon cable to the relay board. The relay board is wired to the AC sockets. The connector with the red and black wires is the AC source.

[https://goo.gl/photos/E5TSuMJUykEPbCek7](https://goo.gl/photos/E5TSuMJUykEPbCek7)


---
**Don Kleinschnitz Jr.** *October 08, 2016 17:43*

The black and white AC source comes through the cabinet to the strip of sockets on the AC panel.

[https://goo.gl/photos/hNq2qp5RUBSAVCnp6](https://goo.gl/photos/hNq2qp5RUBSAVCnp6)


---
**Don Kleinschnitz Jr.** *October 08, 2016 17:44*

The AC panel shown at the bottom of the controller enclosure (old picture before AC source wired and hole in cabinet).

[https://goo.gl/photos/Rwnz3rzWGABWAXaN9](https://goo.gl/photos/Rwnz3rzWGABWAXaN9)



Hope this is not to confusing.


---
**K** *October 08, 2016 17:47*

So you're powering your air assist, exhaust, pump, etc, through the laser PSU?


---
**Don Kleinschnitz Jr.** *October 08, 2016 17:56*

No it does not run through the LPS. The LPS is one of the modules that uses AC power.  



The main switched AC (the control panel on-off switch) is connected to the LPS. 



I disconnected that, connected it to a terminal block so that I could use that switched AC to turn on power to all the stuff in the machine. 



That means that when the main switch is off so is all the stuff in the machine i.e. LPS, DC supplies, and anything connected to the AC control sockets which is air assist, vacuum and pump. Note: the pump is not controlled and comes on with main switch.


---
**K** *October 08, 2016 17:58*

Ok. That makes more sense. I was wondering how you were powering so much with that PSU. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/QahoycTFP3G) &mdash; content and formatting may not be reliable*
