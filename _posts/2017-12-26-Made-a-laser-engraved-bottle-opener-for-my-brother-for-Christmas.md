---
layout: post
title: "Made a laser engraved bottle opener for my brother for Christmas"
date: December 26, 2017 17:00
category: "Object produced with laser"
author: "Ned Hill"
---
Made a laser engraved bottle opener for my brother for Christmas. The opener is a laminate of Maple and Black Walnut and the display stand is made from a piece of live edge Black Walnut.  



![images/e2538b71839e0045fbdef035875d2dba.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e2538b71839e0045fbdef035875d2dba.jpeg)
![images/4b1939f37b8436ce7eeabbea802c154e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b1939f37b8436ce7eeabbea802c154e.jpeg)
![images/374e33756a939102fa6c38edccd47b46.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/374e33756a939102fa6c38edccd47b46.jpeg)
![images/6fc3e18376e420dc52e150663e2ba7e2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6fc3e18376e420dc52e150663e2ba7e2.jpeg)

**"Ned Hill"**

---
---
**HalfNormal** *December 26, 2017 17:28*

Really nice work!


---
**Don Kleinschnitz Jr.** *December 26, 2017 17:31*

Very nice................


---
**Anthony Bolgar** *December 26, 2017 21:40*

You do very nice work Ned. I always enjoy seeing your projects.


---
**David Allen Frantz** *December 27, 2017 13:46*

Very cool! Thought it was a tap handle at first glance. Lol. I was close 😉🍻! Looks great!


---
**James G.** *December 27, 2017 15:42*

Would love to have one of those! Very nice!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/JCC7EhmN44z) &mdash; content and formatting may not be reliable*
