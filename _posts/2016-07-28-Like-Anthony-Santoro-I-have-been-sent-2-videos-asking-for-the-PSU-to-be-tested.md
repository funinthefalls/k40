---
layout: post
title: "Like Anthony Santoro, I have been sent 2 videos asking for the PSU to be tested"
date: July 28, 2016 17:21
category: "Original software and hardware issues"
author: "Pete Sobye"
---
Like Anthony Santoro, I have been sent 2 videos asking for the PSU to be tested. If this was anything else, The manufacturer would not ask for the item to be dismantled and tested by the end user. That is the job of the manufacturer. I have given them an ultimatum. 1, send a replacement PSU. 2, Refund 50% so I can buy and pay to replace the PSU. 3, a 100% refund for the item plus shipping back to them.

Should one of these be done, then an eBay dispute will be lodged.



If this proves unsuccessful, I will have a K40 requiring a new PSU for sale!


**Video content missing for image https://lh3.googleusercontent.com/-BqJTzoaIzow/V5o9LxhKgRI/AAAAAAAABhA/F8U_-MtNRAcRA56Er3OOjBxPxDwLP-HHQ/s0/Light%252BPen.mp4.gif**
![images/c01d6351b93850fc2fe38fdc08a0739e.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/c01d6351b93850fc2fe38fdc08a0739e.gif)



**"Pete Sobye"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 28, 2016 17:27*

Considering a new tube could be $200 based on **+Ariel Yahni**'s experiences, and a new psu is $99, I would be much more inclined to spend $308 on another K40 with the intention of 'spare parts' or gutting for parts for use with a larger frame, or both. 


---
**Ariel Yahni (UniKpty)** *July 28, 2016 17:32*

I would indeed return and buy the whole thing again. The resin I did not is because I I'm in Panama and returning is not an option. Unless the issue Is that something arrived broken like the tube, then I would by it separately as its better chances of arriving safely 


---
**Derek Schuetz** *July 28, 2016 19:16*

I received 250 back from my 320 k40 through a eBay dispute however they eBay messaged me stating they would cover costs soninhaf that proof for the dispute 


---
**Anthony Santoro** *July 28, 2016 23:09*

**+Pete Sobye** suicidal support! I'm in Australia, if an Australian retailer asked somebody to perform this task, it would be shut down instantly.

It seems that things operate differently in China...


---
**Pete Sobye** *July 31, 2016 10:00*

Well I have recently had an email from the seller 'SunshineSmileService' to say that they are sending me a new power supply. Lets hope its only that, which has blown and not the controller too. Time will tell and god knows when the new PSU will arrive on its slow boat from china!


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/ftBaGrqE767) &mdash; content and formatting may not be reliable*
