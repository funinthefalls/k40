---
layout: post
title: "... my product"
date: February 17, 2017 05:12
category: "Object produced with laser"
author: "Laser Cut"
---
... my product 



![images/aee8d98a553b777162b01d3de17900e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aee8d98a553b777162b01d3de17900e0.jpeg)
![images/fb40d481246a3b760f7b2246aa4a8c5a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb40d481246a3b760f7b2246aa4a8c5a.jpeg)
![images/d2a9c2279199fba407e08b4d9cb2804a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d2a9c2279199fba407e08b4d9cb2804a.jpeg)
![images/f445a43d37259fa3c531b4b408ead66f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f445a43d37259fa3c531b4b408ead66f.jpeg)

**"Laser Cut"**

---


---
*Imported from [Google+](https://plus.google.com/114779305649641505346/posts/KPvrJNh6yuf) &mdash; content and formatting may not be reliable*
