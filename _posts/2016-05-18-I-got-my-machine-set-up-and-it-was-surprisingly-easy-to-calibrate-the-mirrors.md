---
layout: post
title: "I got my machine set up and it was surprisingly easy to calibrate the mirrors"
date: May 18, 2016 14:42
category: "Object produced with laser"
author: "Sebastian C"
---
I got my machine set up and it was surprisingly easy to calibrate the mirrors. The CorelLaser plugin for CorelDraw (ugh) was easy to install, even if I had to find a comprehensible chinglish translation of the instructions online.

The first objects are cut and engraved (was searching the whole house for engravable stuff - my wife has now kitchen laddles with nice skulls on them :D)



BTW does anyone know some cheap shops for plywood in germany/europe?

![images/3fd97d9cd3f07f90b94bdd887d172e41.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3fd97d9cd3f07f90b94bdd887d172e41.jpeg)



**"Sebastian C"**

---
---
**Jim Hatch** *May 18, 2016 17:10*

LOL. I like the spoons. Welcome to the crazy fun.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 18, 2016 20:00*

That's cool. Soon you will find that everything is lased in the house haha.


---
**Brien Watson** *May 18, 2016 23:59*

I seen in another group, that they bought bamboo cutting boards, wooden coasters etc, at the Dollar Store...  Guess where I'm heading tomorrow...


---
**Jim Hatch** *May 19, 2016 00:01*

Also see if you have a Woodcraft nearby. Decent prices on Baltic Birch.


---
*Imported from [Google+](https://plus.google.com/108800242160866633862/posts/8sa7wfzWSmy) &mdash; content and formatting may not be reliable*
