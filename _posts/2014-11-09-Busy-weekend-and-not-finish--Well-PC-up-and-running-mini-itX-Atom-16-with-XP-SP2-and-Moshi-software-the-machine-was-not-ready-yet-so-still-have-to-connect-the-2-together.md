---
layout: post
title: "Busy weekend and not finish  Well PC up and running (mini itX Atom 1.6 with XP SP2 and Moshi software), the machine was not ready yet so still have to connect the 2 together"
date: November 09, 2014 18:45
category: "Discussion"
author: "Stephane Buisson"
---
Busy weekend and not finish …





Well PC up and running (mini itX Atom 1.6 with XP SP2 and Moshi  software), the machine was not ready yet so still have to connect the 2 together.



Finish to unpack and have a close look to the beast. 



I cut the useless pump and fan 220 V connectors to fit a local one. test good they work both. not too noisy, that's good.



Laser tube installation (without manual):



I did have a good look at the tube, my first contact with a laser one. no inscription on it, nor on the box, hopefully they senf the good one, see photo of the 2 ends.



The opening laser door is removable, and it's a way for the screw driver, that's cool. I get some missing screws, before locking the tube into place, I fit the water tubes and put some provided silicone for the seal.

The tube look very low (compare the 1st mirror) so I put the 2 squares of plastic I found into that compartment under the tube to level it, then screw it to lock it in place, not tight, just enought. (I have a feeling that I will need some more settings). 



About the high voltage cable, I spin them around the electrodes, then apply some solder, not sure it was like that I should do, but at least I don't have a loose connections with hight voltage, then I isolate with a good layer of silicone.



Silicone is curing now, first test after dinner. hopefully I will be able to align the ray tonight...



![images/4b177b80f2e2d567fa8b6501f164b6b4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b177b80f2e2d567fa8b6501f164b6b4.jpeg)
![images/81b71af077f06ff5f68ae59bbbd95a89.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/81b71af077f06ff5f68ae59bbbd95a89.jpeg)
![images/36ce729d418fe64fd92cf01817e688d1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/36ce729d418fe64fd92cf01817e688d1.jpeg)
![images/6f09d7948e34a8567fa791a760248893.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6f09d7948e34a8567fa791a760248893.jpeg)
![images/d9f8294ce9f91216ddc901b8c0e65832.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d9f8294ce9f91216ddc901b8c0e65832.jpeg)
![images/1122ab5bcaaf60431d325dbb1f8f6009.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1122ab5bcaaf60431d325dbb1f8f6009.jpeg)
![images/d5a81b9d1e8d67847f6fbfdce75f5ae0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d5a81b9d1e8d67847f6fbfdce75f5ae0.jpeg)
![images/ec291076538104be13c76b201328737f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec291076538104be13c76b201328737f.jpeg)
![images/a6881f126b30f8b83d88952c093f083d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a6881f126b30f8b83d88952c093f083d.jpeg)
![images/eaffee15ffed0c9858411c1e8c2b905d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eaffee15ffed0c9858411c1e8c2b905d.jpeg)
![images/af41df4c3970c8e4f715dc0b1c6424c2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/af41df4c3970c8e4f715dc0b1c6424c2.jpeg)
![images/aab815b78d8fe55a5c7fe088a96feb10.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aab815b78d8fe55a5c7fe088a96feb10.jpeg)
![images/bd4b2e04026f9db9f73e73ca860c95df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bd4b2e04026f9db9f73e73ca860c95df.jpeg)
![images/da0217b0e3e365740887b52846f9c800.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/da0217b0e3e365740887b52846f9c800.jpeg)
![images/568c40c3cdbd170738fee547b1d5288d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/568c40c3cdbd170738fee547b1d5288d.jpeg)

**"Stephane Buisson"**

---


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/DMSPjX541iE) &mdash; content and formatting may not be reliable*
