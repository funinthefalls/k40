---
layout: post
title: "With a birthday around the corner I've got an excuse to buy a laser cutter but I've just finished getting a kit built CNC dialed in so I'm not totally sure I feel like jumping in to a new tool rework/setup project right away"
date: September 03, 2016 16:51
category: "Discussion"
author: "Steve Prior"
---
With a birthday around the corner I've got an excuse to buy a laser cutter but I've just finished getting a kit built CNC dialed in so I'm not totally sure I feel like jumping in to a new tool rework/setup project right away.  I'm comfortable following directions and soldering.



So while I understand that all my hobby projects tend to involve a lot of frustration and challenge, I'd like to see if adding a K40 can be done with relatively little fuss, aggravation, and risk.  I live in Northeast USA - is there a vendor that I should buy the K40 which offers advantages over a random vendor on eBay?  This is not a request for random ads for everyone selling it.



Is there any consensus on a set of recommended upgrades which should be done right away and includes a fairly well defined parts list and instructions?  I've already got an unused Arduino Mega and a bunch of Unos which could be used if helpful and recommended.  I'd be driving the K40 from either a Windows 10 laptop or something like a Raspberry Pi with server software if that's a decent option. 





**"Steve Prior"**

---
---
**Anthony Bolgar** *September 03, 2016 17:13*

I purchased my K40 from Globalfreeshipping on bay. Myself and others who purchased from them have received excellent customer service.


---
**Anthony Bolgar** *September 03, 2016 17:14*

Best upgrade is to replace the stock controller with a smoothieboard.


---
**Steve Prior** *September 03, 2016 17:16*

Since my Win10 machines have already upgraded to the latest version I hear there is an issue with Win10 not recognizing unsigned drivers.  Does that mean I'm pretty much not going to be able to use the machine at all (not even to verify operation) until I swap out the controller?


---
**Ariel Yahni (UniKpty)** *September 03, 2016 17:17*

Interesting results here about upgrade and setup

[plus.google.com - Laser cutter / cutting pols](https://plus.google.com/collection/U3XKXB)


---
**Ariel Yahni (UniKpty)** *September 03, 2016 17:23*

**+Steve Prior**​ I did run win 10 with the stock controller without issues


---
**Steve Prior** *September 03, 2016 17:29*

Did you do it before you got the Anniversary update?  I've seen that if you had the driver installed before you got that update the driver is "grandfathered in", but if you try to install it for the first time after that update (as I would be) you'll have problems.


---
**Ariel Yahni (UniKpty)** *September 03, 2016 17:31*

I don't run windows anymore since I run macs, but for sure it was before anniversary as it was 2 month or so ago


---
**Steve Prior** *September 03, 2016 17:37*

So then you bypassed the potential issue.


---
**Robi Akerley-McKee** *September 06, 2016 06:24*

**+Anthony Bolgar**. I second the smoothieboard.  Yes a bit more money that a mks, but faster too.


---
**Joe Alexander** *September 07, 2016 22:14*

I tested a M2 nano board on Win10 maybe 4 days ago with no issues, and that same laptop has run a moshi machine also. Now that i tested the bulb though its going into my Smoothie-based machine so an upgrade is best in the end.


---
*Imported from [Google+](https://plus.google.com/+StevePrior/posts/Lm7KedJCwLz) &mdash; content and formatting may not be reliable*
