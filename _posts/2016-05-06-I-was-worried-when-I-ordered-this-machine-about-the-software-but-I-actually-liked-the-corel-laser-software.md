---
layout: post
title: "I was worried when I ordered this machine about the software, but I actually liked the corel laser software"
date: May 06, 2016 00:41
category: "Hardware and Laser settings"
author: "andy creighton"
---
I was worried when I ordered this machine about the software,  but I actually liked the corel laser software.   It has taken some work to get used to it,  but it really had done a great job of combo engrave/cut projects....maybe even better than our  fslasers.





**"andy creighton"**

---
---
**Rob Thompson** *May 06, 2016 01:01*

I am using out of necessity until I install my smoothieboard but in the end it is pirated software. 



I'm a CAM software developer so don't condone it of course - but I already own CorelDraw (latest version and since 97 version).  



"China" doesn't have issue with piracy but we choose to get what we "pay" for ;-)



In my view add  ma few hundred bucks to the overall cost and try laserweb2. 






---
**Scott Marshall** *May 06, 2016 01:32*

If they would release the controller info so we could interface to it, they wouldn't have to supply software at all. 



To say the Chinese "don't have an issue with piracy" is rather an understatement, they have actually cultivated it to a fine art.

It's like saying the "French kinda like wine"



If their engineering expertise was spent on innovation rather than dissecting and copying, everybody would be better off.



Then they refuse to release the data on the "m2Nano" controller???

WTF??

You gotta love them sanding part numbers of the chips on a $2 crappy ebay regulator board, I guess they're worried about their neighbors ripping them off. Most of those "products" are  exact copies of the spec sheet demo circuit anyway!!! 

OK, that's (not even close) to my 2 cents on that subject. I'll behave now.(for a while)



I have the board out, I'm going to have a go at "decoding" the hardware anyway. Maybe if I get that sorted, one of our software gurus can get us connecting thru it.

((I'm going Smoothie, which is why it's out) REAL Smoothie, not the Chinese ripoff Copy - guess Mr Wolf and his partners should have sanded....)



Looking forward to Laserweb....



Anyway, Andy, I'm going to keep you in mind, I have a heck of a time navigating Corel anything.



I just don't understand the command structure, I grew up on Autocad and it's just a totally different way of going about things. I need to generate a alternate Mindset. I WILL get it, eventually.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 01:37*

I think the CorelLASER software is functional & does the job. Unfortunately, lack of support, lack of instructions, lack of any explanation as to what any of the buttons/features do means quite a lot of trial & error to get used to it.



Also, I am not a fan of using pirated software, so that is one of the serious issues with it, unless you want to purchase a decent version of CorelDraw. I'd rather save the $ for smoothie & go the LaserWeb direction (which with more users will help & support Peter's development/testing of it).


---
**Scott Marshall** *May 06, 2016 01:48*

**+Yuusuf Sallahuddin** You seem to have it learned quite well.

It's just become a quest to me. I have the same problem programming Pics, I spent years with Basic which is sequentially formatted (operations in line order), and C, C+,C++ etc, perform every operation at once, so to speak. I just can't change my mental process to work well with it. 



By the way, I have several subjects I'd like to get into our Library. I need guidance on how to get into it from here, or to hand off my stuff to you so you can do it. Please drop me an e-mail when you get a chance, it's been a zoo here, and I was out of it for a while, so lost your email.

Thanks, Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 02:00*

**+Scott Marshall** I've been carried away with other projects for a bit too, so haven't got to doing anything for the website/forum as yet, but will be scheduling some time this week for it so that I can get some progress happening. I will drop you an email & you can either forward whatever the stuff is via email (depending how much/file size) or you can drop it into the G+ shared folder I set up in my Google Drive for the Everything Laser stuff (I'll put G+ shared folder link in the email). Either that, or we can see about putting it directly onto EverythingLaser forum.


---
*Imported from [Google+](https://plus.google.com/111039030319838939279/posts/UJfrqeDmBQW) &mdash; content and formatting may not be reliable*
