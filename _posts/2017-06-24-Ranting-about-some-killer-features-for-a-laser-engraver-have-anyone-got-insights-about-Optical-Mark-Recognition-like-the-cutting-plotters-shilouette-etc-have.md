---
layout: post
title: "Ranting about some killer features for a laser engraver, have anyone got insights about Optical Mark Recognition like the cutting plotters (shilouette etc) have?"
date: June 24, 2017 15:29
category: "Discussion"
author: "Jorge Robles"
---
Ranting about some killer features for a laser engraver, have anyone got insights about Optical Mark Recognition like the cutting plotters (shilouette etc) have?





**"Jorge Robles"**

---
---
**Ashley M. Kirchner [Norym]** *June 24, 2017 16:58*

I don't know what the Silhouette does, but I can tell you what the workflow is at the shop I worked at. For nested items that are getting kiss-cut (vinyl material), the print has corner marks on each corner of the sheet as well as a barcode (note that some times that "sheet" is several feet long, on a large roll being fed into the cutter.) The barcode tells the cutter what the dimensions are and where the corner marks are. It then feeds the material and moves the head to accurately read those corner marks and aligns itself to them. There's a small margin of error that it will accept and work with, for example if over the entire length of the sheet, it's off by a few mm (if the sheet is askew), it will send that data over to the computer to adjust the cutfile accordingly (by rotating it by however many degrees.) But, it's a small margin. If it's too far, it won't do it and we'll have to re-align the sheet again.



For the large CNC, once again all the prints have a barcode on them that has the dimensions of the print and along the perimeter there are little black dots (about 5mm in diameter). The camera on the CNC head will start off by reading in the 4 corners and align itself to those. If the sheet happens to be slightly askew, it will read in a few more dots to get a better alignment. If there are different prints (from different jobs) on the sheet, it will read the dots closest to the individual pieces to get proper alignment. Then the cutfile is sent and it will use that alignment data to precisely cut the pieces out.


---
**Jorge Robles** *June 24, 2017 17:07*

Thank you! i'm rather looking for libraries or open source developments that could help to implement on our machines. Found js-aruco so far, not a bad start, buf feel we will need some hardcore programming to fine place our works :) 


---
**Ashley M. Kirchner [Norym]** *June 24, 2017 18:49*

Yeah, it seems all of those systems are special programming and communications between hardware and computer (such as the plotter telling the computer to rotate the image slightly prior to sending the cut data.)


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/A5XLJ5Zd2eY) &mdash; content and formatting may not be reliable*
