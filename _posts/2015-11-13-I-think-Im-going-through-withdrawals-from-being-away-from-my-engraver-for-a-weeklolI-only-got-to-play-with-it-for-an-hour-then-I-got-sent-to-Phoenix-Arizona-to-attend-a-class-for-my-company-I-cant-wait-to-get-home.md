---
layout: post
title: "I think I'm going through withdrawals from being away from my engraver for a week....lol...I only got to play with it for an hour then I got sent to Phoenix Arizona to attend a class for my company, I can't wait to get home"
date: November 13, 2015 16:36
category: "Discussion"
author: "Scott Thorne"
---
I think I'm going through withdrawals from being away from my engraver for a week....lol...I only got to play with it for an hour then I got sent to Phoenix Arizona to attend a class for my company, I can't wait to get home to play around some more, such a cool device!





**"Scott Thorne"**

---
---
**Anthony Coafield** *November 14, 2015 21:23*

It's crazy isn't it! Mine arrived the day before I had to go away for a week. Just sitting at home taunting me the whole time. 


---
**Scott Thorne** *November 14, 2015 21:27*

Lol...I just got back in Georgia at noon today...I came in walked straight to the garage and fired it up and started playing...I did a tiger face to see how it would turn out, the old board was not big enough buy man it looks good...I'll post a pic.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/SxCr3EZ5PuQ) &mdash; content and formatting may not be reliable*
