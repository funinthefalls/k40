---
layout: post
title: "Hi Everyone, absolute newbie here and still to order my machine"
date: May 31, 2016 12:28
category: "Discussion"
author: "Pete Sobye"
---
Hi Everyone, absolute newbie here and still to order my machine. on a budget (aren't we all!) so been looking at the K40 or one of its variants on fleabay.

I don't plan to do anything complicated with it, just cutting 3mm clear acrylic to make 5 sided boxes. These will be used as covers for display items. so no fancy and intricate cutting or engraving.

On one video of the K40, the author states that the cut of the material is not exactly square as the beam of the laser has passed through the lens and has become conical. Is this really the case ???

Would this machine be suitable for my needs?

Also, I am no programmer so just how easy is it to swap out the controller for a best, are flexible one? Is there such a thing as a step by step guide for installation and programming such a board to use, say, InkScape??

Your help and advice would be very much appreciated.

Thanks

Pete - Cornwall UK





**"Pete Sobye"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 31, 2016 12:48*

You're correct that the beam will not be square. As when it passes through the lens it becomes conical shaped, but it is more like an hourglass, as once it reaches the focal point (50.8mm from the standard lens) it expands out again.



like so:



\/

/\



The intention is to aim the focal point half way through the depth of the material, (so 1.5mm into the acrylic in your case). This will minimise the effect of the angle on the edge (since both upper & lower half will have slight bevel instead the entire piece being angled at the edge). Hope that makes sense.



For the most part, this is very minute & would barely be noticeable or an issue.



I can't comment hugely on the controller issue, as I've yet to upgrade myself (budget too), but there are many in this group that have upgraded controllers & also we have **+Arthur Wolf**  (maker of Smoothie controller) & **+Peter van der Walt**  (developer of LaserWeb1 & 2) as members of this community. Both are very helpful in regards to setting up controller upgrades/software.


---
**Pete Sobye** *May 31, 2016 13:30*

So would this machine be suitable for cutting acrylic pieces for a box?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 31, 2016 13:38*

**+Pete Sobye** Should be, as I've seen other's cutting acrylic pieces with the K40. If you're intending to cut boxes that slot together using finger joints you just need to keep in mind the kerf (width of the laser beam, similar to blade width on a saw) when designing your box. Taking that into consideration you can get pretty tight fits that should lock together nicely.


---
**Jim Hatch** *May 31, 2016 13:42*

**+Pete Sobye**​ the attenuation of the beam won't impact what you're trying to do. As Yuusuf said, focus at the material midpoint and the two outer edges will be flat relative to any piece you glue to and the acrylic will fill the middle. It's a kerf less than 0.1mm. If you do non-glued joints like fingers, etc then you have zero impact altogether. 


---
**Pete Sobye** *May 31, 2016 14:02*

I am not going to cut a 'tabbed' box, it will be flat and straight corners/edges. Also, in the designing, i will place the individual pieces away from each other so they are cut out separately and not a larger piece cut into sections, if you get what i mean.


---
**Jim Hatch** *May 31, 2016 14:25*

**+Pete Sobye**​ then you're fine by focusing at 1.5mm into the piece.


---
**Tony Schelts** *May 31, 2016 17:05*

I cut 3mm accrylic and 4mm with no problem.


---
**Vince Lee** *May 31, 2016 23:23*

Is your intent to make optically clear joints in the corners using acrylic cement and a needle bottle?  If so, the squareness may be an issue and you might need to make a jig to finely sand or file the sides that attach to a face to maximize the contact area.


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/hMZVSYDJbGU) &mdash; content and formatting may not be reliable*
