---
layout: post
title: "I had an idea and wow did my head hurt!"
date: January 07, 2019 22:41
category: "Discussion"
author: "HalfNormal"
---
I had an idea and wow did my head hurt!



With G+ going away and not everyone wanting to converge at the same place, I was wondering if I set up an area on my forum for everyone to leave their contact/new group information would anyone use it.



At first I thought about making it public but then if you do not want your info public this would not be good. I do not want to make anyone sign up to access this information either so I am thinking of making it an invite area only which leaves it out of the public eye and to anyone on the forum not invited.



What are your thoughts? Ideas? Favorite colors?





**"HalfNormal"**

---
---
**greg greene** *January 07, 2019 22:50*

Blue


---
**Don Kleinschnitz Jr.** *January 08, 2019 00:12*

I think it you have a good idea no matter what we do. A sort of contact list for us all is needed.



BTW my sense is that "and not everyone wanting to converge at the same place" is the antithesis of what we all want. 

At least that was my impression and certainly my goal. Its just that everyone has a list of ideal places and that does not converge with a single technology [go figure]. Its a problem with no converging solution.



Nothing magical going to happen so my guess is that as the end approaches we will be forced to pick something.



This exact problem occurred with my closed wood turning group on Yahoo when Yahoo got ugly. Out of frustration with a long debate over platform technology, I simply started a private FB group. We ended up with a Yahoo group AND a FB group. I have feeds from the yahoo group and now the preponderance of us are on FB while listening to the new Yahoo group. The FB group is much more active as it is easier to use and manage. Most importantly we stayed together as a group.



I have been on both the FB Laser Engraving group and WeMe. 



WeMe as far as I can see is a new FB that touts privacy. I doubt their business model will keep them funded.

The FB Laser group is on a good enough platform but its just to broad a group for me to enjoy. I have a perception of lots of noise and very small percentile of technical exploration and expertise. 



Perhaps we just create a private FB group to replace this one and also have your forum. Actually that is not that different than we have now. We have this G+ group and your forum.



#337DFF  


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/SvJK5vCgHDB) &mdash; content and formatting may not be reliable*
