---
layout: post
title: "Hello, New to laser cutting and new to Google+ so sorry if I'm posting incorrectly!"
date: April 25, 2016 14:37
category: "Discussion"
author: "Pigeon FX"
---
Hello, New to laser cutting and new to Google+ so sorry if I'm posting incorrectly! 



I'm considering getting a K40, but had a few questions I was hoping to get some answerers to. 



1: there seems to be a fair few machines classed as K40's and I was wondering if some variants are better then others, and if so what to look out for when picking one?



2: Will I be able to engrave materials like 2 colour engraving acrylic, and powder-coated aluminium enclosures (like Hammond Project boxes)?



3: How upgradable are the machines? As in could I replace the control board for something like a 3D printer control board and open up some options regard firmware and software used?



4: is there a recommended place to buy a K40 in Europe as I would rather avoid a large import duty charge from customers if buying from China (I'm in the UK)



5: Are there any regular forums that have a active K40 community it's worth subscribing to? (Google+ scares me!).  



I'm sure I have many other questions, but those seem like a good starting place! 



Thank you or your help.





**"Pigeon FX"**

---
---
**Jim Hatch** *April 25, 2016 14:56*

#1 - avoid the Moshi board based ones. They'll usually have "Moshi" in the description either for the board or software. If in doubt ask the seller. If it has an air assist included that's a plus but most don't so it's an easy enough upgrade. 



#2 - not sure what you mean by 2 color acrylic. Engraving acrylic cuts away the top surface. If that's a different color than the interior then yes. Mirrored acrylic is a common example of that. Aluminum engraves well too but it is more of a "marking" than a 3D engrave with depth.



#3 - upgrading is common. Adding safety switches, lights, improved exhaust, air assist, bed changes (including Z axis control), optics (lens and/or mirrors) and new controller boards & software are all common mods. None are required as the machines operate pretty well out of the box but easy to make them better.



#4 - I'm from the States so don't know.



#5 - there are a couple of others (Google "K40 laser forum" or "hobby laser forum") but I tend to hang around here more - less of a time suck and pretty quick help when you need it.


---
**Pigeon FX** *April 25, 2016 15:08*

Thank you Jim, really appreciated! 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 15:20*

**+Pigeon FX** 

#1, unsure really.

#2, I've seen people engrave on anodized aluminium. Not certain about powder coated.

#3, A few people in this group have upgraded to different controller boards & a member here (**+Peter van der Walt**) is working on a web based laser software called LaserWeb that interfaces with the K40 when controller board has been upgraded (to various boards such as Smoothie, Ramps, & some others I can't remember off the top of my head). LaserWeb seems pretty spectacular (in my opinion) in regards to features and Peter & those working with him are doing a wonderful job & very responsive to the community's needs regarding features.

#4, I'm from Australia, but if you check in this group's posts, **+Stephane Buisson** has mentioned various times about suppliers for EU to avoid VAT & the likes.

#5, In regards to a regular forum, we are working on a forum site at the moment ([everythinglasers.com](http://everythinglasers.com)) that will be hopefully capable of G+ sign in very shortly (so users from here can post there without having to create a new account) & will be updating it's design & layout to be mobile responsive & functional for our needs as a community. It might be a little while before it's up & running fully & we can get people to utilise it. G+ is pathetic for information organisation & searching for information that is already in our group here multiple times. Hopefully the forum can open up better ability to communicate frequent issues (e.g. alignment, setup, software, modifications, etc).


---
**Vince Lee** *April 25, 2016 17:25*

1: The type controller (Moshi vs Nano) doesn't matter if you plan to replace it.  I have a moshi and don't mind it once I figured it out, but don't have experience with the other board's software.  One advantage of Moshi is that the current version supports importing DXF files, so even if you don't upgrade your controller you can draw in another app like inkscape and just import into MoshiDraw to cut or engrave.



2: I engrave acrylic sign laminate all the time.  Dunno about power coat.



5: This forum and the Laser Cutting and Engraving communities are good on Google+.  Facebook has a K40 Chinese Laser Cutter group that is also good.  There is an Open Source Laser group on Google Groups that is not as active.


---
**Pigeon FX** *April 25, 2016 18:31*

**+Jim Hatch** Hey Jim, looks like the ones I can find here in the UK all have the Moshi and are without the air assist......is there a there a kit for the air assist or better still 3D printable parts? (I guess the board is not a huge issue as I plan to upgrade that anyway once I have got my feet wet with using the machine).  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 18:37*

**+Pigeon FX** Check [thingiverse.com](http://thingiverse.com) for 3d printable air assist for K40.


---
**Venuto Woodwork** *April 25, 2016 19:43*

I would personally get a air assist and new lense from light object.


---
**Pigeon FX** *April 25, 2016 19:54*

**+Venuto Woodwork** Just sent them a email to see if they ship to the UK. What size lenses should I be looking for? (anything else worth grabbing if I put in an order for the Air assist and lenses)


---
**Joe Keneally** *April 25, 2016 20:13*

Get the lens from lightobjects that fits the air assist head you order from lightobjects.  Their stuff is great!!!


---
**Jim Hatch** *April 25, 2016 21:10*

I got the Light Objects air assist head along with the lens and mirror. It's 18mm vs the 12mm mine came with stock. The larger diameter gets you a larger center sweet spot for focusing.



Here's the link: [http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)



This is the lens I got:[http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F508mm-P670.aspx](http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F508mm-P670.aspx)



This is their high quality ZnSe lens. They have a slightly more expensive GaAs but I haven't seen any claims it results in improved power (vs the high quality one does seem to deliver improved results).



Getting the mirror & lens along with the head means it will all work together. Otherwise you'll need to scare up some adapter rings - or make them yourself as the last product of your stock lens & head :-)


---
**Jim Hatch** *April 25, 2016 21:15*

BTW, look closely at the LO picture for the head. The bottom piece the air assist head cone slips onto unscrews from inside the knurled ring. That's where the lens is going to go. The upper part of the head also unscrews and somewhat easier than the lower piece which leads some people to think it's not going to come off and they need a new hole in the mounting plate for the head. That's not the case. You just need to unscrew the smooth cylinder section under the knurled ring to get to the lens recess. Unscrew the upper portion to get to the smaller diameter section that mounts through the plate and into the final upper piece with the mirror mount.


---
**Stephane Buisson** *April 26, 2016 10:22*

**+Pigeon FX** I'll be back in London (N1) in 2 weeks, contact me with a phone number or mail and I could help. where are you located in UK ?

A K40 is just a XY table with optics, a PSU (all hapen in the PSU really (PWM to high voltage)) and a controller board, don't be trick by geeky control board, it's just a "potentiometer" with digital % display, the old fashion work as good and could be cheaper. the main thing is to change the controller board to have access to open source softwares, which are far better than what come with the stock K40.

air assist is a nice addition, to keep clean your optic (one of the main issue).

a budget of about £200 on top of the K40 (best is ebay seller from UK (vat & delivery included), will allow you to be full options. take also a weekend to mod it. not a big deal really.

cheers, and welcome here.


---
**Pigeon FX** *April 26, 2016 12:42*

**+Stephane Buisson** Thanks Stephane, I'm down on the south coast in Hastings, so will definitely  take you up on your kind off as a contact when you get back into London! I was wondering if I could get some more info on what controller board options there were out there, if you know of a article or forum thread covering this? 



I'm quite drawn to a Ramps 1.4 conversation I stumbled across (mainly as I have spare Ramps boards, pololu drivers, and have a basic knowledge of Marlin), but would love to know what other comon and more importantly for me well documented board upgrade options are out there 


---
**Stephane Buisson** *April 26, 2016 13:17*

**+Pigeon FX** ok, I will go to bexhill in a couple of month ;-)).

Ramps is now out dated (poor clock for raster, ok for cut only), Smoothie is better if you can afford the difference. better lens/mirrors to improve efficiency is a real +

get or print yourself a airassist head.

like mine

[https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser](https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser)


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/XbhvWMEsYzE) &mdash; content and formatting may not be reliable*
