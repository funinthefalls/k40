---
layout: post
title: "Could somebody help me please, I've downloaded a file from of Jay Garrick's aka the flash wings but they were designed to be 3d printed, I would like to engrave the lines and then attempt to cut them out of 10mm acrylic"
date: December 14, 2016 12:06
category: "Object produced with laser"
author: "Andy Shilling"
---
Could somebody help me please, I've downloaded a file from [youmagine.com](http://youmagine.com) of Jay Garrick's aka the flash wings but they were designed to be 3d printed, I would like to engrave the lines and then attempt to cut them out of 10mm acrylic. Is there an easy option to do this as I have no experience with 3d files. 



Here the link to the files if anybody wants to look



 [https://www.youmagine.com/designs/jay-garrick-helmet-wings](https://www.youmagine.com/designs/jay-garrick-helmet-wings)





**"Andy Shilling"**

---
---
**Stephane Buisson** *December 14, 2016 14:11*

the file is a 3DSmax, not easy for me, so maybe

in youmagine window select the VR view from top (deactivate the grid) and do a screen capture.

then open it with inkscape. to prepare your trace and engrave...


---
**Andy Shilling** *December 14, 2016 14:19*

**+Stephane Buisson**​ thank you I'll try that, I didn't realise there was a Vr option. I installed Inkscape a couple of weeks ago so it's time I learnt to use it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 14, 2016 14:41*

Try 123D Make to slice the file into layers to cut & attach together?


---
**Cesar Tolentino** *December 14, 2016 15:27*

He has max and stl file. Both can be imported to sketchup. And from sketchup can be exported to dxf. 


---
**Andy Shilling** *December 19, 2016 16:35*

Just before my k40 broke yesterday I manage to finish the Son's birthday present thanks to you guys with your ideas. I ended up using photoshop and a stl viewer to get the image I wanted. 1 engrave layer , 6 cut passes, 3 coats of primer, 3 coats of gold and I'm going to have a very happy 13 year old come 27th.



Thanks for your tips guys **+Stephane Buisson**​ **+Yuusuf Sallahuddin**​ **+Cesar Tolentino**​


---
**Andy Shilling** *December 19, 2016 16:36*

![images/ff9fb7db17918b45db7d6937f5a93a8a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff9fb7db17918b45db7d6937f5a93a8a.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 19, 2016 19:00*

Wow, looks cool. Hope the young fella likes it :)


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/3sYgavhdTVo) &mdash; content and formatting may not be reliable*
