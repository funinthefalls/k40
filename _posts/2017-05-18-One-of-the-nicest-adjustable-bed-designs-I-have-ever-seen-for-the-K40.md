---
layout: post
title: "One of the nicest adjustable bed designs I have ever seen for the K40"
date: May 18, 2017 14:08
category: "Modification"
author: "Anthony Bolgar"
---
One of the nicest adjustable bed designs I have ever seen for the K40

[http://www.thingiverse.com/thing:2321473](http://www.thingiverse.com/thing:2321473)







**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *May 18, 2017 19:08*

FYI....be prepared to print for around 100 hours to get all the pieces made.


---
**HalfNormal** *May 18, 2017 22:31*

While it looks nice, I believe it will limit the cutting area.


---
**Anthony Bolgar** *May 18, 2017 22:40*

All the adjustable bed designs I have found will limit the cutting space. If you have a design that does not could you please share a link to it? I am sure many of us would be interested in it.


---
**Anthony Bolgar** *May 18, 2017 22:40*

Another one I found today


{% include youtubePlayer.html id="7cl0OkYHS5I" %}
[youtube.com - Spring Loaded Adjustable Bed for K40 Laser](https://www.youtube.com/watch?v=7cl0OkYHS5I)


---
**HalfNormal** *May 18, 2017 22:46*

Right now I am using  the simplest design that allows adjustment but does not have upper supports and relies on the cutting table for upper support. I also made it so it removes quickly. What I like most about it is that it allows me to level the bed to the gantry quickly and easily. Everyone will have a reason to use the one they use. It is personal! ;-)


---
**Anthony Bolgar** *May 18, 2017 22:56*

I am going to test out an adjustable laser head I found on thingiverse, so I can adjust the focus via the head instead of a table. I have it almost ready to go, at full extension all I need to do is place a 1/4" pc of something on the bottom of the Z area and that puts the lens in focus (50.8mm) Thicker materials you just raise the bottom of the adjustable head by the thickness of the material. Only down side is the lens is not fully enclosed. If it works well, I will modify it to enclose the head.



[thingiverse.com - Adjustable lensholder with dual line pointer holder, air assist nozzle and focus leveling block for K40 Laser by DerZeitgeist](https://www.thingiverse.com/thing:1648481)


---
**HalfNormal** *May 18, 2017 22:58*

Now that looks nice! Would prefer an enclosed lens.


---
**Anthony Bolgar** *May 18, 2017 23:26*

Yup, first mod to do if it works like I think it will. I need about 60mm of travel so 2 sliding tubes of 35mm each should fit the bill, top tube sliding over the bottom one.


---
**Anthony Bolgar** *May 19, 2017 16:06*

OK, not a 100 hours of printing, but at least 50-60 the way my printrbot is going.




---
**David Regis** *May 20, 2017 15:39*

I have the LightObject Z-table in my K40 and it really doesn't impact the cutting area that badly.  The frame is outside of the cutting area on the two sides adjacent to the home position (upper-left corner).  However, it does limit the size of the workpiece since it does take away from the total area defined by the gantry.



I've been thinking about a scissor-lift design since the mechanism is completely under the foot-print, but I'm not sure how precise the vertical movement would be.  Also, the rate of travel changes with elevation (I think), so being able to rely on the Z-position in the controller display wouldn't be possible.  I believe that it expected to be linear (e.g.,  X mm / step).


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/2Si85UZ1s8w) &mdash; content and formatting may not be reliable*
