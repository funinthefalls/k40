---
layout: post
title: "Do I need to solder pin headers to this location to connect to the LLC board for laser control or is this also routed to one of the green screw connectors?"
date: July 18, 2016 00:30
category: "Smoothieboard Modification"
author: "Custom Creations"
---
Do I need to solder pin headers to this location to connect to the LLC board for laser control or is this also routed to one of the green screw connectors? Board is a Smoothieboard 5x.



TIA!

![images/328168a96589f7ed916f1293246e60b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/328168a96589f7ed916f1293246e60b1.jpeg)



**"Custom Creations"**

---
---
**Scott Marshall** *July 18, 2016 02:24*

That's the spot. Just solder in a 5 pin piece of header.



The end one is gnd and the 4th one in is 2.4 PWM  for small mosfet/laser fire.



It's the only place to get the connection, no screw terminal is tied to it.


---
**Custom Creations** *July 18, 2016 03:27*

**+Scott Marshall** Thank you! Hangout message sent.


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/ER5kpRJtj3m) &mdash; content and formatting may not be reliable*
