---
layout: post
title: "Hi All, Please help a man in need"
date: January 11, 2017 10:42
category: "Modification"
author: "photomishdan"
---
Hi All, 



Please help a man in need.



So I recently  replaced the tube in my K40 due to the original cracking after it froze in bad weather. 



Tube is in, it works. Machine works. But there is now a problem with focus. I can't seem to cut as cleanly as I used to be able to, there is significant charring. Engraving works without too many issues, It seems to loose intensity towards the front of the machine. I've adjusted screws, alignments, mirrors, angles... the position of the current head....



I had a 3D printed air-assist nozzle, and some how the beam now comes out of the lens at and angle and its now got holes in it...



So my question is, would a focus issue be solved with a new laser head with a larger lens in it? If I bought the light objects head, and the beam still came out at an angle, would that be destroyed too? Would it be more sensible to get an eBay special, air assist head that looks like its milled out of a chunk of aluminium? It looks like the focus can be adjusted on the head? I have very very limited budget on this project and just want to get the thing cutting cleanly, on all sides of the machine. 



Thanks in advance of any advice that can be given. 





**"photomishdan"**

---
---
**Joe Alexander** *January 11, 2017 13:40*

This issue should be resolved by performing a complete calibration.  the main cause of beam hitting the edge of the nozzle is the beam doesn't hit the mirror on the nozzle in the center, or that your laser tube is not squared up to the rails appropriately. RabidWombat calibration guide is the way to go.


---
**photomishdan** *January 11, 2017 13:43*

**+Joe** thank you for replying Joe. What is this RabidWombat Calibration guide you speak of? I cannot seem to see it? 


---
**Ned Hill** *January 11, 2017 13:45*

Make sure the lens is flat in the holder.  The light object air assist head is metal so it's unaffected by the laser.


---
**Cesar Tolentino** *January 11, 2017 15:05*

Yes, you will need a calibration of three lenses


---
**Joe Alexander** *January 11, 2017 16:59*

[https://onedrive.live.com/?authkey=%21ABQP32x0lllP5qY&cid=FCC291C442984ADA&id=FCC291C442984ADA%211386&parId=FCC291C442984ADA%211376&o=OneUp](https://onedrive.live.com/?authkey=%21ABQP32x0lllP5qY&cid=FCC291C442984ADA&id=FCC291C442984ADA%211386&parId=FCC291C442984ADA%211376&o=OneUp) a link to the guide from a fellow group member.


---
**photomishdan** *January 11, 2017 17:55*

**+Joe** my hero! Thank you so much. Looks like an amazing guide. Will have to study this thoroughly. Will let you know how it goes 


---
*Imported from [Google+](https://plus.google.com/105674876465839652573/posts/hkVKX9faEnb) &mdash; content and formatting may not be reliable*
