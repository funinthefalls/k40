---
layout: post
title: "Engraving on Merbau Hardwood again. It's my name, Yuusuf Sallahuddin in Arabic Kufi Square style artwork (reads clockwise from bottom right corner in a spiral to the centre)"
date: June 03, 2016 01:29
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Engraving on Merbau Hardwood again. It's my name, Yuusuf Sallahuddin in Arabic Kufi Square style artwork (reads clockwise from bottom right corner in a spiral to the centre).



So this time around I decided to go around the edge with a cut after engraving the main imagery. Much better image alignment this time too. I made the imagery 85 x 85mm & spaced 3mm from each of the edges. Seems I was slightly out still, as the left edge is ~2.5mm & the right edge is ~3.5mm. Need to sort out some more precise jigs for the pieces.



Engrave @ 4mA @ 350mm/s @ 5 pixel steps. Took ~4m36s.

Cut @ 4mA @ 20mm/s @ 1 pixel step. Took ~2m7s.



The cut around the edge gives a slightly darker edge to the engraved area, which seems to be a nice effect in my opinion.



![images/e7d0ba774105325d927c9eaa471cc2fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e7d0ba774105325d927c9eaa471cc2fb.jpeg)
![images/c4df75e3ed804a3ed3c888823596d300.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4df75e3ed804a3ed3c888823596d300.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2016 04:26*

**+Susan Moroney** That would partially work, however I've used that sort of method in the past with painter's masking tape. Unfortunately, it still relies too much on getting things precisely in position by eye. You will be better off with some solid jig (plywood or something cheap) that you can actually push the piece hard up against it. Then if for whatever reason you need to put it back & do another engrave/cut, you can get it back into the exact same position. Or if you're doing multiple of the same piece you can get them all in the same position every time.


---
**Tony Schelts** *June 03, 2016 08:18*

On my CorelLaser software, there is a funtion that draws a box around the area that it is intending to use.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2016 09:43*

**+Tony Schelts** Do you know the name/where I can find this function? I'd like to check if my version of CorelLaser has that too.


---
**Lance Robaldo** *June 03, 2016 10:13*

It's the "preview" button on the engrave/cut screen.  I turn the laser off but have my red cross hairs still on and watch the box it "draws" to make sure my alignment is correct on whatever I'm cutting/engraving.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2016 10:45*

**+Lance Robaldo** Ah, I don't have the red crosshairs at all. Probably a worthwhile mod to add, but I've already got a list a mile long haha.


---
**HalfNormal** *June 03, 2016 14:24*

**+Yuusuf Sallahuddin** wish my name looked as nice as yours!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 04, 2016 02:07*

**+HalfNormal** Technically it could, if we designed it up using a similar art style. There is a similar concept called gago-in & others like it that are used in Japan for wax seals. If they can do it with Japanese characters/Arabic characters/etc then I can't see why we can't do it with English characters.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/FQkpNkhhesY) &mdash; content and formatting may not be reliable*
