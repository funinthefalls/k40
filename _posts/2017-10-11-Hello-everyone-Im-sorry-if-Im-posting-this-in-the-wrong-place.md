---
layout: post
title: "Hello everyone! I'm sorry if I'm posting this in the wrong place"
date: October 11, 2017 08:18
category: "Discussion"
author: "\u0421\u0442\u0435\u0444\u0430\u043d \u0421\u0438\u043d\u043e\u0431\u0430\u0434"
---
Hello everyone! I'm sorry if I'm posting this in the wrong place.

I have a huge problem with the engraving quality that I'm getting with my K40 laser.

As you can see in the pictures some letters are crooked and wavy.

I have tried to fix this in many ways...

I have cleaned, screwed, tightened, lubricated everything, without any improvement.



I would be thankful if I can get someone's opinion on I how I may try to solve this problem.



Thanks!  



![images/6eff3d893c50a9cf5a1eda971279bf3b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6eff3d893c50a9cf5a1eda971279bf3b.jpeg)
![images/b354a4f85252e524125b31fa29d9d6f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b354a4f85252e524125b31fa29d9d6f2.jpeg)

**"\u0421\u0442\u0435\u0444\u0430\u043d \u0421\u0438\u043d\u043e\u0431\u0430\u0434"**

---
---
**Ned Hill** *October 11, 2017 17:17*

Common culprits: 

loose belt

loose mirror/mount 

loose lens holder 

engraving too fast causing the workpiece to shift back and forth 

air line dragging on the workpiece


---
**Стефан Синобад** *October 11, 2017 17:25*

**+Ned Hill** thanks for your reply. 

I checked all of these since I found them already written on the forum somewhere, but none of these solve my problem.

Nothing is loose, everything is clean....

I don't have air assist...




---
**Стефан Синобад** *October 12, 2017 11:46*

anyone?


---
**Ned Hill** *October 12, 2017 14:17*

How does speed affect the engraving?  Also I recall someone reported that a loose set screw on a belt gear caused wavyness.  Of course that was most likely an older machine as most of them seem to be press fit now.  The only thing else I can think of is a bad stepper motor.  


---
**Стефан Синобад** *October 12, 2017 17:41*

**+Ned Hill** It gets slightly better. But the emphasis is on SLIGHTLY.

Barely noticeable.

Sorry I'm a total newbie. When you say 'set screw on a belt gear', do you mean the 2 screws that are used to tighten/loosen the belt? If so, I checked this, and its fine.

Which part is the stepper motor? How can I check if its this part?

And if it is, what is the solution?

Thank you!


---
**Ned Hill** *October 13, 2017 15:11*

Ok, If you are total newbie let's back up a bit.  A few questions:

1.) Is this a brand new machine?

2.) Have you had this issue from the start?

3.) Are you running all stock parts and software?

4.) If you are running stock software then have you selected the M2 mainboard in the software and entered the device ID written on the control board?

5.) What do you have the engrave output file type set to? (Should be WMF).


---
**Стефан Синобад** *October 13, 2017 17:34*

1. I bought it 1 year ago. I do not use it excessively. 

2. No. The engraving was good at the beginning, and got worse.

3. Yes. I haven't changed anything.

4. Yes, M2 is selected. The device ID is entered.

5. Sorry I looked everywhere, but I can't find the output file? Were is this option located?


---
**Nate Caine** *October 13, 2017 19:54*

On a different (and larger) machine, we'd see this problem when the material wasn't held down rigid to the deck.  (How do you keep you material from shifting?)



From your test photos the problem appears different, and in a different location, each time.  So I'd discount it being a file or software problem.  



However, on my <i>actual</i> K40 machine I saw a problem where the belt clamp was mounted to the carriage.  One screw was loose and the other screw had been Super-Glued after they stripped the thread during manufacture.  So I tapped the two holes to a larger screw and it's solid now.  (The two screws in the photo center).  



Also be sure you don't have some debris on the belt that is causing it to "bump".  But I'd discount that possibility.



Have you checked the four carriage rollers?  When properly adjusted the carriage sould be snug.  If one roller is slightly loose, the carriage will wobble.  But this will appear more random.



-----------------

The K40 machine is not necessarily a bad <b>design</b>, but is is horrible <b>manufacture</b>.  So there's a lot of mechanical problems due to mis-drilled holes, wrong or stripped screws, metal burrs, and un-square cuts.



![images/30afd6935ea7e086977243535ac9f0d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/30afd6935ea7e086977243535ac9f0d9.jpeg)


---
**Стефан Синобад** *October 13, 2017 21:44*

Hello **+Nate Caine**. Thanks for trying to help.

I tried holding down the material I engrave, but it didn't improve the engraving at all.

The first thing I did was check for debris, and i cleaned everything perfectly, also didnt help :(

I checked the carriage rollers a few days ago, and one was loose, causing the whole thing to be loose, however I fixed this as well (now everything is snug) and still no improvement.

It just seems incredible to me that nothing improves the engraving at all. :(


---
**eric rice** *October 19, 2017 07:09*

Mine did exactly the same thing, I found my lens holder loose, the lens was walking around randomly. 


---
**Стефан Синобад** *October 19, 2017 10:01*

**+eric rice** The lens mounted to the carriage? Or one of the mirrors?

But I already tried all of this :(




---
**eric rice** *October 19, 2017 16:37*

The final lens. 

The focusing lens. Not mirrors.

If it can move around at all it will look like your problem.


---
**Стефан Синобад** *October 19, 2017 16:40*

Everything is tight... nothing can move around...


---
*Imported from [Google+](https://plus.google.com/+СтефанСинобад/posts/RjuFAU4Fier) &mdash; content and formatting may not be reliable*
