---
layout: post
title: "I have a k40 with light objects air assist head with F50.8mm lens"
date: February 14, 2017 21:47
category: "Discussion"
author: "Robert Selvey"
---
I have a k40 with light objects air assist head with F50.8mm lens. I have hard time cutting 3mm with two passes that's at 40.0 on the digital power display not sure what that would be for milliamps. In the video looks to me he has the same laser head as me.

Would love all the advice I can get thanks.




{% include youtubePlayer.html id="Hb7lgPF7Fms" %}
[https://www.youtube.com/watch?v=Hb7lgPF7Fms&t=10s](https://www.youtube.com/watch?v=Hb7lgPF7Fms&t=10s)





**"Robert Selvey"**

---
---
**Ned Hill** *February 14, 2017 22:08*

How fast are you cutting and what are you cutting?  


---
**Ned Hill** *February 14, 2017 22:15*

If we are talking 3mm ply then I typically can cut that in a single pass at 5mm/s at 30% power with assist.  Usually culprits for low power are bad alignment, piece not at the right focus (focus point should be set at 1/2 the thickness of the piece) or perhaps the lens not in the correct orientation (bump side up).


---
**Robert Selvey** *February 14, 2017 22:53*

I am cutting at 5mm at 40% the mirrors are centered and test fire in all four corners are centered the only thing I can think of is maybe the height of the bed. I did the line test to get the focal point. puts the tip of the laser head 1.25 inches from the bed.


---
**Ned Hill** *February 14, 2017 23:09*

I also have the LO air assist head with 50.8mm lens and my focus point is ~ 1in from the end of the air assist nozzle.  Can you post a picture of one of your cut attempts?


---
**Alex Krause** *February 14, 2017 23:11*

Depends on the plywood you are cutting... The glue used in some ply is difficult to laser 


---
**Robert Selvey** *February 14, 2017 23:57*

You want a pic of something I cut out or just one line and if just one line what power setting ?


---
**Ned Hill** *February 15, 2017 00:28*

one line is fine, say 40% at 5 mm/s


---
**Ned Hill** *February 15, 2017 01:48*

I just picked up on what **+Alex Krause** said, so a question that comes to mind is whether or not  you are not getting any burn through on a single pass or if it's just in certain spots?


---
**Robert Selvey** *February 15, 2017 16:02*

Sorry took so long for the pic of the test line 5mm at 40% line one is 1- 1/2 inches in away from head and line two is  1 inch away.

[https://plus.google.com/photos/...](/photos/109246920739575756003/albums/6387365933154575889/6387365934922185170)


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/EsWzArpmXtr) &mdash; content and formatting may not be reliable*
