---
layout: post
title: "So with halloween coming up, has anyone tried laser-cutting a pumpkin?"
date: September 26, 2016 00:48
category: "Materials and settings"
author: "Tev Kaber"
---
So with halloween coming up, has anyone tried laser-cutting a pumpkin?  :)





**"Tev Kaber"**

---
---
**Ariel Yahni (UniKpty)** *September 26, 2016 01:02*

Saw this today on the FB group

![images/003935d94b2ec8301a4116626e587df1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/003935d94b2ec8301a4116626e587df1.png)


---
**Alex Krause** *September 26, 2016 03:40*

I laserwebbed Freddy Krueger ;)

![images/b449ecfc2d3e841324d1c23d9e54110e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b449ecfc2d3e841324d1c23d9e54110e.jpeg)


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/Agipz96Svg9) &mdash; content and formatting may not be reliable*
