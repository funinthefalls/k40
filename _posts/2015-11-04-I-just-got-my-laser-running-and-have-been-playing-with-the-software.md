---
layout: post
title: "I just got my laser running and have been playing with the software"
date: November 04, 2015 22:34
category: "Software"
author: "Todd Miller"
---
I just got my laser running and have been playing with

the software.  LaserDRW looks to draw seals and it

moves the laser fast.  Using CorelDraw12 gallery items,

cut/burnt some basic shapes into 3 or 4mm hobby wood. 



Now the confusion. Using Corel, I drew a 77mm x 40mm

box and then cut it with the engraving manager

(adjusting ma's enough to actually cut it).  The laser does

what it's told but it retraces the cut again.  I did not find

any setting related to item count and I have my line style

set to hairline. 

I just want to draw and cut with one line...

 





Oh, I almost forgot...



When drawing a simple box or circle in Corel, why

does it always cut starting in the upper left hand

corner even if I drag the box to the other corner of

the paper ? 





**"Todd Miller"**

---
---
**Todd Miller** *November 04, 2015 23:28*

The box was a solid filled line, so I changed the steps to three, that worked !  Thank You !


---
**Joey Fitzpatrick** *November 05, 2015 02:40*

Change you lines to hairline and solid infill on the parts you want cut


---
**Ashley M. Kirchner [Norym]** *November 05, 2015 02:42*

Not a solid filled line. A solid filled BOX. There's a difference.


---
**Ashley M. Kirchner [Norym]** *November 05, 2015 02:43*

See this post:

[https://plus.google.com/+AllreadyGone/posts/U9f8mRUN4U8](https://plus.google.com/+AllreadyGone/posts/U9f8mRUN4U8)


---
**Ashley M. Kirchner [Norym]** *November 05, 2015 02:44*

If you're just cutting a shape, make the shape black, and the rest of the page white. If you just outline the shape, it cuts it twice. Make it black and it cuts once.


---
**Joey Fitzpatrick** *November 05, 2015 03:29*

That is what I meant.  Hope I wasn't too vauge


---
**Leanne Purnell** *November 06, 2015 09:03*

if im cutting I set the lines to be cut to 0.01mm only does one cut that way, I also change my line colour to red (not that it should make a difference). i also don't need to fill the part i want to cut. i use corel laser.



I kind of have it working like my Aspire 8 that i use with my cnc flat bed. have learnt some tricks from it that i can use with corel laser.


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/4NfZrDQfuHq) &mdash; content and formatting may not be reliable*
