---
layout: post
title: "Hello everyone, I'm looking for a rotary axis that I can purchase to mark Yeti Tumblers"
date: December 13, 2016 13:15
category: "Modification"
author: "Jamie Lowe"
---
Hello everyone,



I'm looking for a rotary axis that I can purchase to mark Yeti Tumblers. The builds that I have found are incomplete, inaccurate parts list/sources, or just won't work for my application. I really don't have much time to build one. Can anyone help with locating one to purchase, or know of someone looking to sell?





**"Jamie Lowe"**

---
---
**Anthony Bolgar** *December 13, 2016 13:49*

try [http://www.lightobject.com/Mini-2-Axis-Rotary-Ideal-for-K40-Engraving-Laser-Machine-P941.aspx](http://www.lightobject.com/Mini-2-Axis-Rotary-Ideal-for-K40-Engraving-Laser-Machine-P941.aspx)


---
**Jamie Lowe** *December 13, 2016 13:59*

I was looking at that one, but saw a review say that they couldn't use anything larger than 2" in diameter on it, due to its height. Do you know anything about that?


---
**Jamie Lowe** *December 13, 2016 13:59*

I was looking at that one, but saw a review say that they couldn't use anything larger than 2" in diameter on it, due to its height. Do you know anything about that?


---
**Anthony Bolgar** *December 13, 2016 14:02*

You will have that problem with any rotary, the solution is to cut the bottom open in the work area, and raise up the case on supports to give more Z axis height. I used metal framing studs to make a square platform for the laser to sit on it raised it up 3.5" in height, and I use an aluminum oven drip pan underneath to prevent stray laser strikes from hitting the table it sits on. Hope this helps.


---
**Jamie Lowe** *December 13, 2016 14:04*

**+Anthony Bolgar** oh, so regardless of what rotary I used, none of them have a low enough axis for 3-4" diameter piece? I could swear I've seen people using them without the bottom cut out.


---
**Jamie Lowe** *December 13, 2016 14:04*

**+Anthony Bolgar** oh, so regardless of what rotary I used, none of them have a low enough axis for 3-4" diameter piece? I could swear I've seen people using them without the bottom cut out.


---
**Anthony Bolgar** *December 13, 2016 14:08*

You are probably remembering seeing someone with a 50 or 60W machine doing it, they have deeper work areas than the K40


---
**Ned Hill** *December 13, 2016 14:50*

Yeah with a 50.8mm lens focal length you are looking at a max depth of around 91mm (3.5") for most K40s I believe. 




---
**Jamie Lowe** *December 13, 2016 15:00*

Ah very true. I wonder if the LO rotary would work with a 38mm focal length for my needs though. 


---
**Ned Hill** *December 13, 2016 15:05*

Not sure.  That will only gain you about 0.5" though.


---
**Jamie Lowe** *December 13, 2016 15:12*

I guess I'll have to try it and report back! Lol. 


---
**Jamie Lowe** *December 13, 2016 15:13*

Thanks for your helps, guys




---
**Jamie Lowe** *December 13, 2016 15:29*

Just looked at the LO rotary again and it requires a different driver for the motor. I don't understand why they wouldn't use a Nema 17 motor to be a direct replacement.


---
*Imported from [Google+](https://plus.google.com/101114189070867293080/posts/FPVaf2uZpQP) &mdash; content and formatting may not be reliable*
