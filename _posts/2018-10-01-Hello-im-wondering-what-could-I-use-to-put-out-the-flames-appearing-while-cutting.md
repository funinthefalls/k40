---
layout: post
title: "Hello, i'm wondering what could I use to put out the flames appearing while cutting"
date: October 01, 2018 12:25
category: "Air Assist"
author: "Manon Joliton"
---
Hello, i'm wondering what could I use to put out the flames appearing while cutting. I've checked few 'diy tutos' on how to make an air assist but I haven't found what machine they use to blow air.

Could a air pump (use to inflate mattress) work ? pic1

[https://www.raviday-matelas.com/media/catalog/product/cache/16/image/700x/9df78eab33525d08d6e5fb8d27136e95/g/o/gonfleur-electrique-intex-220-volts.jpg](https://www.raviday-matelas.com/media/catalog/product/cache/16/image/700x/9df78eab33525d08d6e5fb8d27136e95/g/o/gonfleur-electrique-intex-220-volts.jpg)



Or maybe a mini air compressor used to inflate bike and car tyres (less than 10bar, 12V, 35L/min) would be better ? Pic2

[https://www.lidl-shop.be/media/c0e9dfe1fd063a01d4486b3783f9cd9c.jpeg](https://www.lidl-shop.be/media/c0e9dfe1fd063a01d4486b3783f9cd9c.jpeg)



I don't want to spend a lot of money (as i don't have sadly ^^)

I need something that is enough powerfull to put out the flames but as i cut veneer with my k40, I don't want it to blow my veneer pieces all lover the laser (and the vent to suck them up)....

Thank you in advance

Manon





**"Manon Joliton"**

---
---
**Joe Alexander** *October 01, 2018 13:21*

I personally use this as my air assist as do several other people:

[amazon.com - Amazon.com: Commercial Air 3 W/ Air Control Kit: Pet Supplies](https://smile.amazon.com/gp/product/B019UJONV8/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)  its relatively quiet and seems to work well.


---
**Manon Joliton** *October 01, 2018 13:47*

how many GPH does your one make ? do you think it makes a huge difference ?

thank you :3


---
**Joe Alexander** *October 01, 2018 13:49*

im not sure on the gph but it puts out a steady stream that does extinguish the flames when cutting wood or similar materials, and doesn't blow my material around.


---
**Manon Joliton** *October 01, 2018 14:12*

**+Joe Alexander** ok thank you, I'm gonna look arround for one


---
**Don Kleinschnitz Jr.** *October 01, 2018 14:14*

I use this one @ 1300GPH:



[amazon.com - Amazon.com : EcoPlus 1300 GPH (4920 LPH, 80W) Commercial Air Pump w/ 8 Valves &#x7c; Aquarium, Fish Tank, Fountain, Pond, Hydroponics : Aquarium Air Pumps : Garden & Outdoor](https://smile.amazon.com/gp/product/B002JLGJVM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)



You might get away with less but the smaller one is unavailable on amazon and this isn't aweful expensive. I control the output with a valve.


---
**Manon Joliton** *October 01, 2018 14:23*

**+Don Kleinschnitz Jr.** ok, i'm also considering buying an aerograph kit, do you think an aerograph compressor would be strong enough to put out the flames ? it's only 10-15L/min (~1500GPH)




---
**Don Kleinschnitz Jr.** *October 01, 2018 16:13*

**+Manon Joliton** Assuming you mean an "Airbrush Compressor", I used to use one but it was way to noisy and no extra air capacity. Many use air brush compressors.



15 L/min = 237 GPH???


---
**James Rivera** *October 01, 2018 20:29*

I use a super cheap aquarium air pump, but all I wanted is to keep the smoke away from my lens, which it does, and it is very quiet.


---
**Tom Traband** *October 02, 2018 19:04*

The noise from that indicator would drive me crazy. My current setup involves a 5$ aquarium air pump, vinyl tubing and a .35 welding tip. The tubing is zip-tied to a copper wire so it can be simed at the laser focus point.


---
**Manon Joliton** *October 03, 2018 10:15*

**+Tom Traband** Do you know how many GPH your pump is ?


---
**Tom Traband** *October 03, 2018 12:56*

I bought it locally and don't have the package any longer but similar ones on Amazon sized for 10 gallon fish tanks list 1.8 l/min. I think it works because I'm not trying to fill a whole lens-surrounding nozzle, just pointing a jet of air right at the cutting location.


---
*Imported from [Google+](https://plus.google.com/110371358543635278419/posts/A1nGEMMZUTk) &mdash; content and formatting may not be reliable*
