---
layout: post
title: "This is old hat to most of you that completed your smoothie conversion and are happily engraving and cutting"
date: November 14, 2016 04:52
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
This is old hat to most of you that completed your smoothie conversion and are happily engraving and cutting.



Tonight I completed all the basic testing of my K40-S design. The culmination of which was homing from the local control panel. The GLCD is pretty handy.



The design worked exactly as I had documented with only one change.



I had to flop a coil on each stepper to get it going the right way with the default config settings. (I know I could just flip it in software but I like things orderly).



Looks like there isn't much I have to do with the documentation other than insure that it matches what I have done is clean and the parts I have used are included.



Thanks to everyone that has been on this journey for your help.



Tomorrow I clean up the gantry wiring/air feed with my drag chain and get on to learning/using LaserWeb.


**Video content missing for image https://lh3.googleusercontent.com/-S2bGIabu62k/WClDH_sPYHI/AAAAAAAAgzc/EKQ8KJiEcWkWAH3PycI0Xut9sUV2DlxcwCJoC/s0/20161113_212936.mp4**
![images/b35200753843c3dc23dca79b021430bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b35200753843c3dc23dca79b021430bc.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Carl Fisher** *November 14, 2016 23:10*

ahh, I wish my display were as bright as yours. No matter how I adjust the contrast on mine it's always terrible to view from all but the perfect angle.




---
**Carl Fisher** *November 14, 2016 23:10*

Nice job by the way :)


---
**Don Kleinschnitz Jr.** *November 15, 2016 12:39*

**+Carl Fisher** thanks Carl. 

Did you install the 5V regulator. I use both one on the smoothie and the GLCD shield?


---
**Carl Fisher** *November 15, 2016 12:47*

Not that I'm aware of. It pulls power straight from the controller. Its on dim when only under USB board power but kicks up when I turn the machine on. It seems to be an issue with just a really narrow viewing angle on mine. 


---
**Don Kleinschnitz Jr.** *November 15, 2016 15:27*

If you use a GLCD you need the on-board 5v regular or you get a dim display.



[http://smoothieware.org/panel](http://smoothieware.org/panel)



"More power

If you are using the onboard 5V regulator to step down from 12/24V, check the current draw required for your panel - depending on the color/backlight on your GLCD, it may require >250 mA for the backlight.



The normal recommended 5V regulator will not supply enough current for those panels - if the panel powers up, it will have very low contrast.



Use Recom part R-78E5.0-1.0 instead - it will supply 1 amp (vs 0.5 amps for the normally recommended regulator).



It is available at Digikey, and likely at other major electronics component sites.



See Voltage Regulator.



As an alternative to replacing the R-78E5.0 part on the Smoothieboard, solder a 5V 0.5 amp or 1 amp regulator to the adaptor card in the location marked. Ordinary 7805 regulators will work in that role. See [en.wikipedia.org - 78xx - Wikipedia](https://en.wikipedia.org/wiki/78xx)"


---
**Carl Fisher** *November 15, 2016 21:21*

What about tapping the 5v pin on the power supply? Right now the only thing it's driving is my targeting laser, otherwise it's unused.


---
**Carl Fisher** *November 15, 2016 21:22*

**+Ray Kholodovsky**  Any thoughts on this since I'm using your board to drive and power the glcd?


---
**Ray Kholodovsky (Cohesion3D)** *November 15, 2016 21:28*

Don's talking about smoothieboard.  I already have a 5v regulator on my boards (switching, not a 7805 linear which gives off more heat than a stove).  It can output 2 amps, but you are limited to 850mA - 1amp range based on the inductor used.  You have more than enough oomf to drive your GLCD and the board logic.  Please do not tap the 5v from the laser psu.  That is one place where crossing swords becomes problematic. 


---
**Carl Fisher** *November 15, 2016 21:41*

**+Ray Kholodovsky**, Thanks, that's why I wanted to run it by you. I sort of figured that was the case. I can always pick up another GLCD to see if I just have a bum board.


---
**Don Kleinschnitz Jr.** *November 16, 2016 01:35*

**+Carl Fisher** I would reccomend checking the 5V on the GLCD to see of itd drooping. 

Yes I thought you were running smoothie.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/gw3BxNE49Xs) &mdash; content and formatting may not be reliable*
