---
layout: post
title: "Just got my machine today, got everything set up and tried my first etch"
date: June 25, 2016 06:10
category: "Object produced with laser"
author: "Tev Kaber"
---
Just got my machine today, got everything set up and tried my first etch. It worked! Obviously I need to tweak the power/speed/focus, but I'm pretty impressed that it worked on the first try.



This is without any adjustment of the mirrors or lens, just threw a piece of wood in and gave it a try.



I have an air assist head and new lens I may install tomorrow if I can pick up a cheap air compressor at Harbor Freight.



![images/5b843e7a2b56d384515dd16df6851575.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5b843e7a2b56d384515dd16df6851575.jpeg)
![images/19248c4c3c1e080de784648e7c31dcd5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19248c4c3c1e080de784648e7c31dcd5.jpeg)

**"Tev Kaber"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 06:46*

That turned out pretty good, with minimal scorching or soot smearing on the engrave edges. Whatever settings you used must have been pretty close on spot on for that wood.


---
**Robert Selvey** *June 25, 2016 16:18*

To stop the soot and smearing try covering the area with masking tape before engraving.


---
**Tev Kaber** *June 25, 2016 16:34*

Is there a wider masking tape that people use for that?


---
**Robert Selvey** *June 25, 2016 16:52*

I have seen 6" wide masking tape on on amazon not sure if lowes has it. But lowes does show masking film not sure if that would work that's 12" wide.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 19:27*

Alternatively, I've seen here in Australia a 50mm wide masking tape for painting. 6" would be much nicer size though if you can get it.


---
**Ned Hill** *June 26, 2016 05:06*

I'm currently just using some 2" masking I have on hand and just butting strips together.  Alternatively they make masking film like Frisket Film which comes in 12in wide rolls.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/CgKyBStHhhW) &mdash; content and formatting may not be reliable*
