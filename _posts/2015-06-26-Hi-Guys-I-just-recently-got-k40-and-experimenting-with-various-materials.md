---
layout: post
title: "Hi Guys, I just recently got k40 and experimenting with various materials"
date: June 26, 2015 00:40
category: "Materials and settings"
author: "Eldar Khaliullin"
---
Hi Guys, I just recently got k40 and experimenting with various materials. One of the projects that I was asked to try is to cut some rubber gaskets (see photo). I tried various settings , but rubber comes out fragile on edges. For example, I tried to cut at lower power and with multiple passes. Material at the edge comes out fragile and burned. What are some of the best settings that I should use for this ? Should I cut at high power and high speed ? or lower power and multi-passes? Is air assist something that would help here ?

![images/bed46b94a6b8e8cd0a21ab979df7222e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bed46b94a6b8e8cd0a21ab979df7222e.jpeg)



**"Eldar Khaliullin"**

---
---
**ThantiK** *June 26, 2015 02:11*

There is special rubber out there that is formulated for laser cutting.  Normal rubber is going to do this to you, at least that's been my experience.


---
**Eldar Khaliullin** *June 26, 2015 02:22*

**+ThantiK** Thanks for the tip! Unfortunately, I have a specific requirement for this material because of some chemical properties. So I would have to stick with it.


---
**andmif** *June 26, 2015 13:33*

not expert in rubber cutting at all, but I think you have right ideas. I would focus on fast cutting to minimize rubber exposure to the laser. Slow/multi-pass cutting increases exposure to the heat.

And yes, air-assist would be on my list as well: at a minimum it removes the smoke, increasing laser efficiency.

one more thing to try is to wet the rubber a little bit. I know people had better results with leather cutting by dunking it quickly in water, this seem to decrease brittle and charring effects on leather. Maybe it'll help with rubber as well, just don't make it too wet. To clarify, I've never tried cutting wet materials myself, just saw the results.So take precautions if trying first time.


---
**Eric Parker** *July 03, 2015 07:26*

Can you freeze it?  I know this method is occasionally used for mechanical milling of rubber.


---
**Scott Marshall** *December 18, 2015 17:11*

Hot and fast is best, or switch to silicone based rubber. It either burns or not, won't soften like more common buna and neoprene. Not sure about EPDM, but it's readily available in sheets.

Good ole paper gaskets are often forgotten in this era of high tech toilet paper.....


---
**Eric Parker** *December 19, 2015 20:58*

**+Scott Marshall** One of the very first things I produced with my laser was a replacement paper gasket for my sink which was leaking, and I needed to repair at 1:30 in the morning.  I keep trying to convince my friend that a paper or wax-impregnated paper gasket would be perfect for his specific custom application, but he won't listen.


---
**Scott Marshall** *December 21, 2015 13:18*

I know it's not a direct solution, but if the material is thin enough, a vinyl style plotter cutter might do the trick.



 I recently bought a Silhouette Cameo, and it's amazing what it will cut, and just how precise it is for a couple hundred bucks. Nice machine, but the software is so-so (they're happy to nail you another hundred bucks for the upgrade though)



 It's capable to 1/16" (0625) hard cardstock, and probably would handle .080 rubber. I know the craft people are cutting "hobby foam" with it, which runs pretty thick. (maybe that would serve as a low temp gasket?) I've never had the blade depth past 1/2 way, even cutting very heavy (cereal box) cardstock.so, it cuts quite deep and may go up to .100 in softer material. It has a double pass mode, which works good for difficult materials that tend to stick in the sheet at sharp corners.



It has a tacky coated "mat" to which you apply the material, and that stiffens up pliable materials quite well so they don't 'push' around while cutting. 

It uses a carbide blade, depth and  speed adjustable.

I'll try a rubber cutout if I can find some material in the shop. 



Beyond that, the "laser" rubber sounds good as long as it doesn't come with "laser" prices....





More......

Edit, just thought of this, if you only need a few copies, maybe you could cut a pattern with the laser, and trace it with an Xacto.



 Beyond that, maybe cut a plywood die frame with your laser, and glue blade sections into the kerf, then use a cutting mat and rubber hammer to die cut the gaskets... 



Good luck, Scott


---
*Imported from [Google+](https://plus.google.com/+EldarKhaliullin/posts/UQHFZk5neuK) &mdash; content and formatting may not be reliable*
