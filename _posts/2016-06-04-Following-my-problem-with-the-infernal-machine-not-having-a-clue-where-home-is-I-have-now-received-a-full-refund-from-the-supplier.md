---
layout: post
title: "Following my problem with the infernal machine not having a clue where home is, I have now received a full refund from the supplier"
date: June 04, 2016 13:13
category: "Original software and hardware issues"
author: "Stephen Smith"
---
Following my problem with the infernal machine not having a clue where home is, I have now received a full refund from the supplier. I tried to get them to exchange the machine but they simply refunded all my money. I am wondering if the homing problem would be solved by fitting a new motherboard. Just to recap, the machine when switched on tries to home somewhere bottom right but off the limits of the 300x200 area. Any suggestions to a possible other remedy will be greatly appreciated.





**"Stephen Smith"**

---
---
**Jim Hatch** *June 04, 2016 13:32*

Check the cable attachments to your board. It could be they're not attached to the right connectors or they're not right side up. I had a similar problem when I stripped my gantry out and didn't label the cables "because there are only a couple" ☺ Fortunately I had a picture of the original setup from when I was getting the serial # for the board.



That should fix the direction and the stop. It's waiting for a signal from the stop on the head to tell it that it's reached the end but the stop isn't down there, it's up on the other end of the box.


---
**busnut2** *June 04, 2016 13:39*

Second the suggestion.  Possibly the X and Y steppers are reversed.

Had one that way.  Also check the laser setup locations.


---
**Jim Hatch** *June 04, 2016 14:01*

If you've got the molex connector for one and the flat for the other they're likely correct (the flat goes to the X axis motor on the left rail) and there's only one flat connector on my board (Nano M2). But it could be inserted upside down. The molex on mine for the Y had two board connection possibilities and may even had the ability to be inserted upside down as well but I'm not certain. If a couple of quick checks on your end don't fix it, let us know which board you have and types of cable connections. 



BTW, it's entirely possible (although maybe not too likely) that they put the gantry in backwards - that would affect the Y axis stop because the blade for that is mounted on the top left corner of the box on my unit).


---
**Derek Schuetz** *June 04, 2016 15:14*

Well since you received all your money back just but a $40 ramps kit on Amazon and upgrade


---
**Jim Hatch** *June 04, 2016 15:59*

**+Derek Schuetz**​ good suggestion but I figured if he gets it working stock first he'll know the rest of it works (power supply, laser tube). Then he can upgrade to a Ramps or Smoothie and LaserWeb.


---
**Greg Curtis (pSyONiDe)** *June 04, 2016 17:07*

It should be homing to the upper left.



The motor are wired backwards, had the same problem with a Delta 3d printer I fixed. Someone wired one motor in reverse, so when going home (up) one tower would go down.



Flipped a couple leads and voilà, good to go.


---
**Jim Hatch** *June 04, 2016 19:55*

**+Greg Curtis**​Yep - if he flips the connector that should take care of a backwards wired motor too.


---
**Ben Walker** *June 05, 2016 16:30*

When I was testing mine doing similar to yours I ended up totally disconnecting the end stops at the board it presented exactly what you are describing here.  It turned out the solder on the end stops was either non-existent or severely lacking good connections.  I pulled the gantry and resoldered.  Now I am homing.  I wish that was the end of the saga but that did prevent the head from trying to travel across state lines.  It's good that they refunded you.  Now you have a tinker toy if there is any good to be seen in the situation.


---
**Greg Curtis (pSyONiDe)** *June 05, 2016 16:42*

That's an interesting factor I didn't think of. After it does home up and left, it goes down and right until the sensors report clear. If they are constantly reporting blocked, or disconnected, it will try to move away until they report otherwise.


---
*Imported from [Google+](https://plus.google.com/111326322372516669266/posts/ZmTV37evvYZ) &mdash; content and formatting may not be reliable*
