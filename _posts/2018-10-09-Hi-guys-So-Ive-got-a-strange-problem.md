---
layout: post
title: "Hi guys... So I've got a strange problem..."
date: October 09, 2018 23:07
category: "Original software and hardware issues"
author: "Akinwole Akinpelu"
---
Hi guys... So I've got a strange problem... All of a sudden, my  laser cutter wouldn't Fire. There's no door safety lockouts, and the only safety system is the water flow. The laser wouldn't even test Fire if the chiller is not on.



But now, all of a sudden, it wouldn't fire whilst processing a job.

Test fire works fine. But running a job, the laser doesn't fire but the head moves fine



The cutter has the usual M2 nano controller and I use K40 whisperer software to control it.

The fire indicator light also doesn't come on. But again, it test fires fine and the indicator comes on as it should



No settings have been changed



What could I missing?



Thanks





**"Akinwole Akinpelu"**

---
---
**Joe Alexander** *October 09, 2018 23:51*

I would check the wire going from the M2nano to the Laser PSU on "L" and make sure its not damaged or anything, then I would check the flow meter by bypassing it(they have been known to go bad). Other than that i'm not sure since it fires from the test button.


---
**Akinwole Akinpelu** *October 09, 2018 23:56*

Thanks... I'm guessing you mean the LO pin/wiring.



I'll check continuity of this wire and report back



THanks


---
**Akinwole Akinpelu** *October 11, 2018 07:04*

Hi Joe, I've given up trying to trace  the fault. I need a new controller anyway. I looked at the cohesion3D but I think I need something a bit more especially on the offline functionality front. I've gone for the Trocen Awc708c lite controller.



This doesn't have integrated drivers like the M2/C3D controllers. Can you please tell me what integrated controller the M2 uses, what amps is supplied to the motor (thinking 1amp as no heat sink) and the microstepping value?



Thanks 


---
**Joe Alexander** *October 11, 2018 08:36*

What do you mean offline functionality front? The C3D doesn't require an internet connection to use as it is accessed through a USB connection. And the smoothie firmware is usable with an LCD screen. As to what controller the M@ uses I don't know, they usually don't share that information. I looked up my motors once but never found anything, they're usually 1.8° 200steps/revolution and draw no more than an amp each.


---
**Akinwole Akinpelu** *October 13, 2018 13:51*

By offline, I mean not needing a computer to operate. 



I looked at the cohesion board and it looks good especially the fact that it's a drop in replacement. But for it to be used offline (i.e run jobs off an SD card) you need the screen which takes the price up to around 200bucks. 



I have just purchased the Trocen 708 controller which is about the same price and has a bit more features and a better screen. waiting on delivery


---
*Imported from [Google+](https://plus.google.com/112636117282447034401/posts/RL9A1BgUMgE) &mdash; content and formatting may not be reliable*
