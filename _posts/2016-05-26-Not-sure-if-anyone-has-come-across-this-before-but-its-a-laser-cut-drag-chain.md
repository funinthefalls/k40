---
layout: post
title: "Not sure if anyone has come across this before, but it's a laser cut drag chain"
date: May 26, 2016 13:46
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Not sure if anyone has come across this before, but it's a laser cut drag chain. I just started searching before to get a drag chain for the laser (as I'm having issues with my makeshift solution) & came across this. I'm in the process of cutting one right now.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Carl Fisher** *May 26, 2016 13:51*

I may have to try this out. I tried to use the plastic ones but the pivot point doesn't allow for a tight enough radius bend to allow the head to move to all 4 corners. It always binds up at one corner due to lack of bend. 



This one moves the pivot to the edge which should allow a much tighter radius




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 13:53*

**+Carl Fisher** I haven't tried a plastic one before as didn't really feel the need for it, but now with 2 hoses coming off my air-assist nozzle I definitely need something to prevent it tangling/locking up & causing my engraves to miss steps & jump 10mm to the right/left (after 1.5 hours of engraving multiple layers at multiple power levels... aaaah!).


---
**Carl Fisher** *May 26, 2016 13:56*

Been there. I have my air hose and power wire for the target laser run through some spiral wrap right now and using a spring connected to the lid to keep enough tension to keep it clear of the front of the head. The problem is occasionally it binds up on the far side of the gantry by falling over the edge and causes missed steps. I'd really prefer a drag chain but just couldn't get the plastic ones to orient in this machine in such a way that it would clear all obstructions and allow full movement. Drove me crazy.


---
**Stephane Buisson** *May 26, 2016 14:53*

you could try for the fun, but it could be more precise and cheaper to buy 1m from ebay. the first link is different with holes to screw on the frame. precision is key, as if to tight it will not run smoothly, too loose and it will bend in the way.

maybe not worth the effort. for inspiration my 2005 video


{% include youtubePlayer.html id="0r5sdS8zqY0" %}
[https://www.youtube.com/watch?v=0r5sdS8zqY0](https://www.youtube.com/watch?v=0r5sdS8zqY0)


---
**Scott Marshall** *May 26, 2016 14:59*

2mm MDF is unobtainium in the United States, 

Been waiting patiently for someone to start importing it. 



You Aussies and the Brits seem to have the market cornered.


---
**Carl Fisher** *May 26, 2016 15:01*

Maybe some 0.10 acrylic sheet would be a good substitute. A bit pricey though depending on how long the chain is going to be. Balsa is probably too weak.


---
**Purple Orange** *May 26, 2016 17:17*

And I found this version earlier today: [http://www.thingiverse.com/thing:1275013](http://www.thingiverse.com/thing:1275013)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 19:05*

I did make it, out of 2.7mm ply. It is a bit loose/slack due to the design & possibly my kerf. But I found a different method to mount it & at the same time mounted my lasercam to the y-rail also. I'll up some pics/vids later of it all.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/BdH6mvz2dst) &mdash; content and formatting may not be reliable*
