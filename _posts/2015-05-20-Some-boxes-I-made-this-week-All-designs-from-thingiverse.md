---
layout: post
title: "Some boxes I made this week. All designs from thingiverse -"
date: May 20, 2015 11:17
category: "Object produced with laser"
author: "Chris M"
---
Some boxes I made this week. All designs from thingiverse -



[http://www.thingiverse.com/thing:9452](http://www.thingiverse.com/thing:9452)

[http://www.thingiverse.com/thing:28103](http://www.thingiverse.com/thing:28103)

[http://www.thingiverse.com/thing:14018](http://www.thingiverse.com/thing:14018)



![images/702bdc7b5ff668c85bc4cfb29e3f1a17.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/702bdc7b5ff668c85bc4cfb29e3f1a17.jpeg)
![images/159b43bdc423e7204f2c1b9d17756108.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/159b43bdc423e7204f2c1b9d17756108.jpeg)
![images/b170e01c7a72e0ed229344f56cb1627d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b170e01c7a72e0ed229344f56cb1627d.jpeg)
![images/d0ee937c44e6c94fcf5479a70dcd7723.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d0ee937c44e6c94fcf5479a70dcd7723.jpeg)

**"Chris M"**

---
---
**Mauro Manco (Exilaus)** *June 09, 2015 10:52*

[http://www.frag-den-spatz.de/boxdesigner/menue.htm](http://www.frag-den-spatz.de/boxdesigner/menue.htm)


---
*Imported from [Google+](https://plus.google.com/108727039520029477019/posts/DAuzw7HYDaQ) &mdash; content and formatting may not be reliable*
