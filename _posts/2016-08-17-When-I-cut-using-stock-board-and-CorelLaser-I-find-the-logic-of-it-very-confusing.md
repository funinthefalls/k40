---
layout: post
title: "When I cut using stock board and CorelLaser, I find the logic of it very confusing"
date: August 17, 2016 19:07
category: "Original software and hardware issues"
author: "Bob Damato"
---
When I cut using stock board and CorelLaser, I find the logic of it very confusing. If I have a word that I want to cut, it does maybe 100 'dots' first in seemingly random places all over, then maybe the inner circle of an "O", then eventually it will cut the text.  Same with graphics such as arcs and circles. It will do a part of the arc, then a bunch of dots, then more of the arc etc.  



My process is:

- Create document (text and graphics) in Photoshop

-save as bmp

-Open CorelLaser, create doc in appropriate size and dpi

-import each piece of the drawing onto each layer within corellaser

-engrave or cut as needed



While this pretty much works for me, the issue with Coreldraw is that if I have a layer Im trying to align with another layer, I cant see one through the other, so its almost impossible.



Always looking to improve my workflow, so any tips appreciated!

Bob







**"Bob Damato"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 17, 2016 21:25*

I use vectors for cutting (Illustrator), rather than raster (Photoshop). I'd consider testing vectors to see if you still find the same issue with cutting. Even just test in CorelDraw by drawing a circle or rectangle (or other more complicated shape) & see if it does the weird random dots things still.


---
**Bob Damato** *August 17, 2016 22:02*

I figured it had to be something like that, thank you. I am 100% completely illiterate in illustrator however. Might be time to start learning a few things...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 18, 2016 04:47*

**+Bob Damato** Either that or a lot of people use Inkscape or even Corel Draw for doing up their vectors (SVG format usually).


---
**Robi Akerley-McKee** *August 18, 2016 05:59*

I highly recommend Inkscape 0.91.  I use a MKS Gen v1.4 (arduino 2560/RAMPS 1.4) controller in the laser and use the Turnkey Laser Extension in inkscape to generate my g code files.  I use Pronterface to upload the gcode files to the controller.  I'm using the Build-Master-marlin firmware in the arduino/MKS


---
**Bob Damato** *August 18, 2016 13:00*

**+Robi Akerley-McKee**   Wow. Im 72% sure that was english but wouldnt bet my life on it :)    will inkscape work for me with just a completely stock setup? Thanks for the recommendations everyone.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 18, 2016 20:58*

**+Bob Damato** I believe you can use inkscape to design your files & import them into CorelDraw/Laser. Provided you can get the right output format (SVG is good) then should be no problems.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/5JmpvqoUFBM) &mdash; content and formatting may not be reliable*
