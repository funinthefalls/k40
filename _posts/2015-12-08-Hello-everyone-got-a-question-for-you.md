---
layout: post
title: "Hello everyone, got a question for you"
date: December 08, 2015 23:35
category: "Discussion"
author: "Eward Somers"
---
Hello everyone, got a question for you.



I have a k40 that uses the software laserdraw(plugin for correldraw)

I did have to replace the tube because it broke in transit. This forced me to realign all mirrors but I did clean them with alcohol.



Now... I'm having serieus issues getting it to cut through.. well anything thicker than paper. The strength it uses for engraving and cutting seems to be the same.(the cut is nearly as deep as an engraving..)



I don't know what the issue is... software or hardware. 

The documentation I received with it was horrible so I might have some settings wrong.(I followed some pdf I found in here and no progress yet)



Any help is very much appreciated! :) 



P.S. The materials I'm trying to cut are 3mm MDF & white acrylic 





**"Eward Somers"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 09, 2015 00:23*

Could possibly be similar issue to what I had. I could engrave perfectly, however couldn't cut through anything in 1 pass. Turns out my lens was upside down.


---
**Eward Somers** *December 09, 2015 11:22*

I tried it.. Right now I just had the laser fire manually with the test button on the machine and it fired right through the acryl. (at 90% power)



Could it be a software problem?


---
**Gary McKinnon** *December 09, 2015 11:30*

Some people have power issues due to bad grounding, some have had them due to misalignment. What have you tried in the PDF ?


---
**Eward Somers** *December 09, 2015 11:45*

I used the settings they recommended... But.. what's annoying me is that I can't adjust the power settings in laserdraw. I can only change the speed. And what do you mean, bad grounding?


---
**Eward Somers** *December 09, 2015 11:46*

The machine is grounded with the power cable already but there is some.. extra grounding cable hanging out. Engineer friend of mine said that it shouldn't be necessary to connect that one. I might try that one


---
**Victor Hurtado** *December 13, 2015 11:42*

Gracias los pondré en practica


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2015 13:36*

**+Eward Somers** I've recently found another issue with mine that prevented it cutting. The cutting table was too far away from the lens so the focus of the beam was off. I adjusted it closer to the lens (~4mm) and it cuts perfectly now. Maybe check this by placing some different thickness objects beneath what you wish to cut to test if that is causing your issue.


---
**Eward Somers** *December 13, 2015 14:02*

**+Yuusuf Sallahuddin** hmm.. Well I'm currently using the standard distance that it was set up for... Might give this a try. I was wondering something, What line thickness do you use for your cutting lines? I heard that it defaults all lines to 'engraving' if they aren't the correct pixel thickness. 



I'm leaning towards it being a software issue because a manual testfire punches through acryll&mdf without much trouble. When I try cutting with the software it leaves a... 0.4mm depth or something? I'd have to redo it for 10 times or so to get through it with the software.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2015 15:21*

**+Eward Somers** When I cut I don't use a line thickness. I put line thickness to "None" & I set the shape's background colour to black. Whenever I have used a line thickness I notice it trying to cut twice (once on each side of the line).



Manual test fires of mine also punched through the leather that I was trying to cut without too much issue (just tapping the button for a quick tap). But when it was moving trying to cut at any speed (even 1mm/s) and any power (even 10mA+) it wasn't working.



My settings for cutting through 4mm leather should get through 3mm acrylic/MDF I'd imagine. I used ~2.5-3.0 mm/s @ 7mA & 1 pass. If that doesn't work for you (even trying to alter the distance from the lens) then maybe it is a software issue.


---
**I Laser** *February 04, 2016 04:16*

Bit of a 'necro', but make sure your line setting is set to 0.01mm in corel, the default hairline setting is too thick...



Basically when the line is too thick, it's attempting to cut the object out, hence the multiple cuts.


---
*Imported from [Google+](https://plus.google.com/101993772584820352944/posts/XUZq9xHNAoX) &mdash; content and formatting may not be reliable*
