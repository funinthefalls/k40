---
layout: post
title: "I have some free time this week so I have decided to tackle cleaning up the Marlin code of Turnkey Tyrannys fork"
date: January 11, 2016 18:00
category: "Software"
author: "Anthony Bolgar"
---
I have some free time this week so I have decided to tackle cleaning up the Marlin code of Turnkey Tyrannys fork. There is so much code that is 3d printer specific that can be eliminated from the build. That will leave some room for feature enhancements and hopefully speed up the code as well.  Let me know if there is a specific feature you would like to see added, and if I can figure it out, I will try to add it. I am sure that we can come up with a simple streamlined version with the collective knowledge of the group (Hint hint Peter and Arthur)





**"Anthony Bolgar"**

---
---
**Ariel Yahni (UniKpty)** *January 11, 2016 19:36*

**+Peter van der Walt**​ not needed anymore unless he wants to. I changed my ramps on my printer for the rumba to use it with Turnkey as everyone esle


---
**Ariel Yahni (UniKpty)** *January 11, 2016 19:49*

I see there is a specific section about the laser specs. would be good to know what is needed for a laser diode


---
**ChiRag Chaudhari** *January 11, 2016 22:49*

**+Anthony Bolgar** That is pretty awesome. I was thinking the same thing as there is so much useless (well for laser part) stuff in there. 



If I may suggest, if you can add code to change laser test fire power using rotary knob, it will make life bit easy when you want to test different material and see how much it burns at different power levels.



Thanks in advance!


---
**Anthony Bolgar** *January 11, 2016 23:49*

As my brain slowly melts from trying to map out all the different dependencies in the marlin firmware it is dawning on me that this is a larger project than I had anticipated. I am still going ahead with it, if for no other reason than to sharpen my rusty coding skills. Time to buy a bigger white board....lol!




---
**Anthony Bolgar** *January 11, 2016 23:57*

I also want to make sure that any changes I make allow LaserWeb to continue to function with the firmware. I am very happy with the direction Peter is taking the interface, and am very impressed with his dedication and quickness that he reacts to issues and feature requests....good old customer service at its best. Thanks Peter.


---
**ChiRag Chaudhari** *January 12, 2016 00:47*

yup lean that Marlin and with Peters LaserWeb K40 is going to get whole new life!


---
**Anthony Bolgar** *January 12, 2016 04:07*

SO how hard can it be....remove all reference to 11 M codes in the firmware....oh but wait, there is a little bit here, oh and in a totally unrelated file (or so you would think) there is a snippet of code....which then references a completely different fil.....and so on and so on.....grrrrrrrr! It doesn't help either that the code was worked on by God knows how may people each with their own coding style, formatting etc. But I promise, I will slay this bloated code beast, and from it's ashes shall rise a PHEONIX (hmmm....I think I just came up with the perfect name for the firmware once the streamlining is completed).


---
**Anthony Bolgar** *January 12, 2016 04:57*

I'm starting to lean that way as well Peter. But if I can conquer this beast with a little help from the group, I will have a real sense of accomplishment. Haven't done any real coding for the last 14 years, the family business has been keeping me busy. But at one time I was actually quite good at it. What I really miss is the hardware design. I was lead designer on the retrofit of all the CANDU nuclear reactors for the fuel handling system. We took the 1960's tech and brought it into the modern world. (Late 1990's)


---
**Anthony Bolgar** *January 12, 2016 04:58*

I had to give myself a crash course in using github today, I think I have a good handle on the workflow now.


---
**Anthony Bolgar** *January 12, 2016 05:11*

Well, it is time for me to sleep, but will definately mull over the idea of forking repetier.

It would be nice to have some help on the project.


---
**Ariel Yahni (UniKpty)** *January 12, 2016 17:58*

I'll go try repetier on my ramps. It seems so much simpler. 


---
**Anthony Bolgar** *January 13, 2016 11:43*

Hey Peter, I decided to taker your advice. I think you are right, Repetier would be a better firmware to modify. The code is so much cleaner and better wtitten and documented. Marlin is just way to fucked up in it's current state, and the motion algorithms for Repetier are much nicer. Do you want to tackle a repetier mod with me?


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/TNCS1gCFpEU) &mdash; content and formatting may not be reliable*
