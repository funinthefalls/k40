---
layout: post
title: "So I have been following for a while and there is a ton of information and knowledge here"
date: July 11, 2016 16:21
category: "Hardware and Laser settings"
author: "J R III"
---
So I have been following for a while and there is a ton of information and knowledge here.  My friend and I decided to pool our resources together and go a 60W red box ebay cutter.



[http://www.ebay.com/itm/60w-CO2-USB-Laser-Engraver-Laser-Cutting-Engraving-Machine-w-Air-exhaust-Fan-/201451548855?hash=item2ee772b0b7](http://www.ebay.com/itm/60w-CO2-USB-Laser-Engraver-Laser-Cutting-Engraving-Machine-w-Air-exhaust-Fan-/201451548855?hash=item2ee772b0b7)



we are having problems powering it up and would like to know if there is any one out there that has any ideas. of course trying to email the Chinese is already happening.



it sais 220/110 when plugged into 110 us  it did not initially turn on.

found a switch on the power supply set to 220 flipped to 110 now the cabinet light lights up and the laser pointer points. but when turning on the actual machine it sais connecting then reboots  after a couple of seconds sounds like it is trying to move a servo maybe.  I checked the z goes up and down fine with the switches and I can move the xy manually with only expected drag.



Any one have similar machine or ideas or even an other user group to contact. I gladly seek your advice.



Jack 





**"J R III"**

---
---
**Alex Krause** *July 11, 2016 18:25*

Check all your electrical connections to the controller? What controller does it have? 


---
**Øystein Krog** *July 12, 2016 13:55*

Maybe you damaged it when you plugged it in with the PSU set incorrectly? 


---
**Bill Parker** *July 13, 2016 08:03*

I have asked a few sellers on ebay is the stand removable to get it through a standard doorway and then I can put it back on and if so what is the size without stand but all I get back is I can do you good price. Could you tell me please.


---
**J R III** *July 14, 2016 15:17*

We have a 36" door and took it off the hing and rolled it in. Yes the base can come off but taking the door off was easy for us as we had done that for other machinery.


---
*Imported from [Google+](https://plus.google.com/110212014251342843606/posts/5vRy5dqswf8) &mdash; content and formatting may not be reliable*
