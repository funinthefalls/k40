---
layout: post
title: "I started some work on improving the K40 optical path"
date: November 12, 2018 17:37
category: "Modification"
author: "Don Kleinschnitz Jr."
---
I started some work on improving the K40 optical path. 



Here are the inaugural posts. I have not yet completed the final testing to prove it all works. 



[https://donsthings.blogspot.com/2018/11/improving-k40-optics-mirror-1.html](https://donsthings.blogspot.com/2018/11/improving-k40-optics-mirror-1.html)



[https://donsthings.blogspot.com/2018/11/improving-k40-optics-mirror-2.html](https://donsthings.blogspot.com/2018/11/improving-k40-optics-mirror-2.html)



[https://donsthings.blogspot.com/2018/11/k40-optical-alignment-tools.html](https://donsthings.blogspot.com/2018/11/k40-optical-alignment-tools.html)



Let me know what you think.



If there is a  lot of interest I will refine the drawings and laser cut (vs hand fabrication) a set of parts and then publish final drawings and instructions.



Otherwise I will stick with my custom parts.





**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/U7QAkov7V6M) &mdash; content and formatting may not be reliable*
