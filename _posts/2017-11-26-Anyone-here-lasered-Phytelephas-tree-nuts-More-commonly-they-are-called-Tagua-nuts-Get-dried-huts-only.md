---
layout: post
title: "Anyone here lasered Phytelephas tree nuts? More commonly they are called Tagua nuts (Get dried huts only)"
date: November 26, 2017 00:31
category: "Materials and settings"
author: "Steve Clark"
---
Anyone here lasered  Phytelephas tree nuts? More commonly they are called Tagua nuts (Get dried huts only). They look like ivory and are sized up to 1 3/4 to max of 2 inches in size. 

They were used for making carvings, buttons, ect  in the old days as a cheaper replacement for ivory and laser very nicely once you figure  it out. I blue tape them to keep them clean and cut through at around 650 mm and 8 to 10 mm feed for up to .125 inches thick. I made a little fixture so I can band saw slices and sand the surfaces parallel.  



BTW most have a little void in the center. Lots of video on the web about them.  I’m experimenting with using them as inlays in other wood and jewelry or by themselves as standalone jewelry. 







**"Steve Clark"**

---
---
**Don Kleinschnitz Jr.** *November 26, 2017 15:33*

Picture? :)


---
**Ned Hill** *November 26, 2017 15:56*

That's cool.   Gives me some ideas :)  I second Don's request, would love to see how it lasers. Thanks for sharing.


---
**Steve Clark** *November 26, 2017 17:56*

Here is a picture of a test piece. That's all I have left here right now. It is the first square test piece I cut out. the size is small about 3 mm thick x 18 mm. The edges clean up fast with 220 sand paper as the kerf burn is not very deep. 

![images/d929b8db211e9e5364a7eead030762ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d929b8db211e9e5364a7eead030762ce.jpeg)


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/2NXowpyN2vH) &mdash; content and formatting may not be reliable*
