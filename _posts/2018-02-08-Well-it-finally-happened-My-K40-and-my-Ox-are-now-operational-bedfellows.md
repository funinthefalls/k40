---
layout: post
title: "Well it finally happened...... My K40 and my Ox are now operational bedfellows"
date: February 08, 2018 20:42
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Well it finally happened......

My K40 and my Ox are now operational bedfellows.

I got them both fully operational and moved to the garage including a quick disconnect (no screws) for the K40's discharge hose :).

They are both now mobile so when there is good weather I can open the garage and move them to more accessible positions. I was surprised how much room I had even when they are parked.



Now to decide on something stellar to make with them ....







![images/a029ca443b42805ba91f8aaae06830aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a029ca443b42805ba91f8aaae06830aa.jpeg)
![images/90910156f14d72d22bd728496ae2f7fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90910156f14d72d22bd728496ae2f7fc.jpeg)
![images/3ca40fc3da14c1dfdc45f9f70f77a671.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ca40fc3da14c1dfdc45f9f70f77a671.jpeg)
![images/e8c09389685d5cf86284e6844682d823.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8c09389685d5cf86284e6844682d823.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Steve Clark** *February 08, 2018 21:46*

I bet the wife is happy!


---
**Don Kleinschnitz Jr.** *February 08, 2018 22:01*

**+Steve Clark** She is glad mostly that I haven't had anything arrive from amazon for weeks   .....


---
**James Rivera** *February 09, 2018 20:22*

I have my K40 and my Ox in my garage, too. Not really mobile though. I have my K40 next to the window so I can put the vent there when it is in use. My Ox is on a 4x8 piece of crappy fiberboard on top of a folding table. I've been wanting to build a dedicated table for it from some 2x4s I have lying around, not unlike what you have. I was thinking of putting casters on it, but they are expensive. Idea just popped into my head looking at your: 3d print some feet to make it a little easier to move, but still stationary most of the time. I like the idea of a cart for the K40 though. I might copy that idea. 


---
**Don Kleinschnitz Jr.** *February 09, 2018 23:19*

**+James Rivera** I used HF casters and the table has built in jack's. I can share the drawings if you want.


---
**James Rivera** *February 09, 2018 23:35*

**+Don Kleinschnitz** If you already have them, yes I would like to see them, please. Don't go to the bother of drawing them just for me though.


---
**Don Kleinschnitz Jr.** *February 09, 2018 23:58*

**+James Rivera** I have them will post by tomorrow. 


---
**Don Kleinschnitz Jr.** *February 10, 2018 00:47*

**+James Rivera** check this out....

Be careful with the plans as sometimes I make mods on the fly and dont go back and make corrections :(.





[donscncthings.blogspot.com - Building a Table for the Ox](http://donscncthings.blogspot.com/2017/05/building-table-for-ox.html)


---
**James Rivera** *February 10, 2018 02:08*

**+Don Kleinschnitz** Cool! I'm an Excel nerd. I'm curious about how you setup your cut list spreadsheet. Do you still have it?




---
**Don Kleinschnitz Jr.** *February 10, 2018 04:30*

**+James Rivera** I will link you to it tomorrow....


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/YnXwKPB35tQ) &mdash; content and formatting may not be reliable*
