---
layout: post
title: "Not sure if many others here cut leather, but I've noticed on this piece a shrink in the leather of ~1.1.4% (or 1-2 mm in size)"
date: December 14, 2015 05:16
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Not sure if many others here cut leather, but I've noticed on this piece a shrink in the leather of ~1.1.4% (or 1-2 mm in size). I wonder whether anyone knows if the laser cuts on the outside of a shape, or half-way through the edge.

So, e.g. I have a 100mm square. Does it cut on the outside & give me a perfect 100mm square result. Or does it cut 1/2 way through the edge & give me a ~99mm square result?



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Well somehow, not totally sure how, the leather shrunk a tad. Somewhere between cutting it, dyeing it, lacquering it and stitching it.



It's strange. 1.2-1.4% shrink. Was supposed to be 228mm long, is 226mm long. Was supposed to be 84mm high, is 83mm high. Could be possibly lost 1-2mm when lasering it.



The interior card slots were supposed to be 100mm wide, they are 99mm wide. 1% shrink.



Quite strange. Will have to test cut a piece to see if it loses ~1% size. Has anyone else noticed this sort of thing?



![images/ef2b92671589d8d80b2b2d45f31032f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef2b92671589d8d80b2b2d45f31032f0.jpeg)
![images/46caba0359647d317fab74de76a04935.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/46caba0359647d317fab74de76a04935.jpeg)
![images/a4dc1f9e22141e6f68416c14b4bee7fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4dc1f9e22141e6f68416c14b4bee7fc.jpeg)
![images/ec52033aeb50935b1bd875e31eaf54b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec52033aeb50935b1bd875e31eaf54b7.jpeg)
![images/ff910f9a56470f1351b8573e2fa2c39b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff910f9a56470f1351b8573e2fa2c39b.jpeg)
![images/e94628f154d94ecce753a04607f78e8b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e94628f154d94ecce753a04607f78e8b.jpeg)
![images/83414d7187b04e0de59ae4399ba85663.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/83414d7187b04e0de59ae4399ba85663.jpeg)
![images/84135d9dbde272fe79dc27d9be290f8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/84135d9dbde272fe79dc27d9be290f8c.jpeg)
![images/5123674484bc7031bdb492217fd97e63.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5123674484bc7031bdb492217fd97e63.jpeg)
![images/bcbd39c2f3820ed06a924c89f11d5bf0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bcbd39c2f3820ed06a924c89f11d5bf0.jpeg)
![images/40f3a6f62006b88d83707420c580fa10.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40f3a6f62006b88d83707420c580fa10.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Coherent** *December 14, 2015 14:47*

It will cut on your line. anything that cuts has a kerf (width of the cut). Cut a test sample and measure the width of the kerf. Different materials may have a different kerf when cut simply due to the burning or melting properties of the material. Now move the line you're cutting on your object 1/2 of your measured kerf  For example if your kerf was 1mm you'd want to cut .5m outside the original line. Don't forget on an object like a rectangle you want to cut 1/2 the kerf distance on both sides of every edge, so with a 1mm kerf on a 50x60mm rectangle the corrected sized would be 51x61mm  to get a correctly sized cut object of 50x60.  Some CAM software allows you to input the kerf or tool width (like different diameter router bits) and automatically adjusts the cut or gcode file accordingly. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 14, 2015 14:57*

**+Coherent** Thanks very much Coherent. That makes a lot of sense. Will test that out & take it into consideration for future.


---
**ChiRag Chaudhari** *December 15, 2015 18:38*

**+Coherent** Great explanation. When I make boxes out of 3 mm ply, using slot and grove design, I add 0.2mm on both sides of grove so that they fit perfectly into the slot. Took me a few cuts to figure out exact dimension, but yeah as the laser beam has also some tiny width and it also melt/burn the material you are working on. 


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/JUYzpWQWom1) &mdash; content and formatting may not be reliable*
