---
layout: post
title: "Check out this awesome Instructable"
date: August 28, 2016 22:45
category: "External links&#x3a; Blog, forum, etc"
author: "Ariel Yahni (UniKpty)"
---
Check out this awesome Instructable. 





[http://www.instructables.com/id/10-Tips-and-Tricks-for-Laser-Engraving-and-Cutting](http://www.instructables.com/id/10-Tips-and-Tricks-for-Laser-Engraving-and-Cutting)

![images/bece49a9d8776f33214bde5e896a1a09.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bece49a9d8776f33214bde5e896a1a09.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *August 28, 2016 23:44*

Some good tips.



For masking I use blue or green painters tape rather than the general purpose masking tape he shows. Painters tape is lower tack and the glue tends not to burn. Easier to peel off too. Also available in some pretty wide widths.



For the trick he showed where he was engraving multiple items at a time using targets on cardboard I usually cut the shape into it (or use the sheet of material I was cutting out of). That way the piece will drop down and won't move around as the head goes back & forth and the air assist won't blow them around. I only do this with the K40 because the software won't do both cuts & engraves in the same run like the Red Sail does. But the same trick is good for doing the back sides of things after doing the front.


---
**Jonathan Davis (Leo Lion)** *August 29, 2016 00:29*

Am a instructables user 


---
**greg greene** *August 29, 2016 00:57*

A lot of malicious software in your 10 tips :(


---
**Joe Spanier** *August 29, 2016 01:59*

I like paper vinyl transfer tape for the masking. 


---
**Jeff** *August 29, 2016 05:38*

I've been using a roll of brown paper mask from the paint department of Home Depot.  I cut the size I need to cover my material then spray a thin coat of Krylon Easy Tack Repositionable Adhesive Spray from Michael's and apply it to my material.  You can use a brayer to get all the wrinkles out and make sure it's making good contact.  It cuts down on smoke damage and can tolerate light painting afterwards.  It's much safer than using any vinyl products, for your and your machine.﻿


---
**Joe Spanier** *August 29, 2016 12:46*

I dont use vinyl. I use the paper masking that lets you apply the stickers to things. Its just super wide masking tape.


---
**Jeff** *August 30, 2016 18:18*

Joe, good to know, when you said Paper Vinyl Transfer Tape it sounded like you meant the actual Vinyl Transfer Tape for Vinyl Cutters.  The paper tape is good stuff.  I also use Ikonics Green Laser Tape when I need to sandblast after laser etching on the laser for extra fine detail.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/WGDo27hn5w9) &mdash; content and formatting may not be reliable*
