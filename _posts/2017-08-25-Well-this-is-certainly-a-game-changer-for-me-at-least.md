---
layout: post
title: "Well, this is certainly a game changer (for me, at least):"
date: August 25, 2017 20:17
category: "Software"
author: "Bob Buechler"
---
Well, this is certainly a game changer (for me, at least):





**"Bob Buechler"**

---
---
**Randy Randleson** *August 25, 2017 20:42*

That is interesting, I wonder how good it is?


---
**Stephane Buisson** *August 26, 2017 06:15*

not as good as the 123d app, as some features as disappeared at the same  time. like nesting of the components .

but i am happy the main functionality is still alive .


---
**Gunnar Stefansson** *August 26, 2017 18:21*

wow. Now I definitely need to learn it to try it... Thanks for sharing 


---
**Alex Krause** *August 26, 2017 19:10*

I will miss 123dMake


---
**Michael Audette** *August 28, 2017 18:01*

Nice to see them pull in their other tools into F360.  That was a nifty stand-alone before.


---
**Bob Buechler** *August 30, 2017 23:08*

BTW: Apparently this Sketchup script does a decent job too, in case you're not a heavy F360 user:  [sketchupplugins.com - Slicer5 &#x7c; SketchUp Plugin Index](http://sketchupplugins.com/plugins/slicer5/)


---
**Gunnar Stefansson** *August 31, 2017 07:04*

**+Bob Buechler** Thank you alot, that is now bookmarked and needs a test ASAP!! Awesome




---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/9rcRB6ghS5y) &mdash; content and formatting may not be reliable*
