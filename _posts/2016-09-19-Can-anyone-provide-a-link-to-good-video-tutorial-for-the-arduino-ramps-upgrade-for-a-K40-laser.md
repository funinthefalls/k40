---
layout: post
title: "Can anyone provide a link to good video tutorial for the arduino ramps upgrade for a K40 laser?"
date: September 19, 2016 14:11
category: "Modification"
author: "Ben Crawford"
---
Can anyone provide a link to good video tutorial for the arduino ramps upgrade for a K40 laser?





**"Ben Crawford"**

---
---
**Ben Crawford** *September 19, 2016 14:56*

I already have the ramps parts, not going to buy another board.


---
**HalfNormal** *September 19, 2016 15:05*

**+Ben Crawford** Here are a couple of great links to get you started.

[https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion](https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion)



[http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/256422-cnc-3.html](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/256422-cnc-3.html)



I would suggest using this firmware



[https://git.nexlab.net/machinery/MarlinKimbra/tree/k40_noflow_nocooler](https://git.nexlab.net/machinery/MarlinKimbra/tree/k40_noflow_nocooler)



I do not know of any videos. You will need to be patient and persistent. Make sure you read up on how to set the current with the drivers so you do not over/under drive the steppers.


---
**Randy Draves** *September 19, 2016 17:01*

**+Peter van der Walt** what is the big advantage with smoothie over ramps?  It's over double the cost.  I'm still on my original board and don't mind spending the money if it's justified.


---
**Randy Draves** *September 19, 2016 17:09*

**+Peter van der Walt** Thanks. I'll check it out


---
**HalfNormal** *September 19, 2016 17:11*

**+Randy Draves**​ you can run Laserweb3 and have a combination that can't be beat!


---
**Randy Draves** *September 19, 2016 17:25*

**+HalfNormal** I thought you could run laserweb with Marlin?


---
**HalfNormal** *September 19, 2016 19:25*

**+Randy Draves** Yes you can run Laserweb3 on Marlin. The firmware I linked to is one that has been further optimized by another coder for use with the K40. It is a branch of the Turnkey Tyranny  code.

Ahh, I see you are referring to my earlier post. Laserweb3 really shines with a smoothie board. A Ramps cannot take advantage of all Laserweb3 has to offer but it is a great way to begin the road of upgrades and modifications on a budget. 


---
**Roberto Fernandez** *September 19, 2016 21:03*

**+Randy Draves**​ I use this firmware: [github.com - Downunder35m/MarlinKimbra-K40](https://github.com/Downunder35m/MarlinKimbra-K40) with laserweb3.﻿


---
*Imported from [Google+](https://plus.google.com/110714520603831442756/posts/B2yrSw2JdJ5) &mdash; content and formatting may not be reliable*
