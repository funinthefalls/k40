---
layout: post
title: "When I do a test line it's nice and thin, when I'm cutting something out the line is a lot thicker"
date: September 14, 2016 12:47
category: "Discussion"
author: "Robert Selvey"
---
When I do a test line it's nice and thin, when I'm cutting something out the line is a lot thicker. Is that due to using a higher power setting ?





**"Robert Selvey"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 14, 2016 13:04*

Yeah, higher power will give a slightly thicker beam. I tend to use 4mA or 6mA power & multiple passes to minimise the thickness of the cut.


---
**greg greene** *September 14, 2016 14:00*

Make sure your at optimum focus distance - even though it supposed to be 50.8 mm - not all lens are cut the exact same


---
**Robert Selvey** *September 14, 2016 21:43*

I set the focus by doing the slanted wood trick.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/Mx65YotZcoL) &mdash; content and formatting may not be reliable*
