---
layout: post
title: "K40 Lightening Strike BTW this show brought to you through your kind donations to the K40HV lab No way should you try this at home ......"
date: July 20, 2017 17:49
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40LPS



<b>K40 Lightening Strike</b>



<i>BTW this show brought to you through your kind donations to the K40HV lab</i>



<b>No way should you try this at home ......</b>



In case you didn't believe me about the energy in our laser systems take a look at this lightening strike.... OMG.



This is the first time I had access to a spare and working supply thanks to Jeff Johnson so I decided to put on a lightening show.....



<b>Notice:</b>

... that I have two dead man switches to insure that my hands are away from the bad stuff ....

... the arc when I use the chicken stick when the LPS is disabled. 



<b>Observations:</b>

This was a marginal supply, do you mean it gets stronger than this?



This supply sometimes seem to output lower than normal voltage with a lower energy spark. Maybe that is what is wrong with it. I also noticed that it was more consistent with a longer gap?



A pipe dream was to use an air gap as a laser tube proxy ....NOT! The good news is to work on these I do not need to have the HVT installed. 



The arc from this LPS can easily jump more than 2.5 inches, didn't try further than that. This was a lot larger than my calculations suggested.



When I go to large distances (open circuit) there is arcing  on the primary side, around the secondary wind-ings and sometimes through the anode wire's insulation if it is closer to ground than the gap. Looks like a Christmas tree.



I have  wondered if an open ANODE circuit (bad tube) would promote arching and it seems it does. This may mean that when a tube is dead the LPS may arc. It also could be what is wrong with this supply i.e leaky HVT.



The amount of current drawn even in a 2.5" arc is way over 30ma, see the meter. This is why letting this supply arc is not good for it. I accidentally bought a 30A meter instead of the right one, guess I will hook it up.



Clearly and as I expected this supply is not really current limited. 



I flinch every time I fire this thing. My wife could smell the corona all the way down stairs. She thought I had eaten something foul.... and this was a new form of flagellation :)!



If this doesn't cause you to add an interlock into the laser compartment .... 



More lightening to come as I get more curious..... and I'm going to use the rubber gloves from now on.



Edit: adding another lightening video:

[https://photos.app.goo.gl/hHlQcVBssgKPNHz23](https://photos.app.goo.gl/hHlQcVBssgKPNHz23)



Try going to setting in your video player and slow these down to the lowest speed, .25.










**Video content missing for image https://lh3.googleusercontent.com/-kMk6P0jHW3c/WXDtKVM88vI/AAAAAAAAqNs/lTb5gJfmNZs9b5nWizwD7Neu91Xl2m5YQCJoC/s0/20170720_100824.mp4**
![images/a6101033b4a192f63e34b797b6f08fef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a6101033b4a192f63e34b797b6f08fef.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-sUv3Kjk0-kA/WXDtKSQy2iI/AAAAAAAAqNs/yV42ihZ4_60R28ozE0v6A6ToSSeFEUHSACJoC/s0/20170720_101037.mp4**
![images/7250d7e5b591fcec4cc579a83179380d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7250d7e5b591fcec4cc579a83179380d.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Isa Nasser** *July 20, 2017 18:59*

Thanks for sharing , it really put things into prospective when it comes to how dangerous these machines are .






---
**Steve Clark** *July 21, 2017 00:38*

Errr...REAL THICK rubber gloves...


---
**Don Kleinschnitz Jr.** *July 21, 2017 03:11*

![images/39ed1e9d44061a99f5b551ba848d6f34.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/39ed1e9d44061a99f5b551ba848d6f34.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/725RV2iKdsU) &mdash; content and formatting may not be reliable*
