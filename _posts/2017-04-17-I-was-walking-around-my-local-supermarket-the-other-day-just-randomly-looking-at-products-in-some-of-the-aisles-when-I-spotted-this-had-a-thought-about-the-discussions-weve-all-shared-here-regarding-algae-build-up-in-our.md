---
layout: post
title: "I was walking around my local supermarket the other day & just randomly looking at products in some of the aisles when I spotted this & had a thought about the discussions we've all shared here regarding algae build up in our"
date: April 17, 2017 14:32
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
I was walking around my local supermarket the other day & just randomly looking at products in some of the aisles when I spotted this & had a thought about the discussions we've all shared here regarding algae build up in our coolants.



It got me to thinking about the Oligodynamic effect ([https://en.wikipedia.org/wiki/Oligodynamic_effect](https://en.wikipedia.org/wiki/Oligodynamic_effect)), where certain metals have a biocidal effect. According to this article (& the bottle of algae killer) Copper seems to be a substance which kills algae (specifically copper sulfate according to the article, see: [https://en.wikipedia.org/wiki/Oligodynamic_effect#Copper](https://en.wikipedia.org/wiki/Oligodynamic_effect#Copper)).



What I'm wondering is could we place some copper pipe into our coolant reservoirs to prevent the buildup of algae? Secondly, if so, does this increase/decrease the conductivity of the water?



I'm looking to **+Don Kleinschnitz** & **+HP Persson** since you've both done extensive tests on this matter.

![images/6f9ee16ebe859c049ef9c29ca3b35385.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6f9ee16ebe859c049ef9c29ca3b35385.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**HP Persson** *April 17, 2017 14:36*

First thing i see on the bottle is the copper added, not something you want in a coolant. Does it say 30g per litre ?

Probably don´t need the whole bottle, and diluted with distilled it depends on the conductivity you get if it makes any problem or not :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 17, 2017 14:42*

**+HP Persson** I was thinking more along the lines of placing a piece of copper pipe in the liquid just for the biocidal effect. I have no idea the electrical concerns however, so was just curious if it's a viable idea.


---
**Ned Hill** *April 17, 2017 15:00*

Yeah stay away from the copper based algaecide.  I appreciate the thinking on  the pipe, but I"m not sure it would really be a good idea either.  **+Don Kleinschnitz** could always setup an experiment to see how much leaching occurs by monitoring the conductivity. 


---
**Cesar Tolentino** *April 17, 2017 15:12*

Are we talking about algae on the container? If yes, can we just cover it with blanket or something to block the light? I have a 5 gallon bucket  and my whole laser system is by the window, but I don't have algae issues?  Maybe because of the bleached I put in? Just a tablespoon per 5 gallons of water.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 17, 2017 15:28*

**+Ned Hill** Thanks for the advice to stay away from it. Would be interesting to see if there are issues via an experiment, but I'll take yours & HP's advice regarding it.



**+Cesar Tolentino** Yeah I was referring to algae buildup in the reservoir, on the pump, etc. I think the blackout to block the light is a viable option, as my K40 & the reservoir are in the garage (almost 0 light gets in there, except artificial light) & I just use regular rainwater with no additives and it takes a very long time before I see any sort of algae buildup in mine. Need to switch to distilled water though after seeing the results from conductivity tests run by HP & Don.


---
**HP Persson** *April 18, 2017 08:42*

Can confirm the covering of the tank too, and keep it out of light. Keeping the return hose below water level also helps a bit. And keep a lid on the tank to prevent any spores or similar to get into it.


---
**Adrian Godwin** *April 18, 2017 22:32*

Funny story : 



The laser cutter at our hackspace had a problem with the cooling system - the flow detection switch was stuck. Worse still, it was stuck 'on' so it detected flow even when there wasn't any (it was a simple weighted switch, not an impeller).



On taking it apart, we discovered half a dead bumble bee in the switch.



Cover the tank !

 


---
**Claudio Prezzi** *May 25, 2017 16:46*

What about adding a UVC light to the tank? That should also reduce building algae.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2017 23:07*

**+Claudio Prezzi** Is that like what they use for fishtanks to kill the algae? I've seen them in the past & thought something like that might be handy.


---
**Claudio Prezzi** *May 26, 2017 06:19*

Yes, that was what I meant.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/iFyPY1yfqeN) &mdash; content and formatting may not be reliable*
