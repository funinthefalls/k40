---
layout: post
title: "We get a lot of repeat posts requesting help for LPS problems"
date: May 20, 2018 13:16
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
We get a lot of repeat posts requesting help for LPS problems. I judge that LPS and optical alignment are a large portion of the K40 problems once conversions are done.



I have been tinkering with ideas for troubleshooting and repair instructions to help eliminate repeat threads and providing self help.



Attached is a link to such a prototype.





[https://drive.google.com/file/d/1zfU4DWZcEo7YxYvh_9z2pDQ0S1UXHLno/view?usp=sharing](https://drive.google.com/file/d/1zfU4DWZcEo7YxYvh_9z2pDQ0S1UXHLno/view?usp=sharing)



........

See the attached Poll (my first try at polls).



WOULD YOU USE THIS DOCUMENT?

(select your answer(s) below)





**"Don Kleinschnitz Jr."**

---
---
**Travis Sawyer** *May 20, 2018 13:37*

This is great! Thankfully I haven't needed it (yet, knock knock)


---
**HalfNormal** *May 20, 2018 14:36*

The only problem I see initially is that some people will say that is not my control panel. You may want to add the digital one as well.


---
**Don Kleinschnitz Jr.** *May 20, 2018 16:02*

**+HalfNormal** yes, I need to add that and also alternate supply configurations. Quite a bit more work :( that's why I wanted to check if its valuable to the community.


---
**Ned Hill** *May 21, 2018 19:04*

Wow Don  that's fantastic!  Thank you for doing so much work, I'm sure it will help a lot of people.


---
**James poulton** *May 21, 2018 23:20*

Very helpful indeed, cheers Don.




---
**syknarf** *May 23, 2018 16:29*

Great article!!


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/7UgRdkY1kwG) &mdash; content and formatting may not be reliable*
