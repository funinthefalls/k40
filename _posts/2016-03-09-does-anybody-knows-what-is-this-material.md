---
layout: post
title: "does anybody knows what is this material ?"
date: March 09, 2016 18:36
category: "Materials and settings"
author: "Stephane Buisson"
---
does anybody knows what is this material ?

I do like the white finish.

![images/9dfa87a7d66822f4fc6010e29e984654.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9dfa87a7d66822f4fc6010e29e984654.jpeg)



**"Stephane Buisson"**

---
---
**Gee Willikers** *March 09, 2016 18:47*

Melamine maybe?


---
**Joe Keneally** *March 09, 2016 19:07*

Looks like Melamine to me too


---
**Joe Keneally** *March 09, 2016 19:20*

I believe you.  I am also happy to hear that melamine is HARD if not impossible to cut as I have been trying for a while and considering myself a failure!  :)  Thanks for sharing!!!


---
**William Steele** *March 09, 2016 20:15*

Melamine covered MDF can easily be cut with a laser... SeeMeCNC ships all their printers out of it... works perfect.


---
**Joe Keneally** *March 09, 2016 20:17*

I have the thin black/grey chipboard material.


---
**Ashley M. Kirchner [Norym]** *March 09, 2016 22:20*

Or just cut regular 1/8" birch wood on a laser and paint it whatever color you want. :)


---
**Gee Willikers** *March 09, 2016 22:38*

Noted.


---
**David Cook** *March 10, 2016 04:29*

Looks like melamine covered MDF. ... Yes seemecnc uses a lot of it


---
**David Cook** *March 10, 2016 06:04*

Just a quote I ripped from Seemecnc website "...The wood kit's laser cut framework uses a new material introduced to 3D printers by us, melamine laminate. It's easier to work with, and more durable than other laser cut plywoods that are popular. .."


---
**David Cook** *March 10, 2016 21:08*

**+Stephane Buisson** the description in the build file for that just simply says " MDF with white coating"  [http://www.gaudi.ch/GaudiLabs/wp-content/uploads/HackStageMicroscope.pdf](http://www.gaudi.ch/GaudiLabs/wp-content/uploads/HackStageMicroscope.pdf)



[http://www.gaudi.ch/GaudiLabs/?page_id=328](http://www.gaudi.ch/GaudiLabs/?page_id=328)



My firend has a Rostock Max 3D delta printer and it is MDF with a nice white Melamine layer on each side.  looks the same as the Vinyl sheet material that **+Peter van der Walt** posted  :-)   I'm not sure we would want to laser cut PVC material though if possible.  




---
**Vince Lee** *March 13, 2016 02:58*

Looks like it might be similar to this, gonna get some myself to play with: [http://www.laserbits.com/wood-sheets/duro-wood-laminates/wus-113-duro-laser-wood-white-1-4.html](http://www.laserbits.com/wood-sheets/duro-wood-laminates/wus-113-duro-laser-wood-white-1-4.html)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/XhTVFAEwUhh) &mdash; content and formatting may not be reliable*
