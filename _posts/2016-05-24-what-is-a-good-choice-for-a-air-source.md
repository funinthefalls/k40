---
layout: post
title: "what is a good choice for a air source"
date: May 24, 2016 01:09
category: "Discussion"
author: "Jeff Kwait"
---
what is a good choice for a air source.  Air brush compressor, aquarium pump of perhaps one of those cosmetic air brush systems





**"Jeff Kwait"**

---
---
**Derek Schuetz** *May 24, 2016 01:29*

I'm using an air brush compressor and it puts out a pretty strong pulse of air and mad it where I couldn't cut wood about 2.5 to now up to 20 with no flames 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 24, 2016 03:59*

I'm using an air-pump (aquarium style) from a Tanning airbrush system (simply because I had multiple of them). It's not very strong, so I'm going to be using 2 as input for my new air-assist head. Hopefully that will be more powerful. I'd say anything works, provided the airflow/pressure is enough at the cut/engrave spot.


---
**Phillip Conroy** *May 24, 2016 06:15*

It all depends on what u are using laser for-if engraving anuy air pump will do-if cutting 3mm mdf i am using a workshop size pump/tank with air regulator so that i can vary pressure - i use 10 psi many as it keeps the mdf very clean espesialy on the back


---
*Imported from [Google+](https://plus.google.com/114834788519034865166/posts/3w69cAT1FEe) &mdash; content and formatting may not be reliable*
