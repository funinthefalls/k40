---
layout: post
title: "OK, newb her and have not received my K40 yet, but, I have a question"
date: June 20, 2016 19:31
category: "Software"
author: "Terry Taylor"
---
OK, newb her and have not received my K40 yet, but, I have a question. Suppose I have a project in Corel Draw where SOME of the graphic should engrave (let;s say on thin plywood) and some of it should cut (like the outline). Is there a way to do that in a single srawing?





**"Terry Taylor"**

---
---
**Evan Fosmark** *June 20, 2016 19:53*

Whatever you have set to print is what will get processed. Next to the layer name you'll find an ability to toggle printing for that layer. How I've been handling it has having "cut" layers and "engrave" layers, and then toggling the appropriate ones to print.


---
**Terry Taylor** *June 20, 2016 20:04*

I get that but (not having seen the software and process yet) how do you tell the K40 to engrave vs cut?


---
**Evan Fosmark** *June 20, 2016 20:46*

There's a plugin called CorelLaser that comes with the K40. When you install it, there'll be separate options for cutting vs engraving.


---
**Jim Hatch** *June 20, 2016 22:13*

But you can't do both in the second "print" job. You have to send the layer to engraved first, then when that's done (don't move anything) you send the layer to cut.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 20, 2016 22:39*

As Evan & Jim say, you can send both jobs separately via different options in the CorelLaser plugin. Unfortunately there is no ability to do both at the exact same time, however you probably wouldn't really want to anyway as if you cut a piece & it moves then the engrave could be ruined. So engrave first, then cut prevents pieces moving before they need to & allows your engrave to come out where it is supposed to.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 20, 2016 22:43*

Also, if you want to check out the software I have a link to it hosted from my dropbox. You can take a look around at least & familiarise yourself with it before your K40 arrives.



[https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0](https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0)



If you've already got CorelDraw then you only really need to download the CorelLaser 2013.02.rar & install it. If for whatever reason it doesn't work with whatever version of CorelDraw you are using then you may need the CorelDraw x5 with Keygen.rar also.


---
**Terry Taylor** *June 21, 2016 03:42*

I have Corel Draw x7, so I should be OK, thanks!


---
**Ben Marshall** *June 21, 2016 04:00*

There is a "how to" floating around on processing both at the same time using the "add task". I've not tried it myself. I create two separate layers, one for engrave and cut. When I engrave, I turn off the cut layer (little print icon next to layer in object manager, also the eye icon so it doesn't show up in print preview)and print. The vice-versa for cutting. You won't have to move your work b/w functions. ﻿



If you are doing both, 9/10 times you'll want to engrave first and then cut. If you are using material with glue such as plywood, the resin from the cut pieces will re-harden and stick back together if you wait to long. Best to grab it straight of the grill and start tapping out your cuts, especially with small detailed cuts.﻿



Edit: just reread Evans post, didn't mean to double tap the solution!🤓


---
**Ben Marshall** *June 21, 2016 04:12*

**+Terry Taylor** it's hard to understand it now without the program in front of you. Have some cardboard ready, as it's the least expensive way to practice. Take your time and find a good method to process your work to get the hang of it. There are LOTS of helpful people in this community,  don't hesitate to reach out for help...we've all been there.


---
**Terry Taylor** *June 24, 2016 15:10*

Received my K40 on Wednesday, probably won't get a chance to play with it until Sunday or Monday, but is their a good tutorial someplace on aligning the mirrors (as that seems to be an issue with these, as shipped).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 15:48*

**+Terry Taylor** There are heaps of discussions in this group with links to aligning the mirrors. I'll see if I can find some of the recent ones & post back.



edit: check this post ([https://plus.google.com/101703286870399011779/posts/1SKmZs2TR2v](https://plus.google.com/101703286870399011779/posts/1SKmZs2TR2v)) & check Ned & Brandon's comments for links to alignment guides.


---
**Terry Taylor** *June 24, 2016 16:38*

Thanks, Yuusuf, I found some others also. Hope to get to play with this thing soon. For anyone who is looking for a K40 at a bargain price, here is the one that I bought:



[http://www.ebay.com/itm/111215845517?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/111215845517?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



or:

TinyURL

[http://tinyurl.com/hr6q26v](http://tinyurl.com/hr6q26v)


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/bFgyHzCj5LJ) &mdash; content and formatting may not be reliable*
