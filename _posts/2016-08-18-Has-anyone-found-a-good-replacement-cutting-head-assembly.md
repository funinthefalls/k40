---
layout: post
title: "Has anyone found a good replacement cutting head assembly?"
date: August 18, 2016 02:45
category: "Hardware and Laser settings"
author: "ben crawford"
---
Has anyone found a good replacement cutting head assembly? Mine is Rickey and I don't trust it so I would love to replace it.





**"ben crawford"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 18, 2016 04:58*

If by cutting head assembly do you mean the laser head itself or the carriage that slides on the x-rail?



If the head itself a lot of people here have purchased a replacement head from Lightobjects.com & seem satisfied. I can't comment personally as I haven't swapped mine.


---
**ben crawford** *August 18, 2016 11:44*

It's the carriage I want to replace.


---
**HP Persson** *August 18, 2016 16:13*

Swap to linear guides and dump the wheels and build your own ;)

A linear guide can be picked up at aliexpress at $20 or lower.

Newer K40D machines have this kind instead of wheels that deform and dry out with time.




---
**ben crawford** *August 18, 2016 16:41*

Can you send me some links?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 18, 2016 21:03*

**+HP Persson** Is that something like this?

[http://www.aliexpress.com/item/Fast-Shipping-SBR10-L150mm-linear-rail-diameter-10mm-round-linear-guide-support-rails-for-cnc-parts/32375225839.html?spm=2114.40010308.4.9.0BpMbT](http://www.aliexpress.com/item/Fast-Shipping-SBR10-L150mm-linear-rail-diameter-10mm-round-linear-guide-support-rails-for-cnc-parts/32375225839.html?spm=2114.40010308.4.9.0BpMbT)


---
**HP Persson** *August 18, 2016 21:13*

This is better

[http://www.aliexpress.com/item/2pcs-linear-rail-HGR20-L1500mm-cnc-parts-and-4pcs-HGW20CA-linear-guide-rails-block-cnc-parts/32691481788.html?spm=2114.01010208.3.1.PvCTgA&ws_ab_test=searchweb201556_8,searchweb201602_3_10057_10056_10055_10037_301_10033_10059_10058_10032_10017_10060_10061_414_10062_413,searchweb201603_3&btsid=23c3a13e-bc62-41fd-80ec-283379d3db2f](http://www.aliexpress.com/item/2pcs-linear-rail-HGR20-L1500mm-cnc-parts-and-4pcs-HGW20CA-linear-guide-rails-block-cnc-parts/32691481788.html?spm=2114.01010208.3.1.PvCTgA&ws_ab_test=searchweb201556_8,searchweb201602_3_10057_10056_10055_10037_301_10033_10059_10058_10032_10017_10060_10061_414_10062_413,searchweb201603_3&btsid=23c3a13e-bc62-41fd-80ec-283379d3db2f)



Check out my pic on the link below, it has similar linear guides for the Y-axis.

[www.wopr.nu/laser/IMG_0356.JPG](http://www.wopr.nu/laser/IMG_0356.JPG)



The rail can be seen on the Y-gantry, and the "plate" is below the head-holder.

FlashLaser on Youtube has more movies of the K40D, check it out and snag some ideas how to stiffen up the Y :)




---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/PJ1MqvumcMF) &mdash; content and formatting may not be reliable*
