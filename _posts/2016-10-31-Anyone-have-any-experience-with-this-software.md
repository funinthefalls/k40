---
layout: post
title: "Anyone have any experience with this software?"
date: October 31, 2016 11:59
category: "Software"
author: "Bob Damato"
---
Anyone have any experience with this software? They advertise amazing results, but Im not sure its anything I cant get done with photoshop etc. However if its that much better, then Id consider it.

[http://www.laserbits.com/spk-017-photograv-software-package.html](http://www.laserbits.com/spk-017-photograv-software-package.html)





**"Bob Damato"**

---
---
**Jim Hatch** *October 31, 2016 12:15*

I've used it. It's essentially a "one-click" solution to prepping photos for engraving. It's nothing you can't do with Corel but it's quicker. There are several videos on YouTube showing how to do it using Corel so if you're comfortable with that process then PhotoGrav isn't necessary. If you want one-click prep then you can spend the $ to get the simplicity 😀


---
**Bob Damato** *October 31, 2016 12:28*

Interesting, thank you. I havent had a ton of success with corel, but Its likely my stupidity. I should probably read up on how to prepare images for engraving. Thanks Jim, much appreciated.

Bob


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 31, 2016 13:06*

No experience with this, but I'd hazard a guess it just dithers images?


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/eU8XfC7f8nZ) &mdash; content and formatting may not be reliable*
