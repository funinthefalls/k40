---
layout: post
title: "Hello guys, I've just started playing with my machine"
date: October 13, 2016 17:46
category: "Discussion"
author: "Jose Raul Espinoza"
---
Hello guys,

I've just started playing with my machine. Right away I noticed that the power to the laser "cuts off" as I play with the knob. It seems like there is a sweet spot right at the 10 mark. But anything less (5) or any more (15) will not cut. The laser makes a hissing sound but it doesn't burn. 

Also the settings on the knob do not coincide with the display on the meter. If I have 10 dialed the meter reads 5 mA. 



Any ideas to what the issue might be?

Thanks in advance. 





**"Jose Raul Espinoza"**

---
---
**Bill Keeter** *October 13, 2016 18:02*

Curious what people say. I spent the other night calibrating the mirrors on my new machine and have a similar problem. My machine fires way below were the dial icons even start. Plus I can only get power up to about 5 mA. If I turn the dial too much higher and the tube doesn't even fire. Weird. 


---
**Jose Raul Espinoza** *October 13, 2016 18:07*

**+Bill Keeter** correct. That's my situation. I have the dial a little after 10 around where 12 would be and my meter shows 5mA. It burns but doesn't seem powerful enough. Maybe I need to slow the machine way down. But is this behavior normal?


---
**greg greene** *October 13, 2016 18:26*

The quality of the pots on these machines - is to say the least - less than industrial - that's where I'd start my search for a solution


---
**Ariel Yahni (UniKpty)** *October 13, 2016 18:42*

**+Bill Keeter** **+Jose Raul Espinoza** i would contact your sellers if possible and claim those faults.


---
**Bill Keeter** *October 13, 2016 19:06*

I was just happy GlobalFreeShipping sent a new tube to replace the cracked one. But yeah, I'll do more testing tonight and then ask them about the mA. 


---
**Jose Raul Espinoza** *October 13, 2016 19:34*

**+greg greene** I will look into getting a new pot.


---
**Jose Raul Espinoza** *October 13, 2016 19:35*

I'm guessing you all agree this is not right? A working machine should have a wider range of power settings, correct?


---
**greg greene** *October 13, 2016 19:36*

Correct - Mine is 3 ma to 18 Ma


---
**Bill Keeter** *October 13, 2016 19:42*

Would it be worth connection a multimeter to either the pot or the dial. Just to see if the mA reading is accurate? 


---
**greg greene** *October 13, 2016 19:47*

Can't hurt - what you should check for on the pot is a smooth reading throughout it's range/  I'm not sure if it is a linear Pot or a logarithmic one.  I suspect probably linear given it's intended function.  If you see any jumps or loss of reading with one lead connected to the wiper and the other to one of the ends - then it is definitely the pot.


---
**Bill Keeter** *October 14, 2016 03:30*

**+Jose Raul Espinoza** **+greg greene** glad I checked. With the pot on "off" i'm getting 5V and it rolls down toward zero as I go clockwise on the pot. The issue is right around 3.8V the pot blanks out. Then it comes back as I turn it more. Which completely explains my issue when I was test firing during mirror alignment.



Jose, hope that's also you're issue.



Any suggestions on what replacement pot I should buy? 


---
**greg greene** *October 14, 2016 14:58*

First contact the seller for a new one - that one is defective - but I don't know the specs for the pot, if you can find that out then Mouser or Jameco or any other parts supply place should have a replacement - make sure you buy the best quality available.


---
**Scott Marshall** *October 15, 2016 07:21*

A wirewound pot is best. I reccommend a wirewound  1K pot, in either single turn or 10 turn with vernier if you like the fine control. The factory one are no good when new, so I wouldn't waste your time persuing that. The factory pot goes for about 30cents and isn't worth it.



Look to spend 5 to 10 bucks for a decent name brand single turn or about $20-30 for a decent name brand 10-turn with vernier. You CAN go much higher here, but you won't gain much) You can get Chinese pots much cheaper, but they are, in my experience, junk. This i one place where it's worth getting the real thing. Beware for counterfiets, especially of the brand name "Bourns" You'll see odd spellings of the name, they copy the font, the name, but not the guts. They have a easily recognizable trademark blue color, which was the 1st thing that was copied. It's almost worth buying an Ohmite just to NOT get a copy.



Pots have a delicate and hard to manufacture (well) resistance element, and the mechanicals matter as well, getting it right to produce a reliable noise free pot costs money. I've seen online videos of what's inside the copies, and it's amazing the crap they are selling.



Sorry, kind of got off the subject, I was burned bad on a large lot of counterfiet Voltage regulator chips a few months ago. I'm glad they didn't make it into a heart monitor or airbag control. 



The one rule when buying pots is:

If it's too good to be true it is.



PS,



I'll be stocking these in the next couple of weeks if you still need one then let me know.



Scott


---
**greg greene** *October 15, 2016 13:36*

Get one from Scott - even as a spare - he supplies good stuff.


---
**Jose Raul Espinoza** *October 16, 2016 00:41*

**+greg greene**, **+Bill Keeter**, **+Scott Marshall**,

Thank you all for the help. 

I have not had time to continue troubleshooting mine, but since Bill Keeter mentioned his was faulty, then I believe my might be aswell. I will have to buy a new one. The specs on mine say 1K ohms- 2W. 

Where can I see the ones you have for sale **+Scott Marshall**?

 


---
**Scott Marshall** *October 16, 2016 03:14*

**+Jose Raul Espinoza** 



I don't have a lot of my smaller items up on my site yet, but have in stock a lot of things like replacement PSUs etc. I need to photograph them and do write ups so my webmaster can get them posted.



I have options from simple to very high end. Even the "simple" kit is a 1000% improvement over factory. All have heavy wirewound elements (no carbon composite to absorb smoke and breakdown). All pots are fully sealed and guaranteed got 1million plus revolutions



The Single turn Option -  It's single turn with a large aluminum scaled  knob that gives great repeatibility. The Pot is a CTS and is 1k 5watt wirewound for rock solid settings and long life. 

it's $24.95 and is, (of course) Plug and Play



The midrange option - It's a 10 turn Wirewound Genuine Bourns 1K, 2W and comes with a Bourns H-516 Vernier Dial, 10 turn counting, 50 divisions per turn and includes a locking break. It's about .900" in diameter. Very high quality, very repeatable. You'll see about 40 Microamps per division output change. $39.95 Plug and Play



The Top of the line option - this is a NASA grade Vernier on the same fine Bourns 10 turn pot. The Bourns H-46 Vernier Dial is a turns counting dial with 100 divisions per turn and is a large 1.8" diameter super high quality device. You will see about 20microamps change per divisions on it's large lockable dial.

$69.95 Plug & Play



All prices plus shipping, should be 2-3 bucks in the conus, I charge only actual shipping.



These aren't economy devices, and if your're looking for a cheaper option I will be stocking a Chinese alternative, but I can't emphasize enough how much better these are, the feel is exquisite, and the performance better- they are repeatable to within a fraction of a percent (.25% linearity over 1million shaft revolutions is the Bourns Spec) and just plain blow away any 10 dollar ebay pot. In my opinion, the looks alone make these worth the difference. Even the Basic one makes the K40 look like a lab instrument, and the Big 1.8" vernier looks at home on your local nuclear power plant panel.



Plus you get ALL-TEK support. If not 100% satisfied, all your money back - even shipping.



Anyone interested, send me your email, and I'll send photos/spec sheets on the pots & verniers.

Scott594@aol.com


---
**Jose Raul Espinoza** *November 03, 2016 20:07*

So here's an update. I replaced the pot with another from a local store. Looks very similar to the original. Same ratings: 1 k ohms, 2 w. The issues persists, my amp meter is now acting up aswell. The slightest  movement on the pot will shoot the readings all the way to the max. All I did was replace the pot in the same way as the old one. 

I don't know what else. I'm thinking the failure wasn't the pot. 

I noticed that the psu makes a hizzing sound and the sound changes as I rotate the pot, it even stops sometimes and then comes back on. 

The laser light inside the tube also changes, it becomes less focused and kinda dissipates. 

I'm attaching some vids, hopefully someone has a clue of what is going on. 




{% include youtubePlayer.html id="qOf605J84Y8" %}
[youtube.com - November 3, 2016](https://youtu.be/qOf605J84Y8)


---
**Scott Marshall** *November 03, 2016 20:35*

That's not good. 



If you're really exceeding 30ma like it shows (and you probably are), the tube is most likely bad. About the only thing that will cause that to happen is arcing thru the tube, probably to the water and back out again.



It's conceivable that you have a path where the high voltage is getting past the tube and back into the meter circuit, but it's unlikely.



The meter is on the ground side of the tube, so current has to pass thru the tube to cause it to read. If you had an arc or leak to ground, it will bypass the meter and you will see falling readings instead of spiking. 



When you add that evidence to the statement you see a major change in tube discharge (white is Baaad), it's a very high probability the tube is shot.



Don't run it any more, because the supply may be damaged from the overload.



Sorry.



Scott





PS have a variety of pots now in stock from $10 plug and play Asian 10-turn with vernier to $70 for lab grade 10-turn mechanical digital 1000 count.


---
**Jose Raul Espinoza** *November 03, 2016 21:01*

Thank you for the reply **+Scott Marshall**​. Before I replaced the pot with the new one the meter was working ok or at least not as crazy as it is now. 

I don't know if I should risk replacing the tube altogether. The thing is. It still fires and burns, it's just not very strong.

I do remember hearing a few pops when I dialed the pot near the max. 






{% include youtubePlayer.html id="WGDkkJk0640" %}
[youtube.com - November 3, 2016](https://youtu.be/WGDkkJk0640)


---
**Jose Raul Espinoza** *November 03, 2016 22:07*


{% include youtubePlayer.html id="HX08aI9BFlk" %}
[youtube.com - November 3, 2016](https://youtu.be/HX08aI9BFlk)


---
**Scott Marshall** *November 03, 2016 23:12*

Continued use is risky from a safety standpoint and may kill your PSU adding another 100 bucks to the bill once it dies.



I wish I could think of something else it could be, but the only longshot I can think of is if the tube is arcing to the water tubing on BOTH ends of the tube. Bear in mind IF this is happening, your water is energized and a safety hazard. Seems unlikely it wouldn't ground out, but I've seen some pretty wild things happen in high voltage systems. Please be careful.



That would give the power a path thru (around actually) the tube and still carry it thru the ma meter. It's a simple loop circuit with the + power connected to the fully mirrored end of the tube (right from the front) and the ground comes off the emitting end and goes to the ma meter. The - side of the ma meter is connected to the leftmost wire connection on the PSU, which ties to earth and system ground.



One other 'way out there' but possible scenarios is if the ma meter is burned out (open), the high voltage may be arcing across the coil inside the meter and that WOULD produce the sound you mention, and show jumping as the arc quenches and re-establishes itself.



A quick check with an ohmeter will tell the tale here, if the resistance across the meter

s terminals is more than about 100ohms, it's bad.



 (Make sure the power is off, the laser is SAFE and locked out so it can't be inadvertently powered on before working on it)



Sure would be nice if it's a $5 meter instead of a tube. Tube prices seem to be falling, but unfortunately, so does quality. I've been hearing of a lot of people with bad out of the box tubes. Some well known places sell tubes at premium prices, but don't honor warranties, telling customers the factory water pump on the k40 is inadequate. There's about 2500 K40s out there that seem to be working fine for far longer than 2 weeks on the factory pump.  



If you do have to buy a tube, get several positive reports on the seller 1st. 



Scott


---
*Imported from [Google+](https://plus.google.com/106798316295533157122/posts/2iX6WZpXk4X) &mdash; content and formatting may not be reliable*
