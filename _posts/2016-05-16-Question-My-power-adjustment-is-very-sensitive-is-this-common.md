---
layout: post
title: "Question! My power adjustment is very sensitive is this common?"
date: May 16, 2016 22:09
category: "Original software and hardware issues"
author: "Alex Krause"
---
Question! My power adjustment is very sensitive is this common? I only have use of about 1/8 of the rotation of the potentiometer before it gets into dangerous levels is this common? 





**"Alex Krause"**

---
---
**Jon Bruno** *May 16, 2016 22:25*

There could either be a defect in your potentiometer, or it's of the wrong value. But more likely the on board trimpot in the LPSU wasn't properly adjusted from the factory. I found there are typically 2 trimpots in the power supply, one for maximum mA and one for tweaking the gain on the level POT. I do not recommend experimenting unless you are confident in your abilities to work with high voltage devices.


---
**Alex Krause** *May 16, 2016 22:27*

Can I add a resistor to the center tap to increase my range?


---
**Anthony Bolgar** *May 16, 2016 23:11*

I replaced mine with a 10 turn linear pot. Gives the ability to change the power level by very small accurate amounts.


---
**Jon Bruno** *May 16, 2016 23:32*

Yeah, you could try adding a 5k resistor to the wiper. 

That would give you a bit more adjustment...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 17, 2016 03:15*

**+Alex Krause** I've been having similar issue. Only about 1/4 of the pot's rotation is usable & it's very touchy.


---
**Anthony Bolgar** *May 17, 2016 03:19*

That is why I went to a linear 10 turn POT. Incredibly fine adjustments can be made.


---
**Alex Krause** *May 17, 2016 03:23*

**+Anthony Bolgar**​ what's the ohms rating on the Pot you purchaced


---
**Anthony Bolgar** *May 17, 2016 03:40*

I will have to open up the panel tomorrow and have a look, can't remember off the top of my head.


---
**I Laser** *May 17, 2016 07:05*

Like Anthony, replaced mine with a 10 turn pot, it's so much better I wish I did it earlier!



[http://bit.ly/1rvZYm1](http://bit.ly/1rvZYm1)



5K Ohm


---
**Anthony Bolgar** *May 17, 2016 09:21*

Yup, mine is a 5K linear taper as well. Make sure that it is linear taper or you will not get even adjustments through the range.


---
**Brent Dowell** *September 10, 2018 18:03*

Sorry about resurrecting an old thread. I just got a K40 and am experiencing this same issue, i.e.only about 1/8th of a turn of adjustment on the pot.  



Wondering if there was an adjustment to the power supply that could fix this or if it's easier to just punt and get a 10 turn 5k linear pot? 




---
**Alex Krause** *September 10, 2018 21:39*

**+Don Kleinschnitz** can you answer the question listed above  ^^^^^^^


---
**Don Kleinschnitz Jr.** *September 10, 2018 22:20*

**+Alex Krause** There are a few questions above so here goes:



The only adjustment sensitivity problems I have seen are bad pots. The pots they ship are junk.



The replacement pot is a 5K linear. A 10 turn pot is ideal. I also add a  small DVM from wiper to gnd so that I can have a digital value of its position.



There are no adjustments in the supply that will improve its resolution that is exclusively done by the pot.



Info on replacement is on my blog.



[donsthings.blogspot.com - When the Current Regulation Pot Fails!](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)








---
**Alex Krause** *September 10, 2018 22:27*

**+Brent Dowell** please see above answer from Don. He has some amazing information about the k40 on his website 


---
**Brent Dowell** *September 11, 2018 00:08*

Thanks **+Alex Krause** and **+Don Kleinschnitz**. Very helpful.




---
**Brent Dowell** *September 11, 2018 00:11*

Interesting.  The original pot That came with it was jumpy all over the board. I checked it with my multimeter and the resistance would bounce from 0 to a value as it was twisted.  I did replace it with a new one that when I tested appeared to work better. I've got a 10 turn on order and when that gets here will go ahead and replace it it with that.






---
**Brent Dowell** *September 11, 2018 05:18*

I had 3 of the pots in inventory and so tried them all on the machine tonight with one of those little led volt meters attached.  All of the pots had different response rates. One wouldnt even go below 2  volts, so I guess they are all pretty much trash.



The nice thing is taht the lasers response in milliamps matched the volt meter pretty well.



Looking foward to getting my 10 turn pot on Wednesday, but at least I know whats going on now.  Thanks again.




---
**Don Kleinschnitz Jr.** *September 12, 2018 12:15*

**+Brent Dowell** you can also test the pots stand alone using the ohms scale on the DVM. Connect from one side to the wiper and turn the pot while watching the meter.

Strange that all three pots are bad ?


---
**Brent Dowell** *September 13, 2018 03:34*

Well the 10 turn got here today and I installed it and It works much better. 



I did test the other pots and it just seems like while they work, they also just kind of jump around depending on when you are touching the knob. Maybe they'd be fine in a less critical use.  



The 10 turn I find I only use about 3 turns max, but at least it's usable over that entire range, and it does go from 0 - 5 volts at 10 turns.   I'm really only turning my k40 up to about 1.5 volts which gets translated to about 12-14 milliamps. 



But this is very usable now.  Thanks so much for your help!




---
**Don Kleinschnitz Jr.** *September 13, 2018 13:11*

**+Brent Dowell** Sounds a little like you do not have a linear tapir pot? Glad you are back in operation.


---
**Brent Dowell** *September 13, 2018 17:21*

On the volt meter I put on the pot, I'm finding that .5 volts =~ 4ma, 1.5 volts =~ 12ma.  If I crank it up so the volts read 2.5 (Pretty close to 5 turns on the pot), it pretty much maxes out the laser at 27ma.  I've been using it most of the time at around 4ma.



This is the pot I bought.

[amazon.com - Amazon.com: JANSANE 10 Turn Potentiometer 5K Ohm 2w Wirewound Multiturn Adjustable Precision with Rotary Dial Knob 6mm Shaft: Home Improvement](https://www.amazon.com/gp/product/B07D8KN6QW/ref=oh_aui_detailpage_o06_s00?ie=UTF8&psc=1)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/78H5QE8Ms9u) &mdash; content and formatting may not be reliable*
