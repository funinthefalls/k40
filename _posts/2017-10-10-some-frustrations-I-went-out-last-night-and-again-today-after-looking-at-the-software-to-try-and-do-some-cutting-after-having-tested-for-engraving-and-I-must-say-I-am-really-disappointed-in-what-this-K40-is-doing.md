---
layout: post
title: "some frustrations, I went out last night and again today after looking at the software to try and do some cutting after having tested for engraving and I must say I am really disappointed in what this K40 is doing"
date: October 10, 2017 23:07
category: "Discussion"
author: "Padilla's custom leather"
---
some frustrations,  I went out last night and again today after looking at the software to try and do some cutting after having tested for engraving and I must say I am really disappointed in what this K40 is doing.  This is to point that I am considering selling this machine and just waiting till I can afford to get a bigger better machine.  Today all I wanted to do was see if the machine would cut a circle out of some leather that is under 1/8 thick and the most I could get out of it was a little deeper cut than what the machine had done when I did and engraving of an oval logo.  so to maybe answer in advance I had the machine set at the lowest travel speed and 28 milliamps according to the meter and it hardly scorched the surface of the leather.  I was very surprised at the lack of ability to cut all the way through the leather, I did dampen the leather some after a few tries with no luck.  I am thinking about selling I really have the funds to continue to upgrade if the results are going to e lack luster. 





**"Padilla's custom leather"**

---
---
**Jason Dorie** *October 10, 2017 23:15*

28ma is quite high - I think I have mine maxed at 12, and can cut 3mm plywood which I'd expect to be similar to thin leather.  You may be damaging the tube at that power setting.  It's also important to cool the tube below room temperature (not too quickly or you'll crack it).  A tube cooled to around 60-65f will cut significantly better than one at room temp.


---
**Joe Alexander** *October 10, 2017 23:18*

i would also check to see if your focal lens is backwards (bump should be up) or material is not at focal point. standard distance is 50.8mm




---
**Chuck Comito** *October 11, 2017 01:05*

Take some time and research how to focus. It is really important. I went through the same thing when I was starting. Also another important thing to consider is to make sure the beam is actually hitting the center of your lense. You will loose power if it's not..


---
**Padilla's custom leather** *October 11, 2017 01:38*

thanks for the input I went out and dropped the nozzle down a bit and it cut much better on some thin leather tomorrow I will test it on some heavier leather and I will need to find a way to chill the water more, I have a small circulator that I got when my knee was replaced but it is not very big but it might work to chill the water.




---
**Chuck Comito** *October 11, 2017 02:19*

I have my water running from a pump in a 5 gallon bucket. The bucket is sealed and the input and output tubes fit tightly through holes in the lid. I run distilled water in it. I've never had temperature issues or power loss due to water temp. I've heard a lot of people say it but I'm not sure if that statistic is a fact or if it's assumed... either way I'd keep it cool. I'm just now sure how important water temp actually is. As long as you're not overheating the tube you should be fine. 


---
**Jason Dorie** *October 11, 2017 02:30*

It’s easy to test.  Set a raster scan of a simple filled square.  About 1/3 of the way through, drop some ice in your bucket close to the intake of the pump, and watch the cut.


---
**Chuck Comito** *October 11, 2017 02:47*

**+Jason Dorie**​, I will try it. Thanks. 


---
**Paul de Groot** *October 11, 2017 05:38*

**+Padilla's custom leather** you might want to use a better lens. The stock lens has high loses. I couldn't cut much either. 8mA for 2 mm acrylic now i use a usa ZnSe lens and use 4mA with one pass. Clear cuts and no frustrating attempts. Have a look at my blogs on [awesome.tech](http://awesome.tech) 


---
**Paul de Groot** *October 11, 2017 05:38*

[awesome.tech - Inspiring science for hungry minds](http://awesome.tech)


---
**Padilla's custom leather** *October 11, 2017 19:37*

when I went to the blue air cooled nozzle I also bought a new lens and mirror for it, it wasn't the most expensive but next to it.   Now for another question I thought that I saw some where that there is a google page or site that has items that have been created and can be down loaded, any help with this?


---
**Padilla's custom leather** *October 11, 2017 23:51*

To Paul de Groot, I went to your website and am very excited about your product but couldn't find a way to ask a question, I have already upgrade the laser nozzle to a blue one with a new lens and mirror and I also have bought a chain now here is the dumb part, I couldn't figure out how to use the chain so do you provide a bracket, and also being that I already have the items mentioned which package would be the best to get and are they only still being sold through the one site?


---
**Paul de Groot** *October 12, 2017 01:59*

**+Padilla's custom leather** You can find the customer brackets on thingiverse just search for k40 and get someone on 3dhubs to print them. Alternatively I can see if we have some brackets left over from the kickstarter and send them to you. You can buy a single controller from me or from other parties, there are a few options here (e.g. cohesion3d, re-arm). The advantage of my board is that it created for non-tech savvy people who need an integrated tool chain e.g. single open source software and hardware that works together. You can contact me via PM on google+ or on my website (contact us)


---
**Jason Dorie** *October 12, 2017 02:15*

**+Paul de Groot** Not sure if you’ve heard of it yet or have any interest, but I’m working on a more user-friendly software app for layout, edit, and send.  It supports GRBL and I’d love to see if it’s compatible with your board, and if not, make whatever changes are necessary.



[Http://Facebook.com/LightBurnLaser](Http://Facebook.com/LightBurnLaser)


---
**Paul de Groot** *October 12, 2017 05:16*

**+Jason Dorie** If it support grbl gcode then it will definitely work. My grbl changes are in pin PWM assignment to allow 16bits instead of 8 bits pwm and pwm freq options.


---
**Jason Dorie** *October 12, 2017 05:19*

**+Paul de Groot** ..and your communication is otherwise the same?  Is your serial buffer the default size?  I'm also acting as sender and live machine control, with a console, so the details are important.  I can set you up with an install if you're game to try it.  It's pretty painless - aimed at novice users.


---
**Paul de Groot** *October 12, 2017 12:22*

**+Jason Dorie** yes all serial buffer size and comms are left as default. I would definitely like to try it out and let you know how it works with Gerbil.


---
*Imported from [Google+](https://plus.google.com/106971026516976154588/posts/YSVeidriUY1) &mdash; content and formatting may not be reliable*
