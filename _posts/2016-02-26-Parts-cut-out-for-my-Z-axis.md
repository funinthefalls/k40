---
layout: post
title: "Parts cut out for my Z axis....."
date: February 26, 2016 15:47
category: "Modification"
author: "The Technology Channel"
---
Parts cut out for my Z axis..... waiting for honeycombe bed to build

![images/89b4aa84bb9ce3b6fe9d8c32bcc8c406.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/89b4aa84bb9ce3b6fe9d8c32bcc8c406.jpeg)



**"The Technology Channel"**

---
---
**3D Laser** *February 26, 2016 16:25*

Would you be willing to share the plans and parts list 


---
**The Technology Channel** *February 26, 2016 16:36*

Well at the moment its just design as I go.. I will keep all the CorelDraw files together and when its all together and working post the files somewhere... its simple enough really....


---
*Imported from [Google+](https://plus.google.com/105609664958110832961/posts/cnxzg4wzRLz) &mdash; content and formatting may not be reliable*
