---
layout: post
title: "Oh hey, anyone wanting a nice gradient engraved?"
date: April 22, 2017 01:41
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Oh hey, anyone wanting a nice gradient engraved? Just for fun, try to forget to turn on the cooling pump. WARNING: NOT RECOMMENDED, DO NOT DO IT! (I did it by accident.)



Allow me to go smash my head against the wall now. Yes the tube is still working, but dammit, I just shaved countless hours off of its lifespan. And this despite having two temperature gauges that I KNOW should show a temperature difference of no more than 1.3° between the inlet and outlet. Yet, I never actually looked at them. @*#&^$$%(@!!  I should just quit now while I'm ahead, and spend the weekend installing the dumb waterflow sensor and other electronics  I've been meaning to. Urgh.



ALSO: when something like this happens, don't go flipping on the cooling right away. That's like tossing ice cold water on a overheated pan on the stove. Just be calm, turn things off and walk away for a good long time. Let the tube cool down on its own.





**"Ashley M. Kirchner [Norym]"**

---
---
**Ned Hill** *April 22, 2017 01:49*

I had that happen to me on a long engrave one time and when I glanced at the temp guage it was 35C O.O Fortunately I figured out fairly quickly it was a kinked hose in the bucket from all the ice packs I had shoved in. :P  Finally got me to install my flow sensor.




---
**Ashley M. Kirchner [Norym]** *April 22, 2017 01:56*

What was that temp from? Standing water in the bucket?


---
**Ned Hill** *April 22, 2017 02:19*

No, I have a temp sensor attached to the outlet hose on my tube in the compartment.


---
**Ashley M. Kirchner [Norym]** *April 22, 2017 02:25*

Right, so you were measuring standing water. It gets way hotter than that.


---
**Ned Hill** *April 22, 2017 02:29*

Actually the flow wasn't totally killed but it was just a trickle.  The tube was a probably somewhat hotter than 35C though.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 22, 2017 02:30*

Out of curiosity, how hot did your setup get **+Ashley M. Kirchner**? Hope it is all good & doesn't give up the ghost anytime in the near future. I'd be kicking myself in your situation.


---
**Ashley M. Kirchner [Norym]** *April 22, 2017 03:20*

Well, the bucket was sitting at 5C ... I wasn't about to try and flush that through the tube after just running it for a 9 minute raster job, albeit low power (nothing above 25%), still ... I wasn't about to risk it shocking the glass.


---
**Jim Fong** *April 22, 2017 03:20*

One of the first mods I did was to install a flow switch from light objects. It saved me a couple of times already when I forgot to turn on the pump.  Now I put the pump/laser/exhaust fan plugs all on the same surge protector. I use surge protector on/off switch to turn them all on/off. 


---
**Ashley M. Kirchner [Norym]** *April 22, 2017 03:52*

I've had it for a while, just never installed it (because I'm going to add several things all at once). I'm pretty good about that pump except for recently when I changed my flow of operations. Just didn't pay attention today. In already back to cutting. It's 40 degrees out and the window that the machine sit next to is open, so it helped cool the tube down once I turned everything off. 


---
**Steven Kalmar** *April 22, 2017 03:54*

Stories like this scare me.  I have a bad habit of forgetting stuff, like turning on a pump.  I already have plans for a flow sensor to measure the flow rate and combine interlocks using an Arduino.  I should probably move this up the list.  Hopefully you didn't loose any tube life.  Thanks for sharing.  


---
**Ashley M. Kirchner [Norym]** *April 22, 2017 04:02*

Probably only shortened it a bit. It's working fine at the moment. But ya, preventing it from firing with no flow would be a fine mod to do one of these days. 


---
**Cesar Tolentino** *April 22, 2017 05:14*

maybe a solution is to put the laser, the exhaust fan, the water pump, and any other critical needs to an extension strip with power switch? so you only turn on one switch and all goes on at the same time?



[homedepot.com](http://www.homedepot.com/catalog/productImages/400/07/075cbb2d-67f5-404b-bee7-962e0712de4e_400.jpg)




---
**Ashley M. Kirchner [Norym]** *April 22, 2017 06:27*

They are on a power strip, however I tend to unplug the pump when the machine is idling because the pump itself warms up and will heat up the water.


---
**Ashley M. Kirchner [Norym]** *April 22, 2017 06:28*

What I need to do is wire them to the laser cabinet with a light up power switch so I can easily tell what's on and what's not. All parts that I have, just haven't done any of it yet.


---
**Cesar Tolentino** *April 22, 2017 07:06*

Change the pump it should not heat up the water. 


---
**Don Kleinschnitz Jr.** *April 22, 2017 13:55*

Use a temp monitoring system, cheap on Amazon, that has built in temp limits alarm and relay contacts. So that when the water gets to hot the relay trips the interlock. See my blog. 


---
**Ned Hill** *April 22, 2017 14:00*

I really want to set up an interlock alarm that sounds out "Danger Will Robinson, Danger!"


---
**Don Kleinschnitz Jr.** *April 22, 2017 14:04*

**+Ned Hill** not that hard to do:).


---
**Cesar Tolentino** *April 22, 2017 14:21*

**+Ned Hill**​, also add. Resistance is futile. Lol




---
**Ashley M. Kirchner [Norym]** *April 22, 2017 17:16*

Now see, I'd be the one swearing at myself. "Turn the water on first, you dumb F*CK!"


---
**Cesar Tolentino** *April 22, 2017 17:17*

**+Ashley M. Kirchner**​. Hahaha


---
**Ashley M. Kirchner [Norym]** *April 22, 2017 17:27*

**+Don Kleinschnitz**, thanks for the suggestion, but I have all the tools I need to do what I've been wanting to do for months. I just haven't done yet. And I'm getting complacent ...


---
**Steve Clark** *April 23, 2017 01:42*

I'm working on temp monitoring right now so I need to read your blog Don. My pump is inline, not in the water and that was heat problem one of the secondary reasons i went outside the tank.



Currently I'm not directly hooked up to the machine  power supply plugs on the back.  After looking at and being concerned about the sloppy wiring and grounding it came with I  ordered new plugs to replace the factory ones.  I'm using a power strip too a this time.



Chin up Ashley, you could lost the tube and thanks for reminding the rest of us to pay attention!


---
**Don Kleinschnitz Jr.** *April 23, 2017 10:17*

My plan this summer is to solve the cooling problem. I am going to add a temp monitor to the tube, evaluate peltier vs conventional refrigeration (been thinking about a small refrigerator)  and then install a winter heating subsystem.

Of course that is in line with a new Ox router and everything finally moved to the shop so I don't burn down my house :(.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/Y8P84ZJMdrB) &mdash; content and formatting may not be reliable*
