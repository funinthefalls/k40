---
layout: post
title: "Finally got it down stairs it's hard to believe how much mugger this thing is"
date: May 13, 2017 22:41
category: "Discussion"
author: "3D Laser"
---
Finally got it down stairs it's hard to believe how much mugger this thing is.  I can't wait to get all my files converted and switching over.  I am donating my k40 to a local high school shop class once I get it all transferred 

![images/379309d24f192fc3e80028322fdfa992.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/379309d24f192fc3e80028322fdfa992.jpeg)



**"3D Laser"**

---
---
**Phillip Conroy** *May 14, 2017 01:05*

Check water flow rate by filling a container over a minute, the pump that cam with my k50 was only moving 1 liter per minute and most tube makers recommend 2 liters per minute, changed pump to a 150 watt pump and now get just on this


---
**3D Laser** *May 14, 2017 01:09*

**+Phillip Conroy** thinking about getting a water chiller to replace the standard water pump


---
**3D Laser** *May 14, 2017 01:10*

**+Phillip Conroy** what do you use for a laser bed 


---
**Phillip Conroy** *May 14, 2017 06:24*

Knife bed made up of 300 mm long hacksaw blades spaced 8 to 10 mm apart held together with a potplant hanging wire from hardware store,and using nuts to provide the space ins ,all this held off the au bed by 10mm magnets  stacked 33 high ,Iuse a removable steel tray to tick the manes to and remove and spray with oven cleaner  then  hose off weekly. I also use pin bed made up of magnets and wood screws.if I am cutting large objects I use pin bed,if I am cutting small objects, ie 20 mm hearts x 100 I use the knife bed so that they do not fall out of the cutting material and get damaged by the cuts nearthem

![images/4ed233ae65f12afcaf8013f4f7991ed1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4ed233ae65f12afcaf8013f4f7991ed1.jpeg)


---
**Phillip Conroy** *May 14, 2017 06:27*

Honeycomb bed that everybody talks about are good for flexible materials ,ie paper or cloth,if used on stiff materials  ie mdf or plywood they alow glue and smoke resa due to form on the rear of what ever is being cut


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/es78C5HnUZA) &mdash; content and formatting may not be reliable*
