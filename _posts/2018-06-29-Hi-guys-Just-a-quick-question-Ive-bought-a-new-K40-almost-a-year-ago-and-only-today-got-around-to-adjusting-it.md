---
layout: post
title: "Hi guys, Just a quick question, I've bought a new K40 almost a year ago and only today got around to adjusting it"
date: June 29, 2018 18:56
category: "Discussion"
author: "Mishko Mishko"
---
Hi guys,

Just a quick question, I've bought a new K40 almost a year ago and only today got around to adjusting it. The ajustment went well, but when I tried to cut, the beam was kind of divided in two, basically dragging the fuzzy image around 10mm from the cut in both axes.

I checked all the elements from the lens back to the tube, and the output beam from the tube itself on the cardboard directly in front of it gives an image which doesn't look like a point, but rather like a comma rotated by 90 deg. On teh second mirror it looks more like crescent moon, but I guess it doesn't matter...

I tried to clean the front glass with a swab/ethanol, but got the same result.

Does this mean the tube is faulty, or is there any other explanation I'm not aware of?

Sorry if this was already mentioned somewhere, but I couldn't find anything similar. Thanks in advance for any idea/help.







**"Mishko Mishko"**

---
---
**Don Kleinschnitz Jr.** *June 29, 2018 19:08*

can you post a picture of the beams output please.


---
**Mishko Mishko** *June 29, 2018 20:03*

Hi Don, thanks for the quick reply. I don't have the laser where I'm now, it's in my friend's workshop, so I won't be able to take a picture before Monday. I will post it then. In the meantime, it really looks like a comma, a fairly big dot (around 2mm diameter with a carboard perpendicular to the beam at maybe 10mm distance from the tube front) and an arc going from its top to the left. I'm attaching a sketch, looks pretty much like that. It'll have to do until Monday I'm afraid.

![images/23ad2fd54189c09b16106d1441b7dcec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/23ad2fd54189c09b16106d1441b7dcec.jpeg)


---
**Don Kleinschnitz Jr.** *June 30, 2018 12:44*

**+Mishko Mishko** that's a new one... a comma. Normally I would start looking for a reflection somewhere but you say this image is on a piece of cardboard directly in front of the lasers output? Meaning that its the raw output of the laser.

If so that would suggest a bad tube but these symptoms are none that I have seen....

While I wait to see an actual image I will be ..... thinking ......


---
**Mishko Mishko** *June 30, 2018 15:19*

Thanks Don, well maybe I took a little artistic freedom, but not by much. Will fire the laser on Monday and post a photo from microscope. I did not notice this and the laser did cut before the adjustments, and it certainly did not drag this "ghost" beam at some offsett from the actual focused beam, which kind of "etches" a blurred image on acrylic. But I guess all that is irrelevant as the "comma" is indeed an image projected directly from the tube. At the 2nd mirror it looks more like a crescent moon, and at the cutting surface, after the lens, the point is not really a point, but more like a short vertical line, or maybe an ellipse or a slot. Guess that's logical as they are really the projections of the bloody comma;)


---
*Imported from [Google+](https://plus.google.com/114169453703152974799/posts/VxyxhGu8DeY) &mdash; content and formatting may not be reliable*
