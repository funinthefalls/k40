---
layout: post
title: "Can't run CoreIlaser after installing Norton 360. Any help?"
date: July 04, 2016 11:42
category: "Discussion"
author: "Ben Marshall"
---
Can't run CoreIlaser after installing Norton 360. Any help?





**"Ben Marshall"**

---
---
**Anthony Bolgar** *July 04, 2016 12:03*

Uninstall norton and try Avast antivirus (Free version available, and just as good or better than Norton) or any other anti vius.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 04, 2016 12:37*

Maybe you can also add exceptions to Norton 360 for CorelLaser. I'm unsure as I don't bother with antivirus software (except default windows defender).


---
**Scott Marshall** *July 04, 2016 12:54*

2 things I can think of MAY help. The 1st  thing to try is right click on the Corel launcher, and see if Norton offers a "permission to run" option there (it's a common place for them to hide such options). If not go ahead and try to "Run as administrator". Sometimes that will allow you to open blocked programs.

Failing that, open Norton and see if there's a page to add "program to exceptions" or something similar. Many AV programs have a page where they list permissible programs, and allow you to edit it.



Lastly, Look at the Norton site/forum and look for a fix, you can't be the only one. There may be a customer service option, but I swore Norton off years ago because of terrible customer service. Kaspersky is my currently recommended AV package. (I do a bit of computer work for people).



Good luck, Scott


---
**greg greene** *July 04, 2016 13:13*

Norton is not just an AV program - it takes your computer hostage !


---
**ThantiK** *July 04, 2016 16:59*

Norton is a virus in and of itself.  Don't use it. 


---
**HalfNormal** *July 04, 2016 17:03*

**+Ben Marshall** I agree Anthony. I have been using Avast for years and have not had any issues. A friend had Mcafee antivirus and she still ended up hosing her laptop due to infections. I had to do a total wipe and reinstall of the OS. Let us know what you find out.


---
**Ben Marshall** *July 05, 2016 01:58*

I went back to McAfee. Problem solved. I can now keep calm and laser on. Thanks for everyone's input!


---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/NX33SDBkGvP) &mdash; content and formatting may not be reliable*
