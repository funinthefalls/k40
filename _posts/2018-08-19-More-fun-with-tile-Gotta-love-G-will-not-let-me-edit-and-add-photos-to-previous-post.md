---
layout: post
title: "More fun with tile! Gotta love G+, will not let me edit and add photos to previous post!"
date: August 19, 2018 01:36
category: "Object produced with laser"
author: "HalfNormal"
---
More fun with tile!



Gotta love G+, will not let me edit and add photos to previous post!

![images/710cd6df3ea70c3c56861678ca1ccc8d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/710cd6df3ea70c3c56861678ca1ccc8d.jpeg)



**"HalfNormal"**

---
---
**Travis Sawyer** *August 19, 2018 02:14*

Neat!


---
**Printin Addiction** *August 19, 2018 13:31*

Water based acrylic ink works great as well, and probably a lot quicker.






---
**Printin Addiction** *August 19, 2018 13:34*

I wanted something large, but was limited by the bed size of the k40, these tiles are 6x6 matte finish.![images/98db4e0171940867cdc0aa10899cc07f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98db4e0171940867cdc0aa10899cc07f.jpeg)


---
**HalfNormal** *August 19, 2018 14:17*

**+Printin Addiction** I remember that. Great work.


---
**Printin Addiction** *August 19, 2018 18:06*

Thx, I would love to get a bigger laser to be able to engrave 12x12 tiles....


---
**Timothy Rothman** *November 06, 2018 06:24*

**+HalfNormal**  Can you please share your design file and settings.  I assume you were using  an upgraded controller, can you confirm. 



 I too have spent too much time at the Habitat for Humanity and had only a little luck with the stock controller.  



Interested in power/speed and DPI of the image vs scan resolution of the raster.  


---
**HalfNormal** *November 06, 2018 17:05*

**+Timothy Rothman** Yes the tile was done with a DSP controller so it is a little easier to go from picture to laser. That being said, if you take a picture and turn it into a vector then you can use any controller. Search google for mc escher fish poster and you will find it.

As for settings, all you are trying to do is remove the glaze from the tile. This takes at least 80% on a 40 watt laser and slow speed. I did not dither the image, I used black and white setting which is more like vector cutting but in a raster mode. With that said, be careful. The tile will heat up and you can cause cracking in the glaze. This all depends on the tile composition ect. If you experience the cracking, you can preheat the tile with a torch or heat gun. Unfortunately every batch of tiles can make your settings vary. Lots of luck and let me know how it all turns out!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/WdPUA8x3b6M) &mdash; content and formatting may not be reliable*
