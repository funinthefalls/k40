---
layout: post
title: "Anthony Bolgar Stephen Sedgwick Ariel Yahni Yuusuf Sallahuddin Here is 1 of the 4 screw shaft assy"
date: June 11, 2016 13:56
category: "Modification"
author: "Don Kleinschnitz Jr."
---
**+Anthony Bolgar** **+Stephen Sedgwick** **+Ariel Yahni** **+Yuusuf Sallahuddin** 



Here is 1 of the 4 screw shaft assy. on my  table. I got this as a replacement for a bent one.

![images/859466490c8588061e9d629eed148de1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/859466490c8588061e9d629eed148de1.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *June 11, 2016 13:59*

So is basically a leads crew, 2 bearings, a teeth gear and some nuts? 


---
**Don Kleinschnitz Jr.** *June 11, 2016 14:42*

Yes, the 4x lead screws are driven by a cog belt that wraps around the 4 cog pulleys (one on each corner) and driven by the stepper. The plate is captured between the nuts.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 12, 2016 01:26*

I've seen slightly different methods of holding the plate, using a 1 piece nut piece that attaches to the plate. Thanks for sharing Don.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/fZHwXbsWVcQ) &mdash; content and formatting may not be reliable*
