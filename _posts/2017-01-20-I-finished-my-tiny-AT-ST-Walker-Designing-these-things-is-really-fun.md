---
layout: post
title: "I finished my tiny AT-ST Walker. Designing these things is really fun"
date: January 20, 2017 18:06
category: "Object produced with laser"
author: "Jeff Johnson"
---
I finished my tiny AT-ST Walker. Designing these things is really fun. This one was inspired by another model on Thingiverse. 





**"Jeff Johnson"**

---
---
**Chris Pritchard** *January 22, 2017 23:25*

Thank you from one massive star wars fan to another! One quick question. I know all machines are different etc. But, as a guidline, what power settings do you use for light cut? As in ma and speed? Many thanks, once again great job!


---
**Jeff Johnson** *January 22, 2017 23:39*

For the light cut I use 3ma @ 25mm/s. That's just past where the laser starts firing for me.


---
**Robin Sieders** *January 23, 2017 21:47*

Hi **+Jeff Johnson** I tried to download and cut this last night, but no matter what I try, nothing shows up in the .cdr file. Are you still using corellaser, or are you using laserweb?

Thanks


---
**Jeff Johnson** *January 23, 2017 22:05*

**+Robin Sieders** I use CorelDraw X7 and the CorelLaser that was included with the laser. Maybe there's an incompatibility with the version you're using. Try the PDF, sometimes you get a weird line or two out of place but that's easy to fix. It should also import the layers as well - at least it does for me.


---
**Robin Sieders** *January 24, 2017 03:53*

**+Jeff Johnson** Will do, thanks


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/NbZRxFQqp5c) &mdash; content and formatting may not be reliable*
