---
layout: post
title: "Some parts made with a laser"
date: September 13, 2018 15:04
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Some parts made with a laser.




{% include youtubePlayer.html id="QLCUyJK0hVM" %}
[https://www.youtube.com/watch?v=QLCUyJK0hVM&t=15s](https://www.youtube.com/watch?v=QLCUyJK0hVM&t=15s)





**"HalfNormal"**

---
---
**Roberto Marquez** *September 14, 2018 04:46*

I saw this projectl on Show and Tell yesterday.  Nice!


---
**Rene Munsch** *September 14, 2018 09:03*

What is the powder at the glue for?


---
**Roberto Marquez** *September 14, 2018 13:28*

I am not sure.  Maybe some kinda activator for epoxy.




---
**Vilius Kraujutis (viliusk)** *September 15, 2018 07:11*

I would guess it's something just for paint to stick better.


---
**Vilius Kraujutis (viliusk)** *September 15, 2018 07:12*

**+HalfNormal** but what about driving? is it going back and forth only? how about making a turn? :D




---
**HalfNormal** *September 15, 2018 14:51*

**+Vilius Kraujutis** Get a bigger cat!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/gKdNt4x7rX4) &mdash; content and formatting may not be reliable*
