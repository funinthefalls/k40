---
layout: post
title: "Laser cutters lend themselves nicely to making custom clocks"
date: January 28, 2017 08:03
category: "External links&#x3a; Blog, forum, etc"
author: "Anthony Bolgar"
---
Laser cutters lend themselves nicely to making custom clocks. However, it can be difficult to find the parts you need at a reasonable price, or information on how to build and troubleshoot clocks (especially mechanical ones).



So I decided to start a new Google+ community called Clock Repair Resources. It is a place to get information on how to repair clocks, as well as a source of parts for the hobbiest. 



[https://plus.google.com/u/0/communities/109175794108260586666](https://plus.google.com/u/0/communities/109175794108260586666)



<b>Originally shared by Anthony Bolgar</b>



Welcome to Clock repair resources. Feel free to ask any clock related questions here. I am the owner of Niagara Clock, a family owned and operated clock business that was established in 1986. I have 31 years of experience repairing and restoring clocks. Our company also supplies parts to the hobbiest interested in handcrafting timepieces.





**"Anthony Bolgar"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 28, 2017 08:49*

Just joined, as I've recently considered making a clock (or few). Will be interesting to see what comes out of the group.


---
**Anthony Bolgar** *January 28, 2017 08:51*

Me too! I will be posting some parts packages for those who wish to build their own clock, as well as posting some different dial designs. I also have a lot of old inventory of parts that I am going to advertise for a fraction of their value ( I really need to clear out some room in my shop). Welcome to the group and thanks for joining.


---
**Ned Hill** *January 28, 2017 20:15*

Great I've been thinking about making some clocks so I joined as well.


---
**Anthony Bolgar** *January 28, 2017 20:17*

Let me know what you need **+Ned Hill**  I am sure I can accomadate any reasonable request.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/5BTVhRJUAWm) &mdash; content and formatting may not be reliable*
