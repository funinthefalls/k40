---
layout: post
title: "From a yard sale deal to a unique desk lamp"
date: February 24, 2016 23:30
category: "Object produced with laser"
author: "Ben Walker"
---
From a yard sale deal to a unique desk lamp.   I did this before work,  during lunch,  and finished after work today.   Maybe 4 hours total which includes a couple mis-cuts and my air assist going rogue and blocking the laser path.   Not too shabby for a newbie i must admit. 



![images/d6fd6c217004e4b0b4fb71c88b8ffe69.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d6fd6c217004e4b0b4fb71c88b8ffe69.jpeg)
![images/376fb44fac0d2e8be8fefd611c007b59.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/376fb44fac0d2e8be8fefd611c007b59.jpeg)

**"Ben Walker"**

---
---
**Scott Marshall** *February 25, 2016 08:35*

Rockin. 



Gonna tell us the skinney? 



Them's the rules, If you wanna gloat, you gotta tell us how to make one!


---
**Ben Walker** *February 25, 2016 17:24*

**+Scott Marshall** Weeeellll I just sat at the desk and emulated the gold design of the lamp and measured the top ring.  Cut four panels all the same and basically made small L brackets to glue it all together.  Ripped the ring out of the top of the old shade and hot glued it in.  Really simple.  Waiting for durability tests with all that glue.  


---
**Scott Marshall** *February 29, 2016 04:45*

It's hard to see what it's made from. Acrylic painted gold then glued on a clear backer?


---
**Ben Walker** *February 29, 2016 12:41*

It is baltic birch.  I plan to glue vellum to the inside to help diffuse the light.  I have it - just too lazy to cut by hand.  lol


---
**Scott Marshall** *March 01, 2016 14:22*

That's my material of choice. Simpler than it looks I guess.



 Really like the look, and the vellum diffuser would add a nice touch. 



When garage sale season starts here Guess I'll have to keep my eyes open.



Thanks,

Scott


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/Z23oRdauuvd) &mdash; content and formatting may not be reliable*
