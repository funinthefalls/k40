---
layout: post
title: "Not something I've made, but just got some nice swag from a brand we sell at work"
date: April 20, 2017 18:49
category: "Object produced with laser"
author: "Mark Brown"
---
Not something I've made, but just got some nice swag from a brand we sell at work.  If the main body was 3-ish layers thick instead of solid these would be a snap on the k-40

![images/0e2a8ac5b359bf90de37f42415d57fd4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0e2a8ac5b359bf90de37f42415d57fd4.jpeg)



**"Mark Brown"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2017 13:45*

Interesting concept & design. I'm sure your idea of multiple layers would be doable.


---
*Imported from [Google+](https://plus.google.com/116643344835605995638/posts/K9twfGhPcUq) &mdash; content and formatting may not be reliable*
