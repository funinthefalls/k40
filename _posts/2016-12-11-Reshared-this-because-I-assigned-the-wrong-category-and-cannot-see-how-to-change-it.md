---
layout: post
title: "Reshared this because I assigned the wrong category and cannot see how to change it"
date: December 11, 2016 06:08
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Reshared this because I assigned the wrong category and cannot see how to change it.



<b>Originally shared by Don Kleinschnitz Jr.</b>



Update on the laser response front. 



Nothing conclusive yet but made some interesting measurements today.



For some time I have been wanting to measure the lasers output and characterize its response to PWM control.



Looking at the laser light itself has been a bust. Turns out that 10600nm is really out there FAR ....DOH! 



There isn't anything electronic and cheap that has sensitivity in that range, and doesn't get fried. I tried everything including many models of IR sensors and photo-diodes, even avalanche devices . 



The most usable device was a thermopile (the type in PIR's) it will respond at this wavelength but too SLOWLY. I need to measure in the range of 1-10's microseconds!  



Then it dawned on me that I might be able to measure the lasers response indirectly and safely by scoping the tubes current as it responds to PWM pulses. 



Could measuring the current in the tube tell us anything reliable about its output?

Does the tubes current response correlate with the tubes optical response? 

.........

On one hand....

I logic that the light response should be faster than the electric current response curve but likely not slower. 

Therefore:

If I can create current pulse responses that meets the benchmark we probably can conclude that the light response meets the benchmark.

On the other hand ......

If I cannot show current pulse responses that meet the benchmark then we cannot conclude that the light response does not meet the benchmark unless we can prove the correlation between current and light response. 



Now that my K40 conversion is in a reasonable state of use I started to explore this idea of measuring laser light response by measuring its current pulse response. 



To benchmark I calculated that the smallest PWM pulse width for reasonable engraving speeds and resolution with a 5% grey shade step increment would be about 11.7us. [another post will show the math]



The tests results shown below were created by looking at the voltage across the current meter in sync with the low going activation of the "Test" button. 



........

Annotation of the pictures that follow numbered in successive order:

1. This rise time of 1.8us is below the benchmark :)

2. Another capture of a rise time of 1.9us :)

3. A much slower time of 20us captured [no idea why]

4. A good look at  the internal LPS PWM signal. A period of 408us and a pulse width of 84us at 20% DF.



What does this suggest:

... Sugests that the laser can respond fast enough for reasonable engraving applications. Laser response may be 1.8us or less.

.... Its not clear why the response can be slower ie: 20us vs 1.8us

.... The internal PWM period of the LPS is 408us which suggests that we could run the smoothies at that same period?? A shorter period is better however.

.... Finding fast enough edges suggests that this may be a suitable 

method for measuring the lasers response.

.... Need to test using the smoothies PWM signal with the LPS PWM maxed out (IN = max).

.... The voltage across the laser current meter is very small.





![images/94ed92f93cac9dc684ae77f708d52fd1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/94ed92f93cac9dc684ae77f708d52fd1.jpeg)
![images/f391b3111366db2c15f76c1370a9533c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f391b3111366db2c15f76c1370a9533c.jpeg)
![images/307f508174ef1f516b66e94281ee7a75.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/307f508174ef1f516b66e94281ee7a75.jpeg)
![images/9a9c0918a1b5400e93db69d46e6c05b6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a9c0918a1b5400e93db69d46e6c05b6.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Mircea Russu** *December 11, 2016 14:43*

**+Don Kleinschnitz** what about measuring the visible light output by thee tube? I mean the plasma discharge in the tube, the purple coloured light? Is that dependent on the power level?




---
**Don Kleinschnitz Jr.** *December 11, 2016 15:21*

**+Mircea Russu** nice idea, that is also a worth exploring. I wonder, when you pulse the laser continuously does it stay ionized or does it ionize-de-ionize on every pulse. Answering that may require returning to the physics book:). In any case I certainly can put a sensor on the tube :). I will let you know.


---
**HalfNormal** *December 11, 2016 17:01*

**+Don Kleinschnitz** As per our previous discussion, have you tried to see if there is a marked difference in using the IN and L/Enable inputs?

I also had a situation that makes me wonder about the TEST button and how it can control the PS. I had a bad Nano board that would not give full power during program mode ie running during cut/engrave but I could control the power from holding the test button and vary the power pot. I did not troubleshoot the bad board so I do not know what its mechanism of failure is.


---
**Don Kleinschnitz Jr.** *December 11, 2016 17:12*

Its hard for me to answer your question yes or no because I can create specific settings where I can make IN work. Its all in the definition of what "working" means.

The problem with IN + controller PWM is that it creates a composite PWM signal so the controller is not controlling what it thinks it is. From that perspective my view is that YES there is a marked difference in that the IN control creates a "depends" vs a predictable control of laser power by the controller. Additionally there is no benefit to being connected to IN over "L". 

BTW I use "L" only without an Enable. Haven't found a reason for an Enable.


---
**HalfNormal** *December 11, 2016 17:15*

I used the nomenclature L/Enable because on a standard unit, L needs to be "Enabled" for the laser to fire and IN to vary the power. 


---
**Madyn3D CNC, LLC** *December 14, 2016 02:28*

These are the super simplified PWM boards I populate to drive low voltage electronics. Just a 555 timer, few caps and a resistor. There's also a few diodes on the trim pot. 

![images/4ca1b3badeee3258128c2b31e94905ea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4ca1b3badeee3258128c2b31e94905ea.jpeg)


---
**Madyn3D CNC, LLC** *December 14, 2016 02:31*

Better pic includes the FET doing all the high frequency switching driven by a square wave :) 

![images/f87a85024c00e5c6da5bff8330acb09f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f87a85024c00e5c6da5bff8330acb09f.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/538Wo6YsvmE) &mdash; content and formatting may not be reliable*
