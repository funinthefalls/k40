---
layout: post
title: "Community stats and organisation: I just finished to re-read all the posts and comments (about 5H), and rearrange them in categories"
date: December 02, 2015 11:14
category: "Discussion"
author: "Stephane Buisson"
---
Community stats and organisation:



I just finished to re-read all the posts and comments (about 5H), and rearrange them in categories. Sometime the original post don't look like they are into their right category, but the solutions provided in the comments fit that category.

I hope that exercise to organise the informations will help you to point out quickly to a solution. So I suggest you browse by categories.



To date 645 members ->577 posts (2/12/2015)

1st year 552 members->414 posts (community birthday 7Nov.)



You notice the acceleration in data volume, please dig in to find a solution to your problem before asking, and think what is relevant to the community before posting. It's no limit in comments, please use them to chat, but do not create new post for that purpose to not dilute infos. Please select a appropriate category when you post.



Last word, I would like more post about  "Object produced with laser", to lift and support the "Maker spirit".



Thank you for your comprehension, and to being part of this community.

![images/190ed3c08072e342469881d2650884af.png](https://gitlab.com/funinthefalls/k40/raw/master/images/190ed3c08072e342469881d2650884af.png)



**"Stephane Buisson"**

---
---
**Gary McKinnon** *December 02, 2015 12:00*

Thanks Stephane for your hard work, i hope to make stuff soon :)


---
**Stephane Buisson** *December 02, 2015 23:47*

when you make a post the system will ask you to choose from the drop down menu, simple!


---
**Scott Thorne** *December 06, 2015 18:29*

A little late **+Stephane Buisson**....but like everyone else says....great job...you have a very nice site here and I'm glad to be a member....very helpful to us rookies!


---
**andmif** *December 31, 2015 19:21*

@Stephane. Thank you ! organizing all these post is huge help!






---
**Stephane Buisson** *January 05, 2016 10:55*

community reach 800 members on 5/1/2016

(for stat purpose)


---
**Tony Schelts** *January 22, 2016 09:22*

Thanks Stephane for all your help and Advice and for running this site.


---
**Stephane Buisson** *January 25, 2016 23:39*

community reach 900 members on 25/1/2016

(for stat purpose)﻿


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/E7bfGZF1Nwk) &mdash; content and formatting may not be reliable*
