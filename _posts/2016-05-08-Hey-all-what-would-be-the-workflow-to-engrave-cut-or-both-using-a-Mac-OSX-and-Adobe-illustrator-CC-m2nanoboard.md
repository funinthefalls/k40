---
layout: post
title: "Hey all, what would be the workflow to engrave/cut or both using a Mac/OSX and Adobe illustrator CC, m2nanoboard..."
date: May 08, 2016 02:00
category: "Discussion"
author: "Blake Ingram"
---
Hey all, what would be the workflow to engrave/cut or both using a Mac/OSX and Adobe illustrator CC, m2nanoboard... What are the steps (step by step) from a finished design to the laser engraving and cutting?



Can I vector engrave and cut or is better to use bitmap?



How do you designate the cut line with bitmaps?



What is the optimal dpi and file types you can send to the K40 w/ m2nanoboard?



Would it be better to buy a cheap $100. pc that runs windows 10 from walmart or better to spend the money on new board so I can run natively from my mac using the available open source software (I have very little to none electronics experience),





**"Blake Ingram"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 02:34*

Can't do it on OSX using the m2 board. If you switched to Smoothie with **+Peter van der Walt**'s LaserWeb it may be possible. Peter (or other LaserWeb users) would have to verify that.



What you could do is install Windows 10 using BootCamp. That is the method I currently am using for my Macbook. If you own a copy of Win10 (or can get the free upgrade) then I would suggest this route over purchasing another computer (although I intend to purchase something standalone to use with the k40 only, when I get around to it).



Workflow I currenly use is this... design in Adobe Illustrator. Save as SVG. Import into CorelDraw x5 with the CorelLaser plugin. Engrave or Cut. Done.



Previously I used the CorelDraw 12 that came with the K40. Workflow for that was similar, instead of saving as SVG, I would save as AI9 format.



Vectors are how it is used for lines in general. I'm not certain regarding raster cutting, but I imagine it is possible (just never tried). All engraves are done as Raster, however I have been experimenting with methods to do engraving via the Cut command using vectors (with reasonable success).


---
**Ariel Yahni (UniKpty)** *May 08, 2016 02:58*

 No issues with LaserWeb on OSX


---
**Vince Lee** *May 08, 2016 04:47*

You would probably in the long run be happier being able to work on a single machine in osx native but a conversion is absolutely not one I would recommend for anyone without electronics experience to start right away.  Perhaps soon there may be more plug and play solutions but now there is still a fair amount of soldering and debugging involved not to mention mods to mount everything adequately.  There are high voltages nearby too, so if u can do it with software that sounds like the way to go.


---
*Imported from [Google+](https://plus.google.com/101957775596878882298/posts/DGrEp6tDvMT) &mdash; content and formatting may not be reliable*
