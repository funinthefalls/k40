---
layout: post
title: "I have a smoothieboard 5x, but haven't started converting my k40"
date: January 21, 2017 07:03
category: "Smoothieboard Modification"
author: "Josh Speer"
---
I have a smoothieboard 5x, but haven't started converting my k40. I'm wondering if I should go the cohesion3d route, and throw the smoothieboard on ebay. Would that be a mistake?





**"Josh Speer"**

---
---
**Ariel Yahni (UniKpty)** *January 21, 2017 12:33*

I would say that a 5x its an overkill for a standard k40 user now that we have the cohesion3d mini


---
**Josh Speer** *January 21, 2017 16:13*

Thanks Ariel, that is what I'm thinking, and is not a plug and play setup


---
**Ariel Yahni (UniKpty)** *January 21, 2017 16:15*

**+Josh Speer** talk to **+Ray Kholodovsky** on regarding ordering a mini in case you want to order it.


---
**Josh Speer** *January 21, 2017 23:57*

Thanks Ariel, He has replied new stock won't be available until the end of February. I'll keep using corel draw for now.


---
*Imported from [Google+](https://plus.google.com/115420425425105333016/posts/KxgdcRE499K) &mdash; content and formatting may not be reliable*
