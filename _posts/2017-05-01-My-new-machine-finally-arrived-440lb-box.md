---
layout: post
title: "My new machine finally arrived. 440lb box..."
date: May 01, 2017 23:39
category: "Discussion"
author: "Nigel Conroy"
---
My new machine finally arrived.

440lb box... Had to remove door and frame from basement to get it in.... 



Built a ramp to eliminate steps, strapped it to car and used it to slowly lower machine down. 



Worked really well. Had two helpers and went without incident. 



![images/06aa8309f82d3497d2294d32bc48bec8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/06aa8309f82d3497d2294d32bc48bec8.jpeg)
![images/24e725bda7fbe7f7a6daf612d17917c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/24e725bda7fbe7f7a6daf612d17917c1.jpeg)
![images/54bf792e41a7e973d87ac48ac7b02a84.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/54bf792e41a7e973d87ac48ac7b02a84.jpeg)
![images/3eb8c41e9b0c5f3a22590bd779afeefa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3eb8c41e9b0c5f3a22590bd779afeefa.jpeg)

**"Nigel Conroy"**

---
---
**Ned Hill** *May 01, 2017 23:47*

Awesome!




---
**Cesar Tolentino** *May 02, 2017 00:30*

Let the fun begin


---
**3D Laser** *May 02, 2017 00:30*

Which laser was it 


---
**Nigel Conroy** *May 02, 2017 00:32*

The one I linked previously. 60w

I'll get pics of it when I get it unboxed. 


---
**A “alexxxhp” P** *May 02, 2017 01:17*

nice  I purchased similar one and added camera and auto focus on the machine.. upload picks. :)




---
**Steve Clark** *May 02, 2017 01:47*

New Project!!! Sweet!


---
**Nigel Conroy** *May 02, 2017 02:09*

Nice I'm interested in adding a camera.



Yes hopefully won't take me too long to get it set up and operational. 


---
**Nigel Conroy** *May 02, 2017 02:44*

990mm approx

![images/772eb84d614ae68dc761e0f7beca4eb3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/772eb84d614ae68dc761e0f7beca4eb3.jpeg)


---
**Nigel Conroy** *May 02, 2017 02:44*

![images/bb11029e65a69744229e5ac401f64b0d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb11029e65a69744229e5ac401f64b0d.jpeg)


---
**Nigel Conroy** *May 02, 2017 02:45*

![images/eb89b7b8d0c38d241e61ce4446f4aa87.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb89b7b8d0c38d241e61ce4446f4aa87.jpeg)


---
**3D Laser** *May 02, 2017 03:03*

**+Nigel Conroy** do you have experience with RD works or will you use coral draw?


---
**Nigel Conroy** *May 02, 2017 03:10*

**+Corey Budwine**​ I've been using RDworks mainly as last step in process. I use Corel draw for file conversion, trying to get better using the trace function.

I don't like the Laser plugin as it lacks some control.



I design or create things in sketchup or Google Drawing




---
**3D Laser** *May 02, 2017 03:13*

**+Nigel Conroy** how do you get the size to come out right when ever I try to import my size goes all wonky 


---
**Nigel Conroy** *May 02, 2017 03:19*

It might be down to your settings on the export file. 



But one trick is to add a line to the side that you know the length of, say 40mm

When you import check the size of the line and then adjust the entire project based on that.

Does that make sense?


---
**3D Laser** *May 02, 2017 03:27*

**+Nigel Conroy** yes kinda like making a key or legend on a map


---
**A “alexxxhp” P** *May 02, 2017 04:26*

nice


---
**Chris Hurley** *May 03, 2017 00:19*

and this is how you end up with evil labs in the suburbs.  


---
**Nigel Conroy** *May 03, 2017 02:33*

Blower motor has wrong plug

![images/a3aa48ae0d2b495b1e4f1cd8d8872c8e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3aa48ae0d2b495b1e4f1cd8d8872c8e.jpeg)


---
**Nigel Conroy** *May 03, 2017 02:34*

Also have an air bubble in laser tube. 

Is that ok or not?

I can't remember but know I've read about it previously.

![images/867602c8a29030cc90b2e464a4c2938a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/867602c8a29030cc90b2e464a4c2938a.jpeg)


---
**Chris Hurley** *May 03, 2017 02:37*

Its generally bad due to cooling issues. If you raise the coolant container up it should fix it. (Above the tube) 


---
**Nigel Conroy** *May 03, 2017 02:38*

That's what I thought. I above the tube and let it run for a bit. Thanks


---
**Nigel Conroy** *May 03, 2017 02:53*

Is it ok to pulse to test the laser?

Want to make sure everything is working


---
**Chris Hurley** *May 03, 2017 02:56*

I can't be sure. I don't understand the full effects of the bubble. 


---
**3D Laser** *May 03, 2017 02:59*

**+Nigel Conroy** you should be ok if you can lift on end of the machine up you can force it out sometimes 


---
**Nigel Conroy** *May 03, 2017 03:15*

Solved (YouTube)



Loosened tube and slightly rotated tube so outlet was above and bubbles went out.



I need to calibrate and align everything anyway so not that big a deal. 



Everything moved nicely and got a positive pulse from the tube.


---
**HalfNormal** *May 03, 2017 12:47*

You should have bought a new house to go along with the new laser instead of having to remove doors! ;-)


---
**Nigel Conroy** *May 03, 2017 14:34*

True **+HalfNormal**



My next concern is that the blower motor is actually a 220v blower.



The box had the 220v scored out! and the label is just a printed piece of paper stuck on.



The ebayer has instructed me to purchase a plug adapter and they'll refund me $5. 



On the box it has CZ-TD550/220V



Any of you more knowledgeable around electronics able to guide me on this?



From what I've read if it is 220 it will try and draw more amps and either trip the breaker or may overheat and be a fire hazard.




---
**Nigel Conroy** *May 03, 2017 14:35*

![images/f06dcdb86709d5bade78b3afaa4e7ae1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f06dcdb86709d5bade78b3afaa4e7ae1.jpeg)


---
**A “alexxxhp” P** *May 04, 2017 02:06*

I purchased a bouncy house blower fan less noise works great the one that came with my machine is like yours but 110v but it is loud.


---
**Nigel Conroy** *May 08, 2017 02:51*

Been working on alignment etc... Noticed the bed underneath the honeycomb.....



Removed the honeycomb which was held in place by 4 tapped screws and there's an adjustable blade bed underneath.



The fan isn't great. It is 110 volt with a Chinese plug. Travel adapter got it working and it shifts a lot of air. The empellor housing however isn't sealed so there is air blown out from the body of the fan and not just out the ducting.

![images/b143071b6620acc557d24fee738b2715.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b143071b6620acc557d24fee738b2715.jpeg)


---
**Nigel Conroy** *May 08, 2017 02:56*

So any input on when to use blades versus honeycomb?



Also there's no interlocks despite being advertised as having them.


---
**Phillip Conroy** *May 15, 2017 09:47*

Use blade bed if material you are cutting  is stiff,ie mdf ad use honeycomb for paper 


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/bwCH8c6a8tT) &mdash; content and formatting may not be reliable*
