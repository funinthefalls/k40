---
layout: post
title: "Hi Guys, I have some issues with my K40 laser cutter with the rounding of the edges"
date: September 04, 2017 20:53
category: "Original software and hardware issues"
author: "Jelke van der Sande"
---
Hi Guys, 



I have some issues with my K40 laser cutter with the rounding of the edges. As you can see on the picture the edges are not really sharp and round but a bit angular. I added a video to my post. When the laser is cutting the rounded edges it is make a strange noise. Maybe this is related to the angular corners?

It would be great if someone can help me with this!



Greetings,



Jelke



![images/90685bbbe13314db1b8ca612451ca4c8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90685bbbe13314db1b8ca612451ca4c8.jpeg)
![images/d8292e209188955a8cc8d643df2c8458.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d8292e209188955a8cc8d643df2c8458.jpeg)

**"Jelke van der Sande"**

---
---
**HP Persson** *September 05, 2017 06:54*

Share the source file, or any file with this problem. I think this is either a file or setting issue.




---
**Jelke van der Sande** *September 05, 2017 10:36*

**+HP Persson** Thanks for your reaction. I uploaded the source file here: [uploadfiles.io - Uploadfiles.io - laser_drawing.cdr](https://ufile.io/7qcyy) (made with coreldraw)


---
*Imported from [Google+](https://plus.google.com/117864305061411710736/posts/bKZ9rz4gzS6) &mdash; content and formatting may not be reliable*
