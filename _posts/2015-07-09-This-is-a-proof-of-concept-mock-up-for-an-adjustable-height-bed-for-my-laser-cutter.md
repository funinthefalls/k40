---
layout: post
title: "This is a proof of concept mock up for an adjustable height bed for my laser cutter"
date: July 09, 2015 21:42
category: "Modification"
author: "David Richards (djrm)"
---
This is a proof of concept mock up for an adjustable  height bed for my laser cutter. The idea will be to replace the perspex with the aluminium honeycomb. I would  like a motor or handle so it can be operated from outside too. I found the parts designs on thingiverse and printed them in pla on my Rostock.

![images/fffc6cd3783d44f17651612df3e80531.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fffc6cd3783d44f17651612df3e80531.jpeg)



**"David Richards (djrm)"**

---
---
**Fadi Kahhaleh** *July 10, 2015 05:44*

Nice idea... I am also working on a DIY design myself.

Would you please share the thingID on thingiverse that you printed.



Thanks


---
**Stephane Buisson** *July 10, 2015 11:34*

I do like the idea of a moving bed table with handle, and asking myself if a Z stepmotor will not be a bit overkill. it's a lot of work specially on the software side. I mean not the firmware, but a software making use of it , maybe for a use on unflat object with a Z sensor next to the head to keep the focal distance.

nice proof of concept by the way **+david richards** 


---
**David Richards (djrm)** *July 10, 2015 13:26*

Greetings, Here are the things I used to make this:

13733: universal horizontal bearing holder script- openscad

2087: Beaded belt gear

148764: Adjustable foot for Tables and Furniture

The other gear wheel for the idler I have lost, it wasn't very good anyway. hth David


---
**Fadi Kahhaleh** *July 10, 2015 21:34*

Thanks David, will take a closer look at those components.


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/eNCk9QGn3qD) &mdash; content and formatting may not be reliable*
