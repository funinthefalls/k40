---
layout: post
title: "Questions on how to use LaserWed for first time users Tonight I loaded LaserWeb.setup.4.0.68-728.x64exe file and installed it"
date: April 29, 2017 05:17
category: "Software"
author: "Steve Clark"
---
Questions on how to use LaserWed for first time users…



Tonight I loaded LaserWeb.setup.4.0.68-728.x64exe file and installed it. I was able to bring in a DXF file I’ve used in the past and looked at the G-code file. Now I have some questions.



First it looks like the G-code is absolute format and point to point in nature. Is this correct, as I did not see any G2 or G3’s  and the program has some arc’s in it? This could make for a challenge if I was to try importing off of my CADCAM package. Writing a proper post that is. 



Next I was unable to find how to do “simulate” without being hooked up to the laser.( I’m not hooked up on this computer to the laser and I just got messages telling me that was the case.)



So I guess I need some of the “for dummy’s” instructions – grin. 







**"Steve Clark"**

---
---
**Steve Clark** *April 29, 2017 07:07*

Peter, I still can't see anything happening and the blue dot flashes. Here is what I suspect. It is working... but I may have imported it in inches not mm. The part is pretty small, less than an inch so in the morning I'll create another DXF of a simple rectangle and import that making sure it is metric.



Is there a way to scale the screen view?






---
**Stephane Buisson** *April 29, 2017 09:02*

**+Peter van der Walt** "correct community" this is a good section for software question, so welcome here too. but I do agree it should be as well on G+ LW. (as info concentrator).


---
**Steve Clark** *May 06, 2017 19:13*

Peter, just got back to LaserWeb and worked the problems out . Very small objects in inches imported and not convert to metric make for objects not seen  with large screen size! Even when it is there! 


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/8SD13A1xa3A) &mdash; content and formatting may not be reliable*
