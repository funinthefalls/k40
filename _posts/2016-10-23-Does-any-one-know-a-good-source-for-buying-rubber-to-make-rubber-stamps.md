---
layout: post
title: "Does any one know a good source for buying rubber to make rubber stamps?"
date: October 23, 2016 01:23
category: "Material suppliers"
author: "Anthony Bolgar"
---
Does any one know a good source for buying rubber to make rubber stamps? All the ones I can find on ebay, etc, are too thin, they are only 2.3mm thick. To make a good stamp you need a minimum of 3mm but preferably 4-5mm.





**"Anthony Bolgar"**

---
---
**Ariel Yahni (UniKpty)** *October 23, 2016 01:36*

I looked even at craft store in the US without luck. Non of the ones I went had rubber at all, most had linolium


---
**Alex Krause** *October 23, 2016 01:53*

I have some red rubber I picked up at the hardware store I can give it a shot! It's found in the plumbing section


---
**Anthony Bolgar** *October 23, 2016 01:55*

Let me know if it works **+Alex Krause** . Sure hope it does.


---
**James Rivera** *October 23, 2016 07:55*

Haven't tried it, but maybe this will help?

[nettally.com - Rubber Stamp Materials](http://www.nettally.com/palmk/RubberStampMaterials.html)


---
**Anthony Bolgar** *October 23, 2016 10:09*

Thanks for the link **+James Rivera** 


---
**Martin Dillon** *January 16, 2017 20:16*

This is what I have used to make rubber stamps with carving tools.  Not sure how it would work with a laser.

[amazon.com - Amazon.com: Soft Kut Rubber Print Blocks Sold - 4 x 6: Industrial & Scientific](https://www.amazon.com/Soft-Rubber-Print-Blocks-Sold/dp/B0042SZ2B8)




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/ZqMQgh9jYgS) &mdash; content and formatting may not be reliable*
