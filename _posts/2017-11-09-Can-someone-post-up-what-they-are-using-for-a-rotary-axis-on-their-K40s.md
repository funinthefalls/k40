---
layout: post
title: "Can someone post-up what they are using for a rotary axis on their K40's?"
date: November 09, 2017 19:00
category: "Discussion"
author: "Michael Audette"
---
Can someone post-up what they are using for a rotary axis on their K40's?  Curious as to what people use and how they are using it. 





**"Michael Audette"**

---
---
**Mark Dolan** *November 09, 2017 21:21*

I use mine to do glasses and mugs all the time.  Works perfectly.


---
**Michael Audette** *November 09, 2017 21:22*

**+Mark Dolan** What exactly are you using ;)  I don't have one and am looking to build or buy one.




---
**Steve Clark** *November 09, 2017 23:26*

I haven't used one yet... too busy with other projects right now. But I want to build one of these to try for the fun of it.




{% include youtubePlayer.html id="-uafUAX04G4" %}
[https://www.youtube.com/watch?v=-uafUAX04G4](https://www.youtube.com/watch?v=-uafUAX04G4)




{% include youtubePlayer.html id="mCQqn0kfvCo" %}
[youtube.com - RDWorks Learning Lab 123 Let's Test our DIY Rotary Engraver](https://www.youtube.com/watch?v=mCQqn0kfvCo)




---
*Imported from [Google+](https://plus.google.com/102920303032765220786/posts/YDs6fMJPSMB) &mdash; content and formatting may not be reliable*
