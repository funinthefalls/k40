---
layout: post
title: "My K40 cuts everything twice - The supplier has not got a clue why?"
date: May 19, 2015 13:30
category: "Software"
author: "Myron Horwitz"
---
My K40 cuts everything twice - The supplier has not got a clue why?

Anyone any idea why ?

![images/f9316596de533215f7c1600eb791ffbb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9316596de533215f7c1600eb791ffbb.jpeg)



**"Myron Horwitz"**

---
---
**David Wakely** *May 19, 2015 15:03*

Hi there,



There is 2 things you can do: 



1) set the cutting data to hpgl by pressing the cog on the Corel plugin. Be aware this will not enable you to line up your cutting and engraving jobs



2) if you are using WMF as the cutting data you need to make all objects solid fill. See my post over at light object regarding this



[http://www.lightobject.info/viewtopic.php?f=70&t=2588](http://www.lightobject.info/viewtopic.php?f=70&t=2588)



Hope that helps!


---
**David Wakely** *May 19, 2015 15:06*

Also it's not cutting the same line twice it's actually cutting on either side of the drawn line but as these are so close together it looks like the same line. I had this issue when I first got my machine


---
**Imko Beckhoven van** *May 19, 2015 17:38*

Try to change youre line width to a hairline in corel. 


---
**Eric Parker** *May 20, 2015 20:07*

A couple of observations, first being that the repeat function is for doing the same job over and over; it prompts you for the next one. Think of it as a "How many times to cut this job" counter.

 Second, make your rectange solid. as others have said, it's trying to cut both sides of that line.


---
**Mando Mgn** *June 13, 2015 05:58*

Had the same issue,  the object to cut must be completely black.  If not you are cutting I net and out perimeter 


---
**Clifford Livesey** *August 09, 2015 19:20*

Hairline is to thick. Change it to 0.001


---
*Imported from [Google+](https://plus.google.com/103174548266771862885/posts/Xu4KePEmcyw) &mdash; content and formatting may not be reliable*
