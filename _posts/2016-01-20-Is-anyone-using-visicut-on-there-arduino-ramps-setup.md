---
layout: post
title: "Is anyone using visicut on there arduino/ramps setup?"
date: January 20, 2016 22:38
category: "Discussion"
author: "Pedro Flores"
---
Is anyone using visicut on there arduino/ramps setup? I can't get it to fire the laser?

I have the latest development of visicut. Windows 7 using usb. Latest version of turnkey.





**"Pedro Flores"**

---
---
**ChiRag Chaudhari** *January 21, 2016 21:21*

I tried it once, actually twice with mac and pc both. but for some reason I could not get it to recognize the board. So I gave up. I use LaserWeb now.


---
**Pedro Flores** *January 22, 2016 01:00*

Another problem I had with it was that I had to remove the SD card and reinsert it back for it to send the job. So it partially works excepts for the laser not firing.




---
*Imported from [Google+](https://plus.google.com/116137110819103096995/posts/XzoisEYaJ5r) &mdash; content and formatting may not be reliable*
