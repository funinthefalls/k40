---
layout: post
title: "My k50 or sh 350 laser stopped firing,I still have my trusty k40 and will sit the k40 on top of the other laser cutter and wire up the 50 watt tube to the k50 to test if it fires ,if it does the powe're supply is faulty on"
date: May 22, 2017 07:40
category: "Discussion"
author: "Phillip Conroy"
---
My k50 or sh 350 laser stopped firing,I still have my trusty k40 and will sit the k40 on top of the other laser cutter and wire up the 50 watt tube to the k50 to test if it fires ,if it does the powe're  supply is faulty on the k50.has anyone wired a k40 40 watt power supply up to a k 50 50 watt machine?

. If it does not fire will wire up the k50 power supply to the 40 watt k40 laser tube,then if it doesn't fire it is something else .

I suspect the power supply as water temp and flow where ok and it just stopped firing 1/2 way through a small etching job

the last time tube was faulty I just lost most cutting power slowly over 3 days and not straight up,checked tube for cracks and seams ok . Try ed test button power supply and did not fire,amp meter i showing 0 ma, can not here any arcing 





**"Phillip Conroy"**

---
---
**Don Kleinschnitz Jr.** *May 22, 2017 11:16*

Swapping the supply's from a 50-40 and visa versa should work for a test but not for any length of time. I have not tried it.



Why put one machine on top the other rather than swap the supply's? You have to disconnect the anode in either case anyway.



Be very careful the LPS creates lethal voltages and running the anode wire any distance can be dangerous.






---
**Phillip Conroy** *May 22, 2017 11:21*

I was a electronic s tech for 10 years and have worked on hi voltages before up to 80,000 on a radar system,will concept bother as earth's toether and not touch metal drug test firing,I will also discharge any hi voltages with a chicken stick that shorts the chrge to earth


---
**Don Kleinschnitz Jr.** *May 22, 2017 12:10*

**+Phillip Conroy** Ok you seem to have the experience but still be careful and practice HV troubleshooting safety techniques.



You could also test the LPS while still in the dead machine by allowing it to arc to ground.



I just helped David see his video:

[plus.google.com - Today I went to test firing the laser and aligning the mirrors but found that...](https://plus.google.com/107911974344505492651/posts/DFo5xkkheA8)



BTW you do this at your own risk!



I recommend taping the ground lead to the tube near the anode and after isolating yourself from the machine, press the test to see if it arc's. Pulse the test button and do not hold it as you may damage the supply if it arcs to long.


---
**Phillip Conroy** *May 23, 2017 03:20*

 Was hi voltage transformer,soldered in spare k40 one ,all good ,by the looks of it everything inside the k50 is the same as the k40.k50 machine total on hours was 367 and laser had fired for a total of 71 hours.I love the k50 / sh 350 control panel and controler so many settings and machine info


---
**Phillip Conroy** *May 23, 2017 05:26*

Only one wire to unsolder and 2 screws,hot glued the terminals on ,total cost $29 au ,ordered 2 spares 


---
**Don Kleinschnitz Jr.** *May 23, 2017 14:01*

**+Phillip Conroy** 

1. Did you try the arc test if not how did you find that flyback was bad?

2. Isn't the flyback in a 50W a different flyback than a 40W? It is marked on the flyback?



I am looking for a dead flyback that I can de-pot so that we can learn what is inside. If you are willing to donate to the HV lab that would be helpful :). Not sure where you live I am in Utah:US.


---
**Phillip Conroy** *May 23, 2017 19:22*

No I didn't do the arc test,I fired the laser with another powersupply and it worked,then I removed the flyback transformer and measured the resistance, bad one was 0.9 ohms and good spare was 0.3 ohms ,then I installed the new one and fitted to machine and fired the laser.both flyback transformers where marked with 40 and a couple of letters.both power supply looked identital  inside.not happy that the powersupply had only fired the laser for a total of 70 hours ,could have been just a dud from new,will wait and see.I have a hi voltage probe for my multimeter however in this case u

I just didn't need to use it.I am in Australia and have 2 dead flyback transformers I can send you if you supply an address. 

If you look at some of the larger wattage power supply you will see a tube rating range ie 90 to 120 watts .the flyback is easy and fast to chan over with only the 2 tube wires and 2 nuts ,with only one wire to solder the power supply only fires the laser and doesn't supply anything else on the sh 350  lasr machine.x y moters and the control board is powered by a separate 24 volt power supply.


---
**Phillip Conroy** *May 23, 2017 19:38*

Even tho the sh 350 is rated at 50 watts the tube length is only 800mm long x 50mm high and measures 45 watts with my laser beam power probe . I belive these tubes are only 45 watts and the k40 tubes at 700mm long are only 35 watts . I only ever drive my tubes at 16 ma and hatenot knowing what a laser machine % is [ma.no - Markeds assistansen - din hjelp til bedre resultater !](http://ma.no) tube maker rates their tubes in%  so why have laser machine makers forced everyone to aftermarket fit a meter to measure power,a step [backward.in](http://backward.in) my case 100% is 25ma so they have at least set up the power supply correctly . My  sh 350 will cut a straight line all the way through at 16ma and a speed of 20mm/second in 2.7 mm ply ,where my k40 would ever only cut the same ply at 16 ma and a speed of 12 mm/second. 


---
**Don Kleinschnitz Jr.** *May 24, 2017 12:20*

**+Phillip Conroy** i guess if the length is shorter then the smaller flyback is ok. The LPS is sized by length of tube. It does make me wonder since its life was so short.

Was the measurement you did on the primary or secondary of the flyback. Strange that the good one read higher resistance by .6 ohms So the bad one had a short in some windings perhaps?



I will send you a PM with my address, many thanks as I will un-pot the xformer and dissect it for all to see. Hopefully it does not cost more to ship than a new one ?


---
**Don Kleinschnitz Jr.** *June 02, 2017 18:29*

**+Phillip Conroy** got the transformers,,,,, many many thanks. All the way from Australia to Utah! I will test them with my prototype tester, then unpot and unassemble one. Report forthcoming. 


---
**Phillip Conroy** *June 02, 2017 19:14*

You are welcome,a few days before the k50 transformer blow it was making a strange hissing noise  every second cut  


---
**Don Kleinschnitz Jr.** *June 02, 2017 23:39*

**+Phillip Conroy** 

Both transformers passed the 9V battery test. As I suspected that is not a good test.

One xformer passed the driver test with good spark and the other output no spark. I am still not sure about this tester but the good ones I have act the same.



Any chance one of these was good?






---
**Don Kleinschnitz Jr.** *June 03, 2017 03:32*

**+Phillip Conroy** 

![images/34f9b58df56a97eab6a803cd2f127907](https://gitlab.com/funinthefalls/k40/raw/master/images/34f9b58df56a97eab6a803cd2f127907)


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/C32a3vbZKsQ) &mdash; content and formatting may not be reliable*
