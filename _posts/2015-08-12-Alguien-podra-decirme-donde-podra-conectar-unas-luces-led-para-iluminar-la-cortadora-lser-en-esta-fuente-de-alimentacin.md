---
layout: post
title: "Alguien podra decirme donde podra conectar unas luces led para iluminar la cortadora lser en esta fuente de alimentacin?"
date: August 12, 2015 20:38
category: "Modification"
author: "Al Tamo"
---
Alguien podría decirme donde podría conectar unas luces led para iluminar la cortadora láser en esta fuente de alimentación? 

![images/2a9b1e5367716f86ced94b3ce0a1e597.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2a9b1e5367716f86ced94b3ce0a1e597.jpeg)



**"Al Tamo"**

---
---
**Joey Fitzpatrick** *August 12, 2015 22:57*

It depends on if your LED's are supposed to run on 12v or 5v.  There is no 12v line on this power supply(only 5v and 24v as far as I know)  If they are 5v LED's /strips, The Purple or Violet wires should be 5v (use a volt meter to verify first).  You may want to just use a cheap wall wart power supply from an old phone or something else.  This would isolate the LED's from the power supply and main board (in case of some type of problem or short)


---
**Al Tamo** *August 13, 2015 04:38*

Es de 12v, así que tratare de localizar la de algún teléfono antiguo. Muchísimas gracias por contestar siempre tan rápido. 


---
*Imported from [Google+](https://plus.google.com/112166512335650872249/posts/DJ2G1M6csHV) &mdash; content and formatting may not be reliable*
