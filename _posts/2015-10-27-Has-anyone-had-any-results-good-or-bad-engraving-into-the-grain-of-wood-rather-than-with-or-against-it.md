---
layout: post
title: "Has anyone had any results, good or bad, engraving /into/ the grain of wood rather than with or against it?"
date: October 27, 2015 10:11
category: "Discussion"
author: "Peter"
---
Has anyone had any results, good or bad, engraving /into/ the grain of wood rather than with or against it? Seen some boards made up of seemingly odd bits pressed together but side to side leaving the end of the grain exposed. I wondered if it would produce a more constant colour to the wood instead of going over the hard and soft bits of a regular engraving process.





**"Peter"**

---
---
**Coherent** *October 27, 2015 18:37*

Not sure if I understand exactly what your asking, but it would seem to me that even manufactured boards like plys and MDF etc still are made of real wood (parts pieces and fibers etc) and the nature of wood being what it is means some bits are going to be denser etc so will burn differently. I've seen the most consistent engravings in boards like MDF that are basically ground wood and fibers mixed with a binder and pressed. The smaller and uniform the fibers (or the grain of real wood) the better. Some woods (balsa and basswood come to mind) are a couple with fairly consistent grain and density. I'm sure someone out there has done some testing!.


---
*Imported from [Google+](https://plus.google.com/+PeterJones79/posts/THGLsdXsKCW) &mdash; content and formatting may not be reliable*
