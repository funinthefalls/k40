---
layout: post
title: "Finally put together my first laser machine project it's been slightly stressful but here it is If you love the simpsons your love this project"
date: November 02, 2016 15:08
category: "Discussion"
author: "Rob Turner"
---
Finally put together my first laser machine project it's been slightly stressful but here it is 



If you love the simpsons your love this project 

![images/65a40101ae845e8c1318dc304b64914a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65a40101ae845e8c1318dc304b64914a.jpeg)



**"Rob Turner"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 02, 2016 15:21*

That's cool :)


---
**greg greene** *November 02, 2016 15:31*

Which room is Bart in?


---
**Jeff Johnson** *November 02, 2016 16:09*

This is great!


---
**Don Kleinschnitz Jr.** *November 02, 2016 16:43*

Sweet!


---
**Peter Jacobsen** *November 03, 2016 08:29*

Looks awesome :-)


---
*Imported from [Google+](https://plus.google.com/114544098138525257938/posts/A3hhgGtsTiX) &mdash; content and formatting may not be reliable*
