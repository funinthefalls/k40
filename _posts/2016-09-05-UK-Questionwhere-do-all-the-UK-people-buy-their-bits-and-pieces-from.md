---
layout: post
title: "UK Question...where do all the UK people buy their bits and pieces from?"
date: September 05, 2016 14:06
category: "Air Assist"
author: "Darren Steele"
---
UK Question...where do all the UK people buy their bits and pieces from?  I have stuff in my basket at [lightobject.com](http://lightobject.com) but I'm sure there must be European alternatives.  Any suggestions?





**"Darren Steele"**

---
---
**Norman Kirby** *September 05, 2016 21:54*

good question, something I was thinking about also but never got round to asking




---
**Scott Marshall** *September 06, 2016 12:34*

I ship to the UK, I've got one going to Broughshane today.



Websight is [all-teksystems.com - all-tek-systems](http://ALL-TEKSYSTEMS.com)

Right now we're selling mostly aftermarket controller installation kits, but there's a lot of good stuff coming. We have in stock replacement and upgraded K40 power supplies, cables, switck kits etc. More will be added to the site in the next 2 weeks or so.



A good source for optics and K40 mechanical parts is Saite Cutter on Ebay.


---
*Imported from [Google+](https://plus.google.com/+DarrenSteele/posts/dkgBPBchPcT) &mdash; content and formatting may not be reliable*
