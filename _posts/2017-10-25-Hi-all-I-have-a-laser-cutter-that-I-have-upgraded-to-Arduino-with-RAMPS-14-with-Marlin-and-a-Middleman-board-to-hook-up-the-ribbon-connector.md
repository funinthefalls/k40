---
layout: post
title: "Hi all, I have a laser cutter that I have upgraded to Arduino with RAMPS 1.4 with Marlin and a Middleman board to hook up the ribbon connector"
date: October 25, 2017 17:25
category: "Modification"
author: "Vinny Costello"
---
Hi all, I have a laser cutter that I have upgraded to Arduino with RAMPS 1.4 with Marlin and a Middleman board to hook up the ribbon connector. However, I am having a number of issues. Although I have engraved some g-code, all the steppers seem ok and the laser is firing when it should, it is not homing correctly and I don't think the X endstop is working as it hits the end and keeps juddering. Is there a comprehensive guide to this upgrade anywhere? Any help would be most appreciated. Thanks in advance. 



P.S. If there is a better solution to Marlin I would love to hear about that too. I attempted putting GRBL on but I hit a wall due to being a newb on Arduino! So a simple but thorough guide (if that makes sense), would be of great help to me. 

 Vinny





**"Vinny Costello"**

---
---
**Don Kleinschnitz Jr.** *October 25, 2017 18:31*

Check on the Cohesion 3D board. Your x optical ensstop sensor may have failed.

You can get schematics for to the K40 on my blog which include the endstops. See the resources pointed to on top of communities page. 


---
**BEN 3D** *October 25, 2017 20:56*

I had have the same on my device, after I took a look to dons page I solved the Issue easy. There is a little part of metal that goes into the optical sensor and brake a led diode connection. In my case the metal was bented and does not hit the center of the sensor in the correct way. Take look to all comments here in the category Issues with on stock Controler. There is one some weeks ago with the same Problem.





Edit: [https://plus.google.com/+DonKleinschnitz/posts/KZpTBq7k8v8](https://plus.google.com/+DonKleinschnitz/posts/KZpTBq7k8v8)



Edit2: [http://donsthings.blogspot.de/2017/08/repairing-k40-optical-endstops.html?m=1](http://donsthings.blogspot.de/2017/08/repairing-k40-optical-endstops.html?m=1)


---
*Imported from [Google+](https://plus.google.com/112580098952714247889/posts/8k3jezJn3j1) &mdash; content and formatting may not be reliable*
