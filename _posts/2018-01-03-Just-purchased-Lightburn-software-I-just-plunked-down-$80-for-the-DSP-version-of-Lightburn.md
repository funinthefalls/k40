---
layout: post
title: "Just purchased Lightburn software. I just plunked down $80 for the DSP version of Lightburn"
date: January 03, 2018 16:56
category: "Software"
author: "HalfNormal"
---
Just purchased Lightburn software.



I just plunked down $80 for the DSP version of Lightburn. I am a true believer that you get what you pay for! Looking forward to using software and showing off what it can do.





**"HalfNormal"**

---
---
**Ariel Yahni (UniKpty)** *January 03, 2018 18:09*

Money well spent


---
**Anthony Bolgar** *January 04, 2018 02:20*

It is definately great value for the money. Yhe developer is a brilliant guy, he used to design video games so everything he does is very fast and efficient. And he is very proactive on issues that have come up since the release on January 1st.


---
**tyler hallberg** *January 04, 2018 05:05*

Im with everyone here. You wont regret it. Best software I have used.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/7Q2Bt129Vtw) &mdash; content and formatting may not be reliable*
