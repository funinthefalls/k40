---
layout: post
title: "What products are people for masking whan lasering"
date: November 03, 2016 12:55
category: "Discussion"
author: "Tony Schelts"
---
What products are people for masking whan lasering.  Preferrably products easily purchased in the UK





**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 03, 2016 15:37*

If I mask at all, I just use painter's masking tape. Low-tack variety. I believe it's a 3M product (if I recall correctly). It's appox 2" (50mm) wide, so needs to be added in multiple strips.


---
**Tony Schelts** *November 03, 2016 17:31*

Thanks Yuusuf


---
**Bill Keeter** *November 03, 2016 19:31*

You can also use the tacky paper used with vinyl cutters. Not sure what the name is. 


---
**Anthony Bolgar** *November 03, 2016 20:39*

It is called transfer mask. It is what I use (I have a 42" vinyl plotter, so I always have some laying around.)


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/TXwQLT7N2jy) &mdash; content and formatting may not be reliable*
