---
layout: post
title: "Poor Man's Air Assist...harvested an old vacuum cleaner motor, built a cheap wooden box and sound insulated it with leftover Styrofoam"
date: December 06, 2016 00:09
category: "Modification"
author: "Mike Meyer"
---
Poor Man's Air Assist...harvested an old vacuum cleaner motor, built a cheap wooden box and sound insulated it with leftover Styrofoam. I run it in series with the stock exhaust fan out the garage door...man, this thing really pulls the CFMs away the beam. 



![images/3eeca2b9917a45eee863e138ce1ccbc0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3eeca2b9917a45eee863e138ce1ccbc0.jpeg)
![images/aa49c4d77e086b71671513919da6f23c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa49c4d77e086b71671513919da6f23c.jpeg)

**"Mike Meyer"**

---
---
**K** *December 06, 2016 00:12*

Very inventive!


---
**Don Kleinschnitz Jr.** *December 06, 2016 00:27*

I tried this with a shop vac, it worked well but was loud. I was warned that the residue from cutting would destroy the motor. Don't know if that is true or not?


---
**Mike Meyer** *December 06, 2016 00:34*

Yep; that's why I sound insulated it. Besides, it sticks about 6' out the door. Don't really care if I blow up the motor; I got it out the trash. 

    And thanks Kim!


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/QtmeqP4avNU) &mdash; content and formatting may not be reliable*
