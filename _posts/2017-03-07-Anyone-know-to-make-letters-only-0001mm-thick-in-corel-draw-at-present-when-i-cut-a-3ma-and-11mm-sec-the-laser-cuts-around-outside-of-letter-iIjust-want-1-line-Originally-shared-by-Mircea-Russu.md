---
layout: post
title: "Anyone know to make letters only 0.001mm thick in corel draw .at present when i cut a 3ma and 11mm sec the laser cuts around outside of letter ,iIjust want 1 line Originally shared by Mircea Russu"
date: March 07, 2017 08:17
category: "Discussion"
author: "Phillip Conroy"
---
Anyone know to make letters only 0.001mm thick in corel draw .at present when i cut a 3ma and 11mm sec the laser cuts around outside of letter ,iIjust want 1 line 



<b>Originally shared by Mircea Russu</b>



![images/99e3bbdbb773ab4453082045ed31ffd7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/99e3bbdbb773ab4453082045ed31ffd7.jpeg)

**"Phillip Conroy"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 07, 2017 15:35*

Can you type 0.001mm into the line width box? Might have to adjust the preferences for CorelDraw in relation to how many decimal points of precision to use.



Aside from that, I wonder what your Cutting Data & Engraving Data settings are for CorelLaser plugin? I had both mine set to WMF & was able to use 0.1mm line width & only get a single cut without it ever failing. Can't test anymore though since I've upgraded controller.


---
**Maker Cut** *March 08, 2017 22:51*

Make sure that they are set as curves not text. Look in the "objects" menu for 'Convert to Curves'. Make sure that 'Join Curves' is checked as well.


---
**Lucian Depold** *March 14, 2017 08:43*

where can i find more information on this "mod" ??


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/hVhK2oZSTPP) &mdash; content and formatting may not be reliable*
