---
layout: post
title: "Is the \"Getting Started\" post missing from the top of everyone's community view or is it just special treatment of me :)!"
date: January 28, 2018 12:44
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
Is the "Getting Started" post missing from the top of everyone's community view or is it just special treatment of me :)!



This post is supposed to be pinned at the top:



[https://plus.google.com/u/0/+DonKleinschnitz/posts/JTjvo89qJEP](https://plus.google.com/u/0/+DonKleinschnitz/posts/JTjvo89qJEP)





**"Don Kleinschnitz Jr."**

---
---
**Stephane Buisson** *January 28, 2018 13:08*

**+Don Kleinschnitz** nope, after a certain amount of time the pin drop. feel free to repost (a new one) and pin again.

thank you Don.


---
**Don Kleinschnitz Jr.** *January 28, 2018 13:50*

Strangely it is pinned on my mobile?

Also when I go to the post it does not list the "Pin to community" option like it is already pinned??


---
**Don Kleinschnitz Jr.** *January 28, 2018 14:02*

Re-posted ... I think its a G+ bug!


---
**HalfNormal** *January 29, 2018 15:15*

That's not a bug, it's a feature!


---
**Don Kleinschnitz Jr.** *January 29, 2018 15:42*

**+HalfNormal** well its at least a buggy feature!


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/DZ6pfuJZwBf) &mdash; content and formatting may not be reliable*
