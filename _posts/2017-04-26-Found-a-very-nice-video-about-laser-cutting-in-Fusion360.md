---
layout: post
title: "Found a very nice video about laser cutting in Fusion360"
date: April 26, 2017 08:58
category: "Software"
author: "Frank Herrmann"
---
Found a very nice video about laser cutting in Fusion360. Test it and found new postprocessors for GRBL and Smoothie for lasers.


{% include youtubePlayer.html id="c064tN3D6wE" %}
[https://www.youtube.com/watch?v=c064tN3D6wE](https://www.youtube.com/watch?v=c064tN3D6wE)

[http://cam.autodesk.com/posts/?p=grbl_laser](http://cam.autodesk.com/posts/?p=grbl_laser)

[http://cam.autodesk.com/posts/?p=smoothie](http://cam.autodesk.com/posts/?p=smoothie)







**"Frank Herrmann"**

---
---
**Anthony Bolgar** *April 26, 2017 10:17*

Thanks for sharing Frank.


---
*Imported from [Google+](https://plus.google.com/+FrankHerrmann1967/posts/cbiNYqgYQGM) &mdash; content and formatting may not be reliable*
