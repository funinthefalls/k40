---
layout: post
title: "Don't know if it will be any good but this is the new cutting bed i made i just have to put some bolts in the corner for legs"
date: January 31, 2016 09:18
category: "Modification"
author: "beny fits"
---
Don't know if it will be any good but this is the new cutting bed i made i just have to put some bolts in the corner for legs 



![images/dc80886bb77529e5bd8d04a4cd124223.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc80886bb77529e5bd8d04a4cd124223.jpeg)
![images/6d76675969342cc103b70e77faa92847.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d76675969342cc103b70e77faa92847.jpeg)
![images/b5311d863e9d8e434fd5f903f892cffc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b5311d863e9d8e434fd5f903f892cffc.jpeg)
![images/4780d9212faa2c4e48d1eea0d28449be.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4780d9212faa2c4e48d1eea0d28449be.jpeg)

**"beny fits"**

---
---
**ChiRag Chaudhari** *January 31, 2016 18:10*

This is looking good man.


---
**Jerry Rodberg** *January 31, 2016 20:09*

Is that all galvanized steel?  I'd be careful about the fumes you'll generate from pass through if so.  It won't be as much as if you were trying to cut it, but there will still be some.  [http://www.laserworx.com.au/laser-cut-galvanized-steel/](http://www.laserworx.com.au/laser-cut-galvanized-steel/)


---
**beny fits** *January 31, 2016 20:37*

Yes it is galvanized 


---
**I Laser** *February 02, 2016 04:18*

Just wondering how are you planning on getting that bed to the right height.


---
**beny fits** *February 02, 2016 08:09*

I'll be posting pix for that soon


---
**I Laser** *February 02, 2016 09:06*

By the looks of it, your bed is still a fixed height. So unless you plan on burning material of the same height, you'll most likely need to raise/lower the bed.



Check out the adjustable Z tables. ;) Maybe you can mod those bolts your using for legs?


---
**beny fits** *February 02, 2016 09:16*

Its just sitting in their so its easy to get out if i need to but at the moment im just looking at doing the same stuff i was before i have been looking at some z axis tables and how to make tham i may play around with it 


---
**Justin Nguyen** *February 07, 2016 22:52*

what is the size of the bed that you are making ?


---
**beny fits** *February 07, 2016 23:56*

I don't know the size i just used the string and made it based on the size of the original bed but when I went to put it in i had to take 1 bit off to make it fit 

I'll try to find out the size next time im in the office 


---
**beny fits** *February 09, 2016 00:05*

**+Dong Nguyen** 35cm x 30cm is the bed size


---
**Justin Nguyen** *February 29, 2016 16:41*

Thanks **+beny fits**


---
*Imported from [Google+](https://plus.google.com/116814442468846937788/posts/EvLFBVycypk) &mdash; content and formatting may not be reliable*
