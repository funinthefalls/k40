---
layout: post
title: "Easy mask removal with duct tape ..."
date: October 26, 2016 00:51
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
Easy mask removal with duct tape ... Remember, low tack tape on the piece, high tack tape to remove it.



![images/b20a538ba63f66c7638fe9fcc375b488.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b20a538ba63f66c7638fe9fcc375b488.jpeg)
![images/e255d1ff3a5f1c70d027da9d4aac9d5c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e255d1ff3a5f1c70d027da9d4aac9d5c.jpeg)
![images/c27664ded7ca8ff1cedf3ab3508b85ad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c27664ded7ca8ff1cedf3ab3508b85ad.jpeg)
![images/e5d002d2a829fabf5c8ce8b3bfebcd77.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e5d002d2a829fabf5c8ce8b3bfebcd77.jpeg)

**"Ashley M. Kirchner [Norym]"**

---
---
**Tony Schelts** *October 26, 2016 06:36*

What are you laser cutting.  Did you put the design on, and how.?


---
**Ashley M. Kirchner [Norym]** *October 26, 2016 06:56*

I'm cutting exactly what you are seeing there. Sorry, I can't tell you what they are yet, you'll just have to guess. The design was engraved on, through the mask, then air brushed.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2016 07:10*

Very nice. Love the airbrushing.


---
**Tony Schelts** *October 26, 2016 07:14*

very nice. I make tea light holders and boxes etc. I have an airbrush that i used for air assist that is no longer needed. what paint would you use on ply. they look like coffee cup holders.  I like your work Thanks


---
**Tony Schelts** *October 26, 2016 07:15*

where you from Ashley.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2016 08:06*

**+Tony Schelts** Personally I would use acrylic for anything with flex in the final product, as acrylic can bend slightly without cracking.


---
**Tony Schelts** *October 26, 2016 10:15*

Thanks


---
**Ashley M. Kirchner [Norym]** *October 26, 2016 17:36*

Acrylic paints definitely works. I try to stay environmentally friendly with what I make and, at the moment, use a product made by Plaid, it's their Delta Soy paint. However I'm realizing very quickly that these are very hard to find. Stores are always out of stock and they just don't seem to keep any kind of stock available anywhere, not in stores, not online, not even their own web site. So I started looking for alternatives ...



But the reality is, any paint suitable for air brushing would work. It has to be thin enough to work.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/jfUabXcgKLH) &mdash; content and formatting may not be reliable*
