---
layout: post
title: "Arthur Wolf couple of verification/ questions came up while I am investigating the laser power interface to the K40"
date: June 13, 2016 02:51
category: "Modification"
author: "Don Kleinschnitz Jr."
---
**+Arthur Wolf** couple of verification/ questions came up while I am investigating the laser power interface to the K40.



Regarding the PWM capability:

How would the laser power (duty cycle) be modified during operation. I don't see a G or M code that would do that. I do see how the max's and freq are configured.



From the schematic I read that PWM1 is high true into Q6 which means that it will be inverted in an open drain configuration, right?﻿





**"Don Kleinschnitz Jr."**

---
---
**Arthur Wolf** *June 13, 2016 07:59*

Hey.



You set max pwm power using G1 Sx where X is 0 to 1.

Once that's set, it'll also automatically adjust power proportional to speed during acceleration/deceleration to make sure everything is clean.



Also yes I do believe opendrain makes it inverted, so you should invert the pin ( by adding « ! » )


---
**Don Kleinschnitz Jr.** *June 13, 2016 11:00*

**+Arthur Wolf** Thanks .....


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/N4q4RDMMnTu) &mdash; content and formatting may not be reliable*
