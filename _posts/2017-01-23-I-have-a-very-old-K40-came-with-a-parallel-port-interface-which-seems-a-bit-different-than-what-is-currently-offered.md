---
layout: post
title: "I have a very old K40 (came with a parallel port interface) which seems a bit different than what is currently offered"
date: January 23, 2017 19:57
category: "Smoothieboard Modification"
author: "SteveCiciora"
---
I have a <i>very</i> old K40 (came with a parallel port interface) which seems a bit different than what is currently offered.  I'm retrofitting it with a  Cohesion3d board and am a bit confused.  Where does the current machines home to?  On mine it's the far back right hand corner.  I'm hoping the newer ones home to the front left hand side because that would mean my understanding is correct.

Thanks,

- Steve





**"SteveCiciora"**

---
---
**Ned Hill** *January 23, 2017 20:03*

Mine, and I presume most others, home to the back left.


---
**greg greene** *January 23, 2017 20:13*

And that will cause some confusion as it usually is far back it puts the beam over the metal exhaust duct. LaserWeb wants you home to the front left - so when you tell it to home there, the laser will try to move even further back to cut - which it can't do mechanically.  

I just manually set the head at the bottom left - or jog it there and hit the set zero button.  Now the laser - and LaserWeb are in sync as to where to start from.


---
**Glenn Nanney** *January 23, 2017 21:27*

There is a nice discussion of this by the maintainer of LaserWeb at this link: [github.com - Origin location · Issue #103 · LaserWeb/deprecated-LaserWeb1](https://github.com/LaserWeb/deprecated-LaserWeb1/issues/103)



Obvi, it is related to LaserWeb1 but the conversation still applies.  Bottom left should be 0,0 and you can set the smoothie config to home to wherever you have your limit switches. 


---
**SteveCiciora** *January 24, 2017 15:50*

Thanks everyone, this is exactly what I was looking for.  Was hoping to try modifying the config file in Smoothie last night but no time.  Will try tonight.


---
*Imported from [Google+](https://plus.google.com/104905660528832840838/posts/dzQ6upYQ3pt) &mdash; content and formatting may not be reliable*
