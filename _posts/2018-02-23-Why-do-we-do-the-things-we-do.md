---
layout: post
title: "Why do we do the things we do????"
date: February 23, 2018 19:48
category: "Object produced with laser"
author: "Andy Shilling"
---
Why do we do the things we do????



Because we can.



6mm acrylic cut at 15mAh 350mm/m  in 2 passes.

Engraved square cut out @ 10mAh 3500mm/m and then joined it all together with making tape followed by ema plastic weld solvent. I just need to mastic the inside to get it completely air tight then the fun can begin.



![images/47f9f4238598ef06af35c86cd0a9c203.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/47f9f4238598ef06af35c86cd0a9c203.jpeg)
![images/5da8586889aee44ea17cfdab76b5377b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5da8586889aee44ea17cfdab76b5377b.jpeg)

**"Andy Shilling"**

---
---
**James Rivera** *February 23, 2018 21:23*

Nice. What is it for?


---
**Andy Shilling** *February 23, 2018 21:32*

It's a vacuum chamber,  connect it up to an old fridge compressor and it will suck all the air out and give some fun results with what you put inside. Check out you tube the are loads of videos.



My son and I like to make so fun things every so often and this was the latest idea 😉


---
**Don Kleinschnitz Jr.** *February 24, 2018 00:01*

**+Andy Shilling** not meaning to be a buzz kill. Be very careful. When you consider the large surface area it doesn't take much static pressure to fracture acrylic. When it does it is a memorable event. 



I built a 1/4 thick acrylic box and pressurized it with only 30PSI at which point it exploded ripping a hole in the middle (not the seams) of the box's walls. The 6" diameter shard passed by me before impaling itself on the shop wall. If I had been in front of it I would have a serious injury or death.

Vacuum likely produces different results than pressure but be careful.


---
**Abe Fouhy** *February 24, 2018 07:22*

Wow! That is a cautionary tale! Thanks for the heads up Don!

As long as its safe, add some water mixers to your chamber with a water & air pump to make snow in a box at ambient temp!


---
**Andy Shilling** *February 24, 2018 07:25*

**+Don Kleinschnitz** thanks for the heads up, I was wondering about fractures etc, I looked at a few DIY versions and found some where using what seems like the glass dome from an old carriage clock.



I presumed acrylic would be stronger but first test will now be happening via remote viewing just to be safe.


---
**Andy Shilling** *February 24, 2018 07:28*

**+Abe Fouhy** what do you mean by water mixers? Something like an atomiser spray bottle your thing?


---
**Jeremie Francois** *February 24, 2018 09:19*

I would strongly back **+Don Kleinschnitz** advice by, e.g. covering the surfaces with adhesive tape to help holding broken bits together, and to dissipate energy if/when it breaks (acrylic will probably not deform smoothly as a  safety warning).The thicker the better. The soft, transparent film used to protect books would probably be a good choice.


---
**Andy Shilling** *February 24, 2018 09:22*

**+Jeremie Francois** that's not a bad call, I'll see if I can find some today.


---
**Don Kleinschnitz Jr.** *February 24, 2018 15:16*

**+Jeremie Francois** yes my surprise was that the box fractured and not at the seams, a hunk came out of the middle. That acrylic glue was amazing  ......


---
**Ned Hill** *February 24, 2018 19:22*

Yeah I'm going to have to second the caution here.  Not sure how much of a vacuum you are planning to pull, but in my lab days I've seen numerous thing implode under vacuum.  More to the point,  they were all things not specifically designed for vacuum work.  Might suggest a secondary acrylic "blast shield".


---
**Ned Hill** *February 24, 2018 19:36*

One time it was a dumbass that tried to use a large acrylic silica based desiccator as a vacuum desiccator.  It was rectangle shaped and made from 1/8" acrylic with ports so you could flush it with an inert gas if you wanted to.  He attached it to a large vacuum pump. Fortunately it was in a hood with the sash mostly down which contained most of the shrapnel and no one was hurt.  Needless to say I had to read him the riot act and the scenario was added to  the annual safety retraining as an example of using equipment for things it was not designed for.  


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/fGAwpXTR3Bz) &mdash; content and formatting may not be reliable*
