---
layout: post
title: "Greetings all. I am new to the community and new to the lasering world"
date: December 07, 2017 01:38
category: "Hardware and Laser settings"
author: "Kestrels Fury"
---
Greetings all.



I am new to the community and new to the lasering world.  I finally got the laser and the computer to "talk", but I am getting an error on the device id when I try to run the #%& thing.  Any help would be appreciated.



John



![images/2f9a71ca6c46aa6fcb64c623ba123dd6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f9a71ca6c46aa6fcb64c623ba123dd6.jpeg)
![images/eb5449202089ea44c9e8ec3aa146ea85.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb5449202089ea44c9e8ec3aa146ea85.jpeg)

**"Kestrels Fury"**

---
---
**Phillip Conroy** *December 07, 2017 05:41*

Try another good quality usb cable


---
**Kestrels Fury** *December 07, 2017 06:08*

Thanks.  I'll try that.


---
**Kestrels Fury** *December 07, 2017 07:27*

Would you happen to know this diameter of the focusing lens?  I have already replaced the mirrors, so I might as well start shopping for a replacement.  It is just a standard red box laser if that helps.


---
**Anthony Bolgar** *December 07, 2017 22:27*

Did you enter the ID in the software?


---
*Imported from [Google+](https://plus.google.com/104761945487452832708/posts/5C84KBnFJ59) &mdash; content and formatting may not be reliable*
