---
layout: post
title: "Shared on April 14, 2016 01:21...\n"
date: April 14, 2016 01:21
category: "Discussion"
author: "Donna Gray"
---


![images/9f6bbeef06f138a922ab0b0d4d08ba6c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9f6bbeef06f138a922ab0b0d4d08ba6c.jpeg)



**"Donna Gray"**

---
---
**Donna Gray** *April 14, 2016 01:25*

This is a photo of something that I would like to cut with this laser cutter would it work


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 01:41*

You could definitely cut it. Just the edges are not going to be clean like that. I will copy this image & do a test cut to show you the results.


---
**Alex Krause** *April 14, 2016 02:22*

This looks like it was cut with a drag knife in a silhouette cameo or cricut scrapbooking hobby cutter 


---
**Donna Gray** *April 14, 2016 02:24*

I have tried cutting similar on a scan N Cut machine but it doesn't cut cleanly


---
**Alex Krause** *April 14, 2016 02:25*

Those machines come in around the 200-250 USD price range but I have seen them on sale from time to time for 139-159 USD


---
**Donna Gray** *April 14, 2016 02:27*

I was wanting to see if this laser cutter cuts them better than the hobby cutter machines


---
**Ashley M. Kirchner [Norym]** *April 14, 2016 02:48*

Edges would be dark at best ... but definitely possible.


---
**Rob Morgan** *April 14, 2016 04:27*

Easy?? Cut a template first. Overlay on paper and cut again. No burn??


---
**I Laser** *April 14, 2016 04:56*

It would still burn, that's how the laser cuts. ;)


---
**Norman Kirby** *April 14, 2016 07:38*

didn't know how to attach a picture to this comment so have made another thread about the invitation we cut on the K40




---
**Scott Thorne** *April 14, 2016 10:45*

I've cut things like this with my machine with no burned edges what so ever...using light gray card stock...I'll post images when I get off work


---
**Scott Thorne** *April 14, 2016 10:46*

I forgot to add...using cutting speed of 100mm/s...at 30% power


---
**Tony Schelts** *April 14, 2016 12:55*

Very Nice Can you post the CDR or pattern please I would like to give them a go.

They look clean


---
**Donna Gray** *April 14, 2016 19:53*

I didn't have a pattern I just posted the picture to show what type of cut I am looking to do I don't know how to get the pattern from the picture


---
**Donna Gray** *April 14, 2016 19:54*

I don't even have a laser machine I am looking into purchasing a k40 and I have just been asking questions about it to see if it will do what I want it to do


---
**Ben Walker** *April 18, 2016 14:07*

You can def cut this with a k40.  i have the cameo and the curio (it's for sale btw).  Cuts clean but a bear to remove intricate cuts from the mat.  imo for speed, laser is the way to go.  Set your power low or speed high to avoid any charring.  Mine cuts paper like a champ quicker and cleaner than any hobby cutter I have ever used.


---
*Imported from [Google+](https://plus.google.com/103145403582371195560/posts/Exc4bWA6FhQ) &mdash; content and formatting may not be reliable*
