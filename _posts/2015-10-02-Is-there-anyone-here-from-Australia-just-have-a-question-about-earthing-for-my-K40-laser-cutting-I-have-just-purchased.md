---
layout: post
title: "Is there anyone here from Australia, just have a question about earthing for my K40 laser cutting I have just purchased"
date: October 02, 2015 21:49
category: "Hardware and Laser settings"
author: "Leanne Purnell"
---
Is there anyone here from Australia, just have a question about earthing for my K40 laser cutting I have just purchased. I know in US needs grounding rod,



I am just wanting to know if anyone in Australia is using the grounding wire & a rod or do we not need it here. If so what are you using, its all a bit confusing as its all new to me, normally I use a flat bed CNC, thought I would give laser a go.



Would be great to hear from someone in Australia. Have had good replies from people in the US, but I'm guessing things are different here.





**"Leanne Purnell"**

---
---
**Troy Baverstock** *October 02, 2015 22:58*

Someone correct me if I'm wrong, with the machine unplugged, use a multimeter to check for continuity between the metal casing and the earth pin on the plug. I'm not at home right now but you could first check visually to see if the earth pin on the IEC socket on the back of the machine goes to the chassis and also that earthing point on the back.﻿


---
**Gregory J Smith** *October 03, 2015 01:46*

Troy - I'm in Perth WA.  The K40 machine I purchased came with a DIN style plug with an earth pin on the back.  Internally the earth pin is connected to the grounding lug on the back of the the machine.  You can check this by use of a multimeter to make sure your AC earth is connected to the metal frame of the machine.  If so, you can ignore the manual comments regarding the earthing rod.



With regard to machine safety and earthing the following comments may be useful:



. I found that the machine frame is only bonded at one point.  I added extra earth wires between the bonding point and attached to the two lids and also to the frame of the power supply.  Without these extra earth links your earth on the cover is only provided by the hinges - not good enough.

. Only use the machine on an RCD protected circuit.  Recognise that the build quality of these machines appears variable.  Better to be safe.  If you house does not have RCDs then invest in an extension cord or power box with RCD / earth leakage protection.  It might just save your life.

. The machine does not have a lid safety switch.  I fitted on so that the laser turns off whenever the lid is opened.  The switch is wired into the low voltage laser enable signal from the controller board to the power supply.

. Don't be slack.  Make sure that the cover over the power supply parts is screw locked.  I can imagine there is a temptation to leave the screw off, but don't.  There's mains voltage and 22KV in there.  Replace the screw immediately after you've finished in there.


---
**Troy Baverstock** *October 07, 2015 22:11*

Good advice, when I have mine back up and running I will run some extra earths.﻿ Always a good idea to check that your earths are actually working when you move into a house, I've been caught out on that before.﻿


---
*Imported from [Google+](https://plus.google.com/107025904733514724932/posts/8D54U5bLTbM) &mdash; content and formatting may not be reliable*
