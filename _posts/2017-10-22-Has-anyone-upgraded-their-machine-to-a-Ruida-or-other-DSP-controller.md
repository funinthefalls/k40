---
layout: post
title: "Has anyone upgraded their machine to a Ruida or other DSP controller?"
date: October 22, 2017 17:36
category: "Modification"
author: "Anthony Bolgar"
---
Has anyone upgraded their machine to a Ruida or other DSP controller? I have a Ruida 6442S controller that I am wanting to install, and was hoping to get some info from someone who has already done this upgrade.





**"Anthony Bolgar"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2017 01:18*

On fb I see some people have gone to the LightObject DSP. It seems... complicated... it would involve a new psu, the DSP, external stepper drivers, converting all the cables over... 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/gUfd4v5GNhr) &mdash; content and formatting may not be reliable*
