---
layout: post
title: "My 100w RECI in my 900x600 frame..."
date: May 08, 2016 02:00
category: "Discussion"
author: "Todd Miller"
---
My 100w RECI in my 900x600 frame...

![images/e934b2d51ec62116119563ec6de19e48.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e934b2d51ec62116119563ec6de19e48.jpeg)



**"Todd Miller"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 02:37*

Wow, that looks very nice. 900x600 frame... is that the cutting area?


---
**Todd Miller** *May 08, 2016 03:30*

Yes, it should be, but since I have yet to fire

it or move/cut a box.. I don't know yet.


---
**Scott Marshall** *May 08, 2016 03:30*

drool.


---
**David Cook** *May 08, 2016 05:50*

Awesome 


---
**Phillip Conroy** *May 08, 2016 06:54*

Love the hight adjustable tube clamps- i have brought 2 sets however stuffed up as they will not fit ,too wide


---
**I Laser** *May 08, 2016 07:52*

my precious.....



Oh the things I could do with that...


---
**Scott Thorne** *May 08, 2016 08:38*

Great job...love the reci tube.


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/FnzMJCX6b6X) &mdash; content and formatting may not be reliable*
