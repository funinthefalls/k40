---
layout: post
title: "I'm having a braindead moment.....Someone has asked me to etch a Union Jack flag on a piece of edge lit cast acrylic"
date: December 09, 2018 11:46
category: "Object produced with laser"
author: "Nigel Caddick"
---
I'm having a braindead moment.....Someone has asked me to etch a Union Jack flag on a piece of edge lit cast acrylic. Fine.....but how can I differentiate between the red and the blue as the laser only etches white on acrylic? I need a lie down LOL





**"Nigel Caddick"**

---
---
**Stephane Buisson** *December 09, 2018 18:18*

Well I can only help with 2 colors  EU flag blue with yellow stars. Simply stack layers of acrylic on top of each other with one color LED per layer ;-))


---
*Imported from [Google+](https://plus.google.com/111196669641421431078/posts/GhiUTszK82P) &mdash; content and formatting may not be reliable*
