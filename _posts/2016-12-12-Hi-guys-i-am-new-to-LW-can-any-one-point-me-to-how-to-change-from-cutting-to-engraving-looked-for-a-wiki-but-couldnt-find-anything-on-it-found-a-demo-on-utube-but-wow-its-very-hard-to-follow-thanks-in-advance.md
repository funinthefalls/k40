---
layout: post
title: "Hi guy's i am new to LW can any one point me to how to change from cutting to engraving looked for a wiki but couldn't find anything on it found a demo on utube but wow it's very hard to follow thanks in advance"
date: December 12, 2016 23:35
category: "Software"
author: "Dennis Fuente"
---
Hi guy's i am new to LW can any one point me to how to change from cutting to engraving looked for a wiki but couldn't find anything on it found a demo on utube but wow it's very hard to follow thanks in advance.



Dennis 





**"Dennis Fuente"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2016 00:25*

Hey **+Dennis Fuente**. As far as my experience, when importing files, the file type makes a difference on whether it is loaded for cut or engrave. E.g. a JPG/BMP/PNG/etc file will be loaded into the CAM as an engrave & vector formats DXF/SVG/etc will be loaded as cut.



Give that a try with importing different file types & see the different options in the CAM for each file added.


---
**greg greene** *December 13, 2016 00:58*

That's been my experience also - LW is so automatic it sometimes leaves those of us used to the more klunky software wondering :)


---
**Alex Krause** *December 13, 2016 02:46*

Which demo did you watch on YouTube?


---
**Ned Hill** *December 13, 2016 04:12*

So you have to create separate files for cutting vs engraving in Laserweb?     Going to be interesting to figure out the work flow when I convert over.


---
**Ariel Yahni (UniKpty)** *December 13, 2016 04:50*

This is the workflow for now. Mind that LW4 is mega super dooper easier 

[github.com - LaserWeb3](https://github.com/LaserWeb/LaserWeb3/wiki/Workflow:-Setting-up-a-multi-step-job)


---
**Ned Hill** *December 13, 2016 12:51*

**+Peter van der Walt** Thanks for your work.  Just added to the beer fund :)


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/Wa72s527FnC) &mdash; content and formatting may not be reliable*
