---
layout: post
title: "What are people using for power cut-off if the water pump fails?"
date: December 28, 2015 18:11
category: "Modification"
author: "Gary McKinnon"
---
What are people using for power cut-off if the water pump fails?

Wiring diagrams greatly appreciated :)







**"Gary McKinnon"**

---
---
**Anthony Bolgar** *December 28, 2015 19:11*

I am using a flow sensor that triggers an arduino board to display a message on the LCD screen as well as triggering a 5V relay to kill the power to the laser.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 29, 2015 01:52*

**+Phillip Conroy** let me know about these flow switches. I'd imagine you can just run them inline with the power feed to the laser tube.



[http://www.ebay.com.au/itm/White-Plastic-Shell-Magnetic-Water-Flow-Switch-w-Inner-Outer-Thread-SYSZAU-/252199463581?hash=item3ab8426a9d:g:QnoAAOSwHQ9WYlJc](http://www.ebay.com.au/itm/White-Plastic-Shell-Magnetic-Water-Flow-Switch-w-Inner-Outer-Thread-SYSZAU-/252199463581?hash=item3ab8426a9d:g:QnoAAOSwHQ9WYlJc)


---
**Phillip Conroy** *December 29, 2015 14:10*

I hooked mine up to the laser enable button <s>low voltage</s> do not hook upto high voltage at tube 22,000 volts ,will kill you 


---
**Coherent** *December 29, 2015 18:40*

I bought a cheap flow sensor from lightobject and mounted it on the rear and wired it to the laser on switch. Works well for me. There's a photo here. You can click on the link for a larger view.

[http://k40d40lasers.freeforums.net/thread/28/coolant-flow-sensor-air-valve](http://k40d40lasers.freeforums.net/thread/28/coolant-flow-sensor-air-valve)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 30, 2015 02:46*

**+Phillip Conroy** Thanks for giving that extra info Phillip. Wouldn't want any of us to get zapped.


---
*Imported from [Google+](https://plus.google.com/+GaryMcKinnonUFO/posts/bEcZPYUwvK2) &mdash; content and formatting may not be reliable*
