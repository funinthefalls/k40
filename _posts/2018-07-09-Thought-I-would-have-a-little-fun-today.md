---
layout: post
title: "Thought I would have a little fun today"
date: July 09, 2018 03:54
category: "Object produced with laser"
author: "bbowley2"
---
Thought I would have a little fun today.  My grandson loves dragons.

![images/75669b4d80d1685994b1eeaddd56f1aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/75669b4d80d1685994b1eeaddd56f1aa.jpeg)



**"bbowley2"**

---
---
**Don Kleinschnitz Jr.** *July 09, 2018 13:03*

Pretty Sweet!


---
**Steve Anken** *July 09, 2018 14:10*

Very nice but using esp32 you could use you cell phone instead and make up your own patterns and even make it react to music. Check out the fastLED group.


---
**James Rivera** *July 09, 2018 17:09*

**+Steve Anken** Link?


---
**Steve Anken** *July 09, 2018 17:49*

**+James Rivera** [FastLED Users](https://plus.google.com/u/1/communities/109127054924227823508)




---
**bbowley2** *July 09, 2018 21:36*

**+James Rivera** Link to what?




---
**James Rivera** *July 09, 2018 22:14*

**+bbowley2** Seriously? See the post just after mine.


---
**bbowley2** *July 09, 2018 22:15*

**+Steve Anken** I would like the lights to react to music.  


---
**Steve Anken** *July 10, 2018 01:05*

**+bbowley2**  some folks doing that in the fastLED group. see my post to James for the link or just search Communities in Google+.


---
**Nigel Caddick** *December 09, 2018 12:20*

Is that a hand made base? What type of LED strip do you use? I find I can't match the brightness of the plastic ready made bases, the LED's are much closer together and brighter with them, my 5050 and 3528 strips are nowhere near as bright :(




---
**bbowley2** *December 28, 2018 00:53*

**+Nigel Caddick** They are handmade.  I can't remember the exact led's but I put in a mini receiver with the small remote.




---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/L9KTCfNrBEe) &mdash; content and formatting may not be reliable*
