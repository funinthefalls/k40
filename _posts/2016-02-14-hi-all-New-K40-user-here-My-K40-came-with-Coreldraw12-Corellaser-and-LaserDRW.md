---
layout: post
title: "hi all. New K40 'user' here. My K40 came with Coreldraw12, Corellaser and LaserDRW"
date: February 14, 2016 22:26
category: "Software"
author: "Brian Hopper"
---
hi  all. New K40 'user' here.  My K40 came with Coreldraw12, Corellaser and LaserDRW. 

I have a Windows 10 machine (only).

Coreldraw 12 crashes when I import images.  I tried to use CorelDraw x7 - but Corellaser 2013 doesnt seem to work with.  Then Norton anti-virus finds a 'backdoor' file and kills Corellaser.



I have been able to test the laser with LaserDRW and images exported via Inkscape - but I'm not REALLY sure where to start with a good image workflow.  I have seen a  nice tutorial on Instructables using Coreldraw for a stacked workflow to first engraving then cutting workpieces - but I cant get Corellaser to run with Coreldraw x7.



I'd appreciate some suggestions and pointers please.

I am most familiar with Photoshop, I have Inkscape installed and have used 'trace bitmap' to generate cutting images so far.



Thanks !!





**"Brian Hopper"**

---
---
**Jim Hatch** *February 14, 2016 23:00*

You can use Photoshop or download Adobe Illustrator CS 2 (it's unsupported but still hosted on the Adobe site and allowed for use without a purchase - hoping you'll upgrade to the latest version of CS).



Save the file as a BMP. Open LaserDRW and import the BMP file. Then send it to the laser for engraving or cutting.



I have to do the same thing with a $5,000 60W laser our makerspace has except that one's software will open DXF files and I can assign colors to cut or engrave and the order. With the K40 I just do different BMPs for each layer of engraving or cutting. A little more work but not tons. And I didn't spend $5K :-)




---
**I Laser** *February 14, 2016 23:02*

Unlikely it will work on win 10, I'm under the impression it doesn't work on win 8 either. You can try using hyper-v to run an older OS.



Link to the software courtesy of Jim Coogan [https://www.facebook.com/groups/441613082637047/531905473607807/](https://www.facebook.com/groups/441613082637047/531905473607807/) From memory he had issues with a virus in there too, so this one I assume is virus free.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 14, 2016 23:59*

Strangely I run CorelLaser & the CorelDraw12 that came with my K40 through Windows10 Pro with no dramas (running on Bootcamp on Macbook). Only issue I have is that when opening the software, I usually have it crash Adobe Creative Cloud (which doesn't really matter, just reopen it when needed). I use Adobe Illustrator CC to create my files (saved in AI9 format else Corel crashes on import of newer file versions).


---
**I Laser** *February 15, 2016 02:13*

Another one of my assumptions, the software states support up to win 7 and there has been numerous posts about it not running in win 8. I therefore figured it wouldn't run post win 7. Glad to see it does, still not convinced to hit the win10 upgrade button lmao


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2016 05:31*

**+I Laser** Yeah I was also concerned after reading that the software is Win7, thinking I may have to create another boot with Win7, but fortunately it did work in Win10.


---
*Imported from [Google+](https://plus.google.com/114184787185758454112/posts/2k9ojJxPo6g) &mdash; content and formatting may not be reliable*
