---
layout: post
title: "Haven't been able to find an answer to this"
date: June 21, 2018 20:22
category: "Software"
author: "David Davidson"
---
Haven't been able to find an answer to this.



 Using a cohesion board with Lightburn, does the software override the digital power setting on the K40 control board, or is the power setting a percentage of what is set on the K40 control panel?





**"David Davidson"**

---
---
**Don Kleinschnitz Jr.** *June 21, 2018 23:44*



The power supply L pin (from controller) duty cycle is a percent of the control panels duty cycle. 


---
**LightBurn Software** *June 22, 2018 04:25*

In English, the C3D produces a percentage of what you set on the main panel. If you set it to 50%, and LightBurn to 50%, it’d be 25% power output.


---
**Don Kleinschnitz Jr.** *June 22, 2018 12:09*

Or in a universal language...... :) :)

..change the main panel and Lightburn settings to decimal 

then ...multiply the main panel setting by the Lightburn settings 

and finally, multiply that by 100 to get percent power



.50 x.50 = .25  * 100 = 25%


---
**David Davidson** *June 22, 2018 14:50*

So to give lightburn complete control of power, set the main panel to 99.9% and let lightburn have at it.



25% lightburn and 99.9% main panel equals 24.975% output power.



Got it. Thanks!




---
**LightBurn Software** *June 22, 2018 15:55*

**+David Davidson** - you shouldn’t set the main panel to 100% - set it to wherever it needs to be so the test button fires at the right milliampere level for the tube. If you have a digital panel, that value varies quite a bit (they’re fairly inconsistent). For some it’s 40%, for others it’s 60%, but 100% will burn your tube faster. 16ma is about as high as you should go.


---
**David Davidson** *June 22, 2018 22:23*

**+LightBurn Software** Thanks. Going to add an ampmeter to this thing so I can see what's going on. Also, great software. Thanks.




---
**Doug LaRue** *June 23, 2018 01:13*

It seems Mr LightBurn Software said the same thing. leaving it to get the point across. -

**+David Davidson** WRONG and not a good idea. The digital control panels are very poorly implemented and  often anything over 40 or 50% is over driving the laser at 2x mA.  Best is to profile your digital controller to determine what percentage on the display equals 18mA and consider that to be 100%.  From there, set Smoothie to output max 100% and min 0%.  From then on the settings in LightBurn will be a percentage of 18mA(=100% power).




---
**Don Kleinschnitz Jr.** *June 23, 2018 11:40*

**+David Davidson** there are lots of reasons to add an ammeter to help you control what current your tube is actually running. You can check out my blog post on the digital panel and its goods and bad's.

Keep in mind as the tube is used its output vs current changes until it is "worn out". For that reason whatever you control it with; pot or digital will not retain a fixed profile of current vs optical power.


---
**Doug LaRue** *June 23, 2018 18:36*

Yes, recalibration would be required and maybe more often as it ages.


---
*Imported from [Google+](https://plus.google.com/+DavidDavidson-tristar500/posts/Kzid2mz541Y) &mdash; content and formatting may not be reliable*
