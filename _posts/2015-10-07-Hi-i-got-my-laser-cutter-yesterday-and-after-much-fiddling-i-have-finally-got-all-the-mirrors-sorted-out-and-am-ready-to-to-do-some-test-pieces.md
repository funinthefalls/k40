---
layout: post
title: "Hi i got my laser cutter yesterday, and after much fiddling i have finally got all the mirrors sorted out, and am ready to to do some test pieces"
date: October 07, 2015 17:07
category: "Hardware and Laser settings"
author: "Maldenarious"
---
Hi i got my laser cutter yesterday, and after much fiddling i have finally got all the mirrors sorted out, and am ready to to do some test pieces.  So i started to do my first piece, and whilst the machine is operating i sometimes hear a snap/cracking noise coming from it, the laser tube, i'm guessing, is this normal? i have my pump attached and switched on, water is flowing.  Kind of wary of using it until i find out if the noise i am hearing is normal.  Thanks





**"Maldenarious"**

---
---
**Ashley M. Kirchner [Norym]** *October 07, 2015 19:01*

Yeah, tube should not e making any noise (unless you have your ear right up against it and you're hearing the high voltage buzzing.)


---
**Maldenarious** *October 07, 2015 19:31*

Thanks for the responses, it turns out i had a small bubble in the tube, cleared now, hopefully that will sort it.


---
**Ashley M. Kirchner [Norym]** *October 07, 2015 19:34*

Bubbles can be fatal to the tube. Whenever I drain my tube (only happened once so far), I end up having to tilt the machine a bit to get the last of the bubbles out. Tedious but doable.


---
**David Wakely** *October 07, 2015 20:02*

Spot on **+Ashley M. Kirchner**, in my machine I rotated the tube and re aligned because the water outlets were facing downwards meaning any bubble would get trapped. My water outlets now face up so if any bubble does work it's way in they go straight back out


---
**Maldenarious** *October 07, 2015 20:02*

yeah it was just a tiny bubble, i pretty much had to stand the machine on end to get rid of it.  I was still getting the noise after clearing it, so i lifted the tube cover this time when i heard it and i could see electricity sparks arcing from the end of the tube to the case, this cant be normal right?


---
**Ashley M. Kirchner [Norym]** *October 07, 2015 20:08*

Check the wiring, make sure you have good contact to the tube's terminals.


---
**David Wakely** *October 07, 2015 20:14*

And that's why you need to always make sure you have decent earthing! 20,000 volts would do you some serious damage!


---
**Maldenarious** *October 07, 2015 21:33*

The wiring was ok, you did point me in the right direction though :) Turns out there was no rubber insulating tube around it, just a little hot glue. I popped some on there and it seems to be fine now.  Just done my first couple of test pieces, very happy with the results. Thanks for the help and  advice, i really appreciate it.


---
**Ashley M. Kirchner [Norym]** *October 07, 2015 21:41*

Yeah, I don't know how much of 'quality control' there is with these things. It's best to always check everything before turning things on. I've had plugs come un-wired, or just twisted together, not soldered or anything. Other stuff that comes loose during shipping ... I took my time and spent two days visually inspecting and touching everything, making sure it's all properly attached or soldered before I fired the thing up. And even then, I made sure there was someone else there when I test fired the laser, in case something went completely wrong and I caused a mushroom cloud instead. :)


---
**Jon Bruno** *October 10, 2015 14:03*

Yes it was! Lol


---
*Imported from [Google+](https://plus.google.com/115599912786188133985/posts/LFt86Do6EjB) &mdash; content and formatting may not be reliable*
