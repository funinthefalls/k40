---
layout: post
title: "My K40 laser has been working fine for the past few weeks"
date: May 22, 2015 14:21
category: "Discussion"
author: "Trenton"
---
My K40 laser has been working fine for the past few weeks.  Today, the mA seem very unstable when I push the test button.  Also, it doesn't engrave as "cleanly" as before (lines are not very sharp).

Any ideas what could be causing this?  Bad power supply or bad laser tube maybe??

![images/e1fcf61428e90808018a100ebe3b701e.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/e1fcf61428e90808018a100ebe3b701e.gif)



**"Trenton"**

---
---
**Chris M** *May 22, 2015 14:28*

Because noone else has posted, I'll have a guess. The meter only moves when the laser is actually firing, and down in the 0-5mA range the laser often will not fire. I'm happy to be shot down in flames on this one, as I haven't met the analog meter version.


---
**Trenton** *May 22, 2015 14:31*

Thanks for your reply.  I considered that as well, but the meter jumps around like this when I turn the power up as well.


---
**Jim Coogan** *May 22, 2015 14:54*

Chris is correct concerning the laser firing.  The problem with engraving could be something as simple as the mirrors or especially the lens being dirty.  Check them and clean as necessary.  The mirrors tend to stay fairly clean but the lens gets dirty from material like smoke coming up at it.  Even if you have air assist you still need to check the lens occasionally.  I have also seen lines blur when I was working with material that had a slight bend in it.  And check to see if you were focused on the surface of the part of just below it.  If there is a change in the flatness of the part you are engraving and you are focused right at the surface the line will blur and sharpen as it passes over those inconsistencies in the surface.  Hope this helps.


---
**Eric Parker** *May 22, 2015 15:14*

I think your meter is cooked.

The cathode line for the laser runs through that meter.  I could be completely wrong though.


---
**Trenton** *May 22, 2015 15:50*

Ok, so if the meter is bad, would that cause the laser to fire intermittently and produce poor quality engraving?


---
**Eric Parker** *May 22, 2015 18:57*

I'm just speculating again, but if theres something wrong with the meter, it's very possible.  Ammeters are connected in series to their loads, so if something has gone bad with the meter internally, it could very well affect the laser.  The only way I would know to test this, would be to replace the meter, or use a DMM in it's place; though I can't speak to the safety of that, since you are dealing with many thousands of volts.


---
**Trenton** *May 22, 2015 19:00*

Thanks, I really appreciate your help.  Do you know where I can get a replacement?  Ebay? I've been in contact with the seller, but they haven't been much help yet.


---
**Chris M** *May 22, 2015 19:54*

Have you checked the potentiometer? I think that's more likely to be at fault than the meter, on the 'if it moves it might fail' scale.


---
**Trenton** *May 22, 2015 20:00*

Well now it works!  I connected/reconnected my water flow switch connections, and it works great now.  Hopefully that was the problem...


---
**Jose A. S** *June 01, 2015 19:40*

What you mean with water flow switch connections?...pos a pic please!


---
**Trenton** *June 01, 2015 20:47*

I added a water flow switch that turns off the laser if the pump fails.


---
**Jose A. S** *August 03, 2015 14:58*

**+Trenton Hovland** What you mean with water flow switch connections?...pos a pic please!


---
*Imported from [Google+](https://plus.google.com/108213621154298105694/posts/eFbadEntBXT) &mdash; content and formatting may not be reliable*
