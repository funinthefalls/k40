---
layout: post
title: "100 National Healthcare Volunteer Appreciation week buttons done"
date: April 03, 2017 16:33
category: "Object produced with laser"
author: "Ned Hill"
---
100 National Healthcare Volunteer Appreciation week buttons done.  Made from 3mm alder and are 50mm (2") in diameter with magnetic backs.  They went with a totally different design (hospital logo) then I posted about a couple of weeks ago.  They are for volunteers in my wife's dept at the local hospital so I made her help me paint them ;)



![images/993110aad7e782e67a27ca46257d02ae.png](https://gitlab.com/funinthefalls/k40/raw/master/images/993110aad7e782e67a27ca46257d02ae.png)
![images/838e6cfd4cba742e8c0d7086efe5ddf2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/838e6cfd4cba742e8c0d7086efe5ddf2.png)

**"Ned Hill"**

---


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/PSCcbSqQtcn) &mdash; content and formatting may not be reliable*
