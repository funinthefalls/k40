---
layout: post
title: "Has anyone tried this adjustable lens focus mod?"
date: January 29, 2016 17:10
category: "Modification"
author: "Michael Ang"
---
Has anyone tried this adjustable lens focus mod? Any thoughts on it? 
{% include youtubePlayer.html id="CC9kGOlxlHs" %}
[https://www.youtube.com/watch?v=CC9kGOlxlHs](https://www.youtube.com/watch?v=CC9kGOlxlHs)





**"Michael Ang"**

---
---
**David Cook** *January 30, 2016 02:30*

This is a neat mod


---
**Anthony Bolgar** *January 30, 2016 11:39*

One problem however is that both sides of the lens are open to the air, will probably get dirty very fast.


---
**David Cook** *January 30, 2016 17:41*

Would be neat to make it as a telescoping tube mechanism  to handle the dust problem, then the air nozzle could also be integrated.  If I have some time today I might try to 3D model a concept of that


---
**Anthony Bolgar** *January 30, 2016 17:56*

It really isn't adjustable focus.The focal lenght remains the same, it just allows you to place the lens as clocse to 50.8mm as you can without a z table.


---
**David Cook** *January 30, 2016 18:02*

Exactly


---
**Dustin Hartlyn** *January 30, 2016 18:47*

I completely agree with the telescoping tubes. This is how it works with my machine. However, you might be able add a small air tube the blows clean air onto the surface of the lens to keep it from gettting dirty. 


---
**David Cook** *January 30, 2016 19:49*

I was  just thinking that I could design the tube in such a way where the lends snaps into position,  but I wonder if the chinese lens gets hot and might melt the 3D print lol.  I guess if the lens is clean it should not heat up,  what do you guys think ?   if it works then I could 3D print a lens marker on the outside of the tube to reference for aligning the focal point easily  since I would want the air nozzle under the lens.


---
**Rick James** *January 31, 2016 12:55*

[http://i.ebayimg.com/00/s/NjAwWDgwMA==/z/GbUAAMXQHU1RvxsT/$(KGrHqNHJEgFGpRgB!LYBRv)sTU!oQ~~60_35.JPG?set_id=880000500F](http://i.ebayimg.com/00/s/NjAwWDgwMA==/z/GbUAAMXQHU1RvxsT/$(KGrHqNHJEgFGpRgB!LYBRv)sTU!oQ~~60_35.JPG?set_id=880000500F)



i got one of these off ebay works great


---
**Stephane Buisson** *January 31, 2016 13:45*

Moving the lens vs moving the table.



problem is with this system is you still need to lower the table by the material thickness. so not a solution for our K40. (not much space to move the lens up)


---
**Michael Ang** *January 31, 2016 14:24*

That's a good point about not being able to move the lens up very much. But even a few millimeters should be helpful for cutting different thickness of plywood, for example.



Here's a relatively small adjustable head with air assist (maybe the lower part could be 3D printed and attached to the existing K40 head): [http://www.aliexpress.com/item/CO2-Laser-Head-Laser-Engraving-Cutting-Machine-Engraver-High-Quality/32355361516.html](http://www.aliexpress.com/item/CO2-Laser-Head-Laser-Engraving-Cutting-Machine-Engraver-High-Quality/32355361516.html)



Someone started to make the holy trinity of adjustable lens height, air assist, and dual laser pointers: [http://www.thingiverse.com/thing:781204](http://www.thingiverse.com/thing:781204)



I see plastic lens holders on Thingiverse so I don't think it should be a problem. I should be getting a K40 later this month and want to try this mod :)


---
**Stephane Buisson** *January 31, 2016 14:41*

I did think about all that when I design my air assist last year. (3 functions in 1)

[https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser](https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser)


---
**Michael Ang** *January 31, 2016 15:17*

Hey cool! I'll give it a try, thanks.


---
*Imported from [Google+](https://plus.google.com/101545054223690374040/posts/GeyVgsAGu4P) &mdash; content and formatting may not be reliable*
