---
layout: post
title: "Hi All, I'm new to this group as I've just purchased one of these 40w Laser Cutter/Engraver machines"
date: October 15, 2015 07:24
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Hi All,



I'm new to this group as I've just purchased one of these 40w Laser Cutter/Engraver machines. I am having supreme difficulty aligning the mirrors. Any help would be appreciated.



So basically I have done placed the tape over the Mirror #2 (on Y axis) & punched some quick test fires at it (in upper position, closest to the laser tube; & in bottom position, closest to the front of the machine).



What I've noticed is that I can get it to hit near perfect centre in the upper y-axis position, yet in the bottom y-axis position the hit will be about 5-10mm to the right & 5-10mm lower.



I've tried for about the past 4 hours to align this, to no avail. No matter what I do, or what way I adjust it, I cannot get them to even remotely align the two shots.



I imagine that there is possibly something I am doing wrong, or alternatively the Mirror #1's bracket is out of line. Is that a possibility?



Thanks in advance.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Phillip Conroy** *October 15, 2015 11:43*

Try rasing or lowering each end of the laser tube by 1 to 5 mm,i had same problem as you entill i changed the tube and did not tighten the clamps as much


---
**Phillip Conroy** *October 15, 2015 11:46*

I will email you better instruction in the morning  nebula98@dodo.com.au 


---
**Phillip Conroy** *October 15, 2015 11:47*

Try this [http://dck40.blogspot.com.au/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html](http://dck40.blogspot.com.au/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2015 12:05*

**+Phillip Conroy** Thanks Phillip. I will give that a test & see how that goes. :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2015 12:14*

**+Allready Gone** Thanks Allready Gone. I will take a look at those options also.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2015 12:16*

**+Allready Gone** Sorry, just to confirm. When you say "square the carriage" are you referring to the y-axis carriage that has the Mirror #2 on it? or something else?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2015 12:26*

**+Phillip Conroy** Thanks again Phillip. I've sent an email through to you from my gmail. Check your spam if you don't see it


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2015 12:27*

**+Allready Gone** Awesome, thanks very much for that. I will definitely check into that.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/E5DCwh3yQdg) &mdash; content and formatting may not be reliable*
