---
layout: post
title: "Is this normal? It's acting like it is stalling out then resuming"
date: May 12, 2016 18:37
category: "Original software and hardware issues"
author: "Alex Krause"
---
Is this normal? It's acting like it is stalling out then resuming


**Video content missing for image https://lh3.googleusercontent.com/-d5WJ2irXMXk/VzTNeKVo5wI/AAAAAAAACMY/_4qBcvbohzAHwcTTi-2NzcWzEse0OtnRQ/s0/VID_20160512_133423356.mp4.gif**
![images/375d914a5367d29b8b5427d845128eb4.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/375d914a5367d29b8b5427d845128eb4.gif)



**"Alex Krause"**

---
---
**Jon Bruno** *May 12, 2016 19:34*

It's been explained to me a few times that if you increase the ram of the host system this behavior goes away.


---
**Alex Krause** *May 12, 2016 19:36*

I'm stuck with the RAM as it's a Windows tablet operating this I will see if I can increase virtual memory


---
**Jon Bruno** *May 12, 2016 19:36*

Which tablet are you using?


---
**Jean-Baptiste Passant** *May 12, 2016 19:45*

I'm not sure what could be the problem and how smoothieboard handle it, but I believe that it may be a buffer issue. But even though the movement are slower, it looks like buffer is fine as there is no space or strange results (skipped steps or anything).

Any idea **+Stephane Buisson** **+Arthur Wolf** ?


---
**Arthur Wolf** *May 12, 2016 19:50*

Can you try printing from the SD card ?


---
**Alex Krause** *May 12, 2016 19:50*

This is a stock controller and software


---
**Alex Krause** *May 12, 2016 19:51*

Smoothie conversion will happen when my level shifter arrive in the mail. At that point I will put in the 4xc board


---
**Jean-Baptiste Passant** *May 12, 2016 20:20*

**+Alex Krause** This explain that :P

I believe this is a non issue then, normal behaviour of the stock board.

I can't tell, I did not use it to engrave any pictures. Sorry for confusion ;)


---
**Jim Hatch** *May 12, 2016 20:46*

**+Jean-Baptiste Passant** I agree. In fact if you watch the Windows status bar on the lower right you should see msgs pop up about it sending various passes to the laser. (There's actually a little applet loaded that you can see if you click down there - not really needed with LaserDRW but with CorelLaser if you lose the icons for starting/stopping/reset/etc for the laser you can still do that by clicking down there and getting into the laser applet/status thing that's loaded down there.) If you're not watching, you may miss those messages as they display for a few seconds and then automatically disappear.


---
**Tony Sobczak** *May 12, 2016 22:30*

I'm still stock and do not have that issue when doing large raster. I think it's more of a computer issue. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 22:36*

I've had this issue on large complicated rasters, with 16gb of ram on the computer so I do not think it is a ram issue. I would throw my vote in it being the buffer size on the stock controller as the issue.



**+Tony Sobczak**  I think it's not so much regarding the physical size of the raster, rather the complexity of the raster. I can't say for certain, but I imagine rastering a solid square would be a lot less complex in regards to the commands that are sent to the machine, as opposed to something that was more complicated.


---
**Greg Curtis (pSyONiDe)** *May 12, 2016 22:36*

Mine with occasionally pause on larger things, but only once or twice when doing a very large engrave. Still comes out flawless though. Even still see it now and again after I upgraded to 4gb from 2gb RAM.


---
**HP Persson** *May 13, 2016 09:08*

It´s a combination of RAM and USB2 buffer, often occurs on tables or laptops with low amount of ram and alot of other stuff running at the same time.



To prevent this, remove as many USB-devices you can, and close all softwares running in the background.

If it doesnt help, well, not much to do other than live with it or change the unit you are using.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 09:27*

**+HP Persson** That may have been my issue (not the RAM as I have 16gb & usually nothing else open but CorelLaser, but the USB) as I have been running everything through a 4 port USB hub. The cord to the controller + the USB security key + my 2x USB webcam (to watch the laser + the tube) all on the same hub. It is a USB 2 hub in a USB 3 port. Would changing to a USB 3 hub minimise this issue?


---
**HP Persson** *May 13, 2016 09:36*

**+Yuusuf Sallahuddin** Try a own cable from the computer to the control board, and leave the USB-key in the hub.

Cheaper than buying a new hub, you probably wont see any difference in USB2 performance either.

Is the hub powered? if not, that might help too


---
**Jean-Baptiste Passant** *May 13, 2016 09:45*

**+Yuusuf Sallahuddin** 

Not really, keep in mind your peripherals are still USB2.0 and the buffer will keep that limit too.

Also **+HP Persson**  I've never heard of buffer issue coming from too much usb peripherals.

unless you use a HUB as there is one buffer for the whole hub. But generally speaking, hub are something from the past, and it's been a while since I've seen one. But yes, it may be the problem here as I imagine the tablet **+Alex Krause** use might not have enough usb port (one for the key and one for the machine), well thought :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 10:11*

**+HP Persson** The hub is not powered, just running from the usb port. I will just run the controller cable on it's own port in future & everything else off the 2nd port.



**+Jean-Baptiste Passant** I'm using hub as I only have 2 usb ports on the macbook. A couple of thunderbolt ports too. I intend to switch to a different device just for the laser in future (tablet/stick pc/desktop pc, depends what I get at a decent price).


---
**HP Persson** *May 13, 2016 10:20*

Buffer issue depends on how the computers mainboard is built, on some (old/cheaper) laptops, older desktops or pad´s  there is one controller for all ports, while new computers has one controller per port or two ports.



Also, some controllers revert back to USB1.0 if a 1.0 unit is plugged in, having other stuff connected to that controller makes them 1.0 too. 

Thats why i suggested to remove all other devices, to be sure. (pretty rare, but i´ve seen it)


---
**HP Persson** *May 13, 2016 10:30*

**+Yuusuf Sallahuddin** And check if your hub can be powered, most of them have a option for it. That will help your webcams, they are flooding the controller like crazy and are pretty power-hungry :)

Not sure how the macbook is built, unsure if it has one controller for both ports, but relieving the controller from some of the power isnt a bad idea anyways :)



If you run a virutalized setup, also check if that has any kind of USB optimization settings, may help.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 11:32*

**+HP Persson** I will check out all that you mention. Definitely the hub doesn't have powered option. Was just a cheapo I bought about 5 years ago. Thanks for all your suggestions.


---
**Jim Hatch** *May 13, 2016 12:34*

I run mine from an unpowered 1 into 4 USB hub from a Surface 3 (not Pro) laptop/tablet. The S3 only has a single USB connection so I need to use a hub to get the key and the cable to the K40. The hub is a Microsoft one that also has a couple of SD slots in it. On my setup when I tried a powered hub the key wasn't recognized. I didn't think the unpowered one would work but that's the one that did.



Just shows that everything is just a little different with these things - no consistency. Nothing's really plug & play in the K40 world :-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 14:34*

**+Jim Hatch** Actually, I think nothing is really plug & play in the computing world in general. Too many manufacturers, not enough standards (or people sticking to them at least).


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/4n3q8Mry9df) &mdash; content and formatting may not be reliable*
