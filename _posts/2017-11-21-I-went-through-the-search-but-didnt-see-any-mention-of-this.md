---
layout: post
title: "I went through the search but didn't see any mention of this"
date: November 21, 2017 18:53
category: "Original software and hardware issues"
author: "Jeff Bovee"
---
I went through the search but didn't see any mention of this.  Does anyone know what the flow rate of the submersible pump is?  I think I want to have a replacement on hand because mine is beginning to sound a little rough.





**"Jeff Bovee"**

---
---
**Joe Alexander** *November 21, 2017 19:21*

you can always check it by filling a 5 gallon bucket and timing it, then do the math to calculate the gpm/lpm. I currently use a very small pump rated for 240l/h and it cost me like 5 bucks and it seems to work just fine. heres the link to a similar one: [amazon.com - Amazon.com: Anself DC12V 4.8W Mini Brushless Submersible Water Pump for Fish Tank Aquarium Fountain Flowerpot: Pet Supplies](https://www.amazon.com/dp/B011C0X328/ref=sspa_dk_detail_4?psc=1)


---
**Phillip Conroy** *November 21, 2017 19:28*

I have tested 6 pnd pumps and only found that the biggest one would pump enough for a 40 w tube ,it was a 150watt 240v rated at 5600 liters per hour,when hooked up to the laser dropped to 2 litres per minute, just at lower end of what my tube needed.

When i upgraded to a sh g350 50w laser the pond pumps could not suply the recommended flow rate of 4 liter per min ,so i brought one of the pumps used in the cw range of chillers [aliexpress.com - Brushless DC Pump P2430 24V voltage 25W watt 8.5L/min 13PSI for industrial Chiller CW3000](https://www.aliexpress.com/item/75W-pump-submersible-water-pump-220V-75W-3-5M-for-cnc-engraving-machine/32827555436.html?spm=a2g0s.9042311.0.0.125pr2)

Great little pump,dosnt heat up the water as it is external to tank and when hooked up to 50w tube flow rate is 5 litres mer minute


---
**Jeff Bovee** *November 21, 2017 20:45*

That's good info.  Obviously maximum flow would be determined by the size and length of tubing being used.  I wonder if with the pond pumps more rated flow doesn't get you anything because of the limitations of the tube and orifices it needs to go through.  I like the brushless pump, just wish it came in a corded option.  I was looking at a 920 gph pump on Amazon, but now wonder if it would really do any better than the one that came with the K40.


---
**Don Kleinschnitz Jr.** *November 21, 2017 21:33*

[donsthings.blogspot.com - K40 Coolant Pumps](http://donsthings.blogspot.com/2017/05/k40-coolant-pumps.html)


---
**Alex Raguini** *November 21, 2017 21:33*

If it is starting to sound a little rough, check to be certain there are no kinks or obstructions in the lines.  Mine started sounding a little strange and I discovered I had a slight kink in one of the lines.


---
**HalfNormal** *November 22, 2017 01:06*

There is a lot of information available on pumps. Knowing the correct terminology and how it is used is key. Flow rate and pressure are two different variables in the equation. Pond pumps have high flow rates and low pressure. Restriction further limit flow rate. Here is one explanation of many available.  

[globalspec.com - Pump Flow](http://www.globalspec.com/pfdetail/pumps/flow)


---
*Imported from [Google+](https://plus.google.com/115193415731522119744/posts/HVkcXSi8p5S) &mdash; content and formatting may not be reliable*
