---
layout: post
title: "I've pulled the trigger and ordered a Smoothie + 12864LCD"
date: June 22, 2016 21:01
category: "Discussion"
author: "Mircea Russu"
---
I've pulled the trigger and ordered a Smoothie + 12864LCD. 



How do you control the laser:

-PWM on TL/TH and keep pot on IN?

-PWM on IN and another pin controls TL/TH?

-any other wiring I can not think of now?



What is the default pin state on Smoothie? I wouldn't want the laser to fire when the board resets or loses power and makes the pin go the default state (see RAMPS-FD disaster).

I would also  like to use option 1 and still keep the pot as a MAX current level for when PWM is 100% or a lower current for test firing. Do you have any other way to use PWM and also keep the pot?

Thanks.





**"Mircea Russu"**

---
---
**Don Kleinschnitz Jr.** *June 23, 2016 16:10*

Just now the answer seems to be "it depends". Here is  some research I have been doing to get to an answer for my own design. I am on vacation and will test these configurations when I return hopefully getting to a clear design.



[http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.html](http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.html).



Also search this forum and you can get lots of perspectives, opinions and actual implementations.




---
**Mircea Russu** *June 24, 2016 07:45*

**+Don Kleinschnitz** Thank you for all your work. It seems there is no universally adopted solution to this, but open-drain/mosfet tapped directly to the IN pin with the pot in place seems the best choice, except when having pot all way up and then you short 5V to GND via the mosfet, you were disturbed by having the pot all way down, but that's shorting GND to GND.

Maybe add another resistor between 5V and POT MAX pin, also a safety feature since we do not use more than 15-18ma anyway, so 1k res on top of the pot would give 0-66% range of the default pot config.


---
**Don Kleinschnitz Jr.** *June 24, 2016 13:09*

**+Mircea Russu** 

The only time concern may with the pot being at the extreme is in the case a level shifter is used. As you point out in the open drain you are grounding the drain and the source is also at ground.



I am also thinking of adding a high pass filter at the IN port between PWM and IN to block any dc offset from the PWM pin. 



It may very well be that I am chasing a ghost since the area under whatever profile we put on pin IN will still control the laser (as many have proven), it just may be in a complex form to predict power levels.



I am guessing that the LPS input controls are isolated and on the LED side of an optical isolater whereas they ground (for digital controls) or bias (for analog) the LED emitter. 



There will be 2.5 MA (5/2000) flowing through the pot, not sure where the 15-18 ma comes from. If you mean the laser current as shown on the meter, it is not the current through the pot? In any case its still a small amount of current.



I am on vacation and when I return I plan to set up a test and create a truth table for all the controls on the PS and its actual light output power while inputting a known PWM signal and observing the PWM on a scope. From that we can hopefully identify the most reliable and predictable design choice for controlling the LPS.


---
**Mircea Russu** *July 09, 2016 19:52*

**+Don Kleinschnitz**​ my smoothie is not here yet but I've been doing some research about smoothieware sourcecode. Guess what, now it supports defining both laser_pwm_pin and laser_ttl_pin thanks to Fred27's pull request. The ttl pin is active during G1 like commands.  It should also support PPI in future, very useful for low power when 4ma is too much.


---
**Don Kleinschnitz Jr.** *July 10, 2016 03:16*

**+Mircea Russu** can you point me to the above definitions. Thanks


---
**Mircea Russu** *July 10, 2016 06:30*

**+Don Kleinschnitz**  sure thing [https://github.com/Smoothieware/Smoothieware/blob/master/src/modules/tools/laser/Laser.h](https://github.com/Smoothieware/Smoothieware/blob/master/src/modules/tools/laser/Laser.h) and it's made it to the master branch not only edge. 


---
**Mircea Russu** *July 10, 2016 06:31*

Laser_module_pin is deprecated but still works, the new name is laser_module_pwm_pin. 


---
**Don Kleinschnitz Jr.** *July 10, 2016 12:33*

**+Mircea Russu** Thanks. I have barely scratched the surface of the software for Smoothie so I am not sure exactly how this is used. Added it to my software learning to-do.

Does this change the configuration files that we are using?


---
**Mircea Russu** *July 10, 2016 15:17*

[http://smoothieware.org/forum/t-1418077/laser-module-pins-etc](http://smoothieware.org/forum/t-1418077/laser-module-pins-etc) here is the thread. Afaikt you set a pwm pin that goes to LPS IN, and you set another pin which goes to L/TL/TH. This way you don't keep LPS switched on by wire strap forcing L low and controlling everything from the pwm. 


---
**Don Kleinschnitz Jr.** *July 11, 2016 12:33*

**+Mircea Russu** I read the thread thanks. I have to admit at this time I do not see the need for two pins, but I need to noodle this some more.



My thinking follows: 

I assumed that the PWM always controlled the power and fire. Meaning that, from software if you wanted the laser "ON" a PWM signal was asserted and the PWM value set the power. As such when cutting the PWM would be at 100%.



I also have been assuming that TH/TL or +P was just a "Laser Enable" and was always ON as long as interlocks and "Enable" switch was on?



The thread argues that this total control from a PWM is bad for a glass Chinese tube? I do not understand how this can be unless there is some weird case where the PWM is not "0" when it should be? The failure mode was nver clearly explained?



Furthermore some K40 LPS's do not have a TH/TL but use a +P signal to control the laser enable. On K40 configured like this the only laser control signal that comes from the M2 is the L signal that is in the DC power connector. 



Therefore if there needs to be 2 control signals so as not to damage the tube then my K40 would damage the tube because only one signal comes from the controller to the LPS?? 



Logically that suggests that:

1. Two signals are not required from the software and there is something else I do not understand.

2. The tube in many K40's will have a shorter life because their control is designed improperly.



Lastly I assume that the TH/TL or +P signal is still in the interlock or enable circuit. I would hope no-one is running a K40 with the Laser Enable only from software.


---
**Don Kleinschnitz Jr.** *July 11, 2016 13:12*

**+Mircea Russu** Update:  links below seem to explain the situation more:



Seems there is a mode called PPI control that takes advantage of the fact that when a laser fires its initial power level is higher for a few milliseconds. By pulsing rather than leaving the laser on, you can get more power and the tube runs cooler. (See links below).



However, to use this mode you need to know where the laser is so that you can control the pulses relative to the position, not just a matter of pulse-ing it randomly. You need to pulse the laser every Xms of movement. Where Xms = the time that the laser is at high power.



Using PPI affords more light power without heating the tube as much as if you were to PWM at that same power.



So for example if 40W is the max laser power then to get an effective 40W power using PPI you could turn down the current and run a lower average power and the tube would last longer.



I suspect its not that running a K40 using PPM-only will lower the tubes life, its that you can extend/optimize its life using PPI, especially if you are doing a lot of high power cutting. Nothing is free however and this is at the added cost of firing-with-position control.



--------------------------

Laser PPI control:

[http://hackaday.com/2014/06/11/better-lasing-with-pulses/](http://hackaday.com/2014/06/11/better-lasing-with-pulses/)

[http://www.buildlog.net/blog/2011/12/getting-more-power-and-cutting-accuracy-out-of-your-home-built-laser-system/](http://www.buildlog.net/blog/2011/12/getting-more-power-and-cutting-accuracy-out-of-your-home-built-laser-system/)


---
**Mircea Russu** *July 11, 2016 14:29*

**+Don Kleinschnitz**  The LPS (mine at least) has 3 inputs:

-WP (water protection) enables or disables the laser output based on interlocks & rest

-IN sets the power: 0-5V or PWM

-L actual Fire command, active low

Moshi/Nano boaard controls only L and IN is set by the pot.

Let's assume we force (wire strap) WP and L low, that means laser continous fire at the IN set power. If IN pin is 0V (or 0%pwm) means "fire at 0 power" which might or might not be the same as "do not fire" by having L high. I don't know the LPS internals, what is active and what is not depending on L.

My concern: Smoothie with open-drain/mosfet output at IN with pot still in place. While Smoothie operates it can keep pwm at 100% which in turn keeps mosfet open and voltage at 0V. What if Smoothie hangs? Than pwm stops, mosfet closes and we have pot set voltage on IN and laser fires when it should not.


---
**Don Kleinschnitz Jr.** *July 11, 2016 19:33*

I am currently not recommending connecting PWM to IN. Rather connect PWM to L and leave the rest alone. Connect an open drain which will invert the PWM signal and provide a low true PWM to the L pin. Uggh! Not tested on mine yet.



What state would Smoothie PWM hang in ??? It could be off or on? How would you protect against this?



The "Laser Switch" and cover interlocks should remain in the circuit in all cases and when either is open that is the only time the unit is in the safe mode.


---
**Mircea Russu** *July 11, 2016 19:51*

**+Don Kleinschnitz**​​ 

1. WP remains as the protection loop, interlock, "Laser Switch", water flow sensor, water temperature, etc.

2. IN has the pot as pull-up (max power set) and takes inverted pwm via mosfet to modulate for lower than set max power. 100% pwm is the value set by pot. Inverted once in soft and second by mosfet. 

3. L is triggered by switch pin (gcode M3/M5) or laser_module_ttl_pin

That makes sure laser won't fire at smoothie boot.



Now questions: if you can put a scope probe on m2nano L pin does it ever pwm when engraving? I don't have my scope at hand to peek. 

Did you manage to get your hands on a broken LPS? I see a lot of people crying over dead LPS but nobody willing to share for autopsy. ﻿


---
**Don Kleinschnitz Jr.** *July 11, 2016 19:57*

**+Mircea Russu** nope never got a LPS to hack on :(.

I never scoped the L pin while running the M2 and now my electronics is disassembled ready for PWM testing.

Are you thinking that the stock K40 never uses PWM just turns on the L pin and lets it rip at whatever power the pot is set to???

I guess that is possible ....but how would you engrave like that???


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/YsUovUqyqP4) &mdash; content and formatting may not be reliable*
