---
layout: post
title: "My own cutting bed grid. Scrap metal!"
date: August 05, 2017 11:41
category: "Modification"
author: "Jorge Robles"
---
My own cutting bed grid. Scrap metal! Elevator luminary and a couple of cheap rails

![images/0aafa886454bb30064ab3d844fee559b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0aafa886454bb30064ab3d844fee559b.jpeg)



**"Jorge Robles"**

---
---
**Jim Fong** *August 05, 2017 18:45*

Ha that's much nicer than mine. 



Old toaster oven grate and scrap plywood standoffs...

I just cut new plywood to change focus height. ![images/40bcd770db5e19c9bdfcd03b1d85f5b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40bcd770db5e19c9bdfcd03b1d85f5b3.jpeg)


---
**Jorge Robles** *August 05, 2017 18:46*

If works works :)


---
**Gunnar Stefansson** *September 20, 2017 11:26*

Hehe nice, I tried those which is good. but the white paint leaves burn marks under the material which is pretty annoying!  so if you can try and dissolve the paint off or sandplast!! ;)


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/Bc3FURSwxdZ) &mdash; content and formatting may not be reliable*
