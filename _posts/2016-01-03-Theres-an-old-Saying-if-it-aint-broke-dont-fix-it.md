---
layout: post
title: "Theres an old Saying if it ain't broke don't fix it"
date: January 03, 2016 22:42
category: "Discussion"
author: "Tony Schelts"
---
Theres an old Saying if it ain't broke don't fix it.   I wish I'd took notice.   For some reason I decided I wanted to upgrade my Mirrors,  I bought a set that someone else had bought Molybdenum.  They looked great although ticker Still go them in.. It there any chance that some mirrors would absorb different laser light. I used to cut 3mm ply like butter at about 7ma and 4mm a second.  But even at 10 ma and 2mm a second cant get through even in 2 passes.  Any Suggestions.  Is it likely to be poor alignment.  I have been trying for hours to get them perfect. Thanks for any help





**"Tony Schelts"**

---
---
**Brooke Hedrick** *January 04, 2016 01:23*

I have seen those symptoms due to dirty lenses, dirty mirrors, and poor alignment.



How far off is your new alignment?



Did you check alignment in each of the 4 corners?



Are you seeing that the strength varies on different parts of the bed?


---
**ThantiK** *January 04, 2016 01:52*

Are you sure you have the mirror oriented with the correct surface facing the beam? 


---
**Scott Marshall** *January 04, 2016 01:58*

Kinda guessing here, but the one thing that occurs to me is those final lenses are available in 3 or 4 different focal lengths in each diameter. Where I bought mine (saite cutter) they offer 25.4, 38.1, 50.8, 63.5, 76.2 and 101.6mm in a variety of diameters.



You want the focal length to match the distance from the lens to the workpiece. Get the wrong one and your depth of field (focus zone) will be razor thin or too long.



 With the 1st problem you'd have great focus at the surface, but it will rapidly widen by the bottom surface. Easy to check, focus on the top surface, then shoot a piece of paper. Put another piece of paper at the back surface depth (3mm down) and shoot a test pulse again. The dots should be within 10% or so, ideally the same size. Keep the power as low as you can, and don't hold too long, you only want the paper to burn to the actual beam width.



If your depth of field is too long, the focus won't come down to a small diameter, but will be even and soft. Both issues will give poor cuts.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 03:27*

I haven't done mirror/lens upgrades, however I have noticed issues with my cutting at times not being able to cut where it previously could. The factors I've had cause it are either a) lens was upside down; b) mirrors out of alignment slightly or seriously dirty; c) focal length was out (my piece was ~3-4mm further away from the lens than it previously was due to me removing pieces).


---
**Tony Schelts** *January 04, 2016 08:35*

yep might of been duped 40.00 dollars for three.  Even if they were normal mirrors they should work just as well as the previous ones.  I thinks it's a case of going through alignment 2 or three times to get it spot on.  Thanks you guys for all your help and advice. 


---
**Scott Thorne** *January 04, 2016 10:43*

If the mirrors that you bought are 1mm thicker then your alignment is off...I'm positive...I went through the same thing...the ones that came in mine were 2mm thick....the ones I bought were 3mm thick...it throws the beam center off.


---
**Tony Schelts** *January 04, 2016 13:34*

Kind if got there eventually results are getting better.  really not bright when it comes to what screw to turn at what time.  The Alignment was off still slightly,  ran out tape  double sided sticky tape was too messy.  Thanks again all




---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 15:37*

**+Tony Schelts** Yeah took a long time for me to figure out how to adjust the screws to get the desired result. I found adjusting small amounts, 1 screw at a time assisted me in figuring out which way to turn & what they do.


---
**Kirk Yarina** *January 04, 2016 19:19*

**+Tony Schelts**  I stumbled across this youtube video a few days ago - about 1:35 into it they have some pictures showing which way to turn the screws one at a time to move the mirror the way you want.  At 1:44 they show two screw movements.  I'm sure there's better pics out there, but I haven't found one yet.


{% include youtubePlayer.html id="pw8Ro2vEEzY" %}
[https://youtu.be/pw8Ro2vEEzY](https://youtu.be/pw8Ro2vEEzY)


---
**Tony Schelts** *January 04, 2016 21:17*

very useful thanks


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/1WGgN6g2Cm6) &mdash; content and formatting may not be reliable*
