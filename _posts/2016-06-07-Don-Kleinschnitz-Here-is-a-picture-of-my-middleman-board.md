---
layout: post
title: "Don Kleinschnitz Here is a picture of my middleman board"
date: June 07, 2016 13:49
category: "Discussion"
author: "Brian Bland"
---
**+Don Kleinschnitz** Here is a picture of my middleman board. Gray wire is ground. White is 5v in. Green and yellow go to the X and Y endstops.

![images/83abc0fd17bb6fde480879c1935b7550.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/83abc0fd17bb6fde480879c1935b7550.jpeg)



**"Brian Bland"**

---
---
**Daniel Wood** *June 07, 2016 16:58*

Thats neat. I used ethernet cables with twised pairs but using these could be super tidy. 


---
**Don Kleinschnitz Jr.** *June 07, 2016 19:43*

Thank you. BTW are you using 24v and 5v from the existing Laser PS? I have no idea how much capacity it has.


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/HqcygKA3n89) &mdash; content and formatting may not be reliable*
