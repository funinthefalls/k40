---
layout: post
title: "I tested out my cooling system today"
date: May 28, 2017 00:26
category: "Modification"
author: "Martin Dillon"
---
I tested out my cooling system today.

Room temp 22.5C

Cool down for 30 min.      start 21.6C - end 20.9C

Etching at 10ma 35 min.  start 20.9C - end 20.0C

Cutting at 10ma 25 min.   start 20.2C - end 21.3C



Using an old water cooler that I saved from the trash.  Don't know what its rated cooling is but I obviously can't cut none stop but engraving should be okay.

![images/a0b4d5504d8e532f399268f9d7ac4c09.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0b4d5504d8e532f399268f9d7ac4c09.jpeg)



**"Martin Dillon"**

---
---
**Nate Caine** *May 28, 2017 00:52*

Interesting results!



(1) Those etching and cutting numbers are more than adequate.  Are you mainly concernd that cutting beyond 30-minutes would be too hot?



(2) During the 30-minute "cool down" the temperature fell less than 1°C.  I would expect that end temperature to be a lot lower.  (It's a water cooler!)  What accounts for this minor temperature drop?  Was the water circulating during "cool down", and not just cooling the water in the bucket?



(3) Have you considered a small (e.g. 20cm ~8") room fan to blow over the coils?  You might be surprised at the performance improvement of the condenser coils with a little breeze blowing over it.  Have you measured the coil temperature?



(4) Have you been able to characterize the laser tube output power vs temperature?  For example, during a 30-minute etching job, is the power change significant enough that you'd see a change in the results?






---
**Steve Clark** *May 28, 2017 01:22*

I think it is working well for you. I like the temps your getting at this point. However,  what is your room temperature? That would be important to know and tell you how well the cooler is working. 



IMO - Cooling the water is important  and I've come to the conclusion that a balanced temperature within the range of 19C to  2and 22C is my goal to hopefully give me a long tube life along with controlling the ma input.




---
**Martin Dillon** *May 28, 2017 03:19*

I was just posting as an FYI.  I did list the room temp 22.5C.  The pump was circulating the whole time.  Also, I did have a box fan blowing on the compressor and the coils but not during the initial cool down period.  I don't think the chiller it is running at peak performance.  I did rescue it as it was headed to the dump.   I would like to put a temp. sensor on the coils to see how cold it is getting.  


---
**Tim Gray** *May 28, 2017 14:41*

Blow a fan across the coils and you will significantly reduce the heat gain, or even nullify it.




---
**Martin Dillon** *May 28, 2017 15:26*

I meant the cooling coils.  Right now I have my thermometer just sticking in the top of the bucket


---
**Steve Clark** *May 28, 2017 16:42*

**+Martin Dillon** Your right you did mention the room temp… my mistake.



 I should also add that the temperature range I want may not be sustainable depending on the ambient humidity. I wouldn’t want a wet tube.



I like Tim's idea of adding the fan.




---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/HestogYMVCc) &mdash; content and formatting may not be reliable*
