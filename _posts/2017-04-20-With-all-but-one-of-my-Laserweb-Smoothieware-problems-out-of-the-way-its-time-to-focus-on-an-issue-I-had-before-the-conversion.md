---
layout: post
title: "With all but one of my Laserweb/Smoothieware problems out of the way, its time to focus on an issue I had before the conversion"
date: April 20, 2017 19:44
category: "Hardware and Laser settings"
author: "Kelly Burns"
---
With all but one of my Laserweb/Smoothieware problems out of the way, its time to focus on an issue I had before the conversion.  My lost laser power.  I believe this was relatively sudden, but didn't notice it completely until a realigned the laser and it didn't improve anything.



I assume its a Tube or Powersupply failure (or soon to fail) issue, but don't want to buy the wrong thing.



I seem to be getting about 50% less power than I did 4 months ago.  The Power Supply makes a whining noise, but there doesn't seem to be any arcing.  I have heard the arcing noise and this is a bit higher pitch and doesn't sound quite the same. I do not see any arcing in the tube either.  



The ma meter still reads the same as always, so the current definitely hasn't change.  I'm just having to put the power on 80% to get it to engrave any depth.  



I used to be able to cut 3mm Acrylic easily with 2 passes at 10ma.  Now I can barely cut on 16ma and 4 or 5 passes.



I have distilled water in my closed container.  I have read about issues with water causing laser power loss or arcing with Tap Water or other solutions.  I add 1 gallon every couple of months, so some of this water is nearly a year old?  Is that a problem?  







**"Kelly Burns"**

---
---
**Adrian Godwin** *April 20, 2017 19:56*

If you're losing a gallon every couple of months to evaporation, the water will be getting 'less distilled' as the pollutants are just building up. Better to flush it out and change it.



I got a similar effect with tapwater. It sounded a bit fizzy, the current was normal, but no laser power. Although there's no arcing visible, the tube discharge isn't straight at the ends but deviates towards the electrodes.


---
**Anthony Bolgar** *April 20, 2017 19:59*

One thing to check is to make sure the lens is the right way up, also look for a cracked or damaged mirror. My gut feeling is that it is an optical issue as you are still getting the same current. What do you think **+Don Kleinschnitz**?


---
**Kelly Burns** *April 20, 2017 21:11*

Thanks.  I have experience those issues, but I have been doing this a while now.  I'm very good at the Optical and alignment aspect of it.  This problem existed before I took my machine completely a part and remounted everything.  So it's been completely reworked and precisely alilgned.   



The "cracked lens" is definitely a good suggestion as I experienced that same thing shortly after getting the laser.  



 


---
**Stephane Buisson** *April 20, 2017 22:14*

dirt (smoke on mirror), or mirror coating is going when wiping it. possibly.


---
**Kelly Burns** *April 20, 2017 22:19*

I keep multiple sets of clean mirrors and focus lens.  I'm confident it's not an optical issue.   



I'm going to flush cooling system and replace with new water.  



If that doesn't help, I'll start with new power supply.  If that's not it, I'll have a power supply for my next custom build.  


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/ZuxmLKtTf6s) &mdash; content and formatting may not be reliable*
