---
layout: post
title: "I have a k40. I recently replaced the pot with a 5k pot and my meter due to the pot being sporadic and the meter would just keep hitting max"
date: April 13, 2017 10:14
category: "Smoothieboard Modification"
author: "Chris Menchions"
---
I have a k40. I recently replaced the pot with a 5k pot and my meter due to the pot being sporadic and the meter would just keep hitting max.



Now the most I see in the meter via the test button is 8ma. Tube otw out?



Thanks





**"Chris Menchions"**

---
---
**Don Kleinschnitz Jr.** *April 13, 2017 10:47*

Likely a worn out tube.


---
**Chris Menchions** *April 13, 2017 10:48*

Exactly my thoughts


---
**Alex Krause** *April 13, 2017 13:28*

Pretty sure the stock Pot is a 1k


---
**Don Kleinschnitz Jr.** *April 13, 2017 13:30*

**+Alex Krause** I have seen both 1K and 5K used. I have 5K in mine :).


---
**Chris Menchions** *April 13, 2017 13:30*

**+Alex Krause**​ no idea I seen ppl replacing them with 5k for better control that's why I did it


---
**Don Kleinschnitz Jr.** *April 13, 2017 13:33*

**+Chris Menchion** the resistance doesn't provide better control its that pot type that does that. The pot we use as a replacement is a multi-turn pot that has better granularity (more turns for a given resistance). A 5K was all that was available in that type pot. 


---
**Chris Menchions** *April 13, 2017 13:34*

Ok


---
**Chris Menchions** *April 14, 2017 10:04*

Update: **+Don Kleinschnitz** and I have come to the conclusion that it has to be the readout from the power supply. The meter works and the machine cuts and engraves. That same psu has interlock issues where the test button bypasses the interlock. Will be getting a new PSU soon hopfully that fixes it.




---
*Imported from [Google+](https://plus.google.com/104576389148296391165/posts/c9rV7wZcXV6) &mdash; content and formatting may not be reliable*
