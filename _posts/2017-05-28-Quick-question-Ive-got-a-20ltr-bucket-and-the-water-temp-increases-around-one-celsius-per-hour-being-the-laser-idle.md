---
layout: post
title: "Quick question. I've got a 20ltr bucket and the water temp increases around one celsius per hour being the laser idle"
date: May 28, 2017 16:39
category: "Hardware and Laser settings"
author: "Jorge Robles"
---
Quick question. I've got a 20ltr bucket and the water temp increases around one celsius per hour being the laser idle. Pump seems running well so far, but maybe is faulty?





**"Jorge Robles"**

---
---
**Jim Fong** *May 28, 2017 16:50*

I know my pump, a flojet, gets warm while running. The heat has to go somewhere and will warm up the 5gallon bucket of water about the same rate. 



I have a medical chiller that I need to hook up one day. 

[https://imgur.com/a/c0RjA](https://imgur.com/a/c0RjA)


---
**Ned Hill** *May 28, 2017 16:51*

The running pump is creating the heat.  All pumps will do this, it's normal.


---
**Jorge Robles** *May 28, 2017 16:52*

Thanks I supposed too. I would go for 30ltr bucket then :)


---
**Ned Hill** *May 28, 2017 16:54*

Most people just add ice packs or frozen water bottles to their buckets.  I keep about 6 frozen 1/2 liter bottles in the freezer that I use and swap as needed.




---
**Ashley M. Kirchner [Norym]** *May 28, 2017 18:07*

Same here, frozen bottles. Works great. 


---
**Cesar Tolentino** *May 29, 2017 00:13*

Same here with frozen bottled water.


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/2U9SJQehVJP) &mdash; content and formatting may not be reliable*
