---
layout: post
title: "Hi all, this is my first post here"
date: September 29, 2016 20:10
category: "Modification"
author: "Dennis Luinstra"
---
Hi all, this is my first post here. A few weeks ago we bought a K40-III. Since then I read your posts, they are very learnfull!



We bought the K40 for hobby purposes, to cut and engrave 3 and 4mm birch plywood and 3 and 4mm acrylic.



I bought some upgrades and parts from Ebay and LO. e.g. a dsp, air assist nozzle, new lens and mirrors, water flow protection etc. I don't have a lot of spare time at the moment, so it will take a time before it will be competed.



I designed a new control panel, for the dsp and switches and I am not sure what kind of material I should use. Is 4mm acrylic strong enough for it,? or will it break easily? and do you have suggestions on the color of the acrylic (I was thinking of black acrylic an then engrave and cut it out with the K40, I want it to be non-transparant). Or should I use 2mm aluminium and cut it by hand?



Here is a picture of my design in white the current and new cut out in the metal of the K40, red is cutting line and yellow the outline of the dsp/switches/meters:





![images/d261212d80ec68abbdf38575ce2f361a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d261212d80ec68abbdf38575ce2f361a.png)



**"Dennis Luinstra"**

---
---
**greg greene** *September 29, 2016 20:19*

what is this dsp of which you speak?


---
**Dennis Luinstra** *September 29, 2016 20:21*

I bought the Element E5 DSP upgrade kit from LO.


---
**Anthony Bolgar** *September 30, 2016 03:21*

I made my panel out of clear acrylic that I spray painted black on the backside, engraved the back side, the put white paint in the engraved areas. From the front it looks like shiny black acrylic with white lettering, and can't damage the paint because it is on the back side.


---
**Alex Krause** *September 30, 2016 04:48*

**+greg greene**​ a DSP is a really expensive laser controller that comes stock on more expensive machines. They usually range in price of 300-400$ 


---
**greg greene** *September 30, 2016 12:57*

Any advantage to them? Say over a smoothie?


---
**Don Kleinschnitz Jr.** *September 30, 2016 14:59*

**+greg greene** I almost went with the lo DSP. The problem I avoided with the smoothie is that the lo DSP proprietary protocol limits your design tool choices.

Smoothie gets you to Gcode which is not proprietary. A good example of more open tools is laserweb. 


---
**greg greene** *September 30, 2016 15:03*

Good to know


---
**Dennis Luinstra** *September 30, 2016 21:39*

**+Anthony Bolgar**​, what is the thickness of the acrylic you used? The text engraving at the back side is very clever! 


---
**Anthony Bolgar** *September 30, 2016 21:43*

I used 4mm acrylic, but I wishI would have used 6mm, the 4 is almost a little flimsy, but works. 6mm would be perfect . And back painting and engraving is a trick everyone needs to learn, you get some really nice results that way.


---
**Anthony Bolgar** *September 30, 2016 21:49*

[https://plus.google.com/+AnthonyBolgar/posts/A7ppkR1EgBY](https://plus.google.com/+AnthonyBolgar/posts/A7ppkR1EgBY)


---
**Dennis Luinstra** *October 01, 2016 17:07*

Looks great! 


---
**Dennis Luinstra** *October 08, 2016 08:31*

Decided to go for an aluminum front plate because that was easier for me. I am happy with the results so far. The following weeks I have to do the wiring. 

![images/f9f7df428e8d72e625ff3f520a5c4727.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9f7df428e8d72e625ff3f520a5c4727.jpeg)


---
*Imported from [Google+](https://plus.google.com/101821475060654077048/posts/CPrVA9gEWDc) &mdash; content and formatting may not be reliable*
