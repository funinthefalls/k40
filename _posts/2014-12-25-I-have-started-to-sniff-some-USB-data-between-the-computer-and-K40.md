---
layout: post
title: "I have started to sniff some USB data between the computer and K40"
date: December 25, 2014 21:37
category: "Software"
author: "Anton Fosselius"
---
I have started to sniff some USB data between the computer and K40. I hope to use this data to reverse engineer a way to control it from my linux machine.



I started collecting the data when the printer was connected and moshidraw started. i then did some manual control X+ Y+, X-, Y-



Open with wireshark

[https://drive.google.com/file/d/0B_qB0LkRsAT6NFYyVGNwc1d5SGc/view?usp=sharing](https://drive.google.com/file/d/0B_qB0LkRsAT6NFYyVGNwc1d5SGc/view?usp=sharing)



Next step is to try to send the sniffed data back to the K40 from linux and get it to move.



I will later try to "fish out" the gcode from the messeges by cutting simple shape. ex 3 lines of various length, circles of various sizes and so on.





**"Anton Fosselius"**

---
---
**Thomas Oster** *January 01, 2015 22:16*

Hi. If you get basic movements to work,I can create a VisiCut driver and you get the rest of the cake for free


---
**Anton Fosselius** *January 02, 2015 07:26*

**+Thomas Oster**​ that would be great!   


---
*Imported from [Google+](https://plus.google.com/115118068134567436838/posts/RB6EN3Ns4F5) &mdash; content and formatting may not be reliable*
