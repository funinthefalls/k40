---
layout: post
title: "Looking for some good line art? Google \"coloring pages\" and see what you have been missing!"
date: November 20, 2017 13:02
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Looking for some good line art?



Google "coloring pages" and see what you have been missing!





**"HalfNormal"**

---
---
**Ned Hill** *November 20, 2017 16:34*

Cool!  I'll have to remember that.


---
**Vanessa Darrey** *November 20, 2017 17:46*

Genius! 


---
**Madyn3D CNC, LLC** *December 14, 2017 14:57*

HAHA! I do this for vector images when vinyl cutting, never thought of it for use with the laser. My laser is mostly used for text/lettering on our box mods. 

![images/75f2d786fb2fc60eb075dac1962fbb35.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/75f2d786fb2fc60eb075dac1962fbb35.jpeg)


---
**Daniel Brown** *December 30, 2017 16:15*

WOW!!!!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/gLVGc6H9WYk) &mdash; content and formatting may not be reliable*
