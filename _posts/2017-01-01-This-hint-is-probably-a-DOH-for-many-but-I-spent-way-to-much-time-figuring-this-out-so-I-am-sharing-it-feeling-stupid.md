---
layout: post
title: "This hint is probably a DOH! for many but I spent way to much time figuring this out so I am sharing it feeling stupid"
date: January 01, 2017 18:01
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
This hint is probably a DOH! for many but I spent way to much time figuring this out so I am sharing it feeling stupid.



I was having recurring problems with DXF exports and imports from SU. 

Sometimes they worked and other times they did not.



Turns out that the object you want to export must be flat on the x axis to export anything but a line (for the x axis).



In the picture below the vertical object (even if you select it face on) will not export correctly but both of those on the right will.



DOH!

![images/379ba7cd7e794a9c8a48586110338547.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/379ba7cd7e794a9c8a48586110338547.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**greg greene** *January 01, 2017 18:13*

Don, this is more of an Aha ! moment than a DOH one :)


---
**Cesar Tolentino** *January 02, 2017 00:17*

In my dxf export i have two options. 3d and 2d. I always use 2D, also make sure that I have saved SCENE that is flat view without perspective.




---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/dv6K6Y3R5uh) &mdash; content and formatting may not be reliable*
