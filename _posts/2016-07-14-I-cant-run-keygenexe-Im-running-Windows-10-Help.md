---
layout: post
title: "I can't run keygen.exe. I'm running Windows 10. Help!"
date: July 14, 2016 01:28
category: "Software"
author: "Greg Wilder"
---
I can't run keygen.exe.  I'm running Windows 10.  Help!





**"Greg Wilder"**

---
---
**Jim Hatch** *July 14, 2016 02:01*

That's because Win10 thinks it's got a virus. You can try to right click and "run as administrator" or dig into the security settings and disable security.



Or grab a legit copy of Corel off eBay from someone getting rid of theirs. Just don't get a Home & Student edition. 


---
**Greg Wilder** *July 14, 2016 02:35*

Thanks.  I already made the mistake of buying the H & S edition.


---
**Jim Hatch** *July 14, 2016 02:38*

There's probably a way to make H&S work by creating a copy of the exe and naming it what the normal Corel executable is called. But I think you'd also need to make a registry entry for it too.


---
**Greg Wilder** *July 14, 2016 03:22*

Thanks, Jim.


---
*Imported from [Google+](https://plus.google.com/114039464943103282551/posts/4N5DHzi34NN) &mdash; content and formatting may not be reliable*
