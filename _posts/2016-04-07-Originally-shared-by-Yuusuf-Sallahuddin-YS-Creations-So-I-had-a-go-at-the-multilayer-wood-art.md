---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) So, I had a go at the multilayer wood art"
date: April 07, 2016 16:15
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



So, I had a go at the multilayer wood art. I used a reference photograph of myself to manually draw up all the levels of the image. 5 layers in total.



I'm pretty happy with the overall result. 1 pass @ 10mA @ 12mm/s on 3mm plywood.



Not glued together, but just positioned in place. Took a while as I kept bumping things because they weren't glued.



Some of the parts were too small to be usable (e.g. I did have eyelids in the file cut out, but they turned out <1mm in width, so almost impossible to position).



![images/cdb68276732aa8e00b5909688fb021fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdb68276732aa8e00b5909688fb021fd.jpeg)
![images/b5a41df86e2cbd59832d07527795bf8c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b5a41df86e2cbd59832d07527795bf8c.png)
![images/a451f383e6edcfdf9af2f5bc274e762b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a451f383e6edcfdf9af2f5bc274e762b.jpeg)
![images/395cfa0c9f79703620ce6789c812c63a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/395cfa0c9f79703620ce6789c812c63a.jpeg)
![images/249a577c9e87cedb6b95d3ef454435a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/249a577c9e87cedb6b95d3ef454435a9.jpeg)
![images/6bd696455db90ff6cdc36ffef517854f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6bd696455db90ff6cdc36ffef517854f.jpeg)
![images/66d8df6b59cd9d693605e8e7340bac1d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/66d8df6b59cd9d693605e8e7340bac1d.jpeg)
![images/3cec71498f74518c5f960487c756a12f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3cec71498f74518c5f960487c756a12f.jpeg)
![images/c9b27dfc9f619ddbd8c1b2a8f7122beb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c9b27dfc9f619ddbd8c1b2a8f7122beb.jpeg)
![images/9997bd13cc156d32353a42b14f1f8528.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9997bd13cc156d32353a42b14f1f8528.jpeg)
![images/778a2b5632603f85a32def49d186c916.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/778a2b5632603f85a32def49d186c916.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 16:16*

**+Scott Thorne** **+I Laser** Here we go, first attempt.


---
**Scott Thorne** *April 07, 2016 16:27*

**+Yuusuf Sallahuddin**...that turned out awesome...now I want to get home to my engraver...I'm having withdrawals....lol..great job. 


---
**Stephane Buisson** *April 07, 2016 16:29*

you make me think to a 3D jigsaw puzzle for kids or a sort of profiler game  ;-))


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 16:34*

**+Scott Thorne** Can't wait to see what you put together.



**+Stephane Buisson** That's an interesting idea for "Profile Game". I remember a game when I was younger called "Guess Who" where you had to guess based on yes/no questions. E.g. Does you person have glasses? Are they male? Do they have a beard? etc. Then you would flip the little cards down that are not relevant, basically using process of elimination. You could make a similar kind of game with multiple different face types/accessories (e.g. glasses, beard, moustache, etc).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 16:36*

**+Stephane Buisson** Speaking of "Guess Who", here's a reference image:



[http://static.fjcdn.com/pictures/Guess+who+the+game_2eec1c_4740439.jpg](http://static.fjcdn.com/pictures/Guess+who+the+game_2eec1c_4740439.jpg)


---
**I Laser** *April 07, 2016 22:34*

Nice work **+Yuusuf Sallahuddin** ! Looks very cool, personalised profiles, you might be able to corner the market! ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 00:57*

**+I Laser** Well, I could have a try to corner the market at least. I still have a fair few ideas up my sleeve for things I can do using this multilayer technique.


---
**I Laser** *April 08, 2016 10:42*

Do you mind sharing the original pic you based this off? I assume it looks like you lol... 



I think it could be a product people would like, especially if you add some personalised text too. Great 40th birthday gift etc.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2016 11:19*

**+I Laser** Yeah, I will find the original pic & attach it in the same album folder. I think you're onto something with some kind of personalised text too for special events/gifts.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/ZbPXvvmZQeV) &mdash; content and formatting may not be reliable*
