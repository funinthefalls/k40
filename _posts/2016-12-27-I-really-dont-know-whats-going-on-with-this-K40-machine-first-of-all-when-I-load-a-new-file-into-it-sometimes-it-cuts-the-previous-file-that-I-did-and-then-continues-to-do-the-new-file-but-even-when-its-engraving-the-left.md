---
layout: post
title: "I really don't know whats going on with this K40 machine, first of all when I load a new file into it sometimes it cuts the previous file that I did and then continues to do the new file but even when its engraving the left"
date: December 27, 2016 17:11
category: "Discussion"
author: "john dakin"
---
I really don't know whats going on with  this K40 machine, first of all when I load a new file into it sometimes it cuts the previous file that I did and then continues to do the new file but even when its engraving the left hand side of the work is not being cut , the only way I can seem to get it to work is to use the cutting dropdown, it will then cut all around the part, now heres the weird bit, when it does the first cut, its all fine and in proportion but when it does the second cut it overlaps





**"john dakin"**

---
---
**Richard Vowles** *December 27, 2016 17:19*

I suspect if you let people know what your setup is, more help would be forthcoming. What software are you using? What hardware?


---
**john dakin** *December 27, 2016 17:22*

I'm using Laserdrw and the machine is a standard K40 with a standard M2nano board


---
**Ned Hill** *December 27, 2016 17:35*

**+john dakin** to be honest most people don't use laserdraw so I'm not sure how much support you are going to be able to find here.  There appear to be a number of tutorials online that might be able to help.  However, one thing to check is if the device ID is set to the number that is on your M2nano board.


---
**john dakin** *December 27, 2016 17:50*

Yes its all set correctly, ive got a feeling either the mirrors are badly out of alignment or something wrong with the laser tube


---
**Ned Hill** *December 27, 2016 17:55*

What you are describing doesn't sound like an alignment or tube issue, but does sound software related.  But if you haven't, you should do a full alignment check anyway.


---
**greg greene** *December 28, 2016 12:55*

Check belt tension and ensure you workpiece does not move due to the vibration of the machine as it's making that first cut.


---
*Imported from [Google+](https://plus.google.com/110405157088691728751/posts/PohTBwDrEFr) &mdash; content and formatting may not be reliable*
