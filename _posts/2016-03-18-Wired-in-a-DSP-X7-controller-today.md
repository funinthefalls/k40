---
layout: post
title: "Wired in a DSP X7 controller today..."
date: March 18, 2016 00:34
category: "Modification"
author: "Brien Watson"
---
Wired in a DSP X7 controller today...  It works awesome!  Best investment yet!







**"Brien Watson"**

---
---
**Jerry Martin** *March 18, 2016 23:20*

What Power supply do you have working this. Looking for a wire diagram for mine?


---
**Brien Watson** *March 19, 2016 01:42*

I'll get a picture for you Jerry.  It seems confusing at first, but then it all comes together.  Did you order the kit?


---
**Jerry Martin** *March 19, 2016 03:16*

Yes, I did. Did you keep the Potential Meter, some pictures, I see everything gone except the on and off switch.


---
**Brien Watson** *March 19, 2016 14:58*

I installed the digital one that came with the kit that I bought.  It came with X7 controller, digital meter, new laser head, stepper motors and emergency stop switch.


---
**Brooke Hedrick** *March 21, 2016 05:50*

**+Brien Watson** could you post some pics of how you wired in the emergency stop?


---
**Lori Martin** *March 22, 2016 00:11*

I'm trying to figure that out myself?


---
**Jerry Martin** *March 22, 2016 00:29*

Brook, Can you give me a push as I'm trying to figure this out myself!


---
**Brooke Hedrick** *March 22, 2016 01:37*

**+Jerry Martin** That is an item I haven't wired in yet.  I kept the original on/off switch so it wasn't a big priority for me!  I just worry someone will see it on my panel, hit it, and it won't do anything!


---
**Brien Watson** *March 26, 2016 02:19*

The emergency stop switch has tow sides to it.  One side normally open, the other side normally closed.  So worth the power button up and in its running position, do a continuity test to find which two poles show you continuity.  Run your main power that comes in to your PSU (power supply unit) from the emergency switch go to your toggle or push switch, from there run to your PSU.  It's just an inturupter switch that is first in the chain.  Use the red wires.  


---
**Brien Watson** *March 26, 2016 02:20*

I can't add a picture to this post can I?


---
*Imported from [Google+](https://plus.google.com/111917591936954651643/posts/NVbv2y8QqLb) &mdash; content and formatting may not be reliable*
