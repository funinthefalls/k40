---
layout: post
title: "Replace scroll saw with laser :)"
date: July 11, 2017 05:23
category: "Discussion"
author: "Ned Hill"
---
#K40ProjectIdeas Replace scroll saw with laser :)





**"Ned Hill"**

---
---
**Don Kleinschnitz Jr.** *July 11, 2017 13:11*

**+Ned Hill** I wonder if your could use an acrylic layer instead of the LED's under the profile. Etch a profile in the acrylic so it is positioned at the edge of the profile of the wood? 


---
**Stephane Buisson** *July 11, 2017 14:18*

I would go for other kind of light.

like this one :

[ebay.co.uk - Details about  Neon LED String Strip Light Glow EL Wire Rope Tube Car Dance Party + Controller](http://www.ebay.co.uk/itm/Neon-LED-String-Strip-Light-Glow-EL-Wire-Rope-Tube-Car-Dance-Party-Controller-/382086328216?var&hash=item58f61eaf98:m:myMF6n8plAMAePCWfBYVceA)


---
**Don Kleinschnitz Jr.** *July 11, 2017 15:22*

**+Stephane Buisson** I think that's a better choice of lights than the string the author used. 



I was also trying to avoid all the routing for wires, gluing etc.

Bolt layers together like this:

.. Dark wood background layer

.. Acrylic layer with etched silhouette slightly displaced from the next layer

.. Laser cut wood silhouette



Light the acrylic from the edge with LED string. This method may not create enough light?


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/ari4S9hFV1C) &mdash; content and formatting may not be reliable*
