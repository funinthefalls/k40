---
layout: post
title: "Some of my etchings. top left has a bit of white added, oops included"
date: August 17, 2016 04:39
category: "Object produced with laser"
author: "Robi Akerley-McKee"
---
Some of my etchings. top left has a bit of white added, oops included.  Bit of luann plywood I've cut height gauges from, and the contour of the side panel I took from this picture into inkscape.  Tinted acrylic is the cut out for the LCD panel.



![images/b8c23c86d4044d4d40af7456aa6432f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8c23c86d4044d4d40af7456aa6432f5.jpeg)
![images/3c4cd890d49a44d57d9ae42ed089439f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3c4cd890d49a44d57d9ae42ed089439f.jpeg)

**"Robi Akerley-McKee"**

---
---
**Robi Akerley-McKee** *August 17, 2016 04:48*

just remembered my first laser "job"   using the moshidraw board and software to do some engraving onto some hangers for my wifes booth.


---
*Imported from [Google+](https://plus.google.com/103866996421752978311/posts/Rh4Z3tLLAV8) &mdash; content and formatting may not be reliable*
