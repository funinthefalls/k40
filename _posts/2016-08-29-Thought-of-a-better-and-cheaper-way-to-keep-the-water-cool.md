---
layout: post
title: "Thought of a better and cheaper way to keep the water cool"
date: August 29, 2016 14:58
category: "Discussion"
author: "Robert Selvey"
---
Thought of a better and cheaper way to keep the water cool. I'm going to keep my water in a mini fridge and drill in the top of the fridge to run my lines. Should work out really well.





**"Robert Selvey"**

---
---
**greg greene** *August 29, 2016 15:07*

And - it will keep your beer cool too!


---
**Jim Hatch** *August 30, 2016 00:01*

**+greg greene**​ Actually a kegerator is another commonly used chiller for lasers 🙂


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/aTCUm3wx7RC) &mdash; content and formatting may not be reliable*
