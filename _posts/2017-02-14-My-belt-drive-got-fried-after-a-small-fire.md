---
layout: post
title: "My belt drive got fried after a small fire.."
date: February 14, 2017 11:58
category: "Original software and hardware issues"
author: "Pablo Verity"
---
My belt drive got fried after a small fire.. :)



Anyone got a source for a new one? I've just fitted a new 6mm wide belt, and when testing the run, I discovered the belt was running over the drive wheel on the stepper... it's running over the cogs. Bugger, obviously the wrong pitch.



Measuring the old belt, it seems like it's got 2.5mm pitch, and is 4.87mm wide (prob 5mm).



Can't find a supplier anywhere...





**"Pablo Verity"**

---
---
**greg greene** *February 14, 2017 12:42*

Have you tried Light Objects - If you can't find one - maybe a new cog from where you got the belt?


---
**Don Kleinschnitz Jr.** *February 14, 2017 14:18*

[lightobject.com - Y axis belt for K40](http://www.lightobject.com/Y-axis-belt-for-K40-P760.aspx)

[http://www.lightobject.com/X-axis-belt-for-K40-P761.aspx](http://www.lightobject.com/X-axis-belt-for-K40-P761.aspx)


---
**Kostas Filosofou** *February 14, 2017 14:24*

**+Pablo Verity**​​​ the belt is standard MXL belt 6mm wide (.080" & 2.03 mm Pitch)... you can find easy on eBay or any other stores selling belts


---
**Pablo Verity** *February 15, 2017 08:31*

Thanks, guys. I'd measured the pitch with a vernier, and it came out to 2.5mm, so I ordered some from [http://ooznest.co.uk/3D-Printer-Mechanical-Parts/Belts-Pulleys/T2.5-Pulley-Timing-Belt-Kits](http://ooznest.co.uk/3D-Printer-Mechanical-Parts/Belts-Pulleys/T2.5-Pulley-Timing-Belt-Kits). After reading your replies, I'll order some 2mm as well. It'll come in handy, one day! I have the standard board, there's no-where to set the change of pitch, even with new pulleys, it won't be the same? I'll upgrade it sometime, when I can figure out the best way (read easiest..).

Have to say, Google hasn't got this groups thing right, it needs a revamp...



Many thanks for the pointers, folks, seems like I'll be up & running in a couple of days, now.



 :)

[ooznest.co.uk - T2.5 Pulley & Timing Belt Kits](http://ooznest.co.uk/3D-Printer-Mechanical-Parts/Belts-Pulleys/T2.5-Pulley-Timing-Belt-Kits)


---
**Don Kleinschnitz Jr.** *February 15, 2017 14:35*

**+Pablo Verity** what do you mean by: "Have to say, Google hasn't got this groups thing right, it needs a revamp..."


---
**Pablo Verity** *February 15, 2017 19:53*

It.seems awkward to scan through the posts... the layout is less than intuitive imo. 


---
**Don Kleinschnitz Jr.** *February 15, 2017 20:33*

**+Pablo Verity** ah yes, finding stuff is difficult.


---
**Pablo Verity** *February 16, 2017 16:37*

Just fitted a GT2 belt to my K40. It's running over the cogs on the pulley every 10mm, or thereabouts, obviously not the correct pitch for this standard machine. The pitch is slightly more than 2mm, but not 2.5! I'm thinking it's going to be imperial, a standard practice for China, the cheapest... So I'm now on the lookout for 3/32" pitch x 3/16", which is why it measure 4.8mm width, not 5. With the GT2, I burnt an 80mm square and got 78.6 on the X-axis.

If I fit a new GT2 pulley, would it end up correct, again (pulleys are not my strongest subject..) ?

I could always try it.

Surprised others haven't had this problem...

Ain't life grand?


---
**Kostas Filosofou** *February 16, 2017 16:40*

**+Pablo Verity** I have answered to you above what's the correct belt for your machine ... You're get in trouble for no reason...


---
**Pablo Verity** *February 16, 2017 17:21*

Just came to the same conclusion, Konstantinos! The confusion I've wrought might well be due to the many different makers of these machines... I'm pretty sure some might take GT2 belts, but mine sure doesn't, unless I butcher it..!  Thanks for your input. Just for a laugh, I might try a GT2 pulley on it, to see if the belt/pulley combo will work, pretty sure it'll be undersized. Ho hum! Onwards & upwards!  :)




---
**Kostas Filosofou** *February 16, 2017 17:31*

You can try it but you must also change the steps per mm in config .. I don't know if it's possible in stock board..


---
**Pablo Verity** *February 16, 2017 17:39*

Yes, I can do that easily in Mach3, but I have the stock board using that bloody awful LaserDrw. No options to change anything. I'm pretty sure I'd have to flash it. That's too much trouble. I may upgrade with a TB6560 board I have, or Arduino. Eventually.



Just found some suppliers of MXL belts in the UK. So should be up & running in a few days... :)


---
**Kostas Filosofou** *February 16, 2017 17:40*

Good glad to hear it!


---
**Pablo Verity** *February 16, 2017 17:41*

Got some honeycomb coming for my bed. Then of course, I'll have to think of an adjustable bed... The list goes on, & on...


---
**Kostas Filosofou** *February 16, 2017 17:43*

Yes I know I know... the list that never ends...


---
*Imported from [Google+](https://plus.google.com/108260776734176878360/posts/g2LbjGfniAK) &mdash; content and formatting may not be reliable*
