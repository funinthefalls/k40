---
layout: post
title: "firmware & poulie solutions, please read comments on original post (C3D community) Originally shared by Mischa Repsev Skipping steps and losing hair...."
date: June 12, 2018 13:06
category: "Smoothieboard Modification"
author: "Stephane Buisson"
---
firmware & poulie solutions, please read comments on original post (C3D community)



<b>Originally shared by Mischa Repsev</b>



Skipping steps and losing hair....

Can anybody help me out.

X axis on my K40 is skipping a lot while engraving.

When doing vectors or cutting it seems to be working just fine.

So far I tried the most tips I could find in this and other communities.



First installed a 24V PSU to supply the cohesion3d mini and steppers.



Belt tension checked and adjusted (both ways, also after driver replacement)



Replaced the a4988 on the Cohesion3d mini with a drv8825 (adjusted current to optimal) 



Replaced the drv8825 to tmc2100 (again current adjusted) and switched between stealth chop and spread cycle. Much quieter now, but still skipping.



Installed the Jim Fong firmware with the SD card disabled.

Picture below is 150mm/sec (left from lightburn, on the right, same file from SD card without pc connected.

Before I go and exchange the stepper, and modify the gantry to run GT2 timing belts, and also try external stepper drivers, could it be a software issue?



I read some about GRBL, but not clear which version I'll need for this system, and if there is a config file I can change (pin settings especially) to move the y axis to the z output..

Might have pulled the last driver to fast from the board before power was gone..

In Smoothie it was fixed easily, but have no clue how to compile a new version of grbl-lpc with y on the z axis.



Regards,

Mischa



![images/5162d92aecb331145c647e609d7df69d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5162d92aecb331145c647e609d7df69d.jpeg)



**"Stephane Buisson"**

---
---
**Anthony Bolgar** *June 12, 2018 14:41*

**+Ray Kholodovsky** has a compiled version with the axis swapped, not sure if it was the X or the Y though. But I know Jim Fong could probably help out with that, he has compiled both Smoothie and GRBL-LPC in the past. But if you have the previous drivers that came with the board, you could just put one of them back to replace a blown driver, or order another one. GRBL-LPC would be your best bet to help with doing engravings. BTW, have you tried engraving at 100mm/s or slower? And if so did you still have the skipping issues?



A few steps you can try to help in the troubleshooting:



Check your belt tension....they should be taught, but not so tight that they bind. With the power off you should be able to very easily move the laser head side to side.



Make sure the rails are clear of any debris, dust, or buildup of any kind. Wipe them down with a cloth with a mild cleaner on them.



Inspect the wheels on the laser head carriage. They are little white plastic wheels, I have seen some that a part of it cracked off, and this can cause the gantry to skip steps.



Hopefully one of my suggestions will lead you to the fix, please let us know what you find. And don't worry, I am sure the collective mind of this group with your help in testing our hypotheses will get it figured out.


---
**Kiah Connor** *June 19, 2018 01:45*

Probably suggested elsewhere, but - Try a different usb cable + usb port on your computer? 


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/4z1vLjjMkUP) &mdash; content and formatting may not be reliable*
