---
layout: post
title: "So in LaserDRW, I see the option to Cutting, Engraving, and ..."
date: September 30, 2015 03:55
category: "External links&#x3a; Blog, forum, etc"
author: "Ashley M. Kirchner [Norym]"
---
So in LaserDRW, I see the option to Cutting, Engraving, and ... Marking? Can someone enlighten me as to what exactly 'Marking' does?





**"Ashley M. Kirchner [Norym]"**

---
---
**Ashley M. Kirchner [Norym]** *September 30, 2015 05:15*

Huh ... so like some sort of etching product?


---
**Ashley M. Kirchner [Norym]** *September 30, 2015 05:43*

I did. Says to spray the surface, then 'mark' it ... doesn't explain a whole lot.


---
**Kirk Yarina** *October 02, 2015 16:05*

Those are good descriptions of what the marking process is, but how does the LaserDRW "Marking" option differ from the LaserDRW engraving and cutting options?  What, if anything, does it do differently from LaserDRW's engraving raster mode, or cutting vector mode?  When would you chose it over those options, since presumably you can mark with TherMark or a substitute (moly spray on order...) with either of them?


---
**Peter** *October 12, 2015 13:44*

**+Kirk Yarina**​ from what I've seen in LaserDRW it looks like the three options are just so you can save predefined settings for when you want to pick between cut/engrave/mark. 

If you play with the settings and find one that works, save it. Although, saying that, my setting on cut and mark seem to have the same settings regardless of what I set them to. 

I don't see any other use for these three options though. 


---
**Ashley M. Kirchner [Norym]** *October 12, 2015 14:53*

Actually, the Marking option behaves different than the Cutting one. Try it and see. 


---
**Peter** *October 12, 2015 15:21*

**+Ashley M. Kirchner** Totally aimed my reply to the wrong person from my phone. I was just answering based on what i've seen so far while looking over the software. I'll try them out though and see what I find.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/Ptd6wPREDg1) &mdash; content and formatting may not be reliable*
