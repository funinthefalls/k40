---
layout: post
title: "Laser PSU voltage divider 1000:1 ratio. i.e"
date: July 28, 2016 19:32
category: "Modification"
author: "David Cook"
---
Laser PSU  voltage divider 1000:1 ratio.  i.e. 20KV should read ~20V on the meter display.



I do not recommend that anybody do this.  Very dangerous.   but if someone were to try this then make sure you have the correct components and lay all of this onto an insulated wood board. and make sure you wire everything up and are able to get a good distance away before energizing and taking the reading.



I got this divider built and when I went to use it I decided against it as I was not able to do it safely without removing the PSU from my machine.

I'm an engineer at XP Power and we deal with high voltage testing all the time.    This type of test should not be performed by the end user in reality.



and again to CMA  I will say that no body should try this.



LMAO  about the 1 Giga-Ohm resistor,   they even make Terra-Ohm resistors lol.  and who though MegaOhms were the highest ??

![images/8e4261ab03a4be6690acbb42926553fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e4261ab03a4be6690acbb42926553fd.jpeg)



**"David Cook"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 29, 2016 00:22*

Is there a way to make something like this that has a built in meter that we could install in-line into the machine & have the meter in our control panel? That might be a safer way for people to keep track of their laser PSU high voltage line. I'm not hugely knowledgeable regarding electronics, but just had a thought that something permanent to monitor the PSU voltages would be nice for diagnostic purposes.


---
**David Cook** *July 29, 2016 00:33*

**+Yuusuf Sallahuddin** it's possible yes. But would need to be well insulated and protected and really should only be done by a qualified tech.  But hey we are all DIY maker types right ;-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 29, 2016 00:34*

**+David Cook** I would get zapped for sure haha. I steer clear of any kind of voltage that is not powered by a battery.


---
**Don Kleinschnitz Jr.** *August 02, 2016 12:28*

I am thinking that there is a way to measure this HV inductively without connecting to it? Like neon indicators, coil with op amp?


---
**Jorge Robles** *March 14, 2017 07:31*

I'm for doing the same, but with the 5v signal. :D It's not the same, of course.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/Uz1rgD3utxN) &mdash; content and formatting may not be reliable*
