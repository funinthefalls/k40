---
layout: post
title: "Does anyone know of a free program that can convert photos and illustrations to 8-bit grayscale?"
date: August 02, 2016 18:01
category: "Discussion"
author: "Ben Walker"
---
Does anyone know of a free program that can convert photos and illustrations to 8-bit grayscale?  I want to play with 3D engraving.





**"Ben Walker"**

---
---
**Alex Hodge** *August 02, 2016 18:04*

[http://www.sawmillcreek.org/showthread.php?91076-Converting-programs-for-Engraving-Manual-Method-and-Adobe-Photoshop](http://www.sawmillcreek.org/showthread.php?91076-Converting-programs-for-Engraving-Manual-Method-and-Adobe-Photoshop)




---
**Alex Hodge** *August 02, 2016 18:06*

I guess the one I linked requires some version of photoshop, so it's not "free" (you can get an old version cheap though). It's widely considered one of the best methods though.


---
**Ben Walker** *August 02, 2016 18:12*

I have ps and illustrator cc edition.   Do this is great info.   Thanks!  I'm getting a x700 clone this week hopefully.   Goodbye K40


---
**Ben Walker** *August 02, 2016 18:18*

Wait.   This is just to dither a bmp.   Actually looking at something that separates the 256 levels of grey to do bona-fide engraving.    


---
**Alex Krause** *August 02, 2016 18:32*

I use gimp


---
**Jim Hatch** *August 03, 2016 02:38*

You don't want 8 bit grayscale. The laser wants 1 bit dithered. Better to resample at target resolution (I use 600dpi) and size (don't resize it after you've converted it for best results). Then adjust gamma, brightness and contrast before converting to a b&w bmp using Floyd-Steinberg or Stucki dithering (make sure you use the original's full color for the dithering process as the more colors the better the dithering calcs can be.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 03, 2016 02:50*

**+Ben Walker** Since you have PS & Illustrator CC, you could try convert to greyscale in PS, then import into Illustrator & use the Live Trace - High Fidelity Image. I'm unsure how many shades it will convert into but it should be lots of shades of grey.



edit: or alternatively in PS you could just desaturate the image.


---
**Josh Rhodes** *August 03, 2016 04:33*

Free but not open source FastStone Image viewer. Fs does all the things. Quickly too, excellent software. 


---
**Ben Walker** *August 03, 2016 09:58*

Thanks all I found vectric aspire and think I am going to use that instead of paying a service to convert the images.  I want to get the same effect as the Kern lasers are famous for.  Only on a huge budget.  lol


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/SEAutfCDgNB) &mdash; content and formatting may not be reliable*
