---
layout: post
title: "A remix of my last creation. Made for a friend of my mine that is big into Harley motorcycles"
date: January 30, 2017 02:48
category: "Object produced with laser"
author: "Ned Hill"
---
A remix of my last creation.  Made for a friend of my mine that is big into Harley motorcycles.

![images/8a1b02352cbe18f16591a96ec92a6518.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8a1b02352cbe18f16591a96ec92a6518.png)



**"Ned Hill"**

---
---
**Don Kleinschnitz Jr.** *January 30, 2017 03:01*

 Very Sweet!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 30, 2017 04:45*

These pieces look great. Keep up the good work inspiring us with your creations. Out of curiosity, what colour stain are you using for the main parts again? It's a nice colour brown in my opinion.


---
**Ned Hill** *January 30, 2017 17:24*

Thanks **+Yuusuf Sallahuddin**. For stain I'm using Minwax Red Oak 215


---
**Adam J** *January 31, 2017 16:06*

That sir, looks wonderful! Well done :-)


---
**Brien Watson** *February 01, 2017 02:12*

That's an amazing job!  I would love to have that as I am a lifetime member of HOG.  Was that all from a picture?


---
**Ned Hill** *February 01, 2017 02:29*

Thanks **+Brien Watson**.  It's actually 2 layers. For the background I just lined up alternating bar & shield logos blocks and engraved/cut.  The Frame and HOG logo were engraved and cut from the same piece of prestained wood.  The wood was masked before engraving and I then painted the engraved areas of the logo with black paint to add contrast.


---
**Jeff Johnson** *February 01, 2017 21:31*

This is really sweet looking!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/bkZtgJ5DWV6) &mdash; content and formatting may not be reliable*
