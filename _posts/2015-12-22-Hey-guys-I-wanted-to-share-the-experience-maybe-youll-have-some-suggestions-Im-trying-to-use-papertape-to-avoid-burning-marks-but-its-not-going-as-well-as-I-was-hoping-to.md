---
layout: post
title: "Hey guys! I wanted to share the experience, maybe you'll have some suggestions :) I'm trying to use papertape to avoid burning marks but it's not going as well as I was hoping to"
date: December 22, 2015 18:19
category: "Discussion"
author: "Tadas Mikalauskas"
---
Hey guys! I wanted to share the experience, maybe you'll have some suggestions :) I'm trying to use papertape to avoid burning marks but it's not going as well as I was hoping to. My laser engraves very nicelly without the paper tape like in the picture with a tree which I like very much but I got the cut marks on the left side even though I had a tape on the spot where the holes were made. On the one with a lion I had a completelly coveder piece but when I was engraving it seemed that the tape was highly reducing the power to get to the wood intself and even after second run the tape left the glue on the picture and it is sticky. And of course that is not cool. I tried couple of types of papertape and it seems to react the same. Do you use something for protection? :) cheers guys!



![images/f416e6004630e74b304b38a23f5445e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f416e6004630e74b304b38a23f5445e0.jpeg)
![images/ffbaa7938fb31046d93f4e0ff4d939a3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ffbaa7938fb31046d93f4e0ff4d939a3.jpeg)

**"Tadas Mikalauskas"**

---
---
**ChiRag Chaudhari** *December 22, 2015 18:31*

Nice work man, looking great. I am not sure if you are supposed to use masking tape where you want to engrave but for cuts it works great.



Now that gives me idea that the part you want to engrave cover it completely with masking tape or just the edges. Then cut the edges with low power, remove the masking tape where you want to engrave and then run you engraving file. That will give you pretty sharp edges without smoke marks. 



Also are using air assist at all? That will make the job super clean.


---
**Tadas Mikalauskas** *December 22, 2015 20:27*

thanks :) yeah I have tried that but if I'll make something small like 4x2cm with something engraved inside, it won't help if i peel the tape off, it because the smoke will reach the cut out area, so at having it just engrave through the tape would be a lot prettier :)


---
**ChiRag Chaudhari** *December 22, 2015 20:33*

Gooood Air Assist , that is what you need.


---
**Tadas Mikalauskas** *December 22, 2015 20:43*

Oh I forgot to mention, I do have an air assist, it helps a lot, but I could completely eliminate the smoke marks if I find a tape that is very thin and doesn't leave the glue on the engraved are. I know people use this method just that I can not find the right material. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 22, 2015 20:51*

There is a specific painter's masking tape that they use that has a very low tackiness to prevent it sticking to the paint & peeling then paint off when removed from the wall. I purchased some from Bunnings Warehouse (local hardware store) in the paint section & it seems to be fairly decent for masking areas. Although, you can get an even wider version of the same tape (current one I have is about 20-25mm wide) that is about 50-75mm wide (can't remember exactly). i would suggest some of this painter's masking tape (it's cheap) for some tests & I would also try use the method that Chirag suggested (where you lightly cut the engraving edge & remove the tape in the centre area where you would like to engrave).



Another thought is to use some kind of paper, as I've seen painters do also. So place the paper over your material & use tape to stick the paper to the cutting bed maybe. This might be a method to avoid getting sticky glue marks on your wood.



By the way, your engraving jobs look pretty great :)


---
**Richard Vowles** *December 24, 2015 18:03*

You could always use a light layer of the spray on glue that is used for sticky notes. You can buy cans in most hardware stores.


---
*Imported from [Google+](https://plus.google.com/113213933687083540785/posts/ebhhzn8kmez) &mdash; content and formatting may not be reliable*
