---
layout: post
title: "So I have been looking for a place to buy acrylic from I found a place on eBay that will allow me to do 20 8x12 sheets shipped for about 65-70 dollars depending on colors is that a good deal?"
date: January 29, 2016 01:10
category: "Discussion"
author: "3D Laser"
---
So I have been looking for a place to buy acrylic from I found a place on eBay that will allow me to do 20 8x12 sheets shipped for about 65-70 dollars depending on colors is that a good deal?





**"3D Laser"**

---
---
**Sean Cherven** *January 29, 2016 01:35*

This is where I get mine from: 

[http://www.amazon.com/gp/product/B009AEAFIE](http://www.amazon.com/gp/product/B009AEAFIE)


---
**Stephane Buisson** *January 29, 2016 01:36*

**+Corey Budwine** this is a very personal question, depending on how you can afford.

I just would like to underline this kind of post have limited interest for the rest of the world, and look like a bit spammy.

We are now just under 1000 members now, please post  subject of interest for all. Imagine you are on stage in front of 1000 persons, would you ask this question ? is it the best place ? please understand the difference between post and comments. we try to limit post to subject of interest (and categorized them), comments are not limited.


---
**3D Laser** *January 29, 2016 01:58*

**+Stephane Buisson** understood


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 29, 2016 03:06*

**+Corey Budwine** This sort of post would actually be of interest as a comment in a post for the "Material Suppliers" category. I would create a post specific for your country & comment it there (or if there already is one I would comment it on the existing post). It is always good to find suppliers for different materials.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/cVZ24doKjWW) &mdash; content and formatting may not be reliable*
