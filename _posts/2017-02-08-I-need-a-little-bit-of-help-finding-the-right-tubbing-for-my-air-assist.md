---
layout: post
title: "I need a little bit of help finding the right tubbing for my air assist"
date: February 08, 2017 01:11
category: "Discussion"
author: "3D Laser"
---
I need a little bit of help finding the right tubbing for my air assist. I just bought an aquarium o2 pump to replace my old air brush compressor.  I need a 5mm inner diameter tube that is coiled but isn't to stiff the one that purchased is to stiff and interferes with the laser moment any suggestions?





**"3D Laser"**

---
---
**Dan Stuettgen** *February 08, 2017 01:20*

Go to light object web site.  They have the tubing for the air asist units.  Its the same size as the tubing for the water cooling system.


---
**3D Laser** *February 08, 2017 02:32*

**+Dan Stuettgen**  are they coiled 


---
**Dan Stuettgen** *February 08, 2017 02:35*

No. Its not coiled.  It is very flexible and soft.  


---
**Phillip Conroy** *February 08, 2017 04:41*

I brought a airbrush coiled hose on ebay for under $6  ,let me know if you want photosof where iIran my hose 

[ebay.com.au - Details about  3M Air Hose Flexible Retractable Rubber Air Brush Compressor 1/8" to 1/8" End](http://www.ebay.com.au/itm/3M-Air-Hose-Flexible-Retractable-Rubber-Air-Brush-Compressor-1-8-to-1-8-End-/130463947121?hash=item1e6041d971:g:Bg0AAOSw241YmWvm)


---
**3D Laser** *February 08, 2017 14:28*

**+Phillip Conroy** that is the one I have but I need a 5 mm  not three


---
**3D Laser** *February 09, 2017 00:27*

**+Dan Stuettgen** does it get in the way of operating the machine 


---
**Dan Stuettgen** *February 09, 2017 00:32*

No because i installed the cabling chain .  One end is attached to the right hand wall of the cutting area and the other end to the bracket that the air assist lens / mirror assembly is mounted in.  The air assist hose runs through thr cabling chain, so it stays out of the way.


---
**3D Laser** *February 09, 2017 00:36*

**+Dan Stuettgen** could you show me a picture when I tried a cable chance I couldn't get it to work


---
**Dan Stuettgen** *February 09, 2017 00:39*

Sorry. Not at home where my machine it.  Away on business trip.  Will post pic on monday when i am back home.


---
**Pablo Verity** *February 12, 2017 15:01*

Check out the aeromodellers... they use silicon coiled for refuelling their models. Fleabay ought to be a good source.


---
**Scorch Works** *February 26, 2017 19:30*

**+Dan Stuettgen** Did you ever share a picture of your air hose cabling chain.  I am also interested in seeing it. Thanks.


---
**Pablo Verity** *March 03, 2017 11:06*

This is the aeromodellers silicon tubing in situ, works well.

![images/23c1cb2057019265b9f9b92ec65ac6cc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/23c1cb2057019265b9f9b92ec65ac6cc.jpeg)


---
**3D Laser** *March 03, 2017 11:41*

**+Pablo Verity** can you tell me where you purchased this


---
**Pablo Verity** *March 05, 2017 03:14*

Fleabay. It was a couple of decades ago, but a quick search yielded this... You need to search for stuff.

[nexusmodels.co.uk - Recoil Vinyl Fuel Tube 5ft l-ip003-5508548   - Nexus Modelling Supplies](http://www.nexusmodels.co.uk/recoil-vinyl-fuel-tube-5ft-l-ip003-5508548.html)


---
**Pablo Verity** *March 31, 2017 08:00*

You're welcome.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/Fd9mYs5DapL) &mdash; content and formatting may not be reliable*
