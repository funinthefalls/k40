---
layout: post
title: "Does anyone know whether the power and speed settings of the engraving and cutting mode (LaserDRW) interpreted differently by M2 Nano?"
date: November 15, 2018 23:18
category: "Original software and hardware issues"
author: "Winston CHEN"
---
Does anyone know whether the power and speed settings of the engraving and cutting mode (LaserDRW) interpreted differently by M2 Nano?  



Few things that I observe:



1. the same speed setting for engraving and cutting mode in LaserDRW gives different actual speed. 



2. the same power setting for engraving and cutting mode on the power panel gives different actual power. 



Therefore, I am wondering if someone know how the engraving and cutting mode in laserDRW interprets the number. 



Note: We cut thin plastic film and are looking for a clean cut without any melting on the edge. We tested the setting of power, and speed, engraving/cutting mode. We found the engraving with specific setting of power and speed can give a very clean cut. However, when we use the same setting in the cutting mode, we don't see clean cut anymore. It looks like the power is too strong. We tried lowering the power in the cutting mode; however, we just couldn't get the same results. The engraving mode works; however, it just take too much time to just cut one part because the laser in the engraving mode doesn't walk in the way as in the cutting mode. Can someone help me?





**"Winston CHEN"**

---
---
**HalfNormal** *November 15, 2018 23:50*

The simple answer is that vector (cutting) is made up of lines and raster (engraving) is made up of pixels.

Power at the laser focal point is a mixture of laser power setting and speed of the laser travel.

The following link is one of many to explain the difference. This was with a very quick search and little weeding of the results.



[troteclaser.com - Trotec Laser Inc.](https://www.troteclaser.com/en-us/knowledge/tips-for-laser-users/raster-engraving-vector-engraving/)


---
*Imported from [Google+](https://plus.google.com/116727182357674077685/posts/NLcgSoW4kPn) &mdash; content and formatting may not be reliable*
