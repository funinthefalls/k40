---
layout: post
title: "About 3 months ago I picked up a K40 on eBay"
date: December 12, 2016 12:20
category: "Original software and hardware issues"
author: "J.R. Sharp"
---
About 3 months ago I picked up a K40 on eBay. After a rough alignment, it did very well.



However, I had to remove the gantry to take out the duct. The gantry itself is pretty much solid, there is no play in the mounting holes so it should just be drop back in and go. Nope.



Everything was out of alignment. Last night, I finally realigned the main mirror which hit a little high, so I shimmed it some and it was hitting dead-nuts center. Moving down to the Y axis mirror, it was also way off and required me to shim it about 1mm. Also I had to cut some off the bracket to allow me to slide it inboard about 3mm. Now I am to the head and once again I got my points converging, but they are high. After a 3mm shim, I'm finding them barely hitting the hole.



Any one in the Dayton, OH area that can come and help me get this bastard aligned?







**"J.R. Sharp"**

---
---
**J.R. Sharp** *December 12, 2016 23:57*

Anyone?


---
**Joe Alexander** *December 13, 2016 02:35*

I would check to see if your laser tube is perfectly parallel to your gantry otherwise you can end with a skewed beam at the end from what I have noticed.


---
*Imported from [Google+](https://plus.google.com/116586822526943304995/posts/4JQXymRu66T) &mdash; content and formatting may not be reliable*
