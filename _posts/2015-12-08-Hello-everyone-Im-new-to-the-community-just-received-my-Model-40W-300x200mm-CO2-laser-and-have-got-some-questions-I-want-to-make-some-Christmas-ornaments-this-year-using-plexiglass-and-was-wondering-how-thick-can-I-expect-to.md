---
layout: post
title: "Hello everyone I'm new to the community just received my Model 40W 300x200mm CO2 laser and have got some questions I want to make some Christmas ornaments this year using plexiglass and was wondering how thick can I expect to"
date: December 08, 2015 15:18
category: "Materials and settings"
author: "Timothy \u201cMike\u201d McGuire"
---
Hello everyone I'm new to the community just received my Model 40W 300x200mm CO2 laser and have got some questions I want to make some Christmas ornaments this year using plexiglass and was wondering how thick can I expect to cut what settings for power and  speed should I use I could figure this out on my own but not before Christmas so any help would be appreciated 





**"Timothy \u201cMike\u201d McGuire"**

---
---
**Gary McKinnon** *December 08, 2015 16:08*

I've read of people cutting 4mm at 10mm/s on 60% power, i've not done enough cutting yet to give advice.



Some Google results :



[https://www.google.co.uk/search?site=&source=hp&q=k40+laser+cutter+acrylic+speed+power&oq=k40+laser+cutter+acrylic+speed+power&gs_l=hp.3...2762889.2768935.0.2769085.38.31.0.6.6.0.166.2290.28j3.31.0....0...1c.1.64.hp..14.24.1254.0.wBwVMdAk-Mg](https://www.google.co.uk/search?site=&source=hp&q=k40+laser+cutter+acrylic+speed+power&oq=k40+laser+cutter+acrylic+speed+power&gs_l=hp.3...2762889.2768935.0.2769085.38.31.0.6.6.0.166.2290.28j3.31.0....0...1c.1.64.hp..14.24.1254.0.wBwVMdAk-Mg)


---
**Anthony Bolgar** *December 09, 2015 12:17*

Make yourself a test file and cut it in scrap using different settings until you get acceptable results. A good starting point is to start at 25% power at 10mm/s and increase it by 5% power up to a maximum of 60% power. If this does not cut through the material for you, then slow the speed down by 1/mm/s until you get a through cut. The lower the power setting you can get by with, the better, it will increase the life of your tube. I personally do not exceed 70% power ever, if need be I will run multiple passes to achieve the cut.


---
*Imported from [Google+](https://plus.google.com/109629943464502534866/posts/GGCdgAaLTP3) &mdash; content and formatting may not be reliable*
