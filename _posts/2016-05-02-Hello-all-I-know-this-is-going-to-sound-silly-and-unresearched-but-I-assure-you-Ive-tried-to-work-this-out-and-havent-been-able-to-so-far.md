---
layout: post
title: "Hello all, I know this is going to sound silly and unresearched but I assure you, I've tried to work this out and haven't been able to so far"
date: May 02, 2016 05:41
category: "Discussion"
author: "Pippins McGee"
---
Hello all, 

I know this is going to sound silly and unresearched but I assure you, I've tried to work this out and haven't been able to so far.



What does the term 'k40' actually refer to?

I am looking at the 300-700$ blue/white, chinese, 40watt machines - however there is variance between them, some look slightly different to the others, different control panels, etc.



So what is k40? I don't see the term k40 used in any of the ebay listigs, descriptions, nothing.



I'm looking to find the most reputable version in that price range, everyone says 'k40' but of all the choices.. what is k40?



Cheers

any clarification on this is appreciated!





**"Pippins McGee"**

---
---
**Jean-Baptiste Passant** *May 02, 2016 06:14*

K40 is a generic name for the Blue-Box 40W Laser, it's sometimes used in listing, because there is actually a K40 (and a K40-II and a K40-III).

Even though there is a lot of different K40, they work the same, some have generic turnpot, some have digital etc... But the laser is almost always the same (cheap 29W overdriven to 40W).



There is not really a reputable version as you cannot be sure of what you will get unless you contact the seller prior to buying it.



But as I said, they are almost all the same, and the cheapest is probably your best bet.



I see people getting K40 for 370$ in their country, I recommend adding a smoothieboard, a middleman board and a raspberry pi, with all that you would still be in your initial budget of 300-700$.



Smoothieboard is the best upgrade you can get, the middleman helps a lot with wiring and the Raspberry pi make the whole setup wireless.


---
**Scott Marshall** *May 02, 2016 08:12*

Carroll Shelby, when asked what the 500 in GT-500 Shelby Mustang stood for, stated that they couldn't come up with a name, so he sent a salesman at his dealership to pace off the front lot. It was 500 paces across.



So the story goes.....





Maybe somewhere in China there is a laser shop with a small parking lot....





The K40 usually puts out about 30 watts of laser light. (based on member testing)


---
**Jim Hatch** *May 02, 2016 11:42*

With all due respect to Jean-Baptiste, you can be quite successful without upgrading the electronics. However,  I'd suggest there is one "must do" upgrade - an air assist head. This allows you to keep the lens clean by blowing the smoke away from the cut. A dirty lens drastically reduces delivered power and over time the deposits can cook on and damage the lens.



An air assist can sometimes be found on a stock machine - look for it on the eBay listing. Otherwise it will cost you from $20-50 for a new head & air pump. If you upgrade the lens at the same time to a bigger & higher quality one that will also cost another $30-60.



Every other normally made modification is for ease of use (safety switches are sometimes added but those aren't needed if you don't run the laser with the lid up).


---
**Jean-Baptiste Passant** *May 02, 2016 12:42*

**+Jim Hatch** Yes you are right, K40 models are really different from one seller to another, some already have an air assist head.

So I only talked about things I am sure you can upgrade on the laser, as some laser already have air assist, safety switches (door switch on some drawings) and I also saw some bundled with a flow sensor.



I may be in some kind of Smoothieboard cult since I upgraded my laser to it, I even ordered one for my 3D printer actually... So as it was, for me, the best upgrade I did I always recommend it first.



It's also the most expensive (cheaper than a DSP :) ), but it add a lot of things, software control of the laser means I can get the laser to fire a line from 0ma to 16ma and see which power I like the best. Also flow sensor, min AND max endstop, faster motor, screen etc...

Smoothieboard is not one upgrade it's actually a bundle of upgrades.



In my opinion, here is a list of upgrades anyone should do on the K40 machines :

- Smoothieboard

- Air Exhaust

- Flow sensor

- Air Head Assist

- Min & Max endstops

- Less noisy water pump



Once the whole setup is clean and results are goods, I would then upgrade the lens and mirror because why would I do my tests and measurement on my clean, expensive lens and mirror when I can do them on the cheap working ones ?


---
**Jim Hatch** *May 02, 2016 13:38*

**+Jean-Baptiste Passant** I get where you're coming from but that's because I'm a techie :-) I've got a Smoothie ready to go but waiting on a couple of connectors I need in order to build my middleman board. 



I've done a bunch of the upgrades you listed but in a different order.

- Squared the rails since mine came out of the box with a 1/2" out of square in the lower right corner which really screws up the consistency of the alignment and power delivery

- Air exhaust (removed the existing exhaust manifold, added intake fans to the front of the machine so it breathes better)

- replaced the stock bed with my own so I have a larger work surface (with the exhaust manifold out of the way, there's an extra inch or so of material space)

- safety switches (didn't really need them, no one else uses the machine but me but I had a couple and they're cheap anyway so it was a quick install)

- air assist head (LightObject head) to clean up the smoke path and keep my lens clean

- new upgraded lens since the LO head is larger (I could have cut a spacer ring and reused the stock lens but it wasn't a ton of money and the LO lenses are much better - helps make up for the lower real power vs the advertised 40w)

- new mirror for the new air assist head since I was upgrading the lens



Next up:

- waterflow sensor (<$10) 

- LED laser pointer head to help with aiming the laser (it's been sitting on the bench but hasn't been a high enough priority for me to get to)

- Smoothie & LaserWeb upgrade

- Powered Z-Axis table (LightObject again) although I'll lose the extra inch I got from stripping the manifold out but it's more convenient when doing non-standard sizes of materials - right now I primarily use 1/4 & 1/8" materials because I'm cutting and engraving but I think I want to start doing engrave-only work on larger (deeper/thicker) materials


---
**Jean-Baptiste Passant** *May 02, 2016 14:07*

**+Jim Hatch** Looks like we are doing the same upgrades in another order actually. 


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/AKMx6tzUcJ8) &mdash; content and formatting may not be reliable*
