---
layout: post
title: "Is there a way to save or favorite a post in order to come back to it easily in the future?"
date: February 14, 2017 03:38
category: "Discussion"
author: "timb12957"
---
Is there a way to save or favorite a post in order to come back to it easily in the future?





**"timb12957"**

---
---
**Ariel Yahni (UniKpty)** *February 14, 2017 03:41*

Create a private collection and share it to that collection


---
**timb12957** *February 14, 2017 03:43*

Very good, thanks!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 14, 2017 04:09*

That's a good idea **+Ariel Yahni**. I just tend to comment in the post to keep track of it as others reply to it.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/34Zt329LeiU) &mdash; content and formatting may not be reliable*
