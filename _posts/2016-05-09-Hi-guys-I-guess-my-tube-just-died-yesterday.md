---
layout: post
title: "Hi guys, I guess my tube just died yesterday"
date: May 09, 2016 10:48
category: "Discussion"
author: "Gunnar Stefansson"
---
Hi guys, 

I guess my tube just died yesterday. I could output 100% where my ampmeter showed 20mA of current, but it couldn't even cut through my usual 3mm mdf wood even at extremely slow speeds. I cleaned all mirrors and lense but nothing changed... I've only had it for 1 Year with maybe 100hours of operating time. I've ordered a new one already and am hoping that it's the tube's failure. I'll see at that time... But what u guys think? 



I normally cut so I never exceed 18mA. And I do believe that I've had an increase in efficiency at 18mA so I needed to increase all my speeds. I think this is one of the symptoms of a Tube going bad. The beam inside looks true and strong, and as I've read, there's something about an Amplification & cancellation happening with the Resonator within the tube? cause some people have full beam inside and Nothing coming out of the tube!



Any tests I should or could do? and anyone else agree on that it sounds like the Tube is kaputt?



Thanks in advance. 

 





**"Gunnar Stefansson"**

---
---
**Scott Thorne** *May 10, 2016 01:33*

If it's showing amperage them it's a good chance that the problem is alignment. 


---
**Gunnar Stefansson** *May 10, 2016 10:18*

**+Scott Thorne** yes it could be, highly unlikely as it can engrave and partially cut so the focus seems to be spot on as it was before... But I'll have to check that to make sure :)


---
*Imported from [Google+](https://plus.google.com/100294204120049700616/posts/SsTQrxmCem5) &mdash; content and formatting may not be reliable*
