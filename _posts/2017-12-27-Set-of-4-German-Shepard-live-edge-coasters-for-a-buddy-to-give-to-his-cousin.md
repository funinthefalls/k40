---
layout: post
title: "Set of 4 German Shepard live edge coasters for a buddy to give to his cousin"
date: December 27, 2017 13:44
category: "Object produced with laser"
author: "David Allen Frantz"
---
Set of 4 German Shepard live edge coasters for a buddy to give to his cousin. Just needs a bit of finish and they will be good to go. 

![images/f21da3d0894b165df1dba3ff43c0ab51.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f21da3d0894b165df1dba3ff43c0ab51.jpeg)



**"David Allen Frantz"**

---


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/32RpnvXXrgi) &mdash; content and formatting may not be reliable*
