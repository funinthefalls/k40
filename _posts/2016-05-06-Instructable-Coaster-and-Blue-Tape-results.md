---
layout: post
title: "Instructable : Coaster, and Blue Tape results"
date: May 06, 2016 06:27
category: "Object produced with laser"
author: "Jean-Baptiste Passant"
---
Instructable : Coaster, and Blue Tape results





**"Jean-Baptiste Passant"**

---
---
**Alex Krause** *May 06, 2016 06:29*

My post is at the same time you posted this is about a similar project but I want to engrave cork 


---
**Mishko Mishko** *May 06, 2016 08:00*

Funny, I found this site yesterday [http://boxmaker.connectionlab.org/](http://boxmaker.connectionlab.org/)

and now there's an almost identical site for making boxes. What are the odds of this happening;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 08:30*

Again funny, because I was just doing a test on some 19mm thick wood for coasters. Seems something strange is going on in the lasering universe. I think the lasers are starting to control our minds.


---
**HalfNormal** *May 06, 2016 12:59*

It's called THE BAADER-MEINHOF PHENOMENON

You may have heard about Baader-Meinhof Phenomenon before. In fact, you probably learned about it for the first time very recently. If not, then you just might hear about it again very soon. Baader-Meinhof is the phenomenon where one happens upon some obscure piece of information—often an unfamiliar word or name—and soon afterwards encounters the same subject again, often repeatedly. Anytime the phrase “That’s so weird, I just heard about that the other day” would be appropriate, the utterer is hip-deep in Baader-Meinhof.



[http://www.damninteresting.com/the-baader-meinhof-phenomenon/](http://www.damninteresting.com/the-baader-meinhof-phenomenon/)


---
**Mishko Mishko** *May 07, 2016 08:00*

I normally don't pay much attention, if any, to coincidences, as, bottom line, they're just that and nothing more. It just seemed to me as statistically next to impossible, as I litterally stumbled upon both programs, I'm not even particularly interested in box making, and certainly not this type of box. And the programs to me are almost identical.visually and functionally (but maybe I'm doctoring specs to cover for my syndrome, so please check them yourself and see if I'm wrong;) And in a tight time frame of about 15 hours, not bad at all, huh?



Well, interesting or not, I was just waiting to see if maybe yet the third program would appear this morning, seriously limiting my ability to close the case, and mercifully it did not, so I can finally find something equally unimportant to do. After all, it's Saturday, all about switching between unimportant tasks, at least for me ;)


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/SvinAUVb4Xq) &mdash; content and formatting may not be reliable*
