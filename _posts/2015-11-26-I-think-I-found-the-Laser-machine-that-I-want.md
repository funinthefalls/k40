---
layout: post
title: "I think I found the Laser machine that I want"
date: November 26, 2015 00:08
category: "Discussion"
author: "Gilles Letourneau"
---
I think I found the Laser machine that I want.  There is a lot of model and capacity but it's only for hobby.  At the same time I didn't want something too small and reach the limit too fast.  Also buying in China take very long time for delivery in Canada or it cost 1000$ for delivery by air.  So I found one that can be ship by ground and get here in 10 days but now before I make the final move I still have a question.



The model is a 50W with a 400 X 400 mm electric level honeycomb bed.  Air assiste and water pump included.  It is not a DSP controler but it is direct CorelDraw, WinSealXP and LaserDRW. (don't support MoshiDraw)  



The price is 2000 US ship to my door.



Question:



With a 50W what is the max size of plexiglass that I can cut ?



How is the price with the description I gave ?





**"Gilles Letourneau"**

---
---
**Coherent** *November 26, 2015 09:19*

$2000 for a unit without a DSP controller is kinda high. For that price range in a 50w Chinese unit, you should be getting  a DSP controller. A motorized bed is a nice feature but a unit without one with DSP in the wattage & working size you want would be a  much better option in my opinion.


---
**Stephane Buisson** *November 26, 2015 10:38*

Look on [ebay.com](http://ebay.com) (USA), not Ebay.ca, look their shop, contact seller, and grab a black friday deal.



it's the same sellers (differents shops) on both side of the atlantic.



PS this one is in canada : [http://www.ebay.com/itm/50w-co2-Laser-Engraver-Engraving-Machine-Cutting-Cutter-110V-USB-/321905756061?hash=item4af314079d:g:JSYAAOSw-7RVH1hb](http://www.ebay.com/itm/50w-co2-Laser-Engraver-Engraving-Machine-Cutting-Cutter-110V-USB-/321905756061?hash=item4af314079d:g:JSYAAOSw-7RVH1hb)



I haven't extensively search for you, but it's easy to do a lot better than 2k$.


---
**Gilles Letourneau** *November 26, 2015 13:45*

**+Stephane Buisson** Yes I see    But the model I want is only  180$ more for electric bed level and the bed is 40 x 40 instead of 30 x 50.  also in the description on the one I want they say water pump and air compressor is include.  if you add the shipping cost you have the 1950 US that I spoke about.


---
**Gilles Letourneau** *November 26, 2015 13:47*

I'm still wandering the maximum size I can cut with 50W and if 60W is really better.   One of the main reason I want the laser is for cutting plexiglass wood and acrylic.


---
**Coherent** *November 26, 2015 16:06*

Do some searching on the web and youtube and you'll see what materials, thickness, settings etc folks are getting with the 50-60w machines. The majority of the members here have a 40w K40/D40 type machines. Also check the machine sellers web sites that sell the China imports in the US. They normally list materials and thickness capabilities.


---
**Stephane Buisson** *November 26, 2015 17:10*

**+Gilles Letourneau** it could be more efficient to say what thickness of wood-perspex you want to reach

(K40 could do easy 3mm in one go and 5 mm is doable)

as per the XY table size, you are limited by the box size in one go.

as you have maybe see in my youtube I am able to do A3 by rotating the material in the front slot I did in the box.not exactly the same as a larger table, but I am also in hobby category (if exceptionally need bigger I would outsource those specific job)

My total cost is sub 600usd (read K40 Modifications & improvements)


---
**Gilles Letourneau** *November 26, 2015 18:47*

Hi, Marc G, on Youtub I see more exemple about engraving and when it is about cutting, it 's 3 and 4 mm  I did saw 5 mm but not too much explanation.   Yes some manufacturer give size and W but I prefer to ear it from users. I'm a big internet buyer and something I learn...  manufacturer or seller don't always give the right picture :-) 


---
**Gilles Letourneau** *November 26, 2015 18:54*

Hi Stephane,



Normally I will cut around 4 mm but I want to know if it's possible to cut 6 or 7 mm (acrylic and plexiglass)  I have a 3D printer (Makerbot) and sometime a final adjustment could be usefull for me .  I could use the laser to cut plexiglass and create a part that will complete an assembly made by my 3D printer.   all this to say that cutting is the main goal and engraving will be only an extra for me.



question



If we do a double pass with acrylic and Plexiglass, does it make the cut less nice ?   or it's possible to pass one than one time and the result can be nice ?

 


---
**Stephane Buisson** *November 26, 2015 20:09*

To be honest, I never try 6 or 7 mm, but that's a lot. if you understand well the focal lens to be in the middle of material, that mean you will be already on top/bottom at f+/-3.5mm (proportional to the lens 7/50.8 is quiet a lot) =>your side cuts would be rough. Power isn't all.

for some of my design, I just glue to pieces (wood) on top of each other.


---
**Gilles Letourneau** *November 26, 2015 20:20*

Yes that's what I was thinking but since I never had a laser before and I saw a manufacturer table somewhere saying 10 mm at 60W I didn't know what to think.   Was it 10 mm with a single passe at 60W..  Is it possible to do that with two pass with a 50W etc etc.    That's why I'm asking those questions on the forum..  You guys having experience with this so it' s a very good place to learn.   I'm almost ready to order and don't worry I will have plenty of tests to do and things to says after :-)      



question



How long can we use a mirror before it need to be change and the same with the lens? 



If I keep them always clean off course



Should I order some spare parts with the machine ?

  


---
**Stephane Buisson** *November 26, 2015 20:27*

I just take it as it come, I got a broken lens, a shatered mirror, it take a couple of weeks from china, at reasonable price.

you could order a full set  for around 50 usd. why not a bit in advance ? (personal choice)


---
**Gilles Letourneau** *November 26, 2015 22:34*

Who is selling a full set at 50 USD ?  I saw 80 fro 3 mirrors and 30 for a lens.  Do you have a suggestion ?


---
**Stephane Buisson** *November 27, 2015 11:39*

[http://www.ebay.co.uk/itm/3X-K9-Mirror-20mm-25mm-1X-Znse-Lens-18mm-19mm-20mm-CO2-Laser-Engraver-40W-50W-/321809423147?var=&hash=item4aed561b2b:m:mEiLKHAIPIFNzH4S3QvkHPA](http://www.ebay.co.uk/itm/3X-K9-Mirror-20mm-25mm-1X-Znse-Lens-18mm-19mm-20mm-CO2-Laser-Engraver-40W-50W-/321809423147?var=&hash=item4aed561b2b:m:mEiLKHAIPIFNzH4S3QvkHPA)

those are not the best,



add 20% for quality one Mo & HQ znse

[http://www.ebay.co.uk/itm/3-Si-K9-Mo-25mm-20mm-Reflective-Mirror-1-HQ-Znse-GaAs-Focus-Lens-CO2-Laser-/261815216425?var=&hash=item3cf5672929:m:mlYcI_mnOg_V4NhhePFs5Gw](http://www.ebay.co.uk/itm/3-Si-K9-Mo-25mm-20mm-Reflective-Mirror-1-HQ-Znse-GaAs-Focus-Lens-CO2-Laser-/261815216425?var=&hash=item3cf5672929:m:mlYcI_mnOg_V4NhhePFs5Gw)


---
**Gilles Letourneau** *November 27, 2015 19:26*

OK Stephane I just add it to my watch list  thanks


---
**Gilles Letourneau** *November 27, 2015 19:32*

Stephane, If 6 or 7 mm is very thick bacause of focal reason...  10 mm will be very thick so I'm dreaming and I might pay for power that I will never be able to use.   50 or 60 W, is it just faster to cut.  



How thick to you make with your 40W ?  



Because for a hobby, speed is not really an issue so do I really need 50w or 60watt ?     


---
*Imported from [Google+](https://plus.google.com/101789423601866268108/posts/DpG6JEHpAjH) &mdash; content and formatting may not be reliable*
