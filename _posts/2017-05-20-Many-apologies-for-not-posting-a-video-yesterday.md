---
layout: post
title: "Many apologies for not posting a video yesterday"
date: May 20, 2017 06:40
category: "Materials and settings"
author: "Alex Krause"
---
Many apologies for not posting a video yesterday. For alignment of irregular shaped parts for laser engraving I had a bum rush day of finishing several graduation presents... When working with irregular shaped items that require alignment it's best to have a waste substrate such as card stock to work with.... I use card stock because you can mark it with low power without cutting trough. A set of calipers or a steel ruler/scale will be needed to complete this project. I take my measurements of the part and get atleast 3 points of alignment to register to but 4/5 are better. Using those measurements I draw up a rough draft of the part and engrave it in card stock so I can lay the part on it 



![images/97a123d8a59984fb15c1a1105d077fa6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97a123d8a59984fb15c1a1105d077fa6.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-Hvk1Fdb7CYo/WR_k5vS4cHI/AAAAAAAAQeU/E9MS-8efaxYu_CVVJ8PjWnZc_JrXdz4twCJoC/s0/VID_20170519_242234646.mp4**
![images/f176ae2799c2bef48b51e927c5b725b6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f176ae2799c2bef48b51e927c5b725b6.jpeg)

**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 20, 2017 10:56*

Are you running with a thick piece of ply below the cardstock as your cutting bed? Or just to raise the engrave height up a bit?


---
**Todd Fleming** *May 20, 2017 16:39*

Reminds me of softjaws on CNC mills!


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/2Noeyy1gMYz) &mdash; content and formatting may not be reliable*
