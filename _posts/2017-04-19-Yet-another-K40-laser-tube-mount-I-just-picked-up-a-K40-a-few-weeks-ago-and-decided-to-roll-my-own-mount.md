---
layout: post
title: "Yet another K40 laser tube mount. I just picked up a K40 a few weeks ago and decided to roll my own mount"
date: April 19, 2017 21:53
category: "Modification"
author: "Steven Kalmar"
---
Yet another K40 laser tube mount.  I just picked up a K40 a few weeks ago and decided to roll my own mount.  I'm always open for suggestions on improving it.





**"Steven Kalmar"**

---
---
**Steve Clark** *April 20, 2017 00:44*

Very nice...I would like to have an extruder like that but its not in my toy budget right now. However, I do have a 3 axis full sized cnc mill I can make them on when I get time. Thanks for the Fusion file!!


---
**Steven Kalmar** *April 20, 2017 01:14*

**+Steve Clark** this should work well on a CNC.   One could even slice it and cut multiple layers of 1/8" acrylic on the K40 and weld them together.  It's a simple design.  


---
**HalfNormal** *April 20, 2017 15:29*

**+Steven Kalmar** Very nice! I know what I will be printing up this weekend!


---
**HalfNormal** *April 20, 2017 20:22*

Well I have already started printing. 


---
**Steve Clark** *April 20, 2017 20:31*

HalfNormal, Print two and send me one - Grin -


---
**Steven Kalmar** *April 20, 2017 22:19*

**+HalfNormal** let me know how it goes.  What are you printing it in?  I used PETG.




---
**HalfNormal** *April 20, 2017 22:23*

**+Steven Kalmar** using PLA that I have.


---
**HalfNormal** *April 20, 2017 22:24*

**+Steve Clark** You need two so one is not going to do you much good! ;-)


---
**Steve Clark** *April 20, 2017 23:07*

Darn...I should have said set! Heh... Heh...


---
**Craig “Insane Cheese” Antos** *April 29, 2017 18:55*

Just installed a pair of this tonight, made getting perfect alignment on the first mirror simple. Getting them installed on the other hand... Took me a few headscratches. There's not much room in there for laser tube, screwdriver and brackets. 



Ended up having to leave the rear (inner most) frame bolts and the sliders off, and jiggle the bases around the tube+clamps and tighten the frame down then snap the sliders on, finally nipping up everything after alignment. It's a good design. If I ever end up pulling the tube, I'll probably slip a small spring over the adjustment bolts so the clamps are pushed back - it's only an issue if you need to loosen them to move the tube down or in.



Possibly having sockets for nuts in the base would make external assembly easier, that way your able to pushfit the nuts and bolt up through the floor.



If anyone's interested in Metric, the bolts for one clamp are: 4x M5x50mm, 2x M4x30mm and 4x M3x10-20mm. I only had M4 hex head so had to ream out the hole in the clamps to recess the heads.


---
**Craig “Insane Cheese” Antos** *April 29, 2017 18:56*

Printed in PLA @0.2mm

![images/f9266a1c5c447569656f50eba8ff935f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9266a1c5c447569656f50eba8ff935f.jpeg)


---
**HalfNormal** *April 29, 2017 18:57*

I have mine printed and going to try and get them on soon.


---
**HalfNormal** *April 29, 2017 19:05*

If anyone is interested, here is the size comparison between the **+DIY3DTECH.com** version and **+Steven Kalmar** version.

![images/a4e8d2387ab0eae38d2d433c090b78ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4e8d2387ab0eae38d2d433c090b78ce.jpeg)


---
**Steven Kalmar** *April 29, 2017 20:50*

**+HalfNormal** How did it fit?  I only have access to my machine, so I don't know the variations of the different machines out there.




---
**HalfNormal** *April 29, 2017 20:52*

Not in yet but the test fit looks good. Still working on mods so the unit is not all together. 


---
**Steven Kalmar** *April 29, 2017 21:05*

**+Craig Antos** There isn't much room in the channel.  Fortunately it's not something you'd have to install often.  I made the slots in the base because I didn't know how much slide adjustment would be required.  I guess not much since the mirror has some slide adjustment to it.  It wouldn't be difficult to embed nuts in the base and feed the bolt through the bottom, except that the bolt length would have to be just enough to go through the nut without it interfering with the tube.  The base would have to be wider to accept the nut.  That's why I decided to use socket head bolts through the top.  All I had on hand were long bolts, so this way length didn't matter.  I'll probably make some changes and update Thingiverse after I have some time with them.  I haven't used the machine very much.  I just got everything dialed in and made my first successful cut.  



A spring is an excellent idea.  Perhaps from an old pen.  


---
**Craig “Insane Cheese” Antos** *April 30, 2017 04:26*

**+Steven Kalmar** Makes sense, I was just lucky having some 15mm long M5 bolts on hand. I'd say most people are probably going to have longer bolts. Also depends on head type, if you have hex head instead of Phillips/Flat/Socket.


---
**Kirk Yarina** *June 03, 2017 11:52*

Pretty easy to make bolts shorter (longer is much harder :) ).  Screw on a couple nuts, cut to length (hacksaw, Dremel, etc), file or grind a small bevel (aka chamfer) on the threaded end, then unscrew the nuts to realign the threads.  Might take a try it two the first time so get some extras or practice on one you don't need.


---
*Imported from [Google+](https://plus.google.com/106762627155275597313/posts/H3RHp65DPSq) &mdash; content and formatting may not be reliable*
