---
layout: post
title: "Only if I had the fund for it, I would totally sell smoothie based k40's with a number of upgrade options in the US as a Legit seller"
date: December 26, 2016 01:30
category: "Discussion"
author: "Jonathan Davis (Leo Lion)"
---
Only if I had the fund for it, I would totally sell smoothie based k40's with a number of upgrade options in the US as a Legit seller. (generally speaking, since I am not a engineer like most of the people here)





**"Jonathan Davis (Leo Lion)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 01:53*

Could always try contact Chinese manufacturers & see if you can work out some kind of deal on consignment, where they give you x units @ a fixed price/percentage that you pay after selling each one.


---
**Jonathan Davis (Leo Lion)** *December 26, 2016 01:55*

**+Yuusuf Sallahuddin** True but with my funds that is out of the question currently


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 02:05*

**+Jonathan Davis** The point I was meaning was to not pay anything up front. They give to you for free & you pay them when you sell it. Would be hard to build credibility with a seller to the point that they allow that, unless you happened to already know one that would advocate for you on your behalf with their manufacturer/supplier.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 02:07*

**+Jonathan Davis** Alternatively, another option would be to do up a thorough business plan & crowd-source funds from family, friends or even take a proper bank loan.


---
**Jonathan Davis (Leo Lion)** *December 26, 2016 02:14*

**+Yuusuf Sallahuddin** Yeah, i know that is a option but i'm not a fan of the idea where the bank owns my business.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 02:36*

**+Jonathan Davis** Yeah me either, hence I haven't gone that route myself. The crowd-fund would be okay if you have enough family & friends to front you some $. Of course, you'd have to give them something in return (e.g. a custom laser engraved wall sign or something).


---
**Jonathan Davis (Leo Lion)** *December 26, 2016 02:38*

**+Yuusuf Sallahuddin** That i can do but with my crazy machine that i own a chinese benbox system it can be difficult.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 02:42*

**+Jonathan Davis** Ah, but if they all front you the $, you will have heaps of K40s available to use one of them.


---
**Jonathan Davis (Leo Lion)** *December 26, 2016 02:45*

**+Yuusuf Sallahuddin** yeah but i only have the the one machine i bought from Grearbest. Even then that still was trip just to get them to send a refund. On top of another $110 just to get the parts to make it work.


---
**Brent Crosby** *December 26, 2016 07:10*

Do a small ($6k ~ $8k) Kickstarter for 10 machines. I'd pay extra for a machine that was properly configured, with good electronics and "real" software license, or a fully documented open source tool chain. 



Just keep the kickstarter small and financially open (charge for all costs and include say 20% for your time /profit).  Use the kickstarter to prove your business model. By limiting it to 10 units, you will likely get funding and you will not have to attempt to gear up for a 1000 piece run.


---
**Brandon Satterfield** *December 27, 2016 02:04*

Not far out, but not K40s either. ;-) more to come. 


---
**Jonathan Davis (Leo Lion)** *December 28, 2016 05:01*

another idea I had was for an all in one type of system, 3D printer, CNC Mill, Laser engraver. One machine serving multiple purposes with a budget friendly intro price.


---
**Brent Crosby** *December 28, 2016 05:25*

Too much engineering for one person. 


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/P8vKSQoD62r) &mdash; content and formatting may not be reliable*
