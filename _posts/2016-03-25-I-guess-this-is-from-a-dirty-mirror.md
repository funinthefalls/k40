---
layout: post
title: "I guess this is from a dirty mirror?"
date: March 25, 2016 21:56
category: "Discussion"
author: "Brandon Satterfield"
---
I guess this is from a dirty mirror? 3rd cut, trying to make it through 1/8" white board. Burned the mirror out.

![images/3018e53af0da5491ac4bb92e63ae2703.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3018e53af0da5491ac4bb92e63ae2703.jpeg)



**"Brandon Satterfield"**

---
---
**Phillip Conroy** *March 25, 2016 22:33*

That would be my guss,you should see my laser tube i cut mdf for 3hours a day and i can no longer see into my tube it is that covered with mdf dust-still works fine ,had to clean mirriors a few times and focal lens every 3 hours,i have air assist however moisture is commingthrough theair lne and dirtyinng the lens- i have 2 water traps and stillmoisture is a problem


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 25, 2016 23:09*

**+Brandon Satterfield** You say "white board"... Are you referring to melamine board or something else? [http://www.homedepot.com/catalog/productImages/400/60/604278a9-44ba-40b7-b3b0-ed0d15b2cac1_400.jpg](http://www.homedepot.com/catalog/productImages/400/60/604278a9-44ba-40b7-b3b0-ed0d15b2cac1_400.jpg)



**+Phillip Conroy** Do you live in a humid area? Maybe a room dehumidifier would assist with your moisture in the air issue...


---
**Joe Spanier** *March 26, 2016 00:13*

It could be a dirty mirror. The k9 glass mirrors that come in the k40 are utter shit. A little bit of dust and the heat gets generated quickly and they burn out and crack. The Si and MO mirrors are much more tolerant but you still need to clean to clean them with every use. It's also important to make sure they are completely dry before you hit them with the laser


---
**Brandon Satterfield** *March 26, 2016 00:50*

**+Yuusuf Sallahuddin** correct. 


---
**Brandon Satterfield** *March 26, 2016 00:51*

**+Joe Spanier** thanks for the advice and recommendation. I'll hit you up on the other side. 


---
**Alex Krause** *March 26, 2016 01:01*

**+Phillip Conroy**​ look into a dessicant moisture trap and make sure you are draining your compressor regularly I use a dessicant trap at work for our Haas CNC they start out as blue crystals and turn purple after they are fully saturated this unit is set up after running thru two other traps first 


---
**ThantiK** *March 26, 2016 02:49*

That's weird **+Joe Spanier**, we had a converted K40 (FullSpectrum) and took none of these precautions for close to 2 years and never had any issues. :[


---
**Joe Spanier** *March 26, 2016 03:04*

Different k40s come with different stuff. Your mirrors might have been better than the ones that came with mine. 



The waiting for the cleaning fluid to dry is a fairly common killer of mirrors. A little bit of alcohol won't kill it but a "wet" mirror will likely shatter. You might get by with it more than I do with my lasers (100 & 150w)


---
*Imported from [Google+](https://plus.google.com/111137178782983474427/posts/fSQcZ4AhN8m) &mdash; content and formatting may not be reliable*
