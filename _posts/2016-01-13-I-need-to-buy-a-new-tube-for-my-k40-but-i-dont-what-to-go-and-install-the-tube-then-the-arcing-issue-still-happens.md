---
layout: post
title: "I need to buy a new tube for my k40 but i don't what to go and install the tube then the arcing issue still happens"
date: January 13, 2016 17:12
category: "Discussion"
author: "george ellison"
---
I need to buy a new tube for my k40 but i don't what to go and install the tube then the arcing issue still happens. How do i know if my PSU has gone? When i power up the head still moves so is this sign that it is working fine? Many thanks. 





**"george ellison"**

---
---
**Pete OConnell** *January 14, 2016 12:24*

from what I've read there's 2 power supplies, one for the motors and one for the tube. Can you see where it is arcing? I've previously found even with the smallest point, do it in a dark room and you can see the arcing occurring.



Alternatively, get someone who's great with electrics to test it to find where it is arcing, then you can fix what needs fixing :)


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/eXYKjb4ZyX8) &mdash; content and formatting may not be reliable*
