---
layout: post
title: "What can I do on a Smoothie and can not be done on a Ramps?"
date: June 22, 2016 11:32
category: "Discussion"
author: "Mircea Russu"
---
What can I do on a Smoothie and can not be done on a Ramps? I mean strictly about finished lased product, not web interface, ethernet, wifi.

I know Smoothie is 32bit and has more processing  horsepower, but I already have plenty Megas 2560 and Ramps boards while forking out $50 for a Smoothie plus 3 weeks waiting makes it less appealing to me.

If anyone has used both Ramps and Smoothie maybe can chime in about differences.

 Thank you!





**"Mircea Russu"**

---
---
**Stephen Sedgwick** *June 22, 2016 12:30*

Interesting, I was wondering the same thing - with this being said what is the easiest setup within smoothie to get going with the full functions of the K40?  Has anyone been able to get around the power function with acceleration with the megas and ramps (as I hadn't seen that pointed out previously before I ordered ramps/mega combo (haven't installed it just ordered it)?


---
**Stephen Sedgwick** *June 22, 2016 12:43*

Do you know what setup of the smoothie I should be ordering?  Is there a K40 recommendation for what smoothie?  when I looked previously it was smoothie x5 x4 x3 etc.. and really don't want to order more than is needed - I will likely be switching to other digital controls later, should I be getting one of the all in one kits for it?  do they make a plug and play version are is it all going to require replacing plugs etc?  (don't want to cut if I can keep from doing that so will build my own boards with plugs if needed)


---
**Brian Bland** *June 22, 2016 13:15*

In the near future there will be just that.  A drop in Smoothie compatible board that will plug in to a K40.  Already in the testing phase.


---
**Stephen Sedgwick** *June 22, 2016 13:23*

Were you the one creating that Brian?  I haven't looked back at the posts but was hoping for a price on the one that will be coming down the road...


---
**Mircea Russu** *June 22, 2016 13:30*

**+Peter van der Walt** does the original nano M2 board support modulating on acceleration? It seems I'll go down the smooth road in the end.


---
**Ray Kholodovsky (Cohesion3D)** *June 22, 2016 15:23*

**+Stephen Sedgwick** Brian is testing 2 smoothie driven boards I designed and am gearing up to sell, one of which is a drop in upgrade to the k40, right down to the stock connectors it comes with. Would be as simple as re plugging 3 wires, editing a config file, and being ready to run your first job. And yes it will be more affordable than anything else out there (excluding China). 


---
**Stephen Sedgwick** *June 22, 2016 15:55*

Ray Kholoovsky, do you have a general price idea at this point in time?  and what you are thinking for lead time?  I am wanting to figure out what I should be ordering to work with this (as I am not in a huge hurry) I just want to have the cleanest end point.  Feel free to private message, me if it is something we can talk about offline..


---
**Tony Schelts** *June 22, 2016 16:24*

Now this exciting to hear. I really want to uprade at ome point but have absolutely no electronic experience.  Does thid mean it ont need a middle man board etc.


---
**Ray Kholodovsky (Cohesion3D)** *June 22, 2016 16:25*

**+Tony Schelts** Correct, everything will be part of the K40 bundle.  The ribbon cable connector is part of the main board itself, so no middle man, no level shifters, just 20 minutes of your time.


---
**Tony Schelts** *June 22, 2016 21:00*

When it likely to available to the uk


---
**Ray Kholodovsky (Cohesion3D)** *June 22, 2016 21:07*

**+Tony Schelts** I can ship international no problem (I'm in the USA). Not that expensive either. 

I'm currently waiting on round 2 of the boards to come in. If you'd like one, please let me know. 


---
**Tony Schelts** *June 22, 2016 21:51*

What sort of price? very happy to try one and let the uk users know how easy it was to install???:)  joking aside what sort of price.?


---
**Ray Kholodovsky (Cohesion3D)** *June 22, 2016 22:05*

**+Tony Schelts** I started a hangout with you to discuss further. 


---
**Jason He** *June 23, 2016 00:04*

Hi Ray, thanks for your great work on this drop-in Smoothie board. I am quite interested. How far away is the "near future" when a finished and tested product would be sold?


---
**Ray Kholodovsky (Cohesion3D)** *June 23, 2016 00:10*

**+Jason He** Version 1 is working on my own K40 and **+Brian Bland** has had some success with it as well (he has the ribbon cable, I have the individual wire version, so we have both types covered).  Version 2 of boards are currently being manufactured.  I expect to have some available in a few weeks.  I'm still calling it testing,  let me know if you're interested.  Kickstarter coming soon after that, where we need to gather a lot of sales for a contract manufacturer to produce them.


---
**Jason He** *June 23, 2016 00:15*

Sounds like good stuff. I'll wait eagerly for the Kickstarter to purchase my own.


---
**Ray Kholodovsky (Cohesion3D)** *June 23, 2016 00:16*

Glad to hear it. Follow me for updates. 


---
**Justin Mitchell** *June 23, 2016 09:24*

**+Ray Kholodovsky** +1 on price for the smoothieboard to the uk.  Been looking for a while to upgrade the K40 in our hackspace but been blocked on working out exactly what the connectors are, as we want the ability to revert to the original board if we have issues.


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/Uxf69EXgCeB) &mdash; content and formatting may not be reliable*
