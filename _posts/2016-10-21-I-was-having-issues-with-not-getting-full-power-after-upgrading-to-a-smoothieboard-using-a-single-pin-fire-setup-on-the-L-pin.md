---
layout: post
title: "I was having issues with not getting full power after upgrading to a smoothieboard, using a single pin fire setup on the L pin"
date: October 21, 2016 03:56
category: "Smoothieboard Modification"
author: "Anthony Bolgar"
---
I was having issues with not getting full power after upgrading to a smoothieboard, using a single pin fire setup on the L pin. I had removed the Pot. Turns out you either need the POT in place or tie 5V to the IN line on the PSU. Tying 5V to IN solved my issue and now I get full power to the laser tube (24mA) where as before without anything oin the IN line I was limited to 8mA.

  My final issue is to get the limit switches working properly, my K40 has optical limit switches, and they don't seem to be playing nice with the smoothieboard. I think I will just install some good old mechanical ones.





**"Anthony Bolgar"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 21, 2016 05:10*

Yeah, I wouldn't do that. I'd put the pot back in place and use it to set the ceiling. 17-20mA is already pretty high and people say not to run the tube at full power. 


---
**Anthony Bolgar** *October 21, 2016 06:17*

I am limiting it in the Smoothie config. But I am planning on putting the POT back in the circuit anyways. And I am not worried about tube life, I consider it a consumable, and also higher power means faster cuts, so less time on the tube. So either low power and long job times or higher power and shorter job times. Not sure how much of a difference the lower power really makes on tube life when you consider that.


---
**Anthony Bolgar** *October 21, 2016 07:12*

Fixed my homing issues with the optical stops, all works as it should now!


---
**Don Kleinschnitz Jr.** *October 21, 2016 12:21*

**+Anthony Bolgar** what turned out wrong with the endstops?


---
**Don Kleinschnitz Jr.** *October 21, 2016 12:58*

We now have a lot more intelligence in how the internals of these supplies work. I again took a time out from my conversion to analyze  the schematic **+Paul de Groot** provided and I also just completed a preliminary trace of **+Kim Stroman**'s supply. 

One big revelation is that these supplies are "internally" NOT using the same control circuits. This may be why we have such a confusing set of results from various configurations.



BTW you verified that: The pot is necessary to get full power as the current control connects (directly in some and through an  opto in others),  to the power supplies PWM control. Leaving it open will keep the power at a low and undetermined level. In this state noise could change the power level.



<s>----------------------------</s>

Can I see a picture of your power supply?

Did you use a level shifter in line with the L connection?

Did you use the "L" connection on the DC connector for PWM or some other control.



I will post what we have recently found more completely some time today as I get all this stuff straight in my head.


---
**Anthony Bolgar** *October 21, 2016 14:30*

My supply is the one with the green connectors. I used a level shifter in line with the L connection using PWM from the smoothieboard 2.5 pin! (inverted)


---
**Don Kleinschnitz Jr.** *October 21, 2016 14:57*

**+Anthony Bolgar** is your 2.5 pin wired from the edge screw terminals or internal to the board? Is the L you refer to the one in the rightmost DC power connector?


---
**Don Kleinschnitz Jr.** *October 21, 2016 15:00*

**+Anthony Bolgar** another question does you PS have a green or red power led on its circuit card?


---
**Anthony Bolgar** *October 21, 2016 16:28*

It has a red power LED. And the optical end stop issue was solved by changing the 5V from the PSU to the middleman board and using the smoothie endstop +5V and ground to power the middleman board. Had some sort of wierd grounding issue when using 5V from the PSU.


---
**Anthony Bolgar** *October 21, 2016 23:29*

And the 2.5 pin is the internal, not from the edge. tHE l IS THE ONE IN THE CENTER SECTION


---
**Don Kleinschnitz Jr.** *October 21, 2016 23:53*

**+Anthony Bolgar** **+Ariel Yahni**  OMG I think I might see the disconnect. 

This pin is the processor output not an FET's open drain? 

That is why a level converter is being used "light comes on here".

I have to go back and check the schematic and get back to everyone. 

It also explains why the configuration settings are different.


---
**Ariel Yahni (UniKpty)** *October 22, 2016 00:01*

**+Don Kleinschnitz**​, **+Alex Krause**​ has run both connection. On the 2.4 FET and the 2.4 pin pwm before it


---
**Don Kleinschnitz Jr.** *October 22, 2016 00:07*

**+Ariel Yahni** **+Alex Krause** ok.... I am sure that we realize that the configuration is different for these two setups and that a level shifter will not work with the FET connection, and correct grounding is important :).


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/5FkgZewsshY) &mdash; content and formatting may not be reliable*
