---
layout: post
title: "OK so my Saite head arrived this morning and it has come with two washers, one with a recess"
date: June 23, 2016 12:19
category: "Modification"
author: "Pete Sobye"
---
OK so my Saite head arrived this morning and it has come with two washers, one with a recess. Can anyone please explain what these are for? Thanks Pete

![images/67336c94fa04e7bb27f3c983b5e9a560.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/67336c94fa04e7bb27f3c983b5e9a560.jpeg)



**"Pete Sobye"**

---
---
**Jeff Kwait** *June 23, 2016 13:01*

question: is this usable for adjustable focus?


---
**Stephen Sedgwick** *June 23, 2016 13:01*

are these for the different size lens that it can take?


---
**Pete Sobye** *June 23, 2016 13:06*

**+Jeff Kwait** Apparently so, The brass finger screw undoes and the tube below the block slides up and down.


---
**Jeff Kwait** *June 23, 2016 13:32*

Not that I need another head however...


---
**Jeff Kwait** *June 23, 2016 13:33*

is this the one you ordered

[http://www.aliexpress.com/item/CO2-Laser-Head-Laser-Engraving-Cutting-Machine-Engraver-High-Quality/32355354866.html?spm=2114.01010208.8.48.uTwh7n](http://www.aliexpress.com/item/CO2-Laser-Head-Laser-Engraving-Cutting-Machine-Engraver-High-Quality/32355354866.html?spm=2114.01010208.8.48.uTwh7n)


---
**Pete Sobye** *June 23, 2016 13:52*

**+Jeff Kwait** its similar but this is the one I ordered.

[http://www.ebay.co.uk/itm/252303388050](http://www.ebay.co.uk/itm/252303388050)


---
**Pete Sobye** *June 23, 2016 13:54*

**+Jeff Kwait** Plus these mirrors and lens but they have not arrived yet. 

[http://www.ebay.co.uk/itm/172050707677](http://www.ebay.co.uk/itm/172050707677)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 23, 2016 14:27*

I'd imagine the washers are as **+Stephen Sedgwick** mentions, for holding your lens in position. Different washers for different sized lens. The one with recess possibly fits the standard 12mm lens.


---
**Jeff Kwait** *June 23, 2016 14:43*

I just sent the seller on Ali a question about the mounting plate as listed in your ebay link did you have to buy a new mounting plate?


---
**Pete Sobye** *June 23, 2016 15:34*

**+Jeff Kwait** haven't tried mounting it yet Jeff. Someone did mention I would have to modify the plate. Probably make the home bigger. 


---
**Pigeon FX** *June 24, 2016 10:30*

I have one of these, the washers are for holding the stock 12mm lens, if your using a 18mm you wont need them, though you will need to remove the knurled nut at the top of the block to get the laser to line up with the cutter heads mirror.......this is how I did it: 



[https://plus.google.com/photos/photo/107264773673623393202/6285823043333439618?sqid=118113483589382049502&ssid=d2773326-fff1-4c8b-9ae1-c825d14eed20](https://plus.google.com/photos/photo/107264773673623393202/6285823043333439618?sqid=118113483589382049502&ssid=d2773326-fff1-4c8b-9ae1-c825d14eed20)



And regards the plate, yes you will need to open up the stock hole and shim the head in place, or get one of the alternative large hole plates from Saite if fabricating your own is not an option. 




---
**Pippins McGee** *July 27, 2016 02:43*

**+Pete Sobye** can you show photo of it installed in your machine please?

mine just arrived and I installed  - just want to see if it looks the same as yours installed.



mine came with a whole new carriage and wheels because I wanted the new top plate.

the new carraige doesn't have a piece of metal sticking out the left side however for the optical end-stops in the k40.... so not sure how I can use the new carriage.



I guess I can just use the saite head and top plate on the stock carriage...



a photo of yours installed however would be nice as I am curious


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/fqqhvyjJKeu) &mdash; content and formatting may not be reliable*
