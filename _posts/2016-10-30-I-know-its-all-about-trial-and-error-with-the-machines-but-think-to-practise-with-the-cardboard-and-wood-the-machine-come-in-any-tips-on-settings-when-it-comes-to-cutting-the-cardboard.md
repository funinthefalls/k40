---
layout: post
title: "I know it's all about trial and error with the machines but think to practise with the cardboard and wood the machine come in, any tips on settings when it comes to cutting the cardboard ?"
date: October 30, 2016 00:12
category: "Discussion"
author: "Rob Turner"
---
I know it's all about trial and error with the machines but think to practise with the cardboard and wood the machine come in, any tips on settings when it comes to cutting the cardboard ? 





**"Rob Turner"**

---
---
**Ariel Yahni (UniKpty)** *October 30, 2016 00:54*

Start with an easy set of numbers to remember, example 6mA power 10mms speed and go from there


---
**Rob Turner** *October 30, 2016 10:46*

Thanks is it true that the stock laser won't cut plywood wood wbp, I read it on website that gave u all the settings for the stock machine ? 


---
**Ariel Yahni (UniKpty)** *October 30, 2016 11:59*

**+Rob Turner**​ not sure I understood the question. wbp? What's that?  You can cut 3 or 6mm plywood. 3mm should be easy at 10mA power and 10mms speed one pass.  Thicker will requiere multiple passes 


---
*Imported from [Google+](https://plus.google.com/114544098138525257938/posts/Kktc3m9qtuZ) &mdash; content and formatting may not be reliable*
