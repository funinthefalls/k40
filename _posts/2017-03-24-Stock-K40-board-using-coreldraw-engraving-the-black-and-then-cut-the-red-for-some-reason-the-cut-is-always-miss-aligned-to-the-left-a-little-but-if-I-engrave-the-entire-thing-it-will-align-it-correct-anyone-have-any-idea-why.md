---
layout: post
title: "Stock K40 board using coreldraw engraving the black and then cut the red for some reason the cut is always miss aligned to the left a little but if I engrave the entire thing it will align it correct anyone have any idea why"
date: March 24, 2017 01:20
category: "Discussion"
author: "Robert Selvey"
---
Stock K40 board using coreldraw  engraving the black  and then cut the red for some reason the cut is always miss aligned to the left a little but if I engrave the entire thing it will align it correct anyone have any idea why it might be doing that?

![images/f775d5ff4e32d1de566ddf11bf9f659a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f775d5ff4e32d1de566ddf11bf9f659a.jpeg)



**"Robert Selvey"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 24, 2017 02:35*

You need to put a reference point in the top left corner for both engrave & cut. A single pixel should do it.



Alternatively, what I always used to do with the stock software/hardware was to place a rectangle around all elements. Give it no fill & no stroke. But, when selecting the elements to engrave (e.g. TEST in your case) I would select it all & select the rectangle. Even though the rectangle is no fill & no stroke it would be recognised as "there" by the software for alignment purposes. It won't be engraved though. Then when selecting the H for cut, just select the rectangle also. Again it will be recognised as "there" for alignment purposes, but won't cut.


---
**Robert Selvey** *March 24, 2017 03:21*

I did have a reference point in the top left still moved the cut to the left a little.




---
**Ned Hill** *March 24, 2017 04:23*

Check and make sure Refer-X and Refer-Y are both set to the same thing for cutting and engraving (i.e. set to zero).


---
**Rodney Huckstadt** *March 24, 2017 05:16*

Check these settings

![images/1968e358d88645ae62a22d890e64297c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1968e358d88645ae62a22d890e64297c.jpeg)


---
**Rodney Huckstadt** *March 24, 2017 05:18*

Both should be set to wmf. I had this issue once before. Stole this pic fm the web. But will confirm mine when i get home. I haf the exact problem and this fixed it


---
**Robert Selvey** *March 24, 2017 20:06*

Putting them both on wmf seem to fix it but now they do the cut lines twice, is there a way to stop that?


---
**Ned Hill** *March 24, 2017 21:10*

Cut line widths need to be less than or equal to 0.01mm in corellaser or it cuts both side of the line. (Might be less than or equal to 0.1mm but I always used 0.01mm)








---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/18cEfrf46KU) &mdash; content and formatting may not be reliable*
