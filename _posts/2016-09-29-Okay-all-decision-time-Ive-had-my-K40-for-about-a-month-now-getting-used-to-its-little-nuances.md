---
layout: post
title: "Okay all, decision time. I've had my K40 for about a month now getting used to it's little nuances"
date: September 29, 2016 19:29
category: "Discussion"
author: "J.R. Sharp"
---
Okay all, decision time. I've had my K40 for about a month now getting used to it's little nuances. I've managed to get good cuts in both 3mm plywood and acrylic, at 10/10 in a single pass. Before I start prototyping with this, I want to get some upgrades. I've gotten fairly comfortable with Corel and can nab a copy for 100.00.



So, if I intend on continuing with Corel, should I worry about a smoothieboard or similar? Also between the smoothie and LightObject upgrade kits, which rules?



I really need a better way to control my height, so I was looking at the Z-Axis table and of course air assist. I would say that this is gonna be 95% cutting and 5% engraving, so I am not sure a smoothieboard is entirely in order. 



Getting annoyed at inconsistencies and whatnot with my cut depth has made me feel like a better tray and air assist is in order with the copy of Corel X8.





**"J.R. Sharp"**

---
---
**Anthony Bolgar** *September 29, 2016 19:32*

If you want to keep using Corel, then a smoothieboard upgrade is not necessary. (But smoothiebaord with LaserWeb is a winning combination) But an air assist head will make a major improvement on your ability to cut, and engrave. You lens will stay cleaner, and therefor last longer, and you will be able to cut thicker materials with less power. As for an adjustable bed, I personally love the one I have, makes setup a breeze. I just made a small block that I place under the head with the correct distance for the focal length, and raise the bed until it just touches.


---
**Ariel Yahni (UniKpty)** *September 29, 2016 19:36*

I would invest this 100 in smoothie and use Inkscape. At the end for vectors is the same thing ( vectors = cut).  That way you get independence on OS and have a better workflow. Doing 10/10 allready have a very well calibrated setup so I do agree that focus it's very important. The LO Z table will limit more the size of your cut area so beware. 


---
**greg greene** *September 29, 2016 20:06*

4 x 4 adjustable lab table 15 bucks delivered on ebay


---
**J.R. Sharp** *September 30, 2016 01:00*

**+Anthony Bolgar** Can you explain that bed a bit more?




---
**Anthony Bolgar** *September 30, 2016 01:39*

One of my lasers has a motorized adjustable bed. I just use the up and down buttons to change the distance from the laser head. On my other laser (K40) I use a manually adjustable bed that I built using threaded rods and nuts at the 4 corners, I raise it or lower it to be the correct distance from the laser head to be in focus. [https://plus.google.com/+AnthonyBolgar/posts/Cm2NroFkRqQ](https://plus.google.com/+AnthonyBolgar/posts/Cm2NroFkRqQ)


---
**J.R. Sharp** *September 30, 2016 19:25*

Gotcha, seems like a little better version of what I had. I will head back to the redesign board with it :-)




---
**Anthony Bolgar** *September 30, 2016 19:36*

Glad I could inspire you a little.


---
*Imported from [Google+](https://plus.google.com/116586822526943304995/posts/BQSwqWjVf2F) &mdash; content and formatting may not be reliable*
