---
layout: post
title: "Finally my K40 is working perfect"
date: July 16, 2016 18:06
category: "Modification"
author: "Jose Castillo"
---
Finally my K40 is working perfect

![images/90ba1762eac3bfd8794b6d65df888d87.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90ba1762eac3bfd8794b6d65df888d87.jpeg)



**"Jose Castillo"**

---
---
**find nene** *July 16, 2016 18:39*

Where did you get the laser and air holder? 


---
**Ariel Yahni (UniKpty)** *July 16, 2016 19:37*

3d printed 


---
**greg greene** *July 16, 2016 21:13*

Very cool holder - but I don't have a 3d printer :(


---
**Ned Hill** *July 16, 2016 22:31*

**+greg greene** If you get someone's 3d print files you can always try 3d print hub to find someone in your area to print it for you.  [https://www.3dhubs.com/](https://www.3dhubs.com/)


---
**greg greene** *July 17, 2016 01:04*

Getting the file would be the first step then !


---
**Ned Hill** *July 17, 2016 01:10*

You can always check [http://www.thingiverse.com/](http://www.thingiverse.com/) for files.


---
**greg greene** *July 17, 2016 01:15*

Thanks!


---
**Travel Rocket** *August 12, 2016 03:34*

Hi! where did you purchase your air assist?


---
*Imported from [Google+](https://plus.google.com/100644756850747387827/posts/Vq1XC3TM8B5) &mdash; content and formatting may not be reliable*
