---
layout: post
title: "Got the line lasers installed. Was a 12mm hole in the mounting spheres and my line modules were about 8.5mm dia"
date: January 19, 2016 03:53
category: "Modification"
author: "David Cook"
---
Got the line lasers installed. Was a 12mm hole in the mounting spheres and my line modules were about 8.5mm dia. So I built up the diameter with multiple layers of heatshrink tubing then press fit the lasers. Worked good.  Now no matter the height of the work piece, the Intersection is always in the beam path.  Not sure if it will be useful but it's been fun messing with it so far. 



![images/8ff39cfd6c85b0a940a87d73578c9165.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ff39cfd6c85b0a940a87d73578c9165.jpeg)
![images/d0c8b3bb4f307d9fe751bd1d6702bf16.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d0c8b3bb4f307d9fe751bd1d6702bf16.jpeg)
![images/5f58270263d19d90c42e03c506d66ee5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5f58270263d19d90c42e03c506d66ee5.jpeg)
![images/f1c8a3405fc56da02a757424ad93c64f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1c8a3405fc56da02a757424ad93c64f.jpeg)
![images/abef98b56bae6308251279b03f8615e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/abef98b56bae6308251279b03f8615e1.jpeg)
![images/a999b311e62936beab613ca4f588890f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a999b311e62936beab613ca4f588890f.jpeg)
![images/283a0d5c4ab377f89c621b9d60639a4d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/283a0d5c4ab377f89c621b9d60639a4d.jpeg)

**"David Cook"**

---
---
**beny fits** *January 23, 2016 07:08*

i love it how can i get one 


---
**Ariel Yahni (UniKpty)** *April 29, 2016 19:18*

**+David Cook** do you have a part number for those laser pointers you can share?


---
**David Cook** *April 29, 2016 20:53*

**+Ariel Yahni**  I got them from Amazon [http://www.amazon.com/AixiZ-Module-Generator-ECONOMICAL-MODULE/dp/B00JJXI1BY?ie=UTF8&psc=1&redirect=true&ref_=oh_aui_detailpage_o00_s00](http://www.amazon.com/AixiZ-Module-Generator-ECONOMICAL-MODULE/dp/B00JJXI1BY?ie=UTF8&psc=1&redirect=true&ref_=oh_aui_detailpage_o00_s00)


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/hJJ83MyZJDN) &mdash; content and formatting may not be reliable*
