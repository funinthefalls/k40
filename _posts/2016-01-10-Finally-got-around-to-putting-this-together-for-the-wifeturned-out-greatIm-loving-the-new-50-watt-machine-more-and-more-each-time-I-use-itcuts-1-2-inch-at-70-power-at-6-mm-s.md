---
layout: post
title: "Finally got around to putting this together for the wife...turned out great....I'm loving the new 50 watt machine more and more each time I use it.....cuts 1/2 inch at 70 power at 6 mm/s ."
date: January 10, 2016 21:08
category: "Object produced with laser"
author: "Scott Thorne"
---
Finally got around to putting this together for the wife...turned out great....I'm loving the new 50 watt machine more and more each time I use it.....cuts 1/2 inch at 70 power at 6 mm/s .

![images/328f317edcc48b3994720afc63c26c29.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/328f317edcc48b3994720afc63c26c29.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 10, 2016 21:36*

Looks great. Nice work. I'm wondering, does this open like a box?


---
**Scott Thorne** *January 10, 2016 21:38*

Yes it the top comes off....I'm going to stain it later today.


---
**Todd Miller** *January 10, 2016 22:30*

What type of 1/2 material did you use ?



How many ma's is 70%  ?


---
**Scott Thorne** *January 10, 2016 23:16*

Birch plywood....I'm not sure how many ma's that it....the new machine only gives a percentage of power....I cut some1/2 inch acrylic at the same setting.


---
**ThantiK** *January 10, 2016 23:20*

I'd run a quick bit of sandpaper over the finger cuts, pushing the sandpaper outwards toward the edge (so as not to contaminate the un-cut wood surface, and to get a little bit of the char off of the cuts)


---
**Scott Thorne** *January 11, 2016 14:21*

**+ThantiK**...I thought about that but the people that are buying them like the way it looks with the black edges...different strokes for different folks I guess...Lol


---
**Gary McKinnon** *January 13, 2016 12:00*

That would make a lovely chocolate box, very nice :)


---
**Scott Thorne** *January 13, 2016 16:51*

Thanks **+Gary McKinnon**.....I'm going to try and stain a whole sheet of the birch and then redo one and see how it turns out.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/PBvXUZ2Zm5H) &mdash; content and formatting may not be reliable*
