---
layout: post
title: "\"Best Of\" Links for/from K40 User's Community"
date: November 07, 2015 00:27
category: "External links&#x3a; Blog, forum, etc"
author: "Stephane Buisson"
---
"Best Of" Links for/from K40 User's Community





**"Stephane Buisson"**

---
---
**John von** *October 10, 2016 23:24*

Stephane, I had to change the mother board on my computer and all the software for my 40w laser engraver is functional. The problem is when I send a script to be engraver, the message is engraver is not connected. I checked the device id in the properties box and it is the same as what is on the board within the engraver. Can you help me.

John von


---
**Nitro Zeus** *March 28, 2017 03:42*

Do you have the usb drive that came with it? With the red ribbon? Its like a key to iniiälize the printer. When you insert it the pc will magiĉly reĉognize the printér.


---
**Sean Cherven** *April 18, 2017 21:55*

This file is no longer available??


---
**Stephane Buisson** *April 19, 2017 12:54*

just see that, strange as I have not done  anything, should be a dropbox issue. Those files are a bit outdated anyway.






---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/g9RhorfT1gn) &mdash; content and formatting may not be reliable*
