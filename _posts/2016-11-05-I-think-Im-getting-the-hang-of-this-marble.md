---
layout: post
title: "I think I'm getting the hang of this marble"
date: November 05, 2016 22:09
category: "Object produced with laser"
author: "Scott Thorne"
---
I think I'm getting the hang of this marble. 



![images/26f2420365029081d866e82c90219588.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/26f2420365029081d866e82c90219588.jpeg)
![images/42e9f76899a3d99db53c653fbc59fda7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42e9f76899a3d99db53c653fbc59fda7.jpeg)

**"Scott Thorne"**

---
---
**Alex Krause** *November 06, 2016 00:04*

Where are you getting the marble stock from?


---
**Scott Thorne** *November 06, 2016 00:10*

**+Alex Krause**...laser bits...it's 5x3x1...like 6 bucks each...real cheap


---
**Scott Thorne** *November 06, 2016 02:36*

**+Alex Krause**...[signwarehouse.com - Marble Chisel Edged Paperweight](https://www.signwarehouse.com/p/marble-chisel-edged-paperweight)


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/EYDHQucgKj6) &mdash; content and formatting may not be reliable*
