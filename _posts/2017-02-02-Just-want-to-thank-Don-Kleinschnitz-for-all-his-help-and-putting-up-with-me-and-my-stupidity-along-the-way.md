---
layout: post
title: "Just want to thank Don Kleinschnitz for all his help and putting up with me and my stupidity along the way"
date: February 02, 2017 00:37
category: "Discussion"
author: "Chris Menchions"
---
Just want to thank **+Don Kleinschnitz** for all his help and putting up with me and my stupidity along the way. I couldn't of done it without him !! Thanks a million!





**"Chris Menchions"**

---
---
**Don Kleinschnitz Jr.** *February 02, 2017 01:13*

:)


---
**greg greene** *February 02, 2017 01:53*

Yup, without Don and Ray and Peter and others, many of us - especially me, would be struggling along still at square one.


---
**Stephane Buisson** *February 02, 2017 07:04*

it's by underlining where you have difficulties, we can make it more clear for everybody to see.


---
**Chris Menchions** *February 08, 2017 16:55*

Well I've never seen a laser before so was new to it all. Lots of 3d printer experience which really wish there was more control over homing locations


---
*Imported from [Google+](https://plus.google.com/104576389148296391165/posts/cPYPKGGnqcQ) &mdash; content and formatting may not be reliable*
