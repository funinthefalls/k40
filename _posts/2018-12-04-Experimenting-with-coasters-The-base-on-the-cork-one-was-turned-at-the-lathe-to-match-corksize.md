---
layout: post
title: "Experimenting with coasters. The base on the cork one was turned at the lathe to match corksize"
date: December 04, 2018 19:48
category: "Object produced with laser"
author: "Don Kleinschnitz Jr."
---
Experimenting with coasters. 



The base on the cork one was turned at the lathe to match corksize. I wanted to laser cut the cork but the cork mostly burned rather than cut.  I used sealer  (polyurethane) to stop the burned areas from smearing after imaging.



The slate was etched using **+Ned Hill** 's recommendation and it was also sealed with urethane.



The slate was purchased from Michael's per Neds suggestion 





![images/2a58f4e803414c5145868d853ffd1456.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2a58f4e803414c5145868d853ffd1456.jpeg)
![images/f5ded6cd7f61515ff0fcc0cdb8524667.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f5ded6cd7f61515ff0fcc0cdb8524667.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Ned Hill** *December 04, 2018 19:52*

Nice. How did you get the yellow coloring into parts of the etching on the slate?


---
**Travis Sawyer** *December 04, 2018 21:03*

I've used spray lacquer on reclaim roofing slate (prior to etching).![images/3c9423e24174cf3c858f98a549583b42.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3c9423e24174cf3c858f98a549583b42.jpeg)


---
**Don Kleinschnitz Jr.** *December 04, 2018 22:11*

**+Ned Hill** it was free......i.e. no idea. Doesn't look that yellow in real life.


---
**Ned Hill** *December 04, 2018 22:25*

**+Don Kleinschnitz Jr.** So you did nothing to get a 2 tone effect?


---
**Don Kleinschnitz Jr.** *December 05, 2018 00:12*

**+Ned Hill** nope perhaps its the level of power i.e. the dark areas have proportionally more power and that turns it a yellow tint?


---
**Ned Hill** *December 05, 2018 00:14*

**+Don Kleinschnitz Jr.** hmmm interesting. I’ll have to play with that. 


---
**Fook INGSOC** *December 05, 2018 02:11*

...now you just need some Boo-Bees for Halloween!!!😂😂😂

[https://www.google.com/search?q=boobees](https://www.google.com/search?q=boobees)

[google.com - boobees - Google Search](https://www.google.com/search?q=boobees&client=firefox-b-1&biw=414&bih=722&tbm=isch&ei=gTMHXMajLMrmsgXL2rjgBQ&start=0&sa=N)


---
**Ned Hill** *December 05, 2018 05:28*

**+Don Kleinschnitz Jr.** Another question, did you seal it before or after lasering?


---
**Don Kleinschnitz Jr.** *December 05, 2018 13:37*

**+Ned Hill** after ..... I really think the yellow is being amplified by the camera it does not look that yellow in real life.




---
**Nigel Conroy** *December 05, 2018 14:30*

I love engraving slate. This is a cheeseboard. Ocean state job lot has coasters and chessboards fyi![images/5ecd883bd10ac40ace30f8f64f574c4d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5ecd883bd10ac40ace30f8f64f574c4d.jpeg)


---
**Ned Hill** *December 05, 2018 14:46*

**+Nigel Conroy** very nice. 


---
**Ned Hill** *December 06, 2018 02:07*

Was unwrapping a set of blank coasters and found this one with included pyrite (fools gold).![images/16c61880b130114ca6b0486e3e61f2fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/16c61880b130114ca6b0486e3e61f2fd.jpeg)


---
**Nigel Conroy** *December 06, 2018 02:10*

I was wondering what that was....![images/1eccfd84ad20e2e09a84a5692a10a576.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1eccfd84ad20e2e09a84a5692a10a576.jpeg)


---
**Ned Hill** *December 06, 2018 02:17*

Yep, apparently it's not uncommon in slate.


---
**Nigel Conroy** *December 06, 2018 02:18*

I've done a good bit for some craft fairs lately and noticed it on a few pieces


---
**Nigel Conroy** *December 06, 2018 02:18*

Just finished this one with Rhodesian armed forces logo and a family name![images/05dc09e7714861e84c23668de49a2f45.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05dc09e7714861e84c23668de49a2f45.jpeg)


---
**Don Kleinschnitz Jr.** *December 06, 2018 13:44*

**+Ned Hill** maybe the yellow in mine really is gold! lol!


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/7cs5jkSeZAc) &mdash; content and formatting may not be reliable*
