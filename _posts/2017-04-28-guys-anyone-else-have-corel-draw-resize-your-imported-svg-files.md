---
layout: post
title: "guys anyone else have corel draw resize your imported svg files?"
date: April 28, 2017 00:04
category: "Original software and hardware issues"
author: "Chris Hurley"
---
guys anyone else have corel draw resize your imported svg files? 





**"Chris Hurley"**

---
---
**Ned Hill** *April 28, 2017 00:06*

What version of CD are you using? 


---
**Chris Hurley** *April 28, 2017 00:19*

Corel Draw 12. With the laser draw pugin. 


---
**Ned Hill** *April 28, 2017 00:25*

Unfortuantly CD 12 is such an old version it has trouble handling current versions of different file types.  Which why I migrated away from CD 12.  I don't recall that specific problem with svg files but it's been awhile.  Hopefully someone else will chime in if there is a work around.


---
**Chris Hurley** *April 28, 2017 00:26*

Is there a better solution that I could use to run the cutter? 


---
**Steve Clark** *April 28, 2017 01:53*

For now I'm using the LaserDrw width and height  borders to check known sizes. A pain yes  but I'm able to get .005 "resolution with a little fiddling. I've got C3D on order so I not messing with it anymore as I can get my work done this way. 


---
**Ray Kholodovsky (Cohesion3D)** *April 28, 2017 02:00*

Real cad software and LaserWeb for the win.  This is the upgrade Steve is referring to: [cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Kelly Burns** *April 28, 2017 02:34*

Are you importing SVGs into Corel or are you talking about Corel generated SVG importing into Laserdraw are scaled incorrectly ?


---
**Chris Hurley** *April 28, 2017 02:38*

Importing in. If you scale them by 133.333% they seems to fit. 


---
**Kelly Burns** *April 28, 2017 02:40*

"Importing In" doesn't clarify which one.  


---
**Chris Hurley** *April 28, 2017 02:42*

Sorry taking a file and loading it into CD 12 from anywhere else. 


---
**Chris Hurley** *April 28, 2017 02:43*

The Cohesion 3d is really that easy? 


---
**Ray Kholodovsky (Cohesion3D)** *April 28, 2017 02:56*

The wiring is a straight swap for the most common models yes.  There is still a learning curve to the LaserWeb software and to turning the grayscale performance of the laser that I like to advise people of ahead of time. 


---
**Kelly Burns** *April 28, 2017 02:59*

I'm unclear of the value of bringing SVGs into Corel.  Usually Corel draw is the source and then you use laserdraw to get the file output to cutter.   What is the source of the SVGs?  Corel definitely has issues with its SVG processing.  I use x7 and it has the same.  



If you want to make it a little easier and make it 1-step, then look at the Corel Draw Macros.  You could make a macro that lets you select the file and the. Once imported do the 133% resizing. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2017 03:59*

**+Kelly Burns** The value of being able to import the SVGs into CorelDraw is that a lot of us don't use CorelDraw for drawing/editing or have a preferred software for doing our drawing (e.g. for me it was Adobe Illustrator).



I found that with CD12, I couldn't even import SVGs at all without them glitching out like crazy & having things all over the place. But that may be a result of creating them in Adobe Illustrator, or even some of the SVG settings I had on the AI output.



Meanwhile, I no longer use CorelDraw/CorelLaser & am using LaserWeb running on a smoothieboard. I highly recommend this as a long term goal for anyone with the K40 as it just simplifies everything & is great to be able to work with the developers & community to contribute to the software's features (with suggestions, feedback, testing, etc). Win win situation for everyone there.


---
**Chris Hurley** *April 28, 2017 04:13*

I have friends who use ai and it seems like the only thing that will import is svg (if I need to modify the work). I'm the hardware guy for a circle of friends making wargaming aides and terrain. 


---
**Chris Hurley** *April 28, 2017 04:14*

I'm going to start working toward getting that new board. 


---
**Kelly Burns** *April 28, 2017 04:17*

**+Yuusuf Sallahuddin** sure.  I'm using LaserWeb as well.  I guess my question is laserdraw world, why even go back to Corel?   Can't you just tak that SVG created in AI, Inkscape or whatever and open in Laserdraw?  


---
**Kelly Burns** *April 28, 2017 04:19*

**+Chris Hurley** there are other options,  but Rays C3D is pretty much a drop in and the easiest way to go.


---
**Chris Hurley** *April 28, 2017 04:24*

Kelly it might just me being new to this but I find that Corel is the easier way to control the machine with the pugin. But I will be looking more deeper into it now. 


---
**Kelly Burns** *April 28, 2017 04:34*

I'm not familiar with laser draw processing.  I like Corel and I'm very good at it.  Been using for 28+ years.  So I design everything in Corel Daw.  I'm still struggling with a solid workflow with the Coreldraw and LaserWeb4 setup.for similar reasons as you.  Corel's SVG processing is terrible and right now I must use It Inkscape that clean up.



I wish I could be more help for you.  What software are you doing layout and design in? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2017 07:07*

**+Kelly Burns** By the sounds of it, **+Chris Hurley** is actually using the CorelLaser plugin for CorelDraw to send his data to the laser. I got confused when I first started K40ing & thought that LaserDRW was the same thing, but it's the standalone app that has a graphics editor & the plugin builtin together.


---
**Steve Clark** *April 28, 2017 07:32*

I draw in EZ cam  and convert it to a  DXF file sent to Corel which is used to convert the file into emf down loaded into laserdrw. in laserDRW I use the Laser workpiece border set at the same sizes and adjust from there. Then you ready to go.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2017 08:42*

Totally agree with **+Peter van der Walt**, that seems overly complicated & time consuming **+Steve Clark**. I thought it was bad enough drawing in Illustrator & saving as AI9 format then importing into CorelDraw to send via the plugin to the laser.


---
**Steve Clark** *April 28, 2017 15:50*

Grin - I will do that...it looks that I'm going that route soon anyway. The first time finding the "work around" was very frustrating, now that I have the path way it's only minutes. Still, it should not even be necessary to have to do something like this...  as you say.


---
**Steve Clark** *April 29, 2017 03:10*

OK...Today I down loaded a program and the small hole circles were three lobed not round???  Maybe it's time to try something else...



Fortunately they didn't effect the parts as they were just a few 'proof of concept' parts and I going to redraw them anyway. But that was odd...



I'll see tomorrow when I redraw if I get the same thing.


---
**John Hogg** *May 18, 2017 10:32*

I use Inkscape for my designing and editing then export as plain.svg.  Found this great guide that helped get rid of most issues I encountered [atxhackerspace.org - Using Inkscape with the Laser Cutter - ATXHackerspace](http://atxhackerspace.org/wiki/Using_Inkscape_with_the_Laser_Cutter)


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/3hnfm9BWpFF) &mdash; content and formatting may not be reliable*
