---
layout: post
title: "Almost done , thx to a k40 and a small cnc"
date: November 12, 2017 18:31
category: "Object produced with laser"
author: "Mike Gallo"
---
Almost done , thx to a k40 and a small cnc.

Btw a will be for sale.  Size:  35"  long by 20" tall.



![images/aac4b87fc69ed46ed57d91280c394c0e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aac4b87fc69ed46ed57d91280c394c0e.jpeg)
![images/f5ba8f083556ba9abb746cc579664000.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f5ba8f083556ba9abb746cc579664000.jpeg)
![images/41896b7043fde4d1b71f8ce59a96b899.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41896b7043fde4d1b71f8ce59a96b899.jpeg)

**"Mike Gallo"**

---
---
**greg greene** *November 12, 2017 19:21*

beautiful plans for sale too?


---
**Mike Gallo** *November 12, 2017 19:28*

No plans, they are copyrighted by somebody else. Sorry.


---
**Ned Hill** *November 13, 2017 04:52*

Very nice


---
**Don Kleinschnitz Jr.** *November 13, 2017 15:36*

Whoa ...epic work!


---
**greg greene** *November 13, 2017 15:41*

cAN YOU LET US KNOW WHERE TO BUY THE PLANS FROM?


---
*Imported from [Google+](https://plus.google.com/108921378497443668729/posts/HKs9bmZoKJ2) &mdash; content and formatting may not be reliable*
