---
layout: post
title: "Made an adjustable bed based on: Added the angle aluminum to strengthen the expanded aluminum because it was super flimsy"
date: August 15, 2017 14:14
category: "Modification"
author: "Ray Rivera"
---
Made an adjustable bed based on: [https://www.thingiverse.com/thing:1906231](https://www.thingiverse.com/thing:1906231) Added the angle aluminum to strengthen the expanded aluminum because it was super flimsy. However now the belt skips teeth because of the extra weight. Any recommendations on how I can tighten up the belt?



![images/88a3d8420061838069df765fac761ebb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/88a3d8420061838069df765fac761ebb.jpeg)
![images/1673e1bec2f79be7ad228bd40fa308df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1673e1bec2f79be7ad228bd40fa308df.jpeg)
![images/848b4f04261e95383295ad5613f6233d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/848b4f04261e95383295ad5613f6233d.jpeg)

**"Ray Rivera"**

---
---
**Don Kleinschnitz Jr.** *August 15, 2017 15:18*

Add an adjustable idler pulley on back side of belt to take up tension toward middle of assy. A spring tension-er on the idler pulley might also  work without an adjustment. 

Cogged belts need a % wrap when under load.


---
**HalfNormal** *August 15, 2017 15:56*

I have a similar setup and there is not an easy fix that does not involve compromising your work area or having the laser hit the belting. My "fix" is to use a long screw driver to add tension on the belt when adjusting the height. Not perfect but does not interfere with operation.


---
**Don Kleinschnitz Jr.** *August 15, 2017 17:41*

I was thinking of something like this:



![images/494e242234533501dde3dd44ffe978bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/494e242234533501dde3dd44ffe978bc.jpeg)


---
**HalfNormal** *August 15, 2017 17:45*

**+Don Kleinschnitz** The adjusting belt is just under the working platform. Any hardware will interfere with the working area. Also unless you have a solid work surface, you run the risk of damaging anything under the laser.


---
**Don Kleinschnitz Jr.** *August 15, 2017 17:49*

**+HalfNormal** I figured anything that far out of focus would not get any damage :)


---
**HalfNormal** *August 15, 2017 17:49*

All depends on what current you're using!


---
**Arion McCartney** *August 15, 2017 22:15*

Check out my remixed design. It is pretty sturdy and I have not burnt the belt at all. I have cut 1/4" poplar over it many times and it is still good. In my opinion, the belt needs to have tension or you will probably experience skipped teeth.  I used square steel tube for the frame and it is nice and rigid.  [https://www.thingiverse.com/thing:2299337](https://www.thingiverse.com/thing:2299337)


---
**HalfNormal** *August 15, 2017 22:18*

**+Arion McCartney**​ your belt is at the bottom. The original one has the belt at the top and follows the platform.


---
**Ray Rivera** *August 15, 2017 22:19*

Thanks for the input guys, I will try the springs and idler and if that doesn't work I'll try your remix Arion. 


---
**Ray Rivera** *August 17, 2017 02:04*

So I did some close observation and what was happening is the angle aluminum was putting pressure on the belt causing it to skip. So I cut out 8x spacers (2 for each corner) cut out to the diameter of the belt hubs and the inner holes cut to the diameter of the threaded rod for 6x of the spacers and 2x spacers at the diameter of the knob. All it all it works great. I put some Teflon lube on the threaded rod and this guy works nice and smoothly.

![images/e00007fa78337eecf5cff34c67b8e12f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e00007fa78337eecf5cff34c67b8e12f.jpeg)


---
**Ray Rivera** *August 17, 2017 02:04*

![images/6cc7c538ae81528216bf98206ca9a71a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6cc7c538ae81528216bf98206ca9a71a.jpeg)


---
**Ray Rivera** *August 17, 2017 02:05*

![images/c6bdf5d7e315609b89bf42f881e9f57e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c6bdf5d7e315609b89bf42f881e9f57e.jpeg)


---
**Ray Rivera** *August 17, 2017 02:05*

![images/e14f35690eb6632459d44b15921abd55.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e14f35690eb6632459d44b15921abd55.jpeg)


---
**Don Kleinschnitz Jr.** *August 17, 2017 12:45*

Sometimes its the simple stuff?


---
**Ray Rivera** *August 17, 2017 13:39*

Don, you are correct there. The only issue I have is that I lost a few mm of work space inside the bed due to the threaded rod poking up. The difference isn't much. The usable space went from 325mm x 225mm to 323mm x 220mm. I still have enough rod to try and cut them down by an inch or so but at this time i don't think I need to try that out. 


---
*Imported from [Google+](https://plus.google.com/117639914879018784950/posts/2ykaRXCHETM) &mdash; content and formatting may not be reliable*
