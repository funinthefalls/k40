---
layout: post
title: "Few questions is there a adapter to attach the fan to the k40 that creates a better seal and allows access to the laser tube"
date: February 11, 2016 04:31
category: "Discussion"
author: "3D Laser"
---
Few questions is there a adapter to attach the fan to the k40 that creates a better seal and allows access to the laser tube.  Also I want to cut back the exhaust inside the machine and I was thinking would it be wise to make an adapter that would allow even suction across the whole piece being cut rather than the center 





**"3D Laser"**

---
---
**Phillip Conroy** *February 11, 2016 07:11*

when i upgraded to a 8 inch fan i just used duct tape to  create a better seal on the stock fan housing 


---
**Phillip Conroy** *February 11, 2016 07:13*

The stock fan is 10 watts  off hand my 8 inch is 150watts more than 10 times better,smoke no longer swils around cutting bed and i do not have to wait 5 seconds fir the mdf smoke clears


---
**3D Laser** *February 11, 2016 11:51*

Where did you get your fan


---
**Jim Hatch** *February 11, 2016 13:31*

I sealed mine with 3/4"x3/8" rubber stick on insulation. It creates a snug fit in the slot the fan housing fits into and up the side. I did a horizontal strip across the top that seals the area where the machine starts to bend towards the front that would otherwise create a gap. I think the stuff was about $6 at Home Depot for a roll about 10' long.


---
**Scott Marshall** *February 11, 2016 20:42*

I'd recommend just removing the duct. That's what I did, and with that done, the stock fan is workable. I have another big exhaust system (Woodshop style) but haven't yet switched to big exhaust, as the stock fan is working OK for wood, and I'm lazy. Working that way though, maybe in the next few days... I'm working on it anyway. 



The duct is held on with 4 screws around the outlet, just remove them (I did mine while the rails were already out, so you may have to remove them to swing it out) If you don't like the way it works, it's totally reversable. It's easier to cut with it out anyway, so give it a try. Worked for me.


---
**3D Laser** *February 11, 2016 23:24*

**+Scott Marshall** where would you move it to and do you have a picture


---
**Phillip Conroy** *February 12, 2016 05:21*

my monster 8 inch 150 watt, Flow rate: 25 m³/minhttp://[www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)

with [http://www.ebay.com.au/itm/191793245738?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/191793245738?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)

duct reducers

[http://www.ebay.com.au/itm/191791173463?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/191791173463?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Phillip Conroy** *February 12, 2016 05:26*

the duct usedhttp://[www.bunnings.com.au/blauberg-100mm-x-3m-aluminium-duct-_p4420305](http://www.bunnings.com.au/blauberg-100mm-x-3m-aluminium-duct-_p4420305)

i have found that this fan clears the mdf smoke very well,now do not have to wait 5 seconds to clear smoke.- not cheap but would never go back


---
**Phillip Conroy** *February 12, 2016 05:34*

my first fan upgrade was  [http://www.ebay.com.au/itm/Exhaust-Fan-Inline-Hot-or-Cold-Air-Transfer-or-Extraction-10cm-Mixflow-Ventair-/172027596395?hash=item280da4ce6b:g:mEUAAOSwf-VWbCPC](http://www.ebay.com.au/itm/Exhaust-Fan-Inline-Hot-or-Cold-Air-Transfer-or-Extraction-10cm-Mixflow-Ventair-/172027596395?hash=item280da4ce6b:g:mEUAAOSwf-VWbCPC)

at 25 watts  and Air Delivery (High/Low): 190/150m3/h and did not suck much more than the stock fan

-note the h in 190/150m3/hour ver my new fan at Flow rate: 25 m³/min 190m3 divided by 60 =3m3/min even with the reducers and the 1metre suction hose my 8 inch 25 m³/minute is about 8 times more powerfull


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/J6sjrAT2Sq1) &mdash; content and formatting may not be reliable*
