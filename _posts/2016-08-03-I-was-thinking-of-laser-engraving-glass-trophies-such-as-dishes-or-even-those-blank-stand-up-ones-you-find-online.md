---
layout: post
title: "I was thinking of laser engraving glass trophies, such as dishes or even those blank stand up ones you find online"
date: August 03, 2016 13:01
category: "Object produced with laser"
author: "Bob Damato"
---
I was thinking of laser engraving glass trophies, such as dishes or even those blank stand up ones you find online. In my experiments with lasering glass, I have found it to be an utter failure :)  The glass pretty much explodes every time. Am I better off with the acrylic type maybe? Are there preferred settings for glass?  If anyones done glass, Id love to hear their tips.

Bob





**"Bob Damato"**

---
---
**Scott Marshall** *August 03, 2016 13:10*

Try putting masking tape over the glass, and back the power way down.



It works great, but you DO have to play and find the sweet spot. As with a lot of things you laser, less is more. Start VERY low on power and work up. It won't cut deep, but will look almost like sandblasted glass.



I've heard, (but not run across any) that's there are some types of glass that can't be engraved. Maybe you got unlucky and found one on your 1st go?


---
**Anthony Bolgar** *August 03, 2016 13:21*

Others have had success lasering through wet paper towels as well.


---
**Bob Damato** *August 03, 2016 14:40*

I found this which seems to go along with what you are saying

. Thanks guys!


{% include youtubePlayer.html id="1gwHMP9QFVc" %}
[https://www.youtube.com/watch?v=1gwHMP9QFVc](https://www.youtube.com/watch?v=1gwHMP9QFVc)


---
**Jim Hatch** *August 03, 2016 17:16*

I laser glass using either a wet paper towel on top that gets lasered through - it keeps the glass from overheating and fracturing (wet newspaper also works) or through a film of liquid dish soap.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/cTFEKvhwA4E) &mdash; content and formatting may not be reliable*
