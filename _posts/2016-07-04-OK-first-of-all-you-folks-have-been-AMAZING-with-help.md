---
layout: post
title: "OK, first of all, you folks have been AMAZING with help"
date: July 04, 2016 00:53
category: "Discussion"
author: "Terry Taylor"
---
OK, first of all, you folks have been AMAZING with help. I have successfully been able to engrave and cut with LaserDraw (BTW, just as the K40 came out of the box, i have not adjusted a thing!). I tried to use CorelLaser, did the config, it shows the 6c6879 laser, just like in LaserDraw but, when I open the engrave dialog, UNLIKE LaserDraw, it shows the laser and manufacturer as a bunch of Chinese characters and there are no available devices to choose from. CorelLaser capture and LaserDraw capture attached. And, by the way, it obviously does not engrave or cut (and I have the DPI set to 1000)



![images/083cd810e4a328e1bdf5fa19ee115e66.png](https://gitlab.com/funinthefalls/k40/raw/master/images/083cd810e4a328e1bdf5fa19ee115e66.png)
![images/ea43915595c8720ae3f8c6372be3544b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ea43915595c8720ae3f8c6372be3544b.png)

**"Terry Taylor"**

---
---
**greg greene** *July 04, 2016 01:13*

Same here - I have not been able to use the corel either - just laserDRW


---
**Anthony Bolgar** *July 04, 2016 01:17*

You need to enter the info that is on your controller board inside the machine. You will find the model number and serial number there. Enter them into the CorelDraw plugin settings dialog, and then they will appear in the window you did the screen cap of. Not sure exactly how to get to the settings anymore, haven't used the stock controller for about a year now. I am sure someone else on here still has the info on how to do it exactly.


---
**Terry Taylor** *July 04, 2016 01:27*

The settings window is correct, but it does not show up in the print dialog box. I found the video below, that SEEMS to have fixed the problem.




{% include youtubePlayer.html id="4L7t1su-xu0" %}
[https://www.youtube.com/watch?v=4L7t1su-xu0](https://www.youtube.com/watch?v=4L7t1su-xu0)


---
**Anthony Bolgar** *July 04, 2016 01:29*

Glad your issue is resolved.


---
**andy creighton** *July 09, 2016 14:47*

Mine had two versions of each software.   One English and one chinese,  but both had to be installed to figure out which was which. ﻿



Fwiw, I have been really happy with the software.  It works great with no alterations at all for engraving bmp and cutting dwg files 


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/XBgdSnyMdtP) &mdash; content and formatting may not be reliable*
