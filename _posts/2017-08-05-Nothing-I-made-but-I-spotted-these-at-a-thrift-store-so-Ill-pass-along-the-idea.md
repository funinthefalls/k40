---
layout: post
title: "Nothing I made, but I spotted these at a thrift store so I'll pass along the idea"
date: August 05, 2017 17:50
category: "Object produced with laser"
author: "Nate Caine"
---
Nothing I made, but I spotted these at a thrift store so I'll pass along the idea.  

<i>(Obviously these were donated after some sort of fabrication fail.)</i>

  

But you get the idea.  Most are some sort of take on the theme of "building".  



<i>"I love the life we've built together."</i>

<i>"Thank you for helping us build our lives."</i>



One is a gift from Jordan to his dad.  Something he'll see every time he uses his hammer.



For a promo, they could have incorporated some logos.  Or graphic patterns, geometrical, etc. beyond just plain text.﻿

![images/ee0d72b54c141bed62fb60a2b5ff934c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee0d72b54c141bed62fb60a2b5ff934c.jpeg)



**"Nate Caine"**

---
---
**Alex Krause** *August 06, 2017 00:39*

I made some for Father's day this year... I applied masking tape first, engraved through it, spayed a coat of clear coat to seal the grain so the black spray paint wouldn't bleed into it



![images/ec49807e82c8d5b324d228a213adaf16.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec49807e82c8d5b324d228a213adaf16.jpeg)


---
*Imported from [Google+](https://plus.google.com/107379565262839168986/posts/Ry5BY51sHeV) &mdash; content and formatting may not be reliable*
