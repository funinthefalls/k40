---
layout: post
title: "Please help! I know it has to be something stupid"
date: September 10, 2016 20:46
category: "Original software and hardware issues"
author: "VetGifts By G"
---
Please help! I know it has to be something stupid. Trying to cut a single rectangle, but the machine says finished in 1s, health correct data 0% and nothing happens. My rectangle is black(C,M,Y at 0,K is 100) and set to hairline. I restarted both the K40 and my PC twice with the same result. Tried cutting other shapes and it works great. What is my issue?





**"VetGifts By G"**

---
---
**Ariel Yahni (UniKpty)** *September 10, 2016 21:28*

Making the rectangle in another app yields the same result? 


---
**VetGifts By G** *September 10, 2016 22:36*

I have only messed with CorelDraw so far. I changed the color to RGB and my settings for cut data to WMF and that seemed like the problem. It cuts now, but it goes over it twice, while my repeat setting is 1


---
**Jim Hatch** *September 10, 2016 22:40*

Make sure you have the line specified as a stroke weight of .001mm otherwise it may cut multiple times to get the "thickness" it sees.




---
**Ariel Yahni (UniKpty)** *September 10, 2016 22:40*

Dont choose hairline, select something like 0.025mm


---
**VetGifts By G** *September 11, 2016 00:40*

Will definitely try that, I remember watching a video somewhere talking about for cutting to use hairline. I've cut files made not by me that were hairline, so I'm thinking I'm doing something else wrong. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 10:32*

**+Miro G** It's definitely the hairline setting that is the issue. Anything above hairline would try cut both sides of the line. I always used 0.1mm line thickness without the double cut issue, but some other software workflows that people have used require lower again, so setting to 0.001mm like Jim suggests is probably a good habit to get into.


---
**Jim Hatch** *September 11, 2016 13:35*

I've had good & bad luck with .1, .01 and hairline depending on my software. Never had the .001mm fail so I just use that. I set my software (AI & Corel) with that as the default line width. I expect .01mm should work but .1 is my laser kerf and it seems to cause double cuts most.


---
**Norman Kirby** *September 11, 2016 18:44*

i use hairline all the time and never had an issue with cutting or engraving, what I did discover that if in the settings you have the "selected only" chosen and you don't select everything after you press start it immediately come up with finished, you need to alter the settings to current page that then will defo help




---
*Imported from [Google+](https://plus.google.com/113361677380464824789/posts/MDAakNrEREr) &mdash; content and formatting may not be reliable*
