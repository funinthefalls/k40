---
layout: post
title: "Installing usb drivers I was getting an error when trying to install the filter driver through Zadig"
date: July 18, 2017 00:14
category: "Software"
author: "HalfNormal"
---
#K40whisperer



Installing usb drivers



I was getting an error when trying to install the filter driver through Zadig. The way to get around this issue is to download libusb-win32 from [https://sourceforge.net/projects/libusb-win32/](https://sourceforge.net/projects/libusb-win32/) and use the filter driver installation included with files.





**"HalfNormal"**

---
---
**Scorch Works** *July 18, 2017 12:56*

Good tip.  I also had one person tell me that they were able to install the filter driver on windows 7 by using the Zadig version for Windows XP.


---
**HalfNormal** *July 18, 2017 12:59*

I tried an earlier version too but only one version back and it also was giving an error so that is when I used the libusb-win32 version.


---
**Frank Jones** *August 09, 2017 12:52*

Will this help me with usb error I keep getting something about a "backend"  I really want to get the k40 whisperer program to work. 


---
**Scorch Works** *August 09, 2017 13:19*

The "backend" error indicates that the libUSB driver is not installed.  So you need to install the driver either using the directions on the K40 Whisperer web page or you can use the installer linked here.  I would recommend using the directions linked on the K40 Whisperer web page unless you know what you are doing.


---
**Scorch Works** *August 09, 2017 13:36*

Here is a link to the driver setup instructions.



[scorchworks.com - K40 Whisperer USB Driver Setup](http://www.scorchworks.com/K40whisperer/k40w_setup.html#windowsInstallUSB)


---
**Frank Jones** *August 09, 2017 13:44*

I will try that tonight when I get home, is it normal for it to work correctly with drwlaser just not with K40 whisperer? I engraved and cut it out the other day but I made the file in drwlaser and used it to engrave and cut. 


---
**Frank Jones** *August 09, 2017 13:45*

Thank you for all your help


---
**Scorch Works** *August 09, 2017 13:56*

**+Frank Jones** LaserDRW uses a different driver.  So you need to install the libUSB driver for K40 Whisperer to work.  There is some discussion about that in the instructions I linked.


---
**Frank Jones** *August 09, 2017 13:58*

Thanks again I did not realize it used a different driver, awesome I was hoping it would be something pretty easy. 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/CvL83BhayWd) &mdash; content and formatting may not be reliable*
