---
layout: post
title: "Another metal engraving product. I have not tried it but it looks to be cheaper than the competition"
date: December 21, 2017 03:16
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Another metal engraving product.

I have not tried it but it looks to be cheaper than the competition.



[https://enduramark.com/](https://enduramark.com/)





**"HalfNormal"**

---
---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 03:51*

i have used the free samples they gave out when launching. comparable to cermark in every way. works just fine


---
**HalfNormal** *December 21, 2017 17:15*

Thanks for the info. Will have to try it.


---
**Jim Hatch** *December 22, 2017 17:46*

Stainless steel only according to the website but that takes care of a huge set of use cases. I have asked whether the dilution is by weight or volume and am waiting on the response. But I'm ordering some to check it out myself.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/H8jLjxSe9o9) &mdash; content and formatting may not be reliable*
