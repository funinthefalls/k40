---
layout: post
title: "Anybody know a good source of wire frame designs to engrave on acrylic?"
date: November 09, 2017 01:55
category: "Repository and designs"
author: "Anthony Bolgar"
---
Anybody know a good source of wire frame designs to engrave on acrylic?





**"Anthony Bolgar"**

---
---
**Printin Addiction** *November 09, 2017 02:15*

Ive been searching also, so I can make one of those 3D looking acrylic lights. There are hard to find. Blender does have a 3D wire frame mode, I just havent had the time to play around with it.


---
**Anthony Bolgar** *November 09, 2017 02:37*

Thanks for the tip about blender, I'll look into that.


---
**Ariel Yahni (UniKpty)** *November 09, 2017 02:42*

The best I have used is rhino, but if you learn blender it will give you more control


---
**Printin Addiction** *November 09, 2017 03:15*

There are a lot of blender tutorials, and it's free which is real nice. Basically, it is a modifier called wire frame that you apply to a mesh. There is a bit of a learning curve, but you will be able to do a lot once you get the hang of it.


---
**Michael Audette** *November 09, 2017 11:32*

**+Anthony Bolgar** - stumbled on these last week on an facebook board:  [buildowncnc.blogspot.com.eg - 3d led illusion for free](http://buildowncnc.blogspot.com.eg/2017/11/3d-led-illusion-for-free.html)  There's other's there too if you have time to dig.


---
**Printin Addiction** *November 09, 2017 11:49*

**+Michael Audette** Did you try any of these files? I cannot seem to load them.


---
**Printin Addiction** *November 09, 2017 11:59*

I was able to load them in Inkscape, but not Corel. Good find **+Michael Audette**


---
**Michael Audette** *November 09, 2017 12:03*

The facebook group has a files section as well.  Mostly for CNC engraving and there are somewhat regular post of projects and finishing techniques from folks who do this stuff for a living....but there are also relative noobs as well.  Just search for the group named "CNC Tips, Tricks and Project Finishing"




---
**Anthony Bolgar** *November 09, 2017 12:44*

Thank you **+Michael Audette** for the info. Much appreciated.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/EDuwSux9J1j) &mdash; content and formatting may not be reliable*
