---
layout: post
title: "I just tried updating the firmware on my smoothieboard and now the LCD doesn't display"
date: October 01, 2016 23:38
category: "Smoothieboard Modification"
author: "Eric Rihm"
---
I just tried updating the firmware on my smoothieboard and now the LCD doesn't display. It still receives power from the smoothieboard but I don't get any text. I've tried swapping the aux connectors and that didn't fix it either. I'm not sure if it was from flasing the firmware or I messed something up in the process. Anyone have any ideas?  I'm wondering if I burned the LCD out with it just hanging and it shorted touching something.





**"Eric Rihm"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 02, 2016 00:54*

Did you change your config file at the same time? I don't know much about LCDs with Smoothie, but I did notice in the config file there is a section dedicated to the LCD output/commands/etc.


---
**Vince Lee** *October 02, 2016 17:20*

Is it a genuine smoothieboard?  I had the same problem with my knockoff board and spent days messing with the configuration file to no avail.  Finally I got a link to the original firmware from the seller and it came right back up when I flashed it back.


---
**Eric Rihm** *October 03, 2016 04:28*

It is a knockoff, thank you so much I'm gonna give that a try. Have a good feeling that was it.


---
**Jon Bruno** *October 03, 2016 14:06*

Which board are you guys working with?

This may be helpful for someone in the future so please do elaborate.


---
**Eric Rihm** *October 04, 2016 02:02*

I'm using the AZSMZ Mini, I'm having trouble tracking down the stock firmware file. I flashed one of the files off the reprap wiki and it's still not working, need to find the original the links dead on the wiki.


---
**Vince Lee** *October 04, 2016 18:23*

Aha! You have the same board I do and I tried the same firmware images and ran into the exact same thing.  The correct firmware file is hosted on the reprap site but isn't linked to from the AZSMZ page for some reason.  Here is the url for correct file that I got from the seller:  [http://www.reprap.org/mediawiki/images/7/76/AZSMZ_FW_Ver2.1.zip](http://www.reprap.org/mediawiki/images/7/76/AZSMZ_FW_Ver2.1.zip)

[reprap.org - www.reprap.org/mediawiki/images/7/76/AZSMZ_FW_Ver2.1.zip](http://www.reprap.org/mediawiki/images/7/76/AZSMZ_FW_Ver2.1.zip)


---
**Jon Bruno** *October 04, 2016 20:12*

I knew someone would find this useful.


---
**Vince Lee** *October 04, 2016 21:56*

I dealt with this problem and fixed it about a year ago.  I still don't know what the difference is in the firmware, though.  The manufacturer firmware works fine, but it would be nice to be able get any upgrades that might be incorporated into future official releases.


---
**Jon Bruno** *October 06, 2016 03:23*

The problem is that some knock off boards require little tweaks to the f/w to make it work properly. Without knowing what they are you're going to be hard pressed to port the official releases to work properly .


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/gbLuLQtN9Lg) &mdash; content and formatting may not be reliable*
