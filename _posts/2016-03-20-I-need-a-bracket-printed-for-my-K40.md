---
layout: post
title: "I need a bracket printed for my K40"
date: March 20, 2016 23:20
category: "Modification"
author: "Tony Sobczak"
---
I need a bracket printed for my K40.  I bought the air assisted laser head from lightobject but the bracket that is currently on the K40 has a 14mm hole for the head and the new laser head needs a 22mm hole. I have what I think is a correct 3d drawing for the bracket and am wondering if anyone out here would do it.  Bracket will be approximately 48mm x 88mm x3mm. Will pay for it and postage.  Anyone interested??  Please PM me.  



Thanks  

Tony





**"Tony Sobczak"**

---
---
**Scott Marshall** *March 21, 2016 00:01*

If it's just a flat 3mm (1/8") plate with holes/slots, I'd be able to machine you one from aluminum pretty simply. 

My entire carriage was trashed when my laser arrived, so I machined the whole thing from aluminum. 



If it's what I'm thinking, you're looking for a replacement for the top plate with holes to fit the new air assist head. The screw holes would be slotted with an endmill, and the hole bored to any size desired with a boring head, The outer profile could be cut to spec, or left oversized if you want to cut it yourself.



If you send the drawing/sketch I could tell you what the cost to machine would be. shouldn't be too much if it's not very complex. 










---
**Brian Bland** *March 21, 2016 00:06*

Sent you a pm.  You shouldn't need the bracket.


---
**HalfNormal** *March 21, 2016 00:07*

The new head will fit the existing bracket. If you look at the picture on the website, you will see that it screws apart just  above the existing threads. I had to use a rag to hold onto the threads in order to screw the top apart. I was just as mystified as you are until I took a harder look at the online pictures.


---
**Tony Sobczak** *March 21, 2016 05:54*

**+Brian Bland**​ and **+HalfNormal**​ thank you,  thank you, thank you. you are correct. I don't know how I missed that and it took some effort to get it apart but it does work. **+Scott Marshall**​ thanks so very much for the offer but it looks like I won't need the bracket now.


---
**Scott Marshall** *March 21, 2016 07:01*

That's good news.

 I used an air assist unit that DID need a larger hole, and the stock plate didn't have room to just open it up. I had to make a  new one anyhow as well as a complete carriage because mine was totaled in shipping and had a pringles sort of thing going...

If you DO need parts in the future, drop me a line,

I have a modest hobby machine shop and don't mind helping out fellow tech experimenters (or whatever they call us).



Scott


---
**Tony Sobczak** *March 21, 2016 07:03*

I'll keep you on mind **+Scott Marshall**​. I have some idea what I'd like to do. Thanks again. 


---
**Tom Spaulding** *March 21, 2016 16:20*

Don't feel bad, I did the exact same thing. Unscrewing the threaded section of the head didn't occur to me and I couldn't get the second picture of the disassembled parts to show on my PC.


---
**Tony Sobczak** *March 21, 2016 16:31*

Customer support wasn't any help, said to just return it.


---
**Tony Sobczak** *March 21, 2016 18:32*

**+Tom Spaulding**​ how did you route the hose for the air assist? Having trouble keeping it off the deck. 


---
**Tom Spaulding** *March 21, 2016 20:24*

I used coiled air hose for mine. So it stays out of the way pretty well.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/5cikBPFdKFd) &mdash; content and formatting may not be reliable*
