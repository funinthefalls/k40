---
layout: post
title: "Don Kleinschnitz Can't seem to find the other post where you asked about power at the surface, so I will report it here"
date: April 20, 2017 12:41
category: "Discussion"
author: "Anthony Bolgar"
---
**+Don Kleinschnitz** Can't seem to find the other post where you asked about power at the surface, so I will report it here. I measured on 2 different K40's, 5 times on each at various places on the cutting area. Worst place was obviously furthest away from the origin of beam, the bottom right corner. Average power worked out to be 26.55W at the bottom right corner, surface of work piece. Hope this info is helpful BTW, power at 2nd mirror close to origin was 40.2W average. So quite a significant decrease. 





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *April 20, 2017 12:49*

So assuming that at the surface the average power is about 25W a 15W laser diode is about 60% of a standard K40. I assume your values above are for full power. 



On a separate note is your measurements suggesting that 14W was lost in the objective lens assy. That would be lost in the #3 mirror and the objective lens. Be interesting to take out the lens and see if all this loss is in the objective lens. That would verify the value of spending some money on the lens.


---
**Jorge Robles** *April 20, 2017 12:51*

I counted 10% each mirror but I got short. Better mirrors?


---
**Anthony Bolgar** *April 20, 2017 12:57*

I am sure that a higher quality lens would transmit more power to the surface. I think it was **+Scott Thorne** that upgraded his 50W with a USA made lens and could cut crazy thick acrylic in one pass. (I think it was 3/4 thick) I have replaced all my mirrors with MO mirrors, lenses are from Lightobjects site. (Still asian made)


---
**Anthony Bolgar** *April 20, 2017 12:58*

I should also add that I spent days aligning my mirrors, they are as about aligned as you could ever achive with the low quality optics in the K40


---
**Cesar Tolentino** *April 20, 2017 15:23*

Please let me know if this make sense. So when I'm done with my CNC, I want to do a diy co2 laser using k40 as parts. But the difference is that the bed will move like a 3d printer? So that the distance of the lenses/mirror are fix? This is because of the reasons **+Anthony Bolgar**​ ​mentioned.


---
**ALFAHOBBIES** *April 20, 2017 15:51*

Almost sounds like it would be an interesting project to make a moving bed instead of the head moving. Then the laser tube could be vertical above the bed with only a good USA lens in the path of the beam. 



Darn you guys giving me more crazy ideas for another project.


---
**Cesar Tolentino** *April 20, 2017 15:56*

Actually it's a step up idea from blacktooth of buildyourcnc where they put their laser tube along the x axis. So the only mirror that moves is the last one and the rest is fixed.


---
**ALFAHOBBIES** *April 20, 2017 16:15*

I see. The vertical laser would make for a larger machine but may be lower cost to power ratio with out the mirrors. At least I understand the Blacktooth laser design and why you would want to move the tube to get more power. Interesting.


---
**Anthony Bolgar** *April 20, 2017 16:48*

Would there be any issues with cooling the tube if it was vertical?


---
**Cesar Tolentino** *April 20, 2017 16:50*

But it is (semi) closed loop? The pressure on the pump will be based on the difference between the input and the output of the hose, which will be in the bucket?


---
**Anthony Bolgar** *April 20, 2017 16:59*

Pump must be capable of the initial priming head height.




---
**Cesar Tolentino** *April 20, 2017 17:00*

Very true....


---
**Adrian Godwin** *April 21, 2017 10:17*

With some cuts, you'll end with pieces detached from the main sheet of material before the job is completed. If the bed is moving around these may get disturbed. You might be able to avoid this by carefully choosing your cut order.


---
**Cesar Tolentino** *April 21, 2017 15:24*

Also very true.


---
**ALFAHOBBIES** *April 21, 2017 18:23*

Also if parts fell through on an open bed design they could be burned as the beam hits them while cutting. It would be more complicated than it first appears.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/7KokfAu6NJi) &mdash; content and formatting may not be reliable*
