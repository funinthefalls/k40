---
layout: post
title: "Did somebody call for a Doctor?"
date: September 09, 2016 02:40
category: "Object produced with laser"
author: "Alex Krause"
---
Did somebody call for a Doctor? #Laserweb #yearofthelaser #smoothie

![images/55bbd5668433cb623aff8861b3747b7e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55bbd5668433cb623aff8861b3747b7e.jpeg)



**"Alex Krause"**

---
---
**Anthony Bolgar** *September 09, 2016 02:45*

Nice!


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/HUkMRqZVv6r) &mdash; content and formatting may not be reliable*
