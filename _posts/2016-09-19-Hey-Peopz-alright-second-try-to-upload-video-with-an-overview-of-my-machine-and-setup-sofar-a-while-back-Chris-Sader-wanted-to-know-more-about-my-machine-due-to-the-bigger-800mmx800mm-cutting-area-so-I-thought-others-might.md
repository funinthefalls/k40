---
layout: post
title: "Hey Peopz, alright second try to upload video with an overview of my machine and setup sofar, a while back Chris Sader wanted to know more about my machine due to the bigger 800mmx800mm cutting area, so I thought others might"
date: September 19, 2016 20:14
category: "Hardware and Laser settings"
author: "Gunnar Stefansson"
---
Hey Peopz, alright second try to upload video with an overview of my machine and setup sofar, a while back **+Chris Sader** wanted to know more about my machine due to the bigger 800mmx800mm cutting area, so I thought others might be interested aswell as this is not a K40 but a complete DIY :) any questions or suggestions are appreciated ;) tried to keep it short :D


**Video content missing for image https://lh3.googleusercontent.com/-D6cfAPIU8Bk/V-BHDMOLTTI/AAAAAAAATAU/Qju-ob9EI8Qw_g8lvmm-2CyVaiJGpdihQCJoC/s0/Sequence%252B01.mp4.gif**
![images/6b5c40258ad299a5f98255db2b0d032f.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/6b5c40258ad299a5f98255db2b0d032f.gif)



**"Gunnar Stefansson"**

---
---
**Anthony Bolgar** *September 19, 2016 20:21*

Well short it was......just a black square for me.


---
**Gunnar Stefansson** *September 19, 2016 20:46*

**+Anthony Bolgar** should work now, takes some time to process, pretty annoying, G+ should release the post when video was ready! not while!


---
**Morten Førster** *September 19, 2016 20:58*

Looks great!  You have done a fine job of it.  I've had the pleasure of following the project from the start,  and it has come soo fare since version 1.


---
**Alex Krause** *September 20, 2016 02:02*

Beautiful design! Is that openbuilds vslot rail?


---
**Gunnar Stefansson** *September 20, 2016 08:09*

Thx **+Alex Krause** and no they are Square rails Y-Axis 20mm from Rexroth, and 15mm on the X-Axis. A little pricy, but worth every penny! And I got them from a good place. ([www.cnc-discount.de](http://www.cnc-discount.de)) Good and cheap


---
**Joe Spanier** *September 20, 2016 18:18*

Are all 4 of those z motors on the same driver? How are they wired up? Are you running any sort of linear rail with them or just floating on the z screws?




---
**Gunnar Stefansson** *September 20, 2016 18:23*

Yes **+Joe Spanier** all 4 z motors are just all connected same way to one stepper driver, thats also why you hear that High pitch sound. I bought 4 small rails as well but when I put the 4 leadscrews on, it was sturdy as a rock, so didn't want to waste the rails for it, gonna use them for something in the future, so yes just floating on the leadscrews :)


---
**Joe Spanier** *September 20, 2016 18:26*

Im curious to know how that goes as you use it. It might be perfectly fine. Just curious.


---
**Gunnar Stefansson** *September 20, 2016 18:42*

Well **+Joe Spanier** I have been using it for a couple of months with this setup, and it does work fine still :) so far only downside is the high pitch sound though ;) but have a small solution for that, just need todo it :) and you're not the only one curious about it working/lasting :D


---
**Sebastian Szafran** *September 20, 2016 21:02*

**+Gunnar Stefansson** I like this design and large working area, using aluminum profiles gives far more freedom. I am positively surprised to see 20mm profiles giving sturdy build of that size. I thought initially this is a v-slot based build, but I see the profiled linear rails there, possibly 15 or 20mm for the profiles you use. Don't you have leveling issues with large bed when moving up and down? There is a kind of cross ribs to give mor stiffness, possibly enough for it, as the weight of the laser head is minimal. I like your Arduino based laser safety system and the graphs - awesome look. Thanks for sharing.


---
**Gunnar Stefansson** *September 20, 2016 21:56*

**+Sebastian Szafran** Thanks man, yes you're right 15 and 20mm rails, so they fit perfectly with the 20x20 profiles, I was kinda worried that the 20x20 wouldn't be rigid enough, but they are quite amazing and perfect for this kind of application as you don't cut through stuff like a cnc cutting machine would...

 

Right at this moment I drive my bed all the way down until all steppers stall and I have it leveled!!! yeah I know not really pro'ish but did find out in the weekend that even by doing this I need todo fine adjustments on the bed to get it "Perfectly Level" with the Laser Head, but the plan was todo this with the limitswitches once they get installed and use relay's to cut off each stepper depending on their limitswitch, and when all 4 limitswitches are hit the Arduino will signal the Limitswitch Signal to Mach3 so it knows it's homed... 



The Profile where the laser head is on is a 20x40 profile and is rock sturdy and doesn't sag down in the middle and as you said it doesn't carry any weight to it ;) Thanks for the Comment 


---
**Fadi Kahhaleh** *September 21, 2016 03:21*

Impressive setup. Good luck


---
*Imported from [Google+](https://plus.google.com/100294204120049700616/posts/CinRFysWPMr) &mdash; content and formatting may not be reliable*
