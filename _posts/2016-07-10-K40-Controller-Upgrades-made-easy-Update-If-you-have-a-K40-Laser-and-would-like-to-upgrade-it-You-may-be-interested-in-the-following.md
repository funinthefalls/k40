---
layout: post
title: "K40 Controller Upgrades made easy! (Update) If you have a K40 Laser and would like to upgrade it, You may be interested in the following"
date: July 10, 2016 08:33
category: "Discussion"
author: "Scott Marshall"
---
K40 Controller Upgrades made easy!

(Update)



If you have a K40 Laser and would like to upgrade it, You may be interested in the following.



I'm a retired (forcibly due to health) Engineer who used to run a small industrial controls business.

My health is in and out, but I've found a way to use some of my skills to help out K40 users.

My products are designed as “Plug and Play” whenever possible, so allow people who are not electronics hobbyists or off-duty engineers to upgrade their K40s  EASIER than the guys soldering wires all over. Even if you DO know how to do the upgrades yourself, I think you'll find the reasonable pricing and convenience of the ALL-Tek Systems kits a great way to upgrade.



My initial offerings are the Aftermarket Controller Retrofit kits.



These provide an easy way to add on an aftermarket controller board like like a Smoothieboard or a Ramps system.



This is accomplished with the use of an translator printed circuit board which converts the K40 interconnects and signals to match the signals of all aftermarket controllers, and provides a convenient terminal strip with all the “normal” Laser functions right there for your wires to connect to.



The more sophisticated kits have the proper connectors that will plug right into your Smoothieboard or other aftermarket board..



You can even buy your Smoothieboard from us and get  a complete “module” which includes the Auxiliary Power Supply, The ACR translator board, and the Smoothieboard all wired up, firmware for the K40 loaded, test run on a real K40 and ready to install.



Installation of one of these kits typically requires you drill 2 holes, bolt in the Module, plug in some connectors and fire it up.



My 1st board is the ACR (of course), and allows you to replace any the M2nano controller on ANY LASER that uses a M2nano as it's controller. The ACR board emulates the M2nano, duplicating it's every connection and function. (your Laser never knows it's not still hosting an M2nano)



The ACR board includes a power source switch which allows you to use it with the stock K40 power supply or an auxiliary power supply. The ACR board has voltage regulators and filters to provide clean, precise power to every part of your K40. It also includes fuse protection to help prevent damage should something fail. The ACR has 3 LEDs, one for each power supply, 3.3V, 5V & 24V. This allows for a quick check of the power supplies if there's a problem.



When purchased as a kit the auxiliary power supply  is included*, all wired up with the ACR board.



*if you live where shipping from the USA is expensive, we offer the ACR Kit 'less Power Supply' and we provide sources for it in your area, or from China, often with free shipping. When ordered this way, the Kit is under 1 lb and will usually ship for under $22us



See our ACR “Line Card” for available options. If you want something not listed, so sweat, we'll build whatever you want.



The SWITCHABLE!



As you may expect, The SWITCHABLE is similar to the ACR board, but allows you to KEEP your M2nano Controller, (AND all the projects you have developed for it), add on the aftermarket controller of your choice, (or order it “loaded”) and SWITCH between them simply by powering down, flipping the switch, and powering back on.



The SWITCHABLE as all the features of the ACR, and it has indicator LEDS on all the major control functions to allow for easy troubleshooting in the event of a problem.



For a complete ACR lineup with much more detail and pricing Email me ALLTek594@aol.com and I'll send you  a 3 page Catalog



The ACR Line will begin shipping in the next 2 weeks (assuming there's no problems with the final build up and testing - UPS had my boards for 10 days on a 2 day air shipment, which is why they're as late as they are. Sorry to those of you waiting).



The SWITCHABLE! Boards will be about 2 weeks behind the ACRs.



Photos will be posted tomorrow.



Thanks, Scott





**"Scott Marshall"**

---
---
**Don Kleinschnitz Jr.** *July 10, 2016 12:37*

Sounds interesting will this include shematics and what other documentation?


---
**greg greene** *July 10, 2016 13:42*

This does sound interesting - what are the advantages?  Z axis control? PWM Control? Auto focusing? 


---
**Ned Hill** *July 10, 2016 17:09*

**+greg greene** As I understand it you can need to replace the stock control board to get these advantages such as with a Smoothieboard.  Scott has designed an interface board (ACR) that allows easy replacement of your stock controller.  It's nice that he's also offering to sell you a preconfigured, and tested, interface board with a Smoothieboard controller.


---
**Scott Marshall** *July 10, 2016 21:02*

Yes, Don, FULL documentation, FULL Support. I have written a very thorough manual that not only explains the installation procedure, but the 2nd half explains all about how your laser works, what parts do what, and  the practical physics of how the entire system operates. Not only will your Laser work great, You'll learn how it works and how to keep it runnnig at it's best..



Greg, no real bells and whistles on this version, just what you need to EASILY get your aftermarket board hooked up and working.



To do that, the ACR board contains 2 voltage regulators. 3 RC filters to suppress noise, and 3 logic level translators. I t can be connected as PWM if your HV power supply is up to the task, or stock firing method, which I recommend for most users.



PWM on a K40 is a complicated issue, there's several ways of going about it.



The ACR board has all the necessary hardware to allow the controller to communicate with the K40 PSU and the aftermarket controller board. From there it's up to the Aftermarket controller to provide the pulse width control. Some people use a dc voltage output and tie into the level control while using the fire signal to turn the beam on and off. Another way is to eliminate the Power knob (linear control) completely and allow the controller to PWM the tube at about 90% capacity (you do give up the top 10% or so with that method, but for detailed engraving, power isn't what's needed, control is.



As MOST users won't be doing any of that, I didn't include that circuitry on the ACR board, it would only add cost and go unused by most people. Pwm doesn't gain you much unless you're doing detailed engravings like photos.



I personally like the manual control for most everything I do. That said, I Do plan to offer a PWM kit down the road for those who wish to do photo quality work. It should pair nicely with LaserWebs adaptive engraving software.



Ned, Exactly!


---
**Terry Taylor** *July 10, 2016 22:48*

Scott, did you have a website for ordering this upgrade?


---
**Scott Marshall** *July 10, 2016 23:06*

**+Terry Taylor** I'm been in negotiations with a web designer in the last couple days.I was planning on doing it in my "spare time"

I've since found I'm not very good at it, and I have my hands full just getting a high quality product to market and supporting it well. I hope to have a website up and running soon.



In the meantime:

You can order directly thru ALLTEK594@aol.com with Paypal.



Please don't send any money yet. My policy is not to take your money until the product is ready to ship. The ACR line will be shipping within 2 weeks, hopefully sooner. I've had all sorts of the usual 1st run problems happen, lost in the mail parts, wrong parts etc. so the 1st batch has been delayed a bit.



If you want to hold your place in line, you can pre-order.

Just send an email telling me what you need. No deposit is necessary.



There are 24 ACR boards available in the 1st run and 10 of the Switchable. A good portion of them are spoken for.



The quantity of the 2nd run will be determined by demand. I expect there to be plenty available by fall.


---
**Sunny Koh** *July 11, 2016 08:22*

**+Scott Marshall** Can you let me know the details for the switchable, I will be in the US end of August and can pick up the board then.


---
**Scott Marshall** *July 11, 2016 23:32*

**+Sunny Koh** Send me a message  at ALLTEK594@aol.com

I'll send you the ACR info (a lot of it applies to the Switchable) and as soon as the Switchable Catalog is Finished, I will send it as well.


---
**David Spencer** *September 16, 2016 16:57*

Hey **+Scott Marshall** are the switchable boards ready, is the website up?


---
**Scott Marshall** *September 17, 2016 04:54*

Switchable boards are coming along (testing has been slowed by heath issues and the number of ACR sales. The health thing is not unexpected, and I was blessed with a relatively troublefree period for a while there)



 Website is up ALL-TEKSYSTEMS.com 



ACR series selling well, most of the 26 unit prototype run gone (a few left). I'm working on a variety of projects including ACR Mark II which is an improved ACR with indicators and Switch mode regulation. 



So far we have 20 or so happy customers.  If my health would co-operate, we'd have more, and more products. 



Thanks for the interest!



Scott


---
**paul baker** *November 04, 2016 13:50*

Will there be more of these available soon.




---
**Scott Marshall** *November 06, 2016 01:56*

Thanks for your interest Paul.



I have just ordered several hundred printed circuit boards, among them a good supply of the new Mark II ACR.



I just received notification then my board order goes to the production floor on Tuesday. That should mean that I will have the Mark II back in stock in about 3 weeks or less.



If you're interested in one of the remaining ACR model 1, drop me an Email @ Scott594@aol.com.



Things have been slowed down (sorry about the delay in answering) lately because I'm partially disabled due to a health issue which pops up occassionally, and has been bothersome lately.



Lead time on one of the original ACR kits would be about 1 week and 2-3weeks on the newer model.



Scott



PS for those who have been waiting or have an interest in other products, I now have in stock K40 Y stop/distribution boards and mounting bracket assembly. These can be used to change from a FFP to a standard cabled Y Axis feed or to simply replace a crashed Y stop board. They are a very high quality copy of the factory board with name brand components. They are available fully populated or as loose boards/components.



Also available are White to Green PSU adapters, Green to white PSU adapters, PSUs in both flavors, Optional Mark II front panel LED kits (DIY and Plug & Play).



Also available is a FFP (Flat Flexible Cable) to terminal Block "middleboard" style adapter board in 12 pin



I'm also stocking some very nice Multimeters at very reasonable prices. I have a Standard DMM and "Large Screen" model with Transistor tester (Gain and function) with Continuty buzzer/diode tester (reads forward drop) and even a 50HZ 3Vp-p Square wave generator. 





All this and more to up on the website as soon as I can do the photos and descriptions, Email in the meantime if you have an interest.



More coming shortly, Email if you have a specific need.



Scott 


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/jPZDGMcsve7) &mdash; content and formatting may not be reliable*
