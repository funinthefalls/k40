---
layout: post
title: "OK I have beaten my K40 down to one little quirk"
date: October 29, 2017 10:34
category: "Original software and hardware issues"
author: "Frank Fisher"
---
OK I have beaten my K40 down to one little quirk. After 20 mins the power increases (I have replaced bits of PSU, tube, air assist etc.).  Why?  I have to watch it because it suddenly gets nearly 2x as powerful as it has been. I am expecting a new pot but bet it does the same as it has with the last two.  Any ideas?  What could "warm up" in the power supply?





**"Frank Fisher"**

---
---
**Don Kleinschnitz Jr.** *October 29, 2017 11:09*

How do you know the power is changing?

Current meter?

Engraving cutting ability?

...?



Is this while a job is running or static?



I would first monitor the key Tube drive factors:

 With a DVM.

...IN voltage

...PWM value on L

... 5vdc

... 12vdc

... AC in

With a temp probe

... temperature



Look for changes during the "quirk", measure before and after.



I would also take notice of the tubes operation before and after the quirk. (Be cautious opening the laser cover and wear your glasses).






---
**Frank Fisher** *October 29, 2017 11:16*

First clue is stuff sets light instead of cutting :) Then I notice the meter is reading twice the power. When running yes but it stays in that state, then needs adjusting after a rest.




---
**Frank Fisher** *October 29, 2017 11:28*

I had the same quirk before replacing the tube so think I can count it out?




---
**Don Kleinschnitz Jr.** *October 29, 2017 11:28*

**+Frank Fisher** 

"First clue is stuff sets light instead of cutting :)"

Do you mean that its current is higher but it is not cutting as well?



Also: what coolant are you using?


---
**Frank Fisher** *October 29, 2017 12:35*

The power meter goes from 4ma to 8 or more, coolant is a fresh 15L of distilled water yesterday.


---
**Frank Fisher** *October 29, 2017 12:35*

I am cutting card so much above 5ma it sets light.


---
**Don Kleinschnitz Jr.** *October 29, 2017 17:57*

**+Frank Fisher** ah! so it is catching on fire?



Stock machine?

Does it do it on all jobs?



I would put a DVM on the "IN" pin on the LPS and monitor.



First time anyone has complained of to much power.


---
**Frank Fisher** *October 29, 2017 18:17*

I have been complaining for months, when the bloody thing was working at all ;)




---
**Frank Fisher** *October 29, 2017 18:18*

Yes stock apart from replacement parts.

And very consistent, it will warm up if I am daft enough to increase the power on the first couple of jobs instead of holding off and waiting for it to come up to meet where it was left at peak the day before.




---
**Don Kleinschnitz Jr.** *October 30, 2017 12:16*

Very strange indeed. The stock controller doesn't do anything to control power so we should look for ways the LPS control is changing.



Do have digital or pot power control?



Have you made any measurements?


---
**Frank Fisher** *October 30, 2017 13:32*

Pot, back to the original as the ten-turn broke, awaiting one with digital display.  Not measured yet as not back on the cutting until tomorrow and it needs a long session.


---
*Imported from [Google+](https://plus.google.com/114228670405979897634/posts/CNfvuuWWzZh) &mdash; content and formatting may not be reliable*
