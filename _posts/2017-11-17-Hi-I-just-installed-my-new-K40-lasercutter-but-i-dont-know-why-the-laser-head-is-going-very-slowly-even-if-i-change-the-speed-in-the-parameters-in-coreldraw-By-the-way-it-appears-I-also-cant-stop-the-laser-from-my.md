---
layout: post
title: "Hi, I just installed my new K40 lasercutter, but i don't know why, the laser head is going very slowly (even if i change the speed in the parameters in coreldraw...) By the way, it appears I also can't stop the laser from my"
date: November 17, 2017 11:22
category: "Discussion"
author: "Manon Joliton"
---
Hi, I just installed my new K40 lasercutter, but i don't know why, the laser head is going very slowly (even if i change the speed in the parameters in coreldraw...) By the way, it appears I also can't stop the laser from my computer : if I click on 'stop', i got this message : 

'detect exeption, please check

 1 if the machine is not powered please power on

2 If the machine is off line, please connect it

3If prepare to cancel current task, please click cancel

eror id1'

Can anyone help me ? Thanks





**"Manon Joliton"**

---
---
**greg greene** *November 17, 2017 14:01*

are you using the stock controller board with the USB Dongle?  If so - did you enter the correct key into the software?


---
**Mark Brown** *November 17, 2017 21:24*

Double check the board model # (printed on the board somewhere) is correct in CorelLaser's settings.


---
**Manon Joliton** *November 21, 2017 09:54*

Yeah actually I didn't selected the right lasercutter while adding the task in corel draw, now it works :) thanks




---
*Imported from [Google+](https://plus.google.com/110371358543635278419/posts/JrJyqHp2Ju8) &mdash; content and formatting may not be reliable*
