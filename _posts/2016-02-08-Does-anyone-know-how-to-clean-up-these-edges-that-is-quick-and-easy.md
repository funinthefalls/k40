---
layout: post
title: "Does anyone know how to clean up these edges that is quick and easy?"
date: February 08, 2016 14:15
category: "Materials and settings"
author: "Ben Walker"
---
Does anyone know how to clean up these edges that is quick and easy? 

![images/3680b4e57d6f5d9d448e14a8acf5bfc5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3680b4e57d6f5d9d448e14a8acf5bfc5.jpeg)



**"Ben Walker"**

---
---
**William Klinger** *February 08, 2016 14:38*

Get some of the larger nail files from a place like "Sallys" They come in various grits, sizes and even shapes. They do quick work of deburring edges.


---
**Ben Walker** *February 08, 2016 14:53*

Awesome.   I didn't think of that.   Great tip.   Thanks


---
**Aaron Helmick** *February 09, 2016 03:59*

Stay away from the womens tools,  just buy a file.  You wont have to replace it after one use. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 09, 2016 06:03*

Or even buy some sandpaper. Cheap, quick & easy.


---
**Silverado Studio** *February 09, 2016 06:39*

I like to take large tongue depressors and glue various grits of sandpaper on to them.


---
**Ben Walker** *February 23, 2016 14:32*

Thanks for all the help guys.  I am learning a lot and it appeared the charred edges were because the line was not thin enough and was double cutting the wood.  I have since learned some nuances (while a bit legalistic in nature) that I now employ to prevent this burn.  Air assist and an upgraded lens really seemed to make this cutter sing.  Really pondering the Smoothieboard at this time to maybe get variable power for grayscale engraving.  But the stock controller and upgraded lens has me cutting up a storm (pun intended).


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/VQNFDHPMCXH) &mdash; content and formatting may not be reliable*
