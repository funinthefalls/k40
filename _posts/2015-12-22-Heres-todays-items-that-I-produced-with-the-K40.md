---
layout: post
title: "Here's todays items that I produced with the K40"
date: December 22, 2015 11:21
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Here's todays items that I produced with the K40. I made a basic little stand for airbrushing, to hold the work piece on an angle, as when I hold the airbrush it is uncomfortable to work on the piece when it is flat on the table. I also elevated it slightly due to the airbrush colour-reservoir hitting table when I try bring the airbrush close for fine lines.



Also, I made a belt for a customer. Normally I wouldn't use the K40 for this, however I wanted to test engraving YNE on the front near the buckle & my website address ([eternur.storenvy.com](http://eternur.storenvy.com)) on the back behind the buckle section. So while I was at it I decided to try cut the holes for the buckle-post to go through (didn't work) & tried to cut the tip of the belt into a more rounded/triangle tip (didn't work either). The piece of leather was about 3mm thick, already dyed black. I tried 10mA @ everything down to 2mm/s & it still wouldn't cut through without overly burning/charring the edges. Normally I don't have this issue with cutting through leather, as it normally cuts through like butter (since I fixed my lens orientation) on much lower settings. So I am assuming something to do with the dye/sealing/coating on this particular piece of leather is the contributing factor in not being able to cut through it cleanly. Anyway, long story short, I decided to just etch the position of the holes/tip shape into the leather @ 4mA & 15mm/s. This was very useful to get the belt holes aligned perfectly.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/gzDCMT5tjhH) &mdash; content and formatting may not be reliable*
