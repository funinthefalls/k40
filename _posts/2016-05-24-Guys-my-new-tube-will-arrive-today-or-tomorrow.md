---
layout: post
title: "Guys my new tube will arrive today or tomorrow"
date: May 24, 2016 17:03
category: "Original software and hardware issues"
author: "Ariel Yahni (UniKpty)"
---
Guys my new tube will arrive today or tomorrow. Can anyone share experience on changing it from the stock one. What do I need to consider / care in the process? Tx





**"Ariel Yahni (UniKpty)"**

---
---
**Stephane Buisson** *May 24, 2016 23:39*

it''s easy don't panic, remove old one, place the new one in.

keep the wrapping around as the holder are generally to large for the tube diameter (centred, not too low for 1st mirror). what is important is your tube is parralel to Y axis. with about 2 or 3 cm from the first mirror.

cut a bit of pipe slot it on the wire, round several tours of wire on the tube pins to have a good connection (large enought contact area). cover with the pipe and fill the pipe with silicone ( provided with K40), both side the same, let it cure before fire.

You are done and ready for alignment.

check photos from old post (19/4/2015, 10 nov 2014, I forgot the pipe just a lot of silicone and it work fine, help to hold silicone and reduce chances of arcing)


---
**Ariel Yahni (UniKpty)** *May 25, 2016 00:33*

**+Stephane Buisson**​ how did you now I was panicking? :) Thanks


---
**Ariel Yahni (UniKpty)** *May 26, 2016 00:32*

**+Stephane Buisson**​ how long till the Silicon cures? Tx


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/YD94vSrof9v) &mdash; content and formatting may not be reliable*
