---
layout: post
title: "Don't know if I will ever implement this, but it looks pretty good"
date: December 24, 2018 21:21
category: "Modification"
author: "James Rivera"
---
Don't know if I will ever implement this, but it looks pretty good. I could probably make the frame on my CNC out of MDF.  The rotary part does not seem to actually be part of this build, but the same author (PrintControl) posted that separately. I post that link in the comments.





**"James Rivera"**

---
---
**James Rivera** *December 24, 2018 21:22*

The rotary axis part:



[thingiverse.com - Laser rotary axis by Printcontrol](https://www.thingiverse.com/thing:2687482)




---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/Zuh7aQrhS1M) &mdash; content and formatting may not be reliable*
