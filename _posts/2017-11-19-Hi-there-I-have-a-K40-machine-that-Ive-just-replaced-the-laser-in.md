---
layout: post
title: "Hi there, I have a K40 machine that I've just replaced the laser in"
date: November 19, 2017 21:44
category: "Original software and hardware issues"
author: "John Puryear"
---
Hi there,



I have a K40 machine that I've just replaced the laser in. However, now I'm getting very little power. When I turn the dial up all the way I'm just getting maybe 3-5mA. 



Would anyone happen to have advice on what I'm doing wrong?



Thanks in advance!





**"John Puryear"**

---
---
**Alex Raguini** *November 19, 2017 23:58*

I'd check you ammeter.  I thought I had a bad power supply and/or laser tube.  After replacing them, I was still only getting "3-5ma".  I put a multimeter in place of the ammeter and - guess what - I was getting more than 20ma in the circuit.  



I don't know if this is your problem but I'd check it.


---
**HP Persson** *November 20, 2017 12:51*

99% a flyback transformer issue.

1% bet on a broken potentiometer.



Have you confirmed the low power output, as Alex also suggested above?


---
*Imported from [Google+](https://plus.google.com/+JohnPuryear/posts/d5q14S5Zy5b) &mdash; content and formatting may not be reliable*
