---
layout: post
title: "Last question for the night if I want to color fill an engraved piece of wood without getting paint everywhere what is the best way to do it and stain over it?"
date: July 14, 2016 03:28
category: "Discussion"
author: "3D Laser"
---
Last question for the night if I want to color fill an engraved piece of wood without getting paint everywhere what is the best way to do it and stain over it?





**"3D Laser"**

---
---
**Alex Krause** *July 14, 2016 03:34*

Stain first... mask the piece... engrave trough the mask... seal engraved area with a couple of coats of acrylic clear coat to avoid color bleeding into the grain the go over it with the color you want once it's dry...


---
**Alex Krause** *July 14, 2016 03:34*

Make sure you wait a day or two after you stain to allow it to soak in and dry


---
**Anthony Bolgar** *July 14, 2016 04:03*

Good advice Alex.


---
**Craig** *August 24, 2016 09:30*

You can also color fill and then sand lightly before staining.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/Q3acBFPAzWc) &mdash; content and formatting may not be reliable*
