---
layout: post
title: "Corey Budwine Here's my vent setup. It's fits right in my window"
date: February 24, 2016 19:17
category: "Discussion"
author: "Sean Cherven"
---
**+Corey Budwine**​ Here's my vent setup. It's fits right in my window. 



That PCB is a PWM Speed Controller. Got it for like $10 shipped from China.



The fan and power supply I got on Amazon for cheap. The fan was less than $20.



![images/9907a7783f682baf9c40685910328dcb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9907a7783f682baf9c40685910328dcb.jpeg)
![images/b97e16f9679a63b25eeea84a304e65ca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b97e16f9679a63b25eeea84a304e65ca.jpeg)

**"Sean Cherven"**

---
---
**Joshua Harris** *March 01, 2016 18:08*

How loud is this setup? I am looking to do something similar, but am not sure if those bilge fans are quiet enough to work in the space where my window is.


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/R23S3fY9nZp) &mdash; content and formatting may not be reliable*
