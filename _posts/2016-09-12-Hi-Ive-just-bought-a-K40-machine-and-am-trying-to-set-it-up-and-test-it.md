---
layout: post
title: "Hi, I've just bought a K40 machine and am trying to set it up and test it"
date: September 12, 2016 17:23
category: "Original software and hardware issues"
author: "martyn hotchkiss"
---
Hi,

I've just bought a K40 machine and am trying to set it up and test it.



So far I haven't hit any of the horror stories that I've seen posted here, but I have met something that I hope isn't the start of something bad. I hope someone can suggest a simple fix for it (this is a cry for help!)



I've set the machine up and got water flow and the extractor fan working.



The software is a nightmare, but thought I would try out one of the templates.

I opened it into the software, and by experimentation found the output window.

I fitted a piece of 3mm MDF into the holder and tried to get the K40 to burn the MDF

As I drag the image round the window, the laser head follows the movement, and I can position it over the MDF. When I try to output, the laser head moves straight to the front of the machine, and there is a buzzing from (I guess) the stepper motor.

The only way to stop this is to switch the K40 off.

When I power it up again, the laser head defaults to the left corner at the back.

I've managed to get it to do the same a few times, but want to find out if there is something I'm doing wrong, or is this a fault





**"martyn hotchkiss"**

---
---
**Scott Marshall** *September 12, 2016 17:31*

You need to enter the Control board serial number into the Laserdrw box to "wake it up" Until you do, it will do bizzarre things.


---
**martyn hotchkiss** *September 12, 2016 18:02*

Cheers, Scott.

That leads me to another question

Which is the control board, and where in the Laserdraw box do I enter the number?


---
**Jim Hatch** *September 12, 2016 18:41*

The control board is on the right side under the switches. There's a screw holding it closed. Should be screwed to the front of the machine. It's the one you attach the USB cable into.



There's a serial number that's 16 (I think) characters long. Both alpha and numbers.



In LaserDRW there's a device settings option. In the window that pops up. The first field on the top left is a drop down. One of those entries has -M2 in it. Pick that.



On the bottom row on the left there's a device ID - that's where you enter the serial number you copied off the board.



Save it and try sending the job to the laser.


---
**martyn hotchkiss** *September 12, 2016 19:42*

Thanks, Jim - I think I've got the board number, but can't find anywhere to input it



The Software that I've got is "Moshidraw", and when I click on Output I get a screen called "Engraving Outputs"


---
**martyn hotchkiss** *September 12, 2016 20:02*

**+Jim Hatch**



Hi Jim, My software seems to be older and cruder than the stuff I see on youtube demos



The Youtube software is very obvious about the device number,  Mine is nothing like that


---
**Scott Marshall** *September 12, 2016 20:53*

Is this a new machine? I thought the Moshidraw stuff was long gone.



 I was suspicious about this as someone else seemed to have a "new" Moshidraw machine recently.



 I'm wondering if someone is selling off an old container of "lost" Moshidraw K40s?



Could you snap a couple of shots of the electronics? (you'll have to start a new post- it's the only way to put photos up here)



Scott


---
**Jim Hatch** *September 12, 2016 21:09*

Yikes. Moshi. Sorry, ignore my advice because I'm a Nano guy as were most machines sold over the past year plus at least.


---
**martyn hotchkiss** *September 21, 2016 17:02*

Jim, Scott

Thanks for your help

I've been onto the supplier, and they agreed to change my machine it seems that I've got a 2yr old machine which is now obsolete.

Apparently I was sent it in error!

They have replaced it with a new one with has the Laserdrw software, and it all seems to be running fine.

I've used the template pix  to engrave some 2mm MDF, and it has produced exactly what I expected.

Thanks for your help, especially the bit about entering the serial number in the window - that saved me a bit of aggravation


---
*Imported from [Google+](https://plus.google.com/106280016303148662156/posts/QBdaco8eTgV) &mdash; content and formatting may not be reliable*
