---
layout: post
title: "Just got a pile of veneer samples, going to try doing some inlay this weekend"
date: July 01, 2016 20:05
category: "Discussion"
author: "Tev Kaber"
---
Just got a pile of veneer samples, going to try doing some inlay this weekend.

![images/3f0bc0076b384da130407a2901aebcf7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f0bc0076b384da130407a2901aebcf7.jpeg)



**"Tev Kaber"**

---
---
**Alex Krause** *July 01, 2016 20:11*

Where did you get the samples from?


---
**Joe Keneally** *July 01, 2016 20:25*

Please see the previous question!  :)




---
**Tev Kaber** *July 01, 2016 21:02*

From eBay, I ordered from 2 sellers, but this one was better (4x more pieces than the other one):  [http://www.ebay.com/itm/Wood-Veneer-Mix-Various-Types-/122025667258?hash=item1c694beaba:g:vt8AAOSwvg9XbW5Z](http://www.ebay.com/itm/Wood-Veneer-Mix-Various-Types-/122025667258?hash=item1c694beaba:g:vt8AAOSwvg9XbW5Z)


---
**Ben Walker** *July 02, 2016 12:30*

I have used this seller and it is spectacular service and selection.  Someone out there is buying her veneer and putting it on Amazon for nearly 4x what she is charging.  But I agree.  This set I hadn't even used any yet and had to fight the impulse to buy more because it was so nice.


---
**Brandon Smith** *July 10, 2016 07:17*

I just received my order from the eBay seller Tev mentioned, and I couldnt agree more with Tev and Ben. Great pieces, cool selection, perfect price.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/eVBhuetRaBF) &mdash; content and formatting may not be reliable*
