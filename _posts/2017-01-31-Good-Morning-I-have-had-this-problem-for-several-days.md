---
layout: post
title: "Good Morning, I have had this problem for several days"
date: January 31, 2017 11:22
category: "Hardware and Laser settings"
author: "Manuel Maria Organv\u00eddez Rodr\u00edguez"
---

{% include youtubePlayer.html id="AYsVHL0Bvzk" %}
[https://youtu.be/AYsVHL0Bvzk](https://youtu.be/AYsVHL0Bvzk)



Good Morning,

I have had this problem for several days. when recording with the laser. When recording a figure begins to jump during the engraving of the figure as seen in the video.

I have greased the X and Y axes and I have tightened the X axis belt a bit.

Can you help me?





**"Manuel Maria Organv\u00eddez Rodr\u00edguez"**

---
---
**HalfNormal** *February 01, 2017 01:36*

**+Manuel Maria Organvídez Rodríguez** It could be a few things. By the sound it is making, it is possibly a loose pulley on the stepper, a loose wire on the stepper motor or it could be a bad stepper motor. The issue is that you are losing steps and causing the head to drift.


---
**1981therealfury** *February 01, 2017 13:09*

Yea, my first guess when watching the video was a problem with the stepper or a loose belt, you can hear it skipping over a few teeth on the belt when it gets to the end of the line and the motor tries to reverse the direction, my guess is a loose belt or damaged teeth.


---
*Imported from [Google+](https://plus.google.com/104340601585724034705/posts/U6hKM4myKjH) &mdash; content and formatting may not be reliable*
