---
layout: post
title: "I'm looking for a bit of advice please"
date: September 11, 2016 08:43
category: "Discussion"
author: "photomishdan"
---
I'm looking for a bit of advice please. I'm sorry if this is a duplicated post asking all the same questions, if it is please can I be pointed in the right direction?



After years of consideration, and weeks of research I've come to the conclusion that I am just about technically proficient enough to buy myself a K40 and tackle all the the issues that I've read about. 



I'm UK based and have been looking on eBay. Has anyone recently bought a K40 from eBay and would recommend a seller?

I'm aware that I will need to upgrade a lot of things on the unit to get good results and its going to be a huge learning curve. 



This is the one that I am thinking of getting: [http://www.ebay.co.uk/itm/CO2-LASER-ENGRAVER-ENGRAVING-MACHINE-40W-CUTTING-CUTTER-ARTWORK-USB-PORT-PRINTER-/111954607117?hash=item1a1103bc0d:g:j1sAAOSwqfNXlB0q](http://www.ebay.co.uk/itm/CO2-LASER-ENGRAVER-ENGRAVING-MACHINE-40W-CUTTING-CUTTER-ARTWORK-USB-PORT-PRINTER-/111954607117?hash=item1a1103bc0d:g:j1sAAOSwqfNXlB0q)



Should I just go with this and see what I receive (in terms of board) and then go about trying to fix it and upgrade as I progress?



Also are there any upgrades that I should buy when I order the machine, and can anyone provide me with links to must have items?







**"photomishdan"**

---
---
**Anthony Bolgar** *September 11, 2016 09:49*

Best upgrade you can do is to replace the stock controller (Nano or Moshi, doesn't matter, they are both crap) and replace it with an authentic Smoothieboard (Available from Robotseed.com for the EU). Upgrading the exhaust blower is also a priority upgrade, the stock exhaust fan is useless. Personally I upgraded it to a 12V bilge blower that moves 450 CFM of air. Air assist head is also a great upgrade, allows for deeper cuts in one pass and keeps smoke from dirtying the lens, which reduces power significantly with very little smoke or dirt on the mirror. An aquarium air pump is OK to use as an air source, but I use a small 2 gallon air compressor. The air assist head  use is available at [http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx) You will also have to get a new lens if you use this head as the stock mirror is 12mm and the new head uses an 18mm lens. The lens is available at [http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx](http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx) New mirrors are a useful upgrade as well, the stock mirrors are low quality. The K40 comes with a fixed bed, if you want to cut or engrave different thicknesses of materials, you will want to remove the bed, and replace it with some sort of adjustable height bed, I made my own with some threaded rod and galvanized metal grating, but you can get a manufactured one at [http://www.lightobject.com/Power-Z-table-bed-kit-for-K40-small-laser-machine-P722.aspx](http://www.lightobject.com/Power-Z-table-bed-kit-for-K40-small-laser-machine-P722.aspx) Nice thing about that one is that it is motorized, making setting the height a breeze. SO that is the basics of what you should do, it will turn your K40 into a pretty nice machine. Oh, and I almost forgot, if you upgrade the controller to a Smoothieboard, the best software to use with it is LaserWeb, an open source project being developed by **+Peter van der Walt** and other members of the community. It is so much better than the crappy CorelLaser or LaserDrw programs that ship with the K40. You can get it at [https://github.com/openhardwarecoza/LaserWeb3](https://github.com/openhardwarecoza/LaserWeb3)  While you are waiting for everything to arrive, I suggest you read everything in this forum, then read it again, and just to be sure, read everything one more time. Knowledge is power. And spoon feeding is for babies ;) Hope this does not intimidate you, it seems like a lot to digest, but if you approach it in small steps, it is not that hard. Many of us have done all of this so help is available.


---
**photomishdan** *September 11, 2016 10:21*

**+Anthony Bolgar** thank you so much for your reply. If budget was an issue (it is) would the first upgrade be air assist?


---
**Anthony Bolgar** *September 11, 2016 10:27*

I would really suggest the controller upgrade as being the number one upgrade, but for budget issues, yes, I would do the air assist upgrade.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 10:50*

**+photomishdan** I second what Anthony has already mentioned. Another point I'd like to make about the controller upgrade is that by doing it ASAP you won't require 2x learning curve. I spent almost 12 months learning the software that operates on the stock controller, with its limitations & nuances. Now, I've finally made the switch to the Smoothieboard & LaserWeb & I have a whole new learning curve (although it's nowhere near as bad as the original software with ZERO documentation or support). Smoothieboard (controller) & LaserWeb (software) have plenty of active testers, posters in the community & direct support from the developers. So, whilst air assist is a great upgrade to do asap (due to low cost), controller will pay off in less time wasted learning the original software (which is severely limited).


---
**photomishdan** *September 11, 2016 15:20*

**+Yuusuf Sallahuddin** controller upgrade it is then! Any insight into which one? Or does that really depend on what I'm doing with it? Also would this controller work in s lager laser if the time came to upgrade?


---
**photomishdan** *September 11, 2016 15:26*

**+Anthony Bolgar** probably do both! 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 20:13*

**+photomishdan** So, I just spent ages writing a response, only to click in an area & lose the entire thing. So now attempt #2:



In regards to the controller, to some degree it will depend on what you want to do with the machine. If you just want to cut, you can retain the original controller & software as that is suitable for plain cutting. Although, if you want to engrave imagery, the original controller will only do 100% black imagery, so no shades of colour. The best you can manage is pre-processed dithering (to approximate shades of grey). Results look pretty average using this method.



That being said, if upgrading, there are a few options. Some people run on arduinos, raspberry pis, etc. However, I would personally recommend the Smoothieboard as from my understanding it has a lot more computing power as it's brain & is able to utilise the features for LaserWeb better.



So, when it comes to the Smoothieboard, it is capable of controlling a larger laser. As long as the laser has a tube, some stepper motors, then Smoothieboard can control it. It can also be used to control CNC machines, 3D Printers.



So, for controlling a laser you will need the Smoothieboard 3X (3 axis) version minimum. Only because they don't sell a 2X. So you will only really need 2 axis to control the k40, unless you add a motorised z-table like Anthony previously linked. If you are not very electronically inclined (like myself), they offer a 3XC version (which has connectors/plugs/etc already preconnected to the circuitboard).



So, do like Peter says & buy an original Smoothieboard, not one of the Mks/makerbase ripoffs. This helps support the development of Smoothieboard. For UK, you will get it from [robotseed.com - 5XC Smoothieboard 5XC Smoothieboard](http://RobotSeed.com). You can purchase the 4 or 5 X/XC variants also, as they will run the K40, just they have the ability to control extra axis. Which would be useful to control a rotary attachment (for engraving on things like drinking glasses).


---
**photomishdan** *September 11, 2016 20:22*

**+Yuusuf Sallahuddin** thank you so much for this reply (and writing it twice too!) It's good to know that the board would work in other applications, so its more of an investment piece (which is always good when trying to justify these things).


---
**Vince Lee** *September 13, 2016 00:58*

Before making too many decisions on what to upgrade, I suggest using the stock machine for awhile to feel where the pain points are for you.  I myself have done very many upgrades (custom venting, led lighting, radiator cooling, upgraded lens, head-mounted fan for air assist, stainless bed, articulated visible laser spot, flow sensor cutoff and alarm, and a dual-switchable smoothie/moshi controller system.  Still, I recognize that I both enjoy doing the upgrades and have the tools and experience for doing this sort of thing.  Depending on what you use your engraver for, not all of these upgrades may be worth the immediate time and expense.  For instance, even though I have a very nice smoothie system, I probably still use the moshiboard half the time, since many of my jobs involve quick cuts of simple shapes (such as a custom washer cut out of gasket material) that are faster to do with moshidraw than using a separate drawing app.


---
**photomishdan** *September 13, 2016 08:44*

**+Vince Lee** that's interesting. I've been reading up on the Smothieboard conversion and it's something that I'm sure I'll be able to do myself, but I did think I wanted to use the stock controller to see where it's limitations are. All your mods sound amazing.


---
*Imported from [Google+](https://plus.google.com/105674876465839652573/posts/8Nmn4fDDQuv) &mdash; content and formatting may not be reliable*
