---
layout: post
title: "I have started playing musical chairs with laser tubes today"
date: January 03, 2018 00:17
category: "Discussion"
author: "Anthony Bolgar"
---
I have started playing musical chairs with laser tubes today. My Redsail LE400 is getting a new 50W Reci tube which really produces 55W at full power, allowing me to take the 50W tube it had that only really produced 45W to transplant into my K40 to replace the crappy generic 40W tube that only produces 34W. That tube will then be kept as a spare for now. SO I will have a 60W laser, a 55W laser and a 45W laser. Just need to save up for a 100W beast now.





**"Anthony Bolgar"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2018 02:22*

Is there an extra in this assortment? 


---
**Anthony Bolgar** *January 03, 2018 02:29*

Yup, I have a brand new 50W tube sitting in a box.


---
**Phillip Conroy** *January 03, 2018 04:01*

I didn't think reci  made tubes under 80 watts, how do you know power output of tubes?


---
**Ned Hill** *January 03, 2018 23:41*

**+Phillip Conroy**  If my memory serves me correctly, he has a Mahoney laser power meter.  


---
**Anthony Bolgar** *January 04, 2018 01:38*

Yup, love my power meter, paid fo itself already with getting a refund on a tube that was sub spec.




---
**Anthony Bolgar** *January 04, 2018 02:07*

The Reci tube is 5 years old, was given to me by one of the users in this group, Scott Thorne. Was really surprised to see that the tube was still outputting more than the written spec for the power level. Goes to show you how well made RECI tubes are.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/7JyTZqgk4VP) &mdash; content and formatting may not be reliable*
