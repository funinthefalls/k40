---
layout: post
title: "I'm trying to get my AZSMZ mini up and running and I think I have everything connected but I can't get any axis to jog"
date: October 06, 2016 02:00
category: "Smoothieboard Modification"
author: "Eric Rihm"
---
I'm trying to get my AZSMZ mini up and running and I think I have everything connected but I can't get any axis to jog. I'm using the middleman board, do I need to have 5v or 12v going into it?





**"Eric Rihm"**

---
---
**Ariel Yahni (UniKpty)** *October 06, 2016 02:13*

**+Eric Rihm** the middleman only does passthrough of the X axis, thus you should still be able to jog the Y. I have the same board, post a picture of your entire connection to better help you 


---
**Eric Rihm** *October 06, 2016 02:30*

Here's a picture of my setup. The X axis cables from the middleman to smoothie don't match because I spliced them.

![images/8ffa4c9a5aebd0e2c14cadcd7cb2ed1b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ffa4c9a5aebd0e2c14cadcd7cb2ed1b.jpeg)


---
**Ariel Yahni (UniKpty)** *October 06, 2016 02:40*

**+Eric Rihm** you are missing the drivers for the stepper. That long black row at the top houses 5 removable stepper drivers you need at least 2, one for each motor. Either drv8825 or A4988


---
**Ariel Yahni (UniKpty)** *October 06, 2016 02:41*

other than that the endstops


---
**Eric Rihm** *October 06, 2016 02:54*

thanks I ordered a set of DRV8825, how do I hook up the endstops I thought they were carried in the ribbon cable, wasn't really sure about that part.


---
**Ariel Yahni (UniKpty)** *October 06, 2016 03:07*

This is easy just splice each set and then positive on the outside and negative on the middle. Look at my upgrade collection you will find the config file I use

![images/67a32aed963b5b9a05330002c26fa27e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/67a32aed963b5b9a05330002c26fa27e.jpeg)


---
**Eric Rihm** *October 06, 2016 03:17*

Thanks should be able to get everything running now. I'm looking at the endstops and not really where to splice, I only see the ribbon cable returning to the right side of K40. Not sure where the endstop wires go.


---
**Ariel Yahni (UniKpty)** *October 06, 2016 03:22*

Are this mechanical endstops? They attached to the nano via a single connector. Follow them to the actual endstop. Each one is 2 cables. Each pair goes into the azsmz like the above. Red and yellow are NC I beleive and the greens are ground. You will see that labeled on the endstop itself


---
**Eric Rihm** *October 06, 2016 03:37*

I think I have optical endstops, not sure if the wiring is the same


---
**Ariel Yahni (UniKpty)** *October 06, 2016 03:39*

Get me a close up on those. Im not sure either but I can get someone to help with those


---
**Eric Rihm** *October 06, 2016 03:47*

I've looked all around the case and rails and only saw one end stop, I'm probably missing something but couldn't find another one. 

![images/5d3e024c01286872d39f2655da113780.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5d3e024c01286872d39f2655da113780.jpeg)


---
**Ariel Yahni (UniKpty)** *October 06, 2016 03:49*

Yes those are optical. **+Don Kleinschnitz**​ can you help us here with the optical endstops? 


---
**Don Kleinschnitz Jr.** *October 06, 2016 13:11*

This is a schematic of my setup:

[http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/](http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)



The optical end stops need 5v and gnd and the return signal is an open collector from the sensors receiver. The sensor part # is listed in the schematic. 



The schematic details the cabling, the end stop breakout card and the middleman that I use to connect the ribbon cable to the smoothie.



The Y end stop breakout is on a bracket as **+Eric Rihm** picture shows above. The X end stop is under the left side of the x slide with a smaller breakout. 



Note: that I had to modify my end stop bracket (slotted the mounting) so that it clears the interposer properly. Otherwise if the sensor hits the interposer when it starts up it will damage the endstop.

[https://plus.google.com/113253507249342247018/posts/6dmjyZy2A3F](https://plus.google.com/113253507249342247018/posts/6dmjyZy2A3F)



The X breakout connects to the Y breakout carrying the  X stepper and end stop signals. The Y end stop signal is combined with the x signals in the  ribbon cable and routed to the right side of the machine.



I cabled Xmin, Ymax from the smoothie to the middleman Esty & Esty. 



If you dont have a middle man board you could remove the FPC ribbon connector from the Nano and use it in a home made breakout. 

[https://plus.google.com/101545054223690374040/posts/5fH11hFSFFZ](https://plus.google.com/101545054223690374040/posts/5fH11hFSFFZ)



I built a harness that uses 5V from the Xmin, Ymax connectors on the smoothie to provide power to the sensors. Any 5V would work however. There is a equivalent circuit included on the smoothie tab in the schematic.



Let me know if I can help more .....

[digikey.com - K40-S 2.1](http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)


---
**Eric Rihm** *October 08, 2016 04:08*

I just popped in my drivers and the gantry moves now but it makes a weird hissing noise and the drivers heatsinks get burning hot to the touch, not sure if I need to adjust the drivers or something. Here's a video that you can hear the noise in, as soon as the jog any axis I get this weird hissing noise and drivers are crazy hot if I don't jog the axis it doesn't make the hissing noise [http://sendvid.com/jw1mdmms](http://sendvid.com/jw1mdmms) 



I think I managed to get Xmin and Ymax hooked up from the middleman thanks Don, just gotta get the rest now after I figure out the weird noise. 

[sendvid.com - IMG 0059](http://sendvid.com/jw1mdmms)


---
**Ariel Yahni (UniKpty)** *October 08, 2016 10:53*

**+Eric Rihm**​ that's from overcurrent. You need to adjust the little pot counter clockwise. This are very sensitive. I suggest you look for a video on that


---
**Eric Rihm** *October 11, 2016 23:29*

**+Don Kleinschnitz** I've got a middleman and cabled Xmin and Ymax from the smoothie to the Esty and Estx on the middleman, but I'm still trying to figure out the 5v power part that you built a harness for. Could you elaborate on the harness a bit more?


---
**Don Kleinschnitz Jr.** *October 12, 2016 01:38*

**+Eric Rihm** check the schematic posted above there is detail there. 

I essentially use 5v and gnd from the the xmin/ymax connector from the smoothie to provide 5v to middleman. 

So the 5v and gnd from the xmin and ymax go to the middlemans 5v input on a 2 pin connector. The signals from xmin and ymax connectors go to esx and easy on a separate 2 pin connector. 


---
**Eric Rihm** *October 12, 2016 06:03*

thanks Don got it figured out. **+Ariel Yahni** which firmware are you using for your AZSMZ mini smoothie clone? I'm having difficulties with laserweb communicating with my board. I'm able to connect but getting errors sending commands and was suggested to upgrade the firmware. I haven't been able to flash the recommended firmwares due to having a clone board.


---
**Ariel Yahni (UniKpty)** *October 12, 2016 13:34*

Use this one, just rename the file to firmware.bin [https://drive.google.com/open?id=0B-1--YftWEOrV0JXam1oU2ZZYms](https://drive.google.com/open?id=0B-1--YftWEOrV0JXam1oU2ZZYms)


---
**Eric Rihm** *October 12, 2016 23:11*

flashing that one fixes the laserweb problems and I'm able to jog the axis but it always makes my display inactive and I have it enabled in the config file. Not a big deal atm because at least its working now.


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/68mn4tCv6eX) &mdash; content and formatting may not be reliable*
