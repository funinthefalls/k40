---
layout: post
title: "Hi all , this my first post , although i have been following this great community for some time now"
date: July 17, 2017 16:06
category: "Original software and hardware issues"
author: "Isa Nasser"
---
Hi all , this my first post , although i have been following this great community for some time now.

My K40 have been working nicely so  far except today it started to act strangely ,this happened  

while I was trying to engrave ,I heard  a very strong arcing sound  coming from 

the laser tube enclosure  with sparks coming from the  red laser terminal . 

I would appreciate  any insight in this matter with the best way to remedy it.



thank you all.




**Video content missing for image https://lh3.googleusercontent.com/-7Vl2XqTlQzU/WWzgnFJVK4I/AAAAAAAAACQ/JYpJEDeNrkkBhw1ux5SNwvykroUzBTAjQCJoC/s0/VID_20170717_172456.mp4**
![images/67d5ef60b1ce77977d2e0d76b4da69a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/67d5ef60b1ce77977d2e0d76b4da69a8.jpeg)



**"Isa Nasser"**

---
---
**Steve Clark** *July 18, 2017 21:57*

Turn off your machine and unplug it. DO NOT RUN IT until this is fixed. 



You have serious (killing) voltage arcing there. I'm going to let someone else with a better electrical background than me tell you how to fix it as this is something that needs to be fixed correctly






---
**Isa Nasser** *July 19, 2017 05:10*

Thanks Steve , I did stop using it , the laser was not firing , I believe it is a dead tube , I already started ordering a new tube . One thing I have noticed is that the arcing does not happen on lower power settings such as 15% it only start  at 30%  or more. 


---
**Nathan Thomas** *July 19, 2017 11:29*

**+Isa Nasser** was there any condensation on the tube? 


---
**Isa Nasser** *July 19, 2017 19:41*

**+Nathan Thomas**  Yes ,  the last time I used it before the tube died on me , there was condensation  on the water pipes both intake and outfall , the average humidity here ranges from 60% to 80% and sometimes it will reach over 90%.


---
**Nathan Thomas** *July 19, 2017 21:03*

If you're in a high humidity area, that is most likely your problem. Especially if there's condensation on either end of tube. Once it arcs to the water, your tube is done. There are a couple threads in here about that...including a couple videos.



I'd highly recommend getting the humidity issue solved before getting another tube. 


---
**Don Kleinschnitz Jr.** *July 19, 2017 23:15*

**+Isa Nasser** what coolant are you using? 



It may well be your tube is bad but also insure that the red anode wire is not chafing in the cabinet and the wiring has no damage.



I could not see the arc clearly (off to the left) but it looked like it was high up on the wire where it was touching the cabinet, not in the tube?



<b>...you do this at your own risk...</b>



Unplug the machine and let the machine sit for a couple of hours with it unplugged before you inspect the anode wiring. 



Use a discharge stick to isolate yourself and insure that the LPS is discharged.



<b>Make a discharge stick</b>

Make a discharge stick by taping a wire with the end striped back 1/4", to the end of a 2ft x 1/2" diameter dowel. 



Ground the other end of the wire to bare metal on the cabinet .



Holding the end of the dowel opposite the taped wire, probe the bare wire end in and around the anode wiring to discharge it before you do the inspection. 

 

<b>Inspection</b>

See if you can find any chafes breaks etc in the wire around the area of the arc. Also look for any dark marks or tracks.

Inspect the anode connection sheath to see if there is any damage, splits or bare surfaces 



Take photos...


---
**Isa Nasser** *July 20, 2017 04:53*

**+Nathan Thomas**  yes  , I was thinking about getting a dehumidifier to tackle this issue . 


---
**Isa Nasser** *July 20, 2017 05:13*

**+Don Kleinschnitz** I am using tap water , the water comes from a desalination plant , I am not sure about its salinity  level , I use ice packs as will as ice to reduce its temperature , the water ambient temperature in the bucket is 36 c / 97 F , it usually drops to 17 c/ 63 F, that’s when I start using the laser cutter .  I also add some drops of algaecide to inhibit algae growth .  Thanks for the guidelines , I'll have my brother who is an electrician  to assist me , will report back soon .






---
**Don Kleinschnitz Jr.** *July 20, 2017 11:21*

**+Isa Nasser** how long has that water been in use.

I also recommend that you replace the water with distilled water with no additives and see if anything changes.


---
**Isa Nasser** *July 20, 2017 16:20*

**+Don Kleinschnitz** It has been used for about 4 weeks , I usually  empty 2/3 of the water in the bucket every week  . In the beginning when I first purchased the laser cutter I was using distilled water , however it is not easy to get it in huge quantities , I buy it as 1 litre bottles  which is awkward for me ,that when I switched to tap water . Before the tube gave its life I was using tap water with it for about 8 months without any noticeable problems .






---
**Don Kleinschnitz Jr.** *July 20, 2017 16:43*

**+Isa Nasser** it may not be your problem but we have proven that conductive coolant can damage your power supply and laser tube over an extended period of time. The only way to know for sure it to measure the coolants conductivity. This is also true if you add to much additive like algaecide. 

Seems like your supply is ok but the tube may be damaged unless you  have found a defect in the wiring.


---
**Isa Nasser** *July 20, 2017 17:21*

**+Don Kleinschnitz** I believe in this case I'll go back to using  distilled water just to be on the safe side , I'll also refrain from using the Algaecide . I still have to investigate this issue further  and try to find the root cause of the arcing . Thanks for the insight , very much appreciated . 


---
**Don Kleinschnitz Jr.** *July 20, 2017 17:55*

**+Isa Nasser** 

a post on the problem and some working formulations. I recommend using the cited algecide concentration after you tried without it.



[http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
**Isa Nasser** *July 21, 2017 05:22*

**+Don Kleinschnitz** Thanks Don 


---
*Imported from [Google+](https://plus.google.com/112675346235331499232/posts/HM5f6inAqZV) &mdash; content and formatting may not be reliable*
