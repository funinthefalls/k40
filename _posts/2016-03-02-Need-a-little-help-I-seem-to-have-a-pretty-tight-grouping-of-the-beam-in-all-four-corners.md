---
layout: post
title: "Need a little help I seem to have a pretty tight grouping of the beam in all four corners"
date: March 02, 2016 03:48
category: "Discussion"
author: "3D Laser"
---
Need a little help I seem to have a pretty tight grouping of the beam in all four corners.  But the my seem low.  I think where the head is a crooked but that is an easy adjustment.  I am still having trouble getting through 1/8 material that spans the Entire table.  It cuts through some spots but not all 

![images/080de474f58ec03041dc9f2b999c3a09.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/080de474f58ec03041dc9f2b999c3a09.jpeg)



**"3D Laser"**

---
---
**Jim Hatch** *March 02, 2016 04:24*

What kind of material, how much power & how fast?



Plywoods tend to be very erratic due to the different glues used. Cutting is best about 10mm/sec or slower (some materials like cardboard, paper, acrylic can go as high as 30mm/sec). I tend to use 10-12ma for my power setting and sometimes need a couple of passes (set the LaserDRW repeat to 2 or 3) depending on material and thickness. 


---
**Phillip Conroy** *March 02, 2016 07:02*

I use 7.5 speed on mdf and full power of 15ma .half your cutting speed and use max of 15ma for tube life ,also remove and check focal lens,take a photo of a cut with a ruler next to it.



My laser cuts so fine i can not push paper between cut peace and scrap.i changed my focal lenes from a stuffed 12mm one to a good 18-20mm good one ,i am waiting for a feeler gauge to arrive before i can actuletly measure cut width.


---
**I Laser** *March 02, 2016 09:04*

**+Corey Budwine** Looking at the paper it looks like the beam is close but there are two distinct burn marks. They should ideally be on top of each other, on all mirrors.


---
**3D Laser** *March 02, 2016 13:13*

**+I Laser** I think can tweak the last mirror a bit to get them all at the same place my question is are the

Beams hitting to low on the entry point 


---
**Jason Reiter** *March 02, 2016 13:31*

**+Corey Budwine**   I had the same thing where my beam was hitting right at the bottom a little lower than yours is . You have to raise the lasertube with a shim and the re align all the mirrors again. this will raise your final laser point on the head to the center. Once I did this it cuts perfect.


---
**3D Laser** *March 02, 2016 18:30*

**+Jason Reiter** how did you shim your tube 


---
**Jason Reiter** *March 02, 2016 18:43*

I loosened  the clamps and I actually added a small price of the 3mm ply under the rubber on the bottom side of the tube. I thought it would be too much but it was perfect. The first time I folded tape up but it still was not high enough.  I also figured out the first mirrors may not be exactly centered but my test shot from close and far were the same. The last shot to the laser head is the important one to have centered .


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/dUkjGFpNefE) &mdash; content and formatting may not be reliable*
