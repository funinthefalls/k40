---
layout: post
title: "just wondering does any body have their K40 wired with a RAMPS board if so how hard was it?"
date: January 20, 2016 02:46
category: "Hardware and Laser settings"
author: "3D Laser"
---
just wondering does any body have their K40 wired with a RAMPS board if so how hard was it?  and how does it compare to a smoothie board





**"3D Laser"**

---
---
**Anthony Bolgar** *January 20, 2016 06:13*

I have my K40 wired up with an Arduino/Ramps controller and it works great. The Smoothie board is very similair, but has a faster more powerful processor. If you can afford a smoothie board, I think that would be  the better way to go (but not too much better). The Ramps setup is almost as good. Software wise, they both support open source software, Visicut works great with a smoothieboard, TurnkeyTyrannys Marlin firmware with his Inkscape plugin works very nicely, and both boards support a new software called LaserWeb 







The following are links to various build logs for converting the controller from the stock one to a Ramps 1.4 or Smoothieware controller board:



    [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide) (Upgrade to Smoothieboard by Stepahne Buisson)

    [https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion](https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion) (Upgrade to Ramps by ExplodingLemur)

    [http://weistekengineering.com/](http://weistekengineering.com/) ("middle-man" board for easy connections to RAMPS by Jeremy Goss)

    [http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html](http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html) (Ramps by Dan Beavon)

    [https://github.com/TurnkeyTyranny/buildlog-lasercutter-marlin](https://github.com/TurnkeyTyranny/buildlog-lasercutter-marlin) (Firmware for Ramps conversion by TurnkeyTyranny)

    [https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin](https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin) (Inkscape plugin by TurnkeyTyranny)

    [http://www.floatingwombat.me.uk/wordpress/](http://www.floatingwombat.me.uk/wordpress/) (Ramps upgrade by Floating Wombat)






---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/hsWCga8eAev) &mdash; content and formatting may not be reliable*
