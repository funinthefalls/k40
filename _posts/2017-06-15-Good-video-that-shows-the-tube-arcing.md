---
layout: post
title: "Good video that shows the tube arcing"
date: June 15, 2017 18:33
category: "Discussion"
author: "Nathan Thomas"
---
Good video that shows the tube arcing. In this case it was because of using tap water and antifreeze.





**"Nathan Thomas"**

---
---
**Bruno Ferrarese** *June 15, 2017 19:15*

So far I have done zero research about cooling methods so forgive me if its a stupid question. Whats wrong with tap water and antifreeze? 



I'm using tap water and bleach (just to kill bacteria/algae ). Is that bad? Why? 



I was under the impression the "liquid" was running on a separate "chamber" and just the temperature (and viscosity to some degree) was relevant. 


---
**Nathan Thomas** *June 15, 2017 19:57*

I believe **+Don Kleinschnitz** and others have done research on this subject and shows that it is not ideal for the reasons shown on the video. The mostly agreed upon consensus in this group is Distilled water (the more the merrier...I use a 5 gallon bucket) and a little bleach to kill the algae. Some use algicide instead of bleach. But most have gone away from antifreeze. 



I'm not sure how to link posts and articles, but maybe you can search the forum and find the post (very long one) on the subject. The research shows that distilled water is better than tap...but also gives a good to worse reference showing all the options.


---
**Don Kleinschnitz Jr.** *June 15, 2017 20:11*

**+Bruno Ferrarese**​ not at all a stupid question. We all asked that same question (except **+HalfNormal**​​) until problems of leaking charge started to become apparent. 

The not so technical explanation is that conductive coolant enables current flow through the coolant. We are working with 10 of thousands of volts across a long length. 

The least conductive and still algae free is distilled water and aquarium  algaecide. Clorox is next best. Assuming its used in the correct concentration. 



[donsthings.blogspot.com - K40 Coolant](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html?m=1)






---
**Jeff Johnson** *June 15, 2017 20:21*

In your video, is the dimmer, more purple glow the ideal color of the plasma? I know my tube has a glow but I'll pay more attention this evening.


---
**Bruno Ferrarese** *June 15, 2017 20:58*

**+Don Kleinschnitz** I see. Thin glass walls. Great info. Thanks a lot!


---
**Jeff Johnson** *June 30, 2017 14:39*

After extensive testing, I'm not sure what to think now. I flushed the tube with purified water and then drained it and I still get the same results. So tap water, purified water and no water produce identical results. I've also isolated the buzzing sound to the power supply and when it buzzes there is significant power loss. I'm going to be ordering a nice power supply to see if this addresses the issue. I'll update with my results.


---
**Don Kleinschnitz Jr.** *June 30, 2017 15:23*

**+Jeff Johnson** what coolant is in the machine now "distilled water". 



Can you take a video of each end of the laser tube while it is 'buzzing" we are looking for an arc inside the tube.

I have had one user that was using conductive water and the tube developed a corona track through the epoxy seal at the cathode end. Strangely it did not leak but when full voltage was applied it arc'd through the seal to the coolant. You could see the arc, the plasma cloud ran parallel to the tube and then it would intermittently jump to the edge of the seal at the cathode end.



Can you measure the voltage in your bucket to ground?



When it buzzes does the current (via the meter) fluctuate?



Also look for arcs in the dark at the LPS, along the HV wiring and the connections to the tube.


---
**Jeff Johnson** *June 30, 2017 15:57*

**+Don Kleinschnitz** I use sealed gallon water jugs that are frozen as a coolant. I put them in a 5 gallon sealed bucket filled with about 3.5 gallons of purified water.



I haven't measured the voltage but I will.



The current doesn't change when it buzzes.



This is a 45w tube from Light Object (1000mm) and it's done this from the day I installed it.  The previous stock tube did the same.



The HV wire is new and is a spark plug wire. A heck of a lot better insulation than the stock HV wire and should have no problem handling the expected 25kv. I checked the power supply and wiring there is no arcing that I can detect.



I can make the buzz stop and regain full power by turning the power adjust knob a little. The plasma seems to dim, the noise stops. While it buzzes I don't notice any more dancing than when it isn't buzzing, it's just a lot brighter. I really don't know what it is.



After I install a new power supply that I know can handle this tube I'll let you know what happens. I've already replaced one flyback transformer so I'm not confident the stock power supply is in good condition anyway.




---
**Jeff Johnson** *July 01, 2017 00:41*

**+Don Kleinschnitz** I just checked the voltage. Shortly after turning on the laser cutter I get between 10 and 500 mV between ground and the water. Something is shorted out obviously. When the laser is firing I see between 10mV and a max of about 7.5V. It was all over the place and didn't seem to sync with the buzz all the time but sometimes it seemed to .



I also paid much closer attention to the power and it does change with the buzz. It's almost undetectable at higher power (10 - 15mA) but it's easy to see at low power (under 5mA). 



I don't quite know what to think but I'm about ready to order that power supply. It's going to run about 235 for one that isn't cheap and I know will work for this laser tube. It's worth it to me to make sure this thing is running as well as it can. It provides a small, yet steady income, more than paying for my RC plane and drone hobby.


---
**Don Kleinschnitz Jr.** *July 01, 2017 12:16*

#K40LPS 

#K40coolant



**+Jeff Johnson** I don't think millivolts in the bucket is that unusual and of concern, I am seeing users with 100-300 volts. It seems that your power change is in the 1-2 ma range. (make sure that you got the range correct and it is not 10-500 Volts :))



1. The fact that you can see variation on the meter suggests to me that the current variation is in the supply-tube-ground circuit, the coolant is not part of that circuit. 

2. To further support #1 the bucket is not charging much so the coolant does not seem to be part of this problem.



<b>So for now the variation logically doesn't seem to be the coolant.</b>



The supply seems to be the culprit considering the new tube but its not obvious how to determine this for sure without putting the supply on some kind of dummy load which is practically, impractical.



It bothers me that it has done this from the start including both the stock and 45W tube. These supplies see to either work or not. I have not seen one just be unstable but then again I am surprised with every failed one I get.




---
**Jeff Johnson** *July 01, 2017 18:39*

**+Don Kleinschnitz** I just did another test but this time I was cutting using the software to initiate firing the laser. Guess what? Over 500 volts and a really nice arc. This only happens when the software is controlling it, not when I hit the test switch on the top panel. Previously I was using the test switch to check voltage. The power meter shows the same mA whether fired by software or the test switch but obviously something is very wrong with the power supply's internal wiring. I'm ordering that new power supply this evening!


---
**Don Kleinschnitz Jr.** *July 02, 2017 02:33*

**+Jeff Johnson** where was the arc? You are saying 500V was measured in the bucket?



Not at all clear why the test switch at the same current does not arc like running a job as the same current...




---
**Jeff Johnson** *July 02, 2017 02:42*

**+Don Kleinschnitz** The arc was between the multimeter lead and anywhere on the laser cutter body. I got a good 1/4 arc and it was pretty consistent. I put one lead in the water and touched the other to several parts of the laser cutter that are grounded and it arced everywhere as long as I wasn't using the test switch. 



I went back and forth a few times between using the test switch and running a cut job with software and the results were consistent. 500 or more V when using software and  very little when using the test button.



I've ordered a new laser power supply and will report back when I've had a chance to install and test. 




---
**Don Kleinschnitz Jr.** *July 02, 2017 03:00*

**+Jeff Johnson** fascinating, **+Ned Hill** and I have been working on symptoms exactly like this on his machine! We have not determined if this is a problem or not?





**+Ned Hill** I forgot did you have buzzing with your machine?


---
**Jeff Johnson** *July 02, 2017 03:23*

I've been able to identify 3 sources of buzzing. The loudest is the vibration of one of the components in the power supply. The quietest comes from the laser tube and seems to coincide with the power supply buzz. Between those two in volume there's more of a whine that comes from the material being cut. This only happens with MDF and usually only when the laser doesn't cut completely through. I always hear it when cutting shallow texture and detail lines and I think it's caused by the rapid expansion of gas working its way out of the cut.


---
**Ned Hill** *July 02, 2017 03:39*

**+Don Kleinschnitz** I was having sounds that I attributed to arcing that I hadn't heard before.  After I changed the water the sound disappeared.  I'm pretty sure my water was contaminated either with too much residual bleach left over from the flush I did or from a leaking cooling pack.  I was getting arcing with the multimeter lead, or the ground wire, as well.  Since the new water change, with just the machine on,  I'm getting around 50V in the bucket relative to ground with a current of about 15 uA.  Hitting the test fire button jumps the voltage up to 350V with a current of about 40 uA but then drops back to 50V after releasing the button.  Not much difference with different mA power settings.  Running a job now doesn't bump the bucket voltage up like it had been.



**+Jeff Johnson** before my last water change I was seeing up to 700V in the bucket relative to ground after the laser had run a job!  Gave me a bit of a tickle when I stuck my hand in the bucket to fish out a water bottle :P


---
**Jeff Johnson** *July 08, 2017 01:44*

**+Don Kleinschnitz** and **+Ned Hill** My new powersupply arrived and it has solved my problem. No more buzzing and the power has increased at least 20% for the same mA. I assume this is the power I was losing into the water bucket. I ordered the HY-T50 from China and it arrived in a week, on the same day the 24V power supply needed to run the board since it doesn't have one built in like the OEM model.




---
**Don Kleinschnitz Jr.** *July 08, 2017 12:24*

**+Jeff Johnson** great. Some closing questions on the new supply configuration?



1. Did you measure the voltage in the bucket when running and not?

2. How much did the supply cost if you don't mind my ask?

3. Are you using a laser power meter to measure the power gain?

4. Do you want to donate the old supply to the HV lab, I can make some measurements? :).



I still do no understand the mechanism by which the supply causes this current leak. Up to know I assumed it was a defect (HV leak) in the tube or current flow across the water glass barrier. In thinking about this we do have to consider that you have changed LPS type.

More K40 wonders....




---
**Jeff Johnson** *July 08, 2017 13:57*

1. I will measure this evening.

2. $130 shipped from Amazon. Also $20 for the 24v power supply. 

3. I was able to speed up my cutting from 15mm/s to 19mm/s with the same results on 3mm mdf. Not a real measurement,  just an estimate.

4. Email me the address jwjexec@[gmail.com](http://gmail.com). 


---
**Jeff Johnson** *July 08, 2017 13:59*

I can also throw in the old flyback Transformer that failed and had to be replaced.


---
**Don Kleinschnitz Jr.** *July 08, 2017 14:05*

**+Jeff Johnson** you rock..... I will mail you the adr.... :)


---
**Don Kleinschnitz Jr.** *July 08, 2017 14:09*

**+Jeff Johnson** sent you the email request, yes the flyback (HVT) will be use full a "bad" one reference.


---
**Jeff Johnson** *July 10, 2017 14:55*

I also want to mention that the consistency of the laser beam is greatly improved. Cut edges are much smoother and light cutting (I do this to add text, decorations, etc) is much more even. Perhaps that buzz being produced by the old power supply was causing a stutter in the beam, leading to a more pronounced perforation effect, rather than a clean cut.




---
**Don Kleinschnitz Jr.** *July 10, 2017 17:32*

**+Jeff Johnson** it is likely if the supply has a leak (the arc) that the power in the beam will be inconsistent. 



Interested in what the voltage in your bucket is with this new one. 



I wonder if the old supply is bad or the new supply type is just better. We will see what the old one does on the bench?



Thanks again....


---
**Jeff Johnson** *July 11, 2017 22:43*

**+Don Kleinschnitz** I'm pleased to report that my multimeter detects exactly 0 volts between the coolant and ground after installing the new power supply.




---
**Ned Hill** *July 11, 2017 23:24*

That's very interesting. What does that tell us about the how the potential is developing in the bucket then?


---
**Don Kleinschnitz Jr.** *July 12, 2017 01:44*

**+Ned Hill** it tells me that we do not at all understand what is creating the "hot bucket".

Problem is we changed a few things here: the supply and the type of supply....


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/j8uWYcCB4Xw) &mdash; content and formatting may not be reliable*
