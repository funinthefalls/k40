---
layout: post
title: "i have been tracking the K40 for a while now, making sure it will be worth my while"
date: November 30, 2016 05:06
category: "Discussion"
author: "Sean K Hughes"
---
i have been tracking the K40 for a while now, making sure it will be worth my while. i want to expand the possibilities for my illustration and design business ([www.seankhughes.com](http://www.seankhughes.com)). as i am edging closer to purchasing i have come across another brand. i was just hoping to find some feedback on the RECI RC-A40 vs the K40.



i havent been able to find too much online about it.

thanks for any help! :)







[https://www.alibaba.com/product-detail/Desktop-mini-40w-Co2-laser-engraver_60554276024/showimage.html](https://www.alibaba.com/product-detail/Desktop-mini-40w-Co2-laser-engraver_60554276024/showimage.html)





**"Sean K Hughes"**

---
---
**Kelly S** *November 30, 2016 05:43*

Looks like the same k40 I just got of ebay.


---
**Kelly S** *November 30, 2016 05:46*

Well mine has changed a lot since I purchased it, lol...


---
**Stephane Buisson** *November 30, 2016 07:43*

RECI is a reputable tube brand, this is why I let this post out of the spam. BUT WARNING...

a) I notice no clear photo of the tube in this add, could be a fake at this price. the tube could be more expensive than the whole K40. 

b) poor seller rating  


---
**HalfNormal** *November 30, 2016 12:50*

Looks like they appropriated the name RECI to sell a generic K40.


---
**Ned Hill** *November 30, 2016 13:05*

It's showing me an $800usd price when I start an order.  At least for that price you are getting more options, like moly mirrors, air assist head, laser pointer, limit switches, height adjustable bed.  However the pictures are also not consistent through the description so I'm kind of leary of this seller.


---
**Sean K Hughes** *December 05, 2016 16:55*

thanks everyone for taking the time out and for  the second set of eyes on this. much appreciated:)



i think the k40 is my way to go.




---
*Imported from [Google+](https://plus.google.com/100522779171966696697/posts/b8wJD2djcj5) &mdash; content and formatting may not be reliable*
