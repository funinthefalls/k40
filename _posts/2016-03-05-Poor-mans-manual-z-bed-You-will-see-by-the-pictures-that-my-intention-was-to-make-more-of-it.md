---
layout: post
title: "Poor mans manual z bed, You will see by the pictures that my intention was to make more of it"
date: March 05, 2016 22:43
category: "Modification"
author: "Tony Schelts"
---
Poor mans manual z bed, You will see by the pictures that my intention was to make more of it.  On the last picture you will see Pulleys that are only for decoration.  I an very happy with the way this is working. It does mean you have to adjust each corner one at a time.  I have found that as long as your laser head is level you can use a spirit lever and it doesn't take very long to adjust.  You will also see that I have some 3mm ply attached which is great for referencing the position.  Anyway any comment good or bad.  This was meant to be a rought attempt but I may keep it as is. 



![images/06e6476d8e38618483c425e2d2d2df09.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/06e6476d8e38618483c425e2d2d2df09.jpeg)
![images/bf63868f780b461a3ce76a7c09d73fd0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf63868f780b461a3ce76a7c09d73fd0.jpeg)
![images/8ba8910bbcb272859e09d1b52c8f9372.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ba8910bbcb272859e09d1b52c8f9372.jpeg)
![images/a9f3ff416e1de8588240862ba409e2a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a9f3ff416e1de8588240862ba409e2a7.jpeg)

**"Tony Schelts"**

---
---
**HP Persson** *March 05, 2016 23:40*

Looks pretty good, do you get any reflection problems with that mesh?


---
**Tony Schelts** *March 06, 2016 00:34*

No nor really works better than every thing else i have tried, but i haven't tried honeycomb yet.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/2vDCMwrbL5J) &mdash; content and formatting may not be reliable*
