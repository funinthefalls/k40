---
layout: post
title: "Cant buy a no longer available gasket set for a 1968 Lamborghini Miura weber carb?"
date: February 01, 2018 21:51
category: "Object produced with laser"
author: "Bob Damato"
---
Cant buy a no longer available gasket set for a 1968 Lamborghini Miura weber carb? no problem, make them! 



This is the dry run on regular heavy paper stock. The final is done on actual gasket material. You can see old vs new... This thing just paid for itself in this one project alone tenfold. 




{% include youtubePlayer.html id="56wblmzFQds" %}
[https://www.youtube.com/watch?v=56wblmzFQds](https://www.youtube.com/watch?v=56wblmzFQds)







![images/356c75e06e2a5e540a55a19949c8327e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/356c75e06e2a5e540a55a19949c8327e.jpeg)



**"Bob Damato"**

---
---
**Ariel Yahni (UniKpty)** *February 01, 2018 22:25*

You got me at Miura !!!!! 


---
**Josh Rhodes** *February 01, 2018 23:07*

Wow very nice .. you should make a run of 50-100 and sell them to other owner.



What a gorgeous car that is!!


---
**Bob Damato** *February 02, 2018 02:07*

Thanks guys! I should, its a good idea. This is just one of the gaskets, there are several, I did make them all.  Im not good at selling my wares but thats a really good idea.


---
**John-Paul Hopman** *February 02, 2018 18:46*

**+Bob Damato** Don't worry about "selling" them, just throw the photos and results on a few of your hobby forums. The other owners will likely seek you out.


---
**Mark Brown** *February 03, 2018 01:26*

I haven't had a reason to yet, but I had considered making gaskets with the k40.  Glad to know it works.


---
**Steve Clark** *February 03, 2018 18:01*

Gaskets are a nice side job, the biggest challenge is finding customers. I just finished up a fifty part run for a metal casting company. The parts were simple and I was able to do three at a time layered. 



The thickness of the material was the stack limit (and tolerances). 



This time there was some buna-n in the fiber so the cleanup afterwards and halfway through was important along with a good air nozzle and venting system.



 Here is a pic of the paper samples before doing the fiber finished product. 

![images/7b8dbf7a81569a9cff5dccf2dd2e8b4b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b8dbf7a81569a9cff5dccf2dd2e8b4b.jpeg)


---
**Fly Oz** *February 16, 2018 03:56*

Great idea


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/NhkQM9eUmdz) &mdash; content and formatting may not be reliable*
