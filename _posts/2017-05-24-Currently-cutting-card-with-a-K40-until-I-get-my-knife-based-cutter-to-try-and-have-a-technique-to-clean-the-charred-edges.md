---
layout: post
title: "Currently cutting card with a K40 until I get my knife based cutter to try, and have a technique to clean the charred edges"
date: May 24, 2017 06:50
category: "Discussion"
author: "Frank Fisher"
---
Currently cutting card with a K40 until I get my knife based cutter to try, and have a technique to clean the charred edges.  Drop them into a sieve, top it and clamp with another sieve, shake it in front of a hairdryer.  Insane but quick and effective.  Any other techniques?





**"Frank Fisher"**

---
---
**Anthony Bolgar** *May 24, 2017 06:59*

Have you tried cutting through transfer tape on top of the card? It has very low tack and should remove without damaging the card.


---
**Frank Fisher** *May 24, 2017 07:04*

I would have to coat the entire card surface as I cut dozens of parts per sheet.  I think laser isn't the best option as some cuts are so close to other freshly cut pieces they can set light even at 3mA and I can't get much lower.  Have had to stagger cuts to give time to cool. Just have to get a ton of them cut for an event in 3 weeks and it will be 2 weeks before I try the blade instead.


---
**Phillip Conroy** *May 24, 2017 07:44*

 are you using air assist


---
**Frank Fisher** *May 24, 2017 07:53*

No the machine arrived in a broken state, clearly used not new, with a burnt out tube, arcing, freewheeling pot, bad alignment of axis, and likely more to find as I fix them.  Upgrades come later due to cashflow :( ebay & paypal were uninterested in enforcing very clear sales laws here and shrugged saying "dropshippers, what can you do, eh?"


---
*Imported from [Google+](https://plus.google.com/114228670405979897634/posts/CeUveuvVPRV) &mdash; content and formatting may not be reliable*
