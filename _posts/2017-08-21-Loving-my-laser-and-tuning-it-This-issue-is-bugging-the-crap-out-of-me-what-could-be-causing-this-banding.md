---
layout: post
title: "Loving my laser and tuning it! This issue is bugging the crap out of me what could be causing this banding?"
date: August 21, 2017 02:18
category: "Hardware and Laser settings"
author: "William Kearns"
---
Loving my laser and tuning it! This issue is bugging the crap out of me what could be causing this banding? K40 cd3 mini and LW4...

![images/989b45367a526810d752c1ae51cfad6b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/989b45367a526810d752c1ae51cfad6b.jpeg)



**"William Kearns"**

---
---
**Jim Fong** *August 21, 2017 02:53*

Is this plywood or solid?   I sometimes see this when I laser ply and the laser has burned through the face veneer layer. I always thought it could be the glue they applied to bond the layers. 


---
**William Kearns** *August 21, 2017 02:57*

I just started to think it was the glue rollers I will back off on my power


---
**William Kearns** *August 21, 2017 02:58*

yes its birch plywood from a box store


---
**Claudio Prezzi** *August 21, 2017 09:37*

You could check if it's the wood by rotating the material by 90°


---
**Kirk Yarina** *August 21, 2017 16:45*

Sure looks like the banding follows the grain, matching up with the grain outside the engraved area.


---
**Ned Hill** *August 23, 2017 02:24*

Vibration can also cause banding like this.   See this old post of mine. 



[plus.google.com - Had an interesting observation with my machine today. Was reverse engraving ...](https://plus.google.com/u/0/108257646900674223133/posts/2mP6xqYF6Bf)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/eJv1pdv4CoL) &mdash; content and formatting may not be reliable*
