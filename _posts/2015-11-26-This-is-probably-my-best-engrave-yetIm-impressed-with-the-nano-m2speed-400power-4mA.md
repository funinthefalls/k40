---
layout: post
title: "This is probably my best engrave yet....I'm impressed with the nano m2....speed 400....power 4mA"
date: November 26, 2015 17:26
category: "Object produced with laser"
author: "Scott Thorne"
---
This is probably my best engrave yet....I'm impressed with the nano m2....speed 400....power 4mA.

![images/74a852ac1d8e8dbdc58fa937dd85e74d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74a852ac1d8e8dbdc58fa937dd85e74d.jpeg)



**"Scott Thorne"**

---
---
**Gary McKinnon** *November 26, 2015 18:42*

VERY nice.  That guy looks familiar, is he a historic figure ?


---
**Scott Thorne** *November 26, 2015 19:02*

It's Aretino Pietro....I had to look it up.


---
**DIY3DTECH.com** *November 26, 2015 19:54*

Agreed, nice outcome, how long did it take?


---
**Scott Thorne** *November 26, 2015 20:14*

About 14 minutes....but worth every minute!y


---
**DIY3DTECH.com** *November 26, 2015 22:51*

Do you use these for inlays? 


---
**Scott Thorne** *November 26, 2015 22:55*

I guess I'm not understanding, I saved the image from Google, then imported into corellaser and engraved....not sure what inlays are.


---
**Todd Miller** *November 26, 2015 23:51*

Just imported it and engraved ?  I'm still trying to get BW or Grey Scale to etch right.  My best results are to make it a negative and adjust the power & speed, but still doesn't look like it should... Working on an image now....


---
**Scott Thorne** *November 26, 2015 23:53*

I downloaded already black and white then engraved it.


---
**Gary McKinnon** *November 28, 2015 12:58*

Is there a preferred format for images to engrave from ?


---
**Scott Thorne** *November 28, 2015 13:02*

I'm using jpeg, sometimes I convert it to Windows bmp....but all the ones I've posted on here were jpegs....I'm using 3/16 birch plywood 12x12...I purchased 40 sheets from Amazon for 39 bucks...free shipping.


---
**Gary McKinnon** *November 28, 2015 13:53*

Thanks Scott, i like the finish on the birch looks very classic.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/3oWMSyCNopp) &mdash; content and formatting may not be reliable*
