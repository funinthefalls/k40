---
layout: post
title: "The finished pieces (tobacco pouch + lighter cover) that I laser engraved"
date: October 23, 2015 07:46
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
The finished pieces (tobacco pouch + lighter cover) that I laser engraved. I just love how the burn marks look with the beeswax staining. Looks very antique-ish to me.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Completed pouch & lighter cover, stained & coated with natural beeswax.



![images/40b50d7c5f0b8a722ae8a8ddb61a20c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40b50d7c5f0b8a722ae8a8ddb61a20c1.jpeg)
![images/1fa158ff7f54493651966bbb6997b8cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1fa158ff7f54493651966bbb6997b8cd.jpeg)
![images/2902dce63588c5b078020f0000aeb872.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2902dce63588c5b078020f0000aeb872.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Brendan Power** *October 23, 2015 07:56*

Nice :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 23, 2015 08:05*

**+Brendan Power** Thanks Brendan.


---
**Phillip Conroy** *October 23, 2015 08:10*

Nice


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 23, 2015 09:46*

**+Phillip Conroy** Thanks Phil.


---
**Joey Fitzpatrick** *October 23, 2015 14:00*

Those came out great!!!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 23, 2015 15:12*

**+Joey Fitzpatrick** Thanks Joey. I'm very happy with the finish too.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XbUoGwFuh5t) &mdash; content and formatting may not be reliable*
