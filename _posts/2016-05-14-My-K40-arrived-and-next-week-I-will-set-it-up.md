---
layout: post
title: "My K40 arrived and next week I will set it up"
date: May 14, 2016 18:57
category: "Discussion"
author: "Sebastian C"
---
My K40 arrived and next week I will set it up. My problem is, that I only have a spare raspberry pi 2 and I couldn't find any descriptions driving the stock K40 with Linux. Is it possible? Also I could't find coreldraw for Linux, but the graphics will be done in my desktop, so this shouldn't be a Problem. THX for you help





**"Sebastian C"**

---
---
**Ariel Yahni (UniKpty)** *May 14, 2016 20:18*

My suggestions would be 1) get a virtual machine with Windows 2) get a smoothie board so you can use any software. Unless anybody has an alternative 


---
**Sebastian C** *May 14, 2016 20:42*

THX, but wine Corel on a raspberry pi is not really an option. And modifying is maybe planned for the future. Also the only available 4x board is too expensive for me atm. I think I will ask friends and family for old PC donations :D


---
**Ariel Yahni (UniKpty)** *May 14, 2016 20:58*

Well the VM was meant for your computer. You can buy a smoothie clone also.


---
**Sebastian C** *May 14, 2016 21:16*

Maybe I got that VM thing wrong. Did you mean using the Pi as kind of WIFI-Printer? Are there any experiences with this solution?



I saw some smoothie clones, but if I upgrade, than by supporting the devs and not some knock-offs just selling cheap copies without the hard work behind. 

But first I want to just use the machine as it is. 


---
**Ariel Yahni (UniKpty)** *May 14, 2016 21:24*

Not WiFi printer, at least not in the common way. If you have a Linux pc get a Windows VM to run that standard apps that come with the machine, including the security dongle.  I don't think there is any workaround that expect to change the board,  then you can use any software to design and run the laser with LaserWeb. I could be wrong regarding the standard apps but I don't think I am. Agree with you on supporting the Devs


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 14, 2016 21:43*

**+Sebastian C** I'm thinking Ariel is correct that you basically cant run the standard apps/security dongle on anything but windows. So unless your desktop pc is windows, then you probably need to VM windows. But, this would require the laser to be plugged into the desktop. Getting donations of old computer stuff is probably a good idea (any old laptop to control the laser would do).


---
**Sebastian C** *May 14, 2016 22:10*

Like I said, I wasn't paying attention to the stock requirements as everyone is talking about the possible upgrades. But 3 textmessages later I nowbwill get a nice Dell Latitude for free. 

So that whole discussion was justb pushing me to get a smoothieboard faster :D

 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 14, 2016 22:20*

**+Sebastian C** Yeah the sooner we all get Smoothies the easier the workflow & better choice of softwares. :)


---
*Imported from [Google+](https://plus.google.com/108800242160866633862/posts/XvCaTbqMmup) &mdash; content and formatting may not be reliable*
