---
layout: post
title: "Engraving Stainless Steel Last week a machine shop asked me to engrave part numbers on some 300 small 304 Stainless Steel parts they were making for a customer"
date: April 28, 2017 17:40
category: "Materials and settings"
author: "Steve Clark"
---
Engraving Stainless Steel



Last week a machine shop asked me to engrave part numbers on some 300 small 304 Stainless Steel parts they were making for a customer. I had not tried to do this in stainless yet so I suggested doing a few sample parts and then if the customer liked them I would go ahead with the rest. They sign off on them and I ran the rest.

Unfortunately I can’t show you a picture of the part because it is a proprietary design… but I do have a picture of a test piece of material with some engraving on it. 



First get yourself some CRC dry Moly lubricant  for 13 bucks with free shipping like this: 



[http://www.ebay.com/itm/282331750428?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/282331750428?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)

Or this stuff if you got lots of money 63 bucks plus shipping :



[https://www.cermarksales.com/product/lmm-14-black-for-metals-6oz-aerosol-spray/](https://www.cermarksales.com/product/lmm-14-black-for-metals-6oz-aerosol-spray/)



And a can of Acetone, Nitrile gloves, and a coffee can to wash the spray off after engraving. (Wash off in a well ventilated area)

Spray a light coating let dry and engrave.

The laser power settings were not high, I think I used about 12ma. And I was cycling about three a minute. I charged 35 cents each. Total time doing them about 2 hrs. and made just about a $100 profit as I only used about third of the can of Moly spray. Acetone, about a cups worth. I was able to wash three as it engraved three stacked side by side.  



![images/7b2657029483a9ee49f54756bc42b497.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b2657029483a9ee49f54756bc42b497.jpeg)



**"Steve Clark"**

---
---
**Andy Shilling** *April 28, 2017 17:47*

**+Steve Clark** Ive tried the Molykote but not had any joy is it the D123 you are using?


---
**Steve Clark** *April 28, 2017 18:09*

Andy, No I only used CRC 03084 Net Weight: 11 oz. 16oz Dry Moly Lubricant Aerosol Spray as I provide in the link. I got that off of EBay. I've not tried anything else as this worked so well. A further note... I also cleaned the parts in Acetone before spraying the Moly on.


---
**Andy Shilling** *April 28, 2017 18:18*

Ok thanks didn't think to check the link lol


---
**Arion McCartney** *April 29, 2017 03:54*

What speed did you use? Thanks for sharing your process and links!


---
**Steve Clark** *April 29, 2017 04:20*

Arion, I think it was 15mm/sec but I'm not out in the shop to look it up.


---
**Steve Clark** *May 01, 2017 20:37*

Arion, I think I was wrong on the feed rate...more like 6 to 8 mm/sec.


---
**Arion McCartney** *May 04, 2017 11:11*

**+Steve Clark**​ thanks. I just got the crc in the mail yesterday and gave it a try. Didn't work first time but I'm sure I've done something Wong. Some of it stuck, but much of it came off. 


---
**Steve Clark** *May 04, 2017 15:55*

You are not the first who has had trouble. The parts I did were cut out of 304 stainless sheet  .040" thick. I'm wondering it thickness or alloy has an effect.  I have not had a chance to test more yet. 


---
**Arion McCartney** *May 05, 2017 15:34*

My only test was on a thick metal bottle opener, so maybe that's why. I'll have to play around with it more.


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/JBGhi1cNDpz) &mdash; content and formatting may not be reliable*
