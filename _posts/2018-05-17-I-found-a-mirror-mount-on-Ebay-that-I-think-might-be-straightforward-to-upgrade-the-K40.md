---
layout: post
title: "I found a mirror mount on Ebay that I think might be straightforward to upgrade the K40"
date: May 17, 2018 15:48
category: "Modification"
author: "Don Kleinschnitz Jr."
---
I found a mirror mount on Ebay that I think might be straightforward to upgrade the K40.

I bought one to test but have not done so.

I post this in case anyone wants to try it ......



I anticipate this replacing #1 and # 2 mirrors.



[http://donsthings.blogspot.com/2018/05/improved-k40-mirror-mounts-adjustments.html](http://donsthings.blogspot.com/2018/05/improved-k40-mirror-mounts-adjustments.html)





**"Don Kleinschnitz Jr."**

---
---
**Anthony Bolgar** *May 17, 2018 16:17*

Cloudray on ebay has some nice ones that are only $2 more.

[ebay.com - Details about Cloudray CO2 Laser Head Set for 2030 4060 K40 Laser Engraving Cutting Machine](https://www.ebay.com/itm/Cloudray-CO2-Laser-Head-Set-for-2030-4060-K40-Laser-Engraving-Cutting-Machine/122804477362?hash=item1c97b79db2:m:mOtq9ipcK8uVIxnVjqCE0MQ)




---
**Don Kleinschnitz Jr.** *May 18, 2018 03:59*

**+Anthony Bolgar** do you know if the Cloudray ones are direct replacements. The add above is for one or are they trying to say the set of 2 mirrors mounts and a head??


---
**Anthony Bolgar** *May 18, 2018 14:37*

They are supposed to be the direct replacements. I would send them a message to find out for sure. I have bought 2 Ruida controllers and a bunch of lenses from them and have been very pleased with the quality and service.




---
**Don Kleinschnitz Jr.** *May 18, 2018 16:17*

**+Anthony Bolgar** the customer review on amazon said that new brackets were needed.


---
**Tony Sobczak** *May 18, 2018 17:25*

Following


---
**Sean Cherven** *January 17, 2019 15:48*

I got this set, and it's NOT a direct replacement. I'm struggling to figure out how to mount the mirror in the laser tube compartment. Not a direct replacement, and I'm not even sure how to mount it. Does anyone have any idea's?


---
**Don Kleinschnitz Jr.** *January 18, 2019 12:02*

**+Sean Cherven** See if this helps?



[donsthings.blogspot.com - Improving the K40 Optics: Mirror #1 replacement](https://donsthings.blogspot.com/2018/11/improving-k40-optics-mirror-1.html)



[https://donsthings.blogspot.com/2018/11/improving-k40-optics-mirror-2.html](https://donsthings.blogspot.com/2018/11/improving-k40-optics-mirror-2.html)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/FTXKLSNgpZ2) &mdash; content and formatting may not be reliable*
