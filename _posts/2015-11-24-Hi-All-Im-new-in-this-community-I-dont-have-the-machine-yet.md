---
layout: post
title: "Hi All, I'm new in this community, I don't have the machine yet"
date: November 24, 2015 07:18
category: "Discussion"
author: "Akos Balog"
---
Hi All,



I'm new in this community, I don't have the machine yet.

I'd like to use it for cutting, and hole "punching" in 1.5mm thick nylon/polyester webbing. Do you think it would work with this machine, and is it possible, to make nice cuts and holes in this material, with this machine?

(I saw some advertising, from much more expensive 90w laser, that it is good for jobs like this, However I can't pay that much for it)

I'm planning air assist, as I think it is needed, to prevent fusing together behind the laser.



thanks for the help





**"Akos Balog"**

---
---
**Bill Parker** *November 24, 2015 08:28*

Hi Akos I have nylon sheet with mine and it was perfect 40 Watts is plenty of power to cut 1.5mm nylon/polyester webbing. Yes they do a very nice cut like you say air assist will help a lot. If the K40 is for business and will be in continuous use you should consider a better machine. If you intend to do other jobs on it the software that your restricted to is not up to much and the K40 runs at whatever power you have set it to so can not engrave and cut in the same run. There is problems setting the k40 to engrave on low power and then cutting the item with a higher power as the item your cutting does not seem to line up when you rerun the K40 to cut. There is conversions for the K40 that add DSP but that depends on your electronics ability and the cost.


---
**Akos Balog** *November 24, 2015 08:38*

**+Bill Parker**

Hi Bill, Thanks. I don't plan to use it for engraving, I'd like only to finish webbings. Only straight/curved cut on the end, and some holes. So very simple jobs, I hope I can manage them without DSP.

It is only a very small business, I think I'd use it only 1-2 days a week, for a few runs/day. So I hope it is enough for me, as I can't afford much more expensive models (and as I saw, anything more quality stuff would cost 5-10x)


---
**Bill Parker** *November 24, 2015 09:44*

One thing I would say is that the K40 has a mind of it's own as quite a few people say and that is that when it starts it does a long straight line like diagonal across the work piece so it is ok if it is your own material and it is not going to cost you a lot to replace but I would never dream of putting somebody else's item into a K40 I know I sound negative but I would rather you know the downfalls of the K40 before buying one.


---
**Akos Balog** *November 24, 2015 09:59*

**+Bill Parker**

Thanks. I'd work only on my own raw material, and it cheap, so it is not a problem, as long as it won't happen to frequently.

But I think it is caused by the controller board. What type of board does it? I've read that the older moshisoft boards are junk, but the newer M2 boards are better.


---
**Scott Thorne** *November 24, 2015 10:05*

I have the m2 board and I've never had a problem with mine, I've been using it for over a month now.


---
**Gary McKinnon** *November 24, 2015 13:00*

Hi Akos, i also have the M2 board, the most recent model is from July 2015, whn you are going to buy the laser, if you are getting it from ebay, let me know and i will tell you which seller i bought it from, or you could just ask your seller which board it is. 



Once man on a forum i was reading claimed he had been using his K40 every day, under quite heavy use, and had been doing it for over 1,000 hours and the machine was still fine.




---
**Akos Balog** *November 24, 2015 13:26*

**+Gary McKinnon**

Thanks.

I'm looking this machine: [www.ebay.com/itm/262106638496](http://www.ebay.com/itm/262106638496)

The seller couldn't really help, all I could learn from him, that it doesn't support Moshidraw...


---
**Phil Willis** *November 24, 2015 15:55*

**+Akos Balog** It appears to be a regular K40 with Corel Draw and the Laser Draw plug in. I have an identical one. I did add air assist and in order to align engraving and cutting all thats needed is to put the engraving marks on one layer and cutting on another but ensure that you have a small dot in the top left on both layers ( or master layer ), the software works perfectly to engrave then cut as a two pass process. 


---
**Akos Balog** *November 24, 2015 17:29*

**+Phil Willis** thanks, what controller board does yours have?


---
**Phil Willis** *November 24, 2015 18:34*

**+Akos Balog** An M2.


---
**DIY3DTECH.com** *November 24, 2015 20:02*

My M2 also does that strange horizontal movement thing, however it does not (thankfully) turn on the  laser.  I have built several CNC machines as well as 2 Watt laser engraver using GRBL and was looking at the boards and seems to be an easy swap and seeing you can RAMP and Arduino off eBay for $30 and use real GCode could be a winner.  Has anyone done this?




---
**Gary McKinnon** *November 24, 2015 22:32*

Ye, loving your vids 3dtech :) RAMPS+Arduino mod here : [http://3dprintzothar.blogspot.co.uk/2014/08/40-watt-chinese-co2-laser-upgrade-with.html](http://3dprintzothar.blogspot.co.uk/2014/08/40-watt-chinese-co2-laser-upgrade-with.html)


---
**DIY3DTECH.com** *November 25, 2015 00:57*

Wow I now have an itch to convert and stop playing with the junk software also like that they have the light object adjustable Z bed too.  However noticed they are using Marlin and not GRBL but the wiring should be the same.  I am just trying to figure out how deep I want to get in as the reason for buy is wanted an "easier" solution to the CNC however not sure this is it well not the K40 any way.  My goal is to make project kits and sell them as side line business, and look for a fast easy way to produce a quality product without breaking the bank.  Hmmm, going to have check with Santa to see if I have been naughty or nice this year...


---
**Gary McKinnon** *November 25, 2015 01:09*

Ye i wanted cheap and easy cutting so i'm not going beyond air-assist + sight + bed-change + extraction improvement. Mine has worked fine so far exporting from sketchup to pdf then importing to Corel and cutting using the laserdraw plugin. I'm wondering how well it will cut at it's farthest distance due to power falloff, lots to test !


---
**Scott Thorne** *November 25, 2015 15:12*

Mine was terrible, but as I've started my rails were out of square, after squaring it up, it cuts perfect all the way out.


---
*Imported from [Google+](https://plus.google.com/106984452297752036237/posts/Lx83Z7X9ppd) &mdash; content and formatting may not be reliable*
