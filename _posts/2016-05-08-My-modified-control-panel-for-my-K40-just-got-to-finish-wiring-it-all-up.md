---
layout: post
title: "My modified control panel for my K40 just got to finish wiring it all up"
date: May 08, 2016 16:42
category: "Modification"
author: "Neil Robbins"
---
My modified control panel for my K40 just got to finish wiring it all up

![images/ae94a2079fa40ad36af2ba619519ee48.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae94a2079fa40ad36af2ba619519ee48.jpeg)



**"Neil Robbins"**

---
---
**3D Laser** *May 08, 2016 17:44*

Love it !


---
**Todd Miller** *May 08, 2016 18:13*

Looks Awesome !


---
**Jim Hatch** *May 08, 2016 18:51*

Perfect example of Laser Porn :-D


---
**James Rivera** *May 08, 2016 19:02*

Carbon fiber--snazzy! :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 19:04*

Wow, that looks schmick. Love the switches.


---
*Imported from [Google+](https://plus.google.com/112727076545743143342/posts/QgqhUgdZ82d) &mdash; content and formatting may not be reliable*
