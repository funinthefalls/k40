---
layout: post
title: "Looks like an affordable laser color additive"
date: September 03, 2018 03:01
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Looks like an affordable laser color additive.



[https://www.amazon.com/Heves-Laser-Ink/dp/B071F3BPLK](https://www.amazon.com/Heves-Laser-Ink/dp/B071F3BPLK)





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *September 03, 2018 13:20*

Is this the stuff you apply and then expose with a laser to set??


---
**HalfNormal** *September 03, 2018 13:36*

Yep


---
**Elias Galvan** *September 05, 2018 01:45*

Anyone use it yet?  


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/J9YWVGkvXfW) &mdash; content and formatting may not be reliable*
