---
layout: post
title: "I am having a problem with the speed settings"
date: October 30, 2016 03:14
category: "Original software and hardware issues"
author: "MendingThings"
---
I am having a problem with the speed settings. I have a stock K40. I am using Corel Laser to draw and the stock driver to send the gcode to the cutter. When I hit engrave and the window opens up with the position and other settings, I choose either of the three options, engrave, cut or mark, the speed defaults change in the window to the right but if I go and change them, it always cuts or engraves at the same speed. For example, I chose cut, it defaults to 20 mm/s, I cut with that and time it. The I go back and do the same cut but this time I change the speed to 200 mm/s and hit start, it completes the cut in exactly the same time. I am sure it is something simple I am overlooking or at least I hope so.

Anyone got some advice for this laser newby?





**"MendingThings"**

---
---
**Ariel Yahni (UniKpty)** *October 30, 2016 03:35*

Each board has a unique number to it that you need to add into the settings, that could be it if it's not properly setup. 


---
**MendingThings** *October 30, 2016 03:48*

Problem solved. I went into laser draw and it worked fine. So I compared the settings and the SN was correct but I had selected the wrong board. That was the issue. Thanks for the response Ariel




---
*Imported from [Google+](https://plus.google.com/107259123708340177433/posts/dbd2uSJ9m7N) &mdash; content and formatting may not be reliable*
