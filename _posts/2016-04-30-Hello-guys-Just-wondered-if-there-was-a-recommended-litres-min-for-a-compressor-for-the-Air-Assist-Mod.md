---
layout: post
title: "Hello guys, Just wondered if there was a recommended litres/min for a compressor for the Air Assist Mod?"
date: April 30, 2016 16:56
category: "Discussion"
author: "Pigeon FX"
---
Hello guys, Just wondered if there was a recommended litres/min for a compressor for the Air Assist Mod?





**"Pigeon FX"**

---
---
**Phillip Conroy** *May 01, 2016 06:24*

Irun a full size air compressor ,and use 10 psi setting on regulator at laser cutter


---
**Jean-Baptiste Passant** *May 02, 2016 06:20*

**+Stephane Buisson** use a 15l/min, I got the 25l/min version but haven't tested it...

[http://www.aliexpress.com/item/LF-2208P-Electromagnetic-air-pump-Fruit-and-vegetable-detoxification-machines-ozone-generator-aquarium-fish-parts-shipping/1128865153.html?aff_platform=aaf&aff_trace_key=f12a300c085040d69eddd79304606a24-1462169824097-07574-2vnUZVbeY&sk=2vnUZVbeY%3A&cpt=1462169824097&af=cc](http://www.aliexpress.com/item/LF-2208P-Electromagnetic-air-pump-Fruit-and-vegetable-detoxification-machines-ozone-generator-aquarium-fish-parts-shipping/1128865153.html?aff_platform=aaf&aff_trace_key=f12a300c085040d69eddd79304606a24-1462169824097-07574-2vnUZVbeY&sk=2vnUZVbeY%3A&cpt=1462169824097&af=cc)


---
**Pigeon FX** *May 02, 2016 21:21*

What better to go for when it come to cutting something like ply wood....should I be going with Higher L/min or PSI? these are the two I have been looking at:



Airbrush compressor @ 23L/min (start at 3bar/43 psi, stop at 4bar/57 psi)



or



Piston Air Compressor Pump @ 45L/min (Max 0.18bar/2.6 psi)



(I mainly plan of cutting and engraving 1.5mm Laserable 2 Ply Laminate through to 3mm Rigid Laminate, but having a set up that can do thin plywood would be nice)


---
**Jean-Baptiste Passant** *May 03, 2016 06:13*

I've heard good things from people using an Airbrush Compressor.



I'm not the most knowlegeable about L/Min and PSI, but I believe what we want is to get the fumes out of the lens way so :

L/Min is the way to go as you send an airflow, PSI is a pressure, and you probably don't want to stress the material by applying any pressure on it (even though it will probably do nothing).



Again, I am not sure about that, and if anyone have more information please do give them.


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/KxwGn4nYahB) &mdash; content and formatting may not be reliable*
