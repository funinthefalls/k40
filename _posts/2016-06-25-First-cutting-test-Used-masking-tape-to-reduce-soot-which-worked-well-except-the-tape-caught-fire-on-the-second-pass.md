---
layout: post
title: "First cutting test. Used masking tape to reduce soot, which worked well except the tape caught fire on the second pass"
date: June 25, 2016 20:14
category: "Object produced with laser"
author: "Tev Kaber"
---
First cutting test. Used masking tape to reduce soot, which worked well except the tape caught fire on the second pass.

![images/15daa8e18a624f26ef1c81741880b371.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15daa8e18a624f26ef1c81741880b371.jpeg)



**"Tev Kaber"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 21:27*

Very nice result. What settings did you use/material?


---
**Tev Kaber** *June 25, 2016 21:30*

10mm/sec, two passes.  Not sure of the power level, still learning how to read that, the knob isn't labelled and the analog meter is hard to read. From what I've heard, above 90% and below 20% are bad for the laser, so I kept it roughly in the middle, powerwise.


---
**Tev Kaber** *June 25, 2016 21:34*

As for the material, it was some scrap wood I had around, I think it's 1/4" poplar.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 21:38*

**+Tev Kaber** I posted on another of your posts just now on how to read your ammeter value. Try to stay below 18mA. Not sure what that equates to in %. I tend to use anywhere between 4-10mA when I do most of my work.


---
**Robert Selvey** *June 25, 2016 23:11*

If you don't already have air assist you might think about adding it, that should cut down on the flare ups and fires.


---
**Tev Kaber** *June 25, 2016 23:17*

Yeah, I have an air assist head but don't have an air compressor yet so don't have it installed.


---
**Ned Hill** *June 26, 2016 01:47*

I'm using an airbrush pump that does up to 0.7cfm (20 l/min) but, according to my new flow gauge I installed, 12-15 l/min is plenty sufficient for the LO air assist head.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/FDi4teU7sEP) &mdash; content and formatting may not be reliable*
