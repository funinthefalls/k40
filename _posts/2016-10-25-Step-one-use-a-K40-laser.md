---
layout: post
title: "Step one use a K40 laser, ... ;-))"
date: October 25, 2016 22:52
category: "Object produced with laser"
author: "Stephane Buisson"
---
Step one use a K40 laser, ...      ;-))





**"Stephane Buisson"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2016 06:42*

Is that what the actual steps are? I saw this in my email & was thinking "we can do that with the dry moly on the K40"


---
**Stephane Buisson** *October 26, 2016 11:51*

**+Yuusuf Sallahuddin** it seem to me it would be simpler to burn the adesive tape with the K40 then use metal etching with salt and electricity.  no need of moly.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2016 13:14*

**+Stephane Buisson** That sounds like a cool idea. I haven't heard of that method for etching the method. Will have to look into it :) Thanks.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/EpeC9jHrP29) &mdash; content and formatting may not be reliable*
