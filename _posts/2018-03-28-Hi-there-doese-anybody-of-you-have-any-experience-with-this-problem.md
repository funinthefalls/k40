---
layout: post
title: "Hi there, doese anybody of you have any experience with this problem?"
date: March 28, 2018 11:52
category: "Original software and hardware issues"
author: "Jacob Vogt"
---
Hi there,

doese anybody of you have any experience with this problem?  Short ago I aligned my Laser cause it was completly off. Everything is perfect in line now I thought. 

BUT The Raster Engravings got blury. 

I found out how it happens: 

When the Laser goes This way <s>> It engraves over the allready engraved</s>

<s>When the Laser goes this way <</s> It engraves one step lower



So this direction <s>> always goes over an area that <</s> direction always has been on.



Thats why on the lower picture it looks loke it got engraved twice.



Interestingly this effect does not happen in the upper left corner and is the worst if the laser is far ot on the right. 



Does anyone of you have an Idea how to fix that?



![images/352a72280f3e65d6e1162df4ad73cc45.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/352a72280f3e65d6e1162df4ad73cc45.jpeg)



**"Jacob Vogt"**

---
---
**Joe Alexander** *March 28, 2018 11:58*

do you have a laser head that has an air assist nozzle? you may be clipping the beam as it exits, causing a reflection that is slightly off.


---
**Jacob Vogt** *March 28, 2018 12:15*

Now I found it out  loose screws that bond the sled of the x axis on the beam.... I forgot to thighten them yesterday.



Locking the sled and trying to wiggle it made me aware...




---
**Joe Alexander** *March 28, 2018 12:50*

least you found it :P


---
*Imported from [Google+](https://plus.google.com/110308991421747979434/posts/akgQqN8trWU) &mdash; content and formatting may not be reliable*
