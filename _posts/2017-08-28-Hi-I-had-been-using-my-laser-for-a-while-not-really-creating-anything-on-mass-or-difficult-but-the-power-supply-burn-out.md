---
layout: post
title: "Hi, I had been using my laser for a while, not really creating anything on mass or difficult but the power supply burn out"
date: August 28, 2017 20:19
category: "Hardware and Laser settings"
author: "Beth Nixon"
---
Hi,



I had been using my laser for a while, not really creating anything on mass or difficult but the power supply burn out. I have fixed that, replacing it with, what seems to be a better supply from ebay.



Unfortunately, I have a new problem. Once I take the power dial past about half way, the laser seems to get weaker. I have checked the tube out, and it seems to get brighter, but when it hits the wood, it just inst as powerful. 



I have done some testing and if you look at the attached image shows that when I get to 7 or 8 (on my makeshift scale, Also attached.) Its most noticeable on the bottom 7 and 8, 10 ms and 3 passes of the laser). If I crank the dial too 13, I get almost no output., but both the dial and the brightness of the laser say otherwise....



so I have a few questions,



1. What may have caused the power Supply to have burnt out (the High tension lead burnt through)



2. what could now be causing the weakening laser, the more power I pump into it over, 6 on the scale, the laser weakens



3 based on the attached Scale image, would 4 on the scale, normally be about 12 mA?





Additional notes, 

I am using 3 mm mdf to cut with and it takes me at least two passes to cut through the wood .



Thanks in advance for your help.



![images/08bc685ca41ed61fbc91bb7272be0410.png](https://gitlab.com/funinthefalls/k40/raw/master/images/08bc685ca41ed61fbc91bb7272be0410.png)
![images/c95227aac3ea0a72e9a51652dfcccf83.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c95227aac3ea0a72e9a51652dfcccf83.png)

**"Beth Nixon"**

---
---
**HalfNormal** *August 29, 2017 13:06*

Clean the lenses and check alignment. I have had issues from one day to next even though I have not touched anything.


---
**Nate Caine** *August 29, 2017 13:50*

What did you actually <i>observe</i>?  

Did the higher dial settings produce more current on the meter?

Did you see any arcs or Corona discharge?  Hear any snaps or sizzle?



<b>High Voltage problems are very dangerous.</b>  

Since you already had the HV lead burn thru, could this be happening again?  

Did you find the cause of that past problem?  (Sharp metal edge cut the wire?)



Is the anode pin on the laser tube well insulated?

Does the problem change with the laser tube hood open?



-------



As an aside, <b>MDF</b> is notoriously difficult.  It varies from lot to lot, manufacturers, and depends on what wood scrap was used to create it.  (Type of wood, moisture content, etc.).  The tricks you learned one week may not apply to a different batch the next.






---
**Mark Brown** *August 29, 2017 22:35*

It looks to me like your bed might be leaning, so the right side of the engraving (6-8) is sitting too high/low and is out of focus.


---
**Phillip Conroy** *September 01, 2017 20:39*

If you have used laser with more than 15ma you will have shortened it's life ,if you have used  more than 20ma  used up tube life much quicker, over 25 ma than buy new tube . These tubes are not true 40 watt tube as they are not long enough, more like 35 watt,th more power you use over 15 ma the shorter the tubes life will be


---
**Beth Nixon** *September 01, 2017 21:04*

Hi half normal. 



I have recently cleaned my mirrors and realigned my laser. I will be replacing the focusing lens soon too. But I don't think that's the problem. 



Thanks though.


---
**Beth Nixon** *September 01, 2017 21:09*

Nate, 

I did observer a higher ma on the meter and the laser was brighter, it was just when it hit the wood it seemed less powefull. The problem does persist with the tube his open.



There where no arcs but I am not sure what the corona discharge would be. And there where no snaps or sizzles. 



're the original problem with the psi, it was an internal lead that arc'ed rather than a sharp edge. I will attach an image of the wire. It's the white wire that arc'ed. I did try to fix it( hence it not being a chared mess)... but quickly realised you don't mess with that cable...  



The anode tube, it that where the red wire attached to the tube it's self? 

![images/084c9adb988f0cbc31347c605f7ea3fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/084c9adb988f0cbc31347c605f7ea3fd.jpeg)


---
**Beth Nixon** *September 01, 2017 21:11*

Mark. I will get a spirit level on it. Have not tried realigning the bed as yet. I will let you know if that helps


---
**Beth Nixon** *September 01, 2017 21:12*

Phillip, 

That is sound advice and I will not be going above 4 on my scale .... how best to tell if the tube is on its way out? I don't think it is dying yet, but if like to know what too look out for... 


---
**Phillip Conroy** *September 01, 2017 23:23*

I have had 3 tubes slowly loose cutting power over a week

K,also instead of nice purple light inside tube,it changes to white


---
*Imported from [Google+](https://plus.google.com/117408894543986828444/posts/3CugN68wBo2) &mdash; content and formatting may not be reliable*
