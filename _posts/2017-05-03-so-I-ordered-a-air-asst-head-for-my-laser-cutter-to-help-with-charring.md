---
layout: post
title: "so I ordered a air asst head for my laser cutter to help with charring"
date: May 03, 2017 00:27
category: "Air Assist"
author: "Chris Hurley"
---
so I ordered a air asst head for my laser cutter to help with charring. I got this one from light object. Anyone have experience with it?  18mm Laser Head w/Air Assist. Ideal for K40 Machine. I think it should be good from the other reviews I've seen. 





**"Chris Hurley"**

---
---
**Anthony Bolgar** *May 03, 2017 00:53*

I use that exact head on all 3 of my lasers (2 K40's and a RedSail LE400) They are a really well designed head that really improves the quality of cut. You should be very pleased with it once you have it installed. Did you buy the matching lens for it? (The stock K40 is only 12mm in diameter, this head uses an 18mm diameter lens)


---
**Chris Hurley** *May 03, 2017 00:57*

Hummmm nope....thought it was the same.....guess I'll order that next. 


---
**Chris Hurley** *May 03, 2017 01:00*

I guess I can cut a spacer till the other lens gets here. 


---
**Martin Dillon** *May 03, 2017 01:27*

I have the same head and I just put the smaller lens in the center and tightened it down with no spacer.  Seems to work fine.  I have since ordered 3 mirrors and a lens and they arrived last Friday but I haven't had time to replace them.  "If it ain't broke don't fix it".



When I swapped out the head for the air assist, it looked as if there had been some sort of grease inside the original head and the mirror was discolored.  I tried to clean it with acetone but  there was no change.  I was wondering how people tell how strong the laser is at the work surface.  Before I switch out all the mirrors and lens I was going to do a series of test cut and then compare the results with new setup.  Any suggestions?


---
**Anthony Bolgar** *May 03, 2017 01:38*

I use a laser power meter that I purchased from [bell-laser.com - Mahoney CO2 Laser Power Meter Probe 0-200 watts &#x7c; Laser Repair , Upgrades and Accessories - Bell Laser](https://www.bell-laser.com/product-page/mahoney-co2-laser-power-meter-probe-0-200-watts) It is $115.00 


---
**Josh Speer** *May 05, 2017 04:38*

One thing about the LO ahead, it comes assembled. It for so well together, that I didn't realize that the threads unscrew to install it. Luckily I searched online enlarging opening the hole in the chassis where it mounted.


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/4ToFrT4mq3q) &mdash; content and formatting may not be reliable*
