---
layout: post
title: "Hi All, Just got my K40 this week, finished up cutting back the exhaust shroud, aligning mirrors"
date: January 14, 2017 17:54
category: "Discussion"
author: "Robin Sieders"
---
Hi All,

Just got my K40 this week, finished up cutting back the exhaust shroud, aligning mirrors. I ended up having to shim the back left corner to bring everything into alignment. Having an issue when doing engravings from corel-laser. I was able to do engravings without issue up until last night, now whenever I send an engraving from the software the laser fires once quickly at the start of the job, then nothing. Turning the dial to adjust current doesn't seem to make a difference when the engraving is active, but, if I hit the test fire button while it's engraving the meter reads the correct current. I also have no issues when doing a cutting operation in corel-laser, the laser fires at the current set by the dial. I'm thinking maybe I bumped a setting somewhere in the engraving window, but I've tried every visible setting I can think of? Any thoughts? I also have made sure my board serial number and model are correct in the settings, though, even after entering them the company name is blank in the engraving/cutting settings window, not sure if that may be causing some of the weirdness.

Thanks for your help







**"Robin Sieders"**

---
---
**Robin Sieders** *January 14, 2017 18:12*

Here's my settings page for engraving.

![images/ecba95a48bd08c9617240561ae0196ec.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ecba95a48bd08c9617240561ae0196ec.png)


---
**Robin Sieders** *January 14, 2017 18:14*

And my machine settings

![images/f9d54c3deda6e0b08938ee9ebb130f5d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f9d54c3deda6e0b08938ee9ebb130f5d.png)


---
**Robin Sieders** *January 14, 2017 18:25*

Ended up being an easy fix! I had the line width of the shape I was trying to engrave set to hair-width. Changed it to 1.0 pt and it engraved no problem!


---
**Ned Hill** *January 14, 2017 18:42*

If you are just engraving a thin line shape it's actually more efficient to do a vector engrave using the cut function rather than the raster used for engrave.  Just have to play with the speed and power to get right engrave depth.   Keep in mind that with vector cutting/engraving corellaser will do both sides of the line if the width is greater than 0.01mm.  Something to avoid when cutting but not necessarily for vector engraving.


---
**Robin Sieders** *January 14, 2017 19:24*

**+Ned Hill** I didn't even think about doing it that way, but that's definitely a great idea.


---
*Imported from [Google+](https://plus.google.com/+RobinSieders/posts/79mWkmLwJPp) &mdash; content and formatting may not be reliable*
