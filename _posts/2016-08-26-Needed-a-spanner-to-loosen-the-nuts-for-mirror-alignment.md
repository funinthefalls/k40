---
layout: post
title: "Needed a spanner to loosen the nuts for mirror alignment..."
date: August 26, 2016 03:15
category: "Object produced with laser"
author: "Alex Krause"
---
Needed a spanner to loosen the nuts for mirror alignment... couldn't find one that small in my tool box so I made one :) #laserweb

![images/acf31a5bcba5df74a686c7e866a51835.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/acf31a5bcba5df74a686c7e866a51835.jpeg)



**"Alex Krause"**

---
---
**HalfNormal** *August 26, 2016 12:36*

That is cheaper than the $20 I spent on a set of small metric wrenches!


---
**Christoph E.** *August 29, 2016 21:12*

Damn nice Idea. But $20 is still cheap... paid like €150 (but everything is metric here so that's okay) :D


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/T6ERGLv8XyX) &mdash; content and formatting may not be reliable*
