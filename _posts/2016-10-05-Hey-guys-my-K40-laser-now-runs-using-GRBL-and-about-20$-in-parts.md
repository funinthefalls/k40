---
layout: post
title: "Hey guys, my K40 laser now runs using GRBL and about 20$ in parts"
date: October 05, 2016 08:18
category: "Modification"
author: "Timo Birnschein"
---
Hey guys, my K40 laser now runs using GRBL and about 20$ in parts.

I spend the last couple of days designing a simple plug and play PCB to replace the rather unusable moshi board (mostly because of it's interfacing software because the board itself is pretty solid in my opinion).

I unsoldered and reused the main power connector as well as the flatband connector from the original board.

GRBL supports setting the laser power (PWM) in software and allows easy access because it understands standard gCode commands (besides some nice configuration options).

And the best so far: It works!

So much for the positives.



Now the negatives:

1. So far I haven't had much luck short vectors and LaserWeb3. It seems arcs and vectors in general don't come out in an ideal path to be lasered. The head goes back and forth from vector to vector. It also engraves small arcs really really slow and it seems like it sends them as little tiny individual vectors instead of arks. Circles, however, work perfectly fine! Maybe it is me dxf file that is the cause?



2. 2.5D engraving works but is painfully slow and the laser power doesn't seems to be adjusted on the fly but rather from micro-vector to micro-vector. Which makes sense. But it causes the machine to be super slow, quite similar to the small 1mm arcs that I put into corners of laser cut parts to make them fit together without filing.



I slightly modified the latest version of GRBL directly from github ([https://github.com/grbl/grbl](https://github.com/grbl/grbl)) and used my 3D-Printer with a mill attachment to mill the Eagle board.



Does anyone have any hints or ideas how to configure GRBL for fast look-ahead / feed forward engraving for cutting and 2.5D engraving?



![images/510c7150db2ef22883bd5fdc7331cc5d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/510c7150db2ef22883bd5fdc7331cc5d.jpeg)
![images/b95e12b102fc013c21d2453a8cff48ec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b95e12b102fc013c21d2453a8cff48ec.jpeg)
![images/0369110d76d62ce6afd3bacdb934f888.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0369110d76d62ce6afd3bacdb934f888.jpeg)

**"Timo Birnschein"**

---
---
**Walter White** *October 05, 2016 11:33*

Nice job, but why you haven't used arduino uno + grbl shield like this [http://blog.protoneer.co.nz/arduino-cnc-shield/](http://blog.protoneer.co.nz/arduino-cnc-shield/) ?


---
**Ariel Yahni (UniKpty)** *October 05, 2016 12:03*

1. You need them to be polylines. In whats software are you designing? good info here [github.com - LaserWeb3](https://github.com/openhardwarecoza/LaserWeb3/wiki/Workflow:-Existing-DXFs-doesn%27t-import-OK)

[https://github.com/openhardwarecoza/LaserWeb3/wiki/Workflow:-Existing-DXFs-doesn%27t-import-OK﻿](https://github.com/openhardwarecoza/LaserWeb3/wiki/Workflow:-Existing-DXFs-doesn%27t-import-OK%EF%BB%BF)


---
**HalfNormal** *October 05, 2016 12:43*

**+Timo Birnschein** Here is the latest GRBL version that now has laser control built in . The link goes to the x-carve forum and has instructions on how to configure. [https://discuss.inventables.com/t/j-tech-laser-integrated-with-x-carve-grbl-inventables-master-version-1-0c/28493](https://discuss.inventables.com/t/j-tech-laser-integrated-with-x-carve-grbl-inventables-master-version-1-0c/28493)

[discuss.inventables.com - J Tech Laser integrated with X-carve grbl (Inventables/Master version 1.0c) - Upgrades - Inventables Community Forum](https://discuss.inventables.com/t/j-tech-laser-integrated-with-x-carve-grbl-inventables-master-version-1-0c/28493)


---
**HalfNormal** *October 05, 2016 12:44*

**+Timo Birnschein** are your PCB files posted anywhere?


---
**Timo Birnschein** *October 05, 2016 17:48*

**+Walter White** From my point of view those shields are very good in getting things going. I prototyped this board on one! But I believe, making things tidy is also reducing the chance of making mistakes. I just needed to unscrew one of the cables from the power supply of the K40 and add two little wires for laser control. The rest of completely plug and play. I saw some of the builds with a standard shield and adapters to the ribbon cable and it looks like a nightmare to me... but to be fair, I still also build nightmare cnc-machines but I just don't like working with those.


---
**Timo Birnschein** *October 05, 2016 17:50*

**+HalfNormal** I want to publish the files after I have cleaned up some minor things! The board is supposed to be single sided with a bunch of wired on the top of the board but it can also be manufactured as double sided board. I will post the link here - it will likely be github or google drive.


---
**Timo Birnschein** *October 05, 2016 18:05*

**+HalfNormal** Regarding the J-Tech firmware: I love that they put in the extra effort to modify GRBL for real laser support. But I can't find the source. I need to invert the spindle ON pin in order to not burn out my CO2 laser on first try. I also use pin D11 (LaserPWM) as well as pin D13 (#LaserEnable) instead of D12 (was spindle, now open).

Having said that and also that I have searched for quite a while to get my hands on the source code of this open source grbl... I hate to see that J Tech seems to have modified the grbl source without providing it to the public in order to promote their own PicLaser software. That PAINS me. GRBL is a great piece of software made by hobbyists(!!) along with all these other systems like Marlin and the like and all manufacturers that use them usually publish their modified sources for users to do more hacking or for grbl to merge the mods into their code base! This is a great mod for lasers and we can't use it because we need extra hardware (inverter) in order to make it work....


---
**Timo Birnschein** *October 05, 2016 18:07*

**+Ariel Yahni** Thank you! I'm using SolidWorks for my design and I always have problems with my DXF files! I'll check if there is any options similar to the ones described in your linked article! Interesting stuff! I wish LaserWeb3 would integrate a path optimizer for those "broken yet standard" dxf and svg files. It would be totally worth it and would make LaserWeb my absolute goto software!


---
**HalfNormal** *October 05, 2016 18:34*

**+Timo Birnschein** if you ask Larry in the forum, he will supply the uncompiled code so you can change the config.h to invert the logic on the enable pin. I agree about Jtech. I have bugged Jay for a long time to supply the source code. He is now working with Sonny for a better GRBL laser mix. We will see....


---
**Michael Audette** *October 05, 2016 18:48*

Check out JT Photonics GRBL 0.9  (CLOSED SOURCE BOO) or LaserInk for the fast/on-the-fly PWM update stuff.  I haven't tested it wth the K40 but it works on my Little A3 Engraver.  The code is out there for Laserbnk....


---
**Timo Birnschein** *October 05, 2016 18:55*

**+HalfNormal** Now online in its current (!) version. This one is not ideal for single sided milling due to some small pads. Especially the ribbon connector pads are tiny and rip off. It's usable though. [https://github.com/McNugget6750/K40-grbl-board](https://github.com/McNugget6750/K40-grbl-board)

[github.com - McNugget6750/K40-grbl-board](https://github.com/McNugget6750/K40-grbl-board)


---
**HalfNormal** *October 05, 2016 20:44*

**+Timo Birnschein** yea! Thanks for posting so quickly. 


---
**Timo Birnschein** *October 05, 2016 20:57*

Here is a quick video of my current state... Unfortunately, setting the dxf format to polylines didn't help so far...




{% include youtubePlayer.html id="YJK_3x_JYfk" %}
[https://www.youtube.com/watch?v=YJK_3x_JYfk&feature=youtu.be](https://www.youtube.com/watch?v=YJK_3x_JYfk&feature=youtu.be)



Here are my settings for reference. Maybe I need to change those for faster small circles...

$0=10 (step pulse, usec)

$1=25 (step idle delay, msec)

$2=0 (step port invert mask:00000000)

$3=1 (dir port invert mask:00000001)

$4=0 (step enable invert, bool)

$5=1 (limit pins invert, bool)

$6=0 (probe pin invert, bool)

$10=3 (status report mask:00000011)

$11=0.010 (junction deviation, mm)

$12=0.002 (arc tolerance, mm)

$13=0 (report inches, bool)

$20=0 (soft limits, bool)

$21=0 (hard limits, bool)

$22=1 (homing cycle, bool)

$23=3 (homing dir invert mask:00000011)

$24=200.000 (homing feed, mm/min)

$25=8000.000 (homing seek, mm/min)

$26=250 (homing debounce, msec)

$27=1.000 (homing pull-off, mm)

$100=157.575 (x, step/mm)

$101=157.575 (y, step/mm)

$102=250.000 (z, step/mm)

$110=12000.000 (x max rate, mm/min)

$111=12000.000 (y max rate, mm/min)

$112=12000.000 (z max rate, mm/min)

$120=1000.000 (x accel, mm/sec^2)

$121=1000.000 (y accel, mm/sec^2)

$122=1000.000 (z accel, mm/sec^2)

$130=200.000 (x max travel, mm)

$131=300.000 (y max travel, mm)

$132=10.000 (z max travel, mm)


{% include youtubePlayer.html id="YJK_3x_JYfk" %}
[youtube.com - GRBL on a Chinese K40 Laser - Arc speed problems and DXF issues](https://www.youtube.com/watch?v=YJK_3x_JYfk&feature=youtu.be)


---
**HalfNormal** *October 05, 2016 21:29*

**+Timo Birnschein** I asked Larry @ Inventables if he would compile a version for us with the correct logic until the source is released. 


---
**Timo Birnschein** *October 05, 2016 23:12*

**+HalfNormal** Excellent! I hope this gets us somewhere. My bigger issue remains with the arcs as posted here: [https://plus.google.com/+TimoBirnschein/posts/HTqaDAXuwMT](https://plus.google.com/+TimoBirnschein/posts/HTqaDAXuwMT)

[plus.google.com - +Peter van der Walt I love your LaserWeb3 software but I have a couple of…](https://plus.google.com/+TimoBirnschein/posts/HTqaDAXuwMT)


---
**Timo Birnschein** *October 06, 2016 01:23*

IT WORKS! I use dxf2gcode with my own preprocessor configuration in combination with GRBL-Controller and it now works like a charm! Cutting... that is. Engraving still doesn't do much useful at all.

In the GRBL firmware I changed the PWM frequency in spindle_control.c to "No Prescaling" because on fast moves, I could see the individual points. I hope it's not too fast for the power supply now but cuts nicely!

This is a 6.2mm thick piece of MDF cut at 5mm/sec with 12mA on the analogue scale and two passes.

![images/a0d9a3edef6ad3db76b851b496736dee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0d9a3edef6ad3db76b851b496736dee.jpeg)


---
**HalfNormal** *October 06, 2016 01:40*

sweet!


---
**HalfNormal** *October 06, 2016 23:44*

**+Timo Birnschein**  There has been some great ideas on how to get the 1.0c  GRBL to work with the K40 without having to recompile for now on the x-carve forum


---
**Timo Birnschein** *October 07, 2016 00:13*

I have a 0.9j version running right now that is rastering using gcode generated by LW3 and send to grbl using universalGcodeSender (because sending it through LW3 causes my Win 10 to bluescreen out randomly after some time :( **+Peter van der Walt** 



I ported all the changes that Nick Williams made on his LaserInk modified grbl firmware to be able to have planned variable laser power for every single motion command. This actually works well and the limiting factor now seems to be the sender application! I have to deactivate live scrolling in universalGcodeSender to work properly without interrupting the serial stream.


---
**HalfNormal** *October 07, 2016 00:21*

**+Timo Birnschein** I have found that PicSender is well worth the money to stream GRBL. It is rock solid and has a lot of features available. It can stream MB's of data without a hitch.

The issue with both Nick's and JTech's GRBL is that in order to raster correctly, they had to forgo the Arc geometry which makes them awful at vector graphics and cutting. The nice thing about PicSender is that you can save your GRBL settings and reflash the Arduino so that you can use standard GRBL for cutting and laser GRBL for raster.


---
**HalfNormal** *October 07, 2016 01:23*

**+Timo Birnschein**  Be very careful with Nick Williams. See this post [https://plus.google.com/107520953345127905250/posts/6CimCXby6tF](https://plus.google.com/107520953345127905250/posts/6CimCXby6tF)

Jtech did not use his code as he claims. In fact here is some info from tests that PicEngrave did on his firmware.

"FYI, both me and John tested his new grbl Firmware with a raster gcode test file and it takes 1:55 where his older version takes 1:45. BUT, the release dates are the same. :-\ "



He does not have the best reputation in the laser community.

[plus.google.com - New firmware running on arduino provides 300 mm/sec rastering. I have been…](https://plus.google.com/107520953345127905250/posts/6CimCXby6tF)


---
**Timo Birnschein** *October 07, 2016 02:03*

I just tried PicLaser & PicSender demo and I'm super impressed with the results my K40 can produce. I will consider buying the Pic software since it really seems to work quite well (even though the user interface is quite horrible...)


---
**HalfNormal** *October 07, 2016 02:06*

**+Timo Birnschein**​ I agree on the horrible interface but their support is second to none.


---
**Timo Birnschein** *October 07, 2016 02:28*

**+HalfNormal** You mean the interface as well as the support is horrible?


---
**Timo Birnschein** *October 07, 2016 02:36*

**+HalfNormal** Regarding Nick Williams... hm... Not sure what to make of it... All I can say at this stage is that after porting his mods into my slightly modified master of grbl [0.9j.20160726] my laser works like a charm. 



Now I need to figure out how to change the coordinate system in dxf2gcode because 0,0 is bottom left but the K40 has 0,0 at the top left. This mirrors all my cuts.


---
**HalfNormal** *October 07, 2016 02:50*

**+Timo Birnschein** I just wanted to warn you because Nick has taken credit for other people's work. If you are using laserweb to generate the gcode, I seem to remember a fix for the 0, 0 issue but do not remember where to find the answer. Check the laserweb github.


---
**HalfNormal** *October 07, 2016 12:53*

**+Timo Birnschein** I knew it was there! Thanks Peter!


---
**Timo Birnschein** *October 07, 2016 16:49*

Here is the complete repository for the board as well as the grbl modifications on the current master 0.9j. It also implements Nick Williams planned realtime spindle control that I ported over from his repository. Using PicSender this grbl engraves like a charm at up to 50mm/s at a 45degree angle. I didn't try much faster. At 100mm/s it stutters heavily so I didn't push it any further than 50mm/s for stability.



[https://github.com/McNugget6750/K40-grbl-board](https://github.com/McNugget6750/K40-grbl-board)

[github.com - McNugget6750/K40-grbl-board](https://github.com/McNugget6750/K40-grbl-board)


---
**Timo Birnschein** *October 08, 2016 17:30*

**+Peter van der Walt** I saw that setting and I tried it visually but the point of origin didn't change, only the image location... Let me check what the gCode looks like, maybe the origin is changed when I do that. Thanks!


---
**Timo Birnschein** *October 08, 2016 18:11*

**+HalfNormal** I found a little tool that I heavily modified yesterday: [http://microforge.de/img2gco/](http://microforge.de/img2gco/)

Try it for engraving and let me know. I produces very nice results on my K40!

[https://github.com/McNugget6750/img2gco](https://github.com/McNugget6750/img2gco)

[microforge.de - Laser Min Power [0-255]: Laser Max Power [0-255]: Laser 'Off ...](http://microforge.de/img2gco/)


---
**HalfNormal** *October 08, 2016 21:51*

**+Timo Birnschein** I look forward to trying the software. I used it way back when but have not in awhile. Thanks for updating it to work with our systems. My K40 is down for some mods so it might be a few before I have a chance to try it out.


---
**Nick Williams** *October 10, 2016 07:06*

**+HalfNormal**  Not sure what you are referencing as "Taken credit for other peoples work" If you could provide links it would be appreciated as I consider my reputation very important.



The main thing that I have provided to the community is the real time spindle control that Tim has ported over from my repository. I was the first to implement this on the grbl. To my knowledge the only other version used the z axis and some additional electronics. I have personally spoken with Jay at JTech laser and he is a really nice guy, It was he who indicated to me that his version was based of what we have done, this is what the open source community is all about. I am not sure Jay is on this form but I would hope if he sees this he would confirm my claims.



If you visit my repository you will see that it is over 2 years old.

[github.com - LaserInk](https://github.com/nickw89509/LaserInk/tree/master/grbl_0.9).



I have continued to push the extremes of what can be done with the grbl firmware the current modifications will support a streaming mode that will send an entire raster line as a single GCode block the post that you reference above shows this. When released will provide a ten fold performance increase to what is currently possible with the grbl firmware. 



FYI on arcs:

Our currently released version of the firmware does support arcs. The code base that I am using to implement the streaming mode does not only because of code space needed to implement the new streaming mode. 



A note on arcs for both the smoothie and grbl firmware both must convert arcs into small lines on grbl this is a firmware setting not sure how it is controlled on the smoothie, but would expect it is part of the configuration. I have taken a different approach by doing this CPU intensive work when generating the GCode if you are using the object works software.  



I am not angry, I would just like to know the origin of this information so that I can respond. 




---
**Nick Williams** *October 10, 2016 07:48*

One other note if you look at the comment history 

[http://www.instructables.com/id/Shapeoko-2-Arduino-UNO-R3-grbl-9g-8bit-Raster-Phot/](http://www.instructables.com/id/Shapeoko-2-Arduino-UNO-R3-grbl-9g-8bit-Raster-Phot/)

Which was a very popular post by the guys from PicEngrave and go back two years ago look for nickw89509. You will see that it is I who introduce them to using the spindle command to control the laser.

[instructables.com - Shapeoko 2, Arduino UNO R3, grbl 9g, 8bit Laser Diode Photo Engraving](http://www.instructables.com/id/Shapeoko-2-Arduino-UNO-R3-grbl-9g-8bit-Raster-Phot/)


---
*Imported from [Google+](https://plus.google.com/+TimoBirnschein/posts/3pJQwcjEZBL) &mdash; content and formatting may not be reliable*
