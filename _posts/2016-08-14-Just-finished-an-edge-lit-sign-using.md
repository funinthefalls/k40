---
layout: post
title: "Just finished an edge lit sign using ..."
date: August 14, 2016 04:53
category: "Object produced with laser"
author: "Alex Krause"
---
Just finished an edge lit sign using #Laserweb... so much easier to align the engrave and cut paths than with the stock software



![images/6d90ec086552a9a905f81eb2e14e67fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d90ec086552a9a905f81eb2e14e67fc.jpeg)
![images/b467cc59a8cc42186c0dd33256cb6f14.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b467cc59a8cc42186c0dd33256cb6f14.jpeg)

**"Alex Krause"**

---


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/BQyanW6GfGJ) &mdash; content and formatting may not be reliable*
