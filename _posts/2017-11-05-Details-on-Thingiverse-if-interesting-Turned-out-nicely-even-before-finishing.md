---
layout: post
title: "Details on Thingiverse if interesting: Turned out nicely, even before finishing"
date: November 05, 2017 23:00
category: "Object produced with laser"
author: "Arjen Jonkhart"
---
Details on Thingiverse if interesting: [https://www.thingiverse.com/thing:2627967](https://www.thingiverse.com/thing:2627967)



Turned out nicely, even before finishing. 



![images/a81896175bf1356773e5796e29509173.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a81896175bf1356773e5796e29509173.jpeg)



**"Arjen Jonkhart"**

---
---
**Bill Keeter** *November 16, 2017 18:08*

This is great. I'm probably going to make one for each of my kids. Hmm... only have 2.5mm wood at the moment, i'll have to visit Lowes for some 5mm (or maybe Amazon, lol)


---
*Imported from [Google+](https://plus.google.com/109364066707746072621/posts/DG5GZXYMHH1) &mdash; content and formatting may not be reliable*
