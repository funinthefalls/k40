---
layout: post
title: "Ive got the distilled water I need for the cooling system"
date: January 22, 2018 00:49
category: "Original software and hardware issues"
author: "James Rivera"
---
I’ve got the distilled water I need for the cooling system. But now I’m wondering about the white residue in the tubing that came with the K40 laser. Should I be concerned about it? Related question: it is in my garage and while it rarely snows here in the Seattle area, the temperatures can sometimes drop below freezing at night. Any suggestions to help mitigate freezing water destroying my laser tube? I know I cannot use anti-freeze. Would it be safe enough to blow compressed air through the tubing when I’m done to purge most of the water? How do you folks handle such situations?



![images/91098fd575ade1f313124ea70ea79ef4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/91098fd575ade1f313124ea70ea79ef4.jpeg)
![images/ebacaa1597334459dcd5eebd1dba88d0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ebacaa1597334459dcd5eebd1dba88d0.jpeg)

**"James Rivera"**

---
---
**Ned Hill** *January 22, 2018 02:13*

You can try flushing the system a couple if times but personally I would get new tubing.  I've seen where someone, in a similar situation to you, would keep the pump running and used a light bulb to keep the water above freezing.  You could also probably use an aquarium heater with the pump running  Just keep in mind to drain the tube if there is a power outage at the same time as below freezing temps.  As long as most of the water is out then it should be fine.


---
**Don Kleinschnitz Jr.** *January 22, 2018 02:40*

I cleaned the white stuff from my tube by adding white vinegar to the water and letting it run overnight as recommended by **+HalfNormal**, worked great.


---
**James Rivera** *January 22, 2018 03:33*

Thanks for the reply guys! Ok, so it sounds like this plan would be good: 1) just use some tap water mixed with white vinegar, run it through the system for a few hours, drain it (probably with some compressed air), then 2) put in the distilled water with about a capful of plain old Clorox bleach (to keep algae from growing) and run that for a while to make sure no bubbles are present, and in the meantime 3) order an aquarium heater that I can turn on if the forecast says the temperature might get too low. Sound like a plan?


---
**Steve Clark** *January 22, 2018 03:48*

I wouldn't use the bleach...I use Tetra AlgaeControl


---
**James Rivera** *January 22, 2018 04:19*

**+Steve Clark**  Why not use (a very small amount of) bleach? Please explain.


---
**Jim Coogan** *January 22, 2018 05:02*

I live in Monroe and put a fish tank heater in my water with the pump.  Both have been running 24x7 for the last 2+ years with zero issues.  


---
**Don Kleinschnitz Jr.** *January 22, 2018 12:24*

**+James Rivera** Algecide has the least impact on the waters conductivity. I would expect that you will have to tilt the machine to remove the bubbles.



Here is the background: [donsthings.blogspot.com - K40 Coolant](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
**Ned Hill** *January 22, 2018 13:48*

**+Jim Coogan** what's the wattage of the heater you are using? 50W?


---
**Steve Clark** *January 22, 2018 16:51*

**+James Rivera**  Don's report is the reason. Thks' Don.


---
**Claudio Prezzi** *January 24, 2018 08:20*

Also be aware that bleach or vinegar could probably etch the inner glass surface of the tube when used for longer time. You would get blind glass like sometimes in a dish washer.


---
**James Rivera** *January 25, 2018 00:01*

Don's report is why I thought bleach was ok. I'll get the Tetra Algae Control. It looks pretty cheap:

[amazon.com - Robot Check](https://smile.amazon.com/gp/product/B001D4TNH8/ref=ox_sc_act_title_1?smid=ATVPDKIKX0DER&psc=1)


---
**James Rivera** *January 25, 2018 00:02*

I have no idea why it says, "Robot Check". That is the link to Tetra Algae Control on Amazon.


---
**Steve Clark** *January 25, 2018 00:55*

That's the right stuff. I have no idea about the 'robot check' either. The link worked though.


---
**Sebastian C** *February 02, 2018 16:45*

The white stuff is as some have already pointed out probably lime / calcium carbonate from your hard tap water. It should go away with little acid (citric acid for preference). With higher temperature the precipitation will increase, so most fallout will be in/after the laser tube.

In the industry deionized water is often used for closed cooling solutions, but you have to be careful, because it could cause corrosion due to lack of buffering function of the missing ions. For the K40 , there are only glass, silicone/plastics and maybe a bit brass from couplings. 

Since corrosion is no issue for our K40, the conductivity should also be no issue as long as you don't throw in pure salt. 



The microbiology will come automatically as soon as you have contact to the (non sterile) air, so even the purest water would not help as long as you could not guarantee a complete sterile cooling circuit. Algea will show up on bright spots (photosysthesis), so try to use opaque hoses and containers.



Bleach (containing hypochlorite) is also a good disinfectant, but comes often with detergents which could cause even more bubbles.

Also oxidative compound as clorine, ClO2, hypochlorite, peroxides will rapidly increase the decay of your plasic components!



My recommendation: 

clean water (preferably deionized or softened) to avoid lime scaling.

Closed circuits for less ingress of dirt and microbiology

Dark/opaque tubing

regular water changes 

fouling: anti-fouling biocides , algea control or (pure) disinfectants

bubbling: defoamers (e.g. siloxanes) or wetting agents



source: water treatment engineer


---
**James Rivera** *February 02, 2018 18:09*

**+Sebastian C** Good info, thanks!  Regarding the white stuff, I have yet to run water through the system--the white stuff came with it. I'd also say they would have to be using some pretty damn hard water to make that much of it. So unless I got a heavily used laser tube (!) I think something else must be the reason for it.


---
**James Rivera** *February 02, 2018 18:12*

In other related news, I have a brand-new 5 gallon bucket and lid from Home Depot, 5 gallons of distilled water, and some Tetra Algae control.  I also have 10 feet of new silicon tubing from LightObject in case I feel brave enough to change the tubing. EDIT: Now I just need to find the time, which is to say probably not until after Valentine's day.  :-(


---
**James Rivera** *February 02, 2018 18:13*

Random thought/question: could the white stuff be residue from silicone caulk or similar sealant?  If so, then it would be firmly adhered to the tubing by now and is perhaps much ado about nothing, right? EDIT: I don't think there are any large "chunks" that could clog things up if they detached from the tubing.


---
**Ned Hill** *February 02, 2018 18:22*

No white stuff is mostly likely mineral deposits due to the laser manufacturer using tap water during testing. We have seen videos of some of these Chinese places making tubes and it’s not pretty. 


---
**James Rivera** *February 02, 2018 19:09*

**+Ned Hill** Yikes. I wish I could say I was surprised. Any tips/gotchas on replacing the silicone tubing?


---
**Ned Hill** *February 02, 2018 19:17*

**+James Rivera** replacing the tubing should be straight forward.  The glass has barbed fittings so just slide it on all the way and it should be good to go.  Make sure to give yourself plenty of extra length so you can easily tip the machine to remove the air bubbles.


---
**Steve Clark** *February 02, 2018 19:34*

I happen to live near Lightobject so one day when I was there I picked up their tubing.



[www.lightobject.com/5X8-Silicon-Flex-Tube-for-Air-Assistance-or-Water-Cooling-P776.aspx](http://www.lightobject.com/5X8-Silicon-Flex-Tube-for-Air-Assistance-or-Water-Cooling-P776.aspx)



I also added these tie wraps. Don't over tighten. just firm is good enough.



[https://www.mcmaster.com/#tie-wraps/=1bece87](https://www.mcmaster.com/#tie-wraps/=1bece87)




---
**Sebastian C** *February 03, 2018 13:24*

**+James Rivera** : as Ned Hill pointed out is is probably mineral deposits, but I noticed that my silicone tubes are getting blunt(er) even after cleaning with acid, so there could also be residues from that cheap china tubes.

Best solution: replace with higher grade  materials: silicone or ptfe/teflon 

Silicone should be cheap and sufficient for our sweet K40 needs.


---
**James Rivera** *February 03, 2018 18:47*

**+Sebastian C** Agreed.  As I noted above, I already bought 10 feet of silicone tubing from LightObject on their Amazon store:

[amazon.com - 8X12 Silicon Flex Tube for CO2 Water Cooling(10ft): Amazon.com: Industrial & Scientific](https://smile.amazon.com/gp/product/B00HV0ETZE/ref=oh_aui_detailpage_o06_s00?ie=UTF8&psc=1)


---
**Steve Clark** *February 03, 2018 19:29*

James, Call them today (or monday morning) at 1-916-572-6080. You posted you got 8X12mm. That is the wrong size. You need 5X8mm for a K40.


---
**James Rivera** *February 03, 2018 19:46*

**+Steve Clark** I received it Jan. 17th. I'll check the diameter soon (probably Sunday) and if it is incorrect, I think I still have time to return it via Amazon (I hope). Thanks.


---
**James Rivera** *February 03, 2018 19:51*

**+Steve Clark** Actually, I think you may be mistaken:

![images/4b29c45bcc41338fc36177367b872577.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b29c45bcc41338fc36177367b872577.jpeg)


---
**Steve Clark** *February 03, 2018 21:55*

**+James Rivera**  YOU ARE CORRECT ... I was wrong... I should have double checked on my old invoice .  



"

8X12 Silicon Flex Tube for CO2 Water Cooling

LSR-WTUBE8X12	$0.85	16	$13.60"


---
**James Rivera** *February 05, 2018 00:26*

Actually, it looks like the new tubing is too large (I compared it to the existing tubing). Moot point though. I decided to just use the existing tubing and it is working fine. I've already done my test burns and aligned the beam. I just need to do a ramp test and make sure I've got the focus where it needs to be, then I'll be ready to actually try cutting a real project!


---
**Steve Clark** *February 05, 2018 00:48*

When you say it's too large… is that because it goes on with only a very little stretching? 



Mine was like that. It does expand a very little but just feels not trustworthy. That is why I used the tie wraps, just to snug it up a bit. I have no leaks or tubing popping off problems.


---
**James Rivera** *February 05, 2018 03:42*

I didn’t even try. I wanted to make sure the darn thing works before I go mucking around and break it! The good news is, it works. I’ve already successfully done a vector engrave and raster engrave. I tried to do a vector cut, but it didn’t really cut though the thin cardboard—it just burned it a bit—even at 80% power. I haven’t determined the focus point yet, so no big deal. I’m happy!! 😃 


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/hEgN6x1NaKS) &mdash; content and formatting may not be reliable*
