---
layout: post
title: "im thinking about doing some upgrades as i fix my k40 how ezy is it to put on a water switch"
date: July 26, 2016 00:36
category: "Discussion"
author: "beny fits"
---
im thinking about doing some upgrades as i fix my k40 

how ezy is it to put on a water switch





**"beny fits"**

---
---
**I Laser** *July 26, 2016 01:14*

Flow switch? 



Pretty easy if you've got an analog machine, just connect it to the laser enable button. 



If you have a digital k40 it might be a bit more of a pain. There's a pin on the board that if you're lucky they've used a jumper, on mine they'd just dumped a bunch of solder on there. :\


---
**beny fits** *July 26, 2016 01:44*

Awesome i will order 1 now ☺


---
**Pippins McGee** *July 26, 2016 05:40*

**+beny fits** which one is best to order?

I have heard issues with some not being sensitive enough that the flow of these weak pumps does not trigger it?


---
**I Laser** *July 26, 2016 06:58*

Unfortunately the switches I've ordered off eBay were not sensitive enough. My (dubious) fix was to pull them apart, clip the spring and mount them upside down.


---
*Imported from [Google+](https://plus.google.com/116814442468846937788/posts/Hu3ob4fLPYL) &mdash; content and formatting may not be reliable*
