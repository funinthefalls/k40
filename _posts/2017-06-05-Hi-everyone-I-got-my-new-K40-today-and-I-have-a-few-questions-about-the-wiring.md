---
layout: post
title: "Hi everyone, I got my new K40 today and I have a few questions about the wiring"
date: June 05, 2017 05:26
category: "Discussion"
author: "Bromide Ligand"
---
Hi everyone, I got my new K40 today and I have a few questions about the wiring. The unit has a standard 3 pin male socket at the back, in addition there is a grounding terminal. The unit also comes with a cable which has a Female socket with 3 Ping. But the Thing is, on the other end of this cable where you plug it into your wall the top pin is missing.



So Can I just use a similar cable I have which has this 3rd pin and Forget about the grounding terminal ?I will post some images of the cables.

other images [https://imgur.com/a/VUS5V](https://imgur.com/a/VUS5V)









**"Bromide Ligand"**

---
---
**Bromide Ligand** *June 05, 2017 05:32*

the top one is the Chinese plug with the missing top pin which is normally used for grounding. THe bottom one is the one I have with all 3 pins./



![images/e93b3f12d51b3eea8a2aa7cebb5eba56.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e93b3f12d51b3eea8a2aa7cebb5eba56.jpeg)


---
**E Caswell** *June 05, 2017 09:13*

**+Bromide Ligand** You could use another cable as the top pin should be a grounding pin, but you will need to check it out prior to plugging everything in to confirm that it is a grounding pin.

What I would say from my experience of the 220v is ground the machine from the rear point as well.  Helps to reduce some of the EMF that these machines seem to struggle with. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2017 09:17*

I guess as long as the cable is rated for the same amps then maybe it's suitable. But I am not an electrician or very knowledgeable in that, so hopefully someone more knowledgeable comes along.


---
**Don Kleinschnitz Jr.** *June 05, 2017 11:29*

**+Bromide Ligand** you can use your plug just insure that you have "Line" and "Neutral" on the correct pin. I am assuming that matters where you are located. Then test that ground in the plug is connected internal to the K40 to the frame and the ground pin on the LPS. 

I would test from the plug (while plugged in) all the way to the termination point with a DVM.

While you are at it test that all the covers are at safety ground. 



The ground pin on the back is useful if your house grounding does not provide a low impedance to earth ground. Normally you can ignore it.


---
**Mark Brown** *June 05, 2017 12:18*

I took that grounding pin loose and cleaned paint from under it on mine. Since that's the main (only?) point where the ground connects to the body. 


---
**Don Kleinschnitz Jr.** *June 05, 2017 12:31*

**+Mark Brown** making sure that the ground socket is a good connection is a good thing to do. 

The safety ground pin on the 3 pin AC plug should also be connected to the frame. Usually there is a wire from the ground lug in the AC input plug connected to one of the screws in that receptacles mount. If not I would add one :).

The fundamental is to insure that the operator accessible surfaces have a shorter path to earth than your body and any leakage from the electronics finds a path to ground rather than through you ...  


---
**Claudio Prezzi** *June 05, 2017 14:23*

**+Bromide Ligand** The top plug is not missing the ground pin, it is the german (european) standard where the ground pin is in the wall plug. Those cables are replacable by country specific 3 pin cables.


---
**Bromide Ligand** *June 06, 2017 02:05*

The yellow wire is connected to the Ground terminal at the back of the laser and also soldered to the middle pin of the power socket as seen in this pic.

![images/ab41a03550bfa0b9e1b058c926962b47.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab41a03550bfa0b9e1b058c926962b47.jpeg)


---
**Bromide Ligand** *June 06, 2017 02:06*

The socket has a fuse inside, the black wire is connected to the fuse.

![images/ce1f80c45c6a4447c77adb6fa85969ba.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ce1f80c45c6a4447c77adb6fa85969ba.jpeg)


---
*Imported from [Google+](https://plus.google.com/108393288879485897084/posts/j6oWHnz4UZC) &mdash; content and formatting may not be reliable*
