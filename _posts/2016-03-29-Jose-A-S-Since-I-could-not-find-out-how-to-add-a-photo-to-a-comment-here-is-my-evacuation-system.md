---
layout: post
title: "@Jose A. S Since I could not find out how to add a photo to a comment here is my evacuation system"
date: March 29, 2016 15:21
category: "Modification"
author: "Don Kleinschnitz Jr."
---
@Jose A. S     Since I could not find out how to add a photo to a comment here is my evacuation system.



![images/26ff5efd3e1221b51f5a37ed44e79ba0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/26ff5efd3e1221b51f5a37ed44e79ba0.jpeg)
![images/a4fe5d4beb979f36b299900b54599f29.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4fe5d4beb979f36b299900b54599f29.jpeg)
![images/617291422ce548930651a7ff3c73db4f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/617291422ce548930651a7ff3c73db4f.jpeg)
![images/fc8a8aeb5d2b0f19c57c8d1ff3c9bd4d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fc8a8aeb5d2b0f19c57c8d1ff3c9bd4d.jpeg)
![images/3b1b11e0dd52803b701f21d75f1b118b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b1b11e0dd52803b701f21d75f1b118b.jpeg)
![images/e31049d9404a78643a844faf967ecf7a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e31049d9404a78643a844faf967ecf7a.jpeg)
![images/34e2b147690503b8a1c43f10e74ac57b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/34e2b147690503b8a1c43f10e74ac57b.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Jose A. S** *March 29, 2016 15:34*

Wow, Thank you!...everything is clear!..that fan looks so powerful


---
**3D Laser** *March 29, 2016 17:01*

How loud is it


---
**Don Kleinschnitz Jr.** *March 29, 2016 21:44*

Its not to bad (doesnt annoy my wife downstairs)  did not measure DBM. I really did not have a choice as I cannot keep it in the garage in the winter.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/2Kt62LaDRtm) &mdash; content and formatting may not be reliable*
