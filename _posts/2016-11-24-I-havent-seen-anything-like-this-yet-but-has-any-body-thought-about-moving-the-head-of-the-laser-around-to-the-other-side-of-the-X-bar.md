---
layout: post
title: "I haven't seen anything like this yet but has any body thought about moving the head of the laser around to the other side of the X bar?"
date: November 24, 2016 20:57
category: "Modification"
author: "Andy Shilling"
---
I haven't seen anything like this yet but has any body thought about moving the head of the laser around to the other side of the X bar? I was just looking at mine and thought if it was possible you could gain about 50mm at the top.



It would mean swapping the head around and re-mounting the second mirror to the other side of the stepper motor but it could be worth it for a 300x320 ish bed.





**"Andy Shilling"**

---
---
**Bob Damato** *November 24, 2016 21:22*

The X-Y mechanism only has so much travel, wouldnt you lose it at the other end?




---
**Andy Shilling** *November 24, 2016 21:31*

I don't think so because at the bottom of the run I have nothing more than a piece of garden hose. I'm pretty sure if this was taken off the actual mechanism with run under the the bottom x support bar.


---
**greg greene** *November 24, 2016 21:52*

Should work


---
**Ian C** *November 25, 2016 08:14*

Someone else was talking about doing it, think it might have been on a web site, but in doing so it fouled the metal casing, so you'd need to mess with the end stop


---
**Andy Shilling** *November 25, 2016 09:00*

That was a concern but I might look into it a bit more when I get my c3d board.


---
**Ian C** *November 25, 2016 11:21*

Be interesting to see it if you do. I'm not sure if I'm keeping my K40, won't be if I go to an X700 clone, so have been eyeing the mods but resisting ;-)


---
**Dennis Luinstra** *November 25, 2016 16:29*

I tried it a few weeks ago,  it didn't work out. Then I made a new laser mount plate out of aluminum so that the laser is more closer to the carriage at the original side of the carriage also needed to cut aluminum away from the top back side of the gantry and cutting more stuff. I managed to get 34.5 X 26.5cm. Goog luck! 


---
**David Lancaster** *November 25, 2016 17:57*

This guy flipped a K40 head over the bar, and a bunch of other tweaks to get more cutting area.  Does seem like one thing leads to another for a little bit until he gets all the clearance and stuff sorted.

[cnczone.com - Build Thread Cheap laser cutter modifications.](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/138554-software.html)


---
**Kelly S** *November 25, 2016 19:33*

I read that a couple weeks ago.  I'd say it'd be easier to build a box and buy longer rails and swap all the componets over vs trying to make it better.  Same amount of time invested either way. 


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/DP227rcg3xj) &mdash; content and formatting may not be reliable*
