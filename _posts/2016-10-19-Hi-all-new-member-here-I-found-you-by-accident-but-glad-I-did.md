---
layout: post
title: "Hi all new member here, I found you by accident but glad I did"
date: October 19, 2016 08:50
category: "Original software and hardware issues"
author: "Andy Shilling"
---
Hi all new member here, I found you by accident but glad I did. I have been given a k40 by a friend as he never used it. Upon receiving it I found the laser tube cracked, so now that has been replaced I am looking at getting to use it.



 My first problem is the software. The cd that came with it is showing as empty so I downloaded laserdraw. This will bit talk to the cutter as I'm presuming it has something to do with the vendor password. Is there a method to find this password?  ( I have the maddog dongle )



Due to this problem I found a download page for moshilaser and moshidraw I have installed both of these and it seems moshilaser if a plugin for Coralx4. The moshidraw starts but when I try to output to the cutter I get a message saying the program is already running or something like that.



If somebody has the time could they point me in the right direction on what software I should be using and if it's possible to download it. I believe I have the moshi ms10105 board and the machine I presume is an old version as I have a manual dial amp metre rather than the digital display I've seen on the latest ones.



Thanks for your time and effort in reading this and if I need to give any more info please just ask 



Andy





**"Andy Shilling"**

---
---
**Scott Marshall** *October 19, 2016 09:34*

The Newer Laserdrw software is designed to work with the M2nano controller and isn't backward compatible (or compatible with much at all) Properly set up, it imports .dxf and other files, and works well with Coreldraw from what I hear (I'm not a Corel user) Opinions vary on all the available software packages, do your homework on the software before deciding your course, because the hardware you use dictates what software you'll have to use (within limits)



Support for Moshi system has died out, and if you're comfortable with electronics, swapping in a M2nano controller is an option - they can be had under a hundred bucks on ebay or you can usually pick up a slightly used 'take out' here.



Another option is the Smoothieboard controller. I sell plug and play kits designed to work with the M2nano system, but I don't have anything that directly replaces the Moshi arrangement. If you decide to go that route, I'm sure we can get one of my universal kits to work in your laser. The running gear on these lasers is all essentially the same, and it's usually just a matter of getting the right connectors and some minor electronics to get everything talking. The Smoothieboard runs Laserweb and Visicut software. Once set up with Laserweb you can import a variety of formats, and Gcode. It's opensource, and you should look into it before deciding what your best course is. As with all software, opinions vary.



Scott



[ALL-TEKSYSTEMS.com](http://ALL-TEKSYSTEMS.com)  

[all-teksystems.com - all-tek-systems](http://ALL-TEKSYSTEMS.com)


---
**Andy Shilling** *October 19, 2016 09:40*

Cheers Scott ideally I would like to get some kind of output from this first before spending more out.  It did come with another board but I am at work at the moment so can't see which this is but it is different to the moshi board that is fitted at the moment. I will look tonight and get back to you.


---
**Scott Marshall** *October 19, 2016 09:50*

We'll  help you get it running if you've got the parts. The big thing to check is make sure the laser fires (this verifies your power supply is good) and you can do that by pressing the test button. (make sure you have water flowing in the tube any time you fire it) If your power supply is good, you're probably in good shape (motors very rarely fail) 



If you just installed the new tube, make sure it's wired correctly and the High voltage line is insulated properly. The high voltage on these is deadly so be careful.



Your mirrors will be badly out of alignment, just put a piece of paper in front of the passthru hole and close the lid. Set the power to 1/4, close the lid and fire a test shot. If it pierces the paper, you're good to go.



Catch you later.








---
**Andy Shilling** *October 19, 2016 09:57*

Fantastic, I have replaced the connections to the laser because they were just wrapped around, I have also tested the Laser and it is working. I know I need to raise my cooling system to help stop the air bubbles but I will leave the final alignment until I know everything is working.


---
**Jim Hatch** *October 19, 2016 12:22*

You'll need the USB key that came with the machine to make LaserDRW work. It's likely not a software key.


---
**Andy Shilling** *October 19, 2016 12:31*

Hi Jim I have a maddog dongle that installs as Umi v4.1 under Windows 7. I think my main problem is that the disc isn't showing in the pc. The first time I put it in it showed it wasn't empty but none of my pc's can read it and I've got a macbook pro that has no optical drive. If I could find out what was on the cd I might have a fighting chance to get it all talking to each Other.


---
**Jim Hatch** *October 19, 2016 12:46*

You might want to put a post out specifically asking if someone has that CD they can upload to Dropbox or somewhere so you can get it working. There are a couple of folks who have done it for the Nano based software but I don't recall if anyone's posted the stuff for the Moshi based K40s.


---
**Andy Shilling** *October 19, 2016 12:52*

Great idea I'll have a go tonight when I get five minutes to work out how this community thing works properly.


---
**Scott Marshall** *October 19, 2016 14:43*

Wrapped around is the correct way of making the connections to the tube. If you solder to them, it can create micro cracking around the conductor and will cause your tube to eventually fail from the gas leak. A screw type barrel is acceptable if you don't put a strain on the glass with it. They're harder to properly insulate than the wire wrap method. 

A lot of people make this mistake, the wirewrap seems like they just cut corners and you want to improve on it, so out comes the soldering   pencil. There's only 20ma passing thru that connection, so it doesn't have to be super low resistance to work well. Good insulation is the critical part of the high voltage connection. 



The best way to deal with the bubbles is to rotate the tube so the water outlet is on top. Healthy pump flow (2-3 liters per minute or better) helps too. Now is the time as you are going to re-align it anyway.



Scott


---
**Andy Shilling** *October 20, 2016 07:48*

So last night when I got home I checked the spare board I have but it's an older moshi than the one fitted. So after reinstalling windows 7 and the drivers for the maddog USB I started looking at the moshidraw software on their site. I found the download for an English version of moshidraw and installed it and.............. 



Look what I managed to do!



Laser didn't need aligning (bonus)

But I do need to spend a bit of time mastering the settings clearly.

I had no control over the power of the laser from the program so that had to be set via the machine and to get a better cut/depth I needed to slow the mm/S right down.



I'm happy it works. 



Can somebody tell me if I should be able to control the laser power from the software or not please and would the setting to engrave or cut be the option I need to use.

Andy

![images/39fef2580634cdde5c61a57372400962.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/39fef2580634cdde5c61a57372400962.jpeg)


---
**Scott Marshall** *October 20, 2016 09:48*

Wow, you scored on that deal.

 

I would still align the mirrors, as they probably are out a bit. An easy way to tell is if the laser seems weaker as you get closer tothe lower right corner of the bed.



I think you're essentially on your own on the Moshi software, I don't know anyone who uses it, but I think I heard of one other fella on here with one. 



It looks like you've got it figured out pretty well.



Power is controlled from the power knob, even on the newer models. (I'm pretty sure the Moshi machines also had the "Current Regulation" knob and meter)



 You have to go the an aftermarket controller to pick up software level control.



Congrats!



Scott










---
**Andy Shilling** *October 20, 2016 09:55*

Thanks again Scott, I'll continue to work on my settings etc and hopefully I can prove it's worth it moving to a Smoothie in the near future.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/ZVto3gHNxqJ) &mdash; content and formatting may not be reliable*
