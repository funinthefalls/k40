---
layout: post
title: "Hi, this is my K40, I hope you'll like it ;-) Detailled photo of the peltier module (4x inside de wood box), the original board to get DIR and STEP signals, the water tank made with roofmat contain about 10 liters"
date: October 31, 2016 19:15
category: "Modification"
author: "Joachim Franken"
---
Hi, this is my K40, I hope you'll like it ;-)

Detailled photo of the peltier module (4x inside de wood box), the original board to get DIR and STEP signals, the water tank made with roofmat contain about 10 liters.

The power (pwm) and the PPI mode is done by an arduino pro micro. There is also a water sensor controler by the arduino, but the temperature is a stand alone screen and probe.

With PPI mode I can setup the pulse time in ms, the PPI, and the power.



I have problem with the PPI mode, after certain time of cutting the power decrease, sometimes after 30 sec of cut, sometimes 5 min. If someone know why??



![images/b3dcd3374b25e4993d291b79cccec0b0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b3dcd3374b25e4993d291b79cccec0b0.jpeg)
![images/9d2af409936b8181f06543a884a37404.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9d2af409936b8181f06543a884a37404.jpeg)
![images/abd59ff55827141c45a3939c8dcec4bb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/abd59ff55827141c45a3939c8dcec4bb.jpeg)
![images/d6337c71004f1805a19c6a5ef09fb38d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d6337c71004f1805a19c6a5ef09fb38d.jpeg)
![images/231cbcd84d6d8b5c44a7c7e874e052a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/231cbcd84d6d8b5c44a7c7e874e052a6.jpeg)
![images/035a24192069d43a4aae4679e8ae9199.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/035a24192069d43a4aae4679e8ae9199.jpeg)
![images/e2fcb4847b5133bf5368a71bddc4e2f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e2fcb4847b5133bf5368a71bddc4e2f6.jpeg)

**"Joachim Franken"**

---
---
**HalfNormal** *November 01, 2016 01:05*

**+Joachim Franken** how are you implementing the PPI mode?


---
**Joachim Franken** *November 01, 2016 15:36*

Yes but I don't see improvement in cutting, my psu give 22mah a full power, so I limited the pwm range to 18mah, I tested at 18mah, from 3 to 8ms, and from 100ppi to 800ppi, but at 800ppi and 5ms at 10mm/s, the signal is near continuous fire


---
**Joachim Franken** *November 01, 2016 17:25*

Sorry, I miss read you question, I count the step on x and y and fire during the time programmed once the distance between two point is reached. It's working with interrupt to get the step, polling is not a good method




---
**HalfNormal** *November 02, 2016 16:04*

**+Joachim Franken** Thanks for the info. What controller are you implementing this on?


---
**Joachim Franken** *November 03, 2016 14:03*

The original 6c6879m2, i don't see why every body tell that's a crap, the only missing feature is to control the laser power by software. I add a valve for the air assist controled by this board also.


---
**HalfNormal** *November 03, 2016 14:42*

I am a little confused. What are you using other than the M2Nano to control the laser? PPI control is not in the software or hardware that comes stock with the unit. 


---
**Joachim Franken** *November 03, 2016 20:48*

An arduino pro micro with atmega 32u, all signals are intercepted by interrupt, and calculation of the travel distance is done once motors have done 4 steps. With this system, i can engrave at 400mm/s when ppi is inactive, the fire signal is intercepted by interrupt and send on another pin of the arduino


---
**HalfNormal** *November 04, 2016 01:31*

Is your sketch available?


---
**Joachim Franken** *November 07, 2016 13:30*

Yes I have github, i'll put on, are you able to help me on the psu problem?




---
**HalfNormal** *November 07, 2016 13:34*

I will do my best. **+Timo Birnschein**​ might be interested too.


---
**Joachim Franken** *November 07, 2016 16:53*

[https://github.com/JoachimF/PPI](https://github.com/JoachimF/PPI) Have fun ;-)

[github.com - JoachimF/PPI](https://github.com/JoachimF/PPI)


---
**HalfNormal** *November 08, 2016 01:10*

Thanks!


---
**Joachim Franken** *November 08, 2016 14:15*

My code isn't too messy?


---
**HalfNormal** *November 08, 2016 17:06*

Your code is just fine. WILL let you know if I do any tweaks. 


---
*Imported from [Google+](https://plus.google.com/107809295738006995715/posts/EFNRb29mxRU) &mdash; content and formatting may not be reliable*
