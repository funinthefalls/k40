---
layout: post
title: "Just another cutting board ;)"
date: June 09, 2016 08:26
category: "Object produced with laser"
author: "Alex Krause"
---
Just another cutting board ;)

![images/1d7dd5e8a247061cc0d5b7cccfd140c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1d7dd5e8a247061cc0d5b7cccfd140c4.jpeg)



**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 09, 2016 10:09*

You know what would be cool? You know how when people are dead in movies they do the "chalk outlines" on the bitumen? You could do vegetable outlines somehow on the boards to make it look like a vege was killed there.


---
**Ariel Yahni (UniKpty)** *June 09, 2016 12:00*

have to get myself on the cutting board trail immediately. Dinner is coming jajajaj


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/5B9RJxQzDVP) &mdash; content and formatting may not be reliable*
