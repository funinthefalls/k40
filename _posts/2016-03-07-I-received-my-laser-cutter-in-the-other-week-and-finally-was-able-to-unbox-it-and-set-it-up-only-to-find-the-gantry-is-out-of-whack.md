---
layout: post
title: "I received my laser cutter in the other week and finally was able to unbox it and set it up only to find the gantry is out of whack"
date: March 07, 2016 20:30
category: "Discussion"
author: "G Ragonese"
---
I received my laser cutter in the other week and finally was able to unbox it and set it up only to find the gantry is out of whack.  After contacting the seller they would either pay the service to have it fixed, send another one, or give compensate me for my troubles.  Is this something I should be able to correct myself or would I be better off taking a chance in having another one shipped out?

![images/eee3eb777c4f9a513bb61771c18c41b5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eee3eb777c4f9a513bb61771c18c41b5.jpeg)



**"G Ragonese"**

---
---
**Stephane Buisson** *March 08, 2016 17:02*

check the belts, it's an easy fix.

then look the for Table assembly square with  the body (4 screws underneath), or you you will have trouble to set the mirror path ray.

(sorry for the late answer, your post end in the spam box)


---
**G Ragonese** *March 08, 2016 18:32*

No worries!!  Are there any tools I can but to make sure I have it square?


---
**The Technology Channel** *March 08, 2016 19:14*

A square??? prob all you need.


---
**Martin Larsen** *March 08, 2016 21:33*

i got mine yesterday.. Excact same problem. Some machineworker must have been pretty tired when adjusting the saw for these alu profiles :)


---
**Sunny Koh** *March 09, 2016 01:25*

The Chinese gave me this instructions to fix it [http://v.youku.com/v_show/id_XNzMyODA2NTIw.html?from=s1.8-1-1.2#paction](http://v.youku.com/v_show/id_XNzMyODA2NTIw.html?from=s1.8-1-1.2#paction)


---
**Anthony Bolgar** *March 10, 2016 21:32*

That is a scary way to fix it. I would never reef on the gantry to make it parallel. 


---
**James White** *March 18, 2016 00:24*

In regards to the alignment... If you remove the lower black cover (directly over the y stepper)  you gain access to an adjustment coupler. Just loosen it enough to let one side of the shaft spin and square it, retighten and reattach the cover. If you adjust it by yanking on it, it causes the belt to  skip teeth on the pulley allowing for a crude way to align the axis. I use method A.


---
*Imported from [Google+](https://plus.google.com/114662781480101912819/posts/HqCFp5LPbnk) &mdash; content and formatting may not be reliable*
