---
layout: post
title: "Does anyone know why I'm get wavy lines instead of straight lines when raster engraving?"
date: February 22, 2018 15:59
category: "Discussion"
author: "Duncan Caine"
---
Does anyone know why I'm get wavy lines instead of straight lines when raster engraving?

3mm MDF 150mm/sec 2mA  also tried reducing speed to 100mm/sec



Inkscape and K40 Whisperer

![images/6e43d759c18f7b55316e3883df76b1ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6e43d759c18f7b55316e3883df76b1ac.jpeg)



**"Duncan Caine"**

---
---
**James Rivera** *February 22, 2018 16:12*

Probably still too fast.


---
**Ariel Yahni (UniKpty)** *February 22, 2018 16:37*

Loose belt


---
**BEN 3D** *February 22, 2018 17:20*

Is it the same if you engrave a single circle? If not, then it should be a software issue ;-)


---
**James Rivera** *February 22, 2018 18:33*

Disregard my comment. I agree with **+Ariel Yahni** — you very likely have (at least) one loose belt, because that definitely looks like backlash.


---
**Steve Clark** *February 22, 2018 21:52*

**+James Rivera** **+Ariel Yahni**  and or timing pullys set screw could be loose.


---
**Peter Alfoldi** *February 23, 2018 19:04*

Fyi, My timing pulleys do not have any grub screws... what a shameful design :(


---
**Duncan Caine** *February 23, 2018 22:19*

Update



I've tried several things with no success.  I've also had the whole frame out because I couldn't find out how to adjust the  belt tensions.  I now know how. I don't want to teach you to suck eggs, but let me know if you want me to tell you.  I have learned that the frame is not square or level and I will address that over the next couple of days and let you know how I get on ....phew!


---
**Ariel Yahni (UniKpty)** *February 23, 2018 22:48*

**+Duncan Caine** you have learned what "most" do. Still it's important that the info is continuously shared with the community


---
**Grafisellos Barranquilla** *June 15, 2018 00:25*

yo tube ese mismo problema y era la correa . se soluciono tensionando si necesita escribame y le explico como


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/2hoFHvLybod) &mdash; content and formatting may not be reliable*
