---
layout: post
title: "Hello, does anyone have picture of their y axis connection?"
date: November 21, 2018 19:20
category: "Original software and hardware issues"
author: "Andero A"
---
Hello, does anyone have picture of their y axis connection? I'd like to see where the belt should run and where the white gear should be, thanks!

![images/4576276f5d3603c806c078cd0681e808.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4576276f5d3603c806c078cd0681e808.jpeg)



**"Andero A"**

---


---
*Imported from [Google+](https://plus.google.com/106364731785191924991/posts/QpoL3q5x8ay) &mdash; content and formatting may not be reliable*
