---
layout: post
title: "New to the group, so forgive me if someone already asked this or a similar question, I looked but didn't find anything"
date: September 09, 2016 19:44
category: "Hardware and Laser settings"
author: "Steve Burgess"
---
New to the group, so forgive me if someone already asked this or a similar question, I looked but didn't find anything.



I just received the K40 and everything seemed ok (other than a few loose nuts and bolts that I had to refit, and I still need to figure out the software). Anyway, I turned on the pump and noticed a leak at the "business" end of the tube... I replaced the little hose loop and cut back the other hose to get a tighter fit, put plenty of that white goo over them... but there's still a steady drip. I fear it might be the tube, specifically the end of the laser tube. Am I going to have to get another tube? Any suggestions?





**"Steve Burgess"**

---
---
**Ian C** *September 09, 2016 20:13*

Can you post a photo of the tube and area where you think there could be a problem? That may help the more experienced group members identify your problem


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 20:21*

Hopefully not, but if so contact the seller & request a partial refund to replace the tube (to the value of what it will cost you to get a tube shipped to you).


---
**Steve Burgess** *September 09, 2016 20:56*

![images/071fb2e8a1b6827063506f6670c775a6.png](https://gitlab.com/funinthefalls/k40/raw/master/images/071fb2e8a1b6827063506f6670c775a6.png)


---
**Steve Burgess** *September 09, 2016 21:51*

The picture above shows more or less where the leak seems to be. It looks like there is a seam in the tube, just behind where the two hoses connect. I;m hoping that a lot of that white silicon will fix it, but I fear that the pressure will push drops through again.


---
**Ian C** *September 09, 2016 22:06*

Hey. I'd go with some two part epoxy resin to try and seal over the cracking. That is the front cooling jacket and with my own K40 on delivery I had to glue it back on and used two part epoxy resin. Silicone is OK, but it doesn't dry hard, where as epoxy will. You're sure it's not just the seal between the silicon cooling pipe and glass barbed outlet pipe? If the glass is a little rough there, the silicone pipe might not achieve a nice tight seal around it


---
**Steve Burgess** *September 09, 2016 23:12*

**+Ian C** That's a really great idea.. I'll try first thing tomorrow. Thanks Ian, greatly appreciated.


---
**Ian C** *September 10, 2016 17:22*

**+Steve Burgess** You're welcome buddy. Keep us posted on how you get on


---
**Steve Burgess** *September 10, 2016 23:25*

Well, don't want to jinx it, but it's looking like you solved the problem!.... Many thanks again... Now to make some fumes!!!


---
**Ian C** *September 11, 2016 10:03*

**+Steve Burgess** Awesome, have fun buddy


---
*Imported from [Google+](https://plus.google.com/113164096506505999811/posts/f67PDJJynUu) &mdash; content and formatting may not be reliable*
