---
layout: post
title: "Finally getting back to my large format K40"
date: November 11, 2017 20:58
category: "Modification"
author: "Steven Whitecoff"
---
Finally getting  back to my large format K40. Purchased the mechanicals several months ago and now that outdoor hobby season is over its time to continue with this. I paid for a 1280x800 setup but the seller was paranoid at meeting my needs so he sent a 1400x900 for the same price. I also got a good enough price on components I went with closed loop steppers and controllers. At this point I need to decide on a base, source some kind of enclosure(thinking from a scrap yard) and assemble it. Trying hard to keep it under $2k including the K40 I bought early this year, now Cohesion controlled. I have the drivers and RepRap display so its mostly mechanical stuff needing done.

![images/e93bd5362d71a82febaa7b5fca5983ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e93bd5362d71a82febaa7b5fca5983ff.jpeg)



**"Steven Whitecoff"**

---
---
**Anthony Bolgar** *November 11, 2017 21:15*

Have you considered getting a larger capacity CO2 tube since you should have the space in the new case you will be fabricating?


---
**greg greene** *November 11, 2017 22:26*

I don't get it, you can buy a 100W 1K by 700mm all set up for $2500, including rotary and electrically driven bed



But if it's the experience your after - that's priceless


---
**Ray Kholodovsky (Cohesion3D)** *November 12, 2017 03:00*

The machines that frustrate me most are the ones I purchased assembled. No tinkering = no fun. Of course, I'm not running a service so I can fiddle with stuff all day. 


---
**Steven Whitecoff** *November 14, 2017 12:57*

Anthony, my application is cutting foam, balsa and liteply all of which use very low power settings with a K40....why would I spend money for more power?

Greg- I've not seen anything like that kind of price. Given that I can't get my money back for the K40 that would make  me have nearly $3k invested vs the $1500 I'm in for not counting the K40. Also my absolute minimum size requirement is 1280x800. The setup pictured above has genuine Hiwin cars and tracks so I bet I have better quality motion control. Also none of the budget units I've seen offer closed loop stepper/controllers and I went one size over the normal size incase I want to add a router head. I would agree if I count my time then a built machine might make more sense but I'm retired and have far more time then money.




---
**Steven Whitecoff** *November 14, 2017 13:58*

Thats a Nano board in the bag for size comparision

![images/bf6bd1c6497b178be7d76a53bfa5119c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf6bd1c6497b178be7d76a53bfa5119c.jpeg)


---
**Steven Whitecoff** *November 14, 2017 13:59*

![images/2369423433d31c94473dbb48bfe3f987.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2369423433d31c94473dbb48bfe3f987.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2017 14:15*

Looks like a C3D Mini sized bag 


---
**Steven Whitecoff** *November 14, 2017 16:39*

Thats correct...big thanks for a having drive wiring example on your site, eliminated a lot of searching to figure which mode(NPN, PNP or differential)to connect the Mini to the HBS507 which it turns out is the Chinese market model and very under documented vs the  Leadshine D series. But a little poking around at CNCzone lead to an english setup utility so I'm starting to feeling like this could actually work :D

The biggest issue remains finding an inexpensive 78"x48"x12"(or more)enclosure, hoping to find a equipment cabinet or locker used. NOT liking how this is on track to be a 300lb behemoth thats going to require a rethink of my shop layout, but thats the price of being able to cut 48"+ materials.


---
**greg greene** *November 14, 2017 19:53*

Geez - the way your going - your going to need a 20 foot sea can ! :)


---
**Steven Whitecoff** *November 15, 2017 17:51*

Sea can? Anyway, I'm stalled trying to figure the best way to mount and enclose this. It does not seem so big on the bench but when I realize I need for example a storage cabinet big enough to easily get my whole body into, twice, it hits home. Worse I need to move this thing in 2 years clear across the country. Its the same old story, pick light, strong or cheap- any two is no problem but all three sure IS.




---
**greg greene** *November 15, 2017 18:14*

**+Steven Whitecoff** 

I's a nice problem to have I guess!

couple of sheets of 4 x 8 tin, some rivets, a bit of angle iron for a sturdy frame, a metal table with motorized wheels (From a scooter - 36V) - and then - how to get it through a doorway.   :)


---
**greg greene** *November 15, 2017 18:14*

**+Steven Whitecoff** 

I's a nice problem to have I guess!

couple of sheets of 4 x 8 tin, some rivets, a bit of angle iron for a sturdy frame, a metal table with motorized wheels (From a scooter - 36V) - and then - how to get it through a doorway.   :)


---
**Steven Whitecoff** *November 16, 2017 18:31*

So the plan is locally source 4'x6' 0.032" aluminum on a 1.25" square tubing frame, brazed together, somewhere around 18" high. All in should total a bit under $300 and hopefully  under 150lbs for the system.


---
**Steven Whitecoff** *April 21, 2018 20:52*

Finally made some progress with my Chinese supplier convincing him that gearing the Y axis 20(or 24):38 was not a good practice as the travel/per step did not resolve to a even number which in a low precision (I doubt even a Cohesion mini process more than 4-5 decimal places) application would result in a significant error over the 100000 step full travel takes. So he made me a replacement reducer to yield a 1:3 ratio like my X axis. It should be here in a few weeks and I can get going on making a base/enclosure to get this puppy cutting.


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/cUYnUbbQBoC) &mdash; content and formatting may not be reliable*
