---
layout: post
title: "Got the triple length z axis bed installed 12\" of travel"
date: July 29, 2017 23:06
category: "Modification"
author: "William Kearns"
---
Got the triple length z axis bed installed 12" of travel

![images/5c7add38e70b2e6e26abe49ea3d6276a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c7add38e70b2e6e26abe49ea3d6276a.jpeg)



**"William Kearns"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 30, 2017 02:06*

Is this a modified LO bed? Seems taller but I'm not exactly sure what was done. 


---
**William Kearns** *July 30, 2017 02:08*

Yes I've tripled the length of the rods 


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/HzYRJV4Hy8i) &mdash; content and formatting may not be reliable*
