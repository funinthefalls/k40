---
layout: post
title: "After about an hour of engraving (NOT cutting) wood and this is what my lens looks like"
date: December 13, 2015 22:44
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
After about an hour of engraving (NOT cutting) wood and this is what my lens looks like. Urgh. Second pic is after cleaning it. I'm getting used to having to clean it every every hour or two of constant engraving (wood). Because I haven't been doing a lot of acrylic recently, I don't have a benchmark to compare it to.



![images/e3402ea28b4d2fe54c80521bf56f2658.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e3402ea28b4d2fe54c80521bf56f2658.jpeg)
![images/0c3c719f18572b8dfbce8f5b1d18d56d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c3c719f18572b8dfbce8f5b1d18d56d.jpeg)

**"Ashley M. Kirchner [Norym]"**

---
---
**Scott Thorne** *December 14, 2015 00:59*

The air assist completely stops that.


---
**Phillip Conroy** *December 14, 2015 01:00*

Adding air asist will keep lens clean,a small air compressor and air blowing the smoke away from the lens


---
**Yuxin Ma** *December 14, 2015 02:20*

**+Phillip Conroy** I'd like to add this for people who aren't experienced with compressed air. Don't get compressor that's too small if you mind the noise. I have a 3 gallon one. It charges up to 100PSI but I only use 25PSI. So I can keep the air going for a few minutes before the compressor has to kick in. If you fiddle with the cut-in and cut-out pressure of your compressor, you can probably do lots of cuts before it kicks in. 


---
**Ashley M. Kirchner [Norym]** *December 14, 2015 05:23*

Here's the thing though, I was told by several folks NOT to run the air assist during engraving, only cutting, and my own experience, albeit with acrylic, is that having it on during engraving can cause problems. And that's only one issue. On the one job where I was running the compressor the whole time, engraving, cutting, flipping the pieces over to engrave the other side, the compressor got too hot and shut off in the middle of a cut pass. It's the small Harbor Freight that some folks recommend on here. So I'm at that point where I'm either cleaning the lens constantly, or overheating the compressor because it's constantly running. Do I get a larger one that can fill up quicker, which would shut it off periodically? What else can I do?


---
**Scott Thorne** *December 14, 2015 09:12*

**+Ashley M. Kirchner**​...I use a 40 gallon shop compressor, and I use it when engraving and cutting with no issues what so ever....it stays at 15 psi.


---
**Coherent** *December 14, 2015 14:58*

I simply set the airflow lower when engraving than when cutting. It keeps the smoke and fumes out of the lens head and clears the smoke from obstructing the laser path.


---
**Ashley M. Kirchner [Norym]** *December 14, 2015 19:26*

Considering this is a small compressor, I think even at 15psi it would constantly be running, or switching on/off constantly. So I think I'm going to look for a larger one and plumb that in.



**+Scott Thorne**, when I was engraving acrylic with the air on, it was turning the engraving very white, and from the explanation I got, it was the air blowing the hot material back into the piece and melting it back in place. Which kinda made sense as I was finding bits and pieces on the areas that have not been engraved, and those pieces would be stuck on, like melted on. Easy enough to wipe off since they're small, but it left small pitted areas. Engraving acrylic with the air off is much better.



Granted, I did not change the PSI, it's whatever the pump was putting out as it never has a chance to fill up the tank to actually build pressure, it was bleeding it off too fast.


---
**Scott Thorne** *December 14, 2015 19:47*

Makes perfect sense, I've never engraved acrylic....only cut it so I wouldn't know about that but now I know....Lol


---
**Scott Marshall** *December 18, 2015 17:44*

Proper cabinet ventilation is more the issue.



Air assist blows through the kerf, and will keep the lens clean as a side effect, but proper downdraft will draw the smoke off the surface and out before it condenses on the lens. Engraving is worse than cutting because most of the smoke goes down through the kerf when cutting. The K40 has horrible venting, it needs more fan, venting around the perimeter of the workpiece, and adjustable air inlets above the workpiece (in the cover) will create an airwash (like in woodstove window airwashes) over the workpiece surface, keeping it (and the lens) clean.



Think Weber grill and Buck Stove.


---
**Ashley M. Kirchner [Norym]** *December 18, 2015 19:53*

I've actually been leaving the lid open a little bit during both engraving and cutting jobs. It helps, but I still get the lens covered like that after a while. I'm going to redesign the nozzle that I made and redirect the air inside. Right now it just comes in from the side at a 90 degrees angle and blows out the hole on the bottom, but I suspect internally it's not doing a good job of actually keeping the lens clean. I'm trying to decide if I redirect the air to actually hit the lens, if that would help. We shall see I guess.


---
**Scott Thorne** *December 18, 2015 20:41*

Be careful leaving the lid open, it covers the tube and first mirror in dusty junk.


---
**Ashley M. Kirchner [Norym]** *December 18, 2015 20:48*

That happens regardless though. I didn't use to leave the front lid open at all. Opened the back lid one day and discovered everything covered in dust. So it happens either way.


---
**Scott Thorne** *December 18, 2015 21:01*

Lol...didn't know that...thanks for the heads up....I'm leaving mine open from now on.


---
**Ashley M. Kirchner [Norym]** *December 18, 2015 21:06*

Yeah, if there's one thing these machines are not, is properly sealed. :) I have a large chuck of aluminum block in the bottom of mine to act as dampening weight (boy what a difference that makes in keeping the rattling noises down). Unfortunately, it covered the hole in the bottom so it can't suck air through air. leaving the front lid open just a little bit (I have two 1/2 posts on either corner of the door) helps with air flow. Not ideal still, but better.


---
**Scott Marshall** *December 18, 2015 22:35*

It's a losing deal either way, open cover slows down the little amount of airflow from the fan (and is not very safe), but closing it traps the dust and condensable resins and such which then deposit on all the optics. If you have a shop dust collector, tieing into that would be ideal, as would good sized central vac.

Unless the included fan is better than I think, a cheap bathroom vent fan (you can get them for $20 at home centers) with a piece of thin plywood in place of the grille might work ok  - cut a hole to fit your duct. and use foam filter material on the laser intake vents to let air in and keep shop borne stuff out. Good old duct tape will seal the leaks. It looks like the front could be cut out large enough you could tape a furnace filter over it and use it for a 'large workpiece' slot when needed.

Airspeed is key, you need to make the smoke go directly for the suction inlet, and never go near the mirrors. The little duct at the workpiece edge is the right idea but way too small. Better to pull down around the entire perimeter. That's the way the big machines do it. (Plasma and water jet as well) Some of them I've seen pull so hard they actually hold down the workpiece with the vacuum. As a bonus that alone provides some head 'assist' by drawing air down thru the kerf.



Commercial blowers are pricy, but an old furnace fan found at a garage sale or curbside trash is a score. they are quiet and powerful enough to put well away from the work area and pull through expanding duct. When I 1st started my business we painted machinery in my 2 car "paint booth" with a furnace fan blowing thru a trap door in the overhead door. 



Sorry to run on, I'm forcibly retired, and sure miss my work.


---
**Ashley M. Kirchner [Norym]** *December 18, 2015 22:46*

That's one thing the air assist has been helpful with, is pushing the sap/resin/smoke from the cut wood through the kerf and down onto the honeycomb. I was just looking at that last night thinking I need to clean the bed and replace the honeycomb. There's enough build up on it right now that the honeycomb is actually turning very rigid. Without the air assist, that stuff would just stay on the edges and burn black.



However I do believe that my lens is (still) getting dirty just because of the air flow inside of the nozzle of the air assist. I'm going to design a few different ones, print, and test them, see what works.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/J2Sxb6yW9KQ) &mdash; content and formatting may not be reliable*
