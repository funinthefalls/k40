---
layout: post
title: "Hi party ppl, How to adjust alignment of the laser tube itself?"
date: July 06, 2016 05:35
category: "Hardware and Laser settings"
author: "Pippins McGee"
---
Hi party ppl, How to adjust alignment of the laser tube itself? I see no screws or adjustment points to ensure the tube is firing into the middle of the first mirror (which mine is not)

Or should i only try adjust first mirror to get hit with the beam in its middle, as oppose to trying to fix the tube to shoot it into the middle of first mirror?



Cheers!





**"Pippins McGee"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 06, 2016 09:16*

It doesn't necessarily have to hit precisely centre on the first mirror. However, you need to make sure that it's hitting away from the edge of the mirror. Personally, I would adjust that mirror instead of the tube if I needed to make adjustments. There are mounts for the tubes that I have seen some purchased that allow adjusting for the tube.


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/iPhLRkieR27) &mdash; content and formatting may not be reliable*
