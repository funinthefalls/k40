---
layout: post
title: "Hydraulic mechanical arm toy with syringes Cutting plywood 3mm with laser K40"
date: January 08, 2017 16:47
category: "Object produced with laser"
author: "Mirco Slepko"
---
Hydraulic mechanical arm toy with syringes﻿

Cutting plywood 3mm with laser K40



![images/30f2c80e373c8fb396d48f7cd0398703.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/30f2c80e373c8fb396d48f7cd0398703.jpeg)
![images/0a1d313cdf256838187cde462d18a1db.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a1d313cdf256838187cde462d18a1db.jpeg)
![images/12896c9c8e0405a468c7a2f8a0a08e5a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/12896c9c8e0405a468c7a2f8a0a08e5a.jpeg)

**"Mirco Slepko"**

---
---
**Cesar Tolentino** *January 08, 2017 17:12*

This is awesome. Imagine applying that basic solution to your laser bed and make it adjustable?  Simply amazing...




---
**Brandon Smith** *January 08, 2017 18:13*

Wow, beautiful work! Any chance you would share the files? I would love to make this for the kids.


---
**Ned Hill** *January 08, 2017 18:23*

Hmmm...that's an interesting idea **+Cesar Tolentino**


---
**Mirco Slepko** *January 09, 2017 08:00*

Hi, I am preparing the files and will be ready in a few days on my blog


---
**Mirco Slepko** *January 09, 2017 12:37*

Hey Cesar,  you give  me a great idea!!  

Thank you. 


---
**Cesar Tolentino** *January 09, 2017 23:49*

Haha glad to help. Please share when your done...


---
**Mirco Slepko** *January 10, 2017 07:27*

surely I will


---
**Jérémie Tarot** *January 12, 2017 06:33*

Amazing ! 👍


---
**Mirco Slepko** *January 12, 2017 07:35*

thank you!


---
**Nigel Conroy** *January 12, 2017 20:22*

**+Mirco Slepko** this is a great project. I'm a teacher and have a small group that this will be ideal to do with. Can you tell me what size bolts you used and approx how many?


---
**Mirco Slepko** *January 13, 2017 07:35*

the exact number of bolts not I can not tell right now but I can tell you that I used M6 screws to hold the larger pieces while M4 and M3 parts for medium to smaller ones with less stress.


---
**Nigel Conroy** *January 13, 2017 19:18*

Thanks, have a starting point to go from now.


---
**Mirco Slepko** *January 13, 2017 19:37*

;-)


---
*Imported from [Google+](https://plus.google.com/+MircoSlepko/posts/SbVdPR1dKq6) &mdash; content and formatting may not be reliable*
