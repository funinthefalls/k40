---
layout: post
title: "5 Minutes free time. :-) It's not completely centered, but that's the most useful file by now, thanks for posting, it's great (sorry didn't find the original post)"
date: June 15, 2016 20:48
category: "Object produced with laser"
author: "Christoph E."
---
5 Minutes free time. :-)



It's not completely centered, but that's the most useful file by now, thanks for posting, it's great (sorry didn't find the original post).





There it is :-) [https://plus.google.com/114995491064499005334/posts/BUxxZwt1Jgw](https://plus.google.com/114995491064499005334/posts/BUxxZwt1Jgw)



Thank YOU ben crawford. :-)

![images/446b72a95c2108c5f7258659af6e4b80.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/446b72a95c2108c5f7258659af6e4b80.jpeg)



**"Christoph E."**

---
---
**Jim Hatch** *June 15, 2016 22:19*

Looks like a wider range than I have on mine. I don't hit 4ma (where it begins to lase) until your 60% mark and then it's at 20ma about where your 110% would be so couldn't just rotate it to start 0 further clockwise. It doesn't look like the machines have the same linear power scale.


---
**Christoph E.** *June 15, 2016 22:33*

**+Jim Hatch**

mine starts lasering @about 10-15% of that scale. Sounds like there is something going wrong with yours)



I don't even expect it to be an exact scale, this is just great for testing different materials and keep the results relatively constant.


---
**Jim Hatch** *June 15, 2016 23:34*

I don't think so. I expect it's just different pots. Yours is likely a wider range than mine. I wouldn't expect all the mfg use the same value pots for their power controls. Mine still runs thru the whole range of 0-24ish milliamps. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 16, 2016 04:48*

My 0 point is at around where the 90 is on this scale & 4mA is about where is says 100 here.10mA is somewhere around where the 0-10 on this pic.


---
**Alex Krause** *June 16, 2016 05:48*

I have even less range than all have stated lol I have a feeling it has to do alot with the internal trim pots of the power supplies due to over driving the tubes 


---
*Imported from [Google+](https://plus.google.com/100193166302371572888/posts/JsGg9CgAb1c) &mdash; content and formatting may not be reliable*
