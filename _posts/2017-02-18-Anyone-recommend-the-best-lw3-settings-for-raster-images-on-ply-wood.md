---
layout: post
title: "Anyone recommend the best lw3 settings for raster images on ply wood?"
date: February 18, 2017 23:38
category: "Discussion"
author: "Charlie Thcakeray"
---
Anyone recommend the best lw3 settings for raster images on ply wood? (K40 machine btw) 





**"Charlie Thcakeray"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 19, 2017 02:33*

There is no "best" settings that will be transferable from one machine to another. Each machine is going to function differently.



That being said, there are a few things you can do to dial in your settings.



First run some greyscale calibration tests. You're going to have to play around with settings a bit to find what suits your machine. Some config settings may need tweaking (for whichever controller you are using) & then some settings tweaks in LW3 itself.



Something like this image will help you work it out.



As a rough reference, you could look on my previous posts. I've generally specified all the power/speed/settings for the particular job.





edit: I wouldn't recommend using this exact image, as the colours seem out in my opinion. I'd redraw a new one making sure the colours are exact shades of black.

![images/22a5b19a52fa583e3cf04c3742212121.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/22a5b19a52fa583e3cf04c3742212121.jpeg)


---
**Alex Krause** *February 19, 2017 05:03*

The type of ply you choose will also determine alot of your settings... Birch engraves differently than maple that engraves different than Oak or mahogany ply... Also are you wanting to do photo engraving or logo/word engraving?


---
**Charlie Thcakeray** *February 19, 2017 10:25*

**+Alex Krause** hey Alex, I'm wanting to do photo engraving, just trying to save some time you see rather than keep playing around with it but I guess I'll have to do that.


---
*Imported from [Google+](https://plus.google.com/109895480370746543865/posts/bYxeufku2he) &mdash; content and formatting may not be reliable*
