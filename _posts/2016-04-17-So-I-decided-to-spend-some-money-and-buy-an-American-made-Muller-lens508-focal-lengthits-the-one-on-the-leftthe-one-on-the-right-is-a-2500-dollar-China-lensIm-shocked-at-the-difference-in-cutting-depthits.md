---
layout: post
title: "So I decided to spend some money and buy an American made Muller lens...50.8 focal length...it's the one on the left...the one on the right is a 25.00 dollar China lens...I'm shocked at the difference in cutting depth...it's"
date: April 17, 2016 17:26
category: "Discussion"
author: "Scott Thorne"
---
So I decided to spend some money and buy an American made Muller lens...50.8 focal length...it's the one on the left...the one on the right is a 25.00 dollar China lens...I'm shocked at the difference in cutting depth...it's like night and day....1/4 inch difference in depth of cutting.

![images/326119ce207b0b3087084233388edfb8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/326119ce207b0b3087084233388edfb8.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 17, 2016 18:10*

It does look to be a higher quality lens. Out of curiosity, what is the price difference?


---
**Joe Spanier** *April 17, 2016 18:22*

I'm also interested


---
**Scott Thorne** *April 17, 2016 18:22*

160.00...but in my opinion it's well worth it.


---
**Joe Spanier** *April 17, 2016 18:26*

For that difference I would say so. That's still cheaper than buying an official lens from universal for their lasers. 


---
**Thor Johnson** *April 17, 2016 19:07*

Got a link?  Is it a meniscus lens (concave/convex) or is one side flat?


---
**Scott Thorne** *April 17, 2016 22:03*

**+Thor Johnson**...it was a typo earlier...it was supposed to say Meller...anyway...laser depot USA sells them...I'm quite impressed with it...flat side by the way. 


---
**Jeremie Francois** *April 18, 2016 05:40*

Good to know,  thank you!  And a beautiful picture btw :)


---
**ED Carty** *April 18, 2016 13:27*

**+Scott Thorne** Awesome info Thanks


---
**ED Carty** *April 18, 2016 14:48*

That is some thick Plexi. wow.


---
**ED Carty** *April 18, 2016 14:52*

**+Scott Thorne** that thick plexi would be a great bottom for Amp gauge box, and some thin stuff for top and sides. A couple Leds in the box for back lighting.. sweet stuff man


---
**Scott Thorne** *April 18, 2016 15:34*

It's 1 inch thick...I cut through it in 5 passes...lol


---
**ED Carty** *April 18, 2016 15:37*

Sweet


---
**Bob Buechler** *March 16, 2017 21:41*

Did anyone ever find a part number for this? My search for Meller Optics turns up this: [laserresearch.net - Laser Optics Categorized by Function](http://www.laserresearch.net/optics.asp) but I'm not sure what to select from there. They list their stats in inches, and 50.8mm focal length converts to 2"... and 18mm diameter (for the LO air assist head) converts to .71" .. and I can't find a lens they offer with those measurements. Help? :) 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/CRJgpLn54f6) &mdash; content and formatting may not be reliable*
