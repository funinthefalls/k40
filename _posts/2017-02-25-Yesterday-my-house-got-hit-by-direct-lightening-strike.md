---
layout: post
title: "Yesterday my house got hit by direct lightening strike"
date: February 25, 2017 17:46
category: "Discussion"
author: "Jim Fong"
---
Yesterday my house got hit by direct lightening strike.  Stuff all around the house is dead, including my K40 laser.  You can smell the magic smoke from the power supply area. It was plugged in to surge protector and was off at the time too. All the computers connected to the cnc machines died including the some of servo and stepper drives for the 4 CNC's.  Lots of electronic stuff/appliances gone.  



Furnace is dead, I'm waiting for the heating guy to come take a look. Suppose to get colder this week too. 



Very bad start for the new year. Hopefully the insurance will help pay for most of this. 





**"Jim Fong"**

---
---
**greg greene** *February 25, 2017 17:54*

get them to buy you an upgraded model !


---
**Ned Hill** *February 25, 2017 17:59*

Wow, that's terrible.  You always hope surge protectors will protect your electronics but direct hits are a beast.


---
**Richard Vowles** *February 25, 2017 18:10*

What happened to your grounding wires? 


---
**Ray Kholodovsky (Cohesion3D)** *February 25, 2017 18:39*

Oh dear. 


---
**Jim Fong** *February 25, 2017 19:05*

Furnace guy left. Toasted mainboard. 20+ year old heater so not worth fixing he said.  Not sure what to do until claims adjuster comes over on monday. 



The lightening hit the metal furnace exhaust stack on top of the roof. Blew that off.  The energy traveled down to the furnace room in the basement.  That happens to be my workshop with 4 CNC machines.  Zapped everything in that room and even some stuff that wasn't plugged in.  I don't understand how that happen. 



Anything connected to Cable modem/phones lines all died.  Network computers, even my raspberry pi.  I just checked my home theater system and it doesn't turn on.  Still a lot I haven't even looked at.  Even the LED nightlights I have in the bathrooms died. 



My next door neighbor was home and saw sparks coming out of his wall plugs.  He lost some stuff but not as bad as me. 



Living room TV still works but no cable box. 



Atleast I got internet on my cell phone. 






---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 25, 2017 19:23*

Wow, that seems very unfortunate. Hope insurance will cover it all & get you back on your feet swiftly. At least the positive is the old saying "lightning never strikes twice", so we can hope that holds true & you don't have this issue again in future.


---
**greg greene** *February 25, 2017 19:47*

EMI will kill the stuff not connected


---
**Ned Hill** *February 25, 2017 19:51*

EMI or do you mean EMP? **+greg greene**


---
**greg greene** *February 25, 2017 20:17*

- Sorry :)  EMP


---
**Jim Fong** *February 25, 2017 20:40*

**+greg greene** I hope it didn't get that bad.  I had over a dozen brushless servo and stepper drivers sitting on the shelf waiting to be installed on new projects.  It was in the same area that the other equipment died.  



I know one of the Applied Motion STM17s that I was using on the 3d printer died including the power supply. That wasn't even plugged in. 


---
**greg greene** *February 25, 2017 20:41*

I've had it happen with a near miss too.


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/BFmkzAzdY1p) &mdash; content and formatting may not be reliable*
