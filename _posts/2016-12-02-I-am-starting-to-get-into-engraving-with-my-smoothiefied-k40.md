---
layout: post
title: "I am starting to get into engraving with my smoothiefied k40"
date: December 02, 2016 17:25
category: "Hardware and Laser settings"
author: "Carle Bounds"
---
I am starting to get into engraving with my smoothiefied k40. My K40 is one of the newer ones with the digital power setting which is displayed in percentage. Most people on here talk about their power settings in milliamps. I noticed last night when I engrave that a thing above 25 percent kind of burns the wood. I do have air assist going. I am guessing when I see people engraving on here at 6 or 7ma that it would be like 10 to 20 percent for me. What's a good speed to start out at? 30mm/s or so? I'm just trying to get a good starting point.





**"Carle Bounds"**

---
---
**Kelly S** *December 02, 2016 19:13*

Every unit and every power supply/tube is different in some way.  You will have to experiment at different power levels.  


---
**Carle Bounds** *December 02, 2016 19:19*

I was just wondering if around 15 percent was normal and nothing was wrong with my set up


---
**Ned Hill** *December 02, 2016 22:28*

Here's a decent idea of what a percent power to mA conversion would look like.  Most engraving speeds I use fall between 200-500mm/s and between  25-50% power.  Set up some small test engraves and experiment with changing speed vs power.  Do the same for cutting.  I do this each time I use a different material and mark each test with the info and keep for reference.

![images/e4b51f05043cd22bb19d1e0f3f940577.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e4b51f05043cd22bb19d1e0f3f940577.png)


---
**Cesar Tolentino** *December 03, 2016 15:57*

Im not sure if this can help but I always do a test cut of all the materials that goes thru my laser. So i know my settings everytime i deal with that material. I call it "material test cut". Not sure if you can use this, but will share anyway for others to use maybe.



For Vector Test:



DWG File

[https://drive.google.com/open?id=0B1xyPwLXXQk3RTYtZkNxN2ZLVEU](https://drive.google.com/open?id=0B1xyPwLXXQk3RTYtZkNxN2ZLVEU)



DXF File

[https://drive.google.com/open?id=0B1xyPwLXXQk3a2tCVXJPd3Nhdms](https://drive.google.com/open?id=0B1xyPwLXXQk3a2tCVXJPd3Nhdms)



SVG File

[https://drive.google.com/open?id=0B1xyPwLXXQk3RmhpbGNaREsyVUU](https://drive.google.com/open?id=0B1xyPwLXXQk3RmhpbGNaREsyVUU)



For Raster:



Well i dont have raster capability, but i think this will do the same since you just need to know which power band is the most useful to your material.



DWG File

[https://drive.google.com/open?id=0B1xyPwLXXQk3Ykl4M3M3WG91Zmc](https://drive.google.com/open?id=0B1xyPwLXXQk3Ykl4M3M3WG91Zmc)



DXF File

[https://drive.google.com/open?id=0B1xyPwLXXQk3dlBtMlBQLXdZMVk](https://drive.google.com/open?id=0B1xyPwLXXQk3dlBtMlBQLXdZMVk)



SVG File

[https://drive.google.com/open?id=0B1xyPwLXXQk3UHVnNGlvbHFqYlE](https://drive.google.com/open?id=0B1xyPwLXXQk3UHVnNGlvbHFqYlE)



Hope this helps.

![images/bb5651e4741c400b6c0aaf24c9252df7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb5651e4741c400b6c0aaf24c9252df7.jpeg)


---
**Kelly S** *December 03, 2016 16:56*

I will find those useful, thanks for sharing **+Cesar Tolentino**


---
**Cesar Tolentino** *December 03, 2016 17:00*

In my lasersaur board I can do lasing by color.  So I do from the lowest speed first so that will be on the right side of squares first.  I am looking at the fastest with lowest number of cut with the least burn damage I can live with. 


---
**Cesar Tolentino** *December 03, 2016 17:01*

So in the pic above on the most right ... My 600 is the same with my 400.  So I use 600mm/min


---
*Imported from [Google+](https://plus.google.com/106012823225343259431/posts/3zoiLVMf73e) &mdash; content and formatting may not be reliable*
