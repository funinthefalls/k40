---
layout: post
title: "Hey all, Patrick from Aus here. I am looking for some plywood suppliers in Australia that do 3mm bamboo ply that use a non toxic glue that is suitable for cutting and engraving"
date: April 15, 2018 08:35
category: "Material suppliers"
author: "Patrick G"
---
Hey all, Patrick from Aus here. I am looking for some plywood suppliers in Australia that do 3mm bamboo ply that use a non toxic glue that is suitable for cutting and engraving. I ahave been using marine ply from Bunnings but have found it to cause negative respiratory issues for myself and my partner. Anyone share any supplies or have any substitutes?



Cheers Patrick Adelaide Aus





**"Patrick G"**

---
---
**Kevin Lease** *April 15, 2018 10:15*

I use Baltic birch plywood in us


---
**Nick R** *April 15, 2018 11:53*

Heya, hows does that marine ply from Bunnings actually do when being cut?  I have only tried the non marine grade stuff they have.  As for other places, I have not really found any, but I havent exactly looked either yet (Also from Adelaide, Aust.)


---
**Kevin Lease** *April 15, 2018 11:56*

I searched the web then contacted several us companies and paid small fee had 5 sheets cut to size I needed for my laser bed shipped to me that was cheaper than amazon or Etsy in us


---
**Kiah Connor** *April 16, 2018 02:33*

Perth Aus here - I've tried the non-marine-grade bunnings plywood with some success, but you do end up with occasionally sticky cuts (from the glue), plus it's just inconsistent enough that sometimes you won't cut all the way through.



Good for bulk material for tests though.



I had them cut a large sheet into 300m strips, and purchased a circular saw for remaining cuts because they charge $1 a cut which would have cost more than the wood itself.


---
**Patrick G** *April 22, 2018 00:08*



**+Kevin Lease** 

That's pretty excellent. Who did you purchase through? Was the shipping costly and was the wood in good condition with no defects or bowing?


---
**Patrick G** *April 22, 2018 00:22*

**+Nick R** 

The marine ply is ok but I'm cutting on a stock machine with no mods to extraction of air assist. 



I generally do a 3 pass at 10mm/ps at 35% power. Clean cuts 80% of the time with the need for stanely knife and sand finish on the rest. Definitely not the fastest but leads to minimal flame ups.



Each time I have cut with it I have had pretty negative health effects with symptoms similar to a chest infection and am unsure if it is just regular effect of cutting, or the glues or the wood treatment. Due to that I haven't cut anything for nearly a year. Wanting to make a low negative pressure enclosure but have not gotten around it yet. 


---
*Imported from [Google+](https://plus.google.com/108727782554326629229/posts/GzhznvD51eM) &mdash; content and formatting may not be reliable*
