---
layout: post
title: "How many of you are using the CW-3000 water chiller for the K40?"
date: December 22, 2018 03:11
category: "Discussion"
author: "Sean Cherven"
---
How many of you are using the CW-3000 water chiller for the K40? Would you say it's good enough?



I understand it's just a radiator and fan, so no need to mention that. I'm just asking if it'll be good enough.



Can it be used in addition to a 5 gallon bucket of water?



=================



Another idea I have is to use this on a 5 gallon bucket of water:

[https://www.amazon.com/dp/B001JSVLBO/](https://www.amazon.com/dp/B001JSVLBO/)



Do you think one of these installed on the side of a 5 gallon bucket would be enough?





**"Sean Cherven"**

---
---
**Ned Hill** *December 22, 2018 17:54*

This uses  peltier cooling and the stated cooling capacity is 50W.  The effective wattage will be lower.  If you search for "peltier" in this group you will find some good technical talk on using peltiers for cooling.  I seem to recall that needed effective cooling capacity is going to be somewhere around 200 watts.  Also depends on your starting ambient temperature and how much you are going to run your laser.   So I got a feeling that this unit is probably going to be undersized to cool a 40W laser.


---
**James Rivera** *December 23, 2018 06:15*

As I recall it, peltier coolers were initially used to cool CPUs. You might be able to find cheaper ones in that genre. Maybe. I haven’t checked.


---
**Sean Cherven** *December 23, 2018 06:55*

And what about my question about the CW-3000?


---
**Joe Alexander** *December 23, 2018 10:15*

i believe some people may use a cw-3000 to cool their laser but having the right size reservoir is key. too small and it gets hot too fast. too big and it cant cool it fast enough. TBH sizing up to the cw-5000 is a better viable options it doesn't use TEC-based cooling, or add frozen water bottles to mitigate the buildup.. I use just a pump and frozen water bottles and seem to have no issue, but I also dont run my laser for massive amounts of time. longest run i have done is maybe 1.5hrs and it took maybe 3-4 frozen bottles.

   There have been setups using peltiers that work but they usually involve 3-4 units linked together as a single unit is usually insufficient. as stated by others closer to 200 watts of cooling is about right, and only if you can cool the hot side efficiently enough.


---
**Sean Cherven** *December 31, 2018 02:57*

So I went ahead and ordered a CW-3000 for now. My K40 is in the basement, and the ambient temp doesn't normally exceed 65 deg F.



I'll monitor it closely, and if I find the temps get too high I will look into getting a CW-5000.


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/dP9vsNwc7rz) &mdash; content and formatting may not be reliable*
