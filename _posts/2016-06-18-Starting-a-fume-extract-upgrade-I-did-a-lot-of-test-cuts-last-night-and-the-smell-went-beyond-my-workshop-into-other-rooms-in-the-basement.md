---
layout: post
title: "Starting a fume extract upgrade. I did a lot of test cuts last night and the smell went beyond my workshop into other rooms in the basement"
date: June 18, 2016 02:56
category: "Discussion"
author: "Ben Marshall"
---
Starting a fume extract upgrade. I did a lot of test cuts last night and the smell went beyond my workshop into other rooms in the basement. Bought an Attwood 4000 4inch in-line bilge blower. I didn't realize at the time that it is DC powered. Ultimately I did not want to buy a car battery just to power it (and eventually a charger only for charging said battery). and most power converters have regulators (most ac/DC adapters have them soldered in) which WILL NOT WORK WITH THIS. 



So I opted for a 12V battery charger with at least 6mA (needs 6 to start, or so I've read). It has to be a a MANUAL charger, any type of "smart" or "automatic" will not work as it must sense voltage before sending current.



Manual chargers send current as soon as it is plugged into 110v outlet.  



The cool thing about the charger I bought is there are three mA positions. So after hard wiring and shrink wrapping, my fan has three power settings which will be useful for lighter materials. I should have the temporary tubing hooked up tomorrow after the silicon has cured. I'll report the difference shortly thereafter. ﻿



![images/3a002bd5bb8c11247d87b2f982929ff1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3a002bd5bb8c11247d87b2f982929ff1.jpeg)
![images/28e6064103d19b3cad8809f6949348ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/28e6064103d19b3cad8809f6949348ae.jpeg)
![images/f15a8f6c33ca2809edf4607d7a30480a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f15a8f6c33ca2809edf4607d7a30480a.jpeg)

**"Ben Marshall"**

---
---
**Anthony Bolgar** *June 18, 2016 03:11*

You could also use a computer power supply. I power all my accessories from one for my K40, the bilge blower, cabinet lighting, laser diode pointer, and water pump. They are very cheap, you most likely can even find a free one from a computer repair store. There are plenty of 3D printed cases/panel fronts on Thingiverse to dress up the computer ATX power supply. Just a thought, you solution is also decent.


---
**Ben Marshall** *June 18, 2016 03:14*

How many watts does your psu put out?


---
**Anthony Bolgar** *June 18, 2016 03:22*

The one I am using is 650 Watts.


---
**Derek Schuetz** *June 18, 2016 06:52*

i just used this to power that same fan [https://www.amazon.com/gp/product/B01461MOGQ/ref=oh_aui_detailpage_o09_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01461MOGQ/ref=oh_aui_detailpage_o09_s00?ie=UTF8&psc=1)


---
**Anthony Bolgar** *June 18, 2016 07:08*

If the bilge blower is your only low voltage DC accessory then the power brick Derek linked to would be perfect. Like I said, all my accessories are either 12V , 5V or 3.3V, so that is why I chose an ATX power supply.


---
**Derek Schuetz** *June 18, 2016 14:46*

**+Anthony Bolgar** I just noticed that the fan supposibly pulls 10amps is that correct?



Nvm my math is bad it's 3


---
**Anthony Bolgar** *June 18, 2016 16:59*

That is very possible 10A X 12V = 120 Watts


---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/UcLviwzC5pB) &mdash; content and formatting may not be reliable*
