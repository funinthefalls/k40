---
layout: post
title: "Hi guys, I'm going to buy a k40 laser cutter, but I need some other information: The standard working area of 200x300 can be increased?"
date: May 23, 2016 08:39
category: "Discussion"
author: "Walter White"
---
Hi guys, I'm going to buy a k40 laser cutter, but I need some other information:

The standard working area of 200x300 can be increased? If yes, how much?

Every chinese 40w laser machine have a speed limit of 350 mm/s; this limit is by the electronics? mechanics? or other? With a 32bit electronics it is possible to go faster?



Thanks





**"Walter White"**

---
---
**Phillip Conroy** *May 23, 2016 10:37*

Speed all depends on what you are cutting or engraving -i cut 3mm thick mdf at 10ma power and 7.5mm/second -engraving can go a lot higher as the laser head is only going side to side-like an ink jet printer


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 11:48*

Regarding cutting bed, the standard is 300x200, however I removed things inside the machine (rails, original cutting bed, etc) & with some minimal tweaking you can get it to 320 x 220 easily. As far as increasing it significantly you'd be looking at replacing the entire X/Y rails & the belts & the rods that support the movement mechanisms. Unsure of the cost, but Lightobjects.com has a lot of parts that some here have been using to create their own larger sized lasers.



I am using 500mm/s for my engrave speed. I tend to not go slower than that ever because it's a painfully slow process even at 500mm/s. I don't know about speed limit of 350mm/s, as since I got it I've been using 500mm/s & it is definitely faster than when I set it to 350mm/s. I just change the mA to lighten or darken the result usually or adjust the pixel steps (higher pixel steps gives quicker engrave but lower resolution to the engraved image).



For cutting, I use speeds varying between 10-50mm/s. Higher speeds are not very good for intricate shapes as the machine vibrates horribly. Maybe less of an issue if it was bolted to something extremely sturdy, but I've yet to test that. 



I believe if anything it is the mechanics that are limiting the speeds. Any faster it will fall apart or cause damage to the mechanisms I imagine. I may be wrong on that however, just personal observation.


---
**Walter White** *May 23, 2016 13:07*

Thanks **+Yuusuf Sallahuddin**  for the info.

For now I'm interested only on engrave operations, and It's a very good news that the machine can go up to 500mm/s.

How long does it take to engrave a 200x300 area? Do you use the standard electronics or a custom one?

And for engraving a photo do you use pwm with a grayscale or "halftone" method?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 15:39*

**+Walter White** 

In regards to the 200 x 300mm area, the closest I have done is a tobacco pouch for a mate that took almost the entire 200 x 300mm for all parts. However, the actual engrave was minimal over that area, but it's top-left-most & bottom-right-most points were close to the bounds of the 200 x 300mm. This job took somewhere in the range of 4 hours @ 500mm/s @ 1 pixel step. To be perfectly honest, this is a ridiculous amount of time. If you can afford a minute loss of clarity in the final image, I would go with something like 3 or 5 pixel steps to speed things up.



I am using the default controller/software. Personally I believe it is functional, albeit a bit lacking in features that would improve the end result.



There is no power control in the engrave with the default software/hardware. It's basically 100% of whatever you have the power set to. This I believe is the opposite of what you are referring to by PWM. Grey scale is also not possible with the default software/hardware.



Whatever colour the pixels are in the image doesn't actually matter. Even white pixels cut/engrave at 100% of the selected power (as I recently found out). In order to create an engrave with depth/shades of colours you have a few options. 1) dither the image; 2) create multiple layers of different colours/shades & engrave them separately (at different powers).



I've experimented with both & realistically dithering is the method that is most time efficient, however I personally think the overall result/effect is a poor substitute for different power levels. I've done some tests of multiple layers/shades at different power levels (or even different speeds of the same power). This gives a nicer overall effect, but to some degree is material dependant (e.g. some materials 6mA engrave looks just the same as 10mA engrave).



I've also been experimenting with a vector engraving method. If you check my youtube videos you will find a video I did to show the setup of a file. But basically you create a bunch of very thin rectangles, spaced very closely together (although not touching each other). Vector cutting @ 25mm/s over an entire 200 x 300mm area is much quicker than 500mm/s engrave over the same area. Mostly due to the fact that the laser carriage doesn't waste time going to areas where it doesn't need to fire.



I've still yet to perfect the spacing on this, as again the settings seem to be material dependant (e.g. plywood using RectSizeRectSpacingPowerSpeed settings results in total black engraved area whilst hardwood with the same RectSizeRectSpacingPowerSpeed settings results in very fine lines that are spaced apart & do not touch each other or fill the colour properly; where RectSizeRectSpacingPowerSpeed is any arbitrary settings that are the same in both instances). Also, I've found using this method with speeds above 25mm/s will shake your mirrors out of alignment.



Sorry for the giant reply.


---
**Walter White** *May 23, 2016 16:31*

Thanks again **+Yuusuf Sallahuddin** !

Actually I can engrave a 180x240mm grayscale image on wood with a 2w laser diode and 0.15mm line spacing, in about 2:30h (speed about 250mm/s). So I think that I've no advantage to switch to a k40. 

But it might be interesting try faster speed with a strong chassis and 32 bit electronics!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 17:25*

**+Walter White** If you are planning on getting a K40 & want PWM & the likes, I'd consider looking into the Smoothie controller (by Arthur Wolf) & LaserWeb2 (by **+Peter van der Walt**). They're doing some wonderful work with lasers/engraving/etc.



The only real advantage for a k40 over a diode I believe is the higher power output. You'll probably be able to engrave deeper. But, you could probably just buy a tube & some nema steppers & whatever else is required & build your own. I'm not so familiar with the electronic/mechanical side of things, so to be honest I can't really say if it would be cheaper/better, but from looking at other's posts on this sort of thing, definitely looks like a decent way to go.


---
**Alex Krause** *May 23, 2016 18:37*

**+Walter White**​ the Breaking Bad cutting boards I did were about 1hr 40min start to finish at 320mm/sec and take up pretty much the whole work bed of the K40


---
**Alex Krause** *May 23, 2016 18:48*

;)


---
**Walter White** *May 23, 2016 20:46*

my initial idea was to immediatly change the electronics with an arduino uno+cnc shield; this combo works already pretty good with the laser diode.

this job [https://www.dropbox.com/s/x6zbmog1pnml1hm/2016-04-05%2007.17.48.jpg?dl=0](https://www.dropbox.com/s/x6zbmog1pnml1hm/2016-04-05%2007.17.48.jpg?dl=0) was made with pwm and take about 3 hours.

the real goal is to complete the full 200x300 area in less then 1 hour.



anyone can explain me why a k40 laser cost about 450€ (in europe) and his big brother (50w-300x500 area) it cost 3 times (1500€)?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 20:56*

**+Walter White** You can probably swap over the controllers reasonably easy & use the same software you currently do. I'm not certain about the price differences in the products, but here in Australia the k40 was about $600 & the big brother was $2000ish when I was purchasing (around 3.3 times).


---
**HalfNormal** *May 23, 2016 23:33*

Supply and demand. 


---
**Ian Hayden** *May 25, 2016 09:48*

if area is important - go the full hog and get the 600x400mm  I got one (a) as a backup for the 300x200, incase of tube failure (b) load balance for high volume orders (c) versatility and functionality - eg it includes a Z-bed - so no fluffing around if your engraving object is more than 1cm thick.  Currently one of my tubes has blown - and it takes 3 weeks to get  replacement - so am glad i took the plunge.  Yes it is twice the price, but i reckon more than twice the value. Check that you have enough space - they are bigger than they look


---
**Anthony Santoro** *June 18, 2016 03:51*

**+Yuusuf Sallahuddin**​ just following on a point you made in a comment above. I need to cut 3mm acrylic at 6.5mm/s and 3mm ply at 9.5mm/s. Have you made modifications to be able to cut at 25mm/s?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 18, 2016 06:58*

**+Anthony Santoro** No, I tend to use multiple passes when cutting materials, at lower power. Also, for example with 3mm ply I use 4mA power @ around 20mm/s & about 5 passes if I recall correct. I prefer to use super low power & speeds >10mm/s. I find that low power = thinner beam & faster speeds = less burn. I don't mind having to cut the same thing 10 times to get through it.



edit: although I think what you're referring to where I mention 25mm/s is where I am doing a mock engrave using the Cut function. This method where I go at 25mm/s doesn't cut through the material, but just marks the top layer. So using it I "cut" (rather "mark") the material with a series of closely spaced rectangles which emulates an engrave. Much quicker process than doing a standard engrave, although the effect is quite a lot different.


---
**Anthony Santoro** *June 18, 2016 07:13*

Ah, understood. 

What about laser degradation, do you find it to be linear or exponential? 


---
*Imported from [Google+](https://plus.google.com/118017715722776268014/posts/aNW3oHKsHcD) &mdash; content and formatting may not be reliable*
