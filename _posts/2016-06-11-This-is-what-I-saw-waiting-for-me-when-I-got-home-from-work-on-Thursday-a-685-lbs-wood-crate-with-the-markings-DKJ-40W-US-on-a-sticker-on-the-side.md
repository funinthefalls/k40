---
layout: post
title: "This is what I saw waiting for me when I got home from work on Thursday, a 68.5 lbs wood crate with the markings \"DKJ-40W-US\" on a sticker on the side"
date: June 11, 2016 22:58
category: "Object produced with laser"
author: "Bruce Garoutte"
---
This is what I saw waiting for me when I got home from work on Thursday, a 68.5 lbs wood crate with the markings "DKJ-40W-US" on a sticker on the side. There were 17 self threading hex head screws securing the lid. Up on opening I saw how well packed it was, secured with stiff foam on every side. As you can see by the photos, it is the type of foam that flakes off and the little particles statically attach to whatever they find, but hey, as a packing material, it did its job well.

All the extras were pretty well packed inside, but I would suggest to the manufacurer to perhaps put all the goodies in a plastic bag and taping it shut to keep the smaller pieces from wandering around the inside during shipping. The other suggestion would be to use a sleave on the DVD that can be closed to keep it from coming out and getting scratched up. Thankfully, I was able to retrieve the data from the DVD after a short buffing, so all was well.

There were a lot of upgrades on this machine that, for me at least, more than justify the extra $80 over some of the other units I saw. There is an LED lighting bar inside the cutting area, an Emergency Kill switch on the top, a digital ambient temperature thermometer, digital power display, and a turning knob labeled "Switch Machine", which I have no idea at this time what it does.

I had to vacuum all the little Styrofoam pieces out of it, which allowed me to open all three covers and take a look inside. All in ll, it looks like it is a decent little machine, and I look forward to learning how to use it.

Task #2 leveling out the counter in the shop where it will reside. The bottom is 1 1,/2 inches lower than the top, and considering that this unit also has coaster wheels on it, that seems like a good idea.

Here is the link to the one I purchased, and I noticed that the price has gone up $10 to $399. 

"[http://www.ebay.com/itm/291776502952](http://www.ebay.com/itm/291776502952)"

Thanks everyone here for all the great information that has helped me make my decisions while purchasing this machine and all the upgrades that will go on it.



![images/60306c1de262a2f5c471b032109592f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60306c1de262a2f5c471b032109592f5.jpeg)
![images/147c2872100d55e62d33f5f752cd5874.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/147c2872100d55e62d33f5f752cd5874.jpeg)
![images/a5993786e2db534e5dd87fd40088811c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a5993786e2db534e5dd87fd40088811c.jpeg)
![images/5ddb27992fbdebad2be78f3ac770f85e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5ddb27992fbdebad2be78f3ac770f85e.jpeg)
![images/4d275c40a6d7718cef8ab3e47210c00f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4d275c40a6d7718cef8ab3e47210c00f.jpeg)
![images/d026ccea71199dda2b51f42a56414a68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d026ccea71199dda2b51f42a56414a68.jpeg)
![images/343b008e3838aab92c140d4ffa68581a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/343b008e3838aab92c140d4ffa68581a.jpeg)
![images/dec6c9982af8228c263921c8ce2a350b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dec6c9982af8228c263921c8ce2a350b.jpeg)
![images/865ce756064194dd2b687de4cf176cd1](https://gitlab.com/funinthefalls/k40/raw/master/images/865ce756064194dd2b687de4cf176cd1)

**"Bruce Garoutte"**

---
---
**Scott Marshall** *June 11, 2016 23:35*

One of the better packed examples I've seen.



When you set it up, plug the water pump and K40 into a switchable outlet strip. That will prevent you from accidentally firing the laser without coolant flow, a common cause of very early tube death.



Have fun!


---
**Corey Renner** *June 12, 2016 00:04*

I just got the upgraded K40 with digital controls and (2) switched outlets on the back.  What's funny is that the outlets are two prong and the supplied water pump is three prong.  I guess I voided my warranty snapping off that ground prong....


---
**Scott Marshall** *June 12, 2016 00:10*

**+Corey Renner** Careful doing that, better to cut the plug off and wire direct. A submersible water pump is the sort of device that should be on a GFCI, or at very least, grounded. If there's an insulation failure of all that Chinese quality, you could very easily energize your water, tubing, and laser tube with AC mains voltage. All could appear well until someone reaches in the tank....


---
**Corey Renner** *June 12, 2016 00:26*

I hear you, but the whole point of those 3-prong cords is to provide a chassis ground and there is no metal chassis in those plastic pump bodies anyway.  I bet you a delicious beer that the ground wire isn't connected to anything inside the pump anyway.


---
**Bruce Garoutte** *June 12, 2016 04:32*

I noticed that just to the right of the second plug is a ground wire terminal. I think I'll get a three to two prong converter at the hardware store tomorrow, and screw the attached wire into it just to be safe.


---
**Scott Marshall** *June 12, 2016 10:03*

**+Corey Renner** If they followed standard design practice, it's hooked to the motor frame (laminations on the shaded pole motors in those pumps) which provides a ground path in the event of a housing leak. If there's no ground, the AC is free to seek the next easiest path to ground. Which could be you. As economical as the Chinese are, they wouldn't buy a ground wire and not attach it. It cost nothing to connect it once you've included it.



The reason I warn you is the water situation, a ground fault on almost anything else will give you a nasty shock, but generally won't kill you. This is due to the fact that you contact the device with a small area, and it provides relaitvely poor contact. Reaching into a tank of water with a bad pump in it only requires a good ground (like a wet concrete floor) and you've got a situation that can kill you dead.



There's no offense intended here, I just don't like to see people create dangerous situations for themselves and others, Be safe.



Bruce has a good solution.



Scott


---
**Ned Hill** *June 12, 2016 18:53*

Awesome, I bought the same one from that seller.  Suppose to be here Tuesday :)


---
**Evan Fosmark** *June 13, 2016 19:49*

**+Ned Hill** Same here! Going to work from home Tuesday so I can be here when it arrives. Looking forward to getting it set up.


---
*Imported from [Google+](https://plus.google.com/101697686569322649899/posts/ZwgF2ghmFbC) &mdash; content and formatting may not be reliable*
