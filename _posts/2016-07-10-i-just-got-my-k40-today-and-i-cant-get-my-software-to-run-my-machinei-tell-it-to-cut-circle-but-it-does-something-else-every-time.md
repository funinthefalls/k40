---
layout: post
title: "i just got my k40 today and i cant get my software to run my machine.....i tell it to cut circle but it does something else every time....."
date: July 10, 2016 01:12
category: "Software"
author: "Scott Howard"
---
i just got my k40 today and i cant get my software to run my machine.....i tell it to cut circle but it does something else every time..... it seems to do the same thing every time. any ideas??





**"Scott Howard"**

---
---
**Jon Bruno** *July 10, 2016 01:26*

typical issues are that you need to enter the SN and board type in the laserdrw driver.

if this is incorrect it will act wonky straight away


---
**Ariel Yahni (UniKpty)** *July 10, 2016 01:27*

There is a section to select the board you have. These days it seems everybody receives M2 boards


---
**Scott Howard** *July 10, 2016 02:06*

Where do you put ss number


---
**Ariel Yahni (UniKpty)** *July 10, 2016 02:18*

In laserdrw or corellaser there is an option to open machine preferences.  There  you will find to select board type 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 10, 2016 04:45*

CorelLaser plugin will have a setting button for Device Initialise. You put the SN & the board model into that page.


---
*Imported from [Google+](https://plus.google.com/113905238524886327481/posts/YmhSvVB1apr) &mdash; content and formatting may not be reliable*
