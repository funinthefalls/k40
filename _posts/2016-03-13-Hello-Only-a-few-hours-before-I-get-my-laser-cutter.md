---
layout: post
title: "Hello, Only a few hours before I get my laser cutter"
date: March 13, 2016 22:27
category: "Hardware and Laser settings"
author: "Jean-Baptiste Passant"
---
Hello,



Only a few hours before I get my laser cutter.



I just had one last question, I can't find much information about the laser cooling.



I will order a sensor to make sure the laser does not fire when the pump is off, but is there anything I can do to have an optimal cooling ?



Is closed loop the best option, or will a bucket do the job ?



Are there some liquid better than other ?



Do you cool the liquid somhow ?



I am sorry if it has been answered before, but I am new to G+ and can't find a way to search...



Thank you





**"Jean-Baptiste Passant"**

---
---
**Gee Willikers** *March 13, 2016 22:34*

5 gallon / 7-8 liter bucket. Open loop so any air bubbles can escape. Distilled and/or deionized water with pool chlorine crystals or some rv antifreeze to prevent growth. Cool as needed. some of us use dedicated chillers, some use frozen bottles of water in the bucket.


---
**Stephane Buisson** *March 14, 2016 00:12*

**+Jean-Baptiste Passant**  I personally use clear water with a bit of "blue" (product to clean windows, glycol based (same family as antifreeze) against algaes.

just check for bubbles before start. to not forget power the K40 and the pump at once from the same  wall socket.

in the summer or intensive use, frozen plastic bottle in (easy to replace without level change)


---
**Anthony Bolgar** *March 14, 2016 07:44*

If you are interested in a some additional safety for using the laser, you can check out [https://github.com/funinthefalls/LaserSafetySystem](https://github.com/funinthefalls/LaserSafetySystem)


---
**Phillip Conroy** *March 14, 2016 08:27*

Bucket of water must be at same level as laser cutter otherwise when pump stops water drains out of laser tube-introducing air bubbles when next used-if at same level this does not happen


---
**Justin Mitchell** *March 14, 2016 09:25*

I made a simple interlock with a flow sensor and an ATtiny chip, put in the loop with the door open sensors i also fitted it prevents the laser firing unless theres water flowing. [http://www.instructables.com/id/Water-Flow-Sensor-Interlock/](http://www.instructables.com/id/Water-Flow-Sensor-Interlock/)




---
**Jean-Baptiste Passant** *March 14, 2016 09:37*

Thank you for the answers !

I will get a 10 liter bucket and order the water protection sensor tube on ebay.

I will try to get it to work with my RAMPS and everything should be good then.



I will also get some distilled water.



Last thing, I need to get a table to put my laser on with the bucket near it.


---
**Phillip Conroy** *March 14, 2016 09:51*

Wish ihad fitted my flow switch before i blow the tube and power supply-the bugger of it all was i had the flow switch on my work bench for a week  before i blue stuff


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/cJT2APbe22j) &mdash; content and formatting may not be reliable*
