---
layout: post
title: "I attempted to Google this but found no information"
date: December 16, 2016 19:32
category: "Modification"
author: "Kelly S"
---
I attempted to Google this but found no information.   Is anyone here familure with discharging the power supply in the k40?  Will be gutting the machine in a week or so and relocating it to a add on enclosure I am building and want to keep it safe and hopefully not kill myself.  :)





**"Kelly S"**

---
---
**Ian C** *December 16, 2016 19:38*

Hey buddy. Just leave the psu unpowered for a few days before hand, as any residual charge in the capacitors will self discharge on its own. Other than that there is no other way to manually discharge them if you have been using it minutes before. A few hours would be enough I am sure, going by other psu encounters, but to be safe in light of the much higher voltage, leave it a few days


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/QQFpRzgAY2D) &mdash; content and formatting may not be reliable*
