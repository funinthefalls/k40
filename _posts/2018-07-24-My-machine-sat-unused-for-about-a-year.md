---
layout: post
title: "My machine sat unused for about a year"
date: July 24, 2018 19:38
category: "Modification"
author: "Jared Roy"
---
My machine sat unused for about a year. During this time the water hoses started to break down and have pieces flaking off the outside and the inside. Figuring that it would be a bad thing to get this material stuck in the laser tube, where can I can a suitable replacement?









**"Jared Roy"**

---
---
**Kelly Burns** *July 25, 2018 02:22*

 I doubt its the tube flaking. It’s like calcium deposits.  I use silicon tubing.  Available on Amazon and locally at aquarium supply and some home centers.


---
**James Rivera** *July 25, 2018 06:44*

Yep. Mine came that way. Just flush it out with fresh water, drain it again, and then add your distilled water.


---
**James L Phillips** *August 08, 2018 23:36*

Got mine at Lowe's. Regular plastic hose. I think it was 5/16" ID.




---
*Imported from [Google+](https://plus.google.com/105970952711765020046/posts/EkDT8g2Vdtq) &mdash; content and formatting may not be reliable*
