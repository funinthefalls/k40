---
layout: post
title: "After discovering my mirrors were out of alignment I have hit a cross roads, A is the mark taken with the Y axis was nearest the fixed mirror, as you can see pretty close to central"
date: October 08, 2016 17:05
category: "Hardware and Laser settings"
author: "J DS"
---
After discovering my mirrors were out of alignment I have hit a cross roads, A is the mark taken with the Y axis was nearest the fixed mirror, as you can see pretty close to central. B is when Y axis was at the opposite end of the run. How on earth can I rectify this as my mind has gone blank no matter how many times I read the instructions

![images/036bd2f8dd704d8dd81aea4c37b75dac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/036bd2f8dd704d8dd81aea4c37b75dac.jpeg)



**"J DS"**

---
---
**Ariel Yahni (UniKpty)** *October 08, 2016 17:19*

As i have recently witnessed the importance of a squared machine  this stuff that surely puzzles many is part of the reason. In your case closer to the origin will always be easier to point to the center but while you get away the real " angle" appears. In your case the fixed mirror is clearly facing "more" to the right so as it goes away it move in that direction. 



My experience and based on my latest mod is that each mirror ( being all in the same plane ) MUST be / create a 45 degrees to the beam in its natural position ( by setting all the screws on the holder to the same distance. i used a this at the center of the holder, also each holder center must be aligned as close as possible [https://goo.gl/images/pQsJ7R](https://goo.gl/images/pQsJ7R) 

[images.google.com - Image: Using a Protractor](https://goo.gl/images/pQsJ7R)


---
**J DS** *October 08, 2016 17:22*

Alas Im either tired or thicker than I thought. I will wait until tomorrow to work this out but thank your Ariel for your input




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 08, 2016 17:26*

Honestly, alignment is a nightmare when you are first starting out, especially if the machine isn't square (as many aren't). Looks to me like your 1st mirror (at the tube end) needs to be angled more to the left (if you are looking from the back of the machine towards the front) in this case. The minor angle difference over a distance (Y axis at front of machine) will equate to enough of a change that it pulls B back into the mirror, whilst at the close end (Y axis near tube) the difference created on A will be minimal.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 08, 2016 17:28*

I.e. 1 degree change for "close"/spot A will maybe move the position 0.1mm, whereas over the 200mm distance to "far"/spot B, the distance will change maybe 10mm. All trigonometry (which right now I can't be bothered working out the correct values).


---
**Gunnar Stefansson** *October 08, 2016 17:46*

Hey **+J DS** I totally feel you're frustration, I had same issue, but in all seriousness this was the video and the method that did it for me... Give this video a shot and do exactly like that and you'll be fine (Hopefully, or else something else is wrong!)




{% include youtubePlayer.html id="wY5D27TQwZI" %}
[https://youtu.be/wY5D27TQwZI](https://youtu.be/wY5D27TQwZI)


{% include youtubePlayer.html id="wY5D27TQwZI" %}
[youtube.com - Laser Alignment](https://youtu.be/wY5D27TQwZI)


---
**Ariel Yahni (UniKpty)** *October 08, 2016 18:13*

The guide above is very good plus **+Yuusuf Sallahuddin**​ math. Bellow what I tried to convey above.  The closer you are to these ( while all in the same plane while looked from the side)  the easier the alignment should be

![images/e687a219ad3278e6650b404cebf85593.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e687a219ad3278e6650b404cebf85593.png)


---
**K** *October 09, 2016 17:57*

You've probably already seen this set of instructions, but if you haven't here they are. It's very helpful. [http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)

[floatingwombat.me.uk - www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**J DS** *October 09, 2016 19:11*

**+Kim Stroman** Hiya Kim ive seen them read them several times and yet as soon as I get the unit infront of me I get brain freeze 


---
**Ian C** *October 09, 2016 19:56*

Hey buddy. To align A & B closer you need to angle the fixed mirror in front of the laser towards point A, so to the left. This will shift point B closer to A. Then move the fixed mirror a little away from the laser tube to bring the now joined spots to the middle of the Y axis mirror. I spent hours aligning my K40 mirrors and the bed, as this is out to on most units. I'm going by memory but that is what I did in that situation. The Floatingwombat pdf is handy to read combined with the various YouTube videos on alignment.


---
**Ian C** *October 09, 2016 20:00*

You also need to raise the gantry/bed at the back corner closest to the fixed mirror, this will raise the mirror and move dot A down towards the middle of the mirror


---
*Imported from [Google+](https://plus.google.com/102322879087562424255/posts/BUgQ5n2vsRr) &mdash; content and formatting may not be reliable*
