---
layout: post
title: "Poor mans laser cooler. Here are some pictures of a cooler that I installed for approximately $0"
date: February 05, 2018 19:57
category: "Modification"
author: "Mark Kirkwold"
---
Poor mans laser cooler. Here are some pictures of a cooler that I installed for approximately $0. The idea is that the return water fills a small basin that has a fan blowing on it. This uses evaporative cooling ([https://en.wikipedia.org/wiki/Evaporative_cooler](https://en.wikipedia.org/wiki/Evaporative_cooler)) and is thus subject to the conditions it is used in. But by way of an example, running in a 75F room at 30% RH, the coolest it could run is 56F.



I used a storage bin. I put some supports in for the cooling surface. I used some FR5 PC board edges I had. The point of the cooling surface is to 1. increase the surface area of the water and 2. prevent the fan from blowing any droplets. Many other materials could be used but I used a cheap Scotch-Brite type pad. A 12V cooling fan was attached such that the fan blows on the surface. The fan is wired to come on with the circulating pump. So, when running, the return water fills up the bin, saturates the cooling surface and runs out the front of the bin back to the water reservoir. 



There are a couple of issues with this setup. Firstly, this uses water without antifreeze. It could still work with antifreeze in the water, but I have no experience with that. Obviously some water is evaporated which needs to be periodically replenished. Finally, the fan is actively getting the water dirty with whatever is in the air.



![images/18a25d001dc88e3559bea4e0b0c7a640.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/18a25d001dc88e3559bea4e0b0c7a640.jpeg)
![images/05d90700ccf3762e09311dded18d2c89.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05d90700ccf3762e09311dded18d2c89.jpeg)
![images/aefa8e6dc02136e9d2d1c4563e8a7098.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aefa8e6dc02136e9d2d1c4563e8a7098.jpeg)
![images/4e2a03e30b3322adc22b1dfb3321b472.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e2a03e30b3322adc22b1dfb3321b472.jpeg)

**"Mark Kirkwold"**

---
---
**Fook INGSOC** *February 05, 2018 20:09*

SWAMP COOLER!!!


---
**Fook INGSOC** *February 05, 2018 20:16*

....or you could hack a dehumidifier!


{% include youtubePlayer.html id="az2w_hYSMnc" %}
[youtube.com - Dehumidifier to Air Conditioner Conversion](https://youtu.be/az2w_hYSMnc)






---
**Buhda “Pete” Punk** *February 05, 2018 20:30*

Hi Mark not sure if this would help. I used a generic Oil& brake line cooler. When I was doing some CPU testing in my younger days I bought an old 3'cube deep freeze and placed a stainless racing oil cooler inside the freezer. Had to drill two holes, but work great for 20%-25% overclocking. I would be interested if it would work for this. Put two fans and a pump on this [gizfab.com](http://gizfab.com/wp-content/uploads/2017/11/Oil-Cooler-Kit-1.jpg)


---
**Don Kleinschnitz Jr.** *February 05, 2018 20:53*

DO NOT RUN ANITFREEZE AS COOLANT IN YOUR LASER!


---
**Buhda “Pete” Punk** *February 05, 2018 22:31*

I used Mineral spirits, was going to try Floranert but was pricey at the time.


---
**Sebastian C** *February 06, 2018 10:35*

**+Mark Kirkwold**

thats the most basic cooling tower I have seen :)

It is really energy efficient but you have all the disadvantages.

It is also a good air scrubber, as you noticed. 

the evaporation will concentrate your dirt even more.

The sponge has an huge surface area, which is good for evaporation but also a good substrate for bacteria/algae.



Use a deep storage tank for dirt sedimentation

--> bleeding and refilling with fresh water

-->biocides

-->Filter ( meaning also preassure drop --> stronger pump needed)



Why would you use antifreeze? 

in normal conditions (i.e. workshop at 20°C/68°F, ~50%rH) you won't get a wet-bulb temperature below 13°C/55°F which is the lowest temperature you could get with an adiabatic cooling method.



Also you will have another chemical mixture added to your water.



Besides my concerns on the water part, this is an excellent hack !






---
**Don Kleinschnitz Jr.** *February 06, 2018 12:50*

One more warning on coolant. the conductivity of the coolant matters. This laser application is nothing like cooling a PC's cpu. To high or increasing conductivity in the coolant will create problems with the laser power supply. 

Some info here: [donsthings.blogspot.com - K40 Coolant](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
**Mark Kirkwold** *February 06, 2018 16:44*

**+Buhda Punk** **+Buhda Punk** **+Sebastian C** **+Don Kleinschnitz** 

I don't use anti-freeze, but I have heard of those that have to (i.e. it's in the garage and could freeze). Some (most) anti-freezes also have a biocide/algaecide effect, which could be useful.

I am skeptical (I could be wrong) that the conductivity of the cooling fluid matters very much. Pure water is a pretty good insulator (19Mohm) but <b>any</b> impurity increases the conductivity dramatically. I have watched the conductivity of DI water increase in real time as it was in contact with air. In a laser tube the insulator is glass. If that breaks down there will be a problem, if it does not, it does not matter if there is liquid mercury on the other side. Lots of people (myself included) use tap water for cooling, which is quite conductive, without trouble. Since tap water varies so much from place to place it cannot be recommended across the board, but it certainly can work.

While I am spouting heresy, I might also say that I am not too concerned about biological contamination. Grok what a UV-C system is (i.e. [https://en.wikipedia.org/wiki/Ultraviolet_germicidal_irradiation#Water_disinfection](https://en.wikipedia.org/wiki/Ultraviolet_germicidal_irradiation#Water_disinfection)) and then consider what a laser tube is.

The system I described works well for what I do and where it is. YMMV. It completely eliminates the need for ice (and the thermal shocks associated with it). It does not let you control the temperature but it does a good job of regulating it, and it 's basically free.

[en.wikipedia.org - Ultraviolet germicidal irradiation - Wikipedia](https://en.wikipedia.org/wiki/Ultraviolet_germicidal_irradiation#Water_disinfectionand)


---
**Mark Kirkwold** *February 06, 2018 16:44*

**+Buhda Punk** **+Buhda Punk** **+Sebastian C** **+Don Kleinschnitz** 

I don't use anti-freeze, but I have heard of those that have to (i.e. it's in the garage and could freeze). Some (most) anti-freezes also have a biocide/algaecide effect, which could be useful.

I am skeptical (I could be wrong) that the conductivity of the cooling fluid matters very much. Pure water is a pretty good insulator (19Mohm) but <b>any</b> impurity increases the conductivity dramatically. I have watched the conductivity of DI water increase in real time as it was in contact with air. In a laser tube the insulator is glass. If that breaks down there will be a problem, if it does not, it does not matter if there is liquid mercury on the other side. Lots of people (myself included) use tap water for cooling, which is quite conductive, without trouble. Since tap water varies so much from place to place it cannot be recommended across the board, but it certainly can work.

While I am spouting heresy, I might also say that I am not too concerned about biological contamination. Grok what a UV-C system is (i.e. [https://en.wikipedia.org/wiki/Ultraviolet_germicidal_irradiation#Water_disinfection](https://en.wikipedia.org/wiki/Ultraviolet_germicidal_irradiation#Water_disinfection)) and then consider what a laser tube is.

The system I described works well for what I do and where it is. YMMV. It completely eliminates the need for ice (and the thermal shocks associated with it). It does not let you control the temperature but it does a good job of regulating it, and it 's basically free.

[en.wikipedia.org - Ultraviolet germicidal irradiation - Wikipedia](https://en.wikipedia.org/wiki/Ultraviolet_germicidal_irradiation#Water_disinfectionand)


---
**Don Kleinschnitz Jr.** *February 07, 2018 12:31*

A number of us were skeptical and in fact many, including me, were using antifreeze. Then after fixing a number of setups that were arching, loosing power, blowing LPS and getting low grade shocks from coolant tanks we did the tests referenced in my post above and concluded that non conductive fluids were necessary. 



The big difference to consider in this cooling system is that these supplies output >20,000vdc and at that voltage apparently conductive water creates a low impedance and loads the LPS. The exact mechanism is unknown the effect is. Perhaps the water becomes the plate in a large capacitance across which is 20,000v.



I personally have been involved in fixing >30 machines and their laser power systems. 

So, if you start having problems with power try changing the water .....


---
**Mark Kirkwold** *February 07, 2018 15:50*

High voltage is a tricky beast, to be sure. I, for one, do not miss having to deal with distributors and spark plug wires. My only point is that water is, almost always, conductive to one degree or another and with these voltages will always be a good-enough path to cause problems, if such a path exits at all.  So, using a non-conductive fluid should be considered a work-around to some other underlying issue. But in those cases I'm sure that it is easier to fix the water!

The way the HV line is attached to the tube seems to me rather, um, janky. Wrap a thin wire around a tiny rod, don't solder!, and then bath the whole thing is non-conductive silicone. What could go wrong? I wonder if that is the source of some of the issues you've seen?; in which case the path to the water is not through the tube but around the outside. Just a thought.


---
**David Cook** *February 09, 2018 05:41*

**+Buhda Punk** flouinert sucks. It has the thermal conductivity of wood..   I use it to cool an rf filter in a 15kw semiconductor power supply. Need it for the isolation but water if it could be used would be WAY better..

Best way to use flourinert is via phase change. 


---
**Buhda “Pete” Punk** *February 09, 2018 21:44*

good to know, Thanks


---
**Fook INGSOC** *February 10, 2018 15:55*

**+Buhda Punk** ...mineral oil is going to be your best bet in a closed loop type system for safety!



[https://en.wikipedia.org/wiki/Transformer_oil](https://en.wikipedia.org/wiki/Transformer_oil)



....have you seen the pc's immersed in mineral oil???




{% include youtubePlayer.html id="results" %}
[youtube.com - mineral oil pc - YouTube](https://www.youtube.com/results?search_query=mineral+oil+pc)


---
**The Best Stooge** *February 13, 2018 07:11*

This works on the same principle as a swamp cooler and for most parts of the world they do not work well due to the RH.  Here the RH can reach 90% and in the summer is maxed out RH for the 100f-105f temps.  Just will not work unless you have a very dry climate that also happens to be hot like a desert.




---
*Imported from [Google+](https://plus.google.com/107985881342308921169/posts/j3Mp2EsmrkV) &mdash; content and formatting may not be reliable*
