---
layout: post
title: "Show your focus helper"
date: September 12, 2016 00:08
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
Show your focus helper

![images/0723fa3abbbfa538f9c434cf0e46b6bb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0723fa3abbbfa538f9c434cf0e46b6bb.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 12, 2016 01:02*

I'm just using a ruler or tape measure. I have found that the optimal point for mine is 70mm from the base of the carriage plate. I'm interested to see what others use however.


---
**Alex Krause** *September 12, 2016 02:15*

Cheapo calipers

![images/51d68ea4679021c480ebedace0fb59a0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/51d68ea4679021c480ebedace0fb59a0.jpeg)


---
**HP Persson** *September 12, 2016 05:50*

I put this on the material, and lower my head down so its touching and turn the thumbscrew to fasten it - done!

(custom lens head)

![images/42704f4f78cf340b7d3b6cc4b2a785da.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42704f4f78cf340b7d3b6cc4b2a785da.jpeg)


---
**Rich Barlow** *September 12, 2016 16:34*

Here are mine . . . 8.5mm for thin materials and 6.5 for thicker ones . . . 



![images/abfdc324dad272ad1ab6c27409d079bb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/abfdc324dad272ad1ab6c27409d079bb.jpeg)


---
**Ariel Yahni (UniKpty)** *September 12, 2016 17:12*

**+Rich Barlow**​ so the notch goes bellow the head ring? 


---
**Greg Curtis (pSyONiDe)** *September 12, 2016 22:12*

After testing, 10mm from the tip of my air assist head to the surface of my project was perfect. So I cut a little widget to allow me to just set the tip of the head on this, while on top of my project material and it's focused.

![images/d6b8c59b2eeb2b2bd00a262f9425f44e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d6b8c59b2eeb2b2bd00a262f9425f44e.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 12, 2016 22:16*

**+Greg Curtis** Do you only have 10mm between base of your air-assist & the focal point?


---
**Greg Curtis (pSyONiDe)** *September 12, 2016 22:17*

**+Yuusuf Sallahuddin**​ yes, I just added a description.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/gak9YwnLBEj) &mdash; content and formatting may not be reliable*
