---
layout: post
title: "Any idea how to make beam straight after lens?looks like in my k40 beam is going out of head center and i se its heat not exacly under head?"
date: April 05, 2016 12:34
category: "Hardware and Laser settings"
author: "Damian Trejtowicz"
---
Any idea how to make beam straight after lens?looks like in my k40 beam is going out of head center and i se its heat not exacly under head?





**"Damian Trejtowicz"**

---
---
**Scott Marshall** *April 05, 2016 13:05*

It's the rotation of the head that changes where the beam leaves the nozzle. Adjust the beam/head height to get the beam going in the center of the entry hole (higher head moves the beam to the right).

(you may have to shim it with paper or felt washers) then turn the mirror carrier (for in/out or up down on the workpiece) until the beam goes out the center of the nozzle. Just stick some masking tape on the nozzle, turn the power way down, pulse, test and repeat...



Edit - 

Also, the head has to be perfectly vertical on both axis (I used a torpedo level)



You've just got to visualize the invisible bank-shot.....



Scott


---
**Damian Trejtowicz** *April 05, 2016 14:44*

I will check all this again

In my case looks like beam is not hitting material straight,i mean there is no 90 degree betwen beam and materiall


---
**Thor Johnson** *April 05, 2016 14:59*

0. Check to see that your beam is hitting the center of the final mirror (on the post).  I had issues that looked like this, but it was because the beam was hitting the edge of the post and causing it to bounce all over the place at certain bed locations.  I use closed-cell foam so I don't worry about smoke ([bit.ly/1Mb5Qdj](http://bit.ly/1Mb5Qdj)).

1. Check to make sure your work is a constant distance to the cutting end (if it's level, you can use a level in the next step).

2. Check with a torpedo level, bulls-eye level, or a square to make sure the post that holds the mirror is perpendicular to your work.

If the above are true, then your last mirror (attached to the post) is at an odd angle (see that it's seated all the way and not on the edge of the counterbore), or your focusing lens is tilted (remove it, mount it so convex side is up and make sure it seats level in the holder.  When putting the post back together, make sure that it comes back together without tilts.


---
**Phillip Conroy** *April 05, 2016 19:41*

What does the cut matetial look like ,Cut 3mm mdf and look closely at the cut.......[http://www.parallax-tech.com/faq.htm](http://www.parallax-tech.com/faq.htm). i had to competly read this a few times to understand it . the laser beam after the focal lens has an hour glass shape[wide than very narrow than widening] all my cuts have a very slight angle to them resulting in  wide at top of cut and very thin on bottom of cut,i mostly mirror image all my work in the laser cut window and use the bottom of the cut as the front of my finished work as this is where the laser beam on my k40 is  the narrowest 0.1mm wide.


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/bL9uhR6Mde9) &mdash; content and formatting may not be reliable*
