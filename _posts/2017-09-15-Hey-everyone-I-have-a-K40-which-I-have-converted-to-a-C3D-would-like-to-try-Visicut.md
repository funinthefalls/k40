---
layout: post
title: "Hey everyone, I have a K40 which I have converted to a C3D, would like to try Visicut.."
date: September 15, 2017 17:11
category: "Software"
author: "E Elzinga"
---
Hey everyone,



I have a K40 which I have converted to a C3D, would like to try Visicut.. can anyone share their settings please? 



Tia.

Erik.





**"E Elzinga"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 15, 2017 17:27*

Not sure how many people are actually using Visicut. What values do you need? We have the settings info for LaserWeb on the documentation site, might it be transferable? 


---
**E Elzinga** *September 15, 2017 18:26*

Possibly.. Guess I just need the pre and post G-code, DPI and i'm curious what speeds fellow K40 owners are using.



This is what I have now (most are the default values):



![images/f610d7147965da61a8c1cceb160a273a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f610d7147965da61a8c1cceb160a273a.png)


---
**Ray Kholodovsky (Cohesion3D)** *September 16, 2017 14:04*

That all looks ok, start with that. 


---
*Imported from [Google+](https://plus.google.com/113837421619430195320/posts/QAxmjx47WHP) &mdash; content and formatting may not be reliable*
