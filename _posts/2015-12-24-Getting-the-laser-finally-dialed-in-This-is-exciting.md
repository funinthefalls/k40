---
layout: post
title: "Getting the laser finally dialed in. This is exciting !!"
date: December 24, 2015 05:35
category: "Object produced with laser"
author: "Lawrence Simm"
---
Getting the laser finally dialed in.    This is exciting !! Lol 

![images/d300e90653531d270ff95f8330e6ff36.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d300e90653531d270ff95f8330e6ff36.jpeg)



**"Lawrence Simm"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 24, 2015 07:58*

That looks cool & I totally agree that the Laser opens lots of exciting possibilities for making things. Have fun :)


---
*Imported from [Google+](https://plus.google.com/105668845795384171497/posts/br2kRY5nUsF) &mdash; content and formatting may not be reliable*
