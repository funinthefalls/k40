---
layout: post
title: "Back along, someone posted a picture of an adapter to take the hose"
date: September 25, 2016 20:42
category: "Discussion"
author: "Pete Sobye"
---
Back along, someone posted a picture of an adapter to take the hose. The adapter was rectangular and fitted straight into the slot on the rear of the machine and did not foul the Laser compartment lid.

Can anyone suggest where I can find one and can another picture be posted please, so i can confirm its what I am looking for. Thanks

Pete





**"Pete Sobye"**

---
---
**greg greene** *September 25, 2016 20:57*

air hose, water hose, or exhaust hose?


---
**HP Persson** *September 26, 2016 03:32*

Check out Thingiverse.com , there is a few solutions there, search for K40.




---
**Alex Krause** *September 26, 2016 03:52*

[https://www.thingiverse.com/thing:1743383](https://www.thingiverse.com/thing:1743383)


---
**Sunny Koh** *September 26, 2016 16:33*

[amazon.com - Kaufhof KWY162 4 Inch Flange Port For Wood Shop Dust Collection - - Amazon.com](https://www.amazon.com/Kaufhof-KWY162-Inch-Flange-Collection/dp/B00O4CWBAM)




---
**Don Kleinschnitz Jr.** *September 26, 2016 20:58*

If you are talking about the rear exhaust. I found a heater floor vent to do mine. I think it came from lowes.

![images/681779b763e6ee5223da04d3d0a8dc72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/681779b763e6ee5223da04d3d0a8dc72.jpeg)


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/bWb8mZa6dgy) &mdash; content and formatting may not be reliable*
