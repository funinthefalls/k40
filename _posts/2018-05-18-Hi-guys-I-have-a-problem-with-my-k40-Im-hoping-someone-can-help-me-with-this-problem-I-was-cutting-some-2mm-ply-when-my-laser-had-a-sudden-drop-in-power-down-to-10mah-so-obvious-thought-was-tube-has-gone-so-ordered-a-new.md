---
layout: post
title: "Hi guys I have a problem with my k40 Im hoping someone can help me with this problem, I was cutting some 2mm ply when my laser had a sudden drop in power down to 10mah so obvious thought was tube has gone so ordered a new"
date: May 18, 2018 06:45
category: "Discussion"
author: "Craig Goddard"
---
Hi guys I have a problem with my k40 I’m hoping someone can help me with this problem, I was cutting some 2mm ply when my laser had a sudden drop in power down to 10mah so obvious thought was tube has gone so ordered a new tube installed it and still the same , then I noticed what looked like a small crack with a D at the side of it which I presumed meant damaged so got onto seller and they sent another tube installed that tube and still the same now I’m thinking psu, any advice would be helpful thanks 





**"Craig Goddard"**

---
---
**Andy Shilling** *May 18, 2018 07:18*

That sounds like the high voltage fly back problem. You can replace these quite cheap but you have to bare in mind they do carry "HIGH VOLTAGE" 



If you open up the psu you will see it at the back with the red HV lead coming out of it, check the numbers and style of it and order a new one and I'm sure you'll be good to go.


---
**Craig Goddard** *May 18, 2018 07:31*

**+Andy Shilling** nice one mate I will have a look at that is there anyway of testing it? Or is it just simply just buy and try 


---
**Andy Shilling** *May 18, 2018 07:41*

**+Craig Goddard** no your can't test it, when mine gave up the ghost the laser would fire at around 5amp down to 1 Amp-ish but after another half hour of trying to work out what was wrong the laser then wouldn't fire at all.



I ended up buying a better psu for an eventual upgrade but also got the flyback as it was only £16 and I now have a spare PSU.


---
**Don Kleinschnitz Jr.** *May 18, 2018 12:21*

**+Craig Goddard** what **+Andy Shilling** said.

Do you hear an kind of hissing or arching from the HVPS?

Does the current meter jump occasionally?


---
**Craig Goddard** *May 18, 2018 16:46*

**+Don Kleinschnitz** yes can here bit off a hiss when firing but can’t see any arching but not taken the cover off the psu yet to have a look, currently in the hospital waiting an X-ray as I dropped a steel plate on my hand, I shall have a look as soon as I’m out thanks for ur help I’ll keep you posted, I’ve not yet come across this problem and the only hpvs I have come across are in China or Japan so will be a couple of weeks when I order one 


---
**Craig Goddard** *May 18, 2018 16:52*

Oh and no doesn’t jump occasionally really miffed me this fault, thought couldn’t be 2 faulty tubes 


---
**Don Kleinschnitz Jr.** *May 18, 2018 17:05*

**+Craig Goddard** the hissing is usually the HVT breaking down. Coincidentally mine just started doing the same thing ...uggh!



As **+Andy Shilling** said you can replace the supply or the HVT. 

Post a picture of your supply showing the green connectors when you can.



I hope your hand turns out to just be a bruise ok ....


---
**Craig Goddard** *May 19, 2018 06:59*

Hands fine just badly bruised here are the pics as requested

![images/1bebf161776ff0fef423c757201bf395.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bebf161776ff0fef423c757201bf395.jpeg)


---
**Craig Goddard** *May 19, 2018 07:00*

![images/6f09baff99c5a398ccba6921fb0c9806.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6f09baff99c5a398ccba6921fb0c9806.jpeg)


---
**Craig Goddard** *May 19, 2018 07:00*

![images/cac87aef8c1a61f7a9a9dc484bea638c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cac87aef8c1a61f7a9a9dc484bea638c.jpeg)


---
**Don Kleinschnitz Jr.** *May 19, 2018 11:36*

We do not know of any way to isolate the LPS from the tube in terms of which is bad. Since the tube is new, its likely the LPS that is bad. 



You can buy a new LPS or replace the HVT. Replacing the HVT is not guaranteed to fix the problem but it does most of the time. 



So you have to decide do you want to <b>change the HVT  for $32:</b>



[https://www.amazon.com/40-50W-Voltage-Flyback-Transformer-Supply/dp/B07BDDD34V/ref=pd_sbs_147_2?_encoding=UTF8&pd_rd_i=B07BDDD34V&pd_rd_r=G17F9CN6NSAKFHVCBCWD&pd_rd_w=b76rX&pd_rd_wg=Jjuy2&psc=1&refRID=G17F9CN6NSAKFHVCBCWD](https://www.amazon.com/40-50W-Voltage-Flyback-Transformer-Supply/dp/B07BDDD34V/ref=pd_sbs_147_2?_encoding=UTF8&pd_rd_i=B07BDDD34V&pd_rd_r=G17F9CN6NSAKFHVCBCWD&pd_rd_w=b76rX&pd_rd_wg=Jjuy2&psc=1&refRID=G17F9CN6NSAKFHVCBCWD)



<b>Or</b> 

The entire supply for approx $100:



[amazon.com - Wisamic 40W PSU Laser Power Supply for CO2 Laser Engraving Cutting Machine - - Amazon.com](https://www.amazon.com/Wisamic-Supply-Engraving-Cutting-Machine/dp/B0771QF637/ref=sr_1_1?ie=UTF8&qid=1526728928&sr=8-1&keywords=k40+laser+power+supply)



........

<i>WARNING: the LPS outputs LETHAL voltages and can kill you. You use this posts advice at your own risk. Follow HV safety and troubleshooting procedures. If you are not qualified working with High Voltages DON'T REPAIR THE LPS.</i>

........



The above part links are for reference and may or may not fit your machine. Check your HVT carefully before buying a replacement.



If you buy a direct replacement for the LPS you will not have to rewire. Make sure the connectors are the same style and configuration.



Repair and other information is here:

[http://donsthings.blogspot.com/2017/07/repairing-k40-lps-1.html](http://donsthings.blogspot.com/2017/07/repairing-k40-lps-1.html)






---
**Craig Goddard** *May 20, 2018 14:54*

Ok mate I’ve ordered a hvt, go cheapest first 👍


---
**Don Kleinschnitz Jr.** *May 20, 2018 16:03*

**+Craig Goddard** OK keep us in the loop.


---
**Craig Goddard** *May 22, 2018 15:16*

**+Don Kleinschnitz** 


---
**Craig Goddard** *May 22, 2018 15:16*

👍


---
**Craig Goddard** *June 06, 2018 07:53*

Hey guys update on this fly back, it’s finally arrived but appears to be the wrong one but the seller says it will fit and sent me wiring instructions which I’m trying to get my head round or you guys may have experience with this and may say yay or na don’t do it? Not sure anyway pics to follow

![images/dd1904a04f3e81f4d9c5377d0da77f33.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dd1904a04f3e81f4d9c5377d0da77f33.jpeg)


---
**Craig Goddard** *June 06, 2018 07:55*

![images/9af314ad2b6d366c6ed95bfbc4008eba.png](https://gitlab.com/funinthefalls/k40/raw/master/images/9af314ad2b6d366c6ed95bfbc4008eba.png)


---
**Craig Goddard** *June 06, 2018 07:55*

![images/6b39f29713151054e03c2cbb7c8950c1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6b39f29713151054e03c2cbb7c8950c1.png)


---
**Craig Goddard** *June 06, 2018 07:59*

The top picture is new and old fly back as you can see mine has 3 wires with a plug which I ordered and they sent me the new one which has one wire and no plug 


---
**Craig Goddard** *June 06, 2018 08:05*

Here is a pic of my psu As I know there a few different models 

![images/7b0a6eb6cbd55cbf8fd28d919c961d97.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b0a6eb6cbd55cbf8fd28d919c961d97.jpeg)


---
**Don Kleinschnitz Jr.** *June 06, 2018 12:37*

**+Craig Goddard** 

No idea why you would be working with the K,L & G signals (inputs) to install a new HVT? Ref: the vendors response.



The input to these transformers (HVT) is in the order of 600V so I would not advise hacking the wrong transformer in. 

Ask them to refund or send you one with the correct connectors.



Post a picture of the bottom of both HVT's pls. I assume the one you got was a PCB mount type and yours screws to the case???






---
**Craig Goddard** *June 06, 2018 13:53*

**+Don Kleinschnitz**  not sure what PCB is but mine screws to the case with plastic 3mm washers I believe the one I purchased did have a 3 pin connector on the end exactly same as mine but the one they sent only has a single black wire, didn’t think it was wise trying to wire it in thanks for the advice I will get back in onto the seller 


---
**Don Kleinschnitz Jr.** *June 06, 2018 14:34*

**+Craig Goddard** PCB = printed circuit board. Some types solder to the board so I guessed that is the wrong one they sent you.


---
**Craig Goddard** *June 20, 2018 16:49*

Hey guys just an update on what’s happening with the laser, the seller finally got me the correct part which looks like one they have bodged together with bare wire showing so sorted that out got it all plugged in switched it on and bang tripped fuses out in house so checked the old one and they had put the wires in wrong, thinking now I should have checked to start with. So sorted that went to put the lid on the psu and bang again I forgot to turn the power off and the lid touched the fuse and earthed 🤦‍♂️,so sorted that out soldered a new fuse in and replaced the fuse in the back of the laser as that blew aswell, switched it on and it all works the power is back to normal so big thanks to you all 👍 took me half a day to re align laser and mirrors then I switched the pc on to have a test run and my pc has gone into power saving mode and nothing will come on the monitor 🤦‍♂️ So I am now trying to sort this out 


---
*Imported from [Google+](https://plus.google.com/117100860646434301952/posts/611dCr4KGhX) &mdash; content and formatting may not be reliable*
