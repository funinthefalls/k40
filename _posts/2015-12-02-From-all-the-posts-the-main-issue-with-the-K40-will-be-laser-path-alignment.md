---
layout: post
title: "From all the posts, the main issue with the K40 will be laser path alignment"
date: December 02, 2015 16:11
category: "Hardware and Laser settings"
author: "Stephane Buisson"
---
From all the posts, the main issue with the K40 will be laser path alignment.

before pointing you out  on the existing tutorials, I would like to underline some evidences which are not so clear to many.



Inspect your hardware before doing any settings. 



- is your head (lens assembly) vertical ?

- is your XY table is “square” ? check if your 2 belts and tensioners for X (moving Y) are in synchro and 90 degree from X.

- does your XY table run smoothly ? vibration will affect your precious settings.

- does your tube strictly parallel to Y ? sometime some foam or rubber are wrap around the tube (some tube could have different diameter). That don’t mean you need to be parallel to the machine frame, but it’s not far off by design.

- does your lens well fitted (centred, convex upward)

- are the optics clean, (don’t take away the gold from glass or the mirror will shatter)



To reach the goal of perfect alignment, you also need to understand each mirror’s job:

Mirror1 function is to set the ray path perfectly parallel to X. (mirror1 to mirror 2 path)

Mirror2 function is to set the ray path perfectly parallel to Y. (mirror2 to mirror 3 path (into head)) 

Mirror 3 is not set-able but the laser head should be vertical 



Understanding of the situation: for a position (X=a, Y=b), you will be able to centered perfectly the spot on the mirror 3 (tape), by correcting slightly the angle from the 2 first mirrors, BUT if the ray isn’t parallel to X and Y, the correction will only be valid for (a,b), this mean you will not be able to have a constant job on all the table. trying to “compensate” method is just a pure waste of time, being parallel to X and Y don’t mean you need to be in the centre of mirror 1 and 2 (common mistake), but 3.



This mean I recommend to start a first setting in the middle of the table (centre on mirror 3), then check parallelism to  X, then Y, then confirm with diagonal. a bit of play in vertical is allowed as distance mirror3 to lens +focal is short.



2 excellent documents to help you with your settings :



[http://dck40.blogspot.co.uk/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html](http://dck40.blogspot.co.uk/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html)



 [www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



to finish, the laser strength is affected by how clean is your optics, and by the chemical reaction into the tube (water temp and electric excitation, too much power saturated the reaction = less strength), a tube wear with time. This why it’s a bit difficult to create a database of settings by material (but still a good idea in ”average”).

![images/fe01d38f1136503369f53306ac4947f4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe01d38f1136503369f53306ac4947f4.jpeg)



**"Stephane Buisson"**

---
---
**Gary McKinnon** *January 13, 2016 12:08*

Great info thanks.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/HXacpbV4ZF9) &mdash; content and formatting may not be reliable*
