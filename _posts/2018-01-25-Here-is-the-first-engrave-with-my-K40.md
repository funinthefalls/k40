---
layout: post
title: "Here is the first engrave with my K40"
date: January 25, 2018 13:01
category: "Object produced with laser"
author: "David Davidson"
---
Here is the first engrave with my K40. So far so great!





**"David Davidson"**

---
---
**Printin Addiction** *January 25, 2018 18:07*

Wood veneer?


---
**David Davidson** *January 25, 2018 22:38*

No, just card stock.




---
**Jakub Loudát** *January 26, 2018 08:54*

Whisperer k40? (who dont know the picture :))


---
**David Davidson** *January 26, 2018 18:41*

Yeah, Whisperer is pretty good.


---
**Ned Hill** *January 26, 2018 20:12*

Nice job and congrats on first burn :)


---
**BEN 3D** *January 28, 2018 11:41*

I am so happy that the k40 whisperer exist ! 


---
*Imported from [Google+](https://plus.google.com/+DavidDavidson-tristar500/posts/XsNugBWncPN) &mdash; content and formatting may not be reliable*
