---
layout: post
title: "Hi all, just a quick one. I got a new K40 laser (new to me) that had a faulty PSU"
date: May 02, 2017 08:39
category: "Hardware and Laser settings"
author: "Colin Rowe"
---
Hi all, just a quick one.

I got a new K40 laser (new to me) that had a faulty PSU.

one of the caps had blow and they did not want to bother with it so took it off them.

I replaced the capacitor and fuse but it keeps blowing fused so shorting some where.

I am getting a new psu but slightly different (not here yet) .

Question is

 on input it has 

FG (ground ?)

L- (to laser via a large green thing)

AC 

AC

question which AC is positive?

Also i get continuity between L- and FG, I checked the tracks on back and it goes to both so why the dedicated L- why not just to ground.

Thanks its doing my head in at the moment.





**"Colin Rowe"**

---
---
**3D Laser** *May 02, 2017 10:05*

Typically it's the transformer that goes on easy fix is just buy a new on off of eBay for 85 dollars 


---
**Don Kleinschnitz Jr.** *May 02, 2017 10:16*

Check out the linked blog for information on LPS's. 



I do not recommend repairing these supplies as the cost  of a new one does not eclipse the cost of parts, time and the danger you expose yourself to.

As **+Corey Budwine** suggests buy a new one .....



FG = frame ground = safety ground

-L is the laser cathodes ground. A separate connection insures that the laser ionizing current has an isolated path back to the LPS ground. This wire and connection is necessary.



Insure that -L is connected to the cathode and FG is connected to the machines frame and the safety ground pin on the plug.



I am not home so I do not know what AC connection is HOT but I do not think it really matters in this supply. I will check when I get home.



[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Joe Alexander** *May 02, 2017 20:06*

the AC is interchangeable, I have tried it both ways. And that large green thing is the load resistor, these are known for failing.


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/GNCFeTBuXS7) &mdash; content and formatting may not be reliable*
