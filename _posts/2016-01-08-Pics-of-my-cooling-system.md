---
layout: post
title: "Pic's of my cooling system"
date: January 08, 2016 02:17
category: "Modification"
author: "Todd Miller"
---
Pic's of my cooling system.



![images/1a7947ec1cf10be26c1444c63f244994.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1a7947ec1cf10be26c1444c63f244994.jpeg)
![images/16b10f79aab9c435bfce14637294ff50.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/16b10f79aab9c435bfce14637294ff50.jpeg)

**"Todd Miller"**

---
---
**I Laser** *January 08, 2016 02:21*

Must be cold where you are, running the water through a rad...


---
**Todd Miller** *January 08, 2016 02:55*

Yes, my basement is my "Lab" where I hang out. On average the temp is about 65f or 18c year around.  Colder in the winter ! Duh ;-)



When I first power up the laser, it's about 16c or so.... I let the cooling pump run for about 20 minutes to stabilize and then do my jobs.



I have yet to see anything above 20c with about 50% load running the laser on longer jobs.



Two fans and a PC radiator seem to work for now.


---
**I Laser** *January 08, 2016 03:02*

Lucky you! Ambient here is anything from 10-40+c. Oh and we don't do basements in Australia.



I was initially looking at using an old swiftech water cooling setup I had lying around but it's too hot here to be of use. I'm routinely throwing frozen 3 litre bottles into my cooling tank just to get temps below 20c lol.


---
**Todd Miller** *January 08, 2016 03:22*

No basements ? 



Where do you go when it Tornado's ?  ;-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 08, 2016 05:26*

Pretty nice setup. Looks colourful too :D


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/AbHqw9s2sgt) &mdash; content and formatting may not be reliable*
