---
layout: post
title: "Can anyone tell me how to get coreldraw x8 to work with my k40 laser"
date: July 22, 2016 13:56
category: "Hardware and Laser settings"
author: "Stevie Rodger"
---
Can anyone tell me how to get coreldraw x8 to work with my k40 laser. Thanks Stevie





**"Stevie Rodger"**

---
---
**Ariel Yahni (UniKpty)** *July 22, 2016 15:37*

**+Stevie Rodger**​ what exactly do you need


---
**Stevie Rodger** *July 22, 2016 20:35*

I am unable to send anything to the laser. I have no access to any toolbar or control to send to. I have Corel 12 installed and it works with the laser toolbar, but nothing for Corel 12. I hope I have explained things clear enough, and hope you can help as I would like to use corel x8 with my k40. Thanks.


---
**Ariel Yahni (UniKpty)** *July 22, 2016 20:48*

**+Stevie Rodger** Its all about the laser plugin. I would recommend uninstall every version of corel that you have plus the plugin, restart the computer and only install the version you want


---
**Stevie Rodger** *July 22, 2016 21:18*

Thanks Ariel, i'll give it a go and see how i get on. I'll let you know the result.


---
**Ariel Yahni (UniKpty)** *July 22, 2016 21:27*

I learned from the best, **+Yuusuf Sallahuddin**​


---
**Ned Hill** *July 22, 2016 23:31*

**+Stevie Rodger** Do you have the home or pro version of Corel x8?  I understand there is an issue with trying to use home versions of CorelDraw with the plugin.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 04, 2016 13:31*

**+Stevie Rodger** Did you manage to get x8 to work with CorelLaser? Another user **+Bob Damato** is considering purchasing it & wanting to know if it works.


---
**Camby Crafts** *September 18, 2016 13:12*

no. i couldn't get it to work. sorry for the late reply.


---
*Imported from [Google+](https://plus.google.com/+StevieRodgerCambyCrafts/posts/XhYFBN9XS6x) &mdash; content and formatting may not be reliable*
