---
layout: post
title: "Is there anyone in this space that would be in the Las Vegas area August 9th-12th?"
date: August 02, 2018 09:47
category: "Discussion"
author: "Eric Lovejoy"
---
Is there anyone in this space that would be in the Las Vegas area August 9th-12th?  The Laser Cutting Village @DEFCON is reaching out to the community for support. 



[https://www.defcon.org/html/defcon-26/dc-26-villages.html](https://www.defcon.org/html/defcon-26/dc-26-villages.html)





![images/69393fc4d1d0606050af4ef5c78a062d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/69393fc4d1d0606050af4ef5c78a062d.png)



**"Eric Lovejoy"**

---
---
**HalfNormal** *August 02, 2018 12:31*

Full Spectrum Laser is based out of Las Vegas. They should reach out to them. I wish I had known about this sooner. I am only 4 hours away from Vegas. 


---
**Eric Lovejoy** *August 02, 2018 13:14*

**+HalfNormal** Hey maybe we will see you out there! I wish i'd thought of posting this sooner too.




---
*Imported from [Google+](https://plus.google.com/107285166924069729045/posts/8yYFHFTG23w) &mdash; content and formatting may not be reliable*
