---
layout: post
title: "Ariel Yahni Can't add pics to the comments for some reason"
date: June 07, 2016 01:01
category: "Smoothieboard Modification"
author: "Derek Schuetz"
---
**+Ariel Yahni** 



Can't add pics to the comments for some reason 



![images/4f4913a14455c32366184f7c5afa8493.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f4913a14455c32366184f7c5afa8493.jpeg)
![images/51d967f9a372a5fe08f6bc676b69b667.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/51d967f9a372a5fe08f6bc676b69b667.jpeg)

**"Derek Schuetz"**

---
---
**Ariel Yahni (UniKpty)** *June 07, 2016 01:04*

Need the picture of the board from the laser


---
**Ariel Yahni (UniKpty)** *June 07, 2016 01:04*

Use the service [imgur.com](http://imgur.com) to upload and then share link


---
**Derek Schuetz** *June 07, 2016 01:07*

What do you need from the board of the laser I don't think I have it anymore?


---
**Ariel Yahni (UniKpty)** *June 07, 2016 01:11*

ooooo. Do you know how to connect the motors, endstops ? I see in the picture a ribbon?


---
**Derek Schuetz** *June 07, 2016 01:13*

Ya I already have it upgraded with ramps. So I know all that I just don't know how to hook up the fire and pwm to a smoothie board like this one and I am seeing different opinions regarding a logic board to get the needed 5v 


---
**Ariel Yahni (UniKpty)** *June 07, 2016 01:15*

Ok so like i said. I have it running on laser on but controlling power from POT still. like this [http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html#comment-form](http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html#comment-form)


---
**Derek Schuetz** *June 07, 2016 01:32*

Oh ok so you set power via potentiometer still not through firmware


---
**Ariel Yahni (UniKpty)** *June 07, 2016 01:39*

Yes. I'm doing the rest as we speak


---
**Stephane Buisson** *June 07, 2016 07:31*

**+Derek Schuetz** the pot set the max value (ceiling to protect the tube lifespan), not the the job intensity. You set the PWM value from the software.


---
**Ariel Yahni (UniKpty)** *June 07, 2016 11:27*

**+Stephane Buisson**​ do you mean that you put the pot to a safe value and then let the software control it up to there? 


---
**Stephane Buisson** *June 07, 2016 12:33*

**+Ariel Yahni** yes, that's the idea. the PWM is all or nothing (0-5V) at a %  of time,  5 V max voltage trigger is hard on the tube, keeping the pot allow to reduce the final voltage and save the tube. to have the same "burn", more laser pulse but less strong, and you are kind with your cutter. (doesn't increase job time).

you increase the pot, time to time for exceptional job or when the tube will be tired(old).



but i don't engrave, just cut for my automata.


---
**Ariel Yahni (UniKpty)** *June 07, 2016 12:40*

**+Stephane Buisson**​ from your guide you don't connect the IN anywhere. You just pass the 5V(via shifter)  and L to the PIN. 


---
**Stephane Buisson** *June 07, 2016 13:56*

**+Ariel Yahni** yes, it's 2 ways to connect, I just reused the way Moshiboard was connected originally (like test fire). but other way (in) are good too.


---
**Ariel Yahni (UniKpty)** *June 07, 2016 13:59*

**+Stephane Buisson**​ the thing I'd if I connect L only to the PWM and set the pot to a value, that's the power I get to the max. So if I put 5mA I get constant 5mA


---
**Stephane Buisson** *June 07, 2016 14:36*

"So if I put 5mA I get constant 5mA" ??? 

mA is on the HV line, I am not putting my finger around... (Vue meter in serie on HV line)



V on pot vary from 0-5V

the equivalent for 15mA (tube protection) would be around 3.6V (more than 3.3V -> need level shifter, all from memory, I did that 9 months ago)


---
**Stephane Buisson** *June 07, 2016 14:42*

**+Ariel Yahni** somebody notice as well to change a resistance value at the end of the comments here :

[https://plus.google.com/114978037514571097180/posts/cFoouZqQAng](https://plus.google.com/114978037514571097180/posts/cFoouZqQAng)


---
**Derek Schuetz** *June 07, 2016 14:44*

**+Stephane Buisson** you can't set a voltage cap in the config file to reduce the pins voltage?


---
**Stephane Buisson** *June 07, 2016 14:51*

**+Derek Schuetz** in my case the 5V is out of Laser PSU not  out the smoothie


---
**Derek Schuetz** *June 07, 2016 14:59*

**+Stephane Buisson** then PWM is not being controlled though the smoothie then? Or is it just that much different then ramps


---
**Stephane Buisson** *June 07, 2016 15:04*

**+Derek Schuetz** PWM connected on L (not IN) in my case.

we are mixing 2 different things here. command (pulse) and V for intensity, maybe I am wrong for both to be considered on IN at once. (your case)



latest info from JNMYDY



[http://www.jnmydy.com/comcontent_detail/&FrontComContent_list01-1285720952066ContId=6&comContentId=6.html](http://www.jnmydy.com/comcontent_detail/&FrontComContent_list01-1285720952066ContId=6&comContentId=6.html)


---
**Derek Schuetz** *June 07, 2016 15:22*

Ok I may just be confusing the terms but if it's working its working. Unfortunately the board I ordered is in responsive I think it has nothing flashed onto it and needs to be hooked up with a UART module and reconfigured 


---
**Ariel Yahni (UniKpty)** *June 07, 2016 15:25*

**+Stephane Buisson**​ I meant if I set the pot dial to 5mA with your config my max would be 5mA. But when I run a job I only the max power to 5mA.that being said I just realized that I'm setting 100% power on LW, so if I set LW to something bellow the equivalent to 5mA I should get less power? 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/eiFakccCunp) &mdash; content and formatting may not be reliable*
