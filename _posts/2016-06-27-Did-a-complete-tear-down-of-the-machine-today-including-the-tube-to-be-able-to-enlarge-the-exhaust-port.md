---
layout: post
title: "Did a complete tear down of the machine today, including the tube, to be able to enlarge the exhaust port"
date: June 27, 2016 20:15
category: "Discussion"
author: "Mircea Russu"
---
Did a complete tear down of the machine today, including the tube, to be able to enlarge the exhaust port. Now using 150mm(6") tubing with my inline fan (341cfm), what a difference does it make!

Assembled all back, tried to realign mirrors and the problems start: -first mirror hits Y mirror dead center all along Y axis. 

-Y mirror adjusted to hit same place on the head along X axis is only valid for the front position, when pushing X axis in the back the laser hits a little down and to the left.

So it hits in the same place in 3 corners: left rear, left front, right front. Hits off to the left by  0.5mm and down by 1mm at right rear.

Removed gantry several times, Y axis are perfectly parallel, X axis is square to Y axis, measured by corner and vernier, it seems that somehow the right part of the X axis raises when approaching the idler.

The right side has only one wheel in the profile which goes down the center of the guide path, it has 1-2mm slack up/down. Should I try to make it push against the upper or lower wall of the gliding path?





**"Mircea Russu"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 20:33*

Someone else had this issue just a few days ago with their TopRight corner was not hitting the same spot. I can't remember the solution, but someone suggested checking the tube is level I think. Might be that when you removed the vent the machines casing now warps (less structural integrity?).


---
**Mircea Russu** *June 27, 2016 20:56*

**+Yuusuf Sallahuddin** I enlarged the hole with a jigsaw without cutting any support structure. The case is wobbly as it is, enlarging that hole by 1" did not seem to make any difference. As long as the beam is parallel to the Y axis I can't see what the level of the tube  might change. I believe is the belt that is raising the right part of the X axis but maybe someone else has been here before.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 21:02*

**+Mircea Russu** I've noticed that the base of my case (in the cutting area) is particularly wobbly. It likes to flex. Could it be that the top right corner region is not level with the other 3 corners?



I assume you didn't have this issue before chopping the hole bigger for the exhaust, so that leads to the assumption that something happened during that process. Hopefully something simple to fix :)


---
**Mircea Russu** *June 27, 2016 21:19*

All measurements were taken with the gantry out of the case. The rear right screw is a little tight inserting. Maybe that flexes the gantry. Will replace tomorrow with narrower one ant check. Thank you.


---
**Phillip Conroy** *June 27, 2016 21:25*

when you removed the tube it is inpossable to get same tension on the boltsholding it down so the tube hight has changed..i used 4 layers of elec tape to pack the bottom of one end of the tube -beam must hit center of firs mirror.aligning the mirrors are a pain.on my machine i put up with only having full power over 12 of cutting bed for 6 months as that was all i needed,when i blue a tube the new one was in a more aligned possition just by dropping it in.that makes me think over time the rubber insulator that goes around the tube looses its hight a very small amount under compression


---
**Stephen Sedgwick** *June 27, 2016 23:23*

The issue he reported in the end was the laser wasn't aligned completely - it was little off alignment itself.


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/WGxoPtEpTw2) &mdash; content and formatting may not be reliable*
