---
layout: post
title: "Originally shared by Alex Krause Started noticing a bunch of shifting in my images..."
date: September 06, 2016 18:29
category: "Hardware and Laser settings"
author: "Alex Krause"
---
<b>Originally shared by Alex Krause</b>



Started noticing a bunch of shifting in my images... I thought I was missing steps at first... so I checked my X axis belt tension on my k40 and felt a ton of slack in the belt. So I jogged the head to where the hole that leads to the electronics pannel is and tightened both screws that I circled in the second image a 1/4 turn at a time until the belt was where it should be... fingers crossed that this fixes my problem and remember don't over tighten the belt



![images/9294f39d9cf3cce1d4bc94180ce8384d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9294f39d9cf3cce1d4bc94180ce8384d.jpeg)
![images/09fd3171f286aff8c6b7a2f7b19a956b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09fd3171f286aff8c6b7a2f7b19a956b.jpeg)

**"Alex Krause"**

---
---
**Jim Hatch** *September 06, 2016 18:47*

Next time, add a drop of Loctite (blue) to the screws and then tighten them. You can remove them one at a time for that without misaligning things.




---
**greg greene** *September 06, 2016 23:52*

I have a silly question - where are the screws located on the machinery?


---
**Alex Krause** *September 07, 2016 00:03*

On the end of the gantry rail "bronze rail" and can be accessed thru the hole on the right hand side  of the cutting compartment that leads to the electronics compartment


---
**greg greene** *September 07, 2016 00:04*

Ah I see it now - thanks very much for the info !


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 07, 2016 10:19*

I had the same problem with mine just the other day. I created a Macro in LW3 to job to the specific position (about Y120 or Y125) for access to that tensioner.


---
**Amer Al fk** *September 22, 2016 10:37*

hi any  what causes of doing that attaced photo  ? i am trying to post my issuer  on communities but not going throw for some reasons   

![images/e9cc96d651551de2c5411d77a84a2209.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9cc96d651551de2c5411d77a84a2209.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 22, 2016 13:52*

**+Amer Al fk** That looks similar to what has happened for Alex in the original post. Basically caused by either of 2 things from my experience:

- belt tension is not correct (too loose usually) & it is skipping steps (causing the shift in position)

- running the engrave speed too high (usually not an issue up to 500mm/s with the stock software unless the belt tension is too loose)



To correct the issue, like Alex says you can access the screws on the far right of the X-axis rail that tighten the tension on the belt. Note: it will be a bit of trial & error to get the right tension.


---
**Amer Al fk** *September 22, 2016 22:52*

thank you **+Alex Krause** thank you **+Yuusuf Sallahuddin** problem close to solve iam trying to tight the screws looks like its work fine on 150mm speed but still not on 100 so i need to do more trial thank you that is really help even seller could not help  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 23, 2016 08:57*

**+Amer Al fk** The sellers generally can't help with anything, they usually just offer refunds (if it is early after you purchased it) because they have no idea what to do or how it works. This community is where I learned a lot of the fiddly things on how to operate my machine, so it's a great source for getting yourself up & running & keeping things that way :)


---
**Richard Gallatin** *June 18, 2017 18:14*

Alex I am starting to get the same thing. Did this fix your problem? 




---
**Alex Krause** *June 18, 2017 18:22*

**+Richard Gallatin**​ that was one of the problems, but I think it ultimately boils down to a few other problems such as the 24v supplied by the laser power supply and emi... I have my smoothieboard plugged into the same circuit as my air conditioning unit and notice this happens when the blower motor trips on and there is a high inductive load... But I was seeing some slip in the same spot that was repeatable, those small 1-2mm shifts we're caused by that... I also notice that that acceleration and jerk settings in the controller can make this worse if set to high


---
**Richard Gallatin** *June 18, 2017 21:36*

OK I am also using a smoothieboard. I friend told me mine coud be to high of a mm/min value for engraving. Getting ready to try a lower number. 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/dpmg3WJZB8x) &mdash; content and formatting may not be reliable*
