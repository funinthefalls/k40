---
layout: post
title: "Have noticed this the odd time when cutting but it's really obvious now with a larger engraving"
date: March 11, 2017 14:31
category: "Original software and hardware issues"
author: "Nigel Conroy"
---
Have noticed this the odd time when cutting but it's really obvious now with a larger engraving.

There seems to be 2 dead spots or lines where the laser hasn't cut....

Any suggestions or ideas?



![images/7ff03d4be451ce5cf36862eeb22474fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7ff03d4be451ce5cf36862eeb22474fb.jpeg)
![images/5abe0ce047913d7c3c8e85b580ff3b4c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5abe0ce047913d7c3c8e85b580ff3b4c.jpeg)

**"Nigel Conroy"**

---
---
**Anthony Bolgar** *March 11, 2017 14:39*

It seems like the belt is skippin over those areas faster than the laser can fire. Does it happen if you move the work piece to a difgferent are a of the table?


---
**greg greene** *March 11, 2017 14:39*

a difference in wood density at those spots?


---
**Nigel Conroy** *March 11, 2017 14:43*

I thought it might be something to do with the belt also.

Missing teeth or worn?



This is the first large engraving I've done to really be able to see the effect in the whole piece.



I don't think it's the wood as it seems to have not engraved at all rather then shallower if the wood density was different, but I'll do another piece and see if the same thing occurs.


---
**Nate Caine** *March 11, 2017 15:53*

Do another piece with a test area.  If you see similar lines, good!  Now move the wood, and repeat the test.  If you see a similar pattern in the wood check to see if the irregular areas line up.  If the do, that would indicate a wood density inconsistency.  If the irregular pattern is at the same place relative to the machine edge, then you might look for a machine flaw such as those you mentioned.


---
**Ned Hill** *March 11, 2017 18:29*

Looks like wood density to me.  You can see the lines running down through piece at a angle into other engraved areas.  Is this plywood?  In plywood you sometimes get areas with higher glue density which is harder to laser.


---
**Mark Brown** *March 11, 2017 20:54*

Those lines aren't perfectly vertical (assuming text as vertical reference), which would make me think it's not a defect in the belt.



I second Nate's advice. Test, move, test, compare.


---
**Nigel Conroy** *March 12, 2017 13:07*

Yes it is 1/4 underlay plywood.

They're slightly off vertical, but but much.

I will rerun the same engrave tomorrow and see if the same result occurs in the same spot.


---
**Nigel Conroy** *March 14, 2017 04:54*

Ran the same engrave pattern on a different piece of plywood with same thickness.

Interesting result, got a similar line but in a different place.

It does make me think it could be the wood density but not sure.

![images/6db77cdc8daed8546e430dbafbf07299.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6db77cdc8daed8546e430dbafbf07299.jpeg)


---
**Nigel Conroy** *March 14, 2017 04:55*

![images/546059744f2d95261d21c89e2200f169.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/546059744f2d95261d21c89e2200f169.jpeg)


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/FFgHe7bcUUW) &mdash; content and formatting may not be reliable*
