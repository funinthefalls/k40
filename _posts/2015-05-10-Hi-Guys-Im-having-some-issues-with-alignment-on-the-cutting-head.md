---
layout: post
title: "Hi Guys, I'm having some issues with alignment on the cutting head"
date: May 10, 2015 20:04
category: "Discussion"
author: "David Wakely"
---
Hi Guys,



I'm having some issues with alignment on the cutting head. The beam is perfectly aligned from the fixed mirror to the Y axis and when it hits the cutting head it seems to be to roughly in the center in all four corners of the stage. The problem I'm having is the beam exiting the head is hitting the air assist nozzle, it seems to be to far right. If i remove the nozzle part it cuts like a dream! Any ideas what the hell is going on as I'm totally lost!





**"David Wakely"**

---
---
**Stuart Middleton** *May 10, 2015 21:07*

It may sound crazy but have you checked the lens it seated properly?


---
**David Wakely** *May 10, 2015 21:33*

No I haven't checked the lens. I'll have a look tommorow. Could that cause this type of issue? Also what way should the lens go, I've seen before curved side up but it's really hard to tell which side is curved! Any other tips for making sure the lens is the correct way?


---
**Sean Cherven** *May 10, 2015 23:48*

I believe the curved side is the reflective side. And after you check the lens, if the issue still exists, you can adjust the actual laser tube up or down to change the exit path out of the nozzle. Also, adjusting the cutting head too (depending on what way it needs to be adjusted)


---
**David Wakely** *May 11, 2015 07:09*

Thanks all for your info. I'll check the lens later and adjust tube and let you know how I get on!


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/YLWtMUgNy3j) &mdash; content and formatting may not be reliable*
