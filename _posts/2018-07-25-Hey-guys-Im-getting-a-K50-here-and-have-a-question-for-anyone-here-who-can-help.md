---
layout: post
title: "Hey guys I'm getting a K50 here and have a question for anyone here who can help"
date: July 25, 2018 19:10
category: "Software"
author: "Nathan Thomas"
---
Hey guys 

I'm getting a K50 here and have a question for anyone here who can help.



I see it uses RDWORKS, can I save my corel files in ai or dxf format and go straight to the machine using the usb interface...bypassing RDWORKS?

Or is it mandatory to download/use RDWORKS to connect and run jobs?



I've had a couple K40s that I use with corel and corel laser. Didn't want to learn or figure out a new software if I didn't have to.





**"Nathan Thomas"**

---
---
**HalfNormal** *July 25, 2018 19:27*

RDWORKS  Imports a variety of file formats.  Personally I've been using LighBurn with my Rudia controller. You have to use software that talks to the Rudia controller. 


---
**Nathan Thomas** *July 25, 2018 19:57*

Gotcha...but can I skip RDWORKS and just run a job directly from the usb interface on the machine?



For example, do my design in CD...save it to a usb in ai or dxf format...then plug the usb into the machine...and run the job?



Or is it a must to go through RDWORKS to run jobs?


---
**HalfNormal** *July 25, 2018 20:35*

Yes the Ruidia controller allows you to do that


---
**HalfNormal** *July 25, 2018 20:37*

You have to set the parameters in the controller


---
**Nathan Thomas** *July 25, 2018 20:38*

Awesome :)

Is there a video somewhere that will show me how to do it...how to find the file on the USB ising the stock k50 control board?



I've searched all over YouTube and don't see a specific video on this 


---
**HalfNormal** *July 26, 2018 00:05*

This site has a manual you can download that has been cleaned up. You have to sign up then it is a free download  [https://rdworkslab.com](https://rdworkslab.com)

[rdworkslab.com - RDWorksLab.com - Index page](https://rdworkslab.com)


---
**HalfNormal** *July 26, 2018 03:55*

I found this video on YouTube that might help. It is quick and simple.


{% include youtubePlayer.html id="inXa7p46Mr8" %}
[youtube.com - How to save and send file to our laser through U- disk](https://www.youtube.com/watch?v=inXa7p46Mr8)


---
**Anthony Bolgar** *July 26, 2018 11:07*

I would download the free trial of LightBurn, it replaces RDWorks as the software for Ruida controllers that run RDWorks. Much easier to learn and a way better software package. You cn get the trial at Lightburnsoftware.com,nd there is a facbook support goup as well as a google+ group. Written in the US with English support, and documentation written in English, not translated Chinglish. :)


---
**Nathan Thomas** *July 26, 2018 12:54*

**+HalfNormal** appreciate it, I'll try it this am


---
**Nathan Thomas** *July 26, 2018 12:55*

**+Anthony Bolgar** I saw that and was going to ask in a separate post if anyone tried it and how they like it 


---
**Anthony Bolgar** *July 26, 2018 15:06*

Everyone who tries it loves it. Make sure to join the facebook group.


---
**HalfNormal** *July 26, 2018 20:00*

LightBurn is all I use.


---
**Jason Todd** *August 22, 2018 01:32*

LightBurn is a breath of fresh air in a sea of frustrating software options written by people that seem to be trying to reinvent the wheel! I'm glad I found it


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/5uCokjMzE3b) &mdash; content and formatting may not be reliable*
