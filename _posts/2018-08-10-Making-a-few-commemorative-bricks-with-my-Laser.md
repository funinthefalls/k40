---
layout: post
title: "Making a few commemorative bricks with my Laser!"
date: August 10, 2018 00:04
category: "Object produced with laser"
author: "Bob Damato"
---
Making a few commemorative bricks with my Laser! 





![images/9f8cc1aa578f245bd4c38168b65cd895.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9f8cc1aa578f245bd4c38168b65cd895.jpeg)



**"Bob Damato"**

---
---
**Ned Hill** *August 10, 2018 02:13*

Nice.  Is that just a normal brick?


---
**Bob Damato** *August 10, 2018 13:01*

Hi Ned, no it is a special one with a high glass content that can be melted with the laser. It otherwise looks and acts like a regular brick though and lasts just as long. The graphic is permanent 


---
**Stephane Buisson** *August 10, 2018 13:37*

that's could be handy for my next project :

[maslowcnc.com - Home](https://www.maslowcnc.com/)


---
**Nigel Conroy** *August 21, 2018 20:31*

Nice do you have a link to the product?




---
**Bob Damato** *August 22, 2018 22:15*

**+Nigel Conroy** [lasersketch.com - LaserSketch.com - Laser Engravable Products - Title Page](http://www.lasersketch.com/)


---
**Nigel Conroy** *August 22, 2018 22:21*

Thanks **+Bob Damato** 


---
**Bob Damato** *August 22, 2018 22:34*

**+Nigel Conroy** No problem! Its a cheesy website with typos, but hes really great to deal with and responsive. 


---
**Fat Dad Custom Designs** *August 24, 2018 18:40*

What settings did you use?


---
**Bob Damato** *August 24, 2018 21:56*

I think I used speed 10 and power right about at 80%. The brick was noticeably warm when it was done. 


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/MS8tDs9oTZP) &mdash; content and formatting may not be reliable*
