---
layout: post
title: "Hi. I use a CAD software to design part to be cut"
date: February 07, 2016 21:08
category: "Software"
author: "Sylvain Maubleu"
---
 

Hi.

I use a CAD software to design part to be cut. I have a K40 laser machine and CorelLaser and Laserdraw to do the cutting. I do not have a Smoothie board just the serial USB interface.

The DXF files exported from CAD are correctely imported in CorelLaser, but every non closed curves are cut on both side of the curve. 

On the other hand, all closed curves (ellipse, circles) or surfaces are cute only once.



All my curves thikness is "Airline", I thought it should have done the job but no.



Do you guies know a mean to have the open curves cut only once? 





**"Sylvain Maubleu"**

---
---
**Thor Johnson** *February 07, 2016 21:25*

The laser software is pretty obtuse; it's seeing a black->white transition and is cutting "both sides" of your open shape.  Before I got CorelLaser running, I had to "fill in" the shapes  with black so it would only cut once (using LaserDRW).  Anyone have more tips (I've only done closed surfaces with CorelLaser)?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 08, 2016 10:33*

I've heard some people suggest a specific line width for open shapes. I can't remember precisely what it is, but I think it was 0.1 line width maybe. Have a search through the group because there is definitely people having mentioned this exact problem before.


---
**Sylvain Maubleu** *February 08, 2016 19:48*

Ok O i tried with the laser offline, and by putting the line thickness to 0.01mm it works. I did not know the value could be entered. 

The "Airline" thickness" is not slim enough. Thancks


---
*Imported from [Google+](https://plus.google.com/102079287613237321930/posts/3BjzSSNw8Yq) &mdash; content and formatting may not be reliable*
