---
layout: post
title: "Shop tip- When you need to precisely align a workpiece for engraving, especially for an odd shape or when engraving close to an edge, give this a try"
date: December 02, 2018 06:43
category: "Discussion"
author: "Ned Hill"
---
Shop tip-

When you need to precisely align a workpiece for engraving, especially for an odd shape or when engraving close to an edge, give this a try. First place multiple layers of painters tape on the engraving surface and trim off the excess. Create a cut layer that is a little smaller than engraving surface and then run the cut fast with low power. The goal is to have the cut not penetrate the bottom tape layer and thereby leave a mark you can see for alignment checking without marking the actual surface. #k40shoptips



![images/293b96da34409e81871b7e0c2a8b81bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/293b96da34409e81871b7e0c2a8b81bd.jpeg)
![images/7a4389c19132f4e61a5925b752fe1b54.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a4389c19132f4e61a5925b752fe1b54.jpeg)
![images/2b654ae909038a91cf8fec7955e80848.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b654ae909038a91cf8fec7955e80848.jpeg)
![images/5df8bcf5ca82949bb245b5f90ebaa7a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5df8bcf5ca82949bb245b5f90ebaa7a6.jpeg)

**"Ned Hill"**

---
---
**Kevin Lease** *December 03, 2018 10:18*

As an alternative, tape a piece of cardboard to the bed, cut out the shape of object from the cardboard and remove the cut out piece of cardboard Then press your workpiece in the cardboard void to both align your piece and keep it from moving


---
**Benjamin Brewer** *December 10, 2018 20:14*

Thank you.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/bwcCZtgZ88D) &mdash; content and formatting may not be reliable*
