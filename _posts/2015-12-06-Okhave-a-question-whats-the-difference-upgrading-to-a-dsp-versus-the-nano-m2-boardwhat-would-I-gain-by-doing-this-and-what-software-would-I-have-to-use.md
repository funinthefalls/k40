---
layout: post
title: "Ok....have a question, what's the difference upgrading to a dsp versus the nano m2 board...what would I gain by doing this and what software would I have to use?"
date: December 06, 2015 15:14
category: "Discussion"
author: "Scott Thorne"
---
Ok....have a question, what's the difference upgrading to a dsp versus the nano m2 board...what would I gain by doing this and what software would I have to use?





**"Scott Thorne"**

---
---
**Sean Cherven** *December 06, 2015 17:32*

You would gain a LOT, but I just can't justify the cost of it right now. I'm holding off for now, but that's just me.


---
**Scott Thorne** *December 06, 2015 18:23*

**+Sean Cherven**​....that's just me to man....if I'm going to do all that then I'm going to go ahead and get a boss laser engraver for 3400.00


---
**FNG Services** *December 07, 2015 00:53*

Same here, I think I would rather put that $500-600 towards I bigger better laser.


---
**Stephane Buisson** *December 07, 2015 16:35*

but what if your upgrade was about or sub 150$ ? powerful as a dsp (or more) and allow you to use Visicut. (Smoothie compatible board), maybe the one in preparation by **+Peter van der Walt** (price unknown).

Are you in a hurry ? (-> original Smoothie board 4XC)


---
**Sean Cherven** *December 07, 2015 18:38*

Will this board he is making be a direct replacement? Meaning plug & play, no need to cut and splice wires?


---
**Stephane Buisson** *December 07, 2015 22:53*

**+Sean Cherven** you should follow his work. Peter said he would named the connections like the one on the psu, so it should be straight forward. 


---
**Scott Thorne** *December 07, 2015 23:36*

**+Stephane Buisson**...then I would consider it but keep in mind that I know very little about smoothieboards


---
**Scott Thorne** *December 07, 2015 23:39*

**+Stephane Buisson**...and not to sound stupid but what exactly is visicut?


---
**Stephane Buisson** *December 08, 2015 00:47*

a free software to drive your laser cutter.

Place your design on the table, choose the job (mark, cut engrave, or combination of those) very simply (like red line is cut, green is engrave, blue is mark, (your setting choice).

pre-settings by materials and thickness.

but also plenty more, like superposition of design on webcam image (ex: a logo on iPhone). plugin for illustrator or inkscape, scriptable, ... all that for free. (byebye Moshi and other chinese incoherences)

I will try to do a post about it.


---
**Scott Thorne** *December 08, 2015 01:54*

**+Stephane Buisson**....cool...thanks...I might just give it a try.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/9LTznC71dKa) &mdash; content and formatting may not be reliable*
