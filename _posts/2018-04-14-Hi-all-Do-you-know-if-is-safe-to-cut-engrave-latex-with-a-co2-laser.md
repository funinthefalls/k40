---
layout: post
title: "Hi all. Do you know if is safe to cut/engrave latex with a co2 laser?"
date: April 14, 2018 15:52
category: "Materials and settings"
author: "syknarf"
---
Hi all.

Do you know if is safe to cut/engrave latex with a co2 laser?

 I'm thinking in use liquid latex to coat acrylic sheets before engraving it, using the latex like a masking tape for paint the engraved zones of the acrylic.





**"syknarf"**

---
---
**Ned Hill** *April 14, 2018 21:06*

latex is fine to laser.


---
**syknarf** *April 16, 2018 19:31*

**+Ned Hill** Thanks for the info !


---
*Imported from [Google+](https://plus.google.com/113055105026792621247/posts/6QWd5z4mdCP) &mdash; content and formatting may not be reliable*
