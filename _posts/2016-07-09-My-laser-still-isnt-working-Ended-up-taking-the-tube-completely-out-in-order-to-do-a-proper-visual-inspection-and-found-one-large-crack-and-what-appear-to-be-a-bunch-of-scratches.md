---
layout: post
title: "My laser still isn't working. Ended up taking the tube completely out in order to do a proper visual inspection, and found one large crack and what appear to be a bunch of scratches"
date: July 09, 2016 03:02
category: "Original software and hardware issues"
author: "Evan Fosmark"
---
My laser still isn't working. Ended up taking the tube completely out in order to do a proper visual inspection, and found one large crack and what appear to be a bunch of scratches. Video outlines my findings.



Hoping the seller will get me a new tube.﻿





**"Evan Fosmark"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 09, 2016 05:07*

Hope the seller comes to the party & sorts it out for you.


---
**Evan Fosmark** *July 11, 2016 15:28*

Confirmed! They're shipping me a new one! 


---
**Robert Selvey** *July 31, 2016 00:30*

That's great to hear that they are going to send you a new one. I just got the same laser you have and was wondering how do you read the power display is 00.5 ma and 20.0 ma ?


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/AKVEfCfqMLc) &mdash; content and formatting may not be reliable*
