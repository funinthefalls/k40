---
layout: post
title: "While I was unable to get the Turnkey Inkscape plugin to work for me, decided to finish up the wiring and gut out unnecessary parts"
date: December 09, 2015 03:55
category: "Modification"
author: "ChiRag Chaudhari"
---
While I was unable to get the Turnkey Inkscape plugin to work for me, decided to finish up the wiring and gut out unnecessary parts. 



> Used some thin pine to cover sharp  the edges of power supply, dont want to get 20,000 V shock.

>Tried to use mostly all the silicon wires  came with CC3D flight controller as they are really flexible. Removed the pot of course,  I think we can use 5V out from there as well !!! B)

> For now I am keeping both Laser ON and Laser Fire switch.

> Using some thick double sided tape mount the Arduino and Ramps right on the Power supply.

> Passed those to 10 pin flat cables under the hinge part, good enough room they dont get stripped and lose mounted LCD controller on top of the control panel.

> Lose a bit weight of the machine.



Next up:

> Mount the Power supply back the way it was.

> Design and cut new Control Panel that fits every thing and looks nice.

> Waiting for LIghtObjects Laser Head to arrive.

> Need to purchase water flow Indicator/Sensor and Optical Limit switches, Lid switch ..... 

> Air compressor and  a blower fan (thinking about using my leaf blower)



But now its time to get my hands on the Inkscape Plugin!



![images/a0fb0e78213f2f10477beb51267c06f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0fb0e78213f2f10477beb51267c06f0.jpeg)
![images/cb15a244ffe02203c02069b0e5b83452.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cb15a244ffe02203c02069b0e5b83452.jpeg)
![images/873a90ac851de08124f4607e3214dc76.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/873a90ac851de08124f4607e3214dc76.jpeg)
![images/9e5c05453a73ee5ce18e473b5b04d4bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e5c05453a73ee5ce18e473b5b04d4bc.jpeg)
![images/3bd73a76116d8d18059b6c6f213fe310.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3bd73a76116d8d18059b6c6f213fe310.jpeg)
![images/5f91e8d0eedcafbd0fef57e983ed71f7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5f91e8d0eedcafbd0fef57e983ed71f7.jpeg)
![images/49eec239bca07716d198a884b3068a6b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/49eec239bca07716d198a884b3068a6b.jpeg)

**"ChiRag Chaudhari"**

---
---
**Anthony Bolgar** *December 09, 2015 06:27*

Looking good.....one issue that I see however, mounting the ramps board to the PSU may lead to problems with EMI. Would be much better to mount it to the plate the original controller board was mounted to{use standoffs to isolate from the metal plate}


---
**ChiRag Chaudhari** *December 09, 2015 06:39*

**+Anthony Bolgar** Good one. Did not think about that. Where there is power there is EMI,... a lot of it. Glad I just used double sided tape to mount that. Originally I was going to drill the top cover of power supply and mount it right there. 



Question, does a piece of thick metal or carbon fiber sheet can reduce/prevent interference ?


---
**Anthony Bolgar** *December 09, 2015 07:00*

It can but it can also act as a wave guide or as a reflective element not unlike some antenna designs. Sort of a hit and miss proposition.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 09, 2015 08:14*

Doesn't a mesh grill in a # pattern block EMI? Like a Faraday Cage? Or does it have to have power going through it?


---
**Anthony Bolgar** *December 09, 2015 12:12*

That is the theory, however in an enclosed metal box, funny things can happen. The best way to eliminate problems is to allow distance between the boards, and any signal lines should be run as shielded cable. My other concern with mounting on top of the PSU is the heat that is generated, may cause premature failure of components on the ramps board.


---
**ChiRag Chaudhari** *December 09, 2015 16:37*

Wow , I absolutely love the brain storming we do here. It teaches you a lot of things, and give you some more idea to try out.



For now just to keep things simple going to move the control board at distance. And now time to mess with the Plugin first. Actually no more work on Laser for next 24 hrs as I have to assemble a FPV racer for customer today.



Purchased the Air brush air compressor form HF, but not sure if thats enough pressure. But thats the later part once I receive the Laser head.


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/gW49XQXTqNi) &mdash; content and formatting may not be reliable*
