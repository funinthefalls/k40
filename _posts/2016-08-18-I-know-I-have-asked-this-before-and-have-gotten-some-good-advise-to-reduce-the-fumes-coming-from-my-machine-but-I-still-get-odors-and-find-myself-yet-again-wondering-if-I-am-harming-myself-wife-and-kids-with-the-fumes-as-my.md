---
layout: post
title: "I know I have asked this before and have gotten some good advise to reduce the fumes coming from my machine but I still get odors and find myself yet again wondering if I am harming myself wife and kids with the fumes as my"
date: August 18, 2016 02:01
category: "Discussion"
author: "3D Laser"
---
I know I have asked this before and have gotten some good advise to reduce the fumes coming from my machine but I still get odors and find myself yet again wondering if I am harming myself wife and kids with the fumes as my laser cutter is in the basement  I am thinking about cutting a fresh air intake in the side of my house aka another dryer vent and cutting a fresh air intake into the side of the machine then trying to seal the thing up air tight but I don't know if that is realistic or will solve the problem any thoughts and advise would be great 

![images/afa8f0e8e73e864ca047619d11b7a156.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afa8f0e8e73e864ca047619d11b7a156.jpeg)



**"3D Laser"**

---
---
**greg greene** *August 18, 2016 02:22*

Not practical - too many opening  


---
**ThantiK** *August 18, 2016 04:08*

The problem is that you have the positive-pressure side inside the room.  That blower needs to have everything on the pressure side, <i>outside</i>.  All of it.  The only thing you should have inside your home is the suction side of things.


---
**Jose A. S** *August 18, 2016 04:43*

Yes, its a problem and difficult to leave the hobby...


---
**Anton Fosselius** *August 18, 2016 04:48*

Regarding the fumes, as long as you only cut/engrave wood you should be fine. Acrylic might be nasty.



I open a basement window and trow out the ventilation tube. Works well during summer. Do not do this during winter as hot air rises and cold air falls. My poor fan had no chance against the -15 C outside. Ended up filling half the house with smoke. A good way to test the fire alarm... lucky for me that my wide was not home ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 18, 2016 04:56*

**+Anton Fosselius** Unfortunately with plywood it also consists of whatever bonding agent they have used to adhere the various layers of ply together. Could also be toxic in large quantities?



**+Corey Budwine** I think the idea to put another hole in the wall of the house to vent it outside (via less pipe) is probably a wise idea just due to having to travel less. Although I am by no means an expert in that area, it just seems to make sense to me though.


---
**Robi Akerley-McKee** *August 18, 2016 07:16*

**+Anton Fosselius** So true...  I'm in so cal, no snow where I'm at, and good luck finding a house with a basement!  Yes add an exhaust fan to put your work area in negative pressure.  You could also look at the carbon air filters they have a hydroponic/Grow shops.


---
**Phillip Conroy** *August 18, 2016 08:15*

Do not forget any cut items will still be giving off fumes for a time after cuttung them ,are you sure you are not just smelling this


---
**Maxime Favre** *August 18, 2016 11:18*

I often work in Hvac automation. Regarding the diameter changes and the path of your tubes it might be a differential pressure problem. Small duct means high speed but low pressure and wide duct lower speed high pressure. Your blower's flow rate could be too low regarding the change of diameter. A longer pipe especially with too many angles induce friction loss which lower the effective flow rate at the output of the pipe.

A solution could be the add a second fan.

Fans in parallel increase flow and fans in serie pressure.

Another is to increase the section of your ducts (all the way).


---
**3D Laser** *August 18, 2016 13:50*

**+ThantiK** so the blower should be outside my house?  That could be possible I might be able to build a box water prop it and set it up on the outside   Of my house 


---
**3D Laser** *August 18, 2016 13:54*

Can someone explain to me the whole negative pressure thing I am not getting what is meant by that or how to accomplish it in my work area I think another problem is that I have it near my heat and AC unit 


---
**ThantiK** *August 18, 2016 15:32*

**+Corey Budwine**​, on the blower there are two sides.  One side has vacuum (negative pressure), the other has positive pressure.  Any portion of your ducting that's on the positive pressure side, will expel fumes from wherever it can.  Any hole, any crack, any crevice.  If those portions of the ducting are inside your home, you WILL get fumes unless you're <i>perfectly</i> sealed, and nobody ever gets a perfect seal at all joints.  However, if all of those joints are on the vacuum side, it will just pull a small amount of air out of the house instead. Leaving you with no fumes. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/iBmwQEZv7fs) &mdash; content and formatting may not be reliable*
