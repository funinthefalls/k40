---
layout: post
title: "Shared on June 23, 2017 08:26...\n"
date: June 23, 2017 08:26
category: "Discussion"
author: "Jorge Robles"
---

{% include youtubePlayer.html id="-wXApAAh8xA" %}
[https://www.youtube.com/watch?v=-wXApAAh8xA](https://www.youtube.com/watch?v=-wXApAAh8xA)





**"Jorge Robles"**

---
---
**HP Persson** *June 23, 2017 20:47*

Nice video, now we know that we have to wear laser glasses when we put our head inside the machine while running :)


---
**Don Kleinschnitz Jr.** *June 24, 2017 00:08*

MMmmm this test from someone who's laser cabinet is made of clear material??? 

I wonder why we keep discussing whether laser safety glasses and interlocks are important. I doubt that we would be having this debate as to whether you should wear safety glasses when grinding or welding or ..... sigh!


---
**Mark Brown** *June 24, 2017 02:43*

**+Don Kleinschnitz** His cabinet isn't clear, it's non-existent. He got the thing working then stopped. 



He also likes to store plywood sheets under the honeycomb bed. XD


---
**Jorge Robles** *June 24, 2017 11:58*

Well, watching his videos seems that he knows what is doing, regardless his setup. Also, having a clear cabinet is not important, as long as the material stops the laser. 


---
**HP Persson** *June 24, 2017 13:48*

He tests with the focused beam too, you have to crawl into your machine and put the eye into the beam path to have that kind of injury.

Impossible to get that results from a scattered beam, or unfocused reflection.


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/cz6qJLSzdPu) &mdash; content and formatting may not be reliable*
