---
layout: post
title: "Trying to make a better bed I have the lightobjects 18MM LASER HEAD W/AIR ASSIST with the 50.8mm lens what is the lasers focus point from the bottom tip of the 18mm head would it be 22.3mm ?"
date: February 13, 2017 02:18
category: "Discussion"
author: "Robert Selvey"
---
Trying to make a better bed I have the lightobjects 18MM LASER HEAD W/AIR ASSIST with the 50.8mm lens what is the lasers focus point from the bottom tip of the 18mm head would it be 22.3mm ?



The problem I have with the bed I have now when I cut through anything laser hits the bed and reflects back and burns the back of everything. 

![images/18648c447720dd24d49c76aa4db1c966.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/18648c447720dd24d49c76aa4db1c966.jpeg)



**"Robert Selvey"**

---
---
**Ned Hill** *February 13, 2017 02:38*

I started out with that expanded metal mesh like you have and had similar problems and eventually migrated to a honeycomb bed.  You get almost no back burn and you don't get a buildup of residue.  Here's what I did. [plus.google.com - Did upgrade the actual bed material to an aluminum honeycomb mesh from the ex...](https://plus.google.com/108257646900674223133/posts/9zkJycL7FQf)


---
**Don Kleinschnitz Jr.** *February 13, 2017 13:16*

Suggest you do a ramp test to find the right focal point.


---
**Maker Cut** *February 13, 2017 15:06*

Even with the right focal point, you will still get burning if you cut through with the expanded metal. You could reduce the power level to the point where you just barely cut through and may get less burning on the back side. It is a trade off though. I would do as **+Ned Hill** suggested and go with honeycomb or pin setup. Right now a pin setup with magnets might be a quick fix.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/Gxv93e5yz1P) &mdash; content and formatting may not be reliable*
