---
layout: post
title: "Suggestions for an off the shelf carbon filter and exhaust setup that'll work with the k40?"
date: September 14, 2017 23:11
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Suggestions for an off the shelf carbon filter and exhaust setup that'll work with the k40?  Need the "it'll be in open air surrounded by people and can't make a stink or health concern" package. 









**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 14, 2017 23:11*

**+Paul de Groot** what did you do with yours for Maker Faire? 


---
**Paul de Groot** *September 14, 2017 23:18*

Hi Ray, basically you create a box with charcoal granules through you suck the laser fumes. My set up at home is very basic. A big vacuum motor, a box with charcoal and a vacuum cleaner hose. At the maker faire, i did not had an air cleaner. I was in a very big hall and did not happen to have a neighbor (they did not show up). So i could just air the hose in the empty space. Since i did just very light engraving and not cutting no body complained.


---
**Martin Dillon** *September 15, 2017 13:27*

I am thinking these would work and the price is not bad.  [htgsupply.com - Carbon Filter Fan Combo - HTG Supply](http://www.htgsupply.com/categories/ventilation-odor-control/carbon-filter-fans-combo-systems)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/BH5bAVcZzJ9) &mdash; content and formatting may not be reliable*
