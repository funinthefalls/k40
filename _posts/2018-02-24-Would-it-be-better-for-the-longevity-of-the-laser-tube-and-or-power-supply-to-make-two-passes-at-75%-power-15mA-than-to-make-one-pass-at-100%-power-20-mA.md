---
layout: post
title: "Would it be better -- for the longevity of the laser tube and/or power supply -- to make two passes at 75% power (15mA) than to make one pass at 100% power (20 mA)?"
date: February 24, 2018 03:43
category: "Hardware and Laser settings"
author: "Joel Brondos"
---
Would it be better -- for the longevity of the laser tube and/or power supply -- to make two passes at 75% power (15mA) than to make one pass at 100% power (20 mA)?





**"Joel Brondos"**

---
---
**BEN 3D** *February 24, 2018 03:45*

Did you solved your 10mA Issue with your new tube an ps? / I think it does not matter if 100% or 75% for longtime, the 25% are just a safety zone to not go unfortunately to 125% by a issue.


---
**Phillip Conroy** *February 24, 2018 05:42*

Check tube makers web site ,

For my spt t45 watt tube max is 25ma ,tho for max life 20 ma 


---
**Joel Brondos** *February 24, 2018 12:55*

**+BEN 3D** Yes, thanks. It was a "DOH!" moment. The little red switch on the side of the power supply was toggled to 230 instead of 115. I switched it to 115 and VOILA! It lives!


---
**Imnama** *February 24, 2018 13:06*

The power output of a laser tube is not lineair with the power input. Most tubes give 90% of the output power at 60% of the input power. So using more then 70% not realy gives more laser output power, but shortens the life of the tube


---
**BEN 3D** *February 24, 2018 13:10*

+Imnama, uhhhh thanks thats nice to know, was new for me.


---
**Don Kleinschnitz Jr.** *February 24, 2018 13:24*

A good question, that we probably cannot answer with much confidence since we do not know the complex relationship between a tubes life and its operating current. 



<b>In regard to the laser</b>

The failure mode on a tube is complex in that it  it looses its power over time from leak down or dissociation. So end of life is somewhat defined by the type of use. Mechanical failures also occur that further shorten the life.



Some things that can effect normal life:

...Current

...Cooling temp



...................

I think someone (**+Anthony Bolgar**) started tests on life but I do not know if it resulted in a current vs power life curve. 



There is evidence that keeping the tube at or below its operating current is best and I suspect cooling has the biggest effect on power vs current and life. Manufacture warranties are based on keeping the tube in the operating range so that probably tells us something.



That said, I have seen some arguments that running at a higher current for a shorter time results in longer life since the tube is in ionization for less time even though @ a higher stress. I buy this theory as long as your are running in the "operating" region of the tube.



<b>As far as the LPS is concerned:</b>  running it at its "operating current" rather than its "max current" should result in the longest life. We know that most LPS's fail before what I would consider normal EOL due to the HVT failing (weak link). Whether that failure is due to high current or just the stress of +2000V is unknown.



Not a good technical answer ....but in many ways our laser tube is still a mystery. I consider my tube an expensive consumable but keep it in the recommended operating range as much as I can. Unfortunately our LPS are also treated as a consumable but we know we can replace the supply or more cheaply the HVT which eases the pain.


---
**Jim Hatch** *February 24, 2018 14:54*

**+Joel Brondos** - yes for your example. Avoid 100% because that eats your tube. (Remember most of these aren't really 40W - mine for example is 32W so the current max can be "max possible" vs max operating.) But if 1 pass at 95% (I limit at 18ma) works to cut, then that's better from Don's point of being ionized for less time. However, operation under 95% doesn't change the lifespan much from the science side of things (too many variables for us to be able to measure over hundreds and thousands of hours). It's the 98-100% usage that pushes the lifespan off the cliff.


---
**Anthony Bolgar** *February 24, 2018 23:32*

My testing took a detour, however I have a few observations:



Quality tubes (Reci for example) are relatively linear until about 93%. Cheap tubes, like the ones that ship with a K40 are not sop linear, mostly because they are really 30W tubes being overdriven to gain wattage.



Any tube, no matter what the make or quality should be considered a consumable. If you are using the laser to make products for sale, include a small amount into your pricing to cover the cost of a replacement tube. I am talking about a very small amount of about 25 cents per hour of use (A cheap tube should last about 1000 hours at 75-90% power. So you will have saved up about $250 towards a new tube) With that being said, if you run at max current, you should still get about 600-800 hours. SO add 35 cents for every hour run to your job cost. You are still covered and can output much more product per hour than trying to baby the tube at ridiculously low current (I see people running 3 or 4 passes at 4-5mA........your tube will die before making as much product as one running at higher wattage.



Get a good power meter and keep track of your tubes output, I use a mahoney power meter from Bell lasers, best $120 I have spent on my lasers. You can see the power drop as it is happening and order a replacement before it dies, so you are not down and waiting for a new tube. The meter has also gotten me a full refund from paypal on a tube the seller claimed was 50W that was really only 38W)



Keep your tube cool....running at 30 deg C is harder on your tube than running at 25mA. Ideal temp is between 15 and 20 deg C. But be careful not to shock the tube with drastic temp changes.



When you do replace your tube, try to get a better made brand name such as Reci. Cloudray also makes some nice tubes.



Please take all my statements with a grain of salt, they are my personal observations and your tube may behave differently, so your mileage may vary.



And finally, please install door interlocks on your laser, your eyes are worth it!


---
**Joel Brondos** *February 26, 2018 03:56*

**+Anthony Bolgar** Where do you get your replacement laser tubes? And where do you get your Mahoney Power Meter? Is it analog - and can it fit in the same spot as the stock meter?


---
**Anthony Bolgar** *February 27, 2018 01:01*

I got my power meter from Bell Lasers in the United States. It is a hand held analog meter that you put the head in the path of the beam and test fire the laser to measure the power. I do it once a week to track my tube life. It does not replace the current meter on the  laser.


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/U5qzP9CHFjB) &mdash; content and formatting may not be reliable*
