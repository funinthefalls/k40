---
layout: post
title: "Just got a k40 laser and I'm trying to install the software"
date: October 07, 2016 17:40
category: "Original software and hardware issues"
author: "Dan Massopust"
---
Just got a k40 laser and I'm trying to install the software.  When I hit set up it shows me the error I'm attaching as a picture.  When I click the bottom Chinese button the instructions come up in English shown.    I can install Corel laser shown in the picture of the file directory but when i run it I get a message saying that I need coreldraw vesion 11 or greater installed.  I see Corel 12 in rar but I don't know what to do with it.  Can anyone help.



![images/2ffbf1b14e25a42b99a392f170847cc2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2ffbf1b14e25a42b99a392f170847cc2.png)
![images/ff605945b4d7d36a9722850001ddc23a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ff605945b4d7d36a9722850001ddc23a.png)
![images/8cc38548e6fc8384ef4ac0ea3e09efb5.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8cc38548e6fc8384ef4ac0ea3e09efb5.png)

**"Dan Massopust"**

---
---
**Stevie Rodger** *October 07, 2016 18:38*

use winrar to extract corel12

then install corel12

then install corel laser




---
**Dan Massopust** *October 07, 2016 18:51*

Stevie Thanks that worked great!!!!  Off I go to get this ready to fire up.


---
**Dan Massopust** *October 07, 2016 18:53*

Well at least I have the software coming up on my screen.  Need to check out the wiring before I turn on the machine.  Thanks again for your help and your very fast response.


---
*Imported from [Google+](https://plus.google.com/117247597344463074944/posts/SBmy2BApuym) &mdash; content and formatting may not be reliable*
