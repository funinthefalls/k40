---
layout: post
title: "Did these K40 Watt CO2 lasers just get much more expensive or is it just me?"
date: November 27, 2016 20:34
category: "Discussion"
author: "Timo Birnschein"
---
Did these K40 Watt CO2  lasers just get much more expensive or is it just me? I bought mine for under 400 from LA and now they are widely over 500 everywhere...





**"Timo Birnschein"**

---
---
**K** *November 27, 2016 22:53*

I noticed that as well. I bought mine for $350 shipped. For a while they dropped below $300 before shipping. Now they've really gone up. 


---
**Kelly S** *November 27, 2016 23:03*

Before I got mine I was waiting to get paid and they jumped from 400 to 500.  Was irritating.  If you can wait, wait till after the new year, I think Christmas has a lot to do with it.  I bit the bullet myself.


---
**Jim Hatch** *November 28, 2016 14:10*

**+Kelly S**​ I think you're right. That plus how much stock is on hand - for those in the US, a lot are sitting in warehouses & if stock is tight the price goes up. I've seen it go up & down over the past couple of years as I've been paying attention. They spike up around $500+ and then two months later they're back into the $300+ range again.


---
**Kelly S** *November 28, 2016 14:25*

If I did it over again. I'd buy one of the old versions as I have upgraded/eliminated the cost of a new model already lmao.  


---
**Jared Roy** *November 28, 2016 16:12*

I was just going to buy one and was disappointed by the price....I thought I remembered wrong, but now I see the price fluctuates.  I'll keep an eye out.


---
**Nick Williams** *November 29, 2016 04:23*

Good old capitalism, supply and demand 🤓 cheers


---
*Imported from [Google+](https://plus.google.com/+TimoBirnschein/posts/NSBsf7LpXuP) &mdash; content and formatting may not be reliable*
