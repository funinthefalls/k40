---
layout: post
title: "So I've been approved to purchase a Laser cutter for use in my classroom to deliver Technology"
date: June 14, 2016 12:29
category: "Discussion"
author: "Nigel Conroy"
---
So I've been approved to purchase a Laser cutter for use in my classroom to deliver Technology.

As per usual I asked months ago and only just got approval, the catch is I need to spend the money by the end of the month.



So the point in this post is a request for anyone willing to help me put a shopping list together?



From my reading so far shopping list will include (but not limited to):



- K40

- Smoothie Board (not sure which one)

- Air assist (if not already on k40 I purchase)

- Simple water system (bucket)

- Upgrade exhaust fan

- Replace window material with a protective material

- Anything else?



So if anyone would be willing to help me put together a list of items I need to purchase I would be very appreciative.



This might also help others in the future and the list could be shared afterwards.



Thanks



Here is a google sheet if anyone wants to add potential items/links and/or comments

[https://docs.google.com/spreadsheets/d/1wKvgItoFosNCoNpZMyFh11z-WFMk9Ayx5VDf1e-T53o/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1wKvgItoFosNCoNpZMyFh11z-WFMk9Ayx5VDf1e-T53o/edit?usp=sharing)





**"Nigel Conroy"**

---
---
**Ulf Stahmer** *June 14, 2016 12:41*

Fantastic news!  the K40 is an amazing device!  You'll love it!  Excellent list.  I'd also recommend some safety systems like these:



I purchased one of these  [http://www.aliexpress.com/item/Waterproof-720P-HD-7mm-lens-Inspection-Pipe-1m-Endoscope-Mini-USB-Camera-Snake-Tube-with-6/32504253632.html](http://www.aliexpress.com/item/Waterproof-720P-HD-7mm-lens-Inspection-Pipe-1m-Endoscope-Mini-USB-Camera-Snake-Tube-with-6/32504253632.html)



I find it really useful because it takes away the temptation to look into the laser while it's cutting.  I would also highly recommend installing micro switches on the access panels (all 3) to shut the laser down if the panels are opened during use.  I would also install a water flow switch to shut the laser down if the coolant flow stops.



My laser power supply has a jumper on it that, if in place, allows current flow to the laser, and if not in place, doesn't.  I simply wired all the switches in in serial for a very simple safety setup.  I would think this is especially important when working with children.


---
**Maxime Favre** *June 14, 2016 12:42*

For a classroom, safety features like door switch, water flow, water temp, etc


---
**Nigel Conroy** *June 14, 2016 12:59*

Thanks for sharing, someone had also suggested the camera as a way to view. I was thinking to re-purpose one of my old android phones as a webcam and view that way. 



Some of the k40's I've seen on ebay now have the door switches, emergency stop etc.. but I like the idea of a jumper on the power supply. Essentially like a key!!! 



Water flow and temp sound like a good idea also. I was thinking them but didn't want to write a massive list on the post. Perhaps I will create a google sheet for people to add items/links to.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 13:16*

Also for classroom, I would say you will be needing plenty of safety goggles. I'm running a small cheap microsoft webcam inside my K40 attached to the rail, so you can sort of see what is going on while lasering. I think I will eventually change the position.



Another thing is a cable drag chain, to contain all your hoses/cables that are going to the head. Maybe also some laser line pointers to show where the laser will fire.


---
**Ulf Stahmer** *June 14, 2016 13:20*

**+Nigel Conroy** I bought these micro switches [http://www.aliexpress.com/item/New-2015-hot-selling-10Pcs-lot-Microswitch-Long-Lever-AC-250V-15A-HV-156-1C25-SPDT/32339341258.html](http://www.aliexpress.com/item/New-2015-hot-selling-10Pcs-lot-Microswitch-Long-Lever-AC-250V-15A-HV-156-1C25-SPDT/32339341258.html) and installed them with some screws and nuts I bought at HomeDepot.  Let me know if you would like some photos of my setup.  I'm traveling on business right now, but will be back home in a few days.



Prior to installing the smoothie, I might also recommend adding a USB hub inside the controller compartment.  I've plugged my K40 board into the hub along with the camera and the software dongle.  That way, nothing gets lost!


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/MhxeNS6568D) &mdash; content and formatting may not be reliable*
