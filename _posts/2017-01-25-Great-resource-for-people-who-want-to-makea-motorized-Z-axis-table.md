---
layout: post
title: "Great resource for people who want to makea motorized Z axis table"
date: January 25, 2017 20:53
category: "Modification"
author: "Anthony Bolgar"
---
Great resource for people who want to makea motorized Z axis table



[https://hackaday.io/post/51315](https://hackaday.io/post/51315)





**"Anthony Bolgar"**

---
---
**Ryan Branch** *January 27, 2017 09:22*

The design could use a little tweaking, but it works well enough for us. That's why I posted the .step files. The problem I have run into with the design is that the belt can slip on the pulleys, causing the bed to become un tram. It likely just needs a better pulley tensioner tbh.


---
**Jorge Robles** *February 14, 2017 07:31*

**+Ryan Branch** could you share the F360 project?


---
**Ryan Branch** *February 16, 2017 05:38*

**+Jorge Robles** I just updated the github with the complete fusion 360 model. Sorry if it's not very organized.


---
**jeremiah rempel** *February 21, 2017 01:25*

I think some adjustable idlers at all four corners to wrap the belt around the gears and also tighten the belt would fix the problem.  I'm building this next month but slightly modified as I described and for a 23" Z travel.



Thank you for sharing with the community.


---
**Ryan Branch** *February 23, 2017 01:46*

**+jeremiah rempel**​ I was thinking about doing the exact same thing. If I were to re-design it, I would put four idlers in the corners to get the belt to wrap half way around the pulleys like you mentioned. Unfortunately I don't think I have enough slack in our current belt to do it. I could always buy a different belt length and add more bearings, but at that point it might cost as much as the one from light object, (total cost of all my versions, not just the parts needed) which is why I haven't done it. 



Another thing that would be worth doing if you are building a larger version would be to use pulleys with more teeth. That will also help keep the belt from slipping. I just used the 20t pulleys since I already had one for the motor, and that just made the z axis leadscrews  1:1 with the motor. 


---
**jeremiah rempel** *February 23, 2017 04:56*

So I also happen to have a set or 3 of 20T GT2 pulleys.  I ordered the belt you recommended but now I'm going to have to get a much longer one.  I just noticed I said extra Z but I meant 23-24" X.  I'm also going to incorporate some Igus style bearing on the top print for the 8mm linear rods.



Well the difference between building your own and getting light Objects table is you can always modify yours for your box or wallet size.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/ACBWC2xNsqf) &mdash; content and formatting may not be reliable*
