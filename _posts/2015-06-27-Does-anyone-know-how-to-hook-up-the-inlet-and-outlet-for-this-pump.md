---
layout: post
title: "Does anyone know how to hook up the inlet and outlet for this pump?"
date: June 27, 2015 20:35
category: "Hardware and Laser settings"
author: "quillford"
---
Does anyone know how to hook up the inlet and outlet for this pump?

![images/54086cf9f7185159aaa6f0c9bc90e2a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/54086cf9f7185159aaa6f0c9bc90e2a7.jpeg)



**"quillford"**

---
---
**David Richards (djrm)** *June 27, 2015 20:41*

Greetings. This looks like a submersible pump. The water will probably go in through the grill on the front and come out of the pipe at the top. I have something similar feeding my k40 laser tube. Kind regards David


---
**quillford** *June 27, 2015 22:21*

**+david richards** what kind of water do you use with yours?


---
**David Richards (djrm)** *June 28, 2015 03:56*

I started out using plain tap water, i have since added antifreeze to it. The reservoir is around 20l it includes 4.5l of red antifreeze. I like this as it is easy to see in the pipes. I have added a flow sensor interlock to inhibit the laser unless the pump is working. Hth David


---
**quillford** *June 28, 2015 04:07*

thanks **+david richards**


---
**Jim Bennell (PizzaDeluxe)** *June 29, 2015 01:48*

Hey Quilford, I Have the same pump mine came with an aftermarket brass fitting that goes in place of the black plastic piece on top. like this-

[http://www.menards.com/main/plumbing/rough-plumbing/pipe-tubing-hoses-fittings-accessories/fittings/hose-barb-fittings/3-8-id-barb-x-1-2-mip-lead-free-brass-id-hose-barb-to-male-/p-2276892-c-19480.htm](http://www.menards.com/main/plumbing/rough-plumbing/pipe-tubing-hoses-fittings-accessories/fittings/hose-barb-fittings/3-8-id-barb-x-1-2-mip-lead-free-brass-id-hose-barb-to-male-/p-2276892-c-19480.htm)



And use a hose clamp to keep it on because it can slide off and then you run the risk of damaging the laser tube with no water flow.



like this -



[http://www.menards.com/main/plumbing/rough-plumbing/pipe-tubing-hoses-fittings-accessories/fittings/hose-barb-fittings/3-8-brass-hose-clamp-2-pack/p-1713375-c-9436.htm](http://www.menards.com/main/plumbing/rough-plumbing/pipe-tubing-hoses-fittings-accessories/fittings/hose-barb-fittings/3-8-brass-hose-clamp-2-pack/p-1713375-c-9436.htm)



Here's mine-

[http://i.imgur.com/CNM10cF.jpg](http://i.imgur.com/CNM10cF.jpg)


---
**quillford** *June 29, 2015 05:22*

**+Jim Bennell** I did notice the tube was rather loose. Thanks for suggesting a clamp.


---
*Imported from [Google+](https://plus.google.com/115654175984463400645/posts/Meyi991YkKf) &mdash; content and formatting may not be reliable*
