---
layout: post
title: "Hi Guys, I made my very first youtube production!"
date: December 30, 2015 09:10
category: "Object produced with laser"
author: "Gregory J Smith"
---
Hi Guys,  I made my very first youtube production! Amateurish really, but shows off how I make little wooden tags for my daughter with a K40 laser.  Hope you enjoy.  Greg Smith


{% include youtubePlayer.html id="jEUWw4pZzw0" %}
[https://www.youtube.com/watch?v=jEUWw4pZzw0](https://www.youtube.com/watch?v=jEUWw4pZzw0)





**"Gregory J Smith"**

---
---
**Tony Schelts** *December 30, 2015 09:24*

Im relatively new to this stuff, what what a simple idea and beautiful.   thanks for the upload.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 30, 2015 09:37*

That's great. You might find that you can increase the engrave speed by increasing the pixel steps option (in the default CorelLaser software). I've done it on things where super detailed engraves are not totally necessary (e.g. I've increased from 1 pixel step to 2 or 3. Any more than 3 seems to lose way too much detail).


---
**Stephane Buisson** *December 30, 2015 10:28*

Nice thank you for sharing


---
**Brooke Hedrick** *December 30, 2015 13:52*

Great use of the machine and nice video!


---
**Scott Marshall** *January 03, 2016 07:11*

Nice job, utilizing the CCL and with the video!


---
*Imported from [Google+](https://plus.google.com/105767412220617661397/posts/CEMoyffEkor) &mdash; content and formatting may not be reliable*
