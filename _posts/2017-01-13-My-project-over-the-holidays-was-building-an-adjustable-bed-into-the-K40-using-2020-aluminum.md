---
layout: post
title: "My project over the holidays was building an adjustable bed into the K40 using 2020 aluminum"
date: January 13, 2017 20:24
category: "Modification"
author: "Jeff Bovee"
---
My project over the holidays was building an adjustable bed into the K40 using 2020 aluminum. Works ok but I think I need to improve it with dual lead screws to keep left to right height in better alignment.



![images/f40ce4eb526df286e238ef2920af55b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f40ce4eb526df286e238ef2920af55b2.jpeg)
![images/6017b71a5541e3f9615fc034178cfc07.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6017b71a5541e3f9615fc034178cfc07.jpeg)

**"Jeff Bovee"**

---
---
**Mark Kerr** *January 14, 2017 15:20*

love it, I guess I need to do similar (or I want to do similar) are there a set of drawings/guides/ component lists for this anywhere?




---
**Jeff Bovee** *January 20, 2017 22:06*

unfortunately no, i just sorta figured it out as i went with the parts that i had on hand.  Probably should have made some plans ahead of time, would have saved me some time 


---
**Robert Selvey** *January 30, 2017 02:04*

Where do you find the honey comb for the bed ?


---
**Jeff Bovee** *April 11, 2017 21:32*

I purchased on Amazon [amazon.com - Amazon.com : Aluminum Honeycomb Grid Core, 1/4" Cell, 10"x12"x .375" : Everything Else](https://www.amazon.com/gp/product/B00WISLQBY/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1) not available now in this size but there are others out there that show up


---
*Imported from [Google+](https://plus.google.com/115193415731522119744/posts/ey8SarH4sax) &mdash; content and formatting may not be reliable*
