---
layout: post
title: "Slate coasters A friend of mine likes dragonflys and her nickname is JAX"
date: April 01, 2018 22:38
category: "Object produced with laser"
author: "HalfNormal"
---
Slate coasters



A friend of mine likes dragonflys and her nickname is JAX.

Here is a 3.5 x 3.5 inch slate tile. 



![images/843ec6d8ba921f8a4963934eab3481ab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/843ec6d8ba921f8a4963934eab3481ab.jpeg)



**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *April 02, 2018 11:08*

Nice, how much power?


---
**HalfNormal** *April 02, 2018 15:04*

40% on a 60 watt @ 100 mm


---
**Ned Hill** *April 03, 2018 02:02*

Ohh looks nice.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/g13NR9cxLPM) &mdash; content and formatting may not be reliable*
