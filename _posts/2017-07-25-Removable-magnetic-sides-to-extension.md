---
layout: post
title: "Removable magnetic sides to extension"
date: July 25, 2017 02:49
category: "Modification"
author: "William Kearns"
---
Removable magnetic sides to extension 

![images/f1d47776bea7734e3eaac00a8371aee2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1d47776bea7734e3eaac00a8371aee2.jpeg)



**"William Kearns"**

---
---
**Jorge Robles** *July 25, 2017 07:54*

Awesome!


---
**Anthony Bolgar** *July 25, 2017 11:58*

Nice idea. 


---
**Don Kleinschnitz Jr.** *July 25, 2017 14:26*

Looking promising, watching with interest


---
**William Kearns** *July 27, 2017 03:19*

Tripling the height of the LO z-axis table (the original and the extended)![images/eb218af610eececf0ec6aae7b500d42a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb218af610eececf0ec6aae7b500d42a.jpeg)


---
**William Kearns** *July 28, 2017 03:58*

The adjusting rods turned out good ![images/f4f7ed2923e1cc8ac8079cc23d0bbbf0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4f7ed2923e1cc8ac8079cc23d0bbbf0.jpeg)


---
**Don Kleinschnitz Jr.** *July 28, 2017 14:19*

#K40LiftTable

Did you replace the floor, most I have seen are black?


---
**William Kearns** *July 28, 2017 14:21*

 No that's what I got 


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/VuMqDmVochc) &mdash; content and formatting may not be reliable*
