---
layout: post
title: "Sharing this interesting source I found while learning CNC things:"
date: December 18, 2016 15:15
category: "Software"
author: "Don Kleinschnitz Jr."
---
Sharing this interesting source I found while learning CNC things: 

[http://linuxcnc.org/docs/html/gui/image-to-gcode.html](http://linuxcnc.org/docs/html/gui/image-to-gcode.html)





**"Don Kleinschnitz Jr."**

---
---
**greg greene** *December 18, 2016 15:52*

I didn't see a download source - is there one?


---
**greg greene** *December 18, 2016 15:54*

Ah I see, time to update my Linux system then - thanks Don


---
**Don Kleinschnitz Jr.** *December 18, 2016 15:57*

Looks like you need LinuxCNC and the Axis UI. I am not a linux participant ...

I thought the approach was interesting.


---
**greg greene** *December 18, 2016 16:01*

Yes - it looks like an interesting piece of software, Once I get my old Linux computer up and running - I'll give it a go.  I'm thinking the Cohesion 3D board should run it OK - it's just GCode output.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/U9RfYc6mKRU) &mdash; content and formatting may not be reliable*
