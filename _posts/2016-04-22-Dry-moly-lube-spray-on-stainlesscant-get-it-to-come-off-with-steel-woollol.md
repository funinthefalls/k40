---
layout: post
title: "Dry moly lube spray on stainless....can't get it to come off with steel wool...lol"
date: April 22, 2016 19:34
category: "Object produced with laser"
author: "Scott Thorne"
---
Dry moly lube spray on stainless....can't get it to come off with steel wool...lol

![images/6661654a2980bfd95cd5bcc2bbd1a368.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6661654a2980bfd95cd5bcc2bbd1a368.jpeg)



**"Scott Thorne"**

---
---
**Joe Keneally** *April 22, 2016 19:55*

WOW!  :)


---
**HalfNormal** *April 22, 2016 22:50*

Sweet!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 22, 2016 23:17*

So has all the black region in the background been lased as well? Or just the word Tracy? Looks very crisp on the writing.


---
**Scott Thorne** *April 22, 2016 23:52*

**+Yuusuf Sallahuddin**...just the word tracy....this was my first test...the rest is just where I sprayed the whole sheet of stainless. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 23, 2016 01:37*

**+Scott Thorne** Cool. I looked up this Moly Lube stuff & it seems that its MoS2 (Molybdenum Disulfide). Is this the active ingredient in the CRC Dry Moly? I can't find that stuff here, but there are other brands/products that maybe are the same.


---
**Scott Thorne** *April 23, 2016 11:47*

**+Yuusuf Sallahuddin**....yes it has to be molybdenum based. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 23, 2016 11:50*

**+Scott Thorne** Cool, might have to get some & give it a go sometime. Would be interesting to test on cutlery.


---
**Coherent** *April 23, 2016 12:54*

I made some tests using the same stuff a while back. Try using acetone or alcohol to clean it after you engrave.


---
**Scott Thorne** *April 23, 2016 13:08*

**+Coherent**...good tip bro...I'll pick some acetone up today


---
**Coherent** *April 24, 2016 16:19*

I think what worked well for me was actually isopropyl alcohol and elbow grease. It's been a bit and I tested a few things. Either one is also great for cleaning before you spray the moly on also and acetone evaporates fast.


---
**Thor Johnson** *May 16, 2016 15:04*

What was the power level/speed?

I've been trying it on SS & Aluminum, but I find that it isn't really bonded well -- cleaning with a nylon brush removes it if you're not careful.



I also tried chrome, but I expected that to not work...


---
**Scott Thorne** *May 16, 2016 15:38*

**+Thor Johnson**...50 speed...60%power


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/EDPhuNe73NK) &mdash; content and formatting may not be reliable*
