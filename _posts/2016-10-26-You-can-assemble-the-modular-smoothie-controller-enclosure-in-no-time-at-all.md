---
layout: post
title: "You can assemble the modular smoothie controller enclosure in no time at all"
date: October 26, 2016 05:06
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
You can assemble the modular smoothie controller enclosure in no time at all.


**Video content missing for image https://lh3.googleusercontent.com/-4jhQTBT364s/WBA51H0FtzI/AAAAAAAAfxY/KJyBgGhLuJUaI3UK23FuJfHTeswKZL_8QCJoC/s0/Smoothie%252Bcontroller%252Bassemble.mp4.gif**
![images/64817433f78ab33d80ffc7c63f1dcf3b.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/64817433f78ab33d80ffc7c63f1dcf3b.gif)



**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/PPXaxQodJ8b) &mdash; content and formatting may not be reliable*
