---
layout: post
title: "Ok so the LaserDraw plugin for Corel is not the easiest bit of software to understand however I feel I must be doing something wrong"
date: October 18, 2015 14:39
category: "Discussion"
author: "Phil Willis"
---
Ok so the LaserDraw plugin for Corel is not the easiest bit of software to understand however I feel I must be doing something wrong.



1) On layer 1, I design a simple key tag, a large ellipse with a smaller one for a hole. I paint the inside of the large ellipse black.

2) On layer 2, I put some text.

3) I hide the first layer and engrave the text.

4) I hide the second layer and cut the tags.

If I don't adjust the origin by about -15mm in X and -10mm in Y, I end up with the cuts being in the middle of my text.



So I have taken to moving the text +15mm X and 10mm Y before starting, but apart from blaming the software I am not sure what is wrong. I have double checked the motherboard type in the settings.



Any suggestions?





**"Phil Willis"**

---
---
**Peter** *October 18, 2015 15:31*

This might help.


{% include youtubePlayer.html id="MR7967VHKnI" %}
[https://www.youtube.com/watch?v=MR7967VHKnI](https://www.youtube.com/watch?v=MR7967VHKnI)



He says something about the "engraving Data" and "Cutting Data" being set to a specific type which helped with the alignment issues he had with different layered jobs.



Let me know if it works.


---
**Phil Willis** *October 18, 2015 16:05*

Thanks. I had already found that video but on re-watching I spotted my pixel step size was 2. So I will give that a try.


---
**Peter** *October 18, 2015 16:08*

I think pixel step adjusts the quality by moving 1 pixel or 2 etc on the Y axis when engraving/cutting. Dont quote me though.

Hope it works.


---
**Joey Fitzpatrick** *October 19, 2015 12:53*

Add a 1mm square object in the top left corner, visable in all layers(a master layer).  select the engraved work first.  add task for the cut work(be sure to check tick box first before adding task) and start.  Be sure not to select your 1mm object when cutting or engraving.  This 1mm object will keep all layers aligned in the software.


---
**Phil Willis** *October 19, 2015 17:58*

**+Joey Fitzpatrick** thank you very much that does sound promising.


---
**Phil Willis** *October 20, 2015 19:42*

**+Joey Fitzpatrick** Worked like a charm!  Thanks again.


---
**Joey Fitzpatrick** *October 20, 2015 22:18*

No Problem!!  This method seems to work the best when cutting and engraving with corellaser.  


---
*Imported from [Google+](https://plus.google.com/+PhilWillisAtHome/posts/8fnB9qWodhL) &mdash; content and formatting may not be reliable*
