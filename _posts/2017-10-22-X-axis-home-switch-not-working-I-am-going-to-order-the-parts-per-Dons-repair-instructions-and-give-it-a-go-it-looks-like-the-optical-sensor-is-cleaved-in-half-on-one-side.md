---
layout: post
title: "X axis home switch not working I am going to order the parts per Don's repair instructions and give it a go, it looks like the optical sensor is cleaved in half on one side"
date: October 22, 2017 20:19
category: "Discussion"
author: "Kevin Lease"
---
X axis home switch not working I am going to order the parts per Don's repair instructions and give it a go, it looks like the optical sensor is cleaved in half on one side



![images/6fd780a883eb2b6ccad79383383ae7c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6fd780a883eb2b6ccad79383383ae7c0.jpeg)
![images/2d423b66a0adfdd0d46594752695de31.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2d423b66a0adfdd0d46594752695de31.jpeg)
![images/9f11fd474c89cc184fda62975f874fe6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9f11fd474c89cc184fda62975f874fe6.jpeg)

**"Kevin Lease"**

---
---
**Adrian Godwin** *October 22, 2017 20:55*

It could have been assembled badly, it's also possible it's been hit by the mechanism that's supposed to run through the gap. Make sure it's correctly positioned or the replacement will get chopped too.


---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2017 01:17*

Just switch over to mechanical switches/ endstops. Cheap and direct. 


---
**Don Kleinschnitz Jr.** *October 23, 2017 01:50*

Make sure that you replace the resistor in the instructions :).


---
**Kevin Lease** *October 23, 2017 11:38*

**+Don Kleinschnitz** ok thank you


---
**Al Kardos** *October 23, 2017 14:20*

I had the same problem. My post is out here somewhere. The copper had been torn up from the board. Added wires to replace them, then realized the metal that sets off the sensor was too high.  Made a new one from scrap.

![images/f7562bc13940be4f09816583dc6dc524.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f7562bc13940be4f09816583dc6dc524.jpeg)


---
**Al Kardos** *October 23, 2017 14:21*

![images/f912b4f753279494e643658f3cf15c1d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f912b4f753279494e643658f3cf15c1d.jpeg)


---
**Don Kleinschnitz Jr.** *October 23, 2017 15:27*

BTW if this is a new machine it may have been misaligned. If not it is useful to investigate why the interposer impacted the sensor, something else may be wrong.


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/4gfzgT65XN2) &mdash; content and formatting may not be reliable*
