---
layout: post
title: "I am having a slight issue with the guide that runs the X axis"
date: July 27, 2015 17:39
category: "Hardware and Laser settings"
author: "James Keaton"
---
I am having a slight issue with the guide that runs the X axis.  It is slightly unlevel (not perpendicular).  Are there adjustments for this?  The laser came with way.  I have engraved quite a bit.  It still works fine, but it engraves/cuts slightly uneven.





**"James Keaton"**

---
---
**Joey Fitzpatrick** *July 27, 2015 18:16*

I used flat m3 washers under the Cutting head Plate (on Top of the Stand Offs) to level mine.  It took  a couple of them to get it level.  I added 1 washer at a time until my cuts were straight


---
**James Keaton** *July 27, 2015 18:24*

Mine is the bar that the laser moves left to right on the X axis that isn't straight.  I guess you wouldn't call it level, just not straight


---
**Paul Haban** *July 27, 2015 19:53*

James, mine had the same issue. Over the course of the X axis, there was a 5.5 mm drift. To fix it, I took off the black sheet metal stepper motor shaft cover that was at the bottom most position of the bed. It is held in place by two screws. Under there is the shaft which has a shaft coupler holding the left and right shafts together. Loosen those 8 screws, align the bar, and tighten the screws.


---
**James Keaton** *July 27, 2015 20:09*

You are the man.  Thank you so much.  I will do this when I get home.


---
**Paul Haban** *July 28, 2015 15:56*

Did that work James?


---
**James Keaton** *July 29, 2015 00:55*

Like a champ.  Thank you so much for your help.  Sorry I just saw this.


---
*Imported from [Google+](https://plus.google.com/109769945337563588796/posts/Kt8vMqiHZBq) &mdash; content and formatting may not be reliable*
