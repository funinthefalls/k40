---
layout: post
title: "Hello all and Happy New Year to everyone!"
date: January 01, 2017 22:00
category: "Modification"
author: "Kostas Filosofou"
---
Hello all and Happy New Year to everyone!



Finally the Project is finished! I have upload it in GrabCad for anyone interested...



I must update the BOM list but the most basic parts is there.



[https://grabcad.com/library/co2-laser-40w-modification-1](https://grabcad.com/library/co2-laser-40w-modification-1)







**"Kostas Filosofou"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 02, 2017 00:43*

Looks amazing. Thanks for sharing. I'm curious how much distance there is between the x-axis motor & the left-hand case wall? Looks super close in the photos.


---
**Kostas Filosofou** *January 02, 2017 00:57*

**+Yuusuf Sallahuddin**​

Is super close indeed :) is about 5mm ... The whole assembly was pain in the ass.I wanted to take advantage of all the available space


---
**James Rivera** *January 02, 2017 03:07*

**+Konstantinos Filosofou** what is your new cutting area?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 02, 2017 03:43*

**+Konstantinos Filosofou** Thought it looked in the mm range. I'm wondering when the machine is moving fast if your x-axis motor (or the mount) will rub on the wall due to vibrations. Hopefully not. Looks like a great use of the available space. Hopefully I'll get around to doing a similar project this year on my K40.


---
**Kostas Filosofou** *January 02, 2017 09:55*

**+James Rivera**​ is about 300x660mm


---
**Kostas Filosofou** *January 02, 2017 09:58*

**+Yuusuf Sallahuddin**​ until now I haven't any problems.. the whole thing is very stable.


---
**Sebastian Szafran** *January 03, 2017 00:30*

Very nice mod **+Konstantinos Filosofou**​ and I will follow you one day. Thanks for sharing 👌 


---
**Bill Keeter** *January 03, 2017 00:32*

Oh, that really looks nice. How good is the cutting on the front right. As this is the farthest distance for the beam to travel. Is the frame standard extruded aluminum or vslot?



Also do you have any video of it cutting?


---
**Kostas Filosofou** *January 03, 2017 13:56*

**+Bill Keeter**  Thanks! The aluminium extrusion is like vslot but cheaper and fits perfect the small wheels for the x axis (look at in the BOM to the link above) .The cutting is good in the whole available area ,the laser alignment is pretty perfect .. I have upload some videos in google+ and I will upload some more when i repair my PSU ..unfortunately broke down.

[photos.google.com - Co2 Laser 40w Modification](https://goo.gl/photos/Yg9YypmkULTCeg3ZA)


---
**Kelly S** *January 03, 2017 23:25*

Awesome, will have to download tomorrow and get shopping.  My psu is larger than the one you have so all electronics will be relocated outside the machine :)


---
**chris B** *February 19, 2017 04:44*

**+Konstantinos Filosofou** I've got almost all the materials ready to build this mod.. but I only see sldwrks files for the 3d printed parts.. we you ever able to convert them over to STL ? Let me know if you're able to as those are easy to slice and print, but I don't think anything other than solidworks can convert them over to stl.. thanks!


---
**Kostas Filosofou** *February 19, 2017 11:49*

**+chris b**  I have upload them in Grabcad!


---
**Jesper Juul** *March 04, 2017 21:04*

**+Konstantinos Filosofou** Okay so I made the the hole motionsystem, but it doesn't fit the opening hole of the box. Did you assemble the motionsystem inside the box?




---
**Kostas Filosofou** *March 05, 2017 03:39*

**+Jesper Juul** exactly ... Is a little bit tricky , takes a lot of patience..


---
**Jeff C** *August 23, 2017 23:00*

AWESOME!!!


---
**Grafisellos Barranquilla** *March 31, 2018 03:02*

**+Kostas Filosofou**. SPECTACULAR. I'M GOING TO DO THE SAME WITH MY K40. SOME SUGGESTION ??? IM FROM COLOMBIA 


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/5dtgUA61SBL) &mdash; content and formatting may not be reliable*
