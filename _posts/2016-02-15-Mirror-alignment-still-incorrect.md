---
layout: post
title: "Mirror alignment still incorrect?"
date: February 15, 2016 17:04
category: "Discussion"
author: "Patryk Hebel"
---
Mirror alignment still incorrect?

![images/ba40006ba2b21348303e67ff3b6cbee3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ba40006ba2b21348303e67ff3b6cbee3.jpeg)



**"Patryk Hebel"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2016 19:19*

I'm not really certain what this picture is, but another possibility is that your cutting/engraving bed is not level so at different points it may be further away from the laser head (making it slightly weaker looking engrave).


---
**Patryk Hebel** *February 15, 2016 19:20*

**+Yuusuf Sallahuddin** if you look carefully it's a Akita (Dog) 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2016 19:26*

**+Patryk Hebel** Oh yeah, I see it now. It's sideways. So I'm curious, your issue with engraving seems to be that the left side is lighter than the right side of this picture. Where was the image (in relation to the top left corner of the cutting/engraving bed)?


---
**Patryk Hebel** *February 15, 2016 20:45*

**+Yuusuf Sallahuddin** Yes it was top left. I just checked my mirrors and they are perfectly aligned. The machine kinda losses power at some point and then starts getting it back up. I don't really understand what's going on.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2016 22:34*

**+Patryk Hebel** I think I recall another of your posts mentioning that the mA on the ammeter actually goes down. Is that right?

If that is so, then maybe the power supply is faulty. I don't know a lot about electronics, so maybe some others here can hopefully assist with electrical issues.


---
**Patryk Hebel** *February 15, 2016 22:36*

**+Yuusuf Sallahuddin** Ok. Thanks for the help anyway :)


---
*Imported from [Google+](https://plus.google.com/102509166997588398801/posts/Xj1Wq6YVJSa) &mdash; content and formatting may not be reliable*
