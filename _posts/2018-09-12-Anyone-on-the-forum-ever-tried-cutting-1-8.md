---
layout: post
title: "Anyone on the forum ever tried cutting 1/8 \""
date: September 12, 2018 22:05
category: "Discussion"
author: "Dennis Fuente"
---
Anyone on the forum ever tried cutting 1/8 " door skin plywood what was the result 

Thanks 

Dennis 





**"Dennis Fuente"**

---
---
**James Rivera** *September 12, 2018 23:27*

Not sure what, “door skin” plywood is, but I’ve cut 1/8” plywood before. Easy peasy.


---
**Mike Meyer** *September 12, 2018 23:42*

3MM Baltic Birch...Piece O' Cake.


---
**Jamie Richards** *September 13, 2018 16:02*

1/8" plywood of any type should be easy with the 50.8 if your tube, power supply, alignment etc. is good.  I was able to cut almost double that in one pass with my 50.8.  3/8" with my 38.1.  3/4" solid with a couple-few passes.  Haven't tried 1/2" yet, but I imagine it'll be easier  than 3/4"! lol


---
**Dennis Fuente** *September 14, 2018 15:28*

Hi Guy's thanks for the reply's, so door skin is some cheap home depot plywood that is mostly used as a layer over an inside door frame it has what looks like finished birch on one side and some real ruff looking brown wood on the other side don't know what's in the middle but so far i used 14 MA and 5 pass and i just start to cut through mirrors clean and aligned i am using LO air assess and a 50.8 focal lens  


---
**Jamie Richards** *September 15, 2018 09:31*

And you have no problem cutting other 1/8" plywood?  That is some crazy-tough stuff!  I thought the Lowe's 5mm poplar utility board was tough, but that stuff...  I was cutting through 1/2" solid oak earlier and it took a few passes, but I was at 5mm/s, forgot to turn it down a bit.  I cut 3/4" pine (assuming pine) at 3mm/s without a fire, so that's probably what I'll need to do with the 1/2" solid oak to get by in a couple passes.  Do you have a photo of the rough side?  Someone may be able to tell what it is by the grain pattern.


---
**Dennis Fuente** *September 15, 2018 16:16*

Hi Jamie 

Well i can cut other type ply's certainly easier then this stuff i have not played around with my laser in a while the software i was using didn't get it i have a new software and it works so now i am back at it i have a 50.8 focus lens i may try a 38 lens and see what happens i may have to play around with the speed right now i have tried 14 Ma's @  5-10 MMs it may be the glue they use to apply the different layers 


---
**Jamie Richards** *September 16, 2018 12:29*

Nasty arse glue.  Guess it has to be to hold up to different environmental conditions depending on where it's placed.


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/BUC8v7ep9hr) &mdash; content and formatting may not be reliable*
