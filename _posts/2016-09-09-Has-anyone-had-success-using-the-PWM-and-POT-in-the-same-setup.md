---
layout: post
title: "Has anyone had success using the PWM and POT in the same setup?"
date: September 09, 2016 01:52
category: "Smoothieboard Modification"
author: "Anthony Bolgar"
---
Has anyone had success using the PWM and POT in the same setup? I would really like to have the POT active, it is a 10 turn, so very fine power adjustments can be made)





**"Anthony Bolgar"**

---
---
**Alex Krause** *September 09, 2016 02:02*

**+Ariel Yahni**​ and I both use the pot in our setups


---
**Anthony Bolgar** *September 09, 2016 02:07*

Good to know.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 02:14*

Mine is also utilising the pot & the PWM.


---
**Anthony Bolgar** *September 09, 2016 02:17*

For the PWM, is it single or dual pin?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 05:21*

**+Anthony Bolgar** Single pin for me. 2.4!


---
**Scott Marshall** *September 09, 2016 16:20*

Just connect to the PSU cable LO line(pin 4) with a level shifter, you get full control, PWM and retain the pot as it always was.

(Pin 2.4 or 2.6 work great, just one pin needed, and a $2 Ebay level shifter or homebrew FET version)

The K40 PSU uses a negative logic fire circuit (low to fire),

Don't try to cheat and use 3.3v to control the 5V logic power supply, it won't safely shut down the laser. While it will work some of the time, it's a poor practice and could lead to unexpected laser fireing. Also include a 15k pullup resistor at the Smoothieboard pin to hold the logic high (and Laser OFF) during the Smoothie boot period.


---
**Anthony Bolgar** *September 09, 2016 16:26*

Thanks for the info.


---
**Anthony Bolgar** *September 09, 2016 16:34*

So the negative logic would be the 2.4! ?


---
**Scott Marshall** *September 09, 2016 17:01*

That's how it will read in the config file.

 ! inverts the output. (there's a whole list of "modifiers" for i/o on the smoothieboard).


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/DzBFEdT1S1W) &mdash; content and formatting may not be reliable*
