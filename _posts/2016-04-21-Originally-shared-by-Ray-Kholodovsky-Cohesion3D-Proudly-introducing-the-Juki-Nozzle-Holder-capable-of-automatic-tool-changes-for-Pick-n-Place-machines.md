---
layout: post
title: "Originally shared by Ray Kholodovsky (Cohesion3D) Proudly introducing the Juki Nozzle Holder (capable of automatic tool changes) for Pick n Place machines"
date: April 21, 2016 01:32
category: "Discussion"
author: "Brian Bland"
---
<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



Proudly introducing the Juki Nozzle Holder (capable of automatic tool changes) for Pick n Place machines.  We're reverse engineered and improved upon this part which costs over $100 from China and are getting a higher quality, more precise version made right here in the USA for only $40 each + $5 domestic shipping.  



A full description is here: [https://groups.google.com/forum/#!topic/openpnp/82hIAKQZnr0](https://groups.google.com/forum/#!topic/openpnp/82hIAKQZnr0)

and a video intro is here:  
{% include youtubePlayer.html id="Rm-aIyV9Cko" %}
[https://youtu.be/Rm-aIyV9Cko](https://youtu.be/Rm-aIyV9Cko)



If you or anyone you know is interested in diving into the wondrous world of Printed Circuit Board assembly, take a look and pass it along!



![images/e23a24f6df96062e5c353b22454b1061.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e23a24f6df96062e5c353b22454b1061.png)
![images/627ec73c94f27d31cfb0647b340b68a9.png](https://gitlab.com/funinthefalls/k40/raw/master/images/627ec73c94f27d31cfb0647b340b68a9.png)
![images/87f12761110c12968a1c5c4ee4ec8530.png](https://gitlab.com/funinthefalls/k40/raw/master/images/87f12761110c12968a1c5c4ee4ec8530.png)
![images/0f2cf39a29e992941792c9b9760430b2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0f2cf39a29e992941792c9b9760430b2.png)
![images/f3b46f54d0ed6b4b247af2ffc5bd1aaa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3b46f54d0ed6b4b247af2ffc5bd1aaa.jpeg)
![images/b88fa835b26db5168c23480ab5531553.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b88fa835b26db5168c23480ab5531553.jpeg)

**"Brian Bland"**

---


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/Q8xKedQx4q7) &mdash; content and formatting may not be reliable*
