---
layout: post
title: "For those who need it. I just did mine"
date: August 30, 2016 02:38
category: "Materials and settings"
author: "Ariel Yahni (UniKpty)"
---
For those who need it.  I just did mine.  [http://www.instructables.com/id/How-to-Adjust-for-Wood-Thickness-and-Kerf-on-a-Las/?ALLSTEPS](http://www.instructables.com/id/How-to-Adjust-for-Wood-Thickness-and-Kerf-on-a-Las/?ALLSTEPS)

![images/8e95da3e2c95c4f36a99c4efa0518cd4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e95da3e2c95c4f36a99c4efa0518cd4.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Jonathan Davis (Leo Lion)** *August 30, 2016 02:46*

Quite a useful idea


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/WZDoVmb8W6A) &mdash; content and formatting may not be reliable*
