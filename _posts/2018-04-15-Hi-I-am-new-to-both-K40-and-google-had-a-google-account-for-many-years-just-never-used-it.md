---
layout: post
title: "Hi, I am new to both K40 and google+, had a google+ account for many years, just never used it"
date: April 15, 2018 16:42
category: "Discussion"
author: "Derek K Johnson"
---
Hi, I am new to both K40 and google+, had a google+ account for many years, just never used it.  I hope to post some of my success's and hopefully not so many failures.





**"Derek K Johnson"**

---
---
**HalfNormal** *April 15, 2018 16:49*

Congratulations on your new K 40 and welcome to the group! Look forward to seeing what you do with your new laser.


---
**Don Kleinschnitz Jr.** *April 15, 2018 17:40*

Welcome, please read the getting started post pinned at the top of  this community including all the links :). 

When you need help just ask. 


---
*Imported from [Google+](https://plus.google.com/104439543384457387314/posts/6N2zT5eh3PW) &mdash; content and formatting may not be reliable*
