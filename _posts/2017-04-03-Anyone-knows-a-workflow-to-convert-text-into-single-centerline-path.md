---
layout: post
title: "Anyone knows a workflow to convert text into single centerline path"
date: April 03, 2017 20:39
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Anyone knows a workflow to convert text into single centerline path. 



Appreciate the suggestions





**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *April 03, 2017 20:56*

Copy & then paste it as an image. Then do a centerline trace on the resulting object. Should be doable in all of the AI/Corel/Inkscape packages. I'm not at my desk so can't check right now but it seems it should work that way.


---
**Ariel Yahni (UniKpty)** *April 03, 2017 21:20*

Thanks will try


---
**Stephen Kongsle** *April 04, 2017 21:33*

If you're okay with tracing from a bitmap, I've had good results using the freeware version of WinTopo for centerline tracing line art. I wouldn't expect it to do a great job with a serif font, but tracing a sans serif font might have acceptable results. Even if it doesn't work out for your text tracing, Wintopo is worth adding to your toolbox for general purpose centerline tracing (and you can't argue with the price).


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Uno6YzwgTHz) &mdash; content and formatting may not be reliable*
