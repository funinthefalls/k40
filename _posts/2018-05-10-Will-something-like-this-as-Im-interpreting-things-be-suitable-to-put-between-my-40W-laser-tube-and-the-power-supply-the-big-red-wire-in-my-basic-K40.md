---
layout: post
title: "Will something like this, as I'm interpreting things, be suitable to put between my 40W laser tube and the power supply (the big red wire) in my basic K40?"
date: May 10, 2018 15:56
category: "Modification"
author: "David Piercey"
---
Will something like this, as I'm interpreting things, be suitable to put between my 40W laser tube and the power supply (the big red wire) in my basic K40?  









**"David Piercey"**

---
---
**Joe Alexander** *May 10, 2018 16:17*

yep that is the proper connector for the high voltage line.


---
**Ariel Yahni (UniKpty)** *May 10, 2018 16:25*

Yes, I use the exact same one


---
**David Piercey** *May 10, 2018 16:28*

**+Joe Alexander** Thanks for the confirmation!


---
**David Piercey** *May 10, 2018 16:29*

**+Ariel Yahni** Thanks for the confirmation!


---
**Kelly Burns** *May 10, 2018 18:16*

Every time I see these linked, I think “I need to order that to have on hand”.  I’m doing it today.  Reports are that they work great. 


---
**David Piercey** *May 10, 2018 20:05*

**+Kelly Burns** Anything else worth having on hand like this?  Replacing on principle (e.g., expected working life, replace before it goes as it going causes other problems)?


---
**Kelly Burns** *May 11, 2018 12:17*

**+David Piercey**  not sure on life, but I have had lenses and mirrors develop cracks.  I keep spare mirrors and lenses and power supply (cheaply K40) I would keep a spare tube if they didn’t have limited shelf life.  When it gets closer to end of life, I’ll order a new one.   Won’t be so critical when I get new DIY laser online. 


---
**David Piercey** *May 11, 2018 16:15*

**+Kelly Burns** Thank you.




---
**Rodney Huckstadt** *September 20, 2018 08:07*

can we get these in australia ? If I can get them local it would be great




---
*Imported from [Google+](https://plus.google.com/114972156206786232028/posts/5mShDuLKwsX) &mdash; content and formatting may not be reliable*
