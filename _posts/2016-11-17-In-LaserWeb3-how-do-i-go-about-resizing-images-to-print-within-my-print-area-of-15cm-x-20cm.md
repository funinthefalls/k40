---
layout: post
title: "In LaserWeb3 how do i go about resizing images to print within my print area of 15cm x 20cm"
date: November 17, 2016 01:13
category: "Software"
author: "Jonathan Davis (Leo Lion)"
---
In LaserWeb3 how do i go about resizing images to print within my print area of 15cm x 20cm





**"Jonathan Davis (Leo Lion)"**

---
---
**Ariel Yahni (UniKpty)** *November 17, 2016 01:52*

Budy have you joined here [https://plus.google.com/communities/115879488566665599508](https://plus.google.com/communities/115879488566665599508)


---
**Jonathan Davis (Leo Lion)** *November 17, 2016 01:54*

**+Ariel Yahni** Now i have joined the laserweb group


---
**Ariel Yahni (UniKpty)** *November 17, 2016 01:54*

When you load an image in the cam section you have the first option called Bitmap Resolution, the higher the number the small the image. The correct procedure would be to set the image size and dpi outside LW and then just set in LW the dpi chosen﻿


---
**Jonathan Davis (Leo Lion)** *November 17, 2016 01:57*

**+Ariel Yahni** ok, i did figure that out via the LaserWeb3 wiki page. on a second question, i go to do a print run but when it completes it doesn't look like the lines were fully burned in.


---
**Jonathan Davis (Leo Lion)** *November 17, 2016 02:00*

I'm guessing it might have something to feed cut rate or something?


---
**Ariel Yahni (UniKpty)** *November 17, 2016 02:09*

Yes, you need to turn the feed rate down so it goes slower thus burning more


---
**Jonathan Davis (Leo Lion)** *November 17, 2016 02:10*

**+Ariel Yahni** Got it and Thank you.


---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2016 02:24*

Looks like you got the answers you needed. Thanks Ariel. 


---
**Jonathan Davis (Leo Lion)** *November 17, 2016 02:27*

**+Ray Kholodovsky** Indeed to that and the machine is running perfectly, Thank you so for your help.


---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2016 02:29*

Well, we spent 3 days getting your setup figured out, so it better be working well! You're welcome. 


---
**Jonathan Davis (Leo Lion)** *November 17, 2016 02:32*

**+Ray Kholodovsky** Indeed to that, and now the information gained can be be used with similar systems.


---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2016 02:34*

**+Jonathan Davis** Sounds great, why don't you pull up those chat records and take some pics so you can make a Upgrade Guide like Carl did for the laser?  [https://www.dropbox.com/s/ngky9g3yh2gpa6r/C3D%20Installation.pdf?dl=0](https://www.dropbox.com/s/ngky9g3yh2gpa6r/C3D%20Installation.pdf?dl=0)


---
**Jonathan Davis (Leo Lion)** *November 17, 2016 02:37*

**+Ray Kholodovsky** I'm not making any promises but i'll see what i can develop, in terms of a install guide.


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/64hGpy5RdmR) &mdash; content and formatting may not be reliable*
