---
layout: post
title: "A few months ago I posted a Masonic award plaque I made for my lodge"
date: October 11, 2017 19:01
category: "Object produced with laser"
author: "Ned Hill"
---
A few months ago I posted a Masonic award plaque I made for my lodge.   With that piece I had glued the frame and panel to a piece of 1/8" ply which was the easiest thing to do.  I then mused about doing some joinery if I made another one and **+Yuusuf Sallahuddin** suggested using splines.  I've done splines on mitered corners but not with a corner composed of two butt joints.  Fast forward to now and I got a commision to make one for another lodge.  After thinking about it some more I decided to use an interior positioned L-shaped spline.  I increased the width of the frame components and used a 1\8" slot cutter with my router table to cut slots in the joining faces of the frame components.  I then modeled and cut the splines on my laser cutter from 1/8" hard maple to give some good contrast with the black walnut of the frame (note, splines were cut 45deg to the wood grain for added strength).  Routed a rabbit on the back inside edge of the frame components to drop the engraved alder panel into.   Bit of a pain with the extra steps but the end result was awesome and extremely solid.  (Pics really don't do this thing justice :) 



![images/994762ee6149854352693a837367c4bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/994762ee6149854352693a837367c4bd.jpeg)
![images/7452f9bc1757150f549092597e06f823.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7452f9bc1757150f549092597e06f823.jpeg)
![images/15d880ece900343ad589c4bab8b67cad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15d880ece900343ad589c4bab8b67cad.jpeg)
![images/380020f5f709e51bce404abbd2cea4ab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/380020f5f709e51bce404abbd2cea4ab.jpeg)
![images/d61d44455bb9e6dca058250a37eeb218.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d61d44455bb9e6dca058250a37eeb218.jpeg)
![images/4c97226454eede0743d93223382341e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c97226454eede0743d93223382341e6.jpeg)
![images/479b4410fa0434a78afb6da53ddf0e9b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/479b4410fa0434a78afb6da53ddf0e9b.jpeg)

**"Ned Hill"**

---
---
**Ariel Yahni (UniKpty)** *October 11, 2017 20:03*

That look amazing!!!


---
**Anthony Bolgar** *October 11, 2017 22:37*

Great job. I think they will love it.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 12, 2017 01:07*

Lovely work Ned. Looks spectacular.


---
**Nigel Conroy** *October 16, 2017 12:26*

Very Nice work as usual Ned. What settings did you use on the Alder?

Assuming that the black is just the engrave burn correct?

Not spray and sand


---
**Ned Hill** *October 16, 2017 14:47*

**+Nigel Conroy** The alder was first coated with gloss brush on poly (2X).  Masked and engraved at ~8ma at 550m/s.  I then darkened the engraving with minwax mission oak satin polyshade spray and removed the masking.  With something this detailed you have have to balance power and speed so you don't blow the details out.  This means the engraving isn't very deep, and therefore not as dark, so the polyshade ups the contrast to pop more details.


---
**Nigel Conroy** *October 18, 2017 00:42*

Thanks

What product are you using to mask?

I've just been using blue painters tape which is tricky to overlap accurately


---
**Ned Hill** *October 18, 2017 03:37*

For large pieces like this I use RTape Conform 4075RLA  vinyl sign transfer tape.  I bought a 8.5" wide roll from here, [signwarehouse.com - RTape Conform 4075RLA High Tack Transfer Tape 8.5 in](https://www.signwarehouse.com/shop/merchant.mvc?Store_code=SC&Screen=PROD&Product_Code=AP-RN-407585)




---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/cJQyyGMXUek) &mdash; content and formatting may not be reliable*
