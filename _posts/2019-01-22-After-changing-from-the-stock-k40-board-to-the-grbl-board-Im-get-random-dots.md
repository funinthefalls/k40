---
layout: post
title: "After changing from the stock k40 board to the grbl board I'm get random dots"
date: January 22, 2019 19:01
category: "Hardware and Laser settings"
author: "Chris X"
---
After changing from the stock k40 board to the grbl board I'm get random dots. I've changed acceleration, re worked the image from scratch and steps per mm.

![images/8c63a776cf50f49ea9c077fc99cfc767.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8c63a776cf50f49ea9c077fc99cfc767.jpeg)



**"Chris X"**

---
---
**'Akai' Coit** *January 22, 2019 19:38*

Have you run the process more than once and the dots appear in different places each time or the same places each time?


---
**Chris X** *January 22, 2019 20:33*

**+'Akai' Coit** yes about 30 times now trying different settings. Always in different spots. It seems like they are always next to an engraved portion. They are rarely in the Middle


---
**'Akai' Coit** *January 23, 2019 02:30*

What about more than once with the same settings? Did the dots still come out in "random" locations or were they consistent?


---
**LightBurn Software** *January 23, 2019 04:38*

What software are you using to generate the GCode? Are you telling that software to dither, or threshold when outputting?


---
**Chris X** *January 23, 2019 08:29*

**+LightBurn Software** laserweb and light burn. I've tried greyscale, black and white, and threshold. 


---
**Chris X** *January 23, 2019 08:30*

**+'Akai' Coit** yes. Dots are random. Always seem to be right next to the engraved portions. Almost as if sometimes fireing a bit to early


---
**'Akai' Coit** *January 23, 2019 08:33*

Initial thought is a glitch in the code it's executing. Never used that software and board. But if you can debug the code that's being ran, it could give you answers.


---
**Chris X** *January 23, 2019 21:56*

Well I feel rediculous. Only after changing my computer's display settings and zooming all the way in was I finally able to find the whole image was covered in 1 or 2 shades of of white pixels! Got it cleaned up and it's good to go! Thanks for your help!


---
**'Akai' Coit** *January 23, 2019 23:21*

Glad you were able to get the issue resolved. :)


---
**Kelly Burns** *January 24, 2019 12:22*

**+Chris X** What would be ridiculous is if you didn't come back and report the actual problem. I have seen this same issue posted before. So you were not alone.  Thanks for posting back.


---
**Chris X** *February 12, 2019 18:21*

Well what I thought was solved is not. Both vector images and raster still producing the same dots. I've installed a resistor and cap on the PWM line for a noise filter and still no result 😥


---
**Chris X** *February 13, 2019 18:06*

Looks like the problem is tied to the $28 setting of 0(default) and other high frequency settings. Upon changing it to 5 or 6 the dots are eliminated. Any insight as to why?


---
**LightBurn Software** *February 13, 2019 19:13*

You initially said "the grbl board", not "the Gerbil board" - the $28 setting is a custom setting for that version of GRBL only, which would've been good to know at the beginning. Not all power supplies respond well to higher frequency PWM signals, so changing the period can help, which is what that specific version of GRBL does with $28.


---
**Chris X** *February 13, 2019 23:01*

**+LightBurn Software** I apologise for this misunderstanding. I'm currently rewiring with all shielded cables. Hopefully this will resolve it.


---
*Imported from [Google+](https://plus.google.com/111502406165042536325/posts/HhmufcfW8iw) &mdash; content and formatting may not be reliable*
