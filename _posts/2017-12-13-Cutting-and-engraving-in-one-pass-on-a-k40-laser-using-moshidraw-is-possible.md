---
layout: post
title: "Cutting and engraving in one pass on a k40 laser using moshidraw is possible"
date: December 13, 2017 03:09
category: "Discussion"
author: "james blake"
---
Cutting and engraving in one pass on a k40 laser using moshidraw is possible.  I know most people will say this can not be done but I did it tonight.  The software does not like to do it and I had to reset machine and software to get it to repeat.  The system will only do the cut then the engrave as far as I have been able to get it to work.  I did it by using 2 colors for the work and selecting the check boxes next to them in the work box and unselecting the all box, you can set each color to its own settings then select both check boxes next to the colors.





**"james blake"**

---
---
**james blake** *December 14, 2017 03:29*

Video of it working.

![images/3e2d81813f34ae7c622e7ee2c32bfd38](https://gitlab.com/funinthefalls/k40/raw/master/images/3e2d81813f34ae7c622e7ee2c32bfd38)


---
*Imported from [Google+](https://plus.google.com/115955612231558356912/posts/PbWzARamGGq) &mdash; content and formatting may not be reliable*
