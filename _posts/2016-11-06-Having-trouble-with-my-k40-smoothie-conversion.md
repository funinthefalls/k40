---
layout: post
title: "Having trouble with my k40 smoothie conversion"
date: November 06, 2016 02:19
category: "Smoothieboard Modification"
author: "George Fetters"
---
Having trouble with my k40 smoothie conversion.  When I press X home it moves in the wrong direction.  when i move X minus it moves in the right direction x home moves away from the minus direction.  I am not sure what is wrong.



Y home moves in the right direction and y minus and y plus does too.





**"George Fetters"**

---
---
**Ariel Yahni (UniKpty)** *November 06, 2016 02:33*

So X moving inverted? Either rotate the connector or add a  !  To the DIR pin in  the config


---
**Scott Marshall** *November 06, 2016 02:55*

If it's moving correctly when you jog, it's your endstop detector not being seen.



If the home command is called, this will cause it to seek in the wrong direction for a short distance them stop.

(this assumes it's set properly in the Config file - if it's backwards there it will do the same thing))

Check your X switch output - it should be high unless made.







PS (make sure BOTH grounds on the FFP are grounded - they run separate grounds to each opto - who knows why, but they're separate all the way from the M2 to each opto  -- pins 6 & 7 of the FFC connector)





PS PS. You may have to add a 4.7K pullup to get the endstops working right - there's a lot of How-to's out there that neglect this tidbit. The M2 has these pullups on it, and you lose the function once you disconnect it. The smoothie inputs don't do the job reliably.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 06, 2016 07:33*

This seems similar to what is going on for me. I just inverted the smoothie config for X with a ! in front. However, I still need to look into what's going on with mine, as images actually laser mirrored too (so I preprocess by mirroring them before adding them). Just been too lazy to bother fixing it for the time being, since I can work around it.


---
**Ariel Yahni (UniKpty)** *November 06, 2016 10:45*

OMG **+Yuusuf Sallahuddin**​ that's a pain in the arse. Let's fix that


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 06, 2016 10:50*

**+Ariel Yahni** Yeah, it is, but I've been a bit lazy to figure out what is going on lately :D There are a few times where I have forgot to mirror my source & then lasered something with words backwards haha.


---
**George Fetters** *November 06, 2016 11:48*

So Jog moves in the right direction but home moves opposite.  In addition home does not move to the home position it moves very little.


---
**George Fetters** *November 06, 2016 11:49*

If I invert the x the jog moves in the wrong direction but the home move correctly.


---
**George Fetters** *November 06, 2016 11:52*

I am wondering if I should change this setting.  alpha_homing_direction                       home_to_min      # or set to home_to_max and set alpha_max



Does anyone have suggested settings for Endstops?


---
**Ariel Yahni (UniKpty)** *November 06, 2016 12:17*

I set mine as Home to Max on Y


---
**George Fetters** *November 06, 2016 14:34*

Did you have to replace beta_min and beta_max when you changed home to max on Y?


---
**Scott Marshall** *November 06, 2016 19:28*

You should have the X optical limit switch connected to Xmin Pin 1 and Ymax pin 1



What you may need to do to your Config file depends on where you got it. You want to set X to min and Y to max. Logic needs to be inverted on both if I recall without looking.



If you send me your Email, I will send you a Copy of my EZConfig - it's all set up to work with K40 and has built in instructions. 



Never edit a Config file with Notepad or Wordpad. Use Gedit (opensource and free)



ALLTEK594@aol.com


---
*Imported from [Google+](https://plus.google.com/114125781899580900020/posts/RAmTxsQA8jm) &mdash; content and formatting may not be reliable*
