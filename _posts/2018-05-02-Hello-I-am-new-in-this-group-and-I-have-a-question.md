---
layout: post
title: "Hello. I am new in this group, and I have a question"
date: May 02, 2018 09:37
category: "Discussion"
author: "Kees Moret"
---
Hello.

I am new in this group, and I have a question.

I haave Coreldraw Laser, but it is in Chinese, is it possible to get an Englisch version?

Thank you< greetz, Kees





**"Kees Moret"**

---
---
**Joe Alexander** *May 02, 2018 21:53*

you might be better off trying an open source software, try K40 Whisperer or Lightburn, they should work with the stock controller or laserweb if you upgrade it.


---
**Ned Hill** *May 03, 2018 13:14*

**+Kees Moret** you can change the language.  Look for the small pink icon in the windows system tray at the bottom and right click it.  There will be a language selection option.  As Joe said, your other option with the stock controller is to use K40 Whisperer.


---
**Kees Moret** *May 08, 2018 19:59*

Sorry for my late reaction..

Thank you both, I find out..

Works Lightburn easyer then Corel?

Thank you already...


---
**Ned Hill** *May 13, 2018 17:42*

**+Kees Moret** light burn is much better but it requires that you upgrade your control board. Corellaser is useable but it’s quite buggy unless you get a more current version of Corel Draw, which is pricey. K40 whisperer is a better control option with the stock board but you need to design in another program like Ink Scape. 


---
**Kees Moret** *May 14, 2018 07:12*

**+Ned Hill**  Thank you for your  advise..

I will try it out.


---
*Imported from [Google+](https://plus.google.com/103280492912981255521/posts/YShNQW1c7yx) &mdash; content and formatting may not be reliable*
