---
layout: post
title: "can someone with a ramps 1.4 upgrade please measure the voltage coming out of there d6 pin when the machine is idle?"
date: May 30, 2016 17:00
category: "Discussion"
author: "Derek Schuetz"
---
can someone with a ramps 1.4 upgrade please measure the voltage coming out of there d6 pin when the machine is idle? Mine is 5 v and when i send a M03 S255 command it drops to 0 and that seems oppsite of what should happen





**"Derek Schuetz"**

---
---
**Alex Krause** *May 30, 2016 18:11*

What do you get when you give an M04 s*** command?


---
**Derek Schuetz** *May 30, 2016 19:23*

i have not tried m04 what does that command do


---
**Alex Krause** *May 30, 2016 19:28*

In standard Gcode implementation on lathes and mills m03 is spindle clockwise at commanded speed m04 is spindle counterclockwise at the commanded S*** m05 is spindle off... you may check m05 output voltage as well


---
**Derek Schuetz** *May 30, 2016 19:36*

still reads 5v with M04


---
**Alex Krause** *May 30, 2016 21:02*

Is it possible that you have your positive and ground wired backwards? Or have one of the settings in the config file wrong?


---
**Derek Schuetz** *May 30, 2016 21:39*

I loaded a fresh firmware on it my d5 has no voltage unless I m05 so that good but my d6 starts at 5v unless I send an m03 s0 command. So maybe this is just normal. the weird thing is when I followed the wiring diagram my laser would not fire so I switched those wire d5/d6 and I could then fire it. It was cutting and engraving fine but it was randomly firing so I'm not sure what was up with that


---
**Derek Schuetz** *May 30, 2016 22:23*

Well I just had sudden realization that almost makes everything makes sense. 


---
**Jon Bruno** *May 31, 2016 13:24*

DO share.. I think I remember a similar issue when I was fooling around with mine. Actually I've stopped messing with it hoping that someone else on the bleeding edge will figure it all out first.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/2JHuCCuRg1a) &mdash; content and formatting may not be reliable*
