---
layout: post
title: "Is it normal that if I move the head of my laser when my X5 is turned off the laser fires?"
date: November 15, 2016 04:49
category: "Smoothieboard Modification"
author: "K"
---
Is it normal that if I move the head of my laser when my X5 is turned off the laser fires? The steppers generating current when moved seems to be causing the laser to fire when the board is off.  If the X5 is on and the stepper motors have been turned off I can manually move the head with no laser fire. Hitting reset on the X5 also causes the laser to pulse.





**"K"**

---
---
**Jonathan Davis (Leo Lion)** *November 15, 2016 05:10*

I couldn't really say since I'm working from a Chinese made A5 BenBox system. 


---
**K** *November 15, 2016 05:11*

I really need to put my interlock switch on. 


---
**Jonathan Davis (Leo Lion)** *November 15, 2016 05:20*

**+Kim Stroman** ok? 


---
**K** *November 15, 2016 05:21*

**+Jonathan Davis** I was just sort of talking to myself there. That'd take care of the laser firing, but it doesn't answer whether or not it should be firing. 


---
**Stephane Buisson** *November 15, 2016 06:44*

Door switch ???


---
**K** *November 15, 2016 08:44*

 **+Stephane Buisson**  I don't have one installed yet. 


---
**Ariel Yahni (UniKpty)** *November 15, 2016 11:00*

**+Kim Stroman**​​ it should not fire


---
**greg greene** *November 15, 2016 14:16*

Definitely not !!!!


---
**K** *November 15, 2016 14:21*

**+Ariel Yahni** **+greg greene** I didn't think so. I went a weekish between really using my machine due to work, and I can't really remember if in the past I've always had the X5 booted and therefore haven't noticed, or if it's a new issue. My other thought was maybe it's due to the power strip I wired up? It's one of three issues I've encountered over the last couple weeks.


---
**K** *November 15, 2016 14:34*

Ok, it's not the power strip. I unplugged the X5 from the strip. The actual laser cutter is not wired into my power strip, it's on a separate one. I still have the issue. The energizing motors light up the LCD as well as cause the laser to fire as it's being moved.




---
**Don Kleinschnitz Jr.** *November 15, 2016 15:23*

Is the Laser Switch closed? What configuration are you using to drive the laser PWM.


---
**K** *November 15, 2016 15:24*

**+Don Kleinschnitz** The switch is closed. I'm using the level shifter setup so I still can use my pot. 


---
**Ariel Yahni (UniKpty)** *November 15, 2016 15:38*

**+Kim Stroman**​ you should be able keep the pot and use **+Don Kleinschnitz**​ 2 wire connection


---
**Don Kleinschnitz Jr.** *November 15, 2016 15:40*

From my analysis, all bets are off when using a level shift-er. I have no idea how the level shift-er electronics reacts in power transitions. As I recall to make it work required some strange grounding which suggest something is not working as expected.



BTW when using the 2 wire L approach you can leave the pot in.



I have noticed that moving the gantry when its not powered up does turn on some LED's on the smoothie. This may be some form of "back emf" from the steppers. Have not tested with the laser enabled cause I have interlocks installed.



WARNING... WARNING!

You should not be in the cabinet without interlocks and the Laser Switch ON.... as I have warned before when building new designs there are modes that can cause the laser to fire that either we have not found or relates to a failure. This may be one of them.


---
**K** *November 15, 2016 16:58*

**+Ariel Yahni**(null) My PSU requires a third to fire, and I could never get the pot work. 


---
**K** *November 15, 2016 16:59*

**+Don Kleinschnitz** I'm going to add locks today. At the least I then can know it won't fire while I'm adjusting the head. 


---
**Don Kleinschnitz Jr.** *November 15, 2016 17:25*

Thank you! 

BTW I found that an easy way to wire interlocks it to bring them to a breakout board which is then connected to the LPS laser switch loop. 

<s>----------</s>

[http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html](http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html)

 <s>---------</s>

The schematic and picture below shows how. 



This board through J75 connects to the LPS laser switch loop.



J11 goes to a led/resistor on the front panel telling you if any interlocks are open.



Then you have simple two-wire assy's that come from each interlock switch to the breakout which uses screw terminals. 



You can ignore the middleman board I mounted it there for convenience. 



[https://goo.gl/photos/qGgxQrTgP1tTKhVD7](https://goo.gl/photos/qGgxQrTgP1tTKhVD7)



[https://goo.gl/photos/Rm7zCA7uDLcQNbb1A](https://goo.gl/photos/Rm7zCA7uDLcQNbb1A)



[https://goo.gl/photos/5sYHC9Ye7XaYbCFSA](https://goo.gl/photos/5sYHC9Ye7XaYbCFSA)

![images/01477d90cda835ac26dd46b931d25ea4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/01477d90cda835ac26dd46b931d25ea4.jpeg)


---
**K** *November 15, 2016 18:12*

**+Don Kleinschnitz** is there any reason not to do

Something like this? Ignore the hand drawn red wire. The drawn green wire is what I had been advised. It would be wired into the laser switch. ![images/7c0c34fe9df6e4ff48694a9e3d0eab0d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7c0c34fe9df6e4ff48694a9e3d0eab0d.jpeg)


---
**Don Kleinschnitz Jr.** *November 15, 2016 18:40*

Any configure where the switches are all in series with each other and the laser switch works. 

Its hard for me to tell from the picture but that looks overly complex.

You want it wired so that when the lid is closed the switch is shorted.


---
**K** *November 15, 2016 18:55*

![images/00973af5f997f5b0146c98e09acfce40.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00973af5f997f5b0146c98e09acfce40.jpeg)


---
**Don Kleinschnitz Jr.** *November 15, 2016 21:43*

Assuming the cover closes the lever you want to connect to NO?


---
**K** *November 15, 2016 21:44*

D'oh. Yes. Thanks!


---
**K** *November 15, 2016 22:11*

I think (fingers crossed) I've figured it out. I pulled all the wires again, and I closely examined each end. The crimp on the jumper going from the shifter to ground on the board was kind of loose. I recrimped it and put a new housing on it. It's now not firing when the carriage is moved.


---
**Don Kleinschnitz Jr.** *November 16, 2016 01:39*

**+Kim Stroman** gnding design is critical when hanging around inductors and OH YA.... 30,000 volt lasers.

Many think that "ground is ground the world around". Grounds and eliminating their loops are every bit a design problem as signals :).



I still think there is something weird about the LS gnding. Level shift-ers are not designed to be drivers they are source follower voltage shift-ers ......


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/6xgBhXjh4cN) &mdash; content and formatting may not be reliable*
