---
layout: post
title: "Not happy with my air assist. Mine makes too much noise and doesn't work well"
date: December 06, 2015 01:49
category: "Modification"
author: "Todd Miller"
---
Not happy with my air assist.  



Mine makes too much noise and doesn't work well.

 

I have seen where Air Brush Compressors have been

used and they have a water separator for dry air (no oil)

and adjustable PSI regulator.



I just don't know how the hose terminates.  It looks

like a proprietary screw on fitting for air brushes,  but

how do you adapt that to a 5mm - 8mm hose ?





**"Todd Miller"**

---
---
**Gee Willikers** *December 06, 2015 02:09*

This may help you: [http://sumidacrossing.org/Tools/Airbrush/AirbrushHoses/ConnectorStandards/](http://sumidacrossing.org/Tools/Airbrush/AirbrushHoses/ConnectorStandards/)  but your best bet may be to connect an airbrush hose to the compressor and cut off the fitting at the other(laser) end.


---
**FNG Services** *December 06, 2015 02:45*

I have a K40 and I just added air assist with this compressor from Harbor Freight [http://t.harborfreight.com/16-hp-58-psi-oilless-airbrush-compressor-60329.html](http://t.harborfreight.com/16-hp-58-psi-oilless-airbrush-compressor-60329.html) . I have the LightObjects air assist head. I used the hose that came with the compressor and just cut the end off. It fits the LO head but it's tight. All seems to work great, nice and quite. The compressor dose get warm.


---
**Gee Willikers** *December 06, 2015 02:51*

**+G Bennett** Is the compressor quiet? I have an airbrush compressor but it's the loudest piece of equipment in the room.


---
**Todd Miller** *December 06, 2015 02:58*

That's the compressor I'm looking at, but

I want quiet.  The air pump I have is too

loud,the buzzing drives me nuts and it's

even sitting on the basement floor.


---
**FNG Services** *December 06, 2015 03:12*

It's very quiet, I have it setting right up next to my laser and the fan on the power supply of the laser is loader then the compressor. With both the power supply fan and the exhaust fan running you will never here the compressor. There are a couple of videos out on YouTube that show how quiet it is.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 06, 2015 03:14*

My solution to noisy stuff is to just crank the music louder haha. But I am using an air-pump from a tanning airbrush kit I got for my air-assist. It is about as loud as an aquarium air-pump. I literally just zip-tied it to the laser head (just as a test to see if it works well enough to actually use permanently).


---
**Gary McKinnon** *December 06, 2015 17:38*

They all make noise, unless you have a low-power one with an acoustic baffle chamber but they don't have the power.



For power and silence you want a magnetic air pump, pricier but cool and high-tech :)


---
**Gary McKinnon** *December 06, 2015 17:44*

And for sizing, cut off the end of the tube and use an aquarium inline adapter.


---
**Todd Miller** *December 11, 2015 00:19*

I received my Harbor Freight  1/5HP 58PSI

air brush compressor today and this thing

is quiet !  I paid $80.00 w/coupon + S/H.



It came with an air brush set, but I'll give

that away to someone that can use it.


---
**Gee Willikers** *December 11, 2015 01:49*

Mine should arrive tomorrow.


---
**Gary McKinnon** *December 11, 2015 13:29*

Found the same on in the UK :



[http://www.ebay.co.uk/itm/HIGH-QUALITY-OIL-LESS-MINI-PISTON-TYPE-AIRBRUSH-AIR-COMPRESSOR-/261651425988?hash=item3ceba3eac4:g:WmUAAOSw54xUWjox](http://www.ebay.co.uk/itm/HIGH-QUALITY-OIL-LESS-MINI-PISTON-TYPE-AIRBRUSH-AIR-COMPRESSOR-/261651425988?hash=item3ceba3eac4:g:WmUAAOSw54xUWjox)


---
**Todd Miller** *December 12, 2015 04:14*

During the last few days I did some light etching in hobby plywood and this pump

gets Hot To The Touch in a short time. I

kept on going for a few hours and the pump

kept going, just hot !


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/UD5rzC6oHJC) &mdash; content and formatting may not be reliable*
