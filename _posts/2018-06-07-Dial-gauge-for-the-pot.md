---
layout: post
title: "Dial gauge for the pot"
date: June 07, 2018 03:32
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Dial gauge for the pot.



[https://www.thingiverse.com/thing:2949736](https://www.thingiverse.com/thing:2949736)





**"HalfNormal"**

---
---
**Justin Mitchell** *June 07, 2018 09:48*

9!? Ours goes to 11 ;)




---
**Don Kleinschnitz Jr.** *June 07, 2018 13:00*

I just use a small voltmeter, more accurate :) Scale 0-5. :).





[photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/JhYsb9ubxvHcw7Eg1)


---
**Ned Hill** *June 07, 2018 13:01*

Not "ludicrous" power? ;)


---
**James Rivera** *June 07, 2018 17:47*

And a setting for plaid! 🤣


---
**Doug LaRue** *June 08, 2018 02:08*

Maybe circular and in mA range?![images/7e6f3a2fec05036da137a35dfb577511.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e6f3a2fec05036da137a35dfb577511.jpeg)


---
**Don Kleinschnitz Jr.** *June 08, 2018 11:38*

**+Doug LaRue** The current  (ma) for a given pot position changes as the LPS and tube get older...


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/dfkatrwXcKZ) &mdash; content and formatting may not be reliable*
