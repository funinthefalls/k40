---
layout: post
title: "I am setting up my new K40 and have one question regarding the grounding wire, rod, etc"
date: August 02, 2015 18:05
category: "Hardware and Laser settings"
author: "James McMurray"
---
I am setting up my new K40 and have one question regarding the grounding wire, rod, etc.  All the outlets in my home are grounded, so would I be able to connect the grounding post on the back of the laser to a the grounded outlet with just a three prong power cord with the "Hot" blades disconnected, so that just the grounding post is connected? 





**"James McMurray"**

---
---
**Jon Bruno** *August 02, 2015 18:13*

Typically youd probably be fine with that's a a minimum of protection but the manufacturers of these "cheap" lasers assemble them after the chassis are painted.

Chances are that the grounding post on the chassis may be insulated somewhat with paint. It would behoove you to examine this point and make a determination of its bond. If there is any paint between the chassis and the grounding conductor the current might much rather travel through you than the provided grounding point. 


---
**ThantiK** *August 02, 2015 18:24*

Let's just put it this way.  We're talking 15Kv here.  Do <i>you</i> trust all of your electronics, etc to handle 15Kv transients on the ground line?



I wouldn't.  Ground your laser properly.


---
**James McMurray** *August 02, 2015 18:33*

Thank You for the information, I don't have the knowledge to know why I should or shouldn't trust my electronics to handle 15KV transients on the ground line.  Seems like I shouldn't.  I bought this for personal use and don't have a good place to pound in a grounding rod as the instructions suggest.  What else can I do?  Waterline under the sink?  


---
**Jon Bruno** *August 02, 2015 18:42*

Grounding to a cold water pipe will offer you a bit more protection than not. As long as the pipe you are grounding to is conductive all the way out to the external source.

It would be inappropriate if you have pex (plastic) pipes anywhere in between.


---
**ThantiK** *August 02, 2015 18:52*

**+James McMurray**, the laser tube operates at <i>very</i>  high voltages.  If anything ever goes wrong, the ground line is there to protect you.  So if something goes wrong, you've got 15Kv going through your ground line.  Which is also connected to everything else with a ground (and a lot of times connected to neutral in the US).  This means if anything ever happens and your laser needs to ground out -- everything you own that's electronic will die.



You just want to make sure you're not endangering yourself and secondly not going to damage your laser.


---
**James McMurray** *August 02, 2015 19:25*

Thank You Very Much for the explanations, I really appreciate your knowledge and am thankful you shared with me.  Now to try and set up a good ground!


---
*Imported from [Google+](https://plus.google.com/115183135050042620815/posts/QH9QARjrgJP) &mdash; content and formatting may not be reliable*
