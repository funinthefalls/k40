---
layout: post
title: "So this will seem like a silly question to some but here goes..."
date: June 28, 2016 15:00
category: "Discussion"
author: "Nigel Conroy"
---
So this will seem like a silly question to some but here goes...

I've noticed some machines mention they can be either 110 or 220 volts.



I'd prefer the 110 volts.

**+Derek Schuetz**  posted a link to this machine and I think I'll get a quicker response from here then the ebayer. 



Is this 110v?



Is it worth the extra money to save the time of having to do the upgrades? 



Any help appreciated.





**"Nigel Conroy"**

---
---
**Ariel Yahni (UniKpty)** *June 28, 2016 15:18*

I would assume that this is 110v because it's on the US. So if you are in the states that would be wise choice. 


---
**Nigel Conroy** *June 28, 2016 15:19*

**+Ariel Yahni** 

Thanks.



Is the system worth the extra money?


---
**Ariel Yahni (UniKpty)** *June 28, 2016 15:22*

**+Nigel Conroy**​ it's all about the budget you have and what you plan/need to cut. Most people buy the entry level 3020 size machine because of budget or need but mostly budget. I don't know anyone that would not want a bigger more powerful tube.  Also it seems this models come with the rotary, motorized bed level all very good added values. In the end if you can afford it, go for it. Unless someone has / knows another better priced machine


---
**Nigel Conroy** *June 28, 2016 15:25*

**+Ariel Yahni** 

I got approved to spend between $1000-$1500 for school classroom.  My thought is this will save me a load of time during my summer holidays setting one up. Which I can spend learning how to use it.


---
**Nigel Conroy** *June 28, 2016 15:33*

On closer inspection of one of the images it has a sticker with 110 Volt on one side and 220 Volts on each side....


---
**Derek Schuetz** *June 28, 2016 16:20*

It should be configured for the US my. guess is the seller is just using some stock photos


---
**Nigel Conroy** *June 28, 2016 16:34*

Thanks, I've ordered one identical that states its 110v. 


---
**Derek Schuetz** *June 28, 2016 16:38*

Make sure there identical some sellers sell old models that one I linked had the motorized. Bed and was the best bang for the buck


---
**John-Paul Hopman** *June 28, 2016 16:58*

I thought all power supplies had a switch for selecting between 110 (US) or 220 (EU).



I believe the 50W lasers I have looked at were all in the 1.5k range. While you wouldn't need to modify the case for a more powerful laser as with a K40 and the larger cutting area is ideal (I would want to cut 12x12 inch sheets myself), you would still eventually want the upgrades offered by air assist, mirrors (?), and smoothie board controller.



If an 8x12 inch cutting area will work for your projects, and you plan to engrave or only cut thin (3mm) sheets, I would suggest buying two or three of the second gen laser cutters and upgrading them yourself over the summer so you are familiar with how they work. You should then always have a spare should one break.


---
**Derek Schuetz** *June 28, 2016 17:08*

**+John-Paul Hopman** the upgraded for the 1500 is well worth it..air assist motor z table much larger cut volume print from USB stick lighting laser alignment emergency stop button independent power supplies and better electronics 


---
**Nigel Conroy** *June 28, 2016 17:17*

Here is a link to the one I purchased 



[http://www.ebay.com/itm/391317039073](http://www.ebay.com/itm/391317039073)


---
**Derek Schuetz** *June 28, 2016 18:11*

So that is an older model with manual tablw


---
**Nigel Conroy** *June 28, 2016 18:15*

**+Derek Schuetz**​ the one you posted has a manual table. Does that mean this doesn't have the updated electronics etc...?



It did say it was an updated version


---
**Nigel Conroy** *July 13, 2016 19:06*

Laser Engraver finally arrived. It's a lot bigger and heavier than I thought!!!



How do I tell if it has the most up to date board, etc...?



It does have a motorized, with an up and down button on the side.


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/1PNyT9i8adv) &mdash; content and formatting may not be reliable*
