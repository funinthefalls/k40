---
layout: post
title: "What's the easiest way to remove the belt from the X axis from the laser head carriage?"
date: September 25, 2016 03:37
category: "Original software and hardware issues"
author: "Alex Krause"
---
What's the easiest way to remove the belt from the X axis from the laser head carriage? **+Jim Hatch**​ **+Ariel Yahni**​ **+Scott Marshall**​ have you guys done this procedure?





**"Alex Krause"**

---
---
**Ariel Yahni (UniKpty)** *September 25, 2016 03:39*

Haven't tried but I can take a look


---
**Alex Krause** *September 25, 2016 03:47*

**+Ariel Yahni**​ take some pictures if you can


---
**Mircea Russu** *September 25, 2016 07:27*

I'm affraid you need to take the XY gantry out. It might be doable via the hole in the electronics box but I honestly don't think the gynecologist work is worth it. I like to see what I'm doing :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 25, 2016 12:32*

Yeah I think pulling the whole XY gantry out would be the simplest bet. Looked to be a simple screw holding the belt onto the carriage then.


---
**Don Kleinschnitz Jr.** *September 25, 2016 15:18*

I hope that is not the case. That means you have to do the same to get the x end stop breakout out from under the x arm also?

I hoped that you could take out the screws on the right side through the electronics compartment hole, remove the mirror and mount on left and then swing the arm upward to get to the bottom ...NO?

Is there a belt adjustment for x anywhere?

As soon as I get this smoothie conversion done I am moving all thi to my own frame design. Like I need more projects :).


---
**Jim Hatch** *September 25, 2016 17:45*

**+Don Kleinschnitz**​​ yes you can remove the screws thru that access hole. It's a bit fiddly and I would use a magnetic tipped driver so the screws don't fall somewhere unreachable (although if they do you can generally tilt the machine up to make them slide somewhere you can get to them).



I had to do it that way to adjust the head & belt after replacing the gantry back into the box when I realized I had taken care of an issue on the corner squaring but created another with the X stop.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/VqrgCcUFfcy) &mdash; content and formatting may not be reliable*
