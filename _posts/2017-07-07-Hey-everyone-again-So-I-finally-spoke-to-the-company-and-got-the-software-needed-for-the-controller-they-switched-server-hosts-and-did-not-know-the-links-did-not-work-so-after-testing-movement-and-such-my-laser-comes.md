---
layout: post
title: "Hey everyone... again! So I finally spoke to the company and got the software needed for the controller (they switched server hosts and did not know the links did not work) so after testing movement and such, my laser comes"
date: July 07, 2017 01:34
category: "Original software and hardware issues"
author: "Brandon D"
---
Hey everyone... again! So I finally spoke to the company and got the software needed for the controller (they switched server hosts and did not know the links did not work) so after testing movement and such, my laser comes on instantly when the laser switch is depressed and current pot is turned. The test button seems to be bypassed. I have removed and tested the switch and it works as intended. Anyone have any suggestions where to check next? I'll include a picture of the psu and all since I know they differ.  Thanks again in advance!



![images/f10e0cdfe7855ad6719c52df340988d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f10e0cdfe7855ad6719c52df340988d8.jpeg)
![images/c53f212651dceed94eaef03fecba42ee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c53f212651dceed94eaef03fecba42ee.jpeg)

**"Brandon D"**

---
---
**Don Kleinschnitz Jr.** *July 07, 2017 03:55*

I am unclear as to what the problem  is?



1. When you engage the Laser Switch the laser fires without engaging any other switch?



If the answer to #1 is yes, try disconnecting "L" on the LPS. The rightmost connector and connection. (the labels are on the PCB). Then engage Laser Switch again.



......

Can you fill out this form which will ask all the questions at once?

[docs.google.com - K40 Problem Profile Form](https://goo.gl/forms/d7sW57aUV68NpgI12)

.....

Also Check this out: [https://plus.google.com/s/%23K40GettingHelp/top](https://plus.google.com/s/%23K40GettingHelp/top)




---
**Ned Hill** *July 07, 2017 04:34*

**+Don Kleinschnitz** see his other post.  He has a second hand laser with a bigbear stepper controller.


---
**Brandon D** *July 07, 2017 11:58*

**+Don Kleinschnitz** I mean that the test laser button effectively does nothing. As soon as the laser switch is depressed, the laser turns on. The test button works, though, out of the loop. I also noticed that the grey wire (second to last) on the green terminal to the right is disconnected, which I believe is required for the control board. Is there a schematic listed anywhere on the unit?


---
**Don Kleinschnitz Jr.** *July 07, 2017 14:26*

**+Brandon D** 



To help us help you.

Please insure that you review these:

#K40ReadTheseFirst

#K40Searching

#K40GettingHelp



This site will provide lots of info including schematics and wiring. There are other web sites also cited in the left sidebar: 

[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



K40 LPS connections:

[https://goo.gl/photos/cMAS1Vrde8Wtwejs7](https://goo.gl/photos/cMAS1Vrde8Wtwejs7)



Questions:

1. Did you try disconnecting the L pin on the LPS? I suspect that the controller is holding this at grnd.

2. I cannot tell what the "L" pin on the LPS is connected to on the LPS? Please provide better picture or wiring diagram as to how it is connected.

3. The "L" on the LPS should be connected to pin 9 (PWM) on the controller without a pullup. Is it?

4. The grey wire from the LPS is 5V from what I can tell the your controller does not need 5V so it may not need to be connected

5. Does bigbear provide a schematic of the controller? This would be helpful in troubleshooting.



Please answer referencing the question #.




---
**Don Kleinschnitz Jr.** *July 07, 2017 16:56*

**+Don Kleinschnitz** thanks for filling out the form....



[https://docs.google.com/forms/d/1MobwxZ8COA6bW_0utAhYLYkoiEJZQERNs5wLi_fXgsY/edit?usp=sharing](https://docs.google.com/forms/d/1MobwxZ8COA6bW_0utAhYLYkoiEJZQERNs5wLi_fXgsY/edit?usp=sharing)


---
**Brandon D** *July 07, 2017 17:30*

Did it record my answers properly?**+Don Kleinschnitz** 

I will also answer the others once I have my psu opened tonight. 


---
**Nate Caine** *July 07, 2017 17:31*

Referring to this page:



[bigbear.dk - Stepper Controller Help](https://bigbear.dk/help/stepcontrol)/



As Don has mentioned, the photos are helpful, but we cannot see where the green "Laser On"  wire at the Laser Power Supply goes to on the BigBear board.   This is the green right-most wire on the right-most 4-pin connector.



I <i>suspect</i> the green wire is connected to <b>Pin-9</b> on the BigBear which is the "Laser PWM Output".  <b>I</b> <i>think</i> <b>that is wrong.</b> 



----------------------------------



If the <b>green</b> wire is connected to Pin-9 on the BigBear, here's what I would do as a sanity check:



(a) Set the <b>"Current Regulation"</b> dial approximately to the half-way postion.



(b) Disconnect the green wire from <b>BigBear Pin-9</b>.  <i>(Leave it hanging, or tape it off.)</i>



(c) Locate the latching <b>"Laser Switch"</b> on the front panel <i>(in your photo this is the one with the</i> <i>orange</i> <i>wires)</i>.



(d) Set the <b>"Laser Switch"</b> to the latched <b>UP (</b><i>INHIBIT</i><b>)</b> position.  This should <i>INHIBIT</i> the laser from firing.



(e) Next, locate the momentary <b>"Test Switch"</b> on the front panel <i>(in your photo this it the one with the</i> <i>pink</i> <i>wires)</i>.



(f) With the orange wire <b>"Laser Switch"</b> in the <b>UP (</b><i>INHIBIT</i><b>)</b> position, press and release the pink wire <b>"Test Switch"</b>.  Nothing should happen.



(g) Now, latch the orange wire <b>"Laser Switch"</b> in the <b>DOWN (</b><i>ENABLE</i><b>)</b> position.  The laser should be OFF. 



(h) Press and release the pink wire <b>"Test Switch"</b>.  The laser should fire when you press it to the momentary down position.



(i) You should observe that when you successfully fire the laser as above, that the <b>"Current Indication"</b> meter will vary according to the setting of the <b>"Current Regulation"</b> dial.



-------------------------------



If those tests are successful, the Laser Power Supply is working.

I would now connect the green wire that you previously disconnected, but <b>instead</b> connect it to <b>BigBear Pin-10</b> ("Laser On/Off").



In your current configuration, the BigBear board tells the laser <i>when</i> to turn On and Off.  But the <i>intensity</i> of the laser is set by the operator on the front panel dial.



If these tests work out, then Don or myself, can tell you how to modify the wiring to allow the BigBear board to have total control.  i.e. (a) turn the laser On and Off, and <i>ALSO</i> (b) control the laser power level.



There are several things to do, so only test and modify one thing at a time.  Carefully document your wiring and any changes you make.



============================

BigBear Documents



Green Terminal connection



Pin 1: Step motor 1 winding A - M1A (Also available in laser FFC)



Pin 2: Step motor 1 winding A - M1A (Also available in laser FFC)



Pin 3: Step motor 1 winding B - M1B (Also available in laser FFC)



Pin 4: Step motor 1 winding B - M1B (Also available in laser FFC)



Pin 5: Step motor 2 winding A - M2A (Also available in laser 4pin connector)



Pin 6: Step motor 2 winding A - M2A (Also available in laser 4pin connector)



Pin 7: Step motor 2 winding B - M2B (Also available in laser 4pin connector)



Pin 8: Step motor 2 winding B - M2B (Also available in laser 4pin connector)



<b>Pin 9: Laser PWM Output</b> - Open Collector (Connect your Laser control input signal to this signal - a pullup to 5V may be needed eg. 1K ohm => 5V/1K=1ma)



<b>Pin 10: Laser On/Off</b> - Open Collector (Connect your Laser control input signal to this signal - a pullup to 5V may be needed eg. 1K ohm => 5V/1K=1ma)



Pin 11: Ground - connect to power gnd and laser control input gnd and the SGL (pin 10).



Pin 12: VDC - Power Input for Motors (9-35Volt) - 24V recommended








---
**Brandon D** *July 07, 2017 22:03*

**+Nate Caine** awesome! While having the green disconnected, it allowed the te st button to operate correctly. Now, I moved it to pin 10, and removed the jumper between the white pin 11(ground) which was jumping from 10 to 11. Now that I've replaced it with the green, it does not work. Shall I replace that jumper? 


---
**Brandon D** *July 07, 2017 22:06*

**+Nate Caine** disregard! It was a user error. I didn't tighten it enough! Haha. Absolutely amazing. It works! Now I just have to get the mirrors and frame properly aligned and I'll be good to go! Thank you so much. And of course, if you're willing and able to help me wire it up to remove the potentiometer, that'd be great. 


---
**Don Kleinschnitz Jr.** *July 07, 2017 22:29*

**+Brandon D** when you say it works do you mean you can engrave or the test button now works properly?



**+Nate Caine** how can he engrave without a PWM signal?


---
**Brandon D** *July 07, 2017 22:41*

**+Don Kleinschnitz** the test button works, and when I sent a trial gcode and it engraved where the mirror is actually calibrated. So now I just need to continue calibrations. I think I found a good guide! 


---
**Nate Caine** *July 07, 2017 23:33*

**+Brandon D** That jumper from Pin-10 to Pin-11 makes absolutely no sense.  I suspect the previous owner, never got this machine working properly.



Did you also verify that the front panel potentiometer is correctly controlling the laser intensity as indicated by the panel meter?






---
**Nate Caine** *July 07, 2017 23:40*

**+Don Kleinschnitz** As currently wired, Brandon's machine has the Laser <i>Intensity</i> set by the front panel dial.  (The PWM from the BigBear is ignored.)  



The BigBear controller controls the X- and Y- steppers, and switches the Laser On/Off.  When "On", the intensity is according to the front panel dial.  This is good enough to get his machine up and running.



Once his machine is stable, he can change a few wires to disable the front panel potentiometer, and instead use the BigBear PWM signal.





One step at a time.


---
**Brandon D** *July 07, 2017 23:54*

Yes. And I actually got some test engraves/cuts! Pics:**+Nate Caine** 

![images/09745d143b1f17fdf9aac6d5985cf0b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09745d143b1f17fdf9aac6d5985cf0b7.jpeg)


---
**Don Kleinschnitz Jr.** *July 08, 2017 12:12*

**+Nate Caine** got it. Many controller have the digital information and the intensity in the PWM signal and the on-off is an enable signal with no digital information. I was not familiar with the bigbear but figured it was the same.....


---
**Nate Caine** *July 08, 2017 18:19*

Just some follow-up on the Brandon's BigBear board.



There is some documentation on the BigBear site about an old Ver1 board and they have changed some pins on the Ver2 (which must be the one Brandon has).



On the <b>OLD Ver1</b> board:

Pin-10, input, SGL (Safety Ground Loop)

Pin-9, output, LSR (Laser On/Off)



On the <b>NEW Ver2</b> board:

Pin-10, output, (Laser On/Off)

Pin-9, output, (Laser PWM)



So that explains the jumper that Brandon found from Pin-10 (SGL) to Pin-11 (GND).  That jumper was attempting to "enable" a Ver1 controller to fire the laser by saying that any interlocks or sensors were "safe" to run the machine.



Apparently, the previous owner had wired things for the Ver1 board, but had the current Ver2 board installed.


---
**Brandon D** *July 09, 2017 21:22*

**+Nate Caine** So, whenever you have the time and interest to help me set up the PWM, I'd be more than willing to undertake it!

 I have a question, though... is it possible to have it turn the power up and down according to how light/dark a particular part of an image is?




---
**Nate Caine** *July 10, 2017 01:27*

[bigbear.dk - Stepper Controller Help](https://www.bigbear.dk/help/stepcontrol)/



Read the section on "G-Codes".  You will see G33 can set the PWM dynamically from inside the G-code program.  You should ask BigBear about any design programs that support that feature.



Also ask them if they have a recommended circuit for the PWM interface and what frequency is the PWM output.   


---
*Imported from [Google+](https://plus.google.com/106429087520568972232/posts/4fAxjwP5Dse) &mdash; content and formatting may not be reliable*
