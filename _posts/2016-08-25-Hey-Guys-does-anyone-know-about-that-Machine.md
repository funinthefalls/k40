---
layout: post
title: "Hey Guys, does anyone know about that Machine: ?"
date: August 25, 2016 22:37
category: "Discussion"
author: "Christoph E."
---
Hey Guys,



does anyone know about that Machine: [http://www.ebay.de/itm/60w-CO2-Lasergraviermaschine-Gravurmaschine-Carving-Gravur-Lasergravur-cutter-/191946272256?hash=item2cb0e3ae00:g:90AAAOSw65FXsSJC](http://www.ebay.de/itm/60w-CO2-Lasergraviermaschine-Gravurmaschine-Carving-Gravur-Lasergravur-cutter-/191946272256?hash=item2cb0e3ae00:g:90AAAOSw65FXsSJC)

?



Does it work, does it do a good job? Is it faster/more reliable than the 40w Version?



Reason I ask: Got Money, yet enough to start some kind of serious business but - of course - not enough to buy a CNC-Laser made in Germany/US (that would be around 10k just for a used Machine, got like 8k for machine and material and I am an experienced web developer who does know how to setup an Onlineshop and promote it).



 So i guessed if one of these was able to the same Job, I

would be able to keep up with the "big" companys without the need to work for someone else as a cleaning lady..



Got my onw building too capable to - if necessary any time - set up up to 30 of that machines, so I don't have to waste money for rent or anything else.



Any feedback + suggestions are welcome. Thank you.



P.S. please don't destroy my dream.



P.P.S. if you <i>seriously</i> think that I can't do it: go ahead and destroy it!





**"Christoph E."**

---
---
**Alex Krause** *August 25, 2016 22:45*

X700 clone I have heard some decent things about them


---
**Christoph E.** *August 25, 2016 22:47*

We'll... just to make sure I'm not missunderstood: I'm currently working as Webdeveloper <i>and</i> cleaning Lady. Before that I've been working as Funeral Director which I quit just because I can't see dead children anymore. :-/



So I'm not a trained professional in CNC-technics...


---
**Alex Krause** *August 25, 2016 22:50*

Contact the seller and ask if it has Ruida control board


---
**Christoph E.** *August 25, 2016 22:52*

Wilco Alex, can you explain what is good about that board? :-)


---
**Alex Krause** *August 25, 2016 22:54*

It will let you know what software you should look up on YouTube for tutorials 


---
**Alex Krause** *August 25, 2016 22:54*

And product reviews


---
**Christoph E.** *August 25, 2016 22:58*

Thanks, I will ask for it. Already asked for a manual, but they just sent me one for the 50 Watt Version. In like the same english as the 40 Watt Version.

Pretty useless, but my k40 works like a charm... just doesn't do what I really need. :-)


---
**Ariel Yahni (UniKpty)** *August 25, 2016 23:05*

Are you allready making a business with the k40 and you knowledge of web design? Is the k40 a a bottleneck or your customers are asking for larger one piece parts that you cannot provide now? 


---
**Christoph E.** *August 25, 2016 23:10*

Hi Ariel, no I'm currently not making a business of engraving/cutting. I'm working as Web-Devoloper 8 hours a day and additionally 3-4 hours as cleaning lady... (yeah, life's expensive and hard... :-( )



Just figured that a Machine like that could be a game-changer.


---
**Ariel Yahni (UniKpty)** *August 25, 2016 23:13*

i feel you, life is really hard but we need to make the best of it. How do you plan to monetize an 8K investment? I don't want to crush your dream but help you realize the path.


---
**Ariel Yahni (UniKpty)** *August 25, 2016 23:16*

BTW i do have a great idea for an experienced web developer related to digital manufacturing without the need to buy a machine


---
**Ariel Yahni (UniKpty)** *August 25, 2016 23:16*

Send me a private message and ill share it with you




---
**Christoph E.** *August 25, 2016 23:20*

Well, I was analysing markets for personalized grave-stones/plates (what a surprise regarding my last jobs :D) especially smaller ones made in slate and granite.



Of course I would also provide house number plates and so on. But yeah... basically that's it. eBay, amazon, etsy and of course my own shop.

I figured that none of the available sellers has got a real "product configurator", yet I can code it...





Edit: mainly ment for animals, but in my last Job i've also seen these for humans. A lot of people sadly don't have the money for "real" thombstones. :-(


---
**Alex Krause** *August 26, 2016 01:15*

[Https://www.facebook.com/groups/1314938658533901/](Https://www.facebook.com/groups/1314938658533901/) you may ask to join this group on Facebook if you are wanting more info on the 2k$ laser before you invest


---
**Christoph E.** *August 26, 2016 23:21*

Joined it. Sadly the provided Info (what I could read so far) didn't really make my decision.

There is people who complain about this and that, but on the other hand you see that anywhere - even at the latest porsche 911 gt...



And yeah, standard language seems to be chinese... how hard can it be to translate or pay some chinese guy to switch the language? :D



Still need to find out more about the Software used (just had 20 Minutes to look at it), but I guess in the end I'll just buy one and try to chance it.


---
**Scott Howard** *October 18, 2016 02:39*

Did you get one yet, I'm looking to get one also


---
*Imported from [Google+](https://plus.google.com/100193166302371572888/posts/AfhKgiTBZJM) &mdash; content and formatting may not be reliable*
