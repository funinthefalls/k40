---
layout: post
title: "M2nano specs Searching for some information on the M2nano, I came across these specs;"
date: December 21, 2016 03:56
category: "Hardware and Laser settings"
author: "HalfNormal"
---
M2nano specs



Searching for some information on the M2nano, I came across these specs;

MAX:600mm/S Carving:7mm/S Cutting:0.5mm/S X-Axis Motor:0.33A/Phase Y-Axis    Motor: 0.44A/Phase



These numbers give a good reference for what is possible and better with a third party controller and software.



Info was found here;

[https://www.aliexpress.com/store/product/CO2-Laser-Rubber-Stamp-Engraving-Machine-K40-LIHUIYU-M-Mother-Main-Board-Control-System-M2-Nano/1518161_32648480580.html](https://www.aliexpress.com/store/product/CO2-Laser-Rubber-Stamp-Engraving-Machine-K40-LIHUIYU-M-Mother-Main-Board-Control-System-M2-Nano/1518161_32648480580.html)





**"HalfNormal"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2016 04:10*

Heh, that's $115. My mini upgrade is $99. Smoothie and LaserWeb don't need a USB key. That's funny. 


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 04:10*

I"d say make it work with what you have on hand or is the cheapest option.


---
**HalfNormal** *December 21, 2016 04:13*

**+Ray Kholodovsky** That is a great sales slogan for your products. Cheaper and "No key? No problem!" 


---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2016 04:14*

Lost your key? It's cheaper to replace the board. 


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 04:17*

You realize that it is easy just to make a copy of the drive and then copy the backup to another flash drive or on to a CD. Better yet  upload a copy of the files to the cloud.


---
**HalfNormal** *December 21, 2016 04:35*

**+Jonathan Davis** The USB key does not contain any files and cannot be copied. It is  an anti-pirate dongle for the LaserDrw software which is the only software that will work on the M2nano board.


---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2016 04:39*

Argh!


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 04:43*

Well doesn't mean someone can't crack it as people pirate paid software on a regular basis. So a special formated flash drive wouldn't be too difficult 


---
**Ashley M. Kirchner [Norym]** *December 21, 2016 04:49*

Hey **+Jonathan Davis**, when you figure out how to make a copy of the "drive", why don't you share that piece of information to everyone here who have owned one of these machines for far longer than you, and have been fighting with them, for far longer than you, and who are rather grateful for others, like **+Ray Kholodovsky**, who create alternatives so we don't have to use that "drive" you claim you can simply "copy". Please do, show us how. I'm just dying to know.


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 04:57*

Okay and I'm not saying that it couldn't be done, just that it was possible to do it since it is mass manufactured product. Also I happen to own a board created by Mr. **+Ray Kholodovsky**  and I think it's quite a versatile system that could with the right bit of tinkering be able too handle running multiple systems. 


---
**Kelly S** *December 21, 2016 05:02*

Seen one of these on ebay for 75 dollars minus the usb.   USA seller too. 


---
**Ashley M. Kirchner [Norym]** *December 21, 2016 05:09*

**+Jonathan Davis**, you've obviously never dealt with hardware encryption. Yep, they're mass produced. Good luck hacking them. As I said, go for it. Put your money where your mouth is. Let us know when you figure it out.


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 05:13*

All I'm saying is that it is possible not that it would actually be done. Since you do have the option to reprogram the board. 


---
**Ashley M. Kirchner [Norym]** *December 21, 2016 05:18*

Again, you have obviously never dealt with encryption hardware. They can not, ever, in no way, shape, or form, be reprogrammed. They are once set devices.



Do yourself a favor and go watch a movie on lions because this is an argument you will not ever win. I have a few decades over you in this field.


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 05:22*

**+Ashley M. Kirchner** Okay and I was never intending to be rude at all. But knowing the current state of technology I was only generalizing the fact that such a feat was possible. Since many online services have been hacked in recent years, like yahoo for example. 


---
**Ashley M. Kirchner [Norym]** *December 21, 2016 05:23*

Software versus hardware. You're not reading. May I recommend Lion King?


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 05:31*

**+Ashley M. Kirchner** No need for your disrespect please, but with all hardware at one point or another software is involved as the chips that run it would have to be programmed. In all known facts I do believe that something of that caliber could be indeed hacked or even a workaround produced. That would allow the controller board to work without the use of the dongle. I'm not the hacker to do it, but one does most likely exist who is probably already figuring that solution out. 


---
**Ashley M. Kirchner [Norym]** *December 21, 2016 05:50*

You are correct, the software that sets the first encryption key, is at the factory where the wafers are made and embedded. The software that sets the second encryption key, can be owned by anyone, it's free. Set the second key, the chip generates the final encryption using the first key and locks itself. There is no way to unlock it. Let me repeat that again: THERE IS NO WAY TO UNLOCK IT. Not even the factory can unlock the chip, they are throw-aways, freaking 88 cents for an AES encryption chip.



Once you learn how electronic circuits work, you'll understand that statement, it's a one way street. You can not, EVER, get to the primary encryption keys. You might be able to figure out the secondary ones, but you still can't unlock the full encryption string that you can then use to decode it.



This is how hardware encryption works. Do the research, teach yourself something.


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 05:57*

**+Ashley M. Kirchner** Still no need to be rude. As another option is to just simply remove the encryption chip all together and replace it with another that mimics the effect. Thus tricking the board to think that it is there, when it is not. 


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 06:01*

At this point in time you are being very much uncivilized. As I would like to try and have a respectable and considerate  conversation, opposed to how you are acting now. I am not a hardware or software engineer but a basic level hobbyists who creates different things by hand for the sake of just doing it for the experience. 


---
**Ashley M. Kirchner [Norym]** *December 21, 2016 06:02*

Again, you fail to understand the technology. Each chip has a different primary key. Replacing it isn't going to do any good. Care to try again ... at losing?


---
**Ashley M. Kirchner [Norym]** *December 21, 2016 06:03*

Seriously dude, all you are doing here tonight is proving to everyone your level of incompetence. Do yourself a favor and go do the research and learn about the technology before you attempt to have a conversation about it.


---
**Ashley M. Kirchner [Norym]** *December 21, 2016 06:05*

Feel free to stop reading anything I say. I'm not forcing you to follow along with my uncivilized comments. That's a choice you are making.


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 06:05*

No, but you are not being respectful or even considering the background as to where I come from. Instead your method of attacking my remarks you can better yet actually explain and teach others from the knowledge you have gained. 


---
**Ashley M. Kirchner [Norym]** *December 21, 2016 06:12*

Care to explain what background that is that you're coming from? All I've seen you do is telling others how they should do something. Making recommendations for things you have absolutely no idea how they work. Or telling me, tonight, 'you can just do this, or that' without ever bothering to even understand what it takes and how the technology works. You want to know what I call that background? Arrogance. And you have plenty of it.



Someone who wants to learn will ASK questions, they don't tell others how to do something they don't know nothing of.



More than once I told you tonight, go do the research so you would understand the technology. You are pretending to know about something you obviously know nothing of.



I'm done with your idiocy. As I said, you are under absolutely no obligation to read anything I say to you. That's a personal choice you are making. Feel free to ignore me all you want.


---
**Coherent** *December 21, 2016 12:35*

I'm usually smart enough to stay out of these types of discussions but...

A usb hardware key or "hasp key" can be duplicated or cloned. It depends on the exact type, but most can be cloned. Simply do a search for usb dongle clone or copy and you'll find lots of info. Like here: [dongleduplicate.nsys.by - Duplicate ( Hardware copy ) of Hasp or Hasp4 or HaspHL
or other dongle](http://dongleduplicate.nsys.by/)



I'm not saying these K40 dongles can be cloned, but considering the cost point etc, they most likely can be. Of course it may cost as much as a replacement unless you have the equipment and knowledge so the point may be moot anyway.


---
**Joe Alexander** *December 21, 2016 14:31*

Hey guys, how about we get back to the purpose of this group which is to ask questions and spread information in the hopes of assisting fellow owners. Yes many questions could be prevented by reading past the first 3 posts but let's keep it civilized..


---
**Don Kleinschnitz Jr.** *December 21, 2016 15:09*

**+HalfNormal** nice find, I will certainly add this to my research regarding the max limits of this machine.

Why do we think that the max speed is 600mm/s  yet carving and cutting is only 7 and .5 respectively? 

Is carving the same as engraving to them?



Here is what my K40 specs are from Amazon, interesting differences?



[https://www.amazon.com/gp/product/B00C9UZCOS/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00C9UZCOS/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)

<s>--------------------------------------------------</s>

Technical Parameters:

<s>--------------------------------------------------</s>

Interface to Computer: USB Port

Tube Trigger Volt: 20KV;Tube Operating Volt: 15KV

Current:0~22mA

Engraving Area: 260x180mm

Maximum Item Size to Engrave: 10.25W x 8.75L x 2.85H in (260mm x 220mm x 70mm)

Laser Tube (life hours): 1000-1300 Hours

Laser Power: 40W

Engraving Speed: 0-13.8 in./s (0-350mm/s)

Cutting Speed: 0-1.38 in./s (0-35mm/s)

Minimum Shaping Character: 0.04 X0.04in (1mm X 1mm)

Resolution Ratio: 0.001 in (0.026mm) / (1000dpi)

Resetting Positioning: 0.0004 in (0.01mm)

Software Supported: MoshiDRAW software (both NewlyDraw and NewSeal function)

Power Consumption: 250W

Operating Temperature: 32-113F (0-45C)

Recommended Spare Parts/Consumables: Laser Tube, Focal lens, Reflection lens

Voltage: 110V~240V

Frequecy: 50Hz~60Hz




---
**Jonathan Davis (Leo Lion)** *December 21, 2016 16:04*

**+Coherent** My point exactly, as it can be done but it does not mean that someone could do it with the right tools as you have mentioned.


---
**Stephane Buisson** *December 21, 2016 16:11*

It's pointless to clone a USB stick for those board, who want to use that software anyway.

The whole goal of Smoothie mod (when I started it) was to be able to choose freely your software. (visicut before LW1)


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 16:21*

**+Stephane Buisson** Proprietary software just to keep a user from using a competitors tools.  Epilogue lasers being a good example but from their videos it does seem like a good system to work with.

NOt to be rude or anything but can i suggest a possible option? The chinese program Benbox as it provides a few design tools that would be nice to have in Laserweb. Or developing a application that can be used on mobile device as a way to repurpose it.


---
**HalfNormal** *December 21, 2016 16:21*

**+Don Kleinschnitz**​ it appears the specs for your machine is for the Moshi board and not the Nano.


---
**Don Kleinschnitz Jr.** *December 21, 2016 16:34*

**+HalfNormal** interesting since my machine had a M2Nano :).


---
**Stephane Buisson** *December 21, 2016 16:36*

**+Jonathan Davis** you miss part of the point, it's not only the proprietary software, but the hardware is close too.

As per Smoothie compatible softwareS, you could suggest new features to devs for all of them directly in their respective Github repo.

but please be extremely efficient in your request description (as Devs are all extremely busy)


---
**Don Kleinschnitz Jr.** *December 21, 2016 16:39*

**+Stephane Buisson** yes that is an important point. For me it was the frustration of not knowing what the machine was being told to do when things did not work. With my K40-S I can see everything from the electronic circuits to the Gcode that is driving the gantry :).


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 16:39*

**+Stephane Buisson** Yeah and i did make a suggestion on the LAserweb community and Mr. Van Der Watt was quite rude in his responses towards my ideas.


---
**Stephane Buisson** *December 21, 2016 16:43*

**+Don Kleinschnitz** did you try Visicut ?


---
**Jonathan Davis (Leo Lion)** *December 21, 2016 16:44*

**+Stephane Buisson** I downloaded the program myself but have not yet installed it.


---
**Don Kleinschnitz Jr.** *December 21, 2016 17:51*

**+Stephane Buisson** no i did not try visicut. Do you mean with the Nano or smoothie??


---
**Marvin Pagaran** *December 21, 2016 17:51*

Is there any other software for thisbaside from laserdrw?


---
**Stephane Buisson** *December 21, 2016 21:13*

Smoothie work with LaserWeb, Visicut, , ... and other applications (for 3d printing, cnc,...) see [http://smoothieware.org/laser-cutter-guide#toc40](http://smoothieware.org/laser-cutter-guide#toc40)


{% include youtubePlayer.html id="lbTTPkDEhOg" %}
[https://www.youtube.com/watch?v=lbTTPkDEhOg](https://www.youtube.com/watch?v=lbTTPkDEhOg)


---
**Don Kleinschnitz Jr.** *December 22, 2016 00:57*

**+Stephane Buisson** thats what I though. My calendar is full with LaserWeb :).


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/2FfcVMdwmYs) &mdash; content and formatting may not be reliable*
