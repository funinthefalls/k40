---
layout: post
title: "Hello Friends, thx for attention! I have this problem on my K40"
date: February 27, 2018 00:59
category: "Original software and hardware issues"
author: "Fabiano Ramos"
---
Hello Friends, thx for attention!

 I have this problem on my K40. She came with me from the US and since 2014 I've never put her to work since. Today I had to use it, but Axis X has this problem. The video details exactly the problem:


{% include youtubePlayer.html id="HCwRKhoVGmo" %}
[https://www.youtube.com/watch?v=HCwRKhoVGmo](https://www.youtube.com/watch?v=HCwRKhoVGmo)



I pulled out the Belt of X axis to check the engine status, it stands still, humming.



I bought a new motherboard, and put it in place of the old, same problem. What could it be? The Power Supply voltages are normal ( 5.1v e 24.5v). 



I'm totally in the dark. I have no idea what it can be. Has anyone experienced this and knows how to solve it?









**"Fabiano Ramos"**

---
---
**Don Kleinschnitz Jr.** *February 27, 2018 01:47*

for some reason this post it not showing up in the community for me only the alerts and the video does not play?

**+Anthony Bolgar**


---
**HalfNormal** *February 27, 2018 01:54*

**+Don Kleinschnitz** I just checked and it is not in the community for me either. I can play the video.

**+Fabiano Cardoso Ramos** Where are you located? Are you on 110VAC or 220VAC? Just curious because you need to make sure the powersupply is set for the correct voltage.


---
**Fabiano Ramos** *February 27, 2018 02:08*

**+HalfNormal**  My Location is Brazil and my Voltage is 220v. 


---
**HalfNormal** *February 27, 2018 02:10*

If the unit came from the USA, the powersupply is set for 110VAC. You need to see if it can be set to 220VAC or purchase a powersupply that is 220VAC.


---
**Fabiano Ramos** *February 27, 2018 02:12*

**+Don Kleinschnitz**  I edited the post with link video.


---
**HalfNormal** *February 27, 2018 02:16*

You will need to look at the powersupply and see if there are any markings. Unfortunately if you applied 220VAC to the powersupply that is rated for 110VAC, you might have already done some damage.


---
**Fabiano Ramos** *February 27, 2018 02:16*

**+HalfNormal** I found an all in Chinese manual that shows that the base voltage is 220AC.


---
**HalfNormal** *February 27, 2018 02:16*

Voltage depends on the country of use.


---
**Fabiano Ramos** *February 27, 2018 02:26*

**+HalfNormal**  Confirmed 220vAC. Im Using in Correct Voltage.


---
**HalfNormal** *February 27, 2018 02:29*

Great news! There could be a crossed wire on the stepper motor. Have you tried swapping the axis (x and y) and see if the problem follows the wires or stays with the stepper?


---
**Fabiano Ramos** *February 27, 2018 02:33*

**+HalfNormal**  yeap. The  X Stepper in Y axis runs normal when changed the port conection.  The other motor ( Y stepper) shows the same problem when conected in X Axis.



The board is new, arrived today from china. Is a  K40 ms10105 v4.87 Main Board. 




---
**HalfNormal** *February 27, 2018 02:46*

I think then you have it narrowed down to the stepper motor wiring. I was trying to find a diagram for you but if you  search you will find one. What you need to do is swap the wires around. Stepper motors work in wired pairs.. Search for an explanation. It is easy and you cannot hurt the stepper if you wire it wrong.


---
**Fabiano Ramos** *February 27, 2018 18:35*

**+HalfNormal**  Done!  SOLUTION!

Through his guidance I discovered the problem. He's a lot stupid (and Cheap) than we can imagine.



Note the photo, in the circuit relative to the Y Limiter, one of the poles of X motor pinning was with shit solder. Making one of the X motor poles not receive proper power. So you got it right, in a way.



I researched the welds in the area and bingo! Motor in X-axis, moving normally!



Thank you all for your help and patience!

[https://plus.google.com/photos/...](https://profiles.google.com/photos/111567065741989535353/albums/6527304695524443409/6527304696970408050)


---
**HalfNormal** *February 27, 2018 18:44*

Bravo! It never crossed my mind to check the limit switches! Great work and persistence! In the future I will advise on checking limit switches too.


---
**HalfNormal** *February 27, 2018 18:45*

**+Don Kleinschnitz** did you catch this?


---
**Don Kleinschnitz Jr.** *February 27, 2018 18:52*

**+HalfNormal** nope missed it. Not surprised.


---
**Fabiano Ramos** *February 28, 2018 18:15*

Thx for all dudes!


---
*Imported from [Google+](https://plus.google.com/111567065741989535353/posts/a16wy1K32GP) &mdash; content and formatting may not be reliable*
