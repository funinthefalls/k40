---
layout: post
title: "When I power my machine up, it will not home out"
date: July 26, 2017 14:52
category: "Discussion"
author: "Bill Stockwell"
---
When I power my machine up, it will not home out. It is like it lost communication at that point, any ideas? Everything else seams to be in working order.





**"Bill Stockwell"**

---
---
**Don Kleinschnitz Jr.** *July 26, 2017 15:07*

More info on how it moves, or video.


---
**Bill Stockwell** *July 26, 2017 15:19*

It does not move, I hear a click and that is all


---
**Bill Stockwell** *July 26, 2017 15:20*

I can move it manually in both directions with belt and rod.


---
**Don Kleinschnitz Jr.** *July 26, 2017 17:55*

No motors move at all. 

Tell us more about your machines configuration pls.


---
**Bill Stockwell** *July 26, 2017 18:35*

I have had it a few years, and to this point have not really had any issues. It is like the motors are not firing. The machine is not frozen up but it will not home out nor will it move if I move with the computer


---
**Bill Stockwell** *July 26, 2017 18:36*

The limit switches seem to be working.


---
**Don Kleinschnitz Jr.** *July 26, 2017 18:47*

Need to know more about the configuration is this a stock machine?

Your low voltage section of your laser power supply may not be working. 

Can you fire the laser with the test button?

Is the LED (red or green) down on the LPS PDB lit.


---
**Bill Stockwell** *July 26, 2017 18:49*

The gantry tries to move when power up but does not


---
**Bill Stockwell** *July 26, 2017 18:55*

It is a stock machine, the laser fires when tested which board are you talking about? The one that the usb plugs into is red


---
**Don Kleinschnitz Jr.** *July 26, 2017 19:15*

**+Bill Stockwell** the laser power supply is in the right hand cabinet. 

Down on the motherboard of that supply is a led and a PB.

Is the led on when the machine is on?

Do you have a DVM and know how to measure DC voltage with it?

 


---
**Bill Stockwell** *July 26, 2017 21:03*

No there is no other led on inside that box


---
**HalfNormal** *July 26, 2017 23:38*

Sounds like you have a Moshi Board. If you do, you can find the information you need below. Even if you have not updated your software, this seems to fix most problems. I purchased a unit that had problems and this was what fixed it.

[dck40.blogspot.com - Fix for broken Mainboard MS10105 v4.x](http://dck40.blogspot.com/2013/03/fix-for-broken-mainboard-ms10105-v4x.html)


---
**Bill Stockwell** *July 26, 2017 23:45*

Thanks I will try that!


---
**Bill Stockwell** *July 27, 2017 12:39*

That did not work it tells me the file MoshLine.dll is missing although it is clearly in the folder.


---
**HalfNormal** *July 27, 2017 12:41*

I would try reinstalling the Moshidraw software then run the program. 


---
*Imported from [Google+](https://plus.google.com/109337776165740750775/posts/NyFSaCdWNER) &mdash; content and formatting may not be reliable*
