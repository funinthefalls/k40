---
layout: post
title: "I would like to hold material down so it doesn't shift on the lightobject z table that has a pin bed made of \"punk rock\" clothing studs in the holes, I was curious if anyone has some good ideas for holding material down on"
date: July 19, 2017 12:12
category: "Modification"
author: "Kevin Lease"
---
I would like to hold material down so it doesn't shift on the lightobject z table that has a pin bed made of "punk rock" clothing studs in the holes, I was curious if anyone has some good ideas for holding material down on this type of surface?





**"Kevin Lease"**

---
---
**Anthony Bolgar** *July 19, 2017 12:24*

Rare earth magnets?


---
**Kevin Lease** *July 20, 2017 02:17*

I think it has an aluminum bed, magnet does not stick


---
**Mark Brown** *July 20, 2017 22:06*

What are the punk rock studs made of?


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/MCYefP7ERN1) &mdash; content and formatting may not be reliable*
