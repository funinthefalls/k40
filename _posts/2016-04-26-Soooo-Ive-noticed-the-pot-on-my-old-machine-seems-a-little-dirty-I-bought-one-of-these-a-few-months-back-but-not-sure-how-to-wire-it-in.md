---
layout: post
title: "Soooo, I've noticed the pot on my old machine seems a little dirty, I bought one of these ( ) a few months back but not sure how to wire it in"
date: April 26, 2016 03:53
category: "Modification"
author: "I Laser"
---
Soooo, I've noticed the pot on my old machine seems a little dirty, I bought one of these ([http://bit.ly/1rvZYm1](http://bit.ly/1rvZYm1)) a few months back but not sure how to wire it in. Can someone please enlighten me? Thanks :)





**"I Laser"**

---
---
**Jean-Baptiste Passant** *April 26, 2016 07:24*

From what I see on the pictures (can"t access ebay at work), 1 should go to 5V, 3 to Gnd and 2 to IN

Once wired, turn the laser switch off and use a voltmeter between IN and GND, if when you turn clockwise voltage goes up you're good to go, if it goes down then Wire 3 to 5V and 1 to GND


---
**I Laser** *April 26, 2016 12:07*

For anyone else that may be interested on the stock k40 PSU wiring, pin1 white 5v, pin2 purple In, pin3 green Gnd.



So wish I did this mod months ago, setting power is so much more accurate and easier now. Thoroughly recommend anyone with a analog K40 to swap the pot out for one of these!


---
**Jean-Baptiste Passant** *April 26, 2016 13:33*

K40 have a lot of different PSU, and a lot of wiring color, yours will not be the same as mine for exemple ;)


---
**I Laser** *April 26, 2016 13:41*

Ah fair enough, thanks for your help by the way!


---
**Jean-Baptiste Passant** *April 26, 2016 16:50*

You're welcome ;)


---
**Anthony Bolgar** *April 26, 2016 20:16*

MY Redsail came with a 10 turn pot, so much easier to get the exact power you want.


---
**I Laser** *April 27, 2016 01:58*

Yeah it's crazy now thinking of the lines I had on the machine and multiple test fires to make sure the power was right. Now I just dial it in and it's good to go. :D


---
**Don Kleinschnitz Jr.** *April 27, 2016 13:31*

I fixed my setting problem by installing a digital voltmeter across the existing pot :).


---
**Anthony Bolgar** *April 27, 2016 13:37*

**+Don Kleinschnitz** That is a great idea, I assume you turn your pot until it reaches the voltage that correlates to the desired power setting?


---
**Don Kleinschnitz Jr.** *April 27, 2016 16:30*

Yes, the design just wiring in a meter from amazon (was posted somewhere here along with K40 schematics). I make test cuts on various materials and write on those test materials the digital setting, speed etc.  which I keep as a library item for reference.


---
**Don Kleinschnitz Jr.** *April 27, 2016 16:39*

**+Anthony Bolgar** I posted the digital meter hack since i cant add a picture here. Disappointing that you can't directly add from your Google photos to G+.


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/NxwowFj6Azm) &mdash; content and formatting may not be reliable*
