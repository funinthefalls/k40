---
layout: post
title: "I have issue with my second hand bought k40"
date: November 12, 2018 20:49
category: "Discussion"
author: "Yorkshire Fox"
---
I have issue with my second hand bought k40. Im hoping it can be fix but im not holding on to any hope. 



So i have serious power loss. Ive taken a look at my laser tube and i defo think there is some arcing happening. I did have some serious arcing issues but i realised i wasnt using the correct water. 



Hope someone can help or advise.



[https://www.dropbox.com/s/68lm9zxi1pshpo1/20181112_202531.mp4?dl=0](https://www.dropbox.com/s/68lm9zxi1pshpo1/20181112_202531.mp4?dl=0)





**"Yorkshire Fox"**

---
---
**Don Kleinschnitz Jr.** *November 12, 2018 21:45*

Do not have permission to view the video you posted.



Where is the arching?



If you had conductive coolant the tube should be ok but your LPS  likely has a bad HVT.



Have you replaced the coolant and with what?




---
**Yorkshire Fox** *November 12, 2018 21:47*

Should be able to view it now. 



I have replaced the coolant with deionised water


---
**Don Kleinschnitz Jr.** *November 12, 2018 22:25*

Says video is being processed... I will check back. 


---
**Adrian Godwin** *November 12, 2018 22:32*

Does the name 'Yorkshire fox' refer to your location ? If so, I would recommend you contact the Sheffield Hackspace - there are some people there with good experience with laser cutters and high voltage electronics.   If you're not located in Yorkshire, England, we'll do the best we can to help you here!




---
**Yorkshire Fox** *November 12, 2018 22:52*

Cheers. The beam is hitting the metal ring at the top of the tube. 


---
**Don Kleinschnitz Jr.** *November 12, 2018 23:08*

**+Yorkshire Fox** I downloaded the video. It certainly appears that you have good High Voltage. 

It looks like there is a HV leak into the water jacket. I have seen this happen with others use of conductive coolant.

What coolant were you using?

With a voltmeter check the water in the bucket to see if there is a  potential to ground?  Setup the probes in the water with the machine NOT running and do not touch the water so you do not get a shock.



If this diagnosis is correct its possible that the tube is damaged by a HV track formed into the water jacket :( :(. One reason we have been trying to get folks to believe that coolant other than distilled water is damaging to a K40.


---
**Yorkshire Fox** *November 12, 2018 23:17*

I'm from Sheffield and work in Barnsley but live in Notts.  I think I need a new tube to be honest but if I need to I'll get in touch with them. 






---
**Yorkshire Fox** *November 12, 2018 23:19*

**+Don Kleinschnitz Jr.** Thanks Don. Think I made a newbie mistake with doing that. I've drained the tube and I'm going to let it dry in the hope it will remove the track. 


---
**Yorkshire Fox** *November 14, 2018 19:51*

**+Don Kleinschnitz Jr.** distilled water or deironised?


---
**Don Kleinschnitz Jr.** *November 15, 2018 15:24*

I tested with distilled but we believe that de-ionized is ok. 


---
**Yorkshire Fox** *November 15, 2018 15:29*

**+Don Kleinschnitz Jr.** Any suggestions for coolant.


---
**Don Kleinschnitz Jr.** *November 15, 2018 15:53*

**+Yorkshire Fox** Testing showed that distilled water and algaecide was the safest.



[donsthings.blogspot.com - K40 Coolant](https://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
*Imported from [Google+](https://plus.google.com/100999477299382644053/posts/eS2Rd7x9vnn) &mdash; content and formatting may not be reliable*
