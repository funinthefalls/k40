---
layout: post
title: "+Matt S - To adjust your tube height, remove some of the rubber spacer, do not tighten it down trying to compress the rubber"
date: January 29, 2017 02:18
category: "Discussion"
author: "Anthony Bolgar"
---
+Matt S - To adjust your tube height, remove some of the rubber spacer, do not tighten it down trying to compress the rubber. 





**"Anthony Bolgar"**

---
---
**matt s** *January 29, 2017 03:12*

Do I remove the clamp and shave down some of the rubber?


---
**Anthony Bolgar** *January 29, 2017 03:32*

Loosen the clamp off. The rubber under the tube is usually 2 or 3 layers, try removing a layer and then retighten the clamp. You may need to remove all the rubber and then insert a cushioning material that is thinner than the rubber was. Hope this makes sense to you


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/Y68PT7Jg9Qq) &mdash; content and formatting may not be reliable*
