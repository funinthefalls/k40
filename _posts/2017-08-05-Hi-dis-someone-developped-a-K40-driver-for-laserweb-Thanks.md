---
layout: post
title: "Hi, dis someone developped a K40 driver for laserweb? Thanks"
date: August 05, 2017 16:21
category: "Software"
author: "Fabien Taylor"
---
Hi, dis someone developped a K40 driver for laserweb? Thanks





**"Fabien Taylor"**

---
---
**Jack Colletta** *August 05, 2017 16:44*

Laserweb connects to the controller board of the laser.  What board/firmware are you using in your K40?   Check out [cncpro.co](http://cncpro.co) for complete connection instructions. 


---
**Mark Brown** *August 07, 2017 03:17*

"K40 Whisperer" is the only alternative software that's able to communicate with the standard k40 control board. 


---
**Bob Buechler** *August 09, 2017 23:37*

check out Cohesion3d Mini as a drop-in controller replacement. Super simple, no weird cabling changes. Just follow the installation instructions and you're off to the races with LW4: [cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)




---
*Imported from [Google+](https://plus.google.com/108052364627466740668/posts/8XWAe2X8G3N) &mdash; content and formatting may not be reliable*
