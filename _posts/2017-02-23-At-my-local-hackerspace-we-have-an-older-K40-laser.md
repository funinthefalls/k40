---
layout: post
title: "At my local hackerspace, we have an older K40 laser"
date: February 23, 2017 08:41
category: "Hardware and Laser settings"
author: "Noel Bundy"
---
At my local hackerspace, we have an older K40 laser. The electronics have been replaced with an AWC608, and the 18mm Air Assist head, both from LightObjects. LaserCad, of course, is the software we're having to use, and it's not the greatest.



The laser tube, mirrors, and lens have all been replaced, and recently one of our members figured out the bed was too high, and the focal point of the laser was wrong. It's been re-aligned several times, as well. This improved things, but the K40 is still not performing anywhere close to what I see in this group. 



Example: I cut some 3mm cedar planking today. Did some etching, and cut out some circles. At 75mm/s, at 80% power, it took 3 passes. 



I then attempted to cut some 3mm acrylic. It took 6+ passes, starting at 75 / 80% and going all the way to 8 / 95%. And it still didn't cut all the way through. We drastically lose power at the front right corner of the bed. 



What can we do to fix this? From what I'm seeing in the group, I should be able to cut 3mm plywood at far lower power in a single pass, and acrylic should be even easier? 





**"Noel Bundy"**

---
---
**Stephane Buisson** *February 23, 2017 09:06*

when moving, the unit could have twist.

1) it's important everything is square. it's not the laser setting (mirrors) to correct frame/bed twist. position of the unit on the table resting straight flat.

2) laser alignement should be one the same plane (no vertical compensation) tube mirror1 mirror2

3) the working bed should be parallel to the plane and at focus height



Power issue:

4) clean lens and 3 mirrors (also check tube output)

5) check if head's lens is well orientated. 





 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 23, 2017 10:21*

75mm/s may be the reason you're having to do multiple passes on the 3mm wood. On my end, cutting 3mm ply takes about 10mA power (~40% of full potential) @ 10mm/s @ 3 passes. Albeit, my settings may be off a bit & could probably do with a realignment sometime in the near future. But 75mm/s definitely seems fast (in my opinion) to be able to cut anything in one pass.


---
**Don Kleinschnitz Jr.** *February 23, 2017 12:15*

In addition to the above suggestions; I recently worked with a user whose objective lens was installed incorrectly in the LO air assist head was crooked and loose. The lens mounts inside the threaded ring and may need a spacer ring to hold it in place.



Its not unusual especially if your laser is not parallel to the gantry for the beam going through the LO head to hit the inside of the small exit hole. Take it apart and check lens fit, stability and look closely for scoring from the laser on the side walls.



You can quickly check for this condition by tape-ing a target to the bed, firing and lowering the bed. If the spot moves position it may be that the beam is not exiting the last mirror perpendicular to the bed. BTW it assumes that when you lower the bed, the bed itself is not moving out of position.


---
**Noel Bundy** *February 23, 2017 15:13*

The bed in our K40 is a fixed one - to change the Z it involved making new spacers. An adjustable Z is on the list of things I'd like to add. I'll take a look at all the things suggested, and see if that helps. 


---
**Don Kleinschnitz Jr.** *February 23, 2017 15:19*

**+Noel Bundy** another way to check to insure the beam is not interposed with the body of the LO assist cowling is to shoot a target, with and without the cowling installed and look for differences.

Indications might be:

...More than one spot (reflection)

...Change in size or shape

...Reduction in apparent power


---
**Cesar Tolentino** *February 23, 2017 15:47*

To the OP. I believe the speed is too fast. 5mm ply on mine is...  8ma, 2 passes at 300mm/min. Like I mentioned before on this group, I believe the key is to find what is the slowest speed you can cut without flare ups, and then work from there and up. And the first pass always needs to be slower than the second pass, and not the other way around. 


---
**K** *February 23, 2017 16:20*

I'd agree that you can slow down. In the best circumstances I can cut 3mm birch ply in a single pass, 11mm/s @ 7ma (33ish%)


---
**Noel Bundy** *February 23, 2017 16:37*

The 75/80% thing was because someone had written it down, and I was testing. Usually for cuts I do 8mm at 95%, and it still takes 2 or more passes. 😕 I think I need to dedicate an evening to working with the cutter, just to see if I can get it tuned better. Doesn't help that lasercad at times will take 45+ minutes to upload a file. Probably a network thing. 



I really want to switch the printer over to using laserweb, but that's after I get the rest of it working properly


---
**Joe Alexander** *February 26, 2017 18:41*

I am able to cut 3mm acrylic on mine doing 6-8mm/s@10ma and cuts in 2 passes. 


---
**Noel Bundy** *February 26, 2017 19:07*

Well, it's a moot point now. Someone went into the space to fix the laser, and managed to let the co2 out of the tube. 😠


---
**Don Kleinschnitz Jr.** *February 26, 2017 21:13*

HUH? Its not like there is a valve on it lol. Did they crack it?




---
**Noel Bundy** *February 26, 2017 22:00*

Yea. So now they are ordering a new laser tube. At this point, I'm probably just going to buy my own cutter. 


---
**Don Kleinschnitz Jr.** *February 26, 2017 23:01*

**+Noel Bundy** wow that's ugly!


---
**Noel Bundy** *February 26, 2017 23:07*

The person who broke it took responsibility, and they did have good intentions, I'm just a bit tired of working with something that could be so much better. So I'll get my own K40, and start modding it as I go. I have an ArduinoCNC board kicking around, so that'll get me started with a GRBL-based system. And as soon as I can afford it, I'll get the Cohesion3D board. The nice thing is that I have 3D printers, so I can easily fab parts I need. :P I just gotta sort out funding - it's a bit expensive being in Canada, and converting to Metric Dollars makes things a lot more pricy.


---
*Imported from [Google+](https://plus.google.com/+NoelBundy/posts/Cr68f5PwejH) &mdash; content and formatting may not be reliable*
