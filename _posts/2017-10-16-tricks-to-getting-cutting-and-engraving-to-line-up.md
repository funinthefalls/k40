---
layout: post
title: "tricks to getting cutting and engraving to line up?"
date: October 16, 2017 05:15
category: "Discussion"
author: "waco kid"
---
tricks to getting cutting and engraving to line up? 





**"waco kid"**

---
---
**Paul de Groot** *October 16, 2017 05:53*

I use Inkscape and use two layers for engraving and cutting. Both line up in inkscape and i home the machine for cycle. This works well for me. Hopes this helps


---
**Ned Hill** *October 16, 2017 14:27*

**+waco kid** depends on what software you are using.  If you are using the stock software you have to created an origin reference.  Basically a single pixel or small box (say 0.25mm x 0.25mm with no line color) placed at the 0,0 origin.  Then select this origin reference with what you want to engrave and engrave.  Repeat for cutting.  It's a quirk of the stock software. 


---
**Ned Hill** *October 16, 2017 19:04*

Just select the box and what you want to engrave and engrave.  Repeat for cutting by selecting the box and what you want to cut and cut.  The program takes things and moves them toward the origin regardless of where they are on the page.  By having a reference at the origin already the other things you select stay where they are in reference to the box.


---
**Ned Hill** *October 16, 2017 19:05*

Rename your box "Origin" and it's easy to find in the object manager. 


---
**waco kid** *October 16, 2017 20:03*

that works, thank you!


---
*Imported from [Google+](https://plus.google.com/105465271425419072597/posts/CmGDe1tpGYr) &mdash; content and formatting may not be reliable*
