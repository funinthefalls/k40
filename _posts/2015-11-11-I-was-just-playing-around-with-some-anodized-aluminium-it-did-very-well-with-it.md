---
layout: post
title: "I was just playing around with some anodized aluminium, it did very well with it"
date: November 11, 2015 01:38
category: "Materials and settings"
author: "Scott Thorne"
---
I was just playing around with some anodized aluminium, it did very well with it.

![images/2701323e024d0ba6d620d87ec850b494.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2701323e024d0ba6d620d87ec850b494.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 11, 2015 06:49*

That's interesting. Were the cut lines along the lighter section?


---
**Scott Thorne** *November 11, 2015 11:56*

No it was even all the way across....that's at a speed of around 5 and 10 power.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 11, 2015 15:14*

Sorry, I think my original question was phrased wrong. What I meant was are the dark sections the engraved/cut area? Or the lighter section is what was engraved/cut?


---
**Scott Thorne** *November 11, 2015 15:16*

The light section was what I cut....it's deeper than it looks.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 11, 2015 16:40*

**+Scott Thorne** Looks very neat & tidy, I like the effect. Thanks for sharing Scott.


---
**Scott Thorne** *November 11, 2015 22:22*

Anytime Yuusuf.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/Q4MwVH42SCX) &mdash; content and formatting may not be reliable*
