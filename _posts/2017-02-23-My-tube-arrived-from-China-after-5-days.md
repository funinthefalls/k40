---
layout: post
title: "My tube arrived from China after 5 days!"
date: February 23, 2017 07:37
category: "Original software and hardware issues"
author: "Paul de Groot"
---
My tube arrived from China after 5 days! It's a tube from reci-laser, Shenghui factory shipped AUD $285. Nicely packed with care and not broken, pffff. Installed it and now have to wait for the silicone to set. Not taking any shortcuts here😰



![images/b33dd2136f3620dedf1842d2e1d309f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b33dd2136f3620dedf1842d2e1d309f8.jpeg)
![images/73ba4fb2b40e8c461db8e283c0c1d334.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/73ba4fb2b40e8c461db8e283c0c1d334.jpeg)

**"Paul de Groot"**

---
---
**Phillip Conroy** *February 23, 2017 09:17*

When I changed my tube the new tube was sitting to low one end,the packing around the tube compresses over time 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 23, 2017 10:19*

I'm surprised it arrives in one piece knowing how well AusPost likes to handle stuff. Did it get delivered via AusPost once it hit AU or someone else delivered?


---
**Paul de Groot** *February 23, 2017 10:26*

**+Yuusuf Sallahuddin** it was via  TNT with tracking. Paid $35 bucks for it and well spend money.


---
**Don Kleinschnitz Jr.** *February 23, 2017 12:05*

**+Paul de Groot** been watching RECI as a potential vendor when my tube goes, they seem to have good quality, but that is only hearsay.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 23, 2017 12:12*

**+Paul de Groot** Yeah, I think 10% of the item cost for decent shipping is well & truly worth it to ensure it arrives in 1 piece. Looks like you had a good run of it so far. Hope all goes well on the install end of it.


---
**Paul de Groot** *February 23, 2017 19:54*

**+Yuusuf Sallahuddin** i had a night for the silicone to set so after work I give it a go.😅


---
**Maker Cut** *February 23, 2017 22:33*

**+Don Kleinschnitz** I have a RECI tube (100W) and it works very well. My tube will actually produce more than 100W, but 100W is the nominal value at 26mA as measured. They also included a power rating chart per mA on the tube. Which my testing has shown to be accurate.


---
**Paul de Groot** *February 24, 2017 12:27*

**+Maker Cut** my new laser tube works really great. Laser cutting is working already at 3mA before i needed 8mA for leather. Will post my laser cut wallet results tomorrow.


---
**Paul de Groot** *February 24, 2017 12:28*

**+Don Kleinschnitz** so far the results are really promising with the new tube. Will post my results soon.


---
**Eliot uriales** *February 28, 2017 21:52*

Nice


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/Wo7gQVjEifJ) &mdash; content and formatting may not be reliable*
