---
layout: post
title: "Finally got around to installing my temp controller and thermocouple....it turned out great"
date: February 06, 2016 15:39
category: "Discussion"
author: "Scott Thorne"
---
Finally got around to installing my temp controller and thermocouple....it turned out great.

![images/630c9ba6882dab3d6967c9ca25001ae4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/630c9ba6882dab3d6967c9ca25001ae4.jpeg)



**"Scott Thorne"**

---
---
**ED Carty** *February 14, 2016 00:31*

I love your wooden box Scott. That's an awesome looking setup


---
**Scott Thorne** *February 14, 2016 00:36*

**+ED Carty**​...thanks man...really easy to design using inkscape.


---
**ED Carty** *February 14, 2016 01:02*

I tried inkscape along time ago.. negative results. But maybe I should try looking into it again


---
**Scott Thorne** *February 14, 2016 01:03*

Try it again but go to YouTube and watch the how to videos....I learned a lot from them.


---
**ED Carty** *February 14, 2016 01:05*

Right on. 

I will look into it. Right now im busy trying to close out some loose ends on my second job. Once i get that finished I will have some time. You make awesome projects bro. Im happy to see you hard at it.


---
**ED Carty** *February 14, 2016 01:08*

Just out of curiosity what is the thickest plexi glass you can cut cleanly with a 40 watt laser? can you do 1/4 or 1/2' ??

 


---
**Scott Thorne** *February 14, 2016 12:40*

I don't have the 40 watt anymore...I bought the 50 watt but I think it's 1/4 or 3/8 ....I do know if you get the 3 inch focal lens you can cut thicker material with the same power.


---
**ED Carty** *February 14, 2016 13:33*

Cool. Thanks


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/Du4ah14265w) &mdash; content and formatting may not be reliable*
