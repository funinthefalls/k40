---
layout: post
title: "Who is using the MKS SBASE/Smoothie board on their lasers?"
date: May 10, 2016 22:28
category: "Hardware and Laser settings"
author: "Custom Creations"
---
Who is using the MKS SBASE/Smoothie board on their lasers?





**"Custom Creations"**

---
---
**Anthony Bolgar** *May 10, 2016 22:46*

I am in the process of doing the upgrade.


---
**Ariel Yahni (UniKpty)** *May 10, 2016 22:59*

**+Anthony Bolgar**​ which board? 


---
**Anthony Bolgar** *May 10, 2016 23:05*

MKS Sbase


---
**Alex Hodge** *May 10, 2016 23:28*

I'm also in the process of the upgrade. I'm using a middleman board to breakout the stock ribbon cable.


---
**Anthony Bolgar** *May 11, 2016 01:15*

I am upgrading my Redsail LE400 with the MKS board, so most of the changes I am making will not apply to the K40, but it is the next one I am going to upgrade (It is running an arduino/ramps setup using Turnkey Tyranny's Marlin/Inkscape plugin.)


---
**Damian Trejtowicz** *May 11, 2016 06:25*

Im waiting for my mks sbase 1.2 as well


---
**Dennis Fuente** *May 11, 2016 17:00*

I'd be interested in how your MKS board upgrade works as i am thinking of putting one in my machine


---
**Custom Creations** *May 11, 2016 17:09*

I am trying to get someone to trade me a MKS SBASE for my new 7" touchscreen/raspberry pi setup.. Until then, my laser is out of commission. My ramps board is acting up so I can't use it..


---
**David Richards (djrm)** *May 11, 2016 17:54*

I've installed an mks sbase v1.2 in my home made rostock 3d printer, a simpler board (azsmz mini + middleman + ethernet ) will be driving my k40 laser. Ive had no problems with either. I also bought the lcd touch control, but i'm using the graphic mono lcd as it is better integrated into the boards firmware. Hth David.﻿


---
**Dennis Fuente** *May 11, 2016 20:18*

was there any problem installing firmware i use repetier in my 3d printer i was i could just flash the firmware on my K40


---
**Ariel Yahni (UniKpty)** *May 11, 2016 20:22*

**+Dennis Fuente**​ firmware flash on smoothie is just place a file on the SD and it will automatically upgrade. Also config it's pretty easy, just change text file and reboot. It's by far the simplest equation out there


---
**Alex Hodge** *May 11, 2016 21:13*

One concern I have with the upgrade is that I cannot find the specs for my stepper motors anywhere online. 


---
**Dennis Fuente** *May 11, 2016 23:16*

Ariel  yes i see that but uberclock dose not have any only boards they have are the 4 axis way over whats needed for a 2 axis machine MKS Sbase boards are 4 & 5 axis and less then half price of a 3 axis smoothie board.



Alex I think the stepers are nema 17


---
**Alex Hodge** *May 11, 2016 23:24*

**+Dennis Fuente** They are NEMA 17 yes, the issue is that there are tons of different NEMA 17 motors out there and they all have different power ratings for instance. This is important to know when configuring smoothie. You have to know what your max current allowed is for instance.




---
**Ariel Yahni (UniKpty)** *May 11, 2016 23:24*

**+Dennis Fuente**​ so mks does not follow the smoothie standard? My azsmz board does


---
**Ariel Yahni (UniKpty)** *May 11, 2016 23:26*

**+Alex Hodge**​ is correct. For integrated drivers boards you I'm out that on the config.txt file,  for external you need to measure it with a voltmeter. Either way you need the power settings


---
**Alex Hodge** *May 11, 2016 23:26*

**+Dennis Fuente** Also Dennis, what Ariel is referring to also applies to the MKS SBase board. It's smoothie based. You could always use a RAMPS board instead of a smoothie type board. That way you could use Repetier if you'd prefer.


---
**Damian Trejtowicz** *May 13, 2016 06:46*

Im so angry,i should listen people when they said,get genuine smoothie

My mks sbase looks like is broken,

When i connected to computer it was showing com port for second.after that it showing usb device not recognized,for me board is faulty :((


---
**Alex Hodge** *May 13, 2016 15:15*

**+Damian Trejtowicz** Nah, its probably not faulty. I had issues connecting mine via USB initially too. You might need to install a driver first. Depends on what version of Windows you're using. Also, you probably want to use ethernet rather than USB anyway.


---
**Ariel Yahni (UniKpty)** *May 13, 2016 15:22*

Also note that smoothie has a feature where you can get 2 usb devices recognized, one for serial com and the other to access the SD as mass storage 


---
**Damian Trejtowicz** *May 13, 2016 15:43*

In my case it recognize as unrecognize device,i cant change drivers at all.

When i connect board to my router i can get acces thru web,but in place where i will use there is no network thru cable only wifi

What i need to change to use as two devices,i mean com and sd


---
**Ariel Yahni (UniKpty)** *May 13, 2016 16:01*

Look for the following on the config file>

>second_usb_serial_enable	false	This enables a second serial port over the USB connection ( for example to have both Pronterface and a terminal connected)

>msd_disable	false	Disable the MSD ( SD Card access over USB ) when set to true ( requires a special binary, which you can find here, will be ignored without the special binary)

Also make sure you safely unmount the usb everytime


---
**Damian Trejtowicz** *May 13, 2016 16:12*

Specual binary?mean other firmware?sorry for my noob questions but its my first adventure with smoothieware


---
**Ariel Yahni (UniKpty)** *May 13, 2016 16:14*

Well that one you should not touch. Enable the other


---
**Damian Trejtowicz** *May 13, 2016 16:47*

Now im complely dumb...looks like i spend whole night trying sort it at all this


---
**Damian Trejtowicz** *May 14, 2016 13:41*

I dont know what i done,few changes in config file and my board alive,i have two com ports now

Im sorting out now how to connect pwm and liser fire thru level shifter


---
**Dennis Fuente** *May 14, 2016 16:54*

OK so whats the difference between the mks sbase board and a smoothie board both the same thing


---
**Alex Hodge** *May 15, 2016 02:06*

**+Dennis Fuente** MKS SBASE is a 3rd party board that uses Smoothie firmware. Different physical layout and components as well as obviously its made by different people. I think there are a few boards out there that use Smoothie. The MKS SBASE is from a company that calls itself Maker Base out of China.


---
**Dennis Fuente** *May 15, 2016 02:22*

I was also wondering about the DSP controller that LO sells they are on the expensive side but ti looks like you get a complete system


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/WiuZsRSEP3Y) &mdash; content and formatting may not be reliable*
