---
layout: post
title: "As some may know my power supply failed and now I'm waiting on a refund from the seller"
date: April 10, 2017 18:35
category: "Discussion"
author: "Jeff Johnson"
---
As some may know my power supply failed and now I'm waiting on a refund from the seller. The laser cutter I purchased boasted a 2 year warranty but as my experience has shown, the warranty lasts only as long as the eBay return period. They ignored me for nearly a month until I contacted PayPal. As another point of disappointment, PayPal also ignored me until I called them. 



PayPal informed me that they had been communicating with the seller and I should expect to hear from them soon. A few days later I did and they offered to send me a new power supply or cover the cost of a replacement. Since I don't trust them I found a power supply and asked them to cover the cost of $80. They replied the same way I've read from other people in my position, that their profit was too low to refund that much and if I'd take $50. I replied that their profit margin was no concern to me but I searched around and was able to find a replacement for $65. They agreed to this but said I had to close the PayPal case first so that they can access the funds.



I decided to contact PayPal before closing the case and was told that it's likely that the seller is trying to avoid refunding the money because they are able to refund without closing the case. When I mentioned this to the seller, they stopped communicating with me. PayPal says they will handle it for me but I still don't have the refund. This has been very frustrating.





**"Jeff Johnson"**

---
---
**Nate Caine** *April 10, 2017 21:00*

Jeff, I want you to know I feel your pain.  I hope this resolves well in your favor.  



You did well to NOT close the case.  I think the scam there is that they beg you to do that, and then you've passed the time limit to re-file.  



I've also heard those phrases:   "our profit margin is so low" and "please consider our point of view".    



Here's my horror story.  This was on a 100W machine:



When a laser tube failed prematurely, we contacted the company and were told the "1 year warranty" applied only to the machine, not the tube.  The tube, we were informed, had only a 3-month warranty.



We then pointed out that the tube failed in under 3-months.  The company then told us they start the warranty on the day the machine <b>ships</b> from China.  Well, because of customs problems, the machine <b>sat in customs for 4-months</b>.  And the company knew that, since <b>they</b> were doing the customs paperwork.  



In summary, the tube's "3-month warranty" had been expired for over one month on the day we received the machine.



Oh, and the reason the100W tube failed prematurely was because as shipped from the factory, they calibrated the HVPS to drive a 130W tube.  So the poor tube was always being drive 30% higher than indicated.  The company admitted this.  Their only offer was to <b>sell</b> us a new tube "at cost".  Turns out, you could buy the exact same tube on eBay for the same price.  




---
**Abe Fouhy** *April 10, 2017 21:27*

Dang that is shiesty!


---
**Jeff Johnson** *April 10, 2017 21:31*

Well that really sucks. I'm glad I'm only dealing with a $65 part.


---
**HalfNormal** *April 11, 2017 12:56*

My nano went south a few months into use. Same run around. They even claimed that they shipped it but it was returned China post back to them. I ended up purchasing one for $65 and went on my Merry way. I never did get a refund or the one they promised they had shipped. I have an intermittently squealing power supply so I've also purchased a spare but never had to use it.


---
**Jeff Johnson** *April 11, 2017 16:57*

They keep replying that I should be patient and they are doing their best to help me. So I told PayPal that I'd also like to be reimbursed for the failed fan and water pump. They obviously have no intention of voluntarily honoring the warranty.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/DKgqXy4c4A8) &mdash; content and formatting may not be reliable*
