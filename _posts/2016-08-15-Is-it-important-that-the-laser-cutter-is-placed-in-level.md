---
layout: post
title: "Is it important that the laser cutter is placed in level?"
date: August 15, 2016 19:30
category: "Hardware and Laser settings"
author: "Mikkel Steffensen"
---
Is it important that the laser cutter is placed in level?





**"Mikkel Steffensen"**

---
---
**Thor Johnson** *August 15, 2016 19:43*

Nope... just make sure that the laser has no air in it.  I suppose if it's mounted vertically, it could be problematic (releasing the motor causes the carriage to bump and end stop)...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 15, 2016 19:49*

To some degree having the tube out of level will make it difficult to get precise alignment though, so if you can have it level easily it wouldn't hurt anything.


---
**Eric Flynn** *August 15, 2016 19:53*

Yea, just level it.  It will be better overall to have it level.


---
**Mikkel Steffensen** *August 20, 2016 18:07*

Thank you. I will level it.


---
*Imported from [Google+](https://plus.google.com/117825343986845004843/posts/bXgkGgs3Ugr) &mdash; content and formatting may not be reliable*
