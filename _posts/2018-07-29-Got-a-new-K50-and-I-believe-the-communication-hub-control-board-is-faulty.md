---
layout: post
title: "Got a new K50 and I believe the communication hub/control board is faulty"
date: July 29, 2018 20:12
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Got a new K50 and I believe the communication hub/control board is faulty. I'm not able to communicate with my windows 7  laptop using RDWorks or LightBurn. I've tried ethernet, crossover and male-male usb connection with no luck. It won't even locate the files on udisk when saved and then plugged into the machine.



My question is what options do I have now?



Would getting a new controller solve this issue? Or would I need to change something else? What are possible solutions?



Of course I've messaged the Ebay seller but I'm not holding my breath for that. Any and all ideas would be welcomed.





**"Nathan Thomas"**

---
---
**HalfNormal** *July 29, 2018 22:06*

The Ruidia which is DSP based is the best for laser control. That said, you could modify it to use the cohesion3D board if you are unable to get the seller to make good on your problem. It will take some electronic and mechanical know how to make the swap.


---
**Nathan Thomas** *July 29, 2018 23:18*

UPDATE: Ok after 12 hrs of working on this issue, I have it figured out. Posting in case someone else comes across this issue 



It's definitely the ports are faulty. What I did was bypass the ports that are on the outside of the machine, and plugged directly into the control board. The ethernet port on the controller still didn't work, but the usb port on the control board did. So I was able to have my computer and laser  communicate


---
**Nathan Thomas** *July 29, 2018 23:20*

Via LightBurn. So if you come across the problem on your k50 (especially new owners) where your laser isnt connecting, check the ports and bypass them if possible to connect directly to the control board. If red LED 14 is blinking that means your control board is working so you shouldn't have a problem.


---
**HalfNormal** *July 30, 2018 00:50*

Great news!


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/PCXw8e3ka34) &mdash; content and formatting may not be reliable*
