---
layout: post
title: "Could someone help me with a box design"
date: July 14, 2016 03:25
category: "Discussion"
author: "3D Laser"
---
Could someone help me with a box design. I need it to have a removable lid and two compartments





**"3D Laser"**

---
---
**Phillip Conroy** *July 14, 2016 08:53*

I use online make a box link  [http://makeabox.io/](http://makeabox.io/)  and  [http://www.makercase.com/](http://www.makercase.com/)  there are a couple more


---
**Ned Hill** *July 14, 2016 12:18*

**+Phillip Conroy** Question, how do you handle the PDF files that these sites generated.  I can't figure out how to get them into coreldraw.

 


---
**3D Laser** *July 14, 2016 12:18*

**+Ned Hill**  and how do you edit them to make more slots 


---
**Ned Hill** *July 14, 2016 12:24*

**+Corey Budwine** Lol you're asking me?  I'm still trying to figure out how to get them into Corel. ;)


---
**3D Laser** *July 14, 2016 12:26*

Sorry I just hit reply not thinking haha


---
**HalfNormal** *July 15, 2016 02:40*

**+Corey Budwine** **+Ned Hill** To get a PDF into CorelDraw, use the import function. You will then be able to edit them. I do that all the time with the box maker apps. If I do not want tabs at one end, I just delete them and draw a new line. Also you will need to "select all" and make the lines thinner or else it will double cut.


---
**Ned Hill** *July 15, 2016 02:48*

**+HalfNormal** Everytime I try and import a PDF into CorelDraw it keeps telling me it's a corrupt file.


---
**HalfNormal** *July 15, 2016 02:49*

**+Ned Hill** What version of CD are you using? I am using X7.


---
**Ned Hill** *July 15, 2016 02:50*

I'm using 12 which is probably the problem.


---
**HalfNormal** *July 15, 2016 02:53*

Try downloading Yuusuf's X5 version.


---
**Phillip Conroy** *July 15, 2016 04:51*

Copy and paste also works if i remember correctly,with the line i just flood fill the object


---
**Ned Hill** *July 15, 2016 18:47*

**+HalfNormal** Thanks.  I found Yuusuf's dropbox files and installed CD X5.  Importing pdf files works fine now :)


---
**HalfNormal** *July 15, 2016 19:36*

**+Ned Hill**​ I am glad you I was able to help.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/QXNazvBd33K) &mdash; content and formatting may not be reliable*
