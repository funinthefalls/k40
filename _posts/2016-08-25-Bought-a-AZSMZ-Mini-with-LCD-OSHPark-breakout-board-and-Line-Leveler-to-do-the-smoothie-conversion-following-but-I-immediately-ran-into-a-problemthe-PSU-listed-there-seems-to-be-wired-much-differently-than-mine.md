---
layout: post
title: "Bought a AZSMZ Mini with LCD, OSHPark breakout board, and Line Leveler to do the smoothie conversion following , but I immediately ran into a problem...the PSU listed there seems to be wired much differently than mine"
date: August 25, 2016 01:20
category: "Smoothieboard Modification"
author: "Chris Sader"
---
Bought a AZSMZ Mini with LCD, OSHPark breakout board, and Line Leveler to do the smoothie conversion following [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide), but I immediately ran into a problem...the PSU listed there seems to be wired much differently than mine. Of course, I have the K40 with the "upgraded" panel including a temp guage, e-stop, redundant power switch, and digital pot controls/screen.



Here's a photo of the wiring on mine, if it helps. Any recommendations on best place to start figuring out how to wire this thing up?



![images/3156fd8d9394b57168629c72aff5bbe6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3156fd8d9394b57168629c72aff5bbe6.jpeg)
![images/d29b6e6b0814109e29e1a329953799f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d29b6e6b0814109e29e1a329953799f8.jpeg)
![images/e31f21e82e0a6012bf28b50e751571c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e31f21e82e0a6012bf28b50e751571c6.jpeg)

**"Chris Sader"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2016 01:25*

Looks typical to me. You've got the ribbon cable variant of the machine which comes with the screw terminal psu. The left block is ac voltage in, and the right block is the voltages and L (laser fire) going to the stock control board. The middle connections on the psu usually receive the potentiometer and enable/ test fire buttons on the panel, but there may be some differences on that part... 


---
**Ariel Yahni (UniKpty)** *August 25, 2016 01:33*

**+Chris Sader** I have the same smoothie compatible board. I the right most connector of the PSU read L then that goes into a mosfet negative pole on the azsmz. But first you need a middelman board ( look my profile for k40 upgrades ) or rip apart that  that ribbon cable. 


---
**Chris Sader** *August 25, 2016 01:48*

**+Ariel Yahni**​ I have the oshpark middleman board, that will work right?


---
**Chris Sader** *August 25, 2016 01:54*

I'm also assuming you mean the L line goes from PSU into negative pole on D10, then from positive on D10 to laser fire on the oshpark board?


---
**Ariel Yahni (UniKpty)** *August 25, 2016 01:56*

**+Chris Sader** Yes. assemble it and that should take care of the endstops and on of the motors ( dont remember which one but should be easy to identify )


---
**Ariel Yahni (UniKpty)** *August 25, 2016 01:58*

No on your last statement. Oshpark middle man will only take care of what i mentioned above. A better picture of your board and i can confirm exactly. Get that part going including being able to jog correctly and ill take you from there, unless you want to go all the way ( i recommend first get the machine moving )


---
*Imported from [Google+](https://plus.google.com/107747838010116449754/posts/WVuf5P4CHud) &mdash; content and formatting may not be reliable*
