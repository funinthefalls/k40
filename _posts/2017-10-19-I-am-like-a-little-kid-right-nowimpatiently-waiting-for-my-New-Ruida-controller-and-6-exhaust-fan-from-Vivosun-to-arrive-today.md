---
layout: post
title: "I am like a little kid right now,impatiently waiting for my New Ruida controller and 6\" exhaust fan from Vivosun to arrive today"
date: October 19, 2017 16:08
category: "Modification"
author: "Anthony Bolgar"
---
I am like a little kid right now,impatiently waiting for my New Ruida controller and 6" exhaust fan from Vivosun to arrive today.





**"Anthony Bolgar"**

---
---
**Ariel Yahni (UniKpty)** *October 19, 2017 16:22*

I get you. I go crazy waiting for small yellow envelopes from amazon


---
**Printin Addiction** *October 19, 2017 20:28*

I have the 4 inch Vivosun inline blower with adjustment knob, works really well, have it mounted to the ceiling in the basement....

![images/908619b88f95ad9f90ca6a93b3551286.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/908619b88f95ad9f90ca6a93b3551286.jpeg)


---
**Anthony Bolgar** *October 19, 2017 21:50*

I love the new exhaust fan, 450CFM through a 6" pipe, variable speed, and not too noisy.


---
**Anthony Bolgar** *October 19, 2017 21:51*

Got the Ruyida RDC6442XG controller today as well. Hope to have it all setup by Sunday afternoon.




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/2AnjxSrusvM) &mdash; content and formatting may not be reliable*
