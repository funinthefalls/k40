---
layout: post
title: "Looking for Feedback on eBay Purchase of K40 What do you think of this?"
date: December 05, 2017 15:14
category: "Discussion"
author: "Rob Mitchell"
---
Looking for Feedback on eBay Purchase of K40



What do you think of this? Price seems right. Just need to know if I may be missing something in the description.



Would you endorse the purchase of this kit?

From this ebay seller?



Thank You



[https://www.ebay.com/itm/40W-CO2-Laser-Engraver-Cutting-Machine-Cutter-w-Digital-Electric-Current-Displa/263072197823?_trkparms=aid%3D555019%26algo%3DPL.BANDIT%26ao%3D1%26asc%3D41375%26meid%3D4682634550b74773b47c6c0edf47b0c9%26pid%3D100706%26rk%3D1%26rkt%3D1%26&_trksid=p2045573.c100706.m4781](https://www.ebay.com/itm/40W-CO2-Laser-Engraver-Cutting-Machine-Cutter-w-Digital-Electric-Current-Displa/263072197823?_trkparms=aid%3D555019%26algo%3DPL.BANDIT%26ao%3D1%26asc%3D41375%26meid%3D4682634550b74773b47c6c0edf47b0c9%26pid%3D100706%26rk%3D1%26rkt%3D1%26&_trksid=p2045573.c100706.m4781)







**"Rob Mitchell"**

---
---
**Alex Raguini** *December 05, 2017 15:23*

Personally, I'd stay away from the digital control panel.  It looks sexy and modern but it doesn't really allow you to control the power as well as you would think. I had one with a digital panel and have since converted it to a potentiometer and meter reading in milliamps - I now have much finer control.  I also have done a lot of modifications since acquiring one.




---
**Chris Hurley** *December 05, 2017 15:24*

It's the newer version which is nice but the digital controller is a negative for some people. 


---
**Printin Addiction** *December 05, 2017 16:15*

The digital control panel is a little flaky, pressing +1 sometimes increments +.1, +10 sometimes increments +1 or +.1



Also, usually everyone speaks in terms of current when talking about power, so you will always need to do the conversion, and it's not as straightforward as one would think.



Not the end of the world, just something to be mindful of.


---
**Rob Mitchell** *December 05, 2017 16:34*

Ok, got the note about the digital panel. How about this model and vendor. [https://www.ebay.com/itm/40W-CO2-Laser-Engraver-Cutting-Machine-Cutter-w-Digital-Electric-Current-Displa/263072197823?_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D41375%26meid%3D961b17dea08f4035bd65d6ad66932355%26pid%3D100033%26rk%3D2%26rkt%3D6%26sd%3D152683465371&_trksid=p2045573.c100033.m2042](https://www.ebay.com/itm/40W-CO2-Laser-Engraver-Cutting-Machine-Cutter-w-Digital-Electric-Current-Displa/263072197823?_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D41375%26meid%3D961b17dea08f4035bd65d6ad66932355%26pid%3D100033%26rk%3D2%26rkt%3D6%26sd%3D152683465371&_trksid=p2045573.c100033.m2042)




---
**Anthony Bolgar** *December 05, 2017 16:37*

One plus for the 2nd seller you posted is that the ebay add is written in understandable English, and they have 2 U.S. warehouses.


---
**J M** *December 05, 2017 18:14*

What potentiometer did you use to replace the digital readout ?

My machine is digital and the % constantly fluctuates through  the cutting process. I have to keep adjusting. It can go from 15% to 99% power or it will shut off in the middle of a job.


---
**Don Kleinschnitz Jr.** *December 05, 2017 18:31*

**+Jonny M** these may help...



[donsthings.blogspot.com - Adding an Analog Milliamp Meter to a K40](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html)



[http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)


---
**J M** *December 06, 2017 15:16*

**+Don Kleinschnitz**   Excellent - thanks for the link. I will look into this conversion. 


---
*Imported from [Google+](https://plus.google.com/105696631925164417072/posts/XLTzE7f6EP8) &mdash; content and formatting may not be reliable*
