---
layout: post
title: "Shared on May 05, 2016 07:30...\n"
date: May 05, 2016 07:30
category: "Hardware and Laser settings"
author: "Neil Robbins"
---


![images/511fc480d32e0857592ae18d88a5edd4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/511fc480d32e0857592ae18d88a5edd4.jpeg)



**"Neil Robbins"**

---
---
**Neil Robbins** *May 05, 2016 07:37*

Old K40 power supply


---
**Neil Robbins** *May 05, 2016 13:50*

this is the power supply I want to connect to ramps 1.4 and don't know which of the red wires I am meant to connect the ramps to for the laser to fire any help would be helpfull


---
**Jean-Baptiste Passant** *May 05, 2016 18:47*

Thank you for this, you may be lucky as it looks like the old board does not use a ribbon cable so it should be way easier.



You should follow each wire and label them, I believe there is more cables than that, as 8 wires cannot do 2 motors + endstop + laser control + pot + switches + ampmeter etc...



You will probably find all the information by yourself, and wiring it will be a breeze.


---
**Neil Robbins** *May 05, 2016 21:56*

I have taken the moshi board out and replaced it with a ramps board which has the ribbon cable for the  the y axis and the end stops and the x axis is another 4 wires.

this is the laser power supply and I need to know how to wire the 2 red wires to the ramps board


---
**Jean-Baptiste Passant** *May 06, 2016 06:10*

Sorry I thought this was the old board.



I believe the two red and black wires are 24V (maybe 12 on the old models ?) You can check this using a voltmeter.

The middle cables are probably the signal cable from the board to the power supply and the right cable is probably connected to the laser, not sure though.


---
**Neil Robbins** *May 06, 2016 06:18*

the 2 black wires are for the on off switch on the lid. I want to know which red wire goes to ground and which one goes to my ramps board

 


---
**Jean-Baptiste Passant** *May 06, 2016 06:26*

Wow, this power supply is definitely shitty... Sorry but color code is really important and it looks like the old K40 doesn't follow any code.

I understand that they can use another color, but usually, red and black are VIN/OUT and GND.



Sorry but I cannot help you more with it, I did my best but I can't really help with that much information.


---
**Neil Robbins** *May 06, 2016 07:12*

that's ok I have 2 choices just didn't want to blow it up wiring it the wrong way. thanks for your help trying to sort it


---
*Imported from [Google+](https://plus.google.com/112727076545743143342/posts/Gyib7VkWJsm) &mdash; content and formatting may not be reliable*
