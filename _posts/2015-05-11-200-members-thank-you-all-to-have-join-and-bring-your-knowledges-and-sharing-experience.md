---
layout: post
title: "200 members, thank you all to have join and bring your knowledges and sharing experience"
date: May 11, 2015 17:53
category: "Discussion"
author: "Stephane Buisson"
---
200 members, thank you all to have join and bring your knowledges and sharing experience. 

![images/1ca1c6e09a588fef6e0a1bbc5d2d7dd3.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1ca1c6e09a588fef6e0a1bbc5d2d7dd3.png)



**"Stephane Buisson"**

---
---
**David Wakely** *May 11, 2015 18:02*

Thanks Stephane for creating the community for us to share all of the knowledge. The people on here are brilliant and without them the K40 would be useless!


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/iVmFK8TJmyo) &mdash; content and formatting may not be reliable*
