---
layout: post
title: "Hi guys, would love your advice on what lens to use when cutting cardstock"
date: November 02, 2017 18:28
category: "Hardware and Laser settings"
author: "Brian Li"
---
Hi guys, would love your advice on what lens to use when cutting cardstock.



Got the K40 up and running with Don's help fixing the y-axis sensor (thanks again!) - I've been cutting somewhat simple designs into 100lb cover cardstock - I uploaded a stock picture for a rough idea of the type of design I'm cutting.  



Currently, I'm using: 

- stock 50mm lens 

- upgraded dust collector exhaust 

- air assist

- Settings: 12ma at 40mm/second speed



It is cutting through well, but does leave a fair amount of charring on the backside - increasing speed and power result in the cut getting a little too wide and sloppy.



Would a different focal length lens help when cutting paper to more tightly focus the power?  Thanks!  

![images/722ce3f34b8bb617ed8da6823b3bc8ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/722ce3f34b8bb617ed8da6823b3bc8ed.jpeg)



**"Brian Li"**

---
---
**Ariel Yahni (UniKpty)** *November 02, 2017 18:43*

What kind of bed do you have?


---
**Brian Li** *November 02, 2017 19:06*

I built a makeshift "nail" bed using 2.5" screws, so there isn't any flashback from that - sorry, I should have clarified, it is more charring from the top side (laser side) of the paper.


---
**Ariel Yahni (UniKpty)** *November 02, 2017 19:38*

Not sure if there a preferred focal length. Have you tried going faster and multiple passes?


---
**Anthony Bolgar** *November 02, 2017 23:52*

I cut cardstock at 50mm/s using 5mA, but that is on a 60W machine. But I think you should be able to lower the mA setting quite a bit. I would try slowly lowering it by 1mA at a time to see if that helps. Also try going a little faster.


---
*Imported from [Google+](https://plus.google.com/115134399025275644407/posts/39szoSLGVvE) &mdash; content and formatting may not be reliable*
