---
layout: post
title: "I have a question I have been cutting tokens for a game that I play and I want to know how to get a smoother engrave"
date: November 29, 2016 05:00
category: "Discussion"
author: "3D Laser"
---
I have a question I have been cutting tokens for a game that I play and I want to know how to get a smoother engrave.  It has a lot of ridges.  I don't know if I am engraving at to high a power but any lower and I still get residue from the tape 

![images/4df357602651c89a57a234da75e632f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4df357602651c89a57a234da75e632f3.jpeg)



**"3D Laser"**

---
---
**Anthony Bolgar** *November 29, 2016 05:19*

You can try defocusing the beam a little, so that it covers a larger area with a bit of overlap.


---
**Alex Krause** *November 29, 2016 05:39*

What material?


---
**3D Laser** *November 29, 2016 05:40*

Acrylic 


---
**Alex Krause** *November 29, 2016 05:41*

Are you coloring the inlay?


---
**3D Laser** *November 29, 2016 05:42*

**+Alex Krause** yes the pic the white acrylic painted red where it is engraved


---
**Alex Krause** *November 29, 2016 05:46*

Like **+Anthony Bolgar**​ said defocus by a slight amount will smooth out the engrave a bit... or you can engrave a bit deeper and cut an inlay material to glue to it for a smooth surface


---
**John-Paul Hopman** *November 29, 2016 17:31*

I was thinking you could use an enamel paint that would be self-smoothing.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/P2UhoKf7tK7) &mdash; content and formatting may not be reliable*
