---
layout: post
title: "What have I done?! Took an hour and a half to unbox it in the back lane behind the house as I wouldnt go through the gate, then upstairs to the \"office\""
date: October 30, 2015 19:09
category: "Discussion"
author: "Peter"
---
What have I done?!

Took an hour and a half to unbox it in the back lane behind the house as I wouldnt go through the gate, then upstairs to the "office". Some hastily moved junk from the desk as I was bigger than I was told.

Cat in the picture for scale...

If I ever move house its staying where it is.

![images/e8a83ea3a4f5ce7f215eccb6d7359ea2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8a83ea3a4f5ce7f215eccb6d7359ea2.jpeg)



**"Peter"**

---
---
**Ashley M. Kirchner [Norym]** *October 30, 2015 19:39*

Which one is that? 


---
**Peter** *October 30, 2015 19:57*

Its a 50W ebay machine.

The article says "350B 50W CO2 Laser" but its the same non-branded Chinese type Laser.

I had to wait in for delivery, then go to work when it didnt show up, then come back home for delivery again, now back at work so I cant even play.

I dont finish until 10pm so I'll have to be tomorrow now.



How accurate this is I dont know... and im a bit worried now I didnt notice any pump/compressor or "extras" bag... :(

I did find the Rotary Attachment though.





Technical Parameters



Laser power

50W 



Laser type

Hermetic CO2 glass tube



Power supply

AC110±10% 60HZ



Working area

300mm X 500mm



Max moving speed

500mm/s



Locating precision

<0.01mm



Min shaping character

Character: 2x2mm, Letter: 1x1mm



Operating temperature

5℃-40℃



Resolution ratio

≤4500dpi



Control configuration

DSP



Data transfer interface

USB(offline)



Worktable



DC-G350D –up&down table



System environment

WINDOWS 2000/WINDOWS XP/ WIN7/VISTA



Cooling way

Water Cooling and protection system



Graphic Format Supported

The Files which CorelDraw, Photoshop, AutoCAD can 

identify(BMP,GIF,JPEG,PCX,TGA,TIFF,PLT,CDR,DMG,DXF,

PAT,CDT,CLK,DEX,CSL,CMX,AI,WPG,WMF,EMF,CGM,SVG,SVGZ,

PCT,FMV,GEM,CMX)



Compatible software

CorelDraw, AutoCAD（Not Included in this item）



Cutting thickness

0-4mm (depends on different materials)



Voltage  220V



Sloping engraving

Yes, slope can be designed discretionarily



Cuts off the water supply the protection

Yes



Packing

Plywood case



Parts

Cylinder Rotary Attachment



Machine Dimension

1020×650×630 mm



Net Weight

65KG


---
**Greg M (iTrooper)** *October 30, 2015 22:41*

Does the cat come with it? LOL



Actually I've visited someone who has one of the 50w machines, it's a beauty! I'm definitely thinking of picking that one up compared to the K40.



Seems to have everything you need straight out of the box :D


---
**Peter** *October 30, 2015 23:01*

Yeah, the cat jumped out of the box when I opened it. I shall call her spring roll. 



It might potentialy have all the bits straight out of the box but it needs some tidying and a bit or straightening before it could be used! Only been playing for 40mins but its full of the usual dust and warehouse material, floor sweepings and the like, and the bed is about 5mm out from left to right. Highest on the left hand side. Hopefully with the adjustable bed I can just undo the corner screws that raise and lower it and move it manually. Probably out back and front too but I didn't check yet. 




---
**Stephane Buisson** *October 31, 2015 08:20*

890GBP for that price you have good value for money.

most mod being done.

 [http://www.ebay.co.uk/itm/CorelDraw-AutoCAD-50W-CO2-220V-USB-Lsaer-Engraving-Engraver-Cutting-Machine/400970882686?_trksid=p2047675.c100009.m1982&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D33957%26meid%3D3aa2c75a8b1c4c2c9361ede08d67003d%26pid%3D100009%26rk%3D5%26rkt%3D10%26mehot%3Dpp%26sd%3D111809036078](http://www.ebay.co.uk/itm/CorelDraw-AutoCAD-50W-CO2-220V-USB-Lsaer-Engraving-Engraver-Cutting-Machine/400970882686?_trksid=p2047675.c100009.m1982&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D33957%26meid%3D3aa2c75a8b1c4c2c9361ede08d67003d%26pid%3D100009%26rk%3D5%26rkt%3D10%26mehot%3Dpp%26sd%3D111809036078)


---
**Greg M (iTrooper)** *November 01, 2015 13:43*

I've been eyeing that machine for a while too, the K40 is cheaper, and smaller, but I've seen the 50w in person and it's sure a beauty and has a lot of things right out of the box that one would have to mod the K40 a lot to match.


---
*Imported from [Google+](https://plus.google.com/+PeterJones79/posts/ZBM4H69CHDx) &mdash; content and formatting may not be reliable*
