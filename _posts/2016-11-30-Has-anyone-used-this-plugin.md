---
layout: post
title: "Has anyone used this plugin?"
date: November 30, 2016 15:59
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
Has anyone used this plugin?

[http://birchenough.co.uk/sketchup-laser-cutter-plugin/](http://birchenough.co.uk/sketchup-laser-cutter-plugin/)







**"Don Kleinschnitz Jr."**

---
---
**Cesar Tolentino** *November 30, 2016 16:10*

I saw that but I still can't figure why I need it.  As long as I can export to  ext I'm good. Maybe I'm missing something. 


---
**Don Kleinschnitz Jr.** *November 30, 2016 16:16*

I am just starting my tool chain up again after my smoothie conversion.

I am trying to understand where the lasers kerf is accounted for in the design process. This being especially true for tabbed boxes. 

This seems to take a standard sketchup 2D drawing and adjust it for the kerf while in sketchup?

I also thought it would be nice to see the Gcode while in SU and before progressing to export a SVG to LaserWeb. 

All the above are my learning musings!


---
**Cesar Tolentino** *November 30, 2016 16:21*

In my case doing tabbed boxes. The laser cuts in the middle of the line so both if I may say male and female material cut in the same laser always snaps together perfect.  



Now the question is if the other material came from a different machine? Maybe a 3d printed material?



Or if u are really doing super precise cuts then maybe I see the plugin useful?


---
**Ashley M. Kirchner [Norym]** *November 30, 2016 22:40*

Laser cutting will always result in a kerf, so one has to account for that if you want your pieces to fit perfectly without any kind of wobble or looseness between them. For me, my toolchain consists of drawing in 3D (to assemble to piece virtually). Once everything is done, I will offset all the edges and internal cutouts by 0.1mm, export the faces, and cut. They will literally "snap" together, no glue needed and often, once snapped together, taking them apart will result in the wood breaking because it's held together so tight.



So if the plugin accounts for the kerf, then it ought to also ask you what your kerf is. Not all lasers are the same. Otherwise, you'll have to account for it in your drawing.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/M1HcqmBtSjQ) &mdash; content and formatting may not be reliable*
