---
layout: post
title: "It's hard to see but there appears to be a brown burn mark on my tube does that means it dead"
date: March 15, 2016 22:45
category: "Discussion"
author: "3D Laser"
---
It's hard to see but there appears to be a brown burn mark on my tube does that means it dead

![images/924be96073e489fc48d4cc36f1363fa1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/924be96073e489fc48d4cc36f1363fa1.jpeg)



**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 15, 2016 23:03*

Might be best if you can give us a slight indication of where to look. The only brown mark I can see is right near the little white circle of light reflecting (near the centre of the image). Is that where you are referring to?



edit: on closer inspection I can also see a brown ring-like mark down near the silver/metal thing at the bottom of the image.


---
**3D Laser** *March 15, 2016 23:05*

**+Yuusuf Sallahuddin** yes I am trying to narrow down if it's my tube or power supply that is blown



I think the brown ring is just a reflection but I will have to look closer 


---
**I Laser** *March 15, 2016 23:22*

From previous posts, it's usually either a crack in the glass or the end dislodges.



[http://bit.ly/1S2TdQC](http://bit.ly/1S2TdQC)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/Uc4jXVSdKrt) &mdash; content and formatting may not be reliable*
