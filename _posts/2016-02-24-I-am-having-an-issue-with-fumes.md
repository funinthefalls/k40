---
layout: post
title: "I am having an issue with fumes"
date: February 24, 2016 01:17
category: "Discussion"
author: "3D Laser"
---
I am having an issue with fumes.  As you can see my exhaust has a ways to travel.  I cut a few pieces of acrylic tonight and my whole house smells.  I need a solution fast thanks for the help in advance

![images/70751c864eb9bba1ac4910fe74bc7937.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/70751c864eb9bba1ac4910fe74bc7937.jpeg)



**"3D Laser"**

---
---
**Tony Sobczak** *February 24, 2016 01:25*

Did you place any weather stripping between the fan and the engraver? I can also see that the fan housing is crooked. Since the weather stripping not much odor inside the shop. ﻿


---
**Alex Krause** *February 24, 2016 02:05*

Aluminized ducting can be fragile you could have a hole in it


---
**Alex Krause** *February 24, 2016 02:06*

**+Corey Budwine**​ where does the aluminized duct vent to? Is it a shared duct with another appliance?


---
**3D Laser** *February 24, 2016 02:07*

I'm going to move the whole set up closer to the vent tomorrow


---
**3D Laser** *February 24, 2016 02:08*

**+Alex Krause** it vents out an old dryer vent on the side of the house 


---
**Sean Cherven** *February 24, 2016 02:17*

When ever I cut acrylic, I hardly smell anything.. Even without the exhaust.. Now polycarbonate (plexi-glass & lexan) is NOT acrylic, and the fumes smell horrible! And can probably kill you if your exposed to it long enough.


---
**3D Laser** *February 24, 2016 02:22*

It is cast acrylic according to the listing I bought it from


---
**Sean Cherven** *February 24, 2016 02:35*

That's interesting then.. Acrylic does have a little smell, but it's not that bad compared to polycarbonate. That stuff is toxic.


---
**3D Laser** *February 24, 2016 02:45*

looking at the listing I bought it from it was extruded acrylic is that bad to cut?


---
**Sean Cherven** *February 24, 2016 03:08*

Acrylic is fine, just make sure you don't get polycarbonate of any type. If you think acrylic fumes are bad, you haven't seen (or should I say smelled?) anything.


---
**Sean Cherven** *February 24, 2016 03:11*

**+Carl Duncan**​ Yeah because extruded acrylic has a grain to it, like wood does. But it only shows when engraved or cut. They say extruded acrylic is also a bit weaker.


---
**charlie wallace** *February 24, 2016 05:04*

enclose the cutter in another box, its full of holes.


---
**Phillip Conroy** *February 24, 2016 08:27*

I usehttp://[www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT) u need to vent out side and have shortest lengh og tubing as possable, also used 8 inch to 6 inch adaptor then a 6inch to 4 inch adaptor wkrks well with fan outside and only 2 metres of tubing,hardy need ajr asist it is that good,original fan needed 5 seconds to clear mfd smoke when finished cutting ,new fan no smoke builds up in laser cutter so k can open lid straight after finished cutting,


---
**Tony Sobczak** *February 24, 2016 09:48*

**+Sean Cherven**​ I thought that PLEXIGLAS IS acrylic. One of the first big names. ﻿


---
**William Steele** *February 24, 2016 14:25*

The fan on the back is useless... as someone else stated, it needs to be at the end of the run... it needs to pull out the air by creating a vacuum, not by pressurizing it... otherwise, any leaks will cause the exhaust to get into the house.  If it's at the end, it will always draw fresh air through the laser and pull it out the hose.




---
**Joe Keneally** *February 24, 2016 14:40*

Try this one - [http://www.homedepot.com/p/Inductor-4-in-In-Line-Duct-Fan-DB204/100073963](http://www.homedepot.com/p/Inductor-4-in-In-Line-Duct-Fan-DB204/100073963)




---
**Tony Sobczak** *February 24, 2016 18:36*

Ebay has them much cheaper.


---
**ben ward (FutureLab3D)** *February 25, 2016 10:18*

You need to buy a fan with strong suction. Maybe a carbon filter system.


---
**Bee 3D Gifts** *February 28, 2016 21:32*

We use something close to this: [https://jet.com/product/detail/c229a6ed777e4e11a60984e595705cf6?jcmp=pla:ggl:home_garden_a1:household_appliances_climate_control_appliances_fans_a1_other:na:na:na:na:na:2&code=PLA15&k_clickid=299dba85-8546-4aa0-bb76-b953af7f501e&abkId=403-177937&gclid=Cj0KEQiA0sq2BRDRt6Scrqj71vQBEiQAg5bj07S5-FxJr-2KzhJD8ebMm1SMsPq5EQJF3XRS3sMTNA8aAlIB8P8HAQ](https://jet.com/product/detail/c229a6ed777e4e11a60984e595705cf6?jcmp=pla:ggl:home_garden_a1:household_appliances_climate_control_appliances_fans_a1_other:na:na:na:na:na:2&code=PLA15&k_clickid=299dba85-8546-4aa0-bb76-b953af7f501e&abkId=403-177937&gclid=Cj0KEQiA0sq2BRDRt6Scrqj71vQBEiQAg5bj07S5-FxJr-2KzhJD8ebMm1SMsPq5EQJF3XRS3sMTNA8aAlIB8P8HAQ)



Basically a 4" vent blower - we had some wicked smells going untill i reduced the vent length and added the blower. no more smell or fumes.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 08:20*

**+Sean Cherven** Just checking, does polycarbonate cut well? I've seen that you mention the fumes are horrible/harmful, so I'm assuming you've attempted to cut it. Any success?


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/bs6JzY789h9) &mdash; content and formatting may not be reliable*
