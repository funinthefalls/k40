---
layout: post
title: "Here's the test run of the problems I'm having"
date: July 12, 2016 19:18
category: "Hardware and Laser settings"
author: "Meir Santos"
---
Here's the test run of the problems I'm having.  All power levels were at 30....engraving dips on the lower right but cutting seems to still work.  Weird!

![images/eff4ec76887879f4a69a90e7941dccab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eff4ec76887879f4a69a90e7941dccab.jpeg)



**"Meir Santos"**

---
---
**Derek Schuetz** *July 12, 2016 19:27*

Put tape in front of the last mirror move it all the way to the left and fire laser then move all the way to the right and fire the laser if the dots don't line up then Thats your issue


---
**greg greene** *July 12, 2016 19:39*

Agreed - look on You tube - there is a video showing a quick and easy way to align the mirrors - with diagrams and a walk through.


---
**Meir Santos** *July 12, 2016 20:03*

I think I figured it out....dirty mirrors!  I cleaned the mirrors and wala!  Big Difference!  I will do some test and realign if needed....thanks!


---
**3D Laser** *July 12, 2016 21:30*

Be careful running it at 30ma you will blow your tube in no time with power levels that high.  I'm able to cut through birch plywood at about 8ma and 7mps if it is taking you that much power your out of alignment for sure 


---
**greg greene** *July 12, 2016 21:31*

Corey, is that a single pass cut?


---
**3D Laser** *July 12, 2016 21:44*

Yep for 1/8 inch ply


---
**greg greene** *July 12, 2016 21:50*

Wow, mine takes 5 passes - but I have the speed set to 25 mm/sec at 10 ma - Can't wait to try your settings.  Am also changing to the 18 mm Light Object air assist head with MO mirror and lens so I can get it to focus better.

Thanks for the info !!!!


---
**3D Laser** *July 12, 2016 21:55*

**+greg greene** yes I have an air assist with an 18mm lens and SI mirrors it makes a huge difference 


---
**Meir Santos** *July 12, 2016 22:41*

Ok...I'll do more test and try using your settings till I get something....and do some realigning.  Thanks!


---
**Brook Drumm** *July 12, 2016 22:43*

We clean our lasers daily under heavy use. And alignment helps a lot if certain areas dip in effectiveness 


---
**Ariel Yahni (UniKpty)** *July 12, 2016 23:05*

If it takes you more than 1 pass at 10mms / 10ma then you have and issue. Thats whats the average user is doing for 3mm




---
**Meir Santos** *July 13, 2016 01:11*

I also have the 18mm ZnSe lens and air assist....still not getting your results.  My X axis is pretty aligned but I noticed that my Y axis dot alignment is in the same spot but off to the left of the mirror by a lot.  Can that have something to do with it? 


---
**Derek Schuetz** *July 13, 2016 01:13*

**+Meir Santos** that's exactly your issue


---
**3D Laser** *July 13, 2016 02:04*

**+Meir Santos** they need to be the same spot if possible my y axis is dead on but the beam does start to raise a bit as it travels Dow the x axis but it doesn't affecting the cutting all that much for me.  If my machine was another 100mm longer I would have some issues 


---
**David Cook** *July 29, 2016 00:12*

here is a decent guide I just found today for alignment procedure.  I'm about to run through it on my machine this week, I'm getting triple line ghosting on the fatr right after installing my LightObjects air-head and 18mm lens. [https://drive.google.com/open?id=0ByNecjb0RD-TX2I4ZGNEZERLbkE](https://drive.google.com/open?id=0ByNecjb0RD-TX2I4ZGNEZERLbkE)




---
*Imported from [Google+](https://plus.google.com/107056752453136978834/posts/3v19UraT1AR) &mdash; content and formatting may not be reliable*
