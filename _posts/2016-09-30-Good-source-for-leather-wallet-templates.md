---
layout: post
title: "Good source for leather wallet templates"
date: September 30, 2016 03:41
category: "Repository and designs"
author: "Ariel Yahni (UniKpty)"
---
Good source for leather wallet templates



[http://makesupply-leather.com/templates/page/2/](http://makesupply-leather.com/templates/page/2/)





**"Ariel Yahni (UniKpty)"**

---
---
**Bob Damato** *September 30, 2016 13:02*

Good stuff there thank you. Anyone actually use our lasers to cut the leather? 


---
**Ariel Yahni (UniKpty)** *September 30, 2016 13:07*

**+Yuusuf Sallahuddin**​ **+Alex Krause**​ have. I'm on my way


---
**Bob Damato** *September 30, 2016 13:35*

Be sure to have your exhaust fan on full blast  :)


---
**Bill Keeter** *September 30, 2016 13:44*

does the leather cut clean? or do you need to add tape or something around the leather cut area?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 30, 2016 13:50*

I almost exclusively use my K40 for leather (with the exception of plywood for testing things). As you mention **+Bob Damato**, the smell is quite horrible for most leather/suede. As per a suggestion from I believe it was **+Alex Krause** in the past, I tested placing cut suede (which smelled horrible) in a container with newspaper & bicarbonate of soda. Forgot about it & ended up leaving there for about a week & the smell had mostly subsided/been absorbed?



**+Bill Keeter** The leather most definitely does not cut cleanly. You will always have a burned edge on your cut, but that can be sanded back a little & then finished as per usual leather finishing (burnishing + edge-kote or beeswax). The thinner the leather, the better the edge will be however (e.g. my 1-1.5mm thick kangaroo hide cuts beautifully with only the smallest darkening of the edges, which I actually leave because it looks nice). I would recommend using a low-tack painter's tape for engraving though, to prevent any sooty stains which can get into the leather around the engrave.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/APPHwNAZU1g) &mdash; content and formatting may not be reliable*
