---
layout: post
title: "I purchased this for exhaust system However I'm getting some air leakage from under the \"leather\" material that covers it"
date: December 14, 2016 15:30
category: "Discussion"
author: "Nigel Conroy"
---
I purchased this for exhaust system



However I'm getting some air leakage from under the "leather" material that covers it. So obviously the fumes/smoke is not all being pushed out the ducting.



Is this normal or is the fan faulty?



[https://www.amazon.com/gp/product/B01BE4N7UU/ref=oh_aui_detailpage_o00_s01?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01BE4N7UU/ref=oh_aui_detailpage_o00_s01?ie=UTF8&psc=1)





**"Nigel Conroy"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 14, 2016 19:36*

Most people seem to place the fan at the end of the exhaust run, to prevent these sort of issues. From what I've read, there is always a strong possibility of leakage when pushing the air. Less so when pulling the air as it will pull in the leak points (rather than push out). Maybe try the other end of your run.


---
**Nigel Conroy** *December 14, 2016 20:10*

Thanks **+Yuusuf Sallahuddin** I had a similar thought. There is an AC unit in the window at the moment that I'm hoping to get removed. Currently venting at the top of the window so a bit more difficult to mount at the top but I'll give that a try.


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/6qBF4kZFpzd) &mdash; content and formatting may not be reliable*
