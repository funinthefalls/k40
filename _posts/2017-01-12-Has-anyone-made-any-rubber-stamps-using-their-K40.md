---
layout: post
title: "Has anyone made any rubber stamps using their K40?"
date: January 12, 2017 09:24
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Has anyone made any rubber stamps using their K40?



I'm wondering what material is suitable/best for lasering a stamp but having a lot of difficulty finding any actual materials when I search (basically just finding a lot of people that will do it for you).



Any advice/suggestions is greatly appreciated.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Andy Shilling** *January 12, 2017 09:39*

I have made this stamp using linoleum but it had to vented extremely well.

![images/45def40674d7c191ac4dc208a67ab8cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/45def40674d7c191ac4dc208a67ab8cd.jpeg)


---
**Kostas Filosofou** *January 12, 2017 09:48*

Something like that?  [ebay.de - Details zu  Stempelgummi Lasergummi für CO2 Lasergravierma<wbr/>schine Laser Stempel Gummi](http://www.ebay.de/itm/291463370023)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 12, 2017 09:58*

Thanks guys. Yeah that looks like what I want **+Konstantinos Filosofou**.


---
**Ned Hill** *January 12, 2017 13:31*

**+Andy Shilling** Modern "linoleum" is actually made from PVC which  releases highly corrosive HCl gas when heated/burned.  So it is not recommended for laser cutting.  Just as a PSA :)


---
**Andy Shilling** *January 12, 2017 13:38*

**+Ned Hill**​ thanks for the heads up, I presumed it was safe because it's a natural hessian backed linseed oil and Cork product. I realise there are other types of lino but I thought linoleum was specific to the original product. 


---
**Ned Hill** *January 12, 2017 13:43*

**+Andy Shilling** If you have some of the true linoleum then yes that should be fine.  Just wanted to make people aware that a lot of what people generically call "linoleum" is actually vinyl now days.


---
**Andy Shilling** *January 12, 2017 13:51*

**+Ned Hill**​ your a good man, I didn't even think about that. What I have here is the proper stuff but I would still like to make people aware that using the natural linoleum with still give off a nasty aroma and should be well ventilated.


---
**Jeff Bovee** *January 13, 2017 20:18*

I had found this material on Amazon but have not tried it.  [amazon.com - Amazon.com: Speedball Speedy-Carve Block, 6"X12": Arts, Crafts & Sewing](https://www.amazon.com/dp/B001DNCFJ2/ref=wl_it_dp_o_pC_nS_ttl?_encoding=UTF8&colid=20M50FTUEVZ1X&coliid=I6HL8AAIGOU4C)




---
**Vince Lee** *January 13, 2017 21:37*

I've used the laser rubber from laserbits with good results:  [laserbits.com - Welcome to  Premium Laser Rubber](http://www.laserbits.com/stamp-products/low-odor-laser-rubber.html)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 14, 2017 03:52*

Thanks **+Jeff Bovee** & **+Vince Lee**. I'll check them out.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/YY869P567UK) &mdash; content and formatting may not be reliable*
