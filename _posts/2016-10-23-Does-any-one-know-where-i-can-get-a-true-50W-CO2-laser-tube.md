---
layout: post
title: "Does any one know where i can get a true 50W CO2 laser tube?"
date: October 23, 2016 11:15
category: "Material suppliers"
author: "Anthony Bolgar"
---
Does any one know where i can get a true 50W CO2 laser tube? I do not one of the fake relabled tubes most of the chinese suppliers on Aliexpress and ebay try to pass off as a higher wattage than what they really are.





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *October 23, 2016 13:19*

Thanks **+Peter van der Walt** I may not have room for the 60W in my Redsail LE400, but I know a 50W does fit. If it is not too much oversize, I am OK building an extension box to protect it.

 Edit: After looking at their web site, I noticed that 60W is their smallest offering (Max 75W at full power....nice!) Interested in finding out the price.


---
**Anthony Bolgar** *October 23, 2016 13:44*

Not too shaby, hopefully they have not gone up too much since then, I fired off an email too them for pricing.


---
**varun s** *October 23, 2016 16:44*

Check out SPT Laser Tube they do have a aliexpress store, decent prices.


---
**varun s** *October 23, 2016 16:49*

$125 and peak power of 70W this is the same brand Light Object also carries [https://m.aliexpress.com/item/32276098173.html#autostay](https://m.aliexpress.com/item/32276098173.html#autostay)


---
**Anthony Bolgar** *October 23, 2016 19:17*

But shipping is over $300 USD to Canada


---
**varun s** *October 23, 2016 19:55*

You can contact them and ask for alternative shipping carrier, they are friendly and respond fast too.


---
**varun s** *October 23, 2016 19:55*

Why not check Light Object? Closer to Canada!


---
**Eric Rihm** *October 24, 2016 17:39*

I'd buy a 60w china tube and get the length beforehand, should get you the 50w you need without having to deal with bait and switch. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/cWEB4C8WQMV) &mdash; content and formatting may not be reliable*
