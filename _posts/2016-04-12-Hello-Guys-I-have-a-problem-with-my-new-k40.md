---
layout: post
title: "Hello Guys! I have a problem with my new k40"
date: April 12, 2016 13:58
category: "Discussion"
author: "Rui Rodrigues"
---
Hello Guys!



I have a problem with my new k40.

When I try engrave aluminium pen the laser can't engrave until metal, why?



Thank you

![images/02910adbe9c1fbcb25e129188f276fae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/02910adbe9c1fbcb25e129188f276fae.jpeg)



**"Rui Rodrigues"**

---
---
**Jim Hatch** *April 12, 2016 14:18*

What exactly are you trying to do? Looks like you're attempting to cut through the  lacquer (or whatever paint finish) first. You need more power or slower speed as it looks like you did mark it. 



Aluminum won't cut or engrave in the way you see wood or acrylic or other materials - you're just marking it. It's burning off the anodization layer. You won't get the 3D engraving but just a marking that almost looks like paint or dye. 



I just did a bunch of iPads and Samsung tablets without any problem. Scott Thorne here has experience with aluminium engraving too.


---
**Rui Rodrigues** *April 12, 2016 14:39*

Hello Jim!



I just want take off the black paint but I already test with max power and low power and I can't put in pure alumminium colour



This machine has 1 week and I am really noob, I have to calibrate or update? Wich board drive I have select in device settings?



Thabk you


---
**Jim Hatch** *April 12, 2016 15:14*

Okay, first you need to get it setup correctly. If you can engrave or cut cardboard or wood then it's likely in good shape.



Typically though you need to make sure the software is set for your board - Nano M2 and Moshi boards are the most common. You can find out what kind you have by looking inside the machine on the right side under the control panel. The board type is printed on it. The serial number is as well. You enter that information in the machine settings in either LaserDRW or CorelLaser depending on which you're using.



Once that is setup you may need to align the mirrors. There are some good videos on YouTube for that. But first check to see if you can do a test engrave with the software. Make sure the focusing lens/head is 50.7mm (2”) from the material - that's the focusing distance you need for the laser.



Get it working with cardboard or wood first and then move on to the pen.



The way the laser works the power increases when you add more power on the machine control and when you slow the speed - the slower it goes the more power it is delivering to any spot because it's on the spot longer. So 10ma at 300mm/s may be the same as 5ma at 150mm/s. (Every machine is different with regards to laser tube efficiency so it's not necessarily a linear equation but that's the idea.)



I do iPads at 25% power and 300mm/s and it works great. The paint you're doing is actually what you're engraving though so it's going to need to be a trial & error situation for you.



Another note - since you're doing a round object the power of the laser is going to be lower at the top & bottom of the image because it's not as focused due to the curve. You may be 50.7mm away at the uppermost surface of the barrel but 51 or 52mm at the edges as it curves away. That reduces the strength of the etching because the laser is not focused on that part.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 15:22*

Hi Rui. You will find that on your controller board, there is an ID telling you which type of board it is. That is important to set it correctly in the CorelLaser software (as it defaults to a set value, which is more than likely wrong). If you don't, all sorts of strange things happen.



After that, make sure your mirrors are in alignment. Slightly difficult process for this to get it precise (took me days the first time I got my machine) but you get used to it & can do it very quickly once you know what you're doing.



Once you've done that, you need to make sure your item is at the correct focal length from the lens (50.8mm for the stock lens).



After that, you should be away to go. Unfortunately, power & speed settings I cannot suggest yet, as I have not done any engraving on aluminium (although I intend to once I get a rotary attachment made or purchased). If you look through this group, you will more than likely find someone's post that has engraved aluminium & removed the paint/anodization layer & some have shared the settings.


---
**Rui Rodrigues** *April 12, 2016 16:07*

Thank you guys! I'll try your ideas!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 16:41*

Looks like I was typing as **+Jim Hatch** had already hit send. Seems I offered pretty similar suggestions.



Hope it works for you Rui.



**+Jim Hatch** I've been meaning/wanting to do my MacBook, but as it is the computer I use, I cannot do it until I get another computer to run the laser software. Unless I somehow manage to run it plugged in while I am lasering it lol.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 23:52*

**+Jim Hatch** Quick question, when engraving the iPads, is it possible to do different power for the engraves & get say 2 different effects (e.g. 25% power that you use to get your current effect & 10% power to get a lighter effect)? I have an idea for engraving my MacBook that would benefit from being able to do multiple tones of engraving if possible. If not, I'd have to modify the idea to cater for only one tone.


---
**Jim Hatch** *April 13, 2016 01:51*

**+Yuusuf Sallahuddin**​ yep that's definitely doable. I haven't done it intentionally but I did get that effect when I was playing with different power levels & speeds. At lower power/faster speeds you get a lighter image than when at the basic setting (25%/300mm/s).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 02:24*

**+Jim Hatch** Awesome. Thanks Jim. When I get the chance to use a different computer to power the laser I'll do my macbook screen back. I have a plan to use the apple logo into the design. I'm thinking something like Adam & Eve grabbing the apple from the tree, with the snake hanging from the tree.


---
*Imported from [Google+](https://plus.google.com/107832996652493137156/posts/gcY7t4W8VSu) &mdash; content and formatting may not be reliable*
