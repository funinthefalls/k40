---
layout: post
title: "Water! I have heard mention of distilled water but if that is not available, has anyone tried using Reverse Osmosis (RO) water from the local fish store (pet shop type fish not fishmonger!)"
date: June 21, 2016 08:53
category: "Discussion"
author: "Pete Sobye"
---
Water!



I have heard mention of distilled water but if that is not available, has anyone tried using Reverse Osmosis (RO) water from the local fish store (pet shop type fish not fishmonger!). 



It is ultra pure and has less dissolved solids than distilled. Usually about 10p per litre here in the UK. 



Thoughts please??





**"Pete Sobye"**

---
---
**Scott Marshall** *June 21, 2016 09:50*

Most grocery store "distilled" water actually IS RO water. (with the price of energy these days you couldn't boil and cool a gallon of water and sell it at a reasonable price. One of my old accounts (I used to run a little industrial controls business) was a company that operated a leasing service for "bring your own container" distilled water dispensers for grocery and drugstores. They had a small (500gpd) RO built into the display. We serviced the ROs and UV sterilizers for them. The laser isn't too particular about it's water, while I wouldn't want chlorine or any of the commonly added tap water chemicals, good clean, soft water will do you just fine. You even can get away with a little antifreeze in cold environments (I used a minimum of pure polypropylene glycol last winter just in case the shop lost heat.)



Susan,

Bleach isn't a great idea, I've been contemplating using a UV sterilizer made for aquariums, they're under 30 bucks and ought o work well at keeping the bugs at bay.

Given the choice of chemical solutions, I'd choose alcohol, (ethanol) or isopropyl) 2 or 3% ought to do it. Cheap Vodka would be an option if you don't have access to reasonably priced commercial stuff.

Another option could be a dash of the  bactericide made for water soluble cutting oil at a minimum dose. Available at machine shop supply or (of course) ebay.



The short answer is RO water is fine, maybe better, (like  lot of things) with a shot of Vodka in it.



 (as usual, MORE than you wanted to know).



Scott


---
**Pete Sobye** *June 21, 2016 11:10*

**+Scott Marshall** another comprehensive reply. Thanks Scott. Like Susan, my water is feeling slimy so need to change out tonight. 

Can't wait for my new head and mirrors/lens to come. I am currently cutting 3mm acrylic and struggling at 95-100% at 2mm speed!! Another question coming about the mirrors. 


---
**Phillip Conroy** *June 21, 2016 11:20*

I use car radator fluid ,cheap green ,looks cool going through the pipes,and laser when i scape off the mdf sticky gunk that has complety covered the tube.sill works and if it is not broken do not f_ck with it..


---
**Scott Marshall** *June 21, 2016 13:55*

**+Pete Sobye** Yes sir, you've got something, probably a few things out of adjustment there. 3mm Acrylic cuts just slightly harder than paper, you should be sailing thru it. I can cut 1/4"baltic  birch ply in 1 pass at those settings. 5-10ma max and 200mm/sec will cut 3mm acrylic if you're dialed in. You're beating up that laser tube for nothing. A common mistake is getting the lens in upside down, and it will really kill the power, as will being a millimeter or 2 off on focal distance. 



The new head will help with the fine work, and the install will amount to a crash course in the proper setup of the laser. It will seem daunting and you'll wonder if it's worth it. It is. In a couple months, you'll be the one answering questions here. Most people that don't give up find out thet once you actually DO the work, it's quickly committed to memory, and you;ll find yourself doing a quick mirror alignment every time you clean it or set up for a large job.



Keep at it.



Scott


---
**Boris Camelo** *September 21, 2017 15:26*

How often do I need to change the demineralised water?




---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/UXqN8ihmxea) &mdash; content and formatting may not be reliable*
