---
layout: post
title: "Started my first real design with my K40-S and LW"
date: January 04, 2017 21:37
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Started my first real design with my K40-S and LW.



Everything is exactly 2x the designs size?



While I am researching what I have set up wrong, suggestions are welcome.





**"Don Kleinschnitz Jr."**

---
---
**Ned Hill** *January 04, 2017 22:18*

I've only just started looking at LW since I'm waiting on my C3D board to upgrade, but I seem to recall reading that scaling is dependent on the PPI setting in LW.


---
**Ned Hill** *January 04, 2017 22:30*

Sorry seems to be DPI.  See this post from the Laser web group.   [plus.google.com - I created an SVG in inkscape which shows the correct dimensions, when I open in…](https://plus.google.com/105967656691766312361/posts/PHvuTyeFCpd)


---
**Don Kleinschnitz Jr.** *January 04, 2017 23:32*

**+Ned Hill** This is a .dxf file loaded not a picture?


---
**Ned Hill** *January 04, 2017 23:36*

Not sure about that.  Haven't gotten that far with LW personally.  


---
**Don Kleinschnitz Jr.** *January 05, 2017 01:31*

Thanks **+Ned Hill**. 

**+Ray Kholodovsky** saved me lots of time...my alpha & beta steps/mm was set to 315.15 ..... 2x what is should have been. Another novice misteak in config file fixed.

We should have a common place to share K40 config files.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/ZB2qWWbFzWf) &mdash; content and formatting may not be reliable*
