---
layout: post
title: "I can't seem to find the link that someone supplied a week or so ago to the metal air assist head and appropriate focal lens for it"
date: May 13, 2016 04:23
category: "Air Assist"
author: "Alex Krause"
---
I can't seem to find the link that someone supplied a week or so ago to the metal air assist head and appropriate focal lens for it. Can someone supply a link of a unit proven to work





**"Alex Krause"**

---
---
**Custom Creations** *May 13, 2016 04:55*

Metal air assist head

[http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)



Focusing lens

[http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F508mm-P670.aspx](http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F508mm-P670.aspx)


---
**Jim Hatch** *May 13, 2016 12:43*

You might also be referring to the Ebay Saite_Cutter head ([http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050?hash=item3abe742d92:g:kagAAOSwIrNWFIY7](http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050?hash=item3abe742d92:g:kagAAOSwIrNWFIY7)) 



I have both but ended up using the LO one (no real reason except I got the lens & mirror from them so figured I wanted it all from the same folks if I had a problem). The Saite has the advantage of being height adjustable though.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/3B8dqK9NLwv) &mdash; content and formatting may not be reliable*
