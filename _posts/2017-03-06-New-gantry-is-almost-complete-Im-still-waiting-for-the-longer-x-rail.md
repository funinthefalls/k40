---
layout: post
title: "New gantry is almost complete. I'm still waiting for the longer x rail"
date: March 06, 2017 21:36
category: "Modification"
author: "Mircea Russu"
---
New gantry is almost complete. I'm still waiting for the longer x rail. 

Should get a few more cm out of it at 40-45cm/27-28cm, not quite 30/50 but close enough for such a small envelope and half the price of the G350. I still intend to keep all the electronics inside the bluebox and just move the separator wall to the right by a few cm.

New 12V steppers, should take the load off the poor LPS. Take note the latest edge smoothieware has some issues with homing, it hangs the board. Using latest master branch and new config style all works great. Hope to see it inside the box soon.



![images/57c76169bc2558fb416a0ada5384a3a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57c76169bc2558fb416a0ada5384a3a6.jpeg)
![images/da6acd9f5cae5cd22c36230f604eb8f7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/da6acd9f5cae5cd22c36230f604eb8f7.jpeg)

**"Mircea Russu"**

---
---
**Jim Fong** *March 06, 2017 22:48*

Looks great. Very similar to what I was building.   "The Great Lighting Strike of 2017" killed my Copley brushless servo drivers I was going to use. A big set back.  Have to figure out what to do now.  


---
**Mark Brown** *March 07, 2017 00:15*

Looks good.  Where did you get the rail?



Jim, did the lightning strike kill the motors you had sitting on the shelf?


---
**Jim Fong** *March 07, 2017 00:59*

**+Twelve Foot** I haven't had the chance to test any of the spare servo and stepper motors. My guess is that they are fine however the lightening did kill one encoder on my mill. Just don't know until I test them. 



I lost a bunch of servo drives on the 4 cnc machines.  The 3 Copley servo drives were just sitting on the shelf.  They were not hooked up.  They don't power up with no status lights.  Bummer since they are really expensive.  



I even have micro controller boards that don't power up any more.  Raspberry PI, Arduino, ARM etc that were sitting on my desk.  I have dozens of them that need to tested.   That is how powerful this lightening strike that hit my house!!!   Major EMP pulse literary destroyed electronics that were not plugged in.  Several ESP8266 boards set off the overload limits on my TEK bench PS when plugged in.  I haven't tested the ones still in the static bags or the dozens of chips. 



The arduinos that I did test gave flash verification errors when I try to program them. 



Lost one Applied Motion STM17 that I was using on the corexy 3d printer I was building.  To expensive to buy another one so I have to use regular stepper motors now.  



Bad luck....



Oh ya, one laptop I thought survived un damaged however none of the USB ports work when I plugged in a flash drive.  






---
**Don Kleinschnitz Jr.** *March 07, 2017 01:24*

Did you by any chance share the model on the warehouse.?






---
**Mircea Russu** *March 07, 2017 08:39*

[aliexpress.com - MGN MGW series - Wholesale products with online transaction](https://www.aliexpress.com/store/group/MGN-MGW-series/1452393_259128619.html?spm=2114.12010608.0.0.8bcPd0) 

This are the rails, they are buttery smooth after some good greasing. Still waiting for the longer X one.

**+Jim Fong** sorry about your missfortune. natural HV is not something to mess with...

**+Don Kleinschnitz** I shall publish the files after I get it working, on the table the moves are perfect, but only laser can tell if I get missed steps and only time will tell if my PLA parts start to crack. I used PLA for more rigidity, using ABS required twice as much material and a heated chamber for the printer.


---
**Don Kleinschnitz Jr.** *March 07, 2017 14:45*

**+Mircea Russu** did you know that you can upload A SU file to the warehouse and share then just keep it refreshed as you make changes. That way we can track your progress? :)




---
**Mircea Russu** *March 07, 2017 20:51*

**+Don Kleinschnitz** just search the warehouse for "k40 laser gantry" you should see it.


---
**Don Kleinschnitz Jr.** *March 07, 2017 21:06*

**+Mircea Russu** ok, BTW you can search for K40 and see my optical layout and other K40 stuff.


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/JaxavDDBuTZ) &mdash; content and formatting may not be reliable*
