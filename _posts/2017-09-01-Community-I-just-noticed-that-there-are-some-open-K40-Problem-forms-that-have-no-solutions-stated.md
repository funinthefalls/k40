---
layout: post
title: "Community I just noticed that there are some open K40 Problem forms that have no solutions stated"
date: September 01, 2017 12:12
category: "Discussion"
author: "Don Kleinschnitz Jr."
---


Community I just noticed that there are some open K40 Problem forms that have no solutions stated.



If your problem has been solved can you please fill in the last section that tells us how it was solved? We are hoping to create a log of problems and solutions that will help others with similar problems. These forms also allow us to collect some statistics on problems.



I have a K40 problem form for a "Brandon D". That name does not resolve on G+.

Brandon, did your problem get solved?

[https://docs.google.com/forms/d/1MobwxZ8COA6bW_0utAhYLYkoiEJZQERNs5wLi_fXgsY/edit#response=ACYDBNh0a5iDPXwhEi7Zsf70MbvQ657LazKIuTfKVLttzzW8LOlNzUvIzRl_Dw](https://docs.google.com/forms/d/1MobwxZ8COA6bW_0utAhYLYkoiEJZQERNs5wLi_fXgsY/edit#response=ACYDBNh0a5iDPXwhEi7Zsf70MbvQ657LazKIuTfKVLttzzW8LOlNzUvIzRl_Dw)



**+Jerry Hartman** did your problem get solved?

[https://docs.google.com/forms/d/1MobwxZ8COA6bW_0utAhYLYkoiEJZQERNs5wLi_fXgsY/edit#response=ACYDBNhwu93HnrsNqhaFqR7bsUvHyHFUteBrgEJLbfFVwQ7CWYwzqv5-hUTPGw](https://docs.google.com/forms/d/1MobwxZ8COA6bW_0utAhYLYkoiEJZQERNs5wLi_fXgsY/edit#response=ACYDBNhwu93HnrsNqhaFqR7bsUvHyHFUteBrgEJLbfFVwQ7CWYwzqv5-hUTPGw)





**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/9A2Bm76i1kM) &mdash; content and formatting may not be reliable*
