---
layout: post
title: "Hello, I have 2 problems and don't know if someone can help me"
date: December 06, 2018 14:45
category: "Original software and hardware issues"
author: "Guido Garzia"
---
Hello,  I have 2 problems and don't know if someone can help me.

This problem came after some weeks of use.



1) The laser power is not the same like you can see on the foto , a small space give a really different result 



2) The engrave and the cut are not allineed using CorelLaser , before I never had this problem, when I start with 2 object one near the other start the problem.

I use also the ancor pixel , but never change



![images/a66759dea6ee05a3ccdc24a4c2dca1f5.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a66759dea6ee05a3ccdc24a4c2dca1f5.png)



**"Guido Garzia"**

---
---
**Guido Garzia** *December 06, 2018 15:26*

I have done a try when check the allineament of the glasses.



If I'm in 0,0 i'm correctly in the center , if I'm in 200/200 I'm 3mm moved in right/down


---
**Don Kleinschnitz Jr.** *December 06, 2018 16:18*

**+Guido Garzia** It sounds like you need an optical alignment. Your beam is not travelling parallel to the gantry's axis and therefore it is on a different place on the mirrors depending where you are in the x/y workspace.


---
**Guido Garzia** *December 06, 2018 16:59*

I solved the problem of quality with the alignament, but I can't still cut and engrave on the same job.



Down you can see a photo in the right a job with a pixel anchor , on the right without any pixel anchor.  Before I can print centred  without use any anchor.



I have unistall and reinstall Coreldraw X5 , I can't try with LaserDraw because can't do the same job I usually do with CorelLaser.



There is some other software I can try?

![images/5dd7e759a43f4f1838b3c0e4aaabf654.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5dd7e759a43f4f1838b3c0e4aaabf654.jpeg)


---
**James Rivera** *December 06, 2018 19:24*

**+Guido Garzia** Ditch that hacked version of Corel. Get K40 Whisperer! 

[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
*Imported from [Google+](https://plus.google.com/116903442422049333652/posts/5VUQFu3TGzr) &mdash; content and formatting may not be reliable*
