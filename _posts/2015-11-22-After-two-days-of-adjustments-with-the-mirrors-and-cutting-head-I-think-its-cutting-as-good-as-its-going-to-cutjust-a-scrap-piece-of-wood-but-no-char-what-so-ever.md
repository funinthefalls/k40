---
layout: post
title: "After two days of adjustments with the mirrors and cutting head, I think it's cutting as good as it's going to cut...just a scrap piece of wood but no char what so ever."
date: November 22, 2015 21:52
category: "Object produced with laser"
author: "Scott Thorne"
---
After two days of adjustments with the mirrors and cutting head, I think it's cutting as good as it's going to cut...just a scrap piece of wood but no char what so ever.﻿

![images/a3eed9900d1776d2d90c0ece022c4e55.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3eed9900d1776d2d90c0ece022c4e55.jpeg)



**"Scott Thorne"**

---
---
**Todd Miller** *November 22, 2015 22:35*

That's awesome ! I etched the same image last night on 3 mm ply, but you cut it out !  How thick of material ?


---
**Scott Thorne** *November 22, 2015 22:35*

3/16 of an inch


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 22, 2015 23:27*

That's awesome. Did you cut using 1 pass or multiple passes? Looks like maybe there is a bit that didn't pop out when you cut it too... the right eyebrow area?


---
**Scott Thorne** *November 22, 2015 23:29*

One pass, and yeah I just noticed that...Lol


---
**Stephane Buisson** *November 23, 2015 12:28*

3/16 inch = 4.7625 mm

very good setting indeed


---
**Scott Thorne** *November 23, 2015 21:09*

Thanks Stephanie...I'm a rookie, still trying to find out my sweet spot...lol


---
**DIY3DTECH.com** *November 24, 2015 01:09*

SO how does one avoid the charring?


---
**Scott Thorne** *November 24, 2015 01:12*

I seem to have avoided it by using more air pressure from my compressor, but I also realigned my mirrors and cutting head...that was done at 6 mA and 8 mm/s .


---
**DIY3DTECH.com** *November 24, 2015 01:21*

Right now I am pushing around 40PSI and running around 10mm/s.  Hmm guess I will have play with more to see what I can get out of it.


---
**Scott Thorne** *November 24, 2015 01:24*

I'm at 25 psi...you have the 18 mm lens correct?


---
**Scott Thorne** *November 24, 2015 01:28*

I actually took a q tip and cut it off at 2 inches and stuck it up the head until it touched the lens and made marks with a sharpie all around the rails do I would know where the top of my work piece would be...easy for me to set it up right that way.


---
**DIY3DTECH.com** *November 24, 2015 01:55*

This is a good idea as I measured down and made a jig to slide under the head to mark 5 cm.  However I am sure to be off by 1/32 or so.  Will try your trick and see how it works...  Thanks again...


---
**Scott Thorne** *November 24, 2015 10:06*

Sure thing....anytime my friend.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/Fi43tXCKpDz) &mdash; content and formatting may not be reliable*
