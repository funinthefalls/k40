---
layout: post
title: "Hey guys how are you all doing?"
date: August 11, 2016 00:04
category: "Discussion"
author: "Jose Castellon"
---
Hey guys how are you all doing?

Well if you've been keeping up with my nonsense the last two days I wanna say thank you and I'm sorry lol. 

I still didn't get my mirrors spot on yet but I figure with the small stuff I'm gonna be engraving it won't make a big difference. 



Been playing with the software and now that's what I need to learn. Mainly how to place the peice on the machine and align everything up right. 



Made this little pokeball just to see if I can land it on the precut circle. 



The image itself doesn't look so bad. 



Just thought I'd show you guys and see what you thought and to thank all of you for all your tips. 



Lets see more pictures of what you guys are making 😁

![images/4ecd544405c224f6e73b4977f8bf1c88.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4ecd544405c224f6e73b4977f8bf1c88.jpeg)



**"Jose Castellon"**

---
---
**Ariel Yahni (UniKpty)** *August 11, 2016 00:45*

**+Jose Castellon**​ are you cutting the circle also?  Or those come from somewhere else.  If the answer is yes then I don't understand the reason, if not then you need to make a sample jig. I would drow the circle on a piece and the place the circle on top


---
**Jose Castellon** *August 11, 2016 01:51*

No I found these precuts at Michael's just to mess around with them. First day really engraving stuff so learning as I go


---
**Ariel Yahni (UniKpty)** *August 11, 2016 01:57*

you are good man, just note that is either you measure it or get some laser pointer


---
**Harvey Benner** *August 11, 2016 02:56*

I tape a piece of scrap to the bed and cut out an alignment fixture. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 11, 2016 06:07*

Yeah, you should definitely make some jigs as Ariel suggests. Just cut a circle out of plywood (or a series of them to do multiple pieces at once) that fits the precuts in. Then you can drop them in & get the position correct every time.


---
*Imported from [Google+](https://plus.google.com/111728896423789579330/posts/HkTRyoXtsMW) &mdash; content and formatting may not be reliable*
