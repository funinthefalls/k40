---
layout: post
title: "Interesting change on my K40 ... I 3D print my nozzles and each time I print a new one, I make a small change, either a tighter fit, or longer/shorter height, reposition the air intake ..."
date: July 06, 2016 02:37
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Interesting change on my K40 ... I 3D print my nozzles and each time I print a new one, I make a small change, either a tighter fit, or longer/shorter height, reposition the air intake ... something. I had been cutting with one that has a 4mm exit hole and it was fine, I never had issues with it. Recently I printed a new one, same shape, about 4mm shorter, but the most noticeable change was the exit hole which I narrows from 4mm down to just over 1mm in size. I get more air pressure out of it, great.



So I have a piece that I made a few times now that consists of several pieces that friction fit together. Never had issues, I know what the kerf was, and I've used the exact same files to cut each time. A few days ago I cut the whole thing again, this time using the new nozzle. Lo and behold the thing is cutting extremely tight. As in, the measured kerf is <b>smaller</b> than with the old nozzle. The cuts are also very clean, no black soot left behind. For grins I put the old nozzle back on and sure enough, the kerf is wider. Now keep in mind, nothing else is changing, not the height of the focusing lens, not the height of the bed nor material. The only two changes is the smaller exit hole and 4mm shorter nozzle.



Which makes me wonder: if, because of the larger hole, I was not getting enough air pressure down the cut, it was causing it to burn just a bit longer and causing the kerf to be wider. I don't know, this is simply an observation of what the nozzle change is doing. Unfortunately I have to go adjust the files now and redo the offset by 0.05mm. :/





**"Ashley M. Kirchner [Norym]"**

---
---
**Scott Marshall** *July 06, 2016 03:19*

Very useful info. I've been wondering which works better, a high volume flood or a concentrated 'beam'.



I guess we know now. Good news for people with low volume air pumps.



Scott


---
**Ashley M. Kirchner [Norym]** *July 06, 2016 03:21*

And that's the other bit, I have one of those airbrush pumps. It doesn't generate enough pressure that it builds up, it just dumps it all back out (into the laser.) So yeah, with the smaller exit hole, the air pressure is certainly higher coming out.


---
**Pippins McGee** *July 06, 2016 03:42*

are you willing to print and sell a nozzle that fits on the LO laser head?

I need more concentrated air beam onto surface..

I too have airbrush pump and the measly puff of air it produces is enough to move smoke away from the lens but nowhere near enough to have an impact on the quality of the cut


---
**Scott Marshall** *July 06, 2016 03:47*

That's really quite good news for a lot of people looking to add air assist.



 It means they don't need a big compressor or pump, that a moderate sized pump or, like you have - an airbrush compressor  will do the job. 



I'll have to try closing my nozzle down. I've got unlimited air so I just turn it up until it keeps the wood surface pretty clean, but even way up, I've never experienced what you describe, which is pretty much the holy grail of laser cutting (in my opinion). Clean, narrow kerf with dead square edges. On small air no less! What else can you ask for?



That's a pretty big find.



I've been developing an air assist  controller kit for the K40, and it's limited to about 6 amps at 120V, so I was concerned there may be a lot of people wanting to control a larger compressor. Good to know that most people won't need a big nasty pump. It has a solenoid valve option for shop air, but I hadn't quite worked out a easy to wire solution for big pumps or compressors.


---
**Ashley M. Kirchner [Norym]** *July 06, 2016 04:12*

Yep, a smaller nozzle hole certainly helps. My bigger problem now is that my beam isn't straight down, it's at an angle, slightly forward. I know why, but not sure how to fix it. Some more tinkering is needed.


---
**Alex Krause** *July 06, 2016 04:14*

If you have a larger openening on your air assist head on a low volume pump try this little hack... put a piece of very sticky tape over the beam exit and fire a quick test fire... now start your air up and see if it helps... in my head I see a smaller beam exit requiring a more precise beam alignment 


---
**Stephane Buisson** *July 06, 2016 07:33*

I do confirm this point, my Air assist have 2 small outputs (needle size) and low pressure input, and I am happy with it.

one vertical stream from the center (lens holder)to clear the way, the second with from aside to focal point to push the smoke away.


---
**Ashley M. Kirchner [Norym]** *July 06, 2016 16:28*

I find I don't need to 'push the smoke' anywhere as it's already getting blown down, through the cut. I used to have a lot of smoke linger above the piece with the old nozzle, it was blowing it around in the cabinet. With the new nozzle and smaller exit hole, I barely see any smoke above the table anymore, it's all getting blown down and sucked out. In fact, the amount of smoke being blown out the window has caused a bit of an alarm with my neighbors, one of them knocked on my door to ask whether I knew that 'something was on fire'. Then I showed him the laser.


---
**David Schick** *July 14, 2016 01:58*

**+Scott Marshall**  Thanks Scott. Any chance you have the STL on thingaverse?


---
**Scott Marshall** *July 14, 2016 09:35*

**+David Schick** If you mean the Air assist controller, no. I'll have it for sale  as finished kits (solder free) , U build it kits (pcb & components), and I will be releasing info on how to build your own. All within the next month or so.



I've joined several of those techie sharing sites, only to find the 'sharing' is a very confusing affair, to use and to contribute to. Concenquently, I don't really use any of them. If I ever have time to spend learning them, I may put some of my designs up. In the meantime, anything I produce will have the diagrams publicly posted here with instructions on how to make one.(for laser related stuff anyway).

Obviously, I'd like you to buy one, but I have enjoyed scratch and kitbuilding for many years (Was heartbroken when Heathkit sold to Zenith, then both evaporated (I was also an authorized Zenith Service Tech back when)

So, as I was saying, I understand a lot of people want a schematic so they can build their own, or a kit they can assemble and customize. I intend to provide all 3 options, Plug and Play, U Build kit (bare PCBs will be available quite reasonable) , and Free Schematics (posted exclusively Here, On the Google K40 Community).



Please look at my kits before you decide to scratch build, I think you'll find my kits actually cheaper then scratch building, and a whole lot less work.



Scott


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/QrTvmQCTTtH) &mdash; content and formatting may not be reliable*
