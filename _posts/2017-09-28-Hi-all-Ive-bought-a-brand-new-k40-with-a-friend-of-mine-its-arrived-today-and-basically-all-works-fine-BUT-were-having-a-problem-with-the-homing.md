---
layout: post
title: "Hi all! I've bought a brand new k40 with a friend of mine, it's arrived today and basically all works fine BUT we're having a problem with the homing"
date: September 28, 2017 18:58
category: "Original software and hardware issues"
author: "Fabio Bregolato"
---
Hi all!

I've bought a brand new k40 with a friend of mine, it's arrived today and basically all works fine BUT we're having a problem with the homing. Problem is that in manual mode the machine moves correctly, while in the auto homing the X axis moves to the right instead of left (basically it tries to  find the home position in the top right) but corellaser setting says that homing cycle is on top-left corner.



Did anyone knows how to solve that issue?

(Sorry for my English, I'm Italian)





**"Fabio Bregolato"**

---
---
**BEN 3D** *September 28, 2017 19:14*

My one is homing in the top left. So may plus and minus of the motor is wired in the wrong way. I will attach photos of my wiring so may you could compare it with yours.


---
**BEN 3D** *September 28, 2017 19:16*

![images/4aa1d9923ad0e0063bb7269b0f0b759c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4aa1d9923ad0e0063bb7269b0f0b759c.jpeg)


---
**BEN 3D** *September 28, 2017 19:17*

![images/acb6a52af78995f8250e7e17ee6b1f4e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/acb6a52af78995f8250e7e17ee6b1f4e.jpeg)


---
**BEN 3D** *September 28, 2017 19:17*

![images/20094c2b263959955b14ace5bdb31230.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/20094c2b263959955b14ace5bdb31230.jpeg)


---
**BEN 3D** *September 28, 2017 19:19*

Some side of my X Axis Kabel is labeld with text so you could see how it is orientated.


---
**Fabio Bregolato** *September 28, 2017 19:25*

**+BEN 3D** Hi Ben! Thanks 4 the reply!



It might be possible that the motor is wired in the wrong way, but when we move the machine with the manual mode in corellaser it moves in the correct way! Problem comes just only in the homing cycle. I've got a cnc router that works with grbl and there is a mask to invert the directions of homing cycle, so I think that there is a firmware problem with the k40 but we are unable to find the firmware 🤔


---
**BEN 3D** *September 28, 2017 19:34*

**+Fabio Bregolato** Don created a great collection of Informations to the on stock device, but I have not read some about the firmware. 

[donsthings.blogspot.de - Don's Things](http://donsthings.blogspot.de/search/label/K40%20Motors)



I am not firm with the software yet, so may there is a solution to switch the homing direction.


---
**Al Kardos** *October 05, 2017 11:02*

Did you get resolution? I had a similar problem, turned out the x sensor had torn away from the copper traces on the of board 



![images/57fe354c9edb7c0aee3f8b167798dd88.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57fe354c9edb7c0aee3f8b167798dd88.jpeg)


---
**Fabio Bregolato** *October 05, 2017 16:23*

**+Al Kardos** yes it was a sensor issue, it was broken. We've replaced it with a new one and now all works fine (for now)


---
**James poulton** *June 05, 2018 23:21*

hello, what sensor was it? im having similar problems..  


---
**Al Kardos** *June 06, 2018 00:01*

It was the x axis sensor. Soldered jumpers ftom the pins to the traces, also extended the metal that goes into the sensor by making a new one from scrap. Has been working fine ever since.


---
*Imported from [Google+](https://plus.google.com/106106894249986461080/posts/XAorAVoQTL7) &mdash; content and formatting may not be reliable*
