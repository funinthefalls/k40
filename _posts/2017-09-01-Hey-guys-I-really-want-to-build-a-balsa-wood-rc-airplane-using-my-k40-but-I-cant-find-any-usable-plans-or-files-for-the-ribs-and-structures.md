---
layout: post
title: "Hey guys, I really want to build a balsa wood rc airplane using my k40, but I can't find any usable plans or files for the ribs and structures"
date: September 01, 2017 20:10
category: "External links&#x3a; Blog, forum, etc"
author: "Tadas A\u0161"
---
Hey guys, I really want to build a balsa wood rc airplane using my k40, but I can't find any usable plans or files for the ribs and structures. Does anyone of you have a website or a file to share? Rc boats are welcome too.





**"Tadas A\u0161"**

---
---
**อมรชัย ธนธันยบูรณ์** *September 01, 2017 20:15*

[devcad.com - Page Redirection](http://www.devcad.com)

more....


---
**greg greene** *September 01, 2017 20:22*

Have you checked out RCUNIVERSE?


---
**Tadas Aš** *September 01, 2017 20:26*

Not yet, thanks guys


---
**Jim Fong** *September 01, 2017 21:47*

[https://www.rcgroups.com/forums/index.php](https://www.rcgroups.com/forums/index.php)


---
**Tadas Aš** *September 01, 2017 21:50*

**+Jim Fong** thanks!


---
**Josh Oxborrow** *September 02, 2017 00:26*

There is a group that does rc planes using foam core that has free plans online you might wanna check out. Check out flitetest on YouTube. 


---
**Tadas Aš** *September 02, 2017 06:23*

I know those guys and I love their work. Usually their kits don't fit in my k40 as far as I know


---
*Imported from [Google+](https://plus.google.com/105123664639790704570/posts/DPPXEwFLbXP) &mdash; content and formatting may not be reliable*
