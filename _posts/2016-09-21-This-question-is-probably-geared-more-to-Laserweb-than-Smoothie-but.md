---
layout: post
title: "This question is probably geared more to Laserweb than Smoothie but..."
date: September 21, 2016 21:53
category: "Software"
author: "Bruce Golling"
---
This question is probably geared more to Laserweb than Smoothie but...

I've got a smoothieboard installed and its talking to laserweb seemingly fine (at least for what I'm doing), but I only know how to place a picture in either the top left or bottom left to begin.  I wonder how I can set the program to start at an arbitrary point - say x+15 and y-15 rather than x0 y0 or whatever the top left coordinates are.  







**"Bruce Golling"**

---


---
*Imported from [Google+](https://plus.google.com/105808932307807066174/posts/QNrBatRxZd1) &mdash; content and formatting may not be reliable*
