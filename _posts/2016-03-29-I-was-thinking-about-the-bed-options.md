---
layout: post
title: "I was thinking about the bed options"
date: March 29, 2016 19:37
category: "Modification"
author: "Mishko Mishko"
---






I was thinking about the bed options. I prefer moving the lens and not the bed for more than one reason, so if I find a good solution for the lens, I'll probably make something  like the one on the image, from 0.6mm galvanized sheet metal. 



DXF for supports has a perforated bend line, so they can be bent by hand.



If anyone is interested , the files are available here:

[https://drive.google.com/file/d/0B7vzLJsBPaNvdDU4aWtReHFSVVk/view?usp=sharing](https://drive.google.com/file/d/0B7vzLJsBPaNvdDU4aWtReHFSVVk/view?usp=sharing)



NOTE: I'm not at home, so I couldn't take any measurements, the dimensions may have to be altered! I've just picked some round numbers,  310X310X75 mm...



![images/67f7d376757e36890068535abf4ade9a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/67f7d376757e36890068535abf4ade9a.png)



**"Mishko Mishko"**

---
---
**Coherent** *March 30, 2016 02:29*

It reminds me of a bed I have on a large 4x8 foot Plasma cutting table. Exact same concept, but the slats are flat on top on the plasma cutting machine. I would think it would work great as long as you could raise and lower it to accommodate for different material thickness. Another option would be to have different height slats for the same purpose. On the plasma cutter, the torch raises and lowers instead for warping etc as it cuts.  Whats nice is you can replace or rotate/move the slats as needed.

 


---
**Mishko Mishko** *March 30, 2016 08:36*

Yes, most big lasers I've seen have this system. For waterjet I don't think it matters much if the slates are flat or toothed, but when the laser cuts across the slates, the molten metal sprays back to the bottom of the plate if they are flat.I don't know about acrylic and wood, but I guess they could also be marked at the intersection, if not, then the teeth probably don't make much sense.

I've also seen some lasers in the range of up to 100W, Trotec for instance, with the same system, but with flat slates.



Maybe someone with a honeycomb bed can tell if this is a case or not?



As I said, I'd only make this type of bed if the lens can be moved up and down to focus and compensate for the material thickness, so the bed can stay at the fixed height at all times, and the slates removed partially or completely for higher objects. But if the bed has to travel up and down, I guess I'd have to go with something thinner, most probably the aluminum expanded mesh, preferably flattened, like this one: [http://www.aluminum-mesh.com/img/flat-expanded-metal-sheet.jpg](http://www.aluminum-mesh.com/img/flat-expanded-metal-sheet.jpg) , to enable as much vertical movement as possible...



I'm also interested if anyone tried using laser pointer beam for focusing? I think if I aimed the beam at the second mirror from the direction of the first one, that should produce the point defined enough at the material surface, then moving the lens or the bed vertically until I get the sharpest image should do the trick? I would try it if I could only find the darn pointer;) I would do this for calibration only, then make some sort of stepped gauge for different material thicknesses...


---
**Phillip Conroy** *March 31, 2016 22:26*

what are you cutting /etching- i use magnets and 65mm long screws and only need 6 to hold my 3mm mdf up-easy cheap movable- min contact to work -works like a pin bed but only need 6


---
**Mishko Mishko** *April 01, 2016 02:45*

I only had two or three days so far to play with this laser, still testing different setup options, power and speed setting, stuff like that. But I know I will mostly cut acrylic, and even that just occasionally when I need to make something quickly. In my products, I do use acrylic and metal (anodized aluminum mostly, sometimes stainless steel), but I outsource all of them. So Laser, small router and retrofitted (also small) Cortini mill are all only used to make some prototypes. And to play, learn different softwares etc...

I've only made a temporary air assist nozzle, and because I don't have the air installation where the laser is currently located, I am using a small 6V pump, it is of course more a joke than a pump, but if nothing else it somehow keeps the flames at bay. It's a 6V pump, 15L/min, I'll post an image later.

I was having trouble to engrave anything at all from the very start, until I found out the wrong board was selected, didn't even know that option existed. So I managed to make a microminiature Stratocaster;)

Magnets are a good idea, I was also thinking about using them with the toothed model above. 

I'll try to make some kind of focusing mechanism for the lens tomorrow...


---
*Imported from [Google+](https://plus.google.com/114169453703152974799/posts/Xa8Ljincyjw) &mdash; content and formatting may not be reliable*
