---
layout: post
title: "One of these setups should get me going finally"
date: January 16, 2016 00:36
category: "Hardware and Laser settings"
author: "David Cook"
---
One of these setups should get me going finally. 

![images/f099a570ce5e7119066eb7ec56564488.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f099a570ce5e7119066eb7ec56564488.jpeg)



**"David Cook"**

---
---
**ChiRag Chaudhari** *January 16, 2016 01:25*

Are you gonna go with Turnkey moded Marlin or Repeiter or grbl ? 


---
**Joe Spanier** *January 16, 2016 01:29*

Personally I would use the Azteeg over the ramps. Your firmware is the same but the board is just built better


---
**ChiRag Chaudhari** *January 16, 2016 01:41*

Yeah that's for sure, but I think his Azteeg was fried up or something if I remember. I am actually waiting for the smoothiebrainz.


---
**David Cook** *January 16, 2016 02:13*

Yes it was my Azteeg X5 that fried.  The X3 is new.  I agree , it is built much better than the Ramps.


---
**ChiRag Chaudhari** *January 16, 2016 02:24*

Oh so tat Azteeg X3 is new! Then yeah definitely go for it. Someone will definitely buy it from you here.  


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/cwqkeg2dqqP) &mdash; content and formatting may not be reliable*
