---
layout: post
title: "I am going to replace mirrors in my K40"
date: May 17, 2016 09:07
category: "Discussion"
author: "Sebastian Szafran"
---
I am going to replace mirrors in my K40. I found the following mirrors available:

20X3mm Mo Molybdenum Reflection Mirror For CO2 Laser Cutter Engraver: [http://www.banggood.com/20X3mm-Mo-Molybdenum-Reflection-Mirror-For-CO2-Laser-Cutter-Engraver-p-966219.html?utm_campaign=android-share&utm_source=androidApp&android_share=1&_branch_match_id=261042582605385078](http://www.banggood.com/20X3mm-Mo-Molybdenum-Reflection-Mirror-For-CO2-Laser-Cutter-Engraver-p-966219.html?utm_campaign=android-share&utm_source=androidApp&android_share=1&_branch_match_id=261042582605385078)

and

20X3mm Si Silicon Reflection Mirror For CO2 Laser Cutter Engraver: [http://www.banggood.com/20X3mm-Si-Silicon-Reflection-Mirror-For-CO2-Laser-Cutter-Engraver-p-966218.html?utm_campaign=android-share&utm_source=androidApp&android_share=1&_branch_match_id=261042582605385078](http://www.banggood.com/20X3mm-Si-Silicon-Reflection-Mirror-For-CO2-Laser-Cutter-Engraver-p-966218.html?utm_campaign=android-share&utm_source=androidApp&android_share=1&_branch_match_id=261042582605385078)



Which one is better to equip the K40?









**"Sebastian Szafran"**

---
---
**Anthony Bolgar** *May 17, 2016 09:24*

The MO mirrors are better quality and have a higher reflective index for infrared light. So if you can afford it, go with the MO mirrors. And be carefull of the way they word the ad, 20X3 does not mead 3 20mm mirrors, it means the mirror is 20mm diameterX3mm thick.


---
**Sebastian Szafran** *May 17, 2016 10:18*

Thanks **+Anthony Bolgar**​, I ordered 3 pcs. and it looks Molybdenum mirrors are less expensive than Silicone. 


---
**Anthony Bolgar** *May 17, 2016 12:51*

The Si mirrors on Banggood are not real SI plated mirrors. They are SI substrate, gold plated. If you look at lightobjects, the quality is from worse to best GoldPlated (What Bangood is calling SI), SI plated, and MO. So Banggood is trying to pass off a cheap mirror as the more expensive one. May just be a language barrier that made their mistake, generally they are very fair and honest.


---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/PaZr5E8NVWh) &mdash; content and formatting may not be reliable*
