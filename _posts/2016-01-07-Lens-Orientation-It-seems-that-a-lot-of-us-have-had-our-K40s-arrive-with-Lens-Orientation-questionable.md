---
layout: post
title: "Lens Orientation: It seems that a lot of us have had our K40's arrive with Lens Orientation questionable"
date: January 07, 2016 23:45
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Lens Orientation:



It seems that a lot of us have had our K40's arrive with Lens Orientation questionable.



So, basically here is a diagram I have drawn up to show what happens for both possible orientations (well, I guess you could also have it sideways, which would be no fun at all).



Anyway, on the left, Concave side of the lens facing down will result in a focused laser beam. This is pretty much what we want for cutting. If it won't cut, then probably you need to check to make sure your lens is concave facing down.



On the right, Convex side of the lens facing down will result in a dispersed laser beam. This, could actually be good in some circumstances (e.g. I have issues with managing to engrave thin leather without burning all the way through it; if I turned my lens to this orientation the power is a lot weaker & maybe won't burn all the way through the leather).



So, for anyone new out there, here is what you want to know.



[https://drive.google.com/file/d/0Bzi2h1k_udXwZy1CREpKeG9NeGc/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZy1CREpKeG9NeGc/view?usp=sharing)





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**ChiRag Chaudhari** *January 08, 2016 01:07*

Nice **+Yuusuf Sallahuddin**​​ . next up air assist. Once there is enough information posted on various issues we can make a full proof PDF and make a sticky.﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 08, 2016 05:29*

**+Chirag Chaudhari** That's a good idea to sticky it. I think Stephane already has a few things posted into a PDF for assisting with setup/etc.


---
**Joel Kunze** *January 13, 2016 00:58*

The diagrams and description of what happens when the lens is flipped are wrong. Regardless of the orientation, the focal length is the same and the lens remains positive/converging. 


---
**ChiRag Chaudhari** *January 13, 2016 02:48*

**+Joel Kunze** It may not be technically or say optically correct image representation of what happens to the beam with different orientation of the lens but we all have experienced the exact same thing. Flipping the lens convex side up results in deeper cuts without increasing laser power. 



Think the the image on the right side with the convex side down should be combination of both image, meaning you do get focused beam at same focal length but at the same time some of the beam is dispersed thus less cutting power.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 13, 2016 08:02*

**+Joel Kunze** If, as you say, the lens does the same thing either way, what is the point of it at all? All I know is that when I removed it (the lens) the laser beam burned a hole ~5mm in size. When I placed it oriented with convex facing down, it was a much finer beam, however it could not cut through 2mm leather without about 10 passes @ 10mA. Then, I flip it so concave faces down & I can cut through the same leather with ease, 1 pass @ 7mA. So my diagram, although maybe not "to scale" or a perfect representation of what is happening, was to show roughly what was happening (in my opinion), that is... that the beam was being dispersed when oriented one way & being focused when oriented the other way. If you are aware of what is a more correct representation, it would be appreciated if you share it with us (to assist all of us & any new people with managing our lens orientation for optimal performance).


---
**Joel Kunze** *January 14, 2016 03:39*

The point of the lens is to focus the beam to a smaller diameter. It will do that no matter which side the light enters from. This is very old and well documented technology.



The reason you see poorer cutting performance with the wrong side up is because the focal point is shifted further down and at the surface of your work the beam is not as small as it was with the lens right side up. Additionally there is a slight increase to the minimum diameter of the beam due to increased aberrations. If your work surface is moved further away/down to compensate for the focal plane shift, the performance would improve to near what it is when the lens is convex side up.



The light is not "dispersed" as described and illustrated.



I'm not attacking your attempt to help show the right orientation, just trying to let you know that the explanation of "why" isn't accurate. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 14, 2016 06:04*

**+Joel Kunze** Thanks for sharing that Joel. I didn't think you were attacking my explanation, I was just curious what was the correct information (since my explanation/diagram was just drawn out of my observation & imagination). So, based on this new information, the diagram should show both lens orientations as focusing the beam, albeit at different focal lengths? 


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/SKB2gqE8WmE) &mdash; content and formatting may not be reliable*
