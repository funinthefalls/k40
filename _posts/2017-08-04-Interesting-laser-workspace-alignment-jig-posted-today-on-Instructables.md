---
layout: post
title: "Interesting laser workspace alignment jig posted today on Instructables"
date: August 04, 2017 17:46
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Interesting laser workspace alignment jig posted today on Instructables. ﻿



<b>Originally shared by Ariel Yahni (UniKpty)</b>



Laser alignment jig. Haven't tried it yet, if you do please share your experience with it.





**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 04, 2017 18:56*

Interesting read. Unfortunately it was not the type of alignment I originally thought it was going to be talking about, but still would be a handy jig to have.


---
**Brooke Hedrick** *August 05, 2017 06:06*

I fell into the same incorrect assumption.


---
**Ariel Yahni (UniKpty)** *August 05, 2017 11:56*

Is this any useful at all? If it is how would you change the title so it does not mislead?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2017 18:33*

I'd imagine it is useful. I'd just change title to more like "Aligning the workpiece" rather than "Aligning the laser".


---
**Don Kleinschnitz Jr.** *August 05, 2017 19:13*

I have a jig almost exactly like this but I use it to align the beam through the air assist nozzle. It also indexes to the side of the nozzle and also sets the FL.

I put a piece of tape on the base and shoot the beam to see if it hits true center. If it is offset from the calibrated  center I know the beam is coming through the nozzle at an angle. 



Travelling so no picture.



I now do the same measurement with my lift table by lowering and raising it and watch to see if the point where the beam hits wanders.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/PruiYeLfAuk) &mdash; content and formatting may not be reliable*
