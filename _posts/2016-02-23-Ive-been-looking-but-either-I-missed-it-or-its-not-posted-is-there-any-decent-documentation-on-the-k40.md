---
layout: post
title: "I've been looking but either I missed it or its not posted, is there any decent documentation on the k40"
date: February 23, 2016 07:30
category: "Discussion"
author: "Tony Sobczak"
---
I've been looking but either I missed it or its not posted,  is there any decent documentation on the k40.  Like what is the focal point of the machine,  how thick a piece can you engrave,  what that spring load opening in the middle of the table.  Any info would be appreciated and is the x7 controller from light object any good? 





**"Tony Sobczak"**

---
---
**Scott Marshall** *February 23, 2016 09:37*

Not in any 1 place (but here on this forum) that I've run across. 

There's a lot of info here, but the forum format makes it difficult to access.

There have been/are several versions of the machine known as the K40. 



Right now we are at the K40 mark 4, but there is a nearly identical machine with a digital tube current meter. Reports are that's it's NOT better than your basic K40 with the dial and analog Ma meter.



Mine cuts 1/8" plywood with 2 passes, I've heard of people doing it in 1 pass by slowing the head down to 7mm/s. 1/4" acrylic is no problem.



The K40 generally comes with LaserDRW and a obsolete but functional version of CorelDraw known as CorelLaser Version 12. A USB"security" key is provided and seems to work well. LaserDRW is basically a file output program with nearly zero drawing tools. It imports only .bmp files which cannot be edited except for scale. It saves files in a ".lyz" file, which are in a class of 1.

CorelLaser can be difficult to install Make sure you use a good file viewer or you won't see all the install files in the ".Rar" folder. It imports PDF, bmp and claims to import many others such as .dwg and .svg, but I can't get it to import .dwg files. I go from Autocad to to dxf or Draftsight to pdf, and it gets you there, albeit with a bit of cussing.



It Engraves well, deeply enough for any Image I've tried to reproduce. The driverware allows 3 settings, cut, Engrave. and Marking. Cut is a single line operation, and can be set to track using several options like "inside 1st", shortest path" etc.

Engraving uses a raster method, sweeping a software selectable pixel increment for each pass, and I believe it has a dithering feature I have not investigated. Works VERY well for a $400 machine.

Marking also uses the Raster setup, and is a raster version of cutting. 

Both raster operations are somewhat time consuming by nature.



The machine comes pretty basic, with very cheap, marginal accessories. If you plan on serious work, You;ll want to upgrade the exhaust fan, or at least remove the restrictive internal duct. The cooling pump is a simple aquarium pump and is servicable.

There are NO safety switches, interlocks or anything to keep you from hurting yourself with exposed laser energy, high voltage or exposed mains. 



The pump and fan plug in externally.



It comes with no air assist head, but an air assist head and larger lens can be had thru many sources like Light Object (kinda pricy in my opinion) or Saite Cutter on Ebay (fast and economical, Air assist head about $50 shipped to US).



While all this sounds bad, it's a lot of gear for the money, and most sellers seem to back it up to some degree. Shipping damage is the most common complaint.



This is really a "kit - assembled for shipping", and will suit the hobbiest/hacker just fine. If you expect a turnkey machine with support, this IS NOT it!



That's my 2 cent review. it's a decent starter machine if you're technically skilled or willing to become so. The people here are very helpful and are really the default support at this point.



The word on the street on the LightObject controller is good, but at that price point, you have a LOT of options, from a $20 Arduino/stepper driver setup to the $550 class controllers used in mainstream machines. The stock board isn't too bad really, with the firmware and it's secrecy being the worst points. It supports a much larger machine and features. The K40 gets you a 315mm  or so cutting widthe by 210 or so height. The factory "bed" is junk, but once removed, accomidates about 3 1/2" of working depth without cutting the floor out.



Have fun, and don't shoot your eye out kid!



Scott



If you decide to go for it, I can vouch for "GlobalFreeShipping" on Ebay. They also go by "Banyan Imports". I had severe shipping damage to my machine (NO fault of theirs) and they took care of it fast, fairly and without any delaying tactics. Several others here have also bought through them. (Total cost including shipping was about $380 including shipping to Upstate NY, USA)



Air assist Head and parts:

[http://www.ebay.com/itm/40W-Tube-CO2-Laser-Rubber-Stamp-Engraving-K40-3020-3040-Head-Integrative-Mount-/252273200569](http://www.ebay.com/itm/40W-Tube-CO2-Laser-Rubber-Stamp-Engraving-K40-3020-3040-Head-Integrative-Mount-/252273200569)



﻿


---
**Tony Sobczak** *February 23, 2016 15:38*

Thanks so much for this info. As a matter of fact global free shopping is where I bought my machine from. Great people. 


---
**Scott Marshall** *February 24, 2016 05:34*

They Are. Glad to help.


---
**G Ragonese** *February 24, 2016 22:55*

Hey Scott,  where in upstate are you?


---
**Scott Marshall** *February 25, 2016 08:37*

Elbridge, Just west of Syracuse. Weedsport Exit Tway


---
**G Ragonese** *February 25, 2016 08:44*

No kidding.  I'm in Syracuse! I just had my machine delivered last week and have been doing more reading up on the machine since this is a whole new world to me


---
**Scott Marshall** *February 29, 2016 04:52*

Good knowing somebody local doing the same sort of thing. May come in handy for us someday.



If you have any problems getting it working, drop me a line, and I'll help if I can.



have fun with it!

Scott






---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/1tv15hgeehX) &mdash; content and formatting may not be reliable*
