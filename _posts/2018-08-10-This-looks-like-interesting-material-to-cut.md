---
layout: post
title: "This looks like interesting material to cut"
date: August 10, 2018 12:15
category: "Material suppliers"
author: "Don Kleinschnitz Jr."
---
This looks like interesting material to cut.

I assume its safe?? (polyethylene foam (PE foam or PEF)).


{% include youtubePlayer.html id="a03NhkGzYK0" %}
[https://www.youtube.com/watch?v=a03NhkGzYK0&feature=push-sd&attr_tag=Q_d2ksH4LqzGGiyX%3A6](https://www.youtube.com/watch?v=a03NhkGzYK0&feature=push-sd&attr_tag=Q_d2ksH4LqzGGiyX%3A6)



When searching for same on amazon I found this:

[https://www.amazon.com/5S-SHADOW-ORGANIZERS-COLOR-CUSTOM/dp/B0797XDWVR/ref=pd_sbs_267_8?_encoding=UTF8&pd_rd_i=B0797XDWVR&pd_rd_r=596e2c13-9c96-11e8-af6e-0720243a5fcc&pd_rd_w=1OCAj&pd_rd_wg=UNsvs&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=f9e73f8b-e90f-46a6-8123-a09c49edb7a8&pf_rd_r=SD6BZD6RRWG9MH47A25Z&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=SD6BZD6RRWG9MH47A25Z#customerReviews](https://www.amazon.com/5S-SHADOW-ORGANIZERS-COLOR-CUSTOM/dp/B0797XDWVR/ref=pd_sbs_267_8?_encoding=UTF8&pd_rd_i=B0797XDWVR&pd_rd_r=596e2c13-9c96-11e8-af6e-0720243a5fcc&pd_rd_w=1OCAj&pd_rd_wg=UNsvs&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=f9e73f8b-e90f-46a6-8123-a09c49edb7a8&pf_rd_r=SD6BZD6RRWG9MH47A25Z&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=SD6BZD6RRWG9MH47A25Z#customerReviews)







**"Don Kleinschnitz Jr."**

---
---
**'Akai' Coit** *September 26, 2018 22:44*

From what I've read online, PE is generally unsafe to cut, but I'd guess with the right settings, it should be ok. I think what I mostly saw was about it melting and catching fire, but I'm going off memory at the moment, so don't quote me on that.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/SqsdX39T6R3) &mdash; content and formatting may not be reliable*
