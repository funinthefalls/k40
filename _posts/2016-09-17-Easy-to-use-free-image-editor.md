---
layout: post
title: "Easy to use free image editor"
date: September 17, 2016 03:51
category: "Software"
author: "Ariel Yahni (UniKpty)"
---
Easy to use free image editor [https://pixlr.com/web](https://pixlr.com/web)





**"Ariel Yahni (UniKpty)"**

---
---
**greg greene** *September 17, 2016 14:03*

GIMP is also a good free alternative


---
**James Rivera** *September 23, 2016 05:03*

GIMP is freaking amazing. If you haven't edited a photo with layers (as in, the way the Photoshop does things) you haven't experienced powerful photo editing. I've tried Pixlr. It is 100% meh. 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/BFPDB2RFefk) &mdash; content and formatting may not be reliable*
