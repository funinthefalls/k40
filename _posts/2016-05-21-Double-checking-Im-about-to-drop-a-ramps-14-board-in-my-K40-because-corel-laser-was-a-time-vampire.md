---
layout: post
title: "Double checking. Im about to drop a ramps 1.4 board in my K40 because corel laser was a time vampire"
date: May 21, 2016 07:45
category: "Discussion"
author: "Derek Schuetz"
---
Double checking. Im about to drop a ramps 1.4 board in my K40 because corel laser was a time vampire. Can you take a look at the pic and let me know if I have the pwr and pwm wires coming out I the right ports and if I am suppose to keep the original wires in there also (looks to be the laser fire and potentiometer wire) and if I'm suppose to remove the right most wire altogether.



Thank you 

![images/6b515b249d7f586f82bf4dd9349d2ff7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b515b249d7f586f82bf4dd9349d2ff7.jpeg)



**"Derek Schuetz"**

---
---
**Julia Longtin** *May 21, 2016 16:32*

Can't quite tell, but i'm performing this operation myself... please keep me updated!


---
**Derek Schuetz** *May 21, 2016 17:39*

Well I hooked it up to an old azteeg x3 I had and it all powered up but when I pressed the fire button everything just bogs down and loses power then troops up


---
**Carl Fisher** *May 21, 2016 21:10*

Same PS I have so I'm watching as well.




---
**Derek Schuetz** *May 21, 2016 22:51*

Ok well I have the ramps hooked up to motors and end stops no laser yet and my ends stops are not reading and if I hit home the x and y move slightly and stop which leads me to believe it thinks here already triggered. What I found weird was the end stops in the wiring diagram only have one wire which makes no sensr


---
**Alex Hodge** *May 22, 2016 02:18*

The one wire for the endstops is just the signal wire. You still need 5v/3.3v and ground.


---
**Derek Schuetz** *May 22, 2016 02:50*

Well there's a 5v and ground also but its going to a different area of the board


---
**Derek Schuetz** *May 22, 2016 02:52*

If you look at the wiring schematic it doesn't seem to make any sense 


---
**Derek Schuetz** *May 22, 2016 04:26*

**+Alex Hodge** could you explain the wiring of the end stops because I'm at a lost. The diagram shows only 1 wire for x and y going to the end stops and a 5v and ground going to aux 3 but that whole aux is taken up by the screen already


---
**Alex Hodge** *May 22, 2016 15:19*

What wiring diagram are you using?


---
**Derek Schuetz** *May 22, 2016 15:20*

**+Alex Hodge** [https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion](https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion)


---
**Derek Schuetz** *May 22, 2016 16:03*

And miraculously it just works nothing changed......now do I remove the pot and test fire button?


---
**Derek Schuetz** *May 22, 2016 18:33*

Removed all wires to pot and fire button and hooked up d5 and d6 and hit fire through ramps and nothing happens


---
**Derek Schuetz** *May 22, 2016 20:58*

All is functioning it seems will hopefully try some guide tonight to see if it can do that just took a lot of trial and error


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/E9A44CvryYV) &mdash; content and formatting may not be reliable*
