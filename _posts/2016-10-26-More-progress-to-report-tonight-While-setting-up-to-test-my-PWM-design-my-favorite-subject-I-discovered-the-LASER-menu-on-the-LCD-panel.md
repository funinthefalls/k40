---
layout: post
title: "More progress to report tonight. While setting up to test my PWM design (my favorite subject) I discovered the \"LASER\" menu on the LCD panel"
date: October 26, 2016 04:22
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
More progress to report tonight. 



While setting up to test my PWM design (my favorite subject) I discovered the "LASER" menu on the LCD panel. Specifically the "Test Fire".



BTW suddenly the LASER menu showed up? I was messing with laser configuration but don't know if I enabled something, anyway .....



I learned the hard way that setting the PWM pin in the configuration file does not include the "P" in the pin assignment field. 



I will post a sequence of pictures of getting access to the "Test Fire"  menu first in case you have not seen it. Very nice capability for stand alone functionality. 

Then I will post some scope pictures of the PWM working at 0-5V (without level shifting).



Note: the "Feed Hold" on the panel. No idea how that happened, what it is and how to get rid of it. Since that happened LW has commands queued and I do not know how to release the queue or UNHOLD the feed :)? 



Was successful tonight in sending commands to the smoothie from LW including messages to the LCD display.



![images/6c4fc94e1c3472f5f1bfae811b82b268.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c4fc94e1c3472f5f1bfae811b82b268.jpeg)
![images/6fc1ad5b352cc4ccfdfd257cb0d23dd1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6fc1ad5b352cc4ccfdfd257cb0d23dd1.jpeg)
![images/e028331c6d07caf61f13b80fa7b0b7ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e028331c6d07caf61f13b80fa7b0b7ae.jpeg)
![images/218217876b8a00989fc3eef7878420de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/218217876b8a00989fc3eef7878420de.jpeg)
![images/e18bd0bc35452814059bb0bd0f995581.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e18bd0bc35452814059bb0bd0f995581.jpeg)
![images/acc22627a537d93f98ead972abef9a2a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/acc22627a537d93f98ead972abef9a2a.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Don Kleinschnitz Jr.** *October 26, 2016 04:27*

The scope channel 1 was connected to the P2.4 output of the processor.

Channel 2 is the output of Q8 in open drain mode. Q8's drain is pulled up to smoothie 5v with a 1000 ohm resistor (same current at the LPS will need) for this test and not yet connected to the LPS. That's tomorrow ...  



Connections to channel 1

![images/406ca9c914b4fd15101ff032746319b5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/406ca9c914b4fd15101ff032746319b5.jpeg)


---
**Don Kleinschnitz Jr.** *October 26, 2016 04:27*

Connections to channel 2

![images/c05ab2ff684edf90b60e79f17253bdd7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c05ab2ff684edf90b60e79f17253bdd7.jpeg)


---
**Don Kleinschnitz Jr.** *October 26, 2016 04:29*

Top channel: PWM set at 50% showing input to Q8 (P2.4)

Bottom channel:  output from open drain. Note the open drain provides 5V to ground.

![images/5cb42e32f8683385f9e55ff189e688b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5cb42e32f8683385f9e55ff189e688b8.jpeg)


---
**Don Kleinschnitz Jr.** *October 26, 2016 04:31*

A table of measures. Note DF and period.

![images/eb7ec93759a86a17b19bca6a720bb948.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb7ec93759a86a17b19bca6a720bb948.jpeg)


---
**Don Kleinschnitz Jr.** *October 26, 2016 04:40*

**+Peter van der Walt** yah but had no idea it was this capable ....nice!


---
**Don Kleinschnitz Jr.** *October 26, 2016 04:58*

**+Wolfmanjm** we owe you a beer at least!


---
**Don Kleinschnitz Jr.** *October 26, 2016 05:36*

**+Peter van der Walt** link gives and error?


---
**Don Kleinschnitz Jr.** *October 26, 2016 05:45*

**+Don Kleinschnitz** that paypal link ....


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/a39CwktPFyB) &mdash; content and formatting may not be reliable*
