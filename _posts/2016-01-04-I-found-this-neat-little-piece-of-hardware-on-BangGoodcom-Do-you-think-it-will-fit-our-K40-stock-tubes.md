---
layout: post
title: "I found this neat little piece of hardware on BangGood.com Do you think it will fit our K40 stock tubes?"
date: January 04, 2016 15:49
category: "Hardware and Laser settings"
author: "Sean Cherven"
---
I found this neat little piece of hardware on BangGood.com

Do you think it will fit our K40 stock tubes?



[http://www.banggood.com/CO2-Laser-Tube-Support-Stand-Holder-Adjustable-50-80MM-p-965906.html](http://www.banggood.com/CO2-Laser-Tube-Support-Stand-Holder-Adjustable-50-80MM-p-965906.html)





**"Sean Cherven"**

---
---
**Stephane Buisson** *January 04, 2016 16:12*

60-90mm I think most 40W tube are 50mm you will need a way to center it.


---
**ChiRag Chaudhari** *January 04, 2016 16:14*

Nice find. But its a bit bigger. 


---
**Kirk Yarina** *January 04, 2016 19:02*

Here's one that claims 50 to 80mm:  [http://goo.gl/lW9Jr2](http://goo.gl/lW9Jr2)



Although it shouldn't be too hard to 3D print one


---
**Pedro Flores** *January 10, 2016 05:26*

you could try this [http://www.thingiverse.com/thing:420853](http://www.thingiverse.com/thing:420853) 


---
**Sean Cherven** *January 10, 2016 05:47*

I've already tried that, the base is too big to fit in the k40


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/BpRZzgYQ5it) &mdash; content and formatting may not be reliable*
