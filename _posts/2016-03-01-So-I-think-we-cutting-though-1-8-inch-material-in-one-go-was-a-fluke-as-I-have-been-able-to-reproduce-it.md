---
layout: post
title: "So I think we cutting though 1/8 inch material in one go was a fluke as I have been able to reproduce it"
date: March 01, 2016 02:40
category: "Discussion"
author: "3D Laser"
---
So I think we cutting though 1/8 inch material in one go was a fluke as I have been able to reproduce it. 🙁   I think my laser is a tad bit lout of alignment as it still isn't cutting evenly across the piece that an I am having a hard time finding the focal point this machine is frustrating 





**"3D Laser"**

---
---
**Phillip Conroy** *March 01, 2016 02:58*

7google how to align k40,can you place work where it cut fine,if no air aassist mirrors willneed cleaning and esecily focal lens bottom,even with a monster extraction fan and 10 psi of air assist i have to clean lenes every 3 hours of cutting 3mm mdf.



When machine is aligned and good clean mirrors and lfocal lens,i have a great cut ,very clean on both sides .



The focal lens has a focal point of 50mm ,which means you should try and get work peace at 50 mmfrom  lens,in my case 50mm plus 1.5mm however i likeit is where it is now as the botton of the cut is veryfine.

Remember that the laer beam is focused in the shape of an hour glass-wide spread then very  fine then it spreads




---
**Phillip Conroy** *March 01, 2016 03:09*

This link is well worth readinghttp://[www.parallax-tech.com/faq.htm](http://www.parallax-tech.com/faq.htm)    it explanes the laser beam very well,  it sounds like you need to align the laser mirrors to get the machine at least cutting fine over only 50% of the cutting area first ,than with more reasherch fine tune latter.

I had my laserworking great only over 50% of the cutting area for over 6 months as i mostlycut 10cm by 20cm 3 mm mdf which fitted just fine in the aligned area.

Do not forget your  laser tube my need rising or dropping one orboth ends. If the laser tube is out you will never get it aligned over 100% of cutting bed..........



I have had to rise my laser tube 2 times so far, i  just rapped a few layers of electrical tape onto itself and used it as a packer underneaf the right end of  tube.


---
**3D Laser** *March 01, 2016 03:41*

**+Phillip Conroy**  how do you clean your lens and mirrors


---
**Phillip Conroy** *March 01, 2016 03:59*

I just use a clean cotton bud dipped in alcohol ,wipe across lens/mirror than dry cotton bud wipe againn- even if lens/mirror looks clean most times it is not,shin a torch on it at diffrent angles to fine best view....mirror and lens on laser head getsthe most dirty,  focal lens goes with the concave side up/ flat side to work


---
**Jason Reiter** *March 01, 2016 14:04*

This instruction was great . I spent last night and got my alined almost perfect I couldnt beleive how close they were. It was great then you get to the last step where it says if the beam is high you can shim laser head up. If it is low adjust laser tube highth up and start over !!!!! grrrr. you can guess what mine was . So  I will be going at it again tonight . mine will cut one pass at 15ma  3mms.



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**Phillip Conroy** *March 01, 2016 18:17*

i would be intersted in how small a beam width you get,i used these instructions my self they are good


---
**Jason Reiter** *March 02, 2016 00:10*

Finally I have sucsess!!!  So after shiming well basicly everything and re aligning I have cut 3mm ply at 10mms @ 8ma perfect. I also thinned out my lines to 0.005 and it make a nice thin line no burn and cuts perfect.  **+Phillip Conroy**  I will take a picture to see.  Dr T.J. Fawcett instructions are right on and a must for alignment it takes time but well woth it. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/1oV13gN57LK) &mdash; content and formatting may not be reliable*
