---
layout: post
title: "New head just arrived :)"
date: April 18, 2016 08:30
category: "Modification"
author: "Damian Trejtowicz"
---
New head just arrived :)

![images/b20eadd70079e152ff9fe7597ffc79c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b20eadd70079e152ff9fe7597ffc79c0.jpeg)



**"Damian Trejtowicz"**

---
---
**Phillip Conroy** *April 18, 2016 08:46*

  Ice,looks like same as i brought,what air compressor are u using?


---
**Damian Trejtowicz** *April 18, 2016 08:49*

Big industrial one from my painting workshop :)


---
**Phillip Conroy** *April 18, 2016 09:08*

When i fitted my air assist with 6 cfm home worksop compressor i hadto clean focal lens of mosture every 30min of laser cutting,even with 2 water traps- finaly fixed with 1meter pipe filled witn silca gel


---
**Damian Trejtowicz** *April 18, 2016 10:39*

:( i need wait till tomorrow.

head have 20mm dia mountingand my stock is only 14.

now i need find dxf for right one and cut tomorrow on my other laser


---
**Damian Trejtowicz** *April 18, 2016 15:35*

Im so angry,i made mounting from acrylic using my old head.fit fine but now i cant get opics right,beam is out miles away.

I aleeady spend few hours setting up mirrors and still no good.

Beam keep line on y axis but x axis is out.

Looks like my frame is bend because on x min is on center but on max is about 5cmcover the head


---
**Scott Marshall** *April 18, 2016 20:44*

That would just be a #2 mirror adjustment out, it needs to angle down to get the beam parallel to the X travel. Once you have the beam parallel to travel, then you set the head mirror height to match. I had to cut some felt washers to shim mine up a tad.



It's a common mistake for people to aim at the mirror centers with the adjustments, that's not what you want to do with the aiming screws.



Parallel is what matters, once you are parallel, and have the same head impact spot for any position on the field then you move the laser, head or mirrors as required to get the hit near their centers. The only one that's critical is the lens pass thru and exit nozzle be centered. (Done with height and rotation of head) The mirrors work fine if you're off to the side a little, as long as it's a clean bounce.


---
**Tony Sobczak** *April 19, 2016 01:48*

How did you get that to fit. A relative bought one and it hit the base.  His is stock. The tip of the head hit the part that is springloaded.


---
**Scott Marshall** *April 19, 2016 03:31*

Mine has a custom carriage (it was damaged in shipping and I milled one from scratch) so my install was a little different, but used the same sort of top plate. The hole is larger for the head though. I bought mine thru Saite Cutter, and it appears identical. There's no spring loading in it, the lower part (carries the focal lens and nozzle) is adjustable with the thumbscrew, and the top (mirror carrier) is a threaded barrel with a stop/mounting ring. 



On my install, The top plate was too high to put the ring on the top of the plate and still hit the mirror on center, and too low to move it to the bottom, so I put 2 thick (.030ish) felt washers (burned out nice on laser) between the square body and bottom of the plate, and secured with the ring on top.

 It may fall different with different arrangements. 



My factory head was so mutilated I didn't have real good measurements to work with, so my height is likely different than a stock one.



I like this setup, because it allows adjustment of the focal point by loosing the thumbscrew and sliding the nozzle up/down. That lets you make a 1st pass focused on the top of the part, then re-focus lower, and finish the cut. Less draft on the kerf that way, and less burning on plywood.



And it sure beats moving the workpiece to do fine focus adjustments. The  head is (in my opinion) worth the price for that feature alone, Just loosen the screw and slide the nozzle up/down a bit, retighten, and fire!



(2 faster (about 18mm/s) passes like this have a nicer cut than 1 slow pass focused in center)


---
**Damian Trejtowicz** *April 19, 2016 06:11*

**+Scott Marshall** .

You have right,#2 mirror was to low,so i use the shim to rise,now the beam is hitting head in right way and beam goes thru nozzle center.

The biggest problem of this construction is that,we can't setup #3 mirror


---
**Scott Marshall** *April 19, 2016 22:46*

Damian:

The trick is to get the height right (however you can, it's the 'foundation') then rotating the head to get the beam leaving the mirror vertical and passing through the lens center, It took me a lot of juggling the head height and radial adjustment. The lens reverses your result, so check my tricks (I removed it for the initalsetup) it's really a trial and error deal.

(you can also make paper or tapes disks and put them in the lens socket to "rough in" the height and radial adjustments - aim for center)



A couple of tricks I came up with along the way:



1st  I used an improvised plumb bob to mark a spot directly below the nozzle on the machine floor. Then I took the lens/nozzle assembly out, and worked the 3rd mirror adjustments to "hit the spot". I then re-installed lens/nozzle assembly and fine tuned the adjustments using a piece of tape on the nozzle. "dirty" the nozzle mouth with graphite or something similar, and it will leave a circle on the tape in which to center your burn dot. 



The plumb bob - There's a lot of ways to do it, but I took some cotton string, tied multiple knots until it "hung" in the nozzle opening, then used a mechanical pen tip (Nickel plated brass cone that covers the mechanism) and did the same, slipping it on the free end, and tying a knot to hold it. While not perfect (hangs a little canted) it works with a little intuitive compensation.



One last trick, feel your nozzle, if it's getting warm, your beam is hitting it internally.



Last, last trick.-  Draw yourself "Cheat-sheet" diagrams, trying to keep all the beam directions in your head is a bit much, and one mistake will send you off on a 1/2 hr waste of time (I know). 



You'll get it, and it's worth it. Sure does take patience though.

Hope I helped at least a little...



Scott


---
**Purple Orange** *September 28, 2016 09:03*

Sorry for hijacking this thread, but +Scott Marshall how did you do to replace your damaged carriage? A step-by-step instruction would be highly appreciated, as my K40 also was delivered with a damaged carriage and i am about to replace that with a new one, but are uncertain where to begin and how to do it. Any help on the matter would be highly appreciated. Cheers!


---
**Scott Marshall** *September 28, 2016 09:40*

How to replace a K40 Carriage:



1st remove the entire motion control frame, this is the only practical way to work on it. Every thing unplugs and it's only 2 bolts holding it to the sheetmetal.



The carriage itself is retained by the 4 rollers, these are mounted on an eccentric (the shaft is turned off center) so what's required to install/adjust the carriage assembly is to loosen the set screws so they will turn. I use the socket head screw retaining the roller to turn the assembly, go clockwise if possible and they won't loosen. you may have to remove 2 to get the carriage off. Once it's dropped down, there's a clamp underneath holding the belt together and to a "fin" on the the carriage. Before removing it, go to the right end of the X rail and find the bottom screw. This screw adjusts the belt tension, back it almost all the way off (CCW)



Mark the belt (white out or white paint are good) for length and orientation so you can duplicate the length and position when re-clamping it. This will 'ballpark the length" so it's within adjustment range.



Install the new carriage by reversing the process. 



The carriage is loaded with burrs and such, assembly will go much smoother if you remove all the rollers, chamfer the roller hole openings by hand with a countersink or large drill bit, and clean up the rest with a file. File the outer diameter of the roller eccentric to remove any setscrew burrs and make sure they slide in and out easily.



Install the belt using the previous marks.



Use Blue Loctite or nail polish on the clamp screw, you don't want to overtighten this, and it will loosen at the correct torque. If the plate starts to bend or the belt starts to squeeze out, stop tightening. Put the carriage onto the rail. You may have to 'hang it' by 2 rollers and slide the other 2 in place. Adjust the rollers to eliminate all play. Drag should be detectable but the carriage should move under it's own weight.



Draw up the belt tension screw until the belt is snug, but not tight. Better too loose here than too tight. You'll have to re-adjust it later anyhow.



Visually verify the tab on the carriage is 'breaking' the opto interrupter on the left side of the travel. The tab should come to rest right between the 2 legs of the interrupter (This is a black horseshoe shaped device about 8mm across) Make sure the tab does not drag on either leg, it should be solidly in the gap, but never touch.



Re-install the motion control framework and align the mirrors. 

Hope that does it, ring me on my site if you have any questions.



[all-teksystems.com - all-tek-systems](http://ALL-TEKSYSTEMS.com)



Scott


---
**Purple Orange** *September 28, 2016 10:05*

**+Scott Marshall**​ thanks for the fast response.  Not a quick fix then.  <b>sigh</b> I'll save the work for later this year when I have a bit more spare time.  Thanks again for your detailed description.  Cheers. 


---
**Scott Marshall** *September 28, 2016 11:40*

It's not as bad as it sounds, maybe a couple of hours. If you're doing the head at the same time, maybe 3.

Good Sat afternoon project.



Have fun, Scott


---
**Tony Sobczak** *September 28, 2016 14:33*

Once you have that installed I'd appreciate a photo. 


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/bCGpprj2Sw2) &mdash; content and formatting may not be reliable*
