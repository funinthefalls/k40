---
layout: post
title: "So looking on removing my very loud air compressor from my air assist to make the house happier when I am working"
date: December 20, 2016 03:09
category: "Air Assist"
author: "Kelly S"
---
So looking on removing my very loud air compressor from my air assist to make the house happier when I am working.  Was looking at [https://www.amazon.com/Goplus-Commercial-Aquarium-Hydroponics-Aquaponics/dp/B019RK3VNQ/ref=pd_sbs_201_t_2?_encoding=UTF8&psc=1&refRID=N4M4KB7YV25D223DKQKE](https://www.amazon.com/Goplus-Commercial-Aquarium-Hydroponics-Aquaponics/dp/B019RK3VNQ/ref=pd_sbs_201_t_2?_encoding=UTF8&psc=1&refRID=N4M4KB7YV25D223DKQKE) anyone use something similar? Or recommenced something different? 





**"Kelly S"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 03:20*

I use a DC air pump with tubing. Can even drive it  from one of the larger mosfets on the cohesion3d mini :)

[m.banggood.com - 12V DC Diaphragm Vacuum Pump Air pump High Pressure Micro Vacuum Pump  Sale - Banggood Mobile](http://m.banggood.com/12V-DC-Diaphragm-Vacuum-Pump-Air-pump-High-Pressure-Micro-Vacuum-Pump-pnew-1046082.html)


---
**Cesar Tolentino** *December 20, 2016 03:21*

Almost the same price as airbrush compressors. Let me know how it works for you. 


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 03:23*

Speaking of... Since I pulled that one to cool my 3d printer volcano hotend :) I need to get another one for if I ever do get my pick n place running. 


---
**Kelly S** *December 20, 2016 03:30*

**+Ray Kholodovsky** Are you using a 3d printer style turbo fan?  Does it suck up the fumes and just blow them back out?  Curious.


---
**Cesar Tolentino** *December 20, 2016 03:31*

Ray. Can you show us your setup?


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 03:34*

**+Kelly S** I'm using that pump I linked above.  It has tubing (let's estimate 1.4" ID) which goes thru a cable chain and into a printed air assist head from thingiverse which is mounted around the moving optics head.


---
**Cesar Tolentino** *December 20, 2016 03:36*

Where can u get this 1.4" ID tubing?


---
**Kelly S** *December 20, 2016 03:37*

Missed the link sorry.  Does it put out decent air flow?


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 03:37*

Whoa.  Typo.  I meant 1/4" .  And that was just an estimate.  I think I'm using 5/32" but it's tight so 1/4 would be good.  From mcmaster.


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 03:39*

Pretty good.  I don't have a compressor so that one does every job (pick n place suction, laser air assist, cooling the molten plastic being spat out by the monster that's on one of my printers :)


---
**Kelly S** *December 20, 2016 03:40*

Nice, may try this first.  Thanks  :) 


---
**Ashley M. Kirchner [Norym]** *December 20, 2016 04:46*

Do you run that pump continuously Ray? 


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 04:51*

For the extent of the job, yes.  I have a separate DC adapter for it that I plug in and unplug after the job. Ultimately, it should just get hooked up to one of the large mosfets on c3d. And those have a separate power in terminal so you can hook up a separate 12v supply for that since I wouldn't want this additional load on the laser psu. Oh and then just set it to on and off with a start/ end gcode and a switch in the smoothie config. 


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 04:54*

And it's a bit loud so you should make a set of foam/ rubber C shapes to mount it. Like pipe clamps or some such. 


---
**Kelly S** *December 20, 2016 05:31*

Was looking at it, and printing something to hold it with a insulator would be pretty simple.  Do you have a recommended 12v supply Ray? And if you wouldn't mind maybe sending me a PM with a picture of where you hooked it to, and what gcode to use, that would be amazing. ;)


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 05:33*

I use a lot of ATX supplies from before I understood how good 24v is. 

For that pump, a 1-2 amp wall wart or a 6 amp power brick would be fine. The latter should be 10-12 bucks on eBay. "12v6a"


---
**Ashley M. Kirchner [Norym]** *December 20, 2016 05:36*

K, I was just curious. I have a much larger compressor, specifically the California Air Tools 5510SE, 120psi. I have a regulator on the laser that drops that PSI down to what I need in the laser. When the laser is running, it kicks in every 4-5 minutes to re-pressurize. And quiet. Here are two YouTube videos (NOT BY ME) that compare it against other brands:



Craftsman vs the 5510SE: 
{% include youtubePlayer.html id="Y7SnHzuwKu0" %}
[https://www.youtube.com/watch?v=Y7SnHzuwKu0](https://www.youtube.com/watch?v=Y7SnHzuwKu0)



Unknown brand vs the 5510SE


{% include youtubePlayer.html id="AZWqHndbkVI" %}
[https://www.youtube.com/watch?v=AZWqHndbkVI](https://www.youtube.com/watch?v=AZWqHndbkVI)


---
**Ashley M. Kirchner [Norym]** *December 20, 2016 05:43*

The ON/OFF switch and pressure release valves are louder than the running noise level. :)


---
**Kelly S** *December 20, 2016 05:45*

After some digging I will just convert a atx supply into a dedicated unit.  In case i want to run other stuff later on, ;)


---
**Don Kleinschnitz Jr.** *December 20, 2016 13:43*

In my conversion I am using a 4X relay board that turns on and off the blower and compressor's AC (no DC supply needed). This relay board is set up to interface to the smoothie when I am ready to control it from G/M code. It also can be controlled from the front panel.



I like this air pump very quiet with simple 1/4" air hose connections.([https://www.amazon.com/gp/product/B002JLGJVM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B002JLGJVM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)) 



More info here: [http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)

[amazon.com - Robot Check](https://www.amazon.com/gp/product/B002JLGJVM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1air)


---
**Kelly S** *December 20, 2016 14:14*

Thanks Don, I seen you post this on Facebook yesterday I think.  


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/X6NRw1HjwS2) &mdash; content and formatting may not be reliable*
