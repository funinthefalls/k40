---
layout: post
title: "Upgrades! Thanks Ray Kholodovsky"
date: August 31, 2016 20:28
category: "Smoothieboard Modification"
author: "Carl Fisher"
---
Upgrades!  Thanks **+Ray Kholodovsky**​

![images/bfe325b5652225e749a3a31f70d162d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bfe325b5652225e749a3a31f70d162d2.jpeg)



**"Carl Fisher"**

---
---
**greg greene** *August 31, 2016 21:19*

they look cool - but  - which ones are they?  :)


---
**Stephen Sedgwick** *August 31, 2016 21:57*

I love his little board design, it is very very clean - have yet to do much playing with mine (none yet honestly) but hope to this next week or so... other projects were put first (thanks to my honey do list),  I like the display component you have here have to post how it works.




---
**Carl Fisher** *September 01, 2016 00:02*

**+greg greene** The main controller is from **+Ray Kholodovsky** of Cohesion3D. It runs Smoothie. 



The glcd panel is the RepRap Discount panel. Got the config and LCD up and running tonight but have to shelve it for a week. I also need to order my stepper drivers to plug into the sockets on the board and get everything up and running.


---
**greg greene** *September 01, 2016 00:04*

Good luck and keep us informed please, I'm going to upgrade mine - mainly for engraving pictures - but don't know the best way to do that yet.


---
**Ray Kholodovsky (Cohesion3D)** *September 01, 2016 00:07*

The best way is this way, obviously.  You know that greg :)


---
**greg greene** *September 01, 2016 00:08*

Perhaps!  I looked at the Cohesion site - but didn't find the board anywhere - just a great discussion on 3d circuit fabrication - which as a A Ham operator - I'm also interested in.


---
**Ray Kholodovsky (Cohesion3D)** *September 01, 2016 00:10*

Yeah, that's from a project I briefly worked on 2 years ago.  I'm really behind on things like getting a new site with info up, I spend all day building boards, designing more boards, and talking to people with my boards :)


---
**greg greene** *September 01, 2016 00:11*

When your ready,send me some info on it - price etc. and what I would need to use it in the K40 - What software runs it best?


---
**Ray Kholodovsky (Cohesion3D)** *September 26, 2016 02:35*

Pre-production, those were handmade by my for some testers.  I am in the process of confirming the factory to mass produce these.


---
**Ray Kholodovsky (Cohesion3D)** *October 06, 2016 15:30*

**+jim jam** pre-orders within a week!  Will keep you posted.


---
**greg greene** *October 06, 2016 21:02*

thanks


---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2016 22:48*

**+jim jam** Check my profile to see the latest version of this board.  I think we're going to do pre-orders for it very soon and I'll try to get 100 manufactured pretty quickly.


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/4kaijgth3o6) &mdash; content and formatting may not be reliable*
