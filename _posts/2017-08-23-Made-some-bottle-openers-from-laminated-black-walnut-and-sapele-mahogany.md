---
layout: post
title: "Made some bottle openers from laminated black walnut and sapele mahogany"
date: August 23, 2017 00:07
category: "Object produced with laser"
author: "Ned Hill"
---
Made some bottle openers from laminated black walnut and sapele mahogany.  Design Inspired by **+Alex Krause**. I don't have a CNC router like he does so I had to do it the old fashion way with a bandsaw and a belt sander :).  I laser cut a template out of a piece of 1/8" ply to hold them for engraving and used that to also trace the outline to the wood before cutting with the bandsaw. Sapele mahogany engraves very nicely. 



![images/deca80fce6c15c87b657ab92cdd8f9f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/deca80fce6c15c87b657ab92cdd8f9f9.jpeg)
![images/e645e2c5e96dc32587eb425d27324502.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e645e2c5e96dc32587eb425d27324502.jpeg)
![images/b3502244e6b27bdc9064c6aa16d4f4e8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b3502244e6b27bdc9064c6aa16d4f4e8.jpeg)

**"Ned Hill"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 23, 2017 02:01*

To be fair, Alex's router spends more time cutting  the heads off the screws than the wood those screws are holding down :) 


---
**Kirk Yarina** *August 23, 2017 11:51*

Been there.  Good reason to use nylon screws.


---
**William Kearns** *August 25, 2017 06:17*

Very nice!


---
**Ispabella Designs** *August 26, 2017 02:12*

Very nice!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/XvzbcgcvJRc) &mdash; content and formatting may not be reliable*
