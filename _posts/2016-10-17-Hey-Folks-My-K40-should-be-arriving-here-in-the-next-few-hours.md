---
layout: post
title: "Hey Folks, My K40 should be arriving here in the next few hours"
date: October 17, 2016 18:58
category: "Hardware and Laser settings"
author: "Kris Sturgess"
---
Hey Folks,



My K40 should be arriving here in the next few hours.



What size of reservoir do you all run? 1gal? 5gal?



Do you place it above, level or below the machine????



Hoping to get everything checked over, mirrors aligned and possibly fire it tonight.



Cheers!



Kris





**"Kris Sturgess"**

---
---
**Jim Hatch** *October 17, 2016 19:55*

5gal at machine level.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 17, 2016 21:21*

I'm currently using around 5 gal at machine level also.


---
**Robert Selvey** *October 17, 2016 22:35*

Does machine level make a difference compared to underneath ?


---
**Jim Hatch** *October 17, 2016 22:53*

It helps keep air from getting into the tube. 


---
**Stephane Buisson** *February 08, 2017 07:29*

**+Kris Sturgess** is it that profile member, you do have issue with ?


---
*Imported from [Google+](https://plus.google.com/114699437542869593311/posts/cBCR9j5GvPH) &mdash; content and formatting may not be reliable*
