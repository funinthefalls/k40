---
layout: post
title: "Good evening guys :) i think im right here"
date: December 27, 2017 17:19
category: "Hardware and Laser settings"
author: "Martin W"
---
Good evening guys :) i think im right here.



I want to upgrade my 40W laser to an 50W CO2 Laser.



I ordered the Tube and a new Power Supply. (Dy-10 50W)



And i ordered a Smoothieboard 4cx too :)

I added new Stepper motors and Laser Glasses :)



So my question is:



Did someone have a wire instruction? So that i can directly connect the smoothie to the Dy-10 Power supply? In the internet are very much confusing things :O



So i think that im here by the right side of laser blaster :)





**"Martin W"**

---
---
**Chris Hurley** *December 27, 2017 17:50*

Hello there! There is a thread on the go that talks about doing this just a few days ago that has the wiring schematic on it. 


---
**Chris Hurley** *December 27, 2017 17:53*

[https://plus.google.com/112428355097604312279/posts/D1kLCrKwPmb](https://plus.google.com/112428355097604312279/posts/D1kLCrKwPmb)


---
**Chris Hurley** *December 27, 2017 17:53*

[plus.google.com - Help... I've been using K40 for more than 2 years and few months ago i replac...](https://plus.google.com/112428355097604312279/posts/D1kLCrKwPmb)


---
**Martin W** *December 27, 2017 18:26*

Hey **+Chris Hurley**, thanks for that. I saw it yesterday :)



My brain is actually a little bit confused of diagrams and plans in the internet. 



I tell you my thinking, i hope its right.



The normal wiring is clear. Like Stepper and Power to Smoothie.

The only Problem that i have is the PWM connection from the smoothie to the HighVoltage Power Supply.



In my Smoothie Configuration i set the laser_module_pwm_pin   to 2.4 so i get the Ground from the HighVoltage power supply to 2.4 and go from the other pin 2.4 to the IN port in the PSU and not in the L/TL. Then i i connect the 2.5^ smoothie pin to the L/TL pin on the PSU.







Thats my thinking correct me please if its wrong :)


---
**Joe Alexander** *December 28, 2017 00:57*

this pic helped me with my re-wire, and is my current setup with the exception that my laser psu has the green connectors.(and my 2.4 mosfet died so its on 2.5)

![images/469712bd8f6673c9beec11ff893cccd5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/469712bd8f6673c9beec11ff893cccd5.jpeg)


---
**Martin W** *December 28, 2017 10:48*

**+Joe Alexander** thanks. And now my confusing is complete :D i saw soooo much pictures :D The picture i post is my PSU can u just explain where i connect my Ground and PWM? Im very confused now :O



![images/ffe3cbc90802d01b0c1bbd6b2fa3a6e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ffe3cbc90802d01b0c1bbd6b2fa3a6e0.jpeg)


---
**Joe Alexander** *December 28, 2017 12:47*

on the rightmost 4 pin: L is your in to pin 2.4 and gnd to mosfet ground.


---
**Martin W** *December 28, 2017 13:00*

**+Joe Alexander**  Yeah thanks. Again for dummys like me actually :D 



laser_module_pwm_pin 2.4  >> L Right.(Thats PWM)



 switch.laserfire.output_pin 2.5^  >> I didnt need anymore??


---
*Imported from [Google+](https://plus.google.com/105895260462741765378/posts/VEhwFWqV2J6) &mdash; content and formatting may not be reliable*
