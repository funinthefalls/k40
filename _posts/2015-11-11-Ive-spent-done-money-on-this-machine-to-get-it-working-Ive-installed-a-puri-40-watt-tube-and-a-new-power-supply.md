---
layout: post
title: "I've spent done money on this machine to get it working, I've installed a puri 40 watt tube and a new power supply"
date: November 11, 2015 01:18
category: "Hardware and Laser settings"
author: "Scott Thorne"
---
I've spent done money on this machine to get it working, I've installed a puri 40 watt tube and a new power supply. I must say that I'm impressed with the quality of the work and the power of the puri tube.

![images/382c79f36f14d90933c3d415016deaeb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/382c79f36f14d90933c3d415016deaeb.jpeg)



**"Scott Thorne"**

---
---
**Scott Thorne** *November 11, 2015 01:19*

I had the heat set too high....other than that, it came out good.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/eMRy3peSg9d) &mdash; content and formatting may not be reliable*
