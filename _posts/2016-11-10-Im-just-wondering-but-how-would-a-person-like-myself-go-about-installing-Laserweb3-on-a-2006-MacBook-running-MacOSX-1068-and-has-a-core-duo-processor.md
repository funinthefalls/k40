---
layout: post
title: "I'm just wondering but how would a person like myself go about installing Laserweb3 on a 2006 MacBook running MacOSX 10.6.8 and has a core duo processor?"
date: November 10, 2016 15:08
category: "Discussion"
author: "Jonathan Davis (Leo Lion)"
---
I'm just wondering but how would a person like myself go about installing Laserweb3 on a 2006 MacBook running MacOSX 10.6.8 and has a core duo processor? 





**"Jonathan Davis (Leo Lion)"**

---
---
**Ariel Yahni (UniKpty)** *November 10, 2016 15:52*

Same as here but instead of using cmd you will use Terminal ( type terminal on spotlight) ﻿[https://plus.google.com/+ArielYahni/posts/6Eq99pE4ZZY](https://plus.google.com/+ArielYahni/posts/6Eq99pE4ZZY)


---
**Jonathan Davis (Leo Lion)** *November 10, 2016 16:12*

Tried that did not work as it set current osx system. The highest version of google chrome my system can run is version 38!


---
**Jonathan Davis (Leo Lion)** *November 10, 2016 21:19*

when i do it according to the install instructions i keep getting the result as seen by the image. As i do have it fully working on windows and now trying to get it to work on a 2006 Macbook with a core duo processor. 



![images/f62552348998ddfb6d95f4365c9df3d0.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f62552348998ddfb6d95f4365c9df3d0.png)


---
**Jonathan Davis (Leo Lion)** *November 10, 2016 21:29*

**+Peter van der Walt** a 32bit version is not aviable


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/4fG15qtgYxm) &mdash; content and formatting may not be reliable*
