---
layout: post
title: "Thanks Vince Lee , I got this today. Awesome community!!!"
date: February 24, 2017 19:52
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Thanks **+Vince Lee**​, I got this today. Awesome community!!!

![images/d1046b8f728212d27394c660b9ab73c8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1046b8f728212d27394c660b9ab73c8.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2017 19:59*

I have one of those sitting around as well. Along with a middleman pcb Vince sent me. 


---
**Jose Salatino** *February 24, 2017 20:00*

Yo quiero que me manden cosas!!!


---
**Ariel Yahni (UniKpty)** *February 24, 2017 20:04*

Look at the date it was sent


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2017 20:10*

Yeah fun stuff. Mine was in the spring-fall timeframe. 


---
**Alex Krause** *February 24, 2017 20:37*

What is it?


---
**Jon Bruno** *February 24, 2017 20:48*

How was it shipped? Eventual Express?


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2017 20:50*

I like that **+Jon Bruno**! It's one service level below snail mail. 


---
**Jon Bruno** *February 24, 2017 21:09*

LOL!


---
**Ariel Yahni (UniKpty)** *February 24, 2017 21:25*

Lol


---
**Vince Lee** *February 25, 2017 01:42*

Nuts!  Did they have somebody swim it over?


---
**HalfNormal** *February 25, 2017 03:22*

Vince Lee was good to me as well. Great people found here.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/R9tRgkbjv5q) &mdash; content and formatting may not be reliable*
