---
layout: post
title: "So we decided to upgrade our K40 laser"
date: December 19, 2016 12:56
category: "Modification"
author: "Peter Jacobsen"
---
So we decided to upgrade our K40 laser. To be honest the Corel software is just to unstable in my opinion. 

On our laser engraver we use Cut2d, does anyone in here use Cut2D on their K40 laser, and if so, which controller board did you upgrade the K40 with?





**"Peter Jacobsen"**

---
---
**Anthony Bolgar** *December 19, 2016 13:14*

Two main methods of upgrading, A smoothieware based board like the smoothieboard, or azteegX5, or C3D mini. Or a grbl controller (basically an arduino uno with some stepper drivers) The grbl controller is dirt cheap, but the smoothieware based boards offer better quality and much smoother output. LaserWeb is an excellent open source laser /cnc milling software. With Cut2D you will need to find a post processor for the board you upgrade to. I think both smoothiware based and grbl have available post processors.


---
**Peter Jacobsen** *December 19, 2016 14:18*

Is the laserweb 3 software easy to use? Will it support the most commen formats (ai, svg) 


---
**Anthony Bolgar** *December 19, 2016 14:49*

Imports bitmaps, svg, dxf. Very easy to use, and a massive new rewrite of LaserWeb is happening right now, V4 will be released in the very near future, and incorporates CNC milling CAM functions as well as drag knives, and plasma cutters.


---
**Peter Jacobsen** *December 19, 2016 14:53*

So the smoothieboard 4xc is the one I should get right? Is there a guide to replace the board?


---
**Anthony Bolgar** *December 19, 2016 14:58*

Yes, and this google community has a sub section for smoothie upgrades, check out the categories on the left.


---
**Richard Vowles** *December 19, 2016 17:50*

Well, the Cohesion 3d mini laser upgrade bundle is the better smoothie variant to get with k40 lasers. It has its own Google group [http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Richard Vowles** *December 19, 2016 17:50*

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Peter Jacobsen** *December 19, 2016 20:49*

Is the Cohesion board a proven design? Will it work as easy as described?

From where will it ship? 

I can wait for 6 weeks for the perfect board, the main thing is that we can use Laserweb software after the upgrade.


---
**Richard Vowles** *December 19, 2016 21:56*

**+Ray Kholodovsky** ?


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2016 22:06*

Hello! 

Yes, we've tested the Mini with around 40 people/ k40's at this point and the results have been good. We're currently on design revision #5 so I think it's fair to say that we've worked out all the bugs at this point.  You can ask around the group for testimonials (**+Carl Fisher** **+Joe Spanier** have/ will get both the old version and the new version of the design so they are the ones to tell you how simply drop-in the design is)

It runs smoothie firmware and works great with LaserWeb so you're getting the best of both performance worlds there. 

Shipping should be closer to 4 weeks, the production is underway already. 

On a side note I wrote a laser post processor for vectric aspire, maybe it will work for you as well. 


---
**Peter Jacobsen** *December 20, 2016 21:04*

That sounds perfect. From where will the board ship?


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 21:06*

New Jersey, USA. 


---
**tyler hallberg** *December 21, 2016 02:34*

**+Ray Kholodovsky** I ordered a board about a week and a half ago, when do you think they will be ready to ship? Mid January?


---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2016 02:40*

Samples came in today **+Tyler Hallberg**. Should be just a few more weeks once we confirm functionality and get the rest of the batch assembled and over here. 


---
*Imported from [Google+](https://plus.google.com/117580774600105315145/posts/ffasBMjEfci) &mdash; content and formatting may not be reliable*
