---
layout: post
title: "Thanks Jose Salatino for modifying your app so I can laser it"
date: June 10, 2016 03:19
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
Thanks **+Jose Salatino**​ for modifying your app so I can laser it. Love the Crosshatch 

![images/f0ebd50d2c2bedda9d26b0d8ca372e50.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f0ebd50d2c2bedda9d26b0d8ca372e50.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Jose Salatino** *June 10, 2016 09:26*

Thanks a lot.


---
**Jose Salatino** *June 10, 2016 09:27*

How much time to do it?


---
**Ariel Yahni (UniKpty)** *June 10, 2016 11:49*

It took a good while


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/QZRWCDY6aAj) &mdash; content and formatting may not be reliable*
