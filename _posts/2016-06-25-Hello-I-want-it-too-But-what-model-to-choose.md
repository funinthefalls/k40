---
layout: post
title: "Hello, I want it too! But what model to choose?"
date: June 25, 2016 23:39
category: "Discussion"
author: "Michal Benda (RewDroid)"
---
Hello,

I want it too!

But what model to choose? There's so many possibilities. What are the latest / best?

thanks for the help





**"Michal Benda (RewDroid)"**

---
---
**Derek Schuetz** *June 25, 2016 23:48*

What are your plans for it?


---
**Michal Benda (RewDroid)** *June 26, 2016 00:19*

More frequent deploy:

- Cutting and engraving acrylic

- Cutting and engraving plywood



Sometimes deploy:

- Engraving wood

- Cutting paper cardboard


---
**Derek Schuetz** *June 26, 2016 00:45*

So just hobby? Do you like to tinker or do you just want a machine that just does its thing


---
**Michal Benda (RewDroid)** *June 26, 2016 07:05*

Yes and no ;). I want to use it for my event (badges, key rings, etc.) Sometimes the small things for my friends and hobby projects. I do not want to modify the machine


---
**Michal Benda (RewDroid)** *June 26, 2016 20:10*

What's recommendation, please? thanks


---
**Derek Schuetz** *June 26, 2016 21:15*

[https://www.ebay.com/itm/152109833330](https://www.ebay.com/itm/152109833330) 



Best value if you don't want to do modifications and you want something that just works


---
**Michal Benda (RewDroid)** *June 26, 2016 21:45*

Thanks! But For me, it is sufficient dimension 300x200. Or 300x200 not for me?  thanks


---
**Derek Schuetz** *June 26, 2016 21:46*

300x200 is small I wish I had more room and if I had to buy again I would buy the one I linked


---
**Michal Benda (RewDroid)** *June 26, 2016 21:51*

My space is small :( And 300x200 ideal fot me projects ;) please Link to this size thanks! 


---
**Derek Schuetz** *June 27, 2016 03:14*

Anything you see on eBay for under $350 then


---
**Michal Benda (RewDroid)** *June 27, 2016 05:46*

I know, I just wanted some newest model.

There are so many

Thank you for your time


---
*Imported from [Google+](https://plus.google.com/118052961829303361596/posts/MzHH3Ue9PJZ) &mdash; content and formatting may not be reliable*
