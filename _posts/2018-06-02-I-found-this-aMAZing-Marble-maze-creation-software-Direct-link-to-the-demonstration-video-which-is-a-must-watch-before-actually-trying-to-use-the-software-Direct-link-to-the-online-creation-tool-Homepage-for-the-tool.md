---
layout: post
title: "I found this aMAZing! Marble maze creation software Direct link to the demonstration video which is a must-watch before actually trying to use the software: Direct link to the online creation tool: Homepage for the tool:"
date: June 02, 2018 04:19
category: "Repository and designs"
author: "HalfNormal"
---
I found this aMAZing!



Marble maze creation software



Direct link to the demonstration video which is a must-watch before actually trying to use the software:


{% include youtubePlayer.html id="4fWBe2R-6Nw" %}
[https://www.youtube.com/watch?v=4fWBe2R-6Nw](https://www.youtube.com/watch?v=4fWBe2R-6Nw)



Direct link to the <b>online</b> creation tool:

[https://adashrod.github.io/LaserCutMazes/designer](https://adashrod.github.io/LaserCutMazes/designer)



Homepage for the tool:

[https://adashrod.github.io/LaserCutMazes/welcome](https://adashrod.github.io/LaserCutMazes/welcome)





**"HalfNormal"**

---
---
**Ned Hill** *June 04, 2018 18:29*

Cool!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/5v5gXnZKRPp) &mdash; content and formatting may not be reliable*
