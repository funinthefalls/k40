---
layout: post
title: "Hi everyone, So I've had my K40 for a week now and am having a lot of fun planning out my hardware upgrades and learning the intricacies of the machine and software"
date: July 09, 2016 06:18
category: "Software"
author: "Michael Whelan"
---
Hi everyone,

So I've had my K40 for a week now and am having a lot of fun planning out my hardware upgrades and learning the intricacies of the machine and software. I'm very well versed in Autocad, so a DWG/DXF workflow is much preferred over anything else. To that end I have been drafting in cad, importing DXF's to corel draw, and using the corel laser toolbar to send the work to the laser.

My problem that I hope you all can help with is that the laser is cutting everything twice, even though I have set the cad line weights to 0.00mm and the corel object line weights to hairline as recommended in other threads related to this problem. I have also tried setting the cutting data format to WMF, EMF, BMP, and HP-GL with no change in behavior. Objects drawn in corel also exhibit the same behavior, but objects drawn in laserdraw do not.

Thanks for the help. This community is great.





**"Michael Whelan"**

---
---
**Michael Whelan** *July 09, 2016 06:48*

Thanks to Gee Willikers for his helpful post. Setting the lineweight to 0.025mm took care of the problem. Sorry for repetitive question.


---
*Imported from [Google+](https://plus.google.com/103446533191709263947/posts/PV31Tm9hQ4b) &mdash; content and formatting may not be reliable*
