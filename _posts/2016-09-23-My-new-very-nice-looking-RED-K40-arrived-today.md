---
layout: post
title: "My new very nice looking RED K40 arrived today"
date: September 23, 2016 22:45
category: "Discussion"
author: "Scott Marshall"
---
My new very nice looking RED K40 arrived today. I'd like to share it's RED wonderfulness, but I still can't figure out why I suddenly can't post photos. (I have some ACR photos I'd like to put up and a very nice "PSU guide" with photos I'd also like to contribute. 



ANybody! help. Yuusuf reduced one for me. no go. I made one monochrome, no go. I put some in an album. No go.



I start here, browse to the photo, the wheel goes around, and then, where I used to see my picture, nothing. Right back to the text box, no error, no nothing.



It used to work great.



Right now it's getting dark and I should go drag my nice new (NO apparent damage - hope that's not jinxing me, I haven't opened the lid yet) RED Laser into the shop lest it get damp.



Did I mention it's RED.?? Very cool, A RED blue box.



Scott





**"Scott Marshall"**

---
---
**Ariel Yahni (UniKpty)** *September 23, 2016 23:03*

**+Scott Marshall** do you get and error?


---
**Scott Marshall** *September 23, 2016 23:06*

**+Ariel Yahni** no, it just stops loading and goes back to the entry panel. 


---
**Bill Keeter** *September 24, 2016 00:30*

RED?! does it cut faster? 



Seriously though RED version looks nice. Any difference from the standard blue model?


---
**Jim Hatch** *September 24, 2016 01:09*

File too large? Try a lower resolution photo.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 24, 2016 02:36*

**+Jim Hatch** I shrunk one of his photos to <1mb in size & Scott was still having issues uploading. It seems to be an issue he is having with Google+ not letting it load for some reason.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 24, 2016 02:37*

Actually, I have a thought **+Scott Marshall**. We could maybe try a Remote Assistance through Windows sometime & see if we can figure out what is causing it.


---
**quillford** *September 24, 2016 03:46*

Perhaps try something like chrome incognito. I've had stuff like this happen to me with certain plugins I forgot I had enabled, like CORS enabler.


---
**Josh Rhodes** *September 24, 2016 04:10*

**+Jim Hatch**​ you could try using Google drive instead. Then you can post the photo from drive to plus.


---
**Scott Marshall** *September 24, 2016 07:32*

Sorry I disappeared there, had to take a break.

Thanks for all the suggestions, I think I'm going to do some search engine work, maybe I'm not the only one with this issue.



Mr Yahni reached out to the Google people on my behalf and it doesn't look like they've responded as yet, maybe they'll help (one can dream) - Thanks Ariel.



I tend toward a minimalist approach to most of the 'social' networking 'machine' and don't have a lot of Google products (or anyone elses) installed for just this reason. For me, reliable Email and a decent forum engine are all I really need, but that's become increasingly difficult to find with the software monster that is overwhelming the PC world. I don't want to deprive anyone of all the Facebook style bells and whistles, but at it's core, a forum should facilitate information exchange without wasting the users time.



This Group succeeds DESPITE Google, not because of it.



We would be much better off with a conventional forum engine, and I've stated that opinion before. Our numbers are severely limited by the fact that visitors cannot find a anything, and none of our accomplishments are visible. They come, stumble through the mess, and leave.  We wouldn't have the following we do if it wasn't for the fact that we are the ONLY place with this information, even as hard as it is to access.



 When you land on a normal forum, there is the subject matter and forum history, laid out neatly in a single page of  topics, each 'sticky' ed with the most useful and commonly requested information. How many times have you answered the same question? I feel guilty when I tell a visitor to "look around" or "read up" because I know it's nearly impossible, and certainly not fun.



How long will we have to continue to endure this restrictive and confusing format? I have wasted a lot of  time here trying to do tasks that are a simple  2 click operation on normal forums. As have most of us.



Sorry to go off complaining, but this does indeed get old.



I spent 2 hours doing a nice PSU guide with marked up photos to overcome the lack of stickies, only to run into this.



 The group is asked weekly on average about the different PSU connections, or how to install a PSU, and that is covered 20 times over if we simply had a decent sticky library. 



Forums are about information exchange with friends, I have no idea what this format is designed to do well, but I've not seen it yet.



Rant over. Sorry to go into that again, I couldn't help it.



As to Bill's question, it looks to be the standard K40, (which is what I expected)but I still haven't opened the lid.



I ordered it from GlobalFreeShipping - also "Banyan Imports" (ebay) as with my 1st K40.



It took 4 days to show up here in Central New York via Fedex ground and total cost was $315.



Wow! The 1st one took 2 weeks, showed up smashed and was settled on. Looks Like GFS has fixed the operation up a bit. I think you can order from them without concern.



Upon opening the double boxed with foam between - I DID notice the old luggage bag is gone, replaced with a ziplock bag containing a User Manual!! Neatly bound with a Office supply protector, it shows promise!!  Excited to see the schematics, parts lists and mirror alignment instructions, I open it to find - NOTHING. Another joke for my collection of useless Chinglish Documents I've taken to collect. This one has some beauties in it. Some of the hits are: under the "Common Faults Analysis"  - if you have a "Desultory Light when working" - you should check "Check water circulation is swimmingly or not",  and much more similarly insightful advice.

It may be used to level the machine.



Also in the bag is the usual assortment of stuff. A roll of white double sided tape. No idea what that's for, maybe holding workpieces in place like I do in the mill? The little tube of RTV, not sure if I'd trust MY high voltage to it, a Surprisingly NICE USB cable (way to go K40 guys!!) and the IEC line cord.



I'll be doing a step by step box to 'finished" series of videos/write-ups which I'll have on my website (and here if I can fix the photo thing)



I'm going to have a try at the youtube video process, and do videos on the post shipping inspection and adjustment thru to the Air assist head selection and install, possibly with a few popular heads. 

You'll see an ACR module installation/Smoothification, and one with an Atzeeg board. Also on my list is a 'Wiring what's what', 'Safety around the K40',  'PSU Installation' and many of the questions you often see here will be answered with a video demonstration.



If you have a subject you would like to see covered, please let me know.



This assumes I can make it all work. It's going to be raw and bloody, as I don't have a video editor, and am not very slick in front of the camera. I'll give it a go, and see what we get, advice and guidance will be welcomed.

Film @ 11 (so they say...)



Scott








---
**Don Kleinschnitz Jr.** *September 24, 2016 09:49*

Don't know if this will help nor if you are using chrome. Recently I was having problems with various online programs not rendering or printing correctly. I found that my Chrome was not fully updated. I did an update but to my surprise it was waiting in the help/about for me to release it. After doing so, lots of stuff started working properly again.


---
**Josh Rhodes** *September 24, 2016 13:25*

**+Jim Hatch**​​ there's no software involved. As long as you've got a reasonably updated browser (not IE5.. ) and a Google account. Just go to [http://drive.google.com](http://drive.google.com) and you should be able to upload. Ignore any popups asking to install software. ﻿


---
**Scott Marshall** *September 24, 2016 17:55*

Thanks Don, I'm using AOL which has IE at it's core. 



Yuusuf, I'm running Win7, does remote work with that or do you need 10?



I'm expecting it to be something simple, haven't had time to work on it, I have a bunch of orders to get out. Got way behind during the week while I was sick frequently.



 If you're reading this and are one of those people waiting for a kit, they all go out on Monday. Sorry for the delay.



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 24, 2016 19:46*

**+Scott Marshall** I will give it a bit of a look. I haven't used Remote Assist for many years, but if it does work I'll find out & we can give it a go.



edit: Just had a look at this article which explains how to do the Remote Assistance ([http://www.digitalcitizen.life/how-provide-remote-support-windows-remote-assistance](http://www.digitalcitizen.life/how-provide-remote-support-windows-remote-assistance)) & then checked on my end if following the Win 8.1 procedure works for Win 10 also. It does. So basically Win 10 can assist Win 7 remotely, however for whatever reason the "Easy Connect" option is unavailable so the way to do it is via an invite file or an email (i.e. you sending from your end to mine).


---
**Jim Hatch** *September 24, 2016 21:37*

**+Scott Marshall**​ yep, Google+ sucks but it's free. Something like Discourse or even Wiki software to host the forum would cost $ and with the relatively decentralized nature of this and similar groups we might find it difficult to find someone willing to go thru the headaches & costs to host a true forum. So G+ it is.



The white tape that came with your new K40 is used to align the mirrors if you'd like. It's explained (not well) in the Chinglish manual. 🙂


---
**Bill Keeter** *September 25, 2016 20:09*

**+Scott Marshall** if you guys really wanted a forum. It probably wouldn't be too hard to setup phpBB. It's free and open source. Has a plugin for TapaTalk (popular mobile forum app). The main thing would be figuring out the Domain name and making a list of who should be made admins and moderators. Then it's a matter of seeing who already has hosting and bandwidth available. 



I could maybe host, but if the forum became too big we would probably have to move it or expand my hosting. Plus i'm a noob to the group so it seems a little inappropriate for me to host. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 26, 2016 01:46*

**+Bill Keeter** We actually have one already, setup through wordpress. Just needs a bit more content & users to make it worthwhile. We've had previous discussions about setting it up as a supplementary source of information with mixed responses from within the group & moderators (as there were concerns we would be poaching users from here).


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/D5wBinAzepx) &mdash; content and formatting may not be reliable*
