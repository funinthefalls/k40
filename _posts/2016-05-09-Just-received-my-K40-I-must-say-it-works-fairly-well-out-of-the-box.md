---
layout: post
title: "Just received my K40 I must say it works fairly well out of the box"
date: May 09, 2016 02:13
category: "Discussion"
author: "Jeff Kwait"
---
Just received my K40 I must say it works fairly well out of the box.  What would be the top mods to make it better??





**"Jeff Kwait"**

---
---
**Jim Hatch** *May 09, 2016 02:39*

Remove the exhaust manifold (blue flat intake right behind the back rail - leads to the exhaust port. Too small and gets in the way of the laser head.



Then get a LightObject air assist head, upgraded lens and air pump (aquarium air pump or airbrush compressor).



Get a water sensor from Amazon and wire it into your cooling water line so it will shut the laser down if you're not pumping water.



Safety switches for the top to turn off the laser if you're worried about someone opening it while the laser is going.



Smoothieboard & LaserWeb software.


---
**Vince Lee** *May 09, 2016 02:41*

Top of the list I think would be some kind of air assist or fan, better blower, and a water flow sensor to protect the tube.


---
**Scott Marshall** *May 09, 2016 05:17*

These are Free (or nearly so) and makes a big difference.



#1 -  Align the mirrors. Good project to get your feet wet, and after it's long journey, it most likely needs it. See link below.

You may want to do this AFTER #2 & #3, as they involve some disassembly and  will screw up you mirror adjustments.



#2 - +1 on removing exhaust "pipe" - it's held in with 4 screws around the rectangular outlet port. Slide the fan back in. A better fan should be on the list soon.



While you have it apart,

#3 - Remove the factory spring loaded clamp thing, and make a nail board with blocks to adjust height, then:



#4 - Run tests and find your focal point, change height of workpiece by small increments to see where it cuts thin material best, that is a good starting point for most work. Some people set up a piece of material in a 'ramp' to do this, I like to just raise an lower the deck, cutting a shape out at each step. Find where you get a good cut with the lowest power.

Start at about 50.8mm (2.00") from the lens, that's the approx focal length for the stock lens.



#5 - Add an air assist head (and compressor/pump) from Saite Cutter (ebay) is a HUGE plus.

It gives a better cut, allows the use of better lenses, keeps the lens clean, and Prevents FIRES!



#6 - Read thru our old stuff here, there's a ton of info hidden.Read, Read, and Read!



SAVE YOUR LASER!!!!!!!!!!!! Yes it was mentioned above, but it's important.

ALWAYS MAKE SURE YOUR PUMP IS ON. Dozens of people destroy the tube, by forgetting to turn on the pump, it only takes a few seconds with no water to kill it.  A cheap and easy 'fix' is to plug the laser and pump into a power strip so you have to turn it on (and your pump) to power on the laser. Verify flow before hitting "start"



Spend some time with it before forking out a lot of money just to "mod" stuff, You can spend a lot and not get much out of it, or spend a little intelligently and gain a lot.



Have fun!



Alignment: [http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



Good reading:[http://www.ophiropt.com/](http://www.ophiropt.com/)


---
**Phillip Conroy** *May 09, 2016 08:00*

Donot trust the shitty power adaptors -i changed the pulg ends to betterones with needing the adaptors


---
**Greg Curtis (pSyONiDe)** *May 11, 2016 20:22*

I would take a wooden shim and a piece of paper and cut a line in the paper going up the incline of the shim. Use the lowest setting possible and very slightly increase power until one area of the paper gets cut through. Measure the gap between this area and the tip of your laser output and that's your gauge length.



I then cut a small price of acrylic and made sure it matched the distance from the aforementioned measurement and now I use that as my gauge.



 I bring the head up (I have an adjustment screw) and hover over my workpiece with the gauge on it, and let the adjustable head set on the gauge, tighten and off I go.


---
*Imported from [Google+](https://plus.google.com/114834788519034865166/posts/1UyohfJivgd) &mdash; content and formatting may not be reliable*
