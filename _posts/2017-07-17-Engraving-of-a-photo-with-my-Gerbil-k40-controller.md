---
layout: post
title: "Engraving of a photo with my Gerbil k40 controller"
date: July 17, 2017 08:54
category: "Object produced with laser"
author: "Paul de Groot"
---
[https://upload.wikimedia.org/wikipedia/commons/b/be/Madrid_-_Fargo_Power-Wagon_WM300_-_130120_101212.jpg](https://upload.wikimedia.org/wikipedia/commons/b/be/Madrid_-_Fargo_Power-Wagon_WM300_-_130120_101212.jpg)

Engraving of a photo with my Gerbil k40 controller 

![images/2b1c3c9bc3203c658d8f606c0561be51.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b1c3c9bc3203c658d8f606c0561be51.jpeg)



**"Paul de Groot"**

---
---
**Anthony Bolgar** *July 17, 2017 12:27*

Looks good **+Paul de Groot**


---
**E Caswell** *July 18, 2017 07:08*

**+Paul de Groot** looking good and watching kickstarter with interest. :-) never know I might just put my hand in my pocket as well. 


---
**Paul de Groot** *July 18, 2017 09:52*

Thanks!


---
**Jeff C** *July 19, 2017 20:26*

WOW looks fantastic


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/XAFhThNQLfk) &mdash; content and formatting may not be reliable*
