---
layout: post
title: "I have been using different beds for a long time, mesh, wire, pin-bed and a bunch of other strange test beds"
date: October 17, 2017 17:04
category: "Modification"
author: "HP Persson"
---
I have been using different beds for a long time, mesh, wire, pin-bed and a bunch of other strange test beds. But then i started thinking, i only do acrylic cutting and always from larger sheets.



So i thought about the smartest and easiest bed to use...



This is what i ended up with, as a prototype :)

The bed has registration holes on two L-brackets per side so it always lines up perfectly in the machine, and i can do more frames to insert knowing they will always line up.

Since the picture i have added two "piano wires" to keep the acrylic from flexing when cut :)

Now i can cut all acrylic pieces into 240x320 size (my k40 is a bit expanded) and go for it. No aligning, no problems.



If anyone are in similar situation like me, keep it simple :)



![images/e2be67e390bc9212ba3a2d727385c986.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e2be67e390bc9212ba3a2d727385c986.jpeg)
![images/ef8526d9530a55563f3d8d239904a291.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef8526d9530a55563f3d8d239904a291.jpeg)

**"HP Persson"**

---
---
**Printin Addiction** *October 17, 2017 17:26*

Are the mounts custom made? That is one serious looking gantry.....


---
**HP Persson** *October 17, 2017 17:27*

**+Printin Addiction** No, L-brackets found in the hardware store. The tapped holes was already in the gantry i re-used :)

The frame just have registration bumps aligning them on the brackets.



I do have a movable z-head, so my bed is not critical at what height its mounted on. (head is removed in the pic)


---
**Printin Addiction** *October 17, 2017 17:34*

Sorry, I should have been more specific, the laser head mount and motor mounts (the gold anodized stuff)


---
**HP Persson** *October 17, 2017 17:35*

**+Printin Addiction** No, it´s a K40D , the newer model of the K40 machine. You can see more about it on Youtube if you search for K40D. Almost everything inside is different :)




---
**Martin Dillon** *October 18, 2017 14:10*

Awsome looking.  How well does the water cooler work?  Too bad, it looks like it still uses Corel Draw.


---
**HP Persson** *October 18, 2017 18:40*

**+Martin Dillon** There is K40D with Ruida controllers too, but the cost is pretty high :)

Which water cooler? i have a bunch of them, not sure which i have shown in here though :)


---
**Martin Dillon** *October 18, 2017 23:38*

I just did a quick search and came up with this one.  Which has built in water cooling. [es.aliexpress.com - Caliente! nueva! co2 40 watts mini máquina de escritorio grabado láser FL-K40D](https://es.aliexpress.com/item/HOT-NEW-co2-40-watts-mini-desktop-laser-engraving-machine-FL-K40D/32287999582.html)


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/YGx9hBR38tS) &mdash; content and formatting may not be reliable*
