---
layout: post
title: "I have started putting a new tube in my K40, how far should the laser be from the 1st mirror?"
date: May 10, 2018 14:48
category: "Discussion"
author: "Jerry Martin"
---
I have started putting a new tube in my K40, how far should the laser be from the 1st mirror? I’m guessing as my new tube is larger 66w!





**"Jerry Martin"**

---
---
**greg greene** *May 10, 2018 14:52*

Isn't that tube too long?


---
**Jerry Martin** *May 10, 2018 15:07*

I added an extension to add for the longer tube from light object plenty of room!


---
**Don Kleinschnitz Jr.** *May 10, 2018 15:49*

I can't think of a reason it really matters. The closer the better to decrease angular sensitivity but not so close as to make adjustments and targets a challenge. 


---
**greg greene** *May 10, 2018 15:52*

tube from light objects also?


---
**Kelly Burns** *May 10, 2018 18:11*

The distance doesn’t matter, but in the future take pictures before you remove old one.  



Also are the mounts big enough?  The diameter of a 60 watt tube is generally larger.  My true 40 Watt replacement required changing the mounts out.  


---
**Jerry Martin** *May 10, 2018 18:11*

**+greg greene** 



No purchased from another firm , twenty dollars difference between 40 w and the 66w!



Had to replace tube, broke due to cold


---
**Matt Staum** *May 13, 2018 01:19*

**+Jerry Martin** What length is your tube?


---
**Jerry Martin** *May 15, 2018 23:08*

**+Matt Staum** 40 inches


---
*Imported from [Google+](https://plus.google.com/111904744658622469563/posts/782hCGvwcJC) &mdash; content and formatting may not be reliable*
