---
layout: post
title: "I have just bought an in line extractor, as an upgrade to the original fan"
date: January 06, 2017 16:25
category: "Discussion"
author: "george ellison"
---
I have just bought an in line extractor, as an upgrade to the original fan. My plan is to create a sort of wooden panel with a rubber seal and a 100mm hole in the middle, this will sit in the slot where the old one was. They connect a small section of tube from the panel hole to the extractor and then a longer tube from the other end of the extractor, out the chimney. Does anybody know if this will work or have anybody tried this? Thanks





**"george ellison"**

---
---
**Don Kleinschnitz Jr.** *January 06, 2017 18:05*

My first version was like this. To get a better seal I ended up snapping off the slot brackets, added a foam gasket and screwed a floor duct to over the hole.


---
**george ellison** *January 06, 2017 19:19*

Thanks, sounds like that should work well, going to try a tight fit with a panel of ply with rubber seal around the edge, if too much smokes escapes then a more permanent solution is needed, even though you still need access tot he tube cover from time to time. 




---
**Phillip Conroy** *January 06, 2017 21:26*

If you can always put fan at end of line ,that way if any tiny leaks will only scuck in small amount of clean air,if at 1/2 way or at laser cutter the ducting will be pressurized and any holes will leak smoke into room.I  use 250 watt ,8 inch in linemetal fan at the end of 2 meter duct works very well,all depends on fan size .pp'sstock fan is 15wattsdo not waste money on small fans of less than 100 watts 


---
**Don Kleinschnitz Jr.** *January 06, 2017 21:50*

**+Phillip Conroy** Good idea.


---
**george ellison** *January 08, 2017 11:21*

Do you mean have it as close to the back of the machine as possible as apposed to nearer the exit of the tube. thanks




---
**Don Kleinschnitz Jr.** *January 08, 2017 11:32*

**+george ellison** I think **+Phillip Conroy** is saying that if you put the vacuum at the end of the duct nearest the exit, not at the machine, all of the pipe that is indoors is under vacuum not pressure. That way any leaks in the exit duct will suck in room air vs  blowing contaminated air into the room. i.e. put the vacuum right at the buildings exit.


---
**george ellison** *January 09, 2017 17:43*

Great thank you, will try that out




---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/Dz3Gn9yNAme) &mdash; content and formatting may not be reliable*
