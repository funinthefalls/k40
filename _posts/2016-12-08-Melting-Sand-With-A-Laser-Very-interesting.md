---
layout: post
title: "Melting Sand With A Laser! Very interesting"
date: December 08, 2016 03:43
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Melting Sand With A Laser!



Very interesting.



[http://www.williamosman.com/2016/11/melting-sand-with-laser.html](http://www.williamosman.com/2016/11/melting-sand-with-laser.html)





**"HalfNormal"**

---
---
**HalfNormal** *December 08, 2016 03:48*

I noticed **+Alex Krause** beat me to a post about William Osman on the LaserWeb group by a few minutes! I have to give credit where credit is due.


---
**Alex Krause** *December 08, 2016 03:49*

:)


---
**James Rivera** *December 08, 2016 06:19*

I wonder if tamping the sand flat before the burn/melt would improve the quality of the "print" by allowing better laser focus?


---
**Alex Krause** *December 08, 2016 06:22*

I would use what is known as play sand... the kind that is for sandboxes finer particulates and cleaner in general... all sorts of crap gets dumped into the ocean and beach sand can contain anything from plant,dead animal matter, plastic, metal ect...


---
**Brett Cooper** *December 08, 2016 06:27*

I guess I works best with silica sands rather than iron sands.

But it is pretty easy to turn glass into powder.



My laser is like 2watt so it doesn't stand a chance at melting glass but if I turned some lead free solder into powder it might work well with iron sand (filings) #todo  Sugar might also be worth trying on my low powered system.


---
**James Rivera** *December 08, 2016 06:38*

**+Alex Krause** I'm not sure what would go into doing this because I haven't Googled it yet :) but I wonder if making a "pulverizer" for smashing raw sand into smaller, more uniform size particles would be a cheap alternative to buying sand?


---
**Alex Krause** *December 08, 2016 06:39*

**+James Rivera**​ [https://www.google.com/shopping/product/13009553741287632007?lsf=seller:8740,store:11929435869678161578&prds=oid:7378604219417475030&q=play+sand+home+depot&hl=en-US&ei=GABJWNb3OMnbmQHGgYKoCA&lsft=cm_mmc:Shopping-_-LIAs-_-D22-_-100318476&lsft=gclid:CjwKEAiAg5_CBRDo4o6e4o3NtG0SJAB-IatYeGUVRQBV6i1Mb0KQyj3WrUqZD-zNt12ikc9mYrFjkhoCnBvw_wcB](https://www.google.com/shopping/product/13009553741287632007?lsf=seller:8740,store:11929435869678161578&prds=oid:7378604219417475030&q=play+sand+home+depot&hl=en-US&ei=GABJWNb3OMnbmQHGgYKoCA&lsft=cm_mmc:Shopping-_-LIAs-_-D22-_-100318476&lsft=gclid:CjwKEAiAg5_CBRDo4o6e4o3NtG0SJAB-IatYeGUVRQBV6i1Mb0KQyj3WrUqZD-zNt12ikc9mYrFjkhoCnBvw_wcB)


---
**James Rivera** *December 08, 2016 06:41*

**+Alex Krause** LOL! Ok, that's cheaper/faster than DIY sand!


---
**greg greene** *December 08, 2016 14:30*

Very cool - will have to try that - I wonder if pot metal would react the same - could  finally be a way of 'welding' all those broken parts ! :)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/5VgqR2k8YE3) &mdash; content and formatting may not be reliable*
