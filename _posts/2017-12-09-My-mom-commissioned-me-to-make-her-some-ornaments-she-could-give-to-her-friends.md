---
layout: post
title: "My mom commissioned me to make her some ornaments she could give to her friends"
date: December 09, 2017 14:31
category: "Object produced with laser"
author: "Ned Hill"
---
My mom commissioned me to make her some ornaments she could give to her friends. They are all big wine drinkers so here’s what I came up with.  3mm alder. The piece at the bottom was etched and cut out from between the glasses and then glued to the bottom. 

![images/ec631d90ed6b72acf93163ecb71a0a54.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec631d90ed6b72acf93163ecb71a0a54.jpeg)



**"Ned Hill"**

---
---
**Laser Junkie** *December 09, 2017 14:59*

Nice work


---
**HalfNormal** *December 09, 2017 16:15*

I have never seen green wine! 😊


---
**Ned Hill** *December 09, 2017 16:17*

It's actually yellow. :P The slightly bluish tint of the illuminating leds may make it look a bit green perhaps.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/SkA3khSJSaw) &mdash; content and formatting may not be reliable*
