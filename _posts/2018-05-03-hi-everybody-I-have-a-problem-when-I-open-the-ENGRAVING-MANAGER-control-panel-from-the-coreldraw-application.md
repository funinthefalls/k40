---
layout: post
title: "hi everybody, I have a problem when I open the ENGRAVING MANAGER control panel from the coreldraw application"
date: May 03, 2018 03:45
category: "Software"
author: "Luca Vianini"
---
hi everybody,



I have a problem when I open the ENGRAVING MANAGER control panel from the coreldraw application. the fields COMPANY and MODEL remain empty, while if I access the same panel through LASERDRW the fields are full and the laser works well. how can I do ?





**"Luca Vianini"**

---
---
**Ned Hill** *May 05, 2018 14:09*

Don't believe I have seen that problem before.  The only thing I can recommend is to uninstall corel laser and reinstall it.


---
*Imported from [Google+](https://plus.google.com/116006277179982995021/posts/QDfaybGug4G) &mdash; content and formatting may not be reliable*
