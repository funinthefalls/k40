---
layout: post
title: "Any recommendation on a replacement water pump?"
date: September 17, 2016 04:16
category: "Discussion"
author: "John Austin"
---
Any recommendation on a replacement water pump?

Since I am waiting on the replacement tube I would go ahead and get a water pump.









**"John Austin"**

---
---
**Scott Marshall** *September 17, 2016 05:15*

These are about the right size and you'll never go wrong with a Little Giant product (I've used hunderds of the industrial ones and they are incredibly reliable)



 I don't know for sure about the new cheaper line, they look Chinese, and I have not tested them.



 I've got a S1200 T6 in service right now, but it's way too big (bought it super cheap so didn't care) - I've got it slowed down electronically  and have a PE-1 due in  to test. I've used them before for parts washers and similar service. The next step up - the 2 series  (jumps up to 1/40hp from the 1/125hp for the PE-1) would probably be a little on the large side for a K40, but well within throttle-able range. For the extra $20-$30, the Series 2 version might be a wise choice, especially if you plan on running a cooling loop or radiator.



I expect the PE-1 (sometimes called a PE-140 - LG has a loose numbering system) to be about twice as powerful as the Stock K40 pump (which is about proper sizing for the K40 tube) and a good more reliable. 

The PE-1 (or PE-140) comes in a variety of 'styles" - outlet positions, strainer type etc, but they all use the same impeller/rebuild kit (YES - you CAN get parts, and they cost less than a new pump) 



Peformance on a PE-1 should yield about 1.5-2gpm (about twice stock) which is enough to easily flush all the bubbles out and eliminate 'hot spots' but it shuts off at 12' so (About 4psi) so shouldn't  blow off tubing connections or hurt the tube.  They can be had for <$50 (paid $38 shipped for mine on the bay but that was an auction)



If my testing works out, A PE-1 will be part of a cooling kit I'm developing.





Look for models #518200(series 1) and 518400(series 2)



Scott







[littlegiant.com - littlegiant.com/media/130962/995104.pdf](http://littlegiant.com/media/130962/995104.pdf)



Here's the line of pumps we're looking in (includes the series 2)

[http://www.little-giantpump.com/where_to_buy_little_giant_small_pumps.htm](http://www.little-giantpump.com/where_to_buy_little_giant_small_pumps.htm)


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/TFLWxUSNvui) &mdash; content and formatting may not be reliable*
