---
layout: post
title: "My K40 is due tomorrow, took advantage of a temporary price drop to finally order one from eBay last Friday"
date: August 13, 2015 17:15
category: "Discussion"
author: "Kirk Yarina"
---
My K40 is due tomorrow, took advantage of a temporary price drop to finally order one from eBay last Friday.  I'll have to wait to find if it's going to be agony (well, other than the agony of waiting)  or ecstasy...





**"Kirk Yarina"**

---
---
**Kirk Yarina** *August 16, 2015 15:20*

It arrived Friday, double boxed, rigid foam packed, soft foam around laser.  Came from love_happyshopping (has a good reputation for CNC parts) for $365 delivered.



Unpacked, check connections, screws, etc., hooked up the water cooling.  Test fired the first time!  Came with LaserDRW and an ancient version of CorelDraw with a plugin.  The controller is an M2Nano, not a moshiboard.  All surface mount components, no potentially loose socked chips.  The dongle says Lihuiyu.  There were a couple loose screws that I replaced, the sheetmetal piece that holds the controller is slightly bent so the USB socket doesn't quite line up, and the enable switch needed a bit of wire rearrangement so it wasn't crooked, but no real problems.  I played around a bit with some scrap cardboard and thin plexiglass, then the wife complained about the smoke (need more vent hose to reach the basement window) in the basement.  The (big surprise :) ) software isn't quite ready for prime time, with annoying issues like it won't open files on my network drive, but it's usable.  It has an exercise option to run the axis back and forth to wear them in; and  the same "dry" bike lube from the 3D printer quieted them down a bit.  It will do a bit over 300x200mm of travel without hitting the stops.



I used a slightly under 5 gallon drywall-style bucket with tap water, then picked up 4 gallons of blue windshield washer fluid (per a recommendation on another forum) yesterday to refill it.  I've got a couple flow switches coming, and microswitches on hand for safety switches.  I'll 3D print an air assist nozzle "real soon now", and already have a 65lpm aquarium air pump I can borrow from the cnc router.



The plan is to use it essentially as-is for now, figure out what it'll do without any mods first.  Lots to learn.



One quick question, how do you use a workpiece that's bigger than the little spring clamp mechanism?  Just lay it on top of the platform, or do I need to take the table out and support it from underneath to get the focus distance right if it's thicker?  If that's the case, is the platform hard to remove?



Thanks!


---
**Kirk Yarina** *August 20, 2015 23:43*

Thanks, Carl!  I discovered that it works fine with objects laid on top of the clamp, too.  The focal distance probably isn't quite right, but so far it's good enough.  Honeycomb or something similar is starting to look good, the clamp mostly just gets in the way.



Blue WW fluid is working OK so far, the pail looked full enough with 3 gallons, and there doesn't seem to be any noticeable temperature rise.  It's sitting on a concrete basement floor, which probably helps.  It should help keep the inside of the glass clean, but I wonder how long it'll take for the methanol to evaporate out, and if that'll matter.  I taped the top edge of the included blower, and even engraving a silicone Kindle cover (lots of smoke and flame, only a small depression when cleaned up) there wasn't any detectable odor.



Been experimenting to see what it'll do before trying any mods, but the flow switch came yesterday so it and maybe an interlock microswitch are first on the list.  The laser enable wiring was easy to trace (it's pink) so it'll be easy to add them without cutting the factory wiring.  Air assist is next, need to spend a day with the 3D printer first, needs a little attention to some minor issues.



Did a quick pass over my laser logbook (every device gets it's own notebook), minimal power did a nice job of removing the black on the front cover.  It now has a laser triangle/starburst on it.   Tried a photo on maple, not a lot of detail.  Two layer engraving tags work great, but it takes some experimenting to find the point where the color layer comes off but the layer under it looks good.  Having fun!


---
**Kirk Yarina** *August 30, 2015 15:38*

Me, too!  Well worth the money.   Semi-retirement means all it has to be is fun and something new and interesting to learn :)



The nearest HD is 100 miles away; I got a couple of the reed switch flow detectors, but their metric pipe thread means adapters are hard to find in the US north woods.  LO has an inexpensive pressure operated switch with barbed fittings that looked worth a try when I ordered their air assist (3D printer non-functional for an upgrade), the reduced hassle was worth the price.  The laser enable on my machine is easy to get to and flow/interlock switches won't take any wire cutting and splicing to install.



So far it does a decent or better job on two layer engraving tags, window Plexiglass, silicone device cases, couple different types of wood, and now experimenting with engraving glass using the wet newspaper technique.  Anodized aluminum could be better, but that looks like yet another software issue.


---
*Imported from [Google+](https://plus.google.com/+KirkYarina/posts/SyavC1FEb1B) &mdash; content and formatting may not be reliable*
