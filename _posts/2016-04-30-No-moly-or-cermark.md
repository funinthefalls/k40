---
layout: post
title: "No moly or cermark"
date: April 30, 2016 04:04
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
No moly or cermark 
{% include youtubePlayer.html id="N--NIUI7AbE" %}
[https://youtu.be/N--NIUI7AbE](https://youtu.be/N--NIUI7AbE)





**"Ariel Yahni (UniKpty)"**

---
---
**Scott Thorne** *April 30, 2016 11:47*

True...but there is a big difference between burning a coating off to produce an image and bonding an image to metals.


---
**Ariel Yahni (UniKpty)** *April 30, 2016 12:00*

Agree. Not the same. But have you tried this before?


---
**Jim Hatch** *April 30, 2016 12:57*

Anodization is a coating used to prevent further corrosion of aluminum (the light grey of aluminum is really a very light layer of corrosion but that then keeps it from corroding like rust that will eat the metal until it's all gone). 



IPads, Macbooks, etc all mark ("etch") great with lasers. We mark all of our company ones with our logo, "property of", phone number, website, etc. with a laser. Works great and sort of helps keep them from walking :-) 


---
**Scott Thorne** *April 30, 2016 13:09*

**+Ariel Yahni**...go to my Google page and scroll down through the pics....I did a silver I pad....the pic is on my page...it turned out great.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/88UpV8fdLnB) &mdash; content and formatting may not be reliable*
