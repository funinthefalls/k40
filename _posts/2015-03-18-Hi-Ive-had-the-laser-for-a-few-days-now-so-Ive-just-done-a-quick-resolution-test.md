---
layout: post
title: "Hi. I've had the laser for a few days now so I've just done a quick resolution test"
date: March 18, 2015 21:09
category: "External links&#x3a; Blog, forum, etc"
author: "Eddie Pratt"
---
Hi. I've had the laser for a few days now so I've just done a quick resolution test. The bars and boxes are 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0.5, 0.25 and 0.1mm. I can't see much difference between the 0.25 and 0.1mm. It's lightweight card (about 160gsm) and laser settings were 6mA power and 34mm/s speed.



[For those that work in imperial: 0.394, 0.354, 0.315, 0.276, 0.236, 0.197, 0.157, 0.118, 0.079, 0.039, 0.0197, 0.0098, 0.0039 inches]

![images/e3c045c4a3861bae54f5c427e59a06e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e3c045c4a3861bae54f5c427e59a06e5.jpeg)



**"Eddie Pratt"**

---
---
**Jim Coogan** *March 18, 2015 22:18*

Great test.  I need to do that once I put mine back together.  Here's an interesting article on lens resolution.  [http://www.parallax-tech.com/faq.htm](http://www.parallax-tech.com/faq.htm)


---
**Eddie Pratt** *March 18, 2015 22:45*

Thanks Jim. That article is full of really useful info. Thank you.


---
*Imported from [Google+](https://plus.google.com/108440927309352829036/posts/dPHTtCduF4e) &mdash; content and formatting may not be reliable*
