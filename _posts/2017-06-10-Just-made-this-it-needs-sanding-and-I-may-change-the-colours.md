---
layout: post
title: "Just made this, it needs sanding and I may change the colours"
date: June 10, 2017 19:47
category: "Object produced with laser"
author: "Don Recardo"
---
Just made this, it needs sanding and I may change the colours



![images/94914942c9e311a520422c51291b8b89.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/94914942c9e311a520422c51291b8b89.jpeg)
![images/308d9ee52162f6b308b3afffb7906db3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/308d9ee52162f6b308b3afffb7906db3.jpeg)
![images/4f0a0a4b4d246eec1ef09c332972b1c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f0a0a4b4d246eec1ef09c332972b1c5.jpeg)

**"Don Recardo"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 10, 2017 20:24*

Holy cow! That looks very complicated. How many layers in it?


---
**Don Recardo** *June 10, 2017 20:26*

eight




---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 10, 2017 22:22*

It seems like a nice effect. This reminds me I would be interested in trying some more multilayered pieces in future :) Thanks for sharing.


---
**Abe Fouhy** *June 11, 2017 00:25*

Awesome!


---
**Ned Hill** *June 11, 2017 02:42*

That's awesome, nice job. :)




---
**Don Recardo** *June 11, 2017 10:55*

Thank you for all the kind comments


---
*Imported from [Google+](https://plus.google.com/106290033771296978518/posts/YiQXG1pr8xr) &mdash; content and formatting may not be reliable*
