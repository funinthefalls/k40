---
layout: post
title: "Hello, I am new to all this stuff"
date: September 06, 2016 14:49
category: "Discussion"
author: "A Little Bit of Everything"
---
Hello, I am new to all this stuff. I just got a K40 laser and I am looking to upgrade it. I have been looking at several options. Either a Smoothie board or one of these [http://www.panucatt.com/azteeg_X5_mini_reprap_3d_printer_controller_p/ax5mini.htm](http://www.panucatt.com/azteeg_X5_mini_reprap_3d_printer_controller_p/ax5mini.htm) any thoughts which would be better?





**"A Little Bit of Everything"**

---
---
**A Little Bit of Everything** *September 08, 2016 18:17*

No comments? How about the Light Object X7 DSP kit? I am looking for some feedback before I pull the trigger on one of these. Any help would be appreciated.


---
**Rich Barlow** *September 12, 2016 13:31*

I'm running mine right now on a Arduino running GRBL and a Protoneer CNC shield.  I works but it can be a bit temperamental with disconnects and a slight weirdness around the edges.  I ordered a Smoothie last week though - my preferred software (LaserWeb3) has features that are only available with the Smoothie (like acceleration based power).  Plus the work folks do with that board looks pro to me!  


---
*Imported from [Google+](https://plus.google.com/103167102860682775651/posts/eQi4kaLqcEY) &mdash; content and formatting may not be reliable*
