---
layout: post
title: "DIY Laser Bed I have been fooling around trying to make an inexpensive adjustable table for the K40"
date: November 12, 2016 00:57
category: "Modification"
author: "HalfNormal"
---
DIY Laser Bed



I have been fooling around trying to make an inexpensive adjustable table for the K40. It is not too easy to keep it cheap and usable! I have been basing it on a lift table design but this only gives about 2 inches of usable travel. An adjustable head would be a better option with that amount of limited travel. So the table is still a work in progress but the bed has been made. It is not any cheaper than the LightObject bed but was fun making. It consists of some c-channel aluminum, expanded metal and range filters.





![images/d85a6afae3c88645b45eb272b557670a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d85a6afae3c88645b45eb272b557670a.jpeg)
![images/7554cdd69d11a07ddab5f5f3e6ba6654.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7554cdd69d11a07ddab5f5f3e6ba6654.jpeg)

**"HalfNormal"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 12, 2016 05:12*

When you say range filters, are you referring to range-hood filters? If so, I think it's a great idea, although small parts will not be able to fall all the way through. So I'm 50/50 on it.


---
**HalfNormal** *November 12, 2016 05:17*

Yes range - hood filters. You can put a small shim under the item to let parts drop.


---
**ben ball** *November 28, 2016 00:42*

I've been on the fence about buying one of these lasers and not having an adjustable bed has been one of the hold ups. 




---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/NmPg85Xxhv6) &mdash; content and formatting may not be reliable*
