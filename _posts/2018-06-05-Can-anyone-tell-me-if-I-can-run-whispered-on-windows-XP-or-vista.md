---
layout: post
title: "Can anyone tell me if I can run whispered on windows XP or vista?"
date: June 05, 2018 22:52
category: "Software"
author: "That Bearded Guy"
---
Can anyone tell me if I can run whispered on windows XP or vista?





**"That Bearded Guy"**

---
---
**James Rivera** *June 06, 2018 00:17*

If you look at the setup page, it mentions Vista and XP for the USB driver install, which seems to imply it works on them.



[scorchworks.com - K40 Whisperer USB Driver Setup](http://www.scorchworks.com/K40whisperer/k40w_setup.html)




---
**That Bearded Guy** *June 06, 2018 13:24*

Ok great I must have missed that part. Waiting for my laser to co.e in and also looking for a desktop tower to run it from. I keep finding alot of older towers cheap. 


---
**James Rivera** *June 06, 2018 15:54*

You don’t need much. I’m using my 3rd oldest PC, an Intel Q6600, running Win7. I’d throw Linux on an old PC before using Vista or XP though. You could even use a Raspberry Pi.


---
*Imported from [Google+](https://plus.google.com/+ThatBeardedGuy/posts/75E75Vvbqih) &mdash; content and formatting may not be reliable*
