---
layout: post
title: "I purchased a K40 laser on Ebay"
date: September 03, 2016 16:32
category: "Software"
author: "Rix Metal Worx"
---
I purchased a K40 laser on Ebay. my first laser ever. It came with Corel Laser, Laser Draw and Winsealxp. But it did not come with Corel Draw. What version of Corel Draw would be the best bang for the buck to purchase to use exclusively for the laser, I have a dedicated laptop just for the laser with Windows 10. I have never used Corel Draw so I am totally in the dark on what I should purchase.





**"Rix Metal Worx"**

---
---
**Phillip Conroy** *September 03, 2016 17:36*

I use corel x7 student and hve not had any issues,works well,has taken over a year to learn both laser cutter and corel-read read and when in dout read some more


---
**HalfNormal** *September 03, 2016 17:39*

Here is the hard truth. A legitimate copy of CorelDraw X7 starts at $299.00. If you purchase an older version, it will soon be no longer supported. For that price, you can upgrade your hardware and use any graphics program, free or pay, on the laser. It really depends on your comfort level with hardware and learning new software. **+Scott Marshall** has some great boards to help with the transition from old to new hardware.


---
**Rix Metal Worx** *September 03, 2016 17:44*

The only graphics program I have ever used is Photoshop. Would be nice to be able to use it. I am very comfortable with mechanical hardware, not so much with the electrical aspects of it, but I am not afraid to tackle it when pointed in the right direction.


---
**Rix Metal Worx** *September 03, 2016 19:21*

Going off the deep end. Building a beast. [http://www.lightobject.com/X7-DSP-Controller-card-for-CO2-Laser-Engraving-Cutter-with-color-screen-P940.aspx](http://www.lightobject.com/X7-DSP-Controller-card-for-CO2-Laser-Engraving-Cutter-with-color-screen-P940.aspx)


---
**Jim Hatch** *September 03, 2016 20:21*

There should be an install for Corel 12 on the DVD that came with the K40. It might be a RAR file if I recall correctly but don't hold me to it. But if you can't find it **+Yuusuf Sallahuddin**​ on the group has it loaded in the cloud somewhere he's got a link for. Once it's daytime on his side of the world I expect he'll weigh in.



Otherwise any version of Corel <b>except</b> the Home & Student versions will work. I'm using X8 myself which is the latest but 12 was fine - just gets easier to do image tracing and prepping photos for engraving.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 04, 2016 06:58*

**+Rix Metal Worx** The closest that I have hosted still is the original CD that came with my K40 (as a .rar file). ~700mb. It contains the copy of CorelDraw12 that came with the K40 & Corel Laser plugin, etc.



[https://drive.google.com/file/d/0Bzi2h1k_udXwamRBZkduekxlMHM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwamRBZkduekxlMHM/view?usp=sharing)


---
**Jaime Menchaca** *September 04, 2016 08:21*

Awesome thanks **+Yuusuf Sallahuddin**​ i just recieved my laser and also didnt have the corel draw included , hope you dont mind ill also be downloading and trying the software  cd you have uploaded


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 04, 2016 08:22*

**+Jaime Menchaca** You're welcome to grab it :) It seems that a few people didn't receive the CD or the software, which kind of makes the laser useless haha.


---
**Rix Metal Worx** *September 04, 2016 17:57*

**+Yuusuf Sallahuddin** thanks ! Downloaded it no problem.  


---
**Armando Araiza** *December 14, 2016 23:35*

**+Yuusuf Sallahuddin** is there a venmo account or any way I can send you money for a coffee? you just saved me time as I have misplaced my cd I will be downloading this evening thank you again!








---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2016 06:33*

**+Armando Araiza** You're welcome. No need for a coffee donation. Unless you would like to donate to something more awesome, the beer fund for the guys developing LaserWeb (open source software to run our lasers & a variety of other machines). Check out the community here: [https://plus.google.com/u/0/communities/115879488566665599508/stream/4ecb0765-98e2-46d0-a226-b5409eb5d8f2](https://plus.google.com/u/0/communities/115879488566665599508/stream/4ecb0765-98e2-46d0-a226-b5409eb5d8f2) & if you feel like swinging those guys some $ for a beer (to keep them working hard) would be awesome.


---
*Imported from [Google+](https://plus.google.com/115916575345683901299/posts/XmRV62yNJfP) &mdash; content and formatting may not be reliable*
