---
layout: post
title: "Adjusting mirrors and focus on my K40"
date: December 23, 2018 21:53
category: "Hardware and Laser settings"
author: "Sotm"
---
Adjusting mirrors and focus on my K40. All mirrors beam shows as a dot and I have that centered in all mirrors. But when the beam leaves the focal lens it is a line instead of a dot. Why?  Is the focal lens bad?





**"Sotm"**

---
---
**Joe Alexander** *December 23, 2018 22:20*

make sure it isnt being clipped by the end of your laser head nozzle, this can cause beam deformation. 


---
**Sotm** *December 23, 2018 22:26*

No air nozzle installed.  Not hitting the inside of focal tube either.


---
**Don Kleinschnitz Jr.** *December 23, 2018 22:30*

Cracked lens or mirror.


---
**Sotm** *December 23, 2018 22:33*

I get perfect dots al the way through the mirrors


---
**greg greene** *December 23, 2018 23:05*

Then it's a bad focus lens


---
**Sotm** *December 24, 2018 01:31*

Thanks


---
**Don Kleinschnitz Jr.** *December 25, 2018 18:15*

Also check that the beam is coming into and out of the lens holder parallel and in the center. If not the beam can be deflected and loose power.


---
*Imported from [Google+](https://plus.google.com/108625883590527536778/posts/DZsq9mBko5t) &mdash; content and formatting may not be reliable*
