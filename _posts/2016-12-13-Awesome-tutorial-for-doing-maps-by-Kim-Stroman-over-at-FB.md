---
layout: post
title: "Awesome tutorial for doing maps by Kim Stroman over at FB"
date: December 13, 2016 05:01
category: "Software"
author: "Ariel Yahni (UniKpty)"
---
Awesome tutorial for doing maps by **+Kim Stroman**​ over at FB. Could you post some details at this community please





**"Ariel Yahni (UniKpty)"**

---
---
**K** *December 13, 2016 05:11*

Surely! I need to get better at posting over here. It's pretty simple. Using [openstreetmap.org - OpenStreetMap](http://www.openstreetmap.org/) you can download vector (svg) files of maps. To do this you click on the right hand side SHARE button. It brings up a download option where you can download your map. There's also an export button on the left, but that exports oms files, which aren't easily editable in vector program. When I opened my SVG in Illustrator all the text was giant and it obscured the map. The letters were one over the other and I had to delete them to reveal my map.  You can then edit and delete any info you don't want to create the map you want to cut. 


---
**K** *December 13, 2016 05:11*

This is where you download your map.

![images/849007c847e84437c5aa95650322ae69.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/849007c847e84437c5aa95650322ae69.jpeg)


---
**K** *December 13, 2016 05:12*

What my map looked like right after downloading.

![images/fb96fed3ccc2e2117775847782d7c543.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb96fed3ccc2e2117775847782d7c543.jpeg)


---
**K** *December 13, 2016 05:14*

The final product. It's two layers. The top layer are the streets. The bottom is engraved with a compass rose, map key, and some features I wanted to show (water, parks, trails).

![images/214dfd521d9691a03d4442657a7cb6c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/214dfd521d9691a03d4442657a7cb6c5.jpeg)


---
**Ariel Yahni (UniKpty)** *December 13, 2016 11:30*

Awesome thanks


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2016 12:06*

Wow, that's really nice :) I've been wanting to have a go at something like this for a while. Thanks for sharing.


---
**Anthony Bolgar** *December 13, 2016 14:15*

Is this site for US maps only, it can't find my Canadian address?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2016 14:43*

**+Anthony Bolgar** I found my address in Australia, so it seems dependent on the area. From what I read on the site the maps are possibly made by users.


---
**K** *December 13, 2016 16:41*

**+Anthony Bolgar** Well dang. I looked at it zoomed out at first and just assumed it was for all addresses. :-(


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/N17LdU2DXhn) &mdash; content and formatting may not be reliable*
