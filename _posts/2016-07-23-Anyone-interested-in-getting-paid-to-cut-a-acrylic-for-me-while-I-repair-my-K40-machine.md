---
layout: post
title: "Anyone interested in getting paid to cut a acrylic for me while I repair my K40 machine?"
date: July 23, 2016 04:49
category: "Discussion"
author: "Jeff Sells"
---
Anyone interested in getting paid to cut a acrylic for me while I repair my K40 machine?  If interested, drop me an email at jeff.sells@jsells.us





**"Jeff Sells"**

---
---
**Alex Krause** *July 23, 2016 05:08*

What kind of items?


---
**Jeff Sells** *July 23, 2016 05:21*

Go to Etsy.com  and search on "acrylic cake topper" - this is what I am needing to cut out.


---
*Imported from [Google+](https://plus.google.com/104512730011153775816/posts/RSN2SUPykdy) &mdash; content and formatting may not be reliable*
