---
layout: post
title: "Laser Peltier cooling system Part 2. There are a lot of ways of doing this and this was how I did it"
date: May 24, 2017 03:51
category: "Modification"
author: "Steve Clark"
---
Laser Peltier cooling system Part 2.



 There are a lot of ways of doing this and this was how I did it. If I was to do it again, somethings I would do differently.

 

The tank and outer shell plus two cans of insulating foam are needed. I’ll try to post pictures in order below.

 

I put blocks of 5/8” wood to allow the foam to expand at the bottom in between the shell and red tank . You need to Drill 1/2 “ holes about every six  inches up and down the sides and in the bottom no matter what you make your shell out of. I didn’t drill enough and had some problems with bulging from the foam expanding. I fit checked the tank before hand and built a top for the shell with clearance for foam and tubes. The top will be screwed on.



The red tank inside the shell needed holes drilled in the top and a square cut in it. I drilled the holes before I set it in foam and washed chips out from the inside but had to do more holes after the foam had set. So I made sure to wash them out later. The square is for a float switch. This I will be hooked up to a a warning light to let me if I’m low on water. I added a vent hole also that can also be used to add algaecide without taking the lid off. The plumbing outsidewill have some tee’s in it so that I can pump water out or add to it without removing the tank from under  the stand.

  

The foam needs to be applied in layers otherwise it will not setup.  The bottom first (3/4 “ thick max) position the red tank and put a heavy weight on it to keep in place. Wait at least an hour or overnight is even  better. BTW- I Put a couple of 2 x 2’s under the shell so that the excess foam could easily come out the ½” holes in the bottom.  



A trick that worked for me with the spray foam was I flushed the nozzle out with acetone in between layers to avoid clogging of the can.  With each layer be sure to put no more than 1 inch of foam or it will not cure correctly. Again wait at least an hour or more between those layers.  



Do not over fill the wet foam and do not cover the foam/ shell with anything ( I found out the hard way).  It will slow and prevent  the “set”. Misting a little water on each layer is suppose to help.



I was limited to 13.5 inches height to fit under my stand so the cover I built out of wood was to fit. Holes are drilled in the edge of the cover to allow for the coolant lines and electrical wire from the float.



However you set up your shell, once the foam has set and dried completely any excess can be shaved off with a long serrated bread knife (Don’t tell the wife I used it). The foam that flowed out the ½ inch holes was cut or busted off.



The ½  Poly tubing  I used came with a curve from being coiled so I had to heat it up with a heat gun at about 275F to get the curve out then quenched in water to hold the shape. It turns kind of clear when its ready to be bent. Afterwards I heated in the 90 degree angles  so that I could come out the side. Next I used a old solder gun tip to mold a groove in them to help hold the silicone tubing on when it was tie wrapped. 



Next in part 3 the Peltier and wiring.







![images/1e77b46da8e966548822356455492061.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e77b46da8e966548822356455492061.jpeg)
![images/893d7f4e04638195e47c9f443658cddb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/893d7f4e04638195e47c9f443658cddb.jpeg)
![images/8019513618984fbe38699f1ca2501958.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8019513618984fbe38699f1ca2501958.jpeg)
![images/a3a542826317f629605c1a9af6f093ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3a542826317f629605c1a9af6f093ce.jpeg)
![images/db7b0a1fb93ed38813baefcb8eee91c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db7b0a1fb93ed38813baefcb8eee91c4.jpeg)
![images/4c2854b54c80f7b7d8a617d10e2c43bb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c2854b54c80f7b7d8a617d10e2c43bb.jpeg)
![images/0012f9d834a7829f5b902f4645bc223f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0012f9d834a7829f5b902f4645bc223f.jpeg)

**"Steve Clark"**

---
---
**Phillip Conroy** *May 24, 2017 06:41*

Does it work 


---
**Jorge Robles** *May 24, 2017 06:53*

Very interesting


---
**Steve Clark** *May 24, 2017 08:04*

Yes it will work when I've added the power box with main breaker switch, fuse, a 30 amp 12v  ps, junction racks. The peltiers outside the box, thermostat control, and relay... the float and lighting circuits and add the new flow switch control. then build the mounting rack for peltiers and fan's.












---
**Jorge Robles** *May 24, 2017 08:06*

LOL, that's quite a work. What wattage of peltiers have you calculated? Does the flow of the pump affect the chilling?


---
**Beau H** *May 25, 2017 13:58*

The blue can of spray foam for windows and doors might work better for this as it's designed to expand and fill but not apply crazy pressure like the red can does. 


---
**Steve Clark** *May 25, 2017 17:01*

**+Jorge Robles** When I change over to the new system I’ll see how many liters/per minute my pump puts out through the new cooling system.  The spec says at one foot 510 GPH but that is with ½ pipe. The pump could be flow controlled by some sort of line restricted by pass or such. I got this impeller pump so I could have added flow volume ability without too high of pressure.



The Peltier cooler is rated 180 watt so that should give me about 126 actual cooling watts. I think it will be enough, but if not, I can add 60 watts (48 cooling watts)at a time if I can’t balance the system by flow rate. The Pelter system will be thermostat controlled so assuming I have enough cooling flow it will be turning on and  off. The coolant goes directly to the tank to mix.




---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/XZG1aB35SV3) &mdash; content and formatting may not be reliable*
