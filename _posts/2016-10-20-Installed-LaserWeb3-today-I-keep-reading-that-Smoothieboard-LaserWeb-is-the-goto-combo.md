---
layout: post
title: "Installed LaserWeb3 today. I keep reading that Smoothieboard/LaserWeb is the goto combo"
date: October 20, 2016 22:59
category: "Software"
author: "Kris Sturgess"
---
Installed LaserWeb3 today. I keep reading that Smoothieboard/LaserWeb is the goto combo.



Im looking to get a Smoothieboard setup in the near future.



Can someone tell me what LaserWeb can do for me? Is it just a interface for controller?  Or is it for graphic editing? Or both?



I'm looking to do a setup similar to Octoprint that I use on my 3D printer (OctoPrint/RPi3).



From what ive read this is somewhat similar (webcam/RPi/Esp8266).



Just looking for some basic info. Overwhelmed with everything out there.



Still fumbling my way around with Laserdrw on a stock K4 for now.



Kris







**"Kris Sturgess"**

---
---
**Ariel Yahni (UniKpty)** *October 20, 2016 23:17*

**+Kris Sturgess** LaserWeb is the part in the process where your graphics become machine language. So that means telling the laser where to cut/raster, speed and power. You can run LW on and RPi many of us do.  Please take a look at the wiki here [https://github.com/openhardwarecoza/LaserWeb3/wiki](https://github.com/openhardwarecoza/LaserWeb3/wiki) for details on getting LW on RPi plus other important processes related to your question. Dont hesitate to come back with more questions, we are here to help

[github.com - LaserWeb3](https://github.com/openhardwarecoza/LaserWeb3/wiki)


---
**Kris Sturgess** *October 20, 2016 23:31*

So it's like a slicer of sorts. I use Simplify 3D/Octoprint.






---
**Ariel Yahni (UniKpty)** *October 20, 2016 23:35*

You could say that, the big difference is that this is the first OpenSource, fully browser based, easy to use workflow, plus it not only does laser but also cnc. When the project started there was nothing close to what this is today


---
**Kris Sturgess** *October 20, 2016 23:38*

I'll play with it some more when I get back home. 



So with Inkscape and LaserWeb I should be pretty much in business?


---
**Ariel Yahni (UniKpty)** *October 20, 2016 23:42*

Oh yes. Just make sure all lines and circles are path so in inskcape that is object to path and stroke to path


---
**Jim Hatch** *October 21, 2016 01:00*

Consider it CAM with a dose of CAD. You can do design (CAD) but this is where manufacturing (CAM) is enabled. You can still use your favorite CAD package like AI, Corel or Inkscape to create your designs but let LaserWeb drive the cutter.


---
**HalfNormal** *October 21, 2016 01:14*

**+Ariel Yahni** You left out drag knife! :-)


---
**Ariel Yahni (UniKpty)** *October 21, 2016 01:22*

**+HalfNormal**​ indeed and I need to add a holder for the knife to my cnc


---
**Don Kleinschnitz Jr.** *October 21, 2016 01:23*

Or Sketchup/SketchUCam






---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/aJ9ML7Z8YDr) &mdash; content and formatting may not be reliable*
