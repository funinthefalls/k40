---
layout: post
title: "Ok. On to the next *%)@# Challenge!"
date: December 07, 2016 00:07
category: "Modification"
author: "Bob Damato"
---
Ok. On to the next *%)@# Challenge! I put in my drag chain with the wire and hose through it.  Nice! But now of course I have a new challenge to overcome. Now I notice that when I cut/engrave the  right half of my designs dont come out.  Well, it seems the drag chain curl gets in the way of the beam! I need a way to have pressure toward the back wall to keep it out of the way.  Any suggestions?





**"Bob Damato"**

---
---
**Don Kleinschnitz Jr.** *December 07, 2016 00:26*

Oh crap....I haven't turned mine on yet have to check when I get home.... Picture of your install?. 


---
**Bob Damato** *December 07, 2016 00:58*

[http://maxboostracing.com/laser.jpg](http://maxboostracing.com/laser.jpg)   The picture is me in the middle of doing an engraving and you  can see the chain is frightfully close to the beam line!  If the item were  moved up more Id be screwed.


---
**Bob Damato** *December 07, 2016 00:59*

Oh, and yes my air assist hose is kinked, it wont turn that radius, I need to find a spring or something to put inside there.  in other words, yes, yet another challenge :)


---
**Kelly S** *December 07, 2016 02:22*

**+Bob Damato** I suggest a Air Brush hose with the sleeve removed, makes for a super flexible hose.  Onto chain position; try to mount it in a way that the first few links are either perfectly horizontal or aimed back just a tad.  Should prevent further issues.  Softer hose may cure it all together.  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 07, 2016 06:10*

**+Bob Damato** Mount it further back on the carriage plate. I have mine attached to the back most screw on the carriage plate. Only issue I have is it gets awfully tight at the back of the cutting bay (i.e. drag chain pushes up against back wall when at Y=0). Can sometimes hear it slapping the back wall when doing fast cuts up that end of the machine.


---
**Mircea Russu** *December 07, 2016 06:35*

Use some rubber band going from about 1/3 of the chain to the back-right corner. Not very light and also not very tight :) I would post a pic but my machine is torn apart for rail upgrades. 


---
**Bob Damato** *December 07, 2016 09:58*

**+Yuusuf Sallahuddin**  If I mount it further back then it wont make the radius and I cant go to my 0,0 point.  


---
**Don Kleinschnitz Jr.** *December 07, 2016 12:13*

**+Bob Damato** thanks for the heads up I had a similar problem but did not know it yet. Mine is in the upper left position it was interposing the output of #1 mirror. 

I think I fixed it by removing two links I now use 24 links. The mounting position on the head plate and back wall are important in getting the geometry right at the extremes while keeping the chains loop at a minimum. Another improvement I will do later is to move the mounting point on the head plate a bit right to give even more clearance. 

I haven't tested this dynamically as my machine is down while I cut the rear air vent back.

I did not notice a kink in my air line as of yet. I am using clear 1/4 inch acrylic tubing from HD.

I updated this post with this info and pictures:

[donsthings.blogspot.com - K40 Gantry Cable Management](http://donsthings.blogspot.com/2016/11/k40-gantry-connection-management.html)


---
**Ned Hill** *December 07, 2016 12:17*

+Bob Damato I have a drag chain I'm still in the process of mounting, but I've have noticed that my chain has 2 different ends.  One bends back and the other does not.  If yours is the same you may need to switch ends so the chain body does't curl back into the beam.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 07, 2016 12:24*

Oh, I just remembered to mention that my mounting point for my chain is on the carriage plate at the back screw hole, but the other mounting point is actually on a laser cut piece of plywood that I attached to the right side of the moving rail (x-axis rail). So my chain's start point moves with the rail also. In case that is of assistance to anyone...


---
**Don Kleinschnitz Jr.** *December 07, 2016 12:37*

**+Yuusuf Sallahuddin** interesting, is your drag chain mounted parallel or perpendicular to it pivot pins. 

Picture?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 07, 2016 12:43*

**+Don Kleinschnitz** I'll grab a pic & show. It's a bit of a dodgy setup, but it works.



Actually, after taking photos just now it looks like I have moved it from the backmost screw on the carriage plate. Probably for the same reason you mentioned **+Bob Damato**, where it won't let you get all the way to Y=0.



Will post pics & tag you guys.


---
**Kelly S** *December 07, 2016 14:53*

Mine goes much like **+Yuusuf Sallahuddin**  I think.

![images/2eb0b329e308104620fc60b67c5bb5f4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2eb0b329e308104620fc60b67c5bb5f4.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 07, 2016 15:19*

**+Kelly S** Yep, but more professional looking than mine haha.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/R64MpHZPtDo) &mdash; content and formatting may not be reliable*
