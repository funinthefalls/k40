---
layout: post
title: "I have tried google , but my problem is this If I cut and etch in the same job on corellaser, there is a small offset between the cut and etch if the setting have PLT?"
date: August 30, 2016 22:52
category: "Hardware and Laser settings"
author: "Rodney Huckstadt"
---
I have tried google , but my problem is this If I cut and etch in the same job on corellaser, there is a small offset between the cut and etch if the setting have PLT? for cut, and if I set wmf for both cut and etch it is fine but the cut has extra lines and destroys the job ? any advice on things I can try ( I have converted the cut to and object and will try that tonight but any other help would be cool)





**"Rodney Huckstadt"**

---
---
**Rodney Huckstadt** *August 30, 2016 22:55*

I am upgrading to smoothie eventually but in the interim corellaser mostly works fairly well ( i do want to etch photos, eventually )


---
**Tony Schelts** *August 30, 2016 23:29*

I have found and had the advice that if your using coreldraw you will be using layers,  I create a engrave layer and a cut layer.  But just important I create a layer at the top called locator. which will be for a circle that measures about 0.01 mm.  This will be in its own layer and will be used in all the others.  I place this in the top right left hand corner and  I never have any problems using this method. 


---
**Rodney Huckstadt** *August 30, 2016 23:50*

yeah I do all that ,  i think it is an issue with the file formats ,  to the cutter, ie wmf for the etching and plt/? for the cut, if I use wmf for the cut it cuts extra lines through the job,


---
**HalfNormal** *August 31, 2016 02:14*

**+Rodney Huckstadt** make sure your lines are .0001 inches or mm thick. Wider lines will cut twice.


---
**Rodney Huckstadt** *August 31, 2016 02:31*

it's not a cut twice problem, the whole cut is moved to the right about 5mm,  yes they are alligned before the cut/etch


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 31, 2016 05:53*

I used WMF for both Cut & Engrave. There shouldn't be any "extra lines" cut when cutting, except some have had that issue when the paths were not closed paths in the SVG or whatever cutting file. Any chance you can post a photo of the result & the original cut file?


---
**Rodney Huckstadt** *August 31, 2016 13:20*

'll see what i can do


---
**Derrick Armfield** *September 11, 2016 18:03*

Rodney,  did you get it figured out?   Having same issue with shifting to the left.   



Finally figured out WMF.   Did a search on WMF etc and finally figured out that it's a Save As "WMF".   



Going to try running a practice peice to see if opening the WMF file has fixed my problem.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 20:30*

**+Derrick Armfield** You definitely need your CorelDraw Settings (in the CorelLaser toolbar) to have WMF for both Cut & Engrave. I will link some screenshots I have from the past on these issues.

(Corel Draw Settings)

[drive.google.com - Check CorelDraw Settings.png - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)

(Device Initialise Setting)

[https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)


---
**Derrick Armfield** *September 11, 2016 21:19*

Thank You!   I never would have found the right place to do settings without that screen shot!   Thank You Very Much


---
**Derrick Armfield** *September 11, 2016 23:31*

Did all the settings and still having issues with it engraving and cutting in 2 different places.  



Had read about people putting a 1 pixel by 1 pixel reference point.   Can somebody explain that a bit better?


---
**Derrick Armfield** *September 11, 2016 23:34*

Found the link to the Indestructible website and am running through that right now.   Will update with results.


---
**Derrick Armfield** *September 12, 2016 04:15*

Followed the link on the Instructables website and it finally worked!  Thanks to all for your help.


---
*Imported from [Google+](https://plus.google.com/109243783228349869845/posts/VJo93XcQoaE) &mdash; content and formatting may not be reliable*
