---
layout: post
title: "Hello guys, Good morning. I've been struggling for a long time with misalignment at the point that I have discovered that my mirrors are not the problem"
date: December 02, 2015 15:12
category: "Discussion"
author: "Jose A. S"
---
Hello guys,

Good morning. I've been struggling for a long time with misalignment at the point that I have discovered that my mirrors are not the problem. I have created a pattern to see the misalignment., look the imagen on the left is for engraving and the imagen on the right is for cutting, both are the same pattern but I do not understand why one is move to left of the dash line. The file is the same, the reference point has not change due the fact I am using the same pattern. Can anyone suggest what Can I do?



THank you



Best Regards



Jose



![images/3abe795c843fc6787627841ae06c8ded.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3abe795c843fc6787627841ae06c8ded.jpeg)
![images/d78aea70dc71fa41c3aac6d4aa5d6047.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d78aea70dc71fa41c3aac6d4aa5d6047.jpeg)

**"Jose A. S"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 02, 2015 17:14*

A lot of people have suggested for this kind of thing to put a small dot (1px x 1px) or something in the top left corner of your image (in Corel Draw) & then when you cut & when you engrave, you make sure to select that dot also. Then it shouldn't misalign like in your images. Hopefully that makes sense & helps out.


---
**Jose A. S** *December 02, 2015 18:07*

I have started putting this little dot but is not the solution.


---
**Coherent** *December 02, 2015 21:01*

The photo's don't really show much or explain the exact problem. Without more info hard to help. If you're loading the exact same file and the only difference is you are selecting the cut icon vs engraving icon, something has to be different in those two settings. If it's two different files then a difference between the two files is also a possibility. Also cutting works best in this software if you fill in the solid shape with black. Even a hairline cut may make 2 passes or locate the cut slightly off. Also try the technique mentioned here to engrave then cut in the same job. 
{% include youtubePlayer.html id="MR7967VHKnI" %}
[https://www.youtube.com/watch?v=MR7967VHKnI](https://www.youtube.com/watch?v=MR7967VHKnI)


---
**Phil Willis** *December 04, 2015 22:41*

A K40 with Corel/Laser draw needs a black filled shape to cut out. It will always try and cut both sides of even the smallest line. See my post from a while back. 



Golden Rules:

1. Black dot in top left corner of always visible master layer.

2. Engraving on layer 1 

3. Black filled shapes on layer 2 for cutting.

4. Hide print layer 2 and engrave.

5. Hide print layer 1 and cut.

6. Obviously don't move the work between tasks. Holes in the shapes will be cut out before the shape as long as you select "Inside first".


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/gvy6XMgCymt) &mdash; content and formatting may not be reliable*
