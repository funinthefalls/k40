---
layout: post
title: "I feel like such a Noobert for asking, but what is the go to purchase site for the current best K40 Laser Cutter manufacturer?"
date: April 03, 2017 15:59
category: "Material suppliers"
author: "Roberto Marquez"
---
I feel like such a Noobert for asking, but what is the go to purchase site for the current best K40 Laser Cutter manufacturer?





**"Roberto Marquez"**

---
---
**Stephane Buisson** *April 03, 2017 16:14*

search Ebay  with " 40W CO2 Laser Engraving Cutting Machine "


---
**Roberto Marquez** *April 03, 2017 16:56*

Thanks, Stephane.  I figured ebay.  I guess I mean to ask, if I search ebay for '40W CO2 Laser Engraving Cutting Machine' which search result is the one recommended most by the community?  I dont' know much about the K40, and am guessing that that is specific model.   



Also is there a buyer's guide for comparable models?


---
**Stephane Buisson** *April 03, 2017 17:03*

k40 is a generic name (an old ref), go for the cheapest (with manual pot), around 300 usd. you would do some mods to improve the performances anyway. if you go for an C3D board and an air assist compressor and head you will end around 500$. dig the community for the links. going for the open sources softwares is the way to go.


---
**1981therealfury** *April 04, 2017 00:38*

If you go digging on Ebay, just be careful of a company called AMONSTAR. They trade under many different names, and almost none of the people that i know of that have purchased anything through them have come away happy.  Normally if you enquire about an item on Ebay they will immediately offer you a discount to purchase outside of Ebay and often give you an email that mentions amonstar in the address.



I did see someone had started a list of Ebay accounts that trace back to Amonstar on the K40 owners facebook page a while ago, and i think there were at least 7 or 8 sellers that traced back to them.


---
**Roberto Marquez** *April 04, 2017 01:17*

Thanks for the heads up, therealfury.


---
*Imported from [Google+](https://plus.google.com/+RobertoMarquez/posts/i7koL2jQWRU) &mdash; content and formatting may not be reliable*
