---
layout: post
title: "Hey guys i am looking at a new focausing lens for my k40 looking at LO i see a 15 MM lens and a 18 MM is it better to go with a larger dia dose it make a difference Thanks"
date: April 23, 2016 18:31
category: "Discussion"
author: "Dennis Fuente"
---
Hey guys i am looking at a new focausing lens for my k40 looking at LO i see a 15 MM lens and a 18 MM is it better to go with a larger dia dose it make a difference

Thanks





**"Dennis Fuente"**

---
---
**Gee Willikers** *April 23, 2016 18:35*

Which ever fits the head on your machine, so same diameter as the original. 


---
**Gee Willikers** *April 23, 2016 18:36*

Focal length will change the characteristics of your engraving, not lens diameter.


---
**Dennis Fuente** *April 23, 2016 19:04*

Gee thanks for your response but LO only offers a 15 MM and an 18 MM lens so i guess its the 15  MM lens i can make the hole any size i want as i have access to a machine shop


---
**Venuto Woodwork** *April 23, 2016 19:24*

I would get the 18mm Laser head w/ air assisted. Ideal for K40 machine



With the High quality 18mm ZnSe Focus lens (F50.8mm)



I have this combo and it made a works of difference.


---
**Venuto Woodwork** *April 23, 2016 19:24*

All from light object


---
**Dennis Fuente** *April 23, 2016 19:35*

Thanks for the info i don't know if i like the way the air assist is set up but i may go with it anyway thanks


---
**Scott Marshall** *April 23, 2016 20:00*

The bigger the lens, the bigger the "sweet spot" thru the center so that alignment in all axis will be less critical.



All else being equal, bigger is better.



I went from stock to 18mm (in several focal lengths) and the larger lens is indeed easier to set up and burns a deeper/thicker material than the smaller one. 



It IS possible the factory lens was of much poorer quality(less uniform grinding, reflective coatings and beam scattering defects)  than the replacements (the base line from Saite Cutter - 20 bucks), but I tend to think it's the larger dia & thickness that's making the biggest difference.



Scott


---
**Dennis Fuente** *April 23, 2016 20:05*

Thanks Scott that's the info i was looking for 18 MM it is 


---
**I Laser** *April 23, 2016 22:08*

Any of you guys notice the lightobject air assist head slightly scratches your lens where the holder tightens up? I have two and notice it on both. :(


---
**Gee Willikers** *April 23, 2016 22:27*

**+I Laser** yes. But I figure its not in an active area of the beam so no big deal.


---
**I Laser** *April 23, 2016 22:30*

Thanks, figured the same, just having some issues with power and **+Phillip Conroy** suggested it may be the focal lens. Trying to cover all bases! :)


---
**Dennis Fuente** *April 23, 2016 22:50*

I noticed scraches around the mirrors where the ring screws on but it's outside of the working area used


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 23, 2016 22:53*

**+Dennis Fuente** My mirror mounts have some little white plastic rings, kind of like a washer, that go between the mirror & the metal screw-ring.


---
**Tony Sobczak** *April 23, 2016 23:31*

**+Venuto Woodwork** I agree, got the same from LOL and cutting 3mm plywood at 10mm/sec & 5ma. Very happy with that combo.


---
**Dennis Fuente** *April 23, 2016 23:32*

Well my machine was built the day after the chinese new year if you get what i mean


---
**Scott Marshall** *April 24, 2016 00:45*

It's standard practice in most better quality optics to use a fiber washer between any metal and glass. Once you nick a glass or crystal lens or mirror, it's almost a guaranteed crack sooner or later.



I use a paper or cardstock washer (just cut out of index card with punches or Xacto) between lens and holder (Saite Air Assist).



The fibre washer also provides a little compressiblity which glass/crystal does not have and you get a better air seal as well.



If you do tighten the head directly on the lens, go VERY lightly.



Scott


---
**Gee Willikers** *April 24, 2016 01:31*

I've always wanted to cut a washer with the laser, I just never remember to write the dimensions down before I reassemble the head.


---
**HalfNormal** *April 24, 2016 01:41*

**+Gee Willikers** [http://www.thingiverse.com/thing:1145526](http://www.thingiverse.com/thing:1145526)


---
**Scott Marshall** *April 24, 2016 03:32*

The washer ID for a Saite cutter air assist head (the aluminum one with the square body and adjustable focus - 18mm lens) is 20mm.



 I have Corel and Draftsight (Acad - .dwg) files I can send, but it only takes a minute to draw the 2 concentric circles. 30mm is a good OD. I made mine out of regular green felt (mostly because I had some within reach at the time) and it cuts great with 2-3ma. and 18mm/s. Make a bunch and stack them, saving the rest for next time you 'adjust' it.

That gives you pretty fine adjustment and a little final height adjustment using final torque.

Cork gasket material (thin), hobby foam or even Corrugated cardboard would probably all work ok too, anything somewhat compressible and laserable ought to do it.



The advantage of the compressible gasket is that you make, in effect a friction held radial adjustment. 



The final radial adjustment can be made by simply turning the assembly against the friction, and you don't have to try to tighten it down and "hit" the proper radial adjustment at the exact time it pulls to torque, a nearly impossible and frustrating endeavor....



If you are concerned about it slipping after the fact (mine has not) you could add a couple drops of CA (super glue) to the felt gasket once you're sure it's dead on. That will make it require replacement if you disassemble it, but you've got extra gaskets.


---
**I Laser** *April 25, 2016 00:01*

I know you should never assume, but given the crappy stock lens holders on both my machines held the lens flush and didn't scratch it, I assumed the LO head would do the same. By the time I removed the first one to clean it, it was too late and both were scratched. :\



Great idea **+Scott Marshall** I'll make a gasket before I next pull them out to clean.


---
**Custom Creations** *April 26, 2016 05:28*

**+Venuto Woodwork** are these items what you speak of?



[http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F508mm-P670.aspx](http://www.lightobject.com/High-quality-18mm-ZnSe-Focus-lens-F508mm-P670.aspx)



[http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)



Would none of the other heads work on the K40?


---
**Ariel Yahni (UniKpty)** *April 28, 2016 13:17*

Interesting thread guys. So is the LO head the best option out there? I have read mixed thoughts regarding it.


---
**Dennis Fuente** *April 28, 2016 17:52*

Just recieved my LO 18MM lens and purchased the head to go along with it, the only thing i can see that would be a draw back is you loose height with the new head assembly but the air assist keeps the head area under pressure this i would think will keep any debree away from the lens I currently have my machine torn completly apart it makes me want to scrap everything except for the electronics and start over.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2016 18:07*

**+Dennis Fuente** Hope the new lens & head assembly works well for you. I too know the feeling of wanting to scrap it all. I want to redo the frame & extend it so that it can do larger material eventually. I think when I get around to it I will do my conveyor table idea for the Y-axis & just have the laser tube mounted directly inline with the X-axis (meaning I can remove 2 mirrors as I will fire straight at the lens mirror). It will be a nasty large machine though, but functionality is more important than size.


---
**Dennis Fuente** *April 28, 2016 21:31*

I am also looking at making the frame larger as mine was bent and i had to take it over to the press to straighten it out just crap steel, so i just finished up installing the LO lens and head assembly i  have to say in test so far i can't see any difference in cutting my ply, also the tube coneections are realy crap i re did mine again the connections are in a bad spot this is because if i rotate the tube to rid it of bubbles the connections hot connector is up against the case and it has arched i am thinking of cutting away the case to allow more room for the connection are the larger machines any difference in quality how much would one have to spend to get a good machine 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 02:20*

**+Dennis Fuente** Check with **+Scott Thorne** regarding the larger machines, as if I recall correct he got the 100w version. He did mention not much difference in the cutting quality I think, but you do have a significantly larger cutting area.



I think if you are looking for extremely good quality cutting/engraving, you're probably looking at the more professional quality machines (epilog, trotec, etc). I don't know for certain, but the prices I have seen are in the $10000 range (but that's on ebay, so might be different from official suppliers/retailers). Also, I think you would need to evaluate the kind of tasks you are using it for. As it seems there are other options that are more suitable for certain tasks (e.g. CNC machine). Might work out cheaper & much better investment.


---
**Scott Thorne** *April 29, 2016 10:14*

**+Yuusuf Sallahuddin**...my machine is a 50watt....with 20 x 12 work area...﻿it has adjustable z table by push button and ruida dsp controller.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 10:36*

**+Scott Thorne** For some reason I thought your new one was 100w. I knew it was much larger work area though. Basically A3 size. I want something in the range of 1m wide & conveyor setup so I can roll through massive lengths of material off a spool hahaha. Not half greedy lol.


---
**Scott Thorne** *April 29, 2016 11:05*

**+Yuusuf Sallahuddin**...that's a good idea. 


---
**Ariel Yahni (UniKpty)** *April 29, 2016 12:21*

Building that frame is not  that complicated at all. [http://www.openbuilds.com/builds/openbuilds-freeburn-1-v-slot-co2-laser-60-100w.1001/](http://www.openbuilds.com/builds/openbuilds-freeburn-1-v-slot-co2-laser-60-100w.1001/)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 12:31*

**+Ariel Yahni** Thanks Ariel. I'll take a look.


---
**Ariel Yahni (UniKpty)** *April 29, 2016 12:32*

BTW thats **+Peter van der Walt**​ design


---
**Ariel Yahni (UniKpty)** *April 29, 2016 12:37*

So regarding the LO air assist, do you only need to change one mirror? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 12:41*

**+Ariel Yahni** Thanks for that. It looks like a good design that **+Peter van der Walt** has done for it. I like the use of angle brackets for affixing the corners of each piece.



I am not sure about the LO air assist, so hopefully someone who got it can get back to you on that one.


---
**Dennis Fuente** *April 29, 2016 17:21*

Ariel

I just installed an LO head and lens don't need to change any of the mirrors, as to a larger frame only thing is a source for the rails and belts other then that it's a piece of cake as we already have the guts we need 


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/KbJzdLmedwK) &mdash; content and formatting may not be reliable*
