---
layout: post
title: "Anyone that has the LO Z-Axis table, how did you mount it in your K40"
date: January 31, 2017 01:44
category: "Modification"
author: "Tony Sobczak"
---
Anyone that has the LO Z-Axis table, how did you mount it in your K40.  Stepper to front or rear?  Photos would be appreciated. It's all put together just need to get it inside the K40.







**"Tony Sobczak"**

---
---
**Ned Hill** *January 31, 2017 02:24*

**+Don Kleinschnitz** has one and I'm willing to bet that  he has an awesome write up over on his build blog somewhere. [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/)


---
**Don Kleinschnitz Jr.** *January 31, 2017 03:51*

LOL **+Ned Hill** your right.

[donsthings.blogspot.com - Stand alone K40 Zaxis Table & Controller Build](http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)



I just put it in the bay with the stepper at the back.

I however built it stand alone.


---
**Brooke Hedrick** *January 31, 2017 05:03*

Back for me as well.


---
**Tony Sobczak** *January 31, 2017 06:05*

Screw it down? 


---
**John Sturgess** *January 31, 2017 08:10*

Is anyone aware of the minimum table height from the laser bed? I need to engrave on objects approximately 50mm high with the standard 50.8mm focal lens. I'm not sure that the Z table will be low enough for the top of the object to be at optimal distance for engraving. I'm aware there is 70mm travel in total, i've emailed LO but obviously no reply.


---
**Brooke Hedrick** *January 31, 2017 12:38*

I didn't screw mine down.  With the smoke removal port at the back and xy axis framing structure on the left, I am able to gently slide the table to the back-left corner and it stops just where I need it to.  It doesn't move often.  In the rare case it does, I just slide it back.  The LO table has some decent mass to it.


---
**Joe Alexander** *January 31, 2017 13:27*

There is always the idea of using low-profile toggle-down clamps or thumbscrews to make it attached yet modular, been thinking on the same issue myself :)




---
**Don Kleinschnitz Jr.** *January 31, 2017 14:12*

**+John Sturgess** where do you want me to measure the top of the table when full up to?


---
**Don Kleinschnitz Jr.** *January 31, 2017 14:26*

**+Brooke Hedrick** I have been thinking about cutting the bottom of my cabinet and mounting the table in a set of adjustable mounts ....later.


---
**John Sturgess** *January 31, 2017 14:34*

**+Don Kleinschnitz** Hi Don, the measurement i'm interested in is the top of the table(with the Z table as low as it'll go) to the bottom of the laser head mount. From there i'll be able to work out the distance from my laser head mount to the lens. I imagine they'll be enough room to allow for the 50.8mm focal length plus 50mm object height (for engraving) but want to make sure before i buy one. Thanks!




---
**Don Kleinschnitz Jr.** *January 31, 2017 15:13*

**+John Sturgess** 4.5" in my machine.


---
**John Sturgess** *January 31, 2017 15:24*

**+Don Kleinschnitz** Thank you :)


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/HJHRmGZwVW9) &mdash; content and formatting may not be reliable*
