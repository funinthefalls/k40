---
layout: post
title: "Stephane Buisson Found a pic of my PSU on the Facebook laser group I really don't want to take it out as I have to flip the laser on its side to do it but this one is what I have..."
date: June 04, 2016 07:21
category: "Original software and hardware issues"
author: "Alex Krause"
---
 **+Stephane Buisson**​  Found a pic of my PSU on the Facebook laser group I really don't want to take it out as I have to flip the laser on its side to do it but this one is what I have... typo and all for the "Text" button lol a guy on FB had to drive to get a replacement and the second pic is what they replaced his with 



![images/4a688d584860af6d7249900ccf0f728e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4a688d584860af6d7249900ccf0f728e.jpeg)
![images/b8eaf042414c9f4569f86add5f9d6b97.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8eaf042414c9f4569f86add5f9d6b97.jpeg)

**"Alex Krause"**

---
---
**Alex Krause** *June 04, 2016 07:24*

[www.jnmydy.com](http://www.jnmydy.com) is the supplier of the one on the left by the silk screen on the board but I can't find that PSU on their site only the one on the Right is available now


---
**Stephane Buisson** *June 04, 2016 07:59*

it's hard to get any answer from jnmydy.

just basic info like

[http://www.jnmydy.com/comcontent_detail/&FrontComContent_list01-1285720952066ContId=6&comContentId=6.html](http://www.jnmydy.com/comcontent_detail/&FrontComContent_list01-1285720952066ContId=6&comContentId=6.html)


---
**Ariel Yahni (UniKpty)** *June 04, 2016 15:01*

**+Alex Krause**​ you can loosen the psu from bellow by pulling the machine a little bit out of the table. Well that's what I did


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/VStA2MCW1SG) &mdash; content and formatting may not be reliable*
