---
layout: post
title: ".......Repairing/replacing your sensor & potential design problem..."
date: February 02, 2017 15:06
category: "Modification"
author: "Don Kleinschnitz Jr."
---
.......Repairing/replacing your sensor & potential design problem...



Helping another K40 owner with a end-stop sensor problem (standard Moshi board) I ran into what I believe to be either a design problem, a part incompatibility or both.



The problem started when the Y end-stop optical sensor was damaged by misalignment and was subsequently replaced with a new TCST 1030. From that point on the machine did not operate properly having difficulty homing and other movement issues.



After replacing various parts including the Moshi controller the owner sent me the entire end-stop assy.

Troubleshooting



I hooked my end-stop tester up to the assy and immediately it was apparent the neither axis optical sensors was working. 



.......X end-stop



I found that the 6 pin ribbon cable had an intermittent and after learning that you can't buy "just one' of these I repaired the condition by adding layers of scotch magic tape to the cable. That thickened the cable and improved the contact between the cable and socket. I added hot glue as an added measure.



.......Y end-stop



The Y end-stop was working but the output transistor was not fully turned ON leaving the ON condition at more than 3V and the off condition at 4.9V, certainly not a legitimate logic switching level.

I checked and tried everything including replacing the sensor. Nothing worked to improve this marginal condition and the wiring was sound and the components were the right value.

The only difference between the X and Y optical end-stops was that the Y one had a new sensor.



.............The design problem 



One or both of the following conditions cause the problem manifested in the Y axis above:



.....The CTR for the stock TCST 1030 and the over the counter part are different. 

.....The current limiting resistor (1000 ohms) in the stock design creates an operating point (3-4 ma) that is in the non-linear part of the CTR curve.



A better value is 100 ohms which runs the emitter at 37ma and further up the curve where it is flat.

The max current for this device is 60ma so this operating point was judged to be safe.



This change was tested and resulted in these sensor output values:



On = .5

Off = >3VDC



........The fix......



If you have to replace the stock sensor then likely you will have the above problem. 

Therefor you may also need to replace the 1K resistor with a 100 ohm resistor at the same time.



Replacement sensor: TCST 1030, Purchase, Data Sheet



........Optical End-stop Reliability........



I have heard of K40 users having reliability problems with optical end-stops. I was surprised to hear this since I had used them in many devices successfully. They however have to be designed properly.

The marginal operating point identified above could be the cause of these problems.



[http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)



![images/1e60f94916dba8add5806c22271c47eb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e60f94916dba8add5806c22271c47eb.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Andy Shilling** *February 02, 2017 15:25*

I believe this is one of the problems I had and now I have opted for the mechanic type. I ordered new optic sensors but never got round to fitting them, not that they would have helped by the sounds of it. 

Do you have a place in the U.S that can supply these ribbons? I have had no luck here in the UK, even being able to buy it off the roll and having to make the ends up would be ok; I'm sure we could come up with something been the community.


---
**Don Kleinschnitz Jr.** *February 02, 2017 23:33*

I have not found the ribbons anywhere. Perhaps we should redesign the K40 end-stop subsystem to go with conversions. You can't get parts for the factory setup.


---
**timb12957** *February 06, 2017 11:55*

I am the user Don made these repairs for. I can't thank him enough for all he has done for me and this board! I am making a donation to your blog Don! Keep up the great work!


---
**Don Kleinschnitz Jr.** *February 06, 2017 12:08*

**+timb12957** thank you so much Tim your generous donation will aid in getting more info. like this to the community. I expect that the new repairs bring your machine back on line. The repaired parts will ship today or tomorrow. I certainly learned a lot.


---
**timb12957** *February 08, 2017 18:22*

Don you are a WIZARD! Parts came today, installed them and it works like a charm!! I was thrilled to have my laser (That I had come to think would never work again) back up and running!


---
**Don Kleinschnitz Jr.** *February 08, 2017 19:09*

**+timb12957** whoa,  that is good news. That problem was a 'booger". :)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/h7316mZSfNm) &mdash; content and formatting may not be reliable*
