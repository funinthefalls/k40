---
layout: post
title: "The middleman board has XA+/- and BA+/- (4 total endstop pins) while the smoothingboard only has 3"
date: September 01, 2016 17:52
category: "Smoothieboard Modification"
author: "Jerry Hartmann"
---
The middleman board has XA+/- and BA+/- (4 total endstop pins) while the smoothingboard only has 3.



What do I wire?





**"Jerry Hartmann"**

---
---
**Ariel Yahni (UniKpty)** *September 01, 2016 17:57*

 Aren't those for the X stepper motor? 

![images/bbfe6cdafc71b2c29f52c07967c84180.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bbfe6cdafc71b2c29f52c07967c84180.png)


---
**Jerry Hartmann** *September 01, 2016 17:59*

Probably

Will look again tonight :S


---
**Jon Bruno** *September 01, 2016 18:23*

I thought that EStY and EStX are for the (E)nd (St)ops Y & X


---
**Jerry Hartmann** *September 02, 2016 04:11*

**+Ariel Yahni** Alright, M1 is X and M2 is Y?




---
**Don Kleinschnitz Jr.** *September 02, 2016 10:34*

ESty and EStx are endstop connections connected to Smoothie Xmin and Ymin respectively. You have to decide how you provide 5V and grnd to the middleman (I plan to use 5v and grnd from the Xmin connector). 

XA-, XA+, XB-, XB+ are X stepper connections connected to Smoothie M1. 

The Y stepper is a separate cable from the middleman connected to Smoothie M2.


---
**Jerry Hartmann** *September 13, 2016 00:36*

**+Don Kleinschnitz** I've never crimped a cable before. The smoothieboard isn't labelled as far as pin 1, etc is there a specific order?


---
**Don Kleinschnitz Jr.** *September 13, 2016 12:35*

What are you trying to wire, the end stops or the steppers?


---
*Imported from [Google+](https://plus.google.com/105524720582279612505/posts/Q4m2hWQixT3) &mdash; content and formatting may not be reliable*
