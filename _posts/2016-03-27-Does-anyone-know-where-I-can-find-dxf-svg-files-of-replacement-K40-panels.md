---
layout: post
title: "Does anyone know where I can find dxf/svg files of replacement K40 panels?"
date: March 27, 2016 01:01
category: "Modification"
author: "ThantiK"
---
Does anyone know where I can find dxf/svg files of replacement K40 panels?  I've got the LaserSafetySystem almost complete (just waiting on 2 stupid barb fittings) and I'd like to cut a black acrylic panel for it.





**"ThantiK"**

---
---
**Joe Spanier** *March 27, 2016 02:07*

Which panel? I have one that replaces the control panel insert


---
**ThantiK** *March 27, 2016 02:14*

I found the dimensions of the 20x4 i2c backpack panel I've got.  Converting it over to an inkscape svg now.  I'll have to test dimensions on it later; AI doesn't like to import things at the correct scale from SVG for whatever reason - so I'll have to do some test cuts at the lab before I can start replacing the panel.  Code is done, just need places to mount the stuff.


---
**Anthony Bolgar** *March 27, 2016 02:40*

I could send you my custom panel that has the 128X64 Ramps display, the 2004 LCD for the safety system as well as the ammeter, laser enable and laser test buttons. Look at [https://lh3.googleusercontent.com/MoG_np8WCxsQ3MWLbbvD2ObyitW79lWQAt8B-6nR7HfohCXWVUbNdRPUdhzIrnxs9f7QxU9cv3mOl8hUFtNMKQ3Y6jP5JLlthRk67Ndae7KZPX2j-tk9X9nu_wfn=s0](https://plus.google.com/photos/104696927629289302865/album/6227369853795730177/6227369853906059346?authkey=CO-5pZG7mezPqwE&sqid=118113483589382049502&ssid=5c5218e7-4c52-4f3f-ab90-a7db94060f25)


---
**ThantiK** *March 27, 2016 02:43*

**+Anthony Bolgar** did you cut that top section yourself?  That looks nice!


---
**Anthony Bolgar** *March 27, 2016 02:52*

yUP, i CUT BOTH THE ACCESSORIES PANEL AS WELL AS THE NEW MAIN PANEL. iT IS CLEAR ACRYLICC, i BCKPAINTED IT BLACK, THEN MIRROED THE FILES AND ENGRAVED FROM THE BACK SIDE. tHEN SPRAYED WHITE PAINT OVER THE ENGRAVED AREAS. cAME OUT LOOKING GREAT, AND SINCE THER PAINT IS ON THE BACK SIDE, IT WILL NOT SCRATCH OR CHIP FROM USE.


---
**Anthony Bolgar** *March 27, 2016 02:52*

Ooops....sorry about the caps lock.


---
**ThantiK** *March 27, 2016 03:16*

**+Anthony Bolgar**, that's freakin' genius.  Unlimited colors, looks amazing, super awesome factory look, and easy to do with existing laser. :D


---
**Anthony Bolgar** *March 27, 2016 03:18*

Thanks, it was one of my better ideas, makes up for some of the idiotic things I have attempted....lol!


---
**Brian Bland** *March 27, 2016 06:11*

**+Anthony Bolgar**​ Could you send me the file also.  That is what I want to put on my K40 also.


---
**Damian Trejtowicz** *March 27, 2016 10:00*

**+Anthony Bolgar** Could You send me this file as well.looks amazing and wish to put one in my k40


---
**Anthony Bolgar** *March 27, 2016 10:23*

OK, will upload a copy to google drive.


---
**Brian Bland** *March 27, 2016 10:28*

Thanks.


---
**Anthony Bolgar** *March 27, 2016 10:28*

[https://drive.google.com/drive/folders/0B23r3Hp4r5U7aUx1Mld5RHFRdDQ](https://drive.google.com/drive/folders/0B23r3Hp4r5U7aUx1Mld5RHFRdDQ)


---
**Kelly Burns** *March 28, 2016 13:04*

That is awesome Anthony.  Looks fantastic and very well done.  


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/j6ovGAWZLiw) &mdash; content and formatting may not be reliable*
