---
layout: post
title: "Hi, I lost the software for k40.can someone give me a download link and also instuctions for laser allignment if possible Thanks"
date: September 27, 2017 10:16
category: "Discussion"
author: "Dheeraj Arora"
---
Hi,



I lost the software for k40.can someone give me a download link and also instuctions for laser allignment if possible



Thanks







**"Dheeraj Arora"**

---
---
**Don Kleinschnitz Jr.** *September 27, 2017 10:28*

**+Dheeraj Arora**



There are lots of posts on this community that can be accessed with a search .... here is one.





[k40laser.se - Mirror alignment - no more sweat and tears - Updated! - K40 Laser](https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/)


---
**Dheeraj Arora** *September 27, 2017 10:41*

thanks Don Kleinschnitz

any link for software download


---
**Don Kleinschnitz Jr.** *September 27, 2017 11:14*

**+Dheeraj Arora** I don't run a standard K40 controller but search the communities posts. I'm sure that someone that does will respond.


---
**Chuck Comito** *September 27, 2017 11:25*

**+Dheeraj Arora**​ Chinese k40 laser group on Facebook has a file section where you can download them. 


---
**Ned Hill** *September 28, 2017 01:38*

Another alignment guide.  [plus.google.com - - Laser Alignment Guide - With all the laser alignment questions we get, here...](https://plus.google.com/u/0/108257646900674223133/posts/LGfT6SS3Tcc)


---
**Laurent Humbertclaude** *September 30, 2017 09:54*

Get the k40whisperer it is basic but works like a charm. 

[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
*Imported from [Google+](https://plus.google.com/118105035571850216408/posts/DMepSTPK1Ev) &mdash; content and formatting may not be reliable*
