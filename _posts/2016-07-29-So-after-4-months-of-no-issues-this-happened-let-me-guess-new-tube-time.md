---
layout: post
title: "So after 4 months of no issues this happened let me guess new tube time???"
date: July 29, 2016 18:43
category: "Discussion"
author: "Jeff Kwait"
---
So after 4 months of no issues this happened let me guess new tube time??? It has a 6 month warranty from the seller let's see what happens


**Video content missing for image https://lh3.googleusercontent.com/-Z3mP_ci-Lh8/V5ujyiElZUI/AAAAAAAAGTk/WRXOaPTshN0eCia-Z9NmdtJfA7GGl43qA/s0/gplus443210552.mp4.gif**
![images/a28d4fee6421a13e5923e4bccc57450a.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/a28d4fee6421a13e5923e4bccc57450a.gif)



**"Jeff Kwait"**

---
---
**Jon Bruno** *July 29, 2016 18:53*

Yup tube time.

Don't keep doing that though or it'll be new power supply time too.


---
**Jeff Kwait** *July 29, 2016 19:03*

I will see what the eBay seller will do already looking at the light object's 35w tube


---
**Scott Marshall** *July 29, 2016 19:24*

Yep. +1 on Tube time. And Jon is right about letting it arc. It can kill the power supply next time you hit it. It's an extreme overload when it arcs.



You may want to ask around here more, read old posts, there's a lot of people who have purchased several tubes and can lead you to a reliable source, (which Light Object is, but far from the cheapest)


---
**Evan Fosmark** *July 29, 2016 20:02*

FWIW, I have an LO 35w tube and I'm happy with it so far. Have to run it at about 15% higher power to get the same effect, but that was expected.


---
**Jon Bruno** *July 29, 2016 20:02*

I have two tubes in the closet waiting to go.

not LO but under a year old and times a ticking...Are you in the states? Maybe we can help each other out...

I don't want them to expire unused. I only need 1 spare.


---
*Imported from [Google+](https://plus.google.com/114834788519034865166/posts/9ipreF2WrfG) &mdash; content and formatting may not be reliable*
