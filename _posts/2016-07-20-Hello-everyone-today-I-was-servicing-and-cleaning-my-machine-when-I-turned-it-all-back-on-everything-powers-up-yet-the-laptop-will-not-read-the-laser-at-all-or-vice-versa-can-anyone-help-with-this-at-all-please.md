---
layout: post
title: "Hello everyone today I was servicing and cleaning my machine when I turned it all back on everything powers up yet the laptop will not read the laser at all or vice versa can anyone help with this at all please"
date: July 20, 2016 16:00
category: "Discussion"
author: "Craig Longson"
---
Hello everyone today I was servicing and cleaning my machine when I turned it all back on everything powers up yet the laptop will not read the laser at all or vice versa can anyone help with this at all please 





**"Craig Longson"**

---
---
**Scott Marshall** *July 20, 2016 16:14*

Check all the connectors inside the electrical box, especially the 4 pin one going from the power supply to the controller board (M2nano) - little green PCB about the size of a playing card.



Push each on to ensure it's down all the way. Repeat with connectors across the front of the main power supply on the bottom.



Also check the wires that run to the panel switches, they all plug onto the terminals, and come loose quite easily.



Do this with the unit unplugged for safety.



It's most likely one of the ones on the M2nano board. Also double check your USB cable and make sure it's plugged into the same port as before. This matters on some computers.



Good luck


---
**greg greene** *July 20, 2016 16:17*

Also check the usb cord and it's connection s to both the laser and the laptop.


---
**Craig Longson** *July 20, 2016 16:35*

I have tried that guys thanks but still nothing I didn't even go into that part all I did was disconnect the Ian when I was cleaning it all and now it just won't connect to each other 


---
**greg greene** *July 20, 2016 16:38*

Is your dongle plugged in?


---
**Craig Longson** *July 20, 2016 17:05*

Yes the dongle is plugged in too everything is powered up just it won't read it or initialize 


---
**Bruce Garoutte** *July 20, 2016 20:50*

The first question a computer tech would ask is, "Did you power everything off, then back on again, (including the laptop)?

I would power the laptop on first. Plug a known good device, (flash drive, etc.) into the USB port that the laser will plug into and ensure it is working. If it is then, plug the laser into the USB port, then power it on. Mine won't see the laser until I flick the actual "Laser On" switch as well.

If it still isn't working, try swapping the USB cable with a known good one.

If it still isn't working, try wiggling the laser side of the cable in the plug GENTLY wile watching the plug. Does it seem more loose than usual? If it does, then you may need to re-solder the plug.

If it still isn't working, then check the controller board for any loose connectors or other pieces. You may want to remove the board and check the backside for cold solder joints. If you suspect any, re-heat/solder them.



Hopefully, you will find the cure in these suggestions somewhere.


---
**Scott Marshall** *July 21, 2016 07:15*

It's  possible somehow you somehow damaged your M2 board. If so the only solution would be to replace it (you can find used take-outs right here reasonable, or new ones from about 90-$120 on ebay.



I'd try everything new 1st, (different USB Cable, different computer)



The M2 Nano board draws it's power from the USB port on some older K40s, so your power/charging settings could make a difference there.



Most of recent production k40 controller boards are powered from the main power supply (which also could cause this problem, although it's rare -  this if you have a late model that powers the M2 thru the large  4 pin cable (rightmost cable  coming out of the power supply it's actually 4 individual wires wrapped in spiral wrap).



That's the place to check if you have a meter and know how to use it check between pin 2 (gnd) usually a white wire, and pin 3 (+5V) usually a pink wire. You should have 5V there, and 24v from pin 2 to pin 1 (motor power)



Pin out on this cable  is #1 -24V, #2 - Gnd, #3 - 5V, #4 - Laser fire signal



Good luck, Scott


---
**Craig Longson** *July 21, 2016 07:41*

I'm not clued up at all in the electrical side of things and which is the board you need a pic of please ? 


---
**Scott Marshall** *July 21, 2016 07:56*

I don't have a picture ready to go, but it's an easy connector to find. If you look at the power supply (I'm assuming you have a K40 made in the last year or so) which is on the floor of the electrical box, the connector you are looking for is on the far righthand side of the power supply side facing you. It has a cable with a black wrap on it about 8" long. Rectangular white plastic conector about 1" wide, It's the largest white connector on the power supply and has orange, white, pink & black wires in it. It's plugged into (opposite end) the Controller board, known as the M2nano. 



If you're not electrically experienced, you may want to get a friend who is to take the measurements for you, as it involvers working in a live panel.

The same power supply puts out the back side wiring 15,000 volts to power the laser tube, so experience and caution are requirements to take these measurements safely.



Wish I could help more.



Scott


---
**Scott Marshall** *July 21, 2016 08:01*

If it comes to it, you may have to buy/borrow a M2 nano to exchange and try to verify where the problem is. 



You may want to try reloading the software again from scratch, failing all other solutions, it's a longshot, but the software is on the unstable side and sometimes does quirky stuff.



It's free to try it.



Scott


---
*Imported from [Google+](https://plus.google.com/106752542842598017201/posts/SQ1ZqxhvG5K) &mdash; content and formatting may not be reliable*
