---
layout: post
title: "One last n00b question for the day, what is the best way to align the mirrors?"
date: January 12, 2017 21:22
category: "Discussion"
author: "Abe Fouhy"
---
One last n00b question for the day, what is the best way to align the mirrors? Should I remove the tube and shoot a pen laser down the throat and verify with the tape method, where you move the laser closest to the laser mirror then farthest away?



I heard of the flyingwombat calibration, googled that but couldn't find anything.



Once calibrated, how do I calibrate the height, is the wedge/ramp method the best?



Thanks all!





**"Abe Fouhy"**

---
---
**Cesar Tolentino** *January 12, 2017 21:34*

There is a YouTube video out there that shows how to align the two or three mirrors.  No need to remove the tube. 


---
**Ned Hill** *January 12, 2017 21:43*

Here's the floating wombat instructions.  [onedrive.live.com - OneDrive](https://1drv.ms/b/s!AtpKmELEkcL8imoUD99sdJZZT-am)


---
**Ned Hill** *January 12, 2017 21:47*

I recommend labeling your mirror screws as A,B,C as given in the instructions to make things easier.

![images/f69512faaa55cbeb36c673d846b353aa.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f69512faaa55cbeb36c673d846b353aa.png)


---
**Abe Fouhy** *January 12, 2017 23:50*

Sweet! Thank you guys so much. I am really impressed with this communities eagerness to help. I am hoping to contribute as much as possible once I know what I am doing! haha


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/9RNAayU3d4J) &mdash; content and formatting may not be reliable*
