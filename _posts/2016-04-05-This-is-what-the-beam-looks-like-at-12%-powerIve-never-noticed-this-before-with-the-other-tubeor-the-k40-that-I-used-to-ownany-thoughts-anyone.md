---
layout: post
title: "This is what the beam looks like at 12% power...I've never noticed this before with the other tube...or the k40 that I used to own...any thoughts anyone"
date: April 05, 2016 14:24
category: "Discussion"
author: "Scott Thorne"
---
This is what the beam looks like at 12% power...I've never noticed this before with the other tube...or the k40 that I used to own...any thoughts anyone.

![images/2ac1b7592e708fdb2fe21dcfa11263f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2ac1b7592e708fdb2fe21dcfa11263f5.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:34*

It's almost a perfect doughnut shape. What is the lowest power you can shoot at? I'd be interested to see the difference in beam shape for different power %s to see if the dead-spot increases in size at lower powers or at higher powers.


---
**Scott Marshall** *April 05, 2016 14:35*

I'd expect the elements in the laser tube are out of position, causing exactly what Yuusuf described, only in the tube optics instead of the head.



That may be why the larger tubes use externally adjustable optics. 

Pretty hard to hold all those elements in just the right place while your glass cools....


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:36*

**+Scott Marshall** That being said, is there a lens/optics set-up inside the tube?


---
**Scott Thorne** *April 05, 2016 14:42*

**+Yuusuf Sallahuddin**...that is the lowest power setting that it will fire at..

12% power.


---
**Scott Marshall** *April 05, 2016 14:44*

**+Yuusuf Sallahuddin**

Yes it's ALL inside. The mirrors are concave, and focus on one another, which keeps the beam straight and minimizes scattering losses.



If they're off by even a few thousandths, over 3 feet, it's a big error.



Remember the light has gone back and forth a few million times by the time it hits the tape....


---
**Scott Thorne** *April 05, 2016 14:44*

**+Scott Marshall**....I think you guys are right...nothing I can do about it...I'm gonna run the mess out of until it quits them reinstall the reci tube.


---
**Scott Thorne** *April 05, 2016 14:45*

**+Yuusuf Sallahuddin**...at 20% you can't see the dead spot at all...crazy right.


---
**Scott Thorne** *April 05, 2016 14:46*

In the tube there are just two mirrors...one fully mirrored and one 90% mirrored...I think that is correct.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 14:49*

**+Scott Thorne** That is odd that increasing the power removes the dead spot. So I guess, just don't run under 20% haha.



**+Scott Marshall** I'll have to take a closer look at my tube. I've been wary to even look at it in case I break it just by looking haha.


---
**Scott Marshall** *April 05, 2016 14:50*

Yep, here's the workings

Ophir again...



Look at the Spherical Mirrors (it's for a beam expander, but the principle is the same) pictorial and imagine what happens if the mirrors are off and beam out is not  precisely swept by the beam in mirror, no center fill,  instant ring.



About looking at your tube,

I doubt you'll be able to see the error,  it's so tiny to detect it you'd need a good optica lbench and know how to use it. Or turn it on...





[http://www.ophiropt.com/co2-lasers-optics/beam-delivery-mirrors](http://www.ophiropt.com/co2-lasers-optics/beam-delivery-mirrors)


---
**Thor Johnson** *April 05, 2016 15:17*

Looks like your laser is running in TEM01 mode instead of TEM00-[https://en.wikipedia.org/wiki/Transverse_mode](https://en.wikipedia.org/wiki/Transverse_mode)



Unfortunately that kind of operation is inherent to the optics in the tube  (primarily Brewster angle of end plates), so you can't do anything short of replacing the tube.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 15:50*

**+Scott Marshall** I meant I wanted to look at my tube to see if I could see mirrors in it. I honestly don't really know how it works as I've barely looked at the tube itself. I only really have that doughnut looking ring when I deliberately defocus my beam/material.


---
**Scott Marshall** *April 05, 2016 17:03*

Good views of a tube here (full screen it) [http://lasertubevideo.com/efr-products_2](http://lasertubevideo.com/efr-products_2)


---
**HalfNormal** *April 05, 2016 20:46*

This could be a "second" sold as a first. Second is a product not perfect.


---
**Scott Thorne** *April 06, 2016 00:08*

It probably is **+HalfNormal**...I may have been duped......only paid 225....but it works good...cuts good...engraves good.


---
**HalfNormal** *April 06, 2016 02:05*

**+Scott Thorne** As long as you are happy with it, that is all that matters. I always look for the bargain and hope it is not a total dud! 


---
**Scott Thorne** *April 06, 2016 02:32*

**+HalfNormal**...amen brother...it works great at normal power...I just noticed the anomaly when I was aligning it...but to be honest the reci tube that was in here might have done the same thing at extremely low power...if it did I never noticed it. 


---
**MNO** *April 06, 2016 08:16*

I'm not hitech but i would be thinking that it is about how beam form in tube. And that it not fully form and saturate fully with low power. 


---
**I Laser** *April 06, 2016 10:19*

I've had the same pattern on my machine too, only happens at lower settings too. I just assumed it was normal :\


---
**Scott Thorne** *April 07, 2016 03:15*

After visiting the puri website and reading up on the tube specs that I purchased...the photo that I posted is supposed to look like that...from what I've read it's designed that way for higher power output...I feel better knowing that now...lol


---
**Scott Marshall** *April 07, 2016 07:49*

1st I've heard of it, but there's a lot of new stuff out there.



My el cheapo stock k40 tube outputs a solid (although a tad lopsided) solid dot at a level just high enough to mark Scotch Blue (same as you use I think). That's up close, with the tape on the 1st mirror) 



Now I'm disappointed I don't have the new trick "annular discharge power augmentation beam" on mine. (The Doughnut of Power)

 

Hey, if it cuts well, I wouldn't care if the beam is panda shaped.



Scott


---
**Scott Thorne** *April 07, 2016 13:06*

**+Scott Marshall**...lol...my thoughts exactly...not sure that I fully understand why it would have more power but I'm far from a photonic expert....I never new that there were so many tube types  that you could purchase either...while on vacation last night I was up until 4 am listening to the sweet sound of my wife snoring so I thought I would read up on the infamous donut mode...in a nutshell the experts say that regardless of mode type the style and quality of the lens in the most important thing. 


---
**I Laser** *April 08, 2016 01:51*

Lol @ the doughnut of power!!


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/QNwjpqGBtRz) &mdash; content and formatting may not be reliable*
