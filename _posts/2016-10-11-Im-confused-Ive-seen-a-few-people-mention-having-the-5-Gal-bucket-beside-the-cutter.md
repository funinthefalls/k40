---
layout: post
title: "I\"m confused. I've seen a few people mention having the 5 Gal bucket beside the cutter"
date: October 11, 2016 18:48
category: "Discussion"
author: "Bill Keeter"
---
I"m confused. I've seen a few people mention having the 5 Gal bucket beside the cutter. So that when the water pump is turned off the water doesn't drain out. 



This is confusing. If both ends of the hose are below the water's surface you should be able to keep the bucket below the work bench. Right? As there's no pressure to cause the water to drain out? Or i'm I forgetting something?





**"Bill Keeter"**

---
---
**Jim Hatch** *October 11, 2016 19:10*

correct. However, if you do that there's no splashing noise to tell you when the pump is running that it's actually cycling water. Also, keeping it next to or above lets any air that does get in (for instance from outgassing of the water itself) the tube will get out by itself.


---
**Ashley M. Kirchner [Norym]** *October 11, 2016 20:24*

If you install a flow meter inline with the tube, you don't need the "splashing noise" to tell you it's working. For me, I have two temperature probes, one on the inlet and another on the outlet. When the laser isn't running, but water is flowing, the temperature will be equal on both ends. When the laser is running, the outlet temperature will be slightly higher. And since I'm using frozen water bottles to keep the water cold, I know when the bucket needs more water. If there is a sudden blockage, the outlet temperature will shoot up. For now, this works. Eventually though I plan on using that outlet temperature to trigger an alarm and/or possibly even cut off the laser output ... but that remains to be seen. Right now it's working well.


---
**Ninetynine Centurions** *October 11, 2016 22:22*

Norym, what temp probes are you using?  just tee them with the soft tubing?




---
**Ashley M. Kirchner [Norym]** *October 12, 2016 00:03*

Two of these: [http://r.ebay.com/Atolt0](http://r.ebay.com/Atolt0)

How you put them inline is up to you. I 3D printed some fittings that I stick the probe in from the top (and use silicone to seal) and it has an inlet and outlet port for the hose. I'm sure you can figure something out. Just keep in mind that a T-fitting might not work as well because the water flowing through it isn't necessarily going to also agitate into the "T" section. You really need water flowing past the sensor.


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/1ozMwqy4xrc) &mdash; content and formatting may not be reliable*
