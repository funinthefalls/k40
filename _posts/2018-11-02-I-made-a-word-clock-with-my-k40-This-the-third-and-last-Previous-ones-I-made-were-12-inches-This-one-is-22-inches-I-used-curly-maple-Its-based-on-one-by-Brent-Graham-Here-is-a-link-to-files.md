---
layout: post
title: "I made a word clock with my k40 This the third and last Previous ones I made were 12 inches This one is 22 inches I used curly maple Its based on one by Brent Graham Here is a link to files:"
date: November 02, 2018 16:17
category: "Object produced with laser"
author: "Kevin Lease"
---
I made a word clock with my k40

This the third and last

Previous ones I made were 12 inches

This one is 22 inches

I used curly maple

It’s based on one by Brent Graham

[http://grahamworkshop.com/wordclock/](http://grahamworkshop.com/wordclock/)



Here is a link to files:

[https://drive.google.com/drive/folders/1MTNYSbBGt7w8hy3UMrOsgkdSDhQSbl4A?usp=sharing](https://drive.google.com/drive/folders/1MTNYSbBGt7w8hy3UMrOsgkdSDhQSbl4A?usp=sharing)



![images/2bb0e4fc1e7789ebbc6867ca9d821108.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2bb0e4fc1e7789ebbc6867ca9d821108.jpeg)
![images/cb9a835d6d3bcdefdbae8026ac36f727.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cb9a835d6d3bcdefdbae8026ac36f727.jpeg)

**"Kevin Lease"**

---


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/3RDNLE3SJL9) &mdash; content and formatting may not be reliable*
