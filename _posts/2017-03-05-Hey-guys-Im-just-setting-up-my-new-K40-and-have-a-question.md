---
layout: post
title: "Hey guys, I'm just setting up my new K40 and have a question"
date: March 05, 2017 18:34
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Hey guys, I'm just setting up my new K40 and have a question. Does anyone know what the difference between the "switch machine" button and the "power switch"?

When I turn the switch machine knob everything powers on, including the lights. So it appears as that's the power button. But if so, what does the actual button labeled POWER do?

Sorry if this is a dumb question, this is my first K40 and the manual i received is for the older model and bad English. And I can't find any videos with a control panel exactly like mine.

![images/3eae03957148b5a6599a37636d9b3d56.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3eae03957148b5a6599a37636d9b3d56.jpeg)



**"Nathan Thomas"**

---
---
**Andy Shilling** *March 05, 2017 18:44*

Do you have extra power points at the back of the machine? You'll possibly find it will give power out to your water pump if you plug it into it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 05, 2017 20:19*

That is an odd setup. I wonder is the "Switch Machine" the one that turns everything on & then the "Power Switch" like an "enable" or something for the laser? I guess best way to work it out would be to trace the wire paths to see where they all go & hopefully make some sense of it...


---
**HP Persson** *March 06, 2017 09:36*

On mine, switch machine knob turned on the lights :P

Best you can do is follow the cables. The k40´s differs alot between sellers sometimes.




---
**E Caswell** *March 06, 2017 20:36*

They both do exactly the same thing, they are wired in series. The only difference is the button on the left is a BIG RED one and locks off as an emergency switch supposed to do. They don't even switch each of the sockets in the back. The rocker power switch just switches the laser on and off, same as the little press switch does on the right hand side of the led display. You will also notice that the LCD display for temp reads all of the time, that's because it's battery powered so you will need to keep an eye on that as it's taking water temp from the laser tube.




---
**E Caswell** *March 06, 2017 20:47*

Pic of my panel, same as yours but mine is 220v. 

![images/098b670b40345b04f1c02010c38a5340.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/098b670b40345b04f1c02010c38a5340.jpeg)


---
**Fernando Bueno** *March 06, 2017 21:27*

In the model I have, which visually is the same as the one shown in the photo, the rotary switch turns on the machine and the red switch gives current to the extractor and to the water pump.


---
**Nathan Thomas** *March 06, 2017 23:10*

**+PopsE Cas** thank you sir


---
**Frank Dart** *March 06, 2017 23:38*

Wow... was just about to post a pic of mine (same panel) for a note to **+Don Kleinschnitz**.

Basically the top right knob controls power overall (if the E-stop isnt engaged).

The laser button to the right of the digital display will turn the power to the laser on or off.  If you see numbers in the display it is powered, if its just a decimal point, it is off.

The lower right red switch, seems to only control power to the controller board.  So please be aware that it is still possible for the laser to fire even with this switch in the off position.

Hope that clears that up a little.



Somewhat related message for Don:

That D+ D- jumper on the red power supply we were discussing seems to relate to that lower red switch that powers the controller.  I had left the jumper off, thinking it was pointless, and did a few test fires with the button... everything normal.

Next I tried running a quick cut job, steppers moved along with the job, but laser wasnt firing.  If I pressed the test while it was running the laser did fire.

I powered it all down and put the jumper back on and everything worked perfect.  So it appears that the jumper is acting in line with that switch and only affecting the boards ability to fire the laser while still allowing manual firing from the control panel.  I can live with that... lol.




---
**Don Kleinschnitz Jr.** *March 07, 2017 00:38*

**+Frank Dart** your saying that the lower red switch switches the DC on/of to the controller?



With that red switch on and the jumper out the laser does not fire during the job?



With the red switch on and the jumper in the laser does fire during the job...



That's weird, Is the LPS connected to the Red switch in any way?






---
**Frank Dart** *March 07, 2017 01:08*

yes to all 3..

Only had a few minutes to look, but yes one of the wires off the switch goes to the 5v on the LPS.

Next chance I get I'll trace out the others.


---
**E Caswell** *March 07, 2017 14:30*

laser switch (Red one on bottom panel)

![images/4cd3c533e074dc0e438a70e786327b6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4cd3c533e074dc0e438a70e786327b6a.jpeg)


---
**Don Kleinschnitz Jr.** *March 07, 2017 14:59*

**+PopsE Cas** ok I get that the DC supply is interrupted by the lower red switch. 

Pretty strange way to stop the laser from fireing ... but whatever. Not great for the controller electronics, also not a good way to make the machine safe..



OK, got it..... I had a neuron leak last night.



With that red switch on and the jumper out the laser does not fire during the job? ANS: that's cause the interlock circuit is open but controller is on.



With the red switch on and the jumper in the laser does fire during the job... ANS: that's cause the interlock is closed and controller is on.  



The D jumper is independent of the RED dc power switch. The laser is disabled from being fired by the controller by turning off its DC power?



Man I would rewire this thing like a standard K40 with interlocks and a single fire enable switch.




---
**E Caswell** *March 07, 2017 16:12*

**+Frank Dart** **+Don Kleinschnitz** tested a few things today with what Frank said on previous post. 



With the red power switch off. (But laser selected on with switch next to LED panel) 



- No laser operation when asked for by software, (does start as soon as switch selected on during any Laser work)

- Test button does not fire test.



As for rewire, I agree. It's not up to the required spec but not a priority currently.






---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/MSLoBMykw8r) &mdash; content and formatting may not be reliable*
