---
layout: post
title: "A small contribution to the community.. May it help keep members healthier by extracting those fumes away!"
date: July 20, 2015 01:21
category: "Repository and designs"
author: "Fadi Kahhaleh"
---
A small contribution to the community.. May it help keep members healthier by extracting those fumes away!



[http://www.thingiverse.com/thing:932739](http://www.thingiverse.com/thing:932739)





**"Fadi Kahhaleh"**

---
---
**Joey Fitzpatrick** *July 20, 2015 13:19*

Funny you should post this. I made one just like it this weekend.  I found the design here [https://www.thingiverse.com/thing:454820](https://www.thingiverse.com/thing:454820)  .  I have not installed it yet because i am waiting on an inline fan to arrive.  Hopefully this design will work better that the terrible bathroom fan that the units are shipped with.  


---
**Fadi Kahhaleh** *July 20, 2015 22:43*

**+Joey Fitzpatrick** what a sheer coincidence!! I actually did search Thingiverse.com but that didn't come up for some reason. After some googling found [cnczone.com](http://cnczone.com) post and made slight modifications to accomodate for my setup.



Thanks for sharing though.


---
**Lars Mohler** *July 21, 2015 16:51*

Thanks for the contribution. I was looking for something like this.


---
**Cam Mayor** *August 01, 2015 03:35*

I built one without checking and it turned out my outlet was narrower than the ducting adapter. Missed it by about a couple of inches.


---
**Fadi Kahhaleh** *August 01, 2015 05:10*

Sorry to hear that... what can I say, Chinese K40 Quality Control... One would think they should all be extremely similar in dimensions!


---
**Cam Mayor** *August 01, 2015 13:06*

There may have been a change up in the machine layout. They probably found an even cheaper fan to use and narrowed up the brackets. I'm just hoping everyone is not as inattentive as myself and actually measures first. It is a great project though, I had a lot of fun building it.


---
*Imported from [Google+](https://plus.google.com/115068648817066933731/posts/JnVC1hvEuWB) &mdash; content and formatting may not be reliable*
