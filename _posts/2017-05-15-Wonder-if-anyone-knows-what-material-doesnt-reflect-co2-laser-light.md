---
layout: post
title: "Wonder if anyone knows what material doesn't reflect co2 laser light?"
date: May 15, 2017 18:31
category: "Modification"
author: "ALFAHOBBIES"
---
Wonder if anyone knows what material doesn't reflect co2 laser light? I need something to put in the bottom of my laser that doesn't burn and will not reflect laser light back up at the underside of parts being cut.





**"ALFAHOBBIES"**

---
---
**Phillip Conroy** *May 15, 2017 19:01*

Cement sheeting,what bed are you using?


---
**Phillip Conroy** *May 15, 2017 19:04*

All metals will refect the laser beam,how far from metal is your cutting material.what pressure air assist are you using?


---
**ALFAHOBBIES** *May 15, 2017 19:08*

Cement sounds good. I'm using my own bed design. It's mostly open so I want to put something in the bottom of the machine.


---
**Phillip Conroy** *May 15, 2017 19:17*

Can you add a photo of your bed and tell me how far it is away from metal below it?


---
**ALFAHOBBIES** *May 15, 2017 19:33*

![images/3bb04346ed918d8bf2f1416a0454134e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3bb04346ed918d8bf2f1416a0454134e.jpeg)


---
**ALFAHOBBIES** *May 15, 2017 19:33*

![images/0621adbaff6d2d8a2837a3ecc5635389.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0621adbaff6d2d8a2837a3ecc5635389.jpeg)


---
**ALFAHOBBIES** *May 15, 2017 19:33*

![images/a79b949f643cbcaf97c1c26d35e19222.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a79b949f643cbcaf97c1c26d35e19222.jpeg)


---
**ALFAHOBBIES** *May 15, 2017 19:38*

You can see in the first picture the hole in the bottom of the machine. I covered all the holes with aluminum duct tape but of course that reflects a lot of the light. I think I will try 1/4" cement board in the bottom. Also may try some in the frame too.


---
**Don Kleinschnitz Jr.** *May 15, 2017 20:59*

I think anodize Al absorbs not reflects CO2 wavelength. 


---
**ALFAHOBBIES** *May 15, 2017 22:21*

Just thinking more about it and thought I saw someone use ceramic tile upside-down. I have some around that I could try.


---
**Don Kleinschnitz Jr.** *May 15, 2017 22:24*

**+ALFAHOBBIES** water is a good absorb-er @ CO2  wavelengths :). 


---
**Ned Hill** *May 15, 2017 22:38*

You can reduce/diffusion  reflection from a metal plate by roughing it up hard with like 80 grit sandpaper.  The resulting grooves increase absorption and what does get reflected is greatly dispersed.


---
**Stephane Buisson** *May 15, 2017 23:50*

the hole at the bottom is an air input, needed if you want a good air circulation & exhaust.

also handy for cleaning the ashes and bits away.


---
**ALFAHOBBIES** *May 16, 2017 00:28*

The hole also let's the laser light burn anything that sits under it. I just let more air in the front. Almost set a box of plywood on fire because of that hole.


---
**Anthony Bolgar** *May 16, 2017 01:09*

I put an aluminum oven tray liner under mine to protect the table top.


---
*Imported from [Google+](https://plus.google.com/118265639968468013312/posts/fzPEzDHiwMY) &mdash; content and formatting may not be reliable*
