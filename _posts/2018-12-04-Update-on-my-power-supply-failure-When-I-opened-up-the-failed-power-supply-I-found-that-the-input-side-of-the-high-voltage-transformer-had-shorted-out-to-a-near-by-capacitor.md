---
layout: post
title: "Update on my power supply failure. When I opened up the failed power supply, I found that the input side of the high voltage transformer had shorted out to a near by capacitor"
date: December 04, 2018 00:36
category: "Hardware and Laser settings"
author: "HalfNormal"
---
Update on my power supply failure.



When I opened up the failed power supply, I found that the input side of the high voltage transformer had shorted out to a near by capacitor. I opened the new power supply and found that the line followed the same path. I insulated the capacitor and line with kapton tape. I then added some non conductive foam between the problem areas. A quick inspection of the failed power supply also showed that the output line was rubbing on the power supply case. I made sure this was also isolated on the new power supply. 



**+Don Kleinschnitz Jr.** The power supply failure was not related to the oil from the refrigeration compressor in the cooling water.



Everything is working good as new. (as it should!)





![images/d57b70e31e9c8fba2c8b00aebc87203a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d57b70e31e9c8fba2c8b00aebc87203a.jpeg)
![images/1529a03b76735372c6706bc6871cd8ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1529a03b76735372c6706bc6871cd8ac.jpeg)

**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *December 04, 2018 15:38*

First time I have seen this kind of failure. Is the old supply working now?


---
**HalfNormal** *December 04, 2018 15:55*

**+Don Kleinschnitz Jr.** I have not done any repairs on the damaged PS. The replacement was modified with additional insulation and inspection of all areas that could possibly rub and cause a similar failure.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/RWqGKQSiuqd) &mdash; content and formatting may not be reliable*
