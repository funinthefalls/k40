---
layout: post
title: "Hi all, I've a question bout mirror alignment"
date: July 13, 2017 09:12
category: "Hardware and Laser settings"
author: "Francesco Primiceri"
---
Hi all, I've a question bout mirror alignment.

after adjusting the first mirror I tested the second. The result in the image below. when I move the gantry in the bottom and after in the top I have 2 different point on right and on left. Can I adjust this problem by adjusting with screws or my gantry is not square?



Thanks in advance.

![images/1bcbb732367ea86affdcef8f6ff568f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bcbb732367ea86affdcef8f6ff568f5.jpeg)



**"Francesco Primiceri"**

---
---
**Chris Hurley** *July 13, 2017 11:34*

Ok so think of it like this. You have two points which the beam passes though. This will give you the path of the beam. You need to adjust the first mirror to get those spots together. 


---
**Francesco Primiceri** *July 13, 2017 11:39*

**+Chris Hurley** Ok, you don't think that my gantry is not square? it's only a problem of first mirror alignment?


---
**Chris Hurley** *July 13, 2017 11:41*

Yes. It's the first mirror. 


---
**Chris Hurley** *July 13, 2017 11:44*

Like this. 

![images/6daa592b1333d2d16b821ccd3d03d76d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6daa592b1333d2d16b821ccd3d03d76d.jpeg)


---
**Chris Hurley** *July 13, 2017 11:45*

Sorry for the not so straight lines. 


---
**HP Persson** *July 13, 2017 15:13*

You can never align this with the mirrors. If the dot moves sideways, your gantry is not square.

I wrote a easy guide how to see whats up with the gantry, check it out.

When done, you have aligned everything too, as a bonus ;)



Start with the 2nd mirror in bottom position, adjust 1st mirror until you hit center.

Move 2nd mirror to top position, test fire again.

If the dot moved sideways, gantry is not square.

If the dot moved up-down, gantry is not level.



[k40laser.se - Mirror alignment - no more sweat and tears - K40 Laser](https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/)


---
**Francesco Primiceri** *July 13, 2017 17:01*

Ok, and now I've the laser beam in the mirror 2 in same position when the gantry is in the bottom or in the top but now the problem is that the laser beam isn't in the center of the mirror 2 but in the top left. which is the problem?




---
*Imported from [Google+](https://plus.google.com/+FrancescoPrimiceri/posts/Fm7wboyBSML) &mdash; content and formatting may not be reliable*
