---
layout: post
title: "Any idea what to check? Does not happen often but annoying when it does"
date: December 14, 2016 02:15
category: "Hardware and Laser settings"
author: "Kelly S"
---
Any idea what to check?  Does not happen often but annoying when it does.  Do not even run it fast.

![images/5f5b2fd9dde00185c53ee9b07555a106.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5f5b2fd9dde00185c53ee9b07555a106.jpeg)



**"Kelly S"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2016 02:17*

What is the issue?


---
**Ariel Yahni (UniKpty)** *December 14, 2016 02:19*

If you are sure the material is not moving then it could be the belts. Check and see if the have the same tension more or less


---
**Kelly S** *December 14, 2016 02:22*

Will do, and Ray the focal point shifted to the right, just did it again actually.  grrr. 


---
**Kelly S** *December 14, 2016 02:23*

And now it is back where it belonged lol.. maybe my stuff is shifting




---
**Kelly S** *December 14, 2016 02:31*

So after further review, it seems the material shifted. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 14, 2016 02:43*

I suggest using magnets to hold the material in place. But if it isn't shifting, as Ariel said, the belt could be skipping steps.


---
**Kelly S** *December 14, 2016 02:49*

Ah that would be hard, bed is aluminum and is then standing on cones for air flow.  :o  The wood I was using was rough cut ply and I think it shifted sitting on some fibers.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 14, 2016 02:56*

**+Kelly S** Ah maybe double-sided tape or something similar will do the trick instead.


---
**greg greene** *December 14, 2016 03:15*

also check the beam is not hitting the side walls of the air nozzle in all points of travel. This will happen if your beam is not hitting the center of all the mirrors.


---
**Stephane Buisson** *December 14, 2016 07:47*

also check if the lens isn't loose in the holder


---
**Madyn3D CNC, LLC** *December 16, 2016 15:02*

This happened to me before, problem went away once I re-adjusted the lens and the mirror ON the laser head. Something tells me it had to do with the lens being off-centered. 




---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/XsPXWG2hJf2) &mdash; content and formatting may not be reliable*
