---
layout: post
title: "This is one of the Pot Pourrie boxes, I was thinking of offering custom made one with pictures on them"
date: August 02, 2016 15:28
category: "Object produced with laser"
author: "Tony Schelts"
---
This is one of the Pot Pourrie boxes, I was thinking of offering custom made one with pictures on them. Just a thought.  My Daughter loved this one I made with my grand daughters on them. The top panel come off you put your finger in the large heart at the bottom and it comes of.



![images/1b053a776d9407adb041de5d013b6ca5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b053a776d9407adb041de5d013b6ca5.jpeg)
![images/35c528a481dcb998ade5bb3274543ee2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/35c528a481dcb998ade5bb3274543ee2.jpeg)
![images/5e87c04853a7e21cb5c2c13cbb3eff16.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5e87c04853a7e21cb5c2c13cbb3eff16.jpeg)
![images/663d972742540959a434b795a6176d8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/663d972742540959a434b795a6176d8c.jpeg)
![images/f2dc96f049f4a5f32aa54bc912d27c2a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f2dc96f049f4a5f32aa54bc912d27c2a.jpeg)

**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 02, 2016 15:51*

That's very nice Tony. I think it is worthwhile to see if you can sell them as a custom product. Just a thought, get people to pay 1/2 in advance (as I've been stung on some custom jobs that people never ended up paying for or picking up).


---
**greg greene** *August 02, 2016 16:45*

what wood did you use?


---
**Tony Schelts** *August 02, 2016 17:15*

3mm laser birch ply.


---
**Robert Selvey** *August 04, 2016 02:54*

Where is a good place to find 3mm birch ?


---
**Tony Schelts** *August 04, 2016 07:40*

Im in the uk, but i purchased 100x 375 x 300 for about £80.00 really fast delivery.  This is the company on Ebay and here is a link for 50 boards.  Its good stuff.

[http://www.ebay.co.uk/itm/3mm-Birch-laser-plywood-50-sheets-300-x-375-for-Laser-CNC-Pyrography-crafts-/122052678715?hash=item1c6ae8143b:g:3d0AAOSwyQtVninx](http://www.ebay.co.uk/itm/3mm-Birch-laser-plywood-50-sheets-300-x-375-for-Laser-CNC-Pyrography-crafts-/122052678715?hash=item1c6ae8143b:g:3d0AAOSwyQtVninx)


---
**Bob Damato** *August 10, 2016 16:40*

**+Robert Selvey** I get poplar at home depot in really nice sizes.




---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/KVLNTi74MgH) &mdash; content and formatting may not be reliable*
