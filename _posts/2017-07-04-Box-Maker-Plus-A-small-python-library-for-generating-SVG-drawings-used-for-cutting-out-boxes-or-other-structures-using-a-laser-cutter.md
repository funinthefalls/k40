---
layout: post
title: "Box Maker Plus! \"A small python library for generating SVG drawings used for cutting out boxes or other structures using a laser cutter"
date: July 04, 2017 15:18
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Box Maker Plus!



"A small python library for generating SVG drawings used for 

cutting out boxes or other structures using a laser cutter. 



It also comes with a set of ready-to-use, fully parametrized generators: 



* Various simple boxes 

* Flex boxes with rounded corners and living hinges 

* Type trays with and without walls and floors 

* Book cover with flex spine 

* Magazine file 



And a few one trick ponies: 



* An Arcade Cabinet 

* A drill stand 

* A castle tower 

* A housing for a special kind of lamp 

* A cutlery stand 



It also offers an API to easily draw your own parts."



[https://hackaday.io/project/10649-boxespy](https://hackaday.io/project/10649-boxespy)





**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/2KzpCa9iYtj) &mdash; content and formatting may not be reliable*
