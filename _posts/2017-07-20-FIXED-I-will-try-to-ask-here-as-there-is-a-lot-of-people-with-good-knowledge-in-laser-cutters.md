---
layout: post
title: "FIXED! I will try to ask here as there is a lot of people with good knowledge in laser cutters.."
date: July 20, 2017 13:50
category: "Modification"
author: "Kostas Filosofou"
---
FIXED!



I will try to ask here as there is a lot of people with good knowledge in laser cutters.. 

After i have almost finish with my custom 50w laser build i have make some test cuts .. the pieces i cut was squares and circles , in my X axis i am dead right in my measurement but in my Y axis i am off about 0.2mm.

I have measure my Y axis distance with dial gauge it was right .. i have fire laser dot in distance from 0-10, 0-100, 0-200 mm is dead right .. but when i cut a piece i am off .. Any thoughts why is this happened ??



I am using #C3D mini with LW3﻿



![images/857e80c005814b4d440c073e99c4c514.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/857e80c005814b4d440c073e99c4c514.jpeg)
![images/edded81d8a79db4acdb78c2ec966b7fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/edded81d8a79db4acdb78c2ec966b7fb.jpeg)

**"Kostas Filosofou"**

---
---
**Don Kleinschnitz Jr.** *July 20, 2017 14:16*

Are you saying that you measured the Y movement without cutting and it is dead on. Then when you measure it when cutting it is off <b>.2mm</b>?



How did you measure these .....?


---
**Ariel Yahni (UniKpty)** *July 20, 2017 14:19*

Nice build. Why LW3 and not LW4


---
**Kostas Filosofou** *July 20, 2017 14:23*

**+Don Kleinschnitz** measure what? the 0.2mm difference in actual piece i was cut do you mean ? with my caliper .. 


---
**Kostas Filosofou** *July 20, 2017 14:25*

**+Ariel Yahni** Thanks ! .. i wanted first to finish with my build and then i start to learn the LW4 ... as i find it a bit complicated :) 


---
**Don Kleinschnitz Jr.** *July 20, 2017 14:45*

**+Kostas Filosofou** you measured the cut piece and it was off .2mm from the design? 

Is it off this amount on both sides? Edges are often not cut square.

.... .2mm is not a lot?


---
**Kostas Filosofou** *July 20, 2017 14:58*

**+Don Kleinschnitz** exactly was off about 0.2mm of the design .. but only on my Y axis .. yes i was measure both sides-edges of the piece .. I know is not that match ..but you know .. 

I was searching in the web now and a find a article that says maybe is the mirror alignment the problem .. as i am sure that i am 100% center i'll check it all from beginning on more time ,maybe i miss something


---
**Ariel Yahni (UniKpty)** *July 20, 2017 14:59*

**+Kostas Filosofou**​ let me know if I can help you on LW4 or take a look at my stating up videos


---
**Ariel Yahni (UniKpty)** *July 20, 2017 15:08*

Maybe steps or something is preventing correct movement


---
**Ariel Yahni (UniKpty)** *July 20, 2017 15:09*

**+ThantiK**​ this could be what you where looking for


---
**Kostas Filosofou** *July 20, 2017 15:10*

**+Ariel Yahni** Thank you :) I have already see some of your videos ...good job by the way! definitely i watch it again when i start the learning procedure  


---
**Kostas Filosofou** *July 20, 2017 15:15*

**+Ariel Yahni** i have already check my steps .. as i mention in my post i have measure manually ( i move the axis 1mm-10mm -100mm throw the LW) with my dial gauge and i am ok


---
**Kostas Filosofou** *July 20, 2017 15:44*

**+Dan Shookowsky** how do I know that?


---
**Alex Krause** *July 20, 2017 15:46*

Are you accounting for the kerf of the laserbeam?


---
**Kostas Filosofou** *July 20, 2017 15:48*

**+Dan Shookowsky** I am using illustrator .. 


---
**Kostas Filosofou** *July 20, 2017 15:51*

**+Dan Shookowsky** oh.  I didn't thought about that I'll try it


---
**Ariel Yahni (UniKpty)** *July 20, 2017 16:13*

I'm not sure at this point if LW accounted for the line width 


---
**Alex Krause** *July 20, 2017 16:14*

Also have you tested LW4 to see if the same error occurs, I see you stated you are running Lw3


---
**Kostas Filosofou** *July 20, 2017 16:20*

**+Alex Krause** no but I try it also .. I want at first to be sure that the problem isn't hardware-mechanical


---
**Don Kleinschnitz Jr.** *July 20, 2017 16:39*

**+Kostas Filosofou** you can always look at the Gcode to see its positioning correctly.


---
**Claudio Prezzi** *July 20, 2017 22:26*

I think what you see is the difference in with and height of the laser beam. It's not an ideal round dot, it's more of a oval.



LW3 does not compensate the laser kerf (cut width). in LW4 you have the option to select a "cut outside" operation which will compensate the beam width.


---
**Kostas Filosofou** *July 20, 2017 22:45*

**+Claudio Prezzi** ok maybe is a good time to switch to LW4


---
**Claudio Prezzi** *July 21, 2017 07:02*

:-)


---
**Kostas Filosofou** *July 23, 2017 18:26*

**+Claudio Prezzi**​ i think i leave it there :) .. thanks for the tip that was the solution (cut outside) .. now I must address the rest of the 'problems' that I didn't have with LW3 .. for example in the non cutting move's the machine moved too slow and I didn't find any setting for this...

Anyway thanks for the tip

![images/b4af6d26b465736531b969559bd72985.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b4af6d26b465736531b969559bd72985.jpeg)


---
**Claudio Prezzi** *July 25, 2017 06:35*

The default for travel speed for Smoothieware is usualy defined in config.txt on the SD Card, but can also be changed by a G0Fxxx (mm/min)in the gcode (or via Console).


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/jD4k1Z1QXzf) &mdash; content and formatting may not be reliable*
