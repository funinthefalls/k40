---
layout: post
title: "Met Dan Shapiro from Glowforce lasers. We both make wallets "
date: May 21, 2017 02:12
category: "Discussion"
author: "Paul de Groot"
---
Met Dan Shapiro from Glowforce lasers. We both make wallets 😁

![images/e7291432031fdcefbd252d3bbd30a892.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e7291432031fdcefbd252d3bbd30a892.jpeg)



**"Paul de Groot"**

---
---
**Joe Alexander** *May 21, 2017 02:36*

that the bay area maker faire? I wanted to go this year but had a graduation to attend :)


---
**Paul de Groot** *May 21, 2017 03:08*

**+Joe Alexander** it's at San Mateo 😁


---
**Don Kleinschnitz Jr.** *May 21, 2017 04:00*

Wish I could have made it :(.


---
**Paul de Groot** *May 21, 2017 04:02*

**+Don Kleinschnitz** yes I always tell myself next year i have a stand.  So finally I do😊


---
**Don Kleinschnitz Jr.** *May 21, 2017 11:16*

**+Paul de Groot** how about a picture of the stand.


---
**Jim Hatch** *May 21, 2017 14:31*

I have a Glowforge Pre-Release unit from them. Works so much easier than the K40. But it's nearly 10 times the price. For a reasonably capable tinkerer it's not 10x better in functionality than the K40. Perfect for its market though. Kinda like a Honda motorcycle vs a do it yourself minibike. They'll both do the job but they're both for different markets 🙂


---
**Paul de Groot** *May 21, 2017 14:48*

**+Jim Hatch** I agree a stock K40 is just a toy. I basically hacked the K40 and made it into an open source laser. Great for people with a small budget. 


---
**Jim Hatch** *May 21, 2017 16:32*

You can do an awful lot with a K40 if you're willing & able to mod it. Some of the mods are easy and cheap and some get challenging and more expensive. But at a $350 entry price it can do an awful lot. To get into business with laser produced stuff, you'll be needing the bigger more expensive mods. Even then you're probably only 1/3 the cost of a K40. It's a great entry level tool though.


---
**Paul de Groot** *May 28, 2017 04:59*

**+Don Kleinschnitz** did not pick this up before. So here is a picture 😊

![images/00a775414cc4a486d5f33986d1eeeae3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00a775414cc4a486d5f33986d1eeeae3.jpeg)


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/Yt8ugM2d6aS) &mdash; content and formatting may not be reliable*
