---
layout: post
title: "New issues to solve! Yeah! I have solved the issue with my x and y home"
date: November 07, 2016 16:07
category: "Smoothieboard Modification"
author: "George Fetters"
---
New issues to solve!  Yeah!



I have solved the issue with my x and y home.  The issue turned out to be my home made adapter for the ribbon cable.  There were 2 ground wires in it that needed to be wired together on my adapter and fed out to the controller.  I have 2 more issues to solve and then I will be cutting.



1. The laser does not fire when I send gcode to it.  I am using gcode generated out of inkscape firing on command M03.  I am not sure if I need to set something on the config for that or not.  I am using pin 3.26 on the Azteeg mini x5 to fire and running that through a 3.3v to 5v bidirectional logic level converter out to L on the powersupply.



2. If I hold down the test button I can etch.  I noticed that the cut is backwards and upside down.  I'm sure there is a way to solve this.  Do I need to invert the X and Y direction in the config file?



Thanks for all your help





**"George Fetters"**

---
---
**George Fetters** *November 07, 2016 16:54*

I have the laser_module configured in config.ini  Thats good to knwo about g1 and g0.  I will give laserweb a try thanks!


---
**George Fetters** *November 10, 2016 03:20*

Ok I think I have figured out why the laser is not firing.  I switched to using G1 and G0.  This did not fix the problem.  I am using an Azteeg X5 mini and apparently there are just 3 pins that provide pwm.  All 3 of these pins pass the reference voltage from the power supply out the pwm pin.  My powersupply is 24v.  To do this right I think I need an optical-isolator that can be controlled using 24v to control the 5v from the powersupply to L.  Is anyone else controlling their laser this way?


---
*Imported from [Google+](https://plus.google.com/114125781899580900020/posts/7t4r2WQc2Pw) &mdash; content and formatting may not be reliable*
