---
layout: post
title: "Hi Guys question can i run laserweb with out having a board in machine to actually run the program on i want to see what it looks like before i go and tare my machine apart again thanks"
date: May 01, 2016 17:27
category: "Discussion"
author: "Dennis Fuente"
---
Hi Guys question can i run laserweb with out having a board in machine to actually run the program on i want to see what it looks like before i go and tare my machine apart again thanks





**"Dennis Fuente"**

---
---
**Jim Hatch** *May 01, 2016 17:49*

Yes. I have run it on my laptop as a way to make sure it would work before going to the trouble of installing the Smoothie.


---
**Ariel Yahni (UniKpty)** *May 01, 2016 18:21*

Sure. Visit [www.laserweb.xyz](http://www.laserweb.xyz) for V1 or beta v2 at [testv2.laserweb.xyz](http://testv2.laserweb.xyz) l


---
**Dennis Fuente** *May 01, 2016 18:31*

Jim thats exactly what i want to do did you install a smoothie and laserweb how do you like it thanks Ariel for your reply i am going to test it out oh and one other thing will it run ok on win XP


---
**Ariel Yahni (UniKpty)** *May 01, 2016 18:54*

**+Dennis Fuente**​ should not have any issues just make sure u follow exactly the install instructions for LW1


---
**Jim Hatch** *May 01, 2016 18:54*

Don't know about XP. It does run on Win10 (my machine) and the instructions mention Win 7/8 so those are probably a good bet.



I haven't installed my Smoothie yet. I'm making up a middle board so I can use the same connectors as are on my Nano board. That way if it I have issues I can revert to stock pretty quickly.


---
**Dennis Fuente** *May 01, 2016 19:14*

Areil 

Thanks for the link i down loaded the zip file but i don't see an EXE to install it is this cloud based thanks


---
**Dennis Fuente** *May 01, 2016 19:17*

Jim i have a nano board in my machine most interested in your installation whats a middle board and which smoothie did oyu go with Thanks 


---
**Ariel Yahni (UniKpty)** *May 01, 2016 19:19*

**+Dennis Fuente**​ look at my collection named K40 upgrades


---
**Ariel Yahni (UniKpty)** *May 01, 2016 19:22*

**+Dennis Fuente**​ on that page follow this instructions [http://imgur.com/Erk97k9](http://imgur.com/Erk97k9)


---
**Jim Hatch** *May 01, 2016 19:49*

I got a Smoothie 4XC - the 3 wasn't available.



The stock board uses a flat wire connector to go to the board from the X-axis stepper motor. A middle board allows you to socket a flat wire connector but use a standard wire connector (molex type) to hook up the Smoothie. All I'll have to do is swap wires to revert.



Someone here did a really cool middle board for their Moshi board to Smoothie conversion that used dip switches to swap between the boards. Nice but the more manual wire swap will do me okay for now. It's only an insurance policy for a failed Smoothie anyway.


---
**Dennis Fuente** *May 01, 2016 20:32*

Jim Did smoothie raise the price i understand they are coming out with a V2 i have done several CNC related upgrades to this same kind of change and once i was commitited no going back and the machine was down learnd my lesson there i want to run software and be able to use my machine during a converstion Thanks


---
**Dennis Fuente** *May 01, 2016 20:33*

Ariel Thanks working on it 


---
**Dennis Fuente** *May 01, 2016 20:38*

Ariel the computer that is running my 

K40 is not connected to the web as i am running several different CNC type machine through this machine i don't want any virous on it i have had that happen before and lost all my designs and CNC machine setups so thats a no go for me is this a requirement that the machine be connected to the web. Thanks 


---
**Ariel Yahni (UniKpty)** *May 01, 2016 20:47*

**+Dennis Fuente**​ for LW2 you go to a hosted github which is very secure. LW1 is a local install based in the above picture 


---
**Jim Hatch** *May 02, 2016 03:21*

**+Dennis Fuente** I don't know about Smoothie old pricing but the 3 series is never in stock at Uberclock (US supplier) but was running $99 when they had it. Another $40 got me the 4XC which has some additional features like another stepper controller, ethernet on board, etc.



There are Smoothie replicas available on eBay for 1/2 that price. Peter recommended a couple awhile back. 


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/WXJGTyssjbH) &mdash; content and formatting may not be reliable*
