---
layout: post
title: "Need a little help I just replaced my mirrors and lens and I am having a horrible time getting it to cut again"
date: October 28, 2016 02:10
category: "Discussion"
author: "3D Laser"
---
Need a little help I just replaced my mirrors and lens and I am having a horrible time getting it to cut again.  I got it all aligned and fed on but it seems the focal length just isn't right.  I am getting a very wide beam and can't seem to get it to focus enough to cut right.  I have done the ramp test and still can't find a goo distance.  Could my lens be faulty or am I missing something 





**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2016 02:33*

Oh, I just posted elsewhere suggesting a ramp test... Ignore that. Any chance you can post a pic of the ramp test result (whilst the ramp is still in the machine)?


---
**Scott Marshall** *October 28, 2016 02:43*

Is your lens in correctly?



The concave side faces the incoming beam (handy rule that works miniscus or plano-convex)



I just wrote a 6 page article on mirrors and lenses. It will be up soon On [EverythingLasers.com](http://EverythingLasers.com)







[everythinglasers.com - EverythingLasers - A site for everything lasers, for and by makers.](http://EverythingLasers.com)


---
**Alex Krause** *October 28, 2016 03:39*

You had mentioned that you purchased a 3" focal length mirror... depending on the diameter of the mirror as well you beam will be larger and also less powerful than the original focal length mirror with 2" focal length


---
**Alex Krause** *October 28, 2016 03:39*

![images/047ad01e2f85cbc3684848c28a8586f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/047ad01e2f85cbc3684848c28a8586f2.jpeg)


---
**Scott Marshall** *October 28, 2016 06:39*

Alex is right, I didn't see that. I have a 3" lens and it' so soft, I've yet to find anything it's really good at. I have the bottom cut out of my blue K40 so bought the long lens to engrave large items like baseball bats that fit below the floor. It works, but only for lettering. If you try to do anything finer (more detailed) with it, the results are marginal.



The 2 1/2" lens is a cutting animal though, it's by far king in thick materials, 3mm and up ply etc.



As the focal length gets longer, the minimum beam diameter increases but the 'in focus' zone gets longer too. This longer, more parallel cutting zone puts the entire thickness of the plywood in the sweet spot.



The 3" lens may be a great lens for cutting very thick materials in higher power laser, but with only 30w on tap, it seems to reduce the watt density to the point where it's not very good at cutting tough materials. It may be great for foam or that sort of material where it's thick but easily cut.


---
**3D Laser** *October 28, 2016 10:13*

**+Alex Krause**  I got a new 2 inch lens now


---
**3D Laser** *October 28, 2016 10:14*

**+Yuusuf Sallahuddin** sure I will try to do that later today


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2016 10:34*

**+Corey Budwine** Hopefully that can assist us all with helping out, but from the comments I've seen from Scott & Alex, it's looking like the fault may lie with the larger focal point lens having a wider beam anyway (which I'm glad to find out for my own knowledge).


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/WjBhneHmkTJ) &mdash; content and formatting may not be reliable*
