---
layout: post
title: "Can someone point me to a good source for patterns for cardboard stacked projects"
date: March 17, 2018 21:09
category: "Repository and designs"
author: "timb12957"
---
Can someone point me to a good source for patterns for cardboard stacked projects. I have found a few, but struggle with the fact that some are meant for larger lasers and thus the scale does not match up if I reduce it to my 12" x 8" cutting area and use 1/8 cardboard. The proportions are off.





**"timb12957"**

---
---
**Ariel Yahni (UniKpty)** *March 17, 2018 22:05*

I case you.want to make your own, search for slicer for fusion 360


---
**HalfNormal** *March 17, 2018 23:31*

Break up the pattern into smaller areas. Cut out just the items that fits your area instead of the whole pattern.


---
**timb12957** *March 18, 2018 01:13*

I have explored slicer but not enough for it to be useful yet. Halfnormal, what if I do not know the pattern was meant to be 1/8 thick?


---
**HalfNormal** *March 18, 2018 01:17*

A slicer is not what need. You need a program that can read the file ie cad or graphics program. Unfortunately if the pattern is not made for 1/8 inch you will have to manually modify it. See the post about the dinosaur.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/D163Ga1vxJt) &mdash; content and formatting may not be reliable*
