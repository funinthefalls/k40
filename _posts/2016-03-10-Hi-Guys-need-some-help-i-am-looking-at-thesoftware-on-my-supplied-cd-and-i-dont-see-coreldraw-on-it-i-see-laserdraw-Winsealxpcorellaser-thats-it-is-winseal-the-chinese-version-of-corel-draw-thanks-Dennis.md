---
layout: post
title: "Hi Guys need some help i am looking at thesoftware on my supplied cd and i don't see coreldraw on it i see laserdraw, Winsealxp,corellaser, that's it is winseal the chinese version of corel draw thanks Dennis"
date: March 10, 2016 21:29
category: "Software"
author: "Dennis Fuente"
---
Hi Guys need some help i am looking at thesoftware on my supplied cd and i don't see coreldraw on it i see laserdraw, Winsealxp,corellaser, that's it is winseal the chinese version of corel draw

thanks 

Dennis 





**"Dennis Fuente"**

---
---
**Dennis Fuente** *March 10, 2016 21:33*

this is what i find on the cd thanks for any help





LaserDRW¡ª¡ªfor making stamp

CorelLASER¡ª¡ªfor artcraft engraving

WinsealXP¡ª¡ªfor making stamp



note: you need install CorelDraw 12 or even newer version, then, you can install CorelLASER


---
**Anthony Bolgar** *March 10, 2016 21:39*

You can use CorelDraw/Laser to make anything, you do not need the other programs


---
**Dennis Fuente** *March 10, 2016 23:27*

So i just install laser draw and that's all i need if so then to install it would i first install the dongle key then the program i don't see any drivers on the CD are they in the program install

Thanks 

Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2016 23:30*

No, the dongle key has nothing on it. It is just a hardware key, to enable the laser. If you don't have it plugged in, you won't be able to use the laser.



[https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing)

This is the K40 software (~690mb).



You will find in there CoreDraw12.rar (spelled incorrectly). You may find your disk has the same, just may have overlooked it because of the incorrect spelling.



Basically, install the CorelDraw12 from the .rar file. Then install CorelLaser. I can't remember if anything else was necessary to install (USB drivers or anything like that maybe?).


---
**Dennis Fuente** *March 10, 2016 23:35*

First off many thanks to all of you who on this forum hellping others strugling to get there machines up and running Thanks Yyysuf i will give that a try.



Dennis 


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/hGynr18weij) &mdash; content and formatting may not be reliable*
