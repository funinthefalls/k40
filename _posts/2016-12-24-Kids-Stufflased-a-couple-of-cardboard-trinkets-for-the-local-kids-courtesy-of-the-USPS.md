---
layout: post
title: "Kid's Stuff...lased a couple of cardboard trinkets for the local kids courtesy of the USPS..."
date: December 24, 2016 00:49
category: "Object produced with laser"
author: "Mike Meyer"
---
Kid's Stuff...lased a couple of cardboard trinkets for the local kids courtesy of the USPS...



![images/de7aa8a79e2a46c2bee4f0a13e521e8f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de7aa8a79e2a46c2bee4f0a13e521e8f.jpeg)
![images/9f52e9c1c764d8076a92f6afea03f54d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9f52e9c1c764d8076a92f6afea03f54d.jpeg)

**"Mike Meyer"**

---
---
**Catherine Sun** *December 24, 2016 01:57*

Can make this use the CO2 laser cutting machine


---
**Mike Meyer** *December 24, 2016 02:02*

If I understand you correctly, yes, that's how I made it, using a K40 laser.


---
**Catherine Sun** *December 24, 2016 02:07*

**+Mike Meyer** Good, work, yes


---
**Catherine Sun** *December 24, 2016 02:13*

**+Mike Meyer** Merry Christmas


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 24, 2016 04:12*

That's a crafty way to reuse some old cardboard & amuse the kids (& yourself) at the same time.


---
**Carl Fisher** *December 24, 2016 15:01*

That's awesome


---
**Mike Meyer** *December 24, 2016 19:43*

Thanks for the kind words! Merry Christmas to all!


---
**Bill Keeter** *December 28, 2016 14:53*

I haven't tried cutting cardboard. How are you avoiding burn marks? Air assist? If so, how are you keeping the material from moving around as it's cut out?


---
**Mike Meyer** *December 28, 2016 15:04*

I don't have air assist...the cardboard doesn't move around as it's cut...I cut with minimum power; the cardboard stock that the USPS uses seems to work best around 3ma of power...if you crank too high, then it will burn. You have to mess around a bit to find that sweet spot that cuts, but does not burn.


---
**Bill Keeter** *December 28, 2016 15:32*

Very cool. thx. I might try this out to make some custom small boxes.


---
**Matthew Wilson** *January 03, 2017 23:44*

Where or how do you get the designs to import into your program? Do you create them yourself or is there a community place to get these designs?


---
**Mike Meyer** *January 06, 2017 14:20*

Matthew...sorry for the delay, been way out of town...anyhow, I used the template published here:

[opensauceproject.wordpress.com - Cardboard Jets](https://opensauceproject.wordpress.com/2013/05/01/cardboard-jets/comment-page-1/)

I copied the jpeg into CorelDraw, and then I used the bezier tool to copy and modify it as needed. BTW, do a google search for "cardboard airplane templates" and you'll find lots...Have fun!


---
**Matthew Wilson** *January 06, 2017 16:33*

Awesome thanks! I finally got mine up and running last night. Aligning the bed was a huge pain, then aligning the mirrors was not as bad but time consuming.


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/VWg1WGgRdaP) &mdash; content and formatting may not be reliable*
