---
layout: post
title: "More Power to Me! I had 2, 20 amp, dedicated outlets installed today"
date: July 13, 2016 02:58
category: "Discussion"
author: "HalfNormal"
---
More Power to Me!



I had 2, 20 amp, dedicated outlets installed today. I can now run my compressor and two lasers without worrying about tripping breakers. I also had a 220, 30 amp circuit installed for future updates.





**"HalfNormal"**

---
---
**Anthony Bolgar** *July 13, 2016 11:40*

The 20A circuits will really help with reliability, 15 A is so close to tripping with things like Air compressors. Good idea to install the dedicated lines, don't need breakers popping when someone turns on the bathroom light. ;)


---
**Corey Renner** *July 13, 2016 17:11*

[http://s.quickmeme.com/img/95/95c6d2e44bc716d5f5aa106de2574371d5b349842efdb2468a1d2eb9d4ab9a67.jpg](http://s.quickmeme.com/img/95/95c6d2e44bc716d5f5aa106de2574371d5b349842efdb2468a1d2eb9d4ab9a67.jpg)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/6C5oEz9JmTq) &mdash; content and formatting may not be reliable*
