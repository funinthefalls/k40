---
layout: post
title: "Good image of proper lens orientation and difference between an average lens (plano) and good lens (meniscus)"
date: December 13, 2016 23:05
category: "Discussion"
author: "Anthony Bolgar"
---
Good image of proper lens orientation and difference between an average lens (plano) and good lens (meniscus)

![images/6314a9bd3ccf25fd5b819dac303ef646.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6314a9bd3ccf25fd5b819dac303ef646.jpeg)



**"Anthony Bolgar"**

---
---
**Ned Hill** *December 14, 2016 01:02*

Nice, and this image shows why the orientation of a Plano-convex lens is important.

![images/5bcf3dff7d1ddaf7466d4c212adffc80.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/5bcf3dff7d1ddaf7466d4c212adffc80.gif)


---
**K** *December 14, 2016 03:16*

Does a meniscus lens give better/higher power cutting? 


---
**Ned Hill** *December 14, 2016 03:42*

A meniscus lens would give you a thinner kerf and more effective cutting power.  However, they tend to be more expensive.


---
**K** *December 14, 2016 03:44*

**+Ned Hill** Thinner kerf, you say? Hmmm... I just looked it up on eBay and found one for $44.00 for a 2.5" fl. The 2" fl was listed at $599! I think maybe that was a typo.


---
**Ned Hill** *December 14, 2016 03:48*

All depends on the quality.  Last time I looked most of the ones I saw were running around $80 USD or more  for a 20mm with a 50.8mm focal.




---
**Ned Hill** *December 14, 2016 03:52*

I would be interested to know if anyone has upgraded to a meniscus lens and how much gain there actually was over a plano-convex.




---
**Anthony Bolgar** *December 14, 2016 04:11*

I am hoping Santa brings me one for Christmas.


---
**Anthony Bolgar** *December 14, 2016 14:12*

Wow, who would have thought this image would be so popular!


---
**Anthony Bolgar** *December 14, 2016 14:17*

I would love to find the person who reshared this to some magical laser community, all of a sudden I am getting 10 likes a minute. Wherever it is, it is a goldmine to get information out for projects like Smoothieboard and LaserWeb.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/Z1f2ydnebwB) &mdash; content and formatting may not be reliable*
