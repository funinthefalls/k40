---
layout: post
title: "Thinking about upgrading my 40watt Chinese laser with a DSP controller, power table/bed kit and a honeycomb bed for the surface......"
date: March 31, 2016 17:26
category: "Modification"
author: "Damien Thompson"
---
Thinking about upgrading my 40watt Chinese laser with a DSP controller, power table/bed kit and a honeycomb bed for the surface...... What do u guys think? Are them upgradesmworth upgrading? Also I have already ordered a air assist head and a 50mm lens.





**"Damien Thompson"**

---
---
**Scott Marshall** *April 01, 2016 01:31*

In my opinion, your money and time can be better spent.



The K40 doesn't have enough depth to use long focal length lenses (except on thin flat stock) without cutting the floor out (done to mine), but even then, a simple manual height adjustment adjustment system (anything from assorted blocks to a screw system will do) and nail bed work great and won't break the bank. 


---
**Brien Watson** *April 01, 2016 02:45*

A DSP controller you will not regret!!  Best move I ever did.


---
**Brooke Hedrick** *April 01, 2016 04:24*

Very happy with my DSP controller as well.


---
**Anthony Bolgar** *April 01, 2016 19:54*

There is nothing wrong with what you want to do, other than the cost. There are much cheaper options out there that would be just as good.


---
*Imported from [Google+](https://plus.google.com/100192457805389347663/posts/RNrdqo3oaDE) &mdash; content and formatting may not be reliable*
