---
layout: post
title: "Simple K40 adjustable pin bed: -steel sheet milled at a factory, unfortunately they only had square punch -350 rivets -some M6 threaded rod -M6 nuts -3d printed nut holders/adjusters \"Remember children 3 point define a plane\""
date: June 26, 2016 16:44
category: "Modification"
author: "Mircea Russu"
---
Simple K40 adjustable pin bed:

-steel sheet milled at a factory, unfortunately they only had square punch

-350 rivets

-some M6 threaded rod

-M6 nuts

-3d printed nut holders/adjusters



"Remember children 3 point define a plane" the math teacher said. The other 2 front rods are there only if I ever want a 4 corner support.



![images/40c56cf40c87fe4ad580894d27332aa5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40c56cf40c87fe4ad580894d27332aa5.jpeg)
![images/2354adf0a6a624b1502bbf89f1792f3f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2354adf0a6a624b1502bbf89f1792f3f.jpeg)
![images/53e87f788cd6a1d517179cd3e1514bed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/53e87f788cd6a1d517179cd3e1514bed.jpeg)
![images/f12f44579498cb9a26f586733c48b440.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f12f44579498cb9a26f586733c48b440.jpeg)
![images/65407516a8a4d39d43428933f73d5b60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65407516a8a4d39d43428933f73d5b60.jpeg)

**"Mircea Russu"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 17:25*

That's a good idea to use rivets. If you could use steel rivets would be great as magnets would grip on their tips (through material) to hold material in place (for soft/flexible materials such as fabric/leather).


---
**Mircea Russu** *June 26, 2016 17:43*

Only problem is rivets inserted by hand do not have all the same length. Did dremel out a few that were longer by more than a millimetre or so. I just lay down the material and adjust in 3 points near each top corner and bottom half. No need to use all 4 corners. I ordered the metal sheet cut also in front corners as I didn't know how much it will flex.




---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/A5vZ3UzjNEt) &mdash; content and formatting may not be reliable*
