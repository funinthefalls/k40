---
layout: post
title: "Hi folks, so i was using my cutter, and half way through a job the laser stopped firing"
date: March 19, 2016 18:11
category: "Hardware and Laser settings"
author: "Maldenarious"
---
Hi folks, so i was using my cutter, and half way through a job the laser stopped firing. Prior to this it has been working without a problem.  When i switch the machine on, the head moves to its default position, and i hear the laser fan power,  as i expect. However the laser now isn't firing, i can't even do a test fire. Any thoughts or ideas? I've had a check of the tube, and see no cracks. It feels like power just isn't reaching it.





**"Maldenarious"**

---
---
**Anthony Bolgar** *March 19, 2016 22:00*

does the tube give off any purple hint of color when you press the test fire button?. Also, have you checked to make sure that the wires are still connected to the laser enable button? I had one of the leads slip off the terminal from the small vibrations the machine was doing. 


---
**Maldenarious** *March 19, 2016 22:15*

I couldnt see any change in the tube at all, the current indicator dosent change either.  As for the laser enable button i've just been going over that, even tried removing it entirely and joining the wires (read that in another post), the results the same - i get a quiet buzzing sound from the power supply when firing the laser.


---
**Tom Spaulding** *March 19, 2016 23:51*

One test I saw was to put a packing peanut close to the tube but not touching. If it moves when you test fire the tube is probably the problem. If it doesn't move look for a power supply issue.


---
**Anthony Bolgar** *March 20, 2016 01:39*

I had a buzzing sound from my PSU when there was a simple short circuit on the 5V output.


---
**Maldenarious** *March 20, 2016 03:37*

i only get the buzz when i hit the test switch which makes sense to me, pretty sure its always been that way. 



I'm curious about failed tubes, to those of you who have experience with them, does your current indicator still function when the laser is broken?


---
**Maldenarious** *March 20, 2016 20:31*

have ordered a new psu, hopefully that is it.


---
**Maldenarious** *April 03, 2016 19:06*

new psu sorted it for me :)


---
*Imported from [Google+](https://plus.google.com/115599912786188133985/posts/CUUcdy4XH7c) &mdash; content and formatting may not be reliable*
