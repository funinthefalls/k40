---
layout: post
title: "I like working with 1/8 x 8 basswood"
date: September 18, 2017 02:41
category: "Materials and settings"
author: "timb12957"
---
I like working with 1/8 x 8 basswood. I have been buying just a couple of sheets at a time from Hobby Lobby. The cost for a 24" sheet is $8.99 (can use 40% off coupon for one). If I wanted to buy larger quantities, say 10 sheets, does anyone know a less expensive source?





**"timb12957"**

---
---
**Ashley M. Kirchner [Norym]** *September 18, 2017 03:41*

Not sure if it's any better other than perhaps quality, but [ocoochhardwoods.com](http://ocoochhardwoods.com) sells 8x24" Basswood for $6. Of course you'll have to add the shipping cost to that. I tend to buy the exact amount for the shipping bracket, so in this case, 5 sheets is $30 + $13 s/h = $43 ... or $8.60 per sheet.

[ocoochhardwoods.com - Ocooch Hardwoods - Supplier of Thin Wood for Scroll Sawing, Carving Blocks, Intarsia wood, Plywood for scroll sawing, and more.](http://ocoochhardwoods.com)


---
**Jim Hatch** *September 18, 2017 13:25*

Instead of basswood, I use 12x12x1/8 sheets of Baltic Birch from Amazon in packs - I thin my last order was for 48 and only a couple of dollars each including shipping.




---
**Ashley M. Kirchner [Norym]** *September 18, 2017 15:08*

Same here, I use Baltic Birch from Ocooch. $2 per 12x24 sheet, cut into three 8x12 pieces. 15 sheets is the max for the lowest shipping bracket. Same calculation, $30 + $13 s/h = $43, or  $2.86 per full sheet. Or 95 cents per 8x12 section. 


---
**Cody Ludlum** *September 18, 2017 20:54*

I purchased a 5ft x 5ft sheet of 1/8 baltic birch from local wood supplier / lumber yard. I had them cut it in half, then I cut into 12x8 sheets. it made a lot of sheets, I think it it is under 50 cents a sheet. the 5ft x 5ft sheet was 13$




---
**timb12957** *September 19, 2017 02:36*

I checked Ocooch, they do not even list Baltic Birch. As well I checked Amazon. What I saw there was Baltic Birch Plywood. In my experience I have difficulty cutting most plywood. That is why I have settled in on using 1/8 solid Basswood. I will try a local lumber yard for something suitable. Thanks for the input.


---
**Jim Hatch** *September 19, 2017 02:46*

When people talk about Baltic Birch, they're generally talking about Baltic Birch Plywood. There is a lot of difference between BB ply and almost any other plywood you buy. It is a defined & standardized product. There are very specific rules about the quality of the face plus, the makeup of any inner plies, the glues used, knots (none) and patches.



Laser operators like BB because of this definition. It means it's a very reliable and consistent product. There's no mystery wood or crazy glues in the middle. No solid hard to cut knots or embedded voids.



If they plywood you've cut was from a big box it isn't in the same league. 



Get a sheet from Woodcraft (stores all over). Full sheets are 60 in by 60 inch. You're not finding it in the local wood stack - birch plywood is not the same. Birch ply only has to have face plus of birch and can have knots and voids and anything Weyerhauser wants to use as the inner plies. If it doesn't say Baltic Birch or if it's in standard 4x8 sheets it's not BB. (There are some companies making limited runs of large sheet BB plywood but it's extremely uncommon and expensive.)


---
**Ashley M. Kirchner [Norym]** *September 19, 2017 05:06*

**+timb12957**, it's a ply, not scroll saw wood. You were looking in the wrong section. As **+Jim Hatch** pointed out, Baltic Birch ply is rather different from regular ply. I used to buy it from Woodcraft as well as Woodworkers Source and neither of them come close to the quality of Ocooch's Baltic Birch.


---
**timb12957** *September 20, 2017 01:00*

Ah, so that would make a great difference. Thanks for the info!


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/P59aTdhnBXU) &mdash; content and formatting may not be reliable*
