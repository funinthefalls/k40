---
layout: post
title: "Pile of Natural Vegetable Tanned Kangaroo Hide, chopped up into rough pieces ready to be lasered for my big project"
date: October 02, 2016 12:15
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Pile of Natural Vegetable Tanned Kangaroo Hide, chopped up into rough pieces ready to be lasered for my big project.



Lasered a whole bundle of A3 paper, cut down slightly to fit in the K40, to make the "template" pieces to allow me to make sure I can fit it all on one hide.



Next step, open #LaserWeb3 & get cutting all the individual pieces.



Keep your eyes peeled over the coming days for my final product :)

![images/f9c5d7df88b03dde6a73c9e992b69fd8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9c5d7df88b03dde6a73c9e992b69fd8.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Alex Krause** *October 02, 2016 12:21*

I need me some veg tan


---
**Ariel Yahni (UniKpty)** *October 02, 2016 12:58*

ohhh im going to love this


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 02, 2016 13:10*

I knew you would both be interested in this one. **+Alex Krause** check Tandy Leather for veg tan (should be reasonable postage within USA).


---
**Ulf Stahmer** *October 03, 2016 01:10*

Nice work **+Yuusuf Sallahuddin**! Good luck in the competition!  You've got my vote!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 03, 2016 01:54*

**+Ulf Stahmer** Thanks heaps Ulf.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/41Hm49JEyq2) &mdash; content and formatting may not be reliable*
