---
layout: post
title: "Has anyone modified the enclosure to fit larger materials on the cutting bed?"
date: September 15, 2016 20:08
category: "Modification"
author: "Dave Dick"
---
Has anyone modified the enclosure to fit larger materials on the cutting bed?





**"Dave Dick"**

---
---
**Ariel Yahni (UniKpty)** *September 16, 2016 00:18*

You mean have a larger cutting bed? I'm working on that but if you ask about fitting bigger parts only the removing the side separator and the back metal smoke outlet helps


---
**Dave Dick** *September 16, 2016 03:50*

I'd like to engrave parts that are 22" long. I'm thinking of removing the left side panel and lengthening the case to fit the larger piece, engraving half of the piece, then turn the piece around and engrave the other half... I haven't seen a clear photo of the inside, so I'm not sure if this will interfere with the travel of the laser head


---
**Joe Alexander** *September 16, 2016 05:45*

You could always cut a slot in the front to allow for long pieces to be engraved, albeit not all at once.


---
*Imported from [Google+](https://plus.google.com/102804551257359808686/posts/faqgCWA8BaR) &mdash; content and formatting may not be reliable*
