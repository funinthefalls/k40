---
layout: post
title: "Did someone had a bad experience buying a k40 in china ?"
date: March 20, 2016 18:44
category: "Discussion"
author: "Lo\u00efc Verseau"
---
Did someone had a bad experience buying a k40 in china ? Looking for some stories to prevent a bad buy... 





**"Lo\u00efc Verseau"**

---
---
**HP Persson** *March 20, 2016 20:25*

I ordered one in november, it got stuck at customs here into Sweden and was sent back to the seller.

Took me 2 months before i got a refund, they tried to put the shipping charges on me and blaim that my phone didnt work.

So yes, there are bad people out there.



You can sort out the worst ones by not going with them who needs 7 days to ship anything for you, they are drop shippers.

If they have the units, they should be able to ship it out quickly too :)

3-5 days is normal, and also check they have sold the K40´s earlier to be sure they know what they sell, and not just dropshipping crap.

Today the stars doesnt mean anything, rather check how many they sold. A lot of them are selling 50 cent ear rings to boost the ratings, and they suck on the expensive stuff they sell.



Ordered one now in feb., arrived on time and was pretty much aligned and in good condition. Ordering a new one soon for my other location, going with the same seller.



I could go on about this for years :)

Alot to watch out for.


---
**Loïc Verseau** *March 20, 2016 20:37*

How do you get the refund ? Had to fight with paypal or anything ?


---
**HP Persson** *March 20, 2016 20:40*

I bought trough Aliexpress, they have a guarantee that i can request refund partially or completely.

And i could show the delivery was never made so there was nothing to talk about, they refunded me after i escalated it to a dispute.



Seller wanted me to pay $300 for shipping they didnt deliver to me and refund only $200, didnt work :)


---
**Stephane Buisson** *March 20, 2016 23:05*

Local Ebay +Paypal protection is the best way to avoid import taxe and VAT surprise.


---
**Loïc Verseau** *March 20, 2016 23:14*

Do you have some experience with someone who received crap instead of the lasercut ?




---
**Stephane Buisson** *March 20, 2016 23:25*

if you mean wrong item, or non conform item. not much report here. (1 under rated tube 30 fo 40W), Paypal protection for that.

Main issue is transport damage. 


---
**Loïc Verseau** *March 20, 2016 23:26*

and what paypal did ?


---
**Stephane Buisson** *March 20, 2016 23:48*

in that case the seller offer to send a new 40w certified tube for free.

[https://plus.google.com/109462644991158070594/posts/hAfMBR5eofd](https://plus.google.com/109462644991158070594/posts/hAfMBR5eofd)

Paypal offer a Buyer protection plan for damage or non conformity and deal with the seller.

[http://pages.ebay.co.uk/ebay-money-back-guarantee/index.html](http://pages.ebay.co.uk/ebay-money-back-guarantee/index.html)


---
**Loïc Verseau** *March 21, 2016 06:49*

So, here is my story, I bought a 6040 lasercutter 60W to a chinese seller in january. Was pretty cool with me, offers me lens and mirrors (at least for the prise it was not a big deal but..).

Lasercutter arrived well. +440€ tax fee. (But that's normal things i suppose. 

Thing is, the lens and mirrors he offers were not there. (hummm...) AND lasercutter is definitly not 60W (laserlenght is 800mm long. I suppose it's a 50W. 

I paid by paypal, so I was thinking everything will goes right. Sight.... Forget it.

I send few email t the seller (who respond me that i'm an idoit). Then I opened a claim to paypal. They lock the money and ask for an expertise of the lasercutter. So I do. And with this expertise they told me that my claim is good. I will be refunded because the lasercutter is not what i ordered. BUT and here it hurs a lot ! 

I have to send it back to my own way at my cost ! In other way I have to pay half the price in shipping cost to be refunded... 

Don't know what to do know. Asking for help.

;) 


---
**Stephane Buisson** *March 21, 2016 09:10*

don't give a mark to the transaction before checking (missing parts). deal a new certified tube with the seller (cheaper than return) and be aware the figure is a peak rating. Buy Locally (at least by continent EU, US), return will be cheaper.


---
**Loïc Verseau** *March 21, 2016 17:10*

No marks anyway, I paid directly by paypal.. Will sell the lasercut I think


---
*Imported from [Google+](https://plus.google.com/102787032729416934676/posts/A8P8URdmVqT) &mdash; content and formatting may not be reliable*
