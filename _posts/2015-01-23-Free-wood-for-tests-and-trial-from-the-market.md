---
layout: post
title: "Free wood for tests and trial from the market ;-))"
date: January 23, 2015 15:28
category: "Materials and settings"
author: "Stephane Buisson"
---
Free wood for tests and trial from the market ;-))

![images/9b88dcfe79394ea300d736f6e7325c11.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b88dcfe79394ea300d736f6e7325c11.jpeg)



**"Stephane Buisson"**

---
---
**Anton Fosselius** *January 23, 2015 17:26*

Haha I have also used those ;) ﻿is it a makibox in the background?


---
**Stephane Buisson** *January 23, 2015 21:02*

yep, my makibox is nearly a year old now.


---
**Alessandro Milano** *January 26, 2015 08:29*

Hope to see your test result with some engraving data (power, speed...)

Enjoy !!!


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/57ozsyxMQK9) &mdash; content and formatting may not be reliable*
