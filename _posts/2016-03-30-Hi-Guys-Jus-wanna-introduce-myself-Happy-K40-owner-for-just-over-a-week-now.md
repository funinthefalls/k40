---
layout: post
title: "Hi Guys, Jus wanna introduce myself (Happy K40 owner for just over a week now)"
date: March 30, 2016 14:23
category: "Modification"
author: "David Gilbert"
---
Hi Guys, Jus wanna introduce myself (Happy K40 owner for just over a week now). Wanna also share my first planned mod/upgrade to my machine, a new control panel with 3.6" TFT touch screen to display water and room temps along with flow rate meter. Also going to swap out the push button laser activation switch for a key as an additional safety measure. Would appreciate any feedback!



![images/f89d6462b1307600f50effead7756714.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f89d6462b1307600f50effead7756714.jpeg)
![images/4e7daeaa9a89e304d63950de58f15483.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e7daeaa9a89e304d63950de58f15483.jpeg)

**"David Gilbert"**

---
---
**Alex Krause** *March 30, 2016 14:36*

Awesome design Mate!


---
**MNO** *March 30, 2016 15:46*

looks cool. What type of LCD that is and what sort of flow meter You are using. I'm willing to build something less colorful. Just arduino + 2 -3 temp sensors + 2 contact switches + flow meter + relay + 1602 lcd. I'm waiting for parts still to come from Farnell.



Did someone tough already to exchange that potentiometer with digital one and pair it with digital ammeter. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 15:53*

Welcome to the group. First mod looks great.


---
**MNO** *March 30, 2016 16:04*

99l/min is that showing right. i still waiting for my laser so i don't know what that pomp is capable of. Can someone check what stock pomp is capable... i was thinking no more than 10-20 l/min max


---
**Stephane Buisson** *March 30, 2016 16:54*

Welcome with us **+David Gilbert** 

nice job it's a good idea to design the control panel first.

Maybe you can add more switches for air assist, exhaust and pump. like the idea of the key (fablab, makerspace?? or little brother?).


---
**Will Travis** *March 30, 2016 17:55*

Welcome David, this looks great.   I would love to learn more about your project.   Are you running the display with a Raspberry Pi?  What are you planning to use touch for on your screen?


---
**David Gilbert** *March 30, 2016 23:23*

Hi Guys,



All is planned to be powered by an arduino uno. The screen is a 3.6" tft module available on ebay.



At present the display is not hooked up to sensors so the data you see on the screen if from my GUI design phase and may very well change. 



I'll be working on it over the next few weeks in between jobs and when i get the spare time. 



Once done, I plan to share a complete set of plans, arduino code and build list for others too.


---
**Will Travis** *March 31, 2016 00:55*

Thanks David, thanks for sharing your work.  I will be an avid follower.  


---
*Imported from [Google+](https://plus.google.com/109913113329313347454/posts/38nqf4B3uyy) &mdash; content and formatting may not be reliable*
