---
layout: post
title: "Finished my Bluetooth water temp sensor. Finally can get rid of the gauge and watch the temps on screen and also works great"
date: November 07, 2017 23:53
category: "Modification"
author: "A \u201calexxxhp\u201d P"
---
Finished my Bluetooth water temp sensor. Finally can get rid of the gauge and watch the temps on screen and also works great.

![images/92162a66d1a2ec022fe2484ec02cd5bf.png](https://gitlab.com/funinthefalls/k40/raw/master/images/92162a66d1a2ec022fe2484ec02cd5bf.png)



**"A \u201calexxxhp\u201d P"**

---
---
**HalfNormal** *November 12, 2017 15:13*

**+A P** Are you going to share any info on the build?


---
*Imported from [Google+](https://plus.google.com/114764932222807234342/posts/HYoVQjZfgWX) &mdash; content and formatting may not be reliable*
