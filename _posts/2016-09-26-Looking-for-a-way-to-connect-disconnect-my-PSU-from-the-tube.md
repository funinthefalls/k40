---
layout: post
title: "Looking for a way to connect/disconnect my PSU from the tube"
date: September 26, 2016 18:14
category: "Modification"
author: "Ariel Yahni (UniKpty)"
---
Looking for a way to connect/disconnect my PSU from the tube. Is the following item what I'm looking for?  Does the quality makes a difference?  I don't want a $5 item to kill me  [http://www.ebay.com/itm/322204363935](http://www.ebay.com/itm/322204363935)





**"Ariel Yahni (UniKpty)"**

---
---
**Anthony Bolgar** *September 26, 2016 18:39*

I used one in my Redsail LE400 refit. They work pretty good.


---
**Jon Bruno** *September 27, 2016 17:36*

Yep, you're just trying to keep the pixies in.. That coupler works pretty well with that.. I don't think they have thumbs.


---
**Phillip Conroy** *September 28, 2016 09:11*

I was an electronics tech with the Australian Air Force for 10 years so i know a few connectors -it looks like bannana plugs as used in multi meters ,so is a good conector and will handle being pulled apart quit a few times.insulation looks good as well 


---
**Eric Rihm** *October 01, 2016 21:38*

This is a great find thanks, I was struggling with finding connectors that wouldn't arc and were insulated enough.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/PgrTrUw4zGd) &mdash; content and formatting may not be reliable*
