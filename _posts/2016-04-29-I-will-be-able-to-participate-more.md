---
layout: post
title: ":) I will be able to participate more"
date: April 29, 2016 06:09
category: "Discussion"
author: "Alex Krause"
---
:) I will be able to participate more. 



<b>Originally shared by Alex Krause</b>



K40 purchased tonight :)

![images/09bcd01afd1d0d83c9bf4c1758bd4640.png](https://gitlab.com/funinthefalls/k40/raw/master/images/09bcd01afd1d0d83c9bf4c1758bd4640.png)



**"Alex Krause"**

---
---
**I Laser** *April 29, 2016 08:57*

Welcome to the dark side. 



Queue doctor evil laugh:

Muuuhahahahahahaaa!


---
**Scott Marshall** *April 29, 2016 10:21*

Congrats. Good price too.


---
**Richard** *April 29, 2016 15:32*

Since it's a laser beam, which is concentrated light, wouldn't it be welcome to the light side? ;)


---
**Jarrid Kerns** *April 29, 2016 17:16*

Nope, dark. If you were in the light you may go blind or get burned.


---
**I Laser** *April 30, 2016 03:11*

**+Richard Beline** Ha, definitely the dark side when you add up the cost and wasted time!


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/N11nJabzT99) &mdash; content and formatting may not be reliable*
