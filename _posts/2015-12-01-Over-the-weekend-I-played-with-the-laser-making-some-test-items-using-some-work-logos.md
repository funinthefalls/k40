---
layout: post
title: "Over the weekend I played with the laser making some test items using some work logos...."
date: December 01, 2015 23:24
category: "Hardware and Laser settings"
author: "Todd Miller"
---
Over the weekend I played with the laser making

some test items using some work logos....



I discovered that sometimes my ellipse's would not

connect to where it started, like a step off.  At first I

thought it was only doing it on a 5 mm cut, but it

eventually did it @ 5, 10, 20 & 30 mm once in a while.



Would this be belt slipping  ?



![images/1fd33c5d3132d95f3301fac96beb2eb9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1fd33c5d3132d95f3301fac96beb2eb9.jpeg)
![images/43098a96d3ebe149926e87204f1e8b4a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43098a96d3ebe149926e87204f1e8b4a.jpeg)

**"Todd Miller"**

---
---
**Stephane Buisson** *December 01, 2015 23:38*

yes belts or motor skipping steps.

I notice a 3rd possible cause, the metal air vent (exhaust) is very close to the shaft of the  X motor and could rub on it.


---
**Sean Cherven** *December 01, 2015 23:38*

That looks like a belt or lashback issue.  The motors could be skipping steps... Try doing a slower speed and see if that helps. Check for any binding on the gantry.


---
**Todd Miller** *December 02, 2015 02:08*

It may be a vector issue.  I just did a few

runs drawing 30 or so random circle/ellipse

on construction paper all over the work space.



At this time, no issues at speeds from 5mm-30mm.  Maybe it's an issue with my original drawing.





I can cut 3mm hobby plywood from 4-11ma

at speeds from 10-30mm.


---
**Nick Williams** *December 02, 2015 03:23*

If this is material specific it may have to do with flatness of the material, I would be the first to say odds are that it is one of the previous items. Just another possibility to check of your list?


---
**Tony Schelts** *December 02, 2015 09:52*

Did you do these images in one go or in different stages?

Looks good


---
**Todd Miller** *December 02, 2015 16:08*

Three separate files; etch the front with one file, etch the other side with another file and the final cut with another

file.  I don't know how to do it in one file with layers..yet. 


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/K6bryHvwgvj) &mdash; content and formatting may not be reliable*
