---
layout: post
title: "I want to build a rotary for my stock k40"
date: November 13, 2016 15:31
category: "Modification"
author: "giavonni palombo"
---
I want to build a rotary  for my stock k40. What is the step angle of the stock Y axis stepper? What is the max amp motor that can be wired directly to the stock board? Thank you in advance. 





**"giavonni palombo"**

---
---
**Stephane Buisson** *November 13, 2016 16:30*

motor vary from K40 built models, about max 0.5 Amp.


---
**Paul de Groot** *November 13, 2016 21:46*

I found that I could max source 1A to both steppers continuous. So 0.5A should work (Nema17) for a rotary device.


---
**giavonni palombo** *November 14, 2016 01:44*

What about step angle or steps per revolution ?


---
**Paul de Groot** *November 14, 2016 01:57*

**+giavonni palombo** most stepper have just 200 steps per revolution.  You use the electronics to make micro steps like 1/16 or even 1/128 with the latest step sticks. This effectively multiplies the resolution of the position. Pretty standard. 


---
**Stephane Buisson** *November 14, 2016 06:47*

1 amp is about what the PSU could deliver, take care when you choose your stepper max 0.5A. and don't forgot to unplug one of the 2 stepper (replace).


---
**HalfNormal** *November 14, 2016 12:46*

**+giavonni palombo** I measured 200 steps per revolution when playing with an arduino controller.


---
**giavonni palombo** *November 14, 2016 14:38*

Im thinking something like this but only 1 stepper a small nema 17 or a nema 14. The person explains that if the rollers are the same size as the dive gear on the k40s Y then there is no need to adjust mico stepping. Make sense.  This is what I want, not to have to adjust mico stepping every time I want to use the rotary. So I should be able to get a stepper and unplug the Y then plug in the rotory and do nothing else.  Correct? Im just trying to see if the Chinese didn't use some unique stepper that will take more that a simple unplug the Y and plug in the rotory. 

![images/5449b3bf4b44691074adae3175a7e69d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5449b3bf4b44691074adae3175a7e69d.jpeg)


---
**ben ball** *November 28, 2016 00:44*

Stupid question...Will that fit under the laser of a stock k40?


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/KLfe86csZUi) &mdash; content and formatting may not be reliable*
