---
layout: post
title: "So I had a very odd experience with my laser this week all of the sudden the first fourth of my bed went out of focus"
date: June 28, 2016 02:02
category: "Discussion"
author: "3D Laser"
---
So I had a very odd experience with my laser this week all of the sudden the first fourth of my bed went out of focus.  Not for sure what's going on I cleaned all the mirrors and still am having trouble getting it to cut in one pass 





**"3D Laser"**

---
---
**Ariel Yahni (UniKpty)** *June 28, 2016 02:08*

Did you have to remove the focus lense? If so try rotating the holder if nothing else changed. A little can  come a long way


---
**Ned Hill** *June 28, 2016 02:15*

Check mirror alignment?


---
**3D Laser** *June 28, 2016 02:19*

When it first started acting up I took it all apart and cleaned it I did notice some of the gold covering has started to come off my SI mirror I almost have it aligned perfectly again it is hitting lower on the front end where it's not cutting as well looks like I have some thinking to do 


---
**Phillip Conroy** *June 28, 2016 06:19*

Was it a full moon this week?temp changes ,can maks a diffrence to alignment esp if you are just in the circle target,even running machine at high speeds can also  throw alignment out


---
**Ned Hill** *June 29, 2016 14:51*

Yesterday I noticed that my machine wasn't cutting as well as it has been.  Found that the top part of my lens had a build up of some kind of residue.  It's probably from all the ply I have been cutting which generates a lot of smoke and vaporized glue.  Some of which was apparently settling down on top  of the lens.  Cleaned it off and all back to normal.


---
**3D Laser** *June 30, 2016 03:03*

It seems my focal length has shifted a bit again very odd experience 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/JBo5ayxywBC) &mdash; content and formatting may not be reliable*
