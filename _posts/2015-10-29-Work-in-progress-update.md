---
layout: post
title: "Work in progress update !"
date: October 29, 2015 16:57
category: "Smoothieboard Modification"
author: "Stephane Buisson"
---
Work in progress update !





**"Stephane Buisson"**

---
---
**Anton Fosselius** *October 29, 2015 17:10*

Nice. Will go with some smothieware compatible chinese board in the future..


---
**Stephane Buisson** *October 29, 2015 17:13*

**+Anton Fosselius** Attention, at least for the moment, Visicut need an ethernet connection to communicate with a board (no usb only). but you could go with Fusion 360+Smoothie(usb).


---
**Joey Fitzpatrick** *October 29, 2015 18:34*

Nice Job!!  I will be watching your progress closely.  Are you using the 4X or  5X board?


---
**Stephane Buisson** *October 29, 2015 18:51*

**+Joey Fitzpatrick** 4XC is plenty enough ;-))


---
**David Cook** *October 29, 2015 19:36*

Nice !   I really wish Visicut had direct USB  so I could use it with my X5 board lol.   


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/2HMmLYv4TGL) &mdash; content and formatting may not be reliable*
