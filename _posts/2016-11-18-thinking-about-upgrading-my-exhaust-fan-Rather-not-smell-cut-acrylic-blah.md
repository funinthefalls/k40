---
layout: post
title: "thinking about upgrading my exhaust fan. Rather not smell cut acrylic (blah)"
date: November 18, 2016 15:16
category: "Discussion"
author: "Bill Keeter"
---
thinking about upgrading my exhaust fan. Rather not smell cut acrylic (blah). Which one are you guys using? I'm looking for something I can run in my garage in the evening that won't wake the kids. 







**"Bill Keeter"**

---
---
**Don Kleinschnitz Jr.** *November 18, 2016 15:27*

[http://donsthings.blogspot.com/2016/06/k40-air-systems.html](http://donsthings.blogspot.com/2016/06/k40-air-systems.html)



[donsthings.blogspot.com - K40 Air Systems](http://donsthings.blogspot.com/2016/06/k40-air-systems.html)


---
**Bill Keeter** *November 18, 2016 15:32*

**+Don Kleinschnitz** yeah, I was thinking something like that. Wasn't sure if a 4 or 6" inlet mattered. How loud is it? Low hum or sounds like running a vacuum cleaner?


---
**Don Kleinschnitz Jr.** *November 18, 2016 15:39*

Much quieter than a vacuum cleaner. My wife has not complained, that's my goal :)


---
**Niels Sorensen** *November 18, 2016 15:48*

I have the 4" version. It does just over 200 CFM and provides good ventilation.  It's very quiet and came with a variable speed controller.   I also have a 6" version installed for one of the A/C ducts.  It's also quiet but I had to get a separate variable speed controller.


---
**Bill Keeter** *November 18, 2016 15:56*

**+N Sorensen**  do you have a link? The one I just saw on Amazon doesn't have the speed controller.


---
**Niels Sorensen** *November 18, 2016 17:08*

[https://www.amazon.com/gp/product/B01CTM0H6I/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01CTM0H6I/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1)


---
**Niels Sorensen** *November 18, 2016 17:12*

It looks like both the 4" and 6" have the variable speed now


---
**Kelly S** *November 19, 2016 15:01*

Just got this one in the mail yesterday, it works really well.

![images/7f34b13824c585fbbb1c8a5701da4cce.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7f34b13824c585fbbb1c8a5701da4cce.png)


---
**Jim Bilodeau** *November 20, 2016 23:49*

I have this 3" blower and designed/printed a duct for it. You would need a 12VDC power supply for it. Seems to work well!



[https://www.amazon.com/gp/product/B001O0DE9E/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B001O0DE9E/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)



[http://www.thingiverse.com/thing:1874500](http://www.thingiverse.com/thing:1874500)





[amazon.com - Amazon.com : Attwood Blower H20 Resist (White, 3-Inch) : Boat Blowers : Sports & Outdoors](https://www.amazon.com/gp/product/B001O0DE9E/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/B8eq8SNR54g) &mdash; content and formatting may not be reliable*
