---
layout: post
title: "Some more upgrades for my K40 Now that MKS SBASE is working good, I decided its time to get some mesh material"
date: July 26, 2016 19:55
category: "Modification"
author: "David Cook"
---
Some more upgrades for my K40



Now that MKS SBASE is working good,  I decided its time to get some mesh material.  this is 0.008" thick aluminum mesh by 1/2" tall.  was cheap on ebay.   I later found out the seller would custom cut a size.  so I will be going back for a larger piece.



also 3d printed some cones to support the mesh at the proper focal point for cutting 1/8" material



also relocated my optical Y-Endstop so that my machine homes to 0,0 bottom left corner  ( I understand I could have set this to happen in the config file,  but the hardware solution seemed easy and legit)



Bought the air assist head and the improved 18mm lens from LightObjects.  as stated before this head is sharp lol  no I mean really sharp.   they did not deburr it at all lol.  no prob for the low price.



now I need to re-align my optics because I am getting heavy ghosting on the far right side of the bed.  Crap I may have cracked my new lens !  I hope it's just out of alignment and reflecting off the bottom of the air cone.. I'll check it tonight.



this is just a random kind of post, I want to update you guys on my build so far.



driving the steppers 1/32 really quiets down the machine and makes it seem a lot smoother.



Link to the MESH on EBAY  [http://www.ebay.com/itm/262374763640?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/262374763640?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



[https://goo.gl/photos/YP89gHkRMA9EtXv1A](https://goo.gl/photos/YP89gHkRMA9EtXv1A)





**"David Cook"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 26, 2016 20:41*

Ebay link please?


---
**greg greene** *July 26, 2016 20:49*

Those cones look great !  Sadly I do not have a 3D printer yet


---
**David Cook** *July 26, 2016 20:52*

**+Ray Kholodovsky** [http://www.ebay.com/itm/262374763640?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/262374763640?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**David Cook** *July 26, 2016 20:54*

**+greg greene** I'm not sure if the cones are the right solution yet.  I shaped them like a pyramid so they are stable,  I also had to add metal tape to the tops because PLA smells nasty when hit by a laser beam lol


---
**Jim Hatch** *July 26, 2016 21:20*

**+greg greene** You can buy them at paint & craft stores - they're called paint standoffs.


---
**greg greene** *July 26, 2016 21:23*

Yes = of course !  I have some of those - thanks !


---
**Jim Hatch** *July 26, 2016 21:44*

Just saved you spending $1500 on a 3D printer :-)


---
**greg greene** *July 26, 2016 21:45*

But, but I WANT a 3d printer !!:)


---
**Jim Hatch** *July 26, 2016 21:48*

You can have my Makerbot Replicator 2X - haven't used it in the better part of a year. It just sits there with the last failed print sitting on its bed looking at me. 3D printers are a dark art for even tinkerers like us. My success to fail ratio is well on the fail side - everything has to be just right to get a good print (for large things anyway - things that are non-trivial). I don't have the patience anymore.


---
**David Cook** *July 26, 2016 22:50*

**+Jim Hatch** my printer was under$400


---
**greg greene** *July 27, 2016 00:07*

Send me the details of the machine and how much you want for the remains !


---
**Jim Hatch** *July 27, 2016 00:32*

**+greg greene**​ it's a standard Makerbot Replicator 2X. Has a heated bed and dual heads so you can do two colors of filament at the same time. Maybe 10lbs of  different filament left on mostly unopened spools of different colors. Replaced the laptop tape on the heated bed with a Teflon based cover. I've used it for ABS but it's set for PLA and other materials too. That's why I got it - wanted versatility. Biggest problem selling it is shipping - it's a big box. I used to do prosthetic printing for eNable but haven't been able to dedicate the time due to work (it can take 30 hrs to print all the parts for an adult prosthetic hand).


---
**greg greene** *July 27, 2016 00:56*

I ship heavy ham radio stuff all the time so I know how expensive it can get, might be a good machine for me to play with - I have no commercial use for it.


---
**HalfNormal** *July 27, 2016 01:24*

**+greg greene** I have this one. Bought it over a year ago and have had great luck. It is $224 delivered. Has a lot of online support too. [https://www.3dprintersonlinestore.com/full-acrylic-reprap-prusa-i3-kit](https://www.3dprintersonlinestore.com/full-acrylic-reprap-prusa-i3-kit)


---
**greg greene** *July 27, 2016 01:30*

That looks interesting alright - how reliable have you found it?


---
**HalfNormal** *July 27, 2016 01:36*

Very reliable. Some great improvements on [https://www.thingiverse.com](https://www.thingiverse.com) with a support forum to boot.


---
**greg greene** *July 27, 2016 01:43*

Awesome !


---
**David Cook** *July 28, 2016 19:41*

**+greg greene**  lol one is not "hokey"  :-)


---
**greg greene** *July 28, 2016 19:45*

Ah !


---
**Arestotle Thapa** *August 21, 2016 03:40*

**+David Cook** are you using the original optical end stops with smoothie or physical switches?


---
**David Cook** *August 21, 2016 06:56*

**+Arestotle Thapa** I am using the original optical end stop. But I relocated the Y end stop to trigger on the front edge of the extrusion so that home is bottom left 


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/gNMgydSHVff) &mdash; content and formatting may not be reliable*
