---
layout: post
title: "Hi Guys, Please suggest some idea to fix my new laser machine ."
date: October 09, 2015 18:13
category: "Discussion"
author: "Raja Rajan"
---
Hi Guys,



Please suggest some idea to fix my new laser machine .



My laser machine is not engraving or cutting anything and the the power in amp meter is still in ZERO but the laser header is moving and i don't see any laser hitting in my acrylic sheet.



The seller is saying he can refund $25 and No returns,it's been two weeks now ,Holiday is season is close by!



Is there any way i can trouble shoot which part needs to be replaced? or any other solution ?



There are the steps i did? Am i missing something ?



1) Water pump is connected and water is flowing .

2) Test switch is working and i can see some spots on my acrlyic when i hit Test Laser .

3)Optical path verified.

4)Updated the device id and Model no in initialization device part.



![images/7eb33f3e25ce71e7c569d962133c028a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7eb33f3e25ce71e7c569d962133c028a.jpeg)
![images/d3d683b09b706ebf2aabc4e640309ea2.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/d3d683b09b706ebf2aabc4e640309ea2.gif)

**"Raja Rajan"**

---
---
**David Wakely** *October 09, 2015 18:34*

You need to look on your power supply inside the machine and check that the K+ cable isn't damaged or broken. If you have a multi meter check continuity between this and the corresponding cable on the main board. This is more than likely where your issue lies


---
**Ashley M. Kirchner [Norym]** *October 09, 2015 18:55*

The laser SWITCH needs to be depressed DOWN for the laser to fire. It's an ON-OFF switch. In the video is appears you pressed it twice, in essence you turned the laser on and then off again. When you're cutting/engraving something you need to depress it once to turn the laser on (the button is in the depressed or down state) and when you're done you can turn the laser off by depressing it again (and the button pops back up.) The laser TEST switch is a momentary switch that you use to simply test the laser, it does not keep it on once you let go. That's done that way on purpose.


---
**Raja Rajan** *October 09, 2015 18:58*

Hi Ashley* & Already Gone ,Ya i understand that ,i have pressed the laser switch once but still noting is working!Really thanks for your reply!


---
**Raja Rajan** *October 09, 2015 18:59*

Thanks David! Let me take a look at that and will update you!


---
**Ashley M. Kirchner [Norym]** *October 09, 2015 19:39*

If the laser is firing when you do a test, I don't think the wire is the problem, however it's always a good idea to check all connections. Specially the wires that connect to the laser tube itself. Make sure they have a good contact, a couple of tight wraps around the terminal.


---
**Joey Fitzpatrick** *October 09, 2015 19:56*

The test Fire button is hooked directly to the power supply.  It bypasses the mainboard.  There is also a test fire button on the power supply circuit board(a small button)  these 2 buttons do the same thing.  when the laser fires with either test button, the power supply fires the laser at 100% duty cycle. The mainboard sends a fire signal (PWM signal) on the TTL (active low) wire.  This should be a green wire going to the K+ terminal on the Power Supply(some power supply's have it labeled with a L)  You should check this wire to see if the mainboard is actually sending the fire signal.  It will show 5v when it is not firing and it should drop almost to ground when it is(will be less than 1 volt)  If you can figure out if the signal is there or not, you can narrow down your problem


---
**Andrew Cilia** *October 10, 2015 04:05*

I did not see you trying to adjust the laser power knob. Maybe the power is set to zero


---
**Martin Byford** *November 27, 2015 15:08*

Hey guys im having the exact same issue!

Did any one come up with a solution??


---
*Imported from [Google+](https://plus.google.com/110717916181924497071/posts/jGS2XEasZac) &mdash; content and formatting may not be reliable*
