---
layout: post
title: "So this is what i found at the end of the tube exit would this cause my problems thanks to all for the help"
date: April 20, 2016 22:06
category: "Discussion"
author: "Dennis Fuente"
---
So this is what i found at the end of the tube exit would this cause my problems thanks to all for the help 

![images/4e6beea444778ffbe873098c9a359e1a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e6beea444778ffbe873098c9a359e1a.jpeg)



**"Dennis Fuente"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 20, 2016 22:35*

I am unsure what that is, but it does look to be a crack or partial crack. However, as it is on the side of the tube shell, I'd imagine that it isn't in the beam path. I'd be more inclined to check the orange thing on the right of the image for cracks/dirt/chips (it looks to be a lens). I am going to check my tube to get a better look at what that end looks like. Also, I notice what looks to be air bubbles below the silver thing on the left of the image?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 20, 2016 22:41*

Yeah, it's definitely a lens at that end. Hard to get a look at it without totally removing mirror 1, but might be possible to hit it with a blast of air (from your compressor/airpump to maybe dislodge any debris that is on it which could be causing the split).


---
**Dennis Fuente** *April 20, 2016 23:01*

Yes it has air bubbles due to the machine being turned off i used a mirror in front of the tube to check for debree nothing there can't find any other reason for it not cutting through 3 MM ply i had the machine almost to full power and it would not cut through it, and the beam is split right from the tube what else could it be and a new tube is $160.00 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 01:46*

**+Dennis Fuente** I'm about as in the dark as you regarding what could be causing it other than some debris. But you mention there is none, so it must be something internal in the tube causing it. I wonder whether you've had it for short enough period to get a replacement tube from the seller for free?


---
**Alex Krause** *April 21, 2016 02:32*

Is that a crack or is that debris inside the tube?


---
**Phillip Conroy** *April 21, 2016 06:01*

you are all wrong it is a thin metal strip going from the terminal to the metal collar.this is normal,and is needed for the tube to work


---
**Phillip Conroy** *April 21, 2016 06:15*

Make sure all mirrors are clean shine a torch on them at diffrent angles entill you can see it is clean-also check focal lens ,how wide is the cut on top of your work peace?.if wider than 1mm than focal lenes is problem-<s>----</s>.My laser tube is covered in mdf dust and i can not even see into it,and is still working ,thinking of drilling a small hole so that i can clean the laser tubes output window as very hard to do in machine as you can not see the out put window unless you use a hand mirror

l spent 6 months doing double cutts to get through 3mm mdf before i changed my focal lens and now it cuts it like butter on half the power [7ma and 7.5 m/sec].Remember the beam exits the tube at around 4mm wide and the focal lenes concentrats the beam down to 0.1mm.Focal lenes and m irrors are cheap and wear out even if you can not see the wear........


---
**Scott Thorne** *April 21, 2016 14:08*

There is no lens in the tube...only a fully and 90% partial mirrors...the 90% mirror is where the beam comes out.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 14:09*

**+Scott Thorne** So it's like a one-way mirror or something?


---
**Dennis Fuente** *April 21, 2016 16:34*

Hi guys my thanks to all for your response, Phillip what i think is a crack is in front of the metal ring, As to the mirrors they are brand new light objects i used latex gloves to install them they are bright and shinny, last night before i gave up on it for the day i heard an electrical snap somewhere i will have to give that a look today just so many thing's going on at the same time with this thing trying to sort it out it's a pain


---
**Scott Thorne** *April 21, 2016 17:56*

**+Yuusuf Sallahuddin**...it only let's a percentage of the actually power the tube creates escape.


---
**Phillip Conroy** *April 21, 2016 19:02*

Niice photoo,if you zoom in you can clearlg see it is not a crack ,it is thin metal ,an electrical connector from yhe terminal to the metal collour,send this photo to a tube seller and ask them about it


---
**Dennis Fuente** *April 21, 2016 22:48*

Hi Phillip

I did just that i sent it to the manufacure and i am waiting their reply but for today i took the laser tube out of the machine completly for a close up inspection this is what i found the connections from the PSU to the tube are a joke i removed and cleaned them throughly the next step gave me the reason the manufacture is using silicone to glue the wires on nothing will stick to the tube termanials i don't know what the material is but it will not accept soldier, silver soldier i can't get it to hot as it may fracture the glass so i used a soldiering iron no luck so i soldiered best possible.

Next step was to align the tube and mirrors spent the day doing this moved the tube forward aft moved the mirrors up down shimmed everything best i feel i could get it and the beam is still split, i did see in the rear of the tube once i had it out the back ring connector was off center and titlted to one side i still think it's the tube that is the problem.


---
**Phillip Conroy** *April 22, 2016 04:38*

Do not solder the terminals of the laser tube as you will damage the internal connections. To wire tube to power supply strip about 1 inch of insulation off the wire core ,and twist the wire around the terminal as tightly as you can.than apply heaps of silicon sealent -but do not push into the wire


---
**Dennis Fuente** *April 22, 2016 04:44*

Well what  I found  was the  wire was  filled with  silicone and had a  very  poor  connection  still can't get  single  beam 


---
**Phillip Conroy** *April 22, 2016 04:46*

Want to put money on the tube being ok,like i said before split beam can be normal for a tube ,mine is and cuts very well ie 3mm mdf like butter.the reason the tube is split is the mode the tube is operating in and if i understod  Quantum mechanics i could 

 explane it better


---
**Dennis Fuente** *April 22, 2016 04:52*

So  a split  beam is normal so what  power  do you  use  to  cut 3mm ply I have to go way up on power and  it really  scorches  the  wood  badly I have even tried using  several  passes  and still  can't get it to  cut


---
**Phillip Conroy** *April 22, 2016 06:00*

for 3mm MDF i use 8ma at 7.5 mm/sec cuts like butter-parts just fall out, suspect you have dirty/damaged  focal lens, how wide is the mark of the top of the work peace? if more than 1mm wide focal lens is your problen,.

do you have air assist as this helps cutting .


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 22, 2016 07:00*

**+Dennis Fuente** Myself, I use 10mA power @ 20-30mm/s & 2 passes. The higher speed minimises charring as it isn't on the one spot for very long. Parts fall out very easily after the 2nd pass.


---
**Dennis Fuente** *April 22, 2016 16:42*

So I gave it a try this morning @ 10 Ma and 10 MMs it still will not cut through hobby ply i have not tried a faster speed i did try 15 MMs but same condition am i chasing my tail here I would happly change out what ever would solve the problem but i don't know what that is


---
**Scott Thorne** *April 22, 2016 16:54*

**+Dennis Fuente**...are you sure the focal length is right...try cutting it at 6mm/s...at 12mA.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 22, 2016 17:15*

Yeah, as Scott mentions, focal length can be a big factor. Mine was out by 4mm at one point & it made a huge difference. Couldn't cut through anything, even with max power, slow speed (like 3mm/s) & 10+ passes. Once I realised my focal point was out & fixed it, went back to normal.


---
**Phillip Conroy** *April 22, 2016 23:31*

Focal length is one of the reasons you need to measure cut width on the top part of the cut -------------IF WIDER THAN 1MM YOU HAVE A PROBLEM-----------------ps the focal length on a stock 12mm lens is 50mm ,and the stock lens failed on my machine after 100 hours.can you start a new post and add photos of something you have cut ,add a ruler across the cut for size refrence ,photos of top and botton -also add air assist pressure if known,speed of cut and ma settings


---
**Dennis Fuente** *April 23, 2016 00:21*

Phillip i jsut got back in and saw your reply i gave things a rest for today but i just measured the top of the cut it is 0.81 MM so it's under the 1MM i do have air assist its the harbor freight aiir brush compressor with a 3d printed nozzle i made it's right at were the beam cuts i did measure the focal length and it's at 50.8 MM to the top of the material so what you think it may be the focal lens is junk if so what would you recomend thanks for the help 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 23, 2016 01:39*

**+Dennis Fuente** Quick suggestion is to take out the focal lens altogether, just for a test & see if you are still getting the split beam. You will end up with a much larger radius burn spot (something around 5mm) but should be able to tell if the beam is split.


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/AVADW4vFSzx) &mdash; content and formatting may not be reliable*
