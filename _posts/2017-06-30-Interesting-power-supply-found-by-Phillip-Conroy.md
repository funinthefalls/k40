---
layout: post
title: "Interesting power supply found by Phillip Conroy ."
date: June 30, 2017 12:25
category: "External links&#x3a; Blog, forum, etc"
author: "Don Kleinschnitz Jr."
---
#K40LPS

Interesting power supply found by **+Phillip Conroy**.

Seems to include much of the internal protection and voltage monitoring that one would expect from a modern supply.

I have my doubts that it reports the actual HV output, rather a proxy from the internal PWM controller. If so it may not identify a bad HVT?



I was surprised to find 40w units for $50, additionally the parts $ here are very impressive. I have never done business with Aliexpress as they do not accept PayPal.



[https://www.aliexpress.com/wholesale?spm=2114.01010208.8.13.WhoiAj&initiative_id=QRW_20170630051708&SearchText=co2+laser+power&productId=32619998836](https://www.aliexpress.com/wholesale?spm=2114.01010208.8.13.WhoiAj&initiative_id=QRW_20170630051708&SearchText=co2+laser+power&productId=32619998836)



<b>This is 80w version did not find a 40W version.</b>

[https://www.aliexpress.com/item/Z-Series-Intelligent-Z80-Co2-Laser-Power-Supply-80W-For-Laser-Engraving-Machine-With-LCD/32806003257.html?spm=2114.13010608.0.0.nXRrMW](https://www.aliexpress.com/item/Z-Series-Intelligent-Z80-Co2-Laser-Power-Supply-80W-For-Laser-Engraving-Machine-With-LCD/32806003257.html?spm=2114.13010608.0.0.nXRrMW)





**"Don Kleinschnitz Jr."**

---
---
**Anthony Bolgar** *June 30, 2017 12:45*

Banggood has 40W supplies for $50 with free shipping


---
**greg greene** *June 30, 2017 12:48*

Now - if they just came with a pin out and a schematic


---
**Don Kleinschnitz Jr.** *June 30, 2017 15:10*

**+greg greene** which supply are you looking for a pin out and schem for?




---
**HP Persson** *June 30, 2017 21:15*

The big PSU pinout.

If you have the manual for Ruida controller, what each pin does is easier to understand than this one :P

![images/894193580155fab461c83c1fac6ea7c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/894193580155fab461c83c1fac6ea7c4.jpeg)


---
**HP Persson** *June 30, 2017 21:16*

On another note, aliexpress rocks.

Paypal banned them due to much problems, so they started Alipay instead, they take care of their own payment solution.

Money is held until product received.

Been shopping there for 3years+, never lost money.


---
**Don Kleinschnitz Jr.** *July 01, 2017 12:19*

**+HP Persson** probably have to give them a try with these low of price's but I get nervous when I see a combination of new payment methods and challenged security certificates on their site.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/PGENGKunpbb) &mdash; content and formatting may not be reliable*
