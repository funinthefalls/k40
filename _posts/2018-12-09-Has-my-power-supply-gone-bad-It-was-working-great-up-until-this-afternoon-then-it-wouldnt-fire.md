---
layout: post
title: "Has my power supply gone bad? It was working great up until this afternoon, then it wouldnt fire"
date: December 09, 2018 20:50
category: "Original software and hardware issues"
author: "S Th"
---
Has my power supply gone bad? It was working great up until this afternoon, then it wouldn’t fire. You can see the power light comes on then gradually fades to off after about 10 seconds. 

![images/9b852ed0f999c08d23b8671ce93c44c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b852ed0f999c08d23b8671ce93c44c1.jpeg)



**"S Th"**

---
---
**Don Kleinschnitz Jr.** *December 09, 2018 21:08*

Unplug the dc connectors (all but the left connector) and then power up and see if the led fades again. 


---
**S Th** *December 09, 2018 22:10*

**+Don Kleinschnitz Jr.** yep. Still fades


---
**Don Kleinschnitz Jr.** *December 10, 2018 15:34*

**+S Th** ok, that looks like the 5V supply or something that feeds it is broken. That LED is connected to the 5V regulator on the LPS. 

Can you measure the 5V and 24V on that supply unloaded with a DVM?



Is this a stock machine or modified? If modified what is connected to the 5 &24V?


---
**S Th** *December 11, 2018 00:22*

Both 5V and 24V are good. I do have a separate PC PSU connected to the main power in parallel with the laser PSU to run accessories. So nothing is connected to the laser PSU except the digital control board and the motion control board. I even disconnected the LED strip that came with the unit.  



Both the digital control and the motion control seem to be functioning normally. The four output pins of the digital control read 0V, 5V, and two output the control voltage between 0-5V (though only one is connected to a cable). And the steppers home normally on power up. 


---
**Don Kleinschnitz Jr.** *December 11, 2018 17:01*

**+S Th** ....with nothing plugged into the LPS** but the AC connector and the power on,..... the 5V and 24V reads good? Does the LED fade in this test?



** "So nothing is connected to the laser PSU except the digital control board and the motion control board." ?????




---
**S Th** *December 11, 2018 17:13*

That is correct. 5V and 24V read good with the green connecter attached (AC input) and all of the white connectors unattached. They even read good after the LED has faded out. 


---
**Don Kleinschnitz Jr.** *December 12, 2018 00:31*

**+S Th** this is very strange. Certainly something is wrong as the laser is not firing. 

The LED hangs on the output of a 5V regulator which is fed by the 24V switching supply. At least one of those voltages should fade if the LED fades.

Looks like the LPS is bad but not clear why?



As a stab in the dark does the laser fire with the test button down on the LPS next to the power light?



![images/190647f7a3c96fdb47900bc2e90d06a1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/190647f7a3c96fdb47900bc2e90d06a1.jpeg)


---
**S Th** *December 12, 2018 03:53*

**+Don Kleinschnitz Jr.** Nope, test button on the LPS doesn’t fire the laser either. I must really be in a bad way if the great Don K doesn’t have an answer. Luckily I had an EBay coupon and was able to order a replacement LPS that’s only gonna cost me $35. Maybe I’ll send you the old one to tinker with. 


---
**Don Kleinschnitz Jr.** *December 12, 2018 04:01*

**+S Th** I would love to get it. I have never seen this fault. 


---
*Imported from [Google+](https://plus.google.com/116589695629402182526/posts/6pTKChKmrbL) &mdash; content and formatting may not be reliable*
