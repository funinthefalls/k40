---
layout: post
title: "Well i bought Coreldraw x7 home and student which is great, but when i run CorelLaser i get a CorelLASER error The CorelDRAW(ver >=11) software not installed!"
date: April 25, 2016 16:05
category: "Discussion"
author: "Tony Schelts"
---
Well i bought Coreldraw x7 home and student  which is great, but when i run CorelLaser i get a CorelLASER error

The CorelDRAW(ver >=11) software not installed!



Any ideas????

will I be able to create files and use in LaserDRW? both cut and engrave?





**"Tony Schelts"**

---
---
**Scott Marshall** *April 25, 2016 20:04*

Tony, 

The software kit that came with my K40 had Corel 12 included. 

I couldn't get it to work until I was clued in by I Laser and Yuusuf. They described the install process they used and I knew something was wrong.



I had used the Corel.exe file visible on the disk directory, and it seemed to install fine, but got the error as you did. Turned out the program installer is buried in a .RAR file which my file viewer (Free Viewer) did not see. I installed WinRAR (from the WinZIP folks) and there was the installer. Run it as with any installer, and you should be on the air with Corel 12. You will be instructed NOT to register it, and can't. Use the dongle and serial number (same for all users) to activate it.



Yes, you will be able to cut and engrave and mark (never really got that one). 

Corel is pretty tough to draw in, so you'll probably end up drawing/composing in another package (I use Draftsight, because I'm a fossil from the Acad 10 days) and copying it into Corel  via .pdf , .bmp files etc.

The same setup panel as in LaserDRW with the controller board number spot on top will come up when you go to laser. You set all the parameters in THAT box, not in corel. Power isn't adjustable thru Corel either, there's no hardware support for it. And Don't waste time trying layers. (cut and engrave at once) They don't work. At least not for me.



I'm Guessing the problem with your older version 7 may be compatibility with the laser driver plug-in that takes the Corel output and converts it to a controller compatible signal. It probably only works with >v12.



Good luck,

Scott


---
**Tony Schelts** *April 25, 2016 20:31*

Coreldraw 12 is ancient Coreldraw x7 is very new only superseded by X8


---
**Scott Marshall** *April 25, 2016 20:35*

That makes sense??.  Show how much I know about Corel.



I'd try clearing it all out, loading the Corel x7, then run the Liytu (or whatever) installer. From what I understand, it loads the plug in if you already have Corel, otherwise it loads 12 and the plug in to match.



Sorry, I thought I had your answer. There's got to be somebody here that's running x7, hope they chime in.



Scott


---
**HalfNormal** *April 25, 2016 21:42*

I installed Corel 12 then X7 without issues on Win7 and 10.


---
**Tony Schelts** *April 25, 2016 22:20*

I had no issues with x7 but it wasnt legit.  so i bought x7 home and student now plugin wont work.


---
**Tony Schelts** *April 25, 2016 22:21*

Thanks Scott for trying good group this is.


---
**HalfNormal** *April 25, 2016 22:47*

**+Tony Schelts**​ did you reinstall Corel laser after installing x7?


---
**Tony Schelts** *April 25, 2016 22:50*

yes.


---
**Tony Schelts** *April 25, 2016 22:51*

It might be a case of Uninstalling it again. including laserdrw and perhaps corel draw,  Reinstall CD and then CorelLaser etc.?


---
**Anthony Bolgar** *April 26, 2016 15:15*

If the pirated version works use it, you are OK legally as you purchased an actual license for the software.


---
**Tony Schelts** *April 26, 2016 18:07*

The pirated version is corporate and costs £500 plus. I purchased X7 home and student cost £67 big difference but plugin works with corporate only :(


---
**Tony Schelts** *April 26, 2016 18:09*

Reinstall didnt work got an email of [http://www.3wcad.com/](http://www.3wcad.com/) site and have tried emailing them hope they understand and respond


---
**Mike Mauter** *April 27, 2016 02:00*

I think the difference is that the Home and Student version does not have Macro and VBA support. This is in small print on the bottom of the page.

"VBA / VSTA support

- Macro functions disabled

- Some Professional Print options disabled (CMYK features, composite, color separations, PostScript and prepress tabs in the print dialog)

- Enhanced Color Management, Default Color Management settings and large print preview disabled

- Some additional functions and support removed: Color Styles, Object Styles , Color Harmonies, QR codes, Planar Mask tool in Corel PHOTO-PAINT, color

 proofing option, Undo dockers, Universal Laser & Roland color palettes, the ability to create left & right master pages, and custom placeholder text

- Some external file format support removed: DXF/DWG, DCS, CGM, JPEG 2000, etc.

- The layout tab has been simplified

®

- Option to purchase Premium Membership not available for CorelDRAW Home & Student Suite Users" I think the only option is to use the X7 Version for your design work and then save the file as  CDR version 12 file. then open it with Version 12 and engrave or cut.






---
**Tony Schelts** *April 27, 2016 15:30*

I think your right thanks for the suggestion .


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/8J7c5byYp3h) &mdash; content and formatting may not be reliable*
