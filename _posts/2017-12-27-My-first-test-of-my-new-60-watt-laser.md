---
layout: post
title: "My first test of my new 60 watt laser"
date: December 27, 2017 12:52
category: "Object produced with laser"
author: "HalfNormal"
---
My first test of my new 60 watt laser.



My son sells Kenworth trucks and he wanted to make something for his boss.

The wood is just standard pine. He wanted the "burned" look so no masking or sanding done, just a coat of clear coat. Getting used to the software and settings but not too many issues.



![images/1a84c85072a3879476ab0dc6d970cf74.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1a84c85072a3879476ab0dc6d970cf74.jpeg)



**"HalfNormal"**

---
---
**Ned Hill** *December 27, 2017 14:45*

Very cool.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/icZ2MVtxr8D) &mdash; content and formatting may not be reliable*
