---
layout: post
title: "My today first cut plastic :)"
date: November 29, 2017 23:14
category: "Object produced with laser"
author: "Jakub Loud\u00e1t"
---
My today first cut plastic :) 

![images/8bf0488325c1b28bce17187d7db8a21d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8bf0488325c1b28bce17187d7db8a21d.jpeg)



**"Jakub Loud\u00e1t"**

---
---
**Jean-Phi Clerc** *November 30, 2017 15:48*

hi, great ! with a K40?


---
**Jakub Loudát** *December 01, 2017 08:43*

**+Jean-Phi Clerc** Yes 40W




---
**Jean-Phi Clerc** *December 01, 2017 08:45*

:-) coool


---
**Frank Farrant** *December 08, 2017 22:59*

Looks great.  What thickness and settings did you use ?


---
*Imported from [Google+](https://plus.google.com/+JakubLoudát/posts/cM1qUcxA6j4) &mdash; content and formatting may not be reliable*
