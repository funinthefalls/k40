---
layout: post
title: "hi all , i have a question about the laser tube cooling system ,would a PC water cooling system work with radiator pump and reservoir ?"
date: May 07, 2018 09:55
category: "Discussion"
author: "Nathan R"
---
hi all ,



i have a question about the laser tube cooling system ,would a PC water cooling system work with radiator pump and reservoir ? .  I am thinking of a better cooling setup more compact as i dont like the idea of a bucket of distilled water near the electronics..lol .



what are your thoughts has this been done ?





**"Nathan R"**

---
---
**HalfNormal** *May 07, 2018 14:37*

Do a search of the site and you will see what it takes to cool the water and what people have done to cool it. You will also see what has worked and what has not.


---
**Tim Rennels** *May 07, 2018 23:33*

There's a good guide to cooling here: [k40laser.se - Watercooling - a deep dive - K40 Laser](https://k40laser.se/watercooling/watercooling-deep-dive/)


---
**Nathan R** *May 08, 2018 07:30*

cool thanks guys 


---
*Imported from [Google+](https://plus.google.com/107472733712638889175/posts/ZfdB4mHWTaq) &mdash; content and formatting may not be reliable*
