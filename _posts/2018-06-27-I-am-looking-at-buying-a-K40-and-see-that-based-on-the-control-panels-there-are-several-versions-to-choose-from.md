---
layout: post
title: "I am looking at buying a K40 and see that based on the control panels, there are several versions to choose from"
date: June 27, 2018 17:47
category: "Discussion"
author: "Todd Wolaver"
---
I am looking at buying a K40 and see that based on the control panels, there are several versions to choose from. Is one better than another or is there one that is easier to upgrade vs another? Or does it not matter?



From what I can see it looks like the following variants are available:



- Analog

- Digital

- Digital with E-stop

- Digital with E-stop, main switch, temp meter (I assume this would be optimal)





![images/786a097247649eb8024db62609e1d7fe.png](https://gitlab.com/funinthefalls/k40/raw/master/images/786a097247649eb8024db62609e1d7fe.png)



**"Todd Wolaver"**

---
---
**Adrian Godwin** *June 27, 2018 18:16*

The main disadvantage of the digital models - at least as far as I am aware - is that they display the proportion of some arbitrary maximum demand for power sent from the controller to the laser power supply. The analog ones display the actual laser current. The laser is somewhat nonlinear and increasing the demand beyond a certain point can actually reduce the power. There's also no way to be sure whether it's set up correctly so a high setting can reduce the life of the tube if it causes excessive current to flow.



My preference would be for both digital (to set the current) and analog (to measure it), but if the later digital models include current measurement that would also be good.


---
**Don Kleinschnitz Jr.** *June 27, 2018 18:58*

Current measurement is an important function to have. 


---
**Paul de Groot** *June 27, 2018 23:08*

Both models are identically in terms of laser power supply, tube, nano m2 controller. You can always add a potmeter and a switch to choose between the dig.control panel and the potmeter. Adding an amp meter is great to see how much the tube uses. If you replace gge controller then tge dig control panel has little use.


---
**Todd Wolaver** *June 28, 2018 05:39*

Thanks for the info. I suspect I may get the analog and Cohesion3D Mini.


---
**Don Kleinschnitz Jr.** *June 28, 2018 12:05*

**+Paul de Groot** 

Paul, I do not see any relationship/connection between the power control panel type and the controller type. What is your thinking in regard to: <i>If you replace gge controller then tge dig control panel has little use.</i>.?


---
**chad cory** *July 03, 2018 06:11*

I have digital with e-stop and the two switch and temp, upgraded with c3d mini and lightburn and love it. But I should have saved some money and just got the cheapest one.


---
*Imported from [Google+](https://plus.google.com/105972473099433019146/posts/KwMqjfEwpEd) &mdash; content and formatting may not be reliable*
