---
layout: post
title: "Sometimes you just have to go with your gut"
date: September 28, 2016 05:58
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
Sometimes you just have to go with your gut. Small 4x4" black slate pieces, Photoshop dithering on a color image, converted to 2 color index, and inverted. The picture really does not do it justice ... This is my daughter who sadly passed away middle of August.﻿

![images/dc03fc7d3807fd8104ec182c8245115a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc03fc7d3807fd8104ec182c8245115a.jpeg)



**"Ashley M. Kirchner [Norym]"**

---
---
**Ian C** *September 28, 2016 06:24*

Sorry for your lose my friend, our thoughts are with you and your family. 


---
**Tony Schelts** *September 28, 2016 06:26*

My Condolences Ashley,  much grace.  That's a great picture.  Could you post the process for this and what machine where you using.  I have some pieces of slate.  Thank you . 




---
**photomishdan** *September 28, 2016 11:06*

I'm really sorry for your loss, that is so awful. 

How durable is the engrave? If you wet the slate does the image remain?


---
**Bob Damato** *September 28, 2016 12:18*

**+photomishdan** I have done several slate coasters now and it is quite durable and the look is great.  Ashley is correct, pictures dont do it justice. I would also love to know the process for this particular one, it came out quite well.  **+Ashley M. Kirchner** my sincere condolences.  :(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 28, 2016 14:29*

As others have mentioned, condolences to you & your family in this testing time. This is a beautiful piece of work & a wonderful way to memorialise your daughter & the joy she would have brought to your family. 


---
**J.R. Sharp** *September 28, 2016 15:33*

That is beautiful. My condolences. Sorry to ask a shop question, but is this with stock board or smoothie?


---
**Ashley M. Kirchner [Norym]** *September 28, 2016 19:23*

Thanks everyone, it's not been easy. This Saturday she would've celebrated her 21st birthday.



Anyway, this was done on a stock K40 as far as the electronics is concerned. Mine came with the M2 Nano Lihuiyu board and I just haven't replaced it yet. This thing does not do proper PWM gray scaling, so my only recourse is to use a dithering process. The image is far from perfect, however given that the slate itself isn't smooth, and it's also not meant to be seen up close, it works pretty well.



I started with a full color picture in Photoshop CC. I first perform an Image Resize, making sure the 'Resample' option is checked OFF, and I size the longest dimension to the final piece size, in this case 100mm. From there I go to Image -> Mode -> Indexed Color and set the parameters as: 

Palette: Uniform

Uncheck the 'Transparency" box, then change the Colors to 2.

Set Dither to Diffusion, and below that play with the percentage till you're happy with the result. In my case, I raised it slightly from 75 to 80%.



After that, common sense kicks in: what am I putting it on? Light colors material, or dark? Since the laser burns on black pixels, if you're engraving a light colored material, you're done, save the file and go for it. But, if you're putting it on a dark material THAT ENGRAVES LIGHT, like black slate does, then you have to invert the image before you save it out. Experiment, have fun.


---
**Ashley M. Kirchner [Norym]** *September 28, 2016 19:25*

My next attempt though will be to try multiple passes. See, a single pass will produce an image, however it's not very "bright". And either slowing it down, or increasing the output, will cause the slate to 'burst' where it's being burned, resulting in wider etchings and you lose detail. So I want to try the same speed and output, but multiple passes, see if that makes a better engraving.



For cutting, it works great. If I get some time later today, I'll take some close up pictures of what it looks like between a 'cut' pass and an 'engrave' pass. The engraving is barely noticeable when you run your finger over it, whereas the 'cut' pass does actually dig into the stone.


---
**your patio** *September 28, 2017 18:11*

Condolences


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/2LRikPHUCVL) &mdash; content and formatting may not be reliable*
