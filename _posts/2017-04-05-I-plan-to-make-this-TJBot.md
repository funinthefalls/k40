---
layout: post
title: "I plan to make this TJBot: ."
date: April 05, 2017 09:01
category: "Discussion"
author: "rshendro"
---
I plan to make this TJBot: [https://www.instructables.com/id/Build-TJ-Bot-Out-of-Cardboard/](https://www.instructables.com/id/Build-TJ-Bot-Out-of-Cardboard/). I have my equivalent of K40 machine that uses CorelLaser software.



The design file specifies cut operation but it uses both solid linecut and broken linecut. Apparently the broken linecuts are for folding the cardboard. Now my understanding with Corellaser, I need to have closed colored area for doing cut. So my question how do I  do that broken linecuts?



Thanks for any hints. 





**"rshendro"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 10:44*

You don't need to have closed lines for cutting. You may need to change the cut data to WMF or EMF to allow you to cut lines without any issues. I used WMF for both cut & engrave data. Others have found that sometimes when you have curved lines that CorelLaser joins the curves in undesired places. If this happens, then you need to switch it to EMF data output.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 10:44*

Oh, side note... you want to set the line width to 0.001mm (not hairline) otherwise it will try cut the lines twice (on both sides of it).


---
**rshendro** *April 05, 2017 10:56*

Thanks Yuusuf...this is new information for me. Really appreciate it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 12:47*

**+rshendro** You're welcome. Hopefully it resolves your issue. These machines are actually quite great. Shame about the lack of manual or documentation.


---
*Imported from [Google+](https://plus.google.com/100686046588453560579/posts/1QCXWYFS6Bx) &mdash; content and formatting may not be reliable*
