---
layout: post
title: "Hello, any help would be great Turning on the machine, it drives up to the top but then moves very slowly to the right, not going home (top left) as it should"
date: September 26, 2016 18:34
category: "Discussion"
author: "ian humble"
---
Hello, any help would be great

Turning on the machine, it drives up to the top but then moves very slowly to the right, not going home (top left) as it should. Anyone?







**"ian humble"**

---
---
**Anthony Bolgar** *September 26, 2016 18:50*

Make sure you have entered the correct serial number in the software. The nano controllers do funny things if the serial number is not set correctly.


---
**ian humble** *September 26, 2016 18:52*

Well, we've used it hundreds of times. Now it's started doing this....


---
**David Spencer** *September 26, 2016 18:53*

Try cleaning the end stops, mine got dirty and did the same thing.






---
**ian humble** *September 26, 2016 19:10*

Tried that. Could this fella be the culprit? Something has gone wrong 



![images/d7029fa619993ac10de8db70e4f16df1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d7029fa619993ac10de8db70e4f16df1.jpeg)


---
**Ariel Yahni (UniKpty)** *September 26, 2016 19:51*

**+ian humble**​ from experience and a couple of them users this most likely is : cleaning the optical endstops and / or check each solder joint on the cables


---
**ian humble** *September 26, 2016 20:07*

Thanks, I'll look at these things.



 Is there anywhere in the UK that sells spares for these machines?


---
**Don Kleinschnitz Jr.** *September 27, 2016 11:53*

This post (work in progress) may help you. There are links to schematics to use for testing.

[plus.google.com - Can anyone please help me? I removed the mechanism from my laser in order to…](https://plus.google.com/113253507249342247018/posts/6dmjyZy2A3F).




---
**ian humble** *September 27, 2016 12:37*

That was a lot... thank you


---
*Imported from [Google+](https://plus.google.com/114003426045732749773/posts/1brzupxXFkH) &mdash; content and formatting may not be reliable*
