---
layout: post
title: "Really enjoying the laser engraver. I decided to try and use corel draw and designed my own pen display for 12 pens cuts through 4mm accrylic like butter coolio"
date: November 17, 2015 16:25
category: "Object produced with laser"
author: "Tony Schelts"
---
Really enjoying the laser engraver. I decided to try and use corel draw and designed my own pen display for 12 pens cuts through 4mm accrylic like butter coolio.



![images/245e6ae1055b13c57298b25654915c02.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/245e6ae1055b13c57298b25654915c02.jpeg)
![images/fb43ff31ac5df7d7bf0703f9f8632dcf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb43ff31ac5df7d7bf0703f9f8632dcf.jpeg)
![images/00857339faf2aa78f7d24ff3f19fde65.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00857339faf2aa78f7d24ff3f19fde65.jpeg)

**"Tony Schelts"**

---
---
**Anthony Coafield** *November 18, 2015 08:17*

That's beautiful. Where did you get the plans? I'd love to make some of those. Far nice than the display stand I bought.


---
**Tony Schelts** *November 18, 2015 08:55*

I created it in corel draw. The bottom row of holes were just small holes and I re drilled them with the size I wanted and at a slight angle to the pens would lean backwards.  I didnt try just cuting straight holes?  Do you want me to upload the plans? I live in the UK so you posted at 2.08 am my time.


---
**Anthony Coafield** *November 18, 2015 10:34*

If you don't mind posting the plans that would be great. No hurry. I'm here in Australia, so a much more civilised hour of the day!


---
**Tony Schelts** *November 18, 2015 12:14*

Like I said I have never created a pen stand plan before,  this was made for pens that were 10-11 mm at the top end if your creating wider pens the plans may need adjusting. If you don't have the opportunity to do it yourself. Let me know the size and I will have a play around for you. Im at work at the moment and do all that stuff in my spare time


---
**Anthony Coafield** *November 18, 2015 20:35*

Thanks Tony. Most of mine are a normal slimline pen so should fit beautifully, but if I make bigger it will be good for me to learn to tweak stuff also. I usually learn things best by tweaking before starting form scratch.


---
**Tony Schelts** *November 18, 2015 23:39*

Forgot I was going Cinema when I got home will post tomorrow when I get home sorry


---
**Anthony Coafield** *November 18, 2015 23:59*

Really no hurry. Enjoy the movie!


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/Z7QCRiUeBWk) &mdash; content and formatting may not be reliable*
