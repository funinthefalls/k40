---
layout: post
title: "My name is Larry and I am a laser hoarder!"
date: December 11, 2017 13:00
category: "Discussion"
author: "HalfNormal"
---
My name is Larry and I am a laser hoarder!



I just purchased a 60 watt laser to go with my two K40s and one 2.4W solid state laser (on my cnc X-Carve)



[https://www.ebay.com/itm/252730728666](https://www.ebay.com/itm/252730728666)





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *December 11, 2017 13:24*

Christmas came early :).... nice looking unit. Are you going to keep the controller or modify?


---
**Ariel Yahni (UniKpty)** *December 11, 2017 13:26*

You have done the most important thing... Accept who you really are :)


---
**HalfNormal** *December 11, 2017 13:28*

**+Don Kleinschnitz**​ it comes with a DSP controller. I will play with it and decide what to do next.


---
**Ariel Yahni (UniKpty)** *December 11, 2017 14:24*

**+HalfNormal**​ that dsp will play very nice with Lightburn


---
**HalfNormal** *December 11, 2017 15:35*

**+Ariel Yahni**​ I was wondering about that. Now I'm excited to try Lightburn.


---
**Anthony Bolgar** *December 11, 2017 15:58*

You are catching up to me, I have a 60W 500 X 400 bed, K40, Redsail LE400 50W, and a 3.5W diode engraver (approx 2' X 4' work area) Had another K40 untill about 4 -5 weeks ago, sold it.


---
**Ariel Yahni (UniKpty)** *December 11, 2017 16:02*

**+Anthony Bolgar** you sir are in another league


---
**Anthony Bolgar** *December 11, 2017 16:56*

Looking at getting a 100W laser to round out my Laser farm.


---
**HalfNormal** *December 11, 2017 18:20*

**+Anthony Bolgar**​ Now you tell me! I need to see if I can cancel my 60 and get a 130!


---
**Anthony Bolgar** *December 12, 2017 02:29*

Trust me, you will love LightBurn, so very easy to use, and fast, files that have taken 45 minutes to load in an Opensource package are loading in 2-3 seconds in LightBurn. It is very optimized, the developer used to be a video game developer where everything was as fast and compact as could be.


---
**Erik Quintana** *December 12, 2017 19:07*

I have a K40 and an X-Carve follow your steps.




---
**HalfNormal** *December 12, 2017 19:10*

Careful! You might step in something!


---
**Madyn3D CNC, LLC** *December 14, 2017 14:53*

That's a beauty. My life will be complete once I have one of these sitting in my workshop. 


---
**Inland Empire Engravings** *March 22, 2018 02:24*

Does this machine engrave on glass cups


---
**HalfNormal** *March 22, 2018 02:30*

Yes but you need to purchase a rotation base for another  $100.00


---
**Inland Empire Engravings** *March 23, 2018 00:36*

**+HalfNormal** in your opinion is this machine worth it? Or is there a better machine in your opinion or around the same price? Does the machine have any flaws? Have you ever engraved on glass? I do etching on glass is almost the same as laser engraver? Sorry for all the questions it's just I'm really interested in a laser machine that fits my needs.


---
**HalfNormal** *March 23, 2018 00:44*

I personally think it has been a great machine for the price. 60 watts does what I need. It has no problem with glass. I had to invest  $100 extra for a rotary table. I also purchased LightBurn for $80.00. You might be happy with RDWorks but they do not have the support of LB.


---
**Inland Empire Engravings** *March 23, 2018 00:55*

**+HalfNormal** witch rotary table is for this machine


---
**HalfNormal** *March 23, 2018 03:43*

**+Inland Empire Engravings** This is the one I purchased.

[ebay.com - Details about Regular Rotary Axis For 60W-100W Laser Engraver Engraving Machine](https://www.ebay.com/itm/Regular-Rotary-Axis-For-60W-100W-Laser-Engraver-Engraving-Machine/362230059542?epid=22014216439&hash=item5456981216:g:QGcAAOSwVqlaDIjY)


---
**Inland Empire Engravings** *March 23, 2018 18:36*

**+HalfNormal** 

Do you know of a rotary  that is adjustable for shot glasses and that it's able to engrave all the way around a beer mug that has a handle? I just ordered my machine thank you for all your support


---
**HalfNormal** *March 23, 2018 19:23*

I purchased the rotary off eBay. Just do it look on eBay and see if you can see something that will fit your bill. You just might need to purchase a couple of different rotary attachments to get what you need.


---
**Inland Empire Engravings** *March 23, 2018 22:20*

**+HalfNormal** 

Would you know if this is compatible with the 60w laser engraver


---
**HalfNormal** *March 23, 2018 22:21*

No link


---
**Inland Empire Engravings** *March 23, 2018 22:26*

**+HalfNormal** 

[https://www.amazon.com/gp/aw/d/B073PFKSWC/ref=mp_s_a_1_2?ie=UTF8&qid=1521843256&sr=8-2&pi=AC_SX236_SY340_FMwebp_QL65&keywords=laser+rotary+attachment#productDescription_secondary_view_div_1521843353925](https://www.amazon.com/gp/aw/d/B073PFKSWC/ref=mp_s_a_1_2?ie=UTF8&qid=1521843256&sr=8-2&pi=AC_SX236_SY340_FMwebp_QL65&keywords=laser+rotary+attachment#productDescription_secondary_view_div_1521843353925)


---
**HalfNormal** *March 23, 2018 23:17*

Looks like it will work. You will need to get the correct connector or make a cable to interface it to the laser. It is a screw on connector to disconnect the y axis and connect the rotary attachment.


---
**Inland Empire Engravings** *March 24, 2018 16:04*

**+HalfNormal** 

I'm trying to order the rotary table but they're asking me single phase or 3 phase a stepper motor which one is for this machine


---
**HalfNormal** *March 24, 2018 18:36*

Not sure about a 1 phase or 3 phase but the steppers used in this unit are 2 phase bipolar stepper Motors.


---
**Inland Empire Engravings** *March 30, 2018 00:57*

**+HalfNormal** 

Would you be able to let me know what power and speed works best for different types of materials that works best for you


---
**HalfNormal** *March 30, 2018 01:04*

Trial and error is the best teacher! Get a lot of scraps and play! Have a notebook handy to write down what works and what does not. Not all lasers and materials are equal so what works for one might not on another. Here is what I do. Start low power and a fairly fast speed. Then start to slow the speed a little then increase power. Back and forth until you get the results you are looking for. I cut a square a few inches square for the template.  This the best way to know your machine and materials. Have fun!


---
**HalfNormal** *March 30, 2018 01:07*

Oh yeah, different software will have different results. 


---
**Inland Empire Engravings** *April 04, 2018 23:51*

**+HalfNormal**  Can you let me know what controller is used can't figure it out  using lightburn

![images/64537c7badb6b0475190d9f51bc75a5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/64537c7badb6b0475190d9f51bc75a5b.jpeg)


---
**HalfNormal** *April 04, 2018 23:53*

You should have a selection for Rudia


---
**Inland Empire Engravings** *April 08, 2018 17:35*

**+HalfNormal** 

Thank you so much for your help you same me from a big headache. Would you know why every time I put a picture to engraved it pops up this message

![images/d84e20cb2e8def82f7d6075bc7f9eacb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d84e20cb2e8def82f7d6075bc7f9eacb.jpeg)


---
**HalfNormal** *April 08, 2018 17:41*

You are welcome. Happy to help when I can. I am unfamiliar with that error message. The fastest way to get help with lightburn is on their facebook page. I did try a quick google search and came up empty. Please let me know what you find out.


---
**Inland Empire Engravings** *April 14, 2018 23:47*

I restarted the program and it worked good that message only pops up when I import certain pictures. What's the fastest speed that I can run it?  And what the most power I can run ? For safety


---
**HalfNormal** *April 14, 2018 23:54*

There are always a lot of variables on what works and what does not. The lowest power is always the best for safety. Speed is relative to the material and expected outcome ie lighter or darker and type of material. Remember that what worked once on a certain material might not work the same again due to natural variations. I always work with scraps before committing. I wish that you could just set and forget but that is not how the laser works. Keep working with it and it will come easier to find what works most of the time. Most of all have fun!


---
**Inland Empire Engravings** *October 16, 2018 01:22*

Hello HalfNormal I have a question I wanted to see if you can help my out with. By any chance do you know we're I can get  the right power supply for this laser mines fried a few days ago? Thank you


---
**HalfNormal** *October 16, 2018 12:52*

**+Inland Empire Engravings** Funny you should ask! Mine also failed but I was able to get a replacement in 3 days. The replacement is plug and play. A bit of advice, when you are resiliconing the lead to the laser, put tape around the bottom or the silicone will want to run out.

I am going on vacation the end of the week and will not have cell service the whole time. I will check my G+ as often as I can to help you out. Here is the link.

[ebay.com - Details about 60W Laser Power Supply for CO2 Laser Tube Engraver Engraving Cutter Machine](https://www.ebay.com/itm/60W-Laser-Power-Supply-for-CO2-Laser-Tube-Engraver-Engraving-Cutter-Machine/253238124107?ssPageName=STRK:MEBIDX:IT&_trksid=p2057872.m2749.l2649)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/4j8ezKH35Wf) &mdash; content and formatting may not be reliable*
