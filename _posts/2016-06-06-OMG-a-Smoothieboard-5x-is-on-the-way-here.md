---
layout: post
title: "OMG a Smoothieboard 5x is on the way here !!!!!"
date: June 06, 2016 16:47
category: "Modification"
author: "Don Kleinschnitz Jr."
---
OMG a Smoothieboard 5x is on the way here !!!!! Now its getting serious.....:)





**"Don Kleinschnitz Jr."**

---
---
**Don Kleinschnitz Jr.** *June 06, 2016 16:55*

**+Peter van der Walt** Just dawned on me .... is there a local display available for smoothieboard ?


---
**Don Kleinschnitz Jr.** *June 06, 2016 17:17*

**+Peter van der Walt** ....I might have just answered my own question and sorry for all the questions. 

Is it this the display you recommend:

[http://shop.uberclock.com/products/smoothieboard-glcd-shield](http://shop.uberclock.com/products/smoothieboard-glcd-shield)?

Am I correct in assuming this plugs right on to the smoothie. Does the display plug into it directly or via a cable?

Do I have to then keep the smoothie board and display together. The picture is unclear on how it is assembled. 



Does this have its own code to download?


---
**Don Kleinschnitz Jr.** *June 06, 2016 18:03*

**+Peter van der Walt** thanks, sorry if the questions are annoying. 

Yes I have been all over the wiki and its very good and very clear in most cases. 

In the case of the local control panel however I did not find it to be clear, even what to buy is a bit convoluted. I actually missed the uberclock GLCD as it only shows the shield. How the included Reprap GLCD screen  (which is an option) connects to the GLCD can only be surmised from looking at adapter boards that are similar, not the uberclock configuration. 

Don't take this as a complaint or criticism just a "smoothie newbie's" perspective.

I am going back to uberclock for that LCD :)


---
**Don Kleinschnitz Jr.** *June 06, 2016 18:04*

... and the GLCD just sold out .....:(


---
**Don Kleinschnitz Jr.** *June 06, 2016 20:12*

Ok I feel stupid.... especially after looking at this link: [http://robotseed.com/index.php?id_product=23&controller=product&id_lang=2](http://robotseed.com/index.php?id_product=23&controller=product&id_lang=2)


---
**Stephane Buisson** *June 07, 2016 07:41*

take care cheap glcd do have the connectors inverted, just easy when you are aware of.


---
**Don Kleinschnitz Jr.** *June 07, 2016 12:42*

Thanks **+Stephane Buisson** I'm going to wait for a kit from uberclock.


---
**Corey Renner** *June 11, 2016 17:00*

Don, what made  you go with the 5x?  Seems like more X's than necessary, or am I overlooking something (new to K40 world).


---
**Don Kleinschnitz Jr.** *June 11, 2016 17:07*

I over-killed it cause I wanted to be able to re-purpose the board for later projects and the price difference did not seem big.


---
**Corey Renner** *June 11, 2016 17:37*

Makes sense, I may do the same.  Thanks, c


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/BgTGgwHzN7C) &mdash; content and formatting may not be reliable*
