---
layout: post
title: "why can i not get to the search area on the left side of the screen unless I hit f11?"
date: December 18, 2016 23:02
category: "Discussion"
author: "John Austin"
---
why can i not get to the search area on the left side of the screen unless I hit  f11?





**"John Austin"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 19, 2016 02:40*

Is it possible that your screen resolution is not large enough? If you resize the browser window (for me it's to about 1/2 my screen res, 2880 x 1800 / 2 = ~ 1440 x 900) it changes into what looks to be tablet view. Up the top right is a ... (rotated 90 degrees). If you click that you can access the search feature.


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/W22t5pH656Z) &mdash; content and formatting may not be reliable*
