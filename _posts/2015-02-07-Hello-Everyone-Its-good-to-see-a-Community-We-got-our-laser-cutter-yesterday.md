---
layout: post
title: "Hello Everyone! Its good to see a Community We got our laser cutter yesterday"
date: February 07, 2015 05:01
category: "Software"
author: "Bee 3D Gifts"
---
Hello Everyone!

Its good to see a Community We got our laser cutter yesterday. Luckily everything was calibrated and we got it to cut, but man, the edges are super jagged? Any idea what we could be doing wrong?





**"Bee 3D Gifts"**

---
---
**Other World Explorers** *February 07, 2015 11:35*

I assume you are using Moshi draw...

For me I was using the wrong driver to cut with. Try cutting a small 1 inch by 1 inch circle using each driver in turn. You will find one that works far better then the one mentioned in the instructions. Also when ever possible use vector art rather than BMP style. 



Oh and welcome to the group! 


---
**Bee 3D Gifts** *February 07, 2015 18:56*

Thank you so much...I was using Jpeg..Now I just put them into Illustrator and Export them as EMF's. So if anyone else has that issue..it fixed ours! Thanks again!! =) 


---
*Imported from [Google+](https://plus.google.com/101949715741571451458/posts/gBPSAYdFFcn) &mdash; content and formatting may not be reliable*
