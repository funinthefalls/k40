---
layout: post
title: "Originally shared by Pete OConnell I've got 3 x 20x40x1500 for my X & Y axis, with a Vslot gantry plate on either end of my X axis with 4 delrin wheels on each"
date: June 03, 2016 17:11
category: "Repository and designs"
author: "Pete OConnell"
---
<b>Originally shared by Pete OConnell</b>



I've got 3 x 20x40x1500 for my X & Y axis, with a Vslot gantry plate on either end of my X axis with 4 delrin wheels on each. 2 wheels have eccentric spaces and the other 2 have aluminium spacers.



The X axis length is 1420mm, if I move the X axis along the Y axis in the middle it works like a dream - however if I move the X axis along by holding it on either end I get slack on the other end.



At the moment there are no motors etc on the frame, just the aluminium and wheels.



Any ideas on what I can do to reduce the slack?

![images/d0486fade3f673814e7f7020cc72c13a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d0486fade3f673814e7f7020cc72c13a.jpeg)



**"Pete OConnell"**

---
---
**Pete OConnell** *June 03, 2016 17:11*

shared this from my post over at openbuilds - apologies if you see this twice :)


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/68NMFqhovtf) &mdash; content and formatting may not be reliable*
