---
layout: post
title: "Has anyone added a Z table to your K40??"
date: February 05, 2016 21:39
category: "Discussion"
author: "CUPE New Brunswick"
---
Has anyone added a Z table to your K40??  I bought one from ebay and put it together (without any instructions, NOT provided) and when I went to install it, it's WAY to high for the laser head.  I can lower the table, but the frame around it is higher that the laser hear..





**"CUPE New Brunswick"**

---
---
**3D Laser** *February 05, 2016 22:09*

Can you post the link of the one you bought 


---
**Brien Watson** *February 06, 2016 02:43*

That's the same one that I have Carl. Problem is, the laser nozzle head sticks down lower Than the frame of the bed.  So when it tries to home it James up against the frame...


---
**Brien Watson** *February 06, 2016 02:44*

So, will I have to change out my laser Head??


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 06, 2016 11:28*

Or you could cut out the bottom of the laser chassis & raise the chassis up to cater for the extra height that your Z-table takes up maybe.


---
*Imported from [Google+](https://plus.google.com/100899443981626177789/posts/NZVo7pPiit6) &mdash; content and formatting may not be reliable*
