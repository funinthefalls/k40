---
layout: post
title: "Stock on the left for my first mirror"
date: August 04, 2016 22:13
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Stock on the left for my first mirror.  I have being using it like this since the beginning. Now off to a new beginning 

![images/be0985ffb07e97ab8843f02f8673e31a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be0985ffb07e97ab8843f02f8673e31a.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Robert Selvey** *August 05, 2016 01:41*

Where do you get a new mirror for the laser head ?


---
**Ariel Yahni (UniKpty)** *August 05, 2016 01:59*

Look at this on eBay [http://www.ebay.com/itm/201351431116](http://www.ebay.com/itm/201351431116)


---
**Robert Selvey** *August 05, 2016 03:28*

I wonder how cutting mirrors out of a hard drive platter would work out.


---
**Alex Krause** *August 05, 2016 03:38*

**+Robert Selvey**​ seen people do it... have also seen people polishing copper rounds to a mirror finish


---
**Robert Selvey** *August 05, 2016 03:49*

Wonder how well they dissipate the heat.


---
**Bart Libert** *August 05, 2016 08:30*

I only use MO mirrors, no hd platters. Never had to even clean one on my 80W cutter




---
**John Austin** *August 05, 2016 22:34*

anything special to use when cleaning the mirrors?

Looks like mine have black spots on it. The laser is new.


---
**Robert Selvey** *August 05, 2016 23:59*

I use alcohol pad or alcohol on a cue tip.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/arBFJDprURP) &mdash; content and formatting may not be reliable*
