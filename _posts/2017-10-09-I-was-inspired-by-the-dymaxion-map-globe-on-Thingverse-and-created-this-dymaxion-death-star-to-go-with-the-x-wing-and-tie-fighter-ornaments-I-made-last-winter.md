---
layout: post
title: "I was inspired by the dymaxion map globe on Thingverse and created this dymaxion death star to go with the x-wing and tie fighter ornaments I made last winter"
date: October 09, 2017 17:38
category: "Object produced with laser"
author: "Jeff Bovee"
---
I was inspired by the dymaxion map globe on Thingverse and created this dymaxion death star to go with the x-wing and tie fighter ornaments I made last winter.  Will be uploading the files to Thingverse once I clean up some of the errors and will follow up with a link.

![images/fd6887e8492bbb3eabd350737e8fa497.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fd6887e8492bbb3eabd350737e8fa497.jpeg)



**"Jeff Bovee"**

---
---
**Ned Hill** *October 09, 2017 17:48*

Nicely done.


---
**Jeff Bovee** *October 15, 2017 04:40*

Uploaded files

[thingiverse.com - Dymaxion Death Star by jbovee](https://www.thingiverse.com/thing:2576935) 


---
**Vanessa Darrey** *October 17, 2017 18:35*

Wow that came out soo good! kudos 


---
*Imported from [Google+](https://plus.google.com/115193415731522119744/posts/BUZFiTbRrSo) &mdash; content and formatting may not be reliable*
