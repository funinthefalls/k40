---
layout: post
title: "First I wanted to share this sketchup plugin: It allows you to take your sketchup output and turn it into an svg"
date: January 18, 2016 17:27
category: "Software"
author: "Nathaniel Swartz"
---
First I wanted to share this sketchup plugin: [https://github.com/simonbeard/sketchup-svg-outline-plugin](https://github.com/simonbeard/sketchup-svg-outline-plugin)



It allows you to take your sketchup output and turn it into an svg.  I've only used it simple objects, but it seems like it might work with more complex models too.



Second, is it possible to search anymore?  I want to see if anyone had posted this but the search at the top of the new google plus seems to search everything not just the group.







**"Nathaniel Swartz"**

---
---
**Stephane Buisson** *January 18, 2016 17:46*

yep I did, and it seem to me you have the old version link.



with old G+ a search in this community and here we go ->

 [https://plus.google.com/117750252531506832328/posts/SYX3ghtvDW8](https://plus.google.com/117750252531506832328/posts/SYX3ghtvDW8)


---
**Don Kleinschnitz Jr.** *January 23, 2016 16:34*

The latest version (thanks for that) installed the .rb in the plugin directly but does not show up in the extension directory in SU and does not show up in the SU ui??? ideas?


---
**Stephane Buisson** *January 23, 2016 23:56*

take care to the path (not to over-embed in a extra folder); I did lose time with that


---
**Don Kleinschnitz Jr.** *January 24, 2016 16:12*

Thank you that worked. It installed  like this /SketchUp/SketchUp 2015/SketchUp/Plugins/sketchup-svg-outline-plugin-master. I moved the "contents" of sketchup-svg-outline-plugin-master up one level and it works .......


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/VGz4zhBmS3a) &mdash; content and formatting may not be reliable*
