---
layout: post
title: "What are good mirrors to get to replace the ones that came with the K40 ?"
date: February 09, 2017 00:04
category: "Discussion"
author: "Robert Selvey"
---
What are good mirrors to get to replace the ones that came with the K40 ?





**"Robert Selvey"**

---
---
**Ned Hill** *February 09, 2017 00:28*

I replaced my mine with Moly mirrors from Light Object.  Moly is really over kill at the K40 power level but they are robust.  You should be able to get the cheaper gold plated ones from Light Object and be just fine.  You'll also find some people talking about making your own mirrors from hard drive platters or from copper disks if you want to explore that route.  Seems to be some debate on the effectiveness of hard drive platter mirrors but cooper ones seem to do well, just not sure how robust they are.  Hope this helps.  I'm sure some more people will comment with suggestions.


---
**Robert Selvey** *February 09, 2017 00:29*

I don't see mirrors under k40 on lightobjects sight ?


---
**Ned Hill** *February 09, 2017 00:30*

[lightobject.com - Lens & Mirrors](http://www.lightobject.com/Lens-Mirrors-C16.aspx)




---
**Ned Hill** *February 09, 2017 00:31*

You want the 20mm mirrors


---
**Robert Selvey** *February 09, 2017 00:43*

I have the replace the ones that came in it I assume they are 20mm


---
**Stephane Buisson** *February 09, 2017 12:04*

If you dig a bit on ebay you will realize the laser items market is hold by just a few big sellers in china. 



 Mo mirror could just cost £8

[ebay.co.uk - Details about  1x Dia 20mm Mo Reflection Mirror for 10600nm CO2 Laser Engraver Cutter 60W -150W](http://www.ebay.co.uk/itm/1x-Dia-20mm-Mo-Reflection-Mirror-for-10600nm-CO2-Laser-Engraver-Cutter-60W-150W-/321882419139?hash=item4af1afefc3:g:TFYAAOSwU9xUNoZ2) 


---
**Robert Selvey** *February 09, 2017 12:33*

Can you get descent moly mirrors from China ?


---
**Stephane Buisson** *February 09, 2017 13:36*

mine work well . no risk on 40w laser, they are made for more powerful machine. origin is more how long you are happy to wait on transit. (LO have plenty of Chinese stuff).


---
**Robert Selvey** *February 10, 2017 16:24*

What would be good replacement mirrors to get MO Cu Si K9 ?


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/DjDLKG97MHN) &mdash; content and formatting may not be reliable*
