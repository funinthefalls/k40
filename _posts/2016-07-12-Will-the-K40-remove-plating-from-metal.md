---
layout: post
title: "Will the K40 remove plating from metal?"
date: July 12, 2016 01:37
category: "Discussion"
author: "Carl Fisher"
---
Will the K40 remove plating from metal? I know it can't engrave metal directly but if I do a brush plating of say a dark ruthenium over a metal would I be able to engrave off the plating?









**"Carl Fisher"**

---
---
**Jim Hatch** *July 12, 2016 02:00*

not sure. I haven't tried ruthenium. But it does engrave ipad/imacs/etc. It also will engrave painted metal and burn off the paint & powder coatings as well. It will fuse moly to metal.



So, probably can only tell if you try :-)


---
**Alex Krause** *July 12, 2016 02:24*

I have done annodized aluminum and it takes it right off... sometimes there is a bit of residual staining depending on the color 


---
**Carl Fisher** *July 12, 2016 16:00*

Once my plating gear shows up, I'm going to try to plate 24k over polished steel and see if I can laser engrave on that. I don't have high hopes, but you never know.  I do know it works with anodizing and powder coat, but what I'm engraving you can't do that to.


---
**Alex Krause** *July 12, 2016 16:02*

24k gold? I wouldn't even bother... the stock mirrors that come with the machine are gold plated 


---
**Carl Fisher** *July 12, 2016 16:13*

Ok, good to know. I'll play with some of the other platings then. I have my eye on a flat black plating as well and its' cheap to experiment with.


---
**Jim Hatch** *July 12, 2016 16:57*

Moly on steel will engrave black. And a spray can of CRC Dry Moly Lube is only $11 on Amazon.


---
**Carl Fisher** *July 12, 2016 17:07*

I do know that, but since I'm going to be playing with brush plating anyway I thought maybe it might work. No harm in asking right :)


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/AiAPUXxUxQv) &mdash; content and formatting may not be reliable*
