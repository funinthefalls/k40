---
layout: post
title: "This is my 3D printed replacement vent"
date: June 15, 2016 03:49
category: "Hardware and Laser settings"
author: "Corey Renner"
---
This is my 3D printed replacement vent.  It flows much better than stock, doesn't obstruct the bed (it ends at the clearance cut in the rear I-beam) and it is installed/removed from behind the machine without having to mess with the gantry.  I've been using it with the oem fan and it works great.

![images/705de047cd3739d8981a6db751ab9fd8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/705de047cd3739d8981a6db751ab9fd8.jpeg)



**"Corey Renner"**

---
---
**Ben Marshall** *June 15, 2016 03:57*

Do you have any pictures of it installed?


---
**Joe Keneally** *June 15, 2016 03:57*

Could you put the file for this 3D print on thingiverse?


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2016 04:06*

Yeah, I'd also get this printing asap 


---
**Richard** *June 15, 2016 04:07*

If you think it works well now, wait until you get a dust scoop the same size as the opening and something with some real power, like a Dust Collector. The one I have does 660 CFM, and believe me, it sucks ALL dust, smoke, etc out of the way. Overkill? Maybe... but it's worth it.


---
**Corey Renner** *June 15, 2016 04:20*

Sure, here you go.

[http://www.thingiverse.com/thing:1627210](http://www.thingiverse.com/thing:1627210)



I'll take pictures when I get a chance.  Cheers, c


---
**Jim Hatch** *June 15, 2016 04:25*

What's the point of this? Why not just leave the vent out altogether. I've been doing that and using the stock fan as well.  I do have intake fans on the front of the box so the box can breathe - otherwise the fan will try to pull more air than can be replenished through the stock intake hole & various cracks. Haven't found a need for a replacement vent for the one I pulled out.


---
**Corey Renner** *June 15, 2016 04:37*

I get a beautiful smooth sheet of smoke coming from the cut and out the vent.  When I tried it with no vent, I had a cloud, lots of turbulence.  The oem vent is a really bad design, lots of features that block flow.  My machine does have slots in the front which seem to work well and I don't need to crack the lid open like some.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 15, 2016 06:43*

Nice design **+Corey Renner**, but I've just a thought. You mention in your last comment that you get a nice smooth sheet of smoke coming from the cut. I am curious does that sheet of smoke extract higher than the level of the material? As the smoke running along certain materials stains it badly. If you could get this so that the sheet of smoke is about 10mm above the surface of the material would be great.


---
**Joe Keneally** *June 15, 2016 10:25*

**+Corey Renner** Thank you my good Sir!  :-)


---
**Ben Walker** *June 15, 2016 12:00*

excellent.  noticed plumes of smoke when I completely removed my hood.  this sounds perfect other than it is plastic and I have noticed tiny embers being drawn in from time to time.  May go ahead and line my print with foil to help cut down on any possible melt-downs

THANK YOU for sharing this


---
**Corey Renner** *June 15, 2016 15:16*

I meant to line it with that aluminum tape that is used for sealing ducts, but I haven't gotten around to it yet.  The fact that the plastic duct leads to a plastic fan which leads to a plastic hose has reduced the urgency somewhat, but it should probably still be done.  I used that same aluminum tape to seal the big hole in the bottom to keep the laser from engraving the table that it's sitting on.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 15, 2016 15:29*

**+Corey Renner** It's odd that they place that hole in the bottom beneath where the laser will fire, rather than to the side where there is plenty or space but no chance of the laser firing through it.


---
**Corey Renner** *June 15, 2016 15:41*

Agreed, that's the worst possible place for that hole.  Another thing that I noticed on mine was that they use (5) standoffs to mount the bed, the (4) in the corners are fine, but the one in the front center was a disaster.  It happens to be exactly where the extenal stiffening rib is, so they drilled another hole off to the side, then installed the standoff crooked (very crooked) and twisted.  I removed that one.  Update: I added a picture to the Thingiverse page showing the vent installed. [http://www.thingiverse.com/thing:1627210](http://www.thingiverse.com/thing:1627210)


---
**Jim Hatch** *June 15, 2016 16:36*

**+Corey Renner**​ if you've sealed off the air intake hole on the bottom you'll need to add another intake somewhere else. The seams of the lids aren't going to allow enough intake to make up for what the exhaust pulls. The air assist should help but you'd want to figure out if it's pumping enough volume in to generally equalize the in/out volumes. Otherwise without sufficient intake capacity the exhaust will labor and ultimately burn the motor. If you have it on the far end (or middle) of the exhaust hose it might create enough suction to collapse the exhaust hose.


---
**Corey Renner** *June 15, 2016 17:07*

Jim,

I get plenty of airflow.  Mine is one of the new machines that has 12 slots cut into the front of the main door.  It also has big cutouts into the control panel area which is completely ventilated.  Besides the big hole in the bottom, I also plugged two small 7/16" holes in the rear of the main cutting area with nylon stoppers.  I'm new to the K40, but my hackerspace had a small Epilog and has had the giant red chinese one for a couple of years now.  My K40 with oem fan is venting better than either of those.


---
**Jim Hatch** *June 15, 2016 17:15*

Sounds like they made a big step up in ventilation with the new generation. That's good news (I'll probably add a 60 or 80w in the next year or so). With the added venting I've got with 4 small front mounted fans I get better venting with the stock exhaust fan than the big red Chinese laser at our MakerSpace.


---
**Vince Lee** *June 15, 2016 17:46*

On my k40, the cutouts into the control panel area ended up being a problem, as there is a fan in the back of that section creating a competing vacuum.  While the fumes were cleared fine from the cutting area, some were coming into the room via the control panel area fan until I sealed up the holes.


---
**Ben Walker** *June 15, 2016 20:24*

**+Vince Lee** I believe that hole between the brains and the braun is to allow access to the y axis rail end screws.   At least it is a handy spot to attach a cable chain.   


---
**Corey Renner** *June 16, 2016 03:04*

Mine didn't come with that extra fan on the controller side, although the PSU fan is practically a leaf-blower.


---
**Tev Kaber** *July 01, 2016 19:46*

Nice! I don't have a working 3D printer right now, though, so I may try something similar cut from 3mm plywood.


---
*Imported from [Google+](https://plus.google.com/116200173058623450852/posts/cEoFFbCvWdc) &mdash; content and formatting may not be reliable*
