---
layout: post
title: "A while back I asked if anyone had work on the laser with Tagua nuts"
date: December 23, 2017 02:09
category: "Object produced with laser"
author: "Steve Clark"
---
A while back I asked if anyone had work on the laser with Tagua nuts. Here is a ring I made out of one then lasered. It has four small flowers at and just about the limits of detail.  The ring is only 4.75 mm wide and the flower is 3.7 mm wide. The ring is 17.5 inside diameter. It is natural finish and really looks like ivory. 



Next time I have time I’m going to make a little manual indexer to rotate the parts. Flats and crowns are next along with full cut through might be interesting.





![images/9a06ea69a60116f3d901a3b74af89a20.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a06ea69a60116f3d901a3b74af89a20.jpeg)
![images/361ecbb8a271513380dceaa911a3c3f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/361ecbb8a271513380dceaa911a3c3f9.jpeg)

**"Steve Clark"**

---
---
**Ned Hill** *December 23, 2017 15:16*

That's beautiful, nice work.

 


---
**Steve Clark** *December 24, 2017 18:59*

Thank's Ned, Very interesting stuff to work with. Cuts with a band saw and on the lathe also cuts well with a very nice cut surface on the laser. The burn is not deep into the cut side and if one so desires sands off without too much effort.



It's not waterproof but seems to be water resistant. I Rubbed clear wax on the ring to further add resistance. We will see how it holds up. 


---
**Chris Hurley** *December 24, 2017 19:07*

Would you mind sir if I made something based on your work for local sale? (Newfoundland) 


---
**Steve Clark** *December 24, 2017 21:08*

 I don't mine you doing so there. I do plan on selling them here in the US. Please let us see your finished products!



I did cover a sample with super glue but it doesn't seem to be absorbing much into the material. I need to get some of the super thin stuff as my research so far says you can't use acetone or other solvents because they break down the chemical bonds that make up SG not dilute it. 


---
**Cris Hawkins** *December 27, 2017 02:34*

+Steve Clark, what do you have for machines these days? It's been decades since I've seen your shop, but I suspect you have some fun stuff. I have an old Bridgeport Boss CNC with MACH3 upgrade, a manual mill and a couple lathes.



I can't figure out how to send a personal message in this Google system (pretty stupid if you ask me), are you still in the Santa Rosa area?



Cris


---
**Steve Clark** *December 27, 2017 04:41*

**+Cris Hawkins**  No I live in Lodi now... Retired mostly. I have a 3 axis Kent Ser II with a MillPwr control. and a Feeler FTL-618 Precision Toolroom lathe for my home shop. 



Look up 'cg-mechanical Santa Rosa'. then call and ask Jesse for my email. Check with me after the first as I'm going to be out of town the rest of the week. I'll email him it's ok.






---
**Cris Hawkins** *December 27, 2017 06:12*

**+Steve Clark** Thanks Steve, will do. I live north of Seattle now. I'll be in Santa Rosa in January.


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/7dKCGYLPdXt) &mdash; content and formatting may not be reliable*
