---
layout: post
title: "Hi all As I've had the K40 nearly a year - starting to worry about maintenance"
date: December 04, 2018 13:11
category: "Discussion"
author: "Frank Farrant"
---
Hi all



As I've had the K40 nearly a year - starting to worry about maintenance.  



When is it necessary to change the Mirrors in the K40 ?  I clean them regularly, but I'm wondering when they may need renewing.



Also, is it possible to buy EFR Power Supply and Tubes in the UK for the K40 ?    I'm not sure what the difference between normal and EFR but a friend has suggested I get them as they are 'better'.  Is he correct and would you suggest I do so ? 



Also, although I want to use the K40 for the small things I make, I'm interested in a 600 x 400 cutting bed machine and wondered if anyone had any suggestions.  

Is this worth considering ?

[https://www.alibaba.com/product-detail/Cheap-price-Co2-laser-cutting-engraving_60773735969.html?spm=a2747.manage.list.79.66fc71d29vnaNiice](https://www.alibaba.com/product-detail/Cheap-price-Co2-laser-cutting-engraving_60773735969.html?spm=a2747.manage.list.79.66fc71d29vnaNiice)







**"Frank Farrant"**

---
---
**James Rivera** *December 04, 2018 18:13*

That link doesn't really tell you what wattage you're getting. Is it 50, 60, or 80 watts? No way to tell. It does look like a decent machine though (induction sensor end stops, honeycomb bed, air assist).


---
**Frank Farrant** *December 04, 2018 20:33*

**+James Rivera** Hi James

I have a price for 50W and 60W.  

I'm looking for a 600 x 400 cut area and this is where I got my K40D from.  

I've been advised to ask for an EFR Tube and Power Supply (not that I know what that is).  Can you see if that is what is included in the Spec ?


---
**James Rivera** *December 04, 2018 20:58*

**+Frank Farrant** Dude, I don't have any information that you don't, sorry.


---
**Ned Hill** *December 04, 2018 21:36*

If you take care of your mirrors they should last for many years.  You have to keep them relatively clean, so you don't burn in residue, and be careful in cleaning them so they don't get scratched up.  

If you have have original mirrors then it is worthwhile upgrading to some quality Moly mirrors like these from cohesion3d.  Better power throughput and  more robust then the stock mirrors.  

[cohesion3d.com - Molybdenum (MO) Mirror - 20 MM - for K40 Laser](http://cohesion3d.com/molybdenum-mo-mirror-20-mm-for-k40-laser/)


---
**Joe Alexander** *December 04, 2018 21:55*

EFR is simply a specific manufacturer of laser tubes and power supplies and is only a personal preference. The tube and laser PSU that comes in the machine is probably sufficient, especially for hobby use.


---
*Imported from [Google+](https://plus.google.com/110419041310362555971/posts/FtukUyBP6CX) &mdash; content and formatting may not be reliable*
