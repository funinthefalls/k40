---
layout: post
title: "I like the laser cutting head height adjustment in this video at about ."
date: February 24, 2018 18:32
category: "Discussion"
author: "James Rivera"
---
I like the laser cutting head height adjustment in this video at about 
{% include youtubePlayer.html id="j-b3pomqKkI" %}
[4:54](https://youtu.be/j-b3pomqKkI?t=293&t=4m54s).





**"James Rivera"**

---
---
**BEN 3D** *February 24, 2018 19:04*

not bad


---
**Don Kleinschnitz Jr.** *February 24, 2018 19:10*

Pretty common "ramp test"?



Overkill approach:

You can also build a ramp gauge with a know height and length above and along the table. Set this gauge at a known depth below the head. 

Do the math (trigonometry) to determine the correct bed height at any point along the hypotenuse and permanently mark that on the gauge :). 

Burn the line and the value marked on the gauge at the thinnest part is the correct depth.  




---
**James Rivera** *February 24, 2018 19:58*

**+Don Kleinschnitz** Yeah, I wasn't really advocating this as the best ramp test. I just liked the quick focus height adjustment. Seems quicker/cheaper than making a moving z-table, although I'm certain there are good arguments for the adjustable z-table.


---
**Don Kleinschnitz Jr.** *February 24, 2018 22:10*

**+James Rivera** some time back I caved and bought the LO table as I decided it would cost me that much to make one.

I have a " clamp" table design I have parts for and haven't built yet. 


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/eUP4n6DyLnv) &mdash; content and formatting may not be reliable*
