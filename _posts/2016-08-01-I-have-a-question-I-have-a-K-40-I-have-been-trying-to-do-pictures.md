---
layout: post
title: "I have a question I have a K-40 I have been trying to do pictures"
date: August 01, 2016 17:13
category: "Software"
author: "Mike Kvia"
---
I have a question I have a K-40 I have been trying to do pictures. Question is the only program that can be used for pictures is Corel? If I try to just put a picture up and laser it, it looks like crap. Please help thanks





**"Mike Kvia"**

---
---
**Bob Damato** *August 01, 2016 17:27*

I prep my pictures in Photoshop then use laserDRW to print it. They come out pretty good and Im an extreme rookie!


---
**Alex Krause** *August 01, 2016 18:08*

Resample image in gimp/Photoshop to 300-500dpi adjust brightness and contrast. Then Dither your image to get a laser friendly photo... youtube has some tutorials how to accomplish if you are not familiar with the photo editing software


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 21:23*

Here is a tutorial someone recently shared from Epilog for photo engraving: [http://support.epiloglaser.com/article/8205/50104/photo-laser-engraving-on-wood-with-CorelDRAW](http://support.epiloglaser.com/article/8205/50104/photo-laser-engraving-on-wood-with-CorelDRAW)


---
**Robi Akerley-McKee** *August 08, 2016 06:05*

I use a MKS/RAMPS 1.4 setup (on 2 different machines. I use Inkscape for vector and my raster prep.  Pronterface to upload.


---
*Imported from [Google+](https://plus.google.com/111296943589759664756/posts/PGKCaGJXjnT) &mdash; content and formatting may not be reliable*
