---
layout: post
title: "Replacing powersupply on a non-k40 machine - though similar control board etc"
date: March 06, 2018 21:32
category: "Original software and hardware issues"
author: "timne0"
---
Replacing powersupply on a non-k40 machine - though similar control board etc. Works with whisperer.



Yes this is different to the K40 I have. Power supply HY-T50 has arrived (old supply was dead) and I'm trying to put it into an A2 machine that's been donated to the space.



It has a 6C6879-Laser-B1 board.



I can connect the laser tube fine, the items I have problems placing are



K+

24V

GND

5V

L



I'm thinking this power supply needs a separate 24v supply to the B1 board. I have one spare, so will connect 24v/GND off that.



I have left:



TH

TL

WP



Assuming 5V can plug into the same place the other 5V is taken from, should be OK?



K+ and L go into TH?







![images/ac49822381ef1ac31a0397ed00a8651d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ac49822381ef1ac31a0397ed00a8651d.jpeg)
![images/9b2874f62fc791f2f78b77717c053bb8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b2874f62fc791f2f78b77717c053bb8.jpeg)

**"timne0"**

---
---
**timne0** *March 06, 2018 21:43*

D+/D- loops on itself btw.


---
**Don Kleinschnitz Jr.** *March 06, 2018 21:58*

See if this helps. Not the exact same setup as yours but the same supply. 

This supply does not have a DC supply so as you suggest you have to supply a standalone. Your controller would connect similarly to the Nano in the picture below.

 

[photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/GAxUfhdBXoPGG3ga2)


---
**timne0** *March 06, 2018 22:14*

Ah yes thats the one you sent me last time! D'oh


---
**Don Kleinschnitz Jr.** *March 07, 2018 02:08*

**+timne0** I was wondering about that lol...


---
**Stephane Buisson** *March 07, 2018 16:33*

also worth looking that updated link:

[smoothieware.org - laser [Smoothieware]](http://smoothieware.org/laser#example-setup)


---
**timne0** *March 08, 2018 13:00*

Thanks Stephane but it's an original B1 board not smoothie ware.


---
**timne0** *March 08, 2018 13:03*

**+Don Kleinschnitz** laser fires, power supply definitely works as does the control panel switch, but when I plug the pc into the B1 board, I get a connection noise and can install the driver's etc, but just get lots of transmission errors. Tried coral laser with the dongle, it repeatingly flicks between "connected" and "disconnected". Power checked all the lines but nothing is currently helping. Any ideas? If not, I'll assume the board is dead. I did adjust the 24v powersupply once I noticed there were issues from 24.5v to 24v


---
**Don Kleinschnitz Jr.** *March 08, 2018 14:51*

**+timne0** it sounds like you have some form of grounding problem. 

Typical solutions are:

...Insure that all the grounds are connected to one ground. This includes controller, LPS and external supply.

...Shielded USB cable

...Some users have had problems with particular USB hardware in their PC.



Frankly I would get rid of that controller and convert to a C3D. Not all users, but many of us got tired of the CorelWare and lack of Gcode compatibility. 



Just thought, you are running with an external 24V supply. Where are you getting 5V for the controller board?




---
**timne0** *March 08, 2018 15:38*

5v from powersupply.  It is well grounded


---
**timne0** *March 08, 2018 15:38*

I'll try an external


---
**Don Kleinschnitz Jr.** *March 08, 2018 15:44*

**+timne0** you running 5V from the LPS? Probably not enough capacity.


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/jknyqUN9bWH) &mdash; content and formatting may not be reliable*
