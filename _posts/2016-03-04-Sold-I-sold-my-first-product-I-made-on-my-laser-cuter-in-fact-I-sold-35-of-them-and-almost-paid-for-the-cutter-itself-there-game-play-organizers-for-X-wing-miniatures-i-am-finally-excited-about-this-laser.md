---
layout: post
title: "Sold I sold my first product I made on my laser cuter in fact I sold 35 of them and almost paid for the cutter itself there game play organizers for X wing miniatures i am finally excited about this laser!"
date: March 04, 2016 13:29
category: "Discussion"
author: "3D Laser"
---
Sold I sold my first product I made on my laser cuter in fact I sold 35 of them and almost paid for the cutter itself there game play organizers for X wing miniatures i am finally excited about this laser!  

![images/e8fdec8c429c1074b90dd9a3258eb031.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8fdec8c429c1074b90dd9a3258eb031.jpeg)



**"3D Laser"**

---
---
**Jim Hatch** *March 04, 2016 13:55*

Two layers of 1/4" acrylic glued together? Or is that some plywood? The blue one looks like acrylic for the top layer but the bottom layer of the yellow one looks like it's got a wood grain.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 04, 2016 14:00*

That's pretty cool that you're managing to make a return on your investment. Are they made with 2 layers of acrylic? I don't know anything about X-wing miniatures game, but just a suggestion to enhance appeal (& hopefully help you sell more), put some kind of related imagery/design onto the organisers (or text to specify what goes where on your organiser thing). A suggested font (since I assume X-wing game is space related) is a cool one I like called "Space Age" (link here: [http://www.dafont.com/space-age.font](http://www.dafont.com/space-age.font)) or even "Distant Galaxy" (since it seems Star Wars related, link: [http://www.dafont.com/sf-distant-galaxy.font](http://www.dafont.com/sf-distant-galaxy.font))


---
**Francis Lee** *March 04, 2016 15:24*

**+Jim Hatch** that's probably the top of the table...


---
**Jim Hatch** *March 04, 2016 15:34*

**+Francis Lee**​ Ah, that makes sense.


---
**3D Laser** *March 04, 2016 16:48*

Yes it's two sheets of acrylic glued together (got to find a better way to do it though)


---
**3D Laser** *March 04, 2016 16:48*

I also have a guy working on a logo but he is taking his sweet time I wish there is someone who could help me with a few designs 


---
**I Laser** *March 04, 2016 21:56*

Nice work! Looks like you got a good niche there. ;)



What glue are you using? I've got some clear acrylic I need to join, there's apparently an acrylic cement that chemically bonds the pieces together. Being clear I'm worried about glue marks like on your transparent blue board has.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 04, 2016 21:59*

**+Corey Budwine** I had a thought that maybe you could cut some extra holes at strategic locations throughout the design & use other cut "pegs" to join the two layers together by filling those holes.



I have drawn a rough image to show what I mean:

[https://drive.google.com/file/d/0Bzi2h1k_udXwbHdYYmcweWVaUTg/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbHdYYmcweWVaUTg/view?usp=sharing)



In my drawn image, imagine the blue squares are holes cut in both the bottom & top layer of acrylic, at exact same locations.



Then the bright green rectangles are "pegs" that are cut to size to fit perfectly into the blue holes. You could maybe just glue the "pegs" into the holes then, instead of having to glue the entire pieces together (or not even glue them at all if it is a tight enough fit).



Maybe this idea or some modification of it could be of use to you.



Depends how complex the idea/designs are, I could assist on occasion when I have spare time.


---
**3D Laser** *March 05, 2016 01:36*

**+Yuusuf Sallahuddin** i did think about that my issue is my laser isn't 100% dead on and there is still a slight bevel so cutting a peg would not work at this moment I think I am going to stick with solid acrylic that way I can use an apoxy and not have to worry 


---
**Jim Hatch** *March 05, 2016 03:09*

An acrylic glue (it's actually primarily acetone) with a needle applicator works great & mostly invisible.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/EB17h5CyKhM) &mdash; content and formatting may not be reliable*
