---
layout: post
title: "I've got a question. My K40 has a laser head like the one in the picture and I'd like to replace it with one like this: I have 2 questions regarding this upgrade: Should I do it?"
date: February 19, 2017 12:16
category: "Modification"
author: "Eward Somers"
---
I've got a question.



My K40 has a laser head like the one in the picture and I'd like to replace it with one like this:

[http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)



I have 2 questions regarding this upgrade: Should I do it? I've heard that the closed laserhead is much better than my current one.



And, does anyone have any experience with this replacement.? I'm assuming I'll have to print/cut some sort of mount to make it fit.



![images/2df7cd6601115f2ef2afcb621ea3a996.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2df7cd6601115f2ef2afcb621ea3a996.jpeg)



**"Eward Somers"**

---
---
**greg greene** *February 19, 2017 13:22*

I like your head better !  I have the LO one and it works fine but yours appears to be easier to mount the led etc off it.


---
**Alex Krause** *February 19, 2017 14:35*

Can you take a picture of the rest of the machine? Never seen one come stock with linear rails before


---
**Don Kleinschnitz Jr.** *February 19, 2017 14:58*

**+Eward Somers** I have the LO head with modified plate. I also like your head better because:

....The optical path is visually open to and through the objective lens

....Could be easier to modify and make the objective lens snap-in replaceable.

I have always wondered if the lenses stay clean since the air assist is not used to clean them. Others have argued that indistrial cutters use head designs like this.



What vintage machine is this? It looks like the #3 mirror is lower than a K40 relative to the gantry. 



I redesigned my stock K40 head and you can reference that design if you decide to go the LO route. 

[donsthings.blogspot.com - K40 Optical Path Improvements](http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html)


---
**Ulf Stahmer** *February 19, 2017 15:53*

My K40 has the same head. Aligning the mirrors is a bit tricky as you have to hang the post-it from the top of the head for alignment. Otherwise I like the head. It makes lens cleaning a breeze.


---
**Ulf Stahmer** *February 19, 2017 15:57*

**+HP Persson** made an adjustable lens head for his machine of similar style. Search his name in the field on the left and see the cool stuff he's done.


---
**Eward Somers** *February 19, 2017 16:17*

Hmm.. I wanted to go the LO route to easily add an air assist. But I might wait a bit after reading your comments.


---
**Don Kleinschnitz Jr.** *February 19, 2017 16:26*

**+Eward Somers** we never asked why you don't like what you have :)?




---
**Eward Somers** *February 19, 2017 16:27*

**+Don Kleinschnitz**  "Vintage" The machine is 1 year old :p one of the many k40's available from ali.



It is quite unlike most lasers I've seen on this one. Has 3 main switches instead of the one. Several holes in the side panel that seem to be made for big round buttons like emergency stops and square holes meant for a display. All of these conveniently covered up with googletranslateenglish stickers warning me to clean regularly.


---
**Eward Somers** *February 19, 2017 16:28*

**+Don Kleinschnitz** It has no air assist and the mirrors get really dirty like this. My current solution is removing the laser pointer and I screwed a tiny PC fan in its place. 






---
**Don Kleinschnitz Jr.** *February 19, 2017 16:32*

**+Eward Somers** heh .... contamination is what I wondered about and has held me back.

Maybe make a thin clear snap-on acrylic cover that has some positive pressure on it from the same source as and in addition to your air assist tube?


---
**Eward Somers** *February 19, 2017 16:34*

**+Don Kleinschnitz** I'm not really following where you're going with this snap-on acrylic cover? :)


---
**Don Kleinschnitz Jr.** *February 19, 2017 16:49*

**+Eward Somers** I was thinking that the fan may not be effective because the air is not forced around the lenses and there is likely a lot of air movement in the cabinet.

The idea was to put a sealed air duct (shroud/cover) around the lens/mirror with some positive air flow on it. Kinda like the LO air assist does.


---
**Eward Somers** *February 19, 2017 16:51*

Well, it's not perfect, a targeted air assist tube with a nozzle would be better and is what I'm looking into now.. But.. the fan does work to reduce the flames from the wood. Not sure how much it helps keep the smoke away but it's better than nothing


---
**HP Persson** *February 19, 2017 22:27*

Don´t add a nozzle, you will lower the efficiency of the machine. Yes, i have tested it pretty thoroughly with the same machine you are showing :)



And for others who not know theese machines, they are the NEW K40 , often referred to as K40D, better everything, controller is new, rails and guides are new design. Still Corel based controllers though.



This was my solution of air assist, after testing it with three different nozzles and a wide range of pumps and pressure/flows. After this i made a movable head with a small fan + a pipe like this for air assist.

And now building a motorized one, to set focus with the lens holder instead of the bed.

![images/43d3fb627cad8f7de661aea58b52f40a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43d3fb627cad8f7de661aea58b52f40a.jpeg)


---
**Mark Brown** *February 28, 2017 02:03*

**+HP Persson** When you say "lower the efficiency" you mean reduced cutting power?  Not that I'm doubting you, but do you have any idea why that would be?


---
**HP Persson** *February 28, 2017 07:56*

**+Twelve Foot** Yeah, sorry, language barriers, some words in Swedish isnt good to directly translate to eng :)



Nozzles do work, but you add problems to the machines that you didn´t had before. 

You need more air flow, harder to clean the lens, some nozzles are so bad they act as a vacuum cleaner (venturi effect) where you actually make your lens dirtier than just leaving it open (pulling smoke from above down to the lens).



Adding a nozzle to a K40D is making the machine, as a tool, worse.

But if you already have a bigger compressor, have no issues with more cleaning, and fix the gap betweeen top/down part of the lens to prevent vacuum effect it will cut as good as without nozzle.

I can´t just see why adding problems and paying for it is something anyone would do though :)



I added a small 40mm fan blowing around the lens, no smoke reaching the lens at all, and it takes me 5 seconds to clean it :)



The best solution though is to test it, see what works for your setup, start with the easiest solution.

But blindly buying a nozzle just because everyone has one is not the way to go in my opinion.


---
**Eward Somers** *February 28, 2017 08:34*

**+HP Persson** Could you provide with some info on how you upgraded your machine? :) I'm curious where you put your air assist, etc + How do you keep your bed so clean? Mine is a total mess from the residue that's left behind when cutting wood.


---
**HP Persson** *February 28, 2017 08:49*

**+Eward Somers** Hehe, i have made alot of mods to it.

First thing i did was to add fans in front of the bed, to always have movement on the material i was cutting, this moves away all smoke from the lens, and also helps the exhaust, i don´t need a big one.



Air assist is just a small 25L/min pump and a small pipe as shown earlier, aimed to the cut to help keep beam free from smoke and debris.

I do only cut acrylic on mine, so no tips on the wood problem.



I also changed the bed to acrylic with 20mm nails in it, to keep material off the bed, but in correct focus. See pic below.

The black part is my earlier custom movable laser head, right now i´m making V2 (motorized autofocus). There you can also see the small fan blowing around the lens to keep it clear from smoke. A air assist pipe is also there behind.

Fan alone is not air assist, it only keeps lens clean :)

![images/42656fcde1b32c0c7eef4b30a7487b46.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42656fcde1b32c0c7eef4b30a7487b46.jpeg)


---
**Mark Brown** *February 28, 2017 14:01*

**+HP Persson** Alright, I'll keep that in mind. Thanks for the explanation.


---
*Imported from [Google+](https://plus.google.com/101993772584820352944/posts/YVFbrmqSdzu) &mdash; content and formatting may not be reliable*
