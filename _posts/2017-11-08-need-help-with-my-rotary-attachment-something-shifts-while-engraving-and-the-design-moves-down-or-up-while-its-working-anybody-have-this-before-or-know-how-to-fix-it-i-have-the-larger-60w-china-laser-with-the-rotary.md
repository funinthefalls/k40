---
layout: post
title: "need help with my rotary attachment, something shifts while engraving and the design moves down or up while its working, anybody have this before or know how to fix it, i have the larger 60w china laser with the rotary"
date: November 08, 2017 18:38
category: "Original software and hardware issues"
author: "Mike Jankowski"
---
need help with my rotary attachment, something shifts while engraving and the design moves down or up while its working, anybody have this before or know how to fix it, i have the larger 60w china laser with the rotary attachment that the 3 rotating bard the object sits on. As you can see at the bottom of the A it shifted downward and the last 2 letters are slightly lower. Thanks

![images/d2eff2ca9f9f38d0e79300a4527caaca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d2eff2ca9f9f38d0e79300a4527caaca.jpeg)



**"Mike Jankowski"**

---
---
**HalfNormal** *November 12, 2017 15:09*

It looks like the bottle is "walking". It is shifting while rotating. It could be one of a multitude of issues. You need to watch a test run and see when and where this is happening.


---
*Imported from [Google+](https://plus.google.com/108409488968890912770/posts/CJwaDvKfRyM) &mdash; content and formatting may not be reliable*
