---
layout: post
title: "SINGLE CUT SETTING? Hi all, can anyone tell me how to cut just once in LaserDRW?"
date: September 22, 2015 14:00
category: "Software"
author: "Brendan Power"
---
SINGLE CUT SETTING?

Hi all, can anyone tell me how to cut just once in LaserDRW? No matter what I try, my machine cuts the same path twice. I've tried setting the Repeat number to 0 in the cutting/engraving settings but the box is greyed out and set at one repeat. 



It looks as if you can change to 0 but it doesn't get saved, and when you press Start it reverts to the one repeat. Any help appreciated,



BP 





**"Brendan Power"**

---
---
**Joey Fitzpatrick** *September 22, 2015 14:08*

Set you lines to hairline and make sure any areas that are going to be cut out have a solid infill.  LaserDRW behaves differently than most laser cutter software.  


---
**Sean Cherven** *September 22, 2015 14:35*

Joey is correct. Set lines to hairline, or the thinnest u can make them. And also make sure they are solid infill.


---
**Brendan Power** *September 22, 2015 15:26*

Thanks for the replies. Re. The 'hairline setting', is that an instruction, and where is it? If you have a screenshot of the recommended setting and dialog box I'd appreciate it. 



On the solid infill, that's tricky for my designs as they are derived from CAD files of complex shapes. I wish I could import them as dxf files but that's not possible in LaserDRW. So I export them as bitmaps from my CAD software and then get the correct size  by referencing them against a dimension line that I draw in LaserDRW, with some trial and error.



Its rather clunky but since LaserDRW won't import dxf files directly it's the only way I can get what want cut to the right dimensions. To go through and infill every single enclosed area in these small complex designs would be a nightmare, as I'd have to trace over the design with small boxes and other shapes in LaserDRW, well nigh impossible to do accurately and VERY tedious. 



Even though the process above seems rather Neanderthal it does give me parts the correct size, it's just that the machine insists on cutting twice. 



BP


---
**Brendan Power** *September 22, 2015 15:40*

BTW I'm not using Corel Draw/ Corel Laser, if that's where the Hairline setting is. I read that they won't work with Windows 8.1, which I have on my laptop. In any case I now have a method of getting my designs cut to the right size in LaserDRW (above).



So really I'm asking for the setting in LaserDRW itself to ensure there is only one cut. I'm cutting delicate parts from 1-1.5mm plastic and want to get the cuts as thin and clean as possible. BP 


---
**Sean Cherven** *September 22, 2015 19:52*

The hairline setting is the line width. Click on the line, and look at its properties.



DXF can not be imported into laserdraw, however, it can be in CorelDraw.



Which reminds me, CorelDraw w/CorelLaser will work on Windows 8.1, as that's what I use, and it works fine for me.


---
**Sean Cherven** *September 22, 2015 19:53*

However, you're problem is more likely the solid infill. The software wants to cut on both sides of the black line.


---
**Brendan Power** *September 22, 2015 21:37*

Thanks for your suggestions! I'll try them tomorrow 


---
**Andreas Horn** *September 23, 2015 21:22*

**+Brendan Power** I had a similar behavior while using WMF (Windows Meta File) as cutting/transfer option. (You can use it as fileformat without problems, but on Corel Laser, there is in option how the file should be transfered to the lasercutter, there you can choose between wmf, hpgl and others. HPGL works best for cutting)



Btw. Corel Draw / Corel Laser works fine for me on Windows 8.1 (64bit) and Windows 10 (32bit) - I didn't try other versions.


---
**Brendan Power** *September 23, 2015 21:40*

'Filling holes' in an imported bitmap design sounds like the only thing that you guys say would work, but my designs have LOTS of small holes, and I could only fill them by tracing around them in LaserDRW. As I said earlier, that's not practical or possible with any accuracy, so I shan't be trying it. Are there really no other options in LaserDRW to make the machine do only one pass down a line?


---
**Brendan Power** *September 23, 2015 23:45*

Ah... The penny is starting to drop! I'm in bed now but can visualise what you mean, I think it will work. Thanks! Will try and send the test file first thing tomorrow (I'm in England). Brendan


---
**Brendan Power** *September 24, 2015 17:45*

I did the infill thing as Almost Gone and others suggested. It does stop the second pass, you're right. However for what I was cutting (thin plastic), doing a slower single cut was worse than a faster double cut, as the heat buildup warped the plastic, plus created more smoke. So I reverted to the open drawing with just lines, no infill. But I learned a couple of things, so it's all good. Thanks people, this group is excellent :)


---
**Linda Barbakos** *November 30, 2016 09:00*

**+Joey Fitzpatrick** Sorry to resurrect such an old query but after months of neglecting my laser, I've been welcomed back with this same issue. I just want to understand if the infill setting is done in CorelDRAW or does my file need to be infilled before I import it into CorelDRAW i.e. in Illustrator. Thanks.


---
**Brendan Power** *December 07, 2016 06:39*

After grasping the concept with the help of this group I've been using my laser regularly with excellent results. I use Dketchup for design. I do the infill there, export as a Bitmap, resize in LaserDRW and cut. It works brilliantly. The only slightly annoying aspect is the need to resize to the correct dimensions in LaserDRW, but it only takes a minute.


---
**Eric Raymond** *December 27, 2016 20:27*

Could someone please explain "solid infill", I'm having the same problem.  Are you talking about the individual line, or the whole drawing?  How do you do that?  I've tried looking it up in help, no bueno. Thanks


---
*Imported from [Google+](https://plus.google.com/111366654976766633425/posts/dLjxuWbVMTq) &mdash; content and formatting may not be reliable*
