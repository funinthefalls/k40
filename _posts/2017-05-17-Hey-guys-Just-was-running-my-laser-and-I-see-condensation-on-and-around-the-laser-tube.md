---
layout: post
title: "Hey guys, Just was running my laser and I see condensation on and around the laser tube"
date: May 17, 2017 23:43
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Hey guys,

Just was running my laser and I see condensation on and around the laser tube. I've never noticed this before and wanted a bit of guidance before I panicked too much. Any ideas what could be the problem?



![images/1d7414c734923e74a9d90bfa7d5d11eb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1d7414c734923e74a9d90bfa7d5d11eb.jpeg)
![images/6953d32161ad626364ead5d7e64c283e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6953d32161ad626364ead5d7e64c283e.jpeg)
![images/5948e321e3908d33a769b4cb1c4f7aa0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5948e321e3908d33a769b4cb1c4f7aa0.jpeg)

**"Nathan Thomas"**

---
---
**Alex Krause** *May 18, 2017 00:31*

How are you chilling your water?


---
**Nathan Thomas** *May 18, 2017 01:06*

5 gallon bucket with ice packs. The lowest the temp got was 19C...but I was running the laser and it got up to 24-25...normal temp. So I'm lost as to what's happening and why


---
**Alex Krause** *May 18, 2017 01:10*

You are lowering the temperature of your tube below the dew point causing condensation


---
**Ned Hill** *May 18, 2017 01:22*

Yeah and the dew point will depend on the temp, humidity and atmospheric pressure where the laser is located.


---
**Nathan Thomas** *May 18, 2017 01:32*

And I have no idea what it is.

So I guess I'll have to replace the laser tube. Or could it be something else?


---
**Alex Krause** *May 18, 2017 01:37*

You would be better off running at 24-24c without the ice packs than at 19c with condensation 


---
**Alex Krause** *May 18, 2017 01:38*

If you are doing this in your home in the basement or in an inclosed room you could just add a dehumidifier and dry out the room first


---
**Nathan Thomas** *May 18, 2017 01:39*

That's a good idea I'll try it next time. 


---
**Nathan Thomas** *May 18, 2017 01:40*

I didn't even notice it until I heard and saw sparks out the back where the tube is. Any ideas whether the tube is blown or could it be another problem?


---
**Alex Krause** *May 18, 2017 01:41*

You were arcing to ground


---
**Nathan Thomas** *May 18, 2017 01:56*

Not sure. When I test fire I see and hear a small spark.  Is that it?


---
**Don Kleinschnitz Jr.** *May 18, 2017 02:35*

**+Nathan Thomas** where do you see the spark. Turn out the lights and take a video.


---
**Nathan Thomas** *May 18, 2017 03:13*

I guess I have to create a new thread now. Just tried to, now the laser isn't firing at all. No sparks just a low buzz as the head goes back and forth but not firing **+Don Kleinschnitz**​


---
**Steve Clark** *May 18, 2017 03:16*

Mabe one of these would help?

Someone here bought one but I don't recall who it was.



[ebay.com - Details about  Multifunctiona<wbr/>l Digital Thermo-Hygrome<wbr/>ter Thermometer Humidity Dew Point Meter](http://www.ebay.com/itm/Multifunctional-Digital-Thermo-Hygrometer-Thermometer-Humidity-Dew-Point-Meter-/192183248045?hash=item2cbf03a4ad:g:hAgAAMXQqWNSKBLr)


---
**Phillip Conroy** *May 18, 2017 07:19*

It was me ,lost all cutting  power one  night ,next day  all fine ,winter in Australia and at night did not have heater on.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/TJN8my7X3h7) &mdash; content and formatting may not be reliable*
