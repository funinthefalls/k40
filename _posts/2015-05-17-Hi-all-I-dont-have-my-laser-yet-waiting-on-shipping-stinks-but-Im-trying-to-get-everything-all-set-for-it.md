---
layout: post
title: "Hi all, I don't have my laser yet, (waiting on shipping stinks), but I'm trying to get everything all set for it"
date: May 17, 2015 23:26
category: "Modification"
author: "Eric Parker"
---
Hi all, I don't have my laser yet, (waiting on shipping stinks), but I'm trying to get everything all set for it.  I've ordered a new air assist head from lightobject, and an 18mm lens to go with it, I have the hose and compressor for the air assist, a 5 gallon bucket with lid, 3 gallons of distilled water and a jug of automotive coolant, laser safety goggles, and a whole mess of switches (for interlocks).



What I'm really wondering about is the 10k wirewound multiturn pot that I ordered to replace the stock pot.  I was lookign at datasheets on laser power supplies and they generally specified a 5k-10k pot, but it looks like these use a 1k pot.  Is the 10k going to be an issue?

from what I understand it's just being used as a voltage divider as a reference for the output power of the machine, so personally, I wouldn't think it too terribly critical.





**"Eric Parker"**

---
---
**Stephane Buisson** *May 18, 2015 09:44*

Welcome here Eric,

You will be able to start in a snap with all that. saving a lot of time by doing only one miror alignement and so on... ;-))



I am glad this community is able to help, thx for reading.


---
**Tim Fawcett** *May 18, 2015 16:25*

The 10K Pot won't be a problem - that is what I use on mine after the upgrades I did


---
**Eric Parker** *May 18, 2015 18:27*

Well that's good to know.  I was considering a PWM control, but after learning that the reason it needs a 20kHz PWM is because it doesn't smooth it, but rather pulses the laser at full current, I decided this is a better option.


---
**Troy Baverstock** *May 19, 2015 03:28*

On a side note, do you know if it needs the 2w handling if it's just a reference voltage? I want to run a dual motorised pot from a micro controller, but can't seem to find one rated above about .5w


---
**Eric Parker** *May 19, 2015 03:56*

I shouldn't think it would need the 2 watts. 

P=E²/R.



so, with your half watt pot, lets assume 10k, we know volts(E), and we know resistance(R);

(5)²/10,000 =  0.0025 watts, or 2.5mW.  In other words, you're fine.


---
**Eric Parker** *May 19, 2015 05:42*

well, I mean, on the lower end you are looking at 2.5 watts if you have it turned to like 10 ohms, but I would imagine theres some sort of resistor involved elsewhere.


---
**Troy Baverstock** *May 19, 2015 06:33*

Cheers, that means I get my mico control, presets, precise settings and remote operation without really modifying the existing electronics. 



So you are basing it off a 5v level? I suppose I could measure the current through a multimeter then.


---
**Eric Parker** *May 19, 2015 09:24*

I am basing it off a 5v level, yes.  That's what the laser PSU seems to be putting out.


---
**Tim Fawcett** *May 19, 2015 11:07*

It doesn't need the 2W rating - I am using a 100mW cermet trimmer. Remember that the power supply will produce more power than the laser tube will handle. I have my pot set to a maximum of about 50%. This gives a maximum laser current of around 18mA which is the upper limit for the tube. Any more than that and you shorten the life of the tube significantly


---
*Imported from [Google+](https://plus.google.com/+EricParkerX/posts/GB5vjwpNumz) &mdash; content and formatting may not be reliable*
