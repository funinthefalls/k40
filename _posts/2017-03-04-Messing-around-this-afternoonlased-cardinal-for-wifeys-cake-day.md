---
layout: post
title: "Messing around this afternoon...lased cardinal for wifey's cake day"
date: March 04, 2017 02:44
category: "Object produced with laser"
author: "Mike Meyer"
---
Messing around this afternoon...lased cardinal for wifey's cake day

![images/f062a058852aa35f196acae8ddc298a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f062a058852aa35f196acae8ddc298a9.jpeg)



**"Mike Meyer"**

---
---
**Don Kleinschnitz Jr.** *March 04, 2017 04:42*

Very nice!


---
**Ned Hill** *March 04, 2017 13:33*

Very Nice.  Did you do the branch as an inlay into the bird or is it all one piece?


---
**Mike Meyer** *March 04, 2017 14:08*

Everything but the etching for the feathers was cut individually, painted and then glued onto a backer board that I cut from the outline of the bird and branch. BTW, 3mm birch plywood was used.


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/9uZY8Z2XqWW) &mdash; content and formatting may not be reliable*
