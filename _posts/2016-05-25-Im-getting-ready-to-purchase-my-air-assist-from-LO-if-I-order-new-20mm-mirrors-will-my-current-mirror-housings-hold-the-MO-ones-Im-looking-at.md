---
layout: post
title: "I'm getting ready to purchase my air assist from LO if I order new 20mm mirrors will my current mirror housings hold the MO ones I'm looking at..."
date: May 25, 2016 05:13
category: "Modification"
author: "Alex Krause"
---
I'm getting ready to purchase my air assist from LO if I order new 20mm mirrors will my current mirror housings hold the MO ones I'm looking at... I know I have to get a new lens for sure but is a mirror upgrade absolutely needed?





**"Alex Krause"**

---
---
**Phillip Conroy** *May 25, 2016 06:04*

Should fit,i have worn out 2 mirrors so far and 2 focal lens with about 300 hours cuttimg 3mm mdf,banggood is cheap for mirrors,ebay for focal [lens.it](http://lens.it) pays to bave spares on hand


---
**Alex Krause** *May 25, 2016 06:08*

MDF is pretty harsh with bonding agents and formaldehyde. I would rather go with trusted source lens and mirrors rather than rolling the dice and getting crappy Chinese optics 


---
**Anthony Bolgar** *May 25, 2016 06:10*

The standard housing fits my 20mm X 3mm thick MO mirrors just fine. It makes a significant difference in transmitted laser power over the cheap gold plated stock mirrors. They also last longer than the cheap mirrors, cleaning the mirrors destroys the gold plated ones pretty quickly.


---
**Alex Krause** *May 25, 2016 06:13*

**+Anthony Bolgar**​ Recommended optics from LO to go with my air assist not sure which lens to order. I know what mirrors I want


---
**Phillip Conroy** *May 25, 2016 06:16*

I know mdf is harsh,you should see my laser tube ,it is covered in the sticky stuff,i have now a 250watt 8inch exhust fan and 10psi air assist that keeps it away form mirrors and focal lens now,be aware that aif assist may introduce moisture on focal lens-before i added 2 water traps and silica gel in a 1inch x 1meter pvc pipe just before the air assist .i was cleaning focal lens every 20 min of cutting ,now once a week


---
**Anthony Bolgar** *May 25, 2016 06:21*

The lens I chose was this 18mm lens from Lightobject.com

[http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx](http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx)

It is reasonably priced at $35.00 and I am impressed with its quality. Much better than the old stock 12mm lens.


---
**Alex Krause** *May 25, 2016 06:23*

**+Phillip Conroy**​ I think I remember suggesting you use silica dessicant what do you use as your air source?


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/aJ3qQBnCKbz) &mdash; content and formatting may not be reliable*
