---
layout: post
title: "Got MK4Duo working on my K40 with PWM modulation (which it wasn't doing before) but I'm getting a weird ghosting effect on the X (300mm) axis when raster engraving"
date: January 06, 2017 08:15
category: "Hardware and Laser settings"
author: "timne0"
---
Got MK4Duo working on my K40 with PWM modulation (which it wasn't doing before) but I'm getting a weird ghosting effect on the X (300mm) axis when raster engraving.  Normal cutting is absolutely fine.  An y ideas what could be causing the raster problems?





**"timne0"**

---
---
**timne0** *January 06, 2017 08:34*

OK so it appears having read through a lot of posts that it could be either the mirror alignment to the centre of the lens or alternatively I've got the lens the wrong way up.  I'm about to install an entirely new lens (19mm) with an air assist nozzle so will check all of that.


---
**Ned Hill** *January 06, 2017 19:43*

Note that for a plano-convex lens the flat side goes down.

![images/ed59203d72176242b6856761a67771ee.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/ed59203d72176242b6856761a67771ee.gif)


---
**Andy Carter** *December 16, 2017 17:31*

Hi Tim. I'm struggling to get MK4Duo  compiling for my K40.  Any chance I could get a copy of your  config  files?


---
**timne0** *December 16, 2017 18:37*

[wiki.cheltenhamhackspace.org - K40 Laser Cutter - Cheltenham Hackspace Wiki](https://wiki.cheltenhamhackspace.org/wiki/K40_Laser_Cutter) read that, I've done my best to explain! (Last bit at the bottom)


---
**Andy Carter** *December 17, 2017 07:24*

what version of the IDE did you use to compile and upload the firmware?




---
**timne0** *December 17, 2017 08:29*

For Mk4duo? I think the latest. It was only when I was looking at old versions of the software which had previously worked I needed to roll back the IDE. I gave up on Mk4duo because it was never stable. I moved to Cohesion3D board because it's mostly plug and play, more dynamic development and a working version. Then my tube went bang... Now fitting a 50w.


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/6e7wJefBZAo) &mdash; content and formatting may not be reliable*
