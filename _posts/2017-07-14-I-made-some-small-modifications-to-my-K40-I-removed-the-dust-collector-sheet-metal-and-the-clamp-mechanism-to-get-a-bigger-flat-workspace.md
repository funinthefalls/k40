---
layout: post
title: "I made some small modifications to my K40: - I removed the dust collector sheet metal and the clamp mechanism to get a bigger flat workspace"
date: July 14, 2017 18:36
category: "Modification"
author: "Claudio Prezzi"
---
I made some small modifications to my K40:

- I removed the dust collector sheet metal and the clamp mechanism to get a bigger flat workspace.

- I've cut back the rubber tube on Y-Axis to get 20mm more travel.

- The available workspace is now 320 * 220 mm.﻿

![images/c840dec47b6240a0d31eb7fdbade3473.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c840dec47b6240a0d31eb7fdbade3473.jpeg)



**"Claudio Prezzi"**

---
---
**Joe Alexander** *July 14, 2017 19:11*

I did the same and was able to squeeze out 320 x 230mm. I also removed the aluminum plate and use the cheap honeycomb on a manual lab jack for my bed and a focus "shim" to check height.


---
**Anthony Bolgar** *July 14, 2017 19:46*

That was one of the first mods I made to my K40's, I get 320X230 on each one.


---
**Jorge Robles** *July 14, 2017 20:14*

**+Claudio Prezzi** Could you take a photo of the cutted rubber? Thanks!


---
**Claudio Prezzi** *July 15, 2017 09:03*

**+Joe Alexander** **+Anthony Bolgar** You are right, the rubber tube could be cut even more to get additional 10mm in Y.


---
**Claudio Prezzi** *July 15, 2017 09:06*

**+Jorge Robles** I will take a picture of the rubber tube later. The tube is on the left Y shaft in the front and works as a mechanical endstop.


---
*Imported from [Google+](https://plus.google.com/+ClaudioPrezzi/posts/VKW2p95ChFs) &mdash; content and formatting may not be reliable*
