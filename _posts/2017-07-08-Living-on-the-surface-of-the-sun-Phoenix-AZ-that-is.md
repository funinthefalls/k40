---
layout: post
title: "Living on the surface of the sun, Phoenix, AZ that is"
date: July 08, 2017 17:50
category: "Discussion"
author: "bbowley2"
---
Living on the surface of the sun, Phoenix, AZ that is.  I can't make enough ice to keep my K40 cool so I bit the bullet and ordered a water chiller.

[https://www.amazon.com/gp/product/B01M64SBZC/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01M64SBZC/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)





**"bbowley2"**

---
---
**Anthony Bolgar** *July 08, 2017 19:04*

This will only bring the water temp down to the ambient room temp, it is not a real cooler. The CW5000 is the one you should be getting.


---
**Steve Clark** *July 08, 2017 19:15*

I read the info on it and it is deceiving...I would also be concerned about it.  You might want to have Amazon cancel the order. 



This is the lease expensive one I've run across that is a true freon R134A chiller. 



[http://www.lightobject.com/Lightobject-600W-Mini-Water-Chiller-for-CO2-laser-machine-AC110V-60Hz-P999.aspx](http://www.lightobject.com/Lightobject-600W-Mini-Water-Chiller-for-CO2-laser-machine-AC110V-60Hz-P999.aspx)


---
**Todd Miller** *July 08, 2017 22:17*

I would agree with Anthony, I have this LightObject chiller

and it's a true refrigeration unit, the item you are looking at is not.



I use it with 100W Reci tube and a K40 unit.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/DUDa2bCLZKf) &mdash; content and formatting may not be reliable*
