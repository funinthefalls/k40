---
layout: post
title: "My first real product cut on the laser"
date: July 08, 2016 23:16
category: "Object produced with laser"
author: "Carl Fisher"
---
My first real product cut on the laser. The first in a line of pen stands for my custom fountain pens. 3mm acrylic that is just screaming for a base with some LEDs to make it glow. 



![images/a365a32787a1ec18d425b9e04a3c6cb5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a365a32787a1ec18d425b9e04a3c6cb5.jpeg)
![images/850e1f8bff62174b2d160b508b7a0fca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/850e1f8bff62174b2d160b508b7a0fca.jpeg)

**"Carl Fisher"**

---
---
**Carl Fisher** *July 08, 2016 23:17*

It took quite a few test joints to get the mesh right. Are there any proven joints out there that would make this process easier to adapt to materials that can vary slightly in thickness?  This was "3mm" plexi but really was about 2.7 on average with some sheets slightly thicker and some slightly thinner. This makes it very difficult to produce multiple copies without tweaking the opening for the joint every time.




---
**Jim Hatch** *July 08, 2016 23:33*

Were you using Cast Acrylic? The uniformity of that stuff is usually better. Also, not sure what you used for the design software but if you use one with parametric design capability (I think there's a plugin for Inkscape if that's what you're using). Then you could just enter the material thickness and let it adjust the joints for you.


---
**Carl Fisher** *July 09, 2016 01:33*

I don't know to be honest. I was given a bunch of scrap, half of which is actually bigger than will fit in the bed without precutting pieces. It's paper backed and seems to have a tollerance of about .02 give or take between sheet thickness.




---
**Carl Fisher** *July 09, 2016 01:34*

As for the software, I am starting to use LibreCAD more than anything else.




---
**Jim Hatch** *July 09, 2016 01:50*

There's support for parametric modeling in Librecad. Not sure what they call it but I've seen comparisons with AutoCAD that said they both have it. Worth a lookup.



PD is great for things that you don't necessarily want to scale at the same rate as other parts. Typically it's things like holes you've speced for a specific size screw or bolt. You only want those to scale in specific increments to match actual screw sizes.



Or you're doing finger joints - the depth of the finger changes with the thickness of the material not with the size of the object otherwise they won't slot together right.


---
**Carl Fisher** *July 09, 2016 02:01*

Looks like there was a feature request for Parametric in Libre but possibly not implemented yet. [https://github.com/LibreCAD/LibreCAD/issues/403](https://github.com/LibreCAD/LibreCAD/issues/403)


---
**Jim Hatch** *July 09, 2016 02:20*

I think that was a few years ago. The AutoCAD comparison I saw was more recent. I don't know what doc files you get with Libre but it would seem to be worth some searches in it.


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/cdgtH4gWdwL) &mdash; content and formatting may not be reliable*
