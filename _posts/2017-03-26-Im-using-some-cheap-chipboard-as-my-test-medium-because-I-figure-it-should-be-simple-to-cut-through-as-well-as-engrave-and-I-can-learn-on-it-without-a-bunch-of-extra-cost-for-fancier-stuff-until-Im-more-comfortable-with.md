---
layout: post
title: "I'm using some cheap chipboard as my test medium, because I figure it should be simple to cut through as well as engrave and I can learn on it without a bunch of extra cost for fancier stuff until I'm more comfortable with"
date: March 26, 2017 05:10
category: "Materials and settings"
author: "Bob Buechler"
---
I'm using some cheap chipboard as my test medium, because I figure it should be simple to cut through as well as engrave and I can learn on it without a bunch of extra cost for fancier stuff until I'm more comfortable with the software and the mechanics. 



So far, I've been able to engrave on it with no problem, but cutting has actually proven trickier than I expected because to get any kind of depth per pass, I need to be moving pretty slow and running the laser at a fairly high power, which just scorches the chipboard. 



Should I really need multiple passes on thin (1.45mm), unfinished cardboard? 



Product link:  [https://www.amazon.com/dp/B00161W6L8/](https://www.amazon.com/dp/B00161W6L8/)



My K40 is the digital panel version so this coming week I'll be installing an ammeter and spending some quality time dialing in the alignment just in case that's my issue, but I've measured to ensure the chipboard is already at the correct height, and I am getting a nice, clean and small dot when I test fire on it, so I'm not sure how much more I'll improve it via alignment testing.



At least with the ammeter I'll be able to see what my power really looks like. 



Thoughts?





**"Bob Buechler"**

---
---
**Phillip Conroy** *March 26, 2017 06:48*

Cardboard is hard to cut,3mm thick mdf or plywood  should cut at 11/mm second  and 10 ma


---
**Bob Buechler** *March 26, 2017 07:25*

**+Phillip Conroy** thanks for the tip on the mdf and ply. When you say cardboard is hard to cut, do you mean it's difficult to find the right blend of settings, or that the K40 struggles to cut it?


---
**Jim Hatch** *March 26, 2017 13:04*

**+Phillip Conroy** I use this chipboard all the time for my test runs before doing a project with acrylic or Baltic Birch. It's the same stuff that is used to back a pad of paper. 



**+Bob Buechler** have you been able to cut other materials? How is your alignment? Sure your lens is in correctly? Can you cut MDF or acrylic or Baltic Birch?


---
**HalfNormal** *March 26, 2017 16:55*

If you are not using air assist then cutting chipboard will be a problem due to wanting to burn instead of cut.


---
**Bob Buechler** *March 26, 2017 17:06*

**+Jim Hatch** I haven't wanted to graduate to anything else until I figured this out. Just to avoid any confusion, but more data is better than less, so I'll give it a try.



Question: what speed and power do you use when testing your cut jobs on this stuff?


---
**Bob Buechler** *March 26, 2017 17:11*

**+HalfNormal** Learned this the hard way. My ventilation fan is strong and I swapped the bed out for a simple sheet of expanded steel, both of which seem to have reduced flare ups to almost none. 



I bought the LO air assist head but didn't realize it needed an 18mm lens so that arrives Tuesday. Trouble there is that it changes the height of the lens slightly compared to the stock head, so I'm reluctant to introduce it til I figure out why I can't easily cut chipboard. 










---
**HalfNormal** *March 26, 2017 17:16*

**+Bob Buechler**​ you can make a spacer for the stock lens to work. If you have access to a 3d printer there is one on thingverese. 


---
**Bob Buechler** *March 26, 2017 17:21*

Yeah, I printed one. Just didn't want to add more variables, ya know?


---
**HalfNormal** *March 26, 2017 17:26*

No worries. It does take time to figure it all out. That to me was half the fun of ownership! Others might call it a curse!


---
**Bob Buechler** *March 26, 2017 17:46*

**+HalfNormal** Oh definitely. For me it was half that and half not being able to justify to myself the cost of a glowforge as an entry point to the hobby. At least this way, when I've become baseline functional, I'll have a pretty decent understanding of the systems involved. 


---
**Jim Hatch** *March 26, 2017 17:48*

Make sure the bed is 50.8mm from the lens - if you're using the stock lens that'll be the bottom of the lens housing. If you're using the LO air assist head the lens is up where the knurled ring is not the bottom of the head assembly.


---
**Bob Buechler** *March 26, 2017 17:51*

**+Jim Hatch** I saw a trick where someone measured and cut a Q-Tip to 50.8 (2"), and used that to measure from the bottom of the lens downward. My bed is aligned using this method to half the depth of the chipboard.


---
**Jim Hatch** *March 26, 2017 18:02*

You need to make sure it's the right height for cutting. Engraving can work with it out of focus but cutting won't ever work.


---
**Bob Buechler** *March 26, 2017 18:06*

I believe it is. Even on the stock head, the lens sits back into the housing a bit. So to avoid any misinterpretation, measured from the lens itself to the bed, 50.8mm. 


---
**Jim Hatch** *March 26, 2017 18:33*

Is the lens right side up (bump up)? You should be able to cut chipboard at 18ma and 15mm/s. There's some variation on the tubes (mine is really outputting 38W but I've seen some at 30 or 32W) so you may have to slow down to 10 or even 5mm/s but it will cut in one pass. If you don't have air assist take 2 passes at a faster speed to reduce the chance of it flaming.



I cut a spacer that fits between my nozzle and the bed for common sized materials and another that is 50.8mm that I can measure from the ring. Makes it easy to get the right height. I'm building the Scorch spring loaded bed which will make it even easier to get the height right.


---
**Bob Buechler** *March 26, 2017 18:46*

**+Jim Hatch** I don't have any metalworking tools, or I'd make a Scorch bed too :/. Great design though. Good idea on the spacers. Can you maybe post a pic or two of your spacers in use? I'd like to make my own, and want to make sure I am measuring from the right spots (still not convinced I'm doing it right). 



I'll give those speeds / power a try. Though my digital panel is measured only in percentage. The soon-to-be-installed ammeter is measured in mA, but I don't know how to determine what the true wattage of my tube is.


---
**Bob Buechler** *March 26, 2017 22:57*

**+Jim Hatch** welp, there is definitely something off. Ran a bunch of test jobs between 300 and 900mm/min, between 60 and 80% power and between 1 and 3 passes. No combinations cut through the chipboard. First thing I'll check is that the mirror isn't backwards (should be curved side up), and that the mirrors are aligned. I'm pretty sure the height is right.


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/hwUtv7nf9FY) &mdash; content and formatting may not be reliable*
