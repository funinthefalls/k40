---
layout: post
title: "So, playing around with 3d models & Autodesk 123d Make to split models into 3d slices to combine together"
date: April 10, 2016 01:36
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So, playing around with 3d models & Autodesk 123d Make to split models into 3d slices to combine together.



Used a basic downloaded model of a human head, hollowed it out, radial slices (4 horizontal, 20 vertical) originating from the centre of the model.



Cut it out of 3mm ply, which turns out is not quite 3mm (I think more like 2.7mm) as things are loose in the 3mm slots (unless it's kerf width issue). Either way, would glue the piece anyway, so doesn't really matter. This was actually just tied with pieces of string as I put it together, to stop the pieces falling out as I added extra pieces.



More slices on the vertical would give a much nicer & more full 3d shape, but would also use more material (which I ran out of until I go hardware store again).



So, firstly I marked the images (with all the numbers) by using cut @ 10mA @ 50mm/s. This just gave a light burn into the top to show which piece connects to which other piece at which spot.



Then I cut it all, 10mA @ 12mm/s. There is a lot of material wastage in this, as I hollowed the model. Used 11 pieces of 300mm x 200mm ply to complete this. Basically 2/3rds of a 1200 x 810mm sheet (~au$11).



One thing I'd note is because I chose to radially slice the model, the ears kind of disappeared. So would need to create some ear attachments that hook onto the little bits coming off the side.



![images/5a22386f7444aa6cf08c5eeeba97f1a5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a22386f7444aa6cf08c5eeeba97f1a5.jpeg)
![images/4e48d75908246d6148d2ba34c901642f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e48d75908246d6148d2ba34c901642f.jpeg)
![images/a0a504114a438b610fadff62cfc2ae48.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0a504114a438b610fadff62cfc2ae48.jpeg)
![images/9b17ef90b7e902b571ae3dab5d7ae2cb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b17ef90b7e902b571ae3dab5d7ae2cb.jpeg)
![images/8e5afab12c2114825fe5fc3de0cf6e03.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e5afab12c2114825fe5fc3de0cf6e03.jpeg)
![images/afeeafe0ac27c4363f3eda0389e7498a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afeeafe0ac27c4363f3eda0389e7498a.jpeg)
![images/df34be95e012826d17ed13f87e33fab2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/df34be95e012826d17ed13f87e33fab2.jpeg)
![images/68a491971b8c661be743416d6869ac9f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/68a491971b8c661be743416d6869ac9f.jpeg)
![images/3f29e8a62ba097853791d1a1b083e808.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f29e8a62ba097853791d1a1b083e808.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Jarrid Kerns** *April 10, 2016 02:13*

Cool! 


---
**Scott Thorne** *April 10, 2016 03:04*

**+Yuusuf Sallahuddin**...where are you downloading the models? 


---
**Josh Rhodes** *April 10, 2016 03:40*

Nicely done.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 09:20*

**+Scott Thorne** Most of the better models I downloaded were from a place called TurboSquid. They seem to have a better selection of higher quality models. I have a whole bundle of other links I found for free 3d models also.



[http://www.turbosquid.com/Search/3D-Models/free](http://www.turbosquid.com/Search/3D-Models/free)

[http://tf3dm.com/](http://tf3dm.com/)

[https://www.cgtrader.com/free-3d-models](https://www.cgtrader.com/free-3d-models)

[https://clara.io/library](https://clara.io/library)

[http://archive3d.net/](http://archive3d.net/)

[http://3dsky.org/](http://3dsky.org/)

[https://www.creativecrash.com/3dmodels](https://www.creativecrash.com/3dmodels)





Be aware, one of these sites when I downloaded some of the files, Windows Defender flagged the zip/rar as containing a virus of some sort. I can't remember which site it was, so scan anything with virus scanner or have virus scanner active while downloading.


---
**Scott Thorne** *April 10, 2016 11:09*

**+Yuusuf Sallahuddin**...thanks man...I'm going to check them out today


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 12:54*

**+Scott Thorne** Also, there is another option for 3d models that I am testing now. Autodesk also makes a software called 123d Catch. This one allows you to take photos of an object from all angles & then you upload them via the app & it processes them on their server end & creates a 3d model that you can download. I am not 100% sure how decent it is, but I am testing it at the moment with about 50 photos of a bic lighter. Problem is, 2mbps upload & 50x 20 megapixel images takes a long time to upload.



The app is available for iOS, Android, Windows 10 Mobile, Windows 10 Desktop, etc.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 15:51*

**+Scott Thorne** Update on that 123d Catch. So I've tested 2 models now. 1st was an empty 1.25L coca-cola bottle. I took 40 odd photos of it, with 20MP camera & the result turned out horrible. I assumed this was due to the transparency of the bottle. 2nd was a Bic Lighter. This time I took 50 photos of it, same 20MP camera. I even went to the effort of placing a piece of white paper behind it every time I took photo (to remove backgrounds). Again, turned out horrible. Could be related to it being unable to map the background correctly because of me putting the white paper, or the shinyness of the plastic. Either way, I'm not really happy that the software does it's intended job well. Creates poor quality 3d models that are not very accurate representations. At least in the 2 tests I've done. I could just be doing something incorrect, however I think it is easier to create my own models/combine existing models together. It's not like I need a 3d model every day.


---
**Josh Rhodes** *April 10, 2016 17:56*

123d catch works best with no background, I made the same mistake.  



They actually recommend placing so sticky notes with drawings around the object, this helps the software index where the photo was taken.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 18:03*

**+Josh Rhodes** Thanks for that. I might give it another go later sometime & see if those suggestions assist with creating a better model.


---
**Tony Sobczak** *April 10, 2016 19:46*

Thanks for the links **+Yuusuf Sallahuddin**​.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 20:13*

**+Tony Sobczak** You're welcome. Have fun.


---
**David Cook** *April 19, 2016 01:03*

this is soo cool,  Ill have to try this.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 19, 2016 01:47*

**+David Cook** Yeah I thought so too. Many possibilities when we can do things like this.


---
**David Cook** *April 19, 2016 02:18*

I've wanted to do this with my models but never wanted to take the time to make so many layers manually lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 19, 2016 04:34*

**+David Cook** Haha, yeah I tried manually first. Turns out this software (which someone else put me onto) does it all for you.


---
**Helce David Melgarejo** *June 26, 2016 01:14*

Hi you could help me ?. I want to cut 3 mm mdf but I'm not getting, I dealt with many configurations but I can not cut. I have not attended air, and change the base by a metal honeycomb, the height of the base relative to the laser head is 50mm. Thank


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 03:06*

**+Helce David Melgarejo** Hi Helce. Hopefully I can help. There's 2 things that come to mind that could be causing your issues with not being able to cut 3mm mdf.



1) Lens is in upside-down (mine came shipped upside-down)

2) Alignment of the machine mirrors is not quite right



For 1) this is a simple fix. Just unscrew the lens base & flip the lens over, making sure that it is clean/no fingerprints or oils before replacing. The lens orientation should be convex side facing upwards, you will be able to see your reflection in it & concave side facing downwards, you cannot see your reflection in it.

This makes a large difference in the ability to cut.



For 2) this is a more complex issue to align the machine, however once you know what you are doing it takes only around 10-15 minutes. The simplest explanation for this is provided in these links:



[https://1drv.ms/b/s!AtpKmELEkcL8imoUD99sdJZZT-am](https://1drv.ms/b/s!AtpKmELEkcL8imoUD99sdJZZT-am)



[http://www.lightobject.info/viewtopic.php?f=16&t=2835](http://www.lightobject.info/viewtopic.php?f=16&t=2835)



Once you've aligned the mirrors & the lens is oriented correctly then you should be able to cut through 3mm MDF. I would suggest settings around 10mA @ 12mm/s to cut through. It may or may not require 2 passes, as you mentioned you are not using Air-Assist yet. I would highly recommend adding some kind of Air-Assist to assist the laser with extra oxygen at the burn point, which makes cutting easier. Even something cheap/simple like an aquarium air-pump focused through a football inflation needle works well (that was my original air-assist). Just make sure the air is angled as close to the burn/cut point as possible.



Try this out & let me know how it turns out for you.


---
**Helce David Melgarejo** *June 26, 2016 19:30*

thanks for the help !, definitely improved with cleaning and lined with mirrors, achieve cutting mdf 3mm to 9mm / s 10mA in two passes, (I do not have isopropyl alcohol, only clean with cotton swab) one more question, what that height should be the head of the laser with respect to the material? since the machine does not have movement in the z axis. I'm a new user and I'm learning. Thank you!.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 19:37*

**+Helce David Melgarejo** That's great news Helce. The distance between the base of the lens & the centre of the material is what you want to aim for. As the beam focuses in to a point & then defocuses out again. What in effect will happen is it will create an hourglass like shape when cutting like this:



\/

/\



So the centre point of that hourglass shape you want to aim for it to be in the centre of your material, so that the angles on the cut are minimised/less noticeable. Unless you are going for an angled edge effect.



With the stock lens that comes with the machine it is a 50.8mm (2 inch) focal point. So measuring from the base of your lens to the centre of your material should be 50.8mm (2 inch).


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/UN3eDxdsjgP) &mdash; content and formatting may not be reliable*
