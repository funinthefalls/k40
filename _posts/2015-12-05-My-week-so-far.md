---
layout: post
title: "My week so far"
date: December 05, 2015 03:02
category: "Object produced with laser"
author: "Gee Willikers"
---
My week so far.



![images/dd1af1742b970cbc11e492e721e4e0f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dd1af1742b970cbc11e492e721e4e0f5.jpeg)
![images/42c5c47d2cb3fb48853efff6b585d273.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42c5c47d2cb3fb48853efff6b585d273.jpeg)
![images/0522878c399b26dc40dcd2e991972fb6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0522878c399b26dc40dcd2e991972fb6.jpeg)
![images/a1bc534d6085af65056f2875563c66dd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a1bc534d6085af65056f2875563c66dd.jpeg)
![images/ee25104ed0d5c496ec646e94101962cc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee25104ed0d5c496ec646e94101962cc.jpeg)

**"Gee Willikers"**

---
---
**Scott Thorne** *December 05, 2015 03:17*

Looks great man.


---
**Gary McKinnon** *December 05, 2015 18:33*

Cool drawings :)


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/Jp4uD1DX36G) &mdash; content and formatting may not be reliable*
