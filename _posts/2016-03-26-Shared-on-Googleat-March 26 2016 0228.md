---
layout: post
title: "Shared on March 26, 2016 02:28...\n"
date: March 26, 2016 02:28
category: "Discussion"
author: "Brien Watson"
---


![images/dca3407aa3d27a2976a0de85cc522f2f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dca3407aa3d27a2976a0de85cc522f2f.jpeg)



**"Brien Watson"**

---
---
**Brien Watson** *March 26, 2016 02:29*

 Pretty crude drawing. But I'll make it better tomorrow.  But I hone you get the idea.  Wiring in your emergency switch is really, a piece of cake.


---
**Gee Willikers** *March 26, 2016 03:15*

Also ensure the hot line is fused, not the neutral as it is shipped on some machines.


---
**Lori Martin** *March 27, 2016 21:39*

Thanks!


---
*Imported from [Google+](https://plus.google.com/111917591936954651643/posts/Fbj3C3rpAhH) &mdash; content and formatting may not be reliable*
