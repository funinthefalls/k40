---
layout: post
title: "Anybody know where I can get a copy of RD works the link on RDworkslab.com isn't working"
date: April 26, 2017 11:01
category: "Discussion"
author: "3D Laser"
---
Anybody know where I can get a copy of RD works the link on RDworkslab.com isn't working 





**"3D Laser"**

---
---
**Martin Dillon** *April 26, 2017 13:02*

not sure what it is, but I downloaded it.  Went to forum and found this link [http://en.rd-acs.com/down.aspx?TypeId=50074&FId=t14:50074:14](http://en.rd-acs.com/down.aspx?TypeId=50074&FId=t14:50074:14)


---
**Martin Dillon** *April 26, 2017 13:08*

maybe a better site for download [http://www.thunderlaser.com/laser-download](http://www.thunderlaser.com/laser-download)

[en.rd-acs.com - SHENZHEN RUIDA TECHNOLOGY CO.,LTD](http://en.rd-acs.com/down.aspx?TypeId=50074&FId=t14:50074:14)


---
**3D Laser** *April 26, 2017 14:04*

Thanks guys I just bought the k50 big blue 50w laser should be here in a week or two but wanted to play around with the software and get things transferred over first 


---
**Phillip Conroy** *April 26, 2017 21:39*

Rdworks sucks ,I use coreldraw x7 and the plugin for rdworks,with the k40 what you see on the screen is what gets [cut.no](http://cut.no) so with the plugin ,ie if you type a word the k40 will cut it in 1 peace ,the rdworks plugin treats the word as separate letters so to cut as one peace you must first weld the letters together so that it cuts as 1 peace.the best way of seeing this is to type a word and view it as a wire frame image. Also make sure you change the document properties to RGB that way the colors will be treated as separate layers. It has built in nesting which is great and works well.I am still learning  .the first mod I did was to add a meter to see what the power was set to as I hate not knowing what the % setting are.The ruda controler is very powerful ,ie all the features that you could want and the color screen is great.another thing to note is if engraving u must allow 5 to 10 mm for the head to slow down and change direction otherwise  a error comes up on the screen ,so set the start posting to current head position and move the head right 10 mm .


---
**Nigel Conroy** *April 27, 2017 19:40*

As someone mentioned checkout [youtube.com - SarbarMultimedia](https://www.youtube.com/user/SarbarMultimedia/feed)



He has the big blue 50w laser and his earlier videos are all on that machine and learning/using RDWorks.



I also have access to one of them at work and the other Conroy has one so ask away and I'm sure one of us can help you out.


---
**3D Laser** *April 28, 2017 12:32*

**+Nigel Conroy** thanks Nigel I do have a question i am one of those silly people that used laser draw for everything is there away to convert those files or do I have to recreate the wheel


---
**Nigel Conroy** *April 28, 2017 12:44*

Unfortunately I have not used laser draw, can they be exported from laser draw to a different format?


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/1FB7Z5HxPAh) &mdash; content and formatting may not be reliable*
