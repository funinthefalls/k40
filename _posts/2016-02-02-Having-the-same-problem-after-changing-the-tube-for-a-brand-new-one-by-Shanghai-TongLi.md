---
layout: post
title: "Having the same problem after changing the tube for a brand new one by Shanghai TongLi"
date: February 02, 2016 15:02
category: "Hardware and Laser settings"
author: "Sunny Koh"
---
Having the same problem after changing the tube for a brand new one by Shanghai TongLi. So added the Ampere Meter and found the output to be 1 mAh, Guys do you think it is the front control panel or the PSU?



Seems that the buttons sometimes behave different from what you expect such as when you want to increase, it decreases and so on. Or should I get them to ship the PSU to Singapore. Too exhausted to fly up again as next week I will be in LA.

![images/8257566708259178ef2956f4d95bccfd.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/8257566708259178ef2956f4d95bccfd.gif)



**"Sunny Koh"**

---
---
**Stephane Buisson** *February 02, 2016 15:53*

You could replace the front control panel by a 1Kohm pot for cheap. (to find out) and check the 5V.

but I bet it's your PSU. (the 22kV side)


---
**Stephane Buisson** *February 02, 2016 16:24*

to help you look at that:


{% include youtubePlayer.html id="yJImenhGE58" %}
[https://youtu.be/yJImenhGE58?t=1356](https://youtu.be/yJImenhGE58?t=1356)


---
**Sunny Koh** *February 02, 2016 16:42*

**+Stephane Buisson** That was what I am thinking, so a 1k ohm pot will work to do some testing. Might as well try it. At least shipping that is easier than pusing 2 Laser Tubes from Shenzhen to Singapore. 



Should I put up pictures of the "shopping" list.


---
**Sunny Koh** *February 02, 2016 17:11*

Found that my machine has a K- (Not Connected) K+ (1 connector), a G IN 5V (Another Connector)


---
**Stephane Buisson** *February 02, 2016 17:23*

different psu around, but very similar, sometime they connect on different ground pin on psu, on mine I have only the 5v from middle connector (read K40+Smoothie=SmoothK40)


---
**ED Carty** *February 02, 2016 19:13*

It looks like a safety switch or something in yoir safety circuit is not happy


---
**Sunny Koh** *February 03, 2016 11:26*

Okay, results are in, it is the PSU, after spending SGD$4.20 (SGD$1.20 for the 3 pin, SGD$1.00 for the 2 pin and SGD$1.00 for the pot) to confirm. But I think it is good to have the analog backup to test.



Just for the record



G => Pin 1 1K ohm Pot

IN => Pin 2 1K ohm Pot

5V => Pin 3 1K ohm Pot



And to test fire



Short K+ and K-


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/CkBuBqyaZ4f) &mdash; content and formatting may not be reliable*
