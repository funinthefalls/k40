---
layout: post
title: "Post shared in communities I'm member of"
date: October 10, 2018 07:37
category: "Discussion"
author: "J\u00e9r\u00e9mie Tarot"
---
Post shared in communities I'm member of. Comments could be posted in original post to share and discuss ideas and opinions at large. Post should be shared in relevant communities you enjoy hanging in and where this has not been posted. 



<b>Originally shared by Jérémie Tarot</b>



With the announce of G+ service end in a few month: Considering Facebook is a poisonned alternative, as well as any proprietary, commercial personal data hog. Considering the benefits that the Maker, Engineering, etc communities would pull from a common and  independant communication platform.

Isn't it time to envision gathering forces, energies and resources under a common organisation umbrella that would build, promote and maintain such a platform.



Base feature set would be replacement for G+ as well as content syndication (pages, blogs, videos, podcasts) with integrated commenting. Ideally, building on it's success, the platform would extend to code and project logs hosting, because GitHub dependence is even worth than anything else.



This, of course, would be based on Open Source code. Initial funding could be gathered from a crowdfunding campaign for the benefit of an ad-hoc foundation. A Wikimedia sister for the making, Engineering, fabrication... communities, knowledge and projects.



What do you think? I'm surely dreaming in front of my morning (third) coffee, but doesn't it all starts like this 🙂





**"J\u00e9r\u00e9mie Tarot"**

---
---
**HalfNormal** *October 12, 2018 00:28*

A community is only good as the participants! With any community, you have a lot of lurkers and very few actual contributors. It is also like one big extended family, love/hate relationships ect. Most people have full time jobs and host sites as hobbies which of course cost money to run and maintain.



All that being said, I have a forum that I have setup and maintained for the past few years with little traffic. I have reached out to a lot of people who have hardware/software in the laser area and offer full authority over their forum. I get a lot of positive feedback but no one actually uses the site for their products yet. (I actually hosted an area for someone who used to be here and had products but his health made him end his participation.)



I know it will be interesting to see what the outcome is when G+ actually shuts down.


---
**Jérémie Tarot** *October 12, 2018 08:30*

**+HalfNormal** totally agree with your points on communities and hosting.

That's part of the reasons I think we should try to build a platform for communities to offer them the nicest home possible to live in and prosper, and to mutualise resources under an independant umbrella that may eventually gather/attract funding to finance the expenses.



OTOH, I don't get your point about your forum experience? 


---
**HalfNormal** *October 12, 2018 12:42*

**+Jérémie Tarot** My forum experience point is that just because you create a forum/community/group, does not mean you will get traffic to your site no matter how great it is. It also takes time and money which is always in short supply! ;-)


---
*Imported from [Google+](https://plus.google.com/+JérémieTarot/posts/1MzMJXhxYJp) &mdash; content and formatting may not be reliable*
