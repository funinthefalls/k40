---
layout: post
title: "Forgot i have not shared this one on G+"
date: May 09, 2018 17:39
category: "Modification"
author: "HP Persson"
---
Forgot i have not shared this one on G+.

It´s a movable lens holder for the K40 you can make with your laser.



Stop with unlevel movable beds, move the lens instead ;)

It replaces the stock plate holding the laser head, and the head is re-used without the lens installed.



It´s still in very early beta - but feel free to try it out.

Instructions included in the file -> [www.k40laser.se/movablehead-v1.zip](http://www.k40laser.se/movablehead-v1.zip)



Works with 12 and 18mm lenses.

(some parts has updated since this pic was made)

![images/c901f8b47158998dba776c3944937461.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c901f8b47158998dba776c3944937461.jpeg)



**"HP Persson"**

---
---
**James Rivera** *May 09, 2018 18:26*

This looks pretty cool!  But, how did you incorporate air assist?  I have the nozzle upgrade from LightObject and I'm not 100% sure how I could use it with this.


---
**HP Persson** *May 09, 2018 18:29*

You can´t, the cone will interfere with the lens holder.

I am working on addons that can be bolted to the movable part to hold air assist pipes, laser pointers and similar.



It might be hacked though, to keep the cone and press-fit it in the lens holder to keep it, i have never seen a LO nozzle close up so not sure how at the moment :)


---
**Stephane Buisson** *May 10, 2018 13:15*

 As **+HP Persson** said it make more sense for K40 with poor depth to adapt the lens than to add a z table. (money wise)



A long time ago, I did something similar, never update it, if you like the idea, don't do it from that file (need improvement), make your own.



PRO: don't need k40 disassembling, lens on air assist cone.

CON: 3d print is flamable, take care, ...



[youmagine.com - K40 chinese lasercutter air assist with cross lines laser](https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser)




---
**Kelly Burns** *May 10, 2018 18:23*

Air assist doesn’t have to go through a nozzle.  It’s not a plasma cutter.  While it’s best to focus the air,  a brass tube directing air towards the focal point of the beam will do nicely.  A few commercial designs use this type of adjustable lens holder 


---
**Kelly Burns** *May 10, 2018 18:28*

**+Stephane Buisson**  almost every material we are lasering is flammable ;)   That said, I designed an adjustable holder that adapts a non-adjustable aluminum one.  I did have an heat build up from dirty lens that melted the holder.   


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/7DLicqsshuR) &mdash; content and formatting may not be reliable*
