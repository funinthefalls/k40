---
layout: post
title: "We have movement guys!!! Originally shared by Ariel Yahni (UniKpty) first dry run"
date: September 25, 2016 01:48
category: "Modification"
author: "Ariel Yahni (UniKpty)"
---
We have movement guys!!!



<b>Originally shared by Ariel Yahni (UniKpty)</b>



#UK40OB first dry run 

#yearofthelaser #LaserWeb #Smoothieware





**"Ariel Yahni (UniKpty)"**

---
---
**greg greene** *September 25, 2016 02:01*

Looks like it's working fine - what cutting area are you going to get with it?


---
**Ariel Yahni (UniKpty)** *September 25, 2016 02:03*

I'm about 30x60 or 65 I think.


---
**greg greene** *September 25, 2016 02:06*

awesome - where did you get the rail parts from?


---
**Alex Krause** *September 25, 2016 02:10*

[openbuildspartstore.com - Linear Rail](http://openbuildspartstore.com/linear-rail/)


---
**greg greene** *September 25, 2016 02:18*

thanks


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 25, 2016 12:48*

Looks like very smooth movement :) Much smoother than the original K40 guts.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/RYq7aYhfLnr) &mdash; content and formatting may not be reliable*
