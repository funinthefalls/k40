---
layout: post
title: "Did anyone have trouble connecting Lightburn to their computer?"
date: July 29, 2018 14:11
category: "Software"
author: "Nathan Thomas"
---
Did anyone have trouble connecting Lightburn to their computer?



I just downloaded it to connect to my K50 and they aren't communicating. I've followed the video tutorial on their page about connecting the ruida/ethernet as well and still nothing. 



I'm using a windows 7 laptop if that helps...









**"Nathan Thomas"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2018 14:18*

You are sure it is a Ruida controller? Do you use the RDWorks software with the machine previously? 


---
**Nathan Thomas** *July 29, 2018 14:24*

From their video it looks like the ruida which is the standard on the blue 50 watt I believe. 



I hadn't tried RDWORKS but im thinking about trying it now if I cant get LB to connect 


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2018 14:29*

The “K50” sometimes comes with a Ruida and other times with the M2Nano or similar board as in the K40. 

Send picture of your machine and controller. 


---
**Nathan Thomas** *July 29, 2018 14:33*

I sure hope I'm wrong then cause I'm stumped...![images/62d360b6958c191676e7c28472a1d95c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/62d360b6958c191676e7c28472a1d95c.jpeg)


---
**Nathan Thomas** *July 29, 2018 14:33*

![images/d96b003367c85033bf757b338b00ce35.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d96b003367c85033bf757b338b00ce35.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2018 14:37*

Ok, yes, this appears Ruida like. 



There’s a support group for LB on G+ and on FB. The FB one gets a lot more traffic. Recommend you post there. 


---
**Nathan Thomas** *July 29, 2018 14:38*

Yeah I just found it, still waiting to be accepted.  Thanks


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2018 14:42*

Enjoy. Don’t say I didn’t tell you when the responses aren’t “instant”. Unfortunately I’m on fb constantly. G+ I have to check manually. The developer will be with you... in time. 


---
**Sebastian Szafran** *July 29, 2018 19:14*

I am using Lightburn with Red Chinese laser. My machine is connected via Ethernet and works without any issues.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/9FU9Ta5Hp9W) &mdash; content and formatting may not be reliable*
