---
layout: post
title: "Nice little trick to set the hight from the material"
date: November 08, 2016 12:04
category: "Hardware and Laser settings"
author: "Stephane Buisson"
---
Nice little trick to set the hight from the material. 
{% include youtubePlayer.html id="0eGiQzf6nac" %}
[13:31](https://youtu.be/0eGiQzf6nac?t=810&t=13m31s)





**"Stephane Buisson"**

---
---
**Bob Damato** *November 08, 2016 13:21*

Damn. I wish mine cut that quick and that clean!


---
**Don Kleinschnitz Jr.** *November 08, 2016 13:43*

**+Stephane Buisson** I saw that clever lever. On my list for a new head design with snap in lenses..... some day!


---
**Joey Fitzpatrick** *November 08, 2016 14:51*

That is a Trotec feature.  A very useful tool to have.


---
**Brook Drumm** *November 08, 2016 16:22*

Im surprised how he is choosing to draw the gear. That was so painful for me to watch because it was so slow and inefficient :(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 08, 2016 16:36*

I was surprised at his near computer illiteracy. He comes across as much more talented on the Mythbusters show. The Trotec machine looks like a beast. I also wish I could cut that cleanly. I guess that's what 120W of power does for you.


---
**Tony Schelts** *November 08, 2016 16:38*

Would you like to post a tutorial on how to draw gears?


---
**Stephane Buisson** *November 08, 2016 18:30*

don't draw a gear, use a gear generator plugin.

see my post here 14/12/2015


---
**Stephen Sedgwick** *November 08, 2016 18:44*

**+Brook Drumm** I would agree you create a tutorial on that?


---
**adrian miles** *December 18, 2016 09:07*

the focus setting is interesting....people say to set the focus point in the middle of the material but that and other auto focus setting systems all seem to use the face as the focus point


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2016 14:27*

**+adrian miles** Depends on if you want to cut or engrave. Cutting works best when focused to the centre of the material because the beam (in profile) forms an X shape, where it focuses in to a near point & then defocuses out again after that region. If you focus in on the top of the material, you will have bevelled cuts (ever so slightly angled edges) whereas focusing on the centre you're more likely to have > shaped cuts (without such severe angles) which is less noticeable.



I imagine in a multipass run, if your z-height changed accordingly, it could be useful to focus directly on the top of the material (more power focused on a smaller area) & successively as it passes again & again it could refocus further into the material (after it has already vapourised the above layers).


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/SyKNQ2hRJi2) &mdash; content and formatting may not be reliable*
