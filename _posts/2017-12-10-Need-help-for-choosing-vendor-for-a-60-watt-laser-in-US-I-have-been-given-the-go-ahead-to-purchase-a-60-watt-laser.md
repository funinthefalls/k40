---
layout: post
title: "Need help for choosing vendor for a 60 watt laser in US I have been given the go-ahead to purchase a 60 watt laser"
date: December 10, 2017 02:24
category: "Discussion"
author: "HalfNormal"
---
Need help for choosing vendor for a 60 watt laser in US



I have been given the go-ahead to purchase a 60 watt laser. Need to know if there's been any problems with specific vendors on Ebay or suggestions for the good ones. Thank you so much for your help.





**"HalfNormal"**

---
---
**Anthony Bolgar** *December 10, 2017 18:19*

I bought mine from [asc365.com - ASC365.com Screenprintng padprinting sublimation officesupply buttonmaker package stentil cuttingplotter](http://ASC365.Com) They have warhouses in Toronto,Canada, and somewhere in Californis. Great company to deal with, they honor warranty claims without any fuss, and offer a 1yr warranty on everything other than the tube (6 months)


---
**HalfNormal** *December 10, 2017 21:56*

**+Anthony Bolgar** Thanks for the link!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/aTZm6D14zM2) &mdash; content and formatting may not be reliable*
