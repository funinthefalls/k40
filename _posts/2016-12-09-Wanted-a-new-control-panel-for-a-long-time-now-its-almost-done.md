---
layout: post
title: "Wanted a new control panel for a long time, now its almost done"
date: December 09, 2016 10:55
category: "Modification"
author: "HP Persson"
---
Wanted a new control panel for a long time, now it´s almost done.

This is a prototype of the layout. I have ordered a new button-pad, thats why it looks crazy with the old one still there :)



Temps showing PSU temp, water tank temp, and the third is for the 2nd loop outside the window with a radiator.



With theese buttons i can controll red dot, lights, fans, air assist, water pumps, exhausts and autofocus without scattered buttons or glued buttons on the old panel :)

![images/a011d853e1775c95fb2fddef5171c828.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a011d853e1775c95fb2fddef5171c828.jpeg)



**"HP Persson"**

---
---
**Don Kleinschnitz Jr.** *December 09, 2016 15:32*

Very nice did you make an entirely new sheet metal door. This is not a K40 (the depth of the cover)?




---
**HP Persson** *December 09, 2016 15:36*

It´s a K40.

I cut out a large area and inserted a acrylic panel in the metal lid and wrapped it with vinyl :)

Going to wrap rest of the white metal in the same color.



The black piece with buttons is just a thick vinyl sheet too.


---
**Don Kleinschnitz Jr.** *December 09, 2016 16:19*

The front lip on the doors are much bigger than mine? Did you cut the front of the cabinet?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 09, 2016 16:24*

Nice job on the switch/meter panel.



Same as Don, the front of your doors are also much larger than mine. I think mine is only about an inch high on the front of the doors.


---
**HP Persson** *December 09, 2016 16:27*

Ah, yeah its the upgraded/newer version of K40, sometimes refered to as K40D. Inside is upgraded aswell.


---
**Bob Damato** *December 12, 2016 12:41*

Wait.... autofocus? :)    Im listening...


---
**HP Persson** *December 12, 2016 12:47*

Arduino controlled stepper motor moving the lens until the sensor touches the surface. The sensor is on a pre-set height and after that it backs off 3 revs so the sensor isnt dragging on the material.



The system knows where the exact height (focus) is to cut or engrave.

It´s not actually checking the optic focus, it just moves the lens to a pre-set distance i measured.

Had it manually earlier, but it was cool building it motorized.

No matter what thickness on the material i use, one button and boom, it´s in focus.



First prototype is working now, i´ll post a video of it tomorrow :)




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 13:13*

**+HP Persson** You mention that it doesn't matter what thickness material you are using, so does that mean you have to reset a "thickness" setting for different thicknesses? Because say 6mm thick compared to 3mm thick is 1.5mm difference in the focus point...



Seems like a great idea though :)


---
**HP Persson** *December 12, 2016 14:13*

I´m working on making it to count revs/steps properly away from absolute zero (home) to identify the thickness with 0.1mm resolution, as you mention what i described only works for engraving :)



System knows were zero is, and when the sensor stop it using the calculated amount of travel done it "knows" what thickness it is and can move the lens to a proper focus.

So far only coded a small piece of this in 0.5mm resolution , but i´m getting there :)

Not yet found a better solution than counting steps to perform it, if someone has a idea i´m glad to try it out :)



Quick pic without the lens/sensor/red dot/airassist assembly.

(and no, the final version will not be 3d printed, but aluminium or brass  :) )

![images/5a38d5bb72b02dbe7d3324e6bf41d4f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a38d5bb72b02dbe7d3324e6bf41d4f6.jpeg)


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/f9fkStWthLZ) &mdash; content and formatting may not be reliable*
