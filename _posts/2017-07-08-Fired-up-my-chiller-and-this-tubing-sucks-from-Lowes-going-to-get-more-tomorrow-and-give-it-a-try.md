---
layout: post
title: "Fired up my chiller and this tubing sucks from Lowes going to get more tomorrow and give it a try"
date: July 08, 2017 04:14
category: "Modification"
author: "William Kearns"
---
Fired up my chiller and this tubing sucks from Lowes going to get more tomorrow and give it a try. I am using a ps from a computer to power pump fan and peltier

![images/bcc62b98b2a4d32f7c5064096f25b523.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bcc62b98b2a4d32f7c5064096f25b523.jpeg)



**"William Kearns"**

---
---
**Don Kleinschnitz Jr.** *July 08, 2017 12:25*

#K40PeltierCooler


---
**laurence champagne** *July 08, 2017 15:40*

Wow, nice. Please keep us updated, I'm in the process now of putting a parts list together to make my own peltier cooler.


---
**Todd Mitchell** *July 09, 2017 01:53*

Nice idea with the thermos and Peltier!  Do you think the thermos will hold enough water? to keep it cool?



What is the black stuff around the pipe nipples?


---
**William Kearns** *July 09, 2017 02:03*

**+laurence champagne** new tubing installed no leaks just fired it up and timing now to see how long it takes to drop the temp. ![images/23de55d03c06e991185b25a15654e92e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/23de55d03c06e991185b25a15654e92e.jpeg)


---
**William Kearns** *July 09, 2017 02:05*

It's 64oz may need to had one or two more peltier. Testing how efficiently it cools now.


---
**William Kearns** *July 09, 2017 02:08*

I had to drill into the thermos and sealed the fittings in with JB weld. Will clean up after testing 


---
**Todd Mitchell** *July 09, 2017 02:20*

Is the JB Weld flexible?  Just curious how it's going to handle the rounded shape of the thermos.


---
**William Kearns** *July 09, 2017 02:22*

It's an epoxy that you mix and hardens over a couple of hours. I've used it for all kinds of stuff it's like the duct tap of epoxy. 


---
**William Kearns** *July 09, 2017 02:23*

After it cures no it's extremely hard you can sand it drill it out and even tap it


---
**Todd Mitchell** *July 09, 2017 02:42*

Good to know that you can tap it as well.  I'm just using the good ol bucket of water solution for now but have been thinking of these thermoelectric coolers for sometime.  I might give this a go.


---
**Don Kleinschnitz Jr.** *July 09, 2017 04:44*

What kind of thermal control? On/off, PWM, Voltage reg, Current reg. ???


---
**William Kearns** *July 09, 2017 04:45*

On off thermostat 


---
**Steve Clark** *July 09, 2017 16:59*

**+William Kearns** **+Don Kleinschnitz**  Don, here we are with the same issues I have. My first thought was/is the same as Williams... an  on /off thermostat...but the more reading I do the more I realize, though it may work this way, there are problems that can occur and we could avoid these by being able the adjust voltage or current within that sweet spot .



William, my research says we need to have available Peltier capacities greater than required and do this efficiently, you need to keep the operating current somewhere within 20 to 60% of the Imax of the Peltier elements . So there is a need to be able to adjust the PS voltage/current down. Take a look at this video. (Don, one of your links turns out to be the source this same video)




{% include youtubePlayer.html id="bp3Ox7H4dwA" %}
[youtube.com - 1.2.1 Thermoelectric Cooling Design](https://www.youtube.com/watch?v=bp3Ox7H4dwA)



At this point for me I need to come up with a way to control the voltage and I think William you may come up against this also.




---
**William Kearns** *July 10, 2017 03:39*

Interesting read and video will have to dive deeper into something that steps the power down. Does anyone know how to reset these little blue thermostats? Mine is stuck in three dashes and ![images/41696a783f626af303f7ee904639d6da.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41696a783f626af303f7ee904639d6da.jpeg)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/K9gZUmYGKoR) &mdash; content and formatting may not be reliable*
