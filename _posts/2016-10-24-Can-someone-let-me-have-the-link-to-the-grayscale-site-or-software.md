---
layout: post
title: "Can someone let me have the link to the grayscale site or software"
date: October 24, 2016 08:57
category: "Discussion"
author: "Tony Schelts"
---
Can someone let me have the link to the grayscale site or software. cant find it for carving?





**"Tony Schelts"**

---
---
**Scott Thorne** *October 24, 2016 10:28*

Which software are you looking for? 


---
**Tony Schelts** *October 24, 2016 13:22*

I think Ashley made a comment on one of your post saying something about software that you load a photo and it creates a grayscale image ?


---
**Alex Krause** *October 24, 2016 13:29*

Photograv


---
**Tony Schelts** *October 24, 2016 13:48*

thanks


---
**Scott Thorne** *October 24, 2016 14:11*

You can do that with photoshop, gimp or paintshot pro..but you can't do the 3d grayscale with any of them...artcam by autodesk is what company's are using.


---
**Scott Thorne** *October 24, 2016 14:14*

Photograv...is only for editing photos for 2d engraving...similar to Universal 1 touch photo software...it's not for 3d carving...sorry. 


---
**Tony Schelts** *October 24, 2016 23:14*

thats cool thanks. just wanted to have a play.


---
**Ariel Yahni (UniKpty)** *October 25, 2016 03:05*

**+Anthony Bolgar**​ shared this yesterday to make 3D bas relief [https://plus.google.com/+AnthonyBolgar/posts/c5HxLQvi96R](https://plus.google.com/+AnthonyBolgar/posts/c5HxLQvi96R)


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/MaiSne6QyyG) &mdash; content and formatting may not be reliable*
