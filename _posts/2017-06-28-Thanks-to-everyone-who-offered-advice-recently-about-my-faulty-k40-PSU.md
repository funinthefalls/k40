---
layout: post
title: "Thanks to everyone who offered advice recently about my faulty k40 PSU"
date: June 28, 2017 13:30
category: "Original software and hardware issues"
author: "Calum Stirling"
---
Thanks to everyone who offered advice recently about my faulty k40 PSU. I was delighted after changing a couple of components to find that the laser actually engraved. I had bought the machine as spares or repairs on the basis that it had only been used twice and sold on as the seller couldnt get the mirrors aligned. So very pleased to see it working yesterday.  



I spent hours on the mirror alignment yesterday, which meant changing the tube height, raising the first mirror and setting the other two. Eventually all seemed aligned enough (before final tweaking) to try an engrave. Was pleased to see it working and did five small engravings at about 20% power using the stock laser drw files. 



Today however things were a bit different and am writing to ask for your advice. 



I tried the same test engrave as yesterday, same power and same settings using laser drw. The first one was ok but the subsiquent two the power was dropping, by the third the meter was not rejestering as firing. 



After running the engrave file I did test fires of the laser from 0-100% and can see that both the pot and meter and intensity of laser burn spot match. 



What might be the likely cause of the issue? Could it just be something as simple as the alignment having wandered off overnight. I see there is a 8mm bubble at the neck of the tube, is that an issue. Perhaps I have inadvertantly changing a software setting. 



I'm a bit stumped, any ideas would be most welcome.





**"Calum Stirling"**

---
---
**Anthony Bolgar** *June 28, 2017 13:57*

It sounds like the tube was overheating, the warmer the coolibng water becomes, the less power the lase has. I think the airbubble is the culprit, you can try tilting the whole machine while the pump is running to try and clear the bubble. Sometimes, just running the pump only for a couple of hours will clear the bubbles as well.


---
**Calum Stirling** *June 28, 2017 14:03*

**+Anthony Bolgar**  Thanks for thoughts. I ran maybe four 3min engrave files and did 10 test fires within 30mins of switch on. Fairly sure the pump was on. Is that enough time for overheating? Also the same air bubble was there yesterday and it worked ok. 


---
**Don Kleinschnitz Jr.** *June 28, 2017 14:18*

**+Calum Stirling** irrespective of this problem you should get rid of the air bubble.

1. Is this right? when you engrave the meter shows decreasing current while the job is running? But when you test fire the meter shows full current?



I agree with **+Anthony Bolgar** it sounds like the tube is heating up. 



2. Do you have a temp meter monitoring the water?

3. How do you know there is water flowing?

4. Did you do the test fire directly after the job ran and the power dropped. i.e. did you test fire while the tube was potentially overheated of was there time for it to cool down?



Alternately the LPS HV could be drooping under load. I doubt it but suggest this because it has recently been repaired :).


---
**Calum Stirling** *June 28, 2017 14:41*

Howdy, Thanks for thoughts on this.



1. Ok so its a heating tube issue. I'll get rid of that bubble, one more verified fix to tick off.



2. I do not have a temp meter installed yet. I will do, and a have a flow meter to install. 



3. I heard the pump running this morning in the 15ltrs of distilled but cannot verify it actually was circulating. When I installed the water and pump last week I could see a flow returning to the reservoir but to be honest I'm not sure whether, if the pipes were incorrectly labelled which direction the water is circulating. I think I have connected the hoses to run the water clockwise from the back to front of tube. 



4. I did the test fires directly after the job ran and the power dropped. i.e. It would only have had 10mins max to cool down. Bear in mind however this is Scotland so ambient water temp will be a bit less.  How quickly does the water take to heat? 



5. Is it likely i've made the tube unusable within such a short space of time. I must have fired the laser a couple of hundred times yesterday whilst aligning the mirrors. But it did engrave faultlessly after that with no noticeable drop off so hopefully there is life left in the tube yet. 



6. Yes it could be a PSU HV issue. 


---
**Don Kleinschnitz Jr.** *June 28, 2017 14:50*

4. I did the test fires directly after the job ran and the power dropped. i.e. It would only have had 10mins max to cool down. Bear in mind however this is Scotland so ambient water temp will be a bit less.  How quickly does the water take to heat? 

To make sure I understand. If you test fire directly after a bad engrave does the power drop while you hold the test for a few seconds?



The best thing to do at this point I think is to get rid of the bubble and get a temp probe in the water. 


---
**Calum Stirling** *June 28, 2017 14:57*

I'll need to get back to you about test fire and meter reading as I only done momentary fires.



Bubble elimination and and temp probe. gotcha!



Am I correct in thinking that the water should not go above 25C (77f) and that the water should circulate into the beam exit end of the tube first? 


---
**Nathan Thomas** *June 28, 2017 16:11*

**+Calum Stirling** 25 C is a good barometer but also take into consideration ambient temp, humidity, etc. It won't explode if it gets over 25 C, but it will decrease tube life and power the higher the temp.



I may be misunderstanding you, but I think it's backwards. My machine has the water exiting from the end that the beam exits.


---
**Stephane Buisson** *June 28, 2017 18:03*

optimum temp is 15/18°C you can use frozen water bottle in your bucket to confirm the solution of this problem.


---
**Calum Stirling** *June 28, 2017 21:19*

**+Stephane Buisson** Ok, i'll fit a sensor and see what that says :)


---
**Calum Stirling** *June 29, 2017 19:53*

Update:



As suggested I got rid of the bubble and added a thermometer to the reservoir.  Monitoring the temperature over the afternoon whilst the laser was being used it went from 14 deg to 22 degrees. I was mostly cutting tests on 3mm acrylic, (perhaps 30mins laser time over 4 hours machine ON time).  



However just before I left I checked the tube again and low and behold a bubble? Looking at the tube I could see lots of micro bubbles, which appear to be clinging to the glass wall and refilling the void before the neck, see pic. I tipped the unit again and ran the pump but it didnt seem to shift them. 



Any idea why they are there and how to get rid of these? 






---
**Calum Stirling** *June 29, 2017 20:40*

Now with added bubbles..

![images/0cfda1ea867fc173760922dfd9f08398.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0cfda1ea867fc173760922dfd9f08398.jpeg)


---
**Anthony Bolgar** *June 29, 2017 22:17*

**+Don Kleinschnitz** what is a safe additive he can use to lower the surface tension and get rid of the micro bubbles? I know RV antifreeze works, but is it OK to use in the coolant, or does it affect the conductivity too much?


---
**Calum Stirling** *June 29, 2017 22:23*

**+Anthony Bolgar**  Hiya, I have added about 5% antifreeze to my water when I first filled the reservoir. TBH I have no idea if it may have another additive in the antifreeze, which is cause bubble 'stickage'. I'm just reading about cavitation bubbles now. I should look at the orientation of the water return, which could assist in the release of the bubble build up. Crazy innit haha!


---
**Don Kleinschnitz Jr.** *June 30, 2017 03:09*

**+Anthony Bolgar** **+Calum Stirling** nope antifreeze is a no-no. We have proven to multiple 10's of users that anything but the right mix of algaecide or second choice clorox will render the water to conductive and risk lower power, LPS damage and a "powered up" bucket.



I had small bubbles in mine also and it just took some effort to get them all out. The ultimate fix is supposed to be reorienting the outlet port upward.



**+Calum Stirling** search the forum for bubbles to get more hints. I would FLUSH the system and restore with distilled water and algaecide according to the formula I posted.


---
**Calum Stirling** *June 30, 2017 08:58*

**+Don Kleinschnitz** Thanks, will get onto it.  


---
*Imported from [Google+](https://plus.google.com/106058704145699553667/posts/bjK9GE9WR5r) &mdash; content and formatting may not be reliable*
