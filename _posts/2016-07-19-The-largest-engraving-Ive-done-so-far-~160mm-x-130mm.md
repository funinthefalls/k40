---
layout: post
title: "The largest engraving I've done so far, ~160mm x 130mm"
date: July 19, 2016 03:46
category: "Object produced with laser"
author: "Tev Kaber"
---
The largest engraving I've done so far, ~160mm x 130mm. 7mA@400mm/s. 

![images/3b13454f5018f3d17ba7cac8cf475a94.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b13454f5018f3d17ba7cac8cf475a94.jpeg)



**"Tev Kaber"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 19, 2016 08:24*

That turned out wonderful. Is this with stock hard/software or have you switched to LaserWeb?


---
**Tev Kaber** *July 19, 2016 10:36*

Air assist + new lens, otherwise stock hardware. Corel X5 + stock CorelLasr. 


---
**Tev Kaber** *July 19, 2016 12:24*

I realized after I did it that I forgot to adjust the height, I had it set for 3mm but this was 1/4". So it probably could have been a little crisper, but still looks pretty good.


---
**Corey Renner** *July 19, 2016 14:50*

Wow.


---
**David Richards (djrm)** *July 19, 2016 19:19*

One of my favourite pictures, I'll have to make one for myself now. Thanks for sharing, David.


---
**Ned Hill** *July 19, 2016 20:55*

Nicely done.  Are you an astronomy fan?  That image is actually the APOD for today.  [http://apod.nasa.gov/apod/astropix.html](http://apod.nasa.gov/apod/astropix.html)


---
**Tev Kaber** *July 19, 2016 21:01*

Hah, I didn't notice that!  Nice timing. :)


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/dv5k2JDBDuW) &mdash; content and formatting may not be reliable*
