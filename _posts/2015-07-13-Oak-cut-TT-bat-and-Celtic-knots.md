---
layout: post
title: "Oak cut TT bat and Celtic knots"
date: July 13, 2015 19:08
category: "Object produced with laser"
author: "David Richards (djrm)"
---
Oak cut TT bat and Celtic knots. The small balsa wood logo engraving was the very first thing I made.

![images/246569c9197f70b490b025a35ab9fffe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/246569c9197f70b490b025a35ab9fffe.jpeg)



**"David Richards (djrm)"**

---
---
**Antonio Chagas** *July 23, 2015 15:34*

How did you set the machine parameters to cut and engrave? How thick was the Balsa Wood ?


---
**David Richards (djrm)** *July 23, 2015 22:13*

Hi Antonio, I'm sorry I did not record the details of those jobs, engraving djrm on the thin balsa wood (<2mm) needed quite fast travel at very low power, more would burn right through. The bat and handle are approx 4mm oak faced plywood, they were cut slowly at nearly full power, I gave them two passes. The Celtic knots were somewhere in between, they are vector drawings and so are cur not engraved, if I remember correctly. hth David.


---
**Antonio Chagas** *July 24, 2015 01:39*

Thank you


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/6UtL6j13cJF) &mdash; content and formatting may not be reliable*
