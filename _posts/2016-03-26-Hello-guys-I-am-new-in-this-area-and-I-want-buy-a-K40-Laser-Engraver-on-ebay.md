---
layout: post
title: "Hello guys! I am new in this area and I want buy a K40 Laser Engraver on ebay"
date: March 26, 2016 18:30
category: "Discussion"
author: "Rui Rodrigues"
---
Hello guys!



I am new in this area and I want buy a K40 Laser Engraver on ebay. 

I know that is an chinese machine but have a razonable stock quality? or I have to do modifications to have an acceptable quality?



I just need this machine to engrave logos on aluminium pens.



which are the principal troubles of this machines?



Thank you!





**"Rui Rodrigues"**

---
---
**Anthony Bolgar** *March 26, 2016 18:49*

You cannot engrave aluminum. What you can do is burn through an anodized coating, or use cermark to mark the aluminum.


---
**Rui Rodrigues** *March 26, 2016 22:12*

Hi Anthony!

Yes I want to burn anodized aluminium. This machine can do this work with quality? Thank you


---
**Anthony Bolgar** *March 26, 2016 23:35*

It will be just fine for doing this, I suggest making a jig to hold the pens, and you will just have to try different speed and power settings until you find the best combination.


---
**Rui Rodrigues** *March 28, 2016 00:33*

nice! I already sent a offer to ebay seller.



This machine:



[http://www.ebay.com/itm/40w-mini-desktop-co2-laser-engraver-cutter-engraving-cutting-usb-cnc-router-ca-/262318824568?hash=item3d136b9c78:g:hhcAAOSwT6pVhAlu](http://www.ebay.com/itm/40w-mini-desktop-co2-laser-engraver-cutter-engraving-cutting-usb-cnc-router-ca-/262318824568?hash=item3d136b9c78:g:hhcAAOSwT6pVhAlu)



is better than:



[http://www.ebay.com/itm/Hot-Sale-40W-CO2-Laser-Stamp-Engraving-Cutter-Craftworks-Cutting-USB-300mmx200mm-/222026548871?hash=item33b1d04687:g:mS8AAOSwKtVWxDbd](http://www.ebay.com/itm/Hot-Sale-40W-CO2-Laser-Stamp-Engraving-Cutter-Craftworks-Cutting-USB-300mmx200mm-/222026548871?hash=item33b1d04687:g:mS8AAOSwKtVWxDbd)



?



Thank you


---
*Imported from [Google+](https://plus.google.com/107832996652493137156/posts/XiYkUk7FeAp) &mdash; content and formatting may not be reliable*
