---
layout: post
title: "Well I think I popped my controller board"
date: October 28, 2016 19:28
category: "Original software and hardware issues"
author: "Kris Sturgess"
---
Well I think I popped my controller board.



I was finishing up my custom panel for my emergency stop switch. I was just closing up the door and the bottom separated from the switch and arced on the frame.



Popped the 110v fuse.



Replaced the fuse and everything powers up, and I can test fire laser no prob.



BUT i have no computer control to cut or engrave....



Thoughts? and/or suggestions?



Any other fuses to check?



Kris



P.S. My wife thinks it's a ploy to upgrade controllers... lol





**"Kris Sturgess"**

---
---
**Scott Marshall** *October 28, 2016 19:44*

You should have wired the E stop into the 5V control power. 



But that's not going to help now. Double check all the connectors to the PSU, one may have been pulled loose in the melee. 



Beyond that, it may have killed the M2, but shouldn't have if you simply grounded the AC mains to the cabinet. Weird things happen with a dead short, and currents can often go places you don't figure they would.

 If you have a meter, verify the M2nano is getting it's 5V and 24V. (5v is the most important, the 24 runs the motors)



On the large white connector on the M2 controller Pin 3 is ground and pin 4 is 24v, pin 2 is 5v (pin 1 is the fire line, but it's not in play here)



If the voltages there show 5 and 24, it's probably the board.



Tell your wife we certify your innocence. (like that will help)



Scott


---
**Kris Sturgess** *October 28, 2016 20:14*

Hi Scott,



I removed the key switch (never gonna use) that was in series with the E Stop. They were wired in to the 110v circuit and kill the power to the entire machine. Should I disconnect from 110v and add it in series to the interlock system instead? just have it power off the laser?



Tested the M2 connector and I'm getting 23.8v and 4.9v



Checked and re-plugged all connections.

Checked and re-plugged USB connection



So I'm thinking the worst. :-(



Where are you at with your ACR full kits? ;-)



Thanks for your help.



Kris



P.S. Anyone with a M2 Nano sitting around they want to get rid of for now?


---
**Don Kleinschnitz Jr.** *October 28, 2016 23:30*

**+Kris Sturgess** ​have you a dead LPS you might want to trade for a M2?


---
**Kris Sturgess** *October 28, 2016 23:33*

Sorry Don I don't.  How much you want for the M2?


---
**Scott Marshall** *October 28, 2016 23:54*

I'd recommend, as you say, putting the Estop in the shutdown loop,  hard power downs (fully loaded) of a switching style power supply like used in the tube circuit are harsh to say the least. The spikes generated can be damaging. (as I'm afraid you demonstrated)



If you need to do a stop, it's nice to not total your PSU or laser tube.

I think the Manufacturer installs Estop buttons as on/off switches to say they have one. Done properly, there's a master power and then a separate Emergency stop which breaks control power (machine circuits in industrial speak) If you want to stop the motors as well (advisable for a safety oriented estop) break the 5V supply. That will freeze all power releasing devices, but will leave the main PSU idling.



The K40 isn't really laid out for a proper estop, usually the circuits are divided, and the Estop doesn't crash the controller. It's something I considered kitting, but there hasn't been much interest. If you have the estop wired as a line voltage I'd only use it in a critical situation where property damage or injury was at hand. Chancing damage to your  laser  to save a sheet of plywood isn't a good plan.



I currently have 1 available full kit (tetering as I have someone who just emailed me considering upgrading his older ACR - he was customer #2). And I may have another available but am waiting to hear back on his choice. 

I also may have a original available but need to look into it. (I have one here that I forget what the deal is on)

The Originals go for $279.95 with a 3x, and the MarkII goes for $299.95 with the 3X.  The 4X is +$25 and will be a few days as I have a shipment of those coming. Shipping is a flat $15



If you commit now,  I can let you have the MarkII prototype and it will ship Monday. I'm going to hope this works out and nobody gets stiffed. 

Drop me a note @ Scott594@aol.com



Scott


---
**Kris Sturgess** *October 29, 2016 01:51*

**+Scott Marshall** e-mail coming your way shortly.




---
**Kris Sturgess** *October 29, 2016 06:04*

**+Scott Marshall** Email sent. Let me know.




---
**Kris Sturgess** *October 30, 2016 03:10*

**+Scott Marshall** did you get my e-mail? Hoping to get that unit shipped out Monday if possible?


---
**Scott Marshall** *October 30, 2016 11:56*

I sent several. I was wondering why I hadn't heard back. I'll resend.




---
**Kris Sturgess** *October 30, 2016 12:01*

Ya I've havn't received anything. Even been checking junk folder. You can also try [kris.sturgess](http://kris.sturgess)@[gmail.com](http://gmail.com)    maybe AOL blocks hotmail?


---
**Scott Marshall** *October 30, 2016 12:06*

I'm not sure what's going on, I just checked my 2nd email I have 2, ALLTEK594 & Scott594.



I'll find the emails - think I sent 2, and resend them. Let me know here if you don't have them in 20minutes. Thanks for your patience.

Scott


---
**Scott Marshall** *October 30, 2016 12:10*

Sent to the hotmail, doing the gmail now...


---
**Scott Marshall** *October 30, 2016 12:15*

I resent the email to both addresses, the attached manual is going to the gmail address and is uploading now. it'll be 1/2hr or so until it's done.

Scott


---
**Kris Sturgess** *November 01, 2016 20:44*

**+Scott Marshall**​ just checking in. It's been dead silent on the email front. Nothing coming in via Hotmail and now Gmail has gone silent. Not sure if your just busy or if your email has gone totally wonky. If you need to contact me my phone # 1-403-596-1618. Cheers! 


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/R1p5m9W2Drb) &mdash; content and formatting may not be reliable*
