---
layout: post
title: "Someone mentioned the idea in one of the posts in this community, the idea of using a bar fridge to use as a water chiller for cooling the CO2 laser tube by drilling holes in the door and having a container of water inside"
date: October 10, 2017 00:49
category: "Discussion"
author: "Nick R"
---
Someone mentioned the idea in one of the posts in this community, the idea of using a bar fridge to use as a water chiller for cooling the CO2 laser tube by drilling holes in the door and having a container of water inside.  I was wondering if anyone has actually done this, as this idea seems to be one of the cheapest solutions as second hand bar fridges are cheap. (at the cost of space) Anyone done this? or can foresee any issues with this idea?





**"Nick R"**

---
---
**Martin Dillon** *October 10, 2017 13:06*

I modified one of these for a chiller  [homedepot.com - Water Dispensers - Water Filters - The Home Depot](http://www.homedepot.com/b/Kitchen-Water-Dispensers-Filters-Water-Coolers-Racks/N-5yc1vZcdad)  It was being thrown away.


---
**Steve Clark** *October 11, 2017 04:13*

It will work but I'm not sure if it would keep up with long runs of use. Maybe. The biggest problem might be balancing the water temp and keeping it even. I wouldn't discourage you from trying it as long as you know (IMO) the biggest challenge will be to control to water temp consistency. Also, be careful about temp dew points when cold water cooling you laser.


---
**Nick R** *October 11, 2017 04:23*

Well, im considering replacing the thermostat in the bar fridge with an STC-1000 and putting the temperature sensor into the water itself near the inlet hose opening.  This way the fridge would act upon the water temperature, rather than the fridge temperature.

If its set to say, 19C then it wouldnt be too cold..  it would just depend on if the fridge could counter the heating effect of the tube effectively.


---
**Scorch Works** *October 12, 2017 00:11*

Cooling the air which in turn cools the water seems like an inefficient  setup. If at all possible try to get the cooling coil into the water or in contact with the water.  I am not sure how you would do it since the cooling coil is usually part of the freezer box structure.


---
**Nick R** *October 12, 2017 02:58*

I dont think, given the way the bar fridge I have is, that there would be a way to bend the pipe, but I thought about this some more, based on your above comment, and thought of this concept :   Have an aluminium tank like this [ebay.com.au - Details about High-per Universal Aluminum Alloy Coolant Overflow Expansion Reservoir Fuel Tank](http://www.ebay.com.au/itm/High-per-Universal-Aluminum-Alloy-Coolant-Overflow-Expansion-Reservoir-Fuel-Tank-/172679113323?epid=2082062686&hash=item28347a2a6b:g:b74AAOSwAPVZG~z9)  in the freezer section, which sits on the cooling pipes directly and then run a hose down to the secondary container that houses the main tank where you have the water pumped from.



Essentially, hot water from the laser tube goes into a small aluminium flat tank which has direct contact with coils within the freezer section, assisting the speed of the cooling, which then via convection, sends the water to the bottom main tank which is then used to pump to the laser tube.  The main tank would be about 20 - 30L in size, and would have to be a sealed unit.


---
**Scorch Works** *October 12, 2017 03:25*

Yes, that sounds like it would be better than cooling the air. 


---
*Imported from [Google+](https://plus.google.com/100109974643694541147/posts/fQsTT3GSsAL) &mdash; content and formatting may not be reliable*
