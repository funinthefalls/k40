---
layout: post
title: "After two years on and off doing mostly items for my wedding and that of friends I've finally made some money off this wretched machine that I love/hate"
date: December 14, 2016 10:37
category: "Object produced with laser"
author: "Linda Barbakos"
---
After two years on and off doing mostly items for my wedding and that of friends I've finally made some money off this wretched machine that I love/hate. Now I can justify a few upgrades :)



![images/532a68fc5c1602d5527d6fb5a025f99c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/532a68fc5c1602d5527d6fb5a025f99c.jpeg)
![images/8d53576732f3ce00448e03b95a1c9cb0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d53576732f3ce00448e03b95a1c9cb0.jpeg)

**"Linda Barbakos"**

---
---
**Cesar Tolentino** *December 14, 2016 15:24*

Really like it a lot.  Would you share where you get your packaging?


---
**Bob Damato** *December 14, 2016 15:42*

Those are great. I love the fonts, especially Isla and Georgia. Do you know what they are called?


---
**Linda Barbakos** *December 14, 2016 16:09*

**+Cesar Tolentino** The plastic pouch I bought from my local variety store (like a dollar store equivalent) they come in a variety of sizes. You can find them online [https://www.aliexpress.com/wholesale?catId=0&initiative_id=AS_20161214080802&SearchText=plastic+bag+with+adhesive+seal](https://www.aliexpress.com/wholesale?catId=0&initiative_id=AS_20161214080802&SearchText=plastic+bag+with+adhesive+seal). Then I cut out a cardboard backing and printed my logo on a A4 sheet and cut to size.

[aliexpress.com - Shop plastic bag with adhesive sealThen online Gallery - Buy plastic bag with adhesive sealThen for unbeatable low prices on AliExpress.com](https://www.aliexpress.com/wholesale?catId=0&initiative_id=AS_20161214080802&SearchText=plastic+bag+with+adhesive+sealThen)


---
**Linda Barbakos** *December 14, 2016 16:10*

**+Bob Damato** [dafont.com - Brushgyo Font &#x7c;](http://www.dafont.com/brushgyo.font)


---
**Cesar Tolentino** *December 14, 2016 16:32*

Very nice touch.  Thank u for the source


---
**John Riggs** *December 14, 2016 19:29*

What is the material you are cutting?




---
**Linda Barbakos** *December 15, 2016 00:18*

**+John Riggs** Material is 3mm acrylic which I then sprayed. 


---
*Imported from [Google+](https://plus.google.com/107262725887494713165/posts/SndT8dzmyzm) &mdash; content and formatting may not be reliable*
