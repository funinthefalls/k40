---
layout: post
title: "Went to clean the lens today for the first time and couldn't get the bottom section were the lens is held apart"
date: October 25, 2016 16:25
category: "Discussion"
author: "Nigel Conroy"
---
Went to clean the lens today for the first time and couldn't get the bottom section were the lens is held apart.



Anyone had this issue and managed to solve it?



Marked the head up a bit trying to get it undone





**"Nigel Conroy"**

---
---
**Scott Marshall** *October 28, 2016 10:06*

Gentle heat and a soak in penetrating oil ought to do it. (if it's not yet galled)



Aluminum (like stainless steel) galls. This a a very nasty property, that, once initiated is totally destructive. What happens is the 2 parts actually fuse under relatively low pressure and weld themselves together. For this to happen requires a clean surface (or usually bad luck and lack of lubrication). I don't claim to understand the molecule mixing that makes it happen, but sure know the feeling when it happens. You can have it happen just threading parts together by hand to test fit them. Once they start to grab, it's all over but the shouting. ALWAYS Lubricate the big 3 culprits (Aluminum, Stainless and copper - I believe some exotics like inconel are prone as well, probably because of the nickel content) before even touching the parts to each other.



If you soak it and get some oil in there, it will probably prevent galling as you back it out, but I expect it has already occurred.

 I'm guessing that the manufacturer galled it by putting it together dry.



If that's the case, you may as well order as new head(and lens), because that one is destroyed. If you have not yet upgraded to a quality air assist head with adjustable focus, now is a good time.



Lightly lubricate any Aluminum, copper, or stainless steel threads before assembly. (threadlocker like Loctite works as well if you need it's services but still want to protect your threads)

Silicone based grease is what is used in most industrial assembly, and it's clean, cheap and resists most chemicals and solvents (which is probably why it's so popular for this application. If you need just a dab, shop for the little pouches of "dielectric" or "light bulb" grease in your automotive section. If you want a larger supply, Ebay, or NAPA have it in small tube to large tubes.



In a pinch get ANY lube you have on dry Aluminum threads, even a drop of used motor oil off of a dipstick can save you in a pinch.



Scott


---
**Nigel Conroy** *October 28, 2016 16:35*

**+Scott Marshall** Helpful and informative as ever. Thank you



I'll have a go at the suggested solution and if I can't get it apart I'll go back to the supplier and/or get a different head.


---
**Nigel Conroy** *November 02, 2016 18:49*

So I did bring the head home and tried with a vise and pliers etc.. no luck.



I've contacted the seller and they are willing to send a replacement (30 days) or reimburse me for the cost of a new one I get myself.



Any suggestions for replacement head?

I'll attach some images of my current one.

![images/a1fbf84e5d29a680885c43acf7c3be22.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a1fbf84e5d29a680885c43acf7c3be22.jpeg)


---
**Nigel Conroy** *November 02, 2016 18:50*

![images/a452e9df9ec86e3e028fe1b69dd1ab65.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a452e9df9ec86e3e028fe1b69dd1ab65.jpeg)


---
**Nigel Conroy** *November 02, 2016 18:50*

Damaged part I can't get apart 

![images/ec7bb8d34e4f41510a63730327fc5296.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec7bb8d34e4f41510a63730327fc5296.jpeg)


---
**Scott Marshall** *November 02, 2016 18:57*

Looks like a well made head. I would say go with the replacement if it's not so effected. Check it and lube with silicone grease right away. tighten gently and maybe buy/make a spanner for the collar.



It may be the manufacturer is aware of the problem. If they have been putting them all together dry, you can't be the only one with a problem.



Scott


---
**Nigel Conroy** *November 02, 2016 19:17*

Thanks Scott, I was hoping I wouldn't have to wait that long. But I'll take your advise


---
**Scott Marshall** *November 02, 2016 19:30*

You may be able to find that exact head on ebay, I'd start with Saite Cutter (they have a direct website as well) and search, looking for that exact head. They are only a few styles being used in the cheap cutters, and I'll bet you can find that exact one (so it will be a drop in) and then just bill your seller (don't let them delay tactic you, shipping from anywhere is <10 days in this day and age... .

If you're in a hurry, maybe take the replacement and buy a replacement (which will ship sooner), then you can have a spare ready maybe with a different Focal length lens in it for thick cutting or fine engraving or whatever suits you.



Last chance, get brutal and pipe wrench it, it may wipe it out, but it may come apart enough to make it usable until the new one comes (you can always grind off the threads and tape/glue it together for now - IF the lens survives the brute force extraction.)



I have to wrap it for now, feeling a bit ragged.



Good luck!




---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/faXrYG2dySy) &mdash; content and formatting may not be reliable*
