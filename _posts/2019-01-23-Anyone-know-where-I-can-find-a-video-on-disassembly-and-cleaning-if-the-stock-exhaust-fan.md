---
layout: post
title: "Anyone know where I can find a video on disassembly and cleaning if the stock exhaust fan?"
date: January 23, 2019 03:02
category: "Original software and hardware issues"
author: "'Akai' Coit"
---
Anyone know where I can find a video on disassembly and cleaning if the stock exhaust fan? I know it's not the greatest, but I would like to keep mine alive until I can get around to getting something better. On that note, any recommendations? 





**"'Akai' Coit"**

---
---
**'Akai' Coit** *January 24, 2019 09:44*

Figured out how to disassemble and cleaned it tonight (2 or 3 hours in total, but most of that was cleaning because it REALLY needed it). Process wasn't too bad. Will take pics and post next time I do it (which may be soon since I don't feel like I cleaned it thuroughly enough).


---
**ki ki** *January 24, 2019 15:29*

I also took mine apart and cleaned it. I also put a washer in the 'axle' so the fan part was closer is closer to the case, not sure it made a huge difference... but in all its easy enough to take apart and rebuild.


---
**'Akai' Coit** *January 25, 2019 04:41*

I need to find out what size washer to use for that. I ended up using an o-ring I had on hand since all the washers I had were to small or too big. :/


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/DkMLwQVco67) &mdash; content and formatting may not be reliable*
