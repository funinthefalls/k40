---
layout: post
title: "What to do?? My water pump stopped working and I didn't notice for quite a while..."
date: March 14, 2017 11:08
category: "Original software and hardware issues"
author: "Andreas Benestad"
---
What to do?? My water pump stopped working and I didn't notice for quite a while... so when I realized I checked my laser tube and the end bit had popped out like this. Any input friends?

Can it be glued on..?

![images/09e307f5dffb906ce0c3d18f8b400d16.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09e307f5dffb906ce0c3d18f8b400d16.jpeg)



**"Andreas Benestad"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 14, 2017 11:38*

Someone else recently had this same issue (not sure the water flow was their cause though) on a new tube they had bought & the seller told them to glue it on using glass glue. Not sure if that is the best idea or not?


---
**Andreas Benestad** *March 14, 2017 11:55*

Cool. Yeah, someone else on Facebook just said that "superglue is your best friend, as long as the tube isn't fried."

So I have superglued it back on (just regular superglue). Seems to be working fine, so I will just have to keep a close eye on it.


---
**Roberto Fernandez** *March 14, 2017 12:05*

**+Andreas Benestad** , I think standar superglue is not good for glass. There is a glass version, look in a cars spare parts store, there is a special superglue for mirrors.


---
**Don Kleinschnitz Jr.** *March 14, 2017 12:07*

I have heard of them being glued back on but never followed up to see if it worked. 

Tenuous to keep the glue off the output glass area. 

I think that I would try to put a very small amount of epoxy glue around the outer edge of the  water jackets mating surface, with a toothpick, then hold it in place with some kind of wedge or maybe tape around the nose of the tube.

Most of the links I found suggest that when this fails so does the seal at that end and you get leak down. But what do you have to loose.



Now its time to get a water flow sensor :)!



I found this supposedly made-for-glass glue: [amazon.com - Loctite Glass Glue 2-Gram Tube (233841) - General Purpose Glues - Amazon.com](https://www.amazon.com/Loctite-Glass-Glue-2-Gram-233841/dp/B000PSBBM8)




---
**Andreas Benestad** *March 14, 2017 12:11*

**+Don Kleinschnitz** Yeah, a water flow sensor has been on my mind for quite some time. Suddenly that thought became a little more urgent... :-) 

I have already glued it on with superglue, I think I was able to keep it off the glass output area. It seems to be cutting at the same power as before. Not sure if I trust the glue though..will be checking up on it quite regularly.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 14, 2017 12:18*

Keep us posted with any info on the performance/glue job over time **+Andreas Benestad**. Will be good to know the long-term results of your fixup job.


---
**Ned Hill** *March 14, 2017 12:46*

If you are going to use a cyanoacrylate glue like super glue I would recommended that you mask the mirror first because of the fuming nature of the glue.


---
**Don Kleinschnitz Jr.** *March 14, 2017 12:48*

**+Ned Hill** heh I was thinking of the same thing but was afraid that the mask would wick the glue across the face of the jacket and then you could not get it off :(.


---
**Chris Hurley** *March 15, 2017 02:37*

white gorilla glue worked for me. I used sponge to keep it in place using the mirror as a stop. I didn't use supper glue as it does not like temperature changes.  


---
**Don Kleinschnitz Jr.** *March 15, 2017 03:05*

**+Chris Hurley** you were able to put your tube back in operation?


---
**Gunnar Stefansson** *March 15, 2017 11:19*

Hehehe, **+Yuusuf Sallahuddin** It was me :) I epoxied it back on again and everything has been fine ever since. 


---
**Don Kleinschnitz Jr.** *March 15, 2017 11:20*

**+Gunnar Stefansson** what did you do to keep the glue from getting in the wrong places. Did you mask anything? What kind of epoxy?


---
**Gunnar Stefansson** *March 15, 2017 11:39*

**+Don Kleinschnitz** Well I was just careful I guess ;) And I used standard 2component Epoxy from the hardware store... And it's been a year now and everything is fine... The seller suggested using epoxy over superglue, as it has better resistence to the rapid temperature difference...


---
**Don Kleinschnitz Jr.** *March 15, 2017 11:40*

**+Gunnar Stefansson** nice this is good to know .....


---
**Gunnar Stefansson** *March 15, 2017 11:47*

**+Don Kleinschnitz** oh almost forgot, I removed as much of the old glue with a sharp knife and fine sandpaper! to help it stick better. Suggest you do the same! ;)


---
**Don Kleinschnitz Jr.** *March 15, 2017 11:48*

**+Gunnar Stefansson** thanks 




---
**Chris Hurley** *March 19, 2017 23:49*

Yes it did work. It's still on but I had a preexisting condition with the tube causing me to replace it. As for cleaning the tube I didn't as I was worried it would damage the lens. I'm actually thinking of ways I can still use the old tube. Maybe just for low power stuff (the lens is not focusing the full beam.)


---
*Imported from [Google+](https://plus.google.com/112581340387138156202/posts/Jz9gAKuSfTc) &mdash; content and formatting may not be reliable*
