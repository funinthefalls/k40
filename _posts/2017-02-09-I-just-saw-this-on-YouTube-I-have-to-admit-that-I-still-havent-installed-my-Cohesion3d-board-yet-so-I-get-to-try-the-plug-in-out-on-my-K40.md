---
layout: post
title: "I just saw this on YouTube I have to admit that I still haven't installed my Cohesion3d board yet so I get to try the plug in out on my K40"
date: February 09, 2017 19:33
category: "Discussion"
author: "Jim Fong"
---
I just saw this on YouTube




{% include youtubePlayer.html id="7kZaHvAZel8" %}
[https://youtu.be/7kZaHvAZel8](https://youtu.be/7kZaHvAZel8)



I have to admit that I still haven't installed my Cohesion3d board yet so I get to try the plug in out on my K40.  





**"Jim Fong"**

---
---
**Jim Hatch** *February 09, 2017 20:33*

Nice find for those who aren't using Corel. Here's the Inkscape extension link: [scorchworks.com - Laser Draw (LaserDRW) - Inkscape Extension](http://scorchworks.com/LaserDRW_extension/laserdrw_extension.html)



I like the way he packages the output files by function so you don't need to do it by hand (in Corel I do them as layers and fire them one at a time).


---
**Cesar Tolentino** *February 09, 2017 23:10*

Thanks. This is a great find. It sure makes the work flow easier


---
**1981therealfury** *February 10, 2017 14:14*

Wow nice find indeed.  This will take a lot of the headache out of my multi tasked jobs.  cheers for sharing.


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/ebTLEyZvEHz) &mdash; content and formatting may not be reliable*
