---
layout: post
title: "Yea! the GLCD screen has arrived !"
date: December 12, 2016 22:29
category: "Smoothieboard Modification"
author: "greg greene"
---
Yea! the GLCD screen has arrived !



I got the GLCD screen to go with my Cohesion 3D board, figured out where to connect it and now I have a wonderful blue screen !  The panel has an SD card - so I'm expecting there is a program that goes on the card to wake it up so it's more than a mood enhancer.  Can anyone point me to one?



Thanks All, - the journey continues





**"greg greene"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 22:56*

The config file contains a glcd section which is commented out with the # symbol.  There's a folder with glcd uncommented for you in the dropbox folder. 


---
**greg greene** *December 13, 2016 00:55*

Thanks ray !


---
**greg greene** *December 13, 2016 01:01*

Hmm can't seem to find the dropbox now


---
**greg greene** *December 13, 2016 01:56*

got it Ray - Thanks


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/Sb2YmYfiWE3) &mdash; content and formatting may not be reliable*
