---
layout: post
title: "Just thought I would post some pics of my K40 as it is to date"
date: August 19, 2016 23:26
category: "Modification"
author: "Eric Flynn"
---
Just thought I would post some pics of my K40 as it is to date. Still a lot to do!!



![images/b39829ac87c46854834e14749cd5c9fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b39829ac87c46854834e14749cd5c9fd.jpeg)
![images/357b806441b1c7a395a479ae62d8efe0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/357b806441b1c7a395a479ae62d8efe0.jpeg)

**"Eric Flynn"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 19, 2016 23:44*

Does the red make it go faster? I like it. Looks good.


---
**Eric Flynn** *August 19, 2016 23:45*

Yep, adds 10W, and a few hundred mm/s :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 19, 2016 23:49*

**+Eric Flynn** You're going to have to add a spoiler on it too!


---
**I Laser** *August 20, 2016 00:08*

The red ones are K41's :P


---
**David Cook** *August 21, 2016 07:09*

Looks great 


---
*Imported from [Google+](https://plus.google.com/107613521854079979112/posts/drKZNKipGE2) &mdash; content and formatting may not be reliable*
