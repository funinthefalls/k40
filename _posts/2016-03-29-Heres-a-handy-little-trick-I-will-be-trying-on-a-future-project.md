---
layout: post
title: "Here's a handy little trick I will be trying on a future project!"
date: March 29, 2016 13:32
category: "Discussion"
author: "Ulf Stahmer"
---
Here's a handy little trick I will be trying on a future project!



[http://hackaday.com/2016/03/28/two-sided-laser-etching/](http://hackaday.com/2016/03/28/two-sided-laser-etching/)





**"Ulf Stahmer"**

---
---
**Scott Marshall** *March 30, 2016 01:42*

Looks like there ought to be 1 more step (3a), at the flip, you're going to have to rotate the rear image 90 deg ccw to align it with the front image for the keyed corner to re-engage.


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/C83p1uTDAZT) &mdash; content and formatting may not be reliable*
