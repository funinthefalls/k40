---
layout: post
title: "Finally have the table and air assist head installed, have to cut the vent duct off 2 inches, but with the air assist I don't think it will matter"
date: November 18, 2015 22:21
category: "Hardware and Laser settings"
author: "Scott Thorne"
---
Finally have the table and air assist head installed, have to cut the vent duct off 2 inches, but with the air assist I don't think it will matter.

![images/5e12e482417f69ade3bfeb2dc88f7bd2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5e12e482417f69ade3bfeb2dc88f7bd2.jpeg)



**"Scott Thorne"**

---
---
**David Richards (djrm)** *November 18, 2015 23:02*

Hi Scott, that looks fine. I completely removed the air duct on my machine. I think it only adds resistance to the flow of air, especially once the cutting head is away from the back of the case. The bigger pump and air assist help too. David.


---
**Scott Thorne** *November 19, 2015 00:24*

Thanks David, I just tried it out and it works great.


---
**Coherent** *November 19, 2015 00:35*

Take that air duct completely out. (It sucks) err the smoke out through the larger hole it leaves just fine. And if you upgrade to a better exhaust fan, it will work even better. You have to unbolt the x/y frame to get it out.  Just recheck your x/y squareness and your mirror alignment when you're done... they will likely be off a little.


---
**Scott Thorne** *November 19, 2015 14:02*

Thanks Marc G.....that's gonna be my weekend project.


---
**Bharat Katyal** *November 21, 2015 17:20*

Did you build that table or buy it from somewhere already made?


---
**Scott Thorne** *November 21, 2015 17:21*

I bought it from lightobjects.


---
**DIY3DTECH.com** *November 22, 2015 12:57*

I also reworked my exhaust duct (you can see a video here: 
{% include youtubePlayer.html id="4DXWD274yTk" %}
[https://youtu.be/4DXWD274yTk](https://youtu.be/4DXWD274yTk)).  I now use my Rigid Shop Vac and 1.5" duct tube which I also use for my CNC venting outside and with suction of the shop vac and the fact I sealed the exhaust chamber, there are not issues evacuating the fumes.  Also just did the air assist (with Light object head) this weekend too.




---
**Bharat Katyal** *November 23, 2015 00:13*

did you have to remove the rails to get the vent out? Mine doesn't seem like it wants to come out without it. Also is the goal to have the vent be leveled with the cutting surface?


---
**Scott Thorne** *November 23, 2015 00:17*

I had to remove the x,y rails it's only 4 bolts to remove it, but I'm glad I listened to Marc G and David Richards because it works so much better now.


---
**David Richards (djrm)** *November 23, 2015 00:19*

Hi, i think the vent came out the back without any problems. D


---
**Scott Thorne** *November 23, 2015 00:46*

I wish mine would have, it came out from the inside, but I'm glad I took the rails out because it allowed me the chance to find out that the rails were over 1/4 inch out of square.


---
**Bharat Katyal** *November 23, 2015 00:59*

[http://imgur.com/27d1Fig](http://imgur.com/27d1Fig)

[http://imgur.com/BRgu9FV](http://imgur.com/BRgu9FV)



Theres no coming out on this. if I did take the air duct out what would be a cheap solution for a vent?

Also should the vent  be on same level as acrylic? Mine isn't cutting through due to the paper, it just wants to burn paper, without paper it cuts fine but leaves marks. I'm thinking about going to walmart to try to get another air compressor to see if that helps! Im going to be up all night cutting


---
**Scott Thorne** *November 23, 2015 01:02*

If it's leaving marks maybe the head is not lined up correctly or the mirrors, mine was leaving a little tail when cutting, the laser was way out of alignment.


---
**Bharat Katyal** *November 23, 2015 01:15*

But actually it was working just fine at some point. What happened was it started burning in certain corners at some point. I would also level the acrylic I'm cutting up with 2 3mm (total 6mm from bed)acrylics 


---
**Bharat Katyal** *November 23, 2015 01:16*

I'm curing mirror acrylic and black. Mirror is cutting without a problem! The black however is having issues related to burning paper. I can cut without paper just fine


---
**Scott Thorne** *November 23, 2015 01:26*

Oh ok....then I would say that everything is lined up, I started using more air pressure with the laser head and it made a big difference when cutting.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/KM6cCGsrLuA) &mdash; content and formatting may not be reliable*
