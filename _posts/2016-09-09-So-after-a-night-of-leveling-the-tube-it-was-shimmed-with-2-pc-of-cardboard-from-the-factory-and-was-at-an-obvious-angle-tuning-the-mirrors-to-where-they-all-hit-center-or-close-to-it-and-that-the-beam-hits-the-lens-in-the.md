---
layout: post
title: "So after a night of leveling the tube (it was shimmed with 2 pc of cardboard from the factory and was at an obvious angle), tuning the mirrors to where they all hit center or close to it and that the beam hits the lens in the"
date: September 09, 2016 13:49
category: "Discussion"
author: "J.R. Sharp"
---
So after a night of leveling the tube (it was shimmed with 2 pc of cardboard from the factory and was at an obvious angle), tuning the mirrors to where they all hit center or close to it and that the beam hits the lens in the center...I went ahead and built a rough bed surface out of honeycomb steel and some bolts. I was finally able to cut through 3mm birch ply, but struggled with the plywood from lowes. It seems as if the plywood from lowes is junk and has a main center with 2 thin veneers on both sides. Should I just say this wood is for engraving only and cut my losses or does everything think I may still have a shit laser?







**"J.R. Sharp"**

---
---
**J.R. Sharp** *September 09, 2016 14:23*

Thanks. I noticed the birch was great 3 ply all the way through...but the stuff from lowes looked way different in construction. I have some thinner, maybe 6mm hardwood somewhere. Interested to try that.


---
**J.R. Sharp** *September 09, 2016 14:24*

3 passes on 3mm Baltic at 15mm/s and 10mw will cut through. Does this seem out of the realm of normality? Any better suggested settings?


---
**Ariel Yahni (UniKpty)** *September 09, 2016 14:27*

I would say it's to much to, 1mm per pass. But I haven't tried that specific kind of ply. Also tube water temp can be affecting your cut. 


---
**J.R. Sharp** *September 09, 2016 17:04*

What should my speed / power be? Which would yield the cleaner cut? 1 pass or 3? Water temp is cool, not heating up much at all. Going to be switching to glycol/radiator cooling soon.




---
**Ariel Yahni (UniKpty)** *September 09, 2016 17:16*

My temp was cool to the touch and the placed a probe to find out I was at 30C withiut running the tube or pump. More passes would mean the beam could deflect and the cuts could not be 90 degrees which I hate. The fastest you can get with the less amount of heat in one pass is my preference. For instance I get very good result on a local ply (I'm not in the US) where I do 3mm with 10mA @ 10mms.


---
*Imported from [Google+](https://plus.google.com/116586822526943304995/posts/5oCXKnWV7NF) &mdash; content and formatting may not be reliable*
