---
layout: post
title: "Some of my first engravings!!!"
date: December 31, 2017 15:32
category: "Object produced with laser"
author: "Jeff Lin Lord"
---
Some of my first engravings!!!   



![images/be53b18cb4133f632156c4cbca49a16f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be53b18cb4133f632156c4cbca49a16f.jpeg)
![images/78193f3775ddbdd57962fa5f4132dbf5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/78193f3775ddbdd57962fa5f4132dbf5.jpeg)
![images/d8a58627122403e04dabeac9c54afd00.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d8a58627122403e04dabeac9c54afd00.jpeg)
![images/bab61e18ba7bd6666a03986dda367083.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bab61e18ba7bd6666a03986dda367083.jpeg)
![images/f0db0143c5a065325c120afcfc192cbb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f0db0143c5a065325c120afcfc192cbb.jpeg)

**"Jeff Lin Lord"**

---
---
**Ned Hill** *December 31, 2017 22:17*

They look great.  Welcome to your new addiction ;)


---
**Jeff Lin Lord** *January 02, 2018 03:30*

Thanks!!    


---
*Imported from [Google+](https://plus.google.com/105510525760807493549/posts/emBCfD2PGvn) &mdash; content and formatting may not be reliable*
