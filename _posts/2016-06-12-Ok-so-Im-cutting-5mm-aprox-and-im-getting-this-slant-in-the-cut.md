---
layout: post
title: "Ok so I'm cutting 5mm aprox and im getting this slant in the cut"
date: June 12, 2016 22:20
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Ok so I'm cutting 5mm aprox and im getting this slant in the cut. 2 pass is being used.  Is this optical, mechanical or focal? Or all of the above



Consider that both sides are slanted in the same direction from top to bottom ( top being the far end of the machine ). Side themselves ( left and right )  are ok 

![images/5f9d4aad0d4bb5700fa751c9c6f5f0c7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5f9d4aad0d4bb5700fa751c9c6f5f0c7.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Alex Krause** *June 12, 2016 22:33*

Set the focal distance to the middle of the work piece, and make sure the piece is level/parallel to the gantry there will still be a bit of this becuse of how the focal lens works


---
**Scott Marshall** *June 12, 2016 22:35*

All - almost.



Optically, the focus is paper thin, it widens on either side of the focal point. You may want to try the 1st pass focused on the top surface, and the 2nd pass focused at the center of depth. works in plywood, I haven't tried it in acrylic.



Also, the beam scatters when it hits the material, causing the widening. That may be the reason the trick above works. You won't get it like a saw cut, but you can get close.. Gluabe if you use the thick stuff.



Lastly, if your beam isn't hitting the exact center of the lens, it cants, you have to adjust head height and rotation to sort it out.


---
**Ariel Yahni (UniKpty)** *June 12, 2016 23:12*

Its going to be a real hassle to get focus on the middle without some kind of  z bed. Once you move the piece its difficult to place it exactly in the same spot. What are you gyus doing?


---
**Scott Marshall** *June 13, 2016 00:09*

I have the Saite Air Assist head, it's got about 3/8" of easy adjustable focus. Worth the 35 bucks just for that.



I burn the 1st pass, open the lid, adjust the nozzle down with a stop block placed gently on the work surface, close up and do the 2nd burn.


---
**Ariel Yahni (UniKpty)** *June 13, 2016 00:35*

 **+Scott Marshall**​ that's almost 10mm, very nice. You don't need a bed with that, at least for cutting


---
**Scott Marshall** *June 13, 2016 00:43*

This is the one:



It is a bit of a fussy install, you have to open  the factory hole,  make some shims to shift it to get the adjustment right, well worth it.



[http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050](http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050)



They also have a loaded one with mirror and lens installed.


---
**Alex Krause** *June 13, 2016 00:55*

Is that the air assist that needs a new adaptor for the gantry mount?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 13, 2016 01:28*

**+Scott Marshall** You mention that if the beam isn't hitting the centre of the lens it cants, which makes sense. What I'm wondering is if you had a larger diameter lens, would the degree of sensitivity to hitting the centre reduce? & would the angle of cant be lessened?


---
**Jim Hatch** *June 13, 2016 02:31*

**+Alex Krause**​ yes. Or drilling out the stock mounting plate.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/VECnhk3sS6b) &mdash; content and formatting may not be reliable*
