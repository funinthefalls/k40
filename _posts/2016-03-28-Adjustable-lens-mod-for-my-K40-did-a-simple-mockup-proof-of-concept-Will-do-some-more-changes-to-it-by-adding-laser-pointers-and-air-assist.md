---
layout: post
title: "Adjustable lens mod for my K40, did a simple mockup, proof of concept :) Will do some more changes to it, by adding laser pointers and air assist"
date: March 28, 2016 14:25
category: "Modification"
author: "HP Persson"
---
Adjustable lens mod for my K40, did a simple mockup, proof of concept :)



Will do some more changes to it, by adding laser pointers and air assist.

And re-cut it in 4mm acrylic.



Doing a custom bed also, so haven´t tried it fully yet but it works perfect, when tightened its level in all directions so it should work nice!




{% include youtubePlayer.html id="5Id7vQNlNAo" %}
[https://youtu.be/5Id7vQNlNAo](https://youtu.be/5Id7vQNlNAo)





**"HP Persson"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 28, 2016 16:08*

That's a great idea. Rather than z-table adjustment to move the work piece, you can just move the lens up or down. Seems much easier.


---
**HP Persson** *March 28, 2016 16:13*

Yeah, the bed I'm making is just a acrylic sheet full of 1" nails as standoffs. Not sure if it will work, but the k40 is all about exploring and testing ☺


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 28, 2016 17:07*

**+HP Persson** That's an interesting idea for the bed too. Seems you like to think outside the box. I like it. Just make sure to keep the laser inside the box, else someone may get burned (it hurts too).


---
**I Laser** *March 28, 2016 22:16*

Would be so much easier. I now have two machines with different depth beds, so way too many jigs...



I'm having fun trying to level the x gantry on my second machine (beam is internally hitting the air assist nozzle), so that air assist looks like a way easier setup too. Does it work well? I know the gantry should be level otherwise you get a bevel effect, but for some engraving I don't care. :)



**+Yuusuf Sallahuddin** (it hurts too) - hope that's not from experience!


---
**HP Persson** *March 28, 2016 22:28*

Been cutting some 3mm acrylic tonight with the new adjustable head and the fan.

It moves away most of the smoke, compared to my air nossle from the airbrush pump (30L/m) it´s about the same. $7 fan VS $80 airbrush compressor...



Only negative thing with the fan is the smoke below the material stays longer. 

With the air nossle, it shoots trough the cut and moves the smoke below too.



Still running the exhaust-fan delivered with the machine, upgrading that will probably bring me some changes in the smoke too :)

And i´ll probably add some more fans below the bed, just to help move the smoke to the back of the machine.


---
**Joe Spanier** *March 29, 2016 01:57*

Where did you get that mirror lens mount. Is that what came with your k40?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 29, 2016 02:16*

**+I Laser** Definitely from experience. I've shot myself in the hands too many times while performing alignments of the mirrors.


---
**HP Persson** *March 29, 2016 07:18*

**+Joe Spanier** Check out this URL with pics of my K40 when delivered -> [www.wopr.nu/laser](http://www.wopr.nu/laser)


---
**Don Kleinschnitz Jr.** *March 29, 2016 16:11*

Have you had any issues with misalignment of the objective lens when you move it like this. If the objective lens is at an angle the edge of the cut will not be square.


---
**HP Persson** *March 29, 2016 16:23*

**+Don Kleinschnitz** Not anything yet, have only cut for 10-15 minutes yet with it, 2-3mm acrylic and the edge is perfect.



I´ll try to set it misaligned on purpose, to see what it does on the next run!


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/fLotaxyGdkf) &mdash; content and formatting may not be reliable*
