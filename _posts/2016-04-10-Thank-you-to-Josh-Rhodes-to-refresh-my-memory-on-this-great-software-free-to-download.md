---
layout: post
title: "Thank you to Josh Rhodes to refresh my memory on this great software free to download"
date: April 10, 2016 11:38
category: "Software"
author: "Stephane Buisson"
---
Thank you to **+Josh Rhodes** to refresh my memory on this great software [http://www.123dapp.com/make](http://www.123dapp.com/make)

free to download.



a good complement to Sculptris (.obj export)





**"Stephane Buisson"**

---
---
**Stephane Buisson** *April 10, 2016 11:38*


{% include youtubePlayer.html id="JImOkZFOxAQ" %}
[https://www.youtube.com/watch?v=JImOkZFOxAQ&nohtml5=False](https://www.youtube.com/watch?v=JImOkZFOxAQ&nohtml5=False)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 13:26*

I second that thankyou, not for refreshing my memory, but for introducing me to this software. I am also testing out Autodesk 123d Catch (photo to 3d model software). Could come in handy if it works well enough.


---
**Josh Rhodes** *April 10, 2016 18:04*

It's good to run through their tutorials. 



Unfortunately the time I used it, it would not give me nearly the detail I wanted.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/HS2d1ZVfGat) &mdash; content and formatting may not be reliable*
