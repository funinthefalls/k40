---
layout: post
title: "laser cutting INVOLUTE GEAR : It's amazingly simple with this Sketchup plugin"
date: December 14, 2015 15:01
category: "External links&#x3a; Blog, forum, etc"
author: "Stephane Buisson"
---
laser cutting  INVOLUTE GEAR :

It's amazingly simple with this Sketchup plugin.





**"Stephane Buisson"**

---
---
**Stephane Buisson** *December 14, 2015 15:23*

Sketchup (2015&2016) to SVG (inkscape):

[https://github.com/JoakimSoderberg/sketchup-svg-outline-plugin](https://github.com/JoakimSoderberg/sketchup-svg-outline-plugin)


---
**Gary McKinnon** *January 13, 2016 12:06*

**+Stephane Buisson** Love that plugin. I also like this one for spirals : [https://extensions.sketchup.com/en/content/curve-maker](https://extensions.sketchup.com/en/content/curve-maker)


---
**Stephane Buisson** *January 13, 2016 13:08*

I know that one it was before here

[http://www.drawmetal.com/](http://www.drawmetal.com/)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/1a3XfZUHSJP) &mdash; content and formatting may not be reliable*
