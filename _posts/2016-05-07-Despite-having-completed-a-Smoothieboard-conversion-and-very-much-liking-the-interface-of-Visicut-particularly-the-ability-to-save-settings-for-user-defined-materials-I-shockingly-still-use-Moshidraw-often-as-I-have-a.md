---
layout: post
title: "Despite having completed a Smoothieboard conversion and very much liking the interface of Visicut (particularly the ability to save settings for user-defined materials), I shockingly still use Moshidraw often (as I have a"
date: May 07, 2016 21:45
category: "Software"
author: "Vince Lee"
---
Despite having completed a Smoothieboard conversion and very much liking the interface of Visicut (particularly the ability to save settings for user-defined materials), I shockingly still use  Moshidraw often (as I have a switchable converter board), and the latest version (2016x) is a big leap from previous versions and still has features I sometimes can't find in Visicut, such as the ability to draw and edit images within the app (great for quick projects or changes), detect and retry for communication errors when talking to the engraver (visicut often drops the connection), correct for hysteresis when doing bidirectonal engraving or to engrave from any direction. 



Still, the English translation is painfully horrible which makes these features hard to find, so I decided to try to fix the translation problems myself and put together a manual for the program for anybody who is still using Moshidraw.  I had to guess at some stuff as I can't read Chinese, so please let me know of places where I guessed wrong or otherwise missed something.



English translation File (for 2016x only, replace in install folder):

[http://www.tealpoint.com/_vince/k40/english.ini](http://www.tealpoint.com/_vince/k40/english.ini) 



Manual Doc: [https://docs.google.com/document/d/1sHOAcbXneLrBOZVRUsUdrzp36M0himbGvIeqiyRdWM4/edit?usp=sharing](https://docs.google.com/document/d/1sHOAcbXneLrBOZVRUsUdrzp36M0himbGvIeqiyRdWM4/edit?usp=sharing)





**"Vince Lee"**

---
---
**Don Kleinschnitz Jr.** *May 08, 2016 12:00*

Vince are you using Visicut with your smoothie? Did you do a conversion of K40 to smoothie, if so do you have a log?


---
**Vince Lee** *May 08, 2016 22:25*

**+Don Kleinschnitz** Yes, I'm using Visicut.  I tried Laserweb but my machine is too slow to run a browser-based program like it and it couldn't find my serial port anyway.  But I really like the Visicut interface so far and look forward to it continuing to improve.  I buildlog for my smoothie conversion is here: [http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html](http://makermonkey.blogspot.com/2016/04/hacking-k40-laser-cutter-buildlog.html)


---
**Don Kleinschnitz Jr.** *May 08, 2016 23:53*

**+Vince Lee** Thanks for the link, a really nice build and your success drives me one step more toward a smoothie board :). 

Do you sell the switch over boards? I guess that wont help me since I have a nano board huh?

Do you have a schematic of the whole smoothie setup? 

I am on my last design steps and working out the stepper interface and end stops (mine are optical, yours?) then the LPS control. 


---
**Vince Lee** *May 09, 2016 03:33*

**+Don Kleinschnitz** I have a few spare boards still. Private message me your address and I can send you one.  I had thought of selling them, but to be worthwhile, I think it would need to include all the parts or even be preassembled, and it probably wouldn't be worth my time for what I could charge.



Even with a nano, if you have a ribbon cable I think the switcher would work.  Looking at photos, the connectors appear to be the same.  The only thing you might need to do is add a jumper between the first and last pins of the power connector, which are both ground.  I left the first pin unconnected in my adapter board because the cable from my power supply used the last wire, but if yours uses the first wire instead you can just tie them together.  The ribbon and Y stepper lines are probably the same (but check for sure!).  



My build log has the only diagram I have, but the log also links to the source files of the adapter board if you want to look at them or modify them to make your own version.  It only cost me $20 total to get a stack of them made and shipped; the month-long wait was the most painful part.


---
**Don Kleinschnitz Jr.** *May 09, 2016 13:13*

Wow thanks for the help. How do I private message you ..... duh!


---
**Don Kleinschnitz Jr.** *May 09, 2016 21:01*

Also the link to source has .grb files. Do you have a .sch file?


---
**Tony Sobczak** *May 14, 2016 00:48*

**+Vince Lee**​ do you have a parts list?    I'm not sure as to what I need to order.      


---
**Vince Lee** *May 14, 2016 01:20*

No I don't have a parts list but off the top of my head you need 3 of the exact model dip switches indicated on the board.  Everything else are plugs and jacks duplicating what is already used with your nano.  For the power connector you need two 6-pin Jacks like the one on the nano and two matching plugs to make a jumper from the board to the nano. If your engraver has an ffc you need two jacks and a short ffc to be used as a jumper.  Else if you have a separate xmotor connector you'll need matching plugs, probably xh2.54 4-pin headers. 


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/95yC1FAEaM2) &mdash; content and formatting may not be reliable*
