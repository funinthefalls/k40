---
layout: post
title: "Hey guys, I am prototyping a openbuilds gantry in Fusion 360 to increase my cutting space"
date: September 09, 2016 18:44
category: "Discussion"
author: "John Riggs"
---
Hey guys, I am prototyping a openbuilds gantry in Fusion 360 to increase my cutting space. Does anyone know if there is a CAD file of the laser tube? Would help to make it, thanks!





**"John Riggs"**

---
---
**Jim Hatch** *September 09, 2016 18:56*

I think there's one in the lasersaur GIT project.


---
**John Riggs** *September 12, 2016 18:00*

You guys are awesome!




---
*Imported from [Google+](https://plus.google.com/107437989596447066794/posts/6GrZmMiUgne) &mdash; content and formatting may not be reliable*
