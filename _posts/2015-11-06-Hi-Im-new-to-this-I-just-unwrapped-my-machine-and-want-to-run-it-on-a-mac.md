---
layout: post
title: "Hi Im new to this - I just unwrapped my machine and want to run it on a mac"
date: November 06, 2015 21:19
category: "Discussion"
author: "little savage"
---
Hi Im new to this -

I just unwrapped my machine and want to run it on a mac.  Does anyone so so successfully out there?

thankyou!





**"little savage"**

---
---
**Ian Hayden** *November 07, 2015 00:23*

i tried via vmware fusion and windows 7 on a macbook pro.  Couldnt get the k40 to see the license dongle.  Didnt spend much more time trying.  you could try boot camp as an option.  If anybody has managed it, i would be interested too.   


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2015 05:17*

I am running via Windows 10 on bootcamp on macbook pro. Works fine. Doesn't register the usb license key via OSX for me either.


---
**Bharat Katyal** *November 11, 2015 09:26*

Im using windows 7 on parallel and works fine!!


---
**Michael Reese** *January 04, 2016 19:50*

I just got a machine and am trying to run it on a Macbook pro retina through Bootcamp.  MoshiDraw does not recognize the usb dongle and gives me the "insert soft key" error message. 

I have tried Moshi2015, 2014, Left, etc... same result.  Any suggestions?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 05, 2016 06:08*

To add to my previous comment, just for clarification, I am not using MoshiDraw, but the LaserDrw plugin for Corel Draw. Unfortunately I cannot suggest anything Michael other than update your Bootcamp drivers to the newest versions & try again. I can't see any reason why it wouldn't be working if Windows works through bootcamp & the USB ports work (for other devices).


---
**Michael Reese** *January 05, 2016 18:39*

**+Yuusuf Sallahuddin**



Yuusuf is correct and I was trying MoshiDraw when I should have been using the LaserDrw and Corel package.  I got ahead of myself when my Macbook didn't have a CD drive and I pulled the software I thought I should be using from the web instead of that which shipped with the machine.  A new cd drive and a quick look yesterday shows that it should now work.  I'm waiting on mirror alignment and other needs before a full scale test.  The fan arrived broken with the x-bar bent around it and the first mirror was dangling from the mounts.  So there is some work to do first.



After so much research and anticipation of fighting with MoshiDraw it appears that (most of?) the latest machines are LaserDRW and Corel with no need for MoshiDraw.



So I hope that helps someone else. 

1. Don't be afraid of getting MoshiDraw.

2. Read the directions.

3. Listen to Yuusuf.   ;-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 05, 2016 20:04*

**+Michael Reese** Haha, I'm actually new to all this stuff too, but I like point #3.



Wow, sounds like some serious issues with your machine. I wonder, do they not offer to replace that or refund some money?


---
*Imported from [Google+](https://plus.google.com/100427771477556593667/posts/bdb5E7wsDkE) &mdash; content and formatting may not be reliable*
