---
layout: post
title: "So I've just done an interesting back to back test with and without air assist running, otherwise there was no other changes"
date: February 01, 2017 15:56
category: "Object produced with laser"
author: "Rob Kingston"
---
So I've just done an interesting back to back test with and without air assist running, otherwise there was no other changes. 



From top to bottom 1 & 3 are with air assist on, 2 & 4 are without.



Now to me the ones with air assist look no better - in fact, potentially look worse? Anyone else experienced no gains/ negative effect from air assist? 

![images/f4fa31a56a20a7abfa11d372bf506850.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4fa31a56a20a7abfa11d372bf506850.jpeg)



**"Rob Kingston"**

---
---
**Stephane Buisson** *February 02, 2017 07:00*

Air assist is mostly  to cut. 

during cutting had several effects, first it help combustion in deep cut and help to remove ashes, and last it's to keep fume out of the lens (stay clean & don't diminish the power ray intensity).



as per engraving it's less evident depending on material, but to keep your lens clean.


---
**HalfNormal** *February 02, 2017 12:15*

The type of material is also a factor. 


---
**Joe Alexander** *February 02, 2017 12:54*

1 & 3 are noticeably crisper on the edges and obviously deeper cute overall. It does help bring it out but with thin material I'd imagine it could be a problem. What speed/pwr settings?


---
**Ned Hill** *February 02, 2017 15:08*

I've always run with air assist when engraving because it keeps the bottom of the lens clean like Stephane said.  Also prevents flare ups from happening.  That being said I do have a flow controller on my setup and I run lower flow for engraving and crank it up for cutting.


---
**matt s** *February 05, 2017 03:10*

I just got my k40 and have no idea which compressor I should get for my cutter. Any recommendations? I plan on 3d printing my air assist nozzle.


---
**Ned Hill** *February 05, 2017 03:29*

**+matt s**  Depends on how much you want to spend and how much noise you can deal with. Ideally you can go with a  regular air compressor but they tend to be on the noisy side unless you can pipe it in.  Other options are an airbrush compressor or a large aquarium/pond type pump.  I personally use an airbrush compressor that will do about 25 lpm max and is relatively quiet.  The drawback to an airbrush compressor is that it's not made to run continuously and it works best with some back pressure.  I have a fan blowing on mine to help keep it cool and feed it through a 5 gal air tank to dampen any pulsations and have a flow controller.  An aquarium pump is made to run continuously but I understand they can be somewhat noisy.  You want something that can do at least 20 lpm (264 gph).  Hope this helps


---
**matt s** *February 05, 2017 03:55*

**+Ned Hill** Thank you for all the info. I'm leaning towards an airbrush compressor I think. I need a good paint pen for my 3d printer parts & it seems like it could knock out a few needs I have.


---
*Imported from [Google+](https://plus.google.com/104868032632666700277/posts/S5sns1ozJDi) &mdash; content and formatting may not be reliable*
