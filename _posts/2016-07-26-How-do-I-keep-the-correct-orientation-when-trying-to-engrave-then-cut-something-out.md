---
layout: post
title: "How do I keep the correct orientation when trying to engrave, then cut something out?"
date: July 26, 2016 13:28
category: "Original software and hardware issues"
author: "David Spencer"
---
How do I keep the correct orientation when trying to engrave, then cut something out? I have tried loading the project and then deleting the cut parts but that moves the file when I go to engrave. if I leave the part that is supposed to cut the laser just goes back and forth in a straight line.





**"David Spencer"**

---
---
**Jim Hatch** *July 26, 2016 13:51*

Put a "registration" mark, dot, hole or something else on all layers of your project. Or as **+Yuusuf Sallahuddin**​ does, create a box the size of your workspace around everything. It can be hairline and no stroke weight so it doesn't actually cut but the software sees it and keeps everything the correct size & position.


---
**David Spencer** *July 26, 2016 20:57*

Ecuse my ignorance but what is stroke wieght?


---
**Jim Hatch** *July 26, 2016 21:18*

Stroke weight is how thick the line is. For cuts for instance I use .001mm as the stroke weight - I just want a single pass of the beam. If the stroke weight is heavier (say 1 point) then you actually get either multiple passes or it creates an engraved box (really thin box but a box engrave nonetheless).



In Corel (and other design programs) you can designate the stroke weight as well as the outline and fill color. With a line, there's no distinction to the two colors - there's only outline. So you can make a really thin lined tiny object or a colorless (not white, colorless - there's a difference, it's the absence of color) lined object (like a circle) to use as your registration mark. 



I think Yuusuf creates a 0 weight box the size of his bed on every layer. I do it by project with a custom one for each project - it just helps me keep track of what's "in" vs "out" of a design.


---
**David Spencer** *July 26, 2016 21:22*

thank you for he help.


---
**Jim Hatch** *July 26, 2016 21:46*

NP. I use both AI and Corel so don't remember what the specific fields are but in the top bar under the menu options on one (Corel) it's got a field (only visible for things with lines) for stroke and on the other it's in a panel docked to the side (AI).


---
**Bob Damato** *August 05, 2016 14:38*

When you are about to cut/engrave, in the dialog box under the "refer" option, select "center". This will keep them aligned accordingly.

bob


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/4Nebd9xWnLi) &mdash; content and formatting may not be reliable*
