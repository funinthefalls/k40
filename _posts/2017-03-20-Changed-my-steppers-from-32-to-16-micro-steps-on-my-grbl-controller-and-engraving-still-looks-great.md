---
layout: post
title: "Changed my steppers from 32 to 16 micro steps on my grbl controller and engraving still looks great"
date: March 20, 2017 20:27
category: "Hardware and Laser settings"
author: "Paul de Groot"
---
Changed my steppers from 32 to 16 micro steps on my grbl controller and engraving still looks great.

![images/27642724c44ea729dcee083d3cb902dd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/27642724c44ea729dcee083d3cb902dd.jpeg)



**"Paul de Groot"**

---
---
**Steve Anken** *March 20, 2017 23:10*

Anything beyond 10 is pretty much iffy at best. This is a good tutorial for the physics of steppers.

[homepage.divms.uiowa.edu - Jones on Stepping Motor Microstepping](http://homepage.divms.uiowa.edu/~jones/step/micro.html)


---
**Paul de Groot** *March 20, 2017 23:14*

**+Steve Anken**​ great read!


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/X4MH4ckXMWS) &mdash; content and formatting may not be reliable*
