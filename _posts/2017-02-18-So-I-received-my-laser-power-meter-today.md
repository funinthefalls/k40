---
layout: post
title: "So I received my laser power meter today"
date: February 18, 2017 02:25
category: "Discussion"
author: "Anthony Bolgar"
---
So I received my laser power meter today. Just for fun and giggles, I tested the tubes in both of my K40's  The one registered at 29W at full power, which I fully expected to see a low value, but what shocked me is that the second one was actually 42W at full power. Throws a wrench into my plans to compare tube power/life vs. high/low mA settings. 





**"Anthony Bolgar"**

---
---
**Stephane Buisson** *February 18, 2017 08:30*

For a start, could you tell us any spec/info you have on each tube. (pay attention it's frequent you could have 2 labels on top of each other with different's info). 

does the tube's lengh the same ?


---
**Anthony Bolgar** *February 18, 2017 14:36*

Will take a look today.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/aNn7P4hspTa) &mdash; content and formatting may not be reliable*
