---
layout: post
title: "Is there an actual tutorial for using laserweb"
date: January 19, 2017 21:34
category: "Smoothieboard Modification"
author: "Tony Sobczak"
---
Is there an actual tutorial for using laserweb.  And can you use laserweb3 to get used to it before installing my cohesion3d mini. 



TIA



Prefer something in writing rather than youtube. Easier to reference.







**"Tony Sobczak"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 22:08*

Here's some light reading to get you started: [github.com - LaserWeb3](https://github.com/LaserWeb/LaserWeb3/wiki)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 19, 2017 23:04*

In regards to your second question, you can test LaserWeb (once installed on your system) without having a laser plugged in. 


---
**Tony Sobczak** *January 20, 2017 04:55*

Looking forward to it


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 04:57*

I can attest to that. It's mostly been me telling people, hey, here's a screenshot of the settings you need to pop in.  Then open a picture or vector, set it up, and run.  Hit connect to start talking to the board at some point in time in between. :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 20, 2017 10:25*

The only bit that is remotely difficult is working out what specific settings work for certain materials, which is just a trial & error thing & running some calibration engraves/cuts for that material. The rest is pretty straight-forward :)


---
**Tony Sobczak** *January 20, 2017 17:18*

I have 100's of cdr files. And easy way to convert? 


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 17:22*

LaserWeb takes pictures and vector files. So you should look into how to export CorelDraw into pictures or something like DXF. 


---
**Tony Sobczak** *January 20, 2017 18:23*

That I can do, I was just hoping there might be a way to do a batch. 


---
**Tony Sobczak** *January 20, 2017 19:07*

**+Ray Kholodovsky** Where's the screenshot?


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 19:07*

Not following. What screenshot?


---
**Tony Sobczak** *January 20, 2017 23:18*

Misunderstood - Their telling YOU to open a screenshot.(previous post from you).


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 23:20*

Ah, a snapshot of the LaserWeb settings to pop in? I'll get that posted on the support site. 


---
**Tony Sobczak** *January 20, 2017 23:34*

Thanks 


---
**Chris Sader** *January 21, 2017 04:43*

**+Ray Kholodovsky**​ would love to see those settings too. I've got it running well, but it'd be interested to see if you have anything that's different from my settings


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2017 04:46*

Here you go.

![images/5bc4855f2e76a36777cbe8d869da279a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5bc4855f2e76a36777cbe8d869da279a.png)


---
**Chris Sader** *January 23, 2017 02:35*

**+Ray Kholodovsky** comparing your screenshot to my settings...I don't currently have the M3/M5 Start/End codes in there. Do I need those? Something I'm missing without them?


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 17:33*

**+Chris Sader** the M3 and M5 are for 2 wire replace pot pwm approach.  If you have that then you need those in there.

If you have the 1 wire approach which I am recommending more now - sending PWM to L - then you do not need them in there.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/XkSC2qwSwhq) &mdash; content and formatting may not be reliable*
