---
layout: post
title: "Been trying to build successively bigger objects so that I can move away from the notion that the 3020 table is the limiting factor in what you can make on the K40"
date: January 17, 2019 02:16
category: "Object produced with laser"
author: "Mike Meyer"
---
Been trying to build successively bigger objects so that I can move away from the notion that the 3020 table is the limiting factor in what you can make on the K40. This is a design I came up to test the hypothesis...it will ultimately be a backlit wall lamp.



![images/4e081b070e6d72805775d678fc9dc1fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e081b070e6d72805775d678fc9dc1fd.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-GN_1ntGI7oY/XD_lgvnF8NI/AAAAAAAAKyI/gKj2N624I-s-33cI2XXe3OeWUQyLPlkFwCJoC/s0/VID_20190116_161807006.mp4**
![images/545bc9a692208789a2d17d7a6b8d9163.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/545bc9a692208789a2d17d7a6b8d9163.jpeg)
![images/af75dbfca28593d778d12e31fb5fe30b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/af75dbfca28593d778d12e31fb5fe30b.jpeg)

**"Mike Meyer"**

---


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/9DrPMCkHueV) &mdash; content and formatting may not be reliable*
