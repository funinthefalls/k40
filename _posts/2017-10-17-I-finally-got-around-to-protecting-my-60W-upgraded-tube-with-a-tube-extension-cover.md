---
layout: post
title: "I finally got around to protecting my 60W upgraded tube with a tube extension cover"
date: October 17, 2017 18:25
category: "Modification"
author: "Anthony Bolgar"
---
I finally got around to protecting my 60W upgraded tube with a tube extension cover. Made from some 3" PVC plumbing fittings for about $20.



![images/67a489766477404b0626434037c17424.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/67a489766477404b0626434037c17424.jpeg)
![images/0844d68576f2f21dd3d5aa21b4555020.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0844d68576f2f21dd3d5aa21b4555020.jpeg)
![images/a830ae166b736bb8efeab1a45a087ba2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a830ae166b736bb8efeab1a45a087ba2.jpeg)

**"Anthony Bolgar"**

---
---
**Ned Hill** *October 18, 2017 04:21*

Nice solution.  


---
**Ned Hill** *October 18, 2017 04:30*

He bought a 50W laser and put in a 60W tube.  Higher power means longer tubes hence the case extention.  




---
**Don Kleinschnitz Jr.** *October 18, 2017 11:39*

He heh .... that works nice!

#K40TubeExtension


---
**Anthony Bolgar** *October 18, 2017 11:40*

Thanks Ned. It is super strong. And a lot cheaper than a metal extension from LightObjects. I had considered making one with 1/4" acrylic, but to me it did not appear to be strong enough to really protect the tube.


---
**Anthony Bolgar** *October 18, 2017 11:41*

Oh, and it is orange because the case is biege and orange.


---
**Anthony Bolgar** *October 18, 2017 11:43*

The flange piece was a 3" toilet flange(new, not used...lol) and I trimmed the flange back to 12mm wide, thena coupling, threaded coupling and a threaded end cap. This way I can access the HV wire without having to dismantle it, and for a tube replacement I will just unbolt it from the case.




---
**Jean Carlos Perez** *October 26, 2017 08:04*

buena idea te felicito.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/U77dqjuMu43) &mdash; content and formatting may not be reliable*
