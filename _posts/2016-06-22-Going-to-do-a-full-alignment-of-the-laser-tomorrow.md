---
layout: post
title: "Going to do a full alignment of the laser tomorrow"
date: June 22, 2016 21:03
category: "Hardware and Laser settings"
author: "Pete Sobye"
---
Going to do a full alignment of the laser tomorrow. Fixed mirror and the X/Y mirrors. I am fed up cutting 3mm acrylic at 1mm/s at 95/100% power. 



To remove the mirrors from their housings, do I just unscrew the housing from the front of the mounting plate? I will need to remove them when my new mirrors arrive soon. Also it shouldn't alter the alignment should it?





**"Pete Sobye"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 22, 2016 21:23*

Yeah the mirror housing just unscrews from the front (the round circle). Removing them will only alter your alignment if you happen to bump it as you do it & loosen the 3 screw in adjusters. I wouldn't be too concerned with losing alignment when you do that, as it's usually a reasonably quick process to align it once you know what you're doing. My advice: take the time to learn to align it well. Might take a few hours the first time (for me it took days due to an issue with the Y-rail running "downwards" compared to the beam). Once you've had a fair play around with aligning the machine you will find that it's quite a simple process.


---
**Phillip Conroy** *June 22, 2016 21:59*

The trick with alignment is to move 2 screws at a time ie top 2  ,side 2


---
**Ned Hill** *June 22, 2016 22:09*

Always great alignment guide. [https://1drv.ms/b/s!AtpKmELEkcL8imoUD99sdJZZT-am](https://1drv.ms/b/s!AtpKmELEkcL8imoUD99sdJZZT-am)


---
**Brandon Smith** *June 22, 2016 23:15*

That is a great alignment guide Ned. There is also this:



[http://www.lightobject.info/viewtopic.php?f=16&t=2835](http://www.lightobject.info/viewtopic.php?f=16&t=2835)


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/1SKmZs2TR2v) &mdash; content and formatting may not be reliable*
