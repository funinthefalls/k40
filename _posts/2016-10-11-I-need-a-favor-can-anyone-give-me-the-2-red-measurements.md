---
layout: post
title: "I need a favor: can anyone give me the 2 red measurements?"
date: October 11, 2016 09:53
category: "Discussion"
author: "Walter White"
---
I need a favor: can anyone give me the 2 red measurements? Basically is the space inside the rails.

Thanks

![images/447f19a114783460f88a0811c693fb57.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/447f19a114783460f88a0811c693fb57.jpeg)



**"Walter White"**

---
---
**Don Kleinschnitz Jr.** *October 11, 2016 12:22*

x= 14.125"

Not sure what dimension you want from the picture?

Inside of bottom gantry bar to inside top gantry bar: 13.5"

Inside of bottom gantry bar to outside top gantry bar: 14.6




---
**Walter White** *October 11, 2016 12:43*

Thanks **+Don Kleinschnitz** !


---
**Ariel Yahni (UniKpty)** *October 11, 2016 13:29*

I've got inside X 14 3/8 and inside Y 13 3/8


---
**Ashley M. Kirchner [Norym]** *October 11, 2016 16:33*

Keep in mind that the moving part does not go to zero all the way up in the corner if you are running a stock controller. Nor all the way left for that matter. 


---
**Walter White** *October 12, 2016 06:54*

**+Ashley M. Kirchner**​ thanks for the tip. My intention is to buy a k40 and immediately replace the stock controller with arduino and grbl shiel for use the full area available. 


---
**Ariel Yahni (UniKpty)** *October 12, 2016 13:08*

I don't beleive the stock controller will limit you on the size, there even a size setup on the plugin if I recall correctly


---
**Ashley M. Kirchner [Norym]** *October 12, 2016 16:25*

You can't reset the home position though. It's set at the factory, or when they burn in the rom or USB key, or wherever they're storing that information. There's a whole 3/4" on the left that's wasted, and another inch or so at the top. But you can adjust the size it will use.


---
*Imported from [Google+](https://plus.google.com/118017715722776268014/posts/RG6fTunztxV) &mdash; content and formatting may not be reliable*
