---
layout: post
title: "Working on some ideas for how to design multi-layered wood cut artwork that I Laser mentioned recently"
date: April 07, 2016 12:26
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Working on some ideas for how to design multi-layered wood cut artwork that **+I Laser** mentioned recently. Some examples of someone else's finished work can be seen here: [http://mtomsky.deviantart.com/gallery/](http://mtomsky.deviantart.com/gallery/)



I have a few ideas up my sleeve for what I want to create, just have to find some reasonable 3d models (or create them) to enable simple slicing.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



So I have been trying to figure out the easiest way to design those awesome 3d-multi-layered wood cuts.



I figured it out. If we can have a 3d model of the scene (or object), we can create slices of the model to get different shapes for different depths. Here are some examples I did whilst trying to work out the software (since I am not very familiar with 3d software).



So I used an .stl model I got from [http://www.thingiverse.com/thing:182366](http://www.thingiverse.com/thing:182366) of the Sphinx (well not the real sphinx, cos this one has nose still).



From there I used a software called Slic3r ([http://slic3r.org/](http://slic3r.org/)) to slice the STL into an SVG (with multiple layers). Then I coloured all the layers & added 0.01mm borders.



So, as you can see, there are way too many slices (some of these images have 200+ slices). We want something in the range of 10 slices. I haven't found an option in Slic3r to choose how many slices, but it's easy enough to Union the slices in Adobe Illustrator (or I imagine other imaging software). So e.g. I select slices 0-9, union into 1 layer. 10-19, another layer, 20-29, another layer, etc. Then I can minimise the layers by 90% (so 200 becomes 20). Then, if that is still too ambitious, can minimise again as necessary.



So, thought you might be interested in the simplest process I can think of so far. Any thoughts/ideas on how to make even simpler or better are welcome.



![images/b6862753fe059b104b54243aab1982a8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b6862753fe059b104b54243aab1982a8.png)
![images/c39f9d24fbe2d9cc237ad4bc435d7ae7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c39f9d24fbe2d9cc237ad4bc435d7ae7.png)
![images/2a041a371aacf76989f686a06da922af.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2a041a371aacf76989f686a06da922af.png)
![images/8dd00f27afc1935760d45c9125aee51e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8dd00f27afc1935760d45c9125aee51e.png)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ariel Yahni (UniKpty)** *April 07, 2016 12:31*

Look for laserweb2 at [v2.laserweb.xyz](http://v2.laserweb.xyz)


---
**Scott Marshall** *April 07, 2016 12:37*

I'll have to put "3rd & 4th axis" on the dream list (right after focal change on the fly) so we can angle cut the slices...


---
**Thor Johnson** *April 07, 2016 12:45*

As far as making "number of slices", you do that via the layer height; you can set it to 1/8" for just stacking boards, or even thicker.


---
**Thor Johnson** *April 07, 2016 12:48*

**+Scott Marshall** Now that's a nifty idea, but they do need to go together because geometry is annoying.  And then we gotta get laserweb to do something nifty with it because the angle of the cut will definitely affect the size (geometry being annoying again)... 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 12:51*

**+Ariel Yahni** **+Thor Johnson** Thanks for that. Just had a test via LaserWeb2 & can see that it does it nicely to cut via layer height (e.g. 3mm).



**+Scott Marshall** Not sure what you mean by angle cutting the slices. Are you referring to being able to tilt the laser head to give a mitre cut on the edges?


---
**Stephane Buisson** *April 07, 2016 12:53*

3D softwares, I do like Sculptris as modeler (top if you have a wacom tablet) and MeshMixer (easy to make your slices).

but you must understand little amount of slices will result in poor Z resolution.

3 of my examples made with sculptris:

[https://www.youmagine.com/designs/hedgehog-pencils-holder](https://www.youmagine.com/designs/hedgehog-pencils-holder)

[https://plus.google.com/117750252531506832328/posts/e6QX5KKPahY](https://plus.google.com/117750252531506832328/posts/e6QX5KKPahY)
 or 

[https://plus.google.com/117750252531506832328/posts/APSo7szwGL7](https://plus.google.com/117750252531506832328/posts/APSo7szwGL7)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 13:04*

**+Stephane Buisson** Thanks Stephane. I understand that we will lose some resolution minimising the amount of slices, but I don't want to have 200 layers of 3mm ply for my artwork. Even 20 layers would result in a 60mm depth into the artwork. The idea is not really for super detailed art, but things similar to the ones on mtomsky's deviant art page, where they are more cartoony like representations.



I will take a look at the softwares you mentioned. I don't have a wacom tablet (yet), however strangely enough I was looking at purchasing one earlier this morning.



edit: I like the examples you've shown, especially the spiky pencil/toothpick holder hedgehog.


---
**Josh Rhodes** *April 07, 2016 14:30*

123d can do this operation, it can even make x-y and x-z slices that mesh together. [http://www.123dapp.com/make](http://www.123dapp.com/make)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 14:52*

**+Josh Rhodes** Thanks for sharing Josh. I'll take a look at that.



edit: just took a look at the vid on the link you shared. That looks perfect & awesome. Thanks again.


---
**Ashley M. Kirchner [Norym]** *April 07, 2016 17:02*

Angle cutting is complicated in terms of the laser beam path itself. Particularly with the stationary laser tubes. Some of the larger industrial machines that do multi-axis cutting, have laser modules that sit right on the head itself. Makes it easier to move around. But on a stationary rig, getting the mirrors right as it's rotating is going to be challenging. Not impossible, just challenging.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/hNXGj1B83Gr) &mdash; content and formatting may not be reliable*
