---
layout: post
title: "I cant seem to make a rubber stamp to save my life"
date: September 18, 2016 21:04
category: "Object produced with laser"
author: "Bob Damato"
---
I cant seem to make a rubber stamp to save my life. I have the right material etc, that  isnt the issue. I cant seem to mirror correctly in Coreldraw.



It is a return address stamp. I do my text  OK as such:



name

road

city, state, zip.



Then I go up to the little icon and mirror it.  It looks perfect in corel draw. When I go to etch it, it is now :



City, state,zip <reversed correctly>

road <reversed correctly>

Name <reversed correctly>



OK the obvious answer is to swap the name and city/state/zip to get this going, but Id still love to know what Im doing wrong.







**"Bob Damato"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 18, 2016 22:26*

There is an option in the engrave dialog box to mirror the output. You can just tick that when you send the job to engrave I believe. I haven't tested though, so I recommend testing on some scrap/wood first.



See: [drive.google.com - Engraving Settings.png - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwYUdVc1I5NFY1R2c/view?usp=sharing)

Look up top left where it says "Rotate & Mirror"


---
**Bob Damato** *September 19, 2016 01:12*

Ill give this a try, thank you.




---
**Harvey Benner** *September 19, 2016 02:14*

I normally group all the items in CorelDraw then just mirror the group.


---
**Bob Damato** *September 19, 2016 13:03*

**+Harvey Benner**  Thank you. That is what I did, but I might have done it differently than you. How do you mirror?


---
**Scott Marshall** *September 20, 2016 02:20*

It's "Flip" vs "mirror" I believe....



said the old guy running autocad..


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/JvWC9VTdu5M) &mdash; content and formatting may not be reliable*
