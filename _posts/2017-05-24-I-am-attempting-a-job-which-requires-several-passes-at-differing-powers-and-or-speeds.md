---
layout: post
title: "I am attempting a job which requires several passes at differing powers and/or speeds"
date: May 24, 2017 19:37
category: "Original software and hardware issues"
author: "Paul Townsend"
---
I am attempting a job which requires several passes at differing powers and/or speeds.

I use Corelaser 2013 with CorelDraw X5 driving my K40 with M2 board.

I often and unpredictably get an offset of a few millimetres in the Y direction when starting the next pass.

How can I stop this?





**"Paul Townsend"**

---
---
**Anthony Bolgar** *May 24, 2017 19:43*

First thing I would check is the belt tension on the Y axis.


---
**Thor Johnson** *May 24, 2017 22:33*

The other thing I'd look at is putting registration dots for every layer diagonally.  My laser always disengages the stepper at end of job (and will jump to the nearest step), so I line up for each pass (and I do 2 dots for each pass to make sure the &*% software doesn't "rescale" the drawing before sending it).


---
**Gavin Dow** *May 25, 2017 15:28*

I made a custom template in CorelDraw that shows the limits of my bed as the "page", and already includes a dot in the top left corner. I include the dot on every cut or engrave pass I do. That seems to be enough to keep the software from auto-scaling, auto-adjusting, auto-anythinging. I have not had any issue with alignment between jobs.

There's also a do-not-release, or something like that, in the settings, that prevents the steppers from disengaging after each job. I don't remember the specific term, but I'll try to check tonight when I'm back at my laser computer.


---
**Paul Townsend** *May 25, 2017 19:19*

Thanks to Anthony, Thor and Gavin for advice.

I looked at tensioning the Y belt. Unfortunately access is from below the laser and using the original screw adjusters would entail dismounting the laser and its bed so full re-alignment afterwards would be essential. Rather than do that time consuming job I made new adjusters with little rollers near the front where access is easy.

Sadly no improvement.

I had already been adding a single corner alignment point on every layer so now added another diametrically opposite.

Using this still no help.

Then the Eureka moment. I used CTRL A on each layer which shows the bounds and one layer had a minute dot outside of the wanted area.

Once I erased that, all is well. Lesson learnt!


---
**Anthony Bolgar** *May 25, 2017 19:23*

Glad you found a fix for the issue.


---
**Ned Hill** *May 26, 2017 17:13*

**+Paul Townsend** For future reference You don't have to remove the bed to get to the y-belt tensioners.  There are typically two small holes in the back of the machine cabinet that allow you to stick a long screwdriver through to adjust the belts.


---
*Imported from [Google+](https://plus.google.com/116723644477013102639/posts/EuL65uNsVK4) &mdash; content and formatting may not be reliable*
