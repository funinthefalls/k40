---
layout: post
title: "Anyone have any luck with the tabbed box plugin for Inkscape?"
date: July 20, 2016 03:50
category: "Discussion"
author: "Alex Krause"
---
Anyone have any luck with the tabbed box plugin for Inkscape?





**"Alex Krause"**

---
---
**Stephane Buisson** *July 20, 2016 11:28*

Sorry your question isn't clear enough to me.

Visicut plugin for inkscape work fine with me.


---
**Norman Kirby** *July 20, 2016 12:02*

is that the "Eggbot extension" if so yes I have with good success




---
**Gary Hamilton** *July 20, 2016 12:06*

I have been doing some searching for this!  The only think I found was this (which I hope is not the only thing out there). [https://github.com/hellerbarde/inkscape-boxmaker](https://github.com/hellerbarde/inkscape-boxmaker) I have this on my list to look more this weekend.  I can do it manually but I am lazy.  If you find or have a good tool please share.


---
**Norman Kirby** *July 20, 2016 12:20*

sorry about previous comment i got it mixed up and the eggbot is a hatch fill extension, i do use the above extension boxmaker and have used it successfully




---
**Gary Hamilton** *July 20, 2016 12:34*

**+Alex Krause** I also came across this site.  Which may be what I start with.  Took a minute to find it. [http://www.makercase.com/](http://www.makercase.com/)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/jiZ57KvX7Ag) &mdash; content and formatting may not be reliable*
