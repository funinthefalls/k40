---
layout: post
title: "Just checking out some faster settings...turned out pretty good as far as I'm concerned"
date: November 27, 2015 19:27
category: "Discussion"
author: "Scott Thorne"
---
Just checking out some faster settings...turned out pretty good as far as I'm concerned.

![images/988c1e5b58bb9856935e1346c7f92dd7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/988c1e5b58bb9856935e1346c7f92dd7.jpeg)



**"Scott Thorne"**

---
---
**DIY3DTECH.com** *November 27, 2015 19:43*

Dude!  Blade Runner right?  Very nice, I like this one the best!


---
**Scott Thorne** *November 27, 2015 22:52*

Yup


---
**Gary McKinnon** *November 28, 2015 12:29*

My favourite film of all time ! :) What speed were you at with this one, looks great ?




---
**Scott Thorne** *November 28, 2015 12:31*

400mm/s


---
**Gary McKinnon** *November 28, 2015 12:40*

Fast! I thought the limit on these was 350, but i read somewhere else it's 500.


---
**Scott Thorne** *November 28, 2015 13:09*

Mine is 500mm/s......my power stays around 6 when I'm engraving and 8 when I'm cutting.


---
**Andrew ONeal (Andy-drew)** *December 14, 2015 04:52*

Is all work done in Corel or inkskape, I posted my first ever post above with my issues. Best guess design in inkskape then send to laser via pronterface? Will Corel work the same way or Corel files have to be ran through inkskape to generate gcode?


---
**Scott Thorne** *December 14, 2015 09:09*

I've never use inkscape....I use Corel laser or laser draw.


---
**Andrew ONeal (Andy-drew)** *December 14, 2015 20:43*

Wait, you still use moshidraw electronics?


---
**Gary McKinnon** *December 14, 2015 20:54*

Good results, what speeds ?


---
**Scott Thorne** *December 14, 2015 21:06*

No...I have m2 nano board....I use coral laser.


---
**Scott Thorne** *December 14, 2015 21:06*

4mA at 400mm/s


---
**Gary McKinnon** *December 14, 2015 21:11*

Fast !


---
**Scott Thorne** *December 14, 2015 21:40*

**+Gary McKinnon**...thanks...I think I've got it setup like I want it now....now more up grades for the moment.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/fC1VLXm8vDg) &mdash; content and formatting may not be reliable*
