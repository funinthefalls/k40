---
layout: post
title: "A few weeks ago I was showing my previously shared inlaid Masonic cube at my lodge and one guy said he would buy one if I could make it spin"
date: September 16, 2017 23:52
category: "Object produced with laser"
author: "Ned Hill"
---
A few weeks ago I was showing my  previously shared inlaid Masonic cube at my lodge and one guy said he would buy one if I could make it spin.  Challenge accepted :)  Found a small 6V low rpm geared motor (link below) that I thought would work.  It's rated from 1-7V and I found that 1.5V gave me ~3 rpm which seemed like a good speed, plus it's super quiet at that speed.  At 1.5V it's only drawing ~ 15mA so you could run it off a battery if you wanted to, but  I opted to run it from a 3V wall plug, that I stepped down to 1.5V with a couple of 10 ohm resistors as a voltage divider.   I used some 1/4" dowel to make a couple of motor supports.   I was actually very please that, after some careful measurements, everything lined up perfectly on the first cut.  Made the box from 1/8" birch ply that I stained and finished with some spray lacquer.  Very happy with how it turned out.   

Motor - [http://www.dx.com/p/type-30-dc-motor-dc-6-0v-15rpm-low-noise-d-shaft-460829#.Wb25J8h95pi](http://www.dx.com/p/type-30-dc-motor-dc-6-0v-15rpm-low-noise-d-shaft-460829#.Wb25J8h95pi)



![images/7bc87904c9dfc0ee431c5a2c3571aeaf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7bc87904c9dfc0ee431c5a2c3571aeaf.jpeg)
![images/6b72bd4724bf2e09cb68a115419cff3e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b72bd4724bf2e09cb68a115419cff3e.jpeg)
![images/b5dc9820c942932be10f815777e6b974.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b5dc9820c942932be10f815777e6b974.jpeg)
![images/06822867ca4f94d0d8de5e627e3b2bfb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/06822867ca4f94d0d8de5e627e3b2bfb.jpeg)
![images/d943354094ad8fbb264f72504bb792de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d943354094ad8fbb264f72504bb792de.jpeg)
![images/0915f1ff0f1916a800e89f045e3dbe4b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0915f1ff0f1916a800e89f045e3dbe4b.jpeg)
![images/5acab44dd9dd75cd2d64fbc42ee36a8d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5acab44dd9dd75cd2d64fbc42ee36a8d.jpeg)

**"Ned Hill"**

---
---
**Anthony Bolgar** *September 16, 2017 23:55*

Very nice!


---
**Stephane Buisson** *September 17, 2017 06:02*

**+Ned Hill** you can also use a microvawe motor tray, exist at different speeds, work directly from main. it's what I use for my automata.



[ebay.co.uk - Details about  TYJ50 AC 220V-240V 30/33r/min CW/CCW Synchronous Reduction Electric Gear Motor](http://www.ebay.co.uk/itm/TYJ50-AC-220V-240V-30-33r-min-CW-CCW-Synchronous-Reduction-Electric-Gear-Motor/191752210321?ssPageName=STRK:MEBIDX:IT&_trksid=p2060353.m1438.l2649)


---
**BEN 3D** *September 17, 2017 09:56*

I love it!


---
**Don Kleinschnitz Jr.** *September 17, 2017 14:09*

Excellent work..... and fun to watch....


---
**HalfNormal** *September 17, 2017 14:42*

Love your craftsmanship! 


---
**Ned Hill** *September 17, 2017 16:34*

Thanks everyone!  

Thanks for the motor link **+Stephane Buisson** I hadn't considered that type.



This project had me bemoaning the closing of my local RadioShack stores.  My small electronic components inventory is severely lacking,  but thanks to Amazon Prime I now have 49 extra slide switches. :P


---
**Don Kleinschnitz Jr.** *September 17, 2017 16:58*

**+Ned Hill** I miss RS also but only when I am in a hurry for a part. I found everything and more on Amazon, Adafruit and RS online. BTW RS online has some really good deals these days. I have noticed that if you are not careful you can get cheap switches etc that just fall apart in your hand or die when you solder them ....



Also Digikey has a good selection of electronics parts and ships pretty fast and reasonable....


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/Y82pvuc8up8) &mdash; content and formatting may not be reliable*
