---
layout: post
title: "I wasn't happy with the job the exhaust fan was doing so I bought a 200 cfm inline booster from Lowe's in the hope that this would help with extraction"
date: October 11, 2016 19:30
category: "Modification"
author: "Nigel Conroy"
---
I wasn't happy with the job the exhaust fan was doing so I bought a 200 cfm inline booster from Lowe's in the hope that this would help with extraction. 



I placed this at the end of the hose just inside the window and have the original stock fan running at the same time. This hasn't made any real noticeable difference with extraction. 



Would having both fans running at the same time be an issue or is  200 cfm not strong enough?



Any suggestions on the right fan for the job would be appreciated.



Once I'm happy with the install I'll use foil tape on the joints and shorten the connection to the window vent.





![images/c69c68af805537e1954c5891fab51ad6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c69c68af805537e1954c5891fab51ad6.jpeg)
![images/6658b8e8e2064c07346b4a4fe8f1cb6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6658b8e8e2064c07346b4a4fe8f1cb6a.jpeg)

**"Nigel Conroy"**

---
---
**Jim Hatch** *October 11, 2016 19:48*

It's the supply side. If you're still using the stock exhaust housing on the inside of the box it's going to restrict the exhaust. Also, the K40 doesn't allow for a lot of air to come into the box - just the seams and a 2" hole in the bottom of the cabinet. If you can't get replacement air in, you're not going to exhaust effectively.



Most people remove the exhaust manifold inside the box. Some of us have added additional venting into the box by mounting small fans facing inward to the front of the box to drive more air in to make it easier to exhaust well.


---
**Don Kleinschnitz Jr.** *October 11, 2016 20:10*

I am running 440CFM and open the lid  a bit. It mad a juge difference. Stock blower is useless .....


---
**Nigel Conroy** *October 11, 2016 20:19*

Thanks for the reply +Jim Hatch and **+Don Kleinschnitz**

Following your comments I did a few tests and with the lid slightly open indeed the exhaust worked a lot better.

I have a slightly different version then the stock K40 and I do remeber seeing the hole in the bottom of the machine in photos and videos. Mine doesn't have this.

The fan is a builtin fan in the exhaust port. 



Perhaps I should look at adding some intake fans as you've suggested.



How did you go about adding these? 

![images/fb0a65b46e6c6b6ffb76e9294b409cd0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb0a65b46e6c6b6ffb76e9294b409cd0.jpeg)


---
**Don Kleinschnitz Jr.** *October 11, 2016 20:38*

I did not need any internal fans. [http://donsthings.blogspot.com/2016/06/k40-air-systems.html](http://donsthings.blogspot.com/2016/06/k40-air-systems.html)

[donsthings.blogspot.com - K40 Air Systems](http://donsthings.blogspot.com/2016/06/k40-air-systems.html)


---
**Jim Hatch** *October 11, 2016 20:59*

**+Nigel Conroy**​ I added 4 small USB powered fans to the front of my K40 case. They're on the upper portion of the front just below the lid. That allows them to push air across the workpiece. They're a couple inches square each so I just used a hole saw in my drill to cut the right size hole, attached them with machine screws and nuts and ran the two power cables (they came wired 2 to a cable) to a USB power adapter I keep plugged into a switched power strip. When I power the strip everything else powers up too. 



That way I didn't need to prop the lid which causes it to be a Class 4 laser not a Class 1 home approved laser. With the lid partially open the laser can refract or reflect and escape the machine. It won't be focused but it's still a coherent beam and will blind you so laser safety glasses are a must if you prop the lid for air intake. Small odds but still not zero and there are people who have had it happen to them (mostly in high use settings vs a home shop).


---
**Don Kleinschnitz Jr.** *October 11, 2016 21:28*

What about the holes for the fans? Light can escape from them? 

As I read the safe use standards, I thought at this power and wavelength (not-visible) these are class 4 anyway, which requires a closed optical path.

I plan to put my vent holes in the bottom later which reduces the probability of leakage.


---
**Jim Hatch** *October 12, 2016 00:44*

**+Don Kleinschnitz**​ Class 1 [devices.dont](http://devices.dont) emit harmful laser [light.when](http://light.when) operated as designed/intended. So they're Class 1 from the factory because they are closed boxes with no optical escape paths and are intended to be operated with the lid closed and only then is the laser button supposed to be pushed.



However, it would be better if there were a lid interlock that prevents the laser from firing with the lid up. 



Leaving the holes cut for the fans open would create a Class 4 (unsafe laser light can escape and harm humans) device. But, put a spinning fan on the hole and you begin to close it off. Put a foam filter on the inside of the fan and it's back to a closed box and Class 1 status.


---
**Nigel Conroy** *October 20, 2016 15:20*

So I mocked up a CPU fan on the front door, made a cardboard door with hole for fan and put it in place of door. The CPU fan pushes a lot of air and the movement of the smoke inside the machine was better, not static smoke under viewing window.



There was still more of a smoke smell in the room then I would have liked so I went around the machine with a piece of string to check if air was moving in or out of gaps (movement of string showing air flow).



I found on the side of the machine with the controller and switches etc. there was air coming out into the room. Opening the side panel I could easily figure out that the small fan (in photo) inside was blowing outwards directly inline with the vent holes in the side of the machine.



Question is can I mount this fan in opposite direction? and/or cover the vent holes in the side of the machine?





![images/4da1cff4bfe0251af2881b1588399603.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4da1cff4bfe0251af2881b1588399603.jpeg)


---
**Nigel Conroy** *October 20, 2016 15:29*

![images/4c23931831b4b72e32f7ff6461c3147c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c23931831b4b72e32f7ff6461c3147c.jpeg)


---
**Nigel Conroy** *October 20, 2016 15:31*

Is something like this (obviously original door with hole in it) adequate for air from the front?

This is a single cpu fan that moves a lot of air by itself, or should I do multiple?

![images/f8a4259d37c40cfc35bd633a20afbe5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f8a4259d37c40cfc35bd633a20afbe5b.jpeg)


---
**Don Kleinschnitz Jr.** *October 20, 2016 15:32*

I think your problem still is that there is insufficient pull on the main evacuation fan. If that pull is sufficient then I can't imagine that it is migrating down to the PS area. 

If so I would not mess with the LPS fan (if that is the fan you are talking about) I would rather seal off that area from the main box if possible and insure it has a fresh air inlet. 

I have I similar vent by my LPS in my K40 and get no exit smell, but as I previously said there is a hurricane sucking on the main  box.


---
**Nigel Conroy** *October 20, 2016 15:42*

Do you get any smell **+Don Kleinschnitz**?


---
**Jim Hatch** *October 20, 2016 16:16*

Did you try to add more input air? Prop the lid up an inch and see if that helps. If it does then you need more incoming air not more exhaust fan capability.


---
**Nigel Conroy** *October 20, 2016 18:12*

Great point **+Jim Hatch** 

The simple things are the ones we miss sometimes!!!


---
**Don Kleinschnitz Jr.** *October 20, 2016 18:54*

**+Nigel Conroy** nope..


---
**BEN 3D** *November 23, 2017 20:33*

I `am searching for a German Version for a 400 cfm. I found this description on wikipedia "Common Fan Motor, CFM Aero engines later modify to commercial fan motor". Edit: That was really confusing for me, because, cfm stands for cubic food / m :-D. So for any other Guy from Europe, Dons 400 cfm is a 750 m³/h Fan like this [https://www.amazon.de/Rohrventilator-Rohrl%C3%BCfter-Niederdruck-Radialventilator-Absaugl%C3%BCfter/dp/B00M65GBQE/ref=sr_1_10?s=diy&ie=UTF8&qid=1511469577&sr=1-10&keywords=m%C2%B3+%2Fh+abluft](https://www.amazon.de/Rohrventilator-Rohrl%C3%BCfter-Niederdruck-Radialventilator-Absaugl%C3%BCfter/dp/B00M65GBQE/ref=sr_1_10?s=diy&ie=UTF8&qid=1511469577&sr=1-10&keywords=m%C2%B3+%2Fh+abluft)


---
**BEN 3D** *November 23, 2017 22:06*

Lol, the stock fan is just 2.5 m³/h thats just 1.47 cfm. I bought a used hood called "F-2060 Inox" for 9€ (11$) that comes with a 125W Motor I did not know the cfm or m³/h value name is , I will give it a try, otherwise I will buy a bigger one, like Don´s. The used hood looks similar to this one [https://www.datagate.ee/major-appliances/hood/cata-hood-f2050-convential-50-cm-560-m179h-inox-d-53-db/](https://www.datagate.ee/major-appliances/hood/cata-hood-f2050-convential-50-cm-560-m179h-inox-d-53-db/)



So it should be a 220 cfm  (380 m³/h)


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/FRHuoPLjFjc) &mdash; content and formatting may not be reliable*
