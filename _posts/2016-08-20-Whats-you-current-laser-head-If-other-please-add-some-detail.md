---
layout: post
title: "Whats you current laser head? If other please add some detail"
date: August 20, 2016 13:08
category: "Hardware and Laser settings"
author: "Ariel Yahni (UniKpty)"
---
Whats you current laser head? If other please add some detail. 





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *August 20, 2016 13:11*

FB group pol for reference [https://goo.gl/Qun8GC](https://goo.gl/Qun8GC)


---
**Alex Krause** *August 20, 2016 13:23*

Stock for me :( don't have the cash available right now to get air assist and a new focal lens


---
**Bart Libert** *August 20, 2016 13:28*

I see that he picture for the lightobjects one and the adjustable one are not the same. Does that mean that the LO head is not adjustable ?




---
**Ariel Yahni (UniKpty)** *August 20, 2016 13:32*

**+Bart Libert**​ that particular one no


---
**Bart Libert** *August 20, 2016 13:37*

so for the lightobject upgrade head with air assist you really need a up/down table for your material ?


---
**Jim Hatch** *August 20, 2016 13:52*

**+Bart Libert**​ You just need to be able to adjust the bed height. I've got a set of spacers that I can pop under my main bed tray (I pulled the stock bed out) based on the normal sizes of things I do - 1/8", 1/4", 1/2" (engrave only). I do have a small "lab jack" I'll put in sometime so I can just twist a knob to set the height.



The Saite adjustable head requires a new mounting plate vs the LO basic air assist head which can go into the stock mount.


---
**HP Persson** *August 20, 2016 13:57*

Made my own movable head, instead of moving the bed.




---
**Ariel Yahni (UniKpty)** *August 20, 2016 14:08*

**+Jim Hatch**​ can you share a picture of your setup? 


---
**3D Laser** *August 21, 2016 23:12*

**+Ariel Yahni** 

I use a lab jack too they are cheap and great 


---
**Ariel Yahni (UniKpty)** *August 21, 2016 23:18*

**+Corey Budwine**​ link? 


---
**Jim Hatch** *August 21, 2016 23:26*

**+Ariel Yahni**​ here's the one I got: [https://www.amazon.com/gp/product/B019QFND1C/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B019QFND1C/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**3D Laser** *August 22, 2016 00:05*

I got an eight inch one here though it was a bit to big and I had to take out half of the jack


---
**3D Laser** *August 22, 2016 00:06*

[http://www.hometrainingtools.com/laboratory-scissor-jack-8-x-8-stainless-steel](http://www.hometrainingtools.com/laboratory-scissor-jack-8-x-8-stainless-steel)


---
**Jim Hatch** *August 22, 2016 02:13*

I looked at a bigger one but just screwed a piece of plate steel on it - not like ply & acrylic sheets are very heavy. Use super magnets to hold it to the bottom of the k40.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Qtr4SkcEvh4) &mdash; content and formatting may not be reliable*
