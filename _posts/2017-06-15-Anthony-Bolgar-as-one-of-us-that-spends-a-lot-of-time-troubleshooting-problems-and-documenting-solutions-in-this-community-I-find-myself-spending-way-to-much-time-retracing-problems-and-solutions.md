---
layout: post
title: "Anthony Bolgar as one of us that spends a lot of time troubleshooting problems and documenting solutions in this community I find myself spending way to much time retracing problems and solutions"
date: June 15, 2017 11:20
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
**+Anthony Bolgar** as one of us that spends a lot of time troubleshooting problems and documenting solutions in this community I find myself spending way to much time retracing problems and solutions. "Been there done that"... Deja vu all over again"....



I think we are getting to a point in this community that we have seen almost every kind of problem possible (or close to it) and have some pretty good solutions. 



I also notice that the majority of the posts come in two major categories: 

.. Advice: getting advice

...Problem: looking for a solution to a problem. 



We all know that searching the community is frustrating. Even to the point where many of us have collections that focus and collect on key subjects.



It dawned on me that part of our search problem may simply be that our "filter" categories may not be granular enough.



So I am wondering if changing/adding to the filter categories can help us better find existing advice and solutions.





Here is a set of categories that illustrate my thinking: 

Advice: PWM & L CONTROL

Advice: LASER POWER SUPPLY

Advice: OPTICAL COMPONENTS

Advice: COOLANT

Advice: LASER TUBE

Advice: GANTRY 

Advice: AIR ASSIST

Advice: AC POWER

Advice: LIFT/STAGE

Advice: CONFIGURATION

Advice: CONTROLLER

Advice: LASER FINDER DOT

Advice: ENDSTOPS/INTERLOCKS



Problem: PWM & L Control

Problem: LOW OPTICAL POWER

Problem: LASER POWER SUPPLY

Problem: OPTICAL ALIGNMENT

Problem: COOLANT

Problem: LASER TUBE

Problem: GANTRY MOVEMENT

Problem: IMAGE QUALITY

Problem: AIR ASSIST

Problem: AC POWER

Problem: LIFT OPERATION

Problem: ENDSTOPS/INTERLOCKS



I don't know if there is a limit on categories or how to change them. I know it seems like a long list but I sense this level of granularity would improve our search filters and allow members to do some searching before posting. Often solving their own problems.

To use these categories one decides while posting if they are looking for advice or a fix and then choose the appropriate subcategory.



If we had to make the list shorter we could loose the ADVICE/PROBLEM and just keep the subsystem granularity. But I hope not :).



...BTW I have also been noodling how we can provide troubleshooting methods and reporting because I find myself repeating a troubleshooting, testing approach and asking for the results.....

Examples are: 

...Testing a LPS for proper operation

...Verifying the PWM control is working



With these filters we could post in each categories section a standard and proven way of how to identify, report, troubleshoot and test a problem in that category. This would dramatically reduce the amount of churn when trying to help someone with a problem. 



What are your thoughts?









**"Don Kleinschnitz Jr."**

---
---
**Stephane Buisson** *June 15, 2017 12:27*

Categories are limited in numbers (12 if I remember well), so no more available.



the problem being "the solution" is often in the comment in reply of a question. but sometime the post giving a valid solution or tip, is not related to the post (or question not in phase with this good answer).



I often change the category of a post (related to good answer in those case).



What I am demanding to the community is to not create a new post like they do when chatting. please be concise (but with plenty of comments), that make search easier for everyone.



use the search community on the right (not on top).



The community should reach 4000 members around the end of the month or early July. So your work is really valuable, keep sharing.



**+Don Kleinschnitz**


---
**Nate Caine** *June 15, 2017 13:49*

(1) Some forums have a "sticky" post at the top, that say "read this before you post".  And then a link to FAQs or previously answered questions.



(2) Google+ is marginal at best for "chit-chat" and not well structured for searching.  Plus "searching" implies you have to know what you are looking for in the first place.  Often folks don't know.



(3) It's particularly frustrating when a completely new person drops in, with dozens of questions, that people helpfully answer, and then there is never any feedback as to what worked, or didn't work, or how the problem was resolved.  And you never see the person again.  Until they have another problem.  Is this K40 Tech Support?



(4) Also it's funny that someone gets a K40 broken straight out of the box, and their <b>first</b> move is to post questions here, as opposed to contacting the vendor and bitching at them.



(5) The same half-dozen topics consume most the bandwidth:  

     (a) power supply problems.  

     (b) Cohesion 3D installation/configuration.  

     (c) optical alignment. 

     (d) LaserWeb.  

     (e) Corel.  

     (f) cooling/coolant



(6) A differently arranged forum would have FAQs and solid answers for common questions to be consulted beforehand.  Instead the ad-hoc answers that arise here are often wrong, or pointers to other sites with marginal answers.



(7) Some comment environments have "scoring" abilities.  So readers can up-vote or down-vote answers.  And "best answers" are presented at the top of the list.



(8) And that same up-vote, down-vote soon identifies some of the experts.  So in the future, you know that "an answer by Jones is gonna be worth reading", while "another rambling comment by Smith is best skipped".



(9) Comments are unstructured in a straight linear list.  In reality comments are often focused on a previous comment (and in other environments presented in an indented format). Unfortunately, not here.  



(10) Need a basic trouble shooting flow-chart, diagram, procedure, to <b>guide</b> a user to an appropriate line of questioning.  



For example, "no output".  Is it power supply?  Tube?  Interlock?  Mirror?   



Some basic debugging to focus the question is in order.  Instead those same procedures are presented over and over again every time the question is asked.  They need to be collected in a centralized place.



(11)  A guide or check off sheet for <b>writing</b> a question.   Background details.   



Often a question is presented because the writer is pre-supposing or wishing for an answer.  Then as the comments and questions proceed  the reality appears.  What started off as a "my machine blew a fuse" line of questioning actually was "I completely rewired my machine, swapped out the power supply, did a smoothie conversion, and re-aligned the optics".  "Now my machine is blowing fuses".  These are two very different questions.


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 14:18*

5b - I thought we've been good about people posting in the C3D group. I get them up and running, then when they have software questions I send them to LW and questions about how do I improve this engraving or what water pump should I get I send them here. 


---
**Don Kleinschnitz Jr.** *June 15, 2017 14:18*

Ok, this is all good analysis but I am still wanting to try for an improvement in all this ..... not seeing it :).


---
**Nate Caine** *June 15, 2017 14:36*

**+Don Kleinschnitz**​​​ In your first pass proposal I'd suggest you not partition between "Advice" and "Problem".  (Half the time one is going to merge into the other anyway.)  I'd say focus on the SUBJECT.  The categories you've shown look reasonable.  Some history will tell you how to fine tune them.



Several of my points, distilled down, point out the need for a more automated way of commenting (up-voted, expert users, FAQs, etc).  Sadly you can't fix that, but we've all seen forums that use those techniques.  Google+ isn't structured that way.



As a moderator, do <i>you</i> have the ability to move comments around?  Or apply an "Editor's Choice" or something similar to an existing line of questions?  Some way to cut thru the chaff?



For example, the recent question about gluing on the detached coolant loop plate on the laser tube:  That question has been asked before.  And both times there was a long list of suggestions and counter-suggestions about what sort of glue should work best, how to prepare the surface, how to mask for fume outgassing.  It would be nice to vector the current question to the previously answered one.  Or to "promote" the correct solution, instead of reading the same 20 comments over again.  


---
**Don Kleinschnitz Jr.** *June 15, 2017 15:03*

**+Nate Caine** you have sited a good example in the glue thing. I had to find that post (it was painful) and the user who posted wished he had found it prior to choosing an approach. Lots of wasted effort :(.



..... I need to educate myself more on what I can edit....



Are you suggesting  we should just change the current categories to these [more than 12 but I guess we can tweak them]:

PWM & L CONTROL

LASER POWER SUPPLY

OPTICAL COMPONENTS

COOLANT

LASER TUBE

GANTRY 

AIR ASSIST

AC POWER

LIFT/STAGE

CONFIGURATION

CONTROLLER

LASER FINDER DOT

ENDSTOPS/INTERLOCKS



I do not know what happens to links if we delete/change a category **+Anthony Bolgar**?



Also perhaps putting a "guide" as a post in each section that has links to other posts or resources that deal with common exchanges/solutions etc .... using keywords in the text for searching ?



... and I like the notion of adding troubleshooting guides and "how to write a post". 



Like you referenced I find that I have to ask lots of leading questions to find out the background behind the state of the machine. I have learned not to take things at face value until the state of the machine has been verified.



BTW: to be clear, I am not whining about helping folks, or dissing on those posting. I just can see as this group grows we will be challenged to scale. In addition I hate inefficiency .....



That burning smell is me ....... trying to think........!


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 15:06*

FYI, we've tried all these "user interaction training" approaches, fact of the matter is that a good chunk of new people simply do not read pinned posts or search groups (or know that they can).

I started annotating a collection like so, the idea is that when a question pops up I just take a scroll link to it: [plus.google.com - Cohesion3D Support Questions](https://plus.google.com/collection/8JBoCE)


---
**Don Kleinschnitz Jr.** *June 15, 2017 15:21*

So you collect posts that are relevant and organize them in a set of collections ... mmm

..... I have been doing that for some time for my own ease of archiving K40 stuff so I already have these categories. 



Do you have to collect these under your account?

Do folks have to follow you to get access to them OR can you just link a person directly?



This might be a brilliant recognition that what we have already solves this problem if used in a different way ???:)



I also just noticed that you can pin a item in a collection to the top of the collection. This could be used to highlight other posts and resources.



Lets try can you get to the below? 

[plus.google.com - K40 Laser Cooling](https://plus.google.com/collection/8wXtlB)


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 15:37*

Don I've seen you do this already. The important distinction is that I add a description instead of just "capture". 



If nothing else, the collection is something I will one day task someone with converting to a wiki. 



I'm sure it's all public. I can see your link. 


---
**Nate Caine** *June 15, 2017 15:41*

How do you guide users to the collection?  Do you observe their post, and then post a link to the collection?  Or do you see a more automated way for them to discover it?  



If you only have limited bandwidth, you want to expend your energy on the questions and answers and not on endlessly curating the posts and collections.


---
**Don Kleinschnitz Jr.** *June 15, 2017 15:44*

As an experiment I just added a pinned post to a collection that links to a search term of my blog, exposing multiple posts on a subject. This is pretty flexible. 

[https://plus.google.com/collection/g5JMnB](https://plus.google.com/collection/g5JMnB)



.......

I also added an experimental form to a collection that could be used to troubleshoot and collect data about problems. (as a side benefit it has analytics). 



[plus.google.com - K40 Endstops](https://plus.google.com/collection/wsZDkB)


---
**Don Kleinschnitz Jr.** *June 15, 2017 15:49*

**+Nate Caine** my thinking is when you see a post with a common problem you just link them to the collection and perhaps ask them to fill out the troubleshooting form. Hopefully for common stuff they fill out the form and in the process fix their problem. Even if they don't, the content is captured and an expert can start helping with useful information in hand?

The information in the collection can describe a common problem, point to the common troubleshooting form along with posts that were captured from others with the same problem and how they fixed it.


---
**Anthony Bolgar** *June 15, 2017 17:57*

Sorry I have not got back to you sooner **+Don Kleinschnitz** One thing you can try is what many call centers use in tech support, create a document that has all your troubleshooting info organized by category, then as people have questions, all you need to do is cut and paste from the document. Speeds things up considerably.


---
**Abe Fouhy** *June 15, 2017 21:30*

**+Stephane Buisson**​​ **+Don Kleinschnitz**​​​ **+Nate Caine**​​ **+Ray Kholodovsky**​​ **+Anthony Bolgar**​​ and other moderators, here is my 2 cents as a semi active user who has managed other forums. Let's keep with Google + groups (just pointing at the elephant), it is super at keeping me involved with the k40 notifications, easy posting and viewing while on the toilet and bed (let's be honest haha). It is awesome. We have nearly 4000 members because of the awesome interactions with great content and quick turnarounds, we really have a family here. 

I do agree that the searching sucks on Google plus, I have +1 many comments just to hope the feature of checking at likes would come, no luck yet. Some posts from a year ago I know I may never find. I think an integrated plugin for the group with a knowlegebase would be awesome. After some basic searching I can't find that yet, but found a post on how make canned responses in Q&A format, can you guys take a read?

[https://gsuite.google.com/learning-center/products/groups](https://gsuite.google.com/learning-center/products/groups)/#/  scroll down to "make canned responses"

[gsuite.google.com - Groups – Google Learning Center](https://gsuite.google.com/learning-center/products/groups/#/)


---
**Ned Hill** *June 16, 2017 03:39*

Lot's of good problem definition and thoughts here.  To add my 2 cents, a partial solution to making searching easier maybe to have predefined hashtags (#) to use for posts or commenting.  Like #LASERFINDERDOT, to use one of Don's examples.


---
**Don Kleinschnitz Jr.** *June 16, 2017 22:10*

**+Abe Fouhy** these references are for "

Google Groups" not G+? To my knowledge they are two separate environments?


---
**Abe Fouhy** *June 16, 2017 22:13*

**+Don Kleinschnitz** I thought G+ was google groups?


---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2017 22:17*

This is a g+ community. [Groups.google.com](http://Groups.google.com) is the email list and corresponding web interface. 


---
**Abe Fouhy** *June 16, 2017 22:18*

Ooo oo well I feel a bit silly haha


---
**E Caswell** *June 17, 2017 12:52*

**+Don Kleinschnitz** **+Anthony Bolgar** **+Ray Kholodovsky**

Some valid comments as a "newbie" to the K40 and everything We love about it :-(  the issues I had trying to find solutions.

I am eternally great full of the K40 G+community and the help I have had is second to none. 

However,

Firstly, what is a PWM ? (I found out by researching before asking the stupid question so please don't reply explaining it.) 

I could go down all of the search topics referenced above and I would ask the same question, what are they and what do they reference?

The problem becomes when mods have all the information and they use the same techie speak as they automatically think everyone is starting from the same base level of knowledge.

How would I know I had a PWM or L problem? How would I know if my machine is set up correctly? How would I know I had a optical problem ?

So you see the problem isn't the information on the community, it's the knowledge of people using the community, having said that to make changes to the search function on all the g+ forums I am on would be a massive benefit.

There needs to be an "idiots guide" or something of that ilk to enable the community to guide the questions too. Much like Ray has done on the C3D, I found that really helpful. 

Like I am doing now is setting up an A axis, how, what where and the likes of questions I have searched before asking the question on the community. It's not easy to capture the info easily. 

The only way I have kept up to date with fixes is by saving as a favourite or bookmaking it. 

So in my view what Don says is it does need some work in organisation but the topic questions needs some thought to be able to guide the many newbies to the right place.

Such as

My laser won't work

LPS issues



Correct laser set up (stock)

mirror alignment and the likes



None stock equipment set up.

Smoothie, Grbl 

firmware and config information, how to do it and what it does.



Laser not firing

everything else works but not the laser-



Laser not cutting correctly

not cutting through materials as expected-



Improving cutting ability

1air assist mods or feed speeds-



Laser raster poor quality

PWM and L set up



Gantry moving and response issues-

mechanical bits not moving



Control equipment upgrades

 Smoothie, cohesion and the likes.



That's a starter and how I see it, not sure what you guys think?



thanks for all the efforts in to the G+  and looking at the problems.




---
**Don Kleinschnitz Jr.** *June 17, 2017 13:17*

I have been experimenting with organizing the collections as Ray suggested as I was already doing that for personal reasons. 



[start time out]

**+PopsE Cas** you can use "collections" to bookmark posts its very powerful but like G+ takes a bit of getting used to. If you are interested create a new post on the subject and we will add instructions on how to create a "collection" to a "collection".

[end time out]



I am also creating (using Google Forms)  some experimental "troubleshooting guides" and "data collection forms" as a way to create some self help and data gathering about a problem. 

Example: [docs.google.com - Basic K40 Laser Power Supply Checks](https://goo.gl/forms/ryPrrMqqJRke8l2A3)



We can easily add to the organization the collections and forms along the lines of an "idiots guide". I will put that in que.



**+PopsE Cas** all you say about "newbies" is true, and we all were one at one time or another. 



Editorial: often I don't know someone is a "newbie" and I do not know what their level of technical expertise is.



For example its not useful to explain how to fix a LPS with a DVM when the person does not own one or know how to use one. How you troubleshoot and fix a problem is different dependent on the knowledge of the receiver.



The complexity of tackling a problem is framed with:

... users expertise

....configuration of the machine

....history of the machine

....characteristics of the problem

....a library of solutions



.... and just as a reminder,  creating troubleshooting and Idiots guides take a lot of hours to get right :0! 


---
**Don Kleinschnitz Jr.** *June 17, 2017 14:13*

**+Ned Hill** I tried your hashtag idea and it works pretty nicely....

[plus.google.com - #K40search Searching G+ Playing around with searching G+ and hashtags I foun...](https://plus.google.com/+DonKleinschnitz/posts/5BE57XijdiY)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/HqfsQbeqyqm) &mdash; content and formatting may not be reliable*
