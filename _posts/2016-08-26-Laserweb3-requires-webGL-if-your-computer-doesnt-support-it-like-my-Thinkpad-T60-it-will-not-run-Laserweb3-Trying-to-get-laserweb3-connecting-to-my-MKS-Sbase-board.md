---
layout: post
title: "Laserweb3 requires webGL, if your computer doesn't support it like my Thinkpad T60, it will not run Laserweb3 Trying to get laserweb3 connecting to my MKS Sbase board"
date: August 26, 2016 19:11
category: "Software"
author: "Robi Akerley-McKee"
---
Laserweb3 requires webGL, if your computer doesn't support it like my Thinkpad T60, it will not run Laserweb3



Trying to get laserweb3 connecting to my MKS Sbase board.  Laserweb3 only sees /dev/ttyS[0-3] comports, MKS comes up as /dev/ttyACM0.  Anyone know which file the serial port search happens in?





**"Robi Akerley-McKee"**

---
---
**Robi Akerley-McKee** *August 27, 2016 00:56*

it is....


---
**HalfNormal** *August 28, 2016 00:39*

I have been purchasing used computers from the university and have had to put a newer video card in order to support webGL. Sad when the video card cost more than the computer. I am buying Dell 2.5 Ghz Core 2 Duo machines with 4 gigs of memory and a HD for $15. They can be upgraded to a quad core Q6600 CPU for another $15 from Ebay. Stock they are plenty fast.


---
*Imported from [Google+](https://plus.google.com/103866996421752978311/posts/gVr5iWTQuCj) &mdash; content and formatting may not be reliable*
