---
layout: post
title: "Can someone explain the ppm setting in the Turnkey Tyranny Inkscape plugin?"
date: December 08, 2015 11:45
category: "Software"
author: "Anthony Bolgar"
---
Can someone explain the ppm setting in the Turnkey Tyranny Inkscape plugin? What should I be setting it at?





**"Anthony Bolgar"**

---
---
**ChiRag Chaudhari** *December 08, 2015 19:35*

**+Anthony Bolgar** Can you please tell how were you able to install the plugin? I am running Win 7 64-bit os on two of my pc, Inkscape v 0.91, Python 2.7 installed on c:/Python27 also same folder copied to program files/inkscape and renamed to python. But the darn Plugin simply does not show up under Extension>Export at all. I have also tried both 32 and 64bit versions of Inkscape. There are 3 other plugins under Export but I guess they are defaults. 

Any clue ?




---
**Anthony Bolgar** *December 08, 2015 21:06*

I just followed the directions in the readme file. Make sure that you follow it step by step.


---
**ChiRag Chaudhari** *December 08, 2015 21:17*

Thanks for reply, but guess what I just found out the what was the issue. I was downloaded each file individually by Right-Click and SaveAs, but there was some extra information in that file causing it not to work. I downloaded whole folder then took the files from in there and now it works! 



OOOh dang it. wasted so much time figuring this out.


---
**Anthony Bolgar** *December 08, 2015 21:28*

Glad you were able to figure it out.


---
**ChiRag Chaudhari** *December 08, 2015 21:34*

**+Anthony Bolgar**  Hey where is the PPM setting you are talking about. May be I am so damn happy to get the plugin working, I am going blind. I cant see anywhere PPM settings :O


---
**Anthony Bolgar** *December 09, 2015 00:14*

It is in the layers settings, the plugin uses the following format for naming layers:

40 [PPM=40 Feed=150]



where the first number outside the brackets is percentage of power to the laser, in this case 40%, PPM=pulse per minute (have no idea what it does) and Feed is the feed rate in mm/minute. So anything on this layer will cut at 40% power at 150 mm/minute feed rate.



Here is an excellent tutorial on using the plugin:

[http://bluewraith.blogspot.ca/2015/06/using-inkscape-with-turnkey-tyrannys.html](http://bluewraith.blogspot.ca/2015/06/using-inkscape-with-turnkey-tyrannys.html)


---
**ChiRag Chaudhari** *December 09, 2015 03:29*

Great thanks for the link. Since I was unable to install the plugin since last week, I was gutting out the machine. Almost done with electrical side. 



I have been using Epilogue laser at University of Illinois, and it has Power/Speed and Frequency settings. So I ma guessing this PPM is the frequency part. We cut 3mm plywood at 1000 and Acrylic or any easy to melt material at 5000 frequency there.  



I think higher the frequency lesser the time laser stays ON so it does not heat and melt material, and lower the frequency more time laser stays on and produces more heat to cut through materials like wood. 


---
**John Revill** *May 28, 2016 22:42*

PPM is for pulses per millimeter. a value for 40 is so high it would look like a solid line. I found this setting pretty useless in its standard form as you can not set to duration of each pulse. I am currently working on the extension to make it more flexible. With the new extension, you will be able to se ppm and pulse length. So the end result of something like ppm=0.1,plength=5  would result is 0.1 pulse per mm, or 1 pulse every 10mm (1cm) at a length of 5mm which would look like a dotted line when 50% on on and 50% is off. I use ppm to create perforated cuts in paper and thin cardboard.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/2Ag2D9Jfxxm) &mdash; content and formatting may not be reliable*
