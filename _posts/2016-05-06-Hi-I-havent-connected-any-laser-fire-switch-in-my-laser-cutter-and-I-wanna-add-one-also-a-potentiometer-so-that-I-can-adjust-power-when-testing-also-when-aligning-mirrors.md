---
layout: post
title: "Hi, I haven't connected any laser fire switch in my laser cutter and I wanna add one also a potentiometer so that I can adjust power when testing also when aligning mirrors"
date: May 06, 2016 08:03
category: "Smoothieboard Modification"
author: "Athul S Nair"
---
Hi,



     I haven't connected any laser fire switch in my laser cutter and I wanna add one also a potentiometer so that I can adjust power when testing also when aligning mirrors. I'm using smoothieboard and using TL to trigger laser on and off (by M3 and M5 command). I just wanna know can I make this connection without messing up smoothieware (Already finished connections in blue colour)

![images/429e11deb3becf0a7c999bde582b7c08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/429e11deb3becf0a7c999bde582b7c08.jpeg)



**"Athul S Nair"**

---
---
**Jean-Baptiste Passant** *May 06, 2016 09:35*

Hmmm, even though the wiring may look correct, there is a thing that doesn't look right to me.



I'm ok for the Switch, should be good.



But the pot wiring got me thinking...

Let's say I turn the pot to 50% so 2.5V, then the pin 2.3 will receive 1.65V. Unless the level shifter is one way only and have protection. The smoothieboard shouldn't receive any current by this pin, and unless it is protected by something I may not be aware of I would not recommend it.



You can however wire the pin 2.3 to the 5V on the pot, and use the pot as a limiter. So you can ask the smoothieboard to output 100% and use the pot to limit it to X%.

I would not recommend it anyway, because 50% of 5V is 2.5V, but if you ask the smoothieboard to output 3V, then 50% is only 1.5V.

So % does not change but Voltage will. 



Maybe **+Stephane Buisson** can help you ?


---
**Athul S Nair** *May 06, 2016 10:00*

**+Jean-Baptiste Passant** Mine is a bi-directional LLC.  Also say the software is running smoothieboard (cutting a material at 50% PWM (2.5V at the IN and 1.65 V at pin2.3 ) and if I acidently turn the pot to max , wouldn't that cause trouble?

DO you have this setup in your machine? If so can you tell me the connection?



[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)



But this guide looks similar to my connection diagram.


---
**Athul S Nair** *May 06, 2016 10:11*

The guide I posted above is written by @Stephane Buisson  But in which he connected the pwm pin to L and there's no IN connection from board


---
**Jean-Baptiste Passant** *May 06, 2016 11:18*

Well looks like you have the same power supply I have, and I personally got rid of the pot :S

I believe **+Stephane Buisson** have another power supply but I am not sure.



Also I am working on a guide on my setup because it's not exactly the same as in the guide.



I got rid of the pot and switch myself so I cannot really help.


---
**Athul S Nair** *May 09, 2016 11:59*

**+Jean-Baptiste Passant** [https://drive.google.com/open?id=0B5_IWI68ZqZTeXh5MllMaUR6eUU](https://drive.google.com/open?id=0B5_IWI68ZqZTeXh5MllMaUR6eUU)



What about this?


---
**Jean-Baptiste Passant** *May 09, 2016 18:43*

Looks better for me. Did not thought about that.


---
**Vince Lee** *May 12, 2016 02:57*

Why do you need the m3 and m5 commands to enable the laser separate from the pwm?  The pwm by default shuts off for g0 commands and turns on for g1 commands.  Is this intended as some sort of secondary safety feature?


---
**Jean-Baptiste Passant** *May 12, 2016 06:47*

**+Vince Lee** Safety but also different power supply.


---
**Vince Lee** *May 12, 2016 13:36*

**+Jean-Baptiste Passant** even with a different power supply I would try test if the TL line responds well to pwm at some frequency.  Would simplify much.


---
**Jean-Baptiste Passant** *May 12, 2016 14:07*

**+Vince Lee** Well, in my case TL doesn't respond well to pwm, and using a switch is not that much complication. I'm no sure what's wrong exactly, but I prefer it that way.


---
**Athul S Nair** *May 13, 2016 06:48*

In my case if I directly connect TL and WP to ground and  USB connection with smoothie board is not established my laser PSU will produce laser .



[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)

See the functions of control interface

TL= low

WP= low

IN=hang in the air <s>-</s>> laser at 40%



SO I thought setting up a switch module is a good idea


---
**Vince Lee** *May 13, 2016 12:33*

**+Jean-Baptiste Passant** yeah mine did not respond well to pwm at first either until I increased the pwm interval to about 5000 microseconds if I remember right.


---
**Vince Lee** *May 13, 2016 13:02*

**+Athul S Nair** I thought it was something like that.  If you can't pwm control tl and the pwm line is open when powered down maybe you can add a large value pulldown resistor and change the configuration so the pin isn't open drain and has no pullup.  Alternatively, add a regulator so the smoothie powers up with the laser.


---
**Stephane Buisson** *May 14, 2016 10:18*

I like the ideas you develop here, the difficulties (as community point of view, not particular case) is the different power supply (at least 3 differents one).

I am quiet open to other solutions, the blue box link is what work for me, and was based on the way the Moshiboard inject the signal into my PSU. that don't mean it's no other solution. please comment (edit) the blue box link with your experience.


---
**Athul S Nair** *May 18, 2016 06:29*

**+Vince Lee** 

You suggesting about adding a pull down resistor to IN terminal of power supply to avoid powering of laser when USB connection is not established?



 Another solution to the problem would be adding a pull up resistor to the TL terminal.

 

I thinks that's what the configuration does(one with M3 and M5). 

But instead of  adding an external pull up resistor , it can be enabled in the configuration( ^ : pull up pin)


---
**Vince Lee** *May 18, 2016 13:23*

**+Athul S Nair** My suggestion for a pulldown on the "in" line was meant as a possible alternative for having to also control the "tl" line to prevent the laser from firing when the smoothie board is powered down.


---
*Imported from [Google+](https://plus.google.com/114978037514571097180/posts/MxAWWe4jmzS) &mdash; content and formatting may not be reliable*
