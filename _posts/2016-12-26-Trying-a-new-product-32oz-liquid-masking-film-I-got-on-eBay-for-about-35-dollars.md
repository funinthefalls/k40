---
layout: post
title: "Trying a new product. 32oz liquid masking film I got on eBay for about 35 dollars"
date: December 26, 2016 20:37
category: "Discussion"
author: "3D Laser"
---
Trying a new product.  32oz liquid masking film I got on eBay for about 35 dollars.  I made a few pieces already and it masks so much better than masking tape 

![images/bbd6175ab362b885a96c1ba46cba2ab5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bbd6175ab362b885a96c1ba46cba2ab5.jpeg)



**"3D Laser"**

---
---
**Ned Hill** *December 26, 2016 21:15*

How easy does it come off?  Says, to use on non porous surfaces so I imagine it probably will not work well on wood unless it's sealed.


---
**3D Laser** *December 26, 2016 21:49*

**+Ned Hill** on the acrylic it works fantastic haven't tried it on wood yet


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 22:10*

Interesting. Do you just apply with a brush or sponge?


---
**3D Laser** *December 26, 2016 22:20*

I have a sponge roller from the dollar store I use it works great 


---
**Arion McCartney** *December 27, 2016 02:06*

This is probably a newbie question, but does that just allow the laser to engrave the surface of clear acrylic glass? Does it burn into the surface and discolor it? Thanks.


---
**3D Laser** *December 27, 2016 02:10*

**+Arion McCartney** I make a lot of acrylic tokens for the game Star Wars Armada after I cut the tokens I go back and color fill the engravings.  I have been using masking tape but it can leave a residue plus the areas where the tape overlaps causes the engraving to not be even.  With this it covers the entire material and the. I cut and engrave the tokens leaving the film on the areas I still want black then I color fill with a paint pen.  Once dry I remove the film leaving a clean finish 


---
**Arion McCartney** *December 27, 2016 02:15*

**+Corey Budwine**​ thanks for that explanation. Very neat process. Would you mind posting a picture of the finished product?


---
**3D Laser** *December 27, 2016 02:23*

**+Arion McCartney** sure here goes a simple one but came out clean ![images/46b806ac4bf471b87523e4cf6b2d6145.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/46b806ac4bf471b87523e4cf6b2d6145.jpeg)


---
**Arion McCartney** *December 27, 2016 02:25*

Looks good thank you!


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/8S5TznYqPvX) &mdash; content and formatting may not be reliable*
