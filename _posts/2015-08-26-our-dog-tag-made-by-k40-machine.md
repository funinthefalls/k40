---
layout: post
title: "our dog tag made by k40 machine"
date: August 26, 2015 03:31
category: "Discussion"
author: "Haotian Laser"
---
our dog tag made by k40 machine

![images/4106bd5fa00a5ecdb28ab346aaab8e6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4106bd5fa00a5ecdb28ab346aaab8e6a.jpeg)



**"Haotian Laser"**

---
---
**Kirk Yarina** *August 27, 2015 16:30*

What software, font, and settings are you using for those?  They're nice looking characters.


---
**Haotian Laser** *August 28, 2015 02:31*

thanks for your love. Our software is corellaser controller, can you tell me your email or skype? i will send the video how make it to you directly. My email is amber@htlasercnc.com and skype is 18365894301@163.com, pls contact me if interest. thanks.


---
**Stephane Buisson** *August 28, 2015 08:49*

no need for a new post, here is **+Haotian Laser** video


{% include youtubePlayer.html id="QIaU1_s_ufE" %}
[https://www.youtube.com/watch?v=QIaU1_s_ufE](https://www.youtube.com/watch?v=QIaU1_s_ufE)


---
*Imported from [Google+](https://plus.google.com/100621511215713605855/posts/YvhNUD4us1h) &mdash; content and formatting may not be reliable*
