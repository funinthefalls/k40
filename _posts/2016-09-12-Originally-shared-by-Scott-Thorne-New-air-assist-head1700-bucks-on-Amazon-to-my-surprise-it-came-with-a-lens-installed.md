---
layout: post
title: "Originally shared by Scott Thorne New air assist head....17.00 bucks on Amazon, to my surprise it came with a lens installed"
date: September 12, 2016 04:07
category: "Discussion"
author: "J.R. Sharp"
---
<b>Originally shared by Scott Thorne</b>



New air assist head....17.00 bucks on Amazon, to my surprise it came with a lens installed.

![images/b5aa0861f60b891eb78fdc5114990033.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b5aa0861f60b891eb78fdc5114990033.jpeg)



**"J.R. Sharp"**

---
---
**Stevie Rodger** *September 12, 2016 05:06*

could you share a link to this please, as i would like to purchase one also. Thanks


---
**Ariel Yahni (UniKpty)** *September 12, 2016 11:46*

**+Stevie Rodger**​ here is the link [https://plus.google.com/+ArielYahni/posts/6UDfdShztaB](https://plus.google.com/+ArielYahni/posts/6UDfdShztaB)


---
**Stevie Rodger** *September 12, 2016 11:58*

Thank you **+Ariel Yahni** 


---
*Imported from [Google+](https://plus.google.com/116586822526943304995/posts/gXd2WioTJhH) &mdash; content and formatting may not be reliable*
