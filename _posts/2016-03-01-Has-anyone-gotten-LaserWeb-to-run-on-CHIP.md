---
layout: post
title: "Has anyone gotten LaserWeb to run on C.H.I.P"
date: March 01, 2016 03:16
category: "Discussion"
author: "Sean Cherven"
---
Has anyone gotten LaserWeb to run on C.H.I.P. The $9 Computer?



Link: [http://www.getchip.com](http://www.getchip.com)



I'd much rather use that than the Raspberry Pi.



**+Peter van der Walt**​, maybe this is something you can write up a tutorial on?





**"Sean Cherven"**

---
---
**ThantiK** *March 01, 2016 03:19*

People don't even have C.H.I.P's delivered yet.  Only people who have them is kernel hackers and developers.  First batches don't go out until May.  It even says June 2016 right on their home page.


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2016 03:20*

Yeah, I ordered mine on cyber Monday and June is the timeframe I got as well. 


---
**Sean Cherven** *March 01, 2016 03:21*

Not true, they're on the third batch I think. I was signed up for the second batch and I got mine a few weeks ago. 


---
**Sean Cherven** *March 01, 2016 03:23*

So far I like it. Seems to be very fast. A lot faster than the Raspberry Pi B.


---
**ThantiK** *March 01, 2016 04:31*

There's nothing at all keeping it from running.  Install NodeJS via whatever package manager its distro comes with and run it.  LaserWeb is run via an interpreter; there's nothing to be done on Peter's side.


---
**ThantiK** *March 01, 2016 04:33*

Looks like it is as simple as apt-get install nodejs and also installing git and following the already existing instructions on the laserweb github.


---
**Sean Cherven** *March 01, 2016 04:41*

Hmm.. I may give it a try after I convert my controller board. 



I find it interesting that nobody else here has gotten their C.H.I.P. yet. But then again, I got mine through kickstarter...


---
**ThantiK** *March 01, 2016 04:45*

I did too.  Except, if I wanted the HDMI adapter or ANY other addition, I had to wait for the non-alpha versions.  I suspect a lot of people chose that route.  People who got theirs early, ONLY had the options for the barebones C.H.I.P. with...<b>shiver</b> ANALOG video.



I don't even think I OWN anything that will do analog anymore.


---
**Alex Krause** *March 01, 2016 05:20*

I joined this group for all the brilliant people that are members here a laser or 3d printer is my next investment and following the same process I used before building my OX CNC I'm feeling out all the opensouce communities before I dive into something. There are alot of SOC alternatives out there what would be my best bet in having an integrated solution to control my new hobbies I'm waiting on my Pine64 but I've heard many good things from raspi and beaglebone. Read a few write-ups on chip and a few other SOC. Any input would be gladly welcomed 


---
**Joshua Harris** *March 01, 2016 06:18*

I have a CHIP sitting on my desk right now. I was a kickstarter backer, and I've had mine for a few weeks now. 



That said, I haven't had a chance to do more than look at it yet, so I can't help with any other info.








---
**Stephane Buisson** *March 01, 2016 07:16*

**+Sean Cherven**  what's your strategy ?

A)just running the server on CHIP (usb K40 connected) & use it from Chrome on a powerful PC on the network ?

B) all on the CHIP?



If A (a bit like octoprint), a RasPi should be enough

If B need reactivity  tests. (sorry don't know how powerful is CHIP)


---
**Sean Cherven** *March 01, 2016 13:28*

**+Stephane Buisson**​, Option A most likely.



**+ThantiK**​, all of my LCD Flat Screen TV's still have analog input. That's how I been using it. Just wish the resolution was a bit higher, lol.


---
**Sean Cherven** *March 01, 2016 13:33*

What I'd really like to see done, is a shield for the C.H.I.P. or Raspberry Pi that will hook up directly to the K40 (eliminating the need for a controller board, and eliminating the need for the slow USB/Serial port.



That would be just awesome.


---
**Jon Bruno** *March 02, 2016 16:47*

I have laserweb installed on my CHIP. I haven't had a chance to control anything with it but it does run. 

I was going to write up a doc but I've since forgotten everything I did to get it running.. So I guess my comment is useless beyond the fact of saying that I did it.


---
**Jon Bruno** *March 02, 2016 16:48*

**+ThantiK** I got mine from the first backer batch.


---
**Jon Bruno** *March 02, 2016 16:51*

I basically just followed the Raspi2 install, but had to use Adafruits NodeJS .. [https://learn.adafruit.com/node-embedded-development/installing-node-dot-js](https://learn.adafruit.com/node-embedded-development/installing-node-dot-js)


---
**Jon Bruno** *March 02, 2016 16:52*

[https://plus.google.com/+PetervanderWalt/posts/gXtqm5QRsBB](https://plus.google.com/+PetervanderWalt/posts/gXtqm5QRsBB)


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/Eg5JHSgtsex) &mdash; content and formatting may not be reliable*
