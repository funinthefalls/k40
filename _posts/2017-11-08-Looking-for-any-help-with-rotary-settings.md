---
layout: post
title: "Looking for any help with rotary settings"
date: November 08, 2017 14:53
category: "Original software and hardware issues"
author: "John Warren"
---
Looking for any help with rotary settings. 



After many revisions of my rotary device I finally have one that I can work with.  (not the best, but functional).  I was using Scorch works k40Whisperer software but was running into issues compressing the issues to accommodate for the round surface.  I would have to reduce the height by up to 50% to make it the right size when burned.  This started causing issues with more detailed images.



Yesterday I switched to corellaser that was supplied with the device.  working with the rotary settings has allowed me to burn with more detail and less changes to the image file.  



Issue I am running into is when the program is done the rotary homes but is not where it started.  So if I wanted to run the program a 2nd time on the same thing it is in the wrong place.  usually this wouldn't be a problem but sometimes I do want to run it again to clean up some problems.



Does anyone know any special setting that I may have missed?  or is it more likely my makeshift rotary?



any help or advice is much appreciated!!





**"John Warren"**

---
---
**Joe Alexander** *November 09, 2017 15:37*

as you are using a homemade rotary the info on that would help diagnose any issues.


---
**John Warren** *November 09, 2017 17:14*

Mine is based off of this design.



[thingiverse.com - Rotary Axis by nottingham82](https://www.thingiverse.com/thing:2430836)



I used a nema 17 motor connected to the Y connector.  I then scrapped the 2 gear wheels (I kept having intermittent binding issues) and replaced them with a printed 60 tooth gt2 wheel and a 16 tooth attached to the stepper.  200 mm gt2 closed belt and I no longer skip steps.  



The X axis returns to the same spot, but Y (rotary) comes up short.


---
*Imported from [Google+](https://plus.google.com/114420019102377897867/posts/6gxr8eWSL3M) &mdash; content and formatting may not be reliable*
