---
layout: post
title: "What are everyone's preferences regarding the laser homing itself when you first power it up?"
date: June 28, 2017 20:40
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
What are everyone's preferences regarding the laser homing itself when you first power it up?  The stock board does this. 



Do you like this?  Comment with deeper thoughts. 



ADD: Please tell me why in the comments. 





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 21:11*

Please say why you have this preference. 


---
**Jeff Lamb** *June 28, 2017 21:14*

I prefer that it doesn't as I always feel more comfortable when the machine moves because I've told it to rather than on it's own. It also means I'm more likely watching the machine so if it catches something like the air hose I'm already on hand to sort it rather than kidder the belts to death.


---
**Stephen Sedgwick** *June 28, 2017 21:16*

Homing on power-up makes it easier to pull material in and out of the machine.  The downside is if you are correcting an error in between and need to change the build but want to start from the same position - material spot.  However homing on power up to me allows items to be taken in and out of the machine easier.


---
**Alex Krause** *June 28, 2017 21:21*

**+Stephen Sedgwick**​ I home at the beginning and end of a gcode cycle no need to home on power up for me


---
**greg greene** *June 28, 2017 21:21*

I like it to do it as it ensures starting from a known point.  My problem is sometimes when I home via command, it doesn't seem to believe it even though the LW numbers are changed and begins a cut where it shouldn't.


---
**Jim Fong** *June 28, 2017 21:24*

First thing I do after laserweb connects is to hit the home button. Good indication that the board is working  right. 


---
**Joe Alexander** *June 28, 2017 21:26*

I believe that if you make a text file on the sd card with a certain name and put the commands in there it will execute on board boot. It was quite a while back that I read this and unfortunately cannot remember the specifics. Ill start looking for it.


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 21:28*

Yep, and that's what we're researching for the need of here **+Joe Alexander**


---
**Ashley M. Kirchner [Norym]** *June 28, 2017 22:09*

I prefer it homes when I tell it to. There's been a time or two when I had stuff in the machine and hit the power button only to have the head crash into whatever was in it (including, but not limited to, my hand.) Having to manually do it for me is just a normal step now which will also confirm it's working properly.


---
**Tony Sobczak** *June 29, 2017 00:33*

Home only when I want it to. 


---
**Steve Clark** *June 29, 2017 01:00*

I put yes but it would be nice if the control asked " yes or no"


---
**Arion McCartney** *June 29, 2017 04:55*

I prefer do not home on power up. I just like to be in control of when homing takes place. 


---
**Claudio Prezzi** *June 29, 2017 07:19*

I clearly prefer manual homing. Automatic homing is a risk if something is in the way.


---
**John-Paul Hopman** *June 29, 2017 14:33*

I could see homing on startup to get the laser head out of the way, though then you have to bring the laser back to the center to adjust the focal height.



I suppose if you really wanted to home on start-up, it might actually be better to home post-cut, rather than startup. That way the laser is out of the way to access the object and when you start up the next day, the laser is practically homed.


---
**Nathan Thomas** *June 29, 2017 15:58*

Home on start up. I cut from the exact same position every time anyways. Having to move the laser head out the way to start a new job would be an unnecessary step for what I do.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/SBXfgj3nGr3) &mdash; content and formatting may not be reliable*
