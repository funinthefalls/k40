---
layout: post
title: "Reposting here on behalf of Donna Gray ."
date: June 03, 2016 00:05
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Reposting here on behalf of **+Donna Gray**.



I need help with my k40 laser cuter The laser just stopped working the head moves still it just seems to have no power to the laser The laser tube looks perfect no cracks or scorch marks I have looked all over it how do I fix this it is only a month old I do test fire and nothing happens It has a green light that is on at the power box but red light does not come on when I press test fire can you please help me I am devastated that my laser cutter doesn't work





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Alex Krause** *June 03, 2016 00:26*

Was this also posted on the Facebook group?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2016 00:36*

**+Alex Krause** Not sure as I don't use facebook. Just Donna posted it on one of my posts so I thought I would repost here as people here have better electrical knowledge than I do.


---
**Alex Krause** *June 03, 2016 00:38*

Is she located in Australia?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2016 00:42*

**+Alex Krause** Yeah. Has the issue been solved? If so, I'll delete the post.


---
**Donna Gray** *June 03, 2016 06:16*

No Issue is still not resolved and yes it was me in the facebook post also I thought maybe it was different people in that group I have a person going to look at the machine next week so if there is any info I could give him where to start would be appreciated We are going to check the connection wires beside the test fire light and he is going to check out the laser tube also but anything that can help him would be great


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2016 06:40*

**+Donna Gray** I would suggest they check the entire power supply to determine if power is getting to the tube from there. Just make sure he is aware of the high voltage (so no-one gets zapped).


---
**Donna Gray** *June 03, 2016 06:44*

Yep all good he is an electrical technician and he is very aware of the high voltage he is the first local person who understood everything I relayed from the posts from all the helpful people so fringers crossed he can help also the supplier has said they will replace the whole power supply but the catch is they want me to order it etc and pay for it but then say they will pay???? I am a bit suss about that


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2016 07:17*

**+Donna Gray** Yeah that sounds totally sus. I wouldn't be doing it that way. Did you pay through PayPal originally? If so, I would open a case through PayPal & keep all the communication with the seller through the case there.


---
**Donna Gray** *June 03, 2016 07:21*

yep I think I will do that I asked them to refund me the amount I need to pay for the new power supply first so I can then order the new power unit Just waiting now for their reply otherwise I will open a case with paypal


---
**Derek Schuetz** *June 03, 2016 07:30*

**+Donna Gray** if you want more than an automated answer open the claim. The they'll try to have with you 


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Dj4FfhFCPq1) &mdash; content and formatting may not be reliable*
