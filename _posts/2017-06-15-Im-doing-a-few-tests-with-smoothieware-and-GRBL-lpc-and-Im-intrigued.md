---
layout: post
title: "I'm doing a few tests with smoothieware and GRBL-lpc and I'm intrigued"
date: June 15, 2017 00:51
category: "Discussion"
author: "Bruno Ferrarese"
---


I'm doing a few tests with smoothieware and GRBL-lpc and I'm intrigued. 



GRBL-LPC top / Smoothieware bottom - two different materials (3mm birch plywood / 2.7mm Chinese plywood from Home Depot)



GRBL gives me a weird result. it's stronger on the left/right edges and faded out in the middle while smoothieware yelds an even result despite stuttering.



Both smoothie and grbl have the same conf (I had to change grbl to support 0.005 junction deviation that I'm using with smoothie). Same pwm freq (200 smoothie / 5000 grbl). Every single configuration parameter match. C3D controller. 



Laser Raster Merge (using vector - same happens with regular raster/image)

Laser min:0 / max:50

Laser dia: 0.1

Cut rate: 250mm/s



BTW, same happens using either M3 or M4 on GRBL side.



Ideas/comments/questions?



**+Claudio Prezzi**





![images/a73eee6464aacc4fcb067644b10bddbe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a73eee6464aacc4fcb067644b10bddbe.jpeg)
![images/c0d6eaa11708669566425ed913598996.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c0d6eaa11708669566425ed913598996.jpeg)

**"Bruno Ferrarese"**

---
---
**Bruno Ferrarese** *June 15, 2017 01:14*

The so called photographer that lives inside me is deeply embarrassed.  Let me try to redeem myself.



BTW, each group (wood/2017/leaf) is roughly 22x6mm. 

![images/f3ea1a8e86f5d87abed2eebfb9f06043.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3ea1a8e86f5d87abed2eebfb9f06043.jpeg)


---
**Todd Fleming** *June 15, 2017 02:13*

There's extra burning where the machine accelerates/decelerates. Both Smoothie and grbl-lpc compensate, but that can only do so much since lasers aren't linear.



On both Smoothie and grbl-lpc: Use Laser Raster Merge's overscan option to move the acceleration outside of the image area.



Smoothie: Use the SD Card to improve performance.




---
**Bruno Ferrarese** *June 15, 2017 02:25*

Yes, I have tried overscan (3mm seems enough) but then I get it all very light compared to smoothie. I would have to double laser power to get the same darkness. Smoothie also seems to give a slightly better/crispy result. 

I love grbl but not getting the same quality. 


---
**Todd Fleming** *June 15, 2017 02:29*

There's a tradeoff with wood: slow scan with less intensity produces a darker image. Try running Smoothie over the sdcard; it should speed up and produce something more similar to what grbl produces, assuming you have both set to the same PWM frequency. 


---
**Bruno Ferrarese** *June 15, 2017 03:49*

I just did it. Indeed the same result as GRBL.  


---
**Paul de Groot** *June 15, 2017 06:17*

What happens when you run on half of the normal speed?


---
**Bruno Ferrarese** *June 15, 2017 18:25*

Sorry, a pipe burst in the garage. Guess what is right by the water softener? :( Got wet but nothing major. I'll give it a couple of days for it to dry out really well before firing again. Looking at the bright side of life its 100F/38C today so I feeling lucky. 


---
*Imported from [Google+](https://plus.google.com/111443543748488809183/posts/WVrVfwBRgdi) &mdash; content and formatting may not be reliable*
