---
layout: post
title: "Please tell me. Can I put 20mm dia"
date: August 08, 2017 17:28
category: "Discussion"
author: "\u0e1b\u0e31\u0e0d\u0e0d\u0e32 \u0e2d\u0e34\u0e19\u0e17\u0e23\u0e4c\u0e18\u0e07\u0e0a\u0e31\u0e22"
---
Please tell me. Can I put 20mm dia. focus lens into original K40 bracket?

![images/d3b0617a99d93faf7fa552b3a588b501.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d3b0617a99d93faf7fa552b3a588b501.jpeg)



**"\u0e1b\u0e31\u0e0d\u0e0d\u0e32 \u0e2d\u0e34\u0e19\u0e17\u0e23\u0e4c\u0e18\u0e07\u0e0a\u0e31\u0e22"**

---
---
**Nate Caine** *August 08, 2017 21:22*

I measured mine.  The threads will clear a lens of 21.2mm.  So a 20mm will fit.  I was concerned about the lens thickness to be certain the the threads engage, but it looks good.



<b>Remember:</b>  <i>Bump side up.  Flat side down.</i>



Why do you want a 20mm lens?  The original lens is either 18mm or 19mm, and only the center 10mm is used anyway.  There is even provision for using a 12mm lens.  



You may want to put an O-ring around the lens perimeter to keep it centered.


---
**Mark Brown** *August 08, 2017 22:39*

The standard lens is 12mm, on mine at least.


---
**ปัญญา อินทร์ธงชัย** *August 09, 2017 02:18*

20mm lens so easy to buy in my country. that is my reason. so if it can mount, it should be work well right?


---
**Mark Brown** *August 11, 2017 04:35*

I think it would be fine.  Like Nate said you might want to put something around it (laser cut a ring?) to keep the lens centered in  the head. 


---
**อมรชัย ธนธันยบูรณ์** *August 24, 2017 02:37*

+ปัญญา อินทร์ธงชัยInner size of cap is 20.2 mm. You can use 20mm lens.


---
**ปัญญา อินทร์ธงชัย** *August 24, 2017 02:43*

 thank you


---
*Imported from [Google+](https://plus.google.com/102276185937218204651/posts/4wor47iihtq) &mdash; content and formatting may not be reliable*
