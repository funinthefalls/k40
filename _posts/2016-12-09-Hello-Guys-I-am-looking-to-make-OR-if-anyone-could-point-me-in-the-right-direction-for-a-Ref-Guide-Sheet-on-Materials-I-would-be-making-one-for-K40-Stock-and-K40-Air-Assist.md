---
layout: post
title: "Hello Guys , I am looking to make OR if anyone could point me in the right direction for a Ref/Guide Sheet on Materials I would be making one for K40 Stock and K40 Air Assist"
date: December 09, 2016 13:54
category: "Materials and settings"
author: "Jeffrey Rodriguez"
---
Hello Guys , 



I am looking to make OR if anyone could point me in the right direction for a Ref/Guide Sheet on Materials I would be making one for K40 Stock and K40 Air Assist. just want to help out the community and new comers as myself have something to go off . If anyone can set the example in such format below I'll be adding it to a spreadsheet and uploading it ( if it hasn't been done as I said before ) or just an updated spreadsheet :) . 



Acrylic - 9mm/s : 65% PWR : 8 Passes



Birch Wood (Michaels) - 22mm/s : 50% PWR : 7-8 Passes



For acrylic I am looking for that smooth edge Finish if anyone can help me out I would grately appreciate it ! 



Cheers 





**"Jeffrey Rodriguez"**

---
---
**Cesar Tolentino** *December 09, 2016 15:14*

This is going to be tough since the intensity of laser changes as it age. so everybody will have a different cut reaction. Even on the same material. The one that came from my garage is harder to cut compared to the one that is already in the house. I believe it's because of the moisture in the material. 



Check my previous post somewhere. I posted a dxf, svg and dwg file as a test cut for every material you cut... Similar below.



![images/e47d904bb4fecdfb6788d47b9fad7d8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e47d904bb4fecdfb6788d47b9fad7d8c.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 09, 2016 16:27*

Also, due to variations in the measurement of the power (some of us use mA & others use %) will also make it difficult. If I recall correct, **+Ariel Yahni** was working on a reference sheet a while back? But can't remember 100% on that.


---
*Imported from [Google+](https://plus.google.com/111028342291028521666/posts/ZwgmvqHPQSX) &mdash; content and formatting may not be reliable*
