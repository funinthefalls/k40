---
layout: post
title: "is there a big difference between:"
date: November 26, 2017 20:18
category: "Discussion"
author: "Dag Elias S\u00f8rdal"
---
is there a big difference between:





**"Dag Elias S\u00f8rdal"**

---
---
**Ned Hill** *November 26, 2017 21:26*

A CVD (Chemical Vapor Deposition) lens is a lens with an antireflective coating that reduces transmission loss due to reflection.  Also will reduce image ghosting.  Becomes more important at higher power, but I don't have any info to say how much of a difference there is between coated and uncoated at the K40 power level.


---
*Imported from [Google+](https://plus.google.com/+DagEliasSørdalurm8/posts/H17L5zC3Fh7) &mdash; content and formatting may not be reliable*
