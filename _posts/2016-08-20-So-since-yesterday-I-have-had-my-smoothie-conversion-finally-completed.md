---
layout: post
title: "So, since yesterday I have had my smoothie conversion finally completed"
date: August 20, 2016 01:37
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So, since yesterday I have had my smoothie conversion finally completed. So now I'm playing around with LaserWeb3 trying to find what settings I'm happy with. Seem to have got it sorted now I think.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Changed my spot size in LW3 to 0.5. I originally had it set at 0.15. Now things are looking good :)



From left to right, spot size 0.15, 0.5, 0.25, 0.4.



I prefer the result of #2 or #4 (so 0.5 or 0.4 spot size) than the others.



10mA @ 500 white 200 black.

![images/f7a125bda758491402f157501fff1b12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f7a125bda758491402f157501fff1b12.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Scott Marshall** *August 20, 2016 01:54*

Where did I hear .5??



Nice work!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 20, 2016 01:56*

**+Scott Marshall** I think you suggested it to me ;) Hahaha


---
**I Laser** *August 20, 2016 02:32*

You joined the dark side **+Yuusuf Sallahuddin** ~!!!


---
**Alex Krause** *August 20, 2016 02:44*

**+Yuusuf Sallahuddin**​ .5 or .05 dot size?


---
**Eric Flynn** *August 20, 2016 03:12*

#2 has that old woodcut look, but #4 looks better overall IMHO.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 20, 2016 03:12*

0.5 dot size. Not .05. Mind you, these particular pieces of wood are 18mm thick & I didn't adjust z height... So they're probably about 15mm out of focus compared to a 3mm piece of wood (that I am focused for).


---
**Eric Flynn** *August 20, 2016 03:16*

That would make a huge difference.  However, a defocused beam is useful for some things, depending on what you want to achieve.  For instance, when cutting, I always focus a bit into the material, not on top of it. Genrally gives better results. Depending on the material for engravings, a slightly defocused beam may give better results as well.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 20, 2016 03:18*

**+Eric Flynn** Yeah, same... when cutting I focus half-way through the material.


---
**Eric Flynn** *August 20, 2016 03:20*

I try to get in the center, but I usually just fudge it close enough.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 20, 2016 03:21*

**+Eric Flynn** Famous Aussie saying: Near enough is good enough haha.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Ze9W1H2racn) &mdash; content and formatting may not be reliable*
