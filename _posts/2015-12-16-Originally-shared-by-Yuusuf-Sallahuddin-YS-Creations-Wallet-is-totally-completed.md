---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) Wallet is totally completed"
date: December 16, 2015 10:12
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Wallet is totally completed. So from start to finish:

Design the wallet inner/outer; design the image for the outer; laser engrave image; laser cut pieces; airbrush pieces; hand-stitch pieces.



The laser & airbrush make things interesting. Never airbrushed before, so it's a new level of what-can-be-done for me to play around with.



![images/ad59a5c001ab0dd6e4fbd9fb5bc30542.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad59a5c001ab0dd6e4fbd9fb5bc30542.jpeg)
![images/0917d3a87595b3c4549d0f7aa799c7c3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0917d3a87595b3c4549d0f7aa799c7c3.jpeg)
![images/6615600ff1a2746e734356f8e0a8c36e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6615600ff1a2746e734356f8e0a8c36e.jpeg)
![images/bc1c23863a04783a55e2472497e89f54.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bc1c23863a04783a55e2472497e89f54.jpeg)
![images/cd269a3a75ac06d108b32c8ca9734f60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cd269a3a75ac06d108b32c8ca9734f60.jpeg)
![images/dbd41908c0b9d28b86a00a1257dafc23.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dbd41908c0b9d28b86a00a1257dafc23.jpeg)
![images/2bdba3134a57c5b28160082ad9cbf9d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2bdba3134a57c5b28160082ad9cbf9d9.jpeg)
![images/382c57d03623d5c740da8d39f50f635b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/382c57d03623d5c740da8d39f50f635b.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Stephane Buisson** *December 16, 2015 11:51*

Really nice, you are doing very well with this craft.

could the K40 help you to enjoy and concentrated, by simplifying your job.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 15:25*

**+Stephane Buisson** Thanks Stephane. I've been doing it for a while, but never with such nice results. I am really enjoying the K40 laser & it's assistance with my craft. Probably the best $500 I have spent in a while.


---
**ChiRag Chaudhari** *December 16, 2015 16:38*

Wow man, you really are an artist. Looks like you and leather get along very well. Its like a painting. With everyday use how long you think the colors last? I mean before it starting to really fade.



I always have trouble finding perfect wallet for myself, as I like it very thin. Big wallets dents my butt :P. I have to ask you to make one for me. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 18:59*

**+Chirag Chaudhari** Thanks very much for that. This wallet (before putting cards in) is actually very thin. So thickness depends how much money you put in it too haha.



I'm not 100% sure about the colouring & time it will last, however as it is a dye & embeds into the leather, I imagine it will last a reasonable amount of time. Since this is the first piece I have done like this, I don't have experience with the time-frames as yet.



I would be happy to discuss making one for you & design you would like.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/64WekuidVnj) &mdash; content and formatting may not be reliable*
