---
layout: post
title: "Just playing around while I was at the laser"
date: June 18, 2016 20:43
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
 #yearofthelaser  

Just playing around while I was at the laser. Thought I'd engrave this on leather & then hit it with some colourful dyes.

![images/c3d9a7bc8f78a1a32d4aa96228188b4f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c3d9a7bc8f78a1a32d4aa96228188b4f.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ariel Yahni (UniKpty)** *June 18, 2016 21:59*

Indeed and I have still not done leather #yearofthelaser 


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/jJjT75k9kfA) &mdash; content and formatting may not be reliable*
