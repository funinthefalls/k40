---
layout: post
title: "Hello everyone....i have a big problem with my K40 ...."
date: June 19, 2015 22:43
category: "Software"
author: "Dario Vallesi"
---
Hello everyone....i have a big problem with my K40 .... i engrave my drawing...and it's ok.... when i select a part (layer) of the same drawing to cut it (an hole for keychain) the machine cut it starting to the top left point ...as you can see in the pic.....like it start again from the zero point....it happend with everything i try to cut (or engrave again) i can't understand why! anyone can help me? i use Moshidraw 2014

![images/217b98b3a2d36842851c2a11c005bb5e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/217b98b3a2d36842851c2a11c005bb5e.jpeg)



**"Dario Vallesi"**

---
---
**Steve Moraski** *June 19, 2015 23:00*

I ran into the same issues as you.  My terminology may be incorrect so bear with me.  What I did to correct this is to make sure that the cut lines and the image to engrave are two separate objects.  On the right hand side of the screen should be a list of your objects on the object manager tab an example name might be Polyarray1_Carve.  Make sure your cut and engrave images are separate.  Once they are first make sure that only the object you want to engrave has the check box checked and engrave with preferred settings .  When it is time to cut uncheck the object you engraved and check the one you want to cut.  For both of these operations make sure you right click and select all. 


---
**Chris M** *June 20, 2015 07:33*

Any easy way to make sure that all cuts and engraves are aligned is to put a small (1mm) object in the top left wate area of the design, so that it is the leftmost and topmost object in the design. Make sure it is present in each layer to be cut or engraved, and it will align on the work peice and everything else will be lined up too.


---
**Dario Vallesi** *June 20, 2015 09:14*

Thanks guys, i'll try as you suggest...and I'll let you know!


---
**Dario Vallesi** *June 21, 2015 12:09*

the trick with the dot on top left works!!! ...but it's strange to fix the proportion issue with this "bug" ...


---
*Imported from [Google+](https://plus.google.com/112890511120063209880/posts/Z2BhNJ6gkko) &mdash; content and formatting may not be reliable*
