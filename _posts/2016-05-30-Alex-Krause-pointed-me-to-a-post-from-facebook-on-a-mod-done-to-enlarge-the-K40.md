---
layout: post
title: "Alex Krause pointed me to a post from facebook on a mod done to enlarge the K40"
date: May 30, 2016 12:20
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
**+Alex Krause** pointed me to a post from facebook on a mod done to enlarge the K40. He sims to get 20x50. Since before getting mine i was thinking on doing this. Seems that reusing the Y axis linear guides and adding a longer X axis should not be to difficult. Thougths 

![images/eb204cbb392baceac23019570acfb449.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb204cbb392baceac23019570acfb449.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Stephane Buisson** *May 30, 2016 12:22*

I saw one like this on ebay a year ago ;-))


---
**Anthony Bolgar** *May 30, 2016 12:30*

Where are the electronics supposed to fit now that he has claimed that space for the gantry? Seem like a waste of effort, the stock machine without that stupid vent opening can do a max of 334 X 234mm


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 30, 2016 12:35*

It is a slight improvement being able to get 50 instead of 30, but I second Anthony's question. Where to put the electronics now? I think it's probably better to just rip out all the stuff you want & build an entire new frame/chassis to the size you wish & recycle the k40 chassis for something else.


---
**Miguel Sánchez** *May 30, 2016 12:36*

I would think of that if you absolutely need it ( 200x500 mm is an odd size anyway), and only after you have everything else under control. Otherwise you may create new problems even before you solved the old ones. Just my two cents.


---
**William Steele** *May 30, 2016 13:07*

The electronics are clearly in behind under the laser... seems there is plenty of room there.   I find the argument about 500 not being worth it funny... that's a 20 inch cut... seems like someone could find a lot of use over a 12 inch (if you're lucky) cut.



If you've already upgraded to Ramps/Smoothie, then you're most of the way there anyway... so a little re-positioning and a few new parts and you've doubled the original machine's cutting size.



Are their specific plans for this upgrade somewhere out there, as I am definitely going to do it to one of my K40s just for the the fun of it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 30, 2016 13:10*

**+William Steele** Depends on what you are cutting, but that extra 200mm could come in handy, but 500mm x 200mm is not a workable area for my needs. If I am going to go to all the effort of upgrading the size, I would like to upgrade the Y length also. But, 500mm is better than 300mm.


---
**Ariel Yahni (UniKpty)** *May 30, 2016 13:22*

Yes Electronics are all in the back. The guy only posted pictures.  I think a few openbuilds profiles can make this easy. 


---
**William Steele** *May 30, 2016 13:38*

The original K40 can easily be upgraded (stock) to 300x250... by eliminating that silly blocking tube on the linear rail and removing the motor cover.  Then, you rotate the focus lens mount 180 to the back side of the gantry.  Last step is to remove the air duct in the back (under the laser.)  That, combined with this, will give you a nice 250 x 500 cutting area.


---
**Ariel Yahni (UniKpty)** *May 30, 2016 13:50*

Push the Psu to the back and place the board under the right hand door


---
**Björn König** *May 31, 2016 18:51*

Like the 500mm. Could do front plates for rack units ...


---
**Don Kleinschnitz Jr.** *August 23, 2016 14:09*

This is interesting but if I go to this much trouble seems like I should go all the way to cutting front and rear slots and mount z table below. Or a new frame that does it all :). 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/ZJHS6hb2PeM) &mdash; content and formatting may not be reliable*
