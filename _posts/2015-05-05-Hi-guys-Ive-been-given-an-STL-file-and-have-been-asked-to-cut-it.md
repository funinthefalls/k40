---
layout: post
title: "Hi guys. I've been given an STL file and have been asked to cut it"
date: May 05, 2015 19:06
category: "Software"
author: "Stuart Middleton"
---
Hi guys. I've been given an STL file and have been asked to cut it. The STL is a 3D model of a piece of acrylic but nothing I have will load it and I'm having trouble finding anything to just extract the 2D shape. Any ideas?





**"Stuart Middleton"**

---
---
**Stephane Buisson** *May 05, 2015 19:18*

I'am.a fan of sketchup, it could import export stl with plugin, dxf too.

Otherwise blender is a swiss army knife for format convertion.

Both are free.


---
**Stuart Middleton** *May 05, 2015 19:45*

Thanks. I've just tried sketchup with the plugin but it gives an error. I'll give blender a go. 


---
**Imko Beckhoven van** *May 05, 2015 20:19*

Use a cad program and create a 2d view export dwg... I use autodesk inventor but freecad shoud be able to do the trick


---
*Imported from [Google+](https://plus.google.com/+StuartMiddleton1/posts/by2THSWrCZN) &mdash; content and formatting may not be reliable*
