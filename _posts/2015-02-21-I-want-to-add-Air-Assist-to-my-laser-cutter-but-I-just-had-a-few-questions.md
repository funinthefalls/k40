---
layout: post
title: "I want to add Air Assist to my laser cutter, but I just had a few questions"
date: February 21, 2015 17:35
category: "Modification"
author: "Sean Cherven"
---
I want to add Air Assist to my laser cutter, but I just had a few questions.



1. Is this air pump strong enough?

[http://www.amazon.com/dp/B002JPRNOU/](http://www.amazon.com/dp/B002JPRNOU/)



2. What tubing should I use? I see some machines using a coiled tubing, does anyone know where I can find this coiled tubing?



3. What air assist nozzle do you recommend?





**"Sean Cherven"**

---
---
**Stephane Buisson** *February 21, 2015 17:54*

Hi Sean and welcome here.



This one could do (1), you don't need much pressure, excessive pressure would be too hard on some detailed model.



(2) on Ebay  you will find everything you need. coil or not it's up to you depending on the noozle and the way you will mount it. just pay attention at  your X max position the tube could bump into the frame or be in the way.



(3) you have 2 options, the cheap one something based on my design with the laser crosshair line, advantage it's cheap if you have a 3D printer and you don't need to re-align the laser path,

 or better  that one [http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)


---
**Sean Cherven** *February 21, 2015 17:59*

Do you think that air pump is too strong?


---
**Stephane Buisson** *February 21, 2015 18:03*

I don't know that exact model, but 20w power input don't look much to me.


---
**Sean Cherven** *February 21, 2015 18:04*

Should I go bigger? or do you think this one is enough?


---
**Stephane Buisson** *February 21, 2015 18:07*

mine is bigger 60w, maybe too much (but it's leaking)


---
**Jim Coogan** *February 21, 2015 19:01*

I have an aquarium pump (3.5w) made for a 60 gallon tank, I use the same hose they use in a fish tank, and I use the air assist Stephane pointed out.  I also have a fish tank heater in my water because the laser is in an unheated but insulated garage/shop.  The reason for the fish tank gear is because I am experimenting to see what a minimal and optimal setup would be for others that have asked me.  I am 99.99% sure the air pump is too small but I have 4 other pumps that I will also be testing.  Too much pressure and parts move and you create turbulence around the cut line which could throw material around.  Most posts I have seem talk about 20-30 psi as a good range for pressure from a pump or compressor.  


---
**Tim Fawcett** *February 21, 2015 21:37*

Sean, I am using an AS-18 compressor to run the air-assist - this is blowing through a 3mm diameter tube - it does seem to nicely blow the crap away from the cutting point but it is perhaps a little over the top. The pump you have seen does seem to appear on flea-bay listed as an air assist pump. I don't think you need a lot of power - the main thing is to blow the smoke away from the cut point to prevent it scattering the beam. As long as the blow nozzle is close to the later dot point, you just need enough air to blow everything away


---
**Eric De Larwelle** *February 21, 2015 22:16*

**+Tim Fawcett** I agree, keeping the path of the laser clear of smoke is the main objective. A little blow is better than a blast.


---
**Bill Parker** *March 13, 2015 06:01*

I have just had a reply from an Ebay seller who says it needs minimum 80LPM and if money not the problem then 140LPM is perfect for the job. That could be him being greedy and wanting to make more money.


---
**Bill Parker** *March 13, 2015 06:07*

This was my reply from Ebay seller

Hi,



Thanks for your interest. I went through a lot of cheap pumps and ended up buying this one which works very well but is pricey. But buy cheap, buy twice (or 5 times in my case).



[http://www.ebay.co.uk/itm/140-LTR-PISTON-AIR-PUMP-KOI-POND-FISH-AQUARIUM-TANKS-HAILEA-HYDROPONICS-ACO009E/400822797147?_trksid=p2047675.c100011.m1850&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D28111%26meid%3Dae42cbd483fd4fe78b986709d21f642e%26pid%3D100011%26prg%3D11472%26rk%3D1%26rkt%3D10%26sd%3D390970974098](http://www.ebay.co.uk/itm/140-LTR-PISTON-AIR-PUMP-KOI-POND-FISH-AQUARIUM-TANKS-HAILEA-HYDROPONICS-ACO009E/400822797147?_trksid=p2047675.c100011.m1850&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D28111%26meid%3Dae42cbd483fd4fe78b986709d21f642e%26pid%3D100011%26prg%3D11472%26rk%3D1%26rkt%3D10%26sd%3D390970974098)



The 140 litre one I have is noisy but it works and if that's not a problem for you then you can't go wrong with it... at first I tried a submersible water pump (like the one that comes with the K40 but it wasn't powerful enough). I then tried a couple of 12v tyre inflators with a high psi but the air flow wasn't consistant. The best of all I tried was an air pump and you'll need at least an 80-100 litre air one.



There are various Hailea models and this listing is overpriced but happens to have all of them listed.



[http://www.ebay.co.uk/itm/Hailea-Piston-Air-Pumps-ACO-208-ACO-308-ACO-318-ACO-388D-ACO-009E-/281256754499?pt=UK_HomeGarden_Garden_PondsWaterFeatures_UK&var=&hash=item417c357d43](http://www.ebay.co.uk/itm/Hailea-Piston-Air-Pumps-ACO-208-ACO-308-ACO-318-ACO-388D-ACO-009E-/281256754499?pt=UK_HomeGarden_Garden_PondsWaterFeatures_UK&var=&hash=item417c357d43)



If price is not an issue, I'd go for the 140litre one. If noise is a bigger issue then 80 litres will work. You will also find it for cheaper on eBay yourself. I just included those listings as a reference and have no affiliation with them.



Let me know if you have any other questions.



Regards,


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/WUxva5Xx1XN) &mdash; content and formatting may not be reliable*
