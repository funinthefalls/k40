---
layout: post
title: "This weekend I got the motorized Z bed from Lightobjects installed with a Z min end stop on my Smoothieboard"
date: November 19, 2018 17:31
category: "Discussion"
author: "Steve Prior"
---
This weekend I got the motorized Z bed from Lightobjects installed with a Z min end stop on my Smoothieboard.  I've got Laserweb4 configured with Z support so it can raise/lower the bed just fine.  It seemed to make the most sense to have the Z home down instead of up so homing would have no chance of crashing the head into whatever was on the bed.



What I'm looking for now is tips on the best workflow with this setup.  My current thought is to determine the best focus point to the top of the bed and set that as the default Z for operations in Laserweb4, then measure the thickness of whatever I am engraving, subtract that from the default Z and use that for the operation height.



But what workflow have other people used with this configuration?  Does homing to Z min work out the best in real life?  Is there a better way of doing all of this?





**"Steve Prior"**

---


---
*Imported from [Google+](https://plus.google.com/+StevePrior/posts/agAF2X5DzXt) &mdash; content and formatting may not be reliable*
