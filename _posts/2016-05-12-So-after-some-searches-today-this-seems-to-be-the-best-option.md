---
layout: post
title: "So after some searches today this seems to be the best option"
date: May 12, 2016 02:48
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
So after some searches today this seems to be the best option. What do you think [http://m.aliexpress.com/item-desc/32408231582.html](http://m.aliexpress.com/item-desc/32408231582.html)





**"Ariel Yahni (UniKpty)"**

---
---
**ThantiK** *May 12, 2016 03:11*

So, when you got your laser was the tube visibly, physically damaged?


---
**Alex Krause** *May 12, 2016 03:15*

I would contact the seller first to make sure you aren't in one of the remote areas listed where they Jack up the shipping cost


---
**Ariel Yahni (UniKpty)** *May 12, 2016 03:16*

Not at first sight , but after seeing the crack today i went back to many pictures i took when unboxing and it was there.


---
**Ariel Yahni (UniKpty)** *May 12, 2016 03:17*

Already talked to him. Its 180.00 DHL to Panama in 5 days. If its broken they send another one, i dont sea another solution to guarantee a safe delivery


---
**Ray Kholodovsky (Cohesion3D)** *May 12, 2016 03:19*

What is happening with the original seller of the K40? I don't think they would refund you, maybe/ hopefully they would send a new tube. Can you dispute the original purchase?


---
**Ariel Yahni (UniKpty)** *May 12, 2016 03:22*

they asked me for some videas, photos, etc so waiting on their response


---
**ThantiK** *May 12, 2016 03:52*

**+Ariel Yahni**, sometimes it's just bad luck.  In most cases, the tubes are well packed and make it to their destination in one piece.  I suspect due to your location, the chances of the tube getting cracked is a bit higher.


---
**Alex Krause** *May 12, 2016 03:55*

The box mine came in had no Fragile handle with care labeling or even a this side up sticker which blows my mind 


---
**Ariel Yahni (UniKpty)** *May 12, 2016 03:58*

Mine neither. Also mostly packed God but the exhaust tube was crushed also by the laser lid, so not much care there


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/BkhhLReAe7Z) &mdash; content and formatting may not be reliable*
