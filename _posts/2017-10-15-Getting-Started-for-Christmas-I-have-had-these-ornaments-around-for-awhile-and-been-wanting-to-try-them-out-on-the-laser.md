---
layout: post
title: "Getting Started for Christmas! I have had these ornaments around for awhile and been wanting to try them out on the laser"
date: October 15, 2017 22:57
category: "Object produced with laser"
author: "HalfNormal"
---
Getting Started for Christmas!



I have had these ornaments around for awhile and been wanting to try them out on the laser. My wife added the color touches. They are glass.



![images/b12cee5948728126279e6a6b7d8925fa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b12cee5948728126279e6a6b7d8925fa.jpeg)
![images/0081492aec44fbf8aec3c21c76ac36f7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0081492aec44fbf8aec3c21c76ac36f7.jpeg)

**"HalfNormal"**

---
---
**Ned Hill** *October 16, 2017 14:33*

That's an awesome idea.  Nice job.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/8DTGwzkRYkn) &mdash; content and formatting may not be reliable*
