---
layout: post
title: "Black marble photo engrave.....very nice results....beware though...its very soft use high as speed...low power"
date: October 28, 2016 22:28
category: "Object produced with laser"
author: "Scott Thorne"
---
Black marble photo engrave.....very nice results....beware though...its very soft use high as speed...low power.

![images/4587142edf7c59af37b6d75196985efe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4587142edf7c59af37b6d75196985efe.jpeg)



**"Scott Thorne"**

---
---
**Scott Marshall** *October 29, 2016 09:06*

That's amazing.


---
**Scott Thorne** *October 29, 2016 10:20*

**+Scott Marshall**....thanks man. 


---
**Tony Schelts** *October 29, 2016 16:18*

Really Nice, 




---
**Nick Williams** *October 30, 2016 21:28*

Hey Scott really nice work, I have admired a number of your pieces. Did you prep the marble with something to bring out the white,or is that the natural result of engraving on the black marble??


---
**Scott Thorne** *October 30, 2016 21:50*

That's all natural...no prep...really low power and fast speed....it's softer than you would think. 


---
**Craig** *October 31, 2016 06:32*

When you say High speed low power - what is that on the K40 laserdrw software. It wants MM/s speed and has an analog dial to show mAmps for power.  What amp/speed did you use for granite?


---
**Scott Thorne** *October 31, 2016 09:11*

300mm/s at around 5mAs


---
**Bob Damato** *October 31, 2016 12:35*

Thats terrific Scott. How did you prep your image beforehand? Ive done some marble stuff but it didnt look this good.




---
**Scott Thorne** *October 31, 2016 12:46*

Photoshop...grayscale dithered and sharpened...thanks **+Bob Damato**


---
**Bob Damato** *October 31, 2016 15:11*

**+Scott Thorne** when you say dithered do you mean as converting to bitmap in ps?


---
**Scott Thorne** *October 31, 2016 15:32*

Yes..I'll explain in better detail when i get off work today. 


---
**Bob Damato** *October 31, 2016 23:44*

**+Scott Thorne** cool, thank you, I look forward to it.




---
**Scott Thorne** *November 01, 2016 10:44*

First this to do is change the dpi to 300 for marble and 600 dpi for wood....then change the image to grayscsle....then use the unsharp mask to soften then edges...then use diffusion dither on the image...its in the grayscale settings....adjust contrast and brightness accordingly....good luck.


---
**Bob Damato** *November 01, 2016 15:08*

Thank you **+Scott Thorne**!


---
**ED Carty** *November 05, 2016 13:18*

still amazing me my friend.. still amazing me




---
**Scott Thorne** *November 05, 2016 13:30*

**+ED Carty**...thanks man....good to hear from you. 


---
**ED Carty** *November 05, 2016 14:01*

Yewa it has been a while. I switched jobs. Now im a maintenance manager so i have been very busy. But i have been keeping an eye on you my friend. good work.


---
**ED Carty** *November 05, 2016 14:02*

i dont know how you do it, but you are getting better. I need to get with you soon. My daughter is getting married and i have a project to make outta wood. you interested in making a few extra $$ ? i would pay you for supplies and time..


---
**Scott Thorne** *November 05, 2016 18:22*

**+ED Carty** just let me know my friend....I'm a Maintenance  Supervisor for the company i work for a well. 


---
**ED Carty** *November 05, 2016 20:13*

**+Scott Thorne** sweet. I appreciate it. So we both know the wors of the job. Cool.


---
**Richard Gallatin** *June 18, 2017 21:48*

Scott very good work. I am starting out trying granite with some crappy results. I do not have the ma meter so I got no idea what to set my power at. I have read and tried 85 % but that seems way to high. 


---
**Scott Thorne** *June 21, 2017 00:03*

**+Richard Gallatin**. ..I run min...at 10% and my max at 45% with a speed of 300 mm/s..thanks for the comment by the way. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/Gu67u1UDc6V) &mdash; content and formatting may not be reliable*
