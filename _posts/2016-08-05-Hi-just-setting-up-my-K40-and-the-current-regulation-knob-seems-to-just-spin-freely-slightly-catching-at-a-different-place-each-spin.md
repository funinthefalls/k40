---
layout: post
title: "Hi, just setting up my K40 and the current regulation knob seems to just spin freely, slightly catching at a different place each spin"
date: August 05, 2016 16:38
category: "Original software and hardware issues"
author: "Frank Fisher"
---
Hi, just setting up my K40 and the current regulation knob seems to just spin freely, slightly catching at a different place each spin.  Is this normal?  How do I know how where it is meant to be?





**"Frank Fisher"**

---
---
**Scott Marshall** *August 05, 2016 16:52*

Sounds like a loose knob screw


---
**Jon Bruno** *August 05, 2016 16:53*

It may have loosened up on you during shipping.



if you look closely the center of the knob ( the hub of the knob) it has a cap.. You should be able to easily pull it off.

Under that cap is a small nut that can be used to tighten the collet that holds the knob onto the shaft.


---
**Jim Hatch** *August 05, 2016 16:58*

As to "where it's meant to be" - there's no "right" place to set it. You're going to want to do some test cuts to match the pointer to the ma gauge. I used a sharpie to put some tuck marks for 0 (where it's just about to lase), 10/20/30/50/75/100% power using 20ma as 100%. You can push it higher but that'll shorten tube life.



Other people have made nice labels but I didn't bother.


---
**Frank Fisher** *August 05, 2016 17:04*

OK thanks, I can live with that, manually calibrated using a sharpie for the moment.  As long as I push it gently back to both marks I know roughly where it is, tightening the nut hasn't helped so far but I am moving on to the next (bigger) problem.


---
**Jon Bruno** *August 05, 2016 17:07*

If the nut is secure there may be damage to the potentiometer.


---
*Imported from [Google+](https://plus.google.com/114228670405979897634/posts/FTtniessTvF) &mdash; content and formatting may not be reliable*
