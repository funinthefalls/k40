---
layout: post
title: "Christmas lasered postcard ! (at )"
date: December 23, 2016 14:36
category: "Object produced with laser"
author: "Stephane Buisson"
---
Christmas lasered postcard ! (at 
{% include youtubePlayer.html id="wRyA6qJ9RUs" %}
[3:00](https://youtu.be/wRyA6qJ9RUs?t=185&t=3m00s))





**"Stephane Buisson"**

---
---
**greg greene** *December 23, 2016 14:45*

very well done


---
**Madyn3D CNC, LLC** *December 23, 2016 17:13*

This is great and I just subscribed to this guys channel. Thanks for sharing!




---
**Stephane Buisson** *December 23, 2016 17:15*

great channel by the way ;-))


---
**Don Kleinschnitz Jr.** *December 24, 2016 13:50*

Awesome and great channel....


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/F7AAZQaBFDJ) &mdash; content and formatting may not be reliable*
