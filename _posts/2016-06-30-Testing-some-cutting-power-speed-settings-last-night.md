---
layout: post
title: "Testing some cutting power/speed settings last night"
date: June 30, 2016 15:24
category: "Object produced with laser"
author: "Tev Kaber"
---
Testing some cutting power/speed settings last night. At 6mA (which I used at 320mm/s for the engrave), 9mm/s cut through this 3mm plywood, at 10mm/s it didn't quite make it. 



![images/cb9d86981422849d03f834f479bfaf8f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cb9d86981422849d03f834f479bfaf8f.jpeg)
![images/71110279a222c75f04e7a6f87e7a7622.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/71110279a222c75f04e7a6f87e7a7622.jpeg)

**"Tev Kaber"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 30, 2016 15:40*

Very nice piece. The engrave is quite a nice contrast to the non-engraved area.


---
**Michal Benda (RewDroid)** *June 30, 2016 16:13*

Nice job! Good detail


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/AV8uWN1Tdob) &mdash; content and formatting may not be reliable*
