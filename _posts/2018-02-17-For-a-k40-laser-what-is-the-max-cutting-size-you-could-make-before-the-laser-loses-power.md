---
layout: post
title: "For a k40 laser what is the max cutting size you could make before the laser loses power?"
date: February 17, 2018 03:15
category: "Modification"
author: "Dar Thw"
---
For a k40 laser what is the max cutting size you could make before the laser loses power? I want to build a custom cabinet and redo my k40 to cut 24 x 24 or so.  Will that be fine? Also what is the purpose of having the laser tube parallel to the x axis? Could you not have the laser tube going straight to the mirror at the x and y junction?  Thanks everyone for this awesome community, Ive already learned alot from reading around.





**"Dar Thw"**

---
---
**Printin Addiction** *February 17, 2018 03:21*

Remember, the design is for making cheap affordable lasers to cut rubber stamps, and having the tube go straight would make the cabinet much larger, and cost more money without much benefit, so I would think you should be fine in both cases IMHO.




---
**Dar Thw** *February 17, 2018 03:28*

**+Printin Addiction** Yeah I figured the main reason was form factor. Was just thinking if it would help in the having enough power to reach out to 24 inch x 24 inch by not having one more mirror to absorb energy.


---
**Joe Alexander** *February 17, 2018 05:58*

An idea that some have went with would be to put the laser tube on your gantry, removing that accumulating beam distance and balancing the power over the work area more efficiently. Would be an easier alignment also i'd imagine.


---
**Duncan Caine** *February 18, 2018 09:09*

If you do decide to go ahead with your size upgrade I'm sure there are lots of K40 user who would be interested, so please do keep us all informed of your progress.


---
**Peter Alfoldi** *February 18, 2018 22:09*

**+Joe Alexander** Hi, i have seen somewhere the tube on X Gantry, but just don't see the benefit (still need 3 mirrors) so I'm wondering how could that help? In my opinion the added mass on gantry + cabling complexity just isn't worth it... though I may be missing something..

And directing the tube on Y axis - yes, you save a mirror, but the tube will stick out 70cm from back - again don't think it's worth it


---
**Dar Thw** *February 19, 2018 16:48*

Yeah I dont see moving the tube around with wires and watercooling hoses hanging off of it being too good. I will be drawing up plans for a increasing the size soon, from what Ive been reading as long as Ive got good alignment the laser should have plenty of power to cut 24" x 24".


---
*Imported from [Google+](https://plus.google.com/107923441357901969822/posts/bVve28YctVa) &mdash; content and formatting may not be reliable*
