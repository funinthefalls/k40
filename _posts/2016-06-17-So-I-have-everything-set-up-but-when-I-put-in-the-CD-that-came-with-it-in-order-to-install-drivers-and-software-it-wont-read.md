---
layout: post
title: "So I have everything set up, but when I put in the CD that came with it in order to install drivers and software, it won't read!"
date: June 17, 2016 04:36
category: "Software"
author: "Evan Fosmark"
---
So I have everything set up, but when I put in the CD that came with it in order to install drivers and software, it won't read! Does anyone have a copy shared out somewhere?





**"Evan Fosmark"**

---
---
**Alex Krause** *June 17, 2016 04:53*

**+Yuusuf Sallahuddin**​ has it post if you dig thru the community 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 04:58*

I have a copy in my dropbox. But I suggest using the CorelDraw X5 version (rather than the v12 they give). It's also "non-genuine".



Link here: [https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0](https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0)



You should find 3 files in that folder. 1 for CorelDraw x5, 1 for CorelLaser & 1 readme that goes through the install/keygen setup stuff.



Also note, I added a block in my windows firewall after installing to block Corel Draw from being able to communicate with the internet. To prevent it trying to check if it's legit version haha.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 05:00*

Or if you want the original cd (with all the crazy chinese files) it is here: [https://drive.google.com/folderview?id=0Bzi2h1k_udXwM3lXS21zeFRKZm8&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwM3lXS21zeFRKZm8&usp=sharing)



named K40 Software CD.rar.



For some reason, if I recall correct, the CorelDraw X5 doesn't work from google drive. Someone tried to download it & google had removed the file from the list or removed the keygen from within the file. So if you want that version grab from the Dropbox link.


---
**Evan Fosmark** *June 17, 2016 05:19*

You guys are awesome. Thanks Yuusuf! I'll start installing. Hoping to get a test cut soon.


---
**Bruce Garoutte** *June 17, 2016 20:52*

Thanks Yuusuf, I to shall use this one.


---
**Hayden DoesGames** *June 18, 2016 16:11*

Yuusuf your a legend! Don't have my laser yet but im itching to start doing a few programs for the big day :) many thanks!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 18, 2016 18:16*

**+Evan Fosmark** **+Bruce Garoutte** **+Mark Jones** You're all welcome. Have fun & can't wait for more people like you to join the laser crew!


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/ZYmUHeYNWea) &mdash; content and formatting may not be reliable*
