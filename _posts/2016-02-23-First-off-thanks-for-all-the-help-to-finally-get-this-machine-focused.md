---
layout: post
title: "First off thanks for all the help to finally get this machine focused"
date: February 23, 2016 02:16
category: "Discussion"
author: "3D Laser"
---
First off thanks for all the help to finally get this machine focused.  now I am having another issue.  I can't get it to cut through 1/8 inch material.  I included a photo.  as you can see the lines are super wide.  the material is about two inches away from the laser, I can't figure out how to get this thing to cut and it is driving me crazy!  please help!

![images/43422b716c9bf41f459c6db869be7655.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43422b716c9bf41f459c6db869be7655.jpeg)



**"3D Laser"**

---
---
**ThantiK** *February 23, 2016 02:35*

Take a long scrap piece of wood.  Lay it across your bed with one side propped up the "two inches" you say that it's away from the laser head.  Laser cut a line that goes the length of the scrap.  Find the spot that is the thinnest, and then prop your material so it's all at that height.  It should cut right through.


---
**3D Laser** *February 23, 2016 03:11*

The thing is, the piece I am currently trying to cut spans the length of the bed and I can't get through it.  very frustrating


---
**Jim Hatch** *February 23, 2016 03:30*

**+ThantiK**​'s suggestion will tell you what your focal point is (vs what it "should" be). You might also want to make sure your lens isn't in upside down.


---
**3D Laser** *February 23, 2016 03:56*

Thanks both of you guys where correct looks like my lens was upside down and my local length is about a half an inch closer than what it should be at 7mm a sec and 75% power I was just able to cut through the material thanks guys


---
**Scott Marshall** *February 23, 2016 05:39*

If you're cutting 1/8 ply in one pass, even @ 7mm/sec, you've got a good alignment. now.


---
**Tony Schelts** *February 23, 2016 23:23*

I bought the light objects air assist nozzle and 18mm lens,  The machine was okay before, but i can now cut 3mm birch ply at 8mm a sec at 7ma.  If you lower your table so that your barely getting a hit, then put a piece of ply and raise one end you can work out the best height for the lens, by getting it to cut a line.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/8WHCGkmgaVd) &mdash; content and formatting may not be reliable*
