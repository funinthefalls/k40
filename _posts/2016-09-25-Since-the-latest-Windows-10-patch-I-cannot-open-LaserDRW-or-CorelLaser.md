---
layout: post
title: "Since the latest Windows 10 patch, I cannot open LaserDRW, or CorelLaser"
date: September 25, 2016 01:11
category: "Software"
author: "Jamie Lowe"
---
Since the latest Windows 10 patch, I cannot open LaserDRW, or CorelLaser. What do I do to fix this? I can't seem to figure it out. I have uninstalled and reinstalled all the software. 





**"Jamie Lowe"**

---
---
**Mike Grady** *September 25, 2016 07:59*

I had the same problem, do a google search for uninstall windows update windows 10


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 25, 2016 12:40*

I just checked mine, Windows 10 Pro, version 1511, OS Build 10586.589 & my updates are all up to date. I tested my CorelLaser & it works still (CorelDraw x7 with CorelLaser plugin 2013.02).


---
**Jamie Lowe** *September 25, 2016 15:13*

I did a system restore and that has allowed me to open it. Now I can't see the launcher for engraving within CorelLaser though.

![images/b088337ec62ae1472af5c3f473b2e77e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b088337ec62ae1472af5c3f473b2e77e.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 26, 2016 01:39*

**+Jamie Lowe** You will find an icon down near the clock in windows (may have to click the arrow) that will allow you to access all the CorelLaser features (cut/engrave/options/etc).


---
*Imported from [Google+](https://plus.google.com/101114189070867293080/posts/JAN68oEtuvY) &mdash; content and formatting may not be reliable*
