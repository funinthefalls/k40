---
layout: post
title: "Could someone please sanity check me. I think the power supply has had is on my K40 but cannot be sure"
date: October 20, 2015 10:37
category: "Hardware and Laser settings"
author: "Peter"
---
Could someone please sanity check me.

I think the power supply has had is on my K40 but cannot be sure.

All the voltages read ok, 0V, 5V, 24V or 240Vac but the IN from the wiper on the pot reads 1.6V and 4V when there is nothing attached to it. the 1.6 or 4 is random; can be either when i turn off/on and before/after I press test fire. Am I missing something? Should there be voltage at that pin somehow?



The pot was fried when I looked inside to clean it and I dont think 5V did that. Ive changed the pot for a multi turn one of the same values and cannot get a stable output from either one.




{% include youtubePlayer.html id="h9Yoje-6hPA" %}
[https://youtu.be/h9Yoje-6hPA](https://youtu.be/h9Yoje-6hPA)



[https://drive.google.com/file/d/1JJeBa5Q4_8FJ5VloqNVIFbwc4l6O26b2yA/view?usp=sharing](https://drive.google.com/file/d/1JJeBa5Q4_8FJ5VloqNVIFbwc4l6O26b2yA/view?usp=sharing)





**"Peter"**

---
---
**Stephane Buisson** *October 20, 2015 11:08*

with a bit of cautious (don't have the same board), but your value seem similar to mine.



In is PWM (Pulse), need oscilo to trace it.

and you could got a floating signal from the board.



What problem are you solving ?

does your K40 fire ?


---
**Peter** *October 20, 2015 11:15*

It does fire but voltage seems to go from 0 to 1.6 to 4+.

mA reading on the dial is either just under 10 or around 20. Cannot for the life of my get the current to change smoothly with the old (damaged) or new (multi turn) potentiometer.


---
**Peter** *October 20, 2015 11:23*

(if in is PWM then I wouldnt need a new PSU to upgrade to smoothieboard or other? How can I check for sure its PWM IN as I thought most originals weren't)


---
**Stephane Buisson** *October 20, 2015 11:24*

working on the subject now, keeping or not the pot is the question. the advantage would be to limit phycally the current, but the current amount level will be pushed by the smoothie. (if i am not wrong)


---
**Joey Fitzpatrick** *October 20, 2015 12:58*

If your voltage is fluctuating wildly like that(on the output pin). You most likely have a bad ground to your pot.  You can always try and use a different ground point(I believe that they are all tied together on these power supplies)


---
**Peter** *October 20, 2015 14:38*

Problem solved, sort of.

The seller had some kind of hissy fit when I asked a question about replacement through Amazon and thought I should know that it caused some kind of black mark on his account. Requested that I send the unit back for refund. 



Problem I have now is where to get another from a reliable UK seller. Preferably one with air assist and digital display.


---
*Imported from [Google+](https://plus.google.com/+PeterJones79/posts/KGZKVQZAdRf) &mdash; content and formatting may not be reliable*
