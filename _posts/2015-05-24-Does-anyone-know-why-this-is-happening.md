---
layout: post
title: "Does anyone know why this is happening?"
date: May 24, 2015 19:27
category: "Hardware and Laser settings"
author: "David Wakely"
---
Does anyone know why this is happening? 

![images/73a62a71a44ef061666b7be5f0613dc8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/73a62a71a44ef061666b7be5f0613dc8.jpeg)



**"David Wakely"**

---
---
**Dan Shepherd** *May 24, 2015 19:34*

That is usually caused by loose belts. There were some posts on how to adjust them yesterday in the facebook {laser engraving and cutting }group  [https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
**David Richards (djrm)** *May 24, 2015 19:38*

Greetings David, i'm not really sure what problem you are trying to show, but I have seen something similar when trying to cut at a speed faster than that my machine could cope with without skipping steps. Kind regards, David.


---
**Sean Cherven** *May 24, 2015 19:57*

Looks like loose belts to me. There are tiny access holes that you use a screw driver to tighten the belts. Note: there are two belts on the Y Axis. One on each side.


---
**Imko Beckhoven van** *May 24, 2015 22:58*

Its just one belt.. The one thats moving "up and down" on the photo


---
**Sean Cherven** *May 24, 2015 23:02*

I'd check them all


---
**David Wakely** *May 25, 2015 10:08*

How do i tighten the belts?


---
**Customer Service Sinjoe** *May 26, 2015 13:28*

Loose the belt;check if any damage happened on pitch;adjust the turning speed to make it slower when cutting the corner.I guess belt problem is the main reason,maybe.


---
**Sean Cherven** *May 26, 2015 13:32*

On mine, I noticed the gantry was crooked, and was causing measurement issues when cutting. I had to loosen the belt, straighten out the gantry, and re tighten the belt. That fixed the issue for me.


---
**Imko Beckhoven van** *May 26, 2015 13:48*

with a crooked gantry the start and endpoint is the same.. it looks like its missing steps, to loos or to fast. Might even be to tight in combination with to fast (skipping) 


---
**Chris M** *May 26, 2015 21:03*

David, I note that no one has told you how to tighten (adjust) the belts. For the x-axis, the two adjustment screws (ignore the allen bolts) are on the end of the frame, and can be accessed from the PSU bay. Buggered if I know how to get to the y-axis adjusters, but I haven't really tried yet.


---
**David Wakely** *May 26, 2015 21:21*

Thanks I'll give it a go, since I've aligned my mirrors better it seems to have fixed 99.9% of the problem!


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/KPR2G1s1wc3) &mdash; content and formatting may not be reliable*
