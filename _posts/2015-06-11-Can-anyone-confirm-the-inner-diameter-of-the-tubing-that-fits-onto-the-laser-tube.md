---
layout: post
title: "Can anyone confirm the inner diameter of the tubing that fits onto the laser tube?"
date: June 11, 2015 03:32
category: "Hardware and Laser settings"
author: "Troy Baverstock"
---
Can anyone confirm the inner diameter of the tubing that fits onto the laser tube?



The stuff the comes with it looks to be 6mm, but it seems quite stretched over the glass nipple. Somehow I have ordered 8mm inner which has arrived, but not I'm not sure it will fit. So if anyone could confirm it without me cutting this off and being stuck until new tubing arrives it would be much appreciated.





**"Troy Baverstock"**

---
---
**Sean Cherven** *June 11, 2015 11:04*

I've been wondering the same thing.


---
**Stuart Middleton** *June 11, 2015 18:32*

I've just replaced mine. I think I got 8mm internal. Fits perfectly. I'll check later and post photos. 


---
**Troy Baverstock** *June 12, 2015 03:12*

Cheers, I got 8mm because it fits all the DIY water chilling gear I've bought. Would be good for flow if it fits directly.


---
**Troy Baverstock** *June 24, 2015 00:32*

**+Stuart Middleton** were you able to confirm the inner diameter of the hose you chose?


---
**Stuart Middleton** *June 24, 2015 19:03*

Ah yes, it's 8mm internal diameter. Fits easily but tight and a couple of small zip ties fasten it in place.


---
*Imported from [Google+](https://plus.google.com/+TroyBaverstock/posts/dy5abo5Nyie) &mdash; content and formatting may not be reliable*
