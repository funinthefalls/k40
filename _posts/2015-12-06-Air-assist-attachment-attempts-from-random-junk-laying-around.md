---
layout: post
title: "Air-assist attachment attempts (from random junk laying around)"
date: December 06, 2015 15:38
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Air-assist attachment attempts (from random junk laying around). The ball pumping needle is actually quite awesome to use to focus the airflow & increase the pressure. Bonus of bi-directional airflow, straight down & straight to side.



![images/a4d0aea5260a8104150e57996a3f7a7b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4d0aea5260a8104150e57996a3f7a7b.jpeg)
![images/fca61bb571edac6fe883a063bc9ce5b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fca61bb571edac6fe883a063bc9ce5b8.jpeg)
![images/15bddb0366594d2f0da12ebc3eefa7ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15bddb0366594d2f0da12ebc3eefa7ed.jpeg)
![images/3673023de5fcb33ed52220328cb9b1d0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3673023de5fcb33ed52220328cb9b1d0.jpeg)
![images/3fa5dcdf0e57722d81c5c9c73d685b90.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3fa5dcdf0e57722d81c5c9c73d685b90.jpeg)
![images/fee4e792585bb177e6c5b6a36714b319.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fee4e792585bb177e6c5b6a36714b319.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Mubei65qRWc) &mdash; content and formatting may not be reliable*
