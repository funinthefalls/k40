---
layout: post
title: "Hey! I just got a K40 and am excited to try it out, but I keep getting \"This function needed USB-Key Support.\""
date: March 30, 2018 23:34
category: "Hardware and Laser settings"
author: "Do It"
---
Hey!



I just got a K40 and am excited to try it out, but I keep getting "This function needed USB-Key Support." I have tried different USB cables that connect to the machine, I have entered the mainboard number into the software, but nothing seems to work. Has anyone had a similar problem?



Thanks!

Anders





**"Do It"**

---
---
**Anthony Bolgar** *March 31, 2018 00:12*

That error is asking for the USB Dongle (Gold colored) that allows the software to work. Instead of using the crappy software that comes with it, you should download K40 whisperer. Much better to use, and does not require the dongle.


---
*Imported from [Google+](https://plus.google.com/115818733227475886828/posts/BZQB8t7rkfN) &mdash; content and formatting may not be reliable*
