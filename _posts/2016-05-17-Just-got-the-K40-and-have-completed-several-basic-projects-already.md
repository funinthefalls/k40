---
layout: post
title: "Just got the K40 and have completed several basic projects already"
date: May 17, 2016 14:43
category: "Modification"
author: "Jeremy Hill"
---
Just got the K40 and have completed several basic projects already.  I am thankful that I had some pretty minor issues with it so far.  I did the modification to the air extraction framework inside the box.  I will likely need to cut more out eventually.  I am interested in the Light Object z-table.  I see there is a requirement for a stepper board.  I have seen a lot of great examples of folks who have conducted the z-table mods but have not seen a basic detail of the up/down switch involved.  Several have gone into some amazing details of modifying stepper boards or intricate switches.  I am interested in a simple up/down (perhaps two switches) connection to the stepper.  Also, I assume a separate power supply is required versus the stock power supply in the system.  So my conundrum is trying to find a simple layout and parts list to buy to make the table useful.  I have combed through several forums to try to find this and I must be missing something.  Any help you can provide would be greatly appreciated.  Thank you.  On another note- I am on travel for a few weeks so won't be able to check this site that often but I will try to get back to it as much as possible.  Thanks again!





**"Jeremy Hill"**

---
---
**Don Kleinschnitz Jr.** *May 17, 2016 15:07*

This should get you what you need. I just finished mine.

[http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html](http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)

I used one up/dwn momentary toggle switch, most use two PB switches. You will need a stepper driver and controller if it runs standalone. You will need limit switches and an associated mount if you want to keep from driving the table to its extremes.  The BOM is in the blog. Let me know if you have any questions.


---
**Jim Hatch** *May 17, 2016 15:22*

**+Don Kleinschnitz**​ What do you mean "if it runs standalone"? Does that mean you don't need a separate stepper driver and controller if you wire it into the K40? It looks like you built yours to be able to pull it out of the machine and use it for something else. Like your acrylic control box - when I put my table into my K40 I'm wiring the switch on the top panel so I wouldn't build that box you put on the back of your table.


---
**Jeremy Hill** *May 17, 2016 15:35*

**+Don Kleinschnitz** Looks like a good detail.  It appears I will be in for more than a simple switch, stepper, and laser table 


---
**Don Kleinschnitz Jr.** *May 17, 2016 15:54*

I think this is pretty easy from here. My challenge was getting the controller to work and that is solved.


---
**Don Kleinschnitz Jr.** *May 17, 2016 18:04*

**+Jim Hatch** By stand alone I mean that it is not connected and operates independent of whatever DSP it has. 

I made it modular simply so that I can get it out easy to install a rotary table or just have more room for a big engraving. 

My plan it to integrate it with my smoothie conversion and use a means of measuring the surface to auto focus. In that case the switch on the integrated control panel would seldom be used.


---
**Mishko Mishko** *May 17, 2016 19:36*

If I ever find time to make Zaxis table, I plan to use these two, probably install the generator and a couple of switches in the main panel, should be enough for manual moving the table, but not for auto focusing:



[http://www.ebay.com/itm/2A-phase-3D-Printer-Big-Easy-Driver-board-v1-2-A4988-stepper-motor-driver-board-/221920107052?hash=item33ab781a2c:g:4qUAAOSwhcJWKH9Q](http://www.ebay.com/itm/2A-phase-3D-Printer-Big-Easy-Driver-board-v1-2-A4988-stepper-motor-driver-board-/221920107052?hash=item33ab781a2c:g:4qUAAOSwhcJWKH9Q)



[http://www.ebay.com/itm/331591954111](http://www.ebay.com/itm/331591954111)


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/Z2zcyqCa7M7) &mdash; content and formatting may not be reliable*
