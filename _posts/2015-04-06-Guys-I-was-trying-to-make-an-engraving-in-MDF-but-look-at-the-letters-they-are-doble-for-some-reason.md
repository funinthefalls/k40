---
layout: post
title: "Guys, I was trying to make an engraving in MDF, but look at the letters, they are doble for some reason"
date: April 06, 2015 02:51
category: "Discussion"
author: "Jose A. S"
---
Guys, 



I was trying to make an engraving in MDF, but look at the letters, they are doble for some reason. Could you suggest what parameters I must use?

![images/237f238b802514722f7210a43108d1af.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/237f238b802514722f7210a43108d1af.jpeg)



**"Jose A. S"**

---
---
**Jim Coogan** *April 06, 2015 03:09*

If it went back over it a second time then click on engrave and check the setting to the right.  There is something called ADVANCED PARAMETERS.  Under it you will find some called REPEAT.  It should say 1.  Anything nore and you start to see double, or more :-)


---
**Jose A. S** *April 06, 2015 03:18*

Jajaja...my vision is OK, by the way..glasses are recommended to operate the machine. What settings reco do you recommend for engraving  speed and power?


---
**Jim Coogan** *April 06, 2015 03:56*

That depends on the material.  The more power you use the faster you need to go or you start setting fires :-)  When I do something like MDF I use 6mA, 350mm/s speed, and it can take a while.  The red line, or top power setting this machine is 19mA.  Stay below it or you will wear the tube out before its time.  I keep a notebook for everything I do which shows type of material, engrave or cut, settings, etc.  Comes in really handy.  You can also test scrap material and adjust the power setting as you go to see what will work for that wood and that design.


---
**Jose A. S** *April 06, 2015 04:06*

Thank you for your advise!


---
**Jose A. S** *April 06, 2015 05:23*

Hey!...thank you Peter!


---
**Jon Bruno** *April 27, 2015 15:14*

Peter is right about the ddoouubbllee lines. I had the same problem yesterday and I found that I was out of alignment.

Very helpful indeed.


---
**Lisa Samtani** *May 11, 2015 12:46*

I had this problem with the double lines and was blaming laser alignment for a long time.



This is how I fixed it finally



My belt was slipping. If your belt is not tight enough then it will slip at the end of a cycle and skip a tooth on the gear as it begins its return. Look on the end of your belt rail (right hand side) and find two screws tighten these screws. you can access them via the hole between the electrical and mechanical compartments on your k40. When the belt tightens it will stop this skipping.



Hope this helps


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/RddThGMsYyu) &mdash; content and formatting may not be reliable*
