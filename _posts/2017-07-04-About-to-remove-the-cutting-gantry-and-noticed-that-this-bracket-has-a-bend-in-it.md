---
layout: post
title: "About to remove the cutting gantry and noticed that this bracket has a bend in it"
date: July 04, 2017 17:29
category: "Modification"
author: "William Kearns"
---
About to remove the cutting gantry and noticed that this bracket has a bend in it. Should I take it off and straighten it out. I think it is causing some slack 

![images/0feaec271499e39437eaccb8eec71069.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0feaec271499e39437eaccb8eec71069.jpeg)



**"William Kearns"**

---
---
**Steve Clark** *July 04, 2017 19:27*

Yes I would, as it has to be effecting your focus on that side. 


---
**William Kearns** *July 04, 2017 21:18*

Ahh nice and straight and the roller sits flat and. I slack up or down![images/97c904478771cd1da10fd5a37a2335f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97c904478771cd1da10fd5a37a2335f9.jpeg)


---
**Steve Clark** *July 05, 2017 03:57*

Nice job. The X axis pulley wheel looks tilted inward... I not sure if that is normal, you might want to inspect it. 


---
**William Kearns** *July 05, 2017 04:47*

will remove and inspect and sort that out next. Maybe i reassembled it incorrectly but it looked like it only went one way?


---
**William Kearns** *July 05, 2017 05:30*

looks like i mounted it upside :(

 down on the bracket will swap out Wednesday afternoon. Its K40 reading time now!


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/Hb7QqrN8Gwd) &mdash; content and formatting may not be reliable*
