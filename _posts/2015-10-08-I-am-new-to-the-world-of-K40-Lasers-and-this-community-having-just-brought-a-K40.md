---
layout: post
title: "I am new to the world of K40 Lasers and this community, having just brought a K40"
date: October 08, 2015 08:40
category: "Discussion"
author: "Phil Willis"
---
I am new to the world of K40 Lasers and this community, having just brought a K40. Its unpacked but having seen the cooling pump I thought I would also invest in a proper chiller (a CW300) which is yet to arrive .



Is anyone using a CW3000 here and what coolant would they recommend for operating the K40 in an unheated brick garage in the UK? Not that I intend to run it when its freezing but thats where it will be located.



Thanks in advance.





**"Phil Willis"**

---
---
**David Wakely** *October 08, 2015 09:36*

I use diluted car screen wash as my coolant liquid. It prevents a lot of the algae growth. The ideal temperature is around 15-20 degrees c.


---
*Imported from [Google+](https://plus.google.com/+PhilWillisAtHome/posts/jVGJZ1fFwCj) &mdash; content and formatting may not be reliable*
