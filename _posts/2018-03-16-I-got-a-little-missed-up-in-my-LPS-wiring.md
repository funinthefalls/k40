---
layout: post
title: "I got a little missed up in my LPS wiring"
date: March 16, 2018 18:18
category: "Discussion"
author: "bbowley2"
---
I got a little missed  up in my LPS wiring.



![images/7fa6aea351f1e28434d4a3ead350eea8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7fa6aea351f1e28434d4a3ead350eea8.jpeg)



**"bbowley2"**

---
---
**Phillip Conroy** *March 16, 2018 18:43*

The laser return wire ( the one used for ma meters as well) may be on the back of the pwr supply  ( could be a female banna plug screw thingie)


---
**Don Kleinschnitz Jr.** *March 16, 2018 19:16*

Here are the connections. That pin 4 is the 24V. 



[photos.app.goo.gl - photos.app.goo.gl/cgQfdVfvHrRlKGu72](https://photos.app.goo.gl/cgQfdVfvHrRlKGu72)






---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/Dm9kCrJV9a2) &mdash; content and formatting may not be reliable*
