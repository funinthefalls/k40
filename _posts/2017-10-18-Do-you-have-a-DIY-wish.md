---
layout: post
title: "Do you have a DIY wish?"
date: October 18, 2017 12:14
category: "Hardware and Laser settings"
author: "Jorge Robles"
---
Do you have a DIY wish?



[https://es.aliexpress.com/item/1313-100w-DIY-assembly-Co2-laser-engrave-machine-100w-laser-tube-laser-power-controller-and-all/32688519318.html?shortkey=QfqAFBfE&addresstype=600](https://es.aliexpress.com/item/1313-100w-DIY-assembly-Co2-laser-engrave-machine-100w-laser-tube-laser-power-controller-and-all/32688519318.html?shortkey=QfqAFBfE&addresstype=600)





**"Jorge Robles"**

---
---
**Ariel Yahni (UniKpty)** *October 18, 2017 12:33*

Are you looking to go bigger?


---
**Jorge Robles** *October 18, 2017 12:55*

I'm comfortable... For now :)


---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2017 16:04*

 No shortage of boards to power it? 


---
**Jorge Robles** *October 18, 2017 16:04*

Not yet :))


---
**BEN 3D** *October 18, 2017 17:45*

Kit looks great, but with a matching instructable page and a controler board and software it would even better.


---
**Jason Dorie** *November 06, 2017 22:22*

That’s a Ruida controller - comes with decent software as is. The controller itself is very capable.


---
**Steven Whitecoff** *November 14, 2017 14:06*

After shopping for several months I went with this company and Jason was VERY good to work with, I got a much better price on shipping than shown, he got me parts not listed on the site for less than I could find them anywhere and then he gave me a larger machine for the same price quoted just to be sure I could cut the size I wanted. 


---
**Steven Whitecoff** *November 14, 2017 14:14*

Have fun after you click on the link above, I got a cookie that now ONLY displays the Spanish(?) site for Aliexpress....grrrrr


---
**Jorge Robles** *November 15, 2017 19:57*

Sure you can click on the preferred language flag :P


---
**The Best Stooge** *November 16, 2017 23:26*

**+Jorge Robles** You have to log in to change back to your native language, lol, as it got me too.



Ruida is a damn nice 32 bit controller and is fast, and more responsive with less issues than a Smoothie or even Grbl-lpc.  Too bad it isn't gcode based but with Lightburn, or RDWorks (Thanks to Russ), who cares?




---
**Steven Whitecoff** *November 16, 2017 23:33*

Login in did not change the language but deleting the aliexpress cookie did work


---
**Jason Dorie** *November 16, 2017 23:39*

**+The Best Stooge** - Ruida’s binary format is much more efficient than GCode’s use of text / float numbers, and doesn’t have all the weird baggage that the GCode “standard” brings with it.  It’s GCode-ish under the hood, but more rigid.


---
**The Best Stooge** *November 16, 2017 23:43*

**+Jason Dorie** Exactly so who cares?  I am used to tweaking the nipples of Gcode but that isn't, or shouldn't, be the case with a Ruida.


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/MFaWebumBA9) &mdash; content and formatting may not be reliable*
