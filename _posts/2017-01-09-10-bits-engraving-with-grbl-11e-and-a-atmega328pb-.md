---
layout: post
title: "10 bits engraving with grbl 1.1e and a atmega328pb "
date: January 09, 2017 11:43
category: "Modification"
author: "Paul de Groot"
---
10 bits engraving with grbl 1.1e and a atmega328pb 😊

![images/d1ca98552c501f0e5bfee0ebe8bf2135.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1ca98552c501f0e5bfee0ebe8bf2135.jpeg)



**"Paul de Groot"**

---
---
**Thomis Bastelstube** *January 11, 2017 11:13*

looks good!! which hardware do you use? 




---
**Paul de Groot** *January 11, 2017 20:02*

**+Thomas Sebastian** i use an arduino with the extended atmega328pb which has 2 more 16bits timers and 1 additional uart. On rop of this my own stepper  shield with 32 micro steps stepsticks 


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/fzRL12tEdqS) &mdash; content and formatting may not be reliable*
