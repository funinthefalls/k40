---
layout: post
title: "Anyone else seen this issue? Even with a very short program, LONG after the laser has completed cutting (or engraving) my computer continues to pop up a black information box stating it is \"Sending Cutting information to"
date: February 21, 2018 01:19
category: "Original software and hardware issues"
author: "timb12957"
---
Anyone else seen this issue? Even with a very short program, LONG after the laser has completed cutting (or engraving) my computer continues to pop up a black information box stating it is "Sending Cutting information to laser". It appears briefly then disappears. Each time it shows the percentage finished. I have to wait until this stops to begin another cut. I am using the stock software with CorelDraw 12.





**"timb12957"**

---
---
**HalfNormal** *February 21, 2018 01:43*

Unfortunately this is normal behavior for the program. You should not have to wait for it to stop to start again. If you do right click the icon and click stop.


---
**James Rivera** *February 21, 2018 06:12*

It is checking in with the Chinese government. 😉


---
**timb12957** *February 23, 2018 02:43*

Love the humor James! Half, right click does nothing for me :(


---
**HalfNormal** *February 23, 2018 02:45*

Pinkish icon bottom right of computer screen. 


---
**HalfNormal** *February 23, 2018 03:48*

Yep


---
**timb12957** *February 23, 2018 03:49*

I should have read more careful. I thought I was supposed to right click the black pop up box. Got it now!


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/bLmurXHH5pS) &mdash; content and formatting may not be reliable*
