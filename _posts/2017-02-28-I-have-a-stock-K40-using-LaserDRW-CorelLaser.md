---
layout: post
title: "I have a stock K40 using LaserDRW/CorelLaser"
date: February 28, 2017 07:20
category: "Original software and hardware issues"
author: "Roel Wijmans"
---
I have a stock K40 using LaserDRW/CorelLaser. I had my belt snap and had to replace it. The only one on quick notice I could find was a 6mm GT2 instead of the 5mm GT2 that was on the machine. It seemed to fit fine and worked fine until this week. Now I'm getting ghosting/double engraving of letters. After doing a test I get the following; The top lines were engraved in one direction only and the bottom lines were engraved unidirectional. Any idea how I can fix this problem?

![images/54abd69bb51e1500ad779352e3527f07.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/54abd69bb51e1500ad779352e3527f07.jpeg)



**"Roel Wijmans"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 28, 2017 07:39*

It's interesting to note the near uniformity of the offsets. Judging by this pic, I'm assuming your belt that snapped was the X-axis belt?



My only thought is whether the belt tension is too tight/not tight enough in one direction for the X-axis belt?


---
**Roel Wijmans** *February 28, 2017 07:44*

correct, its the X belt. I'll try and see what happens when I tighten or loosen only one side of the belt. The tension is pretty even on both sides at the moment


---
*Imported from [Google+](https://plus.google.com/107510639890676348125/posts/bKEJvwXkL63) &mdash; content and formatting may not be reliable*
