---
layout: post
title: "Looking to add some LED lights and computer fans"
date: January 12, 2016 14:15
category: "Modification"
author: "Jc Connell"
---
Looking to add some LED lights and computer fans. Where do you guys get 12v power from a stock machine?





**"Jc Connell"**

---
---
**Jc Connell** *January 12, 2016 15:33*

These are the LEDs I have and I'm considering using: [http://www.amazon.com/gp/product/B00BG4T6EU?psc=1&redirect=true&ref_=oh_aui_search_detailpage](http://www.amazon.com/gp/product/B00BG4T6EU?psc=1&redirect=true&ref_=oh_aui_search_detailpage). I also have one of these remaining: [http://www.amazon.com/gp/product/B00C1YI2XA?psc=1&redirect=true&ref_=oh_aui_search_detailpage](http://www.amazon.com/gp/product/B00C1YI2XA?psc=1&redirect=true&ref_=oh_aui_search_detailpage). I think they both have the drivers already, is that correct? Either way, where would I connect these wires?


---
**Stephane Buisson** *January 12, 2016 17:04*

stock is 24V (motors) and 5 v (board), but barely any spare amp available. Consider to add one.


---
**Coherent** *January 12, 2016 22:58*

If you have an old 12 volt DC power supply (wall wart type from a discarded charger or laptop or something).  if you don't have one, ask a friend, they are pretty common.  Even better, an old computer power supply would work well and already have the fan connectors. The small strip or two of leds will draw very little. Just hard wire the DC power supply to the main power inside the unit... add a switch or two for lights and fans if you want and you're good to go.


---
**Jc Connell** *January 12, 2016 23:27*

Thank you. This is a great thought. I have a few power supplies laying around. I plan to upgrade to the LightObject DSP kit eventually. I see that it includes power supplies. Could I use one of them when I upgrade?


---
*Imported from [Google+](https://plus.google.com/+JcConnell/posts/gFkJvZuHFF9) &mdash; content and formatting may not be reliable*
