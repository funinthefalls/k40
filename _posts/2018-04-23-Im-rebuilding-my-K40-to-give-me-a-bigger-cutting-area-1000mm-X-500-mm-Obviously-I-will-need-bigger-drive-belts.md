---
layout: post
title: "Im rebuilding my K40 to give me a bigger cutting area 1000mm X 500 mm Obviously I will need bigger drive belts ."
date: April 23, 2018 14:55
category: "Modification"
author: "Don Recardo"
---
Im rebuilding my K40 to give me a bigger cutting area 1000mm X 500 mm

Obviously I will need bigger drive belts . Does the K40 use MXL belt,  GT2 belt, GT2.5 or some other pitch ?





**"Don Recardo"**

---
---
**Frederik Deryckere** *April 25, 2018 15:36*

don't know what it is but it ain't GT2. I say grind off the pulleys and stick with GT2 which is readily available.

That's what I did in my conversion


---
**HP Persson** *April 25, 2018 17:47*

MXL, 2.032" pitch

Anything else with original controller will give you wrong sizes.




---
**Duncan Caine** *April 28, 2018 12:41*

Light Object say the pitch is 2.00mm, as it happens, I've just been down this road, needing longer belts.  The diameter of the pullies is 12.72mm and there are 20 teeth, so the pitch is 1.998mm (0.0788"), close enough to 2mm as makes no difference.  In the end I bought from Light Object, just to be on the safe side.  If you do decide to use the GT2 belt do report back and let us know the result.


---
**Don Recardo** *May 03, 2018 07:44*

Thanks all for the comments . infact when I dissasembled the old machine I found  the old belt is marked by the manufacturer as an MXL belt . New case now made, Mechanical bits assembled, new 60Watt tube bought .Will post a pic when its finished


---
*Imported from [Google+](https://plus.google.com/106290033771296978518/posts/EF7mTzcy8Nx) &mdash; content and formatting may not be reliable*
