---
layout: post
title: "CNC Control Boards for Sale I'm currently cleaning up and getting rid of all the excess CNC parts and pieces, that I have purchased and accumulated over the years"
date: October 03, 2018 16:52
category: "Material suppliers"
author: "Rustin Guerin"
---
CNC Control Boards for Sale



I'm currently cleaning up and getting rid of all the excess CNC parts and pieces, that I have purchased and accumulated over the years. I have the following controller boards that are all either brand new or lightly used. Let me know if interested price includes shipping to US via USPS. I can send more pics if need be.

rgueri1@gmail.com





- Smoothieboard 5XC V1.0b - used but working fine $110

- xPRO-V2 - new in package $75

- xPRO-V3 - new in package $100

- PMDX-424 - (for Mach4) used but working fine $180







![images/ee1e75bd2d9473a94e98dfbf971b88b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee1e75bd2d9473a94e98dfbf971b88b7.jpeg)
![images/12245fbf63272b39001992f4715d3e53.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/12245fbf63272b39001992f4715d3e53.jpeg)
![images/fbef8e412b12f7c4ebeb8212565afa28.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fbef8e412b12f7c4ebeb8212565afa28.jpeg)
![images/29c770f1c67341e59ccc40eaeab90875.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/29c770f1c67341e59ccc40eaeab90875.jpeg)

**"Rustin Guerin"**

---
---
**salvatore sasakingsoft** *October 03, 2018 20:05*

ciao mi sai dire che cinghia monta la laser k40 ?


---
**Rustin Guerin** *October 03, 2018 20:38*

I believe any of the first three will work. I don’t have a K40, so I’m not 100% sure. They are all three open source and run GRBL or similar. 


---
**salvatore sasakingsoft** *October 03, 2018 20:39*

io  parlo delle cinghie  




---
**James Rivera** *October 03, 2018 21:53*

**+salvatore sasakingsoft** I think something is getting lost in translation. Maybe this will answer your question?



[donsthings.blogspot.com - K40 LPS Configuration and Wiring](http://donsthings.blogspot.com/2017/01/k40-lps-configuration-and-wiring.html)


---
**Rustin Guerin** *October 04, 2018 02:27*

I’m not sure what the “straps” are?




---
*Imported from [Google+](https://plus.google.com/112489114642705697365/posts/F3WYozGiqcL) &mdash; content and formatting may not be reliable*
