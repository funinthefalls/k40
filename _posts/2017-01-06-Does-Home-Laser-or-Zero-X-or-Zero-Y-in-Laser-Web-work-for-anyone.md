---
layout: post
title: "Does \"Home Laser\" or \"Zero X\" or \"Zero Y\" in Laser Web work for anyone?"
date: January 06, 2017 02:52
category: "Smoothieboard Modification"
author: "Chris Sader"
---
Does "Home Laser" or "Zero X" or "Zero Y" in Laser Web work for anyone? 





**"Chris Sader"**

---
---
**Wolfmanjm** *January 06, 2017 06:40*

smoothie in grbl mode uses $H to home not G28.1




---
**Chris Sader** *January 06, 2017 23:01*

hmmm. putting $H in the settings didn't work for me. Clicking Home Laser doesn't do anything.


---
**Wolfmanjm** *January 06, 2017 23:40*

what version of smoothie are you running?


---
*Imported from [Google+](https://plus.google.com/107747838010116449754/posts/LqyouNtaaAb) &mdash; content and formatting may not be reliable*
