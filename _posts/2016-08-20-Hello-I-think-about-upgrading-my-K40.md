---
layout: post
title: "Hello, I think about upgrading my K40"
date: August 20, 2016 17:56
category: "Modification"
author: "Mikkel Steffensen"
---
Hello,



I think about upgrading my K40. 



But what is best:

-Smoothie board

-DSP x7 from lightobject





**"Mikkel Steffensen"**

---
---
**Eric Flynn** *August 20, 2016 19:11*

The DSP is the best choice, but more $$.  Honestly, the stock controller and software are pretty darn decent once you get your machine set up, and learn the software.  If I wanted to spend the money, I would definitely go with the DSP over the Smoothie.  I personally hate the smoothie workflow, and will be sticking with the stock controller for now just because of it.  Smoothie is not all its chocked up to be IMHO.


---
**Jim Hatch** *August 20, 2016 20:09*

At the end of the day it's less about the controller and more about your software & workflow. Having to transfer between multiple software packages to go from design (say in AI or Inkscape) to CorelLaser or LaserDRW is prone to fiddly errors. Corel X8 is good if you're a Corel user and thinking about upgrading.



Wherever you can cut steps out or go straight from design to laser and with as few jobs/layers as possible, it's a good thing.



The most irritating thing about the stock k40 is that I can't specify cut/engrave, speed & power by line color like I can with my other laser. So check your software options with each hardware option.


---
**HP Persson** *August 20, 2016 21:53*

Start with WHY.

Identify the problem you have, and go for the option solving it best.

If you just want to upgrade and money is no issue - DSP

If you like to tinker and look for solutions - Smoothie.



I also planned to upgrade, bought a smoothie but i got stuck in WHY, i´m just doing cutting and stock board gives me all i need :)

So i´ll save it until it breaks down.


---
**Don Kleinschnitz Jr.** *August 21, 2016 12:42*

I looked at length at both options. At the end of the day it's what protocol the machine uses to cut and engrave that's most important to me.

 I got tired of converting from one format to the other and having to constantly tweak things to get them to work and be the right size.

 I also wanted something where I knew what was inside and had English forms of communications so that when something didn't work I could figure it out or get help. 



I decided I needed the machine to run on Gcode. That lead me to the smoothie as the controller and firmware. Laserweb was my choice for the main user interface from my pc and software driver.

All of this is open source and you can get help from some impressive developers and makers right here in this forum. 



A side benefit is that once I finish my conversion I will be able knowlegeable enough to build any z,y,z device with the same setup incl. Laser, CNC, 3d printer etc.

 I will also be able to use any cad tool that outputs Gcode. I currently use SketchUp and inkscape. 

I can interface to my CNC devices via SD card, Ethernet or USB enabling me to put my machine almost anywhere.

Lastly, when I have a problem with a part I can look at the Gcode to see what the controller is being told to do.




---
**Jim Hatch** *August 21, 2016 14:08*

+1 Don


---
*Imported from [Google+](https://plus.google.com/117825343986845004843/posts/MVrpue5miX9) &mdash; content and formatting may not be reliable*
