---
layout: post
title: "i feel like i'm sooooooo close.. Hello K40 people.."
date: July 31, 2016 10:53
category: "Smoothieboard Modification"
author: "patrick brown"
---
i feel like i'm sooooooo close.. 



Hello K40 people.. 



i'm starting to think that i've missed something here.. and so would be super great full if someone can give me a pointer... 



ive got a K40... (but you knew that.. )



and i think it's a fantastic bit of kit...  but like all good makers /modders . tinkerers the original work flow with the moshi software was a real pain.. 



now i spend all my day in solidworks.. and there is real nice 2.5d plug in called HSM works.. which can create gcode and this is what i us for my little cnc milling machine... it works like a charm... (i say that but i could be editing my memory as i don't use if very often... )



so i decided a little while ago to change the cnc board.. and make something that would work with HSM...  and also i would like to be able to control it like i do with my 3d printer.. (sorry ... i just cant stop collecting gadgets) which is via octoprint.. (which is a stunning product.. )



anyway... 



so progress to date.. 



got a smoothie board, which looks like this.. 



and a laser power supply that looks like this... 



with a test fire button.. and a power level pot



now i've been through forums / read pages of articles on getting the beam to be controlled by the board.. but can i get it too work.. 



so i want to solve this once and for all.. 



in my mind i need two things.. one the details of what wires go where.. and two the config.. 



and then a method of testing it.. 



now i have a test method.. as i can use the web interface on the smoothie board and issue a M3 to turn the beam on.. and M5 to turn it off.. so i'm just after the other bits..  keep it simple as i'm an idiot.. but if i get this too work.. i'll create a guide and make sure its available.. as i cant believe that i'm the only person struggling with this.. 



thanks



Pb... 













![images/dde7517fa6b0c999c834b9a6b607ecea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dde7517fa6b0c999c834b9a6b607ecea.jpeg)
![images/8de804304bea1a7f2d5737f8d67c2e2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8de804304bea1a7f2d5737f8d67c2e2b.jpeg)

**"patrick brown"**

---
---
**patrick brown** *August 05, 2016 21:47*

Hi guys .... Ok Friday is funday here in my world.... So time to have a play.....now I noticed that the firmware I was running was the edge (I had to work out that the GitHub shows a whole list of different options and now I've found the master one) 

But after doing the firmware.bin thing with the master firmware I now get nothing on the screen.... And no blinky lights.... What's the deal with flipping firmware's .... Should be that easy.... 

 Or do I need to redo something more trixy.... 😊



Pb 


---
**patrick brown** *August 05, 2016 22:00*

It seemed to do something and it renamed the firmware file (like it does) and being the fumble fingers I am I clearly tried it a few times, but no joy, which made me wonder if as I went for the very latest edge firmware to the master which is a couple of months old if that's the issue... 😬


---
**patrick brown** *August 06, 2016 09:14*

Aahhhhhhh i have found a 362Kb bin file of rhwe master firmware.. (the first one i downloaded was 56Kb.. ) and now i'm on the master build.. so we are getting there.... 



what would be super handy is getting hold of some test gcode files... anybody got any?



thanks



Pb 


---
**patrick brown** *August 06, 2016 12:11*

ok ok ... what a learning curve this is... 



ok i've got the laserweb running.. and i can export a gcode file... all good... 



the next thing is how do i get it to cut it.. 



am i being daft and missing something.. 




---
**patrick brown** *August 06, 2016 12:21*

got ya... i did look.. but clearly not hard enough ...  



the performance via Ethernet seems a bit slow... i suspect that a direct usb is the prefered connection method.. ??? 


---
**patrick brown** *August 12, 2016 22:21*

So I've been playing with the setup a bit more... And a couple of things struck me, laser web runs nicely on a raspberry pi  so that's nice.... I've not managed to get the rastering working, I think I'm still missing a wire connection....

I've been playing with laser web and I can't find out if I can control a dxf layer, it would be nice to take output from 123d and go directly into laserweb, at the moment I need to go through deleting layers in draft sight... Is there a better way... The wiki talks about using three svg files... But it would be nice to just control the layers...  Also how did I manage to break homing, it used to work, now it's stopped.... Odd...



Pb 


---
*Imported from [Google+](https://plus.google.com/118426473257139962706/posts/aHpFvmDnFdB) &mdash; content and formatting may not be reliable*
