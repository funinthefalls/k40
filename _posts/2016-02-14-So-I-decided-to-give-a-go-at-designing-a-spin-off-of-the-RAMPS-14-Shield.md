---
layout: post
title: "So, I decided to give a go at designing a spin-off of the RAMPS 1.4 Shield"
date: February 14, 2016 21:18
category: "Discussion"
author: "Sean Cherven"
---
So, I decided to give a go at designing a spin-off of the RAMPS 1.4 Shield.



I have modified it specifically for the K40, including the addition of the K40 Connectors, meaning no more cutting / splicing wires, and no more K40 Middleman Boards!



Take a look at the schematics, and tell me what ya'll think!

Is there anything you would like to see added?



Note: this is still a work-in-progress. I just want to get your opinions is all.



**+Peter van der Walt**, do you see any compatibility problems with LaserWeb?

![images/306563b68123e1bf370948d3b0f13dfa.png](https://gitlab.com/funinthefalls/k40/raw/master/images/306563b68123e1bf370948d3b0f13dfa.png)



**"Sean Cherven"**

---
---
**Sean Cherven** *February 15, 2016 03:58*

I understand that, but I want something to play work until smoothiebrainz is complete. Do you have an ETA on when it'll be complete?


---
**Sean Cherven** *February 15, 2016 04:09*

How much are you planning on selling them for? And is it a shield or a complete controller? Do you have it on github? If so, what's the link?


---
**Sean Cherven** *February 15, 2016 04:14*

What's the project name? You have a ton of projects lol


---
**Richard Vowles** *February 15, 2016 06:28*

You have a Patreon for that?


---
**Sean Cherven** *February 15, 2016 13:20*

Oooh, so if I donate $169 I will get one shipped? Is there a deadline? I won't have the money for a month or two.


---
**Sean Cherven** *February 15, 2016 13:37*

**+Peter van der Walt**  I just took a look at the schematics and board files in Eagle, and I don't see any sockets for the Ribbon Cable, or any of the stock connectors on the K40???


---
**Stephane Buisson** *February 15, 2016 13:59*

**+Sean Cherven** Smothiebrainz isn't only for K40 (CNC,...), the daughter board will make it device dependant. Hardware ressource dedicated and limited for that function will reduce cost. (motor drivers, end stop, ...). you can expect middleman board function (connectors) to be on smoothiebrainz daughter board.


---
**Sean Cherven** *February 15, 2016 14:12*

Oh, I see... Are there any documents or info on how this will attach to/work with the K40?


---
**Stephane Buisson** *February 15, 2016 14:39*

too early, prototype main board is just out and **+Peter van der Walt**  just received them.


---
**Thor Johnson** *February 16, 2016 21:55*

What does a smoothie get you that an Ardiuno doesn't?

I understand the smoothie is faster, but aren't both almost "stupid fast" compared to using steppers to control XY axes (now, if you were using galvos...)?


---
**Stephane Buisson** *February 16, 2016 23:27*

**+Thor Johnson** speed and acceleration.

Acceleration is important, distributed energy isn't constant, when laser head speed down the amount of power should also follow proportionally. that a lot calculation for a poor Arduino clock.


---
**Thor Johnson** *February 16, 2016 23:38*

Oh... Ok.  Now I see some light (snicker).  Especially since the laser axes are running much faster than a 3D printers (photons are easier to steer than plastic), the real goal is to maintain a constant speed and if you cannot, adjust the laser power to match what you're actually doing so you don't overburn square corners.



Does  the smoothie support everything (like the grayscale photos) that the Arduinos do at the moment, or are there some gaps?


---
**The Technology Channel** *February 25, 2016 13:50*

Is the serial and Ethernet protocol define anywhere for the smoothie board?


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/MQdN96RSTrf) &mdash; content and formatting may not be reliable*
