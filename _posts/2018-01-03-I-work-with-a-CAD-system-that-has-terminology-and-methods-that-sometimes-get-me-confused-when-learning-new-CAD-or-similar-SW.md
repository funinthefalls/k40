---
layout: post
title: "I work with a CAD system that has terminology and methods that sometimes get me confused when learning new CAD or similar SW"
date: January 03, 2018 02:02
category: "Software"
author: "Steve Clark"
---
I work with a CAD system that has terminology and methods that sometimes get me confused when learning new CAD  or similar SW.



I downloaded LightBurn today and watched the video’s. But somewhere along the line I have missed a “how to operation” for what I call trimming or extending of a line. It seems the term may have changed because I cannot find how to do it.



In the system I have  I can “trim” a line to an intersection  of two lines, a point mark on the line, an arc or a datum point on a line.



I can extend a line or arc by picking the end and clicking on a place or by using a few other methods of defining that point.



Can someone give me the method of doing this in LB? I’m really looking forward to trying out LightBurn!







**"Steve Clark"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2018 02:21*

There is a Lightburn group on Fb and also G+. I recommend the fb one, more activity there, and asking the dev there. 


---
**Carl Fisher** *January 03, 2018 03:26*

I think you would rely on snapping one line to a point on another. I don't believe there is a trim functionality available at this time, at least that I have found during testing and documentation.




---
**LightBurn Software** *January 05, 2018 09:07*

We check in here, too. There is currently no means to do what you describe. Features for node editing will improve over time - we’re big fans of Vectric software and the features they provide for node editing are the bar we’re striving for.


---
**Steve Clark** *January 08, 2018 00:28*

Thanks’ for the feedback! I look forward to that addition when it occurs. People will be amazed how fast they can generate 2D vector drawings with it.

I can only imagine how much effort and thought you have put into this already!


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/6fWGTaTDyvz) &mdash; content and formatting may not be reliable*
