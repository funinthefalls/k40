---
layout: post
title: "I used my k40 1st time yesterday using laserdrw no problems"
date: July 29, 2016 20:03
category: "Software"
author: "Wayne Keller"
---
I used my k40 1st time yesterday using laserdrw  no problems.

Today I used same file I was using yesterday, when I go to the engrave option, or engrave icon it is greyed out..  Any suggestions?





**"Wayne Keller"**

---
---
**Jim Hatch** *July 29, 2016 21:59*

Is the USB key plugged in and did LaserDrw say it was verified?


---
**Nick Civitillo** *March 18, 2017 18:16*

I am having the very same issue.  I just got my machine yesterday too and it is acting up.  We must have got the same batch...

USB key and cable are connected and verified.  I am able to "reset" to home position, but I cannot cut/engrave.  If I reinstall Laserdrw I am able to cut a single shape, but if I draw anything else, the engrave option is greyed out.


---
*Imported from [Google+](https://plus.google.com/116685428300997367954/posts/WwjrmMnTGR3) &mdash; content and formatting may not be reliable*
