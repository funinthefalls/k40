---
layout: post
title: "Just opened the box should there be a disk? K40"
date: October 30, 2016 18:25
category: "Original software and hardware issues"
author: "Larry Baird"
---
Just opened the box should there be a disk?  K40





**"Larry Baird"**

---
---
**Ariel Yahni (UniKpty)** *October 30, 2016 18:38*

Should, could, maybe, are you lucky?  


---
**Ariel Yahni (UniKpty)** *October 30, 2016 18:40*

Find here link with Corellaser and Corel draw if you did not get it [https://plus.google.com/114544098138525257938/posts/VKpfwi9KvgQ](https://plus.google.com/114544098138525257938/posts/VKpfwi9KvgQ)


---
**Jim Hatch** *October 30, 2016 19:08*

Check inside the front & the back where the laser tube is. They suffer all sorts of things in the machine (where it can bounce around the gantry, head & mirrors 😜) instead of putting a separate pocket in the foam for the accessories.


---
**HalfNormal** *October 30, 2016 19:51*

Sometimes they include a bag that has some things stuffed into it.


---
**Larry Baird** *October 31, 2016 01:33*

 Thank you all, I am sure I will need more help and Jim you were correct.  I found it in with the blower motor.


---
*Imported from [Google+](https://plus.google.com/102480913963084823552/posts/iWfDG2PLDRS) &mdash; content and formatting may not be reliable*
