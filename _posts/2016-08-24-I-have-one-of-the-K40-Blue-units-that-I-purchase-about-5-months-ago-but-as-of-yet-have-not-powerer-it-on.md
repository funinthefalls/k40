---
layout: post
title: "I have one of the K40 Blue units that I purchase about 5 months ago, but as of yet, have not powerer it on"
date: August 24, 2016 00:27
category: "Smoothieboard Modification"
author: "Dan Stuettgen"
---
I have one of the K40 Blue units that I purchase about 5 months ago, but as of yet, have not powerer it on.  Primarialy because I have been trying to learn corel draw and moshi draw, but find them hard to figure out.  I want to use the Laser cutter to cut and etch 1/16 , 1/32 and 1/8  thick burch plywood and veneers for my Railroad model building in G scale.  I went to a maker Community open house to see if I could find someone that could help me learn to use this machine.  After taking to them they suggested I upgrade the controll system with either Arduino based controller or the more expensive Smoothie board.  Well I am not a big fan of Arduino boards  , so I opted to get a Smoothie 4x board.  I dont need a 4 axis system as I already have a Di Vinci 3D printer.  The only other axis I feel I would need for the Laser cutter is the Z-axis, should I decide to strip out the small holding frame and put in a larger Z-table.  Some mods I have already done are adding an E-MER switch and the lid cutoff switch.  One thing I would  like to do is add a LCD display to show amps and such and have the ability to jog and zero the laser head.  Has anyone added this type of display to thier K-40 using the Smoothie board?  Any Help would be greatly appericated.    I will also be adding a Air blower to the laser head and possible be adding a better cooling system than the bucket of water and pump.  I had thought about getting one of those Igloo electric coolers and putting the distilled water in it and then have the pump circulate the water through the electric cooler to keep the water cold.  IS this a good Idea or will I crack the laser tube?   Dan in Cypress Texas





**"Dan Stuettgen"**

---
---
**Jonathan Davis (Leo Lion)** *August 24, 2016 00:51*

Hmm


---
**Phillip Conroy** *August 24, 2016 05:00*

Forget stock software and only learn corel ,i have a 1 minute guild in one of my posts,just click on my name and read every thing i have posted,the guild is on the right hand side ,will try and add link


---
**Phillip Conroy** *August 24, 2016 05:15*

Link to one minute file creation for laser cutters



[https://plus.google.com/u/0/113759618124062050643/posts/1YW6LuJWNYQ](https://plus.google.com/u/0/113759618124062050643/posts/1YW6LuJWNYQ)


---
*Imported from [Google+](https://plus.google.com/116093665495871650574/posts/azgyyigsDjg) &mdash; content and formatting may not be reliable*
