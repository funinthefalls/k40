---
layout: post
title: "I've just bought my K40, and I'm working on upgrading it with air-assist"
date: April 01, 2016 09:01
category: "Modification"
author: "Akos Balog"
---
I've just bought my K40, and I'm working on upgrading it with air-assist. (I've ordered the new head from lightobject)

My question is, what airpump/compressor should I use?

Is the pressure important, or only the air-flow?

Is the air-assist only helps cleaning the lens clear, or should it also help the cutting in some way? (cooling the surface maybe)



I want to cut nylon webbing. I've read in a webpage, that they are using 25w co2 + 10 psi air assist, for similar jobs ([http://www.synrad.com/search_apps/application_briefs/118-3.htm](http://www.synrad.com/search_apps/application_briefs/118-3.htm))



Do I really need 10 psi?



I'd like a low-cost solution, so I've found 3 options, they are at the same price:

- Hailea ACO-328 air compressor (made for aquariums/ponds) - low pressure (maybe 5-6 psi if I'm correct), high airflow - 70 liters/min

- small airbrush compressor with or without air tank (small - 3 liters) - max 60 psi (with regulator, so I can go down to 10 psi), but maximum airflow is 23 liters/min

-Or should I go for some "real" compressor, with bigger tank (20l), bigger max pressure (110 psi, so it must be regulated). It would be big and noisy....





**"Akos Balog"**

---
---
**Thor Johnson** *April 01, 2016 14:15*

If you get a big compressor, you'll need to get a filter-drier, or you'll find that water will condense in the line and spray on your optics (bad).



A dedicated compressor (no tank), located close to the machine won't have this problem because air won't drop in temperature between the compressor and the cutter.

I started out with a real compressor (and needle valve to make 20m/s through my air-assist nozzle); that velocity is roughly 42 liters/min.  The pressure required to make it through the tubing is going to be ~4 PSI (I calculated as 5m of 1/8" tube and it was 2psi, but I'll say 4 to not worry about it).  So I'm thinking large aquarium pump.


---
**Thor Johnson** *April 01, 2016 14:19*

It does help the surface too -- if you're running at low speeds, cutting Birch ply (I cut 40W power 5mm/s, 1 pass), if you don't run air-assist (or it's too small), you'll have a flame right under your optics.  20m/s of air blows it out for a nice, clean cut (still gotta use tape).   My air assist has a 1/4" hole in the tip (I printed mine), so all the calcs are based on that.

Cutting Nylon is probably even better-- nylon tends to melt according to many other pages ([https://www.pololu.com/docs/0J24/3](https://www.pololu.com/docs/0J24/3)).


---
**Akos Balog** *April 02, 2016 17:18*

Thanks, however finally I've bought the "big" compressor. I'll need to buy a water separator, and I hope it will work as I think ;)


---
*Imported from [Google+](https://plus.google.com/106984452297752036237/posts/Q5YZqAUJZk1) &mdash; content and formatting may not be reliable*
