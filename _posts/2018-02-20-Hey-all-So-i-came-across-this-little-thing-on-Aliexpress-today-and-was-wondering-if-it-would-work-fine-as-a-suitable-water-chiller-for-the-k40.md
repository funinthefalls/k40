---
layout: post
title: "Hey all. So i came across this little thing on Aliexpress today and was wondering if it would work fine as a suitable water chiller for the k40?"
date: February 20, 2018 07:22
category: "Modification"
author: "sam cv"
---
Hey all. So i came across this little thing on Aliexpress today and was wondering if it would work fine as a suitable water chiller for the k40? 



Any thoughts, tips or knowledge from experience with such a product would be appreciated. Thank you :) 



[https://www.aliexpress.com/item/100W-110-240v-Aquarium-Water-Chiller-Warmer-Cooler-Temperature-Conditioner-For-Fish-Tank-Coral-Reef-Shrimp/32851802230.html?spm=2114.search0104.3.18.2289347cQ680vH&ws_ab_test=searchweb0_0,searchweb201602_2_10152_10151_10065_10344_10068_10342_10343_10340_10341_10084_10083_10618_10630_10307_5722316_5711211_10313_10059_10534_100031_10629_10103_10626_10625_10624_10623_10622_10621_10620_10142,searchweb201603_38,ppcSwitch_3&algo_expid=d336891f-ffba-456f-87fa-3e8e7fffaf8e-2&algo_pvid=d336891f-ffba-456f-87fa-3e8e7fffaf8e&priceBeautifyAB=0](https://www.aliexpress.com/item/100W-110-240v-Aquarium-Water-Chiller-Warmer-Cooler-Temperature-Conditioner-For-Fish-Tank-Coral-Reef-Shrimp/32851802230.html?spm=2114.search0104.3.18.2289347cQ680vH&ws_ab_test=searchweb0_0,searchweb201602_2_10152_10151_10065_10344_10068_10342_10343_10340_10341_10084_10083_10618_10630_10307_5722316_5711211_10313_10059_10534_100031_10629_10103_10626_10625_10624_10623_10622_10621_10620_10142,searchweb201603_38,ppcSwitch_3&algo_expid=d336891f-ffba-456f-87fa-3e8e7fffaf8e-2&algo_pvid=d336891f-ffba-456f-87fa-3e8e7fffaf8e&priceBeautifyAB=0)





**"sam cv"**

---
---
**Phillip Conroy** *February 20, 2018 07:54*

Do not waste your money


---
**Phillip Conroy** *February 20, 2018 07:54*

Way under powered at 100 watts


---
**sam cv** *February 20, 2018 08:13*

what should i be looking at, power and flow rate - wise ?  ( know about the CW 3000 and 5200, but i mean if one was to look at DIY options that are suitable for the k40)

 


---
**sam cv** *February 20, 2018 08:15*

**+Phillip Conroy**  I meant as in what should i be looking at as in is there an off-the-shelf alternative chiller that won't break the bank and work like a cw 3000, but at a faction of the price or just cheaper - instead of making one from scratch?


---
**bbowley2** *February 27, 2018 18:36*

I bought a used chiller from a hydroponics store.  CW 5000 for $200.00.  I've seen them on Craig's list also.  They are around.


---
**sam cv** *March 01, 2018 16:08*

**+bbowley2**  wow, that was quite a score you got there !




---
**bbowley2** *March 01, 2018 20:31*

They are out there.  I saw one for $250 on Craigs list not long ago.




---
*Imported from [Google+](https://plus.google.com/101915993108014864632/posts/5dHEzZzgMjm) &mdash; content and formatting may not be reliable*
