---
layout: post
title: "I purchased the ACR Full kit from all-tek systems about a month ago.( )"
date: November 17, 2016 20:33
category: "Smoothieboard Modification"
author: "Carle Bounds"
---
I purchased the ACR Full kit from all-tek systems about a month ago.([http://www.all-teksystems.com/product-page/162fea05-fb48-56a7-8ec1-38a6200b7fe3](http://www.all-teksystems.com/product-page/162fea05-fb48-56a7-8ec1-38a6200b7fe3)). I can tell you that Scott Marshall makes a great product! Super nice guy... great support...he helped me do some rewiring over the phone to get my board powered. I had purchased one of the newer K40 machines with the upgraded control board so the power supply was a little different than he was use to.



I am new to all this so please bear with me. When I plug the motor connector into the "Y motor" on the board (See photo) as stated in the directions the motors acted strange. From within laserweb3 when I jogged I could only click the X axis button and the movement was diagonal instead of left and right. So yesterday I plugged the motor connector into the "X motor" connector and now the motors work fine!



The only problem I have now if the laser will not fire in laserweb. When I press the test fire button on the K40 the laser fires. When I run a job in laserweb I see the head move around correctly buy no laser fire.



There are the settings the directions say to put into laserweb. I wonder if moving the connector to X Motor caused an issue:

Gcode Setup Page



Start G-Code 

G28



Laser ON Command

M3



Laser OFF Command

M5



PWM Max S Value

255



Homing Sequence

G28



End G-Code

M5

G28



Travel Moves (mm/s)

30 



Anyone have any ideas until I can get ahold of Scott?









![images/73488b3d2c47f566088f29ef1d8b7dbb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/73488b3d2c47f566088f29ef1d8b7dbb.jpeg)
![images/feeada2067241cc171a686f11bff0a17.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/feeada2067241cc171a686f11bff0a17.jpeg)

**"Carle Bounds"**

---
---
**Ariel Yahni (UniKpty)** *November 17, 2016 21:32*

**+Carle Bounds**​ your gcode is wrong


---
**Ariel Yahni (UniKpty)** *November 17, 2016 21:32*

You are using smoothie so first PWM Max Value  should be 1


---
**Ariel Yahni (UniKpty)** *November 17, 2016 21:33*

Do you have a analog POT installed? 


---
**Ariel Yahni (UniKpty)** *November 17, 2016 21:34*

Also if you are starting LaserWeb as node server-smoothie then you don't need laser On and Laser Off but for reference in smoothie is G1 (on)  G0 (off).  Unless **+Scott Marshall**​ has some other magic in his ACR


---
**Carle Bounds** *November 17, 2016 21:38*

I had read that PWM Max Value should be set to 1 with Smoothieboards today. Might be a mistake in the documentation.... I will try it when I get home. Would PWM set wrong stop the laser from firing?


---
**Don Kleinschnitz Jr.** *November 17, 2016 21:40*

How is your PWM connected to the LPS?


---
**Carle Bounds** *November 17, 2016 21:42*

I'm really not sure how it's connected. This was purchased as a plug and play solution you just drop in your k40. 


---
**Don Kleinschnitz Jr.** *November 17, 2016 21:47*

Have you ever gotten the laser to fire from the controller (not the test sw)?




---
**Carle Bounds** *November 17, 2016 21:47*

Hopefully setting the PWM Max value to 1 will allow the laser to fire in Laserweb. I will try it when I get home.


---
**Ariel Yahni (UniKpty)** *November 17, 2016 21:48*

**+Carle Bounds**​ it should not make a difference but surely setting the wrong On would prevent it


---
**Alex Krause** *November 17, 2016 21:57*

Please post a copy of your config file on google drive so we can check a few things out


---
**Ariel Yahni (UniKpty)** *November 17, 2016 22:02*

**+Alex Krause**​ his film should be the same we looked the other day as its standard on the ACR


---
**Carle Bounds** *November 17, 2016 22:07*

No I have never got the laser to for from the controller+Don Kleinschnitz. The laser does fire from the test laser button on the front panel so at least I know the laser works.


---
**Carle Bounds** *November 17, 2016 22:09*

I can get the config file when I get home from work. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 17, 2016 22:41*

**+Scott Marshall** Tagging you here so you can see if there is anything you know of that will be causing **+Carle Bounds**' issue.



**+Carle Bounds** A thought, since you mention the issue is that when you click X jog buttons it moves diagonally & when you click Y jog it does nothing (?) I wonder is your ribbon cable plugged in the correct way? I believe the ribbon controls the X-axis stepper whilst the plug controls the Y-axis stepper (if I recall correct).


---
**Carle Bounds** *November 17, 2016 23:01*

If pwm set to 1 does not work I can try changing the orientation of the ribbon cable. Can changing the direction of the ribbon cable burn anything up? :\


---
**Alex Krause** *November 17, 2016 23:37*

I'm curious if you have the laser module enabled 


---
**Carle Bounds** *November 18, 2016 00:39*

Here is my config I pulled from the SD card on the smoothieboard:

[https://drive.google.com/file/d/0B5PZCS2MZHcQc0o4UTNiVk9MRms/view?usp=sharing](https://drive.google.com/file/d/0B5PZCS2MZHcQc0o4UTNiVk9MRms/view?usp=sharing)

[drive.google.com - Config - Google Drive](https://drive.google.com/file/d/0B5PZCS2MZHcQc0o4UTNiVk9MRms/view?usp=sharing)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 18, 2016 01:08*

**+Carle Bounds** I can't guarantee changing ribbon cable direction won't kill something, so might be best to wait for someone more knowledgeable in that sort of thing to weigh in on it.


---
**Carle Bounds** *November 18, 2016 01:36*

Well changing the laserweb settings to pwm 1 instead of 256 for smoothieboard did not resolve issue.  Turning the ribbon cable around didnt work either. I tried moving the motor connector back to the Y input but the motors acted strange again and it tried to home in the lower left hand corner. Put the connector back in X and it works fine. When I run jobs it moves correctly and I can see it trying to create the correct shapes but laser is not coming on still. Hmmmmmm


---
**Alex Krause** *November 18, 2016 01:38*

Did you try the laser on off commands as G0 and G1


---
**Carle Bounds** *November 18, 2016 01:39*

I created a macro button in laserweb from an article I found to create a test fire button in laserweb3. The code I used is:

G1 Z1 F600 S0.1



is that correct? It doesn't fire with the macro


---
**Carle Bounds** *November 18, 2016 01:44*

Yes I set the laser off to g0 and laser on to g1. Also in my post above you see I tried a macro to make it fire with g1


---
**Alex Krause** *November 18, 2016 01:48*

I'll double-check your config file when I get home


---
**Carle Bounds** *November 18, 2016 01:48*

Man thanks so much!


---
**Alex Krause** *November 18, 2016 02:47*

looked the config over it looks good assuming you are connected to the pin in the middle of the smoothie board for the laser. did your machine come with the ribbon cable connected to the m2nano board, If it did then the ribbon cable is for the Xaxis motor and the end stops and the Y axis motor will use the four pin cable with the white connector. could you post a picture of your entire wiring setup? so we can see where the wires are running from the board to the PSU


---
**Mike Mauter** *November 18, 2016 03:34*

Scott usually puts either a 2 or 3 amp automotive type fuse on his boards. If I remember correctly x and y will move but laser will not fire if that fuse blows. Test fire will still work.


---
**Carle Bounds** *November 18, 2016 03:40*

![images/b7c2a694ee0a558f8b5c15d3ec3cd4f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7c2a694ee0a558f8b5c15d3ec3cd4f1.jpeg)


---
**Carle Bounds** *November 18, 2016 03:41*

![images/8d1420f70c2ae23a318d6c09fdcae282.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d1420f70c2ae23a318d6c09fdcae282.jpeg)


---
**Carle Bounds** *November 18, 2016 03:44*

The two blue wires you see not attached goto that red toggle switch on the front. Scott said that was used to turn the old k40 board of and on and was not needed as the black switch next to the emergency stop button powered the whole k40. He said I could use the red toggle for something else later. Just wanted to say why those wires are not attached.


---
**Carle Bounds** *November 18, 2016 03:56*

I pulled out the automotive fuse and visually inspected it. The wire inside looks intact and it does not appear blown. Thanks though!


---
**Alex Krause** *November 18, 2016 04:02*

![images/c50ad4e197aa801667b7c8b034166366.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c50ad4e197aa801667b7c8b034166366.jpeg)


---
**Alex Krause** *November 18, 2016 04:05*

I'm pretty sure that's the wire you need to connect to the laser fire connection on the board.... let's have **+Don Kleinschnitz**​ verify, he has done extensive research on the psu's


---
**Carle Bounds** *November 18, 2016 04:33*

Here are where those two blue wires traced back to the power switch toggle.

![images/4df86780394ccd82f18b7dce922f7948.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4df86780394ccd82f18b7dce922f7948.jpeg)


---
**Carle Bounds** *November 18, 2016 04:34*

But this switch actually powers the k40 on...

![images/a73bfaf2b0a356af6f6d53ee36bc4697.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a73bfaf2b0a356af6f6d53ee36bc4697.jpeg)


---
**Carle Bounds** *November 18, 2016 04:37*

Closer PSU photo

![images/9e8a0160f9fdee6afec102ac1199b23f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e8a0160f9fdee6afec102ac1199b23f.jpeg)


---
**Carle Bounds** *November 18, 2016 04:44*

And here is a photo of what the PSU looked like before it was rewired for the acr.

![images/0999371a2ad9abd07c7813b2a621dc43.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0999371a2ad9abd07c7813b2a621dc43.png)


---
**Don Kleinschnitz Jr.** *November 18, 2016 04:47*

**+Alex Krause** **+Carle Bounds** 

It looks to me like the PWM from smoothie is not connected to the LPS. Its hard to tell from the picture.

There should be wires from the smoothie to the LPS somewhere, likely the "L" pin on the rightmost LPS connector. The one that **+Alex Krause** suggests is open.

I am not familiar with Scott's setup but I would guess that something (PWM) is supposed to be wired to the "L" pin which is the rightmost pin on the rightmost LPS connector.

From the picture on his site I see that there is a yellow and green wire coming from the pins that are the typical PWM pins to Scotts board. I do not know where they are supposed to exit his board and connect to the power supply however.


---
**Don Kleinschnitz Jr.** *November 18, 2016 04:59*

This is all my best guess.....



It looks like there should be a pair of wires from the smoothie to the "Fire" terminals on Scotts board. They are the green and yellow wires from the upper middle of the smoothie.

[https://static.wixstatic.com/media/b1107d_1184c05d2fa74d4ab411084270753642~mv2_d_2798_1688_s_2.jpg/v1/fill/w_862,h_520,q_85,usm_0.66_1.00_0.01/b1107d_1184c05d2fa74d4ab411084270753642~mv2_d_2798_1688_s_2.jpg](https://static.wixstatic.com/media/b1107d_1184c05d2fa74d4ab411084270753642~mv2_d_2798_1688_s_2.jpg/v1/fill/w_862,h_520,q_85,usm_0.66_1.00_0.01/b1107d_1184c05d2fa74d4ab411084270753642~mv2_d_2798_1688_s_2.jpg)



Then I think there should be a cable from the "LO" plug on Scotts board to the "L" pin on the LPS.

This picture shows both the "Fire" terminal on the right and the "LO" socket on the left.

[https://static.wixstatic.com/media/b1107d_5e335191e8a245bc8450fdcad1bba358~mv2_d_2220_2000_s_2.jpg/v1/fill/w_577,h_520,q_85,usm_0.66_1.00_0.01/b1107d_5e335191e8a245bc8450fdcad1bba358~mv2_d_2220_2000_s_2.jpg](https://static.wixstatic.com/media/b1107d_5e335191e8a245bc8450fdcad1bba358~mv2_d_2220_2000_s_2.jpg/v1/fill/w_577,h_520,q_85,usm_0.66_1.00_0.01/b1107d_5e335191e8a245bc8450fdcad1bba358~mv2_d_2220_2000_s_2.jpg)



[static.wixstatic.com - static.wixstatic.com/media/b1107d_1184c05d2fa74d4ab411084270753642~mv2_d_2798_1688_s_2.jpg/v1/fill/w_862,h_520,q_85,usm_0.66_1.00_0.01/b1107d_1184c05d2fa74d4ab411084270753642~mv2_d_2798_1688_s_2.jpg](https://static.wixstatic.com/media/b1107d_1184c05d2fa74d4ab411084270753642~mv2_d_2798_1688_s_2.jpg/v1/fill/w_862,h_520,q_85,usm_0.66_1.00_0.01/b1107d_1184c05d2fa74d4ab411084270753642~mv2_d_2798_1688_s_2.jpg)


---
**Carle Bounds** *November 18, 2016 16:03*

There was probably a wiring mistake made for sure. Keep in mind Scott had not worked with this power supply before. He was also telling me what wire to put where over the phone and could not see what was happening. A lot of room for error for sure. Here is a screenshot from Scotts ACR manual as well as a picture of how my ACR is wired right now. Can someone look at this and verify that the black wire from the ACR should go into position 2 on the PSU?

![images/80501dee90ccc05913505bb26f972bbe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/80501dee90ccc05913505bb26f972bbe.jpeg)


---
**Carle Bounds** *November 18, 2016 16:04*

Here is what the ACR manual states

![images/edf00e7cb8b8b3140ef1d1c55fab2bc5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/edf00e7cb8b8b3140ef1d1c55fab2bc5.jpeg)


---
**Steve Prior** *November 18, 2016 16:28*

I just got my ACR from Scott running this week.  What I found was that it was miswired - if you follow the wires from the M1 output of the Smoothieboard they were plugged into the Y inputs of the ACR board, and X was plugged into M2.  My K40 didn't have the flat cable, so I was using the individual connectors to plug the stepper motors in to.  I know the ACR is meant to be a plug and play solution, but it appears some wiring accidents are happening.  I would recommend checking with a continuity meter each connection from the Smoothieboard to the ACR to make sure it goes where you think it should - the ACR connections are labeled so this is easy to do.  I wonder if you're using the flat cable and the connections are reversed, so you've basically got two drivers driving the same connector - that would kill things pretty fast if true, so check ASAP.  The X and Y motion wasn't what I expected at first, the X moved backwards and Y seemed strange.  I reversed the X motor by swapping the polarity of one of the coils on the input to the ACR board - I liked the idea of doing it there instead of a software config change.  The Y axis takes getting used to - the homing switches are at X=0, Y=MAX, so Y- is towards the front of the machine, so when you're getting started this is confusing because when you do a manual jog in the Y- direction it moves forward, but then when you kick off a home it moves in the other direction - this is actually correct.


---
**Carle Bounds** *November 18, 2016 16:34*

Aside from the laser not firing....the X and Y homing and movement is great as long as I plug the motor cable into the X connection on the ACR and not the Y connection.


---
**Carle Bounds** *November 18, 2016 16:38*

I'm just wondering if the black wire from the ACR simply goes into position 2 instead of 1 on the far right connector on the PSU to resolve the laser issue as in my photo above.


---
**Carle Bounds** *November 18, 2016 16:39*

**+Steve Prior** Thanks!




---
**Steve Prior** *November 18, 2016 16:39*

I figured I'd fix it once early on and then all the labels on the board would be correct and easier to figure out in the future.  I'm not sure if having the X connected to the M2 output of the Smoothieboard would cause other weirdness I hadn't gotten to yet.


---
**Don Kleinschnitz Jr.** *November 18, 2016 17:07*

**+Carle Bounds** 

that would connect the ACR output to 5VDC so I would not do that!



The LPS DC connector is as such:



Starting from the right going left:

L

5VDC

Gnd

24VDC



I don't know what the black wire from the ACR is?


---
**Carle Bounds** *November 18, 2016 17:09*

OK thanks Don. I won't do that...


---
**Don Kleinschnitz Jr.** *November 18, 2016 17:16*

I just saw the instructions that you posted. The pin descriptions are from left to right and suggests:



Black to pin 2 that is grnd

Red to pin 4 that is "L" 

 So if you are wired like this you are correct.



What is in the config file for your PWM?


---
**Carle Bounds** *November 18, 2016 17:33*

**+Don Kleinschnitz** look 16hrs ago on this thread. I posted a link to my config


---
**Don Kleinschnitz Jr.** *November 18, 2016 17:47*

**+Carle Bounds** I am pretty lost at this point cause I am not certain how you have the PWM wired now.

It seems to me that when the laser was on all the time the PWM polarity was backward. Did you try changing it in the config file.



The config up above has the PWM inverted but that may be correct for use with a level shifter however not for the schema I sent you.



**+Steve Prior** how is your smoothie to ACR and then ACR to LPS wired and what is the PWM set to in the config.




---
**Carle Bounds** *November 18, 2016 18:12*

I have not edited the config file. FYi...Also this is the full acr plug and play board with an additional power supply to take some load off the k40 PSU.


---
**Carle Bounds** *November 18, 2016 18:41*

**+Don Kleinschnitz**  take a look at this picture please. The black wire from the ACR is going to pin 2 which is what you were thinking should be right but.... the red cable is spliced to another cable going up into the K40 cable wrap and not into pin 4......... This could be the mistake right?

![images/e7d8a1f40827137bcf2f8745e9054627.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e7d8a1f40827137bcf2f8745e9054627.jpeg)


---
**Steve Prior** *November 18, 2016 18:50*

**+Don Kleinschnitz** I have not changed the config at all from what Scott provides.  My laser is the simpler one with the analog power knob, and I've got a "white connector" power supply.  My wiring is exactly what's in the instructions.  The only things I have changed were the wires between the Smoothieboard and ACR boards - I corrected the swap of X and Y so now M1 is connected to the labeled X inputs on the ACR.  I also swapped one coil on the X input of the ACR to correct the X direction of travel.  Firing the laser works, though I don't know for sure yet PWM is working (I think it is) since I'm just getting familiar with LaserWeb3.


---
**Carle Bounds** *November 18, 2016 19:44*

**+Steve Prior**​ so your red wire from the acr is going to L pin 4 on the PSU correct? And the black wire from the PSU to pin 2 gnd of the PSU?


---
**Don Kleinschnitz Jr.** *November 18, 2016 22:03*

**+Carle Bounds** in regard to the black and red wire I think the answer is yes but I do not know where the other end of the red wire goes (or I don't remember). Nor the black. 

I cant see any of the connections on the ACR.

Can we used the name of the connections on the ACR as well as the wire colors to identify stuff.

Perhaps multiple pictures taken perpendicular to the controller.


---
**Carle Bounds** *November 18, 2016 22:13*

**+Don Kleinschnitz** the other end of the red and black wire are already connected to the acr. Scott shipped the acr with the black and red wires connected on the acr


---
**Carle Bounds** *November 18, 2016 22:13*

**+Don Kleinschnitz** I will send a picture of what I mean she. I get home....


---
**Carle Bounds** *November 18, 2016 22:45*

Here are pics showing the red and black wires from the acr. It was shipped like that so I'm pretty sure its right.

![images/0360d7b688ff14e0be1bfd598ddd15f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0360d7b688ff14e0be1bfd598ddd15f6.jpeg)


---
**Carle Bounds** *November 18, 2016 22:45*

![images/43e5ab4a59f4e41fd623044f76321b98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43e5ab4a59f4e41fd623044f76321b98.jpeg)


---
**Carle Bounds** *November 18, 2016 22:51*

Scott had me connect the red wire to a black wire that is going into the wire wrap. That's what the electrical tape is for. Might have been a mistake as we did it over the phone.... Now I have to try to trace that black wire after I disconnect it from the red wire that goes to L0 on the PSU. Not sure where it goes.

![images/b4b01a748c6a252ec67d0a15df950497.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b4b01a748c6a252ec67d0a15df950497.jpeg)


---
**Don Kleinschnitz Jr.** *November 18, 2016 23:04*

**+Carle Bounds** on these pictures I need to see the labels on the ACR connectors. Sorry to be a pain. 


---
**Carle Bounds** *November 18, 2016 23:47*

**+Don Kleinschnitz** no problem! I may have to tell you what some of the markings say because they are no real clear and won't show in a photo. 


---
**Don Kleinschnitz Jr.** *November 19, 2016 13:09*

**+Carle Bounds** WOW! This morning I realized that I have been confusing two post threads with similar problems. 

This one and [https://plus.google.com/u/0/114125781899580900020/posts/abPmZDokSjA](https://plus.google.com/u/0/114125781899580900020/posts/abPmZDokSjA)

In some case I crossed answers between these posts.



So if I have sounded a little nuts that's why! 

.....

A little bit of a reset:

I am basing my comments on thinking that the smoothie connects from a header to ACR-fire using a green and yellow wire on one side of the ACR, through an on board level shifter and then out to "LO" on the other side of the ACR.

Is this correct?

....................................

The ACR-LO should go to the LPS-"L". 

.......Your ACR-LO is a Red wire and is spliced to LPS-l-Black [pin 4].

The ACR-gnd should go to LPS-gnd 

........Your ACR-gnd black connects to LPS-gnd-black [pin 2].



Assuming that the smoohie to ACR came wired right the wiring from ACR to LPS should be:

ACR-gnd connected to LPS-gnd pin 2

ACR-LO connected to LPS-L pin 4



Note: all these pin numbers are from right to left on the LPS rightmost connector.



[plus.google.com - Ok I am getting close. I have the X and Y working correctly and the laser…](https://plus.google.com/u/0/114125781899580900020/posts/abPmZDokSjA)


---
**Carle Bounds** *November 19, 2016 16:33*

**+Don Kleinschnitz** I'm going to get the acr rewired to the 2 and 4 pins today. Now I am just going to have to trace that black wire going up into the wire wrap and see what it goes to. The black wire was spliced to the acr-lo red wire which was wrong. I'm not sure what that black wire attached to. Hopefully you can tell mW where to connect it. Thanks you have been such a great help. 


---
**Carle Bounds** *November 19, 2016 21:38*

**+Don Kleinschnitz** OK I have traced the black cable that Scott and I spliced to the red cable coming from the acr. It is going to that red power toggle switch on the front of the control board that Scott said I didn't really need. The red toggle switch apparently allowed you to turn the old stock control board off and on independent of main power.... Scott could not think of any reason I needed to ever use that switch. Maybe in the future I could possible use that switch to power a laser diode or something.... Anyways. Where should I attach this loose black wire?

![images/aa8be1923b3869686ff21b9d3456068d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa8be1923b3869686ff21b9d3456068d.jpeg)


---
**Carle Bounds** *November 19, 2016 21:38*

![images/5c3df83a2c1acc21f87022b303264e8e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c3df83a2c1acc21f87022b303264e8e.jpeg)


---
**Carle Bounds** *November 19, 2016 21:54*

**+Don Kleinschnitz** OK the laser is coming on now!!!!! I think that black wire goes to ground but I know the power switch button is not plugged in and has no power so I tried it.... Awesome!


---
**Don Kleinschnitz Jr.** *November 19, 2016 22:52*

**+Carle Bounds** so the PWM is working??

I am guessing he routed PWm through that switch to shut off anything from the controller.


---
**Carle Bounds** *November 20, 2016 00:03*

I just opened a little .svg file he gave me on the CD into laserweb and it cut it out the cardboard. A simple vector cut. To be honest I don't know how to test pwm yet..... I'm really new to this.


---
*Imported from [Google+](https://plus.google.com/106012823225343259431/posts/N1gpsp74317) &mdash; content and formatting may not be reliable*
