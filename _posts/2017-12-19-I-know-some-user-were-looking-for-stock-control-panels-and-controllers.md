---
layout: post
title: "I know some user were looking for stock control panels and controllers"
date: December 19, 2017 13:22
category: "Material suppliers"
author: "Don Kleinschnitz Jr."
---
#K40StockParts



I know some user were looking for stock control panels and controllers.



First time I have seen the digital display for sale.



[https://www.aliexpress.com/item/CO2-Laser-Rubber-Stamp-Engraving-K40-LIHUIYU-M-Mother-Main-Board-Control-Panel-Dongle-B-System/32667821671.html?spm=2114.10010108.100009.3.6578dccdf3oejm&traffic_analysisId=recommend_2037_null_null_null&scm=1007.13482.91320.0&pvid=9f9428d8-2b08-42dc-960e-e33b1661357a&tpp=1](https://www.aliexpress.com/item/CO2-Laser-Rubber-Stamp-Engraving-K40-LIHUIYU-M-Mother-Main-Board-Control-Panel-Dongle-B-System/32667821671.html?spm=2114.10010108.100009.3.6578dccdf3oejm&traffic_analysisId=recommend_2037_null_null_null&scm=1007.13482.91320.0&pvid=9f9428d8-2b08-42dc-960e-e33b1661357a&tpp=1)





**"Don Kleinschnitz Jr."**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2017 16:21*

Once again I maintain I'm in the wrong line of work. 


---
**Steve Clark** *December 19, 2017 16:39*

I disagree Ray, your product is far and away a better choice!


---
**HP Persson** *December 19, 2017 19:14*

Wow, what a price, and this is the older model of the digital panel too, i wonder what the new one is priced at :P


---
**Steve Clark** *December 20, 2017 01:57*

I keep meaning to put the old board on ebay... it could help pay for the new upcoming Lightburn... 


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2017 01:59*

I should start offering a $10 refund off your C3D when you ship me your old M2Nano... 


---
**Steve Clark** *December 20, 2017 03:51*

**+Ray Kholodovsky**  LOL - Sure, then sell then on the bay for $30!


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/1uGWMKmuXrM) &mdash; content and formatting may not be reliable*
