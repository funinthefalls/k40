---
layout: post
title: "Interesting piece on designing a living hinge"
date: April 14, 2017 00:29
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Interesting piece on designing a living hinge. Has anyone tried it with Acrylic?



<b>Originally shared by Make:</b>



Looking for a way to make bends and springs in a single piece of rigid material?





**"Ariel Yahni (UniKpty)"**

---
---
**Ashley M. Kirchner [Norym]** *April 14, 2017 03:14*

Yes. It's not as forgiving as wood. Breaks too easy. Wood on the other hand, no issues at all.


---
**Steve Clark** *April 14, 2017 03:29*

Try using polycarbonate  a little more difficult to cut but very flexible.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/1c6HrpJut2B) &mdash; content and formatting may not be reliable*
