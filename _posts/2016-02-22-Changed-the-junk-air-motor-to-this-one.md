---
layout: post
title: "Changed the junk air motor to this one"
date: February 22, 2016 22:21
category: "Modification"
author: "HP Persson"
---
Changed the junk air motor to this one. When testing them side by side this new one seems a bit less powerful.

But hey, it was only $15 and if it doesnt do the job, i just change it for something else :)

Was curious to get a neat solution not taking alot of space behind the laser :)



What do you think, yay or nay? :P

![images/a2c2889f2d411dc42a92dacf9d1f9681.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a2c2889f2d411dc42a92dacf9d1f9681.jpeg)



**"HP Persson"**

---
---
**3D Laser** *February 22, 2016 22:31*

What's the link to it


---
**HP Persson** *February 22, 2016 22:39*

It´s in Swedish, i translated the data below ;)



Airflow: 107 m³/h // 63 CFM

Power: 14 W

Noise: 36 dB(A)

Speed: 2300 rpm



Almost as a regular computer fan, didnt notice that until now :)

Maybe i´ll do a test with stacked 120mm fans too


---
**ThantiK** *February 23, 2016 00:57*

The problem is that it's not a squirrel-cage fan.  It cannot generate a high amount of static pressure.  It might be able to do 63 CFM with absolutely no obstruction, but it's got so low static pressure that it can't overcome obstacles/turns/restrictions.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 23, 2016 01:01*

I've read regarding stacking fans (as I thought similar to use multiple 120mm fans) doesn't really increase the output a great deal (definitely not double). I've thought it may be possible to funnel multiple side-by-side into the exhaust tubing.


---
**Phillip Conroy** *February 23, 2016 05:58*

My fanhttp://[www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT) hardly need air assist now it is that good not cheap but will last years,ps with stock extraction fan cutting mdf 3 hours a day my laser tube is covered in mdf dust,


---
**HP Persson** *February 23, 2016 08:37*

Tried the tube-fan i showed pic of above, it "worked" but it didnt remove the smoke quick enough for my liking.

Was thinking about making positive pressure, by having fans in the front blowing in, and fans in the back taking it out, to get a better flow trough the box, without the need for monster fans.



Your ideas and tips are highly appreciated :)


---
**William Steele** *February 23, 2016 10:19*

The fan should always be the last thing in the hose... So it pulls a vacuum on the laser and doesn't have to push air through the hose as well... That way it will never push smoke into the interior of the building.


---
**HP Persson** *February 23, 2016 10:22*

**+William Steele** Good thought, what about having the fan included with the laser at the back, and the new one i bought at the end of the hose? Maybe a bad idea, was just thinking if you got any experience on that :)

Thanks


---
**William Steele** *February 23, 2016 14:26*

I don't recommend a fan on the back of the laser... if there is any imbalance, then you'll wind up blowing smoke into the room.


---
**I Laser** *February 23, 2016 22:23*

**+Phillip Conroy** how loud is that thing?


---
**Phillip Conroy** *February 24, 2016 10:33*

Very quit,it is 2 meters out side and can just hear it running


---
**Phillip Conroy** *February 24, 2016 10:38*

I just added air assist [i had a air hose next to laser head blowing air across the work however bugger to aim and did not keep fumes from dirting focal lens] laser head from ebay cost 50 dollars bugger to align mirrors again ,and found out my laser head was out of square causing laser beam to hit side of air assist and not come out the end ,took 4 hours to align mirrors ans an hour to align laser head so that all is good now


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/R2PBuAnDibk) &mdash; content and formatting may not be reliable*
