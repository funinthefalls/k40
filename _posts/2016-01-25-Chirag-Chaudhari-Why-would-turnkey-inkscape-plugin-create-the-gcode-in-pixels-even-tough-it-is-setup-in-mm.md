---
layout: post
title: "Chirag Chaudhari Why would turnkey inkscape plugin create the gcode in pixels even tough it is setup in mm"
date: January 25, 2016 00:08
category: "Discussion"
author: "Pedro Flores"
---
**+Chirag Chaudhari** Why would turnkey inkscape plugin create the gcode in pixels even tough it is setup in mm. 



![images/bc3e3b09a345f719f630950467fb0803.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bc3e3b09a345f719f630950467fb0803.jpeg)
![images/9a79232f52f5f34728e2f08efec0c3f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a79232f52f5f34728e2f08efec0c3f0.jpeg)
![images/0c7d8f67ce20d02405fba9c7bce0a0e8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c7d8f67ce20d02405fba9c7bce0a0e8.jpeg)
![images/d899954310b6ea8e718e89c208010fc1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d899954310b6ea8e718e89c208010fc1.jpeg)

**"Pedro Flores"**

---
---
**Ariel Yahni (UniKpty)** *January 25, 2016 01:14*

**+Pedro Flores**​ have you tried LaserWeb? 


---
**Pedro Flores** *January 25, 2016 01:16*

Yeah but it is all erratic. Creating spider webs even if using SD card. 


---
**Pedro Flores** *January 25, 2016 01:39*

[https://plus.google.com/116137110819103096995/posts/f2b63nZC8rQ](https://plus.google.com/116137110819103096995/posts/f2b63nZC8rQ)


---
*Imported from [Google+](https://plus.google.com/116137110819103096995/posts/8thV9CgCAWc) &mdash; content and formatting may not be reliable*
