---
layout: post
title: "Cool. Will do soon"
date: March 12, 2016 05:12
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
Cool. Will do soon





[http://www.instructables.com/id/Super-Solder-Inlay-for-Your-Woodworking-Projects](http://www.instructables.com/id/Super-Solder-Inlay-for-Your-Woodworking-Projects)

![images/65a7190efe32b0ca0af46be9c0a6742d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65a7190efe32b0ca0af46be9c0a6742d.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Phil Willis** *March 12, 2016 09:16*

I would think using lead free pewter would make a better job, since it pours and melts at a lower temperature.


---
**Phil Willis** *March 13, 2016 12:31*

**+Nathan Walkner** + copper + antimony ([https://en.wikipedia.org/wiki/Pewter](https://en.wikipedia.org/wiki/Pewter))


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/A981FxC2WZ4) &mdash; content and formatting may not be reliable*
