---
layout: post
title: "I have not used my K40 for over a month due to hand injury and now I have some algae in the water lines building up, what can I put in the water to prevent the algae ?"
date: December 13, 2016 18:35
category: "Discussion"
author: "Robert Selvey"
---
I have not used my K40 for over a month due to hand injury and now I have some algae in the water lines building up, what can I put in the water to prevent the algae ? 





**"Robert Selvey"**

---
---
**Cesar Tolentino** *December 13, 2016 18:49*

I don't know if this is good but I put a few drops of Clorox in mine.  Been using this technique since 2013.  Change water every year


---
**Alex Krause** *December 13, 2016 19:10*

You can drain the tank partially and fill with a few gallons of distilled white vinegar. This will kill all the algea if you let the pump run for a day ... then you flush the tank and fill with tap water run pump for a bit dump reservior then fill again with tap water repeat running the pump followed. Finally dump the tank fill will with distilled water and add a few drops of algeacide found in the aquarium section at Walmart and if you find you have hard to remove air bubbles in your tube add a drop maybe two of dawn dish soap and stir the tank well. This will break the surface tension of the water 


---
**Don Kleinschnitz Jr.** *December 13, 2016 19:28*

What some recommend: 1 part RV antifreeze to 3 parts distilled water (kills algae, keeps water from freezing). I am pretty sure regular antifreeze is too conductive and can cause internal HV arcs.


---
**greg greene** *December 13, 2016 20:56*

a few ounces of rubbing alcohol keeps mine clean




---
**Madyn3D CNC, LLC** *December 14, 2016 02:07*

Keep your water in a light proof enclosure. Algae cannot grow well in the dark. I also use a very small amt of bleach, 30:1 +/- ratio about 


---
**Kirk Yarina** *December 14, 2016 16:46*

Cheap blue car windshield washer fluid works well in mine. 


---
**Don Kleinschnitz Jr.** *December 14, 2016 17:55*

All, FYI I have found at least one LPS failure that I worked with someone on where we  became reasonably convinced that the conductivity of the cooling fluid matters. 

In this case it was regular car antifreeze and this machine was under production loads. 

Apparently if the fluid is too conductive it promotes arching to the coolant across the epoxy seal at the output mirror and reduces the power output and damages the supply. This seems to be a long term effect .... 

Why the seal did not leak and kill the tube when this condition occurred is beyond me. Just saying......


---
**Madyn3D CNC, LLC** *December 16, 2016 19:04*

Antifreeze does have a high EC, makes a lot a sense. Don, you mention a problem with arcing through the epoxy seal which I had never thought of before, but now after a little google-fu I think that just may have been what happened to one of my first tubes when I got the laser a while back.



 I'm sure the Chinese are using the "just enough to make it work" method of production with these lasers so it makes sense that a little jump in the EC of coolant would promote arcing through that seal. 



Although I couldn't find any specefic numbers for the EC of antifreeze (I'm sure it ranges a LOT) I did find this doc where Chevron is promoting one of their antifreeze products for specialized industrial application. It's interesting to note, on page 10 they list one of the many advantages of this particular antifreeze as having "reduced electrical conductivity"....   [http://www.michiganpetroleum.com/images/application_category_documents/30.pdf](http://www.michiganpetroleum.com/images/application_category_documents/30.pdf)


---
**Don Kleinschnitz Jr.** *December 16, 2016 19:30*

**+Madyn3D CNC, LLC** some data on conductivity in this table: 

[http://www.dow.com/ethyleneglycol/about/properties.htm](http://www.dow.com/ethyleneglycol/about/properties.htm)


---
**Robert Selvey** *December 18, 2016 02:43*

Can you run the laser with the distilled white vinegar in the water or you have to run it for a day and then change the water out ?


---
**Alex Krause** *December 18, 2016 03:05*

Run it for several hours then flush and replace with distilled water


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/PVvuKhhJLK3) &mdash; content and formatting may not be reliable*
