---
layout: post
title: "I have been asked to make some napkin rings from plywood and engrave names on each one so considering in investing in a k40 laser cutter for this job and other wood projects before I purchase just wanted to ask if this"
date: October 03, 2017 11:27
category: "Discussion"
author: "Donna Cook"
---
I have been asked to make some napkin rings from plywood and engrave names on each one so considering in investing in a k40 laser cutter for this job and other wood projects before I purchase just wanted to ask if this machine would be ok and how long does the laser tube last thanks donna





**"Donna Cook"**

---
---
**Don Kleinschnitz Jr.** *October 03, 2017 14:49*

What machine?


---
**HalfNormal** *October 03, 2017 15:32*

Lots of variables to consider. A stock K40 will need some upgrades to make it more than a simple hobby machine. Tube life is dependent on how much power the tube is driven at. More power, less tube life. How thick do the napkin rings need to be? There is a limit on how thick the K40 can cut and that depends on material too. How quickly do you need to have the job done? Yes, you can do napkin rings on a K40 but not out of the box.


---
**Donna Cook** *October 04, 2017 08:13*

Thanks for your reply I was going to use 3mm ply and need to make 350 for Xmas day and they need to be engraved is this do able?


---
**Donna Cook** *October 04, 2017 08:27*

Can you recommend a good home laser cutter?


---
**HalfNormal** *October 04, 2017 12:17*

You are looking at a starting price around $5000.00 for an out of box system. FSL lasers is a good place to start.


---
**HalfNormal** *October 04, 2017 12:18*

The K40 can do what you want but it will not out of box.


---
**Nate Caine** *October 07, 2017 14:57*

I'm confused about the design.  Do you have even a sketch of what your napkin ring would look like?  And where you'd engrave the names?



Purchasing wooden napkin rings and engraving names or logos is one thought.  



Cutting rings from 3mm ply with the laser would likely leave some burnt or charred edges that would transfer the soot to the linen napkins (either when inserted or removed).  So the wood would likely need to be finished/sealed.



The diameter of a typical napkin ring is pretty small, so the outside "arc" is probably not flat enough to engrave directly.  So you'd likely need a rotary attachment unit for your laser cutter.  So that's another accessory and cost to consider.


---
**Nate Caine** *October 07, 2017 15:12*

![images/d2e6885a288ea8e115c689ebd04b0efa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d2e6885a288ea8e115c689ebd04b0efa.jpeg)


---
**Nate Caine** *October 07, 2017 15:13*

![images/88360b0d132adc9fd5e4494a4d4d9a69.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/88360b0d132adc9fd5e4494a4d4d9a69.jpeg)


---
**Nate Caine** *October 07, 2017 15:14*

![images/03c12e4f1fb6d5d11a5b7294be9749b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/03c12e4f1fb6d5d11a5b7294be9749b3.jpeg)


---
**Gary Balu** *October 23, 2017 05:56*

**+Nate Caine** Nate is on point. Doable, but no particularly the best solution. Without a rotary attachment you wouldn't be able to engrave more than three or four letter, depending on the size. 




---
*Imported from [Google+](https://plus.google.com/111990685464505125500/posts/iv4wNHQQKhL) &mdash; content and formatting may not be reliable*
