---
layout: post
title: "I'm saving up to get the next size up from the K40"
date: August 08, 2016 08:38
category: "Discussion"
author: "Tony Schelts"
---
I'm saving up to get the next size up from the K40.  Can anyone with one let me know how much better they are and can you do more on it.





**"Tony Schelts"**

---
---
**greg greene** *August 08, 2016 13:22*

Next size being in tube power?  or work piece area?


---
**Tony Schelts** *August 08, 2016 17:09*

Tube power which inclue lage work area


---
**Ben Walker** *August 09, 2016 11:39*

I'm totally impressed with my x700.  The k40 has been officially retired.   It's a dream to use a larger machine.   You won't be disappointed.   


---
**Tony Schelts** *August 09, 2016 11:57*

Where did you purchase it from.  Im in the UK


---
**Ben Walker** *August 09, 2016 12:32*

Sanvan Corp.  They shipped it from San Bernardino CA.  Not sure but I'm assuming they service Europe as well.   From China.  


---
**Tony Schelts** *August 09, 2016 12:42*

Thanks, is similar to the one your on about.

[http://www.ebay.co.uk/itm/80W-Co2-Laser-Cutter-700x500mm-Laser-Engraver-Laser-Cutting-Machine-High-Quality-/201621735997?hash=item2ef1978a3d:g:ZwMAAOSwbsBXlCTk](http://www.ebay.co.uk/itm/80W-Co2-Laser-Cutter-700x500mm-Laser-Engraver-Laser-Cutting-Machine-High-Quality-/201621735997?hash=item2ef1978a3d:g:ZwMAAOSwbsBXlCTk)


---
**Ben Walker** *August 10, 2016 19:54*

Looks just like it.  Productivity is through the roof.  Very happy


---
**Tony Schelts** *August 10, 2016 20:50*

What Wattage was it and what do you need to get it up and running.. Is it faster as well as a larger area that increases productivity.  What software do you use with it.???  Thanks


---
**greg greene** *August 10, 2016 20:57*

If you can see the ad - it tells you all that stuff


---
**Tony Schelts** *August 10, 2016 20:58*

Hello Greg I know I posted it, I wondered if it was the same wattage as ben had bought


---
**greg greene** *August 10, 2016 21:03*

Ah I see !  Looks like a nice machine.


---
**Tony Schelts** *August 10, 2016 21:07*

yep I want one.


---
**Ben Walker** *August 12, 2016 20:48*

yep same machine.  It runs RDworks with the corel plug in.  But no need to use corel.  I use illustrator cc and save native and import to rdworks.  No converting to illustrator 10 required.  I added a LAN dongle and assigned it a private IP address and can use it from any computer directly.  Stunningly easy to use compared to k40.

The dongle to webify it: [https://www.amazon.com/gp/product/B014SK2H6W/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B014SK2H6W/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/CcfrnSbPnrw) &mdash; content and formatting may not be reliable*
