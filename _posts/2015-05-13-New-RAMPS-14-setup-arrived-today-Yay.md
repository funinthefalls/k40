---
layout: post
title: "New RAMPS 1.4 setup arrived today! Yay!"
date: May 13, 2015 17:05
category: "Modification"
author: "Stuart Middleton"
---
New RAMPS 1.4 setup arrived today! Yay! Missing the driver boards! Boo! 





**"Stuart Middleton"**

---
---
**Jim Root** *May 13, 2015 17:19*

Is this a replacement for the K40 control board?


---
**Sean Cherven** *May 13, 2015 18:23*

It's not a direct replacement. You need to cut wires, including the ribbon cable, unless you make/buy a breakout board for it.


---
**Stuart Middleton** *May 13, 2015 19:36*

Yep, replacing my board with it. The wiring is trivial and I did plan to do it this weekend! Grrr


---
*Imported from [Google+](https://plus.google.com/+StuartMiddleton1/posts/8B9E67CgCXy) &mdash; content and formatting may not be reliable*
