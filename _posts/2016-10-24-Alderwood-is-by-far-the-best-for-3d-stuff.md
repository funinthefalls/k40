---
layout: post
title: "Alderwood is by far the best for 3d stuff"
date: October 24, 2016 10:27
category: "Object produced with laser"
author: "Scott Thorne"
---
Alderwood is by far the best for 3d stuff

![images/3aba4b21c112ae05694f52052dfd26bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3aba4b21c112ae05694f52052dfd26bd.jpeg)



**"Scott Thorne"**

---
---
**3D Laser** *October 24, 2016 10:34*

How long does it take to engrave that 


---
**Scott Thorne** *October 24, 2016 10:50*

An hour....3 passes


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 24, 2016 11:56*

Looks great. Is there any post processing involved to clean up the wood?


---
**Scott Thorne** *October 24, 2016 12:03*

Hot water and a soft scrub brush


---
**Scott Thorne** *October 24, 2016 13:00*

+ Yuusuf Sallahuddin...thanks man. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 24, 2016 13:04*

**+Scott Thorne** You're welcome. I'm seriously impressed with these pieces you've been sharing.


---
**Scott Thorne** *October 24, 2016 14:12*

+ Yuusuf Sallahuddin...thanks again....I've been getting lucky.


---
**Harvey Benner** *October 24, 2016 21:32*

Very nice!


---
**Tony Schelts** *October 25, 2016 14:20*

that is fantastic. Thanks for the link.  any idea how it work in Oak. I have only oak at the moment. pretty dark i think


---
**Scott Thorne** *October 25, 2016 14:36*

I've never tried oak...let me know. 


---
**George Fetters** *November 06, 2016 14:56*

live this output.  Awesome etch


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/PfeX4boBTA6) &mdash; content and formatting may not be reliable*
