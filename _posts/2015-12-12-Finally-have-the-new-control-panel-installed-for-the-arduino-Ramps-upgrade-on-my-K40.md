---
layout: post
title: "Finally have the new control panel installed for the arduino/Ramps upgrade on my K40"
date: December 12, 2015 12:15
category: "Modification"
author: "Anthony Bolgar"
---
Finally have the new control panel installed for the arduino/Ramps upgrade on my K40. Just need to finish the interlock and coolant monitoring system (that is what the second LCD is for). I also installed a keyed power switch on the side of the case, and there is an SD card slot on the side by the hole for the USB port. When I get a chance I will also post some pictures of my adjustable bed (it is a manually adjustable bed made for about $20.00) Next big challenge is to build a rotary attachment.



![images/335b8d8a702f7454ef623d11b62d0c95.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/335b8d8a702f7454ef623d11b62d0c95.jpeg)
![images/306d44f691ba273cd74ecde1a6a583cf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/306d44f691ba273cd74ecde1a6a583cf.jpeg)
![images/e28158d98bccc47274976c196af24c72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e28158d98bccc47274976c196af24c72.jpeg)

**"Anthony Bolgar"**

---
---
**Scott Thorne** *December 12, 2015 12:43*

That turned out beautiful man...great job!


---
**Anthony Bolgar** *December 12, 2015 12:45*

Thanks! I played around with different designs for about 3 days before I settled on this one.


---
**Scott Thorne** *December 12, 2015 12:48*

Looks great!


---
**Anthony Bolgar** *December 12, 2015 12:56*

It is actually clear acrylic. I painted the back side matte black then engraved the lettering from the back side, and filled the lettering with white paint. So from the front side everything is protected from scratching, and it gives great contrast for the engraving.


---
**Scott Thorne** *December 12, 2015 13:09*

Thanks for the info....great idea...it turned out great.


---
**ChiRag Chaudhari** *December 12, 2015 16:23*

Nice! Actually [2 sec pause]  Prettttttty Bad Ass. 


---
**ChiRag Chaudhari** *December 12, 2015 16:32*

PS: Now waiting for you to  post the pics of your adjustable bed. That is next big thing that confuses me, if I want to cut different thickness material how the hack will I keep the focal length to 2". 


---
**Timothy “Mike” McGuire** *December 12, 2015 23:00*

Awesome mod  can't wait to see video of it in action




---
**Jon Bruno** *May 14, 2016 17:51*

Hey Anthony, may I have a copy of this design?


---
**Anthony Bolgar** *May 14, 2016 18:19*

Of course you can Jon. I will post the files this weekend


---
**Jon Bruno** *May 14, 2016 20:10*

Thanks!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/A7ppkR1EgBY) &mdash; content and formatting may not be reliable*
