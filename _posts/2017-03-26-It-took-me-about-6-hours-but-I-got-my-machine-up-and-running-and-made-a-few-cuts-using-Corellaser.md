---
layout: post
title: "It took me about 6 hours but I got my machine up and running and made a few cuts using Corellaser"
date: March 26, 2017 01:21
category: "Original software and hardware issues"
author: "Martin Dillon"
---
It took me about 6 hours but I got my machine up and running and made a few cuts using Corellaser.  Am I right in thinking that the only way to control laser power is with the knob?  Is there no software control?  Could someone restate what my software options are if I keep the stock control board?



I am also curious what the plugs are on the back of my machine.  They have wires connected to them coming from the power supply.

![images/bfdfce10fa8f5017da456961b9e2fc8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bfdfce10fa8f5017da456961b9e2fc8c.jpeg)



**"Martin Dillon"**

---
---
**Ned Hill** *March 26, 2017 01:30*

With the stock board you are only able to control the power with the knob.  With the stock board you can use either corellaser or laserdraw.  The plugs in the back are for accessories like a water pump.




---
**James Rivera** *March 26, 2017 02:43*

I recently unpacked my K40 and had the same questions. I have not yet tested them, but they are labeled as 100v out.  I suspect they used non-US (European?) outlets.  Again, I haven't tested their power output yet, but I suspect they do indeed put 110v out, and converting them to US receptacles is probably OK (and then plugging the fan and water pump into them), but with no docs on how much current they can safely draw, I will probably not bother with them.


---
**Mark Brown** *March 26, 2017 03:38*

The outlets on the back are tied straight to the main (rocker) power switch, which in turn is tied straight to your house main.  So you get out whatever you put in.



They're actually weird dual sockets that will take both Euro (like my water pump) or US (like my exhaust blower) plugs.


---
**chris82o** *March 26, 2017 05:24*

**+Ned Hill** yes you are only able to control the power via the knob I've been using laserdraw and its not that bad as people make it seem the more i use it the easier it gets. I definitely  want to upgrade the board to be able to use open software. 


---
**Martin Dillon** *March 26, 2017 15:02*

In order to get the software to control the power, would I also have to upgrade the power supply?


---
**Ned Hill** *March 26, 2017 15:06*

No, stock power supply are fine.  The only thing you need to change is the control board.  Like to a Choesion3D.  The new control board will let you use other software like Laserweb which lets you control power.


---
**chris82o** *March 26, 2017 15:30*

**+Ned Hill** I am dieing to get my hands on a choesion3


---
**Ian Busuttil Naudi** *March 30, 2017 14:22*

Can someone suggest a place to purchase the Choesion3D and do you need any kits to connect it to the K40?


---
**Ned Hill** *March 30, 2017 14:26*

**+Ian Busuttil Naudi** [cohesion3d.com - Cohesion3D: Powerful Motion Control](http://cohesion3d.com/) 

 It's pretty much plug and play.  Great google group here for support.  **+Ray Kholodovsky** the creator of the cohesion3d is the mod.

[https://plus.google.com/u/0/communities/116261877707124667493](https://plus.google.com/u/0/communities/116261877707124667493)


---
**Eric Lovejoy** *June 01, 2018 04:17*

current control for laser power is on my todo list. Ill be looking into moding the k40 whisperer software to add a more server oriented interface, then to add options via arguments for things like power level. 


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/6NGoENrGnBT) &mdash; content and formatting may not be reliable*
