---
layout: post
title: "Add some color to your laser work!"
date: May 17, 2018 02:56
category: "Material suppliers"
author: "HalfNormal"
---
Add some color to your laser work!



[http://www.smoke-wood.com/SM-wd-21.HTML](http://www.smoke-wood.com/SM-wd-21.HTML)





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *May 17, 2018 12:40*

I wonder what ingredient makes this paint work, or is it modified?

What would standard paints do when exposed to low power IR. IR heaters are used to cure paints in many manufacturing operations.



I wonder if powdercoat paints would melt like this when exposed, they are thermalsetting. The material in this link does look like a powder.



Lots of colors here, including chrome:

[https://www.eastwood.com/hotcoat-powder-coating/powders.html](https://www.eastwood.com/hotcoat-powder-coating/powders.html)



Some cheap powder to try:

[https://www.harborfreight.com/catalogsearch/result/index/?dir=asc&order=EAScore%2Cf%2CEAFeatured+Weight%2Cf%2CSale+Rank%2Cf&q=powder+coat](https://www.harborfreight.com/catalogsearch/result/index/?dir=asc&order=EAScore%2Cf%2CEAFeatured+Weight%2Cf%2CSale+Rank%2Cf&q=powder+coat)



EDIT:

Evidently powder coating materials work :)

The panel shown in this article is impressive.



[https://jtechphotonics.com/?p=5582](https://jtechphotonics.com/?p=5582)



We should give this a try and capture the settings for different materials and powders.




---
**HalfNormal** *May 17, 2018 12:52*

Looking into this a little more I think it's what you're saying is that it is a derivative of powder coating if not just powder coating.


---
**Don Kleinschnitz Jr.** *May 17, 2018 13:06*

**+HalfNormal** my bet is that this is standard powder coat paint and it is cured by selective exposure to the laser at the right energy.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/5Qwar82fQLu) &mdash; content and formatting may not be reliable*
