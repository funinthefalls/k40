---
layout: post
title: "Organizing tools with magnets. I use small neodymium magnets to organize tools on the front of my laser cutter"
date: February 04, 2017 19:16
category: "Discussion"
author: "Jeff Johnson"
---
Organizing tools with magnets. 

I use small neodymium magnets to organize tools on the front of my laser cutter.

![images/a2bfcb1f21e9f6355f4be6bde1a04a34.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a2bfcb1f21e9f6355f4be6bde1a04a34.jpeg)



**"Jeff Johnson"**

---
---
**Richard Vowles** *February 05, 2017 00:22*

I'd probably point that knife down.


---
**Jeff Johnson** *February 05, 2017 01:04*

**+Richard Vowles** I usually have the cap on but I couldn't find it. I'll take your suggestion and point it down until I find the cap. That's a fresh blade and I like my fingers. 


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/LGR4fPxmtbH) &mdash; content and formatting may not be reliable*
