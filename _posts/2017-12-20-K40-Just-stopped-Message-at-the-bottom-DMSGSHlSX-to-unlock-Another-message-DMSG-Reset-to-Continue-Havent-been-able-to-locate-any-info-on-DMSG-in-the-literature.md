---
layout: post
title: "K40 Just stopped. Message at the bottom DMSG:'SH'l'SX' to unlock Another message DMSG Reset to Continue Haven't been able to locate any info on DMSG in the literature"
date: December 20, 2017 17:06
category: "Discussion"
author: "bbowley2"
---
K40 Just stopped. Message at the bottom DMSG:'SH'l'SX' to unlock

Another message DMSG Reset to Continue

Haven't been able to locate any info on DMSG in the literature.

If I disconnect the K40's power and unplug my C3D board I can get the K40 to give me jog control but when I execute RUN the K40 and the k40 locks up and I get one of the error messages. GRBL firmware.





**"bbowley2"**

---
---
**Paul de Groot** *December 20, 2017 20:26*

Normally the message $H $X  refers to get the machine back on line. The actual error could be a software limit switch, so the actual coordinates of the cutting or engraving go beyond the set size of the machine.  You csn disable software end stops. Also you can use $C to execute the gcode without morion to test were the gcode breaks. Use cncjs as gcode sender so you get all the feedback on your [screen.cncjs](http://screen.cncjs) can be found on github


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/1Tc7CkH5zoB) &mdash; content and formatting may not be reliable*
