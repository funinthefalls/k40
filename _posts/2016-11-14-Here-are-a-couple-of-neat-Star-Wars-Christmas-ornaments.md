---
layout: post
title: "Here are a couple of neat Star Wars Christmas ornaments"
date: November 14, 2016 19:12
category: "Object produced with laser"
author: "K"
---
Here are a couple of neat Star Wars Christmas ornaments. I cut them from 3MM birch ply. The engraving was done vector instead of raster. The plans are not mine. You can get them here, along with a fantastic Millennium Falcon that I have yet to cut. 

[http://c4labs.build/forums/topic/yt-1300-light-freighter-with-cut-files/](http://c4labs.build/forums/topic/yt-1300-light-freighter-with-cut-files/)



I had to scale the plans to get them to work with my material. Please note that I had to scale the Tie Fighter and X-Wing differently since the tabs didn't seem to be consistent between the two models. The plans have a skips in the cut lines so that they don't fall out of the sheet. They're around 1.5mm, and I found that a little too wide to make removal from the sheet easy enough, so I decreased the width to about .5mm. 





![images/8d26ddefaedca95e23d4cad3eabc42de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d26ddefaedca95e23d4cad3eabc42de.jpeg)
![images/9e1f6e7d6b612d7a5be125f183ad364f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e1f6e7d6b612d7a5be125f183ad364f.jpeg)

**"K"**

---
---
**Steve Anken** *November 14, 2016 20:36*

Acrylic with LEDs would really make them stand out on the tree. :-)


---
**Mark Leino** *November 15, 2016 05:12*

Wow! That is awesome


---
**Cesar Tolentino** *November 17, 2016 14:05*

Would you mind sharing your scaled down files? Thanks.


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/g6rGqW4PpJ1) &mdash; content and formatting may not be reliable*
