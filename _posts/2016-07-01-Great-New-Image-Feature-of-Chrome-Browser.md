---
layout: post
title: "Great New Image Feature of Chrome Browser!"
date: July 01, 2016 16:06
category: "Discussion"
author: "HalfNormal"
---
Great New Image Feature of Chrome Browser!



If you are searching for images, art ect and you use the Chrome browser, when you click the image you will now be able to save that image online. So no more downloading and comparing, you just save what you like and then go back and choose what you want. It also makes it easy to see what you have looked at before and have it available for a later project. Enjoy!





**"HalfNormal"**

---
---
**Ned Hill** *July 01, 2016 17:36*

Cool, that's awesome.  Hadn't noticed that before. Thanks for sharing. :)


---
**Don Kleinschnitz Jr.** *July 01, 2016 19:15*

MM how do I use this function. If I search for an image, click on it, everything looks the same..


---
**Ned Hill** *July 01, 2016 19:27*

**+Don Kleinschnitz**  in google image search after you click on the image you should see this on the right side of the image. [https://www.dropbox.com/s/82bc7r6sxze4byt/Google%20image%20save.PNG?dl=0](https://www.dropbox.com/s/82bc7r6sxze4byt/Google%20image%20save.PNG?dl=0)  Where there are "save" and "veiw saved" buttons.  If you don't see them you made need to make sure you are using the most current version of chrome.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 01, 2016 21:52*

Nice. Also hadn't noticed it before. Thanks for sharing.


---
**Don Kleinschnitz Jr.** *July 02, 2016 03:56*

**+Ned Hill** thanks never paid attention to that and did not know I had a "save" space.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/WSwguhAmeTY) &mdash; content and formatting may not be reliable*
