---
layout: post
title: "Can someone kindly explain the differences between the ACR kit and the Cohesion3d mini?"
date: December 14, 2016 22:42
category: "Discussion"
author: "Jason He"
---
Can someone kindly explain the differences between the ACR kit and the Cohesion3d mini? I understand that the end result is enabling smoothieboard, but it looks like the ACR requires the smoothieboard hook up to it while the cohesion3d integrated the smoothieboard into itself. Can anyone shed some more light? Thanks.





**"Jason He"**

---
---
**greg greene** *December 14, 2016 22:56*

I don't know about the ACR kit, I have the Cohesion board.  You only need to hook up three wires, plug in the stepper motor driver for your machine, drop a config file onto a micro SD card and insert it, and your in business.  If not running Win 10 - you also need to download and install a driver so the board can talk to you machine.  You can add a GLCD Panel and etherboard connector if you like.  Ray has the adaptors for those.   


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2016 22:57*

Yeah so the Mini is drop in and play. The smoothie "brain" and motor driver (sockets) are all on one board along with all the k40 connectors that we've seen to date. Comes with everything you need to get up and running including a preconfigured sd card with all the laser settings. 

It's an inexpensive all in one solution that requires no external/ separate boards. 


---
**greg greene** *December 14, 2016 22:58*

Highly recommended :)


---
**Wolfmanjm** *December 14, 2016 23:19*

Please do not call a cohesion a smoothieboard. Smoothieboard is a specific product and a cohesion board is a different product. Using the term smoothieboard will confuse everyone.. thank you.


---
**Jason He** *December 15, 2016 01:05*

Sorry. What I should have said was Smoothieware.



It looks like the Cohesion3d Mini is exactly what I needed. Thanks for the help guys!


---
**Ray Kholodovsky (Cohesion3D)** *December 15, 2016 01:11*

**+Jason He** glad to hear it. If you have any more questions or need anything else, please by all means let me know. 

Cheers,

Ray

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2016 06:40*

**+Jason He** You're correct that the ACR requires a Smoothieboard + the ACR module. The ACR is basically an interface to simplify the wiring process (for people like me who have no idea). Pretty much take the plugs out of original controller, plug them into the ACR. Take the plugs from the ACR, plug them into the Smoothieboard. Currently however, **+Scott Marshall**, the designer/developer/manufacturer is unable to be contacted. I suspect it is due to his ongoing health issues & hope he'll be back in the near future, but no idea really what is going on for him.


---
*Imported from [Google+](https://plus.google.com/109350981963273445690/posts/4jMCqZQUQYm) &mdash; content and formatting may not be reliable*
