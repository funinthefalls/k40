---
layout: post
title: "Got my laser kerf figured out to give me good friction fits"
date: June 28, 2016 01:19
category: "Object produced with laser"
author: "Ned Hill"
---
Got my laser kerf figured out to give me good friction fits.  Cut from 1/4" baltic birch plywood.  



![images/af5da0428425c311f721bf274a2a3ac9.png](https://gitlab.com/funinthefalls/k40/raw/master/images/af5da0428425c311f721bf274a2a3ac9.png)
![images/e6e04bbc6ccdc23314c95df20d2bf338.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e6e04bbc6ccdc23314c95df20d2bf338.png)

**"Ned Hill"**

---
---
**Anthony Bolgar** *June 28, 2016 01:29*

Looks real good. Keep up the good work!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 28, 2016 02:34*

That came out really nice. Good work :)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/dQCYzea3HGs) &mdash; content and formatting may not be reliable*
