---
layout: post
title: "So, it turns out the stuff I got yesterday (Liqui Moly) is no good for the stainless marking"
date: May 19, 2016 05:43
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So, it turns out the stuff I got yesterday (Liqui Moly) is no good for the stainless marking. Even after leaving it overnight, it was still dripping wet. So I think **+Scott Marshall** is correct that it is never meant to dry.



Today I purchased some Minehan Circle Spray. According to the MSDS, it has 40-60% MoS2 (Molybdenum Disulphide). This is much higher than the CRC Dry Moly & Quick Dry Moly sprays (they're both <10%).



So this stuff cost me AU$23 odd. It is used for industrial purposes in general & there is a supplier of it semi-local (about 1/2 hour away).



Did some tests today with the new stuff & it works wonders. Also, cleanup is a breeze. Just used water & a nail-brush to scrub it off, with minimal effort. Closer to the text, I used the Dremel for about 10 seconds with a brush attachment to just get the last bit off that didn't want to come off with the nail-brush. I used my vector engrave method, which is not really suitable for this, as it leaves gaps between each rectangle. Either that, or I need to decrease the spacing between rectangles.



Also, as a last test for today, I tried on a small piece of Aluminium plate. I sprayed the stuff on, hit it with the heatgun (couple of seconds to dry then) & then I lasered it. It didn't bond, however if you look closely at the picture (last one) you can see some squiggly lines in the centre region where it did mark it, albeit super faint. So I imagine with either more power or more passes or slower speed (I used 20mm/s) it just might mark permanently & darker. That's a test for another day.



![images/a72876b274737629e81d613343eb91b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a72876b274737629e81d613343eb91b1.jpeg)
![images/6d56c3a1da2b6d00df07ea9a4972c4ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d56c3a1da2b6d00df07ea9a4972c4ae.jpeg)
![images/c4ccc714cad8cac3b9a4d8b29e1b23ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4ccc714cad8cac3b9a4d8b29e1b23ae.jpeg)
![images/2b4fb5f8d546015c774b3f6b941c8b98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b4fb5f8d546015c774b3f6b941c8b98.jpeg)
![images/c3f0a3a34087f377f09507a1d7069236.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c3f0a3a34087f377f09507a1d7069236.jpeg)
![images/85256e87006fb2e5616eaa6e07533511.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/85256e87006fb2e5616eaa6e07533511.jpeg)
![images/6c5fb3e443219241c38652cc7b967ab8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c5fb3e443219241c38652cc7b967ab8.jpeg)
![images/0e616c7bfe51a14ec8fdd056c5ffb075.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0e616c7bfe51a14ec8fdd056c5ffb075.jpeg)
![images/9a2cd8269c5d75b53ec806cbc8ecc546.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a2cd8269c5d75b53ec806cbc8ecc546.jpeg)
![images/2df2dd1e2323e32bb0742ca99d67512c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2df2dd1e2323e32bb0742ca99d67512c.jpeg)
![images/243abb5985077ce7c906200af00e8bae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/243abb5985077ce7c906200af00e8bae.jpeg)
![images/444a6d49a662539a29912404710cbb63.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/444a6d49a662539a29912404710cbb63.jpeg)
![images/a27f897c72c77c5cb3ad01207dfec9fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a27f897c72c77c5cb3ad01207dfec9fd.jpeg)
![images/dbbaedc8d673cf56bd60d1dd81714f17.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dbbaedc8d673cf56bd60d1dd81714f17.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Alex Krause** *May 19, 2016 05:52*

Thoughts on the aluminum test... since aluminum conducts heat better than stainless steel makes me wonder if the bonding temp was harder to reach... we need a test with a heated work bed to confirm


---
**I Laser** *May 19, 2016 06:16*

Looks good, much better result than previous tests.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 06:39*

**+Alex Krause** That's an interesting concept. Might work. Could maybe test by aiming the heatgun at the aluminium while lasering it. I'll do a test later sometime with the speed at say 1mm/s just to see what happens. Not practical, but it will show if it is possible to bond with more heat in the spot.


---
**Phillip Conroy** *May 19, 2016 06:44*

love the cutting table-what shop did you buy this stuff from?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 06:48*

**+Phillip Conroy** Haha, my cutting table was just a dodgy setup to try get the bowl at the right height.



I got the Circle Spray from the Stapylton store of [http://www.minehanagencies.com.au/lubricants.html#circlespray](http://www.minehanagencies.com.au/lubricants.html#circlespray)



They have a few stores, but all located in QLD.

Townsville, Stapylton, Airlie Beach, Mt Isa. Fortunately for me Stapylton is reasonably close.


---
**Mishko Mishko** *May 19, 2016 07:34*

I've also tried with some graphyte/MOS2 grease bought from local manufacturer, with no effect whatsoever, so I guess either teh MoS2 content is too low, or there are other factors, too. Not even a slightest imprint on stainless steel...

So I've ordered a spray on Ebay, will try and report if it works, if it does, this may me useful for people in Europe, where CRC shipping costs may be too high...

[http://www.ebay.com/itm/131718003118](http://www.ebay.com/itm/131718003118)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 10:55*

**+Mishko Mishko** I'm curious what sort of settings you used when you tested the graphite/mos2 grease that didn't work? I've been using around 10mA @ 20mm/s.


---
**Mishko Mishko** *May 19, 2016 15:37*

I don't remember exact speed, but I think I didn't go lower than 50mm/s. And I did crank it up all the way to 25mA in the end. Not getting even a scratch, I gave up quickly, I may try some more, as it's really a shame, this costs next to nothing, 400 ml is around € 3,00,and if if did work, I'd simply apply it via some kind of stencil, to get even thickness, and remove it with a spatula after engraving. This way it would last a couple of lifetimes of intense engraving LOL.

Pure MoS2 is available on ebay as a powder, but the guy selling it doesn't ship where I live. The idea is to encapsulate it in the sufficient concentration in whatever carrier would make it easiest to apply, but some sort of grease sounds as the way to go to me. Vaseline and similar stuff would work too I guess. If MoS2 is the sole component which makes engraving possible, which I have still to find out. Could take some time haha...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 20, 2016 01:43*

**+Mishko Mishko** I believe MoS2 is the only thing that is necessary, because the stuff I got is only MoS2, Butane & Isopropanol. I am fairly certain the butane & iso are just for aerosol capability & carrier. I'd test that graphite stuff you have again at much lower speeds, try like 10mm/s. If it marks, then bonus due to the cheap cost.



**+Allready Gone** Sounds like me. Once I learn a new trick for a material, nothing is safe anymore haha.


---
**George Canfield** *May 20, 2016 16:41*

I believe the problem may lie in the reflectivity  of the aluminum


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 21, 2016 00:13*

**+George Canfield** Could be the case, but the aluminium plate I used is not really reflective. It is quite matte finish. But, what I see with my eyes may not necessarily be the case with a laser beam bouncing off it.


---
**George Canfield** *May 21, 2016 00:16*

I was requoting a video I watched recently discussing why aluminum was sometimes used as a cutting bed....I will try to find it again and link it.... I am just a noob to the laser


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 21, 2016 00:17*

**+George Canfield** I think it is used as a cutting bed due to lack of reflectivity in general. But I may remember incorrectly. I'm still a noob myself too.


---
**Mishko Mishko** *May 21, 2016 06:32*

For CO2 lasers with sufficient power to cut metal, aluminum is harder to cut than say C-Mn steel ot stainless steel, as it is more reflective, the molten pool even more so, and in rare cases it can reflect back throught the optics and even damage the laser. Copper is even more problematic in the same way, being an almost ideal mirror for this wavelength. But I don't think any of that would apply to the small power laser like K40, no chance of melting, or even scratching metal...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 21, 2016 07:06*

**+Mishko Mishko** Ah, so I had it backwards & aluminium is more reflective. Thanks for the extra input Mishko.


---
**Mishko Mishko** *May 21, 2016 10:48*

Yes, but I think that's only significant when it's molten, otherwise I doubt that has any effect with small power lasers. Also depends on th ealloy, for example the laser i worked on cut ALMg3 without problems, but wouldn't even scratch Al2SiO3 , just a ton of sparks...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 21, 2016 14:58*

**+Mishko Mishko** Ah, seems there is a lot to it. That's interesting to know that different alloys of the same base metal will react differently.


---
**Peter Jacobsen** *October 27, 2016 13:38*

Do you think this will work to mark metal?[rocol.com - DRY MOLY Spray](https://www.rocol.com/products/dry-molybdenum-sulphide-aerosol-spray) 

Contains more than 70% molybdenum disulphide

/Peter


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 27, 2016 14:22*

**+Peter Jacobsen** Most people are using only about 8% MoS2 with decent results. Check some of **+Scott Thorne**'s previous efforts with it. If I recall correct he was using a CRC Dry Moly lube (correct me if I'm wrong Scott). Make sure whatever you're going to use is "dry" moly. The liquid moly definitely does not work. I can't say for certain, but I have a feeling that the product you linked will work equally as well as what I used. What I used was about 80% (or 88%) if I recall correct.


---
**Scott Thorne** *October 27, 2016 14:46*

**+Yuusuf Sallahuddin**...that is it....CRC Dry Moly Lube.


---
**I Laser** *November 07, 2016 21:46*

Decided to give this another crack, I tried to get something suitable months ago but had no luck. Seems molybdenum is hard to come by here. The info I've found states CRC Dry Moly Lube only has 1-3% molybdenum disulphide.



[https://s17.postimg.org/mdddwfbbz/data.jpg](https://s17.postimg.org/mdddwfbbz/data.jpg)



The CRC version is not available in Australia, though according to CRC's web site it will be available from mid October (though I'm yet to see any stockists). I've dropped them a line to find out what's going on.



Can't find detailed information on Rocol's version, but its prohibitively expensive at around $95 for a 400ml can.



A company called Ambersil (which is apparently linked to CRC) has a dry moly spray available for around $30 for 400ml. Though no information on moly content.



I've also contacted Minehan again to see if they've got stockists in Victoria. 



Can't believe how hard it's been to get my hands on this stuff!





[s17.postimg.org - s17.postimg.org/mdddwfbbz/data.jpg](https://s17.postimg.org/mdddwfbbz/data.jpg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2016 21:53*

**+I Laser** I would imagine that Minehan could post you some. Worst case scenario I could organise picking some up next time I'm up that way & post for you (although not sure if they'd let me since it is an aerosol spray). Let me know how you get on with your enquiries.


---
**I Laser** *November 07, 2016 22:13*

Thanks for the offer **+Yuusuf Sallahuddin**



Pretty sure aerosol cans are a no go in the standard postal chain. Would have to be via  road freight, which is in the too hard basket. ;)



I'll let you know how I go, might also be of use to others in Australia trying to get their hands on the fabled spray. I'm really surprised CRC is not readily available. Cheers


---
**I Laser** *November 08, 2016 22:15*

Still no update from either Minehan or CRC :\



It's as elusive as gold at this stage lol!



edit: I rang CRC direct, turns out it was only made available in Australia last week and they've not had a single commercial order yet. They passed me on to Blackwoods which is a CRC distributor in Victoria, I'm going through the process of getting a quote from them.


---
**I Laser** *November 09, 2016 23:17*

And another small (hopefully final) update. Haven't heard back from Blackwoods regarding availability or pricing for CRC Dry Moly Lube



In the meantime managed to get hold of a very helpful guy at Minehan (Nathan). They don't have any distributors in Victoria, so the only option was to courier a couple of cans. It's a little steep ($23) for the courier, but at this stage was the only real option available. 



Works out about $37 per can delivered. So still cheaper than some of the other options. Looking forward to finally giving this a crack!



Thanks again for the heads up about circle spray **+Yuusuf Sallahuddin**  ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 10, 2016 09:37*

**+I Laser** Sweet, I guess $23 for courier is a price to pay just to have a go. Worst case scenario it make great lubricant for gate latches etc :)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/6mzzddKsQMd) &mdash; content and formatting may not be reliable*
