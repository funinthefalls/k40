---
layout: post
title: "I am having a very hard time getting my laser to focus to be able to cut through 3mm ply"
date: April 02, 2016 19:09
category: "Discussion"
author: "3D Laser"
---
I am having a very hard time getting my laser to focus to be able to cut through 3mm ply.  I had it almost dead to right but my tube needed to be changed and it was hitting low. after an hour or two I got it to dead center of the cutting head but it still is not focusing enough to cut.  I did the ramp test and still I could not find a spot where the line was thin and made it through the material.  should I lower the tube even through it was hitting on the lower part of the circle and the tube was not level. or is something else presenting me from focusing 





**"3D Laser"**

---
---
**Phillip Conroy** *April 02, 2016 21:18*

Stock k40 focal lens has a focal point of 50mm js your work at this distacne to the botton of the f/lens?.i had a damaged f/lens on my cutter.if the beam is spread at 50 mm form f/lens it is the f/lens [rised side goes up]- experance will tell here ,i clean my f/lens every 30 minutes at present as i sm getting a lot of moisture hitting the f/lens.do you have air assist?,i ask this as when i installed mine the beam was comptly cut off as was hitt the side of the air assist


---
**3D Laser** *April 02, 2016 21:33*

I have upgraded the lens to A 18mm lens from light objects 


---
**HP Persson** *April 03, 2016 00:30*

**+Corey Budwine** Checked so it is a 50.8 and not a 63.5 focus lens you got there? It may explain the missing thin line on the ramp test.

Or maybe it is a 63.5mm and you know it and have set the height correctly?



i cannot imagine anything else than mirror/lens problem


---
**3D Laser** *April 03, 2016 00:32*

It's the 50.8 before I replaced my tube it cut fine now I can't get it to cut which makes me think the tube itself is misaligned as it seems to have good power 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/gwosrX9t1sy) &mdash; content and formatting may not be reliable*
