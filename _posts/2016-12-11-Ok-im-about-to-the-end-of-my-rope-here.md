---
layout: post
title: "Ok i'm about to the end of my rope here"
date: December 11, 2016 16:49
category: "Original software and hardware issues"
author: "woodknack1"
---
Ok i'm about to the end of my rope here. For some reason I can not for the life of me get the engraving and cutting lined up.  I have done this in the past with no problems but now it just wont work. I have a reference mark set in the top corner of each of my layers (engrave & cut). This has worked fine in the past for me. Seems like the software or controller is acting screwy on me! Frustrating when you spend hours messing around with the same end results.







**"woodknack1"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 17:15*

You wouldn't by any chance have reinstalled the plugin or something? Is this with CorelDraw/CorelLaser or LaserDRW?


---
**woodknack1** *December 11, 2016 18:21*

not i didn't reinstall the plugin. but I might try that! yes im using coreldraw with corellaser and I also have laserdrw.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 18:32*

I just thought if you had, you may need to reset the board ID & model in the plugin settings.



Are your cut & engrave settings in the plugin both set to WMF? See: [https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)


---
**Ned Hill** *December 11, 2016 18:37*

Do the cut/engrave previews show both the cuts and engraves in the correct positions?


---
**woodknack1** *December 11, 2016 18:37*

yes ive tried that too (WMF) the problem with that is in cut in WMF its adding lines that are not really there!?


---
**Ned Hill** *December 11, 2016 18:39*

Ah so you are having problems cutting open curves?


---
**Ned Hill** *December 11, 2016 18:40*

What version of coreldraw are you using?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 18:40*

**+woodknack1** Wow, that's extremely odd then. Although I have heard others complain of that issue in the past, just I cannot recall the fix for it. Might it be unclosed paths on your vectors that you are cutting causing the extra lines? (i.e. do they seem to be going from one end of an arc to another end of the arc in a straight line?)


---
**Ned Hill** *December 11, 2016 18:43*

I'm using corel X8 and I found that I had to change my cut output file to EMF to get it to leave the open curves open.


---
**woodknack1** *December 11, 2016 18:44*

ok see I highlighted the top cover? Watch when is goes into corellaser as cutting in WMF you will see added extra lines.

![images/ef5443778d78f09c8e1a099b2545713c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef5443778d78f09c8e1a099b2545713c.jpeg)


---
**woodknack1** *December 11, 2016 18:44*

![images/0d8f85501ccd683b7448bec903a42c88.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d8f85501ccd683b7448bec903a42c88.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 18:46*

**+woodknack1** That is exactly what I thought was happening. It is that the paths are not joined/closed for that piece. You need to make sure all the lines are connected as a composite path. What software are you using for the design phase?


---
**woodknack1** *December 11, 2016 18:46*

where those extra lines are coming from is beyond me. Im lost and have wasted hours messing with differnt things trying to get it to cut right.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 18:47*

**+woodknack1** It's the unjoined paths. The software for some reason reads unjoined paths & joins them... Hence the extra line/s. It's a simple fix if you can tell me which software you use to design in the first place.


---
**woodknack1** *December 11, 2016 18:57*

coreldraw 8




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 19:10*

Okay, I've figured out how to stop it... Object > Combine (or Ctrl + L) when all curves for the shape are selected. I will post screenshots.





![images/bcd6d6451226d4e8da342ce7002b44fc.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bcd6d6451226d4e8da342ce7002b44fc.png)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 19:10*

![images/faca792cd14ffed677f1036c43e99726.png](https://gitlab.com/funinthefalls/k40/raw/master/images/faca792cd14ffed677f1036c43e99726.png)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 19:15*

Not sure if Ned's comment got lost in the thread, but he mentions that he had to change to EMF. That's interesting to note. I've never tried that.


---
**woodknack1** *December 11, 2016 19:18*

didn't work. they were grouped so I had to ungroup them to get the combiner option to highlight. I combined the and still the same thing. add lines.




---
**Ned Hill** *December 11, 2016 19:29*

WMF

![images/cdd5f4263dd11b97dd34f9e5b334175f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/cdd5f4263dd11b97dd34f9e5b334175f.png)


---
**woodknack1** *December 11, 2016 19:30*

Ned since your are using corel8 too you try it. draw a rectangle add a oval then join it to the rectangle and cut away the lines you dont want. join and combine them. I still am getting the extra lines unless Im doing something wrong?




---
**woodknack1** *December 11, 2016 19:33*

emf does NOT work for lining up the engraving with the cutting.




---
**woodknack1** *December 11, 2016 19:59*

what is NMF?




---
**woodknack1** *December 11, 2016 20:00*

i just tryed to weld it. still having same problem.


---
**woodknack1** *December 11, 2016 20:07*

wish i could send you the file




---
**woodknack1** *December 11, 2016 20:15*

try this.



[drive.google.com - box.cdr - Google Drive](https://drive.google.com/file/d/0B30jgLzgMJbcUTJSSTA5WFZoak1NcTFBaWE2Wm1xbGtjUFhR/view?usp=sharing)


---
**woodknack1** *December 11, 2016 20:21*

in order to cut it out with the engraving your need to use WMF (or it will not line up) wheni do that I get the extra lines in cut mode in some pieces.




---
**woodknack1** *December 11, 2016 20:30*

hide the engraving layer and you will see the cutting line




---
**woodknack1** *December 11, 2016 20:32*

just shut off the engraving layer and highlight the first cutting piece. open in corel laser in WMF and see the extra lines.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 20:46*

**+woodknack1** any chance you could save the file in a Corel X7 version? I can try repair it for you.


---
**woodknack1** *December 11, 2016 20:47*

Sorry all I have is corel draw 8 ( had to buy it). Id really like to know what Im doing wrong.. Not just have someone fix it for me..


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 20:51*

**+woodknack1** Yeah, maybe Ned can help out with that file & work out what is going wrong & tell you how to prevent in future, as with x7 it won't let me open it... Although, I might try open in Adobe Illustrator & see...



edit: Illustrator won't open it & Inkscape asks me to choose page & when I try to choose page 2 it crashes lol. Sorry, can't help from my end.


---
**woodknack1** *December 11, 2016 21:08*

ya maybe right there. but I got rid of the origin layer and put a dot in each corner same spot in engrave and cut. which im sure will work fine. but i couldn't use WMF as i had extra lines that would cut.



also by having the print command off in origin would just mean it wont try to fire that mark, but it still sees it and reconizes it.


---
**woodknack1** *December 11, 2016 21:09*

you would have to shut the eye off for it not to see it.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 21:32*

You can use a no-fill no-border 1mm x 1mm square in the top left corners maybe? The plugin registered they were there for me (when I was still using it) for alignment purposes, but it didn't laser it.



Actually, what I would do is create a 300 x 200mm no-fill no-border box around the entire cut area & entire engrave area & do it that way.



Then when cutting I'd select all cut elements + the no-fill no-border rectangle. I'd do same for engraving.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 01, 2017 16:24*

**+Adam Gibbons** I think you'll find what version of Windows you are using will make absolutely no difference to the way the software runs (with the exception that in Win 7 I did notice that the toolbar didn't disappear every time I used it). But for the most part, your issue with the cutting twice etc will still be around until you get things sorted in the software settings.



A few points to check (not sure if you have done already) is the Board Model, Board ID. Then if they're set, I'd suggest using WMF for both cut & engrave in the output. Set border width of objects to cut as 0.01mm. A thought I'm wondering, is do objects with a border & a fill colour maybe cut with the double cut? I always used a 0.01mm border with no-fill colour.



Oh note, this is for CorelDraw with CorelLaser plugin. Can't say about LaserDRW as I have no experience there.


---
*Imported from [Google+](https://plus.google.com/107255578063171490994/posts/hKRchyextya) &mdash; content and formatting may not be reliable*
