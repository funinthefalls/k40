---
layout: post
title: "So, I remembered seeing somewhere that you can laser denim"
date: March 13, 2016 09:44
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So, I remembered seeing somewhere that you can laser denim. Engrave it too.



I decided to give it a test today. Basically just a scrap of denim from some old jeans I had laying around. It's not heavy denim, rather a lighter thickness. Here's the results.



I started with testing engraving. My power was already set to 8mA, so I decided to test that @ 500mm/s. No good. Burned straight through.



Then I dropped the power to somewhere around 5-6mA. Worked okay, but looked a little deep for my liking. Basically we just want to discolour the dye, not significantly weaken the fabric.



So, final engrave test was at the lowest power I can still get my cutter to do anything, around 3mA. This seems to give a nice discolouration, without looking like it is weakening the fabric.



The large squares were tests for cut. I first tried 3mA power (since it was still set to that from engraving) & then 15mm/s speed. Went 99% of the way through, but some threads were still hanging on & it didn't quite want to pop out.



Then I tried 10mm/s @ 3mA. Worked perfect. Straight through it.



So, the short version is... for lightweight denim:



- Cutting @ 3mA power @ 10mm/s.

- Engraving @ 3mA power @ 500mm/s.



![images/eecad6b953cf4c64cd41bcb6be19e6e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eecad6b953cf4c64cd41bcb6be19e6e4.jpeg)
![images/1f00b0c449a4e51fe2153076d192f071.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f00b0c449a4e51fe2153076d192f071.jpeg)
![images/e1898b59309fa748fc71555e6700e0cb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1898b59309fa748fc71555e6700e0cb.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Joseph Coco** *March 14, 2016 19:27*

It looks good. Do you think it would survive a few machine washings or does the material feel weaker?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 14, 2016 20:25*

**+Joseph Coco** for the 3mA @ 500mm/s engraving, the material doesn't feel like it has lost any strength. I will do another test on another scrap & chuck it through the wash a few times to verify. I'd imagine it will survive fine, since it doesn't seem to be any weaker in that section. Anything higher than 3mA, it is possible it will fall apart (as the 5-6mA seems weaker & the 8mA was definitely weaker, basically falling apart in that section as soon as I touched it).


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/DPzfJvVT7xJ) &mdash; content and formatting may not be reliable*
