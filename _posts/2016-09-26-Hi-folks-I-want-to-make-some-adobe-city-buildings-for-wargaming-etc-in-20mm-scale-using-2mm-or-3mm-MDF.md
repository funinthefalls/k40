---
layout: post
title: "Hi folks. I want to make some adobe & city buildings for wargaming etc in 20mm scale using 2mm or 3mm MDF"
date: September 26, 2016 17:25
category: "Discussion"
author: "Andy Flatman"
---
Hi folks.

I want to make some adobe & city buildings for wargaming etc in 20mm scale using 2mm or 3mm MDF. 

Looking for some templates I could use for Corel laser etc. 

Anyone know where I can source these from.



Thanks Andy





**"Andy Flatman"**

---
---
**Alex Krause** *September 26, 2016 18:09*

Thingaverse maybe?


---
**Ariel Yahni (UniKpty)** *September 26, 2016 18:44*

I would suggest searching for groups that do the same. Haven't seen repository for that


---
**Andy Flatman** *September 27, 2016 22:31*

Thanks for you replies. Will keep looking.


---
*Imported from [Google+](https://plus.google.com/103599013932150582097/posts/aj4XsgjRYek) &mdash; content and formatting may not be reliable*
