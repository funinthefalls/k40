---
layout: post
title: "Looking to get air assist for my k40 anyone recommend a uk stockist? plus on a scale of difficulty converting it 1 being easy and 10 being too hard for a numb skull like me?"
date: September 25, 2016 23:03
category: "Air Assist"
author: "J DS"
---
Looking to get air assist for my k40 anyone recommend a uk stockist?﻿ plus on a scale of difficulty converting it 1 being easy and 10 being too hard for a numb skull like me?

﻿





**"J DS"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 26, 2016 01:48*

I can't recommend a stockist, but on the scale of difficulty it should be 1-2. It will be a very simple unscrew original, screw on new one.


---
**Alex Krause** *September 26, 2016 03:46*

Hardest part might be realigning the beam path to make sure it doesn't hot the inside cone of the air assist


---
**J DS** *September 29, 2016 14:47*

ooh thats kinda worrying as I am about as clumsy as a bull elephant ice skating




---
*Imported from [Google+](https://plus.google.com/102322879087562424255/posts/2GFDE8i69XE) &mdash; content and formatting may not be reliable*
