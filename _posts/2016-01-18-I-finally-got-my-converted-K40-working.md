---
layout: post
title: "I finally got my converted K40 working!"
date: January 18, 2016 15:43
category: "Discussion"
author: "David Cook"
---
I finally got my converted K40 working!  Using a Ramps 1.4 controller plugged into a Kangaroo 64bit Windows 10 micro desktop PC. Exciting!!

My garage smelled like a forest fire lol. So I finally got my exhaust hose hooked up. 

Now I just need to play with the software and settings next.

Using Turnkey Tyranny Marlin firmware and the associated Inkscape plugin 



![images/e45d0cc4b959ea8b4632f5a793acfcee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e45d0cc4b959ea8b4632f5a793acfcee.jpeg)
![images/07ee93c320a5232c87ce065cd81cec50.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07ee93c320a5232c87ce065cd81cec50.jpeg)
![images/b6f97530419f93505c2ea96798336402.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6f97530419f93505c2ea96798336402.jpeg)
![images/60c0d34405d91d088aaaff0e92a925c8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60c0d34405d91d088aaaff0e92a925c8.jpeg)
![images/9cf0f9c71763f62a8f2a38cb1eb9dbee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9cf0f9c71763f62a8f2a38cb1eb9dbee.jpeg)
![images/f6a8a7befd2a18d928c240e5127b2c50.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6a8a7befd2a18d928c240e5127b2c50.jpeg)
![images/0d774ce69cbf3c27d9f1cee5172461d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d774ce69cbf3c27d9f1cee5172461d4.jpeg)

**"David Cook"**

---
---
**Phil Willis** *January 18, 2016 15:56*

Nice, will be following your adventures as I intend to do exactly the same.


---
**Stephane Buisson** *January 18, 2016 16:03*

haaaa the smell ! I know it too much, i need to find a way to exhaust through my London flat letter box ... and to silence my compressor, a couple of big fan to feed the Air assist would be enough ???


---
**ChiRag Chaudhari** *January 18, 2016 16:12*

Yup exhaust is the next thing in my list too. And because of that I have been only cutting plywood so far not acrylic. 

What exhaust system you are planning to get?


---
**Stephane Buisson** *January 18, 2016 16:16*

don't know yet, but one approach could be to use the same motor for exhaust and air assist ... for power efficiency.

need to make some time for myself.


---
**David Cook** *January 18, 2016 18:15*

possibly thinking of adding something like this Charcoal can filter  eventually.  [http://www.bghydro.com/can-lite-filter-6in-600cfm.html?utm_source=google_shopping](http://www.bghydro.com/can-lite-filter-6in-600cfm.html?utm_source=google_shopping)


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/9fZALhnev3g) &mdash; content and formatting may not be reliable*
