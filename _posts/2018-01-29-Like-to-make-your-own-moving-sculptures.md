---
layout: post
title: "Like to make your own moving sculptures?"
date: January 29, 2018 15:13
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Like to make your own moving sculptures? 

Here is the program for you!

Best yet, it is free!



Linkage Mechanism Designer and Simulator



[http://blog.rectorsquid.com/linkage-mechanism-designer-and-simulator/](http://blog.rectorsquid.com/linkage-mechanism-designer-and-simulator/)





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *January 29, 2018 15:49*

I found it a bit tricky to get started but have found it invaluable for envisioning linked mechanics. Somewhere I did a model for a k40 scissors lift?


---
**greg greene** *January 29, 2018 16:27*

Seems like the documentation link is broken


---
**HalfNormal** *January 29, 2018 17:05*

I was able to save the documentation

Try this:

[http://www.rectorsquid.com/linkage.zip](http://www.rectorsquid.com/linkage.zip)


---
**greg greene** *January 29, 2018 17:07*

O got this

The document name you requested (/linkage.pdf) could not be found on this server. However, we found documents with names similar to the one you requested.

Available documents:



/linkage.zip (common basename)

/linkage.msi (common basename)


---
**HalfNormal** *January 29, 2018 17:08*

**+greg greene** see corrected link above


---
**greg greene** *January 29, 2018 17:09*

thanks !


---
**Anthony Bolgar** *January 29, 2018 17:44*

Thanks for sharing


---
**HalfNormal** *January 29, 2018 17:46*

**+Don Kleinschnitz** There is a scissor lift example under samples at the end of the page.


---
**Tony Sobczak** *January 29, 2018 20:25*

Following 


---
**Ned Hill** *January 31, 2018 13:46*

Thanks for sharing.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/YHQWhg6FC55) &mdash; content and formatting may not be reliable*
