---
layout: post
title: "I removed the original bed and removed the lousy clamp"
date: March 18, 2018 21:41
category: "Modification"
author: "James Rivera"
---
I removed the original bed and removed the lousy clamp. Then I used some foil tape rated for high temperature (I already had for use on the underside of a 3D printer heat bed) to cover the hole left by the clamp. On the flip side I put some cardboard that is the same thickness as the metal bed, then covered it up with more foil tape. Then I measured the distance from the floor of the case to the lens, subtracted the focal length, the bed thickness, and 6mm (the max thickness of most stuff I will cut, I think) and 3D printed some new stand-offs to replace the ones I removed. Works like a charm! That being said, I see now why people here use metal honeycomb beds. Eeeew. The residue is yucky and I can see it making the bottom of the work piece all dirty. I’ll probably see about getting some, then 3D printing some new standoffs for the height difference. In the meantime...it’s alive!!!!  🤓👍



![images/9b1383d49d9a148884e2f5ec53e2df87.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b1383d49d9a148884e2f5ec53e2df87.jpeg)
![images/2a9d66e6460f9efe895e7119dc314c11.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2a9d66e6460f9efe895e7119dc314c11.jpeg)
![images/3981f4d62e270c6da1a0271a745cadd7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3981f4d62e270c6da1a0271a745cadd7.jpeg)
![images/6b923a7a032acd2d5155c0d6fb39b762.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b923a7a032acd2d5155c0d6fb39b762.jpeg)
![images/6f02dd04aa0f54c2dd8473efb2ef4e77.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6f02dd04aa0f54c2dd8473efb2ef4e77.jpeg)

**"James Rivera"**

---
---
**James Rivera** *March 19, 2018 04:59*

BTW, this is the foil tape I used.![images/126b7f217d73177b0efc6bcfdfe0ca81.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/126b7f217d73177b0efc6bcfdfe0ca81.jpeg)


---
**Phillip Conroy** *March 19, 2018 08:08*

My half blade and half pin bed ![images/60922f0f39c2dc6684ac073eccab2b18.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60922f0f39c2dc6684ac073eccab2b18.jpeg)


---
**Ned Hill** *March 19, 2018 15:31*

On your bed height I would recommend only subtracting 3mm for material thickness if you are going with a fixed height.  That way if you are cutting something 6mm then the focus is 1/2 way into the piece which is optimum.  Then for thinner things you are still close enough that it wouldn't make a huge difference.  


---
**James Rivera** *March 19, 2018 16:56*

**+Ned Hill** you know, that was my original intent; I’ll have check my math. I may have already done it that way. If not, adding 3mm under the work piece is a lot easier than taking it away. 🙃


---
**James Rivera** *March 19, 2018 17:00*

**+Phillip Conroy** if I already had a bunch of old hacksaw blades lying around, I’d probably do that, too. For now, I think I’m going to see how well I can get the height and cutting settings dialed in first. Second, actually. I need to get my exhaust finalized so I can try engraving/cutting acrylic—without poisoning myself!


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/CqVJgpEv849) &mdash; content and formatting may not be reliable*
