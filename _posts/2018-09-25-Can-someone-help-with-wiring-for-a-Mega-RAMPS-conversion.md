---
layout: post
title: "Can someone help with wiring for a Mega/RAMPS conversion?"
date: September 25, 2018 21:07
category: "Modification"
author: "Dan Evans"
---
Can someone help with wiring for a Mega/RAMPS conversion?



I've uploaded GRBL-mega to the Arduino fine, and can control X and Y gantry, but I can not get the laser to fire.



I've seen several ways to wire the laser LPS to the RAMPS, remove pot and wire into the IN on the LPS, and wire L to laser fire, but I'm confused by all the different information.



I'd like to keep the pot in, so I can set that for 100% power and not worry about running too much current through the tube.



I <i>think</i> i need 2 wires from the LPS, fire and PWM, so if someone could point out how they should be connected, I'd appreciate it.





**"Dan Evans"**

---
---
**HalfNormal** *September 25, 2018 21:45*

This link will explain what you need to know. Read the whole article. 

[http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html?m=1](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html?m=1)


---
**Dan Evans** *September 25, 2018 22:21*

**+HalfNormal** Yeah, that's the post I was following. Although it refers to a Smoothie board, I tried to use it to connect my RAMPS.



I connected the L connection to D8 (+) on the RAMPS, which is the default PWM connection, and a ground to D10 (-).



Couldn't get it to fire, so I guess I've not connected it right.


---
**HalfNormal** *September 25, 2018 22:36*

You need a positive voltage enable not ground. The pwm is for control.


---
**Dan Evans** *September 25, 2018 23:14*

That's the part I'm lost on. The linked article shows 2 wires, PWM and a ground.


---
**HalfNormal** *September 26, 2018 00:23*

here are a couple more links



[github.com - presentations](https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion)



[https://www.instructables.com/id/Goodbye-Moshi-or-How-Run-Your-Laser-Printer-on-Ard/](https://www.instructables.com/id/Goodbye-Moshi-or-How-Run-Your-Laser-Printer-on-Ard/)


---
**Dan Evans** *September 26, 2018 08:46*

I've read them. They both use Turnkey, rather than GRBL, remove the pot, and don't connect to the L pin.


---
**Joe Alexander** *September 26, 2018 08:55*

to control the laser you need a pin that can toggle the L wire to ground, usually done through a MOSFET. So I would imagine that you would need the L wire and gnd from the laser PSU to the RAMPS board, but I'm not sure which pin is capable of that but if you are then that should answer your question. (whichever pin it is is essentially acting as a switch, toggling the laser on/off)


---
**Don Kleinschnitz Jr.** *September 26, 2018 14:33*

Assuming this is a K40: 

I would first prove the L pin on the LPS works by grnding the L pin on the LPS with all the interlocks closed and "Laser Switch" on. Warning! This should fire the laser. 



Then you need to find a open drain output on the controller that is PWM capable and connect it to L. I think most people use the RAMPS heater/fans outputs.



Then you need to insure that the controller output you chose is enabled and set to the right polarity using whatever means the controller uses to setup the I/O. The ON condition of the PWM should grnd the L pin.



You can use one or two outputs to control the laser:

-Controlling the L pin with a PWM signal is sufficient as the K40 control panel provides the "laser enable" function already wired into the LPS. 



-Some add another "fire enable" controlled from the software but the controller output must be compatible with the input you choose to control on the LPS. 



I would stick with only controlling the supply with PWM on the L pin its a lot simpler. Leave the LPS wired as it came stock with the pot and switches.



What manuf. RAMPS board are you using?

Do you have a schematic of the controller board??

Post a picture of the LPS connectors.


---
**Dan Evans** *September 26, 2018 16:54*

Yes, it's a K40 with the analogue controls (blue box, 1k pot for laser power). The LPS is a GGGG type, green plugs, with green LED next to the test button. 



RAMPS is a Geetech board, and the Mega is running grbl-mega. The cpu_map.h maps pwm to D8. To allow for 24v operation, diode D1 is removed.



[https://reprap.org/wiki/File:RAMPS1.4schematic.png](https://reprap.org/wiki/File:RAMPS1.4schematic.png)



On the LPS, I've not touched the AC plug, or the 6 pin middle plug, which are as normal. The 4 pin right hand connector (that usually goes to the M2 Nano) is the one I've made connections to.



I've fed 24v and ground to the lower 5A power input on the RAMPS, nothing in the 5v, and the L pin to D8 connection.



If I press the test fire button on the case, the laser fires. If i press the test button on the LPS, nothing. If i try and fire the laser from software, nothing.

[reprap.org - File:RAMPS1.4schematic.png - RepRap](https://reprap.org/wiki/File:RAMPS1.4schematic.png)


---
**Don Kleinschnitz Jr.** *September 27, 2018 00:49*

<b>"If I press the test fire button on the case, the laser fires. If i press the test button on the LPS, nothing. If i try and fire the laser from software, nothing."</b>



The above statement suggests strange operation. 

The button down on the LPS should fire the laser no matter what? 



Its not clear why the TEST button on the control panel would work and the one on the supply would not. I would expect the other way around.



On the LPS:

Does the test button work if you disconnect all wires from the controller to the LPS other than power.



Does the laser fire if you ground the L pin on the LPS. 



I need to understand better exactly how you have the LPS connected to the controller.



1. D8 or P$2 is connected to L on the LPS?

2. The 24V from the LPS feeds the Ramps?

3. There is a ground from the LPS to the Ramps?

4. No other connections from LPS to Ramps?



When you run a test job does LED2 (seen below) illuminate? 

Did you re-assign Laser PWM to D8 in the firmware?



![images/05098d629a3adcf4220b3dcad94450ab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05098d629a3adcf4220b3dcad94450ab.jpeg)


---
**Kelly Burns** *September 27, 2018 02:37*

I think it’s another situation where the connections for laser on/off and the fire/test switch are reversed.  I have now seen it 4 times including with my own.   


---
**Don Kleinschnitz Jr.** *September 27, 2018 14:00*

**+Kelly Burns** interesting, are you using the same setup. Can you add some color? Are you referring to the control panel controls or the controller controls?


---
**Dan Evans** *September 27, 2018 19:30*

So, what I've tried today is: 



Revert back to stock wiring. Laser fires through software and on front panel test button. Does NOT fire on LPS button, but I can live with that.



Grounded L on 4 pin LPS connector. Laser fired as expected.



Measured RAMPS D8 to Q3 drain - good circuit. Q3 Source to ground - good circuit. 



Connected 24v to RAMPS input. Connected L to D8 (Q3 drain). Connected 4 pin LPS connector ground to a RAMPS ground.



Fires on front panel test button, but not software. LED 2 did not light when test button was pushed, however this takes +ve supply from 12V2 (the 11A supply) which wasn't connected.



I changed the RAMPS config to use D9, which uses the +12V supply that is connected (24v from the LPS actually).



This time, when the test button is pressed LED3 lights up, and the laser fires. No fire from software though.



Back to stock, K40Whisperer works fine.


---
**Kelly Burns** *September 27, 2018 19:44*

**+Don Kleinschnitz Jr.** Sorry for the late reply.  My Google+ app wouldn't work on my phone.



I was speaking of the Stock Switches on the Control Panel.  



Dan Testing of putting stock controller back in showed that my thought was incorrect in this case.



Basically, when this has happened to people with Stock K40, the test works because Both switches are closed when doing a test, but when firing through the controller, nothing happens.  this is because the Momentary Test Fire switch is actually the "Laser Enable" switch.   If you turn the Laser Enable switch off and hold the TEST button down while the program is running, it will "fire" as expected.





In 3 of the cases including mine, the Lasers were purchased from someone who couldn't get them working properly.  In a 4th case the person had received from Amazon.  Tested fine with switch, but when a program was run, the head would move, but no laser.












---
**Kelly Burns** *September 27, 2018 20:33*

**+Dan Evans** **+Don Kleinschnitz Jr.**   I don't see where this suggestion test of Don's was completed: 



 "I would first prove the L pin on the LPS works by grnding the L pin on the LPS with all the interlocks closed and "Laser Switch" on. Warning! This should fire the laser. "  



The fact that the On-board test button on the power supply doesn't work in STOCK Configuration is suspect in my opinion.  I'm wondering if the on-board TEST button on the LPS actually does what Don is suggesting above.  If it is, then its possible that the L input circuitry has a fault in it.   Just a thought....






---
**Dan Evans** *September 27, 2018 20:38*

4th line of my last post 😉


---
**Kelly Burns** *September 27, 2018 20:44*

OK, sorry I missed that.  Doesn't seem to make sense. 



I have never hooked mine to Ramps.  I have run Smoothieware and GRBL LPC on  Smoothie, MKS sBase and C3D.  Never had issue with getting L to work.  PWM.  



If you get adventurous again, let us know.  I really hate giving up on things like this. Seems like something simple.






---
**Dan Evans** *September 27, 2018 21:07*

I'll keep trying. I think it's something simple like my connections to the MOSFET, and the L not being grounded fir some reason. 



I'd rather get the RAMPS working because I have it lying around, and I don't have a Smoothie board handy.


---
**Don Kleinschnitz Jr.** *September 28, 2018 15:29*

Using your test results below:

We need to get consistent on our labels and configuration for each of these test so we can insure that I am not confusing the results. My guess is that there is some thing in the wiring or configuration that is keeping the L's PWM signal from proper control.



The LPS Test button should work. On these supplies the LPS Test overrides all other signals and acts like the "L". I think this is a hint?



Lets use these terms:

Control Panel Test Button = CP Test 

Laser Power supply test button = LPS Test

Laser enable button on K40 control panel = Laser Enable



For each test we need to verify configuration which is:

<b>Software:</b>

<b>Controller:</b>

<b>LPS L connection to:</b>

<b>Laser enable from controller:</b>



 

<i>Revert back to stock wiring. Laser fires through software and on front panel test button. Does NOT fire on LPS button, but I can live with that.</i>

Note: This is improper operation for the LPS.

<b>Software:</b> whisperer

<b>Controller:</b> Stock

<b>LPS L connection to:</b> stock controller

<b>Laser enable from controller:</b> none



<i>Grounded L on 4 pin LPS connector. Laser fired as expected.</i>

What was the configuration?

<b>Software:</b>

<b>Controller:</b>

<b>LPS L connection to:</b>

<b>Laser enable from controller:</b>



<i>Measured RAMPS D8 to Q3 drain - good circuit. Q3 Source to ground - good circuit.</i> 

Not sure what you mean. That D8 on the output is connected to Q3's drain?

Q3 source to to ground? The source on the mosfet to ground should read as open. It needs to be an "open drain". That means that the source is not connected to a pullup or ground or anything. The drain of the MOSFET should connect directly to L on the LPS?



<i>Connected 24v to RAMPS input. Connected L to D8 (Q3 drain). Connected 4 pin LPS connector ground to a RAMPS ground.</i>

Make sure that nothing else is connected to D8 especially a pullup to any voltage. I think this is configurable on the board?



<i>Fires on front panel test button, but not software. LED 2 did not light when test button was pushed, _*</i>[Which "test" button?]<b> however this takes +ve supply from 12V2 (the 11A supply) which wasn't connected._ *{The MOSFET must not have ANY voltage connected to its drain!]</b>

What is the configuration:

<b>Software:</b>

<b>Controller:</b>

<b>LPS L connection to:</b>

<b>Laser enable from controller:</b>



<i>I changed the RAMPS config to use D9, which uses the +12V supply that is connected (24v from the LPS actually).</i>



In our configuration there must be NO supply connected to the driving MOSFET's drain.



<i>This time, when the test button is pressed LED3 lights up, and the laser fires. No fire from software though.</i>

Is this the CP Test button? 



My guess is that the CP test button is driving LED3 to ground through the LPS this means that the CP test connection () and L are connected internally. This is normal for this supply.

The key question is why does D9's LED 2 not act the same?



<i>Back to stock, K40Whisperer works fine.</i>

That's good then we know we have a wiring or software config problem.



Next post will be a schematic that we can reference for better understanding.


---
**Don Kleinschnitz Jr.** *September 28, 2018 15:32*

This schematic illustrates my view of the wiring configuration including relevant internal circuits.

[https://photos.app.goo.gl/79QWbCqFVdYRRrVL6](https://photos.app.goo.gl/79QWbCqFVdYRRrVL6)



I suspect that there is something wrong with the way the PWM output from the RAMPS is configured. 



With the power on to the controller and D8/(P$2)/[L] disconnected from the LPS what does the voltage read to ground?


---
**Dan Evans** *September 28, 2018 19:38*

I'm working tonight, so I'll do some more testing tomorrow and let you know the results.


---
**Dan Evans** *September 29, 2018 21:31*

I'm leaning towards a wiring problem because with:



+24v and ground from LPS connected to 5A power input (X4-3 and X4-4 on schematic)

and L from LPS connected to D8



continuity measurement between D8 and ground is open circuit (as expected).



When I try and fire the laser from software the open circuit remains, i.e. the MOSFET doesn't change state at all.



On the Smoothie drawing, 



[2.bp.blogspot.com](https://2.bp.blogspot.com/-_c1g5Dnynmw/WCivKHGJHoI/AAAAAAAAgyE/nSLxxIP1cWofQcP742-0mm9moSmNP2v_ACPcB/s1600/20161113_105213.jpg)



there are 2 connections from LPS G, one to the main power connection, one to POWER IN 1 ground. I'm missing the second ground. 






---
**Joe Alexander** *September 30, 2018 04:37*

the mosfet you are connecting to L should have its output connected to ground so that when it changes state it grounds out the L signal. right everyone or am i crazy?


---
**Don Kleinschnitz Jr.** *September 30, 2018 05:07*

The mosfet your using should SWITCH to ground.

Confused why are you referencing smoothie when you are using Ramps/Mega?

My query above asked for the <b>voltage</b> on D8 not continuity??


---
**Dan Evans** *September 30, 2018 09:05*

There is no voltage on D8. I was measuring continuity to see if the MOSFET switched, but didnt see evidence that it did.



What ive done today is connect both power supplies to the RAMPS with no other connections (so the LEDS would light) and found that an M3 command did not switch Q3 at all.



I then looked at the code and it appears that the cpu_map is wrong as i managed to switch Q2 with an M3 code. Ive now changed the code and have an M3 switching Q3, which when connected to LPS L, turns the laser on, and is PWM controllable.



Using laserGRBL software I can M3 S1000 to get full power, S500 gives 50% etc, with maximum power being set on the front panel pot. In short, exactly what i was trying to achieve.



Thanks to everyone for your help.


---
**Don Kleinschnitz Jr.** *September 30, 2018 12:43*

**+Dan Evans** so you are fixed and the problem turned out to be a configuration change? 


---
**Dan Evans** *September 30, 2018 12:59*

It would appear so. The comments in the code state an input for D8, but changing that has fixed the problem


---
**HalfNormal** *September 30, 2018 17:23*

When you have time, it would be great to share the section you had to change in order to get the ramps to work with GRBL with examples.


---
**Dan Evans** *September 30, 2018 17:25*

Sure. I'm away with work until Friday but I'll post that info when I'm back


---
*Imported from [Google+](https://plus.google.com/111222365726671745706/posts/bkWLgTAC2Ck) &mdash; content and formatting may not be reliable*
