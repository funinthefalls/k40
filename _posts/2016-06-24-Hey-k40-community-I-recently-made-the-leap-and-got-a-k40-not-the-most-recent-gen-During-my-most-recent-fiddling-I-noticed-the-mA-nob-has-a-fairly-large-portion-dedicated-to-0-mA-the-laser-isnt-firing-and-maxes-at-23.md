---
layout: post
title: "Hey k40 community. I recently made the leap and got a k40 (not the most recent gen ;( ) During my most recent fiddling I noticed the mA nob has a fairly large portion dedicated to 0 mA (the laser isnt firing) and maxes at 23"
date: June 24, 2016 06:34
category: "Original software and hardware issues"
author: "Nick Lee"
---
Hey k40 community.

I recently made the leap and got a k40  (not the most recent gen ;(  )



  During my most recent fiddling I noticed the mA nob has a fairly large portion dedicated to 0 mA (the laser isnt firing) and maxes at 23 mA.  Does that seem consistent with everyone else?



It also came with some tape and a tube of... i think silicon....  did anyone else get these? or know what they are for?



Thanks for your help!





**"Nick Lee"**

---
---
**Jason He** *June 24, 2016 06:40*

Tape is used for aligning the laser. Don't know about the mystery tube though.


---
**Gee Willikers** *June 24, 2016 06:41*

0-5ma effectively does nothing because its not enough power to fire the tube. Never go above 18ma with a K40, you'll quickly destroy the tube.


---
**Nick Lee** *June 24, 2016 06:49*

**+Gee Willikers** Thanks for the heads up good to know.


---
**Nick Lee** *June 24, 2016 06:52*

Here is the mystery tube pic

[https://drive.google.com/file/d/0BxAWfVv0_HQKUExLTjVKV2dlY2kxWUJsMjU1SGp5Qm51VUNN/view?usp=sharing](https://drive.google.com/file/d/0BxAWfVv0_HQKUExLTjVKV2dlY2kxWUJsMjU1SGp5Qm51VUNN/view?usp=sharing)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 07:30*

The mystery tube is silicon. It's for if/when you need to replace your tube, so you can seal the wiring (as it's very high voltage). My machine has the pot for selecting mA & it also has a large 0 range. There was a discussion recently where it seems to be pretty common, albeit not exactly the same range though. I can't get my laser to fire below 4mA. Even though it may register on the ammeter it doesn't mark anything.


---
*Imported from [Google+](https://plus.google.com/107557500099114558538/posts/b7fqk91Kvi3) &mdash; content and formatting may not be reliable*
