---
layout: post
title: "As a comparison for the terrain scraper, the first one is the original MakeCNC plan, built in 3D"
date: September 16, 2016 21:59
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
As a comparison for the terrain scraper, the first one is the original MakeCNC plan, built in 3D. The second one below that is my modified version. While possibly more accurate to the real thing, it's also a lot more complicated to build. The front end loader model I did last year is there for size comparison.



![images/be800d3c810e27073fadb87ce3487d4f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/be800d3c810e27073fadb87ce3487d4f.png)
![images/201f6e5434ce11bc7d24ff57b3156d85.png](https://gitlab.com/funinthefalls/k40/raw/master/images/201f6e5434ce11bc7d24ff57b3156d85.png)
![images/9400d86391b1c8ea20f4295e9be23734.png](https://gitlab.com/funinthefalls/k40/raw/master/images/9400d86391b1c8ea20f4295e9be23734.png)

**"Ashley M. Kirchner [Norym]"**

---
---
**greg greene** *September 17, 2016 02:05*

a lot of different parts from the original - I just got the plane from MakerCNC - Definitely a lot of extra work - how much wood did you go through ?


---
**Ashley M. Kirchner [Norym]** *September 17, 2016 07:10*

Uh, I think 11x 8x12 sheets? 9 of them are 1/8" and 2 of them are 1/4". I nest the parts, sometimes within the void of others. For example, the inner circle of the tires have smaller parts in them. It cuts down on waste material and I get to use as much of it as I can.The pieces are placed 1mm from each other. When you take into account the laser kerf, there's even less material left behind after I pull the pieces out.



Also, keep in mind that I split them up. So I cut all of the bucket parts first ans assemble that. Then I cut all of the rear assembly (without wheels) and assembly and attach it to the bucket. Then comes the cab section, the hinge, and lastly the rear and front wheels. Doing it all at once just becomes a jigsaw puzzle with no instructions. :)


---
**greg greene** *September 17, 2016 14:11*

Thanks !  I also have a 3D router so I'll probably do a hybrid with the wheels and such on the router - should be fun !


---
**Ashley M. Kirchner [Norym]** *September 17, 2016 23:44*

That would be fun to try, absolutely. A router is another one of those wishlist items for me.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/ADTqpFs6CWz) &mdash; content and formatting may not be reliable*
