---
layout: post
title: "If I switched to smoothieware which board and our version is suggested"
date: March 29, 2016 18:07
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
If I switched to smoothieware which board and our version is suggested. I've seen several different ones and at different price points, please set me in proper direction.





**"Andrew ONeal (Andy-drew)"**

---
---
**Anthony Bolgar** *March 29, 2016 19:15*

Personally, I would buy an authentic Smoothieboard, as a way of supporting Arthur Wolf and others who made the Smoothieboard available in the first place. You can get one at [http://robotseed.com/index.php?id_lang=2](http://robotseed.com/index.php?id_lang=2) if you are in Europe or [http://shop.uberclock.com/collections/smoothie](http://shop.uberclock.com/collections/smoothie) in North America. The 5xc is the top of the line model with ethernet and a web interface. The 4xc would be my next choice, has one less stepper driver. The lowest cost one would be the 3xc, has only 3 stepper drivers and only connects via USB. But if you do not need the ethernet option, the 3Xc would be perfect for upgrading the K40 laser.


---
**Randy Randleson** *March 30, 2016 00:35*

I think maybe he means which one? 3, 4 or 5. Im not sure myself, Im still really new to this, but I would definately buy 2  #3's one for my Laser cutter and one for my 3d printer. I just need to figure out how to implement it




---
**Richard Vowles** *March 30, 2016 18:59*

You can only buy the 5, all the rest are out of stock.


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/ZrSviTEGPNy) &mdash; content and formatting may not be reliable*
