---
layout: post
title: "3d printer and laser cutter fun...."
date: March 08, 2016 03:38
category: "Object produced with laser"
author: "The Technology Channel"
---
3d printer and laser cutter fun....



![images/a576736ab54ac1d0bbe32b5c66dccfdd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a576736ab54ac1d0bbe32b5c66dccfdd.jpeg)
![images/ec08144a9c0781a9974d4aa70f1b8ef0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec08144a9c0781a9974d4aa70f1b8ef0.jpeg)

**"The Technology Channel"**

---
---
**David Cook** *March 08, 2016 05:54*

 Badass!! Nice work


---
**Scott Marshall** *March 08, 2016 18:29*

Foam? or printed, painted then etched?Painting process? Gotta know the details.﻿ Great effect.


---
**The Technology Channel** *March 08, 2016 19:07*

3d printed head took 20 hours...., base was laser cut, then I cut out the letters with the red acrylic and glued them in place, painted the head with plasicote paint primer, then plasicote chrome, added a bit of black and red oil paint.


---
**The Technology Channel** *March 08, 2016 19:08*

No etched, its 5mm acrylic black base, cut right through, did the same with red.


---
**Scott Marshall** *March 08, 2016 20:55*

I thought maybe you had engraved around the eyes etc.  -- Simpler than I thought. The chrome paint really looks nice on the extruded surface. Nice work. Thanks.


---
*Imported from [Google+](https://plus.google.com/105609664958110832961/posts/a8g4NeKPzLM) &mdash; content and formatting may not be reliable*
