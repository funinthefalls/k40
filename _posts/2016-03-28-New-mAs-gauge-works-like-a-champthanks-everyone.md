---
layout: post
title: "New mA's gauge works like a champ...thanks everyone"
date: March 28, 2016 21:18
category: "Modification"
author: "Scott Thorne"
---
New mA's gauge works like a champ...thanks everyone.

![images/6321fb3579d345e8a6fa1949f3b2818c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6321fb3579d345e8a6fa1949f3b2818c.jpeg)



**"Scott Thorne"**

---
---
**Phillip Conroy** *March 28, 2016 22:19*

please tell me that is not a laser tube poking out the rear right hand side,with 22,000 volts on it+++++++++++++++++++++++ which can jump  and kill ===============


---
**Scott Thorne** *March 28, 2016 22:39*

**+Phillip Conroy**...that it is...I removed the extension cover to run the gauge wires...relax...theven extension cover is back on now....good looking out though. 


---
**ThantiK** *March 28, 2016 23:23*

Got a link for the ammeter you bought?


---
**Scott Thorne** *March 28, 2016 23:28*

[https://www.amazon.com/gp/aw/d/B00CK5YJUA/ref=yo_ii_img?ie=UTF8&psc=1#](https://www.amazon.com/gp/aw/d/B00CK5YJUA/ref=yo_ii_img?ie=UTF8&psc=1#)


---
**ED Carty** *March 29, 2016 02:39*

Nice


---
**ED Carty** *March 29, 2016 02:41*

That would rock with a smoked lexan or plexi and some RGB leds in it. so when the numbers change so do the colors for whether its low right on or high amp pull..


---
**Phillip Conroy** *March 31, 2016 22:28*

only problem is with a fast changing reading you will not see the peaks ,i would still use the moving needle meter as well


---
**Scott Thorne** *March 31, 2016 22:57*

**+Phillip Conroy**...I'm good...I'm not going to overthink this...lol..but thanks. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/9QZkLv2thnU) &mdash; content and formatting may not be reliable*
