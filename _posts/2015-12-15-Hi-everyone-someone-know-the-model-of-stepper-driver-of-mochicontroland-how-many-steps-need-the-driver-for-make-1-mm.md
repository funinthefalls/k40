---
layout: post
title: "Hi, everyone, someone know the model of stepper driver of mochicontrol....and how many steps need the driver for make 1 mm ???"
date: December 15, 2015 12:08
category: "Hardware and Laser settings"
author: "Alessandro Zupo"
---
Hi, everyone, someone know the model of stepper driver of mochicontrol....and how many steps need the driver for make 1 mm ???





**"Alessandro Zupo"**

---
---
**Stephane Buisson** *December 15, 2015 13:10*

K40 is a generic name for a laser cutting machine.

it's in fact many different models and versions.

recently it's 2 electronic board providers (Moshi and nano)

so it's no way to answer your question.

same apply for motors and XY table.

as an indication for my mod with smoothieboard , I used 157.57steps per mm. ([http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide))

calculation is ratio (output /design)*actual step/mm


---
**Alessandro Zupo** *December 18, 2015 12:53*

ok, i understand. i will post soon a photo. thank you very much


---
*Imported from [Google+](https://plus.google.com/103855001849930569119/posts/JdkawXMs87U) &mdash; content and formatting may not be reliable*
