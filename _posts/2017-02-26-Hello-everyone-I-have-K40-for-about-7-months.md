---
layout: post
title: "Hello everyone. I have K40 for about 7 months"
date: February 26, 2017 22:18
category: "Discussion"
author: "Maja Velimirovic"
---
Hello everyone.

I have K40 for about 7 months. One week ago I have noticed that my lasertube is lossing power. I bought new and replaced it. 

Today, after the laser engaving something, I have noticed that it shows same loss of power. When I check laser beam at lasertube it shows multiple beam. When press test button, lasertube is slow, it does not start beam right away. 

Does anyone knows what can be the problem? Lasertube? Or maybe power supply? Or something else?

Thank you in advance





**"Maja Velimirovic"**

---
---
**Don Kleinschnitz Jr.** *February 26, 2017 23:02*

What current a max pot setting but dont run it long that way.


---
**Maja Velimirovic** *February 27, 2017 09:40*

Sorry, I do not understand 


---
**Don Kleinschnitz Jr.** *February 27, 2017 11:16*

1. What does the current meter read when you turn the pot all the way up and pulse the test button.

2. Any arcing?


---
**Maja Velimirovic** *February 27, 2017 15:40*

1. When I push test button scale goes to max. 

2. Yes, I have arcing but not to much.


---
**Don Kleinschnitz Jr.** *February 27, 2017 16:16*

Where is it arching. You have to stop the arcing as that can be part or all of your problem.

Can you post a video of the arc.


---
**Ned Hill** *February 27, 2017 18:03*

**+Don Kleinschnitz** I think he's thinking about tube ionization rather than arcing.  Since he's talking about multiple beams it sounds more like an optics issue.  His symptoms seem to be a bit mixed.  I agree that a video of what is happening would definitely be useful.


---
**Maja Velimirovic** *March 06, 2017 19:25*

+Dan Kleinschnitz **+Ned Hill** I have to thank you. I have changed my lasertube, supplier said that it was faulty tube. 


---
*Imported from [Google+](https://plus.google.com/110563519336458822965/posts/douGdyz6MNe) &mdash; content and formatting may not be reliable*
