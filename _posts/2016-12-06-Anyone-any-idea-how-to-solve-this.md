---
layout: post
title: "Anyone any idea how to solve this?"
date: December 06, 2016 07:58
category: "Software"
author: "Eoin Kirwan"
---
Anyone any idea how to solve this? 

![images/707d606bc4eb1dee5920c76777c2f6a1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/707d606bc4eb1dee5920c76777c2f6a1.jpeg)



**"Eoin Kirwan"**

---
---
**Mauro Manco (Exilaus)** *December 06, 2016 08:28*

Open  your k40 read on board serial and update that on corellaser.


---
**Mauro Manco (Exilaus)** *December 06, 2016 08:58*

if u use multiple k40(as me) and only one notebook...write board serial on top of k40  :) you need change that everytime


---
**Mauro Manco (Exilaus)** *December 06, 2016 09:01*

Engrave-> device inizialize-> device id ...close and re-open software and use k40


---
**Alex Krause** *December 06, 2016 17:07*

Install a smoothie board...


---
**Eoin Kirwan** *December 06, 2016 17:42*

**+Alex Krause** where do I get a smoothie board? 


---
**Jonathan Davis (Leo Lion)** *December 06, 2016 23:20*

**+Eoin Kirwan** [http://cohesion3d.com/](http://cohesion3d.com/)

[http://smoothieware.org/](http://smoothieware.org/)


---
**Eoin Kirwan** *December 11, 2016 21:26*

Is a smoothie board a physical component or software? 


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 21:26*

**+Eoin Kirwan** Physical.


---
**Eoin Kirwan** *December 11, 2016 21:26*

Expensive? 


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 21:28*

**+Eoin Kirwan** Somewhat but in the long term its worth the money


---
**Eoin Kirwan** *December 11, 2016 21:30*

Is it absolutely necessary? Or can I do as Mauro suggests? 


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 21:31*

**+Eoin Kirwan** Your call on that one.


---
**Eoin Kirwan** *December 11, 2016 21:31*

By just putting the device Id into coral laser? 


---
**Eoin Kirwan** *December 11, 2016 21:32*

I'll see if putting the code into coral laser works! 


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 21:34*

**+Eoin Kirwan** for smoothie, you would use LaserWeb3. which is way better to use as your machine as i have heard can come with a illegal copy of the corel program.


---
**Eoin Kirwan** *December 11, 2016 21:35*

Ok! Thanks! Where are you based? Visit my website.... [Www.handcraftedpens.ie](http://Www.handcraftedpens.ie).


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 21:37*

**+Eoin Kirwan** I'm in texas but i am just a user of the smoothieware system not a reseller.


---
**Eoin Kirwan** *December 11, 2016 21:39*

Cool!! I may ask you more questions in the future!! 


---
**Eoin Kirwan** *December 11, 2016 21:39*

Thanks 


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 21:39*

**+Eoin Kirwan** Your welcome


---
**Eoin Kirwan** *December 11, 2016 21:45*

Any idea how to input an input an image to Newlyseal and cut it? 


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 21:47*

**+Eoin Kirwan** No idea


---
**Eoin Kirwan** *December 11, 2016 21:47*

Ok


---
**Eoin Kirwan** *April 12, 2017 17:19*

Mauro, Is Corallaser the best package provided for the K40 laser engraver? I also have WinsealXP and Laser DRAW. What are these?




---
*Imported from [Google+](https://plus.google.com/103438187016886371314/posts/GFxemq2Yt7g) &mdash; content and formatting may not be reliable*
