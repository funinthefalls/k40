---
layout: post
title: "A while ago trying a simple alignment check after depressing Test for a brief I've heard a crack/pop sound..."
date: September 01, 2016 20:05
category: "Discussion"
author: "Mircea Russu"
---
A while ago trying a simple alignment check after depressing Test for a brief I've heard a crack/pop sound... (My heart stopped for a second) but everything went back to normal afterwards. 

Today I've tried some engraving and in when doing some fine detail that crack/pop sound again. It's just like an arcing but only when briefly powering the tube, all is good when cutting at constant power for more than a few milliseconds. Did try to depress the Test a few times and I got the sound again when depressing really fast.

I can see no trace of arcing at the positive pole of the tube, nor along the positive wire. It must be inside the LPS. The machine is grounded by the European Schuko socket and the flat's electric system, no way for me to get real ground (by sticking a rod in the ground).

Should I order another LPS just in case?





**"Mircea Russu"**

---
---
**greg greene** *September 01, 2016 21:13*

the HV lead on the anode of the laser tube is simply wrapped on and held with silicone.  It CANNOT be soldered as the heat will break the glass on the tube.  Every now and then there will be an incident where the HV lead comes away from the lead, with the MACHINE OFF AND UNPLUGGED and your trusty tube of silicone in hand (They should have sent you one, but you can get 100% CLEAR silicone from any hardware store), re wrap the lead on tight, apply silicone and all should be well


---
**Scott Marshall** *September 01, 2016 22:28*

Arcing is the jumping of the high voltage to ground, and isn't caused by a bad connection at the tube (unless it has fallen right off, which doesn't seem the case). 



Power down, unplug and make safe the machine, then clean the  area around the power connection with a paper towel and WD-40 or a similar product. What happens especially in humid weather or if you are chilling your water a little too cool is the dirt attracted by the High voltage becomes damp and will act as a conductor, encouraging the arcing.

The wiping with a light oil removes the dirt and displaces the moisture. If it's arcing from the factory connection, there may be a 'leak' in the factory RTV insulation. A cleaning to remove the oil with ipa and a fresh coat of rtv over the factory connection will fix it for good. No need to remove the factory connection unless it's obviously flawed. They seem to be done fairly well in most cases.



After cleaning the tube, wipe the length of the high voltage wire with the same cloth and inspect it for cracks, pinholes, or dark spots.



Since you can make it actually happen, you may want to inspect the electrical compartment under dim lighting and make it arc using the test button. Keep hands well away while it's on, if it arcs to you, it can be a fatal shock. The High voltage in these is not like a sparkplug wire, it carries enough current to kill.



If you do see a blueish-purple arc occur, make a note of the location, power down, safe the machine and inspect. Usually a bad wire will show a small black dot at the hole or crack. If it's the wire (probably jumping to a metal chassis part) you should replace the bad section of wire or repair it with a special self vulcanizing tape or vinyl tubing and RTV silicone.



If it sounds like it's in the power supply, as you suspect it is either the wire arcing tot he power supply case, or the transformer. You can temporarily remove the cover (in the safe condition) and run th edark room test to see if you can identify the source.



If it's the transformer (usually a white semi-cylindrical device about the size of a D battery), it may be possible to repair it, but a new PSU would be the best choice. If it's the wire, the repair is a layer of good quality self vulcanizing tape, then RTV/vinyl tubing sleeve over that. 

If there's room, you may be able to install a HV quick connect in the bad section of wire.



Hope it helps.





Scott



If you do need a PSU, I'm now stocking them here in the USA, and have upgraded versions with built in quick connects so you won't have to foll with the tube connection.












---
**Mircea Russu** *September 02, 2016 05:01*

Thanks for your answer guys. I shall try to find the spot by using a camera in the LPS compartment, I think is safer. The weirdness is that is doing it only on very small pulses, like "not enough power to light the tube but this energy has to go somewere" case and not on high power continous cuts. 


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/6FSPGqWQjmN) &mdash; content and formatting may not be reliable*
