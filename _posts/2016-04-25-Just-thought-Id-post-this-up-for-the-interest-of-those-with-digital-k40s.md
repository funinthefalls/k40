---
layout: post
title: "Just thought I'd post this up for the interest of those with digital k40's"
date: April 25, 2016 00:17
category: "Hardware and Laser settings"
author: "I Laser"
---
Just thought I'd post this up for the interest of those with digital k40's.



David Richards initially posted this, his power/percentage settings are in blue, mine are purple. He measured his at multiple points, whereas mine is through a single mA meter.



Also note, you shouldn't be pushing more than 18mA if you value your tube. :)

![images/9585d70e705dbbc6136a401530b731aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9585d70e705dbbc6136a401530b731aa.jpeg)



**"I Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 01:36*

It's interesting that there is such a huge difference as you get past 15%. e.g. 20% varies by ~4mA, 50% by nearly 5mA, 80% by somewhere around 7mA difference. Seems that each machine is about as unique as their users.


---
**I Laser** *April 25, 2016 03:26*

To be honest I'm not sure what to make of the difference, I believe David tested his from multiple points, whereas mines just off the mA meter.



Seeing as mine reads higher I'm just using that, if it were the other way around I'd probably be nervous enough to   perform more testing lol. ;)


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/Tor1F38FaNw) &mdash; content and formatting may not be reliable*
