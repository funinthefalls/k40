---
layout: post
title: "Hi, is there any way to visually identify the type of plastic I am using under the laser, I have heard people say stay away from PVC but if its not marked how do you know?"
date: October 12, 2015 13:27
category: "Materials and settings"
author: "Peter"
---
Hi, is there any way to visually identify the type of plastic I am using under the laser, I have heard people say stay away from PVC but if its not marked how do you know?



I ask that question because I've just got my cheapo 40w Chinese laser set up and I'm trying to find out what it can do. The picture is of two pieces of 2 or 3mm thick plastic (dont know what type) separators used in tool boxes. Tried with the same power and speed, 5mm/s,  to cut a star but with not very good results. I will not be testing with the same stuff again, it was the first thing I could find and didnt think about the material to start with. 



The black took over 10 passes at /possibly/ 3mA, the dial is so twitchy that I hardly touch it and it jumps three to five points on its own. The white took two passes on the same setting and cut out the shape but obviously not very neatly. 



I have aligned as best I can, brought the top of the material as close to the focus point as I can and have some air assist from an airbrush pump aimed at the focus point. 



Any warnings or advice to better cut shapes from plastic?



Thanks, 

Peter

![images/f19b1412ab82c6ef60bf635245452c9b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f19b1412ab82c6ef60bf635245452c9b.jpeg)



**"Peter"**

---
---
**Anthony Bolgar** *October 12, 2015 17:52*

The laser will not fire reliably at 3mA. Turn it up to 6 or 7mA and you will get much better results.


---
**Joey Fitzpatrick** *October 12, 2015 17:56*

acrylic will cut cleanly.  PVC , Polyethylene ect.. will melt.  Like the plastic in your photo.  Some can give of poisonous and corrosive gasses


---
**Peter** *October 12, 2015 18:47*

Thanks for the replies! :)



I have no acrylic for the moment. I'll be scouring ebay etc for some cheap suppliers or for someone getting rid of scraps for now.



I went from this test to some 1mm foam sheet. turned the power down further but the cut seemed pretty wide. Probably melt rather than cut. I have a Mr Beam (kickstarter project) with a 1W laser for lighter stuff.



I could not get the potentiometer to set any power setting with any accuracy. It jumps all over the place. My dial goes from 0 - 30mA but it looks like the pot was made for something with a much bigger range. It has "WTH118-1A 2W 1K" printed on the back of the pot... is this what its range is, 0-1000mA (0-1A)? No wonder I cant adjust it properly. Any way to swap this out for something I can actually move more than a fraction of a degree at a time?



I have put the dodgy plastic aside and wont be using it again. 

Need to source some cheap scraps while im still burning/melting stuff up.



Anyone got any tips on making the thing a bit more airtight? Cant stand the smell of burnt flesh (leather!) in this room for much longer.


---
**Peter** *October 12, 2015 19:12*

Thanks, AG.

I have a ream of paper standing by and ive been using that to work out the work area for a job. Cut the paper with a "preview" run and place the material inside the cut. 

I have tried to adjust the power during cuts - I realised quite quickly on some test engravings that the tube is firing on and off frequently. It was a cut job I was trying to adjust earlier. Its like the pot is dirty inside and wont fine tune, like old rotary volume dials on a walkman would crackle if you didnt use them for a while. 

Ill keep playing and see what works.


---
**Scott Marshall** *December 18, 2015 17:06*

PVC Burns ( forms a black film that crazes when hit with a torch, Polypro waters (leaves streaks on the surface, and polyethylene melts and smells like a candle. 



PVC is noticably heavier then the other 2, and sinks while the other 2 float.



PVC is harder and 'dry' when scratched by dragging a knife along an edge, PE and PP are soft and malleable when scraped.



A piece of PVC welding will stay bent if bent, the others spring back.



I worked in a plastics fab shop years ago, welding together Lab fume hoods and semiconductor process benches.



Those are the rules we gave the newbies, after a while you can tell at a glance. 



Hope it helps.

Scott


---
*Imported from [Google+](https://plus.google.com/+PeterJones79/posts/j6bsTL5JCmf) &mdash; content and formatting may not be reliable*
