---
layout: post
title: "I have been having a look around this morning on ebay & the likes for some components for monitoring the temperature of my water going into the tube & as it gets out of the tube"
date: December 26, 2015 02:58
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
I have been having a look around this morning on ebay & the likes for some components for monitoring the temperature of my water going into the tube & as it gets out of the tube. I am trying to find something that is either located in Australia or ships here free (or relatively cheaply). Anyway, I came across this seller who sells the following things that I thought might be useful to me & anyone else.



[http://www.ebay.com.au/itm/DC-12V-10A-Intelligent-Digital-Display-Thermostat-Temperature-Controller-Switch-/281842870176?hash=item419f24e7a0:g:GMgAAOSw1vlUrOQu](http://www.ebay.com.au/itm/DC-12V-10A-Intelligent-Digital-Display-Thermostat-Temperature-Controller-Switch-/281842870176?hash=item419f24e7a0:g:GMgAAOSw1vlUrOQu)



& this other one



[http://www.ebay.com.au/itm/Digital-Time-Voltage-Dual-Temperature-Display-Module-for-Car-interior-wate-tank-/281856099378?hash=item419feec432:g:Fm8AAOSwFnFV9s81](http://www.ebay.com.au/itm/Digital-Time-Voltage-Dual-Temperature-Display-Module-for-Car-interior-wate-tank-/281856099378?hash=item419feec432:g:Fm8AAOSwFnFV9s81)



Anyone (more electronically-minded than me) know whether it's worth giving these a try?





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Phillip Conroy** *December 26, 2015 05:36*

The small glass tube output end is where i cracked my tube when i forgot to turn on the water pump,i find that 15 to 20 deg is the best temp 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2015 07:12*

**+Phillip Conroy** That's unfortunate that you cracked your tube. I thought I best start sorting out some sort of temperature monitoring as it is getting well & truly into summer here & looking to get 40 degree C days I imagine.


---
**Scott Marshall** *December 26, 2015 13:24*

Here's what I ordered for mine. Not needing  control, these will give cheap monitoring once the thermistors are fitted into a couple of plastic barb tees.



You get a temp in and a temp out for under 10 bucks. I made my living in industrial controls for over 40 years, and just couldn't justify fancier equipment.



 I do have an old Ranger data logger I may hook to it temporarily to make sure there no spikes or odd happenings going on, but once that's done, the only other thing I might want would be an overtemp/low flow alarm/shutdown.



[http://www.ebay.com/itm/371108188641?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/371108188641?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Richard Vowles** *December 26, 2015 18:17*

You can also get them on aliexpress for around $5.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2015 22:48*

**+Scott Marshall** Thanks Scott. I saw that style of temperature gauge while I was looking around too. As **+Richard Vowles** says, you can find them on aliexpress cheaper. I actually found them on aliexpress for about $1 each.

[http://www.aliexpress.com/item/Digital-LCD-Probe-Fridge-Freezer-Thermometer-Thermograph-Meter-for-Refrigerator-110C-Black-White/32455167342.html?spm=2114.01020208.3.91.CDHmGb&ws_ab_test=searchweb201556_2,searchweb201644_4_79_78_77_82_80_62_81,searchweb201560_5](http://www.aliexpress.com/item/Digital-LCD-Probe-Fridge-Freezer-Thermometer-Thermograph-Meter-for-Refrigerator-110C-Black-White/32455167342.html?spm=2114.01020208.3.91.CDHmGb&ws_ab_test=searchweb201556_2,searchweb201644_4_79_78_77_82_80_62_81,searchweb201560_5)


---
**Phillip Conroy** *December 27, 2015 05:36*

Water flow is more impotant than temp as a tube will only last 10 min with no water flow and high temps will just lower the laser output power temparaly )


---
**Phillip Conroy** *December 27, 2015 05:36*

[www.ebay.com.au/itm/White-Plastic-Shell-Magnetic-Water-Flow-Switch-w-Inner-Outer-Thread-SYSZAU-/252199463581?hash=item3ab8426a9d:g:QnoAAOSwHQ9WYlJc](http://www.ebay.com.au/itm/White-Plastic-Shell-Magnetic-Water-Flow-Switch-w-Inner-Outer-Thread-SYSZAU-/252199463581?hash=item3ab8426a9d:g:QnoAAOSwHQ9WYlJc)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 27, 2015 10:13*

**+Phillip Conroy** Thanks Phillip. Does that water-flow switch reduce the water-flow at all? I am unsure from the picture what is actually inside it. Looks like a good investment though. As you said, no water flow is going to be a serious problem.


---
**Phillip Conroy** *December 27, 2015 13:10*

The water flow switch  lowers the flow,i just used a better bigger pump,ps water tank must be same level as the laser cutter to prevent air pockets,and water flowing out of tube when pump is off


---
**Phil Willis** *January 04, 2016 10:58*

**+Phillip Conroy** After doing about 4 hours worth of cutting I noticed that the performance of my K40 dropped considerably. The water exiting the unit was warm to the touch and the reservoir was lukewarm. Do you know what sort of performance impact I should expect? I was finding that it was unable to cut 3mm acrylic even at 20mA.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/EGCyuQLxM6p) &mdash; content and formatting may not be reliable*
