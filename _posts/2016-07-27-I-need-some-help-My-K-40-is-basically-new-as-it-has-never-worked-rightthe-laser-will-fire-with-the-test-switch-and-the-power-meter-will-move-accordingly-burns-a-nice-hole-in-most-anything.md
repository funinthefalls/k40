---
layout: post
title: "I need some help. My K-40 is basically new as it has never worked right....the laser will fire with the test switch and the power meter will move accordingly (burns a nice hole in most anything)"
date: July 27, 2016 16:55
category: "Original software and hardware issues"
author: "Harold Oney"
---
I need some help.  My K-40 is basically new as it has never worked right....the laser will fire with the test switch and the power meter will move accordingly (burns a nice hole in most anything).  when i send something to the cutter, unless i set the speed at something ridiculous like 5mm/s  the laser will not work.  the cutter head moves but no laser is emitted and the power meter barely bumps no matter what the power setting is.  Any one had this problem or can point me in a direction.





**"Harold Oney"**

---
---
**greg greene** *July 27, 2016 17:12*

using corel I have had the same thing - using LaserDRW - it all worksw


---
**Harold Oney** *July 27, 2016 17:15*

Unfortunately, I have tried both and it still happens.  I am not sure what the deal is.


---
**greg greene** *July 27, 2016 17:19*

some have had problems with the power setting pot, try moving it just a little


---
**Ariel Yahni (UniKpty)** *July 27, 2016 17:19*

Make sure you select the correct board number in plugging setting. Usually ends in M2


---
**Ariel Yahni (UniKpty)** *July 27, 2016 17:19*

Make sure you select the correct board number in plugging setting. Usually ends in M2


---
**Jim Hatch** *July 27, 2016 17:56*

And the device ID has to be set correctly too. 



Also, they have to be set in both LaserDRW and CorelDraw settings.



Lastly, someone here found there was an order that was needed in Corel - either set first picking engrave to get to the settings or first by picking a cut to get the settings right. Then it "took" but if you did it in the other order the software doesn't save the settings correctly. Anyone remember who found that out?


---
**HalfNormal** *July 27, 2016 20:37*

I did have a bad Nano board that would not fire correctly.


---
**Jim Hatch** *July 27, 2016 21:14*

Harold, have you told the seller and gotten them involved in fixing the problem? If it's a bad Nano board or power supply they can replace just the component. If you've done all that we've recommended then you can also push for them to replace the whole machine.


---
**Harold Oney** *July 27, 2016 22:49*

Jim,

I have shared it with them.  Sent them a video of the issue as they requested.  After 5 months of noncommittal responses, i finally filed a claim with PayPal to get a refund and they denied the claim with no explanation.  the machine is still under warranty.


---
**HalfNormal** *July 28, 2016 01:16*

I was never able to get my seller to send a new board. They said they sent one but it was returned ect ect. I finally bought one and everything has been fine since.


---
**Jim Hatch** *July 28, 2016 01:37*

**+Harold Oney**​ that sucks. PayPal is worth arguing with but I expect they'll say the time is too long. Usually if you have an issue you want to give them one chance and then file the claim. They will try to run the clock otherwise. Pushed hard & early and they'll do things like refund significant $ ($200) or send a new tube, etc. But you're better off getting money and then getting the parts.


---
**Ariel Yahni (UniKpty)** *July 28, 2016 01:44*

Indeed they will. I got 300 for a 340 order on a bad tube


---
*Imported from [Google+](https://plus.google.com/106457360071444565259/posts/gdti85AajDn) &mdash; content and formatting may not be reliable*
