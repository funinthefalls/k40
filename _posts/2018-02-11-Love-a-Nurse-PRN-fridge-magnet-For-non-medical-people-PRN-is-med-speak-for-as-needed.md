---
layout: post
title: "Love a Nurse PRN fridge magnet For non medical people, PRN is med speak for as needed"
date: February 11, 2018 00:34
category: "Object produced with laser"
author: "HalfNormal"
---
Love a Nurse PRN fridge magnet



For non medical people, PRN is med speak for as needed.

![images/7aec0c5a9ddfb6d13fa3f7362a768691.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7aec0c5a9ddfb6d13fa3f7362a768691.jpeg)



**"HalfNormal"**

---
---
**Ned Hill** *February 13, 2018 16:11*

Love it.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/7WFUUdPdmDD) &mdash; content and formatting may not be reliable*
