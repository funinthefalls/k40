---
layout: post
title: "Does anybody use activated carbon filters in there machine I am thinking about getting a few pads to throw in my machine to help with some of the lingering Odors it's not bad but I do get a wiff of lingering wood smell at"
date: July 05, 2016 16:52
category: "Discussion"
author: "3D Laser"
---
Does anybody use activated carbon filters in there machine I am thinking about getting a few pads to throw in my machine to help with some of the lingering Odors it's not bad but I do get a wiff of lingering wood smell at times 





**"3D Laser"**

---
---
**Gary McKinnon** *July 05, 2016 17:01*

Yes just buy the cheap stuff used for oven hoods it works a treat.


---
**HP Persson** *July 07, 2016 17:32*

Yeah, i bought active carbon filter and cut it into 3 layers and added between the fan and the hose.

Got very little acrylic smell inside since i added this without changing the exhaust at all.



I played a bit with different layers and at 3+ the smell didnt get any better, but it´s not much at all left i really have to know what i smell before i notice it. I tried in front of the fan too, didnt change anything for me. Try it out, you may have to try what the best position is for your setup and maybe more than 1 layer.

My wet clothes drying smells more :P


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/EEM7eswn5wr) &mdash; content and formatting may not be reliable*
