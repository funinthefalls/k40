---
layout: post
title: "If you have converted you machine to smoothie what's your value on this line and why?"
date: June 09, 2016 00:36
category: "Smoothieboard Modification"
author: "Ariel Yahni (UniKpty)"
---
If you have converted you machine to smoothie what's your value on this line and why?  laser_module_pwm_period





**"Ariel Yahni (UniKpty)"**

---
---
**ThantiK** *June 09, 2016 01:17*

Just left it at 20 if I remember correctly.


---
**Alex Krause** *June 09, 2016 01:26*

[http://smoothieware.org/laser-cutter-guide#toc13](http://smoothieware.org/laser-cutter-guide#toc13)


---
**Ariel Yahni (UniKpty)** *June 09, 2016 01:28*

yeah it seems many leave it like that but read today of someone setting it at 200. I remeember on my diode i have it also in 200 and i get much better grays but not sure how to do the math and know whats the accurate value here


---
**Alex Krause** *June 09, 2016 01:35*

I know that the pwm period will effect your duty cycle. Is it possible for you to run a test of the 50% power scenario in a small square done several time in intervals of 10-20 micro second increases to see what your best reault is?


---
**Ariel Yahni (UniKpty)** *June 09, 2016 02:30*

**+Alex Krause** starting at 20? Square full black?


---
**Alex Krause** *June 09, 2016 02:37*

Might try three squares 10% 50% 100% black this would give you a quick idea of the changes... do maybe 1cm squares 


---
**Vince Lee** *June 09, 2016 18:51*

The pwm period shouldn't affect the duty cycle (the percentage high versus low) but only affect the frequency at which each cycle restarts.  However, uou might need to adjust it and the upper/lower bounds if your power supply is reacting non-linearly to the PWM.




---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/9zhHvUkYK7T) &mdash; content and formatting may not be reliable*
