---
layout: post
title: "Some things I've cut with my K40 and am currently selling"
date: May 07, 2015 14:38
category: "Object produced with laser"
author: "David Wakely"
---
Some things I've cut with my K40 and am currently selling. All made with 3mm Birch Plywood. Thought I'd share with you all!

![images/46e43089a8818459cc1b908b17263914.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/46e43089a8818459cc1b908b17263914.jpeg)



**"David Wakely"**

---
---
**Jon Bruno** *May 07, 2015 15:06*

Dave can you test for me by masking the birch with painters tape?

I'm curious if the smoke stains are eliminated. LIke on the crown on the Dad keychain..

IDK, can stains be avoided?

Great work btw the numbers on the calendar are tiny!


---
**David Wakely** *May 07, 2015 18:01*

I will certainly give it a go, I would like to get rid of the smoke marks to although in some prices it's gives it a bit of character. I have changed some of the designs since cuttin like you said about letter sizing and I notice that some on the letting hole positioning is to close too the edge. I'm quite impressed that I've been able to make semi-professional looking stuff with one of these machines!


---
**Jon Bruno** *May 07, 2015 18:10*

well they look great so far.. a few more weeks and you'll be setting up at the flea market selling personalized trinkets.



Myself however, I am waiting for a new tube..

I'd imagine its coming on a slow boat from China so I may end up ordering one myself from a US seller just so I can get back to work.


---
**Stephane Buisson** *May 07, 2015 18:55*

Far from my k40 for few  months, I couldn't test something I read on the net.

Maybe you could have a try and report to the community.

Simply clean your object with a toothbrush and water, smokeburn should disappear, due to the glue into plywood, the object should not be affected.(warp)


---
**Jon Bruno** *May 07, 2015 19:07*

i have wiped smoke off before with a cloth and alcohol but I guess water would have done the trick.

I was just wondering about masking the work because I read someplace that it was supposed to really help with keeping it clean but im not sure how it affects the process.


---
**ThantiK** *May 07, 2015 23:24*

btw, yes - painters tape will get rid of the redeposition of burnt material.  We do this at my hackerspace with acrylic.  It comes with a layer of clear backing and we tell people to leave the backing on, so when its done,  you peel the backing off and all of that nasty goes away.


---
**Chris M** *May 08, 2015 19:57*

I just saw on the epilog laser site that they recommend a gentle scrub with Permatex Fast Orange Hand Cleaner to remove residue. Here in the UK I guess that Swarfega Orange is an equivalent.



[http://support.epiloglaser.com/article/8205/30190/easily-remove-engraving-residue-from-wood](http://support.epiloglaser.com/article/8205/30190/easily-remove-engraving-residue-from-wood)


---
**David Wakely** *May 10, 2015 16:56*

Just to let you know, I tried Jons suggestion of painters tape and it has made a huge difference to smoke marks they are virtually gone. I tried scrubbing with isopropanol and that didn't seem to shift it. Painters tape/ masking tape seems to do the trick perfectly!


---
**Jon Bruno** *May 11, 2015 01:30*

Sweet!


---
**Haotian Laser** *September 01, 2015 07:31*

very nice


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/RahM5uEvN4J) &mdash; content and formatting may not be reliable*
