---
layout: post
title: "New cutting bed that I just finished creating"
date: May 12, 2016 10:52
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
New cutting bed that I just finished creating. I used 27lb fishing leader wire to create a woven mesh with minimal surface area that will support the piece to be cut. It could do with something to tension it tighter (can't really think of much to use other than turnbuckles or guitar string tensioners, any suggestions welcome).



Overall size is 355mm x 330mm (to the outside of the 12 x 12 aluminium angle frame).



Cost was about $3.50 per 1m length of aluminium angle (x2) & $5 for the fishing leader wire. I already had the aluminium plate laying around at home, so not sure what it would cost for something like that. I guess you could use whatever you have laying around for that purpose.



![images/825b0c7b27cf0680dd871c7cb00a6aca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/825b0c7b27cf0680dd871c7cb00a6aca.jpeg)
![images/14c0c9ebb4006c22c0f291aa31050640.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14c0c9ebb4006c22c0f291aa31050640.jpeg)
![images/7617a4e5cea3b893e8154e46a13897bf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7617a4e5cea3b893e8154e46a13897bf.jpeg)
![images/bbe455ddcdbae0009287c370cd9380f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bbe455ddcdbae0009287c370cd9380f3.jpeg)
![images/7dd174f724546bcf02af88d7dd6b0cbb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7dd174f724546bcf02af88d7dd6b0cbb.jpeg)
![images/c3c82440d10f3e1aec9c2a3b6d572513.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c3c82440d10f3e1aec9c2a3b6d572513.jpeg)
![images/86c1be7f9a89b984a72617082b7cdfd8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/86c1be7f9a89b984a72617082b7cdfd8.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ariel Yahni (UniKpty)** *May 12, 2016 11:52*

Awesome idea, would love to see pics installed


---
**Gunnar Stefansson** *May 12, 2016 12:16*

That's a nice idea, really minimizes the risk of getting burn marks on fx acrylic and glossy stuff. Tension will be important for sure. but weight from the material will also be at a minimal.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 22:18*

Thanks **+Susan Moroney**.



**+Ariel Yahni** I've still got to work out how I am going to set it's height. I am considering while I am at it, to make a manual Z-bed with this, as I could use the 4 triangular aluminium plates to have bolts go through.



**+Gunnar Stefansson** I think the nail/needle beds might be the best way to minimise the possibility of burn marks, but depends on how much support you need.


---
**Ariel Yahni (UniKpty)** *May 12, 2016 22:25*

Im going the same way on my design but will be motorized. I will use spectra fishing line instead of belt. It's being allready proven in 3DP. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 22:49*

**+Ariel Yahni** You will use the fishing line as the pulley? That's an interesting idea.


---
**Ariel Yahni (UniKpty)** *May 12, 2016 22:54*

**+Yuusuf Sallahuddin**​ look here [https://plus.google.com/communities/103539262895202172380](https://plus.google.com/communities/103539262895202172380)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 23:05*

**+Ariel Yahni** Thanks for that. I've joined & searched spectra & see someone using it as a replacement for the belt. Looks interesting, but I wonder how will it turn the bolts that raise/lower the bed? Unless you are planning on having the motor above the bed & lifting the bed by winding the spectra onto a spool maybe?


---
**Ariel Yahni (UniKpty)** *May 12, 2016 23:12*

**+Yuusuf Sallahuddin** there are some designs that include a Z axis using printed pulleys and spectra / kevlar fishing line. So basically you place 4 screws, fix the nuts to the bed and then use the stepper with a 3DP or cut part as the coil. I have the design in my head and is one of the first thing i wanted to do, but now everything is delayed :-(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 23:21*

**+Ariel Yahni** I look forward to seeing the design when you are able to get to it. Hope the tube issue can be sorted out for you ASAP. It would be frustrating to wait to get a new gadget & then not be able to use it because some part is broken.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 06:58*

**+Ariel Yahni** Any progress on your end for getting your laser up & running? Or the seller still giving you the run-around?


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/cSWKBPKnyrX) &mdash; content and formatting may not be reliable*
