---
layout: post
title: "A quick CorelLaser/LaserDRW plugin question. The plugin's toolbar disappears right after starting the laser (with either the cut or engrave icon), and the only way it will reappear is by restarting the program"
date: August 24, 2015 17:58
category: "Software"
author: "Kirk Yarina"
---
A quick CorelLaser/LaserDRW plugin question.  The plugin's toolbar disappears right after starting the laser (with either the cut or engrave icon), and the only way it will reappear is by restarting the program.  The LaserDRW plugin doesn't seem to be accessible through any of the menus, either.  Is there a way to get it back?  I'm cutting gears out of acrylic using DXFs, so CorelDraw is the only apparent option.





**"Kirk Yarina"**

---
---
**Kirk Yarina** *August 27, 2015 16:33*

Thanks, Carl.  Company from OOT is interfering with using the K40, but I'll try that as soon as I can.  I followed their install directions and don't remember which order CorelDraw and the plugin were installed in.  It's a bit weird, and only disappears as soon as it starts engraving or cutting, never for resets or option changes.


---
**Kirk Yarina** *August 28, 2015 21:53*

I removed and installed CorelLaser a couple times, no change.  I didn't see how to (re)install the plugin separately.


---
**Bob Steinbeiser** *February 18, 2016 20:42*

I have this problem as well.  I can find the Corellaser program by right clicking on the program icon on the bottom toolbar (by the clock) but doing so de-selects what I'm trying to engrave in Coreldraw!   Has anyone been able to find a fix here?


---
**Sebastian Hehn** *June 30, 2016 14:40*

I have the same problem, no solution found yet...


---
**Terry Taylor** *July 04, 2016 01:29*

Add me to this list. Corel Draw x5, Windows 10 64 bit.


---
**Rodney Huckstadt** *August 02, 2016 03:05*

look on the windows  taskbar under the hidden items arrow , this happens to me all the time, so I just click on the hidden items and right click on the pink icon for the laser and away I go, works the same.


---
*Imported from [Google+](https://plus.google.com/+KirkYarina/posts/BWoJ7WY2XiJ) &mdash; content and formatting may not be reliable*
