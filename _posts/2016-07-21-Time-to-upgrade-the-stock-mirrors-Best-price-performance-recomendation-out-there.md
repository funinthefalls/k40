---
layout: post
title: "Time to upgrade the stock mirrors. Best price / performance recomendation out there?"
date: July 21, 2016 22:56
category: "Modification"
author: "Ariel Yahni (UniKpty)"
---
Time to upgrade the stock mirrors. Best price / performance recomendation out there? 





**"Ariel Yahni (UniKpty)"**

---
---
**Anthony Bolgar** *July 21, 2016 23:49*

Saite cutter has some reasonably priced MO mirrors. Look for them on Ebay.


---
**Ariel Yahni (UniKpty)** *July 21, 2016 23:59*

What the max size / thickness you can fit on the stock housing?


---
**Anthony Bolgar** *July 22, 2016 00:01*

I fit 20mm diameter by 3mm thick mirrors in the stock holders.


---
**greg greene** *July 22, 2016 00:11*

That's the head mirror - the beam  reflectors are larger are they not?


---
**Anthony Bolgar** *July 22, 2016 00:30*

They are all 20mm on my K40.


---
**greg greene** *July 22, 2016 00:37*

Mine look much bigger - Hmm maybe I'd better go measure  - 25MM


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/S7nVx7A7769) &mdash; content and formatting may not be reliable*
