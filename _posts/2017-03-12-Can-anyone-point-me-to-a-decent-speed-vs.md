---
layout: post
title: "Can anyone point me to a decent speed vs"
date: March 12, 2017 00:08
category: "Hardware and Laser settings"
author: "Bob Buechler"
---
Can anyone point me to a decent speed vs. power chart for your typical K40? I don't even know what the recommended speed range is for the K40. I know you're not supposed to drive your tube past 80% power unless you want to shorten it's life, but that's about it.



While we're on the subject, has anyone found a good 40W CO2 materials chart that gives light and dark engraving, and cutting numbers by material type and thickness? 



Examples: 

- MDF 3mm -- Engrave Light: XX% Power / XXmm/min speed

- Opaque Acrylic 4mm -- etc.





**"Bob Buechler"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 12, 2017 00:45*

From my experience, other people's settings don't always play nice with MY machine. It will be the same for you. But, they can be a vague guideline at least of where to start testing.



Personally though, for every new material I use I take a scrap & perform a series of calibration tests. Run some basic cuts (a 10mm square for example) at various different speeds/power levels to determine which is best/cleanest.



Run a calibration gradient BMP through the raster to test variations in the PWM power level & how it affects your specific material.


---
**Bob Buechler** *March 12, 2017 00:50*

Oh understood. It's like anything else... cooking, for example. Everyone has different ovens, and they live in different altitudes, so recipes are guidelines. Such a chart would have to be too. But it would be nice to have someplace to close-ish to start from, and then you can just dial it in through a few brief rounds of testing instead of having to guess every time you use some new material or thickness.


---
**Mark Brown** *March 12, 2017 01:29*

What about top speed?  How fast can I go before sending the head shooting off the end of the rail?


---
**Gavin Dow** *March 14, 2017 14:12*

**+Mark Brown** I raster engrave around 300mm/s regardless of material, and just adjust power depending on how deep I want to go. Never had any issue with losing steps. Just make sure you don't command your laser to go beyond the physical limits of the machine - it will do WHATEVER you tell it to do with the stock firmware & hardware! Run a preview first with the beam off so if it crashes it's only at low speed.

I straight-line vector cut at anywhere between 7-15mm/s, though I'm still tinkering with this. I've gone higher, but I generally run out of power to do meaningful cuts above 15mm/s on all but the thinnest materials. 12mm/s SEEMS to be a nice all-around speed, but like I said, still tinkering.

I have experienced issues attempting to do detailed vector cuts above 8mm/s - usually tends to lose steps in both axes. I have to go slow and low power to get anything other than long straight lines and smooth curves.


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/GkzXzhjvwsm) &mdash; content and formatting may not be reliable*
