---
layout: post
title: "Is there a way to take the USB Key and create an ISO and mount it so I don't have to leave it plugged in?"
date: November 17, 2015 19:54
category: "Software"
author: "James Keaton"
---
Is there a way to take the USB Key and create an ISO and mount it so I don't have to leave it plugged in?﻿





**"James Keaton"**

---
---
**Phil Willis** *November 17, 2015 20:07*

Its not drive but electronic key that unlocks part of the software so not possible to make an iso.


---
**Anthony Coafield** *November 17, 2015 21:39*

Thanks for the question and answer. I was wondering that too.


---
**James Keaton** *November 17, 2015 22:55*

I assumed that there is a way.  There has to be a way to clone it.  Maybe not just being hopeful


---
**Peter** *November 18, 2015 09:33*

Get yourself a short USB extension cable, plug the key to that and fold the cable back inside the machine. Keeps it plugged in but out of the way so  you don't snap it off or anything. 


---
**Frank Norgaard** *November 19, 2015 09:59*

Maybe this can help [http://www.sonand.com/2014/08/how-to-install-hasp-multikey-usb-dongle.html](http://www.sonand.com/2014/08/how-to-install-hasp-multikey-usb-dongle.html)


---
**Coherent** *November 19, 2015 19:50*

**+Frank Nørgård**

Thanks for the link. I played around with USB Key/HASP emulators a few years ago without much luck, but looks like things have progressed a bit. Very interesting reading.


---
*Imported from [Google+](https://plus.google.com/109769945337563588796/posts/CCDMczmULhC) &mdash; content and formatting may not be reliable*
