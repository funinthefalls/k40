---
layout: post
title: "CorelLaser question. If I try to engrave and cut I set the first parameters then click add task I go back to the Corel screen however the laserdraw panel disappears I a running Windows 10 and Corel X7"
date: May 15, 2016 00:32
category: "Discussion"
author: "Jeff Kwait"
---
CorelLaser question.  If I try to engrave and cut I set the first parameters then click add task I go back to the Corel screen however the laserdraw panel disappears I a running Windows 10 and Corel X7





**"Jeff Kwait"**

---
---
**Jim Hatch** *May 15, 2016 00:50*

Yeah, that happens. You can find the laser controls in the system tray (lower right corner next to the power/network/date/etc indicators. Click on the little up arrow there to reveal all of the apps in the system tray and you'll find the laser controls there.


---
**Terry Taylor** *July 04, 2016 01:30*

Tried that, but it does not seem to bring the controls back.


---
**Terry Taylor** *July 04, 2016 01:34*

Forget what I said, when I right click, they come back.


---
*Imported from [Google+](https://plus.google.com/114834788519034865166/posts/YHZvVby3R4Z) &mdash; content and formatting may not be reliable*
