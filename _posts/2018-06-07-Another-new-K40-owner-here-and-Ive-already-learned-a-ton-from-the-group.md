---
layout: post
title: "Another new K40 owner here and I've already learned a ton from the group"
date: June 07, 2018 19:53
category: "Materials and settings"
author: "S Th"
---
Another new K40 owner here and I've already learned a ton from the group. Thanks everyone. I'm going to need to stock up on small electronics to get started, so what are your suggestions for wire gauge sizes, connectors, small components, etc?





**"S Th"**

---
---
**Don Kleinschnitz Jr.** *June 07, 2018 20:12*

Very hard to answer that question without knowing details of what you plan to do :)?




---
**S Th** *June 08, 2018 03:27*

**+Don Kleinschnitz** You’re right I should’ve been more specific. My upgrade priorities are 1) water temperature monitoring with the potential for active temperature control in the future, 2) led ring light for a laser finder, 3) interlock  circuit, 4) revert digital display pcb to analog ammeter, pot and switches, and 5) distribute power to air assist, water pump and exhaust. I won’t be upgrading the circuit board, at least not for now. I’m fairly proficient with inkscape, so I think k40 whisperer will be sufficient for me for a while. I don’t plan on adding a motorized z-axis or other steppers, and I’m really doing this as bare-boned as I can get away with (my unit was a $225 Craigslist find from a couple who really didn’t know what they were doing).


---
**Don Kleinschnitz Jr.** *June 08, 2018 11:45*

**+S Th** Look around this blog and see if this gives you what you want. Then ask specifics about any upgrade you want to undertake. I think most of what you need you may find on this blog.

As far a connectors and wire: you can get the wire size from AWG charts and needed currents by Googling. The connectors depend on the application.

In all cases when you get to implementing a specific upgrade we can make specific part recommendations. Many are in the blog.

[donsthings.blogspot.com - Don's Things](https://donsthings.blogspot.com/)



There is lots of help here !🙂


---
**S Th** *June 08, 2018 20:05*

**+Don Kleinschnitz** Thanks Don, I've been reading you're blog non-stop for the last week. If I can get my machine to a small fraction of where yours is I'll be a happy camper.


---
*Imported from [Google+](https://plus.google.com/116589695629402182526/posts/YHEPLwER3KC) &mdash; content and formatting may not be reliable*
