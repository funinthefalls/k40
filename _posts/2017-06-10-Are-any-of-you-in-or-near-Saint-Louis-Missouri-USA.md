---
layout: post
title: "Are any of you in or near Saint Louis, Missouri USA?"
date: June 10, 2017 00:24
category: "Discussion"
author: "Alex Hayden"
---
Are any of you in or near Saint Louis, Missouri USA?  Are these lasers able to cut 1/8inch steel plate?  If yes, I would be very interested in having someone​ cut a project. 





**"Alex Hayden"**

---
---
**HalfNormal** *June 10, 2017 01:02*

Sorry, CO2 lasers cannot cut steel.


---
**Alex Hayden** *June 10, 2017 01:16*

**+HalfNormal** that is not true. But the K40 might not be able to. Needs to have an oxygen nozzle and a higher watt laser. But a CO2 laser can cut metals. [Coherent.com](http://Coherent.com) has some so does [kernlasers.com](http://kernlasers.com) among others. 


---
**HalfNormal** *June 10, 2017 01:32*

**+Alex Hayden** You need a high watt laser with the proper gas mixture to cut very thin metal with  a CO2 laser. A YAG laser is what is used to cut metal of any substance. I can cut a metal plate with a butter knife too but would never do it. :-) I should have said that a CO2 laser is not the most efficient way to cut metal. Here is a quote from one manufacture "Material thickness at which cutting or processing is economical ~0.12" to 0.4" depending on material" [teskolaser.com - Tesko Laser: laser cutting and metal fabricating services](http://www.teskolaser.com/index.html)


---
**Mark Brown** *June 10, 2017 02:02*

Here's an 80w co2 laser cutting thin steel.  The k40 wouldn't do it though.  Plus the cut looks nasty.




{% include youtubePlayer.html id="0_W4dTC3vec" %}
[https://youtu.be/0_W4dTC3vec?t=5m](https://youtu.be/0_W4dTC3vec?t=5m)


---
**Cesar Tolentino** *June 10, 2017 13:03*

What you need is plasma cutter.


---
**Martin Dillon** *June 11, 2017 14:14*

Yeah, a plasma cutter is the way to go. **+Alex Hayden** what is the accuracy needed for the project?  I have a cnc plasma cutter and would be glad to do a test cut for you.  All of the cut surfaces are rough but if you don't need machined surfaces it should work okay.


---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/RZJAxuJvFhL) &mdash; content and formatting may not be reliable*
