---
layout: post
title: "I have the Shenhui K40 laser, and I have ordered a rotary axis for bottles etc and a stepping motor driver for this, can anyone explain how to connect it all and get it working as it came with no instructions, the rotary has"
date: December 24, 2016 19:41
category: "Hardware and Laser settings"
author: "john dakin"
---
I have the Shenhui K40 laser, and I have ordered a rotary axis for bottles etc and a stepping motor driver for this, can anyone explain how to connect it all and get it working as it came with no instructions,  the rotary has a nema 17 motor





**"john dakin"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2016 19:58*

Show us pictures! I wasn't aware of any k40 that came with the rotary. Maybe you just have to unplug the y motor and plug in the rotary but you should show pics of all the machine and the electronics 


---
**john dakin** *December 24, 2016 20:40*

Hi, It doesn't come with the rotary, but this is an optional extra designed for K40 machines, I need to know how to wire this up and connect it

![images/f4ac0ab9080e473980ca603122e3d06d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4ac0ab9080e473980ca603122e3d06d.jpeg)


---
**greg greene** *December 24, 2016 21:01*

Does your controller have a stepper driver port for it? Most K40's just come with X and Y axis  (Left/Right and Up/down) you probably need to get one of Ray's 3D boards - it has the ability to control 4 steppers


---
**john dakin** *December 24, 2016 21:04*

I'm not really sure, this is the stepper driver for it, the K40 has a few spare sockets on it, its a M2 Nano board on the K40

![images/7730110fc25e18ca0d32cbfd39eb19f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7730110fc25e18ca0d32cbfd39eb19f2.jpeg)


---
**greg greene** *December 24, 2016 21:08*

I'm not sure that the Nano board will run it, but You can try Ray's suggestion, and  use the y axis maybe the guys at Light Objects have done this before and have a suggestion


---
**john dakin** *December 24, 2016 21:13*

The Guys at Light Object don't really give any support for it, but they say the K40 laser board cannot handle the rotary that's why they told me I have to buy the stepper controller to power it, but I was thinking that it must be connected to the M2 nano board to get its instructions, there is a spare socket on the M2 nano that has markings YL XL GND AND NC  I was thinking maybe this is a spare axis socket but I'm not sure


---
**greg greene** *December 24, 2016 21:17*

I have not heard of anyone doing that - but that doesn't mean it can't be done.


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2016 21:17*

No. That's for endstops. I'm assuming your machine has the ribbon cable which is why that would not have anything plugged in to it. You can try swapping Y motors directly but there's no way I know of to pull signals off the m2nano board to run an external driver. 

In which case, you'd need to upgrade to a new board.  You can check out my mini for laser bundle at [cohesion3d.com](http://cohesion3d.com)


---
**john dakin** *December 24, 2016 21:20*

There is a spare Y axis socket on the M2 Nano board that is not being used


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2016 21:23*

Nope. Once again I'm assuming you're running the ribbon cable variant which means that the endstops and one of the motors are fed thru that instead of the individual endstop and motor ports you've described. 


---
**greg greene** *December 24, 2016 21:26*

Get Ray's board - you will want to upgrade the software anyway to really use the Rotary - Ray's board is easy to use and is pretty much bulletproof - I know - I've abused it in ways that would give Ray grey hair ! :)


---
**Ned Hill** *December 24, 2016 22:32*

For whatever reason the corellaser plugin engrave settings does have a rotary option.  Not sure if it does anything with the stock board though.

![images/0efe91e9e070ea3bf8e2136e9a5bd843.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0efe91e9e070ea3bf8e2136e9a5bd843.png)


---
**john dakin** *December 24, 2016 22:42*

ok thanks I will take a look


---
**Don Kleinschnitz Jr.** *December 25, 2016 13:18*

Pretty sure you unplug the y axis and there is another y axis connector that external rotary stepper driver connects to. Never tried it though, converted to smoothie. I've read about this somewhere, try the FB forum. 


---
**john dakin** *December 25, 2016 19:43*

I have now cancel  my order with light object for the Rotary, this was due to having no backup support from the company and after spending a full day researching about this on various forums, I have now found that its not going to be an easy conversion without spending more money and even the experts cannot really give the answers or a solution to wiring up this item


---
**greg greene** *December 25, 2016 19:47*

Sorry to hear that, but in the mean time you may be able to pick up some hints on this forum from someone who has been able to do it - though I doubt it will be with the stock board.


---
**john dakin** *December 25, 2016 19:49*

Thanks, yes you are right, it cannot be directly connected to the main m2nano board as it will blow it


---
**Ray Kholodovsky (Cohesion3D)** *December 25, 2016 20:01*

Ok, so I emailed you back about this, grab one of my external stepper adapters and the mini bundle, and if you do want to run that rotary in the future you can hook the black box driver up to the external driver adapter on one of the additional driver sockets of the c3d mini. 


---
**john dakin** *December 25, 2016 20:02*

Thanks Ray




---
**greg greene** *December 25, 2016 20:05*

I figured Ray would have the answer - as usual !



Merry Christmas !


---
*Imported from [Google+](https://plus.google.com/110405157088691728751/posts/3zyHAgKJw5k) &mdash; content and formatting may not be reliable*
