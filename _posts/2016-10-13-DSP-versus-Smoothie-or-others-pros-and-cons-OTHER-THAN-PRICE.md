---
layout: post
title: "DSP versus Smoothie (or others), pros and cons OTHER THAN PRICE ."
date: October 13, 2016 21:41
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
DSP versus Smoothie (or others), pros and cons <b>OTHER THAN PRICE</b>. I don't want to start a 'my setup is better than yours' or other wars between what's best. I believe that's all relative to the operator and what they want to achieve. That said, has anyone done both, like, dislike, pros, cons, wine, beer ... ready, go!





**"Ashley M. Kirchner [Norym]"**

---
---
**Ariel Yahni (UniKpty)** *October 13, 2016 21:45*

I would like to know thechinal difference


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 13, 2016 23:00*

That would be nice to see a totally objective discussion of pros & cons of each different controller type.


---
**Ashley M. Kirchner [Norym]** *October 14, 2016 00:08*

I think the only way for that to happen is if someone has used both types of controllers. And I don't know anyone who has. But still, even those who have only gone from the stock controller to either a Smoothie or DSP, I think it's still worth the discussion ...


---
**Scott Marshall** *October 14, 2016 20:54*

I'm biased, but for K40 upgrading, (and a lot of other applications) 



I think the Smoothie has it all, (Danger Tech-speak ahead...)



On Board high quality motor controllers, microstepping(/16), 2A capacity (software settable – no wiring required) available in as many as you need 3 – 5 Axis

If you need bigger motors than 2A, no problem, it had 5 sets of motor controller outputs, just hook up your Geckodrive and run that full size milling machine, router, whatever. More power – uh-huh!



It has a ton of I/O – plenty of inputs and outputs, Onboard MOSFET outputs in 2 handy sizes with the option to supply onboard power (up to 36V) or to switch external loads such as SSRs, PLUS, there's even more outputs available as logic level pins, - just add your own MOSFET etc and drive anything you need to.

Easy to use endstop inputs with available power taps to accommodate most any device NPN/PNP (sourcing/sinking).



It's an all in one compact package with all the most commonly used I/O devices already onboard and ready to use.

(Just add an ACR board and it's a Plug-in solution for the K40 and it's brethren)



Then you have the hand onboard self updating firmware setup. Plain language Configuration file is automatically installed at each start-up. You just change the Config file with a text editor and it does the rest. No white knuckled flashing of the ROM then hoping it doesn't brick on you. If it doesn't work, just change it back. Handy, and all on a SD card, so you can change a Smoothieboards personality with the swapping of a SD Card. It even works with the other guys accessories!



Support, No contest here. Full online documentation, Several support forums, and direct access to it's developers thru Art Wolf's own group. Now I'm a link in the chain. You won't be left with a fancy paperweight because you can't figure out how to hook up your Smoothieboard. 



It's the only controller with Plug and Play K40 interfacing available (ALL-TEKSYSTEMS.com)

Even more Smoothieboard support devices will be coming soon from Scott Marshall (ALL-TEK).

Best support, Built in high quality motor controllers, Versatility!



Did I mention ALL-TEK Systems is now an authorized Smoothieboard Dealer? 

(thanks Art) and we FULLY SUPPORT your Smoothieboard.



PS,

OK, I forgot the DSP. Haven't played with one (pretty pricey and I don't hear good things about customer service) , but you'll still have to supply the software chain to the controller. I don't believe the current differences there are significant, but I'll let a DSP Representative tell you about that option.


---
**Ariel Yahni (UniKpty)** *October 14, 2016 20:57*

So Smoothie said to DSP " move over, I'm the new king in town" 


---
**Scott Marshall** *October 14, 2016 21:10*

We'll just let all the DSP guys flock in and tell us the pluses. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 14, 2016 22:23*


{% include youtubePlayer.html id="60bzbfHP1Hs" %}
[https://youtu.be/60bzbfHP1Hs](https://youtu.be/60bzbfHP1Hs)


---
**Ashley M. Kirchner [Norym]** *October 15, 2016 01:49*

Thank you **+Scott Marshall** for that response. Question: what's the software provided for the Smoothie? Or, a different question: what NATIVE software can it support? For example, can I work in Illustrator (the latest version) and either print directly or be able to read a layered AI file directly in, without needing to go some sort of song and dance to get it working? Not that I have anything against a song and dance, as long as there's alcohol involved ...



And please don't suggest CorelDRAW because I'm not about to go buy another graphics package when I already have the full Adobe suite. Just my dos pesos ...


---
**Ariel Yahni (UniKpty)** *October 15, 2016 02:00*

For smoothie the preferred choice is [http://laserweb.xyz](http://laserweb.xyz)  You design in ai export as an svg, drag and drop your layers into the browser in the order you want to process them an that's it. , no native ai support. 


---
**Ashley M. Kirchner [Norym]** *October 15, 2016 03:21*

When you say 'drag and drop layers', I read that as needing to export each layer separately. Is that correct?


---
**Scott Marshall** *October 15, 2016 03:28*

Laserweb3 supports nearly every file type there is, so almost any drawing package can export to it. (I would list them, but there are so many it would be a chore). 

In the extremely remote possibility that you have an unsupported file, Laserweb3 allows use of Standard G code generated elsewhere, like Cambam, MeshCam, Simple CNC, SImpleCam, Solid CAM, EZ Cam, there's hundreds of them, dozens of free ones.



In addition to Laserweb3 and also supporting Gcode input are Pronterface, Visicut and a couple of others that escape me at the moment. Pronterface is free and included on the SD card of the Smoothieboard.



The ultimate connectivity of The Smoothieware is nearly unlimited, especially thru the use of G code which is the universal CNC language. Only a couple of years ago, G code was the only choice for most Hobby CNC work, and VERY commonly used in commercial machine shops, sign shops, engraving routers etc.

When I built my 1st CNC machine it was pretty much a choice of Mach 2 and your choice of 2 or 3 CAM converters.



Now options are huge and rapidly expanding.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 03:47*

**+Ashley M. Kirchner** You can import SVG as multiple layers, but if you need a Cut Layer & an engrave, you put them in as separate files (e.g. SVG for cut, jpeg for engrave).



I have tested multi-layered SVGs from AI & LaserWeb reads all elements in correctly. I am still playing around with LaserWeb (learning the ins & outs) so can't give a huge deal more info yet.



There are some minor nuances with the setup of each "shape" in your AI file in regards to cutting order. Bottom-most shapes in a layer cut first, top-most cut last. I have yet to test the cut order with multiple layered svg files.


---
**Scott Thorne** *October 16, 2016 23:23*

As you can see from the 3d carve photos that I've posted...dsp does have some advantages....I've never used open source, so of course I'm gonna be partial to my ruida controller.....lol


---
**Scott Marshall** *October 17, 2016 00:28*

From what I've seen of your work, it's hard to fault ultimate performance of the DSP. How user friendly is the software?


---
**Scott Thorne** *October 17, 2016 09:08*

 It's great...very simple to use and it has a lot of features. 


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/hbuEqk1bS5p) &mdash; content and formatting may not be reliable*
