---
layout: post
title: "My new stand for my laser. Cooling water tank and pump, air assist compressor, and PC will be behind the door, the 3 drawers have plenty of room for raw materials"
date: May 03, 2016 21:21
category: "Modification"
author: "Anthony Bolgar"
---
My new stand for my laser. Cooling water tank and pump, air assist compressor, and PC will be behind the door, the 3 drawers have plenty of room for raw materials. Size is 26" wide X 58" long X 38" tall. Picked it up for $100 at my local Habitat for Humanity Restore. Looks like it came out of a high school science lab.



![images/da71655655725a8ee5982a4204ec56b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/da71655655725a8ee5982a4204ec56b2.jpeg)
![images/72fa096a3c2c8e235bf560303f504b49.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/72fa096a3c2c8e235bf560303f504b49.jpeg)

**"Anthony Bolgar"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 03, 2016 21:51*

Nice. Looks to be a decent stand for the K40. I'm just operating on a folding trestle table currently. Was on a different table to begin with (a wood/ply construction I made a few years ago) but moved to the trestle when I was pulling the K40 apart (needed the extra space to put things as I pulled them out). Something like this looks much better as it has the storage options & can hide certain things out of the way in the cupboard section as you mentioned.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/HiTiDDf3Hc9) &mdash; content and formatting may not be reliable*
