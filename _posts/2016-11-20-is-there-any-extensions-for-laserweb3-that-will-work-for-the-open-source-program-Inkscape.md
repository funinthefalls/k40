---
layout: post
title: "is there any extensions for laserweb3 that will work for the open source program Inkscape?"
date: November 20, 2016 20:11
category: "Software"
author: "Jonathan Davis (Leo Lion)"
---
is there any extensions for laserweb3 that will work for the open source program Inkscape?





**"Jonathan Davis (Leo Lion)"**

---
---
**Ariel Yahni (UniKpty)** *November 20, 2016 21:01*

**+Jonathan Davis**​ LaserWeb does the function that and extension would do. Just draw, convert to svg or bitmap and open in Laserweb 


---
**Jonathan Davis (Leo Lion)** *November 20, 2016 21:02*

**+Ariel Yahni** Alrighty then


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/BZGe8HWL3Xq) &mdash; content and formatting may not be reliable*
