---
layout: post
title: "Recieved my machine today wow i am impressed packing was perfect box inside of another box all the parts were paded with foam and taped down looked great this machine came with laserdrw everything is here except i don't see a"
date: March 09, 2016 23:48
category: "Discussion"
author: "Dennis Fuente"
---
Recieved my machine today wow i am impressed packing was perfect box inside of another box all the parts were paded with foam and taped down looked great this machine came with laserdrw everything is here except i don't see a dongle is this version machine supposed to have one 



Thanks Dennis 





**"Dennis Fuente"**

---
---
**Ashley M. Kirchner [Norym]** *March 09, 2016 23:50*

Check the bag, if it came with one. Mine did. 


---
**Dennis Fuente** *March 09, 2016 23:58*

thanks for the response the machine had a clear envelope with the power cord , software ETC i have looked over the machine and i don't see a dongle 

thanks 

Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2016 00:36*

Did you get some little black bag thing? My dongle was in that also.


---
**3D Laser** *March 10, 2016 00:37*

Mine was tied to the axis with ribbon


---
**Dennis Fuente** *March 10, 2016 17:44*

well i have looked over everything and i don't see a dongle key is it possible this version dose not need one i have installed the software and plugged the machine into the usb and my computer sees the machine i have not tried to move it or cut anything as in tthe software i don't see a means to jog it so far.



Thanks

Dennis 


---
**Dennis Fuente** *March 10, 2016 20:21*

Hi Guys so i started to cut up the boxes that the machine came in and the inner box had the dongle stuck inside one of the folds wow i was lucky to find it i could have thrown it out with the box  anyway now i am trying to configure the software from the cd wow it's a strugle so first off i don't see corel draw on the cd i find corel seal and laserdraw in the videos on the cd it says to install corell draw 12 or newer so i don't understand whats supposed to be installed also when do i plug the dongle in after installation of the software or before how about drivers wow a lot missing.



Thanks 

Dennis 


---
**Ashley M. Kirchner [Norym]** *March 10, 2016 20:56*

There ya go. Good job looking for that dongle. As for the software, yeah I had issues as well. It may be one of those Chinese named files as well. Possibly as a .rar file. I forgot.


---
**Dennis Fuente** *March 10, 2016 21:00*

Thanks Ashley for the response as you can imagine i am trying to get the machine going and can't figure out the software looking all over the net for info and finding nothing it's flustrating to say the least i don't see a .rar file i do see other's but no correl draw as shown in the manual or video and i can't read chinese 



Thanks Dennis 


---
**Ashley M. Kirchner [Norym]** *March 10, 2016 21:14*

Yeah, neither can I. :) But I do remember it having a pirated copy of Corel on the disk. I'm not home to look at my disk. Maybe someone else can look on theirs and help you out.


---
**Dennis Fuente** *March 10, 2016 21:26*

Thanks Ashley 

The only thing i can find on the cd is something called WinsealXP i have installed it but i don't see anywhere on the program it being called coreldraw



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2016 23:26*

Hey Dennis. I have a copy of the K40 software on my google drive, that I just put up for another member the other day as he was sent the wrong disk in his box.



[https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing)



You can grab it there. It does have a CorelDraw12.rar file in there (although I think they spelled it incorrectly as CoreDraw12).



As Ashley mentioned, it is a pirated version & I believe for some users it has flagged as a virus on their system.


---
**Dennis Fuente** *March 11, 2016 17:36*

Thanks Yuusuf 

I will try and dowload a copy 


---
**Dennis Fuente** *March 11, 2016 23:10*

Yuusuf Thanks very much for sharing the correl draw file i down loaded it and installed it works great one thing i did notice is that it says the program will come for registration again in 15 days dose this mean its a trial version 

Also since my machine is using laserdraw can i run any other program or am i stuck with this unless i hack the whole machine went through this with my 3d printer did it but it was kind of a pain are there other drawing programs that i can use i see that laser draw only accepts a certin type of file kinda limited

Thanks again

Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 12, 2016 01:02*

There was a text file with a CD Key for the Corel Draw. Serial: DR12WEX-1504397-KTY



Basically it is a pirated version, but once you input this cd key, it should allow you to use it as a full version.



I believe that using the default controller board, we cannot use any other software except:

-CorelDraw with CorelLaser plugin,

-LaserDraw.



CorelDraw with the CorelLaser plugin will allow you to input probably more file types. I use Adobe Illustrator for my file drawing, then save as version 9 format (anything higher doesnt work). Then I import that into CorelDraw/CorelLaser.



Works pretty much perfect from there.


---
**Ashley M. Kirchner [Norym]** *March 12, 2016 01:44*

CorelDRAW itself worked fine for me, however whenever I tried sending it to the laser, the plugin would constantly crash CorelDRAW itself. No matter what I did, it wouldn't work. So eventually I just gave up and now I'm only using Illustrator, export to a bitmap (PNG) and import into LaserDRW. I wish I could make use of the layers in Illustrator to set what to cut and what to engrave, but LaserDRW only imports a bitmap and wouldn't have a clue what to do with an Illustrator file. Ah well, live and learn. Maybe it's time to switch it all out for a Ramps controller and use Inkscape ... need to research that and see what I can do with that.


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/LWGKvpwZAoH) &mdash; content and formatting may not be reliable*
