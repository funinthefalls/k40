---
layout: post
title: "Next part of project completed! Originally shared by Yuusuf Sallahuddin (Y.S"
date: October 05, 2016 15:43
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Next part of project completed!



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Well, the next part is completed for my project. Again, #LaserWeb3 does a superb job with engraving my imagery.



6mA (pot), 100% power (LW), 400 white, 300 black.



Pretty happy with the results of this piece.



It is #VitruvianMan (Leonardo #DaVinci) combined with the logo for #Instructables (the cool robot).



This was a much faster engrave than the previous piece (as it is so much less complicated). About 36 mins, & the airbrushing was about 1 hour max.



#VitruvianRobot

![images/81dd5e12d945314eb6c0cb0ec84fe6cf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/81dd5e12d945314eb6c0cb0ec84fe6cf.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/ZPHVZhdw9TR) &mdash; content and formatting may not be reliable*
