---
layout: post
title: "Puzzled. Fixed mirror sorted, fixed mirror to Y mirror aligned"
date: June 23, 2016 19:42
category: "Original software and hardware issues"
author: "Pete Sobye"
---
Puzzled.



Fixed mirror sorted, fixed mirror to Y mirror aligned. 

Y to X mirror sorted and aligned.  So the laser hits virtually the same mark in top left, bottom left and  bottom right but it's high by a couple of mm in the top right. Cannot get it to line up properly. 

Also, can I get the damn thing to cut 3mm acrylic any faster than 3mm/s 

Suggestions please. 





**"Pete Sobye"**

---
---
**Derek Schuetz** *June 23, 2016 19:53*

Only thing I can see happening is your beam is hitting the inside of your cone on you air assist


---
**Evan Fosmark** *June 23, 2016 20:01*

What power level are you using for your 3mm acrylic? I can cut it smoothly at 7.5mm/s with 29% power.


---
**Pete Sobye** *June 23, 2016 20:09*

I am having to go to 20ma (it's the anologue meter type) and 2-3mm per second. Air assist head not fitted yet and waiting for new mirrors and lens. 


---
**Derek Schuetz** *June 23, 2016 20:10*

Is your focal length correct have you done a slanted board test?


---
**Evan Fosmark** *June 23, 2016 20:11*

How far is the material from the laser cutter? (mine is ~50mm)


---
**Pete Sobye** *June 23, 2016 20:25*

I did manage to cut through at 6mm per second using the slanted board method but when I tried to do it again, I couldn't. Need some physical help. 


---
**Derek Schuetz** *June 23, 2016 20:27*

Did you adjust your bed to the middle of the cut through heigh


---
**Mircea Russu** *June 23, 2016 20:36*

[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf) the best guide I've read. The thing is you have to adjust the  mirror to hit the same spot on the next part and then move next mirror to be hit in the center. A little counterintuitive at first but very logic. I can explainmore when at the computer, now I'm mobile :)


---
**Stephen Sedgwick** *June 23, 2016 21:52*

Wait you just said - I did manage to cut through 6mm per second using the slanted board method... but now you cannot - is your mirror dirty?



Slant method helps determine best focal length - if you remove the lens itself you should still burn paper...if it is coming through the center that will tell you first if all the other alignments are good... as you indicated it goes up as it gets further away from the second mirror you will want to angle that mirror down a little to hit right in the center (this deflection will be more than up next to the mirror).  This will effect your cut.  Then once that is aligned - if you take out the lens itself you should have a paper burn right in the middle otherwise you will still want to make sure it is aligned all the way across perfect (as close to the center as physically possible). 



It sounds like you are possibly dealing with a dirty lens and still a slight alignment issue



We can chat more if needed..


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 00:47*

From what you say, it is just 1 corner that the beam is hitting too high, the TopRight.



What this makes me wonder is on the CentreRight, is the beam hitting too high (but slightly lower) than the TopRight? It could be that the entire Right hand rail is not level.


---
**Pete Sobye** *June 24, 2016 15:17*

Matter resolved. The laser tube was not level. It was 2mm off on the opposite end to the mirror. Also, all mirrors now aligned correctly and the lens was in the wrong way! Sorted and now she cuts 3mm at 6mm/s at about 13ma!!


---
**Stephen Sedgwick** *June 24, 2016 15:59*

perfect!!... glad to hear that


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/hBHJQcXHUY6) &mdash; content and formatting may not be reliable*
