---
layout: post
title: "Hi guys, I've searched this forum and google and can't find anything about cutting stamp rubber"
date: October 14, 2016 00:00
category: "Discussion"
author: "larry champagne"
---
Hi guys, I've searched this forum and google and can't find anything about cutting stamp rubber. Specifically, I was wondering if there is a setting or technique to get a chamfer on the edge on the engraving. The laser was advertised as being able to make stamps and even came with Winsealxp so I just assumed being able to chamfer the engravings would be doable since this is common with stamp making.





**"larry champagne"**

---
---
**Ashley M. Kirchner [Norym]** *October 14, 2016 00:20*

Laser shoots straight down. There will be no chamfer done unless you want to redesign the head to point the beam at an angle for you.


---
**Ariel Yahni (UniKpty)** *October 14, 2016 00:22*

You can chamfer with a very dialed in smoothie or DSP a lots of tests. This is done via greyscale 


---
**Ashley M. Kirchner [Norym]** *October 14, 2016 01:18*

He said "dialed in" ... 1-800-2SMOOTH. :)


---
**Scott Marshall** *October 14, 2016 19:33*

One thing you might want to try on a ruined stamp to see if it works is to play a propane torch  across the face quickly. The sharp edges will burn/melt away.(theoretically)

This sort of process in used in manufacturing a variety of cut goods which can't be tumbled or blasted. 'Flame deburring' it's known as.



I can't promise it will work, but it's where I'd start.  



Good luck

Scott


---
*Imported from [Google+](https://plus.google.com/113953362128101179522/posts/4xqcTDzwx4u) &mdash; content and formatting may not be reliable*
