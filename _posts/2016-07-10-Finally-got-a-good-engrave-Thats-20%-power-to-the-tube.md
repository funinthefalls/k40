---
layout: post
title: "Finally got a good engrave. That's 20% power to the tube"
date: July 10, 2016 23:10
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Finally got a good engrave. That's 20% power to the tube. Word of warning, do not use the latest smoothie edge firmware with lasers. Pwm is broken there and will always run at full power. Arthur posted about this a while back, but in case you didn't see... The June 26 build, which is what's on the master branch is the "latest" that you will want to use with laserweb. 

![images/a54fc6e1157b688c64b597f203423359.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a54fc6e1157b688c64b597f203423359.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ariel Yahni (UniKpty)** *July 10, 2016 23:42*

Do a raster 


---
**Jon Bruno** *July 18, 2016 21:35*

Which version? I grabbed the latest off the master branch and I get a build of July 2


---
**Ray Kholodovsky (Cohesion3D)** *July 18, 2016 21:37*

Hmm, Arthur said master was June 26. Anyways what I did was hunt through the commits of the edge branch and then "browse files" of June 26. 


---
**Jon Bruno** *July 18, 2016 21:38*

ok i'm off hunting.. thanks for the tip..

 is this still an issue to date?

New Smoothie owners may run into this hurdle and become flustered.. (Iam )


---
**Ray Kholodovsky (Cohesion3D)** *July 18, 2016 21:40*

Have you used the current master and experienced an issue?


---
**Jon Bruno** *July 18, 2016 21:44*

just starting out and my board is just waking up, I wanted to make sure it had the correct FW installed because out of the gate it wasn't responding properly. I remembered your post and thought I should start here.


---
**Jon Bruno** *July 18, 2016 21:48*

Eh.. whatever it is.. I found the Jun 26 build and it's in for now. I'll peruse the commits and see if there's any mention of a fix.


---
**Ray Kholodovsky (Cohesion3D)** *July 18, 2016 21:49*

No, the issue with the later firmwares has to do with laser pwm not working properly.  Not responding would be a completely different issue.


---
**Jon Bruno** *July 18, 2016 21:51*

What I meant was my board had a bad or no FW flashed to it.. I didn't know which version to use because I remembered your post.


---
**Jon Bruno** *July 18, 2016 21:51*

(clone....)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/6Gc2SMbtUQq) &mdash; content and formatting may not be reliable*
