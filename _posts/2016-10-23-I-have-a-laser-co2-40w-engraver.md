---
layout: post
title: "I have a laser co2 40w engraver"
date: October 23, 2016 01:41
category: "Original software and hardware issues"
author: "John von"
---
I have a laser co2 40w engraver. I posted my problem and have received suggestions but nothing worked. Here is my problem: when I send the script to the engraver, I get a message that the engraver is not connected. These are things that I checked or tried: make sure the id code was the same both on the board and the settings on the computer screen: when I turned on the engraver, I do get a light on the board under the fan housing; the fan works and I can shoot a test laser on a board; I re-loaded the software and that didn't work. DO I NEED TO PURCHASE A NEW BOARD (THE ONE THAT CONNECTS THE USB CABLE? IF SO, FROM WHOM CAN I BUY ONE?

Many thanks for your prior help and please advise me now. 





**"John von"**

---
---
**Anthony Bolgar** *October 23, 2016 01:53*

Here are some other suggestions, USB is finicky at best. Try to use the shortest USB cable you can find. Make sure the cable is high quality. Do not use a USB hub. Keep power cords away from the USB cable. Hope this helps you.


---
**K** *October 23, 2016 03:20*

Does yours use a USB dongle to authenticate? If so, do you have that plugged in?


---
**Rodney Huckstadt** *October 23, 2016 09:22*

also the USB dongle,  don't plug it into a usb 3 port, I di that and it would not recognise it. (coulda just been my PC but worth a mention)


---
**Andy Shilling** *October 24, 2016 09:22*

What operating system are you using as I have mine on Windows 7 and for some reason it wouldn't install the dongle so I had to manually add the driver for it. Mine was labelled Umi 4.1 if that helps.


---
**Rodney Huckstadt** *October 24, 2016 10:44*

windows 10 here and works a treat


---
**John von** *October 25, 2016 01:38*

Andy, my operating system is xp. how do I manually add the driver for the dongle?


---
**John von** *October 25, 2016 01:39*

Andy, where can I go online to search for another dongle motherboard


---
**Andy Shilling** *October 25, 2016 07:02*

I'm not sure, I'm new to this all. I have an old moshi board in mine and nobody used these anymore. Your best bet if to ask one of the other guys on here. You will probably be pushed to a Smoothie board as these seem to be the crowning glory for the k40 machines.


---
**John von** *October 25, 2016 23:09*

Andy, thanks for the advice. I will look into the Smoothie but they are probably expensive.


---
*Imported from [Google+](https://plus.google.com/112642592493783290997/posts/6PiDs9HrL3C) &mdash; content and formatting may not be reliable*
