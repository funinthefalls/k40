---
layout: post
title: "Does anyone happen to know of an adapter board for the ribbon cable from moshiboard to go to another controller board such as RAMPs?"
date: July 01, 2015 05:08
category: "External links&#x3a; Blog, forum, etc"
author: "quillford"
---
Does anyone happen to know of an adapter board for the ribbon cable from moshiboard to go to another controller board such as RAMPs? (I unsoldered the connector from the moshiboard and tried to make my own adapter board on perfboard but ended up ruining the connector.)﻿





**"quillford"**

---
---
**Joey Fitzpatrick** *July 02, 2015 13:57*

I believe you are looking for a middle man board.  I found a website that had all the info you need.  I made one .I had oshpark make the PCB (you get 3 pcbs for 6.95 & free shipping) and i got all the connectors from digikey. Here is the link to the site.  [http://weistekengineering.com/?p=2556](http://weistekengineering.com/?p=2556)﻿


---
**quillford** *July 04, 2015 04:37*

Thanks **+Joey Fitzpatrick** . Found it on OSH Park. Here it is for others looking: [https://oshpark.com/profiles/OCybress](https://oshpark.com/profiles/OCybress)


---
*Imported from [Google+](https://plus.google.com/115654175984463400645/posts/8LqubG7jpCw) &mdash; content and formatting may not be reliable*
