---
layout: post
title: "I recently bought a K40 laser engraver/cutter on eBay"
date: September 15, 2017 00:52
category: "Hardware and Laser settings"
author: "Douglas Cruze"
---
I recently bought a K40 laser engraver/cutter on eBay. It arrived in good shape. I aligned the mirrors without any problem. I installed the Corel LaserDraw software and entered the correct model and machine id. The laser connects. When I go to do a job the laser traces the pattern but no beam comes from the laser. I checked the power supply and tested the output test button and it indicates everything is fine. Laser works. Any ideas to try next?





**"Douglas Cruze"**

---
---
**Anthony Bolgar** *September 15, 2017 01:07*

What is your current set at? If you have a digital current display on your K40 it is a percentage of max power, not an actual mA setting.


---
**Douglas Cruze** *September 15, 2017 01:10*

It is set at 20 % but I have tried 10% and 5% when I was performing the

alignment.


---
**Ned Hill** *September 15, 2017 01:44*

Just to be clear, when you press the test button the beam hits the work area?


---
**Douglas Cruze** *September 15, 2017 01:45*

Yes it does.


---
**Ned Hill** *September 15, 2017 02:01*

I seem to recall some else had this issue and it was a bad wire or connector.  Not sure which wire you should check though.  **+Don Kleinschnitz** has a wiring schematic linked over at his blog [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Schematics)


---
**Douglas Cruze** *September 15, 2017 02:02*

Thanks. I'll check it out.


---
**Phillip Conroy** *September 15, 2017 08:17*

So laser was firing when you alignment of mirrors but not now,does it fire by pressing test button power supply 


---
**Joe Alexander** *September 15, 2017 09:38*

check the trigger wire from the controller board to the laser PSU (verify good crimps, fully seated, etc.) and that if you have the one with green connectors that they are fully pushed in. other than that its trial and error.


---
**Aart A** *July 24, 2018 12:20*

Hi there, we had the same issue! Mirror aligned, on TEST fireing, but no laser beam at the process at all. I contact the seller, and after a serious step-by step check of main  switch, cables, connectors etc they have no idea. Maybe the control board, maybe the PSU. But: I measured the voltage at GND and L, and there was static 8 Volts when no program running, and 8-8.5V jumping when a program running. It seems the Control Board send the signals to the PSU. Now I need to know, what voltage is the right to triggering the L port and drive the Laser to shoot?


---
**Joe Alexander** *July 28, 2018 01:19*

on my power supply i believe the laser is triggered when you link L to ground. to me that means the controller board is only toggling a relay (more likely a MOSFET) that pulses the condition between the two like a switch. This is how a smoothieboard handles this signal and makes sense. Perhaps the controller related chip is damaged? that's all I can think it would be if the front panel button fires and so does the TEST button on the laser PSU. make sense?


---
**Aart A** *July 28, 2018 16:27*

**+Joe Alexander** Yes it makes sense. I just measuring the follows: Power supply 8 Volts are static. When starts a program the Control Board fluctuate the 8Volts up to 9ish - the multimeter obviously slow to follow the fast signals. It seems the Control Board generate a signal to the L port / to the Laser / to trigger. The Laser head movements are perfect. I need to know, a perfect Power supply what Voltage show t the GND/L port? Because as mine is over 8Volt, it seems stopping triggering. Somejhow I read an article - was not so clear - but there was notes that the trigger signal is from 0- 3 .3 Volts somewhere. The static 8Volts just stopping the triggering?


---
*Imported from [Google+](https://plus.google.com/111671639743204148775/posts/HVHTJThtoRY) &mdash; content and formatting may not be reliable*
