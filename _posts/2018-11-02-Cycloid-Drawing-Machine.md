---
layout: post
title: "Cycloid Drawing Machine"
date: November 02, 2018 08:11
category: "Object produced with laser"
author: "Stephane Buisson"
---
Cycloid Drawing Machine



[https://www.thisiscolossal.com/2016/03/cycloid-drawing-machine/](https://www.thisiscolossal.com/2016/03/cycloid-drawing-machine/)

![images/7f428eec03225c60a7aa09f94a0ae98e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7f428eec03225c60a7aa09f94a0ae98e.jpeg)



**"Stephane Buisson"**

---
---
**Don Kleinschnitz Jr.** *November 02, 2018 13:53*

#K40Projects


---
**James Rivera** *November 02, 2018 18:48*

Reminds me of my old Spirograph! :-)

[en.wikipedia.org - Spirograph - Wikipedia](https://en.wikipedia.org/wiki/Spirograph)


---
**Mike Meyer** *November 03, 2018 17:38*

Wow, so feckin' cool!


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/TAUULE3dLid) &mdash; content and formatting may not be reliable*
