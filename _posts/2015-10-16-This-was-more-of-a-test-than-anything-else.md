---
layout: post
title: "This was more of a test than anything else"
date: October 16, 2015 04:30
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
This was more of a test than anything else. I happen to see a clear circuit posted, as in a clear PCB with the tracks visible. My brain kept racing the whole day, wondering what could be done to replicate something similar. I got home and it suddenly hit me ... acrylic ... laser engraver ... conductive ink ... and the result was a simple continuity test. Mind you, I don't think I'll continue playing with this but it was an interesting proof of concept ...





**"Ashley M. Kirchner [Norym]"**

---
---
**Stephane Buisson** *October 16, 2015 10:45*

very interesting indeed !

I see from your Youtube video you use liquid silver as conductive ink. How strong is the bonding ?

if the laser drill the "pcb", will this ink lock/bond a component into position ? (ink to metal too)

where to find out more about this ink ? (electric specs.)


---
**Ashley M. Kirchner [Norym]** *October 16, 2015 14:52*

No idea about "bonding" the components. I didn't try and likely won't. It was simply a proof of concept. I have plenty of others things I do need to be making at the moment. Perhaps at some future time I'll revisit this, but not right now.


---
**Ashley M. Kirchner [Norym]** *October 16, 2015 17:57*

Now that I'm a little bit more awake ... **+Stephane Buisson** my only recommendation to you is to try it. What I had on-hand was one of those simple "kits" that came with a conductive silver pen to draw with. It's something I just wanted to play with. I create my own electronics for driving mainly LEDs and such (from electrical design all the way to final production and programming) so the pen was just something I had laying around. Filling in the "trace" on the acrylic takes a bit since it's a ball point pen and you have to keep going over it multiple times till there's enough sliver to fill the engraved channel so that when you clean off the surface, only the stuff in the channel remains.



But once it's there, you can see in my video that it works, you get continuity from one end to the other. However, those conductive inks also have a much higher resistance value compared to regular copper traces. The small 30mm distance that I had on the straight one, the resistance on that was 3.8 Ohms where the 45mm one had a value of 22.4 Ohms. The ink is rated at 2-10 Ohms per cm. So unless you find something with a much better conductivity (which does exist, just a lot more expensive,) you're going to have to keep the traces very short.



As for actually holding parts in place, things like surface mount components might be hard to do. Through hole components however, you can simply laser "drill" the holes and press-fit the components in. Then you just have to figure out how to fill the hole with the ink so you'd have contact with the part. But as I said, your best option here is to experiment.


---
**Kirk Yarina** *October 16, 2015 18:24*

Cool!  The added resistance could be factored in for the LED current limiting and not be a problem, and CA glue could hold them in place.



A little googling found lots of types of conductive paint, shouldn't be hard to find some for experimenting.


---
**Ashley M. Kirchner [Norym]** *October 16, 2015 18:28*

Yep, straight on driving LEDs sure. Anything between 330 to 1k Ohms will be fine depending on the color of the LED. However, when you start adding controllers into the mix, and high speed signals like SPI, having resistance along the line can start to cause some headaches.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/XDby5gikfLZ) &mdash; content and formatting may not be reliable*
