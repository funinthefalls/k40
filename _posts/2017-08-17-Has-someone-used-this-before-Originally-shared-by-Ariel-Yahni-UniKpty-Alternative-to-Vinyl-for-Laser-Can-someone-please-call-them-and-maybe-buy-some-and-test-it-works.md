---
layout: post
title: "Has someone used this before? Originally shared by Ariel Yahni (UniKpty) Alternative to Vinyl for Laser Can someone please call them and maybe buy some and test it works?"
date: August 17, 2017 17:09
category: "Materials and settings"
author: "Ariel Yahni (UniKpty)"
---
Has someone used this before?



<b>Originally shared by Ariel Yahni (UniKpty)</b>



Alternative to Vinyl for Laser



Can someone please call them and maybe buy some and test it works?





**"Ariel Yahni (UniKpty)"**

---
---
**Anthony Bolgar** *August 17, 2017 17:26*

They do not say how big a sheet is, and you must buy a minimum of 10 sheets, and pay 13.60 for shipping.


---
**Ariel Yahni (UniKpty)** *August 17, 2017 17:27*

The sheet is 12x18, that's from where I read the info on them


---
**Anthony Bolgar** *August 17, 2017 17:43*

12X18 isn't too bad then, at about 2.25 a sheet.


---
**Anthony Bolgar** *August 17, 2017 17:45*

I just use a vinyl plotter that I got for  $300, it does up to 34" wide. I bought it to do lettering on my company vehicle, was cheaper to buy the plotter and vinyl and do it myself than to have a local sign shop do the graphics. And now I have it to use whenever I want, I have actually made at least 30 times the cost in profit on doing small jobs for others. (Alot of boat registry and snowmobile registry numbers)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/gXZ2UHFvCdL) &mdash; content and formatting may not be reliable*
