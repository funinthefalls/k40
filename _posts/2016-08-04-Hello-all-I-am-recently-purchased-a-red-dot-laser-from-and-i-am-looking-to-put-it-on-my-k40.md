---
layout: post
title: "Hello all, I am recently purchased a red dot laser from and i am looking to put it on my k40"
date: August 04, 2016 17:48
category: "Discussion"
author: "Jason Johnson"
---
Hello all, I am recently purchased a red dot laser from [lightobject.com](http://lightobject.com) and i am looking to put it on my k40. I have it wired up and ready to go except for the mounting. For those of you with an alignment laser, how did you mount it? Can you share pictures? Unfortunately I do not have a 3D printer to print my own that others have shared on [thingiverse.com](http://thingiverse.com). Your input is greatly appreciated!





**"Jason Johnson"**

---
---
**greg greene** *August 04, 2016 18:04*

Alibaba has a mount for it 17 bucks


---
**Jason Johnson** *August 04, 2016 18:30*

I did a quick search on there with no luck. Do you happen to know what it may be called on there or provide a link? My searches are populating pretty vast variety of selections.


---
**greg greene** *August 04, 2016 18:39*

AliExpress I told you AliBaba - but that was wrong

	

Co2 Laser Engraving Cutting Cutter Head Focus Diode Module Red Dot Positioning DIY CNC Engraver Cutter Part DC 5V

(Linda Shi)


---
**Jason Johnson** *August 04, 2016 18:55*

perfect! thank you Greg!


---
**greg greene** *August 04, 2016 18:56*

They sure use a lot of words to describe it !!!


---
*Imported from [Google+](https://plus.google.com/114673513688962471823/posts/fLGXeY156UC) &mdash; content and formatting may not be reliable*
