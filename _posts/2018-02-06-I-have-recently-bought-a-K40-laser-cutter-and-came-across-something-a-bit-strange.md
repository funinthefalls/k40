---
layout: post
title: "I have recently bought a K40 laser cutter and came across something a bit strange"
date: February 06, 2018 15:52
category: "Original software and hardware issues"
author: "Duncan Caine"
---
I have recently bought a K40 laser cutter and came across something a bit strange.  When adjusting the mirror alignment I also did a final spot test with the masking tape over the final aperture (before focal lens) and I found that the beam was always offset from centre no matter what I did.  



I took the laser head off, stripped it down, measured all the components and made a drawing.  This what I discovered.  To get the beam central on the focus lens, I need to be 1.45mm above the entry aperture centre.



Am I correct in this conclusion?

![images/c210a507ff1a59608a533bcdccbba4a2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c210a507ff1a59608a533bcdccbba4a2.png)



**"Duncan Caine"**

---
---
**Steve Clark** *February 06, 2018 17:31*

You may be. I found mine had the same problem. So I added some washers to shim it up. However, I was replacing the stock lens housing with the air assist LightObject one at the time so...



The K40 alignment is challenging with the poor mechanical design they have.


---
**Frederik Deryckere** *February 07, 2018 15:03*

may very well be true. Although it doesn't really matter after the beam entered the laser head since it focuses to a fixed point anyway.




---
**Ned Hill** *February 07, 2018 15:18*

I had shim my head up as well.


---
**James Rivera** *February 11, 2018 00:15*

This could explain why I have barely been able to cut through 1mm cardboard on 90% power. I haven't had time to troubleshoot that yet. The other thing I noticed with mine is that the large hole where the beam enters the cutting head is off-center. Specifically, the center axis of the hole does not intersect the center axis of the vertical cylinder--not even close. I dismissed it as not being much of a problem, as long as the beam hits the center of the mirror. I will reexamine that latter assumption when I look into the beam height issue noted here. Thanks!

EDIT: this may explain how the beam wanted to burn through my 3D printed air assist nozzle before I could cut with it.


---
**Duncan Caine** *February 11, 2018 08:13*

**+James Rivera** James, cardboard at 90% means you have a more of a problem than I had. Tip: when I was going through the mirror alignment procedure and had got them as good as I could (I was using masking tape) I too couldn't cut cardboard, most frustrating ...... and then I cleaned the mirrors and focal lens with acetone and bingo, straight through 3mm acrylic at about 10mA and 10mm/sec. (38%/40%) on my dial. The glue from the masking tape had deposited on the mirrors and diffused the laser beam.


---
**James Rivera** *February 11, 2018 17:20*

**+Duncan Caine** I might have to try that. So acetone is safe on the lens and mirrors?


---
**Steve Clark** *February 11, 2018 20:32*

I wouldn't. I don't know about anyone else but I use alcohol and distilled water 50/50 and good cotton balls. softly dragging the soaked ball over the surface but never pushing down on it . 



One wipe around get another clean  cotton ball do it again one wipe only. As many as you need until clean. You know it's right when it dries clean.


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/FpmKAtw5JPN) &mdash; content and formatting may not be reliable*
