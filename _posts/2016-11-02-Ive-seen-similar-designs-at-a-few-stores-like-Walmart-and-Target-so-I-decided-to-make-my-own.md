---
layout: post
title: "I've seen similar designs at a few stores like Walmart and Target so I decided to make my own"
date: November 02, 2016 16:22
category: "Object produced with laser"
author: "Jeff Johnson"
---
I've seen similar designs at a few stores like Walmart and Target so I decided to make my own. My wife claimed ownership of it as soon as i showed it to her. There aren't really instructions other than the picture and there are more of the circle pieces than I used. Just use as many as you think look good. The star is double sided with the small rectangle pieces in the center to line them up. The discs with notches are glued into the base and the star. A 1/4 inch dowel is used in the center. After you determine how many of the extra discs you want to use on the base for height, you can cut the dowel to fit. DO NOT glue the discs (without notches) and branches if you want to be able to move them but you can glue the dowel to the base and star if you like. I've attached the CDR file. This is made for 3mm MDF and will fit on a 2 300x200mm sheets.



[https://drive.google.com/open?id=0B751t-yy-x-nWjFndUgzMTJfWDg](https://drive.google.com/open?id=0B751t-yy-x-nWjFndUgzMTJfWDg)

![images/225da0dafbf5e985186404cbd2b0cf7e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/225da0dafbf5e985186404cbd2b0cf7e.jpeg)



**"Jeff Johnson"**

---


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/H7PF37HWADw) &mdash; content and formatting may not be reliable*
