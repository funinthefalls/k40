---
layout: post
title: "I've ordered a light optics laser head with air assist but forgot to order a lens"
date: April 04, 2017 22:25
category: "Modification"
author: "Phillip Meyer"
---
I've ordered a light optics laser head with air assist but forgot to order a lens. I'm in the UK so going to order one locally but what focal length should I look for to work with the standard K40 bed? There seem to be 3 options from 1.5" to 2.5". Also is an HQ Znse lens good enough? Looking at a few other threads this seems good enough for the K40 but I'm not certain. Thanks!





**"Phillip Meyer"**

---
---
**Gee Willikers** *April 04, 2017 22:32*

Shorter focal length for engraving, longer for cutting. I use a 50.8mm, most of what I do is simple engraving on and cutting of .125" hobby plywood and acrylic.


---
**Gee Willikers** *April 04, 2017 22:34*

I use the lightobject HQ ZnSe lens and head btw. Cut some small washers for both sides of the lens - the holder WILL scratch a circle into it where lens meets opening.


---
**Phillip Meyer** *April 05, 2017 08:45*

Thanks, I usually cut so,maybe a 2.5" lens would be better?


---
*Imported from [Google+](https://plus.google.com/117194752728291709572/posts/e4BAsinfFT6) &mdash; content and formatting may not be reliable*
