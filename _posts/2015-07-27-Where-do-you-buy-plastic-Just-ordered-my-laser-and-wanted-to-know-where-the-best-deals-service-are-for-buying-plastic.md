---
layout: post
title: "Where do you buy plastic! Just ordered my laser and wanted to know where the best deals/service are for buying plastic"
date: July 27, 2015 02:45
category: "Material suppliers"
author: "Tyler Tinsley"
---
Where do you buy plastic!



Just ordered my laser and wanted to know where the best deals/service are for buying plastic.



also what is the best/cheaper types of plywood i should look for locally.



thanks! i tried to search the group history but my google fu failed me.





**"Tyler Tinsley"**

---
---
**Fadi Kahhaleh** *July 27, 2015 02:53*

Basswood and other similar craft wood is great for our 40W laser.



MDF 1/8" is also ok, I need multipass with 1/4" MDF which causes it to char and burn.

(Still looking for a good air assist option that won't break the bank)



as for Plastics, I usually go with [estreetplastics.com](http://estreetplastics.com) or [eplastics.com](http://eplastics.com)



Good luck.

(P.S, your local hardware store - Menard's, Home Depot...etc- will have clear acrylic for

an acceptable price if needed immediately) 


---
**Tyler Tinsley** *July 27, 2015 03:02*

thanks for the links!



Is the plastic at hardware stores marked if it's extruded or cast? and what is the difference between the two in terms of how that effects the laser cutter. i'm lucky enough to live next to "tap plastic" that has some great scrap bins but i will eventually run a kickstarter and require a bulk order of stuff.


---
**Fadi Kahhaleh** *July 27, 2015 03:33*

ahhh good question. Not sure to be honest.

At menard's there are three types, expensive, mid grade and lower grade (everyday use)

I usually get the latter as it works great for my

needs and I haven't run into issues yet. (except the fumes problem that I can't 100% get rid off)


---
**Joey Fitzpatrick** *July 27, 2015 03:49*

I buy acrylic from Amazon.  You can get pre-cut pieces fairly cheap.   If I need something quickly,  I stop by a local plastic supply place here in my town. (most cities should have a similar supply house)  You need to make sure you avoid poly carbonate plastic(The stuff sold at hardware stores)  it doesn't cut with a laser, it just melts and gives off poisonous gasses.  


---
**Jason Barnett** *August 14, 2015 19:48*

**+Tyler Tinsley** If the plastic has a paper backing then it is cast, if it is plastic backing, then it is extruded.

I know the stuff at Tap plastics is set up this way, but I don't know if it is universal or not. You can check out the videos on their website I think one even talks about the difference between them and shows the difference when laser etching (Cast is MUCH nicer...).

[http://www.tapplastics.com/product_info/videos](http://www.tapplastics.com/product_info/videos)


---
*Imported from [Google+](https://plus.google.com/100038874802383337409/posts/VdsFx88xtg8) &mdash; content and formatting may not be reliable*
