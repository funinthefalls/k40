---
layout: post
title: "Test cut using a pattern of lines & using a pattern of dots"
date: October 26, 2015 08:53
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Test cut using a pattern of lines & using a pattern of dots. To imitate an engrave.

**+Ian Hayden**



As you will see the dots pattern didn't really work. The machine vibrated like crazy while attempting to do the dots, somehow misaligning itself in the process.



The line pattern actually worked quite well, giving an effect very similar to an engrave.



I used 6mA power, 30mm/s cut speed, 3 pixel steps.



(edit) more comment in each individual picture.



![images/6105b3a9e9d4f328765e003625c49a61.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6105b3a9e9d4f328765e003625c49a61.jpeg)
![images/fa7e94b555a0b3dc6906132903793d5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fa7e94b555a0b3dc6906132903793d5b.jpeg)
![images/38f3888e30d85dab459545eba4b4947c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/38f3888e30d85dab459545eba4b4947c.jpeg)
![images/e1a8ad3338baee79934d18bc675bc05f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1a8ad3338baee79934d18bc675bc05f.jpeg)
![images/fabb9afbef7b0a766030f727cccfa44e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fabb9afbef7b0a766030f727cccfa44e.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ian Hayden** *October 26, 2015 12:53*

thanks for trying.  Since there is no quick solution to my problem, i have started engraving all 30 at a time.  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 27, 2015 01:11*

**+Ian Hayden** You're welcome. Thought this could possibly speed it up, but doesn't really seem to be the case. Have fun engraving them all :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2015 13:03*

An update note: As I said earlier, when cutting the dot pattern the machine went crazy, vibrating like crazy. At the time I didn't notice, however it totally threw my mirrors out of alignment, so anything past 200mm width was no longer hitting. I DO NOT recommend that dot pattern. The line pattern, however, is okay though. Pretty much no vibration when that was cutting in.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/28eQ6BsE6zn) &mdash; content and formatting may not be reliable*
