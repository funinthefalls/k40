---
layout: post
title: "What would cause some places not to cut all the way through?"
date: May 28, 2017 00:07
category: "Discussion"
author: "Robert Selvey"
---
What would cause some places not to cut all the way through?

![images/0d6b3dd703d81180f1ce0603edf8896f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d6b3dd703d81180f1ce0603edf8896f.jpeg)



**"Robert Selvey"**

---
---
**Anthony Bolgar** *May 28, 2017 00:18*

More glue if it is plywood, mirrors may be out of alignment


---
**Cesar Tolentino** *May 28, 2017 00:24*

I would say glue too and wood knot...


---
**Chris Hurley** *May 28, 2017 00:44*

Well the most common reason would be different densities in the wood or glue. 


---
**Robert Selvey** *May 28, 2017 00:54*

What do you do about it run another pass?


---
**Cesar Tolentino** *May 28, 2017 01:12*

For me.  Glue is tough. If you can invert it and lase the other side, that should do the trick... But it's hard. It takes art to perfect it.



I just stay away with the material I guess.



Try two speeds two passes. One will be faster and the other one slower. For my 5mm ply I do 500mm/min first pass, 300 on 2nd pass at 8ma power on a 4 year old tube.


---
**Mike Meyer** *May 29, 2017 20:41*

Cover the surface with masking tape; make one pass around 8ma. Without moving the work piece, gently tap the surface of the material; it'll be pretty obvious if you've cut completely through. If not, take another another swack at it. As has been mentioned, glue, knots, surface irregularities, etc. all contribute to incomplete cuts. BTW, the masking tape will eliminated surface charring and give you a clean cut.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/imjoe4vR8RB) &mdash; content and formatting may not be reliable*
