---
layout: post
title: "So I've been messing around with engraving and I feel like I'm jumping ahead without understanding the capabilities"
date: September 09, 2016 16:18
category: "Discussion"
author: "Adam J"
---
So I've been messing around with engraving and I feel like I'm jumping ahead without understanding the capabilities. From my test runs I'm guessing I cannot engrave greyscale images using LaserDraw or Coreldraw using the regular board? (since it seems to only engrave pure black areas).





**"Adam J"**

---
---
**Ariel Yahni (UniKpty)** *September 09, 2016 16:24*

Yes and no. Not pure greyscale but you can aproximate with pre processing the image by applying dithering. 
{% include youtubePlayer.html id="TiEOXElDbHc%EF%BB%BF" %}
[https://youtu.be/TiEOXElDbHc﻿](https://youtu.be/TiEOXElDbHc%EF%BB%BF)


{% include youtubePlayer.html id="TiEOXElDbHc" %}
[youtube.com - China Laser Engraver Cutter, Image preparation the gimp](https://youtu.be/TiEOXElDbHc)


---
**Jim Hatch** *September 09, 2016 16:31*

Ariel is correct. You have 2 options - burned or not burned. Black & white. Dithering allows you to approximate grayscale.



If (when) you upgrade to a Smoothie controller and LaserWeb 3 software, you'll get grayscale. The difference is that the stock controller & software can't do variable power. For grayscale you need to be able to say "burn this pixel lightly" or "burn this pixel a lot".


---
**Adam J** *September 09, 2016 16:48*

Thanks for the response guys. So anything grey is ignored with a stock controller? This seems to be what's happening, just want to confirm.


---
**J.R. Sharp** *September 09, 2016 17:02*

What kind of dithering?




---
**Ariel Yahni (UniKpty)** *September 09, 2016 17:13*

**+J.R. Sharp**​ the video above uses Floyd, I would says a very good option 


---
**Jim Hatch** *September 09, 2016 19:16*

I usually at least look at each of the dithering options - Floyd does seem to be the one I generally end up with. 


---
**Adam J** *September 09, 2016 20:31*

I know there's other threads about the topic and I am currently looking through, but how difficult is the Smoothie upgrade? I've seen digital displays and stuff and I'm not too bothered about those, is it simply a case of purchasing and replacing the board / plugging it in? Or is that wishful thinking? ;-)


---
**Ariel Yahni (UniKpty)** *September 09, 2016 20:34*

**+Pikachupoo**​ depends, as anything in life there is the easy and the hard way. If you want easiest talk to **+Scott Marshall**​ he manufacturers some easy add one to make it super simple to connect the stock board to a smoothie


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/BpmjJ1vkkJF) &mdash; content and formatting may not be reliable*
