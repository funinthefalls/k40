---
layout: post
title: "Shared on February 28, 2015 19:18...\n"
date: February 28, 2015 19:18
category: "Discussion"
author: "Tim Fawcett"
---




![images/825a1037bd04a346e812af7fb7e10412.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/825a1037bd04a346e812af7fb7e10412.jpeg)
![images/90d9cd068d6868d53dd7ce94644ad9ee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90d9cd068d6868d53dd7ce94644ad9ee.jpeg)
![images/b07780774c8150ab28ba335bd32e9b74.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b07780774c8150ab28ba335bd32e9b74.jpeg)
![images/1d7359e26edfca2b3e257c79a2a43659.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1d7359e26edfca2b3e257c79a2a43659.jpeg)
![images/78949bb25db42a7239304bb4a788a8f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/78949bb25db42a7239304bb4a788a8f0.jpeg)
![images/4a4bb67824a0204741563341dbaeab03.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4a4bb67824a0204741563341dbaeab03.jpeg)
![images/12521369cedf330560567d7158af8230.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/12521369cedf330560567d7158af8230.jpeg)

**"Tim Fawcett"**

---
---
**Tim Fawcett** *February 28, 2015 19:23*

OK, so I have got the interlock controller built and the software loaded up. It provides door interlocking, emergency stop of the laser output, and shut down in the event of the laser or coolant going over 28C or the water flow failing or dropping below about 8L/min (my circulation system runs about 10-12l/min). TThe4 cotrol allows the flow or temperatures to be viewed and pressing and holding the button for 1 second with the door open ovverides the interlocks so you can perform maintenance such as aligning the laser. This resets when the door is closed.  I have also done a board which will break out the controller connections so I can swap between Moshiboard and Marlin (and/or possibly Smoothieboard) until I have the replacement controller up and running well


---
**Sean Cherven** *February 28, 2015 19:43*

Can you provide circuit and component details? I would like to start building one myself! Also, where did you get the white faceplate with the words printed on it?


---
**Sean Cherven** *March 03, 2015 22:33*

Any updates on this?


---
*Imported from [Google+](https://plus.google.com/113171525751920354895/posts/LWd8CwLStaW) &mdash; content and formatting may not be reliable*
