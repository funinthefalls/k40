---
layout: post
title: "Can anyone tell me the difference between Light objects improved and high quality mirrors?"
date: May 28, 2016 17:24
category: "Modification"
author: "Alex Krause"
---
Can anyone tell me the difference between Light objects improved and high quality mirrors? I notice there is a significant price difference just wondering if anyone noticed an difference with cutting performance 





**"Alex Krause"**

---
---
**Ariel Yahni (UniKpty)** *May 28, 2016 17:27*

I will need to get one of those sets also


---
**Julia Longtin** *May 28, 2016 19:36*

I used chopped up hard drive platters for my 100W laser.


---
**ThantiK** *May 28, 2016 19:38*

**+Julia Longtin**​ when/how did you learn about this? Any more info? I've got tons of hard drives laying around. 


---
**Julia Longtin** *May 28, 2016 19:59*

you have to use the silver ones (not the dark ones). I haven't published anything on this, as i am in the midst of adding thermistors to my mirrors.


---
**Ariel Yahni (UniKpty)** *May 28, 2016 20:08*

**+ThantiK**​ [http://diylaser.blogspot.com/2012/01/get-your-co2-laser-mirrors-for-free.html?m=1](http://diylaser.blogspot.com/2012/01/get-your-co2-laser-mirrors-for-free.html?m=1) I'm going to give it a shot also. 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/3TVebnzx6ma) &mdash; content and formatting may not be reliable*
