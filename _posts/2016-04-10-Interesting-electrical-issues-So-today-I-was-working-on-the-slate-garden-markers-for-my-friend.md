---
layout: post
title: "Interesting electrical issues So today I was working on the slate garden markers for my friend"
date: April 10, 2016 03:38
category: "Discussion"
author: "HalfNormal"
---
Interesting electrical issues



So today I was working on the slate garden markers for my friend. After 4 hours running, and before a marker was finished, the GFI breaker blew. Swapped outlets on a different circut and it blew too after a few minutes. Ran an extension cord to a non GFI breaker and no issues. The most interesting part is the power supply squealing almost went away. Things that make you go mmmm!





**"HalfNormal"**

---
---
**HP Persson** *April 10, 2016 08:48*

Measure up the connectors and all ground points with a multi meter or similar to see that it´s grounded properly.



GFI compares the current on hot/neutral side and if it sees a difference in a few amps it triggers. If it triggers it may be due to bad ground.



The strange thing is that you can run it for a few minutes before it trips...



Are you running it on 110 or 230v?


---
**Scott Thorne** *April 10, 2016 11:22*

Most gfi's have current ratings...chances are that it's pulling the max amps that the gfi can handle. 


---
**Scott Marshall** *April 10, 2016 13:37*

As Mr Person said, Ground fault Circuit Interrupters  compare current on the hot line to current on the neutral, and if it varies any more than a few milliamps (the dangerous level), they trip. They however won't function properly with a bad ground and can't protect against it. If you have any doubt your house wiring isn't properly grounded you test it with a plug in outlet/GFCI tester. They're under 10 bucks and should be in any experimenters tool box.



The principle behind a GFCI is if all the current entering via the hot line (black wire) is not returning through the neutral(which is the 0v line or Functional ground, white wire in the US), then it must be going somewhere else, possibly through a human who is touching the device and a ground.



It'a good thing. Heed the warning! 



If the sound of your power supply changes, it's probably leaking high voltage to ground. It sounds like you have a progessive power supply failure which has been getting worse, and is now critical.



You can test the K40 with a GOOD multimeter, preferably digital, Put it on the 10 or 100 meg ohm scale and check from line to chassis. Switch On, K40 unplugged, check at the plug, measure both power terminals to the ground. If you have any reading at all besides infinity, disconnect the power supply Hot wire and see if it goes away. You can then check the power supply with it disconnected to confirm the fault. (same test, line terminals to chassis. This will show any leakage in the "off state", but won't show if you have high voltage leakage, as it jumps gaps. It's not 100% accurate, but if it shows any leakage at all, it's a problem for sure. 

To do this properly with 100% certainty, you need a device called a Megger, which is an ohm meter that uses high voltage to test. 



If you're tripping the GFCI, there's a safety issue. 



As Scott mentioned, they DO have a current rating, but that's a maximum safe current, but not a trip rating. They don't include a circuit breaker. Code (and safe practice) requires both a GFCI and Circuit Breaker in residential wiring. Code calls for GFCI's specifically within 6 feet of a water source (average armspan), in all bathrroms or in any outdoor or wet location. They a good idea on every outlet (one can protect the entire circuit - they have output terminals)

They've saved a lot of lives, and rarely nuisance trip. If they do nuisance trip, they should be replaced.



Be Safe,

Scott


---
**HalfNormal** *April 10, 2016 15:29*

**+Scott Marshall** **+Scott Thorne** My money is on a powersupply ready to fail. I had been using the same outlet for months until yesterday. I have been using more current to engrave the slate so it only makes sense that I am pushing the powersupply harder and to its ultimate demise! So my dilemma begins, do I purchase another exact replacement or purchase one from light object which is claimed to be better quality and maybe one that will handle a 50 or 60 watt tube in the future? Decisions, decisions.


---
**Scott Thorne** *April 10, 2016 15:36*

**+HalfNormal**...I bought the one from light objects...while it works good...it's massive...I caught hell installing it in my machine and I have a much bigger machine than the k40....I only get 22 mA's max out of the new one too...the sales article claims 25...I have yet to see that....one other thing..the direct replacement on ebay is 95.00....the one I have is 245.00...while I do admit it looks like it's built better. 


---
**Scott Thorne** *April 10, 2016 15:37*

**+HalfNormal**...let me add that the one that came in my 50 watt machine is identical to the one that was in my k40!


---
**HalfNormal** *April 10, 2016 15:59*

To get ahead of the game, I ordered a new powersupply from Ebay for $95.00 with shipping from NH. With USPS it should be here by Friday. 


---
**Scott Thorne** *April 10, 2016 16:10*

**+HalfNormal**...good choice bro...I could have ordered 2 for what I paid for the one I have now. 


---
**Scott Marshall** *April 10, 2016 20:54*

It seemed to me you asked about it making noise a while back.



Probably the beginning of the end.



Hopefully it'll hold till the new one arrives. Be careful running the old one in the meantime, 15kv shocks are unpleasant or worse. 



Knocking on wood, mines's holding.....



Scott


---
**HalfNormal** *April 10, 2016 22:14*

**+Scott Marshall** Cut my teeth in electronics on old tube tv's. The first thing our instructor did was give everyone a wooden stick to poke around the HV powesupply. ﻿


---
**Scott Marshall** *April 11, 2016 01:53*

You know your way around that supply then.

I get concerned, some people don't understand what a 300w 15kv supply is all about. As you know, It's NOT like static electricity, or a car ignition.



 As I'm sure you know wood is a pretty good conductor above about 5 kv.

We have something in common, Worked at a TV shop during the 70's and early 80 thru college. Saw a lot of tube sets in those days, then hybrids and modular Solid state. I have a couple of Zenith triplers around (Circa 1976), used to use them with a neon sign xfmr for making Kirlian photos and feed supplies for Tesla coils. I wish they had enough guts to supply the current we need, but they were intentionally designed to limit the current for safety. 


---
**HalfNormal** *April 11, 2016 04:38*

**+Scott Marshall** Ah Kirlian photography and Tesla coils. You forgot Jacobs Ladders! Way too much fun back then. My Tube test equipment would be worth a fortune now if I had kept it. Found an old HeathKit color bar generator I had when I was moving. It had been destroyed due to the humidity and poor conditions I subjected it to. Great memories!


---
**Scott Marshall** *April 11, 2016 05:01*

Lol, had ladder over my the tube rack at the TV shop. Half the customers loved it, the other half were scared to death. Just a sign xfmr and a couple welding rods, but it was just amazing to most people.



I've got a few Heathkits from back then too, a TV CRT rejuvinator (hard to find a CRT in any condition these days), and a nice (for the time) 20mhz dual trace scope. The scope needs a focus resistor. Hard to come by, and like you said - it was killed by humidity. Sold all my Ham Gear about 25 years ago, too bad, that stuff is still quite desirable and ages well.



Still have a few thousand tubes, they're not really worth much, I ran thru all the 6l6s and 6550s long ago. Maybe my sons will find a use for them in another 50 years...



Too bad I didn't run into you back in the good ole days.... 


---
**HalfNormal** *April 11, 2016 13:01*

**+Scott Marshall** That was when Electronics was really Electronics. Now all you have to do is program an IC and you're done.


---
**Scott Marshall** *April 12, 2016 14:40*

The time you speak of: Back when LEDs were exotic, expensive, and came in any color you wanted as long as it was red. Transistors came in 2 kinds Germanium and Silicon, or NPN and PNP, a Gate was something you walked through, TVs were available in Oak or Cherry. A phone card was known as a Blue Box. Practical lasers used ruby rods and flashtubes, Cable TV was a questionable experiment, DOS was new, and not the best choice. Your car's Diagnostic port was the hood.



I built a computer from scratch (1978), (I was a Jr in HS) - it took rows and rows of 2114 memory chips 32k x 8 bits total (Huge back then), 70a@5v, powered a tape drive, 2mhz 6502 (overclocked x2, probably the 1st overclocking ever, Used a Teletype for printing (110cpM, all caps)and papertape strorage. An old 12" Zenith BW TV was the display.

Primitive as it seems now, it beat all comers for speed, even managed to do 2 meter moonbounce antenna tracking with it. Games were Lunar lander (on the TTY) and Chess or Qubic.

Still have the computer, TTY and power supply. Hasn't been powered on in 20 years.



I miss my old stuff, the smell of the old tech rosin solder, tubes burning in, and a little ozone off the finals.... A cup of coffee on the radio would boil dry while you if you didn't drink it in an hour. You don't see 50 foot antennas on houses anymore. Sadly, even mine.



Now the innovations are like you say, largely firmware and software improvements.

Pics are nice, but I fear, as a result of downloadable "easy tech", our new engineers/experimenters will be lost to the basics.



This laser group is as close to that old experimenter mind set as I've seen in a long time. People doing things the way they can with what they have, because they can. It's got the advantage of being global, which we never dreamed of using our computers for in 1978.



The canned solutions available today are both a luxury and a curse to folks like us. It's progress I suppose, but not nearly so much fun as it was "doing things the hard way". I suppose our kids will look upon these as the "good old days before optical computers"



Scott



Sorry for getting so far off track...


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/WkxD9ZfpB3y) &mdash; content and formatting may not be reliable*
