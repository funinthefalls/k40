---
layout: post
title: "60% of 40w at 250mm/s Originally shared by Gee Willikers Slate test"
date: October 19, 2015 02:04
category: "Object produced with laser"
author: "Gee Willikers"
---
60% of 40w at 250mm/s



<b>Originally shared by Gee Willikers</b>



Slate test.



![images/0ca75ea18a9a71237c42bfb29c6104bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0ca75ea18a9a71237c42bfb29c6104bd.jpeg)
![images/5015d7f2a36111d0f8783c5ed679f9d1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5015d7f2a36111d0f8783c5ed679f9d1.jpeg)

**"Gee Willikers"**

---
---
**Gee Willikers** *October 19, 2015 02:22*

With clear coat: [https://plus.google.com/+JeffGolenia/posts/Vktj8BQQ9LU](https://plus.google.com/+JeffGolenia/posts/Vktj8BQQ9LU)


---
**Mike Hull** *October 19, 2015 09:13*

That looks great


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/4QTzLs9PSxV) &mdash; content and formatting may not be reliable*
