---
layout: post
title: "I think someone had shared this video a year or two ago, but I don't see it anymore"
date: July 30, 2018 21:53
category: "Modification"
author: "Ned Hill"
---
I think someone had shared this video a year or two ago,  but I don't see it anymore.  I had done this for my fan so I thought I would reup this K40 Stock fan mod.  It definitely increases the efficiency of the stock fan, not huge but is noticeable.  After adding the spacer on mine I had to grind down the shaft hole on the squirrel cage to expose enough thread to get the nut to grab on.  Just used a coarse sanding drum on the dremel.  #K40Exhaust




{% include youtubePlayer.html id="fpFHMRyNtFw" %}
[https://youtu.be/fpFHMRyNtFw](https://youtu.be/fpFHMRyNtFw)





**"Ned Hill"**

---


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/f9UEx7j7DPN) &mdash; content and formatting may not be reliable*
