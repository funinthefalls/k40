---
layout: post
title: "Crystal Ball, is my power supply failing?"
date: April 04, 2016 04:39
category: "Hardware and Laser settings"
author: "HalfNormal"
---
Crystal Ball, is my power supply failing?



So I ran my laser all afternoon making up for lost time. My power supply is starting to make a "squealing" noise when it first starts. Not all the time but about 50% of the time. It lasts about 2 or 3 seconds sometimes. If you have ever heard a car's fan belt when it is loose, this is what it sounds like. I know the power supply is a switching type so there are high frequencies involved but something tells me this is not normal. Anyone else have this issue and was it a tell tale sign of impending doom? 





**"HalfNormal"**

---
---
**Scott Marshall** *April 04, 2016 09:04*

Only proceed if you are familiar with working around high voltage, and understand the risks. Keep hands, tools, and wires well away from the HV elements, they do not have to touch to draw an arc, and standard insulation won't stop the HV from jumping into/out of a wire. I've had a couple of close calls with HV systems, and they both involved what I thought was a safe tool. Don't trust screwdriver handles etc to protect you. Eyes only when it's on.



One tool you can use with safety is a length of vinyl tubing (clean and dry) - put one end near the suspect source, and listen at the other, thus using it as a stethoscope. Keep your hands well back from the power supply and don't lean on the chassis while searching.



Remove the power supply cover (disconnect the fan so you can get it out of the way)  and see if it's coming from the high voltage output transformer. It's in the rear half in it's own space. While in there turn the room lights out fire it up, keeping a close eye out for arcing or what's called corona discharge (looks like a blue glow and is indication of an arch trying to establish).



Does it get worse with higher current settings? If so it may just be switching squeal as you mentioned. If the output transformer windings were not properly potted they can move making the noise. (and causing internal arcing.



Often the inductors in a HV supply will vibrate nearby metal, causing the noise to seem to be coming from elsewhere (or nowhere)



Watch for the smell of ozone, and indicator of corona discharge.



Hope you find it easily.



Scott


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/SL6quDbA2mU) &mdash; content and formatting may not be reliable*
