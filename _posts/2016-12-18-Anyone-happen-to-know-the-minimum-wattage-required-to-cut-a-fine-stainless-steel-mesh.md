---
layout: post
title: "Anyone happen to know the minimum wattage required to cut a fine stainless steel mesh?"
date: December 18, 2016 16:33
category: "Discussion"
author: "Kelly S"
---
Anyone happen to know the minimum wattage required to cut a fine stainless steel mesh?  It's super thin, .3 or so mm.





**"Kelly S"**

---
---
**Anthony Bolgar** *December 18, 2016 16:49*

All depends if you use an oxygen or nitogen assist with the cutting. A 230W with high pressure nitrogen can cut 0.16 cleanly at a decent feed rate. Extrapolating that to 0.3 thick and I think you are better off buying a galvo laser than a CO2 for that purpose.


---
**Kelly S** *December 18, 2016 16:51*

Alright thanks, have a few projects that require small squares of ss mesh and using scissors gets old after a while lol.  Seen a video of a 40 watt machine and was curious if the k40 could do it.  Looking to build my own off of the freeburn plans and was curious if a c02 was up to the task. 


---
**Anthony Bolgar** *December 18, 2016 16:57*

Before discounting the idea, you should get an accurate measurement of the thickness, and do a little more research. The 230W value I stated was from the trotec web site, other manufacturers may have different ideas of what it would take. Maybe talk to a tube manufacturer?


---
**Steve Anken** *December 18, 2016 18:03*

Anthony, do you know anybody using laserweb for galvo? Any hybrid systems that also use a gantry to extend the printable area? For production etching the galvo is killer but I've only seen stand alone versions with small print areas.


---
**Jonathan Davis (Leo Lion)** *December 18, 2016 18:23*

I wouldn't dare cut metal unless the work area is extremely well vented. knowing most lasers you can be talking about dealing with a  fire hazard very easily.


---
**Anthony Bolgar** *December 18, 2016 19:19*

No one I know yet, but I think **+Scott Thorne** has one on the way that he will be upgrading to a smoothie based system.


---
**Scott Thorne** *December 18, 2016 19:55*

**+Anthony Bolgar**...this is true...i have to figure it out when it gets here.


---
**Madyn3D CNC, LLC** *December 18, 2016 20:46*

**+Jonathan Davis** Hi there! Care to elaborate a little on the potential fire hazards involved with cutting metal? Assuming we're not talking about a neglected ventilation system with built-up resins and soot. I'm interested in the fire hazards directly related to cutting metal. 



Just curious, as metal is probably the most prominent medium to be lased in my machines...  for a few years now. 


---
**Jonathan Davis (Leo Lion)** *December 18, 2016 20:48*

**+Madyn3D CNC, LLC** Mostly the effect of sparks coming off of the metal. that land on a nearby work areas.


---
**Kelly S** *December 18, 2016 21:04*

Ah that wouldn't be a issue with anything I run, fully enclosed with good fan.   Even first 3/4s of my ducting is metal.


---
**Jonathan Davis (Leo Lion)** *December 18, 2016 21:05*

**+Kelly S** But you do have a fire proof insulator though at least for protection.


---
**Kelly S** *December 18, 2016 21:08*

It runs free air to the exit point, if there was a fire it'd be noticed immediately and extinguished. 


---
**Jonathan Davis (Leo Lion)** *December 18, 2016 21:14*

**+Kelly S** Good.


---
**Kelly S** *December 19, 2016 02:41*

Like butter >.>  first test I think was too hot lol.

![images/b8678703bb93e91fc7ad92a80b9cb575.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8678703bb93e91fc7ad92a80b9cb575.jpeg)


---
**Kelly S** *December 19, 2016 03:29*

Got it tuned in, if anyone was wonder, the k40 can cut ss mesh very easily.  


---
**Kelly S** *December 19, 2016 05:10*

Yep i got it.  :D

![images/428fdc0e2762b4a1e9c98be65c092323.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/428fdc0e2762b4a1e9c98be65c092323.jpeg)


---
**Anthony Bolgar** *December 19, 2016 06:06*

Awesome. Seeing is believing. Good to know for the future.


---
**Ned Hill** *December 19, 2016 14:25*

Lol Kelly reminds me of screens used in a certain type of smoking device I used in my younger days. ;)


---
**Kelly S** *December 19, 2016 14:27*

**+Ned Hill** lol Ned, I gave that up years ago.  These are for tube caps for ant keeping projects I have.  :)

![images/44b2542e3cc770641279593a3e4122dd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44b2542e3cc770641279593a3e4122dd.jpeg)


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/8jybjw9VTNm) &mdash; content and formatting may not be reliable*
