---
layout: post
title: "It is going to take some time to get used to using a 60W laser"
date: October 04, 2017 03:13
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
It is going to take some time to get used to using a 60W laser. Cutting is no problem, power settings were easy enough to figure out. But engraving is a little harder. 20% power at 350mm/s is about the highest power I can run to get nice engravings on plywood and mdf. Photos are my hardest one to figure out right now. I guess this just gives me an excuse to play with the laser more. :)





**"Anthony Bolgar"**

---
---
**Paul de Groot** *October 04, 2017 05:18*

Well that's the complexity of engraving. It's all about the tube's characteristics. I assume that a high powered tube might have more problems in managing low power ranges. I might not even go below a certain grayscale but just switches off. I found the 40 watt already very challenging to produce nice grayscales.(2 mA range)


---
**Anthony Bolgar** *October 04, 2017 05:28*

On the plus side it blows through 6mm plywood or mdf like a hot knife through butter.




---
**Jason Dorie** *October 04, 2017 07:43*

Try slower and lower power - you want to scorch the wood, not vaporize it.  :)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/ZE61RaRe8L7) &mdash; content and formatting may not be reliable*
