---
layout: post
title: "Oh no !!! Im having a problem My K40 has worked fine for a year or two but now , it still cuts but I need to give it more power to work as well as it did before"
date: March 09, 2018 12:21
category: "Discussion"
author: "Don Recardo"
---
Oh no !!!  Im having a problem 



My K40 has worked fine for a year or two but now , it still cuts but I need to give it more power to work as well as it did before. More importantly the beam has got much wider.

My first thought was . align the mirrors and bed height again  and thats when I found something odd

To line up the mirrors I always put a bit of paper over the hole in the laser head. Did a test fire  and adjusted the mirrors till the small burnt spot was in the centre

but now I see I am not getting a small burnt spot , instead I am getting a ring like a letter " O " on the paper . The ring is about 2mm diameter and its only the edge thats burnt , the center is untouched .

What could cause this and is there a cure

I will try to add a pic to show the beam as it enters the laser head 

the ruler is in mm for scale 



Regards

Don

![images/39c8d44a44c1da4f7356d73e28b943bb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/39c8d44a44c1da4f7356d73e28b943bb.jpeg)



**"Don Recardo"**

---
---
**Ned Hill** *March 09, 2018 18:10*

It appears your laser tube has entered into a TM01 mode.  As far as I know it can't be fixed and will require a tube replacement. 

![images/1a63824fa519c14b0036f66bb499ef7b.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/1a63824fa519c14b0036f66bb499ef7b.gif)


---
**Don Recardo** *March 09, 2018 18:16*

Thanks for the reply but what do you mean TM01 mode


---
**Don Recardo** *March 09, 2018 18:19*

Sorry the picture showing what you meant hadnt appeared on my screen untill I clicked the mouse


---
**Ned Hill** *March 09, 2018 18:21*

Sorry it should have been TEM01 not TM01.  The power density across the diameter of a laser output beam is not uniform and is dependent on the laser active medium, its internal dimensions, optical feedback design and the excitation system employed. The transverse cross sectional profile of a laser beam, which shows its power distribution, is called the transverse electromagnetic mode (TEM).   The higher number TEM modes have a greater spread in intensity which makes it harder to focus into a fine spot. 


---
**Ned Hill** *March 09, 2018 18:23*

Nominally these CO2 lasers tubes are designed to operate in the TEM00 mode, but as they age with use they can drift into higher modes.


---
**Abe Fouhy** *March 10, 2018 05:48*

Could it be a dirty lens in the center too?


---
**Don Recardo** *March 10, 2018 08:41*

**+Abe Fouhy**  no . It can't be the lens . The picture of the burn is taken before the beam reaches the head or lens. Basically it's the shape that the beam must be in as it leaves the laser tube


---
**Abe Fouhy** *March 10, 2018 12:32*

Gotcha. New tube


---
**James Rivera** *March 10, 2018 20:06*

I’m a newb, but I think this means your tube is nearing its end of life. More info on TEM01:



[en.m.wikipedia.org - Transverse mode - Wikipedia](https://en.m.wikipedia.org/wiki/Transverse_mode)


---
**James Rivera** *March 10, 2018 20:08*

Jeez, only now do I see the other comments. Thx G+.


---
**Don Recardo** *March 10, 2018 20:31*

Cheers James. I accepted the others view that it's goodnight Vienna for my tube. This afternoon I ordered a new 60w tube. 

I have already got the rails and carriages to make a 1000mm x 500mm  machine so with a 60w tube it should be a usefully bit of kit


---
**Tim Gray** *March 11, 2018 22:45*

Tubes have a definite lifespan, you got 2 years so you got lucky and had a lot of good use out of it before it drifted out of a useable beam pattern.



Be sure you test your new tube right away, china shippers will gleefully send you a defective tube that is already in TEM01




---
**Don Recardo** *March 11, 2018 22:59*

**+Tim Gray** thanks for the heads up tim. I will make sure i test  it right  away


---
*Imported from [Google+](https://plus.google.com/106290033771296978518/posts/5TYouz5cESF) &mdash; content and formatting may not be reliable*
