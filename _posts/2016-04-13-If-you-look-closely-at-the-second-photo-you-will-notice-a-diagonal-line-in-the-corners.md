---
layout: post
title: "If you look closely at the second photo you will notice a diagonal line in the corners"
date: April 13, 2016 15:52
category: "Discussion"
author: "Tony Sobczak"
---
If you look closely at the second photo you will notice a diagonal line in the corners. that does not show up in corel draw. This was downloaded from thinginverse.  It came in dfx ai and eps formats. I also downloaded another item that came in dwg and dfx.  The result was the same in the dfx but not the dwg format. I tried saving in different formats and it never shows up except on the cut.  Help please help.



**+Phillip Conroy** other formats print fine, no issues so I believe all setting are correct.



+Scott Marshall since a freebie, I don't think that's an issue.



**+Tony Schelts** Looks like its only one layer.



![images/c2e17d070837dcf92e31beb8da4bedb7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c2e17d070837dcf92e31beb8da4bedb7.png)
![images/0271dd75bb5b4cf08cbc44bb7d51aabc.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0271dd75bb5b4cf08cbc44bb7d51aabc.png)

**"Tony Sobczak"**

---
---
**Scott Marshall** *April 13, 2016 16:19*

I agree with Tony, it's probably on a hidden/frozen layer, I'd guess the original draftsman used them as a reference, or polyline placeholder before drawing the arcs and then turned the layer off. 

I've had a similar problem, I tried using layers of cutlines and engraving, turning off the unwanted layer, and exporting a dxf, and Corel somehow sees ALL layers, then merges them, even though you didn't think they exported.



It seems that exporting (from Draftsight anyway) a .dxf or .pdf file with hidden layers carries them into the output file. Then Coreldraw unfreezes and merges them whether or not you want it to or not.

I had to go back, copy my drawing from a master into 2 additional files, one for cut (with engraving ERASED), and another for Engraving (with cutlines ERASED). Then I outputted the 2 dxf files, and had to run engrave, then cut without moving the workpiece.



A MAJOR PIA, As soon as I can get a Smoothie, Corel is history.



I never found a way to fix the drawing once in Corel.



For it to work in Corel at all, the original drawing must be in layer 0, white, default linewidth. All other layers MUST be empty, or you'll be seeing them again...



I think your only fix is to convert the downloaded dxf back to a dwg, erase the lines in Draftsight or Autocad, then recreate the dxf for corel.

There may be another way, but I don't know it. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 17:00*

If you have the AI file, I can remove them for you & then send back as a CorelDraw CDR file if you would like. I haven't worked with dxf before, but looks like Scott knows what he's talking about.


---
**Scott Marshall** *April 13, 2016 17:42*

.dxf was invented by Autodesk for Autocad 11 as a very early (probably the 1st) Vector line file  for plotting Autocad drawings on a XY flatbed pen plotter (Circa 1985 ish). (My era) Since then it's gradually become kind of a public domain vector file.



I'm fairly competent in Acad and it's clones, a hacker in Corel and totally lost in Sketchup. There very well may be a fix in Corel, but it beyond me. Of course, I'm trying to learn it on this wonderfully unsupported version from China...



Yuusuf's your man.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 18:10*

**+Scott Marshall** Yeah I have no clue with ACAD or similar software. Recently I've started playing around with Sketchup & found that it was very difficult to work with. I wanted to use it for 3D modelling, however I just can't seem to figure it out (& I don't really like watching long winded tutorial videos).



But yeah, I'm happy to take a look at the AI file if you have it available **+Tony Sobczak** & I can remove the angled lines on the corners & output as a file that will work.


---
**Scott Marshall** *April 13, 2016 21:03*

**+Yuusuf Sallahuddin** The thing I discovered (thanks to some helpful folks over on Openbuilds) is that Sketchup besides using 3d command structure (surface oriented rather than line oriented) which is nothing like the old school Acad and Lotus logic guys my age learned on, is plug-in driven, and once you have Sketchup going, it's only a start. (and they don't tell you that up front)



 If you want to use the drawings for anything, you need plug-ins to even get them to a form CAM software can chew on. Even many of the drawing tools are optional plug ins. So ultimately, each persons Sketchup is as personal and unique as he is and how he "optioned" it. 

That makes a horrible learning curve even worse, as your version doesn't work the same as mine (unless we share all our plugins and settings), so peer to peer assistance is tough at best.

I ultimately decided I'm happy with my old 2d Autocad (now only available as a very expensive subscription)- My $6000 licensed copy from my business has been "obsoleted" ala Microsoft.  Draftsight is a free clone, and works very well for us old guys from the 386 days.

Autodesk is pushing Fusion 360, an Adobe like all inclusive "take over your life cloud driven" behemoth, with the same sort of learning curve as Sketchup. It's targeted at Boeing and General Motors, not ma and pa shops like mine was. It is at least free if you make under 100 grand /year. 

It's a tough thing if you're hobbyisst like us. I've been looking hard for about 2 years 3? and still haven't found anything better than DraftSight which isn't a artsy answer either, I'd like to be able to do pretty laser photos and such, as well as mechanical constucts for which I am happy with Draftsight.



This struggle has led me to try a Smoothieboard and the Opensourceware (Smoothieware I think it's called) available for it. (as soon as I can get one) There's Stephanie and Peter here (and others) who I know will help me sort it out, That's worth a lot

My CNC mill gets along with Mach 3/draftsight fine,(still a beginner on that too) but it would be nice to do some 3d stuff on that. Visicut has a following amongst the wood guys with the CNC routers, but I can learn only so many of these programs before my head explodes.



Sorry to run on on the subject, but right now, the lack of good quality software for small scale CNC is our biggest stumbling block. Corel looked like an option, but they too seem to have gone into the cloud subscription structure and want it all. Seems nobody wants to sell you a functional program and maybe offer updates every few years. They could all take a hard look at X plane and Mach 2,3,4 for what makes us happy. Or me anyway.



Compatibility and uniformity among command structures is nonexistant.

There isn't even a universal hardware interface like Mach 3 uses (keep coming back to Mach!)



 I and most of us I'd imagine, gladly would pay 2-$300 for a good quality drawing editor with a relativity intuitive command structure that will service our Mill, Lathe, Router, Laser, Plasma, 3d Printer or Waterjet. (Mach does, I sure wish Artsoft would write a drawing suite, then we'd have a keyboard to steel (or wood or whatever) solution.



Sorry again, for my rant, it's a pet peeve.



Scott


---
**Tony Sobczak** *April 13, 2016 21:40*

**+Yuusuf Sallahuddin** Here's another similar file that has the same issue. This one has two 180 arc's that have the same issue when cutting.  This is in the ai format also.



[https://www.dropbox.com/s/3h1grmij6yj3eli/Flex_Box_v3.ai?dl=0](https://www.dropbox.com/s/3h1grmij6yj3eli/Flex_Box_v3.ai?dl=0)



Waiting t here what you find out.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:06*

**+Scott Marshall** Yeah, I have a few peeves similar to that regarding quite a lot of topics, so don't get me started haha.



But, I can see where you're coming from. Albeit, my first pc was a 486, so I'm a little bit newer to the scene than you by the sounds of it & I didn't really get into graphic design related stuff until around 2000 odd, when I finally got a decent pc that could handle it (AMD K6-2).



**+Tony Sobczak** I'll take a look now & see what is up with the file. Hopefully there is something that is noticable that causes the issue. I'll report my findings once I have them.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:07*

**+Tony Sobczak** Found it straight away.



The issue with these files is the paths are not closed.



I will show you what I mean in some screenshots.


---
**Scott Marshall** *April 13, 2016 22:09*

Loved the K6-2, blew Intel out of the water for years. Still have one on my desk as a paper weight. Nicest looking chip ever made. 



Watching .....


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:13*

[https://drive.google.com/file/d/0Bzi2h1k_udXwWVJxeVl3TV9FeDQ/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVJxeVl3TV9FeDQ/view?usp=sharing)

[https://drive.google.com/file/d/0Bzi2h1k_udXwdzEzakcwNVhnRGc/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwdzEzakcwNVhnRGc/view?usp=sharing)



Here is what I am talking about.



See the 2nd picture, I've highlighted the individual lines in different colours. That shows that they are not connected together (or else they would all be the same colour).



**+Scott Marshall** Duron was one of my favourites back in the day. A 900 MHz Duron would easily overclock to 2.5 GHz, & I was living in a town with very hot & humid temperatures at the time. Albeit, I made a case out of 50 120mm (105cfm) case fans to keep it cool lol.


---
**Scott Marshall** *April 13, 2016 22:22*

It's the 1st picture. I can't zoom in as my Adobe trial expired, but I get the open polygon error. Autocad/Draftsight has the same issue. Where did the angled lines com in? are they an error warning by showing a closure?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 22:24*

**+Scott Marshall** Actually, what happens is that the paths are obviously joining for the curves when imported into Corel. As the curves are only 1/4 of a circle, the two nodes that are the end points for the path joining up would create a 45 degree angled line (if they were to be joined). So for some strange reason, when imported into CorelDraw from the dxf file, it must be joining those nodes & creating the diagonal line.



It won't do it when importing from AI.



edit: Oh yeah, just noticed that when I added the photo links, it must have messed up the order. So it is the 1st picture that shows the colour highlighted lines.


---
**Scott Marshall** *April 13, 2016 22:27*

I understand. 

Thanks Tony too. I jumped in here trying to understand this, I appreciate the tolerance. Hope it goes clean from here.

Scott


---
**Tony Sobczak** *April 14, 2016 01:53*

**+Yuusuf Sallahuddin**​ What can I do you fix this.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 02:21*

**+Tony Sobczak** I am not certain for DXF software, how to fix it. But for Adobe Illustrator, I would select the 2 nodes at each point where the lines join (e.g. in my image where the red joins the blue) & use the Join command. Continue doing that for all the joining positions on the shape & eventually you will have 1 solid closed path. Then no issues where it imports with those angular lines.


---
**Tony Sobczak** *April 14, 2016 03:53*

I have illustrator but not familiar with it but I will give out a shot. Thanks so much. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 04:06*

If you have any issues, let me know & I will give more detailed instructions on how to do it.


---
**Tony Sobczak** *April 14, 2016 21:00*

I looked up a couple ways in line (knife and paths close all) but neither worked very well. Close all paths added extra line. So info from you would be greatly appreciated.


---
**Tony Sobczak** *April 14, 2016 21:01*

Oops did not follow your instructions. Will try that now.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 21:35*

Ah, let me know if what I suggested works. If not, I'll take the file & see if I can create a video tutorial to show how to close the paths.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 22:06*

**+Tony Sobczak** Actually, I found a super easy way to do it. I've recorded my screen for a couple of minutes with that similar file you provided. I am uploading to youtube at the moment & will provide you the link when it completes.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 22:17*

**+Tony Sobczak** 
{% include youtubePlayer.html id="wJMwi_n1MNc" %}
[https://www.youtube.com/watch?v=wJMwi_n1MNc&feature=youtu.be&ab_channel=YuusufSallahuddin](https://www.youtube.com/watch?v=wJMwi_n1MNc&feature=youtu.be&ab_channel=YuusufSallahuddin)



That's the easiest way I know of to do it.


---
**Tony Sobczak** *April 15, 2016 17:23*

Oh man this worked great!   Thanks so much **+Yuusuf Sallahuddin** .   You been a great help.  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2016 17:30*

**+Tony Sobczak** You're welcome Tony. Glad it worked & helped you out. 


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/5A8o44xE5eQ) &mdash; content and formatting may not be reliable*
