---
layout: post
title: "Been to busy to post lately but this is one of my recent projects"
date: June 27, 2017 14:07
category: "Object produced with laser"
author: "Jeff Johnson"
---
Been to busy to post lately but this is one of my recent projects. The youth pastor at my church contacted me with a last minute request for some gifts for the graduating seniors. She didn't know what she wanted other than involving an anchor with the Bible verse and text. This is what I came up with . The anchor is in walnut and the base is hickory. 

![images/eca0e192a46c52a8ddb62db6ce3c4717.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eca0e192a46c52a8ddb62db6ce3c4717.jpeg)



**"Jeff Johnson"**

---
---
**Ned Hill** *June 27, 2017 14:21*

Very cool.


---
**Mark Brown** *June 28, 2017 00:16*

You beveled the edges a little?


---
**Ned Hill** *June 29, 2017 03:41*

**+Mark Brown** looks to me like he engraved the outline and then cut.




---
**Jeff Johnson** *June 30, 2017 14:20*

I engraved a border on the anchor and then cut it to give it a squared bevel. The base is beveled just a little by sanding.




---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/ax4vQRH1Qo1) &mdash; content and formatting may not be reliable*
