---
layout: post
title: "Going to have a go at this so expect lots of stupid questions from me in the future :)"
date: April 17, 2017 10:09
category: "Discussion"
author: "Tom Brennan"
---
Going to have a go at this so expect lots of stupid questions from me in the future :)

[https://store-kq4bjok.mybigcommerce.com](https://store-kq4bjok.mybigcommerce.com)





**"Tom Brennan"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 16:47*

I really do love that article **+Peter van der Walt** 

Unfortunately it is usually paired with my "bang head here" desk mat.


---
**Tom Brennan** *April 17, 2017 20:50*

Thanks for the comments folks i have read through that article with interest. I am the type of bloke that waves good by to the fire department as they leave And starts to think I will Need Help With This after all ! If at first you dont succeed try and try again.

Not afraid to ask when things go wrong But will never waste folks time on common sense questions.

Thanks again folks


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 20:52*

Again it's not a concern about "common sense questions" feel free to ask those.  I believe no question is stupid, we just prefer it is asked in a pleasant and informative way to make our jobs easier. 


---
**Darryl Kegg** *April 26, 2017 19:08*

Just ordered mine, counting down the minutes (seconds) until I get the shipment notification...


---
**Ray Kholodovsky (Cohesion3D)** *April 26, 2017 19:10*

I saw, and I'm counting the microseconds till the assembler sends over the final ship notice, then 1 week and we'll be shipping! 


---
*Imported from [Google+](https://plus.google.com/101321940155855591581/posts/4Ev6FjrD9TX) &mdash; content and formatting may not be reliable*
