---
layout: post
title: "Would you believe that plaster and alcohol will engrave metal?"
date: January 05, 2019 20:05
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Would you believe that plaster and alcohol will engrave metal?



[https://frankieflood.blogspot.com/2014/12/cermark-alternative.html](https://frankieflood.blogspot.com/2014/12/cermark-alternative.html) 





**"HalfNormal"**

---
---
**Ned Hill** *January 05, 2019 20:45*

Hmmm think I've heard this before but never looked into it.  The plaster is mostly Calcium sulfate so you are probably forming a "hot" black oxide (Fe3O4) on the stainless steel surface.  


---
**Lee Studley** *January 06, 2019 03:11*

Good to know!


---
**'Akai' Coit** *January 14, 2019 04:51*

This looks very interesting. I'll have to do more research on this!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/DRfjPexd7VL) &mdash; content and formatting may not be reliable*
