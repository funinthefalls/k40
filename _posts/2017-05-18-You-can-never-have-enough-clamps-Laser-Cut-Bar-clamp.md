---
layout: post
title: "You can never have enough clamps..... Laser Cut Bar clamp"
date: May 18, 2017 14:51
category: "Object produced with laser"
author: "Nigel Conroy"
---
You can never have enough clamps.....

Laser Cut Bar clamp.



Not my design. Created by 150watts



Files are on thingiverse

There's a YouTube channel and instructable.



Will test clamp force later as glue was setting up last night.







![images/91a91caed7e4111a8e53667f905472c7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/91a91caed7e4111a8e53667f905472c7.jpeg)
![images/42eb218f96d12fccf5c7a0e0d55ba39e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42eb218f96d12fccf5c7a0e0d55ba39e.jpeg)

**"Nigel Conroy"**

---


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/Eht7DAg5211) &mdash; content and formatting may not be reliable*
