---
layout: post
title: "Help... I've been using K40 for more than 2 years and few months ago i replaced the tube with the new one which i bought around a year ago"
date: December 24, 2017 16:57
category: "Discussion"
author: "Alisha kansha"
---
Help...

I've been using K40 for more than 2 years and few months ago i replaced the tube with the new one which i bought around a year ago. Since 2 weeks ago my laser power drop drastically more than a half i think. To cut a 3 mm plywood i need 3 pass with 6 mm/s speed and 50% power.

First i notice the laser beam i arcing around the plate of anode side, tried to fixed the connection and replacing the optical parts as suggested by some peoples on the net but still same. My machine is the one with digital panel. Next i installed mA meter to check the current flowing to the tube and at 100% percent power the mA reading is around 22 mA, i'm not sure is it correct or not. 

Another thing is when i check the tube output directly before the first mirror, i notice the beam looks very low in power.

How can i diagnose the problem? is it the tube which is spoiled or the power supply. I don't want to waste money buying wrong spare part.





**"Alisha kansha"**

---
---
**Don Kleinschnitz Jr.** *December 24, 2017 17:12*

22 ma of current suggests to me that the LPS is outputting enough current for good performance.



How do you know the power is low: <i>Another thing is when i check the tube output directly before the first mirror, i notice the beam looks very low in power.</i>



My initial guess is that you have and alignment problem. Realign the optical path first.


---
**Alisha kansha** *December 24, 2017 17:26*

**+Don Kleinschnitz** 

I place a clear plexiglass in front of the tube before the first mirror and press the test button. Even with 50% power, i can see there's only small mark on the tube which is totally different with normal condition. Previously in normal condition i can see a full circle mark on the plexiglass, but now the beam mark on the plexiglass like only "O" shape letter. What i also notice is increasing power from 40% to 50% or even 80% looks like nothing different.

![images/84c1431f96f665020b0ced376ecc1c56.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/84c1431f96f665020b0ced376ecc1c56.jpeg)


---
**Chris Hurley** *December 24, 2017 17:47*

I had a similar situation but it was right from the get go. The first lens in the tub was bad. 


---
**Don Kleinschnitz Jr.** *December 24, 2017 18:19*

It looks like your laser is acting up and running in the TEM01 mode vs the normal which is TEM00.



There are many reasons why a laser "mode hops" such as changes in the resonant cavity, temp, including the supply. 



Though from **+Chris Hurley** experience, it would seem most likely some thing changed inside your tube, don't know off the top of my head what else to suggest.



Is the tube still arching or did you fix that?





[photonics.com](https://www.photonics.com/images/Web/Articles/2009/3/8/UnderstandingTheBasics_Coherent_Figure5.jpg)


---
**Chris Hurley** *December 24, 2017 18:55*

**+Don Kleinschnitz**  any idea how to change a tube from TEM 01 to TEM 00? 


---
**Don Kleinschnitz Jr.** *December 24, 2017 19:50*

**+Chris Hurley** Don't t think that is possible it it internal to the tube. But doing so is over my head anyway :)!

How did you determine that your internal mirror was bad?




---
**Chris Hurley** *December 24, 2017 21:52*

The cooling ring actually fell off after a little while because too much of the heat was being transferred into the glue which is used to affect the cooling ring to the lens.


---
**Paul Mott** *December 25, 2017 08:24*

**+Chris Hurley** Sounds like you did not have sufficient coolant flowing through the system. This alone can damage a tube beyond recovery. Sorry but it is not good news.


---
**Alisha kansha** *December 25, 2017 12:39*

**+Don Kleinschnitz** 

The tube is still arcing. I plan to replace with 60 watt tube. How significant is the power between 40 and 60 watt? what speed i can achieve to cut 3 mm plexiglass with 60watt tube assuming the optical is in a good condition?


---
**Don Kleinschnitz Jr.** *December 25, 2017 12:47*

**+Alisha kansha** I would suggest the power between 40-60 to be significant assuming you get a good tube and LPS to match. I plan to do it some day.



Search this forum for others that have done it for advice. As a minimum you will have to extend your box and get a new LPS. 



I don't know a #, of the top of my head, how much faster you will be able to go as there are lots of factors relating to your actual machine. "Lots faster" I would guess :).


---
**Alisha kansha** *December 25, 2017 12:55*

**+Don Kleinschnitz** 

I plan to buy this one

[https://www.aliexpress.com/store/product/Co2-Laser-Power-Supply-60W-HY-T60/1513187_32583563984.html?spm=2114.12010612.0.0.3388cd8h9Hjs1](https://www.aliexpress.com/store/product/Co2-Laser-Power-Supply-60W-HY-T60/1513187_32583563984.html?spm=2114.12010612.0.0.3388cd8h9Hjs1)



and tube from local company which i believe it is a chinese tube also.



![images/dbf57ef0bc92403a7dcf50301653ac6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dbf57ef0bc92403a7dcf50301653ac6a.jpeg)


---
**Don Kleinschnitz Jr.** *December 25, 2017 15:29*

**+Alisha kansha** if you have a stock controller, the LPS that you picked may not have a 24V supply output so you would need a separate supply. Thus your wiring will not be plug n play.


---
**Alisha kansha** *December 25, 2017 23:57*

I understand that the stock controller need 24v supply. Is there will be any problem by using 24v from separate lps? Do i just have to wire the vcc and gnd or any specific wiring if using 2 power supply?


---
**Don Kleinschnitz Jr.** *December 26, 2017 17:16*

**+Alisha kansha** You can run 2 external supplies: 24V  in addition to your  new LPS. I do not know how safe it is to run the 5v from your new supply to the stock controller as I do not know the capacity of the new supplies 5V nor the load of the stock controller. I did however help someone else and I believe the new LPS handled the 5V ok.



You will essentially have to wire:



-the new LPS AC input and safety ground are wired correctly.



-the new 24V and 5V supplies to the stock controller card and insure the controller ground is tied to the the LPS ground



-the new LPS into the chassis cable harness connected to the old LPS insuring that the coolant, laser switch and fire are all reconnected properly. [properly depends on the specifics of the new LPS]



-the "L" signal from the stock controller to the proper signal on the new LPS. [properly depends on the specifics of the new LPS]



Are you ok reading schematics?

When you decide what parts you want I(we) can help you create a wiring diagram so that you get the correct parts and wire them correctly.



Attached is a diagram for a similar supply conversion I helped someone else with. Caution: It <b>may or may not</b> apply depending on the parts you intend to buy and the model of K40 you are running.



Post a picture of you control panel pls.



 [photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/Sdqs13A1DHMtM2Ry2)




---
**Alisha kansha** *December 27, 2017 02:22*

**+Don Kleinschnitz** 

Thanks Don for the schematic, currently i'm thinking to replace the stock controller too with RDC6442G.


---
**Don Kleinschnitz Jr.** *December 28, 2017 01:35*

**+Alisha kansha** I would recommend a C3D controller, plug an play!

**+Ray Kholodovsky**


---
**Ray Kholodovsky (Cohesion3D)** *December 28, 2017 02:01*

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



Cheaper than the Ruida and works with the awesome **+LightBurn Software** which will be out in a few days on Jan 1.  


---
**Alisha kansha** *December 28, 2017 04:05*

What is make C3D better than ruida or similiar controller. The good news at least it cheaper...


---
**Ray Kholodovsky (Cohesion3D)** *December 28, 2017 04:39*

I think some people like the functionality, it gives you full power control of the laser for real grayscale engraving, lets you expand the machine to have a Z axis table and a rotary, and  lets you use powerful software like this: 


{% include youtubePlayer.html id="iW0C8wPln8o" %}
[youtube.com - LightBurn demo - A project from start to finish](https://www.youtube.com/watch?v=iW0C8wPln8o)



Ultimately you can get either the Ruida or the C3D... and you will use the same software to do the same things... C3D is a direct drop in for the K40 - should be just a few screws and cables to get switched over. 


---
**Alisha kansha** *December 28, 2017 05:03*

**+Ray Kholodovsky** 

Hi Ray, are the owner of cohesion3d? Just curious, how many days will the shipping by usps to Indonesia?

Actually my current plan is to upgrade from 40 to 60 watt, that mean i need to replace the lps too. Concerning laser controller, i don't have any preference brand or model, but of course i'm looking for a better one than the stock controller. By using c3d, can i auto calibrate the motor to get correct measuring distance? Previoisly i have changed the gear ratio of the standard k40.


---
**Ray Kholodovsky (Cohesion3D)** *December 28, 2017 05:06*

Yes, you can configure steps per mm and other stuff on the board. 

International USPS first class can be 2-3 weeks or longer, Priority says 6-10 business days but it is not guaranteed. 

Yes, I am the owner.  


---
*Imported from [Google+](https://plus.google.com/112428355097604312279/posts/D1kLCrKwPmb) &mdash; content and formatting may not be reliable*
