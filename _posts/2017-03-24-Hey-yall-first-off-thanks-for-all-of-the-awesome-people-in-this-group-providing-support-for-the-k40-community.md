---
layout: post
title: "Hey ya'll, first off thanks for all of the awesome people in this group providing support for the k40 community!"
date: March 24, 2017 04:18
category: "Discussion"
author: "John"
---
Hey ya'll, first off thanks for all of the awesome people in this group providing support for the k40 community! I've been lurking for a while, I run Epilogs for a day job and took the plunge into the k40 a few months ago. 



My machine arrived in good condition, I made a DIY Z- table etc. and I can cut and engrave just fine. I do however, have a question about the k40 that I haven't seen asked yet. The right side of my X-axis arm is attached to the belt of my Y-axis via a screw, this screw also holds the Y- axis belt together... Is this normal  for a k40? I ask because my X-axis arm sags on the right side and I fear if I tension the Y belt enough to level it, the belt will break and if I don't level it, alignment will be damn near impossible. 



Again, thank you all for the shared knowledge and for your input.





**"John"**

---
---
**Ned Hill** *March 24, 2017 04:41*

Well on the right of the x-axis arm the bracket does attach to the belt with a screw.  There should also be a roller attached to the middle of the bracket that runs along a channel in the side of the bed frame

![images/2ea24e9461db45874dc6f55eb03c74a3.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2ea24e9461db45874dc6f55eb03c74a3.png)


---
**Alex Krause** *March 24, 2017 04:44*


{% include youtubePlayer.html id="_NRnRwHtjrc" %}
[https://youtu.be/_NRnRwHtjrc](https://youtu.be/_NRnRwHtjrc)


---
**Alex Krause** *March 24, 2017 04:45*

This show how to remove it but you can also tension the x axis belt with these instructions


---
**Alex Krause** *March 24, 2017 04:48*

You say the belt sags on the right side? Are you sure you aren't actually referring to the y the foward and back motion is the y the left and right motion is the X.... The y axis belt tensioners are the two holes in the rear of the machine a few inches below the tube cover they are around a 3/8-1/2 inch size hole 


---
**John** *March 24, 2017 23:25*

I've tensioned the Y-belts, the problem I'm having is that the entire X-axis arm drops on is off level because its weight is only supported by the Y-belt on the right hand side. **+Ned Hill** my bracket does have the roller attached but there is no channel on the gantry for it. I think this is why my X-axis arm is slanting on the right, the weight of the arm is only supported by the Y-belt that it is screwed to. 




---
**Ned Hill** *March 24, 2017 23:29*

Yeah that's definitely not normal.  Don't think I've heard of a machine coming like that.  Contact the seller.


---
**Ned Hill** *March 24, 2017 23:30*

Can you provide a pic of that side of the gantry?


---
**John** *March 24, 2017 23:49*

![images/ea2da53f494f77df5d9935e8f68514de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ea2da53f494f77df5d9935e8f68514de.jpeg)


---
**John** *March 24, 2017 23:49*

![images/b78bde9ab44aa9d6f895926050ba8078.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b78bde9ab44aa9d6f895926050ba8078.jpeg)


---
**John** *March 24, 2017 23:50*

**+Ned Hill** The first one is similar to the photo you posted. The second is looking through the access hole towards the gantry where I'm assuming this little channel for the roller should exist..




---
**Ned Hill** *March 25, 2017 01:03*

**+John** Here is a pic of mine through the access hole. It appears to me that  you got a flawed gantry.

![images/443c301595594783c3400563a4d9591a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/443c301595594783c3400563a4d9591a.jpeg)


---
**Mark Brown** *March 25, 2017 02:09*

Now I'm picturing some Chinese person finishing assembling this, then noticing a leftover piece.  "What the herra this?  Ah, probry not important."


---
*Imported from [Google+](https://plus.google.com/102491447170683453911/posts/CMZqggBuqDu) &mdash; content and formatting may not be reliable*
