---
layout: post
title: "Hi all. Total newbie here. Just ordered a K40 yesterday - xmas comes early!"
date: November 23, 2016 14:53
category: "Modification"
author: "Dave Durant"
---
Hi all. Total newbie here. Just ordered a K40 yesterday - xmas comes early!



I saw this over at Kickstarter but don't see much more info anywhere. 



"Since most of the usable pins of the LPC chip has been broken out on the Re-ARM, you can use it for other purposes. Like a custom shield to upgrade the K40 Laser to smoothie and control it using LaserWeb by Peter van der Walt. How about a CNC shield that uses external drivers? You can create your own shields to fit your motion control needs.



K40 is a low cost(< $400) Laser engraver that can be modified to perform like more expensive machines by using Smoothieware and Laserweb. The Re-ARM plus the K40 shield allows an easy upgrade. There is an active G+ community dedicated to K40 Laser modifications that can be found (link). "



Anybody have thoughts on this new controller from kickstarter or info on where I can find a "K40 shield"? 



Thanks!





**"Dave Durant"**

---
---
**Ariel Yahni (UniKpty)** *November 23, 2016 15:06*

It's an awesome new solution and you can get a shield specially designed for the K40 . it's made by the awesome **+Roy Cortes**​


---
**greg greene** *November 23, 2016 15:39*

Man the support from the community just grows and grows doesn't it.


---
**Dave Durant** *November 23, 2016 17:36*

Great - they just got another backer! Replacing the controller was near the top of my list and Re-ARM sure looks nice.


---
**Justin Nesselrotte** *November 24, 2016 03:48*

Yeah, I've seen his progress in getting this to kickstarter, and I've been very impressed so far.


---
**Benjamin Mitchell** *January 07, 2017 23:40*

Are the K40 shields available yet?  I dont seem to find any information on them. 


---
**Dave Durant** *January 08, 2017 00:28*

**+Benjamin Mitchell**, I asked about that in the KS comments and he said it was still in testing and should be reading in a few weeks. We're just past that few weeks so hopefully there will be more news coming soon.. 


---
*Imported from [Google+](https://plus.google.com/+DaveDurant/posts/Y95AyuSQxur) &mdash; content and formatting may not be reliable*
