---
layout: post
title: "Hi all, I want to change the table"
date: March 23, 2017 20:13
category: "Hardware and Laser settings"
author: "MA Lopez"
---
Hi all,

I want to change the table. I have bought a steel mesh like this -> [https://thumbs.dreamstime.com/z/stainless-steel-mesh-background-25227002.jpg](https://thumbs.dreamstime.com/z/stainless-steel-mesh-background-25227002.jpg)



IS there any problem about using steel? (I ask because the original is alu)

Thanks and bect regards, MA









**"MA Lopez"**

---
---
**Ned Hill** *March 23, 2017 20:21*

That's fine.  Other people use that steel mesh as well.


---
**Jim Hatch** *March 23, 2017 20:24*

And it allows you to use small magnets to hold things down like warped wood or paper, cardboard, etc.


---
**Phillip Conroy** *March 23, 2017 20:41*

Depending on what you are doing with laser machine  ie cutting mdf you will get a lot of marks on bottom of cut items if it's is ok than use it.I Mae items to sell so must  be as clean as possible. I use 2  different  beds 1 pin bed - screws with magnets made up so that the center of 3mm mdf is 50 mm from bottom of focal lens,this is great for items that are large and do not have many inside cuts.on some itemps i only uSe 4 magents screw combos . The othe bed i uSe is a knife bed mAde up of hacksaw blades ,great for jods that hAve inside cuts or lots of small items.

![images/0cef5cbb38fb0a1926b6e2ce59201349.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0cef5cbb38fb0a1926b6e2ce59201349.jpeg)


---
**Phillip Conroy** *March 23, 2017 20:41*

![images/3a27e22cca38d23f210d9fbd1f4191e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3a27e22cca38d23f210d9fbd1f4191e0.jpeg)


---
**Phillip Conroy** *March 23, 2017 20:42*

![images/0f601d9fedee7d52c0802a11c1801515.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f601d9fedee7d52c0802a11c1801515.jpeg)


---
**MA Lopez** *March 23, 2017 20:46*

Thanks mates!




---
**Andy Shilling** *March 23, 2017 23:15*

I use this type of bed and with the spikes linked below it's easy to raise things up when you want to cut them. I've fitted a 3mm thread in each stud so it does into the holes in the bed and holds the stud in place.



Look at this on eBay  [ebay.co.uk - Details about  SOLID METAL SCREW FIX SPIKE STUDS Silver Gold Black Punk Leather Craft Rivets](http://www.ebay.co.uk/itm/291548117870)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 24, 2017 02:32*

**+Andy Shilling** Wow, that's a cool idea to use those studs. I have heaps of them that I bought ages ago just sitting there doing nothing. Might see about attempting to use them with my cutting bed too.


---
**MA Lopez** *March 24, 2017 07:06*

Nice idea! thanks!


---
**Andy Shilling** *March 24, 2017 07:10*

**+Yuusuf Sallahuddin**​ the question is why do you have them sitting around? Did you have them for when you wanted to be a rock star lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 24, 2017 12:32*

**+Andy Shilling** Hahaha, I actually bought them to make a dog collar for my brother's dog years back & never ended up doing it because he never ended up giving me his dog's measurements.


---
**Vince Lee** *March 24, 2017 18:02*

I used a similar mesh (but in stainless steel) until recently.  Based on my experience, if you go this way, I suggest not going too thin, as my 1/16" plate never stayed perfectly flat.  I switched to 1" thick aluminum honeycomb recently and am much happier both in its flatness and how clean the cuts are.


---
**Phillip Conroy** *March 24, 2017 22:09*

How do you clean the honey comb bed? I use's oven cleaner on my knife bed ,spray on wait 10 minutes hose off ,works great........


---
*Imported from [Google+](https://plus.google.com/109180025053447503546/posts/NCTXEx8zWYf) &mdash; content and formatting may not be reliable*
