---
layout: post
title: "I have a K40D and apart from having to replace a Power Supply, its been great"
date: July 25, 2018 22:00
category: "Discussion"
author: "Frank Farrant"
---
I have a K40D and apart from having to replace a Power Supply, its been great.  



I make and sell items that I cut with the K40 and need to consider a bigger cutting area.  



I don't have the skills or time to even begin to try to make the cutting area bigger on the K40, so I wondered what laser people would recommend ?



I asked the company where I bought the K40 and they mentioned the: FDC-350 or FDC-460 ?  Anyone seen either of these or have any recommendations ?



Thanks

Frank







**"Frank Farrant"**

---
---
**HalfNormal** *July 26, 2018 00:00*

This what I purchased when I out grew the K40 [https://m.ebay.com/itm/New-60W-110V-CO2-Laser-Engraving-Machine-Engraver-Cutter-w-USB-Interface/252730728666?_mwBanner=1&_rdt=1](https://m.ebay.com/itm/New-60W-110V-CO2-Laser-Engraving-Machine-Engraver-Cutter-w-USB-Interface/252730728666?_mwBanner=1&_rdt=1)

[http://ebay.com/itm/New-60W-110V-CO2-Laser-Engraving-Machine-Engraver-Cutter-w-USB-Interface/252730728666?_mwBanner=1&_rdt=1](http://ebay.com/itm/New-60W-110V-CO2-Laser-Engraving-Machine-Engraver-Cutter-w-USB-Interface/252730728666?_mwBanner=1&_rdt=1)


---
**HalfNormal** *July 26, 2018 00:02*

I have been very happy with it.


---
*Imported from [Google+](https://plus.google.com/110419041310362555971/posts/1HuVc1i1PVn) &mdash; content and formatting may not be reliable*
