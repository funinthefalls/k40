---
layout: post
title: "Ultifaker GO laser cut 3D printer This project was inspired by by an instructable called Ultifaker which was a CNC version of the same I added a heated bed and and modified the files to be laser friendly"
date: October 21, 2016 20:13
category: "Object produced with laser"
author: "Nick Williams"
---
Ultifaker GO laser cut 3D printer 



This project was inspired by

by an instructable called Ultifaker which was a CNC version of the same I added a heated bed and and modified the files to be laser friendly.





![images/4982b3d02cb00d13e6a7d407d9611f17.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4982b3d02cb00d13e6a7d407d9611f17.jpeg)
![images/8f409b82e3e25528ed329fef526ebe5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f409b82e3e25528ed329fef526ebe5b.jpeg)
![images/163a49d3e6291a92049279a690b0137b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/163a49d3e6291a92049279a690b0137b.jpeg)

**"Nick Williams"**

---


---
*Imported from [Google+](https://plus.google.com/107520953345127905250/posts/GFo5xvoHY3m) &mdash; content and formatting may not be reliable*
