---
layout: post
title: "I just picked up one of the 10W LED laser cutters to join my K40"
date: August 01, 2016 19:26
category: "Software"
author: "Randy Randleson"
---
I just picked up one of the 10W LED laser cutters to join my K40. I look forward to trying LaswerWeb3 with it!!





**"Randy Randleson"**

---
---
**greg greene** *August 01, 2016 19:41*

Which one did you get?


---
**Randy Randleson** *August 01, 2016 19:43*

This is the one [http://www.aliexpress.com/item/10W-laser-AS-5-10000MW-diy-laser-engraving-machine-engrave-metal-diy-marking-machine-metal-laser/32652153790.html?spm=2114.13010608.0.54.LpazGh](http://www.aliexpress.com/item/10W-laser-AS-5-10000MW-diy-laser-engraving-machine-engrave-metal-diy-marking-machine-metal-laser/32652153790.html?spm=2114.13010608.0.54.LpazGh)


---
**Randy Randleson** *August 01, 2016 19:45*

**+Peter van der Walt** Thank you Peter! I read the comments and they said it puts out 10w, I guess we will see


---
**Sunny Koh** *August 02, 2016 06:05*

**+Wayne Moore** Let me know if it can mark Acrylic as I am looking for a portable solution to do pen shows. I think it can mark coated metals but not so sure for Acrylic.


---
**Randy Randleson** *August 02, 2016 10:22*

I got the "software" installed last night. The USB driver is PL2303 which was nice (and I was surprised. I suspected it would be some bizarre driver). I'll get it cutting today to see how it fares just for fun. Then on to getting it to work on laserweb3!


---
*Imported from [Google+](https://plus.google.com/102868778145826564225/posts/JKG4mXJZHRA) &mdash; content and formatting may not be reliable*
