---
layout: post
title: "Had another idea for you Ashley M"
date: December 17, 2015 23:17
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Had another idea for you **+Ashley M. Kirchner**. This might be less prone to breaking. The kid could slide a bar through the groove to various notches to hold up the bucket in various positions. Depends how old the kid is though.

![images/f3843696bdb0f6739021b023010a0627.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f3843696bdb0f6739021b023010a0627.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ashley M. Kirchner [Norym]** *December 18, 2015 01:25*

Yeah, that's a possibility. I just have to play with it and see what I come up with. One possibility of friction is to put something in between the wood plates that will add some resistance, like nylon washers, then make it so the pieces are held together tightly and letting the washers do the movements. I've done that before with something else, so I just have to try it out and see what works. Hmm, I wonder is 1/16th or 1/32nd thin pieces of wood would work ... uh, now my idea train is moving!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2015 02:32*

**+Ashley M. Kirchner** What about washers that have little bumps on them at specific intervals? I can't remember what they're called off the top of my head, but I remember using some with some nuts that had bumps on them, so they kind of grip into each other.



Or, you could laser cut those end circles for the axle to have little bumps on the interior that match up to little bumps on the exterior of the panel that the axle slides through, then jam together tight like you said so the bumps will act as position locks.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/NwR2ZYZAQTG) &mdash; content and formatting may not be reliable*
