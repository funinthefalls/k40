---
layout: post
title: "Hey everyone, I'm finally back in the mindset to attempt the smoothie conversion again"
date: December 10, 2016 21:30
category: "Smoothieboard Modification"
author: "Chris Sader"
---
Hey everyone, I'm finally back in the mindset to attempt the smoothie conversion again. Like **+Eric Rihm** and **+Ariel Yahni** (I assume this is still true?), I'm using an AZSMZ + LCD combo, and a Weistek middle man board.



I've seen rumblings that things might have changed since I last attempted this in terms of the optimal way to swap the Smoothie in. Is that true? Anyone have a good wiring diagram to go off of for the AZSMZ setup?





**"Chris Sader"**

---
---
**Ariel Yahni (UniKpty)** *December 10, 2016 21:37*

**+Chris Sader**​ connection of smoothie has moved as per our connection. 1 cable only.  Mind that I don't have an LCD


---
**Chris Sader** *December 10, 2016 21:41*

Sorry, **+Ariel Yahni** not sure what you mean. 1 cable from where to where? What about the rest of the middleman and power connections? Keep in mind, back when I tried and gave up the first time, I reverted back to stock, so I'm starting from scratch here.


---
**Ariel Yahni (UniKpty)** *December 10, 2016 23:04*

**+Chris Sader**​ my bad. Start here [https://plus.google.com/107471533530265402525/posts/YsNCc5Bnrzq](https://plus.google.com/107471533530265402525/posts/YsNCc5Bnrzq)


---
**Chris Sader** *December 11, 2016 03:57*

**+Ariel Yahni** I'm sorry, I'm trying here, I really am.



I don't have the same middleman board as he does, I have the Weistek, so there are some things that aren't translating.



On the Weistek, I have a bunch of pins with nothing connected yet:



1. To PSU - one pin labeled IN, one labeled L/H

2. 5v Out and its associated GND

3. The GND pin next to 24v in.

4. LaserFire 

5. PWM



Also, I don't see a place on the middleman to send the 24v from the middle man to the azsmz like he has it with his middleman.



I obviously don't have the laser hooked up yet either as I wasn't sure whether to use LaserFire or PWM or something else...I also don't see how to connect that to the azsmz from the middleman.



I swear I'm not this stupid. I've build 3d printers from scratch without this much of a headache.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 03:59*

No worries **+Chris Sader** ill try better, its my fault


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:01*

Do you have your middleman populated? Meaning it has the connectors? Can i geta picture of your PSU and Stock board?


---
**Chris Sader** *December 11, 2016 04:09*

![images/97ad1e44d5430a34ec0f97ece97e8afb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97ad1e44d5430a34ec0f97ece97e8afb.jpeg)


---
**Chris Sader** *December 11, 2016 04:09*

![images/3b9ccff780cc84ddd3808a89bea29701.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b9ccff780cc84ddd3808a89bea29701.jpeg)


---
**Chris Sader** *December 11, 2016 04:10*

**+Ariel Yahni** yes, middleman has the connectors soldered in.


---
**Chris Sader** *December 11, 2016 04:12*

I've got the Y motor plugged directly in to the azsmz, the ribbon cable plugged into the middleman board, and the 5v, 24v and GND from the power supply plugged into the middleman (I haven't done anything with LO from the power supply plug yet).




---
**Chris Sader** *December 11, 2016 04:12*

BTW, not your fault. Didn't mean to come off that way if I did.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:14*

Ok so here we go. The middleman will only be used as a convertion for the cables


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:14*

[https://www.google.com/search?q=K40+middleman&client=ms-android-samsung&prmd=sivn&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiNkOSAo-vQAhWDXiYKHUaEAH0Q_AUIBygB&biw=412&bih=652&dpr=3.5#imgrc=1leLhBUGyHiBrM%3A](https://www.google.com/search?q=K40+middleman&client=ms-android-samsung&prmd=sivn&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiNkOSAo-vQAhWDXiYKHUaEAH0Q_AUIBygB&biw=412&bih=652&dpr=3.5#imgrc=1leLhBUGyHiBrM%3A)


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:19*

So if you connect the ribbon to the middleman you then have to go from there to the X axis driver on the azsmz


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:19*

Thus just remaining connecting the Y axis that you should be able to connect directly to the azsmz


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:22*

I just see you update above, so from wich connector are you getting all those? Is it the one from the right? 


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:26*

If it's that one then you are good. From there you. Need to come out to the board. 5V are not needed so you. Only. Need 24v and gnd


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:28*

From there just remaining is getting g that L cable from the same PSU connector and putting it into the negative screw on the D8 ( left next to the voltage in of the board)  only one cable


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:29*

Next get my config file from my K40 updated collection and place it on the SD


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:29*

Make sure you smoothie firmware is the latest cnc version on the smoothie site


---
**Chris Sader** *December 11, 2016 04:32*

**+Ariel Yahni** yes, from the connector on the right. That connector has 5v, 24v, GND, and L. You're saying I don't need to connect the 5v from there at all? and the 24v and GND from that connector go into the middleman, or directly to the azsmz? If they go to the middleman, how do I get them to the azsmz?


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:39*

24v and. Gnd directly. Into azsmz. No need for. Middleman


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:39*

5v not needed. Azsmz has a 5v regulator 


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:40*

Once you have that focus on getting the config and machine moving, forget about the laser fire. That's easy


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:41*

Once the machine moves correctly then connect the L as stated above


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:41*

Here is a fresh shot of my board. Yellow is the L

![images/2dcef7d46f816b48727f429ecaed7b1c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2dcef7d46f816b48727f429ecaed7b1c.jpeg)


---
**Chris Sader** *December 11, 2016 04:42*

Nice. Very helpful **+Ariel Yahni**, I'm going to give it a shot now. I'll update soon.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:50*

**+Chris Sader**​ do you know [how.to](http://how.to) adjust the current on the drivers?  It's. Very important to not over current them or they will burn. Please replay back asap


---
**Chris Sader** *December 11, 2016 04:53*

I know how to adjust them, **+Ariel Yahni**, but I don't know what the current should be set to.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 04:55*

Yeah that's a mystery since no specs on the motors. Start at the center of the pot, run a job for a couple of minute and feel the temperature, adjust accordingly 


---
**Don Kleinschnitz Jr.** *December 11, 2016 04:56*

This may help: [donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



Although this is a Smoothie there are details of the endstops, middleman etc. in the sections and/or the schematics.


---
**Chris Sader** *December 11, 2016 05:07*

**+Ariel Yahni** we have movement! I had the X-motor backwards, but that's fixed now. Hitting "Home All Axes" on the LCD moves both axes about a mm, then doesn't do anything. 



Where are your 3 endstop wires coming from? Mine are the optical ones.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 05:07*

**+Chris Sader**​ going to sleep now. Good luck and hope to get good news from you in the morning :) 


---
**Ariel Yahni (UniKpty)** *December 11, 2016 05:10*

Optical you migth want to talk to **+Don Kleinschnitz**​ regarding those


---
**Chris Sader** *December 11, 2016 05:15*

Hi **+Don Kleinschnitz**, I don't have the same smoothie board as you, but I've been trying to follow a combination of your posts, this guy's [https://plus.google.com/107471533530265402525/posts/YsNCc5Bnrzq](https://plus.google.com/107471533530265402525/posts/YsNCc5Bnrzq), and Ariel's advice.



I noticed the guy in the link above (though he uses a different middleman board) connects one wire from his middleman to the left-most pin on the AZSMZ (my smoothie clone) endstop connectors. I assumed that would be where I would connect from the EstY and EstX pins on the middleman board you and I use. Is that right? See my question above to Ariel about the Home All command on the Smoothie LCD




---
**Don Kleinschnitz Jr.** *December 11, 2016 06:15*

Did you see this:

[donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)

It shows how endstops are connected to the Middleman including power?



The optical endstops are open collector and the transmitter requires 5vdc. 



The details of the endstops when connected are here: [http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)



If these don't help ping me back.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 14:57*

**+Chris Sader**​ how are we doing? 


---
**Don Kleinschnitz Jr.** *December 11, 2016 16:49*

**+Chris Sader** here is my best guess at your wiring. No warranties expressed or implied :). I did this from the 2.0.1 schematics online.



I was not sure about the exact order of the board pin-outs on the end-stop connectors vs the schematics. I suggest checking them with a meter to verify gnd and 5v are where I put them.



I also was not sure where to pick up a ground for the "L" signal but if you are wiring to the LPS 24vdc and using its grnd you may not need it.

 

![images/d19936821f557af0db8cedf73e443616.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d19936821f557af0db8cedf73e443616.jpeg)


---
**Chris Sader** *December 11, 2016 18:39*

**+Ariel Yahni** I plan on tinkering some more this afternoon. When I left it last night, I was able to move the axes, but the Home All Axes function on the LCD didn't do anything.



**+Don Kleinschnitz** Thanks for this! I'll print it out and use as a reference while I work on it in a while. One question: The 5v and Gnd goes to both endstop connections on the AZSMZ? Or does it only go to the X as in your drawing?


---
**Don Kleinschnitz Jr.** *December 11, 2016 19:00*

You need a source of 5v for the middleman which connects to both endstops. You can pick it up on the endstops connectors on the board and route it to the middleman, as long as you have 5v to the middleman you are good. One connection is fine it connects to both endstops on the middleman.




---
**Chris Sader** *December 12, 2016 05:10*

**+Ariel Yahni** quick update. I setup Laserweb3 on my mac so I could get started testing movement by running a job, but couldn't get LW to see the AZSMZ usb port. All it was showing me was a Bluetooth port option, which obviously didn't work.


---
**Ariel Yahni (UniKpty)** *December 12, 2016 10:52*

**+Chris Sader** No drivers are need on OSX so something is wrong. Try the following. Connect the azsmz to you mac but do not turn on the K40, the board should come On via the usb. From there wait and look under " about this mac " at the top left under the apple menu and click system report. From there look under USB and it should show the USB device. Of course im going to asume that you did click refresh on the LW interface so it will show the connected serials ports available


---
**Chris Sader** *December 12, 2016 20:37*

**+Ariel Yahni** no luck. only shows built in apple keyboard devices and such.


---
**Ariel Yahni (UniKpty)** *December 12, 2016 20:45*

**+Chris Sader**​ wow. It's like if someone doesn't want you to use it


---
**Ariel Yahni (UniKpty)** *December 12, 2016 20:46*

Any chance you have a windows to try?  Maybe get the windows drivers? 


---
**Ariel Yahni (UniKpty)** *December 12, 2016 20:47*

[http://smoothieware.org/windows-drivers](http://smoothieware.org/windows-drivers)


---
**Chris Sader** *December 14, 2016 01:04*

**+Ariel Yahni** nothing on windows either. crap. 


---
**Chris Sader** *December 14, 2016 01:05*

Sigh... **+Ray Kholodovsky** wanna give me a pity discount on one of your boards? :'(


---
**Ariel Yahni (UniKpty)** *December 14, 2016 01:12*

But I remember you where able to jog the machine correct? 


---
**Chris Sader** *December 14, 2016 04:44*

**+Ariel Yahni**​ I am able to jog it using the LCD control.


---
**Ariel Yahni (UniKpty)** *December 14, 2016 04:48*

The only thing that occurs to me is that the USB is broken. I had that with another board. Contact the seller for a refund or exchange


---
**Chris Sader** *January 05, 2017 22:10*

**+Don Kleinschnitz** thanks again for the wiring diagram above. Very helpful! I've got everything hooked up and moving, so I'm on to hooking up the laser. I took the LO wire from the PSU and put it into D8 (-) like you have it pictured, but I'm not getting any laser fire when I run GCODE through laserweb. Any ideas Don or **+Ariel Yahni**?


---
**Ariel Yahni (UniKpty)** *January 05, 2017 22:22*

Is the laser section in the config enabled? Is every line without the point key? Is the laser fire pin the correct one for DO?


---
**Chris Sader** *January 05, 2017 22:36*

That seems to have been the problem **+Ariel Yahni**. I changed the pin to 2.5 and it's working now. You guys are awesome, thanks for bearing with me!



Now off to realign everything again!


---
**Ariel Yahni (UniKpty)** *January 05, 2017 22:38*

**+Chris Sader**​ it has been almost a pregnancy for you. Congrats if it's working. Would love to see what you do specially as your photography is awesome


---
**Don Kleinschnitz Jr.** *January 05, 2017 22:44*

Are we all talking about the same output pin. My recommendation was P2.8. Given **+Ariel Yahni** has the same board as you check his physical connection and config settings. 

Try a Laser/Laser fire from the GLCD assuming you have the same firmware, that will show you if the board is connected correctly.


---
**Ariel Yahni (UniKpty)** *January 05, 2017 22:57*

**+Don Kleinschnitz**​ [we.dont](http://we.dont) use the same pin azsmz


---
**Don Kleinschnitz Jr.** *January 05, 2017 23:33*

**+Ariel Yahni** yes I looked up the AZSMZ and provided this quess? 



[https://goo.gl/photos/FA2pXP6iJLGSpRyR8](https://goo.gl/photos/FA2pXP6iJLGSpRyR8)



Same as your setup?


---
**Chris Sader** *January 05, 2017 23:35*

2.5 seems to be working for me...should I look into it?




---
**Don Kleinschnitz Jr.** *January 05, 2017 23:39*

**+Chris Sader** shit , I missed that its working :).


---
**Ariel Yahni (UniKpty)** *January 05, 2017 23:50*

**+Don Kleinschnitz** Same as my setup


---
*Imported from [Google+](https://plus.google.com/107747838010116449754/posts/Ska3CjkrZX9) &mdash; content and formatting may not be reliable*
