---
layout: post
title: "Just ordered my K40 laser this weekend, coming from a US based warehouse, so SHOULD be here pretty soon"
date: June 20, 2016 17:41
category: "Discussion"
author: "Terry Taylor"
---
Just ordered my K40 laser this weekend, coming from a US based warehouse, so SHOULD be here pretty soon.





**"Terry Taylor"**

---
---
**Anthony Bolgar** *June 20, 2016 18:42*

Welcome to the club. Make sure that you tell everyone in the house that if they don't want it laser engraved, don't leave it out (This includes pets and elderly relatives) <grin>


---
**Ariel Yahni (UniKpty)** *June 20, 2016 18:57*

**+Anthony Bolgar**​ we need a warning sing 


---
**Anthony Bolgar** *June 20, 2016 18:58*

What warning should we sing? Or did you mean sign?...lol!


---
**Ariel Yahni (UniKpty)** *June 20, 2016 19:35*

We could sing also


---
**Ray Kholodovsky (Cohesion3D)** *June 20, 2016 21:12*

Just make the laser sing, as it is engraving the warning sign.  You know, stepper motor music and all.


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/HkT6kE1sTnu) &mdash; content and formatting may not be reliable*
