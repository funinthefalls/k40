---
layout: post
title: "So I have been using a airbrush compressor for my air assist but I think it's time to change this out"
date: December 30, 2017 20:17
category: "Modification"
author: "Chris Hurley"
---
So I have been using a airbrush compressor for my air assist but I think it's time to change this out. The airbrush compressor supply is effective but is loud (old style) and heats up after 2 hours of use. I have been looking at the fish tank compressors and have seem them in alot of your builds but I'm unsure of the size being used or how they would compare to the airbrush compressor. Could you point me to the size I need? (20w,40w 802 or 100w) 





**"Chris Hurley"**

---
---
**Ned Hill** *December 30, 2017 22:57*

I'm still using an airbrush compressor, but I can recommend that you get something that will put out at least 10L/min.  


---
**Don Kleinschnitz Jr.** *December 31, 2017 14:08*

[plus.google.com - K40 Airflow and Assist](https://plus.google.com/collection/gf0DkB)


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/dva28m4HrCW) &mdash; content and formatting may not be reliable*
