---
layout: post
title: "Bored with your laser? Looking for something different to do with it?"
date: August 24, 2018 04:23
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Bored with your laser? Looking for something different to do with it? How about A short stop-motion study with laser cut wood?



[https://www.thisiscolossal.com/2014/04/woodoo-a-stop-motion-animation-with-laser-cut-wood/](https://www.thisiscolossal.com/2014/04/woodoo-a-stop-motion-animation-with-laser-cut-wood/)





**"HalfNormal"**

---
---
**Ned Hill** *August 25, 2018 13:11*

lol, hmm give me some ideas :)


---
**Kelly Burns** *August 27, 2018 04:08*

Very clever 


---
**Mike Meyer** *September 01, 2018 17:42*

Brilliant!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/7QA7o1wj6UH) &mdash; content and formatting may not be reliable*
