---
layout: post
title: "So Annoying we can't continue a thread and add photos to our successive replies :( The bed arrived today, honeycomb in a frame"
date: December 10, 2015 16:59
category: "Modification"
author: "Gary McKinnon"
---
So Annoying we can't continue a thread and add photos to our successive replies :(



The bed arrived today, honeycomb in a frame. I have to update the extraction before installing the bed.



It's 320x220mm, the outer edging is 22mm, i'm assumin i'm okay to cut over that ?



I also got some black aluminium to put underneath and stop the beam going through the air vent in the bottom of the chassis.



![images/268c312ae4a4f50e73916c6ccac545b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/268c312ae4a4f50e73916c6ccac545b1.jpeg)



**"Gary McKinnon"**

---
---
**Tony Schelts** *December 10, 2015 22:53*

how much did it cost, and was this to the uk?


---
**Gary McKinnon** *December 11, 2015 01:30*

Came from China, ordered on 28th November. £30 ($45) :



[http://www.ebay.co.uk/itm/171963291915?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.co.uk/itm/171963291915?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Gary McKinnon** *December 11, 2015 01:30*

Yes to UK.


---
**Tony Schelts** *December 13, 2015 23:52*

Gary how are you going to fix it, my machine only has 5  threaded posts. Is yours the same? do you know it is okay to screw brackets onto the sides?


---
**Gary McKinnon** *December 14, 2015 10:16*

I've been too busy to look at it in detail, i also have the five posts. Will post when i do it.


---
*Imported from [Google+](https://plus.google.com/+GaryMcKinnonUFO/posts/UJKRoAjYWBw) &mdash; content and formatting may not be reliable*
