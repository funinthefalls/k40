---
layout: post
title: "The new engraver came in today... Damn it's sweet, did some testing....the alignment is dead on"
date: December 24, 2015 02:15
category: "Discussion"
author: "Scott Thorne"
---
The new engraver came in today...

Damn it's sweet, did some testing....the alignment is dead on.



![images/f5f553698d9fd3a8924cc26973949317.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f5f553698d9fd3a8924cc26973949317.jpeg)
![images/1e71671e024c37a55f666c3214408625.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e71671e024c37a55f666c3214408625.jpeg)
![images/ab15674f0390a75c79250440db31321a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab15674f0390a75c79250440db31321a.jpeg)
![images/71f6175fcadbafb7ef350462f3736685.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/71f6175fcadbafb7ef350462f3736685.jpeg)

**"Scott Thorne"**

---
---
**Todd Miller** *December 24, 2015 02:45*

That's the Shit !  Merry X-mas to you !


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 24, 2015 07:57*

That looks like it's actually built much better than the K40s, even though I assume it is similar manufacturer. Great holiday present to yourself :D Have fun.


---
**Scott Thorne** *December 24, 2015 12:09*

Thanks guys....I hope everyone has a merry Christmas!


---
**Ross Hendrickson** *December 24, 2015 14:12*

Nice! Where did you source it?


---
**Scott Thorne** *December 24, 2015 14:20*

Amazon....andyking company......it has a 50 watt reci tube....nice man....everything came perfectly aligned.


---
**Scott Thorne** *December 25, 2015 20:00*

Yup


---
**Coherent** *December 29, 2015 19:15*

In the photo it looks like a motor attached to the table? The similar models on ebay show the same setup, but state "manual up down table". I wondered if the Chinese description of manual was different than mine (as some of their descriptions when translated are).  Is it motorized by switch or panel control or can/must you adjust the height by turning the knob on top? (or both).


---
**Scott Thorne** *December 29, 2015 19:17*

Push buttons on the side for up and down.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 30, 2015 02:49*

**+Coherent** I imagine that "Manual" refers to you needing to press the buttons Up/Down in order to get it to go up or down, rather than being automatically controlled by software. Chinese translations are difficult to read, yet sometimes amusing.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/Y72F75UwQWV) &mdash; content and formatting may not be reliable*
