---
layout: post
title: "Hey guys, I recently bought a used red and black 500x700 cutter"
date: February 09, 2018 11:06
category: "Smoothieboard Modification"
author: "Chris Menchions"
---
Hey guys, I recently bought a used red and black 500x700 cutter. The machine didn't have the controller so I am attempting to convert it to smoothieware or grbl. It's a 130w tube with 150w psu, I've wired it like I did with my k40 or as best I can due to the differences between the 40w psu and the 150w.



I wired in a pot and connected L to my smoothieboard. It moves and homes fine, test fire works but when I try to run a job it seems to short out the 12v psu I have driving my smoothieboard. The board doesn't seem affected but the psu light flickers on and off and the tube doesn't fire. Every time the tube should fire the psu dies.



Only things connected to the psu is leds and the board which I already tried removing the leds to see if power was leaking.



Another thing I noticed when I connect P and G the laser fires right away...I thought p was the interlock.



I'm kind of lost now on what to do or check. Any suggestions?





**"Chris Menchions"**

---
---
**Chris Menchions** *February 09, 2018 11:07*

Sorry about the user name my kids changed the wrong account name now I can't change it back for 2 months.


---
**Don Kleinschnitz Jr.** *February 09, 2018 12:02*

We need to know more about your setup to be able to help:



Wiring diagram of power and PWM signal connections?



Pictures of:

1. LPS showing the connectors

2. DC power supply showing connectors

3. Smoothie showing its connections to the LPS






---
**Chris Menchions** *February 09, 2018 12:28*

Sure thing, thanks for taking the time to answer. 



I will get some pictures tonight after work though it's a huge mess right now until I have it working correctly.



I did read on another forum that a pot and pwm isn't possible for my supply. It has 3 pins like on a regular psu for gnd/N/L then one connector with T,L,P,G, IN,5V. Which is different from my k40psu.



I'll get the info and get back to you.


---
**Don Kleinschnitz Jr.** *February 09, 2018 16:35*

**+Etho Pro 888's Slab Lab** you should be able to put a pot on the "IN" pin. The photo will help ...


---
**Chris Menchions** *February 09, 2018 17:15*

Yeah I'll get them asap. I did have the pot on 5v and in like the k40!


---
**Chris Menchions** *February 11, 2018 12:48*

So I tore out all the wires to start over. Here is my plan, let me know what you think!![images/8e06823e0a9cb7fb810b54ea54bd060e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e06823e0a9cb7fb810b54ea54bd060e.jpeg)


---
**Don Kleinschnitz Jr.** *February 11, 2018 16:41*

Need to know exactly what pins on what smoothie your using. 

The L pin needs a ground through a mosfets drain. The + on most smoothie mosfets is VDD so in your schematic. In your drawing you may be grounding the VDD supply.

Also insure that the L pin on the rightmost connector is left open. 


---
**Chris Menchions** *February 12, 2018 10:15*

It's actually mks sbase not genuine smoothie but pins should be the same. Using p2.4 as stated above and it's ground. They are connected(tried reversing the wires also ) to L and G on lps. 


---
**Chris Menchions** *February 12, 2018 10:17*

L left open? That's where the pwm hooks up on the 40w supply? Or is the 40w completely different beast?


---
**Chris Menchions** *February 12, 2018 10:21*

This has me confused as the lps hasn't got the same pinout as the 40w used on k40 machines.


---
**Don Kleinschnitz Jr.** *February 12, 2018 13:18*

**+Etho Pro 888's Slab Lab** I suggest posting a picture of the connectors on your LPS, that will help me get oriented.



The only thing that I see that may be a problem is the output (PWM) on your MKS Sbase P2.4. Often the + is pulled up to VDD on the card. In your drawing you have it connected to GND and that would connect the  the VDD + pullup to the LPS gnd. I am not sure about this because I do not have a MKS schematic, do you?



[https://photos.app.goo.gl/0Rp0XYTBWRvfvR5h1](https://photos.app.goo.gl/0Rp0XYTBWRvfvR5h1)



You may not want to connect the + P2.4 at all and use another verified ground from elsewhere on the MKS board.



With the machines power off, using a DVM on ohms, see if the + P2.4 is connected to ground or to VDD through some resistance.




---
**Chris Menchions** *February 12, 2018 13:21*

I will post pc of the connector! Ok I did try both ways but I'll show what I mean on my pics. I'll test the pins also on ohms and report back.


---
**Don Kleinschnitz Jr.** *February 12, 2018 14:22*

**+Etho Pro 888's Slab Lab** ... and a picture of the LPS label with part # might help.



I also noticed when rereading your post:

<i>Another thing I noticed when I connect P and G the laser fires right away...I thought p was the interlock.</i>

Does this happen when there is nothing else connected to the connector on L or H?






---
**Chris Menchions** *February 12, 2018 14:40*

Sure but I don't think there is a label on it with model number but I'll pull out out and check.



I haven't tested any other configuration as I was worried about damaging the supply or tube. Figured best to ask the pros first ;)


---
**Chris Menchions** *February 12, 2018 18:52*

Sorry just re reading...I don't have schematic for the board only pinout. U know what mks are like.


---
**Chris Menchions** *February 12, 2018 20:59*

![images/30ab9bb6c16a93bf89cc003ddd4edb6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/30ab9bb6c16a93bf89cc003ddd4edb6a.jpeg)


---
**Chris Menchions** *February 12, 2018 21:00*

![images/e719fe547186afb70c44c49734b24bef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e719fe547186afb70c44c49734b24bef.jpeg)


---
**Chris Menchions** *February 12, 2018 21:07*

I don't have anything hooked up anymore so I'm going to wire as per your instructions :) I'll do the pot to 5v in G firstly.




---
**Chris Menchions** *February 12, 2018 21:18*

ok no resistance on any of my ohm settings between the 2 fan (P2.4) terminals




---
**Don Kleinschnitz Jr.** *February 13, 2018 00:47*

**+Etho Pro 888's Slab Lab** Huh.... 



You measured:

From the P2.4+ to the VDD (+12 or +24VDC) and you got an open?

You may be missing a power jumper. But not to worry because I do not think you want this pin anyway.



The P2.4- may read open as hopefully it is the drain of a MOSFET.

 



<s>--------------------</s>

Lets move on to the wiring: 

FYI This supply looks like it has the same connections as yours?



_ [https://www.aliexpress.com/item/MYJG150W-new-CO2-laser-power-supply-150W-with-signal-fault-diagnosis-voltage-regulator-and-LED-display/32792418545.html](https://www.aliexpress.com/item/MYJG150W-new-CO2-laser-power-supply-150W-with-signal-fault-diagnosis-voltage-regulator-and-LED-display/32792418545.html) _



<b>Below is my recommendations for wiring.</b>



Once wired I would leave the wire to P2.4- open and test the setup by grounding it. 

With P at ground (enable switch and interlocks etc closed) the laser should fire when the wire on L is touched to ground.



Then you can hook up the controller and try it .....



[photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/7JaZErfSaUoA6khZ2)




---
**Chris Menchions** *February 13, 2018 01:11*

I tested the+/-of p2.4. Sorry. 



That is my psu yes!



Ok I will follow the instructions tomorrow after work and let you know. Thanks


---
**Chris Menchions** *February 13, 2018 01:18*

Also where the 5k resistor I'm adding a 5k pot just to confirm with you.


---
**Don Kleinschnitz Jr.** *February 13, 2018 04:23*

**+Etho Pro 888's Slab Lab** that is a 5K pot  ;)? Note the wiper ...


---
**Chris Menchions** *February 13, 2018 08:07*

I'm a bit slow lol


---
**Chris Menchions** *February 14, 2018 22:46*

Ok so wired as your diagram the laser is always firing. this is hooked to 2.4 pos terminal. 

without P and G it doesn't fire which is correct.




---
**Don Kleinschnitz Jr.** *February 15, 2018 00:35*

**+Etho Pro 888's Slab Lab** does it fire all the time if you remove the smoothie's PWM wire?


---
**Chris Menchions** *February 15, 2018 01:04*

I will check tomorrow


---
**Chris Menchions** *February 15, 2018 13:43*

On my k40 L is on ground p2.4 and gnd is on positive but it's a different board that's an smini...like sbase but without heat bed mosfet.



I did try to wire it that way and well it shorts out of course. Wonder if the board is bad...


---
**Don Kleinschnitz Jr.** *February 15, 2018 14:33*

**+Etho Pro 888's Slab Lab** first lets take the board out of the circuit. With L on the LPS connector "DISCONNECTED" and everything else wired as my drawing shows does the laser fire all the time?


---
**Chris Menchions** *February 15, 2018 14:34*

Ok I'll do that this evening and get back to you.


---
**Chris Menchions** *February 15, 2018 21:25*

Yes it fires when not hooked up to the sbase. P is still tied to G


---
**Chris Menchions** *February 16, 2018 07:49*

Hopefully the psu isn't fried. I seen it working a while back on the stock controller.


---
**Chris Menchions** *February 16, 2018 12:07*

Could the psu be labeled incorrectly?


---
**Don Kleinschnitz Jr.** *February 16, 2018 14:56*

I doubt it's labeled wrong. We have something keeping the LPS enabled. 



Leaving the controller wire disconnected. 

Now disconnect the T wire from the supply and see if it fires all the time?



Btw what kind of pushbutton  do you have on the T connection and what pins are u using the NC or NO connections?




---
**Chris Menchions** *February 16, 2018 14:59*

I have nothing going to T. Never did hook anything to that. Only terminals I touched were L, p, g, in, 5v.



I have a guy with similar machine and psu going to send me pics of his but where this is not going to be the same controller I dunno what good it will do.


---
**Chris Menchions** *February 16, 2018 15:04*

Where would T go to?


---
**Don Kleinschnitz Jr.** *February 16, 2018 15:06*

I'm confused did you wire per the drawing I posted?

Can you draw for me your version of what is currently wired?.


---
**Chris Menchions** *February 16, 2018 15:06*

Sorry I'm not using any pushbutton. I just want to get it running bare min then I'll hook up the interlock and test fire.



What is nc and no? 


---
**Chris Menchions** *February 16, 2018 15:08*

I did wire it like the diagram except I left t unhooked as I don't even have a button purchased yet. The P is short wire I jumped to G. 



Once I confirm it functions properly I was going to connect the door switch etc and add a test button.


---
**Chris Menchions** *February 16, 2018 15:10*

I will get a pic in a few hrs of what I have wired for u to confirm. But right now nothing is hooked up to the sbase, pot works but with p jumped to g laser always in fire mode


---
**Don Kleinschnitz Jr.** *February 16, 2018 15:11*

Ok if you do not have a PB then let's skip that for now. 

I need to know exactly what is connected where. 

We can ignore the controller for now and just get the supply to work. 


---
**Chris Menchions** *February 16, 2018 15:12*

Exactly my thoughts! I'll get u pictures and edit to label the wires asap!


---
**Don Kleinschnitz Jr.** *February 16, 2018 15:14*

You have P connected to G and the pot connected as my drawing shows.

Nothing is on L or T? 

In this config it fires all the time? 


---
**Chris Menchions** *February 16, 2018 15:14*

Yes sir that is correct


---
**Don Kleinschnitz Jr.** *February 16, 2018 15:16*

Disconnect P, does it fire all the time? 


---
**Chris Menchions** *February 16, 2018 15:22*

No it does not fire even when L was on my p2.4...I tried on the ground and on pos for p2.4(fan output)..my k40 L goes to neg terminal and the ground goes to the pos terminal..see attached pic. Red wire is L and blue is from g on lps.https://lh3.googleusercontent.com/uLasH60Fa3f7ENsPHe_i5QX37OvGaZdTpRt3JPvA4aEVLcHXywWJgwTVbmlOUY57Cp0U8nLJKQ


---
**Don Kleinschnitz Jr.** *February 16, 2018 17:56*

**+Etho Pro 888's Slab Lab** 

We do not want the controller connected to the LPS at all for this test?? I am confused as to why are we looking at pictures of the controller?



TOTALLY disconnect the controller from the LPS and set aside. 

Post a picture of just the LPS with its wiring clearly in view, pls.



I would like the pot connected to 5V, IN, G as my drawing shows and T,L,P not connected to anything. 



Then see if when you turn on the LPS it fires.


---
**Chris Menchions** *February 16, 2018 17:58*

Yes I understand...that is controller on my k40. Just showing how L is on ground and ground is on positive of the fan output. Sorry for the confusion.


---
**Don Kleinschnitz Jr.** *February 16, 2018 18:01*

**+Etho Pro 888's Slab Lab** I am very confused as to what the configuration is right now :).

Is the LPS currently connected to the controller in any way. Can you post a picture of the connections on the LPS?


---
**Chris Menchions** *February 16, 2018 18:03*

Yeah...my point was with showing u the controller on the k40 that the L goes to neg on the fan output. 



You said to wire the L on the red/black m machine to pos.



I'm just confused as to why the k40 works is all.


---
**Don Kleinschnitz Jr.** *February 16, 2018 18:07*

We are trying to isolate the LPS and verify that its control works as we expect. Then we will connect the controller?



--------- Starting Step 1: <s>------</s>

Totally isolate the LPS from the controller: no wires from the controller to the LPS.



No wires on T, L, P.



<b>On the LPS:</b>

Connect the pot across 5V and ground (G) with the wiper (center leg) connected to the IN.

Post a picture of the LPS connector wired like this.



Turn on the supply and see if it fires.  


---
**Chris Menchions** *February 16, 2018 18:08*

Yes I understand. Let u know in about an hour.


---
**Chris Menchions** *February 16, 2018 21:14*

It does NOT constantly fire with the jumper removed from P and G.


---
**Chris Menchions** *February 16, 2018 21:26*

![images/ee5c9a46b5c0a1a7ccfb6dd3cc418300.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee5c9a46b5c0a1a7ccfb6dd3cc418300.jpeg)


---
**Don Kleinschnitz Jr.** *February 16, 2018 22:51*

1: The picture shows T and P disconnected and the annotation shows that L is not connected either... Y/N?



2: If you plug the connector in like the picture shows it does NOT fire the laser continuously ...Y/N?



3: if you only add a wire from the P to ground and plug it in the LPS fires constantly Yes/No?


---
**Chris Menchions** *February 16, 2018 22:54*

1:Y-L is not connected

2:Y-No it does not

3:Y-It fires constantly



Hope thats clear enough


---
**Don Kleinschnitz Jr.** *February 16, 2018 22:54*

Do you have a P and L led on the LPS?




---
**Chris Menchions** *February 16, 2018 22:56*

Yes I believe so. I'll be in the shop in a few. But threes a power and 2 others I believe


---
**Don Kleinschnitz Jr.** *February 16, 2018 22:59*

**+Etho Pro 888's Slab Lab** 

4: Ok when you have P connected to G and T&L are not connected it fires. What state are the P and L Leds in?



5: BTW when you say it fires all the time I assume you mean the laser tube ionizes?


---
**Chris Menchions** *February 16, 2018 23:01*

I think they they are both on. The 2 between the ac power and the green plug. One red one green I think


---
**Chris Menchions** *February 16, 2018 23:14*

5: BTW when you say it fires all the time I assume you mean the laser tube ionizes?



Correct and i can control the power level with the pot. 




---
**Don Kleinschnitz Jr.** *February 16, 2018 23:15*

**+Etho Pro 888's Slab Lab** I am looking for these LED's





![images/e38dff80522b9557d3fd90aff3930650.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e38dff80522b9557d3fd90aff3930650.jpeg)


---
**Chris Menchions** *February 16, 2018 23:16*

![images/0af6d750df92ff43085d720a9f38a2e8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0af6d750df92ff43085d720a9f38a2e8.jpeg)


---
**Don Kleinschnitz Jr.** *February 16, 2018 23:18*

Ok the LPS should not be firing without the T or L being asserted???

Something weird here ....


---
**Chris Menchions** *February 16, 2018 23:19*

yes...I'll attach the jumper again and take a pic of the leds to show u




---
**Don Kleinschnitz Jr.** *February 16, 2018 23:24*

**+Etho Pro 888's Slab Lab** OK




---
**Chris Menchions** *February 16, 2018 23:30*

![images/57bb98f90c80e3f1fae021c565ccd609.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57bb98f90c80e3f1fae021c565ccd609.jpeg)


---
**Don Kleinschnitz Jr.** *February 16, 2018 23:35*

That picture  is suggesting that L is asserted??????


---
**Don Kleinschnitz Jr.** *February 16, 2018 23:37*

Ok disconnect the P and plug it back in and show me the LED's


---
**Chris Menchions** *February 16, 2018 23:56*

Yes but it is not. L is most definitely not connected.



Ok doing that now.


---
*Imported from [Google+](https://plus.google.com/104576389148296391165/posts/J4yj7nRctjR) &mdash; content and formatting may not be reliable*
