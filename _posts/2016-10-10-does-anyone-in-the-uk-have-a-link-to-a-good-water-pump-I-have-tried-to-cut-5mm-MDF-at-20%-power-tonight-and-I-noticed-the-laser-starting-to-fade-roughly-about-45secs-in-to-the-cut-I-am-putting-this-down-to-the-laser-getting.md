---
layout: post
title: "does anyone in the uk have a link to a good water pump, I have tried to cut 5mm MDF at 20% power tonight and I noticed the laser starting to fade roughly about 45secs in to the cut, I am putting this down to the laser getting"
date: October 10, 2016 19:53
category: "Discussion"
author: "Norman Kirby"
---
does anyone in the uk have a link to a good water pump, I have tried to cut 5mm MDF at 20% power tonight and I noticed the laser starting to fade roughly about 45secs in to the cut, I am putting this down to the laser getting to hot and there for reducing the power output, can anyone help please







**"Norman Kirby"**

---
---
**Scott Marshall** *October 10, 2016 22:18*

I can't help you on the link, being on the wrong side of the pond. but  I can tell you that the stock pump, while being far from high end, is pretty reliable and they don't fail too often. 



Common things that reduce flow as you describe are dirt, fuzz and slime in the intake screen, pinched or kinked tubing -very commonly found at the rear where the tubing hangs out the cabinet holes - the tubing is silicone and very soft and easily kinked.



Pop the screen off the pump and give it all a quick soak in water with a good dose of bleach added. this will strip of the slime (bacteria) and then run some bleach (enough so it smells like a swimming pool will do) in the water in the future to keep stuff from growing. Alcohol works too and doesn't smell.



I've seen similar pumps with pet hair wrapped on the shaft, binding up the bearing as well. 



When all is well, the stock system should fill a 1 gallon jug about 1/2 full in 60seconds. About 1/2gpm or 2Lpm.



This might get you running but if you want a better pump down the road, the Little Giant PE-1 is a good quality replacement pump which is just right for the k40 and available worldwide.





Happy Lasering...



Scott






---
**Phillip Conroy** *October 11, 2016 07:53*

do you mean 5mm thick mdf?, have you cut this thickness before.Even at 100% power [18ma] i can not cut 4mm ply,not enen at 2mm/sec,it cut maybe 50 % into the 4mm


---
**Kirk Yarina** *October 11, 2016 13:25*

You can use blue windshield washer fluid for cooling (like I do), plus you get freeze protection if you need it.  Pink RV antifreeze also is said to work, although I haven't tried it.  Both seem to keep the slime away.


---
**Scott Marshall** *October 13, 2016 12:25*

**+Kirk Yarina** I like that. Great tip, Thanks Kirk.


---
**Scott Marshall** *October 13, 2016 12:28*

**+Phillip Conroy** 

 Align your mirrors meticulously  and focus with care.



 It will transform your laser. Free horsepower!! How can you not love that??!!



If mine is aligned "pretty close" it struggles to cut 3mm ply in 1 pass, and often won't. Get it adjusted perfect and wow. Another kick in the pants can be had by going to a 2 1/2" lens - they have a longer "in focus" range (depth of field for you camera guys) and so are more concentrated thru the  longer distance. (5mm out of focus will kill the cutting power alone) Aim (focus on) the center of the material. Air assist is required, as firing thru a cloud of smoke weakens the beam as well. 



It's all the little things that add up to weaken the laser. Eliminate them and you'll be amazed what a 30w laser can do.


---
*Imported from [Google+](https://plus.google.com/103798744920995011573/posts/bWSVZqYdPuv) &mdash; content and formatting may not be reliable*
