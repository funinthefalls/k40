---
layout: post
title: "Hi Guys just got both my smoothie board and maker base sbase boards they both arrived at the same time, so comparing both boards side by side i have to say the sbase board is well done it is 5 x and already has the power"
date: June 10, 2016 17:05
category: "Discussion"
author: "Dennis Fuente"
---
Hi Guys just got both my smoothie board and maker base sbase boards they both arrived at the same time, so comparing both boards side by side i have to say the sbase board is well done it is 5 x and already has the power relay on the board it has heat sinks on the driver chips the smoothie dose not have the regualtor relay on it and no heat sinks on the chips for the price i have to say the sbase is a better board





**"Dennis Fuente"**

---
---
**Anthony Bolgar** *June 10, 2016 17:20*

The smoothie heat sinks are actually the way the board was designed, the bottom of the board is a giant heat sink. And for the few extra dollars you have access to support from **+Arthur Wolf** and the boys directly. Also, purchasing a true smoothie helps fund the next generation of smoothieware and smoothieboard. That being said, the MKS Sbase board is well made, and if money is an issue then it is a good choice.


---
**Arthur Wolf** *June 10, 2016 17:26*

﻿**+Dennis Fuente** The heatsinks on the SBASE are stupid design, anybody who has taken 10 minutes to read the datasheet would not have done it that way. The drivers are designed to be cooled from the bottom, using the copper of the PCB. That's how Smoothie does it, and because of this, we get perfect heatsinking. By contrast, on the MKS, you could potentially get the drivers to de-solder themselves in some situations.



About the voltage regulator, we do not include it by default on the Smoothieboard in an effort to make the board as inexpensive as possible. When we first designed the board some years ago, using voltage regulators was very rare. It's getting more common now, and MKS copied the idea from Smoothieboard ( like everything on that board really ). On Smoothieboard v2, it's possible we will be including the voltage regulator by default ( or at least make it not require any soldering ).



A further note on the SBASE. As you use the SBASE, you might come to appreciate some things about it, like how easy it is to configure, how powerful and full of functionalities it is. All of those things are things that the Smoothie community did, it's tens of thousands of hours from hundreds of volunteers giving their time away to the project. And SBASE is stealing that from the community. They are taking a project that is open-source, making it closed-source, and hurting the community immensely by doing that. MKS is toxic, if there is anything about it you like, by buying the board, you are working on destroying that thing you like.



Cheers.


---
**Dennis Fuente** *June 10, 2016 17:56*

Hello Arthur First off thanks for being part of the design team on the smoothie board as I mentioned I did purchase a genuine smoothie and I understand your complaint on the coping as I have had my own small business and did my own design and research only to have the Chinese copy it and then sell it for less so I get it, I am only making observations and comparisons to between the two boards at first glance the regulators from digi key are only 3 bucks and cheaper if you buy more then one a friend and I are both doing CNC projects and we both have the sbase and smoothie boards we both plan to use and compare the two i am sure that quality will prevail.


---
**Arthur Wolf** *June 10, 2016 18:00*

**+Dennis Fuente** They are very very similar boards : MKS essentially copied the Smoothieboard design and made a few modifications, so you won't see much difference there. There has already been quite a few comparison and not much emerged from those ( the SBASE has quite a few design flaws, but those won't show up for most users. See reprap forums ).

The real difference is both the price, and the fact that things like MKS are slowly destroying the work of the community.


---
**Dennis Fuente** *June 11, 2016 22:28*

I am in support of your effort and as i stated i purcahsed a genuine smoothie when they became available as i didn't know when the boards would come into uberclock i oredered a Sbase board so now i am looking to install a board into my k-40 or build a new machine all together and use this board to run it, thanks again for your and others work on this project.


---
**Arthur Wolf** *June 12, 2016 08:50*

**+Dennis Fuente** Sorry if I sounded harsh, it's nothing against you at all, it's just that often people don't realize how toxic the MKS stuff is, and I can get pretty passionate about it ( it's threatening years of work ).

I hope your K40 will work well, and I'm around if you need help with anything.


---
**Dennis Fuente** *June 12, 2016 15:54*

Arthur 

I get it honestly i have not started on the change yet i have some others thing's i need to get done can't thank guys like you enough for taking the time to make this stuff and no i did not take it personally


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/L8cxZox9Wkr) &mdash; content and formatting may not be reliable*
