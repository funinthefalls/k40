---
layout: post
title: "sorry if it has been asked before but i'm looking to get a K40 and can't find if there is any real (better electronics / board has better features / better PSU ?) difference between the \"analog\" and \"digital\" readout versions"
date: October 27, 2017 12:57
category: "Discussion"
author: "Gunther Vermeir"
---
sorry if it has been asked before but i'm looking to get a K40 and can't find if there is any real (better electronics / board has better features / better PSU  ?) difference between the "analog" and "digital" readout versions (i searched and did read up :) ) or not.



I'm EU based (don't want to import from china) and on ebay the analog is 315€ [http://www.benl.ebay.be/itm/USB-Laser-Machine-Laser-a-Graver-Engraving-Cutting-Machine-Engrave-40W-CO2-UK-PL/162594923528](http://www.benl.ebay.be/itm/USB-Laser-Machine-Laser-a-Graver-Engraving-Cutting-Machine-Engrave-40W-CO2-UK-PL/162594923528)

and the "digital" is 400€ [http://www.benl.ebay.be/itm/40W-USB-CO2-Upgrade-de-gravure-laser-Machine-de-decoupe-gravure-Cutter-gravure/161947562700](http://www.benl.ebay.be/itm/40W-USB-CO2-Upgrade-de-gravure-laser-Machine-de-decoupe-gravure-Cutter-gravure/161947562700)

...85€ is a rather big difference at that pricepoint.

Don't really care about led lights or wheels , the water temp is a 2 usd addon...

I get that a digital display value might be a bit more convenient to set the laser power (aldo i like knobs :)..) but is that the only difference?

I'm aware many people add a analog meter to the digital version , that is not what i'm asking.



thank you!







**"Gunther Vermeir"**

---


---
*Imported from [Google+](https://plus.google.com/+GuntherVermeir/posts/QA9kMqkRCL2) &mdash; content and formatting may not be reliable*
