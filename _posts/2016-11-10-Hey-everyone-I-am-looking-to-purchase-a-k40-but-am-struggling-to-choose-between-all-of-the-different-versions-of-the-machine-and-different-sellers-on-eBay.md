---
layout: post
title: "Hey everyone, I am looking to purchase a k40 but am struggling to choose between all of the different versions of the machine and different sellers on eBay"
date: November 10, 2016 20:11
category: "Discussion"
author: "Brendan Sherry"
---
Hey everyone, I am looking to purchase a k40 but am struggling to choose between all of the different versions of the machine and different sellers on eBay. Is there any kind of buyers guide that exists which covers the different features that are available and if they are worth the extra money. I have searched around and have not been able to find anything. Also, are there any recommended eBay sellers? Are there any to stay away from?



Any help that could be provided would be greatly appreciated. Thanks!





**"Brendan Sherry"**

---
---
**Antonio Garcia** *November 10, 2016 22:29*

Hi there,

I´m in the same boat :), at the moment I don´t know if it´s worth it pay a little be more, and get a k40 with some upgrades

[http://www.ebay.co.uk/itm/300x200mm-Engraving-Area-40W-CO2-Laser-Engraver-machine-Power-Master-Switch-/161799971586?hash=item25ac07a302:g:GNQAAOSwOyJX9xlI](http://www.ebay.co.uk/itm/300x200mm-Engraving-Area-40W-CO2-Laser-Engraver-machine-Power-Master-Switch-/161799971586?hash=item25ac07a302:g:GNQAAOSwOyJX9xlI)

or go for the cheapest and then slowly make upgrades by yourself....

[http://www.ebay.es/itm/40W-CO2-USB-Laser-Engraver-Cutter-Engraving-Cutting-Machine-Laser-Printer-/231777365741?_trksid=p2141725.m3641.l6368](http://www.ebay.es/itm/40W-CO2-USB-Laser-Engraver-Cutter-Engraving-Cutting-Machine-Laser-Printer-/231777365741?_trksid=p2141725.m3641.l6368)

[ebay.co.uk - Details about  300x200mm Engraving Area 40W CO2 Laser Engraver machine +Power Master Switch](http://www.ebay.co.uk/itm/300x200mm-Engraving-Area-40W-CO2-Laser-Engraver-machine-Power-Master-Switch-/161799971586?hash=item25ac07a302:g:GNQAAOSwOyJX9xlI)


---
**HalfNormal** *November 10, 2016 23:58*

You know what they say, opinions are like a-holes, everyone has one! You will find the same issue on the value of buying a stripped down version and doing your own upgrades to buy what you can afford. Also, some people have had great luck with one vendor but others do not receive the same service.

So what is the best advice? Search this site and all other sites to find the answer to your questions. Do not believe everything the vendor claims even in an email reply. Close your eyes and throw a dart at a list of possible vendors to pick one out! You are not working with A+ vendors so just be prepared to take a hit in the wallet if and when you need vendor support.

Want quality and support? Pay for it from a namebrand vendor! ;-)


---
**Bill Keeter** *November 11, 2016 01:33*

The trick is to find a vendor that has high volume and that also HATES negative feedback. I received a K40 with a cracked laser tube. Took a ton of photos and messaged the seller. A week later a new tube was delivered for free. All cus they absolutely wanted positive feedback on ebay. Negative feedback kills sales.



So buy from one of the big sellers and hold your feedback score until they've made you happy.



(I used ebay seller: globalfreeshipping)


---
**Mark Leino** *November 11, 2016 04:02*

**+Bill Keeter** I just recently had great service from globalfreeshipping as well, because like you said, they would have done just about anything to avoid negative feedback. Their price was actually the best at the time I ordered as well. 


---
**Brendan Sherry** *November 11, 2016 14:55*

Thanks everyone for the great advice, are there any differences between red units and the blue units that can be found on eBay?


---
*Imported from [Google+](https://plus.google.com/108248381027751686716/posts/BX29jf4Hdtr) &mdash; content and formatting may not be reliable*
