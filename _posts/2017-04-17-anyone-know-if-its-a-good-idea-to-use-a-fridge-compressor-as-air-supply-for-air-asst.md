---
layout: post
title: "anyone know if it's a good idea to use a fridge compressor as air supply for air asst?"
date: April 17, 2017 01:32
category: "Modification"
author: "Chris Hurley"
---
anyone know if it's a good idea to use a fridge compressor as air supply for air asst? I'm looking into doing this because I get black charring when cutting 3mm mdf.  





**"Chris Hurley"**

---
---
**Chris Hurley** *April 17, 2017 01:33*

I should also say I get the compressor for free. 

 


---
**Jaden Bragge** *April 17, 2017 05:01*

I was looking at doing this, I found while they can reach high pressure, their flow is pretty average. Something like 1 cf/min. Thats what I've read, if anyone has actually done it, I'd take their word on it. I just ended up using a $10 aquarium air pump


---
**Robi Akerley-McKee** *April 17, 2017 06:31*

I use an air pump from a select comfort mattress.  Using a relay to switch it on and off.  Relay is powered by a 12vdc wall wart. Using an 2n2222 transister with common grnd to the wall wart an a 5vdc signal off my RAMPS 1.4 board (MKS Gen 1.4 in the other k40).


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/eV7gkSFBSt5) &mdash; content and formatting may not be reliable*
