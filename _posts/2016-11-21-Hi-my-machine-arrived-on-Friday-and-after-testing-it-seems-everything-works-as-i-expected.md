---
layout: post
title: "Hi, my machine arrived on Friday, and after testing it seems everything works as i expected"
date: November 21, 2016 19:32
category: "Discussion"
author: "Antonio Garcia"
---
Hi,

my machine arrived on Friday, and after testing it seems everything works as i expected.



It had a small crash in one side of the case , but nothing important.

It came with the mirrors aligned, but after disassembling to cut the big blue extractor, and try to square the x and y axis, i had to aligned them back.

I´m quite happy with the machine, for the price, but now start the upgrading time :).



I have some questions. I bought a head air assist, a bike compressor (probably is not the best choice, but it was really cheap in my local store), but my doubts are for upgrading the controller. 

I´m not sure if go for (like most of you)  smoothie or try to use some of my old controllers from my old cnc (grbl shield)  or my 3d printer (sav mk1), what do you think??



The software which comes with the machine is a pain in the ass, every time i try to make a job with a few tasks (engraving + cutting ), after placing the first task, the plugin for the corel laser disappears, so i just can make one task per time,  anyway i don´t like the software too much, so i will go for laserweb :)



I could cut acrylic (3mm) and plywood(5mm) in two passes, not bad at all, for my first attempt.



Thanks you for all of you, because after reading this group the last 20 days, before my machine arrived, the experience was painless ;) at least so far.



![images/ec715ae8c0c806262358339fda61288e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec715ae8c0c806262358339fda61288e.jpeg)
![images/cbb5f5cf31c6a667fa56c32d9a4ea3ec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cbb5f5cf31c6a667fa56c32d9a4ea3ec.jpeg)

**"Antonio Garcia"**

---
---
**Cesar Tolentino** *November 21, 2016 19:43*

Also remember that we don't do power of more than 15 ma.  I actually don't do more than 10 ma. But that's just me.  This is for the 40 watt k40 machine


---
**Antonio Garcia** *November 21, 2016 20:03*

Sure Cesar!, at the moment i didn´t go further  than 15 ma.


---
**Cesar Tolentino** *November 21, 2016 20:07*

Well then that's good because on mine I need 4 passes on a 5 mm ply.  Maybe it's the ply I'm using.  Congrats for the first successful cut


---
**Bill Keeter** *November 21, 2016 20:21*

I cut 3mm acrylic in one pass at 8mm/sec at 12mA. I'm in the same boat of slowing upgrading my K40. At the moment i've only redone the bed.. but making a wish list for all the other stuff.


---
**Antonio Garcia** *November 21, 2016 20:58*

**+Cesar Tolentino** What speed are you using? i cut acrylic 15mm/sec and plywood 12mm/sec, two passes both. The only issue with the plywood is that i haven´t installed the new head with air assistant yet, and in the second pass, i get flames, and it´s a little bit scary :), but once is finished the flames gone.

**+Bill Keeter** i will follow your upgrades :). 




---
**Stephane Buisson** *November 22, 2016 08:18*

air assit is next in line. Smoothie vs GBRL, smoothie is more powerfull and manage acceleration. on software side you have a choice of 3 with smoothie.  Laserweb, Visicut, Fusion 360.

[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)


---
**Jim Christiansen** *November 22, 2016 14:28*

I bought one last year and the cabinet is damaged in the same way but even worse.  It won't sit level and it also has one crushed microswitch.  Now that I have found this Google group perhaps I'll try to get the darn thing going!


---
**Cesar Tolentino** *November 22, 2016 15:20*

**+Antonio Garcia** I mostly cut 800mm/min or around 13mm/sec @ 8ma power using lasersaur board. Going slower burns the wood instead of cutting


---
**Ben T.** *November 28, 2016 06:52*

The corellaser plugin will not disappear completely after your first task, it will just minimize into the right bottom corner in the windows taskbar.


---
*Imported from [Google+](https://plus.google.com/+AntonioGarciadeSoria/posts/BEmkSmqKqXC) &mdash; content and formatting may not be reliable*
