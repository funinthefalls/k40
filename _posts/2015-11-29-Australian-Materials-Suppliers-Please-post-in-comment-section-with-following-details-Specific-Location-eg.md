---
layout: post
title: "Australian Materials Suppliers Please post in comment section with following details: Specific Location (e.g"
date: November 29, 2015 11:57
category: "Material suppliers"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Australian Materials Suppliers

Please post in comment section with following details:



Specific Location (e.g. Brisbane)

Specific Material (or types of materials supplied by this supplier)

Supplier Details (Name, Address, Phone, Website, Contact Person [if any])

![images/682d49edfc67da98e863d05bc63eeb35.png](https://gitlab.com/funinthefalls/k40/raw/master/images/682d49edfc67da98e863d05bc63eeb35.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 29, 2015 12:01*

Adelaide/Brisbane/Melbourne/Perth/Sydney



Plastic Materials (ABS/Acetal/ACM/Acrylic/Acrylic Mirror/Corrugate Flute/Foam PVC/HDPE Polyethylene/Nylon/Oil Filled Nylon/Polycarbonate-Lexan/Polypropylene/Prismatic Light Diffuser/PTFE-Teflon/PVC/UHMWPE Polyethylene)



Plastic Wholesale - Quote Central

[http://www.plasticwholesale.com.au/](http://www.plasticwholesale.com.au/)



More later.


---
**Stephane Buisson** *November 29, 2015 12:06*

excellent ! exactly in the spirit ;-))


---
**Anthony Coafield** *November 29, 2015 21:07*

This will be really handy. Thanks!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 30, 2015 00:05*

**+Anthony Coafield** Feel free to post any that you know of too Anthony :)


---
**Anthony Coafield** *November 30, 2015 03:49*

I will, I just don't know any yet. I've only messed around on some stuff I grabbed at Bunnings, and scrap wood I had lying around. Just waiting for my new laser to be delivered...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 09:21*

Another Gold Coast supplier of plastics:

[http://www.allstarplastics.com.au/](http://www.allstarplastics.com.au/)



A Gold Coast region supplier of Dry Moly Lube (Circle Spray):

[http://www.minehanagencies.com.au/lubricants.html#circlespray](http://www.minehanagencies.com.au/lubricants.html#circlespray)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Mx1nW6EUBnS) &mdash; content and formatting may not be reliable*
