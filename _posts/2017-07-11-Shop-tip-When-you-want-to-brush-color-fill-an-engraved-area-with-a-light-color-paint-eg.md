---
layout: post
title: "Shop tip - When you want to brush color fill an engraved area with a light color paint (e.g"
date: July 11, 2017 19:46
category: "Discussion"
author: "Ned Hill"
---
Shop tip - When you want to brush color fill an engraved area with a light color paint (e.g. white) the dark engraving residue can be a problem in that it can contaminate the paint and cause it to darken.  You can carefully wipe off the residue with a damp cloth, but a faster and better way is to apply 2 layers of masking to the piece before engraving.  After engraving simply pull the top layer of masking off and the residue is gone.  Very handy for large areas and you don't have to worry with accidentally wiping residue into the the engraving.  Works best with a medium to heavy tack masking first with a lighter tack mask on top.  #K40ShopTips



![images/4d83d2a9561452a65fa3d5ec0b63d73c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4d83d2a9561452a65fa3d5ec0b63d73c.jpeg)
![images/b2168441359a11edd089a8b6d8e1557f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2168441359a11edd089a8b6d8e1557f.jpeg)

**"Ned Hill"**

---
---
**Anthony Bolgar** *July 12, 2017 00:57*

Good idea Ned


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/M6SFUmiAT1X) &mdash; content and formatting may not be reliable*
