---
layout: post
title: "Hey guys, I'm posting here trying to determine if getting a K40 is the best option for me"
date: March 26, 2017 13:09
category: "Discussion"
author: "Frank Wiebenga"
---
Hey guys, I'm posting here trying to determine if getting a K40 is the best option for me. Right now I have a Shapeoko 3 and have one of those 3w solid state lasers that I just plug an Ethernet cable in and is controlled via pwm. Super easy to switch from CNC to laser. However painfully slow to engrave. I'd really like to build a CO2 system around my existing platform. Would it be cheaper to buy tube, mirrors, and cooling separate or purchase a K40 and strip?



PS. I've never used a CO2 laser, but assume like solid state specific nm glasses need to be worn. Unenclosed, probably not preferred. ﻿





**"Frank Wiebenga"**

---
---
**Don Kleinschnitz Jr.** *March 26, 2017 13:39*

I'm not a router user (yet) but I imagine that converting your Shapeoko to a quality 40W gas laser cutter is an involved undertaking: 



Thinking out loud of optical and mechanical things that come to mind:



1). Enclosures and interlocks over the entire work space.

2). Laser tube mounting at one end of the workspace

3). A precision and stable moving optical path (mirrors on moving gantry). From the lasers mount to the objective head movable in full X and Y without misalignment. That is unless you put the laser on the moving gantry (which has its own set of design considerations). [plus.google.com - FabCreator](https://plus.google.com/+Fabcreator)



4). Mount for the objective lens

5). Air assist system routed to head. Only needed for cutting.

6). Air evacuation system for work area

7). HV power supply mounting and cabling

8). Coolant circuit.


---
**Don Kleinschnitz Jr.** *March 26, 2017 13:59*

[openbuilds.com - LASER - PLASMA BUILDS &#x7c; OpenBuilds](http://www.openbuilds.com/?category=laser-plasma-builds&id=303)


---
**HalfNormal** *March 26, 2017 16:49*

If you go to Inventibles.com, home of X-carve/Shapeoko, someone has added a CO2 laser to their build. The cost was the same as a K40 and not as safe. 


---
**Kyle Kerr** *March 27, 2017 15:48*

I can imagine lack of safety, however, the two routers you mentioned have a massive increase in work area over a K40.


---
*Imported from [Google+](https://plus.google.com/110671713111366449013/posts/dGRxD299yJJ) &mdash; content and formatting may not be reliable*
