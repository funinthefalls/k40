---
layout: post
title: "Wood Inlay I have a good friend who does woodworking"
date: March 15, 2017 17:56
category: "Object produced with laser"
author: "HalfNormal"
---
Wood Inlay



I have a good friend who does woodworking. He supplies custom tables and other custom wood products to the local restaurants. We were talking and I told him I could do inlay with the laser. He was making a cutting board for a customer and asked if I could do the inlay. The inlay is about 3/16" birch and the base is maple. The finished product was sanded and oiled. The restaurant's name if SHIFT.



![images/6be90da2690438ef348de640cb2ae869.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6be90da2690438ef348de640cb2ae869.jpeg)
![images/f55940144afd2779df7addaa7749f1b0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f55940144afd2779df7addaa7749f1b0.jpeg)
![images/67449b3fbc8b509ca719d53045737d3e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/67449b3fbc8b509ca719d53045737d3e.jpeg)
![images/441e94a52d4b9005dcc64b7777847156.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/441e94a52d4b9005dcc64b7777847156.jpeg)

**"HalfNormal"**

---
---
**Steve Anken** *March 15, 2017 19:43*

Cool, never did laser inlay but I have done CNC. Very nice.




---
**Mark Brown** *March 15, 2017 20:45*

So you inlayed birch into a square block of walnut, then he put that in the maple cutting board?  Pretty cool, like Steve said, I hadn't considered using the laser to hollow out for inlays.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 15, 2017 23:45*

Great end job. Looks very nice.


---
**Ned Hill** *March 16, 2017 14:45*

Nice work. Reminds me that I have a box of veneer sheets stashed somewhere that I've been meaning to try some inlays with.


---
**Nigel Conroy** *March 17, 2017 13:38*

Seamless integration and doesn't look like a laser was used. Very nice


---
**BeenThere DoneThat** *March 18, 2017 09:09*

I bought some veneer to try that with. I didnt realize what I was buying, and I got the kind with the hot melt glue on the back. I can only imagine it might be a mess. Been too busy to mess with it. 



Did uou find you needed to scale the inlay to the base? Or was it just a simple matter of cutting both parts and gluing?



Has anyone tried this with thin veneer?


---
**HalfNormal** *March 18, 2017 16:37*

**+BeenThere DoneThat** With thicker pieces, I did not compensate for size due to the kerf (angle of the cut) In this case, sandpaper is your friend.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/c9CHMtkhcgT) &mdash; content and formatting may not be reliable*
