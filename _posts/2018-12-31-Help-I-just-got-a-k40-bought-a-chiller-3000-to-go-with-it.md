---
layout: post
title: "Help I just got a k40 bought a chiller 3000 to go with it"
date: December 31, 2018 20:19
category: "Hardware and Laser settings"
author: "Steve Britt"
---
Help I just got a k40 bought a chiller 3000 to go with it. Chiller hooked up but not putting water to laser. Also I push test fire on laser it doesn't fire. Any ideas what I doing wrong? Edit: Chiller Works





**"Steve Britt"**

---
---
**Don Kleinschnitz Jr.** *December 31, 2018 20:53*

It IS or IS NOT firing?

Do you have the laser enable SW engaged?

Do you have power switch or emergency switch engaged? 

Post picture of machine panel. 




---
**Steve Britt** *January 01, 2019 03:08*

Sorry I didn't check before I posted, it isn't firing. I have found it will fire with the test button on power supply but not on panel. Chiller is working I was wrong on that. The alignment looks like Stevie Wonder did it, it's way off.

![images/e7ca591d365d0b42bae1766fca0a4bf5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e7ca591d365d0b42bae1766fca0a4bf5.jpeg)


---
**Steve Britt** *January 01, 2019 03:08*

Alignment

![images/cf9741880b0aaeb50da6917ebae1a8f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cf9741880b0aaeb50da6917ebae1a8f5.jpeg)


---
**Steve Britt** *January 01, 2019 03:09*

Alignment 2

![images/93c41f438b42e6f1d85f88c21430a825.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/93c41f438b42e6f1d85f88c21430a825.jpeg)


---
**Steve Britt** *January 02, 2019 02:43*

Of course no help from seller yet ugh.... 


---
**Don Kleinschnitz Jr.** *January 02, 2019 14:29*

When you tried the test switch on the panel did you have the laser on/off switch engaged? 



If the machine has a water flow switch; water needs to be flowing and the switch working for the laser to fire from the panel.



Its a common problem for the water flow switch to be bad....


---
**Steve Britt** *January 06, 2019 04:40*

Laser switch is on emergency stop disengaged. Is there a fix for the flow sensor? 


---
**Don Kleinschnitz Jr.** *January 06, 2019 18:17*

**+Steve Britt** you can short it out to find out if it is what is wrong. If its bad you will need to replace it or repair it. Depends on the type it is.



Do you have a water flow switch. Picture?


---
**Steve Britt** *January 06, 2019 20:20*

Is this it? 

![images/aa85d783cef293920d7886a09eb29bb9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa85d783cef293920d7886a09eb29bb9.jpeg)


---
**Don Kleinschnitz Jr.** *January 07, 2019 15:42*

yes, pretty common for these to fail. You can short out the connections to test if its the culprit.


---
**Steve Britt** *January 10, 2019 03:03*

**+Don Kleinschnitz Jr.** Thank you I will try that. 


---
*Imported from [Google+](https://plus.google.com/107148229222111416873/posts/RxjpKGSzJY4) &mdash; content and formatting may not be reliable*
