---
layout: post
title: "Where would be the best place to buy a smoothie board and a middle man board?"
date: November 17, 2016 07:31
category: "Discussion"
author: "Charlie Thcakeray"
---
Where would be the best place to buy a smoothie board and a middle man board? (UK) 





**"Charlie Thcakeray"**

---
---
**Andy Shilling** *November 17, 2016 07:41*

I believe robotseed is the distributor for the uk, it's the only place I've found it.


---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2016 07:55*

You don't need that anymore. 

Check out what I've been working on: [plus.google.com - Some more shots from the K40 Conversion I did with my new Cohesion3D Mini…](https://plus.google.com/+RayKholodovsky/posts/UrNpWddMZok)


---
**Andy Shilling** *November 17, 2016 08:02*

Ray I've been thinking about converting mine as I have the old moshi board, will I just require the same psu I was going to get for the Smoothie board. And what sort of price are we looking at for Uk delivery please.


---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2016 08:10*

Hi Andy, I'm hand making these right now for testers but have a good quote back and want to pull the trigger on the first production run of 100 of these ASAP. If you're not in a huge hurry I would encourage you to stay tuned for pre-orders in the coming weeks. I am doing the laser bundle, which is the board + laser pwm cable + glcd adapter, right now for $100.  Uk Shipping is $20-35 for first class and priority mail respectively.  You will still need a microsd card and Pololu style drivers like the A4988, I am working on stocking these but that's a little bit farther out.


---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2016 08:11*

Oh, please check out Carl's awesome conversion documentation here: 

[https://www.dropbox.com/s/ngky9g3yh2gpa6r/C3D%20Installation.pdf?dl=0](https://www.dropbox.com/s/ngky9g3yh2gpa6r/C3D%20Installation.pdf?dl=0)


---
**jonjon f** *November 17, 2016 23:06*

Hi Ray ,where can I pre-order your this board?


---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2016 23:09*

Hi **+jonjon f** I will let you know once we are up for pre-order (working on quoting it with factory now), right now I just made some units for testers. I also replied to you on hangouts :)


---
**jonjon f** *November 17, 2016 23:34*

Thanks. I can't wait to do my first mod on my k40🤔.


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 02:10*

Hi **+Andy Shilling** **+jonjon f** and anyone else interested, the Cohesion3D Mini (and Laser Upgrade Bundle you'd need) are now available for pre-order at [cohesion3d.com](http://cohesion3d.com)  

We also have the ReMix board for sale if you have a higher power application in mind.


---
**Andy Shilling** *November 24, 2016 05:18*

**+Ray Kholodovsky**​ thank you, what is the price for shipping to the uk please. Once you get back to me I'll  be putting my order in. ( Happy days) 


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 05:38*

**+Andy Shilling** you should be able to put in your address during checkout and get an exact shipping quote.  I checked, it is around $15 for first class and $35 for priority in a small flat rate box. 


---
**Andy Shilling** *November 24, 2016 05:41*

**+Ray Kholodovsky**​ On it now, thank you.


---
*Imported from [Google+](https://plus.google.com/109895480370746543865/posts/a2iP1aiGefx) &mdash; content and formatting may not be reliable*
