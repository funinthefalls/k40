---
layout: post
title: "2 Questions for the group, I will post one here and then do another post"
date: August 14, 2016 19:09
category: "Material suppliers"
author: "Terry Taylor"
---
2 Questions for the group, I will post one here and then do another post. I am looking for anodized aluminum plaques (like the one below, that are given out at this like car rallyes). I found some at LaserBits, but they are larger and with round corners. The ones that I am looking for would be about 1x3"

![images/3a78694ec5e1985ceac4825cf8deb347.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/3a78694ec5e1985ceac4825cf8deb347.gif)



**"Terry Taylor"**

---
---
**greg greene** *August 14, 2016 19:22*

Check ebay for laserable business cards


---
**Scott Marshall** *August 14, 2016 19:23*

You're in a sister business.

The custom enclosed trailer (race cars etc) guys buy the full sheets of treated aluminum and often sell the drops. You can get the baked on paint or anodize. Custom cycle shops often have smaller pieces of anodize for sale too. If you're doing a lot, it would pay to buy a full sheet direct and have it sheared to size for your signs. (the 24ga and thinner are often in rolls)

Hope it helps



Scott


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/jXLYVCneeY9) &mdash; content and formatting may not be reliable*
