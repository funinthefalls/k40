---
layout: post
title: "Anyone available to assist me with a few questions involving living hinges?"
date: August 31, 2017 03:15
category: "Discussion"
author: "Alex Krause"
---
Anyone available to assist me with a few questions involving living hinges?





**"Alex Krause"**

---
---
**Ashley M. Kirchner [Norym]** *August 31, 2017 05:12*

What about living hinges?


---
**Alex Krause** *August 31, 2017 05:17*

I see several different styles In images would like to know more about the bend radius achievable while maintaining material integrity. Also looking for a good generator of such hinges to cut down on manual design time


---
**Ashley M. Kirchner [Norym]** *August 31, 2017 06:46*

I ended up doing some test pieces myself, using just the simple, straight lines design. I created test pieces starting with 3mm gaps, decreasing till I got to 1.5mm. The closer the slits, the more flexibility you have. With the attached image, remember that you're looking at the edge, so the gaps appear larger, but there's another slit cut in between the ones you can see.

![images/04bcce0e4e939e42aa72d507f5dc4da0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/04bcce0e4e939e42aa72d507f5dc4da0.jpeg)


---
**Ashley M. Kirchner [Norym]** *August 31, 2017 06:47*

I haven't seen a generator per se, and I just copy my templates from one design to the next. Ariel recently posted this link which has some flex design possibilities. I have not tried it (have not need to) but it may be a good start: [festi.info - Boxes.py](http://www.festi.info/boxes.py/)


---
**Ashley M. Kirchner [Norym]** *August 31, 2017 06:49*

Here's another look of the flexibility and how the slits are cut ...

![images/0e35b6472c74d1447e0c0f6a7b66a04c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0e35b6472c74d1447e0c0f6a7b66a04c.jpeg)


---
**Stephane Buisson** *August 31, 2017 08:04*

**+Alex Krause** it's quite a lot of things on this community about living hindges. check the links in posts around march 2016 (search living hinge in this community.



Orbrary have free ready made download living hindge designs.




---
**HalfNormal** *August 31, 2017 16:17*

Found this with a quick search;

[https://www.ponoko.com/blog/how-to-make/how-to-design-a-living-hinge/](https://www.ponoko.com/blog/how-to-make/how-to-design-a-living-hinge/)


---
**Stephane Buisson** *August 31, 2017 16:52*

**+Alex Krause**

dig up this site :

[deferredprocrastination.co.uk - Categories &#x7c; Deferred Procrastination](http://www.deferredprocrastination.co.uk/blog/category/)

for " Lattice Living Hinges" or simply "Lattice Hinge"



[http://www.deferredprocrastination.co.uk/blog/2011/lattice-hinge-test-results/](http://www.deferredprocrastination.co.uk/blog/2011/lattice-hinge-test-results/)



and more ...










---
**Alex Krause** *August 31, 2017 16:59*

Thank you everyone for your help it is much appreciated I now have a better understanding as to the behavior if of different designs... I'm going to test a few out and see if I picked up what I needed and go from there


---
**Periwinkle Newyoutube** *September 06, 2017 13:48*

I added an extension to Inkscape that creates a living hinge easily. Works well.


---
**HalfNormal** *September 06, 2017 13:50*

**+Periwinkle Newyoutube**​ link?


---
**Periwinkle Newyoutube** *September 07, 2017 15:46*

easy to find with google, here is some info [wiki.lvl1.org - Inkscape Extension to Render a Living Hinge - LVL1](http://wiki.lvl1.org/Inkscape_Extension_to_Render_a_Living_Hinge)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/NY382TcV33H) &mdash; content and formatting may not be reliable*
