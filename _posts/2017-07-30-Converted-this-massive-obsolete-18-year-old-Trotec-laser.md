---
layout: post
title: "Converted this massive obsolete 18 year old Trotec laser"
date: July 30, 2017 08:17
category: "Modification"
author: "Paul de Groot"
---
Converted this massive obsolete 18 year old Trotec laser. Amazing how technology has changed in 18 years...see the photo of the old motherboard and the new controller. I am amazed that the laser tube still works after all that time. Got it from wBay for $250 plus another $250 for postage. It's massively heavy. Hopefully this convinces some more people for my Gerbil controller @ kickstarter.



![images/f699c9de3fca1bd6aabe518c1941c3fe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f699c9de3fca1bd6aabe518c1941c3fe.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-4R7hEfkCWCg/WX2WAXcN2OI/AAAAAAAAHUY/Zd9sgpeASScUL_LZTlXmFvmmRSqUMEPRQCJoC/s0/gplus-1352256110.mp4**
![images/810d62bdbc0168843f63c0ef28356ae2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/810d62bdbc0168843f63c0ef28356ae2.jpeg)

**"Paul de Groot"**

---
---
**Paul de Groot** *July 30, 2017 08:23*


{% include youtubePlayer.html id="3FY6ghpwyag" %}
[https://youtu.be/3FY6ghpwyag](https://youtu.be/3FY6ghpwyag)


---
**Anthony Bolgar** *July 30, 2017 08:27*

I think one link would have been enough ;)




---
**Paul de Groot** *July 30, 2017 09:56*

How did that happen? Strange


---
**Jim Fong** *July 30, 2017 11:17*

Wow nice score!  Standard CO2 tube or RF type?  Lucky it was stepper and not servo motor, makes it easier to convert.   I had a chance to buy a old Epilog once but passed when I found it was servo driven. To costly to convert.  


---
**Paul de Groot** *July 31, 2017 23:26*

RF type but it has a buildin driver so can use my pwm output directly. It did come with dc motors with encoders so had to replace these with steppers. Was easy to do with some minor adapter plates and bushes.


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/8vrF9ha97Xx) &mdash; content and formatting may not be reliable*
