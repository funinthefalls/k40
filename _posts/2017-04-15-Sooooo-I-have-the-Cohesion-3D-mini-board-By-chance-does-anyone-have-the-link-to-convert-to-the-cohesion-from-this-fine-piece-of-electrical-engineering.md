---
layout: post
title: "Sooooo.... I have the Cohesion 3D mini board, By chance does anyone have the link to convert to the cohesion from this fine piece of electrical engineering"
date: April 15, 2017 01:20
category: "Smoothieboard Modification"
author: "Blake Benefield"
---
Sooooo.... I have the Cohesion 3D mini board, By chance does anyone have the link to convert to the cohesion from this fine piece of electrical engineering.

![images/1360e69e406d23fe467caa017385ae14.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1360e69e406d23fe467caa017385ae14.jpeg)



**"Blake Benefield"**

---
---
**Ariel Yahni (UniKpty)** *April 15, 2017 01:25*

Info should be on the Cohesion community or here [https://cohesion3d.freshdesk.com/support/home](https://cohesion3d.freshdesk.com/support/home)


---
**Blake Benefield** *April 15, 2017 03:44*

I've found info on the nano boards, I haven't been able to find a thread where someone has done the moshi conversion.


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 03:53*

This is a less common board, we'll have to finagle some items around. Start with the ribbon cable. See if you can identify the motors, endstops, and individual items in the power connector (that's the big one, probably 6 pins) 



There are a few instances earlier, search the Cohesion3D group for moshi, moshiboard, etc... or just scroll most of the way to the bottom. [plus.google.com - C3d Mini – Setup My laser machine has 2 Model numbers 1st 1 is KH40W from ven...](https://plus.google.com/u/0/117786858532335568822/posts/TfiJPuYAdAn)


---
**Blake Benefield** *April 15, 2017 04:07*

Power is at least identified for the most part.  5v on the left.  24v on the right.



It would appear that the ribbon controls the X axis (L/R) while the Y axis has a stepper motor plugged straight to the board (CN4).

![images/f0330d8777a5b3105637ffe6c8a94cde.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f0330d8777a5b3105637ffe6c8a94cde.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 04:12*

You'll find similarities to this, check the comments thread:



[plus.google.com - C3d Mini – Setup My laser machine has 2 Model numbers 1st 1 is KH40W from ven...](https://plus.google.com/u/0/117786858532335568822/posts/TfiJPuYAdAn)



For the power cable, there are 2 options. Rekey the pins in here to keep the 6 pin housing. 

Or cut the board end off. 24v, gnd, and L all have screw terminals for you to put the wires in. L goes to 2.5 - bed which is the 4th from the left bottom screw terminal. 

The 24v and gnd are top left. 



Get acquainted with the pinout diagram for Mini on the docs site. 


---
**Andy Shilling** *April 15, 2017 07:24*

Yes ribbon is fit both endstops and x axis and y has the plug to go straight to the board. You'll need to swap +24v and G around if i remember correctly as they are reversed on the C3D board. Also you will need to cut that plug down by one pin to allow it to fit the power pins on the C3D.


---
**HP Persson** *April 15, 2017 16:35*

Can u show a pic. of the PSU connected to the Moshi board? would help with figuring out what goes where, there is a bunch of them for moshi´s.


---
**Blake Benefield** *April 15, 2017 17:24*

These are the 2 red wires going to Moshi. The 2 pulled to the left and up.  According to Moshi I need to swap them in the power connector as the C3D is Laser, Open, Ground, 24v... The Moshi is G,L,O,O,24V,G.



I've reconfigured the connection accordingly.  The board powers up but does not home the laser.

![images/3fa2f7f704a0c38258aeea2aed85bf28.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3fa2f7f704a0c38258aeea2aed85bf28.jpeg)


---
**Blake Benefield** *April 15, 2017 17:25*

This is the power from the 24v.  

![images/46edd162c7faaabdd6ca10ddcea7baa4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/46edd162c7faaabdd6ca10ddcea7baa4.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 17:26*

The board does not home the laser on boot. 


---
**Blake Benefield** *April 15, 2017 17:28*

Sweet.  So new question.  Is the laser supposed to fire when the test button is pressed?  It lights up a pretty lavender color but nothing comes out of the end.


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 17:29*

Yeah... did it work "properly" before you swapped the board? What is your pot set to, try a higher value. In general the answer for that is your mirrors are misaligned. 


---
**Blake Benefield** *April 15, 2017 17:33*

No. I did what it is doing now... Only it also homed when power was turned on.  



The laser was at a friends completely covered in dust. And dirt.  He had no clue where the usb dongle was.



Enter the Cohesion on the road to restoring it to working order..


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 17:47*

Ok. So unrelated to the board. Time to get LaserWeb up and running and jog the machine. 


---
**Blake Benefield** *April 15, 2017 18:03*

Yeah if the board itself doesn't home on power then I think so.  I'm just trying to figure out the laser thing now.



But yes... I believe this unrelated to the C3D as this is what it did prior to the swap as well.



I'm downloading laserweb now.





![images/b546571dbb1fc05e19d0b25c39feeda8](https://gitlab.com/funinthefalls/k40/raw/master/images/b546571dbb1fc05e19d0b25c39feeda8)


---
**Robi Akerley-McKee** *April 17, 2017 06:24*

**+Blake Benefield** make sure your computer supports webgl, mine didn't.  I had to switch computers for laserweb


---
*Imported from [Google+](https://plus.google.com/100746839086487970332/posts/GCU4SUrEogN) &mdash; content and formatting may not be reliable*
