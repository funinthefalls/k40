---
layout: post
title: "My k40 was arcing badly, i re-did both connections to the tube, sealing them with silicone"
date: December 30, 2015 16:11
category: "Discussion"
author: "george ellison"
---
My k40 was arcing badly, i re-did both connections to the tube, sealing them with silicone. No arcing this time but a quiet noise, and no reading on the meter and tube not firing? Any suggestions, thanks. 
{% include youtubePlayer.html id="S58KUCt-x1M" %}
[https://www.youtube.com/watch?v=S58KUCt-x1M](https://www.youtube.com/watch?v=S58KUCt-x1M)





**"george ellison"**

---
---
**Scott Marshall** *December 30, 2015 16:25*

It sounds like the power supply is arcing internally.



 Probably either a bad power supply or bad laser.



 I'm betting the laser is bad - which was causing the arcing in the 1st place  - now that you have insulated the tube, the electricity is finding the next closest path, which is to arc internally through the power supply. If the tube was conducting, IT would be the easiest path for the high voltage.



I hope I'm wrong, but that's my long distance, youtube  diagnosis. 



Scott


---
**Timothy “Mike” McGuire** *December 30, 2015 23:58*

Scott I do believe you're right the voltage has to go somewhere path of least resistance I wonder if both sides negative and positive of the tube have been re-silicone doesn't look promising unfortunately


---
**Scott Marshall** *December 31, 2015 00:07*

The ground won't generally arc, since it's at the same potential  as the frame and control ground.



Sorry for the bad news. Hope you get it fixed with a minimum of expense. 




---
**george ellison** *December 31, 2015 09:35*

Thanks to all, i kind of knew it may be a new tube i had to buy i was just trying to find out if it was definitely a new tube. I didn't want to buy one, install it then the same happens. I may have got air into the tube before which caused it to fail perhaps? Many thanks


---
**Scott Marshall** *December 31, 2015 14:25*

From what I hear on these, they usually fail within a month or so of being put into service, or last a long time. I don not know if it's a gas leak that causes them to stop working, but it seems to be a logical assumption.  Holding my breath on mine.

How old was yours George?


---
**george ellison** *December 31, 2015 16:06*

Had mine around 9 months. The tube was fine but i think air may have got through the tube from the water pump. Do you think this would have caused it to fail?


---
**Scott Marshall** *January 01, 2016 01:09*

Probably not. It's where the glass is fused to form the laser cavity seal that would cause the CO2 mix to leak. They form micro cracks from heat cycling if they aren't properly annealed to relieve internal stresses. 



You may  have thermal shocked it which could crack the tube internally and cause a gas leak. If your water stopped flowing long enough for it to get pretty hot, then came back on, the cold water on the hot glass can easily cause failure. A bubble or 2 won't do it. It would have to be a minute or more with no water in the tube to let it get hot enough to crack.



These lasers lack any flow protection or warning. I saw a post where someone was concerned if he put a flow switch inline it would restrict the flow, and to some degree, he's correct. I'm a controls engineer and when I get a few minutes, I'll post a cheap easy way to put a flow alarm/shutdown circuit on it which will not cause any flow problems.(there's a cheap differential pressure switch that works nice, or an optical solution)



I used to do contract work for Corning Glass Works and learned a lot building CRT presses where they make/made the old style picture tubes. The glass processing is similar to the laser tubes. They have to spend quite a long time in an annealing furnace after fabrication or will break on cooling or carry internal stresses that will cause later failure. Even bottles do. 

The Chinese manufacturers sometimes 'cheat' and don't anneal them long enough. It's all about fuel cost and time.



Edit - PS

Just thought of this, If you want to see an extreme example of this stress buildup, watch this. It's worth a look....


{% include youtubePlayer.html id="xe-f4gokRBs" %}
[https://www.youtube.com/watch?v=xe-f4gokRBs](https://www.youtube.com/watch?v=xe-f4gokRBs)


---
**george ellison** *January 01, 2016 09:46*

Thanks very much, now you come to say it i remember i did forget to turn on the pump a few times, then turn it on after a minute of so. That could be the issue. I will look for cracks and get back to you, thanks again.


---
**Scott Marshall** *January 01, 2016 10:04*

That could do it. The cracks are probably too small to see. Look where the mirrors are fused on, and the power connections. You might spot them easier in the dark with a blue spectrum LED/Ultraviolet light. The cracks and stresses often fluoresce. It's the power connections that are tricky, it takes a very special alloy and process to run the metal through the glass and get the coefficient of expansion of the metal the same as the glass. If you're off by a fraction, temperature cycling cracks it and it leaks very slowly. Could be a month before it fails, that's why I think so many seem to fail 2 to 6 weeks in service. It's been a lightbulb issue for 100yrs. They just get it right, and somebody develops LEDS. Science can be cruel.

A lot of expensive industrial/scientific instruments like high power x-ray tubes, transmitter tubes and ESMs are left on forever or have a heater built into the glass tube to keep the tubes at operating temp.



Back to the $400 laser.... a tad lower on the engineering ladder;

They should at least have a main power switch that controls the pump so it comes on when you power on the laser power supply.

 Minimum is an understatement for this setup. it's really a kit without instructions. MIne needs a ton of parts because they threw the fan and water pump in the cutting compartment loose and it bashed everything to death.


---
**george ellison** *January 02, 2016 11:40*

Thanks, i agree, it could have been designed a lot better but you get what you pay for i suppose. Will get back to you when i get it sorted, many thanks for the info


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/SLWz9z2K2ye) &mdash; content and formatting may not be reliable*
