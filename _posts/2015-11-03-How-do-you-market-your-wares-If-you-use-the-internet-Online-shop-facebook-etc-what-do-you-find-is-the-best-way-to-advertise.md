---
layout: post
title: "How do you market your wares? If you use the internet, Online shop, facebook etc, what do you find is the best way to advertise?"
date: November 03, 2015 16:12
category: "Discussion"
author: "Peter"
---
How do you market your wares?

If you use the internet, Online shop, facebook etc, what do you find is the best way to advertise? If you do shop or market stalls, how do you present your work?





**"Peter"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 03, 2015 17:33*

Personally I don't market much. Most of my products sell through word of mouth, or someone actually seeing a product of mine (i.e. one I made for myself) and asking where can they get one. Also, I occasionally give free products to people I know (or seriously discounted) as a way to spread awareness to people they may know/associate with. However, I have to admit I don't get a stable amount of work (yet).



From others I know that run home-businesses, most of their marketing/advertisement is through social networking (Facebook & Instagram mainly) and they sell either through Etsy or personal websites.



My opinion in regards to what is the best method of advertising your wares is to choose that of a combined method. Some amount of online/social media advertising would be wise in this day & age, whilst retaining some of the older methods of advertising would also be beneficial (e.g. leaflets, business cards, word of mouth, radio/television/newspaper/magazine advertising). I would also consider methods like markets/stalls to be a great way to showcase your product, where people can see and touch it. Something about being able to physically see & touch a product seriously appeals to me (and I assume most potential customers, hence retail outlets still exist in this day & age of internet shopping ease).



To sum it up, I think advertising/marketing for your wares should be done as your budget permits, using a variety of methods.


---
*Imported from [Google+](https://plus.google.com/+PeterJones79/posts/B5xGDkv3s2X) &mdash; content and formatting may not be reliable*
