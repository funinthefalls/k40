---
layout: post
title: "So I just got my k40 in the mail, and it's unboxed and sitting on my workbench"
date: November 04, 2015 17:56
category: "Discussion"
author: "Nathaniel Swartz"
---
So I just got my k40 in the mail, and it's unboxed and sitting on my workbench. I've been reading posts on here, a similar facebook group, and watching unboxing videos since I put in the order.  I know to make sure my water is cold, but not too cold, check for any air bubbles in the tube, aligns and clean the mirrors, make sure the fan and pump are running, check all the electrical connections, make sure my ground is good..... Am I missing anything?  Any gotchas I should know about?





**"Nathaniel Swartz"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 04, 2015 17:58*

Check for mirror alignment (I guess that is after you've got it up & running).

(edit) + link here [http://dck40.blogspot.com.au/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html](http://dck40.blogspot.com.au/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html)


---
**Stephane Buisson** *November 04, 2015 18:20*

relax, put your goggles on , take a good breath, welcome among us and enjoy ;-))



PS: identify your components, electronic board, PSU, softwares, check wiring, take photos. try factory solution to have a reference comparison point. after few attempt you would like to think about how to make it better. (read my post next saturday)


---
**Nathaniel Swartz** *November 04, 2015 18:54*

So the water inlet and outlet on the tube should be facing up so that bubbles are easier to get out, right?


---
**Anthony Bolgar** *November 04, 2015 21:20*

Also, do not set the power level too high,the lower amperage you can run at = the longer the tube will last.


---
**Nathaniel Swartz** *November 04, 2015 23:38*

It test fires, and I haven't lit anything on fire, so I'm good.  Thanks for the tips!


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/MTFYtGZepaB) &mdash; content and formatting may not be reliable*
