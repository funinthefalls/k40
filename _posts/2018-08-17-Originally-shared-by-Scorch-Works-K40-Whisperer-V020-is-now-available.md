---
layout: post
title: "Originally shared by Scorch Works K40 Whisperer V0.20 is now available"
date: August 17, 2018 00:33
category: "Software"
author: "Scorch Works"
---
<b>Originally shared by Scorch Works</b>



K40 Whisperer V0.20 is now available.

[http://www.scorchworks.com/K40whisperer/k40whisperer.html](http://www.scorchworks.com/K40whisperer/k40whisperer.html)…



Highlights of the fixes/ new features:

- Fixed bug that resulted in 45 degree angles being cut too fast

- Added keyboard shortcuts for some main window functions 

- Fixed divide by zero error when running g-code

- Added support for hidden layers in DXF files 

- Improved interpretation of some DXF files 

- Added ability to save and run EGV files. Can run EGV files made by LaserDRW and generate files that can be run from LaserDRW. 

- Changed behavior after number of timeouts is exceeded.

- Added logic to wait for the laser to finish running a job before the interface becomes active again.

![images/f340e5c173baaa2fc11e79251d40100a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f340e5c173baaa2fc11e79251d40100a.png)



**"Scorch Works"**

---
---
**Jean-Phi Clerc** *August 17, 2018 05:21*

thank you for this update of your excellent software


---
**Djinn per se** *August 27, 2018 15:04*

is there a  x64 driver? I have installed using the tutorial but just works x32 version and i feel  that take much time to process


---
**Scorch Works** *August 27, 2018 18:45*

**+Djinn per se** Are you saying the 64 bit executable does not work on your computer?  If so try v0.21 which is on the web page now.


---
*Imported from [Google+](https://plus.google.com/+ScorchWorks/posts/ZaNyguUuBqB) &mdash; content and formatting may not be reliable*
