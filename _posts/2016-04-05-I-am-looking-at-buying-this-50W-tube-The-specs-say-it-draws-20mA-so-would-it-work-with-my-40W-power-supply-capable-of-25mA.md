---
layout: post
title: "I am looking at buying this 50W tube The specs say it draws 20mA, so would it work with my 40W power supply (capable of 25mA)?"
date: April 05, 2016 03:57
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
I am looking at buying this 50W tube

[http://www.ebay.ca/itm/50w-CO2-Sealed-Laser-Engraver-Tube-Water-Cool-85CM-Shenhui-DC-G350-engraver-/160879770393?hash=item25752e7b19](http://www.ebay.ca/itm/50w-CO2-Sealed-Laser-Engraver-Tube-Water-Cool-85CM-Shenhui-DC-G350-engraver-/160879770393?hash=item25752e7b19)

The specs say it draws 20mA, so would it work with my 40W power supply (capable of 25mA)?





**"Anthony Bolgar"**

---
---
**Scott Thorne** *April 05, 2016 11:54*

I'm not sure that the power supply that comes in the k40 will put out 25 mA's ....I bough a bigger power supply from light objects is 40 to 60 watts....it's quite a bit bigger than the supposedly 50 watt that came in my machine....it's max is 22 mA's.


---
**Scott Thorne** *April 05, 2016 11:56*

Also...do some research...I'm finding out that a 50 watt tube is really 1000 mm long not 850 like the one that came in my machine


---
**Anthony Bolgar** *April 05, 2016 13:21*

I know that it is not a true 50W, but at least it is around 40-45W, the tubes that come in the K40 range from 28W-35W. This way I would at least have a true 40W machine.


---
**Scott Thorne** *April 05, 2016 13:36*

**+Anthony Bolgar**...I would say go for it...if you have to have another power supply then get it later....but you will gain a good bit with the one you have. 


---
**Anthony Bolgar** *April 05, 2016 13:40*

I will actually need a PSU as this one will probably go into the Redsail LE400 that I have gutted. The build quality of the LE400 is miles ahead of a K40, but the electronics were from the stone age, the worst was thge HV section of the PSU was in open air in the electronics compartment.


---
**Scott Thorne** *April 05, 2016 13:52*

Don't make the mistake of purchasing the one I got...it's huge...it works great but it's massive compared to the one that came out of my 50 watt machine.


---
**Scott Thorne** *April 05, 2016 13:57*

One other thing...after it was all said and done...the problem that I'm having is too much power for engraving ....lol....I'm having to turn the power down so low that it will barely fire at that low setting...at 10% power it won't fire at all...at 12% it fires....but if I turn the speed down below 200mm/s it engraves too deep on some things so I'm having to run it at 300 and above. 


---
**Jon Bruno** *May 18, 2016 17:13*

Make sure the voltage requirements are in line with your PSU's output as well. it's not always about the mA's

some of the larger tubes have different trigger and operating voltages.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/WFKC6GgBNoL) &mdash; content and formatting may not be reliable*
