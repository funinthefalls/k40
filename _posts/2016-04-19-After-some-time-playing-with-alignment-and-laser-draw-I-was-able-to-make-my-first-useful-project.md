---
layout: post
title: "After some time playing with alignment and laser draw I was able to make my first useful project"
date: April 19, 2016 01:03
category: "Hardware and Laser settings"
author: "andy creighton"
---
After some time playing with alignment and laser draw I was able to make my first useful project. 

![images/0f18e41ef4200b004e5efb319232e026.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f18e41ef4200b004e5efb319232e026.jpeg)



**"andy creighton"**

---
---
**ED Carty** *April 19, 2016 01:18*

Very Nice project


---
**andy creighton** *April 19, 2016 01:23*

I was really surprised.  The k40 cut 1/4" Luan at 10mm/s, two passes, and 12ma power. 

My $3500 40w at work would struggle with this. 


---
**Jim Hatch** *April 19, 2016 03:14*

Clever. Looks nice.


---
**Anthony Bolgar** *April 19, 2016 04:59*

Upgrading the optics to quality mirrors and lens will also amaze you at what the little K40 can do with some basic modifications. If you are going to keep the K40 for any length of time I would suggest doing the optics upgrade, as well as changing the stock controller out for a Smoothieboard or Ramps/Arduino controller combo. It will make creating items much quicker and easier, with a higher level of quality. Cost to do the 2 upgrades would run between $150 - $250 which still keeps the total investment in the laser to a reasonable level. And welcome to the world of lasers, if you are anything like me, you will find it to be a fun addiction.


---
**Jim Hatch** *April 19, 2016 12:32*

**+Anthony Bolgar**​ I agree wholeheartedly. I can do nearly everything on the K40 in the garage that I can do on the 60W one in our local Makerspace. The difference is size of bed and speed. Lower power means slower speeds and the bed size means it's smaller projects or I need to break it into smaller component pieces. But I can cut 1/4" and can engrave everything I've been able to fit inside.


---
**Anthony Bolgar** *April 19, 2016 13:06*

I just picked up an older Redsail LE400 that I have been upgrading to a Smoothieboard, it had an adjustable Z table already, bed is 460mm X 280mm. Nice thing is that the laser tube area is big enough that I can replace the 35W tube with a true 50W tube without having to mod the case at all. I bought a 10" Windows tablet that I will be using as a touchscreen interface for the laser. I will probably end up using the Redsail for cutting mostly and use the K40 mostly for engraving.


---
**andy creighton** *April 24, 2016 00:55*

Believe it or not,  I like the laser draw software.   I design in autocad and it imports the dxf really well.    

The optics will have to happen, though. 


---
*Imported from [Google+](https://plus.google.com/111039030319838939279/posts/MoLebiLHmg4) &mdash; content and formatting may not be reliable*
