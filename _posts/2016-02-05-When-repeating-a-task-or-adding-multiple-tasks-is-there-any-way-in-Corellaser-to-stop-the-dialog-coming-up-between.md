---
layout: post
title: "When repeating a task or adding multiple tasks is there any way in Corellaser to stop the dialog coming up between?"
date: February 05, 2016 03:56
category: "Software"
author: "I Laser"
---
When repeating a task or adding multiple tasks is there any way in Corellaser to stop the dialog coming up between?



I'd rather the machine just did its thing without needing my intervention...



(I did search for this, before posting, just couldn't find anything)





**"I Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 05, 2016 05:12*

I had the same issue & I found that setting it to "All Layers" in the main dialog (where you change things like ReferX/Y) then rather than clicking "Starting" option, I add each thing as task. E.g. I added engrave of one shape, then added cut of one shape, then added cut of another shape, then I clicked start on the tray icon (near clock). It stopped it bringing up the dialog. I hope that makes sense (I don't have the software in front of me currently).


---
**I Laser** *February 05, 2016 06:28*

Excellent, thanks very much. I'd tried the all layers but with repeat not adding tasks.


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/ahJBXCcc2BV) &mdash; content and formatting may not be reliable*
