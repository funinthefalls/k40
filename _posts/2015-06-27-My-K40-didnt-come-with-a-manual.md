---
layout: post
title: "My K40 didn't come with a manual"
date: June 27, 2015 19:05
category: "Discussion"
author: "quillford"
---
My K40 didn't come with a manual. Is there one available online? Also, are there any precautions or things I should check before turning it on?





**"quillford"**

---
---
**Jim Root** *June 27, 2015 19:41*

I believe mine came with a very rudimentary manual on the CD. It was in some kind of odd format that took a while to get opened and of course once opened it wasn't worth much. Got most of my questions answered here.


---
*Imported from [Google+](https://plus.google.com/115654175984463400645/posts/77qjNqWDfBf) &mdash; content and formatting may not be reliable*
