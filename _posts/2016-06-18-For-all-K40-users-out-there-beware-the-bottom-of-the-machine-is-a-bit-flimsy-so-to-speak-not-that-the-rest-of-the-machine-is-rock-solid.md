---
layout: post
title: "For all K40 users out there beware the bottom of the machine is a bit flimsy so to speak, not that the rest of the machine is rock solid"
date: June 18, 2016 16:10
category: "Modification"
author: "Mircea Russu"
---
For all K40 users out there beware the bottom of the machine is a bit flimsy so to speak, not that the rest of the machine is rock solid.

Mine came with the posts for the aluminium bed bent in every possible direction just to match the holes on the bed. After removing the original aluminium bed the bottom of the machine started flexing up/down and moving the x/y gantry by a few mm which in turn threw away any attempt to get the mirrors aligned. Just decide which way the gantry seems more level and add either something heavy on the bottom to keep it flexed down or add something underneath the machine to keep the bottom flexed up, like another rubber foot. 

I decided to add a piece of metal inside to keep it flexed down and also to close the round hole through which the laser can burn the table. After aligning and repositioning the mirrors by shimming I get a constant dot at the center of the lens.

Now it's time to make an adjustable pin bed.





**"Mircea Russu"**

---
---
**Jim Hatch** *June 18, 2016 16:20*

**+Mircea Russu**​ That might be a mfg defect. Most all of the K40s I've seen have 2 square tubes running across the bottom about 1/3rd of the way from the front and the back and spanning the width. That gives the bottom plenty of stiffness. I think it's about 10mm square tubing. If you don't have that then you could add a couple of lengths of it with a few nuts & bolts. Also complain to the seller and ask to return it so they make you a settlement offer (not worth returning it but they may pay for your new stiffening rails).


---
**Gee Willikers** *June 18, 2016 17:21*

Mine has a flat sheet bottom and had bent posts and bed. I removed the bed and posts, then screwed the machine to a peice of 3/4" (18mm) MDF.


---
**Mircea Russu** *June 18, 2016 17:46*

Thank you. that's what I intend to do also, screw it to the table.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 18, 2016 18:19*

**+Jim Hatch** **+Mircea Russu** Mine doesn't have the square tubes that Jim mentions. It's also very flimsy on the bottom & warps, although I've not had any issues that I noticed so far. I had severely bent posts to begin with, but I just pulled them straight haha.


---
**Brandon Smith** *June 18, 2016 19:45*

I just put a board under mine when I noticed the same thing. Works perfect, and its level now.


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/VjBZjiw94AP) &mdash; content and formatting may not be reliable*
