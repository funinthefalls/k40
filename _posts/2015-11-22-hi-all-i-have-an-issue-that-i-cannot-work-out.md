---
layout: post
title: "hi all i have an issue that i cannot work out!"
date: November 22, 2015 20:37
category: "Discussion"
author: "Martin Byford"
---
hi all

i have an issue that i cannot work out!

Hopefully someone can help

I have the k40 laser engrave.

The laser is working fine when i hit the test button. however when i draw an image using coreldraw or laserdrw and try to sart engraving the laser head moves but the laser does not fire.

i cannot figure out why it wont fire?

Any one have any ideas??

Thank very much





**"Martin Byford"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 22, 2015 23:25*

Possibly you need to change your mainboard model in the software setup. You will find the mainboard model # on the actual mainboard inside the K40's right panel (beneath the buttons/dials). And in the software you will need to check the device setup & make sure the model # is set to what your mainboard is. If not that, I have no idea.


---
*Imported from [Google+](https://plus.google.com/109380924123528435976/posts/jmuDmFeVkyN) &mdash; content and formatting may not be reliable*
