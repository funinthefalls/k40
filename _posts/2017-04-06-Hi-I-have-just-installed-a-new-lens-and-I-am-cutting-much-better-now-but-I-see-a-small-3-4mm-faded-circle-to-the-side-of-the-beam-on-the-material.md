---
layout: post
title: "Hi I have just installed a new lens and I am cutting much better now but I see a small 3/4mm faded circle to the side of the beam on the material"
date: April 06, 2017 19:24
category: "Discussion"
author: "george ellison"
---
Hi I have just installed a new lens and I am cutting much better now but I see a small 3/4mm faded circle to the side of the beam on the material. Is this normal or is it because my beam isn't fully aligned, thanks. The second faded circle isn't a 2nd cut line like you sometimes get. 





**"george ellison"**

---
---
**Don Kleinschnitz Jr.** *April 06, 2017 19:38*

A reflection in the air assist nozzle?


---
**Ned Hill** *April 06, 2017 19:54*

Not normal.  Don has a good suggestion.  Definitely check your beam alignment.  If that doesn't fix it then it's likely a flaw in the lens.


---
**Don Kleinschnitz Jr.** *April 06, 2017 19:54*

Do you have air assist and what kind of nozzle?




---
**george ellison** *April 08, 2017 14:22*

I don't have an air assist, The alignment seems to be fine, the beam falls in the centre of the mirror in most places. It's a brand new lens. Shall alter alignment and see it it changes or disapears. Don't think it is a shadow from anything. Thanks


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/FzyK3njBCLC) &mdash; content and formatting may not be reliable*
