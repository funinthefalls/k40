---
layout: post
title: "Thanks Vince Lee ! A big THANK YOU to Vince Lee for sending his spare middleman and Dual Adaptor boards"
date: October 14, 2016 23:58
category: "Discussion"
author: "HalfNormal"
---
Thanks **+Vince Lee**!



A big THANK YOU to Vince Lee for sending his spare middleman and Dual Adaptor boards. He even included the documentation. What a great community we have here.





**"HalfNormal"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 15, 2016 00:29*

He sent me a set as well. They look great. 


---
**Don Kleinschnitz Jr.** *October 15, 2016 01:16*

What a awesome community....:)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/QM3fHb3rQEV) &mdash; content and formatting may not be reliable*
