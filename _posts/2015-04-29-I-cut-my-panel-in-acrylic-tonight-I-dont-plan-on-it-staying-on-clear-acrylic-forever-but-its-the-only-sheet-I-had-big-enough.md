---
layout: post
title: "I cut my panel in acrylic tonight, I don't plan on it staying on clear acrylic forever but its the only sheet I had big enough"
date: April 29, 2015 22:10
category: "Modification"
author: "David Wakely"
---
I cut my panel in acrylic tonight, I don't plan on it staying on clear acrylic forever but its the only sheet I had big enough.



I was a bit rough with it and it and ended up cracking it but it works and fits!



I've only got one power switch wired up but everything else works. Looks pretty awesome in the dark!



Next step is to solder the Arduino circuit, I've attached the circuit diagram, its a bit messy but it works!



![images/e755ddad7887629216d3bab02087773b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e755ddad7887629216d3bab02087773b.jpeg)
![images/8d494748c5dc0eaed7ab472cf7cad7bf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d494748c5dc0eaed7ab472cf7cad7bf.jpeg)
![images/0586ad09583efb7db5ed744f00ee600d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0586ad09583efb7db5ed744f00ee600d.png)

**"David Wakely"**

---
---
**Jon Bruno** *April 29, 2015 22:20*

Looks great David! 

You could save a few pins by going with an I2C module for your LCD .

What are you using as a flow meter?

And are you willing to share your sketch? 

Its simple enough to write but why do it if someone already has.. Lol

I was planning on the same for monitoring the temp but throw in an alarm and flow sensor and I'm all in.﻿


---
**David Wakely** *April 29, 2015 22:30*

I did read about this somewhere but to be honest I'm just starting out with Arduino and the LCD came with a kit so i just used it. I have also seen that more than one temp sensor can be on the same wire with addresses or something?? It gets quite confusing but I'm still learning!



Its nice to see there are so many people from around the world appreciating my work. This group + lightobject forum really makes these machines great!


---
**David Wakely** *April 29, 2015 22:33*

Of course I will share the sketch. These are the exact items I purchased.



[http://www.ebay.co.uk/itm/111431573979](http://www.ebay.co.uk/itm/111431573979)



[http://www.ebay.co.uk/itm/350896148106](http://www.ebay.co.uk/itm/350896148106)


---
**Jon Bruno** *April 29, 2015 22:33*

Yeah man you can address many devices on the I2C bus. If you need any help writing it I'm here for ya.

I recently built what I call a nano-cluster for my micro car. Basically it's a .96" OLED display that reads out RPM, AFR, Temp, Battery voltage, and of course the time... As well as a little animation. It's on my YouTube channel if you'd like to see it. (SigmazGFX)﻿



And yes, keep up the good work. It's very inspiring.


---
**David Wakely** *April 29, 2015 22:39*

That sounds really cool. Ive upload the sketch here [http://www.thingiverse.com/download:1277454](http://www.thingiverse.com/download:1277454) its mainly public code thrown together to get it to do what i wanted but its given me a good understanding on how it all works, everyone's gotta start somewhere right?


---
**Jon Bruno** *April 29, 2015 22:40*

Yes sir. Good stuff.


---
**Jon Bruno** *April 30, 2015 00:39*

One question Sir.. which version of CorelDraw was this created in ? the CorelDraw12 reports it as an invalid format file.


---
**David Wakely** *April 30, 2015 08:40*

It is in X7. I will upload a backward compatible one for you.


---
**David Wakely** *April 30, 2015 08:45*

Ive uploaded it here [https://www.thingiverse.com/download:1278413](https://www.thingiverse.com/download:1278413)



Hope this helps!


---
**Jon Bruno** *April 30, 2015 11:09*

Thanks! I'll try it this evening


---
**Mauro Manco (Exilaus)** *May 20, 2015 22:48*

David add relay as normal close on laser switch in case of overtemp open relay..and laser stop to emitt. And head move in air. I stay make similar but without arduino....great panel I need change it on my k40.


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/BopDBB7YZ6j) &mdash; content and formatting may not be reliable*
