---
layout: post
title: "Hello everyone, I just bought my first K40 and been reading through the posts, but I have a few questions: What's the minimum GPH recommended for the air assist?"
date: May 03, 2018 13:10
category: "Discussion"
author: "Bentley Davis"
---
Hello everyone, I just bought my first K40 and been reading through the posts, but I have a few questions:



What's the minimum GPH recommended for the air assist? I see people buying a large variety, mainly for quietness, but no one has mentioned a minimum GPH for the air pump.



What's a good water pump for the coolant? I saw a post about a pump having a 6' pump ability so it can be placed low.



Are there any immediate things I need to check once I unbox it? I know I'll need to align the mirrors. I'm used to working with Chinese clones of 3D printers, so I'm used to tearing things open and Q/Cing them. TIA!





**"Bentley Davis"**

---
---
**Ned Hill** *May 03, 2018 14:44*

For the air assist it's going to depend somewhat on how you are implementing the air deliver.  That is, what is the diameter of the nozzle to the work piece.  Larger nozzles require more air.  I use a Light Objects air assist head which has an ~ 6mm opening and I find that I need at least 10L/min (~159GPH) and a max of ~ 20L/min.



I use the stock water pump which is probably on the low side of what is really needed.  Not sure what the rated flow rate is at the moment.



Make sure your mirrors and lens are clean and that the lens is oriented with the curve/bump side up.  Besides getting an air assist set up, the next upgrade you will want to consider is upgrading your mirrors and lens.     The stock lens should have a focal length of ~50.8mm but you will want to do a ramp test to verify your focal length for optimum performance.


---
**Ned Hill** *May 03, 2018 14:46*

Also, only use distilled water for cooling.


---
**Joe Alexander** *May 03, 2018 14:51*

I recall a post about water flow being at least 3-4 L/min, I use a small quiet fountain pump rated 240 L/h and it works great and is super quiet.


---
**James Rivera** *May 03, 2018 16:38*

Everything **+Ned Hill** said. I have the LightObject air assist nozzle, too. I also use the stock water pump. For the air, I bought a cheap aquarium air pump. It does not move much air, but I just wanted to keep the smoke out of the way of the beam and it does that just fine. I cleaned my mirrors--they were filthy and looked like someone cut 20mm diameter circles out of an old bathroom mirror. For now, they're working though. I upgraded my lens to use LightObject's 50.8mm ZnSe lens and that made a huge difference for me. My stock lens was actually crumbling around the edges (but that might've been my fault for torquing the lens holder too tight). Also, check **+Don Kleinschnitz**'s blog. A treasure trove of all things related to K40 upgrades.

[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com)


---
**Doug LaRue** *May 06, 2018 03:17*

Two problems with the air assist head nozzle is the diameter of the hole and how far away from the cut it is.  A simple brake line pipe from the auto parts store works much better since you can get it 5-10mm from the cut and the end can be closed down to blow a stream with far less volume of air required.  I currently use a 3D printed one - [thingiverse.com - K40 40W Laser Air Assist by VanBot112](https://www.thingiverse.com/thing:2421971)




---
*Imported from [Google+](https://plus.google.com/118082643259684269593/posts/XhRDD2LK4Vn) &mdash; content and formatting may not be reliable*
