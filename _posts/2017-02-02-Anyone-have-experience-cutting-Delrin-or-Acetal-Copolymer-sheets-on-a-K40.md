---
layout: post
title: "Anyone have experience cutting Delrin or Acetal Copolymer sheets on a K40?"
date: February 02, 2017 16:28
category: "Materials and settings"
author: "Bill Keeter"
---
Anyone have experience cutting Delrin or Acetal Copolymer sheets on a K40? Curious if I can use the same power/speed settings as cutting acrylic.



I really like the material. Just haven't tried cutting it on my K40 yet. Last year I was just using the preset settings on a Universal 60w.





**"Bill Keeter"**

---
---
**Nick Hale** *February 03, 2017 17:05*

I ordered some delrin.  Just waiting for it to come in to cut. 


---
**Bill Keeter** *February 03, 2017 17:16*

yeah, I have a couple 1/16" sheets i'm probably going to cut this weekend. I'll post if I have to adjust any of my settings


---
**Coherent** *February 03, 2017 22:40*

I'm intersted in the tests. My thoughts are delrin (Acetal homopolymer) machines very well without melting and I've read you can cut it on a laser with success especially if it's nice and flat. About impossible to glue the stuff, but FWIW I've heard Go2 glue from locktite works on the stuff. 


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/D1PMfDD93p7) &mdash; content and formatting may not be reliable*
