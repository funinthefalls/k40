---
layout: post
title: "Hi all. I've searched the Internet for material settings with a 38.1mm lens, but no luck"
date: November 22, 2016 13:44
category: "Hardware and Laser settings"
author: "Dennis Luinstra"
---
Hi all. I've searched the Internet for material settings with a 38.1mm lens, but no luck. 



Does anyone here use such a lens and which material settings do you use? (% power and speed in mm/s). Next week I hope that I'm able to test my K40 for the first time with my DSP and air assist (I'll post some pictures then). I want to use 3mm and 5mm Poplar plywood (cutting,  normal and deep line engraving)  in the future maybe 3 and 5mm acrylic). Can anyone help me out with this question? 





**"Dennis Luinstra"**

---
---
**Kelly S** *November 24, 2016 17:47*

The best way to figure it out is lots of testing.  :)  every machine is different in many ways even though they look similar.  Just use scrapes to find the focal point and then start playing with cutting and engraving on different materials.  When you find a setting first he material you are working on write it down.


---
*Imported from [Google+](https://plus.google.com/101821475060654077048/posts/9diekwGNrQE) &mdash; content and formatting may not be reliable*
