---
layout: post
title: "Double vertical line. When engraving, I have double vertical line"
date: October 18, 2016 22:02
category: "Discussion"
author: "Gilles Letourneau"
---
Double vertical line.



When engraving, I have double vertical line.

I made 3 tests to show my problem.



The top square is made while engraving at 100 mm/s

The second square is made at 200 mm/s

The bottom one is made with the cutting option.

It look like the problem while engraving only.



I'm using Corel laser

![images/b4033e24c4edbf139511e5cd51e0252c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b4033e24c4edbf139511e5cd51e0252c.jpeg)



**"Gilles Letourneau"**

---
---
**greg greene** *October 18, 2016 22:17*

Line width ?  


---
**Gilles Letourneau** *October 18, 2016 22:47*

Okay after reading other post on the net I check the belt and found that it was loose so after turning the screw to tight the belt everything is ok now.




---
**greg greene** *October 18, 2016 22:56*

Groovy ! (pun intended)


---
**Phillip Conroy** *October 19, 2016 03:33*

Cane you add a photo cutting 3mm mdf or ply with a ruler in metric ovrr cut ,cut width looks too wide to me


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 19, 2016 06:33*

Could it be related to lens refraction of the beam through a scratch maybe? because on the bottom "cut" the vertical lines are definitely thicker than the horizontal & the corners look like something weird is going on also.


---
**Gilles Letourneau** *October 19, 2016 14:26*

Hi Yuusuf,  it's the first thing I checked but I finally found that it was a loose belt.  So loose that it was the difference between pulling and pushing the lens from left to right that create the double line.   Tightening the screws of the belt fixed my problem. 


---
**Craig** *October 31, 2016 06:41*

Unless you upgraded the board, the K40 won't cut vector lines. If you want a rectangle cut (with one line) it has to be a black rectangle, then the K40 will cut the outside of the box.  Apparently there is now way w/o upgrading to vector cut a "line" w/o upgrading.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 31, 2016 06:57*

**+Craig** That's actually not the case. You can just create a vector line, then set line-width to 0.01mm. It will vector cut it using the CorelLaser plugin's "Cut" function.



edit: also, the colour doesn't matter for filled rectangles/shapes. I tested a white fill & it still cut around the outside of it (1 line).


---
**Craig** *November 07, 2016 06:52*

Nope - without upgrading to smoothie or similar, I annot get my K40 to cut a "line" . It will cut a super thin rectangle (2 passes).  If I want to cut a circle, I have to cut a black circle so it will cut the outside in one vector pass.  If I were to draw a circle t would cut inside and outside the line.  There isn't a setting I can see in corel laser to set line with to .01. It goes from 0.5 to hair width to none in outline width.   Unless thereis a setting im missing.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2016 07:43*

**+Craig** You type in 0.01 into the line width box.


---
*Imported from [Google+](https://plus.google.com/101789423601866268108/posts/3wmyqZhWzYn) &mdash; content and formatting may not be reliable*
