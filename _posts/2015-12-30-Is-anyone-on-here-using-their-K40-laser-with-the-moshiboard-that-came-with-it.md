---
layout: post
title: "Is anyone on here using their K40 laser with the moshiboard that came with it?"
date: December 30, 2015 19:39
category: "Smoothieboard Modification"
author: "Bryon Miller"
---
Is anyone on here using their K40 laser with the moshiboard that came with it?  I keep trying to find people that are happy with their laser and most of them mention they were pissed off until they upgraded the controller board.  Are there any users here that are using this thing with the board that came with it and are happy with it?  I know I want to get an x7 dsp but I don't feel like dropping $500 on an upgrade for a $400 item right now.  I'd at least like to get some successful attempts with the board and software this came with.  As is, it seems like this should have been much cheaper and sold as a DIY kit.





**"Bryon Miller"**

---
---
**Jim Hatch** *December 30, 2015 21:02*

I'm using the board that came with mine but it's not a "Moshi". It says it's an M2 Nano board made for LiHuiyu Studio Labs. I use LaserDraw as the driving software (I make my drawings in Inkscape or Corel and copy to LaserDraw for the cut).


---
**Sean Cherven** *December 30, 2015 21:18*

I am using the stock board, and I'm perfectly happy with it. I use the LaserDRW software (Not CoralDRAW).



I mean granted, you can't do as much as u would be able to do by upgrading the board.



Sometime down the road, I may design a drop-in replacement 32-bit controller board (Probably based off of the Teensy 3.2) that will be compatible with LaserWeb and others. I really don't feel like cutting and splicing wires inside the K40.  Drop-in replacement is where I stand.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 30, 2015 22:54*

Like **+Jim Hatch**, I'm also using the default board (which off the top of my head is an M2 board also). I design my images in Adobe Illustrator, save in older format (AI9) & then import into CorelDraw using the LaserDrw plugin. I'm reasonably happy with how it works. For the price of the machine, it does what I wanted it to do (although temperamental at times with alignment etc). I personally wouldn't spend $500 on an upgrade for it. I'd save that $ towards a bigger/better machine.


---
**Jim Root** *December 30, 2015 23:20*

I'm also using the oem board but is also not the moshi board and I use laserdrw software. I do need to replace mirrors and probably the lens and plan to add air assist. Beyond that I think that I would also save the money for a better machine but I'm not sure at this point. 


---
**Richard Vowles** *December 31, 2015 02:34*

I'd like to know where you can get a SmoothieBoard for $90 **+Peter van der Walt** - they seem all out of everything but the 5XC... As far as I can tell, Laser's only need the 3XC.


---
**Arestotle Thapa** *December 31, 2015 04:18*

I've been using Moshi. It has issues but with trial & error you can make it work (for most part). In my case if I'm engraving larger objects then I slow down the speed so it doesn't get stuck in the middle of engraving. I'm guessing that gives it enough time to transfer/re-transfer.  For cutting (vector), I make sure no parts have negative co-ordinates. i've usually noticed the laser acting weird when the X or Y value is below or around 0. I may eventually upgrade the board. I just don't know if there are opensource alternatives that are as easy and capable as lightobjects where I can cut and engrave on same run with different power levels per part/layer...


---
**Brooke Hedrick** *December 31, 2015 06:15*

Hi Bryon,



I bought a Globalfreeshipping k40 from eBay.  It came with MoshiBoard and MoshiDraw.  I spent about 3 weeks with it and even worked with support with little progress and a lot of delays going back and forth.



Was I PO'd...  That's probably not the right word, but highly frustrated would be spot on.  I knew what I could be getting into - you often get what you pay for and a Laser Engraver for 400$ shipped!...



I spent the money and time to upgrade to Lightobject DSP AWC708c lite and LaserCAD.  For me this was about 3 weeks of gutting, reading, and installing.  But, when I was done and aligned, all of my oddball issues with the head running into the ends of the rails, failed jobs that cut in random directions in the middle of the job, and jobs not even starting in the right place went away.



It is now a pleasure to use the machine.  I look forward to using it to get tasks done.  I no longer spend an hour or two messing around and wasting material to cut/engrave simple or complex projects.



I can't speak for the other replacement options out there, but many in this group have had great success.  If you happened to get the non-moshi board, you have a decent chance of sticking to the stock guts and having early success.  I wish I had known (read this group further back in time) what to look for when I bought mine.  In the end I did get to learn a bunch about how these machines work in the process.


---
**george ellison** *December 31, 2015 09:39*

I have made no upgrades to the board and mine has worked brilliantly, until the tube broke! (my fault) :)


---
**Stephane Buisson** *December 31, 2015 12:18*

what a brilliant community ! your comments are spot on, that describe perfectly the situation with the K40.

it's cheap, need a bit of extra work (a new board for Mac/Linux users, or to not use Moshi for everyone else), tighting somme electric connections, aligning mirrors, lens convex up. and a lower your expectations into K40 specs.

but I agree with **+Peter van der Walt** putting about 100 $ on top will change your life. After all that was my way to think (from design to lasercut) and not the other way around. Honestly a K40 should work like a inkjet printer.

I do like the ethernet connection, (usb is too short) because the k40 isn't in the same room as my desk.


---
**Arestotle Thapa** *December 31, 2015 14:14*

**+Brooke Hedrick**



Hi Brooke,



I was considering LO X7. I emailed them few times but didn't get any replies. I'm just wondering if they're just re-branding/reselling the controller/software from China (without really adding any values). It seems AWC708c from China is the same controller and has the same software as LO.



I do have few questions and would really appreciate if you could help:

> did you buy the whole kit or just the controller?

> did your original power supply work or you had to get a new one?

> How about stepper driver power supply?



Thank you,

Arestotle



PS. Has anyone used both Light Object (or AWC708/608) and one of the opensource controller for rastering? If so what is their experience?


---
**Brooke Hedrick** *December 31, 2015 15:34*

Hi **+Arestotle Thapa**,





> I was considering LO X7. I emailed them few times but didn't get any replies. I'm just wondering if they're just re-branding/reselling the controller/software from China (without really adding any values). It seems AWC708c from China is the same controller and has the same software as LO.



What is the question you are wanting to ask them?  Their forums are pretty active and a 'head person' there seems to respond pretty quickly.  Not sure how the Holidays are affecting them.





I do have few questions and would really appreciate if you could help:

> did you buy the whole kit or just the controller?

Kit - [http://www.lightobject.com/X7-DSP-upgrade-kit-for-DK40-small-CO2-laser-machine-P942.aspx](http://www.lightobject.com/X7-DSP-upgrade-kit-for-DK40-small-CO2-laser-machine-P942.aspx)



> did your original power supply work or you had to get a new one?

The original supply for the laser worked perfectly.  I am using the additional 24v supply as recommended in the comments section for the kit I ordered.



> How about stepper driver power supply?

I completely removed the MoshiBoard and generally followed:  [http://www.lightobject.info/viewtopic.php?f=16&t=1372](http://www.lightobject.info/viewtopic.php?f=16&t=1372)



Thank you,

Arestotle



>PS. Has anyone used both Light Object (or AWC708/608) and one of the opensource controller for rastering? If so what is their experience?



Can you help me understand the difference between rastering and engraving vs cutting?  I have tried understanding this for a bit and haven't come up with a clear difference.  Maybe in the case of the LO it just means I don't need a computer to translate a bitmap into an engraving vs just cutting.






---
**Jim Hatch** *December 31, 2015 15:37*

**+Peter van der Walt** Do you have a recommendation for a particular ramps board? The smoothies are all out of stock (even overseas) except the 5X. Getting a ramps board from someone like E3D and adding steppers, etc. starts to potentially open up a different rabbit hole of mods :-) 


---
**Jim Hatch** *December 31, 2015 15:47*

**+Yuusuf Sallahuddin** Where did you get your LaserDRW plugin for CorelDraw? One of the things I'd like to do with mine is to be able to drive the cutter using different line colors for engraving vs cutting like I can with Universals or Epilogs (that may be wishful thinking for a $400 laser though). The Inkscape or Corel drawing that I then need to import into LaserDRW to drive the cutter separately is somewhat tedious and being able to use the plugin from Corel where I could skip the copy/import step would be cool.


---
**Stephane Buisson** *December 31, 2015 16:03*

**+Jim Hatch** Visicut do it by colors, by layers, by line thickness, etc ...

( I will have a go at Laserweb very soon).



I am not a developer , but here is an idea, a plugin for Sketchup (Ruby anyone?), based on layer & group.


---
**Jim Hatch** *December 31, 2015 16:26*

**+Peter van der Walt** Got em. The Azteeg is also an out of stock item but found the Azsmz on ebay and stepper motor drivers on Amazon (the Azsmz board doesn't include the SD card or stepper motor drivers). Should be all-in for $75. :-) Thanks! 


---
**Jim Hatch** *December 31, 2015 16:57*

Waiting on the board now - should be here in a week or so. While we wait, I sent you a beer via your donate button. Have a great New Years!


---
**Arestotle Thapa** *December 31, 2015 20:32*

Hi **+Brooke Hedrick**



You answered most of the questions I had  (the ones that I emailed to LO).  Thank you.



Engraving (raster/bitmap) is almost like scanning one pixel at a time so it is much slower than cutting/marking (vector). You can also engrave vector drawings too. For example text is usually vector but you may want to engrave instead of cut if you want the text to be thicker instead of just outline of the text. In case of Moshi, the OutputType for raster is Engrave and vector is Outline. You can set each object in Moshi to either cut, carve, assist, and so on. I'd think LaserCad may have similar options too.



Do you know of a download location of LaserCad? I'd like to see how that works.



Happy New Year!!!



Regards,

Arestotle


---
**Brooke Hedrick** *December 31, 2015 20:37*

**+Arestotle Thapa**



Good to hear.  You can download LaserCAD from [https://www.sinjoe.com/index.php?route=product/product&product_id=96](https://www.sinjoe.com/index.php?route=product/product&product_id=96) .  There is a download section.  No dongle is required and you don't have to be connected to the controller for it to work either.


---
**Arestotle Thapa** *January 01, 2016 02:57*

Thanks Peter, thanks Brooke.






---
**Vince Lee** *March 01, 2016 00:53*

I'm using a Moshiboard, and while using it does get frustrating times, I've learned the app's quirks well enough that I can get by most of the time.  The app is buggy but still fairly functional.  I have a smoothieboard and have made an interface board to allow me to switch back and forth between the two, but haven't finished it yet since the Moshiboard works well enough that I haven't been too motivated.  My main frustrations are:



1) Occasionally it gets in a spot where it has a null pointer stored somewhere and constantly posts assertion alerts and doesn't draw onscreen until I close the file and reopen.



2) After closing the app, its driver sometimes blue-screens the computer



3) Older versions of the app used to drop bits sometimes when engraving a large image, causing the cutter to freak out partway through the job.  Since upgrading to a newer version of the board/software, this no longer happens since the data is protected with a checksum.



4) In general, the UI is confusing and the english translation is awful.  I edited the English.ini file to correct the translation in a previous version; I need to update those translations for the latest release.


---
**Arestotle Thapa** *March 01, 2016 02:52*

**+Vince Lee** which smoothie board are you using?


---
**Vince Lee** *March 03, 2016 17:43*

**+Arestotle Thapa** It says AZSMZ Mini Ver 2.1.  I haven't hooked it up yet, but I created and wired up my own adapter board that routes all the data and power lines into a single ribbon connector.  This is to let me easily switch back and forth between the smoothieboard an moshiboard either my moving a single ribbon cable or using a large rotary switch on the control panel.


---
*Imported from [Google+](https://plus.google.com/110125443441622069473/posts/6927WLrwETa) &mdash; content and formatting may not be reliable*
