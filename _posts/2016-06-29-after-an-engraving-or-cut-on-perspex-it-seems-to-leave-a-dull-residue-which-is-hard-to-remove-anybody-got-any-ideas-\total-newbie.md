---
layout: post
title: "after an engraving or cut on perspex it seems to leave a dull residue which is hard to remove anybody got any ideas, \\total newbie"
date: June 29, 2016 01:14
category: "Discussion"
author: "Glyn Jones"
---
after an engraving or cut on perspex it seems to leave a dull residue which is hard to remove anybody got any ideas, \total newbie.





**"Glyn Jones"**

---
---
**Alex Krause** *June 29, 2016 02:21*

I've heard of people using Brasso and also car polish and some people have said to put a thin layer of dish soap on the acrylic before you cut it


---
**Ned Hill** *June 29, 2016 03:03*

You could always mask it before cutting/engraving.  The Brasso on plastic doesn't work as well since they changed the formula. For polishing plastics I use the Novus plastic polish kit [https://www.amazon.com/NOVUS-7136-Plastic-Polish-Kit/dp/B002UD0GIG/](https://www.amazon.com/NOVUS-7136-Plastic-Polish-Kit/dp/B002UD0GIG/)  


---
**Glyn Jones** *June 29, 2016 08:13*

Hi thanks for the input will give all these methods a try.


---
**Ben Marshall** *June 30, 2016 02:27*

Small butane burner to melt it smooth? Have not used acrylics yet


---
*Imported from [Google+](https://plus.google.com/+GlynJonesCarlos/posts/fgirTAD4Fvd) &mdash; content and formatting may not be reliable*
