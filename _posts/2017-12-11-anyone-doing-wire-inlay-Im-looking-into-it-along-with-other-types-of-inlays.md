---
layout: post
title: "anyone doing wire inlay? I'm looking into it along with other types of inlays"
date: December 11, 2017 22:03
category: "Object produced with laser"
author: "Steve Clark"
---
anyone doing wire inlay? I'm looking into it along with other types of inlays. Pretty cool use of the laser!




{% include youtubePlayer.html id="HOd03cTeMwU" %}
[https://www.youtube.com/watch?v=HOd03cTeMwU](https://www.youtube.com/watch?v=HOd03cTeMwU)





**"Steve Clark"**

---
---
**thomas dillon** *December 11, 2017 22:34*

I bought laser engraver  just for inlays. I used copper, brass, plastics, many different stone, and resins. thank god for the laser because my free hand is god awful.


---
**Steve Clark** *December 11, 2017 23:04*

**+thomas dillon** I also have some Bismuth alloy I want to try... it melts at 158F (70C) that should be interesting for complex shapes. It's a little pricey but a little also goes a long way.



[https://www.mcmaster.com/#bismuth-alloys/=1an5glh](https://www.mcmaster.com/#bismuth-alloys/=1an5glh)




---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/C4uSPDNpJ6P) &mdash; content and formatting may not be reliable*
