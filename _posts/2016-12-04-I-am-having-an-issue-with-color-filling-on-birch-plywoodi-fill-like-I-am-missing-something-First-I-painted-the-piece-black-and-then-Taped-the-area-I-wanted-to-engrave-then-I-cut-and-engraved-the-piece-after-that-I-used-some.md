---
layout: post
title: "I am having an issue with color filling on birch plywoodi fill like I am missing something First I painted the piece black and then Taped the area I wanted to engrave then I cut and engraved the piece after that I used some"
date: December 04, 2016 21:46
category: "Discussion"
author: "3D Laser"
---
I am having an issue with color filling on birch plywoodi fill like I am missing something 



First I painted the piece black and then Taped the area I wanted to engrave then I cut and engraved the piece after that I used some white paint to color fill the piece but the paint still gets everywhere any ideas what I can do to make it work?

![images/bda4df5a2161dd54dffe44c19aab65a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bda4df5a2161dd54dffe44c19aab65a2.jpeg)



**"3D Laser"**

---
---
**Mike Meyer** *December 04, 2016 22:13*

Regardless of what type of tape you use, (including Frog tape), you're going to get some bleed through because of fibrous nature of the wood. The grain in the wood channels the paint past the tape. Here's an easy work around; prior to painting anything, seal the wood. I mixed up a dab of bondo and laid down a very thin coat to fill the grain. Let it dry, sand with 320, and then lay down your base coat. Tape, engrave, paint and when you pull tape, the lines will be clean.


---
**Jim Hatch** *December 04, 2016 23:04*

A coat or two of poly works too. And make sure you really press the tape down - use a printers rubber roller (you can get them at Michael's or Hobby Lobby).


---
**greg greene** *December 05, 2016 00:11*

One other thing is liquid masking - available at Hobby shops. Just spread it on,  and let it dry.  Afterwards it peels of, but being a liquid - will fill the wood grain so the paint stays where it should


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 05, 2016 00:26*

What kind of paint are you using? Also what kind of brush? I've recently done some using red acrylic & a sponge roller brush with very minimal bleed through on the plywood.


---
**Cesar Tolentino** *December 05, 2016 00:33*

Not sure if this is related. But when u do tape on a paint.  First paint it with the color of the wood which is black.  That color will be the one to go in the tape. Wait for it to dry. Then paint it with white next. It won't go in the tape anymore since black already sealed it. Then remove tape


---
**Ned Hill** *December 05, 2016 00:44*

You painted the surface first so it was basically sealed.  The Bleed through could be because the surface is too rough and the tape isn't getting a good seal.  Also try a thicker paint that is less likely to seep through.  I like using a stencil paste to do filling like that.


---
**HalfNormal** *December 05, 2016 01:38*

Another product to try is sanding sealer to seal the grain.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/CKJw33YzVeZ) &mdash; content and formatting may not be reliable*
