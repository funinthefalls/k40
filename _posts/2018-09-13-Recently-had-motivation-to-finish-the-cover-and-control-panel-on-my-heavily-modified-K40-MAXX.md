---
layout: post
title: "Recently had motivation to finish the cover and control panel on my heavily modified K40 MAXX"
date: September 13, 2018 23:23
category: "Discussion"
author: "Kelly Burns"
---
Recently had motivation to finish the cover and control panel on my heavily modified K40 MAXX

![images/57d2ed4a222276612de6891b6ffe33e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57d2ed4a222276612de6891b6ffe33e1.jpeg)



**"Kelly Burns"**

---
---
**Travis Sawyer** *September 14, 2018 00:40*

Really like the coolant readout! Is there a how-to somewhere?


---
**Kelly Burns** *September 14, 2018 02:12*

**+Travis Sawyer** I have documented everything so far and I have plan to publish a project. I just need to find the time to do it.



I used this [amazon.com - Robot Check](https://www.amazon.com/Thermometer-FlowMeter-Cooling-Liquid-Cooler/dp/B01A0HIEFU/ref=sr_1_1?ie=UTF8&qid=1536890202&sr=8-1&keywords=pc+water+cooling+thermometer)



I cut the PC Power Supply connector off and hooked it directly into the 5v line coming from the K40 power supply.  This Flow Meter is just visual, but it provides an easy way to put the temperature prob in-line.  I have it between the pump and the laser tube.



I was going to make a window in the panel and light the flow meter with an LED so so I could see the wheel moving and know the water was flowing.  I decided not to have the water connections close to the electronics.




---
**Don Kleinschnitz Jr.** *September 14, 2018 15:38*

This is how I solved the water flow and bubble problem. The water jacket temp meter is on my new panel.

[donsthings.blogspot.com - Improved K40 Cooling Circuit](http://donsthings.blogspot.com/2018/01/improved-k40-cooling-circuit.html)




---
**Don Kleinschnitz Jr.** *September 14, 2018 15:40*

Water temp on the upper right. [photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/VpQrNVzfLgMLf7Ld6)


---
**Don Kleinschnitz Jr.** *September 14, 2018 16:37*

BTW nice looking panel.....


---
**Kelly Burns** *September 14, 2018 16:37*

**+Don Kleinschnitz** Is this a competition?  ;)  if so, I forfeit !   



I see a meter like mine, how does the “water jacket” temp differ?  



I like your flow/pressure sensor.  I think I’ll do that when I replace the pump on my k40 and also use on my new scratch build.  I had to bypass the flow sensor on mine.  It was cheap and gave false failure indications.  As a result it would shut the laser off.  


---
**Don Kleinschnitz Jr.** *September 14, 2018 16:54*

Not at all a competition.... your panel looks much better than mine! 



The water jacket sensor is measuring the surface of the laser. I wanted (**+HalfNormal** idea?) to measure the temp of laser & compartment. Eventually I want to test blowing air across the laser as additional cooling.



My controller is on the front of the machine with separate GLDC. I want to make a side box like yours when I upgrade to a longer 50W laser and then move everything into that area and consolidate all panels.

This would would be an add on box that screws to the side of the machine with everything you need for a conversion.

It also allows you to expand the area into the full width of the current box.


---
**Don Kleinschnitz Jr.** *September 15, 2018 11:48*

BTW how did you get such nice lettering on the panel :)?


---
**Travis Sawyer** *September 15, 2018 11:50*

Right!?! Can't wait for the write up. My guess: engraved acrylic overlay with wiped on acrylic paint.


---
**Don Kleinschnitz Jr.** *September 15, 2018 11:52*

**+Travis Sawyer** now that you mention it I see signs of an overlay :)!


---
**Kelly Burns** *September 16, 2018 20:11*

Yes. Engrave and Acyluc Fill


---
**salvatore sasakingsoft** *October 03, 2018 20:08*

ciao mi sai dire che cinghia monta la laser k40 ?


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/U3APBCiyib1) &mdash; content and formatting may not be reliable*
