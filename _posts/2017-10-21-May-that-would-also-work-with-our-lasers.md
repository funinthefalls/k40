---
layout: post
title: "May that would also work with our lasers"
date: October 21, 2017 15:53
category: "Object produced with laser"
author: "BEN 3D"
---
May that would also work with our lasers.





**"BEN 3D"**

---
---
**Ariel Yahni (UniKpty)** *October 21, 2017 16:17*

Yes, I have etched glass on a K40. I made  this last year

[instagram.com - Instagram post by uniK â€¢ Jun 19, 2016 at 2:52am UTC](https://www.instagram.com/p/BG0eppvLKTi/)


---
**HalfNormal** *October 21, 2017 16:46*

The K40 etches glass without any other steps involved. This was done for a friend who used it for a project for her friend.

![images/14558d6a0c35be229cddf4b214b3e9ca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14558d6a0c35be229cddf4b214b3e9ca.jpeg)


---
**Scorch Works** *October 21, 2017 17:50*

You can also engrave the back side of a mirror then paint it.  Then you get an image on the mirror  and retain the smooth surface on the front of the mirror.



Below is a link to an item I made while playing with the process.  I left the bird unpainted so the bird transmits light like frosted/etched glass.

[instagram.com - Instagram post by Scorch â€¢ Oct 10, 2017 at 1:44am UTC](https://www.instagram.com/p/BaDKy0rll3z/?taken-by=scorch_works)


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/ModRbU3ohA3) &mdash; content and formatting may not be reliable*
