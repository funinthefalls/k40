---
layout: post
title: "My first complete design...I'm happy with it....inkscape is sweet"
date: January 27, 2016 22:22
category: "Object produced with laser"
author: "Scott Thorne"
---
My first complete design...I'm happy with it....inkscape is sweet.

![images/5a5b2855d63fd3ef6631170b67f05ce7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a5b2855d63fd3ef6631170b67f05ce7.jpeg)



**"Scott Thorne"**

---
---
**Scott Thorne** *January 27, 2016 23:37*

Thanks Carl!


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/X1kPj2gQUkG) &mdash; content and formatting may not be reliable*
