---
layout: post
title: "Hi guys, I have an almost un-modded K40 (Just got the LO Air assist head) and just wondered if anyone else has had this same issue with theirs"
date: November 23, 2016 09:31
category: "Original software and hardware issues"
author: "1981therealfury"
---
Hi guys,



I have an almost un-modded K40 (Just got the LO Air assist head) and just wondered if anyone else has had this same issue with theirs.  Basically the POT that controls the current to the laser tube starts at about 6oclock direction, but up until about 8oclock area if the laser fires there is just a squeak that comes from i assume the PSU and no laser firing.



Once it gets to 8oclock then i get a laser fire and between 8oclock and 9oclock i get the full range of power from minimum about 8ma to about 24ma at 9oclock.  After this turning the pot further does not change the power output of the laser any more but the pot continues twisting right through to the 3oclock direction.



Not sure if i just have a dodgy pot or if its PSU related.





**"1981therealfury"**

---
---
**Stephane Buisson** *November 23, 2016 13:11*

dodgy pot surely, keep your mA under 15 to save your tube.


---
**1981therealfury** *November 23, 2016 13:17*

I usually run 10-12ma max as i only usually work with 3mm ply or 3mm acrylic which cut fine at the 10-12ma region.  It is useable with the issue i have it just annoys me that i can't dial in that fine tuned ma because the full range of power is covered in about 1/40th of a turn of the pot lol.



Will have to look into a new pot but not sure where to start.


---
**HalfNormal** *November 23, 2016 15:29*

**+1981therealfury** The stock pot is a 2 thousand ohm, single turn pot. You can use just about any value and multiple turn too. The higher ohms and more turns, the finer the control.


---
*Imported from [Google+](https://plus.google.com/+1981therealfury/posts/enPDoQaU9Yy) &mdash; content and formatting may not be reliable*
