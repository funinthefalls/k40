---
layout: post
title: "I need a little help with Inkscape"
date: January 31, 2017 04:51
category: "Discussion"
author: "3D Laser"
---
I need a little help with Inkscape.  I am trying to make an x wing template holder and the svg files I have seen to be converting to the wrong size.  When I open the file up the templates are about half the size they should be according to the creator of the file they should be regulation size any idea why the images are smaller when I open them up 





**"3D Laser"**

---
---
**Joe Alexander** *January 31, 2017 05:12*

are you making sure that both the document size and DPI match in both inkscape and LaserWeb?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 31, 2017 05:21*

Not sure what your files DPI is, but I've noticed when I create in Inkscape they are coming out at 90dpi. Maybe a setting somewhere in Inkscape to adjust this (not sure as I rarely use it, but will start using it more frequently soon since LW seems to support it better than Adobe Illustrator).



If you want to share the SVG file I can take a look & see if I can see anything that may be causing your issue.



Oh wait, are you using LW or CorelLaser/LaserDRW still?


---
**Joe Alexander** *January 31, 2017 05:28*

here is a setting in preferences, along with one when you use the export png image ability that allows you to set your dpi (I use up to 1000)


---
**3D Laser** *January 31, 2017 12:21*

**+Yuusuf Sallahuddin** I am still using the original hardware it serves my needs for now as I am looking to get a bigger machine in the next six months I haven't wanted to invest much more into this one 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 31, 2017 12:39*

**+Corey Budwine** In that case, when I imported SVGs into CorelDraw to use with CorelLaser, the SVGs always came in at odd sizes. However, they were perfectly proportionate to the original (i.e. the width & length of the overall objects were in the same ratios compared to the original).



If you're using LaserDRW, not much that I can suggest as I have no idea about it. But, if using CorelDraw/Laser then you can manually rescale the entire set of objects by selecting them all & then using the dimension fields (for width & height) making sure to have the proportion lock toggled on (so you just adjust width & it will rescale height at the same factor).



If using LaserDRW, the only thing I can really suggest is to adjust the dpi you are creating the SVGs in with Inkscape (or if you are using already created ones, open in Inkscape & resave with a different dpi). Might take a bit of trial & error to figure out which is the correct dpi scale for importing into LaserDRW however.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/fFEcKQHgYSD) &mdash; content and formatting may not be reliable*
