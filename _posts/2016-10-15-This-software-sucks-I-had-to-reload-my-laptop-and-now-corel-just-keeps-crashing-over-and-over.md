---
layout: post
title: "This software sucks! I had to reload my laptop and now corel just keeps crashing over and over"
date: October 15, 2016 14:38
category: "Software"
author: "Ric Miller"
---
This software sucks! I had to reload my laptop and now corel just keeps crashing over and over. I have an old XP machine and a windows 7 machine and I cannot get either to work. 



On the XP machine I constantly get an A.I.R. error and the buttons are everywhere and half Chinese. 



On the Windows 7 machine it crashes every time I import a jpg.  I would just recreate the jpg in corel but I dont know how to do these rounded corners and skewed sides. 

I ordered a smoothie board and cannot wait to try laserweb3 but I am waiting on a middleboard still from a kind but forgetful soul.



HELP!!!!



[https://drive.google.com/file/d/0BwRbUywASsGJdGs2bjhQMEJCckU/view?usp=sharing](https://drive.google.com/file/d/0BwRbUywASsGJdGs2bjhQMEJCckU/view?usp=sharing)





**"Ric Miller"**

---
---
**Ric Miller** *October 15, 2016 14:49*

After doing a little more research I bet I was using LaserDRW. I will try that and report back. The reason I reloaded my machines is because I couldn't remember the passwords. I have amnesia from a medical condition. That part sucks but at least I woke up and owned a laser. It could have been worse. I could be learning how to feed my hogs or something :) 


---
**HalfNormal** *October 15, 2016 15:25*

**+Ric Miller** I wish I had that excuse when I buy my toys! ;-)

I am sorry you are having so much trouble with the software. The LaserWeb and Smoothie communities would not be doing such a great business if the K40 worked out of the box.


---
**Ric Miller** *October 15, 2016 15:27*

I now have a second k40 in the box because of the illness, but I will get into that later. Yeah, it can be convenient and very inconvenient. 


---
**HalfNormal** *October 15, 2016 15:29*

I own two K40s also but that is because I am a laser horder.


---
**Ric Miller** *October 15, 2016 16:21*

I am cutting now but the dimensions are way off. I put in my model and serial number in the settings. Ugh, I feel like I have done this before! :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 17:05*

I imagine that waking & finding you own a Ferrari somehow wouldn't go down too bad, until you see your bank balance. But on the issue of the software, it is capable of working with Windows 7 (CorelDraw + CorelLaser plugin). I did that for a while until I upgraded to Win 10.


---
**Ric Miller** *October 15, 2016 17:12*

I wonder why every time I try to import a jpg it crashes then? Maybe I should try to make the image a BMP and import it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 17:38*

**+Ric Miller** Any chance you could share a Google Drive (dropbox/onedrive/whatever else) link to the jpg (sharing via a post here tends to mess up the original file)? I can test & see if there is an issue with the jpg for you.


---
**Ric Miller** *October 15, 2016 18:01*

That was hard for me to understand. does drive get messed up or do the the others?


---
**Ric Miller** *October 15, 2016 18:06*

The driv e share link is: [https://drive.google.com/file/d/0BwRbUywASsGJdGs2bjhQMEJCckU/view?usp=sharing](https://drive.google.com/file/d/0BwRbUywASsGJdGs2bjhQMEJCckU/view?usp=sharing) 



[drive.google.com - 1590A_LASER.jpg - Google Drive](https://drive.google.com/file/d/0BwRbUywASsGJdGs2bjhQMEJCckU/view?usp=sharing)


---
**Ric Miller** *October 15, 2016 18:08*

This is what I am trying to veneer. [https://www.mammothelectronics.com/products/4s1590a-enclosure?variant=28989130247](https://www.mammothelectronics.com/products/4s1590a-enclosure?variant=28989130247)

[mammothelectronics.com - 4S1590A Enclosure - (3.64" x 1.52" x 1.06")](https://www.mammothelectronics.com/products/4s1590a-enclosure?variant=28989130247)


---
**Ric Miller** *October 15, 2016 18:27*

This is how far it's off :)![missing image](https://plus.google.com/photos/...)


---
**Ric Miller** *October 15, 2016 18:29*

Btw, I cut on a postal envelope 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 18:35*

**+Ric Miller** Yeah, I meant the Drive links are good. Others get messed up. I will take a look at these files & see how it goes importing into my CorelDraw/CorelLaser & let you know.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 18:42*

**+Ric Miller** I managed to get it to import into CorelDraw/CorelLaser without any issues.



[https://drive.google.com/file/d/0Bzi2h1k_udXwa0Nka01WckdaMm8/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwa0Nka01WckdaMm8/view?usp=sharing)



On a side note, are you trying to cut or engrave this?



Also, you mention "that's how far it is off" so are you having issues getting the positioning of the piece/cutting/engraving?


---
**Ric Miller** *October 15, 2016 18:52*

**+Yuusuf Sallahuddin** Could you please help me with my machine / software settings? I am literally mentally challenged at the moment. It has to be something easy. I would rather use my XP machine since its in the garage, but the WIN7 machine will work too. you tell me. whats best. I just want to be able to draw dimensional stuff in "fireworks, illustrator, corel, gimp, etc" and laser it correctly.


---
**Ric Miller** *October 15, 2016 18:56*

I am cutting, position is nonproblem


---
**Ric Miller** *October 15, 2016 19:00*

My area. Like I said, not new but amnesia. ![missing image](https://plus.google.com/photos/...)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 19:10*

**+Ric Miller** Yeah, I can assist with the settings etc.



So, I am unsure about using Win XP, as I haven't tested that. Win 7 definitely works.



I do all my design in Illustrator & export those files as either Adobe Illustrator 9 format (when I was running CorelDraw12 with the CorelLaser plugin) or now SVG (when I was running CorelDraw x7 with the CorelLaser plugin). That is for vector cut files.



For engraving, I just used bmp or jpg, either worked.



So, I have a few images to check for your settings. I will share here:



First, CorelDraw Settings:

[https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)



Second, Device Initialise Settings:

[https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)



Cutting Settings (you may vary speeds though):

[https://drive.google.com/file/d/0Bzi2h1k_udXwLW8zT3RLcC1Zd1U/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwLW8zT3RLcC1Zd1U/view?usp=sharing)



Engraving Settings (again may vary speeds):

[https://drive.google.com/file/d/0Bzi2h1k_udXwYUdVc1I5NFY1R2c/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwYUdVc1I5NFY1R2c/view?usp=sharing)



Hopefully some of that helps to start you off. Anything else, I'll see what I can do to assist.


---
**Ric Miller** *October 16, 2016 01:34*

After reloading the software 3 times I can draw a 20mm x 20mm square and get a relatively close cut. Thanks folks. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 16, 2016 04:38*

**+Ric Miller** Good to hear that you've got it mostly sorted. Any extra assistance needed & hopefully we can help again.


---
**Mike Cahill** *December 10, 2016 20:56*

I've got two of em as well one just isn't enough, I'm most of the way through converting one to run arduino with ramps1.4 because I am so disappointed with the software, a great product let down by crap software   


---
*Imported from [Google+](https://plus.google.com/102361688882180483990/posts/iXWgNPNNuEK) &mdash; content and formatting may not be reliable*
