---
layout: post
title: "Note to self... Lasers are dangerous! I made the mistake of walking away from the machine for 5 minutes while it was cutting"
date: August 15, 2016 07:28
category: "Discussion"
author: "Anthony Santoro"
---
Note to self... Lasers are dangerous! 

I made the mistake of walking away from the machine for 5 minutes while it was cutting. Don't repeat my mistake. 



![images/796a07b170f538318898bd168297a25b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/796a07b170f538318898bd168297a25b.jpeg)
![images/d6821152638ca700c6bc424958cb8eb1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d6821152638ca700c6bc424958cb8eb1.jpeg)

**"Anthony Santoro"**

---
---
**Stephane Buisson** *August 15, 2016 07:40*

feel sorry for you.

As everybody in the community, we are playing with fire.

Don't leave a working machine unattended.


---
**Tony Schelts** *August 15, 2016 07:57*

Gutting,  was the machine destroyed or is it cosmetic. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 15, 2016 08:12*

**+Anthony Santoro** That's extremely unfortunate. What exactly were you cutting at the time? Could it have played a role in the destruction?



**+Tony Schelts** Looking at the second picture, it looks pretty done!


---
**Bart Libert** *August 15, 2016 09:02*

WHAT happened here ? Your workpiece catched fire ?




---
**Anthony Santoro** *August 15, 2016 09:39*

I was cutting 3mm acrylic sheet. I was planning on making the cut in 3 passes. After the first pass I opened the lid to take a look, all was okay. Second pass started, I walked away, returned and there was smoke everywhere! I quickly hit the safety switch and sprayed the fire with an extinguisher that I had close by. 

The machine is toast. I can probably salvage some parts, but for the most part it's scrap. My saving grace was a 6mm ply sheet that I sit atop the cutter during cutting, if this wasn't there then the flames would have come out of the machine and onto the rest of the equipment in the room. 



I actually don't know what caused the fire, I'm guessing maybe the acrylic got too hot and became alight. I don't have another explanation. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 15, 2016 09:58*

**+Anthony Santoro** Possibly the fumes off the acrylic were flammable & not being extracted well for some reason?



edit: I just found that the fumes are a fire hazard. -Fire and explosion hazards:

Heated material can form flammable vapors with air. -

[http://www.alro.com/datapdf/plastics/plasticsmsds/msds_plexiglasg.pdf](http://www.alro.com/datapdf/plastics/plasticsmsds/msds_plexiglasg.pdf)


---
**Anthony Santoro** *August 15, 2016 10:14*

**+Yuusuf Sallahuddin** that may be the case, I'm not too sure.


---
**William Steele** *August 15, 2016 14:06*

Also, cutting acrylic needs an air assist to blow down through it, or it will ignite at the surface layer.  Did you remove the paper/plastic backing on it?


---
**HalfNormal** *August 15, 2016 14:22*

I am truly sorry for your loss. The best lesson learned here is that you kept a fire extinguisher close by. I am sure it could have been much worse without your quick thinking and keeping the correct safety equipment handy. 


---
**David Cook** *August 15, 2016 16:08*

This is unfortunate.  However,  thank you very much for posting your pics and your story here for all to see.This is a good reminder of the danger this machine can be.






---
**Alex Krause** *August 15, 2016 16:09*

Take out the electronic partition and rebuild the gantry with Openbuilds vslot and a linear actuator... you can "re-make" the machine to have an approx  200 X 500mm cutting area


---
**Jeremie Francois** *August 15, 2016 18:54*

Huh. First: sorry for that. Second idea: could a smoke detector and better, any kind of temperature-activated switch have saved your machine? I feel like it may not be that hard to detect such issue, shut everything down and switch an alarm? Now... I have read that some K40 do not even come with a switch on the lid :/


---
**Anthony Santoro** *August 16, 2016 00:47*

Hindsight is a wonderful thing. 

I think I will use the machine structure to rebuild something bigger and better! 

I have a spare machine, so I will use that for the standard cutting (once the useless supplier provides me with a replacement for the faulty tube), then use the burnt machine as a project machine. 


---
**Robi Akerley-McKee** *August 16, 2016 18:16*

Air Assist has saved my butt many times.  It doesn't take much air to blow out any fires started while cutting. I also keep a spray bottle handy to shoot spreading ember burns -- balsa does it quite a bit.  I drilled some 7/8" "gun ports" on the front to stick in the spray bottle.  I have gaff tape covering the holes as little lids to break sight lines..  They also help with the air extraction since they help pull the smoke to the rear across the work table. I covered up the big hole in the bottom, piece of plaque AL sheeting, then gaff taped that to the bottom.  1/2 conduit hole saw with carbide cuts the 7/8" hole.


---
**Victor Hurtado** *August 22, 2016 03:12*

My solidarity my friend, hope you can overcome this setback as soon as possible LUCK.


---
*Imported from [Google+](https://plus.google.com/106649411786258336023/posts/J2GrJaRY4BV) &mdash; content and formatting may not be reliable*
