---
layout: post
title: "I have decided to upgrade my K40 by building a new frame supporting the XY rails and to increase the cutting area to 620mm X 450mm"
date: April 10, 2018 12:02
category: "Modification"
author: "Duncan Caine"
---
I have decided to upgrade my K40 by building a new frame supporting the XY rails and to increase the cutting area to 620mm X 450mm.  This is as far as I've got.


{% include youtubePlayer.html id="_MojfHjxDUU" %}
[https://youtu.be/_MojfHjxDUU](https://youtu.be/_MojfHjxDUU)





**"Duncan Caine"**

---
---
**Don Kleinschnitz Jr.** *April 11, 2018 11:41*

Nice start. Would be great if you were to list the source links for parts ....


---
**Duncan Caine** *April 12, 2018 15:30*

**+Don Kleinschnitz** Hi Don, I don't know who manufactures the tube system, but I bought it from a supplier local to me in Kingston on Thames near London 

[http://www.tradesystems.co.uk/Dexion-Speedfame-Compatible-25mm-Square-Tube-System.html](http://www.tradesystems.co.uk/Dexion-Speedfame-Compatible-25mm-Square-Tube-System.html)



and the linear rails I bought on ebay, similar to this 

[https://www.ebay.co.uk/itm/2Pcs-SBR16-Guide-Linear-Rails-Shaft-L300-1500mm-with-4Pcs-SBR16UU-For-CNC-Kit/142713036010?ssPageName=STRK%3AMEBIDX%3AIT&var=441704656988&_trksid=p2057872.m2749.l2649](https://www.ebay.co.uk/itm/2Pcs-SBR16-Guide-Linear-Rails-Shaft-L300-1500mm-with-4Pcs-SBR16UU-For-CNC-Kit/142713036010?ssPageName=STRK:MEBIDX:IT&var=441704656988&_trksid=p2057872.m2749.l2649)






---
**HalfNormal** *April 13, 2018 03:53*

I found a US supplier.



[eztube.com - EZTube &#x7c; The Aluminum Framing System by International Designs](https://eztube.com/)




---
**Duncan Caine** *April 13, 2018 06:55*

**+HalfNormal** and its aluminium, even better.


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/HGeUWmsodK2) &mdash; content and formatting may not be reliable*
