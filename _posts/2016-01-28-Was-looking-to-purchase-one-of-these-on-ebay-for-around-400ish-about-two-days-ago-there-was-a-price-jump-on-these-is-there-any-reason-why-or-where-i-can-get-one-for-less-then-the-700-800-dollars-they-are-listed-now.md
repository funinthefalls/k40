---
layout: post
title: "Was looking to purchase one of these on ebay for around 400ish about two days ago there was a price jump on these, is there any reason why or where i can get one for less then the 700-800 dollars they are listed now?"
date: January 28, 2016 06:04
category: "Discussion"
author: "Gunstar Mods LLC"
---
Was looking to purchase one of these on ebay for around 400ish about two days ago there was a price jump on these, is there any reason why or where i can get one for less then the 700-800 dollars they are listed now?





**"Gunstar Mods LLC"**

---
---
**HP Persson** *January 28, 2016 06:54*

I bought one from Aliexpress yesterday, some of them have local warehouses in EU/US to ship from.


---
**Stephane Buisson** *January 28, 2016 11:37*

Chinese new year effect, I notice that last year too. be patient for market refresh.


---
**3D Laser** *January 28, 2016 12:03*

I just bought mine on eBay for 396 with shipping I saw there was about a 10 dollar hike in shipping but that's all


---
**Ben Walker** *January 28, 2016 12:48*

I got mine on ebay - 382 delivered (well it is not delivered yet).  Seems the ebay algorithms play hide and seek.  Look for High Precise 40W Laser from equipmentwholsaler1.  They have them for $344 but the ship it steeper than last week - about 15$ higher.

[http://www.ebay.com/itm/High-Precise-40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/260825065645?hash=item3cba62a8ad:g:nVUAAOSwqu9U0J0b](http://www.ebay.com/itm/High-Precise-40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/260825065645?hash=item3cba62a8ad:g:nVUAAOSwqu9U0J0b)


---
**Gunstar Mods LLC** *January 28, 2016 14:47*

**+Ben Walker** Of course 0 left now lol


---
**Gunstar Mods LLC** *January 28, 2016 14:51*

**+HP Persson** do you happen to have a link?


---
**Gunstar Mods LLC** *January 28, 2016 14:51*

**+Corey Budwine** Do you have a link?


---
**HP Persson** *January 28, 2016 14:55*

**+Luke Rogers** Could not see any at the moment for the US, but i remember seeing them alot lately when trying to find one with nice price.




---
**3D Laser** *January 28, 2016 15:14*

I just looked on eBay and you are right the price did jump 


---
**Ben Walker** *January 28, 2016 15:19*

Wow.  Did not expect that.   Must be because of holiday in China.  Or maybe I should mark mine up 40% and sell it and wait for prices to drop.  I have a glowforge ordered so this was just to buy time while I wait.   


---
**3D Laser** *January 28, 2016 15:20*

**+Ben Walker** I'm hearing aweful things about the glow forge from people who work with lasers for a living what do you think about it ben


---
**Sunny Koh** *January 28, 2016 15:33*

Trust me logistics within China is a complete disaster right now. I am headed to Shenzhen to pickup my paid for replacement Tube, send back the original tube and have loads of shopping. Let's say I will be leaving China come Monday with not everything that I paid for for online shopping.


---
**Sunny Koh** *January 28, 2016 15:35*

Just to let everyone know, Most of China will only be back at work on Feb 15. That is right, 1/2 of Month they will not be in the office.



The most basic machine you can get within China at the moment is 1450RMB plus delivery in Taobao


---
**Ben Walker** *January 28, 2016 17:46*

**+Corey Budwine** Well it is hard to say whether it is a good machine or not because the 50 that are out actually belong to Glowforge and the users are under a non disclosure agreement.  They are testing the final software now.  I (as of last week) could have 10 K40's for what the Glowforge cost.  I was ?lucky? to get in on the preorder very early so it appears I will be getting mine in the first batch.  The hardware is being built right now and I do know that they sourced some very high priced components to build them.  The variable power supply has my interest really high - grayscale raster images.  This is an amazing improvement (think Epilog).  The portability of the glowforge is also a great selling point for me.  And of course the auto-focus and the drag a design and drop it onto the material is fascinating.  As soon as they get to shipping the company is releasing the software as open source so we can run our own servers and not need to use the google cloud.  There are more positives than negatives in my opinion.  Some things do concern me - the shallow depth and absolutely no rotary option without rebuilding the whole machine.  The mirrors never need to be aligned as the laser rides the gantry.  A lot of people see the machine in action and think it is an off the shelf cheap resistor laser.  And they have gotten some flack for calling it a 3D printer.  Which opens up a whole new conversation.  

I hope to report back once mine lands.  Good or bad.  Same for the K40


---
**Stephane Buisson** *January 28, 2016 17:56*

**+Ben Walker** is the glowforge a CO2 laser ? where do they hide the bucket of water ? look small to hide a 70cm (40w) laser tube.

is it a laser diode ? could you cut wood ? worry about power.

Sorry I didn't do more investigation on the glowforge, when I did learn "bre Petis" was behind the project. (ex makerbot)

BUT it's amazing what you can do with modified K40 (<550usd). (if you need to project your design on material just use VisiCut and a IP cam)


---
**3D Laser** *January 28, 2016 19:49*

One of the biggest cons I have heard is how is the thing vented and what will filters cost for it


---
**Ben Walker** *January 29, 2016 14:39*

**+Stephane Buisson** yes it is 45 watt co2.  With a permanently sealed cooling system.   This is another huge advancement.   They did have trouble with the first units when they were being demonstrated in full sun but the units in use now are running nearly non-stop indoors.   


---
**Ben Walker** *January 29, 2016 14:45*

**+Corey Budwine** so you have two choices.  It has built in extraction fans and can be vented out a window or any other vent.   It's a standard US dryer vent (supplied of course) or you can have the stand alone filter as I got.   Yes expensive but I will only be using that when it is to cold to use the outside venting.   I don't plan to cut hazardous materials so I expect that filter to last me years.   It does cut 1/4 inch solid wood and acrylic and has an software feature to double that by simply flipping the part over.   Very sophisticated software that sets the registration.   And of course being able to engrave 20 inches x infinity with pass through option.   


---
**Ben Walker** *January 29, 2016 14:50*

I'm seeing how this k40 sales model works:  they start selling these k40 at basement prices until they get the sales up into the 100s.  Then they raise the shipping charge.   Once there are hundreds in transit and the same seller,  under a different seller account starts selling them at very premium prices.   Of course when you look at the auctions you see the price (average today $850) and how many have sold and if you investigate the ratings and see happy people you automatically assign value to the item.   Smart


---
**3D Laser** *January 29, 2016 15:08*

**+Ben Walker** I think they will drop after the Chinese New Year.  We will see what happens feb 15


---
**3D Laser** *January 29, 2016 15:15*

620 is the cheapest I found the K40 for today glad I bought mine last week!


---
**Gunstar Mods LLC** *January 30, 2016 04:04*

i confirmed they will drop after the Chinese new year


---
**Gunstar Mods LLC** *January 30, 2016 04:05*

**+Corey Budwine**  i bought one of the ones for 620 and someone about 8 minutes later bought the last one.  yeah i know i over paid but i needed one and could not wait two more weeks




---
**Gunstar Mods LLC** *January 30, 2016 04:05*

**+Ben Walker** ive talked to the vendors they will go back down in price in about two weeks


---
*Imported from [Google+](https://plus.google.com/116628870170489301579/posts/52nAYcqLSV2) &mdash; content and formatting may not be reliable*
