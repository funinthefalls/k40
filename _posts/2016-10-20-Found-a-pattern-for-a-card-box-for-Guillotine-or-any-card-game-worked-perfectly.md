---
layout: post
title: "Found a pattern for a card box for Guillotine (or any card game) - worked perfectly!"
date: October 20, 2016 05:10
category: "Object produced with laser"
author: "Tev Kaber"
---
Found a pattern for a card box for Guillotine (or any card game) - worked perfectly!



Pattern here: [http://www.funelements.com/2013/01/21/laser-cut-deck-box/](http://www.funelements.com/2013/01/21/laser-cut-deck-box/)





![images/5faed47ba8cba922f1481928d3065a8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5faed47ba8cba922f1481928d3065a8c.jpeg)
![images/03d7edc2a424d9f567ea3554386a9453.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/03d7edc2a424d9f567ea3554386a9453.jpeg)
![images/08c5974184bd5d4b2794a7f63c481f90.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/08c5974184bd5d4b2794a7f63c481f90.jpeg)

**"Tev Kaber"**

---
---
**Alex Krause** *October 20, 2016 05:17*

May need to make me a cards against humanity box or three :P


---
**Gunnar Stefansson** *October 20, 2016 06:05*

Simple and Functional, really like the edges, looks very clean


---
**Tev Kaber** *October 20, 2016 11:09*

I used an orbital sander to clean scorching, also to get a perfect smooth fit with the inner box.


---
**Tev Kaber** *October 20, 2016 11:14*

The first pic compares an unsanded part with a sanded one. 


---
**Aleš Tomeček (Alandran)** *October 20, 2016 19:48*

Looks nice, thanks for sharing. Did you use custom width, or the default zero? thanks


---
**Tev Kaber** *October 20, 2016 21:08*

I did a little cleanup, joining shapes, changing text, setting line width to 0.0011


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/jMkWqmfuyFB) &mdash; content and formatting may not be reliable*
