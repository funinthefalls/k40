---
layout: post
title: "Anthony Bolgar , does this work for you?"
date: October 25, 2016 02:29
category: "Modification"
author: "Ulf Stahmer"
---
**+Anthony Bolgar**, does this work for you?  Let me know and I can send you the file to post on GitHub. I forked your code and updated it for a waterproof DS18B20 temperature sensor, but that sensor requires a 4.7k ohm pull-down resistor. I've got a version showing that configuration as well.

![images/26e11e559c60fb5939801afaf1d6c684.png](https://gitlab.com/funinthefalls/k40/raw/master/images/26e11e559c60fb5939801afaf1d6c684.png)



**"Ulf Stahmer"**

---
---
**Ulf Stahmer** *October 25, 2016 02:53*

Just noticed that the TMP36 sensor is analog and should be going to A0 (the DS18B20 is a digital sensor), and the upper right black arrow on the relay should be going to the centre screw for a N/C connection.


---
**Anthony Bolgar** *October 26, 2016 16:13*

Looking good, feel  free  to send a PULLREQUEST


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/NMHTTXD5JXq) &mdash; content and formatting may not be reliable*
