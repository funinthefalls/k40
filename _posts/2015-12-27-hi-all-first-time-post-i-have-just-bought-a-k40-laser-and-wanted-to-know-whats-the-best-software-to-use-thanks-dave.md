---
layout: post
title: "hi all first time post.. i have just bought a k40 laser and wanted to know whats the best software to use thanks dave"
date: December 27, 2015 23:44
category: "Software"
author: "Dave Patmore"
---
hi all

first time post..

i have just bought a k40 laser and wanted to know whats the best software to use

thanks dave





**"Dave Patmore"**

---
---
**I Laser** *December 28, 2015 10:04*

The one that came with it, unless it's moshi :P


---
**Stephane Buisson** *December 28, 2015 10:34*

Welcome here Dave,



before replying to your question, please understand how this community work.

please do not make 3 posts in row in less than 5 mn, it's not nice to read, give work to the moderator, and not easy to organize as archive. You may not have seen but we try to organize the interesting info into categories, for newcommers like you to find answer to your questions.

Most of your questions are already answered, if you want to look at.



The K40 come with a proprietary hardware and software, it's not much you can do about it. Except to change the controller, then you will be able to go for open source software.

it doesn't cut carbon fiber.

please read mod section for K40 improvements.



I am deleting your 2 other posts, as you will be able to find all your answers on this one.


---
**Gary McKinnon** *December 28, 2015 18:14*

**+Stephane Buisson** How would you feel about having a website/forum for this. The right software would make your life a lot easier, vbulletin or whatever? I could help with costs, it's my work so i get cheap deals on hosting, domain names etc. Communities doesn't lend itself well to structured discussion.




---
**Dave Patmore** *December 28, 2015 19:28*

hi thanks for the replys

so the main board determines what software can be used, it came with

corell laser

laser draw

i downloaded laser works as i found a few tutorials on it,

i have not as yet set the laser up i have to make room in my shed


---
**Tony Schelts** *December 29, 2015 09:59*

I have had my K40 for a few months now, and I have found the Corellaser works verywell. as long as when you set the laser profle its M2 not M1.. Thanks to who ever told me that made loads of differenct.  


---
**Bryon Miller** *December 30, 2015 19:30*

Mine has a moshi board in it, the software is practically useless.  It crashes all the time, has tons of floating point errors and the support is the worst I've ever seen.  Not for lack of trying, but the language barrier creates laughable support at best.  I can't understand what the hell they're saying in their manual, nothing makes any sense.  There are english words, but no sense being made.


---
*Imported from [Google+](https://plus.google.com/106320656164461253905/posts/LWJBx4frPmp) &mdash; content and formatting may not be reliable*
