---
layout: post
title: "HELP NEEDED - since I got my crappy K40 laser I have had nothing but trouble, after 2 weeks the power would only work on a narrow band of the pot otherwise the laser would cut out"
date: August 04, 2016 15:55
category: "Hardware and Laser settings"
author: "David Coxon"
---
HELP NEEDED - since I got my crappy K40 laser I have had nothing but trouble, after 2 weeks the power would only work on a narrow band of the pot otherwise the laser would cut out. Then THOR took up presence in my power supply and when i lifted the lid there were sparks like lighting inside the power supply before it blew in a awsom manner tripping out the mains breakers, the company in china refunded me the cost of a new power supply which took 10 days to arrive.



Since then the laser has only worked on around 7-10mA setting and it wont cut through 3mm plywood any more - now it seems to be getting worse on works on a narrower range of the pot. I changed the pot out for a new one, brough a new mother board and dongle and still the laser wont fire above 7mA and cuts out any lower. I changed the wires over from the power supply through to the motherboard and to the pot and nothing seems to work on getting the laser to operate on any other setting of the pot.



I was just about cutting through the plywood an now it just wont cut through to the back no matter how many times I over cut the design. Seems like the replacement power supply is going the way the first one did only working just above stall limit. What can i do to re-wire the laser so it works properly between 0-20mA on the meter and works at proper settings.



You would think a new power supply and new pot would allow a test fire to work. I have  a replace DSP motherboard i want to plug in but if the laser wont fire on moving the pot I dont want to plug it in and damage the new board. Any suggestions as to why it will only work on 5% of the dial and not fire on anything above 7mA





**"David Coxon"**

---
---
**Eric Flynn** *August 04, 2016 16:25*

It could be the tube. Is it not firing at all above 7ma, or just not lasing? Do you see plasma in the tube above 7ma?   3 potential failure modes. The new supply is also bad, the tube is bad , or you have a failure in the HV wire.


---
**David Coxon** *August 04, 2016 16:36*

The laser is cutting out but doesn't have enough power to chop through 3mmplywood anymore - the dial only works on 5% or the range like it did before it blew up the first motherboard and power supply so the tube is firing under instruction, it just only works on a very narrow range of the dial, changed the pot twice for new components and has no effect


---
**Eric Flynn** *August 04, 2016 17:12*

Yes, I understand what you are saying, but the tube could be the problem ,not the supply.  It could be the HV wire or connections as I said as well.  You are focused on the PSU and pot.  Unless the new PSU and/or pot was also bad , you need to be looking at the tube.


---
**Jon Bruno** *August 04, 2016 17:54*

I agree that if it's behaving as it did before the swap the tube is suspect , but Hang on a sec... Where do you live David? what is the Mains voltage there?

I asked about the MAINS voltage because I myself live in the states and have 110v service. I purchased a replacement LPSU and it was set up for 220V operation. I had to install a jumper on the board to get it to properly work otherwise I would only get 4-7mA out of it with the POT at the full power setting.


---
**Eric Flynn** *August 04, 2016 18:08*

Good point Jon. Newer supplies have a switch for it.  That's not likely the case if at one point his old supply was fine, and it started acting up, and the new one does the same. Sounds like the tube to me.  Worth a shot on the mains switch though.


---
**Jon Bruno** *August 04, 2016 18:12*

unfortunately these cheap-o chinese power supplies rarely come with switches.

Of the 3 supplies I've had only one had a switch.

any way to cut costs.... 

But yeah, I agree.. he probably has an issue with the glassware.


---
**David Coxon** *August 04, 2016 21:54*

**+Jon Bruno** Live in the UK with 240V supply


---
**David Coxon** *August 04, 2016 21:56*

which jumper are you talking about - have you got more details as it seems to be permanently limited - the first PSU was working on the pot before it blew and the second units has always been limited




---
**Jon Bruno** *August 04, 2016 22:26*

It was a bridge in on the board that would normally be attached to a 220/110v selector.

Sans bridge = 220v


---
**David Coxon** *August 06, 2016 18:27*

so can anyone answer the question if the power supply is working and the tube is firing the laser enough to burn the wood and produce smoke why i cant get the pot to work on anything but a small 5% margin of the dial between 5-7MA or the laser cuts out. Checked the power levels on the board provide a stable 5V and 24V and took the tube out and examined closely. Changed the pot for a new 1K resistor unit and checked it reads levels on moving the pot. There seems to be nothing else between the power line, pot and TTL input line so the laser should fire on most of the range of the pot. Disconnected the motherboard and tried test firing and works only on the 5-7ma range. 



Do i just abandon the laser and stop wasting time and money on it as its been a p*** poor machine from the start. 1) new power supply 2) new motherboard 3) new POT - what else need changing apart from the laser machine for a bigger one that works


---
**Eric Flynn** *August 06, 2016 18:46*

We told you.  You are assuming your tube is good.  I would say the tube is bad.  These exact symptoms can arise from a bad tube.  There are several things that can go wrong with a tube to produce these exact symptoms.  It could be contaminated gas , bad electrodes , bad glass/optics that move as it heats at higher currents etc.  I have seen this EXACT behavior on a bad tube myself!!


---
*Imported from [Google+](https://plus.google.com/102140523888140492630/posts/AKV74wNXWBZ) &mdash; content and formatting may not be reliable*
