---
layout: post
title: "Since upgrading the controller from an old Moshiboard i'm now using PWM to control the power and the potentiometer sets the maximum"
date: February 04, 2018 09:30
category: "Discussion"
author: "John Sturgess"
---
Since upgrading the controller from an old Moshiboard i'm now using PWM to control the power and the potentiometer sets the maximum. I noticed a unusual smell when last using the machine and found this on inspection. After the upgrades am i same to remove this from the circuit completely?

![images/bf2887e36c54d20b9100f1a302aa786a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf2887e36c54d20b9100f1a302aa786a.jpeg)



**"John Sturgess"**

---
---
**BEN 3D** *February 04, 2018 10:42*

It looks like the potentiometer (justable resistor) got too much ampere and gets to hot. May it help to replace with a bigger or potentiometer or add a additional resistor in line.


---
**Don Kleinschnitz Jr.** *February 04, 2018 11:58*

I do not think this has anything to do with your upgrade and would not advise removing it until we clearly know its purpose.



My guess: looks like a ballast resistor used on older machines LPS.



How old is this machine?



I need to know how this resistor is connected into the machine to know for sure.


---
**John Sturgess** *February 04, 2018 12:16*

**+Don Kleinschnitz** the machine is from around 2013/14. It is indeed the infamous large green ballast resistor found on earlier machines. Its hooked upto the return side of the tube before or after the Ammeter.



The majority of machines at this time didn't have PWM capable PSUs, my one did and I've always wondered if it was just a throwback to the previous PSUs requirements. 


---
**Don Kleinschnitz Jr.** *February 04, 2018 15:05*

**+John Sturgess** if you have a newer LPS you likely do not need it if its  very old style you do.

Did you put in newer supply and leave that installed?


---
**John Sturgess** *February 04, 2018 16:05*

**+Don Kleinschnitz** here's a picture of the PSU 

![images/b7c715a74790f0df8a419c8a65b18e5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7c715a74790f0df8a419c8a65b18e5b.jpeg)


---
**Don Kleinschnitz Jr.** *February 04, 2018 18:06*

That's look like new vintage.

Where in the circuit is the resistor currently connected?

Can you measure the value of that by the way?




---
**Andy Shilling** *February 04, 2018 21:09*

That is indeed a ballast resistor and you have no need for it now, You have done the exact upgrade I made this time last year.




---
**Fook INGSOC** *February 05, 2018 03:56*

It appears to be a current limiting resistor!...which is a VERY rudimentary way to control/limit the current to a laser diode instead of having a constant current PWM driver circuit!!!


---
**Don Kleinschnitz Jr.** *February 05, 2018 12:15*

**+Fook INGSOC** except for the vintage LPS, these <b>are</b> PWM controlled power supplies. The ballast resistors purpose in the old supply is to stabilize the discharge. 


---
*Imported from [Google+](https://plus.google.com/112856126515261932166/posts/ZFhqyetrNGt) &mdash; content and formatting may not be reliable*
