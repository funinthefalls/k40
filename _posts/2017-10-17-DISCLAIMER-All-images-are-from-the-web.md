---
layout: post
title: "DISCLAIMER: All images are from the web"
date: October 17, 2017 18:13
category: "Hardware and Laser settings"
author: "Bruno Ferrarese"
---
DISCLAIMER: All images are from the web. None are mine.



After a long hiatus I'm back playing with my K40. 



Any idea why I can't get the clean engrave look like the ones on the photos (dough roller, your logo, tiger, coffee house)? All my engraving end up with a black burnt finish like the watch and the gray scale. The stock m2 nano gives me the clean/no burnt look but I can't get that with a different controller. So far I have tested an arduino/mks_sbase/cohesion with both grbl-lpc and smoothie. I always end up with a black engraving. I understand that is what most people are aiming for but its not my case. I want a somewhat deep engrave but not black/burnt.  

Have you been able to acchieve the clean look I'm looking for? If so, how? what did you do? 



![images/ef986c174ccc9265af6d4b5e0009f454.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef986c174ccc9265af6d4b5e0009f454.jpeg)
![images/dadc7fe0f2816c690b48e4e96d298a05.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dadc7fe0f2816c690b48e4e96d298a05.jpeg)
![images/27d98bb641903dafbbfc205a5450d4f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/27d98bb641903dafbbfc205a5450d4f0.jpeg)
![images/c59ffb603502c35086311ed19c1daf53.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c59ffb603502c35086311ed19c1daf53.jpeg)
![images/19155f104e1fd74174f20bb8073bd794.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19155f104e1fd74174f20bb8073bd794.jpeg)
![images/ef21b85d2bc46781d497d8c9269c656b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef21b85d2bc46781d497d8c9269c656b.jpeg)

**"Bruno Ferrarese"**

---
---
**Jeff Lamb** *October 17, 2017 18:19*

I normally find that a high power and fast speed give me depth and brown whilst lower power and low speed gives a shallower darker engraving.


---
**Bruno Ferrarese** *October 17, 2017 19:26*

**+Jeff Lamb** What you consider a fast speed? I'm using 350/20% (3ma) and still get the black engraving. On the smoothie I have tried 100 / 150 /200 us. On the grbl side I have tried from 2KHz to 10KHz. All ended up with a black engraving. I think I covered all variables but clearly I'm failing somewhere.  


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 20:17*

Is the question about the amount of grayscale contrast? Your top row of the grid looks fairly decent.  On smoothie you should try 200 (C3D default) or 400 as the pwm period in config.txt



For grbl-lpc it is the frequency which is 1/period value. *1000 I guess. 



Sounds like you've tried a bunch of these.  Some K40's respond less to pwm than others. 



Jeff is correct that grayscale is more vivid at lower speeds.  For example, 300+ is definitely on the faster end.  Try 100mm/s.


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 20:19*

Oh, even that last picture is not yours?  You should try doing a variable power raster like that and show the result. 


---
**Bruno Ferrarese** *October 17, 2017 20:26*

**+Ray Kholodovsky** My gray scale <b>very</b> similar to that.   



I don't know if I'm getting it. What you call vivid? dark? blackish? That's what I don't want. I want a deep engrave but not black. Look at the tiger. It is almost like it was carved. That's what I'm looking for. 


---
**Claudio Prezzi** *October 17, 2017 21:58*

To reach deep engrave with less darkening you should use shorter PWM periods like 50-100us on smoothieware (higher PWM frequencies like 10'000-20'000 Hz on grbl-LPC) with fast feedrate, max. 50% power on gcode and high power on the pot. Don't go lower than 50us (or higher than 20kHz on grbl-LPC) because that's about the limit of the laser power supply. 


---
**Jim Fong** *October 17, 2017 22:01*

Material has a big impact on the final outcome. If you use ply material, once you hit a glue layer line, it turns dark.  The pitch from pine boards also burns.  Clear pine lasers nice.  Cherry will laser dark at even low power.  


---
**Claudio Prezzi** *October 17, 2017 22:16*

I guess the tiger was engraved with a diode laser. The 40W CO2 laser probably has too much power.


---
**Jim Fong** *October 18, 2017 03:20*

Various test on pine. You can see how much the wood grain varies. [https://plus.google.com/photos/.](https://plus.google.com/photos/.)..



K40/grbl-lpc Lightburn



II’l try to find a clearer piece of pine. 


---
**Jim Fong** *October 18, 2017 03:20*

![images/6a36fd01ff5c2f41869c12af0dfd7693.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6a36fd01ff5c2f41869c12af0dfd7693.jpeg)


---
**Ned Hill** *October 18, 2017 04:36*

You can also probably clean the engraving with a toothbrush and  a cleaner.  I think I recall people mentioning something like using GoJo hand cleaner perhaps.




---
**Jeff Lamb** *October 18, 2017 10:31*

**+Bruno Ferrarese** Fastest I normally run is about 300mm/s, slowest for engraving tends to be about 95mm/s.  pwm period is set to 400 (I run lpc-grbl but cant remember the converted figure). Hope that helps.


---
**Claudio Prezzi** *October 18, 2017 14:08*

On grbl-LPC you can set the PWM frequency in Hz with $33.


---
**Claudio Prezzi** *October 18, 2017 14:09*

Frequency in Hz = 1 / period in s

(400us ~ 2500 Hz)


---
**Bruno Ferrarese** *October 18, 2017 14:14*

**+Claudio Prezzi** I tried what you said. At 10KHz my laser wont fire. At 9KHz it gives me a better engraving but still very dark.



**+Jim Fong** Thanks for the pictures. I can't get those results. Not close to that. Same speeds/power.



**+Ned Hill** That's a help in having a better final product. Thanks.



What drives me crazy the most is the fact that have I tried whisperer from **+Scorch Works** with the original m2 mano and voila I get the clean deep engrave I'm looking for. 


---
**Jim Fong** *October 18, 2017 15:56*

**+Bruno Ferrarese** I’m using the stock grbl-lpc pwm frequency settings. Not sure what it is.  I can look it up when I get home from work.  The potentiometer is set at 10ma for 100% power.  Although I’m using Lightburn laser software, it would be the same with Laserweb4.  



If the stock M2nano works, that would lead me to believe the other controllers are not correctly varying the power somehow. **+Ray Kholodovsky** would have to look at your wiring to the Cohesion3d board.   Same board I am using.  


---
**Adrian Godwin** *October 21, 2017 16:03*

maybe this is the explanation :




{% include youtubePlayer.html id="pzropA_AcL8" %}
[youtube.com - RDWorks Learning Lab 110 3D Engraving in depth revisit Part 1](https://www.youtube.com/watch?v=pzropA_AcL8)



Russ explains that the relationship between power as a fraction of the laser maximum is not a linear function of the % power as seen in the design. 




---
*Imported from [Google+](https://plus.google.com/111443543748488809183/posts/8QTAmMDjNYj) &mdash; content and formatting may not be reliable*
