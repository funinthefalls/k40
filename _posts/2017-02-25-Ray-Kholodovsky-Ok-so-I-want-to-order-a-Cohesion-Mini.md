---
layout: post
title: "Ray Kholodovsky Ok so I want to order a Cohesion Mini"
date: February 25, 2017 21:41
category: "Smoothieboard Modification"
author: "Steven Whitecoff"
---
Ray Kholodovsky

Ok so I want to order a Cohesion Mini. Question is what do I need to use it in my Nano equipped Kehui 3020 K40 and then transfer it use in a large format setup with external drivers for larger stepper later?

The extra drivers options are for what use?





**"Steven Whitecoff"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 25, 2017 22:01*

This is the bundle to upgrade a K40: 

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)

If you want to try a rotary or z table in the k40 get the 2 additional A4988 drivers. 



Then you get a set of these (2-4 again depending on Z table and rotary): [http://cohesion3d.com/external-stepper-driver-adapter-for-pololu-style-socket/](http://cohesion3d.com/external-stepper-driver-adapter-for-pololu-style-socket/)



And as I may have mentioned I recommend the GLCD adapter so that you can use a "reprap glcd 12864" which you can get on Amazon. 



We also carry endstops if you need those for your large build. 


---
**Steven Whitecoff** *February 26, 2017 03:51*

Thanks, its as I thought- I need almost everything you make motion control wise. Ordering now...but I see many GLCDs most of which say for RAMPS....do I need a specific one or not?


---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 04:01*

Any of those. ![images/68896e450cd8f2ebe02efcee1907e548.png](https://gitlab.com/funinthefalls/k40/raw/master/images/68896e450cd8f2ebe02efcee1907e548.png)


---
**Steven Whitecoff** *February 26, 2017 04:04*

Well right in the middle of the order, I get bad gateway and now the site appears to be down....guess I broke it :D


---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 04:09*

Looks like my storefront provider, bigcommerce, is having an outage. Wonderful. 

I'll keep an eye on it, as I can't load my store page right now either. Sorry! Please give ordering another try tomorrow. 


---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 04:10*

Or right now, as I just got the store to load again. Might be finicky for a while. 


---
**Steven Whitecoff** *February 26, 2017 04:40*

As you said,it just got finicky, I was able to register then complete the order. Thanks nothing like Saturday night support to make one feel good about buying from someone. 

FWIW, the large format project looks better and better as get into it, so I might have to get a second board as I've decided to turn the K40 into a CNC router(dremel size) and I guess using the same software for both makes more sense than struggling with Corels shortcoming so I'm likely to buy another mini from you.


---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 04:47*

Saw the order, thanks for your support!

Sounds cool. You've actually given me some thoughts about making a larger laser. Like right now I'm making some printers from MDF (see this post: [https://plus.google.com/+RayKholodovsky/posts/6GcudiL7wG4](https://plus.google.com/+RayKholodovsky/posts/6GcudiL7wG4) ) and each part is more like 400mm wide so the K40 size is quite useless for me. It's printers all over again, most of the stuff I do needs a 200x200mm bed and then I break out my 300x300 custom printers for the larger stuff. 

Of course a large laser build is quite the undertaking and time + capital expenditure so I'll wait to see what you come up with. Take lots of pics! :)


---
**Steven Whitecoff** *February 27, 2017 04:28*

Ray, heres where I need your help- in looking at mechanical systems it appears the two major versions I'm considering have a belt reduction of the stepper to motion belts. One machine has a modest reduction(1:2?) and the other more(1:3 or 4). That means all else being equal, less travel per step = more resolution. Question is can all that be configured to match including steppers at 1.2 and 1.8 deg/step? Does your board work with 2 and 3 phase or is that all done in the driver electronics and a step is a step?Are there limits to maximum steps? looking at 48k steps at 1000/inch.Same question for LaserWeb etc.


---
**Steven Whitecoff** *February 27, 2017 04:31*

FWIW, while way less expensive than any thing Ive seen, this project will run about $1500 plus the K40. That all the stuff you cant conviently make at home. Still have to deal with enclosure, vent, air, cooling, controls. I guess the GLCD will help with that.




---
**Ray Kholodovsky (Cohesion3D)** *February 27, 2017 04:56*

Steven, here's the short answer: 

In smoothie you specify steps per mm. That's it. That value is based on all the configurations you've specified. 

If you're using external drivers you will tell them what microstep amount you want, and then you'll adjust your steps/ mm accordingly. 

Anything along the lines of 2/ 3 phase sounds like a servo and again is entirely controller dependent. 

The theoretical step generation limit of smoothie is 100khz. Up to 90khz is good in practice. Jim Fong has tested this and posted in this group. 

LaserWeb has nothing to do with this, it just sends the gcode to the board. 


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/3fRja9AXSky) &mdash; content and formatting may not be reliable*
