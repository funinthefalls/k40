---
layout: post
title: "This might be an easy (or at least cheap) start to an LED base for an edge lit design"
date: March 09, 2016 03:51
category: "Materials and settings"
author: "Stuart Rubin"
---
This might be an easy (or at least cheap) start to an LED base for an edge lit design.

[http://hackaday.com/2016/03/08/turn-a-free-flashlight-into-led-strips/](http://hackaday.com/2016/03/08/turn-a-free-flashlight-into-led-strips/)





**"Stuart Rubin"**

---
---
**HP Persson** *March 09, 2016 15:21*

Cool, have a light like that in the car. Have to try this :)


---
*Imported from [Google+](https://plus.google.com/104069580748008438325/posts/dPBuMrk5boS) &mdash; content and formatting may not be reliable*
