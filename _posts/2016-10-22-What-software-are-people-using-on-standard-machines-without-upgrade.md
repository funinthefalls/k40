---
layout: post
title: "What software are people using, on standard machines without upgrade"
date: October 22, 2016 22:43
category: "Discussion"
author: "Tony Schelts"
---
What software are people using, on standard machines without upgrade.  what

is Coredraw x12 like to use?







**"Tony Schelts"**

---
---
**Ariel Yahni (UniKpty)** *October 22, 2016 23:43*

All drawing software are very similar and can produce the same result. At the end it's all about preference. In the stock case you a tide to Corel as the plugging runs there but it does not suggest you cannot draw in inkscape and load the file in corel


---
**Jim Hatch** *October 23, 2016 00:29*

I'm either using Corel X8 or AI CS3 (never upgraded past that) depending on which laser I'm going to use. Corel is my go-to software but sometimes I need AI to get a good DXF for the 60W laser because that requires it's software - LaserCut which sucks but can import older DXF formats.


---
**K** *October 23, 2016 03:18*

Illustrator and Photoshop for all design work then import into Corel to cut/engrave. X12 is not a bad program though, like Ariel says, it's mostly about preference. 


---
**Tony Schelts** *October 23, 2016 08:22*

x12 says 15 days usage unless I register.  If i register will it still work


---
**Jim Hatch** *October 23, 2016 20:49*

It's pirated software on the CD. There's a license key text file too - that's got the license # that works. Put that in and skip registering. Should take care of you.



Or buy a licensed copy off eBay. You can find older versions (than X8 which is the latest) that are new in box for decent prices.




---
**Tony Schelts** *October 23, 2016 23:27*

Thanks guys


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/UPmhQY9v8Fi) &mdash; content and formatting may not be reliable*
