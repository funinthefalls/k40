---
layout: post
title: "Hi K40 team! I am in the process of building a VERY manual Z table"
date: April 15, 2016 08:04
category: "Modification"
author: "Anthony Santoro"
---
Hi K40 team! 

I am in the process of building a VERY manual Z table. When I say VERY manual, I mean that I'm going to remove the table in the machine and chock up the material to be cut with some wooden blocks. When I want to cut something thicker, I'll take blocks away, thinner, I'll add blocks... High tech.

Is there a way for me to work out the optimal focal point for my laser? 





**"Anthony Santoro"**

---
---
**Scott Marshall** *April 15, 2016 13:43*

If it's stock, it's 50.8mm or 2" from the lens +- a mm or so.


---
**Don Kleinschnitz Jr.** *April 15, 2016 15:55*

Do a ramp test (google it).

 Put a piece of material on an angle on the bed (I use fiber board). Prop up one side. Use the laser to mark a line horizontally along that material with the lowest energy that will make a legible mark. Look at the results and you can see where the line is thinnest. 

Considering the angle that the wood is at you can determine what that distance is from the head that gives you the best results. 

I measured that distance with a micrometer by marking it across the wood with a pencil and then put the angled assy on a table and measured the elevation at that point with a micrometer. Then subtracted that value from the distance from the lens to the head.


---
**Anthony Santoro** *April 23, 2016 07:09*

Thanks for the tips guys. Do you recommend the focal point to be at the top, middle or bottom of the material. E.g. If I'm cutting 3mm acrylic should I have the focal point at the top of the piece (top = 50.8mm from lens) or bottom (top=47.8mm from lens)? 


---
**Don Kleinschnitz Jr.** *May 03, 2016 12:58*

On the top, somewhere there is an post on here linked to advice on cutting acrylic and unlike most other advice I have seen it suggests to focus on the top of acrylic as it flows as it cuts. On other materials it should be in the middle.


---
*Imported from [Google+](https://plus.google.com/106649411786258336023/posts/Yvo8Yaajf1h) &mdash; content and formatting may not be reliable*
