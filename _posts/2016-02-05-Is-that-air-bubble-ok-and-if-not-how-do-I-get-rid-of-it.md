---
layout: post
title: "Is that air bubble ok and if not how do I get rid of it"
date: February 05, 2016 02:34
category: "Discussion"
author: "3D Laser"
---
Is that air bubble ok and if not how do I get rid of it

![images/57f96ba418ababe9a2d27ed364df86e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57f96ba418ababe9a2d27ed364df86e1.jpeg)



**"3D Laser"**

---
---
**Jose A. S** *February 05, 2016 03:08*

Those bubbles dissapear with the flow itself..


---
**Andrew ONeal (Andy-drew)** *February 05, 2016 03:53*

Is the machine level and more importantly is the laser level? I had and still get bubbles like this on the exit end. I would have to lift laser on outlet side then maneuver until all bubbles passed, if necessary turn off water pump letting water recess about mid way then restart pump then repeat as necessary. To aid in this issue I now use a heavy duty mail clip to kink the outlet hose until next use. After clipping water line I power down, there are more solutions to solve this in a way that is electronic in nature.


---
**I Laser** *February 05, 2016 03:54*

As Andrew said try raising the machine on an angle to help the air escape. My water tubing sits submerged in my tank, so air is never reintroduced.


---
**Andrew ONeal (Andy-drew)** *February 05, 2016 04:07*

Lol never took that into consideration man. Lmao, just when I think I thought of every angle, I happen to learn more. Awesome, love this community and all the knowledge within. Note, extend outlet hose to submerge.


---
**Phillip Conroy** *February 05, 2016 04:12*

Just make sure water tank is at same level as laser cutter that wAy water does not drain out when pump is off,i like to see water going into  ta k from laser tube to check flow rates,i also have a water flow switch on the pump that does not alow the laser to fire with out water flowing


---
**Phillip Conroy** *February 05, 2016 04:16*

i can no longer see into my laser tube as it is coated in mdf dust after about 30 hours cutting mdf,just upgraded extraction fan for the second time to a 8inch 25 cubic meters a minute ,that moves a lot of air about 10 times stock fan


---
**Andrew ONeal (Andy-drew)** *February 05, 2016 04:21*

I've had mine for a few months but didn't take into account all the design aspects or photo editing. I definitely underestimated the skill needed to take an image in inkskape then generate code. I still don't know how in fact, I'm still just testing with no completed projects. Very very frustrating.


---
**3D Laser** *February 05, 2016 04:28*

**+Phillip Conroy** what did you upgrade to I am worried that my exhaust has to far to travel for the fan to be real effective


---
**Stephane Buisson** *February 05, 2016 10:03*

Air bubble danger is if the metal ring isn't cooled properly, with metal expansion, it will crack the tube. a small bubble not touching the metal ring could be ok.


---
**I Laser** *February 05, 2016 10:42*

It's so not worth it, considering how easy it is to get all the air out!



Corey just search this forum for the amount of people that have screwed their tube because of air or heat!



As for your exhaust, how long is you pipe? I originally had the stock fan and a pc fan in an enclosure to up the pressure, it pushed about 3 metres no problems!



edit: glad I was of help **+Andrew ONeal** 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/Yc7y15bo6fZ) &mdash; content and formatting may not be reliable*
