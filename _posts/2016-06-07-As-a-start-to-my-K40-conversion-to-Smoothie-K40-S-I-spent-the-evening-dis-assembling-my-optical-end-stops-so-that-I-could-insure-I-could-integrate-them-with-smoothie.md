---
layout: post
title: "As a start to my K40 conversion to Smoothie (K40-S) I spent the evening dis-assembling my optical end stops so that I could insure I could integrate them with smoothie"
date: June 07, 2016 04:47
category: "Modification"
author: "Don Kleinschnitz Jr."
---
As a start to my K40 conversion to Smoothie (K40-S) I spent the evening dis-assembling my optical end stops so that I could insure I could integrate them with smoothie. Tos date I could not find any info on the optical stops on my machine.



Details on my blog here: [http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)



If you look at my initial findings and see anything wrong please let me know. Otherwise I will find out when I start testing :).



Looks pretty straightforward, open collector output, just need to insure that the middleman board or smoothie has a pull-up and low true detection. 

The optical sensor is a TCST1030, with a max Ic rating of 100 ma.







**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 07, 2016 07:51*

It's great that you are thoroughly documenting your transition. I can't help out, but I will be keeping my eyes on your posts for my future reference. Thanks Don.


---
**Brian Bland** *June 07, 2016 11:48*

I didn't have any problem hooking my optical endstops up to Smoothie.   I will post later how I hooked them up. 


---
**Don Kleinschnitz Jr.** *June 07, 2016 12:43*

**+Brian Bland** that would be great ...


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/L99FncbX91f) &mdash; content and formatting may not be reliable*
