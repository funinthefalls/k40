---
layout: post
title: "Hi guys, I'm running into a strange issue where cutting and engraving doesn't line up"
date: August 23, 2017 08:16
category: "Original software and hardware issues"
author: "Luke Roberts"
---
Hi guys, I'm running into a strange issue where cutting and engraving doesn't line up. I'm using the stock M2 board with CorelLaser.

As seen in the photo the further from the origin I get the bigger the difference. DPI is set at 1000 (default).

When I use the K40 whisperer software they line up perfectly so it's not a hardware issue. Any help would be appreciated. Cheers.

![images/4f981d5b584c1fa9ee79db2b0b7cbb18.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f981d5b584c1fa9ee79db2b0b7cbb18.jpeg)



**"Luke Roberts"**

---
---
**Ned Hill** *August 23, 2017 16:57*

What do you have the output file types set to in "CorelDraw Settings" for engraving and cutting?  Also what version of CorelDraw are you using?


---
**Luke Roberts** *August 23, 2017 21:18*

I'm using CorelDraw X8 and the settings are WMF for engraving and PLT - HP-GL/2 plotter file for cutting. It's the cutting that is the incorrect dimension.

When I set cutting to WMF it's perfect but now it repeats lines twice, even if it's just a hairline. Any suggestions? Thanks for taking the time.


---
**Ned Hill** *August 23, 2017 21:29*

Need to manually  make line widths to 0.01mm or less for cutting or it does the double cut.   


---
**Ned Hill** *August 23, 2017 21:37*

WMF is right for cutting.  For X8 WMF is typically fine for engraving but I would actually recommend BMP for engraving as there are occasional issues that crop up with WMF engraving.


---
**Luke Roberts** *August 23, 2017 21:46*

Perfect. Thanks for that. I've always used hairline as I thought that was small enough for cutting but apparently not! Cheers mate.


---
*Imported from [Google+](https://plus.google.com/105203594248067813781/posts/AmKzsCf5cLG) &mdash; content and formatting may not be reliable*
