---
layout: post
title: "As I am limited space wise would this mini air compressor be any good?"
date: October 01, 2016 10:56
category: "Air Assist"
author: "J DS"
---
As I am limited space wise would this mini air compressor be any good?







**"J DS"**

---
---
**Anthony Bolgar** *October 01, 2016 11:07*

As long as it does not overheat. Not all airbrush compressors are made to run continuous duty. Some are, but not usually the cheap ones.


---
**Don Kleinschnitz Jr.** *October 01, 2016 11:35*

Her is an alternative that I am looking at as recommended on this forum. [amazon.com - Amazon.com: EcoPlus 728457 5 to 80W Single Outlet Commercial Air Pump, 1300 GPH: Pet Supplies](https://www.amazon.com/gp/product/B002JLGJVM/ref=crt_ewc_title_huc_2?ie=UTF8&smid=A3N1ITIX788DZC&th=1)


---
**J DS** *October 01, 2016 11:43*

thank you 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 01, 2016 12:23*

Looks like almost the exact same air-compressor that I purchased from ebay. Not really worth it or suitable for air-assist in my opinion. I think Don's option is likely much better suited.


---
**greg greene** *October 01, 2016 12:31*

I got the one Don got - works good - but a little noisy - needs a rubber mount else it 'walks' all over the place as it vibrates.


---
**Gunnar Stefansson** *October 01, 2016 14:42*

I use a similar air assist compressor with dual pumps, it's super silent and doesn't run hot even after 1½hour continuous use! Size wize I would probably go for **+Don Kleinschnitz** version... 




---
**Phillip Conroy** *October 02, 2016 04:36*

It all depends on what you use laser for if only engraving these small pumps are fine,if cutting 3mm mdf all day then bigger the better-shop 2 1/2 hp 50 liter tank pump for me ,i use 10 psi -watch out for mositure  condensing in air lines  if pump has a tankxxxxxxxxxxx 


---
*Imported from [Google+](https://plus.google.com/102322879087562424255/posts/a2KPwPAhDTJ) &mdash; content and formatting may not be reliable*
