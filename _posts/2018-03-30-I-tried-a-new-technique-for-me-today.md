---
layout: post
title: "I tried a new technique for me today"
date: March 30, 2018 04:33
category: "Object produced with laser"
author: "Steve Good"
---
I tried a new technique for me today.



I saw a guy do this on a YouTube video and wanted to give it a try.

Cast acrylic spray painted black. Engraved at 15% 200mm/s

I'm not sure what product I will eventually make with this technique but I'll think of something.

Does anyone know if laser engraving spray paint is dangerous?



![images/4cf6f2e4698e5cc0f3b0673030d12f11.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4cf6f2e4698e5cc0f3b0673030d12f11.jpeg)



**"Steve Good"**

---
---
**Claudio Prezzi** *March 30, 2018 07:49*

I guess it depends on the type of spray paint. I use acrylic spray paint for sucht things.


---
**Jim Hatch** *March 30, 2018 13:42*

Agree with **+Claudio Prezzi** . The paint when wet contains volatiles you don't want to be sniffing but once dry it's not releasing any problem chemicals (like chlorine) when laser. It's a fairly common technique you're using and no reported problems I've seen. 


---
*Imported from [Google+](https://plus.google.com/106153533504584428744/posts/3uLMHMoJPBZ) &mdash; content and formatting may not be reliable*
