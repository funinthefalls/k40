---
layout: post
title: "Its not at all obvious especially when coming from another laser workflow, but using Corel Laser/Laser Draw requires the bit you expect to keep to be filled black and any holes to be filled white"
date: November 05, 2015 12:01
category: "Discussion"
author: "Phil Willis"
---
Its not at all obvious especially when coming from another laser workflow, but using Corel Laser/Laser Draw requires the bit you expect to keep to be filled black and any holes to be filled white.



ie the image on the left will produce the output on the right, with a single cut on the edge of the black fill. Also ensure the inside option is ticked so that the holes are cut first before the outline.



Hope this helps someone.



![images/499ca74892f8a5f55b46e0df85a437c1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/499ca74892f8a5f55b46e0df85a437c1.png)
![images/3fd363546b48dfbf0933ca995129ea1a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3fd363546b48dfbf0933ca995129ea1a.jpeg)

**"Phil Willis"**

---
---
**Stephane Buisson** *November 05, 2015 13:04*

You understand now why I choose another workflow.

in Visicut, you can choose to work with colors or stroke width (cutting marking engraving), very efficient.


---
*Imported from [Google+](https://plus.google.com/+PhilWillisAtHome/posts/feaWiraAFx3) &mdash; content and formatting may not be reliable*
