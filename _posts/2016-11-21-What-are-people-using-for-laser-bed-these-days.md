---
layout: post
title: "What are people using for laser bed these days"
date: November 21, 2016 09:56
category: "Discussion"
author: "Tony Schelts"
---
What are people using for laser bed these days.  Honeycomb. Stats??





**"Tony Schelts"**

---
---
**Don Kleinschnitz Jr.** *November 21, 2016 12:53*

I use a LO lift table and these:

[https://www.amazon.com/gp/product/B00LAYPNIY/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00LAYPNIY/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**K** *November 21, 2016 18:16*

**+Don Kleinschnitz** Have you found them to be consistent in height?


---
**Don Kleinschnitz Jr.** *November 21, 2016 18:37*

The studs yes, the table... best I can tell. 


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/H9ZvE2PMTBK) &mdash; content and formatting may not be reliable*
