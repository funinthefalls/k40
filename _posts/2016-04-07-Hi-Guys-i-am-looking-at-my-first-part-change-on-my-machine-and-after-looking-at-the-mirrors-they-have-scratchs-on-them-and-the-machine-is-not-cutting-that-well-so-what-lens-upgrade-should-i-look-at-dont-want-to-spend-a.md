---
layout: post
title: "Hi Guys i am looking at my first part change on my machine and after looking at the mirror's they have scratch's on them and the machine is not cutting that well so what lens upgrade should i look at don't want to spend a"
date: April 07, 2016 18:04
category: "Discussion"
author: "Dennis Fuente"
---
Hi Guys i am looking at my first part change on my machine and after looking at the mirror's they have scratch's on them and the machine is not cutting that well so what lens upgrade should i look at don't want to spend a bunch of money as this is a hobby thing for me.



Dennis  





**"Dennis Fuente"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 18:27*

Here's some options from LightObjects.

[http://www.lightobject.com/Lens-Mirror-C16.aspx](http://www.lightobject.com/Lens-Mirror-C16.aspx)



Depending on what exactly is scratched, I would just replace that part on it's own (since you mention it is just hobby for you).



However, I am fairly sure you can get sets of lens/mirrors on ebay as well. The mirrors are usually Mo (molybdenum) & the lens ZnSe (Zinc Selenide).


---
**Dennis Fuente** *April 07, 2016 20:47*

Yuusuf 

I have been looking on ebay and light objects for lens and mirrors, question all that i have seen are larger then the standard part what dose that mean i have to change the holding fixture as well whats the story on the light objects air assist lens fixture it takes a larger lens would this change the power of the laser would it be better or worse



Dennis


---
**Dennis Fuente** *April 07, 2016 20:52*

wow looking at light object sure can spend some money on mirrors and lens 


---
**I Laser** *April 07, 2016 22:44*

Can't remember the details but the standard lens holder accommodates a smaller lens than the light objects air assist head. The light objects head is 18mm, the supplied lenses are 12.5mm if I remember correctly.



I've bought a few lenses off ebay from saitecutter ([http://www.ebay.com/usr/saite_cutter](http://www.ebay.com/usr/saite_cutter)) cheaper than lightobjects and good quality.


---
**Dennis Fuente** *April 07, 2016 23:26*

Thanks for the response i will take a look at them



Dennis 


---
**Anthony Bolgar** *April 08, 2016 09:21*

I use saite cutter as well1. Decent quality


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/7fQewCX6XH8) &mdash; content and formatting may not be reliable*
