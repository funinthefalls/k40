---
layout: post
title: "I've been feverishly working on upgrades to my k40, like a new exhaust system, etc"
date: December 23, 2017 05:31
category: "Discussion"
author: "James Lilly"
---
I've been feverishly working on upgrades to my k40, like a new exhaust system, etc. before I "really" use it.  The machine is practically brand new with maybe 10 minutes of use.  I finally finished my exhaust and want to test it.  When I fired up the machine I get no beam at all.  Everything else seems to turn on okay, lights, displays xy motors.  I thought maybe it was out of alignment , but when I press the laser test fire  button I get nothing.  Only thing I've really done is move it about 3 feet on my bench. Ideas where to start?





**"James Lilly"**

---
---
**James Lilly** *December 23, 2017 06:35*

Okay-When I press the small "test" button on the motherboard(?) connected to the power supply, the tube does fire.  All of the displays on the control panel light up.  The power buttons all change up and down with the corresponding buttons.  The laser test button lights up, but the tube does not fire when press the test fire button, nor will it fire when I send a file from the computer.  I'm somewhat electronically challenged.


---
**Anthony Bolgar** *December 23, 2017 07:35*

If there is a flow meter installed, make sure the cooling water is working




---
**Phillip Conroy** *December 23, 2017 07:51*

And lid closed if safety switch fitted 


---
**Printin Addiction** *December 23, 2017 14:18*

Should post an image of the control panel, as there are a few variances out there.


---
**Jim Hatch** *December 23, 2017 14:51*

Do you have the laser switch on too? (Some have a second switch that enables the laser itself, otherwise it won't fire. It's what passes for a safety interlock.)


---
**Phillip Conroy** *December 23, 2017 18:11*

Also check that the switches all work,on my k40 the laser enable was a pushbutton switch ,however instead of push on push off it needed to be help in to work


---
**James Lilly** *December 23, 2017 18:51*

Thanks for the suggestions.  



The machine does have the water break protection, so I did remove it to see if it was "stuck". Water did/does flow through it, I thought it might give a false signal. I gave it a little cleaning, but same thing (nothing) when I re-installed it.  Anyway to by pass it?  I've tried it unplugged, same result.



So here is the model pretty much that I bought:



[ebay.com - Details about Upgraded 40w CO2 Laser Engraving Cutting Machine Cutter Water-Break Protection](https://www.ebay.com/itm/Upgraded-40w-CO2-Laser-Engraving-Cutting-Machine-Cutter-Water-Break-Protection/201932489498?epid=2098244650&hash=item2f041d431a:g:tOYAAOSwuxFYx4TQ)




---
**James Lilly** *December 23, 2017 18:53*

Here's the control panel

![images/96fb7942e840916ecd533acd93c9e135.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96fb7942e840916ecd533acd93c9e135.jpeg)


---
**James Lilly** *December 23, 2017 18:53*

On

![images/e49af8c6fc3cbf0519eff7e59937cbc4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e49af8c6fc3cbf0519eff7e59937cbc4.jpeg)


---
**James Lilly** *December 23, 2017 18:54*

Inside

![images/efe36ffcd54292bcff5a71e6e60ae58c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/efe36ffcd54292bcff5a71e6e60ae58c.jpeg)


---
**James Lilly** *December 23, 2017 18:55*

Power supply, etc. Black wires are the water-break protection.

![images/741d516b7e56c79e7094c8b187913870.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/741d516b7e56c79e7094c8b187913870.jpeg)


---
**James Lilly** *December 23, 2017 18:56*

Water-Break protection unit.

![images/de474cf11db8c10bdc578d98d356cd0a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de474cf11db8c10bdc578d98d356cd0a.jpeg)


---
**Anthony Bolgar** *December 23, 2017 18:58*

To test the signal from the flow meter just remove the flow meter wires from the power supply and jumper the WP pins. If it fires then you have a bad flow meter


---
**James Lilly** *December 23, 2017 18:59*

No safety switches on the lids.  I can adjust the power on the control unit, that is If I hit the test button on the mother board on 12% I can see the difference between lets say 25%, so that part seems to function.


---
**James Lilly** *December 23, 2017 19:01*

**+Anthony Bolgar** Is there a jumper device I should get?


---
**Anthony Bolgar** *December 23, 2017 19:26*

You can use a piece of wire.


---
**James Lilly** *December 23, 2017 19:32*

Thanks, I'll find something


---
**James Lilly** *December 23, 2017 23:59*

**+Anthony Bolgar** Anthony - Your troubleshooting was correct.  The Water-Break unit did fail.  The jumper did produce a beam.  Thanks!  I'll investigate the unit.  One mistake I've made is not using distilled water. I noticed some residue in my bucket perhaps that has compromised its function. Another is the unit itself, a simple magnetic device with some sort of flow flap inside. Maybe it got shifted a little when I moved the laser.  It was zip tied to holders glued to the bottom of the case, and not very stable to begin with. The holes that hold the wired piece to the cooling tube are actually a short slot so apparently there is a little side to side adjustment built into the device.  If I'm inspired I'll play with it.  I like the Idea of a fail safe if the water gets low, leaks or whatever.  If I can fix it (which is not likely judging by its quality) great, if not I'll find some sort of replacement. Once again, I thank everyone on the site for you suggestions and soon I can post my new exhaust system.


---
**Anthony Bolgar** *December 25, 2017 22:16*

Glad I was able to help.


---
*Imported from [Google+](https://plus.google.com/114642009939368433927/posts/9TNtKKhCE4B) &mdash; content and formatting may not be reliable*
