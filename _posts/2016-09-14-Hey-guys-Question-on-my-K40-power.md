---
layout: post
title: "Hey guys, Question on my K40 power"
date: September 14, 2016 20:21
category: "Original software and hardware issues"
author: "Gage Toschlog"
---
Hey guys,



Question on my K40 power. I have noticed that I seem to get deeper cuts at about 50% power than anything above that. Any ideas what may be causing that? It's taking me many passes (5+) at slow speeds to cut 1/4" acrylic at this half power setting.







**"Gage Toschlog"**

---
---
**Ariel Yahni (UniKpty)** *September 14, 2016 21:33*

Something wrong there. How much mA is that 50%? Having a good beam alignment  will let you do that faster at about 10mA in about 3 passes


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 14, 2016 21:42*

Also focus, do a ramp test to check your optimal focus point. Set the cutting bed to have the optimal focus halfway into the material thickness.


---
**Scott Marshall** *September 14, 2016 23:41*

It could be low water flow dropping efficiency.  If the flow is low, water can allow local heating of the tube and drop output. The harder you run it, the greater the effect. It's none too good for the tube either. From what I read tube efficiency drops quickly in just 15 or 20 degrees difference (I've never tested it personally, but I see the difference when my water warms up)



You can do a quick test and run the return line into a gallon jug. A normal stock system puts out about 1/2 to 3/4 gpm, which is a little low, but seems to do the job in most setups. It doesn't hurt to increase it any way you can. make sure the inlet screen is clean (they get plugged with scum) and there's no kinks etc in the line. Run the return line all the way back down to the tank level, that cuts off a couple feet of head by using gravity (you have to push the water up, use the stored energy by letting it 'pull' on the water - think of a siphon helping your pump - free horsepower.... If the water is cascading down the distance, it's getting back, but not 'pulling' on the water and returning it's stored potential energy to the system. It's the high school physics Kinetic (moving) energy to Potential (stored) energy and back again. Gotta love science.



All little things, but they add up. A "tune up can gain you 25% or more flow for free.



If you find the flow is very low, get a new pump, the laser tube will suffer soon.


---
**Gage Toschlog** *September 15, 2016 11:58*

Ok, thanks guys. I will give your suggestions a shot. Thanks for the wisdom.


---
*Imported from [Google+](https://plus.google.com/105155515364049131807/posts/FqoUbpZHWdv) &mdash; content and formatting may not be reliable*
