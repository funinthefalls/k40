---
layout: post
title: "Afternoon, am I glad I found this group"
date: September 09, 2016 15:29
category: "Discussion"
author: "J DS"
---
Afternoon, am I glad I found this group. My K40 came yesterday and usually I just unbox and bugger about seeing what I can do with new gadgets. 



This how ever I have taken my time and read as much as I could and just set it up some 30 hours after it arrived. I usually am not too phased with stuff but this is stressing me out and Ive not even switched the bugger on yet. Ive installed the software hopefully! now trying to suss coral out and the coral laser thing comes up 100% Chinese text. 



Im having a good read on here before I switch it on





**"J DS"**

---
---
**Ariel Yahni (UniKpty)** *September 09, 2016 15:37*

Welcome, read, read and then read again. I mean read here and not the included manual. 


---
**J DS** *September 09, 2016 15:38*

Many thanks the manual is going to be the first thing I learn to cut with :-D


---
**Ariel Yahni (UniKpty)** *September 09, 2016 15:39*

I've could not nave said it better, exelent test material


---
**Jim Hatch** *September 09, 2016 16:16*

Remember you're harnessing the power of the sun 😀


---
**J DS** *September 09, 2016 18:18*

More like dark side of the moon at the moment






---
**Randy Draves** *September 09, 2016 21:37*

I too am a newbie like yourself. The most important thing I've learned so far is to get your mirrors aligned correctly. I follow the instructions in the manual and got it somewhat close but still it didn't work very good. I did find however this pdf online that's helped me out a lot and I suggest you read it. 



[http://www.google.com/url?sa=t&source=web&cd=2&ved=0ahUKEwiDz-mynYPPAhUE4WMKHX_QDJ4QFggeMAE&url=http%3A%2F%2Fwww.floatingwombat.me.uk%2Fwordpress%2Fwp-content%2Fuploads%2F2015%2F02%2FK40-Alignment-Instructions.pdf&usg=AFQjCNHQL0UI0i6FTTHE3jdJ8BeBsekmjQ&sig2=r69wubFPV1akPuknXn49Sg](http://www.google.com/url?sa=t&source=web&cd=2&ved=0ahUKEwiDz-mynYPPAhUE4WMKHX_QDJ4QFggeMAE&url=http%3A%2F%2Fwww.floatingwombat.me.uk%2Fwordpress%2Fwp-content%2Fuploads%2F2015%2F02%2FK40-Alignment-Instructions.pdf&usg=AFQjCNHQL0UI0i6FTTHE3jdJ8BeBsekmjQ&sig2=r69wubFPV1akPuknXn49Sg)



I was getting frustrated and thinking it was an operator issue when all along my mirrors were not aligned correctly.  This person's write up really help me out a lot and now my stuff is coming out pretty good.  



Still got alot to learn but the people here have been very helpful and have taught me a lot


---
**Randy Draves** *September 09, 2016 21:54*

Also something to learn from my mistakes be very careful when aiming the mirrors. I thought I was being careful by standing out of the line of fire of the laser when pressing the test button. But my Y mirror was so out of whack that wasn't even close to the Target. I had to use a bigger sheet of paper to find where it was hitting and then adjust. Later on that evening my wife found little burn marks on the bottom of my shirt near the crotchal region. Luckily I already have kids and in a free vasectomy may be good. But this is not for everyone. LOL. I can only assume that it was reflecting off of something and bouncing back at me. Luckily I didn't get hurt but it could have been worse.  Try to keep the lid closed at all times and this will help prevent that from happening.


---
**Ariel Yahni (UniKpty)** *September 09, 2016 22:04*

**+Randy Draves**​ we always say beware you will loose and eye, but a ball!!! 


---
*Imported from [Google+](https://plus.google.com/102322879087562424255/posts/NswE8hL5zz9) &mdash; content and formatting may not be reliable*
