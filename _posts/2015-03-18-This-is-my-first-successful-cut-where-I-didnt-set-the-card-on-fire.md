---
layout: post
title: "This is my first successful cut where I didn't set the card on fire"
date: March 18, 2015 21:15
category: "Object produced with laser"
author: "Eddie Pratt"
---
This is my first successful cut where I didn't set the card on fire. Card is about 160gsm and settings were 6mA power and 34mm/s speed. I put some card underneath to stop back reflections from the metal bed. It took about 6 minutes to cut.

![images/95060c5faf4b5ee38bd37c91a43df42a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/95060c5faf4b5ee38bd37c91a43df42a.jpeg)



**"Eddie Pratt"**

---
---
**Jim Coogan** *March 18, 2015 22:16*

Wow.  Very nicely done and great detail.


---
**Eddie Pratt** *March 18, 2015 22:39*

Thanks Jim :-)


---
**Bee 3D Gifts** *March 21, 2015 02:16*

Beautiful! I was wondering about that...I want to make wedding invitations but I am afraid I will burn them all..lol


---
**Eddie Pratt** *March 21, 2015 08:59*

As far as I can tell, you'll always have burn marks on paper or card. A lot of people seem to use dark materials to hide this. It's also quite a slow process as you have to manually and carefully pick out some of the small pieces of detail that are cut out. Also you have to constantly observe them for fires! I suggest you try a prototype before aggreeing to produce a large run of them!


---
*Imported from [Google+](https://plus.google.com/108440927309352829036/posts/Z6uhZxq7zkd) &mdash; content and formatting may not be reliable*
