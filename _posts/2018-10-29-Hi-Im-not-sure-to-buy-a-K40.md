---
layout: post
title: "Hi, I'm not sure to buy a K40"
date: October 29, 2018 17:54
category: "Software"
author: "Michael Weber"
---
Hi,

I'm not sure to buy a K40. On one hand I'm very excited what nice stuff I could create with this machine. On the other hand I wonder if there are drivers and software to run a stock K40 on a Windows 10 machine.



Any hints?





**"Michael Weber"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 29, 2018 18:22*

They say whisperer should work on win10. 


---
**Mike Meyer** *October 29, 2018 19:16*

I had a laptop with W10 on it and I used Coreldraw12 with the stock drivers. The laptop died, I picked up a W7 machine for 80 bucks on craigslist and it runs the same software.


---
**Michael Weber** *October 29, 2018 19:27*

So Whisperer will work with the stock K40 controller or do I have to change the controller board?


---
**James Rivera** *October 29, 2018 19:37*

**+Michael Weber** I have K40 Whisperer running on my stock K40. You do have to install some USB drivers, but they work fine for me on my Win7 box. The only thing I've updated (so far) is adding an air assist and a new ZnSe lens.  I did buy a #Cohesion3d mini board to upgrade it (and then use #LightBurn software), but I haven't done so yet, simply because K40 Whisperer and Inkscape works well enough for me at the moment. Here is a link with the details about it.

[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
**Michael Weber** *October 29, 2018 20:16*

I like to hear it works with Inkscape. For my cutting plotter I create vectors using Illustrator end import them into Inkscape. That works very well. So I hope a K40 will do so as well :)

Thanks!


---
**Printin Addiction** *October 30, 2018 02:13*

**+James Rivera** I still have my Gerbil Kickstarter in the box, as I have great results, and a good workflow with inkscape and k40 whisperer....


---
*Imported from [Google+](https://plus.google.com/117095824606896746809/posts/affxK8AQyGY) &mdash; content and formatting may not be reliable*
