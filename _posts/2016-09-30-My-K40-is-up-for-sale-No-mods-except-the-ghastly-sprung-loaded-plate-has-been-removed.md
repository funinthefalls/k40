---
layout: post
title: "My K40 is up for sale. No mods except the ghastly sprung loaded plate has been removed"
date: September 30, 2016 11:22
category: "Discussion"
author: "Pete Sobye"
---
My K40 is up for sale. No mods except the ghastly sprung loaded plate has been removed.



A few days ago the tube stopped working so I ordered a new tube. that came today so I installed it and all was well but it does not fire. So instead of spending time and money on trying to find a fault, I will just get a new one from fleabay. So, what I have will be the cabinet with X/Y. NO laser tube. Recent replacement PSU (although that could be the fault!) controller board wih dongle and then all the other bits such as water pump, exhaust pump, tube etc.. Make me an offer and I am afraid it will be collect only as I do not have the original packaging. however, I can break the machine and sell the parts individually.

I am in Cornwall in the UK... Pete 07809 154611 pete.sobye@icloud.com





**"Pete Sobye"**

---
---
**Bryan Hepworth** *October 10, 2016 20:02*

is this still up for grabs?




---
**Pete Sobye** *October 12, 2016 10:38*

Hi Bryan. Sorry, I bought a new PSU (just to try) and that appeared to be the root of the problem. Try cutter is now back in operation so I will be keeping it, for now.




---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/jj8yn2d5XSp) &mdash; content and formatting may not be reliable*
