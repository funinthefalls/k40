---
layout: post
title: "Just a quick question. How long can a tube go without water flow before it blows, assuming the water temp is 20c?"
date: March 10, 2016 11:05
category: "Discussion"
author: "I Laser"
---
Just a quick question. How long can a tube go without water flow before it blows, assuming the water temp is 20c?





**"I Laser"**

---
---
**Anthony Bolgar** *March 10, 2016 14:16*

Give me $400 for a tube and I will let you know.....lol!




---
**Scott Marshall** *March 10, 2016 14:19*

A big danger is the water coming back on and thermal shocking it.

I personally know of 2 people here who have forgotten to turn on the water, then realized it, and quickly turned on the pump, shocking the tube. Easy mistake to make, I could see myself doing it in the panic of the moment. 



I've not blown up a tube yet (knocking on laser engraved wood), but do have some experience with water cooled transmitting gear (very high power vacuum tubes) and my experience is "not long" - especially where you have metal elements fused to the glass, they have dissimilar coefficients of expansion, and the metal elements are the heat source, making the situation even worse. 



I'd say 5-15 secs could easily be fatal at high power settings, to fairly long (minutes) at low power 3-5 ma engraving levels at low duty cycles.

I doubt you get a lot of grace period, which is why I'm upgrading my pump, interlocking with a manual restart (to prevent the thermal shock) both flow and temp.

This is only my "best guess" and I'm sure opinions vary. 



I just received a e-mail ad from Laser Depot and they have Puri tubes on sale very reasonable in the USA.



Hope you don't need one... 



Scott







﻿


---
**I Laser** *March 10, 2016 22:53*

Just to clarify, I stole your idea (Scott) for flow detection. Only issue is, there's a delay of about 7 seconds between flow ceasing and the circuit being broken. The other switch is pretty instantaneous and I'm concerned 7 seconds is too long!



It obviously works perfectly for those occasions where you've forgotten to engage the pump, but if the pumps fails....



Not that I've experienced either, knocking on engraved wood too! :)


---
**Scott Marshall** *March 11, 2016 02:11*

If you're using the Pill Bottle arrangement, just drill the drain hole a touch larger. Mine responds faster than that. I can't time it for you right now, as I've got it all torn apart. I'd be a bit nervous at 7 seconds myself, if the pump failed after 15 minutes of full throttle cutting, it could very well be 2 long.



I'm experimenting with a simple conduction switch, which senses the water conductivity between the 2 brass fittings on either side of my visual paddle flow meter for just that reason. I haven't got there yet though, it's likely going to take some tweaking to get IT right  as well.



I'm going to a bigger pump just because the factory one is too marginal to support any "normal" industrial controls. I'm figuring the 50 bucks will be worth it in the long run. The stock pump only pumps about 1 quart per minute with the stock plumbing circuit, and that's pretty borderline for me. In addition, there's a very noticable temp rise (10+deg F) when using it hard. 

I like Little Giant pumps, they're much more robust, and last forever. March and Teel also make good stuff. I don't think aquarium duty is going to cut it here. Shoot for 4+ LPM ot 1 GPM at 10' of head, and a 5+ psi shutoff. Then you will no longer have to chase bubbles, can use good quality flow switches etc, and have peace of mind your $600 Laser tube isn't depending on a $20 pump.



That's my position on the pump situation. It'sll get you running, but sooner or later it will let you down.



Seriously looking at converting a cheap window A/C unit into a chiller. The 250Watts we need to "burn" are only about 850 BTU, and I can get a cheap room 5000BTU A/C with manual controls (easy to replace with MY controls) for about $120USD delivered.



I have a bunch of ideas coming together, I'll post up details soon. 



[http://rapidtables.com/convert/power/Watt_to_BTU.htm#table](http://rapidtables.com/convert/power/Watt_to_BTU.htm#table)



Scott


---
**I Laser** *March 11, 2016 02:31*

Thanks Scott. I thought 7 seconds would be pushing it, this is my second attempt I'm using an aluminium can. The first had too much flow and the pump wasn't man enough... Was hoping someone would say it's fine lol, looks like more modifications!



I had also read somewhere (probably here) you want to be careful about the pump as too much pressure will blow the tube.


---
**Scott Marshall** *March 11, 2016 02:36*

I think it's the increased volume that's your issue. The pill bottle doesn't hold enough volume to float the float for very long.



As long as you don't put a monster pump on it, and valve off the return, you won't hurt it, you'll blow lines off 1st. Most centrifigual pumps just can't put out the pressure to hurt it. The factory pump is the right style, it's just undersized.

Too small and you'll get bubbles, hot spots, and that's a REAL threat.

The pumps that generate dangerous pressures are the diaphram pumps for RV's designed to supply showers, sinks etc. Stay away from positive displacement style pumps (gear, diaphram, piston, etc) and you'll be ok. 



A lot of guys on here giving advice don't have much real world experience. You'd have to be trying to overpressure a tube enough to damage it.


---
**I Laser** *March 11, 2016 07:45*

The only suitable float I could find locally is one of these: [http://bit.ly/1UkepDG](http://bit.ly/1UkepDG)



It's side mounted and placed as low on the can as possible. I need to slightly widen one of the holes, just concerned I'll screw it up like the first one lol.



Nice to know regarding the pump, as the included ones are so dodgy!


---
**Scott Marshall** *March 11, 2016 12:25*

The side hung floats require more space by design. 

You have to work with what you have on hand. 

Maybe try some stuffer blocks on either side of the float, and make sure you have a ring of holes (or cut down the top edge) at the max level point to limit any water not needed above the switch point. 



 A trick with those side hung floats is to turn them a bit, reducing the drawdown distance. That will widen the flooded space a bit, but you may increase the sensitivity enough to speed things up. @ 45deg, you cut the drawdown distance in half.



I'd say you ought to be able to get the shutoff time down to a second or 2 if you keep on 'adjusting' the float can.



If all else fails, find or make a vertically hung float, compact as possible.



Wish you were closer, I'd send you a float.



These are the ones I'm using:

[http://www.ebay.com/itm/331510410224?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/331510410224?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**I Laser** *March 11, 2016 23:09*

After increasing the hole size it's now down below 2 seconds. Which prior to reading your post I figured was acceptable. Thanks again for your help.



Hadn't thought about rotating to increase the effect, I'll give that a go as it's an easy thing to try. ;)



I was going to buy another switch from eBay but it took nearly two months for the first one to arrive and I'd grown impatient lol. I swear the Chinese postal service walks all mail along the great wall before posting them... :D


---
**Scott Marshall** *March 11, 2016 23:46*

Glad you got it working, I'd feel pretty comfortable at 2 sec.

For 95% of uses, the side hung floats are great, you just found a weakness. You can make your own float switches pretty easy with small magnets and reed switches, but they may be just as hard to get fast.



You're doing pretty good working with a limited inventory. 

The normal Chinese shipment to here is 7 -10 days and we've got a lot of stuff available faster for not a lot more money. 

It's easy to forget how good I've got it. 

 

Scott


---
**MNO** *March 12, 2016 00:29*

I'm not specialist but, by just placing container with fluid (water) higher will decrease lift pressure, that pomp need to generate.  Perfect situation when water tank would be on the same level. That should increase flow rate and increase life of pomp. With that Chinese pomp that can be quite a lot.



You can always add second pomp and sense flow. if first stops switch to second one. Arduino nano + flow indicator + 2x simple relay shield for arduino + 1 termistor for measuring temperature   on laser tube = redundant water cooling system... add 2 more terminators on water input and output and a little of math and You have  pro water cooling sytem :)



You can use flow sensor used for PC water cooling 

[http://www.thewatercoolingshop.co.uk/alphacool-flow-indicator-g1-4-with-rpm-signal-plexi-17176.html](http://www.thewatercoolingshop.co.uk/alphacool-flow-indicator-g1-4-with-rpm-signal-plexi-17176.html)



Or simple 

[http://www.aliexpress.com/item/Flow-Scout-Meter-with-LED-light-And-Pointer-Thermometer-For-Water-Liquid-Cooler-System-CPU-with/32285104154.html?spm=2114.01010208.3.145.1MIbkg&ws_ab_test=searchweb201556_9,searchweb201602_1_505_506_503_504_10034_10020_502_10001_10002_10017_10010_10005_10006_10011_10003_10021_10004_10022_10009_10008_10018_10019,searchweb201603_3&btsid=322546db-5690-4ad8-8337-04e06ebb87cf](http://www.aliexpress.com/item/Flow-Scout-Meter-with-LED-light-And-Pointer-Thermometer-For-Water-Liquid-Cooler-System-CPU-with/32285104154.html?spm=2114.01010208.3.145.1MIbkg&ws_ab_test=searchweb201556_9,searchweb201602_1_505_506_503_504_10034_10020_502_10001_10002_10017_10010_10005_10006_10011_10003_10021_10004_10022_10009_10008_10018_10019,searchweb201603_3&btsid=322546db-5690-4ad8-8337-04e06ebb87cf)



perhaps something like that pomp


{% include youtubePlayer.html id="hgGFaW4xdrg" %}
[https://www.youtube.com/watch?v=hgGFaW4xdrg](https://www.youtube.com/watch?v=hgGFaW4xdrg) 


---
**I Laser** *March 12, 2016 02:27*

Ha, I've actually got that exact pump (MCP655), albeit an earlier model, from an old PC water cooling setup I had. It's rated at 50psi/3.5bar so prior to Scott's comments I assumed it was too powerful for the tube.



I can't really move the tank as it's quite large (25 litres) and I don't have the spare bench space.



I'm quite happy with the <2 second cut off, homemade switch, so will stick to the current setup. I've wasted so much time lately setting things up, I just need to get back to lasing...


---
**Scott Marshall** *March 12, 2016 05:32*

I think the rating on the PC pump refers to the static (overall contained system pressure) not what the pump will generate. Most PC cooler systems are sealed and can build fairly high static pressure from water expansion, thus the rating. Flow on them is pretty small. 50 PSI is indeed way too much for the tube, I'd keep it under 10 psi MAX - which  will take a rather large pump to generate with an open discharge. The stock pump is good for around 5psi at shutoff which is equal to 11.5 ft of water If I recall what the box said) 



Handy Guide:

[http://www.kylesconverter.com/Pressure/Inches-of-Water-to-Pounds-per-Square-Inch](http://www.kylesconverter.com/Pressure/Inches-of-Water-to-Pounds-per-Square-Inch)



Most PC sized cooling gear is going to be too low of a volume and heat transfer capacity to handle a 40W laser. 



I agree with simple is better. Less stuff is usually less to go wrong. A good reliable pump and shut-off system like you have is great. 



I agree with the "back to lasing", I've got mine apart, and really miss being able to use it. Been ill, so unable to get back to it, but I hope to get the structural work done, then get it running and implement the electrical changes gradually without disabling it. 



I hate having stuff "apart for upgrade" for extended periods, parts and interest get lost, turn to work....



Lasers Armed.... Fire!

Scott


---
**Scott Marshall** *March 12, 2016 19:45*

Looks like the concept has been adopted by others....

[https://plus.google.com/u/0/107471533530265402525/posts/UBRM5rSR6qj?cfem=1](https://plus.google.com/u/0/107471533530265402525/posts/UBRM5rSR6qj?cfem=1)


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/aRx2F9cjTyt) &mdash; content and formatting may not be reliable*
