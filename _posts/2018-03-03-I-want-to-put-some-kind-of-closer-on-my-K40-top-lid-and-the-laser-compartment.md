---
layout: post
title: "I want to put some kind of closer on my K40 top lid and the laser compartment"
date: March 03, 2018 21:29
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
I want to put some kind of closer on my K40 top lid and the laser compartment.

They both tend to slam shut when I am not paying attention.



I am looking at gas spring closer's but all the gas springs seem to be to heavy duty for this application. 



What are the different way you have solved this problem.



Don't forget to point us to any parts you purchased ......





**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *March 03, 2018 22:07*

Actually I think gas spring closer is a very good option. I used thousand of them for chipboard cabinet doors. There are ligth duty ones


---
**Adrian Godwin** *March 03, 2018 22:17*

Would a drawer buffer do ?



They act as a simple stop but then slowly take up the last few mm on a  damper.



[amazon.co.uk - Robot Check](https://www.amazon.co.uk/TOOGOO-Kitchen-Cabinet-Drawer-Buffers/dp/B0169T73LO/ref=pd_lpo_vtph_201_lp_img_2?_encoding=UTF8&psc=1&refRID=DWR9Y2ST3CF427ZEA7RD)






---
**Don Kleinschnitz Jr.** *March 03, 2018 22:28*

**+Adrian Godwin** mmm I will take a look. That may keep it from slamming but not hold it open?


---
**Don Kleinschnitz Jr.** *March 03, 2018 22:29*

**+Ariel Yahni** the lightest pressure I could find was 18 lbs which I fear may stress the flimsy cover when you try an pull down on it.


---
**Adrian Godwin** *March 03, 2018 23:21*

The drawer closer won't hold it open, but doesn't the lid tip back far enough to stay ? Although the gas supports are nicer, I think they'll limit how far the lid will open, which might be a nuisance.



Alternative low-tech solution : put your fingers on the rim. You'll soon remember not to drop the lid !  


---
**Phillip Conroy** *March 04, 2018 00:33*

Now might be a good time to replace horrible famable sight window, with glass ,then pick gas struts to suit


---
**Mike R** *March 04, 2018 08:26*

I haven't installed any but how about torsion springs? 

![images/e38554ae385eafb5c722fd0eee85c95b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e38554ae385eafb5c722fd0eee85c95b.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/LGHbZzE2aVh) &mdash; content and formatting may not be reliable*
