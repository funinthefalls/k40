---
layout: post
title: "Laser Cutter Jigs I have some things like anodized aluminum dog tags and 35mm oak medallions that I want to engrave on the laser cutter"
date: February 26, 2018 00:46
category: "Object produced with laser"
author: "Joel Brondos"
---
Laser Cutter Jigs



I have some things like anodized aluminum dog tags and 35mm oak medallions that I want to engrave on the laser cutter.



Does anyone have a good idea about how I can line these up for that purpose?



Do you make a jig that the parts fit on or into? And if so, How do you line it up with the laser each time?



And you do you do double-sided objects like "coins"?





**"Joel Brondos"**

---
---
**James Rivera** *February 26, 2018 02:31*

Check this out. It might help you.[plus.google.com - Curious to know if anyone has any tips/techniques to cut stuff thicker than y...](https://plus.google.com/+JamesRivera1/posts/8vNC9pEaqc8?iem=4&gpawv=1&hl=en-US)


---
**Ned Hill** *February 26, 2018 03:55*

To hold the pieces I would cut out the shape of the pieces, in 1/8" ply so it just barely fits each piece.  I would include a small notch in one side of each  cut out to allow easy remove of the items after etching.  Then glue the cut out ply to an uncut piece of ply to make a floor (optional step).  How you register your jig in the bed is going to depend on what kind of bed you are using.  I have an origin jig in my laser where I took a small piece of ply and push it all the way into the top left corner.  Then cut the ply from the origin in the x and y directions.   This allows me to place the corner of my work piece at the origin each time. Here's a pic of my origin jig.



![images/afd0c862dfc32bb9a4446bff8ec21cf5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afd0c862dfc32bb9a4446bff8ec21cf5.jpeg)


---
**Ned Hill** *February 26, 2018 04:04*

So for me I would take a piece of ply big enough to hold however many of the pieces I wanted to engrave at one time and put one corner at the origin against the jig.  Then make my cutout pattern and make the cuts.  The cutout pattern file will then also serve as the engraving template.


---
**Joel Brondos** *February 27, 2018 00:46*

**+Ned Hill** True, as long as the cutout pattern does not move even a smidge (while taking out the initial cuts).



Also, how did you cut your honeycomb bed material? Metal shears? Band saw? Dremel tool?


---
**Ned Hill** *February 27, 2018 01:05*

**+Joel Brondos** In practice as long as you have a way to reposition the jig (i.e. origin jig) then you are fine. You can always check the alignment by jogging the laser head to the corners and doing a test fire to make sure the corners are were they should be relative to the template file. I’ve done this numerous times. The thing you really need to watch out for is making sure the template doesn’t move around after you get it positioned. When I’m doing engraving work like this I use an expanded steel mesh bed that let’s me use magnets to lock everything in place. 



The honeycomb is a piece I picked up off eBay. It’s thin aluminum so it’s easy to cut with a razor knife. I made a frame from aluminum angle stock to hold it. 


---
**Ned Hill** *February 27, 2018 01:35*

This is what I use as a bed for jig engraving.  Expanded steel mesh with aluminum angle stock supports. Only used two end supports since the mesh is only really flexible along the long axis of the holes. I use the honeycomb for everything else because the expanded mesh causes lots of back burn and builds up lots of wood cutting residue that can be problematic if you care about the back of the piece. ![images/494f7667da77b1933b855d9d3caf6838.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/494f7667da77b1933b855d9d3caf6838.jpeg)


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/frUnQkYJmCG) &mdash; content and formatting may not be reliable*
