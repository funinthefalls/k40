---
layout: post
title: "I am working on a home made blade bed at the moment"
date: July 11, 2017 14:16
category: "Discussion"
author: "3D Laser"
---
I am working on a home made blade bed at the moment.  I got some 1/8 flat steel bars cut to about 16 inches.  Then I just made some boxes with slots to hold the blades and also magnets to hold it all in place.  I will post some pics once I have it all in place 

![images/f1cb9930657b8e33f345198de8cce4e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1cb9930657b8e33f345198de8cce4e9.jpeg)



**"3D Laser"**

---
---
**Chris Hurley** *July 11, 2017 14:31*

Guys I'm going to ask you stupid question: what makes the blade bed so good or why do we use it over a grill? 


---
**Don Kleinschnitz Jr.** *July 11, 2017 15:24*

**+Corey Budwine** as an alternative to the cut steel bars I have seen hack saw blades used....


---
**Don Kleinschnitz Jr.** *July 11, 2017 15:26*

**+Chris Hurley** these beds are various ways to reduce flashback. 



See #1 in this link:

[jumbieindustries.com - 5 Tips for Laser Cutting Acrylic](http://www.jumbieindustries.com/blog/5-tips-for-laser-cutting-acrylic-lucite-perspex-plexiglas-etc)


---
**3D Laser** *July 11, 2017 16:16*

**+Don Kleinschnitz**  I saw that too but didn't seem to stable this thing will be as solid as a rock when done.  A little heavy.  If I had it to do over again I might spring for the aluminum rails instead of steel though they are way for reflective 


---
**Phillip Conroy** *July 12, 2017 00:06*

I used hack saw blades ,thin,cheap straight and the teeth help hold object being  cut,held together with pot plant hanger front bunnings

![images/aa37f89e66b497419753c3cd96e2bb70.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa37f89e66b497419753c3cd96e2bb70.jpeg)


---
**Phillip Conroy** *July 14, 2017 09:59*

I have used my hacksaw bed for hundreds of hours cutting,the only downside with knife beds is cut stuff half dropping through the blades and catching the laser head ,hasn't been a problem ,happened a couple of times  ,thinking of adding  fly wire under the hack saw blades .I have alternated the teeth of the hacksaw blades so that it holds what I am cutting very well .the only time I have used magnets is to flat en out warped plywood 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/8wAwyD7o9P5) &mdash; content and formatting may not be reliable*
