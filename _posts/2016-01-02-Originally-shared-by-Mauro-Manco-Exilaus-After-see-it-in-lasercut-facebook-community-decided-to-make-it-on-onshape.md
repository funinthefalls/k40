---
layout: post
title: "Originally shared by Mauro Manco (Exilaus) After see it in lasercut facebook community, decided to make it on onshape"
date: January 02, 2016 21:16
category: "Repository and designs"
author: "Mauro Manco (Exilaus)"
---
<b>Originally shared by Mauro Manco (Exilaus)</b>



After see it in lasercut facebook community, decided to make it on onshape.

Adjust variable export dxf and cut :)



[https://cad.onshape.com/documents/1ed69d3daebe4a6b8722ea7f/w/c4682e38aaaa452bb3e6d0cd/e/143230cf39474266ae3589de](https://cad.onshape.com/documents/1ed69d3daebe4a6b8722ea7f/w/c4682e38aaaa452bb3e6d0cd/e/143230cf39474266ae3589de)



  



![images/a04c27fb46c45796502c8d647ddf22f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a04c27fb46c45796502c8d647ddf22f9.jpeg)
![images/a803b715893fb699d6f8610453a21a69.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a803b715893fb699d6f8610453a21a69.jpeg)
![images/d47188b0c1f1748197bb6564dc5ab253.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d47188b0c1f1748197bb6564dc5ab253.jpeg)

**"Mauro Manco (Exilaus)"**

---
---
**Tony Marrocco** *May 26, 2017 05:54*

How much did that led setup cost ? Is that in the file you uploaded too ? Like which led strip you used.


---
*Imported from [Google+](https://plus.google.com/114285476102201750422/posts/QEno5LU1kad) &mdash; content and formatting may not be reliable*
