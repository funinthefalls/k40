---
layout: post
title: "First pic is the fixed mirror Second pic is the y-axis mirror one shot near and one shot far"
date: August 21, 2016 20:25
category: "Discussion"
author: "Scott Pollmann"
---
First pic is the fixed mirror



Second pic is the y-axis mirror one shot near and one shot far. 



Last pic is the lens holder - four shots the head in the bottom left is the spot that's not precise.  The other three are close.  



Any suggestions on what to tweak?



![images/73d44c2c9b6a59ef932902a2f45041b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/73d44c2c9b6a59ef932902a2f45041b2.jpeg)
![images/e4efdea6952e3d4f59dbc2587cd7dfe1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e4efdea6952e3d4f59dbc2587cd7dfe1.jpeg)
![images/067ac24905e1e1ed52679b714e86df9d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/067ac24905e1e1ed52679b714e86df9d.jpeg)

**"Scott Pollmann"**

---
---
**Robi Akerley-McKee** *August 21, 2016 22:06*

Looks like fixed mirror aim is just a bit off.. and y axis is off alot. y axis looks like beam is going up and right as you get out farther.  I moved my fixed mirror out a bit so the beam hits the Y mirror in the centerline.  Height of the beam is pretty much set by the tube mounting.  On both of my machines the beam is a touch high.



Aiming mirrors. you have three screws. bottom, outside top and corner. You only turn the bottom and outside screws. bottom aims up and down. outside aims left and right.  If near is high and far is low, you need to go higher on the bottom screw (turn it in). left and right same thing.. if near is closer to the outside of the machine than far, you need to make the beam aim more to the outside of the machine (the angle is acute) which I think is loosen outside (not the corner screw).  I get the back mirror all aimed in, then I go to aiming the next mirror. I have a stock head and I haven't changed anything on it for aiming.


---
**Scott Pollmann** *August 21, 2016 22:10*

**+Robi Akerley-McKee**  thanks.  As they said.  Adjust and adjust and adjust.  Thought I had read that the beam doesn't need to hit the center of the mirror.  I'll keep trying.  Then maybe I'll be able to cut something. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 21, 2016 23:33*

Looks to me like your X axis rail may not be level if the bottom-left is lower than the bottom right.


---
**greg greene** *August 22, 2016 00:50*

checkout [theflyingwombat.com](http://theflyingwombat.com) for some really good alignment instructions


---
**Robi Akerley-McKee** *August 22, 2016 01:56*

**+Yuusuf Sallahuddin** good point... The laser cutter itself might be racked a bit (twisted).  Or the table its on.


---
**Scott Pollmann** *September 02, 2016 14:49*

School started, and I've been in planning mode.  I did move the laser to a better table to help keep the bed level.


---
**greg greene** *September 02, 2016 15:35*

adjust the mirror that is closest to the laser tube to align the spots on the y axis mirror to a single spot.



Check out the instructions on the [theflyingwombat.com](http://theflyingwombat.com)


---
**Scott Pollmann** *September 02, 2016 15:39*

The y axis mirror does hit a single spot, that spot just isn't in the center of the y axis mirror.  It's the x axis mirror that isn't being hit "correctly"


---
**greg greene** *September 02, 2016 15:42*

Yup, that means the beam is not hitting the first mirror dead centre - or that the first mirror needs to be adjusted so the beam hits the second(Y axis) mirror dead centre as the second mirror travels down the rail.


---
*Imported from [Google+](https://plus.google.com/103382513064902122956/posts/5hfjV27MTxS) &mdash; content and formatting may not be reliable*
