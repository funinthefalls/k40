---
layout: post
title: "Anyone know how far out Mac distance the laser beam will travel between first second and third mirrors?"
date: April 11, 2016 06:14
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
Anyone know how far out Mac distance the laser beam will travel between first second and third mirrors? Did some research and found one article/schematic that Incorporated a beam expander.





**"Andrew ONeal (Andy-drew)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 10:31*

Hi Andrew. This is approximate, as I didn't measure precisely. I did this up for someone else that was interested in the approximate distances.



[https://drive.google.com/file/d/0Bzi2h1k_udXwel94Y0hWRDRCUlE/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwel94Y0hWRDRCUlE/view?usp=sharing)


---
**Thor Johnson** *April 11, 2016 14:22*

Beam expanders have uses:

a) You can use higher powers without worrying about mirrors (because the beam is wider, its watts/cm^2 is lower)

b) The wider the beam, the crisper the final focus can be ("diffraction limited" or "aperture limited" -- why you need a big lens to get F1.8 photographs)

c) If you expand the beam, it diverges less, so it can be shot further...



On my K40, I go from a 2mm beam just after the tube, up to a 4 mm beam in the bottom right.  But my final lens is something like 6mm diameter, so... theoretically, we could go twice as far before the beam gets too big to go through the lens and you need an expander to fix it.


---
**Andrew ONeal (Andy-drew)** *April 22, 2016 03:36*

Awesome input guys and sorry for late reply.


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/HDVH5pTBTxS) &mdash; content and formatting may not be reliable*
