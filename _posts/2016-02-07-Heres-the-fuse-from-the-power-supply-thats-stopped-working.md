---
layout: post
title: "Here's the fuse from the power supply that's stopped working"
date: February 07, 2016 16:43
category: "Hardware and Laser settings"
author: "Abe Tusk"
---
Here's the fuse from the power supply that's stopped working.  On the side is printed T4AL250V which I assume is 250V, 4A.  This is blown, right?



Follow up from previous post: [https://plus.google.com/102172526207413999595/posts/4iq5nkkcq99](https://plus.google.com/102172526207413999595/posts/4iq5nkkcq99)



![images/915c56b34070893ac2395d72fbe0d720.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/915c56b34070893ac2395d72fbe0d720.jpeg)
![images/1b5004af8e3db01865302ab3718ed54c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b5004af8e3db01865302ab3718ed54c.jpeg)

**"Abe Tusk"**

---
---
**Jim Hatch** *February 07, 2016 17:06*

Can't tell from the picture if the fusible link is broken. Did you test it with an ohm meter? You will likely see some resistance if it's good but infinite if it's blown.



It's a Time delay 4A fuse by the way. They're used when the circuit is expected to get a quick power surge and the settle. Motors and power supplies tend to do that.



You can find them on Amazon but your local hardware store may also have them. They're pretty common.


---
**Tom Nardi** *February 07, 2016 17:39*

Yeah, much better to just test with a meter than trying to microscopically examine it. 


---
**Abe Tusk** *February 07, 2016 18:44*

I tested for continuity and there was none.  It's showing infinite resistance on my multimeter.


---
**Jerry Rodberg** *February 07, 2016 19:35*

Those on board fuses are usually there to prevent catastrophic damage to the user and his power system in most commercial electronics.  While you can replace it, the replacement has a 50-50 chance of  blowing instantly if you can't determine what caused it the first time.  If you try and bypass it, keep a fire extinguisher nearby.


---
**Abe Tusk** *February 07, 2016 19:54*

I'm going to try to replace and hope for the best.  If I had to guess, the power supply is probably shot but I want to try replacing the fuse on the off chance it will work.


---
**Scott Thorne** *February 07, 2016 22:57*

You said this blew while you were upgrading to a smoothie right....then check your connections first.


---
*Imported from [Google+](https://plus.google.com/102172526207413999595/posts/6Dstf7ZUwx4) &mdash; content and formatting may not be reliable*
