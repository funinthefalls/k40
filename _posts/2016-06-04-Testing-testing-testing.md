---
layout: post
title: "Testing testing testing"
date: June 04, 2016 02:30
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Testing testing testing. 



![images/ba3f543951aedf7108c92e2ae8292087.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ba3f543951aedf7108c92e2ae8292087.jpeg)
![images/7bec7ef2d81a5ffc76022a4ecc1dca26.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7bec7ef2d81a5ffc76022a4ecc1dca26.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 04, 2016 03:10*

Row 1, Column 2 & Row 2, Column 1 look pretty good.



What's the little flip thingy? A phone/tablet stand?


---
**Ariel Yahni (UniKpty)** *June 04, 2016 03:45*

Yes was trying to get a feel for the living hinge


---
**Mr Shahrouz** *June 04, 2016 04:00*

Hi

Very nice your testing, I 'm confused because I can not really do a great job so


---
**Ariel Yahni (UniKpty)** *June 04, 2016 04:04*

**+Mr Shahrouz**​ what issues do you have? 


---
**Mr Shahrouz** *June 04, 2016 04:06*

I get help from Mr. scoot thorne could you please help me to know what software are you working with and what is it material?


---
**Ariel Yahni (UniKpty)** *June 04, 2016 04:15*

**+Mr Shahrouz**​ the best image is done using Gimp, image editor. 
{% include youtubePlayer.html id="TiEOXElDbHc" %}
[https://youtu.be/TiEOXElDbHc](https://youtu.be/TiEOXElDbHc)  . The material is plywood 3mm


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 04, 2016 04:16*

**+Mr Shahrouz** The standard software is Corel Draw (v12) usually. With a plugin called Corel Laser. I've upgraded to Corel Draw x5 instead of v12 as v12 is old.  You can find all the software available on my link here with information on how-to install also in there:



[https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0](https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0)


---
**Tony Sobczak** *June 04, 2016 04:47*

Follow


---
**Alex Krause** *June 04, 2016 04:58*

**+Ariel Yahni**​ laser cut an adjustable z platform :) or Atleast a frame for it 


---
**Ariel Yahni (UniKpty)** *June 04, 2016 05:01*

**+Alex Krause**​ still thinking of proper design


---
**Alex Krause** *June 04, 2016 05:04*

I've been racking my head debating on powered vs manual... the opto sensor idea I had that for auto z level makes me want powered but a part of me wants to take total control of the platform


---
**Alex Krause** *June 04, 2016 05:25*

When testing on larger sheets like this be careful about noting your perfect settings as you move further away in both X,Y your power diminishes because the nature of the cheap Chinese basic hardware that comes with it so power settings for a particular grid of the machine may be different if you move you object elsewhere in the work area


---
**Ariel Yahni (UniKpty)** *June 04, 2016 05:32*

There no reason you cannot have both


---
**Hayden DoesGames** *June 04, 2016 09:03*

Would it be possible to etch a photo on perspex? Still waiting on funds to buy a Laser and deciding which one to buy lol.




---
**Mr Shahrouz** *June 04, 2016 11:25*

Thank you and help you appreciate the group of friends who help each other


---
**Mr Shahrouz** *June 04, 2016 11:33*

For example, to import a picture quality that the software corel draw.

I dxf format and then save it using software start engraving laser work , but I 'm really stink


---
**Mr Shahrouz** *June 04, 2016 11:38*

I think to edit and convert color photos into a good software for the device have problems


---
**Mr Shahrouz** *June 04, 2016 11:40*

Please, can I ask you a short video of the procedure or file that could be useful to provide me ?


---
**Mr Shahrouz** *June 04, 2016 11:54*

**+Yuusuf Sallahuddin** thank you

View and download it , hope is not a problem


---
**Mr Shahrouz** *June 04, 2016 11:57*

**+Ariel Yahni** 

Thank you, I do not know why I can not play my server problem .Do not let other files directly ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 04, 2016 14:36*

**+Mr Shahrouz** You are welcome to download it. :) 



The video that Ariel provided earlier is great for how to convert a colour image to become laser-ready. I tested the procedure (for Photoshop) that the guy mentions (in another of his videos) & it works really well.


---
**Mr Shahrouz** *June 04, 2016 19:46*

**+Yuusuf Sallahuddin** HelloJust saying , but I do not know why the video was not broadcast ? !I would find the problem , because tips and other friends , thank you .Let me check and I will let you know the result .


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 00:03*

**+Mr Shahrouz** This is the video that Ariel shared showing how to prepare for GIMP: 
{% include youtubePlayer.html id="TiEOXElDbHc" %}
[https://youtu.be/TiEOXElDbHc](https://youtu.be/TiEOXElDbHc)



You can also find a link from the same youtube guy that shows how to do the same thing in Photoshop.


---
**Ashley M. Kirchner [Norym]** *June 05, 2016 00:18*

Wow, you're really scorching the wood there with the living hinge. No air assist? My living hinges are clean. 


---
**Ariel Yahni (UniKpty)** *June 05, 2016 00:32*

**+Ashley M. Kirchner**​ no AA yet installed. 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/U9KdFHbhPRm) &mdash; content and formatting may not be reliable*
