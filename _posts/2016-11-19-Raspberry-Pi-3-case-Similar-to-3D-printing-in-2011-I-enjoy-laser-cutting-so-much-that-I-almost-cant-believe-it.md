---
layout: post
title: "Raspberry Pi 3 case. Similar to 3D printing in 2011 I enjoy laser cutting so much that I almost can't believe it..."
date: November 19, 2016 19:04
category: "Discussion"
author: "Timo Birnschein"
---
Raspberry Pi 3 case.

Similar to 3D printing in 2011 I enjoy laser cutting so much that I almost can't believe it...



![images/8834df41f2ded3f06d4bed92157da0b6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8834df41f2ded3f06d4bed92157da0b6.jpeg)
![images/98e0b4f0d8eda2edf9366beec7adcc22.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98e0b4f0d8eda2edf9366beec7adcc22.jpeg)
![images/89b659d49106cd5db7818e2d5e171c9c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/89b659d49106cd5db7818e2d5e171c9c.jpeg)
![images/eee06a0da24b69ed4b51e27d9bb9ac13.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eee06a0da24b69ed4b51e27d9bb9ac13.jpeg)
![images/cfcd67e3e065f19ebd645164ad0d085e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cfcd67e3e065f19ebd645164ad0d085e.jpeg)
![images/d4f00dd80ef89e7abb1b7eb1aaee576f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d4f00dd80ef89e7abb1b7eb1aaee576f.jpeg)

**"Timo Birnschein"**

---
---
**Cesar Tolentino** *November 19, 2016 19:14*

Nice. I like the coloring. In with you with laser cutting. It's very addictive.  I'm just so sad that my local hardware stop stocking 1/8 High density board or even mdf.  Even the whiteboard version.  It's hard to source new material in the 1/8 thickness. 


---
**Brandon Smith** *November 19, 2016 19:23*

Any chance you have the file to share, or a link. I like the retro look, and have been wanting to make a new case for my rpi3.


---
**Timo Birnschein** *November 19, 2016 19:36*

**+Brandon Smith** extra for you :) [http://www.thingiverse.com/thing:1903743](http://www.thingiverse.com/thing:1903743)

[thingiverse.com - Retro Raspberri Pi 3 Case by McNugget](http://www.thingiverse.com/thing:1903743)


---
**Brandon Smith** *November 19, 2016 19:38*

Great! Thanks so much. Will cut it tonight.


---
**Timo Birnschein** *November 19, 2016 19:40*

Let me know how it turned out! :)


---
**Mauro Manco (Exilaus)** *November 19, 2016 20:50*

Wow soon cut it me too !!!


---
*Imported from [Google+](https://plus.google.com/+TimoBirnschein/posts/8Pc3FcYfHWh) &mdash; content and formatting may not be reliable*
