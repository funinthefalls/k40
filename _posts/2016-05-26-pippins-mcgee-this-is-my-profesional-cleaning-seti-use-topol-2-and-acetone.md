---
layout: post
title: "pippins mcgee this is my profesional cleaning set,i use topol 2, and acetone"
date: May 26, 2016 17:24
category: "Discussion"
author: "Damian Trejtowicz"
---
**+pippins mcgee**  this is my profesional cleaning set,i use topol 2, and acetone



![images/d58d255c3b7e307d96553f4937ea3782.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d58d255c3b7e307d96553f4937ea3782.jpeg)



**"Damian Trejtowicz"**

---
---
**Mishko Mishko** *May 29, 2016 08:12*

I used that, too, when I worked with Trumpf laser. Still have a bottle of Topol, but it's too old, and teh lens holder too big for K40 lenses...


---
**Hayden DoesGames** *May 30, 2016 08:01*

Use this every day on the TruLaser 3030 I run at work. Apparently Trumpf now don't recommend using too much acetone. 


---
**Damian Trejtowicz** *May 30, 2016 08:02*

They changed politics,now in cleaning set is isopropynol not aceton


---
**Hayden DoesGames** *May 30, 2016 08:07*

Ah! Wasn't aware of that, thanks. I had the day off last time it was serviced so never got chance to speak to the engineer lol




---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/DCop43otrHV) &mdash; content and formatting may not be reliable*
