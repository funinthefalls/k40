---
layout: post
title: "Hello! I was wandering if anyone can help, I recently got a K40 and I also have CorelDraw x8 but I cant seem to get the CorelLaser to work with it rather than CorelDraw 12 that comes on the disc"
date: January 23, 2017 17:56
category: "Software"
author: "Super Cute Box"
---
Hello!

I was wandering if anyone can help, I recently got a K40 and I also have CorelDraw x8 but I cant seem to get the CorelLaser to work with it rather than CorelDraw 12 that comes on the disc. 

Many thanks for any help!



<b>*Solved!*</b>





**"Super Cute Box"**

---
---
**Tony Sobczak** *January 23, 2017 18:29*

I have it working without any issues.  Make sure 12 in not installed. Install x8 first then corelLaser.




---
**Jim Hatch** *January 23, 2017 19:21*

+1 on Tony's comment. You need to reinstall CorelLaser after you install X8. I am using it as well. I didn't uninstall Corel 12 though - I have both versions running on my machine (it was a "just in case" move if X8 didn't work I'd be able to revert quickly).


---
**Super Cute Box** *January 23, 2017 19:35*

**+Tony Sobczak** **+Jim Hatch** hmm I tried this but my CorelLASER comes up with the error: The CorelDRAW(Ver >=11) software not installed!

the CorelLASER is 2013.02 which came on the disc


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 23, 2017 19:38*

Is the CorelDraw x8 that you have Home & Student by any chance?


---
**Super Cute Box** *January 23, 2017 19:45*

**+Yuusuf Sallahuddin** no it's Graphics Suite - i read that it didn't work with home & student so i checked this already. I'm just doing a reinstall of everything now starting again see if it will work


---
**Super Cute Box** *January 23, 2017 19:52*

**+Yuusuf Sallahuddin****+Tony Sobczak****+Jim Hatch** okay it is working now! I just uninstalled them all and re-installed again and it worked! Thanks for your help! Finally I can use x8  with the cutter


---
**Tony Sobczak** *January 23, 2017 20:03*

+jim hatch. How did you get corel laser to find 8.  I have 7 & 8 installed and if I don't rename 7 it's what corel laser finds. * was installed after 7 of course.


---
**Nick Hale** *January 24, 2017 04:35*

This is the first time I've seen that home and student doesn't work. That's why I cannot get mine to work


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 24, 2017 07:20*

**+Nick Hale** That's unfortunate if you specifically went out & purchased that version. I recall about 12 months back someone else purchased a home & student version only to find out it doesn't work.


---
**Jim Hatch** *January 24, 2017 20:36*

**+Tony Sobczak**  - I believe the CorelLaser is keyed off of a registry entry for Corel and reinstalling it after installing Corel X8 gets CorelLaser attached to the X8 version. If I fire up Corel X8 I don't see the extension toolbar. If I fire up CorelLaser it loads that Winseal XP splash screen and then starts Corel X8 with the extension toolbar. I can still fire up Corel 12 but that also comes without the extension toolbar. When I did my X8 install I figured if it didn't work after installing CorelLaser I'd still be able to use Corel 12 - I didn't uninstall it because that was a PITA to install since I didn't have good install files for it, just what came off the K40's CD.



**+Nick Hale** - I think it comes up every few months. I've been using X8 since last spring. Any version <b>except</b> Home & Student works. That's because the naming of the main executable between X8 any version and H&S is different. I think that's what drives the registry entries that CorelLaser install is looking for in order to attach to. The Corel 12 and X8 (any version but H&S) are the same main exe & dll names. I puttered around a bit seeing if I could rename things so that CorelLaser found what it needed but didn't bother since my standard version of X8 worked I didn't need to use my H&S version (I have both licensed versions). It wasn't hard to find the files but then spelunking through the registry wasn't as fast as I thought it would be so I lost interest :-)


---
**Nick Hale** *January 26, 2017 17:08*

Nice.  Ill look into renaming the files.  Thanks


---
**Tony Sobczak** *January 26, 2017 18:27*

[CorelDrw.exe](http://CorelDrw.exe)


---
*Imported from [Google+](https://plus.google.com/117397895942530721173/posts/Hh1zzamN7Sd) &mdash; content and formatting may not be reliable*
