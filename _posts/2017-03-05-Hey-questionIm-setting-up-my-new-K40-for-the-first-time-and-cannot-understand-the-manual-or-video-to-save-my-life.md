---
layout: post
title: "Hey, question...I'm setting up my new K40 for the first time and cannot understand the manual or video to save my life"
date: March 05, 2017 22:54
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Hey, question...I'm setting up my new K40 for the first time and cannot understand the manual or video to save my life. Did you guys install and setup the LaserDraw software? Is it mandatory to run the machine? I have Corel Draw x7 already, can I just use that directly instead of the included software?





**"Nathan Thomas"**

---
---
**Ned Hill** *March 06, 2017 01:17*

I would stick with CD X7 unless it's the student/home version which will not work.  If it's the stand alone version then  you will need to install the corel laser plugin from here [lcshenhuilaser.com - Download - Liaocheng shenhui laser equipment Co,.ltd.](http://www.lcshenhuilaser.com/download.html)

download the corelLaser 2013.02.exe file.  Once it's installed make sure coreldraw is closed and just launch plugin which will open coreldraw.  You'll have a floating toolbar at the top for the plugin.  If it disappears after running a job you will find it in your desktop tool tray (pink icon) and right click.  If you don't have the version of CD that works with the plugin then you will need to install the LaserDraw software.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 06, 2017 03:09*

I feel like there may have been a USB driver setup file on the CD too? I vaguely recall something about an INF file I had to choose in a list.


---
**Mark Brown** *March 06, 2017 03:36*

**+Yuusuf Sallahuddin**  There wasn't with mine, 3 weeks ago.  I just installed CorelDraw/Laser and Laser Draw.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 06, 2017 03:56*

**+Twelve Foot** I just installed all sorts of stuff that was on the cd. Probably a bunch of stuff I didn't need too lol. It was a long time ago when I last installed/used CorelDraw/CorelLaser so hard to recall exactly what was there.


---
**Mark Brown** *March 06, 2017 04:01*

I actually should have said "there may have been one with mine, but if there was I didn't bother with it".


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/RDye85zxJss) &mdash; content and formatting may not be reliable*
