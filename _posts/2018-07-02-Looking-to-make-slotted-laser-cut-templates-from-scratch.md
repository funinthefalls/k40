---
layout: post
title: "Looking to make slotted laser-cut templates from scratch?"
date: July 02, 2018 03:57
category: "Software"
author: "HalfNormal"
---
Looking to make slotted laser-cut templates from scratch?

Here is the program for you! 



[http://flatfab.com/](http://flatfab.com/)



![images/15eef5f1e84f2c85148bc698a64f5e72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15eef5f1e84f2c85148bc698a64f5e72.jpeg)



**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *July 02, 2018 12:37*

OK, this is seriously cool! 

Thanks for the find! My grand-kids are in for a treat and I can see a new K40 tube in my future as this will suck up a lot of Co2 or dull a lot of cnc bits :)!


---
**Ned Hill** *July 02, 2018 16:37*

Whoa, that is cool!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/ZFqjvNZzQ97) &mdash; content and formatting may not be reliable*
