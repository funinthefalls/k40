---
layout: post
title: "Can any one of you guys post a working smoothie config please?"
date: July 12, 2016 20:54
category: "Smoothieboard Modification"
author: "Mircea Russu"
---
Can any one of you guys post a working smoothie config please? My board is here, updated to the latest firmware, glcd works, just waiting to be connected but now I struggle to get through all the config options and I'm afraid not to mess something up, speeds and current values trouble me the most, also all the endpoint config with xmin ymax.

Thank you!





**"Mircea Russu"**

---
---
**Ariel Yahni (UniKpty)** *July 12, 2016 23:03*

**+Mircea Russu** What board do you have?




---
**Mircea Russu** *July 13, 2016 04:34*

Mks sbase v1.3


---
**Ariel Yahni (UniKpty)** *July 13, 2016 04:55*

I can send you my config but you'll have to make it work for you


---
**Mircea Russu** *July 13, 2016 05:02*

That's not a problem. Only difference might be the pin assignment. Thank you. 


---
**Ariel Yahni (UniKpty)** *July 13, 2016 05:04*

Your email please


---
**Mircea Russu** *July 13, 2016 06:21*

executivul at yahoo dot com  Thank you!


---
**Jon Bruno** *July 22, 2016 23:11*

**+Mircea Russu** Make sure you change the Alpha and Beta stepper max current to the correct value for your motors. if you leave it at 1.5A you could burn out your stock K40 LPSU I have read that the typical max output on the 24v rail is 1.0A


---
**Mircea Russu** *July 23, 2016 07:45*

I used 0.5 on both and can't see any degradation in quality as missed steps. Thank you!


---
**Jon Bruno** *July 23, 2016 14:27*

One thing I saw when I first tested at 1.5 was that the X axis would randomly switch direction. This was probably caused by the fields being to strong between the poles.

Of course the crecendo was when the power supply gave up the ghost.


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/DViasSNxmsf) &mdash; content and formatting may not be reliable*
