---
layout: post
title: "Is there a way to engrave a gradient?"
date: October 02, 2015 18:07
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Is there a way to engrave a gradient? I'm finding that if my image has a gradient, or really any sort of color variations, what gets engraved looks pretty much uniform. It'd be nice if it can distinguish between colors, particularly gradients. Or is this one of the limitations of using the stock electronics that comes with the thing? Would an upgrade to a DSP controller allow for finer control?





**"Ashley M. Kirchner [Norym]"**

---
---
**Sean Cherven** *October 02, 2015 21:17*

It's one of the limitations of the stock controller. There are other ways to do it, such as something like dot matrix. But results vary. A DSP Controller would solve this issue, and is a excellent upgrade, however, for me, I just don't have the extra money to fork over for it right now.


---
**Ashley M. Kirchner [Norym]** *October 02, 2015 21:33*

Well it's on my list of things to replace shortly here. Good to know it would work better with a DSP controller. 


---
**Kirk Yarina** *October 06, 2015 00:13*

LaserDRW will engrave a gradient.  I did a quick test on copier paper and discovered that you can vary the number of pixels used for shading.  Here's a an example on copy paper at the lowest usable power, not a good quality test but you can see how it works.  It's a left to right over right to left gradients, with different settings of the pixel parameter (screenshot below).  From the top left it's 1,2,3,5, and 10, plus the one in the bottom right corner is rotated 90 degrees to see if that makes a difference.  Set at 1 it burned through the paper, and the low power setting probably affected the amount of shading - I plan to try it again on a better substrate that can take a higher power, but it looks like it dithers over an increasing number of pixels as the setting goes up.  Never put a photo in a G+ comment before, looks like they show up as links rather than images.



[https://goo.gl/photos/6wYBrXZFnQkB8oZe8](https://goo.gl/photos/6wYBrXZFnQkB8oZe8)



And the screenshot:

[https://goo.gl/photos/QC5PraQieBj4EoU57](https://goo.gl/photos/QC5PraQieBj4EoU57)


---
**Ashley M. Kirchner [Norym]** *October 06, 2015 02:49*

Right, so that means I can't draw an image with a gradient in it. It will have to be a solid color and then the steps adjusted in LaserDRW. It also means I can't do the whole image like that as it will affect the whole thing.


---
**Sean Cherven** *October 06, 2015 12:43*

Kirk, can you explain how this works a little bit better?


---
**Kirk Yarina** *October 06, 2015 13:21*

**+Ashley M. Kirchner** No, you can draw an image with a gradient in it, assuming your drawing program lets you.  The steps parameter lets you adjust how many pixels are LaserDRW uses to produce that gradient.  Look up dithering and/or halftones for a better explanation, but it'll take a block of pixels (really don't know if they're from the scaled image's pixel density, or if LD will rescale and use something else like it's default 1000 ppi; I suspect it might be the former)  and only fire the laser on some of them so the average of the block will be the intensity of that area.  Since the K40's steps/inch is probably a whole lot higher than the pixel density of your image it should produce a smooth shading.  My test image was small, and I didn't have a lot of time to refine the test (planned to try again last night on some maple test blanks, but wife, honeydo list, other commitments...) but it should be pretty easy for you to set up a test and experiment to see what settings work best for you given a shaded image.  If you do please report back.



GIMP, and presumably other image manipulation programs, will let you change the pixel density (pixels per inch) of an image and the images ppi may interact with "pixels" - an article I found while googling recommended lowering the ppi, dithering, then upping it to get a better image, but I didn't try this yet.    Btw, GIMP also include a built-in dithering option, plus a separate halftone filter.  I couldn't find where LaserDRW's outstanding ( :) ) documentation explained exactly what it's parameters do, other than the recommendation in the recently posted PDF to set pixels to "2" is what prompted me to experiment with it.  I found a chinese language PDF document, but google translate mangled the formatting until it was unreadable.



Halftoning and dithering is what magazines, etc., use to print images so it's a long established technique and works very well.  In the old pre computer days they rephotographed pictures through a screen to break the image into different size dots based on the intensity in that hole in the screen, if I understand the technique correctly.   It looks like LaserDRW does the same by how many dots in a "pixel" by "pixel" block it will fire on.  For example, a 4x4 block will give 16 shades of gray assuming the size of the block is smaller than your eye can see (exactly the way your LCD TV works, except with 3 colors).  I don't know if other controllers can vary the tube power dynamically for each pixel, or if the tube/power supply can even respond quickly enough (I vaguely remember something about RF lasers being able to do this).  If they don't you'll still need to do the equivalent of dithering to do shading/gradients.  LaserDRW, clunky as it is, has the capability built in.



I should mention that my approach is to see just how much the stock K40 will do without modification before considering changing anything.  After all, you can't tell if your expensive upgrade is an improvement unless you know what you're starting from.  Retired, need to spend those toy dollars as efficiently as possible (maybe some other toy is a better choice :) ), was an engineer and like to see just what the limits are.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/hwNZWSPh17E) &mdash; content and formatting may not be reliable*
