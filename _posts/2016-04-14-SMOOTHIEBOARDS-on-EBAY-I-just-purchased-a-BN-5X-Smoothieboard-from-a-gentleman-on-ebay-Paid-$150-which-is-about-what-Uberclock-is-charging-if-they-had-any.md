---
layout: post
title: "SMOOTHIEBOARDS on EBAY: I just purchased a BN 5X Smoothieboard from a gentleman on ebay, Paid $150 which is about what Uberclock is charging if they had any"
date: April 14, 2016 18:47
category: "Discussion"
author: "Scott Marshall"
---
SMOOTHIEBOARDS on EBAY:



I just purchased a BN 5X Smoothieboard from a gentleman on ebay, Paid $150 which is about what Uberclock is charging if they had any. 





The auction started at $100, but I couldn't see getting in a battle where it could easily go past that, given the circumstances.  Didn't really want to spend the money, got a ton of cash out on my milling machine upgrade, but I had to do it while I could.



There's another one (also 5X) on there that's been opened, but the seller says he just bought it and never got around to using it. Bidding starts at $89 if anybody is interested.





**"Scott Marshall"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2016 20:03*

Also **+Peter van der Walt** and I are working on various boards that run smoothie (like his Smoothiebrainz and my 'ReMix' derivative of it with 6 stepper drivers and plenty other features on board, at approximately the same price point you are talking about. 


---
**Scott Marshall** *April 14, 2016 20:06*

Sounds good, especially with some healthier stepper drivers. 



Any expected release date, or specs of what to expect?


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2016 20:25*

There are some pics on my g+ of the board layout in eagle and I want to post another update on it sooner rather than later.  

The drivers are stepstick sockets so you can pop in the A4988 or DRV8825 modules per your preference.  

4 standard mosfets for 3 hotends + fan and 1 huge mosfet for powerful heated beds.  Or use them for general IO, which is needed for all the actuators on a pick n place. 

ESP8266 for wifi control (experimental, but Peter has this working), microSD slot, USB, and currently grappling with getting an ethernet jack on board.   So in short, everything a smoothie 5X has - 6 socketed drivers instead of 5, but only 5 mosfets in total, + wifi, and in a smaller footprint (Cramming as much as I can into a 100x100mm board size).  

I'm still in the design phase.  A lot of the layout is done and some of the routing, 

but it'll be some weeks before the first batch is done.  I am looking for testers, so if you're interested please contact me (hangouts works best).  


---
**Scott Marshall** *April 14, 2016 20:50*

Wow, Nice work. Hard to believe you'll be able to sell them that cheap.

I like the plug in driver option, the 8825 is a nice 2.5a driver for 2 bucks. For the  laser and 3d printer folks, it's probably all they'll need.

I'm surprised nobody has developed a compatible plug in remote in that goes to 4 or 5a.

I'm not google versed, but I'll drop you a line on your section, I would be definitely interested in doing beta testing.  I've got history in the field.



Scott


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2016 21:33*

**+Scott Marshall** I sent you a message on hangouts to discuss further. Anyone else is welcome to message me through my G+ profile or email raykholo (at) gmail (dot) com. 


---
**Dennis Fuente** *April 14, 2016 22:08*

Hi Ray 

I'd like a look at your board also.



Dennis 


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2016 03:47*

**+Scott Marshall** **+Dennis Fuente** , as you requested, I put up a detailed post here:  [https://plus.google.com/+RayKholodovsky/posts/YDwDF2Lk95j](https://plus.google.com/+RayKholodovsky/posts/YDwDF2Lk95j)


---
**Scott Marshall** *April 15, 2016 13:47*

Thanks Ray, sorry I missed you last night, I took to feeling bad and  didn't get back to the computer till late. 

Scott


---
**Dennis Fuente** *April 17, 2016 17:47*

Ray 

The board looks great any idea when you will have some of them and what's the price point for them sure alot going on in that little space 



Dennis 


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2016 18:39*

**+Dennis Fuente** probably looking at a few weeks before I get the first boards in.  And assuming there's no major issues with them, they're going out to people interested in testing and aren't afraid to get their hands a bit dirty.  Price point is going to be in the $150 - $200 range.  I think $179 for the board is quite fair, + $15 for a set of stepper drivers, and ~$5 domestic shipping in the USA. 

If you're interesting in being one of the first testers, let me know and I can add you to the list.


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/J98KGN3P6Et) &mdash; content and formatting may not be reliable*
