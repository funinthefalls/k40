---
layout: post
title: "Hi everybody. Just joined here but I'm part of a few laser and 3d groups on Facebook"
date: February 09, 2016 22:54
category: "Discussion"
author: "Tim barker"
---
Hi everybody. Just joined here but I'm part of a few laser and 3d groups on Facebook.



I just recently wired up the ramps 1.4 upgrade to my k40. Wired everything to a T but for some reason I'm having lots of trouble trying to get my steppers to move.



The LCD keeps saying endstops hit even when its far from them. The y barely moved in little jerks and the x doesn't move at all.



Does anybody here have experience with this problem?



Thanks in advance.





**"Tim barker"**

---
---
**Sean Cherven** *February 09, 2016 23:05*

Most likely the endstops are wired wrong or configured wrong in the firmware. Did you adjust the power to the stepper motors?


---
**Tim barker** *February 09, 2016 23:10*

If you mean the wiring to the stepper motors, then yes I have that right. As far as adjusting the power to them goes I've not been versed on how to do that yet. Been looking for a data sheet for these steppers to find out more about them, but I haven't had any luck.


---
**Tim barker** *February 09, 2016 23:11*

And as far as the firmware goes, I'm just using the turnkey tyranny setup the same as it comes.


---
**Tim barker** *February 09, 2016 23:17*

Also i forgot to mention that i've disconnected the endstops and tried to move the head around just in the center of the work area and i still get the same result. which makes me think it would be the firmware. but like i said, im using the turnkey marlin just how it came.


---
**Sean Cherven** *February 09, 2016 23:31*

the endstops are wired seperately from the motors.. does your K40 use optical endstops, or microswitches?


---
**Sean Cherven** *February 09, 2016 23:31*

And to adjust the power to the steppers, u need to adjust the tiny POT on the stepsticks. 


---
**Sean Cherven** *February 09, 2016 23:32*

[http://reprap.org/mediawiki/images/thumb/0/02/Pololu_v-ref_checking.jpg/400px-Pololu_v-ref_checking.jpg](http://reprap.org/mediawiki/images/thumb/0/02/Pololu_v-ref_checking.jpg/400px-Pololu_v-ref_checking.jpg)


---
**Sean Cherven** *February 09, 2016 23:39*

well if u disconnect the endstops, depending on if they are normally open, or normally closed, could cause the endstop to register as being hit.


---
**Tim barker** *February 09, 2016 23:51*

optical endstops.



the little pots on the drivers don't seem to have an end or a beginning. is there a general way to adjust those without having a meter on hand?


---
**Sean Cherven** *February 09, 2016 23:53*

I just adjust them until the steppers move well, and provide enough force to avoid missed steps. But I think your issue is more endstop related.


---
**Tim barker** *February 10, 2016 00:08*

so if there's something in the code that has the endstops set as a different than my norm, open or closed - wise, that would give me the trouble then? ill have to looke more into the code then. i'm new to that kind of stuff.


---
**Sean Cherven** *February 10, 2016 00:10*

Yes, that can cause the stepper motors to stop and jitter like that, cuz it think it's hit a endstop so it stops


---
**Tim barker** *February 10, 2016 00:25*

gotcha. makes sense. well that sounds about right for whats going on with my y axis. my x isnt moving at all so im wondering if thats another issue altogether. going through and looking at the code right now.


---
**Tim barker** *February 10, 2016 00:32*

Question though...

Wouldn't unplugging the wires for the endstop signals give me use of my steppers, even if the code was wrong?


---
**Sean Cherven** *February 10, 2016 01:11*

Only if they are configured as normally open. 


---
**Tim barker** *February 10, 2016 01:29*

gotcha thanks


---
**Anthony Bolgar** *February 10, 2016 01:32*

Endstops on my K40 are wired normally open, hitting a stop closes the circuit.


---
**Tim barker** *February 10, 2016 01:41*

So i disabled min and max endstops in the firmware and uploaded. Doesn't say endstops hit anymore but im still getting the same response. Stepper just barely moving like a mm at a time when i try to move it using the lcd screen.



Any thoughts?


---
**Sean Cherven** *February 10, 2016 01:42*

I'm out of ideas, sorry.


---
**Tim barker** *February 10, 2016 01:45*

No worries. I'll keep trying. I appreciate your help though.



I'm wondering if there's something telling the steppers to move at the wrong rate or something. Although I'm not entirely sure what the proper rate would be. steps/mm i mean


---
**Sean Cherven** *February 10, 2016 02:05*

Do you have all 3 jumpers installed under the stepsticks?


---
**Tim barker** *February 10, 2016 02:24*

Well i didnt know what you meant by jumpers until i looked it up just now. I didnt receive any with my kit, so i didnt know i was missing them


---
**Tim barker** *February 10, 2016 02:26*

I'm guessing that's a big piece of the puzzle right? lol


---
**Sean Cherven** *February 10, 2016 02:32*

Yup, those are gonna be needed. You can change the firmware to use 1X instead of 16X step rate to test it out without the jumpers, but your steppers will not be as smooth as needed for actual cutting/engraving.



But as a test, just change the firmware to use 1X stepper rate instead of 16X. You may need to adjust the POT on the stepstick to get the motors moving.


---
**Tim barker** *February 10, 2016 02:57*

you were absolutely right. and i was wrong the jumpers did come with my kit. i found them, put them on, and now y is moving like butter. still dont have any x movement from the lcd control, but i feel lightyears ahead of where i was. you're a life saver, man. just need to get x moving now and im back in business! thank you


---
**Sean Cherven** *February 10, 2016 04:17*

Does X do anything? Even jitter/studder?


---
**Tim barker** *February 10, 2016 04:27*

No jitter, stutter or anything. LCD says "x endstop" hit until i disable the endstops and then it says "no move". I plugged the y stepper into the x pins on the board and it runs the y stepper just fine. So with that i'm assuming that the firmware and driver are fine.



Which leaves me with wiring and motor correct?


---
**Sean Cherven** *February 10, 2016 04:31*

Sounds like wiring to me. I'd also check out the endstop if it says endstop is hit. Check and wiring and report back.


---
**Sean Cherven** *February 10, 2016 04:33*

If the LCD says "no move", that makes me think its a firmware issue, because the stepper motors don't have any type of feedback, therefore the controller would not know if the motor has not moved... 


---
**Tim barker** *February 10, 2016 04:37*

I mispoke. the lcd doesnt display anything out of the norm when the endstops are disabled.



Checked the wiring and it is all correct. Is it possible that different motors would have different wiring orders on the ribbon cable?


---
**Sean Cherven** *February 10, 2016 04:41*

With these Chinese units, anything is possible. lol



Try plugging the x-stepper into the y-driver, and see if x moves when u move y from the lcd panel. If it works, then that rules out wiring as an issue.


---
**Tim barker** *February 10, 2016 04:52*

I get nothing when I plug x into y driver. So i guess at this point trying different wiring combinations is my best bet lol


---
**Sean Cherven** *February 10, 2016 04:54*

Pretty much. Report back what you find.


---
**Tim barker** *February 10, 2016 05:02*

Will do. Thank you for the help so far.


---
**David Cook** *February 10, 2016 06:49*

Endstop Testing



No matter which type of endstop you use, it should be recognized by your electronics. Test procedure:



Turn on your electronics/printer.Connect.Send M119 manually.Firmware should report back status "0" or "open".Engage the endstop by pressing the switch, putting cardboard into the light barrier, etc.Send M119 again.Firmware should report "1" or "triggered" or "closed".



Your endstop works if both reports are as expected.



Endstop Troubleshooting



Endstop shows no reaction at all.Measure the endstop signal on the cable connector plugged into your electronics or, even better, the CPU's signal pin. It should swap between < 1 V and > 2 V. If not, it's a wiring or electronics problem.Signal voltage changes, but M119 reports always the same.Then your firmware is misconfigured. Likely it reads the wrong CPU pin. Another possible cause is, you plugged your endstop into a different header.Endstop shows the opposite of the intended reaction.If your endstop reports "closed" while being untouched and "open" when engaged, the signal is interpreted the wrong way. On mechanical endstops you can change the wiring (seeMechanical Endstop). Common to all endstop types is, you can invert signal interpretation in your firmware (config.h, configuration.h, etc.).


---
**David Cook** *February 10, 2016 06:50*

I had to disable max end stops on mine to get it working 


---
**David Cook** *February 10, 2016 06:50*

Used M119 in repetier to verify end stop status until I got it right. 


---
**Tim barker** *February 11, 2016 01:42*

I got it all fixed up! Went through a bunch of wiring combinations but I eventually gave up and bought a multimeter. Started testing everything and it turned out I had some bad soldering on my middleman board (first time really soldering too). But its all worked out. Both axis move fine. And now im just on to setting up personal preferences in the firmware fleshing out the details. Thanks a ton for the help!


---
**Sean Cherven** *February 11, 2016 01:58*

No problem man! Keep that multimeter nearby, you'll be sure to need it in this field. 


---
*Imported from [Google+](https://plus.google.com/113646430521733366133/posts/6xsyWF6WAJv) &mdash; content and formatting may not be reliable*
