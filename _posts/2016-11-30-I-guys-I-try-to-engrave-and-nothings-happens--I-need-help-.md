---
layout: post
title: "I guys I try to engrave and nothings happens  I need help "
date: November 30, 2016 04:59
category: "Original software and hardware issues"
author: "Cesar Padilla"
---
I guys I try to engrave and nothings happens 🤔 I need help 😐


**Video content missing for image https://lh3.googleusercontent.com/-yARbMQ5Ol-4/WD5cwIEl7gI/AAAAAAAAYe4/jmtBNjL_Bh8n5m2Q5KKoXEt9js-Aq0TdwCJoC/s0/20161129_212614.mp4**
![images/43322c2a8a340f0e614793bf3e93335b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43322c2a8a340f0e614793bf3e93335b.jpeg)



**"Cesar Padilla"**

---
---
**Ned Hill** *November 30, 2016 12:50*

You need to check the mirror alignment.


---
**Ian C** *November 30, 2016 12:51*

Hey. Looks to me like your mirror alignment may be out and maybe the X and Y axis rails going by most experiences with the K40. Check the mirrors first using masking tape on the first fixed mirror, then work your way to the head. Read up all you can on mirror alignment as it's a PITA the first time you have to do it. It looks like your piece of paper has been zapped a couple of times, so that could point to bed alignment as well if you get some laser engaging working in certain parts of the travel range only. The laser is firing, so it can only be alignment at this stage


---
**Cesar Padilla** *November 30, 2016 13:16*

I made the alignment last night and the beam hits in the middle of the mirrors 🤔 


---
**Ned Hill** *November 30, 2016 15:36*

Then the beam is most likely hitting the inside of the head.  Check and make sure the lens is seated properly.  I had this happen to me once.


---
**Cesar Padilla** *November 30, 2016 17:09*

I'm going to check that thanks ✌


---
**Cesar Padilla** *December 15, 2016 18:34*

hi guys just to tells that one mirror and the lens was dirty, i just clean and now its working fine thankss for your time :D 


---
*Imported from [Google+](https://plus.google.com/109463094459516755907/posts/QbCcj6wsS8c) &mdash; content and formatting may not be reliable*
