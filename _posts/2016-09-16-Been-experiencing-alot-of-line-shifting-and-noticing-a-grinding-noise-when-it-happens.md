---
layout: post
title: "Been experiencing alot of line shifting and noticing a grinding noise when it happens..."
date: September 16, 2016 05:13
category: "Hardware and Laser settings"
author: "Alex Krause"
---
Been experiencing alot of line shifting and noticing a grinding noise when it happens... I removed the idler pulley from my X axis and saw this... the bracket is bent along with a .5mm burr from where they drilled the hole... looks like I will need to try and straighten things out along with removing the burr :( laser is down and out for the time being... if I can't fix it luckily Lightobjects has a replacement for it for 5$ + shipping 

![images/2156206ecc71d5329488c146d543fd2c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2156206ecc71d5329488c146d543fd2c.jpeg)



**"Alex Krause"**

---
---
**Alex Krause** *September 16, 2016 05:15*

Might be a good idea to order some belt as well. Does anyone know how the pulley that's affixed to the stepper motor is connected? I can see a set screw anywhere to hold it on


---
**Alex Krause** *September 16, 2016 05:31*

![images/fb16e355bc27214d961695ac77328f3d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb16e355bc27214d961695ac77328f3d.jpeg)


---
**Joe Alexander** *September 16, 2016 05:44*

Usually its pinched by a piece of metal and a screw on the laser carriage


---
**Alex Krause** *September 16, 2016 05:55*

**+Peter van der Walt**​ lol I had thought about using a Precision Adjustment Hammer... I will take it to work with me tomorrow and use a vise and a Die grinder ... hopefully good as new... 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 06:23*

Is that from the far right on the X-axis rail? Makes me think I should check my own too since you mention grinding noises.


---
**Alex Krause** *September 16, 2016 06:25*

**+Yuusuf Sallahuddin**​ yes it's the belt tensioner screws that is accessible thru the electronics cabinet hole that is perpendicular to the X axis


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 07:45*

**+Alex Krause** Cool, I'll give mine a check to make sure it isn't awry like yours. Here's hoping :)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/9gXiAMtxa3t) &mdash; content and formatting may not be reliable*
