---
layout: post
title: "Hi guys, loving this group, very helpful"
date: July 23, 2016 18:48
category: "Materials and settings"
author: "Adam J"
---
Hi guys, loving this group, very helpful. Recently got my K40 and after a couple of days finally got the laser lined up nicely. I'm wanting to cut some 2mm and 3mm MDF. Been using LaserDRW to get me started, managed to cut after 3 passes @ 8 m/a. Anybody know what settings I should have? Hear these machines can cut thin mdf in a single pass?





**"Adam J"**

---
---
**Ariel Yahni (UniKpty)** *July 23, 2016 18:53*

Haven't done mdf yet but as its much more dense than other materials I would expect higher power than ply or acrylic,  but 3 passes seems to much. ﻿


---
**Scott Marshall** *July 23, 2016 19:17*

I agree with Ariel, you should be able to zip thru 2 or mm Mdf in 1 pas no problem. 8ma is too low on the power end though, step it up to 18 or 20 for starters 6 - mm/sec.

Plywood is much tougher then MDF and I can cut 3mm Baltic birch 3mm 3 ply in 1 pass, but everything has to be spot on.



We can't get 3mm MDF in the United States yet, not sure why nobody is importing it, but would love to get some. I can cut 6mm homecenter MDF in 2 passes, so it makes sense you should be able to cut 3mm in 1 pass.



If indeed your mirrors are perfectly aligned, I'd guess your focus is the issue.. Try some test cuts, adjusting the work height in 1mm increments, then zero in from there. Start out at 24ma and about 6mm/sec and adjust speed and current for best cut once you find the sweet spot.



You'll want a good air assist to keep the flames damped out at low cutting speeds, otherwise the flames will ruin your lens in short order.



have fun, Scott


---
**Adam J** *July 23, 2016 19:22*

Is it safe to crank up the power so high? Thought it was necessary to keep it under 10ma? Thanks for the speedy reply, most appreciated!


---
**Scott Marshall** *July 23, 2016 19:35*

Yes it is. There's some debate on just how high you have to go to reduce the tube life significantly, but <25 milliamps is pretty well accepted as safe, although I'm sure you'll find a few who disagree. I run my to 29ma on occasion for short runs, and haven't seen any ill effects.



These tubes put out about 30 watts maxed out, which if you crank the numbers thru ohms and watts laws, it gives you around 9% effeciency, so cooling is my personal biggest concern. At 30 watts output, you are carrying away about 400W of heat. Lots of nice cool water flowing thru is your best bet toward long tube life. This is all just my opinion, but I do have a some experience in the field, so it's not just guessing.



There's a few folks on here that may disagree, and I take nothing from them, it's one of those subjects that's kind of subjective, largely because the manufacturer refuses to weigh in, or even to rate the tube correctly.



Hope my thoughts on the subject help.



Scott


---
**Adam J** *July 23, 2016 20:01*

Thanks for your replies! 



I'm also just laying the 3mm mdf on the standard bed. Is that the right thing to do? I've noticed people mentioning on focusing to the center of the material, how do I do this?


---
**Scott Marshall** *July 23, 2016 22:37*

That's your problem. Remove the stock bed and put in the nearest recycle bin. It's the wrong height in the 1st place, and it's not adjustable.



The focus is critical on these, and they can barely cut paper if it's not correct.

A cheap way to get going well is to use a piece of expanded metal or screen and make a set of various height spacer blocks from wood. I use kraft sticks for shimming the last  few mm.



I discovered that they sell a barbecue grill "topper" - a disposable grate made from thing expanded aluminum, and they work great for a laser bed.



There's a lot of other ways df doing it, I also have a "nail bed" which is just a piece of 6mm plywood with wire brads about every 2 cm or so in a grid. It needs to be shimmed as well, but I do a lot of work in 3mm plywood and once it's set to the proper height , I can just lay my plywood on it and go.



As you mentioned, some materials benefit from focusing at different depths inside of the material. (That's how fine the focus needs to be adjusted and how much it matters). On 6mm plywood, I run the 1st pass with the focus set on the top surface, then move the focus down 1-2 mm and finish the cut.



This is made much easier with an adjustable focus air assist head. I like the one Saite cutter on Ebay sells, besides adding the air assist, it uses a bigger, more forgiving lens, protects the lens from smoke and spatter so you don't have to clean it often, and the most important, it allows you to adjust the focus at the cutter head instead of by moving your workpiece up and down.



They're not very expensive, but you will need an air pump or compressor and a regulator/flow control valve  to adjust the flow.



This is the single biggest improvement you can make to the k40, in my opinion.



Here's a link to the "loaded' one, it has the optics all installed.



[http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005](http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005)



When you start thinking about a controller upgrade, I know a guy....



Scott


---
**Ariel Yahni (UniKpty)** *July 23, 2016 22:42*

Is this it **+Scott Marshall**​ Clean BBQ - Disposable Aluminum Grill Liner. Set of 12 Sheets of Grill Topper [https://www.amazon.com/dp/B003SFOCPS/ref=cm_sw_r_cp_apa_gk.KxbB3P6VHK](https://www.amazon.com/dp/B003SFOCPS/ref=cm_sw_r_cp_apa_gk.KxbB3P6VHK)


---
**Scott Marshall** *July 23, 2016 22:50*

**+Ariel Yahni**

No, the ones I have have very thin 'wires' in the typical diamond pattern of expanded metal. They punch slits in the metal and "stretch" it with a hydraulic press.



I found mine in Wal-mart, the seasonal section. I didn't mention it to Pikachupoo because I thought that he/she may not live in a country with Walmart . But then again, they're about everywhere in the free world, I'd have to guess.



I think the cost was about 3 bucks for 2 pieces, anout 14-15" square, and kind of 'dished" up on the edges.



If I see them online, I'll link it here.



Scott


---
**Adam J** *July 24, 2016 01:57*

Thank you for your help. I've ordered one of those, now I just need to be patient for it to arrive. I have an air compressor from an airbrush kit I'm not using. Hopefully I can use that for air assist. And yes, I know the kind of BBQ grill you're talking about, we do have those here in the UK.


---
**Scott Marshall** *July 24, 2016 11:46*

Airbrush compressors seem to do a tolerable job, largely depend on it's size/quality. 

You'll get a feel for the setup you come up with and will be a pro at it after a couple week s of burning away, having fun. 

I think that's the evolution we all go through. Nothing beats getting the basics and going for it.



Have fun, Scott


---
**Pippins McGee** *July 25, 2016 08:45*

i cut 3mm mdf every day, if it helps - when i have my machine in a half-decent alignment, i constantly cut it at at 5ma@5mm/s, or 8ma@8mm/s, or 10ma@10mm/s. i vary between those 3 choices, they all cut 3mm mdf in 1 pass.

for 6mm mdf, i do same but 2 passes.

for 9mm mdf, i do same but 5-6 passes (main issue is not having adjustable bed/laser head to adjust focus between cuts on such thick material...)


---
**Adam J** *July 25, 2016 22:51*

Pippins, thanks for the information. Just wondering what mods you've made from standard?


---
**Pippins McGee** *July 25, 2016 23:10*

no worries, not a lot. just:

lightobject laser head (for air assist), with airbrush compressor (i have a saite_cutter head coming in mail so I can adjust focus for different thickness materials)

& upgraded the extraction fan to a 270cfm bilge blower

& raised the stock bed a little with washers and longer screws, so items are in focus distance of the laser.


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/EuX7ytgmJd6) &mdash; content and formatting may not be reliable*
