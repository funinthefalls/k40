---
layout: post
title: "New toys for the HV lab........ I finally selected a probe for my DVM and also to connect to my scope hopefully without frying them"
date: March 28, 2017 02:14
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
New toys for the HV lab........



I finally selected a probe for my DVM and also to connect to my scope hopefully without frying them.



A bit of overkill but this voltage scares the shit out of me :).



I was attracted to the "Valtage Divider", it can be used to measure my 2.1 jijawatts of power.



Bought here: [https://www.amazon.com/gp/product/B004PA02Q8/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B004PA02Q8/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1)





![images/6de396769515d69ae264d78308868539.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6de396769515d69ae264d78308868539.jpeg)
![images/9c989e45d139561d0c21844c57ab947c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9c989e45d139561d0c21844c57ab947c.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Imnama** *March 28, 2017 17:02*

I can advise you to watch this youtube vid first before 'attacking' high voltages.  
{% include youtubePlayer.html id="-_LJXKLgIYc" %}
[youtube.com - #142: Basics of High Voltage Probes and how to use them](https://www.youtube.com/watch?v=-_LJXKLgIYc)




---
**Don Kleinschnitz Jr.** *March 28, 2017 17:19*

**+Imnama** thanks good video :)


---
**Mark Brown** *March 28, 2017 19:27*



"Valtage divider, compatible with mast multimeters"



Think the O on their keyboard gave out?


---
**Vince Lee** *March 28, 2017 23:05*

Ooh.  Here's one with a built in meter in case you don't want to calibrate for your meter's impedence:  [amazon.com - Tenma 72-6530 High Voltage Probe with Meter - Voltage Testers - Amazon.com](https://www.amazon.com/Tenma-72-6530-Voltage-Probe-Meter/dp/B008DJS750/ref=sr_1_2?s=industrial&ie=UTF8&qid=1490742190&sr=1-2&keywords=high+voltage+meter)




---
**Paul de Groot** *March 28, 2017 23:53*

**+Don Kleinschnitz**​ let us know what your findings are. I assume that these psu's are not that precise. I guess you might find 9kV instead of 15.


---
**Don Kleinschnitz Jr.** *March 29, 2017 01:04*

**+Paul de Groot** 20k probably is peak unloaded. While operating is likely lower...


---
**Don Kleinschnitz Jr.** *March 30, 2017 13:59*

**+Vince Lee** this is the one I would use if I just wanted to measure my machine and its a lot safer. I am going to hook mine to my scope ....YIKES! 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/3y8m2HvrCQB) &mdash; content and formatting may not be reliable*
