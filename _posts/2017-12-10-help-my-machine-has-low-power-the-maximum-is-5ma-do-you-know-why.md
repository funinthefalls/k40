---
layout: post
title: "help, my machine has low power, the maximum is 5ma, do you know why?"
date: December 10, 2017 01:03
category: "Hardware and Laser settings"
author: "Erik Quintana"
---
help, my machine has low power, the maximum is 5ma, do you know why?


**Video content missing for image https://lh3.googleusercontent.com/-aCrK7jHvqCU/WiyH3OYmMvI/AAAAAAAARxY/PnlNhjU4uhckaE5jUbc-j-gAYzMRHo9RQCJoC/s0/VID_20171209_182245.mp4**
![images/cce91b59f76f001ee66e44169a5876d5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cce91b59f76f001ee66e44169a5876d5.jpeg)



**"Erik Quintana"**

---
---
**Hodgkins Family** *December 11, 2017 22:43*

This is exactly the same fault as my machine,  I replaced the dial in the hope that a $10 part would be the fix, but now i am thinking it is going to be the power supply?


---
**Erik Quintana** *December 11, 2017 23:13*

I think in the power supply.


---
**HalfNormal** *December 12, 2017 00:02*

I had a similar problem that actually turned out to be my M2nano board so three items to check, controller board, power supply and tube.


---
**Erik Quintana** *December 12, 2017 00:12*

**+HalfNormal** There is some method to check?


---
**HalfNormal** *December 12, 2017 00:17*

Check out [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/) for trouble shooting tips.


---
**Erik Quintana** *December 12, 2017 00:18*

I'll check the blog, thanks!


---
**Erik Quintana** *January 10, 2018 00:21*

The problem was the power supply, it was changed and now it works.


---
*Imported from [Google+](https://plus.google.com/+ErikQuintana/posts/E2LqTWDjkUU) &mdash; content and formatting may not be reliable*
