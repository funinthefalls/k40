---
layout: post
title: "First Christmas season with my laser, so many things to make :) Found this neat and easy to make tree from Thingiverse ."
date: November 26, 2016 21:31
category: "Object produced with laser"
author: "Ned Hill"
---
First Christmas season with my laser, so many things to make :)  Found this neat and easy to make tree from Thingiverse [http://www.thingiverse.com/thing:37406](http://www.thingiverse.com/thing:37406).  I added a small slot (3 x18mm) below the star to allow ribbon to be threaded through for more decorating options. ﻿ The stand cut files are for 3.0mm mdf, but my 3mm ply was about 3.15mm so I had to sand the slots a bit to get them to fit.  The outer layer is cut from 3 sheets of A4 colored paper.  I used 3 different shades of green, but you can do any colors you like.  You could even cut different toppers and make this a year round tree for different occasions.  



![images/0497fd14f8438a3c21b9489f3c068236.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0497fd14f8438a3c21b9489f3c068236.jpeg)
![images/056a5b39048582981689d4e16a20e924.png](https://gitlab.com/funinthefalls/k40/raw/master/images/056a5b39048582981689d4e16a20e924.png)

**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 26, 2016 23:50*

That's pretty cool. A more durable option than paper would be fabric of some sort or leather (too expensive).


---
**Ned Hill** *November 27, 2016 04:24*

I used heavy scrapbooking paper so it's fairly durable.  Plus at $0.39USD per sheet I'm not overly concerned.  I think fabric might be too droopy (unless you add some starch maybe).  Leather might be kind of interesting though.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 27, 2016 07:11*

**+Ned Hill** I was thinking fabric like Canvas. Although at the prices they charge, it may be a bit overkill for this kind of job.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/RQxNxLEEKpN) &mdash; content and formatting may not be reliable*
