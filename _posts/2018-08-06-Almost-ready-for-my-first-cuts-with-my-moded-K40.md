---
layout: post
title: "Almost ready for my first cuts with my moded K40!"
date: August 06, 2018 17:10
category: "Modification"
author: "Mario Gayoso"
---
Almost ready for my first cuts with my moded K40!

Just need to learn how to use the lightobject 600 w water chiller, connect it and calibrate the laser beam.



**+Adam Hied** did you setup your chiller? Maybe you can help me!



Any help about the first cuts, printing a pattern (link for patterns) for different speeds and power in acrylic and wood, tool to determine the focal distance, etc is more than welcomed!!



Here a link of my Mod Process, hope it helps!



[https://www.facebook.com/media/set/?set=a.10160655040555164.1073741962.592240163&type=3](https://www.facebook.com/media/set/?set=a.10160655040555164.1073741962.592240163&type=3)







**"Mario Gayoso"**

---
---
**Stephane Buisson** *August 07, 2018 07:21*

**+Mario Gayoso** welcome in this community.



focal is fixed at 50.8 mm  mean you can do yourself a little piece of wood acrylic to put between the lens and object to check. (engrave)

when cutting the distance should be adapted from the mid thickness of the material. do a slanted test as proof.

chiller temp : the best temp is about 18/20°C some manufacturer give a 15/30°C range for best operation and increase tube lifespan.



speed and power: some tables exist, but do your own, vary from machine to machine, and sensible to mirrors alignment, air assit, etc ...










---
**Mike Meyer** *August 07, 2018 13:14*

Check out this website...[https://k40laser.se/lens-mirrors/mirror-alignment-the-ultimate-guide/](https://k40laser.se/lens-mirrors/mirror-alignment-the-ultimate-guide/).  Lots of good info on correctly setting up the k40.

[k40laser.se - Mirror alignment - the ultimate guide - K40 Laser](https://k40laser.se/lens-mirrors/mirror-alignment-the-ultimate-guide/)


---
**Mario Gayoso** *August 07, 2018 23:28*

**+Stephane Buisson** Awesome advice Stephane! Cant wait to turn it on, but Im a little afraid to be honest! haha what goes first the chiller, then the machine then test the laser to align? or I dont need to run the chiller to align?




---
**Mario Gayoso** *August 07, 2018 23:29*

**+Mike Meyer** Thanks Mike!  I will take a look! I'm also waiting on someone to help me out with the chiller! In the meantime I', struggling with OCTOPRINT and RASPBERRY PI, anybody knows how to set wifi?


---
**Stephane Buisson** *August 08, 2018 07:14*

**+Mario Gayoso** no need for a chiller at the beginning, (until you laser continously), the water bucket is enought for a start. (temp < 25°C) you could also add ice (interchanging plastic water bottle  in the bucket for not raising your water level).

take time to align your laser that's the most important, if you feel the need to increase the laser power, that could indicated a misaligned laser. try to keep it (<15 mA). Air assist would be your next mod.



Electronic / software : well we don't need a raspberry pi with octoprint (that's for 3D printing), plug your computer to k40 with a good usb cable. then depending on your k40 electronic (choice of 2 Nano/Moshi), choose the appropriate software. post again when you find out yours. 


---
**Mario Gayoso** *August 08, 2018 07:33*

**+Stephane Buisson**  So far I upgraded the following: Zbed, 600w water chiller, 24v fan for the exhaust, duct vent, cut the exhaust to expand the working area, air assist with drag chain, lights, C3D mini, glcd screen, control panel with switches for air, exhaust,lights and the chiller. Lightburn as far as software and I have a couple more coming.I took all apart and put together something completely different but is turning on, I just need to align and shoot the laser for the first time. I hope to do it this weekend. Do you have a website or facebook page where you put your work?


---
**Stephane Buisson** *August 08, 2018 07:41*

**+Mario Gayoso**, my work hummm !!!  let say I look over this community on a daily basis, all informations are here grouped by categories on the left. you have plenty of links to the best page on the web as well as youtube. please take the time to dig in it. also "search in community" work great. also look for **+Don Kleinschnitz** website.


---
**Mario Gayoso** *August 09, 2018 18:13*

**+Stephane Buisson**  Tonight I will try my first attempt to shoot the laser. I will align the laser and then do a ramp test.



Chiller goes on first, then ,machine, then laser, then test right?




---
*Imported from [Google+](https://plus.google.com/+MarioGayoso/posts/6PMUFcTMETC) &mdash; content and formatting may not be reliable*
