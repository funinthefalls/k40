---
layout: post
title: "Ok I am really panicking now I was in the middle of cutting some 3mm ply wood and my laser just stopped The head is moving but there is no laser what so ever I have looked at the laser tube it looks perfect I don't know what"
date: June 01, 2016 04:34
category: "Discussion"
author: "Donna Gray"
---
Ok I am really panicking now I was in the middle of cutting some 3mm ply wood and my laser just stopped The head is moving but there is no laser what so ever I have looked at the laser tube it looks perfect I don't know what has happened Helpp!!! Can anyone give me a clue as to what has happened I have turned it off then back on again checked all the leads etc





**"Donna Gray"**

---
---
**Anthony Bolgar** *June 01, 2016 04:38*

If you manually press the test fire button, does the machine output as laser beam? Also, make sure the laser enable switch is on. When you press the test fire button, does the mA meter needle move?


---
**Donna Gray** *June 01, 2016 04:40*

No nothing it is like it has just stopped working The Ma meter doesn't move when I test fire


---
**Derek Schuetz** *June 01, 2016 04:43*

Check to see if your power supply is still hooked up correctly. Or your tube is dead


---
**Donna Gray** *June 01, 2016 04:52*

I have checked power etc I have gone all over the tube it looks perfect I have only had the machine for 2 months


---
**Anthony Bolgar** *June 01, 2016 04:57*

Some Power supply's have a test button and a LED light. If yours has one, press the test button on it and see if the little red LED lights up.


---
**Derek Schuetz** *June 01, 2016 04:57*

I had mine for a week and my tube died but I was also arcing so idk. But apparently you can always visually tell of the tube is bad


---
**Donna Gray** *June 01, 2016 05:10*

I have looked all over the tube it looks exactly like it did when I bought it  I am hoping it us not the laser tube surely it would last longer than 2 months I haven't used it that much


---
**Donna Gray** *June 01, 2016 05:12*

I will unplug all the leads and replug them in again tonight and see what happens I can't see it being power as the laser head was still moving there was just no laser beam at all


---
**Donna Gray** *June 01, 2016 05:13*

How do I get it fixed in Australia what type of business would fix it for me


---
**Derek Schuetz** *June 01, 2016 05:21*

You most likely hair have to install a new laser tube


---
**Stevie Rodger** *September 10, 2016 08:20*

I think my tube died after just 4 months, and light use. Just the same scenario as above,  Is there a fault with these tubes,  I would have expected to have a lot more use out of my tube than i have.


---
*Imported from [Google+](https://plus.google.com/103145403582371195560/posts/diwcWGUhTrZ) &mdash; content and formatting may not be reliable*
