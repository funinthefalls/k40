---
layout: post
title: "Does anyone know where I can get a clean version of CorelLaser?"
date: March 06, 2017 14:32
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Does anyone know where I can get a clean version of CorelLaser?

The file that came with the laser and everything I find via google search sets off my norton antivirus and I can't install...





**"Nathan Thomas"**

---
---
**Ned Hill** *March 06, 2017 14:43*

It happens with all versions.  You just have to have Norton ignore it.


---
**Nathan Thomas** *March 06, 2017 14:49*

**+Ned Hill** gotcha, thank you sir


---
**Mark Brown** *March 06, 2017 14:50*

Norton thinking it's a virus probably has something to do with "Laser" trying to access Corel's files, which seems virus-ey.  But that's the only way for it to work.  So yeah, disable Norton for the install.


---
**Ned Hill** *March 06, 2017 14:58*

After you get it installed you will need to go into the device initialization settings and make a few changes.  If your control board says M2 nano then make sure the mainboard is set for M2 like shown below.  Next there will be a serial number for your board, usually on a white sticker on the board.  Write down the number and enter it into the device ID field.

![images/8a7b4532ae49422e8c6766cbcc05c042.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8a7b4532ae49422e8c6766cbcc05c042.png)


---
**Ned Hill** *March 06, 2017 15:47*

I would also recommend these settings in the CorelDraw settings.  For earlier versions of CD (like version 12) the "Engrave data" needed to be set to WMF, but I found that with newer versions of CD I had less issues using BMP in that field.

![images/7b162cbcfd6b2780c16e2245e1ffdbb8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7b162cbcfd6b2780c16e2245e1ffdbb8.png)


---
**Nathan Thomas** *March 06, 2017 18:46*

I was about to ask that lol! I see some ppl go with JPG/PLT and others both set to WMF. Is there a difference...does it matter much?


---
**Anthony Knapkin** *March 10, 2017 21:46*

I didnt think anyone still used Norton. I hope you dont pay for it.


---
**Steven Whitecoff** *March 12, 2017 03:06*

I think which cut and engrave data type you use depends on the source file going into Corel, the main thing is if your cuts/engraves have issues with placement, try different setting for the data, for me HPGL did the trick. But thats history now that I have the Cohesion Mini running the show.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/EHZWTVaYn6T) &mdash; content and formatting may not be reliable*
