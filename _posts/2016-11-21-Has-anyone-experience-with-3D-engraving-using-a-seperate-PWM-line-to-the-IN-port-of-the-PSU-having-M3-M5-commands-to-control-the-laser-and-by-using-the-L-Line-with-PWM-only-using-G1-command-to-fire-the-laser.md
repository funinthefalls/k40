---
layout: post
title: "Has anyone experience with 3D engraving using a seperate PWM line to the IN port of the PSU having M3/M5 commands to control the laser and by using the L-Line with PWM only using G1 command to fire the laser"
date: November 21, 2016 21:16
category: "Smoothieboard Modification"
author: "ViciousViper79"
---
Has anyone experience with 3D engraving using a seperate PWM line to the IN port of the PSU having M3/M5 commands to control the laser and by using the L-Line with PWM only using G1 command to fire the laser. I use PWMed L line and adjusted pot to 18mA. The results are disappointing. It looks washed out in the lower layers when engraving greyscale. I am thinking about to add the PWM line to IN and test it once more...

![images/f99c37e5529558850ac26b96cfecc897.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f99c37e5529558850ac26b96cfecc897.jpeg)



**"ViciousViper79"**

---
---
**Alex Krause** *November 21, 2016 22:11*

Is this MDF?


---
**Alex Krause** *November 21, 2016 22:19*

M3 m5 commands don't play well with the laser module of smoothie


---
**ViciousViper79** *November 21, 2016 22:25*

hm ok... so no good idea to do extra pwm line. it is plywood quite thin though.


---
**Alex Krause** *November 21, 2016 22:28*

Try solid wood for grayscale engrave... different layers of ply have the grain direction alternating and glue between layers to contend with... 


---
**Alex Krause** *November 21, 2016 22:31*

Also the layers inside of ply are a lower grade of wood... 


---
**Don Kleinschnitz Jr.** *November 22, 2016 00:11*

**+ViciousViper79** before you move PWM to the IN line make sure that your system is working as expected while engraving. 



I recommend running a grey scale test and if that works but does not look good I would suggest something is wrong with your settings. 



If it creates greyshade moving to the IN line with the POT will likely NOT help. Its my opinion that moving PWM to IN is not a good idea in any case.

<s>--------------</s>

If you connect to the IN with the pot installed the actual PWM will not reflect what the control software is expecting. The DF will be biased by the product of the pot and the external PWM values. In fact the amount of bias error you create will vary with your pots position. 



What this means is that if the pot is set a 50% power when Laserweb asks for 50% it will be getting .5 * .5= 25% and so on. Its actually isn't that simple because the % setting on the pot and the control it creates in the LPS is not linear.



When using the L line for normal operation with a pot installed you should have it set to full on and let the controller vary the power. The only reason I leave a pot in is for an extra safety factor when testing changes, other that that its full on.

<s>----------------</s>

If you are set up right the controller will not exceed the limits set in the configuration and you will not exceed where you want the tube to operate i.e less than 18 ma.

Verify your max power by using the test button to find out what max power is with the pot full on. 

Then calculate what % you want the max limit of be and set that in the configuration file.

I advise testing by sending G codes for various power levels and verifying that it stays within limits on the current meter. Make sure the Gcode keeps the laser on long enough to get a good reading on the analog meter.

Alternately you can do this from a GLCD panel with the "LASER" function.

After this is set up the controller should have full and accurate control of the power in the range you want. 



If you want the technical foundation for my advice go here: [http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)

[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)


---
**ViciousViper79** *November 22, 2016 00:30*

Thanks for the hints! I also got a recommendation that M3/M5 commands are not that good with Smoothieboard. I think I am starting to get into it. It is a long process of trial and error on how to work out the correct numbers for movement speed and laser power. Actually I get quite good results by setting the pot to 6ma and limiting the laser to 10%-30%. I will post later. The way I do it right now is to set the pot to 18ma, which I don't want to exceed and then let the smooethieboard use power values from 0.0 to 1.0 for the laser with allowing max PWM which will hit 18ma. I guess that might be the scaling problem for my greyscale. I will try your approach.


---
**ViciousViper79** *November 22, 2016 00:31*

![images/ca3fb92470f9b30d7062e6db91238c45.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca3fb92470f9b30d7062e6db91238c45.jpeg)


---
**ViciousViper79** *November 22, 2016 00:32*



Best results so far: Proportional feed rate: 200-75. Laser power: 5% - 30%. Pot set to at about 6ma.

![images/48320757ded7b6d089fe10b79fdb9bf9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/48320757ded7b6d089fe10b79fdb9bf9.jpeg)


---
**Vince Lee** *November 22, 2016 00:37*

I use a similar setup (PWM the Fire line with pot connected), and found with grayscale tests that I had to turn down the PWM frequency (increase the period) by a factor of 10 or so to get a reasonably linear output.  My guess is that this can vary lots from PSU to PSU.  I have the pot connected because my adapter board can switch back and forth between the original Moshiboard.  I kind of like having the pot connected even when using my Smoothie as the plywood I cut varies a bit from sheet to sheet, and a few times I've been able to make slight adjustments in the middle of a job to compensate.  I just have to calibrate the power with a test fire at the start.


---
**ViciousViper79** *November 22, 2016 00:46*

Yes, that is how I tink about the setup, too. I also have  the moshi board still in place and can use it as a fallback... though I don't think it will ever happen. Cutting and engraving is so easy now with Inkscape and LaserWeb. It was frustrating using the CorelLaser stuff. I tried using 200 as PWM frequency but that resulted in oddly shaped circles so I guess it somehow messed things up. Although it pretty much makes no sense to me...


---
**Don Kleinschnitz Jr.** *November 22, 2016 01:19*

**+Vince Lee** you can do that exact same thing with the L and pot connected. 


---
**Don Kleinschnitz Jr.** *November 22, 2016 03:24*

**+ViciousViper79** changing the PWM frequency changed shape of circles...I need to think about that one :)


---
**ViciousViper79** *November 22, 2016 11:37*

**+Don Kleinschnitz** Yes... I know that it sounds weird. I could only think of inteference disturbing signal for the steppers or something like that...


---
**ViciousViper79** *November 22, 2016 15:56*

Testing with acryl glas now and results are now predictable. So I guess plywood is really bad for engraving. The reason why it is so blurry is because the lighter layer is evaporated and then engraving on the darker middle layer gives completely different results. Any recommendations on woods that suite engraving? I tested some spruce but the veins give unpleasant results.


---
**ViciousViper79** *November 22, 2016 16:42*

I think the reason for the deformation when adjusting the PWM frequency was the work piece moving. Had the same issue and realized the acrylic glas was no longer aligned to the frame. I will give 5KHz another try.


---
**Vince Lee** *November 22, 2016 18:19*

**+Don Kleinschnitz** The reason I'm using the Fire line is because the adapter I made that switches between the moshiboard and smoothieboard, so it uses the original wire harness and plugs in between the smoothieboard and PSU.  Using the same Fire line as the moshiboard was the easiest plug-and-play solution and thus the first one I tried.


---
**Don Kleinschnitz Jr.** *November 22, 2016 19:53*

**+Vince Lee** :) actually both fire and L are connected together inside the lps.


---
*Imported from [Google+](https://plus.google.com/+ViciousViper79/posts/Jzp1GJpdKzi) &mdash; content and formatting may not be reliable*
