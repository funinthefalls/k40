---
layout: post
title: "My K40 is engraving quite alright when it's in top left corner, but when it goes further to the right it losses the mA power and does not engrave"
date: February 05, 2016 12:03
category: "Discussion"
author: "Patryk Hebel"
---
My K40 is engraving quite alright when it's in top left corner, but when it goes further to the right it losses the mA power and does not engrave. Any suggestions? :/





**"Patryk Hebel"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 05, 2016 12:15*

Yep, align your mirrors more precisely so that the beam is more centered in all 4 corners of the cutting table. I had same problem early on because my mirrors weren't aligned well enough.



Oh, I just registered you said it loses mA power. Do you mean that the mA meter shows a lower value?


---
**Patryk Hebel** *February 05, 2016 12:16*

**+Yuusuf Sallahuddin** Ok. I will try that :)


---
**Joseph Midjette Sr** *February 05, 2016 12:19*

Sounds like a mirror alignment issue. If alignment is off at the first or second mirrors it will worsen as the distance increases from the head. Check alignment on all starting with the one closest to the tube. Dead center. There are guides on here and youtube that will show you exactly what you need to do...


---
**Patryk Hebel** *February 05, 2016 12:52*

**+Yuusuf Sallahuddin** Yeah I guess my mirrors still were not aligned well. In the process I burned my thumb, but I guess it was worth. Cheers buddy for help. :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 05, 2016 14:43*

**+Patryk Hebel** Yeah it can give some pretty nasty burns. I've shot myself a fair few times while aligning it, but as you said, well worth having it aligned better.


---
*Imported from [Google+](https://plus.google.com/102509166997588398801/posts/PkGuSWw1YcP) &mdash; content and formatting may not be reliable*
