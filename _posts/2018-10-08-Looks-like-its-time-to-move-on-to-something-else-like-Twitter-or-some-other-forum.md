---
layout: post
title: "Looks like it's time to move on to something else like Twitter or some other forum"
date: October 08, 2018 20:07
category: "Discussion"
author: "Jarrid Kerns"
---
Looks like it's time to move on to something else like Twitter or some other forum.  Any thoughts?





**"Jarrid Kerns"**

---
---
**Adrian Godwin** *October 08, 2018 21:00*

Please, not twitter. And an even bigger please, not facebook.

I can't say I'll miss google+, it has a lot of annoyances but I've managed to cope by reading most things in email. 



Once upon a time I'd have said Yahoo, but that's had several unpopular revisions and all the groups I used there have dried up. 



Why is these communications companies go to such trouble to attract users and then piss them off or abandon them ?


---
**Adrian Godwin** *October 08, 2018 21:01*

Just a thought - is there a way to archive the postings here before the axe falls ?




---
**David Davidson** *October 08, 2018 21:37*

Reddit?




---
**David Piercey** *October 08, 2018 21:53*

What about an actual forum, hosted by cohesion?




---
**Don Kleinschnitz Jr.** *October 08, 2018 21:59*

IMO everything but this is UGLY! I find G+ users to be more of a technical crowd. What the hell happened!


---
**James Rivera** *October 08, 2018 22:07*

**+Jarrid Kerns** why is it time to move on? What is wrong with this Google+ community? I find it to be very useful, and people are responsive.


---
**James Rivera** *October 08, 2018 22:07*

**+Adrian Godwin** Before what "axe falls"?!?  Am I missing something here?


---
**Don Kleinschnitz Jr.** *October 08, 2018 22:09*

Evidently Google seed G+ as a failure and the scope of recent security issues is causing them to give up! Supposedly they will give us a way to keep the content. We will see what happens between now and Aug 2019 when G+ dies.


---
**Jarrid Kerns** *October 08, 2018 22:12*

**+James Rivera** Google it bro


---
**Don Kleinschnitz Jr.** *October 08, 2018 22:22*

Try this:

 [accounts.google.com - Sign in - Google Accounts](https://takeout.google.com/settings/takeout)



I am trying the archive creation.... while I am holding my breath.


---
**HalfNormal** *October 09, 2018 00:56*

Everybody is welcome at [www.everythinglasers.com](http://www.everythinglasers.com) no BS, no advertisements, just people enjoying lasers.

[everythinglasers.com - Home - Everything Lasers](http://www.everythinglasers.com)


---
**Stephane Buisson** *October 09, 2018 15:29*

Don't go away to fast, this community is still alive, and I will make my possible to make the content available to anybody in the future.



I am looking at what other community manager are doing, open to any suggestions, back up tools  etc...).

 

also "Google will announce new Enterprise-focused products for Google+ in the near future".



dear Members please keep posting .






---
**HalfNormal** *October 09, 2018 15:40*

**+Stephane Buisson** you have done an outstanding job on cultivating a community here. I will continue to support this group until the bitter end. Rather than see everyone scatter, it will be nice to have a place to end up and keep the community together. I will also offer to mirror this group as well.


---
**Nick Spirov** *October 10, 2018 07:57*

It's too early for decisions, let's see what alternatives are there and also, Google will help export the data. When they decided to scrap the Google Reader, eventually a great product emerged out of it - Feedly, and there was a flawless transition to it.


---
**Don Kleinschnitz Jr.** *October 10, 2018 13:15*

**+Nick Spirov** heh never heard of Feedly! Looking now...


---
**Adrian Godwin** *October 11, 2018 21:33*

**+Don Kleinschnitz Jr.** In another g+ group, [groups.io](http://groups.io) was recommended. I don't know anything more about it myself but could be worth a look.


---
**Jarrid Kerns** *October 12, 2018 13:32*

Couple communities are testing different sites. I'm trying mewe and it's good for chat and q&a, but doesn't seem it would work for a knowledge resource.  Sadly, I  haven't found many that do.  It does have hashtag categorization, so we are giving that a run. 


---
**Duncan Caine** *October 17, 2018 09:38*

**+Stephane Buisson** Hi Stephanie, as the Owner, I assume you have all our contact details and would be able to contact us all when our new 'group home' is found after G+ ends


---
*Imported from [Google+](https://plus.google.com/115581185804501674768/posts/6hMQZYro7gC) &mdash; content and formatting may not be reliable*
