---
layout: post
title: "Do you own a K40 or other Chinese laser clone?"
date: May 25, 2016 07:30
category: "Discussion"
author: "Alex Krause"
---
Do you own a K40 or other Chinese laser clone? Are you not satisfied with the status quo of the Corel laser software? Are you wanting to upgrade your machine to more reliable hardware? If you answered yes to any of the previous questions then please join me on helping fund **+Peter van der Walt**​ in acquiring a K40 for himself. The Man,the Myth,The Legend lives in a region of the world that makes it very difficult to ship a K40 affordably (South Africa).I have faith that we as a community can come together and help out this extremely talented Man in acquiring a K40 to better serve all of us in his development of Laser Web 2. Any little bit helps in this venture and all donations are much appreciated. If you feel compelled to help please make a donation at the following link [openhardwarecoza.github.io/donate](http://openhardwarecoza.github.io/donate) 





**"Alex Krause"**

---
---
**Stephane Buisson** *May 25, 2016 08:39*

done, but look like it take time to update.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 09:35*

I didn't know Peter didn't have a K40. I will chuck some across next pay to help out.


---
**Jeremie Francois** *May 25, 2016 09:50*

BTW **+Peter van der Walt** did you think about trying to contact the company or people who make the Corel laser software? May be you could get funded or sponsored and ship yours instead? That would be a win for everyone :)


---
**Jeremie Francois** *May 25, 2016 10:07*

**+Peter van der Walt** yep I guess so, but you lose nothing trying... Some Chinese companies are willing to get real reputations (which does not seem to be the case here, right!) :D


---
**Stephane Buisson** *May 25, 2016 11:31*

**+Jeremie Francois** if only the chinese could sell a K40 with better mirrors & lens and without proprietary board. that could be a first step, then adding open source board make sense as they don't wish (not able) to do any support.

the best of each world, cheap K40, open source hard and soft, community support.

but it's too much asking to them. (they just don't reply to email). if the community convert many K40, that will start to make sense with volume.


---
**Ariel Yahni (UniKpty)** *May 25, 2016 11:51*

If someone know as **+Peter van der Walt**​says what the top level is I can have someone in China contact the directly


---
**Jeremie Francois** *May 25, 2016 12:04*

**+Stephane Buisson** actually this is a real issue... I guess you are right :s


---
**Stephane Buisson** *May 25, 2016 13:03*

**+Peter van der Walt** I did have a contact from some Youtube comment (video manual) 2 years ago.

i think it could be our member +Joy Yang (many in G+ look from pict)


---
**Stephane Buisson** *May 25, 2016 13:05*

**+Peter van der Walt** 

[https://plus.google.com/+JoyYangwm03511/posts](https://plus.google.com/+JoyYangwm03511/posts)



[http://www.shenhuilaser.com/](http://www.shenhuilaser.com/)


---
**Jim Hatch** *May 25, 2016 13:06*

Done. Thanks for the good work.


---
**Ariel Yahni (UniKpty)** *May 25, 2016 13:40*

**+Stephane Buisson**​ tx. **+Peter van der Walt**​ would you like to contact them or want me to forward it to my Chinese agent?   If so will need to come up with a scaled strategy on what we want from them


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 14:46*

**+Ariel Yahni** Some kind of barebones assembly would be cool, if we are wanting to be able to use opensource controller & software. All they'd need to provide on their end is frame/gantry/psu/tube. I can't imagine they would reduce the overall cost much however, since the controller board would be the only thing they're not including. But never know & can't hurt to ask.


---
**Jeremie Francois** *May 25, 2016 15:38*

Also he may be told he could be interested in building up a real company/brand as others did in China. Reputation is the next step for many, and he could get real recognition this way (win-win again! ) :)



(I am amazed how efficient it can be! possibly putting a south African guy in contact with an unknown Chinese manufacturer with a local agent in less than 5 hours?!).


---
**Stephane Buisson** *May 25, 2016 15:53*

**+Peter van der Walt** check your mail just now


---
**Ariel Yahni (UniKpty)** *May 25, 2016 16:24*

You guys let me know how it goes


---
**Jeremie Francois** *May 25, 2016 16:42*

idem :)


---
**Alex Krause** *May 25, 2016 16:51*

:)


---
**Jim Hatch** *May 25, 2016 16:53*

Did mine come through? Should have made that a bit better 😃


---
**Alex Krause** *May 25, 2016 17:09*

Yah I put a note to seller


---
**Alex Krause** *May 25, 2016 17:10*

Lol Copeland is my son's name it's my eBay sellers account paypal


---
**Jim Hatch** *May 25, 2016 17:12*

**+Peter van der Walt**​ great. The github page showed 100 when I threw mine in the pile so your post made me think it didn't go through (possible PayPal issue) and I would re-do it. 


---
**Don Kleinschnitz Jr.** *May 25, 2016 23:08*

Talking about a better machine?: I still think a solid $400 base laser cutter/engraver unit without the controller is possible. Then guys like me just add a smoothie and laser web. If I had this to do over I would build from scratch, but my k40 is a good teacher....


---
**Casey Cowart** *May 27, 2016 04:39*

Halfway there! 😂


---
**Jim Hatch** *May 27, 2016 12:39*

Woohoo! Glad to see folks rallying around. It's not like we'd be able to buy the software (purpose built for our needs) for less than a reasonable donation ☺


---
**Manuel Conti** *May 28, 2016 06:45*

Done. Tnks for your job! 


---
**Alex Krause** *May 28, 2016 16:56*

Hope you are getting closer :)


---
**Ariel Yahni (UniKpty)** *May 28, 2016 17:18*

Sea freight almost always have hidden cost, specially since most of them are not Door to Door. 


---
**Ariel Yahni (UniKpty)** *May 28, 2016 17:23*

Well being near the port it's a plus, there are many cases where air freight it's cheaper or even a tiny bit more expensive but much faster delivery. The K40 cost me from Miami to Panama 180.00 by air


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/4proncSbRie) &mdash; content and formatting may not be reliable*
