---
layout: post
title: "How would you deal with a \"hiccup\" on a customers personal possessions that you have been tasked with engraving?"
date: October 22, 2015 19:28
category: "Discussion"
author: "Peter"
---
How would you deal with a "hiccup" on a customers personal possessions that you have been tasked with engraving? Power cut resulting in half an engraving, pet jumps up and material moves in the machine etc.

Can you cover yourself with insurance of some kind? Should you? What about stock wasted due to similar issues?





**"Peter"**

---
---
**Ashley M. Kirchner [Norym]** *October 22, 2015 23:19*

The same way folks that do picture framing protect themselves: put it on paper for the client to sign. While you will do your utmost to protect their piece, things can (and in many cases have) happen. You need to make it clear that some things are simply out of your control (power outage? Yeah, blame the mother in law for that one.)


---
**I Laser** *October 22, 2015 23:51*

Can't imagine clause or no clause your customers would be recommending your services if you destroy something they've given you to engrave.



Personally I just wouldn't take on one time jobs.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 23, 2015 08:02*

I concur with Allready Gone & Ashley M. Kirchner. I think that some kind of clause of something they sign when they commission the job is necessary. Maybe not on on all items, but maybe on items over a certain value.


---
*Imported from [Google+](https://plus.google.com/+PeterJones79/posts/HrkixM8P9Kh) &mdash; content and formatting may not be reliable*
