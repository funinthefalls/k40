---
layout: post
title: "What does a normal laser tube look like when in use?"
date: November 11, 2017 15:53
category: "Original software and hardware issues"
author: "Chris Hurley"
---
What does a normal laser tube look like when in use? 


**Video content missing for image https://lh3.googleusercontent.com/-TWULiofr1MQ/WgcdBcjc_qI/AAAAAAAAMgw/EfFpbiiT824CT2pbKPGD7g4axsLCZhEkwCJoC/s0/VID_20171111_122054.mp4**
![images/523cbe7f0ec5377caac5185ac758c610.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/523cbe7f0ec5377caac5185ac758c610.jpeg)



**"Chris Hurley"**

---
---
**Don Kleinschnitz Jr.** *November 11, 2017 16:08*

Looks pretty normal to me. Are you having a problem?


---
**Chris Hurley** *November 11, 2017 16:18*

It always seems as if people's 40k's are cutting better. I guess I'm second-guessing my self. 


---
**Anthony Bolgar** *November 11, 2017 18:34*

Alignment of the mirrors can make a huge difference even if you are only off center by a mm. Other reasons for peoples success can be from replacing the mirrors and lens with higher quality ones, the cheap lenses actually absorb quite a bit of the lasers energy. I tested one K40 that had 35W at the tube, by the time it got to the lens it was down to 25W due to cheap mirrors. 


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/DPhqyyH8bQ6) &mdash; content and formatting may not be reliable*
