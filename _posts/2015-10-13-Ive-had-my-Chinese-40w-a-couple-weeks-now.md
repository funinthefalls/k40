---
layout: post
title: "I've had my Chinese 40w a couple weeks now"
date: October 13, 2015 17:33
category: "Object produced with laser"
author: "Coherent"
---
I've had my Chinese 40w a couple weeks now. I seem to have all the alignment, air assist and replacement table working... This was yesterdays .jpg engraving test after pulling out the old air duct and table with the spring clamp thing. 1/4 birch about 6" (135mm) round, 140mm speed at about 3 power. I'm pretty happy with the precision and detail these things can obtain. Eventually I'll do a Lightobject X7 DSP upgrade, but for now it does work!

![images/d31bdf6dbcdc3d93d2b2ba8c525c233a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d31bdf6dbcdc3d93d2b2ba8c525c233a.jpeg)



**"Coherent"**

---
---
**Phillip Conroy** *October 13, 2015 17:58*

Would love to try this file on my laser cutter,how long do]id it take


---
**Coherent** *October 13, 2015 18:07*

About 30 minutes I think. Here's where the file is from. I think you can click on it to enlarge and then right click save?

[http://www.123rf.com/photo_9156242_vector-maya-calendar--over-white.html](http://www.123rf.com/photo_9156242_vector-maya-calendar--over-white.html)



But if you search google for mayan calendar there are tons out there.


---
**Phil Willis** *October 13, 2015 19:21*

What did you use for a replacement table? I am at the same stage and want to replace the engraving table.



I was looking at doing something like this: 



[http://www.thingiverse.com/thing:273050](http://www.thingiverse.com/thing:273050)


---
**David Cook** *October 13, 2015 19:52*

That is cool !


---
**Coherent** *October 14, 2015 18:41*

Phil, I used some 3/4 aluminum angle close to what's in the Thingiverse photo. I used 4 short pieces of 80/20 extruded aluminum to attach the sides together, then tapped down through each piece of 80/20 with a 1/4-20 tap. I threaded 4" or so pieces of threaded rod in each piece, then attached a "foot" and knob to each threaded rod I printed on a 3d printer. Basic, but I can raise/lower the bed by turning the 4 corner knobs/legs. It even allows for odd shaped pieces if needed. There are apps for your phone or tablets that work like a spirit level to level it quickly. The bed itself is  just expanded metal (cheap duct cover grid from Home Depot) pop riveted to the frame.  I had most on hand, so only cost a couple dollars.


---
**Phil Willis** *October 14, 2015 21:26*

**+Marc G** Thanks, most useful.


---
*Imported from [Google+](https://plus.google.com/112770606372719578944/posts/cYUFp7QcjQ6) &mdash; content and formatting may not be reliable*
