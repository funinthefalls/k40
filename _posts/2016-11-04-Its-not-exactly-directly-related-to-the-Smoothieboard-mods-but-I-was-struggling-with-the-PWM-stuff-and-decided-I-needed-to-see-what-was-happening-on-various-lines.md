---
layout: post
title: "It's not exactly directly related to the Smoothieboard mods but I was struggling with the PWM stuff and decided I needed to see what was happening on various lines"
date: November 04, 2016 11:14
category: "Smoothieboard Modification"
author: "Darren Steele"
---
It's not exactly directly related to the Smoothieboard mods but I was struggling with the PWM stuff and decided I needed to see what was happening on various lines.  So I bought a Seeed DSO Nano.  I hooked it up to 2.4 and decided I either didn't know how to use the scope [ it's been a while since I last used an oscilloscope in anger ] or that things weren't behaving as I expected.



So I pulled out a Raspberry Pi, knocked up a bit of Python to drive a GPIO line and hooked up the DSO Nano.  Given a known input source I was pretty much able to work out how to control the Oscilloscope.  Next step...proper mucking about with the Smoothieboard PWM...and then probably adopt the technique pioneered by **+Don Kleinschnitz** - if it works with my LPSU :)

![images/4c8f8ac5e051a55a23aa29c8596cb32a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c8f8ac5e051a55a23aa29c8596cb32a.jpeg)



**"Darren Steele"**

---
---
**Don Kleinschnitz Jr.** *November 04, 2016 12:14*

Nice, let me know if you need any help.


---
**Arthur Wolf** *November 06, 2016 08:32*

Neat :) I never fully learned how to use my DSO and I have a logic analyzer now that I like much better.


---
**Don Kleinschnitz Jr.** *November 06, 2016 11:37*

I have 2 scopes and a logic analyzer. Then **+Peter van der Walt** dangled one on bangood  in front of me. Its been on back order forever :)


---
**Don Kleinschnitz Jr.** *November 06, 2016 12:06*

**+Peter van der Walt** the unit is finally on the way. God I hope its the new version??


---
*Imported from [Google+](https://plus.google.com/+DarrenSteele/posts/XB6owAMvo9q) &mdash; content and formatting may not be reliable*
