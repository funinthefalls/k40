---
layout: post
title: "So I am having an issue with software I think now that I got my machine up and running I think all of a sudden I am having a software issue the shapes I am trying to cut out does not match what is on the computer"
date: February 23, 2016 16:26
category: "Discussion"
author: "3D Laser"
---
So I am having an issue with software I think now that I got my machine up and running I think all of a sudden I am having a software issue the shapes I am trying to cut out does not match what is on the computer.  It seems it's larger than what they should be anybody ever had this issue  

![images/705389c8aa760d72935022ef9722eb57.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/705389c8aa760d72935022ef9722eb57.jpeg)



**"3D Laser"**

---
---
**Jim Hatch** *February 23, 2016 16:38*

Which software you're using would be helpful. Also, there's been some discussion recently on Corel vs laser sizes and the configuration settings you need to make sure you get out what you think you're sending.


---
**3D Laser** *February 23, 2016 16:53*

I'm using laserdrw I only started having this issue recently as well before I took it all all apart it was wasn't cutting great but marked exactly where it needed it be now it seems off the laser even starts at a different spot now to than before and I don't think I changed any of the settings 


---
**Jim Hatch** *February 23, 2016 17:19*

I had an alignment problem where the laser hit off to the side of where I thought it would (direct downward). It was caused by bad mirror alignment and skewed XY gantry alignment. Check the laser focusing lens hold plate to make sure that's level & square as well. Someone also noted they had an issue with their laser tube not being aligned parallel to the machine housing & X gantry. That will kick off the alignment as well.



Taking it all apart means a whole bunch of things have changed and now need to be realigned.


---
**Thor Johnson** *February 23, 2016 17:28*

Is the above cut with just a single pass (or is the laser scanning each line several times) -- if it's a single pass, then I think your focus is way off (or you have a deposit on the focus lens).


---
**3D Laser** *February 23, 2016 17:35*

It's a single pass but multiple objects on the board being cut out 


---
**3D Laser** *February 23, 2016 17:37*

I think the picture is misleading I will take a screen shot and a picture of the cut out when I get home so you can see what it should look like  and how it came out 


---
**Ben Walker** *February 23, 2016 17:40*

Corey, Have you made cure that the laser cutting software has the correct model number selected?  The number will be on the board in the service door.  I had to use my phone to take a photo of it to make sure.  Before I had the right number selected my machine did all kinds of random marks.  Nothing discern-able as shapes but it did act very erratic.


---
**3D Laser** *February 23, 2016 17:43*

Yes all that is fine it seems that things are cutting at like 110% size rather than 100%


---
**Ben Walker** *February 23, 2016 17:52*

hmmm.  I have not run into anything like that.  Of course I am almost as new to this as you are.  That said I made sure I designed in mm instead of inches or cm's.  I'm curious if you have your design grid somehow out of dimension.  Does not sound like a mechanical issue.  Now I am going to have to start measuring my keychains.  (!!!).  When I get off work and get back to my calipers.


---
**3D Laser** *February 23, 2016 18:01*

Yea it's bazar I don't know what is happening.  at first I though I plugged in the stepper motor in the wrong port but that wasn't the case I don't know if the file was corrupted or not


---
**Ben Walker** *February 23, 2016 18:05*

Is it double cutting?  Had that happen (but not at 10% bigger).  When I design in illustrator I have to set path at .001mm or it makes those wide cuts during the second cut.  Once you get it dialed in it will cut 1/4 inch clean and perfect.  No sanding even required anymore.  I just wipe off the smoke residue.


---
**3D Laser** *February 23, 2016 18:08*

I am just making squares and circles in laserdrw at the moment 


---
**HalfNormal** *February 23, 2016 19:25*

**+Corey Budwine** Did you get the lens spacers?


---
**3D Laser** *February 23, 2016 19:55*

**+HalfNormal** I did thanks again.  I had to shave them a bit but they work great.   


---
**HalfNormal** *February 23, 2016 19:56*

I'm glad it worked out for you.


---
**3D Laser** *February 23, 2016 19:57*

**+HalfNormal** yes I couldn't have done it without you. I owe you big 


---
**Jim Hatch** *February 23, 2016 20:23*

What do you have the laser set for beam size in LaserDRW. Those lines look awful thick for cutting lines. There's a setting in the pop-up that shows up when you tell it to cut (I'm traveling so can't look it up right now). It's where you set things like X/Y position, repeat, etc.


---
**3D Laser** *February 23, 2016 20:29*

I am not for sure I have the lines set to .001 when I drawled the shapes but I didn't change any other settings what should they be at


---
**Jim Hatch** *February 23, 2016 20:45*

There's another place you can set the line thickness in when you actually send it to cut. If no one gets you an answer sooner I'll check when my flight lands tonight and I can get home to my laser & laptop.


---
**3D Laser** *February 23, 2016 20:46*

**+Jim Hatch** so you know what it should be set at


---
**3D Laser** *February 23, 2016 23:15*

Got home and checked I don't see where you can change the line width help pleade


---
**Thor Johnson** *February 24, 2016 00:38*

Click on the shape, and the  right side pops up.  See [http://imgur.com/gallery/XF8T3rj](http://imgur.com/gallery/XF8T3rj)


---
**Jim Hatch** *February 24, 2016 02:15*

What Thor said (or pic'ed) :-) Also check the Step parameter on the top of that screen. Also, when you click on Engrave the popup has a Properties button in the upper right. Click that and check the parameters in the Engraving Machine Properties box that will pop up. Check the Resolution (3rd box down on the left) - mine is 400 dpi. (You can also get to that screen by clicking on File|Device Initialize) I think those are the right values (they work for me anyway).


---
**Jim Hatch** *February 24, 2016 02:19*

Have you done the focus test? Where you lower the bed or what you're using for a bed and then put a piece of wood on an incline to about 25mm below the laser head at the high end (so you don't whack it). Then run a cutting line. You'll see where the line is best cutting and then set your material for that height from the laser head. the lines you've got look like they're either doubled up or defocused a bit so they're thicker vs the tight cut you're looking for. (which is okay when you're engraving acrylic by the way because it helps smooth the etch lines)


---
**3D Laser** *February 24, 2016 02:20*

I figured out the issue it was a glitch in the file one piece was cutting 10mm shorter than it should I have to redo the file and it should work 


---
**Tony Sobczak** *February 25, 2016 08:40*

Good info.  Thanks. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/JiHCnxvYyXx) &mdash; content and formatting may not be reliable*
