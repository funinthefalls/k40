---
layout: post
title: "Whats the file format you use on your laser cutter software or to prepare for laser cutting"
date: July 28, 2016 19:49
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Whats the file format you use on your laser cutter software﻿ or to prepare for laser cutting 





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *July 28, 2016 21:19*

To follow up on FB group poll [https://m.facebook.com/questions.php?question_id=711159539015732](https://m.facebook.com/questions.php?question_id=711159539015732)


---
**Josh Rhodes** *July 28, 2016 21:37*

SVG because inkscape is where it's at!


---
**Anthony Santoro** *July 28, 2016 23:10*

Files created in Illustrator, converted to AI v9, opened in CorelLaser as CDR.

It works great!


---
**Jim Hatch** *July 28, 2016 23:49*

Use DXF from AI or Corel which is the only format that works with both lasers. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 29, 2016 00:23*

I used to use AI9 format, until I upgraded to CorelDraw X5 with CorelLaser. Now I solely use SVG.


---
**Alex Krause** *July 29, 2016 03:10*

Dxf exported from vectric... because it has awesome nesting features and is less cluttered than inkscape for the vector work I do


---
**David Cook** *July 29, 2016 04:29*

dxf from my cad mostly﻿ (Solidworks or AutoCAD LT)


---
**Jose Salatino** *July 29, 2016 06:40*

I use my own sketch in processing to obtain gcode from  sgv  files


---
**Justin Mitchell** *July 29, 2016 08:19*

EMF exported from Inkscape, as it's the only format that LaserDraw seems to import sanely, and i never could seem to get CorelLaser to work properly.


---
**dstevens lv** *July 30, 2016 01:26*

The choice of tool largely depends on what kind of work I'm doing.  My machine host is a dedicated Corel X8 machine.   For the vector cuts I import dxf, usually cleaned up or created in Autocad.  Pretty much all the cutting I do using this method.



For engraving and art I import  the files into X8 or create them in X8.  The CAM tools typically don't have features many artists use in creating or editing content.  Files that I import include svg, pdf, bmp, jpeg, png and others.



At the very least I see dxf and svg as being the absolute minimum.  They'll be the most portable and users have access to tools like Freecad and Inkscape as open solutions. 


---
**Craig** *August 24, 2016 09:27*

Have to use (import) jpg or png.  Any SVGs created with inkscape, when opened in Corel laser, look like someone grabed random nodes and dragged them up to a corner.  Not one design opens correctly ( but they do in inkscape).  ):


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 24, 2016 10:22*

**+Craig Cyburt** Are you using the Corel Draw 12 that came with the K40? I found svgs from Illustrator would be the same when imported into CD12 also. When I swapped to CDx5 (or greater) the SVG random nodes/gibberish issue was removed.


---
**Jim Hatch** *August 24, 2016 11:32*

**+Craig Cyburt**​ I have the same problem. I'm using Corel X8. I think it depends on how the shapes were created in Inkscape - poly tool, line to line & then joined, etc. I'm not a huge user of Inkscape so I haven't dug into why. When I get a file like that from someone I open it in AI, export as DXF and then use Corel.



I think Inkscape can produce a couple or three types of SVG formats and it could be one is the format that Corel doesn't like but I don't know for sure and it's not something I lose sleep over since I have a workaround.


---
**Ariel Yahni (UniKpty)** *August 24, 2016 11:41*

Almaybe try in insulate stroke to path or object to path. One of those could fix that


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/gUcihTHZXRz) &mdash; content and formatting may not be reliable*
