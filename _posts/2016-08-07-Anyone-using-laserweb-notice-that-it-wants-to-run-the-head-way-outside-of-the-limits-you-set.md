---
layout: post
title: "Anyone using laserweb notice that it wants to run the head way outside of the limits you set?"
date: August 07, 2016 21:25
category: "Software"
author: "Alex Hodge"
---
Anyone using laserweb notice that it wants to run the head way outside of the limits you set? For instance, I set a 200x200mm bed dimension in laserweb settings...open a small clip art pic to test, scale it to 50x50mm placed in bottom left. Generate gcode. Lo and behold the first movement lines are saying to go to Y345! Obviously this causes the head to slam repeatedly against the end of it's travel. Still thinking it's moving, laserweb now thinks it's at Y345 (which of course its, not) so it then moves back down to what it thinks is Y50 (but it's not). You get the picture. Anyone got any ideas how I can stop this from happening? Thanks in advance!





**"Alex Hodge"**

---
---
**Alex Krause** *August 07, 2016 21:29*

Can you refresh your browser reload the image you were testing on a take a fresh screen shot for me?


---
**Alex Krause** *August 07, 2016 21:32*

Installing limit switches on Y min, and X max will stop the crashing if you enable a few setting in config file


---
**Alex Hodge** *August 07, 2016 21:40*

done. [https://goo.gl/photos/vr96ToBo3P61QFe86](https://goo.gl/photos/vr96ToBo3P61QFe86)


---
**Alex Krause** *August 07, 2016 21:44*

You have your image to import in the top left part of the screen... change the setting to bottom left its in the tool tab of the settings 


---
**Alex Krause** *August 07, 2016 21:45*

It is generating a bad offset because of that y-147


---
**Alex Hodge** *August 07, 2016 21:46*

**+Alex Krause** I have a Y max endstop installed and enabled but laserweb is ignoring it. I know it's fine as its the stock optical that until an hour ago I was using as Y min. That still doesnt address why laserweb would be wanting to run my machine 147mm outside of the defined limits.


---
**Alex Hodge** *August 07, 2016 21:51*

**+Alex Krause** That's true, but I wanted it to import to the top left normally. In this case I moved it down by using the offset. It reflects the proper positioning in the GUI. So, why would it generate a bad offset?


---
**Alex Hodge** *August 07, 2016 21:53*

what would be the normal way of moving a picture around the bed? Do I always have to leave the picture in the bottom left? What's the point of the setting allowing you to import to top left?


---
**Alex Krause** *August 07, 2016 22:27*

It's an issue that needs fixed on the GUI I will try and add more detail of what is happening wrong with it on github issues tonight to try and get it fixed it has to do with how the file is imported and that it is placing the image at the top of the screen and not telling the offset it's up there... visually it is at the top of the user interface but laserweb still thinks the image is at 0,0


---
**Ariel Yahni (UniKpty)** *August 07, 2016 22:30*

Limits are set in the firmware not on LW. Those settings will show your bed size and other future enhancements. If you are trying to run out of bounds then there no way LW can prevent that as of now. 


---
**Alex Krause** *August 07, 2016 22:33*

**+Ariel Yahni**​ I tried it last night and it generated the overlay of the gcode outside of the viewer window I will open an issue tonight I only had a few minutes to play yeasterday


---
**Ariel Yahni (UniKpty)** *August 07, 2016 22:36*

it will yes but i believe just cosmetic which need to be sorted out indeed. I just did it myself also to confim


---
**Alex Krause** *August 07, 2016 22:39*

**+Alex Hodge**​ install mechanical endstops... the smoothie users guide recommends them... also putting them on both the min and max of the travel and setting your config file to stop all motion if limit is detected is also recommend... especially for a flying death ray ;)


---
**Alex Hodge** *August 08, 2016 15:09*

**+Alex Krause** The one I installed for my new y min was an extra mechanical I had laying around. I suppose I will have to get some more cable chain and wire up an x max as well. The stock optical endstops work just fine for homing. I'm not sure why y max is being ignored. Ah wait, in my smoothie config I need to enable alpha and beta limit enable yeah? This will just stop the job dead in it's tracks if one of the endstops is triggered while it's running?


---
**David Cook** *August 09, 2016 20:14*

In Marlin I could set a software end stop.  would be neat to do this in Smoothie.  there is probably a way but I have not found it yet.


---
**Alex Krause** *August 09, 2016 20:16*

Try a pull request I think this issue has been fixed


---
**Alex Hodge** *August 09, 2016 20:21*

**+Alex Krause** It was up to date as of Sunday, unless this was fixed/changed since then.


---
**Alex Krause** *August 09, 2016 20:23*

Try it again there was an update this morning with a please test note by the issue on github


---
*Imported from [Google+](https://plus.google.com/106033318557602563181/posts/DhTQX7uvwtS) &mdash; content and formatting may not be reliable*
