---
layout: post
title: "Hey, has anyone got any idea why after some time the corellaser cut, engrave and other buttons in the top right corner disapperar after some time working in coreldraw?"
date: September 03, 2017 14:11
category: "Original software and hardware issues"
author: "Tadas A\u0161"
---
Hey, has anyone got any idea why after some time the corellaser cut, engrave and other buttons in the top right corner disapperar after some time working in coreldraw? 

![images/127f7f0a5682b019b5569b2cf4cfcc4f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/127f7f0a5682b019b5569b2cf4cfcc4f.jpeg)



**"Tadas A\u0161"**

---
---
**Martin Dillon** *September 03, 2017 14:21*

The only thing I can think of is that your USB port stops working and then Corel will not work if it can't see the dongle.


---
**Tadas Aš** *September 03, 2017 14:22*

I unplugged the dongle and the buttons remain there 


---
**Tadas Aš** *September 03, 2017 14:22*

![images/9314a1bf7596a8e1b55ba4003f89bfb7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9314a1bf7596a8e1b55ba4003f89bfb7.jpeg)


---
**Tadas Aš** *September 03, 2017 14:23*

These. I don't think it's the dongle that causes it


---
**Martin Dillon** *September 03, 2017 14:55*

It only checks every  so often.  leave it out for a while and see if it happens.


---
**Chris Hurley** *September 03, 2017 15:17*

I usally power cycle everything and it's good to go. 


---
**HalfNormal** *September 03, 2017 15:18*

Actually this is very normal. The buttons are going to be in one icon in the bottom right corner of the computer next to time/date. You need to right click on the icon and then you will see the information. The icon is going to be reddish pink in color.


---
**Tadas Aš** *September 03, 2017 15:19*

Guys at one of the facebook groups said it's a glitch and the buttons appear down by the clock


---
**Tadas Aš** *September 03, 2017 15:22*

**+HalfNormal** and how about the buttons in the top left corner? The file, edit, view and help? Sometimes there are only 4 of them and sometimes 11 with text, bitmaps, layout, arrange and others. Do these hide somewhere else too?


---
**HalfNormal** *September 03, 2017 15:24*

That is a behavior of the drawing program and it all depends on which screen you're in. A quick search on YouTube will help you learn the drawing program


---
**Tadas Aš** *September 03, 2017 15:29*

Okay, will do, thanks!


---
**Phillip Conroy** *September 03, 2017 19:58*

So meting to do with vba,I had same problem with Rd works on my k50 so I just assigned a keyboard key to start it


---
*Imported from [Google+](https://plus.google.com/105123664639790704570/posts/U2sPk5WLoNm) &mdash; content and formatting may not be reliable*
