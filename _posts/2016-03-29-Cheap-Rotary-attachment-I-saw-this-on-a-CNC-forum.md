---
layout: post
title: "Cheap Rotary attachment I saw this on a CNC forum"
date: March 29, 2016 17:27
category: "Modification"
author: "HalfNormal"
---
Cheap Rotary attachment



I saw this on a CNC forum. Looks like it can be modified and used in the K40 fairly easily. $90 USD



[http://www.ebay.com/itm/191714031261?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/191714031261?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT)





**"HalfNormal"**

---
---
**Jean-Baptiste Passant** *March 29, 2016 20:23*

I have no idea how this thing work, and I cannot imagine using it as a rotary attachment for the k40. 



Any more information on how to use it ?


---
**HalfNormal** *March 29, 2016 20:31*

It is used as a drive for a roller attachment like this one

[http://www.thingiverse.com/thing:299018](http://www.thingiverse.com/thing:299018)


---
**Scott Marshall** *March 30, 2016 01:25*

The gear reduction may be problematic if you aren't running software that allows you to set the drive ratios. That drive is aimed at milling machine users to turn a rotary table. Mach 3 (and virtually all other CNC setups) allows you to set the steps/inch (or mm), so the gear reduction is easily accomodated and provides improved precision.



A stepper motor (200 step/rev. I'm pretty sure) and drum with idler wheels is all you really need, just do the  math based on the steps /rev and belt drive to determine the drum dia to match the travel of the Y axis in the K40 and then you can just plug into the Y output on the board when you want to roll engrave.

20 bucks and some inginuity out to do it.﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 07:15*

If you are going to be building stuff to attach to this to use as a rotary, you could probably do the same from scratch for similar or cheaper price & not have to compromise your design to "fit" this part. One of the users here (**+Allready Gone**) posted some images of his rotary attachment setup in the past & although they are unable to be found, I believe Stephane added them to the PDF file that is stickied at the top of the group main page.


---
**MNO** *March 30, 2016 09:18*

Yeap... i think so also. If You already have a laser cutter, build one Your self. 

check what steper You have for Y axis and find something like it. Do some math to find out well size to match travel distance of Y axis...

Cut parts and put them together...

This is how i would do. Of course You can get one from China.  But that is no fun :)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/QL6HivKkopF) &mdash; content and formatting may not be reliable*
