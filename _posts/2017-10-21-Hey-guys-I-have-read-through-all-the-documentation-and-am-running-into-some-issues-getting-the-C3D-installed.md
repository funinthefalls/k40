---
layout: post
title: "Hey guys, I have read through all the documentation and am running into some issues getting the C3D installed"
date: October 21, 2017 00:36
category: "Smoothieboard Modification"
author: "Abe Fouhy"
---
Hey guys, 



I have read through all the documentation and am running into some issues getting the C3D installed. I guess my questions are:



1. There are a lot of connections on the C3d that are duplicates, I have a power below the aux power header that is a +24VDC/GND connector by terminals and then on listed for the K40 that has the laser fire, gnd +24VDC. I am assuming I use the k40 connections.

2. What are the terminal screw mosfet connections for?

3. I have a set of pins for end stops and another series of end stops broken down by the mosfet terminals. Can I just use the one by the k40 power plug? My endstops use 3 wires, 1 brown, 1 blue, 1 black for each endstop. So i have two blue, two black going into my old controller connector and two brown going into the 24vdc side on the plug. What is the pinout on the C3D connector, which pin is 24vdc, gnd, and signal for the endstop connector?

4. How can I expand it to use the ethernet ports?

5. There is a marking for the k40 PWM outuput, how/why should I hook this up?



Thanks in advance all!





**"Abe Fouhy"**

---


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/BJzC3cwpwzv) &mdash; content and formatting may not be reliable*
