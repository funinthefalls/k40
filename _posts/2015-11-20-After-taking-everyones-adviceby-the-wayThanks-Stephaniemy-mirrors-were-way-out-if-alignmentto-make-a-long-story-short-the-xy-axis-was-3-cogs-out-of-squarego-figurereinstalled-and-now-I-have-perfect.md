---
layout: post
title: "After taking everyone's advice...by the way...Thanks Stephanie...my mirrors were way out if alignment....to make a long story short the x,y axis was 3 cogs out of square...go figure...reinstalled and now I have perfect"
date: November 20, 2015 23:33
category: "Discussion"
author: "Scott Thorne"
---
After taking everyone's advice...by the way...Thanks Stephanie...my mirrors were way out if alignment....to make a long story short the x,y axis was 3 cogs out of square...go figure...reinstalled and now I have perfect alignment...Thanks everyone for the tips.﻿





**"Scott Thorne"**

---
---
**Sean Cherven** *November 21, 2015 00:11*

Mine was out of square also. It was causing some real strange offsets when cutting.


---
**Scott Thorne** *November 21, 2015 00:14*

Yup....couldn't get the laser head to align properly....it's good now...not to mention the x axis had a rough spot when moving it manually...now it's smooth as a baby's a@@!﻿


---
**Gary McKinnon** *November 21, 2015 02:43*

Did the tape-test and mine is very slightly offline with x at max and y at zero, but negligible for the moment.




---
**Scott Thorne** *November 21, 2015 10:42*

here was something wrong,  when I ran the x all the way out it was over 1/8 of an inch  off to the right, it's spot on now....I used a speed square to check it.﻿


---
**Sean Cherven** *November 21, 2015 12:33*

Yeah that's the exact issue I was having. Squaring it fixed the problem.


---
**DIY3DTECH.com** *November 25, 2015 01:12*

Any pointers on squaring it up?   As since I have enlarged the bed have found that to be an issue too. Thanks...




---
**Scott Thorne** *November 25, 2015 01:31*

I used a speed square to align it, but I would imagine a tape measure would be more accurate.


---
**DIY3DTECH.com** *November 25, 2015 03:04*

I have machinist squares and the like but what part I guess I am asking do I loosen and re-tighten to square up?


---
**Scott Thorne** *November 25, 2015 10:06*

You have to jump teeth on the cog belts to square it up.


---
**Scott Thorne** *November 25, 2015 10:09*

When the head was close to the 2nd mirror it was dead center of the head mirror hole, when I moved it all the way out as far as it would go it was off, I couldn't get it aligned no matter what I did, that's when I found out the x,y was out of square


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/g4LTjgFnkVv) &mdash; content and formatting may not be reliable*
