---
layout: post
title: "So I definitely love this community, I love it in such a way, that I'm concerned about the migration of the valuable content that exists here, since the entire Google+ service will end its activities"
date: December 12, 2018 00:10
category: "Discussion"
author: "Fabiano Ramos"
---
So I definitely love this community, I love it in such a way, that I'm concerned about the migration of the valuable content that exists here, since the entire Google+ service will end its activities.



Apparently due to an API bug Google decided to close the service months earlier.

Is there a way to help? What are the plans to migrate to the community?



[https://gbatemp.net/threads/google-will-shut-down-earlier-than-announced-due-to-another-api-bug-and-possible-data-leak.525829/](https://gbatemp.net/threads/google-will-shut-down-earlier-than-announced-due-to-another-api-bug-and-possible-data-leak.525829/)







**"Fabiano Ramos"**

---
---
**Don Kleinschnitz Jr.** *December 12, 2018 00:51*

This just feels strange. Google can't fix an API bug... NOT!

Something else going on!


---
**James Rivera** *December 12, 2018 01:18*

**+Don Kleinschnitz Jr.** Settle down...put away the tinfoil hat. ;-)  I suspect Google are just not allocating resources to this project, and want to minimize any further "black eyes" in the press, especially for a product that doesn't generate any money (at least not directly?).


---
**Don Kleinschnitz Jr.** *December 12, 2018 03:58*

Na!  my bet is something better is in the works. Google will not give away all of the social network real estate. BTE most of Google software does not generate $ directly ;). 


---
**Dave Durant** *December 12, 2018 04:00*

I think closing it down because of bugs they fixed before anybody knew about this is BS. 



More likely some G exec doesn't like it and was looking for a reason. 


---
*Imported from [Google+](https://plus.google.com/111567065741989535353/posts/68PTVE1xkqe) &mdash; content and formatting may not be reliable*
