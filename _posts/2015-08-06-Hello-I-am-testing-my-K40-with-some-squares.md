---
layout: post
title: "Hello, I am testing my K40 with some squares"
date: August 06, 2015 19:37
category: "Hardware and Laser settings"
author: "Educentral Service"
---
Hello, I am testing my K40 with some squares. They should be lined up correctly, but the result looks like this. Any ideas what is going wrong?

![images/6e230e021a4ce0ec22321506e9cd2413.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6e230e021a4ce0ec22321506e9cd2413.png)



**"Educentral Service"**

---
---
**Anton Fosselius** *August 06, 2015 20:53*

Running too fast or lose belt.﻿ so either your steppers are skipping steps or your belt is slipping/jumping threads. I would guess lose belt based on your results.


---
**Joey Fitzpatrick** *August 06, 2015 22:40*

Like Anton said.  It appears that you have a loose belt on your X axis. make sure that both x & y axis move smoothly without binding.


---
**Educentral Service** *August 07, 2015 05:51*

Thank you!


---
**I Laser** *August 07, 2015 07:30*

Another possibility, I had similar issue where stuff wouldn't line up. It turned out I had the wrong motherboard set.



So if the belts are fine, double check the model number on the laser and in the settings match.


---
**Tim Scheck** *October 30, 2015 18:20*

Did you ever fix this problem? 


---
*Imported from [Google+](https://plus.google.com/108538131529508928325/posts/aMhxGw6De4y) &mdash; content and formatting may not be reliable*
