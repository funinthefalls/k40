---
layout: post
title: "Anyone US based have a spare middleman board and connector they want to sell?"
date: August 13, 2016 02:22
category: "Smoothieboard Modification"
author: "Jeff Lawrence"
---
Anyone US based have a spare middleman board and connector they want to sell? I only need one and thought someone may have done the OSHpark thing and got an extra couple boards they have no use for.



I don't see any for sale on the designers site anymore 





**"Jeff Lawrence"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 13, 2016 03:19*

I'd be happy to order a set from OSHPark and solder up a few Middle Man boards if you/ other people need. 

I make a smoothie powered board that is currently being tested by a few people from this community, it has the ribbon cable connector on board so with it people don't need to mess with adapters boards like this, it's a drop in upgrade for the K40. 

Let me know what you need. 


---
**Thor Johnson** *August 13, 2016 20:11*

I have a few (did the osh park thing).  


---
**Jeff Lawrence** *August 14, 2016 16:27*

Do you have an extra connector also? If you are interested in selling let me know what you want for a set to 63141 - My email is Lawrence_jeff at 

[hotmail.com](http://hotmail.com)


---
**Ray Kholodovsky (Cohesion3D)** *September 26, 2016 03:11*

**+jim jam** this never happened, I think he found another option.  You don't need this if you're getting my mini board, as the ribbon connector is built into the mini.  Can I put you on the waiting list for when I run the first batch with the factory?


---
*Imported from [Google+](https://plus.google.com/+JeffLawrence/posts/gsrkpfGU3LV) &mdash; content and formatting may not be reliable*
