---
layout: post
title: "this has a shit load of free dfx files...enjoy guys"
date: May 15, 2016 01:10
category: "Repository and designs"
author: "Scott Thorne"
---
this [http://mydxf.blogspot.com/2010/07/](http://mydxf.blogspot.com/2010/07/) has a shit load of free dfx files...enjoy guys. 





**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 15, 2016 01:17*

Comes up with an error saying the site doesn't exist.


---
**Scott Thorne** *May 15, 2016 01:27*

[http://mydxf.blogspot.com/2010/07/.](http://mydxf.blogspot.com/2010/07/.)..**+Yuusuf Sallahuddin**...try that..it works on my computer


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 15, 2016 01:44*

**+Scott Thorne** Yep that's good :)


---
**Scott Thorne** *May 15, 2016 01:52*

**+Yuusuf Sallahuddin**...it has some cool files...and lots of them. 


---
**Scott Thorne** *May 15, 2016 01:54*

Ok...I edited the post **+Yuusuf Sallahuddin**


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 15, 2016 02:48*

**+Scott Thorne** Just did a test cut of a cool little aztec looking bird from one of those files. Came out pretty nice. Minimal editing of the dxf files required. Thanks for sharing.


---
**Scott Thorne** *May 15, 2016 02:53*

**+Yuusuf Sallahuddin**...anytime my friend.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/3ZjzLq229Bo) &mdash; content and formatting may not be reliable*
