---
layout: post
title: "Hello. Just got my laser cutter unboxed and it had broken parts I repaired what needed but am attempting to align the mirror"
date: March 11, 2017 22:08
category: "Discussion"
author: "John Bellico"
---
Hello. Just got my laser cutter unboxed and it had broken parts I repaired what needed but am attempting to align the mirror. What side of the lender faces down? And why if I have the beam hitting the head would it not come out the bottom? Also got the beam to come out once and it was a half moon shape not a point any help please 





**"John Bellico"**

---
---
**Jim Hatch** *March 11, 2017 22:38*

The bump on the lenses faces up. If there's no bump but there is a dimple then that side goes down.



If the beam hits the head mirror off center it may hit the side of the nozzle. That will cause a half-moon shape.


---
**John Bellico** *March 11, 2017 22:47*

No bumps or dimples. 


---
**John Bellico** *March 11, 2017 22:47*

One side has a reflection and the other is concave 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 11, 2017 22:52*

Reflection is up, concave is down.


---
**John Bellico** *March 11, 2017 22:54*

Thankyou


---
**Mark Brown** *March 11, 2017 23:05*

What I did. Rotate the head to be sure that mirror is facing straight towards the #2 mirror, then adjust the #2 mirror to center the beam in the head.


---
*Imported from [Google+](https://plus.google.com/103554525240934204428/posts/h5hdSaiDhxF) &mdash; content and formatting may not be reliable*
