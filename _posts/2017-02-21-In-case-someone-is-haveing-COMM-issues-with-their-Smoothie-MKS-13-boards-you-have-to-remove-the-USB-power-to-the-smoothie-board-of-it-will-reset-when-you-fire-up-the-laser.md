---
layout: post
title: "In case someone is haveing COMM issues with their Smoothie/MKS 1.3 boards you have to remove the USB power to the smoothie board of it will reset when you fire up the laser"
date: February 21, 2017 01:07
category: "Smoothieboard Modification"
author: "jeremiah rempel"
---
In case someone is haveing COMM issues with their Smoothie/MKS 1.3 boards you have to remove the USB power to the smoothie board of it will reset when you fire up the laser.



[https://en.wikipedia.org/wiki/USB](https://en.wikipedia.org/wiki/USB)





**"jeremiah rempel"**

---
---
**Damian Trejtowicz** *February 22, 2017 12:43*

So,all problems with communication should gone when i remove power from a plug?

Where You find this idea?


---
**jeremiah rempel** *February 22, 2017 13:46*

In the copious amounts of reading I ran across it and it makes total sense.  You have two voltage regulator circuits connected which leads to very large voltage spikes.  Or that is what I understand.



I just put some clear nail polish on the #1 pin of my USB cable and BAM the MKS started working you would expect it to.  Now when my smoothie gets here I'll replace the MKS and put that into my 3D printer to get rid of the Arduino Melzi board.


---
**Damian Trejtowicz** *February 22, 2017 19:43*

I will try this at weekend and see how its work


---
**jeremiah rempel** *February 23, 2017 05:00*

Good luck.  I don't want to hurt the feeling of the creators of the smoothie by helping out people with MSK's but I believe this will help both camps.  People like myself didn't even know about the smoothie.  I ordered the MKS the same day I ordered the Laser.  Eventually I'll have both now.



And this stupid auto correct is really making me sound not so smart with it "Fixing" my "grammar".  Maybe My grammar isn't as good as I though... LOL.


---
**Damian Trejtowicz** *February 23, 2017 12:37*

I had the same,everyonewas talking about smoothieware and i didnt know mks its not genuine


---
*Imported from [Google+](https://plus.google.com/106888670206217097592/posts/aFDP5vkGE6q) &mdash; content and formatting may not be reliable*
