---
layout: post
title: "Whats your source for air assist? Add suggestions or your setup bellow, pump capacity, pressure, etc"
date: August 22, 2016 11:59
category: "Hardware and Laser settings"
author: "Ariel Yahni (UniKpty)"
---
Whats your source for air assist? Add suggestions or your setup bellow, pump capacity, pressure, etc 





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *August 22, 2016 12:00*

FB group pol for reference [https://goo.gl/n5xf2S](https://goo.gl/n5xf2S)


---
**Phillip Conroy** *August 22, 2016 12:52*

Other -fullsize 2hp-50liter air tank had moisture problems on focal lens-fixed by adding 1meter 25mm wide pvc pipe filled with silca gel beadsjust before air assist-adjustable pressure regulator at laser cutter-1-2 psi etching and for cutting 3mm mdf 10psi or more if wanting perfectly clean botton of cut mdf peace


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 22, 2016 14:58*

Dual air pumps - unknown flow/pressure. Pathetically weak. Will be upgrading to a higher powered pump when I get around to it.


---
**Scott Marshall** *August 22, 2016 18:18*

I'm Lucky enough to have a shop air system. Back in my 1st All-Tek days, we ran a lot of air equipment in our panel shop and did a bit of painting on panel boxes and machinery frames as well, so I invested in a good air compressor and distribution system. It's still going after 35 years, 3 pumps and a couple pressure switch/starters (original motor though!) I'm amazed the tank hasn't rusted through yet - it will now that I brought it up... 

It's unlimited air for this application, but I quickly found less is more for air pressure on assist heads. It's  filtered/regulated down to 5psi at the Laser and then the flow is reduced down by a flow control valve.

The 7 1/2hp  80 gallon compressor is in another building so I don't hear it at all. I timed it once and it cycles about every 23 minutes when I'm Lasering (took some doing as I don't do many engravings that big).



I'm looking into buying a few pumps to test. 

I'm designing a complete system for people who want an easy to install kit. Suggestions on the best quality pumps or your experience is welcome. (also on junk so I don't waste my time and money on them)



I've designed  a nice plug and play air assist kit but still need a quality pump to include as an option. 



So if you have a pump you love - (or hate) I'd appreciate your thoughts on it. Please tell me what head you're using and how loud it is.



We could use a "review" section on pumps anyhow, I hear occasional comments on them, but there doesn't seem to be a "go to"  favorite as there is with many K40 accessories. 



Scott


---
**Robi Akerley-McKee** *August 23, 2016 05:09*

I'm using an older select comfort air mattress pump for my air assist.  Before that it was a 1960's Craftsman diaphragm air pump.  New pump is less air but a heck of alot quieter!


---
**Kirk Yarina** *August 23, 2016 12:00*

Will be switching from an aquarium pump to the shop air compressor after running  some piping


---
**Phillip Conroy** *August 23, 2016 12:07*

Try and run the first 5 meters on a slight angle slopping up and install a drain tap at bottom of rise,or just unplug hose every couple of days with pressure in line at the time and see how much water comes out of line.....


---
**Other World Explorers** *August 23, 2016 19:36*

I just use a simple Lakewood 8 inch fan. I tried using low psi from my 60 gal compressor but I needed a more broad coverage rather than a focused point if air. 

Lol works perfectly for my needs. 


---
**Akos Balog** *August 25, 2016 08:33*

Tried compressor (with 24L tank), but run out of air very quick, even with using only low (0.5-1 bar) pressure on the output.

I've bought an airpump (Hailea ACO-328), and it is perfect. 

tip: use high diameter tubing till you can, and smaller only at the end, where needed, so it will have smaller resistance (and higher airflow/pressure at the end).


---
**Jorge Robles** *March 26, 2017 11:49*

**+Akos Balog** I can confirm that! Now has twice flow!


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/eJmKSsSw4SD) &mdash; content and formatting may not be reliable*
