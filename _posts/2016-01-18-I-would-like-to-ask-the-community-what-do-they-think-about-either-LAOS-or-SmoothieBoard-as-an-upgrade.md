---
layout: post
title: "I would like to ask the community what do they think about either LAOS or SmoothieBoard as an upgrade?"
date: January 18, 2016 15:17
category: "Modification"
author: "Sunny Koh"
---
I would like to ask the community what do they think about either LAOS or SmoothieBoard as an upgrade? The original board is driving me up the wall as CorelLaser is not taking the speed parameter correctly during the engraving. Or am I doing something wrong.





**"Sunny Koh"**

---
---
**Stephane Buisson** *January 18, 2016 17:54*

I am a very happy man with Smoothie/Visicut. Laos was the way to go b4 Smoothie, but price wise Smoothie is now the solution.

if U could wait a bit -> SmoothieBrainz (**+Peter van der Walt** )

if not original Smoothieboard, Azteeg, ...


---
**Joseph Midjette Sr** *January 18, 2016 18:56*

Go here >>> 
{% include youtubePlayer.html id="4L7t1su-xu0" %}
[https://www.youtube.com/watch?v=4L7t1su-xu0](https://www.youtube.com/watch?v=4L7t1su-xu0)



This explains how to adjust speed in Corel. This guy was having the same problem you are desrcibing. Hope it helps


---
**Sunny Koh** *January 19, 2016 04:34*

**+Joseph Midjette Sr**  Thanks, that worked. At least it gives me some breathing room to properly evaluate one of these.


---
**Sunny Koh** *January 19, 2016 04:36*

**+Peter van der Walt** I am trying to understand what are these. Having search Google, cannot quite understand the objective of the SmoothieBrain project.


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/df6Ya23DdJq) &mdash; content and formatting may not be reliable*
