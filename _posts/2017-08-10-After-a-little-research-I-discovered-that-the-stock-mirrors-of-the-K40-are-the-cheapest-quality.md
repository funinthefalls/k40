---
layout: post
title: "After a little research I discovered that the stock mirrors of the K40 are the cheapest quality"
date: August 10, 2017 21:09
category: "Discussion"
author: "bbowley2"
---
After a little research I discovered that the stock mirrors of the K40 are the cheapest quality.  I just received a set of SI mirrors.  Now al I need are instructions on how to install them.  Anyone have experiences doing this?

Thanks.





**"bbowley2"**

---
---
**Ned Hill** *August 11, 2017 00:52*

Should be as easy as unscrewing the round fronts, on the primary and secondary mirrors, and replacing. Use a soft tissue to hold the mirror in place while screwing the fronts back in place to prevent finger prints.  On the laser head unscrew the metal tab and replace.


---
**Ned Hill** *August 11, 2017 01:34*

I would also recheck your alignment as well after replacement.


---
**bbowley2** *August 11, 2017 01:35*

Thanks.  I'll give a shot.


---
**bbowley2** *August 11, 2017 13:56*

Changed and aligned the new gold coat SI mirrors and I'm now running the laser.  The first original mirror looked fine, mirror had a burn mark that wouldn't clean with 99% alcohol and the third mirror has what looks like a child's finger print in the center. Took some rubbing to remove.  


---
**William Kearns** *August 12, 2017 01:51*

I replaced my mirrors and they are as heavy as lead! They are twice as thick as the original 


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/d5qXp6YVMsx) &mdash; content and formatting may not be reliable*
