---
layout: post
title: "CO2 Laser Reflective Study Here is a great video of how much IR light is scattered from different materials"
date: September 03, 2016 06:05
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
CO2 Laser Reflective Study



Here is a great video of how much IR light is scattered from different materials. This is the reason to be wearing safety glasses at all times!




{% include youtubePlayer.html id="q0CdAIvHaPU" %}
[https://www.youtube.com/watch?v=q0CdAIvHaPU](https://www.youtube.com/watch?v=q0CdAIvHaPU)





**"HalfNormal"**

---
---
**Przemysław Wilkutowski** *September 06, 2016 15:47*

Quick question - is the stock plexy added to this units any good in stopping the reflections ? 



By logic it seems so (it is plexy after all but just want to be extra sure)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/AEavpj96n6h) &mdash; content and formatting may not be reliable*
