---
layout: post
title: "Had some black walnut laying around. So far I like the results"
date: June 30, 2016 02:39
category: "Object produced with laser"
author: "Alex Krause"
---
Had some black walnut laying around. So far I like the results

![images/b3add7d95681992b7ea1a0ba6b3100aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b3add7d95681992b7ea1a0ba6b3100aa.jpeg)



**"Alex Krause"**

---
---
**Anthony Bolgar** *June 30, 2016 03:11*

Looks pretty nice Alex.


---
**Phillip Conroy** *June 30, 2016 03:36*

Nice ,how long did 1 take,what power and ma


---
**Alex Krause** *June 30, 2016 03:38*

One on the left 300mm/s at 9ma, one on the right was 500mm/s 9ma just a few minutes each


---
**Alex Krause** *June 30, 2016 03:40*

2.5 X 2.5 inches


---
**Phillip Conroy** *June 30, 2016 04:04*

Are u using stock  circuit board? ,the ladt thing i etched took 45 min at 250mm/sec and 5ma ,have yet to test faster speeded, worried about vibrations


---
**Phillip Conroy** *June 30, 2016 04:07*

Forget to say size 15 cm x 10cm


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 30, 2016 04:11*

Looks great **+Alex Krause**. Looks like a nice wood to work with.



**+Phillip Conroy** Vibrations are fine with engraving all the way up to 500mm/s. Not so with cutting however.


---
**Alex Krause** *June 30, 2016 04:13*

I typically run between 300-500mm/s on all engraves. And yes stock M2nano for just a short period of time I found out I needed to order a few more components before I switch to smoothie


---
**Anthony Bolgar** *June 30, 2016 04:20*

I wonder what kind of speed I will be able to run the test rig at. And I really wonder how long it will take to engrave a 24" X 50" photo.


---
**Scott Marshall** *June 30, 2016 04:22*

Careful not to breathe the smoke. Black Walnut is poisonous. It's fine to handle etc, but it can effect you if you breathe in the dust.

I 1st found this out years back when a friend got sick building a rifle with it, and then I heard from my sister who keeps horses, it's dangerous to use Black Walnut  sawdust or chips in the stalls as it can sicken/kill the horses.



No need to be paranoid, just don't do a lot of burning with it vented where you're breathing.



Looks good!


---
**Tev Kaber** *June 30, 2016 15:28*

**+Anthony Bolgar** isn't the rig area only ~8" x 11"?  Or do you have a bigger machine?


---
**Anthony Bolgar** *June 30, 2016 15:33*

The test rig I am building has a work area of 24.75" X 59"  It is quite large.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/XiQNN8Zickx) &mdash; content and formatting may not be reliable*
