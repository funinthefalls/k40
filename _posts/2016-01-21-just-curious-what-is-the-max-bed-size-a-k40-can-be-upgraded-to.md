---
layout: post
title: "just curious what is the max bed size a k40 can be upgraded to?"
date: January 21, 2016 04:24
category: "Hardware and Laser settings"
author: "3D Laser"
---
just curious what is the max bed size a k40 can be upgraded to?





**"3D Laser"**

---
---
**I Laser** *January 21, 2016 06:52*

Practically speaking, 300x200mm. After that you'll need to replace too much to make it worthwhile.


---
**Jim Hatch** *January 21, 2016 17:28*

Yeah that's about right - I'm at 350x215 rail to rail by taking out the base plate (replaced it with a set of aluminum sheets that I can build up to allow for different thicknesses of material) and cutting out the exhaust scoop.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/eQgoDyzgakF) &mdash; content and formatting may not be reliable*
