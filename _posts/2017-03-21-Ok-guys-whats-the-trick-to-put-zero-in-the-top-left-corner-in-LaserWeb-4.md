---
layout: post
title: "Ok guys what's the trick to put zero in the top left corner in LaserWeb 4?"
date: March 21, 2017 02:14
category: "Software"
author: "Wes Treihaft"
---
Ok guys what's the trick to put zero in the top left corner in LaserWeb 4? Just did a K40 smoothieboard conversion and I am stumped on how to get origin up in the left top corner where it should be.





**"Wes Treihaft"**

---
---
**Ariel Yahni (UniKpty)** *March 21, 2017 02:25*

The correct way should be for you to move origing to bottom left as per the standard, then just home to top left if you want. Search for this in the group as many have done the same process.

In LW you can only change the default place where the files are loaded, check in the settings


---
**Anthony Bolgar** *March 21, 2017 03:45*

In your smoothie config set y to home to max, so when it homes to the top left corner, your machine coordinates are 0,200. Then you can home to 0,0 which is the bottom left corner which is a cartesian standard location.


---
**ThantiK** *March 21, 2017 05:28*

**+Anthony Bolgar** - just because it's standard for 3D printing, milling, doesn't mean it's the standard for lasers.  You need to be able to load and unload material without bumping lenses, etc. -- the standard for lasers is pretty clearly top left to account for this.


---
**Jim Hatch** *March 21, 2017 12:36*

**+ThantiK**​ I agree. I want my home position to be in the back of the machine so I'm not trying to work around the rail or head on full size pieces. Where I start a job depends on what I'm doing but is generally top left or bottom left. For the K40 it's the top left. For my RedSail it's the bottom left. That's because I can physically align the corners to get consistent placement.


---
**Anthony Bolgar** *March 21, 2017 23:00*

LW uses bottom left as the standard. Not my decision, just the way they made it. The devs have had numerous discussions about this, I would suggest getting in touch with them if you wish to have it changed.


---
*Imported from [Google+](https://plus.google.com/104033989120920258895/posts/ZetEBGHpGB1) &mdash; content and formatting may not be reliable*
