---
layout: post
title: "How can you tell if the focal lens is in the wrong way ?"
date: June 23, 2016 23:50
category: "Discussion"
author: "Rodney Huckstadt"
---
How can you tell if the focal lens is in the wrong way ?





**"Rodney Huckstadt"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 00:42*

If you flip it over & it cuts better! But seriously, the focal lens in general should be with the concave side facing down & the convex/flat side facing upwards.


---
**Rodney Huckstadt** *June 24, 2016 00:51*

cheers :)


---
**Jim Hatch** *June 24, 2016 01:01*

Bumps are up. Easy way to remember it.


---
**Ariel Yahni (UniKpty)** *June 24, 2016 01:15*

O shit really???? I think I have it backwards


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 01:38*

**+Ariel Yahni** If indeed you do, you'll be glad you swapped it over. Makes a huge difference.


---
**Ariel Yahni (UniKpty)** *June 24, 2016 01:39*

But Its weird since I'm getting cuts at 10mms with less than 10mA on 3mm


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 02:00*

**+Ariel Yahni** Maybe you can get the same cuts @ 20mm/s with lens inverted? Or might just be a sharper cut.


---
**Ariel Yahni (UniKpty)** *June 24, 2016 02:07*

Will try it soon


---
**Ned Hill** *June 24, 2016 03:05*

Curved side goes up or you get longitudinal aberration.  [https://1drv.ms/i/s!AtpKmELEkcL8imztsXWs-opsElTO](https://1drv.ms/i/s!AtpKmELEkcL8imztsXWs-opsElTO)

The only reason it would work better the other way around is if you had a crappy made lens. :)


---
**Jim Hatch** *June 24, 2016 06:26*

**+Ned Hill**​ "curved side" is an ambiguous term. Is the the one where the curve goes up (e.g. bump or convex) or the curve that goes inward toward the center of the lens (e.g. depression or concave)? 



A bump is always away from the surface upward and a depression is always downward. Convex and concave respectively.



So bump or convex indicates a known surface and direction. Curved side is open to interpretation.



The planar lenses we use tend to (there are exceptions) focus to a narrower point convex/bump side up. 


---
**Ned Hill** *June 24, 2016 07:07*

You are correct. My comment only applies for plano-convex lenses where one side is flat and the other is convex. I wasn't considering that someone could be using a positive meniscus lens where one side is convex and the other is concave.


---
*Imported from [Google+](https://plus.google.com/109243783228349869845/posts/4veXDkn3TFu) &mdash; content and formatting may not be reliable*
