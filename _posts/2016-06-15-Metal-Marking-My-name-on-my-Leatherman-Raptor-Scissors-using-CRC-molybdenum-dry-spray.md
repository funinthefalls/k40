---
layout: post
title: "Metal Marking My name on my Leatherman Raptor Scissors using CRC molybdenum dry spray"
date: June 15, 2016 22:38
category: "Object produced with laser"
author: "HalfNormal"
---
Metal Marking



My name on my Leatherman Raptor Scissors using CRC molybdenum dry spray. 12 ma @ 3 mm/sec. I am finding that uniformity of the spray affects the quality of the etch. It looks better in person than the pic.



![images/4a639ba30a27cf5e19b74a28985fcc33.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4a639ba30a27cf5e19b74a28985fcc33.jpeg)



**"HalfNormal"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 16, 2016 04:54*

What I have tested so far with the moly I just smothered it in the spray. It was a very thick coating. I would agree with your findings that the uniformity of the spray affects the quality. I tested with a wet moly first & it would actually move when the laser/air assist hit it. Left an extremely faint discolouration since it was barely contacting any of the moly.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Ac4a2ZAgx3q) &mdash; content and formatting may not be reliable*
