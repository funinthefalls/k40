---
layout: post
title: "DONE! Pics, video and BOM coming soon!"
date: May 26, 2018 19:38
category: "External links&#x3a; Blog, forum, etc"
author: "Frederik Deryckere"
---
DONE! Pics, video and BOM coming soon!





**"Frederik Deryckere"**

---
---
**Don Kleinschnitz Jr.** *May 26, 2018 20:03*

Awesome work! 

Now, had you to do over would you still have skavaged a K40 or start from scratch?  


---
**Frederik Deryckere** *May 26, 2018 21:52*

**+Don Kleinschnitz** 

Hmm good question. Going into it blind as I did now I would have to say yes I'd do it again starting from a K40. Simply because it eliminates a huge deal of potential headache knowing that the actual technology that drives the machine has been lifted straight out of a working chassis. I fear the learning curve might have been overwhelmingly steep for a total noob like I was. Considering the same question WITH the knowledge I have since gathered, that would be another matter.



Someone asked me yesterday about doing the conversion, starting with separate parts instead of a K40. As it is now, this would be hard considering I also re-use small & impossible to find custom K40 hardware. I would like to revisit this one day and change the design so  there's no more need for a K40 as the basis. But for now I'm happy it is done to be honest, I've been working on it for 5 months. Time for something new.


---
**Fabiano Ramos** *June 02, 2018 02:21*

Congratz dude, your machine is a total inspiration for all of us. I think some day  i will try  build  one for me. 




---
**Frederik Deryckere** *June 02, 2018 21:36*

Thanks! Means a lot.


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/gN1Ugp1o9D2) &mdash; content and formatting may not be reliable*
