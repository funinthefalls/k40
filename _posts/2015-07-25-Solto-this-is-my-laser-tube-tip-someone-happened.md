---
layout: post
title: "Solto this is my laser tube tip someone happened"
date: July 25, 2015 18:08
category: "Hardware and Laser settings"
author: "tony alexander rico cera"
---
Solto this is my laser tube tip someone happened



![images/bcbbca5d9fd4c9631eb9ded8662af158.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bcbbca5d9fd4c9631eb9ded8662af158.jpeg)
![images/60844e6c0a488ee1997d6d16e3bcfbb2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60844e6c0a488ee1997d6d16e3bcfbb2.jpeg)

**"tony alexander rico cera"**

---
---
**ThantiK** *July 25, 2015 21:52*

Were you trying to run <i>cold</i> water through it?  If you were, the heat from those terminals + the cold water can cause the glass to crack.


---
**tony alexander rico cera** *July 26, 2015 03:05*



water use is at room temperature, I do not think you have cracked glass


---
*Imported from [Google+](https://plus.google.com/111896216284635092101/posts/8wmHqfqgdnP) &mdash; content and formatting may not be reliable*
