---
layout: post
title: "Shared on February 07, 2017 20:27...\n"
date: February 07, 2017 20:27
category: "Modification"
author: "Jorge Robles"
---
[http://www.thingiverse.com/thing:2092572](http://www.thingiverse.com/thing:2092572)





**"Jorge Robles"**

---
---
**Austin Baker** *February 09, 2017 20:52*

Newbie question: What is that, and what is it used for? I couldn't figure it out from the thingverse pictures!


---
**Jorge Robles** *February 09, 2017 22:34*

These are used for placing door switches (interlocks), on the main cabinet an tube cabinet. I will post installation photos tomorrow.


---
**Jorge Robles** *February 10, 2017 10:18*

**+Austin Baker**​ 

![images/f03e6bf0e0921e477607811ce1c53f5c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f03e6bf0e0921e477607811ce1c53f5c.jpeg)


---
**Jorge Robles** *February 10, 2017 10:18*

![images/14fc23e19ccdbf23b4c45dda053cfa14.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14fc23e19ccdbf23b4c45dda053cfa14.jpeg)


---
**Austin Baker** *February 10, 2017 14:26*

Ah, I see -- nice!


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/3nruCaJppNz) &mdash; content and formatting may not be reliable*
