---
layout: post
title: "So I am working at trying to install end stops on my Benbox Gearbest/bangood Laser engraving machine using's Ray Kholodovsky Cohesion 3D controller board as the included one was defective"
date: December 11, 2016 21:08
category: "Discussion"
author: "Jonathan Davis (Leo Lion)"
---
So I am working at trying to install end stops on my Benbox Gearbest/bangood Laser engraving machine using's **+Ray Kholodovsky** Cohesion 3D controller board as the included one was defective. Now I know where to hook them too on the board but i can not seem to figure out how to install them on the machine itself.



![images/7fe5a6e786818ffc8655304844fe4473.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7fe5a6e786818ffc8655304844fe4473.jpeg)
![images/be7d2feb376e06bd64ee6e79f0b0ee0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be7d2feb376e06bd64ee6e79f0b0ee0f.jpeg)

**"Jonathan Davis (Leo Lion)"**

---
---
**Ariel Yahni (UniKpty)** *December 11, 2016 21:33*

You'll need to mount one on each axis, if you want them to be min, then one should be on the bottom left of the Left Y axis and the other on the left on the X


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 21:34*

**+Ariel Yahni** I Get that part of it. It's getting them on the machine is the problem as it is not a k40 system but a china branded one and with one method I had it was putting them on the blocks that held the guide cable down.


---
**Maxime Favre** *December 11, 2016 22:15*

Do you really need endstops on this machine ? 

I installed them on my CNC mill and no one has ever been triggered since initial tests :)


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 22:18*

**+Maxime Favre** My machine has tendency to keep going when there is not enough track for it to continue. Plus it did not come with endstops when i bought from Gearbest.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 22:22*

**+Jonathan Davis**​ you will need to cut a piece of wood or acrylic and make a small  plate to mount the endstops and the screw them into the middle of the profile on each axis. As you say it tends to go away then it means your starting home position is not set correctly on each run. If you move the head into the lower left corner and press the button on LaserWeb that says Set Home the you most likely won't run further than permitted unless you are sending a size larger than the laser. I don't use endstops myself


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 22:35*

**+Ariel Yahni** Let me try that with the software method, and then i will work at making the holders for the Endstops. Bought a generic set off of amazon for $10.


---
**Jonathan Davis (Leo Lion)** *December 11, 2016 23:00*

Another i'm getting is the machine is going away from the graph on laserweb instead of keeping within it. the highest is a positive 30 and on the negative is -180


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/Qch8TqeoxYy) &mdash; content and formatting may not be reliable*
