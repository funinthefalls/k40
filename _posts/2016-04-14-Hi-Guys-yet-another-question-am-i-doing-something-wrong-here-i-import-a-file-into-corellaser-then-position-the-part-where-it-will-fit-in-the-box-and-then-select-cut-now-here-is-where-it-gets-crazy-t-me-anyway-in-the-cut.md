---
layout: post
title: "Hi Guys yet another question am i doing something wrong here i import a file into corellaser then position the part where it will fit in the box and then select cut now here is where it gets crazy t me anyway in the cut"
date: April 14, 2016 22:23
category: "Discussion"
author: "Dennis Fuente"
---
Hi Guys yet another question am i doing something wrong here i import a file into corellaser then position the part where it will fit in the box and then select cut now here is where it gets crazy t me anyway in the cut window the part is in the lower part of the box so i move it up to the top but it won't cut there whats the deal this is my last part thanks 



Dennis 





**"Dennis Fuente"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 22:39*

Is your setting set to TopLeft for the origin?


---
**Dennis Fuente** *April 14, 2016 23:09*

Yes and i set it to 0x &0Y


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 23:46*

**+Dennis Fuente** There is also an option called Refer that says "TopLeft/TopRight/BottomLeft/BottomRight"

Just above the Refer X & Refer Y.



Another thought is when you have it set at 0x & 0y, where is your laser head sitting? Sometimes (if I have bumped it) mine is not in the top left of the cutting bed. In this case, you have to click the Release button (in either Cutting or Engraving dialog) & then manually reposition the head where it belongs (do some test fires to make sure it is in the right position). Then when you cut/engrave it will start as that point being the origin (0x,0y).



Not sure if that helps.


---
**Dennis Fuente** *April 14, 2016 23:55*

Yuusuf 

No the head is at the point of origin @ the top left hand corner of the machine close to the first mirror and the tube my problem is the origin dose not jive with the selected cut position in other words it dose not cut from the home position it cut's toward the front of the machine and i can't figure out why, but after a lot of head banging and testing i finaly got my last part cut don't know what i will do from here one thing for sure the software sucks bad.



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 23:57*

**+Dennis Fuente** Totally agree the software sucks bad. It is definitely worth an upgrade to something better. Unfortunately, with the stock controller, we cannot use any other software. It requires us to do an upgrade of the controller first. If you check the post I made regarding that again, there are other options than the Smoothie, which are cheaper (but have cons). Peter gave a great response listing the different controllers with pros & cons of each.


---
**Dennis Fuente** *April 15, 2016 00:05*

I am  definitely looking I had  enough  for  today 


---
**Scott Marshall** *April 15, 2016 00:29*

That didn't take long.

Try putting a tiny circle or line in the upper corner of the drawing, for some reason if corel doesn't see anything near the origin, it "floats the drawing".



It doesn't always solve the problem, but worth a try.


---
**Dennis Fuente** *April 15, 2016 01:14*

Scott thanks for the response and what you discribed is exactly what it's doing floating the file somewhere other then where i put it, i will give that trick a try thanks.



Dennis 


---
**Dennis Fuente** *April 15, 2016 01:16*

Yuusuf i guess that's the way i will have to go if i want to use better software thanks 



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2016 01:57*

**+Dennis Fuente** Yeah me too. Since day 1 I've been wanting to get away from the horrible Corel software interface. Corel itself probably wouldn't be so bad, just the old version, dodgy pirated version, and lack of support/documentation regarding the plugin makes it a nightmare to get your head around. I've spent a lot of time just testing what this option does & that option does & still have only half an idea haha.


---
**Dennis Fuente** *April 15, 2016 02:12*

I agree  I have to  wait  and save some  hobby  money 


---
**Tony Schelts** *April 19, 2016 15:43*

Sometimes  I have found that I have left a spot or element of some sort somwhere on the page and Corellaser picks up everything that isnt switched of in object manager.  Putting a dot on the page is the best advice I  receieved previously and I always create a layer called Locator to begin with so everything references from that spot.


---
**Dennis Fuente** *April 19, 2016 16:28*

Tony 

So i am not familiar with how to turn objects off in corel and also to have different items set to different layers.



Thanks 

Dennis 


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/HYDwYtMgMzE) &mdash; content and formatting may not be reliable*
