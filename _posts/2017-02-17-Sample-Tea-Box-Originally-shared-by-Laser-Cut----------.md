---
layout: post
title: "Sample Tea Box Originally shared by Laser Cut  ,        "
date: February 17, 2017 05:09
category: "Object produced with laser"
author: "Laser Cut"
---
Sample Tea Box



<b>Originally shared by Laser Cut</b>



Чайный домик, хороший подарок на любой праздник и семейное торжество.

Ручная работа - декупаж.

Материал - фанера 

Изготовим по вашему заказу с любым рисунком и надписью.



![images/e4c406c5f30813707303d650abb27cda.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e4c406c5f30813707303d650abb27cda.jpeg)
![images/3de28eaf5ffbdf2dc9bc39ffdeaf6b7e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3de28eaf5ffbdf2dc9bc39ffdeaf6b7e.jpeg)
![images/7b8c04a78ae0acde518acccc05318133.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b8c04a78ae0acde518acccc05318133.jpeg)
![images/18ffe95c1f37d6cf9dfa1f63be289378.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/18ffe95c1f37d6cf9dfa1f63be289378.jpeg)
![images/103e8a023d2a16f3b627b4925c23db58.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/103e8a023d2a16f3b627b4925c23db58.jpeg)
![images/bfd6ee0097abeffc3e223ab97d601f7c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bfd6ee0097abeffc3e223ab97d601f7c.jpeg)

**"Laser Cut"**

---


---
*Imported from [Google+](https://plus.google.com/114779305649641505346/posts/R8zYRFab7qf) &mdash; content and formatting may not be reliable*
