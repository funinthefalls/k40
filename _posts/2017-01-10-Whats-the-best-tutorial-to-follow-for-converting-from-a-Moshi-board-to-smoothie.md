---
layout: post
title: "What's the best tutorial to follow for converting from a Moshi board to smoothie?"
date: January 10, 2017 16:35
category: "Smoothieboard Modification"
author: "Nathaniel Swartz"
---
What's the best tutorial to follow for converting from a Moshi board to smoothie?  After a few months I finally have the time and funds to do so, so I really want to make the jump and drop the Moshi software for laserweb.





**"Nathaniel Swartz"**

---
---
**Nathaniel Swartz** *January 10, 2017 16:48*

I should also ask, is a stock smoothie board the easiest way to go?  Would another board be better as a plug and play drop in?


---
**Stephane Buisson** *January 10, 2017 16:56*

from my early step below to 2017 solution, I would say Cohesion 3D is the most advanced with this solution :

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



old one : [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)






---
**Don Kleinschnitz Jr.** *January 10, 2017 16:58*

This is what I did: [donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)

Cohesion was not available then. I would probably go that route today.


---
**Nathaniel Swartz** *January 10, 2017 16:59*

**+Stephane Buisson** Thanks for posting that.  A drop in solution like that makes my week :)


---
**Carl Fisher** *January 10, 2017 19:53*

Cohesion3D user here. Direct drop in. Follow the directions provided and unlock the potential of that machine and LaserWeb.


---
**Nathaniel Swartz** *January 18, 2017 01:51*

Thanks for the advice all, just got my Cohesion3D board in the mail, I look forward to having time this weekend to install it.


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/eD3WnCf4jLn) &mdash; content and formatting may not be reliable*
