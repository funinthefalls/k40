---
layout: post
title: "A new led lamp A gift for a fan of this great heavy metal band"
date: May 23, 2018 16:37
category: "Object produced with laser"
author: "syknarf"
---
A new led lamp A gift for a fan of this great heavy metal band.

![images/500a5b0382638dde8e066d7916023d37.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/500a5b0382638dde8e066d7916023d37.jpeg)



**"syknarf"**

---
---
**Ariel Yahni (UniKpty)** *May 23, 2018 16:56*

Maiden rulzzzz


---
**David Cook** *May 24, 2018 19:42*

That is freeking badass !




---
**Jeffrey Kopec** *June 20, 2018 17:09*

Where did you buy the base?


---
**syknarf** *June 20, 2018 17:25*

**+Jeffrey Kopec** I made the base myself, three layers of acrylic glued together. 


---
**Jeffrey Kopec** *June 20, 2018 17:41*

**+syknarf** Nice! looks great. what did you use for the light? Just an led strip?




---
**syknarf** *June 20, 2018 17:44*

**+Jeffrey Kopec** yes, a 12v led stripe


---
*Imported from [Google+](https://plus.google.com/113055105026792621247/posts/95BGU4cD4of) &mdash; content and formatting may not be reliable*
