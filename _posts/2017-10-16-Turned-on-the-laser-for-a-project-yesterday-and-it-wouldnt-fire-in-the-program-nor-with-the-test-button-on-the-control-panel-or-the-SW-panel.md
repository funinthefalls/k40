---
layout: post
title: "Turned on the laser for a project yesterday and it wouldn't fire in the program nor with the test button on the control panel or the SW panel"
date: October 16, 2017 16:14
category: "Modification"
author: "Steve Clark"
---
Turned on the laser for a project yesterday and it wouldn't fire in the program nor with the test button on the control panel or the SW panel. So I hit the test button on the PWS and it fires. 



OK... now it's isolated to either the wire circuit between the PWS and the control panel and C3D and the flow switch. I bypassed the flow switch and it now fires.



[http://www.ebay.com/itm/Hot-Sale-White-Plastic-Shell-Magnetic-Water-Flow-Switch-w-Inner-Outer-Thread-N3-/122118491693?hash=item1c6ed44e2d](http://www.ebay.com/itm/Hot-Sale-White-Plastic-Shell-Magnetic-Water-Flow-Switch-w-Inner-Outer-Thread-N3-/122118491693?hash=item1c6ed44e2d)



So, I’m looking to replace that switch and this is just a heads up that there can be problems with this particular e-bay flow switch.





I’d be interested in seeing what other’s are using as a flow monitoring switch.















**"Steve Clark"**

---
---
**Nate Caine** *October 16, 2017 17:09*

Good and logical debug strategy!



I purchased a similar flow switch on eBay, but when it arrived, it was already defective.  



This type of flow switch uses a spring loaded magnet in the water channel.  When sufficient flow occurs to overcome the spring, the magnet moves far enough to trigger the magnetic reed switch mounted on the side.



I don't think it's a bad switch overall, but the magnetic reed switch connected to the yellow wires is epoxied (I think) into the side cavity of the flow housing.  The white body appears to be HDPE (or similar) which is notorously difficult to bond to.  They probably need to improve the disign with a mechanical means to secure the reed switch to the housing.



Either way, the reed switch on the one I received completely separated from the flow housing.  (see photo)



The switch you've shown looks similar, so take a peek and see if yours has  separated or shifted.  You might be able to re-insert the switch part and bring you machine back to life.  



If so, be sure to secure it in place, so it doesn't separate again.



![images/248295aa77449c1c80edd69b69cb6947.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/248295aa77449c1c80edd69b69cb6947.jpeg)


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/Z3Lh2zYKwq9) &mdash; content and formatting may not be reliable*
