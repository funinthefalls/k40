---
layout: post
title: "So I am getting closer to having my machine set up"
date: April 08, 2017 15:22
category: "Discussion"
author: "Martin Dillon"
---
So I am getting closer to having my machine set up.  I have an adjustable table, cut back the air vent, 3d printed a new shroud and connected a 1hp portable dust collector, added air assist, added lockouts for lid and water, did a slant test to find focal length from tip of air assist.

My question now is how do I get my machine to run faster.  It says to set the max speed to 400 mm/s but I get no where near that speed when cutting vector graphics.  It seems to run faster when engraving.  I am assuming that it is a function of the stock controller board not being able to process the data fast enough.  I am planning on upgrading the board eventually but not right now.





**"Martin Dillon"**

---
---
**Abe Fouhy** *April 08, 2017 16:25*

Nice work! I think your right, stock controller issue.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 08, 2017 16:35*

I found when cutting at speeds over 100-200mm/s I would get a lot of vibration in the machine (especially if there were a lot of quick changes between left-right movements). Engraving at 500mm/s was no problem, as it scanned the entire line. I imagine it may have something to do more with the acceleration over short distances rather than the board being unable to process the data fast enough, but not really sure.


---
**Phillip Conroy** *April 08, 2017 20:31*

The k40 doesn't have the power to cut at the speeds you talkabout ,maybe might cut paper .I use11mm/second and 10ma to cut 2.7mmplywoodand 3mm mdf with one pass,and etch at 350 mm/second and 10 ma.

Etching any faster gives a shallower engrave and I like the deepen grave that shows up the red glue layer in the ply I use.


---
**Martin Dillon** *April 08, 2017 22:29*

I shouldn't have said cut.  I meant etching vector graphics.


---
**Joe Alexander** *April 09, 2017 03:51*

i engrave at 300mm/s but run my vector engraves at around 50mm/s, any more causes inconsistencies in my final result. All at 10ma and cut 1/8" birch at 5mm/s sometimes 2 passes.


---
**Mark Brown** *April 09, 2017 21:59*

You are getting some speed out of it?  When I first tried mine I had the model setting in CorelLaser wrong so it would only go 2mm/s no matter what it was set to.


---
**Martin Dillon** *April 09, 2017 23:25*

Yes I am getting some speed.  I was wondering if there was a setting that would make it go faster, like changing the DPI or some other setting.  There are a few settings I have no idea what they do.


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/JdiuaPmx2o6) &mdash; content and formatting may not be reliable*
