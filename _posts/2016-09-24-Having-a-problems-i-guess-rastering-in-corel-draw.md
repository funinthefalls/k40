---
layout: post
title: "Having a problems i guess \"rastering\" in corel draw?"
date: September 24, 2016 16:45
category: "Software"
author: "Gideon Sauceda"
---
Having a problems i guess "rastering" in corel draw? 

Currently I have been guided to work on Corel 7 and its work great from corel 4. now, i am not able to cut 

drawings that have many complex cuts? or as we call them points. I have dumb down the point count and still

the machine does not want cut. Ive tried the 00.1 and 0.025 on the thickness. it does not want to cut?



Is it the software or the mother board? 



Here's the design that does not want to cut. 

![images/38eac5195d00ee3876279958238139f7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/38eac5195d00ee3876279958238139f7.jpeg)



**"Gideon Sauceda"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 24, 2016 20:00*

There should be no problems getting that to cut. What exactly do you mean by "it does not want to cut"? What happens when you send the Cut command via the cut interface?


---
*Imported from [Google+](https://plus.google.com/116888087804682757951/posts/LHbYzEETRtF) &mdash; content and formatting may not be reliable*
