---
layout: post
title: "Hello Friends, i'm very intrested in PCB Prototyping via a laser"
date: May 04, 2017 07:27
category: "Discussion"
author: "Frank Herrmann"
---
Hello Friends, i'm very intrested in PCB Prototyping via a laser. Somebody here to try etch a pcb with an laser and what is his experiences with this process? Here an example what i mean:




{% include youtubePlayer.html id="mzmjGz0_joM" %}
[https://youtu.be/mzmjGz0_joM?t=42s](https://youtu.be/mzmjGz0_joM?t=42s)





**"Frank Herrmann"**

---
---
**Anthony Bolgar** *May 04, 2017 08:31*

It is not possible with a CO2 laser unless it is in the hundreds of Watts power. Small lasers (Up to 120W) can not cut or etch metal. It requires a Yag metal laser (Small ones start around $10,000)


---
**Frank Herrmann** *May 04, 2017 08:33*

**+Anthony Bolgar** But the copper on a PCB is just 0.0035mm thick. Impossible, real?


---
**Anthony Bolgar** *May 04, 2017 08:43*

No one I know has been able to do it with 40,50,60,or 80W CO2 lasers.


---
**Stephane Buisson** *May 04, 2017 10:17*

maybe not the copper directly, but a transfert layer/film then chemical (old fashion way).


---
**Imnama** *May 04, 2017 12:56*

Copper reflects IR with about 99,9%. You will need a laser with a spectrum that copper absorbs. 

Indirect is possible.


---
**Nate Caine** *May 04, 2017 13:37*

<i>What not to do:</i>



One of our imbecilic users ran our laser at full power (130W) and very low speed and got horrible results.  What he got <b>appeared</b> that the copper etched away.  



What he <b>really</b> got was just barely enough local heating that the copper delaminated from the fiberglass substrate (often leaving a charred mess of fiberglass mesh, embedded in burned epoxy, with splinters of copper hanging off it).



The results were so dramatically bad that I kept a sample to show in my class of what <b>not</b> to do.  As others have said, copper is a very good IR reflector (so not much energy is absorbed), and a very good conductor of heat (so the energy is rapidly conducted away from the spot).  Indeed, one of the "big boy" machines I saw (a 4KW Bystronic) actually uses water-cooled copper mirrors to guide the beam.


---
**Nate Caine** *May 04, 2017 14:16*

<i>What works:  Spray Paint Method</i>  (for single-sided PC boards)



<i>Use spray paint as a mask.  Pattern the mask using the laser.  Remove the exposed copper using a chemical etchant.</i>



(1)  <b>Prepare the board.</b>  Thoroughly clean and degrease your bare copper board.  Scuff it up slightly with an abrasive cleanser (preferably non-chlorinated)...give the paint something to grip to.  Very important to clean the results and rinse with acetone or alcohol.  Let it dry (us hot air if desired).



(2)  <b>Cheap Spray Paint.</b>  Buy some cheap $1 spray paint (from Walmart, etc).  Not good stuff; cheap stuff.  (The good stuff sticks <i>too</i> well.)



(3)  <b>Paint the Board.</b>  Cover the board with two thin layers of paint (in orthogonal  directions--90° directions).   Let it properly dry.



(4)  <b>Artwork.</b>  Prepare your artwork to be a <b>negative</b> of the desired PC board result.  (i.e. The laser will be burning away the paint where you want no copper and leaving paint where you want copper traces.)  Using your bit map settings for something like 200~300 lpi.  Fewer is too "gritty".  Higher, is overkill.



(5)  <b>Exposure.</b>   You may have to experiment a bit to find the proper speed/power settings, but it's pretty obvious when you have them right and there is good latitude around the proper settings.  So you may want to make some test targets beforehand.  And, good news, afterwards you can wash the paint off, and re-use the board.  Run exhaust on "high" to get rid of all the paint dust you will create. 



(6)  <b>Engrave.</b>  After using the laser to "engrave" the board you should have black paint protecting the desired copper traces, and mostly copper exposed (which you will etch).  Now is the time to touch up your artwork.  (often not required).  Use an Xacto knife to scratch off any paint that shouldn't be there.  Use a permanent Sharpie to dot in any painted areas that have gaps or pinholes.



(7)  <b>Clean Up!</b>  Clean the machine.   Even running the exhaust on "high" you will still have the paint dust on surfaces.  Clean your machine, the sooner the better.



(8)  <b>Etch.</b>  Use Ferric Chloride etchant.  (Used to be available from Radio Shack).  Follow the instructions.  Not very dangerous, but stains everything.  Some of the guys used their own aggressive solutions of HCl and Muratic acid... Dangerous and  <b>NOT</b> recommended.  (The slight benefits were outweighed by the dangers--breathing the vapors, splash in the eyes, etc.)



(9)  <b>Mask Removal.</b>  After etching the copper away, remove the protective spray paint using acetone, etc.



(10)  <b>Holes.</b>  Drill the holes.  Be sure to wear safety goggles and dust mask.  (avoid copper crap or broken drill bits in the eyes)  (do <b>NOT</b> breath PCB fiberglass dust--it accumulates deep in the lungs).  



<i>Hint:</i>  When you prepare your artwork, most PC board layout software creates a solid pad for lands that will be drilled later...that for a professional setup.  <i>You,</i>  however, should create component pads with a small clear dot in the center..."donuts".  Later when you drill the board, the clear center exposes the soft fiberglass board and guides the drill.  (If you left a solid pad, the small drill tends to "walk" and it's hard to get a properly centered hole in the pad.)  If you forgot to do these small guide holes in your software, you can even do them after you've exposed the spray paint with the laser, by just manually makin a pin-prick with a sharp point.  It only takes seconds, and really makes the drilling go faster.



(11)  <b>Tin Plating.</b>  Some folks use tin-plating solution at this point.  Follow the instructions.  Once again, works best with a properly clean and degreased board.   Especially sensitive to latent fingerprints.



<i>Enjoy your PC board!</i>



<i>Affordable, quality PC boards are available out of China in a timely manner.  The Laser Spray Paint stuff works, but can be messy and time consuming.  But you can turn a board around in a few hours.</i>


---
**Nate Caine** *May 04, 2017 14:36*

<b>Absorption vs Wavelength of Various Materials</b>



Note both CO2 and YAG laser wavelengths.

![images/22f2a7bd8be42a910659d5e000a4273e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/22f2a7bd8be42a910659d5e000a4273e.jpeg)


---
**HalfNormal** *May 04, 2017 16:09*

**+Frank Herrmann** Another approach to etching copper is using a salt water bath and electrolysis. Search the net and you will find a lot of great info. No nasty chemicals and just as fast.


---
**Frank Herrmann** *May 05, 2017 11:57*

**+Nate Caine** Many thx for the ur describe of the best process to etch a pcb. But you describe to make a etch resist mask on a PCB. In this video you see a complete etch process via laser. I read more about this machine, they use a green laser (530nm) with lot of watt's i guess. Ok, what i'm learned is absorption of material and so on. 



Now i try to find a kind of conductive tape, this was very interesting: [coating-suisse.com - SWCNT Single Wall Carbon Nano Tubes  Electrically Conductive Coatings, Inks and films &#x7c; Coating Suisse](http://www.coating-suisse.com/en/products/transparente-conductive-heatable-films/)



If we found a conductive foil that we can use for laser etching, then it's a big step. But ok, i'm dreaming :)




---
*Imported from [Google+](https://plus.google.com/+FrankHerrmann1967/posts/X3DJthbvULK) &mdash; content and formatting may not be reliable*
