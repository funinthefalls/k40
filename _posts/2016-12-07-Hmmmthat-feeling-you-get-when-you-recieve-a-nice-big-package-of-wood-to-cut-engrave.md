---
layout: post
title: "Hmmm..that feeling you get when you recieve a nice big package of wood to cut/engrave"
date: December 07, 2016 18:40
category: "Discussion"
author: "Ned Hill"
---
Hmmm..that feeling you get when you recieve a nice big package of wood to cut/engrave. I asked the wife if she like my big wood package,  sigh..she was not impressed ;)

![images/00fa8f28ee9865d41d115ae49658ece0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00fa8f28ee9865d41d115ae49658ece0.jpeg)



**"Ned Hill"**

---
---
**Andy Shilling** *December 07, 2016 18:53*

I've got one like that, they just don't understand lol. 


---
**Ashley M. Kirchner [Norym]** *December 07, 2016 19:02*

Perhaps because they're too flexible, not hard. <grin>


---
**Ned Hill** *December 07, 2016 19:02*

Lol, of course I was holding it at crotch level when I asked :D


---
**greg greene** *December 07, 2016 19:12*

It's not that they're mean - it's just that they remember ..........


---
**Kelly S** *December 07, 2016 19:24*

I am lucky, my other half is just as cheesy as I.  Nice package.  


---
**Ned Hill** *December 07, 2016 19:28*

Lol, **+Kelly S** I'll get that half the time the other half it's the eye roll.


---
**greg greene** *December 07, 2016 19:29*

And now the important question



What kind of wood and from where?


---
**Ned Hill** *December 07, 2016 20:17*

Lol, it was a mix of alder wood and 1/8"  birch ply from Ocooch Hardwoods.  I had a bit of a last minute order for 100 Harley-Davidson Owners Group (HOG) logos that completely consumed my stock of 1/8" alder wood and I had to restock and grabbed some 1/8" ply while I as was at it.   

![images/57f3d3684adfda23e68a662a404dad90.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57f3d3684adfda23e68a662a404dad90.jpeg)


---
**greg greene** *December 07, 2016 20:20*

Beautiful work !  Smoothie?


---
**Ned Hill** *December 07, 2016 20:23*

Thanks!  Not yet, still stock controller.  I've got a cohesion3d on order now so hopefully in a month or so when they ship I'll be there.


---
**greg greene** *December 07, 2016 20:29*

I got one of them - great board - got the Ethernet module on the way - more reliable I think than the USB connection.


---
**Ned Hill** *December 07, 2016 20:29*

Lol, after handling a 100 of those things to get the masking off, that laser residue crap really will stain your skin.

![images/0da9f059f67119846426153494e376d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0da9f059f67119846426153494e376d8.jpeg)


---
**greg greene** *December 07, 2016 20:32*

Yes it does - sticky stuff - tried everything to get it off stuff - rubbing alcohol seems to work OK


---
**Kelly S** *December 07, 2016 20:40*

That looks great, never though about using masking tape to keep the fumes from making the wood lighter.


---
**Ned Hill** *December 07, 2016 20:50*

Yeah I wanted a clean look since there was a lot of engraving to these pieces.  Use a light tack masking tape and use some heavy tack duct tape to help remove.  Found on this order that it works best to lay a paper towel over first and work in with a soft rubber ink roller to suck up the bulk of the residue before using the duct tape.  That way it works faster and uses less duct tape.


---
**Ashley M. Kirchner [Norym]** *December 07, 2016 21:38*

Reminds me I need to place another order ...


---
**Jeff** *December 09, 2016 17:29*

I found this grease cleaner at Home Depot in their cleaning chemical section.  It works great for cleaning up laser residue. I sprayed it in the bed and grid and wiped it off. A little on a paper towel cleaned my rollers, rails, and the rest of the laser.  It works well in the kitchen too!



![images/4e92aa177436301fe5676a4441b5fe5f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e92aa177436301fe5676a4441b5fe5f.jpeg)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/9sJNbmLktNt) &mdash; content and formatting may not be reliable*
