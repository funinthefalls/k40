---
layout: post
title: "Hi everybody, after 4 weeks of easy use of my k40, I suddenly have a problem"
date: December 08, 2017 10:24
category: "Discussion"
author: "Manon Joliton"
---
Hi everybody, after 4 weeks of easy use of my k40, I suddenly have a problem. My laser used to go 2 times over my drawing. But since yesterday, i dont know why, when it goes for the 2nd time, it doesn't cut on the same line (see picture 1, as you see, some of my elephants are cut as usual but the two on the righ are failed)

Furthermore, when i ask for a square, it cuts a rectangle : for example, I ask for a 6mm square, I get a 6by4mm rectangle. sometimes, it cut the righ lengh for the first side but the wrong lengh for the third (see picture 2)... I don't understand.

The issue seems to be linked with the X axis as the shift is only on this axis...

Can someone help me ? Thank you



![images/33e863915a937c79bbcf4026f317f201.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/33e863915a937c79bbcf4026f317f201.jpeg)
![images/03f0fc34324e3264af40b4195d2af8d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/03f0fc34324e3264af40b4195d2af8d4.jpeg)

**"Manon Joliton"**

---
---
**Joe Alexander** *December 08, 2017 10:30*

on the right side of the gantry there is a screw to adjust the belt tension, adjust as needed. Also check if there is a set screw on the stepper gearhead and if so tighten that also (but dont overstress it where it starts to bind up). I would also check the motor wire crimps as that was my issue once before, and continuity test the wire real quick with a multimeter(beep it out). Hope that helps!


---
**Manon Joliton** *December 08, 2017 10:33*

Thanks, I'll try that, I tell you if it gets better (or worse)


---
**Manon Joliton** *December 08, 2017 10:55*

I adjusted the belt tension and it works as et used to, do you think I should check the other things you suggested or is it ok like that ? Thank y ou very much


---
**Joe Alexander** *December 08, 2017 10:56*

couldn't hurt but if it works now then its up to you :)


---
**Manon Joliton** *December 08, 2017 10:57*

OK tanks :)


---
**Salifou Bamba** *December 08, 2017 20:25*

Regarde un des miroirs, un doit être casser. One of miror  might  Be broken...


---
**William Niedermeyer** *January 15, 2018 18:50*

I had this problem right of the bat. I took apart the X gantry to get to the tension bracket. What I found was the belt slipping and grinding away on the tension wheel. This was caused by a metal burr in the bracket that the tensioner wheel screw shaft attaches the wheel too. Tensioning the bracket according to the posts was actually digging the plastic tensions wheel into the burr causing it to stop or grind the wheel and belts. To over come this I went another route. I took a metal toothed wheel that matched the belt teeth, and moved the bearing from the plastic tension wheel to it. I cleaned the burr on the bracket and reassembled the tension wheel onto the bracket and put the x gantray back together. This looks like a manufacturing defect that may be plauging a lot of people seeing drift in their cutting. I also used a medium thread lock on the tension wheel shaft nuts. And, put a drop of light machine oil on the bearing inside. Hope this helps. 




---
*Imported from [Google+](https://plus.google.com/110371358543635278419/posts/Yv6VyuJPJ43) &mdash; content and formatting may not be reliable*
