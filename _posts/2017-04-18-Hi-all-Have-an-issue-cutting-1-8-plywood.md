---
layout: post
title: "Hi all. Have an issue cutting 1/8\" plywood"
date: April 18, 2017 17:41
category: "Hardware and Laser settings"
author: "Arion McCartney"
---
Hi all.  Have an issue cutting 1/8" plywood.  Maybe I am missing something... I can cut 1/4" solid wood with two passes @ 9mA no problem.  When cutting plywood, some areas cut, but most areas don't.  I've read somewhere a while back that the glue used in the plywood could be the problem?  I thought I purchased laserable plywood.  Maybe I am wrong.  I made 4 passes at 10mA on this plywood and some areas cut and most areas did not.  I tried to sand the back side to reveal the cuts, but the cuts that did not go through were only half way through the plywood.  I feel like this is a dumb question... Thanks for the help.  Here is the plywood that I am cutting:  [https://www.amazon.com/gp/product/B01MXVB6O6/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01MXVB6O6/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)





**"Arion McCartney"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2017 17:53*

Another option I've seen with plywood I've worked with is not just the glue, but the actual ply layers themselves. One 3mm (~1/8") I tried I couldn't cut through it even with 20 passes, whilst my normal stuff I could cut through quite easily. Turned out that the centre ply was a much harder wood.


---
**3D Laser** *April 18, 2017 18:00*

Some plywood is better than others I have noticed.  Ocooch hardwood has the best plywood IMHO I get 150 sheets at a time for 187.50 shipped (1.05 a sheet plus shipping) cut to 10x14 inches.  It fits perfect in the k40 aT those measurements.  Very rarely do I have an issue cutting in one pass at 10ma and 7ms 


---
**Arion McCartney** *April 18, 2017 18:05*

**+Yuusuf Sallahuddin**​**+Corey Budwine**​ thanks for the info. I guess I'll just try a different plywood :-(  I'll check out Ocooch, thanks for that suggestion.


---
**Ned Hill** *April 18, 2017 18:06*

What cutting speed are you using?  In my experience most problems with plywood come from voids in the sheet that have are filled with the glue which is harder to laser through.  Also check your alignment and make sure you have the plywood being cut at the right focus height.  I can typically cut 1/8" birch ply in one pass at 10mm/s and about 12ma.  Can probably go lower on the power but I want to make sure I get it all cut in one pass.




---
**Arion McCartney** *April 18, 2017 18:19*

**+Ned Hill**​Cutting at 10mm/s. Alignment is good.


---
**Ned Hill** *April 18, 2017 18:49*

I second Ocooch, good material and people.


---
*Imported from [Google+](https://plus.google.com/110543831642314750018/posts/77QDmgmwHfS) &mdash; content and formatting may not be reliable*
