---
layout: post
title: "I just ordered a K40 and I'm wanting to upgrade to the Cohesion3d mini board, but I'm a noob can could use to assistance before I purchase this board"
date: January 23, 2017 19:38
category: "Modification"
author: "matt s"
---
I just ordered a K40 and I'm wanting to upgrade to the Cohesion3d mini board, but I'm a noob can could use to assistance before I purchase this board. I'm wanting to 3d print the rotary attachment to add to my unit. Do I need to order the two additional motors addon's from their website to make this happen? I'm also wanting to build the adjustable bed and possible add a motor to that for easy up and down. Any help with be greatly appreciated. Should I also order the GLCD adapter?





**"matt s"**

---
---
**Alex Krause** *January 23, 2017 19:46*

The remix might be a better fit to save some fuss of changing your config file **+Ray Kholodovsky**​ what do you think?


---
**Alex Krause** *January 23, 2017 19:47*

Also when your K40 arrives please post the connections to the original board if it's the type without the ribbon cable might be easier to upgrade


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 19:50*

Mini, add the 2 drivers, and highly recommend the GLCD adapter (for use with the "RepRap GLCD 12864" you can get on Amazon).


---
**matt s** *January 23, 2017 19:54*

 **+Alex Krause** Awesome, I'll hold off on ordering it until the unit arrives next week and we can go from there. In the meantime I planned on printing an air assist nozzle/laser pointer holder & better exhaust mount. Do you have any recommendations on the files? or have any other must have mods I should be making?  Thank you for your help it's greatly appreciated.


---
**matt s** *January 23, 2017 19:56*

**+Ray Kholodovsky**  Regardless of which k40 arrives it's supposedly gen 3. I should just order a mini?


---
**matt s** *January 23, 2017 19:58*

**+Ray Kholodovsky** I see the positive reviews on this forum about your board but none on your site yet. Once I get everything ordered and assembled I'll be happy to add a review on their for you. 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 19:58*

What Alex said - it depends on the wiring configuration your machine comes with.    Mini is currently sold out and the next batch has a February ETA. But ReMix is in stock and ships quickly.  Depends on what you want to do and how quickly. 


---
**matt s** *January 23, 2017 20:21*

+Ray Kholodovsky (Cohesion3D) I don't mind the wait. I'm just wanting to get the best board possibly and the mini's plug in play aspect seems perfect. I guess I'll just wait until my unit get here take a few shots and go from there.


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 20:22*

I agree with that - post some pics, tag me, and we'll take it from there.


---
**matt s** *January 23, 2017 20:52*

Awesome, Thank you for the help.


---
**matt s** *January 27, 2017 01:08*

**+Ray Kholodovsky**Hello, My K40 arrived today and this was the board that came with my unit. Any suggestions on which board I should order?

![images/157c1fff5cd2df9374201826710d7578.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/157c1fff5cd2df9374201826710d7578.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 01:12*

I see ribbon cable. Get the mini, no question. GLCD Adapter and additional drivers as we discussed above. 


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 01:13*

One more thought... Lightobject sells a rotary and they insist you need an external stepper driver (big black box to power that motor).  I don't know.  But we also have an external stepper adapter to make wiring such a black box driver easier.  


---
**matt s** *January 27, 2017 01:38*

**+Ray Kholodovsky**Ok I'll order the mini shortly. I think I was going to 3d print the parts and build a rotary for the unit when I build the adjustable bed, but I'm not sure if it will be worth the hassle or not compared to buying one from lightobjects. Thank you for the help its greatly appreciated. 


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 01:40*

Yeah, I've seen a few DIY made rotaries as well.  You should be fine with with the extra A4988 driver but I'm just telling you what I've seen.


---
**matt s** *January 27, 2017 02:11*

If you think getting the wiring adapter will save me in headaches later I'll def add one to my order. **+Ray Kholodovsky** 


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 02:30*

It's $2.50 just in case, the way I see it.


---
*Imported from [Google+](https://plus.google.com/106809986839735143620/posts/RAkkZqop58Q) &mdash; content and formatting may not be reliable*
