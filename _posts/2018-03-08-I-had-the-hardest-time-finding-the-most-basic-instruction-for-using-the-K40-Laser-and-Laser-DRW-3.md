---
layout: post
title: "I had the hardest time finding the most basic instruction for using the K40 Laser and Laser DRW 3"
date: March 08, 2018 02:40
category: "Discussion"
author: "robdude1969"
---
I had the hardest time finding the most basic instruction for using the K40 Laser and Laser DRW 3. Here is a beginners step by step k-40 laser first use video using laserdrw3 and anodized aluminum for the work. There is much room for improvement from this point forward, but this is going to make a print that looks like what you fed it.


{% include youtubePlayer.html id="kI8PljQ4f1o" %}
[https://youtu.be/kI8PljQ4f1o](https://youtu.be/kI8PljQ4f1o)





**"robdude1969"**

---


---
*Imported from [Google+](https://plus.google.com/111096344623823643646/posts/BnaiZftfXBV) &mdash; content and formatting may not be reliable*
