---
layout: post
title: "I'm thinking of my laser as an investment now as I will be dumping a fair amount of money into it ($~500)"
date: March 04, 2017 01:13
category: "Discussion"
author: "Abe Fouhy"
---
I'm thinking of my laser as an investment now as I will be dumping a fair amount of money into it ($~500). How many of you make money off your machine? Can you provide some specifics on $/hr you shoot for, $/job and some metrics on how many jobs I'm looking at to pay for this to break even? How long did it take you to break even?



Thanks in advance!





**"Abe Fouhy"**

---
---
**Phillip Conroy** *March 04, 2017 02:46*

I aim for $30 to $40  an hour of cutting time,I do not charge for file creation as I mainly do this while waiting for laser to  is cutting.I avoid engraving as takes to long. I have about 6 people I sell to on a regular basis and give discounts for Bulk orders over 10 items.

It is not so much the laser cutter that Costs the most but all the extras paid $500 for laser cutter and $2000 on replacement tubes (2) powers up ply, air assist,watercolor, aircompressor etc.

My tube lasted 600 hours a cording to my hour meter (not all cutting 75%0


---
**Abe Fouhy** *March 04, 2017 03:02*

Oh wow, so rough estimate at 600 hrs x $35/hr = $21,000. So so $21000-$2500 = $18,500! How many months for this work? Do you need help to get more output?


---
**Abe Fouhy** *March 04, 2017 04:20*

**+Phillip Conroy** why two tubes in 600hrs, aren't they 1000 hes each?


---
**Don Kleinschnitz Jr.** *March 04, 2017 04:41*

**+Phillip Conroy** what coolant do you use?




---
**Phillip Conroy** *March 04, 2017 06:03*

Not quit more like $7000 ,a lot of first couple Hurd red hours was learning ,some hours make 50 others 10 .the tubes are rated at 1000 hours ,if tested in one go maybe ,not in real life  ,power for most of these hours was 10ma.

For coolant I use cheapest car radiator green stuff.


---
**Abe Fouhy** *March 04, 2017 06:07*

To do lets say a cutting board or kitchen spoon what is a reasonable price? As stated before trying out the bamboo cases when i get up and running. Still $7k is respectable. What time frame is that in? All year or around Christmas?


---
**Phillip Conroy** *March 04, 2017 21:03*

All year


---
**Abe Fouhy** *March 04, 2017 21:07*

**+Phillip Conroy**​ Cool. Yearly how much do you think is possible part time?


---
**Phillip Conroy** *March 05, 2017 01:01*

Depends on how many hours you put into it .I make for a couple of resellers on facebook ,so for them only charge $30 -hour of cutting time? 


---
**Phillip Conroy** *March 05, 2017 01:04*

Abe what items are you thinking of selling


---
**Phillip Conroy** *March 05, 2017 01:06*

Maybe find a local cake shop and make custom cake name toppers,


---
**Phillip Conroy** *March 05, 2017 01:08*

Where in the world are you 


---
**Abe Fouhy** *March 05, 2017 01:22*

Thats a cool idea! I'm in Portland, Or. I was thinking the cell and doing portrait engravings for wedding photos. Possibly engraving the toasting glasses. Thoughts?


---
**Phillip Conroy** *March 05, 2017 01:26*

Portland a tourist town?,if yes than make something with a Portland theme and call into local shops with a sample


---
**Phillip Conroy** *March 05, 2017 01:31*

To e grave glasses you will need a rotary attachment.engraving a glass would take 10 to 15 minutes .at 10 minutes a glass and 6 per hour to make 30 = $5 to engrave each glass ,not including buying the glasses


---
**Abe Fouhy** *March 05, 2017 02:03*

It is a big tourist town! I have a few ideas, has anyone done watches? The glasses sounds like a good idea. I know when I got married we spent about $60 bucks for two and like $100 for the cake knife and spatchula


---
**Kirk Yarina** *March 05, 2017 13:40*

Don't forget this is hobby quality, not especially reliable or durable.  Not all that great for business use, and not something you'd use for commission work.  Good for learning and a fun hobby, but you really should think about a better quality laser to go into business with.   Maybe spend that $2500 up front on a better unit, even that's probably pretty light.  Then plan on more devices if you're starting to be successful, need a spare or two, when you can't afford to be left without a usable tool.  Don't forget the cost of supplies, learning time, etc.



This happened a lot in the early 3D printer days (and probably most new/cheap gadgets).  Spend a few hundred dollars on a printer with dreams of big dollar signs, followed by lots of frustration and disappointment, and a bot in the back of the closet or in the trash.  Sure, there were a few that made a little money, but if it were that cheap and easy you'd see one on every street corner.  Don't bet the mortgage or rent payment, just what you can afford to lose.






---
**Abe Fouhy** *March 05, 2017 18:09*

**+Kirk Yarina**​ my plan was just for fun, but I just figured if I could break even at $500 doing a few jobs and learning at the same time, I could justify to my wife why I am spending so much money on a toy haha


---
**Don Kleinschnitz Jr.** *March 05, 2017 18:14*

**+Kirk Yarina** when and if your conversion is done properly these units will be every bit as good as a commercial unit. 

From a software perspective they will be better than what I have seen. 

That said if your in this to get the same quality but cheaper that's not going to happen. Also if you in this not for the fun of doing the conversion then this is also the wrong place to be. IMO


---
**Phillip Conroy** *March 05, 2017 18:41*

My laser uses stock board and has been very reliable, I do not do many engraving jods (5%) the k40 with air assist and using corel draw  is a very capable unit ,I admit  I am thinking off a better machine at $2500   for a 60 watt 400mm x 600mm cutting bed,only for the quicker speed and thicker cutting capacity, I need bigger cutting bed for very few jobs 

Do not be put off using the k40 to make money. Just be aware that it took me 12 months to learn the machine and software. 

Engraving glasses will require a rotary attachment and uf I was you I would wait on that until you learn more. 


---
**Abe Fouhy** *March 05, 2017 18:47*

**+Phillip Conroy** sounds good, I just need to get it working again. With your amount of work you're doing on it, you could justify a new toy! :)


---
**Phillip Conroy** *March 05, 2017 19:08*

If you do buy a k40 ,get air assist straight away,and keep a logbook of how you do stuff.their are a few traps for newbies like selecting the correct board in the laser cutting software, and making lines 0.001mm thick so that  the machine only cuts the line once .oh and the cutting bed is crap.the clamp is next to useless .I made my own pin bed out of 10 mm long magnets with screws ontop,I only needed 12 to do most jobs .the laser beam will only cut at the focal length plus 2 to 3 mm ie focal length is 50. 8mm plus 2mm =52.8mm aim for the center thickness to be this distance from the bottom of your focal lens  


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/PmU6HbmJCs1) &mdash; content and formatting may not be reliable*
