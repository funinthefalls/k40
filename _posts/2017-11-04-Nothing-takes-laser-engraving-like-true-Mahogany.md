---
layout: post
title: "Nothing takes laser engraving like true Mahogany"
date: November 04, 2017 17:44
category: "Discussion"
author: "Kelly Burns"
---
Nothing takes laser engraving like true  Mahogany.  

![images/9457aa347d968e6abb34bbe3786dc7f7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9457aa347d968e6abb34bbe3786dc7f7.jpeg)



**"Kelly Burns"**

---
---
**Fook INGSOC** *November 04, 2017 18:49*

Add some DRAMA!!!


{% include youtubePlayer.html id="qTOyO6HAC_g" %}
[https://youtu.be/qTOyO6HAC_g](https://youtu.be/qTOyO6HAC_g)


---
**Mike Gallo** *November 05, 2017 17:13*

Hi, may ask what settings did you use to engrave it.  Speed, power etc ecc, thank you.


---
**Kelly Burns** *November 05, 2017 18:57*

**+Mike Gallo** This was 150mm/sec 15% on a K40 with a TRUE 40 Watt tube.  The image is an 2-Color (B&W) Pen and Ink Drawing. 


---
**Mike Gallo** *November 05, 2017 18:58*

Thank you.


---
**Kelly Burns** *November 05, 2017 19:02*

btw...  Sapele LASERS great too. 


---
**Mike Gallo** *November 05, 2017 19:03*

Ok good too know , cheer's


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/1s8huSuX8tM) &mdash; content and formatting may not be reliable*
