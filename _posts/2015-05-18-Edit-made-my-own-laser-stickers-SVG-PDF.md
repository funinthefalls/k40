---
layout: post
title: "Edit: made my own laser stickers. SVG PDF"
date: May 18, 2015 21:53
category: "Repository and designs"
author: "Eric Parker"
---
Edit: made my own laser stickers.

SVG [https://www.poweredbyredstone.com/laser/LaserSticker.svg](https://www.poweredbyredstone.com/laser/LaserSticker.svg)

PDF [https://www.poweredbyredstone.com/laser/LaserSticker.pdf](https://www.poweredbyredstone.com/laser/LaserSticker.pdf)





**"Eric Parker"**

---
---
**Eric Parker** *May 19, 2015 02:07*

feel free to modify/reproduce/whatever.


---
**Stephane Buisson** *May 19, 2015 14:38*

Excellent!  with the right wavelength and power.

thank you


---
*Imported from [Google+](https://plus.google.com/+EricParkerX/posts/WKRSXg2hAA4) &mdash; content and formatting may not be reliable*
