---
layout: post
title: "Here is the latest version of my forever calendar"
date: March 04, 2016 14:37
category: "Object produced with laser"
author: "Ben Walker"
---
Here is the latest version of my forever calendar.   Still a bit of work required to make it perfect before I make one with higher quality wood.   This is on 1/8 Baltic birch with a veneer applied.   Very fun to make. 



![images/763fcc40adf758c54e601a4f327b3607.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/763fcc40adf758c54e601a4f327b3607.jpeg)
![images/4f2c6f1240d604a4fb5a598035500104.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f2c6f1240d604a4fb5a598035500104.jpeg)
![images/b5415871978fdd74e457fc7e55da77e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b5415871978fdd74e457fc7e55da77e4.jpeg)

**"Ben Walker"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 04, 2016 15:38*

Looks good. I like the whole idea of this. It's just literally a forever calendar. Maybe I'm getting greedy, but it would be cool if somehow we could add the ability to put notes for certain days (that could be wiped off).


---
**3D Laser** *March 05, 2016 01:50*

Hey Ben I wanted to send you a private massage but couldn't figure out how on my phone I was wondering if it wouldn't be to much to ask I need a little bit of help with a graphic for one of my designs.  I didn't know if you would be willing to help


---
**Ben Walker** *March 05, 2016 02:26*

**+Corey Budwine** I sent you a hangout message.   That's the closest thing to email on g+.  Let me know if you didn't get it.   


---
**3D Laser** *March 05, 2016 04:04*

**+Ben Walker** got your message sent you one back


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 06, 2016 23:01*

**+Allready Gone** Hey, you're back. Haven't seen you around here for a while. Hope all is well.


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/A6uVwXnUp2J) &mdash; content and formatting may not be reliable*
