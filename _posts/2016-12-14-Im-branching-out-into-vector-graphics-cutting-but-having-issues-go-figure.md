---
layout: post
title: "Im branching out into vector graphics cutting but having issues (go figure)"
date: December 14, 2016 20:42
category: "Original software and hardware issues"
author: "Bob Damato"
---
Im branching out into vector graphics  cutting but having issues (go figure). I made a very simple christmas ornament... circle, snowflake, date.  I made it in photoshop, saved it as JPG, uploaded it to vectormagic to convert (I dont have illustrator), then open in corellaser.  I make a 5x5 box, import it as svg and there it is! looks great! Problem is when I hit cut, I get the outline of the ball and thats it. nothing on the inside. Ive tried "all pages" and "Only selected" (while selecting all) etc.  The funny thing is, if I go on the right side layer list and see all the curves, I can select them individually and they are actually highlighting as curves.  

Ive tried importing as pdf, svg, eps, etc. all with pretty much the same results.



If I select a handful of the curves by themselves those  selected ones will cut, but thats a band aid, not the answer.  What am I doing wrong?  100% stock setup, corellaser.







**"Bob Damato"**

---
---
**Bob Damato** *December 14, 2016 22:20*

I  would like to add that when I go to hit 'starting', on that preview screen the design looks exactly correct and complete. but it doesnt cut it all.



This is the preview just fine. But I only get the outline of the ball cut.

[http://maxboostracing.com/preview.jpg](http://maxboostracing.com/preview.jpg)


---
**Bob Damato** *December 14, 2016 23:26*

Also, if I cut as a jpg, it does the whole thing, but we all know how crappy, slow and messy that is.




---
**Ned Hill** *December 15, 2016 00:18*

Make sure the white areas inside the ball have no fill.  Even if it has white color, corellaser will treat it as a solid object and just do the outline.


---
**Bob Damato** *December 15, 2016 00:21*

Ahhh interesting, thank you **+Ned Hill** This is where my coreldraw ignorance comes in.  Ill see if I can figure that out!




---
**Bob Damato** *December 15, 2016 00:22*

**+Ned Hill** Is that done as Im making the vector image in vectormagic, or once I import it into coreldraw?




---
**Ned Hill** *December 15, 2016 00:24*

Corellaser get's a little squirrelly with images composed of curves sometimes.  I've had to resort to etching images with jpg output files before to get around this.


---
**Ned Hill** *December 15, 2016 01:17*

**+Bob Damato** I imagine you need to do it before coreldraw.  Probably need to create it with no background.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2016 06:44*

I think your issue is actually up in the cut options. Not 100% sure, but you have it set to "Style: Cutting... As Drawing"



You may want to set it to "Style: Cutting... Inside First" & give that a go & see if it makes any difference.


---
**Bob Damato** *December 15, 2016 12:16*

**+Yuusuf Sallahuddin** thank you. I did try inside first but it still gave me the middle finger. I did not try as drawing. Ill definitely try that tonight!




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 15, 2016 12:33*

**+Bob Damato** Must be something else going on. Would be interesting to know what's going on. Unfortunately I can't test the file from my end as I'm no longer using the stock controller. But might be worthwhile someone else testing it to see if the issue replicates on their end.


---
**Bob Damato** *December 15, 2016 14:18*

**+Yuusuf Sallahuddin** I can certainly post a link for it as well as the cdr if anyone wants to give it a shot!


---
**Ned Hill** *December 15, 2016 18:13*

**+Bob Damato** I'm still stock for a while longer so I would be happy to take a look at it if you still can't get it to work.


---
**Bob Damato** *December 15, 2016 22:47*

Thanks **+Ned Hill**  I just put two files up there. [maxboostracing.com - maxboostracing.com/Laser/LRP_Ornament_Simon.svg](http://maxboostracing.com/Laser/LRP_Ornament_Simon.svg) and [http://maxboostracing.com/Laser/SimonOrnament.cdr](http://maxboostracing.com/Laser/SimonOrnament.cdr)




---
**Ned Hill** *December 15, 2016 23:33*

**+Bob Damato** **+Yuusuf Sallahuddin**  Ok, it's what I thought was happening.  When you converted your jpg to svg  the white elements in the center became curves that are sitting on top of the black background shape of the ornament.  In the pic below, I offset the white curves from the black background so you can see what I'm talking about.  So all corellaser sees is the black background and just does the outline since the interior black parts are really part of the background.

![images/2c573d3e2c95c5e2616cc4caeaf41db2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2c573d3e2c95c5e2616cc4caeaf41db2.png)


---
**Ned Hill** *December 15, 2016 23:33*

To fix this you have two options.  The easiest is to just delete the white box curve and then give all the other curves no fill and set the curve outlines to black at 0.01mm (bottom pic).  The other way is to convert the curves to bitmap and then do an outline trace and delete all the white parts (top pic).  Let me know if you have any questions.

![images/4f309b433bb90be7a7a9e57bd9ab57e0.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4f309b433bb90be7a7a9e57bd9ab57e0.png)


---
**Bob Damato** *December 16, 2016 03:30*

Holy cow **+Ned Hill**  It seems I do have a bit of a learning curve with the vector side of the world. I see exactly what you are saying now though, and it makes sense. Ill play with it this weekend and let you know how it goes. Thank you so much!!




---
**Bob Damato** *December 19, 2016 16:17*

**+Ned Hill** That did it. The easy way for me was to turn it into a bmp then do a quick trace. Probably not the most ideal way to do it, but it came out great. Thank you again!




---
**Ned Hill** *December 19, 2016 16:42*

You're very welcome, glad I could help :D


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/CzVuF2AjzZ8) &mdash; content and formatting may not be reliable*
