---
layout: post
title: "Hello what speed and power do you use for cutting in 3 mm acrylic?"
date: September 07, 2016 15:02
category: "Materials and settings"
author: "Mikkel Steffensen"
---
Hello what speed and power do you use for cutting in 3 mm acrylic? 





**"Mikkel Steffensen"**

---
---
**Ariel Yahni (UniKpty)** *September 07, 2016 15:14*

This was poll I did. On my best alignment I got 10mA @ 10mms 1 pass [https://plus.google.com/+ArielYahni/posts/6nAq4E9LhbR](https://plus.google.com/+ArielYahni/posts/6nAq4E9LhbR)


---
**greg greene** *September 07, 2016 15:14*

6 to 7 ma and 20mm should do it,  But it depends on how good your alignment is and focus distance


---
**Ariel Yahni (UniKpty)** *September 07, 2016 15:16*

Wow **+greg greene**​ that's a very God alignment to get thefe


---
**greg greene** *September 07, 2016 16:05*

Took me quite a few attempts to get it - but she cuts like a champ now !!!!


---
**Mikkel Steffensen** *September 07, 2016 16:25*

Okay. Thank you. It was just because I can't cut through 3 mm acrylic with a speed of 12 mms and 25 mA. Is it because I have a bad alignment?


---
**Ariel Yahni (UniKpty)** *September 07, 2016 16:30*

**+Mikkel Steffensen**​ o yes very bad, at least you should cut 3mm at 5mms with 15 MA power 


---
**greg greene** *September 07, 2016 16:31*

Wow, I can cut 7 to 8mm cedar at that speed with10 ma on one pass - but lots of smoke


---
**Ariel Yahni (UniKpty)** *September 07, 2016 16:42*

**+greg greene**​ you are killing me here. What's your. Back magic??? 


---
**greg greene** *September 07, 2016 17:05*

I followed the alignment instructions on [theflyingwombat.com](http://theflyingwombat.com) - took several attempts - but I get a single spot in all positions - did have a problem with the air assist head holder in that part of the beam was hitting the side of the nozzle, after I corrected that it started to work great.


---
**Ariel Yahni (UniKpty)** *September 07, 2016 17:07*

How did you correct that specific part of the beam hitting the cone? 


---
**Ariel Yahni (UniKpty)** *September 07, 2016 17:08*

Also I that the correct link? 


---
**greg greene** *September 07, 2016 17:10*

It took a washer under one corner of the head holder.  I put tape of the nozzle and moved the head with washers till the beam hit the tape in the center of the nozzle.  Took awhile to do, but well worth doing.


---
**Ulf Stahmer** *September 07, 2016 17:16*

**+greg greene** do you mean these instructions? [floatingwombat.me.uk - www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**greg greene** *September 07, 2016 17:19*

Yup - repeat as necessary !


---
**greg greene** *September 07, 2016 17:20*

And yes - it is essential to get the beam hitting in the centre of the mirrors, else you will have a very hard time getting a single spot on the head mirror.


---
*Imported from [Google+](https://plus.google.com/117825343986845004843/posts/X8zMiK8MV3V) &mdash; content and formatting may not be reliable*
