---
layout: post
title: "Newbie question, so I want to cut very small lettering into wood so need very fine lines but need a lot of power as want to do it on one go, so if I were to change the lense would this accomplish that and if so what kind of"
date: March 26, 2017 10:27
category: "Modification"
author: "Martin McCarthy (NGB Discos)"
---
Newbie question,  so I want to cut very small lettering into wood so need very fine lines but need a lot of power as want to do it on one go,  so if I were to  change the lense would this accomplish that and if so what kind of lense would I need? 





**"Martin McCarthy (NGB Discos)"**

---
---
**Ned Hill** *March 26, 2017 14:47*

What thickness of wood and how small would the lettering be?


---
**Mark Brown** *March 26, 2017 19:44*

And do you want to cut completely through, or engrave on the surface?


---
**Martin McCarthy (NGB Discos)** *March 29, 2017 09:52*

its will be on a circular piece of wood, about 32mm in diameter so the letters would only be a 1-1.5mm high and want to cut all the way through 




---
**Ned Hill** *March 29, 2017 12:28*

Don't think it is possible with these machines.  A a smaller focal length lens will give you a smaller spot size but you also get a shorter depth of field which is bad for cutting as material thickness increases.  Also the thicker the wood the more power you will to apply to cut which leads to more burn out (wider kerf).  Even with 1/8" (3mm) wood the letters will a mess at that size.  Not to mention having to accommodate thin lines to connect things like the center of an "O".


---
*Imported from [Google+](https://plus.google.com/108373254461031274579/posts/79gfoV1P7gH) &mdash; content and formatting may not be reliable*
