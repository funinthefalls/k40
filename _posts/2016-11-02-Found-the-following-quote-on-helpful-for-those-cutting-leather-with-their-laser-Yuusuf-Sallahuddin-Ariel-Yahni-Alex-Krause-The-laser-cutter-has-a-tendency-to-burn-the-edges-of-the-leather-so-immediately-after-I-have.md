---
layout: post
title: "Found the following quote on , helpful for those cutting leather with their laser: Yuusuf Sallahuddin Ariel Yahni Alex Krause \"The laser cutter has a tendency to burn the edges of the leather so immediately after I have"
date: November 02, 2016 08:04
category: "Materials and settings"
author: "Anthony Bolgar"
---
Found the following quote on [instructables.com](http://instructables.com), helpful for those cutting leather with their laser: **+Yuusuf Sallahuddin** **+Ariel Yahni** **+Alex Krause** 



"The laser cutter has a tendency to burn the edges of the leather so immediately after I have finished my cut I submerge all the pieces in warm water and agitate them to remove the soot. This precaution helps reduce the smell of burnt leather and also makes the pieces supple for the hand lasting process. I leave the leather soaking in water for about half an hour. You can also use a brush or cloth along your edges to help reduce the soot."





**"Anthony Bolgar"**

---
---
**Stephane Buisson** *November 02, 2016 09:28*

out of subject but interresting


{% include youtubePlayer.html id="IaSm3zoWrAo" %}
[https://www.youtube.com/watch?v=IaSm3zoWrAo](https://www.youtube.com/watch?v=IaSm3zoWrAo)


{% include youtubePlayer.html id="IaSm3zoWrAo" %}
[youtube.com - LEATHER UPHOLSTERY- A Plastic Piece Wrapped in Leather](https://www.youtube.com/watch?v=IaSm3zoWrAo)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 02, 2016 12:32*

Thanks **+Anthony Bolgar**. That's interesting & will have to give that an attempt.



A note to keep in mind though, wetting leather will allow it to stretch or bend easily... so when drying do it on a flat surface where you can get air on both sides (e.g. cake rack)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/6AVDxyp9ueF) &mdash; content and formatting may not be reliable*
