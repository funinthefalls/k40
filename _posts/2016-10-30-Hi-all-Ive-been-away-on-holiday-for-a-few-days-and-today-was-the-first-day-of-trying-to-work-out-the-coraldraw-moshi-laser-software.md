---
layout: post
title: "Hi all, Ive been away on holiday for a few days and today was the first day of trying to work out the coraldraw/moshi laser software"
date: October 30, 2016 22:03
category: "Original software and hardware issues"
author: "Andy Shilling"
---
Hi all, Ive been away on holiday for a few days and today was the first day of trying to work out the coraldraw/moshi laser software.

After doing a few engravings into MDF I left it for a bit. I then went back to try and work out how to zero position the laser and realised  the head was not moving according to position in the software. Then I also realised the green item in the photo has started to arc across to the chasis. Is this part replaceable (if so what is it) or could I just insulate it more where it is arcing across.

![images/10aef58f1a47a9846343be8b89e34d68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/10aef58f1a47a9846343be8b89e34d68.jpeg)



**"Andy Shilling"**

---
---
**Scott Marshall** *October 31, 2016 00:11*

It's the ballast resistor, and should be replaced. Looks past saving.



These aren't used anymore on the new machines, but I'll look to see if I can find a value for you, and I can tell you where to source one.



Scott



If you remove it, there may be a value printed on it, if so, it's easy to track down a substitute. They're not expensive, $10-15 tops.



It's a 50ohm 50 watt. 

Source coming shortly

Scott



I buy a lot of stuff from Digi-key (for about 40yrs now)

1st class operation



$3.64 and in stock. They ship any way you like, that will go USPS 1st class cheap.



[http://www.digikey.com/product-detail/en/stackpole-electronics-inc/KAL50FB50R0/KAL50FB50R0-ND/1646206](http://www.digikey.com/product-detail/en/stackpole-electronics-inc/KAL50FB50R0/KAL50FB50R0-ND/1646206)



You might find one on ebay, but all else being equal, I'd buy from DK



Scott



Edit (last)

 Don't try to run with it bad, it grounds the tube and if it opens (burns out) it will expose the wiring to the full 15kv and created a safety hazard, especially if you have a defective chassis ground which is very common on the K40 (they use slide on terminals which slide off very easily).

Since it's pulled to ground, your replacement need not be a high voltage resistor. 



When you install the replacement, solder or clamp/screw securely. you don't want it coming loose for the same reason as above.



Good luck, Scott


---
**Andy Shilling** *October 31, 2016 06:19*

Cheers buddy I'll look at finding one tonight here in the uk. If I don't have any luck I'll try Dk and see what the postage will be like. Thanks once again 


---
**Steve Brown** *October 31, 2016 07:54*

Noooo the value of the resistor is 50k as in 50,000 ohms. 


---
**Andy Shilling** *October 31, 2016 09:49*

Ok so what do I need lol a 50k or 50ohms? And do these go on a regular basis so is it worth getting two or more? 


---
**Steve Brown** *October 31, 2016 10:10*

50k. There's, what, 15mA of current flowing so ohms law tells us 50ohms would drop about 0.75 volts which doesn't sound useful for a ballast resistor.  50k would drop 750 volts which sounds more reasonable where there's 15kv involved.


---
**Andy Shilling** *October 31, 2016 10:13*

I found a link to this on another page and think this is what I need. It's a bit different to mine but these k40 aren't known for using quality components I've found out.



[uk.farnell.com - RESISTOR, AL CLAD, 50W 50K 5%](http://uk.farnell.com/te-connectivity-cgs/ths5050kj/resistor-al-clad-50w-50k-5/dp/1259521)


---
**Andy Shilling** *October 31, 2016 21:45*

Ok so I've order the replacement Ballast resistor but I have now realised that my control board may also be  dead. Usually when I turn the machine on the head returns to the top left corner but this is not happening  and even trying to set a job Corallaser knows the machine is attached and ready but will not do anything.



Luckily I have a slightly old board I can swap it out with but is there anything that may be worth looking at trying to replace on the board?

![images/95dfef55275eeb342ae4069480828fa6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/95dfef55275eeb342ae4069480828fa6.jpeg)


---
**Steve Brown** *October 31, 2016 22:40*

Have you checked the PSU rails are correct? 5v and 24v if that's what yours has? The 24v supply failed on mine before which caused similar symptoms 


---
**Andy Shilling** *October 31, 2016 22:51*

I will look at that tomorrow when I get home. I'm presuming it is the control board because when I connected the older version board I have it fired up as it did originally. 



I can't actually run the laser because of the ballast resistor but I should be able to run the older board with the laser turned off to check that does indeed work.



I know I'll end up swapping out to a Smoothie before long but I need to get this running again so I can learn the basics of using corallaser  before moving on to a better system.


---
**Andy Shilling** *November 01, 2016 22:05*

Right back up and running, new ballast resister fitted thanks Scott and Steve for the info on this one. 



Looks like my old board is done as it will not make the head move. I've got my v3.9 moshi on there at the moment and everything is back to how it should be.



What sort of price am I looking at to upgrade to a Smoothie board and lose this moshi stuff?


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/FJp1zfVcEgY) &mdash; content and formatting may not be reliable*
