---
layout: post
title: "Finally got around to that control panel"
date: March 01, 2017 00:29
category: "Modification"
author: "Madyn3D CNC, LLC"
---
Finally got around to that control panel. It's laser cut/engraved 1/4" acrylic with some specialty metallic carbon fiber vinyl plotted on the vinyl cutter. Display is backlit with a few blue LED's but the camera is failing at picking that up.



These are incredibly easy to design and configure, but for what it's worth if anyone would like the CORELdraw PNG file just let me know and I'll send it. 



This was one of the first mods I had in mind when getting the laser. Over a year later and it's finally done. (took about 2 hours from design to installation) I suppose mechanical/electrical mods that actually enhance performance were more important. Now I just need a new tube before this one goes, as it's made it for a good year already and I expect it to crap out on me any day now. I believe **+Jon Bruno** and I were having a discussion about that ;) ;) ;) 



<b>DESCRIPTION</b>

White momentary e-switches are for z-table, dual laser alignment engagement,  and test fire. The DVM displays are for a 12v lipo source (LED's) battery voltage level and **+Don Kleinschnitz**'s schematic for digital pot value (0-5v). The guage is just an air pressure guage to indicate air assist is working properly, and the main breaker switch has 3 gates, PSU/control board/water pump. Water turns on when the machine turns on. 



Also, just going to tag **+Ray Kholodovsky** so he can spot the love. (...IF he can make out the engravings with this crappy camera and lighting)



![images/a3819396b440652bab3d9a1acc9d7c45.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3819396b440652bab3d9a1acc9d7c45.jpeg)
![images/beac99b9178d710ab76dd48c97030a00.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/beac99b9178d710ab76dd48c97030a00.jpeg)
![images/d763886656d41fadf8bf04d69c9a305a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d763886656d41fadf8bf04d69c9a305a.jpeg)
![images/1c892fff4eaf34bd8bd9df92a7b11fb8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c892fff4eaf34bd8bd9df92a7b11fb8.jpeg)
![images/b656b84c081a9db252eaecfc6b96079e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b656b84c081a9db252eaecfc6b96079e.jpeg)

**"Madyn3D CNC, LLC"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 00:33*

I can read it :)

Wait is my board in this machine? The connector pattern doesn't quite look like it in that last pic, what little I can see. 

More pics? Tell me more!


---
**greg greene** *March 01, 2017 01:07*

I want it I want it I want it !!!

Do you have my Email addr?


---
**Madyn3D CNC, LLC** *March 01, 2017 14:39*

The Cohesion board will be in there by the time this laser makes it back down into the shop :) 



It's been getting a major overhaul over the last 3-4 weeks, right now I'm finishing up the z-table and interlocks and then I'll be tearing out the M2 nano before it's ultimately moved back into the shop and put back to work. 



I already purchased a smoothie kit with the bells and whistles a while back, and once I started researching for installation is when I learned about the Cohesion3D boards. So then I thought- "wait a minute" 



After lurking in the Cohesion group for a few weeks, watching, evaluating overall feedback, from some of the first beta guys, I decided to sell the smoothie to my buddy who's building a kossel and go with the Cohesion board instead.  



Once all is said and done, I'll be posting plenty of pics in the Cohesion group and taking part in the community. I can't wait!


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 14:40*

Woohoo!


---
**greg greene** *March 01, 2017 14:43*

you will like it !!


---
**Madyn3D CNC, LLC** *March 01, 2017 14:47*

Greg, here's the .cdr file for the panel. If for some reason it won't work I can email it but I double checked on my end and it opens right up in corel :) 



 [accounts.google.com - Meet Google Drive – One place for all your files](https://drive.google.com/drive/folders/0BxQBchV_lLZASkIzWGEwbGVaMnc)


---
**L Y** *March 30, 2017 22:14*

you use coreldraw with your cohesion board?


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/3CoaKqGe343) &mdash; content and formatting may not be reliable*
