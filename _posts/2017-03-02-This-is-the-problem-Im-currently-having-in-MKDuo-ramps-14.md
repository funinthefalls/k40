---
layout: post
title: "This is the problem I'm currently having in MKDuo (ramps 1.4)"
date: March 02, 2017 20:38
category: "Software"
author: "timne0"
---
This is the problem I'm currently having in MKDuo (ramps 1.4). 2nd image shows three test lines (amongst the stuff worked fine) which are basically really wobbly. Any ideas? Have upgraded the software and the laser head recently... Hoping it's not mechanical.



![images/c52943795c278279be508840fa72e71b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c52943795c278279be508840fa72e71b.jpeg)
![images/43521c35dc2a0fcb351d35fd4de838e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43521c35dc2a0fcb351d35fd4de838e4.jpeg)

**"timne0"**

---
---
**greg greene** *March 02, 2017 20:53*

looks like the beam hitting the inside of the head - or air assist nozzle.


---
**timne0** *March 02, 2017 21:08*

Have spent about three hours getting the beam dead center. Maybe it's not then! Gah.


---
**greg greene** *March 02, 2017 21:18*

Centered on the Head? Centered where it exists the head? Is the lens in straight?


---
**timne0** *March 02, 2017 21:19*

Centered on each mirror and the lens.


---
**timne0** *March 02, 2017 21:19*

But I'm not saying it's perfect. I'll have another look


---
**greg greene** *March 02, 2017 21:20*

If the head has a nozzle, it also has to be centered EXITING the head.


---
**timne0** *March 02, 2017 21:21*

I got it dead center on the lens. Not sure how I can check its exiting the head centrally.


---
**greg greene** *March 02, 2017 21:23*

Put a piece of tape over the nozzle


---
**timne0** *March 02, 2017 21:25*

I've been using bits of paper, will use tape and double check it.


---
**greg greene** *March 02, 2017 21:32*

air nozzle?


---
**timne0** *March 02, 2017 21:32*

Will do both.


---
**timne0** *June 22, 2017 12:38*

This problem is in firmware.  We're hoping it's all solved now.




---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/P6RBiE6UKxX) &mdash; content and formatting may not be reliable*
