---
layout: post
title: "Looking to upgrade my extraction setup. I've read people saying that you want about 400cfm, looking at a 6\" one of the below"
date: August 07, 2015 08:02
category: "Modification"
author: "I Laser"
---
Looking to upgrade my extraction setup. I've read people saying that you want about 400cfm, looking at a 6" one of the below. Will this be suitable?

Seems to push enough air at about 420cfm but I'm a bit concerned with build up. I burn a lot of MDF and the current setup utilising 90mm Tornado case fans inline with the original centrifugal fan seem to be building up a bit of soot.





**"I Laser"**

---
---
**Fadi Kahhaleh** *August 07, 2015 16:06*

I have a 4" version of a similar product. It does work, but when I do Acrylic/Plexiglass I can smell the fumes, so I am thinking of getting the 6" version for more CFM.


---
**I Laser** *August 08, 2015 03:58*

Thanks Fadi. The 6" has twice the power so should be enough then. Still a bit concerned about the glue residue from the smoke, it seems to be choking up my existing setup a bit.


---
**Fadi Kahhaleh** *August 08, 2015 05:00*

hmm I didn't open up my piping just yet, however I did install a mesh screen (just like the one that keeps bugs out of your home) and you can see that it already accumulated some black burnt residue on it wiping with my finger is proof enough :)



You can read my post on the group here about the adapter box that I have done, and you can easily see that fitting a filter like the one used in Microwave suction ducts is possible and that should take care of all the soot that is coming out of MDF.   


---
**ThantiK** *August 08, 2015 20:08*

So, don't set this up <i>near</i> the laser.  A lot of people make that mistake.  If you're smelling fumes, it's because you've got a leak of positive pressure on the other side of the fan.



Install your venting, and put this fan at the <i>end</i> (away from the laser), so that you have a vacuum inside the hose that shares your breathing space.


---
**Fadi Kahhaleh** *August 08, 2015 20:34*

**+ThantiK** 

So you are saying to keep the blower (Fume Extractor) at a distance from the vent-out of of the K40 Machine?   Wouldn't a longer hose cause less suction to occur?



I have it roughly 3 feet away anyways, but I am unsure what you are trying to relay and if my setup falls under the leak of positive pressure case or not!


---
**ThantiK** *August 08, 2015 20:42*

If you have your hose properly secured on the vacuum side then no, it won't cause less suction to occur.



[http://www.rowmark.com/MARK/laser_guide/images/intrlsr/lzrvnt%201.jpg](http://www.rowmark.com/MARK/laser_guide/images/intrlsr/lzrvnt%201.jpg) -- Correct way to vent.  (White block is the blower)  If there <i>are</i> any leaks in the tubing, it will merely suck the air from the room into the pipe and distribute it outside of the breathing area.



[https://www.parallax.com/sites/default/files/news/Vent-System-03.png](https://www.parallax.com/sites/default/files/news/Vent-System-03.png) -- Incorrect way to vent.  The positive-pressure tubing is sharing the same breathing space, so any leaks will leak acrylic fumes into the laser cutting area.


---
**Fadi Kahhaleh** *August 08, 2015 20:50*

Oh ok.. now I understand what you were saying!



It'll require major re-work and probably some wall damage on my end to make it vent out side directly... arrg... will try to get it as close to the wall as possible and compare.

But your suggestion makes sense.


---
**ThantiK** *August 08, 2015 21:09*

**+Fadi Kahhaleh**, I know it might be a pain - but not breathing in ablated acrylic is totally worth it.


---
**Fadi Kahhaleh** *August 08, 2015 21:55*

**+ThantiK** Agreed.

I noticed that the fumes only come out after  10+ minutes of acrylic burning, if I am doing a small job, I do not smell it... oh well.. back to the drawing board :)


---
**I Laser** *August 10, 2015 01:28*

Have similar issues having the fan on the opposite end, though will have to work around it because the glue is clogging up my current setup and I don't want to screw the new fan! :\



I wear a Sundstrom mask when working, as it's not unusual for the smoke to blow back at the moment. Thanks for the replies guys.


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/PuM2Bf7Wd6U) &mdash; content and formatting may not be reliable*
