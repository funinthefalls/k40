---
layout: post
title: "Hi I NEED HELP I bought a K40 machine, got it last week"
date: May 04, 2018 21:15
category: "Original software and hardware issues"
author: "Denisse Bazbaz"
---
Hi 

I NEED HELP

I bought a K40 machine, got it last week. I tried to install the driver that is included with the CD and it appears driver install failure. (the driver that it appears is engraving.inf) 

I could download LsaerDRW software but when I try to do something on the machine, it appears when I try to engrave and click starts "This function needs USB-KEY support!". 

I have both the yellow usb and the USB cable which is connected to the machine. I think my computer isn't recognizing the machine and thats why i can't use it. 

Can someone please help me start using the machine

Thanks





**"Denisse Bazbaz"**

---
---
**James Rivera** *May 04, 2018 22:26*

I would <b>highly</b> recommend not even trying to use that software, and instead install K40 Whisperer.

[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
**Denisse Bazbaz** *May 04, 2018 22:55*

**+James Rivera** Hi I already downloaded that one, but also appears that USB Error: Laser USB device not found. What I do? 


---
**Denisse Bazbaz** *May 04, 2018 22:56*

I followed the instructions on the website, but neither can make it work... **+James Rivera**






---
**Doug LaRue** *May 05, 2018 01:55*

Some had to use other USB port. Does the laser head move to the home position when you turn on the laser cutter before plugging the USB cable into the computer?


---
**Paul de Groot** *May 05, 2018 02:22*

The usb stick is not a usb at all but it is a dongle with a serial code. I don't understand why eBay seller don't provide even the basic support. It's literally shuffle boxes. I provide drop in controllers since the stock toolchain is not working and highly complex. Next to this is the missing ground in some of these machines which causes all kind of comms issues. Check it. 


---
**Doug LaRue** *May 05, 2018 02:25*

Yes but the OP said K40 Whisperer wouldn't work either and that requires no USB dongle.


---
**Denisse Bazbaz** *May 06, 2018 02:01*

**+Doug LaRue** No indeed the laser head doesnt move when I turn it on, I thought this was because the computer didn't recognize it. 


---
**Doug LaRue** *May 06, 2018 02:05*

Something up with the controller and/or wiring then.


---
**Doug LaRue** *May 06, 2018 02:07*

Should auto home on power ON with or without a computer connected.




---
**Denisse Bazbaz** *May 06, 2018 02:11*

**+Doug LaRue** how can I check it?

I already connect it again 


---
**Denisse Bazbaz** *May 06, 2018 02:14*

**+Doug LaRue** What is the ground wire? On one video I look that I need to connect it but i think it is already connected


---
**Doug LaRue** *May 06, 2018 02:19*

As another stated "If you have a 3-prong cable with proper ground in your wall outlet, you dont need to add any extra ground.

Only remove any plastic washers on the ground bolt and sand away some paint to have proper connection to the metal case."


---
**Doug LaRue** *May 06, 2018 02:26*

Red LED on the power supply should turn when you turn the Power Switch on too. If you have a big red E-Stop button on your machine that too has to be in the correct position. IIRC, it should turn a little bit and pop or pull out.  E-Stop means Emergency Stop and you just hit it/push it in to stop the machine in emergency(like fire).




---
**Denisse Bazbaz** *May 06, 2018 02:26*

**+Doug LaRue** Its turning on, but only the laser head don't moves when I turn it on




---
**Doug LaRue** *May 06, 2018 02:26*

can you post a picture of your top panel?




---
**Denisse Bazbaz** *May 06, 2018 02:28*

**+Doug LaRue** Everything turns on fine, I don't have an E-stop, but the button turns on I think it might be another problem, do you now what else it might be? 


---
**Denisse Bazbaz** *May 06, 2018 02:30*

**+Doug LaRue**

![images/f20f94950007d30e7d0fac14edf4c1fe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f20f94950007d30e7d0fac14edf4c1fe.jpeg)


---
**Denisse Bazbaz** *May 06, 2018 02:31*

This is the laser head when I turn it on **+Doug LaRue**

![images/65cb6d4770b2a27d8d0f5e1a6468c237.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65cb6d4770b2a27d8d0f5e1a6468c237.jpeg)


---
**Doug LaRue** *May 06, 2018 02:40*

This is an all around good site for all things K40 related - [k40laser.se - Electrical warning - K40 Laser](https://k40laser.se/electrical-warning/)


---
**Denisse Bazbaz** *May 06, 2018 03:07*

**+Doug LaRue** Hi it was some cables didn't working. Now I got another problem, I already install the K40 but when I do the Initialize Laser Cutter it appears that it wants to move the laser head, but it doesnt moves, like it goes up and thats it. 

I don't know how to make it work. 

Thanks


---
**Doug LaRue** *May 06, 2018 03:59*

i don't know... when the laser head is manually moved to the middle of the cut area and you then turn on the machine, it will move to HOME(0,0) position.  After plugging the USB cable into the computer running K40 Whisperer(Linux in my case) when I hit the "Initialize" button it will again try to move to HOME.  At that point I can use the arrow buttons to move the laser head around.




---
**Denisse Bazbaz** *May 06, 2018 05:11*

**+Doug LaRue** I understand the problem, I accomplish to make the machine work and move, only is that the laser isn't burning nothing when I press engrave button. Do you know what might be causing this problem? 

Thanks again


---
**Doug LaRue** *May 06, 2018 05:40*

Did you read the instruments on K40 Whisperer? Colors have to match desired mode.


---
*Imported from [Google+](https://plus.google.com/112467917885418951758/posts/PbckwwZ97ZQ) &mdash; content and formatting may not be reliable*
