---
layout: post
title: "I am very excited about my new K40 machine"
date: January 04, 2017 17:23
category: "Discussion"
author: "Gail Lingard"
---
I am very excited about my new K40 machine. I am just in the process of putting it together. If you have any recommendations/ tutorials which might help please can you point me in the right direction. 



Thank you.  





**"Gail Lingard"**

---
---
**Ned Hill** *January 04, 2017 17:47*

Take some time and read through this community's posts.  You will pick up a lot of things and avoid some headaches.  Here's a good tutorial on aligning your laser.  Recommend taking a marker and labeling the adjustment screws A,B,C to make following the guide easier. [onedrive.live.com - OneDrive](https://1drv.ms/b/s!AtpKmELEkcL8imoUD99sdJZZT-am)


---
**Gail Lingard** *January 04, 2017 18:19*

Thank you. I am still putting the machine together :) I will definitely be following the alignment guide when I get to that point. Paper removed from the cover, fan attached but not fitting. Time for some reading through the posts. ;)


---
**Gail Lingard** *January 06, 2017 18:15*

first attempts 

[drive.google.com - File_001.jpeg - Google Drive](https://drive.google.com/file/d/0BzNubLxU_-XWS2Z0Tzg2VlNIeWM/view?usp=sharing)


---
**Gail Lingard** *January 06, 2017 18:22*

I have been using your doc.This is as far as I have gone with the alignment of the first mirror. the laser dot seems central but in need of being moved down. A and B can be adjusted any further so I am not sure what to do. Do I raise the plate? **+Ned Hill** any suggestions?

[drive.google.com - File_000.jpg - Google Drive](https://drive.google.com/open?id=0BzNubLxU_-XWdHZFMW9BVVhLVWs)


---
**Ned Hill** *January 06, 2017 19:39*

Yes you will probably need to shim the plate.  I had to do that as well.  I did it with some washers under each screw.  Note that you may also need to shim the laser head plate as well.


---
*Imported from [Google+](https://plus.google.com/105833930009921938226/posts/JdXcK6WqLW9) &mdash; content and formatting may not be reliable*
