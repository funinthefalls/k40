---
layout: post
title: "Originally shared by Jonathan Davis (Leo Lion) I don't if anyone can help me out here but I can't seem to get the darn laser engraving machine working with the included BenBox software!"
date: August 13, 2016 22:37
category: "Discussion"
author: "Jonathan Davis (Leo Lion)"
---
<b>Originally shared by Jonathan Davis (Leo Lion)</b>



I don't if anyone can help me out here but I can't seem to get the darn laser engraving machine working with the included BenBox software! On top of that the stepper motor does not want to for some reason. 

![images/56ed9a35911e1c4ea5d10bf8b056f332.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/56ed9a35911e1c4ea5d10bf8b056f332.gif)



**"Jonathan Davis (Leo Lion)"**

---
---
**greg greene** *August 13, 2016 23:06*

Is this a K40 machine?


---
**Jonathan Davis (Leo Lion)** *August 13, 2016 23:07*

**+greg greene** Elekes Maker 


---
**greg greene** *August 13, 2016 23:14*

Ah, well for our machines it is a requirement that the software know the make and model of the controller board.  To do that we have to enter a number from the board on a screen in our software.  I'm not familiar with your machine - nor your software - but perhaps it is the same requirement.  Does you machine use a USB key that must be  plugged into your computer ? If so - it may require the serial number of the controller board to be input into the software also.  Hope this helps in some way.


---
**Jonathan Davis (Leo Lion)** *August 13, 2016 23:20*

**+greg greene** well the machine did come with BenBox, and you do have to connect it to a computer to control it.

[http://m.gearbest.com/3d-printers-3d-printer-kits/pp_290386.html](http://m.gearbest.com/3d-printers-3d-printer-kits/pp_290386.html)


---
**greg greene** *August 13, 2016 23:28*

About the only thing I can think of is a stepper motor may be wired to the board wrong - it appears the head is travelling in one direction ok - but not on the other axis, if the wiring is ok then either the stepper motor may be mounted incorrectly, or defective.  Check all connections to ensure they are making a good connection, try switching the stepper motors, if the problem follows the motor - it's the motor - if not - it's the controller.  Try to contact the supplier and see if they have any ideas also.


---
**Jonathan Davis (Leo Lion)** *August 13, 2016 23:31*

**+greg greene** alrighty and I know the stepper motors work that I know for sure. 


---
**greg greene** *August 13, 2016 23:32*

Good luck - these machines can drive you to be a good sailor on Capt Morgan's ship !! :)


---
**Jonathan Davis (Leo Lion)** *August 13, 2016 23:35*

**+greg greene** indeed and for what I paid, this thing shouldn't be giving me the problems I've been getting from it. 


---
**Eric Flynn** *August 14, 2016 00:01*

**+Jonathan Davis**  Its really difficult to tell what problem you are having with no description of the issue, and your video doesnt show anything.  For all I can tell, it is working properly in your video.  I haev dealt with several of these type of machines.  If you explain your issue a little better, and explain what is supposed to be going on in the video, I may be able to help.


---
**Jonathan Davis (Leo Lion)** *August 14, 2016 00:05*

**+Eric Flynn** well for starters the x-axis should be moving and like in the video and so far I can only get it to do just lines and not like the actual image. Like is supposed to do!


---
**Eric Flynn** *August 14, 2016 00:06*

Does the X motor make a "grinding" sound?


---
**Jonathan Davis (Leo Lion)** *August 14, 2016 00:07*

**+Eric Flynn** No, it doesn't do that at all since its practically a brand new machine. 


---
**Eric Flynn** *August 14, 2016 00:08*

It has nothing to do with it being a brand new machine.  If steppers are hooked up incorrectly they will not move properly, and make a grinding sound.


---
**Eric Flynn** *August 14, 2016 00:09*

It appears to be moving some in the video.  I suspect the microstepping setting on the controller is set incorrectly


---
**Eric Flynn** *August 14, 2016 00:11*

Take a close up picture of the control board if you can


---
**Jonathan Davis (Leo Lion)** *August 14, 2016 00:12*

**+Eric Flynn** alrighty 


---
**Eric Flynn** *August 14, 2016 00:13*

Also check that the motor pulley on the X motor is tight.  it may just be slipping on the shaft.


---
**Jonathan Davis (Leo Lion)** *August 14, 2016 00:14*

**+Eric Flynn** i made sure of that when I built it. 


---
**Alex Krause** *August 14, 2016 02:02*

Is there a setting in your software that you tell the machine how many steps per revolution?


---
**Jonathan Davis (Leo Lion)** *August 14, 2016 02:09*

**+Alex Krause** yes there is such a option, and currently it's set to 1 step according to the BenBox App. 


---
**Mauro Manco (Exilaus)** *August 14, 2016 08:45*

Feel mounting 20teeth pulley that means want around  80 step for mm whit 1:16 of microstepping...


---
**Jonathan Davis (Leo Lion)** *August 14, 2016 13:45*

**+Mauro Manco** okay then 


---
**Jonathan Davis (Leo Lion)** *August 14, 2016 21:15*

**+greg greene** **+Eric Flynn** **+Alex Krause** **+Mauro Manco** 

[https://goo.gl/photos/D3uTBhL31EPLqLsz6](https://goo.gl/photos/D3uTBhL31EPLqLsz6)

The controller board itself and the software settings in BenBox, along with a video of what its doing currently.


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/GR4rHaVjeae) &mdash; content and formatting may not be reliable*
