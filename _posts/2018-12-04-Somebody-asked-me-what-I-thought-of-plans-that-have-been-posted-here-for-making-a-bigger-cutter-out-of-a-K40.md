---
layout: post
title: "Somebody asked me what I thought of plans that have been posted here for making a bigger cutter out of a K40"
date: December 04, 2018 15:31
category: "Discussion"
author: "Adrian Godwin"
---
Somebody asked me what I thought of plans that have been posted here for making a bigger cutter out of a K40. My feeling is that it's the wrong way around, but maybe I'm looking from the wrong perspective - I do like to tinker with hardware more than actually do production. 



The way I see it is, the K40 has a smart metal box containing a reject tube, an unreliable power supply, a low-performance motion controller, some lossy mirrors in shaky mounts and a few other cheap parts such as fans, pumps and dodgy software.



Now, that might sound bad. Lol. But the truth is, you get all those bits for very little money - it would cost quite a bit more to get even the same parts individually, let alone higher quality ones.  And most of us don't have the experience to choose them and put them together, at least until we've spent a year or so fixing our K40s.



So it's not a bad deal, if you think of it as a starter kit. Then, as you gain experience, you improve the ventilation and cooling because they're outright insane, the tube runs out of life and you change it for one that really is 40W,  the mirrors get scratched and you replace them, the power supply dies and you replace it with one that's a bit better made, you get impatient with the motion controller and want to add one that also controls tube power or works with better software and, finally, you want more cutting area so you put some new slides in a bigger box.



To me, this makes the K40 worthwhile. It allows a cheap start and incremental improvement at the cost of endless tinkering. Other possibilities are buying a high spec machine to start with (great if it's earning money for you) or putting all the K40 bits in a bigger box (OK, but it seems to me you're throwing away the only bit that's any good and living longer with the poor stuff).



I totally recognise that this isn't everyone's view and people have different priorities (mine is to learn, not to make a profit) so feel free to tell me I'm wrong. I'm just interested to hear another side.

 





**"Adrian Godwin"**

---
---
**HalfNormal** *December 04, 2018 15:45*

You are not totally wrong.

IF you are a tinker/maker/handy then the K40 is a great first laser to see if you really want to invest in a better larger system.

Starter systems from more reputable companies start at around $3500.00 to $5000.00 and you still have poor customer service. Just check out the forums on them.

I purchased a larger system for $1850.00 that I have not had to mess with except poor power supply that has been replaced twice but the ebay seller of the replacement power supply stepped up and replaced the faulty one.

IF your time is money, K40 is not for you. (or any cheap Chinese laser)

IF you expect customer service, K40 is not for you.

IF you plan to build a business around your laser, K40 is not for you.

IF you enjoy a challenge, K40 IS right for you!

Bottom line, your call.


---
*Imported from [Google+](https://plus.google.com/100361775132886183172/posts/5hBjQAxGJpu) &mdash; content and formatting may not be reliable*
