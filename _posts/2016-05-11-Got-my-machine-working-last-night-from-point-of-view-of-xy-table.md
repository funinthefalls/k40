---
layout: post
title: "Got my machine working last night from point of view of xy table"
date: May 11, 2016 18:26
category: "Discussion"
author: "Ian Ferguson"
---
Got my machine working last night from point of view of xy table. Haven't fired up the laser yet but all looks intact. I was wondering with Corel draw and laser draw plugin how do I change the speed of laser motion when selecting cut? Only seems to go at one speed. Would appreciate any help. Is it that I should select and use engrave only and fill the pattern in order to achieve the cut. Any screen grabs would be a great help thanks.





**"Ian Ferguson"**

---
---
**Alex Krause** *May 11, 2016 19:16*

Is it moving really slow?


---
**Ian Ferguson** *May 11, 2016 19:17*

Yeah but just in cut Alex have had it moving fast when using a template from laser draw.


---
**Alex Krause** *May 11, 2016 20:20*

Make sure in Corel laser that you pick your board type in the options and serial number off the board is entered. Mine was acting goofy till I did that 


---
**Ian Ferguson** *May 12, 2016 04:39*

Yeah thanks Alex was pointed to a you tube video on FB that cleared up the issue exactly as u say. I defo had the correct m2 board ref in my software but hey presto M1 works now, how weird is that. Thanks for your help.


---
*Imported from [Google+](https://plus.google.com/100713211036434462043/posts/QP3RhRiErSn) &mdash; content and formatting may not be reliable*
