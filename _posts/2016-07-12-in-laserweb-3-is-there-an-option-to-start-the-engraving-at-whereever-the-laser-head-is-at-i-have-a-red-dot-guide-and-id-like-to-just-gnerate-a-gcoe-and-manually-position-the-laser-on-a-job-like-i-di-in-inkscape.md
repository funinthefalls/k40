---
layout: post
title: "in laserweb 3 is there an option to start the engraving at whereever the laser head is at i have a red dot guide and id like to just gnerate a gcoe and manually position the laser on a job like i di in inkscape"
date: July 12, 2016 01:15
category: "Discussion"
author: "Derek Schuetz"
---
in laserweb 3 is there an option to start the engraving at whereever the laser head is at i have a red dot guide and id like to just gnerate a gcoe and manually position the laser on a job like i di in inkscape





**"Derek Schuetz"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 12, 2016 01:45*

I think it's just a matter of your start gcode. Don't put homing command in. But you'd need to make sure the machine thinks it's at 0,0 so you could at G92 X0 Y0 as your start gcode. 


---
**Derek Schuetz** *July 12, 2016 01:46*

Ah ok that's the missing line I need thank you


---
**Ariel Yahni (UniKpty)** *July 12, 2016 01:54*

Or move to the location you need and then press set zero icon, that will make that point 0,0


---
**Ray Kholodovsky (Cohesion3D)** *July 12, 2016 01:55*

**+Ariel Yahni** cool! I'll do that. 


---
**Steve Anken** *July 12, 2016 09:15*

I have not even been using the end endstop/homing yet and just set zero like I do in Chilipeppr. I have a Machine zero for startup homing my cnc ut after that it is all offset to workspace zeros. I can have several workspaces and it's easy hand code the Gcode wrappers that offset the zero to make multiples of the same gcode file for simple jobs.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/7VYDbyRFJcr) &mdash; content and formatting may not be reliable*
