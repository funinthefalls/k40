---
layout: post
title: "I often read about people setting the power to 12% or 15%, my K40 has a potentiometer that can be adjusted and the power option in corel laser is always greyed out"
date: April 05, 2016 15:17
category: "Hardware and Laser settings"
author: "Darren Steele"
---
I often read about people setting the power to 12% or 15%, my K40 has a potentiometer that can be adjusted and the power option in corel laser is always greyed out.  Am I missing something or have other people got slightly different or modded machines that allow them to set the power using Corel Laser?





**"Darren Steele"**

---
---
**Thor Johnson** *April 05, 2016 15:30*

Not the K40-stock.  Mine has a digital power thingy (0-100%), but it still has to be set by hand per-cut.



Both the Smoothie and  RAMPS (and the DSP controllers) can change power during the cut, but the tools used are different than CorelLaser or LaserDraw.


---
**Stephane Buisson** *April 05, 2016 16:43*

some machine have potentiometer and vue meter, other have digital display in % setting. it does the same by limiting the max current (proportional to the 5V in).

PWM control is another %, not the same if you keep the pot. as ceiling. 

Ex: if you set the pot @ 80% (5Vx80%=4V as ceiling) and 75% PWM, this would be equivalent of 3V.

you fix a ceiling to protect the tube life expectancy. one is set by soft, the other one is manually set.

When new,  for the max current recommended for the tube, V was about 3.6V (can't remember exactly).


---
*Imported from [Google+](https://plus.google.com/+DarrenSteele/posts/UjeoGxXHHTa) &mdash; content and formatting may not be reliable*
