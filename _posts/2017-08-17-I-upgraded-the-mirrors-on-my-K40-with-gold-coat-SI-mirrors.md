---
layout: post
title: "I upgraded the mirrors on my K40 with gold coat SI mirrors"
date: August 17, 2017 14:59
category: "Discussion"
author: "bbowley2"
---
I upgraded the mirrors on my K40 with gold coat SI mirrors.  Got the mirrors allined following instructions but the laser seems to be outputting about 50% of the power as before.  Any ideas?

 





**"bbowley2"**

---
---
**Ray Rivera** *August 17, 2017 15:47*

If you changed your lens too, ensure it is curved side up. 


---
**bbowley2** *August 17, 2017 18:58*

Has to be. One side is gold .




---
**Chris Hurley** *August 17, 2017 19:59*

Lens not mirrors. (Curved side up) have you checked it at each mirror and the head for allinement? This is usually it 95% of the time. 


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/69FMxYMzypx) &mdash; content and formatting may not be reliable*
