---
layout: post
title: "After I got my laser cutter smoothified (thanks everyone for all your help), I have not even had a chance to use it more than once or twice"
date: December 04, 2016 22:36
category: "Discussion"
author: "Mark Leino"
---
After I got my laser cutter smoothified (thanks everyone for all your help), I have not even had a chance to use it more than once or twice. Part of the reason for this is that I don't have a good workstation for the laser to be. I'm planning on doing putting a vent outside for it, since I have just been using my dryer duct for exhaust. Has anyone come up with a table/computer/monitor setup to go with the k40?



On another note, I picked up a couple raspberry pi 3's, these things are a night and day difference compared to the old B+. I tried getting the laserweb3 program to download, but it had a lot of errors. Node.js worked just fine. Any one else using a pi? Is a pi3 capable of running laserweb alone? Or would it just be the hub between computer and laser?





**"Mark Leino"**

---
---
**Jonathan Davis (Leo Lion)** *December 04, 2016 23:32*

I have a Pi 3 coming from [amazon.com](http://amazon.com) via the Uk, so I guess the fact that it has been upgraded it would probably work. But I can not say for certain until I test out their install method for it.


---
**Ned Hill** *December 05, 2016 01:16*

You might want to cross post this to the Laserweb G+ group for the RP3.  


---
**Kelly S** *December 05, 2016 01:36*

For the longest I used a laptop next to it with the exhaust ran to my office.  Recently upgraded to smoothie  (by c3d) and will be trying a LAN connection, but I have a long usb to my desktop.


---
**Jonathan Davis (Leo Lion)** *December 05, 2016 01:44*

**+Kelly S** more like wifi actually yes?


---
**Jonathan Davis (Leo Lion)** *December 05, 2016 03:21*

**+Peter van der Walt** ok and its not like a version of laserweb be made that does work as the instructions to install it are on the website.


---
**Jonathan Davis (Leo Lion)** *December 05, 2016 03:25*

**+Peter van der Walt** alrighty and i will keep that in mind but for now it will be used in an arcade cabinet.


---
**Jonathan Davis (Leo Lion)** *December 05, 2016 03:28*

**+Peter van der Walt** That would be a interesting idea as you can get arcade buttons in a complete set on amazon for $64 and free shipping.


---
**Jonathan Davis (Leo Lion)** *December 05, 2016 03:30*

Plus it could make for a fun interactive art exhibit. Where the user would create the art by controlling where the laser head moves.


---
**Jonathan Davis (Leo Lion)** *December 05, 2016 03:31*

**+Peter van der Walt** Indeed and the button set does come with a usb controller hub that they plug into.


---
**Jonathan Davis (Leo Lion)** *December 05, 2016 03:35*

**+Peter van der Walt** Nice.


---
**Jonathan Davis (Leo Lion)** *December 05, 2016 03:37*

**+Peter van der Walt** Nice and clean edges. lucky but a CNC mill is something that yet i can not afford.


---
**Kelly S** *December 05, 2016 05:59*

**+Jonathan Davis** I do not have a WiFi chip for my c3d board, but I do have a lan card for it.  Will be looking I to tomorrow evening.


---
**Kelly S** *December 05, 2016 06:00*

**+Peter van der Walt** That's an awesome game pad.   :D


---
**HalfNormal** *December 05, 2016 12:40*

**+Peter van der Walt** "Cut out on me Ox CNC from CNCWeb" Nice! :-)


---
**Vince Lee** *December 06, 2016 01:25*

To get it out of the way, I built mine into a coffee table (or rather built a coffee table to house it) and then later made hollow ceiling beams to carry the exhaust.

![images/85e6d79707de3139afd16bedfc039743.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/85e6d79707de3139afd16bedfc039743.jpeg)


---
**Kelly S** *December 06, 2016 02:20*

**+Vince Lee** That's a pretty snazzy laser box.


---
*Imported from [Google+](https://plus.google.com/+MarkLeino/posts/Lcj8GDBaFbN) &mdash; content and formatting may not be reliable*
