---
layout: post
title: "Found a site to generate images with different fonts has a ton of different options from bands to popular TV shows and much more"
date: May 23, 2016 03:31
category: "External links&#x3a; Blog, forum, etc"
author: "Alex Krause"
---
Found a site to generate images with different fonts [www.fontmeme.com](http://www.fontmeme.com) has a ton of different options from bands to popular TV shows and much more





**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 03:57*

Another cool one to add would be an ambigram generator. They are cool. Although searching around I am having difficulty locating one that is active.


---
**Alex Krause** *May 23, 2016 04:08*

**+Yuusuf Sallahuddin**​ What is an ambiagram ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 04:26*

**+Alex Krause**  There are a few versions, but basically it is a word that when viewed in a different orientation either says the same thing, or a different word. An example of a natural ambigram is the word MOW. When rotated 180 degrees, it still says MOW. Other versions are flipped/mirrored. e.g. MOM, when flipped reads WOW. So, ambigram generators (which used to be really easy to find) can take some text that you choose & create an ambigram from it. I don't like the flipped/mirrored ones, as they are too simple. I like the 180 degree ones as that's pretty nifty in my opinion.


---
**Thor Johnson** *May 23, 2016 15:02*

**+Alex Krause** **+Yuusuf Sallahuddin** 

I found this howto; I was trying to find the Princess Bride for Alex, as it's one of my favorite ambrigams, and this has a picture of it: [https://designshack.net/articles/graphics/how-to-design-an-ambigram/](https://designshack.net/articles/graphics/how-to-design-an-ambigram/)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 15:52*

**+Thor Johnson** Yeah that's perfect example of it. That princess bride one is an interesting play, adding the mirrored but slightly different image. I like it.



That's actually a pretty decent tutorial too. Gives a good overview of how-to manually create your ambigram. Also, I just clicked the Flipscript.com link that is in their page & it works today. I clicked it many times yesterday (while searching for generator) & it said the site was down. Must've just been temporary maintenance.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/HsndgvVmpKP) &mdash; content and formatting may not be reliable*
