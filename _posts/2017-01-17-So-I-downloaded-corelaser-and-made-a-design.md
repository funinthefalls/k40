---
layout: post
title: "So I downloaded corelaser and made a design"
date: January 17, 2017 17:34
category: "Discussion"
author: "Matthew Wilson"
---
So I downloaded corelaser and made a design. Once I pressed on engrave, it opened up laserdraw's engraving page and I started the engraver. I have tried 4 or 5 times and every time the laser head moves extremely slow (guessing 5mm sec). I even set the speed to 200mm second and it still goes the same speed. Is there a step I'm missing such as a laser speed inside of corelaser itself? 



Thanks for the help in advance. I am stuck with this platform until I can afford to add more money into this machine, so upgrading is not an option just yet.





**"Matthew Wilson"**

---
---
**Joe Alexander** *January 17, 2017 17:50*

I would check to make sure LaserDRW has the right mainboard and serial put in for your controller. Easy step to skip :P


---
**Ned Hill** *January 17, 2017 18:13*

Like **+Joe**, said make sure your mainboard is set for M2 and the Device ID has the number from your control board.

![images/2bab49d0145436f5319507011f892d6c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2bab49d0145436f5319507011f892d6c.png)


---
**Matthew Wilson** *January 17, 2017 21:44*

Awesome thanks. So obviously the sn will be different, but my origin-x is 1.000 and y is 1.000. Also, the PageSizeX and Y are 400.000. Is that okay or does it need to be what yours is?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 17, 2017 23:41*

Origin X & Y are just the starting points for the laser. Set a 1.000 is fine, however you are losing 1mm of overall workspace. PageSizeX & Y are the size of your workspace. Set at 400.000 is again fine, however if you design something bigger than the physical area, you will find the laser carriage tries to move further than it physically can, causing possible extra wear on the teeth of the belt which will then eventually cause missed steps when moving, resulting in failed cut/engrave jobs.


---
*Imported from [Google+](https://plus.google.com/101681639472204631291/posts/g5EmFVXJHHt) &mdash; content and formatting may not be reliable*
