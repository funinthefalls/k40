---
layout: post
title: "I am in desperate need of help!"
date: March 24, 2018 00:59
category: "Software"
author: "NATALIE BRIDGE"
---
I am in desperate need of help!



What software is everyone running?



Corel laser will not work on my computer as its windows 10 - i have no idea how to get around this - i have a k40 laser cutter just sitting here because i have no software!





**"NATALIE BRIDGE"**

---
---
**James Rivera** *March 24, 2018 01:39*

Try this. It works great for me on my [K40.scorchworks.com](http://K40.scorchworks.com) - K40 Whisperer


---
**Duncan Caine** *March 24, 2018 10:00*

Yep, agree with James, Inkscape to design SVG files and K40 Whisperer to run them.


---
**Stephane Buisson** *March 25, 2018 08:41*

**+NATALIE BRIDGE**,  I create this community with the goal to open the K40 to open source softwares. at first, it was needed to replace the electronic board, we go for smoothie board for it's modern/powerful capabilities. then **+Ray Kholodovsky** develop a Smoothie compatible board, it's a drop in replacement modification.

this open the door for open source software like the old but excelent Visicut, Laserweb with a great community. and also with a licence Lightburn.

and Lately, with reverse engineering, came  K40 Wisperer on unmofied K40. 

Now we can say, we do have a usable machine ;-))


---
*Imported from [Google+](https://plus.google.com/116622498712156797309/posts/TQ5MQmYR7LT) &mdash; content and formatting may not be reliable*
