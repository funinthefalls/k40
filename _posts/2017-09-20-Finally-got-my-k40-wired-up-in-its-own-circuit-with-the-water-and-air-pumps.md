---
layout: post
title: "Finally got my k40 wired up in its own circuit with the water and air pumps"
date: September 20, 2017 13:29
category: "Discussion"
author: "Jake Scott"
---
Finally got my k40 wired up in its own circuit with the water and air pumps. Turned it on to test it out and the endstop for the Y axis(vertical) is fine  but the x axis(horizontal) doesn't seem to be reacting and it bottoms out and the stepper keeps trying to move and the belt starts slipping. Took all the connections on the gantry board off and cleaned them and reattached them. did the same for the board connections. Is a circuit burned out on a board somewhere? I'm pulling my hair out trying to figure this out.





**"Jake Scott"**

---
---
**Don Kleinschnitz Jr.** *September 20, 2017 13:49*

Your X endstop may be damaged. Often the optical sensor gets impacted by the gantry's interposer and damages the sensor.



Check that the white ribbon is plugged in correctly and seated in its connector. 



You can measure the output of the sensor at the controller if you are ambidextrous, can read a schematic and use a DVM. There is a schematic in the link below.



You may have to pull off the gantry and get to the X daughter-card to see if you can detect any damage.



This info may be helpful:



[donsthings.blogspot.com - Repairing K40 Optical Endstops](http://donsthings.blogspot.com/2017/08/repairing-k40-optical-endstops.html)


---
**Jake Scott** *September 20, 2017 14:29*

**+Don Kleinschnitz** I already had the gantry loosened up to take my exhaust duct out to trim it back so I just took the whole damn thing out. Way easier to see the x board and guess what? there were some burrs that were keeping the laser head from moving all the way left for the plate to trip the optical sensor. A little filing and bam, it now can get to the optical sensor. Thanks for the link and the help. I never would have even guessed at what the solution was without that link.

[photos.google.com - k40 laser](https://photos.app.goo.gl/pbX6kM0lUy7dNPPb2)


---
**Joe Alexander** *September 20, 2017 14:45*

usually the x limit switch is a common mechanical one with an arm right? grab a multimeter and set it to continuity test(A.K.A. the beeper) and test your wire ends at the controller so confirm the signal is getting there. if not then check wire for nicks, breaks, etc. that easiest to do and diagnoses a lot.


---
**Jake Scott** *September 20, 2017 15:16*

**+Joe Alexander** In the pictures I posted you can see the little metal tab under the laser table and the black end stop sensor. I'll check for continuity too while i'm at it.


---
**BEN 3D** *September 22, 2017 09:11*

**+Don Kleinschnitz** Thanks, you solved my next Issue. 



I never had seen this kind of optical Endstops before, and I was searching Endstops like them from my 3D printer and I was wonding about that I could´nt find some. Now after I read your link above, I find them, and I see that Y Axis endstops metal does not fit into the Optical sensor and hit to the surface instead. I bent the metal part and now it fits. So thanks to Don a lot., i wish I could shake hands with you ;-D



Cheers Ben


---
**Don Kleinschnitz Jr.** *September 22, 2017 14:26*

**+BEN 3D** Glad that fixed it, consider my hand as virtually "shak'ed" :)!


---
*Imported from [Google+](https://plus.google.com/103443251320728007943/posts/7CUa6shajHa) &mdash; content and formatting may not be reliable*
