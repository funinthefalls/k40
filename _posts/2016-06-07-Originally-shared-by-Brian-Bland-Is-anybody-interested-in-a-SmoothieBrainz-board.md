---
layout: post
title: "Originally shared by Brian Bland Is anybody interested in a SmoothieBrainz board?"
date: June 07, 2016 11:59
category: "Discussion"
author: "Brian Bland"
---
<b>Originally shared by Brian Bland</b>



Is anybody interested in a SmoothieBrainz board?   I have two available.   $95 shipped in US without ESP8266 and $100 shipped in US for one with ESP8266.   I am needing to fund a replacement tube in my K40 to continue testing LaserWeb. 

![images/90a69e1fe960c51c63ca52da505e90c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90a69e1fe960c51c63ca52da505e90c1.jpeg)



**"Brian Bland"**

---


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/ehpafjStB15) &mdash; content and formatting may not be reliable*
