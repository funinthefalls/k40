---
layout: post
title: "Yes it's out of focus yes the mirrors need to be aligned and the power and speed need adjusting along with the image is mirrored for some reason but it's my first burn!"
date: August 09, 2017 04:50
category: "Object produced with laser"
author: "William Kearns"
---
Yes it's out of focus yes the mirrors need to be aligned and the power and speed need adjusting along with the image is mirrored for some reason but it's my first burn! 

![images/c94020ff2a3fec3597147b9f2848bc25.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c94020ff2a3fec3597147b9f2848bc25.jpeg)



**"William Kearns"**

---
---
**Ned Hill** *August 09, 2017 19:02*

Congrats! :)  Assuming you are using stock software, make sure "mirror" is unchecked in the engrave manager  panel.


---
**William Kearns** *August 09, 2017 19:09*

**+Ned Hill** I am using illustrator to make the svg and laserweb for to process and send to the laser ah and the board is cohesion mini


---
**Mike R** *August 10, 2017 05:54*

You may have the X or y axis connector upside down, if it's okay in the software.



Does it jog  in the correct direction in laserweb?


---
**William Kearns** *August 10, 2017 07:10*

After I turned them upside down they did but I have to move all my files to the negative y in order for them to print with a positive image


---
**William Kearns** *August 10, 2017 18:37*

Much better![images/db793aa23a4738de68dc09e6a249fd37.png](https://gitlab.com/funinthefalls/k40/raw/master/images/db793aa23a4738de68dc09e6a249fd37.png)


---
**BEN 3D** *August 10, 2017 19:56*

Yeah cool ! You did it! Grüße aus Deutschland :-)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/3DobLcKqnx7) &mdash; content and formatting may not be reliable*
