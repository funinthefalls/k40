---
layout: post
title: "My K40 arrived today"
date: July 21, 2015 01:26
category: "Discussion"
author: "Gee Willikers"
---
My K40 arrived today. 



[https://plus.google.com/+JeffGolenia/posts/cahMnSbTeLn](https://plus.google.com/+JeffGolenia/posts/cahMnSbTeLn)





**"Gee Willikers"**

---
---
**Stephane Buisson** *July 21, 2015 08:44*

Welcome into our community !



do you mind to share the references on laser power supply and controler board ? (just to see any evolution)


---
**Gee Willikers** *July 22, 2015 00:25*

**+Stephane Buisson** Hello and thank you. Do you mean the markings on the boards such as the revision numbers? I can do that. I wanted to inspect the laser power supply board so I will have it open anyway.


---
**Stephane Buisson** *July 23, 2015 13:09*

Yes, Moshi board revision for the electronic board, or inscription on power supply like on my post (11/11/2014).



It will be useful to see any evolution in the K40 production.


---
**Gee Willikers** *July 23, 2015 17:45*

Will do. 


---
**Haotian Laser** *August 07, 2015 09:08*

hi, very glad to know that you are interested in K40 machine. Please feel free to contact me if any question. We have 10 years skilled engineer 24 hours online, who can speak very good english.


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/8jkyw4zfRV3) &mdash; content and formatting may not be reliable*
