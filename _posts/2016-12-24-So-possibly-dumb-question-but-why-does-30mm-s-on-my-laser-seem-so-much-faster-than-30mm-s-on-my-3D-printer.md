---
layout: post
title: "So, possibly dumb question, but why does 30mm/s on my laser seem so much faster than 30mm/s on my 3D printer?"
date: December 24, 2016 18:34
category: "Discussion"
author: "K"
---
So, possibly dumb question, but why does 30mm/s on my laser seem so much faster than 30mm/s on my 3D printer? Maybe it's not, and I'm just perceiving it that way, but it feels like the laser moves much faster.





**"K"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2016 18:42*

Acceleration values would be the first thing I compare. 


---
**K** *December 24, 2016 20:00*

Ok, I looked at my acceleration values, and they're actually both set the same (2000mm/s on printer and laser.) It must be just that it seems different rather than it actually being different.


---
**Rob Morgan** *December 24, 2016 23:07*

Perhaps there is a feed rate on the printer?


---
**Phillip Conroy** *December 25, 2016 00:16*

Time a 300 mm cut on laser cutter and then ÷ 10


---
**Rob Morgan** *December 31, 2016 02:27*

must be  a max setting on the printed that prevents it from eating itself.


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/3fCvbJMPtQ8) &mdash; content and formatting may not be reliable*
