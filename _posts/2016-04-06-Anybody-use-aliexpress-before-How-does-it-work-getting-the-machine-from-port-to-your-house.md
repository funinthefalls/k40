---
layout: post
title: "Anybody use aliexpress before? How does it work getting the machine from port to your house?"
date: April 06, 2016 15:15
category: "Discussion"
author: "3D Laser"
---
Anybody use aliexpress before?  How does it work getting the machine from port to your house?  And how does customs work?





**"3D Laser"**

---
---
**Loïc Verseau** *April 06, 2016 15:31*

Make the transaction on aliexpress even if the seller ask for paypal. Paypal will not refound. (happened to me), and even if one day they will, you have to pay for the shipping back. (half the price of the lasercutter). :)


---
**3D Laser** *April 06, 2016 15:36*

How does it work with customs and getting it from the port to your house 


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2016 15:48*

Defex? They're not that bad!


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2016 15:51*

Tea and smoothies (both kinds) for me. 


---
**Stephane Buisson** *April 06, 2016 15:52*

Ali express trick : ( I learn at my expenses), a seller will told you, he have to change delivery method and offer you a partial refund, to have your refund you have to open a dispute. then they process the partial refund. then no delivery. you can't open a second dispute. end of it.

if you claim, Aliexpress say it's too late because dispute was refunded, they did pay the difference to the seller. buyer protection scheme will not apply.


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2016 15:53*

**+Stephane Buisson** you bought the $70 3D printer Ponzi scheme?


---
**Stephane Buisson** *April 06, 2016 15:57*

a side from that issue, I got 150$ free coupon, and the next 3 parcels arrived promptly


---
**3D Laser** *April 06, 2016 15:58*

**+Peter van der Walt**(null) I was looking at some of the 50-60w machines they have up there for less than 1000 but the shipping is a little vague and I don't know what I will have to pay in customs and even though it says free shipping it says buyer picks shipping 


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2016 16:03*

Personally for machines I prefer domestic/ more local shipping which is more prevalent on eBay. I bought a 3040 Cnc frame that is coming to me from California today. Plenty of U.S. warehouses  selling K40's as well. Same may be true for other countries. 

Regardless, PayPal won't do anything. The dispute immediately goes on my credit card. 


---
**3D Laser** *April 06, 2016 16:05*

[http://m.aliexpress.com/item/32549590534.html](http://m.aliexpress.com/item/32549590534.html) this is one of the ones I was looking at more than likely I will go with eBay but I was looking at other options 


---
**L Vers** *April 06, 2016 16:05*

False about the aliexpress ban. They told me "you make a deal out of Ali, it does not concern us. Thank's". Corey, if you need a 60 w ask for a certified lasertube, also ask the length of it. And check if you can find the same one in an other Shop, with the same power/length etc etc.. Lot of sellers lies about the power.


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2016 16:05*

That looks big. Shipping is only $140? 


---
**3D Laser** *April 06, 2016 16:08*

Yes and it says buyer picks shipping which threw me off 


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2016 16:20*

It's a bs figure. You'll end up dealing with freight or at least several hundreds of dollars for cheapest method. 



Could always message the seller and ask how shipping works. 


---
**HP Persson** *April 06, 2016 20:21*

I buy alot from Aliexpress, when i use DHL i get the delivery to my door.

My k40 was ordered from Aliexpress and it worked perfect! 

But check with the seller first, it may differ in your country.


---
**MNO** *April 07, 2016 00:47*

i also buy a lot there but mostly small things. had few disputes. Newer won. It is not possible to win... don't buy things if more than 25$.

Rattings there not say nothing. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/i9P4KbSaBPV) &mdash; content and formatting may not be reliable*
