---
layout: post
title: "Got this email today. Laserdepot sale"
date: March 30, 2017 19:56
category: "Material suppliers"
author: "Ariel Yahni (UniKpty)"
---
Got this email today. Laserdepot sale 

![images/a6bef1f375a15d553838f93abcc73b7e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a6bef1f375a15d553838f93abcc73b7e.png)



**"Ariel Yahni (UniKpty)"**

---
---
**Cesar Tolentino** *March 30, 2017 21:00*

Where or what is the address of their store in Las Vegas


---
**Ariel Yahni (UniKpty)** *March 30, 2017 21:33*

10300 W Charleston Blvd #13-379

Las Vegas, Nevada 89144


---
**Cesar Tolentino** *March 30, 2017 21:34*

thank you sir.






---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/cuVUWVhX5Vn) &mdash; content and formatting may not be reliable*
