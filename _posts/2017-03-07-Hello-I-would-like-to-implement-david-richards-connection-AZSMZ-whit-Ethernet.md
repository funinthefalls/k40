---
layout: post
title: "Hello , I would like to implement david richards connection AZSMZ whit Ethernet"
date: March 07, 2017 12:47
category: "Smoothieboard Modification"
author: "Massimo Mezzetti"
---
Hello , I would like to implement david richards connection AZSMZ whit Ethernet.

Has someone the correct cabling diagnam?

Thank you very much

Massimo





**"Massimo Mezzetti"**

---
---
**Stephane Buisson** *March 07, 2017 13:01*

We do not recommend  AZSMZ board as they don't play nicely with open source. not saying it's imposible to make it work, but barely any support from them. you would have more support from smoothieware or C3D and it's really worth the very small price difference.

Maybe a bit late for you if you own one, but for other readers.

**+Arthur Wolf** **+Ray Kholodovsky**




---
**Arthur Wolf** *March 07, 2017 13:05*

Besides not respecting the work of hundreds of volunteers, or the license of the code they make their money off of, AZSMZ is also widely known to be a catastrophically bad design ( the most obvious example is that the USB traces go under the stepper drivers, causing disconnection problems for most users ).

I don't believe there is any possible way to add Ethernet to an AZSMZ


---
**Massimo Mezzetti** *March 07, 2017 13:05*

You are right but I have it already. and it workswith smoothie.. , if someone has capbing for LAN8720 with this AZSMZ ...


---
*Imported from [Google+](https://plus.google.com/107582591063734519545/posts/TaokK4REtzc) &mdash; content and formatting may not be reliable*
