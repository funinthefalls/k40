---
layout: post
title: "Good afternoon I done upgradeof my firmware-cnc.bin for latest version Now i cant force machine to reference to bottom left corner where i have endstops Every time it send head to center of table end setup 0 there Is there"
date: February 16, 2017 12:57
category: "Smoothieboard Modification"
author: "Damian Trejtowicz"
---
Good afternoon

I done upgradeof my firmware-cnc.bin for latest version

Now i cant force machine to reference to bottom left corner where i have endstops

Every time it send head to center of table end setup 0 there

Is there any changes in configuration.txt?

When i use previous firmware(i was using one from november) all is fine





**"Damian Trejtowicz"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 16, 2017 13:52*

G28.2 should be the homing command once you switch to cnc firmware 


---
**Damian Trejtowicz** *February 16, 2017 13:59*

Its not this,when i try g28.2 its freeze board now


---
**Ray Kholodovsky (Cohesion3D)** *February 16, 2017 14:24*

G28.2 with a capital? 

And if that doesn't work you've got a problem somewhere else 


---
**Damian Trejtowicz** *February 16, 2017 14:35*

Yes with capitals,on old firmware-cnc.bin all work fine


---
**Ray Kholodovsky (Cohesion3D)** *February 16, 2017 14:38*

Advise you grab a new stock config file. Back up the current one obviously. Start from scratch and rebuild your settings manually. 


---
**Wolfmanjm** *February 17, 2017 06:38*

actually homing in the CNC build should be $H as in grbl, don't use G28.2


---
**Damian Trejtowicz** *February 18, 2017 13:22*

Sort it out my homing,now work fine .

Just need to chec why i have warning on Lw3


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/3u9TAMCzR9V) &mdash; content and formatting may not be reliable*
