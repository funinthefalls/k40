---
layout: post
title: "I need help. I have a 40w laser engraver but it moves very slowly on the x axis, only on cutting and only going to the start point"
date: August 09, 2015 18:39
category: "Discussion"
author: "Clifford Livesey"
---
I need help. I have a 40w laser engraver but it moves very slowly on the x axis, only on cutting and only going to the start point. Everything else is ok. Thanks 





**"Clifford Livesey"**

---
---
**Clifford Livesey** *August 11, 2015 05:36*

Yes. It only moves slow when going to the starting point for cutting something. And only on the x axis 


---
**Joey Fitzpatrick** *August 11, 2015 13:31*

make sure you have the correct main board selected in coreldraw settings. The machine will move very slowly if you have the wrong board selected. 


---
**Gregory J Smith** *August 16, 2015 00:46*

Mine does the same. X moves at the cutting speed to the start point. If I spoke Chinese I'd register a software improvement request,


---
*Imported from [Google+](https://plus.google.com/108754141667644316151/posts/RXdhcso57aE) &mdash; content and formatting may not be reliable*
