---
layout: post
title: "I stumbled upon what looks like a good deal for EU - 415 S&H included for a K40"
date: February 08, 2016 16:00
category: "External links&#x3a; Blog, forum, etc"
author: "Jeremie Francois"
---
I stumbled upon what looks like a good deal for EU - 415€ S&H included for a K40. Most probably with no import taxes -- since UK is a trojan country for EU ;) If I buy this I can call my home a fablab ;) [Warning though: the crappy Moshi default configuration is better replaced with a more usable controlling hardware and software]





**"Jeremie Francois"**

---
---
**Stephane Buisson** *February 08, 2016 16:12*

yep they are back on ebay 334 GBP UK delivred


---
**Jeremie Francois** *February 08, 2016 18:21*

It runs moshidraw though. But there is a rising new and open source laser controller nowadays ;)


---
**Brooke Hedrick** *February 08, 2016 19:02*

Looks identical to my eBay one.  It came with MoshiDraw.  I replaced with LightObject controller guts.


---
**Kelly Burns** *February 10, 2016 22:01*

I can tell you that it looks like mine and if I had a chance to do over, I would go to next and get something with better controller. If it does have a Moshi controller, I would avoid it. Moshi and MoshiDraw/Laser software are total Crap.  Barely usable IMO.


---
**I Laser** *February 12, 2016 01:40*

I'll post on every buy thread.



DON'T BUY A MOSHI BASED K40!!!



If you want something usable buy a corellaser board...


---
**Jeremie Francois** *February 12, 2016 07:26*

**+I Laser** good advice, now I may buy a moshi because I know how to discard all the controller & software. I even have many unused controllers at home. And, there is no such thing as a native LaserWeb setup from China, yet ;) Now I agree it is something that every buyer shall know. I don't get it why they keep on shipping this limited configuration when it costs close to nothing to use something free & more powerful :/


---
**Stephane Buisson** *February 12, 2016 09:38*

**+I Laser** if a moshi machine is cheaper, they are still welcome for a mod. project ;-))


---
**I Laser** *February 12, 2016 10:25*

By all means if you can find one cheaper and like to tinker then go for it. :)



Just trying to save the frustration for those that aren't sure what they're looking for and end up with a moshi machine lol!


---
**Jeremie Francois** *February 12, 2016 12:02*

"This item is out of stock" All the 31 were gone fast, wow.


---
**Stephane Buisson** *February 12, 2016 12:41*

**+Jeremie Francois** with the software issue solved, it make useful and popular ;-)), community support help too.



K40 is the cheapest (will be back soon I 'm sure), but if budget allow look those 2 other models (more power and larger bedsize) it's the same problematic.



[http://www.ebay.co.uk/itm/50W-USB-Laser-Engraver-Engraving-Cutter-Cutting-Machine-Hermetic-CO2-Glass-Tube-/171973407531?hash=item280a69f32b:g:VpUAAOSwYHxWJSGc](http://www.ebay.co.uk/itm/50W-USB-Laser-Engraver-Engraving-Cutter-Cutting-Machine-Hermetic-CO2-Glass-Tube-/171973407531?hash=item280a69f32b:g:VpUAAOSwYHxWJSGc)



[http://www.ebay.co.uk/itm/60W-CO2-USB-LASER-ENGRAVING-CUTTING-MACHINE-ENGRAVER-CUTTER-WOODWORKING-CRAFTS-/252255509208?hash=item3abb999ad8:g:LLYAAOSwL7VWnHob](http://www.ebay.co.uk/itm/60W-CO2-USB-LASER-ENGRAVING-CUTTING-MACHINE-ENGRAVER-CUTTER-WOODWORKING-CRAFTS-/252255509208?hash=item3abb999ad8:g:LLYAAOSwL7VWnHob)


---
**Jeremie Francois** *February 12, 2016 12:58*

**+Stephane Buisson** my strategy is usually to eye for long on something I want, and eventually to get a more powerful one. Now, even though it would better go to the garage (just b/c of the fumes), with 3 printers, a 4060  CNC and numerous drawers everywhere I am running out of space at home, so bigger is no good -- and I do not need to cut larger 5mm plywood panels for more and bigger 3D printers, nooo ;)

Also, these are nice looking machines (seem more serious indeed), but a bit pricey for me. I must admit I don't have a real need for a laser cutter.

Now, give a man a hammer... I certainly can think of a lot of uses of course... ;)


---
**Kelly Burns** *February 12, 2016 14:23*

Jeremie, I have no issue with the K40.  I am very much a DIY person.  Have built CNCs and other devices.  I have a great deal of experience with both excellent and Bad graphic design software.  I too thought, I will just get the cheap machine and tolerate it for a while and upgrade controller at shortly down the road.  The Moshi stuff is borderline "unusable".  At best, it works, but is totally unreliable.   **+Stephane Buisson** has much more experience with this, but I will add my two cents in... The physical machine is pretty solidly build.  The core components seem to be far above the quality of the electronics and quite good for a machine at this price point.   If you want to get a K40, simply avoid the Moshi/MoshiDraw setup and you will likely be happy.  


---
*Imported from [Google+](https://plus.google.com/+JeremieFrancois/posts/8hrL5n2XAqi) &mdash; content and formatting may not be reliable*
