---
layout: post
title: "Another question...is it possible to use Corel Draw directly with the K40...without using CorelLaser or Laser Draw?"
date: March 06, 2017 14:36
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Another question...is it possible to use Corel Draw directly with the K40...without using CorelLaser or Laser Draw? Is that possible right out the box?





**"Nathan Thomas"**

---
---
**Ariel Yahni (UniKpty)** *March 06, 2017 14:44*

Nope, those are the ones that will generate the code in the machine ( stock board ) language


---
**Mark Brown** *March 06, 2017 14:47*

Right.  CorelLaser is just CorelDraw with some laser controls stuck on the top.



With the stock control board you're stuck with those two (there's actually a third, but it's worse) programs.


---
**Jim Hatch** *March 06, 2017 16:03*

**+Twelve Foot** Actually CorelLaser is an add-in for Corel. You need a base Corel install (they provide a sketchy version of Corel 12) for the add-in to work. That gives you the hook from Corel to the K40 bypassing LaserDrw. The toolbar gets populated with the controls from LaserDrw so you can go direct to the K40. That also means you can use any legit version of Corel you have (except Home & Student edition) up to X8.


---
**Justin Mitchell** *March 07, 2017 16:01*

The choice really is  Random Vector Software (eg Inkscape) plus LaserDraw, OR, CorelLaser.  no other way to communicate with the nano controller board. Change the control board and a whole new world of software options will open up.




---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/6cf8sSYct67) &mdash; content and formatting may not be reliable*
