---
layout: post
title: "I've always had a problem with my cuts - a white tiny ridge / split, always at the entry point on any shape, even a simple circle"
date: November 01, 2018 18:29
category: "Original software and hardware issues"
author: "Adam J"
---
I've always had a problem with my cuts - a white tiny ridge / split, always at the entry point on any shape, even a simple circle. I've had this on two different machines, both when running a Smoothie and the original stock controller. I've tried different power settings and the same happens on both perspex sheet and plywood. I get this when using Laserweb, Lightburn and the nasty Chinese software with a stock controller. Has anybody ever experienced this? Could it be my bed not level?

![images/48b73428080503bb67fb06df4226e911.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/48b73428080503bb67fb06df4226e911.jpeg)



**"Adam J"**

---
---
**Anthony Bolgar** *November 01, 2018 19:28*

Loose belt or loose coupling to a stepper motor are the most likely culprits. Also make sure lens and mirrors are tight in their mounts.




---
**Ned Hill** *November 01, 2018 21:12*

I've also had this a bit, not enough for me to be really concerned with though.  Always assumed it was a belt issue.


---
**Gee Willikers** *November 01, 2018 22:13*

You might want to try a lead-in and lead-out. That's what they do with water jets; the imperfection is the initial burning through of the material.


---
**Stephane Buisson** *November 02, 2018 08:09*

I think **+Gee Willikers** is right,  first perforation issue when the energy couldn't spread into a yet to come  gap. lead-in is the option, but I don't have a software to suggest (I open a ticket a long time ago into laserweb : [https://github.com/LaserWeb/LaserWeb4/issues/87](https://github.com/LaserWeb/LaserWeb4/issues/87), but I don't think that would happen). **+Claudio Prezzi** **+Ariel Yahni**


---
**Claudio Prezzi** *November 02, 2018 08:34*

I also belief this is a result of first perforation. I gess the laser is not long enough at the same position to melt the surface like on the rest of the cut. Instead of a lead-in, which always destrois the inner or outer part of the cut, a short delay atfer laser activation until laser starts moving could also solve the problem. But this should be implemented in the firmware, not in gcode, because it needs an exact timing and is deeply depending on the machine.




---
**Stephane Buisson** *November 02, 2018 08:56*

**+Claudio Prezzi** also depend on material.


---
**Claudio Prezzi** *November 02, 2018 09:15*

**+Stephane Buisson** Oh, you are right, I forgot that.


---
**Tim Gibson** *November 03, 2018 15:56*

It's vapor deposition of the ablated material. The air nozzle cant clear the vapor away in a dead end cut. 


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/E8PqEE84fWW) &mdash; content and formatting may not be reliable*
