---
layout: post
title: "Moving house recently the flimsy Trunions on my bandsaw broke"
date: October 08, 2018 12:00
category: "Object produced with laser"
author: "Nigel Conroy"
---
Moving house recently the flimsy Trunions on my bandsaw broke. 

Rather than pay $30 a trunion in steps the trusty laser!

The table won't be able to pivot but I can't remember a time I actually did that!





![images/a9beff4e322f6c7bb4b31201fc5a85ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a9beff4e322f6c7bb4b31201fc5a85ae.jpeg)
![images/db4229d87cee62efb8fdfba0d11a9460.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db4229d87cee62efb8fdfba0d11a9460.jpeg)
![images/42dd38536bbb5093a273321d66b6ba95.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42dd38536bbb5093a273321d66b6ba95.jpeg)
![images/0a8982e7e9e0178c62e45fd4e9aedf3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a8982e7e9e0178c62e45fd4e9aedf3a.jpeg)
![images/abd347b7dbe3c41b9a0f3d34dfb30471.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/abd347b7dbe3c41b9a0f3d34dfb30471.jpeg)

**"Nigel Conroy"**

---
---
**Don Kleinschnitz Jr.** *October 08, 2018 12:45*

Resourceful....If you made shorter parts for the middle [leaving a slot] would that have kept the table angle adjustment?


---
**Nigel Conroy** *October 08, 2018 12:54*

I was worried about the strength if I left a slot large enough to still pivot. 


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/J1vYpzwceAD) &mdash; content and formatting may not be reliable*
