---
layout: post
title: "I want to use the laser to reduce the thickness of specific areas of a piece of horn before I cut a shape out it"
date: April 04, 2017 11:12
category: "Object produced with laser"
author: "Phillip Meyer"
---
I want to use the laser to reduce the thickness of specific areas of a piece of horn before I cut a shape out it. I can successfully cut through the 6mm horn (with about 4 or 5 passes) but I want to reduce the thickness of some areas by about 1mm or 2mm first, ideally graduating so there is a curve across the surface.

I wondered if anyone has ever done this successfully in either engraving mode or cutting mode?

I don't want the thickness to be even across the whole piece or I'd use a manual 'thicknesser' first. I was thinking of creating a simple shape where I want to reduce thickness and have it 'engrave' this shape, then repeat this several times, with a smaller shape over the areas where I want it to curve in.

Any suggestions would be welcome, I will post the result if it works.





**"Phillip Meyer"**

---
---
**HalfNormal** *April 04, 2017 12:51*

This really depends on the controller and software you have and use. Stock nano/moshi controllers will take more work than a DSP/smoothie and the appropriate software.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2017 13:24*

I'd suggest testing a gradient engrave bitmap, with the curve profile you want based on the scale of greys. May or may not work as well in practice as it theoretically should.


---
**Phillip Meyer** *April 04, 2017 13:57*

I have the basic controller that comes with the K40 but maybe this is the excuse I need to upgrade to a Cohesion3D. Iwill see if I can figure our how to do grey scale in CorelDraw :-(


---
**Ned Hill** *April 04, 2017 14:12*

**+Phillip Meyer** it's not possible to do true gray scaling with the stock controller.  What people do however is to use dithering to create a gray scale effect by varying the density of engraved dots.  It maybe possible to do some profile engraving but it's going to be a step profile where you have a series of engrave elements and you engrave sequentially with increasing power or slower speed.


---
**Phillip Meyer** *April 04, 2017 14:36*

Hmm, then I need a new controller, thanks. The Cohsion3D looks good and relatively easy to fit to a K40. I think I will have to buy one.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 04:37*

**+Phillip Meyer** What you could do with the stock board is create a series of rectangles, each progressively smaller than the previous. Engrave the biggest rectangle first, then the 2nd biggest, then the 3rd, & so on. Do them all at the same power level & it should give you a 45 degree transition. Not perfect & would be a hell of a job to setup each operation. I think the C3D is probably a good way to go to get the results you're after.



Oh, just realised Ned said the same thing about the series of engravings.


---
**Phillip Meyer** *April 05, 2017 22:01*

I'm finding it hard to do that with CorrelDraw. I tried exporting a planar surface from rhino as a png and imported it but when I tried to engrave with it, it just threw up an error. Maybe I need to do something in CorelDraw to have it engrave the solid png image?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 22:25*

**+Phillip Meyer** CorelLaser will only do black & white. So on & off. Any gradiented image will just laser as all black for any variation away from white. There's 2 options that I envisage may work. I'll draw some examples & add them to the post in a minute.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 22:45*

Here is engrave idea 1:



![images/ae709be0014bda788f4d3d8d5d30b0da.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ae709be0014bda788f4d3d8d5d30b0da.png)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 22:49*

Engrave idea 2: Diffusion dither on a gradient pattern.

![images/65aae6dea28a3f26cbe42beea363de89.png](https://gitlab.com/funinthefalls/k40/raw/master/images/65aae6dea28a3f26cbe42beea363de89.png)


---
**Phillip Meyer** *April 07, 2017 11:53*

I managed to reduce thickness consistently using engraving but still need to experiment with a gradient pattern. I think it might be best to just get a better control board for the laser as I find CorelDraw so frustrating (not to mention the fact I have to keep rebooting my Mac to run windows)

![images/4b0b478740e4a42da5b6a4322d9a4e95.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b0b478740e4a42da5b6a4322d9a4e95.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2017 12:28*

**+Phillip Meyer** That looks to be a great result. Did you engrave that whole depth into the material?



I totally agree on the frustrations with CorelDraw. It seems a very clunky interface & made it difficult to work with the laser for me (as last time I used CorelDraw was over a decade ago).



With controller upgrade you will be able to run natively in OSX, so that will be an added benefit for you as well as the improved performance/capability.


---
**Phillip Meyer** *April 08, 2017 22:27*

Hi, yes, engraving seemed to cut about .45mm deep so I ran 5 passes. I tried different size boxes first to try and give a graduated depression but the steps were very obvious (an about 0.5mm apart) so it would have required more sanding that manually filing the additional grooves. I have a cohesion3d board on order, hopefully that will give me more control.


---
*Imported from [Google+](https://plus.google.com/117194752728291709572/posts/R7QDsSVxZQV) &mdash; content and formatting may not be reliable*
