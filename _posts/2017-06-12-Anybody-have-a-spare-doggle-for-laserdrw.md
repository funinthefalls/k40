---
layout: post
title: "Anybody have a spare doggle for laserdrw"
date: June 12, 2017 11:15
category: "Discussion"
author: "3D Laser"
---
Anybody have a spare doggle for laserdrw





**"3D Laser"**

---
---
**Andy Shilling** *June 12, 2017 11:29*

Where in the world are you?


---
**3D Laser** *June 12, 2017 11:54*

**+Andy Shilling** buffalo ny


---
**Andy Shilling** *June 12, 2017 12:00*

Hhhmmm I have one here but not sure it's worth while sending it from the UK. If you can't find one your side of the pond let me know and for the cost of postage you can have this one.


---
**Jim Fong** *June 12, 2017 14:03*

For the M2nano board?   My board died in a power surge but I assume the dongle is still good.  You can have it.  Send me your address.  I'm in Michigan 


---
**3D Laser** *June 12, 2017 14:23*

**+Jim Fong** thank you I donated my laser to a local high school but I still have some files I need to convert to bitmaps and I can't do it without the doggle 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 12, 2017 15:08*

If Jim's dongle doesn't work, I still have mine from my m2nano board also. In Australia though.


---
**Jim Fong** *June 12, 2017 15:44*

**+Corey Budwine** I found it and put it in a envelope. Waiting for address and I'll drop it off at the post office when I go to work later. 


---
**Ashley M. Kirchner [Norym]** *June 12, 2017 15:53*

Isn't file conversion done in the graphics software? What does the dongle have to do with running that? 


---
**Martin Dillon** *June 12, 2017 16:00*

How many files are you talking?  seems to me it would be easier to send the files and have someone save them in the right format.  Or just borrow the dongle back from the High school, since school will be out soon.


---
**3D Laser** *June 12, 2017 16:11*

**+Ashley M. Kirchner** for what ever reason laser draw will not let you export without the doggle I have close to 100 files 


---
**Ashley M. Kirchner [Norym]** *June 12, 2017 17:29*

Did you actually draw your design in laser draw? (I never did that, so I don't know that work flow.)


---
**3D Laser** *June 12, 2017 19:25*

**+Ashley M. Kirchner** yes when I first got the laser I did and just kinda made it work for me.  


---
**3D Laser** *June 12, 2017 19:25*

**+Jim Fong** Jim sent you a hang out message 


---
**Ashley M. Kirchner [Norym]** *June 13, 2017 16:53*

Ah, gotcha. I never drew anything within laser draw EXCEPT my living hinges. I could never get single-pass cutting to work (no matter how thin I made the lines), but drawing them in laser draw worked. But that was just as easy to recreate within Illustrator once I ditched the Chinese controller. No more relying on the dongle. :)


---
**Jim Fong** *June 13, 2017 17:49*

**+Corey Budwine** I checked my email and didn't see your address yet.   Fong.jim@gmail.com






---
**Jim Fong** *June 15, 2017 16:59*

Received your address. Will send out today. 


---
**3D Laser** *June 21, 2017 16:16*

**+Jim Fong** I got the USB key today thanks again!  Let me know if I owe you anything 


---
**Jim Fong** *June 21, 2017 16:21*

**+Corey Budwine** you owe nothing.  Just let me know if it works or not.  The computer it was plugged into died in a power surge, same with the M2nano laser board.  The USB key was something I would not ever need again after upgrading the laser to a C3D board.   


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/EAAMAMZZAqz) &mdash; content and formatting may not be reliable*
