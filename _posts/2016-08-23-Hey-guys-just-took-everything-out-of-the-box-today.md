---
layout: post
title: "Hey guys just took everything out of the box today"
date: August 23, 2016 19:46
category: "Original software and hardware issues"
author: "Doug Western"
---
Hey guys just took everything out of the box today. I have the CD with all of the programs and files on it.  Is there a thread someone can point me to that has the order in which everything needs be loaded/run from the CD.





**"Doug Western"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 24, 2016 03:34*

I don't think such a thread exists. Basically what you want to do is install Corel Draw (from the CD or your own legit copy) then install the CorelLASER 2013.02. That's pretty much all you need to install.



Some have mentioned driver files, but I don't recall ever installing any driver files myself.


---
**Doug Western** *August 25, 2016 09:22*

Is there a trick to installing CorelLaser 2013.02?  I click on it and nothing happens.  Corel Draw loaded just fine.  Am I missing something?  Thank you for your help.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 09:28*

Is it a .exe file? I can't remember what exactly was on the disc originally. If it is a .rar or .zip file you will need to extract it first, then install from the .exe.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 09:29*

Actually, it looks to be an .exe file. So try right click on it & choose "Run as administrator".


---
**Doug Western** *August 25, 2016 09:36*

I tried that to, the screen unknown program shows up I click continue and then nothing


---
**Doug Western** *August 25, 2016 09:37*

What is this UKey file for?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 10:02*

No idea what UKey.exe is for. It's all in Chinese. I just checked the version I have of CorelLaser 2013.02 & it seems to run fine. Straight to a prompt for which language then the standard install wizard pops up.



It's possible your version is corrupt. You can obtain a version from the software manufacturer's website: 

[http://www.3wcad.com/download.asp](http://www.3wcad.com/download.asp)



First link at top of page:

[http://www.3wcad.com/download/CorelLASER%202013.02.rar](http://www.3wcad.com/download/CorelLASER%202013.02.rar)


---
**Doug Western** *August 25, 2016 10:09*

Was looking in another post about the corel5 and the corellaser downloads from a drop box - is that you?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 10:32*

**+Doug Western** Yeah, that was me. I wasn't sharing the links for that anymore as there were concerns regarding piracy. I have since upgraded to x7 though, since that version of x5 stopped working.


---
**Doug Western** *August 25, 2016 11:38*

does the CorelLaser 2013.02 work?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 11:51*

**+Doug Western** Mine did with Corel Draw 12, Corel Draw x5 & Corel Draw x7.


---
**Doug Western** *August 25, 2016 12:15*

UGH it is doing the same thing, I click on it and it gives me the "do you want to allow this app from unknown publisher ..."  I click yes and then nothing.  not sure what is going on


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 12:42*

**+Doug Western** That sounds like a windows defender or something similar being odd. What version of windows are you running?


---
**Doug Western** *August 25, 2016 12:46*

I am running Windows 10


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 13:10*

Okay, I am on Win10 too. So I will see if I can  replicate it & figure out what is causing that issue for you.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 13:13*

**+Doug Western** In your Windows 10 Update & Security settings, can you go to the For Developers tab & allow "Sideload Apps". I have that allowed, I wonder if that will let the app run?


---
**Doug Western** *August 25, 2016 13:31*

Thanks, I will try that when I get back in


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 13:50*

**+Doug Western** Hope it will fix the problem. Else we'll have to see what else it could be. Another thought is do you have antivirus software running? It could be blocking it if so.


---
**Doug Western** *August 25, 2016 19:55*

Ok - I did a reboot on the computer and then it loaded (I guess) now just to figure out how to use it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 26, 2016 02:27*

**+Doug Western** Good news. It's a little fiddly to work out, but there is heaps of help in this group. Check this ([https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing)) in my Google Drive for a bunch of pictures that may assist. Make sure you set your Device ID & Board Model in the CorelLaser settings (otherwise things will not do what you imagine they will).


---
*Imported from [Google+](https://plus.google.com/102298503204290496961/posts/iQiD3KRw2Tb) &mdash; content and formatting may not be reliable*
