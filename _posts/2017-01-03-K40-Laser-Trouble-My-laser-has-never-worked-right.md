---
layout: post
title: "K40 Laser Trouble, My laser has never worked right"
date: January 03, 2017 23:12
category: "Discussion"
author: "Harold Oney"
---
K40 Laser Trouble,

My laser has never worked right.  it will fire when i push the momentary button but when i send it a drawing, the cutter head moves but the laser will not fire.  I opened the back and the tube lit up with the momentary switch and does not light up at all when operating in the normal mode????



Anyone have any ideas?  i was thinking about buying a new motherboard but not sure that is the way to go?



Thoughts? 





**"Harold Oney"**

---
---
**Harold Oney** *January 03, 2017 23:18*

Ok after finding another note on the interweb... could my settings be wrong in my files? 




---
**Jim Hatch** *January 03, 2017 23:59*

Most likely a settings problem in LaserDRW or CorelLaser (you don't mention which you're using). In the machine settings page there are two fields that have to be correct. The first is the Machine Model and the entry should be the only one with -M2 in it. It will be the first field on the settings page. This only applies to M2 Nano board machines and not the Moshi ones but they're the ones that have been shipping for the last year or so. 



The other field is on the bottom of the settings page and is your machine's ID. You'll find it on the controller board and it's 16 characters (alphanumeric). If those aren't correct the software's not sending the info in a format your machine understands. 



You also need to have the USB key plugged into the PC you have connected to the laser.


---
**Harold Oney** *January 04, 2017 00:14*

Jim, have both LaserDRW and CorelLaser. 


---
**Jim Hatch** *January 04, 2017 00:24*

Start with LaserDRW. Once you get it engraving Hello World and cutting a box you'll know you have the settings right. You'll put the same settings in CorelLaser when you move to that but start simple first.


---
**Harold Oney** *January 04, 2017 00:43*

Ok, The laser started off firing and engraved one side of the box and then stopped firing. now the print head moves and it will cut as long as i hold down the momentary button?   I have no idea.


---
**Jim Hatch** *January 04, 2017 02:13*

Did you get both the model & I'd set correctly?



If so, then make sure the bed size is correct - probably defaulted to 400x400mm. It should be more like 200x280. The size depends on your bed's physical size (whether you've taken out the exhaust flanges, etc) but settings in the 200 range will be fine.


---
**Jim Hatch** *January 04, 2017 02:14*

Also make sure you're running it at something between 10 and 18ma to make sure it lases. Don't run it if the water isn't pumping or you'll cook your tube.


---
*Imported from [Google+](https://plus.google.com/106457360071444565259/posts/P3WP4b2j6dp) &mdash; content and formatting may not be reliable*
