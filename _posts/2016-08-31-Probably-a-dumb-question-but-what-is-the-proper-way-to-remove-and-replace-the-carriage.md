---
layout: post
title: "Probably a dumb question, but what is the proper way to remove (and replace) the carriage?"
date: August 31, 2016 19:38
category: "Hardware and Laser settings"
author: "Purple Orange"
---
Probably a dumb question,  but what is the proper way to remove (and replace) the carriage? 

![images/0a9d7416cf10b4dca0cb180c2e1aa61f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a9d7416cf10b4dca0cb180c2e1aa61f.jpeg)



**"Purple Orange"**

---
---
**Jim Hatch** *August 31, 2016 19:55*

Remove the bed - just a couple screws. Then the frame comes out with a half dozen screws - the black piece on the front comes off first. 



Make sure you unplug the wiring - either end is okay but at the board is easiest & then snake them out the hole on the right.



You need to slip the belt on the right side to fix the skew. There's a screw on the top end of the rail that will let you loosen things up to allow you to slip the belt. The belt is ribbed and rides on a cogged wheel.


---
**Ariel Yahni (UniKpty)** *August 31, 2016 20:05*

I don't think there a proper way. Remove and build with what you are comfortable. I'm using openbuilds wheels and profiles


---
**Ariel Yahni (UniKpty)** *August 31, 2016 20:05*

![images/697b809b60153c649233755243d51231.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/697b809b60153c649233755243d51231.jpeg)


---
**Chris Sader** *September 01, 2016 00:40*

**+Ariel Yahni** how's your build coming? I'd love to see how you're designing the frame and carriages. thinking about doing something similar soon


---
**Ariel Yahni (UniKpty)** *September 01, 2016 00:49*

**+Chris Sader**​ should have some updates soon


---
**Chris Sader** *September 01, 2016 16:26*

**+Ariel Yahni** cool. thinking about starting to buy the parts I can't make myself with the laser (i.e., rails, wheels, eccentric nuts, etc.). If you're able to share those basics so I can make sure I've got everything, please do




---
**Ariel Yahni (UniKpty)** *September 01, 2016 16:40*

**+Chris Sader**​ sure, let's move this here conversation here [https://plus.google.com/+ArielYahni/posts/ZwCETMpWXmk](https://plus.google.com/+ArielYahni/posts/ZwCETMpWXmk)


---
*Imported from [Google+](https://plus.google.com/112469922804049225807/posts/QuRkxrXTR1B) &mdash; content and formatting may not be reliable*
