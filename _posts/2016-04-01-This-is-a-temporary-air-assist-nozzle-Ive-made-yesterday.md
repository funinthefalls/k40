---
layout: post
title: "This is a temporary air assist nozzle I've made yesterday"
date: April 01, 2016 19:26
category: "Modification"
author: "Mishko Mishko"
---
This is a temporary air assist nozzle I've made yesterday. During engraving and cutting testing, every material, wood or acrylic backing cought fire immediately, so I finally gave up and made three 5mm acrylic parts and glued them together, and, not having the air installation where the laser is currently situated, attached the small pump, which I normally use for vacuuming. Looks like it did a trick,  flames were gone entirely, although of course it can't compare with a decent compressor. I might add one more pump and install both in the electronics compartment in case I ever decide to take the laser to a picnic or something;) This should provide 30 l/min, and i remember reading somewhere people discussing this kind of capacity as a primary option...

Today i went to the signmaker's shop to buy some acrylic, and just by chance spotted this tape (bottom right). It's an aluminum self adhesive tape and I thought I might try using it in combination with another one for beam alignment. So, they gave me some 2m as a sample (enough for just about a lifetime of beam aligning;), and indeed it worked perfect. The first time i also added an ordinary tape under the aluminum one, as I did not want to risk getting molten metal on the mirror, but the beam doesn't as much as mark it.

My microstratocaster lacks detail big time, I guess I should have done much better prep work, but I'll learn eventually what gives best results.

![images/3198478f4cf9a07f309a1b61aecfe19b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3198478f4cf9a07f309a1b61aecfe19b.png)



**"Mishko Mishko"**

---
---
**MNO** *April 06, 2016 19:10*

like that idea of air assist, when my will arive that will be first thing to do... 


---
**Mishko Mishko** *April 07, 2016 23:07*

It works pretty well for a five minute job. I'm thinking of attaching this pump as a vacuum and bringing the tubing to the point where the laser beam hits the material, to see if it would remove the fumes locally. I've removed the ventilation duct, and the original fan doesn't do a great job of eliminating the fumes, maybe this would do the trick. If the pump is up to the task of course, will try and report;)


---
**MNO** *April 09, 2016 11:01*

I seen something like You say with desoldering iron. it worked ok. But that will be depending from what you want to cut or engrave. Amount of fume created can be to much. 

Remember making head of laser heavy it is never good. Forces that are created on fast engraving then are growing and you can lose quality because of movement 

I think i will make mine same like Yours but a little longer. Anyway it is about cleaning lens and path to work pice. I think about making some direction of that air to be sure it points direct where work pcs is. I think i could do that by gluing i 2cm long aluminium tube 3-5mm wide

And also when i check my focal length i will make to it direct a laser diode holder


---
*Imported from [Google+](https://plus.google.com/114169453703152974799/posts/jY2181ehNrf) &mdash; content and formatting may not be reliable*
