---
layout: post
title: "Is there a way to rotate a shape in laserdrw"
date: July 15, 2016 00:37
category: "Discussion"
author: "3D Laser"
---
Is there a way to rotate a shape in laserdrw





**"3D Laser"**

---
---
**greg greene** *July 15, 2016 01:01*

Sure - grab one of the little boxes on the outline of the shape and move it around on the screen till the shape is in the alignment you want (ie place the pointer on the box on the bottom line and drag it up and over the top line to flip shape upside down)


---
**3D Laser** *July 15, 2016 01:03*

**+greg greene** I want it at an angle  can't figure out how or if it is possible 


---
**greg greene** *July 15, 2016 01:07*

When I import an image the box the image is in is aligned with the margin of the work area.  On the outline of the graphic box - each line has a little box, try grabbing the right hand box and pulling it down, the box should rotate.  For text - its much easier, select text, enter your text in the text window then when done click on the work area, you will see the text box being drawn you can move the cursor at an angle and box will be drawn at that angle with text in it.


---
**3D Laser** *July 15, 2016 01:08*

Yes I have been able to rotate text and images but not shapes not for sure what I am doing wrong 


---
**greg greene** *July 15, 2016 01:10*

Ah I see what you mean - There may be a check box that makes the frees the shape to be rotated instead of drawn snapped to the grids. I haven't tried that however.


---
**Jim Bennell (PizzaDeluxe)** *July 15, 2016 23:59*

When you have an object or shape selected, at the top of the program you will see X and Y coordinates then the size of the shape and then you will see a box with angle of rotation you can set. 



You can also select an object , go to the top- Object menu pull down, Then Transformations and select Rotate. Or press Alt+F8 to pull up the rotate dialog.


---
**3D Laser** *July 16, 2016 00:01*

**+Jim Bennell** you are my hero!


---
**3D Laser** *July 16, 2016 00:08*

**+Jim Bennell** are you sure that is laserdrw I don't see them when I just checked


---
**Jim Bennell (PizzaDeluxe)** *July 16, 2016 01:03*

Oh sorry I was looking at Corel , I haven't used LaserDRW much. Just opened it up and I can't find any options for rotating an object. Only thing I found was if you create a star shape with the star tool, the object properties dialog has a rotate option but the other shapes do not. It doesn't seem like the best program for design. Might be good for bringing designs into it for lasing. 



If you just want your whole design to be engraved or cut at 90,-90,180,mirrored there is an option in the engraving manager (Rotate) its normally set on do nothing. But if you just want to rotate one object not the whole design I suggest using Corel or something else like Inkscape or AI. Most programs will let you hold a hotkey while dragging a rotation to make it snap to 45 degree increments (usually ctrl+mouse1 drag to rotate or shift+mouse1 drag)



I do a lot of my designs in Inkscape and export as WMF for corelLaser. And in corel I use the WFM interpreter in the data settings. seems to work pretty good.  



So what I had said earlier works in CorelLaser.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/f95FfsboR6C) &mdash; content and formatting may not be reliable*
