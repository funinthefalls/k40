---
layout: post
title: "U guys are rocking it! I bought a awc 608 a year ago and gave up because the power supply was always wrong"
date: April 09, 2017 10:07
category: "Modification"
author: "Chev Chelios"
---
U guys are rocking it! I bought a awc 608 a year ago and gave up because the power supply was always wrong. Is anyone willing to help me a bit?





**"Chev Chelios"**

---
---
**Don Kleinschnitz Jr.** *April 09, 2017 10:27*

whats Up?


---
**Chev Chelios** *May 07, 2017 05:27*

The wiring diagram never seemed to include my power supply. I got really tired of weeding through the thousands of confused posts on [lightobject.com](http://lightobject.com). Also the several ground wires just kinda vanish to the symbol but in real life....wth?  Thanks for the reply...Ill shoot you a pic of my PS.



![images/a2028806413c6da28429061ad9adfd23.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a2028806413c6da28429061ad9adfd23.jpeg)


---
**Don Kleinschnitz Jr.** *May 07, 2017 17:07*

**+cody whatley** might have missed it what do you need help with?


---
**Chev Chelios** *May 08, 2017 01:02*

I'm just looking to see if anyone had a tip on the terminals/wiring diagram that leaf in/out of this particular power supply.Also, where all the ground wires meet? Thanks in advance


---
**Don Kleinschnitz Jr.** *May 08, 2017 02:46*

**+cody whatley** is this what  you need? Don't know if this is the exact one but it should be close.

![images/466345617038234b1a0a2296acb4c2c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/466345617038234b1a0a2296acb4c2c1.jpeg)


---
*Imported from [Google+](https://plus.google.com/+codywhatley2017/posts/RiAfgTbZSeb) &mdash; content and formatting may not be reliable*
