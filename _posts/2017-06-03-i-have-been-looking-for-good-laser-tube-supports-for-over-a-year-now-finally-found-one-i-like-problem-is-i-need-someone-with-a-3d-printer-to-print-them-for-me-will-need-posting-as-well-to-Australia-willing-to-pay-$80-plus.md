---
layout: post
title: "i have been looking for good laser tube supports for over a year now ,finally found one i like problem is i need someone with a 3d printer to print them for me ,will need posting as well to Australia ,willing to pay $80 plus"
date: June 03, 2017 09:48
category: "Discussion"
author: "Phillip Conroy"
---
i have been looking for good laser tube supports for over a year now ,finally found one i like problem is i need someone with a 3d printer to print them for me 

,will need posting as well to Australia ,willing to pay $80 plus post ,[https://www.thingiverse.com/thing:1263096](https://www.thingiverse.com/thing:1263096) 





**"Phillip Conroy"**

---
---
**Joe Alexander** *June 03, 2017 10:38*

i currently use those and they are VERY tight. Printed another design but have yet to try it. here's the current ones.

![images/831c2b9f019af9f67c2a49685137b829.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/831c2b9f019af9f67c2a49685137b829.jpeg)


---
**Joe Alexander** *June 03, 2017 10:39*

The ones I have yet to try.

![images/8f5d1588ad5116a2f14d6a289ae3cafb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f5d1588ad5116a2f14d6a289ae3cafb.jpeg)


---
**Don Kleinschnitz Jr.** *June 03, 2017 11:12*

This one may be worth looking at:

[plus.google.com - Yet another K40 laser tube mount. I just picked up a K40 a few weeks ago and ...](https://plus.google.com/106762627155275597313/posts/H3RHp65DPSq)


---
**Joe Alexander** *June 03, 2017 12:30*

that would be the one in the second pic **+Don Kleinschnitz** :P


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2017 13:32*

Try look at 3D Hubs website. You are likely to have someone in your local area operating a hub that will print for you for a reasonable price. Save big on time & shipping from international or further away in Aus.



[https://www.3dhubs.com/](https://www.3dhubs.com/)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2017 14:10*

Just remembered, I had a referral code that will give you $10 off your first print (I think you have to purchase $30 worth of print though) if you're interested in it. Full disclosure, they'll probably give me some credit or something for referring someone (can't remember exactly but it's possible).



[3dhubs.com - 3D Hubs: Browse online 3D printing services](http://3dhubs.refr.cc/WN8KF9G)


---
**Phillip Conroy** *June 04, 2017 08:19*

thanks for the 3d printing suggestion ,have placed order 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2017 08:50*

**+Phillip Conroy** No worries. Hope it works out well for you :)


---
**Phillip Conroy** *June 05, 2017 11:03*

My 50w tube is cutting fine ,however machine sat unused for 1 year before I brought it second hand,and since I know that they slowly loose there gases over time ,was thinking of buying spare tube ,just making up my mind what size in watts 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 06, 2017 12:37*

1kw sounds good to me :D


---
**Phillip Conroy** *June 07, 2017 07:56*

Not if you want to egrave  with it that and I do not have a spare 250000 or the space and power to run it 


---
*Imported from [Google+](https://plus.google.com/108005343562094867099/posts/fVURFD37VXB) &mdash; content and formatting may not be reliable*
