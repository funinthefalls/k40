---
layout: post
title: "First Mod ;-)) budget around 4  Waiting for my 5mW Red Light Cross Curve Line Module to come from HK"
date: November 19, 2014 00:54
category: "Modification"
author: "Stephane Buisson"
---
First Mod ;-))      budget around 4 € 



Waiting for my 5mW Red Light Cross Curve Line Module to come from HK.

as promise earlier I designed the holder to be 3D printed ( in green). a tiny bit of laser cut (light blue) to be glued on the bottom green, sliding on the original head will allow the setting for Z. jut add a couple of screws to hold it in place. (distance between material and below bottom head part should be 30mm (focus 50mm from lens).

pipe for air assist in, air going out by the middle and by side jet both toward focus point.



[http://www.ebay.co.uk/itm/371080302598?_trksid=p2060778.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.co.uk/itm/371080302598?_trksid=p2060778.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)

![images/14ae1e8e698d7a34373e434793e9634c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14ae1e8e698d7a34373e434793e9634c.jpeg)



**"Stephane Buisson"**

---
---
**Henner Zeller** *November 19, 2014 07:21*

If you have two of them, one on each side, you can also indicate if you are in focus. Say you mount them on the x-axis left and right of your lens, then the lines in y direction of both cross-lasers will only sit on each other if you are in focus (if you have adjusted the angle right).



That can help focusing.


---
**Stephane Buisson** *November 19, 2014 10:49*

+Henner Zeller

 I'll like youridea, but space around the head is tight. it's more space in Y to fit the cross line and airassit without bumping into frame side. Not enough space in X


---
**Imko Beckhoven van** *November 19, 2014 11:28*

A beam combiner is a better option. For cutting this is no problem but is you engrave rhe speed and rapid change of direction might be to mutch..


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/ZH6WeFGNCWR) &mdash; content and formatting may not be reliable*
