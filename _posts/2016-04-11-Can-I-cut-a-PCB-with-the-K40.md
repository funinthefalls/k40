---
layout: post
title: "Can I cut a PCB with the K40?"
date: April 11, 2016 02:39
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Can I cut a PCB with the K40? Anyone have any experience with this?



I'm working on my LaserCam mod today & need to use sections of this PCB for my LEDs & resistors.

![images/8ee84821e25b43fe681f069bbe799a6f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ee84821e25b43fe681f069bbe799a6f.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**ThantiK** *April 11, 2016 02:51*

No, you can't.  FR4 is really nasty stuff, and it will just blacken.  It's best to just score with a knife and snap anyways, if I recall correctly.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 03:01*

**+ThantiK** Thanks for that ThanktiK. I wasn't aware. I will do as you suggested.


---
**Thor Johnson** *April 11, 2016 03:39*

If you get FR2 (paper based), it's easier to score a PCB.  Unfortunately, I haven't found anything that does copper... if you have a UV/Blue laser, you can etch photo-resist.  If you paint black, you can use a K40 to get rid of the paint and then use an etchant to do the pcb.



But copper doesn't want to be cut.  It reflects more than it absorbs of the 10um CO2 wavelength, and even if yo got it to absorb (eg, Fiber Laser with 1um), by the time it absorbs it, it conducts the heat rapidly away...


---
**Alex Krause** *April 11, 2016 03:40*

There is toutorials on YouTube on scoring PCB check it out if you are unsure


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 04:10*

Thanks **+Thor Johnson** & **+Alex Krause**.



I've managed to score it with a simple Stanley knife & a ruler then snapped it. Not super straight edge, but doesn't really matter. It's just to hold 1 led & 1 resistor (for each of the 2 that I made) to make it easier to mount them where I want them going.



But, I'll keep in mind what you mentioned for future if I ever want to etch photo-resist. & yeah, YouTube is always a great resource. Thanks again.


---
**Ashley M. Kirchner [Norym]** *April 11, 2016 21:54*

Dremel tool. I made some custom, tiny, single LED pieces a while back. The boards were 7x3 grids which I then cut into smaller pieces. Just watch your fingers. 
{% include youtubePlayer.html id="C_2T23rSzJo" %}
[https://www.youtube.com/watch?v=C_2T23rSzJo](https://www.youtube.com/watch?v=C_2T23rSzJo)


---
**ThantiK** *April 11, 2016 22:57*

**+Ashley M. Kirchner** the problem with the dremel tool is that the dust is <i>severely</i> bad for you.  Majorly bad for you.


---
**Ashley M. Kirchner [Norym]** *April 11, 2016 22:58*

Did you not notice the vacuum tube, which I specifically pointed out in the video as well? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 02:33*

**+ThantiK** Is the dust bad to breathe, or in other aspects as well.

**+Ashley M. Kirchner** I noticed the vacuum & I think it's a great idea to use the dremel like a table saw. Going to have to make a mount/bracket for my dremel to do similar. What would be even cooler is to make it literally like a table saw, where you can use a screen to push the pieces along & keep them running straight. Might have a go at something like that when I have some spare time.


---
**Alex Krause** *April 12, 2016 02:59*

PCB are typically made of fiber glass and can cause severe damage to your lungs


---
**Ashley M. Kirchner [Norym]** *April 12, 2016 03:15*

Yep, that glass dust is horrible. That's why I have that vacuum nozzle so close, to suck up anything that blows off. I also use thin boards, 0.8mm. Standard thickness is 1.6mm, twice as much to cut through.


---
**Alex Krause** *April 12, 2016 03:17*

Does the vacuum have a filter with a small enough micron filter to keep from expelling dust from the exhaust ?


---
**Ashley M. Kirchner [Norym]** *April 12, 2016 03:37*

It has a few stages, yes. The last filter before air gets blown out is also a HEPA filter.


---
**ThantiK** *April 12, 2016 03:41*

**+Yuusuf Sallahuddin**​ the dust is very bad to breathe.  If you don't have a filter fine enough, it will also get blown out the exhaust and you'll just end up breathing it anyways. This is exactly why I suggested the score and snap method.  If you have a wet saw for PCBs, that's fine too, as the fine particles never enter the air. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 13:46*

**+ThantiK** Thanks for that. I was wondering if the dust can enter via your skin too, or if a respirator would prevent the problems.


---
**Vince Lee** *April 12, 2016 19:04*

Hmm.  What about mounting the dremel tool on the laser head then "cutting" with an appropriate bit and the beam off?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 19:07*

**+Vince Lee** You think exactly like I do. I was just thinking about modifying the laser head to have an attachment for the dremel so I can use it to cut metal & other parts. Only thing I am wondering is how to make it raise up & down in between cut sections (else it will just continue cutting all the way to the next shape).


---
**Vince Lee** *April 12, 2016 22:36*

**+Yuusuf Sallahuddin** Offhand, I'm thinking of a dremel mounted on a hinged platform and having a solenoid that by default pushes the dremel sideways, tilting it up a bit in the process.  Harbor Freight (dunno if you have those there) has a cheap and small dremel-type tool that might work perfect for this.  [http://www.harborfreight.com/rotary-tool-kit-80-pc-68986.html](http://www.harborfreight.com/rotary-tool-kit-80-pc-68986.html)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 22:43*

**+Vince Lee** That's awesome to hinge the dremel up. I have a dremel already with an extension to it, so I will not have to mount the heavy motor section on the head, just the extension cable thing (which is a lot lighter). Then I can suspend the motor section above. What I wonder however, is how do we control the solenoid to force it to lift & lower every time we finish one shape & need to go to the next shape? Any idea on that? or manually press a button?



edit: we don't have a harbor freight to my knowledge here in Australia. But we have Bunnings & Masters (our biggest/best hardware shops, albeit a bit expensive prices though, just because they have a bit of a duopoly on the market).



wow that is super cheap. $10. I guess with conversion to aud, it would be more like $15-20, but still cheap compared to the $150 odd I paid for my dremel years ago.


---
**Alex Krause** *April 13, 2016 00:10*

Engraving would be about the heaviest duty task I would do on a k40 frame I don't think it's rigid enough to cut metal. I struggle with Aluminum on my OX CNC with a little 400w spindle 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 00:33*

**+Alex Krause** Thanks for that Alex. I intended to attempt to cut 2-3mm thick aluminium with the dremel attached. But if that is the case, I guess a manual test with the dremel is in order before I consider putting into the K40.


---
**Alex Krause** *April 13, 2016 00:46*

Remember to wear safety glasses :)


---
**Vince Lee** *April 13, 2016 04:25*

**+Yuusuf Sallahuddin**​ the fire line on moshiboard and psu is active low.  One can probably wire it to an ssr instead of the psu to drive the solenoid (with a snub diode!).﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 06:19*

**+Vince Lee** That's a bit beyond my electronics experience, however I see what you mean though. So when the laser would normally fire, instead it will have the solenoid "close" (is that the word?) & allow the hinge to drop the dremel into place. When not firing (i.e. when it is normally moving) the solenoid will "open", moving the hinge so that the dremel is raised? Am I getting you correct?


---
**Vince Lee** *April 14, 2016 03:28*

**+Yuusuf Sallahuddin** yes, that's the idea.  One could use a solid state relay to detect when the fire line was pulled down and instead of triggering the power supply to fire, use it to activate a solenoid.  The relay isolates the two circuits and allows the low current fire line to drive the much higher current needed to activate the solenoid.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 03:37*

**+Vince Lee** Oh right, so that's what the SSR does. I had no idea what they did. Makes sense when you explain that & I think it would definitely be worth a go.


---
**Alex Krause** *April 14, 2016 04:09*

I really think installing a drag knife to a k40 would be a beneficial tool over a rotary tool would allow for cutting leather,card stock, laminates and pvc based vinyl with no risk of chlorine gas exposure 


---
**Alex Krause** *April 14, 2016 04:11*

Any tool other than a laser will require a hold down sytem for stock  since a laser has no tool resistance there is no work holding in the cabinet 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 04:16*

**+Alex Krause** Yeah I want to install a scalpel blade to cut through leather. It would be much nicer than a burnt leather edge. But I've realised the blade needs to be able to rotate in order to follow the cut lines nicely. Unless a drag-knife allows it to auto rotate as it goes? & definitely would need some method of holding it down. Currently I hold things with magnets (to prevent the leather curling as I cut) but would need a much more permanent hold for knife setup.


---
**Alex Krause** *April 14, 2016 04:19*


{% include youtubePlayer.html id="bMoRUZnvbXw" %}
[https://youtu.be/bMoRUZnvbXw](https://youtu.be/bMoRUZnvbXw) this is a drag knife it swivels in the tool holder no power required to drive it


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 05:18*

**+Alex Krause** Thanks. I will check it out.



edit: I've actually seen those before. I thought the machine somehow rotated the knife mechanically. Good to know it works without mechanics. That is exactly what I want/need for my leather work. Thanks again Alex.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 05:34*

**+Alex Krause** This video shows me a bit more clearly how it works. 
{% include youtubePlayer.html id="YqvgA1P5hWg" %}
[https://www.youtube.com/watch?v=YqvgA1P5hWg&ab_channel=BrianOltrogge](https://www.youtube.com/watch?v=YqvgA1P5hWg&ab_channel=BrianOltrogge)



I was thinking it must be powered rotation of the blade because it is attached to the big motor thing. Then I realised (from the video you shared) that it says "disconnect the power to the spindle when using drag-knife". Thanks heaps.


---
**Alex Krause** *April 14, 2016 06:07*

I think I posted this video awhile back when you had a wallet you had made


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 09:20*

**+Alex Krause** I think you may have actually. I knew I'd seen it somewhere haha. I had no idea where though. Thanks again for reminding me, because it would be a great addition to the K40 & probably the best thing that could be added in terms of extra utility (as you mentioned).


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/DZaSGfTbMfJ) &mdash; content and formatting may not be reliable*
