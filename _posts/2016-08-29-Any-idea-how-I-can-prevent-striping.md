---
layout: post
title: "Any idea how I can prevent striping?"
date: August 29, 2016 00:22
category: "Object produced with laser"
author: "Bob Damato"
---
Any idea how I can prevent striping?

Corellaser on x8, engraving at 230mm/s on granite. Somewhere around 30% power (just a guess). Water supply is fresh and cool and pumping well. Not sure why the image is upside down, sorry.

![images/e4a77a542cb27154281e94d34d84b5d0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e4a77a542cb27154281e94d34d84b5d0.jpeg)



**"Bob Damato"**

---
---
**greg greene** *August 29, 2016 01:00*

I notice this on mine too, I found higher power fixed it.  I suspect the laser was unstable at the lower settings


---
**Bob Damato** *August 29, 2016 12:12*

**+greg greene** Thanks Greg, Ill give it a try!


---
**Bob Damato** *August 29, 2016 17:24*

Hmmm.  I went to about 80% power and still the same thing. Whats funny is I used the same image (which looks 100% fine in corel) and tried again on a smaller piece of granite. Same stripes, same places even though its all a lot smaller now (4x3 vs 5x7).

Ill try another pass at 100%


---
**Bob Damato** *August 29, 2016 17:51*

Nope. 100% no better. :(


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/2JR9Y5boxge) &mdash; content and formatting may not be reliable*
