---
layout: post
title: "Just got my K40 today and I would like to know what the tube of Silicone is for ?"
date: July 29, 2016 23:33
category: "Discussion"
author: "Robert Selvey"
---
Just got my K40 today and I would like to know what the tube of Silicone is for ?





**"Robert Selvey"**

---
---
**Ariel Yahni (UniKpty)** *July 29, 2016 23:43*

Is for sealing/isolating the electrical connections in the tube


---
**Robert Selvey** *July 30, 2016 00:05*

Do I need to put it on the connections or only if  I removed the tube ?


---
**Ariel Yahni (UniKpty)** *July 30, 2016 00:23*

**+Robert Selvey**​ your tube should have Allready a piece of hose and silicon so this is for when you are changing the tube


---
**Scott Marshall** *July 30, 2016 05:31*

Some of the stuff in the bag (even the bag) doesn't make a lot of sense. It's like extra things they had so they threw them in. Maybe instructions would be better?



Silicone has a definite shelf life, and I'd use fresh 100% clear RTV if I were replacing a tube. Nobody knows how old the little tube is, or what it's made for/from. The tape is a complete mystery to me. not to  even mention the "bed" and internal exhaust 'duct' both of which should go directly to the recycle to be made into something useful. then there's the plastic exhaust tube....



The K40 is a unique critter that only a laserhead could love. It should go down in history as the best "worst" product ever sold.



Check all the screws and wire connections, they fall off or loosen in transit, especially the ground wire where it goes onto the IEC power cord jack on the inside. The ground wire is attached with a "quick slide" terminal and falls off in shipping, completely ungrounding the chassis.



Plug the pump and laser into a switched outlet strip, that will help keep you from accidentally firing the laser with the pump off. This is a common cause for blown tubes, it only takes a few seconds without waterflow to destroy the tube, which costs nearly the price of the whole unit.



Watch yourself, there's No interlocks and there is exposed laser energy, 15,000volts and mains voltage. It's easy to get casual and put yourself at risk.



Align the mirrors and vent outdoors. Use a metal (fireproof) tube for venting.



If you already have heard all this, I apologize, but it's all common stuff when you 1st get your K40, and some people don't realize the K40's not really legal anywhere as it's sold. I tend to think of it as a kit, pre-assembled for shipping. It needs some TLC and a few parts to be ready for prime time.



Have fun,



Scott



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



You'll want this soon enough:

[http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050?hash=item3abe742d92:g:kagAAOSwIrNWFIY7](http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050?hash=item3abe742d92:g:kagAAOSwIrNWFIY7)



and these:

[http://www.ebay.com/itm/Mo-20mm-Reflective-Mirror-18mm-Znse-Lens-Focal-2-CO2-Laser-Stamp-Engraver-K40-/252283989821](http://www.ebay.com/itm/Mo-20mm-Reflective-Mirror-18mm-Znse-Lens-Focal-2-CO2-Laser-Stamp-Engraver-K40-/252283989821)



(the mirror/lens kit has the proper lens for the new head(it's larger), but you'll need a pump or regulator if you have shop air.)


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/iXimDiEZf1V) &mdash; content and formatting may not be reliable*
