---
layout: post
title: "Getting mixed messages on the orientation of the ribbon cable connector in the middleman board from OSHPark: Anybody know which is right?"
date: August 26, 2016 01:43
category: "Smoothieboard Modification"
author: "Chris Sader"
---
Getting mixed messages on the orientation of the ribbon cable connector in the middleman board from OSHPark:



[http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html)





[http://jimmybdesign.com/k40-laser-cutter](http://jimmybdesign.com/k40-laser-cutter)



Anybody know which is right? I soldered mine in like the one in the second link, so I'm really hoping I wasn't wrong.





**"Chris Sader"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 26, 2016 02:39*

There are pin numbers on the board where the connector goes & also the connector has a tiny number on it somewhere too. Although, it could all be backwards. I'd say there is a possibility that both Don & Jimmy are correct... for their particular machine. It has been noticed that the K40s don't really follow any standard conventions/wiring etc.


---
**Chris Sader** *August 26, 2016 02:53*

Even if I soldered it in backward, could I just reverse everything I connect to the middleman board? I only got one ribbon cable connector


---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2016 03:18*

There's a notch on one side of the connector. That should be next to the square pad on the pcb, which represents pin 1. If you already soldered it, it may be hard to tell, but you can look at the pic in Oshpark. 


---
**Chris Sader** *August 26, 2016 03:30*

In that case, **+Ray Kholodovsky**​ yes, I installed it backwards. So I guess my question about reversing connections to the middleman board stands... Can I do that?


---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2016 03:35*

I'm not sure that you can.  Half of the pins are not used on the ribbon connector, and the ones that are are not symmetrical about the flip axis.


---
*Imported from [Google+](https://plus.google.com/107747838010116449754/posts/36AxL6MdKRF) &mdash; content and formatting may not be reliable*
