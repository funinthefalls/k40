---
layout: post
title: "Sorry for all the posts. I'm off this week and using the time to play around with the k40"
date: June 14, 2017 15:07
category: "Modification"
author: "Nathan Thomas"
---
Sorry for all the posts. I'm off this week and using the time to play around with the k40.



The latest project is to install a red dot laser diode. Anyone has some guidance on this? 



Not trying to do anything fancy...just a simple diode connected to a watch battery and hopefully an on/off switch. I know what I want in my head, but have no idea how to connect the wiring. Does anyone have a link to a video or write up they can post....or give guidance on how you set up your own?



Appreciate it....





**"Nathan Thomas"**

---
---
**Don Kleinschnitz Jr.** *June 14, 2017 15:35*

Usually these diodes take more than 3v (watch battery). You could put 2x watch batteries in parallel (6V not sure what the max supply voltage is). Depending on the diode forward voltage, one without an internal resistor could work at 3v with the right resistance. 



The simplest thing....

I used one from amazon that runs on 5V. I wired mine to the 5V supply and I leave it on all the time. Alternately, you can wire a switch in series with the 5V and the anode of the diode, putting it on the panel or wherever convenient!



[amazon.com - Amazon.com : Farhop 5 Volt 5mW 650nm Red Dot Laser Diode Module, 5 Pieces : Electronics](https://www.amazon.com/gp/product/B00SAEADNW/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Steve Clark** *June 14, 2017 16:00*

Here is one I did... quick and simple. Also within this post are links to styles others have come up with.



[https://plus.google.com/102499375157076031052/posts/Vd5h8KtdMtM](https://plus.google.com/102499375157076031052/posts/Vd5h8KtdMtM).



 


---
**Nathan Thomas** *June 14, 2017 16:16*

**+Don Kleinschnitz** Do you have a write up or can you show me how you wired it to the 5V supply. I'm assuming you mean you wired it somehow to the power supply of the machine?


---
**Nathan Thomas** *June 14, 2017 16:20*

**+Steve Clark** I liked this when you first posted it. Wouldn't mind doing something similar, but the wiring part is above my paygrade. I'm still trying to figure that part out, let alone the on/off switch :/


---
**Thor Johnson** *June 14, 2017 19:36*

So, this is the one I did (I have a 3D printer, so I made me a bracket).  It's always on, but when you close the lid it lifts out of the way.  But if the lid's open and you test fire, it tends to fry the diode.

On mine, the power supply had a 5V connection marked and a ground, so I tied it in there: [thingiverse.com - Laser pointer for K40 Chinese CO2 Laser by ThorMJ](https://www.thingiverse.com/thing:1002341)


---
**Don Kleinschnitz Jr.** *June 14, 2017 20:41*

**+Thor Johnson** I really liked this idea and its on my list of upgrades. 

If you have proper interlocks, the lid being up should prevent firing!!!!  :).


---
**Don Kleinschnitz Jr.** *June 14, 2017 20:49*

Here is how mine is hooked up. Mine does not have the  switch ....

Does that help?

![images/f80337ae70b90f2aa63487c7393c933a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f80337ae70b90f2aa63487c7393c933a.jpeg)


---
**Thor Johnson** *June 14, 2017 23:40*

Interlocks?  We ain't got no safety devices here... Don't look at laser with remaining eye!   ;) As long you keep your head out of the machine, your eyes are safe.  Keep hands out if you want them to be safe as well.


---
**Don Kleinschnitz Jr.** *June 15, 2017 00:05*

**+Thor Johnson** sorry but I don't recommend anyone taking chances with their eyes and encourage interlocks. Even if your a cyclops! ;)


---
**Nathan Thomas** *June 15, 2017 15:56*

**+Don Kleinschnitz** Yes sir,  thanks alot!


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/TG2aT5MVCFn) &mdash; content and formatting may not be reliable*
