---
layout: post
title: "Importing DFX into CorrelDraw (X7) Hi all"
date: June 22, 2016 10:38
category: "Software"
author: "Imnama"
---
Importing DFX into CorrelDraw (X7)



Hi all. I have a DFX file using three layers (engrave, halfway cut and full cut). If I import this file into RDWorks, I get three seperate layers, so I think the Dfx file is Ok. RDWorks does not work with my k40, so I need to use correldraw. If I import the file into correldraw, everyting is put into one big layer. The objects have a different color for the layer (red, blue and black).

So now I have to manually create three layers (red blue and black) and move the objects from the big layer into the new layers.



Question one:

Is there a way to import a Dfx in correldraw with the correct layers in place

Question two:

If not one, is there a way to sort a layer on color. Now, they appear in random order (creation order?) witch make moving them to a separate layer  a troublesome task.

Or select objects with the same color?





**"Imnama"**

---
---
**Imnama** *June 23, 2016 16:21*

I found out how to do this. In edit->find and replace -> find objects you can select objects on outline color


---
*Imported from [Google+](https://plus.google.com/105177731458209028076/posts/2dV7KwU3rwN) &mdash; content and formatting may not be reliable*
