---
layout: post
title: "Popped a fuse. Damn. Now I have to go buy a fuse >_> I haven't a clue what size this thing takes"
date: May 19, 2015 22:07
category: "Discussion"
author: "Eric Parker"
---
Popped a fuse.  Damn.  Now I have to go buy a fuse >_>  I haven't a clue what size this thing takes.





**"Eric Parker"**

---
---
**Sean Cherven** *May 19, 2015 22:10*

Do you know what caused the fuse to blow? You might wanna look into it, because it'll most likely blow again immediately.


---
**David Wakely** *May 19, 2015 22:12*

That's not a good sign for a machine only a few hours old! You should probably investigate it, sometimes the Chinese wiring can be questionable!


---
**Eric Parker** *May 20, 2015 00:25*

I do know what caused it, actually.  Completely my fault.


---
**Sean Cherven** *May 20, 2015 00:32*

Ahh gotcha. I'm not near my unit, so I can't tell you what size fuse it uses


---
**Eric Parker** *May 20, 2015 01:09*

I changed up the wiring a bit to use standard buss fuses. should work out better.


---
*Imported from [Google+](https://plus.google.com/+EricParkerX/posts/9ZjXe1UVjKo) &mdash; content and formatting may not be reliable*
