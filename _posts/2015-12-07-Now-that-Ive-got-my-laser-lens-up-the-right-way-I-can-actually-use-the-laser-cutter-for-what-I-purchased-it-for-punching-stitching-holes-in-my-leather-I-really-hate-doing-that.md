---
layout: post
title: "Now that I've got my laser lens up the right way I can actually use the laser cutter for what I purchased it for: punching stitching holes in my leather (I really hate doing that)"
date: December 07, 2015 07:22
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Now that I've got my laser lens up the right way I can actually use the laser cutter for what I purchased it for: punching stitching holes in my leather (I really hate doing that). So here is a test of it on some 2mm thick leather. 7mA @ 7mm/s Cut. I then stitched it afterwards.



Basically I created a pattern brush in Adobe Illustrator with 1mm circles, spaced apart @ 400%, add space option selected. I then just choose it as the brush for the outlines of whatever shape & voila, it creates a precise stitching line for me to stitch.



Only issue is slight amount of soot in the holes discolours the thread as I stitch. Possibly not an issue after I dye & lacquer the leather though, as the soot will be "set" into place.

![images/86d88433ab43e771a174acb22efaf592.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/86d88433ab43e771a174acb22efaf592.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**David Cook** *December 07, 2015 16:51*

Very cool!  What a difference 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 10, 2015 08:53*

Strangely, I went to use the laser cutter to test cut some kangaroo hide (0.8-1mm thick) yesterday & it wouldn't cut through on 7mA @ 7mm/s in 1 pass. Also, the cuts weren't nearly as clean as the other day.



So, I decided to check the mirrors & lens & clean them. I've realigned, cleaned, etc. Got the alignment pretty much 99.9% perfect now. Yet I still cannot cut through the kangaroo hide in 1 pass. Also, very messy burnt edges.



I then decided to recheck the 2mm leather I cut perfectly the other day. Same problem, can't cut with 1 pass & very burnt looking edges.



Anyone have any ideas what could be causing this? I don't get it...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2015 14:15*

I figured it out. Removing the main cutting board & just putting my perforated galvanised steel board in lowered the cutting area by 4mm. This in turn must have got the laser beam out of focus a bit. I just tested cutting again, with a 4mm piece of leather beneath what I want to cut. Perfect clean cuts.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XJy85S7ATqo) &mdash; content and formatting may not be reliable*
