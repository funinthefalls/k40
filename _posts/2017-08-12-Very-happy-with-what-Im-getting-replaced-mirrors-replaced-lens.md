---
layout: post
title: "Very happy with what I'm getting. ( replaced mirrors replaced lens"
date: August 12, 2017 14:19
category: "Modification"
author: "William Kearns"
---
Very happy with what I'm getting. ( replaced mirrors replaced lens. CD3 smoothie board, 

![images/0164299eaaecb805d290268e68ad1cf8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0164299eaaecb805d290268e68ad1cf8.jpeg)



**"William Kearns"**

---
---
**Ariel Yahni (UniKpty)** *August 12, 2017 14:38*

Just a little patience GnR :)


---
**Don Kleinschnitz Jr.** *August 12, 2017 17:21*

What mirrors and lenses?


---
**Kevin Lease** *August 13, 2017 01:32*

I like it, can you give some details how you did it please?


---
**William Kearns** *August 13, 2017 02:22*

Dropped it down just below the focal plane so the beam was a little wider trying to avoid lines. Then used a rub and buff wax to fill the burned part let dry little bit then buff the excess off


---
**Kevin Lease** *August 13, 2017 15:58*

**+William Kearns**silver looks higher than wood rather than filling in to be flush with wood, did you laser a stencil out of tape and then fill that in?


---
**William Kearns** *August 13, 2017 16:40*

No tape the silver is lower then the wood


---
**Ray Rivera** *August 17, 2017 15:49*

I second **+Don Kleinschnitz** 's comment, which mirrors and lens. I tracked down some MO mirrors but looking for a lens still - 2-2.5" focal length.




---
**William Kearns** *August 17, 2017 17:18*

Lens![images/35893adb050c9655bc1a09e43fbe7544.png](https://gitlab.com/funinthefalls/k40/raw/master/images/35893adb050c9655bc1a09e43fbe7544.png)


---
**William Kearns** *August 17, 2017 17:20*

Mirrors eBay and the lens focal is 2"![images/04be03fe9007a52374771f5bea760d50.png](https://gitlab.com/funinthefalls/k40/raw/master/images/04be03fe9007a52374771f5bea760d50.png)


---
**Ray Rivera** *August 17, 2017 18:44*

Those are the same MO mirrors I was looking at; thanks for the follow-up.




---
**Don Kleinschnitz Jr.** *August 17, 2017 21:29*

#K40Optics


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/AvCpnGYbuFo) &mdash; content and formatting may not be reliable*
