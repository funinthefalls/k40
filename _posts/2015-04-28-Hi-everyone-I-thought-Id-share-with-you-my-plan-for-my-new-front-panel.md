---
layout: post
title: "Hi everyone, I thought I'd share with you my plan for my new front panel"
date: April 28, 2015 13:40
category: "Repository and designs"
author: "David Wakely"
---
Hi everyone, I thought I'd share with you my plan for my new front panel. Hoping to get it cut soon in acrylic. I am going to hook up and Arduino based temp and flow monitor with an LCD and LED + Buzzer alarm for low water flow or high temps. I'll post completed pics and drawings when done!



Let me know what you think!

![images/14d654962294994b5773c019d59b2bc7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14d654962294994b5773c019d59b2bc7.jpeg)



**"David Wakely"**

---
---
**Stephane Buisson** *April 28, 2015 14:03*

Thank you so much, in the name of all the comunity !


---
**Sean Cherven** *April 28, 2015 15:13*

Oh wow! That's just awesome! Are you planning on releasing the files so other's can make one?


---
**David Wakely** *April 28, 2015 17:49*

Yep defiantly will release the files, just need to tinker with it a bit more! I'll also release the Arduino sketch too!


---
**Sean Cherven** *April 28, 2015 19:33*

Awesome! Also schematics / wiring diagrams too!


---
**sam cv** *February 08, 2018 19:56*

did you ever finish this - i would love to see how it turned out :D




---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/AWSziVwKaUV) &mdash; content and formatting may not be reliable*
