---
layout: post
title: "Anyone had a Water Error 1 on the red laser"
date: September 16, 2016 23:48
category: "Discussion"
author: "Tony Schelts"
---
Anyone had a Water Error 1 on the red laser.  Not sure what to do.





**"Tony Schelts"**

---
---
**Anthony Bolgar** *September 17, 2016 00:10*

Sounds like your flow sensor is jammed. At least that is what I could find out searching with google. ;) Not sure about it though, but I would trace out the plumbing of the water lines, look for the sensor, and take it out and check that air can blow through in at least one direction. Hope this helps.


---
**Scott Marshall** *September 17, 2016 05:36*

About the only water error you might have would be lack of flow, unless there's a low level float in the tank.



Pet peeve #82 - Error codes that mean NOTHING.


---
**Tony Schelts** *September 17, 2016 07:13*

Thanks guys


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/Y6H4gCzsWAc) &mdash; content and formatting may not be reliable*
