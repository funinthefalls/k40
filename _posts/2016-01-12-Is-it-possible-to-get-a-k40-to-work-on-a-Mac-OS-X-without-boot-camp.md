---
layout: post
title: "Is it possible to get a k40 to work on a Mac OS X without boot camp"
date: January 12, 2016 17:26
category: "Modification"
author: "3D Laser"
---
Is it possible to get a k40 to work on a Mac OS X without boot camp 





**"3D Laser"**

---
---
**Stephane Buisson** *January 12, 2016 17:29*

**+Corey Budwine**  I am on Mac, well I make a stone 2 birds, I mod the board and went for open sources, and get rid of all trouble at once. see K40+Smoothie=SmoothK40


---
**3D Laser** *January 12, 2016 17:31*

I keep hearing about modding but don't know where to begin I am about to buy a 360 dollar eBay special but I am not for sure what board it will come with 


---
**Stephane Buisson** *January 12, 2016 17:34*

get your K40 (cheapest is best before chineese new year), you will be able to mod a bit later. (about 150USD)

**+Peter van der Walt**  will come with a new board soon, a bit of patience ... (another Smoothie compatible)


---
**3D Laser** *January 12, 2016 17:35*

What is a smoothie board? And how much is it


---
**Stephane Buisson** *January 12, 2016 17:44*

Smoothie is a firmware made by arthur Wolf. it work on board based with lpc1769 (more modern and powerfull than Ramps/arduino solution). You can support **+Arthur Wolf** by buying from link on [http://smoothieware.org/](http://smoothieware.org/) (USA or EU).

other board are also compatible under different brand...



when mod is done well you have a open source K40 with a usb (possibly ethernet) with no more ties with chinese softwares. and open source is fantastic, with community in english support.


---
**3D Laser** *January 12, 2016 17:50*

Would I need a new board then?  I'm not that great at electronics I don't know how well I would trust myself to rewire the machine


---
**Stephane Buisson** *January 12, 2016 17:54*

well it's not complicated, if you are not sure get some help.

it's just a board exchange, nothing "electronic", maybe some connectors to solder at max. if you wait for **+Peter van der Walt**  I am sure you will see a tutorial comming.


---
**Stephane Buisson** *January 12, 2016 18:01*

forgot to mention, the new K40, had some mods made (air assist, laser pointer), but are still with proprietary electronic and software. not a lot for the price difference, and board mod still to do.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/CdNfvWYtTNX) &mdash; content and formatting may not be reliable*
