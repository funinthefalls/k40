---
layout: post
title: "Peter van der Walt Is there a preset for K40 for LaserWeb?"
date: February 08, 2016 01:13
category: "Smoothieboard Modification"
author: "Arestotle Thapa"
---
**+Peter van der Walt** Is there a preset for K40 for LaserWeb? Specially to connect through LAN? Thank you





**"Arestotle Thapa"**

---
---
**Ariel Yahni (UniKpty)** *February 08, 2016 02:21*

**+Arestotle Thapa**​ so you upgraded to smoothie. There a no presets yet. Test the connection via USB laserweb should recognize the board and you are set. Interesting will be the answer to connect via LAN


---
**Arestotle Thapa** *February 08, 2016 03:01*

**+Ariel Yahni** I'm trying smoothie. I'll test with USB.


---
*Imported from [Google+](https://plus.google.com/104400841682788049551/posts/Kg1CJksmpjS) &mdash; content and formatting may not be reliable*
