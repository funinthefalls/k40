---
layout: post
title: "Thanks Scott Thorne for the motivation and tech tip"
date: November 01, 2016 23:27
category: "Object produced with laser"
author: "Bob Damato"
---
Thanks **+Scott Thorne** for the motivation and tech tip. I did find that 600dpi on granite came out better than 300dpi though.  Thats me on the right.

![images/cc8bc2002c6dc273eaa94caaf40311ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc8bc2002c6dc273eaa94caaf40311ac.jpeg)



**"Bob Damato"**

---
---
**Scott Thorne** *November 01, 2016 23:47*

Great job...glad i could help


---
**Scott Thorne** *November 01, 2016 23:58*

I've never tried granite.


---
**Nick Williams** *November 02, 2016 00:56*

Hey Bob what controller are you using??


---
**Bob Damato** *November 02, 2016 07:07*

Hi **+Nick Williams**  Im using stock everything and Corelx8.  I wanted to get the most out of it and find its limitations before upgrading.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/LYm3TpaTo2P) &mdash; content and formatting may not be reliable*
