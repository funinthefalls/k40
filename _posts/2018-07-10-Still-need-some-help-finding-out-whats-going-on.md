---
layout: post
title: "Still need some help finding out what's going on ..."
date: July 10, 2018 10:25
category: "Original software and hardware issues"
author: "Manon Joliton"
---
Still need some help finding out what's going on ...

Here is what happens, it's supposed to be 2 circles and one rectangle but their shapes are just fucked up and idk why the laser kind of stops cutting sometimes

Can you please help ^^'

Thank you


**Video content missing for image https://lh3.googleusercontent.com/-Q5r-A9VwFXg/W0SJlJDwfiI/AAAAAAAAAO8/gmlfa0qVuk07o87PsZLSRj6AOKx0mxtsQCJoC/s0/gplus699657162.mp4**
![images/a449d4ee39a189a82e1bebd42c10fd22.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a449d4ee39a189a82e1bebd42c10fd22.jpeg)



**"Manon Joliton"**

---
---
**Don Kleinschnitz Jr.** *July 10, 2018 12:39*

Lots wrong here...



Did you follow the earlier advice to do a complete alignment?



It seems that the mechs are struggling and that the optics do not stay in alignment across the beds area. 



Tell us something about this machines configuration and the software you are using.



I detect (from video) 2 areas of problem:



<b>The drive system is not running smoothly:</b>

Potential causes:

..Belt alignment or tension

..Slipping pulleys on steppers

..Broken or worn belts/bearings

..Dirty or damaged gantry surfaces, wheels or bearings.

Try:

Check the gantry movement without poser by manually moving it and feeling for looseness, binding, grinding or lack of smooth motion.



<b>The optical system is misaligned/damaged:</b>

Potential causes:

..Reflections in the head caused by bad/misaligned or damaged lens

..Alignment changes at different bed positions caused by misaligned mirrors, and objective lens.

Try:

Complete alignment

Remove and inspect objective lens






---
**Manon Joliton** *July 10, 2018 13:54*

**+Don Kleinschnitz** I checked the alignement, it is good, the mirrors and lens looks great too.

What do you mean by 'check the gantry movement without poser' ? I'm not used to the laser part terms ^^ what do you call the poser ? the gantry ?

Thanks in advance ^^




---
**Don Kleinschnitz Jr.** *July 10, 2018 15:42*

**+Manon Joliton** 

<i>poser</i> should have been = power :) 

The <i>gantry</i> is the x/y movable components of the machine.

We are trying to find something wrong with the mechanical drive system by manually moving it with power off.



Can you post the burned targets that you used when you did the alignment.


---
**Manon Joliton** *July 11, 2018 10:40*

Hi ! It doesn't seem to be a reflection problem anymore, I managed to coincide both up and down spot in the center of the 2nd mirror (i didn't know that i could move the mirror to the right or th e left by moving the whole support so the two spots coincided at the extreme right of the mirror, thats probably what caused the reflection, but i still have trouble to find out what change the shape, it doesn't seems to be a belt tension problem has i changed it and the problem doesn't disappear, but i noticed some place on the y axis where the head seems to block (see picture) does it give you any sign to find where the problem is from ? Thank you ^^


---
**Manon Joliton** *July 11, 2018 10:50*

The lines are where it seems to 'block' and i circled some spots on the rectangle cause it's easier to see them on straight lines, those spots are where the laser blocks, it's like if the machine has to make a really little loop (half a millimeter) and it shifts the whole drawing![images/44103b5cc5464b6f090a2c3a838a02e3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44103b5cc5464b6f090a2c3a838a02e3.jpeg)


---
**Don Kleinschnitz Jr.** *July 11, 2018 12:19*

**+Manon Joliton** 

Still do not know what the configuration of your machine is?

Software?

Controller?



I am bothered by the whistling noise that is in the video any idea what that is?



Reading back through your posts this machine has had lots of mechanical problems so my guess is that the drive system still has something wrong with it. Why the laser comes on and off seems to be a separate problem. Lets see if we can rationalize the drive system first. My guess is that something in the drive is slipping.



<b>Test #1</b>

Lets reduce the test to something simple and see if that gives us a hint. 

Lets draw a simple square and draw it both in the upper left and also in the middle of the engraving area.

Please take a video of that test and post it.








---
**Manon Joliton** *July 11, 2018 13:49*

Kleinschnitz if by software you mean what i use on my ccomputer i use CorelLaser (sold with my k40) the controller is the more complete one, switch for light, switch for power, +-10 +-1 +-0,1 buttons, laser test button etc maybe you need a pic ?

For the test, i have to cut 2 square at the top left and in the center of the cutting area right ?


---
**Don Kleinschnitz Jr.** *July 11, 2018 17:29*

**+Manon Joliton** 

Yes cut two squares one in upper left and one in the center of the bed.


---
**Manon Joliton** *July 12, 2018 10:47*

**+Don Kleinschnitz** I can't post it in a comment, should I post it as a new post ?

it's supposed to be 2 square of 30mm but it's actually 2 rectangles of 30*28mm for the top left one and 30*28.5mm for the center one

What does it mean ?

Thank you for spending your tile on this problem, sorry if i take time before answering...


---
**Don Kleinschnitz Jr.** *July 12, 2018 12:52*

If you "reply" do you not get a camera icon to click on to link to a picture?

Alternately put it on a cloud somewhere and post the link...


---
**Manon Joliton** *July 12, 2018 13:05*

[https://drive.google.com/file/d/1GXB5rebbYTdZJ3CdfAVTamiBLyEUb_se/view?usp=drivesdk](https://drive.google.com/file/d/1GXB5rebbYTdZJ3CdfAVTamiBLyEUb_se/view?usp=drivesdk)

[drive.google.com - k40laserproblem.mp4](https://drive.google.com/file/d/1GXB5rebbYTdZJ3CdfAVTamiBLyEUb_se/view?usp=drivesdk)


---
**Don Kleinschnitz Jr.** *July 12, 2018 13:40*

**+Manon Joliton** 

It looks to me like the X axis is working correctly.

As the head makes the Y down move it whistles, moves and then gets a small X offset that is then corrected near the end of the upward Y stroke. 

1.) Has the machine always whistled? Can you tell where it is coming from?

2.) Are you sure that the Y error is not in the source file?



I cannot run your file as I have a modified machine.



**+Ned Hill** you have a stock machine. Can you send **+Manon Joliton** a 30x30 mm set of boxes like in the video above so we can see if the problem is in the software or mechs???


---
**Manon Joliton** *July 12, 2018 13:52*

It has whistled since 3-4month, can't tell why, seems to come from behind the machine or from the box in the right compartment (can't tell how it's called sorry >_<)

this 'shifting' problem happened to me 5month ago and I just had to change the mainboard name in the parameters in CorelLaser, do you want a pic of those parameters ?


---
**Don Kleinschnitz Jr.** *July 12, 2018 16:41*

**+Manon Joliton** 



Yes lets post a picture of those parameters.

I cannot help with them but I can find help within this community to insure they are right.



Please also post a  picture of the box you think is making the whistle.



+HalfNormal can you help verify config for stock machine?


---
**Ned Hill** *July 12, 2018 18:01*

**+Don Kleinschnitz** **+Manon Joliton**

I can help with the config parameters as well.  



I'm using a current version of coreldraw and not the old stock version that comes with the machines, so I'm not sure how well it would work.  Maybe I can export it as a Jpeg and he can import it in and see if that make a difference.



That little X jog is weird.  I once had a problem with circles not quite matching up from beginning to end but that ended up being a belt tension issue.  What output format are you using for cutting?  What happens if you change the output format?  Output formats are under "Coreldraw settings" and is listed as "Cutting Data".  Nominally it should be  WMF.  You could also try EMF or JPG.



Also, in case you didn't already know, you can get rid of the double cutting by setting the cut line widths to 0.01mm or less.  If the line widths are larger it cuts both sides of the line, hence the double cut.






---
**Manon Joliton** *July 13, 2018 10:00*

The cutting data setting is WMF, here are my engraving/cutting properties, last time I had this kind of problem, I switched my mainboard from M1 to M2 and it worked, this time, I tried all the mainboard but the M2 is still the better (less worth) one![images/8e4176965bdb3c5d98d66b4bfcbebbfe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e4176965bdb3c5d98d66b4bfcbebbfe.jpeg)


---
**Manon Joliton** *July 13, 2018 10:02*

And this is where I think the noise come from, it seems to happens when the DEL lights on (so when the laser is working it's logic....)![images/71450e1f3a4cf2f910cde2c98e1d8265.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/71450e1f3a4cf2f910cde2c98e1d8265.jpeg)


---
**Don Kleinschnitz Jr.** *July 13, 2018 13:03*

**+Manon Joliton** 

DEL lights? What is DEL.



So here is a possibility to consider: 

...The Laser Power Supply (LPS) is arcing internally when the laser starts up.

...This arc is creating electrical noise that is causing movement errors in the controller ...



Try this at night: 

.....open the right cabinet and with the lights in the room off and the cabinet open, run the job.

<b>See if there is arcing (lightning) coming from inside the LPS?</b>



<b>DO NOT OPEN THE SUPPLY NOR GET ANY PART OF YOUR BODY WITHIN 50mm of the LPS while it is running.</b>



An arching LPS is a common and frequent failure with these machines. 


---
**Manon Joliton** *July 13, 2018 13:14*

I meant LED (DEL is the french word sorry)

there is no lightning inside the right cabinet, the LPS is what i called the little box ?

Anyway, there is no light/lightning coming frol there


---
**Don Kleinschnitz Jr.** *July 13, 2018 13:25*

**+Manon Joliton** 

Ok you may not be able to see the arching.



Try this:

Turn the lasers power to a low setting. Preferably so it just barely marks paper, run the same test job. 

We are trying to see if the movement error is caused by the LPS.

Listen to see if the whistling noise is less or gone?

Does the movement error still happen?


---
**Manon Joliton** *July 13, 2018 14:22*

During the video, i'm cutting paper so it's only a 11.5% power

It's whistling the same way at 40% and at 11%

Weird thing, at 11.5 the paper is cut, at 11.4, the paper isn't even marked


---
**Don Kleinschnitz Jr.** *July 13, 2018 15:26*

Trying to figure a way to elliminate the LPS as the source of the problem.

Since this started have you had to increase power to get it to cut as you did before?


---
**Manon Joliton** *July 13, 2018 15:33*

**+Don Kleinschnitz** yes, i had some power loss but I just had to check the alignment and then it get better, think it's just about rhe alignment


---
**Ned Hill** *July 13, 2018 15:58*

**+Manon Joliton** if the power setting is too low the tube will not ionize.  Hence why it cuts out at 11.4%.


---
**Manon Joliton** *July 13, 2018 16:11*

**+Ned Hill** ok so it's normal if it seems not to work at all at 11.4 but works perfectly at 11.5 ?

Also, is it normal that when it cuts, it takes little time to reach it's maximal cutting power ? Like on 3 cm it cuts at low power abd for the rest of the shape, it goes with a more powerful cutting


---
**Ned Hill** *July 13, 2018 16:20*

**+Manon Joliton** Yes, there is a lower power threshold were the laser will not ionize.  The exact number depends on the LPS and the Tube for each machine.  If you are running the laser at the edge of ionization the laser output can fluctuate.  So it's not recommended to run that low.



Also, I looked at your machine parameters and the only thing I see is that, typically, the thread priority is set to "lower" with the "anti-disturb" checked off.  I don't think it will make a lot of difference, but those are the normally recommended settings.


---
**Manon Joliton** *July 13, 2018 16:25*

Ok, i'll change that tomorrow but I don't think it would change anything ^^

What speed do you usually use ? Cause when I used it the first time it was set to 500mm/s and it seems very high to me but as it was working i didn't change it


---
**Ned Hill** *July 13, 2018 17:12*

The max speed  is fine at 500mm/s.  That fast is only used for large engraving areas.  Some people would say that the motors are only rated for 350mm/s but I haven't had any problems personally running up to 550 mm/s.  For cutting, the speeds are usually between 4-25mm/s.  For engraving I typically run anywhere from 300-500 mm/s.


---
**Manon Joliton** *July 13, 2018 17:15*

Ok, but cutting at 500 or 250 isn't a bad thing for the machine right ?

'Cause I usually cut my veneer wood at 16%- 18% power and 250mm/s and if i make it slower it would burn the veneer


---
**Ned Hill** *July 13, 2018 17:27*

Typically the biggest issue at faster cutting speeds is that you get momentum bounces of the head as it changes directions which can mess up the cut line.  It can also cause more wear and tear to the machine. I typically cut wood veneer at 15% power and 15 mm/s without problems.  Of course my tube is old and is less powerful than new tubes.  Your problem with burning is more related to the fact that you aren't using an air assist to knock down flames.


---
**Don Kleinschnitz Jr.** *July 13, 2018 21:56*

**+Manon Joliton** **+Ned Hill**



<b>Warning:</b> this will make your head hurt :(....



Sometimes it helps to look closely at the image to see if it is telling us something.

Don't know if it will help but I slowed down the video and created a pictorial story board for the movement which is attached below.



I did not at first notice that the squares are drawn in two different rotations which makes the drawing error look different but it is actually not.



<b>Observations:</b>

...The Y stroke is offset in the -x direction only during a vertical stroke

...The -X direction error always occurs on a downward Y stroke

...On the upward Y stroke [after a -X error] the error is fixed after the Y stroke starts

...The error initially occurs after the first Y stroke.

...The error does not occur during a change in direction

...There is a pause when the error occurs

...The whistling noise only occurs on Y strokes.

...The amount and position of the error is the same in the CW and CCW direction of drawing. 

...The amount and position of the error and correction is the same on both of the squares drawn irrespective of the position on the bed.

...The error is repeatable at any location on the bed with two different drawing objects.

...The error is only in the -X direction

...Something is smart enough to correct the error on the subsequent Y move.

...The error is repeatable and predictable.



Since the error is repeatable and then it gets corrected mid Y stroke I don't think this particular problem is caused by:

...LPS NOISE 

...Mechanical error

...Optical errors or alignment



<b>These seem more likely:</b>

...This error is in the source file

...There is a bug in the firmware

...The controller is not working properly (low probability)



Does this look right and make sense?



Its hard to imagine what is causing a Y move to pause and then move in the -X direction without it being told to do so.



**+Ned Hill** can we get **+Manon Joliton** a known good source file?

![images/5389a24184d152618fae9de013c4a2f4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5389a24184d152618fae9de013c4a2f4.jpeg)


---
**Manon Joliton** *July 14, 2018 03:00*

**+Don Kleinschnitz** yeah this sums up well the situation, except sometimes (under 10% of the time) the problem disappear (for the exact same shape in the same series) I think it depends of if it starts with a vertical or horizontal movement or maybe if it starts at the middle of a side idk


---
**Don Kleinschnitz Jr.** *July 14, 2018 12:03*

**+Manon Joliton** I think our next step is to somehow verify that the source file you are using does not contain these errors.


---
**Manon Joliton** *July 14, 2018 12:12*

**+Don Kleinschnitz** ok, how shoul i proceed ?

About this, once I was making some marquetry with the laser for jewelry and in the design, the laser used to cut lines that i didn't create on the corellaser file![images/9d921479c2fc448f90876e5aef52aa38.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9d921479c2fc448f90876e5aef52aa38.jpeg)


---
**Don Kleinschnitz Jr.** *July 14, 2018 14:00*

We need to find someone with a stock K40 that can send you a corel test file that works. Then you can try it to see if its your machine or your source creation. Unfortunately I cannot since my machine is modified. 

Another thing you can try is to use K40 Whisperer to drive your machine and see if a simple box works properly using it.

[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html#download)


---
**Ned Hill** *July 14, 2018 14:07*

He can send me the file with the squares and I can take a look at it. If I don’t see anything I can convert it to jpg and send it back to see if it makes.difference. . 


---
**Manon Joliton** *July 14, 2018 14:27*

I tried to do a 30mm square using k40 whisperer but I still have the same 'loops' and my square remains as a rectangle :3


---
**Don Kleinschnitz Jr.** *July 14, 2018 20:24*

**+Manon Joliton** 

What do you mean by "loops". Does the squares marking look exactly like the one your posted in the video?



Please send +Ned Hill your squares file and lets see what that does however I am going to guess it will mark properly.



Looking back over your posts you posted:

<i>"Yesterday, i checked the tension of the left and right 'roll' (i don't know how it's called?), could you help me find out what is happening ?</i>

<i>I noticed the rotary pole in the front of the machine   didn't turn 'round' because of some screw that were missing on what i think is a motor ? (The little   black cube that make the rotary pole rotate)</i>

<i>so i fixed the screws but it still doesn't work..."</i>



Can you post pictures of this shaft and its motor and coupler?



In the X dimension. Have you checked the belt tension and the shafts coupling to the x stepper motor? Similiar to what you did on the Y?



Here is an idea for some more tests while we are waiting for **+Ned Hill** results.



Horizontal draw and Y move tests:

Mark a horizontal line 4 inches long in the x direction.

Space down 2 inches in the Y direction and mark another 4 inch line

Space down 2 inches in the Y direction and mark another 4 inch line



Vertical draw and X move test.

Mark a 4 inch veritcal line at the left edge

Move 2 inches in the Y and mark antoher vertical line 



Post videos of each pls.



Still looking for clues to what is happening :(!




---
**Manon Joliton** *July 16, 2018 11:35*

**+Don Kleinschnitz** this is what wasn't fixed well last week so I screw it back

This motor make the right and left belt to turn and before I fixed it, the shaft didn't turn round but had a twisted movement

I'm gonna post the vids as i did last time and share you the link



Also, how should I sent the square file to Ned ?

Sorry, I saw your post th is morning, idk why i didn't receive any notification about it![images/8b953d5c6db094e4caa8515ada0650c7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8b953d5c6db094e4caa8515ada0650c7.jpeg)


---
**Manon Joliton** *July 16, 2018 12:00*

link to the squares file : 



[drive.google.com - squares.cdr](https://drive.google.com/open?id=11nx4wU7J3hmu78xf8Esvr_LCHSCQQ96U)



link to the vids : 



[https://drive.google.com/open?id=1N45ADKXXyC0Xdd6FAPXXMu4JLSVv2eCW](https://drive.google.com/open?id=1N45ADKXXyC0Xdd6FAPXXMu4JLSVv2eCW)



[https://drive.google.com/open?id=119c9AZxc3GO4X4G47GmZA_Ye4acKPmB2](https://drive.google.com/open?id=119c9AZxc3GO4X4G47GmZA_Ye4acKPmB2)


---
**Don Kleinschnitz Jr.** *July 16, 2018 13:43*

**+Manon Joliton** 

So the last video verifies what we already mostly knew:

...X moves correctly when there is no Y movement.

...Y moves a distance <i>A</i> and then X shifts to the left. The gantry pauses during this time. 

...The distance <i>A</i> seems to always be the same and occurs only after the start of a Y move.

...The error is repeatable

...The machine always whistles during Y moves [this continues to bother me and feels like a hint <b>]</b>



<b>*So what could cause X to move a short delay after Y moves? What things are in common</b>

.....

<b>X Cables loose:?</b> Have you inspected the cables going to the X motor

<b>Power supply droop:?</b> Do you have and can you use a digital multimeter? If so we may want to measure the 24v to see if it is drooping. 

<b>24V power supply not operating properly</b> 



<b>Something weird with the Y drive:?</b> I am only going here because this is one of the things that's been changed. 



<i>Check to see if the Y coupler is moving</i>

...With a "sharpie" mark as thin a line as possible on the Y shaft near the coupler. Also make a line on the coupler lined up with the mark on the Y shaft.

Do this on both sides [the Y shaft and the motor shaft] of the coupler. After running the square jobs a few times inspect the coupler and shaft and see if there is any signs of movement [i.e. the marks do not line up].



<b>Isolate the source of whistling during Y moves.</b>

...Unplug the steppers one at a time, run the job and see if whistling stops



...You could also disconnect the Y shaft and see if the whistling stops when you run the job. This would suggest the 24V supply is having a problem on Y moves.



Uggh .....some of these tests are stabs in the dark. Hopefully soon we will find a test that tells us enough to find the source of this problem. 




---
**Manon Joliton** *July 16, 2018 14:17*

- the x cables (a white ribbon of cable ?) looks ok, but it has been bent a little (see pic in next post)

- i don't think i have a multimeter

- what do you call coupler ? The Y shaft is what i pictured this morning right ?

-what do you call steppers ?


---
**Manon Joliton** *July 16, 2018 14:18*

This is the x cable, it has been a little bent![images/ccbce11862fbc8b16862f1bb03edd5cc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ccbce11862fbc8b16862f1bb03edd5cc.jpeg)


---
**Manon Joliton** *July 16, 2018 14:38*

**+Don Kleinschnitz** 

I think I noticed something new :

- the whistle happens even when the laser is off (so it's linked with the movement) i think we knew this but i confirm

-the shift/loop that occur during a Y movement is caused (or at least linked) because the left part and right belt aren't coordonated

during half a second, the x axis isn't horizontal but oblique



[https://drive.google.com/file/d/1e4q7LDz2S9kXKFdT3G4IVyi559kf9vNg/view?usp=sharing](https://drive.google.com/file/d/1e4q7LDz2S9kXKFdT3G4IVyi559kf9vNg/view?usp=sharing)


---
**Manon Joliton** *July 19, 2018 09:16*

**+Don Kleinschnitz** I found out that a too strong belt tension could cause noises and problem with cutting, I don't think mines are too strong but I had to screw it to make it stronger in the first days I had my machine cause if I didn't, the right end of the X axis would tuch the Y right part that cover the Y belts

I thought it was just a belt tension problem at this time and it seemed to work better after I strengthened the tension but maybe it was not something normal ?


---
**Manon Joliton** *July 19, 2018 11:22*

**+Don Kleinschnitz** the problem came from the shaft, the screws that hold the left shaft and the right in one shaft won't stop unscrewing, causing the left shaft to go slower than the right

I screw it again (for the 3rd tile in 2 days) and it worked



But now i'm looking for something that would hold bo th part of the shaft in one (and that don't need to be screwed back every 10min ^^ )



thank you very much **+Don Kleinschnitz** and **+Ned Hill** 


---
**Don Kleinschnitz Jr.** *July 19, 2018 11:58*

**+Manon Joliton** 

<b>Whoa are you saying you found the problem?</b>



The Y shaft and coupling are shifting?



Set screws in flats on the shafts for that application are the best.

Loosen the screws and pull the coupling back. Are there flats on the motor and Y shafts. I have never taken mine apart.

If there are flats then make sure the screws in the coupling are engaging the flats on the shafts.

Alternately you can get various set screws here:

[mcmaster.com - McMaster-Carr](https://www.mcmaster.com/#set-screws/=1ds2zeh)



I would try "cup point" or "nylon tip" set screws. I do not know the size.

Nylon will not mar the shaft but may not hold as good.

Cup point will hold but may mar the shaft.

There are various types of point on these screws.



Might be good to get a few types to try. 



Let us know what works so we can share in this community.



After you get this working reliably I would re-tension the belts to see if you can reduce the Y noise. That kind of noise usually means the stepper motor is under stress.










---
**Eric Lovejoy** *July 21, 2018 13:55*

looks like your cutting platform is not aligned, or your laser  is out of focus at certain points. 



I would go get a small level, torpedo, or something like that, and make sure the platform is level. 



The odd shapes, suggest the drive motors are not functioning properly. it could be the motors, or the control board, but the armature still moves on the X,Y, axis, so that might not be it.  



The problem could be that the belts are loose, or that the belts need adjustment. 



my laser has screw holes around the case that I can poke an allen wrench into and make belt adjustments. maybe yours does too.  check for loose belts, slipping, and make sure the armature is secured to the belts. 




---
**Eric Lovejoy** *July 21, 2018 13:59*

Another tip: get some scotch tape,  and stick it to the inside wall of your laser. Then, test to find the focal points, at 0,0 0,100, and 0,300 or something like that, points along the Y axis. Transfer those points to the tape, then using a ruler, make a straight line on the tape. you Will then be able to see where you need to adjust your bed, to get items placed at the focal point of your laser.  no more guessing.


---
*Imported from [Google+](https://plus.google.com/110371358543635278419/posts/3SEZ3DXszCp) &mdash; content and formatting may not be reliable*
