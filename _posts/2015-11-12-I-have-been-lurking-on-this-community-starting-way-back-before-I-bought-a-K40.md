---
layout: post
title: "I have been lurking on this community starting way back before I bought a K40"
date: November 12, 2015 16:12
category: "Discussion"
author: "Warren Gamas"
---
I have been lurking on this community starting way back before I bought a  K40. I really appreciate the community here for all the information.  I did want to share back a litter regarding my experience to date. I was pleased with how quickly my laser arrived after the ebay purchase.  The box was in great shape as was the laser. After aligning the laser, I managed to complete a couple projects before it quit working.  I contacted the sellers through ebay.  They asked for pictures and video of the issue  Here is the video of the issue.  I opened up the power supply and found that one of the resisters (R24) was not soldered to the board. I sent them pictures of this as well. They promised to send a replacement power supply which arrived this week.  Yesterday, I installed the new power supply and I am happy to report that I am lasing about again!  Thank you all for sharing your experiences and solutions in this community!





**"Warren Gamas"**

---
---
**Scott Thorne** *November 12, 2015 16:37*

 I guess you are one of the lucky ones that got your broken stuff replaced for free, my tube was broken, and I still have not received a new one from technology etrade....stay away from this company...also the power supply was damaged, all together cost me 375 extra just to get it working.


---
**Ruwan Janapriya** *November 13, 2015 01:36*

**+Warren Gamas** do you mind sharing the link for the seller? 

Also **+Scott Thorne** could you do the same please? :-)


---
**Scott Thorne** *November 13, 2015 01:40*

 Sure thing.


---
**Warren Gamas** *November 13, 2015 01:43*

I purchased this laser....



[http://www.ebay.com/itm/40W-LASER-ENGRAVER-ENGRAVING-MACHINE-CO2-GAS-CUTTER-CARVING-STRONG-PACKING-/171884351947?ssPageName=ADME:X:RTQ:US:1123](http://www.ebay.com/itm/40W-LASER-ENGRAVER-ENGRAVING-MACHINE-CO2-GAS-CUTTER-CARVING-STRONG-PACKING-/171884351947?ssPageName=ADME:X:RTQ:US:1123)



from supply2worldwide



We used email to communicate (through eBay message) the issues.  It wasn't fast since their work day was my bedtime but we managed. In the end, I have been pleased with their communication and resolution to my issue. 


---
**Scott Thorne** *November 13, 2015 01:43*

The seller was technology etrade....I have been back and forth with them for a month and they begged me to keep the machine and promised me that they would get me another tube on the way...now I get no response at all from them.


---
**Warren Gamas** *November 13, 2015 01:44*

Sorry to hear that Scott... I had a couple sleepless nights myself when I was considering what I would do if I had your issue.


---
**Scott Thorne** *November 13, 2015 01:47*

Well, I had to buy a tube and a power supply out of pocket, I don't regret it because it actually does very good work, but I wish the company would stand up for the product it sells and make it good.


---
**Ruwan Janapriya** *November 13, 2015 01:49*

Thanks Warren and Scott. 

I believe this is the link for Scott's seller: [http://www.ebay.com/itm/40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-High-Precise-/141690337410](http://www.ebay.com/itm/40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-High-Precise-/141690337410)


---
**Scott Thorne** *November 13, 2015 02:00*

You are right, I was looking for it, but couldn't find it, stay away at all cost....thanks Ruwan.


---
**Scott Thorne** *November 13, 2015 12:50*

As fate would have it, I get an email this morning from technology etrade stating than a new tube had been sent with a tracking number, maybe I jumped the gun on selling them short.


---
**Scott Thorne** *November 13, 2015 14:30*

That could be...lol


---
**Ruwan Janapriya** *November 13, 2015 15:45*

That is great news Scott!


---
*Imported from [Google+](https://plus.google.com/+WarrenGamas/posts/QRxV7BTkP5g) &mdash; content and formatting may not be reliable*
