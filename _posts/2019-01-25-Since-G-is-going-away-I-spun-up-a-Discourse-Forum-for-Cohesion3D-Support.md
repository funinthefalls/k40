---
layout: post
title: "Since G+ is going away, I spun up a Discourse Forum for Cohesion3D Support"
date: January 25, 2019 02:53
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Since G+ is going away, I spun up a Discourse Forum for Cohesion3D Support.  There's a general "laser talk" section that you all are welcome at.  It's still empty, but I hope you'll help me fill it up: 



[https://forum.cohesion3d.com/c/laser-talk](https://forum.cohesion3d.com/c/laser-talk)





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Stephane Buisson** *January 25, 2019 09:00*

Discourse is indeed a very good solution, my only points for our present community is the hosting cost (linux 64 bit + docker), and the loss of exposure when leaving Google. Certainly the best option around for business like yours or Lightburn. Well done **+Ray Kholodovsky**


---
**HalfNormal** *January 25, 2019 16:28*

**+Stephane Buisson** when setting up any site, you set up your robots.txt so that it is crawled by all the search engines. You do not lose out on what G+ does for search.


---
**James Rivera** *January 26, 2019 05:03*

**+Ray Kholodovsky** I tried to login to the root website ([cohesion3d.com](http://cohesion3d.com), not this forum) and it didn't recognize my username+password, so I created a new one. Then I realized my order history was on my old account (I bought a C3D mini K40 upgrade kit). Did I just overwrite my account?  


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2019 05:56*

Separate matters: 

We deployed a new site in the last few months. All the old accounts and order history did not transfer over. 



I have order records and you should have confirmation emails should you need anything. 



The account for the forum is separate from the account for the new website (different servers, etc.)


---
**Jamie Richards** *January 27, 2019 21:50*

Discourse is a good alternative, and there's a mobile app!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/TSAo9ppefJx) &mdash; content and formatting may not be reliable*
