---
layout: post
title: "A quick mock up of the air assist system for my K40 and Redsail LE400 lasers"
date: December 19, 2016 01:35
category: "Modification"
author: "Anthony Bolgar"
---
A quick mock up of the air assist system for my K40 and Redsail LE400 lasers. It is controlled by a 12V solenoid (So I can turn it on/off with Gcode), and has an inline filter to reduce the risk of oils contaminating the material being cut or engraved, regulator, and pressure gauge. I am making the switch to running shop air from my big compressor, instead of an airbrush pump on each machine. I bought a 3 outlet airline kit from Lowes to get the shop air to the machines, as well as having an extra outlet available for air tools, etc.



![images/be160c1ae01d099bbd2fc8c4ec7d8542.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be160c1ae01d099bbd2fc8c4ec7d8542.jpeg)
![images/44f14a0fc741536e2896d9a3245c2b5a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44f14a0fc741536e2896d9a3245c2b5a.jpeg)
![images/9aa46d1844e6a36b06d3c19da528b8be.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9aa46d1844e6a36b06d3c19da528b8be.jpeg)

**"Anthony Bolgar"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 19, 2016 02:42*

I've seen many users posting about using shop air via the "big compressor". I'm curious how large is the big compressor & it's tank capacity?


---
**Ned Hill** *December 19, 2016 03:21*

In my outside shop I have a 33 gal (125L) tank with 1.7hp compressor.  Haven't wanted to go through the hassle of running a line inside to my study where my laser is so I've just been using my airbrush compressor.  One day I'll probably make the switch and do what Anthony is doing.


---
**Phillip Conroy** *December 19, 2016 03:45*

Email me if you get water on focal lens and have to clean it every 20 minutes  of cutting/etching like I had to do when I first started using shop compressor. No amount of water traps with adjustable  air pressure valve or just the water traps alone will remove the 0.05% of the water moisture that was making it to my focal lens passed 3 water traps.i ended up making a desiccant  cystal water trap just before the focal lens . nebula98@[dodo.com.au](http://dodo.com.au) 


---
**Roberto Fernandez** *December 19, 2016 06:39*

**+Phillip Conroy** a silicagel filter is the solution.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/4YRMj84Sa72) &mdash; content and formatting may not be reliable*
