---
layout: post
title: "Hi, I'm owner of this little blue machine"
date: November 19, 2018 12:44
category: "Smoothieboard Modification"
author: "Fran Lozano"
---
Hi, I'm owner of this little blue machine. I'm upgrading the controller board and I found the "system" pulse per inch that improve quality instead of pwm.



The problem is that I couldn't find a firmware which incorporates a ppi system.  The closer I could find is the M4 code in grbl 1.1 to engrave and cut. The rest, as I understand, are different systems  programmed to use ppi and they connect to the main board ( a cohesion3D, a smoothieboard, or other type of controling boards).



Is all of this correct? Or do you know any firmware with ppi implemented and it can be programmed in a board such as cohesion3D?



Thanks a lot





**"Fran Lozano"**

---
---
**HalfNormal** *November 19, 2018 12:59*

Somewhere in this forum, someone cobbled together a way to do ppi by reading the steppers with an arduino. Not sure if anyone has incorporated this method in hardware/software.


---
**HalfNormal** *November 19, 2018 13:01*

Found it!

[plus.google.com - Hi, this is my K40, I hope you'll like it ;-) Detailled photo of the peltier ...](https://plus.google.com/107809295738006995715/posts/EFNRb29mxRU)


---
**Fran Lozano** *November 19, 2018 16:24*

Hi HalfNormal, thank you for your help. I already know Joachim works but I am saerching something more developed, something integrate in grbl or similar firmware. Finally I buy a DSP chinese controller. It seems allow ppi control.


---
**HalfNormal** *November 19, 2018 16:30*

If you are using a DSP controller, you should look into trying LightBurn software.


---
**Fran Lozano** *November 19, 2018 16:38*

I found this software in Choesion3D website and it looks good, surely better than coreldraw :D!

Now I am investigate wich dsp buy, E5 or X7. K40 is a little machine and surely with E5 its enough. But maybe I will add an Z table and rotatory axis and display in X7 its very attractive.


---
*Imported from [Google+](https://plus.google.com/108914621718927041496/posts/JhiiZjxptVi) &mdash; content and formatting may not be reliable*
