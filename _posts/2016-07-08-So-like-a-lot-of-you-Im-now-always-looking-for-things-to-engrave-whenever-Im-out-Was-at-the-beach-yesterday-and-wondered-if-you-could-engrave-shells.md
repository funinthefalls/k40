---
layout: post
title: "So like a lot of you I'm now always looking for things to engrave whenever I'm out :) Was at the beach yesterday and wondered if you could engrave shells"
date: July 08, 2016 00:15
category: "Object produced with laser"
author: "Ned Hill"
---
So like a lot of you I'm now always looking for things to engrave whenever I'm out :)  Was at the beach yesterday and wondered if you could engrave shells.  Turns out yes you can.   35% power (~10ma) at 180mm/s .  (Holes were already there.)

![images/be9efcc1a5bd2fc1861be52b72902cdd.png](https://gitlab.com/funinthefalls/k40/raw/master/images/be9efcc1a5bd2fc1861be52b72902cdd.png)



**"Ned Hill"**

---
---
**HalfNormal** *July 08, 2016 03:43*

**+Ned Hill** I was wondering this too but Arizona is a wee bit far from a beach! I was going to try a hobby store and see if they had some inexpensive shells to play with. Thanks for checking it out.


---
**Ned Hill** *July 08, 2016 03:48*

The ones from the hobby store will probably already have a clear coat on them, but if not clear coat them for a darker wet look.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 08, 2016 07:50*

I know where I'm going! Off to the beach! Thanks for sharing Ned. This could make some pretty cool little trinkets.


---
**Scott Marshall** *July 08, 2016 10:27*

Do a virgin Mary and throw it back on the beach....


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 08, 2016 11:38*

**+Scott Marshall** Love it. People would trip out lol.


---
**Ned Hill** *July 08, 2016 12:43*

**+Scott Marshall** Lol, I like how you think ;)


---
**I Laser** *July 14, 2016 09:35*

**+Scott Marshall** lmao classic.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/BtGqkoDqy66) &mdash; content and formatting may not be reliable*
