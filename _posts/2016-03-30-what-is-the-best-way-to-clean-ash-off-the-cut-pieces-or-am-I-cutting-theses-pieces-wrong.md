---
layout: post
title: "what is the best way to clean ash off the cut pieces or am I cutting theses pieces wrong"
date: March 30, 2016 03:11
category: "Discussion"
author: "3D Laser"
---
what is the best way to clean ash off the cut pieces or am I cutting theses pieces wrong





**"3D Laser"**

---
---
**Gee Willikers** *March 30, 2016 03:54*

Denatured alcohol. Cover the piece in tape first, before you cut. Vinyl sign application tape works well and is available in various widths and tackiness. Works especially well with engraving and painting/colorfill.


---
**Phillip Conroy** *March 30, 2016 07:49*

What are you using for a cutting bed,do you have air assist,what psi are u using

Tape works however can be a pain to remove if work is detailed


---
**Don Kleinschnitz Jr.** *March 30, 2016 12:13*

I just read in another forum:

"Got a can of Pam or other cooking spray? Peel protective film off, spray a light coat, then engrave and cut. Plop finished piece in water that has been freshly boiled and taken off the heat and let soak for 10 minutes. Then go wash in sink with hot water and dish soap. Dry thoroughly. The cooking spray prevents the hot gases and particulates from sticking to surface of plastic and the boiling water is to anneal the plastic and help relieve stresses causes by the cutting and engraving and will prevent crazing later on. It works. Trust me."

Haven't tried it myself.


---
**3D Laser** *March 30, 2016 12:28*

What about for ply


---
**Gee Willikers** *March 30, 2016 22:56*

Sealing the wood eases cleanup.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/5g8eeyUkZjD) &mdash; content and formatting may not be reliable*
