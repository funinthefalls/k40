---
layout: post
title: "Peter van der Walt what hardware is needed to enable the support of laser web with the cutter?"
date: January 12, 2016 00:06
category: "Software"
author: "Pete OConnell"
---
**+Peter van der Walt** what hardware is needed to enable the support of laser web with the cutter? maybe a link to a guide? 





**"Pete OConnell"**

---
---
**Ariel Yahni (UniKpty)** *January 12, 2016 01:10*

Don't think hardware is an issue.  Just need to be a compatible firmware: Marlin, Smoothie, Girl and I think repetier is what Peter is using himself. If you have a board that work with any of those you are set. 


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/Ggx1JZb2WLf) &mdash; content and formatting may not be reliable*
