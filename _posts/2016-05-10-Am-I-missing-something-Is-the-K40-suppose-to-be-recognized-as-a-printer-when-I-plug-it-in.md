---
layout: post
title: "Am I missing something? Is the K40 suppose to be recognized as a printer when I plug it in?"
date: May 10, 2016 02:31
category: "Discussion"
author: "Alex Krause"
---
Am I missing something? Is the K40 suppose to be recognized as a printer when I plug it in? Not having much luck installed lzrdraw any advice how to get cutting tonight?





**"Alex Krause"**

---
---
**Gee Willikers** *May 10, 2016 02:41*

No but the software (and its usb hardware lock) should see it.


---
**Alex Krause** *May 10, 2016 02:42*

Let me double check if this cheepo USB hub is working that might be my problem


---
**Gee Willikers** *May 10, 2016 02:43*

I think the computer should at least recognize the usb chipset and ask to install a driver.


---
**Alex Krause** *May 10, 2016 02:45*

I haven't seen that yet... Trying to get this to run on my win10 tablet. 


---
**Gee Willikers** *May 10, 2016 02:47*

Win10 may be the issue. Its discussed a lot in the facebook group but honestly I haven't followed the conversations.


---
**Alex Krause** *May 10, 2016 02:48*

Whats the name of the FB group?


---
**Gee Willikers** *May 10, 2016 02:51*

[https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
**Alex Krause** *May 10, 2016 03:21*

**+Susan Moroney**​ I have the m2 board have you tried the lzrdraw software that was included on the CD? Does it just work to ( if you have tried it)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 10, 2016 03:21*

100% what **+Susan Moroney** said. That's the exact process I used for Windows 10 also. No drivers required that I recall of. You won't see it as a printer (or even any connected device) anywhere in Windows.


---
**Alex Krause** *May 10, 2016 03:26*

**+Yuusuf Sallahuddin**​ didn't you post a Dropbox for Corel?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 10, 2016 03:29*

**+Alex Krause** Yeah I did a while back for Corel x5 & CorelLaser install & install instructions in text file & updated .ini files for the text within the program (because spelling errors were driving me nuts).



Here's a link to the folder: [https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0](https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0)



You will want to grab the entire lot if you are doing a clean install of it all.


---
**Ariel Yahni (UniKpty)** *May 10, 2016 03:44*

**+Alex Krause**​ I'm following every step

 Picking laser tomorrow


---
**Alex Krause** *May 10, 2016 05:16*

**+Yuusuf Sallahuddin**​ is it OK to update Corel? When it asks me to


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 10, 2016 05:27*

I ticked or selected to allow it to auto update, so it should be. However I did notice that if you open CorelDraw, after a few goes it might mention that it is pirated version & will "expire". I just ignored it & have stopped opening it via CorelDraw & only use the CorelLaser icon to open it now. Since then I haven't seen any messages about pirated version or any expire messages. Will have to wait & see over time though, since it's only been a couple of weeks since I installed it.


---
**Jim Hatch** *May 10, 2016 06:17*

**+Alex Krause**​ LaserDRW is a standalone laser app. You can do some stuff natively (create text engraves, draw shapes to cut etc). What it also is useful for is testing the new machine.



When you fire it up with the USB dongle plugged in you should see it checking the dongle and updating the splash screen with something to the effect that it's authorized.



Then go into the settings and set the machine type and serial #. The type is the only entry in the drop down box that ends in M2. The serial number is on your board - you'll see the standard etch type silkscreen numbers and also an 8 or 10 digit inked number. That's the serial number.



Once you set those it will communicate properly with the laser. Draw a box and then hit engrave. Click the "starting" button in the pop-up window and watch the magic :-)


---
**Jim Hatch** *May 10, 2016 06:17*

Win10 is not an issue, btw. I use it myself.


---
**Ariel Yahni (UniKpty)** *May 11, 2016 01:47*

Guys I'm getting this on Windows 10 64bit [http://imgur.com/yUW5mor](http://imgur.com/yUW5mor)


---
**Alex Krause** *May 11, 2016 01:50*

**+Ariel Yahni**​ did you install the files **+Yuusuf Sallahuddin**​ posted? Or the one included on the CD?


---
**Ariel Yahni (UniKpty)** *May 11, 2016 01:54*

From **+Yuusuf Sallahuddin**​ Dropbox. I'm on a mac running virtual windows ( that should not be an issue)  key gets recognized 


---
**Alex Krause** *May 11, 2016 01:55*

Did you try the 32bit install?


---
**Jim Hatch** *May 11, 2016 01:58*

When you ran the install did you do it using the "run as administrator" option?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 02:01*

**+Ariel Yahni** Do you get that error when running the install for CorelLaser or when trying to operate it after install?



I am running Win 10 x64 on Macbook via Bootcamp.



Either way, what I did when installing is right click the install file & choose compatibility mode for Windows 7.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 02:05*

**+Susan Moroney** That's odd. I never have to reboot. What are you running?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 02:12*

**+Susan Moroney** Ah, that could be causing some of the fault. I would look into setting compatibility mode on both CorelDraw 12 exe file & the CorelLaser exe file to Windows 7. Might prevent the issues you are having. Can't hurt to try at least.


---
**Ariel Yahni (UniKpty)** *May 11, 2016 02:13*

installing and running as administrator is a no go. Running in compatibility is a no go ( tried win xp, 7, 8 )


---
**Alex Krause** *May 11, 2016 02:14*

Did you do a uninstall and clean install?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 02:17*

**+Ariel Yahni** Does the CorelDraw x5 work on it's own? I am wondering is it the CorelDraw causing the issue or is it the CorelLaser causing the issue.


---
**Ariel Yahni (UniKpty)** *May 11, 2016 02:29*

**+Yuusuf Sallahuddin**​ corel loads fine. It's corellaser throwing the error. Going to do boot camp to make sure it's not the VM


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 03:05*

**+Ariel Yahni** Yeah another thought is can you install a version of Win 7 on the VM? But Bootcamp might be the best option to test first (since I know it is working for me on Bootcamp).


---
**Ariel Yahni (UniKpty)** *May 11, 2016 03:48*

**+Yuusuf Sallahuddin**​ thanks man. Boot camp solve the issue. It's a pain but should be temporarily 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 09:32*

**+Ariel Yahni** That's unfortunate that the VM doesn't work. Maybe a different VM software will allow it to work also.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/7fqFnc15Mh2) &mdash; content and formatting may not be reliable*
