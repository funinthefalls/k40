---
layout: post
title: "My new 50 watt tube from laser depot arrived today in great condition....packaged well and a great price...it will be installed tomorrow!"
date: March 19, 2016 00:51
category: "Discussion"
author: "Scott Thorne"
---
My new 50 watt tube from laser depot arrived today in great condition....packaged well and a great price...it will be installed tomorrow! 





**"Scott Thorne"**

---
---
**Scott Marshall** *March 19, 2016 01:16*

Glad to hear it. Maybe I'll go for one next time.


---
**Alex Krause** *March 19, 2016 02:28*

Does the 50w tube drop in to the chassis of the k40? Or are you working off the larger chassis 


---
**Anthony Bolgar** *March 19, 2016 02:36*

LightObject.com sells a metal case extension that lengthens the tube area so that you can use a 50W tube on a K40. Tube upgrade would also require a PSU upgrade. However, I think it is worth it as most K40's are really only 30-35 watts, and most ebay machines that say they are 50W are actually just 35-40W tubes that they are over driving to produce the higher output, which dramatically shortens the tube life.


---
**Scott Thorne** *March 19, 2016 09:36*

**+Anthony Bolgar**...the max milli amps on the tube is 18....I think the power supply will be fine...the max output of the power supply is 26 mA's


---
**Scott Thorne** *March 19, 2016 09:37*

**+Alex Krause**...I'm working with a bigger machine...but Anthony is right....they overdrive the machines they sell. 


---
**Phillip Conroy** *March 19, 2016 09:54*

What happened to the tube in your machine


---
**Scott Thorne** *March 19, 2016 10:34*

**+Phillip Conroy**...it's fine...like Anthony said I think it's a 40 watt tube not a 50 watt like advertised...the tube that came in it is 800 mm long and a true 50 watt is 990mm long. 


---
**Scott Thorne** *March 19, 2016 11:11*

**+Phillip Conroy**...the reason I bought the new tube is tube depot had a sale...I got it for 225.00...so I figured I would get it while on sale. 


---
**Anthony Bolgar** *March 19, 2016 13:22*

Would you be interested in selling the 40W tube?


---
**Scott Thorne** *March 19, 2016 13:54*

Sure...I have all the packing material...it's a reci tube...says max  watts 48.


---
**Anthony Bolgar** *March 21, 2016 03:04*

Do you still have the reci for sale?


---
**Scott Thorne** *March 21, 2016 09:26*

**+Anthony Bolgar**.... yeah...I'll let you know when I get the puri installed...looks like it's going to be Wednesday before the power supply gets here. 


---
**Anthony Bolgar** *March 21, 2016 09:56*

Thanks.


---
**Scott Thorne** *March 21, 2016 11:53*

**+Anthony Bolgar**..you bet man....I haven't forgotten about you man..

I just figured I would not take the chance and go ahead and get the right size power supply. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/Wv17bSSwumu) &mdash; content and formatting may not be reliable*
