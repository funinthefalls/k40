---
layout: post
title: "Does anyone know where it get an 18mm focal lens in the uk"
date: August 16, 2016 07:40
category: "Discussion"
author: "Tony Schelts"
---
Does anyone know where it get an 18mm focal lens in the uk.  I stil have the 12mm one that came originaly has anyone been able to use it in a lightobjests air assist head.  I dont have a 3dprinter





**"Tony Schelts"**

---
---
**Tony Schelts** *August 16, 2016 07:42*

The reason I need a new lens, is that when I decided to clean the lens. Which i did no problem and then re fitted it. I dropped the head onto a hard surface and it shattered the lens


---
**Jim Hatch** *August 16, 2016 12:39*

In the US, the big box home stores (lumber, hardware, etc) carry lots of bits of hardware including nylon washers & spacers. There is one they sell that works as a spacer that fits in the LO head with a 12mm center hole.



You might look at a local hardware store. Even if you can't find one that's the same, an 18mm nylon washer should do it for you - just redrill the center hole to 12mm. 



Quick and dirty is to put a dot of silicone glue on either side of the LO housing so it holds the smaller lens in place but is removable when you get the bigger lens.


---
**Tony Schelts** *August 16, 2016 13:06*

Thanks Jim


---
**Scott Marshall** *August 16, 2016 13:45*

Make sure your're asking for the right thing. The lens you are looking for is an 18mm diameter with a 50.8 (2.0") focal length (that is if you want a stock K40 Focal length).



The Focal length is the distance from the center of the lens to where it focuses.



Longer focal length lenses (2.5" and 3" are generally available) focus sharper, but with a shorter depth of field. (distance range they are IN focus).



I've found a 2.5" lens cuts 3mm materail better (it doesn't need refocusing to be effective over the 3mm range)



Once you get past 3", your spot size is getting pretty big for 'fine" engraving and such.



If you don't mind the 1/2"  longer working distance, a 2..5" lens is the best for all around cutting (in my opinion)

For fine engraving,  1.5 or 2" work well.





Have fun,



Scott



PS



More on the subject:

[http://www.parallax-tech.com/faq.htm](http://www.parallax-tech.com/faq.htm)



Here's a handy calculator for focusing diameter for a given lens:

[https://www.rp-photonics.com/focal_length.html](https://www.rp-photonics.com/focal_length.html)



Good source for lenses if you don't mind waiting a bit (Saite Cutter)

[http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050](http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050)



More good information on Laser optics:

[http://www.ophiropt.com/](http://www.ophiropt.com/)


---
**Tony Schelts** *August 16, 2016 14:15*

I Need to proof read what I put on here, nearly as bad as predictive text.  Yes 18mm Lens with a focal lenght of 50.8mm


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/JsXCXh7YkkQ) &mdash; content and formatting may not be reliable*
