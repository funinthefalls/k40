---
layout: post
title: "Personalized pencils. I have a project at work and to get everyone involved, I am making personalized pencils"
date: October 15, 2017 22:51
category: "Object produced with laser"
author: "HalfNormal"
---
Personalized pencils.



I have a project at work and to get everyone involved, I am making personalized pencils. Here are the test ones. They turned out decent so I am going to present them to management for approval. These are cheap dollar store pencils BTW.







![images/2144d1ac400153edb3cb4f78fa1f849b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2144d1ac400153edb3cb4f78fa1f849b.jpeg)



**"HalfNormal"**

---
---
**Steve Anken** *October 15, 2017 22:55*

Nobody named Mark I hope. :-)




---
**Ned Hill** *October 16, 2017 14:24*

Cool.  Did you make a jig to hold them to do multiples in the same run?


---
**HalfNormal** *October 16, 2017 14:26*

Made a simple jig to to hold one at a time. The time it would take to make a multiple holder I can do most of the engraving.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/aRRrE5KEm2J) &mdash; content and formatting may not be reliable*
