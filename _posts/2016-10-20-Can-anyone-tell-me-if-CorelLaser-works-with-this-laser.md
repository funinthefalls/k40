---
layout: post
title: "Can anyone tell me if CorelLaser works with this laser?"
date: October 20, 2016 01:23
category: "Software"
author: "Trenton"
---
Can anyone tell me if CorelLaser works with this laser?  Prior to purchasing, I asked the seller and they said it would.  However, now that I have the laser, I can't get it to work with CorelLaser and the seller is not very helpful with troubleshooting.  I can get it to work with the RDWorks plugin for CorelDraw, but would like to be able to use it with CorelLaser.



[http://www.ebay.com/itm/High-Precise-60W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/252518164408?hash=item3acb4167b8:g:t3EAAOSwvg9Xc2V4](http://www.ebay.com/itm/High-Precise-60W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/252518164408?hash=item3acb4167b8:g:t3EAAOSwvg9Xc2V4)









**"Trenton"**

---
---
**Alex Krause** *October 20, 2016 01:48*

That's the controller... it's a Ruida controller that uses RDworks or a variant of it... you can use Corel but not corel laser as that is a proprietary software of the M2nano board

![images/093eefd86354ebcf1ecad7e953a22aff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/093eefd86354ebcf1ecad7e953a22aff.jpeg)


---
**Alex Krause** *October 20, 2016 01:51*

That is a DSP style controller way more powerful than anything the m2nano board could do... you have the ability to do grayscale engraves with this stock


---
**Trenton** *October 20, 2016 01:57*

Ok thanks.  Yeah I figured this controller is more powerful but I use Corel draw and find that the rd works plugin for Corel is very cumbersome.  That's why I was hoping to be able to use Corel laser.  In my opinion, it's the easiest software that I've seen for engraving.  Just two clicks and the laser is engraving exactly what is on the page in Corel.  Rd works can't do nearly as much as coreldraw.


---
**Trenton** *October 20, 2016 01:58*

Any reason that I couldn't swap out the DSP for an M2 nano so I can use Corel laser with it?


---
**Alex Krause** *October 20, 2016 02:14*

I assume that the stepper motors for this machine are much larger than those used in a K40 doubt the driver chips on the m2 nano would be strong enough to even drive the unit... if you want to switch the controller I would switch to a Smoothie board and run laser web... or you could jump on YouTube and familiarize yourself with RDworks better and adapt to the new workflow


---
**Alex Krause** *October 20, 2016 02:16*

I know a ton of people that would kill to have a DSP controller


---
**Ashley M. Kirchner [Norym]** *October 20, 2016 04:08*

I would  take a DSP any day over that dumb M2 Nano that's in my stock machine. That thing is coming out as soon as I place my order for a Smoothie.


---
**Trenton** *October 20, 2016 13:21*

How do you change the laser head origin with RDworks?  I don't see a spot to enter in the starting x y coordinates?  Or is that done on the DSP controller?


---
**Trenton** *October 20, 2016 13:46*

OK, figured out how to move that origin, but is there a way to save different origin positions?  When I press "origin" on the controller, nothing happens.


---
*Imported from [Google+](https://plus.google.com/108213621154298105694/posts/9gFKxGyPjQe) &mdash; content and formatting may not be reliable*
