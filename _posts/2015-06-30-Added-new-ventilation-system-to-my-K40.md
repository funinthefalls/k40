---
layout: post
title: "Added new ventilation system to my K40"
date: June 30, 2015 19:59
category: "Discussion"
author: "David Wakely"
---
Added new ventilation system to my K40. I used 125mm channel ducting and an adapter to 100mm round flexi duct. It's a lot better than the stock vent because I have gained additional working area



![images/2273d653e0a4627fb648764393bf279c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2273d653e0a4627fb648764393bf279c.jpeg)
![images/d29bc570fe0b7c314549f504697b16ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d29bc570fe0b7c314549f504697b16ac.jpeg)

**"David Wakely"**

---
---
**Chris Bounsall** *July 29, 2015 12:57*

Nice. How did you remove the extraction venting?


---
**David Wakely** *July 31, 2015 21:41*

it was a massive pain but i snapped the welds with a screwdriver from the inside. It look a long time to get it out though. Its defiantly worth it as i gained more precious space!


---
**Chris Bounsall** *July 31, 2015 23:04*

Ha, I was so hoping that it was easier than it looked (like a couple of hidden bolts).



So a hammer and screw driver directly behind a glass tube. I assume you took the tube out before doing this!



Your extraction mod is great too, I  hate having to remove it to get access to the tube. Though I've got a couple of extra fans on mine, because I found the stock unit lacking.

﻿


---
**David Wakely** *August 01, 2015 08:51*

No I probably should have taken it out but I didn't I was just real careful. It was mainly just prying and twisting but it came out in the end


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/QY8WENab6HU) &mdash; content and formatting may not be reliable*
