---
layout: post
title: "Just wanted to share. Drag chain that can be cut by your laser"
date: March 30, 2017 15:51
category: "Modification"
author: "Cesar Tolentino"
---
Just wanted to share. Drag chain that can be cut by your laser. Not my original design, so im not taking credit for it. But modified it so that you dont need to glue them.



order of assembly is to attach the two sides and bottom first (like show on the pic below)



then put the top part where it meets the hinge.



then slide the top part to complete the one module. ( see pic below)



the svg file i created has a inside dimensions of 35mm width x 25mm height. I want for my extension cord to go inside wihtout a problem. You can alter the dimension to whatever you want.



Also, I am using a 2.8mm thick mdf board, so your thickness may vary.



Although i am using this for my cnc project. You might be able to use it for your K40 also.





![images/685c10551b4a957e24a45d277b48bb44.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/685c10551b4a957e24a45d277b48bb44.jpeg)
![images/4d159e667af9d37d642973b0a440aa8c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4d159e667af9d37d642973b0a440aa8c.png)

**"Cesar Tolentino"**

---
---
**Cesar Tolentino** *March 30, 2017 15:51*

Here is the original design. He is the one that made this design happen...



Here is the original design from thingiverse.

[thingiverse.com - Laser Cut Drag Chain by msraynsford](http://www.thingiverse.com/thing:145360)


---
**Cesar Tolentino** *March 30, 2017 15:52*

Here is the svg file



[drive.google.com - _DRAG CHAIN.svg - Google Drive](https://drive.google.com/open?id=0B1xyPwLXXQk3akdfSHoyLXQ2cDg)



It can make 7 modules (50mm length) per 8.5x11 sheet of material. Enjoy.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 31, 2017 05:20*

I made one of these in the past (probably a lot smaller than yours) using I think the same plans from thingiverse. I found it to be exceptionally noisy when moving back & forward. Constantly click-clacking as it goes haha. But it's a handy prototype for when you don't have access to another cable chain.


---
**Cesar Tolentino** *March 31, 2017 05:33*

Yes this is true. Click Clack. Mine since is bigger is less but still click Clack haha


---
**BeenThere DoneThat** *April 01, 2017 03:44*

Very unique to me. Awesome. 


---
*Imported from [Google+](https://plus.google.com/+CesarTolentino/posts/jdC5eUzPfvR) &mdash; content and formatting may not be reliable*
