---
layout: post
title: "Is there such thing as a 1 3/4\" (44mm) focal length lens?"
date: May 03, 2017 03:24
category: "Discussion"
author: "Mark Brown"
---
Is there such thing as a 1 3/4" (44mm) focal length lens?



Let's just make sure I'm thinking about this right, a ramp test:

-Put a piece of wood in on a slope

-Laze a line across it

-You get wide scorches at each end, and a cut in the middle

-The middle of the cut section is the focal length, measured from center of lens to center of wood



I've done this twice, got 44mm once and 43mm once.  Which would be "1 3/4", which doesn't exist?



Are the Chinese screwing with me, or am I up too late at night?





**"Mark Brown"**

---
---
**Gee Willikers** *May 03, 2017 03:29*

Is the lens in place with the crown up (toward the laser tube)?


---
**Alex Krause** *May 03, 2017 04:59*

Bump goes up


---
**Phillip Conroy** *May 03, 2017 10:53*

Repeat with lens other way up


---
**HalfNormal** *May 03, 2017 12:46*

Some people use a q-tip to make sure they are getting the correct distance.


---
**Mark Brown** *May 03, 2017 13:02*

Thanks everyone.



Conclusion: I need to learn when to give up and go to bed. Just tried again, and got 50.5mm



So either I put the lens in backwards on two separate occasions, or my brain can't deal with numbers at all at night. 



I'm embarrassed. 


---
**Phillip Conroy** *May 03, 2017 13:30*

I have forgotten how many hours wasted fault finding electronic devices to find out the next day it was not plugged in


---
*Imported from [Google+](https://plus.google.com/116643344835605995638/posts/Ns7jbgssXfo) &mdash; content and formatting may not be reliable*
