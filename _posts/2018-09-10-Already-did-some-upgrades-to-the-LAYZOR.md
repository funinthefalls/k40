---
layout: post
title: "Already did some upgrades to the LAYZOR"
date: September 10, 2018 13:58
category: "External links&#x3a; Blog, forum, etc"
author: "Frederik Deryckere"
---
Already did some upgrades to the LAYZOR. I installed a Cohesion3D board and air assist!



Read more here:

[https://www.manmademayhem.com/?p=3505](https://www.manmademayhem.com/?p=3505)





**"Frederik Deryckere"**

---
---
**Jeff Kes** *September 11, 2018 00:52*

I have a question I would assume at a certain point this would not be a viable option in cost to buy a K40 and do all of these upgrades would it?  Would it be better to source all the parts and build this from scratch?  Did you do any comparison for this or maybe this would be for someone that already has made the investment in a K40?



Thanks and if you don’t have any of this info I was going to start there myself?


---
**Frederik Deryckere** *September 11, 2018 12:11*

I have gotten this question many times already. There are a few people already doing exactly this with the plans as a base and coming up with their own solution for some K40 parts. Since this has been asked many times, I will at one point look into this but I can't really give a timeframe as I'm very busy atm supporting this build, and leaving on a holiday tomorrow.


---
**Jeff Kes** *September 12, 2018 02:07*

**+Frederik Deryckere** no problem I thought I would just ask but I still think you did a great job with the conversion. 



Have fun on your Holiday 


---
**Frederik Deryckere** *September 12, 2018 12:12*

Thanks!


---
**Kieth Wallace** *September 12, 2018 13:40*

How loud is that air pump?


---
**Frederik Deryckere** *September 14, 2018 00:18*

**+Kieth Wallace** if you suspend it not very loud. When the exhaust fan is switched on you can barely hear it


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/gRjaFB13Nvq) &mdash; content and formatting may not be reliable*
