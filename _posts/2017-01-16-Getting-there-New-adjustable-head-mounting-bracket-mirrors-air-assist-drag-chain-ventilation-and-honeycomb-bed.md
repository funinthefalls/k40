---
layout: post
title: "Getting there. New adjustable head, mounting bracket, mirrors, air assist, drag chain, ventilation and honeycomb bed"
date: January 16, 2017 00:24
category: "Modification"
author: "Bill Keeter"
---
Getting there. New adjustable head, mounting bracket, mirrors, air assist, drag chain, ventilation and honeycomb bed. Just need to install the cohesion3d pcb and I'll be all set. 



![images/67179d657994f03b022eef9e4054c998.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/67179d657994f03b022eef9e4054c998.jpeg)
![images/9fc16fbea7a1366b1a838ad5c1ca7c86.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9fc16fbea7a1366b1a838ad5c1ca7c86.jpeg)

**"Bill Keeter"**

---
---
**Tony Sobczak** *January 16, 2017 03:47*

Where did you very three adjustable head? 


---
**Bill Keeter** *January 16, 2017 04:44*

**+Tony Sobczak** it's from Saite Cutter on eBay. Just know it needs a 20mm hole in the mounting bracket. So I had to make my own.  


---
**Tony Sobczak** *January 16, 2017 06:03*

Thanks 


---
**Trevor Boultwood** *January 18, 2017 13:57*

How did you mount you're honeycomb bed? I was thinking of getting some extruded (l shaped) aluminium. Looking great!


---
**Bill Keeter** *January 18, 2017 14:34*

**+Trevor Boultwood** The honeycomb sits on a sheet of metal grating from Lowes. Which I cut to size with metal shears. Then under it I have 6 lasercut standoffs to support the grate. Haven't decided long term if i'll keep the lasercut parts.


---
**Bill Keeter** *January 18, 2017 14:41*

Here's a pic of the standoff design

![images/20d3a791464425bb1df610e29fc70efd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/20d3a791464425bb1df610e29fc70efd.jpeg)


---
**Chris Sader** *January 18, 2017 19:08*

I'm thinking of using a cheap scissor jack off of amazon to support the honeycomb, so it will be adjustable. any reason that wouldn't work?


---
**Robert Selvey** *January 30, 2017 02:05*

Where did you find the honey comb for the bed ?


---
**Tony Sobczak** *January 30, 2017 02:41*

Got mine here. 



[ebay.com - Details about  Aluminum Honeycomb Sheet Core / Honeycomb Grid - 1/4 Cell, 24"x24", T=.250"](http://m.ebay.com/itm/Aluminum-Honeycomb-Sheet-Core-Honeycomb-Grid-1-4-Cell-24-x24-T-250-/152398478312?nav=SEARCH)


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/ctS77KmB1jU) &mdash; content and formatting may not be reliable*
