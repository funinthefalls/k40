---
layout: post
title: "During a week away from home, I returned to discover that my computer didn't work and the diagnosis was that I needed to replace the mother board"
date: October 12, 2016 01:29
category: "Original software and hardware issues"
author: "John von"
---
During a week away from home, I returned to discover that my computer didn't work and the diagnosis was that I needed to replace the mother board. I did and the computer works fine and executes corel laser and the accompanying software. However, when I go to execute an engrave command, the feedback is that the laser is not connected. i checked the device id and it is correct on the computer screen and the board inside the laser. Help, what can I do to correct this.

Thanks, John von





**"John von"**

---
---
**Ariel Yahni (UniKpty)** *October 12, 2016 01:42*

Check to see if the board is getting power at all. Had seen someone this week where no power to the stock board. 


---
**Alex Krause** *October 12, 2016 01:52*

If windows 10 a recent update may have forced the unsigned driver policy... 


---
**John von** *October 12, 2016 03:06*

Ariel, how can I check to see if power is going to the board. Alex, I am using windows XP


---
**Ariel Yahni (UniKpty)** *October 12, 2016 03:09*

The stock board should have a led light up when the machine is powered  on. I just don't remember exactly as I changed from stock almost inmediatly 


---
**HalfNormal** *October 12, 2016 12:44*

The laser control board/dongle nano/moshi could have been affected by the MB failure. I had a MB powersupply fail and was lucky it did not take out the USB components connected.


---
*Imported from [Google+](https://plus.google.com/112642592493783290997/posts/eXijU84xBn5) &mdash; content and formatting may not be reliable*
