---
layout: post
title: "Is that calibration good enough? These points are from the XY four corners of my setup"
date: February 10, 2017 20:46
category: "Hardware and Laser settings"
author: "Jorge Robles"
---
Is that calibration good enough?



These points are from the XY four corners of my setup.

![images/c7eec23abbbdd197219439706570f157.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c7eec23abbbdd197219439706570f157.jpeg)



**"Jorge Robles"**

---
---
**Roberto Fernandez** *February 10, 2017 21:23*

Perfectoa 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 10, 2017 21:36*

Looks reasonable, but I'd personally try to get them all hitting the exact same spot.


---
**Jorge Robles** *February 10, 2017 21:40*

Thanks! I will try to improve, but is a bloody hell 👹


---
**Maker Cut** *February 10, 2017 22:32*

Like **+Yuusuf Sallahuddin** said try to get them to the same spot. That will give you more consistent cuts or engraving through the whole bed and reduce the possibility of getting strange results during a job.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 10, 2017 22:41*

**+Jorge Robles** It is an absolute nightmare to do. I spent 3 or 4 days figuring it all out (many hours of swearing at the machine). It's well & truly worth taking the time to figure it out though, as now it takes me just 15 minutes maximum to get it spot on.


---
**Anthony Bolgar** *February 10, 2017 23:13*

I just run the tube at 28mA so I don't need to be as spot on. :)


---
**Phillip Conroy** *February 10, 2017 23:17*

Everybody is obseced with Cal  not me   my machine spent 6 months  with Calso bad that it would only cut the top left  150mm  square  ,for me at the time that was good eenough as iIwas only cutting stuff  100mm ssquar . Only after blowing a tube and replacing it did iIget cCalright across the whole cutting table .first cCaltook a weekend.then i ddiscovered  the cCaliinstruction tate says only move screws in pairs second cCal took4 hours ,now on my third tube iIcan get usable cCalin 30 minutes.pp'siIhad to pack tube one end tto get good cCal, wish iI could buy aadjustable laser tube mounts ?.....?......


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 10, 2017 23:19*

**+Phillip Conroy** I feel like I've seen adjustable laser tube mounts somewhere... maybe LightObjects? Not sure...


---
**greg greene** *February 10, 2017 23:26*

All in the 10 Ring !


---
**Don Kleinschnitz Jr.** *February 10, 2017 23:35*

**+Jorge Robles** actually hitting the center of the mirror is not as important as the parallelism of the beam to the X and Y planes of movement. 

Single point measurements don't easily show you this. 

I suspect that machines who's optical paths aren't parallel have a lot narrower margin of error as to mirror position.



It takes longer to set up multiple target measurements but the alignment takes substantially less time.

 

[donsthings.blogspot.com - K40 Optical Model & Alignment Tools](http://donsthings.blogspot.com/2016/06/k40-alignment-tools.html)


---
**Maker Cut** *February 11, 2017 00:09*

**+Don Kleinschnitz** Well said Don.


---
**Anthony Bolgar** *February 11, 2017 04:08*

There is a file on Thingiverse for adjustable mounts for the K40 (it is a remix of the 50W holders)[http://www.thingiverse.com/thing:1581069](http://www.thingiverse.com/thing:1581069) If you do not have a printer, I can print out a set for you **+Phillip Conroy**

[thingiverse.com - 40 Watt CO2 Laser Tube Clip Remix by CallJoe](http://www.thingiverse.com/thing:1581069)


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/exUyVSfJVDo) &mdash; content and formatting may not be reliable*
