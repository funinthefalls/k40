---
layout: post
title: "Hello guys, just got done installing Ramps 1.4 and everything working the way I should (I am sure)"
date: December 03, 2015 06:59
category: "Software"
author: "ChiRag Chaudhari"
---
Hello guys, just got done installing Ramps 1.4 and everything working the way I should (I am sure). But I am facing weird problem with Inkscape extension installation. I am following every step as Turnkey Tyranny's Gcode Exporter plugin. Tried on two different win7 machines, and a win 8.1 tab, but its just not happening. Issue is I simply dont see the extension under Extension > Export > . I have tried both 32 and 64 bit versions. Tried both pre-compiled and manual install Python. But when i run Inkscape the plugin is not there.



Does any one know what is that am I doing wrong? What am I missing? Really frustrated after setting all the hardware up and darn software not creating a g-code file to do maiden flight. aaaahhhhhhh :/





**"ChiRag Chaudhari"**

---
---
**ChiRag Chaudhari** *December 03, 2015 21:59*

Yeah I installed it right (I guess), now have to know how to run it. Need bit more reading. then will ask some questions. Thank You.




---
**ChiRag Chaudhari** *December 04, 2015 04:08*

**+Peter van der Walt** I am almost there. Getting an error : Object Expected Source: Microsoft JScript Runtime Error



What i do ?


---
**ChiRag Chaudhari** *December 04, 2015 04:26*

When I try to run Server.js


---
**ChiRag Chaudhari** *December 04, 2015 04:56*

OK got it. Let me digg a little deep and then bug u again. Really appreciate your continuous help.


---
**ChiRag Chaudhari** *December 04, 2015 20:05*

**+Peter van der Walt** Looks like on my macbook everything installed smooth (i guess) and this is what I am getting now:



Chirags-MacBook-Pro:laserweb chiragchaudhari$ node server.js

Got error: connect ECONNREFUSED not enabling webcam

connected to /dev/cu.Bluetooth-Serial-1 at 115200

events.js:85

      throw er; // Unhandled 'error' event

            ^

Error: Cannot open /dev/cu.Bluetooth-Modem

    at Error (native)

Chirags-MacBook-Pro:laserweb chiragchaudhari$ 



what is the next step. how to localhost? Sorry but your instructions are not Noob Proof :P


---
**ChiRag Chaudhari** *December 04, 2015 20:27*

still same error. aaahhhh. let me try on windows machine. one more question lets say everything installed correctly on the win machine. How to use the Access part. I mean where to find hostaddress ? please bare with my noob questions. and thans for such fast replies. \m/


---
**ChiRag Chaudhari** *December 04, 2015 20:58*

once i figure this out im gonna help u write noob prof instructions. do i need pythn installed. on win 8 tab got a whole bunch of errors. log file



[https://drive.google.com/file/d/0B8OiQZgP5gD2NEFrZTlZTGtwdXM/view?usp=sharing](https://drive.google.com/file/d/0B8OiQZgP5gD2NEFrZTlZTGtwdXM/view?usp=sharing)


---
**ChiRag Chaudhari** *December 04, 2015 21:07*

Great! installed Python2.7 and guess wht "npm installed" worked with no errors except following two lines: and back to prompt.



npm WARN package.json reprapweb@ No repository field.

npm WARN package.json reprapweb@ No license field.



Neet to find OTG cable real quick to connect Mega/Ramps to this tiny fella! Very exiting


---
**ChiRag Chaudhari** *December 04, 2015 21:16*

downloading Arduino SDK for drivers. its taking too long to install driver. Thank you thank you thank you. :P


---
**ChiRag Chaudhari** *December 04, 2015 21:34*

YEEEEEEEEESSSSSSSSSSSSSSS!



connected and got GIU in my browser (Chrome)



This is Legen.......wait for it.......Dery!



I guess you can get some sleep now. Next step see what this does. but before that gotta take care of some business. Play time over, be back soon.


---
**ChiRag Chaudhari** *December 04, 2015 21:37*

Machine control working like charm! OMG cant wait to test this baby out. I am dancing like a kid now (literally)



Thank you so much Peter! 


---
**ChiRag Chaudhari** *December 08, 2015 21:23*

SOLVED!



Instead of downloading whole zipped folder and then extracting files form there to copy them to extension folder, I Right-Clicked and Save File as to extension. So the .py and .inx file had some extra information causing it not to work.


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/VeQ714wH3RE) &mdash; content and formatting may not be reliable*
