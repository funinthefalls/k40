---
layout: post
title: "How Easy/Hard is it to fit Honeycomb cells in the K40?"
date: April 25, 2016 19:33
category: "Discussion"
author: "Pigeon FX"
---
How Easy/Hard is it to fit Honeycomb cells in the K40? 



Had a hunt on Youtube for a guide but nothing popped up, is it a simple upgrade to just slap in a 300x200mm Honeycomb bed, or do you need to fabricate a whole bracket to hold it in place? 





**"Pigeon FX"**

---
---
**Scott Marshall** *April 25, 2016 19:52*

Depends on the honeycomb you get, most are just a sheet with no brackets. If you want to save a buck, cut a piece of 1/4" plywood to fit and drive in 17 guage 1" or 1 1/4" brads in a 1" grid. 

Works great. So does the diamond shaped expanded metal.



As far as height/focus goes, most people just make up a shim set to go under the board or honeycomb.



If you go for an air assist, I recommend the Saite cutter rectangular one, it has an integral focus adjustment built in. (uses bigger lenses too) The downside to it is it takes some work to install - you have to open up the hole in the carriage and then shim it to get it adjusted.



The BIG up is you can focus within 1/2" range or so without moving your bed.



There's dozens of options, shop around good before you buy, everybody's different and different setups will suit them. Maybe you never move the bed because you always cut the same thickness? then a power bed adjust is a waste (which it kind of IS in a K40) 



I'd start with the nailboard, then decide if the $40-$50 for a honeycomb will do a better job for you.



Scott


---
**Pigeon FX** *April 25, 2016 20:03*

Thank you Scott, the advice is much appreciated, could you possibly link me to the Saite cutter rectangular one?


---
**Scott Marshall** *April 25, 2016 20:29*

[http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050?hash=item3abe742d92:g:kagAAOSwIrNWFIY7](http://www.ebay.com/itm/CO2-K40-Laser-Stamp-Engraver-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252303388050?hash=item3abe742d92:g:kagAAOSwIrNWFIY7)



That's just the head. You can get kits from them that include the lens and/or a red laser sight attachment.



The lens to fit it is 18mm 50.8 focal length (stock).

They have a great deal on the full optics set with the 3 mirrors (Molybendum which is the most durable and best for the k40 imo), and the lens which will fit the adapter. I use the standard lenses, the "better" ones are +10 bucks, may be worth it, but I've not bought one yet.



That stuff is probably the best upgrade bang for your buck, and only the tip of the iceberg.

The free upgrade it aligning it perfect.



Main thing with a new K40 is lining up the optics, here is a time saver.

[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



And good background info:

[http://www.ophiropt.com/](http://www.ophiropt.com/)


---
**Pigeon FX** *April 26, 2016 00:17*

**+Scott Marshall** Thank you so much for taking the time to help! that PDF is excellent.


---
**Anthony Bolgar** *April 26, 2016 23:42*

Here is a link to pics of how I made my manual bed [https://plus.google.com/photos/104696927629289302865](https://plus.google.com/photos/104696927629289302865)


---
**Anthony Bolgar** *April 26, 2016 23:43*

It is in the December  photos


---
**Pigeon FX** *April 27, 2016 02:48*

**+Anthony Bolgar** Thanks Anthony, that has given me a good idea of what is involved (how easy is it to get to the adjustment knobs under the bed), I'm thinking of moving the knobs to the top of the bed, and using compression spring around the 4 threaded rods, a little like a over sized Prusa i3 3D printer bed? 


---
**Anthony Bolgar** *April 27, 2016 02:59*

I just remove the top honeycomb layer to adjust the knobs, I have marked my most used settings with a black sharpie on the threaded rod, but I like your spring loaded idea, I will give it a try and see how it works.


---
**Anthony Bolgar** *April 27, 2016 03:11*

I also use strong rare earth magnets on the bottom frame to hold it in place. I am in the process of designing a stepper motor add on to make it easier to set the height, was going to prototype it with some 3d printed GT2 pulleys, and an old nema 17 motor I have laying around. My issue right now is figuring out how to join the belt into a continuous loop, hope someone on here knows how to do it.


---
**Pigeon FX** *May 05, 2016 01:32*

**+Scott Marshall** Hello Scott, was just looking at the Saite cutter heads you recommended, and was wondering if I got the one with the "Integrative Mount" I could just switch out the mounts saving the need to bore out and shim?



[http://www.ebay.com/itm/40W-Tube-CO2-Laser-Rubber-Stamp-Engraving-K40-3020-3040-Head-Integrative-Mount/252273200569?_trksid=p999999.c100623.m-1&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D20160323102634%26meid%3De8d39983236f4de38f3b2f0752e99c68%26pid%3D100623%26rk%3D4%26rkt%3D6%26sd%3D252303388050](http://www.ebay.com/itm/40W-Tube-CO2-Laser-Rubber-Stamp-Engraving-K40-3020-3040-Head-Integrative-Mount/252273200569?_trksid=p999999.c100623.m-1&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D20160323102634%26meid%3De8d39983236f4de38f3b2f0752e99c68%26pid%3D100623%26rk%3D4%26rkt%3D6%26sd%3D252303388050)


---
**Scott Marshall** *May 05, 2016 04:51*

I'm not sure which one yo mean by "integrative mount" (Integrated Mount maybe is what they mean?) It isn't that anyway,  it comes with a mount, but it's sure NOT integrated. Chalk it up to bad translation.



The set you linked to supplies the entire carriage, which doesn't need replacement to fit the head. The hole in the top plate needs enlarging however, and it appears the set comes with the top plate already bored to fit the new head. Changing the carriage is a fair amount of work and I wouldn't recommend it unless you 've got experience in this sort of machinery. (and good reason to do so) There's a lot of adjustments involved, and just getting the belt back in place and tensioned properly has tripped people up. It's not rocket science, but it's not as easy as it looks either. Then the endstops need setting. And frankly the whole affair is built so roughly, it's best left alone if it's working well.



If you can get a top plate pre-bored or can enlarge the hole in yours, that's all you really need. some have enough room to open up the hole, others need a new plate made from scratch from what I've heard.



I can make you a plate if you run into trouble, I have a little machine shop here.



The hole size is 20mm so measure yours and see if there's room to open it up without breaking through. If you have the room, 15 minutes with a Dremel and a 1/2" sanding drum,  you'll be all set. (or 1/2hr with a round file)



It's OK to buy the set, I just wouldn't change the carriage yet, put it away for future use. Odds are all you'll ever need from it will be the rollers though.



Edit - PS This is worth reading:

I was just working on a drawing (I think I'm going to offer a mounting plate with a vertical adjustment since I'm looking to make some of my equipment earn it's keep)

I needed a look at the stock plate (tossed mine as it came in trashed) so I looked over that Carriage/head combo from Saite.



I'm not sure they were paying attention when they put that deal up, because a set of 4 rollers is $13.49. The Head alone is $35.09. $35.09 + 13.49=$48.58



The combo costs $45.89! So buying the combo with the head, mounting plate and Carriage with wheels is $2.69 LESS than just the head and a set of rollers.



They're PAYING YOU $2.69 to take the mounting plate (which you need) and the loaded Carriage!!!



If I were you , I'd order it while it's available. You won't beat that deal.

Remember to order a lens and mirror for the new head, it takes larger optics.



Scott


---
**Pigeon FX** *May 05, 2016 14:14*

**+Scott Marshall** Thanks Scott, will grab one with the mount at the cheap price! 



For lens and mirrors is it the 3 pcs Mo Reflection Mirrors 20mm +1 pc ZnSe Focus Lens 18mm Focal length 50.8mm 



They seem to have the exact same kit for $42.29 and $49.99!?!?



[http://www.ebay.co.uk/itm/20mm-Reflection-Mirror-Mo-18mm-Znse-Lens-CO2-Laser-Stamp-engraving-40W-K40-3020-/252371628368](http://www.ebay.co.uk/itm/20mm-Reflection-Mirror-Mo-18mm-Znse-Lens-CO2-Laser-Stamp-engraving-40W-K40-3020-/252371628368)



[http://www.ebay.co.uk/itm/Mo-20mm-Reflective-Mirror-18mm-Znse-Lens-Focal-2-CO2-Laser-Stamp-Engraver-K40-/252283989821](http://www.ebay.co.uk/itm/Mo-20mm-Reflective-Mirror-18mm-Znse-Lens-Focal-2-CO2-Laser-Stamp-Engraver-K40-/252283989821)


---
**Scott Marshall** *May 05, 2016 14:20*

You are correct, as far as I can see. Seems there may be a little "one hand doesn't know what the other is doing" going on at Saite Cutter.



Hey, buy while the low prices are in effect!


---
**Anthony Bolgar** *May 05, 2016 14:59*

They were posting some auctions the last few months and I was able to win them at their starting price, 3 X 20mm MO mirrors for $12.99. Won 4 auctions like this, just received 2 of the packages today.


---
**Pigeon FX** *May 05, 2016 15:03*

**+Anthony Bolgar** Will add them to my watched seller list and try and grab some spares if they do more auctions. thanks for the heads up!


---
**Anthony Bolgar** *May 05, 2016 15:11*

I could not beleive that I won 14 day auctions with the minimum bid.


---
**Pigeon FX** *May 05, 2016 16:18*

**+Scott Marshall** all ordered! do you happen to remember the inner diameter of the air tube needed for the air assist on this head?


---
**Scott Marshall** *May 05, 2016 18:06*

I wouldn't be surprised if we're a good share of the business there.



The air tubing is about 6mm or 1/4" . There's no room to run a cable chain,  what seems to work is to anchor the tubing to the rear top lip (where the door closes) and point it toward the right. leave just enough slack so it doesn't pull tight, but no more, or it can wrap adound the head and bind up the whole works or get in the beam path, which of course destroys it pretty quick.



There's a place on ebay selling 1 meter length of colored polyurethane tubing for about 2 bucks. The PU is a bit stiffer than standard clear PVC  (vinyl) and it seems to work well.



If running from a commercial air compressor, you'll want a Filter-regulator unit. 1/4", 1/8" if you can get one. Again, ebay - about 10 bucks with gauge and mounting bracket. Shop carefully for the fittings, they can be expensive. Brass has gone up like gold lately.





Scott


---
**Pigeon FX** *May 05, 2016 18:53*

**+Scott Marshall** Thanks! was thinking of getting a coiled air hose kit with some barbed adapters like this: [http://www.ebay.co.uk/itm/Neilsen-Air-Tool-Connector-Kit-Coil-Hose-Tyre-Inflator-Blow-Gun-Guage-3T-/370751628131?hash=item565284e363:g:J6gAAOSwiylXBjkL](http://www.ebay.co.uk/itm/Neilsen-Air-Tool-Connector-Kit-Coil-Hose-Tyre-Inflator-Blow-Gun-Guage-3T-/370751628131?hash=item565284e363:g:J6gAAOSwiylXBjkL)



Hopefully that will make hooking it to my pump a little easier. I went with this 70L/min pump, and if that doesn't work will reluctantly use my really loud shop compressor:



[http://www.ebay.co.uk/itm/Hailea-ACO-328-Air-Compressor-with-Brass-Air-Divider-Taps-/222026332838?hash=item33b1ccfaa6:g:ZjMAAOSwB4NWw97Q](http://www.ebay.co.uk/itm/Hailea-ACO-328-Air-Compressor-with-Brass-Air-Divider-Taps-/222026332838?hash=item33b1ccfaa6:g:ZjMAAOSwB4NWw97Q)


---
**Scott Marshall** *May 06, 2016 01:04*

I couldn't make a coil hose fit. I tried 2" cable chain too, and there's just no place to put it.

Space is tighter than you think in there, none of the conventional stuff seems to fit. Maybe you'll figure out how to make use of the coilhose, but you might want to have some straight hose on hand just in case.



 Short of removing the lid and running the feedhose down from a boom, I can't figure out a way to use a coil.

 The air assist works so well, it's worth a little hose minding if that's what you have to do.



 I haven't tried one of the air pumps myself, I'm lucky to have a commercial compressor in an outbuilding from my shop, so you can't hear it. and it's large enough that it only comes on once an hour or so feeding 12lpm to the head.



 There's a lot of people using pumps, I can't imagine they're real quiet either and getting the right size is important because you can't just turn it up if turns out to be too small. I'd get advice on make and model from someone actually successfully using a pump on the same head.



Beware of advice on internet boards, (yes, even mine) there are a lot of "instruction manual experts" mixed in with some very knowledgeable ones, and it's sometimes hard to tell the difference. Always check out things through multiple sources. 

That's being said, this IS the place for hobby laser experimenters, and there's an incredible wealth of knowledge here. We're in the process of working to  make the answers to common questions into a reference section.

Pump specs and sources would be a good line item. 



Anyway, I can tell you that 12 - 20 lpm (measured with a Dwyer Rotameter) is usually adequate for plywood etc. With Acrylic, I turn it way down to where the ball is just rattling. If engraving, the lower end of the range is usually OK especially if you tape the workpiece to prevent smoke stains. That's all with the Saite head, and the high end is usually when I have a longer focal length lens in, which requires more distance between the head and workpiece, thus more air. The longer lenses cut thicker materials well, but are less precise, especially for fine engraving.

With the stock lens, you may get away with less flow, but if it's something like wood, you'll get flames if you turn it down too far. Beware laser fires....



Scott


---
**Scott Marshall** *May 06, 2016 01:39*

Sorry, Missed your question way back, found it doing some computer houskeeping, 

The ID on the Saite head is 20mm. 21max.


---
**Pigeon FX** *May 06, 2016 01:47*

Thanks Scott, I tried to hunt down the smallest coiled air line I could, as my first choose was a cable chain (is I have a few here left over from 3D printing projects, but as you say there is no way of that working) will certainly get some straight 1/4" tubing in, just in case! 



Regards the pump, I kept coming across K40 posts and blogs with that model in various powers from from 30-60L/min......so thought I would get the one up and can always use a tap to reduce the air as needed.....just hope its quieter then my compressor!. 



 


---
**Scott Marshall** *May 06, 2016 01:53*

Hard to say on a lot of that stuff, especially the Chinese stuff.  It's generally exaggerated severely. If you want a laugh run your water pump into a milk jug for 1 minute and see how it's flow compares with the box...



If the noise bothers you, but it in the basement, or outdoors. An old garden hose can be made into a low restriction pipeline...



So, Bigger is better, if you get half what they claim, I'd be surprised.

That said,  You can always turn it down....


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/ZmVgUGhtp4d) &mdash; content and formatting may not be reliable*
