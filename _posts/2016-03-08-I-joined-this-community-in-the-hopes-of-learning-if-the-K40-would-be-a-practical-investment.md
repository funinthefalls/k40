---
layout: post
title: "I joined this community in the hopes of learning if the K40 would be a practical investment"
date: March 08, 2016 05:43
category: "Discussion"
author: "Alex Krause"
---
I joined this community in the hopes of learning if the K40 would be a practical investment. From everything I've seen on here it requires alot of modding to get this machine in working condition (please correct me if I'm wrong). How much on top of the machine cost would I be looking to spend to get one of these in good working order?





**"Alex Krause"**

---
---
**Michael Bridak (K6GTE)** *March 08, 2016 06:08*

I personally spent about $50 in parts (parallel breakout board and 2 step drivers) getting mine working. 


---
**Phillip Conroy** *March 08, 2016 08:08*

Air pump for airr assist=50-120,air assist nossel 50,safety switches on lid 5 ,water flow switch so laser does not fire without water flowing 10,

I use sofware that came with machine ,didnt upgrade any boards ,i use mine at least 5 hours a day ,time to align mirrors 20 hours first time 2nd 10 ,3rd 2hours


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2016 08:49*

Mine works straight out of the box. I have made a few minor modifications: air assist (out of random things I had laying around) & I have replaced the original cutting bed with a perforated gal steel (around au$40). Other than that, I purchased an extension for my exhaust hose (around au$20).


---
**The Technology Channel** *March 08, 2016 11:18*

Modding can cost whatever you like, these machines work fine from the box, and modding is optional to improve the performance. If you get one with corel laser software, then its more than adequate for most things. Some people don't like the software or want more choice, so change the controller board for a more generic board, there are lots of these around, basically 3d printer boards. the cost can be anywhere from $50 to $500 for mods but you don't have to do them all at the same time. Best advice is to buy one, and just see what other things would make it nice to use. I have changed my mirrors lens added air assist, new fan etc, but out of the box it was cutting plywood and acrylic fine.


---
**Stephane Buisson** *March 08, 2016 13:08*

all in all about 500euro to 600 usd. and you got a good machine, with open source hardware and softwares.

Welcome in this community **+Alex Krause** .

(i still have to assemble my OX)


---
**Ben Walker** *March 08, 2016 13:25*

I am using mine stock besides a new lens and air assist.  Once you understand the software it really is a pleasure to use and I find myself using it more than say that 3D printer that I just had to have.  Wish I had pulled the trigger sooner.  Especially since I am patiently waiting for a new Glowforge.  




---
**Alex Krause** *March 09, 2016 03:31*

**+Stephane Buisson** is that total cost with machine plus mods or does the mods cost more than the machine and I would be looking at spending 1000 USD to get it highly functional 


---
**Vince Lee** *March 09, 2016 04:37*

I concur with most of the other posts here.  You don't really <b>need</b> to spend anything to get a fully functional machine, but once you get started you might get the modding bug and <b>choose</b> to upgrade it to some degree.  I must admit that I enjoy the hacking and do it more for fun than need.


---
**Alex Krause** *March 09, 2016 04:38*

Yah I totally understand that I own a Ox CNC 


---
**Alex Krause** *March 09, 2016 04:43*

Recently I have been diagnosed with a serious illness it's know as DIYtis there is no need to call a physician or schedule any appointments. I just want everyone to be aware of the effects of this serious illness in which symptoms may include but not limited to acute attention to detail, analytical breakdown of mechanical processes, an overwhelming urge to improve the world around, and most importantly never knowing when to stop learning.


---
**Stephane Buisson** *March 09, 2016 06:22*

**+Alex Krause** total cost.

mods, well I disagree, it's about to be able to use proper softwares, without proprietary constrainst. it's about efficientcy, air assist is a must have., good lens/mirrors too.


---
**Vince Lee** *March 09, 2016 21:25*

IMHO, "good" is subjective.  That being said, the software and lack of air assist, and lens are probably three of the weakest parts of a stock machine.  I paid $20 for a replacement lens, which gave me maybe 20% more power, and added a simple 24V fan onto the head for air "assist", which cost maybe another $25.  My mirrors were ok.



Even though I have a smoothieboard and will be hooking it up sometime in the future, I've done plenty of projects with MoshiDraw.  It recently added support for importing structured drawing files in a number of formats, so it's not a completely closed ecosystem anymore.  Still not great, but usable. 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/aLpprTGTVXg) &mdash; content and formatting may not be reliable*
