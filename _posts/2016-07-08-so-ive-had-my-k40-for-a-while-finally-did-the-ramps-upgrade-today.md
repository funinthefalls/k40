---
layout: post
title: "so ive had my k40 for a while, finally did the ramps upgrade today"
date: July 08, 2016 19:05
category: "Discussion"
author: "Luke Rogers"
---
so ive had my k40 for a while, finally did the ramps upgrade today.  having some issues.  i can control the steppers and test fire the laser no problem.  im having a issues with it auto homing, and i cant get the inkscape turnkey plugin to show up.  any ideas?





**"Luke Rogers"**

---
---
**Derek Schuetz** *July 08, 2016 19:45*

Are your end stops configured correctly? Are they wired correctly?


---
**Luke Rogers** *July 08, 2016 20:11*

they are wired correctly, when i say auto home from the menu i have no movement from either x or y axis other then a quick jerk


---
**Derek Schuetz** *July 08, 2016 20:32*

Ok run an M119 command to check the status of your end stops when the carriage is in the home location


---
**Luke Rogers** *July 08, 2016 20:48*

says all are triggered


---
**Derek Schuetz** *July 08, 2016 20:49*

And when you do m119 with the carriage no in the home location


---
**Luke Rogers** *July 08, 2016 20:52*

still says x,y,z are all triggered no matter location


---
**Derek Schuetz** *July 08, 2016 21:01*

Ok so that's your issue. It is either wiring or something in your config


---
**Luke Rogers** *July 08, 2016 21:07*

thanks ill look at the config, but im still sitting with a paperweight since i cant get a gcode to compile using inkscape lol


---
**Derek Schuetz** *July 08, 2016 21:08*

Ya I'm not 100% sure on the inkscape thing I just followed all the steps and it worked don't know how to trouble shoot that


---
**Luke Rogers** *July 08, 2016 21:08*

any alternatives to inkscape?


---
**Derek Schuetz** *July 08, 2016 21:36*

Not that I could find that worked well with ramps


---
**Luke Rogers** *July 08, 2016 21:47*

thanks i got the endstops fixed


---
**Alex Hodge** *July 12, 2016 14:24*

So what was the issue with the endstops. I think I've got the same problem...


---
**Luke Rogers** *July 13, 2016 02:33*

Issue was with the 5v and ground were backwards.  But still both axis won't home just one will.  I had to go back to the stock board


---
*Imported from [Google+](https://plus.google.com/+LukeRogerslukegunstar/posts/GPZY9m2ewiH) &mdash; content and formatting may not be reliable*
