---
layout: post
title: "Looking for some advice. I have a K40 laser with the Cohesion 3d Mini board"
date: January 22, 2018 08:48
category: "External links&#x3a; Blog, forum, etc"
author: "Richard Gallatin"
---
Looking for some advice. I have a K40 laser with the Cohesion 3d Mini board. I want to get a rotary attachment that will work for doing pens. What should I look for and will I need to upgrade anything else. 





**"Richard Gallatin"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2018 00:11*

[https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)


---
**Steve Clark** *January 25, 2018 01:11*

On the RD Works site I saw a jig that allows the Y axis to rotate a tube or pin. You might do a search on that site. Oh wait, here it is:




{% include youtubePlayer.html id="mCQqn0kfvCo" %}
[youtube.com - RDWorks Learning Lab 123 Let's Test our DIY Rotary Engraver](https://www.youtube.com/watch?v=mCQqn0kfvCo&t=415s)



There is also a post where he shows how to make it.


---
**Steve Clark** *January 25, 2018 01:15*

**+Ray Kholodovsky**  Thanks Ray, I was just going to look that up as I'm in the process of building a stepper driven rotory axis and up pop's your answer!!


---
*Imported from [Google+](https://plus.google.com/102728020130316654078/posts/WkAf8C95wbe) &mdash; content and formatting may not be reliable*
