---
layout: post
title: "Somewhat related to what I posted earlier, for those of you that are using Inkscape, what's the hardware in your machine?"
date: March 01, 2016 20:02
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Somewhat related to what I posted earlier, for those of you that are using Inkscape, what's the hardware in your machine? Ramps? A DSP? Something else? And what's your work flow like? Inkscape -> what?





**"Ashley M. Kirchner [Norym]"**

---
---
**Jim Hatch** *March 01, 2016 23:50*

Stock right now but I have the board & parts to do the Ramps conversion (so I can use Peter's cool stuff). I use both Illustrator and Inkscape. I go from Illustrator or Inkscape to BMP and then import the BMP into LaserDRW. Then LaserDRW to cut or engrave. If I need to do both for a project I create different BMPs and don't touch the material between the engrave & cut steps so I don't mess up the alignment. 



It's a little tedious but similar to what I have to do with the big Legacy laser I also use. That one I can create a layered DWG file and after import (into LaserCut) I can specify engraves and cuts by line color and do it all in one file.



Theoretically I could go direct from Corel to either laser but the version of Corel that came with my K40 is virus infected so I won't use it and since I'm going to switch to Ramps/Peter's software I haven't messed around with getting a legit copy of Corel working.


---
**Ashley M. Kirchner [Norym]** *March 02, 2016 01:49*

Well yeah, that's my current workflow as well (except I use high resolution PNG files instead of bitmaps - both are raster images, so it really doesn't matter.) However, I want to move away from having to do that. I want to shoot straight up vector files to the laser, the same way we do our CNC at the shop. We create files in Illustrator, red lines will cut, blue lines will engrave. There IS a step in between, the CNC software. It interprets the Illustrator file. I keep hearing from folks who use Inkscape with a plugin so I was wondering about that as well. Does it know to use layers and can that be exported directly to the laser and engrave/cut at the same time. I hate having to do the multiple steps now ... well, maybe hate is a strong word ... no, no it's not, I do, I hate the multi steps. :)


---
**Jim Hatch** *March 02, 2016 02:42*

Well, you can do that if you install the Corel that came with the laser and then install the CorelLaser file which creates the direct drivers for Corel to the laser. Then you can bypass the intervening steps with LaserDRW. You'll get those same icons in the toolbar in Corel that you have in LaserDRW to send the file to the printer.



I didn't do that because the CorelLaser software is for an earlier version of Corel than I have and the version they shipped me had a virus my AV software found when trying the install. It's possible it's not a virus and turning off the AV would let it install but because the laptop is on my home wireless and thus the Internet I don't want to trust it to be a false report. After all it's a pirate copy of Corel in the first place. Not exactly trust inducing.


---
**Ashley M. Kirchner [Norym]** *March 02, 2016 03:02*

Exactly, I refuse to run pirated software. Not to mention it didn't work when I installed it on my Win10 system. I own the full Adobe suite, no reason I can't work with that ... other than licensing fees and proprietary file types and what not. Thanks Adobe. :)


---
**Justin Mitchell** *March 02, 2016 10:57*

Using the original control board, corel just crashes, so its Inkscape -> emf file  -> LaserDRW.   I use emf as it should in theory remain as vectors.


---
**Ashley M. Kirchner [Norym]** *March 02, 2016 17:10*

Yeah, apparently that doesn't happen. Someone sniffed out the data being sent and figured out that it's sending raster data, which the controller than traces back into vectors. I forgot who did that, but that seems to be what those machines are doing.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/5tGNVsTCNNS) &mdash; content and formatting may not be reliable*
