---
layout: post
title: "Hi there, I want to add some Art to my IPhone 6 in aluminum as seen in my design sketch below, how would you solve it?"
date: June 21, 2018 05:14
category: "Discussion"
author: "BEN 3D"
---
Hi there,



I want to add some Art to my IPhone 6 in aluminum as seen in my design sketch below, how would you solve it?



I think I could try to spray the phone black and then laser the not wanted paint away.

Is there a special kind of color that you would use?



Kind Regards

Ben

![images/6269c82aa27e1d1cffcdfc1dee90e304.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6269c82aa27e1d1cffcdfc1dee90e304.png)



**"BEN 3D"**

---
---
**Joe Alexander** *June 21, 2018 05:24*

if its anodized you can try engraving it directly, some colors work well for this. or use a product like thermark or Cermark that you spray on and laser-bond to the substrate. Its a bit pricey though, some try moly spray.


---
**BEN 3D** *June 21, 2018 06:44*

Hey Joe thanks, I will take a look to it and google about it. 



Another possible way, may could be to add a  self-adhesive foil and then cut them. Remove the parts that I will color. Corlor it, and remove the foil after it is finished completly ....


---
**Joe Alexander** *June 21, 2018 08:33*

that sounds feasible as long as you can remove the areas you want without peeling up the rest.


---
**HalfNormal** *June 21, 2018 12:37*

Cermark will work but it is pricey.


---
**Chuck Comito** *June 21, 2018 22:39*

I've heard that the spray on molycoat works similar to the cermark. Maybe google some photos and techniques? I have not tried this!! :-)


---
**BEN 3D** *June 22, 2018 13:49*

CerMark looks perfect


{% include youtubePlayer.html id="PVscWOHZ1bQ" %}
[youtube.com - CerMark and TherMark Laser Spray for Metal](https://youtu.be/PVscWOHZ1bQ)


---
**BEN 3D** *June 22, 2018 13:58*

Wow 300ml off this color cost 100€, really pricey 🙈


---
**HalfNormal** *June 22, 2018 14:00*

Use sparingly! Believe it or not if you search, I posted about using yellow mustard to etch metal.


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/3BBndEgbZw6) &mdash; content and formatting may not be reliable*
