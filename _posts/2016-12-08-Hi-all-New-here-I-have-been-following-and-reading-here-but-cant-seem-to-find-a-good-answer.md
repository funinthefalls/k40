---
layout: post
title: "Hi all, New here. I have been following and reading here but can't seem to find a good answer"
date: December 08, 2016 20:33
category: "Discussion"
author: "robert mackert"
---
Hi all, New here. I have been following and reading here but can't seem to find a good answer. I have one of the cheap o ebay cutters. recently it has stopped working. There is a faint beam from the laser tube but will not mark on paper. all other movements work. How do I figure out what is wrong with it? Or do I just start replacing parts till it works. Thanks any help is appreciated.





**"robert mackert"**

---
---
**greg greene** *December 08, 2016 20:48*

Alignment, alignment, alignment - go to the flying wombat laser alignment website and follow the procedure :)  We have all been through this at some point


---
**J DS** *December 08, 2016 21:02*

I'm with Greg on this 


---
**robert mackert** *December 08, 2016 21:20*

The alignment may not be perfect but it was working just fine. placed paper at 1st mirror out of the tube. no burn I can understand alignment if not burning at work surface, but should not be an issue here. I heard a faint pop right after starting a task. No burning smell. Not sure if Power supply is bad or the tube went.


---
**greg greene** *December 08, 2016 21:29*

Ah, Understood then, yes - examine the tube to see if there are any water leaks or other indications of damage.  Next check to ensure the HV wire is not cracked or touching the case, and is clean on the end that attaches to the tube - it can't be soldered so it is just wrapped around the lead and sealed with silicone (That tube of stuff that came with the laser).  If loose or dirty, clean it and reconnect and reseal.  This has been a common problem reported by others.  If that is ok, then wearing SAFETY GLASSES rated for the laser, open the laser compartment and test fire the laser - you should see the Nitrogen gas ionizing fairly brightly in the tube.  If it isn't then it is likely the cheapo PSU.  BE CAREFULL - your exposing a High Voltage that is potentially lethal - so make sure you are not touching the tube or HV lead when you test fire it.


---
**robert mackert** *December 08, 2016 21:35*

Great info..I'll check it out..


---
**robert mackert** *December 09, 2016 23:02*

Back to this. I have been looking online for a new PSU . Not sure why mine has stopped working yet. Are there any that are plug and play so to speak with the cheap ebay lasers? Also any ideas of where to purchase trustworthy equipment. like power or even a tube.


---
**greg greene** *December 09, 2016 23:03*

Check out Light Objects - they had some a while ago


---
**robert mackert** *December 09, 2016 23:13*

got it..thanks..


---
**robert mackert** *December 12, 2016 20:39*

Had a little time today to check out the machine. It powers up. resets to home. all the internal fans come on. No visible cracks on the tube, no leaking water. Fuse in the plug is good..What else can I check to narrow what is wrong with it? Or is there a tell tale sign that it is either the power supply or the tube? Any info is great.


---
*Imported from [Google+](https://plus.google.com/106610826801074938394/posts/NXgexE1mGYs) &mdash; content and formatting may not be reliable*
