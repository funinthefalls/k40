---
layout: post
title: "Hi, could anyone post a photo with a laser cabinet interlock?"
date: February 07, 2017 11:45
category: "Modification"
author: "Jorge Robles"
---
Hi, could anyone post a photo with a laser cabinet interlock?





**"Jorge Robles"**

---
---
**Don Kleinschnitz Jr.** *February 07, 2017 13:11*

Below is the link to my blog, look under the section on interlocks for wiring information. The mechanicals I made by hand for each cover and most any size hookup wire will work.



The linked switch assy. are a nice way to modular-ize the connections. They may need to be modified to run in series configuration. I have a set on order to find out if and how.



[https://www.amazon.com/gp/product/B01CS82B8K/ref=crt_ewc_title_srh_2?ie=UTF8&psc=1&smid=A11A70Q280RHPK](https://www.amazon.com/gp/product/B01CS82B8K/ref=crt_ewc_title_srh_2?ie=UTF8&psc=1&smid=A11A70Q280RHPK)



[donsthings.blogspot.com - A Total K40-S Conversion Reference](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Jorge Robles** *February 07, 2017 13:40*

Thanks +Don Kleinschnitz, I was looking for a good place to put the switch on the laser tube cabinet physically. I made up one, printing right now. I will post when installed :)


---
**Jorge Robles** *February 07, 2017 19:35*

Et voilà

![images/05b72414f92b6eb948c85dfdc943218c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05b72414f92b6eb948c85dfdc943218c.jpeg)


---
**Fred Padovan** *February 12, 2017 02:29*

Nice,  any chance I could get the stl file for that mount


---
**Jorge Robles** *February 12, 2017 09:09*

k40 printed helpers found on #Thingiverse [thingiverse.com - k40 printed helpers by jorgerobles](https://www.thingiverse.com/thing:2092572)


---
**Fred Padovan** *February 12, 2017 16:28*

Thank you very much 

They are printing now




---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/8tfMMpQhK5Q) &mdash; content and formatting may not be reliable*
