---
layout: post
title: "In case its of interest to anyone, I thought I'd share a picture of the bed I made a year or so ago"
date: August 09, 2017 10:42
category: "Modification"
author: "Chris Horne"
---
In case its of interest to anyone, I thought I'd share a picture of the bed I made a year or so ago. 

The base plate is 3mm steel, the supports are aluminum studs fixed to magnets with countersunk screws. 

The biggest advantage is that the supports can be moved around to support the work but out of the way of the beam.  

The two oddly shaped posts (also on magnets) are used as a registration for example when engraving both sides of something or when cutting something too big to cut in one go.

When I am cutting thin materials I just put a 1.6mm thick copper clad pcb board on top which seems to work well.

![images/3064465cf8ab94f2685ab968a88f657d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3064465cf8ab94f2685ab968a88f657d.jpeg)



**"Chris Horne"**

---
---
**Phillip Conroy** *August 10, 2017 09:35*

Nice


---
*Imported from [Google+](https://plus.google.com/100135238230325816942/posts/QwD2GJ6an6U) &mdash; content and formatting may not be reliable*
