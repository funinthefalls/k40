---
layout: post
title: "Hello Everyone, So, I think our laser tube might be dying!"
date: April 28, 2015 02:29
category: "Hardware and Laser settings"
author: "Bee 3D Gifts"
---
Hello Everyone, So, I think our laser tube might be dying! Very strange since we JUST got in about a month ago. But it is NOT cutting through 3mm Acrylic. Also, if we turn it up to 99.9, the laser completely disappears!? Has this happened to anyone else? We aligned all the mirrors! It hits right in the middle! Very confusing. Also, anyone ever cut acrylic with the paper film on? Catches fire, and it never really used too. Anyways..any thoughts?





**"Bee 3D Gifts"**

---
---
**Sean Cherven** *April 28, 2015 02:40*

I had an issue similar, mine was caused by the beam hitting the edge of the lens. Had to raise the laser tube up by putting a wedge under the tube.


---
**Sean Cherven** *April 28, 2015 02:41*

And I noticed the paper catching fire too. I think it depends on the type of paper film they used.


---
**Bee 3D Gifts** *April 28, 2015 15:54*

Thanks Sean. I suspected it was the tube!! For the paper film, we wedge a small fan under the lid and close it half way to try and avoid the flames..lol. There is a place on Amazon that sells acrylic with the clear film I think called ABLEDIY. At least teh black comes with a clear film. Its much nicer!


---
**Sean Cherven** *May 11, 2015 13:29*

Because the 3rd mirror is only adjustable by rotating the cutting head. If the laser is too high or too low, it can only be adjusted by raising or lowering the laser tube. Unless somebody knows something I don't, that's the only way I know to fix that issue.


---
**Bee 3D Gifts** *May 11, 2015 17:52*

We also found out our laser was getting as cool as it should! I checked the water and it was luke warm!!! Now, I put ice in there and keep it cool and it is cutting much better!


---
*Imported from [Google+](https://plus.google.com/101949715741571451458/posts/92AuMShUnqu) &mdash; content and formatting may not be reliable*
