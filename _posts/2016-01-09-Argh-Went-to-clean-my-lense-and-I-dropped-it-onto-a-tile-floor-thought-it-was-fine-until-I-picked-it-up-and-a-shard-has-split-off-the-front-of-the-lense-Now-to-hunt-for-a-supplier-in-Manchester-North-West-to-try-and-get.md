---
layout: post
title: "Argh!! Went to clean my lense and I dropped it onto a tile floor thought it was fine until I picked it up and a shard has split off the front of the lense :( Now to hunt for a supplier in Manchester/North West to try and get"
date: January 09, 2016 11:52
category: "Discussion"
author: "Pete OConnell"
---
Argh!! Went to clean my lense and I dropped it onto a tile floor thought it was fine until I picked it up and a shard has split off the front of the lense :(



Now to hunt for a supplier in Manchester/North West to try and get another today. If not its onto eBay and a 3 week wait.





**"Pete OConnell"**

---
---
**Jim Hatch** *January 09, 2016 14:33*

You may still be able to use it if the break was far enough from the beam path.


---
**Josh Rhodes** *January 09, 2016 15:45*

I recently found out about [http://www.lightobject.com](http://www.lightobject.com)


---
**Scott Marshall** *January 10, 2016 04:24*

Got my lens/mirror set from Saite cutter in 7 days. That was during the Christmas rush too.


---
**Pete OConnell** *January 11, 2016 17:02*

**+Jim Hatch** it was close to the centre to I doubt it, will buy a new one


---
**Pete OConnell** *January 11, 2016 17:17*

is Saite cutter on eBay? I did a quick google search and can't find anything


---
**Pete OConnell** *January 11, 2016 17:34*

found it - for anyone else it's [http://stores.ebay.com/Saite-Cutter](http://stores.ebay.com/Saite-Cutter)


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/QCen6doku66) &mdash; content and formatting may not be reliable*
