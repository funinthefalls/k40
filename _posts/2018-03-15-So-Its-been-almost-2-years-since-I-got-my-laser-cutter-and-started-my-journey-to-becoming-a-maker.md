---
layout: post
title: "So It's been almost 2 years since I got my laser cutter and started my journey to becoming a maker"
date: March 15, 2018 01:37
category: "Discussion"
author: "Ned Hill"
---
So It's been almost 2 years since I got my laser cutter and started my journey to becoming a maker.  In that time I've become much more proficient in creating graphic designs ( I started with almost no experience).  Tonight I was revisiting an old project that I had started and didn't follow through on because it looked like too much graphic design work.  Now that I've looked at it again I'm like "God, I was such a newbie" and then got it done in an hour :P  Moral of the story,  always keep pushing, always keep making, learning and growing.  





**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 15, 2018 05:16*

You've done well in 2 years Ned. Good to see you still pushing the limits & boundaries of what you're capable of




---
**Don Kleinschnitz Jr.** *March 15, 2018 11:01*

Am I right that you are running a stock controller?


---
**Ned Hill** *March 15, 2018 12:38*

**+Don Kleinschnitz** Yes I'm currently still running the  stock controller.  I do have a C3D mini board to install with a bunch of other parts I've been collecting from your list of suggestions on your blog.   I've got to order a new tube soon as mine is fading, so a major rebuild is in my near future. :)


---
**Don Kleinschnitz Jr.** *March 15, 2018 13:00*

...and using a stock controller makes your MAKING even more impressive!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/SMaRpzC2WGD) &mdash; content and formatting may not be reliable*
