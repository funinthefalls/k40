---
layout: post
title: "Yet another question from the newbie looking into joining the club...."
date: May 25, 2016 20:24
category: "Discussion"
author: "Purple Orange"
---
Yet another question from the newbie looking into joining the club.... 



Is the Analog or the Digital K40 preferred (and why so) ? 





**"Purple Orange"**

---
---
**Anthony Bolgar** *May 25, 2016 20:34*

Analog lets you set the exact amount of power in mA. The digital machine  just sets the power as a percentage of power, ie. on the digital if you set it to 50 it powers the laser at 50% power, unknown how many mA that is. At least that is what was explained to me before. If anyone has a better answer, or correct answer if mine is wrong please comment so I will know for the future.


---
**Sunny Koh** *May 26, 2016 02:03*

I personally prefer digital as it is about repeatability of settings. as you don't have an exact way to say much much of the dial is turn. However, the issue is that you only know the mA reading when the Laser is firing. So it is guess work when working out settings. Adding a after market Ampere meter will get you the best of both worlds. After all, when you decide to switch from the stock board, you can are calling the power to the laser as a percentage.


---
**Stephane Buisson** *May 26, 2016 08:45*

+1 analog, mA reading is accurate in the tube lifespan, pot adjustment isn't by step increment. also cheaper.

if you do your own control panel, more easy to design.


---
*Imported from [Google+](https://plus.google.com/112469922804049225807/posts/iGimc5ZqgjB) &mdash; content and formatting may not be reliable*
