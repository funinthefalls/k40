---
layout: post
title: "What is the best program to edit the Smoothieware configuration file?"
date: May 08, 2016 14:35
category: "Software"
author: "Anthony Bolgar"
---
What is the best program to edit the Smoothieware configuration file? It looks like crap in notepad.





**"Anthony Bolgar"**

---
---
**ThantiK** *May 08, 2016 15:09*

I've always used notepad++, leafpad, nano ﻿(windows, Linux GUI, and command line respectively) 


---
**Anthony Bolgar** *May 08, 2016 15:12*

In the smoothie documentation it says not to use Notepad++ as it can cause the config file to become unreadable.


---
**Custom Creations** *May 08, 2016 15:14*

I always use WordPad for that stuff.


---
**Jean-Baptiste Passant** *May 08, 2016 15:14*

I use Visual Studio Code, overkill but I did not install more software on my computer.


---
**Arthur Wolf** *May 08, 2016 15:19*

**+Peter van der Walt** It'll arbitrarily decide the file's format is not how it likes it and change everything. Changes line endings, encoding etc. Users of notepad++ frequently report problems.


---
**Sebastian Szafran** *May 08, 2016 15:23*

I use Notepad++ with no problems.


---
**Arthur Wolf** *May 08, 2016 15:23*

**+Sebastian Szafran** It works for some.


---
**Anthony Bolgar** *May 08, 2016 15:27*

I have visual studio, so I'll give that a try.


---
**Sebastian Szafran** *May 08, 2016 15:32*

Windows Notepad is great for cleaning files before pasting into Notepad++. And after that I have no issues editing and saving files in N++.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/W5tbC65BQq5) &mdash; content and formatting may not be reliable*
