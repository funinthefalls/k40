---
layout: post
title: "What part are people using for the exhaust port in rear of machine?"
date: May 13, 2016 01:13
category: "Discussion"
author: "Custom Creations"
---
What part are people using for the exhaust port in rear of machine? I have seen links to a part on Amazon but I can not find it now :(



TIA





**"Custom Creations"**

---
---
**Ulf Stahmer** *May 13, 2016 02:14*

This one has been mentioned in posts on this forum [http://www.ebay.ca/itm/Brand-New-4-In-Line-Marine-Bilge-Air-Blower-DC-12V-6Amp-270-CFM-Quiet-for-Boat-/121861740679?hash=item1c5f869887:g:zVMAAOSwEetV9xqs&vxp=mtr](http://www.ebay.ca/itm/Brand-New-4-In-Line-Marine-Bilge-Air-Blower-DC-12V-6Amp-270-CFM-Quiet-for-Boat-/121861740679?hash=item1c5f869887:g:zVMAAOSwEetV9xqs&vxp=mtr)

Search "exhaust" and "marine" in the community search field on the left to see member's posts.


---
**Custom Creations** *May 13, 2016 02:20*

**+Ulf Stahmer** Thanks, but I am looking for the part that attaches onto the back of the machine in the slots like the stock POS fan does. Not the actual fan itself ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 02:42*

None, I just removed the exhaust port & put the fan back where it was. Probably not ideal, but I will sort something better when I get around to it.


---
**Ulf Stahmer** *May 13, 2016 03:14*

Oh, I remember seeing something someone posted about buying a flange at HomeDepot.  I made my own for a few dollars out of a 3-1/4" x 10" duct takeoff and some spare plywood.  I posted the photos here.  Search on "exhaust" to see them.


---
**Custom Creations** *May 13, 2016 03:16*

I bought this. Was cheap and said to fit perfectly.

[http://www.amazon.com/POWERTEC-70150-Rectangular-Dust-4-Inch/dp/B00M3JFNOO?ie=UTF8&psc=1&redirect=true&ref_=ox_sc_act_title_3&smid=ATVPDKIKX0DER](http://www.amazon.com/POWERTEC-70150-Rectangular-Dust-4-Inch/dp/B00M3JFNOO?ie=UTF8&psc=1&redirect=true&ref_=ox_sc_act_title_3&smid=ATVPDKIKX0DER)


---
**Ulf Stahmer** *May 13, 2016 03:22*

Yes. That's the one I saw posted.  Hope it works for you!


---
**Greg Curtis (pSyONiDe)** *May 13, 2016 15:52*

I bought the 4" bilge blower, ~250cfm and a 4" flex hose. Pinched that directly into the opening on the rear and liberal use of the aluminum (real) duct tape.



It is advisable to put the blower nearest the outer end of your exhaust, this creates a negative pressure along the entire run, helping to prevent fume leaks.


---
**Custom Creations** *May 13, 2016 16:43*

**+Greg Curtis** I bought a 6". It was cheaper than most 4" ones so I am going to use a 6"-4" reducer. Yes, I was going to put the fan on the end of the tube opposite of the exhaust port of the machine.

[http://www.ebay.com/itm/172203049670?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/172203049670?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Don Kleinschnitz Jr.** *May 15, 2016 03:23*

I used a floor register vent something like this: [http://www.lowes.com/pd_36418-85334-GVL0084___?productId=3134369&pl=1&Ntt=floor+register+box](http://www.lowes.com/pd_36418-85334-GVL0084___?productId=3134369&pl=1&Ntt=floor+register+box)

 There are lots of choices of these types.


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/C9K3LBHRJ7d) &mdash; content and formatting may not be reliable*
