---
layout: post
title: "Can I use your software with this board?"
date: May 21, 2016 18:26
category: "Software"
author: "Brandon Smith"
---
Can I use your software with this board?



Lasersaur Shield K40 Laser USB Controller for Replacement of MoshiDraw Controller



Miles-milling.com



Currently running lasersaur interface. Would love to try this one also.





**"Brandon Smith"**

---
---
**HalfNormal** *May 22, 2016 16:48*

The product looks like a "middle man style board" that connects to a Ramps/GRBL/Arduino board. They are also confusing the M2Nano and Moshi boards. For the same price you can purchase a smoothie board and have a lot more versatility IMO.


---
**Brandon Smith** *May 22, 2016 17:09*

What additional versatility are you referring to?


---
**HalfNormal** *May 22, 2016 17:16*

You can get the network and/or wireless options for a little more money, faster processor and a vibrant community that has support and constantly updating the smoothie software. Most of the Ramps/Arduino for laser firmware is dead ended. Cadillac vs a VW bug.


---
**Brandon Smith** *May 22, 2016 17:42*

Fair enough. But at what end. It has all the needed functionality that most users require. Networking...vnc, just like my cnc router. Reminds me of people that endlessly tune their cars or motorcycles looking for every last bit of power. At the end of the day, we can still cut and engrave the exact same projects. The biggest issue is to free the k40 from the moshi board. I just wanted a simple drop in solution. There is a reason vw bug was one of the best selling cars of all time...


---
**HalfNormal** *May 22, 2016 18:16*

**+Brandon Smith** Why pay the same money for the VW as the Cadillac? If the board was cheaper, that is one thing, but they are the same price. In fact you can buy a Cheap Chinese smoothie knockoff for half the price as some have done here.


---
**Alex Krause** *May 23, 2016 00:31*

**+Brandon Smith**​ I believe it will be a week or two and official Smoothie 3xc boards will be back in stock price tag is about 110$USD if you are in the USA head on over to the Uberclock website if you are located in Europe they can be obtained from robotseed. If you don't want to wait that long I believe Panucatt has the 3x smoothie based azteeg board in stock currently 


---
*Imported from [Google+](https://plus.google.com/109949255412543943799/posts/2ybBEGTwuS2) &mdash; content and formatting may not be reliable*
