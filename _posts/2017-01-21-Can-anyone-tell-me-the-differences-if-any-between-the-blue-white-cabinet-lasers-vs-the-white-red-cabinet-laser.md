---
layout: post
title: "Can anyone tell me the differences (if any) between the blue/white cabinet lasers vs the white/red cabinet laser?"
date: January 21, 2017 19:16
category: "Discussion"
author: "timb12957"
---
Can anyone tell me the differences (if any) between the blue/white cabinet lasers vs the white/red cabinet laser?

I don't see any and there is a big price difference.



[http://www.ebay.com/itm/High-Precise-40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/201264099304?hash=item2edc466fe8:g:yeYAAOSwvUlWsyC2](http://www.ebay.com/itm/High-Precise-40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/201264099304?hash=item2edc466fe8:g:yeYAAOSwvUlWsyC2)



[http://www.ebay.com/itm/Premium-High-Precision-40W-CO2-Laser-Engraver-w-USB-port-12-x-8-for-Crafting/172183770994](http://www.ebay.com/itm/Premium-High-Precision-40W-CO2-Laser-Engraver-w-USB-port-12-x-8-for-Crafting/172183770994)





**"timb12957"**

---
---
**Ned Hill** *January 21, 2017 20:08*

Doesn't appear to be any difference as far as I can tell.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 21, 2017 22:04*

The red one appears to have 1 extra power outlet on the back of it & a slightly better written description on the switch panel. That seems to be it that I can tell.


---
**Tony Sobczak** *January 21, 2017 23:09*

And that company is great to do business with.


---
**Austin Baker** *January 22, 2017 08:57*

**+Yuusuf Sallahuddin** I bought the red one specifically because of the better translated text on the main panel -- I figured that meant it had a better chance of being a slightly newer revision, but I don't really think it makes any difference functionally. 


---
**timb12957** *January 22, 2017 14:06*

Thank you all for your input. Seems we are all of the same opinion.


---
**Bill Keeter** *January 22, 2017 20:11*

I got the Red K40 myself. So far all my mods and updates have been identical to anyone using a blue version. So I wouldn't worry. 



I think my version just has mechanical end stops and no ribbon cable. Which is what I prefer. But that's no guarantee that other Red K40's would be the same.


---
**timb12957** *January 22, 2017 20:51*

If you look at all the pics closely, some of the blue ones have mechanical endstops and some do not. Seems all the red ones have mechanical endstops.


---
**Nick Hale** *January 23, 2017 00:34*

On amazon under the questions section, the seller claims the red one comes with the illegal pirated Corel and the blue does not.  They stated that was the only difference. 


---
**Bill Keeter** *January 23, 2017 00:48*

Haha. They both use pirated copies. 



..Or if I'm wrong please jump in someone that received an official licensed copy. Lol


---
**Austin Baker** *January 23, 2017 04:17*

**+Nick Hale** The disc included with my red machine did not include Corel -- just laserdrw and the corellaser plugin. I had to get Corel elsewhere.


---
**Austin Baker** *January 23, 2017 04:21*

**+timb12957** What do you mean by mechanical end-stops? The limit-switches? What do the blue machines have?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 23, 2017 17:50*

**+Austin Baker** I have optical end-stops/limit-switches on my blue k40.

edit: I should note I purchased around September of 2015.


---
**timb12957** *January 24, 2017 23:37*

My Blue K40 purchased July 2016 has optical end stops, however the pictures of some of the blue machines being sold now, have mechanical limit switches.


---
**timb12957** *February 02, 2017 21:11*

I had one seller tell me they were from different Chinese suppliers.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 03, 2017 01:27*

**+timb12957** I'd imagine that could definitely be the case, since there were a few different laser manufacturers that I've seen when searching around for CorelLaser software in the past.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/bRFUQw94VKK) &mdash; content and formatting may not be reliable*
