---
layout: post
title: "Ok I have tried all day!.... I've been messing with a simple object ( tic tac toe board) and it just not in the correct location"
date: April 15, 2016 02:01
category: "Software"
author: "giavonni palombo"
---
Ok I have tried all day!.... I've been messing with a simple object ( tic tac toe board) and it just not in the correct location. Its 6"x6" ive placed it in the upper left in corel and also in the upper left in laserdrw. Its engraving  it about 1 and a half inches down from where it should be. The X axis is where it should be.



What am I doing incorrect? Oh and I know its probably simple and obvious. 





**"giavonni palombo"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2016 02:11*

Check the ReferX & ReferY positions on the engraving/cutting dialog windows. If you want it in the TopLeft corner, set ReferX to 0, ReferY to 0. Below them is an option called "Refer" choose where you want your orientation point (in this case TopLeft). Hopefully that is the issue.


---
**giavonni palombo** *April 15, 2016 02:55*

I have those set to 0 and top left. Does it matter if say in corel my object is not in the top left, off some? Or does it only matter in laserdrw the position of my object? 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2016 03:37*

In Corel, I don't even put the object on the "artboard". I just import them wherever I happen to click. It actually matters not where they are located on the document's artboard. The CorelLaser plugin automatically references the TopLeft point of the object as where to start cutting/engraving from, not the TopLeft point of the artboard (or TopRight/BottomLeft/BottomRight, dependant upon which option you have set).



I am not so familiar with the LaserDrw, because I deleted it as soon as I realised it isn't necessary. I just use CorelDraw with the CorelLaser plugin.


---
**giavonni palombo** *April 15, 2016 12:27*

Im using the corellaser plugin to, its the same screen and menu options as my laserdrw. It put my objects in the upper left automatically to. Is there a grid pattern or some type of way to know where the object is placed in the plugin? I know there's the x,y origin but I can never get the object right where I want it. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2016 16:32*

I'm not 100% sure what the exact issue is. After re-reading your post/question, I see that you are wanting it to be at the X axis. Are you wanting it to be precisely next to the metal rail? If so, that will never happen as the laser head/carriage forces it to be about 1-2 inches away from that rail. If not, I'm kind of confused overall.



Regarding whether there is a grid pattern to know where the object is placed in the plugin, I haven't seen one or any option to enable that. 



Any chance of uploading a picture or two to show what you are having problems with? Might make it easier to diagnose the issue.


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/Zx5x613mkFx) &mdash; content and formatting may not be reliable*
