---
layout: post
title: "Quick tip -- Don't use lens cleaner on the the mirrors or focusing lens"
date: August 22, 2016 02:43
category: "Discussion"
author: "Evan Fosmark"
---
Quick tip -- Don't use lens cleaner on the the mirrors or focusing lens. I did this, figuring it was mostly isopropyl alcohol. Nope! Ended up with a coating that was blocking most of the IR. After rubbing it down with 91% iso, it works fine again.





**"Evan Fosmark"**

---
---
**Randy Randleson** *August 22, 2016 11:06*

Thats great advice. I create vinyl decals and 95% of people that have complaints that my decals didn't stick is because they thought they were cleaning the surface with window cleaner or lens cleaner when in fact they were putting a film of anti adhesive on it. Definitely always use straight Iso Alcohol.(The other 5% of people think its a good idea to take the decal off the tape first, lol.)


---
**Eric Flynn** *August 22, 2016 12:48*

Lens cleaners have detergents in them that leave a film typically intended as an aniti-fogging agent.  As you said, NOT a good idea.



Only use high quality lens wipes like Zeiss etc, or pure alcohol with a proper lens paper.


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/HbdxD5wcVMr) &mdash; content and formatting may not be reliable*
