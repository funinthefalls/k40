---
layout: post
title: "So I am looking at buying a 3d printer to go alongside my laser obsession"
date: September 19, 2017 20:46
category: "Modification"
author: "Abe Fouhy"
---
So I am looking at buying a 3d printer to go alongside my laser obsession. As I was looking around I found a super cool way for us K40'ers to get into 3d printing with OpenSLS. Essentially we cut a bottom out of our laser add the ztable (many of you have this done already) and then add two pistons filled with a material that gets melted (sintered) by the laser and then a wiper that applies a small amount on top. Good thing for us that the Cohesion3d will do this already right **+Ray Kholodovsky**?



[https://hackaday.com/2014/06/02/turning-a-laser-cutter-into-a-3d-printer-with-opensls/](https://hackaday.com/2014/06/02/turning-a-laser-cutter-into-a-3d-printer-with-opensls/)

[https://www.eurekalert.org/pub_releases/2016-02/ru-mlc022116.php](https://www.eurekalert.org/pub_releases/2016-02/ru-mlc022116.php)



Whitepaper explanation - 

[http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147399](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147399)



Software - [http://reprap.org/wiki/OpenSLS](http://reprap.org/wiki/OpenSLS)





**"Abe Fouhy"**

---
---
**Steve Clark** *September 25, 2017 01:01*

I consider this of great interest as I thought about doing this. However, it would be more for an experiment than anything else. I've also considered the use of the xyz axis to do thermal 3d printing.



It's all about if the head can support the plastic injector head.


---
**Abe Fouhy** *September 25, 2017 06:15*

It looks really cool, I went to the main page of the guy who made the opensls he is sintering metal too. That would be amazing. Seems like a fun experiment, but i am just getting into 3d printing so I think I have a few projects ahead of this one.


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/6Sqh8tXi8j3) &mdash; content and formatting may not be reliable*
