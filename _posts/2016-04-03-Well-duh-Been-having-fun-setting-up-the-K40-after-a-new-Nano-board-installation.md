---
layout: post
title: "Well, duh! Been having fun setting up the K40 after a new Nano board installation"
date: April 03, 2016 19:43
category: "Discussion"
author: "HalfNormal"
---
Well, duh!



Been having fun setting up the K40 after a new Nano board installation. I finally was able to get the board recognized by the software. I went back to a project I have been working on and of course it does not work the way it is suppose to. Cleaned mirrors, checked distance but still would not cut. Increased current but no dice. I did notice that even though I have air assist, the head was warm...mmm, checked mirror alignment and what do you know, out of alignment! 30 min later and all was better. Moral of story, mirror alignment is critical and should be checked first!





**"HalfNormal"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2016 20:34*

Indeed, a lot of issues start with misaligned mirrors. Maybe we should create a "Check List" of things to check in the case of an issue. Something **+Stephane Buisson** can sticky at the top of the main page with links to individual posts about specific issues & their fixes.


---
**HalfNormal** *April 03, 2016 20:52*

I second a check list!


---
**Stephane Buisson** *April 03, 2016 22:47*

Issue check list :

- laser not powerful enough to cut

(flip lens and clean/align mirrors)

- laser not firing (PSU or Tube ?)

- board not reconised (insert your code with nano board)

- ...



(what do you want in ?)

Not that long in fact (software aside)


---
**HalfNormal** *April 04, 2016 04:35*

Check all connections on all wiring. My 24 V cable is an accident waiting to happen.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 05:58*

**+Stephane Buisson** Yeah something like that. I just notice that we get a lot of the same questions all the time (from new people).



Another thing to add would be:

- speed too slow or running strange: check your board-id in the settings



I just thought making a sticky post would assist with all the newbies finding solutions for common problems quickly.


---
**HalfNormal** *April 04, 2016 12:43*

I hear paper money makes a great stiffening agent in wallets!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 14:30*

**+HalfNormal** Hahaha, we have plastic money in Australia.


---
**Heath Young** *April 05, 2016 02:37*

Whoever crimps those lugs for the wiring should be shot - I had to remove them and ended up tinning the connections after random weirdness at times.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/7dZvDYccGnM) &mdash; content and formatting may not be reliable*
