---
layout: post
title: "So I'm tired of seeing lots of people with 40w plus Lasers and I got to do something"
date: April 26, 2016 01:54
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
So I'm tired of seeing lots of people with 40w plus Lasers and I got to do something. Either I buy a K40 ( rather small footprint)  or I build one. What do you say?  





**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 26, 2016 02:24*

If you are electro-mecha-technically savvy, I'd imagine building one could work out to be more functional. And since you already have the Smoothie running LaserWeb, you could utilise it for the project & save some $ overall. You'd probably find you have less issues with alignment if you built the frame & the likes yourself. Not certain what the overall cost would be though. Maybe cheaper to buy a K40 itself.


---
**Vince Lee** *April 26, 2016 03:10*

Depends maybe on what you can get a k40 for.  When I got mine I paid 360 bucks.  I figure the tube, power supply, motors, mirrors and mounts, pump and other miscellaneous parts are with that.  But now they seem to be more expensive, and you can guarantee better parts with your own build.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 26, 2016 03:34*

**+Vince Lee** is that US$360? I paid AU$550, but got $50 refund due to issues with the machine.


---
**Ariel Yahni (UniKpty)** *April 26, 2016 03:46*

Yes I can see many offers in the US for 360.00 usd. But I need to add shipping to Panama.


---
**HalfNormal** *April 26, 2016 04:06*

Purchased mine for $366.00 USD delivered. 


---
**Alex Krause** *April 26, 2016 04:09*

**+Ariel Yahni**​ post a crowd fund I'm sure there enough people in the community that would help out. You did alot of testing with laserweb and I'm sure there are alot of people that would help you make it affordable


---
**Ariel Yahni (UniKpty)** *April 26, 2016 04:24*

Wow thanks **+Alex Krause**​ I'm humbled


---
**Alex Krause** *April 26, 2016 04:34*

**+Ariel Yahni**​ I'm serious set it up, and I will be the first one to donate 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 26, 2016 04:48*

I'd be happy to pitch in an amount towards it too **+Ariel Yahni**.


---
**Ray Kholodovsky (Cohesion3D)** *April 26, 2016 04:53*

I'll (eventually) contribute a smoothie controller for it. Hint hint. 


---
**Scott Thorne** *April 27, 2016 01:22*

Build it...I'll contribute. 


---
**Ariel Yahni (UniKpty)** *April 27, 2016 03:28*

**+Scott Thorne**​ I will eventually build one but now resources are limited and just getting all the part will be much more expensive than I thought, plus shipping will kill.  Will be more beneficial at this time to get something simpler, further help testing laserweb and help others upgrade their machines


---
**Ray Kholodovsky (Cohesion3D)** *April 27, 2016 03:32*

I think the solution is very simple.  Someone orders a certain $400 item on ebay right before going on vacation to Panama and just happens to check an extra case at the airport. 


---
**Ariel Yahni (UniKpty)** *April 27, 2016 03:38*

**+Ray Kholodovsky** Sir, whats that long glass tube inside that box? Well its a frigging LASER. An then you wake up in jail


---
**Ray Kholodovsky (Cohesion3D)** *April 27, 2016 03:51*

Yes, because society's understanding of technology is entirely based off of action TV shows. 

Oh, and I press the one button on the keyboard and the computer does the thing I need it to <b>before</b> I'm finished explaining the concept. God forbid I have to write 3 lines of code to actually get something done realistically. 



Have you noticed that every TV show accomplishes PC tasks with 2 keystrokes now?


---
**Ariel Yahni (UniKpty)** *April 27, 2016 03:58*

O yes, and the graphics are over scaled


---
**Ray Kholodovsky (Cohesion3D)** *April 27, 2016 04:04*

Everything is shown as an automatically generated GUI now. Of course they're going to focus in on that. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 02, 2016 01:22*

**+Ariel Yahni** Dropped a donation towards your upgrades just now. AUD to USD conversion rate is not nice today (AU$20 = ~US$15).


---
**Ariel Yahni (UniKpty)** *May 02, 2016 01:31*

**+Yuusuf Sallahuddin**​ TX a lot. Really appreciated. 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/TwSojKxRrd6) &mdash; content and formatting may not be reliable*
