---
layout: post
title: "Monitoring the exhaust airflow Hi there, I'd like to share my solution to monitor the exhaust airflow"
date: December 14, 2017 01:09
category: "Discussion"
author: "java lang"
---
Monitoring the exhaust airflow



Hi there, I'd like to share my solution to monitor the exhaust airflow. For what reason? Well, in my case after one year laser cutting I noticed more and more dust and smell in my shop where the laser cutter is installed, though I have  a good working exhaust ventilation(I thought). After some inspection I  was shocked: the laser tube was full of dust and resin (from wood)...finally I've found out that my exhaust filter (forgotten to change/clean) totally blocked any airflow and all the garbage landed inside the machine.

To prevent this situation in future, I asked to myself, how to measure or monitor the airflow and came to a simple solution:

take an old PC-fan (theese with 3 wires), remove the propeller, cut out the coil including the metallic core (this removes the magnetic resistance), reassemble the propeller (the fan is rotating  now already with very small airflow), put 5V on the fan and you'll get a  5V pulse on the yellow cable three times a circumvolution. You can evaluate this info i.e. with an arduino in a single line of code: "freq=1/pulseIn(pin,HIGH);" and give some feedbak to the operator, i.e.with an display or a blinking LED, or a buzzer, or whatever you like.

That's it,

happy lasercutting,

javalang







**"java lang"**

---
---
**Joe Alexander** *December 14, 2017 01:44*

posting incomplete sentence or thoughts is really


---
**java lang** *December 14, 2017 01:46*

completed now :)



Am 14.12.2017 um 02:44 schrieb Google+ (Joe Alexander):


---
**Phillip Conroy** *December 14, 2017 06:26*

Good idea,or just set a monthly alarm on phone to check that it's clear.

I Am currently working on an institutions  on how to monitor this air flow and other temps for a fire alarm, because of the extraction fan pulling all the smoke and heat out of machine how would you know if the cutting materials where on fire? Even sitting next to my laser designing stuff the only time I would know a fire had started would be when the belt burned through  ... think about it guys and grls how much damage can happen before any outside  noticable indercations happen.......how many photos of destroyed laser cutters are on Google images, lots ,how sure are you the stuff you cut 5,10 minutes or even hours ago are not smoldering away then catch fire destroying laser machine and even building .

So far have temperature  pid monitoring in aluminum extraction pipe,thermal bimetal sensors in highest point of laser bed , all hooked up to power 24 hours a day

The pid temperature controler I can set to 5 deg c above room air conditioning temp in 2 seconds to help give early warning when I am cutting.

Back up are 65 deg c thermal fuses hooked up to alarm 


---
**Don Kleinschnitz Jr.** *December 14, 2017 14:30*

My similar attempt....

I am installing a cheap ebay temp controller with PID as a fire alarm. The theory is that the PID, mounted on an under the head will sense a temp limit is exceeded and interrupt the laser with an alarm. Although this will likely destroy the $5 PID it hopefully will stop the laser and notify me to get the extinguisher to the machine.



The key question is will the PID sense the high temp from the fire in time :)???  Will it create false positives due to normal cutting? 


---
**Phillip Conroy** *December 14, 2017 18:10*

Pid temp controlers sample  the temperature every 0.5seconds so yes it would work,however not for long as the cable to the jprobe is not designed to  flex many times,so it would work but not ft or long,maybe 100 movement of the head ...


---
**Phillip Conroy** *December 14, 2017 18:13*

Also you have to get a pid controler that works to trigger cooling and not heating as most of them are setup to do ,ie most are setu p to turn on heating elements. You could use the heating pid controler and use a doub l e pole relay on the output ....


---
**Don Kleinschnitz Jr.** *December 14, 2017 19:16*

Are the cables to the sensor in some way special? I planned to run normal wire out to the sensor? It not a thermal couple its a themister type I believe?



My controller has two relays one for hot one for cold?

I planned to set one of them to open when the temp exceeds the setpoint opening the laser interlock.


---
**Phillip Conroy** *December 14, 2017 20:17*

Yes you are right, should be normal wire,however if you have air assist might not the aire stop the sensor from reading the fire


---
**Don Kleinschnitz Jr.** *December 14, 2017 20:32*

**+Phillip Conroy** if the air assist is working seems there should be no flame up as that is one of it's purpose? 

I don't think this approach is bullet  proof but hope it cuts down the chances of a meltdown.

Kinda difficult to test!


---
**Phillip Conroy** *December 14, 2017 20:49*

Good to see other people thinking about fire alarms,nothing will be perfect as laser operates in a smokey laser bed.

I have seen 0hotos of plastic exhaust fan blades melting so a temp sensor in the extraction pipe should detect a rise in temp ,I am also going to fit an air flow switch to stop laser firing.

So many mods and upgrades so little time to do them


---
*Imported from [Google+](https://plus.google.com/111347667569437072694/posts/ha9wJDie9mM) &mdash; content and formatting may not be reliable*
