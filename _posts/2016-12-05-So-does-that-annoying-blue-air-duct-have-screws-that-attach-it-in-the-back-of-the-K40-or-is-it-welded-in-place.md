---
layout: post
title: "So does that annoying blue air duct have screws that attach it in the back of the K40 or is it welded in place?"
date: December 05, 2016 18:13
category: "Modification"
author: "Carle Bounds"
---
So does that annoying blue air duct have screws that attach it in the back of the K40 or is it welded in place? I am guessing I have to completely remove the XY to get to it. I was thinking of removing it and 3d printing a shorter one but I wanted to see how it was installed back there before I started taking things apart.





**"Carle Bounds"**

---
---
**Ariel Yahni (UniKpty)** *December 05, 2016 18:23*

For most is attached with screw but mind that I have seen people being able. To remove it with removing the gantry. I did had to remove the gantry


---
**Carle Bounds** *December 05, 2016 18:24*

Are the screws on the back of the k40 or inside behind the gantry?


---
**Stephane Buisson** *December 05, 2016 18:27*

back


---
**Jim Hatch** *December 05, 2016 18:27*

On mine they were inside behind the gantry. Made me take out the gantry which was a good thing as the front right corner wasn't square anyway and that was messing up my alignment.


---
**Ashley M. Kirchner [Norym]** *December 05, 2016 18:45*

Mine was screwed in, the screws were on the back of the machine, but in order to actually remove the piece, I had to pull out the gantry. I completely removed it, meaning I have no duct inside of the machine anymore, just the hole in the back.



Some machines come with that piece welded on.


---
**Don Kleinschnitz Jr.** *December 05, 2016 18:46*

I have left mine installed and cut the inside back to allow full movement of the gantry. Screwed floor duct-work to the back to get to 4" pipe.

[donsthings.blogspot.com - K40 Air Systems](http://donsthings.blogspot.com/2016/06/k40-air-systems.html)


---
**Carle Bounds** *December 05, 2016 19:13*

I have one of the newer digital K40's I just bought a month ago. I'm wondering if its welded on as I don't feel any screws when I use my hand to feel behind the gantry


---
**Jim Hatch** *December 05, 2016 19:23*

You might not. I didn't see/feel mine until I took it all apart.


---
**Ashley M. Kirchner [Norym]** *December 05, 2016 19:44*

**+Carle Bounds**, look behind the machine at the exit hole. Do you see screws or welds?


---
**Carle Bounds** *December 05, 2016 19:46*

I am going to check it out tonight when I get home.  I hope I see screw holes!


---
**Ashley M. Kirchner [Norym]** *December 05, 2016 20:01*

If it's welded on, that's still not a show stopper, it will just be a bit more work to grind/cut the welds. If you have a Dremel tool, it'll be easy. Regardless, you will likely still need to pull out the gantry to get the actual duct out. And taking the gantry out means removing 4 bolts (2 back, 2 front under the black plate), disconnecting the cables and lifting it out, then when you put it back in, you'll have to re-align your mirrors again. Nothing impossible.


---
**Ned Hill** *December 05, 2016 20:30*

**+Carle Bounds** I have one of the newer versions as well and I had to not only remove the gantry but unmount the laser tube as well.  One of the tube brackets on my machine has bolts that come down just above the vent and they prevented me from tilting the vent forward enough to clear the ledge in the back.  Major pain in the ass.  I did a post on it after I got my machine.  Hopefully your's isn't the same.  [plus.google.com - Ok just wanted to give a breakdown of what it took to removed the carrige frame…](https://plus.google.com/108257646900674223133/posts/TWpTcM9gQeY)


---
**Carle Bounds** *December 05, 2016 21:57*

**+Ned Hill** Does your panel look like this?

![images/29413fc543e8bb521c66e526dd867839.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/29413fc543e8bb521c66e526dd867839.jpeg)


---
**Kelly S** *December 05, 2016 22:01*

Mine had 4 bolts and the same panel as **+Carle Bounds** there.  I had to remove the gantry to get it out to cut it back.  It gets really fat on the back side and didn't fit.  Works good though cut back to the far side of the rail.  


---
**Ned Hill** *December 05, 2016 22:02*

**+Carle Bounds** Yep.  You might not have to unmount the tube but keep that in mind if the vent won't come out easily.  Unmounting the tube also requires a complete realignment.  Also when you remove the gantry make some marks with a marker where it attaches so you can get it back close to the original alignment.


---
*Imported from [Google+](https://plus.google.com/106012823225343259431/posts/4Ev6t9WjUbW) &mdash; content and formatting may not be reliable*
