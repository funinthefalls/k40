---
layout: post
title: "Just discovered this forum the other day and had a couple of questions"
date: May 10, 2016 12:54
category: "Discussion"
author: "John-Paul Hopman"
---
Just discovered this forum the other day and had a couple of questions. Is the K40 a specific brand of printer, like the glowforge, or is it a class of 40W CO2 laser engravers / cutters? What are the price ranges of the machines, recommended suppliers, etc...essentially an introduction to the subject or reference links to get started.



I have toyed around with the idea of getting a cheap ($400ish) laser cutter, but never bit the bullet.





**"John-Paul Hopman"**

---
---
**Jim Hatch** *May 10, 2016 13:26*

It's a type of cheap laser cutter built by various Chinese factories and shipped all over the world. The "K40" refers to the 40W CO2 lasers. There are variants built on the same platforms with 50/80/etc watts.



They're typically purchased from ebay but can also be purchased from AliExpress. From ebay they're typically $350-$400 delivered in the US.



There are different features based on which factory is building them. Some include a digital display, others an analog meter. Some have air assist heads standard as well as an adjustable Z-axis.



The more of the options, the more expensive. You can add all the optional stuff as upgrades to your machine when you get it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 10, 2016 15:04*

Like Susan, I purchased mine off ebay in Australia. It was delivered from Sydney to Gold Coast for $550 back in September 2015. Took nearly a week for me to get mine though.


---
*Imported from [Google+](https://plus.google.com/114435223224758285486/posts/aVMwB3cdSX8) &mdash; content and formatting may not be reliable*
