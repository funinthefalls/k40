---
layout: post
title: "Well, After taking out the PSU and having a quick peek inside, I found the part that blew"
date: July 26, 2016 16:23
category: "Original software and hardware issues"
author: "Pete Sobye"
---
Well, After taking out the PSU and having a quick peek inside, I found the part that blew. A friend locally suggested that it could be repaired. I am waiting to see what the seller says first.

In the mean time, still looking for a second hand PSU if anyone has one going, perhaps after an upgrade? Get in touch. Original pic included and will delete the old post to save space.



Another question. Anyone having done a PSU swap, did you cut and re-join the red wire feeding the tube or just fit the new one straight? I have not removed the cover on the wire at the tube end to see what the connector is.



![images/4b3e8cf5060a5d8e6ce8835d6308b178.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b3e8cf5060a5d8e6ce8835d6308b178.jpeg)
![images/35a74a27c8ff66d0b5868724a2a2772b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/35a74a27c8ff66d0b5868724a2a2772b.jpeg)

**"Pete Sobye"**

---
---
**Stephane Buisson** *July 26, 2016 16:27*

no connector on tube end. it's just wrap around the tube pin with silicone on it.


---
**Bart Libert** *July 26, 2016 16:56*

Hmmm, I'm not 100% sure but isn't the exploded part some kind of fuse or protection that blew? If so then the problem is not in that part but somewhere else in the circuit. My 2 cents don't try to repair this thing. It is too dangerous to try to repair and use again. These PSU's make very high voltages and require a lot of knowledge to build/repair. Just buy another one and be angry because this one... went down.. :(




---
**Jon Bruno** *July 26, 2016 16:58*

That isn't the first thermistor to pop.

NTC Part # 5D-15


---
**Bart Libert** *July 26, 2016 16:59*

Thats a thermistor ?


---
**Jon Bruno** *July 26, 2016 17:00*

thermistor is missing. and the fuse is destroyed as well.


---
**Jon Bruno** *July 26, 2016 17:02*

I can't say what popped the fuse but being that the thermistor has vaporized there's a good chance you can start looking there. after that make your way up to the rectifier.


---
**Jon Bruno** *July 26, 2016 17:10*

[https://drive.google.com/open?id=0B36nra1ArP-VZTJiQjhuNGwwQmM](https://drive.google.com/open?id=0B36nra1ArP-VZTJiQjhuNGwwQmM)


---
**Jon Bruno** *July 26, 2016 17:31*

But yeah, Don't try to repair it yourself if you aren't qualified. "You could end up seriously killed". Folgertech.com used to sell them fairly inexpencively.. Shipped from NH.


---
**Don Kleinschnitz Jr.** *August 15, 2016 12:36*

This may help:

[https://plus.google.com/111055748369814573959/posts/jThGq47RHhF](https://plus.google.com/111055748369814573959/posts/jThGq47RHhF)




---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/9yAWB1VZkhr) &mdash; content and formatting may not be reliable*
