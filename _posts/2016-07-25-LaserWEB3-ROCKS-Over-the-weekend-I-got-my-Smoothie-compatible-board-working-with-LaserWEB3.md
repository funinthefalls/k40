---
layout: post
title: "LaserWEB3 ROCKS! Over the weekend I got my Smoothie compatible board working with LaserWEB3"
date: July 25, 2016 15:35
category: "Software"
author: "David Cook"
---
LaserWEB3 ROCKS!



Over the weekend I got my Smoothie compatible board working with LaserWEB3. **+Peter van der Walt** nice work !       Once I figured out the layer setup I was able to etch, cut and outline cut  very easily.



Smoothie rockin 1/32 steps makes the machine much quieter and smoother.



I have some ideas for things that could make LaserWEB3 even cooler.  I'll post them in the LW community though.









**"David Cook"**

---
---
**David Cook** *July 25, 2016 15:47*

**+Peter van der Walt** Absolutely.  What's the best way to submit the write up ?


---
**Bart Libert** *July 25, 2016 15:47*

what smoothie compatible board did you use ?




---
**David Cook** *July 25, 2016 15:49*

**+Bart Libert**   I used an MKS SBASE that I bought a while back.   Next time I buy a board it will be a Legit Smoothie board.  


---
**Ariel Yahni (UniKpty)** *July 25, 2016 16:06*

**+An R key _**​ there is an option to add more segments to circles before creating them


---
**Ariel Yahni (UniKpty)** *July 25, 2016 16:17*

But I think **+An R key _**​ means after conversion. 


---
**Ariel Yahni (UniKpty)** *July 25, 2016 16:30*

**+An R key _**​ plugin link you will find on the wiki under 2D sketches 


---
**David Cook** *July 26, 2016 02:15*

**+Peter van der Walt**  here is the link to the write up I just made.  [https://drive.google.com/folderview?id=0ByNecjb0RD-TdkZiZzJRcmh4ajg&usp=sharing](https://drive.google.com/folderview?id=0ByNecjb0RD-TdkZiZzJRcmh4ajg&usp=sharing)


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/CkHuqM1PHDb) &mdash; content and formatting may not be reliable*
