---
layout: post
title: "Hi guys. Just wanted to know if it's possible to upgrade to a bigger build volume"
date: December 11, 2015 13:09
category: "Modification"
author: "Ariel Yahni (UniKpty)"
---
Hi guys.  Just wanted to know if it's possible to upgrade to a bigger build volume.  Has someone done this? 





**"Ariel Yahni (UniKpty)"**

---
---
**Stephane Buisson** *December 11, 2015 13:25*

yes, you will find several attempt on the net. but you need to understand the limit of this cheap K40, (optic precision).

Personally I went for a "Fake A3", I cut a slot in the front side of the machine to allow a larger material to fit on the bed. the print bed is still a A4 (XY ray path unchanged), but by rotating the material 180, I can laser the second half.(A3=A4x2)


---
**Jose A. S** *December 11, 2015 14:53*

Could you share with us pics?


---
**Stephane Buisson** *December 11, 2015 16:10*

you have a hint in my video :


{% include youtubePlayer.html id="0r5sdS8zqY0" %}
[https://www.youtube.com/watch?v=0r5sdS8zqY0](https://www.youtube.com/watch?v=0r5sdS8zqY0)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/UN1sNXATtqG) &mdash; content and formatting may not be reliable*
