---
layout: post
title: "K40 Will Not Cut Mystery Easily Solved!"
date: July 14, 2017 12:54
category: "Discussion"
author: "HalfNormal"
---
K40 Will Not Cut Mystery Easily Solved!



So I went to cut a few items on my machine which has been recently overhauled and low and be hold it will not cut! A shadow burn also appeared out of no where. I have not been doing a lot of work on the machine so this is very mystifying. I removed the head and cleaned the lenses. Not much on the q-tip but some. Re-installed the head and aligned it again and volià, it cuts!



Not sure if the cleaning or head re-alignment did the trick but sometime is is the simple things that get you going again.





**"HalfNormal"**

---
---
**Jim Hatch** *July 14, 2017 13:41*

The first two steps for any cut/engrave issue are 1) check the lens is oriented correctly and 2) realign the mirrors ☺️


---
**Mark Brown** *July 14, 2017 22:37*

Shadow burn points to the beam hitting the inside of the head. 


---
**Ned Hill** *July 15, 2017 05:00*

Been there myself. :)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/aS9qMAkMgHD) &mdash; content and formatting may not be reliable*
