---
layout: post
title: "Lasers and dance clubs are somewhat synonymous, except I don't do dance clubs so this is as close as I'll get"
date: December 16, 2016 01:44
category: "Modification"
author: "Madyn3D CNC, LLC"
---
Lasers and dance clubs are somewhat synonymous, except I don't do dance clubs so this is as close as I'll get. 



Doing some stuffs, thought I'd share. :) 



![images/ea22b2fbf6a25c9e15cfef3fadd53766.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ea22b2fbf6a25c9e15cfef3fadd53766.jpeg)
![images/416a3303bf3f0322a92e18daf3e4ae57.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/416a3303bf3f0322a92e18daf3e4ae57.jpeg)

**"Madyn3D CNC, LLC"**

---
---
**Kelly S** *December 16, 2016 03:40*

Looks a lot cleaner than mine haha, last three days of nothing but wood  :(  Cleaning it out now.  haha.


---
**Madyn3D CNC, LLC** *December 16, 2016 14:59*

**+Kelly S** this one was pretty rough as well with MDF sap/soot and acrylic resin, in fact it was even effecting the bearings it was so thick. I am replacing the laser tube so I took advantage of the downtime to hold it's 1st annual cleaning, 91% iso cut right through the MDF and acrylic crud, then detailed everything with over sized q-tips I swiped from the doctors office a while back, then re-lubricated everything with 100% propylene glycol (PG).



Quick Tip #062- 

Propylene Glycol (PG) is the sole ingredient/base ingredient in a vast majority of mechanical lubricants. Whether it's mechanical hair clippers or a chainsaw you buy, that little tube or packet of lubricant that comes with, is almost always 100% PG, nothing more, nothing less. If you're a maker who owns a lot of CNC stuff, grab yourself a cheap $10 gallon of PG and I promise you won't regret it! (and your equipment will love you for it)


---
**Kelly S** *December 16, 2016 15:19*

That's a great tip actually.   And I do have some of that around.  ;)  plan to extend my k40 cutting area into the electronics side soon so it will be getting all new rails, belts and bearings very soon.


---
**Ned Hill** *December 16, 2016 22:58*

Not so sure about Propylene glycol.  The only lubricant I've seen it used for is personal lubricants not mechanical. I don't think it would  be good for metal parts as it's hygroscopic (absorbs water).  Sure you don't mean something else?  I'm a pharmaceutical analytical chemist and it's also commonly used as a pharmaceutical solvent.


---
**Kelly S** *December 16, 2016 23:18*

I use both pg and vg in my nicotine bases for my e cig.  Never thought of it being used as a lube.  What I use on my 3d printers is Super Lube

21010 Synthetic Grease.   Also used it on the laser cutter and my movements are good.  Just important to keep it well dusted and maintained. 


---
**Madyn3D CNC, LLC** *December 17, 2016 00:44*

**+Kelly S** That's awesome, I owned VapeTechnologies LLC for a few years, we produced box mods that were in many shops and featured at a lot of events. But once the vaping thing turned into such a trendy thing, with all the cartoon labels and 15 y.o's blowing 'tricks' and so much more, it really was not an industry I was proud to be in. Years ago when it was a subculture, it was all about just quitting cigarettes, now it's so much of a trend it's embarrassing. When it turns into 75% of clientele never even smoking cigarettes before, wanting to purchase a box, it was no longer what I wanted to do. I just ordered a gallon each of PG/VG for about 40 bucks each on ebay.  

[www.facebook.com/vapetechES5K](http://www.facebook.com/vapetechES5K)


---
**Kelly S** *December 17, 2016 00:57*

Yeah, I used my vape 6 or 7 years ago to quit smoking.  Back in the days of leaky tanks, cruddy tasting setups lol...  Mix my own nic, down to 4 or 5 mg/ml Could prob cut ti off all together but sometimes It is nice to have lol.    I made a lot of my own mods over the years, now I just have a handy dandy 2x18650 mod with a good tank and am happy. 


---
**Madyn3D CNC, LLC** *December 17, 2016 03:23*

**+Ned Hill** A while back a few of us were interested in the hygroscopic properties of glycerin, PG, and polypropylene glycerol, as the ratios mixed in eliquid effected electronics and PCB's differently. It was a lot of bro-science but it seemed people who used high VG (vegetable glycerin) seemed to suffer with corrosion afterwards, when the liquid would leak inside PCB's. We were aware of corrosion potential of PG, but did not notice any corrosion in electronics when it was high PG juice. You're probably right, to be on the safe side, as they are all certainly hygroscopic, but it did seem some had stronger properties than others.


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/3QUToW4V8Ez) &mdash; content and formatting may not be reliable*
