---
layout: post
title: "Hello laser friends!! I recently acquired a 100w ebay laser from china, I have it close to up and running but have hit a snag"
date: December 06, 2016 18:45
category: "Original software and hardware issues"
author: "Jordan Needham"
---
Hello laser friends!! I recently acquired a 100w ebay laser from china, I have it close to up and running but have hit a snag. It uses Laser DRW 3 I have it initialized and it acts as though its cutting but the tube doesn't fire. I can get the tube to fire with the red test button on the power supply itself so i know it works, somehow laser drw inst sending a message to fire the tube any suggestions? 





**"Jordan Needham"**

---
---
**Phillip Conroy** *December 06, 2016 20:30*

Make sure emergency. Stop button is out,twist accordin . To marking on button itself


---
**Alex Krause** *December 06, 2016 20:44*

Laser drw requires a USB key be plugged into your computer, also that the serial number off the board be entered in the software... can you take a picture of the machine and also off the control board to make sure you are using the right software... seems a bit strange that you got a 100w laser that isn't a Ruida controller, most the higher what lasers use a DSP not the cheesy controller that is used in a K40


---
**Jordan Needham** *December 06, 2016 21:06*

![images/01a6d427c0f99e5fcdd0cafd96ab9f7c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/01a6d427c0f99e5fcdd0cafd96ab9f7c.jpeg)


---
**Jordan Needham** *December 06, 2016 21:07*

![images/1197dccf341a2ac4315f4377ac9c031c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1197dccf341a2ac4315f4377ac9c031c.jpeg)


---
**Jordan Needham** *December 06, 2016 21:07*

![images/4c164b282d60435df31b4e6d50f80395.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c164b282d60435df31b4e6d50f80395.jpeg)


---
**Jordan Needham** *December 06, 2016 21:08*

![images/ff2b23b7446a9a660cfee64aebe0265f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff2b23b7446a9a660cfee64aebe0265f.jpeg)


---
**Jordan Needham** *December 06, 2016 21:09*

![images/a7c2b8d6b2c6df8cb0474ff6cfd3acb6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a7c2b8d6b2c6df8cb0474ff6cfd3acb6.jpeg)


---
**Jordan Needham** *December 06, 2016 21:33*

It does have the orange dongle and I was able to initialize and connect to the Machine by selecting my board number and entering in the serial number it acts like it's cutting but the tube doesn't fire unless I push the little red test button on what I am assuming is the power supply thank you so much for any suggestions you can throw my way it means so very much to me you guys are so awesome!


---
**Jordan Needham** *December 06, 2016 21:33*

**+Phillip Conroy** thank you for the suggestion unfortunately that's one of the first things I checked


---
**Jordan Needham** *December 06, 2016 21:35*

**+Alex Krause** I posted some photos thank you so much for anything you could offer to get me up and running


---
**Alex Krause** *December 06, 2016 21:43*

Did you select the board type on that same menu where you entered the serial number for the board?


---
**Jordan Needham** *December 06, 2016 21:43*

Yes.


---
**Jordan Needham** *December 06, 2016 21:44*

Have the wrong one she liked it at first and it would not even connect


---
**Jordan Needham** *December 06, 2016 21:44*

Sorry. I had the wrong one selected at first and it would not connect


---
**Jordan Needham** *December 06, 2016 21:45*

I can move the head of the engraver from laser drw and it pretends to cut just know laser beans


---
**Alex Krause** *December 06, 2016 21:45*

The black wire on the controller board that is in the connector labeled "Do Not Live Install" is the wire used to fire the laser on the psu... does your digital display work?


---
**Jordan Needham** *December 06, 2016 21:47*

Yes it does it allows me to control z access and Laser power that's about it it also has a small LED labeled laser that flickers when it is pretending to cut.


---
**Jordan Needham** *December 06, 2016 21:48*

I should say the buttons make the numbers on the display change when I press the small red button on the power supply to do a test fire it seems to be at full power regardless of the setting on the display


---
**Jonathan Davis (Leo Lion)** *December 06, 2016 23:18*

**+Jordan Needham** Your best bet Go Smoothie as it will add many additional functions.


---
**Jordan Needham** *December 06, 2016 23:19*

Ok thanks will a smoothie run on this size of machine? Also do I have to swap motors as well or just the board?


---
**Jonathan Davis (Leo Lion)** *December 06, 2016 23:22*

**+Jordan Needham** It will run on any size of machine as you would setup the measurements for your print space in the laserweb3 program.


---
**Jordan Needham** *December 06, 2016 23:31*

 what is the cost to go smoothie?


---
**Jonathan Davis (Leo Lion)** *December 06, 2016 23:35*

**+Jordan Needham** $100+


---
**Jordan Needham** *December 06, 2016 23:36*

Is it a replacement for the main board only? Do I have to replace my power supply or any other Hardware


---
**Jonathan Davis (Leo Lion)** *December 06, 2016 23:38*

**+Jordan Needham** just the controller board. **+Ray Kholodovsky** **+Ariel Yahni** are good people to ask about it.


---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 23:47*

Cohesion3d.com 

Check out the mini laser bundle. 

You just replace the green board in your laser with the mini. All the holes and mounting connections are identical, and it has connectors for all known cable types, making it truly drop in. 


---
**Alex Krause** *December 07, 2016 00:16*

Notice the 36v stepper voltage **+Ray Kholodovsky**​


---
**Jordan Needham** *December 07, 2016 00:18*

**+Ray Kholodovsky** that sounds like just what I need Does it include the software as well that controls the board?


---
**Jordan Needham** *December 07, 2016 00:19*

Is my 36 volt stepper going to be a problem?






---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2016 00:21*

Oh, I didn't scroll up far enough. I thought we were talking normal k40 here.  Good catch. 

This is... Interesting. Usually we see external stepper drivers on the larger machines. The mini itself should not be run above 30v.  It can be used as a smoothie motion brain to command external stepper drivers, we even have adapters to do that. 


---
**Jordan Needham** *December 07, 2016 00:24*

Is the board I have even capable of doing what it's supposed to?


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2016 01:01*

I'm not sure bud. Can you look into what kind of stepper motors are present on the machine? Pics help :) 


---
**Alex Krause** *December 07, 2016 02:02*

Is the button circled in red the one you are using when confirming test fire operations or is there another button on the power supply

![images/ecb325255766c34979595176f9a07fb0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ecb325255766c34979595176f9a07fb0.jpeg)


---
**Jordan Needham** *December 07, 2016 04:45*

The button on the power supply


---
**Alex Krause** *December 07, 2016 04:51*

If the button I circled didn't work to test the laser you have a problem... have you made sure the laser enable switch is functioning properly? Also does your machine have interlocks that could be faulty like a water flow sensor for cooling or lid closed switches


---
**Jordan Needham** *December 07, 2016 04:56*

Nothing seems to happen when I press the circled button I have not taken a meter to the switch I will try that the lid switch is taped down and I don't believe there is a flow sensor what would that look like?


---
**Alex Krause** *December 07, 2016 04:57*

It would look like a coupling connected to the input or output of the fluid flow tubing thru the laser tube that has a wire running out of it


---
**Alex Krause** *December 07, 2016 05:00*

Have you moved the position of the switch located next to the emergency stop on the face of the machine and ran a test in its different positions?


---
**Jordan Needham** *December 07, 2016 05:02*

I thought I did but double sure I'll try again tonight


---
**Jordan Needham** *December 07, 2016 05:02*

I don't think I have a flow sensor


---
**Alex Krause** *December 07, 2016 05:08*

If all those checks have failed... and you purchased using PayPal open a claim immediately and demand a refund... you have verified the tube and power supply is in working order so the problem lies in either the switches,the controller ( which is unlikely since it goes thru the motions) or the wiring of the machine... I am pretty sure that your digital readout should be connected to the controller through one of the top two headers on the control board, can you trace the link between the digital readout the controller and the psu?


---
**Jordan Needham** *December 07, 2016 05:56*

**+Ray Kholodovsky** 

![images/63e86febc192798d105ae8b2e915b516.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/63e86febc192798d105ae8b2e915b516.jpeg)


---
**Jordan Needham** *December 07, 2016 05:56*

![images/1502f38c3e391cbb2acb8afe08e49b30.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1502f38c3e391cbb2acb8afe08e49b30.jpeg)


---
**Jordan Needham** *December 07, 2016 05:56*




---
**Jordan Needham** *December 07, 2016 05:58*

There are 2 stepper motors for the x/y 


---
**Jordan Needham** *December 07, 2016 06:01*

+Alex Krause I tried the switch in all positions as well as bypassing the switch no luck I am able to trace the link between the PSU you and display panel it is the left most connector on the board


---
**Jordan Needham** *December 07, 2016 06:01*

![images/2458e4b3b725ee8356a30e6dd55b9c75.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2458e4b3b725ee8356a30e6dd55b9c75.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2016 06:09*

Allrighty.  They look like Normal Nema 17's to me.  Not even that powerful ones even.  



Could you meter the yellow and red wires ("36v" and gnd) to check the actual voltage your psu is putting out?



A few other things I noticed: Your green board is different size wise as well so my mini is not a drop in solution.  Some mounting hole drilling would be required. 

One way would be to use a 24v PSU to drive the the board with drivers on it.  

Another option would be to keep the "36v" PSU in play and get external stepper drivers. 

A 3rd option would involve something I started development on a little while ago... Essentially an in between solution so that you can run my board, and higher voltage steppers, without having to do those huge black box external steppers.  We should discuss this further...


---
**Jordan Needham** *December 07, 2016 06:12*

Nice man I really appreciate your support and knowing that I have options is a really good feeling I have to go into work trip soon and won't have much time to work on it until I get back I'm also going to be in contact with who I bought it from and see if they can either refund me or set me on the right track either way I think I want to upgrade it and so I will get the parts from you thank you


---
**Rob Morgan** *December 07, 2016 06:53*

Flow sensor in the chiller? Could be a bad pump or flow sensor or tc in the chiller or its not wired correctly maybe. 


---
**Joe Alexander** *December 07, 2016 13:36*

if the laser tube fires withe the button on the Laser PSU then I would guess you have a broken section in your interlock link. Double-check all crimps/screw terminals etc. to make sure they are firmly seated and actually hitting conductor. You could also temporarily jumper the interlock pins and run a test to verify the program is in control of the power output also :)


---
*Imported from [Google+](https://plus.google.com/117791547611711529904/posts/DeZ4KrpXhYL) &mdash; content and formatting may not be reliable*
