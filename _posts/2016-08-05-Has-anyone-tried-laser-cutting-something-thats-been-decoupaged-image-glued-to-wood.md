---
layout: post
title: "Has anyone tried laser-cutting something that's been decoupaged (image glued to wood)?"
date: August 05, 2016 14:59
category: "Materials and settings"
author: "Tev Kaber"
---
Has anyone tried laser-cutting something that's been decoupaged (image glued to wood)?  



I was thinking it would be cool to make a wood puzzle from a full-color photo, but wasn't sure if that would work or if the photo part would get too burned during cutting.





**"Tev Kaber"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 05, 2016 15:14*

You'd probably be better off cutting the wood first, then before moving it from the cutting table place some contact adhesive on one side to hold the pieces in position. Then glue your image to the other side (as a whole piece) & you could then later cut the rest of the image through the puzzle lines with a scalpel or similar tool.


---
**Ariel Yahni (UniKpty)** *August 05, 2016 15:15*

I have that in mind as a to do also. For sure it will burn but you could do multiple passes at low power. 


---
**Bart Libert** *August 05, 2016 15:19*

Did it:


{% include youtubePlayer.html id="NlJWU2GMnjk" %}
[https://www.youtube.com/watch?v=NlJWU2GMnjk](https://www.youtube.com/watch?v=NlJWU2GMnjk)




---
**Bart Libert** *August 05, 2016 15:22*

This illustrates how I camera position the laserhead: 
{% include youtubePlayer.html id="rrNJ29R2mW0" %}
[https://www.youtube.com/watch?v=rrNJ29R2mW0](https://www.youtube.com/watch?v=rrNJ29R2mW0)


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/eSHjGDWSAiJ) &mdash; content and formatting may not be reliable*
