---
layout: post
title: "Here are the feet for my K40"
date: January 25, 2018 12:53
category: "Modification"
author: "David Davidson"
---
Here are the feet for my K40. 



3/8x1 inch bolts, 8 nuts, 4 washers and 5/8" leg ends from Ace Hardware.



![images/a398c6d71d80def3a8407fcbdc887cdc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a398c6d71d80def3a8407fcbdc887cdc.jpeg)
![images/166305603bcc6f47a0516ffc9b359a36.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/166305603bcc6f47a0516ffc9b359a36.jpeg)
![images/d0b2fd7b167b96646e03ba57298ff75b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d0b2fd7b167b96646e03ba57298ff75b.jpeg)
![images/5c41b7da0df68087deb2fc637d79198e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c41b7da0df68087deb2fc637d79198e.jpeg)

**"David Davidson"**

---
---
**Printin Addiction** *January 25, 2018 13:28*

Thats alot better than my solution :)





![images/a2f182962101b09faf5277e6915cbea0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a2f182962101b09faf5277e6915cbea0.jpeg)


---
**BEN 3D** *January 28, 2018 11:43*

But yours is very funny :-)


---
*Imported from [Google+](https://plus.google.com/+DavidDavidson-tristar500/posts/eUAuL558fUU) &mdash; content and formatting may not be reliable*
