---
layout: post
title: "Hi Everyone. I have a chinese K40 that overall has performed well"
date: July 26, 2016 15:07
category: "Discussion"
author: "Mike Mauter"
---
Hi Everyone.

I have a chinese K40 that overall has performed well. Lately I have been noticing darker strips about 1/2 inch wide where the engraving is darker. I assumed that it was due to a difference in the wood(bamboo cutting boards). Yesterday when using the "Preview" option in Corelaser I was watching the amp meter when I noticed that it jumped to a higher value when the laser reached the point in the engraving where it is darker. Once the laser went about 1/2 inch the amp meter dropped to the original value. It did this again about 2 inches down but for only about 1/4 inch. Then as the laser completed its path marking the rectangle it did it again on the other side. I immediately reran the operation and it did not do this. But later in the day it did do it again. I have tried searching for this problem but so far haven't found anything.   There does not seem to be any obvious things like arcing or high water temp. Does anybody have any ideas? Could it be my power supply is going out? Thanks 





**"Mike Mauter"**

---
---
**greg greene** *July 26, 2016 18:39*

does your machine use a digital meter or an analogue one?

Do you turn a knob to set the power?  If so that is likely your problem - the Pot the knob turns is a cheap one and not that reliable.


---
**Mike Mauter** *July 26, 2016 19:21*

Yes it is analog. The thing that seems strange to me is that it tends to do it at about the same place on the bed and never seems to do it in the top fourth. If it was just diminishing  power i would think alignment. But an increase in power would seem to be electrical. Maybe it is a vibration that is causing the    pot to vary?


---
**greg greene** *July 26, 2016 20:45*

Good possibility for that - they are not mil spec units


---
**Alex Krause** *July 27, 2016 06:34*

Bed level issue I have done bamboo cutting boards and the bed level is really noticeable between the different strips in the bamboo... cut a 58.4 mm square of acrylic use it to test your bed level from the laser head to the board


---
**Ben Walker** *July 27, 2016 14:00*

Another thing - I accidentally had my my air assist attached incorrectly and the beam was hitting the edge of it as it exited the nozzle.  Caused the beam to split and do similar to your current situation.   Be careful - the nozzle can get very hot if that is happening.


---
*Imported from [Google+](https://plus.google.com/106963308841855914613/posts/HX7uGZyvoS6) &mdash; content and formatting may not be reliable*
