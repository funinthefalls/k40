---
layout: post
title: "I believe I have everything I need to start the conversion anyone care to walk me thru the logic side of the wiring..."
date: May 31, 2016 20:03
category: "Smoothieboard Modification"
author: "Alex Krause"
---
I believe I have everything I need to start the conversion anyone care to walk me thru the logic side of the wiring... the power side is pretty straight foward﻿

![images/6df50ea605823549becfea2527cd9d94.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6df50ea605823549becfea2527cd9d94.jpeg)



**"Alex Krause"**

---
---
**Ariel Yahni (UniKpty)** *May 31, 2016 20:57*

Good luck and God speed. 


---
**Tony Sobczak** *May 31, 2016 21:26*

Follow


---
**Don Kleinschnitz Jr.** *June 01, 2016 03:03*

Jealous: how did you get a Smoothie. I f I had mine we could build remotely together :).


---
**Alex Krause** *June 01, 2016 03:10*

I bought the smoothie a month or so ago the day they restocked uberclock


---
**Alex Krause** *June 01, 2016 03:13*

There is always the azteeg boards at [www.panucatt.com](http://www.panucatt.com) they are smoothie based boards ;)


---
**Don Kleinschnitz Jr.** *June 01, 2016 03:20*

Thanks but I am sticking to the Smoothie cause most folks seem to be using it. If I learned anything in my K40 and open hardware experience its stick with EXACTLY what other folks are using till you get it working :)


---
**cory brown** *June 01, 2016 04:34*

are you going to be running your steppers on 12 volts?


---
**cory brown** *June 01, 2016 04:54*

the ribbon cable is the hardiest to deal with... do you have a plan for that?


---
**Ariel Yahni (UniKpty)** *June 01, 2016 04:55*

**+Alex Krause**​ doesn't need the ribbon cable


---
**cory brown** *June 01, 2016 04:58*

**+Ariel Yahni** how would you run the X axis stepper motor and end stops?


---
**Ariel Yahni (UniKpty)** *June 01, 2016 05:03*

Our board came with a set of connectors for each of those. 


---
**cory brown** *June 01, 2016 05:07*

The smoothieboard did? or is it not one of those newer K40s without ribbon cable?


---
**Alex Krause** *June 01, 2016 05:17*

**+cory brown**​ I tagged you in a private post about it the board I had yesterday did you get the message?


---
**Alex Krause** *June 01, 2016 05:21*

We have a newer k40 that has the endstops on a seperate jack but the ribbon port is still on the control board just not used


---
**cory brown** *June 01, 2016 05:27*

Well that's headache avoided. Good to know. I seem to have not gotten it... I was notified when you tagged me in your last comment though...


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/TRzJsa9X139) &mdash; content and formatting may not be reliable*
