---
layout: post
title: "Something that I finally wanted to try to do the laser alignment, seems to be show that the principle works except my 3D printer is abit off so you need to align the red dot first"
date: June 04, 2016 17:04
category: "Hardware and Laser settings"
author: "Sunny Koh"
---
Something that I finally wanted to try to do the laser alignment, seems to be show that the principle works except my 3D printer is abit off so you need to align the red dot first. Suggest on the second mirror and the circle cut out between the first 2 mirrors, do the test dot. and you can then adjust the mirrors live. But have not fully tested the method yet. If anyone wants to try, I can post up the OpenSCAD files. Need to get back to other work, will be trying this again.



![images/12743dcf87fc51c2e7e377544ce68ee3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/12743dcf87fc51c2e7e377544ce68ee3.jpeg)
![images/edce89eabe80d6cdf3260c53589c077a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/edce89eabe80d6cdf3260c53589c077a.jpeg)
![images/c4948e00d9df305d2eb852957179893d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4948e00d9df305d2eb852957179893d.jpeg)
![images/87516b17236d8fafe4c6419e2377b85e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/87516b17236d8fafe4c6419e2377b85e.jpeg)

**"Sunny Koh"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 04, 2016 18:33*

I was thinking about this same thing yesterday & was wondering regarding the thickness of the red-dot-pointer beam. Would it be beneficial to focus the beam of the red-dot-pointer into a thinner beam to aid in precision of centring the beam on each mirror?


---
**Don Kleinschnitz Jr.** *June 06, 2016 12:24*

Check out this video and others by this author: 
{% include youtubePlayer.html id="XRizHrFSCwc" %}
[https://youtu.be/XRizHrFSCwc](https://youtu.be/XRizHrFSCwc)



I have been following this idea on multiple forums, and done a few experiments myself.



My K40 does not have enough room at its output to install anything ..... 



A summary of my tinkering here: 

[http://donsthings.blogspot.com/2016/06/k40-alignment-tools.html](http://donsthings.blogspot.com/2016/06/k40-alignment-tools.html)


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/ZmE5Hfz1Dnu) &mdash; content and formatting may not be reliable*
