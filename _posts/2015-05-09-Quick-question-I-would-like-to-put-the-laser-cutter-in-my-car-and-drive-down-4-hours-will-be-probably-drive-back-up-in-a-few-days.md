---
layout: post
title: "Quick question? I would like to put the laser cutter in my car and drive down 4 hours, will be probably drive back up in a few days"
date: May 09, 2015 19:21
category: "Discussion"
author: "jamie gomez"
---
Quick question? I would like to put the laser cutter in my car and drive down 4 hours, will be probably drive back up in a few days. The car is a new car (2014) so no suspension issues. How can I securely drive it down? Theres a lot of bumper on highway going down in general. I guess my main concern is c02 tube. If I could securely take it out and wrap it separately in bubble wrap I would be fine... Or even somehow secure it while its inside? Any advise?

Thanks





**"jamie gomez"**

---
---
**Sean Cherven** *May 09, 2015 23:55*

As long as you don't drop the unit, it should be fine. Just make sure nothing is in the tube compartment.


---
*Imported from [Google+](https://plus.google.com/114723275374564353861/posts/3SPipxNpgi2) &mdash; content and formatting may not be reliable*
