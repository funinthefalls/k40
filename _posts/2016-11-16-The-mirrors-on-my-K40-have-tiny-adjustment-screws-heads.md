---
layout: post
title: "The mirrors on my K40 have tiny adjustment screws heads"
date: November 16, 2016 01:12
category: "Modification"
author: "Ulf Stahmer"
---
The mirrors on my K40 have tiny adjustment screws heads. Couple that with tight spaces and large hands and mirror alignment becomes an exercise in futility! Thus I had put off alignment while focusing on rewiring, installing a smoothie, and the laser safety system.



Now with the smoothie and the safety system installed, my procrastination in aligning the mirrors needed to come to an end. Initially, I was going to 3D print new knobs. Then I was going to buy new screws. But finally, while looking for something in my small parts cabinet, I lucked upon some plumbing washers and thought they looked perfect - and they were!



For the first mirror, I made new knobs by cutting short lengths of the right size Allen key and, with the help of some duct tape, inserting them into the washers. For the second mirror, the flat washers fit perfectly over the knurled screw heads.



Now I was finally able to adjust my mirrors with my big paws!. At first, I was disappointed in the results, but then realized my focal distance was wrong. My test cuts on poplar were more than 6 mm deep at 16 mA and 5 mm/s! Whoo hoo! 



![images/075fa99b3fdfd19fd5f9e4856d33a4fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/075fa99b3fdfd19fd5f9e4856d33a4fd.jpeg)
![images/753192a2685e9348a71bd587fbb1e7e1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/753192a2685e9348a71bd587fbb1e7e1.jpeg)
![images/f28816417dc6a9bde98da8bac4747351.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f28816417dc6a9bde98da8bac4747351.jpeg)
![images/e4c1461d9e7babafba7efb921d5a26cf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e4c1461d9e7babafba7efb921d5a26cf.jpeg)

**"Ulf Stahmer"**

---
---
**Anthony Bolgar** *November 18, 2016 01:08*

Nice trick


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/96xzLeMExJb) &mdash; content and formatting may not be reliable*
