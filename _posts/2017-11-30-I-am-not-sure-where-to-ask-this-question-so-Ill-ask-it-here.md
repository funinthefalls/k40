---
layout: post
title: "I am not sure where to ask this question so I'll ask it here"
date: November 30, 2017 14:08
category: "Discussion"
author: "Rob Mitchell"
---
I am not sure where to ask this question so I'll ask it here. I would like to purchase a K40 laser cutter/engraver. I did a little searching on this forum but could not find where folks are purchasing these from. I am from Canada so I would like to source something with reasonable shipping.



Any help would be appreciated. I'd rather source from a reference where you bought yours so please reply with a seller that you have experience with.



Thank You





**"Rob Mitchell"**

---
---
**Joe Alexander** *November 30, 2017 14:25*

most of us buy it off of eBay(at least for USA) but I have heard that aliexpress works also. Just read carefully as per usual. most sellers on ebay are decent just check the shipping costs, I can't remember which one sold me mine.


---
**Joe Alexander** *November 30, 2017 14:27*

also note the price is known to drop after the holidays, i got mine for just over $300 US


---
**Chris Hurley** *November 30, 2017 14:31*

Where are you in Canada? Are you near the border? 


---
**Rob Mitchell** *November 30, 2017 14:49*

**+Chris Hurley** Ya I'm not too far away in Ottawa


---
**Chris Hurley** *November 30, 2017 15:00*

I was planning to ship one to a ups store just south of the border (free shipping) and driving down to get it. 


---
**Rob Mitchell** *November 30, 2017 16:13*

Have you found a "reputable" seller? That's what I'm hoping to have someone provide. 




---
*Imported from [Google+](https://plus.google.com/105696631925164417072/posts/8EsegQpfU4M) &mdash; content and formatting may not be reliable*
