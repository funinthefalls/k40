---
layout: post
title: "what kind of protective eye where do people use with their K40"
date: January 15, 2016 03:23
category: "Hardware and Laser settings"
author: "3D Laser"
---
what kind of protective eye where do people use with their K40





**"3D Laser"**

---
---
**Sean Cherven** *January 15, 2016 04:27*

I'm still wondering this myself lol


---
**Jim Hatch** *January 15, 2016 05:10*

These: [http://smile.amazon.com/gp/product/B000HJMS4A?psc=1&redirect=true&ref_=oh_aui_search_detailpage](http://smile.amazon.com/gp/product/B000HJMS4A?psc=1&redirect=true&ref_=oh_aui_search_detailpage)



or these: [http://smile.amazon.com/gp/product/B00L2SU1TY?psc=1&redirect=true&ref_=oh_aui_search_detailpage](http://smile.amazon.com/gp/product/B00L2SU1TY?psc=1&redirect=true&ref_=oh_aui_search_detailpage)



I wear glasses but both fit over them so that's not an issue. I tend to wear the first pair more. I have two in case someone comes to play with my laser & me so they have something too.


---
**Jim Root** *January 15, 2016 05:31*

I bought some fancy safety glasses but I don't know what I did with them. I just keep the lid shut.




---
**Stephane Buisson** *January 15, 2016 10:20*

buy 10600  Glasse,not (1060) (CO2 lasers typically emit at a wavelength of 10.6 μm)



this is particularly usefull when you set your mirrors (lid open).


---
**Gary McKinnon** *January 15, 2016 14:58*

These ones from China are good : [http://www.ebay.co.uk/itm/Protection-Safety-Glasses-Goggles-for-10600nm-CO2-laser-with-CE-CERTIFICATION-/321305540858?hash=item4acf4d78fa:g:aMQAAOxycmBS4XTL](http://www.ebay.co.uk/itm/Protection-Safety-Glasses-Goggles-for-10600nm-CO2-laser-with-CE-CERTIFICATION-/321305540858?hash=item4acf4d78fa:g:aMQAAOxycmBS4XTL)


---
**3D Laser** *January 15, 2016 15:07*

As long as I keep the lid down when firing I should be ok right?  


---
**Gary McKinnon** *January 15, 2016 15:13*

Yes no problem there.


---
**David Richards (djrm)** *January 15, 2016 16:23*

**+Gary McKinnon** My own safety glasses look very similar, they just fit over my ordinary glasses. They came in a nice case too.


---
**Gary McKinnon** *January 15, 2016 16:27*

I've read that ordinary perspex does the same job but i don't know it for a fact and won't be testing it!


---
**Anthony Bolgar** *January 15, 2016 16:51*

Any pair of standard safety glasses will work, no need to buy special glasses.


---
**Gary McKinnon** *January 15, 2016 17:19*

Acrylic blocks it : [http://midopt.com/filters/ac760/](http://midopt.com/filters/ac760/)



So yes probably most plastics.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/2y2Ej8kx15g) &mdash; content and formatting may not be reliable*
