---
layout: post
title: "Some work on aluminum for an AR -15 lower receiver for a friend of mine....etched pretty deep at 12mA engraving speed of 200mm/s....the other round piece is a file I downloaded of the net....pretty neat"
date: December 13, 2015 15:20
category: "Discussion"
author: "Scott Thorne"
---
Some work on aluminum for an AR -15 lower receiver for a friend of mine....etched pretty deep at 12mA engraving speed of 200mm/s....the other round piece is a file I downloaded of the net....pretty neat.



![images/0a09a20c4befc52d9aec841c7a42ef9a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a09a20c4befc52d9aec841c7a42ef9a.jpeg)
![images/dfbb7b7310d38d3f81e3c0138252817c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dfbb7b7310d38d3f81e3c0138252817c.jpeg)

**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2015 17:00*

Looking good. That piece for the AR-15 lower receiver looks like maybe you could have done the design as a cut instead of engrave (since it is mostly lines). Would possibly give a smoother look to the entire design.


---
**Scott Thorne** *December 13, 2015 17:24*

I tested it both ways and it came out better as an engrave....don't quite understand why but it's more defined as an engrave rather than a cut.


---
**ChiRag Chaudhari** *December 13, 2015 18:42*

both of them looks pretty darn good. I really need to spend some time getting the cutting bed up so that I can start playing too.


---
**Coherent** *December 14, 2015 14:53*

The lower looks good. I have a couple I want to do that are black iodized. Did you fill in the engraving with a color? It looks gold colored in the photo.


---
**Scott Thorne** *December 14, 2015 15:37*

**+Coherent**​​....no color fills...that's the way it turned out...Lol﻿


---
**Scott Thorne** *December 14, 2015 21:59*

**+Chirag Chaudhari**​...thanks man.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/Hu3VrhwqHSP) &mdash; content and formatting may not be reliable*
