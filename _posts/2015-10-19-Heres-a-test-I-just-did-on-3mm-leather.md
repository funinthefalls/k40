---
layout: post
title: "Here's a test I just did on 3mm leather"
date: October 19, 2015 11:07
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Here's a test I just did on 3mm leather. Settings were 6mA on the dial, 500mm/s engrave speed, 1 pixel steps.



Obviously it doesn't engrave that quickly though. Took more than 10 minutes to complete the project. Didn't time exactly.



![images/7effdeec589fa0477f1bdbacacc1cbc3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7effdeec589fa0477f1bdbacacc1cbc3.jpeg)
![images/f511f3eb3bed1e503528b591271445a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f511f3eb3bed1e503528b591271445a9.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Peter** *October 19, 2015 12:49*

Very neat. I had some results markibg on the inside of some soft 1mm scraps and is cut quite cleanly in the outside of the same but I didn't take the speed/power settings. 

I'd like to be able to cut small patterns to stitch together for 3d keyrings or wallets etc. 


---
**Ashley M. Kirchner [Norym]** *October 19, 2015 16:21*

Hey **+Yuusuf Salahuddin**, do you use the laser to cut leather as well, or just engrave it and cut using scissors or ... ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 19, 2015 21:40*

**+Ashley M. Kirchner** Hi Ashley. I have just purchased this laser so still learning how to use it properly. So far I just did the engraving with the laser & then cut the leather with a Stanley/craft knife & punched the holes with my hole punches.



I intend to use the laser to cut the leather also in future (once I figure out the correct settings).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 19, 2015 21:41*

**+Peter Jones** Yeah I have been keeping track of speed/power settings as I go to hopefully create a bit of a guide for me in future (also for others who are interested). That way when I switch materials I will know what settings to use :)


---
**Alessandro Milano** *October 28, 2015 10:27*

It's a good work.

It will be usefull to create a shared document with Power and Speed settings for every type of material, divided in cut and engrave.

+Ashley M. Kirchner : in the past I tried to cut leather with laser but it didn't work well... so I only print on leather


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2015 12:53*

**+Alessandro Milano** That's very true. So far I haven't tried many settings, but have been keeping track of it. Maybe an option would be a Dropbox collaborated Excel spreadsheet or something.



I've also found so far that cutting leather is very difficult (even at full power with 30mm/s cut speed it only 90% cuts through 3mm leather & makes a hell of a mess with soot everywhere). I'd be interested to test cutting leather whilst it is damp (not dripping). Might give some different results.


---
**Scott Marshall** *December 18, 2015 16:05*

You may try wiping it lightly with neatsfoot oil. you may get a darker mark with less power and less actual burning.



 I've done the neatsfoot thing using a burning iron and it works much better than dry. Hard to say how the laser will react, but I'd try it on a piece of scrap.



On the water, it's worth a try, but I'm betting it just makes it harder to mark, the flash steam probably will eat up the energy without a color change.



Good Luck!

Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2015 22:54*

**+Scott Marshall** Thanks Scott. That's an interesting idea. Will have to grab some neatsfoot oil & give it a try.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Z9BKzQMYMAe) &mdash; content and formatting may not be reliable*
