---
layout: post
title: "New to the Laser world, just getting setup have burnt a few things up ;-) wonder what a down and dirty way to know where to place my material to cut or engrave"
date: February 19, 2016 19:23
category: "Discussion"
author: "Charlie Deckert (Texas Bounty Hunters)"
---
New to the Laser world, just getting setup have burnt a few things up ;-) wonder what a down and dirty way to know where to place my material to cut or engrave. I don't have a red laser mod yet but I have one on order. If I have a piece of material 2 inches wide and I want to engrave inside the borders what is the best way to get the material lined up?



Thanks

Charlie





**"Charlie Deckert (Texas Bounty Hunters)"**

---
---
**Dreamsforgotten** *February 19, 2016 21:57*

I use blue painter's tape, lay it on the bed and do a low power dry run


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 20, 2016 00:59*

I've placed a piece of mdf onto the entire cutting/engraving area & then just cut out a section 300x200mm into the mdf. Then I just push my work piece up against the top left corner to the origin point. Sometimes (when I bump the head) I have to reset the 0,0 (x,y) origin point for the head to fire right in the topleft corner of the cutout section.


---
*Imported from [Google+](https://plus.google.com/109827690982256651231/posts/Kiw7xDsrebr) &mdash; content and formatting may not be reliable*
