---
layout: post
title: "Made a cutting bed for my Lasercutter which can be replaced as needed, it's acrylic so I hope it doesn't leave burn marks on the underneath of objects beeing cutted..."
date: August 18, 2016 08:27
category: "Object produced with laser"
author: "Gunnar Stefansson"
---
Made a cutting bed for my Lasercutter which can be replaced as needed, it's acrylic so I hope it doesn't leave burn marks on the underneath of objects beeing cutted... haven't tested yet though... Honeycomb grid was to expensive for my 800x800mm area... And previous solution left burn marks.



![images/19536dc0ec91fe870d1c15c3a1d9944a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19536dc0ec91fe870d1c15c3a1d9944a.jpeg)
![images/8cfa3345ec420c3b9891da54e552a89b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8cfa3345ec420c3b9891da54e552a89b.jpeg)

**"Gunnar Stefansson"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 18, 2016 08:50*

Looks good. Hopefully it doesn't leave burn marks or melted acrylic on the back of your pieces. You may be best going with some Nail Bed or something?


---
**Gunnar Stefansson** *August 18, 2016 09:01*

**+Yuusuf Sallahuddin** Yes that was also my backup plan if this doesn't cut it ;) I think this is more just as a curiosity :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 18, 2016 09:04*

**+Gunnar Stefansson** I hope it works for you, because it looks like a very well designed piece :)


---
**Gunnar Stefansson** *August 18, 2016 13:01*

Hehehe thanks **+Yuusuf Sallahuddin** it's definetly simple, and looks good new! But let's see, I'll post something when I have cut alot on it ;)


---
**HP Persson** *August 19, 2016 09:46*

I have a acrylic bed too, every 3 months or so i have to ut a new one and transfer the pins to the new. But it´s working soo good i can stand with that extra 2 minutes :)



I get no reflections at all


---
**Gunnar Stefansson** *August 19, 2016 11:10*

**+HP Persson** Awesome, I'm really glad to hear that, well sofar with three cutting projects I havent had any marks at all and the acrylic is holding up nicely... sofar so good ;)


---
**Chris Sader** *August 19, 2016 20:16*

**+Gunnar Stefansson** care to share the design files?


---
**Gunnar Stefansson** *August 20, 2016 12:10*

**+Chris Sader**  Yes I can share them, I use cambam, so it's not a picture file, I can take a picture of it and give some dimensions so you can scale it for the standard laser engraving that most use ;)


---
**Victor Hurtado** *August 22, 2016 03:11*

Hello friend suddenly you help me with the original file of the acrylic to be able to do something for my laser 50x40 cm. THANK YOU.


---
**Gunnar Stefansson** *August 22, 2016 05:23*

**+Victor Hurtado** I could do something, you do however need something to screw the 5mm mounting brackets on to... is your square exactly 50x40cm from the inside?


---
**Gunnar Stefansson** *August 22, 2016 05:52*

**+Chris Sader** **+Victor Hurtado** here are some files, Victor try and see if you can use them first? 

[https://drive.google.com/drive/folders/0B3-WzWhvXB8EQmZOVDdZbHdtazg?usp=sharing](https://drive.google.com/drive/folders/0B3-WzWhvXB8EQmZOVDdZbHdtazg?usp=sharing)


---
**Victor Hurtado** *August 22, 2016 11:43*

Help me with your email, to send you the pictures of my laser machine


---
**Chris Sader** *August 22, 2016 13:48*

**+Gunnar Stefansson**​ thanks, I'll take a look. Can you tell me more about your laser cutter? 800x800mm?


---
**Gunnar Stefansson** *August 22, 2016 17:49*

**+Victor Hurtado** I believe you can modify the files to make something fit your own machine, for me it's simple as I'm using 20x20 profiles allround, you'll just have to see what's possible with yours... **+Chris Sader** my machine is nothing special, just something I made completely from scratch with lots of trial and errors here and there and plenty of room for improvement... I'll post some Pictures and explain what my machine is all about, for those who are interested ;) I'm going on a trip for now, but around 13th Sept, I'll post you something ;)


---
*Imported from [Google+](https://plus.google.com/100294204120049700616/posts/818V2y6zyn8) &mdash; content and formatting may not be reliable*
