---
layout: post
title: "Ok, here's another one :P This one is a Past Master symbol"
date: November 19, 2017 03:26
category: "Object produced with laser"
author: "Ned Hill"
---
Ok, here's another one :P  This one is a Past Master symbol. It's from an antique etching that I spent bunch of time cleaning up.  1/8" Alder with poly finish.



![images/4b3770c614f6dba8900a0cd0fab65835.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b3770c614f6dba8900a0cd0fab65835.jpeg)
![images/c9ab49f42739e2e67aaa9839e2336ae6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c9ab49f42739e2e67aaa9839e2336ae6.jpeg)
![images/6375d80516b6893375207d9c841c3100.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6375d80516b6893375207d9c841c3100.jpeg)

**"Ned Hill"**

---
---
**BEN 3D** *November 19, 2017 08:37*

Fantastic!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/WQ72a3mq5Et) &mdash; content and formatting may not be reliable*
