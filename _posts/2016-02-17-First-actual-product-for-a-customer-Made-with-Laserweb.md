---
layout: post
title: "First actual product for a customer. Made with Laserweb"
date: February 17, 2016 03:18
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
First actual product for a customer. Made with Laserweb



![images/7cc254d8e32a5c53304e83d7775e0a4e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7cc254d8e32a5c53304e83d7775e0a4e.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**Scott Marshall** *February 17, 2016 04:15*

Nice!



Customers......Hmmm.... 

Novel concept. May have to look into that.


---
**I Laser** *February 17, 2016 05:43*

Nice work. :)



**+Scott Marshall** lmao, they do come in handy for justifying the purchases to your other half!


---
**Alex Krause** *February 17, 2016 08:01*

Is this acacia wood?


---
**Scott Marshall** *February 17, 2016 09:15*

Actually, I did sell 4 coasters and 4 keychains to a RC club in Mn that bought shirts from me. (I make tee shirts too. Another CNC gadget - vinyl cutter.



If I paid for the electric I used this month, I'd be happy.



My excuse is: "It keeps me from going crazy (ier) "


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/3cfjZX27Pnx) &mdash; content and formatting may not be reliable*
