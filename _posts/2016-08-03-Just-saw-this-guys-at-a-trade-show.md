---
layout: post
title: "Just saw this guy's at a trade show"
date: August 03, 2016 00:04
category: "Materials and settings"
author: "Ariel Yahni (UniKpty)"
---
Just saw this guy's at a trade show. Good source for ideas 



![images/96d82bdaa0d6d5887704fdc686c9b949.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96d82bdaa0d6d5887704fdc686c9b949.jpeg)
![images/bdf89a5672e3e7a750cdf7db5b99bb24.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bdf89a5672e3e7a750cdf7db5b99bb24.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *August 03, 2016 00:24*

Who is it? Hard to make out from the photos.


---
**Ariel Yahni (UniKpty)** *August 03, 2016 00:36*

[pgrahamdunn.com](http://pgrahamdunn.com)


---
**Jim Hatch** *August 03, 2016 00:38*

Cool. Thanks.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/PmjqxrTgq8A) &mdash; content and formatting may not be reliable*
