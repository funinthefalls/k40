---
layout: post
title: "I've been watching everyone with their K40s on here for a while"
date: May 23, 2016 22:45
category: "Discussion"
author: "Jonathan Ransom"
---
I've been watching everyone with their K40s on here for a while. I'm very interested in getting a laser cutter. I already have a shapeoko 2. Would you guys recommend adding a laser to it or ordering a K40?﻿





**"Jonathan Ransom"**

---
---
**HalfNormal** *May 23, 2016 23:31*

I have both and are used for different projects. Just depends on what you want it for and know it's limitations. 


---
**Anthony Bolgar** *May 24, 2016 00:42*

Adding a laser diode to your CNC will allow you to engrave, but have no illusions about cutting, a diode is no where near powerful enough to cut anything other than very thin dark paper. If you want to be able to cut material as well, then the K40 is your better option.


---
**Jonathan Ransom** *May 24, 2016 03:07*

Good to know **+Anthony Bolgar**​. Thanks for the info!


---
*Imported from [Google+](https://plus.google.com/+JonathanRansom/posts/DZHzLGG9JnR) &mdash; content and formatting may not be reliable*
