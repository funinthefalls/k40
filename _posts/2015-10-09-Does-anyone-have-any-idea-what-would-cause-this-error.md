---
layout: post
title: "Does anyone have any idea what would cause this error?"
date: October 09, 2015 22:30
category: "Hardware and Laser settings"
author: "Trevor Reed"
---
Does anyone have any idea what would cause this error? Every time I turn on the engraver, whether it is connected to my computer or not, the laser head tries to zero itself at some unknown, very large, x value. It will do this indefinitely, so I cannot use the engraver at all. Thanks in advance for help, I am at a loss for ideas.


**Video content missing for image https://lh3.googleusercontent.com/-rCVokKZ_4WU/Vhg_xGpRB6I/AAAAAAAABnU/IiLSjNSDiaQ/s0/laser%252Bengraver%252Berror.mp4.gif**
![images/7916f22d5eaaa271c7f68cc570ebec07.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/7916f22d5eaaa271c7f68cc570ebec07.gif)



**"Trevor Reed"**

---
---
**Trevor Reed** *October 09, 2015 22:32*

The mainboard is a Lihuiyu Studio Labs model 6c6879-Laser-m2:6 and my engraving ID is 16D2EAFD3754A0F2


---
**Joey Fitzpatrick** *October 10, 2015 00:00*

Is this machine new and has it ever worked properly before? I had a similar problem when I first got my K40.  It wound up being a bad connection to my X axis limit switch wiring at the mainboard(The plug on the very end of the mainboard)  The plug will only have 3 wires to it.  The crimp that was made on the middle wire was bad.  Try unplugging the connector and then re-installing it.


---
**Trevor Reed** *October 10, 2015 00:14*

It is new and the problem has affected the machine since I started using it. I was able to manually set it early on when I was using the trial version of the software. But now, no such luck. I'll give what you suggested a shot and hopefully it will work. Thank you!


---
**Trevor Reed** *October 10, 2015 00:23*

Found a faulty wire connection by tracking back from the plug you directed me to. A wire strip and solder job later, problem solved! Thank you very much!


---
**Joey Fitzpatrick** *October 10, 2015 00:34*

Awesome!!  Glad to hear that you got it fixed!!


---
**Anton Fosselius** *October 10, 2015 16:27*

Ah! I got that problem as well! I guessed that there where no limit switches :p


---
**Al Tamo** *October 10, 2015 23:24*

Yo creo que es problema del controlador del eje de la tarjeta, yo tenía el mismo problema hasta que se quemó ese chip, y estoy esperando a recibirlo para cambiarlo, y ver si se soluciona.


---
*Imported from [Google+](https://plus.google.com/115145285623086933273/posts/8GsDGiAo7HF) &mdash; content and formatting may not be reliable*
