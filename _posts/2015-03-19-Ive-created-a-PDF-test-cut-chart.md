---
layout: post
title: "I've created a PDF test cut chart"
date: March 19, 2015 11:47
category: "External links&#x3a; Blog, forum, etc"
author: "Eddie Pratt"
---
I've created a PDF test cut chart. I'm using the Corel Laser package that came with my K40. Just open it with that, if you have it. Resolution test goes from 5mm down to 0.1mm.





**"Eddie Pratt"**

---
---
**Stephane Buisson** *March 19, 2015 14:52*

**+Eddie Pratt** Thank you for sharing , and welcome among us.


---
**Eddie Pratt** *March 19, 2015 19:12*

You're welcome. I hope it's useful to people.


---
**Jose A. S** *March 31, 2015 14:54*

THe link is not working!


---
**Eddie Pratt** *March 31, 2015 15:01*

I just tried. Seemed ok. Not sure what the problem is.


---
**Jose A. S** *April 04, 2015 06:05*

Do you have paramethers to engrave and cut MDF, power and speed.


---
**Eddie Pratt** *April 04, 2015 11:00*

Hi Jose. Sorry I don't have experience with MDF, but it will depend on several factors: thickness of MDF, density of MDF, construction/glue method etc. You should research carefully before deciding about making any cuts. Especially check for fire hazards and fire extinguishers for MDF and also smoke released, as apparently this can release toxins when cutting. I think laser-cut MDF can release several toxic chemicals including: urea- or phenol-formaldehyde that can be hazardous. Cutting can release a lot of smoke, especially high-power, low-speed cuts, and if you don't have good ventilation and extraction, this might be dangerous or cause problems. 


---
**Jose A. S** *April 05, 2015 03:01*

Thank you Eddie. Pretty nice advice :)


---
*Imported from [Google+](https://plus.google.com/108440927309352829036/posts/3Z9ypUsoWrn) &mdash; content and formatting may not be reliable*
