---
layout: post
title: "I'm just getting around to setting up my K40, and I know I should add air assist"
date: March 22, 2017 00:26
category: "Air Assist"
author: "James Rivera"
---
I'm just getting around to setting up my K40, and I know I should add air assist.  I have a 3D printer, and I recall seeing (way back when I first started looking at this) that people were printing their own air assist nozzles. QUESTION: if you did this, which design did you go with, what do you like (or dislike) about it, and if you were to do it again, which design/approach would you take? Thanks in advance for any replies, they are appreciated.





**"James Rivera"**

---
---
**Ashley M. Kirchner [Norym]** *March 22, 2017 00:53*

My nozzle is loosely based on the one here: [http://www.thingiverse.com/thing:890705](http://www.thingiverse.com/thing:890705)



I've made my own adjustments and changes to that design.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 22, 2017 04:14*

I designed my own from scratch, that did not cover the lens, but rather wrapped around it & focused the air in a conical pattern to reach 50.8mm (focal point of my lens).



Personally, I was happy with the design, however the throughput of air is very weak. It requires a much larger air source than I am using (2 pathetically weak aquarium style air pumps). I tried a small airbrush compressor also, but even the output of that was too weak for my design.



All in all, I'd say the conical nozzles that focus air directly downwards are probably a better solution than what I'm using.


---
**Stephane Buisson** *March 22, 2017 13:39*

**+James Rivera**, that was one of my first mod.

i design the following one to try to implement few ideas :



- setting the focus lenght (lens on air assit)

- air comming from the cone center AND from the side (to push fumes away toward exhaust)

- hold crosshair red laser



[youmagine.com - K40 chinese lasercutter air assist with cross lines laser](https://www.youmagine.com/designs/k40-chinese-lasercutter-air-assist-with-cross-lines-laser)



BUT experiences showed plastic Air assist could take fire (so i wrap it into foil )



Today I would go for a more simple way, a little coper pipe/needle (maybe with a 3d printed adapter) about 1mm over laser spot hit coming from 45° aside blowing toward exhaust . being close you will need a smaller compressor which will reduce noise and increase your confort.

No crosshair laser, but 2 single ray showing focus point at their intersection, so if you see 2 spots on material you are out of focus.




---
**James Rivera** *March 22, 2017 16:06*

**+Stephane Buisson** Thanks! Does this cross line laser look like the one you used?



[ebay.com - Details about  650nm 5mW Red Laser Cross Line Module Focus Adjustable Laser Head 5V HQ NEW](http://www.ebay.com/itm/650nm-5mW-Red-Laser-Cross-Line-Module-Focus-Adjustable-Laser-Head-5V-HQ-NEW/122194569291)


---
**Stephane Buisson** *March 22, 2017 16:23*

**+James Rivera** yes that it is the one i got. (but not very good, because coming from aside, it show hitting point only if you are at focus level)

but as I was saying 2 single dot ray (not line or cross), should be a better option. if you fit them each side of the head pointing to focal point. you are at focus when they crossed (in Z) telling you if you are in focus when you see only one dot on the material.

So to recap the laser dot show perfect positioning in XYZ, not like the hair crossed laser showing the hit spot (XY) IF at 50.8mm. (big IF) so not so handy. (I hope I am clear enought)



if you go for the other mod  (the spring loaded bed at focal point) so that crossed laser would be a solution.


---
**Ashley M. Kirchner [Norym]** *March 22, 2017 20:30*

Don't use a cross hair. The point where they cross will move as the material height )or bed height) changes. Use two separate straight lines or two dots like Stephane suggested.



I have a small opening on my nozzle that blows straight down into the cut. It absolutely helps with the cutting. I used to cut wood consistently at 13-15mA, now I cut between 9-11mA. Lower mA also means a longer tube life.



And I have yet to have my nozzle catch fire. :) Stephane must be burning with jet fuel. :)


---
**James Rivera** *March 22, 2017 21:28*

**+Stephane Buisson** So, maybe I should get two of these then?



[ebay.com - Details about  650nm 5mW Red Laser Dot Module Focus Adjustable Laser Head 5V HQ NEW](http://www.ebay.com/itm/650nm-5mW-Red-Laser-Dot-Module-Focus-Adjustable-Laser-Head-5V-HQ-NEW/122180507840)


---
**Stephane Buisson** *March 22, 2017 22:49*

yep, those one look good.


---
**James Rivera** *March 22, 2017 23:35*

Done! I ordered 4. If I fry 1 or 2, I'll have backups. Do they need any special driver circuitry, or just 5v DC?


---
**James Rivera** *March 23, 2017 00:10*

Huh. Come to think of it, I ordered a five pack of lasers <i>similar</i> (except they're 5mW, 4.5v, 650nm) to the ones in the link below (in 2014!) and should have them lying around. I never tried powering them, but now I think I will try them with 3 AA batteries in series to see if they work (or fry).



[https://www.newegg.com/Product/Product.aspx?Item=9SIA86V2Z32250](https://www.newegg.com/Product/Product.aspx?Item=9SIA86V2Z32250)


---
**Ashley M. Kirchner [Norym]** *March 23, 2017 00:10*

Just 5V DC


---
**Ashley M. Kirchner [Norym]** *March 23, 2017 00:39*

generally they run about 4.5 to 5.5v ... possible to run them lower, the light will just be dimmer


---
**BeenThere DoneThat** *March 23, 2017 01:34*

I use this one [thingiverse.com - Air Assist Nozzle for DC-KIII, K40 Chinese 40 watt Laser Cutter / Engraver by 2ROBOTGUY](http://www.thingiverse.com/thing:343494)



I added a set screw and use a right angle air fitting. I use a salvaged air pump from a respirator. 


---
**Kyle Kerr** *March 27, 2017 15:45*

Late to the party. Might I suggest a simple gauge block?

I don't have a machine yet, but, there is a use case I'm contemplating that I would like to try with an unfocused laser. With a gauge block getting consistent results should be a simple matter.


---
**Ashley M. Kirchner [Norym]** *March 27, 2017 15:51*

Gauge blocks works great if you're consistently using the same material thickness. Otherwise, you'll be adjusting the height for each thickness anyway. You want the focus point to be in the middle of the material for cutting, and on the surface for engraving.


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/hjN2TGdS9Vn) &mdash; content and formatting may not be reliable*
