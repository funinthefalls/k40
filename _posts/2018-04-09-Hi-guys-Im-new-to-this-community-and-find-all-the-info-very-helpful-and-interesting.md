---
layout: post
title: "Hi guys, I'm new to this community and find all the info very helpful and interesting"
date: April 09, 2018 02:34
category: "Discussion"
author: "Stefan and Dina Venter"
---
Hi guys,



I'm new to this community and find all the info very helpful and interesting.

I recently purchased a K40 and still test driving it. I bought it to make some cake toppers for my wife's cake business, but have no idea to do these toppers (attached picture). Could someone please give my some ideas with regards to  how do I mark the red vector cuts to cut it out. If I use Inkscape with a pen, it takes forever and doesn't look so good.



Thanks 

Stefan

![images/a8a42dc303ec77d18bcd5b57fdfe67dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a8a42dc303ec77d18bcd5b57fdfe67dc.jpeg)



**"Stefan and Dina Venter"**

---
---
**James Rivera** *April 09, 2018 06:31*

Have you watched Scorchworks YouTube tutorials for K40Whisperer? He shows you exactly how to do it. Short answer is: select the letters, do object to path, then set the stroke color, and turn off the stroke fill.


---
**Stefan and Dina Venter** *April 12, 2018 08:57*

**+James Rivera**  Hi James,

Thanks for your reply, I did manage to do that. I'm slowly but surely learning Inkscape.


---
*Imported from [Google+](https://plus.google.com/111613950403951520185/posts/BbPHJ5UKfDW) &mdash; content and formatting may not be reliable*
