---
layout: post
title: "Has anyone else noticed that even though there are over 1000 users of this group, there are only about 20 of us that are here on a daily basis?"
date: April 26, 2016 20:18
category: "Discussion"
author: "Anthony Bolgar"
---
Has anyone else noticed that even though there are over 1000 users of this group, there are only about 20 of us that are here on a daily basis?





**"Anthony Bolgar"**

---
---
**Alex Krause** *April 26, 2016 20:49*

*Lurking...


---
**Scott Marshall** *April 26, 2016 20:52*

Normal for most forums I've been on. I think we're pretty diverse


---
**Chema Muñoz** *April 26, 2016 20:59*

Most of us are learning from you...


---
**Anthony Bolgar** *April 26, 2016 21:02*

Nice to know that our knowledge is of use to others.


---
**Tony Schelts** *April 26, 2016 21:31*

Well I for one am grateful for this group and must admit I lurk here sometimes just to see whats going on. Unless lurking means something else?


---
**Joe Alexander** *April 26, 2016 21:35*

<--lurker that reads every day, still have yet to buy a machine :P


---
**Tony Schelts** *April 26, 2016 22:28*

oh


---
**MITCH SMITH** *April 26, 2016 22:47*

✋ always lurking


---
**Scott Marshall** *April 26, 2016 23:01*

**+Joe** We gotcha, and you know it, 



It's only a matter of time and you'll be digging thru your house looking for something else to laser. 


---
**Steve B. (Thomas delbroc)** *April 26, 2016 23:22*

It's not lurking, it's "gathering intel".


---
**Ross Hendrickson** *April 26, 2016 23:36*

Learning, not lurking :)


---
**Anthony Bolgar** *April 26, 2016 23:39*

I have to admit I lurked for the first month or so I had my K40. This group has proved very valuable in learning about the laser, but also meeting and making friends with some very intelligent makers.


---
**Jim Hatch** *April 26, 2016 23:55*

Gotta do something while the machine is running. Pretty sure if I leave it alone it'll catch on fire or something :-) 


---
**Scott Thorne** *April 27, 2016 01:14*

**+Anthony Bolgar**...so true my friend...I talk to you guys more than my wife! 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 01:27*

**+Steve B.** **+Ross Hendrickson** Covert Learning Operatives :D


---
**Tony Sobczak** *April 27, 2016 04:43*

Lurker here,  at least 3x/day.


---
**I Laser** *April 27, 2016 06:58*

Yeah there's definitely a lot of people lurking. :D



It's fantastic to be able to pick the brains of the knowledgeable people here when I'm not sure what I'm doing ( which come to think of it happens a bit :| ).



**+Jim Hatch** lol that's why I bought a second machine, most of the time I'm setting up the second while the first it pluggin away, though my plan has seemingly been thwarted for the moment given my issues with the tube on my original machine.


---
**Justin Mitchell** *April 27, 2016 12:53*

Lurker, and occasional heckler ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 13:03*

**+Justin Mitchell** With cool Goggles 8-)


---
**David Cook** *April 27, 2016 13:47*

I noticed lol. It's one of the best groups I belong to though. No arguing just learning and having fun 


---
**Richard** *April 27, 2016 15:29*

It's because the 20 of you respond to questions and concerns several hours before I even see the post, so there's not much point in me adding anything... I do check back regularly, I try for once a day.


---
**David Richards (djrm)** *April 27, 2016 18:19*

I'm lurking on here more than using laser.


---
**Scott Thorne** *April 27, 2016 20:35*

Although I don't post every day...I read all others post  several times a day....gotta find something to do at work when things are running good. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/fJgvG6n2cJe) &mdash; content and formatting may not be reliable*
