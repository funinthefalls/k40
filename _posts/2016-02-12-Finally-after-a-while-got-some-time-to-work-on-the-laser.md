---
layout: post
title: "Finally after a while got some time to work on the laser"
date: February 12, 2016 03:04
category: "Modification"
author: "ChiRag Chaudhari"
---
Finally after a while got some time to work on the laser. First thing I need is to get the exhaust system working. And I'm not sure when my friend will help me drill hole in the concrete wall. So I decided to use my vacuum and its working pretty good. So glad I kept all the attachments and the pipe from the old vacuum we had home. Pretty simple design, removed the stock vent completely and placed the flat attachment in place using 1/8" x8x3 plywood. Now I can cut Acrylic without any problems. 



![images/4ed905d053906dcff6f5d12cd7529249.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4ed905d053906dcff6f5d12cd7529249.jpeg)
![images/acad531baf2ed5b5b2dd1fcb6fda5bc5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/acad531baf2ed5b5b2dd1fcb6fda5bc5.jpeg)
![images/527ef628e6addb90d65057e5d52b6062.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/527ef628e6addb90d65057e5d52b6062.jpeg)
![images/976581d4f07b8e1a6306847ffc5620fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/976581d4f07b8e1a6306847ffc5620fc.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-TtPgW8bpxl0/Vr1LuNuckDI/AAAAAAAAiME/EyIgEwL8lWE/s0/VID_20160211_201929.mp4.gif**
![images/ed1e8e2929b41941fe4a7524ea587fdd.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/ed1e8e2929b41941fe4a7524ea587fdd.gif)

**"ChiRag Chaudhari"**

---
---
**3D Laser** *February 12, 2016 04:01*

How did you get the exhaust piece out


---
**ChiRag Chaudhari** *February 12, 2016 04:22*

**+Corey Budwine** yeah that part is bit PITA. You will need to take entire gantry out.


---
**3D Laser** *February 12, 2016 04:23*

How do I go about getting that out 


---
**ChiRag Chaudhari** *February 12, 2016 04:30*

**+Corey Budwine**  There are four nuts n bolts that you will need to remove in order to take the gantry out. Also you will need to take the black metal cover near Y max position. There are two nuts under there and the other two are on the exact opposite side under the stock exhaust.


---
**ChiRag Chaudhari** *February 12, 2016 04:31*

Will post some pics when I'm at the shop tomorrow if you want. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 12, 2016 05:02*

Pretty nifty idea. I wonder are you just venting the smoke into the vacuum cleaner or is it venting out somewhere else? I'd be slightly concerned of accidentally sucking sparks into the vacuum & causing fire.


---
**Phillip Conroy** *February 12, 2016 05:47*

i would be more concerned about breathing in the fumes ,depending on what you are cutting/engraving .MDF [http://www.hse.gov.uk/woodworking/faq-mdf.htm](http://www.hse.gov.uk/woodworking/faq-mdf.htm)

[http://www.hse.gov.uk/woodworking/faq-mdf.htm](http://www.hse.gov.uk/woodworking/faq-mdf.htm)

so 8if you must use a vac cleaner at least have it outside and run the hose  to the laser


---
**ChiRag Chaudhari** *February 12, 2016 07:28*

**+Yuusuf Sallahuddin** The flat attachment is far enough to not suck sparks and the first pipe from the laser cutter to vacuum is about 4ft long so most likely there should not be no problem. 



**+Phillip Conroy** This the vacuum I am using and it has exhaust on the other end so I am using my old vacuum's 6' long pipe to exhaust everything outside the building.



I have not cut acrylic yet, so once I test that I will know for sure if its working or not. I absolutely hate the smell.



PS: Got Fire Extinguisher handy all the time!


---
**Stephane Buisson** *February 12, 2016 09:46*

ingenious ;-))

but noisy and power angry ;-((


---
**William Klinger** *February 12, 2016 13:54*



what feed and speed are you using to cut the wood? How much pressure do you have on the air nozzle? Thanks. WK


---
**3D Laser** *February 12, 2016 15:05*

I would like to see pictures I want to at least cut mine back 


---
**ChiRag Chaudhari** *February 13, 2016 02:10*

**+Stephane Buisson** definitely noisy but once I buy like 8ft pipe in going to put the vacuum above the ceiling tiles and connect the exhaust to bathroom exhaust. 


---
**ChiRag Chaudhari** *February 13, 2016 02:11*

**+William Klinger** 3mm plywood from Menard's. 

50[feed=50,ppm=40] pulls about 11mA.



Sometimes two passes are required. 


---
**3D Laser** *February 17, 2016 00:01*

**+Chirag Chaudhari** so I got the bed out to remove the exhaust but how did you make sure the bed was square so the mirrors align 


---
**ChiRag Chaudhari** *February 19, 2016 06:58*

**+Corey Budwine** aaaaammmmm I actually never did that part.  :P


---
**Ben Walker** *February 23, 2016 13:43*

I printed an adapter for mine.  cut down the interior part (removed gantry and did realign).  Got a much larger fan and put it at the end of the hose and it is pulling better and allows access to laser door.  At the same time I picked up a bucket shop vac (home depot).  That thing works better than anything I have tried yet at evacuating smoke.  It's a bit loud.  I added a honeycomb bed and it seems that I am producing less controllable smoke with the bed.  I am thinking of using the air input hole in the floor as my exhaust instead.  Anyone tried that?


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/H3jpRRpNbVc) &mdash; content and formatting may not be reliable*
