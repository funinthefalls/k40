---
layout: post
title: "Guys in inkscape is there a tool like the virtual segment delete?"
date: July 17, 2017 03:02
category: "Software"
author: "Chris Hurley"
---
Guys in inkscape is there a tool like the virtual segment delete? 





**"Chris Hurley"**

---
---
**Kyle Kerr** *July 19, 2017 15:53*

Without telling us what you are trying to do your question is open ended. Watching YouTube videos and reading a short tutorial website suggested to me that union would work. Other than that I can't really make any suggestions. For inkscape ideas you might look up Nick Saporito on YouTube. He is a graphics designer, I think, that posts a few videos a week using either inkscape or gimp. Nothing is laser cutting/etching specific, but, I believe his clips might help you make good use of inkscape.


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/Nh4Dn3sQQmk) &mdash; content and formatting may not be reliable*
