---
layout: post
title: "Mads Nielsen on YouTube made a great video on making SMD stencils on he's laser"
date: February 25, 2017 08:22
category: "Object produced with laser"
author: "Morten F\u00f8rster"
---
**+Mads Nielsen**​ on YouTube made a great video on making SMD stencils on he's laser. ​﻿





**"Morten F\u00f8rster"**

---
---
**Don Kleinschnitz Jr.** *February 25, 2017 15:17*

Interesting, I wonder if the "wet paper" technique has any value during normal engraving or cutting. This setup does not seem to have much air assist and evacuation and I wonder if that helps.


---
**Richard Vowles** *February 25, 2017 18:12*

I use my Silhouette Cameo, however I will investigate this as I move into smaller parts.


---
**Ned Hill** *February 25, 2017 19:19*

Yeah **+Don Kleinschnitz** I was also thinking part of his initial problem was the fact that he didn't have air assist. 


---
*Imported from [Google+](https://plus.google.com/+MortenFørster/posts/MVe67FVNm5N) &mdash; content and formatting may not be reliable*
