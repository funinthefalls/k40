---
layout: post
title: "Hello, The size of the engraved object is bigger then it is in coreldraw"
date: May 16, 2017 14:47
category: "Object produced with laser"
author: "Anneke van der Sande"
---
Hello,



The size of the engraved object is bigger then it is in coreldraw.

Example when you draw a picture of size 15 cm x 15 cm. 

De output is 15,2 cm x 15,2 cm.



Is there a setting to solve the size?





**"Anneke van der Sande"**

---
---
**Joe Alexander** *May 16, 2017 14:59*

make sure the DPI is the same between programs, some are default 90 others 96.


---
**Anneke van der Sande** *May 16, 2017 15:20*

Coreldraw has a default setting of 300 DPI and objects also. Everything is a little to big. We have make a test with  a simple square of exact 15 cm .


---
**Joe Alexander** *May 16, 2017 15:27*

what program are you loading the image into? I know laserweb4 its a cinch to change the dpi. for svg's its 96 bitmaps and jpg are 300 by default. Hopefully some of the other fellas on here will chime in and help you as I am not all that familiar with CorelDraw.


---
**Bruno Ferrarese** *May 16, 2017 15:54*

I'm betting its the number of steps/mm.



A K40 needs 157.575 steps to travel 1mm.

Setting it to 160 will make it travel farther. 15cm will give you 363.75 extra steps, which translates to about 2.3mm. 




---
**Cristian Cimpeanu** *May 16, 2017 17:41*

Real scale from CorelDraw to K40 is 98.8 %. This problem appear from duality inch/mm ... in CorelDraw in export>plt have plotter units: 1016. This means  40 inch =1016mm. Some how a floating point error   

lead to this problem.  




---
**Cristian Cimpeanu** *May 16, 2017 17:44*

My solution: CorelDraw->Arange->Transformation->Scale and mirror -> 98.8%




---
*Imported from [Google+](https://plus.google.com/114511093482348580064/posts/6RNGCB9rGuA) &mdash; content and formatting may not be reliable*
