---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) After fixing my alignment (which has taken me a week) I've got around to doing the test project I was working on when I realised alignment was out"
date: April 04, 2017 01:45
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



After fixing my alignment (which has taken me a week) I've got around to doing the test project I was working on when I realised alignment was out.



So it's intended to be a tealight candle holder & the cutouts are intended to bounce shadows in the shape of the cutouts (or the inverse shadows). Not sure if it works yet as I have to wait til the glue dries (my friction fits were out a fraction & I need to adjust a little for kerf on next iteration).



![images/6b5ff47036fb24a37f8f7c0087ddf7b4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b5ff47036fb24a37f8f7c0087ddf7b4.jpeg)
![images/dfe381b844ce2ab52b9b1b26344e5f83.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dfe381b844ce2ab52b9b1b26344e5f83.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ned Hill** *April 04, 2017 02:09*

Nice.  Would love to see pics of them lit when they are ready.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2017 02:19*

**+Ned Hill** Just did a test run in the darkest place I could find. Here's some pics:

![images/590d4f538d2127f32bb425af34a17a29.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/590d4f538d2127f32bb425af34a17a29.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2017 02:20*

![images/a10c78d39172323691fadaa862e83c51.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a10c78d39172323691fadaa862e83c51.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2017 02:20*

![images/d5245407cde19c4d7130500faf646c8b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d5245407cde19c4d7130500faf646c8b.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2017 02:21*

![images/5642dce21f2d9f13d97c819b05d83dad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5642dce21f2d9f13d97c819b05d83dad.jpeg)


---
**Ned Hill** *April 04, 2017 02:21*

Ohh nice!  They work well.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2017 02:23*

**+Ned Hill** Yeah, they work better than I expected. Will be interesting to see when it's fully dark.


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 03:04*

Cool. I just made a bunch of tea light holders, though not designed to cast shadows like that. Really awesome though.


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 03:34*

Foreground shows the tea light candle holders (3.5" cube), the tall ones in the rear have a candelabra sized, LED bulb in them.

![images/17b8bd10ce917b965a3198db374c3055.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/17b8bd10ce917b965a3198db374c3055.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2017 13:19*

**+Ashley M. Kirchner** Are there any concerns with the flame heating the wood up at all? I made mine 90mm cubes to give 25mm clearance on either side of the tealight to hopefully prevent that.


---
**Nigel Conroy** *April 04, 2017 13:20*

Very cool, I've seen the concept used to have shadows thrown from kids night lights.



Those are nice **+Ashley M. Kirchner**

Are you producing to sell?




---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2017 13:35*

**+Nigel Conroy** My inspiration was some artworks I have seen where odd 3D shapes have light cast at them & the shadows show unexpected results (e.g. words or intricate scenes) based on the angle of the light hitting it.


---
**Nigel Conroy** *April 04, 2017 13:37*

**+Yuusuf Sallahuddin** yes I like the effect. Think I need something a little more friendly for my 3+5 year old... Even though they would love the dragon and castle at night might be a bit different!!!



I was thinking Peter Pan or something, which is what I think I saw previously


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2017 13:48*

**+Nigel Conroy** With leds powering it safety would be less of a concern & the shadow effect could be increased due to the brightness of the leds. Also, could possibly have frosted acrylic pieces internally to disperse the light more evenly through the cutouts.


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 17:09*

**+Yuusuf Sallahuddin**​, they are 90mm (about 3.5 inches). The first one I made was a bit smaller and I burned several tea light candles in it with no issues, however it was hard to get the candle in while lit, or to light it while it's already inside. So I decided to make them bigger. I need to find a cheap source for 2" circle metal blanks now. Either brass or aluminum, to put in the bottom just as a precaution so the wood doesn't get too hot or discolor. It hasn't happen during my testing, but I'd rather be safe. 


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 17:09*

**+Nigel Conroy**​, that's the plan, selling them. 


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 17:12*

**+Yuusuf Sallahuddin**​, speaking of LEDs, I'm going to design and build a LED insert for the taller lamps too. Right now they have a single candelabra bulb in them, but I want to try and also make them colorful. I already have the schematic figured out, code written, I need to prototype it and if it works as I expect, I'll send the design for manufacturing. 


---
**Nigel Conroy** *April 04, 2017 19:48*

**+Yuusuf Sallahuddin** I meant from a scaring them point of view not fire hazard.



Spooky dragon shadow on the wall at night just as they go to bed..... might equal bad dream....


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 19:51*

Ah, that just makes them prepared for what the world has to offer, I mean ... it's not like a certain person in office right now doesn't cause bad dreams. :)


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 00:41*

**+Yuusuf Sallahuddin**, as I mentioned earlier, LEDs! Had to dig up a few items that have been buried for a few years, but the first test is up and running ... Now I need to continue wiring up buttons and see what I come up with. Wheee.

![images/b161a96a68f9c0f776e68606cbf346d6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b161a96a68f9c0f776e68606cbf346d6.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 04:45*

**+Nigel Conroy** I registered after my reply that it wasn't safety that was the concern but scary night time shadows haha.



**+Ashley M. Kirchner** Wow, that's absolutely great.



On a side note, I did a test last night in total darkness with the tealight candle & the shadow spread was still only about 1m away that you could see any clear definition. I then tested the led torch from my phone & the spread was about 3-4m & super clear details, so I think it could even go further.


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 05:18*

Further in scaring kids? :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 05:34*

**+Ashley M. Kirchner** Haha, I suppose it would depend on the cutout. But even a giant Mickey Mouse shadow could be scary I suppose.


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 05:48*

True, and you could also get in trouble for using that. Or you can come up with seemingly innocent shapes which when light casts a shadow, turn into something highly inappropriate. :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 12:50*

**+Ashley M. Kirchner** Just had another look at the tealights & candelabra ones you showed again & realised your design is better as they all have some feet a the bottom. I might have to mod my design to incorporate that sort of thing as it looks a touch better than just sitting flat on the surface.


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 19:13*

Yeah, I do that for two reasons: a) to keep the hot bottom off of whatever it's sitting on, and b) for airflow. The bottom of the holders have a grill cut in them that the tea light would sit on. The hot air from the candle can then escape from the top while cold air comes in from the bottom. Because all my designs vary in how much air can flow through the sides (some have large holes, others have tiny ones), having the bottom open allows for airflow regardless. On my template I have the grill setup as a cut, the logo as a pathfill and the warning message also as a cut, but at 10% high speed. That results in very sharp text versus engraving it.

![images/a0a644014595595445bad61122f6b110.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a0a644014595595445bad61122f6b110.png)


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 19:15*

I still can't get raster engraving to work though. I'm still getting a darker burn mark left and right of the image, and if I set the overscan, it creates two thin lines on either side of the image, equal to the distance of the overscan. It's like the laser gets to the end of its path, fires, then reverses direction. Not sure why. Doesn't matter what overscan size I use. So yeah, for not, raster scanning is useless for me till I can figure out what's going on. A shame really.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 19:35*

**+Ashley M. Kirchner** That's a good design to have the airflow in the bottom & have warnings path-filled onto it.



I'm assuming you are running Smoothie or C3D on LaserWeb, so I've a thought regarding your burns on the left & right even with overscan on. I'm wondering if you've got a minimum power or something set in the config. I haven't played with raster for a while, so can't be sure mine's not doing the same thing either. Will have to give it a test on a raster sometime soon & see if I end up with the same issue.


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 19:43*

It looks like the binaries have been updated again, so I'm going to update and do another test to see what happens. And yes, LW on C3D Mini. I posted my config not too long ago, nothing's changed in it. The minimum is set to 0.1 (max 0.8). But even at 0.0, it still does it.


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 20:15*

And only my logo is pathfilled. The warning text is set as a cut operation at low power and higher speed.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 20:44*

**+Ashley M. Kirchner** I will see if I can find your config in old posts & have a look if there is anything that may assist the left & right burn issue. I'm pretty happy with the path fill option, but raster would be useful at times too (for gradients). Low power cuts can make great embellishments at time too (like your warning texts).


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 20:52*

[https://pastebin.com/tTBZyCfX](https://pastebin.com/tTBZyCfX)


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 20:56*

And no you can't see anything of mine in the LW group because I got kicked out "for attacking Peter personally". <b>shrug</b> If I lived my professional life like that (or even personal), I wouldn't be where I am today. Having been in the IT world for over 30 years I've had my fair share of personal and professional attacks.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 21:03*

**+Ashley M. Kirchner** Only thing I notice different in your config to mine is line 21 of mine has this:



acceleration_ticks_per_second                1000             # Number of times per second the speed is updated



Also, I have the min power set to 0.0 & max power set to 1.0 in my config. Everything else seems to be of no significant difference.


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 21:09*

Originally the config was at 0.0 to 0.8, and I wasn't getting a good result at low power, so per Ray's suggestion, I changed it to 0.1 and it works much better at low power now. And the acceleration on mine is set to 3000, again not something I changed, it came that way.


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 21:10*

I should also point out that for whatever reason, my machine appears to be somewhat of a unique beast. Perhaps it's because it's almost two years old now and some things are different compared to what's coming out now, dunno.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 21:12*

**+Ashley M. Kirchner** Mine is from back around September 2015, so about 1.5yrs old. Again, I haven't tested rastering for a while so I will give it a test when I hit the laser next to see if I get the same dark left/rights that you are getting. I will try the overscan also to see if I get it happening then too. After I get around to it I will let you know my results.


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 22:53*

Under 6K (compiled), about 99% of code space on the chip. Don't think I'll be adding much more. Two (2) buttons for control, one button does On-Off/Mode and the other brightness. At the moment 4 LEDs but I'm going to bump that up in the final design to either 5 or 6 (or find a single, low watt LED, although then I have to deal with cooling it.)

![images/821f85def445933d1f602a64866d2039.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/821f85def445933d1f602a64866d2039.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2017 23:15*

**+Ashley M. Kirchner** That's really cool :) Looks like something I'm going to have to learn how to do. Any pointers on a to do list for learning how to make led modules like that? Assume I have 0 knowledge of these things (except basic soldering & even more basic circuit knowledge, e.g. battery->switch->bulb).


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 23:57*

Errr, that makes it tricky. You need to have an understanding of how the microcontroller works, how to wire it up, how to program it., etc., etc. It's not necessarily difficult, but knowledge in C++ is definitely a plus. At the moment, it's just a prototype sitting on my desk. Eventually I'll be making a custom PCB to fit inside the lamps. The 4x3 grid of LEDs you see is something I made a long time ago and decided to just use that for this test. Only the 4 in the lower left are used for this test. The tiny little black square in its upper right corner is the actual chip.

![images/d0eb142cbd4e09bf5f5ca811f6b551ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d0eb142cbd4e09bf5f5ca811f6b551ed.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2017 00:50*

**+Ashley M. Kirchner** Thanks for the info. I have a small amount of experience with C++ from years back, so I could probably refresh enough to figure out what to do on that end. I guess I'm going to have to do a fair bit of reading to figure these things out, as I'd love to start utilising some different electronic components in some builds/projects in future. Just out of curiosity at this stage, do you program the chips by plugging them into some adaptor that then plugs into the computer & then load them up with a code module somehow? Or is it much more complicated than that?


---
**Ashley M. Kirchner [Norym]** *April 06, 2017 00:58*

They are AVR microcontrollers. You need a programmer that connects through USB to you computer and the "In-Circuit Serial Programming" pins on the microcontroller (hence needing to know how they work and what their pins do). Then you can use different programs to write and program. I use Atmel Studio 7 to write code, compile, and write to the chip.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2017 01:09*

**+Ashley M. Kirchner** Thanks again. I'll start my research there & see if I can figure it out (or not).


---
**Ashley M. Kirchner [Norym]** *April 06, 2017 01:32*

Actually, start with an Arduino - the simple Uno will do just fine. That's the base for these AVRs. Once you understand how to program them and how they work, you can start getting more technical with them.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2017 08:04*

**+Ashley M. Kirchner** Ah cool. I've been meaning to learn how to use an Arduino for a while, so now's a perfect excuse to have a go.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/ByifiPkoR4y) &mdash; content and formatting may not be reliable*
