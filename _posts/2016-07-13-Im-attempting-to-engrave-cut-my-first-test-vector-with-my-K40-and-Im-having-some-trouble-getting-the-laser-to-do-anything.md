---
layout: post
title: "I'm attempting to engrave/cut my first test vector with my K40 and I'm having some trouble getting the laser to do anything"
date: July 13, 2016 05:27
category: "Original software and hardware issues"
author: "Jack Sivak"
---
I'm attempting to engrave/cut my first test vector with my K40 and I'm having some trouble getting the laser to do anything. I'm initially just trying to get the laser head to move without firing the laser, so the "Laser Switch" has not engaged for any of my attempts. I'm attempting to cut via LaserDRW rather than through the CorelLaser plugin for CorelDraw. I think it's an issue installing the driver, which ("i'm assuming) is a "setup.exe" file in a "Laser engraving drive" folder on the disk that came with my K40.



The first image is the main screen of that file, and the next three images are the results of pushing each of the three buttons. It has two gibberish .sys files in the same folder, along with a partial-english .inf file, which seem to be the files it's referencing in that helpful popup. As you can see, the driver apparently fails to install. I've tried it with and without the laser as well as USB-key-dongle-thing in, but nothing seems to change.



When I attempt to engrave (Press "Starting" from the LaserDRW Engrave screen) there is a pause for probably half a second, then the window closes and nothing happens. I'm assuming this means that the program can't find or talk to the laser, perhaps due to the driver install issue.



Does anyone have advice on how to tackle this problem? Is there a different way to install drivers for the K40?



![images/3e4139c96c620bc5112842c212910884.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3e4139c96c620bc5112842c212910884.png)
![images/55ff8baec7d99588da993a969e264484.png](https://gitlab.com/funinthefalls/k40/raw/master/images/55ff8baec7d99588da993a969e264484.png)
![images/f4b6146b404db840e5c8c0b262a18efa.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f4b6146b404db840e5c8c0b262a18efa.png)
![images/c75d3265e968f2b4243d3cfa1c2cb938.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c75d3265e968f2b4243d3cfa1c2cb938.png)
![images/2b344e783b3b4185ad992879e4ed4213.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2b344e783b3b4185ad992879e4ed4213.png)

**"Jack Sivak"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 13, 2016 06:51*

If you can find the .inf file then you can try right clicking it (in Windows) & clicking Install. Sometimes you can do that with .inf driver files.


---
**Jack Sivak** *July 13, 2016 07:49*

**+Yuusuf Sallahuddin** It just tells me "The INF file you selected does not support this method of installation"



I'm using Windows 7, if it matters. Does anyone know of a log file that might have information on why it fails?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 13, 2016 08:51*

Not sure what else to suggest. I never really used LaserDRW as it didn't seem to be the way to go. I mostly used CorelDraw/Laser combo. I don't recall having to install any drivers/inf files either (although I may have).


---
**Jack Sivak** *July 13, 2016 16:49*

**+Yuusuf Sallahuddin** I'm fine using CorelDraw/Laser, but I couldn't find how to actually send a message to the machine telling it to engrave. Is there a video somewhere that explains how to use the CorelLaser plugin?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 13, 2016 21:41*

Ah, in the top right hand corner there is a small toolbar with a bunch of icons on it. Alternatively there is an icon in the System Tray (near the clock) in the bottom right corner. The toolbar likes to disappear (after the first job) so you may have to search for the icon near the clock. Then you choose Engrave/Cut (depending on what you want) & set your parameters (speed, X-refer/Y-refer, passes, etc). I don't believe there is a tutorial anywhere, but if you're still having trouble later on I can do some stuff up when I get home to show you.


---
*Imported from [Google+](https://plus.google.com/106960809851059243398/posts/3pcqh9LaHXT) &mdash; content and formatting may not be reliable*
