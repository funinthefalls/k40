---
layout: post
title: "Peter van der Walt Here are the pics of the redsail LE400"
date: April 01, 2016 16:39
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
**+Peter van der Walt** Here are the pics of the redsail LE400



![images/c123835e739609fd30fa73768a2c69b5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c123835e739609fd30fa73768a2c69b5.jpeg)
![images/ae7b7f45ab9491788b649a3ab210c6dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae7b7f45ab9491788b649a3ab210c6dc.jpeg)
![images/0fad43fcf36bab86815b8e441518528f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0fad43fcf36bab86815b8e441518528f.jpeg)
![images/8e5273ecd578347560d7f7bfeb5365a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e5273ecd578347560d7f7bfeb5365a8.jpeg)
![images/67aff35cd4bae3e886516c1167e5a886.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/67aff35cd4bae3e886516c1167e5a886.jpeg)
![images/465b3b9829203fd3e122d34b0034a0cc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/465b3b9829203fd3e122d34b0034a0cc.jpeg)
![images/c874424c2c65dd9585065bb5a49c45d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c874424c2c65dd9585065bb5a49c45d4.jpeg)
![images/f3c7bcb75e2e58244d1086a707303b08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3c7bcb75e2e58244d1086a707303b08.jpeg)
![images/54ac616a9d0566965426760ea4c2a44b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/54ac616a9d0566965426760ea4c2a44b.jpeg)
![images/aa1d03414594348b7bfc93dd75638a1c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa1d03414594348b7bfc93dd75638a1c.jpeg)
![images/b7e04a10387c79e50fcebb58e6956891.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7e04a10387c79e50fcebb58e6956891.jpeg)
![images/6528498d752d12f2ed13e820ac4ff22a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6528498d752d12f2ed13e820ac4ff22a.jpeg)
![images/da5f1f0e66ab2f2b44c92dc0c2da8928.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/da5f1f0e66ab2f2b44c92dc0c2da8928.jpeg)

**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *April 01, 2016 16:41*

I think I will just gut it and put a new PSU and Smoothieboard controller in this one.


---
**Anthony Bolgar** *April 01, 2016 16:43*

[easycut.cn](http://easycut.cn) takes me to a CHinese version of the Redsail site.


---
**Anthony Bolgar** *April 01, 2016 16:45*

Lots of room in this one for a 50-60W tube. And the bed is adjustable up and down 70mm. It works using the control pad, so I know the steppers are OK, as well as ther tube. Weird thing is the Amp meter, says it pulls 1.5 A at full power, never seen a tube pull this much juice.


---
**Anthony Bolgar** *April 01, 2016 16:47*

I just saw that post.


---
**Anthony Bolgar** *April 01, 2016 16:48*

It looks like it uses a serial port for communication to the board, will have to dig out an old PC for this.


---
**Anthony Bolgar** *April 01, 2016 16:50*

No dongle. So it looks like I will just mod the crap out of this one and upgrade it to a true 50W tube.


---
**Jon Bruno** *April 01, 2016 19:34*

You could try a hasp or hardlock emulator before you tear it down.. its always fun to try and circumvent security methods.

hmm, I guess you'd need a legit dump first though...


---
**Anthony Bolgar** *April 01, 2016 19:52*

Decided I will just gut it, replace the PSU (notice the open to the air HV section of the psu in the pics) and put a smoothieboard in it. I found the original manual and it has all the pinouts, so should be a pretty easy upgrade.


---
**Jon Bruno** *April 01, 2016 19:53*

Cool! Good luck.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/SBaZU7vsaMk) &mdash; content and formatting may not be reliable*
