---
layout: post
title: "Working on an edge lit acrylic sign for my better half..."
date: June 01, 2016 05:43
category: "Materials and settings"
author: "Alex Krause"
---
Working on an edge lit acrylic sign for my better half... quick question does acrylic always engrave clear? I was hoping for a frosted look. Any suggestions on where to start with power levels and engrave speed to achieve that effect would be awesome ( **+Brandon Satterfield**​ another fun use for your K40)

![images/887573ff378afc3f9817a45d4c2ed513.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/887573ff378afc3f9817a45d4c2ed513.jpeg)



**"Alex Krause"**

---
---
**Scott Marshall** *June 01, 2016 07:37*

Back off on the power and speed up.



The trick is to have enough speed that the plastic only vaporizes, but never absorbs heat and melts.



You normally do get a frosted look, but if you get the acrylic hot, it re-fuses and melts away the frosted surface. It should look pretty much like sandblasted glass.



Start around 2-3ma and 150-200mm/sec. Focus ON the surface as sharp as you can.


---
**Stephane Buisson** *June 01, 2016 08:34*

good to know, (moved to Material and setting category)


---
**Brandon Satterfield** *June 01, 2016 10:55*

**+Alex Krause** awesome sir, I still haven't finished the cutting board, or the V-bit sine art, or... 

Have 3 products under development.. Simply not enough time in a day. 


---
**Lance Robaldo** *June 01, 2016 12:21*

Also cast acrylic will work better with a laser and give you the "frosted" look.  Extruded acrylic tends to melt.  It doesn't cut or engrave as well as cast acrylic does..


---
**Gunnar Stefansson** *June 01, 2016 12:44*

**+Alex Krause** my laser cutter can't do raster engraving, so I usually use about 2-3mA Power and use normal engraving at speed 1500mm/min... that always ends up very frosted looking.




---
**Stephen Smith** *June 01, 2016 16:28*

Always use cast acrylic for engraving. If you want some excitement try lasering polycarbonate it will engrave as either blue, yellow, red, and possibly other colours. You never know which colour you are going to get until you start.


---
**John-Paul Hopman** *June 01, 2016 18:32*

**+Stephen Smith** Doesn't that also release a toxic gas?


---
**cory brown** *June 01, 2016 22:56*

polycarbonate isn't the same as PVC (Polyvinyl chloride). Polycarbonate is hard for the laser to cut and easy to catch fire, distort, or melt... I don't think it's any more toxic then acrylic.


---
**John-Paul Hopman** *June 01, 2016 22:59*

**+cory brown** Ah. Okay, thanks for the clarification.


---
**Stephen Smith** *June 05, 2016 09:22*

I have always found PC to be very easy to engrave but do be wary of the fumes they are most unpleasant.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/TH1idcxQmGt) &mdash; content and formatting may not be reliable*
