---
layout: post
title: "While checking my design today I got to thinking about the optical end stop interface and configuration and am unsure if I understand it clearly"
date: October 02, 2016 22:17
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
While checking my design today I got to thinking about the optical end stop interface and configuration and am unsure if I understand it clearly.

See the summary circuit for reference below.



The output of the K40 optical end stop card will be:

... High when at the end stop

... Low when not at end stop

<s>-----------</s>

This excerpt from the guide suggest that a a switch works the same way as above.

"You want to connect the Signal ( green in the schematic ) and Ground ( blue in the schematic ) pins for the end-stop on the Smoothieboard, to the C and NC connection points on the end-stop."



This is an X endstop setting from the configuration docs:

"alpha_min_endstop	1.24^ "



This configures the 1.24 port with a pull-up. 

<s>------------</s>

My questions: 

1. What does the firmware look for as an end stop arrival on P1.24; LOW or HIGH true. I assume its high true?

2. Is the Pull-up that the configuration sets with "^" one that is in the processor chip?

3. Is the configuration = "apha_min_endstop 1.24^ " correct for the equivalent circuit shown below?

4. I need to connect the SJ2 jumper to 5V- R7 right? I need 5v to the end stop card.

5. Is R13, C1 simply an input filter?





![images/b70c709841cfac2f7bfc9e11fb9f0712.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b70c709841cfac2f7bfc9e11fb9f0712.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 03, 2016 01:59*

I have no idea, but I'm interested to see the response :)


---
**Arthur Wolf** *October 06, 2016 13:01*

1. Whether or not it looks for low or high true depends on the whether or not you add the "!" character. Just try it and you'll find the right way around ( with the M119 command )

2. Yes, that's within the chip. 

3. I'm bad at reading circuits, I think it is, but you can just try it to know if it is.

4. They already output 5V by default

5. I think so yes.


---
**Don Kleinschnitz Jr.** *October 06, 2016 13:49*

**+Arthur Wolf** thanks I will update this when I am finished testing. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/7Fi87K8Vw6a) &mdash; content and formatting may not be reliable*
