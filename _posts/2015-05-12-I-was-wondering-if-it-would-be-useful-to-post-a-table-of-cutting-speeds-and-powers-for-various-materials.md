---
layout: post
title: "I was wondering if it would be useful to post a table of cutting speeds and powers for various materials"
date: May 12, 2015 20:27
category: "Materials and settings"
author: "Chris M"
---
I was wondering if it would be useful to post a table of cutting speeds and powers for various materials. Something that some person could be responsible for, but containing results from the community. Can such a thing be tied to the top of the forum? Would it be useful?





**"Chris M"**

---
---
**David Richards (djrm)** *May 12, 2015 20:47*

Greetings Chris, It is always useful to be able to refer to what settings have been used sucessfully on different materials however the settings used have many variables on any machine,  optical path quality and laser power calibration for instance. Kind regards, David


---
**Joey Fitzpatrick** *May 12, 2015 20:49*

I think that is a great Idea!  For me, It has been trial and error. Usually resulting in a pile of ruined pieces until I get the settings right.


---
**Chris M** *May 12, 2015 20:58*

David, Joey - my intention is a table of settings that have worked for someone, so a starting point for new users. I think the tricky bit is pinning it to the top of the forum.


---
**Jim Coogan** *May 12, 2015 21:09*

I keep a running log of everything I do on my machine.  I have a box I call my burn box that has all types of materials and different sizes for the purpose of experimenting.  Last night I needed to engrave an Aztec calendar in MDF so I just looked up the last good settings I had and used them.  Worked great.  But something like you are suggesting has to be prefaced with a statement that all machines behave a little different.  Working with a laser is really about trial and error so you get to know your machine.  Now I can look at something and I already know what power and speed setting I will use and which lens works better for what I am doing.  But a list would be a great place for others to start.  It would get them in the ballpark.  Not sure how to pin it here though.  On the Facebook page we have a files area we put things like that in.  I do have a file called laser settings that I found recently.


---
**Jim Coogan** *May 12, 2015 21:12*

I just shared the file called laser settings.  Wonder if we can use something like Google Docs to create a file that can be shared with everyone in this group.


---
*Imported from [Google+](https://plus.google.com/108727039520029477019/posts/MBvbFuHLzuk) &mdash; content and formatting may not be reliable*
