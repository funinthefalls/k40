---
layout: post
title: "I need your advice guys, about fabric material any minimum thickness laser can engraving?"
date: February 11, 2017 14:37
category: "Materials and settings"
author: "adam ian"
---
I need your advice guys, about fabric material any minimum thickness laser can engraving? Before i built one.





**"adam ian"**

---
---
**Ned Hill** *February 11, 2017 15:02*

Well you have to keep in mind that when you are engraving fabric with a laser you are actually burning the fibers.  So it just depends on how dark the engrave needs to be.  If your burn the fibers too much the material will probably fall apart.  


---
**adam ian** *February 11, 2017 15:35*

Ned Hill , thanks your explanation about burning the fabric


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 11, 2017 18:15*

I've played around engraving on denim & some denim it works well to leave a white engrave, but other denims I used just turned to a pile of dust (at the same settings). So you definitely want to use super lower power & fast engrave speeds.


---
**Jonathan Davis (Leo Lion)** *February 12, 2017 00:55*

Interesting thought to consider though I would just use the laser to cut the fabric versus engraving on to it. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 12, 2017 00:59*

**+Jonathan Davis** It works well for cutting small fabric pieces (that fit in your work area). Although you have to watch with certain fabrics (e.g. nylon/polyester) as they just want to melt/burn. I'd imagine without air-assist going it could be problematic. As far as I'm aware, lower powered diode lasers work better for engraving onto fabrics like denim (bleaches the colour out of the section engraved). The CO2 laser seems maybe a bit overpowered for this purpose. Maybe someone with a diode laser can give it a test & see how it goes.


---
**Jonathan Davis (Leo Lion)** *February 12, 2017 01:01*

**+Yuusuf Sallahuddin** I do have a diode laser via a chinese made system form Gearbest. Though i have still yet to figure out how to install the endstops.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 12, 2017 01:01*

**+Jonathan Davis** Cool, well if you get around to testing on denim/fabric I'd love to see results.


---
**Jonathan Davis (Leo Lion)** *February 12, 2017 01:03*

**+Yuusuf Sallahuddin** I will definitely work on seeing what happens and the results posted as a video and blog post.


---
**adam ian** *February 12, 2017 01:47*

**+Jonathan Davis**​

**+Yuusuf Sallahuddin**​

I'm too happy to see the result if you test, just some word with square border around.


---
**Jonathan Davis (Leo Lion)** *February 12, 2017 01:49*

**+adam ian** ok and if it were to be done I would use cheep material found on a clearance rack.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 12, 2017 03:59*

**+adam ian** I don't remember exactly, but I feel like I posted some test on denim about a year ago. I will see if I can find it in my old posts.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 12, 2017 04:15*

**+adam ian** I found my posts of it. There is 2 where I shared my attempts lasering denim 48 weeks ago... So, almost a year back.



See here:



[https://plus.google.com/u/0/+YuusufSallahuddinYSCreations/posts/8AHdPNZdWsr](https://plus.google.com/u/0/+YuusufSallahuddinYSCreations/posts/8AHdPNZdWsr)



[https://plus.google.com/u/0/+YuusufSallahuddinYSCreations/posts/DPzfJvVT7xJ](https://plus.google.com/u/0/+YuusufSallahuddinYSCreations/posts/DPzfJvVT7xJ)


---
*Imported from [Google+](https://plus.google.com/118051184495908064866/posts/UJnbH9uPTpf) &mdash; content and formatting may not be reliable*
