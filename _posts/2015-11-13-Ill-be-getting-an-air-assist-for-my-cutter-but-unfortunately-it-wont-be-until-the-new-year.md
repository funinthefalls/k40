---
layout: post
title: "I'll be getting an air assist for my cutter, but unfortunately it won't be until the new year"
date: November 13, 2015 01:35
category: "Discussion"
author: "Anthony Coafield"
---
I'll be getting an air assist for my cutter, but unfortunately it won't be until the new year. I have a compressor and air nozzles etc, can I use it to blow the wood as I cut until the air assist comes? I'd need glasses etc since I'll have to have the top open but am wondering if it's a bad idea for other reasons also, and if anyone has any tips. Thank you.





**"Anthony Coafield"**

---
---
**Coherent** *November 13, 2015 12:33*

Make sure your air pressure is low enough so you don't accidentally move your material. Even duct taping the end of a air line, tube (or whatever you were planning on holding) to your laser head would be better than trying to follow the head around. Direct consistent air flow is better.


---
**Anthony Bolgar** *November 13, 2015 13:02*

Also, any pair of plastic safety glasses used in the construction industry is fine to protect your eyes from the laser. No need to buy wave length specific safety glasses. If you have access to a 3d printer, there are plenty of air assist nozzles on Thingiverse, I am currently using one of them, and it works great. If you don't have access to a 3d printer, I could send you one if you cover the shipping costs.


---
**Anthony Coafield** *November 13, 2015 21:58*

Thanks so much guys. I'll tape it to the end for now **+Marc G**, that's a great idea.



**+Anthony Bolgar** I don't have access to a 3d Printer (decided on this first, hopefully I'll get everyone to chip in for one of those next birthday!). Thanks for that generous offer. I live all the way over in Australia, but if you don't mind that extra postage inconvenience I'd be thrilled to cover the shipping etc.


---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/MnXvC5gdSP1) &mdash; content and formatting may not be reliable*
