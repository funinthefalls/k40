---
layout: post
title: "Any databases out there for laser cut libraries"
date: May 12, 2018 23:04
category: "Discussion"
author: "Buhda \u201cPete\u201d Punk"
---
Any databases out there for laser cut libraries. Instead of spending days with F360 in designing tabletop workbench cabinets. I am looking for plans for Hobby bench organizers. A database like Thingyverse specifically for laser cut items?





**"Buhda \u201cPete\u201d Punk"**

---
---
**James Rivera** *May 12, 2018 23:39*

Thingiverse already has laser cut projects. I’m not sure, but YouMagine might, too.


---
**HalfNormal** *May 12, 2018 23:43*

Actually Thingiverse has a cornucopia of free templates. Just search laser or laser cutter. Do a search of this site for SVG, template ect. Also check out instructables. All the laser makers have templates on their sites too.



[thingiverse.com - Things tagged with 'Lasercut' - Thingiverse](https://www.thingiverse.com/tag:lasercut)



[https://www.everythinglasers.com/forums/forum/places-to-find-things-to-make/](https://www.everythinglasers.com/forums/forum/places-to-find-things-to-make/)






---
**Buhda “Pete” Punk** *May 13, 2018 00:11*

Thanks will take a look.




---
*Imported from [Google+](https://plus.google.com/+BuhdaPunk/posts/73SRhKL76mG) &mdash; content and formatting may not be reliable*
