---
layout: post
title: "Hi all, is there anyone who can advise please?"
date: February 17, 2018 18:22
category: "Original software and hardware issues"
author: "Peter Alfoldi"
---
Hi all, is there anyone who can advise please? I've just bought a K40 laser and I'm quite disappointed by the quality. It seems I have a shake on Y axis that I can't solve. It was even worse than on picture but got improved when I tightened the belts. This is at 30mm/s

What else can I do?

![images/30210766457ae78ce55a28e325a683f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/30210766457ae78ce55a28e325a683f8.jpeg)



**"Peter Alfoldi"**

---
---
**BEN 3D** *February 17, 2018 18:26*

try 40k whisperer together with inkscape instead of the chinese original software


---
**Peter Alfoldi** *February 17, 2018 18:30*

Thank you - I'll give that a shot


---
**BEN 3D** *February 17, 2018 18:33*

Take look to my collection



[plus.google.com - How To Laser Cut](https://plus.google.com/collection/4eviTE)


---
**Peter Alfoldi** *February 18, 2018 21:51*

So, if it helps anyone, my issue is caused by extensive slop in Y axis linear ball bearing. I have designed a support which basically presses against the guiding rail/rod creating a preload and removing the slop. This solved the wobble, however, the letters were still misalligned, so I was investigating further and I noticed that also one of my GT2 drive gears on Y axis is loose - unfortunately there are no grub screws :( How could they sell such crap? I suppose I can only upgrade to grub screw gears now....


---
**Duncan Caine** *April 04, 2018 13:59*

**+Peter Alfoldi** I've got the same bearing slop on mine.  Do you have a picture of your Y axis support so I can do the same on mine?


---
**Peter Alfoldi** *April 04, 2018 15:39*

Duncan Caine This is how it looks

![images/0a044edd08c872c49712116f5e1fc01e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a044edd08c872c49712116f5e1fc01e.jpeg)


---
**Duncan Caine** *April 04, 2018 17:36*

**+Peter Alfoldi** Thanks Peter




---
*Imported from [Google+](https://plus.google.com/105128850753393988085/posts/GHiz3NJJa8d) &mdash; content and formatting may not be reliable*
