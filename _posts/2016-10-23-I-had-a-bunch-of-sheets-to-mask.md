---
layout: post
title: "I had a bunch of sheets to mask..."
date: October 23, 2016 01:32
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
I had a bunch of sheets to mask... Wasn't about to do it by hand. One pass, cut, flip, another pass, DONE! 

![images/721e34554a5ca0b1e0ad764bfbcc2050.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/721e34554a5ca0b1e0ad764bfbcc2050.jpeg)



**"Ashley M. Kirchner [Norym]"**

---
---
**Anthony Bolgar** *October 23, 2016 01:41*

Nicely done. I have a similar setup, but smaller for applying transfer tape to vinyl graphics.


---
**Ashley M. Kirchner [Norym]** *October 23, 2016 02:01*

That's what that is for. I just happen to use it for masking the wood as well. 


---
**Tony Schelts** *October 23, 2016 08:26*

Im going sound really thick.  Is the masking for do laser engraving without mess.  And is it cost effective.  What do you use and what would you use if this was just a hobby


---
**Ashley M. Kirchner [Norym]** *October 23, 2016 15:10*

It's to prevent burn marks, either during cutting or engraving. Otherwise you end up sanding it off which takes longer. This is RTape, specifically the Conform series. It comes in low, medium, and high tack. I use the low and medium one for masking and when done, I lift it up with a high tack alternative such as duct tape. Some of my pieces are highly detailed so removing all the little pieces one by one would take forever, so I just use a high tack tape to lift the low tack one. 

![images/5ee60b2ef9a966a6bfe1a54cd1a91ab7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5ee60b2ef9a966a6bfe1a54cd1a91ab7.jpeg)


---
**HalfNormal** *October 23, 2016 16:05*

Use high tack to remove low tack, What a simple and great idea!


---
**Tony Schelts** *October 24, 2016 08:28*

Asley What type of laser are you using? that look really detailed what are they






---
**Ashley M. Kirchner [Norym]** *October 24, 2016 14:10*

A stock Chinese K40, blue and white.


---
**Tony Schelts** *October 26, 2016 06:22*

what do you make


---
**Ashley M. Kirchner [Norym]** *October 26, 2016 06:47*

Just about anything my brain comes up with, so long as I can draw it and fit it all together virtually first.


---
**Tony Schelts** *October 26, 2016 07:19*

Same here. still early days.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/7T4EZEQ2nuL) &mdash; content and formatting may not be reliable*
