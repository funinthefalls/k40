---
layout: post
title: "Do I have to use the software that comes with the machine or can I use something else?"
date: December 11, 2015 00:23
category: "Discussion"
author: "Scott Howard"
---
Do I have to use the software that comes with the machine or can I use something else? K40





**"Scott Howard"**

---
---
**Stephane Buisson** *December 11, 2015 00:44*

the electronic board coming with the K40 is "proprietary". mean you have to use the join software.

Going with open source hardware (Smoothie or Arduino kind) allow the use of  open source software like Visicut.


---
**Scott Howard** *December 11, 2015 00:44*

What would you do? 


---
**I Laser** *December 11, 2015 00:51*

If you haven't already purchased, try and get one with a CorelLaser/Laserdraw board not Moshi.

Give it a go, if you find it limiting look at upgrading. I've been using coreldraw for the last 6 months and am mostly content with it.


---
**Stephane Buisson** *December 11, 2015 00:52*

It's done, I went for Smoothie+Visicut.

Buy the cheapest including Moshi, and get rid of the electronic.


---
**Scott Howard** *December 11, 2015 01:21*

Thank you all


---
**ChiRag Chaudhari** *December 11, 2015 02:26*

I got my K40 about 2 weeks ago. Had already ordered Arduino with Ramps 1.4 and LCD. With little bit of time I get playing with the machine, its almost close to running perfect. 

Almost everyone hated the original Software, so I never even tried that. Upgrading to Ramps is actually fairly simple, and you will get answers pretty fast in here. 

Using Inkscape to design and Turnkey plugin to generate is really easy. So Ramps is definitely my fav. 


---
**Scott Howard** *December 11, 2015 02:53*

What is ramps and where do I get it


---
**ChiRag Chaudhari** *December 11, 2015 03:10*

**+Scott Howard** Just like the Green Control board (Moshi) you have in your K40 Ramps 1.4 is Control Board. Its designed as a shield of Arduino Mega 2560, and there also a LCD control you can hook it up with it. I purchased mine form here: [http://www.ebay.com/itm/171970352643](http://www.ebay.com/itm/171970352643) but looks like seller is out but you can fine similar item on eBay around $40.



Ramps was originally designed to control 3D printer, but its original Marlin firmware was modified so that you can control your laser cutter or CNC machine. 



You install the modified firmware on Arduino, and arduino controls the laser machine using Ramps board. If you compare it with stock K40 machine, Arduino plays role of a computer and Ramps is like the green board. Using the LCD panel you can control laser without need of a PC and you can also cut/engrave using just a g-code file via SD card slot. 



Here are a few links I followed to complete my Ramps upgrade.

1. [http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html](http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html)

2. [http://weistekengineering.com/?p=2406](http://weistekengineering.com/?p=2406)

3. [http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/256422-cnc-3.html](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/256422-cnc-3.html)



If you get stuck or dont understand anything just post it here. Someone will get you back on track in no time!


---
**ChiRag Chaudhari** *December 11, 2015 03:13*

Also here is some more info on the software side, which is already included in those build logs but just for access.



1. Modified Marlin Firmware by Turnkey: [https://github.com/TurnkeyTyranny/buildlog-lasercutter-marlin](https://github.com/TurnkeyTyranny/buildlog-lasercutter-marlin)

2. Inkscape Plugin to generate GCode by Turnkey: [https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin](https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin)


---
**I Laser** *December 11, 2015 07:16*

So you basically end up with a stand alone engraver/cutter? Didn't realise the upgrade replaced the computer side. Has the arduino got ethernet or is capable of being networked?


---
**ChiRag Chaudhari** *December 11, 2015 16:42*

**+I Laser** Yeah pretty much. You just need g-codes on SD card and you are all set. One great thing about Turnkey Inkscape Plugin is you can have multiple settings (speed/power/frequency) for cut/engrave in one g-code file to get various results instead of running different files. 



I am not so sure about network the arduino, because 1> I am not a programmer 2> not sure if any ports left to connect Ethernet shield 3> not sure if it has memory available to include code for networking. However I see a lots of things in the code for 3D printer, if one can remove that and include networking codes it may be possible.



But if you want to connect this laser in network, you can use a low grade pc with it and control the machine using LaserWeb. More information here: [https://github.com/openhardwarecoza/LaserWeb](https://github.com/openhardwarecoza/LaserWeb) 


---
**Anthony Bolgar** *December 12, 2015 12:22*

You can always use a Toshiba flashair card instead of a normal SD card. The toshiba card connects to the network and you can navigate anywhere you have granted access to the card on your network from the Print from SD menu on the controller LCD connected to the Ramps board. A lot of people use this with their 3d printers.


---
**I Laser** *December 13, 2015 10:12*

That does sound sweet. Guess when I get bored of corel I'll look at that lol.


---
**ChiRag Chaudhari** *December 16, 2015 02:37*

**+Anthony Bolgar** Added to wish list. I sure do need that!


---
*Imported from [Google+](https://plus.google.com/113905238524886327481/posts/MFPfqEGcKD5) &mdash; content and formatting may not be reliable*
