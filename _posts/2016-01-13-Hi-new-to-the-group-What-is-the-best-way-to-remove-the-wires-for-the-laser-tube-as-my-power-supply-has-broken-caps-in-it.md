---
layout: post
title: "Hi new to the group , What is the best way to remove the wires for the laser tube as my power supply has broken caps in it"
date: January 13, 2016 14:16
category: "Discussion"
author: "Danko Andruszkiw"
---
Hi new to the group , 

What is the best way to remove the wires for the laser tube as my power supply has broken caps in it. the High volt line and  yellow current line do't have any connectors on them  so  can't just disconnect form the power supply. 







**"Danko Andruszkiw"**

---
---
**Danko Andruszkiw** *January 13, 2016 15:35*

No not going to replace it just to fix the caps, is it ok to cut off the silicon then remove the cables ?




---
**Sean Cherven** *January 13, 2016 15:37*

Yeah, you'll have to replace the silicone when you put it back together. My K40 came with a tube of that silicone included.


---
**ChiRag Chaudhari** *January 13, 2016 16:29*

**+Danko Andruszkiw** My power supply also had some issues, well they still there, but got big fat refund from seller. So make sure you get yours I got $89.10 back as there is a US seller selling it for that price. Or they will send you new power supply. 


---
*Imported from [Google+](https://plus.google.com/115960837475499103700/posts/GpZnNy2HUiK) &mdash; content and formatting may not be reliable*
