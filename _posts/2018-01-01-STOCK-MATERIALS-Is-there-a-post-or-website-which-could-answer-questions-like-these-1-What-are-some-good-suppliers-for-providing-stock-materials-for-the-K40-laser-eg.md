---
layout: post
title: "STOCK MATERIALS Is there a post or website which could answer questions like these: (1) What are some good suppliers for providing stock materials for the K40 laser (e.g"
date: January 01, 2018 22:46
category: "Materials and settings"
author: "Joel Brondos"
---
STOCK MATERIALS



Is there a post or website which could answer questions like these:



(1) What are some good suppliers for providing stock materials for the K40 laser (e.g. wood, acrylic, anodized aluminum)?



(2) Is there any way to treat wood BEFORE using the laser on it so that the blackened oils do not ooze out of it?



(3) I am aware that the smoke from burning certain woods can be somewhat toxic . . . and laser engraving acrylic smells terrible (even with a good exterior exhaust). My laser cutter is well-ventilated -- but are there any precautions to take regarding fumes?





**"Joel Brondos"**

---
---
**Jeff Lin Lord** *January 02, 2018 03:32*

For acrylic (although not completely certain on the composition of these materials), I have had great luck with wide screen TVs.  The screens usually have a very decent plexiglass material at least 1/4” or thicker.  Some of the other layers are interesting with textured vinyls and such.  


---
**Ned Hill** *January 02, 2018 06:19*

1.)I get most of my thin wood, including 1/8" ply,  online from ocoochhardwoods.  

2.)The staining effects of the residue from cutting/engraving can prevented by first masking the wood using wide blue painters tape or a lettering transfer tape like RTape Conform 4075RLA.   I get mine from here.[https://www.signwarehouse.com/c/rtape-conform-4075rla-transfer-tape-high-tack](https://www.signwarehouse.com/c/rtape-conform-4075rla-transfer-tape-high-tack) 

3.)  DO NOT laser PVC/vinyl  and then good ventilation is all you need for everything else.  PVC/vinyl release HCl gas which is toxic but is also highly corrosive for the machine.


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/WmoMmZVsAEo) &mdash; content and formatting may not be reliable*
