---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) Found this site just before"
date: December 04, 2015 15:52
category: "Hardware and Laser settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Found this site just before. Not sure if their prices are reasonable on mirrors & components, but maybe someone else is interested.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Anthony Bolgar** *December 04, 2015 16:54*

The mirrors on that web site start at $200 and go up from there. Yes $200+ for ONE mirror. Don't think I will be shopping there.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 04, 2015 18:04*

**+Anthony Bolgar** Yeah I saw that too. Thought it was crazy, but wasn't sure what is a decent price for them.


---
**Scott Thorne** *December 04, 2015 22:08*

I paid 39 for 3 of of Amazon...so far they are working fantastic.


---
**Sean Cherven** *December 05, 2015 02:49*

**+Scott Thorne**  What type of Mirrors did you buy?


---
**Scott Thorne** *December 05, 2015 03:13*

Mo from Amazon...3mm thick...20mm diameter...the price was 19 each but for some reason they sent me 3 of them for 38 dollars....very good quality.


---
**Scott Thorne** *December 05, 2015 03:16*

[https://www.amazon.com/gp/product/B00XJKL4TO/ref=ya_st_dp_summary](https://www.amazon.com/gp/product/B00XJKL4TO/ref=ya_st_dp_summary)


---
**Scott Thorne** *December 05, 2015 03:16*

That's the link for them.


---
**Sean Cherven** *December 05, 2015 03:47*

They out of stock lol.


---
**Anthony Bolgar** *December 05, 2015 09:42*

$11.35 and free shipping



SMO HQ 20mm MO Reflection Mirror Reflector for 10600nm CO2 Laser Engraving Cutting -[http://www.amazon.com/SMO-Reflection-Reflector-10600nm-Engraving/dp/B00OXLP4OE/ref=sr_1_28?ie=UTF8&qid=1449308453&sr=8-28&keywords=Mirror+For+CO2+Laser](http://www.amazon.com/SMO-Reflection-Reflector-10600nm-Engraving/dp/B00OXLP4OE/ref=sr_1_28?ie=UTF8&qid=1449308453&sr=8-28&keywords=Mirror+For+CO2+Laser)


---
**Scott Thorne** *December 05, 2015 11:44*

**+Sean Cherven**​....that figures.


---
**Sean Cherven** *December 05, 2015 12:53*

**+Anthony Bolgar** that one looks to be shipped from China or something. Very long delivery date.


---
**Scott Thorne** *December 05, 2015 13:09*

Yup...around a month or so.


---
**Anthony Bolgar** *December 05, 2015 13:26*

I am so used to buying from China, I don't worry about delivery times, I'd rather save the money.


---
**Sean Cherven** *December 05, 2015 13:28*

I'm the same way, i just don't know about the quality control of these mirrors and lens.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/2o1P32QP9vW) &mdash; content and formatting may not be reliable*
