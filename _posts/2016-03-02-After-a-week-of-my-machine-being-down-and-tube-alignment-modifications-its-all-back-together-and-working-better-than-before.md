---
layout: post
title: "After a week of my machine being down and tube alignment modifications it's all back together and working better than before"
date: March 02, 2016 15:02
category: "Discussion"
author: "Scott Thorne"
---
After a week of my machine being down and tube alignment modifications it's all back together and working better than before. ...3 inch focal length lens makes A BIG difference in cutting thicker materials.





**"Scott Thorne"**

---
---
**3D Laser** *March 02, 2016 15:26*

Where did you get your lens 


---
**Scott Thorne** *March 02, 2016 15:54*

Ebay...3 week delivery time but it was only 26 bucks...not bad....good quality too.


---
**Scott Thorne** *March 02, 2016 15:55*

I am cutting 3/8 acrylic at 50% power at 8mm/s....not bad. 


---
**Scott Thorne** *March 02, 2016 16:03*

[http://m.ebay.com/itm/ZnSe-GaAs-Focal-Lens-for-10-6um-Co2-Laser-Engraver-Cutting-Machine-12-25mm-1-4-/321078811086?nav=SEARCH](http://m.ebay.com/itm/ZnSe-GaAs-Focal-Lens-for-10-6um-Co2-Laser-Engraver-Cutting-Machine-12-25mm-1-4-/321078811086?nav=SEARCH)


---
**Scott Thorne** *March 02, 2016 16:07*

**+Corey Budwine**....if you don't have an adjustable z table the 3 inch lens will be difficult to use. ...as far as setting the focal length goes.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 03, 2016 07:36*

So I'm wondering, you mentioned that it makes a big difference cutting thicker materials. How about on thinner materials, does it cut better/worse than the stock focal length? 


---
**Scott Thorne** *March 03, 2016 10:24*

It Cuts better all the way around. 


---
**Mark Finn** *March 03, 2016 22:30*

would you reccommend ZnSe or GaAs?


---
**Scott Thorne** *March 03, 2016 22:56*

Znse....best of the best....just my opinion. 


---
**3D Laser** *March 04, 2016 00:12*

**+Scott Thorne** I'm looking at getting an adjustable lab jack to mount my honey comb bed to so that I can raise and lower it.  It's a poor man z table If it works as it is only 45 dollars for one I am going mount a ratchet wrench to it so I can raise and lower it 


---
**Scott Thorne** *March 04, 2016 17:20*

**+Corey Budwine**...that sounds like it might just work great. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/25om2Le1ddt) &mdash; content and formatting may not be reliable*
