---
layout: post
title: "Guys I'm looking to do a grave marker for my dog"
date: June 26, 2017 22:59
category: "Object produced with laser"
author: "Chris Hurley"
---
Guys I'm looking to do a grave marker for my dog. Is there any type of stone that I can mark with the laser that would last for a little bit. 





**"Chris Hurley"**

---
---
**Jim Hatch** *June 26, 2017 23:32*

Sorry about your dog. Hard to see them go. You can actually engrave a lot of stone - slate is easy but granite & marble work too. You might use a dark marble or granite tile from Home Depot or Lowes as the stone. You can epoxy it to a bigger stone or boulder. 


---
**greg greene** *June 26, 2017 23:53*

slate works


---
**Jim Hatch** *June 27, 2017 00:09*

**+greg greene**​ - true but slate doesn't last as long as a harder stone like granite. 


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/KiefDTBoUCv) &mdash; content and formatting may not be reliable*
