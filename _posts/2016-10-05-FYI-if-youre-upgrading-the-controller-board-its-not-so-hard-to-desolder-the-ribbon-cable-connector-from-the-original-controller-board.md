---
layout: post
title: "FYI if you're upgrading the controller board it's not so hard to desolder the ribbon cable connector from the original controller board"
date: October 05, 2016 14:24
category: "Smoothieboard Modification"
author: "Michael Ang"
---
FYI if you're upgrading the controller board it's not so hard to desolder the ribbon cable connector from the original controller board. I used a butane lighter to heat all the pins at once while pulling the connector with vise grips. Then with a bit of stripboard and you can make a breakout board. My original controller died and I didn't feel like waiting to get a Middleman board (though if you have time that's a great way to go!) Really enjoying using Visicut from my Mac over Ethernet!

[https://www.flickr.com/photos/mangtronix/29836941070/in/photostream/](https://www.flickr.com/photos/mangtronix/29836941070/in/photostream/)





**"Michael Ang"**

---
---
**Michael Ang** *October 05, 2016 14:24*

[https://www.flickr.com/photos/mangtronix/30048078601/in/photostream/](https://www.flickr.com/photos/mangtronix/30048078601/in/photostream/)

[flickr.com - Ribbon connector adapter](https://www.flickr.com/photos/mangtronix/30048078601/in/photostream/)


---
**Don Kleinschnitz Jr.** *October 05, 2016 14:37*

You can also get these connectors pretty fast and cheap from [http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html)

[donsthings.blogspot.com - K40-S Middleman Board Interconnect](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html)


---
**Michael Ang** *October 05, 2016 14:41*

Yeah I would have but I'm in Europe. If you're Stateside I'm sure it's easier to order the board :)


---
**Don Kleinschnitz Jr.** *October 05, 2016 15:04*

**+Michael Ang** actually having gone the Middleman route I realized that it might have been better to make my own daughter card that included my interlock functions. I did not use most of what is on the middleman.

[https://goo.gl/photos/RjGAjmg9Lio5g33u6](https://goo.gl/photos/RjGAjmg9Lio5g33u6)


---
**Vince Lee** *October 05, 2016 15:16*

Fwiw, when you order a middleman board you get 3 and I got 6 because the first run was missing the silkscreening and I never used one.  I still have a bunch left over as well as many of the custom board I designed that switches back and forth from a smoothieboard to the stock controller board with dip switches.  If anybody wants either drop me a private message and I can put them in the mail so you don't have to wait a month to have a new one produced.


---
**HalfNormal** *October 05, 2016 15:33*

**+Vince Lee** drop me an email larrygon gmail 


---
**Ariel Yahni (UniKpty)** *October 05, 2016 15:40*

**+Vince Lee**​ Welcome back


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2016 17:10*

Hey Vince, I'd take one of the not so good middlemen just to be able to see what it's all about. Let me know if you can send one over. 


---
**Vince Lee** *October 05, 2016 20:44*

Sure thing.  You can just private message me the address to send it (post a message setting me as the recipient).  Here is a link to info about the board:  [https://docs.google.com/document/d/1cdnJmbJ4UsGVkWX3LgAMKOc3ImU9cXQA5RWOFvzn8i0/edit?usp=sharing](https://docs.google.com/document/d/1cdnJmbJ4UsGVkWX3LgAMKOc3ImU9cXQA5RWOFvzn8i0/edit?usp=sharing)


---
*Imported from [Google+](https://plus.google.com/101545054223690374040/posts/5fH11hFSFFZ) &mdash; content and formatting may not be reliable*
