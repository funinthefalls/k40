---
layout: post
title: "Hi guys, I've been using my K40 like machine with some success now, following an air assist conversion"
date: August 07, 2017 18:43
category: "Software"
author: "Peter Gisby"
---
Hi guys,



I've been using my K40 like machine with some success now, following an air assist conversion. The question I have is about the design software and configuring it to do what I think is a simple thing.....



Here's the deal... I have the need to make double sided engraved and cut signs. However, no matter how I try and allign things when I cut the revise side it's always mis-alligned. Is there something I'm missing in the CorelLaser software?



I thought it would be as simple as:

Engrave the reverse side

Flip the board

Engrave the facing side

Cut the facing side



BUT..!!!! this always results in the reverse engraving being about 10mm out, sometimes worse. Is there an easy way I'm just not aware of within CorelLaser to make this happen flawlessly??



Any help very much apreciated.



Thanks.

Peter.





**"Peter Gisby"**

---
---
**Martin Dillon** *August 07, 2017 20:50*

you could use some sort of indexing marks.  Also both pictures need to be the exact same size or you have to set an offset when you engrave the smaller one.




---
**Martin Dillon** *August 07, 2017 20:59*

another way to do it, if you don't want to use an offset, is to make a box around each picture that is the same size in both pictures.  You can then either etch the line or set the live size to zero.  The software will see the box but not etch it.  

All of your problems stem from the fact that Corel Laser will always move your picture into the upper right corner regardless of how you have positioned it in the design software.


---
**Martin Dillon** *August 07, 2017 21:00*

Other members, please feel free to correct me.  I am saying all of this from memory from previous posts.


---
**Mark Brown** *August 10, 2017 02:53*

I usually stick a tiny square in the top left corner of both the engraving and cutting files/layers.  Because like Martin said the software automatically shifts the image to the top left. 


---
**Frank Fisher** *August 10, 2017 10:01*

Yes I always work from a template with a full stop top left (and marked no edit in its own layer).


---
**Mark Brown** *August 12, 2017 02:02*

"Dot in the corner, in it's own layer"?

I need a dot in each layer?


---
**Frank Fisher** *August 12, 2017 07:05*

No make a template with a layer with a dot in the corner, use that for each drawing.  Each file will need it, it is there as an origin point for each cut.  Mark it no edit so it doesn't get moved/deleted if you do Select All.


---
**Mark Brown** *August 12, 2017 11:29*

Okay, that makes sense. Cool, thanks.


---
*Imported from [Google+](https://plus.google.com/111055748369814573959/posts/DQd7iVVf4oY) &mdash; content and formatting may not be reliable*
