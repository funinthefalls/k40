---
layout: post
title: "Someone linked an awesome laser cut box generator the other day, the website totally looked like it was from the 90s, and the guy wanted 13 euro for password access"
date: August 23, 2016 15:47
category: "Repository and designs"
author: "ThantiK"
---
Someone linked an awesome laser cut box generator the other day, the website totally looked like it was from the 90s, and the guy wanted 13 euro for password access.  Anyone know the site I'm referring to?  His box designer had a whole hell of a lot of different parametric designs.





**"ThantiK"**

---
---
**Alex Krause** *August 23, 2016 15:51*

Yes I linked it in laser web **+ThantiK**​ will copy the post link


---
**Ariel Yahni (UniKpty)** *August 23, 2016 15:52*

[http://imgur.com/CaWFQn9](http://imgur.com/CaWFQn9)


---
**Alex Krause** *August 23, 2016 15:52*

[https://plus.google.com/109807573626040317972/posts/CMiNJtmNxyr](https://plus.google.com/109807573626040317972/posts/CMiNJtmNxyr)


---
**ThantiK** *August 23, 2016 18:36*

**+Alex Krause** YES!  I thought it was in the K40 group for some reason and couldn't find it.


---
**Robi Akerley-McKee** *August 24, 2016 23:27*

There is also the Inkscape laser box generator extension


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/XFBjaKD7wRv) &mdash; content and formatting may not be reliable*
