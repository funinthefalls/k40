---
layout: post
title: "Hello every body, sorry for bothering you again"
date: April 09, 2018 16:02
category: "Discussion"
author: "Manon Joliton"
---
Hello every body, sorry for bothering you again.

My K40 suddenly has what I believe is a loss of power, i used to cut my wood veneer at 14-20% of the power but 'til few days, it's not enough anymore ...

I didn't touch anything that would explain this sudden change but 1 week ago i noticed that sometimes the laser didn't cut the same power all over my drawin' (not sure to be really clear sorry)

I mean that it cuts some parts but not every, just like if the power was lower..



If I want to cut out my dedsigns, i have to increase the power from 14% to at least 20% but then the cut isn't neat anymore.



I saw that a power loss could be caused by an old water so i changed it but it still doesn't cut with the usual power ...



Do you have any idea ?

Sorry for my potentially bad english

Thank you :3





**"Manon Joliton"**

---
---
**HalfNormal** *April 09, 2018 16:31*

Clean the mirrors and check alignment. 


---
**Don Kleinschnitz Jr.** *April 09, 2018 16:46*

How old is your tube and how hard do you run it?

Do you have digital panel? i.e. can you read tube current?




---
**Manon Joliton** *April 09, 2018 18:17*

I already clean the mirrors and the alignment is almost correct (could be a gap of 1-2mm between the top right and bottom right corner... maybe that's enough to create the loss ?)

Bought my K40 in September so it's not that old, what do you mean by 'how hard do i run it ?'

I choose my power with a +10 +1 +0.1 or -10 etc button, is that what you call digital panel ? I'm not really informed about the machine ^_^


---
**Ned Hill** *April 09, 2018 19:43*

When you do your alignment do you check all three mirrors?  Do you have an air assist nozzle and if so is the beam exiting head in the center of the nozzle?  When you check the alignment on the first mirror do you get a single spot or a doughnut shape?


---
**Phillip Conroy** *April 09, 2018 22:03*

Do not forget to clean focal lens ,and bump goes up


---
**Manon Joliton** *April 10, 2018 10:25*

Just checked, the alignment is almost perfect, all mirror are clean, cleaned lense yesterday, changed the water too but i still have this problem, i dont understand why i need 2-3% power more than usual ^_^ i have no air assist, the dot made during the alignment are round but i need to press the laser test button longer than i used to


---
**Phillip Conroy** *April 10, 2018 10:44*

Check fan is spinning on laser pwr supply


---
**Manon Joliton** *April 10, 2018 10:48*

**+Phillip Conroy** How do i check that ? I'm a noob xD

Thanks




---
**HalfNormal** *April 10, 2018 12:34*

Differences in material can cause changes in settings.


---
**Manon Joliton** *April 10, 2018 12:37*

**+HalfNormal** Yeah I know, but it's been like 6 mounths since I cut my veneer with the laser and I'm now used to select the best cutting power for each wood/veneer, but since 1 week, those parameters seems not to be enough anymore


---
**HalfNormal** *April 10, 2018 12:38*

Trying all the easy solutions before the ones that cost money! ;-)


---
**Don Kleinschnitz Jr.** *April 10, 2018 12:55*

**+Manon Joliton** from all indications it goes without saying that the power available at the surface is less than it used to be:



These are potential and typical causes of weak power <b>assuming you are using the same materials</b> as previous jobs that worked: 



1. Changes in power settings from panel or software

2. Poor alignment

3. Improper focus

4. The tube is wearing out (tubes power decrease with time and use)

5. The LPS is going bad



Seems you have eliminated 1 & 2. 

Do a ramp test for #3. 

Since your machine does not have a laser current meter its not possible to know how much current is being used. 

If the current is low then I would suspect the LPS. 

If the current was high but still lower in power I would suspect the tube. 



Kinda flying in the blind without a laser current meter.



 








---
**Manon Joliton** *April 10, 2018 15:35*

Thanks for all these informations, how do I check the focuse ?

I also just noticed that while cutting at a higher power (30-40%), a 5mm strip is burnt above the cutting line (see the pic) it's not caused by a litlle flame maybe this coud help finding whats going on ^_^

Do you think i sholdn't use my laser 'til we find out where the problem is from ?![images/6d1066db4c92469e082ef5fc0a3c9013.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d1066db4c92469e082ef5fc0a3c9013.jpeg)


---
**Don Kleinschnitz Jr.** *April 10, 2018 18:22*

Do a "ramp" test. Search forum for instructions. 

Looks like you have a reflection likely in the head. See **+Ned Hill** question as to alignment through the head. 

Post pictures of beam spots on mirrors and through the head.  By beam spots I mean whatever you use to see spot position on mirror and bed. 


---
**syknarf** *April 11, 2018 11:40*

**+Manon Joliton** Hi, if you are getting a double line at high power surely you have a reflection, double check mirror aligment, and get sure the beam is centered when reach the head hole. I had same problem some time ago, and a good aligment solves it.


---
**Manon Joliton** *April 11, 2018 13:34*

**+syknarf** ok i'm gonna check again. Do all the dots need to be in the center of each mirror or is it just for the hole of the laser head ?

(Yesterday i just made sure that the dots 'stacked' when the mirror is close and when it's further)


---
**syknarf** *April 11, 2018 14:08*

**+Manon Joliton** align the dots as close to the center as you can. Especially on the head and the lens. If beam reaches the head close to the borders, the risk of a reflection on the interior of the head is increased, and double beams can be created, also this decreases the cut power.


---
**Manon Joliton** *April 12, 2018 18:06*

Thank y'all guys, did the alignement again and cleaned the inside of the laser head and the lense and it seems to work back, thank you all for your help ✋![images/a778b02e73deacd038a62ee40c349ef3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a778b02e73deacd038a62ee40c349ef3.jpeg)


---
**Thom Dooher** *April 20, 2018 10:53*

I'm having this same problem. I've replaced the pot, the LSU, and just now the tube. I've spent hours aligning and cleaning all the mirrors and focus lens.



 I'm at a loss


---
**syknarf** *April 20, 2018 17:44*

**+Thom Dooher** double lines are usually due to a reflection. If you have made an exhaustive alignment and beam reaches the lens exactly at it's centre, may be that the lens is damaged and causes the double beam?... check if the beam is in centre of the lens, I check it removing the lens and placing a little paper tape , make a test fire and look with a mirror where the dot is on the paper.


---
**Thom Dooher** *April 20, 2018 18:14*

I'm getting the loss of power not the double lines


---
**syknarf** *April 20, 2018 18:36*

**+Thom Dooher** oh, sorry, I misunderstood your comment.

Seems that you have changed or checked almost all the things that can cause a power leak ...


---
**Thom Dooher** *April 20, 2018 18:41*

That's what I keep thinking, but it's driving me crazy. 


---
**Mario Garcia** *April 21, 2018 23:24*

Hello I have same problem, machine was lossing power and now has nothing I did some trials (showed in youtube) there is not arc may be the power sopply is damaged


---
**Thom Dooher** *April 22, 2018 00:07*

I found a dead bug inside my laser head. 


---
*Imported from [Google+](https://plus.google.com/110371358543635278419/posts/gYBFdEcxt5A) &mdash; content and formatting may not be reliable*
