---
layout: post
title: "Someone asked me about getting an air assist head with pointers and I lost the post"
date: December 04, 2017 06:05
category: "Discussion"
author: "Anthony Bolgar"
---
Someone asked me about getting an air assist head with pointers and I lost the post. I have one if whoever asked is seeing this post.





**"Anthony Bolgar"**

---
---
**Gee Willikers** *December 04, 2017 06:09*

Like two laser lines? The type that stays on center independent of z height?


---
**Anthony Bolgar** *December 04, 2017 06:42*

Yup




---
**Gee Willikers** *December 04, 2017 07:17*

Didn't see the post, but it's on my wish list for next year.


---
**Anthony Bolgar** *December 04, 2017 07:19*

I can send you the printed air assist nozzle minus the laser pointers if you just cover shipping.




---
**Gee Willikers** *December 05, 2017 01:13*

No worries, I have a 3d printer.


---
**Joe Alexander** *December 05, 2017 13:28*

i use [thingiverse.com - Dual line laser pointer holder for chinese laser cutter by joostn](https://www.thingiverse.com/thing:445354) with the lightobjest head and it seems to work quite well. alternately you could try [https://www.thingiverse.com/thing:1063067](https://www.thingiverse.com/thing:1063067).


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/EHCAF72YsqR) &mdash; content and formatting may not be reliable*
