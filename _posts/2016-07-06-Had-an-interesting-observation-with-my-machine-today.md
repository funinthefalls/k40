---
layout: post
title: "Had an interesting observation with my machine today"
date: July 06, 2016 02:30
category: "Discussion"
author: "Ned Hill"
---
Had an interesting observation with my machine today.  Was reverse engraving a small image on 3mm Alder and noticed these stripes forming in the engraved background. 15% power at 175 mm/s.  The regular straight lines were puzzling and were not related to the grain of the wood.  Checked all mirrors and lens and made sure everything was tight.  Started the engrave over and the stripes where still there. Looked down at my airbrush compressor, which sits on the bottom shelf under the laser, picked it up and sat it on the floor.  At which point the stripes stopped forming, as you see in the pic, and I knew immediately what was happening.   Confirmed it by measuring the stripe spacing which was almost 3mm which gives almost exactly 60 cycles per 175mm (175 mm/s engrave speed).  The air compressor operates at the US power line freq of 60hz and was causing the light piece of wood to vibrate around the focus point of the laser at 60hz.  So engrave speeds that are around multiples, or sub-multiples,  of your power line freq are going to be susceptible to this phenomenon.

![images/1bab0981c665c318fe1a88cfbbde326d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1bab0981c665c318fe1a88cfbbde326d.png)



**"Ned Hill"**

---
---
**Alex Krause** *July 06, 2016 02:43*

Awesome observation


---
**Scott Marshall** *July 06, 2016 02:46*

You just solved  a long enduring mystery.



We've been asked about unexplained moray patterms in the background for a long time, and nobody has come up with a good explanation.



It's so obvious once you know the answer (I've seen similar problems in laser semiconductor wafer lithography machinery due to vibration), I can't believe it took this long.



They could easily be produced by stepper motor vibration being transmitted to a loose mirror as well. Knowing that vibration is the cause makes it infinitely easier to run down the exact source of the problem and to fix it.



Nice work!



Scott


---
**Ned Hill** *July 06, 2016 02:48*

Thanks Scott.  I did seem to remember somebody posting something about moray patterns not too long ago.  Glad I can contribute something useful to this great group. :)  

My background is analytical chemistry so I have a lot of experience troubleshooting instrumention induced artifacts. 


---
**Alex Krause** *July 06, 2016 05:41*

I think this may hold true for exhaust fans as well... ac motor connected to the chasis of the laser providing oscillation of the work piece


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 06, 2016 09:10*

That's very interesting. Although most of it seems to go over my head, it's great to know there is an actual reason for this phenomena.


---
**Scott Marshall** *July 06, 2016 09:26*

The way it works is analogous to putting a running motor or tuning fork against a glass of water, if you vary the speed (frequency), you'll see the ripples "freeze" and form what is known as an interference pattern. (often called a "moray" pattern, I'm not sure why) It's caused by out going waves meeting the reflected ones (bouncing off the opposite side of the glass). The effect happens in multiples of the primary frequency, so you'll see patterns for a line frequency (60hz) vibration at 120, 240, 480 hz etc.

If you divide the speed of the laser head by the period (time for 1 cycle or 1/f) it will tell you how far apart the pattern will form.



It's responsible for a lot of odd patterns in nature, like  freak waves (or 3 sisters) where the frequencies combine to all peak at one point. They can now predict when and where they will occur using math way over my head.



As usual, probably more than you wanted to know...


---
**Ned Hill** *July 06, 2016 15:02*

Moray pattern sounded familiar so I had to google it, so it's moire pattern which is french for a type of pattern seen in certain fabrics.  Normally this is caused by two overlapping sets of parallel lines where one set is at a angle to the other set.  The pattern I'm seeing with the laser etching looks like a classic moire pattern but the actual cause of the lines is by a different effect.  When I look at the etching under magnification the bright areas are smooth while the dark areas are very thin fins of wood with only one set of parallel lines.  I'm hypothesizing that, as the workpiece vibrates up and down, the dark areas are more in focus while the light areas are slightly out of focus causing the wider beam to overlap the previous raster line more so you get no fins.  My bed is made from an expanded  metal sheet which does have a small amount of flex to it which is probably enhancing the vibration of the piece being etched.


---
**Scott Marshall** *July 06, 2016 17:51*

Makes sense to me, the vibration is more of a start/stop rather than a interaction. 



I wasn't sure of the terms spelling, but at at IBM where I 1st ran into the problem in the e-beam litho lab, they used the spelling Moray. Probably just a convolution of the proper term. On the wafers, it would often manifest as a pattern of diamonds across a sizeable field. Similarly the vacuum pumps would cause all sorts of vibration issues. There were eventually moved out of the lab and into the core (dirty area inside a cleanroom where machinery resides) 



It's a neat effect, but seems to cause a lot of issues with precision machinery.


---
**Corey Renner** *July 08, 2016 21:11*

Ned, I think you mean a Moire pattern, this is a Moray pattern: [http://2.bp.blogspot.com/-0dHthu6dvDk/TmqVarJ1ESI/AAAAAAAAA6I/E0uXixphZCI/s1600/moray-eel-pattern.jpg](http://2.bp.blogspot.com/-0dHthu6dvDk/TmqVarJ1ESI/AAAAAAAAA6I/E0uXixphZCI/s1600/moray-eel-pattern.jpg)



Great catch, btw.  cheers, c


---
**Ned Hill** *July 08, 2016 21:16*

**+Corey Renner** Thanks Corey :) Actually if you read my previous comment I did figure out the correct spelling. ;)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/2mP6xqYF6Bf) &mdash; content and formatting may not be reliable*
