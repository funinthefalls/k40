---
layout: post
title: "Oh ho ho lookie what I got in the mail today"
date: November 28, 2016 18:55
category: "Smoothieboard Modification"
author: "Kelly S"
---
Oh ho ho lookie what I got in the mail today.  :)  My friends and my C3D mini board.  :)  Will document the install from a noobie point of view.  :)

![images/6f8e3902df1e685079749bffe45f5709.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6f8e3902df1e685079749bffe45f5709.jpeg)



**"Kelly S"**

---
---
**Jonathan Davis (Leo Lion)** *November 28, 2016 19:04*

Nice and i just own one board like it and just this past Friday i ordered a set of end stops on amazon as with an A5 china made laser engraving machine you are going to need them  but I did not get a SD card with mine as i had to provide it myself.


---
**Kelly S** *November 28, 2016 19:17*

A5 machine?  I have one of the newer k40's and was told I do not need to add end stops to it.  :)  


---
**Jonathan Davis (Leo Lion)** *November 28, 2016 19:40*

**+Kelly S** its a laser engraving machine you can buy from chinese companies like banggood and gearbest.


---
**Antonio Garcia** *November 28, 2016 21:47*

**+Kelly S** lucky guy!!! :) i will follow your steps :)


---
**Andy Shilling** *November 28, 2016 21:57*

**+Kelly S** You are so lucky, I think my second moshi board is now down so I am just sat here waiting for my C3D mini. I look forward to watching your tutorial as I too am at the Noob level.




---
**Kelly S** *November 28, 2016 22:12*

Well I have it in, glcd does not seem to work right as of yet, currently installing stuff for laserweb3 **+Andy Shilling** I did take lots of pictures along the way and plan to do a write up. 


---
**Kelly S** *November 28, 2016 22:28*

So far, notable things, seems to nee usb power at all times to drive the LAN and the display as well as the board.  Will look further into it later as I was hoping to use a computer just a tad further away than a usb cable will allow.   Also ran into a hiccup installing laser web 3, and with the board installed and a config on the sd card it does not try to home or anything when I flip the red switch that is odd.  Too many questions and Ray is busy at the moment.  o_o  


---
**Andy Shilling** *November 28, 2016 22:42*

Can you put a photo of your psu on

 please as I need to order one up and I'm not sure which one to go for.


---
**Kelly S** *November 28, 2016 23:10*

**+Andy Shilling**. Here is one currently on my phone.

![images/55e9a593322753f0849bf2b15f3b03d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55e9a593322753f0849bf2b15f3b03d2.jpeg)


---
**Kelly S** *November 28, 2016 23:11*

![images/557714b8c8c8bc56a14f9c280d24548d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/557714b8c8c8bc56a14f9c280d24548d.jpeg)


---
**Andy Shilling** *November 28, 2016 23:16*

Thank you, that looks like the one I am going to get, I was just concerned about the connections as some have the white connectors and others the green.


---
**Ned Hill** *November 29, 2016 02:08*

Nice.   Ordered mine last night.  Will be keeping an eye out for your posts on this. :)


---
**Kelly S** *November 29, 2016 02:22*

I am stuck on the laser web portion.  Have it installed but yet to be tested.


---
**Kelly S** *November 29, 2016 03:48*

So, display is up and running thanks to the help from **+Ray Kholodovsky**.  I am down to Laser Web Woes and a strange condition of the tube firing when you manually move the gantry left and right quickly.  

 


---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 03:54*

So, to recap today's progress for the viewing audience (Hi Andy and Ned!)...

Kelly's laser is a slightly different one...   The power on and off operates differently than how we've seen before - so when one switch is off, the 24v at the power connector is off, and that means that the Mini can't power itself.  This is what the earlier "usb power and glcd concerns were about" as well as the current one which we will figure out later. 

If there's a lesson here, it is to order the simplest k40 possible.  These LCDs and niftier buttons are complicating things slightly. But we'll figure it out, we always do.


---
**Ned Hill** *November 29, 2016 04:24*

Hmmm, judging by the pics Kelly posted above I'm pretty sure I have the same model.  So he's definitely my person to follow for this upgrade.  All I have offer in return is moral support at this time, so go Kelly go ;-)


---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 04:26*

**+Ned Hill** do you have any sort of LCD panel on your machine? B/c Kelly's psu looks just like the rest but the top panel is a whole different story. 


---
**Kelly S** *November 29, 2016 04:34*

Here is how my top looked.

![images/d20fae21a541947fbac63cd9ab20c92b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d20fae21a541947fbac63cd9ab20c92b.jpeg)


---
**Ned Hill** *November 29, 2016 04:35*

**+Ray Kholodovsky** Yes, I have an LCD for the the laser power display instead of the current meter found on other K40s.   The underside of the electronics panel in Kelly's pic looks the same as mine


---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 04:36*

Aha. This will be fun. 


---
**Ned Hill** *November 29, 2016 04:37*

Yep I have the same panel Kelly.




---
**Kelly S** *November 29, 2016 04:39*

When I got mine never thought I would have done half of what I have done to it.  But the software is just so bad lol..  It is my last leap of upgrades as I finished my manual adjustable bed, air assist and laser guide for focal adjustment.  When I seen the listign this was supposed to be the latest and greatest k40 around, that was before I learned of this group and what the k40 really had in store for me.  heh.... **+Ned Hill**


---
**Kelly S** *November 29, 2016 04:41*

I did take lots of pictures and will be doing a write up of it when I get close to finished.  :D


---
**Kelly S** *November 29, 2016 20:17*

Small update, got it installed, and laser web working with it.  First test was a little hot, will take some experimenting now to learn the ins and out of new software.  Will now put all the pictures I took of the install on the computer and do a write up of the process I required.  


---
**Kelly S** *November 29, 2016 21:34*

My current write up.  Will be changing as I go along.  [https://docs.google.com/document/d/1f4u79SzF7th84rFIfUXIpY8u4YEpwoGeH7z-K8yjxQc/edit?usp=sharing](https://docs.google.com/document/d/1f4u79SzF7th84rFIfUXIpY8u4YEpwoGeH7z-K8yjxQc/edit?usp=sharing)


---
**Kelly S** *November 30, 2016 16:43*

Wrong type of meter I know, will be fixed tomorrow haha....

![images/b0c1899ba2058207b0f9dbadc24f56e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b0c1899ba2058207b0f9dbadc24f56e0.jpeg)


---
**Andy Shilling** *November 30, 2016 21:41*

Nice start to the write up, My glcd turned up today and I expect endstops tomorrow. The 3d printed bits should be here by Saturday. I can't wait to get it all in and running, Ive installed Laserweb to my Mbp so Imjust sat waiting on the psu and C3D.


---
**Kelly S** *November 30, 2016 22:36*

Hope to hear it all goes well for you!  :)  Look forward to pictures.  :D


---
**Andy Shilling** *January 29, 2017 22:05*

**+Kelly S** just revisiting some posts regarding the C3D upgrade process, did you swap out the optical endstops for the arduino microswitch ones? if so could you post a picture of how you mounted them please.

 I've spend 2 days rewiring my machine as my psu was so old it's unbelievable but I can not think for the life of me how to mount the new endstops. Complete brain fry. 



Cheers


---
**Kelly S** *February 23, 2017 14:46*

Wow, sorry slow reply!!   No, I left all endstops as is.   Only part I swapped was the board. :)   soon to be upgrading the cutting area and may get different switches now **+Andy Shilling**


---
**Andy Shilling** *February 23, 2017 14:50*

**+Kelly S**​ lol don't worry I'm all done now.


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/1xgi9hDvLgK) &mdash; content and formatting may not be reliable*
