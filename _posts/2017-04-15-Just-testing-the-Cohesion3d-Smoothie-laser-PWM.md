---
layout: post
title: "Just testing the Cohesion3d Smoothie laser PWM"
date: April 15, 2017 02:34
category: "Smoothieboard Modification"
author: "Jim Fong"
---
Just testing the Cohesion3d Smoothie laser PWM. Works good enough for now.  100mm/sec.  



Thanks Ray. 



[https://imgur.com/a/wj93b](https://imgur.com/a/wj93b)





**"Jim Fong"**

---
---
**Ariel Yahni (UniKpty)** *April 15, 2017 02:45*

Good job, can you please post the settings used to get this? 


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 03:49*

Woohoo. I can see a difference at least every other square. Not sure how much the difference between 5 - 10% or 90-100% should be visible anyways.  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2017 05:11*

There is a noticeable difference between 5 & 10%. 95-100% looks very similar, but looks like it's pretty much tuned in perfectly.


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 05:15*

I was trying to be conservative in my evaluation :)  


---
**Ashley M. Kirchner [Norym]** *April 15, 2017 07:53*

**+Jim Fong**, could you post your settings please?


---
**Jim Fong** *April 15, 2017 12:11*

**+Ariel Yahni** These are the smoothieboard settings I used. The rest of the config.txt is stock from Ray that was on the memory card sent with the board.  I adjusted the pot at 10ma. 



[https://imgur.com/a/4hrRr](https://imgur.com/a/4hrRr)


---
**Don Kleinschnitz Jr.** *April 15, 2017 12:42*

**+Jim Fong** certainly one of the best response prints I have seen. 

Can you point to or post the source image, pls?






---
**Don Kleinschnitz Jr.** *April 15, 2017 12:44*

**+Claudio Prezzi** this seems to work as I would have expected, in that power is moderate and capable of going to 0 with reasonable steps?




---
**Jim Fong** *April 15, 2017 12:54*

**+Don Kleinschnitz**  something I found by doing a google search. 



[https://jrhzjq.by3302.livefilestore.com/y4mwWYJadyOS-Frff5-hRCgPIVtV13XboACcKloeDgpOW6p9uwEtXOtQTfxCTPuJ6PvuisiSCwx3Jc9-u-Lm-umLLv5Y4D5LqiAkQ90Hx911SlB7LXdnNKwrlvyrvzanBvxL0eTfx5g4NC_DvKs7OuIvsssHGbx1EIwSshvyUF3k6JgpRpnTGYQ5MAdrxb4zpl2gGdunzVxI4_C9K_Y8sQjjg/Calibration%20Gauge%20(360%20DPI).jpg?download&psid=1](https://jrhzjq.by3302.livefilestore.com/y4mwWYJadyOS-Frff5-hRCgPIVtV13XboACcKloeDgpOW6p9uwEtXOtQTfxCTPuJ6PvuisiSCwx3Jc9-u-Lm-umLLv5Y4D5LqiAkQ90Hx911SlB7LXdnNKwrlvyrvzanBvxL0eTfx5g4NC_DvKs7OuIvsssHGbx1EIwSshvyUF3k6JgpRpnTGYQ5MAdrxb4zpl2gGdunzVxI4_C9K_Y8sQjjg/Calibration%20Gauge%20(360%20DPI).jpg?download&psid=1)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2017 12:59*

**+Jim Fong** Not sure if it's my end bugging out, but I can't get that link to load anything.


---
**Don Kleinschnitz Jr.** *April 15, 2017 13:06*

**+Jim Fong** link goes to blank page....


---
**Jim Fong** *April 15, 2017 13:15*

**+Don Kleinschnitz** I found it off the original page



[everythinglasers.com - Calibration Guide for Engraving &#x7c; LaserWeb](http://www.everythinglasers.com/?page_id=18/laserweb/calibration-guide-for-engraving)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2017 13:21*

**+Don Kleinschnitz** **+Jim Fong** Haha, that turns out it was one I created a while back. Here's the original link if anyone wants it.



Calibration Gauge (360 DPI):

[onedrive.live.com - Microsoft OneDrive – Access files anywhere. Create docs with free Office Online.](https://1drv.ms/i/s!AqT6a-6Vl15NiOArFCGSMZJLoBNtSA)


---
**Jim Fong** *April 15, 2017 14:07*

**+Yuusuf Sallahuddin** 



It's not a great image but one of the last ones taken of Scout about 15years ago.  I'm happy.  150mm/sec. 



[https://imgur.com/a/H1fJx](https://imgur.com/a/H1fJx)


---
**E Caswell** *April 15, 2017 20:44*

**+Jim Fong** nice job, I had just downloaded mine to test it and set it up over the weekend..  **+Yuusuf Sallahuddin**  I got my link from GitHub. Attached.



[github.com - deprecated-LaserWeb1](https://github.com/LaserWeb/deprecated-LaserWeb1/blob/master/i/raster/calibration.jpg)




---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2017 22:01*

**+Jim Fong** Looks pretty good. Might be worth testing on different types of wood too, as some woods respond better than ply for engraving. I find when I engrave on ply that a lot of the dark areas don't actually end up dark but just deeper.


---
**Jim Fong** *April 15, 2017 22:13*

**+Yuusuf Sallahuddin** for sure, I have a woodshop so plenty of material to test.  This was just the nearest piece of scrap 3mm ply.  I originally bought the k40 to do images such as this but it was never as good with the stock controller.   Lots more experimenting......wanted to burn my brand/date on pieces of wood which I then can glue on the finished piece.  


---
**Cesar Tolentino** *April 17, 2017 00:32*

**+Jim Fong**​, is that laser web? Or what software did you use.


---
**Jim Fong** *April 17, 2017 00:55*

**+Cesar Tolentino** laserweb4 


---
**Cesar Tolentino** *April 17, 2017 01:30*

Thanks. I am not on the laserweb band wagon yet.


---
**Jerry Hartmann** *July 03, 2017 23:40*

What guide did you follow for to enable PWM?




---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/PjaLvQ7AYZf) &mdash; content and formatting may not be reliable*
