---
layout: post
title: "Originally shared by Todd Fleming My K40 arrived today!"
date: November 29, 2016 21:31
category: "Discussion"
author: "Todd Fleming"
---
<b>Originally shared by Todd Fleming</b>



My K40 arrived today! Thanks to all who contributed toward this!



![images/f0e6c2c4e61151e1f5172f825e9e02fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f0e6c2c4e61151e1f5172f825e9e02fc.jpeg)
![images/b1eecf073cec92993a89332fd1e0658d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b1eecf073cec92993a89332fd1e0658d.jpeg)
![images/b19b9a0a8a9dbda5ecda03c6b3ec22d0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b19b9a0a8a9dbda5ecda03c6b3ec22d0.jpeg)
![images/b89a88f8ebe4b8b50f3cf4824379b710.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b89a88f8ebe4b8b50f3cf4824379b710.jpeg)

**"Todd Fleming"**

---
---
**Kelly S** *November 29, 2016 21:43*

Awesome!  Very excited for you.   Hope all arrived in good condition. 


---
**Anthony Bolgar** *November 29, 2016 21:45*

I just noticed that yours has a much smaller viewing window than mine. If you have any questions about setting up or upgrading your k40, feel free to ask me, I'll do my best to help you through the process.


---
**greg greene** *November 29, 2016 22:04*

Live Long, and Laser :)


---
**Stephane Buisson** *November 30, 2016 00:41*

Psu with white connectors, Smoothie Cohesion3D is the right for you.

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)






---
**Anthony Bolgar** *November 30, 2016 00:50*

Spoke with **+Ray Kholodovsky** about that board, he says they are about 1 month out still. I sent **+Todd Fleming** a middleman board and a level shifter, and he has a 4xc smoothieboard that was donated.


---
**Armando Araiza** *December 01, 2016 06:52*

I also had one come in last week! Mirrors were aligned already and everything! Even had a LED light installed that I was not expecting. Happy Engraving!

 


---
*Imported from [Google+](https://plus.google.com/101442607030198502072/posts/iepPUMXhEXb) &mdash; content and formatting may not be reliable*
