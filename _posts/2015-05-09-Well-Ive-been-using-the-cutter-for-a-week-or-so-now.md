---
layout: post
title: "Well, I've been using the cutter for a week or so now"
date: May 09, 2015 21:24
category: "Object produced with laser"
author: "Chris M"
---
Well, I've been using the cutter for a week or so now. I've cut a load of trivial stuff like hearts and flowers for the OH's craft projects, not quite so trivial stuff like keytags (a rectangle with a hole and some engraved text) and a few tealight holders and a couple of lasered portraits. The tealight holder is based on [www.thingiverse.com/thing:662501/](http://www.thingiverse.com/thing:662501/)



Oh, what have I made most of? Scrap and mistakes :)



![images/e854290a47c4f15fcd481fa1777b90f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e854290a47c4f15fcd481fa1777b90f9.jpeg)
![images/7894e18992290fe93dd0cba0e7344de7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7894e18992290fe93dd0cba0e7344de7.jpeg)

**"Chris M"**

---
---
**Stephane Buisson** *May 10, 2015 08:55*

"Oh, what have I made most of? Scrap and mistakes :)"



On the bright side, I am sure you did learn a lot in the way

Thank you for sharing.


---
**Sean Cherven** *August 15, 2015 02:05*

How did you do that portrait? Are you using the stock controller board?


---
**Chris M** *August 15, 2015 07:24*

The portrait? Removed the background, I probably used Topaz Remask but Corel PhotoPaint has something adequate I think. Added a vignette. Resized. This is important. The size you choose now is the size you will engrave. I think I chose 100x100mm at 500dpi. Why 500? Because the laser is 1000dpi but Pixel SIze is set to 2, making it 500dpi. Reduced to 1-bit (2 colour), probably using the Stucki algorithm. Saved it as a tiff, or maybe a bmp. Never use jpg at this stage. The clever algorithms in jpg will turn your carefully crafted 2-colour image into a mishmash of 256 grey levels. Took it into Corel Drae and lasered it. Probably 300mm/sec and 10% power on poplar ply.



The controller? Stock Lihuyu labs. You're thinking, but that doesn't do greyscale. No, and neither did newspapers, but that didn't stop us from seeing greyscale images in newspapers. That clever Stucki algorithm makes our eyes see greys in that 2 colour image.



It's taken me longer to type this than to do the processing. The lasering took longer than the processing.


---
**Sean Cherven** *August 15, 2015 11:08*

Nice! I'll have to try this out!


---
*Imported from [Google+](https://plus.google.com/108727039520029477019/posts/ZU6qWGBTNyC) &mdash; content and formatting may not be reliable*
