---
layout: post
title: "Hi all, So I'm pretty much there with my SmoothieBoard conversion except for firing the fricking laser beam"
date: July 11, 2016 12:42
category: "Smoothieboard Modification"
author: "Darren Steele"
---
Hi all,



So I'm pretty much there with my SmoothieBoard conversion except for firing the fricking laser beam.  I've followed the instructions on [http://smoothieware.org/laser-cutter-guide#toc15](http://smoothieware.org/laser-cutter-guide#toc15) but for the life of me I cannot get my laser to fire.  I've hooked up the PWM to pin 2.4 and tested the connections with a multimeter and all seems well.  However there is a laser-fire pin on my PSU that is not connected to the SmoothieBoard in any way.  Is the PWM connection really the only one that's required?  Pressing the test fire button on my control panel fires the laser so I know it's operational.  This last bit is just getting me down now.  Any advice from the community?





**"Darren Steele"**

---
---
**Don Kleinschnitz Jr.** *July 11, 2016 12:55*

What vintage laser power supply, can you show us the connectors. 

Which signals: TH/TL or +P do you have?



What is connected to the L pin in the DC connector of the LPS.


---
**Ray Kholodovsky (Cohesion3D)** *July 11, 2016 13:10*

4 pin power connector going from the psu to the included board. Find the wire labeled LO. That's your laser fire signal. 


---
**Stephane Buisson** *July 11, 2016 13:20*

if you have an old (not very very old) PSU, you can also connect like fire test switch. attention without level shifter you have only 3.3v (not full power), with 5V level shifter you fire at 100% all time (so keep the pot as limiter), see other posts on that matter.



this case is discribe here : [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)


---
**Derek Schuetz** *July 11, 2016 14:03*

You do need to attach the laser fire wire to smoothie and add the lines to the config file 


---
**Mircea Russu** *July 11, 2016 14:37*

The new smoothieware supports both laser_module_pwm_pin which goes to IN pin on LPS and the laser_module_ttl_pin which goes to L/TL pin on the LPS. You can go either Level converter way or better the open-drain/mosfet way.


---
**Derek Schuetz** *July 11, 2016 14:48*

**+Mircea Russu** open drain MOSFET way?


---
**Mircea Russu** *July 11, 2016 15:15*

[http://donsthings.blogspot.ro/2016/06/k40-s-laser-power-control-interface_86.html](http://donsthings.blogspot.ro/2016/06/k40-s-laser-power-control-interface_86.html) Thanks to **+Don Kleinschnitz** 


---
**charlie wallace** *July 11, 2016 15:59*

[https://github.com/charlie-x/laser_pwm](https://github.com/charlie-x/laser_pwm) here is my board in eagle to use with the smoothie and my laser build, i'm still not 100% happy with the final DC conversion, but its better than i had with the adafruit breakout and the sparkfun level converter. just needs pwm in , 5v ground and out to the PSU PWM


---
**Monte Krol** *July 17, 2016 07:23*

Hi Darren, there are couple of ways to connect the Smoothie. For the laser to fire, the L pin on the PSU must be grounded. The PWM signal then controls the laser strength, including turning off the laser. I don't have mine hooked up like this, though. I have the PWM output on the other side of the small MOSFET at 2.4, and I have the the V-  pin pulled up to the high pin via a resistor (I don't remember how large it is right now, but it's probably 1K or 10K), and I have the V+ side of the MOSFET hooked up to 5V. That effectively makes the PWM output a 5V active-low signal, and I have that hooked up to the L line on the PSU. I left the potentiometer hooked to the laser strength line (where other people hook up the PWM output), and I still use the knob to control the laser strength.



There was a modification done to Smoothie in Feb, where there is now another pin (called the laser_module_ttl_pin, IIRC). That gives a second signal line (and should be inverted, I think, to make it low), that can be connected to the L line on the PSU, and turns on and off at the same time that the PWM output is activated during G1/2/3 moves. I haven't hooked it up yet, but I plan to configure a pin for that TTL output, connect that to the L line, and connect the PWM to the laser strength line. That seems to be the best hookup to me. I'm not sure if my pulled-up MOSFET trick will work on the strength line, though, so I also have a 3.3V-5V level shifter in case it's needed. Hope this helps!


---
*Imported from [Google+](https://plus.google.com/+DarrenSteele/posts/39cTXCfxnSw) &mdash; content and formatting may not be reliable*
