---
layout: post
title: "Mod Day, Yo. Added a mA meter ($5 delivered, eBay)"
date: July 24, 2016 21:46
category: "Modification"
author: "Corey Renner"
---
Mod Day, Yo.  Added a mA meter ($5 delivered, eBay).  Made some feet that grab so that the laser doesn't fall off the narrow table when bumped (again).  The outriggers prevent front-back motion and one foot has a spring-pin in it that locates left-right so it won't slide side-to-side without being lifted.  Finally, designed a mount for a door interlock switch that mounts to an existing hole and bracket.



![images/41c0cf601535e587ee18a4f44cb84f74.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41c0cf601535e587ee18a4f44cb84f74.jpeg)
![images/c28724b83d4b94ac2d46035edcf82fd9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c28724b83d4b94ac2d46035edcf82fd9.jpeg)
![images/9ce42f3d069d6d335ddf04560c7f57d0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9ce42f3d069d6d335ddf04560c7f57d0.jpeg)
![images/1ae064dcecb0a3cbb7e7d8416562683b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ae064dcecb0a3cbb7e7d8416562683b.jpeg)

**"Corey Renner"**

---
---
**greg greene** *July 25, 2016 01:18*

would you happen to have a link to the meter ad?


---
**Corey Renner** *July 25, 2016 04:20*

Here you go.  Arrived in about a week.  [http://www.ebay.com/itm/152110017502?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/152110017502?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Jorge Robles** *February 06, 2017 18:44*

Could you share the endstop piece (thingiverse)?


---
**greg greene** *February 06, 2017 18:56*

Looks Good - great Job !!!


---
*Imported from [Google+](https://plus.google.com/116200173058623450852/posts/SF74SYLhMcf) &mdash; content and formatting may not be reliable*
