---
layout: post
title: "Is there a FAQ available anywhere with answers to common questions, like what the correct water temperature should be, max comfortable mA setting, new lens and mirror options...?"
date: July 26, 2015 00:29
category: "External links&#x3a; Blog, forum, etc"
author: "Gee Willikers"
---
Is there a FAQ available anywhere with answers to common questions, like what the correct water temperature should be, max comfortable mA setting, new lens and mirror options...?





**"Gee Willikers"**

---
---
**Jim Coogan** *July 26, 2015 01:11*

There is a Facebook group called Laser Engraving and Cutting that can answer a lot of those questions and they are in the process of creating a document to capture a lot of the information they share. [https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
**Gee Willikers** *July 26, 2015 01:24*

Thank you. I joined that group the other day actually.


---
**Jim Coogan** *July 26, 2015 01:40*

Great.  Between this group and that one you should be able to find out just about anything you need to know.


---
**Jim Coogan** *July 26, 2015 20:24*

You need to be a member of the group since it is a closed group.  Feel free to join.  I am one of the admins.


---
**Jim Coogan** *July 27, 2015 16:27*

If you have a Facebook account just click on this link [https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/) or search for "Laser Engraving and Cutting" in the Facebook search box.  When you get to the page you will see a "Join" button.  As soon as you click it myself or one of the other admins will approve you.


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/UXzbGAo6B5n) &mdash; content and formatting may not be reliable*
