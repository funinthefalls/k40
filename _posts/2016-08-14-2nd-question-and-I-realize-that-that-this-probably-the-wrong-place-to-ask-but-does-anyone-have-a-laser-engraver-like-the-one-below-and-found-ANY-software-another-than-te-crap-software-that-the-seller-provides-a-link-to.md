---
layout: post
title: "2nd question, (and I realize that that this probably the wrong place to ask), but does anyone have a laser engraver like the one below and found ANY software (another than te crap software that the seller provides a link to)"
date: August 14, 2016 19:12
category: "Discussion"
author: "Terry Taylor"
---
2nd question,  (and I realize that that this probably the wrong place to ask), but does anyone have a laser engraver like the one below and found ANY software (another than te crap software that the seller provides a link to) that actually works and is somewhat easy to use?

![images/4cc6f1bdacc661111337dcaa5e9a05d1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4cc6f1bdacc661111337dcaa5e9a05d1.jpeg)



**"Terry Taylor"**

---
---
**Ariel Yahni (UniKpty)** *August 14, 2016 19:19*

if it uses grbl then [http://laserweb.xyz](http://laserweb.xyz)﻿


---
**Terry Taylor** *August 14, 2016 19:39*

Hmmm, Had not thought of that, there is a GRBL firmware for it.


---
**Jonathan Davis (Leo Lion)** *August 14, 2016 19:41*

I have such a unit 


---
**Eric Flynn** *August 14, 2016 22:42*

Your not trying to engrave those anodized plates on that are you?


---
**Jonathan Davis (Leo Lion)** *August 14, 2016 22:43*

**+Eric Flynn** hopefully not because it requires a higher grade laser to do that!


---
**Terry Taylor** *August 14, 2016 23:57*

I have no intentions of trying to do aluminum on Banggood laser.


---
**Terry Taylor** *August 14, 2016 23:58*

Johnathon, what software are you using to drive the Banggood laser?


---
**Jonathan Davis (Leo Lion)** *August 15, 2016 00:01*

**+Terry Taylor** BenBox aka the provided software. Currently though I'm having a hard time getting my unit working since the day it was delivered, three days to be exact. 


---
**Randy Randleson** *August 15, 2016 15:17*

Im really not trying to be a smart ass, but is there a forum or group for these? I have a 10W version (not benbox)


---
**Jonathan Davis (Leo Lion)** *August 15, 2016 15:41*

**+Wayne Moore** Benbox.us i believe.


---
**Eric Flynn** *August 15, 2016 16:12*

**+Jonathan Davis**  Yes, you will require a "higher grade" laser to engrave on those aluminum plates.  The machine you have there will not phase them.  You could have spent $100 more and got a K40 and you would have been good to go.


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/GkL6ZhaJ7Fr) &mdash; content and formatting may not be reliable*
