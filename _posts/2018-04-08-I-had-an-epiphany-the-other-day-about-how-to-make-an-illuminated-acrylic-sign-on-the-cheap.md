---
layout: post
title: "I had an epiphany the other day about how to make an illuminated acrylic sign on the cheap"
date: April 08, 2018 21:58
category: "Object produced with laser"
author: "HalfNormal"
---
I had an epiphany the other day about how to make an illuminated acrylic sign on the cheap. It uses a tealight as the illumination source. They make ones that do not flicker like a candle and they also come in different colors. I made a very quick and rough proof of concept. Sorry about the poor quality of the pics.

 



![images/dd6da7fcbe33ee2cf42b95a1749f962a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dd6da7fcbe33ee2cf42b95a1749f962a.jpeg)
![images/2129e08ac0e484107bd92162bb9a2a4b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2129e08ac0e484107bd92162bb9a2a4b.jpeg)
![images/5c56ef5da8d8c053e3d3995960a796f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c56ef5da8d8c053e3d3995960a796f1.jpeg)
![images/a130bfdb594a05972d6262d6ff6d4970.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a130bfdb594a05972d6262d6ff6d4970.jpeg)
![images/8e18e9b9ee52638bbfaeb55c1d879c30.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e18e9b9ee52638bbfaeb55c1d879c30.jpeg)

**"HalfNormal"**

---
---
**Ned Hill** *April 08, 2018 22:03*

Hmm cool idea 😀


---
**Travis Sawyer** *April 08, 2018 23:57*

Neat! And cheap! ( My kind of make!)


---
**Don Kleinschnitz Jr.** *April 09, 2018 12:12*

Now all you need to do is connect it to Alexa!


---
**West Ferris** *April 10, 2018 11:34*

What are the specs on your materials? Where did you source the tea light? What type of wood did you use for the base?

What type of acrylic did you use and thickness. Thanks.


---
**HalfNormal** *April 10, 2018 12:33*

I used some basic 3/8th ply, tealight from the dollar store and some 4 mm acrylic. All except the tealight came from the hardware store.


---
**West Ferris** *April 11, 2018 09:06*

Thanks


---
**Travis Sawyer** *April 17, 2018 20:31*

**+HalfNormal** - Settings for the acrylic?  I've cut some before, but not engraved.  Your idea has spawned an idea and I'm headed to the hardware store :)


---
**HalfNormal** *April 17, 2018 20:55*

It is best to cut it slow and low power. If you notice the cracks in my test, it's because I was rushing and ran it hot and fast.


---
**Travis Sawyer** *April 18, 2018 00:50*

Thanks **+HalfNormal**    This is coming right along!![images/f8d57ee910a58d186c93f1793526814d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f8d57ee910a58d186c93f1793526814d.jpeg)


---
**HalfNormal** *April 18, 2018 00:56*

**+Travis Sawyer** Looks great!


---
**Travis Sawyer** *April 18, 2018 01:33*

'better' background...![images/bd04c9f77e222a32a82a21d702d46471.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bd04c9f77e222a32a82a21d702d46471.jpeg)


---
**HalfNormal** *April 18, 2018 02:45*

**+Ned Hill** Looks like you have some competition! 


---
**Travis Sawyer** *April 18, 2018 02:58*

Haha! Just Brothers from another mother!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/ZjE5AVLbgkE) &mdash; content and formatting may not be reliable*
