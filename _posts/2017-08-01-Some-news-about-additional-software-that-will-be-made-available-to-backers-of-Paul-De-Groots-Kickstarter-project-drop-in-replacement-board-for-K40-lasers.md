---
layout: post
title: "Some news about additional software that will be made available to backers of Paul De Groots Kickstarter project - drop in replacement board for K40 lasers"
date: August 01, 2017 02:32
category: "Discussion"
author: "Anthony Bolgar"
---
Some news about additional software that will be made available to backers of Paul De Groots Kickstarter project - [https://www.kickstarter.com/projects/2118335444/gerbil-the-open-upgrade-for-your-k40-laser](https://www.kickstarter.com/projects/2118335444/gerbil-the-open-upgrade-for-your-k40-laser) drop in replacement board for K40 lasers.



The creator of Laser Ink ( [https://www.kickstarter.com/projects/1923304356/laser-ink/description](https://www.kickstarter.com/projects/1923304356/laser-ink/description) ) will be making his software available for free to backers of the Gerbil board.



Great news, so his board will have the option of using Inkscape plugins, LaserWeb, or Laser Ink. More choice is always better!



I have been running a beta of Paul's board in one of my K40's and I am impressed with the quality of laser rasters it produces.﻿





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *August 01, 2017 14:10*

#K40ToolChain


---
**E Caswell** *August 02, 2017 13:52*

I have already put my backing to it. It sounds like a great project and look forward to seeing how it progresses.  

Would be nice to see it make its target, 3 days may be a little too much to make tho.. 




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/63Xdfy8wUAj) &mdash; content and formatting may not be reliable*
