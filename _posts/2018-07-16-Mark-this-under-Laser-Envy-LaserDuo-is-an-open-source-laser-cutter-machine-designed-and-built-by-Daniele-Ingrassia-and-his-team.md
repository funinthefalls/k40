---
layout: post
title: "Mark this under \"Laser Envy!\" LaserDuo is an open source laser cutter machine designed and built by Daniele Ingrassia and his team"
date: July 16, 2018 14:47
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Mark this under "Laser Envy!"



LaserDuo is an open source laser cutter machine designed and built by Daniele Ingrassia and his team. LaserDuo integrates two different laser sources as key feature, being able to laser cut and engrave a wide range of materials with just one machine having available a CO2 and a Yag laser under the hood.



[http://laserduo.com](http://laserduo.com)





**"HalfNormal"**

---
---
**James Rivera** *July 16, 2018 17:18*

The control boards look pretty decent.



[github.com - An improved & fabbable family of microcontroller boards. · GitHub](https://github.com/satshakit/)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/fHgfcBDohBC) &mdash; content and formatting may not be reliable*
