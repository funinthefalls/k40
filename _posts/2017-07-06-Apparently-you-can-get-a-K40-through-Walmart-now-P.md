---
layout: post
title: "Apparently you can get a K40 through Walmart now :P"
date: July 06, 2017 13:27
category: "Discussion"
author: "Ned Hill"
---
Apparently you can get a K40 through Walmart now :P 





**"Ned Hill"**

---
---
**Kirk Yarina** *July 06, 2017 13:47*

With 2 out of 3 reviews identical fakes, and it's not actually from Walmart.  Trips the run away button


---
**Ned Hill** *July 06, 2017 13:57*

Yeah it's by one of their other party sellers.  Walmart trying to do the Amazon thing.


---
**Nate Caine** *July 06, 2017 14:04*

And they even have the low end $349 model:



Pro-Tech 12"x 8" 40W CO2 Laser Engraving Machine Engraver Cutter with Exhaust Fan USB Port



[https://www.walmart.com/ip/Pro-Tech-12-x-8-40W-CO2-Laser-Engraving-Machine-Engraver-Cutter-with-Exhaust-Fan-USB-Port/133397810](https://www.walmart.com/ip/Pro-Tech-12-x-8-40W-CO2-Laser-Engraving-Machine-Engraver-Cutter-with-Exhaust-Fan-USB-Port/133397810)

![images/22e1c9cf10d5d4af8eafd8475259549f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/22e1c9cf10d5d4af8eafd8475259549f.jpeg)


---
**Nate Caine** *July 06, 2017 14:07*

Replacement Tubes too!



Check out the laser tube's exit-port detached heat exchanger.  

<i>(Pre-damaged for your convenience!)</i>



Pro-Tech 40W CO2 Laser Tube for Laser Engraver Cutting Engraving Machine



[https://www.walmart.com/ip/Pro-Tech-40W-CO2-Laser-Tube-for-Laser-Engraver-Cutting-Engraving-Machine/569435370](https://www.walmart.com/ip/Pro-Tech-40W-CO2-Laser-Tube-for-Laser-Engraver-Cutting-Engraving-Machine/569435370)







![images/3b6541d6e0cb461e1f4c035ca493cb30.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b6541d6e0cb461e1f4c035ca493cb30.jpeg)


---
**Steve Clark** *July 06, 2017 18:06*

A "care plan" on the cheaper one??? Wonder how that works and what is covered? Grin


---
**Ned Hill** *July 06, 2017 23:35*

**+Steve Clark** Seems like pretty much everything.  I don't think that know what they are getting themselves into :)  

![images/64e0e952e82df0b2ac8131e219c1f4fd.png](https://gitlab.com/funinthefalls/k40/raw/master/images/64e0e952e82df0b2ac8131e219c1f4fd.png)


---
**Ned Hill** *July 06, 2017 23:40*

Probably not the tube through since it would most likely be considered a consumable.




---
**Ned Hill** *July 06, 2017 23:44*

![images/e692c696f2ab28f1abcd3c06e66330fd.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e692c696f2ab28f1abcd3c06e66330fd.png)


---
**Ned Hill** *July 06, 2017 23:45*

![images/2897f8a29472fb97e0f0cb8299f0ba1f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2897f8a29472fb97e0f0cb8299f0ba1f.png)


---
**Ned Hill** *July 06, 2017 23:45*

![images/468c6f75652a6ece78b8fead95a410b9.png](https://gitlab.com/funinthefalls/k40/raw/master/images/468c6f75652a6ece78b8fead95a410b9.png)


---
**Madyn3D CNC, LLC** *July 14, 2017 16:02*

Let's see how long until they realize what kind of mess they're getting into with that "care plan". 


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/Q3MEAGpQaDR) &mdash; content and formatting may not be reliable*
