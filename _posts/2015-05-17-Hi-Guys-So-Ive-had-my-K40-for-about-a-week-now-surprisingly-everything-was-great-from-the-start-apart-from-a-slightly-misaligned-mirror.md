---
layout: post
title: "Hi Guys! So I've had my K40 for about a week now, surprisingly everything was great from the start apart from a slightly misaligned mirror!"
date: May 17, 2015 12:48
category: "Software"
author: "april hbaika"
---
Hi Guys!

So I've had my K40 for about a week now, surprisingly everything was great from the start apart from a slightly misaligned mirror! Now I have an issue. I can open LaserDRW, but cannot get it to communicate with the laser. I get no error message about the USB, I have checked that the computer recognises the USB, but I cannot get the release rail, reset button etc. to work and cannot preview or engrave anything now.  Also, I cannot get the Corel Laser plugin to work at all. Any suggestions?





**"april hbaika"**

---
---
**David Richards (djrm)** *May 17, 2015 12:53*

Greetings April, I think there may be problems if you try to use an OS newer than Windows XP, does this apply? I may be wrong. DO you have the dongle fitted/installed? Kind regards, David.


---
**Jim Root** *May 17, 2015 13:11*

Mine worked fine for a short while also then it started moving real slow. Different symptom I know but it turned out that the wrong model and serial number were entered in the software. Once that was corrected it's been working fine. 






---
*Imported from [Google+](https://plus.google.com/108876043640002834129/posts/WzcoXMRnrC5) &mdash; content and formatting may not be reliable*
