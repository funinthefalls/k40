---
layout: post
title: "I recently purchased a pattern on Etsy"
date: March 07, 2018 23:18
category: "Hardware and Laser settings"
author: "Joel Brondos"
---
I recently purchased a pattern on Etsy. I reduced its original size to fit the 300 x 200 mm K40 workspace.



When I cut it out and tried to put it together, the parts didn't fit (interlocking nubs too big to fit into slots). I contacted the designer who suggested that reducing the pattern may have also affected the size of the "kerf," described as the width of the laser cut.



Am I right in understanding that the default "kerf" is 0.1?



I know I've used a smaller setting than that, but can it be made thicker . . . is that what the "kerf offset" setting is for in the CUT settings?



Also, I'm just curious how exactly the laser beam is made thicker or thinner (if that is what is happening) because I'm not aware of any mechanical focusing or iris in the unit which could do such a thing.





**"Joel Brondos"**

---
---
**Joe Alexander** *March 07, 2018 23:43*

the issue would be that if the pattern was designed for lets say 300mm x 500mm using 3mm plywood and you highlight it all and proportionally shrink it down to fit, the width of the slots and nubs will end up being smaller than the thickness of the material and will impede them from joining. you need so size it down without reducing the nubs and tabs, then relocate them(cut the size from body sections and not from the interlocking tabs). This is not including kerf. Another workaround is to cut parts out in groups at the original size rather than all at once, assuming the largest piece isnt already too large to do that.


---
**Ned Hill** *March 07, 2018 23:57*

Yeah you would need to resize the slots to fit the thickness of the wood after you scale the piece. 


---
**Nate Caine** *March 08, 2018 18:31*

I've added a post (Dinosaur pic) that somewhat addresses this.


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/YMCfm3YwUYV) &mdash; content and formatting may not be reliable*
