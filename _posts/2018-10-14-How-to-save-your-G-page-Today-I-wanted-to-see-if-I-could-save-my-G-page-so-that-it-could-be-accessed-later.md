---
layout: post
title: "How to save your G+ page. Today I wanted to see if I could save my G+ page so that it could be accessed later"
date: October 14, 2018 17:24
category: "Discussion"
author: "HalfNormal"
---
How to save your G+ page.



Today I wanted to see if I could save my G+ page so that it could be accessed later. The good news is that yes it can be done. You can also save other sites if you want. You are only limited by your storage availability.



Here is the program I used. It is freeware. I am sure there are other programs available for use.



[http://www.httrack.com](http://www.httrack.com) 





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *October 14, 2018 18:50*

What exactly does it save?


---
**James Rivera** *October 14, 2018 19:10*

Maybe the internet archive (a.k.a. the wayback machine) can be used?

[archive.org - Internet Archive: Wayback Machine](https://archive.org/web/)


---
**HalfNormal** *October 14, 2018 19:14*

**+Don Kleinschnitz Jr.** It saves the G+ site as is locally. When it is backed up, you will be able to interact with it like you would the actual G+ page except adding additional content. 


---
**HalfNormal** *October 14, 2018 19:17*

**+James Rivera** I checked the internet archive and my page was not there. I said to add it and it only added the last few posts I made.


---
**James Rivera** *October 14, 2018 19:22*

**+HalfNormal** Doh! Oh well...


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/fuR2wnGWidw) &mdash; content and formatting may not be reliable*
