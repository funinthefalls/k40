---
layout: post
title: "Hello! I purchased my K40 about two months ago but only this evening did I get the time to set it up and align it"
date: January 06, 2016 01:17
category: "Materials and settings"
author: "Jc Connell"
---
Hello! I purchased my K40 about two months ago but only this evening did I get the time to set it up and align it. I'd like to make my own wallet and I'm wondering if anyone could suggest the right settings to cut 3-4 oz. leather?





**"Jc Connell"**

---
---
**Scott Marshall** *January 06, 2016 02:14*

Welcome. 



There's a couple of fellas here doing belts and such, I'm sure you'll hear from them. 

If you're in a hurry, I'd start mid scale and work up. Make sure it's not "genuine plastileather" and go for it. 

From what I hear the leather takes a fair amount of power to engrave, but the examples I've seen look great. 

Ventilate well (stock fan is NOT adequate) and an air assist nozzle will not only cut better, but keep the lens clean.



Have fun.

Scott


---
**ChiRag Chaudhari** *January 06, 2016 04:48*

You should definitely check out **+Yuusuf Sallahuddin**. He does some pretty amazing work with Lather. Also he is running the machine stock, so he is your guy.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 06, 2016 04:49*

Hi Jc. I use the K40 to cut leather for wallets and other leather products I do. First thing, check your lens orientation. Mine actually came in the machine upside-down (you should be able to see your reflection on the top of the lens). If the lens is upside-down, you will have nothing but trouble trying to cut. I am not familiar with the "oz." measurement for leather, as I work in mm for the thickness of the leather.



However, from looking at [http://www.brettunsvillage.com/leather/conversions.html](http://www.brettunsvillage.com/leather/conversions.html)

I am seeing that 3-4oz is around 1.2-1.5mm thickness.



I am actually just going downstairs to grab some stuff, so will check my test piece when I am down there so I can give you the settings for roughly that thickness.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 06, 2016 05:12*

[https://drive.google.com/file/d/0Bzi2h1k_udXwWmFJYkdDVWtCSHM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWmFJYkdDVWtCSHM/view?usp=sharing)

Here is a sample I've previously done with leather. 2mm thickness cowhide leather on the left. Settings for cutting were ~7mA power with 6, 7 or 8 mm/s cutting speed. ~1mm thickness kangaroo hide leather on right. Settings for cutting were ~7mA power with anywhere from 10-15 mm/s cutting speed being fine.



I recommend taking small offcut/scrap of your leather you are working with & doing some tests with different power settings for cutting & engraving. Keep these scraps for your future reference.


---
**Jc Connell** *January 07, 2016 16:32*

Wow! Thank you. I didn't check this for a few days. Thank you so much for the detailed response. The leather you posted on the left hand side looks nearly identical to what I have on hand. So you are cutting with a stock laser cutter, correct?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2016 17:37*

**+Jc Connell** Yeah basically my laser cutter is stock. Only modification that has been made is that I replaced the cutting table with a piece of perforated steel, & I have attached a makeshift air-assist (using what is basically an aquarium air pump run to a ball-inflation needle to focus the air directly at the cutting point). Aside from that, everything is stock. If you have any issues, someone in this group will usually have an answer (or some suggestions at least). Happy lasering.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2016 17:47*

**+Scott Marshall** Actually, I mustn't have read your response here correctly the other day. Just re-read it. You mention that from what you've heard leather takes a fair amount of power to engrave. Maybe you meant cut, because engraving is extremely easy. I am working with 0.8-1mm kangaroo hide (which is a lot stronger than same thickness cowhide) & I am having trouble turning the power low enough when engraving to prevent burning all the way through it haha. I use pretty close on 4mA power & 500mm/s engrave speed & have actually had to raise the items closer to the lens to get it out of correct focal length or it literally burns 3/4 the way through the piece (making it weaker & not what I want).



**+Jc Connell** Also, for your reference, since I probably didn't mention, engraving on the leather is something you will want to run various tests on, due to difference in outcome. I took a 300mm x 200mm piece of leather & just test engraved the same thing over & over with different power settings, speed settings, pixel step settings. I tend to always engrave with 500mm/s speed, and from there I adjust the power as I see fit. Usually 4-6mA power gives a nice engrave, however sometimes I tested the pixel steps option (normally 1, sometimes I tested 2,3,4,5 pixel steps). Pixel steps seems to speed up the process (because it seems to laser less pixels) & gives a fainter engrave, as each laser dot is spaced apart more. It also causes a loss of detail, so no good for certain things where detail is important. Again, testing on scrap or offcuts is the best way to determine what gives the look you wish to achieve.


---
**Jc Connell** *January 07, 2016 17:57*

I'm actually having a very different experience cutting the leather when compared to yours. I'm wondering if I haven't properly aimed the machine. I'll have to double check the settings I've used when I get home but I recall using approximately the same power and speed you've mentioned except it resulted in an engraving maybe 1/4 or less deep.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2016 18:29*

**+Jc Connell** Is that using Cut @ 7ma & 7mm/s or so? If so, check your lens in the laser head. I couldn't get mine to cut for about 2-3 weeks. I would do like 10 passes to get it to cut (extremely charred & messy). After checking the lens and finding out that it was upside-down (& subsequently putting it the right way up), it cuts much easier. Also, make sure your leather piece is at the focal length (I believe ~50mm for most stock machines). So your leather needs to be positioned at that distance. I was out by ~4mm at one point (after removing the stock cutting table) & it refused to cut yet again. Too close or too far from the base of the lens will cause issues cutting. Just a few things to keep in mind.


---
**Jc Connell** *January 07, 2016 20:33*

How do I identify the correct orientation of the lens? I believe the convex portion is currrently facing the work area.


---
**ChiRag Chaudhari** *January 07, 2016 21:58*

Convex up and concave down. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2016 23:37*

Here's a quick diagram I just did up for the lens orientation, to explain what is going on too.

[https://drive.google.com/file/d/0Bzi2h1k_udXwZy1CREpKeG9NeGc/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZy1CREpKeG9NeGc/view?usp=sharing)



I'll post it onto the K40 group somewhere so that newbies can all find it.


---
**Jc Connell** *January 07, 2016 23:55*

Wow thanks! Mine was also upside down from the factory. Putting in some test figures now. I also cleaned all the mirrors again. Hoping for a significant improvement.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2016 23:57*

**+Jc Connell** Hope it works well for you this time. I noticed a significant improvement. It was like a hot knife through butter when the lens was oriented correctly.


---
*Imported from [Google+](https://plus.google.com/+JcConnell/posts/NcWs8qVtvDG) &mdash; content and formatting may not be reliable*
