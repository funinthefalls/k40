---
layout: post
title: "Compressor for air-assist: First I've bought a big air compressor (24l tank, 8 bar pressure), because I thought it should be fine, I can set the pressure..."
date: July 18, 2016 10:19
category: "Air Assist"
author: "Akos Balog"
---
Compressor for air-assist:

First I've bought a big air compressor (24l tank, 8 bar pressure), because I thought it should be fine, I can set the pressure... I had to buy some filter (to avoid water/oil in the air), and some accessories. I've used it with pressure regulated to ~1 bar, but even with this small, constant airflow it had to run too frequent. It was loud, very loud.

So I bought an aco-328, just received it and plugged in to try it. It is more than enough. (Maybe even the aco-318 would have been enough, but I always want the bigger :D)



Yes I know, everybody said, that the small one (aco) is enough, they were right...





**"Akos Balog"**

---
---
**Gary Hamilton** *July 20, 2016 12:11*

I am n the process of the same thing.  I connected my large compressor but I have order one of the small pumps to see how it does.  My goal is to get rid of the filter and make it more of a standalone unit.




---
*Imported from [Google+](https://plus.google.com/106984452297752036237/posts/e2ny76JMVC1) &mdash; content and formatting may not be reliable*
