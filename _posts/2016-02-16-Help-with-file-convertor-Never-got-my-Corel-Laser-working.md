---
layout: post
title: "Help with file convertor? Never got my Corel Laser working..."
date: February 16, 2016 04:45
category: "Software"
author: "Scott Marshall"
---
Help with file convertor?



Never got my Corel Laser working...  I guess the supplied one is corrupt or just a plug in, and I don't own CorelDraw.



Been going from Draftsight (Autocad Clone).dwg 2013R files thru a .dwg to .bmp convertor demo I found on the web. Then to LaserDRW.

It's a PIA kind of workflow, but it gets there for now. 



The bitmap convertor has 30 demo runs and it's $99.95 to buy. I'm down to 10 left. A LOT of cash for a dwg to bmp converter if that's all it does. 



Any suggestions on a free or reasonable priced converter, better way from Draftsight to Laser?



There's got to be a better way, short of changing the board so I can use Mach 3. I'll probably end up there, but not right now.



Thanks, Scott





**"Scott Marshall"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 16, 2016 06:13*

Sometimes you can create another user profile in Windows, then install the demo software in that profile. When trial expires, you can create another user profile & install again to run until it expires. Doesn't work with all software demos, but has done in the past for me & certain demos. Might get you by a bit more until you can find other options.


---
**Scott Marshall** *February 16, 2016 06:31*

Thanks YS,

Might have to try that. I really don't mind paying for software, but this is a pretty simple sort of thing,.



 If it was a suite that covered many conversions, or was a little better at this one - it's VERY particular regarding layers and artifacts, carries no scale information forward, and just plain doesn't work with certain elements - bottom line is it's NOT a $100 program. $20 maybe.. 



I'm an older engineer, from the days of Autocad 11, and not very skilled with the new Sketchup style of 3d Cad programs, so I 'm doing it with what probably amounts to the hard way.



I HAVE asked a few times about Corel and what others are using for a workflow, but havn' t had many answers at all, and no 2 the same. Except that MOST people seem to have loaded the included CorelLaser and are using it with minor problems .



Maybe I'm going about this wrong, need to ask WHAT others are using?



I don't have a problem buying software, drawing, conversion, or whatever is needed, but I'm not hearing from anybody as to what actually works with this setup and don't want to buy something only to find there's no way to use it efficiently. 



It would seem to me that going from Autocad (.DWG or .DXF) to the K40 is done quite a lot.

I just need to find out how.
















---
**I Laser** *February 16, 2016 07:02*

There's been a few links kicking around for the corellaser plugin and coreldraw. Most of these machines come with both.



<sarcasm> The supplied copy of Coreldraw is legit, even comes with the CD key in a text file and instructions not to register </sarcasm> lol



I'm assuming downloading a new copy is way easier than multiple windows profiles.



My workflow is just coreldraw to laser. I was use to Illustrator but made the effort to get use to Corel and can now do most things I need to quite easily. The only format I could successfully export from Illustrator into corel was SVG, but you lose the editing capabilities. :\



There's obviously a lot of open source advocates here, but that means a hardware upgrade.


---
**Brooke Hedrick** *February 16, 2016 07:35*

I use a separately purchased license for Corel Draw and Corel Paint for creating and converting assets.  I import the results into LaserCAD for cutting or engraving.


---
**I Laser** *February 16, 2016 08:06*

**+Brooke Hedrick** do you have an upgraded board? Why not burn straight from Corel otherwise?


---
**Stephane Buisson** *February 16, 2016 09:46*

InkScape will do all sort of conversions for free, you can import DXF R13 and plenty of other formats ;-)) unfortunately not proprietary DWG.

[https://en.wikipedia.org/wiki/AutoCAD_DXF](https://en.wikipedia.org/wiki/AutoCAD_DXF)


---
**Brooke Hedrick** *February 16, 2016 13:28*

**+I Laser**

Yes, I have an upgraded board.  I am using a Lightobject DSP 7 (awc708c lite).  The original MoshiDraw board came with MoshiDraw, quite old Corel Draw, and the Corel plugin.  



I tried MoshiDraw and in addition to being fairly unintuitive, it was very unreliable.  I never used their Corel or Corel plugin.  It didn't seem possible a legal copy of the software was being included at the price of the k40.  I already owned a legal copy and also didn't want to chance their version messing up my legal install.


---
**Jim Hatch** *February 16, 2016 20:21*

Download Adobe Illustrator CS2 and you can open DWG files and export BMPs.


---
**Scott Marshall** *February 17, 2016 03:47*

Thanks for the answers guys. I appreciate it.



I Laser, What I received with the laser was a off the shelf 'Rishend' DVD+R with a bunch of files burned to it, most of them is Chinese only. The rest partly in chinese. NO English language instructions. There are several links to Chinese only websites, including the one for the "liyauyang" (or something like that) security site, also marked on the USB key.



CorelLaser and LaserDRW are the two applications on he disk (that are intelligible to me) LaserDrw works as I supposed it should, jumping into the security and board serial no verification  before starting LaserDRW.



Corelaser loads and installs  with no errors, and whan started goes through the security verifaction, passes, then errors to a window stating "Corel ver>12 not installed. If I wipe all trace of Corelaser and load fresh, reboot, then start it, it goes to a chinese website, shakes hands, dumps me to a "Site not found" error, and thereafter just errors as in the 1st example. I've heard a lot o people mention they have a copy thtat's corrupted by a virus, while I doubt that, but it does  seem there are a lot of bad copies, including mine.

I didn't get the textfile you did - I don't think.



If I could get a good copy from someone, It may work.

I'm not sure if it's small enough to e-mail.



Stephanie, Thanks, I'll look into Inkscape, Draftsight exports DXF, so that's an option for sure. 



Jim I'll look into adobe illustrator CS2, that may solve things for now.



I DID find another, much more professional software package for converting, It's very powerful, but accordingly complicated, it allows all sorts of conversions, full layer and color control, even programmable scaling. It may be a great link to LaserDRW if I figure it out. I's also 100 bucks, but I could see it if it works well.



I'll report back. Going to check on the suggestions, re-investigate my disk.



Thanks All, Scott












---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 17, 2016 05:25*

Not sure if this makes sense **+Scott Marshall**, but there was a CorelDraw12 install on my cd I got sent & there was also CorelLaser install. CorelLaser is just a plugin for the CorelDraw12. So you need to install CorelDraw12 first, then install CorelLaser plugin. Running CorelLaser (after install) opens CorelDraw, with the plugin active. Running CorelDraw (after install) just runs CorelDraw (without the laser plugin).



There was a .rar file (if I recall correct) that contained the CorelDraw12 software. Not sure if it is virus, but I've had no issues with it (to date).


---
**I Laser** *February 17, 2016 05:35*

Yeah the DVD+R is standard, both my machines came with one. There was a video file in Chinese I found and followed that to install the software and plugin. The only problem I had was the board they show you to select was not right.



I've read about people installing the software and it taking them to a website. That's really not right, as long as the USB dongle is in the back it should work.



I'll see if I can find a link for you, I posted one from another member yesterday but can't vouch for the contents though...


---
**Scott Marshall** *February 17, 2016 09:19*

**+I Laser** 

Thanks, whatever you have, it works better than mine. If my system acts thwe same with your copy, I know it's something on my end, kin of doubt it though


---
**Scott Marshall** *February 17, 2016 09:33*

**+Yuusuf Sallahuddin** 



I was wondering if what I had is only a plaug in. That's how it behaves. its seems to load, verify and run until it goes to link, then tells me I don't have Coreldraw>ver 12 (which I would expect is what the plug in is designed for).



I opened the "CorelDraw 12-english .rar" file and it appears to be half gibberish with the words "mocrosoft 7" appearing now and then. it doesn't look to be an application or executable file.



I'd post the directory if I could figure out how to put up a .jpg here..














---
**I Laser** *February 17, 2016 09:55*

Yuusuf is on the money then, you definitely need coreldraw installed first.



Do you have winrar (or similar) installed to extract the archive contents? It's what's called a compressed archive, it needs to be decompressed first. You should then find a normal folder structure and an executable installation file aptly named setup.exe.



I looked at uploading the software that was provided with my first machine (haven't touched the software provided with the second machine) but it is 740mb, uploading with my 256k upload connection would take some time. Though if you are desperate I will upload it! Just let me know. ;)


---
**Scott Marshall** *February 17, 2016 10:28*

I use "freeviewer" to open.rar files. Seems adequate.



I think the 2 files being discussed here are NOT 2 copies of Coreldraw, but Coreldraw, and the CoreLaser which is the plugin that allows it to output the code for the K40 board. 



Nobody seems to know what that is, I'm thinking it's a modified Gcode, and suspect all that is needed it the info on it so one can write a postprocessor routine for Mach 3 or any of the better CNC programs to control it directly. 



In any case, it looks like my copy of Coreldraw is No bueno, but the plugin probably is ok.



If could can easily send me a copy, it would be greatly appreciated. Scott594@aol.com






---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 17, 2016 15:45*

I'll try find my CD tomorrow (it is nearly 2am & going to bed now) & take a look for the contents (CorelDraw12, CorelLaser, etc). I'll see how large it is & if I can put it into dropbox or something.


---
**I Laser** *February 17, 2016 22:20*

Mine's roughly 740mb, which on my crappy ADSL will take numerous hours and cripple my net in the meantime. I meant to set it off uploading last night when I went to bed but forgot sorry! I'll attempt to upload tonight if Yuusuf is unable to provide a copy.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 17, 2016 22:33*

My CorelDraw12.rar is 686.8mb, I am currently uploading to Dropbox. Once completed I will share the dropbox link here.

edit: according to dropbox software, approximately 1 hour until it completes.


---
**I Laser** *February 17, 2016 22:37*

You must have NBN or ADSL2+, I'm still stuck on a crappy ADSL Telstra resold service. <b>Cries into his weetbix</b> :'(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 17, 2016 22:49*

**+I Laser** I wish I had NBN, but I do have Telstra Cable service (& I'm less than 500m from the exchange).



**+Scott Marshall** I am adding your aol email address to the list of people that the folder is shared with. I am currently uploading, so it won't be completed for a while. I have copied the entire contents of the CD that I received with my K40 to the Dropbox folder. Let me know if any issues accessing the files (after roughly an hour from now).


---
**Scott Marshall** *February 17, 2016 22:58*

Sounds good to me. You guys lost me about a block back on the alphabet soup. I've got no idea what format my signal leaves as, but it seems it may be a mite faster than you guys are hooked to.

Believe it or not, I used to design and manufacture CATV gear, back in the Trap (filter) days of a mere 30 or 40 channels. (a mere 30 years ago) Somewhere along the line I stopped paying attention and the technology left me in the dust.



Anyways, thank you, both of you. I appreciate it.



Everytime I plan on working on the software situation, I get tinkering with the mechanics. 



  


---
**I Laser** *February 17, 2016 23:22*

**+Scott Marshall** ha, if it works you really don't need to know! ;)



Lets put it this way, my download speed is 1.5Mbps (ignoring the peak congestion times!), apparently the average connection speed in the states is nearly ten times faster (12Mbps). 



The NBN (national broadband network) was a scheme that would have seen pretty much the whole of Australia on fibre optic to the premises. Problem was they made the decision to built it into low density rural areas first, which provided practically no ROI. When the conservative government was elected they saved money by 'nerfing' it. End rant sorry lol!



**+Yuusuf Sallahuddin** and I wish I was on cable! First world problems lmao!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 18, 2016 00:29*

**+I Laser** Totally true about the nerfing of the NBN. Ridiculous plan that failed. Should have put it into higher density areas in first place (as those people would actually use it & give ROI). I get around 12mbps down & 1mbps up.



**+Scott Marshall** I walked away from the computer before & it went to sleep mode, so it's still uploading now. I will continue uploading now & will drop another message here once it's completed & you should be able to access the software then.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 18, 2016 01:27*

**+Scott Marshall** If you check the Dropbox folder it should now be accessible. I imagine that an email was sent to your AOL notifying you that I gave you permissions to the folder. Hope it works for you.


---
**Scott Marshall** *February 18, 2016 03:06*

I got your files Yuusuf,  I appreciate the effort. 

It seems to be the same as my disk, and behaves exactly the same. 



The only instructions I can glean from it all tell to follow the installer, and the video works ( 4 wmv - That's different from my disk) and that shows a silent install process which I can't really follow. The Icons on the video screen are unreadable. 



The only installer I can find is part of the drivers file, and when I select the install button, it tells me the drivers are pre-installed.



When I select "Coreldraw.exe" (I did a full erase of all Coreldraw files (I could find) 1st), it goes to a "Allow changes" warning, I give it permission, and it did the jump to website thing, ending up briefly at the liuayang site, then jumping to a "site not found" screen.



It behaves exactly as before. 



I am running as an administrator, so that's not it.



I'm at a loss, it may be an incompatibility thing, or I am missing something  obvious.



My machine is Win 7 Pro 64bit Amd Kaveri precessor running 16gb memory. 



Am I missing an unpacker/installer?

 Laserdrw installed flawlessly from the .exe file.



That's how I'm installing Coreldraw, it goes thru the install, asking for language choice, file location etc, then to the website and crash.



After the 1st time, it goes straight to a permission screen, then to "Coreldraw>ver11 not installed"



Right now, I believe it needs something from the website it keeps acessing.



I could wipe everything, laserdrw, laserkey, and the whole file system and try again, but I'm afraid I'll ose what functionality I have.



Sorry to hear about your connection issues. I live in the rural US and suffered for years with agonizingly slow dial up. 

I forget most of Australia makes our "rural and remote  " villages places seem like a big city by comparison.



I'll keep trying on the Coreldraw, at least I know it's on my end now. It wasn't wasted effort. Thanks.



Scott






---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 18, 2016 03:57*

**+Scott Marshall** Well that is kind of crap that it didn't work. Inside the CorelDraw12-english.rar file there should be an installer file for CorelDraw12. Did you manage to find that?


---
**I Laser** *February 18, 2016 04:10*

You might need to restore your system back to a point before you initially tried to install Corel. Sounds to me like there are still residual components on your PC from the initial installation (assuming of course that Yuusuf never had the website popup).



Your system is beefier than what I'm using (win7 home, e8400, 8GB ram, etc) so unlikely your PC. Although still possible, would be interested to hear from others that had issues with it, might be it doesn't like certain CPU's, chipsets, etc?



By the way, don't let the chinese characters freak you out, with a bit of pausing it is possible to follow that installation video. Just compare their screen with your English version, all the folder contents, dialog boxes, etc are set out the same. ;)



And yes Australia is the broadband backwater of the developed world!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 18, 2016 04:13*

I don't recall having any popup website when I originally installed it, however it has been quite a while since I did that.



Here is a screenshot to what contents should be in the CorelDraw12-english.rar file.



[https://drive.google.com/file/d/0Bzi2h1k_udXwMWhCOFF2TzVEZVE/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwMWhCOFF2TzVEZVE/view?usp=sharing)


---
**Scott Marshall** *February 18, 2016 06:57*

Got it!!



Turns out my .rar reader was only showing about 1/3 of the files. Freeviewer, it's not worth the price. (free)



Anyway, Once you showed me the screenshot, I was on the hunt. Didn't take long after that for me to figure out the rest. 



We both have the same serial numbers by the way, it must be it's the ONE they send to everyone.



Looking through it, it says it imports a wide variety of files, including Autocad DWG and DXF, although I couldn't get it work with a quick try, I' expect I will when I have more time to spend on it. 



I drew a circle, and was able to get it to transfer to the cutter, but the speed is way out of calibration. It took about 4 minutes to trace a 50mm circle set at 18.00 mm/sec, but I'll figure that out too.



It was my mistake, (sort of) after all. 



I think I'll write a quick How-to on setting it up and post it for future victims.



Thanks, Yuusuf, and I Laser for sticking with me.



I really appreciate it,  you saved me a lot of time, grief, and possibly money.



Hope to return the favor sometime.

Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 18, 2016 07:11*

You might find that you need to change the Controller Board ID in the Corel Laser plugin settings somewhere, as for me it was set to incorrect model to begin with & caused issues like cutting/engraving really slow. You'll find the correct model # on your actual controller board (that the usb cable plugs into). Might fix up the issue with the speed being out of calibration.



You're welcome for the help. I think a lot of us went through the same problems to begin with & this group has been very valuable in learning/troubleshooting issues for me. Someone in the group generally has experienced similar problem/s before & has work-arounds to deal with it.


---
**Scott Marshall** *February 18, 2016 07:28*

Yes, I had had to set up the serial number and such to get LaserDrw working properly. There was also a scale issue, I forget the details, but it's something I can work with. Corelaser looks to use the same driver software.



I agree, this is a valuable group, i'm not real fond of the engine, but the people are 1st class .



I hate being dead ended like that, when you're just at a stop and haven't even got anything to try.



I'm sure I'll get the settings sorted out, when I'm better rested, (it's MY turn for the 2am bailout, from the other side of the world). Helping and being helped by others like this is one of the true pluses of the information age.

 We've come a long way since I was a Tv repairman repairing tube TVs back in the '70s.

 Wonder what I'd have thought back then if someone had told me that someday,  I'd be setting in my kitchen discussing my personal laser cutter on my personal computer with my friend in Austrailia.



Wonder what my sons will see in 40 years?



Thanks again, and good afternoon, or is G'day?







Scott










---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/DtV4Hx4ifaS) &mdash; content and formatting may not be reliable*
