---
layout: post
title: "I got a problem. I've lost an awful lot of power"
date: June 01, 2017 01:31
category: "Original software and hardware issues"
author: "Chris Hurley"
---
I got a problem. I've lost an awful lot of power. I tried to install a forced air head from light object but the lenses were not the same size. I tried to get the lense in the middle of the holder on the new head. But I had issues with the air supply so I had to uninstall it until I can get some drag chain. Now after calibrating the mirrors I can't cut anything. 





**"Chris Hurley"**

---
---
**greg greene** *June 01, 2017 01:44*

The air nozzle head is longer than the original one, your beam is bouncing off the inner sides of the air nozzle.  Realign it by adding washers to the head mount on the carriage until the beam can exit the  long nozzle without hitting the sides.


---
**Chris Hurley** *June 01, 2017 01:45*

I've actually gone back to the stock head. Still the same issue. 


---
**greg greene** *June 01, 2017 01:52*

If you didn't have a problem with the stock head, and now have gone back to it and there's a problem, what's changed?

1. You installed the lens bump down not up ?

2. you twisted the head mount so the beam is not entering it in the center and may now be striking the head itself instead of the hole.



When you stop and think about the possible changes to the system you may have introduced - it will help you find your problem.  I'm assuming here that when you changed heads you did not alter the alignment of the mirrors.  If you did, start at the beginning of the alignment procedure and do it all again.  :)

Good luck.


---
**Chris Hurley** *June 01, 2017 02:40*

Checked the mirrors. Lens is bump up (I can see my reflection) . Could it be the water is the issue? Do I need to change it? 


---
**greg greene** *June 01, 2017 12:59*

No, that wouldn't affect it unless it is a long run.  These issues pretty much all come down to beam alignment, what procedure did you use?

 The flying wombat web page has a good one.


---
**Ned Hill** *June 01, 2017 14:41*

It's actually the floating wombat guide ;)  [plus.google.com - - Laser Alignment Guide - With all the laser alignment questions we get, here...](https://plus.google.com/108257646900674223133/posts/LGfT6SS3Tcc)


---
**Joe Alexander** *June 01, 2017 14:43*

**+Ned Hill**thanks I always get it wrong also(keep thinking rabid wombat) 


---
**Chris Hurley** *June 02, 2017 01:48*

ok done better but still not what it was before. I really wish there was a "if you break X this happens" section here. From now on if something comes up I'm making that file. 



So when the "new"head was replaced and it didn't work out I kinda just walked away from the machine for 3 or 4 weeks. Could that have effected it? 




---
**Joe Alexander** *June 02, 2017 03:29*

if your water is also that old I would change that just from habit. Power leakage into the water can be an effect but I don't think that's the issue here(at least not entirely). i know my focus point is like 24mm from the bottom of the nozzle(i have an aluminum shim i use to check pieces) so definitely check that. You can also remove the bottom cone part and see if that remedies it(if it does then its beam clipping in the cone). Observe your Co2 tube when energizing. Does the beam deviate to the edge anywhere(usually the ends)? any popping, buzzing, or arcing? At this point I would say shoot a short video for visual comparison.


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/JE3D3CLVGt4) &mdash; content and formatting may not be reliable*
