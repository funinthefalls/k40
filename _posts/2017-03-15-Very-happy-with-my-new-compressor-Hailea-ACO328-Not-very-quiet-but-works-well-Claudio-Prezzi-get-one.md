---
layout: post
title: "Very happy with my new compressor (Hailea ACO328) Not very quiet, but works well, Claudio Prezzi get one :)"
date: March 15, 2017 13:12
category: "Hardware and Laser settings"
author: "Jorge Robles"
---
Very happy with my new compressor (Hailea ACO328) [https://www.amazon.es/Hailea-SC439-inflaci%C3%B3n-compresor-pist%C3%B3n/dp/B012CT20AY/ref=sr_1_2?ie=UTF8&qid=1489583478&sr=8-2&keywords=aco328](https://www.amazon.es/Hailea-SC439-inflaci%C3%B3n-compresor-pist%C3%B3n/dp/B012CT20AY/ref=sr_1_2?ie=UTF8&qid=1489583478&sr=8-2&keywords=aco328)



Not very quiet, but works well, **+Claudio Prezzi** get one :)





**"Jorge Robles"**

---
---
**Jorge Robles** *March 15, 2017 13:45*

Not pressure, but flow. I have stopped pressure with my finger, still have it :)


---
**Jorge Robles** *March 15, 2017 13:46*

Gonna get a pressure gauge to check anyways.


---
**Jorge Robles** *March 15, 2017 13:49*

Use a freezer one. Silent, good pressure. Almost free on a scrapshop. And get an oil filter, mine was a hippie scooter.


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/V43ZdnWnRdP) &mdash; content and formatting may not be reliable*
