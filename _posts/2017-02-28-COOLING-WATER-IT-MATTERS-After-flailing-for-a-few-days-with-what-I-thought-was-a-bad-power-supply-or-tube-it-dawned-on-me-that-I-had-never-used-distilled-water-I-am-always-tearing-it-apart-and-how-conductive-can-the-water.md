---
layout: post
title: "COOLING WATER \"IT MATTERS\" After flailing for a few days with what I thought was a bad power supply or tube it dawned on me that I had never used distilled water (I am always tearing it apart and how conductive can the water"
date: February 28, 2017 21:38
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
COOLING WATER "IT MATTERS"



After flailing for a few days with what I thought was a bad power supply or tube it dawned on me that I had never used distilled water (I am always tearing it apart and how conductive can the water be anyway?) and I knew that I had some algae in the tank. 



I knew better from listening to others but seriously arching through the glass jacket through the water to gnd in the tank? You better believe it, I have now proven it, to my satisfaction that is. I should have known better because I worked with someone that was blowing supplies every month and it turned out to be CAR antifreeze (that's definitely a no-no).



The symptoms was an intermittent screeching noise when the laser was in operation. It did not do it continuously and it did not do it every time. 



I thought it was coming from the LPS and finally was about to conclude that it was a failing winding inside the fly-back transformer. What bothered me was I could see no evidence of an arc even in a dark room. 



What I did notice was the flux beam shifting and changing shape at the anode end only when it screeched!



So I cleaned the system put in distilled water (3 Gal) and RV antifreeze (1/2 gal), powered it up and from then on no screeeeeeching just smooooth wood smoking power. 



I wonder how many good laser power supplies and tubes have been replaced or fry'd do to this problem. 



By the way these supplies are not designed to withstand many arcs...is this why they are popping?



I wonder how much leakage current there is through the water bucket?



Lesson learned "take your own advice", clean the tank often and use distilled water.









**"Don Kleinschnitz Jr."**

---
---
**greg greene** *February 28, 2017 21:57*

Glad to hear you got it sorted Don !


---
**Abe Fouhy** *February 28, 2017 21:59*

Interesting Don, do you think my PS is good then? I too was just using tap water.


---
**HP Persson** *February 28, 2017 22:13*

Nice to see more users find this out, i have been yapping about the water for long time and got a lot of shit for it :)

And btw, RV-antifreeze has about 400x more conductivity than distilled water, increases the viscosity and lowers the thermal pickup.

It´s not super dangerous, but not good either :)



I would say with confidence that bad water and overpowering is the two top factors why a tube or power supply gives up.


---
**greg greene** *February 28, 2017 22:14*

A few ounces of rubbing alcohol will keep the algae at bay


---
**Abe Fouhy** *February 28, 2017 22:22*

Looks like I better redo this test and put the ps in and test with the "test button"


---
**Andy Shilling** *February 28, 2017 22:55*

I received a new tube last week and it was screeching so I returned it, I'll swap out my water now as I have been using softened water. My last tube didn't do it but it's worth swapping over now I read this.


---
**Don Kleinschnitz Jr.** *February 28, 2017 23:09*

**+HP Persson** Here is some actual data. I don't know why I never thought of measuring this before. I wish I had measured the water before emptying it.

------------------

I used a HM Digital COM 100.

The temp was about 60F

----------------

Distilled water = 2.6 uS

Distilled/RV mix:6:1  = 142 uS

RV antifreeze = 414 uS

Don's tap = 370 uS

Prestone (ethelene glycol) = 633 uS



There is the 400:1 that **+HP Persson** refers to.

I wonder what is acceptable. I will probably replace mine.

I think this is something we all should monitor

.....thanks!


---
**HP Persson** *February 28, 2017 23:22*

Yeah, looks pretty much the same as i found out in my tests. Bottled drinking water was better than RV antifreeze in my tests. Not sure if RV antifreeze was at a level it really damaged anything though.



I did mention in a FB group it would be better to cool the laser with camel urine than RV antifreeze (i was joking), so we actually called the local zoo, got some ph and other values to mimic camel urine in a fluid :P

Thermal pickup was better, but it was conductive as frig and my room smelled like hot yoga pants for hours :P



If you have a camera to shoot slow motion, check out the beam while using RV antifreeze, it will deviate with that too, just quicker and less jumping, while plain distilled doesn´t make it move much at all.

Theoretically this jumping and deflection causes the recombination of gasses to slow down, if it does it enough so we can see it as a tube failure or not, i have no clue about.



Nice findings! there should be more testing and confirmations of tests like this to find the best cooling option, to spare power supplys and laser tubes for us users :)


---
**Alex Krause** *February 28, 2017 23:32*

I use a few drops of algaecide used for aquariums in my distilled water... 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 28, 2017 23:39*

That's interesting. I've just been using rain water from my laundry sink since I got my K40. Although, admittedly I don't use my laser as much as most of you guys (usually only for a couple of minute jobs or max of 1 hour job).


---
**Paul de Groot** *March 01, 2017 00:24*

**+Don Kleinschnitz**​ i think you're right. I noticed this screeching too and found algae in my tank. Just replaced my tube and water. No screeching but I'm definitely going to change the water for distilled water. 


---
**Don Kleinschnitz Jr.** *March 01, 2017 01:05*

**+Yuusuf Sallahuddin** as you know I don't use mine much either. I am guessing it's the algae. Are organisms conductive. 


---
**greg greene** *March 01, 2017 01:05*

Yes all organisms are conductive to a degree


---
**Jim Hatch** *March 01, 2017 03:05*

DowFrost is the antifreeze to use 🙂 Hard to find in small quantities though so you'll have leftovers.



I've gotten the screeching when I let the water overheat (30C). Letting it cool eliminates the screeching. 


---
**Steven Whitecoff** *March 01, 2017 03:25*

I dont believe antifreeze is the only answer. First off, I've only had my laser a 8 days. But after researching it, i found as stated above propylene glycol not ethylene glycol is the proper antifreeze. Further digging indicated that growth of organisms and corrosion prevention are the only issue to be addressed. My solution is to use Redline water wetter.

Its intended to be used in racing engines with water only no antifreeze so its very good at corrosion prevention and heat conductivity. I see no change from just distilled water, but time will tell.

 Why? because I've spent 25 years winterizing boats with proplyene glcol and if left for more than a year you see growth and I believe fermantation as it will smell of alcohol.This effect could be from the plastic tubing used or other causes but rather than guess I avoid it in this application.This is with -50 deg antifreeze which IRC is 40% propylene glycol. Boats were drained of water and straight PG antifreeze fills the entire water system.


---
**Don Kleinschnitz Jr.** *March 01, 2017 03:44*

**+Steven Whitecoff** the problem is not corrosion prevention, its conductivity. 



Apparently the growth of organisms in water clearly increases its conductivity. 



The point we are making is that  RV antifreeze is substantially more conductive than distilled water (algae or not), see the measurements above. 



None of the applications you sited above need to insulate 20,000 from gnd. so that not an apples to apples comparison.



Why use any any antifreeze at all? Some want to prevent freezing and algae. These measurements suggest that antifreeze  protection results in a substantial increase in conductivity over distilled water. Yes RV is better than Auto but are either a good idea?

 

I also have read that RV antifreeze is better than standard car antifreeze and that makes sense as it is less conductive. 



We don't know the threshold for conductivity while corrosion resistance is a minor factor as its a glass tube.



It seems 400x more prudent to just use distilled water and keep it clean.



If we add anything to the water we should measure the conductivity i.e. alcohol.



I am going to let some tap water sit and monitor its conductivity over time as it grows algae.


---
**Don Kleinschnitz Jr.** *March 01, 2017 03:48*

Adding to the measurements above:

Tap water in my house is 370 uS. Close to RV antifreeze.



Prestone (ethelene glycol) = 633 uS



Its fair to say I think that 633 uS is to high a conductivity :).


---
**HP Persson** *March 01, 2017 08:32*

I wrote a article about coolants a while back, i´ll add the infographics i made below, maybe can give some ideas to someone.

This is after testing alot of different coolants and their properties (conductivity, viscosity, thermal properties and bio-growth)



The corrosion part isnt a big issue for us though, but it may affect the anode and/or metal parts in the pump/loop.

![images/f51b2a79fbd193a215897f2e28fe2e18.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f51b2a79fbd193a215897f2e28fe2e18.png)


---
**John Sturgess** *March 01, 2017 08:56*

**+Alex Krause** something like this? [apifishcare.co.uk - Welcome to API Fishcare: Prevent Algae Liquid](http://www.apifishcare.co.uk/product.php?sectionid=1&catid=3&subcatid=21&id=94#.WLaM1OS9OHs) 


---
**John Sturgess** *March 01, 2017 09:21*

Also what about De-ionised water as opposed to distilled water? de-ionised water is alot easier to find in the UK


---
**Ned Hill** *March 01, 2017 12:47*

**+John Sturgess** DI water will be essentially the same as distilled in this case.




---
**Don Kleinschnitz Jr.** *March 01, 2017 14:32*

All, Lets not miss the critical point here. 



Whatever fluid you use for cooling, including whatever additive you use, the aggregate should have conductivity close to distilled water. So the only way to know is to measure what you actually have.



There are lots of conductivity meters choices for sale and I suggest a meter should be in everyone's laser toolbox and part of monthly maintenance. Conductivity is one of the common measurements for water quality so the meters are pretty inexpensive.



Getting this wrong is just to expensive and you won't know its wrong until you blow a supply and a tube. Damage manifests itself over a long period of time. So initially if you have the wrong coolant you may not see anything wrong.



There is strong enough evidence that cooling fluids with to high a conductivity blows LPS and damages tubes.



This is what I use but it is pricey:

[https://www.amazon.com/HM-Digital-COM-100-Waterproof-Temperature/dp/B000VVVEUI/ref=sr_1_1?ie=UTF8&qid=1488377998&sr=8-1&keywords=HM+Digital+COM+100](https://www.amazon.com/HM-Digital-COM-100-Waterproof-Temperature/dp/B000VVVEUI/ref=sr_1_1?ie=UTF8&qid=1488377998&sr=8-1&keywords=HM+Digital+COM+100)



Here is a cheaper one but I have no experience with it:  

[https://www.amazon.com/Temperature-Function-Conductivity-Quality-Measurement/dp/B01JJVORWC/ref=sr_1_1?ie=UTF8&qid=1488377774&sr=8-1-spons&keywords=water+conductivity+meter&psc=1](https://www.amazon.com/Temperature-Function-Conductivity-Quality-Measurement/dp/B01JJVORWC/ref=sr_1_1?ie=UTF8&qid=1488377774&sr=8-1-spons&keywords=water+conductivity+meter&psc=1)

.....................

I am adding to my list of research, finding the right range for coolant conductivity.

We know 2.6 uS is a good and 600 uS is bad.

.............

It would be great if we got a gaggle of measurements from this community of what is in everyones bucket.


---
**greg greene** *March 01, 2017 14:42*

To get conductivity as close as possible to distilled water - I use distilled water :)


---
**Don Kleinschnitz Jr.** *March 01, 2017 14:54*

**+greg greene** that's what I should have said. LOL.....


---
**Ned Hill** *March 01, 2017 15:25*

At first it was kind of surprising to me that the conductivity of the cooling water has this kind of effect considering the high resistivity of glass.  After thinking about it some, I think it speaks to the quality of the glass use in the tubes.  At the temperatures we are using, conductivity in glass is mainly due to interstitial migration of Na+ ions.  Also it may also be due to a high dielectric polarization of the glass which would be enhanced by cooling water containing lots of ions.


---
**Ned Hill** *March 01, 2017 15:44*

Lol this was making me nervous.  
{% include youtubePlayer.html id="_t0-KBDyZvQ" %}
[youtube.com - High voltage dielectric breakdown of glass](https://youtu.be/_t0-KBDyZvQ)


---
**Don Kleinschnitz Jr.** *March 01, 2017 16:21*

**+Ned Hill** I don't know. Do you think this is dielectric breakdown or the lichenburg effect followed by thermal breakdown as the glass was heated?

It looks to me like it was not going through the glass but around it on the surface.



Then again glass's dielectric strength is 470KV/meter.


---
**Ned Hill** *March 01, 2017 16:27*

Yeah I agree that what is shown in the video is not really dielectric breakdown but increased surface mobility of ions as it's heated up.


---
**Don Kleinschnitz Jr.** *March 01, 2017 16:27*

**+Ned Hill** I still wonder how there is current flow (an arc) from the plasma to the water. My tank is floating from gnd. Wonder what the path for current is?


---
**Mark Brown** *March 01, 2017 18:07*

Could the current path be from anode to cathode of the laser tube, but through the water instead of the co2?


---
**Don Kleinschnitz Jr.** *March 01, 2017 18:37*

**+Twelve Foot** mmm that's an interesting idea. 

I noticed that the current did no fluctuate during the screeching as I expected since I imagined a leak (parallel) might reduce the current to the tube.

In your scenario (serial) a leak would not change the current through the cathode.


---
**Mark Brown** *March 01, 2017 19:23*

So in that case the water is conducting so well that it's acting as a short circuit?



Only used my laser once for a few minutes, so take this as a grain of salt.  With just tap water, it seemed to only/mostly screech when below 8ma.



 I hadn't payed much attention but I'd thought the screeching was coming from the tube.


---
**Steven Whitecoff** *March 01, 2017 19:47*

Don and guys, as is often the case with forum posts, my point did not come across. Forget RV antifreeze conducts too much, my point was as an additive its not going to prevent growth long term. So I ruled it out. From posts(from ignorant people?, not that I know) it sounded like they were getting rampant anode/cathode corrosion.I dont even know if the water contacts them...and could not find a definitive answer. So I opted for an available solution. 

Sure as little green apple today, doing some 85% power tests I got what must be the screeching today. So I will be picking up more distilled water in a few minutes and flushing my system. The treated water will be saved until I can test it and or determine the cause of screeching.I'll report back later on results.

[answer.so - answer.so](http://answer.So)


---
**Ned Hill** *March 01, 2017 19:53*

If we are talking about electrolytic / solution conduction then that would have to involved some kind of chemical redox reaction.  Like electrolysis of NaCl  (2 NaCl + 2 H2O → 2 NaOH + H2 + Cl2).  Has anyone noticed bubbles forming at the anode?  It would also be interesting to get pH values of water samples as well.



While I'm a primarily a chemist by trade/education, electrochemistry is not my forte. I will have to dig out my electrochemistry book later to make sure I'm thinking about this right. :)


---
**Steven Whitecoff** *March 01, 2017 19:56*

Unless the water does not contact metal in the tube, corrosion will occur unless the Chinese are doing something special. Distilled water will still corrode many metals including steel.Apparently ultra pure water is worse and actually bad for human consumption as an aside. Lots of urban myths stuff involved so check lots of sources to research.


---
**Ned Hill** *March 01, 2017 19:56*

**+Don Kleinschnitz** as the HV expert here, what is actually screeching in the electronics?  Just out of curiosity.


---
**Ned Hill** *March 01, 2017 20:04*

**+Steven Whitecoff** the problem with Ultrapure water in contact with metal is that it leads to increased metal ion leaching since that is a nonequilibrium condition.  



In the general design of these gas tubes, the electrodes should not be in direct contact with the cooling water unless it is poorly made.


---
**Steven Whitecoff** *March 01, 2017 20:13*

Thats great news, visually I could NOT tell. Distilled water and algae control...OK thats the program. 

I mentioned the UPW as apparently any advantages come with dangers, not to mention with the materials involved, it would never stay UPW. I wonder if all this is a bit of a goose chase cause by long term exposure to cheap chinese plastic, pump metal and glass. This whole measure it thing seems to be the best way to track down what is going on.






---
**Ned Hill** *March 01, 2017 20:22*

**+Steven Whitecoff** It probably is a bit of a goose chase since it seems all you need to do is use clean reasonably pure water.  However **+Don Kleinschnitz** has a penchant for wanting to know why things happen, and I do as well, so I'm just throwing my thoughts into the discussion.


---
**Don Kleinschnitz Jr.** *March 01, 2017 22:23*

**+Steven Whitecoff** **+Ned Hill**  cooling water does not contact the electrodes, that would really be bad :(.



Heh, I chase down technical mysteries because knowing how things work enable you to know why they don't. Also the engineer in me wants to make things better and I love to solve unsolved problems. 



Right now I am trying to find why LPS fail so often, this may be one reason but even then I cannot fully explain it so I am not done. It could be that the supply is find its the coolant that't the culprit. 



This thread is a good example. I took it at face value that RV antifreeze was ok just cause it was less conductive. I also thought that distilled water was nice to have. 



Now that we know how this fails and generally why, we almost know what to avoid. 



Use of antifreeze is recommended across the web and our data suggests that they are ALL wrong and putting their machines at long term risk.



BTW these discussions and thoughts are good cause we can help each other learn.


---
**Don Kleinschnitz Jr.** *March 01, 2017 22:31*

**+Ned Hill** I thought it was the fly-back that was internally arcing, they are infamous for HV break down followed by self destruction.  



Two things did not make sense though. No visible arc, current did not change, and the plasma acted weird right at the anode during the screeching.



My hypothesis is that the conductive water causes an arc inside the tube at the anode. It finds a path through the epoxy and current flows through the water and out the cathode (**+Twelve Foot** idea), though the meter and to gnd. 



If it wasnt for the tube making noise you wouldn't even notice the problem other than a loss of optical power.



Short answer: the screeching was the arc inside the tube at the anode, nothing in the electronics. That's my story (theory) and I am sticking with it ........ until proven wrong :)


---
**1981therealfury** *March 01, 2017 23:31*

Just thought i would wade in here and confirm that standard water and antifreeze isn't the way to go :) i recently changed my water due to algae build up in the tank and i usually use dehumidifier water, but I'm short on supply of that as its winter... I put tap water and antifreeze in it last week, only done a few jobs since then but been experiencing the same issues.  Didn't put two and two together until i read this.



I'm getting the screeching and also some fluctuation in the readings on my ammeter, as well as noticeable dips in cutting / engraving performance... hopefully i have not done any long term damage to anything, ill be flushing the system again when my Distilled water gets delivered in a day or two.



Do you guys use an anti algae treatment for the water or do you go with a biocide?


---
**Ned Hill** *March 02, 2017 00:44*

**+1981therealfury**​ I would recommend an aquarium type algaecide, I think Alex Krause mentioned one above.  You can also prevent algae from forming by protecting your reservoir from not only direct sunlight, but indirect sunlight as well.



**+Don Kleinschnitz**​ as far as I can tell looking at my laser tube, there is no epoxy interface between the gas side and the liquid side.  It's interesting that the screaching is coming from the tube rather than the transformer.  Would that imply some kind of resonating harmonic?

Also depending on the electric susceptibility of the glass you could get a strong polarization of the glass which could lead to a polarization of the ions in the cooling water creating a secondary electric field. Not sure how strong the field would be and what effect that would have though.  Just throwing a few thoughts out there.



Also I looked at the ingredients for auto antifreeze and they seem to typically include a buffering salt like sodium tetraborate to make the solution slightly basic, which is the corrosion inhibiting part, and accounts for the higher conductivity.


---
**Steven Whitecoff** *March 02, 2017 01:46*

I confirm, switching to fresh distilled water stopped the screeching for me. For those who dont read the whole thread, I used  Redline auto water "wetter". Im pretty sure it did not screech at first but after a week it sure was. Probably not meaningful but my DVM said my Redline water was .5meg ohm and the distilled read 1.5 meg ohm, I'll be checking that from time to time to see if it will change with time. 



On a slightly different topic, I am also switching to 3/8" vinyl tubing, it will help flow even with the tube fittings being smaller and since the supplied tubing instantly adsorbed dye from the Redline I'm not thinking its the best material. To use the vinyl 3/8" tubing I'm using silicon sealant and a tie wrap on the connections since there is only the one barb on the tube. 


---
**Don Kleinschnitz Jr.** *March 02, 2017 01:53*

**+Steven Whitecoff** then hell its unanimous :)


---
**Don Kleinschnitz Jr.** *March 02, 2017 01:54*

**+Ned Hill** we need to measure the conductivity of algeacides .....


---
**Ned Hill** *March 02, 2017 02:17*

**+Don Kleinschnitz** True, but you typically don't need to add much algaecide and, as long as you stay away from the copper based stuff, it shouldn't add much if anything to the conductivity of the solution.  But definitely a data point to collect.  


---
**Ned Hill** *March 02, 2017 02:26*

Some typical conductivity values for reference.

![images/f3a43a8e29a2661e3519a069e0d783d2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f3a43a8e29a2661e3519a069e0d783d2.png)


---
**John Sturgess** *March 02, 2017 09:58*

This looks interesting, i'm building my closed loop system at the moment and have an arduino controlled safety system monitoring flowrate and temps. I know this probe isn't accurate like a proper meter but i should be able to calibrate it to only enable my laser when the voltage is within acceptable levels. 



I imagine this code will only run once as part of the boot, i'll have a DPDT relay trigger once its ran to remove any connectivity from the probe tips to anything electrical. 

[teachengineering.org - Build and Test a Conductivity Probe with Arduino - Activity](https://www.teachengineering.org/activities/view/nyu_probe_activity1)


---
**HP Persson** *March 02, 2017 10:42*

**+John Sturgess** I have built a similar one, it´s pretty accurate to a "real" meter, enough for our usage atleast :)

I did the same thing, added the code to the arduino that already controls the security, water leaks, mirrors temps and stuff on my machine. It gives a hint anyway when the water needs to be changed.


---
**Don Kleinschnitz Jr.** *March 02, 2017 14:24*

**+John Sturgess** adding a meter to the water as a constant measurement is a great idea. I wonder if its left in the water it will corrode, although it is using nichrome wires.



We could connect this and other readings to bluetooth or push it up through the  cloud as an IFTT alert :):)



One challenge is that we do not know what would be a good set-point  other than the broad set of readings we have above. 

Thanks for sharing ....

Damn, I don't need another idea to add to my K40 project list :).




---
**Dennis Luinstra** *March 02, 2017 22:16*

I had the same problem with the laser cutter screeching a few weeks ago when using distilled water mixed with 20 percent car anti freeze,  switched to new distilled water without the car fluid and the screeching is gone! It looked like the screeching was coming from the psu,  but I am not sure about it. Unfortunately I could not measures the resistance of the water well, because I don't have a suitable measuring device. My multimeter was not suitable for that job. 


---
**Don Kleinschnitz Jr.** *March 02, 2017 22:46*

.. and we have a BINGO!


---
**Don Kleinschnitz Jr.** *March 03, 2017 16:14*

Some more data:

Additives:

Vinegar: 137 uS 

Clorox: won't measure, causes my meter to freak out. That can't be good ? Not a chemist so I do not know what that means. Likely something to do with how IONS act in Clorox?



So at full concentration I would guess its a conductor or something else IONIC going on.



Practical use of chlorine:



Did some simple tests at 10% concentration. Meter is now reads something. 



@10% concentration = 565uS.

......................

Rule of thumb for adding Clorox

So assuming this is linear (which it not)  then a 5% concentration of a 5 gallon bucket is about 32 oz and that would raise the conductivity 120uS. So the conductivity would increase about 4uS per oz of Clorox?


---
**Ned Hill** *March 03, 2017 17:57*

**+Don Kleinschnitz** Normal Clorox is a solution of sodium hypochlorite (NaClO) which in solution yields the ions Na+ and ClO- (hypochlorite).  I imagine the meter freaked out because the ClO- ion is such a strong oxidizer.  Were you testing it straight or diluted?



Also vinegar is a dilute solution of acetic acid which is a relatively weak acid.  In general, weak acids have a lower conductivity than stronger acids because they dissociate into ions less.  That is, at equilibrium there will be portion of the acetic acid that is not dissociated into ions.  So there is an acid dissociation constant (Ka) which is the ratio of dissociated to undissociated concentration.  This chemical equilibrium constant is therefore also affected by temp.  The higher the temp the more dissociation and therefore more ions.  So ideally you want to be comparing things at about the same temp.


---
**Don Kleinschnitz Jr.** *March 03, 2017 18:17*

**+Ned Hill** I expected there are errors in these measurements due to temp but my meter is supposed to adjust for that ??? I don't think it will be a problem as we are playing with a 2 order of magnitude problem and I doubt that error is that big.



Note: I updated the post above after you read it and did some measurements at lower Clorox concentrations as you suggested.



From GoogleIT research and sloppy (averaging) math I concluded that an oz of Clorox in 5 gallons would kill algae and only raise the conductivity by 4uS.



I am going to drain my current tank and start from there.  


---
**Ned Hill** *March 03, 2017 19:24*

Your meter will adjust for a type of temp related error but will not account for temp dependence of dissociation constants.  That being said, I did some calcs and it looks like at the most you would get about a  5% difference for acetic acid between 25 and 40c.  


---
**Don Kleinschnitz Jr.** *March 03, 2017 20:00*

**+Ned Hill** I like the analytics...


---
**Abe Fouhy** *March 04, 2017 06:12*

Ok I'll bite, why is antifreeze needed? Are you guys run this in an unheated garage? Is this not ok to run in the house with ventilation to the window while running?


---
**Abe Fouhy** *March 04, 2017 06:13*

If it helps we use dowfrost HD for coolant for solar collectors, has low ion flow due to trying to keep the copper from oxidizing 


---
**Don Kleinschnitz Jr.** *March 04, 2017 16:06*

**+Abe Fouhy** I see two reasons in the forums:

1. want antifreeze to stop freezing where the unit can freeze (in garages). However I doubt its that effective.

2. want to use the antifreeze as an algae-cide along with anti-freezing.

........

If you have a strong and quite vacuum system ported to the window you can use a K40 inside. Mine is located on an upper floor in and ported to an outside window.

However I am going to move mine to the garage for fear of fire. I have seen what one of these can do when a cut catches fire and I don't want that in the house.

When I move it to the garage I probably will add in a heater for winter protection.


---
**Abe Fouhy** *March 04, 2017 16:13*

**+Don Kleinschnitz** Cool, that's what I figured. That's news about the fire danger. Good to know!


---
**Don Kleinschnitz Jr.** *March 04, 2017 16:47*

**+Abe Fouhy** specs on dowfrost, its $140 for 5 gallons. 

I will be sticking with distilled water and 1oz of Clorox:).



[http://msdssearch.dow.com/PublishedLiteratureDOWCOM/dh_010e/0901b8038010e417.pdf?filepath=heattrans/pdfs/noreg/180-01286.pdf&fromPage=GetDoc](http://msdssearch.dow.com/PublishedLiteratureDOWCOM/dh_010e/0901b8038010e417.pdf?filepath=heattrans/pdfs/noreg/180-01286.pdf&fromPage=GetDoc)


---
**Abe Fouhy** *March 04, 2017 16:51*

Yea, it's spendy stuff. It's made to run your solar system for 10-15y without getting flushed.


---
**Ned Hill** *March 04, 2017 16:51*

**+Don Kleinschnitz** just as a FYI I checked the chemical compatibility of common tubing types and dilute bleach appears to be fine for all.


---
**John Sturgess** *March 05, 2017 12:39*

**+HP Persson** are you removing any electrical connections from the probe whilst the machine is in use? 



I've got stage 1 of the cooling system sorted, flow rate is approx 1.5 litres a minute, hopefully this will be enough.


---
**HP Persson** *March 05, 2017 12:50*

**+John Sturgess** Yeah, the readings are off the chart if it´s powered while the machine is running, due to the charging of the water. It´s small but it´s there. 

Not sure, but one arduino died while it was connected to the probe and machine running, no clue if it was because of that or regular china clone quality :P



I have a timer on the pump, it runs for 15min when the machine is turned on and shows me a green light (pre-cooling), and when machine is turned off, it runs 15min again. When this is running, the probe is also active trough a relay. I get a reading before usage, and after.

Can probably be solved better, or in the program itself. But just testing it so far :)


---
**Don Kleinschnitz Jr.** *March 05, 2017 13:24*

**+HP Persson** **+John Sturgess** so you guys are reading conductivity real time (when not firing laser)?

Do you think that its that critical to track every time machine operated or is this "nice to have". 

If there is no algae would you expect the conductivity to change over time?


---
**John Sturgess** *March 05, 2017 13:55*

I'm not and will only read conductivity when the machine is initially turned on. For me it's a nice to have, I was already implementing an arduino to control the laser enable circuit so for the cost of a few bits of wire it makes sense to add it. 



Why the arduino to control it? I've started a job a handful of times without running the pump and also with the conductivity test it'll probably force me to swap out the water sooner than I previously would've done. This'll be the first time I've ran anything other than straight tap water though. 


---
**Ned Hill** *March 05, 2017 14:59*

**+Don Kleinschnitz** In order for the conductivity to change over time the solution chemistry needs to change. Let's consider some scenarios: 

1. Organic compounds present in solution from the start could breakdown (oxidize) overtime into things that are more conductive. If the oxidation produces ionizable compounds (acids/bases) a conductivity meter could pick that up. However, there are compounds that are neutral in solution but could still undergo conductivity through electrolysis (redox reaction) that a conductivity meter would not pick up. Your conductivity meter uses AC voltage to measure conductivity to avoid polarizing the solution and to avoid electrolysis reactions. So, the meter is just measuring things that are already ionized and are just bouncing back and forth through the solution with the AC polarity changes. But in our case, if we accept the hypothesis that the problem is due to electrical conduction through the tube cooling solution, any neutral compounds that could undergo a redox reaction would participate under HV DC. As long are you start with reasonable pure water I don't see this as a big issue unless some of the things below come into play. 



2. It's possible that chemicals (ions, organics) could leach from the system (tubing/bucket) overtime and alter the solution conductivity. In general I don't think this would be a big issue, especially for materials that had been in use for a while. 



3. In an open system, water can absorb atmospheric CO2 and form carbonic acid (CO2 + H2O ⇌ H2CO3) which will make the solution slightly acidic, raising the conductivity. If your solution happens to be basic to start with the amount of absorbed CO2 will be greater. But for relatively pure water amount of absorbed CO2 under standard atmospheric conditions should not raise the conductivity significantly. 



4. Algae growth – Algae takes nutrients from the water and CO2 from the air to grow.  So, these little organisms will have an effect on solution chemistry.  How much of an effect there will be, I’m not completely sure because I’m not a biologist.  But, I know that they will typically make the solution slightly basic which increases the conductivity a small amount. The big unknown for me is to what extent algae cells would undergo electrolysis when exposed to a HV DC potential.  This could very well be significant.



So in summary, I think as long as you control the algae growth the likelihood that your solution conductivity will increase significantly over time is small.


---
**Abe Fouhy** *March 05, 2017 17:46*

**+Ned Hill** that post really gave me a geekasm. Thank you for the awesome chemical breakdown. Chemistry rocks!


---
**Ned Hill** *March 05, 2017 17:49*

Lol **+Abe Fouhy** You are certainly welcome.  I'm not a high level electronics geek or hacker so I don't often get to a chance to add to the the technical discussions. :)


---
**Abe Fouhy** *March 05, 2017 18:00*

**+Ned Hill** I hear ya, I dabble in it all, wouldn't say I'm a master of any of it. Lifelong experimenter, working on my engineering degree.


---
**Abe Fouhy** *March 05, 2017 18:05*

Ok guys I may of found a solution, no pun intended. Have we thought about mineral oil? I remember it being used in my HV coil on my car so I googled it to see if it would be thermally conductive, it is!



From wiki "Mineral oil is used in a variety of industrial/mechanical capacities as a non-conductive coolant or thermal fluid in electric components as it does not conduct electricity, while simultaneously functioning to displace air and water. Some examples are in transformers, where it is known as transformer oil,[8] and in high-voltage switchgear, where mineral oil is used as an insulator and as a coolant to disperse switching arcs. The dielectric constant of mineral oil ranges from 2.3 at 50 °C to 2.1 at 200 °C."



Since this is a byproduct of oil production and the base to oil we may be able to draw parallels to this white paper ([https://www.google.com/url?sa=t&source=web&rct=j&url=http://www.tandfonline.com/doi/pdf/10.1080/00071617600650161&ved=0ahUKEwjc8NCD97_SAhVB5WMKHRpGBFgQFggkMAE&usg=AFQjCNGD0lZpW_r_QPM_NFm-1LMx4q7ZUQ&sig2=lB2xKYmpHOlWsQhYmb7cbw](https://www.google.com/url?sa=t&source=web&rct=j&url=http://www.tandfonline.com/doi/pdf/10.1080/00071617600650161&ved=0ahUKEwjc8NCD97_SAhVB5WMKHRpGBFgQFggkMAE&usg=AFQjCNGD0lZpW_r_QPM_NFm-1LMx4q7ZUQ&sig2=lB2xKYmpHOlWsQhYmb7cbw)) that indicate that oil inhibits algea growth by preventing it's ability to perform  photosynthesis, preventing the growth and any growth that does happen will be surrounded by a nonconductive island of oil.


---
**Don Kleinschnitz Jr.** *March 05, 2017 18:08*

**+Ned Hill** awesome explanation!. So it seems the 1oz of Clorox in 5 gallons is backed up by science. Now we test the practice.


---
**Don Kleinschnitz Jr.** *March 05, 2017 18:10*

**+Abe Fouhy** very punny. I am sticking with the clorox/distilled water. Oil is expensive and messy ....would probably have to change pump types.


---
**Abe Fouhy** *March 05, 2017 18:13*

The oil is cheap too $15/gal for mineral oil too, [amazon.com - Amazon.com : UltraCruz Mineral Oil Light, 1 Gallon : Pet Supplies](https://www.amazon.com/gp/aw/d/B00OGKPJB0/ref=mp_s_a_1_5?ie=UTF8&qid=1488737541&sr=8-5&keywords=gallon+mineral+oil)


---
**Don Kleinschnitz Jr.** *March 05, 2017 18:16*

**+Abe Fouhy** add in new pumps and a DC supply with controls :).

Doubt you will pump oil effectively with a submersible, but never tried it :)


---
**Abe Fouhy** *March 05, 2017 18:20*

**+Don Kleinschnitz** I could see that the impeller in the pump may degrade with oil, not sure about the viscosity as they are probally similar. I think you would want/need a different pump though. Are you guys still using the stock fountain pump that came with it?


---
**Don Kleinschnitz Jr.** *March 05, 2017 18:21*

**+Abe Fouhy** yes, water and oil viscosity similar?


---
**Abe Fouhy** *March 05, 2017 18:30*

Cool it is actually less kinematic viscosity at .869 vs 1.0 for distilled water at 20c. **+Ned Hill** would polypropylene impellers degrade in mineral oil or would it be fine since it is a polymerization of oil?


---
**HP Persson** *March 05, 2017 22:25*

**+Don Kleinschnitz** For me, reading water quality is just a fun function to have. Reading it once per week is probably enough :)

Heck, i even control the mirror temps on my machine, to tell me when it´s time to clean them :)


---
**HP Persson** *March 05, 2017 22:28*

And, about the mineral oil. I tested it yesterday after a discussion on facebook. The brand i had had about half of the thermal pickup compared to plain distilled water. 

I only tested it with a peltier and regulated power and measured the heat, not any data of real use in a tube.


---
**Don Kleinschnitz Jr.** *March 05, 2017 22:33*

**+HP Persson** whoa mirror temps ..... interesting. 

I'm going to "get as close as I can to distilled water by using distilled water"  :)


---
**Ned Hill** *March 06, 2017 00:10*

Mineral oil has a couple of drawbacks in this application.  One the thermal capacity about 3.5x less than water and the thermal conductivity is about 2.5x less than water (consistent with **+HP Persson** 's results).  Second is that the viscosity of mineral oil is about 15X that of water at 20C (this will actually vary as mineral oil can be found in different viscosities).  The higher viscosity of mineral oil will mean much higher pressure on the glass which can cause fractures (I've crack a glass cooling jacket in the lab before using water because of too much back pressure).


---
**Don Kleinschnitz Jr.** *March 08, 2017 15:24*

All: some change in our recommendation.



I changed my water last night with the 1oz/5Gal and the conductivity was 281 uS :(. 

Not what I expected (<10). 



Short story;

 .... the sources conductivity data is bogus :)



**+Ned Hill** re-calcuated and we should expect in the range of 30uS using .25oz of Clorox in 5 gal. So I suggest this is the currently correct formulation :).



I am going to do some empirical measurements at these concentrations to verify the calcs. 



I will update this post as I run more tests. I also plan to restart the long term tests.



[donsthings.blogspot.com - Laser: Protecting, Operating & Cooling](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)






---
**Ned Hill** *March 08, 2017 16:03*

Note that the calc I did wasn't rigorous and I am making some assumptions on the actually ion concentrations based on the MSDS sheet for the bleach (approx 7% NaClO). I suspect the actual conductivity value for 0.25oz bleach into 5gal water is going to be a bit higher than 30us.  In fact if we take Don's value of 281us for 1oz bleach and divide by 4 (should be linear) we get 70us.  That's why Don is going to make a series of solutions and measure with his conductivity meter to verify.


---
**Don Kleinschnitz Jr.** *March 09, 2017 21:11*

I completed the coolant remix and test. I tested the conductivity at 1x and 1/2x concentration for the needed amount to control algae.

1x = 44.9-46 uS (measure 1/2 at a time)

1/2 x = 22 uS



**+Ned Hill**​ was close...nice.



Conclusion: add 6ml Clorox to 5 gallons of water and your conductivity will run in the 40's uS.


---
**Ned Hill** *March 09, 2017 21:46*

Yeah for science! :)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/jDRGVhd6zqy) &mdash; content and formatting may not be reliable*
