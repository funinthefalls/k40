---
layout: post
title: "Hi , I bought this machine a week ago ,but when i tried to setup I am unable to engrave/cut anything"
date: October 05, 2015 20:25
category: "Discussion"
author: "Raja Rajan"
---
Hi , I bought this machine a week ago

 [http://www.ebay.com/itm/-/260825065645](http://www.ebay.com/itm/-/260825065645) ,but when i tried to setup I am unable to engrave/cut anything.



Do we need to ground the wire?(the back side ) is that mandatory?



I have connected all water inlet and outlet and Fan and Laserdraw software and and initialize the device everything.



I could manually fire the laser using (Test switch) ,i see the small whole on my acrylic sheet when i hit the test switch.



When i hit engrave i don't see any laser beam coming to acrylic.



I have tested the optical path as well,Except grounding i have done everything.Am i missing something?



PLZZ HELP!!!





**"Raja Rajan"**

---
---
**William Steele** *October 05, 2015 21:25*

Can you manually fire the laser?  If so, then it's not the ground.  It could just settings in the software.  What speed and power settings are you using?


---
**Phillip Conroy** *October 05, 2015 22:32*

On my laser i had to hold in the left button for the laser to fire-that is untill i changed the wiring


---
**Raja Rajan** *October 06, 2015 11:27*

William, thanks for your reply!



Yes, when i try test laser i see the small laser beam hitting on my acrylic sheet and i see some small whole. So am i good with   Ground?

What would be the typical speed i ll share my settings photo


---
**Raja Rajan** *October 06, 2015 14:46*

Hi Philip,yes i have made laser switch [on.is](http://on.is) that anything I am missing ??


---
**Phillip Conroy** *October 06, 2015 18:48*

The push button switches are not 1 push on next push off they must be held down for the lase to fire,the left switch must be held down with a weight ie heivy paper weight,the left switch should be changed to latching type,ie 1 push turns switch on next push turns switch off


---
**Raja Rajan** *October 06, 2015 20:37*

Hi Philip ,thanks! you mean ..i need to keep paper weight on the Laser switch ? or sorry i couldnt get you properly 


---
**Phillip Conroy** *October 07, 2015 04:43*

Yes u need to hold in the left switch,or put a weight on it


---
*Imported from [Google+](https://plus.google.com/110717916181924497071/posts/9AQoLvLDqvK) &mdash; content and formatting may not be reliable*
