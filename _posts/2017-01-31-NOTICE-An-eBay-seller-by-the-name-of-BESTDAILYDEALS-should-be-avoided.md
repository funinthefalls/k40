---
layout: post
title: "NOTICE - An eBay seller by the name of BESTDAILYDEALS, should be avoided!"
date: January 31, 2017 23:04
category: "Discussion"
author: "timb12957"
---
NOTICE - An eBay seller by the name of BESTDAILYDEALS, should be avoided! It is a long story and I will not post it here, but they are under investigation by eBay for unfair practices.﻿ I have posted this on my Facebook page and encourage you to do the same to save your friends from purchasing from an unscrupulous seller.





**"timb12957"**

---
---
**David Komando** *February 01, 2017 05:08*

I bought my red and white k40 from them, delivered earlier this month. Any details?


---
**timb12957** *February 01, 2017 15:54*

Briefly, they canceled a purchase I made claiming it was my request then relisted EXACT item at higher price. I don't trust them


---
**David Komando** *February 01, 2017 16:52*

Don't blame you there!



However, I will say I thought I wasn't sent the encryption USB to run Corel-Laser and they answered within an hour or two saying I really should have it and to recheck everything. After rechecking I did find it under the packing box flap that was ready to head out to the trash. So not terrible customer service. Granted I did pay a premium due to Chinese New Year. There was also another problem of it being shipped the day after it was supposed to arrive.



Final side note, I started receiving a ton of spam from them talking about their current sales etc.


---
**timb12957** *February 06, 2017 12:17*

UPDATE - After dozens of emails to them and posting my complaint to my social media sites, the eBay seller BESTDAILYDEALS has resolved the issue we had with a previous purchase. It took a long time and a lot of effort to resolve this, but I would now have to recommend them for their fairness!


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/Uw1ueW9gBJZ) &mdash; content and formatting may not be reliable*
