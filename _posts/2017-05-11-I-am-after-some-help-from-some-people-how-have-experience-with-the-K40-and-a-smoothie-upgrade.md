---
layout: post
title: "I am after some help from some people how have experience with the K40 and a smoothie upgrade"
date: May 11, 2017 02:04
category: "Smoothieboard Modification"
author: "John Revill"
---
I am after some help from some people how have experience with the K40 and a smoothie upgrade. I have my K40 running well with a Marlin/RAMPs board, but it has a limit to how fast I can push it vs the power of the laser. I am looking at upgrading to a Smoothie board but haven't deviced which one to go with. Ive already been burnt with a faulty MKS SBASE V1.3 board, so I dont, know if I want to try that again. So either a genuine smoothie (Capable of 1/32 microsteps), or a Cohesion 3D (Capable of 1/16 microsteps).  Any suggestions and experiences from people?





**"John Revill"**

---
---
**Anthony Bolgar** *May 11, 2017 05:19*

The cohesion board is the simplest to install, it is as plug and play board.


---
**Joe Alexander** *May 11, 2017 05:23*

2nd the Cohesion board, **+Ray Kholodovsky** can help you with which to get and any conversion questions :)


---
**John Revill** *May 11, 2017 06:02*

I'm thinking the Cohesion 3D with the DRV8825 drivers so I can get the same 1/32 microsteps of the Genuine Smoothieboard. What drivers are you using and are you getting good results?


---
**Ray Kholodovsky (Cohesion3D)** *May 11, 2017 09:16*

You can do that. The reason I do A4988 is because they don't require active cooling. And people don't seem to be having problems with them...

With the DRV's you'll need to mount a fan to actively cool them. 

If you're going all out may as well get SilentStepSticks - TMC2208 - that's 1/256 Micrstepping!


---
*Imported from [Google+](https://plus.google.com/114782552977074073979/posts/Dbr9cYUDyYU) &mdash; content and formatting may not be reliable*
