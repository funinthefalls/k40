---
layout: post
title: "Should I get the co2 laser safety glasses"
date: October 07, 2016 19:18
category: "Original software and hardware issues"
author: "Dan Massopust"
---
Should I get the co2 laser safety glasses.  I've read that regular safety glasses are OK for the k40 I don't want to mess with my eyes if this is not true.  I tried searching this community about this and kept getting adds.  This is my first time on a google community so sorry if I could have searched this.  Have not fired tube yet.





**"Dan Massopust"**

---
---
**greg greene** *October 07, 2016 19:29*

Regular safety glasses are not rated for the wavelength of the Co2 laser - get the right glasses


---
**Don Kleinschnitz Jr.** *October 07, 2016 19:37*

You need CO2 safety glasses .....


---
**Don Kleinschnitz Jr.** *October 07, 2016 19:38*

Oh and BTW "INTERLOCKS" installed!


---
**Ariel Yahni (UniKpty)** *October 07, 2016 23:28*

I meant to paste this [lightobject.com - Protection Glasses](http://www.lightobject.com/Protection-Glasses-C18.aspx)


---
**Kris Sturgess** *October 10, 2016 04:28*

I've been searching E-Bay for co2 laser glasses. I believe rated for 10600nm is what you need. $25.99 and up.


---
**Dan Massopust** *October 10, 2016 11:49*

Thank-you for your help.  I will get co2 rated glasses.  I see some of you posted 3 days ago and yet I just now got the post.  I need to check out what happened.  Well thanks again.


---
**Jorge Robles** *January 30, 2017 10:01*

(Good to have found this thread) Is the case acrylic enough to testing, or are the safety glasses mandatory even running with closed case?


---
**Ariel Yahni (UniKpty)** *January 30, 2017 11:57*

**+Jorge Robles** i believe is enough and most people do it this way but you know, safety is never enough


---
**Don Kleinschnitz Jr.** *January 30, 2017 13:25*

**+Jorge Robles** INTERLOCKS are needed! PLEASE!


---
**Jorge Robles** *January 30, 2017 13:42*

**+Don Kleinschnitz**​ of course, I won't even plug the machine without first installing them, for sure.


---
*Imported from [Google+](https://plus.google.com/117247597344463074944/posts/H25CyVREncs) &mdash; content and formatting may not be reliable*
