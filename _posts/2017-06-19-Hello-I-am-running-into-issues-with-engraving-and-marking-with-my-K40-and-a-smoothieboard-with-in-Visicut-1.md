---
layout: post
title: "Hello, I am running into issues with engraving and marking with my K40 and a smoothieboard with in Visicut: 1"
date: June 19, 2017 23:37
category: "Object produced with laser"
author: "Josef Seibl"
---
Hello, I am running into issues with engraving and marking with my K40 and a smoothieboard with in Visicut:



1. engrave with about 150mm/s.

2. mark with a speed of about 105mm/s.



In the picture, you can see that on right hand side of the letters, the marking cuts into the wood, but ideally it should be exactly on the border between burnt area and wood.On the left hand side, the mark is inside the burnt area. So either the mark is too far right, or the engraving is too far left.



On smaller objects, the mark sits perfectly, but for this part, it is skewed. I already lowered the mark and engrave speed, before the effect was a bit worse.

Acceleration is set to 2000mm/s^2.



Because I am already at pretty low acceleration and movement values, I suppose that there is an underlying problem that needs to be fixed. Where are the places to look first?

![images/29d5089a11c46f4759f93a770dfc8bd9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/29d5089a11c46f4759f93a770dfc8bd9.jpeg)



**"Josef Seibl"**

---
---
**Joe Alexander** *June 20, 2017 00:02*

to me it looks as if its just an even shift over. do you have them set as different operations in Laserweb4? I would imagine if they are different entities then maybe the positioning of the outline for the border is off just a bit to the right? it seems that a shift of maybe a mm left on the marking portion would line everything up, right?


---
**Julia Liu** *June 20, 2017 02:22*

**+Joe Alexander**  Great!


---
**Claudio Prezzi** *June 20, 2017 06:39*

**+Joe Alexander**​ He wrote he is using Visicut (not LaserWeb).


---
**Joe Alexander** *June 20, 2017 07:05*

doh, missed that. Ideally it was the same issue as it would be an easy fix.


---
*Imported from [Google+](https://plus.google.com/117771434142376088707/posts/HoRcxvAFuUB) &mdash; content and formatting may not be reliable*
