---
layout: post
title: "Minor issue with homing sequence on my K40 with a smoothieboard"
date: October 21, 2016 07:21
category: "Smoothieboard Modification"
author: "Anthony Bolgar"
---
Minor issue with homing sequence on my K40 with a smoothieboard. I have home to max set for Y and I have entered 230mm as the max distance in the config. But no matter how many times I save and reboot the smoothieboard, it sets max Y max to 200mm  What am I missing here? Any ideas **+Arthur Wolf** 





**"Anthony Bolgar"**

---
---
**Arthur Wolf** *October 21, 2016 09:37*

Have a config-override file ?


---
**Anthony Bolgar** *October 21, 2016 09:47*

only thing on the SD card is firmware.bin and config.txt


---
**Arthur Wolf** *October 21, 2016 09:49*

Can you try formatting the card and starting back from a fresh most recent firmware and config file ? ( standard procedure )


---
**Anthony Bolgar** *October 21, 2016 09:50*

Will do so and get back to you. Thanks


---
**Anthony Bolgar** *October 21, 2016 10:00*

That fixed the problem. Thanks for the advice, I will remember that in the future if I have any other config issues.


---
**Arthur Wolf** *October 21, 2016 10:01*

**+Anthony Bolgar** Possibly SD card corruption, possibly caused by not unmounting the card after editing the file.


---
**Anthony Bolgar** *October 21, 2016 10:05*

I am just thrilled now, that was the last thing I needed to resolve on the upgrade of my K40. Now it is back in service!


---
**Arthur Wolf** *October 21, 2016 10:06*

:)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/81eJ1roLtEf) &mdash; content and formatting may not be reliable*
