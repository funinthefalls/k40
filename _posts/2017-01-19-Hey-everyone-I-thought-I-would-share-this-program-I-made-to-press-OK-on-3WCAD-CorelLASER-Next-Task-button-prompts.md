---
layout: post
title: "Hey everyone I thought I would share this program I made to press OK on 3WCAD CorelLASER Next Task button prompts"
date: January 19, 2017 08:27
category: "Software"
author: "Kris Backenstose"
---
Hey everyone I thought I would share this program I made to press OK on 3WCAD CorelLASER Next Task button prompts.



Just run it in the background and it will target the "Next Task" window and press the OK button for you. YAY!





**"Kris Backenstose"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 19, 2017 12:31*

Wow, that would have been really handy while I was still using CorelLaser. Great idea :)


---
*Imported from [Google+](https://plus.google.com/113059362277072411228/posts/SGGLQc6cnhP) &mdash; content and formatting may not be reliable*
