---
layout: post
title: "I have misplaced my software CD containing laserdrw 3"
date: October 26, 2015 02:01
category: "External links&#x3a; Blog, forum, etc"
author: "James Keaton"
---
I have misplaced my software CD containing laserdrw 3.  Does anyone have a free download link for it?  Thank you for your time.





**"James Keaton"**

---
---
**Jim Coogan** *October 26, 2015 02:19*

Go to the files area of this Facebook group.  We have the latest version there.  [https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
**Jim Coogan** *October 26, 2015 02:21*

This is the direct link to it.  [https://www.facebook.com/groups/441613082637047/531905473607807/](https://www.facebook.com/groups/441613082637047/531905473607807/)


---
**James Keaton** *October 26, 2015 02:21*

Thank you so much!  I am a part of that group on FB and didn't think to look there.  I just finished my new computer for my engraver and 3D printer and I am having to reload ALL of the software...blah.


---
**James Keaton** *October 26, 2015 02:43*

This community is great, and of course I found my CD after downloading it from the group


---
**Peter** *October 30, 2015 23:07*

Does anyone know if there is a marker for the dongles that come with the k40's?

Mine was plugged OK nto my laptop when I packed the machine up to return. I still have it and the CD, which is obviously legit, like all the others...


---
*Imported from [Google+](https://plus.google.com/109769945337563588796/posts/B2KQhCeUjrX) &mdash; content and formatting may not be reliable*
