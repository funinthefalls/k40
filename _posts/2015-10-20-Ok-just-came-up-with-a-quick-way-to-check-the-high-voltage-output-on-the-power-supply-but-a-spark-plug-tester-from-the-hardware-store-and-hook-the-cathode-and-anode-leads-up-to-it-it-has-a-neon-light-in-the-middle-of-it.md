---
layout: post
title: "Ok just came up with a quick way to check the high voltage output on the power supply, but a spark plug tester from the hardware store and hook the cathode and anode leads up to it, it has a neon light in the middle of it,"
date: October 20, 2015 21:17
category: "Discussion"
author: "Scott Thorne"
---
Ok just came up with a quick way to check the high voltage output on the power supply, but a spark plug tester from the hardware store and hook the cathode and anode leads up to it, it has a neon light in the middle of it, you can also set your voltage with it hooked up.





**"Scott Thorne"**

---
---
**Scott Thorne** *October 20, 2015 21:22*

Both....it's reading the 15k output from the transformer.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/gSBgcXy886Q) &mdash; content and formatting may not be reliable*
