---
layout: post
title: "Problem cutting 1/4 inch hickory! No matter what power setting I use, my 1/4 inch hickory seems to always have a tiny ember burning that eats away at the cut leaves a distorted cut surface"
date: May 16, 2017 19:58
category: "Materials and settings"
author: "Jeff Johnson"
---
Problem cutting 1/4 inch hickory! No matter what power setting I use, my 1/4 inch hickory seems to always have a tiny ember burning that eats away at the cut leaves a distorted cut surface. This doesn't happen with any other wood and I wonder if this is just the nature of hickory. It smells really good, though. I can easily cut 1/4 walnut, cherry, poplar and red oak without any fire. One thing I've noticed is that the problem is made worse by using the air assist. 

Does anyone have any ideas or tips that might help? I've got 10 sheets of this wood and don't want to have to use it in my grill. Maybe moisten it a little?





**"Jeff Johnson"**

---
---
**Anthony Bolgar** *May 16, 2017 20:31*

Maybe cut through a damp paper towel like engraving on glass?


---
**Ned Hill** *May 16, 2017 21:04*

Interesting, I haven't tried Hickory yet.  I like Anthony's idea.




---
**Jeff Johnson** *May 16, 2017 21:38*

**+Anthony Bolgar** The embers collect near the bottom of the cut where it's most narrow and it burns from the bottom up. I'll give this a try and see what happens. If the worst happens I'll throw some bacon on it and have some hickory smoked goodness as a snack.


---
**Jeff Johnson** *May 16, 2017 21:40*

I forgot to mention that engraving works perfectly. I can engrave about 1.5mm deep without any burning.




---
**Ned Hill** *May 16, 2017 21:55*

Are just trying to cut in one pass?  What would happen if you went with lower power or faster speed with multiple passes?


---
**Jeff Johnson** *May 16, 2017 22:10*

I've tried slowing it down , speeding it up, more power, less power, and as many as twelve passes with the same result. I guess this is why Hickory works so good when smoking Meats. Hopefully moisture will solve this problem.


---
**Jeff Johnson** *May 17, 2017 14:21*

Nothing seems to help but I did find the optimum speed / power / passes combination to leave minimal charring. I'm running at 12ma at 25mm/s for 6 - 8 passes. I've discovered that I can make drink coasters or anything else where I can add 1mm to the outer dimension and sand it back. I made these at the suggestion a friend who is a serious Harry Potter fan. BTW, hickory really doesn't take a stain well. The top row is a deep red mahogany stain and the bottom row is a dark walnut stain. I think they look great, though.

![images/3cd98a0bae6e7cfbd487392fa72ade84.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3cd98a0bae6e7cfbd487392fa72ade84.png)


---
**Ned Hill** *May 17, 2017 14:33*

Nice job.  Can you point me to a source file for these?  I have a friend who would probably love a set as well.


---
**Jeff Johnson** *May 17, 2017 15:03*

**+Ned Hill** I just published it on Thingiverse. [thingiverse.com - Harry Potter Drink Coasters by JoeSnuffie](http://www.thingiverse.com/thing:2326694)




---
**Ned Hill** *May 17, 2017 16:04*

Many thanks :-)


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/g5cZVkbcCcu) &mdash; content and formatting may not be reliable*
