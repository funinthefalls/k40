---
layout: post
title: "I am waiting for one of these machines to arrive, but until then I am watching videos and reading as much as I can about the k40 machines"
date: August 30, 2016 22:40
category: "Modification"
author: "Bruce Golling"
---
I am waiting for one of these machines to arrive, but until then I am watching videos and reading as much as I can about the k40 machines.

I have a question about the table where the item being cut sits.  I see different things being used such as honeycombs and alternatives to honeycombs.  Well my question is; wouldn't the focal length of the laser be altered and out of whack with different heights of the z plane?  

I see plenty of tutorials about how to align the mirrors for the x and y axes but no mentions of tuning the focussing height of the lens.

Any help would be appreciated.





**"Bruce Golling"**

---


---
*Imported from [Google+](https://plus.google.com/105808932307807066174/posts/ME96dUG5Pc4) &mdash; content and formatting may not be reliable*
