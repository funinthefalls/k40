---
layout: post
title: "Ornament for my buddys Mom. RIP Maggie"
date: December 20, 2017 02:51
category: "Object produced with laser"
author: "David Allen Frantz"
---
Ornament for my buddy’s Mom. RIP Maggie. 



![images/c46c7fef57d23ccf21082e538b4e7ffc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c46c7fef57d23ccf21082e538b4e7ffc.jpeg)
![images/814fa449a7f3b92f94c3ebcffd978c43.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/814fa449a7f3b92f94c3ebcffd978c43.jpeg)
![images/b38a69c1fe7e58a6b6bc07cbfda603d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b38a69c1fe7e58a6b6bc07cbfda603d7.jpeg)

**"David Allen Frantz"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2017 04:44*

New board is treating you well I take it? 


---
**David Allen Frantz** *December 20, 2017 12:57*

**+Ray Kholodovsky** oh yeah ray thank you so much for developing this thing! Total game changer!


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/HDnrVavsKFo) &mdash; content and formatting may not be reliable*
