---
layout: post
title: "I have coolant pumping thru the laser tube and chiller is running @ temp for now"
date: May 18, 2016 01:57
category: "Discussion"
author: "Todd Miller"
---
I have coolant pumping thru the laser

tube and chiller is running @ temp for

now.  Will fine tune later....

![images/56d8a2d81cf1087bca7e9a44ad088c36.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/56d8a2d81cf1087bca7e9a44ad088c36.jpeg)



**"Todd Miller"**

---
---
**Alex Krause** *May 18, 2016 02:38*

What kind of coolant did you use?


---
**Gunnar Stefansson** *May 18, 2016 08:58*

Looks Amazing **+Todd Miller** I have something similar looking... Love your construction, looking forward to seeing it completed...


---
**Scott Thorne** *May 18, 2016 10:44*

**+Todd Miller**...the whole frame is extruded aluminum right? 


---
**Todd Miller** *May 18, 2016 16:26*

Yes,  the whole frame is extruded aluminum.

For coolant, I use distilled water and RedLine "Water Wetter" additive.


---
**Scott Thorne** *May 18, 2016 19:04*

 Nice job brother! 


---
**Brien Watson** *May 18, 2016 23:45*

I have the same frame system..  From [lightobject.com](http://lightobject.com). Mine is a 600 X 900. What size is yours?  You are a lot farther along than I am..  Nice job so far!


---
**Todd Miller** *May 19, 2016 01:09*

Mine is also 600 x 900.  By the weekend, I'd like to fire up the tube.


---
**Andrew ONeal (Andy-drew)** *May 19, 2016 05:46*

What is the tube rating and what did you set up like this run ya? Also when you say extruded aluminum, what do you mean?


---
**Todd Miller** *May 20, 2016 03:08*

The tube is a 80w RECI, it can run up to 100w but will shorten it's life.  I'd say I have $3.5k so far in parts/shipping.



Extruded aluminum is like... the Play-Doh toy

you played with as a child.  You put a ball of Doh in it and pressed a lever and it would shit out a stream shaped like a star, box, circle or square.


---
**Tyler Johnston** *November 17, 2016 16:33*

Awesome machine your building. I also bought the stages and frame kit.  Not quite as far along though.  Did you use the 3MD560 steppers?   If so how do you wire the 3 phase motor to it?  Any info would be greatly appreciated.  6 wires into 3 ports, was not sure the combination.  Tried forum and YouTube and noticed you were much farther along on the same parts.  


---
**Tyler Johnston** *November 17, 2016 19:10*

Found my answer after I posted.  Truly inspiring work you have done with your build!  Love the photos.


---
**Todd Miller** *November 17, 2016 23:07*

Sorry I didn't get back to you, was at work.

After this original post, I didn't have a chance to work on it until summer was over.  I'm back

at it !  Working on venting this weekend and then install the optics.


---
**Tyler Johnston** *November 17, 2016 23:56*

Hey no problem.  Can't wait to see your updated system.  It has inspired me to get to work on mine!


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/B2trYwpZtZW) &mdash; content and formatting may not be reliable*
