---
layout: post
title: "So I installed an ammeter on my k60 and after doing some testing found some interesting results!"
date: August 14, 2018 17:11
category: "Discussion"
author: "Nigel Conroy"
---
So I installed an ammeter on my k60 and after doing some testing found some interesting results!



Looks like my laser output drops somewhere around the 30% setting.

Any ideas or similar findings?



This was with an analog ammeter so no exact decimal places just eyeballing results.



Has anyone installed a shunt and output the data to an Ardunio?

![images/d31e5c21a421bf6487d9ed7564cce1ce.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d31e5c21a421bf6487d9ed7564cce1ce.png)



**"Nigel Conroy"**

---
---
**Don Kleinschnitz Jr.** *August 14, 2018 19:18*

What current meter, what adjustment mechanism (pot/digital)?

You can look at the voltage on the IN pin and plot that on top of this graph to see what the control voltage is doing.


---
**Nigel Conroy** *August 14, 2018 19:35*

**+Don Kleinschnitz** ![images/ca8a8a878c5cbd74cdfa60122168244a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca8a8a878c5cbd74cdfa60122168244a.jpeg)


---
**Nigel Conroy** *August 14, 2018 19:36*

This is what I installed.


---
**HalfNormal** *August 15, 2018 00:14*

What Don is  trying to say is that you need to track the control voltage NOT percent with the current output.


---
**Don Kleinschnitz Jr.** *August 15, 2018 09:47*

Your graph above show percent (on the panel?) vs laser milliamps  which shows you the laser current relative to what the control panel THINKS its telling the LPS. 



Plot control voltage from the LPS's IN pin vs laser milliamps and that will tell you what your control panel is ACTUALLY telling the LPS. 



What kind of machine?

What does the control panel look like?

What LPS? 


---
**Nigel Conroy** *August 15, 2018 11:13*

Thanks Don, I get what your saying now. I looked at percent as that's what I'd be programming.



I'll have to take a photo of lps to show you. 



It's a Chinese 60watt machine so I just call it a k60. 


---
*Imported from [Google+](https://plus.google.com/116715093970124789545/posts/5h34MMw5XLP) &mdash; content and formatting may not be reliable*
