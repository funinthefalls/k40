---
layout: post
title: "I designed a Y-Wing with a stand to go with the small scale models designed by another guy on Thingiverse"
date: January 18, 2017 16:15
category: "Object produced with laser"
author: "Jeff Johnson"
---
I designed a Y-Wing with a stand to go with the small scale models designed by another guy on Thingiverse. I've modified the ones he's designed to also have a stand and this gets me closer to completing my collection. Up next is a small scale AT-ST and then a B-Wing. I love these smaller models!

[http://www.thingiverse.com/thing:2045525](http://www.thingiverse.com/thing:2045525)





**"Jeff Johnson"**

---


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/JQSLzGmXhN8) &mdash; content and formatting may not be reliable*
