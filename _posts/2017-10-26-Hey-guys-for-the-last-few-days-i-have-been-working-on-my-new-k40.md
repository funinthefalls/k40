---
layout: post
title: "Hey guys, for the last few days i have been working on my new k40"
date: October 26, 2017 19:04
category: "Original software and hardware issues"
author: "Marko Marksome"
---
Hey guys, for the last few days i have been working on my new k40. Today i have alligned the mirrors, and the final beam where it hits the laser head is slightly off the center. In order to bring it to the center i would have to move the head a little down. Is there a way to do that? Is the result good enough, or should the beam hit the third mirror deadcenter?

Ill post a photo so you can judge.

Thanks

![images/9d50484013498a5b4a0c3610bd0dad0a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9d50484013498a5b4a0c3610bd0dad0a.jpeg)



**"Marko Marksome"**

---
---
**Marko Marksome** *October 26, 2017 19:06*

![images/c1066fd00a6133dae57d6817ce275d26.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1066fd00a6133dae57d6817ce275d26.jpeg)


---
**Anthony Bolgar** *October 26, 2017 19:20*

It is best to be dead center, especially if you use an air assist nozzle. On the stock K40 head you could probably get away with that.


---
**Marko Marksome** *October 26, 2017 20:20*

Oh wait, i just realized i need to put the head up and not down. Easiest way would be to put some shims in between the screws. Easy fix... thanks for the info


---
**Marko Marksome** *October 26, 2017 20:22*

Oh no that would only raise the screws... need another solution


---
**BEN 3D** *October 26, 2017 21:24*

Hi Marko,

my last mirror is also not perfekt alighned, similar to yours, but it works well. Please fire some tests before you change it, and compare youre results here thanks! That would help me also :-)



Cheers Ben


---
**Printin Addiction** *October 26, 2017 21:40*

You could put the shims (washers) under the plate (head mount) to raise it a bit. Either right below the mount, or on top of th gantry (beam).


---
**Andy Shilling** *October 26, 2017 21:54*

Could you not get a washer to fit between the top and the plate? They unscrew, I'm sure there is enough thread in there. A washer might be too big though.


---
**Marko Marksome** *October 26, 2017 22:21*

Yes Andy i just realised that. I will put something in between the plate and the top part of the head. Have to see if i can find something that big. If not i can still make something out of 1mm thin acrylic. Thanks to you guys!


---
**Ned Hill** *October 26, 2017 22:52*

I had the same problem with my machine and just put washers under the plate.


---
**Printin Addiction** *October 26, 2017 23:17*

Got to love the self modding nature of these and 3D printers.....I cut a perfect lense adapter for the LO air assist head tomfit the 12mm lense.


---
**Alex Krause** *October 27, 2017 02:02*

I used a pop can to make a shim for mine worked well... But very sharp


---
**Marko Marksome** *October 27, 2017 09:39*

Thats a good idea


---
**Marko Marksome** *October 27, 2017 13:00*

Hey i ended up puting washers underneath the head plate. Works fine the beam is centered now. Bit i have a new problem now. If i change the hight of the bed the beam starts to drift when changing the height or puting on a air assist cap. Is there a way to reposition the third mirror which is on the laser head?


---
**Ned Hill** *October 27, 2017 13:56*

Yeah I've seen that as well.  Could be the result of the plate not being completely level or the head not sitting flush on the plate.  Also the head/mirror maybe slightly rotated so that the face of the mirror isn't perpendicular to the beam.


---
**Marko Marksome** *October 27, 2017 14:16*

Do you know how i could compensate the missallignment?


---
**Marko Marksome** *October 27, 2017 17:20*

If anybody is interested, i have fixed the problem.

I have made sure that the rails are planer trough the whole section. I have adjusted the beam to shine directly into the middle of the third mirror. Works like a charm now and it goes directly trough the hole of the air assist cap. I even managed to burn myself with the beam.. thankfully it was on the lowest setting...


---
**Andy Shilling** *October 27, 2017 19:48*

Photos of the fix would be nice **+Marko Marksome** ;)




---
**Marko Marksome** *October 27, 2017 20:23*

Sure. I will post them tomorrow. Got to work now.


---
**Marko Marksome** *October 28, 2017 16:51*

hey guys, i recorded a video for u.

i leveled the rails with some washers untill the rails are leveled. second, i hightened the laser head also with washers under the plate.  Now the beam is allways hitting the middle of the third mirror, no matter where on the XY axis. 


{% include youtubePlayer.html id="Jextspntoog" %}
[youtube.com - K40 laser rails leveled with washers](https://youtu.be/Jextspntoog)


---
*Imported from [Google+](https://plus.google.com/110288443111992997281/posts/eS3BKSn5T6j) &mdash; content and formatting may not be reliable*
