---
layout: post
title: "My next task.... A customer is asking me to do a photo on a piece of black granite"
date: August 23, 2016 13:47
category: "Object produced with laser"
author: "Bob Damato"
---
My next task.... A customer is asking me to do a photo on a piece of black granite. Ive done clip art stuff on it with great results but on my few test pieces so far, photos arent coming out as well as I hoped.  I have my focus right on the surface, and I have my power way down. Just enough to mark it.





**"Bob Damato"**

---
---
**greg greene** *August 23, 2016 14:00*

Isn't there a 'Marking' choice as well as engrave and cutting?


---
**Bob Damato** *August 23, 2016 14:08*

Yes, I believe there is, and Ive wondered what that was for! (we all know how good the docs are). Is that what its for? Well this is embarrassing.  how would that work on wood too?


---
**greg greene** *August 23, 2016 14:21*

It has on wood I've done - haven't tried other materials yet


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 23, 2016 14:31*

Are you using stock software? If so, I'd highly suggest pre-processing your photo by using a dithering or half-tone pattern. You will probably be best to do a few tests on some cheap material (e.g. ply) first, then test on a scrap of your granite (if you have any) before doing the final run. I imagine it will take a bit of tweaking to get it to the precision of detail that you would like.



Also, with stock software, keep in mind the Pixel Steps setting. 1 pixel step will cause it to create a 1000dpi engrave, whereas 5 pixel steps will be 200dpi. So, on wood, low pixel steps can cause you to have a super blacked out image. On the granite it might be more beneficial to keep it to 1 pixel step for extra clarity in the engrave.


---
**Bob Damato** *August 23, 2016 14:40*

**+Yuusuf Sallahuddin** Yes, I am using stock software and keeping all my settings pretty much default at this point. Those are good tips to try, thank you.  As for processing the image, I am doing the steps someone posted a while ago and that definitely helps a lot!  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 23, 2016 15:00*

**+Bob Damato** I purchased my K40 back in September of 2015 & spent nearly 12 months "testing". I barely did anything except "test" what I can do with it. It's a pretty decent machine, just as you mentioned the software/support documentation is pretty pathetic (or non-existent even).



Due to the stock software/hardware's inability to control power levels as it goes, you're stuck with only 2 real options to get a decent photo done. 1) dither/half-tone, 2) make multiple files of varying shades of the grey from the photo & run each as a separate job & manually adjust the power for each job (e.g. 6 colours = 6 jobs = 4ma/6ma/8ma/10ma/12ma/14ma), or alternatively you could adjust speeds instead of power level (so do all at 10ma @ 500/450/400/350/300/250 mm/s).



The 2nd option would take a much longer time to do however, so unless you're getting paid a fair amount, probably not worth it.


---
**Vince Lee** *August 23, 2016 23:55*

Hmm.  Not sure how linearly multiple passes add up, but it got me thinking that you might be able to get up to 7 power levels with only three passes if you use powers of two (e.g. 4ma, 8ma, 16ma) and some binary math.  Some areas would be included in more than one pass to get the intermediate values (e.g. 4ma + 8ma = 12ma).  It might take a little work to find the correct 3 power levels and then separate the colors and correctly combine them back again into the correct 3 images, but it's something I'd try myself if I only had the stock hardware.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 24, 2016 03:29*

**+Vince Lee** That makes a lot of sense to use 3 power levels & stack some of the intermediate shades together. I did use 1 power at one attempt, for 6 passes (separate files) where each pass successively did less image (i.e. darker colours of the image). I've now converted to Smoothie so, until I get Scott's Switchable kit, I will be sticking to Smoothie/LW3, but once I get the Switchable I'll be able to test that method you suggested (as I'll plug the stock controller back in again).


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/Q6Ws7t1eWpP) &mdash; content and formatting may not be reliable*
