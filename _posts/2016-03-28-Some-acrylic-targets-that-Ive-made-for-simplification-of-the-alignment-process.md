---
layout: post
title: "Some acrylic targets that I've made for simplification of the alignment process"
date: March 28, 2016 00:22
category: "Discussion"
author: "Scott Thorne"
---
Some acrylic targets that I've made for simplification of the alignment process.



![images/cb388122138ac5f739951f36ac405c94.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cb388122138ac5f739951f36ac405c94.jpeg)
![images/b00e422c8b571448ab95dcf35777b8ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b00e422c8b571448ab95dcf35777b8ce.jpeg)

**"Scott Thorne"**

---
---
**ED Carty** *March 28, 2016 01:17*

Sweet


---
**I Laser** *March 28, 2016 02:02*

Assume you've got a red dot going from the tube?


---
**Scott Thorne** *March 28, 2016 02:21*

**+I Laser**...no I don't...why would you assume that? The photo was just to show how it's used...it's on the wrong side of the mirror just to show how to attach the target.


---
**Scott Thorne** *March 28, 2016 02:23*

I've made around 40 these...just to keep up stock....it's way better than using tape. 


---
**ED Carty** *March 28, 2016 02:38*

looks far more accurate and easy to use


---
**Ashley M. Kirchner [Norym]** *March 28, 2016 02:51*

I thought about doing that ... for about 0.02 seconds. Decided it was easier to just draw them in Illustrator, copy paste a bunch of times, print on card stock, then send the same file to cut them out on the laser. I know, I could've done them all in acrylic but it's more fun watching paper catch fire when you forget to adjust the laser settings. :)


---
**I Laser** *March 28, 2016 04:16*

**+Scott Thorne** Should never assume I guess :P



I saw you'd bought a bigger machine and wondered if that had a red dot from the tube. Also thought burning acrylic would be an expensive way of doing it. Like Ashley, cardboard/cardstock is my preference.


---
**Scott Thorne** *March 28, 2016 09:17*

**+I Laser**...I wish it had one from the tube my friend...it has one from the head only...the targets I made were on scrap...lol...not much waste.


---
**Scott Thorne** *March 28, 2016 09:18*

**+ED Carty**...it is...I never have to clean my mirrors afterwards. 


---
**I Laser** *March 28, 2016 10:23*

Fair enough, don't currently do enough acrylic to have scrap. :(



There was talk of beam splitting at one stage, I was just day dreaming of being able to calibrate the mirrors without firing a single test shot!


---
**Scott Thorne** *March 28, 2016 11:42*

**+I Laser**...I've priced a beam combiner and they are expensive...plus they have to be aligned also. 


---
**Ashley M. Kirchner [Norym]** *March 28, 2016 15:19*

Yes, beam combiners are expensive, then you need the mounting hardware, and they too need to be aligned.


---
**I Laser** *March 28, 2016 22:13*

Well that ends that dream then.


---
**Ashley M. Kirchner [Norym]** *March 28, 2016 22:38*

Well, not necessarily. Just because <b>we</b> told you not to, doesn't mean you have to actually not do it. I mean, if I told you to jump off of the cliff, you aren't just going to do that. If, to you, going the beam combiner route makes sense and you want to experiment, then by all means go ahead and do that. I'm sure there are folks on here who did go that way, who have done that, and who are happy with it. There probably aren't a lot of them, but they're out there. There are some machines that come with it. All we can do is give you recommendations, whether positive or negative ...


---
**Scott Thorne** *March 28, 2016 22:42*

**+I Laser**...I just see no need for it...my head came with a pointer that shows where the cut will be...the fact that the combiner has to be aligned as well as the beam kinda makes it pointless ...just my opinion of course.


---
**I Laser** *March 29, 2016 03:26*

**+Ashley M. Kirchner** Ha sorry, forgot the smiley face at the end. Unfortunately this medium doesn't carry sarcasm well lol!



**+Scott Thorne** Was just thinking how nice it would be to turn on the red dot and go through calibrating instead of the current, test fire, adjust mirror, test fire, adjust mirror, ad nauseam...



Given the expense of the combiner and need to calibrate also it's not really worth it. Especially on such a cheap machine!



But how about a red dot setup that slips onto the end of tube?? Just thinking out loud. Could even calibrate with the machine off, saving having to 'release' the head and also would save **+Yuusuf Sallahuddin**'s hands!


---
**Scott Thorne** *March 29, 2016 09:14*

**+I Laser**...I made one but the problem with them is the the beam differs a bit from the co2 beam so they actually coincide. 


---
**I Laser** *March 29, 2016 11:05*

**+Scott Thorne** not sure I completely understand, you'd want them to coincide. I'm assuming (I know I shouldn't....) that's a typo and the red dot and the laser don't hit the same spot?


---
**Scott Thorne** *March 29, 2016 11:51*

Lol...yes....they didn't in my case anyway....I worded that all messed up..sorry man...I left out the word ..dont.


---
**I Laser** *March 29, 2016 12:42*

All good. Well that sucks then, second dream is dead. :D


---
**Scott Thorne** *March 29, 2016 12:58*

**+I Laser**....I could have just been the way I made mine...the 2nd problem I found was my tube was too close to the mirror....but that was the way my machine came set up.


---
**Ashley M. Kirchner [Norym]** *March 29, 2016 15:28*

Mine came literally up against the mirror. I had to undo all the metal straps to be able to move it back a bit.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/JhJYYA8VkiX) &mdash; content and formatting may not be reliable*
