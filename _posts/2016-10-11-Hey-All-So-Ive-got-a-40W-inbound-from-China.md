---
layout: post
title: "Hey All, So I've got a 40W inbound from China"
date: October 11, 2016 01:55
category: "Discussion"
author: "Kris Sturgess"
---
Hey All,



So I've got a 40W inbound from China.



While I anxiously await its arrival I'm working on getting things ready this side.



Currently 3D printing a new exhaust adapter.

Got a 5gal bucket and Distilled water

Powerbar

CO2 Glasses (Inbound E-Bay)



This unit is coming with CorelDraw/LaserDrw. 



My basic understanding is I will be drag/dropping or pasting into LaserDrw???



Is there a preferred file format?



I'm playing around with Inkscape converting files to vector. Hope that will be useful?



Still learning....



Kris





**"Kris Sturgess"**

---
---
**Phillip Conroy** *October 11, 2016 07:25*

I am still learning after 18 months,make sure add a water flow switch and fit it when it comes in,also have water tank at laser cutter level,read ,read and read some more 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 11, 2016 11:59*

If you're planning to use the CorelDraw software flow, there is a plugin that goes with that called CorelLaser. You open CorelLaser.exe & it opens CorelDraw (with the plugin toolbar active). So, you can import directly into CorelDraw then. With the version that shipped with mine (CorelDraw12) I couldn't import SVG files, so I had to use Adobe Illustrator (.AI) files from v9 (anything newer would fail). Not sure what options there are from Inkscape, but there will be a suitable format.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/hC1oficKfaT) &mdash; content and formatting may not be reliable*
