---
layout: post
title: "Does anyone know how to seal Slate after it has been engraved as when I have engraved it the image starts to flake off after a while and I was wondering if there is any thing that can be done to prevent this, I don't want to"
date: April 05, 2016 21:08
category: "Discussion"
author: "Norman Kirby"
---
Does anyone know how to seal Slate after it has been engraved as when I have engraved it the image starts to 

flake off after a while and I was wondering if there is any

thing that can be done to prevent this, I don't want to go 

down the lines of varnish as I like the look of natural slate





**"Norman Kirby"**

---
---
**Anthony Bolgar** *April 06, 2016 02:04*

I use mineral oil to seal the slate. Nice thing is that you can also use it on wood that will come in contact with food, it is food safe, but will not become rancid like vegetable oils. The slate retains the natural look after the mineral oil absorbs, usually after 2-3 days.


---
**Norman Kirby** *April 06, 2016 08:01*

thanks Anthony, will try it


---
**Scott Marshall** *April 06, 2016 10:15*

Future floor wax is what I use on the slate under my pellet stove. Oils tend to hold the dust/ash. It's not 100% natural shinewise, just a little shinier than dry slate. Only need to do it once a year.



I've got the slate scraps/leftovers from when I put the stove in, been meaning to try them in the laser.... now you reminded me.


---
*Imported from [Google+](https://plus.google.com/103798744920995011573/posts/WNJ184NBbjw) &mdash; content and formatting may not be reliable*
