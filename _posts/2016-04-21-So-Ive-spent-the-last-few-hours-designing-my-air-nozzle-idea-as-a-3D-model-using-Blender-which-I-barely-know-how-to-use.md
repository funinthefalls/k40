---
layout: post
title: "So I've spent the last few hours designing my air nozzle idea as a 3D model using Blender (which I barely know how to use)"
date: April 21, 2016 09:48
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I've spent the last few hours designing my air nozzle idea as a 3D model using Blender (which I barely know how to use). I'm vaguely satisfied with the idea, however there seems to be issues in the Blender software which makes the file have issues. E.g. cannot execute boolean (when I try punch sections out of other sections). So with a bit of fiddling, I managed to get something vaguely workable, although I think I would like to redo it. Any suggestions for decent/simple 3D software to create files ready for 3D print?



The picture below shows the nozzle idea (sliced in 2 so you can see the internal channels). So there are 8 nozzles surrounding the ring, all angled to hit perfect focal point of 50.8mm from the lens (assuming the ring is bolted with the base of the ring flush with the lens base). I would prefer to put more nozzles, but my inexperience with Blender made it an extremely difficult process. The overall idea is to create a cone of air coming from the ring surrounding the lens bracket, focusing all the way to the engrave/cut point, to prevent any smoke being able to get anywhere near the lens.

![images/177a29ef70512f52366317b6ff4b6390.png](https://gitlab.com/funinthefalls/k40/raw/master/images/177a29ef70512f52366317b6ff4b6390.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**andmif** *April 21, 2016 10:57*

I tried blender  couple times and felt overwhelmed with features (granted I am not building any complex 3d models).

For simple visual editor, you can try Autodesk 123d ([123dapp.com](http://123dapp.com)). Autodesk has a family of apps for different aspects of 3d modeling, but 123d should work great for your model.

I would also recommend looking into OpenSCAD ([openscad.org](http://openscad.org)). Its instructions based editor and the main benefit is that you can easily change parameters of model elements (like experimenting with  nozzle diameter or angle in your model).This is my primary editor at the moment.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 11:01*

**+andmif** Thanks for that. I'll take a look into both those software you mention.


---
**Phillip Conroy** *April 21, 2016 11:13*

Where are you trying to get the air to hit on the work peice?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 11:51*

**+Phillip Conroy** Pretty much spot on the area that is being focused on. So right on the 50.8mm focal point away from the lens, right where the beam hits.


---
**Francis Lee** *April 21, 2016 12:28*

OpenScad. OpenJScad is a way to test it out (similar but not exactly the same) online through your web browser!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 13:59*

**+Francis Lee** Thanks for that. I've downloaded OpenScad, so will give it a go later sometime. I just had an attempt with Autodesk 123d Design. That turned out to be very simple for my needs (even assisted me with being able to rotate 18 nozzles around the centre axis for the air to exit). Now hopefully I can get it printed with 1mm precision for the air to escape & give it a test.


---
**Francis Lee** *April 21, 2016 14:01*

**+Yuusuf Sallahuddin** good luck!


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/NpcVS8zKswa) &mdash; content and formatting may not be reliable*
