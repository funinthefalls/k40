---
layout: post
title: "I want to add an air assist to my K40 but I'm having troubles finding a cable drag chain that's not too big...."
date: April 28, 2017 19:22
category: "Air Assist"
author: "Andy Knuts"
---
I want to add an air assist to my K40 but I'm having troubles finding a cable drag chain that's not too big.... 

I have some cable chains here but when folded they measure about 100m and that's too big. I only have about 60mm or I lose working area in my K40.

Anyone here using an air assist in combination with a cable chain? 

Do you happen to have a link to the cable chain you're using?



Thanks





**"Andy Knuts"**

---
---
**Ashley M. Kirchner [Norym]** *April 28, 2017 20:14*

This is what's in my machine: [ebay.com - Details about  Machine Tool 7 x 7.3mm Cable Carrier Drag Chain Nested L6G2](http://r.ebay.com/6XjG1P)

You can see how I have it on my gantry in this video: 
{% include youtubePlayer.html id="1TuKdvnA5lE" %}
[https://www.youtube.com/watch?v=1TuKdvnA5lE](https://www.youtube.com/watch?v=1TuKdvnA5lE)


---
**3D Laser** *April 28, 2017 21:05*

Use a coiled tube instead of a cable drag it works great and super easy to install


---
**Phillip Meyer** *April 28, 2017 22:31*

With the cable drag chain I bought you could just pry the segments off with a screw driver and make it shorter or maybe I misunderstood you


---
**Mark Brown** *April 28, 2017 23:28*

**+Phillip Meyer** You misunderstood (I did at first).  He's measuring the width of the chain, or more specifically the tightest radius it will fold into.


---
**Mark Brown** *April 28, 2017 23:29*

**+Ashley M. Kirchner** Are you the one with the chain running across the top of the gantry?  I kind of liked the look of that, rather than my (the usual) setup.


---
**Ashley M. Kirchner [Norym]** *April 28, 2017 23:42*

**+Mark Brown**, I'm one of a few that has it that way, yes. There's one running ontop, and one running along the right side (on the cabinet floor).


---
**Joe Alexander** *April 29, 2017 00:59*

I printed one of mine and it has a much tighter radius than the bought one. You can see both in the photo, the one on the gantry being the printed one. 

![images/d36bf9b95de09287e6d2c25fa59f99ea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d36bf9b95de09287e6d2c25fa59f99ea.jpeg)


---
**Ashley M. Kirchner [Norym]** *April 29, 2017 01:18*

Dunno **+Joe Alexander**, the 7x7 I'm using has a very tight radius. It <b>looks</b> even tighter than what you have printed there but it could just be the angle too. Being that mine is mounted on the top of the gantry, I needed to make sure the head can travel all the way to the front without the drag chain hitting the cover. It doesn't, there's still room if I wanted something with a less tight radius (which I don't need.) Look at the video I posted in my first comment, somewhere around 1:30 you can see it in action.


---
**Joe Alexander** *April 29, 2017 01:42*

yea that's about at tight as mine but not all are, and 3d printing them was waay cheaper to me :) still like your orientation better than mine.


---
**Ashley M. Kirchner [Norym]** *April 29, 2017 02:46*

It's $3 (free shipping) for 1 meter of the chain. While I have a 3D printer, there are other things I do with it. Spending the time to print that just wasn't worth it for me. :)


---
**Steve Clark** *April 29, 2017 03:22*

Wow... that is small Ashley, Mine is 10mm x 15mm a little big but works find. I live within 25 minutes of Lightobject andas I recall they may have one in between. Next time I go up there Ill have to check them out. Something in between would be nice to find. Also mine is like Phillip's you can take off or add segments with a flat screwdriver.


---
**Ashley M. Kirchner [Norym]** *April 29, 2017 03:44*

Yep, the tube isn't very big at an OD of 5mm. There's plenty of room still left in the chain to add wires for the laser pointers I plan on adding later. I just bought 1 meter of it, split in half and run one above the gantry and the other half along the side.


---
**Andy Knuts** *April 29, 2017 19:17*

**+Joe Alexander** I tried priting a cable chain on my ultimaker 3 but i can't get them to move smoothly. It's  a very very tight fit. I had to use a lot of force to get them connected together and it's not moving smoothly... can you tell me where you got the STL's from?




---
**Joe Alexander** *April 29, 2017 22:19*

[thingiverse.com - Cable Chain and Mounts for K40 Laser by DAcreates](http://www.thingiverse.com/thing:1275013) 

here's the link for the one I used. Moves fairly smoothly, just make sure to clean out the dimples and any burrs on the internal surfaces.


---
**Craig “Insane Cheese” Antos** *April 30, 2017 13:08*

**+Andy Knuts** I've 3d printed cable chain before. It starts stuff, but once it's run in it should be fine. I had to pull mine apart to clean the links. In the end, I just buy it now. For a few dollars, the time and effort savings where worth it.


---
**Ashley M. Kirchner [Norym]** *April 30, 2017 20:12*

Exactly why I also just buy it. $3 plus a few days is worth it versus

holding up the printer to print those.


---
**ALFAHOBBIES** *May 01, 2017 16:14*

I used this one from amazon. Fit just fine.



[amazon.com - uxcell R18 10mm x 15mm Black Plastic Cable Drag Chain Wire Carrier 1M Length for CNC - - Amazon.com](https://www.amazon.com/gp/product/B01M0WRHUN/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1)






---
**Steve Clark** *May 02, 2017 00:59*

Alfahobbies, That is the same one I have. I think I got it off E-Bay.


---
*Imported from [Google+](https://plus.google.com/101382109936100724325/posts/B8YyAPLaxTe) &mdash; content and formatting may not be reliable*
