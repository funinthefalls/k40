---
layout: post
title: "Hi Guys, I wondered if anyone could help me"
date: December 06, 2016 11:19
category: "Software"
author: "josh raines"
---
Hi Guys, I wondered if anyone could help me. Im trying to locate the Drivers online for the K40 printer. I been given the task of set up but the fundimental memory stick is missing. 



If anyone can point me in the right direction i would be very grateful :)



Many thanks 

![images/507e611c683bb6d55f19cb115f0e879b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/507e611c683bb6d55f19cb115f0e879b.jpeg)



**"josh raines"**

---
---
**Andy Shilling** *December 06, 2016 11:39*

What control board do you have in it?, from what I understand most won't work without the USB key.


---
**Tim Gray** *December 06, 2016 12:26*

the memory KEY has a serial number coded to the Corel Draw install.  it wont work until you find it, or rip out the guts and install a different controller board.


---
**Jim Hatch** *December 06, 2016 14:12*

You might be able to get a usb stick from someone who has replaced their stock board for a Smoothie upgrade if you post a request here.




---
**Steve Anken** *December 06, 2016 14:42*

Now is the perfect time to replace the controller with a Cohesion3D smoothie board. Drop in replacement with all the right connectors and cables for $100. Pop some beer money to the LaserWeb folks and you also get an active development team working on the next killer version of LaserWeb.


---
**josh raines** *December 06, 2016 14:52*

Thanks for  your help - I have managed to find a download link for various bits of software. It has the CorelLaser software and some other bits but still not having much luck. i have noticed there is a Ukey file though, i wondered if this is what i was after?  



I beleive the board which i have in there at the moment is a Moshisoft MS10105 V4.1 

![images/fde599f53c61a66a418191048437f114.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fde599f53c61a66a418191048437f114.jpeg)


---
**Andy Shilling** *December 06, 2016 15:11*

Right moshi boards do need a USB key but give me a little time and I might be able to find a dongle free version.


---
**josh raines** *December 06, 2016 15:17*

Thank you very much - OK i will see what i can find in the mean time. All of this has helped my understanding massively - Cheers


---
**Andy Shilling** *December 06, 2016 15:26*

I've had a quick look but I'm on my phone,  I will have to post the link when I get home later if that doesn't it might be worth looking into cloning a dongle.


---
**josh raines** *December 12, 2016 11:49*

Hi **+Andy Shilling** did you manage to stumble upon a dongle free version? - also with your comment about cloning a dongle, how would we go about this?


---
**Andy Shilling** *December 12, 2016 11:53*

I've looked into both, I did tag you into a post but it turns out although the software will run, it will not talk to the laser. Also I've not found any way to clone the dongle. EBay will be your best bet for that now. There are a few moshing boards on there.


---
**josh raines** *December 12, 2016 12:49*

No problem, yeah a couple of our guys have been looking into that, currently deciding whether just to upgrade with new control boards and going away from Moshi. Hopefully one way or another we can get her running soon!


---
**Andy Shilling** *December 12, 2016 12:54*

I had mine for a month before deciding to upgrade, I've found moshi ok for engraving but that's it. I'm waiting on my Cohesion3d board to arrive then mine get a complete overhaul. I dont know how many of us on here are from the UK but maybe someone will have a moshi usb you could buy.


---
*Imported from [Google+](https://plus.google.com/101216807486891708297/posts/DqZinmtmEmS) &mdash; content and formatting may not be reliable*
