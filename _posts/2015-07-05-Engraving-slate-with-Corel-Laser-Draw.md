---
layout: post
title: "Engraving slate with Corel Laser Draw"
date: July 05, 2015 18:39
category: "Materials and settings"
author: "David Richards (djrm)"
---
Engraving slate with Corel Laser Draw.


**Video content missing for image https://lh3.googleusercontent.com/-hlJsRTnjN90/VZl5xPgrVCI/AAAAAAABnzI/sI_hcbtfmTc/s0/VID_20150705_184758.mp4.gif**
![images/e08ce3fb95be5c94466037cf5a03ce6a.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/e08ce3fb95be5c94466037cf5a03ce6a.gif)



**"David Richards (djrm)"**

---
---
**David Wakely** *July 05, 2015 21:19*

What speed and ma are you engraving at? When I do slate I use 10ma and 200mms yours looked slower


---
**David Richards (djrm)** *July 05, 2015 22:40*

Hi, I think this was engraved at 120 mm/s, 15 mA. I'll check tomorrow. I have no idea what is optimal for this material. I read somewhere not to over do it - perhaps I am doing.﻿


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/jJx5rNcB9Ts) &mdash; content and formatting may not be reliable*
