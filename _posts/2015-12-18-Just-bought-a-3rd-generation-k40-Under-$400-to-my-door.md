---
layout: post
title: "Just bought a 3rd generation k40, Under $400 to my door"
date: December 18, 2015 15:35
category: "Discussion"
author: "Scott Marshall"
---
Just bought a 3rd generation k40, Under $400 to my door. 



Ordered an air assist head and lens/mirror set as well.



I know it will need some work, but it's what I did for 40+ years. Pretty sure it'll be a decent machine when I'm done. It's my learner. I was going to build a larger one, but for the price, it's a great way to learn the ways of the laser cutter, and I'll be better able to design a larger one that will suit my needs.





**"Scott Marshall"**

---
---
**Scott Marshall** *December 18, 2015 16:48*

OUCH! You guys must pay dearly for a lot of stuff. 



Shipping on this unit was about $30us for a 3000mile ship from the US port, so  I have to believe they are absorbing some of the shipping.



I did a lot of checking into this deal before I forked over 400 bucks, They are bringing in containers of these from China (and the larger models) to the Port of LosAngeles California, so we only pay trucking from there, still it's the full width of the US to New York where I live, and they're shipping it for $30?? That's 10 cents a mile!



Wholesale cost on these out of China has to be <$300 for any money to be made after shipping, paperwork and freight handling. Guess they are earning it.



Surprising they aren't doing the same thing for South Africa, it's a big place and you'd think they could sell them there. Maybe it's a customs issue with the number of countries they  would have to sell to?



Sounds like you're paying door to door and that's a killer.

Guess we don't appreciate how well off we are in a lot of ways.



Scott


---
**Andy Hayward** *December 18, 2015 17:12*

Where did you buy from?


---
**Scott Marshall** *December 18, 2015 17:18*

**+Andy Hayward**

"Globalfreeshipping" on ebay. $397 shipped


---
**ChiRag Chaudhari** *December 18, 2015 23:16*

I purchased mine form here: [http://www.ebay.com/itm/260825065645](http://www.ebay.com/itm/260825065645) Delivered by Fedex on 7th day. First couple of days the tracking was not even working and then BAM! right at the doorstep. 



The reason stuff can be shipped from china to US so cheap is because the deal they have stuck with all the carriers due to the amount of packages they ship. The prices are so ridiculously cheap its un freekin believable. For an example wife ordered some small jewelry items and they were like $0.69, $0.89, $1.29 with free shipping. :o . The cheapest way to ship anything within US 1-3oz is $2.54. Plus you have to ship them in bubble mailer . Pretty crazy. 



Good thing is our Custom Duty Laws are pretty easy on consumer. And if you know what you are doing, you can always tell the shipper to put the certain HTS Code so you dont hit by custom duty charges :)  


---
**Andy Hayward** *December 18, 2015 23:26*

**+Scott Marshall** Thanks, I'll have a look.


---
**I Laser** *December 20, 2015 21:54*

Wow thanks for the heads up guys, there are some great deals if you're looking to buy a k40 at the moment. I picked up a secondary unit for $326 USD delivered! [http://www.ebay.com.au/itm/Hot-High-Precise-Speed-40W-USB-CO2-Laser-Engraver-Engraving-Cutting-Machine-/331632511696?hash=item4d36d68ad0:g:49cAAOSwgQ9V1ESE](http://www.ebay.com.au/itm/Hot-High-Precise-Speed-40W-USB-CO2-Laser-Engraver-Engraving-Cutting-Machine-/331632511696?hash=item4d36d68ad0:g:49cAAOSwgQ9V1ESE)

Will be interested to see how long it takes, as they claim local stock and what the quality is like compared to my current k40.


---
**ChiRag Chaudhari** *December 20, 2015 21:57*

For $326? Wow from where? That's a score.


---
**I Laser** *December 20, 2015 22:38*

Link in the comment ;) I'm in Australia, so not sure whether you'll get them as cheap in the US.


---
**ChiRag Chaudhari** *December 20, 2015 22:40*

Ah ok, I saw the link lead me to Aussie ebay site and you may wanted to say $426 not $326. But I was like holy cow, for $326 that is Christmas !!!


---
**I Laser** *December 20, 2015 22:42*

$326 USD, it is $450 AUD delivered ;) Which is more than $100 AUD cheaper than my current unit.



Not getting too excited about the deal until it's here and running.


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/faFHQqkzHJF) &mdash; content and formatting may not be reliable*
