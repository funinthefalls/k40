---
layout: post
title: "Template routing with a laser cut template - Years ago my brother-in-law worked for Blockbuster for a time after he got out of the army"
date: December 29, 2018 18:07
category: "Object produced with laser"
author: "Ned Hill"
---
Template routing with a laser cut template -



Years ago my brother-in-law worked for Blockbuster for a time after he got out of the army.  He met his wife there who was also a manager.  They live in TX now and she found one of his old name tags when she was doing some cleaning.  She sent it to me to "do" something with. 



 I settled on doing the Blockbuster logo with the tag inlayed into the wood.  I wanted to use a piece of 1" cherry and it gave me a chance to try something that I've been meaning to try , template routing. 



 I guess you can say template routing is a bit like a poor man's CNC. ;)  You create a template from 3mm ply and attach it to a piece of wood with double sided tape.  You then cut the wood close to the template with a bandsaw.  This then allows you to use a router pattern/flush trim bit, which has a flush bearing guide at top or bottom of the cutting head, with a router table.  The bearing rides on the pattern to create an exact copy in the attached wood.  I used a combo bit that had guide bearings at the top and bottom.  This allowed me to flip the piece to guide at a different orientation, without changing bits, to avoid routing "up hill" on the end grain curves.  Otherwise I would have been fighting tear out.   Turned out pretty good.  Finished it off with some wipe on poly.  I next want to try one of those spiral upcut bits that have a flush bearing attached on the end so I don't have to worry with tear out.



![images/23f97f0e482962a53f21c6776f7579b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/23f97f0e482962a53f21c6776f7579b1.jpeg)
![images/6ea56c9aa4d8d29e0c98102a3dea6a92.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6ea56c9aa4d8d29e0c98102a3dea6a92.jpeg)
![images/fb7e34eb12cee70150a26631fe89ef35.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb7e34eb12cee70150a26631fe89ef35.jpeg)
![images/a5895257c3440690a84917cf5adb3f6a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a5895257c3440690a84917cf5adb3f6a.jpeg)
![images/61bdde53c5fa222d9dff585f7495301f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/61bdde53c5fa222d9dff585f7495301f.jpeg)

**"Ned Hill"**

---


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/guBgZswRvd3) &mdash; content and formatting may not be reliable*
