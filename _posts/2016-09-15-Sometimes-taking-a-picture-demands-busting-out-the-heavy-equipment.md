---
layout: post
title: "Sometimes, taking a picture, demands busting out the heavy equipment ..."
date: September 15, 2016 04:23
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
Sometimes, taking a picture, demands busting out the heavy equipment ...



![images/37478a853aa20acfaa4a2d54f031d8cf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/37478a853aa20acfaa4a2d54f031d8cf.jpeg)
![images/90e34e37013d6e1a64fc3a13ec2f9531.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/90e34e37013d6e1a64fc3a13ec2f9531.jpeg)

**"Ashley M. Kirchner [Norym]"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 15, 2016 04:56*

I like the backdrop sheeting you've used. Suits the model well.


---
**Carl Fisher** *September 15, 2016 12:14*

What tripod is that? I'm in the market for a new one but not sure what I want.


---
**Ashley M. Kirchner [Norym]** *September 15, 2016 17:49*

**+Yuusuf Sallahuddin**, it was either that, or the other side which is slightly lighter and the patches are more blended together for a softer feel.


---
**Ashley M. Kirchner [Norym]** *September 15, 2016 17:50*

**+Carl Fisher**, it's an old Manfrotto, with a pistol grip at the top. That thing is at least a decade old and heavy. I rarely take it anywhere, just too heavy to lug around.


---
**Carl Fisher** *September 15, 2016 18:09*

Great, thanks. Looks like a nice setup.


---
**Ashley M. Kirchner [Norym]** *September 15, 2016 18:38*

I had to 'make due' with what I have on hand. I'm missing one of my light stands, so I couldn't light the thing properly.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/38iCpmLGW3o) &mdash; content and formatting may not be reliable*
