---
layout: post
title: "Hello everyone, I first want to thank Scott Marshall for helping me get my laser machine working"
date: September 12, 2016 18:05
category: "Discussion"
author: "Rod Presnell"
---
Hello everyone,

  I first want to thank Scott Marshall for helping me get my laser machine working.  I have been reading a lot of threads about different types of software to use.  Has anyone here done a tutorial that starts from setting up the laser machine to installing the software and tips or tricks to get a nube like me started?  BTW my machine is a K40 with the M2 Nano board.





**"Rod Presnell"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 12, 2016 19:01*

I'm pretty sure no-one has done a full comprehensive guide for setup to cutting/engraving.



A simple list is as follows: 

- Check pump works

- Check fume extraction fan works

- Check lens orientation (should be with the bump/convex side facing up)

- Check alignment (there are guides linked in this community's previous posts)

- Install either LaserDRW or CorelDraw with the CorelLaser plugin

- After installing whichever software, enter the Board Model (choose from dropdown box) & enter the device ID (which you will find on the control board, in the compartment below the switch panel).



The USB stick does need to be inserted in order to use the software. Without it inserted the software is "locked" & won't send commands to the laser.



Can't think of anything else off the top of my head. But feel free to ask away if anything gets you stuck. Someone will have an answer.


---
**Rod Presnell** *September 12, 2016 19:13*

Great thanks for your input.


---
**Martin McCarthy (NGB Discos)** *September 15, 2016 14:30*

**+Yuusuf Sallahuddin** The device Id you mentioned,  is this on the large white sticker that is on the motherboard? 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 15, 2016 21:46*

**+Martin McCarthy** Yeah, that's the one. Should be 16 characters if I recall correct.

See this: 

[https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing)
It's the sticker on the right of the board.


---
*Imported from [Google+](https://plus.google.com/103834600584593086520/posts/f4iDUnKSS3n) &mdash; content and formatting may not be reliable*
