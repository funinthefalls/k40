---
layout: post
title: "Hi all, Few months ago i have a problem with my 40 watt laser tube was that loosing power and arcing on the node side"
date: March 05, 2018 02:16
category: "Discussion"
author: "Alisha kansha"
---
Hi all,



Few months ago i have a problem with my 40 watt laser tube was that loosing power and arcing on the node side. Finally i end up building the new one with 60 watt laser tube. I'm in the middle of testing the laser tube and optical alignment but i have a problem during alignment and hope some can help me here.



So, my laser tube is moving along with the Y axis. I did optical alignment and have a great result, all the laser on the same spot start from X0 and X800 which is the maximum travel of X axis. I try doing some simple cut and engraving and all is quite good until i realized when i did a cutting job in the maximum X and middle of Y where i can not cut a 3 mm plexi glass.



After doing some check i can see the laser spot is getting lower and lower when moving the Y axis. The distance between spot at X800, Y0 and X800, Y 750 is around 10 mm. Currently i suspect this is do to the misaligned of mechanical part where the the motion part (linear guide ) is placed. I think the right side of the linear guide is higher at maximum Y axis. 



Is there any reasonable root cause other than mechanical problem?



Thanks 

![images/b66b6fe808f151695ddf9211a2605222.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b66b6fe808f151695ddf9211a2605222.jpeg)



**"Alisha kansha"**

---
---
**HalfNormal** *March 05, 2018 15:34*

**+Alisha kansha** Definitely a tube/gantry alignment issue.  



Here is a link to a few different resources on what to look for while aligning mirrors.



[https://www.everythinglasers.com/forums/topic/great-alignment-sources/](https://www.everythinglasers.com/forums/topic/great-alignment-sources/)


---
**Alisha kansha** *March 05, 2018 16:47*

**+HalfNormal** I did try to adjust the tube posision by increasing the height but its still the same. So the only thing that may cause ia only the gantry alignment. Will try to check this on the weekend.


---
**HalfNormal** *March 05, 2018 16:50*

What I was trying to convey was that the tube and gantry are not parallel. Sometimes it is easier moving one over the other depending on how bad the misalignment is.


---
**Fabiano Ramos** *March 05, 2018 17:52*

I had a similar problem and in my case, the solution was to align the closer to 90º, the lens holder that is just off the laser tube.



I used this guide, and I did it.

[https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/](https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/)



For me one of the best guides I found on the internet.


---
**Alisha kansha** *March 06, 2018 05:36*

**+Fabiano Ramos** did you have the same problem as mine and can solve by adjusting the 1st mirror? since my tube is move along with Y axis, so mirror 1 and 2 will also moving. The only possible reason for me is the alignment of the gantry


---
**Fabiano Ramos** *March 06, 2018 18:10*

**+Alisha kansha**  yes. I adjusted following the guide that I posted for  you and apparently, according to his own information, is a symptom of not framing the second mirror, relative to the first in exact (or approximate) 90 degree angle.



However, So what I did was unscrew the screws as in the picture, and move GENTILLY left or right as directed in the link I sent. Even fully center on both mirrors. It's laborious, it takes time, but it works.



P.S. There is also a probability of having a small piece of paper underneath the mirror (Image), to tilt it slightly down and compensate for the alignment of the mirror.

![images/c0d98811e415e881345ae3678518618a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c0d98811e415e881345ae3678518618a.jpeg)


---
**Alisha kansha** *March 07, 2018 00:24*

I'll see if this can solve my problem. Thanks


---
*Imported from [Google+](https://plus.google.com/112428355097604312279/posts/1Ryxoi29xTu) &mdash; content and formatting may not be reliable*
