---
layout: post
title: "The mother board in my computer needed to be replaced"
date: October 13, 2016 20:49
category: "Original software and hardware issues"
author: "John von"
---
The mother board in my computer needed to be replaced. The computer now works fine but I keep getting a message that the laser is not connected. The green light on the board under the fan goes on, the fan blows, and the engrave fires a test laser beam. Any suggestions on what to do?





**"John von"**

---
---
**greg greene** *October 13, 2016 20:58*

did you enter the required model # and serial number of the controller board into the software? Is your USB port active?  Can you see it in the OS?


---
**Jim Hatch** *October 13, 2016 21:51*

Also, is the USB key plugged into your computer & not a hub? The USB cable firmly seated on both ends - PC and the laser? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 13, 2016 22:59*

This issue has come up for a few other users where the laser cannot be "seen" by the pc. I am not sure if any of them ever found a fix, since I haven't seen any replies from them about it.


---
**Mike Mauter** *October 14, 2016 01:47*

I have had this happen. I uninstalled and then reinstalled all of the software. The main thing is that the computer seems to loose the driver information. On my disk it is located under "The Latest CDR Software" and then in "Laser Engraving Drive" run the setup.exe file. Sometimes it takes running that file several times for the drivers to install.


---
**John von** *October 14, 2016 12:46*

Thanks Mike. I will try this approach and will report back. Thanks again.


---
*Imported from [Google+](https://plus.google.com/112642592493783290997/posts/bb2uKzTdupi) &mdash; content and formatting may not be reliable*
