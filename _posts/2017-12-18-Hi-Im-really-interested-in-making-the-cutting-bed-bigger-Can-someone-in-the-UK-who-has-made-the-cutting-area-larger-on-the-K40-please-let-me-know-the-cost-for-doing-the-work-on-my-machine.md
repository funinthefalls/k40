---
layout: post
title: "Hi I'm really interested in making the cutting bed bigger, Can someone in the UK who has made the cutting area larger on the K40, please let me know the cost for doing the work on my machine ?"
date: December 18, 2017 21:40
category: "Modification"
author: "Frank Farrant"
---
Hi



I'm really interested in making the cutting bed bigger, 



Can someone in the UK who has made the cutting area larger on the K40, please let me know the cost for doing the work on my machine ?



I can send the machine to you, or depending where you are, you could work on it here ?



Thanks

Frank





**"Frank Farrant"**

---
---
**Joe Alexander** *December 18, 2017 23:40*

its substantially easier to just buy a larger 50w unit rather than expand as it either gains you maybe a few cm or requires replacing the entire gantry. even then your limited to the size of the enclosure minus some room for electronics. check out UK40OB on openbuilds. [openbuilds.com - UK40OB](https://openbuilds.com/threads/uk40ob.7790/) 


---
*Imported from [Google+](https://plus.google.com/110419041310362555971/posts/c2GnEurcr4s) &mdash; content and formatting may not be reliable*
