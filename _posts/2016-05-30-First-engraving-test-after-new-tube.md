---
layout: post
title: "First engraving test after new tube"
date: May 30, 2016 02:53
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
First engraving test after new tube. 

![images/a17060299fdf103d150529654ab10483.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a17060299fdf103d150529654ab10483.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Pippins McGee** *May 30, 2016 02:56*

can i ask where you purchase new tube from?


---
**Ariel Yahni (UniKpty)** *May 30, 2016 02:58*

Here you go. [http://s.aliexpress.com/U73iI7V3](http://s.aliexpress.com/U73iI7V3) 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 30, 2016 06:26*

Looks good. What is it engraved on?


---
**Ariel Yahni (UniKpty)** *May 30, 2016 11:48*

Painted porcelain tile


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/4oxmF5artZW) &mdash; content and formatting may not be reliable*
