---
layout: post
title: "Hello! I have a K40 machine for 6 months and have 2 problems since then"
date: December 14, 2017 10:14
category: "Original software and hardware issues"
author: "OKS985"
---
Hello!



I have a K40 machine for 6 months and have 2 problems since then.

One that is over 2-3 mA force engraves the points on the image around the letters.

There is no problem with 2-3 mA or less.

The other problem is that the letters and lines are wavy engraved. It does not always do it, sometimes it is completely straight, sometimes a bit, sometimes very wavy.



With the wavy problem I saw that others met, but I could not find a solution. I tried to adjust the strap, the mirrors and the focus lens were stable, the engraving table could not move.



Is there any idea for someone?



Thanks!



![images/2e06e52a9fd9b0bd42b6586857847253.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2e06e52a9fd9b0bd42b6586857847253.jpeg)
![images/ecf54f4dbcf94cbfcaf236d42c52815a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ecf54f4dbcf94cbfcaf236d42c52815a.jpeg)

**"OKS985"**

---
---
**Joe Alexander** *December 14, 2017 15:31*

was your material firmly held in place to prevent it from vibrating from the machines movement? I had a similar issue and fixed it by slowing down my operation.


---
**Steve Clark** *December 14, 2017 20:29*

I had what Joe described happen... a good thought to look at.  



By the way, what is that material? Some sort of laminate? Looks nice.


---
**HalfNormal** *December 15, 2017 03:43*

If you have rollers on your head, make sure they are tight.


---
**Martin Dillon** *December 15, 2017 05:35*

I have engraved and then do a vector outline to sharpen the edges.


---
**OKS985** *December 15, 2017 12:46*

**+Joe Alexander** Everything is stable, no one can vibrate, I have checked.


---
**OKS985** *December 15, 2017 12:48*

**+Steve Clark** Rowmark márkájú 1,6 mm vastag UV álló anyag.


---
**OKS985** *December 15, 2017 12:51*

**+HalfNormal** I also pulled the rollers, the head stays stable.


---
**OKS985** *December 15, 2017 15:10*

**+Martin Dillon** Very good idea :)




---
**OKS985** *December 15, 2017 15:11*

Has anyone seen the points?


---
**OKS985** *January 04, 2018 15:51*

I checked everything I read here, but the letter is always wavy. Any other idea maybe? Help me who can :(

![images/e02d39e098f4f4061f1308a9ac999e6c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e02d39e098f4f4061f1308a9ac999e6c.jpeg)


---
**Steve Clark** *January 04, 2018 17:25*

Maybe things are too tight? I would try loosening the pinch on the lens head rollers for a test. Also, inspect each  of them  for defect or damage and that they rolling freely. 



Are you rastering on the X axis? It appears so., Try changing the raster to  the Y axis and see if the problem follows (Rotates). If so, It may not be a mechanical but a controller problem.



Otherwise, I would consider:



Bad mechanical match between pully and belt in X axis. 



Good Luck.










---
**OKS985** *January 07, 2018 18:39*

**+Steve Clark** I tried to keep the X-axis strap down and then tighten, but nothing changed.

I tried to hold the head rollers down and then relax, but there was no change at that time. The head rollers work perfectly, I've checked.

The focus lens can not move, it's tight in place.



This raster on the X axis what do you mean? How can I change the Y axis?



Sometimes the line is straight, the line is wavy ... can not the software be the problem? I've tried already with Win 10, Win 7, Corel X3, X4, K40 Whisperer, and 2 different PCs, but all of them doing the problem.



Thank you!


---
**Steve Clark** *January 08, 2018 01:30*

Sorry, some software will allow you to raster at selected angles. The default is left and right (X axis back and forth) but it may or may not be selectable with the software you are using.

I currently have a C3D board and have been using LaserWeb SW. But,  I've been looking at a new SW called LightBurn, just released the first of this month. It allows you to define the angle you raster at. i.e. Up and down instead of side to side or even any other angle you define.



You said you checked your lens but what about the mirrors? Or even the laser tube mounts? Also, the output lens on the the tube?... (this one with machine off and allow time for the PS to drain.)



It sure sounds like something is causing your beam to flex. Long shots are your case is twisting or the xy frame is... tough without being there to see it happen.




---
**OKS985** *January 11, 2018 15:19*

**+Steve Clark** 

I check the mirrors, tube holders, all stable.



"the output lens on the tube" Where is this? to check it out?



Do you think the problem is the creator of the power supply or the control panel?



Thanks!


---
**Steve Clark** *January 11, 2018 17:28*

I don't know about the  PS but Don may have some better ideas. It just sounds like it's mechanical. However the fact that you’re seeing the problem at higher ma output certainly suggest some PS involvement.



BTW what is your control board the stock one? If you slow your feed down does the problem go away?



I have seen drifting in servo motors do that but that was due the resolvers issues. I'm not at all that versed in stepper motors and how they hold position other than in theory. I just can’t see how the one axis bump could occur over multiple passes and then go back to its position again electronically. It crys mechanical.  Maybe someone better versed in the position hold control of steppers could speak up…



On big machines like industrial cnc machines we would switch the drivers on the axis to see if the problem followed the board. But I don't think its applicable to these stepper systems.




---
*Imported from [Google+](https://plus.google.com/104721146713196679616/posts/13M4LBxr56P) &mdash; content and formatting may not be reliable*
