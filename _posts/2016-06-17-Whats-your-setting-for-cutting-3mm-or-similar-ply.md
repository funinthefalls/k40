---
layout: post
title: "Whats your setting for cutting 3mm or similar ply"
date: June 17, 2016 15:10
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Whats your setting for cutting 3mm or similar ply﻿





**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *June 17, 2016 15:55*

15/8 or 18/10


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 16:39*

I used multiple passes @ 4mA & 20mm/s currently.


---
**Ariel Yahni (UniKpty)** *June 17, 2016 16:43*

**+Yuusuf Sallahuddin**​ what's the reason for that? 


---
**3D Laser** *June 17, 2016 17:23*

I have been doing two passes at 9ma at 6.5ms. It gets through in most places in one pass it gets through in most places but the second pass make sure it cuts all the way through in some of those stubborn places

 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 17:47*

**+Ariel Yahni** No particular reason other than I find that lower mA seems to give thinner cuts & less burn. Also, I tend to be lazy to change the mA & use it on as low as possible when cutting leather/fabrics. I've found that 4mA is about the lowest I can get my machine to actually do anything (anything lower will register on the ammeter but won't show any visible result on the material).


---
**Ben Marshall** *June 18, 2016 02:27*

I've been cutting 6mm ply at 15 mA at 9.9mms with 2 passes. Cut out falls everytime, usually before the second pass is complete


---
**3D Laser** *June 18, 2016 02:34*

**+Ben Marshall** wow Ben that's crazy you must have your laser dead on!  


---
**Ben Marshall** *June 18, 2016 02:42*

**+Corey Budwine** Yes I'm pretty happy with it! I took my time during set up to get it dialed in really well.  I "bracket" power and speed settings until I'm content with the results. To test those settings I reset everything, reboot the laser and software to ensure I can repeat the results. Works out well so far.


---
**3D Laser** *June 18, 2016 03:31*

So I just cleaned my lenses and mirrors and tweaked the alignment  as I haven't done it in a few weeks and I was able to cut 3mm ply in one pass at 10ma and 6ms I think I could do it at a lower power and higher speed now if I want to so if you are having trouble getting through the material check your alignment and how clean your mirrors and lenses are as that can kill your cutting power 


---
**Ben Marshall** *June 18, 2016 15:31*

**+Corey Budwine**​ do you have stick mirrors and lens or did you upgrade? ﻿


---
**3D Laser** *June 18, 2016 17:47*

**+Ben Marshall** I upgraded I have 20mm si mirrors and a 18mm light object lens with the light object air assist 


---
**Scott Thorne** *June 19, 2016 12:17*

5mA at 18mm/s


---
**3D Laser** *June 19, 2016 12:31*

**+Scott Thorne** you have that fun 78mm lens don't you


---
**Ariel Yahni (UniKpty)** *June 19, 2016 12:31*

**+Scott Thorne**​ you are in a league of your own


---
**Scott Thorne** *June 19, 2016 12:54*

I'm using a meller optics 2 inch focal lens....best lens I have...I paid out the ass for it but it was worth it.


---
**Scott Thorne** *June 19, 2016 12:55*

**+Corey Budwine**...I have one but haven't used it in a while. 


---
**Adam J** *September 07, 2016 22:12*

**+Scott Thorne** Got anymore information about that Meller Optics lens Scott? Do you have a link to it? I'm guessing you have it fitted to a K40 and not some higher end laser cutter? Cheers!


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/18ik5cWrScZ) &mdash; content and formatting may not be reliable*
