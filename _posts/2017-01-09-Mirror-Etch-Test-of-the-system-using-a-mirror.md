---
layout: post
title: "Mirror Etch Test of the system using a mirror"
date: January 09, 2017 17:47
category: "Object produced with laser"
author: "HalfNormal"
---
Mirror Etch



Test of the system using a mirror. It is a mirror tile that is usually used to put on a wall.



![images/2f2c01d03a0fa1e64d87e4ef097d7f4c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f2c01d03a0fa1e64d87e4ef097d7f4c.jpeg)



**"HalfNormal"**

---
---
**Thor Johnson** *January 09, 2017 18:42*

Wow!  What was the speed/power, and did you use something (like rice paper) on top?


---
**HalfNormal** *January 09, 2017 18:45*

**+Thor Johnson** The etch was done on the back of the mirror. There is a coating of a thick black material that I had to burn through so I used a higher power than would be used on a standard mirror. Effect was the glass was etched and gave a frosty look.


---
**greg greene** *January 10, 2017 14:34*

Power numbers are important - as is speed, too much power in a single area for too long will cause the glass to shatter, so please let us know what power/speed you used.


---
**HalfNormal** *January 10, 2017 14:37*

300 ipm @ 9 ma. Like I mentioned above this particular mirror has a coating that also needed removing. 


---
**greg greene** *January 10, 2017 14:42*

Thanks !


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/EgEyL1ttyWG) &mdash; content and formatting may not be reliable*
