---
layout: post
title: "Evening all, finally got my new unit , today I have some 3mm acrylic that I want to engrave but even on the lowest settings on the current regulation it's melting right through"
date: September 24, 2016 20:21
category: "Hardware and Laser settings"
author: "J DS"
---
Evening all, finally got my new unit , today I have some 3mm acrylic that I want to engrave  but even on the lowest settings on the current regulation it's melting right through. Any help would be greatly appreciated . Thanks in advance.





**"J DS"**

---
---
**Ariel Yahni (UniKpty)** *September 24, 2016 20:28*

A super powered laser?? What speed are you using? 


---
**J DS** *September 24, 2016 20:29*

Speed is set at 200mm/ps




---
**Ariel Yahni (UniKpty)** *September 24, 2016 20:32*

Try cutting a 20mm square at 20mms


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 24, 2016 20:52*

You could try setting the pixel steps to 3 or 5 in CorelLaser. This will decrease the density of dots in the final engrave, maybe preventing the burn-through.


---
**Jim Hatch** *September 24, 2016 21:30*

Try 400 or even 500 mm/s for speed. Then adjust the power up to where you get a nice engrave. Make sure it's focused on the surface vs 1/2 way thru the material like you would for cutting.


---
**Ariel Yahni (UniKpty)** *September 24, 2016 21:32*

**+J DS**​ can you post a picture of that engrave? Maybe it's not that bad and we can give you better advice


---
**J DS** *September 24, 2016 21:43*

Sorted it I think

![images/7833694f84a48ac1e03a35571ec773b4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7833694f84a48ac1e03a35571ec773b4.jpeg)


---
**Jim Hatch** *September 24, 2016 21:45*

**+J DS**​ And the answer was?....


---
**J DS** *September 28, 2016 15:09*

That Im a tool :-D


---
**Jim Hatch** *September 28, 2016 15:19*

Operator error? 🙂


---
*Imported from [Google+](https://plus.google.com/102322879087562424255/posts/bo2dkNVhHmu) &mdash; content and formatting may not be reliable*
