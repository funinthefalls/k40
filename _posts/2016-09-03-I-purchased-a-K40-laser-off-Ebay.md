---
layout: post
title: "I purchased a K40 laser off Ebay"
date: September 03, 2016 00:02
category: "Software"
author: "Rix Metal Worx"
---
I purchased a K40 laser off Ebay. I got the laser all dialed in mechanically, but I am having a software issue. It was suppose to have Corel Draw included. I ran the disk, I find Corel Laser,Laser draw and Winsealxp. But do not see Corel draw. I have been told it is hidden, or that I am not in the CD's root directory. But I double click it in my Edrive and it opens on the software folder. The disk is labeled software. So if opening it I am not in the root directory, how do I back up to it. I contacted the Ebay seller but have not received a reply. Any help would be greatly appreciated. The other 3 loaded fine. Corel Laser, Laser draw and the Winsealxp. Somebody else seemed confused saying Corel laser should not install without Corel Draw being installed. I bought a brand new laptop just for the Laser, it is fresh and does not have Corel Draw on it.  



![images/239dfb4c31f32c408dcf2f5276912f73.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/239dfb4c31f32c408dcf2f5276912f73.jpeg)
![images/8849c40b4b1bcba543df313429ef1c99.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8849c40b4b1bcba543df313429ef1c99.jpeg)
![images/e8576bfc48edc1e03dc865b0d88b7985.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8576bfc48edc1e03dc865b0d88b7985.jpeg)

**"Rix Metal Worx"**

---
---
**Robert Selvey** *September 03, 2016 01:24*

This is the picture of my cd that came with it the file your looking for should be the Coreldraw.rar let me know if you need help.

[https://plus.google.com/photos/...](/photos/109246920739575756003/albums/6325910755465859969/6325910752997515682)


---
**Rix Metal Worx** *September 03, 2016 01:30*

Yeah, that is totally different than mine. See the pics in my post. When I open disk it opens to software folder.Disk title in drive is even titled software. I cannot find any RAR file.

It seems like I am missing the root directory.


---
**Robert Selvey** *September 03, 2016 01:42*

Yes my coreldraw is in the root dir as a .rar file


---
**Rix Metal Worx** *September 03, 2016 01:51*

And do you see the pic of my E drive, it shows the disk in the drive and it says Software. My laser and disk and laptop are at the shop, tomorrow I will give it another good look thru, but I really think I got a incomplete disk.


---
**Robert Selvey** *September 03, 2016 01:57*

Most of the ebay sellers of the K40 are in china and don't respond till late night.


---
**Rix Metal Worx** *September 03, 2016 01:59*

This was a US seller. I am going to bring the disk home tomorrow from shop and double check it in my PC just to cover all my bases.


---
**Robert Selvey** *September 03, 2016 02:00*

Cool just let me know if I can help with anything.


---
**Rix Metal Worx** *September 03, 2016 02:04*

Thank you, I greatly appreciate it !


---
**HalfNormal** *September 03, 2016 02:37*

**+Rix Metal Worx** The version of CorelDraw included with most of the lasers is a pirated version of CorelDraw 12. Looks like you did not get it on your CD. Corel laser will work with all versions of CorelDraw, so if you purchase a legitimate copy other than the student versions, you will be good to go. You really do not need CorelDraw but having it makes it easier to import graphics and send them to the K40. LaserDRW and WinSeal will work but you will have to play with the software since no manuals exist in english to use them correctly or easily.


---
**Rix Metal Worx** *September 03, 2016 11:43*

Here is the Ebay listing description in the title 40W CO2 LASER ENGRAVING MACHINE CUTTING ENGRAVER LaserDRAW CorelDRAW. 



 Here is the reply I received



Thanks for contacting us.

Sorry but the software we provided for this machine is the LaserDRW software as you see in the disk.

This machine is compatible with corel draw but we don't offer corel draw software, if you would like to use corel draw, you need to install the software on your computer first.

Sincerely hope your understanding.



So it looks like I will be purchasing a copy.


---
**Bruce Golling** *September 03, 2016 14:06*

I had that some issue the other day but discovered that Corel Draw is there but it has to un-rar'd before it will install.  Not sure if your version of win has unrar program, but if not there are plenty of free progs avail.


---
**Rix Metal Worx** *September 03, 2016 16:02*

I downloaded RARzilla. It shows no .rar files on disk.


---
**Bruce Golling** *September 06, 2016 15:01*

It is there in the picture you published.  I copied the corel rar file to my computer and then was able to un rar it.


---
**Rix Metal Worx** *September 06, 2016 23:22*

**+Bruce Golling** In the pictures I posted ? I don't see it. I got a copy of the full disk and my disk was just one folder from the full disk. Seller told me Corel Draw was not in the package deal, he just puts Corel draw in the listing title to let you know its compatible .


---
**Jason Cooper** *September 09, 2016 03:28*

I just bought one too.  No corel draw included.  US shipper as well.  Rix, I think we are both SOL.  The seller told me it was not included after I told them I couldn't find it.  Same price as the ones with it included from other sellers.  Warning to others out there to maybe ask before clicking buy it now...






---
*Imported from [Google+](https://plus.google.com/115916575345683901299/posts/FhGo7aQ5PaR) &mdash; content and formatting may not be reliable*
