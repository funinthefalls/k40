---
layout: post
title: "Anyone out there still us CorelLaser with Corel Draw?"
date: June 12, 2017 21:53
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Anyone out there still us CorelLaser with Corel Draw?



If so, I'd appreciate some help. My CorelLaser shuts down everytime I send a job over to the machine via CorelDraw. The job still sends and CD stays open...but CL closes which means I have to shut down CD to open CL again.



Has this happened to anyone before? Any ideas what the problem could be?



Connected on a laptop via Windows 10 if that helps.



thanks,





**"Nathan Thomas"**

---
---
**Ned Hill** *June 12, 2017 22:14*

Look in the windows tool tray at the bottom. You should see a pink icon.  Right click it. 


---
**Nathan Thomas** *June 12, 2017 22:44*

**+Ned Hill** my win 10 computer isn't even finding it when I connect. How did you get yours to?



My older laptop finds and installs the driver with no problem, but not the new one.


---
**Nathan Thomas** *June 13, 2017 14:08*

Found this article that may explain...

[k40laser.se - Windows 10 and CorelLaser - K40 Laser](https://k40laser.se/software-for-k40/windows-10-corellaser/)


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/QxMSxxZoG6X) &mdash; content and formatting may not be reliable*
