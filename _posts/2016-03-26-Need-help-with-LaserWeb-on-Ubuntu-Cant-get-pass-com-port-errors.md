---
layout: post
title: "Need help with LaserWeb on Ubuntu. Can't get pass com port errors"
date: March 26, 2016 15:23
category: "Discussion"
author: "Kelly Burns"
---
Need help with LaserWeb on Ubuntu.  Can't get pass com port errors.    





**"Kelly Burns"**

---
---
**Anthony Bolgar** *March 26, 2016 15:36*

Did you install it like this:



[https://github.com/openhardwarecoza/LaserWeb](https://github.com/openhardwarecoza/LaserWeb)

Scroll down the readme until you get to the ubuntu install instructions


---
**Anthony Bolgar** *March 26, 2016 15:38*

Also, what type of controller card is you machine running?


---
**Kelly Burns** *March 26, 2016 23:58*

I used that exact guide.  While not a Linux person, I'm pretty confident in dealing with Arduino COM port issues on Windows.   I have it installed and running.  I can load the interface in the web browser and it seems to at least see that the port active and let's me select it, but as soon as I go to control, I get a communications error.  As for the Controller Card, I'm running an Arduino Mega running Marlin.  The Controller setup seems to be working.  I can load a Gcode file from SD card and it seems to process it. 


---
**Kelly Burns** *March 27, 2016 00:00*

When I get home, I'll plug it back in and get the exact error.  I thought I had it saved on my phone, but can't find it.    Thanks for your assistance


---
**Kelly Burns** *March 27, 2016 16:25*

I went to get the exact error message to post here.  When i did that I realized that I had not tried to run the Server as the "root" user.  When I switched the Terminal to root and started the server, it seems to be working.  I guess only the user I was running under does not have permission to access the USB/COM ports.  I currently have the Ramps controller spinning some spare Stepper Motors and it appears to be turning them correctly and I can see Position indicators on the LCD screen changing.  



Thanks for the responses


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/RcUMzPWw2uR) &mdash; content and formatting may not be reliable*
