---
layout: post
title: "Looking for a 3040 (300mm x 400mm) 40W cutter"
date: February 21, 2016 22:08
category: "Material suppliers"
author: "Stuart Rubin"
---
Looking for a 3040 (300mm x 400mm) 40W cutter. For some reason there is a HUGE jump in cost from 3020 (300x200) to 3040. Does anyone know where I can find a low cost 3040? Thanks.





**"Stuart Rubin"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 21, 2016 23:55*

Alternatively you could modify a 3020 to have a larger bed. Possibly be cheaper than the difference in price to the 3040.


---
**Stuart Rubin** *February 22, 2016 01:29*

Yuusuf, that sounds like a reasonable idea. (Of course, I'm looking to buy since I made my own low-power engraver; it worked out, but it had some flaws, then the laser died, and bla bla bla...) Do you know of any examples of modifications I could see? Other than really rebuilding a new bed/frame, I'm not sure how I could make the modifications. Thanks for the idea!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 22, 2016 06:09*

**+Stuart Rubin** Sorry, I haven't seen any examples of it, however I was considering modifying my k40 to have a moving cutting bed (so the laser head will move on the X-axis but the cutting bed will move along the Y-axis). I imagine you would have to remove the Y-axis stepper motor & use it to move the cutting bed (via pulley or cog/teeth).



Unfortunately I don't really have the technical know-how or $ to do it until I have a decent amount of time when I don't need to use the machine.



There are some sketches of my ideas in relation to the "conveyor" here



[https://drive.google.com/file/d/0Bzi2h1k_udXwQzZsOTh2Z0o1Yk0/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwQzZsOTh2Z0o1Yk0/view?usp=sharing)



[https://drive.google.com/file/d/1dcKCjbSNNqj2qHv4KUxMJNj8TiN9XUk_vQ/view?usp=sharing](https://drive.google.com/file/d/1dcKCjbSNNqj2qHv4KUxMJNj8TiN9XUk_vQ/view?usp=sharing)


---
**Stuart Rubin** *February 23, 2016 00:06*

**+Yuusuf Sallahuddin** Looks like a cool idea (if you can pull it off!) Let me know.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 23, 2016 00:54*

**+Stuart Rubin** If/When I get around to it I'll post something in the group here to show it.


---
*Imported from [Google+](https://plus.google.com/104069580748008438325/posts/Tr2DuKZDRCP) &mdash; content and formatting may not be reliable*
