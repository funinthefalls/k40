---
layout: post
title: "Birthday present laser cut on my K40!"
date: October 31, 2016 04:04
category: "Discussion"
author: "Timo Birnschein"
---
Birthday present laser cut on my K40! I'm really pleased with this. My girlfriend had the idea, we designed it and build it together :)

![images/698458d4c47497134366771d2d0ded98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/698458d4c47497134366771d2d0ded98.jpeg)



**"Timo Birnschein"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 31, 2016 06:11*

Looks great. Is it actually a puzzle too? That would be super cool.


---
**Timo Birnschein** *October 31, 2016 14:06*

**+Yuusuf Sallahuddin** thank you! Yes, it's a puzzle! The outlines are glued to a plate, all the pieces are pained with acrylic and the kid can put it together. And you can transport it and show it to your friends without it falling apart 😁


---
*Imported from [Google+](https://plus.google.com/+TimoBirnschein/posts/443briASTVo) &mdash; content and formatting may not be reliable*
