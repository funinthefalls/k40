---
layout: post
title: "I forgot who, but I thought someone here had replaced the rails in their D/K40 for something better, perhaps even making the cutting area bigger?"
date: November 04, 2016 00:05
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
I forgot who, but I thought someone here had replaced the rails in their D/K40 for something better, perhaps even making the cutting area bigger? If this is you, please raise your hand... 





**"Ashley M. Kirchner [Norym]"**

---
---
**Ariel Yahni (UniKpty)** *November 04, 2016 00:16*

[https://plus.google.com/+ArielYahni/posts/TXDKPJXuQYB](https://plus.google.com/+ArielYahni/posts/TXDKPJXuQYB)


---
**Ariel Yahni (UniKpty)** *November 04, 2016 00:17*

[https://plus.google.com/115567306264374614046/posts/Ug8KtzETWna](https://plus.google.com/115567306264374614046/posts/Ug8KtzETWna)


---
**Ashley M. Kirchner [Norym]** *November 04, 2016 00:46*

Aha, so it was you. I remembered a large bed engraving something with a "40" on it ... awesome. Time to do research. Thanks!


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/BrVvunUKVMW) &mdash; content and formatting may not be reliable*
