---
layout: post
title: "Hi All Blackpool Makerspace are having an open day, sponsored by the Craft Council, Make:Shift:Do on Saturday 24 October 2015"
date: October 18, 2015 19:10
category: "Discussion"
author: "Mike Hull"
---
Hi All

Blackpool Makerspace are having an open day, sponsored by the Craft Council, Make:Shift:Do on Saturday 24 October 2015.

All Welcome 10am to 5pm at Crossways Hotel, Tyldesley Road, Blackpool FY1 5DF UK.

We have a new K40 which hasn't even been unboxed yet. All visitors and potential new club members of all ages welcome.

Mike





**"Mike Hull"**

---


---
*Imported from [Google+](https://plus.google.com/105561364875219867696/posts/Gv9xx7ob5iw) &mdash; content and formatting may not be reliable*
