---
layout: post
title: "Shared on May 30, 2016 12:27...\n"
date: May 30, 2016 12:27
category: "External links&#x3a; Blog, forum, etc"
author: "Franco \u201cnextime\u201d Lanza"
---
[https://www.nexlab.net/2016/05/30/stainless-steel-and-other-conductive-metals-electrical-marking/](https://www.nexlab.net/2016/05/30/stainless-steel-and-other-conductive-metals-electrical-marking/)





**"Franco \u201cnextime\u201d Lanza"**

---
---
**Stephane Buisson** *May 30, 2016 12:33*

Thank you **+Franco Lanza** , interesting, keep that under my elbow. (relocated into external link)


---
**Franco “nextime” Lanza** *May 30, 2016 12:46*

Ops, sorry, it's wasn't supposed to be posted in this group, anyway, i will not remove it as it can be an alternative option to laser marking 




---
**Alex Krause** *May 30, 2016 18:26*

Reminds me of arc welding


---
**Franco “nextime” Lanza** *May 30, 2016 19:35*

**+Alex Krause** well, both the processes are using arcs, but in arc welding it's in air and uses the eat of the arcs to weld metal, in EDM a moltitude of micro-arcs/sparks in a dielectric liquid "erode" the metal, and eat is only a secondary effect and not so high or important, so, same base concept, different application :)


---
*Imported from [Google+](https://plus.google.com/+FrancoLanzanextime/posts/48UK655o8Zs) &mdash; content and formatting may not be reliable*
