---
layout: post
title: "Don Kleinschnitz here are some \"drool factor\" pics for you ;) The stock machine has a work area of 500mm X 400mm and comes with a flow sensor, electric Z bed with honeycomb (about 10\" of adjustment) , dot pointer, air assist"
date: September 12, 2017 23:50
category: "Discussion"
author: "Anthony Bolgar"
---
**+Don Kleinschnitz** here are some "drool factor" pics for you ;) The stock machine has a work area of 500mm X 400mm and comes with a flow sensor, electric Z bed with honeycomb (about 10" of adjustment) , dot pointer, air assist nozzle, built in 6" exhaust fan, electrical outlets for air pump (included), water cooling pump (included) and the fan (built in), it also has lighting in the work area. Has an e-stop button, but no lid interlock. The lid is on a gas shock which I really like (no more crashing lids when they accidentally fall). Base of the unit is on 4 casters (2 locking at the front, 2 free wheeling at the back). Not bad for $1600 USD with a 1 year local service warranty (on site service if within 60 miles of the suppliers place of business, which I just make)  Let me know if you want any other info



![images/6b7e29cd188e8b868af8e73d526ab2a5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b7e29cd188e8b868af8e73d526ab2a5.jpeg)
![images/75f5515d4ffa8efb04d9bbe7e51f5f3b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/75f5515d4ffa8efb04d9bbe7e51f5f3b.jpeg)
![images/e962ea1fe89747b7fdf5f155707044ee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e962ea1fe89747b7fdf5f155707044ee.jpeg)
![images/b1eb43fc821be92f9b3bab4dbbc74251.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b1eb43fc821be92f9b3bab4dbbc74251.jpeg)
![images/1fc2a2bc1714d26e4c1c9b92313861b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1fc2a2bc1714d26e4c1c9b92313861b1.jpeg)
![images/7d597c87b7abe5fe92abaaf6533cdde1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d597c87b7abe5fe92abaaf6533cdde1.jpeg)
![images/ddacc0ef28524686241af4ea8cd2b71c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ddacc0ef28524686241af4ea8cd2b71c.jpeg)

**"Anthony Bolgar"**

---
---
**greg greene** *September 13, 2017 00:03*

Is this the K60?


---
**Anthony Bolgar** *September 13, 2017 00:47*

No, it is a Chinese laser though, 50 W with a 500X400mm bed


---
**Don Kleinschnitz Jr.** *September 13, 2017 03:22*

Do you know the vendor so that I can look it up on Amazon or the like?


---
**Anthony Bolgar** *September 13, 2017 03:40*

Local Toronto Ontario vendor, ASC365 



Web site is [asc365.com - ASC365.com Screenprintng padprinting sublimation officesupply buttonmaker package stentil cuttingplotter](http://ASC365.com) they have a California based warehouse as well I think.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/PNR95mRkRKn) &mdash; content and formatting may not be reliable*
