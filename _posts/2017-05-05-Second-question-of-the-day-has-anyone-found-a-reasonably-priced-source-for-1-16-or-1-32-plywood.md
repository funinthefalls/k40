---
layout: post
title: "Second question of the day, has anyone found a reasonably priced source for 1/16\" or 1/32\" plywood?"
date: May 05, 2017 00:21
category: "Material suppliers"
author: "Terry Taylor"
---
Second question of the day, has anyone found a reasonably priced source for 1/16" or 1/32" plywood? Every place that I have found seems to think that their wood is gold-plated (3-4 times the cost of 1/8").





**"Terry Taylor"**

---
---
**Steve Clark** *May 05, 2017 01:04*

Not really, for what its worth it is harder to make. the best I've seen was this link but I imagine the shipping is at lease twice the plywood cost.

[https://www.idealtruevalue.com/store/p/186303-1/32-x-12-x-24-Aircraft-Grade-Birch-Craft-Plywood-Birch-Craft-Plywo.html?feed=Froogle&gclid=Cj0KEQjwoqvIBRD6ls6og8qB77YBEiQAcqqHe0HiyYb3D-QEmqln5pOieRQKlp3QUNpkLRJKfVOWYhoaApGw8P8HAQ](https://www.idealtruevalue.com/store/p/186303-1/32-x-12-x-24-Aircraft-Grade-Birch-Craft-Plywood-Birch-Craft-Plywo.html?feed=Froogle&gclid=Cj0KEQjwoqvIBRD6ls6og8qB77YBEiQAcqqHe0HiyYb3D-QEmqln5pOieRQKlp3QUNpkLRJKfVOWYhoaApGw8P8HAQ)


---
**Vince Lee** *May 05, 2017 04:20*

[inventables.com - Inventables:  Plywood](https://www.inventables.com/categories/materials/wood-mdf/plywood)


---
**Kirk Yarina** *May 05, 2017 14:16*

Inventables price is pretty eye-catching, plus they list the thickness tolerance of 1/16" as +/- 0.125".  -1/16 to 3/16; negative thickness could be pretty cool


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/9ooj5VXaW1c) &mdash; content and formatting may not be reliable*
