---
layout: post
title: "Ordered just now, let's hope I can do it justice Ray Kholodovsky expect many support calls lol"
date: November 24, 2016 05:56
category: "Modification"
author: "Andy Shilling"
---
Ordered just now, let's hope I can do it justice **+Ray Kholodovsky**​ expect many support calls lol 





**"Andy Shilling"**

---
---
**Jim Hatch** *November 24, 2016 14:42*

Nice looking option. I've got a setup coming from **+Scott Marshall** to do the same (although his allows you to leave the existing board & connections in so you can swap back to stock quickly in case something goes wrong or you sell the machine). These are a great way for the K40 community to expand - makes upgrading a plug & play operation which is perfect for most hobbyists vs. electrical geeks :-)


---
**Antonio Garcia** *November 24, 2016 19:45*

hahaha just in time for black friday ;),

Ok before i purchase this board, a brief summary why should i buy this one rather than smoothie?? obviously price is a pros,  plug and play is another pros, any cons which i´m losing?? 


---
**Antonio Garcia** *November 24, 2016 19:52*

... lead time 4-6 weeks :( i found the cons hahaha.

My k40 needs a controller as soon as possible, since the software comes with the machine is not working very well in my computer...


---
**Kelly S** *November 24, 2016 19:54*

**+Antonio Garcia** I also ordered this boad, and it will be shipping very soon.  I do not have it in hand, however from everything I know, it runs official smoothie software.  Not only is it a better cost effective board, it is a easy plug and play kit.  There are currently no cons that I can think of.  



At the start I was looking at smoothie and seen it required the board plus a middle man to even get my ribbon cable to connect properly.   So this saved me on the cost of the board from thst start, then buying a middle man, and then trying to get it wired and working.  I spoke with Ray off and on and he has been wonderful in providing answers to any question i have had.  I.e. ordering the proper screen that is an add on.  I highly look forward to its arrival and am now studying up on laserweb so when it is here, I am set to go as soon as it is installed.


---
**Andy Shilling** *November 24, 2016 20:04*

I've ordered it purely for simplicity, I've only had my machine about a month now and I've got the moshi board as the machine was given to me broken. I have no background in electronics other than I like to pretend I can fix things; ( Dangerous man to know) 

I had been looking to upgrade and this price for what you get I think it's fantastic value especially for me because I also have to buy a new psu along side.


---
**Antonio Garcia** *November 24, 2016 20:26*

**+Andy Shilling** **+Kelly S** here we go!!!! i´ve just ordered it, we´re gonna try it..

I hope working fine :),  my PSU hasn´t got that connector for controlling the pwm but i always can cut it  and soldering a new one :)



The worst part is 6 more weeks with this crappy software/plugin :) which disappear every time i make a cut hahaha


---
**Kelly S** *November 24, 2016 20:31*

Yes waiting stinks **+Antonio Garcia** haha.  I am a very impatient person and need stuff yesterday.   I will be documenting my install of this item for future users.  The last mod on my list as I am finishing my adjustable bed and have my air assist and laser pointers in for getting the focal point set easily every time.  Cutting back the exhaust was fun as that required taking everything out but also a good mod to preform as it open up usable cutting room. 


---
**Antonio Garcia** *November 24, 2016 20:41*

I put it lights, new blower and a holder for the laser pointer, and I'm waiting for the more stuff, my postman is on fire 😂😂😂😂![images/7d2443d5aa8ebb248207c9b16b938146.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d2443d5aa8ebb248207c9b16b938146.jpeg)


---
**Antonio Garcia** *November 24, 2016 20:41*

![images/44dd303b1b0fd0ceef6a7ba688781329.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44dd303b1b0fd0ceef6a7ba688781329.jpeg)


---
**Antonio Garcia** *November 24, 2016 20:42*

![images/3ae2dfbe70104508e7ada29ed4236fe6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ae2dfbe70104508e7ada29ed4236fe6.jpeg)


---
**Kelly S** *November 24, 2016 20:45*

Looks groovy I will share some of mine.  :) nice big blower so I don't smell anything, ever haha...

![images/c93e36eef5216ae2b0e5b18160cba34f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c93e36eef5216ae2b0e5b18160cba34f.jpeg)


---
**Kelly S** *November 24, 2016 20:46*

Adapter I made to hook up a 4 inch pipe easily. 

![images/57104cbb194c396a622ba3f52f7fbb6b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57104cbb194c396a622ba3f52f7fbb6b.jpeg)


---
**Antonio Garcia** *November 24, 2016 20:46*

And of course burning every piece of wood and acrylic that I've got at home 😊

![images/3285c27f47cfb5cf92d81ff33f36e326.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3285c27f47cfb5cf92d81ff33f36e326.jpeg)


---
**Kelly S** *November 24, 2016 20:47*

Cable chain for laser pointer power and air assist 

![images/b91062bdf8977226e2b4203722835839.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b91062bdf8977226e2b4203722835839.jpeg)


---
**Kelly S** *November 24, 2016 20:50*


{% include youtubePlayer.html id="eaFhdqUQaiI" %}
[https://youtu.be/eaFhdqUQaiI](https://youtu.be/eaFhdqUQaiI) and my currently diy adjustable bed.  Will be finished tomorrow after fedex arrives with my new sheet for the bed.


---
**Antonio Garcia** *November 24, 2016 20:52*

Cable chain and level bed is my next... sometimes i have more fun upgrading the machine than cutting hahaha


---
**Charlie Thcakeray** *November 24, 2016 21:06*

**+Ray Kholodovsky** just pre-ordered mine!! So excited can wait to run this little beast! 


---
**Kelly S** *November 24, 2016 21:12*

**+Antonio Garcia** that's funny and I agree, I think I've spent more time actually working/changing things around than I have cutting or engraving with it.  But I have been waiting for all the improvements to be done and new software before I start making finished items so there are no worries of getting the same results with a different setup.


---
**Jonathan Davis (Leo Lion)** *November 24, 2016 21:21*

I've had the mini version for a few days now and with a BenBox laser engraving machine. You will need end stops to keep it from thinking about going further when the track it requires is nonexistent so the end stops are a must have. overall the board works perfectly and worth the price i paid for it even though i had to provide the motor drivers and micro SD card but a 1GB card is sufficient.


---
**Kelly S** *November 24, 2016 21:26*

**+Jonathan Davis** could you by chance point me in a direction for end stops I will require and how you hooked them up please?  And I also had to order the stepper drivers and SD card (but I have many SD cards so it is no biggie).   I know Ray mentioned something about end stops but didn't go into detail.  Thank you in advance!


---
**Jonathan Davis (Leo Lion)** *November 24, 2016 21:28*

**+Kelly S** Ray is still devloping the instructions on how to install the end stops, but the cheapest ones i found were part of a ardunio laser engraving kit on ebay.


---
**Kelly S** *November 24, 2016 21:31*

**+Jonathan Davis** did you use something similar to these?

![images/8d6a04efa59804ebee03766ff05f900c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8d6a04efa59804ebee03766ff05f900c.png)


---
**Antonio Garcia** *November 24, 2016 21:33*

**+Kelly S** I also ordered the RJ45 module for connecting to Laserweb, probaby is not a must, but nice to get it.

[ebay.co.uk - Details about  LAN8720 Ethernet Module Network Transceiver Embedded Web Server RMII PHY](http://www.ebay.co.uk/itm/302056710156?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Kelly S** *November 24, 2016 21:41*

I have that module and my display waiting already :D


---
**Antonio Garcia** *November 24, 2016 22:32*

**+Kelly S** I think would be better if you go to this type of switches for mounting in the rails, with the others mounting in a board, it is gonna be more difficult put them in place.



[https://www.amazon.com/Endstop-Microswitches-3D-Printer-CNC/dp/B00G2E6UZC/ref=sr_1_4?ie=UTF8&qid=1480026597&sr=8-4&keywords=endstop+switch](https://www.amazon.com/Endstop-Microswitches-3D-Printer-CNC/dp/B00G2E6UZC/ref=sr_1_4?ie=UTF8&qid=1480026597&sr=8-4&keywords=endstop+switch)

[amazon.com - Amazon.com: Endstop Microswitches for 3D Printer or CNC: Industrial & Scientific](https://www.amazon.com/Endstop-Microswitches-3D-Printer-CNC/dp/B00G2E6UZC/ref=sr_1_4?ie=UTF8&qid=1480026597&sr=8-4&keywords=endstop+switch)


---
**Kelly S** *November 24, 2016 22:35*

**+Antonio Garcia** thanks, but I guess those will not be required for my application.   :)


---
**Jim Hatch** *November 25, 2016 00:50*

**+Antonio Garcia**​ the plugin controls are dropping to the system status/taskbar in the lower right of your screen down by the clock. You'll see a little upward pointing triangle and if you click that you'll see the hidden system objects like your printer etc - and the plugin for Corel. You don't need to exit & reload Corel each time you send something to the laser.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/3vsEdFoDcAJ) &mdash; content and formatting may not be reliable*
