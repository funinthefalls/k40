---
layout: post
title: "Selling my control board. Comes with software CD, USB dongle and the M2 nano control board"
date: July 03, 2017 12:34
category: "Discussion"
author: "Dushyant Ahuja"
---
[https://www.ebay.com/itm/192236516258](https://www.ebay.com/itm/192236516258) 



Selling my control board. Comes with software CD, USB dongle and the M2 nano control board. 









**"Dushyant Ahuja"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 03, 2017 15:24*

I'm sorry to hear it didn't work out for you. 


---
**Dushyant Ahuja** *July 03, 2017 15:35*

**+Ray Kholodovsky** ya right. Considering I used your board to replace this :-). Hehehehe


---
**Steve Clark** *July 03, 2017 15:49*

You think there is a market for them?  I just happen to have one also! Grin... I'm watching your e-bay but I wouldn't put mine up for sale until yours sells as a courtesy.


---
**Dushyant Ahuja** *July 03, 2017 16:13*

I saw on ebay - people are selling them for $99+, not sure if anyone has bought them.


---
**Ray Kholodovsky (Cohesion3D)** *July 03, 2017 16:44*

I might be in the wrong business... 


---
**laurence champagne** *July 03, 2017 17:41*

Is there a way to adjust the steps per mm on thsese boards?


---
**Dushyant Ahuja** *July 03, 2017 19:27*

Not that I know of. 


---
**Dushyant Ahuja** *July 03, 2017 19:27*

**+Steve Clark** thanks 😊


---
**Paul de Groot** *July 04, 2017 00:31*

I think lots of people have these boards laying around. I even offered it for free to someone 


---
**Dushyant Ahuja** *July 04, 2017 23:09*

Sold. :-)


---
**Paul de Groot** *July 05, 2017 02:18*

**+Dushyant Ahuja** great business model. Sell your useless stock controller and replace it with a proper one! Maybe you can end up with a positive cash flow :)




---
**Dushyant Ahuja** *July 05, 2017 02:22*

**+Paul de Groot** hardly.  Took the first offer I got. Sold it for $60


---
**Paul de Groot** *July 05, 2017 03:10*

**+Dushyant Ahuja** Ahh I assumed you sold it for the full listing price


---
**Steve Clark** *July 05, 2017 03:51*

I'm amazed. 


---
**Madyn3D CNC, LLC** *July 12, 2017 18:11*

There's a market for these, not a big one but people do buy them. 


---
**Dushyant Ahuja** *July 12, 2017 23:52*

**+Madyn3D CNC, LLC** to tell you the truth - I wouldn't have bought it for $60 when you can get the **+Cohesion3D** mini for about a $100


---
**Madyn3D CNC, LLC** *July 13, 2017 16:02*

**+Dushyant Ahuja** No, definitely not a wise move lol, the Smoothie upgrade is well worth the extra 40 bucks. 



I think some of the people buying the Lihuiyu control PCB's are hacking them or whatever to drive plotters or animatronics, or maybe just experimenting with them. I don't think the laser folks are buying them, unless they are unaware of Smoothie/Cohesion. 



Or maybe some people are nervous about stepping outside of what they're comfortable with. I can be like that sometimes when it comes to CAD software. 



I see them sell pretty fast at a decent price most of the time. 


---
**laurence champagne** *July 15, 2017 00:03*

**+Madyn3D CNC, LLC** 

I have to disagree here. For someone who solely does engravings I loved this board. It was so quick and easy to just save a regular Illustrator file, open it up in corel laser, hit the engrave button and It's on it's way.

I just recently upgraded to to the coehesion3d because I couldn't figure out how to adjust the steps/mm on the Nano board. Mine was off about 2% for some reason.



With the cohesion I have to use the god awful laserweb app now. It isn't really user friendly and takes forever to set up a job. If you have to  generate slightly complex g-code it takes SEVERAL minutes. I could go on and on... I also miss how as soon as I turned on the nano it would home and then go to where I had it set on the x and y for my custom zero automatically. 






---
**Dushyant Ahuja** *July 15, 2017 03:15*

**+laurence champagne** you don't need to use laserweb. You can use any GCode generator. That's the beauty of open source. 



Also, you can setup a startup GCode for the cohesion3d board so that it homes and goes to your custom zero automatically. It's pretty simple. 



Worst case - I'm sure there are many people on this group who would gladly sell you their boards. 


---
**Dushyant Ahuja** *July 15, 2017 03:34*

on_boot.gcode : - Played on startup of smoothieboard. Can be used to initialize the smoothie driven device.

[http://smoothieware.org/sd-card](http://smoothieware.org/sd-card)


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/MxkswUwvkDs) &mdash; content and formatting may not be reliable*
