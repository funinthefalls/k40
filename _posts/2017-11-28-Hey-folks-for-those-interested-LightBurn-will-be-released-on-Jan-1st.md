---
layout: post
title: "Hey folks, for those interested: LightBurn will be released on Jan 1st"
date: November 28, 2017 08:44
category: "Software"
author: "LightBurn Software"
---
Hey folks, for those interested:



LightBurn will be released on Jan 1st.

We don't actually need that much time, but we'll be traveling over the holidays, which will make it tough logistically to support the product should issues arise. The extra time will be spent on documentation, tutorials, features, and testing, which should help the launch go a little smoother.



We have a website: [http://LightBurnSoftware.com](http://LightBurnSoftware.com)



There’s not much visible at the moment, but you can sign up to be notified when it goes live (we’re hoping for a day or two early). This will be where you download the trial version as well as purchase licenses.



The license system is in place and testing there has been smoother than expected. We have several users in the beta group who are using LightBurn on a daily basis, on both GCode and Ruida hardware. There are still some rough edges and a few bugs, but that improves continually.



Licenses for GCode based machines will be $30, are good forever, can be installed on up to two computers, and allow free software updates for a year.



Thank you for supporting LightBurn and we look forward to lasering with you in the new year!





**"LightBurn Software"**

---
---
**Andy Shilling** *November 28, 2017 09:43*

Great news, looking forward to using it.


---
**Tony Marrocco** *November 29, 2017 16:02*

Awesome,  signed up and look forward to it.


---
**Steve Clark** *December 19, 2017 05:35*

Wow! Just watched the video's I'm really looking forward to it!


---
*Imported from [Google+](https://plus.google.com/110213862985568304559/posts/Y91HpRiMWSf) &mdash; content and formatting may not be reliable*
