---
layout: post
title: "This is great info. It will make you think twice next time you hit fire with the lid up"
date: September 14, 2016 08:03
category: "Discussion"
author: "Scott Marshall"
---
[http://www.uvex-laservision.de/en/laser-knowledge/laser/](http://www.uvex-laservision.de/en/laser-knowledge/laser/)



This is great info. It will make you think twice next time you hit fire with the lid up.

Every section here is gold, well worth reading through them.





**"Scott Marshall"**

---
---
**Savannah Champagne** *September 14, 2016 08:43*

I'm still confused on something. Is the only danger from a direct hit to my eyes or is there some kind of radiation given off from the beam or the tube that can damage my eyes?






---
**HalfNormal** *September 14, 2016 12:50*

It is the "deflection" of the beam off other surfaces. The beam is coherent and does not spread nor does it radiate energy. So if the deflection directly hits the eye, yes it can cause damage. Deflection in this case would be anywhere the beam is directed due to mirrors or other items that can deflect the beam from its intended course.


---
**Thor Johnson** *September 14, 2016 16:09*

Which includes what you are lasering (if metal or shiny)... but usually that's focused for only a little bit.  Luckily (!?) for a K40 CO2 laser, the beam will not focus in your eyes (but a 40W beam will burn your lens, so don't stick your head in there), and is reflected mainly by metals; since it's absorbed by most things, even the plexi window of the machine is great protection.



OTOH, if you're playing around with the 4W laser diodes (BluRay), those <b>will</b> hurt your retina, and UV/Blue is reflected by dang near everything (and 4mW is bad for eyes, so you only need 0.1% of the beam to hit them and your vision is permanently scarred) -- use protection at all times with UV/Visable lasers (and even 1064 YAG lasers -- they're not far enough to be stopped by your lens).



OT3H, you can look at the glowing tube fine (don't look at the ends).


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/GoE6QNhofbW) &mdash; content and formatting may not be reliable*
