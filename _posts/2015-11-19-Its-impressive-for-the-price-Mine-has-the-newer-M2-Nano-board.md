---
layout: post
title: "It's impressive for the price. Mine has the newer M2-Nano board"
date: November 19, 2015 17:14
category: "Discussion"
author: "Gary McKinnon"
---
It's impressive for the price. Mine has the newer M2-Nano board.

My workflow is :



Sketchup --> PDF --> Coreldraw --> Cut



This test shpe, a gear with 90 teeth, 50mm across, came out well. The broken chunk is where it had come full circle and the weight dropped it (must replace that bed!).



Each of the teeth is only 0.3mm acroos at the tip !



![images/b1f6c3958d96feaa835841db4d43abeb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b1f6c3958d96feaa835841db4d43abeb.jpeg)
![images/65af14b95e55398648cd7637e3eb60d3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65af14b95e55398648cd7637e3eb60d3.jpeg)

**"Gary McKinnon"**

---
---
**Kirk Yarina** *November 20, 2015 19:47*

Nice!  What did you design the gear with?



I've experimented with some eccentric gears, mostly 3 and 4 sided, and been pretty happy with the results.  Still need to figure out how to better allow for the cut width so the teeth aren't a bit shorter and narrower than they should be.


---
**Gary McKinnon** *November 20, 2015 20:58*

I used a free gear plugin for sketchup, you want Cadalog gear 3.1 : [http://www.ohyeahcad.com/download/](http://www.ohyeahcad.com/download/)


---
*Imported from [Google+](https://plus.google.com/+GaryMcKinnonUFO/posts/Q7V4zbTfD1y) &mdash; content and formatting may not be reliable*
