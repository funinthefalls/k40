---
layout: post
title: "hi friends. im from Uruguay.I have a problem"
date: January 18, 2016 14:37
category: "Hardware and Laser settings"
author: "Juan Gianni"
---
hi friends. i´m from Uruguay.I have a problem. i need your help. i have a laser engraving kc 40 3020 and my usb key not work. 



someone can provide me the files on the USB key? thanks very very much.





**"Juan Gianni"**

---
---
**Scott Thorne** *January 18, 2016 14:57*

There are no files on the usb key....is hardware...do you have the right key for the software you are using?


---
**Juan Gianni** *January 18, 2016 16:55*

The machine came with a key but is empty


---
**Scott Thorne** *January 18, 2016 16:58*

What software do you have with your machine?


---
**Scott Thorne** *January 18, 2016 16:59*

The key is not a usb flash drive...it's a hardware key.


---
**Juan Gianni** *January 18, 2016 17:02*

i have  software laserdrw and corellaser


---
**Juan Gianni** *January 18, 2016 17:02*

He came with the CD along with the machine


---
**Juan Gianni** *January 18, 2016 17:04*

and usb lihuiyu studio labs" with key


---
**Scott Thorne** *January 18, 2016 18:21*

That key should work with that software...does the green light come on when you plug the key into the usb socket?


---
**Juan Gianni** *January 18, 2016 18:25*

yes, but the light flashes


---
**Scott Thorne** *January 18, 2016 18:29*

Have you tried a different usb socket?


---
**Juan Gianni** *January 18, 2016 18:34*

yes, and others computers but not work. 



All devices in computers that do not appear as a device


---
**Scott Thorne** *January 18, 2016 18:37*

Then the usb key is bad, contact the company that you purchased the machine from and tell them the you need a new usb dongle, make sure that you tell them that you are using laserdraw and Corel laser, you don't want one for moshidraw....it won't work with your software.


---
**Juan Gianni** *January 18, 2016 18:41*

ok thanks Scott. 



buy it in aliexpress but the seller does not answer my request . keep looking if someone has it . Thank you very much again friend 


---
**Joseph Midjette Sr** *January 18, 2016 19:02*

If you look in the folders on the cd you will find a file called usbkey.exe This should be the drivers application. You might have to turn off your Antivirus because it will detect the application as a virus. Re-enable your antivirus when you are done. Hope it helps


---
**Scott Thorne** *January 18, 2016 19:31*

You're welcome...hope you get it up and running soon.


---
*Imported from [Google+](https://plus.google.com/101277421801047663631/posts/4oxyEeW8rnn) &mdash; content and formatting may not be reliable*
