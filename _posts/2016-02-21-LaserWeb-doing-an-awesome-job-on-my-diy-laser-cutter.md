---
layout: post
title: "LaserWeb doing an awesome job on my diy laser cutter"
date: February 21, 2016 20:31
category: "Object produced with laser"
author: "Tiaan Steyn"
---
LaserWeb doing an awesome job on my diy laser cutter.



<b>Originally shared by Tiaan Steyn</b>



Earring sized Peter Pan and Tinkerbell

![images/0a2e21afc89bee334f032e89db206993.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a2e21afc89bee334f032e89db206993.jpeg)



**"Tiaan Steyn"**

---
---
**Joe Keneally** *February 23, 2016 14:53*

These look great! What material are you using?


---
**Tiaan Steyn** *February 24, 2016 21:05*

using 3mm mdf board


---
*Imported from [Google+](https://plus.google.com/106465140912283318412/posts/dUycLvcwn6T) &mdash; content and formatting may not be reliable*
