---
layout: post
title: "I have heard that engraving food is safe, so I decided to try my luck on some sugar cookies I made for my little guy's birthday"
date: May 23, 2016 14:23
category: "Object produced with laser"
author: "Ulf Stahmer"
---
I have heard that engraving food is safe, so I decided to try my luck on some sugar cookies I made for my little guy's birthday.  Since his favourite team just won the DFB-Pokal, I had to include some of FC-BM cookies as well!



This was my first bigger project on my K40 and my first chance to test out my hacked stock blower (photos of the hack were in a previous post - April 15, 2016).  I don't have an air assist (yet) on my machine and I was really impressed how well the blower hack actually works!



A special shout out to **+Yuusuf Sallahuddin** who's recent comments led me to increase my engrave speed and shorten my engraving time!



![images/df8ff2c7a68efcaba5131db4d473c75d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/df8ff2c7a68efcaba5131db4d473c75d.jpeg)
![images/b6a81b9172ec19f76e5f0fc9ee07900f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6a81b9172ec19f76e5f0fc9ee07900f.jpeg)
![images/5200adcec56ac1b7b2a120932deeab6d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5200adcec56ac1b7b2a120932deeab6d.jpeg)
![images/d21b2244f718effc612200e3e7ebc3d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d21b2244f718effc612200e3e7ebc3d7.jpeg)

**"Ulf Stahmer"**

---
---
**Jim Hatch** *May 23, 2016 15:17*

Nice! 



BTW, you can do chocolate too - just make sure it's low power and high speed so it doesn't overmelt. :-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 15:44*

That is wickedly cool. I bet the little guy loved them. On a side note, did they taste any different than usual? I'd imagine a slight burnt taste?



**+Jim Hatch** You could cut shapes out of thin chocolate maybe for cake art? e.g. flower petals.


---
**Ulf Stahmer** *May 23, 2016 15:47*

**+Yuusuf Sallahuddin** yes, they do have a slight burnt taste.  Only in the larger dark areas, though.  You really can't taste a difference in the lettering.  I'm hoping with your suggestion of a faster speed, the engraving is not as deep, hence less burnt flavour.  My birthday boy hasn't seen them yet.  They're for later today.  I'm pretty sure he'll like them, though.



**+Jim Hatch** I've read about that, too.  Got to give that a try!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 16:04*

**+Ulf Stahmer** What power settings are you using? Try minimise the power down to 4mA (or lower if you can get it to mark, I can't mark below 4mA). Also you could consider increasing the Pixel Step to 3 or 5 which will minimise the overall burn but decrease the clarity ever-so-slightly.



edit: earlier today I was considering Laser Cutting some steak into the shape of Australia. I imagine it might take a bit to cut through a steak though. Thin bbq steaks might be okay though.


---
**Ariel Yahni (UniKpty)** *May 23, 2016 16:12*

Be aware of disease on overturn food. Don't recall the name and doesn't apply to all food


---
**Ulf Stahmer** *May 23, 2016 16:12*

**+Yuusuf Sallahuddin** Excellent suggestions.  My machine only has a % power indicator (I've ordered an ammeter, an am waiting for it to arrive).  I tried 150 mm/s at 15% and I didn't get anything, so I upped the power to 25% and 250 mm/s.  Increasing the pixel step is a great idea.  I'm still very new to this.  I understand the theory of the setting differences, but haven't had the opportunity to try different settings out.



Steak sound's interesting.  You could likely engrave toast really well.  There, the burnt flavour might not make a difference at all.



I am really impressed with the machine's ability to engrave marble, glass and stone!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 17:15*

**+Ariel Yahni** What exactly is overturn food? Never heard of the concept, however thanks for pointing it out, as it is probably a good idea for us to all be wary of food related lasering & disease/bacteria growth.



**+Ulf Stahmer** I have the potentiometer & ammeter setup on my machine. I would like some kind of digital meter (similar to what I believe **+Don Kleinschnitz** has if I recall correct). I'll get around to that once I reach that point on my mental-list of mods to make. Seems like a nice simple mod to keep track of the power % (or value).



I have yet to try marble, glass & stone. I did try some mosaic tiles & I was pretty impressed with the result, even just a basic 1 past test. I think


---
**Ariel Yahni (UniKpty)** *May 23, 2016 17:17*

Mean to say overburn 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 17:20*

**+Ariel Yahni** Oh, over burned. Yeah, that is definitely something to be concerned of as some can be carcinogenic if I recall correct.


---
**Ariel Yahni (UniKpty)** *May 23, 2016 17:22*

[http://www.cancer.gov/about-cancer/causes-prevention/risk/diet/acrylamide-fact-sheet](http://www.cancer.gov/about-cancer/causes-prevention/risk/diet/acrylamide-fact-sheet)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 17:37*

**+Ariel Yahni** I took a look around for more info on this topic & it seems that high-starch content foods tend to pose more of a risk of the acrylamide formation. [http://www.foodscience.csiro.au/fshbull/fshbull29s.htm](http://www.foodscience.csiro.au/fshbull/fshbull29s.htm)



Again, thanks for sharing with us before we go giving ourselves cancer inadvertently :)


---
**Ariel Yahni (UniKpty)** *May 23, 2016 17:40*

Don't think that a couple of cookies would cause any harm but long term it could. I did this before and was brought to my attention also. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 17:42*

**+Ariel Yahni** My idea of doing it with steak might be something to avoid too. More research is needed I think.


---
**Ulf Stahmer** *May 23, 2016 17:54*

Thanks, **+Ariel Yahni**.  I'll keep that in mind.  All the more reason to implement a bigger pixel step, faster engraving and reduced power!


---
**Ariel Yahni (UniKpty)** *May 23, 2016 18:07*

**+Yuusuf Sallahuddin**​ send me a piece of that stake I'll eat it anyway :) 


---
**Don Kleinschnitz Jr.** *May 23, 2016 21:54*

This appears to be no more of a risk than eating french fries??


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 22:31*

**+Don Kleinschnitz** From the articles I read, seems to be the case.


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/eyKnQdiyP9E) &mdash; content and formatting may not be reliable*
