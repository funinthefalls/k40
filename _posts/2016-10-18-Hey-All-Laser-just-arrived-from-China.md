---
layout: post
title: "Hey All, Laser just arrived from China"
date: October 18, 2016 01:41
category: "Hardware and Laser settings"
author: "Kris Sturgess"
---
Hey All,



Laser just arrived from China.



Just finished uncaring it.



I have to dig out my volt meter, but will these sockets provide 110v?



Never seen this "type" before.



Everything seems good on the unit. Maybe a few tweaks here and there.



Kris

![images/a66c81d5b2391f7f0ffbf402463dac2c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a66c81d5b2391f7f0ffbf402463dac2c.jpeg)



**"Kris Sturgess"**

---
---
**Paul de Groot** *October 18, 2016 02:11*

They seem to be multi purpose sockets that allow different international plugs to be used. If 110 goes in then i expect that it is routed to these sockets thru relays. Never seen this before. Maybe an new updated k40 version?


---
**Don Kleinschnitz Jr.** *October 18, 2016 02:23*

They should but pull out a meter or a outlet tester and check them.

Socket tester is the best cause you can test for gnd and backward wiring.

[https://www.amazon.com/Power-Gear-50542-3-Wire-Receptacle/dp/B002LZTKIA/ref=sr_1_6?ie=UTF8&qid=1476757286&sr=8-6&keywords=socket%2Btester&th=1](https://www.amazon.com/Power-Gear-50542-3-Wire-Receptacle/dp/B002LZTKIA/ref=sr_1_6?ie=UTF8&qid=1476757286&sr=8-6&keywords=socket%2Btester&th=1)

[amazon.com - Amazon.com: Power Gear 50542 3-Wire Receptacle Tester: Home Improvement](https://www.amazon.com/Power-Gear-50542-3-Wire-Receptacle/dp/B002LZTKIA/ref=sr_1_6?ie=UTF8&qid=1476757286&sr=8-6&keywords=socket%2Btester&th=1)


---
**Ian C** *October 18, 2016 08:09*

Hey buddy. Take a look inside the electronics area at the back where these are fitted. Trace their wires back from the main AC inlet fitting to these sockets, as I am sure they will be like mine and daisy chained from it. Mine do not have the earth (ground) wire linked so will be no good for components requiring a ground. However the exhaust fan doesn't have an earth so I have mine plugged in here. If they are daisy chained together, whatever voltage you will connect to will be available at these sockets. I wouldn't suggest connecting high load items to them though,  but your fan and water pump are low amp draw items


---
**Kelly S** *November 17, 2016 03:24*

That is how mine came as well, after looking at it a bit I realized it was a universal plug (pretty cool)  And it will be whatever the voltage of the machine was set up to.  And it will only put out what you put in.




---
**Kris Sturgess** *November 17, 2016 03:43*

Also note they are not grounded.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/1fR6E4V5Siv) &mdash; content and formatting may not be reliable*
