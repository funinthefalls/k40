---
layout: post
title: "I keep coming back to the idea of using a focusable lens instead of raisable bed, does anyone know definitively if these things will work, I've seen them around ebay and the like"
date: May 17, 2015 21:56
category: "External links&#x3a; Blog, forum, etc"
author: "Troy Baverstock"
---
I keep coming back to the idea of using a focusable lens instead of raisable bed, does anyone know definitively if these things will work, I've seen them around ebay and the like. [http://www.banggood.com/Aluminum-Alloy-Laser-Head-Laser-Engraving-Machine-Part-p-959118.html](http://www.banggood.com/Aluminum-Alloy-Laser-Head-Laser-Engraving-Machine-Part-p-959118.html)





**"Troy Baverstock"**

---
---
**Imko Beckhoven van** *May 18, 2015 06:40*

It works, but only for cutting. To much mass for engraving at any normal engraving speeds. 


---
**Troy Baverstock** *May 18, 2015 08:37*

I've wondered that about just adding the air assist hose to the normal nozzle. 



What speed should these machines be able to do comfortably? At the moment I get a chatter above even 50, haven't looked into that yet.


---
**Imko Beckhoven van** *May 18, 2015 09:47*

adding a air assist hose is a "must" so you have to take  the extra weight with that. 50 mm sounds slow but its been a while so dont' know the last setting i used. (ive upgraded to lightobject DSP dont know if its fair to compare ;-) )


---
*Imported from [Google+](https://plus.google.com/+TroyBaverstock/posts/FTSUZkjG1Br) &mdash; content and formatting may not be reliable*
