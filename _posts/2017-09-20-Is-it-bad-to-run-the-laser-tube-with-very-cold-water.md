---
layout: post
title: "Is it bad to run the laser tube with very cold water?"
date: September 20, 2017 22:16
category: "Hardware and Laser settings"
author: "Lars Andersson"
---
Is it bad to run the laser tube with very cold water? Say 0 to 5 degrees centigrade.





**"Lars Andersson"**

---
---
**Jason Dorie** *September 20, 2017 22:21*

If it's too cold you'll get condensation on the tube, which can in turn conduct the power away from the plasma (and could short the power supply).  If you push ice-cold water into a warm tube you can crack the glass from the thermal stress.



I know those are common issues, but I'm not certain if the low temperature itself is a problem.


---
**Anthony Bolgar** *September 20, 2017 22:58*

0 is a little too cold ie water freezes at 0




---
**Steve Clark** *September 21, 2017 18:14*

If you want to run you laser tube cold you need to know the humidity and temperature that moisture will form in  the ambient temperature (Room and tubes surface temp). 



This chart below will help you along with buying one of these cheap meters. You want to stay, at the least, 3 to 5 degrees ABOVE the dew point and monitor any changes so you can adjust coolant temp if need be.

Jason pretty much covered the why.



[http://www.ebay.com/itm/Sungwoo-Indoor-Digital-Thermometer-Humidity-Meter-Hygrometer-and-Temperature-/122361041652?epid=2007229444&hash=item1c7d4952f4:g:m0oAAOSwqYhZveI4](http://www.ebay.com/itm/Sungwoo-Indoor-Digital-Thermometer-Humidity-Meter-Hygrometer-and-Temperature-/122361041652?epid=2007229444&hash=item1c7d4952f4:g:m0oAAOSwqYhZveI4)



![images/c0b8f0d0e3fe12fa5e0426b5c6b2f442.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c0b8f0d0e3fe12fa5e0426b5c6b2f442.jpeg)


---
**HP Persson** *September 21, 2017 18:35*

Another issue is air pockets, if you running very cold water around 0c, and you get a pocket the tube is more likely to crack, than if the water is 10-15c.

If you want to use very cold water, make sure all surface tension is killed first, so bubbles and pockets cannot form inside your loop.


---
**Lars Andersson** *October 04, 2017 10:28*

The reason for asking is that the laser is  in an unheated workshop and winter is coming. I keep the premises frost free but not more.

The place is quite dry, so no risk of condensation. 



Just wondering if there is any need to <b>heat</b> the water?


---
*Imported from [Google+](https://plus.google.com/117177573126096232373/posts/JaQtqndaPG3) &mdash; content and formatting may not be reliable*
