---
layout: post
title: "Now everything is nice and in place"
date: June 03, 2016 19:32
category: "Discussion"
author: "Derek Schuetz"
---
Now everything is nice and in place

![images/2ff890c59155a9ab0192169a1c3a8dc7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2ff890c59155a9ab0192169a1c3a8dc7.jpeg)



**"Derek Schuetz"**

---
---
**John-Paul Hopman** *June 03, 2016 20:27*

No labels? Looks good though, nice job.


---
**Derek Schuetz** *June 03, 2016 20:29*

When I upgrade to smoothie I'll do a nicer one with labels just need something quick and clean


---
**Purple Orange** *June 04, 2016 04:55*

How does that display connect to the original (?) board to get the right values and readings? 


---
**Derek Schuetz** *June 04, 2016 05:23*

It doesn't connect to the original board it's been changed out for ramps 1.4


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/86ucK6ZvLUD) &mdash; content and formatting may not be reliable*
