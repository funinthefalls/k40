---
layout: post
title: "Cut this today to have at our table this weekend at the Kansas City Maker Faire!!!"
date: June 23, 2016 21:40
category: "Object produced with laser"
author: "Joe Keneally"
---
Cut this today to have at our table this weekend at the Kansas City Maker Faire!!! We have 40 chickens so.......

![images/2f06f9208b43860d80791bb3bc047c0f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2f06f9208b43860d80791bb3bc047c0f.png)



**"Joe Keneally"**

---
---
**Stephen Sedgwick** *June 23, 2016 21:56*

You said Chickens and KC --- I too have both Chicken and am in KC but wasn't going to the maker fair this weekend would like to touch base before hand as I might want to go - as I want to talk with like minded people in the area...


---
**Alex Krause** *June 23, 2016 22:19*

I'm in central Kansas :)


---
**Alex Krause** *June 23, 2016 22:22*

What kind of wood is this?


---
**Stephen Sedgwick** *June 23, 2016 22:30*

what mods do you have done to your laser?  have you converted it to smoothie or arduino?


---
**Joe Keneally** *June 23, 2016 22:47*

**+Alex Krause** Buckner MO


---
**Joe Keneally** *June 23, 2016 22:48*

**+Alex Krause** 1/8 hardwood plywood 5$ for 4'/8' sheet at Hone Depot


---
**Joe Keneally** *June 23, 2016 22:48*

**+Stephen Sedgwick** still running original hardware and laserdrw




---
**Joe Keneally** *June 23, 2016 22:49*

**+Stephen Sedgwick** cool. I am in Buckner MO


---
**Stephen Sedgwick** *June 23, 2016 23:13*

Alex you done any mods on your machine as of yet?  or any plans?


---
**Alex Krause** *June 24, 2016 00:02*

**+Stephen Sedgwick**​ I have a smoothie and a smoothie Derivative board and all the stuff needed for the conversion just haven't had the time to tear apart the machine to do so


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 00:44*

I keep seeing mention of Maker Faires here & other places. What exactly is a maker faire? I am unsure if we have anything like it here in Australia & am jealous of all you who get to go to such things.


---
**Alex Krause** *June 24, 2016 00:45*

[https://en.m.wikipedia.org/wiki/Maker_Faire](https://en.m.wikipedia.org/wiki/Maker_Faire)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 00:48*

**+Alex Krause** Cheers, will take a look.



edit: I swear I was born in the wrong country! haha.


---
**Jim Hatch** *June 24, 2016 01:08*

**+Yuusuf Sallahuddin**​ MakerFaires are like Disney World for geeks. Absolutely incredible the scope and variety of things people are creating. I don't think there's anywhere else you'd see 3D printed prosthetic hands next to a group building small experimental packages to send into space next to small robotics next to people teaching lock picking next to the R2D2 (fully functional, full size) Star Wars robot builders next to animatronic life size horses, next to....🙂


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 01:37*

**+Jim Hatch** I am soooo jealous. Closest one to me is about 3000km away & a few months. Better start walking now haha.


---
**Alex Krause** *June 24, 2016 05:07*

**+Yuusuf Sallahuddin**​ start one in your area


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 07:25*

**+Alex Krause** Might be the way to go. I'll have to look into it a bit more & see what can be done about getting interest in the local region.


---
*Imported from [Google+](https://plus.google.com/100890899415096702555/posts/gE7NjGE4pDy) &mdash; content and formatting may not be reliable*
