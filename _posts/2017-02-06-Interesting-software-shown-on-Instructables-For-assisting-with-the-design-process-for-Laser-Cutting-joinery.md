---
layout: post
title: "Interesting software shown on Instructables. For assisting with the design process for Laser Cutting joinery"
date: February 06, 2017 20:45
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Interesting software shown on Instructables. For assisting with the design process for Laser Cutting joinery.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



[http://www.instructables.com/id/Joinery-Joints-for-Laser-Cut-Assemblies/](http://www.instructables.com/id/Joinery-Joints-for-Laser-Cut-Assemblies/)



Just saw this on Instructables & thought you awesome developers might want to take a look/consider these sort of features since I recall a lot of random discussion about similar topics.



Alternatively, anyone interested may wanna have a play with the software & see if it is helpful for your needs.





Not really sure what category to post this in here... 







**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**greg greene** *February 06, 2017 23:21*

Just what I was looking for !


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/b453i9o7PgY) &mdash; content and formatting may not be reliable*
