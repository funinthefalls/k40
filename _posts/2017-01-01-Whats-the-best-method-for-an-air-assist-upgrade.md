---
layout: post
title: "What's the best method for an air assist upgrade?"
date: January 01, 2017 03:49
category: "Air Assist"
author: "patrick schrader"
---
What's the best method for an air assist upgrade? I've gone back and read all posts here regarding air assist for the the past year but I've just confused myself.

I see that people either use 1. 3d print air assist, 2. use light objects nozzle (requires new lense or gasket) 3. use the Saite kit on ebay.

Some people also use the wire track or self-retracting tubing (i see some people have issues with the track so maybe i'll just use the self-retracting tubing).

Finally is it better to use an air compressor or air pump or what and how big?  I don't have an existing source of air.



Appreciate any advice and happy new years!





**"patrick schrader"**

---
---
**Anthony Bolgar** *January 01, 2017 06:11*

I currently use an airbrush compressor and the Light Objects air assist nozzle on my K40, but shop air and a 3d printed nozzle on my Redsail LE400. I will be replacing the 3d printed with the Light object setup for the redsail, and will be switching the K40 to a shop air source.


---
**Don Kleinschnitz Jr.** *January 01, 2017 13:11*

Details of my builds evolution and components. Look under the appropriate heading for air systems.



[donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**1981therealfury** *January 04, 2017 13:21*

Personally i went with the Light Objects air assist head, i still use the original lens that came with the K40 (mainly because the new lens i ordered from LO was utter trash for some reason) I just lined up up over the hole and closed it up and it has stayed there without issue.



I use a cheap high flow aquarium air pump from Ebay the airflow and i must say I'm very happy for how much it cost me.  It eliminated all flaming / flaring and about 90% of the charring.


---
**patrick schrader** *January 04, 2017 21:35*

Do you happen to know which pump or gph or lpm (or whatever flow) 



Also how was the lens trash? Did it not work as well or was it scratched up or what?  Was it a different focal length? 


---
**Don Kleinschnitz Jr.** *January 04, 2017 21:40*

I use this and am happy with it so far....

[https://www.amazon.com/gp/product/B002JLGJVM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B002JLGJVM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**1981therealfury** *January 04, 2017 22:11*

This is the pump im using at the minute. 



[http://www.ebay.co.uk/itm/60-LT-PISTON-AIR-PUMP-KOI-FISH-POND-FILTER-AQUARIUM-ACO318-/401173028541?hash=item5d67c69ebd](http://www.ebay.co.uk/itm/60-LT-PISTON-AIR-PUMP-KOI-FISH-POND-FILTER-AQUARIUM-ACO318-/401173028541?hash=item5d67c69ebd)



As for the lens from LO, it was the correct lens that i was advised to get to fit the LO air assist head but it didn't work at all well, couldn't cut anything with it and the engrave line was fat, tried it both ways just incase it was made wrong but it made no difference, then after a full power test it just shattered, so I'm guessing it was faulty.


---
**Simon Cook** *January 17, 2017 07:07*

**+1981therealfury** sounds like it was a different focal length. I think the supplied one is about 50mm but LO also do one that is about 38mm (from memory). If you had the different one your piece would need to be 12mm closer or you will get exactly the results you noticed. 


---
**1981therealfury** *January 17, 2017 09:35*

**+Simon Cook**

 I did a ramp test as soon as i started experiencing issues, the lens didn't focus tightly anywhere along the axis but did have a more focused area in the middle of the ramp, so I'm not sure it was a focal length issue.  The one i ordered from LO was an F50.8mm.


---
**Don Kleinschnitz Jr.** *January 17, 2017 13:51*

Strangely my stock K40 came with the 38mm .... go figure! So don't assume anything.


---
**1981therealfury** *January 17, 2017 13:59*

Yea, i agree, these K40's come in lots of different configurations and design iterations, Almost every one i see is different in some way to mine haha.


---
*Imported from [Google+](https://plus.google.com/106274924798980872372/posts/2mKZCR8YjtD) &mdash; content and formatting may not be reliable*
