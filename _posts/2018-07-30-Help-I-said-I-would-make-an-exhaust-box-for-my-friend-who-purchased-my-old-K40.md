---
layout: post
title: "Help! I said I would make an exhaust box for my friend who purchased my old K40"
date: July 30, 2018 01:28
category: "Discussion"
author: "HalfNormal"
---
Help!

I said I would make an exhaust box for my friend who purchased my old K40.

I need the dimensions of the exhaust opening brackets that hold the fan.

Unfortunately he is unavailable to check the dimensions.

Thanks!







**"HalfNormal"**

---
---
**Paul de Groot** *July 30, 2018 06:35*

Sorry I am in Singapore so can't measure my k40 at home. What about thingiverse [https://www.thingiverse.com/thing:2272337](https://www.thingiverse.com/thing:2272337)

[thingiverse.com - K40 Exhaust Vent by pista01](https://www.thingiverse.com/thing:2272337)


---
**Ned Hill** *July 30, 2018 14:29*

The inside horizontal width of the brackets for mine is 245mm.  The vertical distance from the bottom bracket to the top of the opening is 85mm.  Let me know if you need anything else.


---
**HalfNormal** *July 30, 2018 14:36*

Thanks guys for helping out!


---
**Ned Hill** *July 30, 2018 20:18*

Are you going to do a straight out vent tube connector or an angled 90 deg one?


---
**HalfNormal** *July 30, 2018 20:38*

I am going to have the hole but it will be up to my friend if he wants a 90 or not.


---
**Martin Dillon** *July 31, 2018 22:12*

I printed and used this one [thingiverse.com - 40W Laser Cutter Exhaust Adapter by Bartimaeus](https://www.thingiverse.com/thing:628269)  Reinforced it with epoxy and fiberglass and it is bullet proof.  It has a file with dimensions but you will need a program to view it.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/PBBKy7AwgHM) &mdash; content and formatting may not be reliable*
