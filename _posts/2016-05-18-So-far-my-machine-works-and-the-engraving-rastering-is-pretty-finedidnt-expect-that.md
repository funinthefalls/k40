---
layout: post
title: "So far my machine works and the engraving rastering is pretty fine(didn't expect that)"
date: May 18, 2016 14:39
category: "Air Assist"
author: "Sebastian C"
---
So far my machine works and the engraving rastering is pretty fine(didn't expect that). Worked paper, cardboard, acrylic, wood (3mm), glass etching, but I have problems cutting deeper without charring.



If someone has an spare air assist nozzle or could print one for me I would be really grateful. Just tell me what you want for it.

This one would be nice:[http://www.thingiverse.com/thing:1063067](http://www.thingiverse.com/thing:1063067)





**"Sebastian C"**

---
---
**Jeff Kwait** *May 18, 2016 14:50*

I just bought one on ebay haven't installed it yet

[http://www.ebay.com/itm/262399806384?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/262399806384?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Anthony Bolgar** *May 18, 2016 15:10*

I have a couple of this design printed and ready to go. I also have the line pointers for them. I can let you have one for $10 (Just to cover the cost of the line pointers, if you don't want or need the pointers then just cover the shipping, you can have the nozzle for free)) plus shipping, but not sure if that will be reasonable enough on shipping because I am located in Canada. But let me know if you want it.


---
**Mishko Mishko** *May 18, 2016 18:15*

**+Anthony Bolgar** Is there any advantage in usig two line lasers over one cross line like this one for example?

[http://www.ebay.com/itm/New-Focusable-5mW-650nm-Red-Cross-Line-Laser-Module-Focus-Adjustable-laser-Head-/171875188146?hash=item28048f3db2:g:5jMAAOSwDNdVubrM](http://www.ebay.com/itm/New-Focusable-5mW-650nm-Red-Cross-Line-Laser-Module-Focus-Adjustable-laser-Head-/171875188146?hash=item28048f3db2:g:5jMAAOSwDNdVubrM)


---
**Anthony Bolgar** *May 18, 2016 22:04*

When you use two crossed line lasers in the nozzle I suggested, it crosses on the work piece directly in the center of the nozzle, no matter the distance from the object. A single dot laser is only directly in the center at a specific distance from the object.


---
**Sebastian C** *May 18, 2016 22:48*

**+Anthony Bolgar** Hey thx for your offer. I have already some line lasers here but would just take the nozzle.

I researched some postage fees...omg. I would take it if you ship it with Canada Post :Small PacketTM - International Surface - $8.02 .other options are too expensive, but maybe someone has a better shipping proposal. 


---
**Anthony Bolgar** *May 18, 2016 22:53*

I just sent a small PSU to Peter van der Walt in South Africa. Cheapest option VIA Canada Post by surface turtle delivery (6-8weeks) was $32.00 CDN dollars. But I digress, let me know what shipping method you want to use (pretty sure it will end up being Canada Post small Packet) , and I will get it out to you as soon as possible.


---
**Jerry Martin** *May 20, 2016 03:19*

What will it cost for me to get one of these sent to Wisconsin?


---
**Robin Sieders** *May 23, 2016 14:28*

I can print and ship you one **+Jerry Martin**​, if you'd like. I live on the Canadian/Minnesota Border, so can ship from the US. I don't have the line lasers though.


---
**Jerry Martin** *May 23, 2016 21:24*

I would love to have you do this for me. Send me a email at JerryLouisMartin @ [gmail.com](http://gmail.com) so we can work out the details.


---
**Gary Hamilton** *June 28, 2016 19:41*

**+Anthony Bolgar** I am going to send an email.  If you still have air assists you are looking to sell.


---
**Anthony Bolgar** *June 28, 2016 19:49*

I can print up some more if you want one.


---
*Imported from [Google+](https://plus.google.com/108800242160866633862/posts/ALgz9UmYcyB) &mdash; content and formatting may not be reliable*
