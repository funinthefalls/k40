---
layout: post
title: "I'm running a heavily modified K40 with Smoothie and LaserWeb3"
date: December 11, 2016 16:07
category: "Discussion"
author: "K"
---
 I'm running a heavily modified K40 with Smoothie and LaserWeb3. I'm seeing some very strange patterns in my engraving. I generally only engrave small spaces (see the photo with Mom), and when I do, this pattern covers the entire space. When I test engraved a large circle it pops up at the edge. When I do something larger than letters, but smaller that the 3" circle it comes in symmetrically from the edges (see the photo with the tops of the three avengers icons)It's very odd. The pattern reminds me of a moire pattern. My mirrors are all tight, and I don't have anything vibrating against the machine or the table it sits on. Because it appears at the edges I thought maybe acceleration/deceleration had something to do with that, so I played with the feed speeds in Smoothie going up and down from 4K to 20k. No difference. I've increased/decreased stepper voltage on the off chance the steppers weren't getting enough / getting too much power. I've swapped out beds to be sure the bed I was using wasn't causing undue vibrations. I've reflashed the board. I'm on a Mac. My next step is to try switching to my PC to see if there's a difference, but I can't get past "npm install" and my start menu decided to start crashing, so I'm doing a fresh install of Windows 10. I've made sure none of my equipment was sitting directly against the case or the table the cutter sits on and thus inducing some sort of vibration. I'm sort of out of ideas. 





**"K"**

---
---
**Alex Krause** *December 11, 2016 16:23*

Have you checked to see if you have the latest firmware installed?


---
**K** *December 11, 2016 16:24*

I do indeed have the latest CNC firmware installed.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 16:24*

Here's the particular post I was referring to previously that had something to do with moire discussion: [plus.google.com - Had an interesting observation with my machine today. Was reverse engraving a…](https://plus.google.com/u/0/108257646900674223133/posts/2mP6xqYF6Bf?sfc=true)


---
**Alex Krause** *December 11, 2016 16:25*

Have you tried to generate Gcode from **+Sébastien Mischler**​'s [Raster.js](http://Raster.js) module


---
**K** *December 11, 2016 16:26*

**+Yuusuf Sallahuddin** Yes, thank you for that! I found it yesterday, and I made sure none of my equipment was sitting against the case or table. It's such an odd problem. 


---
**K** *December 11, 2016 16:27*

**+Alex Krause** Not yet. That's the new LW4, stuff, right? 


---
**Alex Krause** *December 11, 2016 16:28*

It is standalone as of yet until it is integrated to lw4 it has all sorts of things you can tinker with just to verify it isn't a gcode generation issue


---
**Alex Krause** *December 11, 2016 16:30*

[https://lautr3k.github.io/lw.rasterizer/](https://lautr3k.github.io/lw.rasterizer/)


---
**K** *December 11, 2016 16:31*

**+Alex Krause** I'll give it a try. Thank you. It's just so puzzling to have this pop up all the sudden. And true consistency of the pattern is really fascinating. 


---
**Ned Hill** *December 11, 2016 17:54*

**+Kim Stroman** can you provide a pic.  You reference one but I don't see it.




---
**K** *December 11, 2016 17:55*

Dang. I didn't realize my photos didn't show up. ![images/e70feabc5a959419e04110dd1213da22.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e70feabc5a959419e04110dd1213da22.jpeg)


---
**K** *December 11, 2016 17:55*

![images/8c9d4bfa0efd00fb237dfa2fe1149712.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8c9d4bfa0efd00fb237dfa2fe1149712.jpeg)


---
**K** *December 11, 2016 17:55*

![images/1c0c136771f0aebaef0917108c87c37e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c0c136771f0aebaef0917108c87c37e.jpeg)


---
**Ned Hill** *December 11, 2016 18:06*

Since it's mainly appears on the edges for large areas  I would agree that it looks like it might be coupled to acceleration/deceleration and PWM.  I'm not using laserweb yet so I'm unable to really help from that end.  Does the pattern or coverage change if you change the PWM?  Not sure what setting that would be.


---
**K** *December 11, 2016 18:11*

**+Ned Hill** by change the PWM what do you mean?


---
**Ned Hill** *December 11, 2016 18:14*

The pulse width modulation of the beam.  I know laserweb handles this but I'm not sure what the settings are since I'm not using it yet.  Maybe **+Alex Krause** can provide more insight into what I'm talking about from the software side.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 18:30*

I'm wondering is it related to having black & white speed settings the same, since you previously referenced 300/300 if I recall correct **+Kim Stroman**. So I wonder have you tested different speeds? I tend to use something like 200 (black) 400 (white) when I engrave, but have yet to test any engraving since a few weeks ago.



If you can share a gcode output file for one of these images you are working with I can test that specific file with my current settings & see if I get the same results.


---
**K** *December 11, 2016 18:34*

**+Yuusuf Sallahuddin** I've tried tons of different speeds. I think all of the photos I have here were 300/175 or there abouts. I think I also tried 300/200. I'm not at home right now, but I'll grab the file later and share it. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 18:41*

**+Kim Stroman** Cheers. It's just morning here in AU, so whenever you send I am likely to be able to have a go at it sometime today & see.


---
**K** *December 11, 2016 19:01*

The gcode I'm going to post (since it's the last test I ran) is for these little star Wars icons, just to give you reference. ![images/55899cce883745dbef42a3c238d30e25.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55899cce883745dbef42a3c238d30e25.jpeg)


---
**K** *December 11, 2016 19:02*

Actually, disregard. That one was done at 300/300. Let me try one at 400/200. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 19:12*

**+Kim Stroman** I am running Smoothie 4x, on firmware-cnc.bin (although haven't updated recently so I might need to check my version later to compare).


---
**K** *December 11, 2016 19:18*

I've got an Azteeg X5 Mini on the latest CNC. Here's a photo of something I just engraved at 400/200. I'll post the gcode for this. ![images/45d19ea47b5cfd81f32cdf09bee30596.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/45d19ea47b5cfd81f32cdf09bee30596.jpeg)


---
**K** *December 11, 2016 19:19*

File on Drive: [drive.google.com - avengers.gcode - Google Drive](https://drive.google.com/file/d/0B1kvMudpU9EMT3V4RGpsSVdta2s/view?usp=sharing)




---
**K** *December 11, 2016 19:25*

They remind me of ringing you see in 3D printing when acceleration is set too high. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 19:31*

I think **+Ariel Yahni** has the azteeg also? Might be good if someone with an azteeg can test too. I'll test on smoothie later in the day :)


---
**Ariel Yahni (UniKpty)** *December 11, 2016 19:34*

Have you tried bringing acceleration down? 


---
**K** *December 11, 2016 20:37*

**+Ariel Yahni** If by acceleration you mean the default feeds at the top of the config file, yes. As low as 4k as high as 20k. No effect.


---
**K** *December 11, 2016 20:38*

I'm trying to install LW3 on my Windows10 machine to rule out some weirdness with the Mac. I get everything installed but when I run server.js I get this popup. Does anyone know what it means?

![images/2d17090fd81735217fa8a8c7029a4949.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2d17090fd81735217fa8a8c7029a4949.png)


---
**Ariel Yahni (UniKpty)** *December 11, 2016 20:42*

**+Kim Stroman**​ this are feed and seek rate. Further down the config is acceleration


---
**Ariel Yahni (UniKpty)** *December 11, 2016 20:43*

Node server


---
**K** *December 11, 2016 20:43*

Under #Planner Module Configuration? What do you have yours set to? Mine is the default 5000.




---
**K** *December 11, 2016 20:45*

The file is now called server.js. At least that's what I gleaned from a previous post and that's what's in my LaserWeb3 folder.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 20:50*

**+Kim Stroman** you need to call the node.exe parsing it the server.js... so the command is node server.js


---
**K** *December 11, 2016 20:51*

Ahhh! I understand now. Thank you!


---
**K** *December 11, 2016 20:52*

I really need to take the time to learn more than the bare minimum as far as that side of things go.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 20:55*

**+Kim Stroman** Somewhat majority of this stuff goes over my head, so glad to be able to impart what little I do understand.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 21:35*

**+Kim Stroman**​ there is an executable file that was build and I also can build one for OSX. But I need to update it


---
**Ariel Yahni (UniKpty)** *December 11, 2016 21:35*

I think acceleration default is 3000.i don't. Have the file with me


---
**Alex Krause** *December 11, 2016 21:37*

3000 default


---
**K** *December 11, 2016 21:47*

Ok, I dropped the acceleration down to 3000, then 1000, then 500. There was no real difference between 500 and 1000, but the difference between 3000 and 1000 is massive. Still some issues, but not nearly as bad. 


---
**K** *December 11, 2016 21:47*

Also, definitely not Mac vs PC related. Same result on either platform. 


---
**Ariel Yahni (UniKpty)** *December 11, 2016 21:51*

I'm having a different issue but I think it's worth it if you also test it yourself. Load you file into LW4 ( hosted nothing to install)  generate a raster via Laser Fill Path option and the run it on LW3


---
**Ariel Yahni (UniKpty)** *December 11, 2016 21:52*

[https://laserweb.github.io/LaserWeb4/dist/](https://laserweb.github.io/LaserWeb4/dist/)


---
**K** *December 11, 2016 21:52*

**+Ariel Yahni** Will do.


---
**Ariel Yahni (UniKpty)** *December 11, 2016 21:55*

![images/3ea442df80d97f249a654fb25c753817.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ea442df80d97f249a654fb25c753817.jpeg)


---
**K** *December 11, 2016 22:00*

For some reason I'm not getting that box.


---
**K** *December 11, 2016 22:03*

Ok, so here's a sample with the acceleration dropped to 500. Before at 5000 after at 500. Still not perfect, but super duper better. ![images/c461b9f5e2dd9d6539b3f4ca30a14eb5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c461b9f5e2dd9d6539b3f4ca30a14eb5.jpeg)


---
**Ariel Yahni (UniKpty)** *December 11, 2016 22:09*

**+Kim Stroman**​ is this LW3 or LW4


---
**K** *December 11, 2016 22:11*

This is LW3. I don't get the settings box you show under CAM. I may need to install something else to get it to work (I just did a reset of my system). 


---
**Ariel Yahni (UniKpty)** *December 11, 2016 22:14*

No need. Once you load the file drag it by its name to the section bellow


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 04:01*

Those results at 500 accel do seem a lot better **+Kim Stroman**.



Interesting to note the effect of accel now. Previously I noticed minimal effect changing it to any value.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 06:34*

Here is my test with the gcode file you sent. Only changes I made was to replace all instances of Y2 with Y (as it was outside my engrave area).



I tested with the grain & against the grain of the wood. 10mA power with whatever your settings were in the gcode.



My smoothie speed settings in config are:



default_feed_rate                            27000

default_seek_rate                            27000

acceleration                                 4000



![images/de89c02d764842a5804a60f1051f5cf2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de89c02d764842a5804a60f1051f5cf2.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 06:50*

Oh, mine is also backwards, as I still haven't bothered looking into whatever causes that on my machine. For me, it's typical "Don't fix it if it ain't broke... or there is some workaround" haha.


---
**K** *December 13, 2016 05:19*

**+Yuusuf Sallahuddin** I turned my machine on this evening and did some engraving. The problem is completely gone! I wonder if when I went from 1000 to 500 it didn't actually "take" and the reboot was actually running at 500. It's ridiculously slow, but the engraving looks great.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2016 12:00*

**+Kim Stroman** That's good news, but would be interesting to know what exactly caused the fix, in case it happens again. But hopefully it hangs in there.


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/2iUDH2ycuqF) &mdash; content and formatting may not be reliable*
