---
layout: post
title: "Waiting on a wood delivery for my next builds, so I thought I would throw up a shop tips post"
date: October 28, 2017 22:40
category: "Discussion"
author: "Ned Hill"
---
Waiting on a wood delivery for my next builds, so I thought I would throw up a shop tips post.  

In this edition of #K40ShopTips let’s consider the impact of wood grain direction on your creations.  In my experience there are two ways direction of the wood grain impacts projects created with wood, structural and visual.  



The structural aspect is typically related to how much a piece of wood will bend when a load is applied, in that a piece of wood is stiffer parallel to the grain then perpendicular to the grain (see pics).  This becomes more important as the wood becomes thinner and the pieces cut become narrower.  In general, you usually like to have the long direction of you piece parallel to the grain for the best stability, but it will depend on your application.  Plywood typically is less affected than solid wood and pieces >= ¼” thickness are also less affected.  



Next up is the visual aspect.  When holding a piece of wood up, with the face of the board facing you, the wood appears brighter when the grain is parallel to the floor then when the grain is perpendicular to the floor (see pics – note that the wood shown does have a coat of lacquer which enhances the effect).  This is caused by way the grain reflects light.  When the grain is horizontal it is reflecting more light up into your eyes and reflects it away to the side when it is viewed vertically.   So, when making pieces that are to be viewed facing out, like for a plaque, I usually like to have the grain oriented horizontally when possible. However If the wood is painted so that the grain doesn’t show then it doesn’t matter.





![images/02fbca3ce0a601f6df8670069dca6b88.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/02fbca3ce0a601f6df8670069dca6b88.jpeg)
![images/f8647e1f5e522c028db9c804213043e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f8647e1f5e522c028db9c804213043e6.jpeg)
![images/b4ebb0a3e3cedcbf7c6c1269ad050601.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b4ebb0a3e3cedcbf7c6c1269ad050601.jpeg)
![images/7aa2a35956d5663e7d779ee5d8f5d06f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7aa2a35956d5663e7d779ee5d8f5d06f.jpeg)
![images/1cbc2ae152588538956f59d6107286d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1cbc2ae152588538956f59d6107286d9.jpeg)

**"Ned Hill"**

---
---
**Anthony Bolgar** *October 28, 2017 23:40*

Another point to remember is that wood will expand and contract. It is more noticeable across the grain vs. the grain running end to end. Plywood is also less affected by this, however hardwood and softwood with a large grain pattern, such as oak can expand and contract as much as 1/4" over a 4' length. When assembling a large cabinet this becomes important, you do not want to continuously glue the joints when dealing with large widths, you need to leave free areas for the expansion and contraction to occur. Do not put too many fasteners close together to allow the expansion/contraction as well.


---
**Ned Hill** *October 28, 2017 23:55*

**+Anthony Bolgar** good point :)  Another thing along those lines to consider is which side of the board to use.  I've sometimes used a board that had a slight cup to it, after I've straightened it or planned it out.  I would then like to keep the "bark side" up or out in case it wanted to cup a little more later on so it's not as noticeable. 


---
**Don Kleinschnitz Jr.** *October 29, 2017 03:42*

Another: always apply finish to both sides of wood to reduce stress and bowing.


---
**Anthony Bolgar** *October 29, 2017 07:09*

Good rule of thumb is whatever you do to the top side should be done to the bottom, like if you glue laminate to the top, you should also laminate the bottom side.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/dfaPEDkyEb2) &mdash; content and formatting may not be reliable*
