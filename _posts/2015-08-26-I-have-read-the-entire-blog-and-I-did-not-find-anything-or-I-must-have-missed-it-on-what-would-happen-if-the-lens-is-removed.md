---
layout: post
title: "I have read the entire blog and I did not find anything (or I must have missed it) on what would happen if the lens is removed?"
date: August 26, 2015 23:41
category: "Hardware and Laser settings"
author: "Phil Dashcam"
---
I have read the entire blog and I did not find anything (or I must have missed it) on what would happen if the lens is removed?



I was thinking that the lens is limiting the effective range of the beam by concentrating it on one "sweet spot" then dispersing or spreading it beyond that point.  Isn't the beam from the laser tube narrow enough to make a good cut?  Or are the mirrors not flat enough and they are actually messing up the beam?  I was also thinking of clearing away the bottom of the machine to make it engrave on a large object below it, for example, a conference or dining table?  Or I should ask, what modifications should I do to make this possible?



I am still thinking of getting a laser machine but the nagging issue I just mentioned is holding me back.  Or should I get one of those cheap low power laser engravers with no beam range limits; the strongest laser I have found is 5.5 watts.



Thanks for any and all feedback on my issue.





**"Phil Dashcam"**

---
---
**ThantiK** *August 26, 2015 23:47*

You have to focus the laser on a pinpoint in order for it to work worth a crap.  You can't take a 40w laser and engrave something across the room.  You have to have the focus somewhat close.  Lasers are coherent light, but even coherent light scatters and interferes with itself.



No, the laser from the tube is not narrow enough to make a cut.


---
**Phil Dashcam** *August 27, 2015 00:01*

Thanks for your quick response.



But if that is true, then there's actually only one sweet spot in the entire x-y coordinate platform because both x and y mirrors are moving and the displacement is as high as 300mm on the y-axis.  I am correct?


---
**ThantiK** *August 27, 2015 00:03*

The power doesn't drop off <i>too terribly</i> at the far edge of the laser, but yes it does drop off.  That's why these types of lasers are typically found in the smaller laser cutters.


---
**Phil Dashcam** *August 27, 2015 00:14*

If I got you correctly, the varying displacement in position of the x and y mirrors are insignificant to cause a drop in the power of the beam at its point of contact at the surface of the target object.  So why would a 100mm displacement on the z-axis be significant and would now require a lens to focus?



Sorry if I sound annoying.


---
**ThantiK** *August 27, 2015 00:19*

Because the beam that's traveling the mirror path isn't tight enough to cut anything.  The power is spread over a wider area so that the mirrors can bounce it.  If you put a piece of wood, or acrylic in the beam path, you'd merely scorch it and not cut it.  Focusing the beam early is simply a recipe for damaging your mirrors.


---
**Jim Coogan** *August 27, 2015 00:22*

Without the lens the beam would be too wide to be of any use but will still burn a wall 20 feet away.  Have done it :-)  The lens focuses the beam to a specific diameter to do its work well.  For a stock lens which is normally 50.8mm that beam diameter is .005".   For a 38.1mm, which is an engraver lens, the diameter is .003".  For the 63.5mm lens it is .007".  This last lens is used a lot for cutting only.  The focal length is where the beam converges into a focal point (sweat spot) with a beam diameter specific to the lens.  The energy lost within the confines of the laser box is dictated by the quality of the lens and the mirrors.  By using upgraded ones you would end up with a significant boost at the point of impact of the beam.  Some have added "basements" to their lasers and then used special lens carrier tubes (like you would see on a microscope) to allow you to reach down further in the box.  A deeper box also allows you to build a Z axis lift so you can move parts into the proper position under the lens.


---
**Phil Dashcam** *August 27, 2015 00:34*

I got it now.  Thanks.



So my next question is, would that raw beam, without an intermediary lens, be good enough for engraving on an object below the confines of the box, after clearing out the platform and the bottom of the case?  Or should I place a lens farther down the z-axis and near the target object to make the beam converge on a small focal point on the object?


---
**Jim Coogan** *August 27, 2015 01:11*

You cannot use the laser without a lens to focus the beam.  It will be too wide to be effective.  There are things like this that allow the lens to be further down the z axis.  [http://www.ebay.com/itm/Co2-Laser-Head-Engraver-Cutter-Mirror-Dia-25mm-Lens-Dia-20mm-FL-100mm-4-SAIL-/151488377371?hash=item234569321b](http://www.ebay.com/itm/Co2-Laser-Head-Engraver-Cutter-Mirror-Dia-25mm-Lens-Dia-20mm-FL-100mm-4-SAIL-/151488377371?hash=item234569321b)


---
**Jim Coogan** *August 27, 2015 01:18*

A 50.8mm lens will focus the beam 50.8mm from the face of the lens.  That is the hot spot.  Once you go beyond that the beam begins to diverge, spread out.  Extension tubes allow the lens to be further down the Z axis but the focal point is still 50.8mm from the lens face.  The problem you have with extensions is that on machines like the K40 the bridge structure may not be stiff enough to carry the weight on one side and could throw off the beam.  In addition, you face the possibility of more vibration from the carriage movement which affects the machines ability to accurately cut or engrave.  It could also prematurely wear out components.


---
**Phil Dashcam** *August 27, 2015 02:08*

@ThantiK

@Jim Coogan

Thanks a lot for your incisive comments.  I guess, my next best option to engrave on a large table is to use a small laser engraver/cutter (maximum power available in the market is 5.5w) that does its work beyond its frame.  It will be slower but I think it will get the job done.  Or is there also a caveat to this option?


---
**Jim Coogan** *August 27, 2015 02:44*

Unfortunately, or fortunately, depending on how you look at it there are just as many options as there are machines.  My K40's cutting/engraving area is 12.6" (320mm) x 8.6" (220mm).  I am in the process of adding a "basement" and switching it over to RAMPS to get more control over the laser.  The way to look at this is what would be the largest item I would engrave or cut on a regular basis.  We all have that once in a blue moon special that exceeds our limits.  But for the most part there is a range you will always work within.  Then look at how much you want to spend and how much time you want to spend engraving a single object.  If you do this as a hobby then the last one doesn't matter.  As a business it does.  After you know these things you will have your own set of specs for what would work for you.


---
**ThantiK** *August 27, 2015 02:58*

Those little blue 5w lasers also have a focal point you know. ﻿ And they don't cut or engrave for shit. 


---
**Phil Dashcam** *August 27, 2015 05:38*

@Jim Coogan

Well said.

I don't plan on getting into business with a laser machine so speed is not a big concern.  I chanced upon this forum and engraving something on a large table (with a glass top) came to mind.  I now also think it would be nice to have one as a garage tool/toy that may come in handy in certain situations. 



@ThantiK

If a K40 cannot do what I am planning to do and you think that a small laser cannot do it either, are you suggesting that my only option is to do it manually by hand with a carving tool or chisel?  But from what I have seen from YouTube video demos of these small lasers, they engrave objects below their frames and they do it quite well.  Unless somebody has a different information, their focal point must be on the edge of their frame or close to it.  That is what I am after and it appears that the K40 cannot do that and it cannot reliably work on objects larger than its platform.  The only drawback I can see with small lasers is that they are really very slow owing to the fact that they are low-powered.



No offense to all K40 owners but I think a small laser will serve my purposes better at this time (I hope).  I thought a K40 could do the work I want to be done.  I just wish I could also find a discussion group on these small laser machines to confirm what I'm planning to do.


---
**Jim Coogan** *August 27, 2015 05:43*

If you have a Facebook account there is a group on it you should check out.  It's called Laser Engraving and Cutting.  There are a lot of very knowledgeable people on there and they might be able to help. 


---
**Phil Dashcam** *August 27, 2015 05:47*

Great!  I'll look into it.  Thanks.


---
**Stephane Buisson** *August 27, 2015 07:24*

**+Phil Dashcam** look at **+Mr Beam** 

[http://www.mr-beam.org/](http://www.mr-beam.org/)


---
*Imported from [Google+](https://plus.google.com/109038025331535061003/posts/EYtYYfQEj7T) &mdash; content and formatting may not be reliable*
