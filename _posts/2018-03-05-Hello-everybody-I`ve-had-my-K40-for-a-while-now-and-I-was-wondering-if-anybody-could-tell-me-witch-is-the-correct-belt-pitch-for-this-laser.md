---
layout: post
title: "Hello everybody, I`ve had my K40 for a while now, and I was wondering if anybody could tell me witch is the correct belt pitch for this laser"
date: March 05, 2018 14:21
category: "Hardware and Laser settings"
author: "Diego Papierman"
---
Hello everybody, I`ve had my K40 for a while now, and I was wondering if anybody could tell me witch is the correct belt pitch for this laser. I`ve tried with the GT2 that is for 3D printing but  doesnt work well. I would appreciate a link to the correct belt. Thanks in advance..





**"Diego Papierman"**

---
---
**HalfNormal** *March 05, 2018 15:40*

According to LightObjects website the belts are Pitch: 2mm

Width: 9.48mm



[http://www.lightobject.com/Y-axis-belt-for-K40-P760.aspx](http://www.lightobject.com/Y-axis-belt-for-K40-P760.aspx)



You can also get some information from the M2nano board operational manual translated into english here.



[https://www.everythinglasers.com/forums/topic/m2nano-manual/](https://www.everythinglasers.com/forums/topic/m2nano-manual/)








---
**Jim Fong** *March 05, 2018 18:14*

From what I’ve read the k40 belt isn’t gt2 2mm pitch but is actually 2.03mm. Probably a Chinese version of MXL .08” belt pitch (2.03mm)



Some older lightobjects web pages list the pitch as 2.3mm but that is a typo error and should be 2.03mm


---
**Diego Papierman** *March 05, 2018 19:56*

**+Jim Fong** 

Thank you very much I also read somewhere that the pitch was 2.03mm but wasn`t sure. 

Could you possibly post a link to where I could buy this belt...? 

Thanks again...


---
**HalfNormal** *March 05, 2018 20:29*

**+Jim Fong** Thanks for clarifying the information. 


---
**Jim Fong** *March 05, 2018 21:13*

**+Diego Papierman** 

lightobjects Even though the description says it is 2mm, I’m fairly certain it will be the correct replacement belt. 



[lightobject.com - X axis belt for K40](http://www.lightobject.com/mobile/X-axis-belt-for-K40-P761.aspx)


---
**Anthony Bolgar** *March 06, 2018 20:41*

**+Jim Fong** is correct, I have purchased a belt from them for a replacement on one of my K40's.




---
*Imported from [Google+](https://plus.google.com/116915114916630445747/posts/ZnBZbfdcacm) &mdash; content and formatting may not be reliable*
