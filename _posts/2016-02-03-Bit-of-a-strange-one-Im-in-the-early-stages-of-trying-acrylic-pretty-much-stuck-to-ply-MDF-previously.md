---
layout: post
title: "Bit of a strange one. I'm in the early stages of trying acrylic, pretty much stuck to ply & MDF previously"
date: February 03, 2016 00:50
category: "Materials and settings"
author: "I Laser"
---
Bit of a strange one. I'm in the early stages of trying acrylic, pretty much stuck to ply & MDF previously. It seems no matter what power/speed or whether I remove the protective sheet from both or either side my machine fails to consistently cut through it. There's always a few bits connecting the piece I'm cutting.



I'm currently just using MDF as a base, so there's no ventilation below, but even when I've tried to cut something sitting the target in mid air it still won't cut completely through. :\



Any ideas?





**"I Laser"**

---
---
**Jim Hatch** *February 03, 2016 01:34*

How thick? Is your laser focal set for the midpoint of the material? For instance, I have a 50.8mm lens so I set the bed to be 50.8mm + 3mm or 53.8mm when cutting 1/4" (6mm) acrylic. That leaves half the thickness above & half below the center of the material and the defocused area is equal on both sides (like an hour glass shape). 



Play with your speed & power. I usually use 75% power & 20-35mm/sec for speed (I think - haven't cut any in a couple of weeks, probably need to start writing this stuff down). You may be going too slow so it melts but puddles.



Also if your extra bits are in the corners check your corner speed to slow it down a bit rounding the curve.



If they're always in the same place relative to the bed, check your mirror alignment for the extreme travels positions in case they're off a hair toward the far ends.


---
**Sunny Koh** *February 03, 2016 01:34*

Settings? I was using 85% Power at 12mm per second for 3mm thickness. I believe the machine max out at 5mm and anything more you have to repeat the cut to fire through as taught by my uncle a few years back.


---
**Jim Hatch** *February 03, 2016 01:39*

**+Sunny Koh**​ My machine is max speed of 350mm/s and cutting speed ranges between 20-35. I only use the faster speeds for engraving. Too slow and the material tends to burn. Too fast & it doesn't cut all the way.



If I can't get a full depth cut I'll do a second or third pass vs slowing it to where it burns. I just set the Repeat parameter & don't touch the piece in between. It automatically repeats the cuts without me needing to send it again.


---
**I Laser** *February 03, 2016 01:53*

The focal length is right (I believe), a ramp test shows narrow point at 24.5mm, I'm trying to cut 3mm acrylic so have it set to 23mm.



I think you might be onto something **+Jim Hatch** regarding the too fast/slow puddles. Even when in the air I've noticed what looked like water drops on the reverse. Though it's probably acrylic because it stinks! Never even considered cutting too slow would be an issue.



**+Sunny Koh** 85% is really pushing your tube, you might want to lower the power and speed to conserve your tube a bit.


---
**HP Persson** *February 03, 2016 07:36*

Sure it´s cast acrylic? and not one of thoose polycarbonate plastics or any of all other types there is.

I know some of the types of transparent plastics just make a mess. Like in my CNC router, if i go with "fake" acrylic, it clogs up the bit. Seen similar in laser cutting.




---
**I Laser** *February 03, 2016 09:01*

Yep that's what the sticker on it says :)



Haven't tried clear yet, been playing with 502 black. Might give the clear a go just to see if it's any different.


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/CKW8BfPutHg) &mdash; content and formatting may not be reliable*
