---
layout: post
title: "Hi, First time poster here! I finally got the wife to agree to let me buy a CO2 laser and need some advice"
date: September 01, 2016 00:29
category: "Discussion"
author: "Randy Draves"
---
Hi,  First time poster here!



I finally got the wife to agree to let me buy a CO2 laser and need some advice.  I just recently got finished building the 3dp burner from Thingiverse.  While it was a fun build and a great design it just couldn't cut it for me.  Literally, it couldn't cut the acrylic that I built it for.  Now I know I need a CO2 laser to do that.  I was reading through the posts here and other forums about the ebay 40W CO2 lasers and how they can be pretty good after some upgrades.  



My question is that when I started looking on ebay I noticed that there is some that are slightly different.  ie. having 110v plugs in place of a fan on the back, red instead of blue.   I don't know if ones older than the other?  



Which one do you guys recommend?  Do you have a preferred seller?



Thanks in advance for any advice.









**"Randy Draves"**

---
---
**greg greene** *September 01, 2016 00:40*

Blue ones are generally for NA market, work ok with upgrades, red ones more expensive - come with some upgrades - either one will work for acryllic




---
**Ariel Yahni (UniKpty)** *September 01, 2016 00:56*

It's all about preference, if you would like almost directly into productive mode then go with the one full of feature and upgrades (if budget permits) if you like to build as you go then choose again based of preference and budget.  Seller rating will only get you starting, I bought from the highest ranking and had so much trouble with him that they ended refunding me almost the complete value 


---
**Randy Draves** *September 01, 2016 01:00*

I see that they have some red ones that are the same price as the blue.  They only difference that I can tell from the pictures is that the lid on the laser tube has latches and there is an extra outlet on the back.  But the window looks smaller.  


---
**Randy Draves** *September 01, 2016 01:09*

I was considering:

[ebay.com - Details about  Professional 40W CO2 Laser Engraver 12"x 8" w/ Exhaust Fan USB Port](http://www.ebay.com/itm/141974463163)


---
**Randy Draves** *September 01, 2016 01:10*

or.

[ebay.com - Details about  High Precise 40W CO2 Laser Engraving Cutting Machine Engraver Cutter USB Port](http://www.ebay.com/itm/201264099304)


---
**Ariel Yahni (UniKpty)** *September 01, 2016 01:14*

They look the same and so are the important specs. I would take red just to have a different color maybe


---
**Randy Draves** *September 01, 2016 03:03*

Well I ended up buying the red one.  The blue one went up about $20 while I was trying to decide.  Here's hoping that nothing is wrong with it. : )


---
**Ariel Yahni (UniKpty)** *September 01, 2016 03:08*

**+Randy Draves**​ don't expect it to be. Welcome to the club 


---
**Anthony Bolgar** *September 01, 2016 03:15*

How much was shipping, the listing will not give me the price of shipping, keeps saying to enter a valid zip code, I tried 3 different zips and no joy. Makes me kinda of nervous when a seller will not let you know the shipping up front.


---
**Randy Draves** *September 01, 2016 03:16*

Thanks!  


---
**Ariel Yahni (UniKpty)** *September 01, 2016 03:20*

**+Anthony Bolgar** it showed me $30.00. Still that 60.00 less from a couple of months now considering that freigth from china have increased almost 5X


---
**Randy Draves** *September 01, 2016 03:22*

The price was 261.86 and shipping to Illinois was 30.


---
**Anthony Bolgar** *September 01, 2016 03:24*

That is a pretty good deal. Enjoy!


---
**Randy Draves** *September 01, 2016 03:28*

After I bought it I clicked on the 18 sold and saw a couple of buyers declines and Buyer retracted offer statements.  I guess I'll find out tomorrow if there would be any issues.


---
**Randy Draves** *September 02, 2016 03:21*

Well it shipped from Tennessee with an estimated delivery date of this Saturday.  Was supposed to be next Friday.  If everything is in decent shape then I'll give the seller a good review.


---
*Imported from [Google+](https://plus.google.com/110537209659823059360/posts/bR71S3BCs9R) &mdash; content and formatting may not be reliable*
