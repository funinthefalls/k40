---
layout: post
title: "I need to upgrade my exhaust. I am cutting mostly ballistic birch ply in my basement but the fumes are leaking into the house above"
date: March 29, 2016 02:41
category: "Discussion"
author: "3D Laser"
---
I need to upgrade my exhaust.  I am cutting mostly ballistic birch ply in my basement but the fumes are leaking into the house above.  right now I am using the stock fan and have it running out of an old dryer vent on the side of the house with about a 4-5 foot run.  I can't go to industrial as the kids bedrooms are right above it and I don't want to drive them crazy.  any ideas I have heard activated charcoal is a good option but dot know how to filer it through there 





**"3D Laser"**

---
---
**Vince Lee** *March 29, 2016 03:07*

IMHO Your best bet is probably an inline bilge fan mounted right at the dryer vent so that the entire run is under vacuum.  Hopefully you don't have any leaky or open windows nearby.  If you have an exhaust stack for a furnace or water heater that may be an option too but I'd be concerned about backflow.


---
**Josh Rhodes** *March 29, 2016 03:10*

Did you mean ballistic birch? Or baltic?


---
**Phillip Conroy** *March 29, 2016 08:24*

When i was in the australian royal air force  as an electroncis tech i used actived filtered charchole soldering filters which worked just for fhe small amount of solderind fumes no way will it work for a laser cutter  unles you have a huge  setup [http://www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK:MEBIDX:IT](http://www.ebay.com.au/itm/111240702828?_trksid=p2057872.m2749.l2649&ssPageName=STRK:MEBIDX:IT) this is the fan i use and it is 10x better than stock ,it needs to be at end of your pipe.i have tryed 6inch fans however the motor was only 60 watts this one is 250watts much stronger,i used 2 tin adaptors to reduce pipe size down to 4 inchhttp://[www.ebay.com.au/itm/191791173463?_trksid=p2057872.m2749.l2649&ssPageName=STRK:MEBIDX:IT](http://www.ebay.com.au/itm/191791173463?_trksid=p2057872.m2749.l2649&ssPageName=STRK:MEBIDX:IT)


---
**Phillip Conroy** *March 29, 2016 08:30*

Ps u need to get the products saftey sheet from the makers website as i cut 3mm mdf and the contents of the glue in mdf was an eye opener,ply would be similar as the 3 sheets are glued together,the place you by the ply would be my first step in getting this saftey sheet,it shows what is released when sanded,drilled,cut and burned------- BE SAFE do no just tacke risks as one of the things resleased when my mdf is burned is cinide ox something -whick can kill 


---
**Don Kleinschnitz Jr.** *March 29, 2016 14:26*

The stock fan is not powerful enough to remove the fumes, (I have tried lots of things to get it to work). Upgraded to 450 CFM, 4" and no fumes (I am in house) and it cuts much better.


---
**3D Laser** *March 29, 2016 17:03*

So I am thinking about getting a 4 inch vortex fan charcoal tub filter will that work if I mount it at the end of the line but the 4 inch is only 171cfm the 6 is 471cfm 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/au23pBfiebC) &mdash; content and formatting may not be reliable*
