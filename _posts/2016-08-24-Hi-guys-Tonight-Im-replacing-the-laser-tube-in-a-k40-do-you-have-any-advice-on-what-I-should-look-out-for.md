---
layout: post
title: "Hi guys! Tonight I'm replacing the laser tube in a k40, do you have any advice on what I should look out for?"
date: August 24, 2016 09:33
category: "Hardware and Laser settings"
author: "Anthony Santoro"
---
Hi guys! 

Tonight I'm replacing the laser tube in a k40, do you have any advice on what I should look out for? 

Does the lens need to be a particular distance away from the first mirror? 

![images/ba93ae96af9f4358dda09ba653bd22c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ba93ae96af9f4358dda09ba653bd22c5.jpeg)



**"Anthony Santoro"**

---
---
**Phillip Conroy** *August 24, 2016 10:00*

When i replaced my tube i allowed only 10mm ,now i have a p\laser power meter i wish i had allowed 40mm.

Do not solder the connections on the tube,just strip 20mm and rap around pin,then coner with elec tape ,than silacone[and lots of it] i put 3 layers of silacone on mine.

Heat water tubes in boiled water  and carefully push on to tube. Try and get tube level. Good luck


---
**Jim Hatch** *August 24, 2016 12:29*

Also get it parallel to the wall of the machine. That will give you a reference inside to make sure your X/Y rails are parallel @ square to the tube.


---
**Stuart Middleton** *August 24, 2016 21:25*

If you still had the original water tubes on, take this opportunity to replace them with some thick walled, 8mm hole tube. Far more robust and easy to get on. The original ones are near impossible to get back on.


---
**Anthony Santoro** *August 24, 2016 22:18*

Thanks for the tips! 


---
*Imported from [Google+](https://plus.google.com/106649411786258336023/posts/Fb4cvLmcCsK) &mdash; content and formatting may not be reliable*
