---
layout: post
title: "My laser wont stop moving in the Y direction when i turn it on"
date: January 24, 2017 02:38
category: "Original software and hardware issues"
author: "Avelino Jacinto"
---
My laser wont stop moving in the Y direction when i turn it on. It goes to the top left corner and wont stop moving. It makes a loud jamming noise. Does anyone know what going on?





**"Avelino Jacinto"**

---
---
**Alex Krause** *January 24, 2017 02:42*

Bad limit switch or optical sensor... Or the connection to the limit switch or optical sensor is bad


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 03:48*

**+Don Kleinschnitz** another candidate for a replacement endstop board? 


---
**Avelino Jacinto** *January 24, 2017 05:14*

Thank You!




---
**Don Kleinschnitz Jr.** *January 24, 2017 15:51*

**+Avelino Jacinto** if you want we can try something new:).

Troubleshooting from remote. 

I created a set of measurements that might tell us if your end stops are wired and workable. It consists of measuring resistance between wires at one end of the cable to see if we can detect a problem.

These are static measurements so if these are all good there still can be a problem. However if we find a bad reading its bad!



This is the first time for this test so it may take a couple of changes to get it right and I did not try this yet on my own machine.

[docs.google.com - Testing your K40 endstops](https://docs.google.com/forms/d/e/1FAIpQLSeJdLiM9SuLu3m7TbTnfGcV2HIlnTFI5mJP8THqdm3QYh88sQ/viewform)


---
*Imported from [Google+](https://plus.google.com/109269493521515792262/posts/BYrpGZowgcn) &mdash; content and formatting may not be reliable*
