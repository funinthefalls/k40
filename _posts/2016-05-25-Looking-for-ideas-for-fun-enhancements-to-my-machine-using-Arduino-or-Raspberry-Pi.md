---
layout: post
title: "Looking for ideas for fun enhancements to my machine using Arduino or Raspberry Pi"
date: May 25, 2016 17:21
category: "Discussion"
author: "Carl Fisher"
---
Looking for ideas for fun enhancements to my machine using Arduino or Raspberry Pi.



Ideally I'd love to have a display to give me status such as air and water pumps running, real time current readout of the laser or any other cool additions.



Curious what others have been able to accomplish and if they'd be willing to share pointers in the right direction for some cool upgrade projects.





**"Carl Fisher"**

---
---
**Scott Marshall** *May 25, 2016 19:00*

If you can wait a couple weeks, I've got a system coming that replaces your front panel insert with a graphical flowmeter, digital current and temp, and has warnings for water tank level, low water flow and high water discharge temp. If you silence the alarm and the condition gets worse (or is severe right off) the panel interuppts power to the tube, protecting it. There will be optional door interlocks, powersupply upgrade and lighting kit. Powersupply upgrade kit will be available within the a week, and includes 3 voltages all at more curent than you'll ever need. It takes all load but the tube off the factory supply, and has enough 12v ampacity to run a 12v fan and pump if you wish.  

Lighting kits are ready to go.



The entire kit will be plug in, and soldeerless. I also am going to be offering a Smoothieboard/Ramps plug in system that includes the power supply upgrade. 

My design is comparator and Cmos logic based as opposed to using programmable chips. It's not off the shelf stuff, it' s purpose designed for the K40 and it's sisters. It's jumper adjustable for different pump flows and you can set the units it displays (liter/gallon & deg C/Deg F)

Alarm levels are user settable with trimpots



This is all in final development, with prototype boards going together as parts roll in. I'll be looking for a few  Beta testers of all skill levels. If you are interested in participating, I'll give you a price break and work with you as you get it running. 

r

I will be releasing my designs if anyone wants to build their own, but it will probably be cheaper to buy mine, as I am buying all the parts in quantities.



I'll be putting up info here as the stuff comes on line.



Drop me an email if you're interested. Scott594@aol.com, I'll give you my number if you want to talk about it.



Scott


---
**Carl Fisher** *May 25, 2016 19:04*

Would love to see and follow that project once you announce it. Depending on the price for the package, it may be worth it but I may also look at a DIY option as I have quite a bit on hand for arduino, power supply, etc... and would probably piece together the rest. The tricky part is integration and the sketch




---
**Sebastian Szafran** *May 25, 2016 20:54*

**+Anthony Bolgar**​ pointed me to a Laser Safety System (I hope I didn't misspelled the name) some time ago. There is a YouTube movie showing it with details in the description. Thanks again Anthony, very useful stuff.


---
**Anthony Bolgar** *May 25, 2016 23:45*

I posted the laser safety system, which was designed and coded by **+Jon Bruno** , in a github repository. It can be accessed at [https://github.com/funinthefalls/LaserSafetySystem](https://github.com/funinthefalls/LaserSafetySystem)


---
**Carl Fisher** *May 26, 2016 01:59*

Email sent **+Scott Marshall**


---
**Anthony Santoro** *June 18, 2016 03:44*

**+Scott Marshall**​ I am pretty keen to learn more about your project also


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/hmaaedqqWX1) &mdash; content and formatting may not be reliable*
