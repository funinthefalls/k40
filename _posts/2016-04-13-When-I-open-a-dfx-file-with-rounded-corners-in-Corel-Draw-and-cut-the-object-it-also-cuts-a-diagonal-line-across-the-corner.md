---
layout: post
title: "When I open a .dfx file with rounded corners in Corel Draw and cut the object it also cuts a diagonal line across the corner"
date: April 13, 2016 07:12
category: "Discussion"
author: "Tony Sobczak"
---
When I open a .dfx  file with rounded corners in Corel Draw and cut the object it also cuts a diagonal line across the corner.  It does not show up on the drawing but does show up and cuts when cutting is selected in Corel Draw.  How do I get rid of the diagonal line? 





**"Tony Sobczak"**

---
---
**Phillip Conroy** *April 13, 2016 08:17*

have you selected the correct board in properties in the cutting window-check against circut board at front of laser cutter,can you stsrt a new thread and add photos of your settings


---
**Tony Schelts** *April 13, 2016 08:44*

I think and I'm not sure, but sometimes when I have drawn something in coreldraw, that I have created more than one layer and sometimes the layer underneath has a node that is joined at the wrong place.  Anyway just a thought.


---
**Scott Marshall** *April 13, 2016 11:38*

I've run across a few demo programs (file converters and such) that used a diagonal line thru the drawing to keep you from using it to make parts till you pony up.


---
**Tony Sobczak** *April 13, 2016 15:53*

Please see:



[https://plus.google.com/104479750532385612568/posts/5A8o44xE5eQ](https://plus.google.com/104479750532385612568/posts/5A8o44xE5eQ)


---
**Coherent** *April 13, 2016 19:29*

I've seen a number of cad drawings for parts that show both the options either a fillet (rounded corner or edge) or chamfered corner like the drawing shows. With manual milling machines it's much easier to chamfer than round a part. With lasers and 3d printers, that's not a concern obviously but may be offered as a design preference option which allows the end user to delete/use whichever they prefer. Options are sometimes placed on a different layer which depending on the software may or may not interpret. To delete it, load the file in autocad (or other software that depicts the lines), delete the unwanted lines and save the file using a new file name.


---
**Tony Sobczak** *April 14, 2016 19:19*

The lines do not show up until cutting, hence no way to delete.


---
**Phillip Conroy** *April 14, 2016 23:22*

Can you add original file download link and i wii have a look


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/jKuMxoMKBh5) &mdash; content and formatting may not be reliable*
