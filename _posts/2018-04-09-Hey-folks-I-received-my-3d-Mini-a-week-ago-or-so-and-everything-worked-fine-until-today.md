---
layout: post
title: "Hey folks. I received my 3d Mini a week ago or so and everything worked fine until today"
date: April 09, 2018 01:21
category: "Discussion"
author: "Craig Bybee"
---
Hey folks. I received my 3d Mini a week ago or so and everything worked fine until today. I noticed that I was loosing power and saw that when I increased the power on the laser to 99.9% is was a very unfocused beam as indicated by wide cut lines. Then all of a sudden the laser would not connect to Lightburn.



I found that the sd card was not working and it would not open on my computer. At this time all I had was a solid Red light on the board. I downloaded the firmware and config file from cohesion3d and put it on a new sd card. The board booted up but now the laser will not respond to movement from Lightburn and everytime I attempt to boot up the laser the computer says that I have to format the sd card (which I have twice). So board boots up with lights as they should be but no movement from the laser.





**"Craig Bybee"**

---
---
**Jim Fong** *April 09, 2018 03:45*

Stop and shutdown the laser.  Sounds like the laser power supply might be damaged or dying(your initial problem of loosing laser power). 



This power supply also runs the Cohesion3d board.  If the power supply is going out it very well could damage the Cohesion3d board too.  



Wait until Ray can answer this post.  


---
**Don Kleinschnitz Jr.** *April 09, 2018 12:28*

With a DVM check the 24v and 5V on the LPS.


---
**Craig Bybee** *April 09, 2018 16:00*

Appears that it is the power supply

 Thanks for the help you guys! 


---
**Don Kleinschnitz Jr.** *April 09, 2018 16:44*

**+Craig Bybee** did the voltages read incorrectly??


---
**Craig Bybee** *April 09, 2018 17:13*

Yes


---
**Craig Bybee** *April 09, 2018 17:43*

I think I messed up checking the voltages. They appear fine on 2nd try. I hooked up the original board and everything works. So, no movement on the 3d Mini board. 


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2018 17:45*

Do you get any green LEDs on the board now? 



New memory card, format FAT32, put files on, check again.  A separate 24v PSU will help tremendously as the 24 volts coming out of the stock psu is very unstable and noisy. 


---
**Craig Bybee** *April 09, 2018 17:48*

Also, the SD card that came with the mini is toast. Can't mount it at all. I ordered a 24v supply this am.


---
**Craig Bybee** *April 09, 2018 17:49*

All lights are correct. I put the files on a new SD and it works.


---
**Craig Bybee** *April 09, 2018 17:55*

The x and y inputs work on the original board but stopped working on the mini


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2018 17:56*

Yeah, the stock k40 setup is great for doing these sorts of things.  24v PSU and also the NOMSD firmware build cut down on this substantially.  (Jim Fong post in Cohesion3D group)


---
**Craig Bybee** *April 09, 2018 18:03*

Should I change to that config file? Sorry, firmware?


---
**Craig Bybee** *April 09, 2018 18:25*

Is there some way to get the board to move the laser head?


---
**Craig Bybee** *April 09, 2018 21:36*

**+Ray Kholodovsky** I have the mini booted up but still no movement of the x and y in lightburn. Has the board been fried?


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2018 21:42*

Need more info.  Make a post in the C3D group with pictures and details about the board behavior. 


---
*Imported from [Google+](https://plus.google.com/101927015571558418192/posts/gorxLiDshqS) &mdash; content and formatting may not be reliable*
