---
layout: post
title: "Might be useful for the K40 Users K40 eBay Chinese CO2 Laser - Electrical Warning This Can Kill You!"
date: June 20, 2016 16:54
category: "Original software and hardware issues"
author: "varun s"
---
Might be useful for the K40 Users



K40 eBay Chinese CO2 Laser - Electrical Warning This Can Kill You!





**"varun s"**

---
---
**Derek Schuetz** *June 20, 2016 17:46*

Quick question I know the unit has a ground screw terminal on it but being in the US my wiring is all 3 prong grounded. So do I need this at all? I keep reading different things


---
**Pigeon FX** *June 20, 2016 18:10*

**+Derek Schuetz** Even if you have the TYPE B USA plug (with the earth pin) You should double check that the earth connection on your K40 are actually connected to the corresponding lug on your IEC plug (where you plug the lasers mains cable in) as if the internal wiring  is not connected to the IEC, and just the Ground lug, you are still at risk, even using a 3 pin plug 



(if unsure, just post some photos and I sure people here will help you out) . 


---
**Derek Schuetz** *June 20, 2016 18:13*

I just finished doing a huge rewiring of it and it is connected to the laser power supply. I took off the ground screw because I don't see a point of it but now that I'm thinking of it if the machine shorts to the metal case can it still travel to the plug?


---
**Pigeon FX** *June 20, 2016 18:30*

**+Derek Schuetz** I would make sure there is a connection from the metal case going to the Earth lug on the IEC plug directly, just in case. 



Realistically the PSU SHOULD be making contact with the metal case of the laser and earthing it through your plug (you can double check this with a multi meter continuity test from a exposed bit of metal inside the case to the earth lug on the plug), but it's always best to have redundancy and a belt and braces approach when it comes to earthing equipment.   


---
**Derek Schuetz** *June 20, 2016 18:32*

**+Pigeon FX** ok sounds good didn't think about how the entire PSU is bolted to the case also but I may just put the lug back and run the earth to it also


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 20, 2016 18:43*

Thanks for sharing that. I was of the assumption that the ground on the power plug should be enough, but after watching that I see that it just might not be.


---
*Imported from [Google+](https://plus.google.com/116786516372429379291/posts/GtQEkid4ngj) &mdash; content and formatting may not be reliable*
