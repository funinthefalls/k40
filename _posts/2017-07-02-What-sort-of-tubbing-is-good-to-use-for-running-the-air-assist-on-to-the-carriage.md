---
layout: post
title: "What sort of tubbing is good to use for running the air assist on to the carriage"
date: July 02, 2017 19:48
category: "Modification"
author: "William Kearns"
---
What sort of tubbing is good to use for running the air assist on to the carriage





**"William Kearns"**

---
---
**Steve Clark** *July 02, 2017 19:53*

This is what I used, very flexible.

 

[mcmaster.com - McMaster-Carr](https://www.mcmaster.com/#5236k831/=18br2xo)


---
**Robert Selvey** *July 02, 2017 23:58*

I used clear vinyl tubing from lowes, you can buy it by the foot very cheap.

[lowes.com](http://lowes.com) - [https://www.lowes.com/search?searchTerm=clear+tubing](https://www.lowes.com/search?searchTerm=clear+tubing)

[lowes.com - Shop EASTMAN 3/8-in x 20-ft PVC Clear Vinyl Tubing at Lowes.com](https://www.lowes.com/pd/EASTMAN-3-8-in-x-20-ft-PVC-Clear-Vinyl-Tubing/1000180543)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/3noi3o3TGyH) &mdash; content and formatting may not be reliable*
