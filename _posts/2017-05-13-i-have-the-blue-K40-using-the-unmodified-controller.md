---
layout: post
title: "i have the blue K40 using the unmodified controller"
date: May 13, 2017 18:48
category: "Hardware and Laser settings"
author: "thebluearchernc"
---
i have the blue K40 using the unmodified controller. i have the unit cutting properly but now i am trying to etch and cut but the etching comes out about 1/10 the side it is suppose to. and i cant figure out why.



any ideas how to get the etching to the right size? 





**"thebluearchernc"**

---
---
**Ned Hill** *May 13, 2017 19:20*

Make sure under, coreldraw settings, that both "Engrave Data" and "Cut Data" are set to  WMF.


---
**Ned Hill** *May 13, 2017 19:21*

I'm assuming that you are using coreldraw but if it's laserdraw I think the same thing applies.


---
**thebluearchernc** *May 13, 2017 19:40*

thank you i would have never figured that out




---
**Ned Hill** *May 13, 2017 19:42*

Welcome. Let us know whether or not that fixes your issue.


---
**thebluearchernc** *May 13, 2017 19:45*

sorry engraving now. and yes it worked


---
*Imported from [Google+](https://plus.google.com/114811379718574106786/posts/GhgGbbu2SWj) &mdash; content and formatting may not be reliable*
