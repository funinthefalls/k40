---
layout: post
title: "Hello everyone, I am new because I received my laser 11/03"
date: March 13, 2016 10:49
category: "Hardware and Laser settings"
author: "Chris"
---
Hello everyone,

I am new because I received my laser 11/03. first electric arcs out at the red line laser sue so the insulation. Now still no laser beam ???

At the power supply reversal left 1 & 2? because no movement on the Ammeter. Test buttons, accessible faders, Ammeter OK

Thank you in advance...



![images/dc9b15fe39104c47c0439d0ff4054207.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc9b15fe39104c47c0439d0ff4054207.jpeg)
![images/c16edfa897bd6e3ca884db57eca9270c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c16edfa897bd6e3ca884db57eca9270c.jpeg)
![images/9bea54b639433e4767ae8f5439df15ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9bea54b639433e4767ae8f5439df15ae.jpeg)

**"Chris"**

---
---
**Jon Bruno** *March 15, 2016 20:40*

Laser tube is cracked somewhere if it's arcing to the chassis.


---
**Heath Young** *March 17, 2016 03:21*

Or the ground wire is disconnected or open circuit. It runs though the milliamp meter. I'd be following that green ground wire with a multimeter.


---
**Chris** *March 18, 2016 20:59*

Merci beaucoup, je suis d'accord avec vous. Soit problème de terre soit tube fissuré. j'ai refait tous les contacts électrique le fil vert est OK.


---
*Imported from [Google+](https://plus.google.com/109462644991158070594/posts/5yGYygDhjgq) &mdash; content and formatting may not be reliable*
