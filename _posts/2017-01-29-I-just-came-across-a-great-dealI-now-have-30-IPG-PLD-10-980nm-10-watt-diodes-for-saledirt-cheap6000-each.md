---
layout: post
title: "I just came across a great deal...I now have 30 IPG PLD-10 980nm 10 watt diodes for sale...dirt cheap...60.00 each"
date: January 29, 2017 16:21
category: "Discussion"
author: "Scott Thorne"
---
I just came across a great deal...I now have 30 IPG PLD-10 980nm 10 watt diodes for sale...dirt cheap...60.00 each.





**"Scott Thorne"**

---
---
**Anthony Bolgar** *January 29, 2017 18:26*

You know I want one :)


---
**Anthony Bolgar** *January 29, 2017 18:29*

How would you use these,I googled them and t appears they are fibre optic cable diodes, can they be used to engrave r cut?


---
**Ariel Yahni (UniKpty)** *January 29, 2017 18:43*

Ide like one if in knew the answer of sbove


---
**Jonathan Ransom** *January 29, 2017 20:55*

I'm interested in more info as well. I have a shapeoko 2 I've been wanting to add a diode to.


---
**Ned Hill** *January 29, 2017 21:55*

980nm is primarily used for clinical application.  This wavelength has lower energy absorption for common materials we typically laser compared to CO2 or UV diode lasers.  However, what it lacks in absorption it more than makes up for in raw power compared to typical UV diode based lasers.  So I would think that it would be good for engraving and perhaps some cutting.


---
**Anthony Bolgar** *January 29, 2017 21:58*

1064nm after fiber doping


---
**Ned Hill** *January 30, 2017 02:02*

Not sure the fibers are doped.  Here's the specification sheet on the PLD-10.  [ipgphotonics.com - www.ipgphotonics.com/640/Widget/PLD-10+Diode+Laser+Datasheet.pdf](http://www.ipgphotonics.com/640/Widget/PLD-10+Diode+Laser+Datasheet.pdf)


---
**Scott Thorne** *January 30, 2017 10:30*

No the fibers are doped beyond the diode...the diodes have 10 inch fiber leads on them


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/EMdkcvGqiLc) &mdash; content and formatting may not be reliable*
