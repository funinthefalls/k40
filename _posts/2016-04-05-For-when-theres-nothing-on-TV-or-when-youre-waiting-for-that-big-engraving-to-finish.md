---
layout: post
title: "For when there's nothing on TV, or when you're waiting for that big engraving to finish"
date: April 05, 2016 18:19
category: "External links&#x3a; Blog, forum, etc"
author: "Scott Marshall"
---
For when there's nothing on TV, or when you're waiting for that big engraving to finish.



I give you........



Laser Porn...

(In no particular order)



[http://lasertubevideo.com/efr-products_2](http://lasertubevideo.com/efr-products_2)




{% include youtubePlayer.html id="pMSyGOoesfM" %}
[https://www.youtube.com/watch?v=pMSyGOoesfM](https://www.youtube.com/watch?v=pMSyGOoesfM)



A nice little CNC gantry welder, and cutter:


{% include youtubePlayer.html id="ESxRPruVczk" %}
[https://www.youtube.com/watch?v=ESxRPruVczk](https://www.youtube.com/watch?v=ESxRPruVczk)




{% include youtubePlayer.html id="8u8wvwQhWv8" %}
[https://www.youtube.com/watch?v=8u8wvwQhWv8&ebc=ANyPxKo0XBLHxJoS6o94zuGfjf1HszRINqw9jo_l8UQadGNNYXezPnWabrvYr0hoE3ljLifnj83Mih1FMPFU5cvwOsykNuzm6w](https://www.youtube.com/watch?v=8u8wvwQhWv8&ebc=ANyPxKo0XBLHxJoS6o94zuGfjf1HszRINqw9jo_l8UQadGNNYXezPnWabrvYr0hoE3ljLifnj83Mih1FMPFU5cvwOsykNuzm6w)




{% include youtubePlayer.html id="c2RlM6FcOcU" %}
[https://www.youtube.com/watch?v=c2RlM6FcOcU](https://www.youtube.com/watch?v=c2RlM6FcOcU)




{% include youtubePlayer.html id="pMSyGOoesfM" %}
[https://www.youtube.com/watch?v=pMSyGOoesfM&spfreload=5](https://www.youtube.com/watch?v=pMSyGOoesfM&spfreload=5)




{% include youtubePlayer.html id="5Hp_DPEapzg" %}
[https://www.youtube.com/watch?v=5Hp_DPEapzg](https://www.youtube.com/watch?v=5Hp_DPEapzg)




{% include youtubePlayer.html id="bbBez-Ad5jg" %}
[https://www.youtube.com/watch?v=bbBez-Ad5jg](https://www.youtube.com/watch?v=bbBez-Ad5jg)



I did not even believe this at 1st:


{% include youtubePlayer.html id="CLaBFkeHG0A" %}
[https://www.youtube.com/watch?v=CLaBFkeHG0A](https://www.youtube.com/watch?v=CLaBFkeHG0A)



It's for real:


{% include youtubePlayer.html id="qRSO9u8VmpM" %}
[https://www.youtube.com/watch?v=qRSO9u8VmpM](https://www.youtube.com/watch?v=qRSO9u8VmpM)



Home shop sized:


{% include youtubePlayer.html id="HrrQZ8LKb50" %}
[https://www.youtube.com/watch?v=HrrQZ8LKb50](https://www.youtube.com/watch?v=HrrQZ8LKb50)




{% include youtubePlayer.html id="09QkTKLkPNE" %}
[https://www.youtube.com/watch?v=09QkTKLkPNE](https://www.youtube.com/watch?v=09QkTKLkPNE)



Lastly, a 500 Terawatt toy for ripping apart time and space....


{% include youtubePlayer.html id="09QkTKLkPNE" %}
[https://www.youtube.com/watch?v=09QkTKLkPNE](https://www.youtube.com/watch?v=09QkTKLkPNE).





**"Scott Marshall"**

---
---
**Richard Vowles** *April 06, 2016 06:06*

That laser paint/metal stripper is amazing!


---
**Scott Marshall** *April 06, 2016 10:12*

I thought it was photoshoped at 1st.


---
**Scott Thorne** *April 06, 2016 13:33*

**+Scott Marshall**...out of curiosity I emailed them about the price of the 60 watt tube...I received a response that the 60 watt tube was 120.00 dollars....I guess I'm not believing that is right...lol


---
**Scott Marshall** *April 06, 2016 14:40*

**+Scott Thorne** Who's that, the place in Nevada? 


---
**Scott Thorne** *April 06, 2016 15:03*

**+Scott Marshall**...Efr lasers...the cl series 60 watt.


---
**Scott Marshall** *April 06, 2016 15:20*

Oh, yeah, I didn't think they sold direct. The operation looks good in the video, smaller shops often make the best stuff.



Wonder if the shipping is $250?, thats the game a lot of them play on ebay.

If they're willing to deal, maybe we can work a group buy, I'm still in the market for a somewhat larger tube. Let me know if it looks like it's for real.



Thanks, Scott


---
**Scott Thorne** *April 06, 2016 15:28*

**+Scott Marshall**...I sent another email to amy...she is a sales rep for  them...I have inquired about shipping, packaging, and cost...if it's under 250 I'm going to give it a try...60 watt tube for 250 is a great price...I'll keep you updated Scott.


---
**Scott Thorne** *April 07, 2016 00:58*

**+Scott Marshall**...she just got back to me...the 60 watt tubes are 150.00 each but they will only sell in sets of 6...lol


---
**Scott Marshall** *April 07, 2016 07:56*

It wouldn't be hard to sell the extras if shipping isn't a deal breaker.

I just may look into it. There seems to be a demand.


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/9txaGEhVJCH) &mdash; content and formatting may not be reliable*
