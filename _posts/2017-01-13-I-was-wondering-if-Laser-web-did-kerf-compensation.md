---
layout: post
title: "I was wondering if Laser web did kerf compensation?"
date: January 13, 2017 13:52
category: "Discussion"
author: "Trampas Stern"
---
I was wondering if Laser web did kerf compensation? 



I was cutting some parts that required hole size to be exact for tapping and they came out much bigger than I wanted, which I assume is do the large kerf on my K40. 



It may be that I need to upgrade the head with a better lens, but I was wondering if laserweb had kerf compensation and if so how do you tell it which side of the line to put the kerf?







**"Trampas Stern"**

---
---
**greg greene** *January 13, 2017 14:09*

your Kerf should be close to  0.15 mm depending on your focus point


---
**Don Kleinschnitz Jr.** *January 13, 2017 14:59*

I don't think LW3 does but coming in LW4 (**+Peter van der Walt** ?).

I am using SketchUCam to get that function before I convert to .dxf and upload to LW3.


---
**Trampas Stern** *January 13, 2017 15:46*

Thanks Peter! 



I will try to use laserWeb4 to generate some gcode and test.


---
**greg greene** *January 13, 2017 16:57*

Works Great - nice job folks !!!


---
**greg greene** *January 13, 2017 17:13*

Done - Make it a Kooteney Gold !!!


---
**Todd Fleming** *January 13, 2017 17:45*

Pretty large; Blacksburg is a collage town.

[vintagecellar.com - Vintage Cellar](https://www.vintagecellar.com/)


---
*Imported from [Google+](https://plus.google.com/105967656691766312361/posts/YQfW2mZDPHw) &mdash; content and formatting may not be reliable*
