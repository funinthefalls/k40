---
layout: post
title: "Making a display gearbox...this is going to be so cool when I get the 6mm wooden dowels from Amazon....I have all the gears cut and ready to install"
date: December 07, 2015 23:38
category: "Object produced with laser"
author: "Scott Thorne"
---
Making a display gearbox...this is going to be so cool when I get the 6mm wooden dowels from Amazon....I have all the gears cut and ready to install.

![images/85396553c5fcdbef23de1d452e3e7469.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/85396553c5fcdbef23de1d452e3e7469.jpeg)



**"Scott Thorne"**

---
---
**Sean Cherven** *December 08, 2015 04:59*

Nice! Keep me posted on the outcome, I wanna see it!


---
**Tony Schelts** *December 08, 2015 09:42*

Yes please looks good. is that 6mm ply how many passes and what settings?


---
**Scott Thorne** *December 08, 2015 10:27*

No it is 1/8 ply.....I'm waiting on 6mm round dowel rod to mount the gears on.....thanks guys.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/bwcaZNMHgmJ) &mdash; content and formatting may not be reliable*
