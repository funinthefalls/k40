---
layout: post
title: "I have a software question for those using CorelDRAW"
date: July 08, 2016 01:21
category: "Software"
author: "Carl Fisher"
---
I have a software question for those using CorelDRAW. I just can't get the hang of the trim functionality. I'm trying to figure out how to trim the circles in the attached image so that it leaves only the shaded area and the outline.



Can anyone help point me in the right direction?



![images/54c2c32f61f26610cadc6700aaecbd5f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/54c2c32f61f26610cadc6700aaecbd5f.png)



**"Carl Fisher"**

---
---
**Jean-Baptiste Passant** *July 08, 2016 05:32*

Sorry but my CorelDraw is in French, option for soldering and triming etc... are in Object > Shape 



I am not sure if that's what you are looking for or if the name are correct :S


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 08, 2016 07:55*

Arrange > Shaping > Weld (to join components). Arrange > Shaping > Trim (to punch out the shapes you don't want). Make sure that what you want to trim is the top most layer first. (CorelDraw x5)


---
**Carl Fisher** *July 08, 2016 12:08*

After I do weld it takes away my option to trim.


---
**Carl Fisher** *July 08, 2016 12:12*

I got it! I think setting the layer orders made all the difference. When I went back to trim the outer circles (without welding) they trimmed up as I expected. Then I was able to weld the inner circles to join them and ultimately remove all of the shapes but the main.  



Thanks!


---
**Carl Fisher** *July 08, 2016 23:15*

I switched over to using LibreCAD instead and was able to get much more done that way. Then I just imported the DXF into Corel and cut away. Libre is much more in line with what I remember from CAD programs.


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/bTzGqZjAxy9) &mdash; content and formatting may not be reliable*
