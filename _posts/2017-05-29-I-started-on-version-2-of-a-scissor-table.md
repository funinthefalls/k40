---
layout: post
title: "I started on version 2 of a scissor table"
date: May 29, 2017 21:55
category: "Modification"
author: "Martin Dillon"
---
I started on version 2 of a scissor table.  I am waiting for glue to dry but should know how it works very soon.  Version one just had one scissor and was a little squishy.  I am hoping this one will work better.  If all goes well I think I will make the final one out of acrylic.  The one you see is 1/8th inch spruce as well as a picture of the old one.



![images/db31530cdd44ee1f6959ca825e1db54d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db31530cdd44ee1f6959ca825e1db54d.jpeg)
![images/9cd22c17c2a50e707311b175cbf9583a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9cd22c17c2a50e707311b175cbf9583a.jpeg)
![images/7aef6f7e3119c4dd0924b5ec863598bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7aef6f7e3119c4dd0924b5ec863598bd.jpeg)
![images/ebfa396e38cabf8a41d71d112f4904c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ebfa396e38cabf8a41d71d112f4904c9.jpeg)
![images/c2547d476d8e8937ad5d053013d9213d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c2547d476d8e8937ad5d053013d9213d.jpeg)

**"Martin Dillon"**

---
---
**HalfNormal** *May 30, 2017 14:23*

I like that you are using a double action scissor to enhance stability. My only complaint on the scissor is the lost room at the bottom of the mechanism.


---
**Martin Dillon** *May 31, 2017 01:49*

I thought the same thing.  When fully collapsed, it takes up 2 inches plus the thickness of the table.  If I remember correctly, it gives me about an inch of adjustment.


---
**Anthony Bolgar** *May 31, 2017 14:51*

I cut the bottom out of one of my K40's to allow for a deep work area, I framed the bottom of the machine with Steel Studs to close it it. It gave me an extra 3.5" of depth.


---
**HalfNormal** *May 31, 2017 14:53*

I am considering doing the same thing. 


---
**Moldes Vazados** *June 08, 2017 08:56*

great!


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/C2CpNrW2VFg) &mdash; content and formatting may not be reliable*
