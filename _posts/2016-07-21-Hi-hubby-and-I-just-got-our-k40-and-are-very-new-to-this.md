---
layout: post
title: "Hi, hubby and I just got our k40 and are very new to this"
date: July 21, 2016 01:32
category: "Discussion"
author: "Josephine Caruana"
---
Hi, hubby and I just got our k40 and are very new to this. Wondering if anyone has tried to cut and engrave using 3mm plywood. we have engraved on it but no luck cutting, seems to thick but that is the thinnest we have found in Bunnings did check out My plywood but the thinner stuff is way to expensive.





**"Josephine Caruana"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 21, 2016 01:34*

3mm plywood is what everyone cuts. 3mm birch ply (and balsa) are the stereotypes of this industry. I can cut 6mm MDF. 

What are your speed and laser power, and are you running the stock electronics? 


---
**Ariel Yahni (UniKpty)** *July 21, 2016 01:44*

**+Josephine Caruana**​ 3mm ply you should be able to cut easily at 10mA on the pot dial and 10mms speed. If not then you have bad alignment 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 21, 2016 01:47*

Yeah, as Ray said, 3mm ply is doable. There are a few factors to consider in being able to cut through it: 1) mirror alignment, 2) lens orientation, 3) focal point.



1) the mirror alignment is not hugely complicated, but it does take some time to figure it out. There are a few posts here with links to alignment guides, so do a search of the group & you will find those links. Basically you want to try get the beam hitting the same spot for all four corners (Top Left/Right, Bottom Left/Right).

2) lens orientation is odd as you would assume the lens comes the correct way up, but in mine it was upside down (& a few others). It should be with the convex (or bump) side facing up.

3) Focal point is 50.8mm for the stock lens, so you want to make sure that from the bottom of the lens to the centre of the material is 50.8mm (or 2 inches). The reason for aiming for the centre of the material is that the beam focuses to a fine point & then defocuses once it reaches the focal point. So overall the side profile shape will be like an X, with the centre of the X being the optimal cutting point (aimed at centre of material thickness). You can do what is called a ramp test to determine where your optimal point is. This is achieved by placing a material in the cutting area on an angle & cutting a line. You will see the line cut along the material vary in thickness. The point with the thinnest line is the optimal focal point for your machine. I'd suggest some of the material above 50.8mm & some below it, so you can gauge if it's correct.



Additionally to all of that, you will find that having air-assist (i.e. air blowing at the point of the cut) improves the ability to cut & also minimises charring, flare-ups (fire), and sooty mess around the cut.


---
**Josephine Caruana** *July 21, 2016 01:53*

Thank you so very much everyone, will have to try this on the weekend and will let you know how we went. So glad I found you guys.


---
**Paul de Groot** *July 21, 2016 02:32*

Hi I had the same issue with plywood from bunnings. It's marine plywood and gets charred after one cycle. After that it doesn't cut anymore.  I converted my k40 to an arduino uno with a cnc shield to get access to more software. I noticed that my k40 cuts way better. The pwm driving was too low from the nano board. Maybe just my nano controller but very happy the change. Costed me just $30.


---
**Peter Lewkowicz** *July 22, 2016 04:07*

Hey Paul what guide did you follow to convert it to Arduino uno? Also what software and firmware are you using?


---
**Paul de Groot** *July 22, 2016 05:24*

I used [instructables.com](http://instructables.com) to get the wiring diagrams.  Nobody was clear in actual instructions so had to figure it out for myself.  Happy to help just msg me. You can buy an arduino uno and a cnc shield from ebay. A stepper stick is abt $2.50 u can use a polulu A4988 or a drv8825. The latter can drive more current and do finer steps. The instructables link to get you started. [Http://www.instructables.com/id/Goodbye-Moshi-or-How-Run-Your-Laser-Printer-on-Ard/](Http://www.instructables.com/id/Goodbye-Moshi-or-How-Run-Your-Laser-Printer-on-Ard/)

The schematic gave me the info to wire my cnc shield see [http://blog.protoneer.co.nz/arduino-cnc-shield/](http://blog.protoneer.co.nz/arduino-cnc-shield/)


---
**Robi Akerley-McKee** *August 08, 2016 06:27*

I tried that route and the board would work for longer than 20 minutes before dying. I switched to the arduino Mega 2560 and RAMPS 1.4 (with mods to run 24v) added air assist and using inkscape turnkey laser extension, pronterface for uploading g code. I've been going great guns with my 2 K40's




---
*Imported from [Google+](https://plus.google.com/+JosephineCaruanafotos-cape/posts/CxzQ9Yr69U7) &mdash; content and formatting may not be reliable*
