---
layout: post
title: "Id like to be able to change the horribly twitchy potentiometer on my control panel but not quite sure which values I'm looking at replacing it with"
date: October 14, 2015 10:41
category: "Hardware and Laser settings"
author: "Peter"
---
Id like to be able to change the horribly twitchy potentiometer on my control panel but not quite sure which values I'm looking at replacing it with.

The existing one has the following markings... "WTH118-1A 2W 1K".

Does the 1A denote the load it is rated up to?

2W?

1K, the resistance value.



What values should I be looking for in the new pot, higher or lower resistance value, and will changing the pot have any impact on the ammeter readings? 



Video shows me trying to keep the power at <=10mA. Pot barely moving and reading jumps all over the place; sometimes power increased when turning down.

Id like to be able to turn a noticeable amount but small movements on the ammeter.



Thanks





**"Peter"**

---
---
**Peter** *October 14, 2015 11:15*

I checked the contacts on the ammeter (and everything else) before writing. I had my finger on the test fire button the whole time, not cutting or engraving.



Ive found some multi-turn pots that might be better. Same values so not likely to mess with the readings.



[http://cpc.farnell.com/vishay-spectrol/534b1102jlb/potentiometer-2w-1k/dp/RE04207?ost=2w+1a+1k&categoryId=700000011539](http://cpc.farnell.com/vishay-spectrol/534b1102jlb/potentiometer-2w-1k/dp/RE04207?ost=2w+1a+1k&categoryId=700000011539)


---
**Peter** *October 14, 2015 11:28*

Already ordered it from CPC. See how it works in a few days I guess. :)

[http://www.farnell.com/datasheets/885200.pdf](http://www.farnell.com/datasheets/885200.pdf)


---
**Joey Fitzpatrick** *October 14, 2015 13:01*

The trimpot output(to the Power supply) is a variable voltage between 0-5v.  As long as the replacement trimpot can output within this range, it should work fine


---
**Gregory J Smith** *October 14, 2015 14:39*

The one you've  ordered from farnell is ok. You're  after a 1kohm (1000ohm) 2Watt pot. Wirewound are generally more reliable than the carbon film variety - less likely to suffer from poor contact. I like the 10 turn approach. Make sure you get a multiturn knob. 


---
**Coherent** *October 14, 2015 18:50*

Let us know if that fixes your problem or it was something else. I'm not a fan of the analog meter on these and was thinking of replacing mine with a small led meter they have at light object. If anyone has already done so, your opinion would be appreciated.



[http://www.lightobject.com/3-digit-Mini-Blue-LED-DC-100mA-meter-Ideal-for-CO2-Laser-power-indicator1](http://www.lightobject.com/3-digit-Mini-Blue-LED-DC-100mA-meter-Ideal-for-CO2-Laser-power-indicator1)


---
**Phillip Conroy** *October 15, 2015 11:55*

My guss is that it is something else as pots are normaly very relable,sounds mkre loke arking somewhere or power supply faulty,could even be a tube faulty,check all conections and temp of water going into the tube


---
**Peter** *October 17, 2015 13:15*

Well, The pot arrived today and ive swapped it out but now I have a different issue. When the pot or right down/off, the slightest turn will "reset" something and the head goes home and settles. a test fire seems to show the ammeter at 20mA wherever the pot is positioned. 


---
**Peter** *October 17, 2015 13:33*

I just took the old potentiometer apart to see if there was anything to clean up and it looks like I found the problem. 

[https://drive.google.com/file/d/1JJeBa5Q4_8FJ5VloqNVIFbwc4l6O26b2yA/view?usp=sharing](https://drive.google.com/file/d/1JJeBa5Q4_8FJ5VloqNVIFbwc4l6O26b2yA/view?usp=sharing)

Now i'm worried about what caused it and with the "reset" issue, is this having the same impact on the new pot?


---
**Gregory J Smith** *October 18, 2015 10:07*

Peter, the reset issue could be caused by incorrect pot wiring. If the wiper is on the 5v supply then a small turn could drag the 5v down to ground - causing a reset. Of course I can't be sure without being there, but you should triple checking the wiring to the power supply. If ok, maybe the power supply is suspect??


---
**Peter** *October 18, 2015 10:47*

Unfortunately I cant find a pinout for the power supply I have. The closest one is this - [http://www.lordtuber.com/IMG_6758.JPG](http://www.lordtuber.com/IMG_6758.JPG)



Mine looks like this - [https://drive.google.com/file/d/0B2AcrOBe3Oo1SUFkRVFnWF95NDA/view?usp=sharing](https://drive.google.com/file/d/0B2AcrOBe3Oo1SUFkRVFnWF95NDA/view?usp=sharing)



Mine - [https://drive.google.com/file/d/1RkeVNdXScE6Ec0qrwGZGqbrqy13Qx0EV5w/view?usp=sharing](https://drive.google.com/file/d/1RkeVNdXScE6Ec0qrwGZGqbrqy13Qx0EV5w/view?usp=sharing)


---
**Phillip Conroy** *October 18, 2015 11:48*

New power supply is $130 on [ebay.com.au](http://ebay.com.au),    


---
**Phillip Conroy** *October 18, 2015 11:51*

Where do u live as when i replace my boroken power supplys hi voltage coil i will have a fully working spare power supply


---
**Peter** *October 18, 2015 12:27*

Thanks Phillip,

Im in the UK, and hoping the the exchange rate works in my favour if its $130 oz dollars.



Ive taken readings from all three connection blocks 4 pin, 6 pin, and 4 pin and they all seem to have sensible values. The bit that messes it up is the variable from the pot. There isnt a combination ive tried that produces a controllable ammeter reading. Either all off or 10 or 20mA.



5V in, through the pot to ground. The middle pin reads from 0 to 105mV but the reading on the ammeter is at either 0 or 20mA


---
**Kirk Yarina** *October 18, 2015 12:52*

From what Stephane said in a different post the pot should have 5v across it.  The wiper will then give you 0 to 5v as it's turned.  Should be easy to test without connecting the wiper at the supply.  Like others have said, yours had a couple wires swapped.



On my K40 the pot connections at the supply have wire ferrules on them and are easy to swap, and all 3 wires are different colors, so you can keep the factory color code.  Look at Stephane's pre-smoothie pics and see if they show both ends of the pot connection.  I'd take a pic of mine, but it's 1500 miles away.


---
*Imported from [Google+](https://plus.google.com/+PeterJones79/posts/4ssqgiHvTQ9) &mdash; content and formatting may not be reliable*
