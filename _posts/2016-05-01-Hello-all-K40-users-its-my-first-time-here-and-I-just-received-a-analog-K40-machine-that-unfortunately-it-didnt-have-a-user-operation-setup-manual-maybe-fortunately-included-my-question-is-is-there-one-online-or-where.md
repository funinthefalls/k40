---
layout: post
title: "Hello all K40 users, it's my first time here and I just received a analog K40 machine that unfortunately it didn't have a user/operation/setup manual (maybe fortunately) included, my question is: is there one online or where"
date: May 01, 2016 22:28
category: "Hardware and Laser settings"
author: "Blake Ingram"
---
Hello all K40 users,



it's my first time here and I just received a analog K40 machine that unfortunately it didn't have a user/operation/setup manual (maybe fortunately) included, my question is: is there one online or where do I start (first) for setup and testing? Thanks in advance for any guidance. Cheers!





**"Blake Ingram"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 22:53*

There is definitely not one floating around. The best place to start for setup is here in this group (in my opinion) as we have all been there & done that. There are a few posts that Stephane has stickied to the top of the main Discussion page.



There is not a great deal involved with setup however. In regards to hardware wise, basically make sure machine is empty of the styrofoam & whatever pieces they put inside it during transit (check tube bay, cutting bay, electronics bay). Then after that, mirror alignment (there is a link to the best way to do that on the stickies). After that, check your lens orientation. Should be concave facing down.



Software wise, you want to probably avoid the crap they gave on the CD, as it is all confusing & a lot of it is useless junk. I've posted a link recently for the software (using CorelDraw x5 instead of 12 combined with CorelLASER). The software CD they give comes with a pirated version of CorelDraw12. You use CorelLASER plugin that comes with it to actually "talk" to the laser cutter. Basically, I recommend not bothering with CorelDraw12. I've installed CorelDraw x5 just the other day (also non-original) & CorelLASER plugin works great with it.



You can find the software & information regarding it here: [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XcCXxJgh3hT](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XcCXxJgh3hT)


---
**Jim Hatch** *May 01, 2016 22:55*

Try these:

[https://richardgrisafi.com/2014/10/10/setup-and-maintenance-of-a-40w-laser-cutter-from-ebay/](https://richardgrisafi.com/2014/10/10/setup-and-maintenance-of-a-40w-laser-cutter-from-ebay/)




{% include youtubePlayer.html id="hWIiMSChApU" %}
[https://www.youtube.com/watch?v=hWIiMSChApU](https://www.youtube.com/watch?v=hWIiMSChApU)




{% include youtubePlayer.html id="KhhOHUe_b6o" %}
[https://www.youtube.com/watch?v=KhhOHUe_b6o](https://www.youtube.com/watch?v=KhhOHUe_b6o)



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



[http://www.instructables.com/id/How-to-Cut-and-Engrave-Using-a-K40-Laser-Cutter/](http://www.instructables.com/id/How-to-Cut-and-Engrave-Using-a-K40-Laser-Cutter/)


---
**Blake Ingram** *May 01, 2016 23:13*

**+Jim Hatch**

Thanks again, you're awesome!


---
**Blake Ingram** *May 01, 2016 23:14*

**+Yuusuf Sallahuddin**



Thanks very much for your insight, much appreciated!


---
*Imported from [Google+](https://plus.google.com/101957775596878882298/posts/ebKpdb2Xc3q) &mdash; content and formatting may not be reliable*
