---
layout: post
title: "Pretty nice DIY cutter design:"
date: April 18, 2017 13:49
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Pretty nice DIY cutter design:

[http://www.instructables.com/id/Make-Your-Own-High-Quality-CO2-Lasercutter-With-To/?utm_source=newsletter&utm_medium=email](http://www.instructables.com/id/Make-Your-Own-High-Quality-CO2-Lasercutter-With-To/?utm_source=newsletter&utm_medium=email)





**"Don Kleinschnitz Jr."**

---
---
**Adrian Godwin** *April 18, 2017 22:28*

Interesting that he spent E1900 on it. A similar price to the VanillaBox that was referenced here recently.



It's a lot different cost to a K40, but I wonder how much time and money we spend improving them ? For me, that's part of the fun. But what's a realistic price for an entry-level machine ? Is $300 all people will pay ?




---
**Paul de Groot** *April 18, 2017 23:26*

Apparently the makers are only 17yr old according to the comments on the instructables. Also a reference to the software [lasergrbl.com - LaserGRBL – Free Laser Engraving](http://lasergrbl.com) i was impressed with the engraving being finalised with a contour line😊


---
**Anthony Bolgar** *April 19, 2017 18:11*

LaserGRBL seems to be a nice simple program. Looks very easy to use for simple projects.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/bqHbbdUYb6V) &mdash; content and formatting may not be reliable*
