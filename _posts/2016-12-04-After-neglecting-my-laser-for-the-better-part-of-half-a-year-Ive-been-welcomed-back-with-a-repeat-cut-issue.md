---
layout: post
title: "After neglecting my laser for the better part of half a year, I've been welcomed back with a repeat cut issue"
date: December 04, 2016 11:03
category: "Hardware and Laser settings"
author: "Linda Barbakos"
---
After neglecting my laser for the better part of half a year, I've been welcomed back with a repeat cut issue. My laser is repeating the cut, even though I have have not specified a repeat. From a previous post [https://plus.google.com/111366654976766633425/posts/dLjxuWbVMTq](https://plus.google.com/111366654976766633425/posts/dLjxuWbVMTq) I think it may be due to my drawing not having a "solid infill" therefore the laser is cutting on either side of my line.. I'm confused as to if this is a CorelDRAW setting or is my .dxf file that is the problem?





**"Linda Barbakos"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 04, 2016 12:23*

Try 0.01mm line width. Usually fixes the issue. Also, I've seen when some people have the Cutting Data set to a funny option (in the CorelLaser plugin settings) that it ends up causing some odd things (double line being one of them).



See: [drive.google.com - Check CorelDraw Settings.png - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)


---
**Linda Barbakos** *December 04, 2016 13:33*

**+Yuusuf Sallahuddin** My settings we nothing like the sample but updating them didn't solve the issue. Changing the line to 0.01mm did the trick though. Thanks a lot!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 04, 2016 17:00*

**+Linda Barbakos** You're welcome Linda. Glad to have helped.


---
*Imported from [Google+](https://plus.google.com/107262725887494713165/posts/Hhu5ksStRQe) &mdash; content and formatting may not be reliable*
