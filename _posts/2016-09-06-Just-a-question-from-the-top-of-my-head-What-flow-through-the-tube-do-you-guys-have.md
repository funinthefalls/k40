---
layout: post
title: "Just a question from the top of my head: What flow through the tube do you guys have?"
date: September 06, 2016 14:54
category: "Discussion"
author: "Mircea Russu"
---
Just a question from the top of my head:

What flow through the tube do you guys have?



The simplest way to measure is to use a stopwatch and a 1-2Liter/0.5-1Gallon bottle.

Put the return tube to the known size recipient, start the pump and the stopwatch. stop when the recipient is full.

Just paste the time and the size of the recipient.







**"Mircea Russu"**

---


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/3CXm9VWYXjA) &mdash; content and formatting may not be reliable*
