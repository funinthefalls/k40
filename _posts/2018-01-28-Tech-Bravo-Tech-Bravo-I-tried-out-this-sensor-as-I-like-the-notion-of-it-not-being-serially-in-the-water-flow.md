---
layout: post
title: "+Tech Bravo (Tech Bravo) I tried out this sensor as I like the notion of it not being serially in the water flow"
date: January 28, 2018 13:18
category: "Modification"
author: "Don Kleinschnitz Jr."
---
+Tech Bravo (Tech Bravo)



I tried out this sensor as I like the notion of it not being serially in the water flow.

[https://www.amazon.com/Lightobject-LSR-H2OPRSNR-Pressure-Sensor-Protection/dp/B00HUC5FC4](https://www.amazon.com/Lightobject-LSR-H2OPRSNR-Pressure-Sensor-Protection/dp/B00HUC5FC4)



I could not get it to activate unless I closed off the output.

I put the switch on on the side leg of the T with the input on the top of the T and the output on the bottom. 

[https://photos.app.goo.gl/lRedUp01WIPhe4kk1](https://photos.app.goo.gl/lRedUp01WIPhe4kk1)



.......

I have a 200gph (12.6 lpm) (non stock) pump that works with my other switch:



[https://www.amazon.com/White-Plastic-Magnetic-Switch-Thread/dp/B00AKVEGTU/ref=pd_cp_328_4?_encoding=UTF8&pd_rd_i=B00AKVEGTU&pd_rd_r=6WG7KHPFMQBARVVS48QZ&pd_rd_w=UcfAh&pd_rd_wg=3EJQg&psc=1&refRID=6WG7KHPFMQBARVVS48QZ](https://www.amazon.com/White-Plastic-Magnetic-Switch-Thread/dp/B00AKVEGTU/ref=pd_cp_328_4?_encoding=UTF8&pd_rd_i=B00AKVEGTU&pd_rd_r=6WG7KHPFMQBARVVS48QZ&pd_rd_w=UcfAh&pd_rd_wg=3EJQg&psc=1&refRID=6WG7KHPFMQBARVVS48QZ)



.....

Has anyone gotten this to work, if so where and how do you have it plumbed in the cooling loop?



When I look at my setup I cannot comprehend how it could work since there really is no pressure building in the side leg of the T against the switch diaphragm. In fact it looks like it would pull a vacuum on that leg as the water flows down through the T???.





**"Don Kleinschnitz Jr."**

---
---
**David Davidson** *January 28, 2018 15:46*

I have mine on a T on the output side of the laser tube. It works if I pinch the tube going to the pump. In hindsight, I think it needs to be on the pressure side where it enters the laser tube. There's no doubt a very big drop in pressure as the water expands in side the laser tube.


---
**Don Kleinschnitz Jr.** *January 28, 2018 16:59*

**+David Davidson** the problem with putting them on the input side is that if the hose breaks between the sensor and the laser it might not sense it. On the other hand since it is a diaphragm the pressure will drop and maybe is will sense it. Have to give it a try.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/artaZbW5Gfu) &mdash; content and formatting may not be reliable*
