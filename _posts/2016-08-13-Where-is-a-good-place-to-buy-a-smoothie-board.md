---
layout: post
title: "Where is a good place to buy a smoothie board ?"
date: August 13, 2016 00:14
category: "Discussion"
author: "Robert Selvey"
---
Where is a good place to buy a smoothie board ?





**"Robert Selvey"**

---
---
**Alex Krause** *August 13, 2016 00:34*

Robotseed if you are in the UK and uberclock if you are in the USA


---
**Brandon Satterfield** *August 13, 2016 00:42*

There is another new place in the US as well... :-)


---
**Alex Krause** *August 13, 2016 00:42*

Does [www.Smw3d.com](http://www.Smw3d.com) sell smoothies?


---
**Alex Krause** *August 13, 2016 00:43*

[http://www.smw3d.com/smoothieboard/](http://www.smw3d.com/smoothieboard/)


---
**Alex Krause** *August 13, 2016 00:44*

Thanks alot for not giving me the heads up **+Brandon Satterfield**​... now let's see if we can get this item sold out ;)


---
**Brandon Satterfield** *August 13, 2016 01:20*

You rock brother!!! 


---
**Brandon Satterfield** *August 13, 2016 01:21*

Hope that between different locations we can keep the community full of smoothie! 


---
**Stephen Sedgwick** *August 16, 2016 12:59*

There will be a plug and play board available soon also for the K40 thanks to Ray Kholodovsky and company...


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/iYP1MGquxo1) &mdash; content and formatting may not be reliable*
