---
layout: post
title: "Help troubleshoot LaserTube/Laser PSU issue: Community, my laser sparked (saw it arcing) at the junction point where I connected a new PSU anode wire to the old anode wire I had hooked up to the tube"
date: July 07, 2015 02:56
category: "Discussion"
author: "Fadi Kahhaleh"
---
Help troubleshoot LaserTube/Laser PSU issue:



Community, my laser sparked (saw it arcing) at the junction point where I connected a new PSU anode wire to the old anode wire I had hooked up to the tube. (although there is some insulation, I think it wasn't enough and the cable got too close to the chassis)



I am no longer seeing any laser beam. After opening the PSU (a Jupiter Co. model BH-40W) I saw no proof of any shorts, fried components or the like, and the fuse is intact.



Closer inspection to the large MOSFET transistors (2SC2625) showed that they are working properly.



Does anyone have any troubleshooting ideas that would help me go through this logically and decide if it is a PSU or a Tube issue?



Thanks





**"Fadi Kahhaleh"**

---
---
**Jon Bruno** *July 09, 2015 16:58*

What methods are you using to test the power transistors. I'm just asking because Transistors and Mosfets are not the same animal. and the 2SC2625 is a Bipolar Junction Transistor, and not a MOSFET


---
**Jon Bruno** *July 09, 2015 17:01*

Since BJT's are current driven and not voltage driven like a MOSFET, a short to the case could possibly be  enough to cause them to fail... I believe the collector on these can handle a max of 10A but I'm not certain you'd max that out with a short on the secondary of the LPSU HV transformer. But some back EMP could have played a role here too.


---
**Fadi Kahhaleh** *July 09, 2015 23:51*

You are right **+Jon Bruno**.

I desoldered the two transistors, tested them individually for proper operation, measuring voltage readings at the Collector and Base with and without load. All was within spec.



Forgot which resistor value I used, and what was the amperage going through the load, but it was OK I think.



Never the less, I went ahead and ordered two new transistors to replace them if needed.



Once I get the new transistors, and some high voltage resistors I ordered, I want to do a test on the HV line of the LPSU by using those resistors as load and not the Laser. (i.e a better lab environment for troubleshooting that is situated on my workbench :)   )





In the meantime, laser is working again on an older LPSU that doesn't support PWM. 



Thanks


---
*Imported from [Google+](https://plus.google.com/115068648817066933731/posts/YLcgBUYsKUV) &mdash; content and formatting may not be reliable*
