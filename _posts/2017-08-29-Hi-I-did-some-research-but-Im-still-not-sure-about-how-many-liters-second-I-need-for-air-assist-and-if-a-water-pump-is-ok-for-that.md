---
layout: post
title: "Hi, I did some research but I'm still not sure about how many liters/second I need for air assist, and if a water pump is ok for that"
date: August 29, 2017 03:00
category: "Air Assist"
author: "Daniele Salvagni"
---
Hi, I did some research but I'm still not sure about how many liters/second I need for air assist, and if a water pump is ok for that.

Would this pump work for air assist? [https://www.amazon.it/SPEED-Laghetto-risparmio-energetico-ruscello/dp/B01CTMOKH0/](https://www.amazon.it/SPEED-Laghetto-risparmio-energetico-ruscello/dp/B01CTMOKH0/)



It is meant for water and has the following models:

3000L/H 10W, 3600l/h 20W, 4500l/h 30W, 5200l/h 40W, 7000l/h 50W, 8000l/h 70W, 10000l/h 80W, 12000l/h 100W



Which one would be enough?



What about water cooling? Would 6 liters/second be enough?





**"Daniele Salvagni"**

---
---
**Ned Hill** *August 29, 2017 05:26*

I would not recommend using a water pump for an air assist.  Not sure if it would even put out air (that one seems to be an impeller drive, so no air), but would defiently overheat.  Using a large aquarium/pond air pump is what a lot of people use.  Some also use a standard air compressor or even airbrush compressors.  Typically you want at least 10 L/min airflow, but also depends on what type of air assist nozzle you are using. 

For water cooling you want at least 40L/min as a minimum. 


---
**Martin Dillon** *August 29, 2017 05:27*

I would not use that type of pump for air.  It is designed to pump water and would not be efficient at pumping air.  They type of pump you want is an aquarium pump they use to aerate the water.  Something like this one   [amazon.it - ACO 208 Pompa d'aria compressore alternativo 2.100 l / h: Amazon.it: Giardino e giardinaggio](https://www.amazon.it/Pompa-daria-compressore-alternativo-2-100/dp/B00BNSQ0YI/ref=sr_1_11?s=garden&ie=UTF8&qid=1503984337&sr=1-11&keywords=aquarium+pump)


---
**Phillip Conroy** *August 29, 2017 07:18*

The ratings of these pumps are 4 nothing hooked up to water outlet, by hooking up 6mm hose cuts flow by 90%, I used a 150 watt pump that pumps 2 liters per minute when connected to laser tubes,


---
**Daniele Salvagni** *August 29, 2017 12:34*

Thanks, I will buy the ACO that everyone is reccomending then, I hope it will not be too noisy. How many Liters/Hour do I need for air assist?


---
*Imported from [Google+](https://plus.google.com/117822352975366881011/posts/PKzaqQsuRdZ) &mdash; content and formatting may not be reliable*
