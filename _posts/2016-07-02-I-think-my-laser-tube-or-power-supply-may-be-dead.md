---
layout: post
title: "I think my laser tube or power supply may be dead"
date: July 02, 2016 19:42
category: "Original software and hardware issues"
author: "Evan Fosmark"
---
I think my laser tube or power supply may be dead.



I was engraving last night (24% power) and start hearing a terrible buzzing/crackling sound coming from the machine (not sure which area). I turned off the machine, checked some connections, and tried again. This time the buzzing was much louder and after stopping the machine I was unable to get it to fire. I checked my tube for cracks (only the top, I haven't taken it all the way out), and couldn't find any. The rest of the machine (gantry/lights/power-meter) seems to run fine. The power supply fan is running. The machine has only ever been operated with water flowing.



This is the power supply in my unit: [http://www.ebay.com/itm/222096396639](http://www.ebay.com/itm/222096396639) The power LED is always lit up, and I don't see anything change when I hit the "test" button (though, mine says "text" instead of "test", haha). Unfortunately I can't find any documentation on this unit.



I'm guessing that the part of the power supply that drives the laser is dead. Has anyone had something similar happen? I read through many of the "power supply" posts on this group, but didn't find anything conclusive. 



My larger concern is that the tube may have a crack I can't see, and then as the CO2 leaked out the power supply got burned out.





**"Evan Fosmark"**

---
---
**Julia Longtin** *July 02, 2016 20:08*

you don't want to test the tube for cracks; you want to see if there is a 'fog' where there should not be.


---
**Evan Fosmark** *July 02, 2016 20:20*

I'm mot sure what that is supposed to look like, but I don't see any fog. Is there a guide somewhere on identifying that? From what I an tell, it looks exactly like it did when I got it.


---
**Derek Schuetz** *July 02, 2016 20:59*

It's a dead tube same issue I had if your hearing arcing then your power supply is producing power 


---
**Evan Fosmark** *July 02, 2016 23:45*

**+Derek Schuetz** I'm not hearing anything from either the tube or the power supply ever since it stopped working.


---
**Evan Fosmark** *July 03, 2016 21:34*

Well I ordered a new power supply. Should be here in a week or so. I opened up the supply, and didn't see any scorching on the PCB, and no cracks on the flyback transformer, but the tube looks pristine, so I'll start with the less-expensive swap-out.



In the meantime, I guess this gives me an opportunity to finish some of the other upgrades I've been putting off.


---
**Derek Schuetz** *July 04, 2016 01:35*

I'm telling you it's the tube you can't necessarily see cracks also if you been using the power at max you probably burned up your gas in the tube quickly


---
**Evan Fosmark** *July 04, 2016 01:39*

**+Derek Schuetz** I don't doubt it may be the tube, but a number of people with similar issues said that their transformer went bad in their power supply. Figured I'd start with the less expensive item. I've never driven the tube over ~35% power (my machine doesn't show mA, only % power), and never ran it without adequate cooling. Even so, I'd be surprised if I burned up the gas since I've only had the machine for less than three weeks and probably only about 15 to 20 hours of time on the tube.



Hoping it's not the tube, as that'd be another $160 or so to get 'er running again.


---
**Evan Fosmark** *July 04, 2016 23:25*

I went through and double-checked the connections again today. Now when I press the test switch I hear a faint hum from the power supply, and see the wattage jump up on my Kill-A-Watt. Still no output, and there's an odd odor to it. Can't tell if it's burnt electronics or maybe ozone.


---
**Rodney Huckstadt** *October 19, 2016 11:33*

My power supply has died as well I think, I set it to 15ma and it only puts out about 6,


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/V41eTaGKFmq) &mdash; content and formatting may not be reliable*
