---
layout: post
title: "Waiting on my K40 from amazon to arrive"
date: June 28, 2017 05:36
category: "Modification"
author: "William Kearns"
---
Waiting on my K40 from amazon to arrive. Would like to upgrade the controller. I am not super versed in programming or the likes but pretty handy. I would like to know if there is a drop in conversion for this and if so what programs do I need to make it run once I swap out the board. This is the model I purchased off Amazon is the Orion Motor Tech. 

![images/e9b0e743daa1d4a4e534a4451dc23659.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9b0e743daa1d4a4e534a4451dc23659.jpeg)



**"William Kearns"**

---
---
**Stephane Buisson** *June 28, 2017 09:48*

Welcome in our community.

drop in replacement, check C3D mini

[Cohesion3D](https://plus.google.com/u/0/communities/116261877707124667493)

support by **+Ray Kholodovsky**

for purchase it's here :

[http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



softwares :

laserweb4

[https://plus.google.com/u/0/communities/115879488566665599508](https://plus.google.com/u/0/communities/115879488566665599508)

download here :

[https://github.com/LaserWeb/LaserWeb4](https://github.com/LaserWeb/LaserWeb4)


---
**William Kearns** *June 28, 2017 13:45*

**+Stephane Buisson** thanks I will check that out


---
**Jeff Lamb** *June 28, 2017 15:45*

**+Stephane Buisson** I cant recommedn the C3D enough - and I had an alternative smoothie board before the C3D - it is just so much easier to fit.


---
**William Kearns** *June 28, 2017 15:46*

Based on what I have read I went ahead and purchased  the c3d board


---
**Ashley M. Kirchner [Norym]** *June 28, 2017 23:15*

Congrats on your purchase. You might want to join the Cohesion3D group as well at [Cohesion3D](https://plus.google.com/u/0/communities/116261877707124667493) and start perusing.


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/AfwwR77AVSd) &mdash; content and formatting may not be reliable*
