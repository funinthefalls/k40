---
layout: post
title: "CorelLASER output file My M2nano controller board failed and I updated it to an arduino and gshield which I will be documenting soon"
date: March 15, 2016 22:31
category: "Software"
author: "HalfNormal"
---
CorelLASER output file



My M2nano controller board failed and I updated it to an arduino and gshield which I will be documenting soon.

Looking over the CorelLaser software, I noticed that you can save the project as a file instead of running the laser program. The extension is .egv. I looked around to see if I could find other programs that use this extension but had no luck. A peek into the file shows  lhymicro-gl and lihuiyusoft corp.

I tried opening it as a hpgl file without any luck. Funny thing is that none of their software works on it either.

Has anyone looked into this?





**"HalfNormal"**

---
---
**Scott Marshall** *March 15, 2016 23:08*

It's "Easy Guide Viewer" by Canon. Not much more on it. Try searching camera forums?

Hope the little snippet helps to some degree, at least you "know the enemy"


---
**HalfNormal** *March 15, 2016 23:13*

Saw that while researching the file. It is not related. In an obscure reference in a manual for lasers, it says it stands for engraving file. That is where the trail petered out.


---
**Scott Marshall** *March 15, 2016 23:49*

I passed it briefly while trying to get my Corel working. You were in on that and I thank you for the help. Currently I'm going from Draftsight outputting a pdf into the Corel. I've not had any luck with svg or any other vector type files, that's how I ran onto it myself. I had no luck either, and figured it was a Lihu/Corel propietary format. 



We need to figure out a decent system for this little laser that doesn't involve ripping the controller out by the roots. the Corel outputs dxf and PDF, but I've had no luck using an outputted dxf file



If I have any sucess, I'll drop you a note.



Scott


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Hp3zM7RAgE9) &mdash; content and formatting may not be reliable*
