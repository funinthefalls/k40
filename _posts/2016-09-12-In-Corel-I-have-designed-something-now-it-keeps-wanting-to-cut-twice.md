---
layout: post
title: "In Corel I have designed something, now it keeps wanting to cut twice"
date: September 12, 2016 00:37
category: "Discussion"
author: "J.R. Sharp"
---
In Corel I have designed something, now it keeps wanting to cut twice. What the heck is causing this?





**"J.R. Sharp"**

---
---
**greg greene** *September 12, 2016 00:45*

sometimes it interprets the line width as a second cut


---
**Rodney Huckstadt** *September 12, 2016 00:50*

set line properties to .001mm


---
**J.R. Sharp** *September 12, 2016 12:20*

Its a file that is imported from CAD, I don't see anything wrong.



[thingiverse.com - Phone Stand & Headphone Wrap (Laser Cut and Living Hinges) by OxyJin](http://www.thingiverse.com/thing:22964)


---
**Jim Hatch** *September 12, 2016 12:49*

Zoom in a lot (500% or more) and see if the lines are really lines or boxes. I imported a PDF that turned into extremely skinny boxes and it wasn't until I did a super zoom I saw that. Looked like a line to me.


---
**HalfNormal** *September 13, 2016 00:58*

**+J.R. Sharp** Line width needs to be .001 or less.


---
**Rodney Huckstadt** *September 13, 2016 03:49*

I have made this exact phone stand , and you need to select all lines and make them .001 thickness


---
*Imported from [Google+](https://plus.google.com/116586822526943304995/posts/ZrRMyHuvFmt) &mdash; content and formatting may not be reliable*
