---
layout: post
title: "In lightBurn 8.03 with my dell i5 laptop running windows 10"
date: September 12, 2018 01:15
category: "Software"
author: "James L Phillips"
---
In lightBurn 8.03 with my dell i5 laptop running windows 10. When I open the program it does not show the color bar at the bottom of the screen until I hit the minimize button twice. Then when i click on any object on the work space that item shifts up and I can't see the color bar again. Any suggestions to fix this?









![images/493c6c7a783def876fa49413f44f8dc3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/493c6c7a783def876fa49413f44f8dc3.jpeg)
![images/1daf8147f223111d8d8e834965cf3eea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1daf8147f223111d8d8e834965cf3eea.jpeg)

**"James L Phillips"**

---
---
**HalfNormal** *September 12, 2018 01:19*

You can drag the color bar anywhere you want. I put mine to the left.


---
**Mike R** *September 12, 2018 06:50*

Happens to mine sometimes I toggle the full screen button and it comes back


---
**James L Phillips** *September 13, 2018 00:03*

LightBurn  does not maximize  like it is supposed to. It is the same every time I open it. I think it is a program error but I am not sure.


---
**LightBurn Software** *September 15, 2018 06:15*

Close the “shape properties” window, or tab it / dock it behind one of the others. The content of that window changes when you select something, and it’s “minimum height” restriction changes to match. We’re likely going to change the way this control is coded to get rid of this, but it’ll take some time.


---
**James L Phillips** *September 16, 2018 12:21*

Thanks for your response.


---
**James L Phillips** *September 16, 2018 16:33*

I removed the shape properties and it still opens full screen hiding the the cut palette. I also removed the cut palette and it still cuts off the bottom of the program screen.  Is this a bug in the latest release and can I install an older version to fix this for now?


---
**LightBurn Software** *September 16, 2018 17:38*

It remembers the size and position. Shrink the window size by dragging the top edge down, then pull the whole window up, and that should fix it,


---
*Imported from [Google+](https://plus.google.com/100626830592271005238/posts/iTitJ9ipi9b) &mdash; content and formatting may not be reliable*
