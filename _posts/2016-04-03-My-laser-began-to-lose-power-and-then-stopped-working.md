---
layout: post
title: "My laser began to lose power and then stopped working"
date: April 03, 2016 03:13
category: "Discussion"
author: "Pedro David"
---
My laser began to lose power and then stopped working. How can I detect a failure? Was it the laser tube? It has only a few hours work.

Thank you.





**"Pedro David"**

---
---
**Scott Marshall** *April 03, 2016 04:05*

It's either a tube failure or power supply failure. If it happened gradually, it's most probably the tube. Sorry.

It seems that about half the time a new tube is bad, it dies just as you describe. The other half it's just DOA.


---
**Phillip Conroy** *April 03, 2016 07:26*

The only way to test the power supply is to chande it with a known working unit as it out puts 22000 which can kill-and is not possable to test for with out sepcial test gear.if it is under warranty send it back.


---
**Phillip Conroy** *April 03, 2016 07:29*

Same thing happened to me ,found out i did not turn water pump on and i cracked the tube as well as damaged the power supply .check tube for water leaks


---
**Vince Lee** *April 03, 2016 15:43*

It's been suggested that the poor man's way to test the high voltage output is to place a packing peanut near the wire and watching for movement when test firing.


---
**Pedro David** *April 03, 2016 16:35*

I've been checking visually the tube and I do not see anything different. Not burned or broken. The machine works well. Missing is the laser...


---
*Imported from [Google+](https://plus.google.com/113379323980322370039/posts/93AYao2E4ap) &mdash; content and formatting may not be reliable*
