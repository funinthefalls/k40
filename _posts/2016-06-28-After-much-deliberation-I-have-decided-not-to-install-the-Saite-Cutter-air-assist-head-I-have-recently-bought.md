---
layout: post
title: "After much deliberation I have decided not to install the Saite Cutter air assist head I have recently bought"
date: June 28, 2016 11:11
category: "Modification"
author: "Pete Sobye"
---
After much deliberation I have decided not to install the Saite Cutter air assist head I have recently bought. I have installed the MO mirrors I got and I am going to pass on the head to someone else. The stock head is working well for me. I am going to buy one of those 3D printed nozzles I have seen, with air assist. 



So, if anyone would like to buy the Saite head I have, it has a 50.8mm ZnSe lens fitted. I am asking £30 delivered (UK) which is a saving on the new price. 



Please contact me on 07809 154611 or pete.sobye@icloud.com. 



Thanks. 





**"Pete Sobye"**

---


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/grfyDvK2SD2) &mdash; content and formatting may not be reliable*
