---
layout: post
title: "Maybe someone will be useful. Laser Rotary Attachment for K40:"
date: September 22, 2016 18:17
category: "Modification"
author: "Linardas Verkulevi\u010dius"
---


Maybe someone will be useful. Laser Rotary Attachment for K40:


{% include youtubePlayer.html id="nAqu-836F5E" %}
[https://www.youtube.com/watch?v=nAqu-836F5E](https://www.youtube.com/watch?v=nAqu-836F5E)

[https://grabcad.com/library/laser-rotary-attachment-for-k40-1](https://grabcad.com/library/laser-rotary-attachment-for-k40-1)





**"Linardas Verkulevi\u010dius"**

---
---
**Damian Trejtowicz** *September 23, 2016 05:26*

I want extend my k40 with rotatary attachment,

Are you using aditional axis or replace Y when use?


---
**Linardas Verkulevičius** *September 23, 2016 11:40*

I raplace Y


---
**giavonni palombo** *November 13, 2016 15:14*

What steeper and are you charging the steps every time you use it?


---
**Linardas Verkulevičius** *November 14, 2016 18:40*

I'm using Nema17 stepper motor 17HD34008-22B (2-phase 4-wire I=1.5A).

What do you mean "charging the steps" ? 




---
**giavonni palombo** *November 14, 2016 21:26*

So you just change out the plug for the Y axis to your rotary, no changing any settings in software? 


---
**Linardas Verkulevičius** *November 15, 2016 06:22*

In software I just select "Rotary"

![images/74efd815d20c85b1d5b0d6205a85d282.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74efd815d20c85b1d5b0d6205a85d282.jpeg)


---
**giavonni palombo** *November 23, 2016 15:23*

Linardas Verkulevicius under the Rotary fixture setting for a "standard fixture" what does Plus /round mean or do? Under "simple fixture" what does speed ratio do? Thank you for your time.


---
**Linardas Verkulevičius** *November 24, 2016 20:26*

Hi, exactly, I don't know what Plus /round means. But I guess it means steps per revolution, it is how many steps motor makes to do one full revolution (360 degrees). 

I don't know anything about speed ratio. 

When I am engraving my glasses I set plus/round value at 1600 and I play only with diameter value. 30 mm diameter forks well for engraving my glasses.

I know it sounds confusing, but when I engrave glass with diameter of 50 mm I set 30 mm diameter in my software and the proportion of engraved image looks well. The result of engraving is showed in my video. 

Sorry for my late reply ;)


---
*Imported from [Google+](https://plus.google.com/113167710139335239517/posts/KhYE7PqDALA) &mdash; content and formatting may not be reliable*
