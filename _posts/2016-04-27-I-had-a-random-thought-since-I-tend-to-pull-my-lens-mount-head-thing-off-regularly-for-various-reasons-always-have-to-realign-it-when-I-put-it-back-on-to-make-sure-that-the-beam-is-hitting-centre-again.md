---
layout: post
title: "I had a random thought, since I tend to pull my lens mount/head thing off regularly for various reasons & always have to realign it when I put it back on (to make sure that the beam is hitting centre again)"
date: April 27, 2016 16:50
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
I had a random thought, since I tend to pull my lens mount/head thing off regularly for various reasons & always have to realign it when I put it back on (to make sure that the beam is hitting centre again).



So here is a guide that I designed up that should make shorter work of repositioning it in the correct orientation.



So it consists of a base ring with two slots at the top notched out to allow the screws to go where they belong. May need slight modification, depending on where your screws usually are when you mount the plate (mine is usually screwed at the back of the slots, i.e. top of the slots in this picture). This base ring also has a notch down the bottom left region to assist with aligning the piece with teeth on it.



The teeth piece has the teeth spaced apart at 5 degree intervals (at the points). In the gulleys is 2.5 degrees.



Then there is the ring with indicator arrow. This piece is intended to slot over the bottom of the Mirror3 mounting piece. Obviously aligning the arrow to be facing the direction the laser beam comes from & trying to get the arrow centred in the centre of the hole that allows the beam to hit the mirror.



If anyone is interested in giving this a go, CDR file is here:

[https://drive.google.com/file/d/0Bzi2h1k_udXwckxpMjI0SDB2dzQ/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwckxpMjI0SDB2dzQ/view?usp=sharing)



AI9 file here:

[https://drive.google.com/file/d/0Bzi2h1k_udXwSUwzWUg3N3BqX1k/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwSUwzWUg3N3BqX1k/view?usp=sharing)



AI (CC 2015) file here:

[https://drive.google.com/file/d/0Bzi2h1k_udXwWk9ZdXl4aUF0YU0/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWk9ZdXl4aUF0YU0/view?usp=sharing)



I haven't tested this as my machine is not put together at the moment (from last time I pulled it apart).

![images/606cf9eac1185a4ffd537a3cbbaa1385.png](https://gitlab.com/funinthefalls/k40/raw/master/images/606cf9eac1185a4ffd537a3cbbaa1385.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**3D Laser** *April 27, 2016 17:40*

Great idea I cut a groove in both my lens holder and the mount and I just line up the lines


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 18:24*

**+Corey Budwine** I was considering doing that, but didn't get around to it yet.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/PAjJi4pTbN1) &mdash; content and formatting may not be reliable*
