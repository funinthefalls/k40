---
layout: post
title: "Originally shared by Gee Willikers Made some tooling for a machine I built at work"
date: November 07, 2015 00:48
category: "Object produced with laser"
author: "Gee Willikers"
---
<b>Originally shared by Gee Willikers</b>



Made some tooling for a machine I built at work. It stickers things, can seaming tins in this case.



![images/8eb069655ff5c6444b342d3e2590e9bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8eb069655ff5c6444b342d3e2590e9bd.jpeg)
![images/cb3e09028890e899704c3486f0e32d5d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cb3e09028890e899704c3486f0e32d5d.jpeg)

**"Gee Willikers"**

---


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/NERAs7dfc6Q) &mdash; content and formatting may not be reliable*
