---
layout: post
title: "Use Citric-Based Hand Cleaner to Remove Laser Residue from Wood Projects"
date: November 23, 2016 17:12
category: "External links&#x3a; Blog, forum, etc"
author: "John-Paul Hopman"
---
Use Citric-Based Hand Cleaner to Remove Laser Residue from Wood Projects [https://blog.adafruit.com/2016/11/23/use-citric-based-hand-cleaner-to-remove-laser-residue-from-wood-projects/](https://blog.adafruit.com/2016/11/23/use-citric-based-hand-cleaner-to-remove-laser-residue-from-wood-projects/)





**"John-Paul Hopman"**

---
---
**Jim Hatch** *November 23, 2016 17:29*

Yep, Gojo hand cleaner is your friend 🙂 I use a large soft bristle fingernail brush (Amazon has them) to go fast - the larger surface area vs many small brushes helps make it quick.


---
**Carl Fisher** *November 23, 2016 19:55*

So does this not raise the grain of the wood and kill the fine detail? I would have figured any water based product would be a no-no but I've been wrong before :)




---
**Jim Hatch** *November 23, 2016 19:59*

Not usually as long as you don't let it puddle up and soak in. 


---
*Imported from [Google+](https://plus.google.com/114435223224758285486/posts/a6qngNruPPj) &mdash; content and formatting may not be reliable*
