---
layout: post
title: "Great group and so much informationhere! I receive my blue and white K40 yesterday"
date: September 30, 2017 23:04
category: "Hardware and Laser settings"
author: "Al Kardos"
---
Great group and so much informationhere! I receive my blue and white K40 yesterday. Won't 

 even go to into the problems that I found with it when I got it. 



Having a problem getting the X axis to home. Y-axis moves all away to the top of the machine then the x-axis slides about 3" to the right and stops. Turning it  off and back on makes the x-axis move another 3" to the right. It continues doing this until it gets to the end of travel and grinds away as the belt skips. 



I checked the x-axis ribbon cable and it is secure on both ends. If I manually move the head to where I want to print it sometimes actually prints in the right place, but it does print. 



I've searched and searched for a simple way to say this is the origin, this is where you start to print. My assumption is that when the machine is turned on it should go to the home position which is the upper left? 



Any help would be greatly appreciated, thanks in advance. 





**"Al Kardos"**

---
---
**Ned Hill** *October 01, 2017 03:30*

Assuming you are using coreldraw or laserdraw, do you have the M2 control board selected in the software (mainboard below)?   Also make sure the device ID is set to what is printed on your control board.  On power on the head should do a homing check to the top left then move to the origin as defined in the settings.



![images/4aad964ce364b4d3a512456daf054e78.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4aad964ce364b4d3a512456daf054e78.png)


---
**Ashley M. Kirchner [Norym]** *October 02, 2017 04:26*

Check for disconnected, dirty, or broken X limit switch, upper left, behind and under the stepper motor.


---
**Al Kardos** *October 05, 2017 10:49*

EXCELLENT suggestions!



Ashley, you nailed it. The x sensor had been damaged to the point that the copper traces had lifted right off the board! Seller is sending a new one.

Thank you all!



![images/cdf40aa5095804cbd3b0b494ef6e27ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdf40aa5095804cbd3b0b494ef6e27ae.jpeg)


---
**Ashley M. Kirchner [Norym]** *October 05, 2017 16:36*

That looks like the carriage has slammed into the sensor and the metal "pin" that's supposed to go in the sensor gap hit it so hard it pushed the whole sensor through the board and ripping out the tracks on the back. Two things:

a) that's shitty solder work on their part because the component should never be soldered off of the board like that, it should've been soldered flat up against the board which would've prevented this damage, and

b) crappy packing job


---
*Imported from [Google+](https://plus.google.com/106756369649678216091/posts/eTMRhf1wVGw) &mdash; content and formatting may not be reliable*
