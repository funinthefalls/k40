---
layout: post
title: "Hello everybody, I have a problem with the dimensions of files to be engraved or to cut"
date: February 26, 2018 10:07
category: "Discussion"
author: "Fabien Lyx"
---
Hello everybody,



I have a problem with the dimensions of files to be engraved or to cut. For example in the software my drawing makes 40mm but when I throw the laser the engraving makes 12mm. I have a problem of dimensions what is what you can help me pleases





**"Fabien Lyx"**

---
---
**Ned Hill** *February 26, 2018 17:56*

Are you using the stock controller and what software are you using?


---
**Fabien Lyx** *February 27, 2018 07:08*

**+Ned Hill** 

Hello, one thank you for your help.

Sorry for my bad English.

What the stock to controller? I use the software to corellaser

Thank you again for your help sir


---
**Ned Hill** *February 27, 2018 16:59*

Stock controller refers to the control board that came with the machine.  Sounds like you are using the stock control board with corellaser.  I forget what causes this problem but it's one of the settings.  The following pics have what your settings should be. The device ID is unique to your machine and is printed on the control board in the laser. 


---
**Ned Hill** *February 27, 2018 17:00*

![images/0c2f9c56dbd8bb34b6d8e9fa59771428.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0c2f9c56dbd8bb34b6d8e9fa59771428.png)


---
**Ned Hill** *February 27, 2018 17:00*

![images/b0de1e4efb534861f753fb8b795bef1d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b0de1e4efb534861f753fb8b795bef1d.png)


---
**Ned Hill** *February 27, 2018 17:06*

Control board location. ![images/19eba9a7a67c4c1980b415353c4c9eca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19eba9a7a67c4c1980b415353c4c9eca.jpeg)


---
**Fabien Lyx** *February 28, 2018 12:50*

**+Ned Hill** 

THANK YOU VERY MUCH for the advice councils and your time.

I look all this and contact again you if need.

Thank you again for your help


---
*Imported from [Google+](https://plus.google.com/115452371683283385024/posts/KJ5MKVjjUBg) &mdash; content and formatting may not be reliable*
