---
layout: post
title: "This is probably going to sound rather dumb but I figured it won't hurt to ask"
date: November 11, 2016 21:12
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
This is probably going to sound rather dumb but I figured it won't hurt to ask. I purchased, and received, new mirrors from saite_cutter on eBay. Forgot who recommended the vendor. My current, stock mirrors, are only polished on one side. Lo and behold, these new mirrors are polished on BOTH sides. One side has the tell-tale yellow coating, and the other side is just silver.



So I ask: is there a difference?



I going to <b>assume</b> that it's the yellow side that I need to be using, but with the silver side also being highly polished, is it worth protecting that side if some day I want to flip the disc over some time in the future, in case I accidentally scratch the yellow side? Inquiring minds want to know ...





**"Ashley M. Kirchner [Norym]"**

---
---
**Anthony Bolgar** *November 12, 2016 09:25*

I have a feeling the laser would chew through the silver only side, but that is just a gut feeling. Can't hurt to protect them just in case it would work, costs nothing to try and once the mirror is dead on the proper side, iworst that happens is it does not work on the silver only side.


---
**Cesar Tolentino** *November 12, 2016 14:11*

we also have to remember the diy mirror out of hard drive discs? those are mirror silver too. So maybe difference only on intensities but should work. So i say protect them, and use them as spare, just in case you ruin the yellow side.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/aiysM3xNkiV) &mdash; content and formatting may not be reliable*
