---
layout: post
title: "Can anybody explain the ghosting I'm getting on smaller text?"
date: September 18, 2016 14:25
category: "Materials and settings"
author: "Adam J"
---
Can anybody explain the ghosting I'm getting on smaller text? Larger text works fine. My alignment is good, cuts are great. I'm using a SAITE head with air assist. Same result in CorelDraw and LaserDRW with various fonts. Is my lens out of focus?!



![images/359df3b625e44cf4fd14925b19b8a45f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/359df3b625e44cf4fd14925b19b8a45f.jpeg)
![images/a1853bb2aae2c14aead612adc422adf5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a1853bb2aae2c14aead612adc422adf5.jpeg)

**"Adam J"**

---
---
**Joachim Franken** *November 01, 2016 22:17*

perhaps a reflexion in the air assist if it is in aluminium


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/3uTr4iBYJq6) &mdash; content and formatting may not be reliable*
