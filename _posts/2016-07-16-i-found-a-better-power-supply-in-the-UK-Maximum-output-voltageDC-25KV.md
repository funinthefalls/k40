---
layout: post
title: "i found a better power supply in the UK Maximum output voltage:DC 25KV"
date: July 16, 2016 08:55
category: "Discussion"
author: "Hilco Smit"
---
i found a better power supply

in the UK



Maximum output voltage:DC 25KV.

Maximum output current: DC 20mA.

Isolation Withstand voltage: Input-output, input-enclosure: AC2000V-10mA-60s; negative pole of output connected to enclosure.

Protection:with well-grounded enclosure, output circuit of power supply can be open for short time (but arc between positive pole and enclosure should be avoided).

Response time (from time of input being available to the output current being up to nominal value): ≤1ms.

TTL voltage level control: effective voltage level can be high or low through setting.

Protection switch: for detection of cooling water, to protect laser device; or activate with enclosure is opened.

Laser power adjustment: (1) by adjusting the output current of power supply through resistor; (2) through PWM control (magnitude being TTL voltage level)

Environmental requirements:temperature (-10~ 40℃), relative humidity ≤85%.

Input voltage:AC220V or AC110V (to be specified when placing order).

![images/97c3295331cd754a3205906029706127.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97c3295331cd754a3205906029706127.jpeg)



**"Hilco Smit"**

---
---
**Stephane Buisson** *July 17, 2016 09:03*

**+Hilco Smit** do you actually try it ?

do you find any documentation for it ?

PWM ?


---
*Imported from [Google+](https://plus.google.com/114814816541136544019/posts/PgboUKJ4mVJ) &mdash; content and formatting may not be reliable*
