---
layout: post
title: "New Canvas 4$ knife from Walmart with wood handle and stainless steel blade..."
date: July 01, 2016 06:29
category: "Object produced with laser"
author: "Alex Krause"
---
New Canvas 4$ knife from Walmart with wood handle and stainless steel blade... going to attempt engraving the wood and marking the blade with moly lube﻿

![images/7911ff4ef0f09034253f50065fab68c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7911ff4ef0f09034253f50065fab68c0.jpeg)



**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 01, 2016 06:49*

That's a beaut canvas. Have fun!


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/6jY2Ms4fvik) &mdash; content and formatting may not be reliable*
