---
layout: post
title: "What could cause this behavior in a print job?"
date: February 21, 2016 23:34
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
What could cause this behavior in a print job? Took laser apart to lubricate and clean, I did notice certain points which align with this image where y axis seem to catch or drag. So I ordered new bearings for the slide rail, but want to make sure that there aren't issues with stepper driver or steps parameters issues in Marlin code. So again, what all can cause this type of behavior?



![images/037e658114dcd20e51754b1495651084.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/037e658114dcd20e51754b1495651084.jpeg)
![images/7697c8be7d8b1036e2d8b2b46a6490c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7697c8be7d8b1036e2d8b2b46a6490c9.jpeg)

**"Andrew ONeal (Andy-drew)"**

---
---
**Anthony Bolgar** *February 21, 2016 23:50*

Too fast of a feed rate cn cause this, try slowing down the laser head.


---
**Scott Marshall** *February 22, 2016 01:56*

All motion should be smooth and effortless, with little or no perceptible play. No catches or drag at all. Check for damaged bearings (you did the right thing by ordering the guide rollers, you'll need them sooner or later) , clean guide rails, belt condition, snug, no missing teeth, no debris in pulleys or belt recesses. 

One last thing, the opto-interupptors  can get dusty and not "see" well, a q-tip gently dragged thru them cleans em out. 



I like to give my machine a "clean and check" whenever it seems to be acting funny or is getting cruddy. I do a lot of plywood and it gets full of dust fast. The laser tube draws it via the high voltage and it gets dirty pretty fast. It's never acted up, but I figure clean is good and if it prevents any arcing and the possible loss of a tube, it's worth it. A clean, dry paintbrush works great. Blow out after.



It only takes 10-15 minutes to go thru and blow out the crap, wipe down the chassis and then clean window and the optics last. I figure it's time well spent even if it saves 1 job, or extends the life at all.



These are straight stepper motors, no feedback at all,  if you have a stoppage or run it too fast (as Mr Bolgar mentioned), it will jump synchronization and keep right on going. The "shift"  in your 1st example is a classic example of what it does. It Looses the motion where the motor was jammed, and continues along "shifted" by whatever motion was lost. You can usually hear the jam occur (Loud banging sound). If it jams/jumps in the same place repeatedly look to the belt or tracks for debris or damage. Failing bearings freeze/drag at random locations, most frequently during a direction change.



That's why the high dollar units use servos or encoder feedback. (they also have fully enclosed optical paths, water cooled optics etc)



For the money, the steppers work fine, you just have to tend to them a bit more. 



Hope it helps,

Scott



﻿


---
**Tony Sobczak** *February 22, 2016 08:09*

Where do you get the spares? E. G.  Guide rollers and anything else I should have on hand? Only have my a few days now and just getting used to it. 


---
**Anthony Bolgar** *February 22, 2016 15:07*

Light Object sells some of the spare parts needed for the K40 laser. [www.lightobject.com](http://www.lightobject.com)


---
**Tony Sobczak** *February 22, 2016 21:40*

Thank you **+Anthony Bolgar**​. 


---
**Andrew ONeal (Andy-drew)** *February 25, 2016 04:52*

Scott thank you for the detailed reply, wish more woulddo the same. I came into this from rc hobby back ground thinking no problem I got this lol. Man was I so very wrong,  the mods were easy but the aligning for maxlaser strength,  gantry,  cleaning,  and then this slide rail bearing (purchase everything through eBay  folger tech stuff,  really cheap if you don't mind doing all you o laid out above). The other issue I'm still struggling with is Inkscape and I've posted before on these issues of setting up jobs in inskape then generating the correct code. This whole experience is like taking tech, photo editing, coding in various forms from arduino to gcode. 



On a more positive note I found a way around the bad bearings by printing diagonally. Doing this finally let me start testing successfully with all kinds of things.  Just now trying to learn vectors since they print so much quicker than raster. I took the panel cut out svg from github but when I print the size is off same with another svg ghost quad frame,  not sure why.  Also in these example there are parts that will not print but will do the action, like outline with no laser fire. Most of these are circular, again not sure. 


---
**Scott Marshall** *February 25, 2016 07:56*

Good job on the diagonal cutting, Clever way to drop the bearing speed. I like you. There was a time when I'd have offered you a job just on that idea.



I'm an RC guy too, Started flying again about 3 or 4 years ago. Wanted to since I was a youngster and never had time. Been into giant scale 2 stroke gas planes for a couple years now. (flying weedeaters I call em).



I was forced to retire from an industrial controls business by health issues. That puts me in good stead to get  the hardware going here,  but the software is a killer. I'm one of those "Old Guys" that made his living with Autocad 11 (What's that? is all the help you get there) All these younger folks that don't remember the 286 and grew up with a smartphone have an entirely different way of going about things. Not that I object, there's no denying the results, but it's tough learning a totally new way of doing things.



I originally started this so I could cut foam planes out, make servo levers in bulk etc. and have since found all sorts of other stuff to do.



I've been buying a lot of stuff from Saite Cutter on ebay. Prices are good and stuff shows up faster than you'd expect.. 

I just discovered that a longer lens opens up your focus zone, and improves cutting a LOT. You'll have to move your work down to keep it in focus, but for $22 buy a 4" lens and you'll be amazed. I was.



I had a "Crash" course on mine, it was dropped off a truck and showed up totaled. They gave me $250 bucks back. Built a billet Carriage and pieced the rest back together, working Ok now. Just adding alarms, interlocks, etc to it over the last few days.



Glad you like my long replies, I tend to run on here. (RC Groups too) It's not like a real conversation where you can get a feel for how much a fellow knows/wants to know. So, I tend to make sure I cover the details. I'm afraid people sometimes feel like I'm telling them stuff they already know, but no insult is intended, I just try to make sure the question is answered.



Have Fun, these are a cool gadget for sure



Scott


---
**Andrew ONeal (Andy-drew)** *March 06, 2016 04:39*

Hey scott, thank you for the compliment regarding the diagonal cutting solution. I wish I had the knowledge of how to actually code like some of the others around here. I think it is amazing the things that can be done these days with just a couple numbers. 



The oldest piece of computing equipment I can remember was our first in the family, the commodore 64 and was old enough to have some great times on it playing games. I wish I had been a bit older when it came out then I could have done more in the area of programming. 



There currently is only one program I am running that I use the diagonal cutting. mostly involves raster projects, pics and such. I think I am going to have to replace the bearings after all just to ensure that the vector cuts/engravings are spot on and not missing any steps. I could not find the correct slide bearing to replace so I am going to use two separate bearings and may have to modify that whole section of the gantry. Not sure just yet what I am goin to do there. I have considered getting a new gantry and build the entire laser to be more mobile/free. Not sure how that works just yet and have not looked into it either. Thinking the co2 laser would not be powerful enough to support reflecting a beam say at 2.5-3 feet away from second mirror.



Like you I am never at a loss for words, a trait love by some but disliked by so many like the wife lol. Was very sorry to hear that you were forced to be retired due to health, but given your back ground perhaps you can still make your dent in electronics industry, seems to be the place to be right now. I took the privilege to add you to my circles, not sure what that means just yet, but you are added. The RC arena has really taken hold of my life, in fact I have been working for sometime on establishing a great sales avenue to offset my income, due to disability. Just secured the wholesale contract fairly recently which are prices unmatched by many. So we shall see what happens there. I did check into the 4 inch lens and put into my cart but not yet purchased.  



Gotta jet, 

Take care and thank you for your response to my cry for help. lol


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/f5hr2h4tXUY) &mdash; content and formatting may not be reliable*
