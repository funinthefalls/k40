---
layout: post
title: "Update the community on dynamic laser control testing"
date: July 23, 2016 11:06
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Update the community on dynamic laser control testing. 



I have been travelling and away from the shop so my smoothie conversion has been on hold and I am behind on G+ reading.

I made progress on the testing setup and hope to complete the first measurements this weekend when I return.



You can see the approach here: 

[http://donsthings.blogspot.com/2016/07/dynamic-laser-response-testing.html](http://donsthings.blogspot.com/2016/07/dynamic-laser-response-testing.html)





**"Don Kleinschnitz Jr."**

---
---
**Don Kleinschnitz Jr.** *July 23, 2016 11:19*

**+Peter van der Walt** that's the plan. We will see if the theory converts to practice :).


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/7TyJswhYNYC) &mdash; content and formatting may not be reliable*
