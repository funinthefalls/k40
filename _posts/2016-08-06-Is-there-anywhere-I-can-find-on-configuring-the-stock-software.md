---
layout: post
title: "Is there anywhere I can find on configuring the stock software?"
date: August 06, 2016 19:47
category: "Original software and hardware issues"
author: "John Austin"
---
Is there anywhere I can find on configuring the stock software?



And I seen a picture some where showing one entering the serial number off the main board.





**"John Austin"**

---
---
**John Austin** *August 06, 2016 20:24*

Found the board model needed to be changed.


---
**John Austin** *August 06, 2016 20:34*

is it common to have to restart corellaser draw after each project?

my function toolbar in top right corner disappears and you have to restart software to get it back.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 20:38*

No, you will find that the toolbar does disappear, but there is an icon down the bottom in windows near the clock. You can right click it and access all the functions (cut/engrave/start/etc) that are on the toolbar.



I have a bunch of config screenshots that I will share here in a minute. Bear with me while I find them & link them.


---
**Jim Hatch** *August 06, 2016 20:39*

The icon bar in CorelLaser disappears from the upper right and tucks itself away in the lower right corner of the Windows taskbar. 


---
**John Austin** *August 06, 2016 20:40*

Thank you. I did get it to finally engrave. Till my  power supply to my fan died.  Now need to find anither 12 v power suppl


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 20:41*

You will find a handful of screenshots (with comments I made) here: [https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 20:42*

**+John Austin** Is this a new machine? If so contact the seller about faulty PSU & ask for a replacement/refund to cover the costs of getting a new one.


---
**John Austin** *August 06, 2016 20:46*

Its a 12 volt power supply that i had to run a fan that i switched out with the stock fan.  I need to see if the stock power supply for the laser has a 12 volt output.



thanks for the link. I will check them out in a few.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 20:54*

**+John Austin** Oh right, for the exhaust fan? I misinterpreted. I thought the laser PSU died because it's fan died. Lucky it wasn't that.


---
**John Austin** *August 06, 2016 21:16*

**+Yuusuf Sallahuddin**

Going to check and see if there is a volt on the  PSU that i can use.

Is there anyway to pause the laser while it is running?


---
**John Austin** *August 06, 2016 21:16*

First successful runs

[http://s94.photobucket.com/user/gunracksonline/media/20160806_171100_zpsphxkp7ei.jpg.html](http://s94.photobucket.com/user/gunracksonline/media/20160806_171100_zpsphxkp7ei.jpg.html)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 22:56*

**+John Austin** There is a pause option for whilst the laser is in the middle of a task, although it has a slight delay before it actually pauses. In an emergency, I just turn the laser-enable switch off to prevent disaster. You will find the pause option either on the toolbar or the right-click menu of the system tray icon.



First successful run looks good. Looking at it closely I have a feeling that either your alignment is not precise yet, or your material wasn't level along the whole engrave. I say this because the words "This is a test" seem to be darker at the beginning than at the end. Could just be the wood or photo too...


---
**Tim Delaney** *September 13, 2016 00:30*

**+Yuusuf Sallahuddin**  Hi, I am an owner of a new "K40" and going thru the learning curve. I am using CorelDraw X7 with "CorelLaser" in Windows 10 and I am having the same problem with the CorelLaser toolbar disappearing [from the upper right] after each print job.  I can't locate it anywhere and my fix has been closing CorelDraw and reopening through CorelLaser each time. I have tried opening Corel Laser without closing CorelDraw, however it does not work that way. Any help would be appreciated, I must be missing something!  Thanks,

Tim


---
**Jim Hatch** *September 13, 2016 01:35*

**+Tim Delaney**​ lower right corner of your Windows desktop is the date/time. If it's recent windows there's also an upward pointing triangle/arrow. Click that and you'll see all the running things that hide their icons when running. One of them is your CorelLaser. Right clicking will bring up the icons that disappeared from Corel when you started the job.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 13, 2016 05:48*

**+Tim Delaney** as Jim mentions, you should find it down near the time/date. Sometimes there is an arrow (which Jim also mentions) that when clicked will bring up a little popup with all the apps that have an icon in that area. I've personally set Windows to always show all apps that are open down there.



**+Jim Hatch** Have you also noticed with the CorelLaser in Win10 that the stupid notifications that Win10 does for the plugin (e.g. "monitoring last progress") sometimes cover the popup menu  & make you have to wait for the notification to get lost before you can do anything? That was seriously annoying me at times.


---
**Jim Hatch** *September 13, 2016 14:01*

**+Yuusuf Sallahuddin**​ yeah, the notifications float there for longer than it takes to read. Sometimes you can hit the ESC key and it will go away. Depends on the app.


---
**Tim Delaney** *September 13, 2016 19:29*

Hi 

Thanks for the tips, I seem to have it sorted out thanks for keeping me pointed in the right direction.  I think part of my problem might be an overzealous version of Macafee on my laptop.

In any event, moving on the the next bug!  Thanks again!


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/YusRnnnCPX1) &mdash; content and formatting may not be reliable*
