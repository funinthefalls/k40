---
layout: post
title: "Does anyone have any experience cutting 3M VHB foam tape with our lasers?"
date: December 10, 2018 21:17
category: "Materials and settings"
author: "Bob Damato"
---
Does anyone have any experience cutting 3M VHB foam tape with our lasers? I need to cut some custom shapes for fender badges, and its making a fairly gooey mess.  I set the level down and made several passes and also cranked it but results werent stellar. It almost  self heals behind the laser.  Im sure die cutting it would be the right answer but that isnt possible.  This is the stuff:



[https://www.ebay.com/itm/3M-VHB-Double-Sided-Foam-Adhesive-Sheet-Tape-5952-Automotive-Mounting-5952-Gopro/400744238326?var=670289078294&_trkparms=aid%3D222007%26algo%3DSIM.MBE%26ao%3D1%26asc%3D20140131123730%26meid%3D84bc762a04744926b7e14329af6a61b6%26pid%3D100167%26rk%3D1%26rkt%3D15%26sd%3D400744238326%26itm%3D670289078294&_trksid=p5411.c100167.m2940](https://www.ebay.com/itm/3M-VHB-Double-Sided-Foam-Adhesive-Sheet-Tape-5952-Automotive-Mounting-5952-Gopro/400744238326?var=670289078294&_trkparms=aid%3D222007%26algo%3DSIM.MBE%26ao%3D1%26asc%3D20140131123730%26meid%3D84bc762a04744926b7e14329af6a61b6%26pid%3D100167%26rk%3D1%26rkt%3D15%26sd%3D400744238326%26itm%3D670289078294&_trksid=p5411.c100167.m2940)



Bob







**"Bob Damato"**

---
---
**Ned Hill** *December 11, 2018 00:16*

Yeah stuff that melts and self-heals is a pain.  I had some thin teflon sheeting I was cutting once that self healed and I had to do multiple passes. 



 Place it in the freezer (or dry ice?) to get it cold and cut it immediately while it's still cold maybe? May have to sandwich it between something else that's thin. (probably won't work, but just spitballing here. ;)



How about laser cutting a template out of ply and then using it and an exacto knife to cut the foam?


---
**Bob Damato** *December 11, 2018 01:16*

**+Ned Hill** HI Ned good recommendations, thank you.  I can definitely try freezing it first thats a good idea.  The template idea probably wouldnt work since its semi complex and I need a lot of them. I cut it with the laser then cleaned it up with an xacto, it was not fun.  Ill cross my fingers for the freezer idea and let you know!


---
**Ned Hill** *December 11, 2018 15:09*

Another thing to try is to crank the flow on your air assist.  You would want the material to cool right after it's cut to prevent it from rejoining in a melted state.  So increasing the air flow should help with the cooling.


---
**Ned Hill** *December 11, 2018 15:17*

Not sure what kind of air assist you have but for this to work most efficiently you would probably need a narrow nozzle flow near the cut.  Bend like some small diameter brass tubing and attach it to your laser head so that one end is close to the cut and attach the other end to an air supply. 

 The normal after lens air assists really aren't that efficient for something like this because of the large opening.


---
**Bob Damato** *December 11, 2018 15:23*

**+Ned Hill** Air assist? Thats what you rich guys have ;)  I know. I keep meaning to get that but havent yet. Any recommendations?




---
**Ned Hill** *December 11, 2018 16:02*

Personally I use a combination lens holder / air assist I got from lightobject and use an airbrush compressor  for air delivery.  It works well enough to keep the bottom of the lens clean and to keep flame ups down. 

But, like I was saying, it's not really efficient for the purposes for getting air down into the cut line.  You really would near something like this thing from thingiverse.  [https://www.thingiverse.com/thing:869652](https://www.thingiverse.com/thing:869652)



That uses some 3d printed parts, but you get the idea.  You would just have to come up with away to attach the tubing to the head.  That part used 1/4 inch copper tubing and it's got a added printed nozzle.  I would think 1/8 tubing would work as is, which is why I was saying brass tubing like you can get from a craft store.  You can also get a small tubing bender fairly cheap from harbour freight.  I'm sure somebody has done something like this before (pretty sure I've seen it somewhere) so you may have to search around.  



Air supply comes down to an air compressor, airbrush pump or a large aquarium/pond air pump.



might want to start a seperate post if you want more ideas for this type of air assist.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/2qL9gbqDD55) &mdash; content and formatting may not be reliable*
