---
layout: post
title: "Problems installing new M2Nano board I received my new M2Nano board and have been trying to get the software to recognize it"
date: April 03, 2016 01:08
category: "Hardware and Laser settings"
author: "HalfNormal"
---
Problems installing new M2Nano board



I received my new M2Nano  board and have been trying to get the software to recognize it. I keep getting this message "engraving device information invalid device-id cannot use it!" after trying to initialize the laser. A message that says "engraving machine connected." is shown when starting the software. The web is of no use. Totally removed all software with removal software to get every bit, swapped USB inputs and watched it reload drivers, searched the registry for any sign of old ID, removed old ID from INI files in file directories and still no luck. If I put the old card in, the software says it is initialized. This means that the ID is hidden somewhere I cannot find it. Maybe the USB key? It does not seem to read the new cards' ID. Any ideas?





**"HalfNormal"**

---
---
**I Laser** *April 03, 2016 01:46*

Your board has a serial number on it, you need to put that serial number into the settings in Coreldraw. Click the gear icon in corel and down the bottom left is the ID/Serial field.


---
**HalfNormal** *April 03, 2016 01:50*

Wish it was that easy! Did try but if you look at the number on the old board, it does not match the number in the ID field. Second, If you remove the number and plug the board back in, the same number comes back.


---
**I Laser** *April 03, 2016 01:58*

Not sure I understand, have you actually tried to change it?



I had the exact same situation. I bought a second k40, when I fired up corel it gave the same error 'invalid ID'. I noticed the serial number in corel didn't match my original machine either. It's like a hash or something.



I took note of it and put the the serial off the new machine in. Restarted corel and it worked fine. I occasionally get the wrong machine connected in my VM, when that happens I have to replace the serial/ID in corel accordingly to get it to work.


---
**HalfNormal** *April 03, 2016 02:04*

All the programs complain, not just corel. Here is the number in the ID field. 8E620BF7120D80F7

Here is the number off the board

23637DFFD1C37C91

This is from the old board that the software recognizes. I have deleted all the INI files in all the software directories and the first number keeps popping up. 


---
**I Laser** *April 03, 2016 02:08*

What other software are you using, the same plugin/settings are used for laserdraw too I believe.



Anyway your situation sounds exactly the same, if you haven't tried changing it and would rather theorise whether it's the solution or not that's up to you. Good luck...


---
**HalfNormal** *April 03, 2016 02:18*

I did it! **+I Laser** thanks for jump starting my brain! I have been working on this problem for hours. When I reinstalled all the programs, I notice that WinsealXP had "NONE" in the ID area where LaserDRW and Corellaser would default to the old ID. I input the board serial number into WinsealXP and it took the number! I was focused on Corellaser because that is the program I use. I had to copy the new ID and paste it into Corellaser for it to work. What a Cluster!


---
**HalfNormal** *April 03, 2016 06:45*

**+I Laser** To clarify why I was a bit confused and dumbfounded. I tried to input the board serial number into the laser software from Corellaser. For whatever reason, when I put the board serial number in that location, the software would not update and take the number. The same happened with LaserDRW. So two places and no luck. I did notice that the old number kept returning to the INI file even when the INI file was deleted. Only when I entered the number into WinsealXP did it all work. In order for the number to actually take in Corellaser I had to take the new hash and enter it into Corellaser manually. I wish I knew why I had to do it that way.


---
**I Laser** *April 03, 2016 11:14*

No need to clarify, glad you got it sorted.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/JkkgqhwLEHi) &mdash; content and formatting may not be reliable*
