---
layout: post
title: "Yuusuf Sallahuddin .....here is a better example"
date: April 03, 2016 00:13
category: "Object produced with laser"
author: "Scott Thorne"
---
**+Yuusuf Sallahuddin**.....here is a better example.

![images/523c2c92aae19cf29e2cec6330eb360e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/523c2c92aae19cf29e2cec6330eb360e.jpeg)



**"Scott Thorne"**

---
---
**ED Carty** *April 03, 2016 00:29*

That is even more amazing considering the size


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2016 00:32*

Wow, that's pretty spectacular. Each of those little square/rectangles in the image must be only like 2-3mm max.


---
**ED Carty** *April 03, 2016 00:32*

You could make a set, and Bear makes an epoxy that would fill it in but dries very clear and make a great coaster set for your favorite liquid libations.


---
**Scott Thorne** *April 03, 2016 00:50*

**+ED Carty**...gee I would have to say Jose at the moment...but that's a great idea. 


---
**Scott Thorne** *April 03, 2016 00:52*

**+Yuusuf Sallahuddin**...I would say that they are smaller than that....I need to measure them and see....it's amazing how the laser fires that fast and with that accuracy...mind blowing. 


---
**Daniel Ezell** *April 03, 2016 01:24*

It would be really interesting to see the moire patterns that would appear when you print the same image on transparency and put it on top of your pattern. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/DaYWFomMrFQ) &mdash; content and formatting may not be reliable*
