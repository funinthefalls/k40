---
layout: post
title: "If anyone can help, I'm having issues with my PSU"
date: January 17, 2017 16:21
category: "Discussion"
author: "Madyn3D CNC, LLC"
---
If anyone can help, I'm having issues with my PSU. I did a through search in the group and found some great info, and even found someone with a very similar issue but no solution in the end. 



The green LED indicator light on the PSU's PCB seems to be dim, and when I press the on-board tactile test switch I can hear a slightly audible "whine" of either a PWM switching or perhaps the flyback transformer trying to work, but not working correctly. 



I have 2 good 40W tubes and both will not fire. All garbage connections/grounds have been soldered and cleaned up.



I see no signs of any component failure. This is driving me nuts. I just want to get this thing back up and running so I can generate some funds to go ahead with the cohesion3D upgrade bundle and trash all these Chinese circuits altogether. 



Here's some pics of the PSU opened up, if there's any other info I can provide please let me know. I have the KH-3020 model laser with corel. All upgrades such as air, lights, etc are on their own LiPo power supply, I have done nothing to the stock K40 PSU itself, aside from taking the housing apart to inspect for shorts/blown components. 







![images/361f8cf3503ddedd471bb55eb56b4f8a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/361f8cf3503ddedd471bb55eb56b4f8a.jpeg)
![images/824d9f80d9b56bdc13780d37946f4d4b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/824d9f80d9b56bdc13780d37946f4d4b.jpeg)

**"Madyn3D CNC, LLC"**

---
---
**Kostas Filosofou** *January 17, 2017 18:59*

Have you read this post? [https://plus.google.com/+KonstantinosFilosofou/posts/9AF1i2hZjU2](https://plus.google.com/+KonstantinosFilosofou/posts/9AF1i2hZjU2)



I don't know if helps you... in my case I don't have any power in the board the green light was off..




---
**Madyn3D CNC, LLC** *January 17, 2017 23:30*

yes I have and good info there. I wish I had some sort of indicator such as a blown component or short to give me a good starting point, but all looks to be fine aside from the LED being dim and the whine it makes when I try to fire it. I'll be one happy camper if I can just swap out a component or the flyback rather than sourcing an entirely new PSU. I found this thread here in CNCzone where it sounds like this individual was having the exact same problem...   [http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/252972-software-posts-2.html](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/252972-software-posts-2.html)


---
**Jon Bruno** *January 24, 2017 00:00*

is the main fuse blown?

it looks a little strange in the photo.

if so, I'd point at the most common failure, the bridge rectifier


---
**Madyn3D CNC, LLC** *January 24, 2017 14:05*

Thank you Jon, much appreciated. As much as I wanted to fix the PSU on my own, down time was getting the best of my pockets so I had to just go ahead and purchase a new PSU, which came in yesterday. 



It's too bad because this is how I learn. This is how I've been learning everything over the years, by hands-on learning.



The main fuse is good, all connections are good, ammeter is good, gates are all good, PSU was making odd noises and the green LED was very dim. I'd still like to get the bad PSU fixed up because there's no shortage of uses for it around here. I will check out the bridge rectifier and report back here in case it help for future members.


---
**Jon Bruno** *January 24, 2017 14:12*

No problem-o. Best of luck!


---
**Madyn3D CNC, LLC** *January 24, 2017 14:24*

I'm going to try and find this bridge rectifier, at first I was confusing it with the flyback because of the schematic symbols. Chinese schematics are always fun to decipher.....   



I suspected the flyback to be bad, my only problem is no proper HV probe to use w/ any of my DMM's (multimeters) to give me a definitive conclusion. I'm not a fan of HV after a bad experience learning first hand how far it can travel on non-conductive mediums. I suspect I would have figured out the issue if I only had a HV probe.  

![images/bfc5366c6219332662c1080691b1750f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bfc5366c6219332662c1080691b1750f.png)


---
**Madyn3D CNC, LLC** *January 24, 2017 14:47*

I'm assuming this circled component is what you referred to as the bridge rectifier. I can go ahead and test that with a general multimeter with no HV probe. The machine has been powered down for 2 weeks now. 

![images/ea382d788e92b05c03cf7f268228bf44.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ea382d788e92b05c03cf7f268228bf44.png)


---
**Jon Bruno** *January 24, 2017 19:39*

Well. lets do this..

I wouldn't worry too much about the High voltage transformer. they rarely go bad if ever.

the That is a full wave bridge rectifier array depicted in the photo but the one you are looking for is not physically 4 diodes. it's a one piece 4 pin package.[https://drive.google.com/file/d/0B36nra1ArP-VZHhLZ3BTazJvaVE/view?usp=sharing](https://drive.google.com/file/d/0B36nra1ArP-VZHhLZ3BTazJvaVE/view?usp=sharing)

It will need to be removed from circuit to be tested.



[http://circuits.datasheetdir.com/345/VISHAY-KBL005-pinout.jpg](http://circuits.datasheetdir.com/345/VISHAY-KBL005-pinout.jpg)


---
**Jon Bruno** *January 24, 2017 19:52*

And just as an FYI.. Transformers DO NOT retain a charge after power has been removed. 

Capacitors can if they aren't properly bled and fumbly people tend to be bitten by them. 



I think the idea that Flyback transformers retain a charge comes from people disassembling old CRT devices. 

The simple act of removing the Anode cap from an antique CRT would sometimes result in a tasty arc from a leftover charge built up in the tube. 

Since the anode runs to the high voltage transformer you can see why some tinkerers would think that it was caused by the transformer itself.



In order for ANY transformer to output current the magnetic field around the windings and core must be alternated or at the very least energized and then disconnected in order for the collapse of the magnetic field to induce a current in the windings. otherwise it's just a lifeless hunk of iron and copper.


---
**Jon Bruno** *January 24, 2017 20:20*

BTW, the Bridge rectifier in the schematic is the part listed as GBU808.


---
**Jon Bruno** *January 24, 2017 21:26*

The low voltage (24v rail) of the schematic is generated by the oscillations of the 5L0308. That's an integrated PWM controlled Mosfet. it turns on and off to create the input for the low voltage transformer. BE SAFE AND DO NOT TEST THIS COMPONENT!! There will be 300+VDC on it's input. this may be a bit too dangerous to play with if you're not familiar with proper safety and precautions in MAINS voltage level diagnostics.Best to test for 5v and 24v at the power supply termianls that feed the controller board. That will outrule the 5L0308 entirely.


---
**Madyn3D CNC, LLC** *January 25, 2017 19:48*

Wow Jon, I wish I could hire someone like you for this type of info but I can't afford that lifestyle. It forces me to learn, which I receive as a good thing. 



As far as HV & MAINS goes, no formal education whatsoever. I understand how it flows, I understand the concentration level of free electrons and how non-conductive surfaces may not be so "non-conductive" with a high enough concentration of electrons. I'm not a fan of HV at all and generally err on the side of caution, as I know it's potential. Generally, if I am forced to work with HV, I will do it standing on a plastic milk crate, a good 15" off the ground, just to be safe. 



You just taught me a lot, most importantly debunking the notion that a flyback transformer will hold a charge. Thank you for that and everything else, I am reading along very closely. 



My new PSU arrived and last night I installed it, so now I'm on to a completely different issue. I'm having serious arcing issues within the tube itself. I have not invested any research into this yet, as I just shut it down and went to sleep last night after discovering the problem. The C02 gas is certainly being excited because the purple beam inside is on, the ammeter is showing plenty of power which is being properly dictated by the potentiometer, but it's not producing any beam at all, won't even burn paper 1/2" away from the tube's lens no matter the power level. 



Whatever the problem may be, it cannot be any worse than the problems I just dealt with and got past over the last 3 weeks, so hopefully I'll have it running smoothly after a little reading today. That's my hope anyways!


---
**Jon Bruno** *January 26, 2017 15:14*

Well, my help is always free if you feel my advice offers anything useful.

I do hope you get it figured out. Safely.

Standing on plastic could help in some instances but if you touch a hot wire with one hand and the chassis or another part of the circuit where the current can flow with another , the plastic won't do much but keep your  fecal secretions from getting onto the floor.

Lol..



Always be careful.  And in regards to these K40 lasers,  These machines are inherently dangerous due to the lack of interlocks and shoddy design/quality control.

So try to stay out from under the blue covers if you can help it.

Do you have a video of your tube's behavior?

Also can you snap a photo of the sticker on your new LPSU?


---
**Madyn3D CNC, LLC** *January 26, 2017 16:49*

I'll be happy to shoot a video and get it uploaded today. Thanks again Jon, i have another tube if that's what I end up needing to do. It's a used tube with quite a few hours on it but it beats a blank. Hopefully it stored well over the last 2 months.


---
**Jon Bruno** *January 27, 2017 18:02*

I have two new ones in the closet that are going to go stale if you happen to wind up needing one.


---
**Madyn3D CNC, LLC** *February 20, 2017 20:05*

Hey Jon, I had a project going for the last 2 weeks that didn't involve the use of a laser, but I'm glad it's over with because I've been anxious to get back to troubleshooting this laser to hopefully get it sorted out. I went ahead and shot a video, describing the issue as best as I can. If I left out any crucial info just let me know. All electronics are K40 stock. I want to dive into smoothie or cohesion control, but I suppose I'd have to get it WORKING before I start thinking about that. After doing more reading, I'm beginning to think it's a bad tube. I noticed someone having similar issues on youtube but he was using anti-freeze in his water. I've always used distilled water in my closed-bucket system ever since I got the laser a year ago. Whenever you have time to check out the video, that would be much appreciated, and if it's the tube then I would certainly be interested in buying one of your extras. Thanks! 
{% include youtubePlayer.html id="gVpyHNwwy3o" %}
[https://youtu.be/gVpyHNwwy3o](https://youtu.be/gVpyHNwwy3o)


---
**Jon Bruno** *February 20, 2017 20:30*

Great video man.. Ive gotta say.. It's certainly not outputting as expected. In my honest opinion. it's behaving like it's a 220V LPSU on  110. I had a replacement unit that did the same thing.. at max position of the POT I was barely able to tickle the mA meter to 5mA

if it is in fact a 110V LPSU it may be out of adjustment. The tube is always suspect but if it was running fine up until the original power supply failed it may be good. most common failures that I've seen (and experienced) have been tube fractures due to thermal issues and then LPSU failure from excessive arcing.

WHat is the model # of the powersupply? can you confirm it's a 110V unit? does it have a voltage selector?

To get my 220V LPSU to properly function it required a jumper be installed in side to the bottom of the board.The design of the power supply was developed for both 110 and 220 markets and apparently they didn't install the jumper in mine.

That's China QC for ya,,


---
**Madyn3D CNC, LLC** *February 20, 2017 21:40*

Excellent info Jon, and thank you! The supply is a switchable supply (110/220) and it came with the switch set on 220, I had to switch it to 110. Maybe there's some on-board pot adjustments that need to happen when switched from 220 to 110? 



My father knows nothing about lasers, but well educated in electronics. He said that if I used a 220V source, the power supply would be doing less work and last longer?? I can only use a 110V source right now, but I did want to get a 220V source to the workshop sometime soon. 



Were the tune fractures visible to you when that happened? I'm assuming if it were a tube fracture I would have lost the Co2 gas and there would be no purple beam? 



Here's the model LPSU model is MYJG-40 220V/110V 40W. Here is the exact listing I purchased..  [http://www.ebay.com/itm/122118768022](http://www.ebay.com/itm/122118768022)


---
**Jon Bruno** *February 20, 2017 21:57*

Yes the glass had a visible crack and it did not make any plasma.. also you'd see water in the external envelope.

There are two trimpots on the board, one controls the low power regulator and the other controls the max mA threshold. What does your mA meter read when you test fire with the POT at max?


---
**Madyn3D CNC, LLC** *February 20, 2017 22:43*

I have not yet test fired the laser at max because my ground is not properly installed. I have a dedicated ground cable in the workshop that goes outside and to a rod 6ft in the dirt. But, while I have it upstairs to repair it, I'm just relying on the AC ground to test at low power, since my dedicated ground cable won't reach up here.



 I'll go ahead and give it a blast at 100%, I see plenty of K40 users using their lasers regularly, relying only on the AC ground on the wall socket. I'm sure I'll be fine. Soon as the wifey takes these kids off my hands I'm going to fire it up again and test fire at 100% while noting the analog mA reading.


---
**Madyn3D CNC, LLC** *February 21, 2017 01:06*

Ok I've got some values documented on video. I also installed a DVM to the ammeter to show 0-5V display. There does seem to be a significant loss in power because previously at 100% power on the dial, the ammeter would be maxed out. Now it's only reaching 20mA even with the pot at 100%.. here's another follow-up video...   
{% include youtubePlayer.html id="ua5nEHlPmAY" %}
[https://youtu.be/ua5nEHlPmAY](https://youtu.be/ua5nEHlPmAY)


---
**Jon Bruno** *February 21, 2017 15:06*

Ok.. so you don't want any more than 15-18mA to the tube as it will degrade the life of it. your old PSU was probably mis calibrated and allowed the max mA to go much higher than it should have. anyway.. ok you have plenty of power now. The fact that you have 0 emission out of the OC is the exact same issue that the other user has on the group.. You have outruled a power problem. there is something wonky with your laser tube.


---
**Madyn3D CNC, LLC** *February 21, 2017 19:03*

The old PSU could have very well been miscalibrated, as it really buried that needle a lot more than what I've seen from other k40 users on youtube. 



I have an old tube that still worked, but it's a real "bottom of the barrel" cheap ebay tube with many hours on it. Tonight I will install it just to see what happens. 



Needless to say, I'll be in the market for a tube ASAP. Those RECI tubes look interesting but I see many people having problems with the lens/metal cap mechanism. 



I can't afford anything fancy right this very moment, but something that will work for now. I want to stay away from ebay as both tubes I bought from ebay over the last year arrived broken.



 Light Object is the only thing coming to mind right now, or I'll be happy to work something out if you'd like to sell one of your spares. You have a good reputation here, and I have a paypal acct ;)


---
**Jon Bruno** *February 21, 2017 19:51*

Yes if you get a chance hook up the old tube and give it a whirl. even if it's weak, if it emits some radiation and burns paper you know where you stand.



Well,Thanks for the flowers .. I try to help when I can.. if that's the reputation you heard about then I'm grateful..

Interestingly enough, I have a local guy interested in my spares and says he'll be by to look them over this friday.. If anything falls through with that I'll set one aside for you.. I was somewhat against shipping them due to their fragile nature but if you get stuck and he doesn't take them I'd make the exception.


---
**Madyn3D CNC, LLC** *February 22, 2017 17:26*

Yea, I hear you on the shipping risks, these tubes seem to arrive at their destinations broken more often than not. Much better off with a local pick up and avoid that strong potential of damage. 



Can't thank you enough for the help. I'll have this junk tube hooked up today so I can just order one. There's not much on Amazon, I've already got 2 strikes on ebay getting broken tubes, I'll probably grab this one.. lightobject has a fairly decent reputation... [http://www.lightobject.com/SPT-35W-CO2-Sealed-Laser-Tube-for-Small-K40-Laser-Engraving-Machine-P208.aspx](http://www.lightobject.com/SPT-35W-CO2-Sealed-Laser-Tube-for-Small-K40-Laser-Engraving-Machine-P208.aspx)


---
**Jon Bruno** *February 22, 2017 21:09*

Well, hang tight until at least friday on pulling the trigger on an LO tube. Good luck with your old haggard tube.. If this guy fails to come through, I'd be fine with wrapping one up safely for you. 

We'll just put insurance on it so if it gets broken it's covered.






---
**Madyn3D CNC, LLC** *February 23, 2017 15:31*

Sounds good to me. I installed that tube last night and fired it up. Took about 10 mins adjusting the mirrors and it was cutting. So there was certainly something going on with the other tube, even though it was producing a nice, steady beam of plasma.. Perhaps I'll try and edit those videos together and film another now that the problem is solved. Maybe it will save some trouble for others in the future seeing as how these problems seem common with these K40's. 


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/eRCnuCCwB8c) &mdash; content and formatting may not be reliable*
