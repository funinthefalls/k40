---
layout: post
title: "What is going on. My laser came with LaserDrw and Corellaser"
date: November 27, 2015 09:40
category: "Discussion"
author: "Tony Schelts"
---
What is going on.  My laser came with LaserDrw and Corellaser. Corel laser is not very consistent.  If i open corellaser and open up a CDR file sometimes it will cut fine and others it head will hardly move.  I would prefer to use Corellasre because designing in corel draw is much easeier.





**"Tony Schelts"**

---
---
**Gary McKinnon** *November 27, 2015 11:26*

Corel Laser is the only plugin for Corel that works with laser draw so we're stuck with it, unless we change out our mainboard.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 27, 2015 11:28*

So far I've been using Adobe Illustrator/Photoshop to create my images I work with, then I import them into CorelLaser. I've had no troubles doing it that way. On occasion I have also saved the file as a CDR & reopened to do the same job again, with no issues also. Are you using the version of CorelDraw that was supplied (v12 if I recall correct from what I got)?



Also, I wonder if you can save the file in a different format, then import into CorelLaser, whether it does the same thing.


---
**Scott Thorne** *November 27, 2015 13:06*

Make sure in Corel laser that the m2 board is selected and not the m1 because it will cause some strange things to happen


---
**Tony Schelts** *December 01, 2015 15:49*

Talk about a slow response. I guess i didn't want to appear stupid what do you mean by the M2 board not M1

 I find that if I use Corel laser it is good for cutting items but then I i want to engrave it sucks. Laserdrw seems to wokr fine with the engraving.


---
**Gary McKinnon** *December 01, 2015 16:10*

**+Tony Schelts**    In the dropdown where you choose which model laser make sure you choose the M2 board if that's what you have.


---
**Scott Thorne** *December 01, 2015 16:52*

**+Gary McKinnon**​ is right, Corel draw selected the M1 board when I installed it, I had to change it to the M2 in the device settings.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/TyixmQXGBJX) &mdash; content and formatting may not be reliable*
