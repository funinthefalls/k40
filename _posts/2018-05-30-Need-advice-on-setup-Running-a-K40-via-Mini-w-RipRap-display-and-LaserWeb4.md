---
layout: post
title: "Need advice on setup. Running a K40 via Mini w/RipRap display and LaserWeb4"
date: May 30, 2018 16:32
category: "Hardware and Laser settings"
author: "Steven Whitecoff"
---
Need advice on setup. Running a K40 via Mini w/RipRap display and LaserWeb4.

First question is where should home be for LW4. My reference is facing the machine as in the masthead picture here. Should I leave the default layout in LW4 as is or use offsets?

Second is should I setup the same home on the RIpRap

Third, most times the machine ignores the home command in LW4.. using G28.2

Generally having trouble getting everything to agree...some projects seem to load outside of the work area, sometimes it wants to cut at negative coodinates...nothing consistant enough to say it does this or that...everytime I go to post about an issue, the next try its something else.





**"Steven Whitecoff"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 30, 2018 17:11*

Can you try using Lightburn?  The free trial is at the bottom of the page: [cohesion3d.com - Lightburn Software](http://cohesion3d.com/lightburn-software/)


---
**Steven Whitecoff** *June 01, 2018 20:12*

Ok, LB installed. It "seems" to work as in its talks to the Mini, will obey home command. Tried a cut off a DFX and it  just runs to the back limit after doing the funky chicken rattle. Same file cuts fine on LW4.


---
**Steven Whitecoff** *June 06, 2018 20:26*

Ray Kholodovsky ?


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2018 21:04*

I missed your reply, sorry. Did you select bottom left as the origin in LB? 


---
**Steven Whitecoff** *June 08, 2018 11:59*

yes, and  it homes fine. I'll have to get back to you next week, going camping/hiking


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/MuTKpZ2RHbZ) &mdash; content and formatting may not be reliable*
