---
layout: post
title: "Hello Friends, now i prepare my K40 and i start yesterday my first tests with 3mm PlyWood"
date: May 31, 2017 12:23
category: "Smoothieboard Modification"
author: "Frank Herrmann"
---
Hello Friends,



now i prepare my K40 and i start yesterday my first tests with 3mm PlyWood. But before i'checked my workflow cuz i love(!) Fusion 360. But ok ok, first some words to my K40 Setup:



I use **+Claudio Prezzi**'s changed grbl-lpc (original from **+Todd Fleming**) with separate PWM setting via $33=XXX Hz: [https://github.com/cprezzi/grbl-LPC/releases](https://github.com/cprezzi/grbl-LPC/releases) 

This works great, download, copy to sd card, start your laser, set some configs and ready to run. Optical Homing and other Features works very well on my laser with an C3D Mini Board from **+Ray Kholodovsky**.



So, my dream is to use the new laser cam in fusion 360 with my grbl. First of all i have to change the new grbl-laser post-processor that he support the PWM Settings in this firmware ($33), cuz i read an article that this settings is very important to cut different materials. 



[https://www.troteclaser.com/en-us/knowledge/tips-for-laser-users/laser-parameters-definition/](https://www.troteclaser.com/en-us/knowledge/tips-for-laser-users/laser-parameters-definition/)



"During the cutting process, the Frequency parameter is decisive and is given in Hz (=Hertz). It specifies the number of laser pulses per second. For a CO2 laser, the value can be set within a range of 1,000 to 60,000 Hz. For example, if you want to achieve a smooth edge when cutting acrylic, you need higher temperatures and thus this value is set to at least 5,000 to 20,000 Hz. On the other hand, when cutting wood a low frequency of 1000 Hz is necessary in order to achieve, for example, the brightest possible cutting edge."



Here my changed post-processor with this new setting in start gcode. I use a dwell line to make a short break to let grbl save this setting in his eeprom.

 

Fusion360 grbl-lpc Postprocessor:

[https://gist.github.com/xpix/6909e474e116bf67f77a795f4817d7ca](https://gist.github.com/xpix/6909e474e116bf67f77a795f4817d7ca)



(!) Please use the new Laser CAM Feature in Fusion:


{% include youtubePlayer.html id="c064tN3D6wE" %}
[https://www.youtube.com/watch?v=c064tN3D6wE](https://www.youtube.com/watch?v=c064tN3D6wE)



(!) Please use compensation type = 'Computer' at profile settings for laser.



To use in Fusion is very easy, also exists a UniversalGcodeSender plugin for F360. please check this github and watch the video:

[https://github.com/tapnair/UGS_Fusion](https://github.com/tapnair/UGS_Fusion)


{% include youtubePlayer.html id="Y7n3THx3L8Q" %}
[https://www.youtube.com/watch?v=Y7n3THx3L8Q](https://www.youtube.com/watch?v=Y7n3THx3L8Q)



So, on the last picture you see my first results. But after some testing i recognized that my stepper settings was wrong :) I.e. diameter of hole was 10mm and not 20mm, but this is an easy change. But please ignore the noted values on the wood pieces ... for curios guys ... its: PWM in Hz, Power in %, mm per minute.



So, please try to use this post-processor in F360 and send me ur opinion.



![images/6233e27d8dba70cc4b058e8d8f200ab6.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6233e27d8dba70cc4b058e8d8f200ab6.png)
![images/d1b3791832a08e0fab30ddeea942088e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1b3791832a08e0fab30ddeea942088e.jpeg)

**"Frank Herrmann"**

---


---
*Imported from [Google+](https://plus.google.com/+FrankHerrmann1967/posts/Sm2BeTsMcjs) &mdash; content and formatting may not be reliable*
