---
layout: post
title: "So a bit of help I have been cutting with my laser (finally)"
date: March 07, 2016 00:33
category: "Discussion"
author: "3D Laser"
---
So a bit of help I have been cutting with my laser (finally). And after spending hours making sure it is hitting dead center I am still getting beveled edges I adjusted the tube night the frame and everything and idea why my edges would still be beveled?





**"3D Laser"**

---
---
**Jim Hatch** *March 07, 2016 00:49*

Out of focus laser. You'll get anything from a full bevel to an hourglass type effect depending on where your laser is focused - top, bottom or middle. If it's severely pronounced then the beam may be hitting at an angle.


---
**Gee Willikers** *March 07, 2016 00:59*

As said above, your cutting is not centered at the focal point of the beam. Or you are still out of alignment - that happened to me recently - the give away was the bevel wasn't equal on every side of a piece. If your beam is centered the bevel should be equal all around.


---
**3D Laser** *March 07, 2016 01:21*

So is it a matter of adjusting the mirrors or the frame and tube again?


---
**Gee Willikers** *March 07, 2016 01:37*

If focus, distance from lens to center of part you are cutting. So up and down, usually 38.1mm or 50.8mm. I'd start there. Remember no cut will ever be perfectly square to the surface. The cut will always have a slight crown.


---
**3D Laser** *March 07, 2016 02:33*

Makes sense looks like I need to make my poor man z bed sooner rather than later 


---
**Gee Willikers** *March 07, 2016 02:39*

fwiw -- Screwing the machine down to a piece of 3/4" ply or similar helps flatten out the bottom and sturdy up the machine. I have the lightobject z-bed. Nice, but the work area is limited to about 12x7.5"


---
**Gee Willikers** *March 09, 2016 23:48*

**+Allready Gone** Do you know what focal length lens you have?


---
**3D Laser** *March 10, 2016 00:35*

50.8 according to the light objects description 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/F9VuoxsDkfS) &mdash; content and formatting may not be reliable*
