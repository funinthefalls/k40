---
layout: post
title: "I have started using Corel Laser as suggested on the forum, I start the Corel Laser program and load the work, and then click on the engrave tab on the far top right of the screen, as soon as the machine starts the engrave"
date: December 28, 2016 19:19
category: "Software"
author: "john dakin"
---
I have started using Corel Laser as suggested on the forum, I start the Corel Laser program and load the work, and then click on the engrave tab on the far top right of the screen, as soon as the machine starts the engrave icon tabs disappear and the program reverts to Corel Draw, is this normal as I cant find the engrave tabs unless I reload Corel Laser?





**"john dakin"**

---
---
**Ned Hill** *December 28, 2016 19:34*

It goes to the Windows tool bar at the bottom.  See pink icon.


---
**Ned Hill** *December 28, 2016 19:35*

Right click icon to see your options.  It's a quirk of the software. 


---
**john dakin** *December 28, 2016 19:52*

Oh Ok ive got it now, I'm running windows 10 pro and its in the hidden icons, Thanks very much for your help


---
**Matthew Wilson** *January 11, 2017 23:48*

where do I get corel laser? I bought the k40 and it only has laserdraw. I am trying to figure out how to design basic stuff in it, but it is extremely difficult.


---
**john dakin** *January 12, 2017 08:08*

Probably not the best program to design things in, I think you can download  corel Laser from there website, but you need your dongle for it to work, Laser DRW is very buggy and has a lot of problems, I stopped using it


---
**Ned Hill** *January 12, 2017 13:37*

**+Matthew Wilson** see this post for links from Yuusuf. [https://plus.google.com/101243867028692732848/posts/Edy6DPmZ6Ft](https://plus.google.com/101243867028692732848/posts/Edy6DPmZ6Ft)


---
**Matthew Wilson** *January 12, 2017 14:19*

Awesome! I got it now. Now the hard part in learning a new program.


---
*Imported from [Google+](https://plus.google.com/110405157088691728751/posts/ZmnBf8p81Bz) &mdash; content and formatting may not be reliable*
