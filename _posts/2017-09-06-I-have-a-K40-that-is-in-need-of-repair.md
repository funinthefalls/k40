---
layout: post
title: "I have a K40 that is in need of repair"
date: September 06, 2017 23:42
category: "Discussion"
author: "Tim Roberts"
---
I have a K40 that is in need of repair.  It needs a new tube, possibly a power supply, and while I'm at it I need a new driver board as the previous owner lost the USB key.

Does anyone have experience shipping replacement tubes into Australia?

Should I fix this one, or look at purchasing a new laser cutter in the next tier up?





**"Tim Roberts"**

---
---
**Phillip Conroy** *September 07, 2017 00:46*

How do you know it is needing new tube


---
**BEN 3D** *September 07, 2017 03:46*

I have not powered on my original k40 yet, but I read a lot. If I want to replace anything in my one, I would take a look to Laserweb and the supported hardware. There are two or three boards that are supported. The dongle is just for the original software. If you have the m2nano board inside, take a look to my collection, you could also use a open source software called... k40 Whisperer [https://plus.google.com/109140210290730241785/posts/3XWXziTzu8E](https://plus.google.com/109140210290730241785/posts/3XWXziTzu8E)



Take also a look here for a upgrade projekt 

[https://plus.google.com/109140210290730241785/posts/DYu72JTdRz1](https://plus.google.com/109140210290730241785/posts/DYu72JTdRz1)



And this for laserweb

[https://plus.google.com/109140210290730241785/posts/FRyhkCm6s45](https://plus.google.com/109140210290730241785/posts/FRyhkCm6s45)


---
**Tim Roberts** *September 07, 2017 06:47*

**+Phillip Conroy**, about a year ago I was using it and the output power of the laser was dropping.  I turned it off for a while, then tried again and had the same issue.  It turns out that it was overheating due to a short in the pump turning it into a heater.  I found this out by testing the temperature of the water, but zapped myself in the process as I was touching the chassis of the K40.

In the mean time I've been using the laser cutter at uni so haven't needed mine at home to be operational, but have come into some paid laser cutting work, so I can't really use the uni one anymore.

Currently I get loud clicking noises, similar to shorts, when I try to run the laser.

**+BEN 3D** yeah I've taken a look previously.  From memory though my K40 isn't running the m2nano board unfortunately.  I'll take another look once I get home today.  I'm contemplating getting a smoothieboard.

EDIT:  The control board in the K40 requires that I use Moshidraw 2012 so that it doesn't brick itself.  Had to fix that once already.


---
**Paul de Groot** *September 07, 2017 06:47*

I bought a recitube from ebay and got it delivered in 4days to Sydney. Also have a look at [awesome.tech - Inspiring science for hungry minds](http://awesome.tech) 


---
**Phillip Conroy** *September 07, 2017 07:53*

I have used the k40 for 2 years with stock board and coreldraw x7,upgraded to a sh -350 also called ebay blue and white,k50 ,it is around $1500 on eBay and is a real work horse,much better than k40,it has 500 mm  300 mm cutting bed,adjustable bed height,adjustability assist,red dot pointer,adjustability a controler that is miles ahead of the [k40.it](http://k40.it) can cut and etch in same go




---
**BEN 3D** *September 07, 2017 09:41*

**+Tim Roberts** the smoothie works with laserweb!


---
**Phillip Conroy** *September 07, 2017 18:36*

Paul who did you buy your new tube from 


---
**Paul de Groot** *September 07, 2017 23:42*

**+Phillip Conroy** i bought the tube from reci. Search online in ebay for that seller. Alternative is aliexpress seller cloudray but i have not brought a tube from them but bought other great laser things of good quality from them. Also they seem to communicate better than the average chinese seller.


---
**Phillip Conroy** *September 08, 2017 11:20*

I had a look on ebay and that seller looked doggy, read negative feedback, their are a lot of fake reci tubes,hope I am wrong ,that is why I brought my spt tube directly from spt website


---
*Imported from [Google+](https://plus.google.com/+TimRobertsSound/posts/TGd4uCXDUzF) &mdash; content and formatting may not be reliable*
