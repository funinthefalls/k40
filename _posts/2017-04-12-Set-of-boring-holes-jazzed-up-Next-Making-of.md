---
layout: post
title: "Set of boring holes jazzed up. Next 'Making of ...'"
date: April 12, 2017 10:02
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
Set of boring holes jazzed up. Next 'Making of ...' will show how I did this.

![images/5d06b1f729ff8603284372227f1540c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5d06b1f729ff8603284372227f1540c6.jpeg)



**"Ashley M. Kirchner [Norym]"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2017 10:45*

That's much nicer with the light etches to create an interesting woven effect. Slight criticism, the top 2 bits on the front just stop kind of oddly. I would consider joining them with a semi-circle or continuing them to the top of the front piece.


---
**Ashley M. Kirchner [Norym]** *April 12, 2017 11:04*

I did catch that too after staring at it for a bit. They got removed for the actual full cut tomorrow.


---
**Ashley M. Kirchner [Norym]** *April 12, 2017 11:05*

There were similar artifacts on the bottom side too that I removed. I just missed those at the top.


---
**Ned Hill** *April 12, 2017 12:50*

That's a very cool effect.


---
**Steve Anken** *April 12, 2017 13:58*

Cool pattern. Here's a CNC version. I got it from the wonderful web site Craftsmanspace.com.

[scontent-lax3-1.xx.fbcdn.net](https://scontent-lax3-1.xx.fbcdn.net/v/t1.0-9/15219609_691367501034235_3731303677343081475_n.jpg?oh=d5ffee36cc28ca649e5d75724d5ec2a3&oe=5998A38E)




---
**Ashley M. Kirchner [Norym]** *April 12, 2017 16:02*

Yeah, I thought I'd try adding in a raster gradient but in the end I opted not to. Just takes too long. 


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/7DDEVXKknpt) &mdash; content and formatting may not be reliable*
