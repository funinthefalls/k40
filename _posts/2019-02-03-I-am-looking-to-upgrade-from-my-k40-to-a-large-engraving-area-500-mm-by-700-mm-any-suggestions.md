---
layout: post
title: "I am looking to upgrade from my k40 to a large engraving area 500 mm by 700 mm any suggestions"
date: February 03, 2019 19:02
category: "Discussion"
author: "William Rhodes"
---
I am looking to upgrade from my k40 to a large engraving area 500 mm by 700 mm  any suggestions





**"William Rhodes"**

---
---
**Anthony Bolgar** *February 03, 2019 19:38*

I would look on ebay for a laser that has a Ruida controller. You can get 60 or 80W versions for around $1500-$2500


---
**Anthony Bolgar** *February 03, 2019 19:39*

Also, this forum is moving to [forum.makerforums.info](http://forum.makerforums.info), you can sign up and start posting there as well.


---
*Imported from [Google+](https://plus.google.com/108124513848096303746/posts/ZjMZPcYWLcT) &mdash; content and formatting may not be reliable*
