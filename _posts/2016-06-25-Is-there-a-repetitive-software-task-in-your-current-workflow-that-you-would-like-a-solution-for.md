---
layout: post
title: "Is there a repetitive software task in your current workflow that you would like a solution for?"
date: June 25, 2016 19:28
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Is there a repetitive software task in your current workflow that you would like a solution for? 





**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 21:26*

Sometimes I have to resend the entire cut/engrave etc to the engraver as I have not managed to cut all the way through in 1 pass (as I tend to use low power settings). With the stock software it's time consuming. Maybe something like a "Redo" or "Resend" button would be great, but from my limited understanding of LW it seems like you can just hit "Send" again to do this. Other than that I can't think of anything repetitive in my workflow.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/UnwDZ4cozS2) &mdash; content and formatting may not be reliable*
