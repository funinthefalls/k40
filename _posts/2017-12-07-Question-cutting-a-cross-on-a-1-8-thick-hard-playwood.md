---
layout: post
title: "Question: cutting a cross on a 1/8 thick hard playwood"
date: December 07, 2017 12:47
category: "Hardware and Laser settings"
author: "Mike Gallo"
---
Question: cutting a cross on a 1/8 thick hard playwood.  Setting to cut at 8 speed 19.2 laser  power , well look at image , some parts get cut clean some not and burn the back. 

Any suggestion to cut it.  Laser tube 80 watt.





![images/829bbd2a9c4ee7c45fe274cb3fcbde44.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/829bbd2a9c4ee7c45fe274cb3fcbde44.jpeg)
![images/b4ca0af8cfb18e16272a6db67def4328.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b4ca0af8cfb18e16272a6db67def4328.jpeg)

**"Mike Gallo"**

---
---
**HalfNormal** *December 07, 2017 12:57*

That would be due to inconsistencies in the wood. 

See this post

[plus.google.com - What makes a plywood good for laser cutting? http://n-e-r-v-o-u-s.com/blog/?...](https://plus.google.com/+HalfNormal/posts/6HfPzAdxZ8n)


---
**Mike Gallo** *December 07, 2017 13:11*

Thank you, I didn't know about metal in the glue, any brand of wood I should look for and store.  I see lots spark while cutting 1/8 and on some 1/4 playwood too.

I buy it at homedepot. 


---
**HalfNormal** *December 07, 2017 13:16*

Search the forums and find out who people buy from. I know there's a lot of suggestions for people on eBay and Amazon that have good product for laser cutting


---
**Mike Gallo** *December 07, 2017 13:19*

Thank you, I will do. 


---
**Chris Hurley** *December 07, 2017 15:08*

I had the same problem and it turned out my bed was not level to the cutter head. It got worse as I walked towards one corner. 


---
**Joe Alexander** *December 07, 2017 15:10*

it could also be alignment, but can it always be alignment? :P


---
**Mike Gallo** *December 07, 2017 16:05*

I don't think it's alignment because I test nine different spot and cut a 1" square 1/8 thick  , clean cut.  


---
**Joe Alexander** *December 08, 2017 06:31*

just re-read orig post: was that 8mm/s@19.2ma or 19.2%? and if percent then what value=100%?


---
**Mike Gallo** *December 08, 2017 11:58*

It's 19 not .2 phone auto correct to something I type early.  I use rdwork 8 , I don't have an amp meter attach to laser yet .


---
*Imported from [Google+](https://plus.google.com/108921378497443668729/posts/1NHknyrmSip) &mdash; content and formatting may not be reliable*
