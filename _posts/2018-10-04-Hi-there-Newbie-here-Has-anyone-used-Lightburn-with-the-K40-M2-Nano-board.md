---
layout: post
title: "Hi there, Newbie here. Has anyone used Lightburn with the K40 M2 Nano board ?"
date: October 04, 2018 15:10
category: "Software"
author: "Neil Adams"
---
Hi there, Newbie here.

Has anyone used Lightburn with the K40 M2 Nano board ?

If so, may I ask what you set the board as ..EG: Marlin ?



Cheers

NGA 





**"Neil Adams"**

---
---
**HalfNormal** *October 04, 2018 15:38*

K40 whisperer is the only third party software that works with the M2 nano.


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 15:43*

If you want to use LightBurn with your K40 you may consider upgrading the controller with a Cohesion3D Mini. it is designed to replace the M2 Nano board that ships with most K40's. it installs in 10 minutes, usually without any modification. It is fully compatible with LightBurn, runs Smoothie & GRBL, and gives you PWM laser power control which is not possible with the stock board. 



[http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Justin Mitchell** *October 05, 2018 11:34*

M2 Nano uses a proprietary closed protocol to talk to the host, hence the shitty chinese software. Afaik K40-whisperer is the only project to have reverse engineered that protocol and provided alternative software, anything else requires you to replace the board with something more open standards.




---
**Neil Adams** *October 12, 2018 12:33*

Thanks to all.

I have ordered a Co-Hesion 3D Mini now.

Appreciate the advice

Dr N


---
*Imported from [Google+](https://plus.google.com/108512725933584531524/posts/JyZGWEcu9jp) &mdash; content and formatting may not be reliable*
