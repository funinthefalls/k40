---
layout: post
title: "Smoothieboard or TinyG? I have a TinyG already that I've been considering pulling off my CNC in favor of an Arduino/GRBL setup"
date: August 23, 2016 19:06
category: "Discussion"
author: "Carl Fisher"
---
Smoothieboard or TinyG?



I have a TinyG already that I've been considering pulling off my CNC in favor of an Arduino/GRBL setup. That would free up the TinyG for use in the Laser.



However I'm not against Smoothie or something else if there is a better option at the same price point. 



Does LaserWeb support TinyG and can I control the laser power with the TinyG for things like grayscale engraving? Or would the community support be better around the Smoothie to get where I want to go with this machine?



Thoughts?





**"Carl Fisher"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 23, 2016 19:26*

LW does not support tinyg. 

Since I make a board that runs smoothie that is a direct upgrade for the k40, I have to say: Smoothie Smoothie Smoothie. 


---
**Carl Fisher** *August 23, 2016 19:28*

Well that narrows it down a bit :) 




---
**Carl Fisher** *August 23, 2016 19:30*

**+Ray Kholodovsky**  any more info on your board?


---
**Ray Kholodovsky (Cohesion3D)** *August 23, 2016 19:38*

**+Carl Fisher** Picture here and description in the comments: [https://plus.google.com/u/0/111302122377301084540/posts/D7uGvHBV333](https://plus.google.com/u/0/111302122377301084540/posts/D7uGvHBV333)


---
**Carl Fisher** *August 23, 2016 22:43*

**+Ray Kholodovsky** Do you have some contact info? I'd like to discuss this board with you outside of this thread.  Thanks.


---
**Ray Kholodovsky (Cohesion3D)** *August 23, 2016 22:54*

**+Carl Fisher** Hangouts, just started a chat with you.


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/8vnsmCtdJCH) &mdash; content and formatting may not be reliable*
