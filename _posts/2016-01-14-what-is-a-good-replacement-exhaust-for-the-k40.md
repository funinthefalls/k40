---
layout: post
title: "what is a good replacement exhaust for the k40"
date: January 14, 2016 03:40
category: "Hardware and Laser settings"
author: "3D Laser"
---
what is a good replacement exhaust for the k40





**"3D Laser"**

---
---
**Jim Hatch** *January 14, 2016 04:28*

I haven't decided whether to replace the exhaust yet. The blower it came with should be able to replace the air in the box several times per minute. I think it can't for 3 reasons - first the housing doesn't fit well into the slots on the back and it's bigger than the cutout so air leaks around the edges.



I fixed that by getting rubber weather stripping from Home Depot - the kind with a sticky back - and stuck it all around the housing. Now it's a snug fit against the back of the machine all the way around.



Second issue is what I think is a constricted air intake going into the exhaust blower. Mine is only 8 square inches. I'm going to remove it to open up the whole of the exhaust housing hole to airflow. 



The third issue is that even if you're able to pull lots of air out, there isn't a good source of replacement air. My machine has just a small 2 inch hole on the bottom of the machine under the bed. It can also pull air in from around the lid, but it seems that replacement air needs to be boosted. I've got 4 20cfm fans I'm going to attach to the front (the grills came in today). These will be mounted behind holes I'll cut in the front of the housing. With 80cfm being sucked into the machine I figure I'll have taken care of the intake side of the flow. I just need some kind of filter media so I don't have errant laser reflections bouncing out past the fans. I have to hit HD to see what they've got.



I'll post pictures once I finish the mod.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 14, 2016 06:09*

**+Jim Hatch** What if you had your intake fans blowing the air into the case via some pipe? So instead of there being a direct hole in the exterior of the case, it would actually be a pipe (with say a 90 degree bend) so no errant laser reflections can escape?


---
**Pete OConnell** *January 14, 2016 12:07*

I did think about putting in fans on the front but my concern was the airflow would affect the position of the material I was cutting. What about simply putting in more air vents to allow air to be pulled in rather than pushed in. This way the fan on the back will be able to perform as well as it can (which I don't think is that great!)


---
**Jim Hatch** *January 14, 2016 14:37*

**+Pete OConnell**​ I don't think the front intake airflow will disturb the material unless perhaps it's paper. I'm basing this on my experience with the large 60W laser I use in my local MakerSpace which has intake fans mounted on the front housing too. It's also why I picked 4 smaller fans rather than 1 large one. I'm looking for volume with low pressure.



**+Yuusuf Sallahuddin**​ I thought about piping it in from the side under the control panel but that adds airflow restrictions again. I believe the straight through airflow even with filter media will suffer less restriction without adding turbulence to the air stream that seems likely with a piped stream.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/ZnD3pFX42E1) &mdash; content and formatting may not be reliable*
