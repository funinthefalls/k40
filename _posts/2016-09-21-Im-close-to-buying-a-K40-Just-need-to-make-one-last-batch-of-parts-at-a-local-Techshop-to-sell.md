---
layout: post
title: "I'm close to buying a K40. Just need to make one last batch of parts at a local Techshop to sell"
date: September 21, 2016 23:52
category: "Hardware and Laser settings"
author: "Bill Keeter"
---
I'm close to buying a K40. Just need to make one last batch of parts at a local Techshop to sell. 



Anyway, what's a "trusted" K40 US seller that people have had good luck with? Ebay or Amazon?



Also what's the max bed size you can get out of K40? I've seen 320x220? Anyway, to squeeze out another inch or two? Maybe replace the rail frame with extruded aluminum?





**"Bill Keeter"**

---
---
**Alex Krause** *September 22, 2016 00:01*

**+Ariel Yahni**​


---
**Ariel Yahni (UniKpty)** *September 22, 2016 00:10*

**+Bill Keeter** you mostly going to have good luck with high rated sellers. You can squeeze a little more by removing some stuff but not much. Im changing my whole gantry to openbuilds. hope to finish this week


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 22, 2016 05:07*

Once I've done my controller upgrade (to Smoothieboard & ACR) I've managed to squeeze 330 x 230mm without much effort.


---
**Scott Marshall** *September 22, 2016 10:48*

Just ordered my 2nd one from Globalfreeshipping. The 1st came in about last December and was banged up, but GFS did me right, and now that I need another laser, that's where I went.



GlobalFreeShipping on Ebay. Ship from Nebraska and California. 


---
**Bill Keeter** *September 22, 2016 16:41*

**+Alex C** Thanks for the ebay listing. I'm sending an offer now. Man, I was expecting to pay over 400. Now if I can get it that cheap I'll be able to buy a smoothieboard + LCD that much sooner. Awesome.


---
**Ariel Yahni (UniKpty)** *September 22, 2016 16:45*

Also remember **+Scott Marshall**​ has a solution for easy upgrade if you need it


---
**Bill Keeter** *September 23, 2016 15:29*

**+Ariel Yahni** I saw Scott's upgrade PCB, but i'm confused. Is it necessary for the smoothieboard? Does it do more than a pass through? I was under the impression I could just transfer the old wires from the existing K40 motherboard to the smoothie.


---
**Ariel Yahni (UniKpty)** *September 23, 2016 15:46*

**+Bill Keeter**​ there is a link to that upgrade under my collection folder in G+. But I will say it's all depends on the user ability and type of connectors.  Some units come with a ribbon that need a middle board. 


---
**Scott Marshall** *September 23, 2016 15:51*

**+Bill Keeter** 

You can Bill, but you do need a level shifter for the Laser firing line.



 The smoothie outputs 3.3v logic and the K40 PSu requires 5V negative logic (0v to fire, 5v to shut down) If you connect it directly, you only provide a 3.3v shutdown signal, and it;s not a safe practice.



The level shifters are easy to make (if you need a diagram- email and I'll send you one) - The Cheap Chinese boards seem to work well, get the one with the FETs on it, - they're only a couple of bucks.



My ACR board makes it a plug in affair to install a Smoothieboard, and is available as a Plug and Play module. 

It was originally targeted at non-technically oriented people who wish to upgrade and don't want to attempt the electronics. 



A large number of the kits I'm selling, however are to skilled and experienced people who recognize it's faster, easier, much less risky, and probably cheaper to use the kit than to scratch assemble your own.



One fellow was working for about 2 weeks to get his scratch conversion going, and immediately upon getting it running, ordered one of my kits. He didn't like the messy wiring and wanted to "neaten it up".



That's a good point I hadn't thought of myself. 



I was answering this question a couple of days ago, and, after thinking about it, I said If I wasn't selling them, I'd buy one. It's true.



I'm not trying to displace the do-it yourselfers, (I am one) and I, in fact, sell the board as a kit for u-builds, and will freely help anyone doing their own conversion. 



It seems there's plenty of people for whom the board was originally intended to keep me busy (and I've got several new products in the pipeline to boot)



Scott


---
**Scott Marshall** *September 23, 2016 16:22*

**+Alex C** You have mail. Any questions, drop me a note.



Scott


---
**Bill Keeter** *September 23, 2016 18:56*

**+Scott Marshall** Thank you. Generally I like to make stuff myself. A couple scratch built 3d printers attest to that. BUT at the same time I don't mind paying a little extra to someone to avoid unnecessary hassles. I've got more than enough projects already to keep me busy. lol.



So yeah, i'll probably buy one of your full kits in a few weeks. Once I have the K40 in hand. Also do you mind sharing some photos of the two boards wired together? Your website only has the one pic. 


---
**Scott Marshall** *September 23, 2016 22:13*

Can't add them here (such is this forum), but I will put up a post with some beauty shots of the ACR assembly. Rough dimensions are 8" w, 6" H and 3" deep. The assembly mounts on the panel where the M2nano sub-panel is mounted (you remove the whole assembly and install the ACR module right to the front using an included template and 2 screws.



I've been meaning to upgrade the website, Yuusuf does a great job with it, but I'm bad about squeezing in time to take photos and he doesn't have a lot to work with.



Scott



If you send me your email, I can send you the prelim installation manual, it's a work in progress, but it's loaded with info and even some photos.



 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 24, 2016 02:31*

**+Bill Keeter** Here is some pics on my Google Spaces where I have Scott's ACR wired to my Smoothieboard:

[https://spaces.google.com/space/379777357/post/UgwSRErqWVVGmMaJKKt4AaABBw](https://spaces.google.com/space/379777357/post/UgwSRErqWVVGmMaJKKt4AaABBw)



Actually they're photos from when I was verifying which wires go where. So maybe not hugely helpful.


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/XFpQY3KVUFe) &mdash; content and formatting may not be reliable*
