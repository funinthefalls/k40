---
layout: post
title: "Does anybody have a source for a new Y-axis stepper motor?"
date: November 02, 2017 05:46
category: "Original software and hardware issues"
author: "Deren Ash"
---
Does anybody have a source for a new Y-axis stepper motor? I can't find anything that's dual shaft, 0.9 degree steps (400 steps/rev), and 4-wire.





**"Deren Ash"**

---
---
**Anthony Bolgar** *November 02, 2017 06:22*

If you can get the model number of the dead stepper motor, it would help in searching for a replacement.


---
**Deren Ash** *November 02, 2017 06:31*

**+Anthony Bolgar** The model number on the sticker is 42-2100C08S/SH.  Unfortunately when I Google that I don't find anything.


---
**Anthony Bolgar** *November 02, 2017 06:49*

Edit:sorry was 1.8deg



Amazon has this one that should fit the bill:

[amazon.com - Robot Check](https://www.amazon.com/NEMA17-Shaft-83oz-Bipolar-Stepper/dp/B00NZPEGZW)


---
**Anthony Bolgar** *November 02, 2017 06:51*

This one is correct:

[amazon.com - Amazon.com : Vexta PK Series Hi Resolution (0.9deg/step) NEMA 17 Stepper Motor (PK243M-03BA) : Camera & Photo](https://www.amazon.com/Vexta-Resolution-0-9deg-Stepper-PK243M-03BA/dp/B00P0WU526)


---
**Deren Ash** *November 02, 2017 07:04*

**+Anthony Bolgar** Thanks!  I just ordered that one.  I'll update this thread when I receive it and install it.


---
**Anthony Bolgar** *November 02, 2017 07:11*

Glad I could help.


---
**Deren Ash** *November 02, 2017 09:10*

Ok, I measured the physical motor in the laser and compared to the spec sheet.  The one that I ordered is smaller.  I think I can 3D print a coupler to account for that, however, I'm wondering if that also means the lower current rating will be doable.  The version that matches the size of what's in the laser (but of course isn't so available) can handle over double the current.  Is 0.22A going to be enough, or will it burn out the motor?  I will of course find out by trial and error when it arrives, but until then...


---
**Anthony Bolgar** *November 02, 2017 14:12*

I think it should be OK, the steppers do very little work in the K40's


---
**Joe Alexander** *November 02, 2017 15:32*

the y axis motor doesnt take much to get going as it doesnt have to move as fast as the x one does(at least for rastering). I bought some 17HS8401B as backups replacements, very quiet and effective. I already replaced my x motor with a 17HS13-0404S and its crazy quieter than the original! Hope that helps :)


---
*Imported from [Google+](https://plus.google.com/109581708182277964430/posts/GNyUDraGMm5) &mdash; content and formatting may not be reliable*
