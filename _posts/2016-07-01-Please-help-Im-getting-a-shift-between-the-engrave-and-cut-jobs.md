---
layout: post
title: "Please help.. I'm getting a shift between the engrave and cut jobs.."
date: July 01, 2016 14:25
category: "Original software and hardware issues"
author: "Chris Boggs"
---
Please help..  I'm getting a shift between the engrave and cut jobs..  I have tried combining engrave and cut jobs together and tried separately..  Unchecked the print icon in the layers menu..  All prints perfectly except the cut layer is offset..  Anyone seen this?  Is it a simple fix?  It doest matter if I do single cut/engrave job or if I try multiple..  Thanks in advance  Chris... 



![images/ad1b8c8e0558968578e4dad67ac9e734.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad1b8c8e0558968578e4dad67ac9e734.jpeg)
![images/80a981edeb019da1a077cf697a235e27.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/80a981edeb019da1a077cf697a235e27.jpeg)
![images/1de314bbfc26378fd34d066359291215.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1de314bbfc26378fd34d066359291215.jpeg)

**"Chris Boggs"**

---
---
**Tev Kaber** *July 01, 2016 15:01*

The fix I've used is to use a registration dot in the upper left that both the engrave and cut jobs include.  It's kind of a hack but does the trick.  There's probably a better way...


---
**Evan Fosmark** *July 01, 2016 15:55*

That's how I do it, too. I make a new layer called "corner", and place a small circle in the top left just above the working area. Ensure you always "print" the corner layer.



It's an unfortunate consequence of CorelLaser trying to line up the work with the 0,0 coordinate.


---
**Chris Boggs** *July 01, 2016 17:29*

Yeah!!!  That did it..  Thanks for your rapid replies..  Love this group..  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 01, 2016 21:51*

Alternatively, I place everything inside a rectangle 300x200mm. The rectangle has no border & no fill, it's just an alignment box. Then I align all the objects where they need to go in that rectangle & group them. Both engrave jobs & cut jobs have corresponding rectangles to keep the items aligned with the other job.


---
**Ben Marshall** *July 04, 2016 01:39*

I don't use a dot. In print preview of your layers, check to make sure your work aligns with the upper left corner(blue arrow in corel indicates where the work corner is). In my experience text boxes or any other object that takes up space outside your work area will cause this to happen. Use process of elimination to find out where the problem is. Delete text objects first one at a time, go to print preview and see if your alignment issues are solved. Undo delete if not. Again, anything that minutely falls out of your workspace will cause the program to shift it in order to fit. 


---
**Tev Kaber** *July 04, 2016 21:06*

**+Yuusuf Sallahuddin** That invisible rectangle trick works great! Thanks!


---
*Imported from [Google+](https://plus.google.com/101316203748949165087/posts/NXxhtkznmfC) &mdash; content and formatting may not be reliable*
