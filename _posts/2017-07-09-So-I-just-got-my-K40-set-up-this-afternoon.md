---
layout: post
title: "So I just got my K40 set up this afternoon"
date: July 09, 2017 06:12
category: "Software"
author: "Drew Jeffries"
---
So I  just got my K40 set up this afternoon.  I just say that I am not at all impressed with Laserdrw. It is horrible.  Is there any other software that I can use on this machine that might be more user friendly?





**"Drew Jeffries"**

---
---
**Anthony Bolgar** *July 09, 2017 07:37*

The only way to use other software is to replace the controller card with either a grbl or smoothieware based controller. Then there are a few different open source options like LaserWeb and Visicut.


---
**Ashley M. Kirchner [Norym]** *July 09, 2017 09:36*

Most users don't use LaserDRW directly, but rather they use CorelDraw with the supplied CorerLaser plugin. That being said, I never bothered with CorelDraw nor the plugin. I created my artwork in Illustrator or Photoshop, exported as a bitmap and imported that into LaserDRW. 



I have since then replaced the board for a Cohesion3D Mini and now use the open source LaserWeb interface. Still do all my design work in Illustrator and Photoshop. 


---
**Drew Jeffries** *July 09, 2017 20:12*

I am engraving this for my daughter's room. If I want to basically outline the letters will telling the laser to cut the letters instead of engraving them only have it cut the outside? If so plan on having it cut really fast as not to go all the way through

![images/943a60d5fefabaeb518b4827f9089694.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/943a60d5fefabaeb518b4827f9089694.jpeg)


---
**Drew Jeffries** *July 09, 2017 20:13*

Thank you for the responses also!!!


---
**Anthony Bolgar** *July 09, 2017 21:06*

Increase the speed and reduce the power level




---
**Ashley M. Kirchner [Norym]** *July 09, 2017 23:26*

I call those a 'kiss cut' where it burns what it normally be cutting through... usually I run them at 3500mm/s at 1/4 power.


---
*Imported from [Google+](https://plus.google.com/113629684760903660746/posts/XYwBzpLdgR1) &mdash; content and formatting may not be reliable*
