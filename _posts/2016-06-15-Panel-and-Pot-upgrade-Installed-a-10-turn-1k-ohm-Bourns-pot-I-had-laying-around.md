---
layout: post
title: "Panel and Pot upgrade Installed a 10 turn 1k ohm Bourns pot I had laying around"
date: June 15, 2016 23:52
category: "Modification"
author: "HalfNormal"
---
Panel and Pot upgrade



Installed a 10 turn 1k ohm Bourns pot I had laying around. It has a nice digital readout. I did test at each 50 mark setting after 100 and posted it under the acrylic. Not sure if I will do anything else due to possibly updating the Nano board. Might add an illuminated switch for the laser on/off button. Was thinking of using a LED but the power logic is reversed and would have to build a circuit just to light it. 



![images/28936a2527b53d8169b3323ce0cd0860.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/28936a2527b53d8169b3323ce0cd0860.jpeg)



**"HalfNormal"**

---
---
**Alex Krause** *June 16, 2016 00:18*

Man that's a sweet pot


---
**Ariel Yahni (UniKpty)** *June 16, 2016 00:27*

I need that POT


---
**HalfNormal** *June 16, 2016 00:50*

**+Ariel Yahni** **+Alex Krause** I hate to tell you that it is not a cheap pot. I only have it from some old equipment I used to work on. New they run around a $100!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 16, 2016 04:55*

Wow $100 for the pot. It is a fabulous pot though.


---
**Akos Balog** *July 01, 2016 09:10*

I think this would work the same, what do you think? [http://www.ebay.com/itm/With-Turn-Counting-Dial-1K-Ohm-3590S-2-102L-Rotary-Potentiometer-Pot-10-Turn-/110968869720?hash=item19d6429358:m:m1IYYSqWH-yZpJN3S2CjoPA](http://www.ebay.com/itm/With-Turn-Counting-Dial-1K-Ohm-3590S-2-102L-Rotary-Potentiometer-Pot-10-Turn-/110968869720?hash=item19d6429358:m:m1IYYSqWH-yZpJN3S2CjoPA)



What power of potentiometer do we need?


---
**Ariel Yahni (UniKpty)** *July 01, 2016 11:36*

Nice find **+Akos Balog**​


---
**Akos Balog** *July 01, 2016 11:52*

**+Ariel Yahni** I've searched, and similar pot can be found at any shop selling electronic parts. You need a 1k Ohm, 2w, 10 turn potentiometer, and a matching counting dial knob. 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/d8b1MgV7A1J) &mdash; content and formatting may not be reliable*
