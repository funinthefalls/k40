---
layout: post
title: "Hello all. I'm rather sure I am in the wrong place but I'm lost in general so that part seems about right"
date: October 04, 2016 23:38
category: "Software"
author: "LaShaundra H"
---
Hello all. I'm rather sure I am in the wrong place but I'm lost in general so that part seems about right. I have a K40 laser from eBay (who'd have thunk?) and when I started (just a about 5 months ago), I used, CorelDraw X5 and CorelLaser...then the blasted thing started telling me that I didn't Corel installed so I put it down for 2 months. Moved on to purchase CorelDraw X7 and that didn't fix my issue as apparently, I purchased the Home & Student Suite, not the Technical Suite....now I'm relegated to using LaserDRW 3. MY question is how do I import 2 separate files into LaserDRW? One as a cut layer, the other as an engrave layer...I got it to work once but apparently that was a fluke because I cannot replicate it. Any help is greatly appreciated.





**"LaShaundra H"**

---
---
**Ric Miller** *October 05, 2016 15:30*

I have never been able to get it to work. I just do it in two files. This is another reason I decided to upgrade to a smoothieboard and laserweb3. 



I should get my board soon and will post me results from the upgrade. 


---
**Mike Cahill** *December 10, 2016 21:05*

I delete the cut elements, do the engraving, then undelete the cut elements, delete the engrave elements, and do the cuts, it works well




---
*Imported from [Google+](https://plus.google.com/101822957178103560073/posts/QwZcirGWCY1) &mdash; content and formatting may not be reliable*
