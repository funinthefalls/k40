---
layout: post
title: "This is an Invitation we done on the K40, works fine on a coloured card but not so well on white"
date: April 14, 2016 07:36
category: "Discussion"
author: "Norman Kirby"
---
This is an Invitation we done on the K40, works fine on a coloured card but not so well on white



![images/758beb589b74cee420e0092a24728841.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/758beb589b74cee420e0092a24728841.jpeg)
![images/3a6d0339f20e3089558c95fee6a2808a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3a6d0339f20e3089558c95fee6a2808a.jpeg)

**"Norman Kirby"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 09:23*

Ah yeah, the edge burns are very subtle. Not so bad in my opinion & the entire piece looks pretty nice.


---
**Tony Schelts** *April 14, 2016 12:53*

Very Nice

Are you willing to let the patterns out.  A friend has asked me to see if I can do someting for her Wedding.


---
**David Cook** *April 19, 2016 00:56*

wow, intricate.  very nice


---
*Imported from [Google+](https://plus.google.com/103798744920995011573/posts/YqDg7dqbtAR) &mdash; content and formatting may not be reliable*
