---
layout: post
title: "A refrigerator magnet for my granddaughter"
date: July 21, 2017 02:41
category: "Object produced with laser"
author: "HalfNormal"
---
A refrigerator magnet for my granddaughter

![images/0f8e9c5d323787ae8ccfd82eae335809.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f8e9c5d323787ae8ccfd82eae335809.jpeg)



**"HalfNormal"**

---
---
**Ned Hill** *July 21, 2017 03:10*

Nicely done :)




---
**HalfNormal** *July 21, 2017 03:11*

I have a good source of inspiration!


---
**Don Kleinschnitz Jr.** *July 21, 2017 03:15*

Nice I did not know wood was magnetic... perhaps just in Arizona?


---
**Ned Hill** *July 21, 2017 03:18*

**+Don Kleinschnitz** induced by his magnetic personality no doubt 😀


---
**HalfNormal** *July 21, 2017 03:20*

**+Don Kleinschnitz** It is made out of iron wood! ;-)


---
**Ned Hill** *July 21, 2017 03:21*

Reminds me that I recently added a couple of calendar mags to my collection.  One for the 4th of July and a couple of airplanes since my daughter went on a trip to TX recently. ![images/cfb7ad9bfb2a710343556eb6c4a79683.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cfb7ad9bfb2a710343556eb6c4a79683.jpeg)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/59VkUqHP27X) &mdash; content and formatting may not be reliable*
