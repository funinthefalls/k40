---
layout: post
title: "A question for those who have worked with Living Hinges before"
date: March 28, 2016 10:03
category: "External links&#x3a; Blog, forum, etc"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
A question for those who have worked with Living Hinges before.



Is there a minimum curve size for bending 180 degrees?



I intend to make something out of 1/8 ply (or 3mm) & I am curious as to how small the curve can be. Also I am curious as to how far around the curve area the living hinge cut lines are required to go?

![images/a5f380e85eea29e3114d0ea2ecb9eb32.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a5f380e85eea29e3114d0ea2ecb9eb32.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Mishko Mishko** *March 28, 2016 11:24*

I guess that depends greatly on the density of the cuts (links), kerf and thickness, and of course the material itself.  This guy made quite a science of lattice hinges:  [https://www.deferredprocrastination.co.uk/blog/category/def-proc/lattice-hinges/](https://www.deferredprocrastination.co.uk/blog/category/def-proc/lattice-hinges/)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 28, 2016 11:41*

**+Mishko Mishko** Thanks Mishko. I'll take a look at that.


---
**Stephane Buisson** *March 28, 2016 15:57*

**+Yuusuf Sallahuddin** 

[https://www.deferredprocrastination.co.uk/blog/2011/laser-cut-lattice-living-hinges/](https://www.deferredprocrastination.co.uk/blog/2011/laser-cut-lattice-living-hinges/)



oups! sorry too late so I post this video too (still learning myself)




{% include youtubePlayer.html id="JSlRusl7UPc" %}
[https://www.youtube.com/watch?v=JSlRusl7UPc](https://www.youtube.com/watch?v=JSlRusl7UPc)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 28, 2016 16:13*

**+Stephane Buisson** Thanks Stephane. Yep it seems you were too late, but better late than never.


---
**Stephane Buisson** *March 28, 2016 16:17*

**+Yuusuf Sallahuddin** as I edit it I am not sure you see the video too


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 28, 2016 17:07*

**+Stephane Buisson** Thanks for that. I may have overlooked the edit if you didn't mention it. I'll take a look.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 28, 2016 17:11*

**+Stephane Buisson** That video is great & very helpful to show just how flexible things become with the living hinge. As I've never tried it, it is good to see the issues faced with different spacing between lines, different overlaps, etc.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/C7Crw6xgzPh) &mdash; content and formatting may not be reliable*
