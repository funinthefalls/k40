---
layout: post
title: "Very basic, laser cutting 101-level question: If a lens's focal length is 50.8mm (2\"), does that mean that, for cutting, the middle of your material must be 50.8mm away from the lens for an optimal cut?"
date: March 23, 2017 17:59
category: "Discussion"
author: "Bob Buechler"
---
Very basic, laser cutting 101-level question:



If a lens's focal length is 50.8mm (2"), does that mean that, for cutting, the middle of your material must be 50.8mm away from the lens for an optimal cut? 





**"Bob Buechler"**

---
---
**Alex Krause** *March 23, 2017 18:11*

This may help explain it... But yes focus to the middle of the material for the best cut

![images/aa82e5eb65900b1b06161154f5ba882b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa82e5eb65900b1b06161154f5ba882b.jpeg)


---
**Bob Buechler** *March 23, 2017 18:45*

Thanks! That does help.


---
**Jim Hatch** *March 23, 2017 19:01*

Depends. Typically yes. But for acrylic where I want to smooth a low power cut (score) or an engrave I often de-focus the laser. For most things there's no real difference top vs middle focus too - especially with 1/8" material.


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/6V5FRuGA7NL) &mdash; content and formatting may not be reliable*
