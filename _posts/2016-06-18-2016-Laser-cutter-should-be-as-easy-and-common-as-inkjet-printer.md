---
layout: post
title: "2016 Laser cutter should be as easy and common as inkjet printer !"
date: June 18, 2016 12:49
category: "Discussion"
author: "Stephane Buisson"
---
2016  #yearofthelaser 



Laser cutter should be as easy and common as inkjet printer !





**"Stephane Buisson"**

---
---
**Jim Hatch** *June 18, 2016 13:42*

Totally true - should be just like plugging in a printer for your PC. Not sure we're there yet though - too many steps, different software, hardware that's not quite there and costs are still too high. Gotta be a mainstream provider of both hardware & software to take it out of the maker world - most people think git is a typo and open source is something hackers do 😃


---
**Jim Hatch** *June 18, 2016 14:02*

**+Peter van der Walt**​ yep, my point exactly. 3D printers still aren't quite there yet either. We're in the early days of what was the laser printer & desktop publishing days of laser cutters. Waiting for the HP & PageMaker of cutters 😃


---
**Stephane Buisson** *June 18, 2016 14:25*

history repeat itself, the real progress will be software more than hardware (autofocus for all ?) and **+Peter van der Walt**  is spot on with Laserweb. ( all OS, modular, ...  & not only Lasercut)



**+Thomas Oster** thesis is to be read, I really do like his approach to laser cut,  material library, mapping, setting)

[http://hci.rwth-aachen.de/visicut](http://hci.rwth-aachen.de/visicut)


---
**HalfNormal** *June 18, 2016 15:39*

I had always wanted a laser cutter and started with a small three watt solid state one. Then Glowforge came on the scene and I said what the heck let me see if I really would use one. Well here I am!


---
**Jim Hatch** *June 18, 2016 16:16*

**+HalfNormal**​ Yep I have one of those coming too. And I'm planning a large(r) format 80W build for this winter based on Smoothie, LaserWeb and LightObject hardware. The three will give me plenty of options for creating stuff.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/a8DiLNwPe6b) &mdash; content and formatting may not be reliable*
