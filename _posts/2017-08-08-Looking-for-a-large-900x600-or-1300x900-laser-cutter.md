---
layout: post
title: "Looking for a large 900x600 or 1300x900 laser cutter"
date: August 08, 2017 11:35
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Looking for a large 900x600 or 1300x900 laser cutter. Anyone has something similar that can share their experience?





**"Ariel Yahni (UniKpty)"**

---
---
**Anthony Bolgar** *August 08, 2017 11:47*

Are you looking for a chines one, or something a little better? Why not build the Freeburn that Peter designed? Just adjust the measurements to fit.


---
**Ariel Yahni (UniKpty)** *August 08, 2017 12:33*

Chinese, will not have time for the build


---
**Anthony Bolgar** *August 08, 2017 12:37*

Check put Redsail...I am happy with my older Redsail LE400, at least a few years a go they were building a decent quality machine.




---
**Ray Kholodovsky (Cohesion3D)** *August 08, 2017 15:52*

I think there's a Chinese one around that size which is 36v and M2Nano. You get the shittier electronics but the bigger bed. Get that and upgrade :) 


---
**Nate Caine** *August 08, 2017 21:04*

<i>A Tale of Two Lasers...</i>



(1) Five years ago, I helped a friend with a minor configuration problem on his <b>Thunder Laser Mars-Series</b>.  It's a 80W machine, <i>well engineered and manufactured</i>.  More so, the support people were helpful, knowledgeable, and quick to respond.  Recently, when the computer upgraded to Windows 10, <i>Thunder Laser</i> was still there for us with the software update.  Originally this was a $5,000 machine.  The entire product line has been upgraded, so I don't know the current models.  



<b>Thunder Laser--recommended!</b>



<b>===============================================</b>



(2) In contrast, around the same time, we were dealing with an <i>XYZ-Tech</i> <b>Exlas 1280 Series</b> machine.  I wouldn't wish this machine on my worst enemy.  <b>Total piece of shit.</b>  This was supposed to be a $7,500 machine, but when the nightmare was over, they had spend about $12,000 ... for reasons known but to God.



<b>Avoid XYZ-Tech at all costs!</b>  



I've mentioned this machine before:  



-They mis-matched the pin sizes in the connectors so they hot-melt glued them together.  



-The x-limit switch fell off as the screws were stripped.



-The inline red laser pointer burned up on the second day.



-The HVPS was mis-calibrated (for a 130W tube) so it burned out the 100W tube in only 3-months service.



-The "1-year warranty" only applied to the machine, not the tube.  



-They offered to sell us a replacement tube "at cost".  But it was actually cheaper on eBay.  



-The "water flow" switch is really a "washing machine pressure switch".  This is the wrong type of switch and soon began leaking.



-The machine was full of metal burrs and filings, left over from manufacturer.



-The hood was misaligned and wouldn't close, so they took a drill and just reamed the hell out of the hole.



-The "safety interlock" was wired ass-backwards so actually never worked (which we discovered when it started buring stuff with the interlocked hood open).



-The motorized z-axis table had stripped screws so fell out during shipping.  In the factory they z-axis probe had mashed a hold in the table mesh.



etc. etc.

 


---
**Ariel Yahni (UniKpty)** *August 08, 2017 21:41*

Thanks **+Nate Caine**​ thanks for the info. I'm curious about how deep can you cut on a single pass for acrylic, MDF and ply


---
**Nate Caine** *August 08, 2017 22:29*

**+Ariel Yahni** For Acrylic, very reliably to at least 13mm (0.5").  Acrylic is a pure substance, so predictable.  Use "cast" Acrylic (as opposed to "extruded") when possible.  The picture was with the 100W machine.  Similar results for the 80W.  Both machines with clean optics and recently aligned.



MDF is more unpredictable, because the composition varies based on the wood chips, sawdust, and glue used.  Perhaps 8mm?  



Do some test cuts on scrap.  We had one guy do a project, and made some test cuts, but substituted his "good" MDF for the final cut.  It failed to cut thru at even 6mm.  



Same advice applies to plywood, which additionally might have voids and defects hidden on the inner layers.  Throw in moisture content and growth patterns makes it even more unpredictable.

![images/677b1329befa00f9c1abba9b54e59760.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/677b1329befa00f9c1abba9b54e59760.jpeg)


---
**Paul de Groot** *August 08, 2017 22:52*

**+Ariel Yahni** try to find a Trotec laser on eBay. The Synrad RF laser tube (75W) has a 30.000hr MBTF and most of these lasers are abandoned when they become intermittent. The cost of repair is just too high ($3500). The fault is actually very small, the RF capacitor shorts on one of the RF transmitters. Cost of the capacitor is about $1 and easy to replace. The working space is massive 1000 x 700mm, the optics (1000dpi) and precision are amazing. Makes a K40 feel like a cheap toy.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/HnGjazb33XF) &mdash; content and formatting may not be reliable*
