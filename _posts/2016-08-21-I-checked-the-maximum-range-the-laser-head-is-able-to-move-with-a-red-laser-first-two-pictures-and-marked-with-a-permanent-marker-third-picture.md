---
layout: post
title: "I checked the maximum range the laser head is able to move with a red laser (first two pictures) and marked with a permanent marker (third picture)"
date: August 21, 2016 16:24
category: "Modification"
author: "Sebastian Szafran"
---
I checked the maximum range the laser head is able to move with a red laser (first two pictures) and marked with a permanent marker (third picture). This is the minimum cut-off length. Another marker line (still third picture) is the 'stock metal bed' edge position, so the maximum cut-off length, assuming no overlap with the metal bed. Cutting off should take place somewhere between two lines, possibly closer to the rear (metal bed edge) line.



The exhaust 'tunnel' has been mounted with 4xM4 screws with nuts and I did not have to unmount the gantry. Last two pictures shows exhaust tunnel from the machine rear side.



**+Ariel Yahni** What do you think?



![images/a21048aa12b1e991824b24a7778a22d1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a21048aa12b1e991824b24a7778a22d1.jpeg)
![images/56a808b5c51fb73ee170dcbcf8eacb0b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/56a808b5c51fb73ee170dcbcf8eacb0b.jpeg)
![images/a3015a3ad32c97d162e6939d9b6a33c8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3015a3ad32c97d162e6939d9b6a33c8.jpeg)
![images/1cc1c2079adb6f72447fd23f12299959.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1cc1c2079adb6f72447fd23f12299959.jpeg)
![images/f8f9850927c768ac33ac51e64e578a5d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f8f9850927c768ac33ac51e64e578a5d.jpeg)
![images/c4d542a4455b4f3bdc6c9d53daadca98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4d542a4455b4f3bdc6c9d53daadca98.jpeg)

**"Sebastian Szafran"**

---
---
**Ariel Yahni (UniKpty)** *August 21, 2016 16:30*

Nice!!! Your was installed from the back, lucky you. I did remove the whole thing so im able to fit larger materials. If you are comfortable with the remaining size after cutting i would say go for it


---
**Sebastian Szafran** *August 21, 2016 16:46*

Thanks **+Ariel Yahni**. I will cut-off first and see how effective the thing is. If not satisfied, I will consider to 3D print new part, with wide exhaust tunnel that closely fits the size of the rectangular hole on the back with length adapted to the 'somewhere between markered lines' distance. Are you running K40 Smoothiefied or other?



All builders advice and experience highly appreciated. I wired all connections, except I am not sure whether PWM is better on IN or on L. Still have a little time to decide, as I am working on putting exhaust pipes into the chimney, but new setup test is close.


---
**Ariel Yahni (UniKpty)** *August 21, 2016 16:53*

Yes I have a smoothie compatible board. Mine does not need a level shifter so Im directly connecting the L to the Mosfet negative pole. **+Alex Krause**​ just finished his convertion and is using a level shifter with an original smoothieboard. He also is using L via level shifter to the 2.4pin not the Mosfet. Hope this helps, if you need assistance don't hesitate to let us know


---
**Alex Krause** *August 21, 2016 17:55*

Do you have a beam combiner installed?


---
**Sebastian Szafran** *August 21, 2016 18:28*

**+Alex Krause**​ not yet, but I am thinking of installing detachable red laser dot between the tube and first mirror and use it for tube alignment. 

This time I used cross-laser directed into last mirror in order to mark the line with a marker.


---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/Pkof7egxLyi) &mdash; content and formatting may not be reliable*
