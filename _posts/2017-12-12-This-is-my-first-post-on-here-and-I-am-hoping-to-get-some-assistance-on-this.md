---
layout: post
title: "This is my first post on here and I am hoping to get some assistance on this"
date: December 12, 2017 00:32
category: "Hardware and Laser settings"
author: "Josh Hutch"
---
This is my first post on here and I am hoping to get some assistance on this. 

I was cutting out some shapes on acrylic and all the sudden the laser stopped working. No bangs, pops pew's or otherwise..The power turns on, corel works as it should, the steppers and all components work as they should. As you can see in this video, the water pump is working correctly, the exhaust is working correctly and the green power light is on  on the mother board. When I press the test switch on top, I hear what sounds like a normal sound and the test switch on the PS lights up. When I press the test switch on the PS, the test light illuminates and you hear a high pitched sound from the area but still no laser...

I have used a chirper to indicate voltage but have not tested the exact voltage. But at least I know there is something go through some of the correct places...

Also if you look at the laser tube, the tube looks normal, no cracks, no smoke damage...

Any thoughts on what could be my problem?


**Video content missing for image https://lh3.googleusercontent.com/-AucUV4UIj88/Wi8jqnSU8UI/AAAAAAAAA-U/1oqrm5vTUrU-xZdv_ddkaaw71rwNNvvLACJoC/s0/video-1513037370.mp4**
![images/0c3392e627a8ae6099ceb59231f7564d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c3392e627a8ae6099ceb59231f7564d.jpeg)



**"Josh Hutch"**

---
---
**Steve Clark** *December 12, 2017 02:12*

Try replacing your water. Use distilled of course and flush. Then get back to us...


---
**Anthony Bolgar** *December 12, 2017 02:27*

Also, what was the water temperature when it cut out? Mine will stop producing a beam capable of even engraving at temperatures over 37 deg Celcius (and power starts dropping off at about 28 degree Celcius


---
**Nate Caine** *December 12, 2017 03:00*

Have you checked the intensity pot?  What does the current meter do when you dial the pot up and press the test button?


---
**Josh Hutch** *December 12, 2017 04:00*

Got the water taken care of. I spent about 30 minutes flushing the lines as best I could and then I ran all the water out of it and flush distilled water into it dumped all that water out and then did a fresh half gallon of new distilled water. Then I ran it for about 20 minutes through the tubes without any laser function or any attempt to function the laser. And no change so far


---
**Josh Hutch** *December 12, 2017 04:02*

Well testing the test button and the intensity I press the test button and move the intensity all the way to the top and then all the way back down and you can hear through the power supply that the intensity was going up so it made more of a whining sound and then as a guy lower you that whining sound turn back down. So I know it's getting juice from the power supply to the amp meter and back but it's just not going into the laser


---
**Joe Alexander** *December 12, 2017 04:14*

is the laser tube lighting up when you test it? the whining is usually an arc so i would check the HV line going to the tube, and the return to the ammeter.


---
**Ned Hill** *December 12, 2017 05:34*

Have you checked your mirrors?  I've seen this caused by a busted mirror or a failed tension spring on the mirror heads numerous times.  


---
**Josh Hutch** *December 12, 2017 05:40*

Yes. Checked and cleaned all the mirrors.

No light in the tube and the ammeter does not respond to anything. But using the chirper, it is chirping next to the HV red cable just not as intensly as it is next to something like the hot black wire in the power cord... 


---
**Joe Alexander** *December 12, 2017 06:17*

i would then venture to say that your flyback transformer or some internal part of the power supply must have failed. happened to me once.


---
**Josh Hutch** *December 12, 2017 06:35*

Im thinking the flyback as well,  it's the only thing,  I haven't been able to test effectively yet.  


---
**Josh Hutch** *December 12, 2017 06:37*

I think it also could be the laser tube itself but I don't see anything on there that would indicate that it's been blown or cracked or flooded or anything else but this is my first laser cutter as well that's why I headed to the pros..... you guys


---
**Joe Alexander** *December 12, 2017 06:39*

i was in the same position as you and went psu first, and it worked out.


---
**Josh Hutch** *December 12, 2017 06:40*

 Do you know where i would get parts or a new psu? 


---
**Joe Alexander** *December 12, 2017 06:41*

whareabouts are you located? i just looked on ebay myself for a my-jg40 power supply with the same connectors(there are like 3 connector setups)


---
**Josh Hutch** *December 12, 2017 06:43*

Kansas City area


---
**Joe Alexander** *December 12, 2017 06:44*

ahh yea snag one off ebay would be my recommendation.


---
**Josh Hutch** *December 12, 2017 06:48*

Does it need to be the exact same or can I use a psu from a better company or oem unit? 


---
**Joe Alexander** *December 12, 2017 07:16*

you can use another type just know that you may have to rewire accordingly.


---
**Josh Hutch** *December 12, 2017 08:05*

I found the one I need,  ill order it and see how it goes,  ill still continue to troubleshoot just in case thats not it.  


---
**Don Kleinschnitz Jr.** *December 12, 2017 13:36*

Some notes:

**+Josh Hutch** be careful... the LPS outputs lethal voltage even if it is not working correctly. 

The supply can look like its not working yet has enough voltage while on or if not discharged to be dangerous.



My guess is your stock machine has no interlocks ...

Surprising, the supply does not look like its that old.

Not to be an ambulance chaser but if the LPS ends up bad and you do not intend to repair or keep it around,  I am collecting dead ones in a continuing attempt to find out why they fail.  :).



With symptoms like yours I would guess the HVT in the LPS is bad.

..........

"...it is chirping next to the HV red cable just not as intensly as it is next to something like the hot black wire in the power cord..."

A chirper will likely indicate anywhere near the LPS as the voltage, even if it is weak, is very high.

....

You can measure the voltage at the "IN" pin on the LPS which controls the power level of the LPS. It should vary between 0 and 5VDC when the current pot is rotated.

.....



You can get parts for your supply from ebay. The exact part depends on what vintage supply you have. 



[http://donsthings.blogspot.com/2017/07/repairing-k40-lps-1.html](http://donsthings.blogspot.com/2017/07/repairing-k40-lps-1.html)





[donsthings.blogspot.com - K40 LPS Replacement and Test](http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html)


---
**Dag Elias Sørdal** *December 12, 2017 14:14*

If you disconnect the wires to the tube and meahure the resistance what do you get?


---
**Don Kleinschnitz Jr.** *December 12, 2017 16:25*

**+Dag Elias Sørdal** **+Josh Hutch**

Danger: if you mess with the HV lead you have to discharge it first!



The HV connection to the tube has a HV diode in series with it (in the HVT). Unless it is shorted to ground (not the normal failure mode) it will read "open" but it actually may not be. Reading a high resistance means that it is ok or the HV diodes are open. Not a conclusive test because both good and bad cases read open.



The cathode wire is in series with the meter so I would short out the meter before measuring to prevent any potential damage to it. With the meter shorted it should read 0 ohms to the FG connection on the LPS.


---
**Steve Clark** *December 12, 2017 16:34*

I also noticed on your video that you are not using the earth ground...that is not good as it opens you up to being the best ground depending on conditions. Don has a whole video example on this.


---
**Josh Hutch** *December 12, 2017 17:14*

I'll  give all this a shot and respond as soon as I get all the suggested testing complete.  


---
**Madyn3D CNC, LLC** *December 12, 2017 19:36*

Sounds exactly like what mine did about a year ago. I ended up changing the LPSU & tube, so never got to isolate it to one or the other. There were no visual indications at all it was a bad tube, but I needed an upgraded tube anyways so I just got both the tube and the LPSU. 


---
**Josh Hutch** *December 18, 2017 00:48*

I found the problem got the repair part in. It was a flyback Transformer. Got her installed, grounded, tested and it works even better than before. Thank you everybody for your input. 


---
**Steve Clark** *December 18, 2017 01:57*

Just to share with us...what method did you use to determine it was the flyback transformer?


---
**Don Kleinschnitz Jr.** *December 18, 2017 03:06*

**+Steve Clark** Don GUESSED!


---
**Josh Hutch** *December 18, 2017 04:02*

Well working with the only tool that I had at the moment I used what was called a chirper. It indicates voltage but it doesn't say how much voltage or amperage is going through Hotwire. 

If it's a hot wire it'll chirp like crazy. 

If it's a white neutral wire it'll chirp a little bit but not quite as much as a hot. 

If it's a ground wire won't chirp at all. 

So obviously it wasn't really a highly technical device.

What it did tell me though is that the plug into the machine had a very hot wire. The hot wires which are typically black indicated a high-voltage by a very loud chirp and quick beat. The neutral wires still chirped but not as much. 

So comparing the sounds from the plug and the hot wires going into the PSU versus the flyback Transformer and the hot wire to the laser itself. 

There was quite a bit of difference meaning power was getting into the flyback Transformer but not as much voltage over to the laser while turned on and being tested. 

I figured that since the laser would be a high voltage area then the problem should lie between the laser and the flyback Transformer.

Since everything before the flyback Transformer seem to have plenty of voltage but everything after the flyback Transformer had a great reduction in voltage, I figured that it must be the flyback Transformer that was my issue.

After reading other posts and blogs about the laser after and went bad I had no indications that the laser was bad the power supply unit still had green light on and when I pressed the test button on the PSU the test button light came on and I heard a whining sound coming from the flyback Transformer, that was also a good indicator that the flyback Transformer was the issue.

Like I said in a prior response the pump was working the fan was working I knew I didn't have additional water inside the laser itself. 

The other indicator was that I wasn't getting any results in the ammeter which I'm assuming the power goes through the flyback Transformer and then shows an indication through the ammeter gauge. 

So with all that it took my best guess and bought the 40 to 60 watt flyback Transformer version. And the second I plugged it in and got everything wired up it fired up much better than before.

So again thanks everyone for your advice and all the tips to narrow this down hopefully my issue is help somebody in the future and maybe my amateur explanation of how I came to the result that I did may help someone in the future as well.


---
**Steve Clark** *December 18, 2017 04:47*

Thank you for the feedback! The process of elimination! 


---
**Don Kleinschnitz Jr.** *December 18, 2017 11:56*

Additional thoughts on LPS testing;



I have been noodling for some time for a definitive AND SAFE way to tell if the laser or LPS is bad. A test or tester that every K40 user could employ. This is because on these machines these two parts are treated by the manufacturers as consumables.



Creating an arc is the best test I know but its dangerous and inconclusive. 



Usually if the LPS will not arc its dead. If it does arc and the tube will not light up its the tube that is dead. 



Inductive indicators may or may not work at very high voltages and they generally do not work with DC. 

The LPS output is mostly DC so I would not expect much of an indication when bad. 

However there is so much energy even on the primary (plus 600v pulsed) you likely would read some activity. Meaning it be a may good indicator, it may not.



Turns out, best I can tell, the guts of the LPS is reasonably robust. When they fail other than the HVT the fuse will blow and its the bridge rectifier. 



I think the HVT's are unreliable because the HV diodes potted inside them are poor quality, potted poorly or perhaps just sized wrong. At these voltages it doesn't take much to fry a diode.



Removing and testing the HVT may be an option but that would require a fixture that also uses high voltage and perhaps not everyone can build one



I am consumed these days with the OX build but just started to put my K40 back together and continue LPS and PWM testing.


---
**Madyn3D CNC, LLC** *December 18, 2017 16:09*

"I am consumed these days with the OX"



Phew, I was hoping you didn't get sick and tired of the K40. Your work has helped me, and many others i'm sure,  with troubleshooting and understanding the K40. It takes a LOT of patience for this kind of commitment you have with these machines. 


---
**Don Kleinschnitz Jr.** *December 18, 2017 16:59*

**+Madyn3D CNC, LLC** not tired of the k40, just lots of family time and learning the nuances of cnc. Learned alot about gcode on mills, tiny G, chillipepr and guess what... found a design problem with pwm control of spindle motors..... go figure!


---
*Imported from [Google+](https://plus.google.com/110021344022622014661/posts/cBCcbc637vj) &mdash; content and formatting may not be reliable*
