---
layout: post
title: "Seeking advice from you experienced drivers of the K40 Mu machine came with a missing lens and bent and broken carriage for the head"
date: August 28, 2016 18:16
category: "Modification"
author: "Purple Orange"
---
Seeking advice from you experienced drivers of the K40

Mu machine came with a missing lens and bent and broken carriage for the head.



Would this be a good replacement part? Air assist is a needed modification anyways as far as i hae read up here. Looks by the thumbscrew  on the head that this has head has a variable focal point too(?)



What you say?

/Cheers





**"Purple Orange"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 28, 2016 18:27*

Looks like it would do the job, however keep in mind that particular listing is without mirror or lens (& the lens required for this part is larger than the stock lens; 18mm as opposed to stock 12mm).


---
**Jim Hatch** *August 28, 2016 18:42*

Or use the stock lens - just get yourself an 18mm nylon washer with a 12mm inner hole or drill out whatever hole the washer has. You can find them at Lowes or Home Depot.


---
**greg greene** *August 28, 2016 21:35*

not an adjustable focal point.  The thumbscrew is to lock on the air assist nozzle I believe


---
**Jim Hatch** *August 29, 2016 02:30*

It looks like a Saiite head that does actually adjust the height using that side screw.


---
**Sean Cherven** *December 29, 2018 19:05*

So is this adjustable focal length or not?


---
*Imported from [Google+](https://plus.google.com/112469922804049225807/posts/HCpurKMP81z) &mdash; content and formatting may not be reliable*
