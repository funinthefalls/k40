---
layout: post
title: "Howzit? Hopin somebody might be able to give me a tip here on what's going on"
date: February 24, 2016 01:02
category: "Hardware and Laser settings"
author: "Tim barker"
---
 

Howzit? Hopin somebody might be able to give me a tip here on what's going on. You all were a big help with my last issue.



So I had to order a new tube recently. Got it installed and all the connections to the posts on the tube seem to be right. No cracks that I can see. Water flowing well. Pretty sure the power supply is working properly as everything else comes on just fine. 



Somehow though I'm not getting my tube to fire at all.



When I try to measure the current by touching the posts on the tube and hitting the test button I get a big spike on my meter and then OL on the display. Still no fire though. 



Any help is greatly appreciated if anyone has some thoughts.

Thanks.﻿





**"Tim barker"**

---
---
**Scott Marshall** *February 24, 2016 05:19*

Please don't do that anymore. 



The power supply you're fooling with can easily kill you dead. It is probably not working, and a good thing too,  or you'd be injured or worse.



NEVER touch any part of the laser tube, it's wiring, or bracketry. Keep your hands well away if you must run it open. You don't even have to touch a bare wire to be hurt, it can jump out and get you. I've worked with this kind of equipment for nearly 50 years, and I don't EVER take it lightly. 



This IS NOT like static electricity, there's easily enough current to stop your heart @ 15,000 volts.



If your tube is connected correctly, you should be seeing a current on the meter as you turn it up. if not, and there's no glow, either the tube or supply (or both) are bad or improperly connected.. 



I can't tell what's going on by the "Spike" you describe, if you're shorting the tube out, it will destroy the power supply for sure. 



Read up on exactly how these circuits work, safety, and service procedures, that should help you determine if the power supply is the issue. The info is available on the general Co2 laser forums.



It seems pretty common that the tube kills the power supply when it dies,  I believe they short when they fail, and the power supplies on the K40 are cheap and have no protection, so burn up before the operator realizes the tube has failed.

A lot of people seem to end up right where you are, having changed the tube they find the power supply is bad also.





There are instruments to measure the high voltage outputs, but they're not for non-professionals, incorrect use can be very hazardous. 



The power supply isn't fused and if bad, will have to be replaced. I don't think they're very expensive (for a stock replacement)



And be careful out there, the k40 is a lot of things, safe is most certainly NOT one of them. 



Scott


---
**Tim barker** *February 25, 2016 02:40*

Wow. Well I guess I'm lucky then. I just happened to read in another forum that it was possible to measure the output on the power supply and I assumed it could be done with a meter. Thank you for letting me know, because nobody else has.



Well the tube is brand new and the connections are pretty straight forward. I'm positive they are connected right. I guess from the sound of things the power supply must be fried.



Is there another way that you can recommend checking the power supply next time? Also is there a way to add protection from shorting it in the future?



Thank you again for the information. Much appreciated.


---
**HalfNormal** *February 25, 2016 04:20*

Read this post, it will answer all your questions. [http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/252972-software-posts.html](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/252972-software-posts.html)


---
**Scott Marshall** *February 25, 2016 08:16*

I didn't want to insult you, but I was more concerned you might get electrocuted. 



The measurements can be done with a High voltage meter - it's actually a probe consisting of a very long resistor which includes insulators to prevent arcing and divides the voltage down to where it can be read with a meter. They're pretty expensive but specifically designed for the purpose. Back in the CRT days you could borrow one from a TV shop (they are used for checking the Anode voltage on the old style picture tubes) - (I worked my way through college working in a tv shop back in the 70s. - they're all gone now).



I'll not repeat The link HalfNormal gave you, At a glance it looks to be sound information. 



If you have a new, known good laser tube, and it's hooked up properly, It will fire. About the only thing that will stop it is a bad power supply. 



These power supplies are cheap, (around 100 bucks I believe - less than a HV probe) If I were you, I'd just order one. You're committed at this point, so you may as well go for it. 



Watch out for "Instruction Manual Experts" on ANY internet forum. There's a lot of guys out there giving advice without any real knowledge or experience. Their ignorance can get YOU killed. Always check the guy isn't full of you know what. Even me. Search the net and get at least 2 or 3 concurrent opinions, (Way more if you're playing with life, limb or love).



If you have any problems getting the new power supply working, drop me a line and I'll help if I can.

 Oh, yeah, DON"T run the power supply open circuit to test it, it can cause internal arcing and failure. Just hook it up normally, start about 1/2 power in short bursts and slowly work up to full power and long firings, closely watching temperature, tube current and tube color. Stop and find the problem if anything seems "off". If you hear snapping, crackling or hissing, stop immediately and find the arcing. 

If it seems way too powerful, it probably is. It won't live long if you overdrive the tube.



Scott












---
*Imported from [Google+](https://plus.google.com/113646430521733366133/posts/JX7c3Ejg3Pu) &mdash; content and formatting may not be reliable*
