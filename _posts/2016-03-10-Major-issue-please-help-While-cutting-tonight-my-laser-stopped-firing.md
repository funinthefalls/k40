---
layout: post
title: "Major issue please help. While cutting tonight my laser stopped firing"
date: March 10, 2016 23:26
category: "Discussion"
author: "3D Laser"
---
Major issue please help.  While cutting tonight my laser stopped firing.  It will power on but and the X and y axis will move but it will not fire.  When I try to hit the test fire button the meddle doesn't move.  I'm afraid I may have fired my tube but I don't see anything wrong with it





**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2016 23:38*

Does the interior of the tube light up purplish when you hit the test fire? If so, the tube is more than likely still working & maybe a problem with alignment or something else.


---
**3D Laser** *March 10, 2016 23:38*

No the tube does not light up and the ma netter does not move 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2016 23:40*

In that case, either the tube is fried or something wrong with the power feed to the tube. I'm not very electrically inclined, so maybe someone else here will have some suggestions for safely testing the power.


---
**3D Laser** *March 11, 2016 00:59*

I was in the middle of cutting when the laser stopped working upon closer inspection it appears my water polymorphism wasn't fully plugged in and stopped working.  I think it over heated


---
**Phillip Conroy** *March 11, 2016 04:42*

When i forgot to turn on my water pump i blown tube[crack in glass] and power supply at the same time,expensive misstake


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 11, 2016 06:32*

I have my water pump plugged into a 4-socket power board, with the laser cutter. So in order to even turn the laser cutter on, the water pump is on at the same time.


---
**Scott Thorne** *March 11, 2016 11:34*

Invest in a 15 Dollar water flow safety switch.


---
**3D Laser** *March 11, 2016 12:07*

**+Phillip Conroy** so my machine powers on and will even move the laser head but how do I know if I blew my power supper will the machine even turn on


---
**Scott Thorne** *March 11, 2016 13:48*

Yes.....you might have blown the flyback transformer on the power supply....if so everything else will work but the tube.


---
**3D Laser** *March 11, 2016 14:39*

**+Scott Thorne** how do I check that 


---
**Thor Johnson** *March 11, 2016 16:43*

To properly test it, you need a HV (15KV) voltage tester (like for a tube TV); attach it carefully and press the fire button:

  If it goes to 15KV and stays there, the tube is open, but the power supply is working a bit -- check the ground wires to make sure that didn't work loose during the cut.

  If it pulses up, but falls down to 8KV or so, then the tube is actually working but not lasing (probably not because you see no current on your meter).

  If it doesn't jump up at all, the power supply is dead.  Tube may also be dead (and it may murder a new power supply).



I think you can take a 120V neon bulb (with resistor) and connect it from HV->Ground and see if it lights, but I think that that could also explode immediately (1Meg limiting resistor that is in the lamp assembly will overheat rapidly)... and the neon bulb will prevent the laser from firing, so remove it even if it doesn't explode.  Shouldn't hurt the power supply.


---
**3D Laser** *March 11, 2016 17:09*

**+Thor Johnson** so to be safe should I replace both


---
**3D Laser** *March 11, 2016 19:52*

The more I am thinking about it the more I am thinking it maybe the power supply.  As while I was cutting with it.  It made a squealing sounds at times 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/Kyy5NDEDfWF) &mdash; content and formatting may not be reliable*
