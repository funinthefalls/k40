---
layout: post
title: "Hello! I am setting up my K40..."
date: August 03, 2016 07:21
category: "Discussion"
author: "Nicola Gibson"
---
Hello!

I am setting up my K40...

It's all new and going through the process that you do with a new pieces of kit where you have to figure out what everything does!

I have set up my machine now and the mirrors are aligned but even with the power up to full I only get an indent of about 0.5mm on 2mm MDF.

I set the speed to 5mmps (oh, not sure without checking if that's the legitimate value, but I set the speed to 5 anyway!)

So far, I have just about managed to cut through regular printer paper by setting the speed to 60 and the temperature to to about 75%.

I'm assuming (just through observation) that ideally the laser needs to be fairly powerful and the speed fast so that it cuts rather than melts/obliterates the material through burning it. Is that right?

Anyway, currently it doesn't make much difference as there isn't sufficient power to get get through cardboard!



Is there another adjustment I can make which I'm missing which will give me more power?



I have the non digital version.

Your help is very much appreciated. Thanks





**"Nicola Gibson"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 03, 2016 07:59*

You can check if your lens is up the correct way. Mine shipped upside down. It should have the bump (convex) side facing up & flat/concave side facing down.



Also, focal point is important for successfully cutting with ease. The centre of the material being cut should be at 50.8mm from the base of the lens.



I prefer to use low power & fast speeds, but sometimes it will require multiple cuts (or passes) in order to cut through a specific material. I use low power because the beam is thinner on low power (4mA on mine as I have the analogue meter). The thinner beam means less issues with wide charring around the cut area.



Also, you will find that having some sort of air-assist drastically improves cutting ability & quality, as it provides more oxygen to the place of cut & also minimises flare-ups of flames (which cause extra charring).


---
**Nicola Gibson** *August 03, 2016 08:10*

Thank you, I'll check my lens and focal length. I have printed an air assist part but I guess I just wanted to see it working properly before I started fixing bits and bobs in.

It also makes a high pitched sound just as the laser gets powerful enough to mark paper. I hadn't given it much thought (I haven't used a mini laser cutter before!) but after I wrote this I found that it might be my red cable shorting... I have had a look and it all looks nicely attached at both ends without damage along the length.  The tube looks nice, no odd charring marks, melted glue or lolipy connections.

So I will check the focus orientation of the lens and keep my fingers crossed :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 03, 2016 08:14*

Beware of that cable you are talking about. It is extremely high voltage & highly dangerous.



Some others have mentioned here before that the high pitched noise can be a Power Supply issue. I think (if I recall correct) that it was something to do with vibration & wasn't a huge concern. Also, I find some materials actually "screech" when being vaporised (kind of like how an old kettle makes a whistle noise when it boils). I've noticed it in particular with plywood.


---
**Nicola Gibson** *August 03, 2016 08:18*

Thanks - I did unplug everything. It would be a really dull way to end things :)


---
**Nicola Gibson** *August 03, 2016 08:45*

Hi again Yuusuf, just thinking about what you said about the focal length - what is the best way to make that measurement and then make the alterations? It occurred to me after reading it that this would need to be altered for every new material. I assume adjusting the bed if the way to go?

Many thanks :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 03, 2016 09:29*

**+Nicola Gibson** Yeah, many users have installed what is known as a z-bed. There are manual versions or stepper-motor controlled versions available for purchase. Personally, I generally use very similar material heights, so I just set for the most common (3mm). However, when I am working with something that is outside of that standard, I just totally remove my bed & use different methods to hold it up (nail bed, basically a piece of wood with nails sticking up to support the workpiece; blocks of wood; or magnets to hold up my platform so that I can adjust the height in 2mm increments by adding extra magnets).



There are many ways to go about it, but I have gone with the total DIY method, so I don't have a manual adjust z-bed or an motor controlled z-bed. There is a site called LightObjects.com that sells a lot of addon modules that you can install with your K40 (e.g. z-bed, rotary attachment, better lens holder/lenses, better mirrors, etc).



To begin with though, I highly suggest removing the horrible cutting bed/clamp thing & replacing it with something else. A lot of people use what is known as a honeycomb bed (which LightObjects sells if you want to reference). I just purchased a piece of perforated stainless steel from a local supplier. Although it does the job, it has some issues with reflection on the back of the workpiece (when cutting through) that are avoided by the honeycomb (since it has minimal contact area with the workpiece).



Best way to measure the focal point is from the base of the lens holder (as the stock lens/lens holder has the lens positioned right at the base of the holder). Some users create small measuring blocks (out of a piece of scrap wood) to gauge the depth. So, if you are using a 3mm piece of material, you want focal point to be 50.8mm to the centre, meaning to the top of the material you want 49.3mm. Chop a block of wood at 49.3mm, place it under your lens holder to gauge the depth to the top of the material. If needed, raise your work platform (or material) by whatever is needed to get it fitting nicely.


---
**Nicola Gibson** *August 07, 2016 17:40*

Thanks you **+Yuusuf Sallahuddin**  :)


---
*Imported from [Google+](https://plus.google.com/111533642814945984061/posts/NmeJn786kDF) &mdash; content and formatting may not be reliable*
