---
layout: post
title: "Does anyone know how big of a bucket should I be using for cooling the laser?"
date: January 12, 2016 04:02
category: "Hardware and Laser settings"
author: "Sunny Koh"
---
Does anyone know how big of a bucket should I be using for cooling the laser?





**"Sunny Koh"**

---
---
**Anthony Bolgar** *January 12, 2016 04:25*

I use a 3 gallon bucket, laser is located indoors 20 degree C ambient room temperature, never had a problem with overheating, even on long jobs.


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/6YWUTGhu6yd) &mdash; content and formatting may not be reliable*
