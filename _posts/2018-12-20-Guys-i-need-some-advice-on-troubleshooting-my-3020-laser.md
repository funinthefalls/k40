---
layout: post
title: "Guys i need some advice on troubleshooting my 3020 laser"
date: December 20, 2018 13:08
category: "Original software and hardware issues"
author: "Jawame .Jawayou"
---
Guys i need some advice on troubleshooting my 3020 laser. Yesterday during a job the laser stopped firing a beam .  



it doesnt fire at all no matter which button ( CP or LPS) . is there anyway to identify the damaged part. i  hope it is only a bridge rectifier but it seems to be that high voltage transformer. Anyway i ll check the rectifier but i was wondering if i have to remove it from the pcb to check it with a multimeter



Is there anyway to tell if it is the tube or the flyback transformer ?





**"Jawame .Jawayou"**

---
---
**Don Kleinschnitz Jr.** *December 20, 2018 13:19*

Is the led on the lps on?



If the rectifier is blown you likely will have a blow fuse inside.


---
**Jawame .Jawayou** *December 20, 2018 13:22*

yes it is . i have movement i have 24v 5v the signals are ok it just cant fire .


---
**Don Kleinschnitz Jr.** *December 20, 2018 13:25*

Sounds like HVT bad. Any arching noises?


---
**Jawame .Jawayou** *December 20, 2018 13:58*

No noise . i cant hear not a single thing or arching sounds or even the old working beam noise. my amp meter doesn't show anything too . i also checked the ground connection.


---
**Don Kleinschnitz Jr.** *December 20, 2018 14:42*

Typically if the tube is dead a good lps may arc near the anode.



It's hard to isolate tube vs lps failures. 

Your symptoms sound like a bad hvt.

You can tape a grounded wire to the tube, near the anode. Not connected to it. Then get away from the tube and pulse the test switch on lps to see if the anode arcs to the ground wire.



This is dangerous, stay well away from the tube while you test. 



You can replace the hvt, but these supplies are not expensive to replace.


---
**Jawame .Jawayou** *December 20, 2018 15:07*

Sorry you 've answered already....

 do you suggest any shop in europe to get a decent lps ? i want to measure the bridge rectifier can i just unplug the 3 wires that go into the HVT ? 

Thanks for helping btw...


---
**Stephane Buisson** *December 21, 2018 13:22*

**+Jawame .Jawayou**, from germany, [ebay.it - Dettagli su 40W 11-220v Alimentatore per CO2 Laser Engraving tagliatrice laser power supply](https://www.ebay.it/itm/40W-11-220v-Alimentatore-per-CO2-Laser-Engraving-tagliatrice-laser-power-supply/332964937949?hash=item4d8641c0dd:g:0qEAAOSwnh5b~2B9:rk:19:pf:0)


---
*Imported from [Google+](https://plus.google.com/103778011358771697071/posts/HcrEP5uDsbA) &mdash; content and formatting may not be reliable*
