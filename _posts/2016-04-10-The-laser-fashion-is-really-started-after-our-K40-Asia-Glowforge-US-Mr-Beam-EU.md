---
layout: post
title: "The laser fashion is really started, after our K40 (Asia), Glowforge (US), Mr Beam (EU), ..."
date: April 10, 2016 11:19
category: "Discussion"
author: "Stephane Buisson"
---
The laser fashion is really started, after our K40 (Asia), Glowforge (US), Mr Beam (EU), ...         EXITING TIME AHEAD.

That is undeniable laser cutting is in fashion now.





**"Stephane Buisson"**

---
---
**Stephane Buisson** *April 10, 2016 11:21*

**+Mr Beam**  based on a 5W laser diode. I am a bit surprised by the thickness of material cut, but not denying anything as those guys are serious.


---
**Muccaneer von München** *April 10, 2016 11:58*

**+Stephane Buisson** in short: it is all about the wavelength. shortwave lasers like our 450nm diode are strong on organic materials - like plywood e.g.

 

every material reflects, absorbs and eventually transmits light. The fact that not every wavelength is reflected equally is the base that we can see different colors. Our blue laser is absorbed by plywood quite well - rough assumption ~90%. The CO2 tube lasers have 10600nm invisible far infrared laser light. They are reflected far more (I don't know numbers) - but in the end from a 40W CO2 tube laser only a fraction of the power is used for the cutting process.

If you have a surface like a macbook it is the other way round. Almost everything of the blue 450nm laser light is reflected, while the 10600nm is absorbed enough to etch it.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 13:24*

**+Stephane Buisson** That's cool that lasers are catching on more. Means we can look forward to better machines in the future for cheaper prices (hopefully) or even more features.



**+Muccaneer von München** Thanks for sharing that insight. It makes decent sense that different wavelengths will affect different materials differently (kind of like how normal wavelength light is all good to our skin, but excess exposure to UV-B is probably going to create skin-cancers).



It would be interesting to have different wavelength laser beams in order to test their effect on different materials.


---
**Jim Hatch** *April 10, 2016 13:28*

**+Stephane Buisson**​ I am surprised too - but because of the limitations. It's not up to K40 capabilities with regards to different material thicknesses. The software looks to be 1000% better than the messing around we have to do though. 



I considered buying one but I do a fair amount of work with 3 & 6mm plywood and acrylic and since I've already mastered the software combo I have to use for the K40, if I were to go to a bigger machine  I'd go with another Chinese one in the 60-80w range.


---
**Muccaneer von München** *April 10, 2016 13:41*

**+Yuusuf Sallahuddin** **+Stephane Buisson** the UV-B example is great! that's exactly the reason for shortwave lasers being good on organic material. 


---
**Muccaneer von München** *April 10, 2016 13:48*

**+Jim Hatch** **+Stephane Buisson** Anyone interested in extending our software stack to the K40ies ?


---
**Stephane Buisson** *April 10, 2016 14:28*

**+Muccaneer von München** did you check on laserweb ( & laserweb 2) from **+Peter van der Walt** ? you have already a lot in common.


---
**Muccaneer von München** *April 10, 2016 14:36*

Yes... On my Radar. Awesome project. I would love to meet Peter some day in person


---
**HalfNormal** *April 10, 2016 15:19*

I am actually making a Mr Beam I for a friend who does woodworking. He will be able to put it anywhere on his wood project and burn on it since he just wants a surface burn and not a cut. The portability factor is also a big plus. He can do modifications to existing installations. He makes tables and other items for local restaurants and bars.


---
**Jim Hatch** *April 10, 2016 15:22*

**+Muccaneer von München**​ I like your software approach. It could be retrofitted to the K40s using a wireless USB adapter so long as you can use the Moshi or Nano boards. If you needed a board like a Ramps or Smoothie, then you've got some competition from Peter's software. His is open source so once the board is purchased there's no additional software expense (although I encourage folks to toss him a $20 or 2 so we can keep him in pints while he works).



For a commercial software product, if you can use the stock boards it becomes an easy upgrade in capability for a non-tinkerer who doesn't want to both rip the heart out of his machine and spool up some more software. Just off the top of my head I'd think you'd want to keep it in the sub-$300 range (if they'd need to also but a USB wireless adapter). Anything more than the cost of the original machine starts to be hard to justify emotionally. 


---
**Muccaneer von München** *April 11, 2016 09:35*

**+Jim Hatch** thanks for the explanation. Probably the best way of retrofitting our software would be to exchange the moshi boards with a combination of Arduino Uno with any grbl compatible shield (ours would work, but has the LD driver on board which you don't need then) and RPi - but then you are at ~ $60 roughly guessed. 

The part which is missing is how to drive a CO2 tube laser with a PWM signal. 

Any circuits for that out there? 


---
**Stephane Buisson** *April 11, 2016 09:55*

**+Muccaneer von München** built in the K40 PSU.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/6mZHpcvUjFU) &mdash; content and formatting may not be reliable*
