---
layout: post
title: "Question for ppl who have changed their laser tube...'"
date: June 15, 2017 17:15
category: "Discussion"
author: "Nathan Thomas"
---
Question for ppl who have changed their laser tube...'



How long did you wait before you test fired?



I understand you have to wait until the silicone cure...my question is more aimed at how long did you actually wait until you fired it safely?



The back of the silicone package is telling me it's gonna take 24 hrs to cure...having a hard time believing others waited that long before test firing lol!





**"Nathan Thomas"**

---
---
**Anthony Bolgar** *June 15, 2017 17:44*

I waited 48 hours, not for any reason other than I was too busy doing other things. But I would wait at least 24 hours.


---
**Ashley M. Kirchner [Norym]** *June 15, 2017 19:56*

Silicone needs curing time, follow the directions.


---
**Paul de Groot** *June 15, 2017 22:12*

Me too, I waited 48hrs since it was still soft in the middle of the so called "connector" and the weather was cold. Wonders if anyone has made a real connector?


---
**Don Kleinschnitz Jr.** *June 16, 2017 14:46*

**+Paul de Groot** I saw someone on here use a crimp terminal....

The problem with HV connections like this is that dust gets in and around them and creates corona tracks that eventually leaks or arcs.

HV connectors are large and sealed for this reason.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/7xAX7ctSBeJ) &mdash; content and formatting may not be reliable*
