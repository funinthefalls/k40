---
layout: post
title: "Laser engraved acrylic... Remember to buy cast acrylic if you are wanting to try something like this Originally shared by Alex Krause I like this photo a bit better..."
date: May 05, 2017 17:12
category: "Object produced with laser"
author: "Alex Krause"
---
Laser engraved acrylic... Remember to buy cast acrylic if you are wanting to try something like this



<b>Originally shared by Alex Krause</b>



I like this photo a bit better... #LaserWeb

![images/c1ec65a5675142131f7558d27057144b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1ec65a5675142131f7558d27057144b.jpeg)



**"Alex Krause"**

---
---
**Ned Hill** *May 05, 2017 17:31*

Nicely done.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/igXb4RLS6Wq) &mdash; content and formatting may not be reliable*
